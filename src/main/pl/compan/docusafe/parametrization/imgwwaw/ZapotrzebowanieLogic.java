/**
 * 
 */
package pl.compan.docusafe.parametrization.imgwwaw;

import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import edu.emory.mathcs.backport.java.util.Collections;
import org.apache.commons.lang.ObjectUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.exports.AttachmentInfo;
import pl.compan.docusafe.core.exports.BudgetItem;
import pl.compan.docusafe.core.exports.ExportedDocument;
import pl.compan.docusafe.core.exports.FundSource;
import pl.compan.docusafe.core.exports.ReservationDocument;
import pl.compan.docusafe.core.jbpm4.AssignUtils;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.Source;
import pl.compan.docusafe.parametrization.imgwwaw.jbpm.PurchaseDocumentTypeDecision;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class ZapotrzebowanieLogic extends ImgwDocumentLogic {
	
	private final Logger logger = LoggerFactory.getLogger(ZapotrzebowanieLogic.class);
	
	public static final String INICJATOR_FIELD = "INICJATOR";
	public static final String STATUS_FIELD = "STATUS";
	public static final String SYMBOL_KOMORKI_FIELD = "SYMBOL_KOMORKI";
	public static final String NUMER_ZAPOTRZEBOWANIA = "NUMER_ZAPOTRZEBOWANIA";
	public static final String PRZEDMIOT_ZAPOTRZEBOWANIA_WARTOSC_NETTO = "PRZEDMIOT_ZAPOTRZEBOWANIA_WARTOSC_NETTO";
	public static final String PRZEDMIOT_ZAPOTRZEBOWANIA = "PRZEDMIOT_ZAPOTRZEBOWANIA";
	public static final String NUMER_ZAPOTRZEBOWANIA_FIELD = "NUMER_ZAPOTRZEBOWANIA";
	public static final String RECIPIENT_HERE_FIELD = "RECIPIENT_HERE";
	public static final String UZASADNIENIE_ZAKUPU_HTML_EDITOR_FIELD = "UZASADNIENIE_ZAKUPU";
	public static final String UZASADNIENIE_ZAKUPU_HTML_FIELD = "UZASADNIENIE_ZAKUPU_HTML";
	
	public static final String DWR_PRZEDMIOT_ZAPOTRZEBOWANIA = "DWR_PRZEDMIOT_ZAPOTRZEBOWANIA";
	public static final String DWR_WARTOSC_NETTO = "DWR_WARTOSC_NETTO";
	
	public static final String JBPM4_VARIABLE_NAME = "financeSourceId";
	public static final String SECTION_SUPERVISOR_ACCEPTATION_CN = "section_supervisor";
	public static final String PRODUCER_ACCEPTATION_CN = "producer";
	
	public static final String SUFFIX = "ZP";
	
	public static final Integer MONEY_SCALE = 2;
	
	protected static final EnumValues EMPTY_ENUM_VALUE = new EnumValues(new HashMap<String, String>(), new ArrayList<String>());

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
		Map<String, Object> toReload = Maps.newHashMap();
		String divisionCode = DSApi.context().getDSUser().getDivisionsWithoutGroup()[0].getCode();
		toReload.put(STATUS_FIELD, 10);
		
		Long authorId = DSApi.context().getDSUser().getId();
		toReload.put(INICJATOR_FIELD, authorId);
		
		toReload.put(SYMBOL_KOMORKI_FIELD, divisionCode);
		
		toReload.put(NUMER_ZAPOTRZEBOWANIA, setRequestNumber(divisionCode));
		
		fm.reloadValues(toReload);
	}
	
	private String setRequestNumber(String divisionCode) {
		boolean isOpened = false;
		Long lp = 0l;
		try{
			Date currentDate = new Date();	
			SimpleDateFormat sdf = new SimpleDateFormat("yy");
			String year = sdf.format(currentDate);
			
			StringBuilder sb = new StringBuilder();
			sb.append(divisionCode)
				.append("/");
			
			String sql = "select count(1) from dsg_imgw_zapotrzebowanie";
			PreparedStatement stmt = DSApi.context().prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			if(rs.next()) {
				lp = rs.getLong(1);
			}
			sb.append(lp)
				.append("/")
				.append(year)
				.append("/")
				.append(SUFFIX);
			
			return sb.toString();
		
		} catch ( EdmException e ) {
			logger.error(e.getMessage(), e);
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (isOpened) {
				DSApi.closeContextIfNeeded(isOpened);
			}
		}
		
		return "";
	}
	
	@Override
	public boolean assignee(OfficeDocument doc, Assignable assignable,OpenExecution openExecution, String acceptationCn,String fieldCnValue) throws EdmException {
		
		boolean assigned = false;
		Long docId = doc.getId();
		FieldsManager fm = new FieldsManager(doc.getId(), doc.getDocumentKind());
		
		if (OPIEKUN_PROJEKTU_ACCEPTANCE.equals(acceptationCn)) {
            Long dictionaryId = (Long) openExecution.getVariable(DICTIONARY_ID_VARIABLE);
                List<String> users = new ArrayList<String>();
                List<String> divisions = new ArrayList<String>();
                AssignUtils.getAssigneesFromAcceptanceTree(users, divisions, FakturaZakupowaLogic.DPK_ACCEPTANCE);
                for (String user : users) {
                    assignable.addCandidateUser(user);
                    AssigneeHandler.addToHistory(openExecution, doc, user, null);
                    assigned = true;
                }
                for (String division : divisions) {
                    assignable.addCandidateGroup(division);
                    AssigneeHandler.addToHistory(openExecution, doc, null, division);
                    assigned = true;
                }
                if (!assigned) {
                    throw new EdmException("Nie znaleziono nikogo do przypisania na drzewie akceptacji " +
                            "dla akceptacji: '" + FakturaZakupowaLogic.DPK_ACCEPTANCE + "'");
                }
		} else if (PRODUCER_ACCEPTATION_CN.equals(acceptationCn)) {
			Recipient recipient = doc.getRecipients().get(0);
			DSUser user = DSUser.findByFirstnameLastname(recipient.getFirstname(), recipient.getLastname());
			Long realizatorId = user.getId();
			assigned = assignUser(doc, assignable, openExecution, realizatorId);
		}
		
		if (!assigned) {
    		assigned = super.assignee(doc, assignable, openExecution, acceptationCn, fieldCnValue);
    	}
		
		return assigned;
		
	}
	
	@Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
        try {
            Map<String, Object> map = Maps.newHashMap();
            map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
            map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
            java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

            DSUser author = DSApi.context().getDSUser();
            String user = author.getName();
            String fullName = author.asFirstnameLastname();

            /*Map<String, Object> fieldValues = new HashMap<String, Object>();
            fieldValues.put(NUMER_ZAPOTRZEBOWANIA_FIELD, getRequisitionNumber(document));
            document.getDocumentKind().setOnly(document.getId(), fieldValues);*/

            perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

            Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
            perms.addAll(documentPermissions);
            this.setUpPermission(document, perms);

            if (AvailabilityManager.isAvailable("addToWatch")) {
                DSApi.context().watch(URN.create(document));
            }
            
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new EdmException(e.getMessage());
        }
    }
    
	public void documentPermissions(Document document) throws EdmException {
        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();;
        String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
        String documentKindName = document.getDocumentKind().getName();

        perms.add(new PermissionBean(ObjectPermission.READ, documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, documentKindName + " - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, documentKindName + " zalacznik - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, documentKindName + " - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, documentKindName + " zalacznik - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, documentKindName + " - usuwanie"));
        Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
        perms.addAll(documentPermissions);
        this.setUpPermission(document, perms);
	}

	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {
	}
	
	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        // zliczanie sumy kwot z mpk'ow ze slownika: "PRZEDMIOT_ZAPOTRZEBOWANIA"
        if (values.containsKey(DWR_PRZEDMIOT_ZAPOTRZEBOWANIA) && values.get(DWR_PRZEDMIOT_ZAPOTRZEBOWANIA) != null && values.get(DWR_PRZEDMIOT_ZAPOTRZEBOWANIA).isSender()) {
        	BigDecimal sum = new BigDecimal(0);
        	sum = sum.setScale(MONEY_SCALE);
        	Map<String, FieldData> dwrValues = (Map<String, FieldData>) values.get(DWR_PRZEDMIOT_ZAPOTRZEBOWANIA).getDictionaryData();
        	for (String key : dwrValues.keySet()) {
        		if (key.contains(PRZEDMIOT_ZAPOTRZEBOWANIA_WARTOSC_NETTO) && dwrValues.get(key).getData() != null) {
        			BigDecimal amount = dwrValues.get(key).getMoneyData().setScale(MONEY_SCALE);
        			sum = sum.add(amount);
        		}
        	}
        	values.get(DWR_WARTOSC_NETTO).setMoneyData(sum);
        }
		return null;
	}
	
	private String getRequisitionNumber(OfficeDocument document) throws EdmException {
		StringBuilder number = new StringBuilder();
		FieldsManager fm = document.getFieldsManager();
		
		String cellCode = (String) fm.getKey(SYMBOL_KOMORKI_FIELD);
		String documentId = document.getId().toString();
		String year = DateUtils.formatYear(Calendar.getInstance().getTime()).substring(2);
		
		number.append(cellCode)
			.append("/")
			.append(documentId)
			.append("/")
			.append(year)
			.append("/")
			.append(SUFFIX);
		return number.toString();
	}
	
    /**
     * Przypisuje u�ytkownika do zadania w procesie
     *
     * @param doc
     * @param assignable
     * @param openExecution
     * @param userId - id usera do kt�rego ma by� przypisane zadanie
     * @return 'true' je�li znaleziono u�ytkownika
     * @throws EdmException
     * @throws UserNotFoundException
     */
    private boolean assignUser(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, Long userId)
            throws EdmException, UserNotFoundException {
        boolean assigned = false;
        String userName = DSUser.findById(userId).getName();
        assignable.addCandidateUser(userName);
        assigned = true;
        AssigneeHandler.addToHistory(openExecution, doc, userName, null);
        return assigned;
    }
    
    private void moveContentFromHtmlEditorFieldToHtmlField(OfficeDocument doc, String htmlEditorFieldCn, String htmlFieldCn) throws EdmException {
    	FieldsManager fm = doc.getFieldsManager();
    	Map<String, Object> toReload = Maps.newHashMap();
    	String content = (String) fm.getKey(htmlEditorFieldCn);
    	toReload.put(htmlFieldCn, content);
    	fm.reloadValues(toReload);
    	//new FieldValue(fm.getField(htmlFieldCn).getFieldForDwr(scheme)tField(), content, content);
    	fm.getDocumentKind().setOnly(doc.getId(), Collections.singletonMap(htmlFieldCn, content));
    }
    
    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) {
        Map<String, Object> results = new HashMap<String, Object>();

        try {
            if (dictionaryName.equals(PRZEDMIOT_ZAPOTRZEBOWANIA)) {
                setBudgetItemsRefValues(dictionaryName, values, results);
                }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return results;
    }

    @Override
    public ExportedDocument buildExportDocument(OfficeDocument doc) throws EdmException {
        FieldsManager fm = doc.getFieldsManager();

        ReservationDocument exported = new ReservationDocument();

        exported.setReservationStateFromId(1);

        //exported.setReservationNumber(fm.getStringKey(ERP_DOCUMENT_IDM));

        //exported.getHeaderBudget().setBudgetDirectCode(Docusafe.getAdditionPropertyOrDefault("export.dokument_rezerwacyjny.idm_budzetu", "IMGW-2014\\00001")/* IDM BUD?ETU, OKRESU BUD?ETOWEGO, USTAWI? NA STA?E WG SERWIS?W ERPOWYCH. DOCELOWO TRZEBA WYRZUCI? DO NAG??WKA*/);
        exported.getHeaderBudget().setBudgetDirectId(Long.valueOf(Docusafe.getAdditionPropertyOrDefault("export.dokument_rezerwacyjny.id_budzetu", "10")));

        exported.setBudgetReservationTypeCode(Docusafe.getAdditionPropertyOrDefault("export.wniosek_rezerwacyjny", "WMR"));
        exported.setReservationMethod(ReservationDocument.ReservationMethod.AUTO);


        List<BudgetItem> positionItems = Lists.newArrayList();
        exported.setPostionItems(positionItems);

        List<Map<String, Object>> positions = (List) fm.getValue(PRZEDMIOT_ZAPOTRZEBOWANIA);
        BudgetItem bItem;
        int nrp = 1;
        for (Map<String, Object> position : positions) {
            positionItems.add(bItem = new BudgetItem());
            bItem.setItemNo(nrp);
            bItem.setQuantity(Objects.firstNonNull((BigDecimal) position.get(PRZEDMIOT_ZAPOTRZEBOWANIA + "_ILOSC"), BigDecimal.ONE));

            bItem.setNetAmount((BigDecimal) position.get(PRZEDMIOT_ZAPOTRZEBOWANIA_WARTOSC_NETTO));

            bItem.setName(position.get(PRZEDMIOT_ZAPOTRZEBOWANIA + "_" + "TERMIN_POZYSKANIA") != null ? "Wymagany termin pozyskania: " + DateUtils.formatCommonDate((Date) position.get(PRZEDMIOT_ZAPOTRZEBOWANIA + "_" + "TERMIN_POZYSKANIA")) : null);
            bItem.setCostKindName(ObjectUtils.toString(position.get(PRZEDMIOT_ZAPOTRZEBOWANIA + "_" + "NAZWA_PRZEDMIOTU")));

            bItem.setBudgetDirectId(exported.getHeaderBudget().getBudgetDirectId());

            bItem.setBudgetKoId(position.get(PRZEDMIOT_ZAPOTRZEBOWANIA + "_" + "BUDGET") != null && !Strings.isNullOrEmpty(((EnumValues) position.get(PRZEDMIOT_ZAPOTRZEBOWANIA + "_" + "BUDGET")).getSelectedId()) ? Long.valueOf(((EnumValues) position.get(PRZEDMIOT_ZAPOTRZEBOWANIA + "_" + "BUDGET")).getSelectedId()) : null);
            bItem.setBudgetKindId(position.get(PRZEDMIOT_ZAPOTRZEBOWANIA + "_" + "POSITION") != null && !Strings.isNullOrEmpty(((EnumValues) position.get(PRZEDMIOT_ZAPOTRZEBOWANIA + "_" + "POSITION")).getSelectedId()) ? Long.valueOf(((EnumValues) position.get(PRZEDMIOT_ZAPOTRZEBOWANIA + "_" + "POSITION")).getSelectedId()) : null);

            if (exported.getHeaderBudget().getBudgetKoId() == null) {
                exported.getHeaderBudget().setBudgetKoId(bItem.getBudgetKoId());
            }

            EnumValues fsEnum = (EnumValues) position.get(PRZEDMIOT_ZAPOTRZEBOWANIA + "_" + "SOURCE");

            if (fsEnum != null && !Strings.isNullOrEmpty(fsEnum.getSelectedId())) {
                FundSource fItem = new FundSource();
                fItem.setFundPercent(new BigDecimal(100));
                fItem.setCode(Source.find(Long.valueOf(fsEnum.getSelectedId())).getZrodloIdn());
                bItem.setFundSources(Lists.newArrayList(fItem));
            }

            nrp++;
        }

        exported.setDescription(buildAdditionDescription(getAdditionFields(doc, fm)));

        String systemPath;

        if (Docusafe.getAdditionProperty("erp.faktura.link_do_skanu") != null) {
            systemPath = Docusafe.getAdditionProperty("erp.faktura.link_do_skanu");
        } else {
            systemPath = Docusafe.getBaseUrl();
        }

        for (Attachment zal : doc.getAttachments()) {
            AttachmentInfo info = new AttachmentInfo();

            String path = systemPath + "/repository/view-attachment-revision.do?id=" + zal.getMostRecentRevision().getId();

            info.setName(zal.getTitle());
            info.setUrl(path);

            exported.addAttachementInfo(info);
        }
        return exported;
    }

    Map<String,String> getAdditionFields(OfficeDocument doc, FieldsManager fm) throws EdmException {
        Map<String,String> additionFields = Maps.newLinkedHashMap();
        additionFields.put("Nr KO",doc.getOfficeNumber() != null ? doc.getOfficeNumber().toString() : "brak");
        additionFields.put("Uzasadnienie zakupu", fm.getStringValue("UZASADNIENIE_ZAKUPU") != null ? fm.getStringValue("UZASADNIENIE_ZAKUPU").replaceAll("\\<.*?\\>", "") : null);
        additionFields.put("Symbol kom�rki", fm.getStringValue("SYMBOL_KOMORKI"));
        additionFields.put("Numer zapotrzebowania", fm.getStringValue("NUMER_ZAPOTRZEBOWANIA"));
        additionFields.put("Opinia", fm.getStringValue("OPINIA"));
        additionFields.put("Decyzja", fm.getStringValue("DECYZJA"));
        return  additionFields;
    }
}
