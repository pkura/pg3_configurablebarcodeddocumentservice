package pl.compan.docusafe.parametrization.imgwwaw.hbm;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Created by brs on 03.02.14.
 *
 */
public class PurchaseDocumentType implements Serializable {
    private final static Logger log = LoggerFactory.getLogger(PurchaseDocumentType.class);

    public static final String ENUM_VIEW_TABLE = "dsg_imgw_view_purchase_document_type";

    private long id;
    private Double erpId;
    private Double czyAktywny;
    private Double czywal;
    private String idm;
    private String name;
    private String currencySymbol;
    private String documentTypeIds;
    private Boolean available;

    public PurchaseDocumentType() {
    }

    public PurchaseDocumentType(long id) {
        this.id = id;
    }

    public PurchaseDocumentType(long id, Double erpId, Double czyAktywny, Double czywal, String idm,
                                String name, String currencySymbol, String documentTypeIds,
                                Boolean available) {
        this.id = id;
        this.erpId = erpId;
        this.czyAktywny = czyAktywny;
        this.czywal = czywal;
        this.idm = idm;
        this.name = name;
        this.currencySymbol = currencySymbol;
        this.documentTypeIds = documentTypeIds;
        this.available = available;
    }

    public void setAllFieldsFromServiceObject(pl.compan.docusafe.ws.imgw.DictionaryServiceStub.PurchaseDocumentType item) {
        setErpId(item.getId());
        setCzyAktywny(item.getCzy_aktywny());
        setCzywal(item.getCzywal());
        setIdm(item.getIdm().trim());
        setName(item.getNazwa());
        setCurrencySymbol(item.getSymbolwaluty());
        setDocumentTypeIds(item.getTypdok_ids());
        setAvailable(new Double(1).equals(item.getCzy_aktywny()) ? true : false);
    }

    public void save() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            new EdmException(e);
        }
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public Double getErpId() {
        return erpId;
    }
    public void setErpId(Double erpId) {
        this.erpId = erpId;
    }
    public Double getCzyAktywny() {
        return czyAktywny;
    }
    public void setCzyAktywny(Double czyAktywny) {
        this.czyAktywny = czyAktywny;
    }
    public Double getCzywal() {
        return czywal;
    }
    public void setCzywal(Double czywal) {
        this.czywal = czywal;
    }
    public String getIdm() {
        return idm;
    }
    public void setIdm(String idm) {
        this.idm = idm;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getCurrencySymbol() {
        return currencySymbol;
    }
    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }
    public String getDocumentTypeIds() {
        return documentTypeIds;
    }
    public void setDocumentTypeIds(String documentTypeIds) {
        this.documentTypeIds = documentTypeIds;
    }
    public Boolean isAvailable() {
        return available;
    }
    public void setAvailable(Boolean available) {
        this.available = available;
    }


    public static List<Long> getDocumentTypesId() throws EdmException {
        List<PurchaseDocumentType> documentTypeList = null;

        try {
            Criteria criteria = DSApi.context().session().createCriteria(PurchaseDocumentType.class);
            criteria.add(Restrictions.eq("available", true));
            documentTypeList = (List<PurchaseDocumentType>) criteria.list();
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
        List<Long> objectIds = new LinkedList<Long>();

        for (PurchaseDocumentType type : documentTypeList) {
            objectIds.add(type.getErpId().longValue());
        }
        return objectIds;
    }
}
