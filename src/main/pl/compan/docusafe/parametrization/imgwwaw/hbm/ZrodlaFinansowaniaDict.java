package pl.compan.docusafe.parametrization.imgwwaw.hbm;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: brs
 * Date: 20.11.13
 * Time: 11:25
 * To change this template use File | Settings | File Templates.
 */
public class ZrodlaFinansowaniaDict implements Serializable{
    private Logger log = LoggerFactory.getLogger(ZrodlaFinansowaniaDict.class);

    private Long id;
    private Integer activityKind;
    private Integer costKind;
    private Integer budget;
    private Integer position;
    private Integer source;
    private Integer contract;
    private Integer product;
    private Integer stawkaVAT;
    private BigDecimal amount;
    private BigDecimal kwotaVAT;
    private Integer kierownik;
    private String nazwaPrzedmiotu;
    private Integer ilosc;
    private BigDecimal wartoscNetto;
    private Date terminPozyskania;

    private static ZrodlaFinansowaniaDict instance;

    public static synchronized ZrodlaFinansowaniaDict getInstance() {
        if (instance == null)
            instance = new ZrodlaFinansowaniaDict();
        return instance;
    }

    public ZrodlaFinansowaniaDict() {
    }

    public ZrodlaFinansowaniaDict(Long id, Integer activityKind, Integer costKind, Integer budget, Integer position, Integer source, Integer contract, Integer product, Integer stawkaVAT, BigDecimal amount, BigDecimal kwotaVAT, Integer kierownik) {
        this.id = id;
        this.activityKind = activityKind;
        this.costKind = costKind;
        this.budget = budget;
        this.position = position;
        this.source = source;
        this.contract = contract;
        this.product = product;
        this.stawkaVAT = stawkaVAT;
        this.amount = amount;
        this.kwotaVAT = kwotaVAT;
        this.kierownik = kierownik;
    }

    public ZrodlaFinansowaniaDict find(Long id) throws EdmException {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(ZrodlaFinansowaniaDict.class);
            criteria.add(Restrictions.eq("id", id));
            return (ZrodlaFinansowaniaDict) criteria.setMaxResults(1).uniqueResult();
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    public void save() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getActivityKind() {
        return activityKind;
    }

    public void setActivityKind(Integer activityKind) {
        this.activityKind = activityKind;
    }

    public Integer getCostKind() {
        return costKind;
    }

    public void setCostKind(Integer costKind) {
        this.costKind = costKind;
    }

    public Integer getBudget() {
        return budget;
    }

    public void setBudget(Integer budget) {
        this.budget = budget;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getSource() {
        return source;
    }

    public void setSource(Integer source) {
        this.source = source;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getContract() {
        return contract;
    }

    public void setContract(Integer contract) {
        this.contract = contract;
    }

    public Integer getProduct() {
        return product;
    }

    public void setProduct(Integer product) {
        this.product = product;
    }

    public Integer getStawkaVAT() {
        return stawkaVAT;
    }

    public void setStawkaVAT(Integer stawkaVAT) {
        this.stawkaVAT = stawkaVAT;
    }

    public BigDecimal getKwotaVAT() {
        return kwotaVAT;
    }

    public void setKwotaVAT(BigDecimal kwotaVAT) {
        this.kwotaVAT = kwotaVAT;
    }

    public Integer getKierownik() {
        return kierownik;
    }

    public void setKierownik(Integer kierownik) {
        this.kierownik = kierownik;
    }

	public String getNazwaPrzedmiotu() {
		return nazwaPrzedmiotu;
	}

	public void setNazwaPrzedmiotu(String nazwaPrzedmiotu) {
		this.nazwaPrzedmiotu = nazwaPrzedmiotu;
	}

	public Integer getIlosc() {
		return ilosc;
	}

	public void setIlosc(Integer ilosc) {
		this.ilosc = ilosc;
	}

	public BigDecimal getWartoscNetto() {
		return wartoscNetto;
	}

	public void setWartoscNetto(BigDecimal wartoscNetto) {
		this.wartoscNetto = wartoscNetto;
	}

	public Date getTerminPozyskania() {
		return terminPozyskania;
	}

	public void setTerminPozyskania(Date terminPozyskania) {
		this.terminPozyskania = terminPozyskania;
	}
}
