package pl.compan.docusafe.parametrization.imgwwaw.hbm;

import org.hibernate.HibernateException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by brs on 20.01.14.
 */
@Entity
@Table(name = IMGWBudgetView2.TABLENAME)
public class IMGWBudgetView2 implements Serializable {
    public static final String TABLENAME = "dsg_imgw_services_budgetview2";
    public static final String SEQUENCE_NAME = TABLENAME + "_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = SEQUENCE_NAME)
    @SequenceGenerator(name = SEQUENCE_NAME, sequenceName = SEQUENCE_NAME)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "bdBudzetId")
    private Double bdBudzetId;
    @Column(name = "bdBudzetIdm", length = 50)
    private String bdBudzetIdm;
    @Column(name = "bdBudzetRodzajId")
    private Double bdBudzetRodzajId;
    @Column(name = "bdBudzetRodzajZadaniaId")
    private Double bdBudzetRodzajZadaniaId;
    @Column(name = "bdBudzetRodzajZrodlaId")
    private Double bdBudzetRodzajZrodlaId;
    @Column(name = "bdGrupaRodzajId")
    private Double bdGrupaRodzajId;
    @Column(name = "bdRodzaj", length = 100)
    private String bdRodzaj;
    @Column(name = "bdStrBudzetIdn", length = 50)
    private String bdStrBudzetIdn;
    @Column(name = "bdStrBudzetIds", length = 50)
    private String bdStrBudzetIds;
    @Column(name = "bdSzablonPozId")
    private Double bdSzablonPozId;
    @Column(name = "bdZadanieId")
    private Double bdZadanieId;
    @Column(name = "bdZadanieIdn", length = 50)
    private String bdZadanieIdn;
    @Column(name = "budWOkrNazwa", length = 50)
    private String budWOkrNazwa;
    @Column(name = "budzetId")
    private Double budzetId;
    @Column(name = "budzetIdm", length = 50)
    private String budzetIdm;
    @Column(name = "budzetIds", length = 50)
    private String budzetIds;
    @Column(name = "budzetNazwa", length = 200)
    private String budzetNazwa;
    @Column(name = "czyAktywna")
    private Double czyAktywna;
    @Column(name = "dokBdStrBudzetIdn", length = 50)
    private String dokBdStrBudzetIdn;
    @Column(name = "dokKomNazwa", length = 200)
    private String dokKomNazwa;
    @Column(name = "dokPozLimit")
    private Double dokPozLimit;
    @Column(name = "dokPozNazwa", length = 200)
    private String dokPozNazwa;
    @Column(name = "dokPozNrpoz")
    private Integer dokPozNrpoz;
    @Column(name = "dokPozPIlosc")
    private Double dokPozPIlosc;
    @Column(name = "dokPozPKosz")
    private Double dokPozPKosz;
    @Column(name = "dokPozRIlosc")
    private Double dokPozRIlosc;
    @Column(name = "dokPozRKoszt")
    private Double dokPozRKoszt;
    @Column(name = "dokPozRezIlosc")
    private Double dokPozRezIlosc;
    @Column(name = "dokPozRezKoszt")
    private Double dokPozRezKoszt;
    @Column(name = "okrrozlId")
    private Double okrrozlId;
    @Column(name = "pozycjaPodrzednaId")
    private Double pozycjaPodrzednaId;
    @Column(name = "strBudNazwa", length = 200)
    private String strBudNazwa;
    @Column(name = "wspolczynnikPozycji")
    private Integer wspolczynnikPozycji;
    @Column(name = "wytworId")
    private Double wytworId;
    @Column(name = "wytworIdm", length = 50)
    private String wytworIdm;
    @Column(name = "zadanieNazwa", length = 200)
    private String zadanieNazwa;
    @Column(name = "zadaniePIlosc")
    private Double zadaniePIlosc;
    @Column(name = "zadaniePKoszt")
    private Double zadaniePKoszt;
    @Column(name = "zadanieRIlosc")
    private Double zadanieRIlosc;
    @Column(name = "zadanieRKoszt")
    private Double zadanieRKoszt;
    @Column(name = "zadanieRezIlosc")
    private Double zadanieRezIlosc;
    @Column(name = "zadanieRezKoszt")
    private Double zadanieRezKoszt;
    @Column(name = "zrodloId")
    private Double zrodloId;
    @Column(name = "zrodloIdn", length = 50)
    private String zrodloIdn;
    @Column(name = "zrodloNazwa", length = 200)
    private String zrodloNazwa;
    @Column(name = "zrodloPKoszt")
    private Double zrodloPKoszt;
    @Column(name = "zrodloRKoszt")
    private Double zrodloRKoszt;
    @Column(name = "zrodloRezKoszt")
    private Double zrodloRezKoszt;
    @Column(name = "available", nullable = false)
    private Boolean available;

    public IMGWBudgetView2() {
    }

    public IMGWBudgetView2(Long id) {
        this.id = id;
    }

    public IMGWBudgetView2(Long id, Double bdBudzetId, String bdBudzetIdm, Double bdBudzetRodzajId, Double bdBudzetRodzajZadaniaId,
                           Double bdBudzetRodzajZrodlaId, Double bdGrupaRodzajId, String bdRodzaj, String bdStrBudzetIdn, String bdStrBudzetIds,
                           Double bdSzablonPozId, Double bdZadanieId, String bdZadanieIdn, String budWOkrNazwa, Double budzetId,
                           String budzetIdm, String budzetIds, String budzetNazwa, Double czyAktywna, String dokBdStrBudzetIdn,
                           String dokKomNazwa, Double dokPozLimit, String dokPozNazwa, Integer dokPozNrpoz, Double dokPozPIlosc,
                           Double dokPozPKosz, Double dokPozRIlosc, Double dokPozRKoszt, Double dokPozRezIlosc, Double dokPozRezKoszt,
                           Double okrrozlId, Double pozycjaPodrzednaId, String strBudNazwa, Integer wspolczynnikPozycji, Double wytworId,
                           String wytworIdm, String zadanieNazwa, Double zadaniePIlosc, Double zadaniePKoszt, Double zadanieRIlosc,
                           Double zadanieRKoszt, Double zadanieRezIlosc, Double zadanieRezKoszt, Double zrodloId, String zrodloIdn,
                           String zrodloNazwa, Double zrodloPKoszt, Double zrodloRKoszt, Double zrodloRezKoszt, Boolean available) {
        this.id = id;
        this.bdBudzetId = bdBudzetId;
        this.bdBudzetIdm = bdBudzetIdm;
        this.bdBudzetRodzajId = bdBudzetRodzajId;
        this.bdBudzetRodzajZadaniaId = bdBudzetRodzajZadaniaId;
        this.bdBudzetRodzajZrodlaId = bdBudzetRodzajZrodlaId;
        this.bdGrupaRodzajId = bdGrupaRodzajId;
        this.bdRodzaj = bdRodzaj;
        this.bdStrBudzetIdn = bdStrBudzetIdn;
        this.bdStrBudzetIds = bdStrBudzetIds;
        this.bdSzablonPozId = bdSzablonPozId;
        this.bdZadanieId = bdZadanieId;
        this.bdZadanieIdn = bdZadanieIdn;
        this.budWOkrNazwa = budWOkrNazwa;
        this.budzetId = budzetId;
        this.budzetIdm = budzetIdm;
        this.budzetIds = budzetIds;
        this.budzetNazwa = budzetNazwa;
        this.czyAktywna = czyAktywna;
        this.dokBdStrBudzetIdn = dokBdStrBudzetIdn;
        this.dokKomNazwa = dokKomNazwa;
        this.dokPozLimit = dokPozLimit;
        this.dokPozNazwa = dokPozNazwa;
        this.dokPozNrpoz = dokPozNrpoz;
        this.dokPozPIlosc = dokPozPIlosc;
        this.dokPozPKosz = dokPozPKosz;
        this.dokPozRIlosc = dokPozRIlosc;
        this.dokPozRKoszt = dokPozRKoszt;
        this.dokPozRezIlosc = dokPozRezIlosc;
        this.dokPozRezKoszt = dokPozRezKoszt;
        this.okrrozlId = okrrozlId;
        this.pozycjaPodrzednaId = pozycjaPodrzednaId;
        this.strBudNazwa = strBudNazwa;
        this.wspolczynnikPozycji = wspolczynnikPozycji;
        this.wytworId = wytworId;
        this.wytworIdm = wytworIdm;
        this.zadanieNazwa = zadanieNazwa;
        this.zadaniePIlosc = zadaniePIlosc;
        this.zadaniePKoszt = zadaniePKoszt;
        this.zadanieRIlosc = zadanieRIlosc;
        this.zadanieRKoszt = zadanieRKoszt;
        this.zadanieRezIlosc = zadanieRezIlosc;
        this.zadanieRezKoszt = zadanieRezKoszt;
        this.zrodloId = zrodloId;
        this.zrodloIdn = zrodloIdn;
        this.zrodloNazwa = zrodloNazwa;
        this.zrodloPKoszt = zrodloPKoszt;
        this.zrodloRKoszt = zrodloRKoszt;
        this.zrodloRezKoszt = zrodloRezKoszt;
        this.available = available;
    }

    public void setAllFieldsFromServiceObject(pl.compan.docusafe.ws.imgw.DictionaryServiceStub.BudgetView2 item) {
        setBdBudzetId(item.getBd_budzet_id());
        setBdBudzetIdm(item.getBd_budzet_idm());
        setBdBudzetRodzajId(item.getBd_budzet_rodzaj_id());
        setBdBudzetRodzajZadaniaId(item.getBd_budzet_rodzaj_zadania_id());
        setBdBudzetRodzajZrodlaId(item.getBd_budzet_rodzaj_zrodla_id());
        setBdGrupaRodzajId(item.getBd_grupa_rodzaj_id());
        setBdRodzaj(item.getBd_rodzaj());
        setBdStrBudzetIdn(item.getBd_str_budzet_idn());
        setBdStrBudzetIds(item.getBd_str_budzet_ids());
        setBdSzablonPozId(item.getBd_szablon_poz_id());
        setBdZadanieId(item.getBd_zadanie_id());
        setBdZadanieIdn(item.getBd_zadanie_idn());
        setBudWOkrNazwa(item.getBud_w_okr_nazwa());
        setBudzetId(item.getBudzet_id());
        setBudzetIdm(item.getBudzet_idm());
        setBudzetIds(item.getBudzet_ids());
        setBudzetNazwa(item.getBudzet_nazwa());
        setCzyAktywna(item.getCzy_aktywna());
        setDokBdStrBudzetIdn(item.getDok_bd_str_budzet_idn());
        setDokKomNazwa(item.getDok_kom_nazwa());
        setDokPozLimit(item.getDok_poz_limit());
        setDokPozNazwa(item.getDok_poz_nazwa());
        setDokPozNrpoz(item.getDok_poz_nrpoz());
        setDokPozPIlosc(item.getDok_poz_p_ilosc());
        setDokPozPKosz(item.getDok_poz_p_kosz());
        setDokPozRIlosc(item.getDok_poz_r_ilosc());
        setDokPozRKoszt(item.getDok_poz_r_koszt());
        setDokPozRezIlosc(item.getDok_poz_rez_ilosc());
        setDokPozRezKoszt(item.getDok_poz_rez_koszt());
        setOkrrozlId(item.getOkrrozl_id());
        setPozycjaPodrzednaId(item.getPozycja_podrzedna_id());
        setStrBudNazwa(item.getStr_bud_nazwa());
        setWspolczynnikPozycji(item.getWspolczynnik_pozycji());
        setWytworId(item.getWytwor_id());
//        setWytworIdm(item.getWytwor_idm());
        setZadanieNazwa(item.getZadanie_nazwa());
        setZadaniePIlosc(item.getZadanie_p_ilosc());
        setZadaniePKoszt(item.getZadanie_p_koszt());
        setZadanieRIlosc(item.getZadanie_r_ilosc());
        setZadanieRKoszt(item.getZadanie_r_koszt());
        setZadanieRezIlosc(item.getZadanie_rez_ilosc());
        setZadanieRezKoszt(item.getZadanie_rez_koszt());
        setZrodloId(item.getZrodlo_id());
        setZrodloIdn(item.getZrodlo_idn());
        setZrodloNazwa(item.getZrodlo_nazwa());
        setZrodloPKoszt(item.getZrodlo_p_koszt());
        setZrodloRKoszt(item.getZrodlo_r_koszt());
        setZrodloRezKoszt(item.getZrodlo_rez_koszt());
        setAvailable(new Double(1).equals(item.getCzy_aktywna()) ? true : false);
    }

    public void save() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            new EdmException(e);
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getBdBudzetId() {
        return bdBudzetId;
    }

    public void setBdBudzetId(Double bdBudzetId) {
        this.bdBudzetId = bdBudzetId;
    }

    public String getBdBudzetIdm() {
        return bdBudzetIdm;
    }

    public void setBdBudzetIdm(String bdBudzetIdm) {
        this.bdBudzetIdm = bdBudzetIdm;
    }

    public Double getBdBudzetRodzajId() {
        return bdBudzetRodzajId;
    }

    public void setBdBudzetRodzajId(Double bdBudzetRodzajId) {
        this.bdBudzetRodzajId = bdBudzetRodzajId;
    }

    public Double getBdBudzetRodzajZadaniaId() {
        return bdBudzetRodzajZadaniaId;
    }

    public void setBdBudzetRodzajZadaniaId(Double bdBudzetRodzajZadaniaId) {
        this.bdBudzetRodzajZadaniaId = bdBudzetRodzajZadaniaId;
    }

    public Double getBdBudzetRodzajZrodlaId() {
        return bdBudzetRodzajZrodlaId;
    }

    public void setBdBudzetRodzajZrodlaId(Double bdBudzetRodzajZrodlaId) {
        this.bdBudzetRodzajZrodlaId = bdBudzetRodzajZrodlaId;
    }

    public Double getBdGrupaRodzajId() {
        return bdGrupaRodzajId;
    }

    public void setBdGrupaRodzajId(Double bdGrupaRodzajId) {
        this.bdGrupaRodzajId = bdGrupaRodzajId;
    }

    public String getBdRodzaj() {
        return bdRodzaj;
    }

    public void setBdRodzaj(String bdRodzaj) {
        this.bdRodzaj = bdRodzaj;
    }

    public String getBdStrBudzetIdn() {
        return bdStrBudzetIdn;
    }

    public void setBdStrBudzetIdn(String bdStrBudzetIdn) {
        this.bdStrBudzetIdn = bdStrBudzetIdn;
    }

    public String getBdStrBudzetIds() {
        return bdStrBudzetIds;
    }

    public void setBdStrBudzetIds(String bdStrBudzetIds) {
        this.bdStrBudzetIds = bdStrBudzetIds;
    }

    public Double getBdSzablonPozId() {
        return bdSzablonPozId;
    }

    public void setBdSzablonPozId(Double bdSzablonPozId) {
        this.bdSzablonPozId = bdSzablonPozId;
    }

    public Double getBdZadanieId() {
        return bdZadanieId;
    }

    public void setBdZadanieId(Double bdZadanieId) {
        this.bdZadanieId = bdZadanieId;
    }

    public String getBdZadanieIdn() {
        return bdZadanieIdn;
    }

    public void setBdZadanieIdn(String bdZadanieIdn) {
        this.bdZadanieIdn = bdZadanieIdn;
    }

    public String getBudWOkrNazwa() {
        return budWOkrNazwa;
    }

    public void setBudWOkrNazwa(String budWOkrNazwa) {
        this.budWOkrNazwa = budWOkrNazwa;
    }

    public Double getBudzetId() {
        return budzetId;
    }

    public void setBudzetId(Double budzetId) {
        this.budzetId = budzetId;
    }

    public String getBudzetIdm() {
        return budzetIdm;
    }

    public void setBudzetIdm(String budzetIdm) {
        this.budzetIdm = budzetIdm;
    }

    public String getBudzetIds() {
        return budzetIds;
    }

    public void setBudzetIds(String budzetIds) {
        this.budzetIds = budzetIds;
    }

    public String getBudzetNazwa() {
        return budzetNazwa;
    }

    public void setBudzetNazwa(String budzetNazwa) {
        this.budzetNazwa = budzetNazwa;
    }

    public Double getCzyAktywna() {
        return czyAktywna;
    }

    public void setCzyAktywna(Double czyAktywna) {
        this.czyAktywna = czyAktywna;
    }

    public String getDokBdStrBudzetIdn() {
        return dokBdStrBudzetIdn;
    }

    public void setDokBdStrBudzetIdn(String dokBdStrBudzetIdn) {
        this.dokBdStrBudzetIdn = dokBdStrBudzetIdn;
    }

    public String getDokKomNazwa() {
        return dokKomNazwa;
    }

    public void setDokKomNazwa(String dokKomNazwa) {
        this.dokKomNazwa = dokKomNazwa;
    }

    public Double getDokPozLimit() {
        return dokPozLimit;
    }

    public void setDokPozLimit(Double dokPozLimit) {
        this.dokPozLimit = dokPozLimit;
    }

    public String getDokPozNazwa() {
        return dokPozNazwa;
    }

    public void setDokPozNazwa(String dokPozNazwa) {
        this.dokPozNazwa = dokPozNazwa;
    }

    public Integer getDokPozNrpoz() {
        return dokPozNrpoz;
    }

    public void setDokPozNrpoz(Integer dokPozNrpoz) {
        this.dokPozNrpoz = dokPozNrpoz;
    }

    public Double getDokPozPIlosc() {
        return dokPozPIlosc;
    }

    public void setDokPozPIlosc(Double dokPozPIlosc) {
        this.dokPozPIlosc = dokPozPIlosc;
    }

    public Double getDokPozPKosz() {
        return dokPozPKosz;
    }

    public void setDokPozPKosz(Double dokPozPKosz) {
        this.dokPozPKosz = dokPozPKosz;
    }

    public Double getDokPozRIlosc() {
        return dokPozRIlosc;
    }

    public void setDokPozRIlosc(Double dokPozRIlosc) {
        this.dokPozRIlosc = dokPozRIlosc;
    }

    public Double getDokPozRKoszt() {
        return dokPozRKoszt;
    }

    public void setDokPozRKoszt(Double dokPozRKoszt) {
        this.dokPozRKoszt = dokPozRKoszt;
    }

    public Double getDokPozRezIlosc() {
        return dokPozRezIlosc;
    }

    public void setDokPozRezIlosc(Double dokPozRezIlosc) {
        this.dokPozRezIlosc = dokPozRezIlosc;
    }

    public Double getDokPozRezKoszt() {
        return dokPozRezKoszt;
    }

    public void setDokPozRezKoszt(Double dokPozRezKoszt) {
        this.dokPozRezKoszt = dokPozRezKoszt;
    }

    public Double getOkrrozlId() {
        return okrrozlId;
    }

    public void setOkrrozlId(Double okrrozlId) {
        this.okrrozlId = okrrozlId;
    }

    public Double getPozycjaPodrzednaId() {
        return pozycjaPodrzednaId;
    }

    public void setPozycjaPodrzednaId(Double pozycjaPodrzednaId) {
        this.pozycjaPodrzednaId = pozycjaPodrzednaId;
    }

    public String getStrBudNazwa() {
        return strBudNazwa;
    }

    public void setStrBudNazwa(String strBudNazwa) {
        this.strBudNazwa = strBudNazwa;
    }

    public Integer getWspolczynnikPozycji() {
        return wspolczynnikPozycji;
    }

    public void setWspolczynnikPozycji(Integer wspolczynnikPozycji) {
        this.wspolczynnikPozycji = wspolczynnikPozycji;
    }

    public Double getWytworId() {
        return wytworId;
    }

    public void setWytworId(Double wytworId) {
        this.wytworId = wytworId;
    }

    public String getWytworIdm() {
        return wytworIdm;
    }

    public void setWytworIdm(String wytworIdm) {
        this.wytworIdm = wytworIdm;
    }

    public String getZadanieNazwa() {
        return zadanieNazwa;
    }

    public void setZadanieNazwa(String zadanieNazwa) {
        this.zadanieNazwa = zadanieNazwa;
    }

    public Double getZadaniePIlosc() {
        return zadaniePIlosc;
    }

    public void setZadaniePIlosc(Double zadaniePIlosc) {
        this.zadaniePIlosc = zadaniePIlosc;
    }

    public Double getZadaniePKoszt() {
        return zadaniePKoszt;
    }

    public void setZadaniePKoszt(Double zadaniePKoszt) {
        this.zadaniePKoszt = zadaniePKoszt;
    }

    public Double getZadanieRIlosc() {
        return zadanieRIlosc;
    }

    public void setZadanieRIlosc(Double zadanieRIlosc) {
        this.zadanieRIlosc = zadanieRIlosc;
    }

    public Double getZadanieRKoszt() {
        return zadanieRKoszt;
    }

    public void setZadanieRKoszt(Double zadanieRKoszt) {
        this.zadanieRKoszt = zadanieRKoszt;
    }

    public Double getZadanieRezIlosc() {
        return zadanieRezIlosc;
    }

    public void setZadanieRezIlosc(Double zadanieRezIlosc) {
        this.zadanieRezIlosc = zadanieRezIlosc;
    }

    public Double getZadanieRezKoszt() {
        return zadanieRezKoszt;
    }

    public void setZadanieRezKoszt(Double zadanieRezKoszt) {
        this.zadanieRezKoszt = zadanieRezKoszt;
    }

    public Double getZrodloId() {
        return zrodloId;
    }

    public void setZrodloId(Double zrodloId) {
        this.zrodloId = zrodloId;
    }

    public String getZrodloIdn() {
        return zrodloIdn;
    }

    public void setZrodloIdn(String zrodloIdn) {
        this.zrodloIdn = zrodloIdn;
    }

    public String getZrodloNazwa() {
        return zrodloNazwa;
    }

    public void setZrodloNazwa(String zrodloNazwa) {
        this.zrodloNazwa = zrodloNazwa;
    }

    public Double getZrodloPKoszt() {
        return zrodloPKoszt;
    }

    public void setZrodloPKoszt(Double zrodloPKoszt) {
        this.zrodloPKoszt = zrodloPKoszt;
    }

    public Double getZrodloRKoszt() {
        return zrodloRKoszt;
    }

    public void setZrodloRKoszt(Double zrodloRKoszt) {
        this.zrodloRKoszt = zrodloRKoszt;
    }

    public Double getZrodloRezKoszt() {
        return zrodloRezKoszt;
    }

    public void setZrodloRezKoszt(Double zrodloRezKoszt) {
        this.zrodloRezKoszt = zrodloRezKoszt;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }
}
