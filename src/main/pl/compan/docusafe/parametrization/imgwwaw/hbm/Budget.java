package pl.compan.docusafe.parametrization.imgwwaw.hbm;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.imgw.DictionaryServicesBudgetViewsStub;

import java.math.BigDecimal;

@SuppressWarnings("serial")
public class Budget implements java.io.Serializable {

    private static Logger log = LoggerFactory.getLogger(Budget.class);
    private long id;
    private long bdBudzetId;
    private String bdBudzetIdm;
    private String bdNazwa;
    private long bdPozBudzetId;
    private BigDecimal bdPozDoWykKoszt;
    private String bdRodzaj;
    private int bdUklad;
    private int czyArchiwalny;
    private long komBudId;
    private String komBudIdn;
    private String komBudNazwa;
    private long okrBudId;
    private String okrBudIdm;
    private String okrBudNazwa;
    private long okrBudRokObr;
    private long okrBudTypId;
    private String pBdPozBudzetNazwa;
    private int pBdPozBudzetNrpoz;
    private BigDecimal pBdPozPKoszt;
    private BigDecimal pBdPozRKoszt;
    private BigDecimal pBdPozRezKoszt;

    public Budget() {
    }

    public Budget(long id) {
        this.id = id;
    }


    public Budget(long id, long bdBudzetId, String bdBudzetIdm, String bdNazwa, long bdPozBudzetId, BigDecimal bdPozDoWykKoszt, String bdRodzaj,
                  int bdUklad, int czyArchiwalny, long komBudId, String komBudIdn, String komBudNazwa, long okrBudId, String okrBudIdm,
                  String okrBudNazwa, long okrBudRokObr, long okrBudTypId, String pBdPozBudzetNazwa, int pBdPozBudzetNrpoz,
                  BigDecimal pBdPozPKoszt, BigDecimal pBdPozRKoszt, BigDecimal pBdPozRezKoszt) {
        this.id = id;
        this.bdBudzetId = bdBudzetId;
        this.bdBudzetIdm = bdBudzetIdm;
        this.bdNazwa = bdNazwa;
        this.bdPozBudzetId = bdPozBudzetId;
        this.bdPozDoWykKoszt = bdPozDoWykKoszt;
        this.bdRodzaj = bdRodzaj;
        this.bdUklad = bdUklad;
        this.czyArchiwalny = czyArchiwalny;
        this.komBudId = komBudId;
        this.komBudIdn = komBudIdn;
        this.komBudNazwa = komBudNazwa;
        this.okrBudId = okrBudId;
        this.okrBudIdm = okrBudIdm;
        this.okrBudNazwa = okrBudNazwa;
        this.okrBudRokObr = okrBudRokObr;
        this.okrBudTypId = okrBudTypId;
        this.pBdPozBudzetNazwa = pBdPozBudzetNazwa;
        this.pBdPozBudzetNrpoz = pBdPozBudzetNrpoz;
        this.pBdPozPKoszt = pBdPozPKoszt;
        this.pBdPozRKoszt = pBdPozRKoszt;
        this.pBdPozRezKoszt = pBdPozRezKoszt;
    }

    public static Budget find(long id) throws EdmException {
        return Finder.find(Budget.class, id);
    }

    public static Budget findByBudzetIdAndPozBudzetId(long bdBudzetId, long bdPozBudzetId) {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(Budget.class);
            criteria.add(Restrictions.eq("bdBudzetId", bdBudzetId));
            criteria.add(Restrictions.eq("bdPozBudzetId", bdPozBudzetId));
            return (Budget) criteria.setMaxResults(1).uniqueResult();
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    public static Budget findByBudzetIdmAndPozBudzetNazwa(String budzetIdm, String pozBudzetNazwa) {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(Budget.class);
            criteria.add(Restrictions.eq("bdBudzetIdm", budzetIdm));
            criteria.add(Restrictions.eq("pBdPozBudzetNazwa", pozBudzetNazwa));
            return (Budget) criteria.setMaxResults(1).uniqueResult();
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getBdBudzetId() {
        return bdBudzetId;
    }

    public void setBdBudzetId(long bdBudzetId) {
        this.bdBudzetId = bdBudzetId;
    }

    public String getBdBudzetIdm() {
        return bdBudzetIdm;
    }

    public void setBdBudzetIdm(String bdBudzetIdm) {
        this.bdBudzetIdm = bdBudzetIdm;
    }

    public String getBdNazwa() {
        return bdNazwa;
    }

    public void setBdNazwa(String bdNazwa) {
        this.bdNazwa = bdNazwa;
    }

    public long getBdPozBudzetId() {
        return bdPozBudzetId;
    }

    public void setBdPozBudzetId(long bdPozBudzetId) {
        this.bdPozBudzetId = bdPozBudzetId;
    }

    public BigDecimal getBdPozDoWykKoszt() {
        return bdPozDoWykKoszt;
    }

    public void setBdPozDoWykKoszt(BigDecimal bdPozDoWykKoszt) {
        this.bdPozDoWykKoszt = bdPozDoWykKoszt;
    }

    public String getBdRodzaj() {
        return bdRodzaj;
    }

    public void setBdRodzaj(String bdRodzaj) {
        this.bdRodzaj = bdRodzaj;
    }

    public int getBdUklad() {
        return bdUklad;
    }

    public void setBdUklad(int bdUklad) {
        this.bdUklad = bdUklad;
    }

    public int getCzyArchiwalny() {
        return czyArchiwalny;
    }

    public void setCzyArchiwalny(int czyArchiwalny) {
        this.czyArchiwalny = czyArchiwalny;
    }

    public long getKomBudId() {
        return komBudId;
    }

    public void setKomBudId(long komBudId) {
        this.komBudId = komBudId;
    }

    public String getKomBudIdn() {
        return komBudIdn;
    }

    public void setKomBudIdn(String komBudIdn) {
        this.komBudIdn = komBudIdn;
    }

    public String getKomBudNazwa() {
        return komBudNazwa;
    }

    public void setKomBudNazwa(String komBudNazwa) {
        this.komBudNazwa = komBudNazwa;
    }

    public long getOkrBudId() {
        return okrBudId;
    }

    public void setOkrBudId(long okrBudId) {
        this.okrBudId = okrBudId;
    }

    public String getOkrBudIdm() {
        return okrBudIdm;
    }

    public void setOkrBudIdm(String okrBudIdm) {
        this.okrBudIdm = okrBudIdm;
    }

    public String getOkrBudNazwa() {
        return okrBudNazwa;
    }

    public void setOkrBudNazwa(String okrBudNazwa) {
        this.okrBudNazwa = okrBudNazwa;
    }

    public long getOkrBudRokObr() {
        return okrBudRokObr;
    }

    public void setOkrBudRokObr(long okrBudRokObr) {
        this.okrBudRokObr = okrBudRokObr;
    }

    public long getOkrBudTypId() {
        return okrBudTypId;
    }

    public void setOkrBudTypId(long okrBudTypId) {
        this.okrBudTypId = okrBudTypId;
    }

    public String getpBdPozBudzetNazwa() {
        return pBdPozBudzetNazwa;
    }

    public void setpBdPozBudzetNazwa(String pBdPozBudzetNazwa) {
        this.pBdPozBudzetNazwa = pBdPozBudzetNazwa;
    }

    public int getpBdPozBudzetNrpoz() {
        return pBdPozBudzetNrpoz;
    }

    public void setpBdPozBudzetNrpoz(int pBdPozBudzetNrpoz) {
        this.pBdPozBudzetNrpoz = pBdPozBudzetNrpoz;
    }

    public BigDecimal getpBdPozPKoszt() {
        return pBdPozPKoszt;
    }

    public void setpBdPozPKoszt(BigDecimal pBdPozPKoszt) {
        this.pBdPozPKoszt = pBdPozPKoszt;
    }

    public BigDecimal getpBdPozRKoszt() {
        return pBdPozRKoszt;
    }

    public void setpBdPozRKoszt(BigDecimal pBdPozRKoszt) {
        this.pBdPozRKoszt = pBdPozRKoszt;
    }

    public BigDecimal getpBdPozRezKoszt() {
        return pBdPozRezKoszt;
    }

    public void setpBdPozRezKoszt(BigDecimal pBdPozRezKoszt) {
        this.pBdPozRezKoszt = pBdPozRezKoszt;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Budget)) return false;

        Budget budget = (Budget) o;

        if (bdBudzetId != budget.bdBudzetId) return false;
        if (bdPozBudzetId != budget.bdPozBudzetId) return false;
        if (bdUklad != budget.bdUklad) return false;
        if (czyArchiwalny != budget.czyArchiwalny) return false;
        if (id != budget.id) return false;
        if (komBudId != budget.komBudId) return false;
        if (okrBudId != budget.okrBudId) return false;
        if (okrBudRokObr != budget.okrBudRokObr) return false;
        if (okrBudTypId != budget.okrBudTypId) return false;
        if (pBdPozBudzetNrpoz != budget.pBdPozBudzetNrpoz) return false;
        if (bdBudzetIdm != null ? !bdBudzetIdm.equals(budget.bdBudzetIdm) : budget.bdBudzetIdm != null) return false;
        if (bdNazwa != null ? !bdNazwa.equals(budget.bdNazwa) : budget.bdNazwa != null) return false;
        if (bdPozDoWykKoszt != null ? !bdPozDoWykKoszt.equals(budget.bdPozDoWykKoszt) : budget.bdPozDoWykKoszt != null) return false;
        if (bdRodzaj != null ? !bdRodzaj.equals(budget.bdRodzaj) : budget.bdRodzaj != null) return false;
        if (komBudIdn != null ? !komBudIdn.equals(budget.komBudIdn) : budget.komBudIdn != null) return false;
        if (komBudNazwa != null ? !komBudNazwa.equals(budget.komBudNazwa) : budget.komBudNazwa != null) return false;
        if (okrBudIdm != null ? !okrBudIdm.equals(budget.okrBudIdm) : budget.okrBudIdm != null) return false;
        if (okrBudNazwa != null ? !okrBudNazwa.equals(budget.okrBudNazwa) : budget.okrBudNazwa != null) return false;
        if (pBdPozBudzetNazwa != null ? !pBdPozBudzetNazwa.equals(budget.pBdPozBudzetNazwa) : budget.pBdPozBudzetNazwa != null)
            return false;
        if (pBdPozPKoszt != null ? !pBdPozPKoszt.equals(budget.pBdPozPKoszt) : budget.pBdPozPKoszt != null) return false;
        if (pBdPozRKoszt != null ? !pBdPozRKoszt.equals(budget.pBdPozRKoszt) : budget.pBdPozRKoszt != null) return false;
        if (pBdPozRezKoszt != null ? !pBdPozRezKoszt.equals(budget.pBdPozRezKoszt) : budget.pBdPozRezKoszt != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (bdBudzetId ^ (bdBudzetId >>> 32));
        result = 31 * result + (bdBudzetIdm != null ? bdBudzetIdm.hashCode() : 0);
        result = 31 * result + (bdNazwa != null ? bdNazwa.hashCode() : 0);
        result = 31 * result + (int) (bdPozBudzetId ^ (bdPozBudzetId >>> 32));
        result = 31 * result + (bdPozDoWykKoszt != null ? bdPozDoWykKoszt.hashCode() : 0);
        result = 31 * result + (bdRodzaj != null ? bdRodzaj.hashCode() : 0);
        result = 31 * result + bdUklad;
        result = 31 * result + czyArchiwalny;
        result = 31 * result + (int) (komBudId ^ (komBudId >>> 32));
        result = 31 * result + (komBudIdn != null ? komBudIdn.hashCode() : 0);
        result = 31 * result + (komBudNazwa != null ? komBudNazwa.hashCode() : 0);
        result = 31 * result + (int) (okrBudId ^ (okrBudId >>> 32));
        result = 31 * result + (okrBudIdm != null ? okrBudIdm.hashCode() : 0);
        result = 31 * result + (okrBudNazwa != null ? okrBudNazwa.hashCode() : 0);
        result = 31 * result + (int) (okrBudRokObr ^ (okrBudRokObr >>> 32));
        result = 31 * result + (int) (okrBudTypId ^ (okrBudTypId >>> 32));
        result = 31 * result + (pBdPozBudzetNazwa != null ? pBdPozBudzetNazwa.hashCode() : 0);
        result = 31 * result + pBdPozBudzetNrpoz;
        result = 31 * result + (pBdPozPKoszt != null ? pBdPozPKoszt.hashCode() : 0);
        result = 31 * result + (pBdPozRKoszt != null ? pBdPozRKoszt.hashCode() : 0);
        result = 31 * result + (pBdPozRezKoszt != null ? pBdPozRezKoszt.hashCode() : 0);
        return result;
    }

    public void setAllFieldsFromServiceObject(DictionaryServicesBudgetViewsStub.Budget item) {
        this.bdBudzetId = item.getBd_budzet_id();
        this.bdBudzetIdm = item.getBd_budzet_idm();
        this.bdNazwa = item.getBd_nazwa();
        this.bdPozBudzetId = item.getBd_poz_budzet_id();
        this.bdPozDoWykKoszt = item.getBd_poz_do_wyk_koszt();
        this.bdRodzaj = item.getBd_rodzaj();
        this.bdUklad = item.getBd_uklad();
        this.czyArchiwalny = item.getCzy_archiwalny();
        this.komBudId = item.getKom_bud_id();
        this.komBudIdn = item.getKom_bud_idn();
        this.komBudNazwa = item.getKom_bud_nazwa();
        this.okrBudId = item.getOkr_bud_id();
        this.okrBudIdm = item.getOkr_bud_idm();
        this.okrBudNazwa = item.getOkr_bud_nazwa();
        this.okrBudRokObr = item.getOkr_bud_rok_obr();
        this.okrBudTypId = item.getOkr_bud_typ_id();
        this.pBdPozBudzetNazwa = item.getP_bd_poz_budzet_nazwa();
        this.pBdPozBudzetNrpoz = item.getP_bd_poz_budzet_nrpoz();
        this.pBdPozPKoszt = item.getP_bd_poz_p_koszt();
        this.pBdPozRKoszt = item.getP_bd_poz_r_koszt();
        this.pBdPozRezKoszt = item.getP_bd_poz_rez_koszt();
    }

    public void save() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }
}
