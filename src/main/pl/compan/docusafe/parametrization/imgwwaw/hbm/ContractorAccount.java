package pl.compan.docusafe.parametrization.imgwwaw.hbm;

import org.hibernate.HibernateException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.ws.imgw.DictionaryServiceStub;

/**
 * Created by brs on 10.12.13.
 */
public class ContractorAccount {

    private int id;
    private double czyAktywne;
    private double erpId;
    /**Powiazanie do dostawcy*/
    private double identyfikatorNum;
    private String idm;
    private int kolejnoscUzycia;
    private String numerKonta;
    private String symbolwaluty;
    private boolean available;

    public ContractorAccount(int id, double czyAktywne, double erpId, double identyfikatorNum, String idm, int kolejnoscUzycia, String numerKonta, String symbolwaluty) {
        this.id = id;
        this.czyAktywne = czyAktywne;
        this.erpId = erpId;
        this.identyfikatorNum = identyfikatorNum;
        this.idm = idm;
        this.kolejnoscUzycia = kolejnoscUzycia;
        this.numerKonta = numerKonta;
        this.symbolwaluty = symbolwaluty;
    }

    public ContractorAccount() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getCzyAktywne() {
        return czyAktywne;
    }

    public void setCzyAktywne(double czyAktywne) {
        this.czyAktywne = czyAktywne;
    }

    public double getErpId() {
        return erpId;
    }

    public void setErpId(double erpId) {
        this.erpId = erpId;
    }

    public double getIdentyfikatorNum() {
        return identyfikatorNum;
    }

    public void setIdentyfikatorNum(double identyfikatorNum) {
        this.identyfikatorNum = identyfikatorNum;
    }

    public String getIdm() {
        return idm;
    }

    public void setIdm(String idm) {
        this.idm = idm;
    }

    public int getKolejnoscUzycia() {
        return kolejnoscUzycia;
    }

    public void setKolejnoscUzycia(int kolejnoscUzycia) {
        this.kolejnoscUzycia = kolejnoscUzycia;
    }

    public String getNumerKonta() {
        return numerKonta;
    }

    public void setNumerKonta(String numerKonta) {
        this.numerKonta = numerKonta;
    }

    public String getSymbolwaluty() {
        return symbolwaluty;
    }

    public void setSymbolwaluty(String symbolwaluty) {
        this.symbolwaluty = symbolwaluty;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public void setAllFieldsFromServiceObject (DictionaryServiceStub.SupplierBankAccount fromService) {
        this.setErpId(fromService.getId());
        this.setCzyAktywne(1);
        this.setIdentyfikatorNum(fromService.getDostawca_id());
        this.setIdm(fromService.getIdm());
        this.setKolejnoscUzycia(fromService.getKolejnosc_uzycia());
        this.setNumerKonta(fromService.getNumer_konta());
        this.setSymbolwaluty(fromService.getSymbolwaluty());
    }

    public void save() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }
}
