package pl.compan.docusafe.parametrization.imgwwaw.hbm;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.imgw.DictionaryServiceStub;

import java.util.Date;

/**
 * Created by brs on 12.12.13.
 */
public class IMGWVatRate {

    private final static Logger log = LoggerFactory.getLogger(IMGWVatRate.class);

    private int id;
    private double czyAktywna;
    private double czyZwolniona;
    private Date dataObowDo;
    private double erpId;
    private String idm;
    private String nazwa;
    private double niePodlega;
    private double procent;
    private boolean available;

    public IMGWVatRate() {
    }

    public IMGWVatRate(int id, double czyAktywna, double czyZwolniona, Date dataObowDo, double erpId, String idm, String nazwa, double niePodlega, double procent, boolean available) {
        this.id = id;
        this.czyAktywna = czyAktywna;
        this.czyZwolniona = czyZwolniona;
        this.dataObowDo = dataObowDo;
        this.erpId = erpId;
        this.idm = idm;
        this.nazwa = nazwa;
        this.niePodlega = niePodlega;
        this.procent = procent;
        this.available = available;
    }

    public static IMGWVatRate find(int id) throws EdmException {
        return Finder.find(IMGWVatRate.class, id);
    }

    public static IMGWVatRate findByIdm(String code) {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(IMGWVatRate.class);
            criteria.add(Restrictions.eq("idm", code));
            return (IMGWVatRate) criteria.setMaxResults(1).uniqueResult();
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getCzyAktywna() {
        return czyAktywna;
    }

    public void setCzyAktywna(double czyAktywna) {
        this.czyAktywna = czyAktywna;
    }

    public double getCzyZwolniona() {
        return czyZwolniona;
    }

    public void setCzyZwolniona(double czyZwolniona) {
        this.czyZwolniona = czyZwolniona;
    }

    public Date getDataObowDo() {
        return dataObowDo;
    }

    public void setDataObowDo(Date dataObowDo) {
        this.dataObowDo = dataObowDo;
    }

    public double getErpId() {
        return erpId;
    }

    public void setErpId(double erpId) {
        this.erpId = erpId;
    }

    public String getIdm() {
        return idm;
    }

    public void setIdm(String idm) {
        this.idm = idm;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public double getNiePodlega() {
        return niePodlega;
    }

    public void setNiePodlega(double niePodlega) {
        this.niePodlega = niePodlega;
    }

    public double getProcent() {
        return procent;
    }

    public void setProcent(double procent) {
        this.procent = procent;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public void setAllFieldsFromServiceObject (DictionaryServiceStub.VatRate fromService) {
        this.setCzyAktywna(fromService.getCzy_aktywna());
        this.setCzyZwolniona(fromService.getCzy_zwolniona());
        this.setDataObowDo(fromService.getData_obow_do());
        this.setErpId(fromService.getId());
        this.setIdm(fromService.getIdm());
        this.setNazwa(fromService.getNazwa());
        this.setNiePodlega(fromService.getNie_podlega());
        this.setProcent(fromService.getProcent());
    }

    public void save() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

}
