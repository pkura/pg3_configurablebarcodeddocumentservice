package pl.compan.docusafe.parametrization.imgwwaw.hbm;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.imgw.DictionaryServiceStub;
import pl.compan.docusafe.ws.imgw.DictionaryServicesCostInvoiceProductStub;

@SuppressWarnings("serial")
public class CostInvoiceProduct implements java.io.Serializable {

    private final static Logger log = LoggerFactory.getLogger(CostInvoiceProduct.class);

	private long id;
	private long wytworId;
	private String wytworIdm;
	private String jmIdn;
	private String klasyWytwIdn;
	private String nazwa;
	private int rodzTowaru;
	private String vatStawIds;
	private Boolean available;

	public CostInvoiceProduct() {
	}

	public CostInvoiceProduct(long id) {
		this.id = id;
	}

    public CostInvoiceProduct(long id, Boolean available, String jmIdn, String klasyWytwIdn, String nazwa, int rodzTowaru, String vatStawIds, long wytworId, String wytworIdm) {
        this.id = id;
        this.available = available;
        this.jmIdn = jmIdn;
        this.klasyWytwIdn = klasyWytwIdn;
        this.nazwa = nazwa;
        this.rodzTowaru = rodzTowaru;
        this.vatStawIds = vatStawIds;
        this.wytworId = wytworId;
        this.wytworIdm = wytworIdm;
    }

    public static CostInvoiceProduct findByIdm(String idm) {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(CostInvoiceProduct.class);
            criteria.add(Restrictions.eq("wytworIdm", idm));
            return (CostInvoiceProduct) criteria.setMaxResults(1).uniqueResult();
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public String getJmIdn() {
        return jmIdn;
    }

    public void setJmIdn(String jmIdn) {
        this.jmIdn = jmIdn;
    }

    public String getKlasyWytwIdn() {
        return klasyWytwIdn;
    }

    public void setKlasyWytwIdn(String klasyWytwIdn) {
        this.klasyWytwIdn = klasyWytwIdn;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public int getRodzTowaru() {
        return rodzTowaru;
    }

    public void setRodzTowaru(int rodzTowaru) {
        this.rodzTowaru = rodzTowaru;
    }

    public String getVatStawIds() {
        return vatStawIds;
    }

    public void setVatStawIds(String vatStawIds) {
        this.vatStawIds = vatStawIds;
    }

    public long getWytworId() {
        return wytworId;
    }

    public void setWytworId(long wytworId) {
        this.wytworId = wytworId;
    }

    public String getWytworIdm() {
        return wytworIdm;
    }

    public void setWytworIdm(String wytworIdm) {
        this.wytworIdm = wytworIdm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostInvoiceProduct)) return false;

        CostInvoiceProduct that = (CostInvoiceProduct) o;

        if (id != that.id) return false;
        if (rodzTowaru != that.rodzTowaru) return false;
        if (wytworId != that.wytworId) return false;
        if (available != null ? !available.equals(that.available) : that.available != null) return false;
        if (jmIdn != null ? !jmIdn.equals(that.jmIdn) : that.jmIdn != null) return false;
        if (klasyWytwIdn != null ? !klasyWytwIdn.equals(that.klasyWytwIdn) : that.klasyWytwIdn != null) return false;
        if (nazwa != null ? !nazwa.equals(that.nazwa) : that.nazwa != null) return false;
        if (vatStawIds != null ? !vatStawIds.equals(that.vatStawIds) : that.vatStawIds != null) return false;
        if (wytworIdm != null ? !wytworIdm.equals(that.wytworIdm) : that.wytworIdm != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (available != null ? available.hashCode() : 0);
        result = 31 * result + (jmIdn != null ? jmIdn.hashCode() : 0);
        result = 31 * result + (klasyWytwIdn != null ? klasyWytwIdn.hashCode() : 0);
        result = 31 * result + (nazwa != null ? nazwa.hashCode() : 0);
        result = 31 * result + rodzTowaru;
        result = 31 * result + (vatStawIds != null ? vatStawIds.hashCode() : 0);
        result = 31 * result + (int) (wytworId ^ (wytworId >>> 32));
        result = 31 * result + (wytworIdm != null ? wytworIdm.hashCode() : 0);
        return result;
    }

    public void setAllFieldsFromServiceObject(DictionaryServiceStub.CostInvoiceProduct item) {
		this.setJmIdn(item.getJm_idn());
		this.setKlasyWytwIdn(item.getKlaswytw_idn());
		this.setNazwa(item.getNazwa().trim());
		this.setRodzTowaru(item.getRodztowaru());
		this.setVatStawIds(item.getVatstaw_ids());
		this.setWytworId((long) item.getId());
		this.setWytworIdm(item.getIdm().trim());
	}
	
	public void save() throws EdmException {
		try {
			DSApi.context().session().save(this);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
}
