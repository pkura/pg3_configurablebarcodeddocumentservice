package pl.compan.docusafe.parametrization.imgwwaw.hbm;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.imgw.DictionaryServiceStub;
import pl.compan.docusafe.ws.imgw.DictionaryServiceStub.RolesInProjects;

import java.util.Date;

/**
 * Created by brs on 10.12.13.
 */
public class Kontrakt {

    private final static Logger log = LoggerFactory.getLogger(Kontrakt.class);
    private int id;
    private double erpId;
    private double identyfikatorNum;
    private String idm;
    private String nazwa;
    private Date pDataKo;
    private Date pDataPo;
    private double typKontraktId;
    private String typKontraktIdn;
    private Integer kierownikId;
    private Integer opiekunId;
    private Boolean dodatkowaAkceptacja;
    private Double bpRodzajWersjiId;

    public Kontrakt(int id, double erpId, double identyfikatorNum, String idm, String nazwa, Date pDataKo, Date pDataPo,
                    double typKontraktId, String typKontraktIdn, Integer kierownikId, Integer opiekunId,
                    Boolean dodatkowaAkceptacja, Double bpRodzajWersjiId) {
        this.id = id;
        this.erpId = erpId;
        this.identyfikatorNum = identyfikatorNum;
        this.idm = idm;
        this.nazwa = nazwa;
        this.pDataKo = pDataKo;
        this.pDataPo = pDataPo;
        this.typKontraktId = typKontraktId;
        this.typKontraktIdn = typKontraktIdn;
        this.kierownikId = kierownikId;
        this.opiekunId = opiekunId;
        this.dodatkowaAkceptacja = dodatkowaAkceptacja;
        this.bpRodzajWersjiId = bpRodzajWersjiId;
    }

    public Kontrakt() {

    }

    public static Kontrakt find(Integer id) throws EdmException {
        return Finder.find(Kontrakt.class, id); 
    }

    public static Kontrakt findByIdm(String idm) {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(Kontrakt.class);
            criteria.add(Restrictions.eq("idm", idm));
            return (Kontrakt) criteria.setMaxResults(1).uniqueResult();
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getErpId() {
        return erpId;
    }

    public void setErpId(double erpId) {
        this.erpId = erpId;
    }

    public double getIdentyfikatorNum() {
        return identyfikatorNum;
    }

    public void setIdentyfikatorNum(double identyfikatorNum) {
        this.identyfikatorNum = identyfikatorNum;
    }

    public String getIdm() {
        return idm;
    }

    public void setIdm(String idm) {
        this.idm = idm;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public Date getpDataKo() {
        return pDataKo;
    }

    public void setpDataKo(Date pDataKo) {
        this.pDataKo = pDataKo;
    }

    public Date getpDataPo() {
        return pDataPo;
    }

    public void setpDataPo(Date pDataPo) {
        this.pDataPo = pDataPo;
    }

    public double getTypKontraktId() {
        return typKontraktId;
    }

    public void setTypKontraktId(double typKontraktId) {
        this.typKontraktId = typKontraktId;
    }

    public String getTypKontraktIdn() {
        return typKontraktIdn;
    }

    public void setTypKontraktIdn(String typKontraktIdn) {
        this.typKontraktIdn = typKontraktIdn;
    }

    public Integer getKierownikId() {
        return kierownikId;
    }

    public void setKierownikId(Integer kierownikId) {
        this.kierownikId = kierownikId;
    }

    public Integer getOpiekunId() {
        return opiekunId;
    }

    public void setOpiekunId(Integer opiekunId) {
        this.opiekunId = opiekunId;
    }

    public Boolean isDodatkowaAkceptacja() {
        return dodatkowaAkceptacja;
    }

    public void setDodatkowaAkceptacja(Boolean dodatkowaAkceptacja) {
        this.dodatkowaAkceptacja = dodatkowaAkceptacja;
    }

    public Double getBpRodzajWersjiId() {
        return bpRodzajWersjiId;
    }

    public void setBpRodzajWersjiId(Double bpRodzajWersjiId) {
        this.bpRodzajWersjiId = bpRodzajWersjiId;
    }

    public void setAllFieldsFromServiceObject(DictionaryServiceStub.Contract fromService) {
        this.setErpId(fromService.getId());
        this.setIdentyfikatorNum(fromService.getIdentyfikator_num());
        this.setIdm(fromService.getIdm());
        this.setNazwa(fromService.getNazwa());
        this.setpDataKo(fromService.getP_datako());
        this.setpDataPo(fromService.getP_datapo());
        this.setTypKontraktId(fromService.getTyp_kontrakt_id());
        this.setTypKontraktIdn(fromService.getTyp_kontrakt_idn());
        this.setBpRodzajWersjiId((fromService.getBp_rodzaj_wersji_id()));
    }

    public void save() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public void setAllFieldsFromServiceObject(RolesInProjects item) throws EdmException {
        if ("Kier. projektu".equals(item.getBp_rola_idn())) {
            if (item.getPracownik_id() < 1)
                this.setKierownikId(null);
            else {
                DSUser user = null;
                try {
                    user = DSUser.findByIdentifier(item.getOsoba_guid());
                    this.setKierownikId(user.getId().intValue());
                } catch (EdmException e) {
                    log.error("Nie znaleziono pracownika o numerze getOsoba_guid() = " + item.getOsoba_guid());
                }

            }
        } else if ("Opiekin Faktur".equals(item.getBp_rola_idn())) {
            if (item.getPracownik_id() < 1)
                this.setOpiekunId(null);
            else {
                DSUser user = null;
                try {
                    user = DSUser.findByIdentifier(item.getOsoba_guid());
                    this.setOpiekunId(user.getId().intValue());
                } catch (EdmException e) {
                    log.error("Nie znaleziono pracownika o numerze ewidencyjnym getPracownik_nrewid() = " + item.getOsoba_guid());
                }

            }
        }
    }
}
