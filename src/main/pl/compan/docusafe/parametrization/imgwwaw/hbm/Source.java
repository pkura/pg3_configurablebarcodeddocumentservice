package pl.compan.docusafe.parametrization.imgwwaw.hbm;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.imgw.DictionaryServicesBudgetViewsStub;

import java.math.BigDecimal;

@SuppressWarnings("serial")
public class Source implements java.io.Serializable {

    private final static Logger log = LoggerFactory.getLogger(Source.class);

    private long id;
    private long bdBudzetPozId;
    private int rodzaj;
    private long zrodloId;
    private String zrodloIdn;
    private String zrodloNazwa;
    private BigDecimal zrodloPKoszt;
    private BigDecimal zrodloRKoszt;
    private BigDecimal zrodloRezKoszt;

    public Source() {
    }

    public Source(long id) {
        this.id = id;
    }

    public Source(long id, long bdBudzetPozId, int rodzaj, long zrodloId, String zrodloIdn, String zrodloNazwa, BigDecimal zrodloPKoszt, BigDecimal zrodloRKoszt, BigDecimal zrodloRezKoszt) {
        this.id = id;
        this.bdBudzetPozId = bdBudzetPozId;
        this.rodzaj = rodzaj;
        this.zrodloId = zrodloId;
        this.zrodloIdn = zrodloIdn;
        this.zrodloNazwa = zrodloNazwa;
        this.zrodloPKoszt = zrodloPKoszt;
        this.zrodloRKoszt = zrodloRKoszt;
        this.zrodloRezKoszt = zrodloRezKoszt;
    }

    public static Source find(long id) throws EdmException {
        return Finder.find(Source.class, id);
    }

    public static Source findByIdm(String code) {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(Source.class);
            criteria.add(Restrictions.eq("zrodloIdn", code));
            return (Source) criteria.setMaxResults(1).uniqueResult();
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getBdBudzetPozId() {
        return bdBudzetPozId;
    }

    public void setBdBudzetPozId(long bdBudzetPozId) {
        this.bdBudzetPozId = bdBudzetPozId;
    }

    public int getRodzaj() {
        return rodzaj;
    }

    public void setRodzaj(int rodzaj) {
        this.rodzaj = rodzaj;
    }

    public long getZrodloId() {
        return zrodloId;
    }

    public void setZrodloId(long zrodloId) {
        this.zrodloId = zrodloId;
    }

    public String getZrodloIdn() {
        return zrodloIdn;
    }

    public void setZrodloIdn(String zrodloIdn) {
        this.zrodloIdn = zrodloIdn;
    }

    public String getZrodloNazwa() {
        return zrodloNazwa;
    }

    public void setZrodloNazwa(String zrodloNazwa) {
        this.zrodloNazwa = zrodloNazwa;
    }

    public BigDecimal getZrodloPKoszt() {
        return zrodloPKoszt;
    }

    public void setZrodloPKoszt(BigDecimal zrodloPKoszt) {
        this.zrodloPKoszt = zrodloPKoszt;
    }

    public BigDecimal getZrodloRKoszt() {
        return zrodloRKoszt;
    }

    public void setZrodloRKoszt(BigDecimal zrodloRKoszt) {
        this.zrodloRKoszt = zrodloRKoszt;
    }

    public BigDecimal getZrodloRezKoszt() {
        return zrodloRezKoszt;
    }

    public void setZrodloRezKoszt(BigDecimal zrodloRezKoszt) {
        this.zrodloRezKoszt = zrodloRezKoszt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Source)) return false;

        Source source = (Source) o;

        if (bdBudzetPozId != source.bdBudzetPozId) return false;
        if (id != source.id) return false;
        if (rodzaj != source.rodzaj) return false;
        if (zrodloId != source.zrodloId) return false;
        if (zrodloIdn != null ? !zrodloIdn.equals(source.zrodloIdn) : source.zrodloIdn != null) return false;
        if (zrodloNazwa != null ? !zrodloNazwa.equals(source.zrodloNazwa) : source.zrodloNazwa != null) return false;
        if (zrodloPKoszt != null ? !zrodloPKoszt.equals(source.zrodloPKoszt) : source.zrodloPKoszt != null) return false;
        if (zrodloRKoszt != null ? !zrodloRKoszt.equals(source.zrodloRKoszt) : source.zrodloRKoszt != null) return false;
        if (zrodloRezKoszt != null ? !zrodloRezKoszt.equals(source.zrodloRezKoszt) : source.zrodloRezKoszt != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (bdBudzetPozId ^ (bdBudzetPozId >>> 32));
        result = 31 * result + rodzaj;
        result = 31 * result + (int) (zrodloId ^ (zrodloId >>> 32));
        result = 31 * result + (zrodloIdn != null ? zrodloIdn.hashCode() : 0);
        result = 31 * result + (zrodloNazwa != null ? zrodloNazwa.hashCode() : 0);
        result = 31 * result + (zrodloPKoszt != null ? zrodloPKoszt.hashCode() : 0);
        result = 31 * result + (zrodloRKoszt != null ? zrodloRKoszt.hashCode() : 0);
        result = 31 * result + (zrodloRezKoszt != null ? zrodloRezKoszt.hashCode() : 0);
        return result;
    }

    public void setAllFieldsFromServiceObject(DictionaryServicesBudgetViewsStub.Source item) {
        this.setBdBudzetPozId(item.getBd_budzet_poz_id());
        this.setRodzaj(item.getRodzaj());
        this.setZrodloId(item.getZrodlo_id());
        this.setZrodloIdn(item.getZrodlo_idn());
        this.setZrodloNazwa(item.getZrodlo_nazwa());
        this.setZrodloPKoszt(item.getZrodlo_p_koszt());
        this.setZrodloRKoszt(item.getZrodlo_r_koszt());
        this.setZrodloRezKoszt(item.getZrodlo_rez_koszt());
    }

    public void save() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }
}
