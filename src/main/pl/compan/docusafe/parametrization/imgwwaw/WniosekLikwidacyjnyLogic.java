package pl.compan.docusafe.parametrization.imgwwaw;

import com.google.common.collect.Maps;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.joda.time.DateTime;
import org.joda.time.JodaTimePermission;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.exports.ExportedDocument;
import pl.compan.docusafe.core.exports.FixedAssetLiquidationDocument;
import pl.compan.docusafe.core.exports.FixedAssetPosition;
import pl.compan.docusafe.core.exports.FixedAssetPositionDetail;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.util.*;

public class WniosekLikwidacyjnyLogic extends AbstractDocumentLogic {

//    private static final String JPDL_ACCEPTATION_CN_LIQUIDATION_COMMISSION = "liquidation-commission";
    private static final String JPDL_ACCEPTATION_CN_TECHNICAL_EXPERTISE = "technical-expertise";
    private static final String STATUS = "STATUS";
    private static final String STATUS_EKSPERTYZA_TECHNICZNA = "EKSPERTYZA_TECHNICZNA";
    private static final String FIELD_EKSPERTYZA_OS_ODP = "EKSPERTYZA_OS_ODP";

    protected static Logger log = LoggerFactory.getLogger(WniosekLikwidacyjnyLogic.class);
    private static WniosekLikwidacyjnyLogic instance;

    public static synchronized WniosekLikwidacyjnyLogic getInstance() {
        if (instance == null)
            instance = new WniosekLikwidacyjnyLogic();
        return instance;
    }

//    @Override
//    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
//        //TODO
//        Map<String, Object> toReload = Maps.newHashMap();
//        for (Field f : fm.getFields())
//            if (f.getDefaultValue() != null)
//                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
//
//        DSUser user = DSApi.context().getDSUser();
//        //toReload.put(OS_WNIOSKUJACA_CN, user.getId());
//        //        if (user.getDivisionsWithoutGroup().length > 0)
//        //            toReload.put("WORKER_DIVISION", fm.getField("WORKER_DIVISION").getEnumItemByCn(user.getDivisionsWithoutGroupPosition()[0].getGuid()).getId());
//        fm.reloadValues(toReload);
//    }

//    public int getPermissionPolicyMode() {
//        return PermissionManager.NORMAL_POLICY;
//    }

    @Override
    public boolean searchCheckPermissions(Map<String, Object> values) {
        return false;
    }

    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
        boolean assigned = false;
//        if (acceptationCn.equals(JPDL_ACCEPTATION_CN_LIQUIDATION_COMMISSION)) {
//            String assignee = (String) openExecution.getVariable(JPDL_ACCEPTATION_CN_LIQUIDATION_COMMISSION);
//            ProfilePrepareProcessListener.AssigneeType assigneeType = ProfilePrepareProcessListener.getAssigneeType(assignee);
//            String assigneeValue = ProfilePrepareProcessListener.getAssigneeValue(assignee);
//            switch(assigneeType){
//                case DIVISION_GUID:
//                    assignToDivision(doc,assignable,openExecution,assigneeValue);
//                    break;
//                case USER_NAME:
//                    assignToUser(doc,assignable,openExecution,assigneeValue);
//                    break;
//            }
////            if (dicId != null) {
////                try {
////                    DSUser user = DSUser.findById(dicId);
////                    assignable.addCandidateUser(user.getName());
////                    AssigneeHandler.addToHistory(openExecution, doc, user.getName(), null);
////                    assigned = true;
////
////                } catch (EdmException e) {
////                    log.error(e.getMessage(), e);
////                }
////            } else {
////                log.error("A found value by id-name [" + JPDL_ACCEPTATION_CN_LIQUIDATION_COMMISSION + "] is null.");
////            }
//        }
        return assigned;
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
        document.setFolder(folder);

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        document.setTitle("" + (String) fm.getValue(fm.getMainFieldCn()));
        document.setDescription("" + (String) fm.getValue(fm.getMainFieldCn()));
    }

//    public void onStartProcess(OfficeDocument document, ActionEvent event) {
//        try {
//            Map<String, Object> map = Maps.newHashMap();
//            map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
//            //map.put(ASSIGN_DIVISION_GUID_PARAM, DSDivision.ROOT_GUID);
//            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
//        } catch (Exception e) {
//            log.error(e.getMessage(), e);
//        }
//    }

    @Override
    public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException {
        if (doc != null) {
            if (JPDL_ACCEPTATION_CN_TECHNICAL_EXPERTISE.equalsIgnoreCase(acceptationCn)) {
                FieldsManager fm = doc.getFieldsManager();
                if (fm.getEnumItemCn(STATUS).equals(STATUS_EKSPERTYZA_TECHNICZNA)) {
                    DSUser loggedInUser = DSUser.getLoggedInUser();
                    if (loggedInUser != null) {
                        doc.getDocumentKind().setOnly(doc.getId(), Collections.singletonMap(FIELD_EKSPERTYZA_OS_ODP, loggedInUser.getId()), false);
                    }
                }
            }
        }
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {
        Set<PermissionBean> perms = new HashSet<PermissionBean>();

//        String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
//        String documentKindName = document.getDocumentKind().getName();
//        perms.add(new PermissionBean(ObjectPermission.CREATE, documentKindCn + "_DOCUMENT_CREATE", ObjectPermission.GROUP, documentKindName + " - tworzenie"));

        Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
        perms.addAll(documentPermissions);
        this.setUpPermission(document, perms);
    }

//    @Override
//    public boolean canChangeDockind(Document document) throws EdmException {
//        return false;
//    }

    @Override
    public ExportedDocument buildExportDocument(OfficeDocument doc) throws EdmException {
        FixedAssetLiquidationDocument exported = new FixedAssetLiquidationDocument();

        FieldsManager fm = doc.getFieldsManager();
        exported.setOperation(new Date());
        exported.setValidity((Date) fm.getKey("DATA_OBOWIAZYWANIA"));
        exported.setDepartmentCode(fm.getEnumItemCn("ODDZIAL"));
        exported.setOperationTypeCode(Docusafe.getAdditionPropertyOrDefault("export.fixed_asset.operation_type", "W"));
        //exported.setOperationTypeCode(fm.getEnumItemCn("TYP_OPERACJI"));
        FixedAssetPosition position;
        exported.addPosition(position = new FixedAssetPosition());
        position.setLiquidationCode(fm.getEnumItemCn("POWOD"));
        position.setCardIdentifier(fm.getEnumItem("NR_INWENTARZOWY") != null ? Long.valueOf(fm.getEnumItem("NR_INWENTARZOWY").getCentrum()) : 1);

        position.addPosition(new FixedAssetPositionDetail().setRatio(BigDecimal.ONE).setRegistrationSystemCode(Docusafe.getAdditionPropertyOrDefault("export.fixed_asset.registration_amortization_code1", "BILANSOWY")));
        position.addPosition(new FixedAssetPositionDetail().setRatio(BigDecimal.ONE).setRegistrationSystemCode(Docusafe.getAdditionPropertyOrDefault("export.fixed_asset.registration_amortization_code2", "PODATKOWY")));

        exported.setDescription(ImgwDocumentLogic.buildAdditionDescription(getAdditionFieldsDescription(doc, fm)));

        return exported;
    }

    private Map<String,String> getAdditionFieldsDescription(OfficeDocument doc, FieldsManager fm) throws EdmException {
        Map<String,String> additionFields = Maps.newLinkedHashMap();
        additionFields.put("Nr KO",doc.getOfficeNumber() != null ? doc.getOfficeNumber().toString() : "brak");
        additionFields.put("Opis przyczyny", fm.getStringValue("OPIS_PRZYCZYNY"));
        additionFields.put("Numer �rodka", fm.getStringValue("NUMER_SRODKA"));
        additionFields.put("Osoba wykonuj�ca ekspertyz�", fm.getStringValue("EKSPERTYZA_OS_ODP"));
        additionFields.put("Ekspertyza", fm.getStringValue("EKSPERTYZA_OPIS"));

        return additionFields;
    }
}