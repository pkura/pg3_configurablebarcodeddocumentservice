package pl.compan.docusafe.parametrization.imgwwaw;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.primitives.Ints;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.exports.AttachmentInfo;
import pl.compan.docusafe.core.exports.BudgetItem;
import pl.compan.docusafe.core.exports.ExportedDocument;
import pl.compan.docusafe.core.exports.FundSource;
import pl.compan.docusafe.core.exports.ReservationDocument;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.Source;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

public class WniosekZamPubLogic extends ImgwDocumentLogic {

    private final static Logger log = LoggerFactory.getLogger(WniosekZamPubLogic.class);
    public static final String SZACUNKOWA_WARTOSC_CN = "SZACUNKOWA_WARTOSC";
    public static final String KOMISJA_PRZETARGOWA_CN = "KOMISJA_PRZETARGOWA";
    public static final String DH_STATUS_PREFIX = "dh_";
    public static final String BASIC_PROCESS_NAME = "WniosekZamPub";
    public static final String DH_PROCESS_NAME = "WniosekZamPubDzialHandlowy";
    public static final String IMGW_PRODUKT_VIEW = "dsg_imgw_produkt";
    public static final String VAT_RATE_VIEW = "dsg_imgw_vat_rates";

    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) throws EdmException {
        boolean assigned = super.assignee(doc, assignable, openExecution, acceptationCn, fieldCnValue);
        return assigned;
    }

    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues, Long documentId) {
        Map<String, Object> result = new HashMap<String, Object>();
        if (dictionaryName.equals(ZRODLO_FINANSOWANIA_CN)) {
            try {
                setBudgetItemsRefValues(dictionaryName, values, result);
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
            }
        }
        if (dictionaryName.equals(SZACUNKOWA_WARTOSC_CN)) {
            try {
                if (DwrUtils.isNotNull(values, SZACUNKOWA_WARTOSC_CN + "_NETTO")) {
                    BigDecimal netto = values.get(SZACUNKOWA_WARTOSC_CN + "_NETTO").getMoneyData().setScale(2, RoundingMode.HALF_DOWN);
                    String kurs_euro = Docusafe.getAdditionProperty("kurs_euro") != null ? Docusafe.getAdditionProperty("kurs_euro") : "4.2125";
                    BigDecimal kurs = new BigDecimal(kurs_euro).setScale(2, BigDecimal.ROUND_HALF_DOWN);
                    BigDecimal nettoEuro = netto.divide(kurs, 2, RoundingMode.HALF_DOWN);
                    result.put(SZACUNKOWA_WARTOSC_CN + "_NETTO_EURO", nettoEuro);
                }
                if (DwrUtils.isNotNull(values, SZACUNKOWA_WARTOSC_CN + "_BRUTTO")) {
                    BigDecimal brutto = values.get(SZACUNKOWA_WARTOSC_CN + "_BRUTTO").getMoneyData().setScale(2, RoundingMode.HALF_DOWN);
                    String kurs_euro = Docusafe.getAdditionProperty("kurs_euro") != null ? Docusafe.getAdditionProperty("kurs_euro") : "4.2125";
                    BigDecimal kurs = new BigDecimal(kurs_euro).setScale(2, BigDecimal.ROUND_HALF_DOWN);
                    BigDecimal bruttoEuro = brutto.divide(kurs, 2, RoundingMode.HALF_DOWN);
                    result.put(SZACUNKOWA_WARTOSC_CN + "_BRUTTO_EURO", bruttoEuro);
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        if (dictionaryName.equals(KOMISJA_PRZETARGOWA_CN)) {
            try {
                String selectedId;
                HashMap<String, String> mapa = new HashMap<String, String>();

                if (DwrUtils.isNotNull(values, KOMISJA_PRZETARGOWA_CN + "_USERID") && !DwrUtils.isNotNull(values, KOMISJA_PRZETARGOWA_CN + "_DIVISION")) {

                    selectedId = values.get(KOMISJA_PRZETARGOWA_CN + "_USERID").getEnumValuesData().getSelectedId();
                    DSUser user = DSUser.findById(Long.valueOf(selectedId));

                    mapa.put("", "--wybierz--");
                    for (DSDivision division : user.getDivisionsWithoutGroup()) {
                        mapa.put(division.getId().toString(), division.getName());
                    }
                    ArrayList<String> sel = new ArrayList<String>();
                    sel.add("");
                    EnumValues ev = new EnumValues(mapa, sel);

                    result.put(KOMISJA_PRZETARGOWA_CN + "_DIVISION", ev);
                } else if (DwrUtils.isNotNull(values, KOMISJA_PRZETARGOWA_CN + "_DIVISION") && !DwrUtils.isNotNull(values, KOMISJA_PRZETARGOWA_CN + "_USERID")) {

                    selectedId = values.get(KOMISJA_PRZETARGOWA_CN + "_DIVISION").getEnumValuesData().getSelectedId();
                    DSDivision division = DSDivision.findById(Integer.valueOf(selectedId));

                    mapa.put("", "--wybierz--");
                    for (DSUser user : division.getUsers(true)) {
                        mapa.put(user.getId().toString(), user.asFirstnameLastname());
                    }
                    ArrayList<String> sel = new ArrayList<String>();
                    sel.add("");
                    EnumValues ev = new EnumValues(mapa, sel);
                    result.put(KOMISJA_PRZETARGOWA_CN + "_USERID", ev);
                }
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
            }
        }
        return result;
    }

    @Override
    public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        return null;
    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();
        for (Field f : fm.getFields()) {
            if (f.getDefaultValue() != null) {
                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
            }
        }
        if (DSApi.context().getDSUser().getDivisionsWithoutGroup().length > 0) {
            String sender = "u:" + DSApi.context().getDSUser().getName();
            sender += (";d:" + DSApi.context().getDSUser().getDivisionsWithoutGroup()[0].getGuid());
            toReload.put("SENDER_HERE", sender);
        }
        toReload.put("SYMBOL_KOMORKI", DSApi.context().getDSUser().getDivisionsWithoutGroup()[0].getCode());

        log.info("toReload = {}", toReload);
        fm.reloadValues(toReload);
    }

    public String generateNrWniosku(OfficeDocument document) throws EdmException {
        StringBuilder sb = new StringBuilder("NJ/");
        FieldsManager fm = document.getFieldsManager();
        String lp = "";
        try {
            PreparedStatement ps = DSApi.context().prepareStatement(
                    "select count(*) from dsg_wniosek_zampub n join ds_document doc on n.document_id = doc.ID where YEAR(CTIME) = ?");
            Calendar cal = Calendar.getInstance();
            Integer calYear = cal.get(Calendar.YEAR);
            ps.setInt(1, calYear);
            ResultSet rs = ps.executeQuery();
            boolean hasNext = rs.next();
            lp = hasNext ? String.valueOf(rs.getInt(1)) : "1";
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new EdmException("B��d przy generowaniu numeru wniosku o zam�wienie publiczne", e);
        }
        /*
        Jeszcze nie ustalone z klientem, kiedy ma si� generowa� numer wniosku, wi�c tryb ustawiam jak "TRYB",
        bo to pole wype�niane jest na p�niejszym etapie procesu.
         */
        String tryb = "TRYB";
        // Rodzaj zam�wienia
        String rodzaj = DataBaseEnumField.getEnumItemForTable("dsg_rodzaj_zamowienia", (Integer) fm.getKey("RODZAJ_ZAMOWIENIA")).getCn();
        // Kod kom�rki zmawiaj�cej
        String kk = (String) fm.getKey("SYMBOL_KOMORKI");
        // �r�d�o finansowania
        String zf = DataBaseEnumField.getEnumItemForTable("dsg_zrodlo_finansowania", fm.getIntegerKey(ZRODLO_FINANSOWANIA_CN)).getCn();
        //Rok YY
        String yy = DateUtils.formatYear(Calendar.getInstance().getTime()).substring(2);

        return sb.append(lp).append("/")
                .append(tryb).append("/")
                .append(rodzaj).append("/")
                .append(kk).append("/")
                .append(zf).append("/")
                .append(yy)
                .toString();
    }

    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
        try {
            Map<String, Object> map = Maps.newHashMap();
            map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
            map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
            java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

            DSUser author = DSApi.context().getDSUser();
            String user = author.getName();
            String fullName = author.asFirstnameLastname();

            Map<String, Object> fieldValues = new HashMap<String, Object>();
            fieldValues.put("NR_WNIOSKU", generateNrWniosku(document));
            document.getDocumentKind().setOnly(document.getId(), fieldValues);

            perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

            Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
            perms.addAll(documentPermissions);
            this.setUpPermission(document, perms);

            if (AvailabilityManager.isAvailable("addToWatch")) {
                DSApi.context().watch(URN.create(document));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new EdmException(e.getMessage());
        }
    }

    @Override
    public ExportedDocument buildExportDocument(OfficeDocument doc) throws EdmException {
        FieldsManager fm = doc.getFieldsManager();

        ReservationDocument exported = new ReservationDocument();

        exported.setReservationStateFromId(1);

        //exported.setReservationNumber(fm.getStringKey(ERP_DOCUMENT_IDM));

        //exported.getHeaderBudget().setBudgetDirectCode(Docusafe.getAdditionPropertyOrDefault("export.dokument_rezerwacyjny.idm_budzetu", "IMGW-2014\\00001")/* IDM BUD?ETU, OKRESU BUD?ETOWEGO, USTAWI? NA STA?E WG SERWIS?W ERPOWYCH. DOCELOWO TRZEBA WYRZUCI? DO NAG??WKA*/);
        exported.getHeaderBudget().setBudgetDirectId(Long.valueOf(Docusafe.getAdditionPropertyOrDefault("export.dokument_rezerwacyjny.id_budzetu", "10")));

        exported.setBudgetReservationTypeCode(Docusafe.getAdditionPropertyOrDefault("export.wniosek_rezerwacyjny", "WMR"));
        exported.setReservationMethod(ReservationDocument.ReservationMethod.AUTO);

        List<BudgetItem> positionItems = Lists.newArrayList();
        exported.setPostionItems(positionItems);

        List<Map<String, Object>> positions = (List) fm.getValue(ZRODLO_FINANSOWANIA_CN);
        BudgetItem bItem;
        int nrp = 1;
        for (Map<String, Object> position : positions) {
            positionItems.add(bItem = new BudgetItem());
            bItem.setItemNo(nrp);
            bItem.setQuantity(BigDecimal.ONE);

            bItem.setNetAmount((BigDecimal) position.get(ZRODLO_FINANSOWANIA_CN + "_"  + "AMOUNT"));

            if (position.get(ZRODLO_FINANSOWANIA_CN + "_"  + "PRODUCT") != null && !Strings.isNullOrEmpty(((EnumValues) position.get(ZRODLO_FINANSOWANIA_CN + "_"  + "PRODUCT")).getSelectedId())) {
                EnumItem costKindEnum = DataBaseEnumField.getEnumItemForTable(IMGW_PRODUKT_VIEW, Ints.tryParse(((EnumValues) position.get(ZRODLO_FINANSOWANIA_CN + "_"  + "PRODUCT")).getSelectedId()));
                bItem.setCostKindCode(costKindEnum.getCn());
                exported.setCostKindCodeAllowed(true);
            }

            bItem.setBudgetDirectId(exported.getHeaderBudget().getBudgetDirectId());

            bItem.setBudgetKoId(position.get(ZRODLO_FINANSOWANIA_CN + "_" + "BUDGET") != null && !Strings.isNullOrEmpty(((EnumValues) position.get(ZRODLO_FINANSOWANIA_CN + "_" + "BUDGET")).getSelectedId()) ? Long.valueOf(((EnumValues) position.get(ZRODLO_FINANSOWANIA_CN + "_" + "BUDGET")).getSelectedId()) : null);
            bItem.setBudgetKindId(position.get(ZRODLO_FINANSOWANIA_CN + "_" + "POSITION") != null && !Strings.isNullOrEmpty(((EnumValues) position.get(ZRODLO_FINANSOWANIA_CN + "_" + "POSITION")).getSelectedId()) ? Long.valueOf(((EnumValues) position.get(ZRODLO_FINANSOWANIA_CN + "_" + "POSITION")).getSelectedId()) : null);

            if (exported.getHeaderBudget().getBudgetKoId() == null) {
                exported.getHeaderBudget().setBudgetKoId(bItem.getBudgetKoId());
            }

            EnumValues fsEnum = (EnumValues) position.get(ZRODLO_FINANSOWANIA_CN + "_" + "SOURCE");

            if (fsEnum != null && !Strings.isNullOrEmpty(fsEnum.getSelectedId())) {
                FundSource fItem = new FundSource();
                fItem.setFundPercent(new BigDecimal(100));
                fItem.setCode(Source.find(Long.valueOf(fsEnum.getSelectedId())).getZrodloIdn());
                bItem.setFundSources(Lists.newArrayList(fItem));
            }

            if (position.get(ZRODLO_FINANSOWANIA_CN + "_" + "STAWKA_VAT") != null && !Strings.isNullOrEmpty(((EnumValues) position.get(ZRODLO_FINANSOWANIA_CN + "_" + "STAWKA_VAT")).getSelectedId())) {
                EnumItem vatRateEnum = DataBaseEnumField.getEnumItemForTable(VAT_RATE_VIEW, Ints.tryParse(((EnumValues) position.get(ZRODLO_FINANSOWANIA_CN + "_" + "STAWKA_VAT")).getSelectedId()));
                bItem.setVatRateCode(vatRateEnum.getCn());
            }

            nrp++;
        }

        exported.setDescription(buildAdditionDescription(getAdditionFields(doc, fm)));

        String systemPath;

        if (Docusafe.getAdditionProperty("erp.faktura.link_do_skanu") != null) {
            systemPath = Docusafe.getAdditionProperty("erp.faktura.link_do_skanu");
        } else {
            systemPath = Docusafe.getBaseUrl();
        }

        for (Attachment zal : doc.getAttachments()) {
            AttachmentInfo info = new AttachmentInfo();

            String path = systemPath + "/repository/view-attachment-revision.do?id=" + zal.getMostRecentRevision().getId();

            info.setName(zal.getTitle());
            info.setUrl(path);

            exported.addAttachementInfo(info);
        }
        return exported;
    }

    Map<String,String> getAdditionFields(OfficeDocument doc, FieldsManager fm) throws EdmException {
        Map<String,String> additionFields = Maps.newLinkedHashMap();
        additionFields.put("Nr KO",doc.getOfficeNumber() != null ? doc.getOfficeNumber().toString() : "brak");
        additionFields.put("Rodzaj zam�wienia", fm.getStringValue("RODZAJ_ZAMOWIENIA"));
        additionFields.put("Przedmiot zam�wienia", fm.getStringValue("PRZEDMIOT_ZAMOWIENIA"));
        additionFields.put("Pozycja planu zam�wie� publicznych", fm.getStringValue("POZYCJA_PLANU"));
        additionFields.put("Numer wniosku", fm.getStringValue("NR_WNIOSKU"));
        additionFields.put("Symbol kom�rki", fm.getStringValue("SYMBOL_KOMORKI"));
        additionFields.put("Tryb zam�wienia", fm.getStringValue("TRYB_ZAMOWIENIA"));
        return  additionFields;
    }
}
