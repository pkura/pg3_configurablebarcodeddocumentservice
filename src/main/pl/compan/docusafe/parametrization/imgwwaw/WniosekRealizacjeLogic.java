package pl.compan.docusafe.parametrization.imgwwaw;

import com.google.common.collect.Maps;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.ValueNotFoundException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DocumentField;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Map;

public class WniosekRealizacjeLogic extends AbstractDocumentLogic{
    protected static Logger log = LoggerFactory.getLogger(WniosekRealizacjeLogic.class);
    private static WniosekRealizacjeLogic instance;
    public static final String WNIOSEK_O_REALIZACJE = "WNIOSEK_O_REALIZACJE";
    public static final String SYMBOL_KOMORKI_CN = "SYMBOL_KOMORKI";

    public static final String SENDER_HERE = "SENDER_HERE";
    private static final String ZAPOTRZEBOWANIE = "ZAPOTRZEBOWANIE";
    private static final String REALIZATOR = "REALIZATOR";

    public static synchronized WniosekRealizacjeLogic getInstance() {
        if (instance == null)
            instance = new WniosekRealizacjeLogic();
        return instance;
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
        document.setFolder(folder);

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        document.setTitle("" + (String) fm.getValue(fm.getMainFieldCn()));
        document.setDescription("" + (String) fm.getValue(fm.getMainFieldCn()));
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {

    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();
        if (WNIOSEK_O_REALIZACJE.equalsIgnoreCase(fm.getDocumentKind().getCn())) {
            Long inicjatorId = DSApi.context().getDSUser().getId();
            DSDivision[] userDivs = DSUser.findById(inicjatorId).getDivisionsWithoutGroup();
            String symbolKomorki = "";
            if(userDivs.length > 0){
                symbolKomorki = userDivs[0].getCode();
            }
            toReload.put(SYMBOL_KOMORKI_CN, symbolKomorki);
        }
        if (WNIOSEK_O_REALIZACJE.equalsIgnoreCase(fm.getDocumentKind().getCn()) && DSApi.context().getDSUser().getDivisionsWithoutGroup().length>0) {
            String sender = "u:"+DSApi.context().getDSUser().getName();
            sender+=(";d:"+DSApi.context().getDSUser().getDivisionsWithoutGroup()[0].getGuid());
            toReload.put(SENDER_HERE, sender);
        }
        fm.reloadValues(toReload);
    }

    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) throws EdmException{
        boolean assigned = false;
        if(acceptationCn.equalsIgnoreCase(REALIZATOR)){
            FieldsManager fm = doc.getFieldsManager();
            try{
                Map documentFieldValues = (Map) fm.getValue(ZAPOTRZEBOWANIE);
                String zapotrzebowanieId = (String)documentFieldValues.get("id");
                Document zapotrzebowanieDoc = Document.find(Long.parseLong(zapotrzebowanieId),Boolean.FALSE);
                if(zapotrzebowanieDoc instanceof OfficeDocument){
                    OfficeDocument officeDocument = (OfficeDocument) zapotrzebowanieDoc;
                    Recipient recipient = officeDocument.getRecipients().get(0);

                    if(recipient == null){
                        throw new UserNotFoundException("Nie znaleziono realizatora do zapotrzebowania");
                    }
                    DSUser user = DSUser.findByFirstnameLastname(recipient.getFirstname(), recipient.getLastname());
                    Long realizatorId = user.getId();
                    assignUser(doc,assignable,openExecution,realizatorId);
                }else{
                    throw new ValueNotFoundException("B��dny typ dokumentu zapotrzebowania");
                }
            } catch (UserNotFoundException e) {
                throw new ValueNotFoundException(e.getMessage());
            } catch (DocumentNotFoundException e) {
                throw new ValueNotFoundException(e.getMessage());
            } catch (EdmException e) {
                throw e;
            }
        }

        return assigned;
    }

    /**
     * Przypisuje u�ytkownika do zadania w procesie
     *
     * @param doc
     * @param assignable
     * @param openExecution
     * @param userId - id usera do kt�rego ma by� przypisane zadanie
     * @return 'true' je�li znaleziono u�ytkownika
     * @throws EdmException
     * @throws pl.compan.docusafe.core.users.UserNotFoundException
     */
    private boolean assignUser(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, Long userId)
            throws EdmException, UserNotFoundException {
        boolean assigned = false;
        String userName = DSUser.findById(userId).getName();
        assignable.addCandidateUser(userName);
        assigned = true;
        AssigneeHandler.addToHistory(openExecution, doc, userName, null);
        return assigned;
    }
}