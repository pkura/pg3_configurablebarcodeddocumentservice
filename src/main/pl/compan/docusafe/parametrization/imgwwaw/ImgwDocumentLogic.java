package pl.compan.docusafe.parametrization.imgwwaw;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.pvm.internal.task.TaskImpl;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.AssignUtils;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.users.*;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.ZrodlaFinansowaniaDict;
import pl.compan.docusafe.parametrization.imgwwaw.jbpm.PurchaseDocumentTypeDecision;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: brs
 * Date: 27.11.13
 * Time: 12:08
 * To change this template use File | Settings | File Templates.
 */
public class ImgwDocumentLogic extends AbstractDocumentLogic {

    public static final String ZRODLO_FINANSOWANIA_CN = "ZRODLO_FINANSOWANIA";
    private final static Logger log = LoggerFactory.getLogger(ImgwDocumentLogic.class);

    public static final String KOORDYNATOR_KOMORKI_ACCEPTANCE = "koordynator_komorki";
    public final String KIEROWNIK_KOMORKI_ACCEPTANCE = "kierownik_komorki";
    public static final String DYREKTOR_PIONU_ACCEPTANCE = "dyrektor_pionu";
    public static final String KIEROWNIK_PROJEKTU_ACCEPTANCE = "kierownik_projektu";
    public static final String DICTIONARY_ID_VARIABLE = "dictionaryId";
    public static final String DIVISION_CODE__VARIABLE = "division";

    public static final String BUDGET_VIEW_TABLENAME = "dsg_imgw_budget";
    public static final String BUDGET_POSITIONS_TABLENAME = "dsg_imgw_budget_position";
    public static final String SOURCES_TABLENAME = "dsg_imgw_sources";
    public static final String CONTRACT_ENUM_TABLE = "dsg_imgw_view_contract";

    public static final String AKCEPTACJA_OZP_ACCEPTANCE = "akceptacja_ozp";
    public static final String OPIEKUN_PROJEKTU_ACCEPTANCE = "opiekun_projektu";

    public static final String OZP_K_ACCEPTANCE = Docusafe.getAdditionProperty("ozp_k_acceptance") == null ? "ozp_k_acceptance" : Docusafe.getAdditionProperty("ozp_k_acceptance");
    public static final String OZP_G_ACCEPTANCE = Docusafe.getAdditionProperty("ozp_g_acceptance") == null ? "ozp_g_acceptance" : Docusafe.getAdditionProperty("ozp_g_acceptance");
    public static final String OZP_W_ACCEPTANCE = Docusafe.getAdditionProperty("ozp_w_acceptance") == null ? "ozp_w_acceptance" : Docusafe.getAdditionProperty("ozp_w_acceptance");
    public static final String OZP_O_ACCEPTANCE = Docusafe.getAdditionProperty("ozp_o_acceptance") == null ? "ozp_o_acceptance" : Docusafe.getAdditionProperty("ozp_o_acceptance");
    public static final String OZP_T_ACCEPTANCE = Docusafe.getAdditionProperty("ozp_t_acceptance") == null ? "ozp_t_acceptance" : Docusafe.getAdditionProperty("ozp_t_acceptance");

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        Folder folder = Folder.getRootFolder();
        String doctitle = document.getDocumentKind().getName();
        folder = folder.createSubfolderIfNotPresent(doctitle);

        folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(document.getCtime()));

        document.setFolder(folder);
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {
        Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
        String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
        String documentKindName = document.getDocumentKind().getName();

        perms.add(new PermissionBean(ObjectPermission.READ, documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, documentKindName + " - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, documentKindName + " zalacznik - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, documentKindName + " - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, documentKindName + " zalacznik - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, documentKindName + " - usuwanie"));
        Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
        perms.addAll(documentPermissions);
        this.setUpPermission(document, perms);
    }

    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) throws EdmException {
        boolean assigned = false;
        try {
            FieldsManager fm = new FieldsManager(doc.getId(), doc.getDocumentKind());
            if (KIEROWNIK_KOMORKI_ACCEPTANCE.equals(acceptationCn)) {
                String divisionCode = (String) openExecution.getVariable(DIVISION_CODE__VARIABLE);
                Long dictionaryId = (Long) openExecution.getVariable(DICTIONARY_ID_VARIABLE);
//                String typFaktury = fm.getEnumItemCn(FakturaZakupowaLogic.TYP_FAKTURY_CN);
//                Boolean zwrot = fm.getBoolean(FakturaZakupowaLogic.ZWROT_KOSZTOW_CN);
//                boolean withoutBudgets = PurchaseDocumentTypeDecision.FZS.equals(typFaktury) || (zwrot != null && zwrot);

                String divGuid = null;
                // dla starego procesu
                if (divisionCode == null && dictionaryId != null) {
                    divGuid = DSDivision.findById(dictionaryId).getGuid();
                    // dla nowego procesu
                } else if (divisionCode != null) {
                    divGuid = DSDivision.findByCode(divisionCode).getGuid();
                }
                if (divGuid == null) {
                    throw new DivisionNotFoundException("Nie znaleziono poprawnej kom�rki do przypisania");
                }
                List<String> userNames = Lists.newArrayList();

                Integer kierownikKomorkiProfileId = 3;
                if (Docusafe.getAdditionProperty("profile.id.kierownik.komorki") != null) {
                    try {
                        kierownikKomorkiProfileId = Integer.valueOf(Docusafe.getAdditionProperty("profile.id.kierownik.komorki"));
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                }
                assigned = assignToUsersFromProfile(doc, assignable, openExecution, divGuid, userNames, kierownikKomorkiProfileId, false, false, 1);

            } else if (DYREKTOR_PIONU_ACCEPTANCE.equals(acceptationCn)) {
                String divisionCode = (String) openExecution.getVariable(DIVISION_CODE__VARIABLE);
                Long dictionaryId = (Long) openExecution.getVariable(DICTIONARY_ID_VARIABLE);
//                String typFaktury = fm.getEnumItemCn(FakturaZakupowaLogic.TYP_FAKTURY_CN);
//                Boolean zwrot = fm.getBoolean(FakturaZakupowaLogic.ZWROT_KOSZTOW_CN);
//                boolean withoutBudgets = PurchaseDocumentTypeDecision.FZS.equals(typFaktury) || (zwrot != null && zwrot);
                List<String> userNames = Lists.newArrayList();

                Integer dyrektorPionuProfileId = 4;
                if (Docusafe.getAdditionProperty("profile.id.dyrektor.pionu") != null) {
                    try {
                        dyrektorPionuProfileId = Integer.valueOf(Docusafe.getAdditionProperty("profile.id.dyrektor.pionu"));
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                }
                String divGuid = null;
                if (divisionCode == null && dictionaryId != null) {
                    divGuid = DSDivision.findById(dictionaryId).getGuid();
                } else if (divisionCode != null) {
                    divGuid = DSDivision.findByCode(divisionCode).getGuid();
                }
                if (divGuid == null) {
                    throw new DivisionNotFoundException("Nie znaleziono poprawnej kom�rki do przypisania");
                }

                assigned = assignToUsersFromProfile(doc, assignable, openExecution, divGuid, userNames, dyrektorPionuProfileId, true, false, 0);
            } else if (KIEROWNIK_PROJEKTU_ACCEPTANCE.equals(acceptationCn)) {
                Long dictionaryId = (Long) openExecution.getVariable(DICTIONARY_ID_VARIABLE);
                String typFaktury = fm.getEnumItemCn(FakturaZakupowaLogic.TYP_FAKTURY_CN);
                Boolean zwrot = fm.getBoolean(FakturaZakupowaLogic.ZWROT_KOSZTOW_CN);
                String divGuid = null;
                if (PurchaseDocumentTypeDecision.FZS.equals(typFaktury) || (zwrot != null && zwrot)) {
                    divGuid = DSDivision.findById(dictionaryId).getGuid();
                } else {
                    DSUser user = DSUser.findById(dictionaryId);
                    if (user != null) {
                        assignable.addCandidateUser(user.getName());
                        assigned = true;
                        AssigneeHandler.addToHistory(openExecution, doc, user.getName(), null);
                        return assigned;
                    }
                }
                if (divGuid == null) {
                    throw new DivisionNotFoundException("Nie znaleziono poprawnej kom�rki do przypisania");
                }
                List<String> userNames = Lists.newArrayList();
                Integer koordynatorKomorkiProfileId = 3;
                if (Docusafe.getAdditionProperty("profile.id.kierownik.komorki") != null) {
                    try {
                        koordynatorKomorkiProfileId = Integer.valueOf(Docusafe.getAdditionProperty("profile.id.kierownik.komorki"));
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                }
                assigned = assignToUsersFromProfile(doc, assignable, openExecution, divGuid, userNames, koordynatorKomorkiProfileId, false, false, 1);

//                ZrodlaFinansowaniaDict zrodlo = ZrodlaFinansowaniaDict.getInstance().find(dictionaryId);
//
//                //tymczasowe przypisanie do kierownik�w, kt�rych wybieramy na s�owniku
//                Integer kierownikId = zrodlo.getKierownik();
//                DSUser user = DSUser.findById(Long.valueOf(kierownikId));
//                if (user != null) {
//                    assignable.addCandidateUser(user.getName());
//                    assigned = true;
//                    AssigneeHandler.addToHistory(openExecution, doc, user.getName(), null);
//                    return assigned;
//                }
            } else if (OPIEKUN_PROJEKTU_ACCEPTANCE.equals(acceptationCn)) {
                Long dictionaryId = (Long) openExecution.getVariable(DICTIONARY_ID_VARIABLE);
                String typFaktury = fm.getEnumItemCn(FakturaZakupowaLogic.TYP_FAKTURY_CN);
                Boolean zwrot = fm.getBoolean(FakturaZakupowaLogic.ZWROT_KOSZTOW_CN);
                if (PurchaseDocumentTypeDecision.FZS.equals(typFaktury) || (zwrot != null && zwrot)) {
                    List<String> users = new ArrayList<String>();
                    List<String> divisions = new ArrayList<String>();
                    AssignUtils.getAssigneesFromAcceptanceTree(users, divisions, FakturaZakupowaLogic.DPK_ACCEPTANCE);
                    for (String user : users) {
                        assignable.addCandidateUser(user);
                        AssigneeHandler.addToHistory(openExecution, doc, user, null);
                        assigned = true;
                    }
                    for (String division : divisions) {
                        assignable.addCandidateGroup(division);
                        AssigneeHandler.addToHistory(openExecution, doc, null, division);
                        assigned = true;
                    }
                    if (!assigned) {
                        throw new EdmException("Nie znaleziono nikogo do przypisania na drzewie akceptacji " +
                                "dla akceptacji: '" + FakturaZakupowaLogic.DPK_ACCEPTANCE + "'");
                    }
                } else {
                    DSUser user = DSUser.findById(dictionaryId);
                    if (user != null) {
                        assignable.addCandidateUser(user.getName());
                        assigned = true;
                        AssigneeHandler.addToHistory(openExecution, doc, user.getName(), null);
                        return assigned;
                    }
                }

//                ZrodlaFinansowaniaDict zrodlo = ZrodlaFinansowaniaDict.getInstance().find(dictionaryId);
//
//                Integer contractId = zrodlo.getContract();
//                if (contractId != null) {
//                    Kontrakt kontrakt = Kontrakt.find(contractId);
//                    Integer opiekunId = kontrakt.getOpiekunId();
//                    DSUser user = null;
//                    try {
//                        user = DSUser.findById(Long.valueOf(opiekunId));
//                    } catch (Exception e) {
//                        throw new ValueNotFoundException("Nie znaleziono opiekuna projektu dla kontraktu " + kontrakt.getNazwa());
//                    }
//                    if (user != null) {
//                        assignable.addCandidateUser(user.getName());
//                        assigned = true;
//                        AssigneeHandler.addToHistory(openExecution, doc, user.getName(), null);
//                        return assigned;
//                    }
//                }
            } else if (KOORDYNATOR_KOMORKI_ACCEPTANCE.equals(acceptationCn)) {
                String divGuid = null;
                List<String> userNames = Lists.newArrayList();
                Integer koordynatorKomorkiProfileId = 2;
                if (Docusafe.getAdditionProperty("profile.id.koordynator.komorki") != null) {
                    try {
                        koordynatorKomorkiProfileId = Integer.valueOf(Docusafe.getAdditionProperty("profile.id.koordynator.komorki"));
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                }
                boolean czyOdbiorca = false;
                if (StringUtils.isNotEmpty(fieldCnValue)) {
                    if (fieldCnValue.equalsIgnoreCase("ODBIORCA")) {
                        Long recipientId = (Long) fm.getKey("RECIPIENT_HERE");
                        String recipientLparam = Recipient.find(recipientId.longValue()).getLparam();
                        divGuid = recipientLparam.split(";")[1].split(":")[1];
                        czyOdbiorca = true;
                    } else {
                        try {
                            divGuid = DSDivision.findByCode(fieldCnValue).getGuid();
                        } catch (NullPointerException e) {
                            log.error(e.getMessage(), e);
                            throw new ValueNotFoundException("Nie znaleziono dzia�u o kodzie " + fieldCnValue);
                        }
                    }
                }
                if (divGuid == null) {
                    throw new ValueNotFoundException("Nie uda�o si� pobra� dzia�u z pola ODBIORCA");
                }

                return assignToUsersFromProfile(doc, assignable, openExecution, divGuid, userNames, koordynatorKomorkiProfileId, false, czyOdbiorca, 0);

            } else if (AKCEPTACJA_OZP_ACCEPTANCE.equals(acceptationCn)) {
                String author = doc.getAuthor();
                DSUser userAuthor = DSUser.findByUsername(author);
                DSDivision[] usersDivisions = userAuthor.getDivisionsWithoutGroup();
                char c = usersDivisions[0].getCode().charAt(0);

                List<String> users = new ArrayList<String>();
                List<String> divisions = new ArrayList<String>();
                String usedAcceptanceCn;
                switch (c) {
                    case 'K':
                        AssignUtils.getAssigneesFromAcceptanceTree(users, divisions, OZP_K_ACCEPTANCE);
                        usedAcceptanceCn = OZP_K_ACCEPTANCE;
                        break;
                    case 'G':
                        AssignUtils.getAssigneesFromAcceptanceTree(users, divisions, OZP_G_ACCEPTANCE);
                        usedAcceptanceCn = OZP_G_ACCEPTANCE;
                        break;
                    case 'W':
                        AssignUtils.getAssigneesFromAcceptanceTree(users, divisions, OZP_W_ACCEPTANCE);
                        usedAcceptanceCn = OZP_W_ACCEPTANCE;
                        break;
                    case 'O':
                        AssignUtils.getAssigneesFromAcceptanceTree(users, divisions, OZP_O_ACCEPTANCE);
                        usedAcceptanceCn = OZP_O_ACCEPTANCE;
                        break;
                    case 'T':
                        AssignUtils.getAssigneesFromAcceptanceTree(users, divisions, OZP_T_ACCEPTANCE);
                        usedAcceptanceCn = OZP_T_ACCEPTANCE;
                        break;
                    default:
                        AssignUtils.getAssigneesFromAcceptanceTree(users, divisions, OZP_O_ACCEPTANCE);
                        usedAcceptanceCn = OZP_O_ACCEPTANCE;
                }

                for (String user : users)
                {
                    assignable.addCandidateUser(user);
                    AssigneeHandler.addToHistory(openExecution, doc, user, null);
                    assigned = true;
                }
                for (String division : divisions)
                {
                    assignable.addCandidateGroup(division);
                    AssigneeHandler.addToHistory(openExecution, doc, null, division);
                    assigned = true;
                }
                if (!assigned) {
                    throw new EdmException("Nie znaleziono nikogo do przypisania na drzewie akceptacji " +
                            "dla akceptacji: '" + usedAcceptanceCn + "'");
                }
            }

        } catch (EdmException e) {
            if (e instanceof ProfileNotAssignedException || e instanceof ValueNotFoundException || e instanceof DivisionNotFoundException) {
                throw e;
            }
            throw e;
        } catch (SQLException e) {
            throw new EdmException(e);
        }
        return assigned;
    }

    protected boolean assignToUsersFromProfile(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String divGuid, List<String> userNames, Integer profileId, boolean akceptacjaDyrektoraPionu, boolean czyOdbiorca, int count) throws SQLException, EdmException {
        String sqlQuery = "select u.NAME " +
                "from DS_DIVISION d " +
                "join DS_USER_TO_DIVISION ud on d.ID = ud.DIVISION_ID " +
                "join DS_USER u on ud.USER_ID = u.ID " +
                "join ds_profile_to_user pu on u.ID = pu.user_id " +
                "where pu.profile_id = ? and d.GUID = ?";

        boolean assigned = false;
        PreparedStatement ps;
        ResultSet rs;

        ps = DSApi.context().prepareStatement(sqlQuery);
        ps.setInt(1, profileId);
        ps.setString(2, divGuid);
        rs = ps.executeQuery();
        while (rs.next()) {
            userNames.add(rs.getString(1));
        }

        if (!userNames.isEmpty()) {
            for (String user : userNames) {
                assignable.addCandidateUser(user);
                AssigneeHandler.addToHistory(openExecution, doc, user, null);
            }
            return true;
        } else if (akceptacjaDyrektoraPionu) {
            DSDivision div = DSDivision.find(divGuid);
            div = div.getParent();
            if (div == null || div.isRoot()) {
                throw new ProfileNotAssignedException("Brak przypisanego u�ytkownika do profilu " +
                        Profile.findById(Long.valueOf(profileId)).getName() + " dla dzia�u " + DSDivision.find(divGuid).getCode());
            }
            assigned = assignToUsersFromProfile(doc, assignable, openExecution, div.getGuid(), userNames, profileId, akceptacjaDyrektoraPionu, false, 0);
        } else if (assignable != null && assignable instanceof TaskImpl && "kierownik_komorki".equals(((TaskImpl) assignable).getActivityName())) {
            DSDivision div = DSDivision.find(divGuid);
            div = div.getParent();
            if (div == null || div.isRoot() || count == 0) {
                throw new ProfileNotAssignedException("Brak przypisanego u�ytkownika do profilu " +
                        Profile.findById(Long.valueOf(profileId)).getName() + " dla dzia�u " + DSDivision.find(divGuid).getCode());
            }
            count--;
            assigned = assignToUsersFromProfile(doc, assignable, openExecution, div.getGuid(), userNames, profileId, akceptacjaDyrektoraPionu, false, count);
        } else if (assignable != null && assignable instanceof TaskImpl && "koordynator_komorki".equals(((TaskImpl) assignable).getActivityName())) {
            DSDivision div = DSDivision.find(divGuid);
            div = div.getParent();
            if (div == null || div.isRoot() || count == 0) {
                throw new ProfileNotAssignedException("Brak przypisanego u�ytkownika do profilu " +
                        Profile.findById(Long.valueOf(profileId)).getName() + " dla dzia�u " + DSDivision.find(divGuid).getCode());
            }
            count--;
            assigned = assignToUsersFromProfile(doc, assignable, openExecution, div.getGuid(), userNames, profileId, akceptacjaDyrektoraPionu, false, count);
        } else {
            DSDivision div = DSDivision.find(divGuid);
            div = div.getParent();
            if (div == null || div.isRoot() || count == 0) {
                throw new ProfileNotAssignedException("Brak przypisanego u�ytkownika do profilu " +
                        Profile.findById(Long.valueOf(profileId)).getName() + " dla dzia�u " + DSDivision.find(divGuid).getCode());
            }
            count--;
            assigned = assignToUsersFromProfile(doc, assignable, openExecution, div.getGuid(), userNames, profileId, akceptacjaDyrektoraPionu, false, count);
        }
        return assigned;
    }

    public void setAcceptances(long docId, Map<String, Object> values, String kierownikKomorkiTaskName, String dyrektorPionuTaskName, String kierownikProjektuTaskName) throws EdmException, UserNotFoundException {
        Map<Long, CostAcceptance> costAcceptancesMap = new HashMap<Long, CostAcceptance>();
        ZrodlaFinansowaniaDict zrodloInstance = ZrodlaFinansowaniaDict.getInstance();

        for (DocumentAcceptance dacc : DocumentAcceptance.find(docId)) {
            DSUser acceptedUser = DSUser.findByUsername(dacc.getUsername());
            if (dacc.getObjectId() != null) {
                if (dacc.getAcceptanceCn().equals(kierownikKomorkiTaskName)) {
                    if (costAcceptancesMap.containsKey(dacc.getObjectId())) {
                        costAcceptancesMap.get(dacc.getObjectId()).setKierownikKomorki(acceptedUser.asFirstnameLastname());
                        costAcceptancesMap.get(dacc.getObjectId()).setKierownikKomorkiData(DateUtils.formatJsDateTime((dacc.getAcceptanceTime())));
                    } else {
                        CostAcceptance costAcc = new CostAcceptance();
                        setMpkValues(dacc, costAcc, zrodloInstance);

                        // akceptacja kierownika kom�rki
                        costAcc.setKierownikKomorki(acceptedUser.asFirstnameLastname());
                        costAcc.setKierownikKomorkiData(DateUtils.formatJsDateTime((dacc.getAcceptanceTime())));

                        costAcceptancesMap.put(dacc.getObjectId(), costAcc);
                    }
                }
                if (dacc.getAcceptanceCn().equals(dyrektorPionuTaskName)) {
                    if (costAcceptancesMap.containsKey(dacc.getObjectId())) {
                        costAcceptancesMap.get(dacc.getObjectId()).setDyrektorPionu((acceptedUser.asFirstnameLastname()));
                        costAcceptancesMap.get(dacc.getObjectId()).setDyrektorPionuData(DateUtils.formatJsDateTime((dacc.getAcceptanceTime())));
                    } else {
                        CostAcceptance costAcc = new CostAcceptance();
                        setMpkValues(dacc, costAcc, zrodloInstance);
                        // akceptacja dyrektora pionu
                        costAcc.setDyrektorPionu(acceptedUser.asFirstnameLastname());
                        costAcc.setDyrektorPionuData(DateUtils.formatJsDateTime((dacc.getAcceptanceTime())));

                        costAcceptancesMap.put(dacc.getObjectId(), costAcc);
                    }
                }
                if (dacc.getAcceptanceCn().equals(kierownikProjektuTaskName)) {
                    if (costAcceptancesMap.containsKey(dacc.getObjectId())) {
                        costAcceptancesMap.get(dacc.getObjectId()).setKierownikProjektu(acceptedUser.asFirstnameLastname());
                        costAcceptancesMap.get(dacc.getObjectId()).setKierownikProjektuData(DateUtils.formatJsDateTime((dacc.getAcceptanceTime())));
                    } else {
                        CostAcceptance costAcc = new CostAcceptance();
                        setMpkValues(dacc, costAcc, zrodloInstance);
                        // akceptacja kierownika projektu
                        costAcc.setKierownikProjektu(acceptedUser.asFirstnameLastname());
                        costAcc.setKierownikProjektuData(DateUtils.formatJsDateTime((dacc.getAcceptanceTime())));

                        costAcceptancesMap.put(dacc.getObjectId(), costAcc);
                    }
                }
            } else {
                values.put(dacc.getAcceptanceCn(), acceptedUser.getLastname() + " " + acceptedUser.getFirstname());
                values.put(dacc.getAcceptanceCn() + "_DATE", dacc.getAcceptanceTime());
            }
        }
        if (!costAcceptancesMap.isEmpty()) {
            values.put("ZRODLA_FINANSOWE", costAcceptancesMap.values());
        }
    }

    private void setMpkValues(DocumentAcceptance dacc, CostAcceptance costAcc, ZrodlaFinansowaniaDict zrodloInstance) throws EdmException {
        ZrodlaFinansowaniaDict zrodlo = zrodloInstance.find(dacc.getObjectId());

        // Budzet
        String budzet = DataBaseEnumField.getEnumItemForTable(BUDGET_VIEW_TABLENAME, zrodlo.getBudget()).getTitle();
        costAcc.setBudzet(budzet);
        // Pozycja
        String pozycja = DataBaseEnumField.getEnumItemForTable(BUDGET_POSITIONS_TABLENAME, zrodlo.getPosition()).getTitle();
        costAcc.setPozycja(pozycja);
        // Zrodlo
        String source = DataBaseEnumField.getEnumItemForTable(SOURCES_TABLENAME, zrodlo.getSource()).getTitle();
        costAcc.setZrodlo(source);
        // Kwota
        BigDecimal kwota = zrodlo.getAmount().setScale(2, RoundingMode.HALF_UP);
        costAcc.setKwota(kwota.toString());
    }

    protected void setBudgetItemsRefValues(String dictionaryName, Map<String, FieldData> values, Map<String, Object> results) throws EntityNotFoundException, EdmException {
        String dic = dictionaryName + "_";

        DwrUtils.setRefValueEnumOptions(values, results, "BUDGET", dic, "POSITION", BUDGET_POSITIONS_TABLENAME, null, FakturaZakupowaLogic.EMPTY_ENUM_VALUE);
        DwrUtils.setRefValueEnumOptions(values, results, "POSITION", dic, "SOURCE", SOURCES_TABLENAME, null, FakturaZakupowaLogic.EMPTY_ENUM_VALUE);

    }

    static String buildAdditionDescription(Map<String, String> additionField) throws EdmException {
        StringBuilder description = new StringBuilder();
        Map<String, String> additionFields = additionField;
        for (Map.Entry<String, String> item : additionFields.entrySet()) {
            if (!Strings.isNullOrEmpty(item.getValue())) {
                description.append(item.getKey())
                        .append(": ")
                        .append(item.getValue())
                        .append("; ");
            }
        }
        return description.toString();
    }
}
