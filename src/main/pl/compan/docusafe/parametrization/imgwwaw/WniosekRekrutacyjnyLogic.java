package pl.compan.docusafe.parametrization.imgwwaw;

import com.google.common.collect.Maps;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.util.Map;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

public class WniosekRekrutacyjnyLogic extends AbstractDocumentLogic {
    private static final String OS_WNIOSKUJACA_CN = "OS_WNIOSKUJACA";
    private static final String VARIABLE_NAME = "przelozonyId";
    protected static Logger log = LoggerFactory.getLogger(WniosekRekrutacyjnyLogic.class);
    private static WniosekRekrutacyjnyLogic instance;

    public static synchronized WniosekRekrutacyjnyLogic getInstance() {
        if (instance == null)
            instance = new WniosekRekrutacyjnyLogic();
        return instance;
    }

//    @Override
//    public boolean canChangeDockind(Document document) throws EdmException {
//        return false;
//    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();
        for (Field f : fm.getFields())
            if (f.getDefaultValue() != null)
                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));

        DSUser user = DSApi.context().getDSUser();
        toReload.put(OS_WNIOSKUJACA_CN, user.getId());
        //        if (user.getDivisionsWithoutGroup().length > 0)
        //            toReload.put("WORKER_DIVISION", fm.getField("WORKER_DIVISION").getEnumItemByCn(user.getDivisionsWithoutGroupPosition()[0].getGuid()).getId());
        fm.reloadValues(toReload);
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
        document.setFolder(folder);

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        document.setTitle("" + (String) fm.getValue(fm.getMainFieldCn()));
        document.setDescription("" + (String) fm.getValue(fm.getMainFieldCn()));

    }

    public int getPermissionPolicyMode() {
        return PermissionManager.NORMAL_POLICY;
    }

    @Override
    public boolean searchCheckPermissions(Map<String, Object> values) {
        return false;
    }

    public void documentPermissions(Document document) throws EdmException {
    }

    public void onStartProcess(OfficeDocument document, ActionEvent event) {
        try {
            Map<String, Object> map = Maps.newHashMap();
            map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
            //map.put(ASSIGN_DIVISION_GUID_PARAM, DSDivision.ROOT_GUID);
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
        boolean assigned = false;
        if (acceptationCn.equals(VARIABLE_NAME)) {
            Long dicId = (Long) openExecution.getVariable(VARIABLE_NAME);
            if (dicId != null) {
                try {
                    DSUser user = DSUser.findById(dicId);
                    assignable.addCandidateUser(user.getName());
                    AssigneeHandler.addToHistory(openExecution, doc, user.getName(), null);
                    assigned = true;

                } catch (EdmException e) {
                    log.error(e.getMessage(), e);
                }
            }
            else{
                log.error("A found value by id-name [" + VARIABLE_NAME + "] is null.");
            }
        }
        return assigned;
    }
}
