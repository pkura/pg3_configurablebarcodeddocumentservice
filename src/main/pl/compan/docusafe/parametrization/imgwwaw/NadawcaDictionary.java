package pl.compan.docusafe.parametrization.imgwwaw;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.PersonDictionary;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class NadawcaDictionary extends PersonDictionary
{
	private final static Logger log = LoggerFactory.getLogger(NadawcaDictionary.class);
	private final static String _SQL = "select id from DSO_PERSON where NIP = ?";
	
	@Override
	public long add(Map<String, FieldData> values) //throws EdmException
	{
		try
		{
			log.info("dodaje nadawce");
			log.info("add/sender = " + values.get("SENDER_NIP"));
			if(values.get("SENDER_NIP") != null ) {
				String nip = values.get("SENDER_NIP").toString().replace("-", "").replace(" ", "");
				
				if(findNadawcaByNIP(nip) != null) {
					return -1;
				}
			}	
		} catch(Exception e){
			log.error("", e);
		}
		return super.add(values);
	}
	
	/** 
	 * Zwraca id 
	 * @param numer
	 * @return
	 */
	private Long findNadawcaByNIP(String nip)
	{
		Long nadawcaId = null;
		try
		{
			PreparedStatement ps = DSApi.context().prepareStatement(_SQL);
			if(nip == null)
				return nadawcaId;
			
			ps.setString(1, nip);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next())
			{
				nadawcaId = Long.valueOf(rs.getLong(1));
			}
		}catch(Exception e){
			log.error("", e);
		}
		return nadawcaId;
	}

	
}
