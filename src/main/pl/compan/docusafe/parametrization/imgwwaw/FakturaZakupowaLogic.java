package pl.compan.docusafe.parametrization.imgwwaw;

import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.exports.*;
import pl.compan.docusafe.core.jbpm4.AssignUtils;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.*;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;

public class FakturaZakupowaLogic extends ImgwDocumentLogic {
    private final static Logger log = LoggerFactory.getLogger(FakturaZakupowaLogic.class);

    public static final String STATUS_OPIS_MERYTORYCZNY = "osoba_merytoryczna";
    public static final String PODSTAWA_ZAKUPU_CN = "PODSTAWA_ZAKUPU";
    public static final String STATUS_CN = "STATUS";
    public static final String RODZAJ_ZAKUPU_WNIOSEK = "RODZAJ_ZAKUPU_WNIOSEK";
    public static final String RODZAJ_ZAKUPU_OPLATA = "RODZAJ_ZAKUPU_OPLATA";
    public static final String RODZAJ_ZAKUPU_UMOWA = "RODZAJ_ZAKUPU_UMOWA";
    public static final String AKCEPTACJA_WG_RODZAJU_PRZEDMIOTU_ACCEPTANCE = "akceptacja_wg_rodzaju_przedmiotu";
    public static final String RODZAJ_PRZEDMIOTU_CN = "RODZAJ_PRZEDMIOTU";
    public static final String TYP_FAKTURY_CN = "TYP_FAKTURY";
    public static final String ZWROT_KOSZTOW_CN = "ZWROT_KOSZTOW";
    public static final String STATUS_OZP = "akceptacja_ozp";

    protected static final EnumValues EMPTY_ENUM_VALUE = new EnumValues(new HashMap<String, String>(), new ArrayList<String>());
    public static final String OPIS_FAKTURY_TABLENAME = "dsg_imgw_opis_faktury";
    public static final String RODZAJ_KOSZTU_TABLENAME = "dsg_imgw_rodzaj_kosztu";
    public static final String RODZAJ_DZIALALNOSCI_TABLENAME = "dsg_imgw_rodzaj_dzialalnosci";
    public static final String CONTRACTOR_FIELD_CN = "CONTRACTOR";
    public static final String VAT_RATES_TABLENAME = "dsg_imgw_vat_rates";
    public static final String DWR_SYNCH = "DWR_SYNCH";
    public static final String DWR_IMP_POZ = "DWR_IMP_POZ";
    public static final String DWR_ZRODLO_FINANSOWANIA = "DWR_ZRODLO_FINANSOWANIA";
    public static final String DSG_IMGW_PRODUKT = "dsg_imgw_produkt";
    public static final String OSOBA_MERYTORYCZNA = "OSOBA_MERYTORYCZNA";

    public static final String DPK_ACCEPTANCE = "dpk";

    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();
        for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields()) {
            if (f.getDefaultValue() != null) {
                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
            }
        }
        fm.reloadValues(toReload);
    }

    @Override
    public void setAdditionalTemplateValues(long docId, Map<String, Object> values, Map<Class, Format> defaultFormatters) {
        try {
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setDecimalSeparator(',');
            defaultFormatters.put(BigDecimal.class, new DecimalFormat("### ### ##0.00", symbols));
            defaultFormatters.put(Date.class, new SimpleDateFormat("dd-MM-yyyy"));
            defaultFormatters.put(Timestamp.class, new SimpleDateFormat("dd-MM-yyyy"));

            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            fm.initialize();

            //kontrahent
            IMGWContractor contractor = IMGWContractor.find(fm.getIntegerKey("CONTRACTOR"));
            values.put("CONTRACTOR_NAZWA", contractor.getNazwa());
            values.put("CONTRACTOR_NIP", contractor.getNip());

            // czas wygenerowania metryki
            values.put("GENERATE_DATE", DateUtils.formatCommonDate(new java.util.Date()).toString());

            // generowanie akceptacji
            setAcceptances(docId, values, KIEROWNIK_KOMORKI_ACCEPTANCE, DYREKTOR_PIONU_ACCEPTANCE, KIEROWNIK_PROJEKTU_ACCEPTANCE);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) throws EdmException {
        boolean assigned = super.assignee(doc, assignable, openExecution, acceptationCn, fieldCnValue);
        try {
            FieldsManager fm = new FieldsManager(doc.getId(), doc.getDocumentKind());

            if (!assigned) {

                if (AKCEPTACJA_WG_RODZAJU_PRZEDMIOTU_ACCEPTANCE.equals(acceptationCn)) {
                    String refValue = fm.getEnumItem(RODZAJ_PRZEDMIOTU_CN).getRefValue();
                    DSUser user = DSUser.findById(Long.valueOf(refValue));
                    assignable.addCandidateUser(user.getName());
                    assigned = true;
                    AssigneeHandler.addToHistory(openExecution, doc, user.getName(), null);
                    return assigned;
                }
            }

            return assigned;
        } catch (EdmException e) {
            throw e;
        } catch (Exception e) {
            throw new EdmException(e);
        }
    }

    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        Field msgField = null;
        StringBuilder msgBuilder = new StringBuilder();

        String statusCn = fm.getEnumItemCn(STATUS_CN);

        try {
            //automatyczne wyliczanie kwoty vat
            if (!values.get("DWR_KWOTA_VAT").isSender() && values.get("DWR_KWOTA_NETTO") != null && values.get("DWR_KWOTA_BRUTTO") != null) {
                setVat(values);
            }
            //synchronizacja kontrahent�w przyciskiem "SYNCH"
            if (values.get(DWR_SYNCH) != null && values.get(DWR_SYNCH).isSender()) {
                ImgwButtonUtils.synchronizeContractorsOnDemand();
            }
            //import pozycji bud�etowuch z excela
            if (values.get(DWR_IMP_POZ) != null && values.get(DWR_IMP_POZ).isSender()) {
                ImgwButtonUtils.importBudgetPositions(fm, values);
            }
            //wype�nienie opisu do projektu wed�ug wybranego szablonu opsiu faktury
            if (DwrUtils.isNotNull(values, "DWR_OPIS")) {
                Integer opisFM = (Integer) fm.getKey("OPIS");
                Integer opisId = Integer.valueOf(values.get("DWR_OPIS").getEnumValuesData().getSelectedId());
                if (opisFM == null || !opisFM.equals(opisId)) {
                    String opis = DataBaseEnumField.getEnumItemForTable(OPIS_FAKTURY_TABLENAME, opisId).getTitle();
                    values.get("DWR_OPIS_DO_PROJEKTU").setStringData(opis);
                }
            }
            //wyliczenie sumy kwot brutto i vat z pozycji bud�etowych
//            if (values.containsKey("DWR_" + ZRODLO_FINANSOWANIA_CN) && values.get("DWR_" + ZRODLO_FINANSOWANIA_CN) != null) {
//                BigDecimal sum = new BigDecimal(0);
//                sum = sum.setScale(MONEY_SCALE);
//                Map<String, FieldData> mpkValues = (Map<String, FieldData>) values.get(DWR_MPK).getDictionaryData();
//                for (String key : mpkValues.keySet()) {
//                    if (key.contains(MPK_KWOTA_BUDZETU) && mpkValues.get(key).getData() != null) {
//                        BigDecimal amount = mpkValues.get(key).getMoneyData().setScale(MONEY_SCALE);
//                        sum = sum.add(amount);
//                    }
//                }
//                values.get(DWR_MPK_SUM_AMOUNT).setMoneyData(sum);
//                // sprawdzenie czy suma kwot brutto mpkow jest wieksza od kwoty brutto na fakturze,
//                //jezeli tak to jest rzucany wyjatek
//                BigDecimal grossAmount = values.get(DWR_KWOTA_BRUTTO).getMoneyData().setScale(2, RoundingMode.HALF_UP);
//                if (sum.compareTo(grossAmount) > 0) {
//                    values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
//                    return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", sm.getString("MpkAmountsAreBiggerThanGrossAmount"), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
//                }
//
//            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            msgBuilder.append(e.getMessage());
        } finally {
            DSApi.close();
        }
        if (msgBuilder.length() > 0) {
            values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
            return new Field("messageField", msgBuilder.toString(), Field.Type.BOOLEAN);
        }
        return null;
    }

    private void setVat(Map<String, FieldData> values) {
        BigDecimal netto = values.get("DWR_KWOTA_NETTO").getMoneyData();
        BigDecimal brutto = values.get("DWR_KWOTA_BRUTTO").getMoneyData();
        if (netto != null && brutto != null) {
            values.get("DWR_KWOTA_VAT").setMoneyData(brutto.subtract(netto));
        }
    }

    @Override
    public ExportedDocument buildExportDocument(OfficeDocument doc) throws EdmException {
        boolean processNotApplicableValue = false;
        Integer notApplicableValue = -1;

        PurchasingDocument exported = new PurchasingDocument();
        FieldsManager fm = doc.getFieldsManager();
        //stdost_id
        exported.setDelivery("O");
        //typdokzak_id
        exported.setPurchaseType("FZP");
        //dostawca_id
        IMGWContractor contractor = IMGWContractor.find(fm.getIntegerKey(CONTRACTOR_FIELD_CN));
        exported.setExternalContractor(contractor.getErpId().longValue());
        //kontobankowe_id
        if(fm.getEnumItem("KONTRAHENT_NR_KONTA_ID") != null)
        {
        	String kontrahent_nr_konta_id = fm.getEnumItem("KONTRAHENT_NR_KONTA_ID").getCn();
        	exported.setContractorAccountId(Integer.valueOf(kontrahent_nr_konta_id.substring(0, kontrahent_nr_konta_id.indexOf("."))));
        }
        //datdok
        exported.setIssued((Date) fm.getKey("DATA_WYSTAWIENIA"));
        //dok_uwagi
        exported.setDescription(getAdditionFieldsDescription(doc, fm));
        //dokobcy
        exported.setCode(fm.getStringValue("NR_FAKTURY"));
        //datprzyj
        exported.setRecieved(doc.getCtime());

        //dokzakpozycje
        List<Long> pozycjeFaktury = (List) fm.getKey(ZRODLO_FINANSOWANIA_CN);
        if (pozycjeFaktury != null) {
            int i = 1;
            List<BudgetItem> budgetItems = Lists.newArrayList();
            ZrodlaFinansowaniaDict zrodloInstance = ZrodlaFinansowaniaDict.getInstance();
            for (Long zrodloId : pozycjeFaktury) {
                ZrodlaFinansowaniaDict zrodlo = zrodloInstance.find(zrodloId);

                BudgetItem bItem = new BudgetItem();
                //vatstaw_id
                bItem.setVatRateCode(IMGWVatRate.find(zrodlo.getStawkaVAT()).getIdm());
                //jm_id
                bItem.setUnit("szt");
                //ilosc
                bItem.setQuantity(BigDecimal.ONE);
                //nrpoz
                bItem.setItemNo(i++);
                //rodztowaru
                bItem.setFixedAsset(false);
                //kwotnett
                bItem.setNetAmount(zrodlo.getAmount().setScale(2));
                
              //komorka_id
                Budget budget = Budget.findByBudzetIdAndPozBudzetId(zrodlo.getBudget(), zrodlo.getPosition());
                bItem.setMpkCode(budget.getKomBudIdn());
                
                //bd_budzet_ko_id
                bItem.setBudgetKoId(Long.valueOf(budget.getBdBudzetId()));
                //bd_rodzaj_ko_id
                bItem.setBudgetKindId(budget.getBdPozBudzetId());
                //budzet_id
                bItem.setBudgetDirectId(budget.getOkrBudId());
                //wytwor_id
                Integer wytworId = DataBaseEnumField.getEnumItemForTable(DSG_IMGW_PRODUKT, zrodlo.getProduct()).getCentrum();
                bItem.setCostKindId(Long.valueOf(wytworId));

                //kontrakt_id
                if(zrodlo.getContract() != null)
                {
                	Kontrakt k = Kontrakt.find(zrodlo.getContract());
                	bItem.setProjectCode(k.getIdm());
                    //przeznzak
                    bItem.setSprzedazOpodatkowana(k.getBpRodzajWersjiId().intValue());
                }


                if (zrodlo.getSource() != null) {
                    FundSource fItem = new FundSource();
                    fItem.setFundPercent(new BigDecimal(100));
                    // zrodlo finansowania
                    // zrodlo_id
                    Source source = Source.find(zrodlo.getSource());
//                    fItem.setCode(source.getZrodloIdn());
//                    fItem.setId(source.getZrodloId());
                    fItem.setId(16L);
                    fItem.setKind(FundSource.KIND_BD_BUDGET);
                    fItem.setAmount(zrodlo.getAmount().setScale(2));

                    bItem.setFundSources(Lists.newArrayList(fItem));
                }


                budgetItems.add(bItem);

            }
            exported.setBudgetItems(budgetItems);
        }

        String systemPath = null;
        if (Docusafe.getAdditionProperty("erp.faktura.link_do_skanu") != null) {
            systemPath = Docusafe.getAdditionProperty("erp.faktura.link_do_skanu");
        } else {
            systemPath = Docusafe.getBaseUrl();
        }

        for (Attachment zal : doc.getAttachments()) {
            AttachmentInfo info = new AttachmentInfo();
            String path = systemPath + "/repository/view-attachment-revision.do?id=" + zal.getMostRecentRevision().getId();
            info.setName(zal.getTitle());
            info.setUrl(path);
            exported.addAttachementInfo(info);
        }

        return exported;
    }

    /**
     * build description from addition fields
     *
     * @param doc
     * @param fm
     * @return
     * @throws EdmException
     */
    private String getAdditionFieldsDescription(OfficeDocument doc, FieldsManager fm) throws EdmException {
        Map<String, String> additionFields = Maps.newLinkedHashMap();
        additionFields.put("Nr KO", doc.getOfficeNumber() != null ? doc.getOfficeNumber().toString() : "brak");
        additionFields.put("Opis faktury", fm.getEnumItem("OPIS").getCn());
        additionFields.put("Rodzaj przedmiotu", fm.getStringValue("RODZAJ_PRZEDMIOTU"));
        additionFields.put("Tryb zakupu", fm.getStringValue("TRYB_ZAKUPU"));
        additionFields.put("Tryb zam�wienia", fm.getStringValue("TRYB_ZAMOWIENIA"));
        additionFields.put("Nr post�powania", fm.getStringValue("POSTEPOWANIE_NR"));
        additionFields.put("Numer umowy", fm.getStringValue("NUMER_UMOWY"));
        additionFields.put("Opis op�aty", fm.getStringValue("OPIS_OPLATY"));
        additionFields.put("Numer sprawy", fm.getStringValue("NUMER_SPRAWY"));

        StringBuilder description = new StringBuilder();
        for (String label : additionFields.keySet()) {
            if (!Strings.isNullOrEmpty(additionFields.get(label))) {
                description.append(label)
                        .append(": ")
                        .append(additionFields.get(label))
                        .append("; ");
            }
        }
        return description.toString();
    }

    public String getCnValue(String tableName, Integer enumId, boolean processNotApplicableValue, Integer notApplicableValue) throws EdmException {
        if (enumId != null && (processNotApplicableValue || !Objects.equal(notApplicableValue, enumId))) {
            return DataBaseEnumField.getEnumItemForTable(tableName, Integer.valueOf(enumId)).getCn();
        }
        return null;
    }

    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) {
        Map<String, Object> results = new HashMap<String, Object>();

        try {
            if (dictionaryName.equals(ZRODLO_FINANSOWANIA_CN)) {
                setBudgetItemsRefValues(dictionaryName, values, results);

                if (values.containsKey(ZRODLO_FINANSOWANIA_CN + "_STAWKA_VAT") && values.get(ZRODLO_FINANSOWANIA_CN + "_STAWKA_VAT").getData() != null
                        && values.containsKey(ZRODLO_FINANSOWANIA_CN + "_AMOUNT") && values.get(ZRODLO_FINANSOWANIA_CN + "_AMOUNT").getData() != null) {
                    int rateId = Integer.parseInt(values.get(ZRODLO_FINANSOWANIA_CN + "_STAWKA_VAT").getEnumValuesData().getSelectedId());
                    BigDecimal stawka = BigDecimal.valueOf(IMGWVatRate.find(rateId).getProcent()).movePointLeft(2).setScale(2);
                    BigDecimal brutto = values.get(ZRODLO_FINANSOWANIA_CN + "_AMOUNT").getMoneyData();
                    BigDecimal vatAmount;
                    if (stawka.equals(BigDecimal.ZERO.setScale(2))) {
                        vatAmount = BigDecimal.ZERO.setScale(2);
                    } else {
                        BigDecimal netto = brutto.divide(stawka.add(BigDecimal.ONE), 2, BigDecimal.ROUND_HALF_DOWN);
                        vatAmount = brutto.subtract(netto).setScale(2);
                    }
                    results.put(ZRODLO_FINANSOWANIA_CN + "_KWOTA_VAT", vatAmount);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return results;
    }


}
