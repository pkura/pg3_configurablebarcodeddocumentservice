package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import com.google.common.collect.Sets;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.ContractorAccount;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.IMGWContractor;
import pl.compan.docusafe.parametrization.imgwwaw.services.ContractorAccountImport;
import pl.compan.docusafe.parametrization.imgwwaw.services.ContractorImport;
import pl.compan.docusafe.parametrization.invoice.DictionaryUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.imgw.DictionaryServiceStub;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by brs on 24.12.13.
 */
public class SynchronizeContractorDictionary implements ExternalActivityBehaviour {
    private static final Logger log = LoggerFactory.getLogger(SynchronizeContractorDictionary.class);

    DictionaryServiceStub.Supplier[] items;
    private int importPageSize;
    private int importPageNumber;
    private Set<Long> itemIds;

    @Override
    public void signal(ActivityExecution execution, String signalName, Map<String, ?> parameters) throws Exception {
    }

    private boolean importContractors(DictionaryServiceStub stub) throws java.rmi.RemoteException, EdmException {
        if (stub != null) {
            DictionaryServiceStub.GetSupplier params = new DictionaryServiceStub.GetSupplier();
            params.setPageSize(importPageSize);
            params.setPage(importPageNumber++);
            DictionaryServiceStub.GetSupplierResponse response;
            if (params != null && (response = stub.getSupplier(params)) != null) {
                items = response.get_return();
                if (items != null) {
                    for (DictionaryServiceStub.Supplier item : items) {
                        if (item != null) {
                            List<IMGWContractor> found = DictionaryUtils.findByGivenFieldValue(IMGWContractor.class, "erpId", item.getId());
                            if (!found.isEmpty()) {
                                for (IMGWContractor resource : found) {
                                    resource.setAllFieldsFromServiceObject(item);
                                    resource.setAvailable(true);
                                    resource.save();
//                                    log.error("Zaktualizowano kontrahenta, nazwa: {}, erpID: {}", resource.getNazwa(), resource.getErpId());
                                    itemIds.add(Long.valueOf(resource.getId()));
                                }
                            } else {
                                IMGWContractor resource = new IMGWContractor();
                                resource.setAllFieldsFromServiceObject(item);
                                resource.save();
                                log.error("Dodano nowego kontrahenta, nazwa: {}, erpID: {}", resource.getNazwa(), resource.getErpId());
                                itemIds.add(Long.valueOf(resource.getId()));
                            }
                        }
                    }
                    return false;
                } else {
                    return true;
                }
            }
        }
        return true;
    }

    private boolean importContractorAccounts(DictionaryServiceStub stub) throws java.rmi.RemoteException, EdmException {
        if (stub != null) {
            stub._getServiceClient().cleanupTransport();
            DictionaryServiceStub.GetSupplierBankAccount params = new DictionaryServiceStub.GetSupplierBankAccount();
            params.setPageSize(importPageSize);
            params.setPage(importPageNumber++);
            DictionaryServiceStub.GetSupplierBankAccountResponse response;
            if (params != null && (response = stub.getSupplierBankAccount(params)) != null) {
                DictionaryServiceStub.SupplierBankAccount[] items = response.get_return();
                if (items != null) {
                    for (DictionaryServiceStub.SupplierBankAccount item : items) {
                        if (item != null) {
                            List<ContractorAccount> found = pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils.findByGivenFieldValue(ContractorAccount.class, "erpId", item.getId());
                            if (!found.isEmpty()) {
                                for (ContractorAccount resource : found) {
                                    resource.setAllFieldsFromServiceObject(item);
                                    resource.setAvailable(true);
                                    resource.save();
                                    itemIds.add(Long.valueOf(resource.getId()));
                                }
                            } else {
                                ContractorAccount resource = new ContractorAccount();
                                resource.setAllFieldsFromServiceObject(item);
                                resource.setAvailable(true);
                                resource.save();
                                log.error("Dodano konto bankowe {} do kontrahenta", resource.getIdm());
                                itemIds.add(Long.valueOf(resource.getId()));
                            }
                        }
                    }
                    return false;
                }
            }
        }
        return true;
    }

    public void initImport() {
        items = null;
        importPageNumber = 1;
        importPageSize = 1000;
        itemIds = Sets.newHashSet();
    }

    public void finalizeContractorImport(boolean delete) throws EdmException {
        int deleted = ServicesUtils.disableDeprecatedItem(itemIds, ContractorImport.SERVICE_TABLE_NAME, false);
        log.error("Synchronizacja kontrahentów: usunięto {} kontrahentów", deleted);
    }

    public void finalizeAccountsImport(boolean delete) throws EdmException {
        int deleted = ServicesUtils.disableDeprecatedItem(itemIds, ContractorAccountImport.SERVICE_TABLE_NAME, false);
        log.error("Synchronizacja kont kontrahentów: usunięto {} kont bankowych", deleted);
    }

    @Override
    public void execute(ActivityExecution execution) throws Exception {
        AxisClientConfigurator conf = new AxisClientConfigurator();
        DictionaryServiceStub stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
        conf.setUpHttpParameters(stub, ContractorImport.SERVICE_PATH);

        boolean done;
        initImport();
        do {
            boolean isOpened = true;
            try {
                if (!DSApi.isContextOpen()) {
                    isOpened = false;
                    DSApi.openAdmin();
                }
//                DSApi.context().begin();
                done = importContractors(stub);
//                DSApi.context().commit();
            } finally {
                if (DSApi.isContextOpen() && !isOpened)
                    DSApi._close();
            }
        } while (!done);
        finalizeContractorImport(true);

        done = false;
        initImport();
        do {
            boolean isOpened = true;
            try {
                if (!DSApi.isContextOpen()) {
                    isOpened = false;
                    DSApi.openAdmin();
                }
//                DSApi.context().begin();
                done = importContractorAccounts(stub);
//                DSApi.context().commit();
            } finally {
                if (DSApi.isContextOpen() && !isOpened)
                    DSApi._close();
            }
        } while (!done);
        finalizeAccountsImport(false);
    }
}
