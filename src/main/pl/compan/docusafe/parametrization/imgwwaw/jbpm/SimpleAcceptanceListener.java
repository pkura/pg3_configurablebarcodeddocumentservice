package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.AcceptancesListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class SimpleAcceptanceListener extends AbstractEventListener {

    private static final Logger log = LoggerFactory.getLogger(SimpleAcceptanceListener.class);

    private String doNotUpdateAssignmentHistory;
    private String type;
    private String acception;

    private String doNotInformLogic;
    private String simpleAcception;

    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        OfficeDocument doc = getOfficeDocument(execution);
        executeTask(execution, doc);
    }

    protected void executeTask(EventListenerExecution execution, OfficeDocument doc) throws EdmException {
        if (!isDoNotUpdateAssignmentHistory())
            AcceptancesListener.updateAssignmentHistory(execution, doc, acception, null);

        if (!isDoNotInformLogic())
            doc.getDocumentKind().logic().onAcceptancesListener(doc, simpleAcception);
    }

    public static OfficeDocument getOfficeDocument(EventListenerExecution execution) throws EdmException {
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);
        return doc;
    }

    public String getAcception() {
        return acception;
    }

    public void setAcception(String acception) {
        this.acception = acception;
    }

    public String getSimpleAcception() {
        return simpleAcception;
    }

    public void setSimpleAcception(String simpleAcception) {
        this.simpleAcception = simpleAcception;
    }

    public String getDoNotUpdateAssignmentHistory() {
        return doNotUpdateAssignmentHistory;
    }

    public void setDoNotUpdateAssignmentHistory(String doNotUpdateAssignmentHistory) {
        this.doNotUpdateAssignmentHistory = doNotUpdateAssignmentHistory;
    }

    public boolean isDoNotUpdateAssignmentHistory (){
        return doNotUpdateAssignmentHistory!=null && "true".equalsIgnoreCase(doNotUpdateAssignmentHistory) ? true : false;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDoNotInformLogic() {
        return doNotInformLogic;
    }

    public void setDoNotInformLogic(String doNotInformLogic) {
        this.doNotInformLogic = doNotInformLogic;
    }

    public boolean isDoNotInformLogic(){
        return doNotInformLogic!=null && "true".equalsIgnoreCase(doNotInformLogic) ? true : false;
    }
}
