package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import java.math.BigDecimal;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 16-08-2013
 * docusafe
 * IndicationDecision.java
 * 
 * Je�li kwota zam�wienia przekracza 5 tys. z� dla us�ugi lub je�li kwota zam�wienia przekracza 10 tys. z� dla dostawy to goToCoordinator.
 */
@SuppressWarnings("serial")
public class IndicationDecision implements DecisionHandler {
    private final static Logger LOG = LoggerFactory.getLogger(IndicationDecision.class);
    private final static String WARTOSC_NETTO = "WARTOSC_NETTO";
    private final static String RODZAJ_ZAMOWIENIA = "RODZAJ_ZAMOWIENIA";
    private final static String USLUGA = "USLUGA";
    private final static String DOSTAWA = "DOSTAWA";
    private static final int MONEY_SCALE = 2;
    
    
    public String decide(OpenExecution execution) {
    	Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
    	Document document;
    	try{
		document = Document.find(docId);
    	FieldsManager fm = document.getFieldsManager();
    	BigDecimal kwotaZamowienia =  new BigDecimal(0);
    	if(fm.getKey(WARTOSC_NETTO) == null){
    		kwotaZamowienia = kwotaZamowienia.setScale(MONEY_SCALE);
    	}else{
    		kwotaZamowienia = ((BigDecimal)fm.getKey(WARTOSC_NETTO)).setScale(MONEY_SCALE);
    	}
    	
    	boolean czyUsluga = fm.getEnumItemCn(RODZAJ_ZAMOWIENIA).equals(USLUGA);
    	boolean czyDostawa = fm.getEnumItemCn(RODZAJ_ZAMOWIENIA).equals(DOSTAWA);
	    	if((czyUsluga) && (kwotaZamowienia.compareTo(new BigDecimal(5000)) == 1)){
	    		return "startProcess";
	    	} else if((czyDostawa) && (kwotaZamowienia.compareTo(new BigDecimal(10000)) == 1)){
	    		return "startProcess";
	    	} else{
	    		return "goToOzp";
	    	}
    	
    	} catch (DocumentNotFoundException e) {
    		LOG.error(e.getMessage(), e);
    		return "error";	
    	} catch (DocumentLockedException e) {
			LOG.error(e.getMessage(), e);
			return "error";	
    	} catch (AccessDeniedException e) {
			LOG.error(e.getMessage(), e);
			return "error";	
    	} catch (EdmException e) {
			LOG.error(e.getMessage(), e);
			return "error";	
    	} catch (Exception e) {
		    LOG.error(e.getMessage(), e);
		    return "error";	
    	}

    }
}
