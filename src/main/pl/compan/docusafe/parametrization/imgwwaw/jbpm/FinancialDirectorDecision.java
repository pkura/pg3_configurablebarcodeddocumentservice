package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: brs
 * Date: 27.11.13
 * Time: 14:27
 * To change this template use File | Settings | File Templates.
 */
public class FinancialDirectorDecision implements DecisionHandler {

    private final Logger log = LoggerFactory.getLogger(FinancialDirectorDecision.class);


    @Override
    public String decide(OpenExecution execution) {
        try {
            OfficeDocument document = Jbpm4Utils.getDocument(execution);
            FieldsManager fm = document.getFieldsManager();
            List<Long> zrodlaIds = (List<Long>) fm.getKey("ZRODLO_FINANSOWANIA");

            if (Math.random() > 0.5) {
                execution.setVariable("financialDirector", "true");
                return "koszty_ogolne";
            } else {
                execution.removeVariable("financialDirector");
                return "koszty_nieogolne";
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            return "error";
        }
    }
}
