package pl.compan.docusafe.parametrization.imgwwaw.jbpm;


import org.jbpm.api.Execution;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.parametrization.imgwwaw.WniosekZamPubLogic;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class CloseDHProcess implements ExternalActivityBehaviour {

    private static final Logger log = LoggerFactory.getLogger(CloseDHProcess.class);

    @Override
    public void signal(ActivityExecution execution, String signalName, Map<String, ?> parameters) throws Exception {

        execution.removeVariable("dh");

//        Jbpm4Provider.getInstance().getTaskService().getTaskParticipations(execution.get).get(0).get

        OfficeDocument doc = Jbpm4Utils.getDocument(execution);
        List<String> processIds = (List<String>) Jbpm4ProcessLocator.processIds(doc.getId());


        for (String processId : processIds) {
            if (processId.startsWith(WniosekZamPubLogic.DH_PROCESS_NAME)) {
                Jbpm4Provider.getInstance().getExecutionService().deleteProcessInstance(processId);


//                List<Execution> executions = (List<Execution>) Jbpm4Provider.getInstance().getExecutionService().findExecutionById(processId).getExecutions();
//                for (Execution exec : executions) {
//                    if (Execution.STATE_ACTIVE_ROOT.equals(exec.getState())) {
//                        try {
////                            Jbpm4Provider.getInstance().getExecutionService().signalExecutionById(execution.getId(), signalName);
//                            ((ActivityExecution) exec).end();
//                            JBPMTaskSnapshot.updateByDocumentId(doc.getId(), "anyType");
//                        } catch (Exception e) {
//                            log.error(e.getMessage(), e);
//                        }
//                    }
//                }
            }
        }


//        doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(od, Collections.<String, Object> singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY, user));
    }

    @Override
    public void execute(ActivityExecution execution) throws Exception {
    }
}
