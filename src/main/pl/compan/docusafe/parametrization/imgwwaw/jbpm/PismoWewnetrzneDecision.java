package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class PismoWewnetrzneDecision implements DecisionHandler {

    private static final long serialVersionUID = 1L;

    private final static Logger LOG = LoggerFactory.getLogger(PismoWewnetrzneDecision.class);

    public String decide(OpenExecution openExecution) {
        try {
        	LOG.info("decyzja");
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            LOG.info("documentKind = " + fm.getDocumentKind().getCn());
            if (fm.getDocumentKind().getCn().equals("normal")) {
                return "dzial";
            } else {
                return "author";
            }
        } catch (EdmException ex) {
            LOG.error(ex.getMessage(), ex);
            return "error";
        }
    }
}