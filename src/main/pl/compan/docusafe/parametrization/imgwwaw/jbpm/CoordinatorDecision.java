package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Created with IntelliJ IDEA.
 * User: brs
 * Date: 19.11.13
 * Time: 13:06
 * To change this template use File | Settings | File Templates.
 */
public class CoordinatorDecision implements DecisionHandler{

    private final Logger log = LoggerFactory.getLogger(CoordinatorDecision.class);

    @Override
    public String decide(OpenExecution execution) {
        try {
            OfficeDocument document = Jbpm4Utils.getDocument(execution);
            FieldsManager fm = document.getFieldsManager();
            Long recipientId = (Long) fm.getKey("RECIPIENT_HERE");
            String recipientLParam = Recipient.find(recipientId.longValue()).getLparam();

            if (recipientLParam.contains("u:")) {
                String username = recipientLParam.split(";")[0].split(":")[1];
                execution.setVariable(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM, username);
                return "odbiorca";
            }
            else if (recipientLParam.contains("d:"))
                return "koordynator_komorki";
            return "error";

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return "error";
        }
    }
}
