package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class KierownikKomorkiProcessListener implements ExternalActivityBehaviour {

    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(KierownikKomorkiProcessListener.class);
    private String dictionaryField;
    private String enumIdField;
    
    private static final String PRZEDMIOT_ZAMOWIENIA = "PRZEDMIOT_ZAMOWIENIA";
	private static final String ZRODLO_FINANSOWANIA = "ZRODLO_FINANSOWANIA";

    @Override
    public void execute(ActivityExecution execution) throws Exception {
        execution.removeVariable("correction");
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);
        final List<Long> argSets = new LinkedList<Long>();
        
        log.info("dictionaryField = " + dictionaryField);
        log.info("dictionaryField = " + doc.getFieldsManager().getKey(dictionaryField.toUpperCase()));

        if ("ZRODLO_FINANSOWANIA".equals(dictionaryField)) {

//            Integer id = (Integer) doc.getFieldsManager().getKey(dictionaryField.toUpperCase());
//            argSets.add(id.longValue());
            argSets.add(1L);

//            List<Integer> ids = (List<Integer>) doc.getFieldsManager().getKey(dictionaryField.toUpperCase());
//            for (Integer id : ids) {
//                log.info("dictionaryField = " + id);
//            }

        }


        execution.setVariable("ids", argSets);
        //execution.setVariable("count", argSets.size());
        //execution.setVariable("count2", 2);
    }

    @Override
    public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
        // TODO Auto-generated method stub

    }
    
    /**Metoda zwraca liste ids z listy w slowniku
	 * @param doc
	 * @param dictionaryName - cn slownika
	 * @param enumField -  cn listy
	 * @return lista ids wybranych pozycji z listy w slowniku
	 * @throws EdmException
	 */
	private Set<Integer> getFinanceSourceIds(OfficeDocument doc, String dictionaryName, String enumField) throws EdmException {
		FieldsManager fm = Document.find(doc.getId()).getFieldsManager();
		Set<Integer> listaZrodel = new HashSet<Integer>();
		List<Map<String, Object>> mapa = (List<Map<String, Object>>) fm.getValue(dictionaryName);
	    if (mapa != null && !mapa.isEmpty()) {
		    for(Map<String, Object> row : mapa){
		        for(Map.Entry<String, Object> entry : row.entrySet()){
		        	if (entry.getKey().contains(enumField) && entry.getValue() != null) {
		        		EnumValues financeSource = (EnumValues)entry.getValue();
		        		listaZrodel.add(Integer.valueOf(financeSource.getSelectedId()));
		        	}
		        }
		    }
	    }
	    return listaZrodel;
    }

}
