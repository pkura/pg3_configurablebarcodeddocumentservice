package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Map;



public class ERPProcessListener implements ExternalActivityBehaviour {

    private static final long serialVersionUID = 1L;
    private String infoKind;
    private Logger log = LoggerFactory.getLogger(pl.compan.docusafe.parametrization.imgwwaw.jbpm.ERPProcessListener.class);


    @Override
    public void execute(ActivityExecution execution) throws Exception
    {
        try
        {
            infoKind.equals("liquidation");
            //
        }
        catch(Exception e)
        {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
            throws Exception {
        // TODO Auto-generated method stub

    }

}
