package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import java.util.List;
import java.util.Map;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 13-08-2013
 * docusafe
 * DemandCostDecision.java
 * 
 * Klasa sprawdza czy w zrodlach finansowania dla mpk wystapil okreslony koszt
 */
@SuppressWarnings("serial")
public class DemandCostDecision implements DecisionHandler {
    private final static Logger LOG = LoggerFactory.getLogger(DemandCostDecision.class);
    private static final String ZRODLO_FINANSOWANIA = "ZRODLO_FINANSOWANIA";
    private static final String PRZEDMIOT_ZAMOWIENIA = "PRZEDMIOT_ZAMOWIENIA";
    private static final String COST_TO_CHECK = "KO";
    Boolean costToCheck = false;
    
    public String decide(OpenExecution execution) {
    	try{
	    	Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			costToCheck = checkGeneralCost(doc, PRZEDMIOT_ZAMOWIENIA, ZRODLO_FINANSOWANIA, COST_TO_CHECK);
			
	    	if(costToCheck){
	    		return "generalCost";
	    	}else{
	    		return "noGeneralCost";
	    	}
    	
    	} catch (Exception e) {
		    LOG.error(e.getMessage(), e);
		    return "error";
	    }

    }
    
    /**
     * Metoda sprawdza czy w slowniku wielowartosciowym wystapilo zrodlo finansowania costToCheckCn
     *  
     * @param doc - dokument
     * @param dictionaryName - nazwa slownika wielowartosciowego
     * @param field - nazwa pola na slowniku wielowartosciowym
     * @param costToCheckCn - cn kosztu do sprawdzenia w polu field
     * @return true jesli znaleziono podana nazwe kosztu, false w przeciwnym wypadku
     * @throws EdmException
     */
    private Boolean checkGeneralCost(OfficeDocument doc, String dictionaryName, String field, String costToCheckCn) throws EdmException {
		FieldsManager fm = Document.find(doc.getId()).getFieldsManager();
		List<Map<String, Object>> mapa = (List<Map<String, Object>>) fm.getValue(dictionaryName);
	    if (mapa != null && !mapa.isEmpty()) {
		    for(Map<String, Object> row : mapa){
		        for(Map.Entry<String, Object> entry : row.entrySet()){
		        	if (entry.getKey().contains(field) && entry.getValue() != null) {
		        		EnumValues val = (EnumValues)entry.getValue();
		        		if(val.getSelectedValue().equals(costToCheckCn)){
			        				return true;
						}
		        	}
		        }
		    }
	    }
		return false;
    }
}
