/**
 * 
 */
package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 04-09-2013
 * docusafe
 * KierownikProjektuDecision.java
 */
public class KierownikProjektuDecision implements DecisionHandler {
	private static final long serialVersionUID = 1L;
	private final static Logger LOG = LoggerFactory.getLogger(KierownikProjektuDecision.class);
	private static final String PROJECT_ACCEPT_CN = "PROJECT_ACCEPT";
	
	@Override
	public String decide(OpenExecution openExecution) {
		try {
			LOG.info("kierownikProjektuDecision");
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            if (fm.getBoolean(PROJECT_ACCEPT_CN) == true) {
            	LOG.info("decision: akceptacja_kierownika_projektu");
                return "projectManagerAccept";
            } else {
            	LOG.info("decision: to_dynamic_join_1");
                return "dynamicJoin1";
            }

		} catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
            return "error";
		}
		
	}
}

