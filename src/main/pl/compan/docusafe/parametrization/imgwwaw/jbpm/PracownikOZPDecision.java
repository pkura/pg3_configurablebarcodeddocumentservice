package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.imgwwaw.FakturaZakupowaLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class PracownikOZPDecision implements DecisionHandler {

	private static final long serialVersionUID = 1L;
	private static final String PODSTAWA_ZAKUPU_CN = "PODSTAWA_ZAKUPU";
    private static final String UMOWA_CN = "UMOWA";

    private final static Logger LOG = LoggerFactory.getLogger(PracownikOZPDecision.class);

    public String decide(OpenExecution openExecution) {
        try {
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            boolean czyUmowa = fm.getBoolean(UMOWA_CN);
            if (czyUmowa) {
                return "wskazanie_os_realizujacej";
            } else {
                return "dzial_planowania_koordynacji";
            }
        } catch (EdmException ex) {
            LOG.error(ex.getMessage(), ex);
            return "error";
        }
    }
}
