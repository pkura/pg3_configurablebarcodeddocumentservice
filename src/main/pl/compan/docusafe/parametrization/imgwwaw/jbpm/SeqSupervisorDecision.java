package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import org.jfree.util.Log;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSContextOpener;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.ProfileInDivisionAssignmentHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.Profile;

import java.util.ArrayList;

public class SeqSupervisorDecision implements DecisionHandler {

    private static final String DIVISION_ID = "divisionId";
    private static final String PROFILE_ID = "profileId";

    public String decide(OpenExecution openExecution) {
        DSContextOpener contextOpener = null;
        try {
            contextOpener = DSContextOpener.openAsAdminIfNeed();
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();

            Long divisionId = (Long) openExecution.getVariable(DIVISION_ID);
            boolean initSeq = false;
            if (divisionId == null) { //pierwszy raz sekwencja
                initSeq = true;
                divisionId = Long.parseLong((String) fm.getKey("AUTHOR_DIVISION"));
                if (divisionId == null) throw new Exception("Not found division");
            }
            DSDivision division = DSDivision.findById(divisionId.intValue());
            if (!initSeq) {
                if (division.isRoot())
                    return endDecide("next", openExecution, null, null);
                division = division.getParent();
            }

            Profile profile = getProfile();
            Boolean supervisorsExists = null;
            while (supervisorsExists == null) {
                supervisorsExists = ProfileInDivisionAssignmentHandler.putUsersFromProfile(new ArrayList<String>(), division.getId(), profile.getId(), true, 0, true);
                if (!supervisorsExists) {
                    if (!division.isRoot()) { // todo tego if-a, while i if (!initSeq) mo�naby z��czy�
                        division = division.getParent();
                        supervisorsExists = null;
                    }
                }
            }

            if (supervisorsExists!=null && supervisorsExists)
                return endDecide("supervisors", openExecution, division.getId(), profile.getId());
            if (initSeq)
                return "error";
            return endDecide("next", openExecution, null, null);

        } catch (Exception e) {
            Log.error(e.getMessage());
            return "error";
        } finally {
            if (contextOpener != null)
                contextOpener.closeIfNeed();
        }
    }

    public String endDecide(String decision, OpenExecution openExecution, Long lastDivisionId, Long profileId) {
        openExecution.setVariable(DIVISION_ID, lastDivisionId); // zpaisanie ostatniego przeszukanego dzia�u
        openExecution.setVariable(PROFILE_ID, profileId); // zpaisanie ostatniego przeszukanego dzia�u
        return decision;
    }

    public Profile getProfile() throws Exception {

        String profileIdAdds = Docusafe.getAdditionProperty("profile.supervisor");
        int profileId = Docusafe.getIntAdditionPropertyOrDefault(profileIdAdds, -1);
        if (profileId < 0)
            throw new Exception("Profile has not been found by adds");
        return Profile.findById((long) profileId);
    }
}
