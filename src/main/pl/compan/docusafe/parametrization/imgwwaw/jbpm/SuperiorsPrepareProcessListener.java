package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.util.TextUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Klasa przygotowuj�ca list� prze�o�onych, do kt�rych ma zosta� skierowany proces.
 */
public class SuperiorsPrepareProcessListener implements ExternalActivityBehaviour {

    public static final List<String> guids = new LinkedList<String>();

    @Override
    public void execute(ActivityExecution execution) throws Exception {
        final List<Long> argSets = new LinkedList<Long>();

        boolean assigned = false;
        String supervisors = (String)execution.getVariable("supervisors");
        TextUtils.splitNamesToList(supervisors, ",");



        //TODO - POBRAC ZE STRUKTURY
        argSets.add(2L);
        argSets.add(4L);

        execution.setVariable("ids", argSets);
        execution.setVariable("count", argSets.size());
        execution.setVariable("count2", 2);
    }

    @Override
    public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception { }

}
