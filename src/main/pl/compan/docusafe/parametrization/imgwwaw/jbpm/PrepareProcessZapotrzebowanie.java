/**
 * 
 */
package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Sets;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class PrepareProcessZapotrzebowanie implements ExternalActivityBehaviour {
	
	private final Logger logger = LoggerFactory.getLogger(PrepareProcessZapotrzebowanie.class);
	
	public static final String CENTRA_KOSZTOW_TABLENAME = "dsg_zrodlo_finansowania";
	public static final String ZAPOTRZEBOWANIE_PRZEDMIOT_TABLENAME = "dsg_imgw_zapotrzebowanie_przedmiot";
	
	public static final String PRZEDMIOT_ZAPOTRZEBOWANIA_FIELD = "PRZEDMIOT_ZAPOTRZEBOWANIA";
	public static final String PRZEDMIOT_ZAPOTRZEBOWANIA_CENTRUMID = "PRZEDMIOT_ZAPOTRZEBOWANIA_CENTRUMID";
	
	
	public void execute(ActivityExecution execution) throws Exception {
		try {
			Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			
			Set<Long> supervisors = getSupervisorsFromSourcesOfFunding(doc);
			
	    	execution.setVariable("ids", supervisors);
	    	execution.setVariable("count", supervisors.size());
		} catch (EdmException e) {
			logger.error(e.getMessage(), e);
		}
	}

	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {

	}
    
    private Set<Long> getSupervisorsFromSourcesOfFunding(OfficeDocument document) throws EdmException {
    	Set<Long> supervisorIds = Sets.newHashSet();
    	FieldsManager fm = document.getFieldsManager();
    	
    	List<Map<String, Object>> valuesFromDwr = (List<Map<String, Object>>) fm.getValue(PRZEDMIOT_ZAPOTRZEBOWANIA_FIELD);
    	
    	Set<String> centrumIds = Sets.newHashSet();
    	if (valuesFromDwr != null && !valuesFromDwr.isEmpty()) {
    		for (Map<String, Object> row : valuesFromDwr) {
    			if (row.containsKey(PRZEDMIOT_ZAPOTRZEBOWANIA_CENTRUMID) && row.get(PRZEDMIOT_ZAPOTRZEBOWANIA_CENTRUMID) != null) {
    				String id = ((EnumValues) row.get(PRZEDMIOT_ZAPOTRZEBOWANIA_CENTRUMID)).getSelectedId();
    				centrumIds.add(id);
    			}
    		}
    	}
    	
		StringBuilder query = new StringBuilder();
		query.append("SELECT supervisors FROM ")
		.append(CENTRA_KOSZTOW_TABLENAME)
		.append(" WHERE id IN (:centrumIds)");
		
		List<String> supervisorsFromDatabase = (List<String>) DSApi.context().session().createSQLQuery(query.toString())
				.setParameterList("centrumIds", centrumIds)
				.list();
		
		if (supervisorsFromDatabase != null && !supervisorsFromDatabase.isEmpty()) {
			for (String item : supervisorsFromDatabase) {
				String[] idsTable = item.trim().split(",");
				if (idsTable != null && idsTable.length != 0) {
					for (String idStr : idsTable) {
						supervisorIds.add(Long.parseLong(idStr));
					}
				}
			}
		} else {
			throw new EdmException("Supervisors not found!");
		}
    	return supervisorIds;
    }
    
}
