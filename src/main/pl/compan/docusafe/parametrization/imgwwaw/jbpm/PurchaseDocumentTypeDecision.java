package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.imgwwaw.FakturaZakupowaLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class PurchaseDocumentTypeDecision implements DecisionHandler {

	private static final long serialVersionUID = 1L;
	private final static Logger LOG = LoggerFactory.getLogger(PurchaseDocumentTypeDecision.class);
	public static final String FZS = "FZS";
	
	@Override
	public String decide(OpenExecution openExecution) {
        try {
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            String typFaktury = fm.getEnumItemCn(FakturaZakupowaLogic.TYP_FAKTURY_CN);
            Boolean zwrot = fm.getBoolean(FakturaZakupowaLogic.ZWROT_KOSZTOW_CN);

            if (FZS.equals(typFaktury) || (zwrot != null && zwrot)) {
                return "other";
            } else {
                return "normal";
            }
        } catch (EdmException ex) {
            LOG.error(ex.getMessage(), ex);
            return "error";
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return "error";
        }
    }

}
