package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.process.InstantiationParameters;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.jbpm4.ActivitiesExtensionProcessView;
import pl.compan.docusafe.process.form.ActivityController;
import pl.compan.docusafe.process.form.SelectItem;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessInstance;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.parametrization.imgwwaw.WniosekZamPubLogic;
import pl.compan.docusafe.web.common.ExtendedRenderBean;

import java.util.ArrayList;
import java.util.List;

public class AcceptancesSelectProcessViewForZamPub extends ActivitiesExtensionProcessView {

    public AcceptancesSelectProcessViewForZamPub(InstantiationParameters ip) throws Exception {
        super(ip);
    }

    @Override
    public ExtendedRenderBean render(ProcessInstance pi, ProcessActionContext context) throws EdmException {
        return render((Jbpm4ProcessInstance) pi, context);
    }

    private ExtendedRenderBean render(Jbpm4ProcessInstance pi,
                                      ProcessActionContext context) throws EdmException {
        ExtendedRenderBean ret = super.render(pi, context);

        Long docId = Jbpm4ProcessLocator.documentIdForExtenedProcesssId(((Jbpm4ProcessInstance) pi).getProcessInstanceId());

        List<DocumentAcceptance> acceptances = DocumentAcceptance.find(docId);

        Field statusyProcesu = Document.find(docId).getDocumentKind().getFieldByCn("STATUS");
        Field statusyProcesuDH = Document.find(docId).getDocumentKind().getFieldByCn("STATUS_DH");

        String docStatus = Document.find(docId).getFieldsManager().getEnumItemCn("STATUS");
        String dhStatus = Document.find(docId).getFieldsManager().getEnumItemCn("STATUS_DH");


        List<SelectItem> selectItems = new ArrayList<SelectItem>();
        for (DocumentAcceptance itemAcceptance : acceptances) {
            itemAcceptance.initDescription();
            if (pi.getProcessDefinitionId().contains(WniosekZamPubLogic.DH_PROCESS_NAME)) {
                if (itemAcceptance.getAcceptanceCn().startsWith(WniosekZamPubLogic.DH_STATUS_PREFIX)) {
                    // nie pokazywanie na li�cie akceptacji dla takiego samego etapu, co aktualnie wykonywany
                    if (!itemAcceptance.getAcceptanceCn().equals(dhStatus))
                        selectItems.add(ActivityController.createSelectItem(itemAcceptance.getId(), itemAcceptance.getAsFirstnameLastname(), itemAcceptance.getUsername(), statusyProcesuDH.getEnumItemByCn(itemAcceptance.getAcceptanceCn()).getTitle(), itemAcceptance.getAcceptanceCn()));
                }
            } else {
                if (itemAcceptance.getAcceptanceCn().startsWith(WniosekZamPubLogic.DH_STATUS_PREFIX)) {
                    continue;
                }
                // nie pokazywanie na li�cie akceptacji dla takiego samego etapu, co aktualnie wykonywany
                if (!itemAcceptance.getAcceptanceCn().equals(docStatus))
                    selectItems.add(ActivityController.createSelectItem(itemAcceptance.getId(), itemAcceptance.getAsFirstnameLastname(), itemAcceptance.getUsername(), statusyProcesu.getEnumItemByCn(itemAcceptance.getAcceptanceCn()).getTitle(), itemAcceptance.getAcceptanceCn()));

            }
        }

        ret.putValue("selectItems", selectItems);

        return ret;

    }


}
