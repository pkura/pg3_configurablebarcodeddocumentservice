package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import com.google.common.collect.Sets;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.ValueNotFoundException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.imgwwaw.FakturaZakupowaLogic;
import pl.compan.docusafe.parametrization.imgwwaw.ImgwDocumentLogic;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.Kontrakt;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.ZrodlaFinansowaniaDict;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.*;

public class
        PrepareProcessListener implements ExternalActivityBehaviour {

    private static final Logger log = LoggerFactory.getLogger(PrepareProcessListener.class);

    private static final long serialVersionUID = 1L;
    private String dictionaryField;
    private String enumIdField;
    
    private static final String PRZEDMIOT_ZAMOWIENIA = "PRZEDMIOT_ZAMOWIENIA";
    private static final String PRZEDMIOT_ZAPOTRZEBOWANIA = "PRZEDMIOT_ZAPOTRZEBOWANIA";

    @Override
    public void execute(ActivityExecution execution) throws Exception {
        execution.removeVariable("correction");
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);
        final List<Long> argSets = new LinkedList<Long>();

        if ("ZRODLO_FINANSOWANIA".equals(dictionaryField)) {
            FieldsManager fm = doc.getFieldsManager();
            String typFaktury = fm.getEnumItemCn(FakturaZakupowaLogic.TYP_FAKTURY_CN);
            Boolean zwrot = fm.getBoolean(FakturaZakupowaLogic.ZWROT_KOSZTOW_CN);
            if (PurchaseDocumentTypeDecision.FZS.equals(typFaktury) || (zwrot != null && zwrot)) {
                if ("dpk_before_fork".equals(execution.getActivityName())) {
                    //dodajemy jeden element, aby w procesie fork mia� tylko jedno rozga��zienie
                    argSets.add(0l);
                } else {
                    String userName = fm.getEnumItemCn(FakturaZakupowaLogic.OSOBA_MERYTORYCZNA);
                    DSUser user = DSUser.findByUsername(userName);
                    for (DSDivision division : user.getOriginalDivisionsWithoutGroup()) {
                        argSets.add(division.getId());
                    }
                }
            } else if ("kierownik_komorki_before_fork".equals(execution.getActivityName())) {
                List<Long> ids = (List<Long>) doc.getFieldsManager().getKey(dictionaryField.toUpperCase());
                Set<String> divisionCodes = Sets.newHashSet();
//                Set<String> userNames = Sets.newTreeSet();
                ZrodlaFinansowaniaDict zrodloInstance = ZrodlaFinansowaniaDict.getInstance();
                try {
                    ForkAssignees.getDivisionCodesFromBudgets(ids, zrodloInstance, divisionCodes);
                } catch (EdmException e) {
                    log.error(e.getMessage(), e);
                    throw new EdmException("Nie uda�o si� przypisa� nikogo z wybranych kom�rek bud�etowych");
                }
                if (divisionCodes.isEmpty()) {
                    throw new EdmException("Nie uda�o si� przypisa� nikogo z wybranych kom�rek bud�etowych");
                }
                for (String code : divisionCodes) {
                    try {
                        DSDivision.findByCode(code);
                    } catch (DivisionNotFoundException e) {
                        log.error(e.getMessage(), e);
                        throw e;
                    }
                    argSets.add(DSDivision.findByCode(code).getId());
                }
            } else if ("kierownik_projektu_before_fork".equals(execution.getActivityName())) {
                List<Long> ids = (List<Long>) doc.getFieldsManager().getKey(dictionaryField.toUpperCase());
                Set<Long> userIds = new TreeSet<Long>();
                ZrodlaFinansowaniaDict zrodloInstance = ZrodlaFinansowaniaDict.getInstance();
                for (Long dictionaryId : ids) {
                    ZrodlaFinansowaniaDict zrodlo = zrodloInstance.find(dictionaryId);
                    Integer dcitionaryBudgetId = zrodlo.getBudget();
                    if (dcitionaryBudgetId != null) {
                        Integer kierownikId = zrodlo.getKierownik();
                        userIds.add(Long.valueOf(kierownikId));
                    }
                }
                argSets.addAll(userIds);
            } else if ("dpk_before_fork".equals(execution.getActivityName())) {
                List<Long> ids = (List<Long>) doc.getFieldsManager().getKey(dictionaryField.toUpperCase());
                Set<Long> userIds = new TreeSet<Long>();
                ZrodlaFinansowaniaDict zrodloInstance = ZrodlaFinansowaniaDict.getInstance();
                for (Long dictionaryId : ids) {
                    ZrodlaFinansowaniaDict zrodlo = zrodloInstance.find(dictionaryId);
                    Integer contractId = zrodlo.getContract();
                    if (contractId != null) {
                        Kontrakt kontrakt = Kontrakt.find(contractId);
                        Integer opiekunId = kontrakt.getOpiekunId();
                        DSUser user = null;
                        try {
                            user = DSUser.findById(Long.valueOf(opiekunId));
                        } catch (Exception e) {
                            throw new ValueNotFoundException("Nie znaleziono opiekuna projektu dla kontraktu " + kontrakt.getNazwa());
                        }
                        userIds.add(user.getId());
                    }
                }
                argSets.addAll(userIds);
            } else {
                List<Long> ids = (List<Long>) doc.getFieldsManager().getKey(dictionaryField.toUpperCase());
                for (Long id : ids) {
                    argSets.add(id);
                }
            }

        } else if(PRZEDMIOT_ZAMOWIENIA.equalsIgnoreCase(dictionaryField)){
        	
        	for (Integer zrodlo : getFinanceSourceIds(doc, dictionaryField, ImgwDocumentLogic.ZRODLO_FINANSOWANIA_CN)) {
    			argSets.add(zrodlo.longValue());
    		}
        } else if(PRZEDMIOT_ZAPOTRZEBOWANIA.equalsIgnoreCase(dictionaryField)){
        	
        	List<Long> ids = (List<Long>) doc.getFieldsManager().getKey(dictionaryField.toUpperCase());
            for (Long id : ids) {
                argSets.add(id);
            }
        }
        execution.setVariable("ids", argSets);
        execution.setVariable("count", argSets.size());
        execution.setVariable("count2", 2);
    }

    @Override
    public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
        // TODO Auto-generated method stub

    }
    
    /**Metoda zwraca liste ids z listy w slowniku
	 * @param doc
	 * @param dictionaryName - cn slownika
	 * @param enumField -  cn listy
	 * @return lista ids wybranych pozycji z listy w slowniku
	 * @throws EdmException
	 */
	private Set<Integer> getFinanceSourceIds(OfficeDocument doc, String dictionaryName, String enumField) throws EdmException {
		FieldsManager fm = Document.find(doc.getId()).getFieldsManager();
		Set<Integer> listaZrodel = new HashSet<Integer>();
		List<Map<String, Object>> mapa = (List<Map<String, Object>>) fm.getValue(dictionaryName);
	    if (mapa != null && !mapa.isEmpty()) {
		    for(Map<String, Object> row : mapa){
		        for(Map.Entry<String, Object> entry : row.entrySet()){
		        	if (entry.getKey().contains(enumField) && entry.getValue() != null) {
		        		EnumValues financeSource = (EnumValues)entry.getValue();
		        		listaZrodel.add(Integer.valueOf(financeSource.getSelectedId()));
		        	}
		        }
		    }
	    }
	    return listaZrodel;
    }

}
