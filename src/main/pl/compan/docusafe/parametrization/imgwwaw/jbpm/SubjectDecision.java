package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 16-08-2013
 * docusafe
 * SubjectDecision.java
 * 
 * Klasa sprawdza czy dla pola RODZAJ_PRZEDMIOTU zostal wybrany rodzaj inny niz "inne"
 */
@SuppressWarnings("serial")
public class SubjectDecision implements DecisionHandler {
    private final static Logger LOG = LoggerFactory.getLogger(SubjectDecision.class);
    public static final String RODZAJ_ZAMOWIENIA_CN = "RODZAJ_ZAMOWIENIA";
    private static final String RODZAJ_PRZEDMIOTU_CN = "RODZAJ_PRZEDMIOTU";
    
    public String decide(OpenExecution execution){
    	Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
    	try {
			FieldsManager fm = Document.find(docId).getFieldsManager();
			String rodzajZamowienia = fm.getEnumItemCn(RODZAJ_PRZEDMIOTU_CN);
				if(rodzajZamowienia.equals("INNE")){
					return "goToPersonIndication";
				}else{
					return "goToSubject";
				}
		} catch (DocumentNotFoundException e1) {
			e1.printStackTrace();
			return "error";
		} catch (DocumentLockedException e1) {
			e1.printStackTrace();
			return "error";
		} catch (AccessDeniedException e1) {
			e1.printStackTrace();
			return "error";
		} catch (EdmException e1) {
			e1.printStackTrace();
			return "error";
		}
    }
}
