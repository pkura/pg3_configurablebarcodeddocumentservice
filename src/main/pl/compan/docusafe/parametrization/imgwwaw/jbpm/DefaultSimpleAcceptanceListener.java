package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.office.OfficeDocument;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class DefaultSimpleAcceptanceListener extends SimpleAcceptanceListener {

    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        OfficeDocument doc = getOfficeDocument(execution);

        if(getDoNotInformLogic()==null){
            setDoNotInformLogic("true");
        }

        if(getDoNotUpdateAssignmentHistory()==null){
            setDoNotUpdateAssignmentHistory("false");
        }

        if (!isDoNotUpdateAssignmentHistory()){
            if (getAcception()==null){
                String statusCn = doc.getFieldsManager().getEnumItem("STATUS").getCn();
                setAcception(statusCn);
            }
        }

        executeTask(execution, doc);
    }
}
