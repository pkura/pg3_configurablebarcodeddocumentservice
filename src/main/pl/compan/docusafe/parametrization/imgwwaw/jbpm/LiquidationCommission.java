package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import pl.compan.docusafe.core.jbpm4.ProfilePrepareProcessListener;

/**
 * Klasa przygotowuj�ca list� os�b nale��cych do komisji likwidacyjnej
 */
public class LiquidationCommission extends ProfilePrepareProcessListener {
    @Override
    protected String getProfileIdAdds() throws Exception {
        return "profile.id.komisja.likwidacyjna";
    }
}