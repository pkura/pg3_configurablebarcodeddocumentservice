/**
 * 
 */
package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.users.DSUser;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class PrepareJoinForSinglePath implements ExternalActivityBehaviour {

	public void execute(ActivityExecution execution) throws Exception {
		String username = (String) execution.getVariable("user");
		DSUser user = DSUser.findByUsername(username);
		final List<Long> argSets = new LinkedList<Long>();
		if (user != null) {
			argSets.add(user.getId());
		} else {
			argSets.add(new Long(0));
		}
		execution.setVariable("ids", argSets);
		execution.setVariable("count", 1);
	}

	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {

	}

}
