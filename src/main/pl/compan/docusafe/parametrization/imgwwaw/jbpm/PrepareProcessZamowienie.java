/**
 * 
 */
package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.zbp.jbpm.PrepareProcessListener;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class PrepareProcessZamowienie implements ExternalActivityBehaviour {
	
	private final Logger logger = LoggerFactory.getLogger(PrepareProcessZamowienie.class);
	
	public static final String CENTRA_KOSZTOW_TABLENAME = "dsg_zrodlo_finansowania";
	
	private List<String> setValue = new ArrayList<String>();
	private Set<Long> argSets = new HashSet<Long>();
	
	public void execute(ActivityExecution execution) throws Exception {
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		
		/************ DO ZMIANY ********************/
		argSets.add(20L);
		argSets.add(21L);
		argSets.add(22L);
		/*******************************************/
		
    	execution.setVariable("ids", argSets);
    	execution.setVariable("count", argSets.size());
	}

	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {

	}
	
    /**
     * Metoda pobiera ze s�ownika MPK o podanej nazwie (parametr dictionaryName) id user�w odpowiedzialnych za dany MPK i
     * dodaje je do listy argSets (zmiennej instancyjnej)
     * @param doc
     * @param dictionaryName - cn s�ownika z MPKami
     * @throws EdmException
     */
    private void getUserIdsFromMPK(OfficeDocument doc, String dictionaryName) throws EdmException {
    	setValue.clear();
	    List<Long> dictionaryIds = (List<Long>) doc.getFieldsManager().getKey(dictionaryName);
	    if (dictionaryIds != null && !dictionaryIds.isEmpty()) {
		    for(Long ids : dictionaryIds){
		    	CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(ids);
		        String centrumCn = DataBaseEnumField.getEnumItemForTable(CENTRA_KOSZTOW_TABLENAME, mpk.getCentrumId()).getCn();
		        List<String> users = new ArrayList<String>();
		        
		        for (AcceptanceCondition accept : AcceptanceCondition.find(centrumCn, null))
				{
					if (accept.getUsername() != null && !users.contains(accept.getUsername()))
						if(!setValue.contains(accept.getUsername())){
							users.add(accept.getUsername());
							if (!argSets.contains(DSUser.findByUsername(accept.getUsername()).getId())) {
								argSets.add(DSUser.findByUsername(accept.getUsername()).getId());
							}
							setValue.add(accept.getUsername());
						}
				}
		    }
	    }
    }
}
