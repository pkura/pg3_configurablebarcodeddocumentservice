package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.DictionaryUtils;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.FieldValue;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.imgwwaw.FakturaZakupowaLogic;
import pl.compan.docusafe.parametrization.imgwwaw.ImgwDocumentLogic;
import pl.compan.docusafe.parametrization.imgwwaw.ZapotrzebowanieLogic;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.Budget;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.ZrodlaFinansowaniaDict;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Sets;

public class ForkAssigneeByDicCn implements ExternalActivityBehaviour {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ForkAssigneeByDicCn.class);
    public static final String KIEROWNICY_KOMOREK = "KIEROWNICY_KOMOREK";
    public static final String KIEROWNICY_KOMOREK_OTHER = "KIEROWNICY_KOMOREK_OTHER";
    public static final String DYREKTORZY = "DYREKTORZY";
    public static final String DYREKTORZY_OTHER = "DYREKTORZY_OTHER";
    public static final String PROJEKT = "PROJEKT";

    private String dictionaryCn;
    
    private String forkPhase;

    @Override
    public void execute(ActivityExecution execution) throws Exception {
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        boolean isOpened = true;
        if (!DSApi.isContextOpen()) {
            isOpened = false;
            DSApi.openAdmin();
        }
        try {
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            
            List<Long> dictionaryIds = (List<Long>) fm.getKey(dictionaryCn);
            if (dictionaryIds != null) {
                ZrodlaFinansowaniaDict zrodloInstance = ZrodlaFinansowaniaDict.getInstance();
                Set<String> divisionCodes = Sets.newTreeSet();

               if (KIEROWNICY_KOMOREK.equals(forkPhase)) {
                    getDivisionCodefromBudgets(dictionaryIds, zrodloInstance, divisionCodes);
                } else if (DYREKTORZY.equalsIgnoreCase(forkPhase)) {
                	getDivisionCodefromBudgets(dictionaryIds, zrodloInstance, divisionCodes);
                } else if (PROJEKT.equalsIgnoreCase(forkPhase)) {
                	getDivisionCodefromBudgets(dictionaryIds, zrodloInstance, divisionCodes);
                }

                if (divisionCodes.isEmpty()) {
                    throw new EdmException("Nie uda�o si� pobra� �adnej kom�rki organizacyjnej z wybranych �r�de� finansowych");
                }
                log.error("DocId: " + docId + " DivisionCodes:");
                for (String divisionCode : divisionCodes) {
                    log.error(divisionCode);
                }
                execution.setVariable("divisionCodes", new ArrayList<String>(divisionCodes));
                execution.setVariable("count", divisionCodes.size());
            }
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        } finally {
            if (DSApi.isContextOpen() && !isOpened)
                DSApi.close();
        }
    }

    private static void getDivisionCodefromBudgets(List<Long> dictionaryIds, ZrodlaFinansowaniaDict zrodloInstance, Set<String> divisionCodes) throws EdmException {
        for (Long dictionaryId : dictionaryIds) {
            ZrodlaFinansowaniaDict zrodlo = zrodloInstance.find(dictionaryId);
            Integer dcitionaryBudgetId = zrodlo.getBudget();
            if (dcitionaryBudgetId != null) {
                Long budzetId = Long.valueOf(DataBaseEnumField.getEnumItemForTable(ImgwDocumentLogic.BUDGET_VIEW_TABLENAME, dcitionaryBudgetId).getRefValue());
                Budget budget = Budget.find(budzetId);
                divisionCodes.add(budget.getKomBudIdn());
            }
        }
    }

    @Override
    public void signal(ActivityExecution activityExecution, String s, Map<String, ?> stringMap) throws Exception {

    }
}
