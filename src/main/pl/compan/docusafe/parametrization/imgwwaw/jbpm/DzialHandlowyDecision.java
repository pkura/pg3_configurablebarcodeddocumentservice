package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class DzialHandlowyDecision implements DecisionHandler {

	private static final long serialVersionUID = 1L;
	private final static Logger LOG = LoggerFactory.getLogger(DzialHandlowyDecision.class);
	private static final String TERMIN_PLATNOSCI_UMOWA_CN = "TERMIN_PLATNOSCI_UMOWA";
	private static final String WYKONANO_W_TERMINIE_CN = "WYKONANO_W_TERMINIE";
	private static final String ZGODNIE_Z_UMOWA_CN = "ZGODNIE_Z_UMOWA";
	
	@Override
	public String decide(OpenExecution openExecution) {
		try {
			LOG.info("dzialHandlowyDecision");
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            //fm.getBoolean(TERMIN_PLATNOSCI_UMOWA_CN) 
            if( !fm.getBoolean(WYKONANO_W_TERMINIE_CN)
            		|| !fm.getBoolean(ZGODNIE_Z_UMOWA_CN) ) {
            	return "osoba_merytoryczna_view";
            } else {
            	return "dzial_planowania_koordynacji";
            }
            
            
		} catch (EdmException ex) {
            LOG.error(ex.getMessage(), ex);
            return "error";
        }
	}

}
