//package pl.compan.docusafe.parametrization.imgwwaw.jbpm;
//
//import org.jbpm.api.model.OpenExecution;
//import org.jbpm.api.task.Assignable;
//import org.jbpm.api.task.AssignmentHandler;
//import org.jfree.util.Log;
//import pl.compan.docusafe.core.DSContextOpener;
//import pl.compan.docusafe.core.EdmException;
//import pl.compan.docusafe.core.jbpm4.AssigneeHandlerUtils;
//import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
//import pl.compan.docusafe.core.office.OfficeDocument;
//import pl.compan.docusafe.util.TextUtils;
//
//import java.util.ArrayList;
//
//public class AssignmentToSupervisorInDivision implements AssignmentHandler
//{
//    public static final String DIVISION_ID = "divisionId";
//
//    private String divisionIdVariable = DIVISION_ID;
//
//    @Override
//    public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
//
//        DSContextOpener contextOpener = DSContextOpener.openAsAdminIfNeed();
//        try {
//            Long divisionId = (Long)openExecution.getVariable(divisionIdVariable);
//            ArrayList<String> usernames = TextUtils.splitNamesToList(usernamesInLine, ",");
//
//            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
//            OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId, false);
//            AssigneeHandlerUtils.assignByUsernames(doc, assignable, openExecution, usernames);
//
//        } catch (EdmException e) {
//            Log.error(e.getMessage());
//            throw e;
//        } finally {
//            contextOpener.closeIfNeed();
//        }
//    }
//
//}
