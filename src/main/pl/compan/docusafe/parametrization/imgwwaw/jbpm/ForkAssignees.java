package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import com.google.common.collect.Sets;
import org.apache.commons.dbutils.DbUtils;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.ProfileNotAssignedException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.Profile;
import pl.compan.docusafe.parametrization.imgwwaw.FakturaZakupowaLogic;
import pl.compan.docusafe.parametrization.imgwwaw.ImgwDocumentLogic;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.Budget;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.ZrodlaFinansowaniaDict;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by user on 27.01.14.
 */
public class ForkAssignees implements ExternalActivityBehaviour {

    private static final Logger log = LoggerFactory.getLogger(ForkAssignees.class);
    public static final String KIEROWNICY_KOMOREK = "KIEROWNICY_KOMOREK";
    public static final String KIEROWNICY_KOMOREK_OTHER = "KIEROWNICY_KOMOREK_OTHER";
    public static final String DYREKTORZY = "DYREKTORZY";
    public static final String DYREKTORZY_OTHER = "DYREKTORZY_OTHER";

    private String forkPhase;

    @Override
    public void execute(ActivityExecution execution) throws Exception {
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        boolean isOpened = true;
        if (!DSApi.isContextOpen()) {
            isOpened = false;
            DSApi.openAdmin();
        }
        try {
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();

            List<Long> dictionaryIds = (List<Long>) fm.getKey(ImgwDocumentLogic.ZRODLO_FINANSOWANIA_CN);
            if (dictionaryIds != null) {
                ZrodlaFinansowaniaDict zrodloInstance = ZrodlaFinansowaniaDict.getInstance();
                Set<String> divisionCodes = Sets.newHashSet();
//                Set<String> userNames = Sets.newTreeSet();

                if (KIEROWNICY_KOMOREK_OTHER.equals(forkPhase) || DYREKTORZY_OTHER.equals(forkPhase)) {
                    String userName = fm.getEnumItemCn(FakturaZakupowaLogic.OSOBA_MERYTORYCZNA);
                    DSUser user = DSUser.findByUsername(userName);
                    for (DSDivision division : user.getOriginalDivisionsWithoutGroup()) {
                        if (division.getCode().split("-").length > 1) {
                            divisionCodes.add(division.getCode());
                        }
                    }
                } else if (KIEROWNICY_KOMOREK.equals(forkPhase)) {
                    Set<String> divisionCodesTemp = Sets.newHashSet();
                    getDivisionCodesFromBudgets(dictionaryIds, zrodloInstance, divisionCodesTemp);
                    getManagersFromDivisionCodes(divisionCodesTemp, divisionCodes);
                } else if (DYREKTORZY.equals(forkPhase)) {
                    Set<String> divisionCodesTemp = Sets.newHashSet();
                    getDivisionCodesFromBudgets(dictionaryIds, zrodloInstance, divisionCodesTemp);
                    getDirectorsFromDivisionCodes(divisionCodesTemp, divisionCodes);
                } else {
                    String typFaktury = fm.getEnumItemCn(FakturaZakupowaLogic.TYP_FAKTURY_CN);
                    Boolean zwrot = fm.getBoolean(FakturaZakupowaLogic.ZWROT_KOSZTOW_CN);
                    if (PurchaseDocumentTypeDecision.FZS.equals(typFaktury) || (zwrot != null && zwrot)) {
                        String userName = fm.getEnumItemCn(FakturaZakupowaLogic.OSOBA_MERYTORYCZNA);
                        DSUser user = DSUser.findByUsername(userName);
                        for (DSDivision division : user.getOriginalDivisionsWithoutGroup()) {
                            divisionCodes.add(division.getCode());
                        }
                    } else {
                        getDivisionCodesFromBudgets(dictionaryIds, zrodloInstance, divisionCodes);
                    }
                }

                if (divisionCodes.isEmpty()) {
                    throw new EdmException("Nie uda�o si� przypisa� nikogo z wybranych kom�rek bud�etowych");
                }
                log.error("DocId: " + docId + " DivisionCodes:");
                for (String divisionCode : divisionCodes) {
                    log.error(divisionCode);
                }
//                execution.setVariable("userNames", new ArrayList<String>(userNames));
                execution.setVariable("divisionCodes", new ArrayList<String>(divisionCodes));
                execution.setVariable("count", divisionCodes.size());
            }
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            throw e;
        } finally {
            if (DSApi.isContextOpen() && !isOpened)
                DSApi.close();
        }
    }

    /**
     * Metoda dodaje do divisionCodes (Set przekazany w parametrze) kody kom�rek organizacyjnych wybranych w �r�d�ach finansowych
     * @param dictionaryIds
     * @param zrodloInstance
     * @param divisionCodes
     * @throws EdmException
     */
    protected static void getDivisionCodesFromBudgets(List<Long> dictionaryIds, ZrodlaFinansowaniaDict zrodloInstance, Set<String> divisionCodes) throws EdmException {
        for (Long dictionaryId : dictionaryIds) {
            ZrodlaFinansowaniaDict zrodlo = zrodloInstance.find(dictionaryId);
            Integer dcitionaryBudgetId = zrodlo.getBudget();
            if (dcitionaryBudgetId != null) {
                Long budzetId = Long.valueOf(DataBaseEnumField.getEnumItemForTable(ImgwDocumentLogic.BUDGET_VIEW_TABLENAME, dcitionaryBudgetId).getRefValue());
                Budget budget = Budget.find(budzetId);
                divisionCodes.add(budget.getKomBudIdn());
            }
        }
    }

    /**
     * Uzupe�niamy zbi�r kod�w kom�rek organizacyjnych, dla kt�rych istniej� kierownicy, na podstawie kod�w wybranych na �r�d�ach finansowych
     * w celu unikni�cia sytuacji, gdy dwie r�ne kom�rki bud�etowe maj� tych samych kierownik�w.
     * @param divisionCodesTemp kody kom�rek organizacyjnych, wybranych w �r�d�ach finansowych
     * @param directDivisionCodes kody kom�rek organizacyjnych, dla kt�rych istniej� przypisani kierownicy
     * @throws EdmException
     */
    protected static void getManagersFromDivisionCodes(Set<String> divisionCodesTemp, Set<String> directDivisionCodes) throws EdmException {

        Integer profileId = 5;
        if (Docusafe.getAdditionProperty("profile.id.kierownik.komorki") != null) {
            try {
                profileId = Integer.valueOf(Docusafe.getAdditionProperty("profile.id.kierownik.komorki"));
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        for (String divCode : divisionCodesTemp) {
            String guid = DSDivision.findByCode(divCode).getGuid();
            getDirectDivisionCode(directDivisionCodes, guid, profileId, divCode, 1);
        }
    }

    /**
     * Uzupe�niamy zbi�r kod�w kom�rek organizacyjnych, dla kt�rych istniej� dyrektorzy pionu, na podstawie kod�w wybranych na �r�d�ach finansowych
     * w celu unikni�cia sytuacji, gdy dwie r�ne kom�rki bud�etowe maj� tych samych dyrektor�w.
     * @param divisionCodesTemp kody kom�rek organizacyjnych, wybranych w �r�d�ach finansowych
     * @param directDivisionCodes kody kom�rek organizacyjnych, dla kt�rych istniej� przypisani dyrektorzy pionu
     * @throws EdmException
     */
    protected static void getDirectorsFromDivisionCodes(Set<String> divisionCodesTemp, Set<String> directDivisionCodes) throws EdmException {

        Integer profileId = 4;
        if (Docusafe.getAdditionProperty("profile.id.dyrektor.pionu") != null) {
            try {
                profileId = Integer.valueOf(Docusafe.getAdditionProperty("profile.id.dyrektor.pionu"));
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        for (String divCode : divisionCodesTemp) {
            String guid = DSDivision.findByCode(divCode).getGuid();
            getDirectDivisionCode(directDivisionCodes, guid, profileId, divCode, 0);
        }
    }

    /**
     *
     * @param directDivisionCodes kody bezpo�rednich kom�rek organizacyjnych, dla kt�rych istniej� przypisani u�ytkownicy o podanym profilu
     * @param divGuid GUID dzia�u, dla kt�rego szykamy u�ytkownik�w
     * @param profileId ID profilu, dla kt�rego szukamy u�ytkownik�w
     * @param divCodeForExceptionMessage kod dzia�u ze �r�d�a finansowania, potrzebny do komunikatu
     * @param count liczba ga��zi drzewa struktury organizacyjnej, kt�re chcemy przeszuka�, w g�r� od dzia�u przekazanego w parametrze 'divGuid'.
     *              Podajemy 0 je�li chcemy przeszuka� struktur� organizacyjn� a� do dzia�u g��wnego.
     * @throws EdmException
     */
    private static void getDirectDivisionCode(Set<String> directDivisionCodes, String divGuid, Integer profileId, String divCodeForExceptionMessage, int count) throws EdmException {
        String sqlQuery = "select u.NAME " +
                "from DS_DIVISION d " +
                "join DS_USER_TO_DIVISION ud on d.ID = ud.DIVISION_ID " +
                "join DS_USER u on ud.USER_ID = u.ID " +
                "join ds_profile_to_user pu on u.ID = pu.user_id " +
                "where pu.profile_id = ? and d.GUID = ?";

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DSApi.context().prepareStatement(sqlQuery);
            ps.setInt(1, profileId);
            ps.setString(2, divGuid);
            rs = ps.executeQuery();
            while (rs.next()) {
                directDivisionCodes.add(DSDivision.find(divGuid).getCode());
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new EdmException("B��d przy wyszukiwaniu " +
                    Profile.findById(Long.valueOf(profileId)).getName() + " dla dzia�u " + DSDivision.find(divGuid).getCode());
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            throw new EdmException("B��d przy wyszukiwaniu " +
                    Profile.findById(Long.valueOf(profileId)).getName() + " dla dzia�u " + DSDivision.find(divGuid).getCode());
        } finally {
            DSApi.context().closeStatement(ps);
            DbUtils.closeQuietly(rs);
        }

        if (!directDivisionCodes.isEmpty()) {
            return;
        }

        try {
            if (profileId.equals(Integer.valueOf(Docusafe.getAdditionProperty("profile.id.dyrektor.pionu")))) {
                DSDivision div = DSDivision.find(divGuid);
                div = div.getParent();
                if (div == null || div.isRoot()) {
                    throw new ProfileNotAssignedException("Brak przypisanego u�ytkownika do profilu " +
                            Profile.findById(Long.valueOf(profileId)).getName() + " dla dzia�u " + divCodeForExceptionMessage);
                }
                getDirectDivisionCode(directDivisionCodes, div.getGuid(), profileId, divCodeForExceptionMessage, 0);
            } else if (profileId.equals(Integer.valueOf(Docusafe.getAdditionProperty("profile.id.kierownik.komorki")))) {
                DSDivision div = DSDivision.find(divGuid);
                div = div.getParent();
                if (div == null || div.isRoot() || count == 0) {
                    throw new ProfileNotAssignedException("Brak przypisanego u�ytkownika do profilu " +
                            Profile.findById(Long.valueOf(profileId)).getName() + " dla dzia�u " + divCodeForExceptionMessage);
                }
                count--;
                getDirectDivisionCode(directDivisionCodes, div.getGuid(), profileId, divCodeForExceptionMessage, count);
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public void signal(ActivityExecution activityExecution, String s, Map<String, ?> stringMap) throws Exception {

    }
}
