/**
 * 
 */
package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import pl.compan.docusafe.core.jbpm4.ProfilePrepareProcessListener;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 08-02-2014
 * ds_trunk_2014
 * PrepareProcessWniosekSzkoleniowy.java
 */
public class PrepareProcessForProfile extends ProfilePrepareProcessListener {

	String adds;
	
	@Override
	protected String getProfileIdAdds() throws Exception {
		return adds;
	}
	
}
