package pl.compan.docusafe.parametrization.imgwwaw.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.imgwwaw.FakturaZakupowaLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class KierownikDecision implements DecisionHandler {

	private static final long serialVersionUID = 1L;
	private final static Logger LOG = LoggerFactory.getLogger(KierownikDecision.class);
	private static final String INNE_CN = "INNE";
	
	@Override
	public String decide(OpenExecution openExecution) {
		try {
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            if (!fm.getEnumItemCn(FakturaZakupowaLogic.RODZAJ_PRZEDMIOTU_CN).equals(INNE_CN)) {
                return "akceptacja_wg_rodzaju_przedmiotu";
            } else {
                return "akceptacja_ozp";
            }
		} catch (EdmException ex) {
            LOG.error(ex.getMessage(), ex);
            return "error";
        }
	}

}
