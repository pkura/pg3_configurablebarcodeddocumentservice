package pl.compan.docusafe.parametrization.imgwwaw;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Constants;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.AcceptanceDivisionImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

public class PismoWychodzaceLogic extends AbstractDocumentLogic{
	private final static Logger log = LoggerFactory.getLogger(PismoWychodzaceLogic.class);
	private static final StringManager sm = StringManager.getManager(Constants.Package);
	
	public static final String SPOSOB_WYSLANIA_FIELD = "SPOSOB_WYSLANIA";
	public static final String NR_LISTU_POLECONEGO_FIELD = "NR_LISTU_POLECONEGO";
	public static final String DWR_RECIPIENT = "DWR_RECIPIENT";
	public static final String RECIPIENT_REGON = "RECIPIENT_REGON";
	public static final String RECIPIENT_NIP = "RECIPIENT_NIP";
	public static final String RECIPIENT_PHONENUMBER = "RECIPIENT_PHONENUMBER";
	
	public static final String OFFICE = "office";
	public static final String KANCELARIA_K = "K";
	public static final String KANCELARIA_G = "G";
	public static final String KANCELARIA_W = "W";
	public static final String KANCELARIA_O = "O";
	public static final String KANCELARIA_T = "T";
	
	public static final Integer KRAJOWY_POLECONY_EKONOMICZNY = 3;
	public static final Integer KRAJOWY_POLECONY_PRIORYTET = 4;
	public static final Integer ZAGRANICZNY_POLECONY_EKONOMICZNY = 7;
	public static final Integer ZAGRANICZNY_POLECONY_PRIORYTET = 8;
	
	private String addToHistory = "true";
	
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		Map<String, Object> toReload = Maps.newHashMap();
		for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
		{
			if (f.getDefaultValue() != null)
			{
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
			}
		}
		fm.reloadValues(toReload);
	}
	
	@Override
	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
		
	}
	
	@Override
	public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException {
		if (values.containsKey(SPOSOB_WYSLANIA_FIELD) && values.get(SPOSOB_WYSLANIA_FIELD) != null) {
			Integer wayOfSend = (Integer) values.get(SPOSOB_WYSLANIA_FIELD);
			if (wayOfSend.equals(KRAJOWY_POLECONY_EKONOMICZNY) || wayOfSend.equals(KRAJOWY_POLECONY_PRIORYTET)) {
				if (values.containsKey(NR_LISTU_POLECONEGO_FIELD) && values.get(NR_LISTU_POLECONEGO_FIELD) != null) {
					String numberOfLetter = (String) values.get(NR_LISTU_POLECONEGO_FIELD);
					if (!numberOfLetter.matches("[0-9]+")) {
						throw new EdmException(sm.getString("NormalOutInvalidNumberOfLetter"));
					}
				}
			}
		}
	}
	
	public TaskListParams getTaskListParams(DocumentKind dockind, long id, TaskSnapshot task) throws EdmException {
        TaskListParams params = super.getTaskListParams(dockind, id, task);
        try{
        	 OfficeDocument officeDocument = OfficeDocument.find(id);
        	 
             if (officeDocument != null && officeDocument.getSender() != null) {
                 String source = officeDocument.getSender().getShortSummary();
                 params.setAttribute(source, 1);
             }
        } catch (Exception e){
        	log.error(e.getMessage(), e);
        }
        return params;
    }
	
	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
		if (values.containsKey(DWR_RECIPIENT) && values.get(DWR_RECIPIENT) != null) {
			Map<String, FieldData> valuesFromRecipient = values.get(DWR_RECIPIENT).getDictionaryData();
			if (valuesFromRecipient.containsKey(RECIPIENT_REGON) && StringUtils.isNotBlank(valuesFromRecipient.get(RECIPIENT_REGON).getStringData())) {
				String regon = valuesFromRecipient.get(RECIPIENT_REGON).getStringData();
				if (!regon.matches("[0-9]+")) {
					pl.compan.docusafe.core.dockinds.dwr.Field msg = new Field("messageField", sm.getString("NormalOutInvalidRegon"), Field.Type.BOOLEAN);
					values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
					return msg;
				}
			}
			if (valuesFromRecipient.containsKey(RECIPIENT_NIP) && StringUtils.isNotBlank(valuesFromRecipient.get(RECIPIENT_NIP).getStringData())) {
				String nip = valuesFromRecipient.get(RECIPIENT_NIP).getStringData();
				if (!nip.matches("[0-9]+")) {
					pl.compan.docusafe.core.dockinds.dwr.Field msg = new Field("messageField", sm.getString("NormalOutInvalidNip"), Field.Type.BOOLEAN);
					values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
					return msg;
				}
			}
			if (valuesFromRecipient.containsKey(RECIPIENT_PHONENUMBER) && StringUtils.isNotBlank(valuesFromRecipient.get(RECIPIENT_PHONENUMBER).getStringData())) {
				String phoneNumber = valuesFromRecipient.get(RECIPIENT_PHONENUMBER).getStringData();
				if (!phoneNumber.matches("[0-9]+")) {
					pl.compan.docusafe.core.dockinds.dwr.Field msg = new Field("messageField", sm.getString("NormalOutInvalidPhoneNumber"), Field.Type.BOOLEAN);
					values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
					return msg;
				}
			}
		}
		return null;
	}
	
	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {
		//ustawienie documentdate. Potrzebne do generowania raportow (np. szczegolowy raport pism wychodzacych korzysta z tego pola)
		if (document instanceof OutOfficeDocument) {
			if (((OutOfficeDocument) document).getDocumentDate() == null) {
				((OutOfficeDocument) document).setDocumentDate(new Date());
			}
		}
	}

	public void onStartProcess(OfficeDocument document,ActionEvent event)
    {
    	try
		{
//    		Map<String, Object> map = Maps.newHashMap();
//			map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
//    		log.info("onStartProcess");
////    		document.getDocumentKind().setOnly(document.getId(), ImmutableMap.of(DATA_PISMA_CN, new Date()));
//    		Role sekretariat = Role.findByName("sekretariat");
//    		if(sekretariat != null) {
//    			for(String x : sekretariat.getUsernames() ) {
//    				log.info("uzytkownik z sekretariatu: " + x.toString());
//    			}		
//    		}
//    		
//			//map.put(ASSIGN_DIVISION_GUID_PARAM, DSDivision.ROOT_GUID);
//			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
    		log.info("onStartProcess");
//    		document.getDocumentKind().setOnly(document.getId(), ImmutableMap.of(DATA_WPLYWU_CN, new Date()));
    		
			//map.put(ASSIGN_DIVISION_GUID_PARAM, DSDivision.ROOT_GUID);
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch")) {
				DSApi.context().watch(URN.create(document));
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
    }
	
	 @Override
		public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
	    	
			boolean assigned = false;
			Long docId = doc.getId();
			log.error(null, "Kod akceptacji acceptationCn = " + acceptationCn);
			/**
		     * Akceptacje dla pisma wychodz帷ego
		     */
	        try {
	        	//Akceptacje kierownikow komorek w zaleznosci od zrodel finansowania
	        	if (OFFICE.equals(acceptationCn)) {
	        		DSUser dsUser = DSUser.getLoggedInUserSafely();
	    			String divisionCode = dsUser.getDivisions()[0].getCode();
	    			if(divisionCode == null){
	    				log.error(null, "Kod dywizji jest nullem");
	    			} else {
	    				log.error(null, "Pobrana warto嗆 kodu dywizji: " + divisionCode);
	    			}
	    			String code = divisionCode.split("-| ")[0];
	    			if(code == null){
	    				log.error(null, "Null otrzymany po splicie");
	    			} else {
	    				log.error(null, "Pobrana warto嗆 po splicie: " + code);
	    			}
	    			if(code != null && code.length() > 0){
	    				if(code.equals(KANCELARIA_K)){
	    					code = "kancelaria_k";
	    				} else if(code.equals(KANCELARIA_G)){
	    					code = "kancelaria_g";
	    				} else if(code.equals(KANCELARIA_W)) { 
	    					code = "kancelaria_w";
	    				} else if(code.equals(KANCELARIA_O)) {
	    					code = "kancelaria_o";
	    				} else if(code.equals(KANCELARIA_T)) {
	    					code = "kancelaria_t";
	    				} else {
	    					code = "kancelaria";
	    				}
	    			} else {
	    				code = "kancelaria";
	    			}
	    			log.error(null, "Dokument przypisany po akceptacji: " + code);
	    				
	    			List<String> users = new ArrayList<String>();
	    			List<String> divisions = new ArrayList<String>();
	    				
	    			if (AcceptanceDivisionImpl.findByCode(code) != null && AcceptanceDivisionImpl.findByCode(code).getOgolna() == 1){
	    				fieldCnValue = null;
	    			} else if (fieldCnValue != null) {
	    				fieldCnValue = doc.getFieldsManager().getEnumItem(fieldCnValue).getCn();
	    			}
	    				

	    			for (AcceptanceCondition accept : AcceptanceCondition.find(code, fieldCnValue))
	    			{
	    				if (accept.getUsername() != null && !users.contains(accept.getUsername()))
	    					users.add(accept.getUsername());
	    				if (accept.getDivisionGuid() != null && !divisions.contains(accept.getDivisionGuid()))
	    					divisions.add(accept.getDivisionGuid());
	    			}
	    			for (String user : users)
	    			{
	    				assignable.addCandidateUser(user);
	    				log.info("For accept " + code + " and fieldValue " + fieldCnValue + " - candidateUser: " + user);
	    				if (Boolean.parseBoolean(addToHistory))
	    					AssigneeHandler.addToHistory(openExecution, doc, user, null);
	    				assigned = true;
	    			}
	    			for (String division : divisions)
	    			{
	    				assignable.addCandidateGroup(division);
	    				log.info("For accept " + code + " and fieldValue " + fieldCnValue + " - candidateGroup: " + division);
	    				if (Boolean.parseBoolean(addToHistory))
	    					AssigneeHandler.addToHistory(openExecution, doc, null, division);
	    				assigned = true;
	    			}
	    			
	            } 
	            return assigned;
	        } catch (EdmException e) {
	            log.error(e.getMessage(), e);
	            return assigned;
	        }
			
	 }
}
