package pl.compan.docusafe.parametrization.imgwwaw;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.AssigneeHandlerUtils;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

public class WniosekSzkoleniowyLogic extends ImgwDocumentLogic {
    protected static Logger log = LoggerFactory.getLogger(WniosekSzkoleniowyLogic.class);
    private static WniosekSzkoleniowyLogic instance;
    
    public static final String WNIOSEK_SZKOLENIOWY = "WNIOSEK_SZKOLENIOWY";
    public static final String SYMBOL_KOMORKI_CN = "SYMBOL_KOMORKI";
    public static final String SUPERVISOR_ACCEPT = "supervisor";
    public static final String MAIN_DIRECTOR = "main_director";
    public static final String SENDER_HERE = "SENDER_HERE";
    public static final String DWR_PROCENT_FINANSOWANIA = "DWR_PROCENT_FINANSOWANIA";
    
    protected static final EnumValues EMPTY_ENUM_VALUE = new EnumValues(new HashMap<String, String>(), new ArrayList<String>());
    
    public static synchronized WniosekSzkoleniowyLogic getInstance() {
        if (instance == null)
            instance = new WniosekSzkoleniowyLogic();
        return instance;
    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
    	Map<String, Object> toReload = Maps.newHashMap();
		if (WNIOSEK_SZKOLENIOWY.equalsIgnoreCase(fm.getDocumentKind().getCn())) {
			Long inicjatorId = DSApi.context().getDSUser().getId();
			DSDivision[] userDivs = DSUser.findById(inicjatorId).getDivisionsWithoutGroup();
			String symbolKomorki = "";
			if(userDivs.length > 0){
				symbolKomorki = userDivs[0].getCode();
			}
			toReload.put(SYMBOL_KOMORKI_CN, symbolKomorki);
		}
		if (WNIOSEK_SZKOLENIOWY.equalsIgnoreCase(fm.getDocumentKind().getCn()) && DSApi.context().getDSUser().getDivisionsWithoutGroup().length>0) {
			String sender = "u:"+DSApi.context().getDSUser().getName();
			sender+=(";d:"+DSApi.context().getDSUser().getDivisionsWithoutGroup()[0].getGuid());
			toReload.put("SENDER_HERE", sender);	
		}
		fm.reloadValues(toReload);
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
        document.setFolder(folder);

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        document.setTitle("" + (String) fm.getValue(fm.getMainFieldCn()));
        document.setDescription("" + (String) fm.getValue(fm.getMainFieldCn()));

    }

    public int getPermissionPolicyMode() {
        return PermissionManager.NORMAL_POLICY;
    }

    @Override
    public boolean searchCheckPermissions(Map<String, Object> values) {
        return false;
    }

    public void documentPermissions(Document document) throws EdmException {
    }

    public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
    {    	
    	
    	
    	if(values.get(DWR_PROCENT_FINANSOWANIA).getData() != null){
			Integer procentFinansowania = values.get(DWR_PROCENT_FINANSOWANIA).getIntegerData();
			if(procentFinansowania.compareTo(new Integer(100)) == 1){
				values.get(DWR_PROCENT_FINANSOWANIA).setIntegerData(0);
				return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", "Podano nieprawid�ow� warto�� pola: Procent finansowania. Prosz� wprowadzi� prawid�ow� warto��.", pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
			}
		}
		return null;
    }
    
    public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
    	log.info("--- Wniosek Szkoleniowy  : START PROCESS !!! ---- {}", event);

        try {
            Map<String, Object> map = Maps.newHashMap();
            map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
            map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
            java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

            DSUser author = DSApi.context().getDSUser();
            String user = author.getName();
            String fullName = author.asFirstnameLastname();

            perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));
    		
            Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
            perms.addAll(documentPermissions);
            this.setUpPermission(document, perms);

            if (AvailabilityManager.isAvailable("addToWatch")) {
                DSApi.context().watch(URN.create(document));
            }


        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new EdmException(e.getMessage());
        }
    }

    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
		boolean assigned = false;
		Long docId = doc.getId();
		return assigned;
    }
    
    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) {
        Map<String, Object> results = new HashMap<String, Object>();

        try {
            if (dictionaryName.equals("ZRODLO_FINANSOWANIA")) {
                setBudgetItemsRefValues(dictionaryName, values, results);
                }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return results;
    }

}