package pl.compan.docusafe.parametrization.imgwwaw.services;

import com.google.common.collect.Sets;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dictionary.DictionaryUtils;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.IMGWAssetCardInfo;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.imgw.DictionaryServiceStub;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class AssetCardInfoImport extends AbstractDictionaryImport {

    protected static Logger log = LoggerFactory.getLogger(AssetCardInfoImport.class);

    private static final String SERVICE_PATH = "/services/DictionaryService";
    private static final String SERVICE_TABLE_NAME_ASSET = "dsg_imgw_asset_card_info";

    DictionaryServiceStub stub;
    private StringBuilder message;

    DictionaryServiceStub.AssetCardInfo[] items;
    private int importPageSize;
    private int importPageNumber;
    private Set<Long> itemIds;

    @Override
    public void initConfiguration(AxisClientConfigurator conf) throws Exception {
        stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
        conf.setUpHttpParameters(stub, SERVICE_PATH);

    }

    @Override
    public void initImport() {
        items = null;
        importPageNumber = 1;
        importPageSize = 1000;
        itemIds = Sets.newHashSet();
    }

    @Override
    public boolean doImport() throws RemoteException, EdmException {
        message = new StringBuilder();
        if (stub != null) {
            DictionaryServiceStub.GetAssetCardInfo params = new DictionaryServiceStub.GetAssetCardInfo();
            message.append("rozmiar strony: " + importPageSize);
            message.append(", nr strony: " + importPageNumber);
            params.setPageSize(importPageSize);
            params.setPage(importPageNumber++);
            DictionaryServiceStub.GetAssetCardInfoResponse response;
            if ((response = stub.getAssetCardInfo (params)) != null) {
                items = response.get_return();
                if (items != null) {
                    message.append("Pobrano karty srodkow do przetworzenia: " + items.length);
                    for (DictionaryServiceStub.AssetCardInfo item : items) {
                        if (item != null) {
                            List<IMGWAssetCardInfo> found = pl.compan.docusafe.parametrization.invoice.DictionaryUtils.findByGivenFieldValue(IMGWAssetCardInfo.class, "idm", item.getIdm());
                            if (!found.isEmpty()) {
                                if (found.size() > 1) {
                                    message.append("Znaleziono wi�cej ni� 1 wpis o idm: " + item.getIdm());
                                }
                                for (IMGWAssetCardInfo resource : found) {
                                    resource.setAllFieldsFromServiceObject(item);
                                    resource.save();

                                    itemIds.add(resource.getId());
                                }
                            } else {
                                IMGWAssetCardInfo resource = new IMGWAssetCardInfo();
                                resource.setAllFieldsFromServiceObject(item);
                                resource.save();

                                message.append("Utworzono nowy kontrakt: idm: " + item.getIdm());
                                itemIds.add(resource.getId());
                            }
                        }
                    }
                    return false; // import niezako?czony
                }
            }
        }
        return true;
    }

    @Override
    public void finalizeImport() throws EdmException {
        int deleted = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME_ASSET, false);
        DataBaseEnumField.reloadForTable(IMGWAssetCardInfo.VIEW_NAME);
        message.append("Usuni�to: " + deleted);
    }

    @Override
    public String getMessage() {
        return message!=null?message.toString():null;
    }

    @Override
    public boolean isSleepAfterEachDoImport() {
        return false;
    }
}
