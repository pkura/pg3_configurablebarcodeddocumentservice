package pl.compan.docusafe.parametrization.imgwwaw.services;

import com.google.common.collect.Sets;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.imgw.DictionaryServicesBudgetViewsStub;
import pl.compan.docusafe.ws.imgw.DictionaryServicesBudgetViewsStub.Budget;
import pl.compan.docusafe.ws.imgw.DictionaryServicesBudgetViewsStub.GetBudgets;
import pl.compan.docusafe.ws.imgw.DictionaryServicesBudgetViewsStub.GetBudgetsResponse;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BudgetImport extends AbstractDictionaryImport {

    protected static Logger log = LoggerFactory.getLogger(BudgetImport.class);

    private static final String SERVICE_PATH = "/services/DictionaryServicesBudgetViews";

    private static final String SERVICE_TABLE_NAME_BUDGET = "dsg_imgw_services_budget";

    private DictionaryServicesBudgetViewsStub stub;
    private StringBuilder message;
    private Set<Long> itemIds;
    private int importPageSize;
    private int importPageNumber;
    private Set<Long> erpIds;

    @Override
    public void initConfiguration(AxisClientConfigurator conf) throws Exception {
        stub = new DictionaryServicesBudgetViewsStub(conf.getAxisConfigurationContext());
        conf.setUpHttpParameters(stub, SERVICE_PATH);

    }

    @Override
    public boolean doImport() throws RemoteException, EdmException {
        message = new StringBuilder();
        if (stub != null) {
            GetBudgets params = new GetBudgets();
            message.append("rozmiar strony: " + importPageSize);
            message.append(", nr strony: " + importPageNumber);
            params.setPageSize(importPageSize);
            params.setPage(importPageNumber++);
            GetBudgetsResponse response;
            if ((response = stub.getBudgets(params)) != null) {
                Budget[] items = response.get_return();
                if (items != null && items.length > 0) {
                    for (Budget item : items) {
                        if (item != null) {
                            Map<String, Object> fieldValues = new HashMap<String, Object>(2);
                            fieldValues.put("bdBudzetId", item.getBd_budzet_id());
                            fieldValues.put("bdPozBudzetId", item.getBd_poz_budzet_id());
                            List<pl.compan.docusafe.parametrization.imgwwaw.hbm.Budget> founds = DictionaryUtils.findByGivenFieldValues(pl.compan.docusafe.parametrization.imgwwaw.hbm.Budget.class, fieldValues);

                            if (!founds.isEmpty()) {
                                for (pl.compan.docusafe.parametrization.imgwwaw.hbm.Budget found : founds) {
                                    found.setAllFieldsFromServiceObject(item);
                                    found.save();

                                    itemIds.add(found.getId());
                                }
                            } else {
                                pl.compan.docusafe.parametrization.imgwwaw.hbm.Budget b = new pl.compan.docusafe.parametrization.imgwwaw.hbm.Budget();
                                b.setAllFieldsFromServiceObject(item);
                                b.save();

                                itemIds.add(b.getId());
                            }
                        }
                    }
                    return false; // import niezakończony
                }
            }
        }
        return true;
    }

    @Override
    public void finalizeImport() throws EdmException {
        int deleted = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME_BUDGET, false);
        message.append("Usunięto: " + deleted);
    }

    @Override
    public void initImport() {
        importPageNumber = 1;
        importPageSize = 1000;
        itemIds = Sets.newHashSet();
        erpIds = Sets.newHashSet();
    }

    @Override
    public String getMessage() {
        return message!=null?message.toString():null;
    }

    @Override
    public boolean isSleepAfterEachDoImport() {
        return false;
    }
}
