package pl.compan.docusafe.parametrization.imgwwaw.services;

import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.IMGWContractor;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.Kontrakt;
import pl.compan.docusafe.parametrization.invoice.DictionaryUtils;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.imgw.DictionaryServiceStub;
import pl.compan.docusafe.ws.imgw.DictionaryServicesSupplierStub;
import pl.compan.docusafe.ws.imgw.DictionaryServicesSupplierStub.GetSuppliers;
import pl.compan.docusafe.ws.imgw.DictionaryServicesSupplierStub.Supplier;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;

public class ContractorImport extends AbstractDictionaryImport {
    public static final String SERVICE_PATH = "/services/DictionaryService";
    public static final String SERVICE_TABLE_NAME = "dsg_imgw_contractor";
    DictionaryServiceStub stub;
    private StringBuilder message;

    DictionaryServiceStub.Supplier[] items;
    private int importPageSize;
    private int importPageNumber;
    private Set<Long> itemIds;


    protected static Logger log = LoggerFactory.getLogger(ContractorImport.class);

    @Override
    public void initConfiguration(AxisClientConfigurator conf) throws Exception {
        stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
        conf.setUpHttpParameters(stub, SERVICE_PATH);
    }

    @Override
    public void initImport() {
        items = null;
        importPageNumber = 1;
        importPageSize = 1000;
        itemIds = Sets.newHashSet();
    }

    @Override
    public boolean doImport() throws RemoteException, EdmException {
        message = new StringBuilder();
        if (stub != null) {
            DictionaryServiceStub.GetSupplier params = new DictionaryServiceStub.GetSupplier();
            params.setPageSize(importPageSize);
            params.setPage(importPageNumber++);
            DictionaryServiceStub.GetSupplierResponse response;
                if (params != null && (response = stub.getSupplier(params)) != null) {
                    items = response.get_return();
                    if (items != null) {
                        message.append("Pobrano kontrahent�w do przetworzenia: " + items.length);
                        for (DictionaryServiceStub.Supplier item : items) {
                            if (item != null) {
                                List<IMGWContractor> found = DictionaryUtils.findByGivenFieldValue(IMGWContractor.class, "erpId", item.getId());
                                if (!found.isEmpty()) {
                                    if (found.size() > 1) {
                                        message.append("Znaleziono wi�cej ni� 1 wpis o erpId: " + item.getId());
                                    }
                                    for (IMGWContractor resource : found) {
                                        resource.setAllFieldsFromServiceObject(item);
                                        resource.save();

                                        itemIds.add(Long.valueOf(resource.getId()));
                                    }
                                } else {
                                    IMGWContractor resource = new IMGWContractor();
                                    resource.setAllFieldsFromServiceObject(item);
                                    resource.save();
                                    message.append("Utworzono nowy kontrakt: erpId: " + item.getId());
                                    itemIds.add(Long.valueOf(resource.getId()));
                                }
                            }
                        }
                        return false;
                    } else {
                        message.append("Nie pobrano, serwis zwraca pust� tablic�");
                        return true;
                    }
                }
        }
        return true;
    }

    @Override
    public void finalizeImport() throws EdmException {
        int deleted = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME, false);
        message.append("Usuni�to: " + deleted);
    }

    @Override
    public String getMessage() {
        return message.length() > 0 ? message.toString() : null;
    }
}
