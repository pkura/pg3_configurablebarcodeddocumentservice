package pl.compan.docusafe.parametrization.imgwwaw.services;

import com.google.common.collect.ImmutableList;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImportService;
import pl.compan.docusafe.service.dictionaries.DictionaryImport;
import pl.compan.docusafe.util.DateUtils;

/**
 * Created with IntelliJ IDEA.
 * User: brs
 * Date: 12.08.13
 * Time: 12:14
 * To change this template use File | Settings | File Templates.
 */
public class IMGWContractorImportService extends AbstractDictionaryImportService {

    private static final ImmutableList<DictionaryImport> AVAILABLE_IMPORTS = ImmutableList.<DictionaryImport>builder()
            .add(new ContractorImport())
            .add(new ContractorAccountImport())
            .build();

    @Override
    protected ImmutableList<DictionaryImport> getAvailableImports() {
        return AVAILABLE_IMPORTS;
    }

    @Override
    protected long getTimerDefaultPeriod() {
        return 600l;
    }

    @Override
    protected long getTimerStartDelay() {
        return 25 * DateUtils.SECOND;
    }
}
