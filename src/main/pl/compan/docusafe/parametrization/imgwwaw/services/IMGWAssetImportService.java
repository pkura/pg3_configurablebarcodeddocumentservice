package pl.compan.docusafe.parametrization.imgwwaw.services;

import com.google.common.collect.ImmutableList;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImportService;
import pl.compan.docusafe.service.dictionaries.DictionaryImport;
import pl.compan.docusafe.util.DateUtils;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class IMGWAssetImportService extends AbstractDictionaryImportService {

    private static final ImmutableList<DictionaryImport> AVAILABLE_IMPORTS = ImmutableList.<DictionaryImport>builder()
            .add(new AssetCardInfoImport())
            .build();

    @Override
    protected ImmutableList<DictionaryImport> getAvailableImports() {
        return AVAILABLE_IMPORTS;
    }

    @Override
    protected long getTimerDefaultPeriod() {
        return 600l;
    }

    @Override
    protected long getTimerStartDelay() {
        return 25 * DateUtils.SECOND;
    }
}
