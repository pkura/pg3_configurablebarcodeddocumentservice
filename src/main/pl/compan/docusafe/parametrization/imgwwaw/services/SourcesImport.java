package pl.compan.docusafe.parametrization.imgwwaw.services;

import com.google.common.collect.Sets;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.imgw.DictionaryServicesBudgetViewsStub;
import pl.compan.docusafe.ws.imgw.DictionaryServicesBudgetViewsStub.GetSources;
import pl.compan.docusafe.ws.imgw.DictionaryServicesBudgetViewsStub.GetSourcesResponse;
import pl.compan.docusafe.ws.imgw.DictionaryServicesBudgetViewsStub.Source;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SourcesImport extends AbstractDictionaryImport {

    protected static Logger log = LoggerFactory.getLogger(SourcesImport.class);

    private static final String SERVICE_PATH = "/services/DictionaryServicesBudgetViews";

    private static final String SERVICE_TABLE_NAME_SOURCES = "dsg_imgw_services_sources";

    private DictionaryServicesBudgetViewsStub stub;
    private StringBuilder message;
    private Set<Long> itemIds;
    private int importPageSize;
    private int importPageNumber;
    private Set<Long> erpIds;

    @Override
    public void initConfiguration(AxisClientConfigurator conf) throws Exception {
        stub = new DictionaryServicesBudgetViewsStub(conf.getAxisConfigurationContext());
        conf.setUpHttpParameters(stub, SERVICE_PATH);

    }

    @Override
    public boolean doImport() throws RemoteException, EdmException {
        message = new StringBuilder();
        if (stub != null) {
            GetSources params = new GetSources();
            message.append("rozmiar strony: " + importPageSize);
            message.append(", nr strony: " + importPageNumber);
            params.setPageSize(importPageSize);
            params.setPage(importPageNumber++);
            GetSourcesResponse response;
            if ((response = stub.getSources(params)) != null) {
                Source[] items = response.get_return();
                if (items != null && items.length > 0) {
                    for (Source item : items) {
                        if (item != null) {
                            Map<String, Object> fieldValues = new HashMap<String, Object>(2);
                            fieldValues.put("bdBudzetPozId", item.getBd_budzet_poz_id());
                            fieldValues.put("zrodloId", item.getZrodlo_id());
                            List<pl.compan.docusafe.parametrization.imgwwaw.hbm.Source> founds = DictionaryUtils.findByGivenFieldValues(pl.compan.docusafe.parametrization.imgwwaw.hbm.Source.class, fieldValues);

                            if (!founds.isEmpty()) {
                                for (pl.compan.docusafe.parametrization.imgwwaw.hbm.Source found : founds) {
                                    found.setAllFieldsFromServiceObject(item);
                                    found.save();

                                    itemIds.add(found.getId());
                                }
                            } else {
                                pl.compan.docusafe.parametrization.imgwwaw.hbm.Source source = new pl.compan.docusafe.parametrization.imgwwaw.hbm.Source();
                                source.setAllFieldsFromServiceObject(item);
                                source.save();

                                itemIds.add(source.getId());
                            }
                        }
                    }
                    return false; // import niezakończony
                }
            }
        }
        return true;
    }

    @Override
    public void finalizeImport() throws EdmException {
        int deleted = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME_SOURCES, false);
        message.append("Usunięto: " + deleted);
    }

    @Override
    public void initImport() {
        importPageNumber = 1;
        importPageSize = 1000;
        itemIds = Sets.newHashSet();
        erpIds = Sets.newHashSet();
    }

    @Override
    public String getMessage() {
        return message != null ? message.toString() : null;
    }

    @Override
    public boolean isSleepAfterEachDoImport() {
        return false;
    }
}
