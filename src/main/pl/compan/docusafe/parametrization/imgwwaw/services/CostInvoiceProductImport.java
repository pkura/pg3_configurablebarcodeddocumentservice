package pl.compan.docusafe.parametrization.imgwwaw.services;

import com.google.common.collect.Sets;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.CostInvoiceProduct;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.imgw.DictionaryServiceStub;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

public class CostInvoiceProductImport extends AbstractDictionaryImport {

    protected static Logger log = LoggerFactory.getLogger(CostInvoiceProductImport.class);

    private static final String SERVICE_PATH = "/services/DictionaryService";
    private static final String SERVICE_TABLE_NAME = "dsg_imgw_services_cost_invoice_product";
    private DictionaryServiceStub stub;

    private Set<Long> itemIds;
    private int importPageSize;
    private int importPageNumber;

    @Override
    public void initConfiguration(AxisClientConfigurator conf) throws Exception {
        stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
        conf.setUpHttpParameters(stub, SERVICE_PATH);

    }

    @Override
    public void initImport() {
        importPageNumber = 1;
        importPageSize = 1000;
        itemIds = Sets.newHashSet();
    }


    @Override
    public boolean doImport() throws RemoteException, EdmException {
        if (stub != null) {
            stub._getServiceClient().cleanupTransport();
            DictionaryServiceStub.GetCostInvoiceProduct params = new DictionaryServiceStub.GetCostInvoiceProduct();
            params.setPageSize(importPageSize);
            params.setPage(importPageNumber++);
            DictionaryServiceStub.GetCostInvoiceProductResponse response;
            if (params != null && (response = stub.getCostInvoiceProduct(params)) != null) {
                DictionaryServiceStub.CostInvoiceProduct[] items = response.get_return();
                if (items != null) {
                    for (DictionaryServiceStub.CostInvoiceProduct item : items) {
                        if (item != null) {
                            if (!"Z_KOSZTY".equals(item.getKlaswytw_idn().trim())) {
                                continue;
                            }
                            List<CostInvoiceProduct> found = DictionaryUtils.findByGivenFieldValue(CostInvoiceProduct.class, "wytworId", (long) item.getId());
                            if (!found.isEmpty()) {
                                if (found.size() > 1) {
                                    log.error("Znaleziono wi�cej ni� 1 wpis o wytworId: " + item.getId());
                                }
                                for (CostInvoiceProduct resource : found) {
                                    resource.setAllFieldsFromServiceObject(item);
                                    resource.setAvailable(true);
                                    resource.save();

                                    itemIds.add(resource.getId());
                                }
                            } else {
                                CostInvoiceProduct resource = new CostInvoiceProduct();
                                resource.setAllFieldsFromServiceObject(item);
                                resource.setAvailable(true);
                                resource.save();

                                itemIds.add(resource.getId());
                            }
                        }
                    }
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void finalizeImport() throws EdmException {
        int updated = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME);
        log.debug("CostInvoiceProduct disabled: " + updated);
    }

    @Override
    public void materializeView() throws EdmException, SQLException {
        //ServicesUtils.deleteRowsInsertFromEnumView(ENUM_TABLE_NAME, ENUM_VIEW_NAME);
//        DataBaseEnumField.reloadForTable(ENUM_TABLE_NAME);
//        DataBaseEnumField.reloadForTable(VIEW_PRODUCT_DELEGATION);
    }
}
