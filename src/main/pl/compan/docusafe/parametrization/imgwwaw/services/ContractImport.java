package pl.compan.docusafe.parametrization.imgwwaw.services;

import com.google.common.collect.Sets;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.Kontrakt;
import pl.compan.docusafe.parametrization.invoice.DictionaryUtils;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.imgw.DictionaryServiceStub;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;

/**
 * Created by brs on 10.12.13.
 */
public class ContractImport extends AbstractDictionaryImport {

    protected static Logger log = LoggerFactory.getLogger(ContractImport.class);

    private static final String SERVICE_PATH = "/services/DictionaryService";
    private static final String SERVICE_TABLE_NAME = "dsg_imgw_services_contract";

    private DictionaryServiceStub stub;

    private Set<Long> itemIds;
    private int importPageSize;
    private int importPageNumber;
    private StringBuilder message;

    @Override
    public void initConfiguration(AxisClientConfigurator conf) throws Exception {
        stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
        conf.setUpHttpParameters(stub, SERVICE_PATH);
    }

    @Override
    public void initImport() {
        importPageNumber = 1;
        importPageSize = 1000;
        itemIds = Sets.newHashSet();
    }

    @Override
    public boolean doImport() throws RemoteException, EdmException {
        message = new StringBuilder();
        if (stub != null) {
            stub._getServiceClient().cleanupTransport();
            DictionaryServiceStub.GetContract params = new DictionaryServiceStub.GetContract();
            params.setPageSize(importPageSize);
            params.setPage(importPageNumber++);
            DictionaryServiceStub.GetContractResponse response;
            if (params != null && (response = stub.getContract(params)) != null) {
                DictionaryServiceStub.Contract[] items = response.get_return();
                if (items != null) {
                    for (DictionaryServiceStub.Contract item : items) {
                        if (item != null) {
                            List<Kontrakt> found = DictionaryUtils.findByGivenFieldValue(Kontrakt.class, "erpId", item.getId());
                            if (!found.isEmpty()) {
                                if (found.size() > 1) {
                                    message.append("Znaleziono wi�cej ni� 1 wpis o erpId: " + item.getId());
                                }
                                for (Kontrakt resource : found) {
                                    resource.setAllFieldsFromServiceObject(item);
                                    resource.save();

                                    itemIds.add(Long.valueOf(resource.getId()));
                                }
                            } else {
                                Kontrakt resource = new Kontrakt();
                                resource.setAllFieldsFromServiceObject(item);
                                resource.save();
                                message.append("Utworzono nowy kontrakt: erpId: " + item.getId());
                                itemIds.add(Long.valueOf(resource.getId()));
                            }
                        }
                    }
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void finalizeImport() throws EdmException {
        int deleted = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME, false);
        message.append("Usuni�to: " + deleted);
    }

    @Override
    public String getMessage() {
        return message != null ? message.toString() : null;
    }

    @Override
    public boolean isSleepAfterEachDoImport() {
        return false;
    }
}
