package pl.compan.docusafe.parametrization.imgwwaw.services;

import com.google.common.collect.Sets;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.ContractorAccount;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.imgw.DictionaryServiceStub;
import pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetSupplierBankAccount;
import pl.compan.docusafe.ws.imgw.DictionaryServiceStub.SupplierBankAccount;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: brs
 * Date: 09.12.13
 * Time: 15:43
 * To change this template use File | Settings | File Templates.
 */
public class ContractorAccountImport extends AbstractDictionaryImport {

    protected static Logger log = LoggerFactory.getLogger(ContractorAccountImport.class);

    private static final String SERVICE_PATH = "/services/DictionaryService";
    public static final String SERVICE_TABLE_NAME = "dsg_imgw_services_contractor_account";
    private DictionaryServiceStub stub;

    private Set<Long> itemIds;
    private int importPageSize;
    private int importPageNumber;

    @Override
    public void initConfiguration(AxisClientConfigurator conf) throws Exception {
        stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
        conf.setUpHttpParameters(stub, SERVICE_PATH);
    }

    @Override
    public void initImport() {
        importPageNumber = 1;
        importPageSize = 1000;
        itemIds = Sets.newHashSet();
    }

    @Override
    public boolean doImport() throws RemoteException, EdmException {
        if (stub != null) {
            stub._getServiceClient().cleanupTransport();
            GetSupplierBankAccount params = new GetSupplierBankAccount();
            params.setPageSize(importPageSize);
            params.setPage(importPageNumber++);
            DictionaryServiceStub.GetSupplierBankAccountResponse response;
            if (params != null && (response = stub.getSupplierBankAccount (params)) != null) {
                SupplierBankAccount[] items = response.get_return();
                if (items != null) {
                    for (SupplierBankAccount item : items) {
                        if (item != null) {
                            List<ContractorAccount> found = DictionaryUtils.findByGivenFieldValue(ContractorAccount.class, "erpId", item.getId());
                            if (!found.isEmpty()) {
                                if (found.size() > 1) {
                                    log.error("Znaleziono wi�cej ni� 1 wpis o erpId: " + item.getId());
                                }
                                for (ContractorAccount resource : found) {
                                    resource.setAllFieldsFromServiceObject(item);
                                    resource.setAvailable(true);
                                    resource.save();

                                    itemIds.add(Long.valueOf(resource.getId()));
                                }
                            } else {
                                ContractorAccount resource = new ContractorAccount();
                                resource.setAllFieldsFromServiceObject(item);
                                resource.setAvailable(true);
                                resource.save();

                                itemIds.add(Long.valueOf(resource.getId()));
                            }
                        }
                    }
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void finalizeImport() throws EdmException {
        int updated = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME);
        log.debug("ContractorAccount disabled: " + updated);
    }
}
