package pl.compan.docusafe.parametrization.imgwwaw.services;

import com.google.common.collect.ImmutableList;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImportService;
import pl.compan.docusafe.service.dictionaries.DictionaryImport;
import pl.compan.docusafe.util.DateUtils;


public class IMGWBudgetAndSourcesImportService extends AbstractDictionaryImportService {

    private static final ImmutableList<DictionaryImport> AVAILABLE_IMPORTS = ImmutableList.<DictionaryImport>builder()
            .add(new BudgetImport())
            .add(new SourcesImport())
            .build();

    @Override
    protected ImmutableList<DictionaryImport> getAvailableImports() {
        return AVAILABLE_IMPORTS;
    }

    @Override
    protected long getTimerDefaultPeriod() {
        return 1800l;
    }

    @Override
    protected long getTimerStartDelay() {
        return 15 * DateUtils.SECOND;
    }
}
