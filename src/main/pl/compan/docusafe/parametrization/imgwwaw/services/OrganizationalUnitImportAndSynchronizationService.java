/**
 * 
 */
package pl.compan.docusafe.parametrization.imgwwaw.services;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Constants;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.imgw.DictionaryServicesOrganizationalUnitStub;
import pl.compan.docusafe.ws.imgw.DictionaryServicesOrganizationalUnitStub.GetOrganisationalUnits;
import pl.compan.docusafe.ws.imgw.DictionaryServicesOrganizationalUnitStub.GetOrganisationalUnitsResponse;
import pl.compan.docusafe.ws.imgw.DictionaryServicesOrganizationalUnitStub.OrganizationalUnit;

import com.google.common.collect.Lists;

/**
 * Serwis do synchronizowania struktury organizacyjnej w IMGW.
 * 
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class OrganizationalUnitImportAndSynchronizationService extends ServiceDriver implements Service {
	
	private static final Logger logger = LoggerFactory.getLogger(OrganizationalUnitImportAndSynchronizationService.class);
	private static final StringManager sm = StringManager.getManager(Constants.Package);
	
	public static final Integer PAGE_SIZE = 100;
	public static final Integer DEFAULT_SYNCHRONIZATION_HOUR = 23;
	public static final Integer MIN_HOUR = 0;
	public static final Integer MAX_HOUR = 23;
	
	private Timer timer;
	private Property[] properties;
	private Integer synchronizationHour;
	private Date startDate;
	
	public OrganizationalUnitImportAndSynchronizationService() {
		properties = new Property[] {new SynchronizationHourProperty()};
		synchronizationHour = DEFAULT_SYNCHRONIZATION_HOUR;
		startDate = getSynchronizationDate();
	}
	
	protected void start() throws ServiceException {
		logger.info("Start uslugi OrganizationalUnitImportService");
		if (timer != null) {
			timer.cancel();
		}
		timer = new Timer(true);
		//timer.schedule(new Import(), startDate, DateUtils.DAY);
		timer.schedule(new Import(), 0, DateUtils.DAY);
		console(Console.INFO, sm.getString("OrganizationalUnitImportAndSynchronizationServiceStart"));
	}

	protected void stop() throws ServiceException {
		if (timer != null) {
			timer.cancel();
		}
		console(Console.INFO, sm.getString("OrganizationalUnitImportAndSynchronizationServiceStop"));
	}

	protected boolean canStop() {
		return true;
	}
	
	@Override
	public Property[] getProperties() 
	{
		return properties;
	}
	
	/**
	 * Metoda zwraca date synchronizowania struktury organizacyjnej
	 * @return date synchronizowania struktury organizacyjnej
	 */
	private Date getSynchronizationDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, synchronizationHour);
		calendar.clear(Calendar.MINUTE);
		calendar.clear(Calendar.SECOND);
		calendar.clear(Calendar.MILLISECOND);
		return calendar.getTime();
	}
	
	class Import extends TimerTask {
		
		private static final String SERVICE_PATH = "/services/DictionaryServicesOrganizationalUnit";
		
		DictionaryServicesOrganizationalUnitStub stub;
		OrganizationalUnit[] items;
		
		public void run() {
			console(Console.INFO, sm.getString("OrganizationalUnitImportAndSynchronizationServiceStartSynchronization"));
			List<String> divisionIdsFromResponse = Lists.newArrayList();
			try {
				DSApi.openAdmin();
				initConfiguration();
				if (stub != null) {
					GetOrganisationalUnits params = new GetOrganisationalUnits();
					boolean itemsWereEmpty = false;
					int pageIndex = 1;
					while (!itemsWereEmpty) {
						console(Console.INFO, "Pobra� stron� "+ pageIndex);
						params.setPage(pageIndex);
						params.setPageSize(PAGE_SIZE);
						GetOrganisationalUnitsResponse response = stub.getOrganisationalUnits(params);
						if (params != null && response != null) {
							items = response.get_return();
							if (items != null) {
								for (OrganizationalUnit item : items) {
									divisionIdsFromResponse.add(String.valueOf(item.getKomorka_id()));
									updateOrCreateNewDivision(item);
								}
							} else {
								itemsWereEmpty = true;
							}
							pageIndex++;
						}
					}
					console(Console.INFO, "Usuwa ");
					deleteNotExistingInResponseDivisions(divisionIdsFromResponse);
					console(Console.INFO, sm.getString("OrganizationalUnitImportAndSynchronizationServiceStopSynchronization"));
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			try {
				DSApi.close();
			} catch (EdmException e) {
				logger.error(e.getMessage(), e);
			}
		}
		
		private void initConfiguration() throws Exception {
            AxisClientConfigurator conf = new AxisClientConfigurator();
            stub = new DictionaryServicesOrganizationalUnitStub(conf.getAxisConfigurationContext());
            conf.setUpHttpParameters(stub, SERVICE_PATH);
        }
		
		private void updateOrCreateNewDivision(OrganizationalUnit item) {
			DSDivision division = null;
			try {
				division = DSDivision.findByExternalId(String.valueOf(item.getKomorka_id()));
				updateDivision(item, division);
			} catch (DivisionNotFoundException e) {
				createNewDivision(item);
			} catch (EdmException e) {
				logger.error(e.getMessage(), e);
			}
		}
		
		/**
		 * Metoda tworzy nowy dzial na podstawie dzialu przeslanego przez webservice - parametr item.
		 * @param item - dzial przeslany przez webservice
		 */
		private void createNewDivision(OrganizationalUnit item) {
			try {
				DSApi.context().begin();
				DSDivision rootDivision = DSDivision.find(DSDivision.ROOT_GUID);
				DSDivision newDivision = rootDivision.createDivisionWithExternal(item.getNazwa(), item.getKomorka_idn(), String.valueOf(item.getKomorka_id()));
				DSDivision parentDivision;
				try {
					parentDivision = DSDivision.findByName(item.getKomorka_nad_nazwa());
				} catch (DivisionNotFoundException e) {
					parentDivision = DSDivision.find(DSDivision.ROOT_GUID);
				}
				newDivision.setParent(parentDivision);
				newDivision.update();
				DSApi.context().commit();
				console(Console.INFO, sm.getString("OrganizationalUnitImportAndSynchronizationServiceCreatedNewDivision", newDivision.getName()));
			} catch (EdmException e) {
				console(Console.ERROR, sm.getString("OrganizationalUnitImportAndSynchronizationServiceErrorCreatedNewDivision", item.getNazwa()));
				logger.error(e.getMessage(), e);
				try {
					DSApi.context().rollback();
				} catch (EdmException exp) {
					logger.error(exp.getMessage(), exp);
				}
			}
		}
		
		/**
		 * Metoda sprawdza czy potrzebna jest aktualizacja dzialu division, jezeli tak to aktualizuje go.
		 * @param item - dzial przeslany przez webservice (aktualny)
		 * @param division - dzial znajdujacy sie w bazie danych
		 */
		private void updateDivision(OrganizationalUnit item, DSDivision division) {
			DSDivision parentDivision;
			boolean divisionHasChanged = false;
			try {
				//jezeli dzial byl wczesniej usuniety to go przywraca
				if (division.isHidden()) {
					division.setHidden(false);
					divisionHasChanged = true;
				}
				if (!division.getCode().equals(item.getKomorka_idn())) {
					division.setCode(item.getKomorka_idn());
					divisionHasChanged = true;
				}
				if (!division.getName().equals(item.getNazwa())) {
					division.setName(item.getNazwa());
					divisionHasChanged = true;
				}
				if (StringUtils.isNotBlank(item.getKomorka_nad_idn())) {
					parentDivision = DSDivision.findByCode(item.getKomorka_nad_idn());
				} else {
					parentDivision = DSDivision.find(DSDivision.ROOT_GUID);
				}
				if (division.getParent() != null && parentDivision != null) {
					if (parentDivision.isRoot()) {
						if (!division.getParent().isRoot()) {
							division.setParent(parentDivision);
							divisionHasChanged = true;
						}
					} else {
						if (!parentDivision.getCode().equals(division.getParent().getCode())) {
							division.setParent(parentDivision);
							divisionHasChanged = true;
						}
					}
				}
			} catch (EdmException e) {
				divisionHasChanged = false;
				logger.error(e.getMessage(), e);
				console(Console.ERROR, sm.getString("OrganizationalUnitImportAndSynchronizationServiceErrorUpdatedDivision", item.getNazwa()));
			}
			
			if (divisionHasChanged) {
				try {
					DSApi.context().begin();
					division.update();
					DSApi.context().commit();
					console(Console.INFO, sm.getString("OrganizationalUnitImportAndSynchronizationServiceUpdatedDivision", division.getName()));
				} catch (EdmException e) {
					logger.error(e.getMessage(), e);
					console(Console.ERROR, sm.getString("OrganizationalUnitImportAndSynchronizationServiceErrorUpdatedDivision", item.getNazwa()));
					try {
						DSApi.context().rollback();
					} catch (EdmException exp) {
						logger.error(exp.getMessage(), exp);
					}
				}
			}
		}
		
		/**
		 * Metoda usuwa dzialy ktore nie sa aktualne. Czyli te ktore nie znajduja sie w danych przesylanych przez webservice i ktore posiadaja externalId.
		 * @param divisionIdsFromResponse - lista external ids'ow dzialow ktore zostaly przeslane przez webservice
		 */
		private void deleteNotExistingInResponseDivisions(List<String> divisionIdsFromResponse) {
			try {
				DSDivision[] divisions = DSDivision.getAllDivisions();
				for (DSDivision division : divisions) {
					if (StringUtils.isNotBlank(division.getExternalId()) && !divisionIdsFromResponse.contains(division.getExternalId()) && !division.isHidden()) {
						try {
							DSApi.context().begin();
							console(Console.INFO, "Usuwa dzia�"+ division.getName()+" "+division.getCode());
							UserFactory.getInstance().deleteDivision(division.getGuid(), DSDivision.ROOT_GUID);
							DSApi.context().commit();
							console(Console.INFO, sm.getString("OrganizationalUnitImportAndSynchronizationServiceDeletedDivision", division.getName()));
						} catch (EdmException e) {
							logger.error(e.getMessage(), e);
							console(Console.ERROR,  sm.getString("OrganizationalUnitImportAndSynchronizationServiceErrorDeletedDivision", division.getName()));
							DSApi.context().rollback();
						}
					}
				}
			} catch (EdmException e) {
				logger.error(e.getMessage(), e);
			}
		}
		
	}
	
	private class SynchronizationHourProperty extends Property {
		
		public SynchronizationHourProperty() {
			super(SIMPLE, PERSISTENT, OrganizationalUnitImportAndSynchronizationService.this, "synchronizationHour", "Godzina synchronizacji", Integer.class);
		}
		
		@Override
		protected Object getValueSpi() {
			return synchronizationHour;
		}
		
		@Override
		protected void setValueSpi(Object object) throws ServiceException {
			synchronized (OrganizationalUnitImportAndSynchronizationService.this) {
				try {
					if (object != null) {
						if (isCorrectHour(object)) {
							synchronizationHour = (Integer) object;
							startDate = getSynchronizationDate();
							if (timer != null) {
								timer.cancel();
							}
							timer = new Timer(true);
							timer.schedule(new Import(), startDate, DateUtils.DAY);
							console(Console.INFO, sm.getString("OrganizationalUnitImportAndSynchronizationServiceSetHour", synchronizationHour));
						} else {
							console(Console.WARN, sm.getString("OrganizationalUnitImportAndSynchronizationServiceIncorrectHour"));
						}
					} else {
						console(Console.WARN, sm.getString("OrganizationalUnitImportAndSynchronizationServiceNoHour"));
					}
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
					console(Console.WARN, sm.getString("OrganizationalUnitImportAndSynchronizationServiceSetHourError"));
				}
			}
		}
		
		/**
		 * Metoda sprawdza czy dany parametr object jest prawidlowa godzina synchronizowania. Czyli czy jest wartoscia typu integer z przedzialu 0 - 23.
		 * Jezeli jest prawidlowa, metoda zwraca true w odwrotnym przypadku zwracany jest false.
		 * @param object - godzina synchronizowania
		 * @return true, jezeli dana godzina synchronizowania jest poprawna, false w odwrotnym przypadku
		 */
		private boolean isCorrectHour(Object object) {
			boolean isCorrect = true;
			if (object instanceof Integer) {
				Integer value = (Integer) object;
				if (value < MIN_HOUR || value > MAX_HOUR) {
					isCorrect = false;
				}
			} else {
				isCorrect = false;
			}
			return isCorrect;
		}
		
	}
	
}
