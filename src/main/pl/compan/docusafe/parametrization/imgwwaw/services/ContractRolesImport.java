package pl.compan.docusafe.parametrization.imgwwaw.services;

import com.google.common.collect.Sets;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.Kontrakt;
import pl.compan.docusafe.parametrization.invoice.DictionaryUtils;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.imgw.DictionaryServiceStub;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;

/**
 * Created by brs on 10.12.13.
 */
public class ContractRolesImport extends AbstractDictionaryImport {

    protected static Logger log = LoggerFactory.getLogger(ContractRolesImport.class);

    private static final String SERVICE_PATH = "/services/DictionaryService";

    private DictionaryServiceStub stub;

    private Set<Long> itemIds;
    private int importPageSize;
    private int importPageNumber;
    private StringBuilder message;

    @Override
    public void initConfiguration(AxisClientConfigurator conf) throws Exception {
        stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
        conf.setUpHttpParameters(stub, SERVICE_PATH);
    }

    @Override
    public void initImport() {
        importPageNumber = 1;
        importPageSize = 1000;
        itemIds = Sets.newHashSet();
    }

    @Override
    public boolean doImport() throws RemoteException, EdmException {
        message = new StringBuilder();
        if (stub != null) {
            stub._getServiceClient().cleanupTransport();
            DictionaryServiceStub.GetRolesInProjects params = new DictionaryServiceStub.GetRolesInProjects();
            params.setPageSize(importPageSize);
            params.setPage(importPageNumber++);
            DictionaryServiceStub.GetRolesInProjectsResponse response;
            if (params != null && (response = stub.getRolesInProjects(params)) != null) {
                DictionaryServiceStub.RolesInProjects[] items = response.get_return();
                if (items != null) {
                    for (DictionaryServiceStub.RolesInProjects item : items) {
                        if (item != null) {
                            List<Kontrakt> found = DictionaryUtils.findByGivenFieldValue(Kontrakt.class, "erpId", item.getKontrakt_id());
                            if (!found.isEmpty()) {
                                if (found.size() > 1) {
                                    message.append("Znaleziono wi�cej ni� 1 wpis o erpId: " + item.getBp_rola_kontrakt_id());
                                }
                                for (Kontrakt resource : found) {
                                    resource.setAllFieldsFromServiceObject(item);
                                    resource.save();

                                    itemIds.add(Long.valueOf(resource.getId()));
                                }
                            }
                        }
                    }
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void finalizeImport() throws EdmException {
//        int deleted = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME, true);
//        message.append("Usuni�to: " + deleted);
    }

    @Override
    public String getMessage() {
        return message != null ? message.toString() : null;
    }

    @Override
    public boolean isSleepAfterEachDoImport() {
        return false;
    }
}
