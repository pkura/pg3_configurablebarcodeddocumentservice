package pl.compan.docusafe.parametrization.imgwwaw.services;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.PurchaseDocumentType;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.imgw.DictionaryServiceStub;

import java.rmi.RemoteException;
import java.util.*;

/**
 * Created by brs on 03.02.14.
 */
public class PurchaseDocumentTypeImport extends AbstractDictionaryImport{
    private static Logger logger = LoggerFactory.getLogger(PurchaseDocumentTypeImport.class);

    private static final String SERVICE_PATH = "/services/DictionaryService";
    private static final String SERVICE_TABLE_NAME_SOURCES = "dsg_imgw_purchase_document_type";

    private DictionaryServiceStub stub;
    private int importPageSize;
    private int importPageNumber;
    private Set<Long> itemIds;

    public void initConfiguration(AxisClientConfigurator conf) throws Exception {
        stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
        conf.setUpHttpParameters(stub, SERVICE_PATH);
    }

    public boolean doImport() throws RemoteException, EdmException {
        if (stub != null) {
            DictionaryServiceStub.GetPurchaseDocumentType params = new DictionaryServiceStub.GetPurchaseDocumentType();
            params.setPage(importPageNumber);
            params.setPageSize(importPageSize);
            DictionaryServiceStub.GetPurchaseDocumentTypeResponse response = stub.getPurchaseDocumentType(params);
            if (response != null) {
                DictionaryServiceStub.PurchaseDocumentType[] items = response.get_return();
                if (items != null && items.length > 0) {
                    for (DictionaryServiceStub.PurchaseDocumentType item : items) {
                        if (item != null) {
                            Map<String, Object> fieldValues = new HashMap<String, Object>(1);
                            fieldValues.put("idm", item.getIdm().trim());
                            List<PurchaseDocumentType> founds = DictionaryUtils.findByGivenFieldValues(PurchaseDocumentType.class, fieldValues);

                            if (!founds.isEmpty()) {
                                for (PurchaseDocumentType found : founds) {
                                    found.setAllFieldsFromServiceObject(item);
                                    found.save();
                                    itemIds.add(found.getId());
                                }
                            } else {
                                PurchaseDocumentType purchaseDocumentType = new PurchaseDocumentType();
                                purchaseDocumentType.setAllFieldsFromServiceObject(item);
                                purchaseDocumentType.save();
                                itemIds.add(purchaseDocumentType.getId());
                            }

                        }
                    }
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }
        return true;
    }

    @Override
    public void initImport() {
        importPageSize = 1000;
        importPageNumber = 1;
        itemIds = new HashSet<Long>();
    }

    @Override
    public void finalizeImport() throws EdmException {
        int deleted = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME_SOURCES, false);
    }
}
