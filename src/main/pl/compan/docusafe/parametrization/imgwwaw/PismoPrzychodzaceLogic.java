package pl.compan.docusafe.parametrization.imgwwaw;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.yetico.NiezgodnoscWewLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class PismoPrzychodzaceLogic extends AbstractDocumentLogic{
	private final static Logger log = LoggerFactory.getLogger(PismoPrzychodzaceLogic.class);
	private final static String DATA_WPLYWU_CN = "DATA_WPLYWU";
	
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		Map<String, Object> toReload = Maps.newHashMap();
		for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
		{
			if (f.getDefaultValue() != null)
			{
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
			}
		}

		toReload.put(DATA_WPLYWU_CN, new Date());
		log.info("data wplywu = " + toReload.get(DATA_WPLYWU_CN));
		fm.reloadValues(toReload);
	}
	
	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {
		// TODO Auto-generated method stub
		
	}

	public void onStartProcess(OfficeDocument document,ActionEvent event)
    {
    	try
		{
    		Map<String, Object> map = Maps.newHashMap();
//			map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
            map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
            map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
    		log.info("onStartProcess");
    		document.getDocumentKind().setOnly(document.getId(), ImmutableMap.of(DATA_WPLYWU_CN, new Date()));
    		
			//map.put(ASSIGN_DIVISION_GUID_PARAM, DSDivision.ROOT_GUID);
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch")) {
				DSApi.context().watch(URN.create(document));
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
    }

//    @Override
//    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
//        boolean assigned = false;
//        try {
//        	log.info("assignee");
//            List<Recipient> recipients = doc.getRecipients();
//            String divisionName = null, divisionGuid = null;
//            if(recipients != null ) {
//            	log.info("recipiensts != null");
//            	if(recipients != null) {
//            		log.info("odbiorca = " + DSUser.findByFirstnameLastname(recipients.get(0).getFirstname(), recipients.get(0).getLastname()).getName());
//            		assigned = true;
//            		assignable.addCandidateUser(DSUser.findByFirstnameLastname(recipients.get(0).getFirstname(), recipients.get(0).getLastname()).getName());
//            		return assigned;
//            	}
//            	log.info("recipient = " + recipients.get(0).getLastname());
//            	divisionName = recipients.get(0).getLparam();
//            	log.info("divisionName = " + divisionName);
//
//	            String[] ids = ((String) divisionName).split(";");
//	            if (ids[1].contains("d:"))
//	            	divisionGuid = ids[1].split(":")[1];
//	            log.info("divisionGuid = " + divisionGuid);
//	            DSDivision div = null;
//	            if(divisionGuid != null) {
//	            	div = DSDivision.find(divisionGuid);
//	            }
//
//
//	            Role sekretariat = Role.findByName("sekretariat");
//	            Set<String> users = sekretariat.getUsernames();
//	    		if(sekretariat != null && "akceptacja-sekretariat".equals(acceptationCn)) {
//	    			log.info("sekretariat != null");
//					for(DSUser user : div.getUsers()) {
//						log.info("username = " + user.getName());
//						if(users.contains(user.getName())) {
//							log.info("przypisuje zadanie: " + user.getName());
//	    	            	assignable.addCandidateUser(user.getName());
//	    	                assigned = true;
//	    	                AssigneeHandler.addToHistory(openExecution, doc, user.getName(), null);
//						}
//		            }
//	    		}
//            }
//
//            return assigned;
//        } catch (EdmException e) {
//            log.error(e.getMessage(), e);
//            return assigned;
//        }
//    }
}
