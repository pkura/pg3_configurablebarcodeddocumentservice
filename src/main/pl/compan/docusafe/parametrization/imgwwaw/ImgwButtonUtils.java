package pl.compan.docusafe.parametrization.imgwwaw;

import com.google.common.collect.Sets;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.directwebremoting.WebContextFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.attachments.DwrAttachmentStorage;
import pl.compan.docusafe.core.dockinds.dwr.attachments.DwrFileItem;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.*;
import pl.compan.docusafe.parametrization.imgwwaw.services.ContractorAccountImport;
import pl.compan.docusafe.parametrization.imgwwaw.services.ContractorImport;
import pl.compan.docusafe.parametrization.invoice.DictionaryUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.imgw.DictionaryServiceStub;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by brs on 24.12.13.
 */
public class ImgwButtonUtils {
    private static final Logger log = LoggerFactory.getLogger(ImgwButtonUtils.class);
    public static final String DWR_IMPORTED_DOCUMENT = "DWR_IMPORTED_DOCUMENT";
    public static final String IMPORTED_DOCUMENT_CN = "IMPORTED_DOCUMENT";

    static DictionaryServiceStub.Supplier[] items;
    private static int importPageSize;
    private static int importPageNumber;
    private static Set<Long> itemIds;

    private static boolean importContractors(DictionaryServiceStub stub) throws java.rmi.RemoteException, EdmException {
        if (stub != null) {
            DictionaryServiceStub.GetSupplier params = new DictionaryServiceStub.GetSupplier();
            params.setPageSize(importPageSize);
            params.setPage(importPageNumber++);
            DictionaryServiceStub.GetSupplierResponse response;
            if (params != null && (response = stub.getSupplier(params)) != null) {
                items = response.get_return();
                if (items != null) {
                    for (DictionaryServiceStub.Supplier item : items) {
                        if (item != null) {
                            List<IMGWContractor> found = DictionaryUtils.findByGivenFieldValue(IMGWContractor.class, "erpId", item.getId());
                            if (!found.isEmpty()) {
                                for (IMGWContractor resource : found) {
                                    resource.setAllFieldsFromServiceObject(item);
                                    resource.setAvailable(true);
                                    resource.save();
//                                    log.error("Zaktualizowano kontrahenta, nazwa: {}, erpID: {}", resource.getNazwa(), resource.getErpId());
                                    itemIds.add(Long.valueOf(resource.getId()));
                                }
                            } else {
                                IMGWContractor resource = new IMGWContractor();
                                resource.setAllFieldsFromServiceObject(item);
                                resource.save();
                                log.error("Dodano nowego kontrahenta, nazwa: {}, erpID: {}", resource.getNazwa(), resource.getErpId());
                                itemIds.add(Long.valueOf(resource.getId()));
                            }
                        }
                    }
                    return false;
                } else {
                    return true;
                }
            }
        }
        return true;
    }

    private static boolean importContractorAccounts(DictionaryServiceStub stub) throws java.rmi.RemoteException, EdmException {
        if (stub != null) {
            stub._getServiceClient().cleanupTransport();
            DictionaryServiceStub.GetSupplierBankAccount params = new DictionaryServiceStub.GetSupplierBankAccount();
            params.setPageSize(importPageSize);
            params.setPage(importPageNumber++);
            DictionaryServiceStub.GetSupplierBankAccountResponse response;
            if (params != null && (response = stub.getSupplierBankAccount(params)) != null) {
                DictionaryServiceStub.SupplierBankAccount[] items = response.get_return();
                if (items != null) {
                    for (DictionaryServiceStub.SupplierBankAccount item : items) {
                        if (item != null) {
                            List<ContractorAccount> found = DictionaryUtils.findByGivenFieldValue(ContractorAccount.class, "erpId", item.getId());
                            if (!found.isEmpty()) {
                                for (ContractorAccount resource : found) {
                                    resource.setAllFieldsFromServiceObject(item);
                                    resource.setAvailable(true);
                                    resource.save();
                                    itemIds.add(Long.valueOf(resource.getId()));
                                }
                            } else {
                                ContractorAccount resource = new ContractorAccount();
                                resource.setAllFieldsFromServiceObject(item);
                                resource.setAvailable(true);
                                resource.save();
                                log.error("Dodano konto bankowe {} do kontrahenta", resource.getIdm());
                                itemIds.add(Long.valueOf(resource.getId()));
                            }
                        }
                    }
                    return false;
                }
            }
        }
        return true;
    }

    private static void initImport() {
        items = null;
        importPageNumber = 1;
        importPageSize = 1000;
        itemIds = Sets.newHashSet();
    }

    private static void finalizeContractorImport(boolean delete) throws EdmException {
        int deleted = ServicesUtils.disableDeprecatedItem(itemIds, ContractorImport.SERVICE_TABLE_NAME, false);
        log.error("Synchronizacja kontrahent�w: usuni�to {} kontrahent�w", deleted);
    }

    private static void finalizeAccountsImport(boolean delete) throws EdmException {
        int deleted = ServicesUtils.disableDeprecatedItem(itemIds, ContractorAccountImport.SERVICE_TABLE_NAME, false);
        log.error("Synchronizacja kont kontrahent�w: usuni�to {} kont bankowych", deleted);
    }

    protected static void synchronizeContractorsOnDemand() throws Exception {
        AxisClientConfigurator conf = new AxisClientConfigurator();
        DictionaryServiceStub stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
        conf.setUpHttpParameters(stub, ContractorImport.SERVICE_PATH);

        boolean done;
        initImport();
        boolean isOpened = true;
        try {
            do {
                if (!DSApi.isContextOpen()) {
                    isOpened = false;
                    DSApi.openAdmin();
                }
                //                DSApi.context().begin();
                done = importContractors(stub);
                //                DSApi.context().commit();
            } while (!done);
            finalizeContractorImport(true);
        } finally {
            if (DSApi.isContextOpen() && !isOpened)
                DSApi._close();
        }

        done = false;
        initImport();
        isOpened = true;
        try {
            do {
                if (!DSApi.isContextOpen()) {
                    isOpened = false;
                    DSApi.openAdmin();
                }
                //                DSApi.context().begin();
                done = importContractorAccounts(stub);
                //                DSApi.context().commit();
            } while (!done);
            finalizeAccountsImport(false);
        } finally {
            if (DSApi.isContextOpen() && !isOpened)
                DSApi._close();
        }
    }

    protected static void importBudgetPositions(FieldsManager fm, Map<String, FieldData> values) throws Exception {
        List<DwrFileItem> files = DwrAttachmentStorage.getInstance().getFiles(WebContextFactory.get().getSession(), IMPORTED_DOCUMENT_CN);
        if (files == null || files.size() < 1) {
            DwrAttachmentStorage.getInstance().removeAll(WebContextFactory.get().getSession(), IMPORTED_DOCUMENT_CN);
            throw new EdmException("B��d importu. Nie dodano za��cznika ze �r�d�ami finansowymi");
        }
        String fileName = files.get(0).getName();
        String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
        if (!"xls".equals(extension) && !"xlsx".equals(extension)) {
            DwrAttachmentStorage.getInstance().removeAll(WebContextFactory.get().getSession(), IMPORTED_DOCUMENT_CN);
            throw new EdmException("B��d importu. Za��cznik musi by� w formacie xls lub xlsx");
        }
//        DSApi.openAdmin();
//        Attachment attachment = (Attachment.find(Long.valueOf(values.get(DWR_IMPORTED_DOCUMENT).getStringData())));
//        String fileName = attachment.getMostRecentRevision().getFileName();
        XSSFWorkbook workbook = new XSSFWorkbook(files.get(0).getInputStream());
        XSSFSheet sheet = workbook.getSheetAt(0);
        List<Long> zrodlaIds = new ArrayList<Long>();
        DSApi.openAdmin();
        for (Row row : sheet) {
            if (row.getRowNum() == 0) {
                continue;
            }
            int lastColumn = Math.max(row.getLastCellNum(), 8);
            ZrodlaFinansowaniaDict zrodlo = new ZrodlaFinansowaniaDict();
            Cell c = null;
            String budgetCode = null;
            String positionName = null;
            String sourceCode = null;
            for (int cellNum = 0; cellNum < lastColumn; cellNum++) {
                switch (cellNum) {
                    case 0:
                        c = row.getCell(cellNum, Row.RETURN_BLANK_AS_NULL);
                        if (c != null) {
                            String code = c.getRichStringCellValue().getString().trim().toUpperCase();
                            Kontrakt t = Kontrakt.findByIdm(code);
                            zrodlo.setContract(t.getId());
                        }
                        break;
                    case 1:
                        c = row.getCell(cellNum, Row.RETURN_BLANK_AS_NULL);
                        if (c != null) {
                            String code = c.getRichStringCellValue().getString().trim().toUpperCase();
                            CostInvoiceProduct t = CostInvoiceProduct.findByIdm(code);
                            zrodlo.setProduct((int) t.getId());
                        }
                        break;
                    case 2:
                        c = row.getCell(cellNum, Row.RETURN_BLANK_AS_NULL);
                        if (c != null) {
                            budgetCode = c.getRichStringCellValue().getString().trim().toUpperCase();
                        }
                        break;
                    case 3:
                        c = row.getCell(cellNum, Row.RETURN_BLANK_AS_NULL);
                        if (c != null) {
                            positionName = c.getRichStringCellValue().getString().trim().toUpperCase();
                        }
                        break;
                    case 4:
                        c = row.getCell(cellNum, Row.RETURN_BLANK_AS_NULL);
                        if (c != null) {
                            sourceCode = c.getRichStringCellValue().getString().trim().toUpperCase();
//                            Source t = Source.findByIdm(code);
//                            zrodlo.setSource((int) t.getId());
                        }
                        break;
                    case 5:
                        c = row.getCell(cellNum, Row.RETURN_BLANK_AS_NULL);
                        if (c != null) {
                            BigDecimal t = null;
                            if (c.getCellType() == Cell.CELL_TYPE_STRING) {
                                String brutto = c.getRichStringCellValue().getString();
                                t = new BigDecimal(brutto).setScale(2);
                            } else if (c.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                                double brutto = c.getNumericCellValue();
                                t = new BigDecimal(brutto).setScale(2);
                            }
                            zrodlo.setAmount(t);
                        }
                        break;
                    case 6:
                        c = row.getCell(cellNum, Row.RETURN_BLANK_AS_NULL);
                        if (c != null) {
                            String code = null;
                            if (c.getCellType() == Cell.CELL_TYPE_STRING) {
                                code = c.getRichStringCellValue().getString().trim().toUpperCase();
                            } else if (c.getCellType() == Cell.CELL_TYPE_NUMERIC){
                                BigDecimal bd = new BigDecimal(c.getNumericCellValue()).setScale(0);
                                code = String.valueOf(bd);
                            }
                            IMGWVatRate t = IMGWVatRate.findByIdm(code);
                            zrodlo.setStawkaVAT(t.getId());
                        }
                        break;
                    case 7:
                        c = row.getCell(cellNum, Row.RETURN_BLANK_AS_NULL);
                        if (c != null) {
                            BigDecimal t = null;
                            if (c.getCellType() == Cell.CELL_TYPE_STRING) {
                                String kwotaVAT = c.getRichStringCellValue().getString();
                                t = new BigDecimal(kwotaVAT).setScale(2);
                            } else if (c.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                                double kwotaVAT = c.getNumericCellValue();
                                t = new BigDecimal(kwotaVAT).setScale(2);
                            }
                            zrodlo.setKwotaVAT(t);
                        }
                        break;
                    default:
                }
            }
            if (budgetCode != null && positionName != null) {
                Budget budzet = Budget.findByBudzetIdmAndPozBudzetNazwa(budgetCode, positionName);
                zrodlo.setBudget((int) budzet.getBdBudzetId());
                zrodlo.setPosition((int) budzet.getBdPozBudzetId());
                if (sourceCode != null) {
                    Map<String, Object> fieldValues = new HashMap<String, Object>(2);
                    fieldValues.put("bdBudzetPozId", budzet.getBdPozBudzetId());
                    fieldValues.put("zrodloIdn", sourceCode);
                    List<Source> sourceList = DictionaryUtils.findByGivenFieldValues(Source.class, fieldValues);
                    if (!sourceList.isEmpty()) {
                        Source source = sourceList.get(0);
                        zrodlo.setSource((int) source.getId());
                    } else {
                        DSApi.close();
                        DwrAttachmentStorage.getInstance().removeAll(WebContextFactory.get().getSession(), IMPORTED_DOCUMENT_CN);
                        throw new EdmException("B��d importu. Dla podanej pozycji bud�etowej " + budgetCode + positionName +
                                " - nie znaleziono �r�d�a o kodzie " + sourceCode);
                    }
                }
            }
            zrodlo.save();
            zrodlaIds.add(zrodlo.getId());
        }
        try {
            PreparedStatement ps = null;
            ps = DSApi.context().prepareStatement("delete from dsg_imgw_faktura_zakupowa_multiple_value " +
                    "where DOCUMENT_ID = ? and FIELD_CN = 'ZRODLO_FINANSOWANIA'");
            ps.setLong(1, fm.getDocumentId());
            ps.executeUpdate();
            DSApi.context().closeStatement(ps);
            String insert = "insert into dsg_imgw_faktura_zakupowa_multiple_value (document_id,field_cn,field_val) " +
                    "values (?, 'ZRODLO_FINANSOWANIA', ?)";
            for (Long id : zrodlaIds) {
                ps = DSApi.context().prepareStatement(insert);
                ps.setLong(1, fm.getDocumentId());
                ps.setLong(2, id);
                ps.executeUpdate();
                DSApi.context().closeStatement(ps);

            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            DSApi.close();
            DwrAttachmentStorage.getInstance().removeAll(WebContextFactory.get().getSession(), IMPORTED_DOCUMENT_CN);
            throw new EdmException("B��d importu. B��d z po��czeniem do bazy danych");
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            DSApi.close();
            DwrAttachmentStorage.getInstance().removeAll(WebContextFactory.get().getSession(), IMPORTED_DOCUMENT_CN);
            throw new EdmException("B��d importu. Patrz logi.");
        }

        DSApi.close();
        Map<String, FieldData> zrodlaValues = new LinkedHashMap<String, FieldData>();
        for (int i = 1; i <= zrodlaIds.size(); i++) {
            zrodlaValues.put(ImgwDocumentLogic.ZRODLO_FINANSOWANIA_CN + "_ID_" + i, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, zrodlaIds.get(i-1)));
        }
        values.get(FakturaZakupowaLogic.DWR_ZRODLO_FINANSOWANIA).setDictionaryData(zrodlaValues);
        DwrAttachmentStorage.getInstance().removeAll(WebContextFactory.get().getSession(), IMPORTED_DOCUMENT_CN);
    }
}
