package pl.compan.docusafe.parametrization.liberty;

import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class UpdateBudget implements ExternalActivityBehaviour
{
	protected static Logger log = LoggerFactory.getLogger(UpdateBudget.class);

	@Override
	public void execute(ActivityExecution execution) throws Exception {
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		String guid = doc.getFieldsManager().getEnumItem("BUDZET").getCn();
		
		// update tej wartosci
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

}
