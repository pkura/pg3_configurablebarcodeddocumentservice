package pl.compan.docusafe.parametrization.liberty;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.parametrization.AbstractParametrization;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;

public class LibertyParametrization extends AbstractParametrization
{

	private final static Logger LOG = LoggerFactory.getLogger(LibertyParametrization.class);

	public void createDocFromOcr(Document doc, Map<String, Object> toReload, Long docId) throws IOException, DocumentException, ParseException, EdmException
	{
		Element root = doc.getRootElement();
		for (Iterator i = root.elementIterator(); i.hasNext();)
		{
			Element client = (Element) i.next();
			Element ocrDoc = client.element("_FAKTURA");

			// pobranie numeru faktury
			String nrFaktury = ocrDoc.elementTextTrim("_NR_FAKTURY");
			LOG.info("Nr faktury: {}", TextUtils.trimmedStringOrNull(nrFaktury, 50));
			toReload.put("NR_FAKTURY", TextUtils.trimmedStringOrNull(nrFaktury, 50));

			// Data wystawienia
			String dataWystawienia = ocrDoc.elementTextTrim("_DATA_WYSTAWIENIA").replace(".", "-");
			LOG.info("Data wystawienia: {}", dataWystawienia);
			try
			{
				Date dWystawienia = DateUtils.parseDateAnyFormat(dataWystawienia);
				toReload.put("DATA_WYSTAWIENIA", dWystawienia);
			}
			catch (Exception e)
			{
				LOG.warn("DocId: " + docId + ": " + e.getMessage(), e);
			}

			// Wystawca faktury
			String wystawcaNazwa = ocrDoc.elementTextTrim("_SENDER_ORGANIZATION");
			LOG.info("wystawcaNazwa: {}", wystawcaNazwa);
			String wystawcaNIP = ocrDoc.elementTextTrim("_SENDER_NIP");
			wystawcaNIP = TextUtils.trimmedStringOrNull(wystawcaNIP, 15);
			if (wystawcaNIP != null && wystawcaNIP.length() > 0)
				wystawcaNIP =wystawcaNIP.replace("-", "").replace(" ", "");
			LOG.info("wystawcaNIP: {}", wystawcaNIP);
			String wystawcaZip = ocrDoc.elementTextTrim("_SENDER_ZIP");
			LOG.info("wystawcaZip: {}", wystawcaZip);
			String wystawcaLocation = ocrDoc.elementTextTrim("_SENDER_LOCATION");
			LOG.info("wystawcaLocation: {}", wystawcaLocation);
			String wystawcaStreet = ocrDoc.elementTextTrim("_SENDER_STREET");
			LOG.info("wystawcaStreet: {}", wystawcaStreet);
			String wystawcaNrKonta = ocrDoc.elementTextTrim("_SENDER_NRKONTA"); // lparam
			LOG.info("wystawcaNrKonta: {}", wystawcaNrKonta);
			toReload.put("NR_KONTA", wystawcaNrKonta.replace(" ", ""));
			Person person = null;
			if (wystawcaNIP != null && wystawcaNIP.length() > 0)
				person = Person.findPersonByNip(wystawcaNIP);
			if (person == null)
			{
				person = new Person();
				person.setOrganization(TextUtils.trimmedStringOrNull(wystawcaNazwa, 50));
				person.setStreet(TextUtils.trimmedStringOrNull(wystawcaStreet, 50));
				person.setNip(TextUtils.trimmedStringOrNull(wystawcaNIP, 15));
				person.setLocation(TextUtils.trimmedStringOrNull(wystawcaLocation, 50));
				person.setLparam(TextUtils.trimmedStringOrNull(wystawcaNrKonta, 200));
				person.setZip(TextUtils.trimmedStringOrNull(wystawcaZip, 14));
				person.setDictionaryGuid("rootdivision");
				person.create();
			}
			toReload.put("SENDER", person.getId());

			// kwota netto faktury
			try
			{
				String netto = ocrDoc.elementTextTrim("_NETTO").replace(",", ".");
				LOG.info("netto: {}", netto);
				toReload.put("NETTO", new BigDecimal(netto).setScale(2, RoundingMode.HALF_UP));
			}
			catch (Exception e)
			{
				LOG.warn("DocId: " + docId + ": " + e.getMessage(), e);
			}

			// brutto
			try
			{
				String brutto = ocrDoc.elementTextTrim("_BRUTTO").replace(",", ".");
				LOG.info("brutto: {}", brutto);
				toReload.put("BRUTTO", new BigDecimal(brutto).setScale(2,RoundingMode.HALF_UP));
			}
			catch (Exception e)
			{
				LOG.warn("DocId: " + docId + ": " + e.getMessage(), e);
			}

			// vat
			try
			{
				BigDecimal vat = ((BigDecimal) toReload.get("BRUTTO")).subtract((BigDecimal) toReload.get("NETTO"));
				LOG.info("vat: {}", vat);
				toReload.put("VAT", vat.setScale(2,RoundingMode.HALF_UP));
			}
			catch (Exception e)
			{
				LOG.warn("DocId: " + docId + ": " + e.getMessage(), e);
			}
			
			try
			{
				BigDecimal netto = (BigDecimal)toReload.get("NETTO");
				BigDecimal brutto =   (BigDecimal)toReload.get("BRUTTO");
				
				if (!(netto.setScale(2, RoundingMode.HALF_UP).multiply(brutto.setScale(2, RoundingMode.HALF_UP)).compareTo(BigDecimal.ZERO) == -1)
						&& netto.setScale(2, RoundingMode.HALF_UP).compareTo(brutto.setScale(2, RoundingMode.HALF_UP)) == 1)
							toReload.remove("BRUTTO");

			}
			catch (Exception e)
			{
				LOG.warn("DocId: " + docId + ": " + e.getMessage(), e);
			}

		}

	}

	public void createDocFromAutostacja(Document doc, Map<String, Object> toReload) throws IOException, DocumentException, ParseException, EdmException
	{
		Element root = doc.getRootElement();

		Element document = root.element("DOKUMENTY").element("DOKUMENT");

		// Numer fakturty
		String nrDokumentu = document.attributeValue("NUMER");
		toReload.put("NR_FAKTURY", nrDokumentu);
		for (Object el : document.elements())
			LOG.info("ELNAME: {} - value: {}", ((Element) el).getName(), ((Element) el).getTextTrim());
		// Data wystawienia
		String dataWystawienia = document.element("DATA_WYSTAWIENIA").getTextTrim();
		toReload.put("DATA_WYSTAWIENIA", DateUtils.parseDateAnyFormat(dataWystawienia));

		// Data ???
		String dataSprzedazy = document.element("DATA_SPRZEDAZY").getTextTrim();

		// Data ???
		String dataWydania = document.element("DATA_WYDANIA").getTextTrim();

		// Data platnosci
		String terminPlatnosci = document.element("PLATNOSC_TERMIN").getTextTrim();
		toReload.put("DATA_PLATNOSCI", DateUtils.parseDateAnyFormat(terminPlatnosci));

		// Waluta
		String waluta = document.element("WALUTA").attributeValue("KOD");
		if (waluta.equals("PLN"))
			toReload.put("WALUTA", 290);

		// Uwagi - brak odpowiednika na dokumencie Docusafe
		String uwagi = document.element("UWAGI").getTextTrim();

		// WYSTAWCA: NAZWA, NIP, ADRES - brak odpowiedniak na dokumncie
		// Docusafe
		Element wystawca = document.element("WYSTAWCA");
		for (Object el : wystawca.elements())
			LOG.info("Wystawca: ELNAME: {} - value: {}", ((Element) el).getName(), ((Element) el).getTextTrim());
		String wystawcaNazwa = wystawca.element("NAZWA").getTextTrim();
		String wystawcaNIP = wystawca.element("NIP").getTextTrim();
		String wystawcaAdres = wystawca.element("ADRES").getTextTrim();

		// ODBIORCA: NAZWA, NIP, ADRES
		Element odbiorca = document.element("ODBIORCA");
		String odbiorcaaNazwa = odbiorca.element("NAZWA").getTextTrim();
		String odbiorcaNIP = odbiorca.element("NIP").getTextTrim();
		String odbiorcaAdres = odbiorca.element("ADRES").getTextTrim();
		for (Object el : odbiorca.elements())
			LOG.info("Odbiorca: ELNAME: {} - value: {}", ((Element) el).getName(), ((Element) el).getTextTrim());

		Person person = Person.findByNip(odbiorcaNIP);
		if (person == null)
		{
			person = new Person();
			// DSApi.context().begin();
			person.setOrganization(odbiorcaaNazwa);
			person.setStreet(odbiorcaAdres);
			person.setNip(odbiorcaNIP);
			person.setDictionaryGuid("rootdivision");
			// person.setDictionaryType( Person.DICTIONARY_SENDER);
			person.create();
			// DSApi.context().commit();
		}
		toReload.put("SENDER", person.getId());

		// WARTOSC: NETTO, BRUTTO, PODATEK
		Element wartosc = document.element("WARTOSC");
		// Netto
		String wartoscNetto = wartosc.element("NETTO").getTextTrim().replace(",", ".");
		toReload.put("NETTO", new BigDecimal(wartoscNetto));

		// Brutto
		String wartoscBrutto = wartosc.element("BRUTTO").getTextTrim().replace(",", ".");
		toReload.put("BRUTTO", new BigDecimal(wartoscBrutto));

		// Vat
		String wartoscPodatek = wartosc.element("PODATEK").getTextTrim().replace(",", ".");
		toReload.put("VAT", new BigDecimal(wartoscPodatek));

	}
}
