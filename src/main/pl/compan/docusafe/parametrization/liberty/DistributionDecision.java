package pl.compan.docusafe.parametrization.liberty;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.record.projects.ProjectEntry;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class DistributionDecision implements DecisionHandler
{
	private final static Logger log = LoggerFactory.getLogger(DistributionDecision.class);
	
	public String decide(OpenExecution openExecution)
	{
		Long docId = Jbpm4Utils.getDocumentId(openExecution);
		Document document;
		try {
			document = Document.find(docId);
			
			// aktualizacja budzety w danym wpisie w rejestrze projektow ktory odpowadia wybrnemu budzetowi
			updateBudget(document);
			
			String filia = document.getFieldsManager().getEnumItem("FILIA").getCn();
			if (filia.equals("FD"))
				return "dystrybucja";
			else if (filia.equals("ACS"))
				return "acs";
			else				
				return "moto";
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			return "error";
		}
	}

	private void updateBudget(Document document) throws EdmException
	{
		FieldsManager fm = document.getFieldsManager();
		List<Long> costIds = (List<Long>)fm.getKey("MPK");
		
		CentrumKosztowDlaFaktury cost = null; 
		Integer entryId = null;
		BigDecimal amount = null, total = null, toSpend = null;
		ProjectEntry entry = null;
		Map<Integer, BigDecimal> projectsCost = new HashMap<Integer, BigDecimal>();
		for (Long id : costIds)
		{
			cost = Finder.find(CentrumKosztowDlaFaktury.class, id);
			entryId = cost.getCentrumId();
			if (entryId == null)
				continue;
			amount = cost.getRealAmount();
			projectsCost.put(entryId, projectsCost.containsKey(entryId) ? projectsCost.get(entryId).add(amount) : amount);
		}
		for (Long id : costIds)
		{
			cost = Finder.find(CentrumKosztowDlaFaktury.class, id);
			entryId = cost.getCentrumId();
			if (entryId == null)
				continue;
			amount = projectsCost.get(entryId);
			entry = ProjectEntry.find(entryId.longValue());
			// budzet w ramach tego budzetu
			total = entry.getGross();
			// aktualizacja budzetu
			toSpend = total.subtract(amount);
			entry.setGross(toSpend);
		}		
	}
}
