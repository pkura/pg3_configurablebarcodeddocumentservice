package pl.compan.docusafe.parametrization.liberty;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.record.projects.ProjectEntry;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CorrectMpkCost implements ExternalActivityBehaviour
{

	private static final Logger log = LoggerFactory.getLogger(CorrectMpkCost.class);

	public void execute(ActivityExecution execution) throws Exception
	{
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Document document;
		try {
			document = Document.find(docId);
			
			// aktualizacja budzety w danym wpisie w rejestrze projektow ktory odpowadia wybrnemu budzetowi
			updateBudget(document);
			
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}
	
	private void updateBudget(Document document) throws EdmException
	{
		FieldsManager fm = document.getFieldsManager();
		List<Long> costIds = (List<Long>)fm.getKey("MPK");
		
		CentrumKosztowDlaFaktury cost = null; 
		Integer entryId = null;
		BigDecimal amount = null, total = null, toSpend = null;
		ProjectEntry entry = null;
		Map<Integer, BigDecimal> projectsCost = new HashMap<Integer, BigDecimal>();
		for (Long id : costIds)
		{
			cost = Finder.find(CentrumKosztowDlaFaktury.class, id);
			entryId = cost.getCentrumId();
			if (entryId == null)
				continue;
			amount = cost.getRealAmount();
			projectsCost.put(entryId, projectsCost.containsKey(entryId) ? projectsCost.get(entryId).add(amount) : amount);
		}
		for (Long id : costIds)
		{
			cost = Finder.find(CentrumKosztowDlaFaktury.class, id);
			entryId = cost.getCentrumId();
			if (entryId == null)
				continue;
			amount = projectsCost.get(entryId);
			entry = ProjectEntry.find(entryId.longValue());
			// budzet w ramach tego budzetu
			total = entry.getGross();
			// aktualizacja budzetu
			toSpend = total.subtract(amount);
			entry.setGross(toSpend);
		}		
	}

	public void signal(ActivityExecution execution, String signalName, Map<String, ?> parameters) throws Exception
	{
		execution.takeDefaultTransition();
	}
}
