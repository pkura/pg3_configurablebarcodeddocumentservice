package pl.compan.docusafe.parametrization.liberty;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Map;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import com.google.common.collect.Maps;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.ocr.OcrUtils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.AbstractParametrization;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class SalesInvoiceLogic extends InvoiceLogic
{
	private static Logger log = LoggerFactory.getLogger(SalesInvoiceLogic.class);
	private static SalesInvoiceLogic instance;

	public static SalesInvoiceLogic getInstance()
	{
		if (instance == null)
			instance = new SalesInvoiceLogic();
		return instance;
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Faktury sprzedażowe");
		
		try
		{
			folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(fm.getValue("DATA_WYSTAWIENIA")));
			
			if (fm.getEnumItem("REJESTR") != null)
				folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getEnumItem("REJESTR").getTitle()));
			else
				folder = folder.createSubfolderIfNotPresent("Rejestr nieokreślony");
			
			folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(fm.getValue("DATA_WYSTAWIENIA")));
			

			document.setTitle("Faktura sprzedażowa nr " + String.valueOf(fm.getValue("NR_FAKTURY")));
			document.setDescription("Faktura sprzedażowa nr " + String.valueOf(fm.getValue("NR_FAKTURY")));
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
		finally
		{
			document.setFolder(folder);
		}
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		log.info("--- SalesInvoiceLogic : START-END PROCESS !!! ---- {}", event);
		getFromAutostacja(document);
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn() + "_DOCUMENT_READ", ObjectPermission.GROUP, "Faktura sprzedażowa - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Faktura sprzedażowa zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn() + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Faktura sprzedażowa - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Faktura sprzedażowa zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn() + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Faktura sprzedażowa - usuwanie"));
		this.setUpPermission(document, perms);
	}

	private void getFromAutostacja(Document doc) throws EdmException
	{
		try
		{
			for (Attachment att : doc.getAttachments())
			{
				String oldType = OcrUtils.fileExtension(att.getMostRecentRevision().getOriginalFilename());
				if (oldType.toLowerCase().equals("xml"))
				{
					createDoc(att.getMostRecentRevision().getBinaryStream(), doc);
					break; // tylko jeden
				}
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException("Błąd importu dokumentu z Autostacji");
		}
	}

	private void createDoc(InputStream inputStream, Document docu) throws IOException, DocumentException, ParseException, EdmException
	{

		Map<String, Object> toReload = Maps.newHashMap();

		SAXReader reader = new SAXReader();
		org.dom4j.Document doc = reader.read(inputStream);
		AbstractParametrization.getInstance().createDocFromAutostacja(doc, toReload);
		docu.getDocumentKind().setOnly(docu.getId(), toReload);

	}
}
