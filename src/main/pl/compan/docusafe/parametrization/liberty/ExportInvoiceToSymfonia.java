package pl.compan.docusafe.parametrization.liberty;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Properties;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;

public class ExportInvoiceToSymfonia implements ExternalActivityBehaviour
{
	protected static Logger log = LoggerFactory.getLogger(ExportInvoiceToSymfonia.class);
	private static final char[] ILLEGAL_CHARACTERS = { '/', '\n', '\r', '\t', '\0', '\f', '`', '?', '*', '\\', '<', '>', '|', '\"', ':' };
	
	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		String templateSourceLibertyInvoiceIntegration = Docusafe.getAdditionProperty("templateSourceLibertyInvoiceIntegration");
		if (templateSourceLibertyInvoiceIntegration == null)
			throw new EdmException("Brak zdefiniownaego parametru templateSourceLibertyInvoiceIntegration, okreslającego scieszke do szablonu templateLibertyInvoiceIntegration.vm");
		
		String outputLibertyInvoiceIntegration = Docusafe.getAdditionProperty("outputLibertyInvoiceIntegration");
		if (outputLibertyInvoiceIntegration == null)
			throw new EdmException("Brak zdefiniownaego parametru outputLibertyInvoiceIntegration, okreslającego scieszke zapisu ");

		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		
		
		log.debug("templateSourceLibertyInvoiceIntegration: {}", templateSourceLibertyInvoiceIntegration);
		
		//Template temp = ve.getTemplate("templateLibertyInvoiceIntegration.vm");
		
		VelocityContext context = new VelocityContext();

		// pobranie kontrahent/wystawce faktury
		Person contractor = doc.getSender();
		
		// ustawienie danych kontrahenta
		putContractorValueToContext(context, contractor);

		// ustawienie pozostałych danych do eksportu
		putOtherData(context, fm, contractor);
		
		String content = applyTemplate(getTemplate("templateLibertyInvoiceIntegration.vm"), context);
		
		// Zapis
		log.debug(outputLibertyInvoiceIntegration);
		log.debug( fm.getValue("NR_FAKTURY").toString());
		String nrFaktury = fm.getValue("NR_FAKTURY").toString();
		for (char c : ILLEGAL_CHARACTERS)
		{
			nrFaktury = nrFaktury.replace(c, '_');
		}
		File file = new File(outputLibertyInvoiceIntegration + "/"+ nrFaktury);
		FileWriter fw = new FileWriter(file);
		fw.write(content);
		fw.flush();
		fw.close();
	}

	private void putContractorValueToContext(VelocityContext context, Person contractor) throws EdmException
	{
		// nazwa kontrahenta
		String conOrganization = contractor.getOrganization();
		context.put("conNazwa", conOrganization != null ? conOrganization : "");
		
		// ulica kontrahenta,
		String conStreet = contractor.getStreet();
		context.put("conAdres", conStreet != null ? conStreet : "");
				
		// kod poczotwy kontrahenta
		String conZip = contractor.getZip();
		context.put("conKod", conZip != null ? conZip : "");

		// miasto kontrahenta
		String conLocation = contractor.getLocation();
		context.put("conMiejscowosc", conLocation != null ? conLocation : "");

		// Nazwa kraj kontrahenta
		String conCountry = contractor.getCountry();
		if (conCountry != null)
			conCountry = DataBaseEnumField.getEnumItemForTable("dsr_country", Integer.parseInt(conCountry)).getTitle();
		context.put("conKraj", conCountry != null ? conCountry : "");

		// Kod kraj kontrahenta
		String conKrajKod = contractor.getCountry();
		if (conKrajKod != null)
			conKrajKod = DataBaseEnumField.getEnumItemForTable("dsr_country", Integer.parseInt(conKrajKod)).getCn();
		context.put("conKrajKod", conKrajKod != null ? conKrajKod : "");
		
		// mail kontrahenta
		String conEmail = contractor.getEmail();
		context.put("conEmail", conEmail != null ? conEmail : "");
		
		// faks kontrahenta
		String conFax = contractor.getFax();
		context.put("conFaks", conFax != null ? conFax : "");

		// nip kontrahenta
		String conNip = contractor.getNip();
		if (conNip == null)
			throw new EdmException("Brak NIP Kontrahenta!!!");
		context.put("conNip", conNip);
		
		// pesel kontrahenta
		String conPesel= contractor.getPesel();
		context.put("conPesel", conPesel != null ? conPesel : "");

		// regon kontrahenta
		String conRegon = contractor.getRegon();
		context.put("conRegon", conRegon != null ? conRegon : "");
		
		// Imie kontrahenta
		String conImie = contractor.getFirstname();
		context.put("conImie", conImie != null ? conImie : "");
		
		// Nazwisko kontrahenta
		String conNazwisko = contractor.getLastname();
		context.put("conNazwisko", conNazwisko != null ? conNazwisko : "");
		
		// Nr konta bankowego kontrahenta
		String conKonto = contractor.getLparam();
		context.put("conKonto", conKonto != null ? conKonto : "");

	}
	
	private void putOtherData(VelocityContext context, FieldsManager fm, Person contractor ) throws EdmException
	{
		context.put("idKontrahent", contractor.getId()+65535);
		context.put("kValue", contractor.getId());
		context.put("numerFaktury", fm.getValue("NR_FAKTURY"));
		context.put("numerKonta", fm.getValue("NUMER_KONTA"));
		 // commonDateFormat.format(date);
		context.put("dataWplywu", DateUtils.sqlDateFormat.format(fm.getValue("DATA_WPLYWU")));
		context.put("dataPlatnosci", DateUtils.sqlDateFormat.format(fm.getValue("DATA_PLATNOSCI")));
		context.put("dataWystawienia", DateUtils.sqlDateFormat.format(fm.getValue("DATA_WYSTAWIENIA")));
		context.put("kwotaNetto", fm.getValue("NETTO"));
		context.put("kwotaBrutto", fm.getValue("BRUTTO"));
		context.put("kwotaVat", fm.getValue("VAT"));
		if ( fm.getEnumItem("REJESTR") != null )
			context.put("rejestr", fm.getEnumItem("REJESTR").getCn());
		
		if (fm.getKey("MPK") != null)
			context.put("MpkIOpis", fm.getKey("MPK") + " " + fm.getValue("NR_FAKTURY"));
		else
			context.put("MpkIOpis", "");
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	 public static Reader getTemplate(String template) throws IOException
	    {
	        return new InputStreamReader(
	            new FileInputStream(
	                new File(new File(Docusafe.getHome(), "templates"), template)), Charset.forName(Docusafe.getAdditionProperty("mail.charset")));
	    }

	 private String applyTemplate(Reader msg, VelocityContext context) throws EdmException {
	        String content;StringWriter sw = new StringWriter();
	        try
	        {
	            VelocityEngine ve = new VelocityEngine();
	            ve.init();
	            ve.evaluate(context, sw, "Mailer", msg);
	        }
	        catch (Exception e)
	        {
	            throw new EdmException(e.getMessage(), e);
	        }

	        content = sw.toString();
	        return content;
	    }
}
