package pl.compan.docusafe.parametrization.liberty;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.dbutils.DbUtils;
import org.directwebremoting.WebContextFactory;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import com.google.common.collect.Maps;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.record.projects.ProjectEntry;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class AbstractInvoiceLogic extends AbstractDocumentLogic
{
	private static Logger log = LoggerFactory.getLogger(AbstractInvoiceLogic.class);
	private final String ACCEPT_MPK = "budget_acceptance";

	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		Map<String, Object> toReload = Maps.newHashMap();
		for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
		{
			if (f.getDefaultValue() != null)
			{
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
			}
		}
		log.info("toReload = {}", toReload);
		fm.reloadValues(toReload);
	}

	public void documentPermissions(Document document) throws EdmException
	{
		throw new EdmException("ABSTRACT!!!");
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		log.info("--- AbstractInvoiceLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn)
	{
		boolean assigned = false;
		try
		{
			log.info("DokumentKind: {}, DokumentId: {}', acceptationCn: {}, enumCn: {}", doc.getDocumentKind().getCn(), doc.getId(), accpetionCn, enumCn);

			// akceptacja mpk
			if (accpetionCn.equals(ACCEPT_MPK))
				assigned = assigneToAcceptMpk(assignable, openExecution, accpetionCn);

		}
		catch (Exception ee)
		{
			log.error(ee.getMessage(), ee);
		}
		return assigned;
	}

	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
	{
		StringBuilder msgBuilder = new StringBuilder();

		// kwota netto nie moze byc wieksza nic kwota brutto
		if (fieldValues.get("NETTO") != null && fieldValues.get("BRUTTO") != null)
			msgBuilder.append(checkNettoBrutto(fieldValues));

		// kwota netto rowna sumie kwot mpk
		if (fieldValues.get("NETTO") != null && fieldValues.get("") != null)
			msgBuilder.append(checkNettoSumMpk(fieldValues));

		if (fieldValues.get("STATUS") != null && fieldValues.get("STATUS").equals("105"))
		{
			OfficeDocument doc = OfficeDocument.find(documentId);
			FieldsManager fm = doc.getFieldsManager();
			List<Long> costIds = (List<Long>)fm.getKey("MPK");
			
			CentrumKosztowDlaFaktury cost = null; 
			Integer entryId = null;
			BigDecimal amount = null, total = null, toSpend = null;
			ProjectEntry entry = null;
			String info = "";
			Map<Integer, BigDecimal> projectsCost = new HashMap<Integer, BigDecimal>();
			for (Long id : costIds)
			{
				cost = Finder.find(CentrumKosztowDlaFaktury.class, id);
				entryId = cost.getCentrumId();
				if (entryId == null)
					continue;
				info = "Brak �rodk�w";
				amount = cost.getRealAmount();
				projectsCost.put(entryId, projectsCost.containsKey(entryId) ? projectsCost.get(entryId).add(amount) : amount);
				entry = ProjectEntry.find(entryId.longValue());
				total = entry.getGross();
				if (total.compareTo(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP)) != 0)
					info = amount.multiply(new BigDecimal(100).setScale(2, RoundingMode.HALF_UP)).divide(total.setScale(2, RoundingMode.HALF_UP), RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_UP) + "%";
				cost.setStoragePlace(total.toString());
				cost.setAccountNumber(info);
			}
		}
		
		if (msgBuilder.length() > 0)
			throw new EdmException(msgBuilder.toString());
	}

	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		log.info("DokumentKind: {}, DocumentId: {}, Values from JavaScript: {}", fm.getDocumentKind().getCn(), fm.getDocumentId(), values);
		StringBuilder msgBuilder = new StringBuilder();

		if(values.get("DWR_NR_FAKTURY") != null  && values.get("DWR_NR_FAKTURY").getData() != null &&
				values.get("DWR_SENDER") != null  && values.get("DWR_SENDER").getDictionaryData().get("id") != null  
				&& ((String)values.get("DWR_SENDER").getDictionaryData().get("id").getData()).length() > 0 )
		{
			
			String duplicate = getInvoiceByNumberAndContractor((String)values.get("DWR_NR_FAKTURY").getData(),Long.parseLong((String)values.get("DWR_SENDER").getDictionaryData().get("id").getData()));
			if(duplicate != null)
			{
				msgBuilder.append("Istniej ju� faktura dla tego kontrahenta o numerze:");
				msgBuilder.append(duplicate);
			}
		}
		
		try 
		{
			// kwota netto nie moze byc wieksza niz brutto, wiliczanie kwoty vat
			if (values.get("DWR_NETTO") != null && values.get("DWR_BRUTTO").getData() != null)
				msgBuilder.append(validateNettoBrutto(values));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		
		if (msgBuilder.length() > 0)
		{
			values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
			return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
		}

		return null;
	}
	
	public String getInvoiceByNumberAndContractor(String invoice,Long contractor) throws EdmException
	{
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try
    	{
    		HttpServletRequest req = WebContextFactory.get().getHttpServletRequest();
			DSApi.open(AuthUtil.getSubject(req));
    		ps = DSApi.context().prepareStatement("" +
    				"select nr_faktury from dsg_liberty_costinvoice f, dso_person p where nr_faktury = ? and p.document_id = f.document_id and p.basepersonid = ? "+
    				"union "+
    				"select nr_faktury from dsg_liberty_costinvfixedassets f, dso_person p where nr_faktury = ?  and  p.document_id = f.document_id and p.basepersonid = ? "+
    				"union "+
    				"select nr_faktury from dsg_liberty_costinvtrans f, dso_person p where nr_faktury = ? and  p.document_id = f.document_id and p.basepersonid = ? "+
    				"union "+
    				"select nr_faktury from dsg_liberty_costinvoiceofsales f, dso_person p where nr_faktury = ? and  p.document_id = f.document_id and p.basepersonid = ? "+
    				"union "+
    				"select nr_faktury from dsg_liberty_salesinvoice f, dso_person p where nr_faktury = ? and  p.document_id = f.document_id and p.basepersonid = ? "+
    				"union "+
    				"select nr_faktury from dsg_liberty_constinvoice f, dso_person p where nr_faktury = ? and  p.document_id = f.document_id and p.basepersonid = ? "+
    				"union "+
    				"select nr_faktury from dsg_liberty_commercialinvoice f, dso_person p where nr_faktury = ? and p.document_id = f.document_id and p.basepersonid = ? ");
    		for (int i = 1; i < 15; i++) {
				ps.setString(i, invoice);
				i++;
				ps.setLong(i, contractor);
			}
    		rs = ps.executeQuery();
    		if(rs.next())
    		{
    			return rs.getString(1);
    		}

    	}
    	catch (Exception e) 
    	{
			log.error("",e);
		}
    	finally
    	{
    		DbUtils.closeQuietly(rs);
    		DbUtils.closeQuietly(ps);
    		DSApi.close();
    	}
    	return null;
	}


	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		try
		{
			Document doc = Document.find(docId);
			FieldsManager fm = doc.getFieldsManager();
		}
		catch (EdmException e)
		{
			log.error(e.getMessage());
		}
	}

	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			FieldsManager fm = dockind.getFieldsManager(id);

			// Ustawienie statusy dokumentu
			params.setStatus(fm.getValue("STATUS") != null ? (String) fm.getValue("STATUS") : null);

			// Nr faktury jako nr dokumentu
			params.setDocumentNumber((fm.getValue("NR_FAKTURY") != null ? (String) fm.getValue("NR_FAKTURY") : null));

			// data wystawienia faktury jako data dokumentu
			params.setDocumentDate((fm.getValue("DATA_WYSTAWIENIA") != null ? (Date) fm.getValue("DATA_WYSTAWIENIA") : null));

		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return params;
	}

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{

	}

	public void specialSetDictionaryValues(Map<String, ?> dockindFields)
	{
		log.info("Special dictionary values: {}", dockindFields);

	}

	public void addSpecialInsertDictionaryValues(String dictionaryName, Map<String, FieldData> values)
	{
		log.info("Before Insert - Special dictionary: '{}' values: {}", dictionaryName, values);

	}

	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		// UMOWA UMOWA_TYTULUMOWY=tytul, UMOWA_NRUMOWY=123
		log.info("Before Search - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.equals("UMOWA"))
		{
			values.put("UMOWA_RODZAJ", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.LONG, 100));
		}
	}


	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values)
	{
		Map<String, Object> results = new HashMap<String, Object>();

		try
		{
			if (dictionaryName.equals("MPK"))
			{
				// wysokosc budzetu
				String entryId = values.get("MPK_CENTRUMID").getEnumValuesData().getSelectedOptions().get(0);
				if (entryId != null && !entryId.equals(""))
				{
				ProjectEntry entry = ProjectEntry.find(Long.valueOf(entryId));
				BigDecimal total = entry.getGross();
				results.put("MPK_STORAGEPLACE", total.toString());
				
				// wykozystanie budzetu
				BigDecimal amount = ((FieldData) values.get("MPK_AMOUNT")).getMoneyData();
				String info = "Brak �rodk�w";
				if (total.compareTo(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP)) != 0)
					info = amount.multiply(new BigDecimal(100).setScale(2, RoundingMode.HALF_UP)).divide(total.setScale(2, RoundingMode.HALF_UP), RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_UP) + "%";
				results.put("MPK_ACCOUNTNUMBER", info);
				}
				else
				{
					results.put("MPK_STORAGEPLACE", null);
					results.put("MPK_ACCOUNTNUMBER", null);
				}
			}
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
		return results;
	}

	private String checkNettoBrutto(Map<String, ?> fieldValues)
	{
		String msg = "";
		BigDecimal netto = null;
		BigDecimal brutto = null;
		if (fieldValues.get("NETTO") instanceof String)
			netto = new BigDecimal(((String)fieldValues.get("NETTO")).replaceAll(",", ".").replaceAll(" ", ""));
		else
			netto = (BigDecimal) fieldValues.get("NETTO");
		if (fieldValues.get("BRUTTO") instanceof String)
			brutto = new BigDecimal(((String)fieldValues.get("BRUTTO")).replaceAll(",", ".").replaceAll(" ", ""));
		else
			brutto = (BigDecimal) fieldValues.get("BRUTTO");
		if (!(netto.setScale(2, RoundingMode.HALF_UP).multiply(brutto.setScale(2, RoundingMode.HALF_UP)).compareTo(BigDecimal.ZERO) == -1)
				&& netto.setScale(2, RoundingMode.HALF_UP).compareTo(brutto.setScale(2, RoundingMode.HALF_UP)) == 1)
				msg = "B��d: Kwota brutto nie mo�e by� mniejsza ni� kwota netto";

		return msg;
	}

	private boolean assigneToAcceptMpk(Assignable assignable, OpenExecution openExecution, String accpetionCn) throws EdmException
	{
		boolean assigned = false;
		String assigneeTo = (String) openExecution.getVariable("assigneeTo");
		log.info("assigneeTo: {}", assigneeTo);

		if (assigneeTo.contains("u:"))
		{
			assignable.addCandidateUser(assigneeTo.replace("u:", ""));
			assigned = true;
		}
		else if (assigneeTo.contains("d:"))
		{
			assignable.addCandidateGroup(assigneeTo.replace("d:", ""));
			assigned = true;
		}
		return assigned;
	}

	private String validateNettoBrutto(Map<String, FieldData> values)
	{
		BigDecimal netto = values.get("DWR_NETTO").getMoneyData();
		BigDecimal brutto = values.get("DWR_BRUTTO").getMoneyData();
		BigDecimal vat = BigDecimal.ZERO;
		String msg = "";
		log.error("netto.compareTo(BigDecimal.ZERO) == -1 : {}", netto.compareTo(BigDecimal.ZERO) == -1);
		log.error("brutto.compareTo(BigDecimal.ZERO) == -1 : {}", brutto.compareTo(BigDecimal.ZERO) == -1);
		if (netto.compareTo(BigDecimal.ZERO) == -1 || brutto.compareTo(BigDecimal.ZERO) == -1)
			return msg;
		if (netto.setScale(2, RoundingMode.HALF_UP).compareTo(brutto.setScale(2, RoundingMode.HALF_UP)) == 1)
			msg = "B��d: Kwota brutto nie mo�e by� mniejsza ni� kwota netto";
		else
		{
			vat = brutto.subtract(netto);
			values.put("DWR_VAT", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.MONEY, vat));
		}
		return msg;
	}

	private String checkNettoSumMpk(Map<String, ?> fieldValues) throws EdmException
	{
		String msg = "";
		BigDecimal netto = (BigDecimal) fieldValues.get("NETTO");
		BigDecimal sum = BigDecimal.ZERO;

		List<Long> mpkIds = (List<Long>) fieldValues.get("MPK");
		for (Long id : mpkIds)
		{
			BigDecimal oneAmount = new BigDecimal((Double) DwrDictionaryFacade.dictionarieObjects.get("MPK").getValues(id.toString()).get("MPK" + "_AMOUNT"));
			sum = sum.add(oneAmount);
		}

		if (netto.setScale(2, RoundingMode.HALF_UP).compareTo(sum.setScale(2, RoundingMode.HALF_UP)) != 0)
			msg = "B��d: Suma kwot poszczegolnych MPK musi by� r�wna kwocie netto!";

		return msg;
	}

}
