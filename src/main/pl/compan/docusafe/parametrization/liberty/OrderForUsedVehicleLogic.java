package pl.compan.docusafe.parametrization.liberty;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.record.projects.ProjectEntry;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import com.google.common.collect.Maps;

public class OrderForUsedVehicleLogic extends AbstractDocumentLogic
{
	private static Logger log = LoggerFactory.getLogger(OrderForUsedVehicleLogic.class);
	
	private static OrderForUsedVehicleLogic instance;

	public static OrderForUsedVehicleLogic getInstance()
	{
		if (instance == null)
			instance = new OrderForUsedVehicleLogic();
		return instance;
	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn)
	{
		boolean assigned = false;
		try
		{
			log.error("DokumentKind: {}, DokumentId: {}', acceptationCn: {}, enumCn: {}", doc.getDocumentKind().getCn(), doc.getId(), accpetionCn, enumCn);
			
		}
		catch (Exception ee)
		{
			log.error(ee.getMessage(), ee);
		}
		return assigned;
	}
	
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		log.info("DokumentKind: {}, DocumentId: {}, Values from JavaScript: {}", fm.getDocumentKind().getCn(), fm.getDocumentId(), values);
		
		try
		{
			DSApi.openAdmin();
			// wysokosc budzetu
			String entryId = values.get("DWR_BUDGET").getEnumValuesData().getSelectedOptions().get(0);
			ProjectEntry entry = ProjectEntry.find(Long.valueOf(entryId));
			BigDecimal total = entry.getGross();
			values.put("DWR_STORAGEPLACE", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, total.toString()));

			// wykozystanie budzetu
			BigDecimal amount = ((FieldData) values.get("DWR_AMOUNT")).getMoneyData();
			if (amount==null)
				values.put("DWR_ACCOUNTNUMBER", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, null));
			else
			{
				String info = "Brak �rodk�w";
				if (total.compareTo(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP)) != 0)
					info = amount.multiply(new BigDecimal(100).setScale(2, RoundingMode.HALF_UP)).divide(total.setScale(2, RoundingMode.HALF_UP), RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_UP) + "%";
				values.put("DWR_ACCOUNTNUMBER", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, info));
			}
		}
		catch (Exception e)
		{
			log.warn("Lapiemy to: " + e.getMessage(), e);
		}
		finally
		{
			DSApi.close();
		}
		
		return null;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn() + "_DOCUMENT_READ", ObjectPermission.GROUP, "Zam�wienie na zakup pojazd�w u�ywanych - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Zam�wienie na zakup pojazd�w u�ywanych zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn() + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Zam�wienie na zakup pojazd�w u�ywanych - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Zam�wienie na zakup pojazd�w u�ywanych zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn() + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Zam�wienie na zakup pojazd�w u�ywanych - usuwanie"));
		this.setUpPermission(document, perms);
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		Map<String, Object> toReload = Maps.newHashMap();
		for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
		{
			if (f.getDefaultValue() != null)
			{
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
			}
		}
		log.info("toReload = {}", toReload);
		fm.reloadValues(toReload);
	}
	
	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		log.info("--- OrderForUsedVehicleLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
			
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}


	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		try
		{
			Document doc = Document.find(docId);
			FieldsManager fm = doc.getFieldsManager();
		}
		catch (EdmException e)
		{
			log.error(e.getMessage());
		}
	}

	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			FieldsManager fm = dockind.getFieldsManager(id);
			// Ustawienie statusy dokumentu
			params.setStatus(fm.getValue("STATUS") != null ? (String) fm.getValue("STATUS") : null);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return params;
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException 
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Zam�wienia na zakup pojazd�w u�ywanych ");
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(document.getCtime()));
		folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getEnumItem("CENTRUMID").getTitle().substring(0, 3)));
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(document.getCtime()));
		document.setFolder(folder);

		document.setTitle("Zam�wienie na zakup pojazd�w u�ywanych " + String.valueOf(fm.getValue("NR_ZAMOWIENIA")));
		document.setDescription(String.valueOf(fm.getValue("MARKA")));
	}
	
	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
	{

	}
}