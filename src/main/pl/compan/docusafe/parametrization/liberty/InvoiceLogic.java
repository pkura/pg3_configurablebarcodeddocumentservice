package pl.compan.docusafe.parametrization.liberty;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.dbutils.DbUtils;
import org.directwebremoting.WebContextFactory;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import com.google.common.collect.Maps;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DockindScheme;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.SchemeField;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.record.projects.ProjectEntry;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class InvoiceLogic extends AbstractDocumentLogic
{
	private static Logger log = LoggerFactory.getLogger(InvoiceLogic.class);
	private final String ACCEPT_MPK = "budget_acceptance";
	private final String FAKTURA_TOWAROWA_CN = "commercialinvoice";
	
	private static InvoiceLogic instance;

	public static InvoiceLogic getInstance()
	{
		if (instance == null)
			instance = new InvoiceLogic();
		return instance;
	}

	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		Map<String, Object> toReload = Maps.newHashMap();
		for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
		{
			if (f.getDefaultValue() != null)
			{
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
			}
		}
		log.info("toReload = {}", toReload);
		fm.reloadValues(toReload);
	}

	public void documentPermissions(Document document) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

		String invoiceCn = "INVOICE", invoiceDescription = "Faktura";
		try
		{
			EnumItem invoiceItem = fm.getEnumItem("INVOICE_TYPE");
			invoiceCn = invoiceItem.getCn();
			invoiceDescription = invoiceItem.getTitle();

			java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
			perms.add(new PermissionBean(ObjectPermission.READ, invoiceCn.toLowerCase() + "_DOCUMENT_READ", ObjectPermission.GROUP, invoiceDescription + " - odczyt"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, invoiceCn.toLowerCase() + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, invoiceDescription + " zalacznik - odczyt"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, invoiceCn.toLowerCase() + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, invoiceDescription + " - modyfikacja"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, invoiceCn.toLowerCase() + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, invoiceDescription + " zalacznik - modyfikacja"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, invoiceCn.toLowerCase() + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, invoiceDescription + " - usuwanie"));
			this.setUpPermission(document, perms);
		}
		catch (Exception e)
		{
			log.error(e.getMessage());
			throw new EdmException(e.getMessage(), e);
		}

	}

	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		log.info("--- AbstractInvoiceLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

			DSUser author = DSApi.context().getDSUser();
			String user = author.getName();
			String fullName = author.getLastname() + " " + author.getFirstname();

			perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

			Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
			perms.addAll(documentPermissions);
			this.setUpPermission(document, perms);
			DSApi.context().watch(URN.create(document));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn)
	{
		boolean assigned = false;
		try
		{
			log.info("DokumentKind: {}, DokumentId: {}', acceptationCn: {}, enumCn: {}", doc.getDocumentKind().getCn(), doc.getId(), accpetionCn, enumCn);

			// akceptacja mpk
			if (ACCEPT_MPK.equals(accpetionCn))
				assigned = assigneToAcceptMpk(assignable, openExecution, accpetionCn);

		}
		catch (Exception ee)
		{
			log.error(ee.getMessage(), ee);
		}
		return assigned;
	}

	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
	{
		StringBuilder msgBuilder = new StringBuilder();

		// kwota netto nie moze byc wieksza nic kwota brutto
		if (fieldValues.get("NETTO") != null && fieldValues.get("BRUTTO") != null)
			msgBuilder.append(checkNettoBrutto(fieldValues));

		OfficeDocument doc = OfficeDocument.find(documentId);
		FieldsManager fm = doc.getFieldsManager();
		
		// kwota netto rowna sumie kwot mpk
		if (fieldValues.get("NETTO") != null && fieldValues.get("") != null)
			msgBuilder.append(checkNettoSumMpk(fieldValues, fm));

		if (fieldValues.get("STATUS") != null && fieldValues.get("STATUS").equals("105"))
		{
			
			List<Long> costIds = null;
			if (fm.getEnumItem("INVOICE_TYPE").getCn().equals("commercialInvoice"))
			{
				costIds = new ArrayList<Long>();
				Long costID = (Long) fm.getKey("MPK");
				costIds.add(costID);
			}
			else
				costIds = (List<Long>) fm.getKey("MPK");

			CentrumKosztowDlaFaktury cost = null;
			Integer entryId = null;
			BigDecimal amount = null, total = null, toSpend = null;
			ProjectEntry entry = null;
			String info = "";
			Map<Integer, BigDecimal> projectsCost = new HashMap<Integer, BigDecimal>();
			for (Long id : costIds)
			{
				cost = Finder.find(CentrumKosztowDlaFaktury.class, id);
				entryId = cost.getCentrumId();
				if (entryId == null)
					continue;
				info = "Brak �rodk�w";
				amount = cost.getRealAmount();
				projectsCost.put(entryId, projectsCost.containsKey(entryId) ? projectsCost.get(entryId).add(amount) : amount);
				entry = ProjectEntry.find(entryId.longValue());
				total = entry.getGross();
				if (total.compareTo(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP)) != 0)
					info = amount.multiply(new BigDecimal(100).setScale(2, RoundingMode.HALF_UP)).divide(total.setScale(2, RoundingMode.HALF_UP), RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_UP) + "%";
				cost.setStoragePlace(total.toString());
				cost.setAccountNumber(info);
			}
		}

		if (msgBuilder.length() > 0)
			throw new EdmException(msgBuilder.toString());
	}

	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		log.info("DokumentKind: {}, DocumentId: {}, Values from JavaScript: {}", fm.getDocumentKind().getCn(), fm.getDocumentId(), values);
		StringBuilder msgBuilder = new StringBuilder();

		if (values.get("DWR_NR_FAKTURY") != null && values.get("DWR_NR_FAKTURY").getData() != null && values.get("DWR_SENDER") != null && values.get("DWR_SENDER").getDictionaryData().get("id") != null && ((String) values.get("DWR_SENDER").getDictionaryData().get("id").getData()).length() > 0)
		{

			String duplicate = getInvoiceByNumberAndContractor((String) values.get("DWR_NR_FAKTURY").getData(), Long.parseLong((String) values.get("DWR_SENDER").getDictionaryData().get("id").getData()));
			if (duplicate != null)
			{
				msgBuilder.append("Istniej ju� faktura dla tego kontrahenta o numerze:");
				msgBuilder.append(duplicate);
			}
		}

		try
		{
			// kwota netto nie moze byc wieksza niz brutto, wiliczanie kwoty
			// vat
			if (values.get("DWR_NETTO") != null && values.get("DWR_BRUTTO").getData() != null)
				msgBuilder.append(validateNettoBrutto(values));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}

		// uzupelnianie kwoty netto mpk dla faktur towarowych
		try
		{
			if (fm.getEnumItem("INVOICE_TYPE") != null && fm.getEnumItem("INVOICE_TYPE").getCn().equals("commercialInvoice") && values.get("DWR_NETTO") != null && values.get("DWR_NETTO").getData() != null)
			{
				BigDecimal netto = (BigDecimal) values.get("DWR_NETTO").getData();
				FieldData mpkFields = values.get("DWR_MPK");
				Map<String, FieldData> mpkValues = (HashMap<String, FieldData>) mpkFields.getData();
				Long mpkId = null;
				CentrumKosztowDlaFaktury mpk = null;
				DSApi.openAdmin();
				DSApi.context().begin();
				if (mpkValues.get("id").getData() != null && !mpkValues.get("id").getData().equals(""))
				{
					mpkId = Long.valueOf(mpkValues.get("id").getData().toString());
					mpk = CentrumKosztowDlaFaktury.getInstance().find(mpkId);
					mpk.setAmount(netto);
					setMpkBudget(mpk, mpkValues);
				}
				else
				{
					mpk = new CentrumKosztowDlaFaktury();
					mpk.setAmount(netto);
					setMpkBudget(mpk, mpkValues);
					Persister.create(mpk);
				}
				DSApi.context().commit();
				DSApi.close();
				mpkId = mpk.getId();
				mpkValues.put("id", new FieldData(Field.Type.STRING, mpkId));
				mpkFields.setDictionaryData(mpkValues);
				values.put("DWR_MPK", mpkFields);
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		if (msgBuilder.length() > 0)
		{
			values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
			return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
		}

		return null;
	}

	private void setMpkBudget(CentrumKosztowDlaFaktury mpk, Map<String, FieldData> mpkValues) throws NumberFormatException, EdmException
	{
		// wysokosc budzetu
		String entryId = mpkValues.get("MPK_CENTRUMID").getEnumValuesData().getSelectedOptions().get(0);
		if (entryId != null && !entryId.equals(""))
		{
			mpk.setCentrumId(Integer.valueOf(entryId));
			ProjectEntry entry = ProjectEntry.find(Long.valueOf(entryId));
			BigDecimal total = entry.getGross();
			mpk.setStoragePlace(total.toString());

			// wykozystanie budzetu
			BigDecimal amount = mpk.getRealAmount();
			String info = "Brak �rodk�w";
			if (total.compareTo(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP)) != 0)
				info = amount.multiply(new BigDecimal(100).setScale(2, RoundingMode.HALF_UP)).divide(total.setScale(2, RoundingMode.HALF_UP), RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_UP) + "%";
			mpk.setAccountNumber(info);
		}
		String filiaId = mpkValues.get("MPK_ACCEPTINGCENTRUMID").getEnumValuesData().getSelectedOptions().get(0);
		if (filiaId != null && !filiaId.equals(""))
			mpk.setAcceptingCentrumId(Integer.valueOf(filiaId));
		String kontoId = mpkValues.get("MPK_CENTRUMCODE").getEnumValuesData().getSelectedOptions().get(0);
		if (kontoId != null && !kontoId.equals(""))
			mpk.setCentrumCode(kontoId);
	}

	public String getInvoiceByNumberAndContractor(String invoice, Long contractor) throws EdmException
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			HttpServletRequest req = WebContextFactory.get().getHttpServletRequest();
			DSApi.open(AuthUtil.getSubject(req));
			ps = DSApi.context().prepareStatement(
					"select nr_faktury from dsg_liberty_invoice f, dso_person p where nr_faktury = ? and p.document_id = f.document_id and p.basepersonid = ? "
							+ " union "
							+ "select nr_faktury from dsg_liberty_commercialinvoice f, dso_person p where nr_faktury = ?  and  p.document_id = f.document_id and p.basepersonid = ?");

			ps.setString(1, invoice);
			ps.setLong(2, contractor);
			ps.setString(3, invoice);
			ps.setLong(4, contractor);
			rs = ps.executeQuery();
			if (rs.next())
			{
				return rs.getString(1);
			}

		}
		catch (Exception e)
		{
			log.error("", e);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
			DSApi.close();
		}
		return null;
	}

	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		try
		{
			Document doc = Document.find(docId);
			FieldsManager fm = doc.getFieldsManager();
		}
		catch (EdmException e)
		{
			log.error(e.getMessage());
		}
	}

	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			FieldsManager fm = dockind.getFieldsManager(id);

			// Ustawienie statusy dokumentu
			params.setStatus(fm.getValue("STATUS") != null ? (String) fm.getValue("STATUS") : null);

			// Nr faktury jako nr dokumentu
			params.setDocumentNumber((fm.getValue("NR_FAKTURY") != null ? (String) fm.getValue("NR_FAKTURY") : null));

			// data wystawienia faktury jako data dokumentu
			params.setDocumentDate((fm.getValue("DATA_WYSTAWIENIA") != null ? (Date) fm.getValue("DATA_WYSTAWIENIA") : null));

			// Kontrahent
			OfficeDocument doc = OfficeDocument.find(id);
			Sender kontrahnet = doc.getSender();
			if (kontrahnet != null)
			params.setAttribute(kontrahnet.getOrganization(), 0);

			try
			{
				// Kwota netto
				params.setAttribute(fm.getValue("NETTO").toString(), 1);
			}
			catch (Exception e)
			{
				log.error(e.getMessage() + e.getLocalizedMessage());
			}
			try
			{
				
				BigDecimal netto = (BigDecimal)fm.getKey("NETTO");
				params.setAmount(netto.setScale(2, RoundingMode.HALF_UP));
				log.error("AMOUNT: {}", params.getAmount());
			}
			catch (Exception e)
			{
				log.error(e.getMessage() + e.getLocalizedMessage());
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return params;
	}

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

		Folder folder = Folder.getRootFolder();
		try
		{
			EnumItem invoiceTypeItem = fm.getEnumItem("INVOICE_TYPE");

			String invoiceTitle = getInvoiceTypeItem(invoiceTypeItem);

			folder = folder.createSubfolderIfNotPresent(invoiceTitle);
			folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(fm.getValue("DATA_WYSTAWIENIA")));

			if (fm.getEnumItem("REJESTR") != null)
				folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getEnumItem("REJESTR").getTitle()));
			else
				folder = folder.createSubfolderIfNotPresent("Rejestr nieokre�lony");

			folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(fm.getValue("DATA_WYSTAWIENIA")));

			document.setTitle(invoiceTypeItem.getTitle() + " nr " + String.valueOf(fm.getValue("NR_FAKTURY")));
		}
		catch (Exception e)
		{
			log.warn(e.getMessage());
		}
		finally
		{
			document.setFolder(folder);
		}

	}

	private String getInvoiceTypeItem(EnumItem invoiceTypeItem)
	{
		int type = invoiceTypeItem.getId();

		switch (type)
		{
			case 121:
				return "Faktury kosztowe";
			case 122:
				return "Faktury sta�e";
			case 123:
				return "Faktura kosztowa (�rodki trwa�e, marketing, reklama)";
			case 124:
				return "Faktury kosztowe za po�rednictwem sprzeda�y";
			case 125:
				return "Faktury kosztowe transport";
			case 126:
				return "Faktury towarowe";
		}
		return "Faktury";
	}

	public void specialSetDictionaryValues(Map<String, ?> dockindFields)
	{
		log.info("Special dictionary values: {}", dockindFields);

	}

	public void addSpecialInsertDictionaryValues(String dictionaryName, Map<String, FieldData> values)
	{
		log.info("Before Insert - Special dictionary: '{}' values: {}", dictionaryName, values);

	}

	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		// UMOWA UMOWA_TYTULUMOWY=tytul, UMOWA_NRUMOWY=123
		log.info("Before Search - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.equals("UMOWA"))
		{
			values.put("UMOWA_RODZAJ", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.LONG, 100));
		}
	}

	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values)
	{
		Map<String, Object> results = new HashMap<String, Object>();

		try
		{
			if (dictionaryName.equals("MPK"))
			{
				// wysokosc budzetu
				String entryId = values.get("MPK_CENTRUMID").getEnumValuesData().getSelectedOptions().get(0);
				if (entryId != null && !entryId.equals(""))
				{
					ProjectEntry entry = ProjectEntry.find(Long.valueOf(entryId));
					BigDecimal total = entry.getGross();
					results.put("MPK_STORAGEPLACE", total.toString());

					// wykozystanie budzetu
					BigDecimal amount = ((FieldData) values.get("MPK_AMOUNT")).getMoneyData();
					String info = "Brak �rodk�w";
					if (total.compareTo(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP)) != 0)
						info = amount.multiply(new BigDecimal(100).setScale(2, RoundingMode.HALF_UP)).divide(total.setScale(2, RoundingMode.HALF_UP), RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_UP) + "%";
					results.put("MPK_ACCOUNTNUMBER", info);
				}
				else
				{
					results.put("MPK_STORAGEPLACE", null);
					results.put("MPK_ACCOUNTNUMBER", null);
				}
			}
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
		return results;
	}

	private String checkNettoBrutto(Map<String, ?> fieldValues)
	{
		String msg = "";
		BigDecimal netto = null;
		BigDecimal brutto = null;
		if (fieldValues.get("NETTO") instanceof String)
			netto = new BigDecimal(((String) fieldValues.get("NETTO")).replaceAll(",", ".").replaceAll(" ", ""));
		else
			netto = (BigDecimal) fieldValues.get("NETTO");
		if (fieldValues.get("BRUTTO") instanceof String)
			brutto = new BigDecimal(((String) fieldValues.get("BRUTTO")).replaceAll(",", ".").replaceAll(" ", ""));
		else
			brutto = (BigDecimal) fieldValues.get("BRUTTO");
		
		if (!(netto.compareTo(BigDecimal.ZERO) == -1 || brutto.compareTo(BigDecimal.ZERO) == -1) && netto.setScale(2, RoundingMode.HALF_UP).compareTo(brutto.setScale(2, RoundingMode.HALF_UP)) == 1)
			msg = "B��d: Kwota brutto nie mo�e by� mniejsza ni� kwota netto";

		return msg;
	}

	private boolean assigneToAcceptMpk(Assignable assignable, OpenExecution openExecution, String accpetionCn) throws EdmException
	{
		boolean assigned = false;
		String assigneeTo = (String) openExecution.getVariable("assigneeTo");
		log.info("assigneeTo: {}", assigneeTo);

		if (assigneeTo.contains("u:"))
		{
			assignable.addCandidateUser(assigneeTo.replace("u:", ""));
			assigned = true;
		}
		else if (assigneeTo.contains("d:"))
		{
			assignable.addCandidateGroup(assigneeTo.replace("d:", ""));
			assigned = true;
		}
		return assigned;
	}

	private String validateNettoBrutto(Map<String, FieldData> values)
	{
		BigDecimal netto = values.get("DWR_NETTO").getMoneyData();
		BigDecimal brutto = values.get("DWR_BRUTTO").getMoneyData();
		BigDecimal vat = BigDecimal.ZERO;
		String msg = "";
		if (!(netto.compareTo(BigDecimal.ZERO) == -1 || brutto.compareTo(BigDecimal.ZERO) == -1) && netto.setScale(2, RoundingMode.HALF_UP).compareTo(brutto.setScale(2, RoundingMode.HALF_UP)) == 1)
			msg = "B��d: Kwota brutto nie mo�e by� mniejsza ni� kwota netto";
		else
		{
			vat = brutto.subtract(netto);
//			values.put("DWR_VAT", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.MONEY, vat));
			values.get("DWR_VAT").setMoneyData(vat);
		}
		return msg;
	}

	private String checkNettoSumMpk(Map<String, ?> fieldValues, FieldsManager fm) throws EdmException
	{
		String msg = "";
		BigDecimal netto = (BigDecimal) fieldValues.get("NETTO");
		BigDecimal sum = BigDecimal.ZERO;

		List<Long> mpkIds = null;
		if (fm.getEnumItem("INVOICE_TYPE").getCn().equals("commercialInvoice"))
		{
			mpkIds = new ArrayList<Long>();
			Long costID = (Long) fm.getKey("MPK");
			mpkIds.add(costID);
		}
		else
			mpkIds = (List<Long>) fm.getKey("MPK");
		for (Long id : mpkIds)
		{
			BigDecimal oneAmount = new BigDecimal((Double) DwrDictionaryFacade.dictionarieObjects.get("MPK").getValues(id.toString()).get("MPK" + "_AMOUNT"));
			sum = sum.add(oneAmount);
		}

		if (netto.setScale(2, RoundingMode.HALF_UP).compareTo(sum.setScale(2, RoundingMode.HALF_UP)) != 0)
			msg = "B��d: Suma kwot poszczegolnych MPK musi by� r�wna kwocie netto!";

		return msg;
	}

	@Override
	public void setAdditionalValues(FieldsManager fieldsManager, Integer valueOf, String activity) 
	{
		if (fieldsManager.getDocumentKind().getCn().equals(FAKTURA_TOWAROWA_CN))
			return;
		try {
			
			DockindScheme dockindScheme = fieldsManager.getDocumentKind().getSheme(fieldsManager);
			Map<String, SchemeField> schemeFields;
			boolean mpkDisabled = false;
			if (dockindScheme != null) 
			{
				schemeFields = dockindScheme.getSchemeFields();
				SchemeField mpkScheme = schemeFields.get("MPK");
				
				if (mpkScheme != null) 
					mpkDisabled = mpkScheme.getDisabled(); // isDisabled()
			}

			if (mpkDisabled) {
				List<Long> mpkIds = new ArrayList<Long>();
				Object mpk = fieldsManager.getFieldValues().get("MPK");
				if (mpk != null) {
					if (mpk instanceof List<?>) {
						mpkIds = (List<Long>) fieldsManager.getFieldValues().get("MPK");
					} else if (mpk instanceof Long) {
						mpkIds.add((Long)mpk);
					}
				}
				
				String statusCn = fieldsManager.getEnumItem("STATUS").getCn();
				List<Long> mpkIdsToShow = new ArrayList<Long>();
				DSUser currUser = DSApi.context().getDSUser();
				
				if (mpkIds.size()>0 && !currUser.isAdmin() && statusCn.equals("budget_acceptance")) {
					Set<String> userNames = new HashSet<String>();
					Set<String> userGuids = new HashSet<String>();
					
					userNames.add(currUser.getName());
					
					if (currUser.getDivisionsGuid() != null) {
						Collections.addAll(userGuids, currUser.getDivisionsGuid());
					}
					
					DSUser[] subsUsers = currUser.getSubstitutedUsers();
					
					if (subsUsers != null) {
						for (DSUser u : subsUsers) {
							userNames.add(u.getName());
							if (u.getDivisionsGuid() != null) {
								Collections.addAll(userGuids, u.getDivisionsGuid());
							}
						}
					}
					
					String filiaId = null, filiaGuid = null;
					for (Long di : mpkIds) 
					{
						filiaId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("MPK").getValues(di.toString()).get("MPK_ACCEPTINGCENTRUMID"))
								.getSelectedOptions().get(0);
						filiaGuid = DataBaseEnumField.getEnumItemForTable("dsg_liberty_filie_view", Integer.valueOf(filiaId)).getCn();
						for (AcceptanceCondition accept : AcceptanceCondition.find(ACCEPT_MPK, filiaGuid)) {
							if (accept.getUsername() != null && userNames.contains(accept.getUsername())) {
								mpkIdsToShow.add(di);
								break;
							} else if (accept.getDivisionGuid() != null && userGuids.contains(accept.getDivisionGuid())) {
								mpkIdsToShow.add(di);
								break;
							}
						}
					}
					if (mpkIdsToShow.size() > 0) 
					{
						if (mpk instanceof List<?>) {
							((List<Long>) mpk).clear();
							((List<Long>) mpk).addAll(mpkIdsToShow);
						} else if (mpk instanceof Long) {
							if (mpkIdsToShow.size() > 0) {
								mpk = mpkIdsToShow.get(0);
							}
						}
					}
				}
			}
		} catch (EdmException e) {
			log.error(e.getMessage());
		}
	}
}
