package pl.compan.docusafe.parametrization.liberty;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class FiliaDecision implements DecisionHandler
{
	private final static Logger log = LoggerFactory.getLogger(DistributionDecision.class);
	
	public String decide(OpenExecution openExecution)
	{
		Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Document document;
		try {
			document = Document.find(docId);
			
			String filia = document.getFieldsManager().getEnumItem("CENTRUMID").getTitle();
			if (filia.equals("MAR") || filia.equals("MLO") || filia.equals("PMO") || filia.equals("Projekty Internetowe"))
				return "moto";
			else
				return "other";
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			return "error";
		}
	}
}
