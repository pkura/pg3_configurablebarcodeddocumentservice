package pl.compan.docusafe.parametrization.liberty;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.record.projects.ProjectEntry;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ChangeMpkCost implements ExternalActivityBehaviour
{
	protected static Logger log = LoggerFactory.getLogger(ChangeMpkCost.class);
	String type = "add";
	String dic = "true";

	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		
		// zdjecie ostation dodanych kosztow do
		updateBudget(doc);

		execution.takeDefaultTransition();
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		// TODO Auto-generated method stub

	}

	private void updateBudget(Document document) throws EdmException
	{
		FieldsManager fm = document.getFieldsManager();
		List<Long> costIds = null;
		CentrumKosztowDlaFaktury cost = null;
		Integer entryId = null;
		BigDecimal amount = null, total = null, toSpend = null;
		ProjectEntry entry = null;
		Map<Integer, BigDecimal> projectsCost = new HashMap<Integer, BigDecimal>();
		
		if (dic.equals("true"))
		{
			if (fm.getEnumItem("INVOICE_TYPE").getCn().equals("commercialInvoice"))
			{
				costIds = new ArrayList<Long>();
				Long costID = (Long)fm.getKey("MPK");
				costIds.add(costID);
			}
			else
				costIds = (List<Long>) fm.getKey("MPK");
			
			for (Long id : costIds)
			{
				log.error("id: "+ id);	
				cost = Finder.find(CentrumKosztowDlaFaktury.class, id);
				entryId = cost.getCentrumId();
				if (entryId == null)
					continue;
				amount = cost.getRealAmount();
				projectsCost.put(entryId, projectsCost.containsKey(entryId) ? projectsCost.get(entryId).add(amount) : amount);
			}
			for (Long id : costIds)
			{
				log.error("id: "+ id);				
				cost = Finder.find(CentrumKosztowDlaFaktury.class, id);
				entryId = cost.getCentrumId();
				log.error("entryId: {}", entryId);
				if (entryId == null)
					continue;
				amount = projectsCost.get(entryId);
				log.error("AMOUnt: {}", amount);
				entry = ProjectEntry.find(entryId.longValue());
				// budzet w ramach tego budzetu
				total = entry.getGross();
				log.error("total: {}", total);
				// aktualizacja budzetu
				if (type.equals("add"))
					toSpend = total.add(amount);
				else
					toSpend = total.subtract(amount);
				entry.setGross(toSpend);
			}
		}	
		else
		{
			entryId = (Integer)fm.getKey("BUDGET");
			if (entryId != null && !entryId.equals("")) {
			    amount = (BigDecimal)fm.getKey("AMOUNT");
				entry =  ProjectEntry.find(entryId.longValue());
				// budzet w ramach tego budzetu
				total = entry.getGross();
				// aktualizacja budzetu
				if (type.equals("add"))
					toSpend = total.add(amount);
				else
					toSpend = total.subtract(amount);
				entry.setGross(toSpend);
			}
		}

	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}
}
