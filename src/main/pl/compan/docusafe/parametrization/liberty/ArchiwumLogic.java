package pl.compan.docusafe.parametrization.liberty;

import java.sql.SQLException;
import java.util.Date;
import java.util.Map;

import org.bouncycastle.asn1.x509.DSAParameter;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.PersonDictionary;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ArchiwumLogic extends AbstractDocumentLogic
{
	private static final ArchiwumLogic instance = new ArchiwumLogic();
	private static final Logger log = LoggerFactory.getLogger(ArchiwumLogic.class);

	public static ArchiwumLogic getInstance()
	{
		return instance;
	}

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{

		if (AvailabilityManager.isAvailable("addToWatch"))
			DSApi.context().watch(URN.create(document));

		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Dokumenty archiwalne");
		
		if(fm.getValue("FILIA") != null && ((String)fm.getValue("FILIA")).length() > 1)
		{
			folder = folder.createSubfolderIfNotPresent(""+fm.getValue("FILIA"));
		}
		
		folder = folder.createSubfolderIfNotPresent(fm.getEnumItem("RODZAJ").getTitle());
		String subType = getSubType(fm);

		if (subType.equals("RAPORT_KASOWY"))
		{
			try
			{
				String raportOd = DateUtils.jsDateFormat.format((Date) fm.getValue("RAPORTOD"));
				String raportDo = DateUtils.jsDateFormat.format((Date) fm.getValue("RAPORTDO"));
				subType = "Od " + raportOd + " do " + raportDo;
			}
			catch (Exception e)
			{
				log.warn(e.getMessage());
			}
		}
		else
			folder = folder.createSubfolderIfNotPresent(subType);

		if (fm.getKey("NAZWAKONTR") != null)
		{
			Person contractor = Person.find(((Long) fm.getKey("NAZWAKONTR")).longValue());
			folder = folder.createSubfolderIfNotPresent(contractor.getOrganization());
		}

		document.setFolder(folder);

		document.setTitle((fm.getValue("TYTULUMOWY") != null ? (String) fm.getValue("TYTULUMOWY") : "Brak tytu�u"));
		document.setDescription("Dokument archiwalny typu " + fm.getEnumItem("RODZAJ").getTitle() + " - " + subType + "Tytu�: " + (fm.getValue("TYTULUMOWY") != null ? (String) fm.getValue("TYTULUMOWY") : "BRAK"));
	}

	private String getSubType(FieldsManager fm) throws EdmException
	{
		switch (fm.getEnumItem("RODZAJ").getId())
		{
			case 100:
				return fm.getEnumItem("UMOWY").getTitle();
			case 105:
				return fm.getEnumItem("AKTYNOTARIALNE").getTitle();
			case 110:
				return fm.getEnumItem("BUDYNK").getTitle();
			case 115:
				return fm.getEnumItem("REJESTR").getTitle();
			case 120:
				return fm.getEnumItem("UMOWYIC").getTitle();
			case 125:
				return fm.getEnumItem("UBEZPIECZ").getTitle();
			case 130:
				return fm.getEnumItem("SADOWE").getTitle();
			case 135:
				return fm.getEnumItem("DOTACJE").getTitle();
			case 140:
				return fm.getEnumItem("SPRAWPRAC").getTitle();
			case 145:
				return "RAPORT_KASOWY";
		}
		return "Nieokre�lony Rodzaj";
	}

	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
	{

	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn() + "DOCUMENT_READ", ObjectPermission.GROUP, "Dokument archiwalny - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn() + "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokument archiwalny zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn() + "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokument archiwalny - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn() + "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokument archiwalny zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn() + "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokument archiwalny - usuwanie"));
		this.setUpPermission(document, perms);
	}

	public void specialSetDictionaryValues(Map<String, ?> dockindFields)
	{
		log.info("Special dictionary values: {}", dockindFields);
		if (dockindFields.keySet().contains("NAZWAKONTR_DICTIONARY"))
		{
			Map<String, FieldData> m = (Map<String, FieldData>) dockindFields.get("NAZWAKONTR_DICTIONARY");
			m.put("DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			m.put("ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}

	}

	public void addSpecialInsertDictionaryValues(String dictionaryName, Map<String, FieldData> values)
	{
		log.info("Before Insert - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.contains("NAZWAKONTR"))
		{
			Person p = new Person();
			p.createBasePerson(dictionaryName, values);
			try
			{
				DSApi.context().begin();
				p.create();
				DSApi.context().commit();
			}
			catch (EdmException e)
			{
				log.error("", e);
			}

			PersonDictionary.prepareFieldNamesForSenderPerson(p.getId(), dictionaryName, values);
		}
	}

	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		log.info("Before Search - Special dictionary: '{}' values: {}", dictionaryName, values);

		log.info("Before Search - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.contains("NAZWAKONTR"))
		{
			values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}

}
