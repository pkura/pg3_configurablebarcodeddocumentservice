package pl.compan.docusafe.parametrization.liberty;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Map;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.ocr.OcrUtils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.AbstractParametrization;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import com.google.common.collect.Maps;

public class AutostacjaListener implements ExternalActivityBehaviour
{

	private final static Logger LOG = LoggerFactory.getLogger(AutostacjaListener.class);
	private OfficeDocument doc;

	@Override
	public void execute(ActivityExecution exec) throws EdmException
	{
		try
		{
			Long docId = Long.valueOf(exec.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			setDoc(OfficeDocument.find(docId));

			boolean sended = false;
			for (Attachment att : doc.getAttachments())
			{
				String oldType = OcrUtils.fileExtension(att.getMostRecentRevision().getOriginalFilename());
				if (oldType.toLowerCase().equals("xml"))
				{
					createDoc(att.getMostRecentRevision().getBinaryStream());
					break; // tylko jeden
				}
			}
		}
		catch (Exception e)
		{
			LOG.error(e.getMessage(), e);
			throw new EdmException("B��d importu dokumentu z Autostacji");
		}
	}

	private void createDoc(InputStream inputStream) throws IOException, DocumentException, ParseException, EdmException
	{

		Map<String, Object> toReload = Maps.newHashMap();

		SAXReader reader = new SAXReader();
		Document doc = reader.read(inputStream);
		AbstractParametrization.getInstance().createDocFromAutostacja(doc, toReload);

		// przeladowanie wartosci
		// DSApi.context().begin();
		getDoc().getDocumentKind().setOnly(getDoc().getId(), toReload);
		// DSApi.context().commit();

		// dodanie id dokumentu to poznijeszcego oswiezenia listy zadan dla
		// niego przez serwis
		// RefreshTaskList.addDocumentToRefresh(getDoc().getId());
	}

	public OfficeDocument getDoc()
	{
		return doc;
	}

	public void setDoc(OfficeDocument doc)
	{
		this.doc = doc;
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		// TODO Auto-generated method stub

	}

}
