package pl.compan.docusafe.parametrization.liberty;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SetVariablesForMpkFork implements ExternalActivityBehaviour
{
	private static final Logger log = LoggerFactory.getLogger(SetVariablesForMpkFork.class);
	private final String ACCEPT_MPK = "budget_acceptance";
	public void execute(ActivityExecution execution) throws Exception
	{
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Document document = Document.find(docId);
		// lista uztykiwnikow(u:), dzia�ow (d:)
		List<String> assignessTo = new LinkedList<String>();
		// id poszczegolnych wpisow
		FieldsManager fm = document.getFieldsManager();
		List<Long> dictionaryIds = null;
		if (fm.getEnumItem("INVOICE_TYPE").getCn().equals("commercialInvoice"))
		{
			dictionaryIds = new ArrayList<Long>();
			Long costID = (Long)fm.getKey("MPK");
			dictionaryIds.add(costID);
		}
		else
			dictionaryIds = (List<Long>) fm.getKey("MPK");
		// Je�eli dokument zawiera pozycje kosztowe
		if (dictionaryIds != null)
		{
			String filiaId = null, filiaGuid=null;
			for (Long di : dictionaryIds)
			{
				filiaId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("MPK").getValues(di.toString()).get("MPK_ACCEPTINGCENTRUMID")).getSelectedOptions().get(0);
				filiaGuid = DataBaseEnumField.getEnumItemForTable("dsg_liberty_filie_view", Integer.valueOf(filiaId)).getCn();
				for (AcceptanceCondition accept : AcceptanceCondition.find(ACCEPT_MPK, filiaGuid))
				{
					if (accept.getUsername() != null && !assignessTo.contains("u:"+accept.getUsername()))
						assignessTo.add("u:"+accept.getUsername());
					else if (accept.getDivisionGuid() != null && !assignessTo.contains("d:"+accept.getDivisionGuid()))
						assignessTo.add("d:"+accept.getDivisionGuid());
				}
				
			}
		}
		// ids - loginy, guidy do dekretacji
		execution.setVariable("ids", assignessTo);
		// liczba subtaskow na potrzeby forka
		execution.setVariable("count", assignessTo.size());
		log.info("DokumentKind: {}, DocumentId: {},Liczba subtaskow na potrzeby forka: {}", document.getDocumentKind().getCn(), document.getId(), assignessTo.size());
	} 

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		// TODO Auto-generated method stub

	}
}
