package pl.compan.docusafe.parametrization.liberty;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class CostInvoiceFixedAssetsAdvertisingLogic extends InvoiceLogic
{
	private static Logger log = LoggerFactory.getLogger(CostInvoiceFixedAssetsAdvertisingLogic.class);
	private static CostInvoiceFixedAssetsAdvertisingLogic instance;

	public static CostInvoiceFixedAssetsAdvertisingLogic getInstance()
	{
		if (instance == null)
			instance = new CostInvoiceFixedAssetsAdvertisingLogic();
		return instance;
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

		Folder folder = Folder.getRootFolder();

		folder = folder.createSubfolderIfNotPresent("Faktura kosztowa (�rodki trwa�e, marketing, reklama)");
		try
		{
			folder = folder.createSubfolderIfNotPresent(fm.getEnumItem("RODZAJ").getTitle());
			folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(fm.getValue("DATA_WYSTAWIENIA")));
			if (fm.getEnumItem("REJESTR") != null)
				folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getEnumItem("REJESTR").getTitle()));
			else
				folder = folder.createSubfolderIfNotPresent("Rejestr nieokre�lony");
			folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(fm.getValue("DATA_WYSTAWIENIA")));
			document.setFolder(folder);

			document.setTitle("Faktura kosztowa - " + fm.getEnumItem("RODZAJ").getTitle() + " nr " + String.valueOf(fm.getValue("NR_FAKTURY")));
			document.setDescription("Faktura kosztowa - " + fm.getEnumItem("RODZAJ").getTitle() + " nr " + String.valueOf(fm.getValue("NR_FAKTURY")));
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
		finally
		{
			document.setFolder(folder);
		}
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn() + "_DOCUMENT_READ", ObjectPermission.GROUP, "Faktura kosztowa (�rodki trwa�e...) - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Faktura kosztowa (�rodki trwa�e...) zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn() + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Faktura kosztowa (�rodki trwa�e...) - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Faktura kosztowa (�rodki trwa�e...) zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn() + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Faktura kosztowa (�rodki trwa�e...) - usuwanie"));
		this.setUpPermission(document, perms);
	}

}
