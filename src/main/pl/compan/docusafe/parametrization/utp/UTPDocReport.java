package pl.compan.docusafe.parametrization.utp;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.criteria.CriteriaBuilder.Case;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.hibernate.HibernateException;


import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.utp.UTPDocReport;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

public class UTPDocReport extends AbstractXlsReports
{
	private String komorka;
	private String symbol;
	private String haslo;
	private Date incomingDateFrom;
	private Date incomingDateTo;
	
	
	public UTPDocReport(String report){
		
		super(report);

		title = new String []
		{
		"Rok spraw w spisie",
		"Kom�rka organizacyjna",
		"symbol klasyfikacyjny z wykazu akt",
		"has�o klasyfikacyjne z wykazu akt"
		};
		
		columns = new String []
		{
			"Lp.",
			"Tytu�",
			"Nadawca",
			"Znak sprawy",
			"Data pisma wszczynajacego sprawe",
			"Data wszcz�cia sprawy",
			"Data ostatecznego za�atwienia sprawy",
			"Uwagi"
		};
		
	
		
	}
	
	protected void createEntries()
	{
		
		
		
		 try
	        {
	            int rowCount = 1;
	           
	            try {
	            	FromClause from = new FromClause();
	            	TableAlias containerDocTable = from.createTable("v_dso_container");
	            	TableAlias rwaDocTable = from.createJoinedTable("v_dso_rwaa", true, containerDocTable, FromClause.LEFT_OUTER, "$l.RWA_ID=$r.id");
	                TableAlias baseDocTable = from.createJoinedTable(DSApi.getTableName(InOfficeDocument.class), true, containerDocTable, FromClause.LEFT_OUTER, "$l.ID = $r.CASE_ID");
	                TableAlias divisionDocTable = from.createJoinedTable("v_ds_division", true, containerDocTable, FromClause.LEFT_OUTER, "$l.OFFICEIDPREFIX=$r.CODE");
	                
	                WhereClause where = new WhereClause();
	                
	                SelectClause selectId = new SelectClause(true);
	                SelectColumn colKO = selectId.add(baseDocTable, "officenumber");
	                SelectColumn colIncomingDate = selectId.add(baseDocTable, "incomingdate");
	                SelectColumn colOpis = selectId.add(containerDocTable, "description");
	                SelectColumn colNadawca = selectId.add(baseDocTable, "sender");
	                SelectColumn colZnakSprawy = selectId.add(containerDocTable, "OFFICEID");
	                SelectColumn colTytul = selectId.add(containerDocTable, "TITLE");
	                SelectColumn colOpenDate = selectId.add(containerDocTable, "OPENDATE");
	                SelectColumn colFinishDate = selectId.add(containerDocTable, "FINISHDATE");
	                SelectColumn colYear = selectId.add(containerDocTable, "CASE_YEAR");
	                SelectColumn colHaslo = selectId.add(rwaDocTable, "description");
	                SelectColumn colCode = selectId.add(rwaDocTable, "code");
	                SelectColumn colD = selectId.add(divisionDocTable, "CODE");
	                
	                 if(komorka != null && !komorka.equals(""))
	                	where.add(Expression.eq(divisionDocTable.attribute("CODE"), komorka));
	                
	                if(symbol != null && !symbol.equals(""))
	                	where.add(Expression.eq(rwaDocTable.attribute("code"), symbol));
	                
	                if(haslo != null && !haslo.equals(""))
	                	where.add(Expression.eq(rwaDocTable.attribute("description"), haslo));
	                
	                if (incomingDateFrom != null)
	                    where.add(Expression.ge(baseDocTable.attribute("incomingdate"), DateUtils.midnight(incomingDateFrom, 0)));

	                if (incomingDateTo != null)
	                    where.add(Expression.le(baseDocTable.attribute("incomingdate"), DateUtils.midnight(incomingDateTo, 1)));
	              
	                SelectQuery selectQuery = new SelectQuery(selectId, from, where, null);
	                
					ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
					
					
						int i = -1;
						if(rs.next())
						{
	                    HSSFRow row = sheet.createRow(rowCount++);
	                    
	                    Date rok = incomingDateFrom;
	                    if(rok!=null)
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(rok.toString().substring(0, 4)));
	                    else
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
	                    
	                    String komorkaOrganizacyjna = komorka;
	                    if(komorkaOrganizacyjna!=null)
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(komorkaOrganizacyjna));
	                    else
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
	                  
	                    String symbolKlasyfikacyjny = symbol;
	                    if(symbolKlasyfikacyjny!=null)
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(symbolKlasyfikacyjny));
	                    else
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
	                    
	                    String hasloKlasyfikacyjne = haslo;
	                    if(hasloKlasyfikacyjne!=null)
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(hasloKlasyfikacyjne));
	                    else
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
	                    
						}
				} catch (HibernateException e) {
		            Logger.getLogger(UTPDocReport.class.getName()).log(Level.SEVERE, null, e);
				} catch (SQLException e) {
		            Logger.getLogger(UTPDocReport.class.getName()).log(Level.SEVERE, null, e);
				}
	            
	        }
	        catch ( EdmException ex )
	        {
	            Logger.getLogger(UTPDocReport.class.getName()).log(Level.SEVERE, null, ex);
	        }

		 
	    
	
		 try
	        {
	            int rowCount = 3;
	            int lp = 1;
	            try {
	            	FromClause from = new FromClause();
	            	TableAlias containerDocTable = from.createTable("v_dso_container");
	            	TableAlias rwaDocTable = from.createJoinedTable("v_dso_rwaa", true, containerDocTable, FromClause.LEFT_OUTER, "$l.RWA_ID=$r.id");
	                TableAlias divisionDocTable = from.createJoinedTable("v_ds_division", true, containerDocTable, FromClause.LEFT_OUTER, "$l.OFFICEIDPREFIX=$r.CODE");

	                
	                WhereClause where = new WhereClause();
	                
	                SelectClause selectId = new SelectClause(true);

	                SelectColumn colNadawca = selectId.add(containerDocTable, "author");
	                SelectColumn colZnakSprawy = selectId.add(containerDocTable, "OFFICEID");
	                SelectColumn colTytul = selectId.add(containerDocTable, "TITLE");
	                SelectColumn colOpenDate = selectId.add(containerDocTable, "OPENDATE");
	                SelectColumn colFinishDate = selectId.add(containerDocTable, "FINISHDATE");
	                SelectColumn colCaseId = selectId.add(containerDocTable, "id");
	                
	           
	                if(komorka != null && !komorka.equals(""))
	                	where.add(Expression.eq(divisionDocTable.attribute("CODE"), komorka));
	                
	                if(symbol != null && !symbol.equals(""))
	                	where.add(Expression.eq(rwaDocTable.attribute("code"), symbol));
	                
	                if(haslo != null && !haslo.equals(""))
	                	where.add(Expression.eq(rwaDocTable.attribute("description"), haslo));
	                
	                if (incomingDateFrom != null)
	                    where.add(Expression.ge(containerDocTable.attribute("CTIME"), DateUtils.midnight(incomingDateFrom, 0)));

	                if (incomingDateTo != null)
	                    where.add(Expression.le(containerDocTable.attribute("CTIME"), DateUtils.midnight(incomingDateTo, 1)));

	                SelectQuery selectQuery = new SelectQuery(selectId, from, where, null);
	                
					ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
					
					while (rs.next())
					{
						int i = -1;
						HSSFRow row = sheet.createRow(rowCount++);
						row.createCell(++i).setCellValue(new HSSFRichTextString(lp++ +""));

						String tytul = rs.getString(colTytul.getPosition());
						if(tytul!=null)
							row.createCell(++i).setCellValue(new HSSFRichTextString(tytul));
						else
							row.createCell(++i).setCellValue(new HSSFRichTextString("-"));

						String sender = rs.getString(colNadawca.getPosition());
						if(sender!=null){
							DSUser user = DSUser.findByUsername(sender);

							row.createCell(++i).setCellValue(new HSSFRichTextString(user.asFirstnameLastname()));
						}else
							row.createCell(++i).setCellValue(new HSSFRichTextString("-"));

						String znakSprawy = rs.getString(colZnakSprawy.getPosition());
						if(znakSprawy!=null)
							row.createCell(++i).setCellValue(new HSSFRichTextString(znakSprawy));
						else
							row.createCell(++i).setCellValue(new HSSFRichTextString("-"));

						Long case_id = rs.getLong(colCaseId.getPosition());
						PreparedStatement ps = null;
						Long docIn = null;
						Long docOut = null;
						if(case_id != null){
							ps = DSApi.context().prepareStatement("SELECT id FROM DSO_IN_DOCUMENT where CASE_ID=?");
							ps.setLong(1, case_id);
							ResultSet rs1 = ps.executeQuery();
							if(rs1.next()){
								docIn = rs1.getLong("id");
							}
							ps = DSApi.context().prepareStatement("SELECT id FROM DSO_OUT_DOCUMENT where CASE_ID=?");
							ps.setLong(1, case_id);
							rs1 = ps.executeQuery();
							if(rs1.next()){
								docOut = rs1.getLong("id");
							}
							InOfficeDocument docI = null;
							OfficeDocument docO = null;
							Calendar dateIn = Calendar.getInstance();
							Calendar dateOut = Calendar.getInstance();
							if(docIn != null){
								docI = InOfficeDocument.findInOfficeDocument(docIn);
								dateIn.setTime(docI.getIncomingDate()); 
							}
							if (docOut != null){
								docO = OutOfficeDocument.find(docOut);								
								dateOut.setTime(docO.getCtime());	
							}
							if(dateIn.before(dateOut) && docIn != null){
								row.createCell(++i).setCellValue(new HSSFRichTextString(DateUtils.formatCommonDate(docI.getIncomingDate())));
							}else if(dateIn.after(dateOut) && docOut != null){
								row.createCell(++i).setCellValue(new HSSFRichTextString(DateUtils.formatCommonDate(docO.getCtime())));
							}else {
								row.createCell(++i).setCellValue(new HSSFRichTextString("-"));

							}
							ps.close();
							rs1.close();
						}else{
							row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
						}                    
						Date dataWszczecia = rs.getDate(colOpenDate.getPosition());
						if(dataWszczecia!=null)
							row.createCell(++i).setCellValue(new HSSFRichTextString(DateUtils.formatCommonDate(dataWszczecia)));
						else
							row.createCell(++i).setCellValue(new HSSFRichTextString("-"));

						Date dataZakonczenia = rs.getDate(colFinishDate.getPosition());
						if(dataZakonczenia!=null)
							row.createCell(++i).setCellValue(new HSSFRichTextString(DateUtils.formatCommonDate(dataZakonczenia)));
						else
							row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
					}
	            } catch (HibernateException e) {
	            	Logger.getLogger(UTPDocReport.class.getName()).log(Level.SEVERE, null, e);
	            } catch (SQLException e) {
	            	Logger.getLogger(UTPDocReport.class.getName()).log(Level.SEVERE, null, e);
	            }

	        }
		 catch ( EdmException ex )
		 {
			 Logger.getLogger(UTPDocReport.class.getName()).log(Level.SEVERE, null, ex);
		 }


	}
	
		public void setKomorka(String komorka) {
			this.komorka = komorka;
		}
		
		public void setSymbol(String symbol){
			this.symbol=symbol;
		}
		
		public void setHaslo(String haslo){
			this.haslo=haslo;
		}
	
		public void setIncomingDateFrom(Date incomingDateFrom) {
			this.incomingDateFrom = incomingDateFrom;
		}

		public void setIncomingDateTo(Date incomingDateTo) {
			this.incomingDateTo = incomingDateTo;
		}

		
	}

