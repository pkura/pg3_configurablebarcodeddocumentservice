package pl.compan.docusafe.parametrization.utp;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

public abstract class AbstractXlsReports 
{
	private String reportName;
	
	protected HSSFWorkbook workbook;
	
	protected HSSFSheet sheet;
	
	protected HSSFRow headerRow;
	
	protected HSSFCellStyle headerStyle;
	
	protected String columns[];
	protected String title[];

	public AbstractXlsReports(String reportName) 
	{
		this.reportName = reportName;
		
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(reportName);
	}
	
	private void createHeader()
	{
		createHeaderStyle();
		
		headerRow = sheet.createRow(2);
		for (int i = 0; i < columns.length; i++)
		{
			headerRow.createCell(i)
				.setCellValue(new HSSFRichTextString(columns[i]));
			headerRow.getCell(i).setCellStyle(headerStyle);
		}
		headerRow = sheet.createRow(0);
		for (int i = 0; i < title.length; i++)
		{
			headerRow.createCell(i)
				.setCellValue(new HSSFRichTextString(title[i]));
			headerRow.getCell(i).setCellStyle(headerStyle);
		}
	}
	
	private void createHeaderStyle()
	{
		headerStyle = workbook.createCellStyle();
		headerStyle.setBorderBottom(HSSFCellStyle.BORDER_DOUBLE);
        headerStyle.setBottomBorderColor(HSSFColor.RED.index);
        
        headerStyle.setBorderLeft(HSSFCellStyle.BORDER_DOUBLE);
        headerStyle.setLeftBorderColor(HSSFColor.RED.index);
        
        headerStyle.setBorderRight(HSSFCellStyle.BORDER_DOUBLE);
        headerStyle.setRightBorderColor(HSSFColor.RED.index);
        
        headerStyle.setTopBorderColor(HSSFColor.RED.index);
        headerStyle.setBorderTop(HSSFCellStyle.BORDER_DOUBLE);
	}
	
	private void autosizeColumns()
	{
		for (int i = 0; i < columns.length; i++)
		{
			sheet.autoSizeColumn((short) i);
		}
	}
	
	abstract protected void createEntries();
	
	public void generate()
	{
		createHeader();
		createEntries();
		autosizeColumns();
	}

	public String getReportName() 
	{
		return reportName;
	}

	public void setReportName(String reportName) 
	{
		this.reportName = reportName;
	}

	public HSSFWorkbook getWorkbook() 
	{
		return workbook;
	}

	public void setWorkbook(HSSFWorkbook workbook) 
	{
		this.workbook = workbook;
	}
}
