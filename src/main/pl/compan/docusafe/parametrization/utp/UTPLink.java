package pl.compan.docusafe.parametrization.utp;

import java.net.URL;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;


@Entity
@Table(name = "UTP_LINK")
public class UTPLink {

	@Id
	@Column(name = "ID", nullable=false)
	private Long id;
	
	@Column(name = "TITLE", nullable=false)
	private String title;
	
	@Column(name = "LINK", nullable=false)
	private URL link;
	
	public Boolean Save() throws EdmException {

		try{
			DSApi.context().session().saveOrUpdate(this);
			return true;
		}catch(HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public URL getLink() {
		return link;
	}

	public void setLink(URL link) {
		this.link = link;
	}
}
