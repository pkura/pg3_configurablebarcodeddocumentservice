package pl.compan.docusafe.parametrization.utp;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import org.apache.commons.lang.StringUtils;



import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.NormalLogic;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.ins.jbpm.PrepareZamawianyTowarTable;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class WniosekZapDoDostLogic extends AbstractDocumentLogic {

    private static NormalLogic instance;
    public final static String BARCODE = "BARCODE";
    public static final String STATUS_DOCKIND_CN = "STATUS";
    public final static String SLOWNIK_POZYCJE_BUDZETOWE = "BUDZET_POZ";
    public final static String SLOWNIK_POZYCJE_FAKTURY = "ZAMOWIENIAPOZ";
    public final static String SLOWNIK_POZYCJE_FAKTURY_INDEKS = "IND";
    public final static String SLOWNIK_POZYCJE_FAKTURY_PRODUKT = "PRODUKT";
    public final static String SLOWNIK_POZYCJE_BUDZETOWE_KONTRAKT = "KONTRAKT";
    public final static String SLOWNIK_POZYCJE_BUDZETOWE_BUDZET = "BUDZET";
    public final static String SLOWNIK_POZYCJE_BUDZETOWE_FAZA = "FAZA";
    public final static String SLOWNIK_POZYCJE_BUDZETOWE_ZASOB = "ZASOB";
    public final static String TABELA_POZYCJE_BUDZETOWE = "UTP_BUDZETPOZ";
    public final static String WIDOK_POZYCJE_BUDZETOWE_KONTRAKT = "simple_erp_per_docusafe_projekty";
    public final static String WIDOK_POZYCJE_BUDZETOWE_BUDZET = "simple_erp_per_docusafe_budzety_projektow";
    public final static String WIDOK_POZYCJE_BUDZETOWE_FAZA = "simple_erp_per_docusafe_etapy_projektow";
    public final static String WIDOK_POZYCJE_BUDZETOWE_ZASOB = "simple_erp_per_docusafe_zasoby_projektow";
    public final static String WIDOK_POZYCJE_FAKTURY_PRODUKTY = "simple_erp_per_docusafe_wytwory_zapdost_view_wniosek";
    public final static String WIDOK_SIMPLE_KOMORKI_ZAPDOST = "simple_erp_per_docusafe_komorki_zapdost";
    public final static String WIDOK_SIMPLE_WALUTA = "simple_erp_per_docusafe_waluta_view";
    public final static String WIDOK_SIMPLE_TYP_ZAPOTRZEBOWANIA = "simple_erp_per_docusafe_typ_zapotrz_dost";
    public final static String ERP_STAWKI_VAT_TABLENAME = "simple_erp_per_docusafe_vatstaw_view";

    static final EnumValues EMPTY_ENUM_VALUE = new EnumValues(new HashMap<String, String>(), new ArrayList<String>());


    protected static Logger log = LoggerFactory.getLogger(WniosekZapDoDostLogic.class);

    public static NormalLogic getInstance() {
        if (instance == null)
            instance = new NormalLogic();
        return instance;
    }


    public void documentPermissions(Document document) throws EdmException {
        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
        perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

        for (PermissionBean perm : perms) {
            if (perm.isCreateGroup()
                    && perm.getSubjectType().equalsIgnoreCase(
                    ObjectPermission.GROUP)) {
                String groupName = perm.getGroupName();
                if (groupName == null) {
                    groupName = perm.getSubject();
                }
                createOrFind(document, groupName, perm.getSubject());
            }
            DSApi.context().grant(document, perm);
            DSApi.context().session().flush();
        }
    }


    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as)
            throws EdmException {
        // TODO Auto-generated method stub

    }

    public void setInitialValues(FieldsManager fm, int type) throws EdmException {

        log.info("type: {}", type);
        Map<String, Object> values = new HashMap<String, Object>();
        DSUser dsu = DSApi.context().getDSUser();

        values.put("DATADOKUMENTU", new Date(System.currentTimeMillis()));
        fm.getField("DATADOKUMENTU").setReadOnly(true);
        values.put("STATUS", 0);
        values.put("NETTO", 0);
        values.put("VAT", 0);
        values.put("BRUTTO", 0);
        values.put("KURS", 1);
        values.put("WALUTA", 1);
        values.put("KOSZTY_WYDZIAL",true);
        fm.getField("STATUS").setReadOnly(true);
        values.put("ZGLASZAJACY", dsu.getId());
        fm.getField("DATAAKCEPTACJI").setReadOnly(true);
        fm.getField("DATAZATWIERDZENIA").setReadOnly(true);
        fm.getField("DATAZGLOSZENIA").setReadOnly(true);
        fm.getField("DATAPRZYJECIAERP").setReadOnly(true);
        fm.getField("DATAREALIZACJI").setReadOnly(true);
        fm.getField("NETTO").setReadOnly(true);
        fm.getField("VAT").setReadOnly(true);
        fm.getField("BRUTTO").setReadOnly(true);
        //fm.getField("WALUTA").setReadOnly(true);
        fm.reloadValues(values);


    }

    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) {
        log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
        try {
            Map<String, Object> map = Maps.newHashMap();
            if (document instanceof InOfficeDocument) {
                map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
            } else {
                map.put(ASSIGN_USER_PARAM,
                        event.getAttribute(ASSIGN_USER_PARAM));
                map.put(ASSIGN_DIVISION_GUID_PARAM,
                        event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
            }


            document.getDocumentKind().getDockindInfo()
                    .getProcessesDeclarations().onStart(document, map);

            String barcode = document.getFieldsManager()
                    .getStringValue(BARCODE);

            if (document instanceof InOfficeDocument) {
                InOfficeDocument in = (InOfficeDocument) document;
                if (barcode != null) {
                    in.setBarcode(barcode);
                } else {
                    in.setBarcode("Brak");
                }
            } else if (document instanceof OutOfficeDocument) {
                OutOfficeDocument od = (OutOfficeDocument) document;
                if (barcode != null) {
                    od.setBarcode(barcode);
                } else {
                    od.setBarcode("Brak");
                }
            }

            DSApi.context().watch(URN.create(document));

            TaskSnapshot.updateByDocument(document);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }


    @Override
    public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(
            Map<String, FieldData> values, FieldsManager fm)
            throws EdmException {

        StringBuilder msgBuilder = new StringBuilder();

        if (values.containsKey("DWR_ZAMOWIENIAPOZ")) {
            FieldData zamFields = values.get("DWR_ZAMOWIENIAPOZ");
            FieldData kursField = values.get("DWR_KURS");
            BigDecimal sumaVat = new BigDecimal("0");
            BigDecimal suma = new BigDecimal("0");
            Map<String, FieldData> fields = zamFields.getDictionaryData();
            int liczbaWierszy = 0;
            boolean czyKurs = false;
            if (fields != null) {
                String suffix = "";
                for (String cn : fields.keySet()) {
                    if (cn.contains("ID_")) liczbaWierszy++;
                    if (fields.get(cn).getData() != null)
                        if (!fields.get(cn).getData().toString().equals("")) czyKurs = true;
                    suffix = cn.substring(cn.lastIndexOf("_"));
                    if (cn.contains("NETTO_BAZ" + suffix)) {
                        //fields.get(cn).setStringData(suffix);
                        if (fields.get(cn) != null) {
                            String tmp = cn.replace("NETTO_BAZ", "ILOSC");
                            String tmp2 = cn.replace("NETTO_BAZ", "CENA");
                            String cnVat = cn.replace("NETTO_BAZ", "VAT");

                            if (fields.get(tmp2).getMoneyData() != null && fields.get(tmp).getMoneyData() != null) {
                                BigDecimal kwNetto = fields.get(tmp2).getMoneyData().multiply(fields.get(tmp).getMoneyData());
                                suma = suma.add(kwNetto);
                                if (fields.get(cnVat).getData() != null) {
                                    BigDecimal stawkaVat = new BigDecimal(DataBaseEnumField.getEnumItemForTable(
                                            ERP_STAWKI_VAT_TABLENAME,
                                            Integer.valueOf(fields.get(cnVat).getEnumValuesData().getSelectedId())).getCn());

                                    sumaVat = sumaVat.add(kwNetto.multiply(
                                            stawkaVat.divide(new BigDecimal(100).setScale(2),RoundingMode.HALF_UP)
                                                    .setScale(2, RoundingMode.HALF_UP)).setScale(2, RoundingMode.HALF_UP));
                                }

                            }
                        }

                    }
//                    else if (cn.contains("DATAOCZEKIWANA" + suffix)) {
//                        if (!fields.get(cn).toString().equals("null")) {
//                            if (values.get("DWR_DATAZAPOTRZEBOWANIA").getData() == null) {
//                                values.put("DWR_DATAZAPOTRZEBOWANIA", fields.get(cn));
//                            } else {
//                                Date dataSlownik = fields.get(cn).getDateData();
//                                Date dataForm = values.get("DWR_DATAZAPOTRZEBOWANIA").getDateData();
//                                if (dataSlownik.before(dataForm)) {
//                                    values.put("DWR_DATAZAPOTRZEBOWANIA", fields.get(cn));
//                                }
//                            }
//                        }
//                    }
                }

                String result = setInternalDemandDate(values);
                if (result != null)
                    msgBuilder.append(result);

                //fm.getField("KURS").setReadOnly(czyKurs);
                //fm.getField("WALUTA").setReadOnly(czyKurs);
                suma = suma.multiply(values.get("DWR_KURS").getMoneyData()).setScale(2, RoundingMode.HALF_UP);
                FieldData sumaNettoField = values.get("DWR_NETTO");
                FieldData sumaVatField = values.get("DWR_VAT");
                FieldData sumaBruttoField = values.get("DWR_BRUTTO");
                sumaNettoField.setMoneyData(suma);
                sumaVatField.setMoneyData(sumaVat);
                sumaBruttoField.setMoneyData(suma.add(sumaVat));
                values.put("DWR_NETTO", sumaNettoField);
                values.put("DWR_VAT", sumaVatField);
                values.put("DWR_BRUTTO", sumaBruttoField);


            }
        }
        String idKierownika = null;
        String komorka = null;
        if (values.containsKey("DWR_KOMORKA") && values.containsKey("DWR_ODZIAL")) {
            if (values.get("DWR_KOMORKA") != null && values.get("DWR_ODZIAL") != null) {
                DataBaseEnumField d = DataBaseEnumField.getEnumFiledForTable(WIDOK_SIMPLE_KOMORKI_ZAPDOST);
                boolean jestCoNajmniej1KomorkaDlaDzialu = false;
                for (EnumItem ei : d.getAvailableItems()) {
                    if (ei.getRefValue().equals(values.get("DWR_ODZIAL").getEnumValuesData().getSelectedId()))
                        jestCoNajmniej1KomorkaDlaDzialu = true;
                }
                if (jestCoNajmniej1KomorkaDlaDzialu) {
                    fm.getField("KOMORKA").setRequired(true);
                } else
                    fm.getField("KOMORKA").setRequired(false);
            }

            /// WYB紑 KIEROWNIKA KOMORKI
            if(values.get("DWR_KOMORKA") != null && values.get("DWR_KOMORKA").getData() != null &&
                    !values.get("DWR_KOMORKA").getData().equals("")){
                boolean contextOpened = false;
                try {
                    contextOpened = DSApi.openContextIfNeeded();

                    String komId = (String) values.get("DWR_KOMORKA").getData();
                    EnumItem item = DataBaseEnumField.getEnumItemForTable(WIDOK_SIMPLE_KOMORKI_ZAPDOST,Integer.valueOf(komId));
                    komorka = item.getTitle().trim();
                    //TODO kierownik narazie ustawiony na sztywno na admina, bo inne komorki organizacyjne, nie ma ich w utp 
                  //  DSDivision div = DSDivision.findByCode(item.getCn().trim());
                  //  DSUser user= znajdzKierownikaPoStanowiskuDSDivision(div);
//                    String kierownikId = znajdzIdKierownikaKomorkiErp(komId);
//                    DSUser user = DSUser.findByExternalName(kierownikId);
                    //idKierownika = user.getId().toString();
                    idKierownika="1";
                    values.get("DWR_AKCEPTUJACY").getEnumValuesData().setSelectedId(idKierownika);
                } catch (Exception e){
                    log.error(e.getMessage(), e);
        }
                finally{
                    DSApi.closeContextIfNeeded(contextOpened);
                }
            }

//            if(values.get("DWR_KOMORKA") != null && values.get("DWR_KOMORKA").getData() != null &&
//                    !values.get("DWR_KOMORKA").getData().equals("")){
//                int komId = Integer.valueOf((String) values.get("DWR_KOMORKA").getData());
//                EnumItem item = DataBaseEnumField.getEnumItemForTable(WIDOK_SIMPLE_KOMORKI_ZAPDOST,
//                        komId);
//
//                Map<String, FieldData> budzetowePoz = values.get ("DWR_BUDZET_POZ").getDictionaryData();
//                FieldData budzetowePozField = values.get("DWR_BUDZET_POZ");
//
//                Map<String, FieldData> mapa = new HashMap<String, FieldData>();
//                mapa.put("BUDZET_POZ_ID_"+1, new FieldData(Type.LONG, 1026));
//
//                budzetowePozField.setDictionaryData(mapa);
//
//                System.out.print("Test");
//
//            }
        }
        //wyb鏎 zatwierdzaj帷ego merytorycznie i dyrektora zatwierdzaj帷ego
        if (values.containsKey("DWR_TYP")) {
            /// WYB紑 AKCEPTUJ。EGO MERYTORYCZNIE
            if(values.get("DWR_TYP") != null && values.get("DWR_TYP").getData() != null &&
                    !values.get("DWR_TYP").getData().equals("")){
                boolean contextOpened = false;
                try {
                    contextOpened = DSApi.openContextIfNeeded();

                    String typZapotrzebowaniaId = (String) values.get("DWR_TYP").getData();
                    EnumItem item = DataBaseEnumField.getEnumItemForTable(WIDOK_SIMPLE_TYP_ZAPOTRZEBOWANIA,
                            Integer.valueOf(typZapotrzebowaniaId));
                    //AKCEPTAJCA MERYTORYCZNA
                    String akceptujacy = znajdzAkceptujacegoMerytorycznie(item.getCn());
                    if (item.getCn().trim().equals("ZDT") && komorka.startsWith("TW")){
                        DSDivision divTW = DSDivision.findByCode("TW");
                        akceptujacy = znajdzKierownikaPoStanowiskuDSDivision(divTW).getName();
                    }
                    if (akceptujacy != null){
                        DSUser user = DSUser.findByUsername(akceptujacy);
                        values.get("DWR_ZATWIERDZAJACY").getEnumValuesData().setSelectedId(user.getId().toString());
                    }else{
                        if (idKierownika != null)
                            values.get("DWR_ZATWIERDZAJACY").getEnumValuesData().setSelectedId(idKierownika);
                    }

                    //AKCEPTACJA DYREKTORA
                    String dyrektor = znajdzAkceptujacegoDyrektora(item.getCn());
//                    DSUser user = DSUser.findByExternalName(dyrektor);
                    if (dyrektor != null){
                        DSUser user = DSUser.findByUsername(dyrektor);
                        String idDyrektora = user.getId().toString();
                        values.get("DWR_PRZYJMUJACY").getEnumValuesData().setSelectedId(idDyrektora);
                    }else
                        values.get("DWR_PRZYJMUJACY").getEnumValuesData().setSelectedId(null);

                    //REALIZUJE
                    String cnKomRealizujacej = znajdzJednostkeRealizujaca(item.getCn());
                    if (cnKomRealizujacej != null){
                        DSDivision div = DSDivision.findByCode(cnKomRealizujacej.trim());
                        DSUser kierownikRealizujacy = znajdzKierownikaPoStanowiskuDSDivision(div);
                        String idRealizatora = kierownikRealizujacy.getId().toString();
                        values.get("DWR_REALIZUJACY").getEnumValuesData().setSelectedId(idRealizatora);
                    }else
                        values.get("DWR_REALIZUJACY").getEnumValuesData().setSelectedId(null);
                } catch (Exception e){
                    log.error(e.getMessage(), e);
                }
                finally{
                    DSApi.closeContextIfNeeded(contextOpened);
                }
            }
        }

        if (msgBuilder.length()>0) {
            values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
            return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
        } else
            return null;
    }

    private String setInternalDemandDate(Map<String, FieldData> values) throws EdmException {
        if (values.containsKey("DWR_ZAMOWIENIAPOZ")){
            FieldData zamFields = values.get("DWR_ZAMOWIENIAPOZ");
            Map<String, FieldData> fields = zamFields.getDictionaryData();

            String zamowieniaPozCn = "ZAMOWIENIAPOZ";
            String dataDokumentuCn = "DATADOKUMENTU";

            Date dataDokumentu = values.get("DWR_"+dataDokumentuCn).getDateData();

            String dataZapotrzebowaniaCn= "DWR_DATAZAPOTRZEBOWANIA";
            String dataOczekiwanaCn = "DATAOCZEKIWANA";
            if (fields != null){
                //sprawdzenie ile jest wierszy w s這wniku
                int i = 1;
                while(i>0){
                    if (fields.containsKey(zamowieniaPozCn+"_ID_"+i))
                        i++;
                    else
                        break;
                }

                Date internalDemanDate = null;
                for (int k = 1; k < i; k++){
                    if (fields.containsKey(zamowieniaPozCn+"_"+dataOczekiwanaCn+"_"+k)){
                        FieldData dataOczekiwana = fields.get(zamowieniaPozCn+"_"+dataOczekiwanaCn+"_"+k);
                        if (dataOczekiwana.getDateData() != null){
                            //sprawdzenie czy dataOczekiwana nie jest wcze郾iejsza od daty wniosku
                            if (dataOczekiwana.getDateData().before(dataDokumentu))
                                return "Data oczekiwana jednej z pozycji wniosku jest wcze郾iejsza od daty wniosku!";

                            if (internalDemanDate == null)
                                internalDemanDate = dataOczekiwana.getDateData();
                            else{
                                if (internalDemanDate.after(dataOczekiwana.getDateData()))
                                    internalDemanDate = dataOczekiwana.getDateData();
                            }
                        }
                    }
                }
                values.put(dataZapotrzebowaniaCn, new FieldData(Field.Type.DATE, internalDemanDate));
            }
        }
        return null;
    }

    private String znajdzJednostkeRealizujaca(String typ) throws SQLException, EdmException {
        String cnJednostkiRealizujacej = null;
        ResultSet rs = DSApi.context().prepareStatement("SELECT akceptujacy FROM UTP_zapotrz_akceptacje " +
                " WHERE typZapotrzebowania = '"+typ.trim()+"' AND rodzaj = 'R'").executeQuery();
        while(rs.next()){
            cnJednostkiRealizujacej = rs.getString(1);
        }
        return cnJednostkiRealizujacej;
    }

    private DSUser znajdzKierownikaPoStanowiskuDSDivision(DSDivision div) throws EdmException {
        DSDivision[] stanowiska = div.getChildren(DSDivision.TYPE_POSITION);
        DSDivision stanKierownik=null;
        DSUser kierownik = null;

        for (DSDivision pos : stanowiska){
            if(pos.getName().equals("Kierownik"))
                stanKierownik = pos;
    }
        if (stanKierownik.getUsers().length>0)
            kierownik = stanKierownik.getUsers()[0];

        return kierownik;
    }

    private String znajdzAkceptujacegoDyrektora(String typ) throws SQLException, EdmException {
        String rodzajDyrektora = null;
        String dyrektor = null;

        ResultSet rs = DSApi.context().prepareStatement("SELECT akceptujacy FROM UTP_zapotrz_akceptacje " +
                " WHERE typZapotrzebowania = '"+typ.trim()+"' AND rodzaj = 'AD'").executeQuery();
        while(rs.next()){
            rodzajDyrektora = rs.getString(1);
        }

        if (rodzajDyrektora != null){
//            rs = DSApi.context().prepareStatement("SELECT id FROM "+ WIDOK_SIMPLE_KOMORKI_ZAPDOST +
//                    " WHERE RTRIM(cn) = '"+rodzajDyrektora.trim()+"'").executeQuery();
//            String idKomDyrektora = null;
//            while(rs.next()){
//                idKomDyrektora = rs.getString(1);
//                dyrektor = znajdzIdKierownikaKomorkiErp(idKomDyrektora);
//            }
            DSDivision div = DSDivision.findByCode(rodzajDyrektora);
            if(div.getUsers().length>0)
                dyrektor = div.getUsers()[0].getName();
        }

        return dyrektor;
    }

    private String znajdzIdKierownikaKomorkiErp(String komId) throws EdmException, SQLException {
        String kierownikId = null;

        ResultSet rs = DSApi.context().prepareStatement("SELECT kierownik FROM simple_erp_per_docusafe_kierownicy_komorek" +
                " WHERE komorka_id = "+komId).executeQuery();
        while(rs.next()){
            kierownikId = rs.getString(1);
        }


        return kierownikId;
    }

    private String znajdzAkceptujacegoMerytorycznie(String typZapotrzebowania) throws EdmException, SQLException {
        String akceptujacy = null;

        ResultSet rs = DSApi.context().prepareStatement("SELECT akceptujacy FROM UTP_zapotrz_akceptacje " +
                " WHERE typZapotrzebowania = '"+typZapotrzebowania.trim()+"' AND rodzaj = 'AM'").executeQuery();
        while(rs.next()){
            akceptujacy = rs.getString(1);
        }


        return akceptujacy;
    }


    @Override
    public Map<String, Object> setMultipledictionaryValue(
            String dictionaryName, Map<String, FieldData> values,
            Map<String, Object> fieldsValues, Long documentId) {

        Map<String, Object> results = new HashMap<String, Object>();
        if (dictionaryName.equals("ZAMOWIENIAPOZ")) {
            Object jm = values.get("ZAMOWIENIAPOZ_JM").getData();
            Object ilosc = values.get("ZAMOWIENIAPOZ_ILOSC").getData();
            BigDecimal iloscBig = null;
            BigDecimal kursBig = null;
            BigDecimal cenaBig = null;
            if (jm != null) {
                int precyzja = 0;
                try {
                    PreparedStatement ps;
//                    boolean co = DSApi.openContextIfNeeded();
                    ps = DSApi.context().prepareStatement("select precyzjajm from simple_erp_per_docusafe_jm_view where id=" + jm.toString());
                    ResultSet rs = ps.executeQuery();
                    rs.next();
                    precyzja = rs.getInt("precyzjajm");
//                    DSApi.closeContextIfNeeded(co);
                } catch (SQLException e) {
                    log.error(e.getMessage(), e);
                    e.printStackTrace();
                } catch (EdmException e) {
                    log.error(e.getMessage(), e);
                    e.printStackTrace();
                }

                if (ilosc == null)
                    return results;
                iloscBig = new BigDecimal(ilosc.toString()).setScale(precyzja, RoundingMode.HALF_UP);
                results.put("ZAMOWIENIAPOZ_ILOSC", iloscBig);
            }
            Object cena = values.get("ZAMOWIENIAPOZ_CENA").getData();
            if (cena != null) {
                cenaBig = new BigDecimal(cena.toString());

                Object kurs = fieldsValues.get("KURS");
                if (kurs != null) {
                    kursBig = new BigDecimal(kurs.toString());
                    results.put("ZAMOWIENIAPOZ_CENA_BAZ", kursBig.multiply(cenaBig));
                }
            }

            Object produkt = values.get("ZAMOWIENIAPOZ_PRODUKT").getData();
            if (produkt != null) {
                Object vat = values.get("ZAMOWIENIAPOZ_VAT").getData();
                if (vat == null) {
                    try {
                        PreparedStatement ps;
//                        boolean co = DSApi.openContextIfNeeded();
                        ps = DSApi.context().prepareStatement("select vat_id from simple_erp_per_docusafe_wytwory_zapdost_view_wniosek where id=" + values.get("ZAMOWIENIAPOZ_PRODUKT").getData().toString());
                        ResultSet rs = ps.executeQuery();
                        if (rs.next()) {
                            if (rs.getObject("vat_id") != null) {
                                Object ob = rs.getObject("vat_id");
                                //FieldData fd=values.get("ZAMOWIENIAPOZ_VAT");
                                for (DataBaseEnumField d : DataBaseEnumField.tableToField.get("simple_erp_per_docusafe_vatstaw_view")) {
                                    EnumValues val = null;
                                    try {
                                        val = d.getEnumItemsForDwr(fieldsValues);
                                    } catch (EntityNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                    results.put("ZAMOWIENIAPOZ_VAT", val);
                                    List<String> selectedPositionId = new ArrayList<String>();
                                    selectedPositionId.add(ob.toString());
                                    ((EnumValues) results.get("ZAMOWIENIAPOZ_VAT")).setSelectedOptions(selectedPositionId);
                                }


                                //fd.getEnumValuesData().setSelectedId(ob.toString());
                                //results.put("ZAMOWIENIAPOZ_VAT", fd);


                            }
                        }
//                        DSApi.closeContextIfNeeded(co);


                    } catch (SQLException e) {
                        log.error(e.getMessage(), e);
                        e.printStackTrace();
                    } catch (EdmException e) {
                        log.error(e.getMessage(), e);
                        e.printStackTrace();
                    }
                }
                Object jmBazowa = values.get("ZAMOWIENIAPOZ_JM").getData();
                if (jmBazowa == null) {
                    try {
                        PreparedStatement ps;
//                        boolean co = DSApi.openContextIfNeeded();
                        ps = DSApi.context().prepareStatement("select jmbazowa from simple_erp_per_docusafe_wytwory_zapdost_view_wniosek where id=" + values.get("ZAMOWIENIAPOZ_PRODUKT").getData().toString());
                        ResultSet rs = ps.executeQuery();
                        if (rs.next()) {
                            if (rs.getObject("jmbazowa") != null) {
                                Object ob = rs.getObject("jmbazowa");
                                //FieldData fd=values.get("ZAMOWIENIAPOZ_VAT");
                                for (DataBaseEnumField d : DataBaseEnumField.tableToField.get("simple_erp_per_docusafe_jm_view")) {
                                    EnumValues val = null;
                                    try {
                                        val = d.getEnumItemsForDwr(fieldsValues);
                                    } catch (EntityNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                    results.put("ZAMOWIENIAPOZ_JM", val);
                                    List<String> selectedPositionId = new ArrayList<String>();
                                    selectedPositionId.add(ob.toString());
                                    ((EnumValues) results.get("ZAMOWIENIAPOZ_JM")).setSelectedOptions(selectedPositionId);
                                }


                                //fd.getEnumValuesData().setSelectedId(ob.toString());
                                //results.put("ZAMOWIENIAPOZ_VAT", fd);


                            }
                        }
//                        DSApi.closeContextIfNeeded(co);


                    } catch (SQLException e) {
                        log.error(e.getMessage(), e);
                        e.printStackTrace();
                    } catch (EdmException e) {
                        log.error(e.getMessage(), e);
                        e.printStackTrace();
                    }
                }

            }

            if (cenaBig != null && kursBig != null && iloscBig != null) {
                results.put("ZAMOWIENIAPOZ_NETTO", iloscBig.multiply(cenaBig).setScale(2, RoundingMode.HALF_UP));
                results.put("ZAMOWIENIAPOZ_NETTO_BAZ", kursBig.multiply(iloscBig.multiply(cenaBig)).setScale(2, RoundingMode.HALF_UP));
            }
        }

        if (dictionaryName.equals("BUDZET_POZ")) {
            String dictName = SLOWNIK_POZYCJE_BUDZETOWE + "_";
            results = ustawPolaZalezne(dictName, values,
                    fieldsValues, documentId, results);
        }
        if (dictionaryName.equals("ZAMOWIENIAPOZ")) {
            String dictName = SLOWNIK_POZYCJE_FAKTURY + "_";
            results = ustawPolaZalezne(dictName, values,
                    fieldsValues, documentId, results);
        }

        return results;


    }


    private Map<String, Object> ustawPolaZalezne(String dictionaryName,
                                                 Map<String, FieldData> values, Map<String, Object> fieldsValues,
                                                 Long documentId, Map<String, Object> results) {

        if (dictionaryName.equals(SLOWNIK_POZYCJE_BUDZETOWE + "_")) {

            Object kontrakt = values.get(dictionaryName + SLOWNIK_POZYCJE_BUDZETOWE_KONTRAKT).getData();
            Object budzet = values.get(dictionaryName + SLOWNIK_POZYCJE_BUDZETOWE_BUDZET).getData();
            Object faza = values.get(dictionaryName + SLOWNIK_POZYCJE_BUDZETOWE_FAZA).getData();
            if (kontrakt != null) {
                DwrUtils.setRefValueEnumOptions(values, results, SLOWNIK_POZYCJE_BUDZETOWE_KONTRAKT, dictionaryName, SLOWNIK_POZYCJE_BUDZETOWE_BUDZET, WIDOK_POZYCJE_BUDZETOWE_BUDZET, null, EMPTY_ENUM_VALUE);
            }
            if (budzet != null) {
                DwrUtils.setRefValueEnumOptions(values, results, SLOWNIK_POZYCJE_BUDZETOWE_BUDZET, dictionaryName, SLOWNIK_POZYCJE_BUDZETOWE_FAZA, WIDOK_POZYCJE_BUDZETOWE_FAZA, null, EMPTY_ENUM_VALUE);
            }
            if (faza != null) {
                DwrUtils.setRefValueEnumOptions(values, results, SLOWNIK_POZYCJE_BUDZETOWE_FAZA, dictionaryName, SLOWNIK_POZYCJE_BUDZETOWE_ZASOB, WIDOK_POZYCJE_BUDZETOWE_ZASOB, null, EMPTY_ENUM_VALUE);
            }

        } else if (dictionaryName.equals(SLOWNIK_POZYCJE_FAKTURY + "_")) {
            Object index = values.get(dictionaryName + SLOWNIK_POZYCJE_FAKTURY_INDEKS).getData();
            if (index != null) {
                DataBaseEnumField base = null;
                for (DataBaseEnumField iterField : DataBaseEnumField.tableToField.get(WIDOK_POZYCJE_FAKTURY_PRODUKTY)) {
                    if (iterField.getCn().equals("PRODUKT_1")) {
                        base = iterField;
                        break;
                    }
                }
                if (base != null) {
                    if (base.getRefField() == null) base.setRefField(SLOWNIK_POZYCJE_FAKTURY_INDEKS + "_1");
                    DwrUtils.setRefValueEnumOptions(values, results, SLOWNIK_POZYCJE_FAKTURY_INDEKS, dictionaryName, SLOWNIK_POZYCJE_FAKTURY_PRODUKT, WIDOK_POZYCJE_FAKTURY_PRODUKTY, null, EMPTY_ENUM_VALUE);

                }
            }

        }

        return results;
    }

    @Override
    public void onLoadData(FieldsManager fm) throws EdmException {
        if (fm.getEnumItem("STATUS") != null) {
            Integer statusid = fm.getEnumItem("STATUS").getId();
            pl.compan.docusafe.core.dockinds.field.Field slownikPozycjeFaktury = fm.getField(SLOWNIK_POZYCJE_FAKTURY);
            pl.compan.docusafe.core.dockinds.field.Field slownikPozycjeBudzetowe = fm.getField(SLOWNIK_POZYCJE_BUDZETOWE);
            String[] buttons = {};
            switch (statusid) {
                case 0:
                    buttons = new String[]{"doAdd", "doRemove"};
                    break;
                case 1:
                    buttons = new String[]{};
                    break;
                case 2:
                    buttons = new String[]{};
                    break;
                case 3:
                    buttons = new String[]{};
                    break;
                case 4:
                    buttons = new String[]{};
                    break;
                case 5:
                    buttons = new String[]{};
                    break;
                case 6:
                    buttons = new String[]{"doAdd", "doRemove"};
                    break;
                case 7:
                    buttons = new String[]{"doAdd", "doRemove"};
                    break;
                default:
                    buttons = new String[]{};
                    break;
            }
            ((DictionaryField) slownikPozycjeFaktury).setDicButtons(Lists.newArrayList(buttons));
            ((DictionaryField) slownikPozycjeBudzetowe).setDicButtons(Lists.newArrayList(buttons));
            
            
        }
        super.onLoadData(fm);
    }

    @Override
    public void setAdditionalValues(FieldsManager fm, Integer valueOf, String activity) throws EdmException {
        Map<String,Object> mapa = fm.getFieldValues();

        if(fm.getEnumItem("PRACOWNIK")!=null)
        	mapa.put("PRACOWNIK2", fm.getEnumItem("PRACOWNIK").getTitle());
        
        if(fm.getEnumItem("KOMORKA")!=null)
        	mapa.put("KOMORKA2",fm.getEnumItem("KOMORKA").getTitle());
        
        if(fm.getValue("DATADOKUMENTU")!=null)
        	mapa.put("DATADOKUMENTU2", DateUtils.formatJsDate((Date) fm.getValue("DATADOKUMENTU")));
        
        if(fm.getValue("DATAZAPOTRZEBOWANIA")!=null)
        	mapa.put("DATAZAPOTRZEBOWANIA2", DateUtils.formatJsDate((Date) fm.getValue("DATAZAPOTRZEBOWANIA")));
        
        if(fm.getFloatValue("BRUTTO")!=null && fm.getEnumItem("WALUTA")!=null){
        	BigDecimal brutto = new BigDecimal((Double) fm.getValue("BRUTTO")).setScale(2, RoundingMode.HALF_UP);
        	String waluta = fm.getEnumItem("WALUTA").getTitle();

    		mapa.put("BRUTTO2", brutto+" "+waluta);
        }
        
        if(fm.getEnumItem("AKCEPTUJACY")!=null || fm.getEnumItem("ZATWIERDZAJACY")!=null){
        	String lista = "";
        	
        	if(fm.getEnumItem("AKCEPTUJACY")!=null)
        		lista += fm.getEnumItem("AKCEPTUJACY").getTitle()+". ";
        	
        	/*if(fm.getEnumItem("ZATWIERDZAJACY")!=null)
        		lista += fm.getEnumItem("ZATWIERDZAJACY").getTitle();*/

        	mapa.put("LISTAOSOBAKCEPTUJACYCH", lista);
        }

        loadZamowionyTowarDictionary(fm.getDocumentId(), mapa);

        fm.reloadValues(mapa);
    }
    
    private void loadZamowionyTowarDictionary(Long documentId, Map<String,Object> mapa){
    	boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();
			
			ResultSet result = DSApi.context().prepareStatement("SELECT ID FROM UTP_WnioZapDoDost_ZAMOWIONY_TOWAR WHERE DOCUMENT_ID="+documentId).executeQuery();
			List<Long> lista = new ArrayList<Long>();
			
			while(result.next()){
				lista.add(result.getLong("ID"));
			}
			
			mapa.put("ZAMOWIONYTOWAR", lista);
		} catch (Exception e){
			log.error(e.getMessage(), e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
    }
    @Override
    public void onSaveActions(DocumentKind kind, Long documentId,
                              Map<String, ?> fieldValues) throws SQLException, EdmException {


        //TODO zapewne mozna to lepiej zrobuic
        if (fieldValues.containsKey("TYP") && fieldValues.containsKey("WALUTA")) {
            PreparedStatement ps = DSApi.context().prepareStatement("select czywal from " + WIDOK_SIMPLE_TYP_ZAPOTRZEBOWANIA + " typ " +
                    " left join " + WIDOK_SIMPLE_WALUTA + " wal on typ.waluta_id=wal.id where typ.id=" + fieldValues.get("TYP"));
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                if (rs.getString("czywal").equals("1")) {
                    DataBaseEnumField d = DataBaseEnumField.getEnumFiledForTable(WIDOK_SIMPLE_WALUTA);
                    for (EnumItem ei : d.getAvailableItems()) {
                        if (ei.getTitle().equals("PLN")) {
                            if (fieldValues.get("WALUTA") == ei.getId()) {
                                throw new EdmException("W wypadku zapotrzebowania walutowego nie mo瞠 by� wybrane jako waluta PLN");
                            }
                        }
                    }

                }
            }
        }


        FieldsManager fm = OfficeDocument.find(documentId).getFieldsManager();
        List budzetowePozycje = (List) fm.getValue("BUDZET_POZ");
        if (budzetowePozycje!=null){
            for (Object o : budzetowePozycje){
                Map mapa = (Map) o;
                EnumValues kontrakt = (EnumValues) mapa.get("BUDZET_POZ_KONTRAKT");
                EnumValues budzetProjektu = (EnumValues) mapa.get("BUDZET_POZ_BUDZET");
                EnumValues etap = (EnumValues) mapa.get("BUDZET_POZ_FAZA");
                EnumValues zasob = (EnumValues) mapa.get("BUDZET_POZ_ZASOB");
                if((kontrakt==null || !kontrakt.getSelectedId().equals(""))){
                    if((kontrakt!=null && kontrakt.getSelectedId().equals("")) ||
                        (budzetProjektu!=null && budzetProjektu.getSelectedId().equals("")) ||
                        (etap!=null && etap.getSelectedId().equals("")) ||
                        (zasob!=null && zasob.getSelectedId().equals("")))
                        throw new EdmException("Brak wymagaych danych - uzupe軟ij pola Projekt, Bud瞠t, Etap, Zas鏏.");
                }
//                if (kontrakt!= null && kontrakt.getSelectedId()!=null && !kontrakt.getSelectedId().equals("")){
//                    String projektId = kontrakt.getSelectedId();
//
//                    try {
//                        PreparedStatement ps;
//                        ps = DSApi.context().prepareStatement("SELECT komorka_id " +
//                                " FROM simple_erp_per_docusafe_projekt_to_komorka_view " +
//                                " WHERE projekt_id="+projektId);
//                        ResultSet rs = null;
//                        rs = ps.executeQuery();
//                        if (rs.next()) {
//                            System.out.println("KomorkaId = "+rs.getString(1));
//                        }
//                    } catch (Exception e) {
//                        log.error(e.getMessage(),e);
//                    }
//                }
            }
        }
/*		if(values.containsKey("DWR_TYP")){
			try{
				DataBaseEnumField d=DataBaseEnumField.getEnumFiledForTable(WIDOK_SIMPLE_WALUTA);
				EnumValues typ=values.get("DWR_TYP").getEnumValuesData();
				//wstawienie sugerowanego gdy waluta jest nie wybrana jeszcze
				if(typ.getSelectedId()!=null&&(values.get("DWR_WALUTA").getEnumValuesData().getSelectedOptions().get(0).equals(""))){
					PreparedStatement ps=DSApi.context().prepareStatement("select waluta_id,czywal from "+WIDOK_SIMPLE_TYP_ZAPOTRZEBOWANIA+" typ "+
							" left join "+WIDOK_SIMPLE_WALUTA+" wal on typ.waluta_id=wal.id where typ.id="+typ.getSelectedId());
					ResultSet rs=ps.executeQuery();
					if(rs.next()){
						if(rs.getString("czywal").equals("1")){
							EnumValues ev=d.getEnumValuesForForcedPush(rs.getString("waluta_id"));
							List<Map<String,String>> opcje=ev.getAllOptions();
							List<Map<String,String>> opcjeBezPLN=new ArrayList<Map<String,String>>();
							for(int i=0;i<opcje.size();i++){
								Map<String,String>opcja=opcje.get(i);
								boolean dodaj=true;
								for(String key:opcja.keySet()){
									if(opcja.get(key).equals("PLN")){
										dodaj=false;
									}
								}
								if(dodaj)opcjeBezPLN.add(opcja);
							}
							ev.setAllOptions(opcjeBezPLN);
							values.put("DWR_WALUTA", new FieldData(Type.ENUM,ev));
						}

					}
				}
				//jest wybrana waluta, ale trzeba sprawdzic czy nie zmieniono typu, a waluta pozostala 
				if(typ.getSelectedId()!=null&&(!values.get("DWR_WALUTA").getEnumValuesData().getSelectedOptions().get(0).equals(""))){
					PreparedStatement ps=DSApi.context().prepareStatement("select waluta_id,czywal from "+WIDOK_SIMPLE_TYP_ZAPOTRZEBOWANIA+" typ "+
							" left join "+WIDOK_SIMPLE_WALUTA+" wal on typ.waluta_id=wal.id where typ.id="+typ.getSelectedId());
					ResultSet rs=ps.executeQuery();
					if(rs.next()){
						if(rs.getString("czywal").equals("1")){
							boolean bylPLN=false;

							if(rs.getString("czywal").equals("1")){
								EnumValues ev=d.getEnumValuesForForcedPush(rs.getString("waluta_id"));
								List<Map<String,String>> opcje=ev.getAllOptions();
								List<Map<String,String>> opcjeBezPLN=new ArrayList<Map<String,String>>();
								for(int i=0;i<opcje.size();i++){
									Map<String,String>opcja=opcje.get(i);
									boolean dodaj=true;
									for(String key:opcja.keySet()){
										if(opcja.get(key).equals("PLN")){
											if(values.get("DWR_WALUTA").getEnumValuesData().getSelectedOptions().get(0).equals(key))bylPLN=true;
											dodaj=false;
										}
									}
									if(dodaj)opcjeBezPLN.add(opcja);
								}


								ev.setAllOptions(opcjeBezPLN);
								
								if(bylPLN){

									ev=d.getEnumValuesForForcedPush(null);
									values.put("DWR_WALUTA", new FieldData(Type.ENUM,ev));	
								}
								else 
									ev=d.getEnumValuesForForcedPush(values.get("DWR_WALUTA").getEnumValuesData().getSelectedOptions().get(0));
								values.put("DWR_WALUTA", new FieldData(Type.ENUM,ev));
							}
							else{
								EnumValues ev=d.getEnumValuesForForcedPush(values.get("DWR_WALUTA").getEnumValuesData().getSelectedOptions().get(0));
								values.put("DWR_WALUTA", new FieldData(Type.ENUM,ev));
							}
						}
					}
				}


				} catch(EdmException e){
					log.debug("",e);
					e.printStackTrace();
				}
				catch (SQLException e) {
					log.debug("",e);
					e.printStackTrace();
				}


				//usuniecie PLN

			}*/
    }

    @Override
    public TaskListParams getTaskListParams(DocumentKind kind, long documentId, TaskSnapshot task) throws EdmException
    {
        TaskListParams params = new TaskListParams();
        FieldsManager fm = kind.getFieldsManager(documentId);

        params.setStatus((String) fm.getValue(STATUS_DOCKIND_CN));

        return params;
    }

}
