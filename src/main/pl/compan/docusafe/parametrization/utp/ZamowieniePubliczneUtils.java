package pl.compan.docusafe.parametrization.utp;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContextFactory;

import pl.compan.docusafe.core.dockinds.dictionary.DictionaryUtils;
import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Lists;

public class ZamowieniePubliczneUtils implements ZamowieniePubliczneConst {
	private static final Logger log = LoggerFactory.getLogger(ZamowieniePubliczneUtils.class);
	private ZamowieniePubliczneUtils() { }
	
	
	/**
	 * Pobiera kwot� brutto z s�ownika pozycje budzetowe zapisanego w sesji (dwrSession).
	 * Wyj�tki zrzucane do logow. 
	 * @param position
	 * @return - null w przypadku wyj�tku wewn�trz metody lub je�li dla przekazanego parametru "position"
	 * zostanie zwrocony wi�cej ni� jeden element.
	 */
	public static FieldData getBudgetPositionGrossFromDwrSession(String position) {
		try {
			String dictionaryNameWithPrefix = POZYCJE_BUDZETOWE_CN+"_";
			HttpSession session = WebContextFactory.get().getSession();
			Map<String, Object> dwrFieldValues = (Map<String, Object>)session.getAttribute(DwrFacade.DWR_SESSION_NAME);
			Map<String, FieldData> selectedFieldvalues = (Map<String, FieldData>)dwrFieldValues.get("SELECTED_FIELDS_VALUES");
			FieldData fieldData = selectedFieldvalues.get(DwrUtils.dwr(POZYCJE_BUDZETOWE_CN));
			List<String> rows = getFieldRowsNum(selectedFieldvalues, POZYCJE_BUDZETOWE_CN, POZYCJE_BUDZETOWE_POZYCJAID_CN, position);
			if(rows.isEmpty() || rows.size()>1)
				return null;
			Map<String,FieldData> row = extractRowFromDictionaryField(fieldData, rows.get(0));
			return row.get(dictionaryNameWithPrefix+POZYCJE_BUDZETOWE_BRUTTO_CN);
		} catch (Exception e) {
			log.error("", e);
		}
		return null;
	}

    /**
     * Pobiera kwot� netto z s�ownika pozycje budzetowe zapisanego w sesji (dwrSession).
     * Wyj�tki zrzucane do logow.
     * @param position
     * @return - null w przypadku wyj�tku wewn�trz metody lub je�li dla przekazanego parametru "position"
     * zostanie zwrocony wi�cej ni� jeden element.
     */
    public static BigDecimal pobierzKwoteNettoZSesji(String position) {
        try {
            String dictionaryNameWithPrefix = POZYCJE_BUDZETOWE_CN+"_";
            HttpSession session = WebContextFactory.get().getSession();
            Map<String, Object> dwrFieldValues = (Map<String, Object>)session.getAttribute(DwrFacade.DWR_SESSION_NAME);
            Map<String, FieldData> selectedFieldvalues = (Map<String, FieldData>)dwrFieldValues.get("SELECTED_FIELDS_VALUES");
            FieldData fieldData = selectedFieldvalues.get(DwrUtils.dwr(POZYCJE_BUDZETOWE_CN));
            List<String> rows = getFieldRowsNum(selectedFieldvalues, POZYCJE_BUDZETOWE_CN, POZYCJE_BUDZETOWE_POZYCJAID_CN, position);
            if(rows.isEmpty() || rows.size()>1)
                return null;
            Map<String,FieldData> row = extractRowFromDictionaryField(fieldData, rows.get(0));
            FieldData iloscFD = row.get(dictionaryNameWithPrefix + POZYCJE_BUDZETOWE_ILOSC_CN);
            FieldData cenaNettoFD = row.get(dictionaryNameWithPrefix + POZYCJE_BUDZETOWE_NETTO_CN);
            if(iloscFD!=null && iloscFD.getData()!=null && cenaNettoFD!=null && cenaNettoFD.getData()!=null){
                Integer ilosc = iloscFD.getIntegerData();
                BigDecimal cenaNetto = cenaNettoFD.getMoneyData();
                return cenaNetto.multiply(new BigDecimal(ilosc));
            }
        } catch (Exception e) {
            log.error("", e);
        }
        return null;
    }
	
	/**
	 * Pobiera z s�ownika wszystkie pola dla kt�rych w fieldCN suffix r�wny parametrowi cnSuffix
	 * @param dictionaryFieldData - pl.compan.docusafe.core.dockinds.dwr.FieldData
	 * @param cnSuffix - szukany suffix
	 * @return
	 */
	public static Map<String, FieldData> extractRowFromDictionaryField(FieldData dictionaryFieldData, String cnSuffix){
		Map<String, FieldData> result = new HashMap<String, FieldData>();
		Map<String, FieldData> dictionaryValues = dictionaryFieldData.getDictionaryData();
		for (String key : dictionaryValues.keySet()) {
			if(getSuffix(key, false).equalsIgnoreCase(cnSuffix)){
				result.put(DictionaryUtils.getFieldCnWithoutSuffix(key), dictionaryValues.get(key));
			}
		}
		return result;
	}
	
	
	public static String getSuffix(String multiDictionaryFieldCn, boolean underscore) {
		try{
			String[] splited = multiDictionaryFieldCn.split("_");
			return (underscore?"_":"")+splited[splited.length-1];
		} catch (Exception e) {
			log.error("", e);
		}
		return "";
	}
	
	/**
	 * Zwraca list� suffix�w wierszy dla kt�rych w polu fieldCN jest warto�� searchValue.
	 * @param dwrValues
	 * @param dictName
	 * @param fieldCn
	 * @param searchValue
	 * @return
	 */
	public static List<String> getFieldRowsNum(Map<String, FieldData> dwrValues, String dictName, String fieldCn, String searchValue) {
		FieldData fieldData = dwrValues.get(DwrUtils.dwr(dictName));
		List<String> result = Lists.newArrayList();
		Map<String, FieldData> pozycje = DwrUtils.extractFieldsFromDictionaryField(fieldData, fieldCn);
		for (String key : pozycje.keySet()) {
			if(pozycje.get(key) != null && pozycje.get(key).getData() != null){
				if(pozycje.get(key).getStringData().equalsIgnoreCase(searchValue)){
					String[] KeySplited = key.split("_");
					result.add(KeySplited[KeySplited.length-1]);
				}
			}
		}
		return result;
	}
}
