package pl.compan.docusafe.parametrization.utp.ws;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 26.09.13
 * Time: 09:36
 * To change this template use File | Settings | File Templates.
 */
public class SprawaBean {
    public String znakSprawy;
    public String uzytkTworzacy;
    public String referent;
    public Date przewTerminZak;
    public int priorytet;
    public String tytul;
    public String opis;

    public String getZnakSprawy() {
        return znakSprawy;
    }

    public void setZnakSprawy(String znakSprawy) {
        this.znakSprawy = znakSprawy;
    }

    public String getUzytkTworzacy() {
        return uzytkTworzacy;
    }

    public void setUzytkTworzacy(String uzytkTworzacy) {
        this.uzytkTworzacy = uzytkTworzacy;
    }

    public String getReferent() {
        return referent;
    }

    public void setReferent(String referent) {
        this.referent = referent;
    }

    public Date getPrzewTerminZak() {
        return przewTerminZak;
    }

    public void setPrzewTerminZak(Date przewTerminZak) {
        this.przewTerminZak = przewTerminZak;
    }

    public Integer getPriorytet() {
        return priorytet;
    }

    public void setPriorytet(int priorytet) {
        this.priorytet = priorytet;
    }

    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }
}
