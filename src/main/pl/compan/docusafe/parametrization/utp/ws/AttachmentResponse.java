package pl.compan.docusafe.parametrization.utp.ws;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 03.10.13
 * Time: 13:18
 * To change this template use File | Settings | File Templates.
 */
public class AttachmentResponse {
    private String fileName;
    private Boolean error;
    private String message;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
