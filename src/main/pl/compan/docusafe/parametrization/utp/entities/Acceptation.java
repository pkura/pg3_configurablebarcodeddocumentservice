package pl.compan.docusafe.parametrization.utp.entities;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;

import com.google.common.collect.Lists;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "DS_UTP_AKCEPTACJE")
public class Acceptation  {
    private static final Logger log = LoggerFactory.getLogger(Acceptation.class);


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "OSOBA", nullable = true)
    private Long osoba;

    @Column(name = "DATA", nullable = true)
    private Date data;

    @Column(name = "STATUS", nullable = true)
    private Integer status;

    @Column(name = "UWAGI", nullable = true)
    private String uwagi;

    public Acceptation() {
    }

    public Acceptation(Long osoba, Date data, Integer status, String uwagi) {
        this.osoba = osoba;
        this.data = data;
        this.status = status;
        this.uwagi = uwagi;
    }

    public static synchronized Acceptation findFirstAcceptation(String acceptationCn, String multiTable, Long docId) throws EdmException
    {
        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().prepareStatement("SELECT a.ID " +
                    "FROM DS_UTP_AKCEPTACJE a " +
                    "INNER JOIN "+multiTable+" b " +
                    " ON a.ID = b.FIELD_VAL " +
                    "WHERE b.FIELD_CN = ? AND " +
                    "b.DOCUMENT_ID = ? AND " +
                    "a.DATA IS NULL AND a.STATUS IS NULL");
            ps.setString(1, acceptationCn);
            ps.setLong(2,docId);

            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                return  (Acceptation) DSApi.context().session().get(Acceptation.class,rs.getLong(1));
            }
        }catch(Exception e){
            log.error("", e);
        }finally {
            if (ps != null)
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new EdmException(e);
                }
        }
        return null;
    }
    
    public static synchronized Acceptation findAcceptation(Long acceptationId) throws EdmException
    {
        try {
        	return (Acceptation) DSApi.context().session().get(Acceptation.class, acceptationId);
        } catch(Exception e) { 
        	log.error("", e);
        }
        return null;
    }

    public static synchronized int countUnacceptedAcceptation(String acceptationCn,
                                                              String multiTable,
                                                              String budgetPositionTable,
                                                              Long docId) throws EdmException
    {
        PreparedStatement ps = null;
        try
        {
            if (budgetPositionTable==null){
                ps = DSApi.context().prepareStatement("SELECT count(*) " +
                    "FROM DS_UTP_AKCEPTACJE a " +
                    "INNER JOIN "+multiTable+" b " +
                    " ON a.ID = b.FIELD_VAL " +
                    "WHERE b.FIELD_CN = ? AND " +
                    "b.DOCUMENT_ID = ? AND " +
                    "a.DATA IS NULL AND a.STATUS IS NULL");
            }else{
                ps = DSApi.context().prepareStatement("SELECT count(*) " +
                    " FROM "+multiTable+" as a " +
                    " INNER JOIN " + budgetPositionTable +" as b ON a.FIELD_VAL=b.ID " +
                    " where FIELD_CN like ? " +
                    " and document_id = ? " +
                    " and b.DATA_AKCEPTACJI IS NULL " +
                    " AND b.STATUS_AKCEPTACJI IS NULL");
            }

            ps.setString(1, acceptationCn);
            ps.setLong(2,docId);

            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                return  rs.getInt(1);
            }
        }catch(Exception e){
            log.error("", e);
        }finally {
            if (ps != null)
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new EdmException(e);
                }
        }
        return -1;
    }

    public static synchronized Acceptation findFirstAcceptationForDSUser(String acceptationCn, String multiTable, Long docId, DSUser user) throws EdmException
    {
        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().prepareStatement("SELECT a.ID " +
                    "FROM DS_UTP_AKCEPTACJE a " +
                    "INNER JOIN "+multiTable+" b " +
                    " ON a.ID = b.FIELD_VAL " +
                    "WHERE b.FIELD_CN = ? AND " +
                    "b.DOCUMENT_ID = ? AND " +
                    "a.DATA IS NULL AND a.STATUS IS NULL" +
                    " AND a.OSOBA = ? ");
            ps.setString(1, acceptationCn);
            ps.setLong(2,docId);
            ps.setLong(3,user.getId());

            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                return  (Acceptation) DSApi.context().session().get(Acceptation.class,rs.getLong(1));
            }
        }catch(Exception e){
            log.error("", e);
        }finally {
            if (ps != null)
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new EdmException(e);
                }
        }
        return null;
    }

    public static synchronized List<Acceptation> findAcceptationForDocument(String acceptationCn, String multiTable, Long docId) throws EdmException
    {
        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().prepareStatement("SELECT a.ID " +
                    "FROM DS_UTP_AKCEPTACJE a " +
                    "INNER JOIN "+multiTable+" b " +
                    " ON a.ID = b.FIELD_VAL " +
                    "WHERE b.FIELD_CN = ? AND " +
                    "b.DOCUMENT_ID = ? AND " +
                    "a.DATA IS NULL AND a.STATUS IS NULL");
            ps.setString(1, acceptationCn);
            ps.setLong(2,docId);
//            ps.setLong(3,user.getId());

            ResultSet rs = ps.executeQuery();
            List<Acceptation> listRet = Lists.newArrayList();
            while (rs.next()){
                listRet.add((Acceptation) DSApi.context().session().get(Acceptation.class,rs.getLong(1)));
            }
            return listRet;
        }catch(Exception e){
            log.error("", e);
        }finally {
            if (ps != null)
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new EdmException(e);
                }
        }
        return null;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOsoba() {
        return osoba;
    }

    public void setOsoba(Long osoba) {
        this.osoba = osoba;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUwagi() {
        return uwagi;
    }

    public void setUwagi(String uwagi) {
        this.uwagi = uwagi;
    }
}
