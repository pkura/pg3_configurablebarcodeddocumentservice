package pl.compan.docusafe.parametrization.utp;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.GroupClause;
import pl.compan.docusafe.util.querybuilder.select.OrderClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

public class UTPDivisionTaskXlsReport extends AbstractXlsReports {
	private String komorka;
	private String zalogowany;
	private boolean zalatwioneSprawy;

	public UTPDivisionTaskXlsReport(String reportName,boolean zalatwioneSprawy) {
		super(reportName);
		if(zalatwioneSprawy){
			title = new String[] { "Raport zada� zako�czonych w kom�rce org." };

			columns = new String[] { "Lp.", "Kod kom�rki org.", "Nazwa kom. org.",
					"Ilosc zako�czonych spraw w kom.org." };
		}
		else{
			
		
		title = new String[] { "Raport aktualnych zada�  w kom�ce org." };

		columns = new String[] { "Lp.", "Kod kom�rki org.", "Nazwa kom. org.",
				"Ilosc aktualnych spraw w kom.org." };
		}
	}


	@Override
	protected void createEntries() {

		try {

			StringBuilder sqlStatement = new StringBuilder(
					"select d.code,d.name,SUM(iloscZad) as iloscSpraw from ds_division d "
					+ "left join( select c.officeIdPrefix,count(c.officeidprefix)as iloscZad from dso_container c where c.DISCRIMINATOR = 'case'  ");

			if(zalatwioneSprawy)
				sqlStatement.append(" AND c.CLOSED = '1'");
			else sqlStatement.append(" AND c.CLOSED = '0'");
			
			sqlStatement
					.append(" group by c.OFFICEIDPREFIX) as con on con.OFFICEIDPREFIX=d.CODE ");
			if (komorka != null && !komorka.equals(""))
				sqlStatement.append(" where d.id='" + komorka + "' ");
			
			sqlStatement.append(" group by d.code,d.name order by iloscSpraw desc");
			String sqlStringStatement = sqlStatement.toString();
			PreparedStatement ps = DSApi.context().prepareStatement(
					sqlStringStatement);
			ResultSet rs = ps.executeQuery();

			// liczba niezakonczonych spraw wraz z symbolem i nazwa komorki do
			// xlsa
			int rowCount = 3;
			int lp = 1;
			while (rs.next()) {
				int i = -1;
				HSSFRow row = sheet.createRow(rowCount++);
				row.createCell(++i).setCellValue(
						new HSSFRichTextString(lp++ + ""));
				String code = rs.getString("code");
				String name = rs.getString("name");
				String count = rs.getString("iloscSpraw");

				if (code != null)
					row.createCell(++i).setCellValue(
							new HSSFRichTextString(code));
				else
					row.createCell(++i).setCellValue(
							new HSSFRichTextString("-"));

				if (name != null)
					row.createCell(++i).setCellValue(
							new HSSFRichTextString(name));
				else
					row.createCell(++i).setCellValue(
							new HSSFRichTextString("-"));

				if (count != null)
					row.createCell(++i).setCellValue(
							new HSSFRichTextString(count));
				else
					row.createCell(++i).setCellValue(
							new HSSFRichTextString("0"));

			}

			lp = 1;
			HSSFRow row = sheet.createRow(rowCount++);
			row = sheet.createRow(rowCount++);
			row.createCell(0).setCellValue(new HSSFRichTextString("Lp."));
			row.createCell(1).setCellValue(new HSSFRichTextString("Kod"));
			row.createCell(2).setCellValue(new HSSFRichTextString("Nazwa kom�rki"));
			
			if(!zalatwioneSprawy){
				row.createCell(3).setCellValue(
						new HSSFRichTextString("Ilosc przydzielonych zada�"));
			// ilosc zadan przyznanych uzytkownikom w danej komorce
			sqlStatement = new StringBuilder(
					"select d.code,d.name,SUM(iloscZad) as iloscZadan from DS_DIVISION d  "
					+ "left join(select utd.DIVISION_ID,t.assigned_resource,count(t.assigned_resource) as iloscZad from DSW_JBPM_TASKLIST t "
					+ "left join ds_user u on t.assigned_resource=u.name "
					+ "left join DS_USER_TO_DIVISION utd on u.id=utd.USER_ID "
					+ "group by utd.division_id,t.assigned_resource) as iloscZadan on iloscZadan.DIVISION_ID=d.ID ");


			//dodajWarunkiNaUzytwkownikowPodleglych(sqlStatement, zalogowany);
			// usuniecie ostatenigo 'Or '

			//sqlStatement.delete(sqlStatement.length() - 3,
			//		sqlStatement.length());
			//sqlStatement.append(") ");
			if (komorka != null && !komorka.equals(""))
				sqlStatement.append(" Where  d.id='" + komorka + "' ");

			sqlStatement.append(" group by d.code,d.name"
					+ " order by iloscZadan desc");

			sqlStringStatement = sqlStatement.toString();
			ps = DSApi.context().prepareStatement(sqlStringStatement);
			rs = ps.executeQuery();
			}
			else{
				row.createCell(3).setCellValue(
						new HSSFRichTextString("Ilosc zako�czonych zada�"));
				sqlStatement = new StringBuilder(
						"select div.CODE,div.name,SUM(iloscZadan) as iloscZadan from DS_DIVISION div "
						+ "left join(select con.ID,con.officeidprefix,count(dsoin.case_id)+count(dsout.case_id) as iloscZadan from ds_document d "
						+ "left join DSO_IN_DOCUMENT dsoin on dsoin.ID=d.ID "
						+ "left join DSO_out_DOCUMENT dsout on dsout.ID=d.ID "
						+ "left join DSO_CONTAINER con on (con.id=dsoin.CASE_ID or con.id=dsout.CASE_ID) "
						+ "where con.id is not null AND D.id not in (select document_id from dsw_jbpm_tasklist) "
						+ "group by con.ID,con.OFFICEIDprefix) as iloscZadanWSprawach on iloscZadanWSprawach.OFFICEIDPREFIX=div.code "
						);


				//dodajWarunkiNaUzytwkownikowPodleglych(sqlStatement, zalogowany);
				// usuniecie ostatenigo 'Or '

				//sqlStatement.delete(sqlStatement.length() - 3,
				//		sqlStatement.length());
				//sqlStatement.append(") ");
				if (komorka != null && !komorka.equals(""))
					sqlStatement.append(" Where  div.id='" + komorka + "' ");

				sqlStatement.append(" group by div.CODE,div.name"
						+ " order by iloscZadan desc");

				sqlStringStatement = sqlStatement.toString();
				ps = DSApi.context().prepareStatement(sqlStringStatement);
				rs = ps.executeQuery();
			}

			while (rs.next()) {
				int i = -1;
				row = sheet.createRow(rowCount++);
				row.createCell(++i).setCellValue(
						new HSSFRichTextString(lp++ + ""));
				String firstname = rs.getString("code");
				String lastname = rs.getString("name");
				String count = rs.getString("iloscZadan");

				if (firstname != null)
					row.createCell(++i).setCellValue(
							new HSSFRichTextString(firstname));
				else
					row.createCell(++i).setCellValue(
							new HSSFRichTextString("-"));

				if (lastname != null)
					row.createCell(++i).setCellValue(
							new HSSFRichTextString(lastname));
				else
					row.createCell(++i).setCellValue(
							new HSSFRichTextString("-"));

				if (count != null)
					row.createCell(++i).setCellValue(
							new HSSFRichTextString(count));
				else
					row.createCell(++i).setCellValue(
							new HSSFRichTextString("0"));

			}

		} catch (HibernateException e) {
			Logger.getLogger(UTPDivisionTaskXlsReport.class.getName()).log(
					Level.SEVERE, null, e);
		} catch (SQLException e) {
			Logger.getLogger(UTPDivisionTaskXlsReport.class.getName()).log(
					Level.SEVERE, null, e);
		} catch (EdmException ex) {
			Logger.getLogger(UTPDivisionTaskXlsReport.class.getName()).log(
					Level.SEVERE, null, ex);
		}

	}

	private void dodajWarunkiNaUzytwkownikowPodleglych(
			StringBuilder sqlStatement, String userId) {
		Map<String, String> podlegli = new HashMap<String, String>();
		zwrocPracownikowPodleglych(podlegli, userId);

		for (String name : podlegli.values()) {
			name = name.split(":")[0];
			sqlStatement.append(" u.name='" + name + "' OR ");

		}

	}

	Map<String, String> zwrocPracownikowPodleglych(
			Map<String, String> uzytkownicyPodlegli, String zalogowany) {

		try {
			// dodanie aktualnego pracownika
			FromClause from = new FromClause();
			TableAlias userTable = from.createTable("ds_user");

			SelectClause select = new SelectClause(true);

			SelectColumn colName = select.add(userTable, "name");
			SelectColumn colFirstName = select.add(userTable, "firstname");
			SelectColumn colLastName = select.add(userTable, "lastname");

			WhereClause where = new WhereClause();
			where.add(Expression.eq(userTable.attribute("id"), zalogowany));

			SelectQuery selectQuery = new SelectQuery(select, from, where, null);
			ResultSet rs;

			rs = selectQuery.resultSet(DSApi.context().session().connection());

			rs.next();
			// nie wstawi jesli juz jest w mapie, ale bedzie sdzukal takze jego
			// podleglych osob
			if (!uzytkownicyPodlegli.containsKey(zalogowany))
				uzytkownicyPodlegli.put(
						zalogowany,
						rs.getString(colName.getPosition()) + ": "
								+ rs.getString(colLastName.getPosition()) + " "
								+ rs.getString(colFirstName.getPosition()));

			// wybranie podleglych id

			from = new FromClause();
			select = new SelectClause(true);
			where = new WhereClause();
			TableAlias US = from.createTable("DS_USER_SUPERVISOR");
			SelectColumn colId = select.add(US, "user_id");
			where.add(Expression.eq(US.attribute("SUPERVISOR_ID"), zalogowany));
			selectQuery = new SelectQuery(select, from, where, null);
			rs = selectQuery.resultSet(DSApi.context().session().connection());
			// i wykonanie dla nich rowniez tej metody rowniez z zastrzezeniem,
			// ze ich nie ma jeszcze na mapie
			while (rs.next()) {
				if (!uzytkownicyPodlegli.containsKey(rs.getString(colId
						.getPosition())))
					uzytkownicyPodlegli.putAll(zwrocPracownikowPodleglych(
							uzytkownicyPodlegli,
							rs.getString(colId.getPosition())));

			}

		} catch (HibernateException e) {

			e.printStackTrace();
		} catch (SQLException e) {

			e.printStackTrace();
		} catch (EdmException e) {

			e.printStackTrace();
		}

		return uzytkownicyPodlegli;
	}
	
	public void setKomorka(String komorka) {
		this.komorka = komorka;
	}

	public void setName(String name) {
		FromClause from = new FromClause();
		TableAlias userTable = from.createTable("ds_user");

		SelectClause select = new SelectClause(true);

		SelectColumn colName = select.add(userTable, "name");
		SelectColumn colId = select.add(userTable, "id");

		WhereClause where = new WhereClause();
		where.add(Expression.eq(userTable.attribute("name"), name));

		SelectQuery selectQuery = new SelectQuery(select, from, where, null);
		ResultSet rs;

		try {
			rs = selectQuery.resultSet(DSApi.context().session().connection());

			rs.next();
			this.zalogowany = rs.getString(colId.getPosition());
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void setZalatwioneSprawy(boolean b) {
		this.zalatwioneSprawy=b;
		
	}




}
