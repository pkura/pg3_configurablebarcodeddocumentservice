package pl.compan.docusafe.parametrization.utp;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;

public class UTPUserAssignmentXlsReport extends AbstractXlsReports {
	private String szukany;
	private Map<String, String> uzytkownicy;
	private Date dekOd;
	private Date dekDo;

	private StringManager sm = GlobalPreferences.loadPropertiesFile(UTPUserAssignmentXlsReport.class.getPackage().getName(), null);
	
	public UTPUserAssignmentXlsReport(String reportName) {
		super(reportName);
		title = new String[] { "Raport dekretacji" };

		columns = new String[] { "Lp.", sm.getString("ImieZrodlaDekretacji"), sm.getString("NazwiskoZrodlaDekretacji"),"Imi� celu dek.","Nazwisko celu dek.", "ID dokumentu",
				"Tytu�/Opis", "Data dekretacji", };
	}

	@Override
	protected void createEntries() {
		try {
			int rowCount = 3;
			int lp = 1;

			StringBuilder sqlStatement = new StringBuilder(
					"select u1.firstname as firstnameS,u1.lastname as lastnameS,u2.firstname as firstnameT ,u2.lastname as lastnameT,d.id,d.title,d.description,dah.ctime "
					+ "from dso_document_asgn_history dah "
					+ "left join dS_document d on d.id=dah.document_id "
					+ "left join ds_user u1 on u1.name=dah.sourceuser "
					+ "left join ds_user u2 on u2.name=dah.targetuser ");
			sqlStatement.append("WHERE d.czy_aktualny=1 AND objective  not like'start-process' and objective not like 'finish-task' AND ");
			// dodanie klauzuli where

			if (szukany != null && !szukany.equals("")) {
				sqlStatement.append(" (dah.targetuser='" + szukany + "' OR  dah.sourceuser='"+szukany+"')  AND");
			} else {
				sqlStatement.append(" ( ");
				for (String name : uzytkownicy.keySet()) {
					sqlStatement.append(" (dah.targetuser='" + name + "' OR  dah.sourceuser='"+name+"')  OR");
				}
				
				sqlStatement.delete(sqlStatement.length()-2, sqlStatement.length());
				sqlStatement.append(") AND");

			}

			if (dekOd != null) {
				sqlStatement.append(" dah.ctime>'"
						+ DateUtils.formatSqlDateTime(DateUtils.midnight(dekOd,0)) + "' AND");
			}
			if (dekDo != null) {
				sqlStatement.append(" dah.ctime<'"
						+ DateUtils.formatSqlDateTime(DateUtils.midnight(dekDo,1)) + "' AND");
			}
			sqlStatement.delete(sqlStatement.length()-3, sqlStatement.length());
			sqlStatement.append(" AND d.czy_aktualny=1");
			//wykonanie zapytania
			String sqlStringStatement = sqlStatement.toString();
			PreparedStatement ps = DSApi.context().prepareStatement(
					sqlStringStatement);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				//String source = rs.getString("sourceuser");
				//String target = rs.getString("targetuser");
				
				
				
				
				String firstNameS=rs.getString("firstnameS");
				String lastNameS=rs.getString("lastnameS");
				String firstNameT=rs.getString("firstnameT");
				String lastNameT=rs.getString("lastnameT");
				String id = rs.getString("id");
				String title = rs.getString("title");
				String ctime = rs.getString("ctime");

				int i = -1;
				HSSFRow row = sheet.createRow(rowCount++);
				row.createCell(++i).setCellValue(
						new HSSFRichTextString(lp++ + ""));
				
				if (firstNameS != null)
					row.createCell(++i).setCellValue(
							new HSSFRichTextString(firstNameS));
				else
					row.createCell(++i).setCellValue(
							new HSSFRichTextString("-"));

				if (lastNameS != null)
					row.createCell(++i).setCellValue(
							new HSSFRichTextString(lastNameS));
				else
					row.createCell(++i).setCellValue(
							new HSSFRichTextString("-"));
				if (firstNameT != null)
					row.createCell(++i).setCellValue(
							new HSSFRichTextString(firstNameT));
				else
					row.createCell(++i).setCellValue(
							new HSSFRichTextString("-"));

				if (lastNameT != null)
					row.createCell(++i).setCellValue(
							new HSSFRichTextString(lastNameT));
				else
					row.createCell(++i).setCellValue(
							new HSSFRichTextString("-"));

				if (id != null)
					row.createCell(++i)
					.setCellValue(new HSSFRichTextString(id));
				else
					row.createCell(++i).setCellValue(
							new HSSFRichTextString("-"));

				if (title != null)
					row.createCell(++i).setCellValue(
							new HSSFRichTextString(title));
				else
					row.createCell(++i).setCellValue(
							new HSSFRichTextString("-"));

				if (ctime != null)
					row.createCell(++i).setCellValue(
							new HSSFRichTextString(ctime));
				else
					row.createCell(++i).setCellValue(
							new HSSFRichTextString("-"));

			}

		} catch (HibernateException e) {
			Logger.getLogger(UTPDocumentUserAssignXlsReport.class.getName())
			.log(Level.SEVERE, null, e);
		} catch (SQLException e) {
			Logger.getLogger(UTPDocumentUserAssignXlsReport.class.getName())
			.log(Level.SEVERE, null, e);

		} catch (EdmException ex) {
			Logger.getLogger(UTPDocumentUserAssignXlsReport.class.getName())
			.log(Level.SEVERE, null, ex);
		}

	}

	public void setSzukany(String szukany) {
		this.szukany = szukany;

	}

	public void setUzytkownicy(Map<String, String> uzytkownicy) {
		this.uzytkownicy = uzytkownicy;

	}

	public void setDekOd(Date dekOd) {
		this.dekOd = dekOd;

	}

	public void setDekDo(Date dekDo) {
		this.dekDo = dekDo;

	}

}
