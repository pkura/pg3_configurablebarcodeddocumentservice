package pl.compan.docusafe.parametrization.utp;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.hibernate.HibernateException;

import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

public class UTPDocumentUserAssignXlsReport extends AbstractXlsReports {
	private Map<String, String> uzytkownicy = new TreeMap<String, String>();
	private String idUser;

	public void setName(String name) {
		this.idUser = name;

	}

	public UTPDocumentUserAssignXlsReport(String reportName) {
		super(reportName);
		title = new String[] { "Raport pism przydzielonych u�ytkownikowi" };

		columns = new String[] { "Lp.", "Imi�", "Nazwisko", "ID dokumentu",
				"Tytu�", "Numer KO","Odbiorca", "Nadawca", "Data utworzenia", };

	}

	@Override
	protected void createEntries() {
		try {


			StringBuilder sqlStatement = new StringBuilder("select distinct u.FIRSTNAME,u.LASTNAME,d.id,d.TITLE,i.OFFICENUMBER,p1.lastname as odbiorcaLN,p1.firstname as odbiorcaFN,p1.ORGANIZATION as odbiorcaORG, "
					+ "p2.lastname as nadawcaLN,p2.firstname as nadawcaFN,p2.ORGANIZATION as nadawcaORG,d.CTIME  from DSW_JBPM_TASKLIST t "
					+ "left join DS_DOCUMENT d on d.id=t.document_id  "
					+ "left join DSO_IN_DOCUMENT i on i.id=d.ID  "
					+ "left join DSO_OUT_DOCUMENT o on o.id=d.ID  "
					+ "left join ds_user u on t.assigned_resource=u.NAME "
					+ "left join dso_person p2 on o.sender=p2.ID "
					+ "left join dso_person p1 on i.sender=p1.ID");

			// where
			
			if (idUser != null && !idUser.equals(""))
				sqlStatement.append(" where u.id='"+idUser+"'");
			else {
				sqlStatement.append(" where ");
				for (String name : uzytkownicy.keySet()) {
					sqlStatement.append(" u.id='"+name+"' OR ");
				}
				sqlStatement.delete(sqlStatement.length()-3, sqlStatement.length());
				
			}
			sqlStatement.append(" and d.czy_aktualny=1");
			try {
				int rowCount = 3;
				int lp = 1;
				// wykosnianie zapytania

				String sqlStringStatement = sqlStatement.toString();
				PreparedStatement ps = DSApi.context().prepareStatement(
						sqlStringStatement);
				ResultSet rs = ps.executeQuery();
				// zapisywanie zapytania do xls
				while (rs.next()) {

					int i = -1;
					HSSFRow row = sheet.createRow(rowCount++);
					row.createCell(++i).setCellValue(
							new HSSFRichTextString(lp++ + ""));
					String docFirstName = rs.getString("firstname");
					String docLastName = rs
							.getString("lastname");
					String docID = rs.getString("id");
					String docTitle = rs.getString("title");
					String docKO = rs.getString("officenumber");
					String docCTime = rs.getString("ctime");
					String odbiorcaName=null;
					String nadawcaName=null;
					if(rs.getString("odbiorcaLN")!=null&&rs.getString("odbiorcaFN")!=null){
						odbiorcaName= rs.getString("odbiorcaLN")+" "+rs.getString("odbiorcaFN");
					}
					if(rs.getString("odbiorcaORG")!=null){
						odbiorcaName=odbiorcaName+" : "+rs.getString("odbiorcaORG");
					}
					if(rs.getString("nadawcaLN")!=null&&rs.getString("nadawcaFN")!=null){
						nadawcaName = rs.getString("nadawcaLN")+" "+rs.getString("nadawcaFN");
					}
					if(rs.getString("nadawcaORG")!=null){
						nadawcaName=nadawcaName+" : "+rs.getString("nadawcaORG");
					}

					if (docFirstName != null)
						row.createCell(++i).setCellValue(
								new HSSFRichTextString(docFirstName));
					else
						row.createCell(++i).setCellValue(
								new HSSFRichTextString("-"));

					if (docLastName != null)
						row.createCell(++i).setCellValue(
								new HSSFRichTextString(docLastName));
					else
						row.createCell(++i).setCellValue(
								new HSSFRichTextString("-"));

					if (docID != null)
						row.createCell(++i).setCellValue(
								new HSSFRichTextString(docID));
					else
						row.createCell(++i).setCellValue(
								new HSSFRichTextString("-"));

					if (docTitle != null)
						row.createCell(++i).setCellValue(
								new HSSFRichTextString(docTitle));
					else
						row.createCell(++i).setCellValue(
								new HSSFRichTextString("-"));

					if (docKO != null)
						row.createCell(++i).setCellValue(
								new HSSFRichTextString(docKO));
					else
						row.createCell(++i).setCellValue(
								new HSSFRichTextString("-"));

					if (odbiorcaName != null)
						row.createCell(++i).setCellValue(
								new HSSFRichTextString(odbiorcaName));
					else
						row.createCell(++i).setCellValue(
								new HSSFRichTextString("-"));
					if (nadawcaName != null)
						row.createCell(++i).setCellValue(
								new HSSFRichTextString(nadawcaName));
					else
						row.createCell(++i).setCellValue(
								new HSSFRichTextString("-"));

					if (docCTime != null)
						row.createCell(++i).setCellValue(
								new HSSFRichTextString(docCTime));
					else
						row.createCell(++i).setCellValue(
								new HSSFRichTextString("-"));

				}
			} catch (HibernateException e) {
				Logger.getLogger(UTPDocumentUserAssignXlsReport.class.getName())
						.log(Level.SEVERE, null, e);
			} catch (SQLException e) {
				Logger.getLogger(UTPDocumentUserAssignXlsReport.class.getName())
						.log(Level.SEVERE, null, e);
			}

		} catch (EdmException ex) {
			Logger.getLogger(UTPDocumentUserAssignXlsReport.class.getName())
					.log(Level.SEVERE, null, ex);
		}

	}
	
	public void setUzytkownicy(Map<String, String> uzytkownicy) {
		this.uzytkownicy = uzytkownicy;

	}

}
