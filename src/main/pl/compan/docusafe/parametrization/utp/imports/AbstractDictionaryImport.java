package pl.compan.docusafe.parametrization.utp.imports;

import org.apache.axis2.AxisFault;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;

import java.rmi.RemoteException;
import java.sql.SQLException;

public abstract class AbstractDictionaryImport {

    public abstract void initConfiguration(AxisClientConfigurator conf)  throws Exception;
    abstract void doImport() throws RemoteException, EdmException;
    abstract void cleanupTransport() throws AxisFault;

    public String getMessage() {
        return null;
    }

    public void materializeView() throws EdmException, SQLException {
        return;
    }

    public void initImport() {
        return;
    }

    //import z potwierdzeniem zakończenia
    //true - zakończony
    public boolean doImportWithConfirm() throws RemoteException, EdmException {
        doImport();
        return true;
    }

    public void finalizeImport() throws RemoteException, EdmException {
        return;
    }
}
