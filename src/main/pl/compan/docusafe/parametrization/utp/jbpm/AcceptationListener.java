package pl.compan.docusafe.parametrization.utp.jbpm;

import org.jbpm.api.listener.EventListenerExecution;
import org.jbpm.pvm.internal.model.ExecutionImpl;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.utp.ZamowieniePubliczneLogic;

import java.util.Date;


public class AcceptationListener extends AbstractEventListener {
    protected int acceptationStatus;
    protected String dictionaryCn;
    protected int updateExistingAcceptation;


    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        long documentId = (Long) execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY);
        String currentAssignee = (String) execution.getVariable("currentAssignee");
        OfficeDocument doc = OfficeDocument.find(documentId);

        String stateName = ((ExecutionImpl) execution).getTransition().getSource().getName();

        String uwagi = doc.getDocumentKind().getFieldByCn("STATUS").getEnumItemByCn(stateName).getTitle();

        DSUser user = DSUser.findByUsername(currentAssignee);
        ZamowieniePubliczneLogic.fillAcceptationTable(doc, dictionaryCn, user,new Date(),acceptationStatus,uwagi,
                updateExistingAcceptation==1 ? true:false);
    }

    public int getAcceptationStatus() {
        return acceptationStatus;
    }

    public void setAcceptationStatus(int acceptationStatus) {
        this.acceptationStatus = acceptationStatus;
    }

    public String getDictionaryCn() {
        return dictionaryCn;
    }

    public void setDictionaryCn(String dictionaryCn) {
        this.dictionaryCn = dictionaryCn;
    }

    public int isUpdateExistingAcceptation() {
        return updateExistingAcceptation;
    }

    public void setUpdateExistingAcceptation(int updateExistingAcceptation) {
        this.updateExistingAcceptation = updateExistingAcceptation;
    }
}
