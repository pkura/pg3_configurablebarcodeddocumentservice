package pl.compan.docusafe.parametrization.utp.jbpm;

import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.events.handlers.RefreshTaskListHandler;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class RefreshTaskListListener extends AbstractEventListener {
    protected final static Logger LOG = LoggerFactory.getLogger(RefreshTaskListListener.class);

    private static final long serialVersionUID = 1L;

	@Override
	public void notify(EventListenerExecution execution) throws Exception {
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        boolean contextOpened = false;
        try {
            contextOpened = DSApi.openContextIfNeeded();
            RefreshTaskListHandler.createEvent(docId);
        } catch(Exception e){
            LOG.error("B��d podczas od�wie�ania listy zada� dla dokumentu o ID: "+docId+": "+e);
        } finally {
            DSApi.closeContextIfNeeded(contextOpened);
        }
	}

}
