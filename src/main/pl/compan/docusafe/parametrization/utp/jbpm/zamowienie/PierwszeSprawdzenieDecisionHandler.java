package pl.compan.docusafe.parametrization.utp.jbpm.zamowienie;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.parametrization.utp.ZamowieniePubliczneLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;

/**
 *  Sprawdzenie czy kwota w przedziale 6000EUR do 14000EUR  <br/>  
 * TAK: do bezpo�redniego prze�o�onego <br/>
 * NIE: do dzia�u Remont�w i Inwestycji (je�eli us�uga remontowa)<br/>
 *      lub do Kanclerza (je�eli nie jest us�uga remontowa)*/
public class PierwszeSprawdzenieDecisionHandler implements DecisionHandler{
    private static final Logger log = LoggerFactory.getLogger(PierwszeSprawdzenieDecisionHandler.class);

    private static final String DRUGIE_SPRAWDZENIE_CN = "drugieSprawdzenie";
    private static final String OPINIA_DZIALU_REMONTOW_I_INWESTYCJI_CN = "opiniaDzialuRemontowIInwestycji";
    private static final String OPINIA_DZIALU_TELEINFORMATYKI_CN = "opiniaDzialuTeleinformatyki";
    private static final String ZATWIERDZENIE_KANCLERZA_CN = "zatwierdzenieKanclerza";

    @Override
    public String decide(OpenExecution openExecution) {
        Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        try {
            boolean czyRobotaBudowlana = ZamowieniePubliczneLogic.checkIfRepairServices(docId);
            boolean czyRobotaBudownictwaTelekom = ZamowieniePubliczneLogic.checkIfDepartmentOfICT(docId);
            boolean czyKwotaWPrzedzialeKanclerzPrzetarg = ZamowieniePubliczneLogic.checkIfAmountIsInRange(
            		docId, new BigDecimal(ZamowieniePubliczneLogic.pobierzProgKanclerza()),
            		new BigDecimal(ZamowieniePubliczneLogic.pobierzProgPrzetargu()));
            	
            if (czyRobotaBudowlana) {
            	if(czyRobotaBudownictwaTelekom){
    				return OPINIA_DZIALU_TELEINFORMATYKI_CN;
            	}
				return OPINIA_DZIALU_REMONTOW_I_INWESTYCJI_CN;
			} else if (czyKwotaWPrzedzialeKanclerzPrzetarg) {
				return ZATWIERDZENIE_KANCLERZA_CN;
			} else {
				return DRUGIE_SPRAWDZENIE_CN;
			}
            /*
            
            if(!czyKwotaWPrzedziale)
            	return DRUGIE_SPRAWDZENIE_CN;
            if (czyUslugaRemontowa) {
                if (czyRobotaBudownictwaTelekom)
                    return OPINIA_DZIALU_TELEINFORMATYKI_CN;
                else
                    return OPINIA_DZIALU_REMONTOW_I_INWESTYCJI_CN;
            } else
                return ZATWIERDZENIE_KANCLERZA_CN;*/
        } catch (Exception e) {
            log.error("B��d podczas pierwszego sprawdzenia przy wniosku zam�wienia publicznego: "+e);
        }
        return DRUGIE_SPRAWDZENIE_CN;
    }


}
