package pl.compan.docusafe.parametrization.utp.jbpm;

import java.util.ArrayList;
import java.util.List;

import org.jbpm.api.listener.EventListenerExecution;

import com.sun.codemodel.JDeclaration;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.field.DSDivisionEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.utp.ZamowieniePubliczneConst;
import pl.compan.docusafe.parametrization.utp.ZamowieniePubliczneLogic;

public class PelnomocnikNaWydziale extends AbstractEventListener implements ZamowieniePubliczneConst{
		
// dzp	"A8FDF18801CDCFE51438B5000E9B2DC598A"
// daf	"D479D58F09D476311420387066A924ABF25"
	@Override
	public void notify(EventListenerExecution execution) throws Exception {
		Long docId = Jbpm4Utils.getDocumentId(execution);
		
		DSDivision grPelnomocnikDZP = DSDivision.find(ZamowieniePubliczneLogic.pobierzGuidPelnomocnikDZP());
		DSDivision dzialOsobyRejestrujacej = DSDivision.find(Jbpm4Utils.getDocument(execution).getFieldsManager().getLongKey(OS_REJESTRUJACA_DZIAL_CN).intValue());
		
		List<String> pelnomocnicyNaWydziale = new ArrayList<String>();
		
		do{
			for (DSUser pelnomocnikDZP : grPelnomocnikDZP.getUsers()) {
				if(dzialOsobyRejestrujacej.isInAnyChildDivision(pelnomocnikDZP))
					pelnomocnicyNaWydziale.add(pelnomocnikDZP.getName());
			}
			
			dzialOsobyRejestrujacej = dzialOsobyRejestrujacej.getParent();
			if(dzialOsobyRejestrujacej == null || dzialOsobyRejestrujacej.getGuid().equalsIgnoreCase(ZamowieniePubliczneLogic.pobierzGuidDzialGlowny())){
				break;
			}
		} while(pelnomocnicyNaWydziale.isEmpty());
		
		System.out.println("Pelnomocnik na Wydziale listener."+pelnomocnicyNaWydziale);
		/*
		dzialOsobyRejestrujacej.
		
		Jbpm4Utils.getDocument(execution).getFieldsManager().getLongKey(OS_REJESTRUJACA_DZIAL_CN);
		DSDivision.find("A8FDF18801CDCFE51438B5000E9B2DC598A").getUsers()[0]
		
		
		DSDivision.find(Jbpm4Utils.getDocument(execution).getFieldsManager().getLongKey(OS_REJESTRUJACA_DZIAL_CN).intValue()).
		DSDivision.find(Jbpm4Utils.getDocument(execution).getFieldsManager().getLongKey(OS_REJESTRUJACA_DZIAL_CN).intValue()).getUserSources(DSDivision.find("A8FDF18801CDCFE51438B5000E9B2DC598A").getUsers()[1])
		
		OfficeDocument officeDocument = Jbpm4Utils.getDocument(execution);
		FieldsManager fm = officeDocument.getFieldsManager();
	 	pl.compan.docusafe.core.dockinds.field.Field jednostkaZamawiajacego = fm.getField(ZamowieniePubliczneLogic.JEDNOSTKA_ZAMAWIAJACEGO_CN);
	 	
	 	fm.getEnumItem(ZamowieniePubliczneLogic.OSOBA_SKLADAJACA_ZAM_CN);
	 	EnumItem enumItemJednostkaZamawiajacego = fm.getEnumItem(ZamowieniePubliczneConst.OSOBA_SKLADAJACA_ZAM_CN);
	 	System.out.println(enumItemJednostkaZamawiajacego.getCn());
	 	System.out.println(enumItemJednostkaZamawiajacego.getId());
	 	
		
		System.out.println(docId);
		
		*/
		
//		throw new  EdmException("Hola hola");
	}

}
