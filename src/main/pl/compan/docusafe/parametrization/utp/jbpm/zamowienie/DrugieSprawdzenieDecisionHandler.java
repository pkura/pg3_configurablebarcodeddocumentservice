package pl.compan.docusafe.parametrization.utp.jbpm.zamowienie;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.parametrization.utp.ZamowieniePubliczneLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;


public class DrugieSprawdzenieDecisionHandler implements DecisionHandler{
    private static final Logger log = LoggerFactory.getLogger(PierwszeSprawdzenieDecisionHandler.class);

    private static final String AKCEPTACJA_SPECJALISTY_FINANSOWEGO_CN = "akceptacjaSpecjalistyFinansowego";
    private static final String WYDANIE_ZGODY_NA_ZAKUP_CN = "wydanieZgodyNaZakup";
    private static final String AKCEPTACJA_KIEROWNIKA_DZIALU_ZAMOWIEN_PUBLICZNYCH_CN = "akceptacjaKierownikaDZP";
    @Override
    public String decide(OpenExecution openExecution) {
        Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        try {
        	
            boolean czKwotaPonizejPrzetargu = ZamowieniePubliczneLogic.checkIfAmountIsLessOrEqual(docId,
            		new BigDecimal(ZamowieniePubliczneLogic.pobierzProgPrzetargu()));
			boolean czyArt6a = ZamowieniePubliczneLogic.checkIfPurchaseFrom6A(docId);
			boolean czyZakupyCentralne = ZamowieniePubliczneLogic.checkIfCentralPurchasing(docId);
            boolean czyZagrozeniePrzekroczeniaPrzetargu = ZamowieniePubliczneLogic.checkIfRiskOfExceeding(docId);
            
			if (!czKwotaPonizejPrzetargu) {
				return akcjaDlaArt6a(czyArt6a);
            } else if (czyZakupyCentralne) {
            	return akcjaDlaArt6a(czyArt6a);
			} else if (czyZagrozeniePrzekroczeniaPrzetargu) {
				return WYDANIE_ZGODY_NA_ZAKUP_CN;
			} else {
				return akcjaDlaArt6a(czyArt6a);
			}
            
        } catch (Exception e) {
            log.error("B��d podczas drugiego sprawdzenia przy wniosku zam�wienia publicznego: "+e);
        }
        return AKCEPTACJA_SPECJALISTY_FINANSOWEGO_CN;
    }
	private String akcjaDlaArt6a(boolean czyArt6a) {
		if (czyArt6a) {
			return AKCEPTACJA_KIEROWNIKA_DZIALU_ZAMOWIEN_PUBLICZNYCH_CN;
		} else {
			return AKCEPTACJA_SPECJALISTY_FINANSOWEGO_CN;
		}
	}
}
