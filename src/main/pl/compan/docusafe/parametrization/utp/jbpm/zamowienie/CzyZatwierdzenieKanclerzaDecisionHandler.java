package pl.compan.docusafe.parametrization.utp.jbpm.zamowienie;

import java.math.BigDecimal;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.parametrization.utp.ZamowieniePubliczneLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class CzyZatwierdzenieKanclerzaDecisionHandler implements DecisionHandler {
	
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(CzyZatwierdzenieKanclerzaDecisionHandler.class);
	
	@Override
	public String decide(OpenExecution openExecution) {
		Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		try {
			boolean czyKwotaWPrzedzialeKanclerzPrzetarg = ZamowieniePubliczneLogic.checkIfAmountIsInRange(
					docId, new BigDecimal(ZamowieniePubliczneLogic.pobierzProgKanclerza()),
					new BigDecimal(ZamowieniePubliczneLogic.pobierzProgPrzetargu()));
			
			if (czyKwotaWPrzedzialeKanclerzPrzetarg) {
				return "true";
			}
			return "false";
			
		} catch (EdmException e) {
			log.error("B��d decyzji.", e);
			return "fasle";
		}
	}

}
