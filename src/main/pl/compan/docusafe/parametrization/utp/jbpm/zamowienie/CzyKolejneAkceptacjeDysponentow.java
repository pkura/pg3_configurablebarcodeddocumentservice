package pl.compan.docusafe.parametrization.utp.jbpm.zamowienie;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.utp.ZamowieniePubliczneLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


public class CzyKolejneAkceptacjeDysponentow implements DecisionHandler {
    private static final Logger log = LoggerFactory.getLogger(CzyKolejneAkceptacjeDysponentow.class);

    private static final String NEXT_DYSPONENT = "nastepnyDysponent";
    private static final String IT_WAS_LAST_DYSPONENT = "nestepneAkceptacje";

    @Override
    public String decide(OpenExecution openExecution) {
        boolean isOpened = true;
        try {
            if (!DSApi.isContextOpen()) {
                isOpened = false;
                DSApi.openAdmin();
            }
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");

            OfficeDocument doc = OfficeDocument.find(docId);
            if(ZamowieniePubliczneLogic.czyIstniejeNastepnaAkceptacjaDysponenta(doc))
                return NEXT_DYSPONENT;
            else
                return IT_WAS_LAST_DYSPONENT;
        } catch (EdmException e) {
            log.error("B��d podczas wyszukiwania dokumentu: "+e);
        } finally {
            if (DSApi.isContextOpen() && !isOpened)
                DSApi._close();
        }
        return NEXT_DYSPONENT;
    }
}