package pl.compan.docusafe.parametrization.utp.jbpm;

import org.jbpm.api.listener.EventListenerExecution;
import org.jbpm.pvm.internal.model.ExecutionImpl;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.utp.ZamowieniePubliczneLogic;

import java.util.Date;

public class AcceptationDysponentListener extends AcceptationListener {

    protected String multiDictCn;
    protected String statusCn;
    protected String dateCn;

    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        long documentId = (Long) execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY);
        String currentAssignee = (String) execution.getVariable("currentAssignee");
        OfficeDocument doc = OfficeDocument.find(documentId);

        DSUser user = DSUser.findByUsername(currentAssignee);
        ZamowieniePubliczneLogic.fillAcceptationStatusForBudgetPositions(doc,
                user, new Date(), dictionaryCn, multiDictCn, statusCn, dateCn, acceptationStatus);
    }

    public String getMultiDictCn() {
        return multiDictCn;
    }

    public void setMultiDictCn(String multiDictCn) {
        this.multiDictCn = multiDictCn;
    }

    public String getStatusCn() {
        return statusCn;
    }

    public void setStatusCn(String statusCn) {
        this.statusCn = statusCn;
    }

    public String getDateCn() {
        return dateCn;
    }

    public void setDateCn(String dateCn) {
        this.dateCn = dateCn;
    }

}