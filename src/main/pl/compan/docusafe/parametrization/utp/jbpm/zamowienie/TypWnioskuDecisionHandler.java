package pl.compan.docusafe.parametrization.utp.jbpm.zamowienie;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.parametrization.utp.ZamowieniePubliczneConst;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class TypWnioskuDecisionHandler implements DecisionHandler {
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(TypWnioskuDecisionHandler.class);
	
	private static final String NOTATKA = "realizacjaZamowieniaSamodzielnaNotatka";
	private static final String ZAPROSZENIE = "realizacjaZamowieniaSamodzielnaZaproszenie";
	private static final String POSTEPOWANIE = "realizacjaZamowieniaSamodzielnaPostepowanie";
    
	
	@Override
	public String decide(OpenExecution openExecution) {
        boolean isOpened = true;
        try {
            if (!DSApi.isContextOpen()) {
                isOpened = false;
                DSApi.openAdmin();
            }
			Document document = Jbpm4Utils.getDocument(openExecution);
			FieldsManager fm = document.getFieldsManager();
			String selectedEnumCN = FieldsManagerUtils.getEnumItem(fm, ZamowieniePubliczneConst.RODZAJ_WNIOSKU_CN).getCn();
			if(selectedEnumCN.equalsIgnoreCase(ZamowieniePubliczneConst.RODZAJ_WNIOSKU_NOTATKA_CN))
				return NOTATKA;
			else if(selectedEnumCN.equalsIgnoreCase(ZamowieniePubliczneConst.RODZAJ_WNIOSKU_ZAPROSZENIE_CN))
				return ZAPROSZENIE;
			else if(selectedEnumCN.equalsIgnoreCase(ZamowieniePubliczneConst.RODZAJ_WNIOSKU_WSZCZECIE_POSTEPOWANIA_CN))
				return POSTEPOWANIE;
		} catch (Exception e) {
			log.error("", e);
		} finally {
            if (DSApi.isContextOpen() && !isOpened)
                DSApi._close();
        }
		return NOTATKA;
	}

}
