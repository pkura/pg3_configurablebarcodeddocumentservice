package pl.compan.docusafe.parametrization.utp.jbpm.zamowienie;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.parametrization.utp.ZamowieniePubliczneLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;

public class TrzecieSprawdzenieDecisionHandler implements DecisionHandler{
	
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(PierwszeSprawdzenieDecisionHandler.class);

    private static final String WSZCZECIE_POSTEPOWANIA_PRZETARGOWEGO_CN = "wszczeciePostepowaniaPrzetargowego";
    private static final String AKCEPTACJA_KIEROWNIKA_DZP_CN = "akceptacjaKierownikaDzialuZamowienPublicznych";
    private static final String REALIZACJA_ZAMOWIENIA_SAMODZIELNA_CN = "realizacjaZamowieniaSamodzielna";
    @Override
    public String decide(OpenExecution openExecution) {
        Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");

        boolean isOpened = true;
        try {
            if (!DSApi.isContextOpen()) {
                isOpened = false;
                DSApi.openAdmin();
            }

            boolean czyKwotaPonizej = ZamowieniePubliczneLogic.checkIfAmountIsLessOrEqual(
                    docId, new BigDecimal(ZamowieniePubliczneLogic.pobierzProgPrzetargu()));
            
			if (!czyKwotaPonizej) {
				if (ZamowieniePubliczneLogic.sprawdzCzyInneWylaczeniaUstawowe(docId))
					return AKCEPTACJA_KIEROWNIKA_DZP_CN;
				else
					return WSZCZECIE_POSTEPOWANIA_PRZETARGOWEGO_CN;
			} else if (ZamowieniePubliczneLogic.checkIfCentralPurchasing(docId)) {
				if (ZamowieniePubliczneLogic.sprawdzCzyInneWylaczeniaUstawowe(docId))
					return AKCEPTACJA_KIEROWNIKA_DZP_CN;
				else
					return WSZCZECIE_POSTEPOWANIA_PRZETARGOWEGO_CN;
			} else {
				return REALIZACJA_ZAMOWIENIA_SAMODZIELNA_CN;
			}
			
        }catch (Exception e) {
            log.error("B��d podczas trzeciego sprawdzenia przy wniosku zam�wienia publicznego: "+e);
        }finally {
            if (DSApi.isContextOpen() && !isOpened)
                DSApi._close();
        }
        return WSZCZECIE_POSTEPOWANIA_PRZETARGOWEGO_CN;
    }
}
