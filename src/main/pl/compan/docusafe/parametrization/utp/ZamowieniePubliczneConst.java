package pl.compan.docusafe.parametrization.utp;

import java.util.ArrayList;
import java.util.HashMap;

import pl.compan.docusafe.core.dockinds.dwr.EnumValues;

public interface ZamowieniePubliczneConst {
//	Widoki

	public static String WIDOK_BUDZET_KOM_TYLKO_KOM="dsg_budzetyKO_komorek_tylko_kom";
	public static String WIDOK_JM="simple_erp_per_docusafe_jm_view";
	public static String WIDOK_STAWKA_VAT="simple_erp_per_docusafe_vatstaw_view";	
	public static String WIDOK_PRODUKTY="simple_erp_per_docusafe_wytwory_zapdost_view";
	public static String WIDOK_ID_PLANU="dsg_plany_zakupow_view";	
	public static String WIDOK_DYSPONENTOW="INVOICE_GENERAL_DYSPONENCI";
	public static String WIDOK_ID_BUDZETU="dsg_invoice_general_okresy_budz";
	public static String WIDOK_BUDZET_KOM_TYLKO_BUDZET="dsg_budzetyKO_komorek_tylko_budzet";	
	public static String WIDOK_BUDZETY_DO_KOMOREK="dsg_budzetyKO_do_komorek";
	public static String WIDOK_TYPY_REZERWACJI="typy_rezerwacji";
	public static String TABELA_JM_SZTUKA_IDN="szt.";
	
	
	public static final String STATUS_CN = "STATUS";
	public static final String RODZAJ_WNIOSKU_CN = "RODZAJ_WNIOSKU";
	public static final String RODZAJ_WNIOSKU_NOTATKA_CN = "NOTATKA";
	public static final String RODZAJ_WNIOSKU_ZAPROSZENIE_CN = "ZAPROSZENIE";
	public static final String RODZAJ_WNIOSKU_WSZCZECIE_POSTEPOWANIA_CN = "WSZCZECIE_POSTEPOWANIA";	
	public static final String RODZAJ_ZAMOWIENIA_CN = "RODZAJ_ZAMOWIENIA";
	public static final String RODZAJ_ZAMOWIENIA_ROBOTA_BUDOWLANA_ID = "3";
	public static final String CZY_ROBOTA_BUDTELE_CN = "CZY_ROBOTA_BUDTELE";
	public static final String OS_REJESTRUJACA_CN = "OS_REJESTRUJACA";
	public static final String OS_REJESTRUJACA_DZIAL_CN = "OS_REJESTRUJACA_DZIAL";
	public static final String NR_WNIOSKU_CN = "NR_WNIOSKU";
	public static final String ROK_BUDZETOWY_CN = "ROK_BUDZETOWY";
	public static final String DATA_UTWORZENIA_CN = "DATA_UTWORZENIA";
	public static final String NETTO_CN = "NETTO";
	public static final String BRUTTO_CN = "BRUTTO";
	public static final String NETTO_EURO_CN = "NETTO_EURO";
	public static final String KURS_CN = "KURS";	
	public static final String DATA_UST_WART_ZAM_CN = "DATA_UST_WART_ZAM";
	public static final String OS_UST_WART_ZAM_CN = "OS_UST_WART_ZAM";
	public static final String KODY_CPV_CN = "KODY_CPV";
	public static final String KODY_CPV_KODENAME = "CN";
	public static final String KODY_CPV_TITLE = "TITLE";
	public static final String PODSTAWA_ZAM_CN = "PODSTAWA_ZAM";
	public static final Long PODSTAWA_ZAM_INNE_ID = 6L;	
	public static final String OPIS_PODSTAWA_ZAM_INNE_CN = "OPIS_PODSTAWA_ZAM_INNE";
	public static final String OSWIADCZENIE_NOTATKA_CN = "OSWIADCZENIE_NOTATKA";
	public static final String MIEJSCE_REALIZACJ_CN = "MIEJSCE_REALIZACJ";
	public static final String GWARANCJA_CN = "GWARANCJA";
	public static final String TYP_REZERWACJI_CN="TYP_REZERWACJI";
	public static final String TYP_REZERWACJI_ERP_CN="TYP_REZERWACJI_ERP";
	public static final String TYP_REZERWACJI_WOZ_CN="1";
	public static final String TYP_REZERWACJI_WB_CN="0";
	public static final String CZY_ZAKUP_Z_ARTYUKULU_6A_CN = "CZY_ART_6A";
    public static final String CZY_ZAGROZENIE_PRZEKROCZENIA_PROGU_14TYS_CN = "CZY_ZAGROZENIE";
    public static final String CZY_ZAKUP_CENTRALNY_CN = "CZY_ZC";
    public static final String CZY_INNE_WYLACZENIA_USTAWOWE_CN = "CZY_INNE";

//	S�ownik - Podmioty mog�ce realizowa� zam�wienie
	public static final String PODMIOTY_REALIZUJACE_CN = "PODMIOTY_REALIZUJACE";
	public static final String PODMIOTY_REALIZUJACE_NAZWA_I_ADRES_WYKONAWCY_CN = "NAZWA_I_ADRES_WYKONAWCY";
	public static final String PODMIOTY_REALIZUJACE_WYCENA_ZAM_BRUTTO_CN = "WYCENA_ZAM_BRUTTO";
	public static final String PODMIOTY_REALIZUJACE_UWAGI_CN = "UWAGI";
	public static final String PODMIOTY_REALIZUJACE_MIEJSCE_REALIZACJI_CN = "MIEJSCE_REALIZACJI";
	public static final String PODMIOTY_REALIZUJACE_GWARANCJA_CN = "GWARANCJA";
	
//  S�ownik - Pozycje bud�etowe
	public static final String POZYCJE_BUDZETOWE_CN = "POZYCJE_BUDZETOWE";
	public static final String POZYCJE_BUDZETOWE_POZYCJAID_CN = "POZYCJAID";
	public static final String POZYCJE_BUDZETOWE_OKRES_BUDZETOWY_CN = "OKRES_BUDZETOWY";
	public static final String POZYCJE_BUDZETOWE_KOMORKA_BUDZETOWA_CN = "KOMORKA_BUDZETOWA";
	public static final String POZYCJE_BUDZETOWE_IDENTYFIKATOR_PLANU_CN = "IDENTYFIKATOR_PLANU";
	public static final String POZYCJE_BUDZETOWE_IDENTYFIKATOR_BUDZETU_CN = "IDENTYFIKATOR_BUDZETU";
	public static final String POZYCJE_BUDZETOWE_POZYCJA_PLANU_ZAKUPOW_CN = "POZYCJA_PLANU_ZAKUPOW";
	public static final String POZYCJE_BUDZETOWE_POZYCJA_BUDZETU_CN = "POZYCJA_BUDZETU";
	public static final String POZYCJE_BUDZETOWE_PROJEKT_CN = "PROJEKT";
	public static final String POZYCJE_BUDZETOWE_PRODUKT_CN = "PRODUKT";
	public static final String POZYCJE_BUDZETOWE_OPIS_CN = "OPIS";
	public static final String POZYCJE_BUDZETOWE_ILOSC_CN = "ILOSC";
	public static final String POZYCJE_BUDZETOWE_JM_CN = "JM";
	public static final String POZYCJE_BUDZETOWE_STAWKA_CN = "STAWKA_VAT";
	public static final String POZYCJE_BUDZETOWE_NETTO_CN = "NETTO";
	public static final String POZYCJE_BUDZETOWE_BRUTTO_CN = "BRUTTO";
	public static final String POZYCJE_BUDZETOWE_UWAGI_CN = "UWAGI";
	public static final String POZYCJE_BUDZETOWE_DYSPONENT_CN = "DYSPONENT";
	public static final String POZYCJE_BUDZETOWE_DATA_AKCEPTACJI_CN = "DATA_AKCEPTACJI";
	public static final String POZYCJE_BUDZETOWE_STATUS_AKCEPTACJI_CN = "STATUS_AKCEPTACJI";

//	S�ownik - Akceptacje
	public static final String AKCEPTACJE_CN = "AKCEPTACJE";
	public static final String AKCEPTACJE_DYSPONENTOW_CN = "AKCEPTACJE_DYS";
	public static final String AKCEPTACJE_ID_CN = "ID";
	public static final String AKCEPTACJE_OSOBA_CN = "OSOBA";
	public static final String AKCEPTACJE_DATA_CN = "DATA";
	public static final String AKCEPTACJE_STATUS_CN = "STATUS";
	public static final String AKCEPTACJE_UWAGI_CN = "UWAGI";

	public static final String TERMIN_WYKONANIA_ZAM_CN = "TERMIN_WYKONANIA_ZAM";
	public static final String OKRES_GWARANCJI_REKOJMI_CN = "OKRES_GWARANCJI_REKOJMI";
	public static final String WARUNKI_PLATNOSCI_CN = "WARUNKI_PLATNOSCI";
	public static final String TERMIN_ZLOZENIA_OFERTY_CN = "TERMIN_ZLOZENIA_OFERTY";
	public static final String W_MIEJSCU_CN = "W_MIEJSCU";
	public static final String DOPISEK_NA_KOPERTE_CN = "DOPISEK_NA_KOPERTE";
	public static final String FAKSEM_NA_CN = "FAKSEM_NA";
	public static final String MAILEM_NA_CN = "MAILEM_NA";
	
//	S�ownik - Kryteria decyzji
	public static final String KRYTERIA_DECYZJI_CN = "KRYTERIA_DECYZJI";
	public static final String KRYTERIA_DECYZJI_ID_CN = "ID";
	public static final String KRYTERIA_DECYZJI_KRYTERIUM_CN = "KRYTERIUM";
	public static final String KRYTERIA_DECYZJI_WAGA_CN = "WAGA";
	
//	S�ownik - Zebrane oferty
	public static final String ZEBRANE_OFERTY_CN = "ZEBRANE_OFERTY";
	public static final String ZEBRANE_OFERTY_ID_CN = "ID";
	public static final String ZEBRANE_OFERTY_NAZWA_WYKONAWCY_CN = "NAZWA_WYKONAWCY";
	public static final String ZEBRANE_OFERTY_ADRES_CN = "ADRES";
	public static final String ZEBRANE_OFERTY_NETTO_CN = "NETTO";
	public static final String ZEBRANE_OFERTY_INNE_KRYTERIA_CN = "INNE_KRYTERIA";
	public static final String ZEBRANE_OFERTY_UWAGI_CN = "UWAGI";
	public static final String ZEBRANE_OFERTY_MIEJSCE_REALIZACJI_CN = "MIEJSCE_REALIZACJI";
	public static final String ZEBRANE_OFERTY_OKRES_GWARANCJI_CN = "OKRES_GWARANCJI";
	public static final String ZEBRANE_OFERTY_WARUNKI_PLATNOSCI_CN = "WARUNKI_PLATNOSCI";

	public static final String UZASADNIENIE_WYBORU_CN = "UZASADNIENIE_WYBORU";
	public static final String OSWIADCZENIE_ZAPROSZENIE_CN = "OSWIADCZENIE_ZAPROSZENIE";
	public static final String ZALACZNIKI_CN = "ZALACZNIKI";
	public static final String OSOBA_USTALAJACA_CN = "OSOBA_USTALAJACA";
	public static final String DATA_USTALENIA_CN = "DATA_USTALENIA";
	public static final String UJETO_W_PLANIE_POZ_CN = "UJETO_W_PLANIE_POZ";
	public static final String NR_SYGNATURY_CN = "NR_SYGNATURY";
	public static final String OSOBA_MERYTORYCZNA_CN = "OSOBA_MERYTORYCZNA";
	public static final String AKCEPTACJA_DZIALU_CN = "AKCEPTACJA_DZIALU";
	public static final String OSOBA_SKLADAJACA_ZAM_CN = "OSOBA_SKLADAJACA_ZAM";
	public static final String JEDNOSTKA_ZAMAWIAJACEGO_CN = "JEDNOSTKA_ZAMAWIAJACEGO";
	public static final String UPOWAZNIONY_PRACOWNIK_CN = "UPOWAZNIONY_PRACOWNIK";
	public static final String ERP_DOCUMENT_IDM_CN = "ERP_DOCUMENT_IDM";
	public static final String ERP_STATUS_CN = "ERP_STATUS";

    //S�ownik �r�de� finansowania
    public static final String ZRODLA_FINANSOWANIA_CN = "ZRODLA_FINANSOWANIA";
    public static final String ZRODLA_FINANSOWANIA_ZADANIE_FINANSOWE_BUDZETU_RAW_CN = "ZADANIE_FINANSOWE_BUDZETU_RAW";
    public static final String ZRODLA_FINANSOWANIA_ZADANIE_FINANSOWE_PLANU_RAW_CN = "ZADANIE_FINANSOWE_PLANU_RAW"; 
    public static final String ZRODLA_FINANSOWANIA_PROCENT_CN = "PROCENT";
    public static final String ZRODLA_FINANSOWANIA_NETTO_CN = "BRUTTO";
    public static final String ZRODLA_FINANSOWANIA_POZYCJAID_CN = "POZYCJAID";

    
    public static final String ACC_DYSPONENCI_CN = "akceptacja_dysponentow";
    public static final String ACC_KIEROWNIK_JEDNOSTKI_CN = "kierownik_jednostki";
    public static final String ACC_PELNOMOCNIK_DZP_CN = "pelnomocnik_dzp";
    public static final String ACC_PELNOMOCNIK_DFA_CN = "pelnomocnik_dfa";
    
        
	public static final EnumValues EMPTY_ENUM_VALUE = new EnumValues(new HashMap<String,String>(), new ArrayList<String>());
}
