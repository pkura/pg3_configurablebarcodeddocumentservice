package pl.compan.docusafe.parametrization.utp;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


import com.google.common.base.Strings;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;

import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;

import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;

import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.JournalEntry;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.handlers.DocumentMailHandler;
import pl.compan.docusafe.events.handlers.DocumentMailHandler.DocumentMailBean;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

public class NormalLogic extends AbstractDocumentLogic {

	private static NormalLogic instance;
	protected static Logger log = LoggerFactory.getLogger(NormalLogic.class);
	private StringManager sm = GlobalPreferences.loadPropertiesFile(
			NormalLogic.class.getPackage().getName(), null);
	public final static String BARCODE = "BARCODE";
	public final static String INCOMINGDATE = "INCOMINGDATE";
	public static final String LINKS_CN = "LINKS";
	public static final String MAILEVENTSIGN = "signEvent";
	public static final String MAILNEWVERSION = "documentVersion";
	private boolean dodajDoHist = false;
	
	public static NormalLogic getInstance() {
		if (instance == null)
			instance = new NormalLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));
		
		for (PermissionBean perm : perms) {
			if (perm.isCreateGroup()
					&& perm.getSubjectType().equalsIgnoreCase(
							ObjectPermission.GROUP)) {
				String groupName = perm.getGroupName();
				if (groupName == null) {
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	public void setInitialValues(FieldsManager fm, int type)
			throws EdmException {
		log.info("type: {}", type);
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("TYPE", type);
		SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
		if(fm.getDocumentKind().getCn().equals("normal")) {
			
			if (values.get("DOC_DELIVERY") == null) {
				values.put("DOC_DELIVERY", 25);
			} else {
				values.put("DOC_DELIVERY", null);
			}
			if (values.get("typ_slownika") == null ){
				values.put("typ_slownika", 4);
			} else {
				values.put("typ_slownika", null);
			}
			if (values.get("RODZAJ_PISMA_IN") == null ){
				values.put("RODZAJ_PISMA_IN", 7);
			} else {
				values.put("RODZAJ_PISMA_IN", null);
			}
			if (values.get("oryginal_kopia") == null ){
				values.put("oryginal_kopia", 0);
			} else {
				values.put("oryginal_kopia", null);
			}
		}
		if(fm.getDocumentKind().getCn().equals("normal")){
			
			if (values.get("data") == null) {
				values.put("data", new Date());
			} else {
				values.put("data", null);
			}
			if (values.get("DOC_DATE") == null) {
				Calendar docDate = Calendar.getInstance();
				docDate.setTime(new Date());
				docDate.add(Calendar.DATE, -3);
				values.put("DOC_DATE", fmt.format(docDate.getTime()));
			} else {
				values.put("DOC_DATE", null);
			}
			if (values.get("data_nadania") == null) {
				Calendar dataNadania = Calendar.getInstance();
				dataNadania.setTime(new Date());
				dataNadania.add(Calendar.DATE, -3);
				values.put("data_nadania", fmt.format(dataNadania.getTime()));
			} else {
				values.put("data_nadania", null);
			}
		}
			
		if(fm.getDocumentKind().getCn().equals("normal_out")){
			if(values.get("DOC_DELIVERY") == null) {
				values.put("DOC_DELIVERY", 26);
			} else {
				values.put("DOC_DELIVERY", null);
			}
			if (values.get("typ_slownika") == null ) {
				values.put("typ_slownika", 4);
			} else {
				values.put("typ_slownika", null);
			}
		}
		
		if(fm.getDocumentKind().getCn().equals("normal_out")){
			fm.getField("RODZAJ_PISMA_OUT").setReadOnly(false);

			fm.getField("DOC_DESCRIPTION").setReadOnly(false);
			fm.getField("SENDER_HERE").setReadOnly(false);
			fm.getField("DOC_DATE").setReadOnly(false);
			fm.getField("POSTAL_REG_NR").setReadOnly(false);
			fm.getField("GABARYT").setReadOnly(false);
			fm.getField("BARCODE").setReadOnly(false);
			fm.getField("potwierdzenie_odbioru").setReadOnly(false);
			fm.getField("DOC_DELIVERY").setReadOnly(false);
			fm.getField("sposob_obslugi").setReadOnly(false);
			fm.getField("DOC_FORM").setHidden(true);
			fm.getField("WAGA").setReadOnly(false);
            fm.getField("JOURNAL").setReadOnly(false);
		}
		fm.reloadValues(values);
	}

	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) {
		
		try {
			if(fm.getDocumentKind().getCn().equals("normal")){
				if(values.get("DWR_DOC_DELIVERY") != null){
					String deliveryId = values.get("DWR_DOC_DELIVERY").getEnumValuesData().getSelectedId();

					if(deliveryId.equals("26") || deliveryId.equals("27")){
						fm.getField("POSTAL_REG_NR").setRequired(true);
					} else {
						fm.getField("POSTAL_REG_NR").setRequired(false);
					}
					if(deliveryId.equals("30") || deliveryId.equals("31") || deliveryId.equals("33")){
						fm.getField("Format").setRequired(true);
					} else {
						fm.getField("Format").setRequired(false);
					}

                    // 33 pendrive, 31 epuap, 30 email, 29 fax
                    if(deliveryId.equals("33")  || deliveryId.equals("31") || deliveryId.equals("30") || deliveryId.equals("29")){
                        fm.getField("BARCODE").setRequired(false);
                    } else {
                        fm.getField("BARCODE").setRequired(true);
                    }

				}
			}
		
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}
		if(values.get("DWR_LINKS") != null ){
			Map<String, FieldData> map = values.get("DWR_LINKS").getDictionaryData();
				for (String key : map.keySet()) {
					if(fm.getDocumentId() != null && map.get(key).getData() != null){
						if (map.get(key).getData().toString().equalsIgnoreCase(fm.getDocumentId().toString())) {
							return new Field(
									"Uwaga: Ustawi貫� powi頊anie do dokumentu w kt鏎ym aktualnie si� znajdujesz.",
									"messgeField");
						}
					}
				}
			}
		return null; 
	}

	@Override
	public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
	{
		OfficeDocument doc = null;
		if(documentId != null && values.get("BARCODE") != null){
			doc = OfficeDocument.find(documentId);
			String barcode = (String) values.get(BARCODE);
			if(!barcode.equals(doc.getBarcode())){
				sprawdzCzyIstniejeBarcode(values);
			}
        } else if(documentId == null && !Strings.isNullOrEmpty((String) values.get("BARCODE"))){
			sprawdzCzyIstniejeBarcode(values);
		}


		//dodanie na formatke kolejnego numeru z dziennika wydzialowego
		if(values.get("JOURNAL") != null){
			String dziennik = String.valueOf(values.get("JOURNAL"));
			PreparedStatement ps = null;
			ResultSet rs = null;
			int numer = 0;
			if(documentId != null){
				doc = OfficeDocument.find(documentId);
				List<JournalEntry> journal = JournalEntry.findByDocumentId(documentId);
				for(JournalEntry j:journal){
					if(j.getJournal().getId().equals(Long.valueOf(dziennik))){
						numer = j.getSequenceId();
						values.put("NRKOLEJNY", numer);
					}
				}
			}
			if(numer == 0){
				try {
					ps = DSApi.context().prepareStatement("SELECT MAX(sequenceid) as numer from dso_journal_entry where JOURNAL_ID=?");

					ps.setLong(1, Long.valueOf(dziennik));
					rs = ps.executeQuery();
					rs.next();
					numer = rs.getInt("numer");

					values.put("NRKOLEJNY", numer+1);
					dodajDoHist = true;
				} catch (SQLException e) {
					log.error("", e);
				}
			}
		}
	}

	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {
		as.useFolderResolver(document);
		as.useAvailableProviders(document);
		log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) {
		log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
		try {
			Map<String, Object> map = Maps.newHashMap();
			if (document instanceof InOfficeDocument) {
				map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
			} else {
				map.put(ASSIGN_USER_PARAM,
						DSApi.context().getPrincipalName());
				map.put(ASSIGN_DIVISION_GUID_PARAM,
						event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			}

			
			document.getDocumentKind().getDockindInfo()
					.getProcessesDeclarations().onStart(document, map);

			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		 
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public void onSaveActions(DocumentKind kind, Long documentId,
			Map<String, ?> fieldValues) throws SQLException, EdmException {

		if(fieldValues.get("LINKS") != null){
			List<Long> list = (List<Long>)fieldValues.get("LINKS");
			for (Long linkId : list) {
				if(linkId.toString().equalsIgnoreCase(documentId.toString())){
					throw new EdmException("Nie dozwolone powi頊anie dokument闚.");
				}
			}
		}

		OfficeDocument document = OfficeDocument.find(documentId);
		String dziennik = "";

		if(fieldValues.get("JOURNAL") != null){
			dziennik = String.valueOf(fieldValues.get("JOURNAL"));
			Journal jName = Journal.find(Long.valueOf(dziennik));
			if(dodajDoHist)
				document.addWorkHistoryEntry(Audit.create("journal", DSApi.context().getPrincipalName(),
						sm.getString("DodanoDoDziennikaWydzialowego",jName.getDescription())));
		}
			if(document.getDocumentKind().getCn().equals("normal_out")){
				if(document.getCaseDocumentId() != null){
					if(fieldValues.get("zwrotka") != null){
						boolean zwrotka =(Boolean) fieldValues.get("zwrotka");
						if(zwrotka)
							DocumentMailHandler.createEvent(documentId, "docInCase");
					}
				}
			}
			if (document.getDocumentKind().getCn().equals("normal_out")) {
				if (fieldValues.containsKey("NUMER_KO")
						&& fieldValues.get("NUMER_KO") != null) {
					try {
						Integer nko = Integer.parseInt(((String) fieldValues
								.get("NUMER_KO")));
						document.setOfficeNumber(nko);

					} catch (Exception e) {
						log.error(e.getMessage(), e);
					}
				}
			} else if (document.getDocumentKind().getCn().equals("normal")) {
				if (fieldValues.containsKey("STATUS_REALIZACJI")) {
					try {

					} catch (Exception e) {
						log.error(e.getMessage(), e);
					}
				}
			}
			
			String barcode = document.getFieldsManager()
					.getStringValue(BARCODE);

			if (document instanceof InOfficeDocument) {
				InOfficeDocument in = (InOfficeDocument) document;
				if (barcode != null) {
					in.setBarcode(barcode);
				} else {
					in.setBarcode(null);
				}
			} else if (document instanceof OutOfficeDocument) {
				OutOfficeDocument od = (OutOfficeDocument) document;
				if (barcode != null) {
					od.setBarcode(barcode);
				} else {
					od.setBarcode(null);
				}
			}
			
			if(document.getDocumentKind().getCn().equals("normal")){
				// dodanie jednego dnia dla pola data otrzymania na liscie zadan
				SimpleDateFormat fmtTime = new SimpleDateFormat("HH:mm:ss");
				SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
				Date dat = new Date();
				String time = fmtTime.format(dat);
				Calendar c = Calendar.getInstance();
				if(fieldValues.get("data") != null){
					try {
						String test = fieldValues.get("data").toString();
						//				c.setTime((Date) fieldValues.get("data")); 

						Date data = new SimpleDateFormat("dd-MM-yyyy").parse(test);
						String newData = data + " "+ time;
						Date da;

						da = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(newData);
						InOfficeDocument doc = InOfficeDocument.findInOfficeDocument(documentId);
						doc.setIncomingDate(da);
					} catch (ParseException e) {
						log.error(e.getMessage(), e);
					}
				}
			}
	/*	if(document.getBarcode() == null){
			if(fm.getField("BARCODE") != null){
				PreparedStatement ps = null;
				ResultSet rs = null;
				try
				{
					ps = DSApi.context().prepareStatement(" select BARCODE from DS_UTP_DOC_IN WHERE document_id<>?");
					ps.setLong(1, documentId);
					rs = ps.executeQuery();
					while(rs.next())
					{
						if(rs.getString(1) != null){
							String barcode = rs.getString(1);
							if(barcode.equals(fieldValues.get("BARCODE"))){
								throw new EdmException("Podany numer barcode, istnieje ju� w systemie");
							}
						}
					}
					ps = DSApi.context().prepareStatement(" select BARCODE from DS_UTP_DOC_INT WHERE document_id<>?");
					ps.setLong(1, documentId);
					rs = ps.executeQuery();
					while(rs.next())
					{
						if(rs.getString(1) != null){
							String barcode = rs.getString(1);
							if(barcode.equals(fieldValues.get("BARCODE"))){
								throw new EdmException("Podany numer barcode, istnieje ju� w systemie");
							}
						}
					}
					ps = DSApi.context().prepareStatement(" select BARCODE from DS_UTP_DOC_OUT WHERE document_id<>?");
					ps.setLong(1, documentId);
					rs = ps.executeQuery();
					while(rs.next())
					{
						if(rs.getString(1) != null){
							String barcode = rs.getString(1);
							if(barcode.equals(fieldValues.get("BARCODE"))){
								throw new EdmException("Podany numer barcode, istnieje ju� w systemie");
							}
						}
					}
				}
				finally
				{
					DbUtils.closeQuietly(rs);
					DSApi.context().closeStatement(ps);
				}
			}
		}*/
	}

	public void setAdditionalTemplateValues(long docId,
			Map<String, Object> values) {

		try {
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			fm.initialize();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String number;
			String allnumber = " ";
			List<Map> lista = (List<Map>) fm.getValue("NUMBER_DICT");
			try {
				for (Map m : lista) {
					number = (String) m.get("NUMBER_DICT_CN");
					allnumber += number + ",  ";

				}
				values.put("DOC_NUMBER", allnumber);
			} catch (Exception e) {

				values.put("DOC_NUMBER", "Brak numeru dokumentu");
			}

			try {
				if (fm.getValue("DOC_DATE") != null) {
					values.put("DATA", dateFormat.format(fm.getValue("DOC_DATE")));
				} else {
					values.put("DATA", " dokument bez daty");
				}
			} catch (Exception e) {

			}

			// numer KO
			try {
				values.put("KO", doc.getOfficeNumber().toString());
			} catch (Exception e) {
				values.put("KO", "...........");
			}
			try{
				values.put("Barcode1", doc.getBarcode());
			} catch (Exception e) {
				values.put("Barcode1", "Brak numeru Barcode");

			}

			Date date = new Date();
			values.put("currentDate", dateFormat.format(date));
			values.put("incDate", dateFormat.format(doc.getCtime()));
			DSUser user = DSUser.findByUsername(DSApi.context().getPrincipalName());
			values.put("imie", user.getFirstname());
			values.put("nazwisko", user.getLastname());
			List<DSDivision> division = user.getDivisionsAsList();
			for(DSDivision d:division){
				values.put("division", d.getName());
			}

			if(fm.getValue("ZNAKPRZESYLKI") != null){
				values.put("zn", fm.getValue("ZNAKPRZESYLKI"));
			} else {
				values.put("zn", "");
			}
			if(doc instanceof InOfficeDocument){
				if(fm.getValue("typ_slownika") != null){
					if(fm.getEnumItem("typ_slownika").getCn().equalsIgnoreCase("pozostali")){
						Sender s = doc.getSender();
						values.put("im", s.getFirstname());
						values.put("nazwiskoS", s.getLastname());
						values.put("firma", s.getOrganization());
						values.put("adres", s.getStreet());
						values.put("miejscowosc", s.getLocation());
						values.put("kodpocztowy", s.getZip());
					} else if(fm.getEnumItem("typ_slownika").getCn().equalsIgnoreCase("kandydat")){
						Map<String, Object> kandydat =  (Map<String, Object>) fm.getValue("KANDYDAT");
						for(Map.Entry<String, Object> m:kandydat.entrySet()){
							if(m.getKey().equalsIgnoreCase("KANDYDAT_IMIE")){
								values.put("im", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("KANDYDAT_NAZWISKO")){
								values.put("nazwiskoS", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("KANDYDAT_ULICA")){
								values.put("adres", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("KANDYDAT_KODPOCZTOWY")){
								values.put("kodpocztowy", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("KANDYDAT_MIEJSCOWOSC")){
								values.put("miejscowosc", m.getValue());
							}
						}
					} else if(fm.getEnumItem("typ_slownika").getCn().equalsIgnoreCase("kontrahent")){
						Map<String, Object> kontrahent =  (Map<String, Object>) fm.getValue("KONTRAHENT");
						for(Map.Entry<String, Object> m:kontrahent.entrySet()){
							if(m.getKey().equalsIgnoreCase("KONTRAHENT_IMIE")){
								values.put("im", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("KONTRAHENT_NAZWISKO")){
								values.put("nazwiskoS", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("KONTRAHENT_ULICA")){
								values.put("adres", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("KONTRAHENT_KODPOCZTOWY")){
								values.put("kodpocztowy", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("KONTRAHENT_MIEJSCOWOSC")){
								values.put("miejscowosc", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("KONTRAHENT_NAZWA")){
								values.put("firma", m.getValue());
							}
						}
					} else if(fm.getEnumItem("typ_slownika").getCn().equalsIgnoreCase("pracownik")){
						Map<String, Object> pracownik =  (Map<String, Object>) fm.getValue("PRACOWNIK");
						for(Map.Entry<String, Object> m:pracownik.entrySet()){
							if(m.getKey().equalsIgnoreCase("PRACOWNIK_IMIE")){
								values.put("im", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("PRACOWNIK_NAZWISKO")){
								values.put("nazwiskoS", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("PRACOWNIK_ULICA")){
								values.put("adres", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("PRACOWNIK_KODPOCZTOWY")){
								values.put("kodpocztowy", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("PRACOWNIK_MIEJSCOWOSC")){
								values.put("miejscowosc", m.getValue());
							}
						}
					} else if(fm.getEnumItem("typ_slownika").getCn().equalsIgnoreCase("student")){
						Map<String, Object> student =  (Map<String, Object>) fm.getValue("STUDENT");
						for(Map.Entry<String, Object> m:student.entrySet()){
							if(m.getKey().equalsIgnoreCase("STUDENT_IMIE")){
								values.put("im", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("STUDENT_NAZWISKO")){
								values.put("nazwiskoS", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("STUDENT_ULICA")){
								values.put("adres", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("STUDENT_KODPOCZTOWY")){
								values.put("kodpocztowy", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("STUDENT_MIEJSCOWOSC")){
								values.put("miejscowosc", m.getValue());
							}
						}
					}
				}
			} else {
				if(fm.getValue("typ_slownika") != null){
					if(fm.getEnumItem("typ_slownika").getCn().equalsIgnoreCase("pozostali")){
						List<Recipient> recipient = doc.getRecipients();
						for(Recipient r:recipient){
							values.put("im", r.getFirstname());
							values.put("nazwiskoS", r.getLastname());
							values.put("firma", r.getOrganization());
							values.put("adres", r.getStreet());
							values.put("miejscowosc", r.getLocation());
							values.put("kodpocztowy", r.getZip());
						} 
					} else if(fm.getEnumItem("typ_slownika").getCn().equalsIgnoreCase("kandydat")){
						Map<String, Object> kandydat =  (Map<String, Object>) fm.getValue("KANDYDAT");
						for(Map.Entry<String, Object> m:kandydat.entrySet()){
							if(m.getKey().equalsIgnoreCase("KANDYDAT_IMIE")){
								values.put("im", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("KANDYDAT_NAZWISKO")){
								values.put("nazwiskoS", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("KANDYDAT_ULICA")){
								values.put("adres", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("KANDYDAT_KODPOCZTOWY")){
								values.put("kodpocztowy", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("KANDYDAT_MIEJSCOWOSC")){
								values.put("miejscowosc", m.getValue());
							}
						}
					} else if(fm.getEnumItem("typ_slownika").getCn().equalsIgnoreCase("kontrahent")){
						Map<String, Object> kontrahent =  (Map<String, Object>) fm.getValue("KONTRAHENT");
						for(Map.Entry<String, Object> m:kontrahent.entrySet()){
							if(m.getKey().equalsIgnoreCase("KONTRAHENT_IMIE")){
								values.put("im", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("KONTRAHENT_NAZWISKO")){
								values.put("nazwiskoS", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("KONTRAHENT_ULICA")){
								values.put("adres", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("KONTRAHENT_KODPOCZTOWY")){
								values.put("kodpocztowy", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("KONTRAHENT_MIEJSCOWOSC")){
								values.put("miejscowosc", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("KONTRAHENT_NAZWA")){
								values.put("firma", m.getValue());
							}
						}
					} else if(fm.getEnumItem("typ_slownika").getCn().equalsIgnoreCase("pracownik")){
						Map<String, Object> pracownik =  (Map<String, Object>) fm.getValue("PRACOWNIK");
						for(Map.Entry<String, Object> m:pracownik.entrySet()){
							if(m.getKey().equalsIgnoreCase("PRACOWNIK_IMIE")){
								values.put("im", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("PRACOWNIK_NAZWISKO")){
								values.put("nazwiskoS", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("PRACOWNIK_ULICA")){
								values.put("adres", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("PRACOWNIK_KODPOCZTOWY")){
								values.put("kodpocztowy", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("PRACOWNIK_MIEJSCOWOSC")){
								values.put("miejscowosc", m.getValue());
							}
						}
					} else if(fm.getEnumItem("typ_slownika").getCn().equalsIgnoreCase("student")){
						Map<String, Object> student =  (Map<String, Object>) fm.getValue("STUDENT");
						for(Map.Entry<String, Object> m:student.entrySet()){
							if(m.getKey().equalsIgnoreCase("STUDENT_IMIE")){
								values.put("im", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("STUDENT_NAZWISKO")){
								values.put("nazwiskoS", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("STUDENT_ULICA")){
								values.put("adres", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("STUDENT_KODPOCZTOWY")){
								values.put("kodpocztowy", m.getValue());
							} else if(m.getKey().equalsIgnoreCase("STUDENT_MIEJSCOWOSC")){
								values.put("miejscowosc", m.getValue());
							}
						}
					}
				}
			}
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}

	}

	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document)
			throws EdmException {
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	@Override
	public void onRejectedListener(Document doc) throws EdmException {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAcceptancesListener(Document doc, String acceptationCn)
			throws EdmException {
		// TODO Auto-generated method stub

	}

	/**
	 * eventParams[0] - id dokumentu
	 * eventParams[1] - akcja: signEvent, ...
	 * eventParams[2|3|4|...] - parametry dla akcji
	 */
	@Override
	public void prepareMailToSend(Document document, String[] eventParams, DocumentMailBean documentMailBean)
	{
		try {
			if(eventParams[1].equalsIgnoreCase("docInCase")){
				Long docId = document.getId();
				Document doc = null;
				DSUser user = null;
				OfficeDocument docCase = OfficeDocument.find(docId);
				if(OfficeCase.find(docCase.getContainingCaseId()) != null){
					OfficeCase ofcase = OfficeCase.find(docCase.getContainingCaseId());
					user = DSUser.findByUsername(ofcase.getClerk());
					Map<String, String> templateParameters = new HashMap<String, String>();
					templateParameters.put("imie", user.getFirstname());
					templateParameters.put("id", String.valueOf(docId));
					templateParameters.put("case", docCase.getCaseDocumentId());
					templateParameters.put("link", Docusafe.getBaseUrl() + "/office/outgoing/document-archive.action?documentId=" + document.getId());
					documentMailBean.setTemplateParameters(templateParameters);
					documentMailBean.setTemplate("document-in-case.txt");

					if(user.getEmail() == null || user.getEmail().isEmpty())
						throw new EdmException("Brak maila");
					documentMailBean.setEmail(user.getEmail());
				}
			} else if(eventParams[1].equalsIgnoreCase(MAILEVENTSIGN)) {
				prepareMailForSignEvent(document, eventParams, documentMailBean);
			} else if(eventParams[1].equalsIgnoreCase(MAILNEWVERSION)) {
				prepareMailForDocumentVersion(document, eventParams, documentMailBean);
			}
		} catch (EdmException e) {
			log.error("Nie uda這 si� wys豉� maila", e);
		} 
		
	}
	
	public TaskListParams getTaskListParams(DocumentKind dockind,
			long documentId) throws EdmException {
		TaskListParams params = new TaskListParams();
		try {
			FieldsManager fm = dockind.getFieldsManager(documentId);

			if (fm.getDocumentKind().getCn().equals("normal")){
				InOfficeDocument doc = InOfficeDocument
						.findInOfficeDocument(documentId);
				if (doc.getDelivery() != null)
					params.setCategory(doc.getDelivery().getName());
			}
		} catch (Exception e) {
			log.error("", e);
		}
		return params;
	}
	
	/**
	 * Zwraca list� u篡tkownik闚 kt鏎zy maja dost瘼 do dokumentu
	 * Na podstawie:
	 * 	ds_permission - Subjecttyp brany pod uwag� to user i group.
	 * 	tabela DSW_JBPM_TASKLIST - U kogo aktualnie znajduje si� pismo.
	 * @param documentId
	 * @return
	 */
	@Override
	public List<DSUser> getUserWithAccessToDocument(Long documentId){
		try {
			Set<PermissionBean> docperm = DSApi.context().getDocumentPermissions(Document.find(documentId));
			HashMap<String, PermissionBean> test = new HashMap<String, PermissionBean>();
			for (PermissionBean permissionBean : docperm) {
				if(!test.containsKey(permissionBean.getSubject())){
					test.put(permissionBean.getSubject(), permissionBean);					
				}
			}
			
			Set<String> keys = test.keySet();
			List<DSUser> dsUsers = new ArrayList<DSUser>();
			for (String string : keys) {
				if(test.get(string).getSubjectType().equalsIgnoreCase("user"))
					dsUsers.add(DSUser.findByUsername(string));
			}
			
			return dsUsers;
			
		} catch (EdmException e) {
			log.debug("b章d podczas szukania dokumentu.");
			return null;
		}
	}
		
	

	private void sprawdzCzyIstniejeBarcode(Map<String, Object> values) throws EdmException
	{
//		try
//		{
		if (values.get(BARCODE) != null)
		{
			String barc = (String) values.get(BARCODE);
			List<OfficeDocument> doc = OfficeDocument.findAllByBarcode(barc);
			List list = OfficeFolder.findByBarcode(barc);
			if (list != null && list.size()>0){
				throw new EdmException("Wprowadzony kod kreskowy = " + barc + " ju� istnieje w systemie !");
			}
			if(doc.size()>0){
				throw new EdmException("Wprowadzony kod kreskowy = " + barc + " ju� istnieje w systemie !");
			}
		}
				/*String sql = "select  TOP 1 *  from DS_UTP_DOC_IN t1 left join DS_UTP_DOC_OUT t2"
						+ " on ( t1.BARCODE = t2 .BARCODE) left join DS_UTP_DOC_INT t3 "
						+ " on ( t2.BARCODE = t3.BARCODE) where t1.BARCODE = ? or  t2.BARCODE = ? or t3.BARCODE = ? ";
				PreparedStatement ps;
				ps = DSApi.context().prepareStatement(sql);
				ps.setString(1, barc);
				ps.setString(2, barc);
				ps.setString(3, barc);
				ResultSet rs = ps.executeQuery();
				if (rs.next())
				{
				
				}
			}
		} catch (SQLException e)
		{
			log.error("B章d prz yweryfikacji barkoduw", e);
		}*/
	}
	/**
	 * eventParams[0|1] - sta貫 parametry
	 * eventParams[2] - adres email
	 * 
	 * @param document
	 * @param eventParams
	 * @param documentMailBean
	 * @throws EdmException 
	 */
	private void prepareMailForSignEvent(Document document, String[] eventParams, DocumentMailBean documentMailBean){
		if (eventParams.length == 3) {
			Map<String, String> templateParameters = new HashMap<String, String>();
			templateParameters.put("documentTitle", document.getTitle());
			templateParameters.put("user", DSApi.context().getPrincipalName());
			documentMailBean.setTemplateParameters(templateParameters);
			documentMailBean.setTemplate("mail-for-internal-sign-action.txt");
			documentMailBean.setEmail(eventParams[2]);
		} else {
			log.error("", "Brak wymaganych parametr闚 do wys豉nia powiadomienia.");
		}
	}
	
	private void prepareMailForDocumentVersion(Document document, String[] eventParams, DocumentMailBean documentMailBean){
		if (eventParams.length == 3) {
			Map<String, String> templateParameters = new HashMap<String, String>();
			templateParameters.put("documentTitle", document.getTitle());
			templateParameters.put("user", DSApi.context().getPrincipalName());
			documentMailBean.setTemplateParameters(templateParameters);
			documentMailBean.setTemplate("new-document-version.txt");
			documentMailBean.setEmail(eventParams[2]);
		} else {
			log.error("", "Brak wymaganych parametr闚 do wys豉nia powiadomienia.");
		}
	}
}
