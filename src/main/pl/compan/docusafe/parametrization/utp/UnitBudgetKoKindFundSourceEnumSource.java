package pl.compan.docusafe.parametrization.utp;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.enums.EnumDataSourceLogic;
import pl.compan.docusafe.core.dockinds.field.enums.EnumSourceException;
import pl.compan.docusafe.core.dockinds.field.enums.ExternalSourceEnumItem;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.util.axis.AxisClientUtils;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

public class UnitBudgetKoKindFundSourceEnumSource implements EnumDataSourceLogic {

    private static final Logger log = LoggerFactory.getLogger(UnitBudgetKoKindFundSourceEnumSource.class);


    @Override
    public List<EnumItem> getEnumItemsFromSource(String refFieldId) throws EnumSourceException {
        final List<EnumItem> enumItems = Lists.newLinkedList();
        if (Strings.isNullOrEmpty(refFieldId)) {
            return enumItems;
        }

        final Long parsedRefFieldId;
        try {
            parsedRefFieldId = Long.parseLong(refFieldId.split("#")[0]);
        } catch (NumberFormatException e) {
            log.error(e.getMessage(), e);
            return enumItems;
        }

        try {
            long start = System.currentTimeMillis();
            AxisClientConfigurator axisClientConfigurator = new AxisClientConfigurator();
            DictionaryServiceStub stub = new DictionaryServiceStub(axisClientConfigurator.getAxisConfigurationContext());
            axisClientConfigurator.setUpHttpParameters(stub, "/services/DictionaryService");
            axisClientConfigurator.setHttpClient(AxisClientUtils.createHttpClient(30, 30));
            DictionaryServiceStub.GetUnitBudgetKoKindFundSourceByParentID params = new DictionaryServiceStub.GetUnitBudgetKoKindFundSourceByParentID();
            params.setId(parsedRefFieldId);
            params.setPage(1);
            params.setPageSize(1000);
            DictionaryServiceStub.UnitBudgetKoKindFundSource[] wsItems = stub.getUnitBudgetKoKindFundSourceByParentID(params).get_return();
//            DictionaryServiceStub.UnitBudgetKoKindFinTask[] wsItems = stub.getUnitBudgetKoKindFinTaskByParentID(params).get_return();
            if (wsItems != null) {
                for (DictionaryServiceStub.UnitBudgetKoKindFundSource wsItem : wsItems) {
                    enumItems.add(ExternalSourceEnumItem.makeEnumItem("" + (long)wsItem.getId() + ExternalSourceEnumItem.SPLIT_CHAR + wsItem.getZrodlo_idn() + ExternalSourceEnumItem.SPLIT_CHAR + makeEnumTitle(wsItem)));
                }
            }
            log.error("UnitBudgetKoKindFinTaskEnumSource for {} total {}" + parsedRefFieldId, (System.currentTimeMillis()-start));
        } catch (Exception e) {
            throw new EnumSourceException(e.getMessage(), e);
        }

        return enumItems;
    }

    private String makeEnumTitle(DictionaryServiceStub.UnitBudgetKoKindFundSource wsItem) {
        StringBuilder sb = new StringBuilder();
        return sb.append(StringUtils.left(wsItem.getZrodlo_idn().trim()+": "+wsItem.getNazwa().trim(), 100))
                .append(" (plan: ")
                .append(BigDecimal.valueOf(wsItem.getP_koszt()).setScale(2, RoundingMode.HALF_UP))
                .append("; rezerwacje: ")
                .append(BigDecimal.valueOf(wsItem.getRez_koszt()).setScale(2, RoundingMode.HALF_UP))
                .append("; wykonanie: ")
                .append(BigDecimal.valueOf(wsItem.getR_koszt()).setScale(2, RoundingMode.HALF_UP))
                .append(")")
                .toString();
    }

    @Override
    public List<EnumItem> getEnumItemsFromSource() throws EnumSourceException {
        return null;
    }
}
