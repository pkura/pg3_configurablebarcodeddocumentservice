package pl.compan.docusafe.parametrization.utp.internalSignature;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Manager wewn�trznych sygnatur.
 * Zadania: Generowanie sygnatur, pobieranie sygnatur, sprawdzanie dla dokument�w itp.
 *
 */
public class InternalSignature {
	private static final Logger log = LoggerFactory.getLogger(InternalSignature.class);
	private static InternalSignature instance;

	private InternalSignature() { } 
	
    public static InternalSignature getInstance() {
        if(instance == null)
        	instance = new InternalSignature();
        return instance;
    }
	    
	/**
	 * Metoda sprawdza czy u�ytkownik o id userId podpisa� dokument o id dokumentId. 
	 * @param userId
	 * @param dokumentId
	 * @return
	 * @throws EdmException
	 */
	public boolean isSignByUser(Long userId, Long dokumentId) {
		if(userId == null || dokumentId == null) throw new NullPointerException();
		
		String sql = "select dsis.USER_ID, dsitd.DOCUMENT_ID from DS_INTERNAL_SIGN dsis"
				+ " left join DS_INT_TO_DOC dsitd on dsis.ID = dsitd.SIGN_ID"
				+ " where dsitd.DOCUMENT_ID = ?";
		
		PreparedStatement ps;
		boolean isSign = false;
		
		try {
			ps = DSApi.context().prepareStatement(sql);
			ps.setLong(1, dokumentId);
			ResultSet rs = ps.executeQuery();
			if(rs.getFetchSize() == 0) isSign = false;
			else{
				while (rs.next()) {
					if(rs.getLong(1) == userId){
						isSign = true;
						break;
					}
				}
			}
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return isSign;
	}

	/**
	 * Generuje sygnature dla u�ytkownika. Je�li u�ytkownik ma sygnatur� zwraca -1L.
	 * Dla wymuszenia wygenerowania nowej sygnatury {@link #generateSignature(Long, boolean))}
	 * 
	 * @param userId
	 * @return id sygnatury,
	 * null je�li nie mo�na wygenerowa� sygnatury lub wyst�pi� b��d.
	 */
	public Long generateSignature(Long userId) {
		return generateSignature(userId, false);
	}
	
	/**
	 * Metoda {@link #generateSignature(Long)} z wymuszeniem sygnatury.
	 * 
	 * @param userId 
	 * @param force Wymusza wygenerowanie sygnatury.
	 * @return id sygnatury,
	 * null je�li nie mo�na wygenerowa� sygnatury lub wyst�pi� b��d.
	 */
	public Long generateSignature(Long userId, boolean force) {
		if(hasSignature(userId))
			if (!force)
				return null;
		
		InternalSignatureEntity intSign = new InternalSignatureEntity();
		intSign.setUserId(userId);
		try {
			intSign.saveOrUpdate();			
		} catch (EdmException e) {
			log.error("", e);
			return null;
		}
		return intSign.getId();
	}
	
	/**
	 * Sprawdza czy u�ytkownik posiada sygnature.
	 * @TODO Do przetestowania.
	 * @param userId
	 * @return
	 * @throws EdmException
	 */
	@SuppressWarnings("unchecked")
	private boolean hasSignature(Long userId) {
		if (InternalSignatureManager.findByUserId(userId) != null)
			return true;
		return false;
	}
	
	/**
	 * Wyszukuje wszystkie sygnatury u�ytkownika po id u�ytkownika.
	 * @param signId
	 * @return
	 * @throws SQLException 
	 * @throws EdmException
	 */
	public synchronized List<Document> getSignedDocuments(Long signId) {
		
		List<Document> signedDocuments = null;
		
		boolean contextOpened = false;
		
		try {			
			contextOpened = DSApi.openContextIfNeeded();
			
			String sql = "SELECT DOCUMENT_ID FROM DS_INT_TO_DOC WHERE SIGN_ID = ?";
			
			PreparedStatement ps = DSApi.context().prepareStatement(sql);
			ps.setLong(1, signId);
			ResultSet rs = ps.executeQuery();
			
			signedDocuments = new ArrayList<Document>();
			while (rs.next()) {
				try {
					signedDocuments.add(Document.find(rs.getLong(1)));					
				} catch (EdmException edm) {
					log.error("", edm);
				}
			}
			
			ps.close();
		} catch (SQLException sqlex) {
			log.error("B��d bazy danych.", sqlex);
		} catch (EdmException e) {
			log.error("", e);
		} finally {
			DSApi.closeContextIfNeeded(contextOpened);
		}
		return signedDocuments;
	}
	
	/**
	 * Wyszukuje wszystkie sygnatury u�ytkownika po id u�ytkownika.
	 * @param signId
	 * @return
	 * @throws SQLException 
	 * @throws EdmException
	 */
	public synchronized List<InternalSignatureEntity> getSignatureForDocument(Long docId) {
		List<InternalSignatureEntity> returnList = new ArrayList<InternalSignatureEntity>();
		List<InternalToDocumentEntity> list = InternalToDocumentManager.findByDocumentId(docId);
		
		for (InternalToDocumentEntity internalToDocumentEntity : list) {
			returnList.add(InternalSignatureManager.find(internalToDocumentEntity.getSignId()));
		}
		
		if(returnList.isEmpty())
			return null;
		return returnList;	
	}
	
	public boolean signDocument(Long userId, Long docId){
		if(userId == null || docId == null) throw new NullPointerException();
		if(!hasSignature(userId)) return false;
		if(isSignByUser(userId, docId)) return false;
		
		boolean isOK = false;
		InternalSignatureEntity internalSignatureEntity = InternalSignatureManager.findUserSignature(userId);
		InternalToDocumentEntity internalToDocumentEntity = new InternalToDocumentEntity();
		internalToDocumentEntity.setDocumentId(docId);
		internalToDocumentEntity.setSignId(internalSignatureEntity.getId());
		try {
			internalToDocumentEntity.saveOrUpdate();
			isOK = true;
		} catch (EdmException e) {
			log.error("", e);
		}
		return isOK;
	}
}
