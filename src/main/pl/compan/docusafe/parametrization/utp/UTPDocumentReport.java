package pl.compan.docusafe.parametrization.utp;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.parametrization.utp.UTPDocReport;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.OrderClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

public class UTPDocumentReport extends Report{
	  //kom�rka organizacyjna		
	  //symbol klasyfikacyjny
	  //haslo klasyfikacyjne	
	
	
	    private String komorka;
	    private String symbol;
	    private String haslo;
	    private Date incomingDateFrom;
	    private Date incomingDateTo;
	    private UniversalTableDumper dumper;


	    public void initParametersAvailableValues() throws ReportException
	    {
	        super.initParametersAvailableValues();
	        try
	        {

	            for( ReportParameter rp : params )
	            {
	                if( rp.getFieldCn().equals("komorka") )
	                {
	                    rp.setAvailableValues(pobierzWszytkieKomorkiOrganizacyjne());
	                }
	                if( rp.getFieldCn().equals("symbol") )
	                {
	                    rp.setAvailableValues(pobierzWszytkieSymbole());
	                }
	                if( rp.getFieldCn().equals("haslo") )
	                {
	                    rp.setAvailableValues(pobierzWszytkieHasla());
	                }
	            }
	        }
	        catch ( Exception e )
	        {
	            throw new ReportException(e);
	        }
	    }
	    
	    
	    @Override
	    public void doReport() throws Exception
	    {
	        for( ReportParameter rp : params )
	        {
	        	if( rp.getFieldCn().equals("komorka") )
	            {
	        		komorka = rp.getValueAsString();
	            }
	        	 if( rp.getFieldCn().equals("symbol") )
		            {
	        		 symbol = rp.getValueAsString();
		            }
	        	 if( rp.getFieldCn().equals("haslo") )
		            {
		                haslo = rp.getValueAsString();
		            }
	        	 
	        	if( rp.getFieldCn().equals("incoming_date_from") )
	            {
	            	String[] tmp = rp.getValueAsString().split("-");
	            	if(tmp.length!=1){
	            		Date tmpDateFrom = new Date(Integer.parseInt(tmp[2])-1900, Integer.parseInt(tmp[1])-1, Integer.parseInt(tmp[0]));
	                	incomingDateFrom = tmpDateFrom;
	            	}
	            }
	            if( rp.getFieldCn().equals("incoming_date_to") )
	            {
	            	String[] tmp = rp.getValueAsString().split("-");
	            	if(tmp.length!=1){
			        	Date tmpDateTo = new Date(Integer.parseInt(tmp[2])-1900, Integer.parseInt(tmp[1])-1, Integer.parseInt(tmp[0]));
			            incomingDateTo = tmpDateTo;
	            	}
	            }
	          
	        }

	        dumper = new UniversalTableDumper();
	        XlsPoiDumper poiDumper = new XlsPoiDumper();
	        File xlsFile = new File(this.getDestination(), "raport.xls");
	        poiDumper.openFile(xlsFile);
	        FileOutputStream fis = new FileOutputStream(xlsFile);

	        doReportForAll().getWorkbook().write(fis);
	        fis.close();
	        dumper.addDumper(poiDumper);

	    }

	    private UTPDocReport doReportForAll() throws Exception
	    {
	        UTPDocReport xlsReport = new UTPDocReport("Raport spraw");
	        xlsReport.setIncomingDateFrom(incomingDateFrom);
	        xlsReport.setIncomingDateTo(incomingDateTo);
	        if(komorka != null && !komorka.equals(""))
	        	xlsReport.setKomorka(komorka);
	        if(symbol != null && !symbol.equals(""))
	        	xlsReport.setSymbol(symbol); 
	        if(haslo != null && !haslo.equals(""))
	        	xlsReport.setHaslo(haslo); 
	        xlsReport.generate();
	        return xlsReport;
	    }

	    private Map<String, String> pobierzWszytkieKomorkiOrganizacyjne()
	    {
	        Map<String, String> komorki = new TreeMap<String, String>();
	        try
	        {
	        	//////
	        	FromClause from = new FromClause();
	            TableAlias divisionTable = from.createTable("v_ds_division");
	            WhereClause where = new WhereClause();
	            OrderClause order = new OrderClause();
	            
	            SelectClause selectId = new SelectClause(true);
	            SelectColumn colId = selectId.add(divisionTable, "id");
	            SelectColumn colName = selectId.add(divisionTable, "NAME");
	            SelectColumn colCode = selectId.add(divisionTable, "CODE");
	         
	            
	            order.add(divisionTable.attribute("CODE"), OrderClause.ASC);
	            
	            SelectQuery selectQuery = new SelectQuery(selectId, from, where, order);
	            
				ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
				
				while (rs.next())
	            {
					komorki.put(rs.getString(colCode.getPosition()), rs.getString(colCode.getPosition()) + " - " + rs.getString(colName.getPosition()));
	            
	            }

	        }
	        catch ( Exception e )
	        {
	            log.debug(e.getMessage(), e);
	        }
	        return sortByValues(komorki);
	    }   
	    
	    private Map<String, String> pobierzWszytkieSymbole()
	    {
	        Map<String, String> symbole = new TreeMap<String, String>();
	        try
	        {
	        	//////
	        	FromClause from = new FromClause();
	            TableAlias rwaTable = from.createTable("v_dso_rwaa");
	            WhereClause where = new WhereClause();
	            OrderClause order = new OrderClause();
	            
	            SelectClause selectId = new SelectClause(true);
	            SelectColumn colId = selectId.add(rwaTable, "id");
	            SelectColumn colCode = selectId.add(rwaTable, "code");
	
	            
	            order.add(rwaTable.attribute("code"), OrderClause.ASC);
	            
	            SelectQuery selectQuery = new SelectQuery(selectId, from, where, order);
	            
				ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
				
				while (rs.next())
	            {
					symbole.put(rs.getString(colCode.getPosition()), rs.getString(colCode.getPosition()));
	            
	            }

	        }
	        catch ( Exception e )
	        {
	            log.debug(e.getMessage(), e);
	        }
	        return sortByValues(symbole);
	    }   
	    
	   
	    
	    private Map<String, String> pobierzWszytkieHasla()
	    {
	        Map<String, String> hasla = new TreeMap<String, String>();
	        try
	        {
	        	FromClause from = new FromClause();
	            TableAlias rwaTable = from.createTable("v_dso_rwaa");
	            WhereClause where = new WhereClause();
	            OrderClause order = new OrderClause();
	            
	            SelectClause selectId = new SelectClause(true);
	            SelectColumn colId = selectId.add(rwaTable, "id");
	            SelectColumn colDescription = selectId.add(rwaTable, "description");
	            
	            order.add(rwaTable.attribute("description"), OrderClause.ASC);
	            
	            SelectQuery selectQuery = new SelectQuery(selectId, from, where, order);
	            
				ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
				
				
				while (rs.next())
	            {
					hasla.put(rs.getString(colDescription.getPosition()), rs.getString(colDescription.getPosition()));
	            
	            }

	        }
	        catch ( Exception e )
	        {
	            log.debug(e.getMessage(), e);
	        }
	        return sortByValues(hasla);
	    }   
	    
	    
	    public static <K, V extends Comparable<V>> Map<K, V> sortByValues(final Map<K, V> map) {
		Comparator<K> valueComparator =  new Comparator<K>() {
		    public int compare(K k1, K k2) {
		        int compare = map.get(k2).compareTo(map.get(k1));
		        if (compare == 0) return 1;
		        else return -compare;
		    }
		};
		Map<K, V> sortedByValues = new TreeMap<K, V>(valueComparator);
		sortedByValues.putAll(map);
		return sortedByValues;
		}
}
