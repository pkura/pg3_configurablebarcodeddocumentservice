package pl.compan.docusafe.parametrization.utp;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils.CalcOperation;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.Field.Type;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.SessionUtils;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.enums.ExternalSourceEnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.exports.AttachmentInfo;
import pl.compan.docusafe.core.exports.BudgetItem;
import pl.compan.docusafe.core.exports.ExportedDocument;
import pl.compan.docusafe.core.exports.FundSource;
import pl.compan.docusafe.core.exports.ReservationDocument;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSDivisionFilter;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.general.hibernate.model.CostInvoiceProductDB;
import pl.compan.docusafe.general.hibernate.model.UnitOfMeasureDB;
import pl.compan.docusafe.general.mapers.MapperUtils;
import pl.compan.docusafe.parametrization.utp.entities.Acceptation;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.PurchasePlanKind;

import com.asprise.util.tiff.g;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.primitives.Ints;
public class ZamowieniePubliczneLogic extends AbstractDocumentLogic implements ZamowieniePubliczneConst {
    private static final Logger log = LoggerFactory.getLogger(ZamowieniePubliczneLogic.class);

    private static final long serialVersionUID = 1L;

    protected static final EnumValues EMPTY_ENUM_VALUE = new EnumValues(new HashMap<String, String>(), new ArrayList<String>());

	@Override
	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));
		
		for (PermissionBean perm : perms) {
			if (perm.isCreateGroup()
					&& perm.getSubjectType().equalsIgnoreCase(
							ObjectPermission.GROUP)) {
				String groupName = perm.getGroupName();
				if (groupName == null) {
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {
		as.useFolderResolver(document);
		as.useAvailableProviders(document);
		log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
	}
	
	@Override
	public Map<String, Object> setMultipledictionaryValue(
			String dictionaryName, Map<String, FieldData> values,
			Map<String, Object> fieldsValues, Long documentId) {
		Map<String, Object> results = Maps.newHashMap();
		ustawZawezanie(dictionaryName, values, fieldsValues, documentId, results);
		przeliczPozycjeBudzetu(dictionaryName, values, documentId, results);
		przeliczZrodla(dictionaryName, values, documentId, results);
		return results;
	}
	
	private void przeliczZrodla(String dictionaryName,
			Map<String, FieldData> values, Long documentId,
			Map<String, Object> results) {
		String prefix = dictionaryName + "_";
		if(dictionaryName.equalsIgnoreCase(ZRODLA_FINANSOWANIA_CN) && values.get(prefix+ZRODLA_FINANSOWANIA_POZYCJAID_CN) != null) {
			try {
				BigDecimal procentUdzial = values.get(prefix + ZRODLA_FINANSOWANIA_PROCENT_CN).getMoneyData();
//				FieldData bruttoPozycjiZDwrSession = ZamowieniePubliczneUtils.getBudgetPositionGrossFromDwrSession(values.get(prefix+ZRODLA_FINANSOWANIA_POZYCJAID_CN).getStringData());
                BigDecimal kwotaNetto = ZamowieniePubliczneUtils.pobierzKwoteNettoZSesji(values.get(prefix+ZRODLA_FINANSOWANIA_POZYCJAID_CN).getStringData());
				if(kwotaNetto != null){
//					BigDecimal bruttoUdzial = bruttoPozycjiZDwrSession.getMoneyData();
					results.put(prefix + ZRODLA_FINANSOWANIA_NETTO_CN, procentUdzial.divide(new BigDecimal(100), 2, RoundingMode.HALF_UP).multiply(kwotaNetto));
				} else {
					results.put(prefix + ZRODLA_FINANSOWANIA_PROCENT_CN, new BigDecimal(0));
					results.put(prefix + ZRODLA_FINANSOWANIA_NETTO_CN, new BigDecimal(0));
				}
			} catch (Exception e) {
				log.error("", e);
			}
		}
	}

	@Override
	public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException {
		if (documentId != null) {
			Object objValid = values.get("VALID");
			if(objValid != null && objValid instanceof Integer){
				if((Integer)objValid < 1)
					throw new EdmException("S這wnik '毒鏚豉 finansowania / udzia�' wymaga ponownego przeliczenia.");
			}
		}
	}
	
	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {		
		przeliczWniosek(values);
		ustawRodzajWniosku(values);
		sprawdzZrodlaFinUdzialy(values);
		
		FieldData rodzajZamowienia = values.get(DwrUtils.dwr(RODZAJ_ZAMOWIENIA_CN));
		FieldData robotaBudowlanaTele = (FieldData) ObjectUtils.defaultIfNull(values.get(DwrUtils.dwr(CZY_ROBOTA_BUDTELE_CN)), new FieldData(Type.BOOLEAN, false));
		FieldData zakupyCentralne = (FieldData) ObjectUtils.defaultIfNull(values.get(DwrUtils.dwr(CZY_ZAKUP_CENTRALNY_CN)), new FieldData(Type.BOOLEAN, false));
		FieldData zagrozeniePrzekroczenia = (FieldData) ObjectUtils.defaultIfNull(values.get(DwrUtils.dwr(CZY_ZAGROZENIE_PRZEKROCZENIA_PROGU_14TYS_CN)), new FieldData(Type.BOOLEAN, false));
		
		
		if (rodzajZamowienia != null && rodzajZamowienia.getEnumValuesData().getSelectedId().equalsIgnoreCase(RODZAJ_ZAMOWIENIA_ROBOTA_BUDOWLANA_ID)) {
			if (robotaBudowlanaTele.getBooleanData()) {
				zagrozeniePrzekroczenia.setBooleanData(false);
			} else {
				zakupyCentralne.setBooleanData(true);
			}
		} else {
			robotaBudowlanaTele.setBooleanData(false);
		}
				
		/*
		Boolean kwotaPowyzejZaproszenia = !dwrCheckIfAmountIsLessOrEqual(values, new BigDecimal(pobierzProgZaproszenia()));
		if (zakupyCentralne.getBooleanData() || kwotaPowyzejZaproszenia) {
			
		}*/
		
		return super.validateDwr(values, fm);
	}


	/**
	 * Sprawdza czy udzia� procentowy w s這wniku zrodla finansoawnia dla poszczeg鏊nych pozycji z slownika pozycje budzetowe jest rowny 100%.
	 * W przypadku gdy jedna nie spe軟ia tego warunku w ukrytym polu "VALID" na dockindzie jest ustawiana
	 * warto嗆 0, je郵i wszystko jest ok ustawiam 1. 
	 * @param values - Mapa DWR-owych warto軼i (klucze z przedrostkiem "DWR_")  
	 */
	private void sprawdzZrodlaFinUdzialy(Map<String, FieldData> values) {
		List<Integer> pozycjeBudzetoweValues = DwrUtils.getValueListFromDictionary(values, POZYCJE_BUDZETOWE_CN, true, POZYCJE_BUDZETOWE_POZYCJAID_CN);
		Boolean valid = false;

        for (Integer lp : pozycjeBudzetoweValues) {
            List<String> rows = ZamowieniePubliczneUtils.getFieldRowsNum(values, ZRODLA_FINANSOWANIA_CN, ZRODLA_FINANSOWANIA_POZYCJAID_CN, lp.toString());

            List<Object> list = Lists.newArrayList();
            FieldData dictionaryFieldData = values.get(DwrUtils.dwr(ZRODLA_FINANSOWANIA_CN));
            for (String rowNum : rows) {
                list.add(ZamowieniePubliczneUtils.extractRowFromDictionaryField(dictionaryFieldData, rowNum).get(ZRODLA_FINANSOWANIA_CN+"_"+ZRODLA_FINANSOWANIA_PROCENT_CN));
            }
            BigDecimal sumaUdzialow = DwrUtils.calcMoneyFieldValues(CalcOperation.ADD, 2, false, list.toArray(new FieldData[list.size()]));
            valid = (sumaUdzialow == null || sumaUdzialow.compareTo(new BigDecimal(100.00))==0);
        }

		values.put(DwrUtils.dwr("VALID"), new FieldData(Type.INTEGER,  (valid?1:0)));
	}
	
	private void ustawZawezanie(String dictionaryName,
			Map<String, FieldData> values, Map<String, Object> fieldsValues,
			Long documentId, Map<String, Object> results) {

		String prefix = dictionaryName + "_";
		String reservationTypeId = ObjectUtils.toString(fieldsValues.get(TYP_REZERWACJI_CN));
		if(dictionaryName.equalsIgnoreCase(POZYCJE_BUDZETOWE_CN)) {
			try {
				DwrUtils.setRefValueEnumOptions(values, results, POZYCJE_BUDZETOWE_OKRES_BUDZETOWY_CN, prefix,POZYCJE_BUDZETOWE_KOMORKA_BUDZETOWA_CN , WIDOK_BUDZET_KOM_TYLKO_KOM, null, EMPTY_ENUM_VALUE);	

				DwrUtils.setRefValueEnumOptions(values, results, POZYCJE_BUDZETOWE_KOMORKA_BUDZETOWA_CN, prefix, POZYCJE_BUDZETOWE_IDENTYFIKATOR_BUDZETU_CN, WIDOK_BUDZET_KOM_TYLKO_BUDZET, null, EMPTY_ENUM_VALUE);	

				DwrUtils.setRefValueEnumOptions(values, results, POZYCJE_BUDZETOWE_KOMORKA_BUDZETOWA_CN, prefix, POZYCJE_BUDZETOWE_IDENTYFIKATOR_PLANU_CN, WIDOK_ID_PLANU,null, EMPTY_ENUM_VALUE);

                DwrUtils.setRefValueEnumOptions(values, results, POZYCJE_BUDZETOWE_KOMORKA_BUDZETOWA_CN, prefix, POZYCJE_BUDZETOWE_DYSPONENT_CN, WIDOK_DYSPONENTOW, null, EMPTY_ENUM_VALUE);

                if(values.get(prefix+POZYCJE_BUDZETOWE_JM_CN)==null||values.get(prefix+POZYCJE_BUDZETOWE_JM_CN).getEnumValuesData()==null||values.get(prefix+POZYCJE_BUDZETOWE_JM_CN).getEnumValuesData().getSelectedId()==null||("").equals(values.get(prefix+POZYCJE_BUDZETOWE_JM_CN).getEnumValuesData().getSelectedId())){
					try{
						//try aby ewnetualny blad nie popsul calosci(najwyzej nie bedzie sugerowania, a tak by wywalilo calosc)
						DataBaseEnumField dbefJm=DataBaseEnumField.getEnumFiledForTable(WIDOK_JM);
						List<UnitOfMeasureDB> sztuka=UnitOfMeasureDB.findByIdn(TABELA_JM_SZTUKA_IDN);
						if(sztuka.size()>0){
							EnumItem ei=dbefJm.getEnumItem(sztuka.get(0).getId().toString());
							EnumValues ev=dbefJm.getEnumValuesForForcedPush(ei.getId().toString());
							results.put(prefix+POZYCJE_BUDZETOWE_JM_CN, ev);
						}
					} catch (EdmException e) {
						log.debug("",e);
					} catch (Exception e) {
						log.debug("",e);
					}
				}
				
				if(values.containsKey(prefix+POZYCJE_BUDZETOWE_PRODUKT_CN)&&values.get(prefix+POZYCJE_BUDZETOWE_PRODUKT_CN)!=null&&values.get(prefix+POZYCJE_BUDZETOWE_PRODUKT_CN).getData()!=null
						//	jesli uzytkownik zmieni jm, to i tak wroci do poprzedniej pozycji, bedzie zawsze ta sugerowana, najlepiej jakby nie bylo mozliwe do edycji
						// &&(values.containsKey(prefix+POZYCJE_BUDZETOWE_JM_CN))
						//	&&((values.get(prefix+POZYCJE_BUDZETOWE_JM_CN)==null)||(values.get(prefix+POZYCJE_BUDZETOWE_JM_CN).getEnumValuesData()==null)
						//			||(values.get(prefix+POZYCJE_BUDZETOWE_JM_CN).getEnumValuesData().getSelectedId()==null)||(values.get(prefix+POZYCJE_BUDZETOWE_JM_CN).getEnumValuesData().getSelectedId().equals("")))
						)
				{
					try{
						CostInvoiceProductDB prod=CostInvoiceProductDB.find(Long.parseLong(values.get(prefix+POZYCJE_BUDZETOWE_PRODUKT_CN).getEnumValuesData().getSelectedId()));
						DataBaseEnumField dbefJm=DataBaseEnumField.getEnumFiledForTable(WIDOK_JM);
						EnumItem ei=dbefJm.getEnumItemByCn(prod.getJmIdn());
						if(ei!=null){
							EnumValues ev=dbefJm.getEnumValuesForForcedPush(ei.getId().toString());
							results.put(prefix+POZYCJE_BUDZETOWE_JM_CN, ev);
						}
					}catch (EdmException e) {
						log.debug("",e);
					} 
					catch (Exception e) {
						log.debug("",e);
					}
				}
				
				//sugerowanie produktu
				if (TYP_REZERWACJI_WOZ_CN.equals(reservationTypeId)) {
					reloadRefEnumItems(values, results, dictionaryName, POZYCJE_BUDZETOWE_POZYCJA_PLANU_ZAKUPOW_CN, POZYCJE_BUDZETOWE_IDENTYFIKATOR_PLANU_CN, DocumentLogicLoader.ZAMOWIENIE, EMPTY_ENUM_VALUE, null);
//                    reloadRefEnumItems(values, results, dictionaryName, POZYCJE_BUDZETOWE_POZYCJA_BUDZETU_CN, POZYCJE_BUDZETOWE_IDENTYFIKATOR_BUDZETU_CN, DocumentLogicLoader.ZAMOWIENIE, EMPTY_ENUM_VALUE, null);
                    reloadRefEnumItems(values, results, ZRODLA_FINANSOWANIA_CN, ZRODLA_FINANSOWANIA_ZADANIE_FINANSOWE_PLANU_RAW_CN, POZYCJE_BUDZETOWE_KOMORKA_BUDZETOWA_CN, DocumentLogicLoader.ZAMOWIENIE, EMPTY_ENUM_VALUE, null);
                    if(values.get(prefix+POZYCJE_BUDZETOWE_POZYCJA_PLANU_ZAKUPOW_CN)!=null&&values.get(prefix+POZYCJE_BUDZETOWE_POZYCJA_PLANU_ZAKUPOW_CN).getData()!=null
							&&(values.get(prefix+POZYCJE_BUDZETOWE_PRODUKT_CN)==null||values.get(prefix+POZYCJE_BUDZETOWE_PRODUKT_CN).getData()==null)){
						EnumItem esei=ExternalSourceEnumItem.makeEnumItem(values.get(prefix+POZYCJE_BUDZETOWE_POZYCJA_PLANU_ZAKUPOW_CN).getEnumValuesData().getSelectedId());
						Long id=Long.parseLong(esei.getId().toString());
						DictionaryServiceFactory dictionaryServiceFactory = new DictionaryServiceFactory();
						PurchasePlanKind ppk=dictionaryServiceFactory.getPurchasePlanKindById(id);

						DataBaseEnumField dbefprod=DataBaseEnumField.getEnumFiledForTable(WIDOK_PRODUKTY);
						List<CostInvoiceProductDB> produkt = CostInvoiceProductDB.findByERPID(MapperUtils.getLongFromDouble(ppk.getWytwor_id()));
						if(produkt!=null&&produkt.size()>0){
							EnumValues ev=dbefprod.getEnumValuesForForcedPush(Long.toString(produkt.get(0).getId()));
							results.put(prefix+POZYCJE_BUDZETOWE_PRODUKT_CN, ev);
							//ustawienie jm
							DataBaseEnumField dbefJm=DataBaseEnumField.getEnumFiledForTable(WIDOK_JM);
							EnumItem ei=dbefJm.getEnumItemByCn(produkt.get(0).getJmIdn());
							if(ei!=null){
								EnumValues ev2=dbefJm.getEnumValuesForForcedPush(ei.getId().toString());
								results.put(prefix+POZYCJE_BUDZETOWE_JM_CN, ev2);
							}
						}

//                        //ustawienie powiazanej pozycji budzetu koszt闚
//                        DataBaseEnumField dbefprod=DataBaseEnumField.getEnumFiledForTable(WIDOK);

                    }
				}
				else if (TYP_REZERWACJI_WB_CN.equals(reservationTypeId)) {
					reloadRefEnumItems(values, results, dictionaryName, POZYCJE_BUDZETOWE_POZYCJA_BUDZETU_CN, POZYCJE_BUDZETOWE_IDENTYFIKATOR_BUDZETU_CN, DocumentLogicLoader.ZAMOWIENIE, EMPTY_ENUM_VALUE, null);
//					reloadRefEnumItems(values, results, ZRODLA_FINANSOWANIA_CN, ZRODLA_FINANSOWANIA_ZADANIE_FINANSOWE_BUDZETU_RAW_CN, POZYCJE_BUDZETOWE_KOMORKA_BUDZETOWA_CN, DocumentLogicLoader.ZAMOWIENIE, EMPTY_ENUM_VALUE, null);
					//TODO sugerowanie produktu, jesli jest WB
				}
				
				//Ustawienie wybranych pozycji plan/budzet
				try {
					Map<String, Object> sessionValues = SessionUtils.getFromSession(documentId, values.get(prefix + POZYCJE_BUDZETOWE_POZYCJAID_CN).getStringData(), new HashMap<String, Object>());
					sessionValues.put(POZYCJE_BUDZETOWE_POZYCJA_BUDZETU_CN, values.get(prefix+POZYCJE_BUDZETOWE_POZYCJA_BUDZETU_CN));
					sessionValues.put(POZYCJE_BUDZETOWE_POZYCJA_PLANU_ZAKUPOW_CN, values.get(prefix+POZYCJE_BUDZETOWE_POZYCJA_PLANU_ZAKUPOW_CN));
					SessionUtils.putToSession(documentId, values.get(prefix + POZYCJE_BUDZETOWE_POZYCJAID_CN).getStringData(), sessionValues);
				} catch (Exception e) {
					log.trace("Ustawianie wybranych pozycji plan/budzet", e.getMessage());
				}
			} catch (Exception e) {
				log.error("", e);
			}
		} else if (dictionaryName.equalsIgnoreCase(ZRODLA_FINANSOWANIA_CN)) {
			String lpPozycji = values.get(prefix+ZRODLA_FINANSOWANIA_POZYCJAID_CN).getStringData();
			if (StringUtils.isNotEmpty(lpPozycji)) {				
				Map<String, Object> selectedBudgetPositions = SessionUtils.getFromSession(documentId, lpPozycji, new HashMap<String, Object>());
				try {
					FieldData enumFieldData = null;
					String budgetPositionId = "";
					//plan
					if (reservationTypeId.equalsIgnoreCase(TYP_REZERWACJI_WOZ_CN)) {
						enumFieldData = (FieldData)selectedBudgetPositions.get(POZYCJE_BUDZETOWE_POZYCJA_PLANU_ZAKUPOW_CN);
						budgetPositionId = enumFieldData.getEnumValuesData().getSelectedId();
						reloadRefEnumItems(values, results, dictionaryName, ZRODLA_FINANSOWANIA_ZADANIE_FINANSOWE_PLANU_RAW_CN, POZYCJE_BUDZETOWE_POZYCJA_PLANU_ZAKUPOW_CN, DocumentLogicLoader.ZAMOWIENIE, EMPTY_ENUM_VALUE, budgetPositionId);		
					} else if (reservationTypeId.equalsIgnoreCase(TYP_REZERWACJI_WB_CN)) {
						enumFieldData = (FieldData)selectedBudgetPositions.get(POZYCJE_BUDZETOWE_POZYCJA_BUDZETU_CN);
						budgetPositionId = enumFieldData.getEnumValuesData().getSelectedId();
						reloadRefEnumItems(values, results, dictionaryName, ZRODLA_FINANSOWANIA_ZADANIE_FINANSOWE_BUDZETU_RAW_CN, POZYCJE_BUDZETOWE_POZYCJA_BUDZETU_CN, DocumentLogicLoader.ZAMOWIENIE, EMPTY_ENUM_VALUE, budgetPositionId);						
					}				
				} catch (Exception e) {
					log.trace("B陰d podczas zaw篹ania zrodla finansowania. ", e.getMessage());
				}
			}
		}
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type)
			throws EdmException {
		Map<String, Object> values = Maps.newHashMap();
		values.put(STATUS_CN, 0);
		values.put(DATA_UTWORZENIA_CN, Calendar.getInstance().getTime());
        DSUser author = DSApi.context().getDSUser();
        values.put(OS_REJESTRUJACA_CN, author.getId());
//        new BigDecimal(Docusafe.getAdditionProperty("kurs_euro"))
		values.put(KURS_CN, Docusafe.getAdditionProperty("kurs_euro"));
		fm.reloadValues(values);
		super.setInitialValues(fm, type);
	}

    @Override
    public ExportedDocument buildExportDocument(OfficeDocument doc) throws EdmException {
        FieldsManager fm = doc.getFieldsManager();

        ReservationDocument exported = new ReservationDocument();
        exported.setReservationStateFromId(Objects.firstNonNull(fm.getIntegerKey(ZamowieniePubliczneConst.ERP_STATUS_CN), 1));
        exported.setDescription(getAdditionFieldsDescription(doc,fm));
        exported.setCreated(doc.getCtime());         //datrez
        exported.setId(doc.getId().toString());
        exported.setReservationNumber(fm.getStringValue(ERP_DOCUMENT_IDM_CN)); //identyfikator    IDM
        exported.setBudgetReservationTypeId(fm.getLongKey(TYP_REZERWACJI_ERP_CN));
        exported.setReservationMethod(ReservationDocument.ReservationMethod.AUTO); //metoda_rezerwacji
        exported.getHeaderBudget().setBudgetDirectId(fm.getLongKey(ROK_BUDZETOWY_CN));

        String mpkDictionaryCn = POZYCJE_BUDZETOWE_CN;
        String sourceDictionaryCn = ZRODLA_FINANSOWANIA_CN;

        List<Map<String, Object>> positions = (List) fm.getValue(mpkDictionaryCn);
        String mpkDictionaryWithSuffix = mpkDictionaryCn + "_";
        String sourcesDictionaryWithSuffix = sourceDictionaryCn + "_";
        List<Map<String, Object>> finanancialSourcesDictionary = (List) fm.getValue(sourceDictionaryCn);

        if (positions != null) {
            int i = 1;
            List<BudgetItem> budgetItems = Lists.newArrayList();
            BudgetItem bItem;
            for (Map<String, Object> position : positions) {
                budgetItems.add(bItem = new BudgetItem());

                bItem.setItemNo(i++);
                bItem.setName(ObjectUtils.toString(position.get(mpkDictionaryWithSuffix + POZYCJE_BUDZETOWE_OPIS_CN), null));
                if (position.get(mpkDictionaryWithSuffix + POZYCJE_BUDZETOWE_JM_CN) != null &&
                        !Strings.isNullOrEmpty(((EnumValues)
                                position.get(mpkDictionaryWithSuffix + POZYCJE_BUDZETOWE_JM_CN)).getSelectedId())){
                    bItem.setUnit(Integer.valueOf(
                            ((EnumValues)position.get(mpkDictionaryWithSuffix + POZYCJE_BUDZETOWE_JM_CN)).getSelectedId()));
                }else{
                    bItem.setUnit("szt.");
                }
                bItem.setQuantity((BigDecimal) position.get(mpkDictionaryWithSuffix + POZYCJE_BUDZETOWE_ILOSC_CN));
                bItem.setNetAmount((BigDecimal) position.get(mpkDictionaryWithSuffix+POZYCJE_BUDZETOWE_NETTO_CN));

//                Number itemNo = (Number) position.get(mpkDictionaryWithSuffix + "POZYCJAID");

                if (position.get(mpkDictionaryWithSuffix + POZYCJE_BUDZETOWE_STAWKA_CN) != null && !Strings.isNullOrEmpty((
                        (EnumValues) position.get(mpkDictionaryWithSuffix + POZYCJE_BUDZETOWE_STAWKA_CN)).getSelectedId())) {
                    bItem.setVatRate(Integer.valueOf(
                            ((EnumValues)position.get(mpkDictionaryWithSuffix + POZYCJE_BUDZETOWE_STAWKA_CN)).getSelectedId()));
                }

                if (position.get(mpkDictionaryWithSuffix + POZYCJE_BUDZETOWE_PRODUKT_CN) != null && !Strings.isNullOrEmpty((
                        (EnumValues) position.get(mpkDictionaryWithSuffix + POZYCJE_BUDZETOWE_PRODUKT_CN)).getSelectedId())) {
                    EnumItem costKindEnum = DataBaseEnumField.getEnumItemForTable(WIDOK_PRODUKTY, Ints.tryParse((
                            (EnumValues) position.get(mpkDictionaryWithSuffix + POZYCJE_BUDZETOWE_PRODUKT_CN)).getSelectedId()));
                    bItem.setCostKindCode(costKindEnum.getCn());
                    exported.setCostKindCodeAllowed(true);
                } else {
                    bItem.setCostKindName(ObjectUtils.toString(position.get(mpkDictionaryWithSuffix + POZYCJE_BUDZETOWE_OPIS_CN), ""));
                }

                if (position.get(mpkDictionaryWithSuffix + POZYCJE_BUDZETOWE_KOMORKA_BUDZETOWA_CN) != null && !Strings.isNullOrEmpty((
                        (EnumValues) position.get(mpkDictionaryWithSuffix + POZYCJE_BUDZETOWE_KOMORKA_BUDZETOWA_CN)).getSelectedId())) {
                    bItem.setMpkId(Long.valueOf(
                            ((EnumValues) position.get(mpkDictionaryWithSuffix + POZYCJE_BUDZETOWE_KOMORKA_BUDZETOWA_CN)).getSelectedId()));
                }

                bItem.setBudgetDirectId(getEnumId(position.get(mpkDictionaryWithSuffix + POZYCJE_BUDZETOWE_OKRES_BUDZETOWY_CN)));

                if (TYP_REZERWACJI_WOZ_CN.equalsIgnoreCase(fm.getStringKey(TYP_REZERWACJI_CN))){
                    bItem.setPlanZamId(getEnumId(position.get(mpkDictionaryWithSuffix + POZYCJE_BUDZETOWE_IDENTYFIKATOR_PLANU_CN)));
                    bItem.setRodzajPlanZamId(getExternalEnumId(position.get(mpkDictionaryWithSuffix +
                            POZYCJE_BUDZETOWE_POZYCJA_PLANU_ZAKUPOW_CN)));
                    if (exported.getHeaderBudget().getPlanZamId() == null) {
                        exported.getHeaderBudget().setPlanZamId(bItem.getPlanZamId());
                    }
                    if (finanancialSourcesDictionary != null)
                        dodajZrodlaFinansowe(bItem, ((BigDecimal) position.get(mpkDictionaryWithSuffix +
                                POZYCJE_BUDZETOWE_POZYCJAID_CN)).intValue(), finanancialSourcesDictionary,
                                sourcesDictionaryWithSuffix,ZRODLA_FINANSOWANIA_ZADANIE_FINANSOWE_PLANU_RAW_CN);
                }

                if (TYP_REZERWACJI_WB_CN.equalsIgnoreCase(fm.getStringKey(TYP_REZERWACJI_CN))){
                    bItem.setBudgetKoId(getEnumId(position.get(mpkDictionaryWithSuffix + POZYCJE_BUDZETOWE_IDENTYFIKATOR_BUDZETU_CN)));
                    bItem.setBudgetKindId(getExternalEnumId(position.get(mpkDictionaryWithSuffix + POZYCJE_BUDZETOWE_POZYCJA_BUDZETU_CN)));
                    if (exported.getHeaderBudget().getBudgetKoId() == null) {
                        exported.getHeaderBudget().setBudgetKoId(bItem.getBudgetKoId());
                    }
                    if (finanancialSourcesDictionary != null)
                        dodajZrodlaFinansowe(bItem, ((BigDecimal) position.get(mpkDictionaryWithSuffix +
                                POZYCJE_BUDZETOWE_POZYCJAID_CN)).intValue(), finanancialSourcesDictionary,
                                sourcesDictionaryWithSuffix,ZRODLA_FINANSOWANIA_ZADANIE_FINANSOWE_BUDZETU_RAW_CN);
                }
            }
            exported.setPostionItems(budgetItems);
        }

        String systemPath = Docusafe.getBaseUrl();

        for (Attachment zal : doc.getAttachments()) {
            AttachmentInfo info = new AttachmentInfo();

            String path = systemPath + "/repository/view-attachment-revision.do?id=" + zal.getMostRecentRevision().getId();

            info.setName(zal.getTitle());
            info.setUrl(path);

            exported.addAttachementInfo(info);
        }

        return exported;
    }

    private void dodajZrodlaFinansowe(BudgetItem bItem, Integer lpPozycji, List<Map<String, Object>> finanancialSourcesDictionary, String sourcesDictionaryWithSuffix, String sourceFieldCn) {
        for (Map<String, Object> map : finanancialSourcesDictionary){
            if (map.get(sourcesDictionaryWithSuffix+ZRODLA_FINANSOWANIA_POZYCJAID_CN) != null &&
                ((BigDecimal) map.get(sourcesDictionaryWithSuffix+ZRODLA_FINANSOWANIA_POZYCJAID_CN)).intValue() == lpPozycji)
            {
                FundSource zrodlo = new FundSource();
                zrodlo.setAmount((BigDecimal) map.get(sourcesDictionaryWithSuffix+ZRODLA_FINANSOWANIA_NETTO_CN));
                zrodlo.setCode(getExternalEnumCn(map.get(sourcesDictionaryWithSuffix +
                        sourceFieldCn)));
                zrodlo.setFundPercent((BigDecimal) map.get(sourcesDictionaryWithSuffix+ZRODLA_FINANSOWANIA_PROCENT_CN));
                bItem.addFundSource(zrodlo);
            }
        }
    }

    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) throws EdmException {
        FieldsManager fm = doc.getFieldsManager();
        if (ACC_KIEROWNIK_JEDNOSTKI_CN.equals(acceptationCn)){
            DSDivision dzialWnioskujacy = DSDivision.findById(fm.getIntegerKey("OS_REJESTRUJACA_DZIAL"));
            String loginKierownika = findManagerOfDepartment(dzialWnioskujacy);
            if (loginKierownika == null)
                throw new EdmException("Nie znaleziono kierownika dzia逝 dla kom鏎ki "+dzialWnioskujacy.getName()
                + ". Skontaktuj si� z administratorem systemu.");

            assignable.addCandidateUser(loginKierownika);
            log.info("For accept " + acceptationCn + " and department " +dzialWnioskujacy.getName()
                    + "(guid:"+dzialWnioskujacy.getGuid()+
                    ") - candidateUser: " + loginKierownika);
            AssigneeHandler.addToHistory(openExecution, doc, loginKierownika, null);
            return true;
        }else if (ACC_DYSPONENCI_CN.equals(acceptationCn)){
//        	Acceptation acceptation = null;
//        	if(doc.getFieldsManager().getField(AKCEPTACJE_DYSPONENTOW_CN).isMultiple())
//        		acceptation = Acceptation.findFirstAcceptation(AKCEPTACJE_DYSPONENTOW_CN,
//        				doc.getDocumentKind().getMultipleTableName(), doc.getId());
//        	else
//        		acceptation = Acceptation.findAcceptation(doc.getFieldsManager().getLongKey(AKCEPTACJE_DYSPONENTOW_CN));
//
//            if (acceptation == null)
//                throw new EdmException("Dodaj dysponent闚 鈔odk闚 do akceptacji.");
//            DSUser dysponent = DSUser.findById(acceptation.getOsoba());
//            assignable.addCandidateUser(dysponent.getName());
//            log.info("For accept " + acceptationCn +
//                    ") - candidateUser: " + dysponent.getName());
//            AssigneeHandler.addToHistory(openExecution, doc, dysponent.getName(), null);
//            return true;
            List<Map> pozBudzetowe = (List<Map>) doc.getFieldsManager().getValue(POZYCJE_BUDZETOWE_CN);
            for (Map poz : pozBudzetowe){
                String selectedId = ((EnumValues) poz.get(POZYCJE_BUDZETOWE_CN+"_"+POZYCJE_BUDZETOWE_STATUS_AKCEPTACJI_CN)).getSelectedId();
                if (Strings.isNullOrEmpty(selectedId)){
                    String userId = ((EnumValues) poz.get(POZYCJE_BUDZETOWE_CN+"_"+POZYCJE_BUDZETOWE_DYSPONENT_CN)).getSelectedId();
                    DSUser dysponent = DSUser.findById(Long.valueOf(userId));
                    assignable.addCandidateUser(dysponent.getName());
                    log.info("For accept " + acceptationCn +
                            ") - candidateUser: " + dysponent.getName());
                    AssigneeHandler.addToHistory(openExecution, doc, dysponent.getName(), null);
                    return true;
                }
            }
        } else if (ACC_PELNOMOCNIK_DZP_CN.equals(acceptationCn)){
            
        	DSDivision grPelnomocnikDZP = DSDivision.find(ZamowieniePubliczneLogic.pobierzGuidPelnomocnikDZP());
    		DSDivision dzialOsobyRejestrujacej = DSDivision.find(Jbpm4Utils.getDocument(openExecution).getFieldsManager().getLongKey(OS_REJESTRUJACA_DZIAL_CN).intValue());
    		
    		List<String> pelnomocnicyNaWydziale = new ArrayList<String>();
    		
    		do{
    			for (DSUser pelnomocnikDZP : grPelnomocnikDZP.getUsers()) {
    				if(dzialOsobyRejestrujacej.isInAnyChildDivision(pelnomocnikDZP))
    					pelnomocnicyNaWydziale.add(pelnomocnikDZP.getName());
    			}
    			
    			dzialOsobyRejestrujacej = dzialOsobyRejestrujacej.getParent();
    			if(dzialOsobyRejestrujacej == null || dzialOsobyRejestrujacej.getGuid().equalsIgnoreCase(ZamowieniePubliczneLogic.pobierzGuidDzialGlowny())){
    				break;
    			}
    		} while(pelnomocnicyNaWydziale.isEmpty());
    		
    		System.out.println(pelnomocnicyNaWydziale);
        	
            if (pelnomocnicyNaWydziale.isEmpty())
                throw new EdmException("Nie znaleziono pe軟omocnika dzia逝 dla kom鏎ki " + dzialOsobyRejestrujacej.getName()
                + ". Skontaktuj si� z administratorem systemu.");

            for (String pelnomocnik : pelnomocnicyNaWydziale) {
				assignable.addCandidateUser(pelnomocnik);
			}
            
            log.info("For accept " + acceptationCn + " and department " +dzialOsobyRejestrujacej.getName()
                    + "(guid:"+dzialOsobyRejestrujacej.getGuid()+
                    ") - candidateUser: " + pelnomocnicyNaWydziale);
//            AssigneeHandler.addToHistory(openExecution, doc, pelnomocnicyNaWydziale, null);
            return true;
        } else if (ACC_PELNOMOCNIK_DFA_CN.equals(acceptationCn)) {
            
        	DSDivision grPelnomocnikDZP = DSDivision.find(ZamowieniePubliczneLogic.pobierzGuidPelnomocnikDFA());
    		DSDivision dzialOsobyRejestrujacej = DSDivision.find(Jbpm4Utils.getDocument(openExecution).getFieldsManager().getLongKey(OS_REJESTRUJACA_DZIAL_CN).intValue());
    		
    		List<String> pelnomocnicyNaWydziale = new ArrayList<String>();
    		
    		do{
    			for (DSUser pelnomocnikDZP : grPelnomocnikDZP.getUsers()) {
    				if(dzialOsobyRejestrujacej.isInAnyChildDivision(pelnomocnikDZP))
    					pelnomocnicyNaWydziale.add(pelnomocnikDZP.getName());
    			}
    			
    			dzialOsobyRejestrujacej = dzialOsobyRejestrujacej.getParent();
    			if(dzialOsobyRejestrujacej == null || dzialOsobyRejestrujacej.getGuid().equalsIgnoreCase(ZamowieniePubliczneLogic.pobierzGuidDzialGlowny())){
    				break;
    			}
    		} while(pelnomocnicyNaWydziale.isEmpty());
    		
    		System.out.println(pelnomocnicyNaWydziale);
        	
            if (pelnomocnicyNaWydziale.isEmpty())
                throw new EdmException("Nie znaleziono pe軟omocnika dzia逝 dla kom鏎ki " + dzialOsobyRejestrujacej.getName()
                + ". Skontaktuj si� z administratorem systemu.");

            for (String pelnomocnik : pelnomocnicyNaWydziale) {
				assignable.addCandidateUser(pelnomocnik);
			}
            
            log.info("For accept " + acceptationCn + " and department " +dzialOsobyRejestrujacej.getName()
                    + "(guid:"+dzialOsobyRejestrujacej.getGuid()+
                    ") - candidateUser: " + pelnomocnicyNaWydziale);
//            AssigneeHandler.addToHistory(openExecution, doc, pelnomocnicyNaWydziale, null);
            return true;
        }
        return false;
    }
    
    @Override
    public void setAdditionalTemplateValues(long docId, Map<String, Object> values) {
		try {
	    	Document doc = Document.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			
			Object objField = fm.getValue(DATA_UST_WART_ZAM_CN);
			if(objField!=null)
				values.put("DATAUSTWART", DateUtils.formatCommonDate((Date)objField));
			objField = fm.getValue(PODSTAWA_ZAM_CN);
			if(objField!=null)
				values.put("PODSTAWA", objField);
			
			String opis = "";
			if(fm.getLongKey(PODSTAWA_ZAM_CN) == PODSTAWA_ZAM_INNE_ID) {
				opis = StringUtils.trim(fm.getStringValue(OPIS_PODSTAWA_ZAM_INNE_CN));
				opis = "Opis: " + opis;
			}
			values.put("OPIS", opis);
			
			objField = fm.getValue(PODMIOTY_REALIZUJACE_CN);
			if(objField!=null) {
				List<Map<String, Object>> podmioty = Lists.newArrayList();
				@SuppressWarnings("unchecked")
				List<Map<String,Object>> list = (List<Map<String, Object>>) objField;
				for (int i = 0; i < list.size(); i++) {
					String prefix = PODMIOTY_REALIZUJACE_CN + "_";
					Map<String, Object> iteam = list.get(i);
					Map<String, Object> podmiotyRealizujace = Maps.newHashMap();
					podmiotyRealizujace.put("LP", i+1);
					podmiotyRealizujace.put("poz1", iteam.get(prefix+PODMIOTY_REALIZUJACE_NAZWA_I_ADRES_WYKONAWCY_CN));
					podmiotyRealizujace.put("poz2", iteam.get(prefix+PODMIOTY_REALIZUJACE_WYCENA_ZAM_BRUTTO_CN));
					podmiotyRealizujace.put("poz3", iteam.get(prefix+PODMIOTY_REALIZUJACE_UWAGI_CN));
					podmioty.add(podmiotyRealizujace);
				}
				values.put("podmioty", podmioty);
			}
			
			String rodzajZamowienia = "";
			try {
				rodzajZamowienia = RodzajZamowieniaRtf.valueOf(fm.getEnumItem(RODZAJ_ZAMOWIENIA_CN).getCn()).getRodzaj(1);				
			} catch (Exception e) {
				log.error("Brak okreslonego rodzaju zamowienia", e);
			}
			values.put("rodzajZamowienia", rodzajZamowienia);
			
			try {
				rodzajZamowienia = RodzajZamowieniaRtf.valueOf(fm.getEnumItem(RODZAJ_ZAMOWIENIA_CN).getCn()).getRodzaj(2);				
			} catch (Exception e) {
				log.error("Brak okreslonego rodzaju zamowienia", e);
			}
			values.put("rodzajZamowienia2", rodzajZamowienia);
			
			objField = fm.getValue(TERMIN_WYKONANIA_ZAM_CN);
			if(objField!=null)
				values.put("terminWykonaniaZamowienia", DateUtils.formatCommonDate((Date)objField));
					
			objField = fm.getValue(OKRES_GWARANCJI_REKOJMI_CN);
			if(objField!=null)
				values.put("okresGwarancji", objField);
			
			objField = fm.getValue(WARUNKI_PLATNOSCI_CN);
			if(objField!=null)
				values.put("warunkiPlatnosci", objField);
			
			objField = fm.getValue(TERMIN_ZLOZENIA_OFERTY_CN);
			if(objField!=null)
				values.put("terminZlozeniaOferty", DateUtils.formatJsDateTimeWithSeconds((Date)objField));
			
			objField = fm.getValue(WARUNKI_PLATNOSCI_CN);
			if(objField!=null)
				values.put("wMiejscu", objField);
			objField = fm.getValue(WARUNKI_PLATNOSCI_CN);
			
			if(objField!=null)
				values.put("zDopiskiem", objField);
			
			objField = fm.getValue(WARUNKI_PLATNOSCI_CN);
			if(objField!=null)
				values.put("faksemNa", objField);
			
			objField = fm.getValue(WARUNKI_PLATNOSCI_CN);
			if(objField!=null)
				values.put("mailemNa", objField);
			
			//Kryteria dezycji
			objField = fm.getValue(KRYTERIA_DECYZJI_CN);
			if(objField!=null){
				List<Map<String, Object>> podmioty = Lists.newArrayList();
				@SuppressWarnings("unchecked")
				List<Map<String,Object>> list = (List<Map<String, Object>>) objField;
				for (int i = 0; i < list.size(); i++) {
					String prefix = KRYTERIA_DECYZJI_CN + "_";
					Map<String, Object> iteam = list.get(i);
					Map<String, Object> podmiotyRealizujace = Maps.newHashMap();
					podmiotyRealizujace.put("LP", "-");
					podmiotyRealizujace.put("poz1", iteam.get(prefix+KRYTERIA_DECYZJI_KRYTERIUM_CN));
					podmiotyRealizujace.put("poz2", iteam.get(prefix+KRYTERIA_DECYZJI_WAGA_CN));
					podmioty.add(podmiotyRealizujace);
				}
				values.put("podmiot", podmioty);
			}
			
			//Zebrane oferty
			objField = fm.getValue(ZEBRANE_OFERTY_CN);
			if(objField!=null){
				List<Map<String, Object>> podmioty = Lists.newArrayList();
				@SuppressWarnings("unchecked")
				List<Map<String,Object>> list = (List<Map<String, Object>>) objField;
				for (int i = 0; i < list.size(); i++) {
					String prefix = ZEBRANE_OFERTY_CN + "_";
					Map<String, Object> iteam = list.get(i);
					Map<String, Object> podmiotyRealizujace = Maps.newHashMap();
					podmiotyRealizujace.put("LP", i+1);
					podmiotyRealizujace.put("poz1", iteam.get(prefix+ZEBRANE_OFERTY_NAZWA_WYKONAWCY_CN));
					podmiotyRealizujace.put("poz2", iteam.get(prefix+ZEBRANE_OFERTY_ADRES_CN));
					podmiotyRealizujace.put("poz3", iteam.get(prefix+ZEBRANE_OFERTY_NETTO_CN)); //TODO Na formularzu jest pole cena brutto
					podmiotyRealizujace.put("poz4", iteam.get(prefix+ZEBRANE_OFERTY_INNE_KRYTERIA_CN));
					podmiotyRealizujace.put("poz5", iteam.get(prefix+ZEBRANE_OFERTY_UWAGI_CN));
					podmioty.add(podmiotyRealizujace);
				}
				values.put("zebrane", podmioty);
			}
			
			//kody
			objField = fm.getValue(KODY_CPV_CN);
			if(objField!=null){
				List<Map<String, Object>> podmioty = Lists.newArrayList();
				@SuppressWarnings("unchecked")
				List<Map<String,Object>> list = (List<Map<String, Object>>) objField;
				for (int i = 0; i < list.size(); i++) {
					String prefix = KODY_CPV_CN + "_";
					Map<String, Object> iteam = list.get(i);
					Map<String, Object> podmiotyRealizujace = Maps.newHashMap();
					podmiotyRealizujace.put("poz1", iteam.get(prefix+KODY_CPV_KODENAME));
					podmiotyRealizujace.put("poz2", iteam.get(prefix+KODY_CPV_TITLE));
					podmioty.add(podmiotyRealizujace);
				}
				values.put("kody", podmioty);
			}
						
			objField = fm.getValue(UZASADNIENIE_WYBORU_CN);
			if(objField!=null)
				values.put("uzasadnienieWyboruOferty", objField);
			
			objField = fm.getValue(DATA_UTWORZENIA_CN);
			if(objField!=null)
				values.put("dataUtworzeniaWniosku", DateUtils.formatCommonDate((Date)objField));
			
			Long userID = fm.getLongKey(UPOWAZNIONY_PRACOWNIK_CN);
			if(userID != null) {
				DSUser dsUser = DSUser.findById(userID);
				String upowaznionyPracownik = StringUtils.trim(dsUser.getLastnameFirstnameWithOptionalIdentifier());
				if(!StringUtils.isEmpty(dsUser.getPhoneNum()))
					upowaznionyPracownik += ", tel: " + StringUtils.trim(dsUser.getPhoneNum());
				if(!StringUtils.isEmpty(dsUser.getEmail()))
					upowaznionyPracownik +=  ", " + StringUtils.trim(dsUser.getEmail());
				values.put("upowaznionyPracownik", upowaznionyPracownik);
			}
			
			objField = fm.getValue(UJETO_W_PLANIE_POZ_CN);
			if(objField!=null)
				values.put("ujetoWPlanie", objField);

			userID = fm.getLongKey(OSOBA_USTALAJACA_CN);
			objField = fm.getValue(DATA_USTALENIA_CN);
			if(objField!=null && userID != null){
				DSUser dsUser = DSUser.findById(userID);
				String dataIosobaUstalajacaZamowienie = DateUtils.formatCommonDate((Date)objField);
				dataIosobaUstalajacaZamowienie += ", " + StringUtils.trim(dsUser.getFirstname() + " " + dsUser.getLastname());
				values.put("dataIosobaUstalajacaZamowienie", dataIosobaUstalajacaZamowienie);
			}
			
			objField = fm.getValue(JEDNOSTKA_ZAMAWIAJACEGO_CN);
			if(objField!=null){
				values.put("komorkaZamawiajacego", objField);
			}
			
			userID = fm.getLongKey(OSOBA_SKLADAJACA_ZAM_CN);
			if(objField != null){
				DSUser dsUser = DSUser.findById(userID);
				String telOsSkladajacejZamowienie = StringUtils.trim(dsUser.getPhoneNum());
				values.put("telOsSkladajacejZamowienie", telOsSkladajacejZamowienie);
			}
			
			objField = fm.getValue(POZYCJE_BUDZETOWE_CN);
			if(objField!=null){
				List<Map<String, Object>> podmioty = Lists.newArrayList();
				@SuppressWarnings("unchecked")
				List<Map<String,Object>> list = (List<Map<String, Object>>) objField;
				for (int i = 0; i < list.size(); i++) {
					String prefix = POZYCJE_BUDZETOWE_CN + "_";
					Map<String, Object> iteam = list.get(i);
					Map<String, Object> podmiotyRealizujace = Maps.newHashMap();
					podmiotyRealizujace.put("poz1", iteam.get(prefix+POZYCJE_BUDZETOWE_OPIS_CN));
					podmioty.add(podmiotyRealizujace);
				}
				values.put("pozycjeBudzetowe", podmioty);
			}
			
		} catch (EdmException e) {
			log.error("", e);
		}catch (Exception e) {
			log.error("", e);			
		}
	}

    private String findManagerOfDepartment(DSDivision div) throws EdmException {
        DSDivision[] stanowiskaKierownika = div.getChildren(new DSDivisionFilter() {
            public boolean accept(DSDivision division) {
                return division.isPosition() && division.getName().startsWith("!Kierownik");
            }
        });

        if (stanowiskaKierownika.length>0){
            if(stanowiskaKierownika[0].getUsers().length>0)
                return stanowiskaKierownika[0].getUsers()[0].getName();
        }

        return null;
    }

    /**
     * metoda sprawdza czy kwota netto zamowienia w EURO z dokumentu
     * jest mniejsza b康� r闚na kwocie podanej w parametrze
     * @param docId
     * @param kwota
     * @return true je郵i kwota z pola dokumentu jest mniejsza badz r闚na, w przeciwnym razie false
     * @throws EdmException
     */
    public static Boolean checkIfAmountIsLessOrEqual(Long docId, BigDecimal kwota) throws EdmException {
        OfficeDocument doc = OfficeDocument.find(docId);
        FieldsManager fm = doc.getFieldsManager();
        BigDecimal kwotaNetto = (BigDecimal) fm.getKey(NETTO_EURO_CN);
        return kwotaNetto.compareTo(kwota) <=0 ? true : false;
    }
    
    public static Boolean dwrCheckIfAmountIsLessOrEqual(Map<String, FieldData> dwrValues, BigDecimal kwota) throws EdmException {
    	FieldData fd = dwrValues.get(DwrUtils.dwr(NETTO_EURO_CN));
    	
    	if(fd != null){
    		BigDecimal kwotaNetto = (BigDecimal) fd.getMoneyData();
    		return kwotaNetto.compareTo(kwota) <=0 ? true : false;
    	} else {
    		return false;
    	}
    }

    /**
     * Metoda sprawdza czy kwota netto (euro) z dokumentu jest w przedziale
     * podanym w parametrach kwotaOd, kwotaDo
     * @param docId
     * @param kwotaOd
     * @param kwotaDo
     * @return
     * @throws EdmException
     */
    public static Boolean checkIfAmountIsInRange(Long docId, BigDecimal kwotaOd, BigDecimal kwotaDo) throws EdmException {
    	boolean dolnyZakres = checkIfAmountIsLessOrEqual(docId, kwotaOd);
    	boolean gornyZakres = checkIfAmountIsLessOrEqual(docId, kwotaDo);
    	return (!dolnyZakres && gornyZakres);
    }
    
    public static Boolean checkIfRepairServices(Long docId) throws EdmException {
        OfficeDocument doc = OfficeDocument.find(docId);
        FieldsManager fm = doc.getFieldsManager();
        String rodzajZamowieniaCn = fm.getEnumItemCn(RODZAJ_ZAMOWIENIA_CN);
        return "ROBOTA_BUDOWLANA".equalsIgnoreCase(rodzajZamowieniaCn);
    }

    public static Boolean checkIfCentralPurchasing(Long docId) throws EdmException {
        OfficeDocument doc = OfficeDocument.find(docId);
        FieldsManager fm = doc.getFieldsManager();
        Boolean czyZakupCentralny = (Boolean) fm.getKey(CZY_ZAKUP_CENTRALNY_CN);
        return czyZakupCentralny != null ? czyZakupCentralny : false;
    }

    public static Boolean checkIfRiskOfExceeding(Long docId) throws EdmException {
        OfficeDocument doc = OfficeDocument.find(docId);
        FieldsManager fm = doc.getFieldsManager();
        Boolean czyZagrozenie = (Boolean) fm.getKey(CZY_ZAGROZENIE_PRZEKROCZENIA_PROGU_14TYS_CN);
        return czyZagrozenie != null ? czyZagrozenie : false;
    }

    public static Boolean checkIfPurchaseFrom6A(Long docId) throws EdmException {
        OfficeDocument doc = OfficeDocument.find(docId);
        FieldsManager fm = doc.getFieldsManager();
        Boolean czyArt6A = (Boolean) fm.getKey(CZY_ZAKUP_Z_ARTYUKULU_6A_CN);
        return czyArt6A != null ? czyArt6A : false;
    }

    public static boolean fillAcceptationTable(OfficeDocument doc,
                                               String acceptationType,
                                               DSUser user, Date data,
                                               int status,
                                               String uwagi,
                                               boolean update) throws EdmException, SQLException {
        if (!update){
            Acceptation acceptation = new Acceptation(user.getId(),
                data, status,uwagi);
            DSApi.context().session().save(acceptation);
            PreparedStatement ps = null;
            try {
                ps = DSApi.context().prepareStatement("INSERT INTO " +
                        doc.getDocumentKind().getMultipleTableName() +
                          "(DOCUMENT_ID, FIELD_CN ,FIELD_VAL) VALUES" +
                          "(?, ? , ?)");
                ps.setLong(1,doc.getId());
                ps.setString(2, acceptationType);//"AKCEPTACJE");
                ps.setString(3, acceptation.getId().toString());
                return ps.executeUpdate() != 0 ? true : false;
            } catch (SQLException e) {
                log.error("B陰d podczas zapisywania do bazy akceptacji: "+e);
            } finally {
                if (ps != null) {
                    ps.close();
                }
            }
        }else{
        	Acceptation acceptation = null;
        	if(!doc.getFieldsManager().getField(acceptationType).isMultiple()) {
        		Long acceptationId = doc.getFieldsManager().getLongKey(acceptationType);
        		acceptation = Acceptation.findAcceptation(acceptationId);
        		acceptation.setData(new Date());
        		acceptation.setStatus(status);
        		acceptation.setUwagi(uwagi);
        		DSApi.context().session().save(acceptation);
                DSApi.context().session().flush();
        	} else {
	            acceptation = Acceptation.findFirstAcceptationForDSUser(acceptationType,
	                    doc.getDocumentKind().getMultipleTableName(),doc.getId(), user);
	            acceptation.setData(new Date());
	            acceptation.setStatus(status);
	            acceptation.setUwagi(uwagi);
	            DSApi.context().session().save(acceptation);
	            DSApi.context().session().flush();
	            return true;
        	}
        }
        return false;
    }
    
    public void wyczyscPoleDataBase(Map<String, Object> results,String fieldWithPrefix){
		HashMap< String, String> mapa=new HashMap<String, String>();
		mapa.put("", "--wybierz--");
		List<Map<String,String>>itemy = new ArrayList<Map<String,String>>();
		itemy.add(mapa);
		EnumValues evDoWst=new EnumValues();
		evDoWst.setAllOptions(itemy);
		evDoWst.setSelectedId("");
		results.put(fieldWithPrefix,evDoWst);
    }
    
    void reloadRefEnumItems(Map<String, ?> values, Map<String, Object> results, String dictionaryCn, final String fieldCn, final String refFieldCn, String dockindCn, EnumValues emptyHiddenValue, String refFieldSelectedId) {
        try {
            Optional<pl.compan.docusafe.core.dockinds.field.Field> resourceField = findField(dockindCn, dictionaryCn, fieldCn + "_1");

            if (resourceField.isPresent()) {
                String dicWithSuffix = dictionaryCn + "_";
                if (refFieldSelectedId == null) {
                    refFieldSelectedId = getFieldSelectedId(values, results, dicWithSuffix, refFieldCn);
                }

                String fieldSelectedId = "";
                EnumValues enumValue = getEnumValues(values, dicWithSuffix + fieldCn);
                if (enumValue != null) {
                    if (!enumValue.getSelectedOptions().isEmpty()) {
                        fieldSelectedId = enumValue.getSelectedOptions().get(0);
                    }
                }

                EnumValues enumValues = resourceField.get().getEnumItemsForDwr(ImmutableMap.<String, Object>of(refFieldCn + "_1", ObjectUtils.toString(refFieldSelectedId), fieldCn + "_1", ObjectUtils.toString(fieldSelectedId)));

                if (isEmptyEnumValues(enumValues)) {
                    results.put(dicWithSuffix + fieldCn, emptyHiddenValue);
                } else {
                    results.put(dicWithSuffix + fieldCn, enumValues);
                }
            } else {
                throw new EdmException("cant find " + fieldCn + " field");
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }

    static boolean isEmptyEnumValues(EnumValues enumValues) {
        return enumValues.getAllOptions().isEmpty() || (enumValues.getAllOptions().size() == 1 && enumValues.getAllOptions().get(0).containsKey(""));
    }

    static String getFieldSelectedId(Map<String, ?> values, Map<String, Object> results, String dic, String fieldCn) {
        String fieldSelectedId = "";
        if (results.containsKey(dic + fieldCn)) {
            try {
                fieldSelectedId = ((EnumValues)results.get(dic + fieldCn)).getSelectedId();
            } catch (Exception e) {
                log.error("CAUGHT "+e.getMessage(),e);
            }
        } else {
            try {
                EnumValues enumValue = getEnumValues(values, dic + fieldCn);
                if (enumValue != null) {
                    fieldSelectedId = enumValue.getSelectedOptions().get(0);
                }
            } catch (Exception e) {
                log.error("CAUGHT " + e.getMessage(), e);
            }
        }

        return fieldSelectedId;
    }
 
    public static EnumValues getEnumValues(Map<String, ?> values, String fieldName) {
        Object value = values.get(fieldName);
        if (value instanceof FieldData) {
            return ((FieldData) value).getEnumValuesData();
        } else if (value instanceof EnumValues) {
            return (EnumValues) value;
        }
        return null;
    }
    
    static Optional<pl.compan.docusafe.core.dockinds.field.Field> findField(String dockindCn, String dictionaryCn, final String dictionaryFieldCn) throws EdmException {
        return Iterables.tryFind(DocumentKind.findByCn(dockindCn).getFieldsMap().get(dictionaryCn).getFields(), new Predicate<pl.compan.docusafe.core.dockinds.field.Field>() {
            @Override
            public boolean apply(pl.compan.docusafe.core.dockinds.field.Field input) {
                return dictionaryFieldCn.equals(input.getCn());
            }
        });
    }
    
    public String getCnValue(String tableName, Object enumValues) throws EdmException {
        if(enumValues == null)
            return null;
        String enumId = ((EnumValues)enumValues).getSelectedId();
        if (enumId != null) {
            return DataBaseEnumField.getEnumItemForTable(tableName, Integer.valueOf(enumId)).getCn();
        }
        return null;
    }
	private Long getEnumId(Object field) {
		return field != null && !Strings.isNullOrEmpty(((EnumValues) field).getSelectedId()) ? Long.valueOf(((EnumValues) field).getSelectedId()) : null;
    }

    private Long getExternalEnumId(Object field) {
        if (field != null && !Strings.isNullOrEmpty(((EnumValues) field).getSelectedId())) {
            return ExternalSourceEnumItem.makeEnumItem(((EnumValues) field).getSelectedId()).getId().longValue();
        }

        return null;
    }

    private String getExternalEnumCn(Object field) {
        if (field != null && !Strings.isNullOrEmpty(((EnumValues) field).getSelectedId())) {
            return ExternalSourceEnumItem.makeEnumItem(((EnumValues) field).getSelectedId()).getCn();
        }

        return null;
    }

    public static boolean czyIstniejeNastepnaAkceptacjaDysponenta(OfficeDocument doc) throws EdmException {
    	
//    	if(!doc.getFieldsManager().getField(AKCEPTACJE_DYSPONENTOW_CN).isMultiple())
//    		return false;
    		
        int ret = Acceptation.countUnacceptedAcceptation(POZYCJE_BUDZETOWE_CN,
                doc.getDocumentKind().getMultipleTableName(),
                "DS_UTP_ZAMPUB_POZ_BUD",
                doc.getId());

        if (ret == -1)
            throw new EdmException("B陰d podczas sprawdzania czy istniej� kolejni dysponenci akceptuj帷y");

        return ret > 0 ? true : false;
    }
    
    /**
     * Wylicza warto嗆 brutto dla jednego wiersza z s這wnika pozycje budzetowe.
     * @param dictionaryName
     * @param values
     * @param documentId
     * @param results
     */
    private void przeliczPozycjeBudzetu(String dictionaryName, Map<String, FieldData> values, Long documentId, Map<String, Object> results) {
		if (dictionaryName.equalsIgnoreCase(POZYCJE_BUDZETOWE_CN)) {
			String prefix = dictionaryName + "_";
			FieldData ilosc = values.get(prefix + POZYCJE_BUDZETOWE_ILOSC_CN);
			FieldData netto = values.get(prefix + POZYCJE_BUDZETOWE_NETTO_CN);
			FieldData stawka = values.get(prefix + POZYCJE_BUDZETOWE_STAWKA_CN);

			if (netto.getData() != null && stawka.getData() != null
					&& ilosc.getData() != null) {
				EnumValues ev = stawka.getEnumValuesData();
				if (ev.getSelectedId() != null
						&& !ev.getSelectedId().equals("")) {
					try {
						DataBaseEnumField dbef = DataBaseEnumField.getEnumFiledForTable(WIDOK_STAWKA_VAT);
						EnumItem e = dbef.getEnumItem(Integer.parseInt(ev.getSelectedId()));
						BigDecimal stawkaProcentowa = new BigDecimal(e.getCn());
						stawkaProcentowa = stawkaProcentowa.multiply(new BigDecimal(0.01));
						stawkaProcentowa = stawkaProcentowa.multiply(netto.getMoneyData());
						stawkaProcentowa = stawkaProcentowa.setScale(2, RoundingMode.HALF_UP);
						BigDecimal wartoscPozycji = stawkaProcentowa.add(netto.getMoneyData());
						wartoscPozycji = wartoscPozycji.multiply(new BigDecimal(ilosc.getLongData()));
						results.put(prefix + POZYCJE_BUDZETOWE_BRUTTO_CN, wartoscPozycji);
						
//						Map<String, Object> sessionValues = SessionUtils.getFromSession(documentId, values.get(prefix+POZYCJE_BUDZETOWE_POZYCJAID_CN).getStringData(), new HashMap<String, Object>());
//						sessionValues.put(POZYCJE_BUDZETOWE_BRUTTO_CN, wartoscPozycji);
//						SessionUtils.putToSession(documentId, values.get(prefix+POZYCJE_BUDZETOWE_POZYCJAID_CN).getStringData(), sessionValues);
					} catch (Exception ex) {
						log.error("", ex);
					}
				}
			}
		}
	}
	
	private boolean przeliczWniosek(Map<String, FieldData> values) {
		boolean result = true;
		try {
			BigDecimal sumaNetto = new BigDecimal(0.00);
			BigDecimal sumaBrutto = new BigDecimal(0.00);
			
			FieldData slownikPozycji = values.get(DwrUtils.dwr(POZYCJE_BUDZETOWE_CN));
			Map<String, FieldData> iloscMap = DwrUtils.getDictionaryFields(slownikPozycji, POZYCJE_BUDZETOWE_CN, POZYCJE_BUDZETOWE_ILOSC_CN);
			Map<String, FieldData> nettoMap = DwrUtils.getDictionaryFields(slownikPozycji, POZYCJE_BUDZETOWE_CN, POZYCJE_BUDZETOWE_NETTO_CN);
			
			String dictionaryNamePrefix = POZYCJE_BUDZETOWE_CN + "_"; 
			for (int i = 1; i <= iloscMap.size(); i++) {
				BigDecimal ilosc = new BigDecimal(iloscMap.get(dictionaryNamePrefix + POZYCJE_BUDZETOWE_ILOSC_CN+"_"+i).getIntegerData());
				BigDecimal cenaNetto = nettoMap.get(dictionaryNamePrefix + POZYCJE_BUDZETOWE_NETTO_CN+"_"+i).getMoneyData();
				BigDecimal stawkaVat = new BigDecimal(DwrUtils.getSelectedEnumCn(slownikPozycji.getDictionaryData(), dictionaryNamePrefix+POZYCJE_BUDZETOWE_STAWKA_CN+"_"+i, WIDOK_STAWKA_VAT));
				
				sumaNetto = sumaNetto.add(ilosc.multiply(cenaNetto));
				sumaBrutto = sumaBrutto.add(sumaNetto.add(ilosc.multiply(cenaNetto.multiply(stawkaVat.multiply(new BigDecimal(0.01))))));
			}
			
			values.put(DwrUtils.dwr(NETTO_CN), new FieldData(Type.MONEY, sumaNetto));
			values.put(DwrUtils.dwr(NETTO_EURO_CN), new FieldData(Type.MONEY, sumaNetto.divide(values.get(DwrUtils.dwr(KURS_CN)).getMoneyData(),4,RoundingMode.HALF_UP)));
			values.put(DwrUtils.dwr(BRUTTO_CN), new FieldData(Type.MONEY, sumaBrutto));
		} catch (Exception edm) {
			log.debug("Blad przeliczania slownika pozycji budzetu", edm);
			result = false;
		}
		return result;
	}

    public static boolean sprawdzCzyInneWylaczeniaUstawowe(Long docId) throws EdmException {
        OfficeDocument doc = OfficeDocument.find(docId);
        FieldsManager fm = doc.getFieldsManager();
        Boolean czyInneWylaczenia = (Boolean) fm.getKey(CZY_INNE_WYLACZENIA_USTAWOWE_CN);
        return czyInneWylaczenia != null ? czyInneWylaczenia : false;
    }

    public static boolean fillAcceptationStatusForBudgetPositions(OfficeDocument doc,
                                                                  DSUser user,
                                                                  Date dataAkceptacji,
                                                                  String dictionaryCn,
                                                                  String multiDictCn,
                                                                  String statusCn,
                                                                  String dateCn,
                                                                  int acceptationStatus) throws EdmException, SQLException {
        FieldsManager fm = doc.getFieldsManager();
        PreparedStatement ps = null;

        String acceptationColumn = "";
        String statusColumn = "";
        String dateColumn = "";
        String dictTable = "DS_UTP_ZAMPUB_POZ_BUD";
        String suffix = "_1";

        if(doc.getDocumentKind().getFieldByCn(multiDictCn) instanceof  DictionaryField)
            dictTable = ((DictionaryField) doc.getDocumentKind().getFieldByCn(multiDictCn)).getTable();

        List<pl.compan.docusafe.core.dockinds.field.Field> fields = doc.getFieldsManager().getField(multiDictCn).getFields();
        for (pl.compan.docusafe.core.dockinds.field.Field f : fields){
            if (f.getCn().equalsIgnoreCase(dictionaryCn+suffix))
                acceptationColumn = f.getColumn().substring(0,f.getColumn().length()-2);
            if (f.getCn().equalsIgnoreCase(statusCn+suffix))
                statusColumn = f.getColumn().substring(0,f.getColumn().length()-2);
            if (f.getCn().equalsIgnoreCase(dateCn+suffix))
                dateColumn = f.getColumn().substring(0,f.getColumn().length()-2);
        }

        try {
            ps = DSApi.context().prepareStatement(
                    "UPDATE a " +
                    " SET " +
                          dateColumn + " = ?, "+
                          statusColumn + " = ? "+
                    " FROM "+ dictTable + " as a " +
                    " INNER JOIN " + doc.getDocumentKind().getMultipleTableName()+
                    " as multi ON multi.FIELD_VAL=a.ID" +
                    " WHERE multi.document_id =  ? " +
                    " and multi.FIELD_CN like ? " +
                    " and a."+acceptationColumn+"= ?");
            ps.setDate(1,new java.sql.Date(dataAkceptacji.getTime()));
            ps.setInt(2, acceptationStatus);
            ps.setLong(3, doc.getId());
            ps.setString(4, multiDictCn);
            ps.setLong(5, user.getId());

            return ps.executeUpdate() != 0 ? true : false;
        } catch (SQLException e) {
            log.error("B陰d podczas zapisywania do bazy akceptacji: "+e);
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
        return false;
    }

    private enum RodzajZamowieniaRtf{
		DOSTAWA("dostaw�.#dostaw�"),
		USLUGA("us逝g�.#us逝g�"),
		ROBOTA_BUDOWLANA("robot� budowlan�.#robot� budowlan�");
		
		int iloscElementow = 2; 
		private final String[] rodzaj;	
		
		RodzajZamowieniaRtf(String rodzaj){
			 this.rodzaj=rodzaj.split("#");
		 }
		public String getRodzaj(int element){
			if (element > 0 && element <= iloscElementow)
				return rodzaj[element-1];
			return "";
		}
	}
	
	private void ustawRodzajWniosku(Map<String, FieldData> values) {
		try {
			FieldData fd = values.get(DwrUtils.dwr(NETTO_EURO_CN));
			EnumValues ev = values.get(DwrUtils.dwr(RODZAJ_WNIOSKU_CN)).getEnumValuesData();
            Integer progZaproszenia = pobierzProgZaproszenia();
            Integer progPrzetargu = pobierzProgPrzetargu();
			if(fd.getData() != null) {
				if(fd.getMoneyData().intValue() < progZaproszenia) {
					ev.setSelectedId("1");
					values.put(DwrUtils.dwr(RODZAJ_WNIOSKU_CN), new FieldData(Type.ENUM, ev));
				} if(fd.getMoneyData().intValue() >= progZaproszenia && fd.getMoneyData().intValue() < progPrzetargu) {
					ev.setSelectedId("2");
					values.put(DwrUtils.dwr(RODZAJ_WNIOSKU_CN), new FieldData(Type.ENUM, ev));
				} else if (fd.getMoneyData().intValue() > progPrzetargu) {
					ev.setSelectedId("3");
					values.put(DwrUtils.dwr(RODZAJ_WNIOSKU_CN), new FieldData(Type.ENUM, ev));
				}
			}
		} catch (Exception e) {
			log.error("", e);
		}
	}

    public static Integer pobierzProgZaproszenia(){
        String additionProperty = Docusafe.getAdditionProperty("utp.zamowienie.progZaproszenia");
        return Integer.valueOf(Objects.firstNonNull(additionProperty,"6000"));
    }

    public static Integer pobierzProgPrzetargu(){
        String additionProperty = Docusafe.getAdditionProperty("utp.zamowienie.progPrzetargu");
        return Integer.valueOf(Objects.firstNonNull(additionProperty,"14000"));
    }

	public static boolean checkIfDepartmentOfICT(Long docId) throws EdmException {
		OfficeDocument doc = OfficeDocument.find(docId);
        FieldsManager fm = doc.getFieldsManager();
        Boolean czyDzialTeleinformatyki = (Boolean) fm.getKey(CZY_ROBOTA_BUDTELE_CN);
        return czyDzialTeleinformatyki != null ? czyDzialTeleinformatyki : false;
	}

    public static Integer getErpStatusId(Long docId) throws EdmException {
        OfficeDocument doc = OfficeDocument.find(docId);
        FieldsManager fm = doc.getFieldsManager();
        Integer erpStatusId = fm.getIntegerKey(ZamowieniePubliczneConst.ERP_STATUS_CN);
        return erpStatusId;
    }

    private String getAdditionFieldsDescription(OfficeDocument doc, FieldsManager fm) throws EdmException {
        Map<String, String> additionFields = Maps.newLinkedHashMap();
        additionFields.put("Opis", doc.getDescription() != null ? doc.getDescription() : "brak");
        additionFields.put("Nr KO", doc.getOfficeNumber() != null ? doc.getOfficeNumber().toString() : "brak");
        additionFields.put("Numer wniosku", fm.getStringValue("NR_WNIOSKU"));

        StringBuilder description = new StringBuilder();
        for (String label : additionFields.keySet()) {
            if (!Strings.isNullOrEmpty(additionFields.get(label))) {
                description.append(label)
                        .append(": ")
                        .append(additionFields.get(label))
                        .append("; ");
            }
        }
        return description.toString();
    }
    @Override
    public String getTemplateFileName(long id, String templateName) {
    	try {
    		String filename = templateName.toLowerCase();
    		
    		if (filename.endsWith(".rtf")) {
				filename = filename.replaceAll(".rtf", "");
			}
    		
        	FieldsManager fm = Document.find(id).getFieldsManager();
        	filename += "_" + fm.getStringValue(ERP_DOCUMENT_IDM_CN).replaceAll(" ", "_");
        	return filename;
		} catch (Exception e) {
			log.error("Blad podczas generowania nazwy pliku.", e);
			return templateName;
		}
    }

	public static Integer pobierzProgKanclerza() {
		String additionProperty = Docusafe.getAdditionProperty("utp.zamowienie.progKanclerza");
		return Integer.valueOf(Objects.firstNonNull(additionProperty, "6000"));
	}
	
	public static String pobierzGuidPelnomocnikDZP() {
		String guidDZP = Docusafe.getAdditionProperty("utp.guid.SpecjalistaDsZamowienPublicznych");
		return guidDZP;
	}

	public static String pobierzGuidPelnomocnikDFA() {
		String guidDZP = Docusafe.getAdditionProperty("utp.guid.SpecjalistaDsFinansowIAdministracji");
		return guidDZP;
	}
	
	public static String pobierzGuidDzialGlowny() {
		String guidDzialGlowny = Docusafe.getAdditionProperty("utp.rootDivision"); 
		if (guidDzialGlowny != null) {
			return guidDzialGlowny;
		} else {
			return DSDivision.ROOT_GUID;
		}
	}
}
