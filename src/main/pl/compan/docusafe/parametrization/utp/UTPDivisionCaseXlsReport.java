package pl.compan.docusafe.parametrization.utp;


import java.math.BigDecimal;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.DateUtils;

public class UTPDivisionCaseXlsReport extends AbstractXlsReports {
	
	private String komorka;
	private Date dataOd;// data otwarcia sprawy od
	private Date dataDo;// data zamkniecia sprawy do
	private String kategoria;// kategoria rwa
	public UTPDivisionCaseXlsReport(String reportName) {
		super(reportName);
		title = new String[] { "Raport zestawienia czasu przetwarzania sprawy w poszczególnych komórkach organizacyjnych." };

		columns = new String[] { "Lp.", "Komórka organizacyjna", 
				"sredni czas rozpatrywania"};
	}

	@Override
	protected void createEntries() {
		
		StringBuilder sql=new StringBuilder("select d.name,s.iloscDniPrzetwarzania,s.iloscSpraw  from DS_DIVISION d "
				+ "left join(select d.name,d.CODE,SUM(datediff(d,c.OPENDATE,c.FINISHDATE)) as iloscDniPrzetwarzania,count(datediff(d,c.OPENDATE,c.FINISHDATE)) as iloscSpraw from dso_container c "
				+ "left join DS_DIVISION d on c.OFFICEIDPREFIX=d.code "
				+ "where d.code is not null and c.CLOSED=1 ");
		
		if(kategoria!=null&&!kategoria.equals(""))
			sql.append(" and c.RWA_ID="+kategoria);
		if( dataOd!=null&&!dataOd.equals(""))
			sql.append(" and c.opendate>='" + DateUtils.formatSqlDate(dataOd)+"' ");
		if( dataDo!=null&&!dataDo.equals(""))
			sql.append(" and c.finishdate<='" + DateUtils.formatSqlDate(dataDo)+"' ");
		
		sql.append("group by d.name,d.code) as s on d.CODE=s.code where d.code is not null");
		if(komorka!=null&&!komorka.equals(""))
			sql.append(" and d.code='"+komorka+"'");
		int rowCount = 1;
		int i = -1;
		
		HSSFRow row = sheet.createRow(rowCount);

		if( kategoria!=null&&!kategoria.equals(""))
		{
			PreparedStatement ps;
			try {
				ps = DSApi.context().prepareStatement(" select description from v_dso_rwaa where id="+kategoria);
				ResultSet rs=ps.executeQuery();
				rs.next();
				row.createCell(++i).setCellValue(new HSSFRichTextString("Kategoria: "+rs.getString("description")));
			} catch (SQLException e) {
				Logger.getLogger(UTPDivisionCaseXlsReport.class.getName())
				.log(Level.SEVERE, null, e);
				e.printStackTrace();
			} catch (EdmException e) {
				Logger.getLogger(UTPDivisionCaseXlsReport.class.getName())
				.log(Level.SEVERE, null, e);
				e.printStackTrace();
			}
		}
		else   row.createCell(++i).setCellValue(new HSSFRichTextString("Kategoria: brak "));
		
		if( dataOd!=null&&!dataOd.equals(""))row.createCell(++i).setCellValue(new HSSFRichTextString("Od: "+dataOd));
		else   row.createCell(++i).setCellValue(new HSSFRichTextString("Data od: brak "));
			
		if( dataDo!=null&&!dataDo.equals(""))row.createCell(++i).setCellValue(new HSSFRichTextString("Do: "+dataDo));
		else   row.createCell(++i).setCellValue(new HSSFRichTextString("Data do: brak  "));

		rowCount=3;
		try {
			
			PreparedStatement ps=DSApi.context().prepareStatement(sql.toString());
			ResultSet rs=ps.executeQuery();
			int lp=1;
			while(rs.next()){
				
				String name = rs.getString("name");
				String iloscDniPrzetwarzania = rs.getString("iloscDniPrzetwarzania");
				String iloscSpraw = rs.getString("iloscSpraw");
				
				i = -1;
				row = sheet.createRow(rowCount++);
				row.createCell(++i).setCellValue(
						new HSSFRichTextString(lp++ + ""));

				if (name != null)
					row.createCell(++i).setCellValue(
							new HSSFRichTextString(name));
				else
					row.createCell(++i).setCellValue(
							new HSSFRichTextString("-"));
				if(iloscDniPrzetwarzania!=null &&iloscSpraw!=null){
					Double tmp=Double.parseDouble(iloscDniPrzetwarzania)/Double.parseDouble(iloscSpraw);
					BigDecimal bd=new BigDecimal(tmp);
					bd=bd.setScale(1, BigDecimal.ROUND_HALF_UP);
					row.createCell(++i).setCellValue(
							
							new HSSFRichTextString(bd.toString()));
				}
				else
					row.createCell(++i).setCellValue(
							new HSSFRichTextString("-"));
				
				
			}
			
		} catch (SQLException e) {
			Logger.getLogger(UTPDivisionCaseXlsReport.class.getName())
			.log(Level.SEVERE, null, e);
			e.printStackTrace();
		} catch (EdmException e) {
			Logger.getLogger(UTPDivisionCaseXlsReport.class.getName())
			.log(Level.SEVERE, null, e);
			e.printStackTrace();
		}
		
		
		
	}

	public void setKategoria(String kategoria) {
		this.kategoria=kategoria;
		
	}

	public void setKomorka(String komorka) {
		this.komorka=komorka;
		
	}

	public void setDataOd(Date dataOd) {
		this.dataOd=dataOd;
		
	}

	public void setDataDo(Date dataDo) {
		this.dataDo=dataDo;
		
	}
	

}
