package pl.compan.docusafe.parametrization.utp;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.Map;
import java.util.TreeMap;

import pl.compan.docusafe.core.DSApi;

import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.OrderClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

public class UTPUsersReport extends Report {

	private String znak;// znak sprawy
	private String identyfikator;// unikalny identyfikator
	private String symbol;// klasa z JRWA
	private String kategoria;// kategoria archiwalne
	private String tytul;// tytul sprawy
	private String komorka;// komorka organizacyjna
	private Date rozp;// data rozpoczecia sprawy
	private Date zak;// data zakonczenia sprawy
	private Date utwOd;// data stworzenia dokumentu od
	private Date utwDo;// data stworzenia dokumentu do
	private String liczbaSpraw;
	private String liczbaWszystkichSpraw;
	private String stanowisko;

	private UniversalTableDumper dumper;
	
	public void initParametersAvailableValues() throws ReportException {
		super.initParametersAvailableValues();
		try {

			for (ReportParameter rp : params) {
				if (rp.getFieldCn().equals("komorka")) {
					rp.setAvailableValues(pobierzWszytkieKomorkiOrganizacyjne());
				}
				if (rp.getFieldCn().equals("symbol")) {
					rp.setAvailableValues(pobierzWszytkieSymbole());
				}
				if (rp.getFieldCn().equals("kategoria")) {
					rp.setAvailableValues(pobierzKategorie());
				}
				if (rp.getFieldCn().equals("znak")) {
					rp.setAvailableValues(pobierzZnak());
				}
				if (rp.getFieldCn().equals("stanowisko")) {
					rp.setAvailableValues(pobierzStanowiska());
				}

			}
		} catch (Exception e) {
			throw new ReportException(e);
		}
	}

	// zwraca stanowiska
	private Map<String, String> pobierzStanowiska() {
		Map<String, String> stanowiska = new TreeMap<String, String>();
		try {
			// ////
			FromClause from = new FromClause();
			TableAlias divisionTable = from.createTable("DS_DIVISION");
			WhereClause where = new WhereClause();
			OrderClause order = new OrderClause();

			SelectClause selectId = new SelectClause(true);
			SelectColumn colId = selectId.add(divisionTable, "id");
			SelectColumn colName = selectId.add(divisionTable, "NAME");
			SelectColumn colParentId = selectId.add(divisionTable, "PARENT_ID");
			
			where.add(Expression.eq(divisionTable.attribute("DIVISIONTYPE"),
					"POSITION"));

			// order.add(divisionTable.attribute("NAME"), OrderClause.ASC);

			SelectQuery selectQuery = new SelectQuery(selectId, from, where,
					order);

			ResultSet rs = selectQuery.resultSet(DSApi.context().session()
					.connection());
			ResultSet rs2 = null;
			while (rs.next()) {
				String tmp=rs.getString(colParentId.getPosition());
				if(tmp!=null){
					//oprocz rootdivision dodaje nazwy komorek nadrzednych
					if(rs.getString(colId.getPosition())!="1"){
						where = new WhereClause();
						where.add(Expression.eq(divisionTable.attribute("ID"), rs.getString(colParentId.getPosition())));
						selectQuery = new SelectQuery(selectId, from, where,
								order);
						rs2= selectQuery.resultSet(DSApi.context().session()
								.connection());
					}
				}
				if(rs2.next()){
					stanowiska.put(rs.getString(colId.getPosition()),
							rs2.getString(colName.getPosition())+": "+rs.getString(colName.getPosition()));
				}
				else{
				stanowiska.put(rs.getString(colId.getPosition()),
						rs.getString(colName.getPosition()));
				}
			}

		} catch (Exception e) {
			log.debug(e.getMessage(), e);
		}
		return stanowiska;
	}

	// zwraca komorki organizacyjne
	private Map<String, String> pobierzWszytkieKomorkiOrganizacyjne() {
		Map<String, String> komorki = new TreeMap<String, String>();
		try {
			// ////
			FromClause from = new FromClause();
			TableAlias divisionTable = from.createTable("DS_DIVISION");
			WhereClause where = new WhereClause();
			OrderClause order = new OrderClause();

			SelectClause selectId = new SelectClause(true);
			SelectColumn colId = selectId.add(divisionTable, "id");
			SelectColumn colName = selectId.add(divisionTable, "NAME");
			SelectColumn colCode = selectId.add(divisionTable, "CODE");
			SelectColumn col = selectId.add(divisionTable, "DIVISIONTYPE");

			Junction AND = Expression.conjunction();
			AND.add(Expression.eq(divisionTable.attribute("DIVISIONTYPE"),
					"division"));
			AND.add(Expression.ne(divisionTable.attribute("CODE"), "NULL"));
			where.add(AND);
			order.add(divisionTable.attribute("CODE"), OrderClause.ASC);

			SelectQuery selectQuery = new SelectQuery(selectId, from, where,
					order);

			ResultSet rs = selectQuery.resultSet(DSApi.context().session()
					.connection());

			while (rs.next()) {
				komorki.put(
						rs.getString(colCode.getPosition()),
						rs.getString(colCode.getPosition()) + " - "
								+ rs.getString(colName.getPosition()));

			}

		} catch (Exception e) {
			log.debug(e.getMessage(), e);
		}
		return komorki;
	}

	// //// zwraca symbole sprawy
	private Map<String, String> pobierzWszytkieSymbole() {
		Map<String, String> symbole = new TreeMap<String, String>();
		try {

			FromClause from = new FromClause();
			TableAlias rwaTable = from.createTable("v_dso_rwaa");
			WhereClause where = new WhereClause();
			OrderClause order = new OrderClause();
			SelectClause selectId = new SelectClause(true);
			// SelectColumn colId = selectId.add(rwaTable, "id");
			SelectColumn colCode = selectId.add(rwaTable, "code");

			order.add(rwaTable.attribute("code"), OrderClause.ASC);

			SelectQuery selectQuery = new SelectQuery(selectId, from, where,
					order);

			ResultSet rs = selectQuery.resultSet(DSApi.context().session()
					.connection());

			while (rs.next()) {
				symbole.put(rs.getString(colCode.getPosition()),
						rs.getString(colCode.getPosition()));

			}

		} catch (Exception e) {
			log.debug(e.getMessage(), e);
		}
		return symbole;
	}

	// ////zwraca niepowtarazjace sie kategorie archiwalne z bazy
	private Map<String, String> pobierzKategorie() {

		Map<String, String> kategorie = new TreeMap<String, String>();
		try {

			FromClause from = new FromClause();
			TableAlias rwaTable = from.createTable("v_dso_rwaa");
			WhereClause where = new WhereClause();
			OrderClause order = new OrderClause();
			SelectClause selectId = new SelectClause(true);
			SelectColumn colCode = selectId.add(rwaTable, "achome");

			where.add(Expression.ne(rwaTable.attribute("achome"), "NULL"));

			SelectQuery selectQuery = new SelectQuery(selectId, from, where,
					order);

			ResultSet rs = selectQuery.resultSet(DSApi.context().session()
					.connection());

			while (rs.next()) {
				kategorie.put(rs.getString(colCode.getPosition()),
						rs.getString(colCode.getPosition()));

			}

		} catch (Exception e) {
			log.debug(e.getMessage(), e);
		}
		return kategorie;

	}

	private Map<String, String> pobierzZnak() {

		Map<String, String> ZnakiSpraw = new TreeMap<String, String>();
		try {

			FromClause from = new FromClause();
			TableAlias rwaTable = from.createTable("dso_container");
			WhereClause where = new WhereClause();
			OrderClause order = new OrderClause();
			SelectClause selectId = new SelectClause(true);
			SelectColumn colCode = selectId.add(rwaTable, "officeid");
			where.add(Expression.eq(rwaTable.attribute("discriminator"),"case"));

			// where.add(Expression.ne(rwaTable.attribute("achome"), "NULL"));

			SelectQuery selectQuery = new SelectQuery(selectId, from, where,
					order);

			ResultSet rs = selectQuery.resultSet(DSApi.context().session()
					.connection());

			while (rs.next()) {
				ZnakiSpraw.put(rs.getString(colCode.getPosition()),
						rs.getString(colCode.getPosition()));

			}

		} catch (Exception e) {
			log.debug(e.getMessage(), e);
		}
		return ZnakiSpraw;

	}

	@Override
	public void doReport() throws Exception {
		for (ReportParameter rp : params) {
			if (rp.getFieldCn().equals("identyfikator")) {
				identyfikator = rp.getValueAsString();
			}
			if (rp.getFieldCn().equals("symbol")) {
				symbol = rp.getValueAsString();
			}
			if (rp.getFieldCn().equals("kategoria")) {
				kategoria = rp.getValueAsString();
			}
			if (rp.getFieldCn().equals("znak")) {
				znak = rp.getValueAsString();
			}
			if (rp.getFieldCn().equals("tytul")) {
				tytul = rp.getValueAsString();
			}
			if (rp.getFieldCn().equals("komorka")) {
				komorka = rp.getValueAsString();
			}
			if (rp.getFieldCn().equals("stanowisko")) {
				stanowisko = rp.getValueAsString();
				// w stanowisku porzekazuje id
				try {
					FromClause from = new FromClause();
					TableAlias divisionTable = from.createTable("DS_DIVISION");
					WhereClause where = new WhereClause();
					OrderClause order = new OrderClause();

					SelectClause selectId = new SelectClause(true);
					SelectColumn colId = selectId.add(divisionTable, "id");

					Junction AND = Expression.conjunction();
					AND.add(Expression.eq(
							divisionTable.attribute("DIVISIONTYPE"), "position"));
					AND.add(Expression.eq(divisionTable.attribute("name"),
							stanowisko));

					SelectQuery selectQuery = new SelectQuery(selectId, from,
							where, order);

					ResultSet rs = selectQuery.resultSet(DSApi.context()
							.session().connection());
					rs.next();
					stanowisko = rs.getString("id");

				} catch (Exception e) {
					log.debug(e.getMessage(), e);

				}

			}

			if (rp.getFieldCn().equals("dataRozp")) {
				String[] tmp = rp.getValueAsString().split("-");
				if (tmp.length != 1) {
					rozp = new Date(Integer.parseInt(tmp[2]) - 1900,
							Integer.parseInt(tmp[1]) - 1,
							Integer.parseInt(tmp[0]));
				}
			}
			if (rp.getFieldCn().equals("dataZak")) {
				String[] tmp = rp.getValueAsString().split("-");
				if (tmp.length != 1) {
					zak = new Date(Integer.parseInt(tmp[2]) - 1900,
							Integer.parseInt(tmp[1]) - 1,
							Integer.parseInt(tmp[0]));
				}
			}

			if (rp.getFieldCn().equals("dataUtwOd")) {
				String[] tmp = rp.getValueAsString().split("-");
				if (tmp.length != 1) {
					utwOd = new Date(Integer.parseInt(tmp[2]) - 1900,
							Integer.parseInt(tmp[1]) - 1,
							Integer.parseInt(tmp[0]));
				}
			}
			if (rp.getFieldCn().equals("dataUtwDo")) {
				String[] tmp = rp.getValueAsString().split("-");
				if (tmp.length != 1) {
					utwDo = new Date(Integer.parseInt(tmp[2]) - 1900,
							Integer.parseInt(tmp[1]) - 1,
							Integer.parseInt(tmp[0]));
				}
			}
			if (rp.getFieldCn().equals("liczbaSpraw")) {
				liczbaSpraw = rp.getValueAsString();
			}
			if (rp.getFieldCn().equals("liczbaWszystkichSpraw")) {
				liczbaWszystkichSpraw = rp.getValueAsString();
			}

		}

		dumper = new UniversalTableDumper();
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "raport.xls");
		poiDumper.openFile(xlsFile);
		FileOutputStream fis = new FileOutputStream(xlsFile);

		doReportForAll().getWorkbook().write(fis);
		fis.close();
		dumper.addDumper(poiDumper);

	}

	private UTPUserXlsReport doReportForAll() throws Exception {
		UTPUserXlsReport xlsReport = new UTPUserXlsReport("Raport uzytkownikow");

		if (znak != null && !znak.equals(""))
			xlsReport.setZnak(znak);
		if (symbol != null && !symbol.equals(""))
			xlsReport.setSymbol(symbol);
		if (kategoria != null && !kategoria.equals(""))
			xlsReport.setKategoria(kategoria);
		if (identyfikator != null && !identyfikator.equals(""))
			xlsReport.setIdentyfikator(identyfikator);
		if (tytul != null && !tytul.equals(""))
			xlsReport.setTytul(tytul);
		if (komorka != null && !komorka.equals(""))
			xlsReport.setKomorka(komorka);
		if (liczbaSpraw != null && !liczbaSpraw.equals(""))
			xlsReport.setLS(liczbaSpraw);
		if (liczbaWszystkichSpraw != null && !liczbaWszystkichSpraw.equals(""))
			xlsReport.setLWS(liczbaWszystkichSpraw);
		if (stanowisko != null && !stanowisko.equals(""))
			xlsReport.setStanowisko(stanowisko);
		xlsReport.setDataRozp(rozp);
		xlsReport.setDataZakn(zak);
		xlsReport.setDataUtwOd(utwOd);
		xlsReport.setDataUtwDo(utwDo);

		xlsReport.generate();
		return xlsReport;
	}

}
