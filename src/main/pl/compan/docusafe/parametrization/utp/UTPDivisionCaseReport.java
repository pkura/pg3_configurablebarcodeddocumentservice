package pl.compan.docusafe.parametrization.utp;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.LinkedHashMap;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.OrderClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

public class UTPDivisionCaseReport extends Report {
	
	private String komorka;
	private Date dataOd;// data otwarcia sprawy od
	private Date dataDo;// data zamkniecia sprawy do
	private String kategoria;// kategoria rwa
	private UniversalTableDumper dumper;
	
	public void initParametersAvailableValues() throws ReportException {
		super.initParametersAvailableValues();
		try {

			for (ReportParameter rp : params) {
				if (rp.getFieldCn().equals("komorka")) {
					rp.setAvailableValues(pobierzWszytkieKomorkiOrganizacyjne());
				}
				if (rp.getFieldCn().equals("kategoria")) {
					rp.setAvailableValues(pobierzWszytkieSymbole());
				}

			}
		} catch (Exception e) {
			throw new ReportException(e);
		}
	}
	
	
	
	@Override
	public void doReport() throws Exception {
		for (ReportParameter rp : params) {
			if (rp.getFieldCn().equals("kategoria")) {
				kategoria = rp.getValueAsString();
			}
			if (rp.getFieldCn().equals("komorka")) {
				komorka = rp.getValueAsString();
			}

			if (rp.getFieldCn().equals("dataOd")) {
				String[] tmp = rp.getValueAsString().split("-");
				if (tmp.length != 1) {
					dataOd = new Date(Integer.parseInt(tmp[2]) - 1900,
							Integer.parseInt(tmp[1]) - 1,
							Integer.parseInt(tmp[0]));
				}
			}
			if (rp.getFieldCn().equals("dataDo")) {
				String[] tmp = rp.getValueAsString().split("-");
				if (tmp.length != 1) {
					dataDo = new Date(Integer.parseInt(tmp[2]) - 1900,
							Integer.parseInt(tmp[1]) - 1,
							Integer.parseInt(tmp[0]));
				}
			}
		}
		dumper = new UniversalTableDumper();
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "raport.xls");
		poiDumper.openFile(xlsFile);
		FileOutputStream fis = new FileOutputStream(xlsFile);

		doReportForAll().getWorkbook().write(fis);
		fis.close();
		dumper.addDumper(poiDumper);
		
	}
	
	private UTPDivisionCaseXlsReport doReportForAll() throws Exception {
		UTPDivisionCaseXlsReport xlsReport = new UTPDivisionCaseXlsReport("Raport zestawienie czasu przetwarzania sprawy w poszczególnych komórkach organizacyjnych oraz czasu przetwarzania jednakowego typu sprawy w danej komórce org. ");


		if (kategoria != null && !kategoria.equals(""))
			xlsReport.setKategoria(kategoria);

		if (komorka != null && !komorka.equals(""))
			xlsReport.setKomorka(komorka);
		xlsReport.setDataOd(dataOd);
		xlsReport.setDataDo(dataDo);

		xlsReport.generate();
		return xlsReport;
	}
	
	
	
	// //// zwraca symbole sprawy
	private Map<String, String> pobierzWszytkieSymbole() {
		Map<String, String> symbole = new LinkedHashMap<String, String>();
		try {

			FromClause from = new FromClause();
			TableAlias rwaTable = from.createTable("v_dso_rwaa");
			WhereClause where = new WhereClause();
			OrderClause order = new OrderClause();
			SelectClause selectId = new SelectClause(true);
			SelectColumn colId = selectId.add(rwaTable, "id");
			SelectColumn colCode = selectId.add(rwaTable, "code");
			SelectColumn colDesc = selectId.add(rwaTable, "description");

			order.add(rwaTable.attribute("code"), OrderClause.ASC);

			SelectQuery selectQuery = new SelectQuery(selectId, from, where,
					order);

			ResultSet rs = selectQuery.resultSet(DSApi.context().session()
					.connection());

			while (rs.next()) {
				symbole.put(rs.getString( colId.getPosition()),
						rs.getString(colCode.getPosition())+" : "+rs.getString(colDesc.getPosition()));

			}

		} catch (Exception e) {
			log.debug(e.getMessage(), e);
		}
		return symbole;
	}
	
	// zwraca komorki organizacyjne
	private Map<String, String> pobierzWszytkieKomorkiOrganizacyjne() {
		Map<String, String> komorki = new LinkedHashMap<String, String>();
		try {
			// ////
			FromClause from = new FromClause();
			TableAlias divisionTable = from.createTable("DS_DIVISION");
			WhereClause where = new WhereClause();
			OrderClause order = new OrderClause();

			SelectClause selectId = new SelectClause(true);
			SelectColumn colId = selectId.add(divisionTable, "id");
			SelectColumn colName = selectId.add(divisionTable, "NAME");
			SelectColumn colCode = selectId.add(divisionTable, "CODE");
			SelectColumn col = selectId.add(divisionTable, "DIVISIONTYPE");

			Junction AND = Expression.conjunction();
			AND.add(Expression.eq(divisionTable.attribute("DIVISIONTYPE"),
					"division"));
			AND.add(Expression.ne(divisionTable.attribute("CODE"), "NULL"));
			where.add(AND);
			order.add(divisionTable.attribute("CODE"), OrderClause.ASC);

			SelectQuery selectQuery = new SelectQuery(selectId, from, where,
					order);

			ResultSet rs = selectQuery.resultSet(DSApi.context().session()
					.connection());

			while (rs.next()) {
				komorki.put(
						rs.getString(colCode.getPosition()),
						 rs.getString(colName.getPosition()));

			}

		} catch (Exception e) {
			log.debug(e.getMessage(), e);
		}
		return komorki;
	}

}
