package pl.compan.docusafe.parametrization.uek;

import com.google.common.collect.Maps;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContextOpener;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.SessionUtils;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;


/**
 * Created by Darek on 01.04.14.
 */
public class WniosekUrlopowyLogic extends AbstractDocumentLogic {

    private final static String DATA_DO = "DATA_DO";
    private final static String DATA_OD = "DATA_OD";
    private final static String ILOSC_DNI_ROBOCZYCH = "ILOSC_DNI_ROBOCZYCH";
    private final static String ILOSC_GODZIN_ROBOCZYCH = "ILOSC_GODZIN_ROBOCZYCH";
    private final static String REALIZATION_DIVISION_CODE_SESSION_KEY = "REALIZATION_DIVISION_CODE_SESSION_KEY";
    private final static String WNIOSKUJACY_JEDNOSTKA = "WNIOSKUJACY_JEDNOSTKA";
    private final static String ZASTEPUJACY = "ZASTEPUJACY";



    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {

        Field msgField = null;
        StringBuilder msgBuilder = new StringBuilder();
        boolean succes = true;

        try {

            if (values.get(dwr(DATA_DO)).isSender() || values.get(dwr(DATA_OD)).isSender() ){

                if(!(succes = CountWorkingDays(values))) {
                    msgBuilder.append("Z�a data rozpocz�cia lub zako�czenia urlopu.");
                }
            }



            if(values.get(dwr(ZASTEPUJACY)).isSender()) {

                Long docId = fm.getDocumentId();



                String sql = "delete from dso_person where discriminator ='recipient' and document_id="
                        + docId.toString();
                PreparedStatement ps = null;
                DSContextOpener opener = DSContextOpener.openAsAdminIfNeed();
                try {
                    ps = DSApi.context().prepareStatement(sql);
                    ps.executeUpdate();
                } catch (SQLException e) {
                    DSApi.context().rollback();
                    e.printStackTrace();
                }
                finally {
                    opener.closeIfNeed();
                }



            }

            if(values.get(dwr("SENDER_HERE")).isSender()) {

                Long docId = fm.getDocumentId();

                String sql = "delete from dso_person where discriminator ='sender' and document_id="
                        + docId.toString();
                PreparedStatement ps = null;
                DSContextOpener opener = DSContextOpener.openAsAdminIfNeed();
                try {
                    ps = DSApi.context().prepareStatement(sql);
                    ps.executeUpdate();
                } catch (SQLException e) {
                    DSApi.context().rollback();
                    e.printStackTrace();
                }
                finally {
                    opener.closeIfNeed();
                }

            }


        } catch (Exception e) {

            if (!succes) {
                values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
                msgBuilder.append(e.getMessage());
                return new Field("messageField", msgBuilder.toString(), Field.Type.BOOLEAN);
        }

            // for(StackTraceElement st : e.setStackTrace()){
            //    msgBuilder.append(st.toString());
            // }
        } finally {
            DSApi.close();
        }
        if (msgBuilder.length() > 0) {

            values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
            return new Field("messageField", msgBuilder.toString(), Field.Type.BOOLEAN);
        }
        return null;
    }


    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {


    }



    private boolean CountWorkingDays(Map<String, FieldData> values) {

        Date DataOD = new Date();
        Date DataDO = new Date();
        int Days = 0;

        try {

            DataOD = values.get(dwr(DATA_OD)).getDateData();
            DataDO = values.get(dwr(DATA_DO)).getDateData();

        } catch(Exception e) {

            return true;
        }
        Calendar KalendarzOD = new GregorianCalendar();
        Calendar KalendarzDO = new GregorianCalendar();
        Calendar KalendarzToday = new GregorianCalendar();
        KalendarzOD.clear();
        KalendarzDO.clear();
        KalendarzOD.setTime(DataOD);
        KalendarzDO.setTime(DataDO);



        if(KalendarzOD.after(KalendarzDO) || KalendarzOD.before(KalendarzToday)) {
            return false;
        }

        KalendarzDO.add(Calendar.DAY_OF_MONTH, 1);

        while(KalendarzOD.before(KalendarzDO)  ){

            if( (KalendarzOD.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY) && (KalendarzOD.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY)) {

                Days++;
                boolean holidays = false;

                try {
                    for (Date x: DateUtils.getHolidays(KalendarzOD.get(Calendar.YEAR))) {

                        Calendar temp = new GregorianCalendar();
                        temp.clear();
                        temp.setTime(x);

                        if (KalendarzOD.compareTo(temp) == 0){

                            holidays = true;
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
                if (holidays) {
                    Days--;
                }


            }

            KalendarzOD.add(Calendar.DAY_OF_MONTH, 1);

        }

        values.get(dwr(ILOSC_DNI_ROBOCZYCH)).setIntegerData(Days);

        return true;

    }

    public String dwr(String cn){
        return "DWR_" + cn;
    }
    @Override
    public void documentPermissions(Document document) throws EdmException {

    }


    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {

    }









}


