package pl.compan.docusafe.parametrization.uek;

/**
 * Created by Raf on 2014-05-13.
 */

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class Znaki implements Comparable<Znaki> {



    private Long id;
    private Integer Lp;

    private BigDecimal wartoscNetto = BigDecimal.ZERO.setScale(2);
    private String Numerogloszenia;
    private Long Przedmiotzamowienia;
    private Long Rodzajzamowienia;
    private Long Tryb;
    private String Uzasadnienie;
    private String Kraj;


    public String getKraj() {
        return Kraj;
    }

    public void setKraj(String kraj) {
        Kraj = kraj;
    }

    public String getUzasadnienie() {
        return Uzasadnienie;
    }

    public void setUzasadnienie(String uzasadnienie) {
        Uzasadnienie = uzasadnienie;
    }

    public Long getTryb() {
        return Tryb;
    }

    public void setTryb(Long tryb) {
        Tryb = tryb;
    }

    public Long getRodzajzamowienia() {
        return Rodzajzamowienia;
    }

    public void setRodzajzamowienia(Long rodzajzamowienia) {
        Rodzajzamowienia = rodzajzamowienia;
    }

    public Long getPrzedmiotzamowienia() {
        return Przedmiotzamowienia;
    }

    public void setPrzedmiotzamowienia(Long przedmiotzamowienia) {
        Przedmiotzamowienia = przedmiotzamowienia;
    }

    public String getNumerogloszenia() {
        return Numerogloszenia;
    }

    public void setNumerogloszenia(String numerogloszenia) {
        Numerogloszenia = numerogloszenia;
    }

    public BigDecimal getWartoscNetto() {
        return wartoscNetto;
    }

    public void setWartoscNetto(BigDecimal wartoscNetto) {
        this.wartoscNetto = wartoscNetto;
    }
    public Integer getLp() {
        return Lp;
    }

    public void setLp(Integer lp) {
        Lp = lp;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Override
    public int compareTo(Znaki stamp) {
        return this.id.compareTo(stamp.id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Znaki znaki = (Znaki) o;

        if (Kraj != null ? !Kraj.equals(znaki.Kraj) : znaki.Kraj != null) return false;
        if (Lp != null ? !Lp.equals(znaki.Lp) : znaki.Lp != null) return false;
        if (Numerogloszenia != null ? !Numerogloszenia.equals(znaki.Numerogloszenia) : znaki.Numerogloszenia != null)
            return false;
        if (Przedmiotzamowienia != null ? !Przedmiotzamowienia.equals(znaki.Przedmiotzamowienia) : znaki.Przedmiotzamowienia != null)
            return false;
        if (Rodzajzamowienia != null ? !Rodzajzamowienia.equals(znaki.Rodzajzamowienia) : znaki.Rodzajzamowienia != null)
            return false;
        if (Tryb != null ? !Tryb.equals(znaki.Tryb) : znaki.Tryb != null) return false;
        if (Uzasadnienie != null ? !Uzasadnienie.equals(znaki.Uzasadnienie) : znaki.Uzasadnienie != null) return false;
        if (id != null ? !id.equals(znaki.id) : znaki.id != null) return false;
        if (wartoscNetto != null ? !wartoscNetto.equals(znaki.wartoscNetto) : znaki.wartoscNetto != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (Lp != null ? Lp.hashCode() : 0);
        result = 31 * result + (wartoscNetto != null ? wartoscNetto.hashCode() : 0);
        result = 31 * result + (Numerogloszenia != null ? Numerogloszenia.hashCode() : 0);
        result = 31 * result + (Przedmiotzamowienia != null ? Przedmiotzamowienia.hashCode() : 0);
        result = 31 * result + (Rodzajzamowienia != null ? Rodzajzamowienia.hashCode() : 0);
        result = 31 * result + (Tryb != null ? Tryb.hashCode() : 0);
        result = 31 * result + (Uzasadnienie != null ? Uzasadnienie.hashCode() : 0);
        result = 31 * result + (Kraj != null ? Kraj.hashCode() : 0);
        return result;
    }

}