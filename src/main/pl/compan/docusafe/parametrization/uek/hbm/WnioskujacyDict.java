package pl.compan.docusafe.parametrization.uek.hbm;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

import javax.persistence.*;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Damian on 02.05.14.
 */


@Entity(name = "uek.WnioskujacyDict")
@Table(name = "dsg_uek_wnioskujacy")
public class WnioskujacyDict {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;

    @Column(name = "WN_USER")
    Long userId;

    @Column(name = "WN_DIVISION")
    Long divisionId;



    public static WnioskujacyDict findById(Long wnioskujacyId) throws EdmException {

        Criteria criteria = DSApi.context().session().createCriteria(WnioskujacyDict.class);
        criteria.add(Restrictions.idEq(wnioskujacyId));

        return (WnioskujacyDict)criteria.uniqueResult();

    }


    public static Map<String,String> getMapToDocFields(){
        Map<String, String> mapping = new LinkedHashMap<String, String>();
        mapping.put("userId","WN_USER");
        mapping.put("divisionId","WN_DIVISION");
        return mapping;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(Long divisionId) {
        this.divisionId = divisionId;
    }
}
