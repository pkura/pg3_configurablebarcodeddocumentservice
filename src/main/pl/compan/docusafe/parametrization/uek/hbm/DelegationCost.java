/**
 * 
 */
package pl.compan.docusafe.parametrization.uek.hbm;

import java.math.BigDecimal;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 08-05-2014
 * ds_trunk_2014
 * DelegationCost.java
 */
public class DelegationCost {

	Long id;
	Long waluta;
	BigDecimal kwota;
	BigDecimal kwotaPln;
	String propozycjaZmian;
	
	public DelegationCost(){
		
	}
	
	public static DelegationCost find(Long id) throws EdmException
	{
		  return (DelegationCost)Finder.find(DelegationCost.class, id);
	}
	
	public void create() throws EdmException 
	{
		 try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();	   
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
		
	}
	
	public void update() throws EdmException 
	{
		 try 
        {
            DSApi.context().session().update(this);
            DSApi.context().session().flush();	   
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
		
	}
	
	public void delete() throws EdmException 
	{
		 try 
        {
            DSApi.context().session().delete(this);
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
		
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getWaluta() {
		return waluta;
	}
	public void setWaluta(Long waluta) {
		this.waluta = waluta;
	}
	public BigDecimal getKwota() {
		return kwota;
	}
	public void setKwota(BigDecimal kwota) {
		this.kwota = kwota;
	}
	public BigDecimal getKwotaPln() {
		return kwotaPln;
	}
	public void setKwotaPln(BigDecimal kwotaPln) {
		this.kwotaPln = kwotaPln;
	}

	public String getPropozycjaZmian() {
		return propozycjaZmian;
	}

	public void setPropozycjaZmian(String propozycjaZmian) {
		this.propozycjaZmian = propozycjaZmian;
	}
	
}
