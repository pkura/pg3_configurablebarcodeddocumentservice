package pl.compan.docusafe.parametrization.uek.hbm;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

public abstract class ErpPositionEntity {

    public abstract Integer getPositionNumber();

    public abstract ZrodloFinansowaniaDict getZrodloFinansowania();




    public static <T extends ErpPositionEntity> T findById(Long id, Class<? extends ErpPositionEntity> clazz) throws EdmException {
        Criteria criteria = DSApi.context().session().createCriteria(clazz);
        criteria.add(Restrictions.idEq(id));

        return (T)criteria.uniqueResult();
    }

    public void save() throws EdmException {
        boolean wasOpened = false;
        Transaction tx = null;
        try{
            wasOpened = DSApi.openContextIfNeeded();
            Session session = DSApi.context().session();
            tx = session.beginTransaction();

            session.saveOrUpdate(this);

            tx.commit();

        }catch (EdmException e){
            if(tx != null){
                tx.rollback();
            }
            DSApi.closeContextIfNeeded(wasOpened);
            throw e;
        }
    }

    public void delete() throws EdmException{
        boolean wasOpened = false;
        Transaction tx = null;
        try{
            wasOpened = DSApi.openContextIfNeeded();
            Session session = DSApi.context().session();
            tx = session.beginTransaction();

            session.delete(this);

            tx.commit();

        }catch (EdmException e){
            if(tx != null){
                tx.rollback();
            }
            DSApi.closeContextIfNeeded(wasOpened);
            throw e;
        }
    }

}
