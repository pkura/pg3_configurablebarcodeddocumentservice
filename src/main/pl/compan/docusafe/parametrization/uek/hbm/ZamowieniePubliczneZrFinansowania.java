package pl.compan.docusafe.parametrization.uek.hbm;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.Cascade;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
@Entity(name = "uek.ZamowieniePubliczneZrFinansowania")
@Table(name = "DSG_UEK_ZAM_PUB_ZR_FINANS")
public class ZamowieniePubliczneZrFinansowania {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "FINANCIAL_AMOUNT")
    private BigDecimal financialAmount;


    @Column(name = "DESCRIPTION")
    private String description;

    @OneToOne
    @Cascade({org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    @JoinColumn(name = "ZRODLO_ID")
    ZrodloFinansowaniaDict zrodloFinansowania;

    public void save() throws EdmException {
        boolean wasOpened = false;
        Transaction tx = null;
        try {
            wasOpened = DSApi.openContextIfNeeded();
            Session session = DSApi.context().session();
            tx = session.beginTransaction();

            session.saveOrUpdate(this);

            tx.commit();

        } catch (EdmException e) {
            if (tx != null) {
                tx.rollback();
            }
            DSApi.closeContextIfNeeded(wasOpened);
            throw e;
        }
    }

    public void delete() throws EdmException {
        boolean wasOpened = false;
        Transaction tx = null;
        try {
            wasOpened = DSApi.openContextIfNeeded();
            Session session = DSApi.context().session();
            tx = session.beginTransaction();

            session.delete(this);

            tx.commit();

        } catch (EdmException e) {
            if (tx != null) {
                tx.rollback();
            }
            DSApi.closeContextIfNeeded(wasOpened);
            throw e;
        }
    }

    public static ZamowieniePubliczneZrFinansowania findById(Long entryId) throws EdmException {

        Criteria criteria = DSApi.context().session().createCriteria(ZamowieniePubliczneZrFinansowania.class);
        criteria.add(Restrictions.idEq(entryId));

        return (ZamowieniePubliczneZrFinansowania) criteria.uniqueResult();
    }

    public static Map<String, String> getMapToDocFields() {
        Map<String, String> mapping = new LinkedHashMap<String, String>();
        mapping.put("financialAmount", "FINANCIAL_AMOUNT");
        return mapping;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getFinancialAmount() {
        return financialAmount;
    }

    public void setFinancialAmount(BigDecimal financialAmount) {
        this.financialAmount = financialAmount;
    }

    public ZrodloFinansowaniaDict getZrodloFinansowania() {
        return zrodloFinansowania;
    }

    public void setZrodloFinansowania(ZrodloFinansowaniaDict zrodloFinansowania) {
        this.zrodloFinansowania = zrodloFinansowania;
    }
}
