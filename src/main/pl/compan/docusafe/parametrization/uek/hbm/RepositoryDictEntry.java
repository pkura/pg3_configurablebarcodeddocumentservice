package pl.compan.docusafe.parametrization.uek.hbm;

import javax.persistence.*;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Damian on 08.05.14.
 */

@Entity(name = "uek.RepositoryDictEntry")
@Table(name = RepositoryDictEntry.TABLE_NAME)
public class RepositoryDictEntry {

    public final static String TABLE_NAME = "dsg_uek_repository_dict_entry";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "DSD_REPO89_PRZEDSIEWZIECIA_DYDAKTYCZNE")
    private Long dsdRepo89PrzedsiewzieciaDydaktyczne;
    @Column(name = "DSD_REPO95_RODZAJ_STUDIOW")
    private Long dsdRepo95RodzajStudiow;
    @Column(name = "DSD_REPO77_RODZAJ_PRZEDSIEWZIECIA")
    private Long dsdRepo77RodzajPrzedsiewziecia;
    @Column(name = "DSD_REPO88_BUDYNKI")
    private Long dsdRepo88Budynki;
    @Column(name = "DSD_REPO97_DS")
    private Long dsdRepo97Ds;
    @Column(name = "DSD_REPO98_DOTACJA")
    private Long dsdRepo98Dotacja;
    @Column(name = "DSD_REPO119_KPZF")
    private Long dsdRepo119Kpzf;

    public static Map<String,String> getMapToDocFields(){
        Map<String, String> mapping = new LinkedHashMap<String, String>();
        mapping.put("dsdRepo89PrzedsiewzieciaDydaktyczne","DSD_REPO89_PRZEDSIEWZIECIA_DYDAKTYCZNE");
        mapping.put("dsdRepo95RodzajStudiow","DSD_REPO95_RODZAJ_STUDIOW");
        mapping.put("dsdRepo77RodzajPrzedsiewziecia","DSD_REPO77_RODZAJ_PRZEDSIEWZIECIA");
        mapping.put("dsdRepo88Budynki","DSD_REPO88_BUDYNKI");
        mapping.put("dsdRepo97Ds","DSD_REPO97_DS");
        mapping.put("dsdRepo98Dotacja","DSD_REPO98_DOTACJA");
        mapping.put("dsdRepo119Kpzf","DSD_REPO119_KPZF");
        return mapping;
    }

    public String getTableName(){
        return this.TABLE_NAME;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDsdRepo89PrzedsiewzieciaDydaktyczne() {
        return dsdRepo89PrzedsiewzieciaDydaktyczne;
    }

    public void setDsdRepo89PrzedsiewzieciaDydaktyczne(Long dsdRepo89PrzedsiewzieciaDydaktyczne) {
        this.dsdRepo89PrzedsiewzieciaDydaktyczne = dsdRepo89PrzedsiewzieciaDydaktyczne;
    }

    public Long getDsdRepo95RodzajStudiow() {
        return dsdRepo95RodzajStudiow;
    }

    public void setDsdRepo95RodzajStudiow(Long dsdRepo95RodzajStudiow) {
        this.dsdRepo95RodzajStudiow = dsdRepo95RodzajStudiow;
    }

    public Long getDsdRepo77RodzajPrzedsiewziecia() {
        return dsdRepo77RodzajPrzedsiewziecia;
    }

    public void setDsdRepo77RodzajPrzedsiewziecia(Long dsdRepo77RodzajPrzedsiewziecia) {
        this.dsdRepo77RodzajPrzedsiewziecia = dsdRepo77RodzajPrzedsiewziecia;
    }

    public Long getDsdRepo88Budynki() {
        return dsdRepo88Budynki;
    }

    public void setDsdRepo88Budynki(Long dsdRepo88Budynki) {
        this.dsdRepo88Budynki = dsdRepo88Budynki;
    }

    public Long getDsdRepo97Ds() {
        return dsdRepo97Ds;
    }

    public void setDsdRepo97Ds(Long dsdRepo97Ds) {
        this.dsdRepo97Ds = dsdRepo97Ds;
    }

    public Long getDsdRepo98Dotacja() {
        return dsdRepo98Dotacja;
    }

    public void setDsdRepo98Dotacja(Long dsdRepo98Dotacja) {
        this.dsdRepo98Dotacja = dsdRepo98Dotacja;
    }

    public Long getDsdRepo119Kpzf() {
        return dsdRepo119Kpzf;
    }

    public void setDsdRepo119Kpzf(Long dsdRepo119Kpzf) {
        this.dsdRepo119Kpzf = dsdRepo119Kpzf;
    }
}
