package pl.compan.docusafe.parametrization.uek.hbm;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.Cascade;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
@Entity(name = "uek.ZamowieniePubliczneModuly")
@Table(name = "DSG_UEK_ZAM_PUB_MODULY")
public class ZamowieniePubliczneModuly {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "POPUP_MODUL_NAZWA")
    private String nazwa;
    @Column(name = "POPUP_MODUL_KODY_CPV_TEXT")
    private String cpvText;
    @Column(name = "POPUP_MODUL_KODY_CPV_MAIN")
    private Long cpvMain;
    @Column(name = "POPUP_MODUL_WARTOSC_NETTO")
    private BigDecimal wartoscNetto;
    @Column(name = "POPUP_MODUL_WARTOSC_NETTO_EURO")
    private BigDecimal wartoscNettoEuro;
    @Column(name = "POPUP_MODUL_WARTOSC_BRUTTO")
    private BigDecimal wartoscBrutto;
    @Column(name = "POPUP_MODUL_FINANS_BRUTTO")
    private BigDecimal wartoscFinansBrutto;
    @Column(name = "POPUP_MODUL_STAWKA_VAT")
    private Long stawkaVat;
    @Column(name = "POPUP_MODUL_REALIZACJA_OD")
    private java.util.Date realizacjaOd;
    @Column(name = "POPUP_MODUL_REALIZACJA_DO")
    private java.util.Date realizacjaDo;

    @OneToMany
    @Cascade({org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    @JoinTable(name = "DSG_UEK_ZAM_PUB_TO_CPV", joinColumns = { @JoinColumn(name = "MODULE_ID") }, inverseJoinColumns = { @JoinColumn(name = "CPV_ID") })
    Set<StawkaVatDict> cpvDict = new HashSet<StawkaVatDict>();

    public void save() throws EdmException {
        boolean wasOpened = false;
        Transaction tx = null;
        try {
            wasOpened = DSApi.openContextIfNeeded();
            Session session = DSApi.context().session();
            tx = session.beginTransaction();

            session.saveOrUpdate(this);

            tx.commit();

        } catch (EdmException e) {
            if (tx != null) {
                tx.rollback();
            }
            DSApi.closeContextIfNeeded(wasOpened);
            throw e;
        }
    }

    public void delete() throws EdmException {
        boolean wasOpened = false;
        Transaction tx = null;
        try {
            wasOpened = DSApi.openContextIfNeeded();
            Session session = DSApi.context().session();
            tx = session.beginTransaction();

            session.delete(this);

            tx.commit();

        } catch (EdmException e) {
            if (tx != null) {
                tx.rollback();
            }
            DSApi.closeContextIfNeeded(wasOpened);
            throw e;
        }
    }

    public static ZamowieniePubliczneModuly findById(Long entryId) throws EdmException {

        Criteria criteria = DSApi.context().session().createCriteria(ZamowieniePubliczneModuly.class);
        criteria.add(Restrictions.idEq(entryId));

        return (ZamowieniePubliczneModuly) criteria.uniqueResult();
    }

    public static Map<String, String> getMapToDocFields() {
        Map<String, String> mapping = new LinkedHashMap<String, String>();
        //(\"[^\"]+\"),(\"[^\"]+\")
        //$2,$1
        mapping.put("nazwa", "POPUP_MODUL_NAZWA");
        mapping.put("cpvText", "POPUP_MODUL_KODY_CPV_TEXT");
        mapping.put("cpvMain", "POPUP_MODUL_KODY_CPV_MAIN");
        mapping.put("wartoscNetto", "POPUP_MODUL_WARTOSC_NETTO");
        mapping.put("wartoscNettoEuro", "POPUP_MODUL_WARTOSC_NETTO_EURO");
        mapping.put("wartoscBrutto", "POPUP_MODUL_WARTOSC_BRUTTO");
        mapping.put("wartoscFinansBrutto", "POPUP_MODUL_FINANS_BRUTTO");
        mapping.put("stawkaVat", "POPUP_MODUL_STAWKA_VAT");
        mapping.put("realizacjaOd", "POPUP_MODUL_REALIZACJA_OD");
        mapping.put("realizacjaDo", "POPUP_MODUL_REALIZACJA_DO");
        return mapping;
    }

    public static Map<String, String> getMapToHbn() {
        Map<String, String> toDocFields = getMapToDocFields();
        Map<String, String> toHib = new HashMap<String, String>();

        for (Map.Entry<String, String> entry : toDocFields.entrySet())
            toHib.put(entry.getValue(), entry.getKey());

        return toHib;
    }

    public Long getId() {
        return id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public String getCpvText() {
        return cpvText;
    }

    public Long getCpvMain() {
        return cpvMain;
    }

    public BigDecimal getWartoscNetto() {
        return wartoscNetto;
    }

    public BigDecimal getWartoscNettoEuro() {
        return wartoscNettoEuro;
    }

    public BigDecimal getWartoscBrutto() {
        return wartoscBrutto;
    }

    public BigDecimal getWartoscFinansBrutto() {
        return wartoscFinansBrutto;
    }

    public Long getStawkaVat() {
        return stawkaVat;
    }

    public Date getRealizacjaOd() {
        return realizacjaOd;
    }

    public Date getRealizacjaDo() {
        return realizacjaDo;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public void setCpvText(String cpvText) {
        this.cpvText = cpvText;
    }

    public void setCpvMain(Long cpvMain) {
        this.cpvMain = cpvMain;
    }

    public void setWartoscNetto(BigDecimal wartoscNetto) {
        this.wartoscNetto = wartoscNetto;
    }

    public void setWartoscNettoEuro(BigDecimal wartoscNettoEuro) {
        this.wartoscNettoEuro = wartoscNettoEuro;
    }

    public void setWartoscBrutto(BigDecimal wartoscBrutto) {
        this.wartoscBrutto = wartoscBrutto;
    }

    public void setWartoscFinansBrutto(BigDecimal wartoscFinansBrutto) {
        this.wartoscFinansBrutto = wartoscFinansBrutto;
    }

    public void setStawkaVat(Long stawkaVat) {
        this.stawkaVat = stawkaVat;
    }

    public void setRealizacjaOd(Date realizacjaOd) {
        this.realizacjaOd = realizacjaOd;
    }

    public void setRealizacjaDo(Date realizacjaDo) {
        this.realizacjaDo = realizacjaDo;
    }

    public Set<StawkaVatDict> getCpvDict() {
        return cpvDict;
    }

    public void setCpvDict(Set<StawkaVatDict> cpvDict) {
        this.cpvDict = cpvDict;
    }
}
