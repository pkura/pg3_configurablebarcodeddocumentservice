package pl.compan.docusafe.parametrization.uek.hbm;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

import javax.persistence.*;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Damian on 12.03.14.
 */

@Entity(name = "uek.ZrodloFinansowaniaDict")
@Table(name = "dsg_uek_zrodlo_finansowania")
public class ZrodloFinansowaniaDict extends ErpPositionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "BUDGET_KIND")
    private Long budgetKind;

    @Column(name = "BUDGET")
    private Long budget;

    @Column(name = "POSITION")
    private Long position;

    @Column(name = "SOURCE")
    private Long source;

    @Column(name = "CONTRACT")
    private Long contract;

    @Column(name = "CONTRACT_STAGE")
    private Long contractStage;

    @Column(name = "FUND")
    private Long fund;

    @Column(name ="PROJECT_BUDGET")
    private Long projectBudget;

    @Column(name ="BUDGET_KO")
    private Long budgetKo;

    @Column(name ="FINANCIAL_TASK")
    private Long financialTask;

    /** Numer pozycji nadawany podczas przygotowania eksportu do ERP*/
    @Column(name ="POSITION_NUMBER")
    private Integer positionNumber;

    @Column(name ="ZRODLO_DESCRIPTION")
    private String description;


    public static ZrodloFinansowaniaDict findById(Long zrodloId) throws EdmException {

        Criteria criteria = DSApi.context().session().createCriteria(ZrodloFinansowaniaDict.class);
        criteria.add(Restrictions.idEq(zrodloId));

        return (ZrodloFinansowaniaDict)criteria.uniqueResult();
    }



    @Override
    public ZrodloFinansowaniaDict getZrodloFinansowania() {
        return this;
    }

    public void save() throws EdmException {
        boolean wasOpened = false;
        Transaction tx = null;
        try{
            wasOpened = DSApi.openContextIfNeeded();
            Session session = DSApi.context().session();
            tx = session.beginTransaction();

            session.saveOrUpdate(this);

            tx.commit();

        }catch (EdmException e){
            if(tx != null){
                tx.rollback();
            }
            DSApi.closeContextIfNeeded(wasOpened);
            throw e;
        }
    }

    public void delete() throws EdmException{
        boolean wasOpened = false;
        Transaction tx = null;
        try{
            wasOpened = DSApi.openContextIfNeeded();
            Session session = DSApi.context().session();
            tx = session.beginTransaction();

            session.delete(this);

            tx.commit();

        }catch (EdmException e){
            if(tx != null){
                tx.rollback();
            }
            DSApi.closeContextIfNeeded(wasOpened);
            throw e;
        }
    }


    public static Map<String,String> getMapToDocFields(){
        Map<String, String> mapping = new LinkedHashMap<String, String>();
        mapping.put("budgetKind","BUDGET_KIND");
        mapping.put("budget","BUDGET");
        mapping.put("position","POSITION");
        mapping.put("source","SOURCE");
        mapping.put("contract","CONTRACT");
        mapping.put("contractStage","CONTRACT_STAGE");
        mapping.put("fund","FUND");
        mapping.put("projectBudget","PROJECT_BUDGET");
        mapping.put("budgetKo","BUDGET_KO");
        mapping.put("financialTask","FINANCIAL_TASK");



        return mapping;
    }

    //__________________GETTERS & SETTERS_______________________

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBudgetKind() {
        return budgetKind;
    }

    public void setBudgetKind(Long budgetKind) {
        this.budgetKind = budgetKind;
    }

    public Long getBudget() {
        return budget;
    }

    public void setBudget(Long budget) {
        this.budget = budget;
    }

    public Long getPosition() {
        return position;
    }

    public void setPosition(Long position) {
        this.position = position;
    }

    public Long getSource() {
        return source;
    }

    public void setSource(Long source) {
        this.source = source;
    }

    public Long getContract() {
        return contract;
    }

    public void setContract(Long contract) {
        this.contract = contract;
    }

    public Long getContractStage() {
        return contractStage;
    }

    public void setContractStage(Long contractStage) {
        this.contractStage = contractStage;
    }

    public Long getFund() {
        return fund;
    }

    public void setFund(Long fund) {
        this.fund = fund;
    }

    public Long getProjectBudget() {
        return projectBudget;
    }

    public void setProjectBudget(Long projectBudget) {
        this.projectBudget = projectBudget;
    }

    public Long getBudgetKo() {
        return budgetKo;
    }

    public void setBudgetKo(Long budgetKo) {
        this.budgetKo = budgetKo;
    }

    public Long getFinancialTask() {
        return financialTask;
    }

    public void setFinancialTask(Long financialTask) {
        this.financialTask = financialTask;
    }

    public Integer getPositionNumber() {
        return positionNumber;
    }

    public void setPositionNumber(Integer positionNumber) {
        this.positionNumber = positionNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ZrodloFinansowaniaDict that = (ZrodloFinansowaniaDict) o;

        if (budget != null ? !budget.equals(that.budget) : that.budget != null) return false;
        if (budgetKind != null ? !budgetKind.equals(that.budgetKind) : that.budgetKind != null) return false;
        if (budgetKo != null ? !budgetKo.equals(that.budgetKo) : that.budgetKo != null) return false;
        if (contract != null ? !contract.equals(that.contract) : that.contract != null) return false;
        if (contractStage != null ? !contractStage.equals(that.contractStage) : that.contractStage != null)
            return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (financialTask != null ? !financialTask.equals(that.financialTask) : that.financialTask != null)
            return false;
        if (fund != null ? !fund.equals(that.fund) : that.fund != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (position != null ? !position.equals(that.position) : that.position != null) return false;
        if (projectBudget != null ? !projectBudget.equals(that.projectBudget) : that.projectBudget != null)
            return false;
        if (source != null ? !source.equals(that.source) : that.source != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (budgetKind != null ? budgetKind.hashCode() : 0);
        result = 31 * result + (budget != null ? budget.hashCode() : 0);
        result = 31 * result + (position != null ? position.hashCode() : 0);
        result = 31 * result + (source != null ? source.hashCode() : 0);
        result = 31 * result + (contract != null ? contract.hashCode() : 0);
        result = 31 * result + (contractStage != null ? contractStage.hashCode() : 0);
        result = 31 * result + (fund != null ? fund.hashCode() : 0);
        result = 31 * result + (projectBudget != null ? projectBudget.hashCode() : 0);
        result = 31 * result + (budgetKo != null ? budgetKo.hashCode() : 0);
        result = 31 * result + (financialTask != null ? financialTask.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}
