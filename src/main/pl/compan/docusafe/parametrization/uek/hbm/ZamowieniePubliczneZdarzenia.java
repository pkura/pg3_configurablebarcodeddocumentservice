package pl.compan.docusafe.parametrization.uek.hbm;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

import javax.persistence.*;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
@Entity(name = "uek.ZamowieniePubliczneZdarzenia")
@Table(name = "DSG_UEK_ZAM_PUB_ZDARZENIA")
public class ZamowieniePubliczneZdarzenia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;

    @Column(name = "ENTRYID")
    Long userId;

    @Column(name = "DATE")
    Long date;

    @Column(name = "DESCRIPTION")
    Long description;



    public static ZamowieniePubliczneZdarzenia findById(Long wnioskujacyId) throws EdmException {

        Criteria criteria = DSApi.context().session().createCriteria(ZamowieniePubliczneZdarzenia.class);
        criteria.add(Restrictions.idEq(wnioskujacyId));

        return (ZamowieniePubliczneZdarzenia)criteria.uniqueResult();

    }

}
