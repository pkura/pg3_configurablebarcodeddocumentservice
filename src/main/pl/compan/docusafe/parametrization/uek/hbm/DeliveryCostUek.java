/**
 * 
 */
package pl.compan.docusafe.parametrization.uek.hbm;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.news.Notification;
import pl.compan.docusafe.core.users.DSUser;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 24-01-2014
 * ds_trunk_2014
 * DeliveryCostUek.java
 */
public class DeliveryCostUek {

	Long id;
	String waga;
	Integer zpo;
	Integer poziomGabarytu;
	Integer poziomListu;
	Integer poziomWagi;
	BigDecimal kwota;
	
	public static final int KRAJOWY_ZWYKLY_EKO = 1;
    public static final int KRAJOWY_ZWYKLY_PRIO = 2;
    public static final int KRAJOWY_POLECONY_EKO = 3;
    public static final int KRAJOWY_POLECONY_PRIO = 4;
    public static final int PRZESYLKI_REKL = 5;
    public static final int ZAGRANICZNY_ZWYKLY_EKO = 6;
    public static final int ZAGRANICZNY_ZWYKLY_PRIO = 7;
    //public static final int ZAGRANICZNY_POLECONY_EKO = 8;
    public static final int ZAGRANICZNY_POLECONY_PRIO = 9;
    public static final int PACZKA = 10;
    public static final int EPUAP = 11;
    public static final int ANULOWANO = 12;
    
    public static final int WEIGTH_0_50 = 1;
    public static final int WEIGTH_50_100 = 2;
    public static final int WEIGTH_100_350 = 3;
    public static final int WEIGTH_350_500 = 4;
    public static final int WEIGTH_500_1000 = 5;
    public static final int WEIGTH_0_350 = 6;
    public static final int WEIGTH_350_1000 = 7;
    public static final int WEIGTH_1000_2000 = 8;
    public static final int WEIGTH_2000_2500 = 9;
    public static final int WEIGTH_2500_3000 = 10;
    public static final int WEIGTH_3000_3500 = 11;
    public static final int WEIGTH_3500_4000 = 12;
    public static final int WEIGTH_4000_4500 = 13;
    public static final int WEIGTH_4500_5000 = 14;

	
	public DeliveryCostUek() {
		
	}
	
	public DeliveryCostUek(Long id, String waga, Integer zpo,
			Integer poziomGabarytu, Integer poziomListu, Integer poziomWagi,
			BigDecimal kwota) {
		this.id = id;
		this.waga = waga;
		this.zpo = zpo;
		this.poziomGabarytu = poziomGabarytu;
		this.poziomListu = poziomListu;
		this.poziomWagi = poziomWagi;
		this.kwota = kwota;
	}

	public static DeliveryCostUek find(Long id) throws EdmException
	{
		  return (DeliveryCostUek)Finder.find(DeliveryCostUek.class, id);
	}
	
	
	public static DeliveryCostUek find
		(Integer zpo, Integer poziomGabarytu, Integer poziomListu, Integer poziomWagi) throws EdmException
	{
    	Criteria crit = DSApi.context().session().createCriteria(DeliveryCostUek.class);
    	crit.add(Restrictions.eq("poziomWagi", poziomWagi));
    	crit.add(Restrictions.eq("poziomGabarytu", poziomGabarytu));
    	crit.add(Restrictions.eq("poziomListu", poziomListu));
    	crit.add(Restrictions.eq("zpo", zpo));
    	
    	if(crit.list() != null && crit.list().size() > 0){
        	return (DeliveryCostUek)crit.list().get(0);
    	} else {
    		return null;
    	}
	}
	
	public void create() throws EdmException 
	{
		 try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();	   
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
		
	}
	
	public void update() throws EdmException 
	{
		 try 
        {
            DSApi.context().session().update(this);
            DSApi.context().session().flush();	   
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
		
	}
	
	public void delete() throws EdmException 
	{
		 try 
        {
            DSApi.context().session().delete(this);
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
		
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getWaga() {
		return waga;
	}
	public void setWaga(String waga) {
		this.waga = waga;
	}
	public Integer getPoziomGabarytu() {
		return poziomGabarytu;
	}
	public void setPoziomGabarytu(Integer poziomGabarytu) {
		this.poziomGabarytu = poziomGabarytu;
	}
	public Integer getPoziomListu() {
		return poziomListu;
	}
	public void setPoziomListu(Integer poziomListu) {
		this.poziomListu = poziomListu;
	}
	public Integer getPoziomWagi() {
		return poziomWagi;
	}
	public void setPoziomWagi(Integer poziomWagi) {
		this.poziomWagi = poziomWagi;
	}
	public BigDecimal getKwota() {
		return kwota;
	}
	public void setKwota(BigDecimal kwota) {
		this.kwota = kwota;
	}



	public Integer getZpo() {
		return zpo;
	}



	public void setZpo(Integer zpo) {
		this.zpo = zpo;
	}
	
	
}
