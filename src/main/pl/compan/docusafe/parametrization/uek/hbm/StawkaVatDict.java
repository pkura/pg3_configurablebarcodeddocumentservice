package pl.compan.docusafe.parametrization.uek.hbm;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;

import javax.annotation.Generated;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Damian on 12.03.14.
 */


@Entity(name = "uek.StawkaVatDict")
@Table(name = "dsg_uek_stawki_vat")
public class StawkaVatDict {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "NET_AMOUNT")
    private BigDecimal netAmount;

    @Column(name = "VAT_RATE")
    private Long vatRate;

    @Column(name = "VAT_AMOUNT")
    private BigDecimal vatAmount;

    @Column(name = "GROSS_AMOUNT")
    private BigDecimal grossAmount;

    @Column(name = "PURPOSE")
    private Long purpose;

    @Column(name ="POSITION_NUMBER")
    private Integer positionNumber;


    public static StawkaVatDict findById(Long stawkaId) throws EdmException {

            Criteria criteria = DSApi.context().session().createCriteria(StawkaVatDict.class);
            criteria.add(Restrictions.idEq(stawkaId));

            return (StawkaVatDict)criteria.uniqueResult();

    }


    public static Map<String,String> getMapToDocFields(){
        Map<String, String> mapping = new LinkedHashMap<String, String>();
        mapping.put("netAmount","NET_AMOUNT");
        mapping.put("vatRate","VAT_RATE");
        mapping.put("vatAmount","VAT_AMOUNT");
        mapping.put("grossAmount","GROSS_AMOUNT");
        mapping.put("purpose","PURPOSE");
        return mapping;
    }


    public void save() throws EdmException {
        boolean wasOpened = false;
        Transaction tx = null;
        try{
            wasOpened = DSApi.openContextIfNeeded();
            Session session = DSApi.context().session();
            tx = session.beginTransaction();

            session.saveOrUpdate(this);

            tx.commit();

        }catch (EdmException e){
            if(tx != null){
                tx.rollback();
            }
            DSApi.closeContextIfNeeded(wasOpened);
            throw e;
        }
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(BigDecimal netAmount) {
        this.netAmount = netAmount;
    }

    public Long getVatRate() {
        return vatRate;
    }

    public void setVatRate(Long vatRate) {
        this.vatRate = vatRate;
    }

    public BigDecimal getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(BigDecimal vatAmount) {
        this.vatAmount = vatAmount;
    }

    public BigDecimal getGrossAmount() {
        return grossAmount;
    }

    public void setGrossAmount(BigDecimal grossAmount) {
        this.grossAmount = grossAmount;
    }

    public Long getPurpose() {
        return purpose;
    }

    public void setPurpose(Long purpose) {
        this.purpose = purpose;
    }

    public Integer getPositionNumber() {
        return positionNumber;
    }

    public void setPositionNumber(Integer positionNumber) {
        this.positionNumber = positionNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StawkaVatDict that = (StawkaVatDict) o;

        if (grossAmount != null ? !grossAmount.equals(that.grossAmount) : that.grossAmount != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (netAmount != null ? !netAmount.equals(that.netAmount) : that.netAmount != null) return false;
        if (positionNumber != null ? !positionNumber.equals(that.positionNumber) : that.positionNumber != null)
            return false;
        if (purpose != null ? !purpose.equals(that.purpose) : that.purpose != null) return false;
        if (vatAmount != null ? !vatAmount.equals(that.vatAmount) : that.vatAmount != null) return false;
        if (vatRate != null ? !vatRate.equals(that.vatRate) : that.vatRate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (netAmount != null ? netAmount.hashCode() : 0);
        result = 31 * result + (vatRate != null ? vatRate.hashCode() : 0);
        result = 31 * result + (vatAmount != null ? vatAmount.hashCode() : 0);
        result = 31 * result + (grossAmount != null ? grossAmount.hashCode() : 0);
        result = 31 * result + (purpose != null ? purpose.hashCode() : 0);
        result = 31 * result + (positionNumber != null ? positionNumber.hashCode() : 0);
        return result;
    }
}
