/**
 * 
 */
package pl.compan.docusafe.parametrization.uek.hbm;

import java.math.BigDecimal;
import java.util.Date;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;


/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 09-05-2014
 * ds_trunk_2014
 * CurrencyDeposit.java
 */

public class CurrencyDeposit {

	Long id;
	Integer sposobWyplaty;
	BigDecimal moneyValue;
	Long waluta;
	Date currencyRateDate;
	BigDecimal currencyRate;
	BigDecimal moneyValuePln;
	
	public CurrencyDeposit(){
		
	}
	
	public static CurrencyDeposit find(Long id) throws EdmException
	{
		  return (CurrencyDeposit)Finder.find(CurrencyDeposit.class, id);
	}
	
	public void create() throws EdmException 
	{
		 try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();	   
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
		
	}
	
	public void update() throws EdmException 
	{
		 try 
        {
            DSApi.context().session().update(this);
            DSApi.context().session().flush();	   
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
		
	}
	
	public void delete() throws EdmException 
	{
		 try 
        {
            DSApi.context().session().delete(this);
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getMoneyValue() {
		return moneyValue;
	}

	public void setMoneyValue(BigDecimal moneyValue) {
		this.moneyValue = moneyValue;
	}

	public Date getCurrencyRateDate() {
		return currencyRateDate;
	}

	public void setCurrencyRateDate(Date currencyRateDate) {
		this.currencyRateDate = currencyRateDate;
	}

	public BigDecimal getCurrencyRate() {
		return currencyRate;
	}

	public void setCurrencyRate(BigDecimal currencyRate) {
		this.currencyRate = currencyRate;
	}

	public BigDecimal getMoneyValuePln() {
		return moneyValuePln;
	}

	public void setMoneyValuePln(BigDecimal moneyValuePln) {
		this.moneyValuePln = moneyValuePln;
	}

	public Long getWaluta() {
		return waluta;
	}

	public void setWaluta(Long waluta) {
		this.waluta = waluta;
	}

	public Integer getSposobWyplaty() {
		return sposobWyplaty;
	}

	public void setSposobWyplaty(Integer sposobWyplaty) {
		this.sposobWyplaty = sposobWyplaty;
	}
	
	
}
