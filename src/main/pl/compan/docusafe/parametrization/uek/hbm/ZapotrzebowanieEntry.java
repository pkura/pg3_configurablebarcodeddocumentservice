package pl.compan.docusafe.parametrization.uek.hbm;


import org.apache.xpath.operations.Bool;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.*;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.*;
/**
 * Created by Raf on 2014-05-07.
 */

@Entity(name = "uek.ZapotrzebowanieEntry")
@Table(name = "dsg_uek_slownik_przedmiotow_zapotrzebowania")
public class ZapotrzebowanieEntry {



        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "ID")
        private Long id;
        @Column(name = "NAZWA_PRZEDMIOTU", length = 250)
        private String nazwaPrzedmiotu;
        @Column(name = "PRODUKTY")
        private Long produkty;
        @Column(name = "ILOSC")//quantity
        private Integer ilosc;
        @Column(name = "WARTOSC_NETTO") //ws kwota
        private BigDecimal wartoscNetto;
        @Column(name = "RODZAJ_DZIALALNOSCI")
        private Long rodzajDzialalnosci;
        @Column(name = "DATA_POZYSKANIA") //
        private Date dataPozyskania;
        @Column(name = "SRODKI_BOOL") //
        private Boolean srodkiBool;
        @Column(name = "SRODKI_STRING") //
        private String srodkiString;
        @Column(name = "PRZEZNACZENIE_ZAKUPU")
        private Long przeznaczenieZakupu;

        @Column(name = "DESCRIPTION")
        private String description;

        @OneToOne
        @Cascade({org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
        @JoinColumn(name="ZRODLO_ID")
        ZrodloFinansowaniaDict zrodloFinansowania;

        public void save() throws EdmException {
            boolean wasOpened = false;
            Transaction tx = null;
            try{
                wasOpened = DSApi.openContextIfNeeded();
                Session session = DSApi.context().session();
                tx = session.beginTransaction();

                session.saveOrUpdate(this);

                tx.commit();

            }catch (EdmException e){
                if(tx != null){
                    tx.rollback();
                }
                DSApi.closeContextIfNeeded(wasOpened);
                throw e;
            }
        }

        public void delete() throws EdmException{
            boolean wasOpened = false;
            Transaction tx = null;
            try{
                wasOpened = DSApi.openContextIfNeeded();
                Session session = DSApi.context().session();
                tx = session.beginTransaction();

                session.delete(this);

                tx.commit();

            }catch (EdmException e){
                if(tx != null){
                    tx.rollback();
                }
                DSApi.closeContextIfNeeded(wasOpened);
                throw e;
            }
        }

        public static ZapotrzebowanieEntry findById(Long entryId) throws EdmException {

            Criteria criteria = DSApi.context().session().createCriteria(ZapotrzebowanieEntry.class);
            criteria.add(Restrictions.idEq(entryId));

            return (ZapotrzebowanieEntry)criteria.uniqueResult();
        }

        public static Map<String,String> getMapToDocFields(){
            Map<String, String> mapping = new LinkedHashMap<String, String>();
            mapping.put("nazwaPrzedmiotu","NAZWA_PRZEDMIOTU");
            mapping.put("produkty","PRODUKTY");
            mapping.put("dataPozyskania","DATA_POZYSKANIA");
            mapping.put("rodzajDzialalnosci","RODZAJ_DZIALALNOSCI");
            mapping.put("ilosc","ILOSC");
            mapping.put("wartoscNetto","WARTOSC_NETTO");
            mapping.put("przeznaczenieZakupu","PRZEZNACZENIE_ZAKUPU");
            mapping.put("srodkiBool","SRODKI_BOOL");
            mapping.put("srodkiString","SRODKI_STRING");

            return mapping;
        }


        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getNazwaPrzedmiotu() {
            return nazwaPrzedmiotu;
        }

        public void setNazwaPrzedmiotu(String nazwaPrzedmiotu) {
            this.nazwaPrzedmiotu = nazwaPrzedmiotu;
        }

        public Long getProdukty() {
            return produkty;
        }

        public void setProdukty(Long produkty) {
            this.produkty = produkty;
        }



        public Date getDataPozyskania() {
            return dataPozyskania;
        }

        public void setDataPozyskania(Date dataPozyskania) {
            this.dataPozyskania = dataPozyskania;
        }

        public Long getRodzajDzialalnosci() {
            return rodzajDzialalnosci;
        }

        public void setRodzajDzialalnosci(Long rodzajDzialalnosci) {
            this.rodzajDzialalnosci = rodzajDzialalnosci;
        }

        public Integer getIlosc() {
            return ilosc;
        }

        public void setIlosc(Integer ilosc) {
            this.ilosc = ilosc;
        }

        public BigDecimal getWartoscNetto() {
            return wartoscNetto;
        }

        public void setWartoscNetto(BigDecimal wartoscNetto) {
            this.wartoscNetto = wartoscNetto;
        }



    public Long getPrzeznaczenieZakupu() {
        return przeznaczenieZakupu;
    }

    public void setPrzeznaczenieZakupu(Long przeznaczenieZakupu) {
        this.przeznaczenieZakupu = przeznaczenieZakupu;
    }

    public String getSrodkiString() {
        return srodkiString;
    }

    public void setSrodkiString(String srodkiString) {
        this.srodkiString = srodkiString;
    }

    public Boolean getSrodkiBool() {
        return srodkiBool;
    }

    public void setSrodkiBool(Boolean srodkiBool) {
        this.srodkiBool = srodkiBool;
    }

    public ZrodloFinansowaniaDict getZrodloFinansowania() {
        return zrodloFinansowania;
    }

    public void setZrodloFinansowania(ZrodloFinansowaniaDict zrodloFinansowania) {
        this.zrodloFinansowania = zrodloFinansowania;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
