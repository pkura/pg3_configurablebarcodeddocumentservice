package pl.compan.docusafe.parametrization.uek.hbm;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

import javax.persistence.*;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
@Entity(name = "uek.CpvDict")
@Table(name = "DSG_UEK_ZAM_PUB_CPV")
public class CpvDict {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;

    @Column(name = "WN_USER")
    Long userId;

    @Column(name = "WN_DIVISION")
    Long divisionId;



    public static CpvDict findById(Long wnioskujacyId) throws EdmException {

        Criteria criteria = DSApi.context().session().createCriteria(CpvDict.class);
        criteria.add(Restrictions.idEq(wnioskujacyId));

        return (CpvDict)criteria.uniqueResult();

    }


    public static Map<String,String> getMapToDocFields(){
        Map<String, String> mapping = new LinkedHashMap<String, String>();
        mapping.put("userId","WN_USER");
        mapping.put("divisionId","WN_DIVISION");
        return mapping;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(Long divisionId) {
        this.divisionId = divisionId;
    }
}
