package pl.compan.docusafe.parametrization.uek.hbm;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.MoneyField;
import pl.compan.docusafe.core.dockinds.field.reader.MoneyFieldReader;

import javax.persistence.*;
import java.awt.geom.RoundRectangle2D;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Damian on 12.03.14.
 */

@Entity(name = "uek.ZrodloFinansowaniaDict2")
@Table(name = "dsg_uek_slownik_przedmiotow_zapotrzebowania")
public class ZrodloFinansowaniaDict2 {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "BUDGET_KIND")
    private Integer budgetKind;

    @Column(name = "BUDGET")
    private Long budget;

    @Column(name = "POSITION")
    private Long position;

    @Column(name = "SOURCE")
    private Long source;

    @Column(name = "CONTRACT")
    private Long contract;

    @Column(name = "CONTRACT_STAGE")
    private Long contractStage;

    @Column(name = "FUND")
    private Long fund;

    @Column(name ="PROJECT_BUDGET")
    private Long projectBudget;

    @Column(name ="ZRODLO_DESCRIPTION")
    private String description;

    @Column(name ="PRODUKTY")
    private Long produkty;

    @Column(name ="NAZWA_ZAMOWIENIA")
    private String nazwaZamowienia;

    @Column(name ="ILOSC")
    private Long ilosc;

    @Column(name ="WARTOSC_NETTO")
    private Float wartoscNetto;

    @Column(name ="RODZAJ_DZIALALNOSCI")
    private Long rodzajDzialalnosci;

    @Column(name ="TERMIN")
    private Date termin;

    @Column(name ="PRZEZNACZENIE_ZAKUPU")
    private Long przeznaczenieZakupu;





    @OneToMany
    @Cascade({CascadeType.ALL,CascadeType.DELETE_ORPHAN})
    @JoinColumn(name="ZRODLO_ID")


    public static ZrodloFinansowaniaDict2 findById(Long zrodloId) throws EdmException {

        Criteria criteria = DSApi.context().session().createCriteria(ZrodloFinansowaniaDict2.class);
        criteria.add(Restrictions.idEq(zrodloId));

        return (ZrodloFinansowaniaDict2)criteria.uniqueResult();
    }


    public void save() throws EdmException {
        boolean wasOpened = false;
        Transaction tx = null;
        try{
            wasOpened = DSApi.openContextIfNeeded();
            Session session = DSApi.context().session();
            tx = session.beginTransaction();

            session.saveOrUpdate(this);

            tx.commit();

        }catch (EdmException e){
            if(tx != null){
                tx.rollback();
            }
            DSApi.closeContextIfNeeded(wasOpened);
            throw e;
        }finally {
            DSApi.closeContextIfNeeded(wasOpened);
        }
    }

    public void delete() throws EdmException{
        boolean wasOpened = false;
        Transaction tx = null;
        try{
            wasOpened = DSApi.openContextIfNeeded();
            Session session = DSApi.context().session();
            tx = session.beginTransaction();

            session.delete(this);

            tx.commit();

        }catch (EdmException e){
            if(tx != null){
                tx.rollback();
            }
            DSApi.closeContextIfNeeded(wasOpened);
            throw e;
        }
    }


    //__________________GETTERS & SETTERS_______________________
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getBudgetKind() {
        return budgetKind;
    }

    public void setBudgetKind(Integer budgetKind) {
        this.budgetKind = budgetKind;
    }

    public Long getBudget() {
        return budget;
    }

    public void setBudget(Long budget) {
        this.budget = budget;
    }

    public Long getPosition() {
        return position;
    }

    public void setPosition(Long position) {
        this.position = position;
    }

    public Long getSource() {
        return source;
    }

    public void setSource(Long source) {
        this.source = source;
    }

    public Long getContract() {
        return contract;
    }

    public void setContract(Long contract) {
        this.contract = contract;
    }

    public Long getContractStage() {
        return contractStage;
    }

    public void setContractStage(Long contractStage) {
        this.contractStage = contractStage;
    }

    public Long getFund() {
        return fund;
    }

    public void setFund(Long fund) {
        this.fund = fund;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setProjectBudget(Long projectBudget) {
        this.projectBudget = projectBudget;
    }

    public Long getProjectBudget() {
        return projectBudget;
    }

    public void setProdukty(Long produkty) {
        this.produkty = produkty;
    }

    public Long getProdukty() {
        return produkty;
    }


    public void setPrzeznaczenieZakupu(Long przeznaczenieZakupu) {
        this.przeznaczenieZakupu = przeznaczenieZakupu;
    }

    public Long getPrzeznaczenieZakupu() {
        return przeznaczenieZakupu;
    }



    public void setRodzajDzialalnosci(Long rodzajDzialalnosci) {
        this.rodzajDzialalnosci = rodzajDzialalnosci;
    }

    public Long getRodzajDzialalnosci() {
        return rodzajDzialalnosci;
    }


    public String getNazwaZamowienia() {
        return nazwaZamowienia;
    }

    public void setNazwaZamowienia(String nazwaZamowienia) {
        this.nazwaZamowienia = nazwaZamowienia;
    }


    public void setIlosc(Long ilosc) {
        this.ilosc = ilosc;
    }

    public Long getIlosc() {
        return ilosc;
    }


    public void setWartoscNetto( Float wartoscNetto) {
        this.wartoscNetto = wartoscNetto;
    }

    public Float getWartoscNetto() {
        return wartoscNetto;
    }


    public void setTermin(Date termin) {
        this.termin = termin;
    }

    public Date getTermin() {
        return termin;
    }








}
