package pl.compan.docusafe.parametrization.uek.hbm;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.*;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by Damian on 29.04.14.
 */

@Entity(name = "uek.InvoiceEntry")
@Table(name = "dsg_uek_invoice_entry")
public class InvoiceEntry extends ErpPositionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "CZY_MAGAZYNOWA")
    private Long czyMagazynowa;
    @Column(name = "PRODUCT")
    private Long product;
    @Column(name = "PRODUCTION_ORDER")
    private Long productionOrder;
    @Column(name = "MPK")
    private Long mpk;
    @Column(name = "PRZED_WYKONANIEM")
    private Long przedWykonaniem;
    @Column(name = "DATA_WYKONANIA")
    private Date dataWykonania;
    @Column(name = "RODZAJ_ZAMOWIENIA")
    private Long rodzajZamowienia;
    @Column(name = "QUANTITY")
    private Integer quantity;
    @Column(name = "UNIT_OF_MEASURE")
    private Long unitOfMeasure;
    @Column(name = "WS_KOD", length = 250)
    private String wsKod;
    @Column(name = "WS_OBSZAR", length = 250)
    private String wsObszar;
    @Column(name = "WS_KWOTA")
    private BigDecimal wsKwota;
    @Column(name = "ERP_DOCUMENT_IDM", length = 50)
    private String erpDocumentIdm;


    @Column(name = "DESCRIPTION")
    private String description;

    @OneToOne
    @Cascade({org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    @JoinColumn(name="REPO_DICT_ENTRY_ID")
    RepositoryDictEntry repositoryDictEntry;

    @OneToOne
    @Cascade({org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    @JoinColumn(name="ZRODLO_ID")
    ZrodloFinansowaniaDict zrodloFinansowania;

    @OneToMany
    @Cascade({org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    @JoinTable(name = "dsg_uek_invoice_to_vat", joinColumns = { @JoinColumn(name = "INVOICE_ENTRY_ID") }, inverseJoinColumns = { @JoinColumn(name = "STAWKA_ID") })
    Set<StawkaVatDict> stawkaVatDicts = new HashSet<StawkaVatDict>();


    @OneToMany
    @Cascade({org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    @JoinTable(name = "dsg_uek_invoice_to_wnioskujacy", joinColumns = { @JoinColumn(name = "INVOICE_ENTRY_ID") }, inverseJoinColumns = { @JoinColumn(name = "WNIOSKUJACY_ID") })
    Set<WnioskujacyDict> wniokujacy = new HashSet<WnioskujacyDict>();

    public static InvoiceEntry findById(Long entryId) throws EdmException {
        return findById(entryId,InvoiceEntry.class);
    }

    public static Map<String,String> getMapToDocFields(){
        Map<String, String> mapping = new LinkedHashMap<String, String>();
        mapping.put("czyMagazynowa","CZY_MAGAZYNOWA");
        mapping.put("product","PRODUCT");
        mapping.put("productionOrder","PRODUCTION_ORDER");
        mapping.put("mpk","MPK");
        mapping.put("przedWykonaniem","PRZED_WYKONANIEM");
        mapping.put("dataWykonania","DATA_WYKONANIA");
        mapping.put("rodzajZamowienia","RODZAJ_ZAMOWIENIA");
        mapping.put("quantity","QUANTITY");
        mapping.put("unitOfMeasure","UNIT_OF_MEASURE");
        mapping.put("wsKod","WS_KOD");
        mapping.put("wsObszar","WS_OBSZAR");
        mapping.put("wsKwota","WS_KWOTA");
        return mapping;
    }


    @Override
    public Integer getPositionNumber() {
        return null;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCzyMagazynowa() {
        return czyMagazynowa;
    }

    public void setCzyMagazynowa(Long czyMagazynowa) {
        this.czyMagazynowa = czyMagazynowa;
    }

    public Long getProduct() {
        return product;
    }

    public void setProduct(Long product) {
        this.product = product;
    }

    public Long getProductionOrder() {
        return productionOrder;
    }

    public void setProductionOrder(Long productionOrder) {
        this.productionOrder = productionOrder;
    }

    public Long getMpk() {
        return mpk;
    }

    public void setMpk(Long mpk) {
        this.mpk = mpk;
    }

    public Long getPrzedWykonaniem() {
        return przedWykonaniem;
    }

    public void setPrzedWykonaniem(Long przedWykonaniem) {
        this.przedWykonaniem = przedWykonaniem;
    }

    public Date getDataWykonania() {
        return dataWykonania;
    }

    public void setDataWykonania(Date dataWykonania) {
        this.dataWykonania = dataWykonania;
    }

    public Long getRodzajZamowienia() {
        return rodzajZamowienia;
    }

    public void setRodzajZamowienia(Long rodzajZamowienia) {
        this.rodzajZamowienia = rodzajZamowienia;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Long getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(Long unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public String getWsKod() {
        return wsKod;
    }

    public void setWsKod(String wsKod) {
        this.wsKod = wsKod;
    }

    public String getWsObszar() {
        return wsObszar;
    }

    public void setWsObszar(String wsObszar) {
        this.wsObszar = wsObszar;
    }

    public BigDecimal getWsKwota() {
        return wsKwota;
    }

    public void setWsKwota(BigDecimal wsKwota) {
        this.wsKwota = wsKwota;
    }

    public ZrodloFinansowaniaDict getZrodloFinansowania() {
        return zrodloFinansowania;
    }

    public void setZrodloFinansowania(ZrodloFinansowaniaDict zrodloFinansowania) {
        this.zrodloFinansowania = zrodloFinansowania;
    }

    public Set<StawkaVatDict> getStawkaVatDicts() {
        return stawkaVatDicts;
    }

    public void setStawkaVatDicts(Set<StawkaVatDict> stawkaVatDict) {
        this.stawkaVatDicts = stawkaVatDict;
    }

    public Set<WnioskujacyDict> getWniokujacy() {
        return wniokujacy;
    }

    public void setWniokujacy(Set<WnioskujacyDict> wniokujacy) {
        this.wniokujacy = wniokujacy;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RepositoryDictEntry getRepositoryDictEntry() {
        return repositoryDictEntry;
    }

    public void setRepositoryDictEntry(RepositoryDictEntry repositoryDictEntry) {
        this.repositoryDictEntry = repositoryDictEntry;
    }

    public String getErpDocumentIdm() {
        return erpDocumentIdm;
    }

    public void setErpDocumentIdm(String erpDocumentIdm) {
        this.erpDocumentIdm = erpDocumentIdm;
    }
}
