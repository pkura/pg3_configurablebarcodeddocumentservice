package pl.compan.docusafe.parametrization.uek;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ExecutionQuery;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.uek.utils.InvoicePopupManagerInwest;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;
/**
 * Created by Raf on 2014-04-14.
 */
public class WniosekInwestycyjnyLogic extends AbstractDocumentLogic {
    private final static Logger log = LoggerFactory.getLogger(WniosekInwestycyjnyLogic.class);
    private final static String DWR_PREFIX = "DWR_";
    public static final String DOCKIND_CN = "wniosek_inwestycyjny";
    public static final String TO_ZAPOTRZEBOWANIE2 = "TO_ZAPOTRZEBOWANIE";


    private final static String EDIT_BUTTON = "EDIT";
    private final static String VIEW_BUTTON = "VIEW";
    private static final String DEL_BUTTON = "DEL";
    private final static String ADD_BUTTON = "ADD";
    private final static String SAVE_BUTTON = "SAVE";
    private final static String CANCEL_BUTTON = "CANCEL";

    private final static String JEDNOSTKA_REALIZUJACA = "DWR_JEDNOSTKA_REALIZUJACA";
    private static final String WNIOSKODAWCA = "WNIOSKODAWCA";
    private static final String WNIOSKODAWCA_JEDNOSTKA = "JEDNOSTKA_ZGLASZAJACA";
    public static final String TO_ZAPOTRZEBOWANIE = "DWR_TO_ZAPOTRZEBOWANIE";



    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {


        try {

            Long documentId = document.getId();
            FieldsManager fm = document.getFieldsManager();
            DocumentKind zapotrzebowanie = DocumentKind.findByCn(ZapotrzebowanieLogic.DOCKIND_CN);
            List<Long> ss = (List<Long>) fm.getFieldValues().get(TO_ZAPOTRZEBOWANIE2);
            if(ss!=null && ss.size()>0) {
                for (Long id : ss) {

                    zapotrzebowanie.setOnly(id, ImmutableMap.of("TO_WINWEST", documentId));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);


        }
    }


    @Override
    public void documentPermissions(Document document) throws EdmException {

    }

    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
        try {
            Map<String, Object> map = Maps.newHashMap();

            map.put(ASSIGN_USER_PARAM,document.getCreatingUser());

            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
            java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

            DSUser author = DSUser.findByUsername(document.getCreatingUser());
            String user = document.getCreatingUser();
            String fullName = author.asFirstnameLastname();

            perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

            Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
            perms.addAll(documentPermissions);
            this.setUpPermission(document, perms);

            if (AvailabilityManager.isAvailable("addToWatch")) {
                DSApi.context().watch(URN.create(document));
            }
        }  catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new EdmException(e.getMessage());
        }
    }

   @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();
        DSUser user = DSApi.context().getDSUser();
        toReload.put(WNIOSKODAWCA, user.getId());
        toReload.put(WNIOSKODAWCA_JEDNOSTKA, user.getDivisionsWithoutGroup().length > 0 ? user.getDivisionsWithoutGroup()[0].getId() : null);
        fm.reloadValues(toReload);
    }
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        StringBuilder msgBuilder = new StringBuilder();

        try {boolean wasOpened = false;
            //Popup Zrodla Finansowania
            if(DwrUtils.isSenderField(values,DWR_PREFIX+ADD_BUTTON,DWR_PREFIX+CANCEL_BUTTON)){
                InvoicePopupManagerInwest.clearPopup(values, fm);
            }else if (DwrUtils.isSenderField(values,DWR_PREFIX+SAVE_BUTTON)) {
                InvoicePopupManagerInwest.saveEntry(values, fm);
                InvoicePopupManagerInwest.clearPopup(values, fm);
            }else if(DwrUtils.isSenderField(values,DWR_PREFIX+  InvoicePopupManagerInwest.ENTRY_DICTIONARY)) {
                String entryButtonName = DwrUtils.senderButtonFromDictionary(values,  InvoicePopupManagerInwest.ENTRY_DICTIONARY, EDIT_BUTTON);
                if (entryButtonName == null) {
                    entryButtonName = DwrUtils.senderButtonFromDictionary(values, InvoicePopupManagerInwest.ENTRY_DICTIONARY, VIEW_BUTTON);
                }
                //klikniÍto edit lub view button
                if (entryButtonName != null) {
                    Integer rowNo = Integer.parseInt(entryButtonName.substring(entryButtonName.lastIndexOf("_") + 1));
                    InvoicePopupManagerInwest.updatePopup(values, fm, rowNo);

                }
                //klikniÍto delete button
                else{
                    String delButtonName = DwrUtils.senderButtonFromDictionary(values, InvoicePopupManagerInwest.ENTRY_DICTIONARY, DEL_BUTTON);
                    if(delButtonName != null){
                        Integer rowNo = Integer.parseInt(delButtonName.substring(delButtonName.lastIndexOf("_") + 1));
                        InvoicePopupManagerInwest.deleteEntry(values, fm, rowNo);
                    }
                } }

            // czysci poprzednie pole realizatora jezeli zostanie poprawiony
            else if (DwrUtils.isSenderField(values,JEDNOSTKA_REALIZUJACA)) {
                    try {
                        wasOpened = DSApi.openContextIfNeeded();
                        Long doc = fm.getDocumentId();

                        if(doc!=null){
                        String sql = "delete from dso_person where discriminator ='recipient' and document_id="
                                + doc.toString();

                        PreparedStatement ps = null;
                        try {
                            ps = DSApi.context().prepareStatement(sql);
                            ps.executeUpdate();
                        } catch (SQLException e) {
                            DSApi.context().rollback();
                            e.printStackTrace();
                        }}
                    } finally {
                        DSApi.closeContextIfNeeded(wasOpened);
                    } }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            msgBuilder.append(e.getMessage());
        } finally {
            DSApi.close();
        }
        if (msgBuilder.length() > 0) {
            values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
            return new Field("messageField", msgBuilder.toString(), Field.Type.BOOLEAN);
        }
        return null;
    }

// zapisanie we wniosku zapotrzebowania numeru z wniosku inwestycyjnego
    private void readDocument(Map<String, FieldData> values, FieldsManager fm) throws EdmException {

        try {
            DSApi.context().begin();
            Long documentId;
            DocumentKind zapotrzebowanie = DocumentKind.findByCn(ZapotrzebowanieLogic.DOCKIND_CN);
            documentId = fm.getDocumentId();
            Set<Long>ids=  DwrUtils.getMultipleDictionaryIds(values, TO_ZAPOTRZEBOWANIE);
            for (Long id: ids){
            zapotrzebowanie.setOnly(id, ImmutableMap.of("TO_WINWEST", documentId));}


/*            Map<String, FieldData> dwrValues = (Map<String, FieldData>) values.get(TO_ZAPOTRZEBOWANIE).getDictionaryData();


            for (String key : dwrValues.keySet()) {

                if (key.contains("TO_ZAPOTRZEBOWANIE_id_") && dwrValues.get(key).getData() != null) {
                    Long amount = dwrValues.get(key).getLongData();

                    zapotrzebowanie.setOnly(amount, ImmutableMap.of("TO_WINWEST", ImmutableList.of(documentId)));
                    DwrUtils.updateMultipleDictionary(values,"TO_WINWEST",documentId,null);
                }
            }*/


            DSApi.context().commit();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            DSApi.context().setRollbackOnly();

        }
    }


    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) {
        Map<String, Object> results = super.setMultipledictionaryValue(dictionaryName, values);

        if(results == null){
            results = new HashMap<String, Object>();
        }
      return results;

    }
    private static String senderButtonFromDictionary(Map<String, FieldData> values, String dictionaryName, String buttonName) throws EdmException {

        Map<String, FieldData> buttons = DwrUtils.getDictionaryFields(values, dictionaryName, buttonName);

        for (Map.Entry<String, FieldData> button : buttons.entrySet()) {
            if (button.getValue().getType().equals(Field.Type.BUTTON) && button.getValue().getButtonData().isClicked()) {
                button.getValue().getButtonData().setClicked(false);
                return button.getKey();
            }
        }

        return null;
    }

    private List<Map<String, String>> zwrocListeZTabeli(
            Collection<EnumItem> availableItems) {
        List<Map<String,String>>itemy = new ArrayList<Map<String,String>>();
        HashMap< String, String> mapa=new HashMap<String, String>();
        mapa.put("", "--wybierz--");
        itemy.add(mapa);
        for(EnumItem e:availableItems){
            mapa=new HashMap<String, String>();
            mapa.put(e.getId().toString(), e.getTitle());
            itemy.add(mapa);
        }
        return itemy;
    }



    private Object findProcessVariable(Long documentId, String variableName){
        RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
        if(runtimeService != null){
            ExecutionQuery query = runtimeService.createExecutionQuery().processVariableValueEquals("docId",documentId);
            Execution execution = query.list().size() >0 ? query.list().get(0) : null;
            if (execution != null) {
                return runtimeService.getVariable(execution.getId(), variableName);
            }
        }
        return null;
    }

    /**
     * Returns name of current process activity
     * @param documentId
     * @return
     */
    private String currentProcessActivity(Long documentId){
        RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
        if(runtimeService != null){
            ExecutionQuery query = runtimeService.createExecutionQuery().processVariableValueEquals("docId",documentId);
            List<Execution> execList =  query.list();
            if(execList != null){
                for(int i = 0 ; i < execList.size(); i++){
                    Execution execution = query.list().get(i);
                    if(execution.getActivityId() != null ){
                        return execution.getActivityId();
                    }
                }
            }
        }
        return null;
    }

}

