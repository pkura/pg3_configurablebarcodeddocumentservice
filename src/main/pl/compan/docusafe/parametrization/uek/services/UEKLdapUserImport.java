package pl.compan.docusafe.parametrization.uek.services;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.google.common.primitives.Ints;
import com.google.common.collect.Lists;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.field.DSUserEnumField;
import pl.compan.docusafe.core.users.*;
import pl.compan.docusafe.core.users.ad.ActiveDirectoryManager;
import pl.compan.docusafe.service.*;
import pl.compan.docusafe.service.mail.MailerDriver;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.Control;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.PagedResultsControl;
import javax.naming.ldap.PagedResultsResponseControl;
import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 *
 */
public class UEKLdapUserImport extends ServiceDriver implements Service
{
	public static final Logger log = LoggerFactory.getLogger(UEKLdapUserImport.class);
    

    private Timer timer;
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(MailerDriver.class.getPackage().getName(),null);
	private Property[] properties;
	private Long period = 240L;
	private String filter = "";
	private String emailAttrName = "mail";
	private String hostname;
	private String port;
	private String dbName;
	private String username;
	private String password;
    private String divisionCodesGetBasicStatement;
    private String divisionCodesGetStatementWithAdCondition;


    protected boolean canStop()
	{
		return true;
	}

	protected void start() throws ServiceException
	{
		log.info("start uslugi LdapUserImport");
		timer = new Timer(true);
//		timer.schedule(new Import(), (long) 2*1000 ,(long) period*DateUtils.MINUTE);
//		console(Console.INFO, sm.getString("StartUslugiLdapUserImport"));
	}

	protected void stop() throws ServiceException
	{
		log.info("stop uslugi");
		console(Console.INFO, sm.getString("StopUslugi"));
		if(timer != null) timer.cancel();
	}

	public boolean hasConsole()
	{
		return true;
	}
	
	class Import extends TimerTask
	{
		public void run()
		{
            ActiveDirectoryManager ad = null;
			LdapContext context = null;
			Connection conn= null;
			try
			{
				System.out.println("HEJO");
				console(Console.INFO, "Start");

                DSApi.openAdmin();
				console(Console.INFO, "Laczy z AD");
				ad = new ActiveDirectoryManager();
				context = ad.LdapConnectAsAdminInLdapContext();
				console(Console.INFO, "Polaczyl sie z AD");			
				int pageSize = 300;
			    byte[] cookie = null; 
				context.setRequestControls(new Control[]{new javax.naming.ldap.PagedResultsControl(pageSize, Control.NONCRITICAL)});				
				
				String tmpfilter = "(&(objectClass=person)(objectClass=user)(!(objectClass=Computer))(sAMAccountName=*)(sn=*)(userPrincipalName=*)(givenName=*)(erpGUID=*))";
				if(!StringUtils.isEmpty(filter))
				{
					tmpfilter = filter;
				}
				console(Console.INFO, "Szuka uzytkownikow " + tmpfilter);
				log.error("Szuka uzytkownikow " + tmpfilter);
				int j = 0;
				List<String> usersRegistrationNumbersFromResponse = Lists.newArrayListWithExpectedSize(2000);
				while(true)
				{
					SearchControls ctls = new SearchControls();
					ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);
					ctls.setCountLimit(0);
					
					
					NamingEnumeration<SearchResult> results = context.search("",tmpfilter, ctls);
					while (results != null && results.hasMore()) 
					{
						try
						{
							j++;
							log.error("Next "+j);
							console(Console.INFO, "Next "+j);
							DSApi.context().begin();
							SearchResult entry = (SearchResult)results.next();
							DSUser user = getUserFromAd(entry);
							usersRegistrationNumbersFromResponse.add(user.getIdentifier());
							DSApi.context().session().save(user);
							DSApi.context().commit();
                        }
						catch (Exception e) 
						{
							UEKLdapUserImport.log.error("",e);
							console(Console.ERROR, "B��d [ "+e+" ]");
							DSApi.context()._rollback();
						}
						if(j % 100 == 0)
						{
							log.error("Wyczyszczenie sesji na idx: " + j);
							DSApi.context().session().flush();
							DSApi.context().session().clear();
							DSApi.close();
							DSApi.openAdmin();
						}
					}
					Control[] controls = context.getResponseControls();
					if(controls!=null)
					{
						for(int k = 0; k<controls.length; k++)
						{
							if(controls[k] instanceof PagedResultsResponseControl)
							{
								PagedResultsResponseControl prrc = (PagedResultsResponseControl)controls[k];
								prrc.getResultSize();
								cookie = prrc.getCookie();
						    }
							else
							{
								throw new EdmException("Niespodziewany ResponseControls");
						    }
						}
					}
					if(cookie==null)
						break;
					context.setRequestControls(new Control[]{new PagedResultsControl(pageSize, cookie, Control.CRITICAL)});
				}
				
				deleteNotExistingInResponseUsers(usersRegistrationNumbersFromResponse);
				
				
			}
			catch(Exception e)
			{
				UEKLdapUserImport.log.error("",e);
				console(Console.ERROR, "B��d [ "+e+" ]");
			}
			finally
			{
				
				log.trace("Import  STOP");
				console(Console.INFO, "Koniec synchronizacji");
				if(context != null)
					try {
						context.close();
					} catch (NamingException e) {
						log.error(e.getMessage(),e);
					}
				DbUtils.closeQuietly(conn);
				DSApi._close();
			}
		}
    }
	
	
	/**
	 * Metoda usuwa uzytkownikow ktorzy nie sa aktualni. Czyli tych ktorzy nie znajduja sie w danych przesylanych przez webservice i ktorzy posiadaja extension.
	 * @param usersRegistrationNumbersFromResponse - lista external ids'ow uzytkownikow ktore zostaly przeslane przez webservice
	 */
	private void deleteNotExistingInResponseUsers(List<String> usersRegistrationNumbersFromResponse) {
		if(usersRegistrationNumbersFromResponse == null || usersRegistrationNumbersFromResponse.size() < 1)
			return;
		try {
			console(Console.INFO, sm.getString("Rozpoczyna usuwanie u�ytkownik�w kt�rych nie zwr�ci� LDAP"));
			List<DSUser> users = DSUser.list(DSUser.SORT_FIRSTNAME_LASTNAME);
			//z powodu zrywania sesji trzeba to przepisa� 
			Map<String,String> usersLoginInentifier = new HashMap<String, String>();
			for (DSUser user : users) {
				usersLoginInentifier.put(user.getName(), user.getIdentifier());
			}
			int j = 0;
			for (String key : usersLoginInentifier.keySet()) {
				j++;
				if (StringUtils.isNotBlank(usersLoginInentifier.get(key)) && !"admin".equals(key) && !usersRegistrationNumbersFromResponse.contains(usersLoginInentifier.get(key))) {
					DSApi.context().begin();
					UserFactory.getInstance().deleteUser(key);
					DSApi.context().commit();
					console(Console.INFO, "Usuwam username = " +key +" guidOsoby w ERP = "+usersLoginInentifier.get(key));
				}
				if(j % 50 == 0)
				{
					log.error("Wyczyszczenie sesji na idx: " + j);
					DSApi.context().session().flush();
					DSApi.context().session().clear();
					DSApi.close();
					DSApi.openAdmin();
				}
			}
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
			console(Console.ERROR,  sm.getString("UsersImportAndSynchronizationServiceServiceErrorDeletedUser"));
			try {
				DSApi.context().rollback();
			} catch (EdmException edm) {
				log.error(edm.getMessage(), edm);
			}
		}
	}

//    private DSUser getSupervisor(DSUser user, Connection conn) {
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        String extensionRaw = user.getExtension();
//        if (extensionRaw != null && Ints.tryParse(extensionRaw) != null) {
//            try {
//                ps = conn.prepareStatement(GET_SUPERVISOR_ID_SQL);
//                ps.setInt(1, Ints.tryParse(extensionRaw));
//
//                rs = ps.executeQuery();
//                while (rs.next()) {
//                    String supervisorId = rs.getString(1);
//                    if (!Strings.isNullOrEmpty(supervisorId)) {
//                        try {
//                            return DSUser.findAllByExtension(supervisorId);
//                        } catch (UserNotFoundException e) {
//                            log.error(e.getMessage(), e);
//                            console(Console.ERROR, "Nie znaleziono przelozonego id: " + supervisorId + ", dla uzytkownika:" + user.getName());
//                        }
//                    }
//                }
//
//            } catch (Exception exc) {
//                log.error(exc.getMessage(), exc);
//                console(Console.ERROR, "Blad podczas ustawiania przelozonego dla uzytkownika:"+user.getName());
//            } finally {
//                DbUtils.closeQuietly(ps);
//                DbUtils.closeQuietly(rs);
//            }
//        } else {
//            log.error("Brak id pracownika");
//            console(Console.ERROR, "Brak id pracownika");
//        }
//
//        return null;
//    }

  


	private DSUser getUserFromAd(SearchResult element) throws Exception
	{
		DSUser user = null;				
		String nameExternal = getStringAttr(element,"userPrincipalName");
		String userGuid = getStringAttr(element,"erpGUID");
		log.error("userGuid = "+userGuid);
		String firstname = getStringAttr(element,"givenName");
        String lastname = getStringAttr(element,"sn");
        String email =  getStringAttr(element,emailAttrName);
        
		log.error(" nameext "+ nameExternal +" ");
		String name = null;
		if(nameExternal.contains("@"))
			name  = nameExternal.substring(0,nameExternal.indexOf("@")).replaceAll("\\.","");
		else
			name  = nameExternal.replaceAll("\\.","");

        name = normalizeUsername(name);

        if(StringUtils.isBlank(userGuid))
        	throw new EdmException("Brak idPracownika");

        if(nameExternal.contains("@"))
        	nameExternal  = nameExternal.substring(0,nameExternal.indexOf("@")).replaceAll("\\.","");

        log.debug("name " + name +" nameext "+ nameExternal +" ");
		if(name != null && firstname != null && lastname != null)
		{
			console(Console.INFO, "Szuka uzytkownika");
			try
			{
				String orginalName = name;

				user = DSUser.findByIdentifier(userGuid);
				log.error("Znalzal po iden"+ user.getIdentifier());
				if(!user.getName().equalsIgnoreCase(name))
				{
					console(Console.INFO, "Znalazl ten sam nameExternal ale inny name "+ name);
					log.error("name " + name +" nameext "+ nameExternal +" ");
				}
				
				
				console(Console.INFO, "Znalazl "+ user.asFirstnameLastname());
                if (user.isDeleted()) {
                    UserFactory.getInstance().revertUser(user.getName());
                }
                user.setLoginDisabled(false);
                user.setFirstname(firstname);
                user.setExternalName(nameExternal);
                user.setLastname(lastname);
                user.setEmail(email);
				user.update();
			}
			catch (UserNotFoundException e)
			{
                try
                {
                    String orginalName = name;
                    for (int i = 1; i < 10; i++)
                    {
                        user = DSUser.findByUsername(name);
                        name  = orginalName+i;
                    }
                    throw new EdmException("Nie ustalil name");
                }
                catch (UserNotFoundException e2)
                {
                	log.error("DODAJE "+ userGuid);
                    console(Console.INFO, "Nie znalazl dodaje "+name);
                    user = UserFactory.getInstance().createUser(name, firstname, lastname);
                    user.setEmail(email);
                    user.setCtime(new Timestamp(new Date().getTime()));
                    user.setAdUser(true);
                    user.setExternalName(nameExternal);
                    user.setIdentifier(userGuid);
                    console(Console.INFO, "Dodal "+user.asFirstnameLastname());
                    log.error("Dodal "+user.asFirstnameLastname());
                }
                
			}	
		}
		else
		{
			throw new EdmException("Brak podstawowych parametr�w "+name+","+firstname+","+lastname+","+email);
		}
		log.error("USER = "+user.getIdentifier());
		return user;
	}

    /**
     * Usuwa ogonki z znak�w diakratycznych, nast�pnie wycina wszystkie znaki, kt�re nie s� [A-z] lub [0-9].
     * Wynik jest konwertowany do ma�ych liter.
     * @param name
     * @return
     */
    private String normalizeUsername(String name) {
        name = TextUtils.normalizeString(name);
        name = CharMatcher.JAVA_LETTER_OR_DIGIT.retainFrom(name);
        return name.toLowerCase(Locale.ENGLISH);
    }


   
	
	
	

   

    private String getStringAttr(SearchResult element,String name) throws NamingException
	{
		if(element.getAttributes().get(name) != null)
		{
			return (String) element.getAttributes().get(name) .get();
		}
		return null;
	}

	public Property[] getProperties() {
		return properties;
	}

	public void setProperties(Property[] properties) {
		this.properties = properties;
	}
	
	 class LdapTimesProperty extends Property
	    {
	        public LdapTimesProperty()
	        {
	            super(SIMPLE, PERSISTENT, UEKLdapUserImport.this, "period", "Czestotliwo�c synchronizacji w minutach", String.class);
	        }

	        protected Object getValueSpi()
	        {
	            return period;
	        } 

	        protected void setValueSpi(Object object) throws ServiceException
	        {
	            synchronized (UEKLdapUserImport.this)
	            {
	            	if(object != null)
	            	{
	            		period = Long.parseLong((String) object);
	            	}
	            	else
	            	{
	            		period = 240L;
	            	}
	            
	            }
	        }
	    }
	    
	    class MailAttrNameProperty extends Property{
	        public MailAttrNameProperty(){
	            super(SIMPLE, PERSISTENT, UEKLdapUserImport.this, "emailAttrName", "emailAttrName", String.class);
	        }
	        protected Object getValueSpi(){
	            return emailAttrName;
	        } 
	        protected void setValueSpi(Object object) throws ServiceException{
	            synchronized (UEKLdapUserImport.this){
	            	if(object == null)
	            		emailAttrName = "mail";
	            	else
	            		emailAttrName = (String) object;
	            }
	        }
	    }
	    
	    class PortProperty extends Property
		{
			public PortProperty()
			{
				super(SIMPLE, PERSISTENT, UEKLdapUserImport.this, "port", "port", String.class);
			}

			protected Object getValueSpi()
			{
				return port;
			}

			protected void setValueSpi(Object object) throws ServiceException
			{
				synchronized (UEKLdapUserImport.this)
				{
					if (object != null)
						port = (String) object;
					else
						port = "";
				}
			}
		}
		
		class DbNameProperty extends Property
		{
			public DbNameProperty()
			{
				super(SIMPLE, PERSISTENT, UEKLdapUserImport.this, "dbName", "Baza", String.class);
			}

			protected Object getValueSpi()
			{
				return dbName;
			}

			protected void setValueSpi(Object object) throws ServiceException
			{
				synchronized (UEKLdapUserImport.this)
				{
					if (object != null)
						dbName = (String) object;
					else
						dbName = "";
				}
			}
		}
		
		class UsernameProperty extends Property
		{
			public UsernameProperty()
			{
				super(SIMPLE, PERSISTENT, UEKLdapUserImport.this, "username", "Uzytkownik", String.class);
			}

			protected Object getValueSpi()
			{
				return username;
			}

			protected void setValueSpi(Object object) throws ServiceException
			{
				synchronized (UEKLdapUserImport.this)
				{
					if (object != null)
						username = (String) object;
					else
						username = "eod";
				}
			}
		}
		
		class HostnameProperty extends Property
		{
			public HostnameProperty()
			{
				super(SIMPLE, PERSISTENT, UEKLdapUserImport.this, "hostname", "Adres serwera", String.class);
			}

			protected Object getValueSpi()
			{
				return hostname;
			}

			protected void setValueSpi(Object object) throws ServiceException
			{
				synchronized (UEKLdapUserImport.this)
				{
					if (object != null)
						hostname = (String) object;
					else
						hostname = "";
				}
			}
		}
		
		class PasswordProperty extends Property
		{
			public PasswordProperty()
			{
				super(SIMPLE, PERSISTENT, UEKLdapUserImport.this, "password", "Has�o", String.class);
			}

			protected Object getValueSpi()
			{
				return password;
			}

			protected void setValueSpi(Object object) throws ServiceException
			{
				synchronized (UEKLdapUserImport.this)
				{
					if (object != null)
						password = (String) object;
					else
						password = "";
				}
			}
		}
	    
	    class FilterProperty extends Property
	    {
	        public FilterProperty()
	        {
	            super(SIMPLE, PERSISTENT, UEKLdapUserImport.this, "filter", "Filtr", String.class);
	        }

	        protected Object getValueSpi()
	        {
	            return filter;
	        } 

	        protected void setValueSpi(Object object) throws ServiceException
	        {
	            synchronized (UEKLdapUserImport.this)
	            {
	            	filter = (String) object;
	            }
	        }
	    }


		public UEKLdapUserImport()
		{
			properties = new Property[] { new LdapTimesProperty() ,new FilterProperty(),new MailAttrNameProperty(),new HostnameProperty(), new PortProperty(),new DbNameProperty(),new UsernameProperty(),new PasswordProperty()};
		}
}
