package pl.compan.docusafe.parametrization.uek.services;

import com.google.common.collect.ImmutableList;
import pl.compan.docusafe.dictionary.erp.simple.UnitBudgetKoKind;
import pl.compan.docusafe.dictionary.erp.simple.imports.*;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImportService;
import pl.compan.docusafe.service.dictionaries.DictionaryImport;
import pl.compan.docusafe.util.DateUtils;

/**
 * Created by Damian on 18.04.14.
 */
public class UEKDictionariesImportService  extends AbstractDictionaryImportService {
    private static final ImmutableList<DictionaryImport> AVAILABLE_IMPORTS = ImmutableList.<DictionaryImport>builder()
            .add(new ContractImport())
            .add(new ContractBudgetImport())
            .add(new CurrencyImport())
            .add(new PurchaseDocumentTypeImport())
            .add(new SourceImport())
            .add(new SupplierImport())
            .add(new UnitBudgetKoImport())
            .add(new VatRateImport())
            .add(new CostInvoiceProductImport())
            .add(new ProductionOrderImport())
            .add(new UnitOfMeasureImport())
            .add(new UnitBudgetImport())
            .add(new FinancialTaskImport())
            .add(new ProjectBudgetResourceImport())
            .add(new ProjectBudgetImport())
            .add(new ProjectBudgetItemImport())
            .add(new RolesInProjectsImport())
            .add(new RolesInUnitBudgetKoImport())
            .add(new ProjectImport())
            .add(new UnitBudgetKoKindImport())
            .build();

    @Override
    protected ImmutableList<DictionaryImport> getAvailableImports() {
        return AVAILABLE_IMPORTS;
    }

    @Override
    protected long getTimerDefaultPeriod() {
        return 600l;
    }

    @Override
    protected long getTimerStartDelay() {
        return 25 * DateUtils.SECOND;
    }
}
