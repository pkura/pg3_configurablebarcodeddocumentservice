package pl.compan.docusafe.parametrization.uek;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.service.epuap.multi.EpuapSkrytka;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.parametrization.uek.hbm.DeliveryCostUek;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.office.massimport.FieldDataProvider;
import pl.compan.docusafe.web.office.massimport.FieldsHandler;
import pl.compan.docusafe.web.office.out.OutgoingImportAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.boot.Docusafe;
import com.google.common.collect.Maps;

/**
 * Logic
 *
 * @version $Id$
 */
public class NormalLogic extends AbstractDocumentLogic implements FieldsHandler.Provider {

	private static final long serialVersionUID = 1L;
	
	private static final String PRZYCHODZACE = "normal";
	private static final String WYCHODZACE = "normal_out";
	private static final String WEWNETRZNE = "normal_int";
	
	public static final String OFFICE = "office";

    public static final String DOC_NUMBER_R = 			"DOC_NUMBER_R";

    public static final String DWR_DOC_ZPO = 			"DWR_DOC_ZPO";
    public static final String DWR_DOC_SEND_KIND = 		"DWR_DOC_SEND_KIND";
    public static final String DWR_DOC_MAIL_ZONE = 		"DWR_DOC_MAIL_ZONE";
    public static final String DWR_DOC_MAIL_SIZE = 		"DWR_DOC_MAIL_SIZE";
    public static final String DWR_DOC_MAIL_WEIGHT = 	"DWR_DOC_MAIL_WEIGHT";
    public static final String DWR_KOSZT_PRZESYLKI = 	"DWR_KOSZT_PRZESYLKI";
    public static final String DOC_MAIL_ZONE = 			"DOC_MAIL_ZONE";
    public static final String DWR_DOC_RETURN = 		"DWR_DOC_RETURN";
    public static final String DWR_DOC_NUMBER_R = 		"DWR_DOC_NUMBER_R";
    public static final String DWR_DOC_NUMBER_R_OUT = 		"DWR_DOC_NUMBER_R_OUT";
    private static NormalLogic instance;
    
    protected static Logger log = LoggerFactory.getLogger(NormalLogic.class);
    private final static StringManager SM = GlobalPreferences.loadPropertiesFile(NormalLogic.class.getPackage().getName(), null);
    
    private static String senderFromOutDoc = null;

    public static NormalLogic getInstance() {

        if (instance == null) {
            instance = new NormalLogic();
        }

        return instance;
    }
    

    @Override
    public void setAdditionalValues(FieldsManager fm, Integer valueOf, String activity) throws EdmException {
    	//Sortowanie s這wnika kosztow podruzy po dacie
    	if(fm.getField("RECIPIENTS") != null) {
    		final String sort = "RECIPIENTS_ORDEROFADDITION";
        	//DictionaryUtils.sortMultipleDictionary(sort, "RECIPIENTS", fm);
    	}
    }
    
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException {
    	String dockindCn = kind.getCn();
    	if (!StringUtils.isBlank(dockindCn)) {
    		if (dockindCn.equalsIgnoreCase(PRZYCHODZACE)) {
                String numerlistu = "";
                 if(values.get(DWR_DOC_NUMBER_R) != null) {
                    numerlistu = values.get(DWR_DOC_NUMBER_R).toString();
                }

    			if (!numerlistu.isEmpty()) {
    				InOfficeDocument inDocument = null;
        			try {
        				inDocument = InOfficeDocument.findByPostalRegNumber(numerlistu);
    				} catch (EdmException e) {
    					log.info("Nie znaleziono dokumentu");
    				}
        			
        			if (inDocument != null && !inDocument.getId().equals(documentId)) {
        				throw new EdmException(SM.getString("DokumentJestJuzZarejestrowanyBlokadaZapisuDokumentu", inDocument.getId().toString(), inDocument.getPostalRegNumber()));
        			}
    			}
    		} else if (dockindCn.equalsIgnoreCase(WYCHODZACE)) {

                String numerlistu = "";
                if(values.get(DWR_DOC_NUMBER_R) != null) {
                  numerlistu = (String) values.get(DWR_DOC_NUMBER_R);
                }



    			if (StringUtils.isNotEmpty(numerlistu)) {
    				List<OutOfficeDocument> outDocument = null;
        			try {
        				outDocument = OutOfficeDocument.findByPostalRegNumber(numerlistu);
    				} catch (EdmException e) {
    					log.info("Nie znaleziono dokumentu");
    				}
        			
        			if (outDocument != null && outDocument.size() > 0) {
        				StringBuilder sb = new StringBuilder();
        				for (OutOfficeDocument outOfficeDocument : outDocument) {
        					if(!outOfficeDocument.getId().equals(documentId)) {
        						sb.append(SM.getString(SM.getString("DokumentJestJuzZarejestrowanyBlokadaZapisuDokumentu", outOfficeDocument.getId().toString(), outOfficeDocument.getPostalRegNumber())));
        						throw new EdmException(sb.toString());
        					}
    					}
        			}
    			}
    		}
    	}
    }


    @Override
    public EpuapSkrytka wybierzSkrytkeNadawcyEpuap (OfficeDocument document) throws EdmException {

        EpuapSkrytka skrytka = new EpuapSkrytka();

        List<Recipient> recipients;
        recipients = document.getRecipients();

        skrytka.setAdresSkrytki(recipients.get(0).getAdresSkrytkiEpuap());

        skrytka.setIdentyfikatorPodmiotu(recipients.get(0).getIdentifikatorEpuap());

        //skrytka.setNazwaSkrytki("SKRYTKA");

        skrytka.setCertificatePassword(Docusafe.getAdditionProperty("cert.X509.password"));
        skrytka.setCertificateFileName(Docusafe.getAdditionProperty("certificate.pfx"));


        try {
            DSApi.context().session().saveOrUpdate(skrytka);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }

        return skrytka;
    }



    @Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        StringBuilder msgBuilder = new StringBuilder();
        
        try{
        	if (WYCHODZACE.equals(fm.getDocumentKind().getCn())) {
            	if ((values.get(DWR_DOC_MAIL_WEIGHT) != null && values.get(DWR_DOC_MAIL_WEIGHT).isSender()) ||
                		(values.get(DWR_DOC_MAIL_ZONE) != null && values.get(DWR_DOC_MAIL_ZONE).isSender()) ||
                		(values.get(DWR_DOC_MAIL_SIZE) != null && values.get(DWR_DOC_MAIL_SIZE).isSender()) ||
                		(values.get(DWR_DOC_MAIL_WEIGHT) != null && values.get(DWR_DOC_MAIL_WEIGHT).isSender()) ||
                		(values.get(DWR_DOC_SEND_KIND) != null && values.get(DWR_DOC_SEND_KIND).isSender())) {
                	
                	calculateStampFee(values, msgBuilder);
                }
            }
            
            if (PRZYCHODZACE.equals(fm.getDocumentKind().getCn())) {
            	Map<String, FieldData> recipientsDictionary = values.get("DWR_RECIPIENTS").getDictionaryData();
            	for (String key : recipientsDictionary.keySet()) {
					if (key.contains("ORDEROFADDITION_")) {
						String rowNumber = key.split("_")[2];
						recipientsDictionary.put("RECIPIENTS_ORDEROFADDITION_" + rowNumber, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.INTEGER, Integer.valueOf(rowNumber)));
					}
				}
            	 if (DwrUtils.isNotNull(values, DWR_DOC_RETURN, DWR_DOC_NUMBER_R_OUT) && (values.get(DWR_DOC_RETURN).getBooleanData().booleanValue() == true) && (!values.get("DWR_RECIPIENTS").isSender())){
            		 fillDocument(values, fm);
                 }
            }
            
            if (msgBuilder.length() > 0) {
                values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
                return new Field("messageField", msgBuilder.toString(), Field.Type.BOOLEAN);
            }
        } catch (Exception e) {
        	log.error(e.getMessage(), e);
        } 
        return null;
    }
    
  
    private Boolean fillDocument(Map<String, FieldData> values, FieldsManager fm){
		 try{
			 String postalRegNumber = values.get(DWR_DOC_NUMBER_R_OUT).getStringData();
   			 OutOfficeDocument outDoc = null;
   			 
   			 DSApi.openAdmin();
   			 if(postalRegNumber != null && postalRegNumber.length() > 0 && OutOfficeDocument.findByPostalRegNumber(postalRegNumber) != null){
	   			 outDoc = OutOfficeDocument.findByPostalRegNumber(postalRegNumber).get(0);
	   			 
	   			 if(outDoc == null){
	       			 return null;
	       		 } else {
	       			 Recipient recipient = outDoc.getRecipients().get(0);
	       			 Person person = (Person) recipient;
	       			 Sender sender = new Sender();
	       			 sender.fromMap(person.toMap());
	       			 sender.setDictionaryGuid("rootdivision");
	       			 sender.setDictionaryType(Person.DICTIONARY_SENDER);
	       			 sender.setBasePersonId(person.getBasePersonId());
	       			 sender.create();
	       			 
	       			 values.put("DWR_SENDER", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.DICTIONARY , sender.getId()));
	       			 
	       			 sender = outDoc.getSender();
	       			 person = (Person) sender;
	       			 recipient = new Recipient();
	       			 recipient.fromMap(person.toMap());
	       			 recipient.setDictionaryGuid("rootdivision");
	       			 recipient.setDictionaryType(Person.DICTIONARY_RECIPIENT);
	       			 recipient.setBasePersonId(person.getBasePersonId());
	       			 DSUser user = UserImpl.findByFirstnameLastname(person.getFirstname(), person.getLastname());
	       			 String lparam = "u:"+user.getName();
	       			 lparam+=(";d:"+user.getDivisionsWithoutGroup()[0].getGuid());
	       			 recipient.setLparam(lparam);
	       			 recipient.create();
	       			 
	       			 senderFromOutDoc = recipient.getLparam();
	       			 
	       			 values.put("DWR_RECIPIENTS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.DICTIONARY , recipient.getId()));
	       			 
	       			 values.put("DWR_DOC_OUT", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.DICTIONARY, outDoc.getId()));
	       			 
	       		 }
   		 }
   		 
		 } catch (EdmException e) {
			 log.error(e.getMessage(), e);
		 } finally {
         	DSApi._close();
         }
		 
		 return true;
    }

    private Boolean calculateStampFee(Map<String, FieldData> values, StringBuilder msgBuilder) {
        	Integer zpo = values.get("DWR_DOC_ZPO") != null && Boolean.TRUE.equals(values.get("DWR_DOC_ZPO").getBooleanData()) ? 1 : 0;
        	Integer poziomListu = null;
        	Integer poziomGabarytu = null;
        	Integer poziomWagi = null;
        	
        	if(values.get(DWR_DOC_SEND_KIND) != null) {
        		try{ 
        			poziomListu = Integer.valueOf(values.get(DWR_DOC_SEND_KIND).getEnumValuesData().getSelectedId());
            	} catch (Exception e) {
            		log.error("Integer.valueOf(values.get(DWR_DOC_SEND_KIND).getEnumValuesData().getSelectedId())" );
            		log.error(e.getMessage(), e);
            		return false;
            	}
        	} else {
        		return false;
        	}
            
        	//KARTKA POCZTOWA
			if (poziomListu == 14 || poziomListu == 15) {
				return findDeliveryCost(values, 0, 0, poziomListu, 0);
			}
        	
			//LIST KRAJOWY
            if(poziomListu < 5 && poziomListu > 0) {
            	if (values.get(DWR_DOC_MAIL_SIZE) != null) {
                	try{ 
                		poziomGabarytu = Integer.valueOf(values.get(DWR_DOC_MAIL_SIZE).getEnumValuesData().getSelectedId());
                	} catch (NumberFormatException e) {
                		log.error(e.getMessage(), e);
    				} catch (Exception e) {
                		log.error("Integer.valueOf(values.get(DWR_DOC_MAIL_SIZE).getEnumValuesData().getSelectedId())" );
                		log.error(e.getMessage(), e);
                		return false;
                	}
                } else {
                	return false;
                }
            //LIST ZAGRANICZNY
            } else if(poziomListu >= 6 && poziomListu <= 9) {
            	if (values.get(DWR_DOC_MAIL_ZONE) != null){
                	try{ 
                		poziomGabarytu = Integer.valueOf(values.get(DWR_DOC_MAIL_ZONE).getEnumValuesData().getSelectedId());
                	} catch (Exception e) {
                		log.error("Integer.valueOf(values.get(DWR_DOC_MAIL_ZONE).getEnumValuesData().getSelectedId())" );
                		log.error(e.getMessage(), e);
                		return false;
                	}
                } else {
                	return false;
                }
            } else {
            	return false;
            }
            
            if (values.get(DWR_DOC_MAIL_WEIGHT) != null) {
            	try{ 
            		poziomWagi = Integer.valueOf(values.get(DWR_DOC_MAIL_WEIGHT).getEnumValuesData().getSelectedId());
            	} catch (Exception e) {
            		log.error("values.get(DWR_DOC_MAIL_WEIGHT).getEnumValuesData().getSelectedId() - po prze豉dowaniu w drools zwraca null" );
            		log.error(e.getMessage(), e);
            	}
            } else {
            	return false;
            }
            
            return findDeliveryCost(values, zpo, poziomGabarytu, poziomListu, poziomWagi);
            
    }

    private Boolean findDeliveryCost(Map<String, FieldData> values, Integer zpo, Integer poziomGabarytu, Integer poziomListu, Integer poziomWagi) {
    	try{ 
        	DSApi.openAdmin();
        	DeliveryCostUek deliveryCost = DeliveryCostUek.find(zpo, poziomGabarytu, poziomListu, poziomWagi);
        	
        	if(deliveryCost != null && deliveryCost.getKwota() != null){
             	BigDecimal kwota = deliveryCost.getKwota();
             	values.get(DWR_KOSZT_PRZESYLKI).setMoneyData(kwota);
             	return true;
            } else {
            	return false;
            }
        } catch (EdmException e) {
        	log.error(e.getMessage(), e);
        	return false;
        } finally {
        	DSApi._close();
        }
    }
    
	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {

		Map<String, Object> toReload = Maps.newHashMap();

		if (WEWNETRZNE.equals(fm.getDocumentKind().getCn()) || WYCHODZACE.equals(fm.getDocumentKind().getCn())) {
			String sender = "u:" + DSApi.context().getDSUser().getName();
			DSDivision[] divisions = DSApi.context().getDSUser().getDivisionsWithoutGroup();
			if (divisions.length > 0) {
				sender += (";d:" + divisions[0].getGuid());
			} else {
				sender += (";d:0");
			}

			toReload.put("SENDER_HERE", sender);
			toReload.put("DOC_DATE", Calendar.getInstance().getTime());
		} else if (PRZYCHODZACE.equals(fm.getDocumentKind().getCn()) && fm.getValue("EMAIL_SENDER") != null) {
            toReload.put("CZY_MAIL", "true");

            if(PRZYCHODZACE.equals(fm.getDocumentKind().getCn())) {
                toReload.put("DOC_INCOMEDATE", Calendar.getInstance().getTime());
            }

            }

		fm.reloadValues(toReload);
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        as.useFolderResolver(document);
        as.useAvailableProviders(document);
        if (document instanceof OutOfficeDocument) {
            FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
            //((OutOfficeDocument) document).setZpo((Boolean) fm.getKey("DOC_ZPO"));
        }
        log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {
        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
        perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

        for (PermissionBean perm : perms) {
            if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP)) {
                String groupName = perm.getGroupName();
                if (groupName == null) {
                    groupName = perm.getSubject();
                }
                createOrFind(document, groupName, perm.getSubject());
            }
            DSApi.context().grant(document, perm);
            DSApi.context().session().flush();
        }
    }

    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
        String docName = document.getDocumentKind() != null ? document.getDocumentKind().getName() : "no name";
        FieldsManager fm = document.getFieldsManager();

        if (WYCHODZACE.equals(fm.getDocumentKind().getCn())) {
        	Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
    		log.info("onStartProcess");
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
        } else {
        	log.info("--- " + docName + " : START PROCESS !!! ---- {}", event);
            try {
                Map<String, Object> map = Maps.newHashMap();
                    List<Recipient> documentRecipients = document.getRecipients();
                    for (Recipient recipient : documentRecipients) {
                    	sliceUserParam(recipient.getLparam(), map, document);
                    }
                    
                    if (fm.getBoolean("DOC_RETURN") && senderFromOutDoc != null) {
                    	sliceUserParam(senderFromOutDoc, map, document);
                    }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                throw new EdmException(e.getMessage());
            }
        }
    	setPermission(document);
    }
    
    /**
     * @param lparam
     * @param map
     * @param document
     * @throws EdmException
     */
    private void sliceUserParam(String lparam, Map<String, Object> map, OfficeDocument document) throws EdmException {
    	 String selectedUserName = null, selectedDivisionGUID = null;
    	 String[] ids = ((String) lparam).split(";");
         if (ids[0].contains("u:"))
             selectedUserName = ids[0].split(":")[1];
         if (ids[1].contains("d:"))
             selectedDivisionGUID = ids[1].split(":")[1];
    	
    	 map.put(ASSIGN_USER_PARAM, selectedUserName);
         map.put(ASSIGN_DIVISION_GUID_PARAM, selectedDivisionGUID);
         document.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess("manual").getLogic().startProcess(document, map);
         JBPMTaskSnapshot.updateByDocument(document);
    }
    
    private void setPermission(OfficeDocument document){
    	java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
    	try{
    		DSUser author = DSApi.context().getDSUser();
            String user = author.getName();
            String fullName = author.asFirstnameLastname();

            perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

            Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
            perms.addAll(documentPermissions);
            this.setUpPermission(document, perms);

            if (AvailabilityManager.isAvailable("addToWatch")) {
                DSApi.context().watch(URN.create(document));
            }
    	} catch (UserNotFoundException e) {
    		log.error("Nie znaleziono u篡tkownika");
    		log.error(e.getMessage(), e);
    	} catch (EdmException e) {
    		log.error(e.getMessage(), e);
    	}
    }


    @Override
    public FieldsHandler getFieldsHandler() {
        return new OutgoingNormalImportAction();
    }

    public static class OutgoingNormalImportAction extends OutgoingImportAction.OutgoingFieldsHandler {

        private static final String DOC_SEND_KIND = "DOC_SEND_KIND";

        public OutgoingNormalImportAction(){
            //addEnumCn(ENUM_DOC_SEND_KIND);
        }


        @Override
        public boolean handleSpecialField(pl.compan.docusafe.core.dockinds.field.Field field, OfficeDocument updatingDocument, Map<String, Object> updatingDockindKeys, FieldDataProvider importDocumentUpdater, String cn) throws EdmException {

            if (cn.equalsIgnoreCase(DOC_SEND_KIND)){
                Object importedValue = importDocumentUpdater.getValueByCn(cn);
                if (importedValue != null){
                    OutOfficeDocumentDelivery delivery = OutOfficeDocumentDelivery.findByName(importedValue.toString());

                    updateField(field, updatingDocument, updatingDockindKeys, cn, delivery.getId());
                    return true;
                }
            }

            return super.handleSpecialField(field, updatingDocument, updatingDockindKeys, importDocumentUpdater, cn);
        }
    }
    
    /* (non-Javadoc)
     * @see pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic#onSaveActions(pl.compan.docusafe.core.dockinds.DocumentKind, java.lang.Long, java.util.Map)
     */
    @Override
    public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException {
    }
}
