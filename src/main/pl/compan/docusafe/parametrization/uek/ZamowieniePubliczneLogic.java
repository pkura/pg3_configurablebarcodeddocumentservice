package pl.compan.docusafe.parametrization.uek;

import com.google.common.collect.Maps;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.api.ActivityInfo;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContextOpener;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.*;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.FieldsPrintProvider;
import pl.compan.docusafe.core.dockinds.logic.MarkerFieldsPrintProvider;
import pl.compan.docusafe.core.jbpm4.Jbpm4ActivityInfo;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.swd.MultipleDictionaryFactory;
import pl.compan.docusafe.parametrization.uek.activiti.UekProcessHelper;
import pl.compan.docusafe.parametrization.uek.hbm.ZamowieniePubliczneModuly;
import pl.compan.docusafe.parametrization.uek.utils.zamowieniePubliczne.ModulyZamowieniaPopup;
import pl.compan.docusafe.parametrization.uek.utils.zamowieniePubliczne.ZrodlaFinansowaniaPoopup;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.util.*;


/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 *         <p/>
 *         <p/>
 *         <p/>
 *         <p/>
 *         do uzupe�nienia zainicjowania
 *         ZAPOTRZEBOWANIA
 *         JEDNOSTKA_REALIZUJACA
 *         WARTOSCI_ZAM_OS_USTALAJACA
 *         OS_PROWADZACA_SPRAWE
 *         <p/>
 *         aktualizacja na podstawie zmiany innego pola
 *         KURS_EURO -> WARTOSC_NETTO_EURO_1
 *         WARTOSC_NETTO_1 -> MODULY_ZAMOWIENIA(WARTOSC_NETTO_EURO_1, WARTOSC_BRUTTO_1, FINANSOWANIE_BRUTTO), MODULY_ZAMOWIENIA (WARTOSC_EUR)
 *         FINANSOWANIE_BRUTTO
 *         <p/>
 *         walidacja
 *         KRYTERIA_OCENY - suma do 100
 */
public class ZamowieniePubliczneLogic extends AbstractDocumentLogic {

    private final static Logger log = LoggerFactory.getLogger(ZamowieniePubliczneLogic.class);

    public static final String KRYTERIUM_OCENY_PROCENT_INVALID = "Prosz� rozpisa� 100% krterium oceny";
    public static final String KRYTERIUM_OCENY_PROCENT_VALID = "Poprawnie rozliczone krterium oceny";
    public static final String KRYTERIUM_OCENY_OPIS_DEFAULT = "Najni�sza cena";

    public static final String DICT_ZDARZENIA = "ZDARZENIA";
    public static final String FIELD_KRYTERIA_OCENY_WALIDACJA = "KRYTERIA_OCENY_WALIDACJA";
    public static final String FIELD_JEDNOSTKA_REALIZUJACA = "JEDNOSTKA_REALIZUJACA";
    public static final String FIELD_WARTOSCI_ZAM_OS_USTALAJACA = "WARTOSCI_ZAM_OS_USTALAJACA";
    public static final String FIELD_OS_PROWADZACA_SPRAWE = "OS_PROWADZACA_SPRAWE";
    public static final String FIELD_KURS_EURO = "KURS_EURO";
    public static final String FIELD_MODUL_SUM_WARTOSC_NETTO = "MODUL_SUM_WARTOSC_NETTO";
    public static final String FIELD_MODUL_SUM_WARTOSC_NETTO_EURO = "MODUL_SUM_WARTOSC_NETTO_EURO";
    public static final String FIELD_MODUL_SUM_WARTOSC_BRUTTO = "MODUL_SUM_WARTOSC_BRUTTO";
    public static final String FIELD_MODUL_SUM_FINANSOWANIE_BRUTTO = "MODUL_SUM_FINANSOWANIE_BRUTTO";
    public static final String FIELD_CZESC_ZAMOWIENIA_WARTOSC = "CZESC_ZAMOWIENIA_WARTOSC";
    public static final String FIELD_CZESC_ZAMOWIENIA_WARTOSC_EUR = "CZESC_ZAMOWIENIA_WARTOSC_EUR";
    public static final String FIELD_POPUP_MODUL_ID = "POPUP_MODUL_ID";
    public static final String FIELD_POPUP_MODUL_NAZWA = "POPUP_MODUL_NAZWA";
    public static final String FIELD_POPUP_MODUL_KODY_CPV_MAIN = "POPUP_MODUL_KODY_CPV_MAIN";
    public static final String FIELD_POPUP_MODUL_KODY_CPV_EXTRA = "POPUP_MODUL_KODY_CPV_EXTRA";
    public static final String FIELD_POPUP_MODUL_WARTOSC_NETTO = "POPUP_MODUL_WARTOSC_NETTO";
    public static final String FIELD_POPUP_MODUL_WARTOSC_NETTO_EURO = "POPUP_MODUL_WARTOSC_NETTO_EURO";
    public static final String FIELD_POPUP_MODUL_WARTOSC_BRUTTO = "POPUP_MODUL_WARTOSC_BRUTTO";
    public static final String FIELD_POPUP_MODUL_STAWKA_VAT = "POPUP_MODUL_STAWKA_VAT";
    public static final String FIELD_POPUP_MODUL_FINANS_BRUTTO = "POPUP_MODUL_FINANS_BRUTTO";
    public static final String FIELD_POPUP_MODUL_REALIZACJA_OD = "POPUP_MODUL_REALIZACJA_OD";
    public static final String FIELD_POPUP_MODUL_REALIZACJA_DO = "POPUP_MODUL_REALIZACJA_DO";

    public static final String DICT_KRYTERIA_OCENY = "KRYTERIA_OCENY";
    public static final String DICT_MODULY_ZAMOWIENIA = "MODULY_ZAMOWIENIA";
    public static final String DICT_CZESC_WIEKSZEGO_ZAMOWIENIA = "CZESC_WIEKSZEGO_ZAMOWIENIA";
    public static final String DICT_ZR_FINANS = "ZR_FINANS";

    public static final String DICTFIELD_KRYTERIA_OCENY_PROCENT = "PROCENT";
    public static final String DICTFIELD_KRYTERIUM_OCENY_PROCENT = "PROCENT";
    public static final String DICTFIELD_KRYTERIUM_OCENY_OPIS = "OPIS";
    public static final String DICTFIELD_POPUP_MODUL_NAZWA = FIELD_POPUP_MODUL_NAZWA;
    public static final String DICTFIELD_POPUP_MODUL_KODY_CPV_TEXT = "POPUP_MODUL_KODY_CPV_TEXT";
    public static final String DICTFIELD_POPUP_MODUL_KODY_CPV = "KOD_CPV";
    public static final String DICTFIELD_POPUP_MODUL_KODY_CPV_MAIN = FIELD_POPUP_MODUL_KODY_CPV_MAIN;
    public static final String DICTFIELD_POPUP_MODUL_WARTOSC_NETTO = FIELD_POPUP_MODUL_WARTOSC_NETTO;
    public static final String DICTFIELD_POPUP_MODUL_WARTOSC_NETTO_EURO = FIELD_POPUP_MODUL_WARTOSC_NETTO_EURO;
    public static final String DICTFIELD_POPUP_MODUL_WARTOSC_BRUTTO = FIELD_POPUP_MODUL_WARTOSC_BRUTTO;
    public static final String DICTFIELD_POPUP_MODUL_STAWKA_VAT = FIELD_POPUP_MODUL_STAWKA_VAT;
    public static final String DICTFIELD_POPUP_MODUL_FINANS_BRUTTO = FIELD_POPUP_MODUL_FINANS_BRUTTO;
    public static final String DICTFIELD_POPUP_MODUL_REALIZACJA_OD = FIELD_POPUP_MODUL_REALIZACJA_OD;
    public static final String DICTFIELD_POPUP_MODUL_REALIZACJA_DO = FIELD_POPUP_MODUL_REALIZACJA_DO;


//    public static final String FIELDDICT_KRYTERIA_OCENY_OPIS = "OPIS";
//    public static final String FIELDDICT_KRYTERIA_OCENY_PROCENT = "PROCENT";
//    public static final String FIELDDICT_MODULY_ZAMOWIENIA_WARTOSC_NETTO = "WARTOSC_NETTO";
//    public static final String FIELDDICT_MODULY_ZAMOWIENIA_WARTOSC_NETTO_EURO = "WARTOSC_NETTO_EURO";
//    public static final String FIELDDICT_MODULY_ZAMOWIENIA_WARTOSC_BRUTTO = "WARTOSC_BRUTTO";
//
//    public final static String DWR_PREFIX = "DWR_";
//    public final static String FAKTURA_ZAKUPOWA = "faktura_zakupowa";
//
//
//    public final static String DATA_WPLYWU = "DATA_WPLYWU";
//
//    public final static String KWOTA_NETTO = "KWOTA_NETTO";
//    public final static String KWOTA_BRUTTO = "KWOTA_BRUTTO";
//    public final static String KWOTA_VAT = "KWOTA_VAT";
//
//    public final static String OPIS_FAKTURY = "OPIS_FAKTURY";
//    public final Integer STATUS_JEDNOSTKA_WNIOSKUJACA_ID = new Integer(40);
//
//    public final static String ADD_BUTTON = "ADD_POZYCJA_FAKTURY";
//    public final static String SAVE_BUTTON = "SAVE_POZYCJA_FAKTURY";
//    public final static String CANCEL_BUTTON = "CANCEL_POZYCJA_FAKTURY";
//    public final static String POZYCJA_FAKTURY = "POZYCJA_FAKTURY";
//    public static final String OPIS_MERYTORYCZNY = "OPIS_MERYTORYCZNY";
//    public static final String WALUTA = "WALUTA";
//    public static final String DATA_WYSTAWIENIA = "DATA_WYSTAWIENIA";
//    public static final String KURS = "KURS";
//    public static final String KWOTA_WYPLATY = "KWOTA_WYPLATY";
//    public static final String WNIOSKUJACY = "WNIOSKUJACY";

    public static final String STATUS = "STATUS";
    public static final String STATUSCN_PROCESS_CREATE = "process_creation";

    public static final String SAVE_REJESTR_BUTTON = "SAVE_REJESTR_BUTTON";
    public static final String ADD_MODUL_BUTTON = "ADD_MODUL";
    public static final String POPUP_MODUL_CANCEL_BUTTON = "POPUP_MODUL_CANCEL";
    public static final String POPUP_MODUL_SAVE_BUTTON = "POPUP_MODUL_SAVE";
    public static final String POPUP_MODUL_ID = "POPUP_MODUL_ID";
    public static final String EDIT_BUTTON = "EDIT";
    public static final String VIEW_BUTTON = "VIEW";
    public static final String DEL_BUTTON = "DEL";
    public static final String ADD_ZR_FINANSOWANIA_BUTTON = "ADD_ZR_FINANSOWANIA";
    public static final String ZR_FINANS_CANCEL_BUTTON = "ZR_FINANS_CANCEL";
    public static final String ZR_FINANS_SAVE_BUTTON = "ZR_FINANS_SAVE";
    public static final String FINANCIAL_ID = "FINANCIAL_ID";
    public static final String UPDATE_EURO_IN_MODULES_PROCEDURE = "PROCEDURE_UPDATE_EURO_IN_MODULES";
    public static final String CREATE_PROCESS = "process_creation";
    public static final String TABLE_NAME_ZDARZENIE = "DSG_UEK_ZAM_PUB_ZDARZENIA";
    public static final String ZAMOWIENIE_PUBLICZNE_MULTIPLE_VAL = "DSG_UEK_ZAM_PUB_MULTIPLE_VALUE";
    public static final String DICTFIELD_ZDARZENIA_DESCRIPTION = "DESCRIPTION";
    public static final String DICTFIELD_ZDARZENIA_DATE = "DATE";
    public static final String DICTFIELD_ZDARZENIA_ENTRYID = "�NTRYID";


    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {

    }

    @Override
    public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException {
        super.onSaveActions(kind, documentId, fieldValues);
        Document doc = Document.find(documentId);
        FieldsManager fieldsManager = doc.getFieldsManager();
//        Map<String, Object> fmValues = fieldsManager.getFieldValues();
//        Object status = fmValues.get(STATUS);
        EnumItem status = FieldsManagerUtils.getEnumItem(fieldsManager, STATUS);
        if (status == null || status.getCn().equalsIgnoreCase(CREATE_PROCESS)) {

        }
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {

    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();

        // todo init from erp: ZAPOTRZEBOWANIA

        DSUser author = DSUser.getLoggedInUser();
        toReload.put(FIELD_WARTOSCI_ZAM_OS_USTALAJACA, author.getId());

        DwrDictionaryBase kryteriaOcenyDict = DwrDictionaryFacade.dictionarieObjects.get(DICT_KRYTERIA_OCENY);
        Map<String, FieldData> defaultKryteriumOcenyValues = new HashMap<String, FieldData>();
        defaultKryteriumOcenyValues.put(DICTFIELD_KRYTERIUM_OCENY_OPIS, new FieldData(Field.Type.STRING, KRYTERIUM_OCENY_OPIS_DEFAULT));
        defaultKryteriumOcenyValues.put(DICTFIELD_KRYTERIUM_OCENY_PROCENT, new FieldData(Field.Type.MONEY, new BigDecimal(100d)));
        long defaultKryteriumOcenyId = kryteriaOcenyDict.add(defaultKryteriumOcenyValues);
        toReload.put(DICT_KRYTERIA_OCENY, defaultKryteriumOcenyId);

        DSDivision[] divisions = author.getDivisions();
        if (divisions.length > 0) {
            DSDivision div = divisions[0];
            toReload.put(FIELD_JEDNOSTKA_REALIZUJACA, div.getId());
        }

        Collection<String> authorSupervisors = UekProcessHelper.getUsersInUserDivision(author, "profile.id.kierownik", -1);
        for (String supervisorName : authorSupervisors) {
            DSUser supervisor = DSUser.findByUsername(supervisorName);
            toReload.put(FIELD_OS_PROWADZACA_SPRAWE, supervisor.getId());
        }

        fm.reloadValues(toReload);
    }

    @Override
    public void setAdditionalValues(FieldsManager fieldsManager, Integer valueOf, String activity) throws EdmException {
        try {
            ActivityInfo activityInfo = new Jbpm4ActivityInfo(activity);
            String taskName = (String) activityInfo.get("task_name");

//            if (taskName == null || taskName.equals(STATUSCN_PROCESS_CREATE)) {
//                List<Object> dict = (List<Object>) fieldsManager.getValue(DICT_KRYTERIA_OCENY);
//                if (dict == null) {
//                    dict = new ArrayList<Object>();
//                }
//                if (dict.isEmpty()) {
//                    Map<String, Object> defaultKryteria = new HashMap<String, Object>();
//                    defaultKryteria.put("ID", 1);
//                    defaultKryteria.put("OPIS", "najni�sza cena");
//                    defaultKryteria.put("PROCENT", new BigDecimal(100));
//                    dict.add(defaultKryteria);
//
//                    fieldsManager.getFieldValues().put(DICT_KRYTERIA_OCENY, dict);
//                    fieldsManager.reloadValues(fieldsManager.getFieldValues());
//                }
//            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public static String joinDictionaryWithField(String dictionaryCn, String fieldCn) {
        return dictionaryCn + '_' + fieldCn;
    }

    @Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        //euro na popup-ie
        updateEuroDwr(values, FIELD_POPUP_MODUL_WARTOSC_NETTO, FIELD_POPUP_MODUL_WARTOSC_NETTO_EURO, true, true);
        //euro na formatce
        updateEuroDwr(values, FIELD_CZESC_ZAMOWIENIA_WARTOSC, FIELD_CZESC_ZAMOWIENIA_WARTOSC_EUR, true, true);
        //euro w slowniku wielowartosciowym
        updateEuroInModulesDwr(values, DICT_MODULY_ZAMOWIENIA, true);


        StringBuilder msgBuilder = new StringBuilder();
        try {
            //Popup Rejestr
            if (DwrUtils.isSenderField(true, values, SAVE_REJESTR_BUTTON)) {
                generateDocument(values, fm);
                //Popup Modu�y
            } else if (DwrUtils.isSenderField(true, values, ADD_MODUL_BUTTON, POPUP_MODUL_CANCEL_BUTTON)) {
                ModulyZamowieniaPopup.clearPopup(values, fm);
            } else if (DwrUtils.isSenderField(true, values, POPUP_MODUL_SAVE_BUTTON)) {
                ModulyZamowieniaPopup.saveEntry(values, fm);
                ModulyZamowieniaPopup.clearPopup(values, fm);

                //suma wielu pozycji slownikowych
                updateSumaModulowByDatabaseData(values);
            } else if (DwrUtils.isSenderField(true, values, ModulyZamowieniaPopup.ENTRY_DICTIONARY)) {
                String entryButtonName = DwrUtils.senderButtonFromDictionary(values, DICT_MODULY_ZAMOWIENIA, EDIT_BUTTON);
                if (entryButtonName == null) {
                    entryButtonName = DwrUtils.senderButtonFromDictionary(values, DICT_MODULY_ZAMOWIENIA, VIEW_BUTTON);
                }
                //klikni�to edit lub view button
                if (entryButtonName != null) {
                    Integer rowNo = Integer.parseInt(entryButtonName.substring(entryButtonName.lastIndexOf("_") + 1));
                    ModulyZamowieniaPopup.updatePopup(values, fm, rowNo);

                    updateSumaModulowByDatabaseData(values);
                }
                //klikni�to delete button
                else {
                    String delButtonName = DwrUtils.senderButtonFromDictionary(values, DICT_MODULY_ZAMOWIENIA, DEL_BUTTON);
                    if (delButtonName != null) {
                        Integer rowNo = Integer.parseInt(delButtonName.substring(delButtonName.lastIndexOf("_") + 1));
                        ModulyZamowieniaPopup.deleteEntry(values, fm, rowNo);
                    }

                    updateSumaModulowByDatabaseData(values);
                }
            }


            //Popup Zrodla Finansowania
            if (DwrUtils.isSenderField(true, values, ADD_ZR_FINANSOWANIA_BUTTON, ZR_FINANS_CANCEL_BUTTON)) {
                ZrodlaFinansowaniaPoopup.clearPopup(values, fm);
            } else if (DwrUtils.isSenderField(true, values, ZR_FINANS_SAVE_BUTTON)) {
                ZrodlaFinansowaniaPoopup.saveEntry(values, fm);
                ZrodlaFinansowaniaPoopup.clearPopup(values, fm);
            } else if (DwrUtils.isSenderField(true, values, ZrodlaFinansowaniaPoopup.ENTRY_DICTIONARY)) {
                String entryButtonName = DwrUtils.senderButtonFromDictionary(values, ZrodlaFinansowaniaPoopup.ENTRY_DICTIONARY, EDIT_BUTTON);
                if (entryButtonName == null) {
                    entryButtonName = DwrUtils.senderButtonFromDictionary(values, ZrodlaFinansowaniaPoopup.ENTRY_DICTIONARY, VIEW_BUTTON);
                }
                //klikni�to edit lub view button
                if (entryButtonName != null) {
                    Integer rowNo = Integer.parseInt(entryButtonName.substring(entryButtonName.lastIndexOf("_") + 1));
                    ZrodlaFinansowaniaPoopup.updatePopup(values, fm, rowNo);

                }
                //klikni�to delete button
                else {
                    String delButtonName = DwrUtils.senderButtonFromDictionary(values, ZrodlaFinansowaniaPoopup.ENTRY_DICTIONARY, DEL_BUTTON);
                    if (delButtonName != null) {
                        Integer rowNo = Integer.parseInt(delButtonName.substring(delButtonName.lastIndexOf("_") + 1));
                        ZrodlaFinansowaniaPoopup.deleteEntry(values, fm, rowNo);
                    }
                }
            } else if (DwrUtils.isSenderField(true, values, DICT_KRYTERIA_OCENY)) {
                String walidacjaValue = KRYTERIUM_OCENY_PROCENT_INVALID;

                FieldData kryteria = values.get(DwrUtils.dwr(DICT_KRYTERIA_OCENY));
                if (kryteria != null) {
                    BigDecimal res = DwrUtils.sumDictionaryMoneyFields(values, DICT_KRYTERIA_OCENY, DICTFIELD_KRYTERIA_OCENY_PROCENT);

                    if (res.doubleValue() == 100d)
                        walidacjaValue = KRYTERIUM_OCENY_PROCENT_VALID;
                }

                FieldData walidacja = values.get(DwrUtils.dwr(FIELD_KRYTERIA_OCENY_WALIDACJA));
                walidacja.setStringData(walidacjaValue);
            }


            if (DwrUtils.isSenderField(true, values, DICT_ZDARZENIA)) {
//                FieldData dictZdarzenia = values.get(DwrUtils.dwrAdd(DICT_ZDARZENIA));
//                Object zdarzenia = dictZdarzenia.getData();
//
//                Collection<Map<String, FieldData>> zdarzeniaData = DwrUtils.getDictionaryData(values, DICT_ZDARZENIA);
//                Zdarzenie.findAll();
//
//                String author = null;
//                Long docId = fm.getDocumentId();
//                if (docId != null) {
//                    Document doc = Document.find(docId);
//                    author = doc.getAuthor();
//                } else {
//                    author = DSUser.getLoggedInUser().getName();
//                }
//                List<pl.compan.docusafe.core.calendar.Calendar> cal = pl.compan.docusafe.core.calendar.Calendar.findUserCalendars(author);
//                cal.get(0).


            }


        } catch (Exception e) {
            log.error(e.getMessage(), e);
            msgBuilder.append(e.getMessage());
        } finally {
            DSApi.close();
        }
        if (msgBuilder.length() > 0) {
            values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
            return new Field("messageField", msgBuilder.toString(), Field.Type.BOOLEAN);
        }
        return null;
    }

    private void updateSumaModulow(Map<String, FieldData> values) {
        updateSumDwr(values, DICT_MODULY_ZAMOWIENIA, DICTFIELD_POPUP_MODUL_WARTOSC_NETTO, FIELD_MODUL_SUM_WARTOSC_NETTO);
        updateSumDwr(values, DICT_MODULY_ZAMOWIENIA, DICTFIELD_POPUP_MODUL_WARTOSC_NETTO_EURO, FIELD_MODUL_SUM_WARTOSC_NETTO_EURO);
        updateSumDwr(values, DICT_MODULY_ZAMOWIENIA, DICTFIELD_POPUP_MODUL_WARTOSC_BRUTTO, FIELD_MODUL_SUM_WARTOSC_BRUTTO);
        updateSumDwr(values, DICT_MODULY_ZAMOWIENIA, DICTFIELD_POPUP_MODUL_FINANS_BRUTTO, FIELD_MODUL_SUM_FINANSOWANIE_BRUTTO);
    }

    private void updateSumaModulowByDatabaseData(Map<String, FieldData> values) {
        Map<String, String> map = ZamowieniePubliczneModuly.getMapToHbn();
        updateSumByDatabaseData(values, DICT_MODULY_ZAMOWIENIA, FIELD_MODUL_SUM_WARTOSC_NETTO, ZamowieniePubliczneModuly.class, "id", map.get(DICTFIELD_POPUP_MODUL_WARTOSC_NETTO));
        updateSumByDatabaseData(values, DICT_MODULY_ZAMOWIENIA, FIELD_MODUL_SUM_WARTOSC_NETTO_EURO, ZamowieniePubliczneModuly.class, "id", map.get(DICTFIELD_POPUP_MODUL_WARTOSC_NETTO_EURO));
        updateSumByDatabaseData(values, DICT_MODULY_ZAMOWIENIA, FIELD_MODUL_SUM_WARTOSC_BRUTTO, ZamowieniePubliczneModuly.class, "id", map.get(DICTFIELD_POPUP_MODUL_WARTOSC_BRUTTO));
        updateSumByDatabaseData(values, DICT_MODULY_ZAMOWIENIA, FIELD_MODUL_SUM_FINANSOWANIE_BRUTTO, ZamowieniePubliczneModuly.class, "id", map.get(DICTFIELD_POPUP_MODUL_FINANS_BRUTTO));
    }

    /**
     * @param dwrValues      wartosci dwr z przedrostkami dwr
     * @param dict           bez przedrostka dwr
     * @param dictFieldToSum bez przedrostka dwr
     * @param resultField    bez przedrostka dwr
     */
    private static void updateSumDwr(Map<String, FieldData> dwrValues, String dict, String dictFieldToSum, String resultField) {
        BigDecimal res = DwrUtils.sumDictionaryMoneyFields(dwrValues, dict, dictFieldToSum);
        FieldData sum = dwrValues.get(DwrUtils.dwr(resultField));
        if (sum != null)
            sum.setMoneyData(res);
    }

    private static void updateSumByDatabaseData(Map<String, FieldData> dwrValues, String dict, String resultField, Class clzz, String identifiedHbnColumn, String toSumHbnColumn) {
        try {
            Set<Long> ids = DwrUtils.getMultipleDictionaryIds(dwrValues, dict);
            BigDecimal res = getSumFromDatabase(clzz, identifiedHbnColumn, ids, toSumHbnColumn);
            FieldData sum = dwrValues.get(DwrUtils.dwr(resultField));
            if (sum != null)
                sum.setMoneyData(res);
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }

    private static BigDecimal getSumFromDatabase(Class clzz, String identifiedHbnColumn, Collection ids, String toSumHbnColumn) throws EdmException {
        DSContextOpener opener = null;
        try {
            opener = DSContextOpener.openAsAdminIfNeed();
            Criteria criteria = DSApi.context().session().createCriteria(clzz);
            criteria.add(Restrictions.in(identifiedHbnColumn, ids));
            criteria.setProjection(Projections.sum(toSumHbnColumn));
            BigDecimal sum = (BigDecimal) criteria.uniqueResult();
            return sum;
        } finally {
            DSContextOpener.closeIfNeed(opener);
        }
//        @Column(name = "POPUP_MODUL_WARTOSC_NETTO")
//        private BigDecimal wartoscNetto;
//        @Column(name = "POPUP_MODUL_WARTOSC_NETTO_EURO")
//        private BigDecimal wartoscNettoEuro;
//        @Column(name = "POPUP_MODUL_WARTOSC_BRUTTO")
//        private BigDecimal wartoscNettoBrutto;
//
//        BigDecimal res = DwrUtils.sumDictionaryMoneyFields(dwrValues, dict, dictFieldToSum);
//        FieldData sum = dwrValues.get(DwrUtils.dwr(resultField));
//        if (sum != null)
//            sum.setStringData(res.toString());
    }

    /**
     * @param dwrValues       wartosci dwr z przedrostkami dwr
     * @param valueField      bez przedrostka dwr
     * @param calculatedField bez przedrostka dwr
     */
    private void updateEuroDwr(Map<String, FieldData> dwrValues, String valueField, String calculatedField, boolean valueIsSender, boolean euroIsSender) {
        FieldData value = dwrValues.get(DwrUtils.dwr(valueField));
        FieldData kursEuro = dwrValues.get(DwrUtils.dwr(FIELD_KURS_EURO));

        if (value == null || value.getMoneyData() == null) return;
        if (kursEuro == null || kursEuro.getMoneyData() == null) return;

        if ((euroIsSender || valueIsSender) && !((valueIsSender && !value.isSender() || (euroIsSender && !value.isSender()))))
            return;

        FieldData euro = dwrValues.get(DwrUtils.dwr(calculatedField));
        if (euro == null)
            euro = new FieldData(Field.Type.MONEY, 0d);
        euro.setMoneyData(new BigDecimal(kursEuro.getMoneyData().doubleValue() * value.getMoneyData().doubleValue()));
        dwrValues.put(calculatedField, euro);
    }

    private void updateEuroInModulesDwr(Map<String, FieldData> dwrValues, final String dictCn, boolean checkIfEuroFieldIsSender) throws EdmException {
        FieldData kursEuro = dwrValues.get(DwrUtils.dwr(FIELD_KURS_EURO));
        if (kursEuro == null || kursEuro.getMoneyData() == null || (checkIfEuroFieldIsSender && !kursEuro.isSender()))
            return;

        FieldData dict = dwrValues.get(DwrUtils.dwr(dictCn));
        if (dict == null) return;
        Set<Long> ids = DwrUtils.getMultipleDictionaryIds(dwrValues, dictCn);
        if (ids.isEmpty()) return;

        EdmException exception = null;

        DSContextOpener opener = DSContextOpener.openAsAdminIfNeed();
        try {
            for (Long idRow : ids) {
                try {
                    String sql = "exec " + UPDATE_EURO_IN_MODULES_PROCEDURE + " ?, ?";
                    CallableStatement stmt = DSApi.context().prepareCall(sql);
                    stmt.setLong(1, idRow);
                    stmt.setDouble(2, kursEuro.getMoneyData().doubleValue());
                    boolean result = stmt.execute();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                    exception = new EdmException(e.getMessage(), e);
                }
            }

            FieldData newDict = new FieldData(Field.Type.DICTIONARY, ids);
            dwrValues.put(DwrUtils.dwr(dictCn), newDict);

            updateSumaModulowByDatabaseData(dwrValues);
        } finally {
            DSContextOpener.closeIfNeed(opener);
        }

        if (exception != null)
            throw exception;
    }

    private void generateDocument(Map<String, FieldData> values, FieldsManager fm) throws EdmException {

        boolean wasOpened = false;
        try {
            wasOpened = DSApi.openContextIfNeeded();
            DSApi.context().begin();
            Long documentId;

            DocumentKind rejestr = DocumentKind.findByCn("rejestr");

            documentId = fm.getDocumentId();
            // OfficeDocument doc = OfficeDocument.find(documentId);
            DocumentKind zamowienie_publiczne = DocumentKind.findByCn("zamowienie_publiczne");

            Document generatedDocument = new Document(
                    "Rejestr",
                    "");

            // generatedDocument.setCreatingUser(DSApi.context().getPrincipalName());//tutaj zmienic na recipenta

            // generatedDocument.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
            // generatedDocument.setDivisionGuid(DSDivision.ROOT_GUID);
            generatedDocument.setDocumentKind(rejestr);
            // generatedDocument.setSummary(rejestr.getName());
            //  generatedDocument.setAssignedDivision(DSDivision.ROOT_GUID);
            generatedDocument.setFolder(Folder.getRootFolder());
            generatedDocument.create();


            //finalizeInteralDocument(generatedDocument);
            // generatedDocument.setArchived(true);
            // DwrUtils.updateMultipleDictionary(values,"To zam pub", generatedDocument.getInDocumentId().longValue(),null);


            FieldsManager fm_rejestr = generatedDocument.getFieldsManager();

            Map new_values = new HashMap();
            new_values.put("DATA_DOK", values.get("DWR_DATA_DOK").getDateData());
            new_values.put("DATA_KONCA", values.get("DWR_DATA_KONCA").getDateData());
            new_values.put("KWOTA", values.get("DWR_KWOTA").getMoneyData());
            new_values.put("OPIS", values.get("DWR_OPIS").getStringData());
            new_values.put("UMOWA_ERP", values.get("DWR_UMOWA_ERP").getEnumValuesData().getSelectedId());

            new_values.put("RODZAJ_DOKUMENTU", values.get("DWR_RODZAJ_DOKUMENTU").getEnumValuesData().getSelectedId());

            new_values.put("TO_ZAMOWIENIE_PUBLICZNE", documentId);


            if (values.get("DWR_PODTYP") != null) {
                new_values.put("PODTYP", values.get("DWR_PODTYP").getEnumValuesData().getSelectedId());
            }


            rejestr.setOnly(generatedDocument.getId(), new_values);

            // Map ID = new HashMap();
            // ID.put("id", new FieldData(Field.Type.LONG, generatedDocument.getId()));

            //values.get("DWR_TO_REJESTR").setDictionaryData(ID);
            DwrUtils.updateMultipleDictionary(values, "DWR_TO_REJESTR", generatedDocument.getId(), null);

            DSApi.context().session().save(generatedDocument);
            DSApi.context().commit();

        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        } finally {
            DSApi.closeContextIfNeeded(wasOpened);
        }

    }

    public void finalizeInteralDocument(OfficeDocument document) throws EdmException {
        ((OutOfficeDocument) document).setInternal(true);
        try {
            java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

            DSUser author = DSApi.context().getDSUser();
            String user = author.getName();
            String fullName = author.getLastname() + " " + author.getFirstname();

            perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

            Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
            perms.addAll(documentPermissions);
            ((AbstractDocumentLogic) document.getDocumentKind().logic()).setUpPermission(document, perms);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }


//    /**
//     * Checks if button of given name in given multipleDictionary was clicked
//     *
//     * @param dictionaryName
//     * @param buttonName
//     * @return Name of clicked button, or null if no button was clicked
//     * */
//    private static String senderButtonFromDictionary(Map<String, FieldData> values, String dictionaryName, String buttonName) throws EdmException {
//
//        Map<String, FieldData> buttons = DwrUtils.getDictionaryFields(values, dictionaryName, buttonName);
//
//        for(Map.Entry<String, FieldData> button : buttons.entrySet()){
//            if(button.getValue().getType().equals(Field.Type.BUTTON) && button.getValue().getButtonData().isClicked()){
//                button.getValue().getButtonData().setClicked(false);
//                return button.getKey();
//            }
//        }
//
//        return null;
//    }
//
//    private void calculateKWOTA(Map<String, FieldData> values) {
//
//        BigDecimal net = values.get(DWR_PREFIX+KWOTA_NETTO).getMoneyData();
//        BigDecimal gross = values.get(DWR_PREFIX+KWOTA_BRUTTO).getMoneyData();
//        BigDecimal vat = values.get(DWR_PREFIX + KWOTA_VAT).getMoneyData();
//
//        if((DWR_PREFIX+KWOTA_NETTO).equals(DwrUtils.getSenderField(values).getKey()) && net != null){
//            if(vat != null){
//                gross = net.add(vat);
//            }else if(gross != null){
//                vat = gross.subtract(net);
//            }
//        } else if((DWR_PREFIX+KWOTA_BRUTTO).equals(DwrUtils.getSenderField(values).getKey()) && gross != null){
//            if(net != null){
//                vat = gross.subtract(net);
//            }else if(vat != null){
//                net = gross.subtract(vat);
//            }
//        } else if((DWR_PREFIX+KWOTA_VAT).equals(DwrUtils.getSenderField(values).getKey()) && vat != null){
//            if(net != null){
//                gross = net.add(vat);
//            }else if(gross != null){
//                net = gross.subtract(vat);
//            }
//        }
//
//        values.get(DWR_PREFIX+KWOTA_NETTO).setMoneyData(net);
//        values.get(DWR_PREFIX+KWOTA_BRUTTO).setMoneyData(gross);
//        values.get(DWR_PREFIX+KWOTA_VAT).setMoneyData(vat);
//
//    }
//
//
//    private void setCurrencyRate(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
//
//        if(DwrUtils.isNotNull(values,DWR_PREFIX+WALUTA)){
//            Integer currencyId = Integer.parseInt(values.get(DWR_PREFIX + WALUTA).getEnumValuesData().getSelectedId());
//            String swift = fm.getField(WALUTA).getEnumItem(currencyId).getCn();
//            if(!DwrUtils.isNotNull(values,DWR_PREFIX + DATA_WYSTAWIENIA)){
//                throw new EdmException("Data Wystawienia jest wymagana aby ustali� w�a�ciwy kurs waluty");
//            }
//            Date date = values.get(DWR_PREFIX + DATA_WYSTAWIENIA).getDateData();
//            BigDecimal rate = getRate(swift, date);
//            if(rate != null){
//                values.get(DWR_PREFIX + KURS).setMoneyData(rate);
//            }else{
//                throw new EdmException("Nie uda�o si� ustali� w�a�ciwego kursu waluty");
//            }
//        }
//    }
//
//    /**
//     * TODO
//     * @param swift
//     * @param date
//     * @return
//     */
//    private BigDecimal getRate(String swift, Date date) {
//        return new BigDecimal(1.2345);
//    }


    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) {
        Map<String, Object> results = super.setMultipledictionaryValue(dictionaryName, values);
//
//        if(results == null){
//            results = new HashMap<String, Object>();
//        }
//
//        if(dictionaryName.equals(ZrodloFinansowaniaUtils.STAWKI_VAT)){
//            try {
//                results.putAll(calculateGrossAmount(values));
//            } catch (EdmException e) {
//                log.error(e.getMessage(),e);
//            }
//        }else if(dictionaryName.equals(WNIOSKUJACY)){
//            try {
//                results.putAll(fillWnioskujacyFields(values));
//            } catch (EdmException e) {
//                log.error(e.getMessage(),e);
//            }
//        }

        return results;

    }
//
//    private Map<String, Object> fillWnioskujacyFields(Map<String, FieldData> values) throws EdmException {
//        final String USER_FIELD = WNIOSKUJACY+"_WN_USER";
//        final String DIVISION_FIELD = WNIOSKUJACY+"_WN_DIVISION";
//
//
//        HashMap<String, Object> results = new HashMap<String, Object>();
//        Map<String,String> options = new HashMap<String, String>();
//        List<String> selected = new ArrayList<String>();
//
//        if(DwrUtils.isSenderField(values,USER_FIELD)){
//            DSDivision[] divisions = null;
//            if(DwrUtils.isNotNull(values, USER_FIELD)){
//                Long userId = Long.parseLong(values.get(USER_FIELD).getEnumValuesData().getSelectedId());
//                DSUser user = DSUser.findById(userId);
//                divisions = user.getOriginalDivisionsWithoutGroup();
//                if(divisions == null || divisions.length == 0){
//                    throw new EdmException("Wybrany u�ytkownik nie znajduje si� w �adnym dziale");
//                }
//                selected.add(divisions[0].getId().toString());
//            }
//            //Je�eli nie wybrano usera trzeba przywr�ci� wszystkie divisiony
//            else{
//                divisions = DSDivision.getOnlyDivisions(false);
//                selected.add("");
//            }
//
//            for(DSDivision div : divisions){
//                options.put(div.getId().toString(),div.getName());
//            }
//
//
//
//
//            EnumValues divisionValue = new EnumValues(options,selected);
//            new FieldData(Field.Type.ENUM,divisionValue);
//
//            results.put(DIVISION_FIELD, divisionValue);
//        }else if(DwrUtils.isSenderField(values,DIVISION_FIELD)){
//            //TODO Przygotowa� ewentualne automatyczne wybieranie usera z division
//        }
//
//        return results;
//    }
//
//
//    private List<Map<String, String>> zwrocListeZTabeli(
//            Collection<EnumItem> availableItems) {
//        List<Map<String,String>>itemy = new ArrayList<Map<String,String>>();
//        HashMap< String, String> mapa=new HashMap<String, String>();
//        mapa.put("", "--wybierz--");
//        itemy.add(mapa);
//        for(EnumItem e:availableItems){
//            mapa=new HashMap<String, String>();
//            mapa.put(e.getId().toString(), e.getTitle());
//            itemy.add(mapa);
//        }
//        return itemy;
//    }
//
//    private Map<String, Object> calculateGrossAmount(Map<String, FieldData> values) throws EdmException {
//        final String NET_AMOUNT_FIELD = ZrodloFinansowaniaUtils.STAWKI_VAT+"_" + ZrodloFinansowaniaUtils.NET_AMOUNT;
//        final String VAT_RATE_FIELD = ZrodloFinansowaniaUtils.STAWKI_VAT+"_" + ZrodloFinansowaniaUtils.VAT_RATE;
//        final String GROSS_AMOUNT_FIELD = ZrodloFinansowaniaUtils.STAWKI_VAT+"_" + ZrodloFinansowaniaUtils.GROSS_AMOUNT;
//
//        boolean wasOpened = false;
//        HashMap<String, Object> results = new HashMap<String, Object>();
//
//        try{
//            wasOpened = DSApi.openContextIfNeeded();
//
//            if (DwrUtils.isNotNull(values, NET_AMOUNT_FIELD, VAT_RATE_FIELD)
//                    && (values.get(NET_AMOUNT_FIELD).isSender() || values.get(VAT_RATE_FIELD).isSender())) {
//
//
//                int rateId = Integer.parseInt(values.get(VAT_RATE_FIELD).getEnumValuesData().getSelectedId());
//
//                BigDecimal stawka = BigDecimal.valueOf(VatRate.find(rateId).getProcent()).movePointLeft(2).setScale(2);
//
//                BigDecimal netAmount = values.get(NET_AMOUNT_FIELD).getMoneyData();
//                BigDecimal grossAmount;
//
//                if (stawka.equals(BigDecimal.ZERO.setScale(2))) {
//                    grossAmount = netAmount;
//                } else {
//                    grossAmount = netAmount.multiply(stawka.add(BigDecimal.ONE)).setScale(2, BigDecimal.ROUND_HALF_DOWN);
//
//                }
//                results.put(GROSS_AMOUNT_FIELD, grossAmount);
//            }
//
//        }finally {
//            DSApi.closeContextIfNeeded(wasOpened);
//        }
//
//        return results;
//    }


    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
//        final String KONTROLA_RACHUNKOWA = "kontrola_formalno_rachunkowa";
//
//        AssignmentHandler handler = null;
//        if(KONTROLA_RACHUNKOWA.equals(acceptationCn)){
//
//            //todo przygotowa� wyb�r innego handlera
//            handler = new ProfileAssignmentHandler();
//            ((ProfileAssignmentHandler) handler).setProfileIdAdds("profile.id."+KONTROLA_RACHUNKOWA);
//        }
//
//        if(handler == null){
//            handler = new AssigneeHandler();
//        }
//
//        try {
//            handler.assign(assignable, openExecution);
//        } catch (Exception e) {
//            log.error(e.getMessage(),e);
//            return false;
//        }
//
//        return true;
        return false;
    }

    @Override
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException {
//        if (documentId == null) {
//            return;
//        }
//
//        if(STATUS_JEDNOSTKA_WNIOSKUJACA_ID.equals(values.get("STATUS"))){
//            validateReceiveDate(values, kind, documentId);
//        }
    }

//    private void validateReceiveDate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException {
//        String userName = DSApi.context().getPrincipalName();
//
//        Set<Object> wnioskujacyIds = ((Map<String,Set<Object>>) findProcessVariable(documentId, "applicants")).get(userName);
//
//        HashSet<String> stringIds = new HashSet<String>();
//        for(Object id : wnioskujacyIds){
//            stringIds.add(id.toString());
//        }
//
//        if(stringIds.isEmpty()){
//            return;
//        }
//
//        Map<String,Map> mDicValues = (Map<String, Map>) values.get("M_DICT_VALUES");
//        if (mDicValues != null) {
//            Pattern pat = Pattern.compile("WNIOSKUJACY_(\\d+)");
//
//            for(Map.Entry<String,Map> dicEntry : mDicValues.entrySet()){
//                Matcher matcher = pat.matcher(dicEntry.getKey());
//                if(matcher.find()){
//                    if(stringIds.contains(dicEntry.getValue().get("ID").toString())){
//                        if(((FieldData) dicEntry.getValue().get("DATA_ODBIORU")).getDateData() == null){
//                            throw new EdmException("Nie wszystkie wymagane pola daty odbioru zosta�y wype�nione");
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//
//    /**
//     * Znajduje zmienn� procesow� dla danego dokumentu
//     * @param documentId
//     * @param variableName
//     * @return
//     */
//    private Object findProcessVariable(Long documentId, String variableName){
//        RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
//        if(runtimeService != null){
//            ExecutionQuery query = runtimeService.createExecutionQuery().processVariableValueEquals("docId",documentId);
//            Execution execution = query.list().size() >0 ? query.list().get(0) : null;
//            if (execution != null) {
//                return runtimeService.getVariable(execution.getId(), variableName);
//            }
//        }
//        return null;
//    }
//
//    /**
//     * Returns name of current process activity
//     * @param documentId
//     * @return
//     */
//    private String currentProcessActivity(Long documentId){
//        RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
//        if(runtimeService != null){
//            ExecutionQuery query = runtimeService.createExecutionQuery().processVariableValueEquals("docId",documentId);
//            List<Execution> execList =  query.list();
//            if(execList != null){
//                for(int i = 0 ; i < execList.size(); i++){
//                    Execution execution = query.list().get(i);
//                    if(execution.getActivityId() != null ){
//                        return execution.getActivityId();
//                    }
//                }
//            }
//        }
//        return null;
//    }


    public void setAdditionalTemplateValuesUnsafe(long docId, Map<String, Object> values, Map<Class, Format> defaultFormatters) throws EdmException {
        FieldsPrintProvider fieldPrintProvider = new ZamowieneiPubliczneFieldsPrintProvider();
        fieldPrintProvider.setAdditionalTemplateValuesUnsafe(docId, values, defaultFormatters);
    }
}

class ZamowieneiPubliczneFieldsPrintProvider extends MarkerFieldsPrintProvider {

    public ZamowieneiPubliczneFieldsPrintProvider() {
        super(FieldMarkers.values());
    }

    @Override
    public String getMarkerValue(Enum marker, FieldsManager fm, OfficeDocument doc) {
        switch ((FieldMarkers)marker) {
            case MODULY:
                break;
        }
        return null;
    }


    public enum FieldMarkers {
        MODULY
    }
}

class Zdarzenie {
    private String description;
    private Date date;
    private Long entryId;


    Zdarzenie(String description, Date date, Long entryId) {
        this.description = description;
        this.date = date;
        this.entryId = entryId;
    }

    public static List<Zdarzenie> findAll(Long docId) throws EdmException, SQLException {

        return MultipleDictionaryFactory.getEntries(new MultipleDictionaryFactory.SqlManager<Zdarzenie>(
                docId,
                ZamowieniePubliczneLogic.ZAMOWIENIE_PUBLICZNE_MULTIPLE_VAL,
                ZamowieniePubliczneLogic.DICT_ZDARZENIA,
                ZamowieniePubliczneLogic.TABLE_NAME_ZDARZENIE) {
            @Override
            public Zdarzenie createObject(ResultSet rs) throws SQLException {
                String desc = rs.getString(ZamowieniePubliczneLogic.DICTFIELD_ZDARZENIA_DESCRIPTION);
                Date date = rs.getDate(ZamowieniePubliczneLogic.DICTFIELD_ZDARZENIA_DESCRIPTION);
                Long entryId = rs.getLong(ZamowieniePubliczneLogic.DICTFIELD_ZDARZENIA_DESCRIPTION);
                return new Zdarzenie(desc, date, entryId);
            }

            @Override
            public Collection<String> getColumns() {
                return Arrays.asList(new String[]{
                        ZamowieniePubliczneLogic.DICTFIELD_ZDARZENIA_ENTRYID,
                        ZamowieniePubliczneLogic.DICTFIELD_ZDARZENIA_DATE,
                        ZamowieniePubliczneLogic.DICTFIELD_ZDARZENIA_DESCRIPTION});
            }
        });
    }
}