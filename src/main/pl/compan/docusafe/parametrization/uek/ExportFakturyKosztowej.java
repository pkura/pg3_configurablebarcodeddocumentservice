package pl.compan.docusafe.parametrization.uek;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axis2.AxisFault;
import org.apache.commons.dbutils.DbUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.parametrization.eod.erp.CostInvoiceLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.simple.CostInvoiceCreateProxyStub;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

public class ExportFakturyKosztowej {
	private static final String NIEPRZEKAZANO_STRING_FLAG = "nieprzekazano";

	private static final Logger log = LoggerFactory.getLogger(ExportFakturyKosztowej.class);

	private static CostInvoiceCreateProxyStub costInvoiceStub;

	private static CostInvoiceCreateProxyStub getStub() {
		return costInvoiceStub;
	}

	public ExportFakturyKosztowej() throws AxisFault, Exception {
		if (getStub() == null) {
			AxisClientConfigurator conf = new AxisClientConfigurator();
			costInvoiceStub = new CostInvoiceCreateProxyStub(conf.getAxisConfigurationContext());
			conf.setUpHttpParameters(costInvoiceStub, "/services/CostInvoiceCreateProxy");
		}
	}

	/**
	 * Wysyla do uslugi CostInvoiceCreateProxy komunikat, ktory zawiera dane
	 * faktury
	 * 
	 * @param documentId
	 * @return guid - nim nalezy odpytac usluge RequestService by otrzymac
	 *         status
	 */
	public String wyslij(Long documentId) {
		String guid = null;
		try {

			OMElement elem = przygotujXML(documentId);
			log.error(elem.toString());
			// costInvoiceStub.mediate();
			OMElement response = costInvoiceStub._getServiceClient().sendReceive(elem);
			log.error(costInvoiceStub._getServiceClient().getAxisService().getEndpointURL());
			guid = response.getFirstElement().getText();
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		}
		return guid;
	}

	/**
	 * Metoda przygotowuje fakture kosztowa do wysłania do zewnetrznego systemu
	 * 
	 * @param documentId
	 * @return xml faktury kosztowej zgodny ze schema Dokzak.xsd SimpleERP
	 * @throws EdmException
	 */
	private OMElement przygotujXML(Long documentId) throws EdmException {
		return przygotujXML(documentId, false, -1);
	}

	private OMElement przygotujXML(Long documentId, boolean processNotApplicableValue, Integer notApplicableValue) throws EdmException {
		// tworzenie xmla faktury zewnetrznej
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace nsS = fac.createOMNamespace("http://www.simple.com.pl/SimpleErp", "s");
		OMNamespace nsXSI = fac.createOMNamespace("http://www.w3.org/2001/XMLSchema-instance", "xsi");
		// tworzenie elementu dokzak - root
		OMElement dokzak = fac.createOMElement("dokzak", nsS);
		dokzak.addAttribute(fac.createOMAttribute("schemaLocation", nsXSI, "http://www.simple.com.pl/SimpleErp ../xsd/Dokzak.xsd"));

		try {
			// Pobranie danych faktury kosztowej
			OfficeDocument doc = OfficeDocument.find(documentId);

			FieldsManager fm = doc.getFieldsManager();
			DateFormat formatOUT = new SimpleDateFormat("yyyy-MM-dd'+'HH:mm");

			// sposobPlatnosci - warplat_id
			String sposobT = fm.getStringValue("SPOSOB");
			OMElement warplat_id;
			if ("Got�wka".equals(sposobT))
				warplat_id = dodajElementIPodrzedny("warplat_id", "type", "T", "idn", "Got�wka");
			else
				warplat_id = dodajElementIPodrzedny("warplat_id", "type", "T", "idn", "Przelew 14");
			dokzak.addChild(warplat_id);

			// stdost_id
			String stdost_idT = "SD";
			OMElement stdost_id = dodajElementIPodrzedny("stdost_id", "type", "T", "idn", stdost_idT);
			dokzak.addChild(stdost_id);

			// uzytk_id
			String uzytk_idT = "esb";
			OMElement uzytk_id = dodajElementIPodrzedny("uzytk_id", "type", "T", "idn", uzytk_idT);
			dokzak.addChild(uzytk_id);

			// waluta - waluta_id
			String waluta_idT = "PLN";
			// if (fm.getEnumItem("KWOTA_WALUTA") != null) {
			// waluta_idT = fm.getEnumItem("KWOTA_WALUTA").getCn();
			// }
			OMElement waluta_id = dodajElementIPodrzedny("waluta_id", "type", "T", "symbolwaluty", waluta_idT);
			dokzak.addChild(waluta_id);

			// typdokzak_id
			String typdokzak_idT = "FZM";
			OMElement typdokzak_id = dodajElementIPodrzedny("typdokzak_id", "type", "T", "idn", typdokzak_idT);
			dokzak.addChild(typdokzak_id);

			
			
			// dostawca_id
			if (fm.getKey("CONTRACTOR") != null) {
				Person kontrahent = Person.find(((Long)fm.getKey("CONTRACTOR")).longValue());
				if (kontrahent.getWparam() != null) {
					OMElement dostawca_id = dodajElementIPodrzedny("dostawca_id", "type", "N", "id", kontrahent.getWparam().toString());
					dokzak.addChild(dostawca_id);
				} else {
					// wystawca faktury - dostfakt_id
					String senderT = kontrahent.getOrganization(); // PZU
					String nipT = kontrahent.getNip(); // NIP
					String streetT = kontrahent.getStreet(); // ulica
					String zipT = kontrahent.getZip(); // kod-pocztowy
					String locationT = kontrahent.getLocation(); // miasto
					String krajT = DataBaseEnumField.getEnumItemForTable("dsr_country", Integer.valueOf(kontrahent.getCountry())).getCn() ; //kraj
					OMElement dostfakt_id = fac.createOMElement("dostfakt_id", null);
					OMElement nip = dodajElement("nip", nipT);
					OMElement nazwa = dodajElement("nazwa", senderT);
					OMElement kodpoczt = dodajElement("kodpoczt", zipT);
					OMElement miasto = dodajElement("miasto", locationT);
					OMElement ulica = dodajElement("ulica", streetT);
					OMElement nrdomu = dodajElement("nrdomu", "");
					OMElement nrmieszk = dodajElement("nrmieszk", "");
					OMElement kraj = dodajElementIPodrzedny("kraj_id", "type", "T", "idn", krajT);
					dostfakt_id.addChild(nip);
					dostfakt_id.addChild(nazwa);
					dostfakt_id.addChild(kodpoczt);
					dostfakt_id.addChild(miasto);
					dostfakt_id.addChild(ulica);
					dostfakt_id.addChild(nrdomu);
					dostfakt_id.addChild(nrmieszk);
					dostfakt_id.addChild(kraj);
					dokzak.addChild(dostfakt_id);
				}
			}
//			// numer konta bankowego dostawcy
			Integer nrKontaT = (Integer) fm.getEnumItem("KONTRAHENT_NR_KONTA").getId();
			if (nrKontaT != null) {
				OMElement nrKontaDost = dodajElementIPodrzedny("kontobankowe_id", "type", "T", "idm", "5");
				dokzak.addChild(nrKontaDost);
			}
			// data wystawienia - datdok
			Date dataWyst = (Date) fm.getKey("DATA_WYSTAWIENIA");
			String dataWystawieniaT = null;
			if (dataWyst != null)
				dataWystawieniaT = formatOUT.format(dataWyst);
			if (dataWystawieniaT != null) {
				OMElement datdok = dodajElement("datdok", dataWystawieniaT);
				dokzak.addChild(datdok);
			}

			// termin platnosci - datoper
			Date termin = (Date) fm.getKey("TERMIN_PLATNOSCI");
			String terminPlatnosciT = null;
			if (termin != null)
				terminPlatnosciT = formatOUT.format(termin);
			OMElement datoper = null;
			if (terminPlatnosciT != null)
				datoper = dodajElement("datoper", terminPlatnosciT);
			if (terminPlatnosciT != null)
				dokzak.addChild(datoper);
			
			// dok_uwagi
			StringBuilder sb = new StringBuilder();
			// sb.append("<![CDATA[");
			sb.append("Nazwa towaru/us�ugi: ");
			sb.append(Strings.isNullOrEmpty(fm.getKey("OPIS") != null ? fm.getKey("OPIS").toString() : "") ? NIEPRZEKAZANO_STRING_FLAG : fm
					.getKey("OPIS"));
			sb.append("; Dodatkowe informacje: ");
			sb.append(Strings.isNullOrEmpty(fm.getKey("ADDINFO") != null ? fm.getKey("ADDINFO").toString() : "") ? NIEPRZEKAZANO_STRING_FLAG
					: fm.getKey("ADDINFO"));
			// sb.append("]]>");
			OMElement dok_uwagi = dodajElement("dok_uwagi", sb.toString());
			dokzak.addChild(dok_uwagi);

			// nr faktury - dokobcy
			String nrFakturyT = fm.getStringValue("NR_FAKTURY");
			if (nrFakturyT == null)
				nrFakturyT = NIEPRZEKAZANO_STRING_FLAG;
			OMElement dokobcy = dodajElement("dokobcy", nrFakturyT);
			dokzak.addChild(dokobcy);

			// data wplywu - datprzyj
			Date dataWplywu = (Date) fm.getKey("DATA_WPLYWU_FAKTURY");
			String dataWplywuT = null;
			if (dataWplywu == null)
				dataWplywu = new Date();
			else
				dataWplywuT = formatOUT.format(dataWplywu);
			OMElement datprzyj = dodajElement("datprzyj", dataWplywuT);
			dokzak.addChild(datprzyj);
			
			String MPK = "MPK";
			List<Long> pozycjeFaktury = (List) fm.getKey(MPK);
			if (pozycjeFaktury != null) {
				// OMElement[] pozycje = new OMElement[pozycjeFaktury.size()];
				List<OMElement> pozycjeList = Lists.newArrayList();
				int i = 0;
				for (Long mpkId : pozycjeFaktury) {
					CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(mpkId);

					OMElement pozycja = fac.createOMElement("dokzakpoz", null);
					// pobieranie danych z mpk faktury:

					// vatstaw_id
					String vatstaw_idT = "22";
//					vatstaw_idT = DataBaseEnumField.getEnumItemForTable(CostInvoiceLogic.VAT_RATES_TABLE_NAME, vatRate.getRateId()).getCn();

					OMElement vatstaw_id = dodajElementIPodrzedny("vatstaw_id", "type", "T", "ids", vatstaw_idT);
					pozycja.addChild(vatstaw_id);
					
					// jm_id
					String jm_idT = "szt";
					OMElement jm_id = dodajElementIPodrzedny("jm_id", "type", "T", "idn", jm_idT);
					pozycja.addChild(jm_id);
					
					// ilosc
					String iloscT = "1";
					OMElement ilosc = dodajElement("ilosc", iloscT);
					pozycja.addChild(ilosc);

					// cena
					BigDecimal cenaT = mpk.getRealAmount();
					OMElement cena = dodajElement("cena", cenaT.setScale(2).toString());
					pozycja.addChild(cena);
					
					// kwota_netto
					BigDecimal kwota_nettoT = mpk.getRealAmount().divide(new BigDecimal(1.23), 2, BigDecimal.ROUND_HALF_DOWN);
					OMElement netto = dodajElement("kwotnett", kwota_nettoT.setScale(2).toString());
					pozycja.addChild(netto);
					
					// nrpoz
					String nrpozT = String.valueOf(i + 1);
					OMElement nrpoz = dodajElement("nrpoz", nrpozT);
					pozycja.addChild(nrpoz);

					// nazwa
					String nazwaT = "-";
					OMElement mpkNazwa = dodajElement("nazwa", nazwaT);
					pozycja.addChild(mpkNazwa);

					// rodzwytw
					String rodzwytwT = "0";
					OMElement rodzwytw = dodajElement("rodzwytw", rodzwytwT);
					pozycja.addChild(rodzwytw);

					// rodztowaru
					String rodztowaruT = "0";
					OMElement rodztowaru = dodajElement("rodztowaru", rodztowaruT);
					pozycja.addChild(rodztowaru);

					//wytwor_id
					if (mpk.getConnectedTypeId() != null) {
						OMElement wytworId = dodajElementIPodrzedny("wytwor_id", "type", "N", "id", "2");
						pozycja.addChild(wytworId);
					}
					
					// komorka_id
					if (mpk.getSubgroupId() != null
							&& DataBaseEnumField.getEnumItemForTable(CostInvoiceLogic.JEDNOSTKI_TABLE_NAME, mpk.getSubgroupId()) != null) {
						String komorkaId = DataBaseEnumField
								.getEnumItemForTable(CostInvoiceLogic.JEDNOSTKI_TABLE_NAME, mpk.getSubgroupId()).getCn();
						OMElement etap_id = dodajElementIPodrzedny("komorka_id", "type", "T", "idn", "Centrala");
						pozycja.addChild(etap_id);
					}

					// dokzapoz_zfi
//					OMElement dokzapoz_zfi = dodajElement("dokzakpoz_zfi", null);
					// dokzapoz_zf
//					OMElement dokzapoz_zf = dodajElement("dokzakpoz_zf", null);
					// kwota brutto???

//					OMElement kwota = dodajElement("kwota", mpk.getRealAmount().setScale(2).toString());
//					dokzapoz_zf.addChild(kwota);

					// procent
//					String procentT = "100";
//					OMElement procent = dodajElement("procent", procentT);
//					dokzapoz_zf.addChild(procent);

					// rodzaj
//					String rodzajT = "2";
//					OMElement rodzaj = dodajElement("rodzaj", rodzajT);
//					dokzapoz_zf.addChild(rodzaj);

//					dokzapoz_zfi.addChild(dokzapoz_zf);
//					pozycja.addChild(dokzapoz_zfi);

					pozycjeList.add(pozycja);
					i++;
				}

				OMElement dokzakpozycje = fac.createOMElement("dokzakpozycje", null);
				for (OMElement poz : pozycjeList) {
					dokzakpozycje.addChild(poz);
				}
				dokzak.addChild(dokzakpozycje);
			}
			List<Attachment> zalaczniki = doc.getAttachments();
			OMElement[] skany = new OMElement[zalaczniki.size()];
			int i = 0;
			for (Attachment zal : zalaczniki) {

				String sciezka = "";

				if (Docusafe.getAdditionProperty("erp.faktura.link_do_skanu") != null) {
					String urlSystemu = Docusafe.getAdditionProperty("erp.faktura.link_do_skanu");
					sciezka = urlSystemu;
				} else {
					sciezka = Docusafe.getBaseUrl();
				}

				sciezka += "/repository/view-attachment-revision.do?id=" + zal.getMostRecentRevision().getId();
				skany[i] = fac.createOMElement("sys_dok_zalacznik", null);
				OMElement nazwaZalacznika = dodajElement("nazwa", zal.getTitle());
				OMElement sciezkaZalacznika = dodajElement("sciezka", sciezka);
				OMElement typZalacznika = dodajElement("type", "URL");

				skany[i].addChild(nazwaZalacznika);
				skany[i].addChild(sciezkaZalacznika);
				skany[i].addChild(typZalacznika);
				i++;
			}

			OMElement sys_dok_zalaczniki = fac.createOMElement("sys_dok_zalaczniki", null);
			for (OMElement skan : skany) {
				sys_dok_zalaczniki.addChild(skan);
			}
			dokzak.addChild(sys_dok_zalaczniki);

		} catch (Exception exc) {
			log.debug(exc.getMessage(), exc);
			throw new EdmException(exc);
		}

		return dokzak;
	}

	private OMElement dodajElement(String nazwaElementu, String wartosc) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMElement element = fac.createOMElement(nazwaElementu, null);
		element.addChild(fac.createOMText(wartosc));
		return element;
	}

	private OMElement dodajElementIPodrzedny(String nazwaElementu, String atrybut, String wartoscAtrybutu, String nazwaElementuPodrzednego,
			String wartoscElementuPod) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMElement element = fac.createOMElement(nazwaElementu, null);
		if (atrybut != null) {
			element.addAttribute(fac.createOMAttribute(atrybut, null, wartoscAtrybutu));
		}
		OMElement elementPodrzedny = fac.createOMElement(nazwaElementuPodrzednego, null);
		elementPodrzedny.addChild(fac.createOMText(wartoscElementuPod));
		element.addChild(elementPodrzedny);
		return element;
	}
}
