/**
 * 
 */
package pl.compan.docusafe.parametrization.uek;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.util.HSSFColor.TAN;
import org.joda.time.DateTime;
import org.joda.time.Days;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dictionary.DictionaryUtils;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.exports.ExportedDocument;
import pl.compan.docusafe.core.exports.FixedAssetLiquidationDocument;
import pl.compan.docusafe.core.exports.FixedAssetPosition;
import pl.compan.docusafe.core.exports.FixedAssetPositionDetail;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.ams.WniosekStLogic;
import pl.compan.docusafe.parametrization.ams.hb.AmsAssetCardInfo;
import pl.compan.docusafe.parametrization.uek.hbm.CurrencyDeposit;
import pl.compan.docusafe.parametrization.uek.hbm.DelegationCost;
import pl.compan.docusafe.parametrization.uek.utils.InvoicePopupManager;
import pl.compan.docusafe.parametrization.uek.utils.InvoicePopupManagerInwest;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.office.common.ManualMultiAssignmentTabAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 29-04-2014
 * ds_trunk_2014
 * WyjazdZagranicznyLogic.java
 */
public class WyjazdZagranicznyLogic extends AbstractDocumentLogic{

	protected static Logger log = LoggerFactory.getLogger(WyjazdZagranicznyLogic.class);
	private final static StringManager SM = GlobalPreferences.loadPropertiesFile(WyjazdZagranicznyLogic.class.getPackage().getName(), null);
	
	
    private static WyjazdZagranicznyLogic instance;
    
    private static final String DELEGOWANY = "DELEGOWANY";
    private static final String DATA_WNIOSKU = "DATA_WNIOSKU";
    private static final String NUMER_WNIOSKU = "NUMER_WNIOSKU";
    private static final String PRZEJAZD = "PRZEJAZD";
    private static final String INNE_KOSZTY = "INNE_KOSZTY";
    
    private static final String WARTOSC = "WARTOSC";
    private static final String KOSZTY_NOCLEGU_PRE = "KOSZTY_NOCLEGU_PRE";
    private static final String DIETA_ZAGRANICZNA_PRE = "DIETA_ZAGRANICZNA_PRE";
    private static final String DANE_WNIOSKODAWCY = "DANE_WNIOSKODAWCY";
    private static final String DANE_WNIOSKODAWCY_DELEGOWANY = "DANE_WNIOSKODAWCY_DELEGOWANY";
    private static final String DANE_WNIOSKODAWCY_JEDNOSTKA_ORGANIZACYJNA = "DANE_WNIOSKODAWCY_JEDNOSTKA_ORGANIZACYJNA";
    
    private static final String CURRENCY_RATE = "CURRENCY_RATE";
    private static final String CURRENCY = "CURRENCY";
    private static final String MONEY_VALUE_PLN = "MONEY_VALUE_PLN";
    private static final String MONEY_VALUE = "MONEY_VALUE";
    private static final String CURRENCY_TABLE_NAME = "dsg_uek_currency";
    
    private static final String KOSZTY_NOCLEGU_PRE_CURRENCY_RATE = "KOSZTY_NOCLEGU_PRE_CURRENCY_RATE";
    private static final String KOSZTY_NOCLEGU_PRE_MONEY_VALUE_PLN = "KOSZTY_NOCLEGU_PRE_MONEY_VALUE_PLN";
    private static final String KOSZTY_NOCLEGU_PRE_MONEY_VALUE = "KOSZTY_NOCLEGU_PRE_MONEY_VALUE";
    private static final String DIETA_ZAGRANICZNA_PRE_CURRENCY_RATE = "DIETA_ZAGRANICZNA_PRE_CURRENCY_RATE";
    private static final String DIETA_ZAGRANICZNA_PRE_MONEY_VALUE_PLN = "DIETA_ZAGRANICZNA_PRE_MONEY_VALUE_PLN";
    private static final String DIETA_ZAGRANICZNA_PRE_MONEY_VALUE = "DIETA_ZAGRANICZNA_PRE_MONEY_VALUE";
    
    private static final String DWR_PREFIX = "DWR_";
    
    private final static String EDIT_BUTTON = "EDIT";
    private final static String VIEW_BUTTON = "VIEW";
    private static final String DEL_BUTTON = "DEL";
    private final static String ADD_BUTTON = "ADD_POZYCJA_FAKTURY";
    private final static String SAVE_BUTTON = "SAVE_POZYCJA_FAKTURY";
    private final static String CANCEL_BUTTON = "CANCEL_POZYCJA_FAKTURY";
    public final static String ENTRY_DICTIONARY = InvoicePopupManager.ENTRY_DICTIONARY;
    
    private static final String WNIOSKUJACY = "WNIOSKUJACY";
    private static final String SUPERVISOR_DECISION = "SUPERVISOR_DECISION";
    
    
    private static final String DATA_OD = "DATA_OD";
    private static final String DATA_DO = "DATA_DO";
    private static final String LICZBA_DNI = "LICZBA_DNI";
    
    private static final Integer MONEY_SCALE = 2;
    
	
	public static synchronized WyjazdZagranicznyLogic getInstance() {
        if (instance == null)
            instance = new WyjazdZagranicznyLogic();
        return instance;
    }

	@Override
	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException {
		if (acceptationCn.equals("przelozony")) {
			FieldsManager fm = doc.getFieldsManager();
			boolean supervisorDecision = fm.getBoolean(SUPERVISOR_DECISION);
			if (!supervisorDecision) {
				throw new EdmException("Warunkiem akceptacji delegacji jest zaznaczenie pola 'Stwierdzam, �e wyjazd pracownika nie zak��ci dzia�alno�ci dydaktycznej, naukowej i organizacyjnej jednostki'"); 
			}
			
		}
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {
		
	}
	
	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
		Map<String, Object> toReload = Maps.newHashMap();
		
		if (DSApi.context().getDSUser().getDivisionsWithoutGroup().length>0) {
			String sender = "u:"+DSApi.context().getDSUser().getName();
			sender+=(";d:"+DSApi.context().getDSUser().getDivisionsWithoutGroup()[0].getGuid());
			toReload.put("SENDER_HERE", sender);	
		} else {
			String sender = "u:"+DSApi.context().getDSUser().getName();
			toReload.put("SENDER_HERE", sender);	
		}
		
		toReload.put(DATA_WNIOSKU, new Date());
		
		fm.reloadValues(toReload);
	}
	
	
	@Override
	public void documentPermissions(Document document) throws EdmException {
	}
	
	@Override
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) {
		Map<String, Object> results = new HashMap<String, Object>();
		
		//Wylicza wartosc PLN waluta * kurs na slowniku noclegow
		if(dictionaryName.equals(KOSZTY_NOCLEGU_PRE)) {
			BigDecimal wartoscPln;
			if (values.get(KOSZTY_NOCLEGU_PRE_CURRENCY_RATE) != null && values.get(KOSZTY_NOCLEGU_PRE_MONEY_VALUE) != null) {
				wartoscPln = values.get(KOSZTY_NOCLEGU_PRE_CURRENCY_RATE).getMoneyData().multiply(values.get(KOSZTY_NOCLEGU_PRE_MONEY_VALUE).getMoneyData());
				results.put(KOSZTY_NOCLEGU_PRE_MONEY_VALUE_PLN, wartoscPln.setScale(MONEY_SCALE));
			}
		}
		
		//Wylicza wartosc PLN waluta * kurs na slowniku diet
		if(dictionaryName.equals(DIETA_ZAGRANICZNA_PRE)) {
			BigDecimal wartoscPln;
			if (values.get(DIETA_ZAGRANICZNA_PRE_CURRENCY_RATE) != null && values.get(DIETA_ZAGRANICZNA_PRE_MONEY_VALUE) != null) {
				wartoscPln = values.get(DIETA_ZAGRANICZNA_PRE_CURRENCY_RATE).getMoneyData().multiply(values.get(DIETA_ZAGRANICZNA_PRE_MONEY_VALUE).getMoneyData());
				results.put(DIETA_ZAGRANICZNA_PRE_MONEY_VALUE_PLN, wartoscPln.setScale(MONEY_SCALE));
			}
		}
		
		return results;
	}
	
	/**Zwraca mape klucz - id wpisu DataBaseEnumField // wartosc - kwota money z pola moneyValueFieldName
	 * @param values
	 * @param dictCn - cn slownika wielowartosciowego
	 * @param enumValuesFieldName
	 * @param moneyValueFieldName
	 * @param dataBaseEnumFieldTableName
	 * @return
	 * @throws NumberFormatException
	 * @throws EdmException
	 */
	private Map<Long,BigDecimal> getCostMapFromMultipleDictionary(Map<String, FieldData> values, String dictCn, String enumValuesFieldName, String moneyValueFieldName, String dataBaseEnumFieldTableName) throws NumberFormatException, EdmException {
		Map<Long,BigDecimal> sumaKosztowDelegacji = new HashMap<Long, BigDecimal>();
		
		if(DwrUtils.isNotNull(values, "DWR_" + dictCn)) {
			Map<String,FieldData> kosztyNocleguMap = (Map<String,FieldData>)values.get("DWR_" + dictCn).getData();
			int rowCounter = 1;
			BigDecimal value;
			EnumValues enumValues;
			EnumItem enumItem;
			StringBuilder fullEnumFieldName = new StringBuilder();
			fullEnumFieldName.append(dictCn).append("_").append(enumValuesFieldName).append("_");
			StringBuilder fullMoneyFieldName = new StringBuilder();
			fullMoneyFieldName.append(dictCn).append("_").append(moneyValueFieldName).append("_");
			while(true) {
				if(kosztyNocleguMap.containsKey(fullMoneyFieldName.toString() + rowCounter) && kosztyNocleguMap.containsKey(fullEnumFieldName.toString() + rowCounter)) {
					if (kosztyNocleguMap.get(fullMoneyFieldName.toString() + rowCounter) != null  && kosztyNocleguMap.get(fullMoneyFieldName.toString() + rowCounter) .getData() != null
							&& kosztyNocleguMap.get(fullEnumFieldName.toString() + rowCounter) != null  && kosztyNocleguMap.get(fullEnumFieldName.toString() + rowCounter) .getData() != null) {
						
						value = kosztyNocleguMap.get(fullMoneyFieldName.toString() + rowCounter).getMoneyData();
						enumValues = kosztyNocleguMap.get(fullEnumFieldName.toString() + rowCounter).getEnumValuesData();
						if (StringUtils.isNotBlank(enumValues.getSelectedId())){
							//enumItem = DataBaseEnumField.getEnumItemForTable(dataBaseEnumFieldTableName, Integer.valueOf(enumValues.getSelectedId()));
							Long selectedId = Long.valueOf(enumValues.getSelectedId());
							if (sumaKosztowDelegacji.get(selectedId) != null) {
								value = value.add(sumaKosztowDelegacji.get(selectedId));
								sumaKosztowDelegacji.put(selectedId, value);
							} else {
								sumaKosztowDelegacji.put(selectedId, value);
							}
							
						}
					} 
					rowCounter++;
				} else {
					break;
				}
			}
		}
		
		return sumaKosztowDelegacji;
	}
	
	/**Aktualizuje slownik zaliczek delegacji zagranicznej w zaleznosci waluta - wartosc
	 * @param values
	 * @throws EdmException
	 */
	private void fillAdvanceCostDictionary(Map<String, FieldData> values) throws EdmException {
		Map<Long,BigDecimal> sumaKosztowDelegacjiAbroad = new HashMap<Long, BigDecimal>();
		Map<Long,BigDecimal> sumaKosztowDelegacjiPln = new HashMap<Long, BigDecimal>();
		
		fillCostMap(values, sumaKosztowDelegacjiAbroad, KOSZTY_NOCLEGU_PRE, CURRENCY, MONEY_VALUE, CURRENCY_TABLE_NAME);
		fillCostMap(values, sumaKosztowDelegacjiPln, KOSZTY_NOCLEGU_PRE, CURRENCY, MONEY_VALUE_PLN, CURRENCY_TABLE_NAME);
		
		int counter = 1;
		CurrencyDeposit currencyDepositObject;
		FieldData kosztyWyjazduField = values.get("DWR_ZALICZKA_WALUTOWA_DIC");
		Map<String, FieldData> kosztyWyjazduValues = new HashMap<String, FieldData>();
		Set<Long> dictionaryIds = DwrUtils.getMultipleDictionaryIds(values, "DWR_ZALICZKA_WALUTOWA_DIC");
		
		DSApi.openAdmin();
		DSApi.context().begin();
		
		if (dictionaryIds != null && dictionaryIds.size() > 0) {
			for (Long id : dictionaryIds) {
				if (id != null) {
					currencyDepositObject = CurrencyDeposit.find(id);
					if (currencyDepositObject.getWaluta() != null && sumaKosztowDelegacjiAbroad.get(currencyDepositObject.getWaluta()) != null) {
						currencyDepositObject.setMoneyValue(sumaKosztowDelegacjiAbroad.get(currencyDepositObject.getWaluta()));
						sumaKosztowDelegacjiAbroad.remove(currencyDepositObject.getWaluta());
					}
					if (currencyDepositObject.getWaluta() != null && sumaKosztowDelegacjiPln.get(currencyDepositObject.getWaluta()) != null) {
						currencyDepositObject.setMoneyValuePln(sumaKosztowDelegacjiPln.get(currencyDepositObject.getWaluta()));
						sumaKosztowDelegacjiPln.remove(currencyDepositObject.getWaluta());
					}
					
					currencyDepositObject.setCurrencyRate(currencyDepositObject.getMoneyValuePln().divide(currencyDepositObject.getMoneyValue()));
					
					currencyDepositObject.update();
					
					kosztyWyjazduValues.put("DWR_ZALICZKA_WALUTOWA_DIC_ID_" + counter , new FieldData(Field.Type.STRING, currencyDepositObject.getId().toString()));
					kosztyWyjazduValues.put("DWR_ZALICZKA_WALUTOWA_DIC_CURRENCY_" + counter , new FieldData(Field.Type.STRING, currencyDepositObject.getWaluta().toString()));
					kosztyWyjazduValues.put("DWR_ZALICZKA_WALUTOWA_DIC_MONEY_VALUE_" + counter , new FieldData(Field.Type.MONEY, currencyDepositObject.getMoneyValue().toString()));
					if(currencyDepositObject.getMoneyValuePln() != null) kosztyWyjazduValues.put("DWR_ZALICZKA_WALUTOWA_DIC_MONEY_VALUE_PLN_" + counter++ , new FieldData(Field.Type.MONEY, currencyDepositObject.getMoneyValuePln().toString()));
				}
			}
		}
		
		if (sumaKosztowDelegacjiAbroad.size() > 0) {
			for (Map.Entry<Long,BigDecimal> entry : sumaKosztowDelegacjiAbroad.entrySet()) {
				currencyDepositObject = new CurrencyDeposit();
				currencyDepositObject.setWaluta(entry.getKey());
				currencyDepositObject.setMoneyValue(entry.getValue());
				currencyDepositObject.setMoneyValuePln(sumaKosztowDelegacjiPln.get(entry.getKey()));
				currencyDepositObject.setCurrencyRate(currencyDepositObject.getMoneyValuePln().divide(currencyDepositObject.getMoneyValue()));
				
				
				currencyDepositObject.create();
				
				kosztyWyjazduValues.put("DWR_ZALICZKA_WALUTOWA_DIC_ID_" + counter , new FieldData(Field.Type.STRING, currencyDepositObject.getId().toString()));
				kosztyWyjazduValues.put("DWR_ZALICZKA_WALUTOWA_DIC_CURRENCY_" + counter , new FieldData(Field.Type.STRING, currencyDepositObject.getWaluta().toString()));
				kosztyWyjazduValues.put("DWR_ZALICZKA_WALUTOWA_DIC_MONEY_VALUE_" + counter , new FieldData(Field.Type.MONEY, currencyDepositObject.getMoneyValue().toString()));
				if(currencyDepositObject.getMoneyValuePln() != null) kosztyWyjazduValues.put("DWR_ZALICZKA_WALUTOWA_DIC_MONEY_VALUE_PLN_" + counter++ , new FieldData(Field.Type.MONEY, currencyDepositObject.getMoneyValuePln().toString()));
			}
		}
		
		DSApi.context().commit();
		DSApi.close();
		
		if (kosztyWyjazduField != null) kosztyWyjazduField.setDictionaryData(kosztyWyjazduValues);
	}
	
	/**Sumuje pola kosztow delegacji PLN z formatki
	 * @param values
	 * @throws NumberFormatException
	 * @throws EdmException
	 */
	private void setAllDelegationCostsInPln(Map<String, FieldData> values) throws NumberFormatException, EdmException {
		Map<Long,BigDecimal> sumaKosztowDelegacjiPln = new HashMap<Long, BigDecimal>();
		fillCostMap(values, sumaKosztowDelegacjiPln, KOSZTY_NOCLEGU_PRE, CURRENCY, MONEY_VALUE_PLN, CURRENCY_TABLE_NAME);
		fillCostMap(values, sumaKosztowDelegacjiPln, DIETA_ZAGRANICZNA_PRE, CURRENCY, MONEY_VALUE_PLN, CURRENCY_TABLE_NAME);
		
		BigDecimal allCost = new BigDecimal(0).setScale(MONEY_SCALE);
		
		if (!DwrUtils.isNotNull(values, "DWR_KOSZTY_WYJAZDU_PLN")) {
			values.put("DWR_KOSZTY_WYJAZDU_PLN", new FieldData(Field.Type.MONEY,allCost));
		}
		
		for (BigDecimal singleCost  : sumaKosztowDelegacjiPln.values()) {
			allCost = allCost.add(singleCost);
		}
		
		allCost = allCost.add(DwrUtils.sumDictionaryMoneyFields(values, PRZEJAZD, WARTOSC));
		allCost = allCost.add(DwrUtils.sumDictionaryMoneyFields(values, INNE_KOSZTY, WARTOSC));
		
		values.get("DWR_KOSZTY_WYJAZDU_PLN").setMoneyData(allCost);
	}
	
	/**Uzupenia slownik kosztow delegacji w zaleznosci waluta - koszt
	 * @param values
	 * @throws EdmException
	 */
	private void fillDelegationCostDictionary(Map<String, FieldData> values) throws EdmException {
		Map<Long,BigDecimal> sumaKosztowDelegacjiAbroad = new HashMap<Long, BigDecimal>();
		Map<Long,BigDecimal> sumaKosztowDelegacjiPln = new HashMap<Long, BigDecimal>();
		
		fillCostMap(values, sumaKosztowDelegacjiAbroad, KOSZTY_NOCLEGU_PRE, CURRENCY, MONEY_VALUE, CURRENCY_TABLE_NAME);
		fillCostMap(values, sumaKosztowDelegacjiPln, KOSZTY_NOCLEGU_PRE, CURRENCY, MONEY_VALUE_PLN, CURRENCY_TABLE_NAME);
		
		fillCostMap(values, sumaKosztowDelegacjiAbroad, DIETA_ZAGRANICZNA_PRE, CURRENCY, MONEY_VALUE, CURRENCY_TABLE_NAME);
		fillCostMap(values, sumaKosztowDelegacjiPln, DIETA_ZAGRANICZNA_PRE, CURRENCY, MONEY_VALUE_PLN, CURRENCY_TABLE_NAME);
		
		int counter = 1;
		DelegationCost kosztDelegacjiObject;
		FieldData kosztyWyjazduField = values.get("DWR_KOSZTY_WYJAZDU_WALUTA");
		Map<String, FieldData> kosztyWyjazduValues = new HashMap<String, FieldData>();
		Set<Long> dictionaryIds = DwrUtils.getMultipleDictionaryIds(values, "DWR_KOSZTY_WYJAZDU_WALUTA");
		
		DSApi.openAdmin();
		DSApi.context().begin();
		
		if (dictionaryIds != null && dictionaryIds.size() > 0) {
			for (Long id : dictionaryIds) {
				if (id != null) {
					kosztDelegacjiObject = DelegationCost.find(id);
					if (kosztDelegacjiObject.getWaluta() != null && sumaKosztowDelegacjiAbroad.get(kosztDelegacjiObject.getWaluta()) != null) {
						kosztDelegacjiObject.setKwota(sumaKosztowDelegacjiAbroad.get(kosztDelegacjiObject.getWaluta()));
						sumaKosztowDelegacjiAbroad.remove(kosztDelegacjiObject.getWaluta());
					}
					if (kosztDelegacjiObject.getWaluta() != null && sumaKosztowDelegacjiPln.get(kosztDelegacjiObject.getWaluta()) != null) {
						kosztDelegacjiObject.setKwotaPln(sumaKosztowDelegacjiPln.get(kosztDelegacjiObject.getWaluta()));
						sumaKosztowDelegacjiPln.remove(kosztDelegacjiObject.getWaluta());
					}
					
					kosztDelegacjiObject.update();
					
					kosztyWyjazduValues.put("DWR_KOSZTY_WYJAZDU_WALUTA_ID_" + counter , new FieldData(Field.Type.STRING, kosztDelegacjiObject.getId().toString()));
					kosztyWyjazduValues.put("DWR_KOSZTY_WYJAZDU_CURRENCY_" + counter , new FieldData(Field.Type.STRING, kosztDelegacjiObject.getWaluta().toString()));
					kosztyWyjazduValues.put("DWR_KOSZTY_WYJAZDU_KWOTA_" + counter , new FieldData(Field.Type.MONEY, kosztDelegacjiObject.getKwota().toString()));
					if(kosztDelegacjiObject.getKwotaPln() != null) kosztyWyjazduValues.put("DWR_KOSZTY_WYJAZDU_WARTOSC_PLN_" + counter++ , new FieldData(Field.Type.MONEY, kosztDelegacjiObject.getKwotaPln().toString()));
				}
			}
		}
		
		if (sumaKosztowDelegacjiAbroad.size() > 0) {
			for (Map.Entry<Long,BigDecimal> entry : sumaKosztowDelegacjiAbroad.entrySet()) {
				kosztDelegacjiObject = new DelegationCost();
				kosztDelegacjiObject.setWaluta(entry.getKey());
				kosztDelegacjiObject.setKwota(entry.getValue());
				kosztDelegacjiObject.setKwotaPln(sumaKosztowDelegacjiPln.get(entry.getKey()));
					
				kosztDelegacjiObject.create();
				
				kosztyWyjazduValues.put("DWR_KOSZTY_WYJAZDU_WALUTA_ID_" + counter , new FieldData(Field.Type.STRING, kosztDelegacjiObject.getId().toString()));
				kosztyWyjazduValues.put("DWR_KOSZTY_WYJAZDU_CURRENCY_" + counter , new FieldData(Field.Type.STRING, kosztDelegacjiObject.getWaluta().toString()));
				kosztyWyjazduValues.put("DWR_KOSZTY_WYJAZDU_KWOTA_" + counter , new FieldData(Field.Type.MONEY, kosztDelegacjiObject.getKwota().toString()));
				if(kosztDelegacjiObject.getKwotaPln() != null) kosztyWyjazduValues.put("DWR_KOSZTY_WYJAZDU_WARTOSC_PLN_" + counter++ , new FieldData(Field.Type.MONEY, kosztDelegacjiObject.getKwotaPln().toString()));
			}
		}
		
		DSApi.context().commit();
		DSApi.close();
		
		if (kosztyWyjazduField != null) kosztyWyjazduField.setDictionaryData(kosztyWyjazduValues);
	}
	
	/** Uzupelnia mape kosztow key - id waluty // value - kwota w walucie 
	 * @param values
	 * @param sumaKosztowDelegacji
	 * @param dictName
	 * @param enumValuesFieldName
	 * @param moneyValueFieldName
	 * @param dataBaseEnumFieldTableName
	 * @throws NumberFormatException
	 * @throws EdmException
	 */
	private void fillCostMap(Map<String, FieldData> values, Map<Long,BigDecimal> sumaKosztowDelegacji, String dictName, String enumValuesFieldName, String moneyValueFieldName, String dataBaseEnumFieldTableName) throws NumberFormatException, EdmException {
		if(DwrUtils.isNotNull(values, DWR_PREFIX + dictName)) {
			Map<Long,BigDecimal> costToSum = getCostMapFromMultipleDictionary(values, dictName , enumValuesFieldName, moneyValueFieldName, dataBaseEnumFieldTableName);
			for (Map.Entry<Long,BigDecimal> entry : costToSum.entrySet()) {
				if (sumaKosztowDelegacji.containsKey(entry.getKey())) {
					sumaKosztowDelegacji.put(entry.getKey(), sumaKosztowDelegacji.get(entry.getKey()).add(entry.getValue()));
				} else {
					sumaKosztowDelegacji.put(entry.getKey(), entry.getValue());
				}
			}
		}
	}
	
	/**Wylicza dni z zakresu dat wyjazdu
	 * @param values
	 * @param msgBuilder
	 */
	private void calculateDates(Map<String, FieldData> values, StringBuilder msgBuilder) {
		if(DwrUtils.isNotNull(values, DWR_PREFIX+DATA_OD, DWR_PREFIX+DATA_DO) && DwrUtils.isSenderField(values, DWR_PREFIX+DATA_OD, DWR_PREFIX+DATA_DO)) {
			DateTime dataOd = new DateTime(values.get(DWR_PREFIX+DATA_OD).getDateData());
			DateTime dataDo = new DateTime(values.get(DWR_PREFIX+DATA_DO).getDateData());
			if(!dataOd.isAfter(dataDo)) {
				int daysBetween = Days.daysBetween(dataOd, dataDo).getDays();
				values.put(DWR_PREFIX+LICZBA_DNI, new FieldData(Field.Type.INTEGER,Integer.valueOf(++daysBetween)));
			} else {
				msgBuilder.append("Podano niepoprawny zakres dat delegacji zagranicznej. ");
			}
			
		}
	}
	
	/**Obsluga dla zrodel finansowania
	 * @param values
	 * @param fm
	 * @throws EdmException
	 */
	private void financeDictionaryHandling(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
		//Popup Zrodla Finansowania
        if(DwrUtils.isSenderField(values,DWR_PREFIX+ADD_BUTTON,DWR_PREFIX+CANCEL_BUTTON)){
            InvoicePopupManagerInwest.clearPopup(values, fm);
        }else if (DwrUtils.isSenderField(values,DWR_PREFIX+SAVE_BUTTON)) {
            InvoicePopupManagerInwest.saveEntry(values, fm);
            InvoicePopupManagerInwest.clearPopup(values, fm);
        }else if(DwrUtils.isSenderField(values,DWR_PREFIX+  InvoicePopupManagerInwest.ENTRY_DICTIONARY)) {
            String entryButtonName = DwrUtils.senderButtonFromDictionary(values,  InvoicePopupManagerInwest.ENTRY_DICTIONARY, EDIT_BUTTON);
            if (entryButtonName == null) {
                entryButtonName = DwrUtils.senderButtonFromDictionary(values, InvoicePopupManagerInwest.ENTRY_DICTIONARY, VIEW_BUTTON);
            }
            //klikni�to edit lub view button
            if (entryButtonName != null) {
                Integer rowNo = Integer.parseInt(entryButtonName.substring(entryButtonName.lastIndexOf("_") + 1));
                InvoicePopupManagerInwest.updatePopup(values, fm, rowNo);

            }
            //klikni�to delete button
            else{
                String delButtonName = DwrUtils.senderButtonFromDictionary(values, InvoicePopupManagerInwest.ENTRY_DICTIONARY, DEL_BUTTON);
                if(delButtonName != null){
                    Integer rowNo = Integer.parseInt(delButtonName.substring(delButtonName.lastIndexOf("_") + 1));
                    InvoicePopupManagerInwest.deleteEntry(values, fm, rowNo);
                }
            }
        }
	}
	
	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
		StringBuilder msgBuilder = new StringBuilder();
		
		setAllDelegationCostsInPln(values);
		
		fillDelegationCostDictionary(values);
		
		if(DwrUtils.isSenderField(values, "DWR_GENERATE_ZALICZKI")) {
			fillAdvanceCostDictionary(values);
		}
		
		if (DwrUtils.isSenderField(values, "DWR_UZASADNIENIE_PRZEJAZDU")) {
			Map<String,FieldData> dictionary = (Map<String,FieldData>)values.get("DWR_UZASADNIENIE_PRZEJAZDU").getData();
			if (dictionary.get("UZASADNIENIE_PRZEJAZDU_SRODEK_LOKOMOCJI_1") != null) {
				EnumValues enumValues = dictionary.get("UZASADNIENIE_PRZEJAZDU_SRODEK_LOKOMOCJI_1").getEnumValuesData();
				if (enumValues != null && enumValues.getSelectedId().equals("10")) {
					if (values.get("DWR_USASADNIENIE_SHOW_OR_HIDE") == null) {
						values.put("USASADNIENIE_SHOW_OR_HIDE", new FieldData(Field.Type.BOOLEAN, true));
					} else {
						values.get("DWR_USASADNIENIE_SHOW_OR_HIDE").setBooleanData(true);
					}
				} else {
					if (values.get("DWR_USASADNIENIE_SHOW_OR_HIDE") == null) {
						values.put("USASADNIENIE_SHOW_OR_HIDE", new FieldData(Field.Type.BOOLEAN, false));;
					} else {
						values.get("DWR_USASADNIENIE_SHOW_OR_HIDE").setBooleanData(false);
					}
					
				}
			}
		}
		
		calculateDates(values, msgBuilder);
		
		financeDictionaryHandling(values, fm);
        
        if (msgBuilder.length() > 0) {
            values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
            return new Field("messageField", msgBuilder.toString(), Field.Type.BOOLEAN);
        }
		
		return null;
	}
	
	private void setRefDivisionField(Map<String, Object> results, Map<String, FieldData> values) {
		if(DwrUtils.isNotNull(values, DANE_WNIOSKODAWCY_DELEGOWANY)) {
			String userId = values.get(DANE_WNIOSKODAWCY_DELEGOWANY).getEnumValuesData().getSelectedId();
			Long divId = null;	
			Map<String,String> options = new HashMap<String, String>();
	        List<String> selected = new ArrayList<String>();
	        DSDivision[] divisions = null;
			try {
				if (StringUtils.isNotBlank(userId)) {
					DSUser user = DSUser.findById(Long.valueOf(userId));
					DSDivision division = user.getDivisionsWithoutGroupPosition()[0];
					
					if(DwrUtils.isNotNull(values, DANE_WNIOSKODAWCY_JEDNOSTKA_ORGANIZACYJNA)){
			            divId = Long.parseLong(values.get(DANE_WNIOSKODAWCY_JEDNOSTKA_ORGANIZACYJNA).getEnumValuesData().getSelectedId());
			        }
		        
	                divisions = user.getOriginalDivisionsWithoutGroup();
	                if(divisions == null || divisions.length == 0){
	                    throw new EdmException("Wybrany u�ytkownik nie znajduje si� w �adnym dziale");
	                }

	                selected.add(divId == null ? "" : divId.toString());
	            }
	            //Je�eli nie wybrano usera trzeba przywr�ci� wszystkie divisiony
	            else{
	                divisions = DSDivision.getOnlyDivisions(false);
	                selected.add("");
	            }

	            for(DSDivision div : divisions){
	                options.put(div.getId().toString(),div.getName());
	            }

	            EnumValues divisionValue = new EnumValues(options,selected);
	            new FieldData(Field.Type.ENUM,divisionValue);
				
	            results.put(DANE_WNIOSKODAWCY_JEDNOSTKA_ORGANIZACYJNA, divisionValue);
			} catch (UserNotFoundException e) {
				log.error(e.getMessage(), e);
			} catch (NumberFormatException e) {
				log.error(e.getMessage(), e);
			} catch (EdmException e) {
				log.error(e.getMessage(), e);
			}
		}
	}
	
	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
		Map<String, Object> fieldValues = new HashMap<String, Object>();
		fieldValues.put(NUMER_WNIOSKU, getApplicationNumberInYear());
		document.getDocumentKind().setOnly(document.getId(), fieldValues);
		
		super.onStartProcess(document, event);
	}
	
	private String getApplicationNumberInYear() {
		String tableName = "dsg_uek_delegacja_zagraniczna";
		try {
			DateTime date = new DateTime();
			int year = date.getYear();
			java.sql.Date  startDate = new java.sql.Date(year, 1, 1);
			java.sql.Date endDate = new java.sql.Date(year, 12, 31);
			StringBuilder sb = new StringBuilder("select COUNT(1) from ").append(tableName).append(" del join ds_document doc on del.document_id = doc.id ")
					.append("where doc.ctime between ? and ?");
			PreparedStatement prepStmt = DSApi.context().prepareStatement(sb.toString());
			prepStmt.setDate(1, startDate);
			prepStmt.setDate(2, endDate);
			
			ResultSet rs = prepStmt.executeQuery();
			
			Integer i = 0;
			if(rs.next()){
				i =rs.getInt(1);
			} 
			
			sb = new StringBuilder().append(++i).append("/").append(year);
			
			return sb.toString();
			
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}
		
		
		return "";
	}
}
