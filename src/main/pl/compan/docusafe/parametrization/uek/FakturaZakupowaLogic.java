package pl.compan.docusafe.parametrization.uek;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.primitives.Ints;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ExecutionQuery;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.exports.*;
import pl.compan.docusafe.core.imports.simple.repository.CacheHandler;
import pl.compan.docusafe.core.imports.simple.repository.SimpleRepositoryAttribute;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.ProfileAssignmentHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.dictionary.erp.simple.Dictionary;
import pl.compan.docusafe.dictionary.erp.simple.FinancialTask;
import pl.compan.docusafe.dictionary.erp.simple.*;
import pl.compan.docusafe.parametrization.uek.hbm.InvoiceEntry;
import pl.compan.docusafe.parametrization.uek.hbm.RepositoryDictEntry;
import pl.compan.docusafe.parametrization.uek.hbm.StawkaVatDict;
import pl.compan.docusafe.parametrization.uek.hbm.ZrodloFinansowaniaDict;
import pl.compan.docusafe.parametrization.uek.utils.InvoicePopupManager;
import pl.compan.docusafe.parametrization.uek.utils.InvoiceToReservationBinder;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Damian on 11.03.14.
 */
public class FakturaZakupowaLogic extends AbstractDocumentLogic {
    private final static Logger log = LoggerFactory.getLogger(FakturaZakupowaLogic.class);

    public final static String DOC_KIND_CN = "faktura_zakupowa";
    public final static Long REPOSITORY_POSITION_CONTEXT_ID = 6l;
    public final static String ENTRY_DICTIONARY = InvoicePopupManager.ENTRY_DICTIONARY;
    private final static String DWR_PREFIX = DwrUtils.DWR_PREFIX;


    private final static String DATA_WPLYWU = "DATA_WPLYWU";

    private final static String KWOTA_NETTO = "KWOTA_NETTO";
    private final static String KWOTA_BRUTTO = "KWOTA_BRUTTO";
    private final static String KWOTA_VAT = "KWOTA_VAT";

    private final static String OPIS_FAKTURY = "OPIS_FAKTURY";


    private final static String EDIT_BUTTON = "EDIT";
    private final static String VIEW_BUTTON = "VIEW";
    private static final String DEL_BUTTON = "DEL";
    private final static String ADD_BUTTON = "ADD";
    private final static String SAVE_BUTTON = "SAVE";
    private static final String COPY_BUTTON = "COPY";
    private final static String CANCEL_BUTTON = "CANCEL";

    private static final String OPIS_MERYTORYCZNY = "OPIS_MERYTORYCZNY";
    private static final String WALUTA = "WALUTA";
    private static final String DATA_WYSTAWIENIA = "DATA_WYSTAWIENIA";
    private static final String KURS = "KURS";
    private static final String STATUS = "STATUS";
    private static final String KWOTA_WYPLATY = "KWOTA_WYPLATY";
    private static final String WNIOSKUJACY = "WNIOSKUJACY";
    private static final String TYP_FAKTURY_FIELD = "TYP_FAKTURY";
    private static final String ZRODLO_FINANSOWANIA_CN = "ZRODLO_FINANSOWANIA";
    private static final String ERP_DOCUMENT_IDM = "ERP_DOCUMENT_IDM";
    private static final String WNIOSEK_O_REZERWACJE = "WNIOSEK_O_REZERWACJE";


    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {

    }

    @Override
    public void documentPermissions(Document document) throws EdmException {

    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();

        toReload.put(DATA_WPLYWU, DateUtils.formatCommonDate(new java.util.Date()));

        fm.reloadValues(toReload);
    }

    @Override
    public void setAdditionalValues(FieldsManager fm, Integer valueOf, String activity) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();
        if("kontrola_formalno_rachunkowa".equals(fm.getEnumItemCn(STATUS))){
            toReload.put(KWOTA_WYPLATY, fm.getValue(KWOTA_BRUTTO));
        }

        fm.reloadValues(toReload);
    }



    @Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        StringBuilder msgBuilder = new StringBuilder();
        try {
            //Popup Zrodla Finansowania
            if(DwrUtils.isSenderField(values,DWR_PREFIX+ADD_BUTTON,DWR_PREFIX+CANCEL_BUTTON)){
                InvoicePopupManager.clearPopup(values,fm);
            }else if (DwrUtils.isSenderField(values,DWR_PREFIX+SAVE_BUTTON)) {
                InvoicePopupManager.saveEntry(values,fm);
                InvoicePopupManager.clearPopup(values,fm);
            }else if(DwrUtils.isSenderField(values,DWR_PREFIX+ ENTRY_DICTIONARY)){
                String entryButtonName = DwrUtils.senderButtonFromDictionary(values, ENTRY_DICTIONARY, EDIT_BUTTON,VIEW_BUTTON,COPY_BUTTON);
                if(entryButtonName != null){
                    Integer rowNo = Integer.parseInt(entryButtonName.substring(entryButtonName.lastIndexOf("_") + 1));
                    InvoicePopupManager.updatePopup(values,fm,rowNo);
                    if(entryButtonName.contains(COPY_BUTTON)){
                        DwrUtils.resetField(values, InvoicePopupManager.ENTRY_ID_FIELD, true);
                    }
                }else{
                    String delButtonName = DwrUtils.senderButtonFromDictionary(values, ENTRY_DICTIONARY, DEL_BUTTON);
                    if(delButtonName != null){
                        Integer rowNo = Integer.parseInt(delButtonName.substring(delButtonName.lastIndexOf("_") + 1));
                        InvoicePopupManager.deleteEntry(values, fm, rowNo);
                    }
                }
                
            }



            //Auto uzupe�nianie kwot Brutto, netto i vat (warto?ci ca�ej faktury)
            else if(DwrUtils.isSenderField(values,DWR_PREFIX+KWOTA_NETTO,DWR_PREFIX+KWOTA_BRUTTO,DWR_PREFIX+KWOTA_VAT)){
                calculateKWOTA(values);
            }
            //Uzupe�nianie opisu merytorycznego na podstawie wybranego template z bazy danych
            else if(DwrUtils.isSenderField(values,DWR_PREFIX+OPIS_FAKTURY)){
                if (DwrUtils.isNotNull(values, DWR_PREFIX+OPIS_FAKTURY)) {
                    Integer opisFM = (Integer) fm.getKey(OPIS_FAKTURY);
                    Integer opisId = Integer.valueOf(values.get(DWR_PREFIX+OPIS_FAKTURY).getEnumValuesData().getSelectedId());
                    if (opisFM == null || !opisFM.equals(opisId)) {
                        String opis = fm.getField(OPIS_FAKTURY).getEnumItem(opisId).getTitle();
                        values.get(DWR_PREFIX + OPIS_MERYTORYCZNY).setStringData(opis);
                    }
                }
            }
            //Uzupe�nienie kursu waluty
            else if(DwrUtils.isSenderField(values,DWR_PREFIX+WALUTA,DWR_PREFIX+DATA_WYSTAWIENIA)){
                setCurrencyRate(values,fm);
            }


        } catch (Exception e) {
            log.error(e.getMessage(), e);
            msgBuilder.append(e.getMessage());
        } finally {
            DSApi.close();
        }
        if (msgBuilder.length() > 0) {
            values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
            return new Field("messageField", msgBuilder.toString(), Field.Type.BOOLEAN);
        }
        return null;
    }

    private void calculateKWOTA(Map<String, FieldData> values) {

        BigDecimal net = values.get(DWR_PREFIX+KWOTA_NETTO).getMoneyData();
        BigDecimal gross = values.get(DWR_PREFIX+KWOTA_BRUTTO).getMoneyData();
        BigDecimal vat = values.get(DWR_PREFIX + KWOTA_VAT).getMoneyData();

        if((DWR_PREFIX+KWOTA_NETTO).equals(DwrUtils.getSenderField(values).getKey()) && net != null){
            if(vat != null){
                gross = net.add(vat);
            }else if(gross != null){
                vat = gross.subtract(net);
            }
        } else if((DWR_PREFIX+KWOTA_BRUTTO).equals(DwrUtils.getSenderField(values).getKey()) && gross != null){
            if(net != null){
                vat = gross.subtract(net);
            }else if(vat != null){
                net = gross.subtract(vat);
            }
        } else if((DWR_PREFIX+KWOTA_VAT).equals(DwrUtils.getSenderField(values).getKey()) && vat != null){
            if(net != null){
                gross = net.add(vat);
            }else if(gross != null){
                net = gross.subtract(vat);
            }
        }

        values.get(DWR_PREFIX+KWOTA_NETTO).setMoneyData(net);
        values.get(DWR_PREFIX+KWOTA_BRUTTO).setMoneyData(gross);
        values.get(DWR_PREFIX+KWOTA_VAT).setMoneyData(vat);

    }


    private void setCurrencyRate(Map<String, FieldData> values, FieldsManager fm) throws EdmException {

        if(DwrUtils.isNotNull(values,DWR_PREFIX+WALUTA)){
            Integer currencyId = Integer.parseInt(values.get(DWR_PREFIX + WALUTA).getEnumValuesData().getSelectedId());
            String swift = fm.getField(WALUTA).getEnumItem(currencyId).getCn();
            if(!DwrUtils.isNotNull(values,DWR_PREFIX + DATA_WYSTAWIENIA)){
                throw new EdmException("Data Wystawienia jest wymagana aby ustali� w�a�ciwy kurs waluty");
            }
            Date date = values.get(DWR_PREFIX + DATA_WYSTAWIENIA).getDateData();
            BigDecimal rate = getRate(swift, date);
            if(rate != null){
                values.get(DWR_PREFIX + KURS).setMoneyData(rate);
            }else{
                throw new EdmException("Nie uda�o si� ustali� w�a�ciwego kursu waluty");
            }
        }
    }

    /**
     * TODO
     * @param swift
     * @param date
     * @return
     */
    private BigDecimal getRate(String swift, Date date) {
        return new BigDecimal(1.2345);
    }




    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) {
        Map<String, Object> results = super.setMultipledictionaryValue(dictionaryName, values);

        if(results == null){
            results = new HashMap<String, Object>();
        }

        if(dictionaryName.equals(InvoicePopupManager.VAT_RATE_DIC)){
            try {
                results.putAll(calculateVat(values));
            } catch (EdmException e) {
                log.error(e.getMessage(),e);
            }
        }else if(dictionaryName.equals(WNIOSKUJACY)){
            try {
                results.putAll(fillWnioskujacyFields(values));
            } catch (EdmException e) {
                log.error(e.getMessage(),e);
            }
        }

        return results;

    }

    private Map<String, Object> fillWnioskujacyFields(Map<String, FieldData> values) throws EdmException {
        final String USER_FIELD = WNIOSKUJACY+"_WN_USER";
        final String DIVISION_FIELD = WNIOSKUJACY+"_WN_DIVISION";


        HashMap<String, Object> results = new HashMap<String, Object>();
        Map<String,String> options = new HashMap<String, String>();
        List<String> selected = new ArrayList<String>();
        Long userId = null,divId = null;
        if(DwrUtils.isNotNull(values, USER_FIELD)){
            userId = Long.parseLong(values.get(USER_FIELD).getEnumValuesData().getSelectedId());
        }
        if(DwrUtils.isNotNull(values, DIVISION_FIELD)){
            divId = Long.parseLong(values.get(DIVISION_FIELD).getEnumValuesData().getSelectedId());
        }

        if(DwrUtils.isSenderField(values,USER_FIELD)){
            DSDivision[] divisions = null;
            if(userId != null){
                DSUser user = DSUser.findById(userId);
                divisions = user.getOriginalDivisionsWithoutGroup();
                if(divisions == null || divisions.length == 0){
                    throw new EdmException("Wybrany u�ytkownik nie znajduje si� w �adnym dziale");
                }

                selected.add(divId == null ? "" : divId.toString());
            }
            //Je�eli nie wybrano usera trzeba przywr�ci� wszystkie divisiony
            else{
                divisions = DSDivision.getOnlyDivisions(false);
                selected.add("");
            }

            for(DSDivision div : divisions){
                options.put(div.getId().toString(),div.getName());
            }

            EnumValues divisionValue = new EnumValues(options,selected);
            new FieldData(Field.Type.ENUM,divisionValue);

            results.put(DIVISION_FIELD, divisionValue);
        }else if(DwrUtils.isSenderField(values,DIVISION_FIELD)){
            //TODO Przygotowa� ewentualne automatyczne wybieranie usera z division
/*            DSUser[] users;
            if(divId != null){
                DSDivision division = DSDivision.findById(divId);
                users = division.getOriginalUsers();
                if(users == null || users.length == 0){
                    throw new EdmException("W dziale nie znajduje si� �aden u�ytkownik");
                }
                selected.add(userId == null ? "" : userId.toString());
            }
            //Je�eli nie wybrano usera trzeba przywr�ci� wszystkie divisiony
            else{
                users = (DSUser[]) DSUser.list(DSUser.SORT_FIRSTNAME_LASTNAME).toArray();
                selected.add("");
            }

            for(DSUser user : users){
                options.put(user.getId().toString(),user.getFirstnameLastnameName());
            }

            EnumValues userValue = new EnumValues(options,selected);
            new FieldData(Field.Type.ENUM,userValue);

            results.put(USER_FIELD, userValue);*/
        }

        return results;
    }


    private Map<String, Object> calculateVat(Map<String, FieldData> values) throws EdmException {
        final String NET_AMOUNT_FIELD = InvoicePopupManager.VAT_RATE_DIC+"_NET_AMOUNT";
        final String VAT_RATE_FIELD = InvoicePopupManager.VAT_RATE_DIC+"_VAT_RATE";
        final String VAT_AMOUNT_FIELD = InvoicePopupManager.VAT_RATE_DIC+"_VAT_AMOUNT";
        final String GROSS_AMOUNT_FIELD = InvoicePopupManager.VAT_RATE_DIC+"_GROSS_AMOUNT";

        boolean wasOpened = false;
        HashMap<String, Object> results = new HashMap<String, Object>();

        try{
            wasOpened = DSApi.openContextIfNeeded();
            BigDecimal stawka,netAmount,vatAmount,grossAmount;

            if (DwrUtils.isNotNull(values, NET_AMOUNT_FIELD, VAT_RATE_FIELD)
                    && (values.get(NET_AMOUNT_FIELD).isSender() || values.get(VAT_RATE_FIELD).isSender()) || values.get(VAT_AMOUNT_FIELD).isSender()) {


                int rateId = Integer.parseInt(values.get(VAT_RATE_FIELD).getEnumValuesData().getSelectedId());

                stawka = BigDecimal.valueOf(VatRate.find(rateId).getProcent()).movePointLeft(2).setScale(2);

                netAmount = values.get(NET_AMOUNT_FIELD).getMoneyData();
                if(values.get(VAT_AMOUNT_FIELD).isSender()){
                    vatAmount = values.get(VAT_AMOUNT_FIELD).getMoneyData();
                }else{
                    vatAmount = netAmount.multiply(stawka).setScale(2,BigDecimal.ROUND_HALF_DOWN);
                }


                if (vatAmount.equals(BigDecimal.ZERO.setScale(2))) {
                    grossAmount = netAmount;
                } else {
                    grossAmount = netAmount.add(vatAmount).setScale(2, BigDecimal.ROUND_HALF_DOWN);

                }
                results.put(VAT_AMOUNT_FIELD,vatAmount);
                results.put(GROSS_AMOUNT_FIELD, grossAmount);
            }

        }finally {
            DSApi.closeContextIfNeeded(wasOpened);
        }

        return results;
    }


    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
        final String KONTROLA_RACHUNKOWA = "kontrola_formalno_rachunkowa";

        AssignmentHandler handler = null;
        if(KONTROLA_RACHUNKOWA.equals(acceptationCn)){

            //todo przygotowa� wyb�r innego handlera
            handler = new ProfileAssignmentHandler();
            ((ProfileAssignmentHandler) handler).setProfileIdAdds("profile.id."+KONTROLA_RACHUNKOWA);
        }

        if(handler == null){
          handler = new AssigneeHandler();
        }

        try {
            handler.assign(assignable, openExecution);
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            return false;
        }

        return true;
    }

    private void validateReceiveDate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException {
        String userName = DSApi.context().getPrincipalName();

        Set<Object> wnioskujacyIds = ((Map<String,Set<Object>>) findProcessVariable(documentId, "applicants")).get(userName);

        HashSet<String> stringIds = new HashSet<String>();
        for(Object id : wnioskujacyIds){
            stringIds.add(id.toString());
        }

        if(stringIds.isEmpty()){
            return;
        }

        Map<String,Map> mDicValues = (Map<String, Map>) values.get("M_DICT_VALUES");
        if (mDicValues != null) {
            Pattern pat = Pattern.compile("WNIOSKUJACY_(\\d+)");

            for(Map.Entry<String,Map> dicEntry : mDicValues.entrySet()){
                Matcher matcher = pat.matcher(dicEntry.getKey());
                if(matcher.find()){
                    if(stringIds.contains(dicEntry.getValue().get("ID").toString())){
                        if(((FieldData) dicEntry.getValue().get("DATA_ODBIORU")).getDateData() == null){
                            throw new EdmException("Nie wszystkie wymagane pola daty odbioru zosta�y wype�nione");
                        }
                    }
                }
            }
        }
    }


    /**
     * Znajduje zmienn� procesow� dla danego dokumentu
     * @param documentId
     * @param variableName
     * @return
     */
   private Object findProcessVariable(Long documentId, String variableName){
       RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
       if(runtimeService != null){
           ExecutionQuery query = runtimeService.createExecutionQuery().processVariableValueEquals("docId",documentId);
           Execution execution = query.list().size() >0 ? query.list().get(0) : null;
           if (execution != null) {
               return runtimeService.getVariable(execution.getId(), variableName);
           }
       }
       return null;
   }

    /**
     * Returns name of current process activity
     * @param documentId
     * @return
     */
    private String currentProcessActivity(Long documentId){
        RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
        if(runtimeService != null){
            ExecutionQuery query = runtimeService.createExecutionQuery().processVariableValueEquals("docId",documentId);
            List<Execution> execList =  query.list();
            if(execList != null){
                for(int i = 0 ; i < execList.size(); i++){
                    Execution execution = query.list().get(i);
                    if(execution.getActivityId() != null ){
                        return execution.getActivityId();
                    }
                }
            }
        }
        return null;
    }


    @Override
    public String onAlternativeUpdate(Document document, Map<String, Object> values, String activity) throws EdmException {
        InvoiceToReservationBinder invoiceBinder = new InvoiceToReservationBinder(document);

        if(invoiceBinder.setup()){
            invoiceBinder.createEntriesFromReservation();
            if(!invoiceBinder.wasError()){
                invoiceBinder.updateDictionary();
            }
        }

        return !invoiceBinder.wasError() ? "Pod��czono pozycje wniosku" : invoiceBinder.getErrorMessage();
    }

    @Override
    public ExportedDocument buildExportDocument(OfficeDocument doc) throws EdmException {
        boolean wasOppened = false;
        try{
            wasOppened = DSApi.openContextIfNeeded();


            PurchasingDocument exported = new PurchasingDocument();
            FieldsManager fm = doc.getFieldsManager();

            exported.setContractor(fm.getLongKey("CONTRACTOR"));

            exported.setCode(fm.getStringKey("NR_FAKTURY"));

            exported.setPurchaseType(fm.getEnumItemCn("TYP_FAKTURY"));

            exported.setIssued((Date) fm.getKey("DATA_WYSTAWIENIA"));

            exported.setPaymentMethodId(fm.getLongKey("SPOSOB_PLATNOSCI").intValue());

            exported.setReturnMethod(fm.getEnumItemCn("SPOSOB_ZWROTU"));

            if(fm.getKey("KWOTA_NETTO") instanceof BigDecimal){
                exported.setNetAmount((BigDecimal)fm.getKey("KWOTA_NETTO"));
            }
            if(fm.getKey("KWOTA_BRUTTO") instanceof BigDecimal){
                exported.setGrossAmount((BigDecimal)fm.getKey("KWOTA_BRUTTO"));
            }
            if(fm.getKey("KWOTA_VAT") instanceof BigDecimal){
                exported.setVatAmount((BigDecimal)fm.getKey("KWOTA_VAT"));
            }

            exported.setCurrencyCode(fm.getEnumItemCn("WALUTA"));


            exported.setDescription(getAdditionFieldsDescription(doc, fm));

            //datprzyj
            exported.setRecieved(doc.getCtime());

            addInvoiceEntries(exported, fm);

            addAttachments(doc, exported);

            return exported;
        }catch (Exception e){
            throw new EdmException(e);
        } finally{
            DSApi.closeContextIfNeeded(wasOppened);
        }

    }

    private void addInvoiceEntries(PurchasingDocument exported, FieldsManager fm) throws EdmException {
        List<Long> entriesIds = (List) fm.getKey(InvoicePopupManager.ENTRY_DICTIONARY);
        EnumItem documentTypeItem = fm.getEnumItem(TYP_FAKTURY_FIELD);
        List<SimpleRepositoryAttribute> repositoryDicts = CacheHandler.getRepositoryDicts(documentTypeItem.getRefValue() != null ? Ints.tryParse(documentTypeItem.getRefValue()) : null, REPOSITORY_POSITION_CONTEXT_ID.intValue());

        if (entriesIds != null) {
            validateEntries(entriesIds);

            int i = 1;
            List<BudgetItem> budgetItems = Lists.newArrayList();

            for (Long entryId : entriesIds) {
                InvoiceEntry entry = InvoiceEntry.findById(entryId);
                ZrodloFinansowaniaDict zrodlo = entry.getZrodloFinansowania();
                Collection<StawkaVatDict> stawkiVat = entry.getStawkaVatDicts();

                if(stawkiVat != null){
                    for(StawkaVatDict stawka : stawkiVat){
                        BudgetItem bItem = new BudgetItem();

                        //nrpoz
                        bItem.setItemNo(i++);
                        stawka.setPositionNumber(i);
                        //Mpk
                        try{
                            DSDivision div = DSDivision.findById(entry.getMpk());
                            bItem.setMpkCode(div != null ? div.getCode() : null);
                        }catch(DivisionNotFoundException dnfe){
                            throw new EdmException("B��d przy eksporcie: B��dny MPK");
                        }

                        //jm
                        bItem.setUnit(entry.getUnitOfMeasure());
                        //ilosc
                        bItem.setQuantity(new BigDecimal(entry.getQuantity()));

                        ProductionOrder order = ProductionOrder.find(entry.getProductionOrder());
                        if(order != null){
                            bItem.setOrderId(order.getErpId() != null ? order.getErpId().longValue() : null);
                        }
                        bItem.setCode(entry.getWsKod());

                        addStawkaVat(bItem, stawka);

                        if(zrodlo != null){
                            addZrodlaFinansowania(bItem, zrodlo);
                        }
                        if(entry.getRepositoryDictEntry() != null){
                            addRepositoryDict(repositoryDicts, bItem,entry.getRepositoryDictEntry());
                        }

                        budgetItems.add(bItem);
                        stawka.save();
                    }
                }

            }
            exported.setBudgetItems(budgetItems);
        }
    }

    private void addRepositoryDict(List<SimpleRepositoryAttribute> repositoryDicts, BudgetItem bItem,RepositoryDictEntry entry) {
        for (SimpleRepositoryAttribute dict : repositoryDicts) {
            Long value = null;
            try {
                value = getRepositoryAttributeValue(entry.getId(), entry.getTableName(), dict, log);
            } catch (RuntimeException e) {
                log.error(e.getMessage(), e);
            }
            if (value != null) {
                bItem.addRepositoryItem(new RepositoryItem(dict.getRepAtrybutId(), value));
            }
        }
    }

    /**
     * TODO
     * @param entriesIds
     */
    private void validateEntries(List<Long> entriesIds) {
        //todo
    }

    private void addStawkaVat(BudgetItem bItem, StawkaVatDict stawka) {
        bItem.setVatAmount(stawka.getVatAmount());
        bItem.setVatRate(stawka.getVatRate().intValue());
        bItem.setNetAmount(stawka.getNetAmount());
        bItem.setGrossAmount(stawka.getGrossAmount());
    }

    private void addZrodlaFinansowania(BudgetItem bItem, ZrodloFinansowaniaDict zrodlo) throws EdmException {

            bItem.setBudgetKindId(zrodlo.getBudgetKind());
            bItem.setBudgetDirectId(zrodlo.getBudget());
            bItem.setBudgetKoId(zrodlo.getBudgetKo());
            bItem.setPositionId(zrodlo.getPosition());
            bItem.setProjectId(zrodlo.getContract());
            bItem.setResourceId(zrodlo.getFund());

            if(zrodlo.getFinancialTask() != null){
                List<FinancialTask> financialTasks = Dictionary.find("erpId",zrodlo.getFinancialTask().doubleValue(),FinancialTask.class);
                if(financialTasks != null && financialTasks.size() == 1){
                    FinancialTask financialTask = financialTasks.get(0);
                    pl.compan.docusafe.core.exports.FinancialTask exportFinancialTask = new pl.compan.docusafe.core.exports.FinancialTask();
                    exportFinancialTask.setTaskCode(financialTask.getIdm());
                    exportFinancialTask.setPercent(new BigDecimal(100));
                    bItem.setFinanancialTasks(Lists.newArrayList(exportFinancialTask));
                }else if(financialTasks != null && financialTasks.size() > 1){
                    throw new EdmException("Znaleziono wi�cej ni� jedno zadanie o erpId = " + zrodlo.getSource());
                }else{
                    throw new EdmException("Nie znaleziono zadania o erpId = " + zrodlo.getSource());
                }
            }

            if(zrodlo.getSource() != null)
            {
                FundSource fItem = new FundSource();
                List<Source> sources = Dictionary.find("erpId",zrodlo.getSource().doubleValue(),Source.class);
                if(sources != null && sources.size() == 1 ){
                    Source source = sources.get(0);
                    fItem.setCode(source.getIdm());
                    fItem.setFundPercent(new BigDecimal(100));
                    bItem.setFundSources(Lists.newArrayList(fItem));
                }else if(sources != null && sources.size() > 1){
                    throw new EdmException("Znaleziono wi�cej ni� jedno �rod�o finansowania o erpId = " + zrodlo.getSource());
                }else{
                    throw new EdmException("Nie znaleziono �rod�a finansowania o erpId = " + zrodlo.getSource());
                }
            }
    }

    /**
     * build description from addition fields
     *
     * @param doc
     * @param fm
     * @return
     * @throws EdmException
     */
    private String getAdditionFieldsDescription(OfficeDocument doc, FieldsManager fm) throws EdmException {
        Map<String, String> additionFields = Maps.newLinkedHashMap();
        additionFields.put("Nr KO", doc.getOfficeNumber() != null ? doc.getOfficeNumber().toString() : "brak");
        additionFields.put("Opis faktury", fm.getStringValue("OPIS_MERYTORYCZNY"));
        additionFields.put("NKUP", fm.getEnumItemCn("NKUP"));
        additionFields.put("Data zam�wienia", fm.getStringValue("DATA_ZAMOWIENIA"));
        additionFields.put("Tryb zam�wienia", fm.getStringValue("TRYB_ZAMOWIENIA"));
        additionFields.put("Szczeg�owy tryb zam�wienia", fm.getStringValue("SZCZEGOLOWY_TRYB"));
        additionFields.put("Numer zam�wienia", fm.getStringValue("NUMER_ZAMOWIENIA"));
        additionFields.put("Numer BZP", fm.getStringValue("NUMER_BZP"));
        additionFields.put("Kod us�ugi mi�dzynarodowej", fm.getStringValue("NUMER_BZP"));

        StringBuilder description = new StringBuilder();
        for (String label : additionFields.keySet()) {
            if (StringUtils.isNotBlank(additionFields.get(label))) {
                description.append(label)
                        .append(": ")
                        .append(additionFields.get(label))
                        .append("; ");
            }
        }
        return description.toString();
    }

    private void addAttachments(OfficeDocument doc, PurchasingDocument exported) throws EdmException {
        String systemPath = null;
        if (Docusafe.getAdditionProperty("erp.faktura.link_do_skanu") != null) {
            systemPath = Docusafe.getAdditionProperty("erp.faktura.link_do_skanu");
        } else {
            systemPath = Docusafe.getBaseUrl();
        }

        for (Attachment zal : doc.getAttachments()) {
            AttachmentInfo info = new AttachmentInfo();
            String path = systemPath + "/repository/view-attachment-revision.do?id=" + zal.getMostRecentRevision().getId();
            info.setName(zal.getTitle());
            info.setUrl(path);
            exported.addAttachementInfo(info);
        }
    }

    public Long getRepositoryAttributeValue(Long postionId, String positionTable, SimpleRepositoryAttribute dict, Logger log) {
        PreparedStatement ps = null;
        Long result = null;
        try {
            ps = DSApi.context().prepareStatement("select centrum from "+ positionTable +" bp join "+dict.getTableName()+" r on bp."+ dict.getTableName() +"=r.id where bp.id = ?");
            ps.setLong(1, postionId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                result = rs.getLong(1) != 0l ? rs.getLong(1) : null;
                if (rs.next()) {
                    log.error("Found more than one repository item to budget position, bp id: "+postionId+", attribute: "+dict.getTableName());
                }
            }
            rs.close();
            ps.close();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        DbUtils.closeQuietly(ps);
        return result;
    }
}
