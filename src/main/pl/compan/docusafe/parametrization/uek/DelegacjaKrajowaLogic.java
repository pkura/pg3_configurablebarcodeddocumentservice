package pl.compan.docusafe.parametrization.uek;

import com.google.common.collect.Maps;
import org.bouncycastle.crypto.DSA;
import org.joda.time.DateTime;
import org.joda.time.Days;
import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.exports.ExportedDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.uek.utils.InvoicePopupManager;
import pl.compan.docusafe.parametrization.uek.utils.InvoicePopupManagerInwest;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;

import org.joda.time.Days;

import java.util.List;
import java.util.Map;

import static pl.compan.docusafe.core.dockinds.dwr.DwrUtils.sumDictionaryMoneyFields;

/**
 * Created by user on 2014-04-30.
 */
public class DelegacjaKrajowaLogic extends AbstractDocumentLogic {
    private final static Logger log = LoggerFactory.getLogger(DelegacjaKrajowaLogic.class);
    public static final String TERMIN_WYJAZDU = "TERMIN_WYJAZDU";
    public static final String TERMIN_POWROTU = "TERMIN_POWROTU";
    public static final String DIETA_KRAJOWA_MONEY = "DIETA_KRAJOWA_MONEY";
    public static final String DATA_PRZELEWU = "DATA_PRZELEWU";
    public static final String DO_WYPLATY_ZWROTU = "DO_WYPLATY_ZWROTU";
    public static final String ZALICZKA_KWOTA = "ZALICZKA_KWOTA";
    public static final String DIETA_KRAJOWA_MONEY_TO_SUM = "DIETA_KRAJOWA_MONEY_TO_SUM";
    public static final String DIETA_KRAJOWA_ILOSC = "DIETA_KRAJOWA_ILOSC";
    public static final String DOJAZD_RYCZALT_ILOSC = "DOJAZD_RYCZALT_ILOSC";
    public static final String DOJAZD_RYCZALT_COST = "DOJAZD_RYCZALT_COST";
    public static final String NOCLEGI_RYCZALT_COST_TO_SUM = "NOCLEGI_RYCZALT_COST_TO_SUM";
    public static final String INNE_WYDATKI_COST_TO_SUM = "INNE_WYDATKI_COST_TO_SUM";
    public static final String NOCLEG_WG_RACHUN_COST = "NOCLEG_WG_RACHUN_COST";
    public static final String SUMA = "SUMA";
    public static final String ZALICZKA = "ZALICZKA";

    private final static String EDIT_BUTTON = "EDIT";
    private final static String VIEW_BUTTON = "VIEW";
    private static final String DEL_BUTTON = "DEL";
    private final static String ADD_BUTTON = "ADD";
    private final static String SAVE_BUTTON = "SAVE";
    private final static String CANCEL_BUTTON = "CANCEL";
    public final static String ENTRY_DICTIONARY = "POZYCJA_FAKTURY";
    private static final String PRZEJAZD = "PRZEJAZD";
    private static final String KWOTA_PRZEJ = "KWOTA_PRZEJ";
    private static final String INNE_KOSZTY = "INNE_KOSZTY";
    private static final String WARTOSC_INNYCH_KOSZTOW = "WARTOSC_INNYCH_KOSZTOW";
    private static final String PONIESIONE_KOSZTY = "PONIESIONE_KOSZTY";
    private static final String WARTOSC = "WARTOSC";
    private static final String KOSZT_PRZEJ_SAM = "KOSZT_PRZEJ_SAM";
    private static final String KOSZT_NOCLEGU = "KOSZT_NOCLEGU";
    private static final String DELEGOWANY = "DELEGOWANY";
    private static DelegacjaKrajowaLogic instance;

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {

    }

    @Override
    public void documentPermissions(Document document) throws EdmException {

    }

    public static synchronized DelegacjaKrajowaLogic getInstance() {
        if (instance == null)
            instance = new DelegacjaKrajowaLogic();
        return instance;
    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();
        long id = DSApi.context().getDSUser().getId();
        toReload.put(DELEGOWANY, id);
       /* if (DSApi.context().getDSUser().getDivisionsWithoutGroup().length>0) {
            String sender = "u:"+DSApi.context().getDSUser().getName();
            sender+=(";d:"+DSApi.context().getDSUser().getDivisionsWithoutGroup()[0].getGuid());
            toReload.put("SENDER_HERE", sender);
        } else {
            String sender = "u:"+DSApi.context().getDSUser().getName();
            toReload.put("SENDER_HERE", sender);
        }

        //toReload.put(DATA_WNIOSKU, new Date());
        //toReload.put(NUMER_WNIOSKU, getApplicationNumberInYear());*/
        fm.reloadValues(toReload);
    }

    @Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        StringBuilder msgBuilder = new StringBuilder();
        msgBuilder.append(handlePopupOperations(values, fm));
        msgBuilder.append(calculateDieta(values));
        msgBuilder.append(validPrzelewData(values));
        msgBuilder.append(validDelegationTime(values));
        msgBuilder.append(calculateRyczalt(values));
        msgBuilder.append(calculateSumOfCosts(values));
        msgBuilder.append(generateZaliczka(values));
        msgBuilder.append(calculateZwrot(values));
        msgBuilder.append(clearZaliczkaWhenHidden(values));
        if (msgBuilder.length() > 0) {
            values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
            return new Field("messageField", msgBuilder.toString(), Field.Type.BOOLEAN);
        }
        return null;
    }

    private String generateZaliczka(Map<String, FieldData> values) {
        StringBuilder msgBuilder = new StringBuilder();
        if (values.get(dwr(ZALICZKA)).getBooleanData().equals(true) && values.get(dwr("ZALICZKA_KWOTA")) != null) {
            BigDecimal sum = new BigDecimal(0.0);
            sum = sum.add(sumDictionaryMoneyFields(values, PRZEJAZD, KWOTA_PRZEJ));
            sum = sum.add(sumDictionaryMoneyFields(values, INNE_KOSZTY, WARTOSC_INNYCH_KOSZTOW));
            sum = sum.add(sumDictionaryMoneyFields(values, PONIESIONE_KOSZTY, WARTOSC));
            sum = sum.add(sumDictionaryMoneyFields(values, KOSZT_PRZEJ_SAM, WARTOSC));
            if (DwrUtils.isNotNull(values, dwr(DIETA_KRAJOWA_MONEY)))
                sum = sum.add(values.get(dwr(DIETA_KRAJOWA_MONEY)).getMoneyData());
            if (DwrUtils.isNotNull(values, dwr(KOSZT_NOCLEGU)))
                sum = sum.add(values.get(dwr(KOSZT_NOCLEGU)).getMoneyData());
            values.get(dwr(ZALICZKA_KWOTA)).setMoneyData(sum);
        }
        return msgBuilder.toString();
    }

    private String handlePopupOperations(Map<String, FieldData> values, FieldsManager fm) {
        StringBuilder msgBuilder = new StringBuilder();
        //Popup Zrodla Finansowania
        try {
            if (DwrUtils.isSenderField(values, dwr(ADD_BUTTON), dwr(CANCEL_BUTTON))) {
                InvoicePopupManagerInwest.clearPopup(values, fm);
            } else if (DwrUtils.isSenderField(values, dwr(SAVE_BUTTON))) {
                InvoicePopupManagerInwest.saveEntry(values, fm);
                InvoicePopupManagerInwest.clearPopup(values, fm);
            } else if (DwrUtils.isSenderField(values, dwr(ENTRY_DICTIONARY))) {
                String entryButtonName = DwrUtils.senderButtonFromDictionary(values, ENTRY_DICTIONARY, EDIT_BUTTON);
                if (entryButtonName == null) {
                    entryButtonName = DwrUtils.senderButtonFromDictionary(values, ENTRY_DICTIONARY, VIEW_BUTTON);
                }
                //klikni�to edit lub view button
                if (entryButtonName != null) {
                    Integer rowNo = Integer.parseInt(entryButtonName.substring(entryButtonName.lastIndexOf("_") + 1));
                    InvoicePopupManagerInwest.updatePopup(values, fm, rowNo);

                }
                //klikni�to delete button
                else {
                    String delButtonName = DwrUtils.senderButtonFromDictionary(values, ENTRY_DICTIONARY, DEL_BUTTON);
                    if (delButtonName != null) {
                        Integer rowNo = Integer.parseInt(delButtonName.substring(delButtonName.lastIndexOf("_") + 1));
                        InvoicePopupManagerInwest.deleteEntry(values, fm, rowNo);
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            msgBuilder.append(e.getMessage());
        }
        return msgBuilder.toString();
    }

    private String clearZaliczkaWhenHidden(Map<String, FieldData> values) {
        StringBuilder msgBuilder = new StringBuilder();
        if (DwrUtils.isNotNull(values, dwr(ZALICZKA_KWOTA)) && values.get(dwr(ZALICZKA)).getBooleanData().equals(false)) {
            values.get(dwr(ZALICZKA_KWOTA)).setMoneyData(new BigDecimal(0.0));
        }
        return msgBuilder.toString();
    }

    private String calculateZwrot(Map<String, FieldData> values) {
        StringBuilder msgBuilder = new StringBuilder();
        BigDecimal sum = new BigDecimal(0.0);
        if (values.get(dwr(ZALICZKA)).getBooleanData().equals(true) && DwrUtils.isNotNull(values, dwr(SUMA)) && DwrUtils.isNotNull(values, dwr(ZALICZKA_KWOTA))) {
            values.get(dwr(DO_WYPLATY_ZWROTU)).setMoneyData(values.get(dwr(SUMA)).getMoneyData().subtract(values.get(dwr(ZALICZKA_KWOTA)).getMoneyData()));
        } else if (values.get(dwr(ZALICZKA)).getBooleanData().equals(false)) {
            values.get(dwr(DO_WYPLATY_ZWROTU)).setMoneyData(values.get(dwr(SUMA)).getMoneyData());
        }
        return msgBuilder.toString();
    }

    private String calculateSumOfCosts(Map<String, FieldData> values) {
        StringBuilder msgBuilder = new StringBuilder();
        BigDecimal sum = new BigDecimal(0.0);
        if (DwrUtils.isNotNull(values, dwr(DIETA_KRAJOWA_MONEY_TO_SUM)))
            sum = sum.add(values.get(dwr(DIETA_KRAJOWA_MONEY_TO_SUM)).getMoneyData());
        if (DwrUtils.isNotNull(values, dwr(DOJAZD_RYCZALT_COST)))
            sum = sum.add(values.get(dwr(DOJAZD_RYCZALT_COST)).getMoneyData());
        if (DwrUtils.isNotNull(values, dwr(NOCLEG_WG_RACHUN_COST)))
            sum = sum.add(values.get(dwr(NOCLEG_WG_RACHUN_COST)).getMoneyData());
        if (DwrUtils.isNotNull(values, dwr(NOCLEGI_RYCZALT_COST_TO_SUM)))
            sum = sum.add(values.get(dwr(NOCLEGI_RYCZALT_COST_TO_SUM)).getMoneyData());
        if (DwrUtils.isNotNull(values, dwr(INNE_WYDATKI_COST_TO_SUM)))
            sum = sum.add(values.get(dwr(INNE_WYDATKI_COST_TO_SUM)).getMoneyData());
        values.get(dwr(SUMA)).setMoneyData(sum);
        return msgBuilder.toString();
    }

    private String calculateRyczalt(Map<String, FieldData> values) {
        StringBuilder msgBuilder = new StringBuilder();
        if (DwrUtils.isNotNull(values, dwr(TERMIN_WYJAZDU)) && DwrUtils.isNotNull(values, dwr(TERMIN_POWROTU))) {
            Date dataPrzyjazdu = values.get(dwr(TERMIN_POWROTU)).getDateData();
            Date dataWyjazdu = values.get(dwr(TERMIN_WYJAZDU)).getDateData();
            int daysBetween = Days.daysBetween(new DateTime(dataWyjazdu), new DateTime(dataPrzyjazdu)).getDays();
            values.get(dwr(DOJAZD_RYCZALT_ILOSC)).setMoneyData(new BigDecimal(daysBetween + 1));
        }
        return msgBuilder.toString();
    }

    /**
     * Data dokonania przelewu, nie wcze�niej ni� 5 dni roboczych przed wyjazdem
     *
     * @param values
     * @return
     */
    private String validPrzelewData(Map<String, FieldData> values) {
        StringBuilder msgBuilder = new StringBuilder();
        if (values.get(dwr(ZALICZKA)).getBooleanData().equals(true) && DwrUtils.isNotNull(values, dwr(TERMIN_WYJAZDU)) && DwrUtils.isNotNull(values, dwr(DATA_PRZELEWU)) && values.get(dwr(DATA_PRZELEWU)).isSender()) {
            Date dataPrzelewu = values.get(dwr(DATA_PRZELEWU)).getDateData();
            Date dataWyjazdu = values.get(dwr(TERMIN_WYJAZDU)).getDateData();
            int daysBetween = Days.daysBetween(new DateTime(dataPrzelewu), new DateTime(dataWyjazdu)).getDays();
            if (daysBetween > 5 || daysBetween < 0) {
                values.get(dwr(DATA_PRZELEWU)).setData(null);
            }
        } else if (DwrUtils.isNotNull(values, dwr(DATA_PRZELEWU)) && values.get(dwr(DATA_PRZELEWU)).isSender()) {
            values.get(dwr(DATA_PRZELEWU)).setData(null);
        }
        return msgBuilder.toString();
    }

    /**
     * Valid DATA_POWROTU before DATA_WJAZDU
     *
     * @param values
     * @return
     */
    private String validDelegationTime(Map<String, FieldData> values) {
        StringBuilder msgBuilder = new StringBuilder();
        if ((values.get(dwr(TERMIN_WYJAZDU)).isSender() || values.get(dwr(TERMIN_POWROTU)).isSender()) &&
                DwrUtils.isNotNull(values, dwr(TERMIN_WYJAZDU)) && DwrUtils.isNotNull(values, dwr(TERMIN_POWROTU))) {
            if (values.get(dwr(TERMIN_POWROTU)).getDateData().before(values.get(dwr(TERMIN_WYJAZDU)).getDateData())) {
                values.get(dwr(TERMIN_POWROTU)).setData(null);
            }
            //values.get(dwr("KOSZTY")).getDictionaryData().put("id", new FieldData(Field.Type.STRING, "-1"));
        }
        return msgBuilder.toString();
    }

    /**
     * Dieta = maksymalna kwota z zakresu dat
     *
     * @param values
     * @return
     */
    private String calculateDieta(Map<String, FieldData> values) {
        StringBuilder msgBuilder = new StringBuilder();
        //Set DIETA
        if ((values.get(dwr(TERMIN_WYJAZDU)).isSender() || values.get(dwr(TERMIN_POWROTU)).isSender()) &&
                DwrUtils.isNotNull(values, dwr(TERMIN_WYJAZDU)) && DwrUtils.isNotNull(values, dwr(TERMIN_POWROTU))) {
            BigDecimal costOfDiets = costOfDiets(values);
            values.get(dwr(DIETA_KRAJOWA_MONEY)).setMoneyData(costOfDiets);
            values.get(dwr(DIETA_KRAJOWA_MONEY_TO_SUM)).setMoneyData(costOfDiets);
            values.get(dwr(DIETA_KRAJOWA_ILOSC)).setMoneyData(new BigDecimal(numOfDiets(values)));
            //FieldData numOfDiets = (FieldData) DwrUtils.getDictionaryValue(values, "KOSZTY", "DIETA_KRAJOWA_ILOSC");
            //numOfDiets.setMoneyData(new BigDecimal(numOfDiets(values)));
            //values.get(dwr("KOSZTY")).getDictionaryData().put("id", new FieldData(Field.Type.STRING, "-1"));

        }
        if (values.get(dwr(DIETA_KRAJOWA_MONEY)).isSender()) {
            if (!DwrUtils.isNotNull(values, dwr(TERMIN_WYJAZDU)) || !DwrUtils.isNotNull(values, dwr(TERMIN_POWROTU))) {
                values.get(dwr(DIETA_KRAJOWA_MONEY)).setMoneyData(BigDecimal.ZERO);
                values.get(dwr(DIETA_KRAJOWA_MONEY_TO_SUM)).setMoneyData(BigDecimal.ZERO);
            } else if (DwrUtils.isNotNull(values, dwr(TERMIN_WYJAZDU)) && DwrUtils.isNotNull(values, dwr(TERMIN_POWROTU))) {
                if (values.get(dwr(DIETA_KRAJOWA_MONEY)).getMoneyData().compareTo(costOfDiets(values)) == 1) {
                    values.get(dwr(DIETA_KRAJOWA_MONEY)).setMoneyData(costOfDiets(values));
                    values.get(dwr(DIETA_KRAJOWA_MONEY_TO_SUM)).setMoneyData(costOfDiets(values));
                } else {
                    values.get(dwr(DIETA_KRAJOWA_MONEY_TO_SUM)).setMoneyData(values.get(dwr(DIETA_KRAJOWA_MONEY)).getMoneyData());
                }
            }
        }
        return msgBuilder.toString();
    }

    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) {
        return super.setMultipledictionaryValue(dictionaryName, values);
    }

    public static double evaluateDietAmount(Date from, Date to) {
        Date diff = new Date(to.getTime() - from.getTime());
        double hours = DateUtils.hours(diff.getTime());

        double diet = (long) (hours / 24);
        double rest = hours % 24;

        if (diet == 0) {
            if (rest < 8) {
            } else if (rest <= 12) {
                diet += 0.5;
            } else {
                diet++;
            }
        } else {
            if (rest > 8) {
                diet++;
            } else if (rest > 0 && rest <= 8) {
                diet += 0.5;
            }
        }
        return diet;
    }

    public static double numOfDiets(Map<String, FieldData> values) {
        Date from = values.get(dwr(TERMIN_WYJAZDU)).getDateData();
        Date to = values.get(dwr(TERMIN_POWROTU)).getDateData();
        return evaluateDietAmount(from, to);
    }

    private BigDecimal costOfDiets(Map<String, FieldData> values) {
        double numOfDiets = numOfDiets(values);
        int dietaKoszt = Integer.parseInt(AdditionManager.getProperty("dieta_koszt"));
        return new BigDecimal(numOfDiets).multiply(new BigDecimal(dietaKoszt)).setScale(2, RoundingMode.HALF_UP);
    }

    private String getApplicationNumberInYear() {
        String tableName = "dsg_uek_delegacja_zagraniczna";
        try {
            DateTime date = new DateTime();
            int year = date.getYear();
            java.sql.Date startDate = new java.sql.Date(year, 1, 1);
            java.sql.Date endDate = new java.sql.Date(year, 12, 31);
            StringBuilder sb = new StringBuilder("select COUNT(1) from ").append(tableName).append(" del join ds_document doc on del.document_id = doc.id ")
                    .append("where doc.ctime between ? and ?");
            PreparedStatement prepStmt = DSApi.context().prepareStatement(sb.toString());
            prepStmt.setDate(1, startDate);
            prepStmt.setDate(2, endDate);

            ResultSet rs = prepStmt.executeQuery();

            Integer i = 0;
            if (rs.next()) {
                i = rs.getInt(1);
            }

            sb = new StringBuilder().append(++i).append("/").append(year);

            return sb.toString();

        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }


        return "";
    }

    @Override
    public ExportedDocument buildExportDocument(OfficeDocument doc) throws EdmException {
        ExportedDocument expoDoc = new ExportedDocument() {
        };
        return null;
    }

    public static String dwr(String cn) {
        return "DWR_" + cn;
    }
}
