package pl.compan.docusafe.parametrization.uek;


import com.beust.jcommander.internal.Maps;
import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ExecutionQuery;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PropertyNotFoundException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.exports.*;
import pl.compan.docusafe.core.exports.FinancialTask;
import pl.compan.docusafe.core.imports.simple.repository.CacheHandler;
import pl.compan.docusafe.core.imports.simple.repository.SimpleRepositoryAttribute;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.dictionary.erp.simple.*;
import pl.compan.docusafe.parametrization.imgwwaw.hbm.Source;
import pl.compan.docusafe.parametrization.uek.hbm.*;
import pl.compan.docusafe.parametrization.uek.utils.InvoicePopupManager;
import pl.compan.docusafe.parametrization.uek.utils.ZapotrzebowaniePopupManager;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.DocumentReplicator;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.util.Dictionary;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * Created by Raf on 2014-04-14.
 */
public class ZapotrzebowanieLogic extends AbstractDocumentLogic {
    private final static Logger log = LoggerFactory.getLogger(ZapotrzebowanieLogic.class);
    private final static String DWR_PREFIX = "DWR_";


    public final static String ENTRY_DICTIONARY = ZapotrzebowaniePopupManager.ENTRY_DICTIONARY;
    private static final String WNIOSKODAWCA = "WNIOSKODAWCA";
    private static final String WNIOSKODAWCA_JEDNOSTKA = "JEDNOSTKA_ZGLASZAJACA";

    private final static String EDIT_BUTTON = "EDIT";
    private final static String VIEW_BUTTON = "VIEW";
    private static final String DEL_BUTTON = "DEL";
    private final static String ADD_BUTTON = "ADD";
    private final static String SAVE_BUTTON = "SAVE";
    private final static String CANCEL_BUTTON = "CANCEL";

    private static final String SEND_BUTTON = "SAVE_WINWEST";
    private static final String CREATE_BUTTON = "CREATE_WINWEST";


    public static final String DWR_PRZEDMIOT_ZAPOTRZEBOWANIA = "DWR_SLOWNIK_PZ";
    public static final String DWR_WARTOSC_NETTO_TOTAL = "DWR_WARTOSC_NETTO_TOTAL";
    public static final String DOCKIND_CN = "Zapotrzebowanie";
    public static final String WARTOSC_NETTO ="WARTOSC_NETTO";
    public static final String DWR_TO_WINWEST ="DWR_TO_WINWEST";


    public static final Integer MONEY_SCALE = 2;

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {

    }

    @Override
    public void documentPermissions(Document document) throws EdmException {

    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();
        DSUser user = DSApi.context().getDSUser();
        //DSDivision division= DSApi.context().getDSDivision();
        toReload.put(WNIOSKODAWCA, user.getId());

        // toReload.put(WNIOSKODAWCA_JEDNOSTKA, division.getId());

        toReload.put(WNIOSKODAWCA_JEDNOSTKA, user.getDivisionsWithoutGroup().length > 0 ? user.getDivisionsWithoutGroup()[0].getId() : null);
        fm.reloadValues(toReload);
    }

    @Override
    public void setAdditionalTemplateValues(long docId, Map<String, Object> values, Map<Class, Format> defaultFormatters) {

        try {
            Document doc = Document.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            fm.initialize();

            Pattern pattern;
            Matcher matcher;

            Map<Long, Znaki> stamps = new HashMap<Long, Znaki>();
            Map<Long, Znaki> stamps2 = new HashMap<Long, Znaki>();
            DocumentKind zapotrzebowanie = DocumentKind.findByCn(DOCKIND_CN);
            //// formularz V
            List<Long> Ids = getidsfromsql();
            int k = 1;
            for (Long Id : Ids) {

                fm = zapotrzebowanie.getFieldsManager(Id);
                Znaki stamp = new Znaki();
                stamp.setLp(k);
                stamp.setNumerogloszenia(fm.getStringValue("NR_OGLOSZENIA"));
                stamp.setRodzajzamowienia(fm.getLongValue("RODZAJ_ZAMOWIENIA"));
                stamp.setPrzedmiotzamowienia(fm.getLongValue("KODY_CPV"));
                stamp.setTryb(fm.getLongValue("TRYB_ZAM"));
                stamp.setUzasadnienie(fm.getStringValue("UZASADNIENIE_TRYBU"));
                stamp.setKraj(fm.getStringValue("KRAJ_POCHODZENIA"));
                stamp.setWartoscNetto((BigDecimal) fm.getKey("MODUL_SUM_WARTOSC_NETTO"));
                k++;


                stamps.put(Id, stamp);
            }


            ArrayList<Znaki> stampsList = new ArrayList<Znaki>(stamps.values());
            values.put("ZNAKI", stampsList);
//

            //// formularz VI
            List<Long> Ids2 = getidfromsqlssecond();
            int k2 = 1;
            for (Long Id : Ids2) {

                fm = zapotrzebowanie.getFieldsManager(Id);
                Znaki stamp2 = new Znaki();
                stamp2.setLp(k2);
                stamp2.setNumerogloszenia(fm.getStringValue("NR_OGLOSZENIA"));
                stamp2.setRodzajzamowienia(fm.getLongValue("RODZAJ_ZAMOWIENIA"));
                stamp2.setWartoscNetto((BigDecimal) fm.getKey("MODUL_SUM_WARTOSC_NETTO"));
                k2++;


                stamps2.put(Id, stamp2);
            }


            ArrayList<Znaki> stampsList2 = new ArrayList<Znaki>(stamps2.values());
            values.put("ZNAKI2", stampsList2);

















           for(int i=1;i<=34;i++) {
               Long j= Long.valueOf(i);
            values.put("Liczba"+ i, getidfrompodstawprawna(j));
           }
            for(int i=1;i<=35;i++) {
                Long j= Long.valueOf(i);
             values.put("Kwota"+ i, getvaluempodstawprawna(j));
            }


            for(int i=1;i<=10;i++) {
             values.put("Roboty_Budowlane"+ i, getvaluefromsektorowe("sektorowe", 3, i ));
            }
            for(int i=1;i<=10;i++) {
             values.put("DOSTAWY"+ i, getvaluefromsektorowe("sektorowe", 2, i ));
            }
            for(int i=1;i<=10;i++) {
             values.put("USLUGI"+ i, getvaluefromsektorowe("sektorowe", 1, i ));
            }

//tabela IV wartosc z zamowienia
            for(int i=1;i<=9;i++) {
                values.put("ROBOTYVALUZAMOW"+ i, getvaluefromklasycznezam("klasyczne", 3, i ));
            }
            for(int i=1;i<=9;i++) {
                values.put("DOSTAWYVALUZAMOW"+ i, getvaluefromklasycznezam("klasyczne", 2, i ));
            }
            for(int i=1;i<=9;i++) {
                values.put("USLUGIVALUZAMOW"+ i, getvaluefromklasycznezam("klasyczne", 1, i ));
            }

            //tabela IV ilosc z zamowienia
            for(int i=1;i<=9;i++) {
                values.put("ROBOTYCOUNTZAMOW"+ i, getcountfromklasycznezam("klasyczne", 3, i ));
            }
            for(int i=1;i<=9;i++) {
                values.put("DOSTAWYCOUNTZAMOW"+ i, getcountfromklasycznezam("klasyczne", 2, i ));
            }
            for(int i=1;i<=9;i++) {
                values.put("USLUGICOUNTZAMOW"+ i, getcountfromklasycznezam("klasyczne", 1, i ));
            }

            //tabela IV wartosc z umowy
            for(int i=1;i<=3;i++) {
                values.put("ROBOTYVALUEumowa"+ i, getvaluefromklasyczneumowy("klasyczne", 3, i));
            }
            for(int i=1;i<=3;i++) {
                values.put("DOSTAWYVALUEumowa"+ i, getvaluefromklasyczneumowy("klasyczne", 2, i));
            }
            for(int i=1;i<=3;i++) {
                values.put("USLUGIVALUEumowa"+ i, getvaluefromklasyczneumowy("klasyczne", 1, i));
            }

            //tabela IV ilosc z umowy
            for(int i=1;i<=3;i++) {
                values.put("ROBOTYCOUNTumowa"+ i, getvaluefromklasyczneumowy("klasyczne", 3, i));
            }
            for(int i=1;i<=3;i++) {
                values.put("DOSTAWYCOUNTumowa"+ i, getvaluefromklasyczneumowy("klasyczne", 2, i));
            }
            for(int i=1;i<=3;i++) {
                values.put("USLUGICOUNTumowa"+ i, getvaluefromklasyczneumowy("klasyczne", 1, i));
            }





        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }


    private Long getidfrompodstawprawna(Long id) {
        Long ids =null;
        PreparedStatement ps = null;

        try {
           // String sql = "SELECT count(*) FROM DSG_UEK_ZAM_PUB WHERE PODSTAWA_PRAWNA=? ";
            String sql = "SELECT count(*) FROM dsg_uek_zapotrzebowanie WHERE TRYB_ZAMOWIENIA=? ";
            ps = DSApi.context().prepareStatement(sql);
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (!rs.next()) {
                return 0l;
            }

            ids = rs.getLong(1);


        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(ps);
        }

        return ids;
    }



    private BigDecimal getvaluempodstawprawna(Long id) {
        BigDecimal value =null;
        PreparedStatement ps = null;

        try {
            //String sql = "SELECT SUM(MODUL_SUM_WARTOSC_NETTO) FROM DSG_UEK_ZAM_PUB WHERE PODSTAWA_PRAWNA=? ";
            String sql = "SELECT SUM(WYCENA) FROM dsg_uek_zapotrzebowanie WHERE TRYB_ZAMOWIENIA=? ";
            ps = DSApi.context().prepareStatement(sql);
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (!rs.next()) {
                return BigDecimal.valueOf(0);
            }

           value = rs.getBigDecimal(1);


        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(ps);
        }

        return value;
    }
    private BigDecimal getvaluefromsektorowe( String type, Integer Rodzaj, Integer Przedmiot ) {
        BigDecimal value =null;
        PreparedStatement ps = null;

        try {
           // String sql = "SELECT SUM(MODUL_SUM_WARTOSC_NETTO) FROM DSG_UEK_ZAM_PUB WHERE PODSTAWA_PRAWNA IN(?) AND PRZEDMIOT_DZIALALNOSCI=? AND RODZAJ_ZAMOWIENIA=? AND MODUL_SUM_WARTOSC_NETTO BETWEEN 10000 AND 30000";
            String sql = "SELECT SUM(WYCENA) FROM dsg_uek_zapotrzebowanie WHERE TRYB_ZAMOWIENIA IN(?) AND RODZAJ_WNIOSKU=? AND ERP_STATUS=? AND WYCENA BETWEEN 100 AND 500";
            ps = DSApi.context().prepareStatement(sql);
            ps.setArray(1, (java.sql.Array) getzamowienietype(type));
            ps.setLong(2, Long.valueOf(Rodzaj));
            ps.setLong(3,Long.valueOf(Przedmiot));
            ResultSet rs = ps.executeQuery();
            if (!rs.next()) {
                return BigDecimal.valueOf(0);
            }

            value = rs.getBigDecimal(1);


        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(ps);
        }

        return value;
    }


    private Long getcountfromklasyczneumowy( String type, Integer Rodzaj, Integer Tryb ) {
        Long ids =null;
        PreparedStatement ps = null;

        try {
          //  String sql = "SELECT count(*) FROM DSG_UEK_ZAM_PUB WHERE PODSTAWA_PRAWNA IN(?) AND UMOWA_RAMOWA=? AND RODZAJ_ZAMOWIENIA=? AND MODUL_SUM_WARTOSC_NETTO BETWEEN 10000 AND 30000";
            String sql = "SELECT count(*) FROM dsg_uek_zapotrzebowanie WHERE TRYB_ZAMOWIENIA IN(?) AND RODZAJ_WNIOSKU=? AND ERP_STATUS=? AND WYCENA BETWEEN 100 AND 500";

            ps = DSApi.context().prepareStatement(sql);
            ps.setArray(1, (java.sql.Array) getzamowienietype(type));
            ps.setLong(2, Long.valueOf(Rodzaj));
            ps.setLong(3,Long.valueOf(Tryb));
            ResultSet rs = ps.executeQuery();
            if (!rs.next()) {
                return 0l;
            }

            ids = rs.getLong(1);


        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(ps);
        }

        return ids;
    }
    private BigDecimal getvaluefromklasyczneumowy( String type, Integer Rodzaj, Integer Tryb ) {
        BigDecimal value =null;
        PreparedStatement ps = null;

        try {
           // String sql = "SELECT SUM(MODUL_SUM_WARTOSC_NETTO) FROM DSG_UEK_ZAM_PUB WHERE PODSTAWA_PRAWNA IN(?) AND UMOWA_RAMOWA=? AND RODZAJ_ZAMOWIENIA=? AND MODUL_SUM_WARTOSC_NETTO BETWEEN 10000 AND 30000";
            String sql = "SELECT SUM(WYCENA) FROM dsg_uek_zapotrzebowanie WHERE TRYB_ZAMOWIENIA IN(?) AND RODZAJ_WNIOSKU=? AND ERP_STATUS=? AND WYCENA BETWEEN 100 AND 500";

            ps = DSApi.context().prepareStatement(sql);
            ps.setArray(1, (java.sql.Array) getzamowienietype(type));
            ps.setLong(2, Long.valueOf(Rodzaj));
            ps.setLong(3,Long.valueOf(Tryb));
            ResultSet rs = ps.executeQuery();
            if (!rs.next()) {
                return BigDecimal.valueOf(0);
            }

            value = rs.getBigDecimal(1);


        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(ps);
        }

        return value;
    }
    private BigDecimal getvaluefromklasycznezam( String type, Integer Rodzaj, Integer Tryb ) {
        BigDecimal value =null;
        PreparedStatement ps = null;

        try {
            String sql = "SELECT SUM(MODUL_SUM_WARTOSC_NETTO) FROM DSG_UEK_ZAM_PUB WHERE PODSTAWA_PRAWNA IN(?) AND TRYB_ZAM=? AND RODZAJ_ZAMOWIENIA=? AND MODUL_SUM_WARTOSC_NETTO BETWEEN 10000 AND 30000";

            ps = DSApi.context().prepareStatement(sql);
            ps.setArray(1, (java.sql.Array) getzamowienietype(type));
            ps.setLong(2, Long.valueOf(Rodzaj));
            ps.setLong(3,Long.valueOf(Tryb));
            ResultSet rs = ps.executeQuery();
            if (!rs.next()) {
                return BigDecimal.valueOf(0);
            }

            value = rs.getBigDecimal(1);


        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(ps);
        }

        return value;
    }
    private Long getcountfromklasycznezam( String type, Integer Rodzaj, Integer Tryb ) {
        Long ids =null;
        PreparedStatement ps = null;
        try {
            String sql = "SELECT count(*) FROM DSG_UEK_ZAM_PUB WHERE PODSTAWA_PRAWNA IN(?) AND TRYB_ZAM=? AND RODZAJ_ZAMOWIENIA=? AND MODUL_SUM_WARTOSC_NETTO BETWEEN 10000 AND 30000";

            ps = DSApi.context().prepareStatement(sql);
            ps.setArray(1, (java.sql.Array) getzamowienietype(type));
            ps.setLong(2, Long.valueOf(Rodzaj));
            ps.setLong(3,Long.valueOf(Tryb));
            ResultSet rs = ps.executeQuery();
            if (!rs.next()) {
                return 0l;
            }

            ids = rs.getLong(1);


        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(ps);
        }

        return ids;
    }





    public static ArrayList<Long> getzamowienietype(String type) {

        ArrayList<Long> types = new ArrayList<Long>();

        String[] zamowienia =Docusafe.getAdditionProperty(("zamowienie."+type)).split(",");

        for(String s: zamowienia) {
          Long number=Long.parseLong(s);
          types.add(number);

        }

        return types;
    }


    private List<Long> getidsfromsql() {
        List<Long> output= new ArrayList();
        PreparedStatement ps = null;
        try {
            String sql = "SELECT document_id FROM DSG_UEK_ZAM_PUB WHERE MODUL_SUM_WARTOSC_NETTO_EURO >15000";
            ps = DSApi.context().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            for (int i=1;!rs.next();i++){
            output.add(rs.getLong(i));}
            if (!rs.next()) {
                return null;
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        } finally {
            DbUtils.closeQuietly(ps);
        }
        return output;
    }

    private List<Long> getidfromsqlssecond() {
        List<Long> output= new ArrayList();
        PreparedStatement ps = null;
        try {
            String sql = "SELECT document_id FROM DSG_UEK_ZAM_PUB WHERE( RODZAJ_ZAMOWIENIA=3  AND MODUL_SUM_WARTOSC_NETTO >20000000) OR ( RODZAJ_ZAMOWIENIA IN(1,2) AND MODUL_SUM_WARTOSC_NETTO >10000000)";
            ps = DSApi.context().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            for (int i=1;!rs.next();i++){
                output.add(rs.getLong(i));}
            if (!rs.next()) {
                return null;
            }

        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        } finally {
            DbUtils.closeQuietly(ps);
        }
        return output;
    }


    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        StringBuilder msgBuilder = new StringBuilder();

        boolean wasOpened = false;
        try {
        //Popup Zrodla Finansowania
            wasOpened = DSApi.openContextIfNeeded();
        if(DwrUtils.isSenderField(values,DWR_PREFIX+ADD_BUTTON,DWR_PREFIX+CANCEL_BUTTON)){
            ZapotrzebowaniePopupManager.clearPopup(values, fm);
        }else if (DwrUtils.isSenderField(values,DWR_PREFIX+SAVE_BUTTON)) {

           /* ZapotrzebowanieEntry entry = ZapotrzebowaniePopupManager.saveEntry(values, fm);
            Collection<Long> ids =  DwrUtils.updateMultipleDictionary(values,ENTRY_DICTIONARY,entry.getId(),null);
            getTotal2(values,ids);*/
            ZapotrzebowanieEntry entry = ZapotrzebowaniePopupManager.saveEntry2(values, fm);
            Collection<Long> ids = ZapotrzebowaniePopupManager.updateZapatrzebowanieDictionary(values,entry.getId(),null);
            getTotal2(values,ids);

            ZapotrzebowaniePopupManager.clearPopup(values,fm);
        }else if(DwrUtils.isSenderField(values,DWR_PREFIX+ ENTRY_DICTIONARY)){
            String entryButtonName = DwrUtils.senderButtonFromDictionary(values, ENTRY_DICTIONARY, EDIT_BUTTON);
            if(entryButtonName == null){
                entryButtonName = DwrUtils.senderButtonFromDictionary(values, ENTRY_DICTIONARY, VIEW_BUTTON);
            }
            //klikni�to edit lub view button
            if(entryButtonName != null){
                Integer rowNo = Integer.parseInt(entryButtonName.substring(entryButtonName.lastIndexOf("_") + 1));
                ZapotrzebowaniePopupManager.updatePopup(values,fm,rowNo);

            }
            //klikni�to delete button
            else{
                String delButtonName = DwrUtils.senderButtonFromDictionary(values, ENTRY_DICTIONARY, DEL_BUTTON);
                if(delButtonName != null){
                    Integer rowNo = Integer.parseInt(delButtonName.substring(delButtonName.lastIndexOf("_") + 1));
                    ZapotrzebowanieEntry entry2 = ZapotrzebowaniePopupManager.deleteEntry(values, fm, rowNo);
                    Collection<Long> ids = ZapotrzebowaniePopupManager.updateZapatrzebowanieDictionary(values,null,entry2.getId());
                    getTotalafterdel(values, ids);
                }
            }
        }


            else if (DwrUtils.isSenderField(values, DWR_PREFIX + SEND_BUTTON)) {
                try{
                    wasOpened = DSApi.openContextIfNeeded();
                    readDocument(values,fm);


                }finally {
                   DSApi.closeContextIfNeeded(wasOpened);
                }
            }
            else if (DwrUtils.isSenderField(values, DWR_PREFIX + CREATE_BUTTON)) {
                try{
                    wasOpened = DSApi.openContextIfNeeded();
                    generateDocument(fm,values);

                }finally {
                   DSApi.closeContextIfNeeded(wasOpened);
                }
            }



        } catch (Exception e) {
            log.error(e.getMessage(), e);
            msgBuilder.append(e.getMessage());
        } finally {
            DSApi.closeContextIfNeeded(wasOpened);
        }
        if (msgBuilder.length() > 0) {
            values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
            return new Field("messageField", msgBuilder.toString(), Field.Type.BOOLEAN);
        }
        return null;
    }



    private static String senderButtonFromDictionary(Map<String, FieldData> values, String dictionaryName, String buttonName) throws EdmException {

        Map<String, FieldData> buttons = DwrUtils.getDictionaryFields(values, dictionaryName, buttonName);

        for (Map.Entry<String, FieldData> button : buttons.entrySet()) {
            if (button.getValue().getType().equals(Field.Type.BUTTON) && button.getValue().getButtonData().isClicked()) {
                button.getValue().getButtonData().setClicked(false);
                return button.getKey();
            }
        }

        return null;
    }


    private void getTotal2(Map<String, FieldData> values,Collection <Long>ids) throws EdmException {


        ZapotrzebowanieEntry entry;

        if (values.containsKey(DWR_PRZEDMIOT_ZAPOTRZEBOWANIA) && values.get(DWR_PRZEDMIOT_ZAPOTRZEBOWANIA) != null) {

            BigDecimal sum = new BigDecimal(0);
            sum = sum.setScale(MONEY_SCALE);
            ids.removeAll(Collections.singleton(null));

            for(Long id :ids){


                entry = ZapotrzebowanieEntry.findById(id);

                BigDecimal first = entry.getWartoscNetto();
                first.setScale(MONEY_SCALE);

                sum = sum.add(first);

                if (entry == null) {
                    throw new EdmException("B��d inicjalizacji Popup: b��dne Id �r�d�a finansowania;");
                }}
            values.get(DWR_WARTOSC_NETTO_TOTAL).setData(0);
            values.get(DWR_WARTOSC_NETTO_TOTAL).setMoneyData(sum);


        }

    }
    private void getTotalafterdel(Map<String, FieldData> values,Collection <Long>ids) throws EdmException {
        try {

        ZapotrzebowanieEntry entry;

       if (values.containsKey(DWR_PRZEDMIOT_ZAPOTRZEBOWANIA) && values.get(DWR_PRZEDMIOT_ZAPOTRZEBOWANIA) != null) {

            BigDecimal sum = new BigDecimal(0);
            sum = sum.setScale(MONEY_SCALE);
            ids.removeAll(Collections.singleton(null));

            for(Long id :ids){
                entry = ZapotrzebowanieEntry.findById(id);
                BigDecimal first = entry.getWartoscNetto();
                first.setScale(MONEY_SCALE);
                sum = sum.add(first);
                if (entry == null) {
                    values.get(DWR_WARTOSC_NETTO_TOTAL).setData(0);
                }}
            values.get(DWR_WARTOSC_NETTO_TOTAL).setData(0);
            values.get(DWR_WARTOSC_NETTO_TOTAL).setMoneyData(sum);


        }
        else{

            values.get(DWR_WARTOSC_NETTO_TOTAL).setData(0);


        } } catch (Exception e) {
            log.error(e.getMessage(), e);

        }

    }


    private List<Map<String, String>> zwrocListeZTabeli(
            Collection<EnumItem> availableItems) {
        List<Map<String, String>> itemy = new ArrayList<Map<String, String>>();
        HashMap<String, String> mapa = new HashMap<String, String>();
        mapa.put("", "--wybierz--");
        itemy.add(mapa);
        for (EnumItem e : availableItems) {
            mapa = new HashMap<String, String>();
            mapa.put(e.getId().toString(), e.getTitle());
            itemy.add(mapa);
        }
        return itemy;
    }



    private void generateDocument(FieldsManager fm,Map<String, FieldData> values) throws EdmException {

        try {
            DSApi.context().begin();
            Long documentId;


            DocumentKind zapotrzebowanie = DocumentKind.findByCn(DOCKIND_CN);
            documentId = fm.getDocumentId();
            OfficeDocument doc = OfficeDocument.find(documentId);
            int ko = doc.getOfficeNumber();

            DocumentKind wniosekinwestycyjny = DocumentKind.findByCn(WniosekInwestycyjnyLogic.DOCKIND_CN);

            OutOfficeDocument generatedDocument = new OutOfficeDocument();


            List<Recipient> recipients= doc.getRecipients();
            Recipient r = recipients.get(0);
            DSUser user = extractUserFromRecipent(r);
            String users = user.getName();
            Long userid= user.getId();
            DSDivision div = extractDivisionFromRecipent(r);
            String divs = div.getName();
            Long divId= div.getId();
            String guid = extractGuidFromRecipent(r);

            //uzupelnia pole wnioskodawca

            generatedDocument.setCreatingUser(users);
            generatedDocument.setCurrentAssignmentGuid(guid);
            generatedDocument.setDivisionGuid(guid);
            generatedDocument.setAuthor(users);
            generatedDocument.setDocumentKind(wniosekinwestycyjny);
            generatedDocument.setSummary(wniosekinwestycyjny.getName());
            generatedDocument.setAssignedDivision(divs);
            generatedDocument.create() ;


            generatedDocument.setInternal(true);


            Map nowa = new HashMap();
            nowa.put("TO_WINWEST",generatedDocument.getId());

           zapotrzebowanie.setOnly(doc.getId(), nowa);

            Map toinw = new HashMap();
            toinw.put("JEDNOSTKA_ZGLASZAJACA",divId);
            toinw.put("WNIOSKODAWCA",userid);


            wniosekinwestycyjny.setOnly(generatedDocument.getId(),toinw);
            wniosekinwestycyjny.setOnly(generatedDocument.getId(), ImmutableMap.of("TO_ZAPOTRZEBOWANIE", (documentId)));

            Map ID = new HashMap();
            ID.put("id", new FieldData(Field.Type.LONG, generatedDocument.getId()));
            values.get("DWR_TO_WINWEST").setDictionaryData(ID);


            DSApi.context().session().save(generatedDocument);


            DocumentReplicator.bindToJournal(generatedDocument);
            generatedDocument.getDocumentKind().logic().onStartProcess(generatedDocument,null);
            TaskSnapshot.updateByDocument(generatedDocument);
            DSApi.context().commit();



        } catch (Exception e) {
            log.error(e.getMessage(), e);
            DSApi.context().setRollbackOnly();

        }
    }
    private void readDocument(Map<String, FieldData> values, FieldsManager fm) throws EdmException {

        try {
            DSApi.context().begin();
            Long documentId;


            DocumentKind zapotrzebowanie = DocumentKind.findByCn(DOCKIND_CN);
            documentId = fm.getDocumentId();

            DocumentKind wniosekinwestycyjny = DocumentKind.findByCn(WniosekInwestycyjnyLogic.DOCKIND_CN);


            Map<String, FieldData> dwrValues = (Map<String, FieldData>) values.get(DWR_TO_WINWEST).getDictionaryData();
            for (String key : dwrValues.keySet()) {
                if (key.contains("id") && dwrValues.get(key).getData() != null) {
                    Long amount = dwrValues.get(key).getLongData();
                    OfficeDocument doc = OfficeDocument.find(amount);
                    wniosekinwestycyjny.setOnly(amount, ImmutableMap.of("TO_ZAPOTRZEBOWANIE", documentId));


                }
            }


            DSApi.context().commit();



        } catch (Exception e) {
            log.error(e.getMessage(), e);
            DSApi.context().setRollbackOnly();

        }
    }



    private Object findProcessVariable(Long documentId, String variableName) {
        RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
        if (runtimeService != null) {
            ExecutionQuery query = runtimeService.createExecutionQuery().processVariableValueEquals("docId", documentId);
            Execution execution = query.list().size() > 0 ? query.list().get(0) : null;
            if (execution != null) {
                return runtimeService.getVariable(execution.getId(), variableName);
            }
        }
        return null;
    }

    /**
     * Returns name of current process activity
     *
     * @param documentId
     * @return
     */
    private String currentProcessActivity(Long documentId) {
        RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
        if (runtimeService != null) {
            ExecutionQuery query = runtimeService.createExecutionQuery().processVariableValueEquals("docId", documentId);
            List<Execution> execList = query.list();
            if (execList != null) {
                for (int i = 0; i < execList.size(); i++) {
                    Execution execution = query.list().get(i);
                    if (execution.getActivityId() != null) {
                        return execution.getActivityId();
                    }
                }
            }
        }
        return null;
    }




    private static BigDecimal getBigDecimal(Float value) {
        return value == null ? null : new BigDecimal(value);
    }

    private DSDivision extractDivisionFromRecipent(Recipient recipient) throws EdmException {
        DSDivision div = null;
        String guid = extractGuidFromRecipent(recipient);
        if (guid != null) {
            div = DSDivision.find(guid);
        }
        if(div == null){
            String divName = recipient.getOrganizationDivision();
            div = DSDivision.findByName(divName);
        }
        return div;
    }

    /**
     * Extracts DSDivision guid from given recipent
     * @param recipient
     * @return extracted DSDivision guid or <Code>null</Code> if no guid can be extracted
     * @throws NullPointerException if recipent is null
     */
    private String extractGuidFromRecipent(Recipient recipient) {
        String guid = null;
        Pattern pat = Pattern.compile( ".*;d:(\\w*)");
        Matcher m = pat.matcher(recipient.getLparam());
        if(m.find()){
            guid = m.group(1);
        }
        return guid;
    }


    /**
     * Extracts DSUser from recipent
     * @param recipient
     * @return extracted DSUser or <Code>null</Code> if no DSUser can be extracted
     * @throws EdmException
     * @throws NullPointerException - if recipent is null
     */
    private DSUser extractUserFromRecipent(Recipient recipient) throws EdmException {
        if(recipient == null){
            throw new NullPointerException();
        }
        String firstName = recipient.getFirstname();
        String lastName = recipient.getLastname();

        DSUser user = DSUser.findByFirstnameLastname(firstName, lastName);
        String guid = extractGuidFromRecipent(recipient);
        if (user != null && guid != null && DSDivision.isInDivision(guid,user.getName())) {
            return user;
        }else if(guid != null && firstName != null && lastName != null){
            user = null;
            DSDivision div = DSDivision.find(guid);
            DSUser[] users = div.getUsers();
            for(DSUser u : users){
                if(firstName.equals(u.getFirstname()) && lastName.equals(u.getLastname())){
                    user = u;
                    break;
                }
            }
        }

        return user;
    }




























    @Override
    public ExportedDocument buildExportDocument(OfficeDocument doc) throws EdmException {
        boolean wasOppened = false;
        try {
            wasOppened = DSApi.openContextIfNeeded();


            ReservationDocument exported = new ReservationDocument();
            FieldsManager fm = doc.getFieldsManager();
            exported.setReservationStateFromId(1);

            //exported.setReservationNumber(fm.getStringKey(ERP_DOCUMENT_IDM));

            //exported.getHeaderBudget().setBudgetDirectCode(Docusafe.getAdditionPropertyOrDefault("export.dokument_rezerwacyjny.idm_budzetu", "IMGW-2014\\00001")/* IDM BUD?ETU, OKRESU BUD?ETOWEGO, USTAWI? NA STA?E WG SERWIS?W ERPOWYCH. DOCELOWO TRZEBA WYRZUCI? DO NAG??WKA*/);
            exported.getHeaderBudget().setBudgetDirectId(Long.valueOf(Docusafe.getAdditionPropertyOrDefault("export.zapotrzebowanie.id_budzetu", "10")));
            exported.setBudgetReservationTypeCode(Docusafe.getAdditionPropertyOrDefault("export.zapotzrebownie", "WMR"));
            exported.setReservationMethod(ReservationDocument.ReservationMethod.AUTO);




            List<BudgetItem> positionItems = Lists.newArrayList();
            List<Long> entriesIds = (List) fm.getKey(ZapotrzebowaniePopupManager.ENTRY_DICTIONARY);


            if (entriesIds != null) {
                validateEntries(entriesIds);
                BudgetItem bItem;
                int nrp = 1;
                for (Long entryId : entriesIds) {

                    //zrodlo.setEntryNo(nrp);
                    ZapotrzebowanieEntry entry = ZapotrzebowanieEntry.findById(entryId);
                    ZrodloFinansowaniaDict zrodlo = entry.getZrodloFinansowania();

                    positionItems.add(bItem = new BudgetItem());
                    bItem.setItemNo(nrp);

                    java.math.BigDecimal bd = new java.math.BigDecimal(String.valueOf(entry.getIlosc()));
                    bItem.setQuantity(bd);
                    bItem.setNetAmount((BigDecimal) entry.getWartoscNetto());
                    bItem.setName(entry.getDataPozyskania() != null ? "Wymagany termin pozyskania: " + DateUtils.formatCommonDate((Date) entry.getDataPozyskania()) : null);
                    bItem.setCostKindName(ObjectUtils.toString(entry.getNazwaPrzedmiotu()));
                    bItem.setBudgetDirectId(exported.getHeaderBudget().getBudgetDirectId());
                    bItem.setCostKindCode(ObjectUtils.toString(entry.getProdukty()));
                    bItem.setPositionCode(entry.getSrodkiString() != null ? "Przesuni�cie �rodk�w: " + entry.getSrodkiString() : null);
                    bItem.setResourceCode(entry.getRodzajDzialalnosci() != null ? "Rodzaj dzia�alno�ci: " + entry.getRodzajDzialalnosci() : null);
                    bItem.setBudgetTaskCode(entry.getPrzeznaczenieZakupu() != null ? "Przeznaczenie zakupu: " + ObjectUtils.toString(entry.getPrzeznaczenieZakupu()) : null);

                    if (exported.getHeaderBudget().getBudgetKoId() == null) {
                        exported.getHeaderBudget().setBudgetKoId(bItem.getBudgetKoId());
                    }
                    addZrodlaFinansowania(bItem, zrodlo);
                    positionItems.add(bItem);
                    nrp++;
                }


            }
            exported.setPostionItems(positionItems);
            exported.setDescription(buildAdditionDescription(getAdditionFields(doc, fm)));

            String systemPath;

            if (Docusafe.getAdditionProperty("erp.faktura.link_do_skanu") != null) {
                systemPath = Docusafe.getAdditionProperty("erp.faktura.link_do_skanu");
            } else {
                systemPath = Docusafe.getBaseUrl();
            }

            for (Attachment zal : doc.getAttachments()) {
                AttachmentInfo info = new AttachmentInfo();

                String path = systemPath + "/repository/view-attachment-revision.do?id=" + zal.getMostRecentRevision().getId();

                info.setName(zal.getTitle());
                info.setUrl(path);

                exported.addAttachementInfo(info);}
            return exported;
        }catch (Exception e){
            throw new EdmException(e);
        } finally{
            DSApi.closeContextIfNeeded(wasOppened);
        }}


    Map<String,String> getAdditionFields(OfficeDocument doc, FieldsManager fm) throws EdmException {
        Map<String,String> additionFields = com.google.common.collect.Maps.newLinkedHashMap();
        additionFields.put("Nr KO",doc.getOfficeNumber() != null ? doc.getOfficeNumber().toString() : "brak");
        additionFields.put("Uzasadnienie zakupu", fm.getStringValue("UZASADNIENIE_ZAKUPU") != null ? fm.getStringValue("UZASADNIENIE_ZAKUPU").replaceAll("\\<.*?\\>", "") : null);
        additionFields.put("Og�em warto�� netto", fm.getStringValue("WARTOSC_NETTO_TOTAL"));
        additionFields.put("Numer zapotrzebowania", fm.getStringValue("NUMER_ZAPOTRZEBOWANIA"));
        additionFields.put("Wydatek strukturalny-kod", fm.getStringValue("WS_KOD"));
        additionFields.put("Wydatek strukturalny-obszar", fm.getStringValue("WS_OBSZAR"));
        additionFields.put("Wydatek strukturalny-kwota", fm.getStringValue("WS_KWOTA"));
        additionFields.put("Jednostaka zg�aszaj�ca", fm.getStringValue("JEDNOSTKA_ZGLASZAJACA"));
        additionFields.put("Wnioskodawca", fm.getStringValue("WNIOSKODAWCA"));
        additionFields.put("Rodzaj wniosku", fm.getStringValue("RODZAJ_WNIOSKU"));
        additionFields.put("Tryb zaam�wienia", fm.getStringValue("TRYB_ZAMOWIENIA"));
        additionFields.put("Opis zam�wienia", fm.getStringValue("OPIS_ZAMOWIENIA"));
        additionFields.put("Wniosek inwestycyjny", fm.getStringValue("TO_WINWEST"));



        return  additionFields;
    }

    static String buildAdditionDescription(Map<String, String> additionField) throws EdmException {
        StringBuilder description = new StringBuilder();
        Map<String, String> additionFields = additionField;
        for (Map.Entry<String, String> item : additionFields.entrySet()) {
            if (!Strings.isNullOrEmpty(item.getValue())) {
                description.append(item.getKey())
                        .append(": ")
                        .append(item.getValue())
                        .append("; ");
            }
        }
        return description.toString();
    }


        /**
         * TODO
         * @param entriesIds
         */
        private void validateEntries (List < Long > entriesIds) {
            //todo
        }


        private void addZrodlaFinansowania (BudgetItem bItem, ZrodloFinansowaniaDict zrodlo)throws EdmException {

            bItem.setBudgetKindId(zrodlo.getBudgetKind());
            bItem.setBudgetDirectId(zrodlo.getBudget());
            bItem.setBudgetKoId(zrodlo.getBudgetKo());
            bItem.setPositionId(zrodlo.getPosition());
            bItem.setProjectId(zrodlo.getContract());
            bItem.setResourceId(zrodlo.getFund());

            if (zrodlo.getFinancialTask() != null) {
                List<pl.compan.docusafe.dictionary.erp.simple.FinancialTask> financialTasks = pl.compan.docusafe.dictionary.erp.simple.Dictionary.find("erpId", zrodlo.getFinancialTask().doubleValue(), pl.compan.docusafe.dictionary.erp.simple.FinancialTask.class);
                if (financialTasks != null && financialTasks.size() == 1) {
                    pl.compan.docusafe.dictionary.erp.simple.FinancialTask financialTask = financialTasks.get(0);
                    pl.compan.docusafe.core.exports.FinancialTask exportFinancialTask = new pl.compan.docusafe.core.exports.FinancialTask();
                    exportFinancialTask.setTaskCode(financialTask.getIdm());
                    exportFinancialTask.setPercent(new BigDecimal(100));
                    bItem.setFinanancialTasks(Lists.newArrayList(exportFinancialTask));
                } else if (financialTasks != null && financialTasks.size() > 1) {
                    throw new EdmException("Znaleziono wi�cej ni� jedno zadanie o erpId = " + zrodlo.getSource());
                } else {
                    throw new EdmException("Nie znaleziono zadania o erpId = " + zrodlo.getSource());
                }
            }

            if (zrodlo.getSource() != null) {
                FundSource fItem = new FundSource();
                List<pl.compan.docusafe.dictionary.erp.simple.Source> sources = pl.compan.docusafe.dictionary.erp.simple.Dictionary.find("erpId", zrodlo.getSource().doubleValue(), pl.compan.docusafe.dictionary.erp.simple.Source.class);
                if (sources != null && sources.size() == 1) {
                    pl.compan.docusafe.dictionary.erp.simple.Source source = sources.get(0);
                    fItem.setCode(source.getIdm());
                    fItem.setFundPercent(new BigDecimal(100));
                    bItem.setFundSources(Lists.newArrayList(fItem));
                } else if (sources != null && sources.size() > 1) {
                    throw new EdmException("Znaleziono wi�cej ni� jedno �rod�o finansowania o erpId = " + zrodlo.getSource());
                } else {
                    throw new EdmException("Nie znaleziono �rod�a finansowania o erpId = " + zrodlo.getSource());
                }
            }
        }

        }

