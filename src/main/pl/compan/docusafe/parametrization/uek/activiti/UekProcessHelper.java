package pl.compan.docusafe.parametrization.uek.activiti;


import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ExecutionQuery;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PropertyNotFoundException;
import pl.compan.docusafe.core.activiti.NotFoundSolution;
import pl.compan.docusafe.core.activiti.helpers.ActivitiDockindHelper;
import pl.compan.docusafe.core.activiti.helpers.CorrectionHelper;
import pl.compan.docusafe.core.activiti.helpers.ProfileAssignmentHelper;
import pl.compan.docusafe.core.activiti.helpers.UserAssignmentHelper;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.jbpm4.ProfileInDivisionAssignmentHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.dictionary.erp.simple.*;
import pl.compan.docusafe.parametrization.uek.hbm.InvoiceEntry;
import pl.compan.docusafe.parametrization.uek.hbm.WnioskujacyDict;
import pl.compan.docusafe.parametrization.uek.hbm.ZapotrzebowanieEntry;
import pl.compan.docusafe.parametrization.uek.hbm.ZrodloFinansowaniaDict;
import pl.compan.docusafe.parametrization.uek.utils.InvoicePopupManager;
import pl.compan.docusafe.parametrization.uek.utils.ZapotrzebowaniePopupManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Damian on 31.03.14.
 */

@Component("uekProcessHelper")
public class UekProcessHelper {



    private final String Z_DIVISION ="JEDNOSTKA_ZGLASZAJACA";
    private final String TRYB_ZAMOWIENIA ="TRYB_ZAMOWIENIA";

    public static final String PRZEDMIOT_ZAPOTRZEBOWANIA = "SLOWNIK_PZ";
    public final static String ZRODLO_ID =PRZEDMIOT_ZAPOTRZEBOWANIA + "_ID";

    public static final String WARTOSC_NETTO_TOTAL = "WARTOSC_NETTO_TOTAL";
    public static final String WARTOSC_NETTO ="WARTOSC_NETTO";
    public static final String TO_WINWEST ="TO_WINWEST";

    public static final String TO_ZAPOTRZEBOWANIE2 = "TO_ZAPOTRZEBOWANIE";
    public static final Integer MONEY_SCALE = 2;

    private final static Logger log = LoggerFactory.getLogger(UekProcessHelper.class);
    
    private final static StringManager SM = GlobalPreferences.loadPropertiesFile(UserAssignmentHelper.class.getPackage().getName(), null);


    @Autowired
    ProfileAssignmentHelper profileAssignmentHelper;
    @Autowired
    UserAssignmentHelper userAssignmentHelper;
    @Autowired
    ActivitiDockindHelper activitiDockindHelper;
    @Autowired
    CorrectionHelper correctionHelper;

    public void catchProcess(DelegateExecution execution) throws Exception {
        System.out.println("proces (definition: \" + execution.getProcessDefinitionId() + \") id = \"+ execution.getProcessInstanceId() + \"  dla dokumentu id = \" + docId + \" zosta� z�apany na etapie \" + ((ExecutionEntity) execution).getEventSource().getId()");
        int throwEx = 0;

        switch (throwEx){
            case 1:{
                throw new EdmException("B��d (EdmException) rzucony do testu na etapie" + ((ExecutionEntity) execution).getEventSource().getId());
            }
            case 2:{
                throw new Exception("B��d (Exception) rzucony do testu na etapie " + ((ExecutionEntity) execution).getEventSource().getId() );
            }
            default:{
                //Dodaje do loga jako error, �eby przypomina�o o usuni�ciu wszystkich takich �apaczy przed produkcj� :-)

                log.error("proces (definition: " + execution.getProcessDefinitionId() + ") id = "+ execution.getProcessInstanceId() + "  dla dokumentu id = " /*+ docId*/ + " zosta� z�apany na etapie " + ((ExecutionEntity) execution).getEventSource().getId());
                break;
            }
        }
    }

    public String getInvCandidatesAfterExportFail(Long docId) throws EdmException {
        try{
            StringBuilder names = new StringBuilder();
            List<DocumentAcceptance> documentAcceptance = DocumentAcceptance.find(docId,"kontrola_ksiegowa");
            for (DocumentAcceptance acceptance: documentAcceptance) {
                names.append(acceptance.getUsername()).append(",");
            }
            if(names.lastIndexOf(",") != -1){
                names.deleteCharAt(names.lastIndexOf(","));
            }
            return NotFoundSolution.checkCandidates(names.toString(),NotFoundSolution.EXCEPTION);
        }catch (EdmException e){
            return profileAssignmentHelper.allUsersWithProfileIdFromAdds("profile.id.akceptacja_vat");
        }
    }

    public String getControllers(Long docId, String profileIdAdds) throws EdmException {
        try {
            Document doc = Document.find(docId);
            FieldsManager fm = doc.getFieldsManager();

            return profileAssignmentHelper.allUsersWithProfileIdFromAdds(profileIdAdds);

        } catch (EdmException e){
            throw e;
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            throw new EdmException(e);
        }
    }

    public String getChancellors(Long docId, String profileIdAdds, Integer scopeLvl) throws EdmException {
        return profileAssignmentHelper.allUsersWithProfileIdFromAdds(profileIdAdds, NotFoundSolution.EXCEPTION.name());
    }

    public Collection<String> getProducers(Long docId,DelegateExecution execution, String profileIdAdds, int scopeLvl) throws EdmException {
        try{
            //wyplucie kolekcji correction user je�eli przekazano do poprawy
            String correctionUser = execution.getVariable(correctionHelper.CORRECTION_USER_VARIABLE).toString();
            if(correctionUser != null && !"noone".equals(correctionUser)){
                Collection<String> cUsers = new ArrayList<String>();
                cUsers.add(correctionUser);
                return cUsers;
            }


            OfficeDocument doc = OfficeDocument.find(docId);

            Set<String> userNamesVar = new HashSet<String>();
            List<Recipient> recipients = doc.getRecipients();

            for(Recipient recipient : recipients){
                DSUser user = extractUserFromRecipent(recipient);
                if (user != null) {
                    userNamesVar.add(user.getName());
                }else {
                    DSDivision div = extractDivisionFromRecipent(recipient);
                    if (div != null) {
                        List<String> userNames = new ArrayList<String>();
                        Long profileId = getProfileIdFromAdds(profileIdAdds);
                        ProfileInDivisionAssignmentHandler.putUsersFromProfile(userNames,div.getId(),profileId,false,scopeLvl,false);
                        userNamesVar.addAll(userNames);
                    }
                }
            }
            if(userNamesVar.isEmpty()){
                throw new UserNotFoundException("Nie znaleziono realizator�w");
            }

            return userNamesVar;
        }catch (EdmException e){
            throw e;
        }catch (Exception e){
            log.error(e.getMessage(),e);
            throw new EdmException(e);
        }
    }

    public Collection<String> getApplicants(DelegateExecution execution, Map<String, Set<Object>> applicants) throws EdmException {
        //wyplucie kolekcji correction user je�eli przekazano do poprawy
        String correctionUser = execution.getVariable(correctionHelper.CORRECTION_USER_VARIABLE).toString();
        if(correctionUser != null && !"noone".equals(correctionUser)){
            Collection<String> cUsers = new ArrayList<String>();
            cUsers.add(correctionUser);
            return cUsers;
        }else{
           return applicants.keySet();
        }
    }


    public Map<String, Set<Object>> getApplicantsMap(Long docId, String profileIdAdds, int scopeLvl) throws EdmException {
        try {
            Document doc = Document.find(docId);
            FieldsManager fm = doc.getFieldsManager();

            List<Long> dicId = (List<Long>) fm.getKey(InvoicePopupManager.ENTRY_DICTIONARY);;
            Map<String,Set<Object>> usersRows = new HashMap<String, Set<Object>>();

            for(Long entryId : dicId){
                InvoiceEntry entry = InvoiceEntry.findById(entryId);
                if(entry != null && entry.getWniokujacy() != null){
                    Set<WnioskujacyDict> applicants = entry.getWniokujacy();
                    for(WnioskujacyDict applicant : applicants){
                        DSUser user = DSUser.findById(applicant.getUserId());
                        if(user == null){
                            throw new Exception("Brak u�ytkownika o id = " + applicant.getUserId());
                        }
                        user = user.getSupervisor() != null ? user.getSupervisor() : user;
                        Set<Object> rowIds;
                        if(usersRows.containsKey(user.getName())){
                            rowIds = usersRows.get(user.getName());
                        }else{
                            rowIds = new HashSet<Object>();
                        }
                        if (rowIds != null) {
                            rowIds.add(entryId);
                            usersRows.put(user.getName(),rowIds);
                        }else{
                            throw new Exception("B��d przy przypisywaniu zadania u�ytkownikowi " + user.getName());
                        }
                    }
                }
            }

            if(usersRows.isEmpty()){
                throw new EdmException("B��d przygotowania u�ytkownik�w wnioskuj�cych");
            }

            return usersRows;

        } catch (EdmException e){
            throw e;
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            throw new EdmException(e);
        }
    }

    public String getOneRecipent(Long docId) throws EdmException {
        try{

            OfficeDocument doc = OfficeDocument.find(docId);
            DSUser user;

            List<Recipient> recipients = doc.getRecipients();
            String out = "";
            Iterator iterator = recipients.iterator();

            for (Recipient r : recipients) {

                iterator.next();
                user = extractUserFromRecipent(r);
                out = (out + user.getName());
                if(iterator.hasNext()){

                    out = (out+",");
                }

            }

            if (out.equals("")) {
                throw new UserNotFoundException("Brak realizator�w...");
            }

            return out;

        }catch(EdmException e) {

            throw e;
        }
    }
    public String getOneRecipentSupervisor(Long docId) throws EdmException {
        try{

            OfficeDocument doc = OfficeDocument.find(docId);
            DSUser user;
            DSUser supervisor;
            List<Recipient> recipients = doc.getRecipients();
            String out = "";
            Iterator iterator = recipients.iterator();

            for (Recipient r : recipients) {

                iterator.next();
                user = extractUserFromRecipent(r);
                supervisor=user.getSupervisor();
                if (supervisor == null) {
                    throw new UserNotFoundException("Brak supervisora dla "+user);
                }
                out = (out + supervisor.getName());

                if(iterator.hasNext()){

                    out = (out+",");
                }

            }

            if (out.equals("")) {
                throw new UserNotFoundException("Brak realizator�w...");
            }

            return out;

        }catch(EdmException e) {

            throw e;
        }
    }
    public boolean getManagerfromdivcheck(Long docId, String profileIdAdds, int scopeLvl) throws EdmException {
        try{
            OfficeDocument doc = OfficeDocument.find(docId);
            Set<String> userNamesVar = new HashSet<String>();
            List<Recipient> recipients = doc.getRecipients();
            String author= userAssignmentHelper.author(docId);
            for(Recipient recipient : recipients){

                DSDivision div = extractDivisionFromRecipent(recipient);
                if (div != null) {
                    List<String> userNames = new ArrayList<String>();
                    Long profileId = getProfileIdFromAdds(profileIdAdds);
                    ProfileInDivisionAssignmentHandler.putUsersFromProfile(userNames,div.getId(),profileId,false,scopeLvl,false);
                    userNamesVar.addAll(userNames);
                }}

            if (userNamesVar.contains(author) && (userNamesVar.size()==1) ) {
                log.info("Jedynym Kierownikiem jest autor");
               return false; }
            if(userNamesVar.isEmpty()){
                throw new UserNotFoundException("Nie znaleziono kierownika dzia�u");
            }return true;
        }catch (EdmException e){
            throw e;
        }catch (Exception e){
            log.error(e.getMessage(),e);
            throw new EdmException(e);
        }
    }



    //wniosek inwestycyjny: s�uzy do zamykania wszystkich wniosk�w zapotrzebowania wybranych we wniosku inwestycyjnym
    public void close_zapotrzebowanie(Long docId) throws EdmException {

        OfficeDocument doc = OfficeDocument.find(docId);
        FieldsManager fm = doc.getFieldsManager();
        RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
        List<Object> ss = (List<Object>) fm.getFieldValues().get(TO_ZAPOTRZEBOWANIE2);
        if(ss != null && ss.size() > 0) {
            for (Object id : ss) {

                ExecutionQuery query = runtimeService.createExecutionQuery().processVariableValueEquals("docId", id);
                List<Execution> execList = query.list();
                if (execList != null && execList.size() > 0) {
                    Execution e = execList.get(0);
                    runtimeService.deleteProcessInstance(e.getProcessInstanceId(), "Awaryjne zakonczenie procesu");
                }
                PreparedStatement ps = null;
                try{

                    TaskSnapshot.updateByDocument(Document.find(Long.valueOf(String.valueOf(id))));

                }catch(Exception e){
                    log.error("blad odswiezania dla dokumentu o id= "+id);continue;
                }
                try {
                    ps = DSApi.context().prepareStatement("delete from ds_process_for_document where document_id=" + id.toString());
                    ps.setLong(1, docId);
                    ps.execute();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                } finally {
                    DbUtils.closeQuietly(ps);
                }


            }

        }

    }


    public Set<String> getManagerfromdiv(Long docId, String profileIdAdds, int scopeLvl) throws EdmException {
        try{
            OfficeDocument doc = OfficeDocument.find(docId);
            Set<String> userNamesVar = new HashSet<String>();
            List<Recipient> recipients = doc.getRecipients();
            String author= userAssignmentHelper.author(docId);


            for(Recipient recipient : recipients){

                DSDivision div = extractDivisionFromRecipent(recipient);
                if (div != null) {
                    List<String> userNames = new ArrayList<String>();
                    Long profileId = getProfileIdFromAdds(profileIdAdds);
                    ProfileInDivisionAssignmentHandler.putUsersFromProfile(userNames,div.getId(),profileId,false,scopeLvl,false);
                    if (userNames.contains(author)) {
                        int index = userNames.indexOf(author);
                        userNames.remove(index);
                    }
                    userNamesVar.addAll(userNames);
                }

            }
            if(userNamesVar.isEmpty()){
                throw new UserNotFoundException("Nie znaleziono kierownika dzia�u");
            }

            return userNamesVar;
        }catch (EdmException e){
            throw e;
        }catch (Exception e){
            log.error(e.getMessage(),e);
            throw new EdmException(e);
        }
    }

    public Set<String> getChanselorfromdiv(Long docId, String profileIdAdds, int scopeLvl) throws EdmException {
        try{
            OfficeDocument doc = OfficeDocument.find(docId);

            Set<String> userNamesVar = new HashSet<String>();
            List<Recipient> recipients = doc.getRecipients();

            for(Recipient recipient : recipients){

                DSDivision div = extractDivisionFromRecipent(recipient);
                DSDivision parent = div.getParent();

                // root = DSDivision.findByName("rootdiv")koncepcja wyszukania
                //   .getChildren()[0].isInAnyChildDivision(div);

                if(parent!=null){
                    userNamesVar.addAll(findChilds(parent,profileIdAdds));

                }
                List<String> userNames = new ArrayList<String>();
                Long profileId = getProfileIdFromAdds(profileIdAdds);
                ProfileInDivisionAssignmentHandler.putUsersFromProfile(userNames, parent.getId(), profileId, false, scopeLvl, true);
                userNamesVar.addAll(userNames);
            }
            if(userNamesVar.isEmpty()){
                throw new UserNotFoundException("Nie znaleziono "+ profileIdAdds+" w strukturze dzia�u");
            }

            return userNamesVar;
        }catch (EdmException e){
            throw e;
        }catch (Exception e){
            log.error(e.getMessage(),e);
            throw new EdmException(e);
        }
    }

    public Set<String> findChilds(DSDivision division,String profileIdAdds)  throws EdmException{
        Set<String> userNamesVar = new HashSet<String>();
        try {
            DSDivision childs[] = division.getChildren("division");
            Long profileId = getProfileIdFromAdds(profileIdAdds);
            List<String> userNames = new ArrayList<String>();

            if (childs != null) {
                {
                    for (DSDivision ch : childs) {
                        userNamesVar.addAll(findChilds(ch, profileIdAdds));
                        userNames.clear();
                    }
                }
            }
            ProfileInDivisionAssignmentHandler.putUsersFromProfile(userNames, division.getId(), profileId, false, 0, true);
            userNamesVar.addAll(userNames);
            return userNamesVar;
        }catch (EdmException e){
            throw e;
        }catch (Exception e){
            log.error(e.getMessage(),e);
            throw new EdmException(e);
        }
    }




    //  public static List<Long> extractIdFromDictonaryField(Map<String,FieldData> values, String dicName) {
    //     return extractIdFromDictonaryField(values.containsKey(DWR_PREFIX+dicName)?values.get(DWR_PREFIX+dicName):values.get(dicName), dicName);

    /*  DwrUtils.extractFieldsFromDictionaryField(   DWR_PREFIX + ZRODLO_ID);



  // znajywanie element�w bud�etu
   */public Integer getbudget(Long docId) throws EdmException {
        try {
            int wynik = 0;
            Long first = 1l;
            Long second = 2l;
            ZapotrzebowanieEntry entry;
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            Set<Long> Budget = new HashSet<Long>();

            List<Long> ss = (List<Long>) fm.getFieldValues().get(PRZEDMIOT_ZAPOTRZEBOWANIA);

            if (ss != null && ss.size() > 0) {
                for (Long id : ss) {

                    entry = ZapotrzebowanieEntry.findById(id);
                    ZrodloFinansowaniaDict raz = entry.getZrodloFinansowania();
                    Long dwa= raz.getBudgetKind();

                    Budget.add(dwa);


                    if (entry == null) {
                        throw new EdmException("B��d inicjalizacji Popup: b��dne Id S�ownika;");
                    }
                }


                if (Budget.size()==2) {
                    wynik= 1;
                } else if ((Budget.size()==1 && Budget.contains(first))) {
                   wynik= 2;
                } else if ((Budget.size()==1 && Budget.contains(second))) {
                    wynik= 3;
                }}
           else{ throw new UserNotFoundException("Nie wype�niono pola Rodzaj bud�etu w s�owniku");

            }
       return wynik;
        }catch (EdmException e){
            throw e;
        }catch (Exception e){
            log.error(e.getMessage(),e);
            throw new EdmException(e);


        }

    }


    //Wniosek inwestycyjny : sprwadza czy wnisek zosta� rozpoczety on poczatku , czy utworzony w zapotrzebowaniu

    public boolean getzapotrzebowanie(Long docId) throws EdmException {
        boolean wasOpened = false;
        try { wasOpened = DSApi.openContextIfNeeded();

                OfficeDocument doc = OfficeDocument.find(docId);
                Set<String> userNamesVar = new HashSet<String>();
                List<Recipient> recipients = doc.getRecipients();
                String author= userAssignmentHelper.author(docId);
                for(Recipient recipient : recipients){
                    DSUser user = extractUserFromRecipent(recipient);
                    String users = user.getName();

                        userNamesVar.add(users);
                    }
            if(userNamesVar.isEmpty()){
                    log.info("Nie ma recipent�w formularz powsta�m,wiec w zapotrzebowaniu");
                    return true;
                }return false;
            }catch (EdmException e){
                throw e;
            }catch (Exception e){
                log.error(e.getMessage(),e);
                throw new EdmException(e);
             }finally {
            DSApi.closeContextIfNeeded(wasOpened);
        }}
    // boolean
    public boolean getBoolFieldValue(Long docId) throws EdmException {
        try{
            Boolean bool=false;

            Document doc = Document.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            BigDecimal val= (BigDecimal) fm.getKey(WARTOSC_NETTO_TOTAL);
            BigDecimal count= BigDecimal.valueOf(3500);
            int res = val.compareTo(count);

            if( fm.getEnumItem(TRYB_ZAMOWIENIA)!= null){
               if(fm.getEnumItem(TRYB_ZAMOWIENIA).getId()== 110){
                   bool= true;}}

            else if (fm.getEnumItem(TO_WINWEST)!= null) {
                bool= true;
            }
            else if(res==-1){
                bool= true;
            }

            else{
                bool= false;
            }
        return bool;
        }catch (EdmException e){
            throw e;
        }catch (Exception e){
            log.error(e.getMessage(),e);
            throw new EdmException(e);
        }
    }

    public  Set<String> getProjectManager(Long docId, String kierownik) throws EdmException {

        Set<String> userNamesVar = new HashSet<String>();
        Document doc = Document.find(docId);

        List<String> userNames = new ArrayList<String>();
        FieldsManager fm = doc.getFieldsManager();
        ZapotrzebowanieEntry entry;

        try{
            List<Long> ss = (List<Long>) fm.getFieldValues().get(PRZEDMIOT_ZAPOTRZEBOWANIA);
            List a=new ArrayList<String>();
            List<RolesInProjects> rolesInProjects = null;
            if (ss != null && ss.size() > 0) {
                for (Long id : ss) {
                    entry = ZapotrzebowanieEntry.findById(id);
                    ZrodloFinansowaniaDict zrodlo = entry.getZrodloFinansowania();
                    Double contract = zrodlo.getContract().doubleValue();
                    rolesInProjects = pl.compan.docusafe.dictionary.erp.simple.Dictionary.find("kontraktid", contract, RolesInProjects.class);
                }
            }
            for (RolesInProjects role : rolesInProjects) {
                String rola = role.getBprolaidn();
                String guid = role.getOsobaguid();
                DSUser user = DSUser.findByIdentifier(guid);
                String Name = user.getName();

                if(rola.equals(kierownik)){
                    userNamesVar.add(Name);
                }

            }


           return userNamesVar;
        }catch (EdmException e){
            throw e;
        }catch (Exception e){
            log.error(e.getMessage(),e);
            throw new EdmException(e);
        }
    }
    public  Set<String> getProjectandDivManager(Long docId, String kierownik,String profileIdAdds,int scopeLvl) throws Exception {
        try{
        Set<String> userNamesVar = new HashSet<String>();
        Document doc = Document.find(docId);
        List<String> userNames = new ArrayList<String>();
        FieldsManager fm = doc.getFieldsManager();
        ZapotrzebowanieEntry entry;

        Long div= Long.valueOf(fm.getEnumItem(Z_DIVISION).getId());

        if (div != null) {
            Long profileId = getProfileIdFromAdds(profileIdAdds);
            ProfileInDivisionAssignmentHandler.putUsersFromProfile(userNames,div,profileId,false,scopeLvl,false);
            userNamesVar.addAll(userNames);
        }



            List<Long> ss = (List<Long>) fm.getFieldValues().get(PRZEDMIOT_ZAPOTRZEBOWANIA);
            List<RolesInProjects> rolesInProjects = null;
            if (ss != null && ss.size() > 0) {
                for (Long id : ss) {
                    entry = ZapotrzebowanieEntry.findById(id);
                    ZrodloFinansowaniaDict zrodlo = entry.getZrodloFinansowania();
                    Double contract = zrodlo.getContract().doubleValue();
                    rolesInProjects = pl.compan.docusafe.dictionary.erp.simple.Dictionary.find("kontraktid", contract, RolesInProjects.class);


                }}
            for (RolesInProjects role : rolesInProjects) {

                String rola = role.getBprolaidn();
                String guid = role.getOsobaguid();


                DSUser user = DSUser.findByIdentifier(guid);
                String Name = user.getName();

                if(rola.equals(kierownik)){
                    userNamesVar.add(Name);
                } }
            return userNamesVar;
        }catch (EdmException e){
            throw e;
        }catch (Exception e){
            log.error(e.getMessage(),e);
            throw new EdmException(e);
        }
    }

    public boolean getBoolMoneyValue(Long docId) throws EdmException {

   boolean bool = false;
        try{ Document doc = Document.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            if(fm.getKey(WARTOSC_NETTO_TOTAL)!=null){
            BigDecimal val= (BigDecimal) fm.getKey(WARTOSC_NETTO_TOTAL);
            BigDecimal count= BigDecimal.valueOf(3500);
            int res = val.compareTo(count);
            if (res==0 || res ==1) {
                bool= true;
            }else{
                bool= false;}
            }

            return bool;
        }catch (EdmException e){
            throw e;

        }catch (Exception e){
            log.error(e.getMessage(),e);
            throw new EdmException(e);
        }

    }


    // zwraca kierownika z pola dzilu na formularzu

    public Set<String> DivisionManager(Long docId, String profileIdAdds, int scopeLvl) throws EdmException {
        try{


            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();

            Set<String> userNamesVar = new HashSet<String>();

            Long div= Long.valueOf(fm.getEnumItem(Z_DIVISION).getId());

            if (div != null) {
                List<String> userNames = new ArrayList<String>();
                Long profileId = getProfileIdFromAdds(profileIdAdds);
                ProfileInDivisionAssignmentHandler.putUsersFromProfile(userNames,div,profileId,false,scopeLvl,false);

                userNamesVar.addAll(userNames);
            }


            if(userNamesVar.isEmpty()){
                throw new UserNotFoundException("Nie znaleziono kierownika dzia�u");
            }

            return userNamesVar;
        }catch (EdmException e){
            throw e;
        }catch (Exception e){
            log.error(e.getMessage(),e);
            throw new EdmException(e);
        }
    }

    /**
     * Returns profile id from adds.properties from add with given name
     * @param profileIdAdds
     * @return profile id
     * @throws pl.compan.docusafe.core.PropertyNotFoundException if there is no adds with given name
     */
    public static Long getProfileIdFromAdds(String profileIdAdds) throws PropertyNotFoundException {
        Long profileId;
        if (StringUtils.isNotBlank(profileIdAdds)){
            profileId = (long) Docusafe.getIntAdditionPropertyOrDefault(profileIdAdds, -1);
            if (profileId < 0){
                throw new PropertyNotFoundException("No adds=" + profileIdAdds);
            }
        } else{
            throw new PropertyNotFoundException("No profile id source");
        }

        return profileId;
    }


    /**
     * Extracts DSDivision from given recipent
     * @param recipient
     * @return extracted DSDivision or <Code>null</Code> if no division can be extracted
     * @throws EdmException
     * @throws NullPointerException if recipent is null
     */
    private DSDivision extractDivisionFromRecipent(Recipient recipient) throws EdmException {
        DSDivision div = null;
        String guid = extractGuidFromRecipent(recipient);
        if (guid != null) {
            div = DSDivision.find(guid);
        }
        if(div == null){
            String divName = recipient.getOrganizationDivision();
            div = DSDivision.findByName(divName);
        }
        return div;
    }

    /**
     * Extracts DSDivision guid from given recipent
     * @param recipient
     * @return extracted DSDivision guid or <Code>null</Code> if no guid can be extracted
     * @throws NullPointerException if recipent is null
     */
    private String extractGuidFromRecipent(Recipient recipient) {
        String guid = null;
        Pattern pat = Pattern.compile( ".*;d:(\\w*)");
        Matcher m = pat.matcher(recipient.getLparam());
        if(m.find()){
            guid = m.group(1);
        }
        return guid;
    }

    public List<String> getApplicantAndDeputy(Long docId, boolean isAdministrative) throws EdmException{

       ArrayList<String> out = new ArrayList<String>();

        try{
           out.add(userAssignmentHelper.author(docId));

            if (isAdministrative) {
                for(String r: activitiDockindHelper.getRecipients(docId)){
                    out.add(r);
                }
            }

           }catch(EdmException e){
            throw new UserNotFoundException("No recipients...");
            }
        if(out.isEmpty()){
        throw new UserNotFoundException("No recipients...");
        }

        return out;
    }

    public boolean isAdministrativeApplicant(Long docId) throws EdmException{

        return false;
    }


    /**
     * Extracts DSUser from recipent
     * @param recipient
     * @return extracted DSUser or <Code>null</Code> if no DSUser can be extracted
     * @throws EdmException
     * @throws NullPointerException - if recipent is null
     */
    private DSUser extractUserFromRecipent(Recipient recipient) throws EdmException {
        if(recipient == null){
            throw new NullPointerException();
        }
        String firstName = recipient.getFirstname();
        String lastName = recipient.getLastname();

        DSUser user = DSUser.findByFirstnameLastname(firstName, lastName);
        String guid = extractGuidFromRecipent(recipient);
        if (user != null && guid != null && DSDivision.isInDivision(guid,user.getName())) {
            return user;
        }else if(guid != null && firstName != null && lastName != null){
            user = null;
            DSDivision div = DSDivision.find(guid);
            DSUser[] users = div.getUsers();
            for(DSUser u : users){
                if(firstName.equals(u.getFirstname()) && lastName.equals(u.getLastname())){
                    user = u;
                    break;
                }
            }
        }

        return user;
    }


    public static Collection<String> getUsersInAuthorDivision(Long docId, String profileIdAdds, Integer scopeLvl) throws EdmException {
        OfficeDocument doc = OfficeDocument.find(docId);
        String authorName = doc.getAuthor();
        DSUser author = DSUser.findByUsername(authorName);
        return getUsersInUserDivision(author, profileIdAdds, scopeLvl);
    }
    public static Collection<String> getUsersInUserDivision(DSUser user, String profileIdAdds, Integer scopeLvl) throws EdmException {
        DSDivision [] divisions = user.getDivisions();
        return getUsersInDivisions(divisions, profileIdAdds, scopeLvl);
    }
    public static Collection<String> getUsersInDivisions(DSDivision [] divisions, String profileIdAdds, Integer scopeLvl) throws EdmException {
        Long profileId = getProfileIdFromAdds(profileIdAdds);
        return getUsersInDivisions(divisions, profileId, scopeLvl);
    }
    public static Collection<String> getUsersInDivisions(DSDivision [] divisions, Long profileId, Integer scopeLvl) throws EdmException {
        try{
            Set<String> userNamesVar = new HashSet<String>();
            for (DSDivision div : divisions){
                List<String> userNames = new ArrayList<String>();
                ProfileInDivisionAssignmentHandler.putUsersFromProfile(userNames,div.getId(),profileId,false,scopeLvl,false);
                userNamesVar.addAll(userNames);
            }
            if(userNamesVar.isEmpty()){
                DSUser user = DSUser.getLoggedInUser();
                if (user.getName().equals("admin"))
                    userNamesVar.add("admin");
                else
                    throw new UserNotFoundException("Nie znaleziono �adnego u�ytkownika w dzia�ach");
            }
            return userNamesVar;
        } catch (EdmException e){
            throw e;
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            throw new EdmException(e);
        }
    }
    
   private String putEnumItemCnVariable(Long docId, String enumFieldName) throws EdmException {
	   String fieldCn = "";
	   try {
		   OfficeDocument doc = OfficeDocument.find(docId);
           FieldsManager fm = doc.getFieldsManager();
           fieldCn = fm.getEnumItemCn(enumFieldName);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new EdmException(SM.getString("bladPodczasPobieraniaWartosciPola", enumFieldName));
		}
	   
	   if (!StringUtils.isNotBlank(fieldCn)) {
		   throw new EdmException(SM.getString("nieUdaloSieUstalicCn", enumFieldName));
	   } else {
		   return fieldCn;
	   }
   }
}
