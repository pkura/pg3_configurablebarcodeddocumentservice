package pl.compan.docusafe.parametrization.uek.utils;


        import org.jfree.util.Log;
        import pl.compan.docusafe.AdditionManager;
        import pl.compan.docusafe.core.AvailabilityManager;
        import pl.compan.docusafe.core.DSApi;
        import pl.compan.docusafe.core.EdmException;
        import pl.compan.docusafe.core.dockinds.FieldsManager;
        import pl.compan.docusafe.core.dockinds.dwr.DwrMappingUtils;
        import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
        import pl.compan.docusafe.core.dockinds.dwr.Field;
        import pl.compan.docusafe.core.dockinds.dwr.FieldData;
        import pl.compan.docusafe.core.users.DSDivision;
        import pl.compan.docusafe.general.canonical.model.StawkaVat;
        import pl.compan.docusafe.parametrization.uek.hbm.*;

        import java.util.*;

/**
 * Created by Raf on 2014-05-07.
 */
public class ZapotrzebowaniePopupManager {

    public final static String ENTRY_DICTIONARY = "SLOWNIK_PZ";
    private final static String ENTRY_ID_FIELD = "ENTRY_ID";
    private final static String DWR_PREFIX = DwrUtils.DWR_PREFIX;

    public static void clearPopup(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        Set<String> popupFields = new HashSet<String>();
        popupFields.addAll(ZapotrzebowanieEntry.getMapToDocFields().values());
        popupFields.addAll(ZrodloFinansowaniaDict.getMapToDocFields().values());
        popupFields.add(ENTRY_ID_FIELD);
        DwrUtils.resetFields(values, fm, popupFields);
    }

    public static ZapotrzebowanieEntry deleteEntry(Map<String, FieldData> values, FieldsManager fm, Integer rowNo) throws EdmException {
        Long entryId = Long.valueOf(DwrUtils.getValueFromDictionary(values, ENTRY_DICTIONARY, "ID_" + rowNo).toString());
        if(entryId != null){
            boolean wasOpened = false;
            try{
                wasOpened = DSApi.openContextIfNeeded();

                ZapotrzebowanieEntry entry = ZapotrzebowanieEntry.findById(entryId);
                Collection<Long> ss = (Collection<Long>) fm.getFieldValues().get( ENTRY_DICTIONARY);

                entry.delete();
                return entry;
            }finally {
                DSApi.closeContextIfNeeded(wasOpened);
            }
        }else{
            throw new EdmException("B��d przy pr�bie usuni�cia wpisu");
        }

    }

    public static void deleteEntry2(Map<String, FieldData> values, FieldsManager fm, Integer rowNo) throws EdmException {
        Long entryId = Long.valueOf(DwrUtils.getValueFromDictionary(values, ENTRY_DICTIONARY, "ID_" + rowNo).toString());
        if(entryId != null){
            boolean wasOpened = false;
            try{
                wasOpened = DSApi.openContextIfNeeded();

                ZapotrzebowanieEntry entry = ZapotrzebowanieEntry.findById(entryId);
                entry.delete();

            }finally {
                DSApi.closeContextIfNeeded(wasOpened);
            }
        }else{
            throw new EdmException("B��d przy pr�bie usuni�cia wpisu");
        }
        DwrUtils.updateMultipleDictionary(values,ENTRY_DICTIONARY,null,entryId);
    }








    public static void updatePopup(Map<String, FieldData> values, FieldsManager fm, Integer rowNo) throws EdmException {
        Long entryId = Long.valueOf(DwrUtils.getValueFromDictionary(values, ENTRY_DICTIONARY, "ID_" + rowNo).toString());

        if(entryId != null){
            boolean wasOpened = false;
            try{
                wasOpened = DSApi.openContextIfNeeded();
                ZapotrzebowanieEntry entry = ZapotrzebowanieEntry.findById(entryId);
                if(entry == null){
                    throw new EdmException("B��d inicjalizacji Popup: b��dne Id wpisu;");
                }

                DwrMappingUtils.setFieldsFromEntity(values, fm, entry.getMapToDocFields(), entry);

                if(entry.getZrodloFinansowania() != null){
                    DwrMappingUtils.setFieldsFromEntity(values,fm,ZrodloFinansowaniaDict.getMapToDocFields(),entry.getZrodloFinansowania());
                }

            }finally {
                DSApi.closeContextIfNeeded(wasOpened);
            }

            values.get(DWR_PREFIX + ENTRY_ID_FIELD).setStringData(entryId.toString());
        }
    }




    public static ZapotrzebowanieEntry saveEntry2(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        StringBuilder description = new StringBuilder();

        ZapotrzebowanieEntry entry = DwrMappingUtils.getEntityFromDwr(values,fm,ZapotrzebowanieEntry.getMapToDocFields(),ZapotrzebowanieEntry.class,null).get(0);
        Long id = extractId(values,ENTRY_ID_FIELD);
        entry.setId(id);
        description.append(DwrMappingUtils.prepareDescription(ZapotrzebowanieEntry.getMapToDocFields(), entry, fm, null));

        ZrodloFinansowaniaDict zrodlo = DwrMappingUtils.getEntityFromDwr(values,fm,ZrodloFinansowaniaDict.getMapToDocFields(),ZrodloFinansowaniaDict.class,null).get(0);
        entry.setZrodloFinansowania(zrodlo);
        description.append(DwrMappingUtils.prepareDescription(ZrodloFinansowaniaDict.getMapToDocFields(), zrodlo, fm, null));

        entry.setDescription(description.toString());
        entry.save();


        return entry;



    }
    public static Collection<Long> updateZapatrzebowanieDictionary(Map<String, FieldData> values, Long idToAdd, Long idToRemove) {
        Set<Long> zapotrzebowanieValues = new HashSet<Long>();

        Map<String, FieldData> zrodlaFields = DwrUtils.getDictionaryFields(values, ENTRY_DICTIONARY, "ID");
        for (FieldData value : zrodlaFields.values()) {
            zapotrzebowanieValues.add(value.getLongData());
        }
        if(idToAdd != null){
            zapotrzebowanieValues.add(idToAdd);
        }
        if(idToRemove != null){
            zapotrzebowanieValues.remove(idToRemove);
        }

        FieldData fd = new FieldData(Field.Type.DICTIONARY, zapotrzebowanieValues);
        values.put(DWR_PREFIX+ENTRY_DICTIONARY, fd);
        return zapotrzebowanieValues;
    }

   /*public static void saveEntry(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        StringBuilder description = new StringBuilder();

        ZapotrzebowanieEntry entry = DwrMappingUtils.getEntityFromDwr(values,fm,ZapotrzebowanieEntry.getMapToDocFields(),ZapotrzebowanieEntry.class,null).get(0);
        Long id = extractId(values,ENTRY_ID_FIELD);
        entry.setId(id);
        description.append(DwrMappingUtils.prepareDescription(ZapotrzebowanieEntry.getMapToDocFields(), entry, fm, null));

        ZrodloFinansowaniaDict zrodlo = DwrMappingUtils.getEntityFromDwr(values,fm,ZrodloFinansowaniaDict.getMapToDocFields(),ZrodloFinansowaniaDict.class,null).get(0);
        entry.setZrodloFinansowania(zrodlo);
        description.append(DwrMappingUtils.prepareDescription(ZrodloFinansowaniaDict.getMapToDocFields(), zrodlo, fm, null));

        entry.setDescription(description.toString());
        entry.save();

        DwrUtils.updateMultipleDictionary(values,ENTRY_DICTIONARY,entry.getId(),null);}*/









   /* public static ZrodloFinansowaniaDict2 saveZrodloFromDWR(Map<String, FieldData> values, FieldsManager fm) throws EdmException {

        StringBuilder description = new StringBuilder();
        boolean wasOpened = false;

        try{
            wasOpened = DSApi.openContextIfNeeded();


            ZrodloFinansowaniaDict2 zrodlo = extractZrodlo(values, fm);

            fillZrodloFromDWR(fm,values,fm.getIntegerKey(BUDGET_KIND),zrodlo,description);



            zrodlo.setDescription(description.toString());

            zrodlo.save();
            return zrodlo;
            //prepareBIGZrodloFinansowaniaDictionary(values, zrodlo, isNewEntry, 60);

        }catch(EdmException e){
            Log.error(e.getMessage(), e);
            throw e;
        }finally {
            DSApi.closeContextIfNeeded(wasOpened);
        }
    }
*/

    private static Long extractId(Map<String, FieldData> values, String idFieldName) throws EdmException {
        if(!idFieldName.startsWith(DWR_PREFIX)){
            idFieldName = DWR_PREFIX + idFieldName;
        }
        String id = values.get(idFieldName).getStringData();
        if(id.matches("\\d+")){
            return Long.parseLong(id);
        }else{
            return null;
        }
    }
}
