package pl.compan.docusafe.parametrization.uek.utils;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrMappingUtils;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.general.canonical.model.StawkaVat;
import pl.compan.docusafe.parametrization.uek.hbm.*;

import java.util.*;

/**
 * Created by Damian on 30.04.14.
 */
public class InvoicePopupManager {
    public final static String ENTRY_DICTIONARY = "POZYCJA_FAKTURY";
    public final static String VAT_RATE_DIC = "STAWKI_VAT";
    public final static String ENTRY_ID_FIELD = "ENTRY_ID";
    private static final String WNIOSKUJACY_DICT = "WNIOSKUJACY";
    private final static String DWR_PREFIX = DwrUtils.DWR_PREFIX;

    public static void clearPopup(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        Set<String> popupFields = new HashSet<String>();
        popupFields.addAll(InvoiceEntry.getMapToDocFields().values());
        popupFields.addAll(ZrodloFinansowaniaDict.getMapToDocFields().values());
        popupFields.addAll(RepositoryDictEntry.getMapToDocFields().values());
        popupFields.add(ENTRY_ID_FIELD);
        popupFields.add(VAT_RATE_DIC);
        popupFields.add(WNIOSKUJACY_DICT);

        DwrUtils.resetFields(values, fm, popupFields);
    }

    public static void deleteEntry(Map<String, FieldData> values, FieldsManager fm, Integer rowNo) throws EdmException {
        Long entryId = Long.valueOf(DwrUtils.getValueFromDictionary(values, ENTRY_DICTIONARY, "ID_" + rowNo).toString());
        if(entryId != null){
            boolean wasOpened = false;
            try{
                wasOpened = DSApi.openContextIfNeeded();

                InvoiceEntry entry = InvoiceEntry.findById(entryId);
                entry.delete();
            }finally {
                DSApi.closeContextIfNeeded(wasOpened);
            }
        }else{
            throw new EdmException("B��d przy pr�bie usuni�cia wpisu");
        }
        DwrUtils.updateMultipleDictionary(values,ENTRY_DICTIONARY,null,entryId);
    }

    public static void updatePopup(Map<String, FieldData> values, FieldsManager fm, Integer rowNo) throws EdmException {
        Long entryId = extractIdFromDic(values, fm, rowNo);

        if(entryId != null){
            boolean wasOpened = false;
            try{
                wasOpened = DSApi.openContextIfNeeded();
                InvoiceEntry entry = InvoiceEntry.findById(entryId);
                if(entry == null){
                    throw new EdmException("B��d inicjalizacji Popup: b��dne Id wpisu;");
                }

                DwrMappingUtils.setFieldsFromEntity(values,fm,entry.getMapToDocFields(),entry);

                if(entry.getZrodloFinansowania() != null){
                    DwrMappingUtils.setFieldsFromEntity(values,fm,ZrodloFinansowaniaDict.getMapToDocFields(),entry.getZrodloFinansowania());
                }
                if(entry.getRepositoryDictEntry() != null){
                    DwrMappingUtils.setFieldsFromEntity(values,fm,RepositoryDictEntry.getMapToDocFields(),entry.getRepositoryDictEntry());
                }
                if(entry.getStawkaVatDicts() != null){
                    DwrMappingUtils.setDictionaryFromEntity(values,fm,"id",entry.getStawkaVatDicts(),VAT_RATE_DIC);
                }
                if(entry.getWniokujacy() != null){
                    DwrMappingUtils.setDictionaryFromEntity(values,fm,"id",entry.getWniokujacy(),WNIOSKUJACY_DICT);
                }
            }finally {
                DSApi.closeContextIfNeeded(wasOpened);
            }

            values.get(DWR_PREFIX + ENTRY_ID_FIELD).setStringData(entryId.toString());
        }else{
            clearPopup(values, fm);
        }

    }




    public static void saveEntry(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        StringBuilder description = new StringBuilder();

        InvoiceEntry entry = DwrMappingUtils.getEntityFromDwr(values,fm,InvoiceEntry.getMapToDocFields(),InvoiceEntry.class,null).get(0);
        Long id = extractId(values,ENTRY_ID_FIELD);
        entry.setId(id);
        description.append(DwrMappingUtils.prepareDescription(InvoiceEntry.getMapToDocFields(), entry, fm, null));

        RepositoryDictEntry repositoryDict = DwrMappingUtils.getEntityFromDwr(values,fm,RepositoryDictEntry.getMapToDocFields(),RepositoryDictEntry.class,null).get(0);
        entry.setRepositoryDictEntry(repositoryDict);
        description.append(DwrMappingUtils.prepareDescription(RepositoryDictEntry.getMapToDocFields(), repositoryDict, fm, null));

        ZrodloFinansowaniaDict zrodlo = DwrMappingUtils.getEntityFromDwr(values,fm,ZrodloFinansowaniaDict.getMapToDocFields(),ZrodloFinansowaniaDict.class,null).get(0);
        entry.setZrodloFinansowania(zrodlo);
        description.append(DwrMappingUtils.prepareDescription(ZrodloFinansowaniaDict.getMapToDocFields(), zrodlo, fm, null));

        Collection<StawkaVatDict> vatRatesDict = DwrMappingUtils.getEntityFromDwr(values,fm,StawkaVatDict.getMapToDocFields(),StawkaVatDict.class,VAT_RATE_DIC);
        vatRatesDict = new HashSet<StawkaVatDict>(vatRatesDict);
        entry.setStawkaVatDicts((Set<StawkaVatDict>)vatRatesDict);

        for(StawkaVatDict vatRate : vatRatesDict){
            description.append("\n").append(DwrMappingUtils.prepareDescription(StawkaVatDict.getMapToDocFields(), vatRate, fm, VAT_RATE_DIC, "\t"));
        }

        Collection<WnioskujacyDict> wnioskujacyDict = DwrMappingUtils.getEntityFromDwr(values,fm, WnioskujacyDict.getMapToDocFields(),WnioskujacyDict.class,WNIOSKUJACY_DICT);
        wnioskujacyDict = new HashSet<WnioskujacyDict>(wnioskujacyDict);
        entry.setWniokujacy((Set<WnioskujacyDict>) wnioskujacyDict);

        for(WnioskujacyDict wnioskujacy : wnioskujacyDict){
            description.append("\n").append(DwrMappingUtils.prepareDescription(WnioskujacyDict.getMapToDocFields(), wnioskujacy, fm, WNIOSKUJACY_DICT, "\t"));
        }

        entry.setDescription(description.toString());
        entry.save();

        DwrUtils.updateMultipleDictionary(values,ENTRY_DICTIONARY,entry.getId(),null);

    }



    private static Long extractId(Map<String, FieldData> values, String idFieldName) throws EdmException {
        if(!idFieldName.startsWith(DWR_PREFIX)){
            idFieldName = DWR_PREFIX + idFieldName;
        }
        String id = values.get(idFieldName).getStringData();
        if(id.matches("\\d+")){
            return Long.parseLong(id);
        }else{
            return null;
        }
    }

    private static Long extractIdFromDic(Map<String, FieldData> values, FieldsManager fm, Integer rowNo) throws EdmException {
        Long entryId = null;
        Object idValue = DwrUtils.getValueFromDictionary(values, ENTRY_DICTIONARY, "ID_" + rowNo);
        if(idValue != null && idValue.toString().matches("\\d+")){
            entryId = Long.valueOf(idValue.toString());
        }
        return entryId;
    }

}
