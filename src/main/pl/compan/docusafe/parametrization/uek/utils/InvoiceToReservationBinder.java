package pl.compan.docusafe.parametrization.uek.utils;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.parametrization.uek.hbm.ErpPositionEntity;
import pl.compan.docusafe.parametrization.uek.hbm.InvoiceEntry;
import pl.compan.docusafe.parametrization.uek.hbm.StawkaVatDict;
import pl.compan.docusafe.parametrization.uek.hbm.ZrodloFinansowaniaDict;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.annotation.Nullable;
import java.util.*;

/**
 * Created by Damian on 10.05.14.
 */
public class InvoiceToReservationBinder {
    private final static Logger log = LoggerFactory.getLogger(InvoiceToReservationBinder.class);
    public final static String DEFAULT_RES_DIC_CN = "WNIOSEK_O_REZERWACJE";
    public final static String DEFAULT_ENTRY_DIC_CN = InvoicePopupManager.ENTRY_DICTIONARY;

    private final String resDictionaryCn;
    private final String entryDicCn;

    private FieldsManager invFm;
    private FieldsManager reservationFm;
    private DocType resDocType;
    private StringBuilder errMsg = new StringBuilder();
    private List<Long> entriesIds = Lists.newArrayList();
    private List<Long> createdEntries = Lists.newArrayList();


    public InvoiceToReservationBinder(Document invoiceDocument) {
        this(DEFAULT_RES_DIC_CN, DEFAULT_ENTRY_DIC_CN,invoiceDocument);
    }

    public InvoiceToReservationBinder(String resDictionaryCn, String entryDicCn, Document document) {
        this.resDictionaryCn = resDictionaryCn;
        this.entryDicCn = entryDicCn;
        this.invFm = document.getFieldsManager();
    }

    public boolean setup(){
        boolean wasOpened = false;
        try {
            wasOpened = DSApi.openContextIfNeeded();

            Number reservationDocumentIds = (Number) invFm.getKey(resDictionaryCn);
            checkState(reservationDocumentIds != null, "Nie pod��czono wniosku o rezerwacj�");

            reservationFm = Document.find(reservationDocumentIds.longValue()).getFieldsManager();

            String resDocKindCn = reservationFm.getDocumentKind().getCn();
            Integer resDocTypeId = getReservationType();
            resDocType = resolveDocType(resDocTypeId,resDocKindCn);

            checkState(resDocType != null, "Ustalenie typu dokumentu do powi�zania nie powiod�o si�");

            if(invFm.getKey(entryDicCn) != null){
                checkState(invFm.getKey(entryDicCn) instanceof List,"S�ownik pozycji nie jest s�ownikiem wielowarto�ciowym");
                entriesIds = (List<Long>) invFm.getKey(entryDicCn);
            }


            return true;
        }catch(EdmException e){
            errMsg.append(e.getMessage());
            return false;
        }finally {
            DSApi.closeContextIfNeeded(wasOpened);
        }


    }


    private Integer getReservationType() throws EdmException {

        if(invFm.getValue(resDictionaryCn) instanceof Map){
            Object value = ((Map) invFm.getValue(resDictionaryCn)).get(resDictionaryCn + "_TYPE");
            if(value instanceof EnumValues){
                String selected = (String) ((EnumValues) value).getSelectedId();
                if(StringUtils.isNotBlank(selected) && StringUtils.isNumeric(selected)){
                    return Integer.parseInt(selected);
                }
            }
        }
        return null;
    }


    public void updateDictionary() throws EdmException {
        checkState(errMsg.length() > 0 , errMsg);
        boolean wasOpened = false;
        try {
            wasOpened = DSApi.openContextIfNeeded();
            Document document = Document.find(invFm.getDocumentId());
            Map<String, Object> toSet = Maps.newHashMap();
            toSet.put(entryDicCn, getAllEntriesIds());
            document.getDocumentKind().setOnly(document.getId(), toSet);
        } catch (Exception e) {
            errMsg.append(e.getMessage()).append("; ");
            log.error(e.getMessage(), e);
        } finally {
            DSApi.closeContextIfNeeded(wasOpened);
        }
    }

    public List<Long> createEntriesFromReservation() throws EdmException {
        checkState(errMsg.length() == 0 , errMsg);
        log.info("Generuje pozycje z wniosku o rezerwacje w fakturze zakupowej");

        boolean wasOpened = false;
        try {
            wasOpened = DSApi.openContextIfNeeded();

            List<Long> reservationPositions = (List<Long>) reservationFm.getKey(resDictionaryCn);
            checkState(reservationPositions != null && !reservationPositions.isEmpty(),"B��d przy pobieraniu pozycji z dokumentu");

            String erpDocumentIdm = reservationFm.getStringKey(resDocType.erpIdmFieldCn);

            checkState(!Strings.isNullOrEmpty(erpDocumentIdm), "Brak IDM wniosku rezerwacji");

            for (Long reservationId : reservationPositions) {
                ErpPositionEntity resPosition = findPosition(reservationId);

                checkState(resPosition != null, "B��d przy pobieraniu pozycji z dokumentu " + resDocType.name());

                Integer pozycjaId = resPosition.getPositionNumber();

                checkState(pozycjaId != null, "Jedna z pozycji wniosku nie posiada numeru porz�dkowego");

                Integer erpPositionId = getErpPositionId(erpDocumentIdm, pozycjaId);

                InvoiceEntry entry = filledEntryFromSource(erpDocumentIdm, resPosition.getZrodloFinansowania(), erpPositionId);

                extendedCopyFromPosition(resPosition,entry);

                entry.save();

                createdEntries.add(entry.getId());
            }

        } catch (Exception e) {
            errMsg.append(e.getMessage()).append("; ");
            log.error(e.getMessage(), e);
        } finally {
            DSApi.closeContextIfNeeded(wasOpened);
        }

        return createdEntries;
    }


    private void extendedCopyFromPosition(ErpPositionEntity resPosition, InvoiceEntry entry) throws EdmException {
        switch (resDocType){
            case WNIOSEK_ZAKUPOWY:{
                return;
            }
            case ZAPOTRZEBOWANIE:{
                return;
            }
            case ZAMOWIENIE_PUBLICZNE:{
                return;
            }
            default:{
                throw new EdmException("Nie obs�ugiwany typ dokumentu");
            }
        }
    }

    private ErpPositionEntity findPosition(Long reservationId) throws EdmException {
        Class erpEntityClass;
        switch (resDocType){
            case WNIOSEK_ZAKUPOWY:{
                erpEntityClass = ZrodloFinansowaniaDict.class;
                break;
            }
            case ZAPOTRZEBOWANIE:{
                erpEntityClass = ZrodloFinansowaniaDict.class;
                break;
            }
            case ZAMOWIENIE_PUBLICZNE:{
                erpEntityClass = ZrodloFinansowaniaDict.class;
                break;
            }
            default:{
                throw new EdmException("Nie obs�ugiwany typ dokumentu");
            }
        }

        return ErpPositionEntity.findById(reservationId,erpEntityClass);
    }

    private InvoiceEntry filledEntryFromSource(String erpDocumentIdm, ZrodloFinansowaniaDict zrodlo, Integer erpPositionId) {
        zrodlo.setId(null);
        zrodlo.setPositionNumber(null);
        InvoiceEntry entry = new InvoiceEntry();
        entry.setZrodloFinansowania(zrodlo);

        entry.setErpDocumentIdm(erpDocumentIdm);

        StawkaVatDict stawkaVat = new StawkaVatDict();
        stawkaVat.setPositionNumber(erpPositionId);

        Set<StawkaVatDict> stawkiVat = new HashSet<StawkaVatDict>();
        stawkiVat.add(stawkaVat);
        entry.setStawkaVatDicts(stawkiVat);

        return entry;
    }

    /**
     * TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
     *
     * @param erpDocumentIdm
     * @param pozycjaId
     * @return
     */
    protected static Integer getErpPositionId(String erpDocumentIdm, Integer pozycjaId) {
/*
        PreparedStatement ps = null;
        ResultSet rs;
        try {
            String GET_POSITIONS_FROM_ERP = "select p.bd_rezerwacja_poz_id, p.nrpoz\n" +
                    "\n" +
                    "from bd_rezerwacja_poz p with(nolock)\n" +
                    "\n" +
                    "left join bd_rezerwacja b with(nolock) on p.bd_rezerwacja_id = b.bd_rezerwacja_id\n" +
                    "\n" +
                    "where b.bd_rezerwacja_idm = ? and nrpoz = ?";

            Connection connection = DriverManager.getConnection(
                    "jdbc:jtds:sqlserver://172.16.11.3/simpleERP_TEST", "sa", "Simple123");
            ps = connection.prepareStatement(Docusafe.getAdditionPropertyOrDefault("export.get_reservation_positions", GET_POSITIONS_FROM_ERP));
            ps.setString(1, reservationCode);
            ps.setInt(2, pozycjaId);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getLong(1);
            }
        } finally {
            DbUtils.closeQuietly(ps);
        }

        return null;
        */
        return pozycjaId;
    }




    protected DocType resolveDocType(Integer docType, String docKindCn){
        DocType resolved = null;
        if(docType != null){
            resolved = DocType.valueOf(docType);
        }

        if(resolved == null && docKindCn != null){
            resolved = DocType.valueOf(docKindCn.toUpperCase());
        }
        return resolved;
    }

    private void checkState(boolean expression, @Nullable Object errorMessage) throws EdmException {
        if (!expression) {
            throw new EdmException(String.valueOf(errorMessage));
        }
    }


    public enum DocType{
        //Nie zmienia� kolejno�ci
        WNIOSEK_ZAKUPOWY("ZRODLO_FINANSOWANIA","ERP_DOCUMENT_IDM"),
        ZAPOTRZEBOWANIE("ZRODLO_FINANSOWANIA","ERP_DOCUMENT_IDM"),
        ZAMOWIENIE_PUBLICZNE("ZRODLO_FINANSOWANIA","ERP_DOCUMENT_IDM");

        public final String entryDicCn;
        public final String erpIdmFieldCn;

        DocType(String entryDicCn, String erpIdmFieldCn) {
            this.entryDicCn = entryDicCn;
            this.erpIdmFieldCn = erpIdmFieldCn;
        }

        public static DocType valueOf(int ordinal){
            for(DocType type : values()){
                if(type.ordinal() == ordinal){
                    return type;
                }
            }
            return null;
        }

    }

    public String getErrorMessage() {
        return errMsg.toString();
    }

    public boolean wasError(){
        if(errMsg.length() > 0 ){
            return true;
        }else{
            return false;
        }
    }

    public Collection<Long> getAllEntriesIds(){
        Set<Long> allIds = Sets.newHashSet();
        if(entriesIds != null){
            allIds.addAll(entriesIds);
        }
        if(createdEntries != null){
            allIds.addAll(createdEntries);
        }
        return allIds;
    }
}
