package pl.compan.docusafe.parametrization.uek.utils.zamowieniePubliczne;

import org.apache.commons.lang3.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrMappingUtils;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.parametrization.uek.ZamowieniePubliczneLogic;
import pl.compan.docusafe.parametrization.uek.hbm.ZamowieniePubliczneModuly;

import java.util.*;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ModulyZamowieniaPopup {
    public static String ENTRY_DICTIONARY = ZamowieniePubliczneLogic.DICT_MODULY_ZAMOWIENIA;
    public final static String ENTRY_ID_FIELD = ZamowieniePubliczneLogic.FIELD_POPUP_MODUL_ID;

    public static void clearPopup(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        Set<String> popupFields = new HashSet<String>();
        popupFields.addAll(ZamowieniePubliczneModuly.getMapToDocFields().values());
        popupFields.add(ENTRY_ID_FIELD);

        DwrUtils.resetFields(values, fm, popupFields);
    }

    public static void deleteEntry(Map<String, FieldData> values, FieldsManager fm, Integer rowNo) throws EdmException {
        Long entryId = Long.valueOf(DwrUtils.getValueFromDictionary(values, ENTRY_DICTIONARY, "ID_" + rowNo).toString());
        if(entryId != null){
            boolean wasOpened = false;
            try{
                wasOpened = DSApi.openContextIfNeeded();

                ZamowieniePubliczneModuly entry = ZamowieniePubliczneModuly.findById(entryId);
                entry.delete();
            }finally {
                DSApi.closeContextIfNeeded(wasOpened);
            }
        }else{
            throw new EdmException("B��d przy pr�bie usuni�cia wpisu");
        }
        DwrUtils.updateMultipleDictionary(values,ENTRY_DICTIONARY,null,entryId);
    }

    public static void updatePopup(Map<String, FieldData> values, FieldsManager fm, Integer rowNo) throws EdmException {
        Long entryId = Long.valueOf(DwrUtils.getValueFromDictionary(values, ENTRY_DICTIONARY, "ID_" + rowNo).toString());

        if(entryId != null){
            boolean wasOpened = false;
            try{
                wasOpened = DSApi.openContextIfNeeded();
                ZamowieniePubliczneModuly entry = ZamowieniePubliczneModuly.findById(entryId);
                if(entry == null){
                    throw new EdmException("B��d inicjalizacji Popup: b��dne Id wpisu;");
                }

                DwrMappingUtils.setFieldsFromEntity(values, fm, entry.getMapToDocFields(), entry);
            }finally {
                DSApi.closeContextIfNeeded(wasOpened);
            }

            values.get(DwrUtils.dwr(ENTRY_ID_FIELD)).setStringData(entryId.toString());
        }
    }



    public static void saveEntry(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        ZamowieniePubliczneModuly entry = DwrMappingUtils.getEntityFromDwr(values,fm,ZamowieniePubliczneModuly.getMapToDocFields(),ZamowieniePubliczneModuly.class,null).get(0);
        Long id = extractId(values,ENTRY_ID_FIELD);
        entry.setId(id);

        entry.setCpvText(prepareCpvText(values,fm));

        entry.save();

        DwrUtils.updateMultipleDictionary(values,ENTRY_DICTIONARY,entry.getId(),null);

    }
    private static String prepareCpvText(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        StringBuilder cpvText = new StringBuilder();

        FieldData main = values.get(DwrUtils.dwr(ZamowieniePubliczneLogic.FIELD_POPUP_MODUL_KODY_CPV_MAIN));
        if (main!=null)
            cpvText.append(FieldsManagerUtils.getEnumTitle(fm,ZamowieniePubliczneLogic.FIELD_POPUP_MODUL_KODY_CPV_MAIN,main.getEnumValuesData().getSelectedId()));

        List<String> extrasList = new ArrayList<String>();
        Collection<Map<String, FieldData>> extrasRows = DwrUtils.getDictionaryData(values, ZamowieniePubliczneLogic.FIELD_POPUP_MODUL_KODY_CPV_EXTRA);
        DataBaseEnumField cpvField = FieldsManagerUtils.getDictionaryField(fm, ZamowieniePubliczneLogic.FIELD_POPUP_MODUL_KODY_CPV_EXTRA, "KOD_CPV", DataBaseEnumField.class);
        if (cpvField != null){
            for(Map<String, FieldData> extraRow : extrasRows){
                FieldData extra = extraRow.get(ZamowieniePubliczneLogic.DICTFIELD_POPUP_MODUL_KODY_CPV);
                if (extra!=null)
                    extrasList.add(FieldsManagerUtils.getEnumTitle(cpvField, extra.getEnumValuesData().getSelectedId()));
            }
            if (!extrasList.isEmpty()){
                if (cpvText.length()>0)
                    cpvText.append(" : ");
                cpvText.append(StringUtils.join(extrasList.iterator(),", "));
            }
        }

        return cpvText.toString();
    }

    private static Long extractId(Map<String, FieldData> values, String idFieldName) throws EdmException {
        if(!idFieldName.startsWith(DwrUtils.DWR_PREFIX)){
            idFieldName = DwrUtils.DWR_PREFIX + idFieldName;
        }
        String id = values.get(idFieldName).getStringData();
        if(id.matches("\\d+")){
            return Long.parseLong(id);
        }else{
            return null;
        }
    }
}
