package pl.compan.docusafe.parametrization.uek.utils;

import org.apache.commons.lang.StringUtils;
import org.jfree.util.Log;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.dockinds.field.XmlEnumColumnField;
import pl.compan.docusafe.parametrization.uek.hbm.StawkaVatDict;
import pl.compan.docusafe.parametrization.uek.hbm.ZrodloFinansowaniaDict2;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by Damian on 12.03.14.
 */
public class ZrodloFinansowaniaUtils2 {
    private final static Logger log = LoggerFactory.getLogger(ZrodloFinansowaniaUtils2.class);


    public final static String DWR_PREFIX = "DWR_";

    public final static String ZRODLO_ID = "ZRODLO_ENTRY_ID";

    public final static String ZRODLA_FINANSOWANIA = "SLOWNIK_PZ";
    public final static String ZRODLO_DESCRIPTION = "ZRODLO_DESCRIPTION";

    public final static String BUDGET_KIND = "BUDGET_KIND";
    public final static String BUDGET = "BUDGET";
    public final static String POSITION = "POSITION";
    public final static String CONTRACT = "CONTRACT";
    public final static String CONTRACT_STAGE = "CONTRACT_STAGE";
    public final static String FUND = "FUND";
    public final static String SOURCE = "SOURCE";


    public final static String PRODUKTY = "PRODUKTY";
    public final static String NAZWA_ZAMOWIENIA = "NAZWA_ZAMOWIENIA";
    public final static String ILOSC = "ILOSC";
    public final static String WARTOSC_NETTO = "WARTOSC_NETTO";
    public final static String RODZAJ_DZIALALNOSCI = "RODZAJ_DZIALALNOSCI";
    public final static String TERMIN = "TERMIN";
    public final static String PRZEZNACZENIE_ZAKUPU = "PRZEZNACZENIE_ZAKUPU";

    public final static String STAWKI_VAT = "STAWKI_VAT";
    public final static String NET_AMOUNT = "NET_AMOUNT";
    public final static String VAT_RATE = "VAT_RATE";
    public final static String GROSS_AMOUNT = "GROSS_AMOUNT";
    public final static String PURPOSE = "PURPOSE";
    public static final String PROJECT_BUDGET = "PROJECT_BUDGET";


    public static ZrodloFinansowaniaDict2 saveZrodloFromDWR(Map<String, FieldData> values, FieldsManager fm) throws EdmException {

        StringBuilder description = new StringBuilder();
        boolean wasOpened = false;

        try{
            wasOpened = DSApi.openContextIfNeeded();


            ZrodloFinansowaniaDict2 zrodlo = extractZrodlo(values, fm);

            fillZrodloFromDWR(fm,values,fm.getIntegerKey(BUDGET_KIND),zrodlo,description);



            zrodlo.setDescription(description.toString());

            zrodlo.save();
           return zrodlo;
            //prepareBIGZrodloFinansowaniaDictionary(values, zrodlo, isNewEntry, 60);

        }catch(EdmException e){
            Log.error(e.getMessage(),e);
            throw e;
        }finally {
            DSApi.closeContextIfNeeded(wasOpened);
        }
    }




    private static void fillZrodloFromDWR(FieldsManager fm, Map<String, FieldData> values, Integer budgetKindKey, ZrodloFinansowaniaDict2 zrodlo, StringBuilder description) throws EdmException {
        if(budgetKindKey != null){
            fillFromStandardFields(fm,values, zrodlo, description);
            if(budgetKindKey.equals(1)){
                fillFromBdProjektow(fm,values,zrodlo,description);
            }else if(budgetKindKey.equals(2)){
                fillFromBdKomorek(fm,values,zrodlo,description);
            }else{
                throwNotFiledFieldException(FieldsManagerUtils.getFieldName(fm,BUDGET_KIND,false));
            }
        }else{
            throwNotFiledFieldException(FieldsManagerUtils.getFieldName(fm,BUDGET_KIND,false));
        }
    }

    /**
     * Fills zrodlo from fields that are specyfic for "Bud�et Projekt�w"
     * @param fm
     * @param values
     * @param zrodlo
     * @param description
     */
    private static void fillFromBdProjektow(FieldsManager fm, Map<String, FieldData> values, ZrodloFinansowaniaDict2 zrodlo, StringBuilder description) throws EdmException {

        if(getStringValue(fm, values, CONTRACT)!= null){
            appendDescription(fm, values, description, CONTRACT);
        }else{
            throwNotFiledFieldException(FieldsManagerUtils.getFieldName(fm,CONTRACT,false));
        }
        zrodlo.setContract(getLongValue(fm, values, CONTRACT));


        if(getStringValue(fm, values, CONTRACT_STAGE)!= null){
            appendDescription(fm,values,description,CONTRACT_STAGE);
        }else{
            throwNotFiledFieldException(FieldsManagerUtils.getFieldName(fm,CONTRACT_STAGE,false));
        }
        zrodlo.setContractStage(getLongValue(fm, values, CONTRACT_STAGE));

        if(getStringValue(fm, values, PROJECT_BUDGET)!= null){
            appendDescription(fm, values, description, PROJECT_BUDGET);
        }else{
            throwNotFiledFieldException(FieldsManagerUtils.getFieldName(fm,PROJECT_BUDGET,false));
        }
        zrodlo.setProjectBudget(getLongValue(fm, values, PROJECT_BUDGET));


        if(getStringValue(fm, values, FUND)!= null){
            appendDescription(fm, values, description, FUND);
        }else{
            throwNotFiledFieldException(FieldsManagerUtils.getFieldName(fm,FUND,false));
        }
        zrodlo.setFund(getLongValue(fm, values, FUND));
    }

    /**
     * Fills zrodlo from fields that are specyfic for "Bud�et Projekt�w"
     * @param fm
     * @param values
     * @param zrodlo
     * @param description
     */
    private static void fillFromBdKomorek(FieldsManager fm, Map<String, FieldData> values, ZrodloFinansowaniaDict2 zrodlo, StringBuilder description) throws EdmException {

        if(getStringValue(fm, values, BUDGET)!= null){
            appendDescription(fm, values, description, BUDGET);
        }else{
            throwNotFiledFieldException(FieldsManagerUtils.getFieldName(fm,BUDGET,false));
        }
        zrodlo.setBudget(getLongValue(fm, values, BUDGET));

        if(getStringValue(fm, values, POSITION)!= null){
            appendDescription(fm, values, description, POSITION);
        }else{
            throwNotFiledFieldException(FieldsManagerUtils.getFieldName(fm,POSITION,false));
        }
        zrodlo.setPosition(getLongValue(fm, values, POSITION));
    }

    /**
     * Fills zrodlo from fields that are independant from budget kind (include field Budget Kind)
     *
     * @param fm
     * @param values
     *@param zrodlo
     * @param description   @throws EdmException
     */
    private static void fillFromStandardFields(FieldsManager fm, Map<String, FieldData> values, ZrodloFinansowaniaDict2 zrodlo, StringBuilder description) throws EdmException {
        if(getStringValue(fm, values, BUDGET_KIND)!= null){
            appendDescription(fm, values, description, BUDGET_KIND);
        }else{
            throwNotFiledFieldException(FieldsManagerUtils.getFieldName(fm,BUDGET_KIND,false));
        }
        zrodlo.setBudgetKind(getLongValue(fm, values, BUDGET_KIND).intValue());


        if(getStringValue(fm, values, SOURCE)!= null){
            appendDescription(fm, values, description, SOURCE);
        }else{
            throwNotFiledFieldException(FieldsManagerUtils.getFieldName(fm,SOURCE,false));
        }
        zrodlo.setSource(getLongValue(fm, values, SOURCE));

        if(getStringValue(fm, values, PRODUKTY)!= null){
            appendDescription(fm, values, description, PRODUKTY);
        }else{
            throwNotFiledFieldException(FieldsManagerUtils.getFieldName(fm,PRODUKTY,false));
        }
        zrodlo.setProdukty(getLongValue(fm, values, PRODUKTY));

        if(getStringValue(fm, values, NAZWA_ZAMOWIENIA)!= null){
            appendDescription(fm, values, description, NAZWA_ZAMOWIENIA);
        }else{
            throwNotFiledFieldException(FieldsManagerUtils.getFieldName(fm,NAZWA_ZAMOWIENIA,false));
        }
        zrodlo.setNazwaZamowienia(getStringValue(fm, values, NAZWA_ZAMOWIENIA));

        if(getStringValue(fm, values, RODZAJ_DZIALALNOSCI)!= null){
            appendDescription(fm, values, description, RODZAJ_DZIALALNOSCI);
        }else{
            throwNotFiledFieldException(FieldsManagerUtils.getFieldName(fm,RODZAJ_DZIALALNOSCI,false));
        }
        zrodlo.setRodzajDzialalnosci(getLongValue(fm, values, RODZAJ_DZIALALNOSCI));


        if(getStringValue(fm, values, ILOSC)!= null){
            appendDescription(fm, values, description, ILOSC);
        }else{
            throwNotFiledFieldException(FieldsManagerUtils.getFieldName(fm,ILOSC,false));
        }
        zrodlo.setIlosc(getLongValue(fm, values, ILOSC));


        if(getStringValue(fm, values, WARTOSC_NETTO)!= null){
            appendDescription(fm, values, description, WARTOSC_NETTO);
        }else{
            throwNotFiledFieldException(FieldsManagerUtils.getFieldName(fm,WARTOSC_NETTO,false));
        }
        zrodlo.setWartoscNetto(getFloatValue(fm, values, WARTOSC_NETTO));


        if(getStringValue(fm, values, TERMIN)!= null){
            appendDescription(fm, values, description, TERMIN);
        }else{
            throwNotFiledFieldException(FieldsManagerUtils.getFieldName(fm,TERMIN,false));
        }
        zrodlo.setTermin(getObjectValue(fm, values, TERMIN));

        if(getStringValue(fm, values, PRZEZNACZENIE_ZAKUPU)!= null){
            appendDescription(fm, values, description, PRZEZNACZENIE_ZAKUPU);
        }else{
            throwNotFiledFieldException(FieldsManagerUtils.getFieldName(fm,PRZEZNACZENIE_ZAKUPU,false));
        }
        zrodlo.setPrzeznaczenieZakupu(getLongValue(fm, values, PRZEZNACZENIE_ZAKUPU));








    }


    private static StringBuilder appendDescription(FieldsManager fm, Map<String, FieldData> values,StringBuilder description, String fieldName) throws EdmException {
        description.append(FieldsManagerUtils.getFieldName(fm,fieldName,false))
                .append(": ")
                .append(getStringValue(fm, values, fieldName).trim())
                .append(" |\n ");
        return description;
    }

    private static String getStringValue(FieldsManager fm, Map<String, FieldData> values, String fieldName) throws EdmException {
        if(fm.getStringValue(fieldName)!= null){
            return fm.getStringValue(fieldName);
        }else if(StringUtils.isNotBlank(DwrUtils.getEnumValues(values, DWR_PREFIX + fieldName).getSelectedId())){
            EnumItem selectedItem = FieldsManagerUtils.getEnumItem(fm.getField(fieldName),DwrUtils.getEnumValues(values, DWR_PREFIX + fieldName).getSelectedId());
            return selectedItem.getTitle();
        } else{
            return null;
        }
    }

    private static Float getFloatValue(FieldsManager fm, Map<String, FieldData> values, String fieldName) throws EdmException {
        if(fm.getFloatValue(fieldName) != null){
            return fm.getFloatKey(fieldName);
        }else if(StringUtils.isNotBlank(DwrUtils.getStringValue(values,DWR_PREFIX + fieldName))){
            String selectedId = DwrUtils.getStringValue(values,DWR_PREFIX + fieldName);
            if(selectedId.matches("\\d+")){
                return Float.parseFloat(selectedId);
            }else{
                throw new EdmException("B��d inicjalizacji pola " + fieldName);
            }
        }else{
            return null;
        }
    }

    private static Date getObjectValue(FieldsManager fm, Map<String, FieldData> values, String fieldName) throws EdmException {
        if(fm.getValue(fieldName) != null){
            Date toDate = (Date)fm.getKey(fieldName);
            return toDate;
        } else {
            return null;
        }
    }
    private static Long getLongValue(FieldsManager fm, Map<String, FieldData> values, String fieldName) throws EdmException {
        if(fm.getLongKey(fieldName) != null){
            return fm.getLongKey(fieldName);
        }else if(StringUtils.isNotBlank(DwrUtils.getStringValue(values,DWR_PREFIX + fieldName))){
            String selectedId = DwrUtils.getStringValue(values,DWR_PREFIX + fieldName);
            if(selectedId.matches("\\d+")){
                return Long.parseLong(selectedId);
            }else{
                throw new EdmException("B��d inicjalizacji pola " + fieldName);
            }
        }else{
            return null;
        }
    }



    /**
     * Rzuca wyj?tek z gotowym opisem o nie uzupe�nionym polu
     * @param fieldName
     * @throws EdmException
     */
    private static void throwNotFiledFieldException(String fieldName) throws EdmException {
        throw new EdmException("Pole " + fieldName + " musi by� uzupe�nione, aby doda� �r�d�o finansowania");
    }


    private static ZrodloFinansowaniaDict2 extractZrodlo(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        ZrodloFinansowaniaDict2 zrodlo;

        String id = values.get(DWR_PREFIX + ZRODLO_ID).getStringData();
        if(id.matches("\\d+")){
            Long zrodloId = Long.parseLong(id);
            zrodlo = ZrodloFinansowaniaDict2.findById(zrodloId);
            if(zrodlo == null){
                throw new EdmException("B��d inicjalizacji Popup: b��dne Id �r�d�a finansowania;");
            }
        }else{
            zrodlo = new ZrodloFinansowaniaDict2();
        }

        return zrodlo;
    }

    public static Collection<Long> updateZrodloFinansowaniaDictionary(Map<String, FieldData> values, Long idToAdd, Long idToRemove) {
        Set<Long> zrodlaValues = new HashSet<Long>();

        Map<String, FieldData> zrodlaFields = DwrUtils.getDictionaryFields(values, ZRODLA_FINANSOWANIA, "ID");
        for (FieldData value : zrodlaFields.values()) {
            zrodlaValues.add(value.getLongData());
        }
        if(idToAdd != null){
            zrodlaValues.add(idToAdd);
        }
        if(idToRemove != null){
            zrodlaValues.remove(idToRemove);
        }

        FieldData fd = new FieldData(Field.Type.DICTIONARY, zrodlaValues);
        values.put(DWR_PREFIX+ZRODLA_FINANSOWANIA, fd);
        return zrodlaValues;
    }




    /**
     * Fake do test�w jak wygl?da przy du�ej ilo?ci wpis�w
     */
    private static void prepareBIGZrodloFinansowaniaDictionary(Map<String, FieldData> values, ZrodloFinansowaniaDict2 zrodlo, boolean isNewEntry, long ileWierszy){
        List<Long> fakeList = new ArrayList<Long>();
        for(long i = 1; i <= ileWierszy; i++){
            fakeList.add(i);
        }
        fakeList.add(zrodlo.getId());
        FieldData fd = new FieldData(Field.Type.DICTIONARY, fakeList);
        values.put(DWR_PREFIX+ZRODLA_FINANSOWANIA, fd);
    }



    public static void clearPopupFields(Map<String, FieldData> values, FieldsManager fm) throws EdmException {

        Set<String> enumFieldsNames = getPopupEnumNames();

        for(String fieldName : enumFieldsNames) {
            FieldData field = values.get(fieldName);
          /*  if (values.get(fieldName) != null && field.getType() == Field.Type.ENUM) {
                DwrUtils.setEnumNoSelected(values.get(fieldName).getEnumValuesData());
            } else if (field.getType() == Field.Type.DATE) {
                if (field == null) {
                    values.get(fieldName).setDateData(null);
                    FieldData data2 = values.get(fieldName);
                } else {
                    values.get(fieldName).setDateData(null);
                    FieldData data2 = values.get(fieldName);

                }
            }*/

           if (field.getType() == Field.Type.ENUM) {
                DwrUtils.setEnumNoSelected(values.get(fieldName).getEnumValuesData());
            }



           else if (field.getType()==Field.Type.DATE){
               if(field==null){values.get(fieldName).setData(null);
                   FieldData data2 = values.get(fieldName);
               }
               else{
                   values.get(fieldName).setData(null);
                   FieldData data2 = values.get(fieldName); }

            }
            else if (field.getType() == Field.Type.INTEGER){
                values.get(fieldName).setIntegerData(0);

            }

            else if (field.getType() == Field.Type.LONG){
                values.get(fieldName).setLongData((long) 0);}

            else if (field.getType() == Field.Type.MONEY){
                values.get(fieldName).setMoneyData(BigDecimal.valueOf(0));}
            }




        values.get(DWR_PREFIX + ZRODLO_ID).setStringData("");

        }

    private static Set<String> getPopupEnumNames() {
        Set<String> enumNames = new HashSet<String>();
        enumNames.add(DWR_PREFIX+BUDGET_KIND);
        enumNames.add(DWR_PREFIX+BUDGET);
        enumNames.add(DWR_PREFIX+PROJECT_BUDGET);
        enumNames.add(DWR_PREFIX+POSITION);
        enumNames.add(DWR_PREFIX+CONTRACT);
        enumNames.add(DWR_PREFIX+CONTRACT_STAGE);
        enumNames.add(DWR_PREFIX+FUND);
        enumNames.add(DWR_PREFIX+SOURCE);
        enumNames.add(DWR_PREFIX+TERMIN);
        enumNames.add(DWR_PREFIX+ILOSC);


        enumNames.add(DWR_PREFIX+PRODUKTY);
        enumNames.add(DWR_PREFIX+RODZAJ_DZIALALNOSCI);
        enumNames.add(DWR_PREFIX+PRZEZNACZENIE_ZAKUPU);

        enumNames.add(DWR_PREFIX+WARTOSC_NETTO);


        return enumNames;
    }



  /*
   enumNames.add(DWR_PREFIX+ILOSC);

enumNames.add(DWR_PREFIX+NAZWA_ZAMOWIENIA);
        enumNames.add(DWR_PREFIX+PRODUKTY);
        enumNames.add(DWR_PREFIX+RODZAJ_DZIALALNOSCI);
        enumNames.add(DWR_PREFIX+PRZEZNACZENIE_ZAKUPU);
        enumNames.add(DWR_PREFIX+ILOSC);
        enumNames.add(DWR_PREFIX+WARTOSC_NETTO);
     ;*/

    public static void updateDwrFromZrodlo(Map<String, FieldData> values, FieldsManager fm, Integer entryId) throws EdmException {

        Map<String, FieldData> zrodloDictionaryMap = values.get(DWR_PREFIX + ZRODLA_FINANSOWANIA).getDictionaryData();
        Long zrodloId = zrodloDictionaryMap.get(ZRODLA_FINANSOWANIA+"_ID_"+ entryId).getLongData();

        if(entryId != null && zrodloId != null){
            values.get(DWR_PREFIX + ZRODLO_ID).setStringData(zrodloId.toString());
        }

        if(zrodloId != null){
            boolean wasOpened = false;
            try{
                wasOpened = DSApi.openContextIfNeeded();
                ZrodloFinansowaniaDict2 zrodlo = ZrodloFinansowaniaDict2.findById(zrodloId);

                updateFieldsFromZrodlo(values,fm,zrodlo);

            }finally {
                DSApi.closeContextIfNeeded(wasOpened);
            }

        }

    }

    private static void updateFieldsFromZrodlo(Map<String, FieldData> values, FieldsManager fm, ZrodloFinansowaniaDict2 zrodlo) throws EdmException {

        putFieldValue(values,DWR_PREFIX+BUDGET_KIND,zrodlo.getBudgetKind() != null ? zrodlo.getBudgetKind().toString() : "");
        putFieldValue(values,DWR_PREFIX+BUDGET,zrodlo.getBudget() != null ? zrodlo.getBudget().toString() : "");
        putFieldValue(values,DWR_PREFIX+PROJECT_BUDGET,zrodlo.getProjectBudget() != null ? zrodlo.getProjectBudget().toString() : "");
        putFieldValue(values,DWR_PREFIX+POSITION,zrodlo.getPosition() != null ? zrodlo.getPosition().toString() : "");
        putFieldValue(values,DWR_PREFIX+CONTRACT,zrodlo.getContract() != null ? zrodlo.getContract().toString() : "");
        putFieldValue(values,DWR_PREFIX+CONTRACT_STAGE,zrodlo.getContractStage() != null ? zrodlo.getContractStage().toString() : "");
        putFieldValue(values,DWR_PREFIX+FUND,zrodlo.getFund() != null ? zrodlo.getFund().toString() : "");
        putFieldValue(values,DWR_PREFIX+SOURCE,zrodlo.getSource() != null ? zrodlo.getSource().toString() : "");
        putFieldValue(values,DWR_PREFIX+PRODUKTY,zrodlo.getProdukty() != null ? zrodlo.getProdukty().toString() : "");
        putFieldValue(values,DWR_PREFIX+NAZWA_ZAMOWIENIA,zrodlo.getNazwaZamowienia() != null ? zrodlo.getNazwaZamowienia().toString() : "");
        putFieldValue(values,DWR_PREFIX+ILOSC,zrodlo.getIlosc() != null ? zrodlo.getIlosc().toString() : "");
        putFieldValue(values,DWR_PREFIX+WARTOSC_NETTO,zrodlo.getWartoscNetto() != null ? zrodlo.getWartoscNetto().toString() : "");
        putFieldValue(values,DWR_PREFIX+RODZAJ_DZIALALNOSCI,zrodlo.getRodzajDzialalnosci() != null ? zrodlo.getRodzajDzialalnosci().toString() : "");
        putFieldValue(values,DWR_PREFIX+TERMIN,zrodlo.getTermin() != null ? zrodlo.getTermin().toString() : "");
        putFieldValue(values,DWR_PREFIX+PRZEZNACZENIE_ZAKUPU,zrodlo.getPrzeznaczenieZakupu() != null ? zrodlo.getPrzeznaczenieZakupu().toString() : "");

      /*  List<Long> stawkiValues = new ArrayList<Long>();

        for(StawkaVatDict stawkaEntry : zrodlo.getStawkaVatDict()){
            stawkiValues.add(stawkaEntry.getId());
        }

        FieldData fd = new FieldData(Field.Type.DICTIONARY, stawkiValues);
        values.put(DWR_PREFIX+STAWKI_VAT, fd);*/

    }

    private static void putFieldValue(Map<String, FieldData> values, String key, String selectedId) {
        EnumValues data = new EnumValues();
        data.setSelectedId(selectedId);
        FieldData field = new FieldData(Field.Type.ENUM, data);
        values.put(key,field);
    }

    public static void deleteZrodlo(Map<String, FieldData> values, FieldsManager fm, Integer rowNo) throws EdmException {
        Integer zrodloId = (Integer)DwrUtils.getValueFromDictionary(values,ZRODLA_FINANSOWANIA,"ID_"+rowNo);
        if(zrodloId != null){
            boolean wasOpened = false;
            try{
                wasOpened = DSApi.openContextIfNeeded();

                ZrodloFinansowaniaDict2 zrodlo = ZrodloFinansowaniaDict2.findById(zrodloId.longValue());
                zrodlo.delete();

            }finally {
                DSApi.closeContextIfNeeded(wasOpened);
            }
        }else{
            throw new EdmException("B��d przy pr�bie usuni�cia wpisu");
        }
        updateZrodloFinansowaniaDictionary(values,null,zrodloId.longValue());
    }
}
