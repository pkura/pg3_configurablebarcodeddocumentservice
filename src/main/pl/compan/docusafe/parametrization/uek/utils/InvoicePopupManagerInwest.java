package pl.compan.docusafe.parametrization.uek.utils;


import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrMappingUtils;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.general.canonical.model.StawkaVat;
import pl.compan.docusafe.parametrization.uek.hbm.InvoiceEntry;

import pl.compan.docusafe.parametrization.uek.hbm.ZrodloFinansowaniaDict;

import java.util.*;
/**
 * Created by Raf on 2014-05-06.
 */
public class InvoicePopupManagerInwest {

    public final static String ENTRY_DICTIONARY = "ZRODLO_FINANSOWANIA";
    private final static String ENTRY_ID_FIELD = "ENTRY_ID";
    private final static String DWR_PREFIX = DwrUtils.DWR_PREFIX;

    public static void clearPopup(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        Set<String> popupFields = new HashSet<String>();

        popupFields.addAll(ZrodloFinansowaniaDict.getMapToDocFields().values());
        popupFields.add(ENTRY_ID_FIELD);


        DwrUtils.resetFields(values, fm, popupFields);
    }

    public static void deleteEntry(Map<String, FieldData> values, FieldsManager fm, Integer rowNo) throws EdmException {

        Long entryId = Long.valueOf(DwrUtils.getValueFromDictionary(values, ENTRY_DICTIONARY, "ID_" + rowNo).toString());
        if(entryId != null){
            boolean wasOpened = false;
            try{
                wasOpened = DSApi.openContextIfNeeded();
                ZrodloFinansowaniaDict zrodlo = ZrodloFinansowaniaDict.findById(entryId);
                zrodlo.delete();
            }finally {
                DSApi.closeContextIfNeeded(wasOpened);
            }
        }else{
            throw new EdmException("B��d przy pr�bie usuni�cia wpisu");
        }
        DwrUtils.updateMultipleDictionary(values,ENTRY_DICTIONARY,null,entryId);
    }

    public static void updatePopup(Map<String, FieldData> values, FieldsManager fm, Integer rowNo) throws EdmException {


        Long entryId = Long.valueOf(DwrUtils.getValueFromDictionary(values, ENTRY_DICTIONARY, "ID_" + rowNo).toString());

        if(entryId != null){
            boolean wasOpened = false;
            try{
                wasOpened = DSApi.openContextIfNeeded();
                ZrodloFinansowaniaDict zrodlo = ZrodloFinansowaniaDict.findById(entryId);

                if(zrodlo == null){
                    throw new EdmException("B��d inicjalizacji Popup: b��dne Id wpisu;");
                }

                DwrMappingUtils.setFieldsFromEntity(values,fm,zrodlo.getMapToDocFields(),zrodlo);



            }finally {
                DSApi.closeContextIfNeeded(wasOpened);
            }

            values.get(DWR_PREFIX + ENTRY_ID_FIELD).setStringData(entryId.toString());
        }
    }



    public static void saveEntry(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        StringBuilder description = new StringBuilder();



        ZrodloFinansowaniaDict zrodlo = DwrMappingUtils.getEntityFromDwr(values,fm,ZrodloFinansowaniaDict.getMapToDocFields(),ZrodloFinansowaniaDict.class,null).get(0);

        description.append(DwrMappingUtils.prepareDescription(ZrodloFinansowaniaDict.getMapToDocFields(), zrodlo, fm, null));


        zrodlo.setDescription(description.toString());
        zrodlo.save();

        DwrUtils.updateMultipleDictionary(values,ENTRY_DICTIONARY,zrodlo.getId(),null);

    }



    private static Long extractId(Map<String, FieldData> values, String idFieldName) throws EdmException {
        if(!idFieldName.startsWith(DWR_PREFIX)){
            idFieldName = DWR_PREFIX + idFieldName;
        }
        String id = values.get(idFieldName).getStringData();
        if(id.matches("\\d+")){
            return Long.parseLong(id);
        }else{
            return null;
        }
    }
}
