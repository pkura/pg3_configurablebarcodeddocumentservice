package pl.compan.docusafe.parametrization.uek.utils.zamowieniePubliczne;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrMappingUtils;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.parametrization.uek.ZamowieniePubliczneLogic;
import pl.compan.docusafe.parametrization.uek.hbm.ZamowieniePubliczneZrFinansowania;
import pl.compan.docusafe.parametrization.uek.hbm.ZrodloFinansowaniaDict;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ZrodlaFinansowaniaPoopup {
    public final static String ENTRY_DICTIONARY = ZamowieniePubliczneLogic.DICT_ZR_FINANS;
    public final static String ENTRY_ID_FIELD = "FINANCIAL_ID";
    private final static String FINANCIAL_AMOUT_FIELD = "FINANCIAL_AMOUT";
    private final static String DWR_PREFIX = DwrUtils.DWR_PREFIX;

    public static void clearPopup(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        Set<String> popupFields = new HashSet<String>();
        popupFields.addAll(ZamowieniePubliczneZrFinansowania.getMapToDocFields().values());
        popupFields.add(ENTRY_ID_FIELD);

        DwrUtils.resetFields(values, fm, popupFields);
    }

    public static void deleteEntry(Map<String, FieldData> values, FieldsManager fm, Integer rowNo) throws EdmException {
        Long entryId = Long.valueOf(DwrUtils.getValueFromDictionary(values, ENTRY_DICTIONARY, "ID_" + rowNo).toString());
        if(entryId != null){
            boolean wasOpened = false;
            try{
                wasOpened = DSApi.openContextIfNeeded();

                ZamowieniePubliczneZrFinansowania entry = ZamowieniePubliczneZrFinansowania.findById(entryId);
                entry.delete();
            }finally {
                DSApi.closeContextIfNeeded(wasOpened);
            }
        }else{
            throw new EdmException("B��d przy pr�bie usuni�cia wpisu");
        }
        DwrUtils.updateMultipleDictionary(values,ENTRY_DICTIONARY,null,entryId);
    }

    public static void updatePopup(Map<String, FieldData> values, FieldsManager fm, Integer rowNo) throws EdmException {
        Long entryId = Long.valueOf(DwrUtils.getValueFromDictionary(values, ENTRY_DICTIONARY, "ID_" + rowNo).toString());

        if(entryId != null){
            boolean wasOpened = false;
            try{
                wasOpened = DSApi.openContextIfNeeded();
                ZamowieniePubliczneZrFinansowania entry = ZamowieniePubliczneZrFinansowania.findById(entryId);
                if(entry == null){
                    throw new EdmException("B��d inicjalizacji Popup: b��dne Id wpisu;");
                }

                DwrMappingUtils.setFieldsFromEntity(values, fm, entry.getMapToDocFields(), entry);

                if(entry.getZrodloFinansowania() != null){
                    DwrMappingUtils.setFieldsFromEntity(values, fm, ZrodloFinansowaniaDict.getMapToDocFields(), entry.getZrodloFinansowania());
                }
            }finally {
                DSApi.closeContextIfNeeded(wasOpened);
            }

            values.get(DWR_PREFIX + ENTRY_ID_FIELD).setStringData(entryId.toString());
        }
    }



    public static void saveEntry(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        StringBuilder description = new StringBuilder();

        ZamowieniePubliczneZrFinansowania entry = DwrMappingUtils.getEntityFromDwr(values,fm,ZamowieniePubliczneZrFinansowania.getMapToDocFields(),ZamowieniePubliczneZrFinansowania.class,null).get(0);
        Long id = extractId(values,ENTRY_ID_FIELD);
        entry.setId(id);
        description.append(DwrMappingUtils.prepareDescription(ZamowieniePubliczneZrFinansowania.getMapToDocFields(), entry, fm, null));

        description.append("\n");

        ZrodloFinansowaniaDict zrodlo = DwrMappingUtils.getEntityFromDwr(values,fm,ZrodloFinansowaniaDict.getMapToDocFields(),ZrodloFinansowaniaDict.class,null).get(0);
        entry.setZrodloFinansowania(zrodlo);
        description.append(DwrMappingUtils.prepareDescription(ZrodloFinansowaniaDict.getMapToDocFields(), zrodlo, fm, null));

        entry.setDescription(description.toString());
        entry.save();

        DwrUtils.updateMultipleDictionary(values,ENTRY_DICTIONARY,entry.getId(),null);

    }



    private static Long extractId(Map<String, FieldData> values, String idFieldName) throws EdmException {
        if(!idFieldName.startsWith(DWR_PREFIX)){
            idFieldName = DWR_PREFIX + idFieldName;
        }
        String id = values.get(idFieldName).getStringData();
        if(id.matches("\\d+")){
            return Long.parseLong(id);
        }else{
            return null;
        }
    }
}
