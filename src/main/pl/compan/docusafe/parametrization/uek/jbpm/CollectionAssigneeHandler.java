package pl.compan.docusafe.parametrization.uek.jbpm;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;

import java.util.Collection;

/**
 * Created by Damian on 28.03.14.
 */
public class CollectionAssigneeHandler extends AssigneeHandler {

    /**
     * Name of execution variable that is collection of users names
     */
    private String usersNamesVariable;

    private boolean addToHistory = true;

    private boolean removeVariable = false;


    @Override
    public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {

        boolean wasOpened = true;
        try{
            OfficeDocument doc = null;
            if (addToHistory){
                wasOpened = DSApi.openContextIfNeeded();
                Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
                doc = (OfficeDocument) OfficeDocument.find(docId,false);

            }


            if(usersNamesVariable != null){
                Object users = openExecution.getVariable(usersNamesVariable);

                if (users instanceof Collection && !((Collection) users).isEmpty()) {
                    for(Object userName : (Collection) users){
                        if (userName instanceof String) {
                            assignable.addCandidateUser((String) userName);

                            if (addToHistory){
                                addToHistory(openExecution, doc, (String) userName, null);
                            }

                        }
                        else{
                            throw new Exception("Nie uda�o si� przypisa� u�ytkownik�w na etapie: " + openExecution.getActivity().getName()+" (B��d pozycja w li�cie u�ytkownik�w);");
                        }
                    }

                    if(removeVariable){
                        openExecution.removeVariable(usersNamesVariable);
                    }

                }else{
                    throw new Exception("Nie uda�o si� przypisa� u�ytkownik�w na etapie: " + openExecution.getActivity().getName()+" (B��dna lista u�ytkownik�w);");
                }

            }

        }finally {
            DSApi.closeContextIfNeeded(wasOpened);
        }


    }
}
