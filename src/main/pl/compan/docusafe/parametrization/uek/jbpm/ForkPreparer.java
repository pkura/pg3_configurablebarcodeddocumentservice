package pl.compan.docusafe.parametrization.uek.jbpm;

import org.apache.commons.lang.StringUtils;
import org.jbpm.JbpmException;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PropertyNotFoundException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.ProfileAssignmentHandler;
import pl.compan.docusafe.core.jbpm4.ProfileInDivisionAssignmentHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.sql.UserImpl;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Damian on 26.03.14.
 */
public class ForkPreparer implements ExternalActivityBehaviour {

    private static final String RECIPIENTS = "RECIPIENTS";
    private static final String FIRSTNAME = "FIRSTNAME";
    private static final String LASTNAME = "LASTNAME";
    private static final String DIVISION = "DIVISION";
    private static final String WNIOSKUJACY = "WNIOSKUJACY";
    private static final String WN_USER = WNIOSKUJACY + "_WN_USER";
    private static final String WN_DIVISION = WNIOSKUJACY + "_WN_DIVISION";
    private static final String WN_ID = WNIOSKUJACY + "_ID";

    private static final String TYPE_RECIPENT = "realizator";
    private static final String TYPE_APPLICANT = "wnioskujacy";

    public static final String SUPERVISORS_MAP_NAME = "applicantSupervisors";

    private String forkType;

    /**
     * Nazwa zmiennej jaka ma by� nadana dla kolekcji wykorzystanej do forka
     */
    private String forkVariableName;
    /**
     * Nazwa jaka ma by� nadana zmiennej do joina
     */
    private String forkCountName;

    /**
     * Nazwa addsa z id profilu po, kt�rym b�d� wybierani userzy z dzia��w
     */
    private String profileIdAdds;

    /**
     * Zakres przeszukiwania. Okre�la liczb� przeszukiwanych poziom�w w g�r� od danego dzia�u
     * -1 do root
     */
    private Integer scopeLvl = 0;



    @Override
    public void execute(ActivityExecution execution) throws Exception {

        if(StringUtils.isBlank(forkVariableName) || StringUtils.isBlank(forkCountName)){
            throw new JbpmException("Nie ustawiono nazwy zmiennej procesowej na etapie " + execution.getActivityName());
        }

        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");

        boolean isOpened = false;


        try {
            isOpened = DSApi.openContextIfNeeded();


            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();


            //Realizatorzy do krok�w 3, 5, 11
            if(TYPE_RECIPENT.equals(forkType)){
                Set<String> userNamesVar = new HashSet<String>();
                List<Recipient> recipients = doc.getRecipients();

                for(Recipient recipient : recipients){
                    DSUser user = extractUserFromRecipent(recipient);
                    if (user != null) {
                        userNamesVar.add(user.getName());
                    }else {
                        DSDivision div = extractDivisionFromRecipent(recipient);
                        if (div != null) {
                            List<String> userNames = new ArrayList<String>();
                            Long profileId = getProfileIdFromAdds(profileIdAdds);
                            ProfileInDivisionAssignmentHandler.putUsersFromProfile(userNames,div.getId(),profileId,false,scopeLvl,false);
                            userNamesVar.addAll(userNames);
                        }
                    }
                }
                if(userNamesVar.isEmpty()){
                    throw new UserNotFoundException("Nie znaleziono u�ytkownik�w, kt�rym mo�na by przypisa� zadanie po etapie " + execution.getActivityName());
                }else{
                    execution.setVariable(forkVariableName,userNamesVar);
                    execution.setVariable(forkCountName,userNamesVar.size());
                }
            }

            //Wnioskuj�cy do kroku 4
            else if(TYPE_APPLICANT.equals(forkType)){
                Set<String> userNamesVar = new HashSet<String>();
                List<Map<String, Object>> dicItems = FieldsManagerUtils.getDictionaryItems(fm, WNIOSKUJACY,WN_ID, WN_USER, WN_DIVISION);
                Map<String,Set<Object>> usersRows = new HashMap<String, Set<Object>>();

                for(Map<String, Object> applicantFields : dicItems){
                    Long divId = null;
                    String divStrId = ((EnumValues) applicantFields.get(WN_DIVISION)).getSelectedId();
                    if(divStrId != null && divStrId.matches("\\d+")){
                        divId = Long.parseLong(divStrId);
                    }else{
                        String userId = ((EnumValues) applicantFields.get(WN_USER)).getSelectedId();
                        if(userId != null && userId.matches("\\d+")){
                            DSUser user = DSUser.findById(Long.parseLong(userId));
                            DSDivision[] divisions = user.getOriginalDivisionsWithoutGroup();
                            if(divisions.length >0){
                                divId = divisions[0].getId();
                            }
                        }
                    }
                    if (divId != null) {
                        List<String> userNames = new ArrayList<String>();
                        Long profileId = getProfileIdFromAdds(profileIdAdds);
                        ProfileInDivisionAssignmentHandler.putUsersFromProfile(userNames,divId,profileId,false,scopeLvl,false);
                        userNamesVar.addAll(userNames);
                    }
                    for(String name : userNamesVar){
                        Set<Object> rowIds = null;
                        if(usersRows.containsKey(name)){
                            rowIds = usersRows.get(name);
                        }else{
                            rowIds = new HashSet<Object>();
                        }
                        if (rowIds != null) {
                            rowIds.add(applicantFields.get(WN_ID));
                            usersRows.put(name,rowIds);
                        }else{
                            throw new Exception("B��d przy przypisywaniu zadania u�ytkownikowi " + name + " na etapie: " + execution.getActivityName());
                        }
                    }
                }


                if(userNamesVar.isEmpty()){
                    throw new UserNotFoundException("Nie znaleziono u�ytkownik�w, kt�rym mo�na by przypisa� zadanie po etapie " + execution.getActivityName());
                }else{
                    execution.setVariable(forkVariableName,userNamesVar);
                    execution.setVariable(forkCountName,userNamesVar.size());
                    execution.setVariable(SUPERVISORS_MAP_NAME,usersRows);
                }
            }


            //ostateczny test czy przygotowano zmienn� do forka
            testIfPrepared(execution);
        }finally {
            DSApi.closeContextIfNeeded(isOpened);
        }
    }

    /**
     * Returns profile id from adds.properties from add with given name
     * @param profileIdAdds
     * @return profile id
     * @throws PropertyNotFoundException if there is no adds with given name
     */
    public static Long getProfileIdFromAdds(String profileIdAdds) throws PropertyNotFoundException {
        Long profileId;
        if (StringUtils.isNotBlank(profileIdAdds)){
            profileId = (long) Docusafe.getIntAdditionPropertyOrDefault(profileIdAdds, -1);
            if (profileId < 0){
                throw new PropertyNotFoundException("No adds=" + profileIdAdds);
            }
        } else{
            throw new PropertyNotFoundException("No profile id source");
        }

        return profileId;
    }


    /**
     * Extracts DSDivision from given recipent
     * @param recipient
     * @return extracted DSDivision or <Code>null</Code> if no division can be extracted
     * @throws EdmException
     * @throws NullPointerException if recipent is null
     */
    private DSDivision extractDivisionFromRecipent(Recipient recipient) throws EdmException {
        DSDivision div = null;
        String guid = extractGuidFromRecipent(recipient);
        if (guid != null) {
            div = DSDivision.find(guid);
        }
        if(div == null){
            String divName = recipient.getOrganizationDivision();
            div = DSDivision.findByName(divName);
        }
        return div;
    }

    /**
     * Extracts DSDivision guid from given recipent
     * @param recipient
     * @return extracted DSDivision guid or <Code>null</Code> if no guid can be extracted
     * @throws NullPointerException if recipent is null
     */
    private String extractGuidFromRecipent(Recipient recipient) {
        String guid = null;
        Pattern pat = Pattern.compile( ".*;d:(\\w*)");
        Matcher m = pat.matcher(recipient.getLparam());
        if(m.find()){
            guid = m.group(1);
        }
        return guid;
    }

    /**
     * Extracts DSUser from recipent
     * @param recipient
     * @return extracted DSUser or <Code>null</Code> if no DSUser can be extracted
     * @throws EdmException
     * @throws NullPointerException - if recipent is null
     */
    private DSUser extractUserFromRecipent(Recipient recipient) throws EdmException {
        if(recipient == null){
            throw new NullPointerException();
        }
        String firstName = recipient.getFirstname();
        String lastName = recipient.getLastname();

        DSUser user = DSUser.findByFirstnameLastname(firstName, lastName);
        String guid = extractGuidFromRecipent(recipient);
        if (user != null && guid != null && DSDivision.isInDivision(guid,user.getName())) {
            return user;
        }else if(guid != null && firstName != null && lastName != null){
            user = null;
            DSDivision div = DSDivision.find(guid);
            DSUser[] users = div.getUsers();
            for(DSUser u : users){
                if(firstName.equals(u.getFirstname()) && lastName.equals(u.getLastname())){
                    user = u;
                    break;
                }
            }
        }

        return user;
    }

    /**
     *Checks if fork variables are prepared
     *
     * @param execution
     * @throws EdmException if one of fork variable wasn't found in execution
     */
    private boolean testIfPrepared(ActivityExecution execution) throws EdmException {
        Object variable = execution.getVariable(forkVariableName);

        if(!(variable instanceof Collection) || ((Collection) variable).isEmpty()){
            throw new EdmException("Odpowiedni "+ forkType + " nie zosta� znaleziony.");
        }

        if(((Collection) variable).size() == execution.getVariable(forkCountName)){
            throw new JbpmException("B��d przygotowania zmiennej do 'multiplicity join'");
        }

        return true;

    }


    @Override
    public void signal(ActivityExecution activityExecution, String s, Map<String, ?> stringMap) throws Exception {

    }


    public Integer getScopeLvl() {
        return scopeLvl;
    }

    public void setScopeLvl(Integer scopeLvl) {
        this.scopeLvl = scopeLvl;
    }

    public String getProfileIdAdds() {
        return profileIdAdds;
    }

    public void setProfileIdAdds(String profileIdAdds) {
        this.profileIdAdds = profileIdAdds;
    }

    public String getForkCountName() {
        return forkCountName;
    }

    public void setForkCountName(String forkCountName) {
        this.forkCountName = forkCountName;
    }

    public String getForkVariableName() {
        return forkVariableName;
    }

    public void setForkVariableName(String forkVariableName) {
        this.forkVariableName = forkVariableName;
    }

    public String getForkType() {
        return forkType;
    }

    public void setForkType(String forkType) {
        this.forkType = forkType;
    }
}
