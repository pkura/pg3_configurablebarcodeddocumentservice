package pl.compan.docusafe.parametrization.uek.jbpm;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.DateField;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Damian on 28.03.14.
 */
public class ReceiptDateHandler implements ExternalActivityBehaviour {
    private final String RECEIPT_DATE = "RECEIPT_DATE";
    private final String WNIOSKUJACY = "WNIOSKUJACY";
    private final String DATA_ODBIORU = WNIOSKUJACY + "_DATA_ODBIORU";
    private final String WN_ID = WNIOSKUJACY + "_ID";

    @Override
    public void execute(ActivityExecution execution) throws Exception {

        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");

        boolean wasOpened = false;


        try {
            wasOpened = DSApi.openContextIfNeeded();

            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();


            String userName = (String)execution.getVariable("currentAssignee");

            Set<Object> wnioskujacyIds = ((Map<String,Set<Object>>) execution.getVariable(ForkPreparer.SUPERVISORS_MAP_NAME)).get(userName);
            List<Map<String, Object>> dicItems = FieldsManagerUtils.getDictionaryItems(fm, WNIOSKUJACY,WN_ID, DATA_ODBIORU);

            for(Map<String,Object> item : dicItems){
                if(wnioskujacyIds.contains(item.get(WN_ID))){
                    if(!(item.get(DATA_ODBIORU) instanceof Date)){
                        throw new Exception("Nie wszystkie wymagane pola daty odbioru zosta�y wype�nione");
                    }
                }
            }


            boolean throwEx = false;
            if(throwEx){
                throw new Exception("Nie wszystkie wymagane pola daty odbioru zosta�y wype�nione");
            }



        }finally {
            DSApi.closeContextIfNeeded(wasOpened);
        }

    }




    @Override
    public void signal(ActivityExecution activityExecution, String s, Map<String, ?> stringMap) throws Exception {

    }
}
