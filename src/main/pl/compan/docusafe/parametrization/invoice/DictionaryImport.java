package pl.compan.docusafe.parametrization.invoice;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;

import java.rmi.RemoteException;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 12.08.13
 * Time: 13:24
 * To change this template use File | Settings | File Templates.
 */
public interface DictionaryImport {
    /**
     * Inicjalizuje ustawienia axisa. Wywo�ywane raz podczas tworzenia timera.
     * @param conf
     * @throws Exception
     */
    void initConfiguration(AxisClientConfigurator conf) throws Exception;
    void initImport();

    /**
     * Metoda w�asciwa synchronizacji
     * @return true, je�li import dobieg� ko�ca / false, jesli nale�y ponowi�
     * @throws java.rmi.RemoteException
     * @throws pl.compan.docusafe.core.EdmException
     */
    boolean doImport() throws RemoteException, EdmException;

    String getMessage();

    /**
     *
     * @return true, je�li wymagana jest przerwa, pomi�dzy wywo�aniami doImport()
     */
    boolean isSleepAfterEachDoImport();
    void finalizeImport() throws EdmException;
    void materializeView() throws EdmException, SQLException;
}
