package pl.compan.docusafe.parametrization.invoice;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;

import java.sql.SQLException;


public abstract class AbstractDictionaryWithoutWebserviceImport implements DictionaryImport {

    @Override
    public void initImport() {
    }

    @Override
    public void initConfiguration(AxisClientConfigurator conf) throws Exception {
        return;
    }

    @Override
    public String getMessage() {
        return null;
    }

    @Override
    public boolean isSleepAfterEachDoImport() {
        return false;
    }

    @Override
    public void finalizeImport() throws EdmException {
    }

    @Override
    public void materializeView() throws EdmException, SQLException {
    }
}
