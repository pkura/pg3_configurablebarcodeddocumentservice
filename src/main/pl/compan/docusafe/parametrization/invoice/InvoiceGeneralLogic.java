package pl.compan.docusafe.parametrization.invoice;

import static pl.compan.docusafe.core.dockinds.field.DataBaseEnumField.REPO_REF_FIELD_PREFIX;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.Collator;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContext;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.Field.Type;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.SessionUtils;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.NormalLogic;
import pl.compan.docusafe.core.imports.simple.repository.CacheHandler;
import pl.compan.docusafe.core.imports.simple.repository.Dictionary;
import pl.compan.docusafe.core.imports.simple.repository.DictionaryMap;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.general.canonical.factory.GeneralCacheHandler;
import pl.compan.docusafe.general.canonical.model.BudzetPozycja;
import pl.compan.docusafe.general.canonical.model.Faktura;
import pl.compan.docusafe.general.canonical.model.GeneralDictionary;
import pl.compan.docusafe.general.canonical.model.GeneralSimpleRepositoryAttribute;
import pl.compan.docusafe.general.canonical.model.Kontrahent;
import pl.compan.docusafe.general.canonical.model.PozycjaFaktury;
import pl.compan.docusafe.general.canonical.model.StawkaVat;
import pl.compan.docusafe.general.canonical.model.WydatekStrukturalny;
import pl.compan.docusafe.general.hibernate.dao.BudzetKOKomorkaDBDAO;
import pl.compan.docusafe.general.hibernate.dao.FakturaDBDAO;
import pl.compan.docusafe.general.hibernate.dao.FakturaPozycjaDBDAO;
import pl.compan.docusafe.general.hibernate.dao.NumeryZamowienDBDAO;
import pl.compan.docusafe.general.hibernate.dao.PlanZakupowDBDAO;
import pl.compan.docusafe.general.hibernate.dao.PozycjaZamowieniaFakturaDBDAO;
import pl.compan.docusafe.general.hibernate.dao.ProjectAccountPiatkiDBDAO;
import pl.compan.docusafe.general.hibernate.dao.ProjectDBDAO;
import pl.compan.docusafe.general.hibernate.dao.SourcesOfProjectFundingDBDAO;
import pl.compan.docusafe.general.hibernate.dao.WniosekORezerwacjeDBDAO;
import pl.compan.docusafe.general.hibernate.dao.WniosekORezerwacjePozycjaDBDAO;
import pl.compan.docusafe.general.hibernate.dao.ZamowienieSimplePozycjeDBDAO;
import pl.compan.docusafe.general.hibernate.dao.ZrodloDBDAO;
import pl.compan.docusafe.general.hibernate.model.BudzetPozycjaDB;
import pl.compan.docusafe.general.hibernate.model.CostInvoiceProductDB;
import pl.compan.docusafe.general.hibernate.model.FakturaDB;
import pl.compan.docusafe.general.hibernate.model.FakturaPozycjaDB;
import pl.compan.docusafe.general.hibernate.model.KontrahentDB;
import pl.compan.docusafe.general.hibernate.model.NumeryZamowienDB;
import pl.compan.docusafe.general.hibernate.model.PlanZakupowDB;
import pl.compan.docusafe.general.hibernate.model.PozycjaZamowieniaFakturaDB;
import pl.compan.docusafe.general.hibernate.model.ProjectAccountPiatkiDB;
import pl.compan.docusafe.general.hibernate.model.ProjectDB;
import pl.compan.docusafe.general.hibernate.model.PurchaseDocumentTypeDB;
import pl.compan.docusafe.general.hibernate.model.UnitOfMeasureDB;
import pl.compan.docusafe.general.hibernate.model.VatRateDB;
import pl.compan.docusafe.general.hibernate.model.WniosekORezerwacjeDB;
import pl.compan.docusafe.general.hibernate.model.WniosekORezerwacjePozycjaDB;
import pl.compan.docusafe.general.hibernate.model.ZamowienieSimplePozycjeDB;
import pl.compan.docusafe.general.hibernate.model.ZrodloDB;
import pl.compan.docusafe.general.mapers.FMToModelMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.parametrization.utp.OfficeUtils;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import uk.ltd.getahead.dwr.WebContextFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import edu.emory.mathcs.backport.java.util.Arrays;


/**
 * 
 * logika faktury 
 * constans InvoiceGeneralConstans (interfejs)
 * 
 */
public class InvoiceGeneralLogic extends AbstractDocumentLogic implements InvoiceGeneralConstans{

	private static NormalLogic instance;

	StringManager sm = GlobalPreferences.loadPropertiesFile(InvoiceGeneralLogic.class.getPackage().getName(), null);
	protected static Logger log = LoggerFactory.getLogger(InvoiceGeneralLogic.class);
	public static NormalLogic getInstance() {
		if (instance == null)
			instance = new NormalLogic();
		return instance;
	}


	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms) {
			if (perm.isCreateGroup()
					&& perm.getSubjectType().equalsIgnoreCase(
							ObjectPermission.GROUP)) {
				String groupName = perm.getGroupName();
				if (groupName == null) {
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {
		//jesli faktury bedzie zapisywalo w katalogu faktur
/*		Folder folder = Folder.getRootFolder();
    	folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
    	document.setFolder(folder);
*/
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type)
			throws EdmException {
		Map<String, Object> values = new HashMap<String, Object>();
		values.put(DATA_WPLYWU_CN, new Date(System.currentTimeMillis()));
		fm.reloadValues(values);
		super.setInitialValues(fm, type);

	}

	@Override
	public Map<String, Object> setMultipledictionaryValue(
			String dictionaryName, Map<String, FieldData> values,
			Map<String, Object> fieldsValues, Long documentId) {
		Map<String, Object> results = new HashMap<String, Object>();

		try {
			if(fieldsValues.containsKey(RODZAJFAKTURY_CN)&&fieldsValues.get(RODZAJFAKTURY_CN)!=null){
				//narazie tylko projektowa i kosztowa ma pola vat, netto i brutto na slowniku, zeby moc sprawdzac z naglowkiem faktury TODO dodac fakture magazynowa
				if(fieldsValues.get(RODZAJFAKTURY_CN).toString().equals(RodzajeFaktury.FAKTKOSZT.zwrocId().toString())
						||fieldsValues.get(RODZAJFAKTURY_CN).toString().equals(RodzajeFaktury.FAKTPROJEKT.zwrocId().toString())
						||fieldsValues.get(RODZAJFAKTURY_CN).toString().equals(RodzajeFaktury.FAKTMAG.zwrocId().toString())){
					if(dictionaryName.equals(POZYCJE_FAKTURY_CN)){
                        String dictName=POZYCJE_FAKTURY_CN+"_";
                        FieldData netto=values.get(POZYCJE_FAKTURY_CN+"_"+POZYCJE_FAKTURY_KWOTANETTO_CN);
						FieldData vat=values.get(POZYCJE_FAKTURY_CN+"_"+POZYCJE_FAKTURY_KWOTAVAT_CN);
						FieldData stawka=values.get(POZYCJE_FAKTURY_CN+"_"+POZYCJE_FAKTURY_STAWKA_CN);
						if(netto.getData()!=null&&stawka.getData()!=null){
							EnumValues ev=stawka.getEnumValuesData();
							if(ev.getSelectedId()!=null&&!ev.getSelectedId().equals("")){
								DataBaseEnumField dbef=DataBaseEnumField.getEnumFiledForTable(WIDOK_SIMPLE_VAT);
								EnumItem e=dbef.getEnumItem(Integer.parseInt(ev.getSelectedId()));
								BigDecimal stawkaProcentowa=new BigDecimal(e.getCn());
								stawkaProcentowa=stawkaProcentowa.multiply(new BigDecimal(0.01));
								stawkaProcentowa=stawkaProcentowa.multiply(netto.getMoneyData());
								stawkaProcentowa=stawkaProcentowa.setScale(4,RoundingMode.HALF_UP);
								vat.setMoneyData(stawkaProcentowa);
								results.put(POZYCJE_FAKTURY_CN+"_"+POZYCJE_FAKTURY_KWOTAVAT_CN, stawkaProcentowa);
							}
						}

						if(netto.getData()!=null&&vat.getData()!=null){
							BigDecimal brutto=netto.getMoneyData().setScale(4, RoundingMode.HALF_UP);
							brutto=brutto.add(vat.getMoneyData());
							results.put(POZYCJE_FAKTURY_CN+"_"+POZYCJE_FAKTURY_KWOTABRUTTO_CN, brutto);
						}
					}
					if(dictionaryName.equals(MPK_CN)){
						String dictName=MPK_CN+"_";
						FieldData netto=values.get(dictName+MPK_KWOTANETTO_CN);
						FieldData stawkaVat=values.get(dictName+MPK_STAWKA_VAT);
						FieldData vat=values.get(dictName+MPK_KWOTAVAT_CN);
						EnumValues budSelectId=values.get(dictName+MPK_BUDZET_CN).getEnumValuesData();
						if(budSelectId.getSelectedId() != null){
						Map<String, Object> sessionValues = new HashMap<String,Object>();
						sessionValues.put("BUDZETID", budSelectId.getSelectedId());
						SessionUtils.putToSession(documentId, "MPKBUDID", sessionValues);
						}
						if(netto.getData()!=null&&stawkaVat.getData()!=null&&vat.getData()==null){
							EnumValues ev=stawkaVat.getEnumValuesData();
							if(ev.getSelectedId()!=null&&!ev.getSelectedId().equals("")){
								DataBaseEnumField dbef=DataBaseEnumField.getEnumFiledForTable(WIDOK_SIMPLE_VAT);
								EnumItem e=dbef.getEnumItem(Integer.parseInt(ev.getSelectedId()));
								BigDecimal stawkaProcentowa=new BigDecimal(e.getCn());
								stawkaProcentowa=stawkaProcentowa.multiply(new BigDecimal(0.01));
								stawkaProcentowa=stawkaProcentowa.multiply(netto.getMoneyData());
								stawkaProcentowa=stawkaProcentowa.setScale(4,RoundingMode.HALF_UP);
								vat.setMoneyData(stawkaProcentowa);
								results.put(dictName+MPK_KWOTAVAT_CN, stawkaProcentowa);								
							}
						}
						results=ustawPolaZalezne(dictName, values,
								fieldsValues, documentId,results);
					}

					if(dictionaryName.equals(POZYCJE_FAKTURY_CN)){
						Boolean czyWniosek=false;
						if(fieldsValues.containsKey(CZYZWNIOSKU_CN)&&fieldsValues.get(CZYZWNIOSKU_CN)!=null){
							if(fieldsValues.get(CZYZWNIOSKU_CN).equals(true))czyWniosek=true;
						}
						
						alboZlecenieAlboFundusz(values,results,czyWniosek);
						fillRepositoryDictionaries(values, results);
						results=ustawPolaZalezne(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD, values,
								fieldsValues, documentId,results);
						results=wyczyscZeWzglNaKontoFunduszu(values,fieldsValues, documentId,results);
					}
					if(dictionaryName.equals(POZYCJE_ZAMOW_CN)){
						Map<String, Object> sessionValues = SessionUtils.getFromSession(documentId, POZYCJE_ZAMOW_CN, new HashMap<String, Object>());
						if(sessionValues.size()>0){
							String dictName=POZYCJE_ZAMOW_CN+"_";
							Long id=values.get(dictName+POZYCJE_ZAMOW__ID_CN).getLongData();
							BigDecimal iloscZreal=values.get(dictName+POZYCJE_ZAMOW_ILOSC_ZREAL_CN).getMoneyData();
							((PozycjaZamowieniaFakturaDB)sessionValues.get(dictName+id)).setILOSC_ZREAL(iloscZreal);
						}

					//	PozycjaZamowieniaFakturaDB pozZamDB=PozycjaZamowieniaFakturaDBDAO.findById(id);
					//	pozZamDB.setILOSC_ZREAL(iloscZreal);
					//	PozycjaZamowieniaFakturaDBDAO.update(pozZamDB);
					}
					
					if (dictionaryName.equals(WYDATEKSTRUKTPOZ_CN)) {
						results = ustawPolaZalezne(WYDATEKSTRUKTPOZ_CN + "_", values, fieldsValues, documentId, results);
					}
				}
			} 

		} catch (EdmException e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}


		return results;
	}


	private void alboZlecenieAlboFundusz(Map<String, FieldData> values,
			Map<String, Object> results,Boolean czyWniosek) {
		try {
			//fundusz moze byc albo zawezony o zrodlo( w wypadku faktury rozliczanej z wniosku) albo on zawezac zrodlo  - 2 rozne pola
			String funduszWlasciwy="";
		//	jesli zawezanie nie bedzie potrzebne, blad wystarczy, to nie jest potrzebne
		//  if(czyWniosek)funduszWlasciwy=POZYCJE_FAKTURY_PROJEKT_DO_ZRODLA_CN;
		//	else
			czyWniosek=false;//powrot do staryego zrodla
				funduszWlasciwy=POZYCJE_FAKTURY_PROJEKT_CN;
			if(values.containsKey(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+POZYCJE_FAKTURY_ZLECENIE_CN)&&values.containsKey(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+funduszWlasciwy)){
				
				
				FieldData zlecenie=values.get(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+POZYCJE_FAKTURY_ZLECENIE_CN);
				FieldData fundusz=values.get(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+funduszWlasciwy);
				EnumValues evFundusz=fundusz.getEnumValuesData();
				EnumValues evZlecenie=zlecenie.getEnumValuesData();
				if((evZlecenie.getSelectedId()==null||evZlecenie.getSelectedId().equals("")||("0").equals(evZlecenie.getSelectedId()))&&evFundusz.getSelectedId()!=null&&!evFundusz.getSelectedId().equals("")&&!("0").equals(evFundusz.getSelectedId())){
					EnumValues evDoWst=new EnumValues();
					if(czyWniosek)evDoWst.setAllOptions(zwrocListeZTabeli(DataBaseEnumField.getEnumFiledForTable(WIDOK_POZYCJE_PPROJEKT_PO_ZRODLE).getAvailableItems()));
					else evDoWst.setAllOptions(zwrocListeZTabeli(DataBaseEnumField.getEnumFiledForTable(WIDOK_POZYCJE_FUNDUSZ).getAvailableItems()));

					evDoWst.setSelectedId(evFundusz.getSelectedId());

					results.put(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+funduszWlasciwy, evDoWst);
					evDoWst=new EnumValues();
					List<Map<String,String>>pustaLista = new ArrayList<Map<String,String>>();
					evDoWst.setAllOptions(pustaLista);
					HashMap< String, String> mapa=new HashMap<String, String>();
					mapa.put("0", "-- Wybrano fundusz - pozycja niedost瘼na--");
					pustaLista.add(mapa);
					evDoWst.setSelectedId("0");
					results.put(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+POZYCJE_FAKTURY_ZLECENIE_CN, evDoWst);
				}
				else if((evFundusz.getSelectedId()==null||evFundusz.getSelectedId().equals("")||("0").equals(evFundusz.getSelectedId()))&&evZlecenie.getSelectedId()!=null&&!evZlecenie.getSelectedId().equals("")&&!("0").equals(evZlecenie.getSelectedId())){
					EnumValues evDoWst=new EnumValues();
					evDoWst.setAllOptions(zwrocListeZTabeli(DataBaseEnumField.getEnumFiledForTable(WIDOK_POZYCJE_ZLECENIA).getAvailableItems()));

					evDoWst.setSelectedId(evZlecenie.getSelectedId());

					results.put(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+POZYCJE_FAKTURY_ZLECENIE_CN, evDoWst);

					evDoWst=new EnumValues();
					List<Map<String,String>>pustaLista = new ArrayList<Map<String,String>>();
					evDoWst.setAllOptions(pustaLista);
					HashMap< String, String> mapa=new HashMap<String, String>();
					mapa.put("0", "-- Wybrano zlecenie - pozycja niedost瘼na--");
					pustaLista.add(mapa);
					evDoWst.setSelectedId("0");
					results.put(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+funduszWlasciwy, evDoWst);
				}
				else{
					EnumValues evDoWst=new EnumValues();
					evDoWst.setAllOptions(zwrocListeZTabeli(DataBaseEnumField.getEnumFiledForTable(WIDOK_POZYCJE_ZLECENIA).getAvailableItems()));	

					results.put(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+POZYCJE_FAKTURY_ZLECENIE_CN, evDoWst);

					evDoWst=new EnumValues();
					if(czyWniosek)evDoWst.setAllOptions(zwrocListeZTabeli(DataBaseEnumField.getEnumFiledForTable(WIDOK_POZYCJE_PPROJEKT_PO_ZRODLE).getAvailableItems()));
					else evDoWst.setAllOptions(zwrocListeZTabeli(DataBaseEnumField.getEnumFiledForTable(WIDOK_POZYCJE_FUNDUSZ).getAvailableItems()));

					results.put(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+funduszWlasciwy, evDoWst);

				}
			}
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
			e.printStackTrace();
		}

	}


	private List<Map<String, String>> zwrocListeZTabeli(
			Collection<EnumItem> availableItems) {
		List<Map<String,String>>itemy = new ArrayList<Map<String,String>>();
		HashMap< String, String> mapa=new HashMap<String, String>();
		mapa.put("", "--wybierz--");
		itemy.add(mapa);
		for(EnumItem e:availableItems){
			mapa=new HashMap<String, String>();
			mapa.put(e.getId().toString(), e.getTitle());
			itemy.add(mapa);
		}
		return itemy;
	}


	protected Set<String> getRepositoryDictsIds(Integer docTypeObjectId, Integer contextId) {
		List<GeneralSimpleRepositoryAttribute> repositoryDicts = null;
		try {
			repositoryDicts = GeneralSimpleRepositoryAttribute.find(docTypeObjectId, contextId);
		} catch (EdmException e) {
			log.error("", e);
		}
		Set<String> repositoryDictsIds = Sets.newHashSet();
		for (GeneralSimpleRepositoryAttribute repositoryDict : repositoryDicts) {
			repositoryDictsIds.add(repositoryDict.getRepKlasaId().toString());
		}
		return repositoryDictsIds;
	}

	private Integer getDocTypeId() {
		WebContext dwrFactory = WebContextFactory.get();
		if (dwrFactory.getSession().getAttribute("dwrSession") instanceof Map<?, ?>) {
			Map<String, Object> dwrSession = (Map<String, Object>) dwrFactory.getSession().getAttribute("dwrSession");
			if (dwrSession.get("FIELDS_VALUES") instanceof Map<?, ?>) {
				Map<String,Object> fieldsValues = (Map<String, Object>) dwrSession.get("FIELDS_VALUES");

				Object docType = null;
				if(fieldsValues.get(TYP_FAKTURY_PLN_CN) != null){
					docType = fieldsValues.get(TYP_FAKTURY_PLN_CN);
				} else {
					docType = fieldsValues.get(TYP_FAKTURY_WAL_CN);
				}
				if (docType != null) {
					try {
						return Integer.valueOf(docType.toString());
					} catch (NumberFormatException e) {
					}
				}
			}
		}
		return null;
	}

	private void fillRepositoryDictionaries(Map<String, FieldData> values, Map<String, Object> results) throws EdmException {
		Integer docTypeId = getDocTypeId();

		if (docTypeId != null) {
			EnumItem docTypeEnum = DataBaseEnumField.getEnumItemForTable(PurchaseDocumentTypeDB.ENUM_VIEW_TABLE, docTypeId);

			clearAllDictionaresFields(DOC_KIND_CN, results);

			if (docTypeEnum != null) {
				Integer docTypeObjectId = docTypeEnum.getId();

				Set<String> repositoryDictsIds = getRepositoryDictsIds(docTypeObjectId, (int) REPOSITORY_POSITION_CONTEXT_ID);

				for (GeneralDictionary dict : GeneralCacheHandler.getAvailableDicts(DOC_KIND_CN)) {
					if (!dict.getDeleted() && dict.getDictSource() == Dictionary.SourceType.REPOSITORY.value) {
						setRepositoryDict(values, results, POZYCJE_FAKTURY_WITH_SUFFIX_FIELD, docTypeObjectId, (int) REPOSITORY_POSITION_CONTEXT_ID, repositoryDictsIds,
								null, EMPTY_ENUM_VALUE, dict, log);
					}
					if(dict.getCn().equals(SLOWNIK_DYNAMICZNY_ZADANIA_PROJEKTOWE)&&values.get(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+POZYCJE_FAKTURY_PROJEKT_CN)!=null
							&&values.get(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+POZYCJE_FAKTURY_PROJEKT_CN).getEnumValuesData()!=null
							&&values.get(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+POZYCJE_FAKTURY_PROJEKT_CN).getEnumValuesData().getSelectedId()!=null
							&&!values.get(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+POZYCJE_FAKTURY_PROJEKT_CN).getEnumValuesData().getSelectedId().equals("")){

						FieldData projekt=values.get(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+POZYCJE_FAKTURY_PROJEKT_CN);
						String id=projekt.getEnumValuesData().getSelectedId();
						DataBaseEnumField d=DataBaseEnumField.getEnumFiledForTable(WIDOK_MPK_PROJEKT);
						if(id!=null&&!id.equals("")){
							EnumItem eip=d.getEnumItem(id);
							String projektNazwa=eip.getCn();
							EnumValues ev=(EnumValues) results.get(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+SLOWNIK_DYNAMICZNY_ZADANIA_PROJEKTOWE);
							String idZaznaczonego="";
							boolean czyJestWNowych=false;
							if(ev.getSelectedId()!=null){
								idZaznaczonego=ev.getSelectedId();
							}
							List<Map<String,String>>itemy=(List<Map<String,String>>) ev.getAllOptions();
							List<Map<String,String>>zawezoneOProjekt=new ArrayList<Map<String,String>>();
							for(Map<String,String> ei:itemy){
								Map<String,String> item=new HashMap<String,String>();
								for(String key:ei.keySet()){
									if(ei.get(key).length()>10&&!key.equals("")){
										if(ei.get(key).substring(0, 10).equals(projektNazwa.substring(0, 10))){
											item.put(key,ei.get(key));
											zawezoneOProjekt.add(item);
											if(key.equals(idZaznaczonego))czyJestWNowych=true;
										}
										
									}
									if(key.equals("")){
										item.put(key, ei.get(key));
										zawezoneOProjekt.add(item);
									}
									
								}
								
							}

							EnumValues evDoWst=new EnumValues();
							evDoWst.setAllOptions(zawezoneOProjekt);
							evDoWst.setSelectedId(id);
							if(czyJestWNowych)evDoWst.setSelectedId(idZaznaczonego);
							results.put(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+SLOWNIK_DYNAMICZNY_ZADANIA_PROJEKTOWE, evDoWst);
						}
					}
					else if(dict.getCn().equals(SLOWNIK_DYNAMICZNY_ZADANIA_PROJEKTOWE)&&values.get(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+POZYCJE_FAKTURY_PROJEKT_CN)!=null
							&&values.get(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+POZYCJE_FAKTURY_PROJEKT_CN).getEnumValuesData()!=null
							&&(values.get(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+POZYCJE_FAKTURY_PROJEKT_CN).getEnumValuesData().getSelectedId()==null
							||values.get(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+POZYCJE_FAKTURY_PROJEKT_CN).getEnumValuesData().getSelectedId().equals(""))){
						EnumValues evDoWst=new EnumValues();
						List<Map<String,String>>pustaLista = new ArrayList<Map<String,String>>();
						evDoWst.setAllOptions(pustaLista);
						evDoWst.setSelectedId("");
						results.put(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+SLOWNIK_DYNAMICZNY_ZADANIA_PROJEKTOWE, evDoWst);
					}
				}
			}
		}
	}

	private void clearAllDictionaresFields(String docKindCn, Map<String, Object> results) {
		for (Dictionary dictionaryToClear : CacheHandler.getAvailableDicts(docKindCn)) {
			results.put(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD + dictionaryToClear.getCn(), EMPTY_ENUM_VALUE);
		}
	}

	protected void setRepositoryDict(Map<String, FieldData> values, Map<String, Object> results, String dicNameWithSuffix,
			Integer docTypeObjectId, Integer contextId, Set<String> repositoryDictsIds, DictionaryMap dictMap, EnumValues defaultEnum, GeneralDictionary dict, Logger log) {
		if (dictMap != null && dictMap.isShowAllReferenced()) {
			DwrUtils.setRefValueEnumOptions(values, results, DataBaseEnumField.REPO_REF_FIELD_PREFIX + dict.getCn(), dicNameWithSuffix, dict.getCn(),
					dict.getTablename(), DataBaseEnumField.REF_ID_FOR_ALL_FIELD, defaultEnum);
		} else {
			if (repositoryDictsIds.contains(dict.getCode())) {
				try {
					Long dictObjectId = Long.valueOf(dict.getCode());
					GeneralSimpleRepositoryAttribute attr = null;
					try {
						attr = GeneralSimpleRepositoryAttribute.find(dictObjectId, docTypeObjectId, contextId);
					} catch (EdmException e) {
						log.error("", e);
					}
					if (attr != null) {
						DwrUtils.setRefValueEnumOptions(values, results, REPO_REF_FIELD_PREFIX + dict.getCn(), dicNameWithSuffix, dict.getCn(),
								dict.getTablename(), attr.getRepAtrybutId().toString(), defaultEnum);
					}
				} catch (NumberFormatException e) {
					log.error("CAUGHT: "+e.getMessage(),e);
				}
			}
		}
	}

	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
			 {

		boolean ctx=false;
		StringBuilder msgBuilder = new StringBuilder();
		boolean bladPozycjeZam=false;
		boolean pozycjeZamJuzZaakceptowane = false;
		Faktura f=FMToModelMapper.zwrocModelZValuesDWR(values);
		try{
		//uzupelnianie vat/netto/brutto w zaleznosci od 2 podanych, walidacja brutto
		if(f.getKwotaNetto()!=null&&f.getKwotaBrutto()!=null&&f.getKwotaVat()==null){
			f.setKwotaVat(f.getKwotaBrutto().subtract(f.getKwotaNetto()));
		}
		else if(f.getKwotaBrutto()!=null&&f.getKwotaVat()!=null&&f.getKwotaNetto()==null){
			f.setKwotaNetto(f.getKwotaBrutto().subtract(f.getKwotaVat()));
		}
		else if(f.getKwotaVat()!=null&&f.getKwotaNetto()!=null&&f.getKwotaBrutto()==null){
			f.setKwotaBrutto(f.getKwotaNetto().add(f.getKwotaVat()));
		}
		if(f.getKwotaNetto()!=null&&f.getKwotaBrutto()!=null&&f.getKwotaVat()!=null){
			BigDecimal bd=new BigDecimal(f.getKwotaNetto().toString());
			bd=bd.add(f.getKwotaVat());
			f.setKwotaBrutto(bd);
			f.setKwotaPozostala(bd);
		}
		if(f.getKwotaNetto()!=null){
			f.setKwotaPozostalaNetto(f.getKwotaNetto());
		}
		if(f.getKwotaVat()!=null){
			f.setKwotaPozostalaVAT(f.getKwotaVat());
		}
		//wypelnianie kursu, jesli wybrana waluta jest PLN
		if(f.getWaluta()!=null){
			if(f.getWaluta().getId().toString().equals(WALUTA_PLN_ID)){
				f.setKurs(new BigDecimal(1));
			}
		}
		if(f.getKurs()!=null&&f.getKwotaNetto()!=null&&f.getKurs().compareTo(new BigDecimal(0.0))!=0){
			f.setKwotaWWalucie(f.getKwotaNetto().divide(f.getKurs(),2,RoundingMode.HALF_UP));
		}
		//zawezenie zamowien po wystawcy
		if(f.getWystawcaFaktury()!=null){
			SessionUtils.putToSession(fm.getDocumentId(), NRZAMOWIENIA_CN+"_ERPID", f.getWystawcaFaktury().getErpId());
		}
		else SessionUtils.putToSession(fm.getDocumentId(), NRZAMOWIENIA_CN+"_ERPID", null);


		
		

		if(f.getPozycjeFaktury()!=null){
			List<PozycjaFaktury> pozycjeFaktury=f.getPozycjeFaktury();
			for(PozycjaFaktury pf:pozycjeFaktury){
				if(pf.getKwotaNetto()!=null){
					f.setKwotaPozostalaNetto(f.getKwotaPozostalaNetto().subtract(pf.getKwotaNetto()));
				}
				if(pf.getKwotaNetto()!=null&&pf.getStawkaVat()!=null){
					ctx=DSApi.openContextIfNeeded();
					StawkaVat sv=pf.getStawkaVat();
					DataBaseEnumField dbef=DataBaseEnumField.getEnumFiledForTable(WIDOK_SIMPLE_VAT);
					EnumItem e=dbef.getEnumItem(Long.toString(sv.getId()));
					BigDecimal stawkaProcentowa=new BigDecimal(e.getCn());
					stawkaProcentowa=stawkaProcentowa.multiply(new BigDecimal(0.01));
					stawkaProcentowa=stawkaProcentowa.multiply(pf.getKwotaNetto());
					stawkaProcentowa=stawkaProcentowa.setScale(4,RoundingMode.HALF_UP);
					f.setKwotaPozostala(f.getKwotaPozostala().subtract(stawkaProcentowa.add(pf.getKwotaNetto())));
					f.setKwotaPozostalaVAT(f.getKwotaPozostalaVAT().subtract(stawkaProcentowa));
					DSApi.closeContextIfNeeded(ctx);
					ctx=false;
				}
			}
		}
		
		if((f.getCzyZZamowienia()!=null&&f.getCzyZZamowienia())||RodzajeFaktury.FAKTMAG.zwrocId().equals(f.getRodzajFaktury())){
			if(f.getDocumentIdZamowienia()!=null&&(values.get("DWR_"+NRZAMOWIENIA_CN).isSender())){
				if(f.getZAAKCEPTOWANOPOZZAM()!=null&&f.getZAAKCEPTOWANOPOZZAM()){
					ctx=DSApi.openContextIfNeeded();
					pozycjeZamJuzZaakceptowane=true;
					Map<String,FieldData>pozycjeZamowienia=((FieldData)((HashMap)((HashMap)WebContextFactory.get().getSession().getAttribute(DwrFacade.DWR_SESSION_NAME)).get("SELECTED_FIELDS_VALUES")).get("DWR_POZYCJE_ZAMOW")).getDictionaryData();
					int i=1;
					Long stareZamowienie=null;
					if(pozycjeZamowienia.containsKey(POZYCJE_ZAMOW_CN+"_"+POZYCJE_ZAMOW__ID_CN+"_"+i)&&pozycjeZamowienia.get(POZYCJE_ZAMOW_CN+"_"+POZYCJE_ZAMOW__ID_CN+"_"+i)!=null){
						Long id=pozycjeZamowienia.get(POZYCJE_ZAMOW_CN+"_"+POZYCJE_ZAMOW__ID_CN+"_"+i).getLongData();	
						PozycjaZamowieniaFakturaDB pozycja=PozycjaZamowieniaFakturaDBDAO.findById(id);
						NumeryZamowienDB zamowienieStare=NumeryZamowienDBDAO.findByErpIDUnique(pozycja.getDOCUMENT_ID_ZAMOWIENIA());
						if(zamowienieStare!=null)
							stareZamowienie=zamowienieStare.getId();
					}
					
					FieldData numerZamowienia = values.get("DWR_"+NRZAMOWIENIA_CN); 
					if(stareZamowienie!=null)numerZamowienia.getEnumValuesData().setSelectedId(stareZamowienie.toString());
					DSApi.closeContextIfNeeded(ctx);
					ctx=false;
				}
				else{
				ctx=DSApi.openContextIfNeeded();
				NumeryZamowienDB zamowienie=NumeryZamowienDBDAO.findByErpIDUnique(f.getDocumentIdZamowienia());
				//usuniecie starych
				
				if(zamowienie!=null){
					List<ZamowienieSimplePozycjeDB> zamowieniaSimpleDB=ZamowienieSimplePozycjeDBDAO.findBySupplierOrderId(zamowienie.getErpId());

					List<PozycjaZamowieniaFakturaDB> pozycjeWlasciwe=new ArrayList<PozycjaZamowieniaFakturaDB>();
					for(ZamowienieSimplePozycjeDB pozDB:zamowieniaSimpleDB){
						pozycjeWlasciwe.add(convertZamowienieSimplePozycjeDBToPozycjaZamowieniaDB(fm.getDocumentId(),pozDB,zamowienie));
					}
					
					Map<String, Object> sessionValues = new HashMap<String,Object>();
							//SessionUtils.getFromSession(fm.getDocumentId(), POZYCJE_ZAMOW_CN, new HashMap<String, Object>());
					for(PozycjaZamowieniaFakturaDB pozDB:pozycjeWlasciwe){
						
						List<PozycjaZamowieniaFakturaDB>tmp = PozycjaZamowieniaFakturaDBDAO.findByDocumentIdAndDocZamId(fm.getDocumentId(),pozDB.getZAMPOZ_ID());
						if(tmp.size()>0){
							pozDB.setID(tmp.get(0).getID());
						}
						else{
							PozycjaZamowieniaFakturaDBDAO.save(pozDB);
						}
						sessionValues.put(POZYCJE_ZAMOW_CN+"_"+pozDB.getID().toString(), pozDB);
						
					//	sessionValues.put(POZYCJE_BUDZETOWE_POZYCJA_PLANU_ZAKUPOW_CN, values.get(prefix+POZYCJE_BUDZETOWE_POZYCJA_PLANU_ZAKUPOW_CN));
						
					}
					SessionUtils.putToSession(fm.getDocumentId(), POZYCJE_ZAMOW_CN, sessionValues);
					FieldData zamowieniaPozycje = values.get("DWR_"+POZYCJE_ZAMOW_CN); 
					Map<String, FieldData> mapa = new HashMap<String, FieldData>();
					
					int i = 1;
					for(PozycjaZamowieniaFakturaDB pozycjaZamowienia:pozycjeWlasciwe)
						mapa.put(POZYCJE_ZAMOW_CN+"_"+POZYCJE_ZAMOW__ID_CN+"_"+(i++), new FieldData(Type.LONG, pozycjaZamowienia.getID())); 
					
					zamowieniaPozycje.setDictionaryData(mapa);
					
				}
				else{
					Map<String, Object> sessionValues = new HashMap<String,Object>();
					SessionUtils.putToSession(fm.getDocumentId(), POZYCJE_ZAMOW_CN, sessionValues);
					DwrUtils.cleanMultipleDictionary(values, "DWR_"+POZYCJE_ZAMOW_CN);
				}
				DSApi.closeContextIfNeeded(ctx);
				ctx=false;

				}
			}
			else if(f.getDocumentIdZamowienia()==null){
				if(f.getZAAKCEPTOWANOPOZZAM()!=null&&f.getZAAKCEPTOWANOPOZZAM()){
					ctx=DSApi.openContextIfNeeded();
					pozycjeZamJuzZaakceptowane=true;
					Map<String,FieldData>pozycjeZamowienia=((FieldData)((HashMap)((HashMap)WebContextFactory.get().getSession().getAttribute(DwrFacade.DWR_SESSION_NAME)).get("SELECTED_FIELDS_VALUES")).get("DWR_POZYCJE_ZAMOW")).getDictionaryData();
					int i=1;
					Long stareZamowienie=null;
					if(pozycjeZamowienia.containsKey(POZYCJE_ZAMOW_CN+"_"+POZYCJE_ZAMOW__ID_CN+"_"+i)&&pozycjeZamowienia.get(POZYCJE_ZAMOW_CN+"_"+POZYCJE_ZAMOW__ID_CN+"_"+i)!=null){
						Long id=pozycjeZamowienia.get(POZYCJE_ZAMOW_CN+"_"+POZYCJE_ZAMOW__ID_CN+"_"+i).getLongData();	
						PozycjaZamowieniaFakturaDB pozycja=PozycjaZamowieniaFakturaDBDAO.findById(id);
						NumeryZamowienDB zamowienieStare=NumeryZamowienDBDAO.findByErpIDUnique(pozycja.getDOCUMENT_ID_ZAMOWIENIA());
						if(zamowienieStare!=null)
							stareZamowienie=zamowienieStare.getId();
					}
					else{
						Map<String, Object> sessionValues = new HashMap<String,Object>();
						SessionUtils.putToSession(fm.getDocumentId(), POZYCJE_ZAMOW_CN, sessionValues);
						DwrUtils.cleanMultipleDictionary(values, "DWR_"+POZYCJE_ZAMOW_CN);
					}
				}
			}
		}
		if(values.get("DWR_"+AKCEPTACJA_POZYCJI_BUTTON_CN) != null && values.get("DWR_"+AKCEPTACJA_POZYCJI_BUTTON_CN).isSender()&&f.getDocumentIdZamowienia()!=null){
			if(f.getWystawcaFaktury()==null) msgBuilder.append(sm.getString("nalezyWskazacWystawce"));
			else{
				Long idZamowienia = f.getDocumentIdZamowienia();
				//odswiezenie kwot pozostalych
				f.setKwotaPozostala(f.getKwotaBrutto());
				f.setKwotaPozostalaNetto(f.getKwotaNetto());
				f.setKwotaPozostalaVAT(f.getKwotaVat());
				if(f.getZAAKCEPTOWANOPOZZAM()!=null&&!f.getZAAKCEPTOWANOPOZZAM()){
					FieldData pozycjeFaktury = values.get("DWR_"+POZYCJE_FAKTURY_CN);
					FieldData mpkFaktury = values.get("DWR_"+MPK_CN);
					ctx=DSApi.openContextIfNeeded();


					Map<String,Object> sessionValuesPozycjeZam=SessionUtils.getFromSession(fm.getDocumentId(), POZYCJE_ZAMOW_CN, new HashMap<String, Object>());
					List<PozycjaZamowieniaFakturaDB> list = new ArrayList<PozycjaZamowieniaFakturaDB>();
					//pobieram z sesji wartosci
					for(String key:sessionValuesPozycjeZam.keySet()){
						list.add((PozycjaZamowieniaFakturaDB) sessionValuesPozycjeZam.get(key));
					}
					//jakby nie bylo nic zapisanego w sesji( bylo zapisane zamowienie, ale zmienia teraz np pozycje tylko )
					if(list.isEmpty()){ 
						Map<String,FieldData>pozycjeZamowienia=((FieldData)((HashMap)((HashMap)WebContextFactory.get().getSession().getAttribute(DwrFacade.DWR_SESSION_NAME)).get("SELECTED_FIELDS_VALUES")).get("DWR_POZYCJE_ZAMOW")).getDictionaryData();
						Map<String, Object> sessionValuesToPut = new HashMap<String,Object>();
						int i=1;
						while(pozycjeZamowienia.containsKey(POZYCJE_ZAMOW_CN+"_"+POZYCJE_ZAMOW__ID_CN+"_"+i)&&pozycjeZamowienia.get(POZYCJE_ZAMOW_CN+"_"+POZYCJE_ZAMOW__ID_CN+"_"+i)!=null){
							Long id=pozycjeZamowienia.get(POZYCJE_ZAMOW_CN+"_"+POZYCJE_ZAMOW__ID_CN+"_"+i).getLongData();	
							PozycjaZamowieniaFakturaDB toSession=PozycjaZamowieniaFakturaDBDAO.findById(id);
							toSession.setILOSC_ZREAL(pozycjeZamowienia.get(POZYCJE_ZAMOW_CN+"_"+POZYCJE_ZAMOW_ILOSC_ZREAL_CN+"_"+i)!=null?pozycjeZamowienia.get(POZYCJE_ZAMOW_CN+"_"+POZYCJE_ZAMOW_ILOSC_ZREAL_CN+"_"+i).getMoneyData():pozycjeZamowienia.get(POZYCJE_ZAMOW_CN+"_"+POZYCJE_ZAMOW_ILOSC_CN+"_"+i).getMoneyData());
							sessionValuesToPut.put(POZYCJE_ZAMOW_CN+"_"+id, toSession);
							list.add(toSession);
							i++;
						}
						//list=PozycjaZamowieniaFakturaDBDAO.findByDocumentIdAndDocZamId(fm.getDocumentId(), f.getDocumentIdZamowienia());
					}

					List <FakturaPozycjaDB> pozycjeFakturyPoszczegolne=new ArrayList<FakturaPozycjaDB>();
					List <BudzetPozycjaDB> mpkPoszczegolne=new ArrayList<BudzetPozycjaDB>();
					int i=0;		
					for(PozycjaZamowieniaFakturaDB poz:list){
						if(poz.getILOSC_ZREAL()!=null&&poz.getILOSC_ZREAL().compareTo(new BigDecimal(0))>=0)continue;
						bladPozycjeZam=true;

					}
					if(!bladPozycjeZam){
						f.setZAAKCEPTOWANOPOZZAM(true);//dla magazynowej nie uzupelnia pol budzetowych
						for(PozycjaZamowieniaFakturaDB poz:list){

							BudzetPozycjaDB mpkDb=new BudzetPozycjaDB();
							FakturaPozycjaDB fakpoz=new FakturaPozycjaDB();

							if(poz.getOKRES_BUDZETOWY()!=null){
								mpkDb.setOkresBudzetowy(poz.getOKRES_BUDZETOWY());
							}
							if(poz.getIDENTYFIKATOR_BUDZETU()!=null){
								mpkDb.setBudzetId(poz.getIDENTYFIKATOR_BUDZETU());
								if(!InvoiceGeneralConstans.RodzajeFaktury.FAKTMAG.zwrocId().equals(f.getRodzajFaktury()))fakpoz.setPozycjaBudzetu(poz.getIDENTYFIKATOR_BUDZETU());
							}
							if(poz.getPROJEKT()!=null){
								mpkDb.setProjekt(poz.getPROJEKT());
								ProjectDB projDB=ProjectDBDAO.findByErpIDUnique(poz.getPROJEKT());
								if(!InvoiceGeneralConstans.RodzajeFaktury.FAKTMAG.zwrocId().equals(f.getRodzajFaktury()))fakpoz.setFunduszProjektowy(projDB!=null?projDB:null);
							}
							fakpoz.setPozycjaZamowieniaId(poz.getZAMPOZ_ID());
							fakpoz.setDocumentId(fm.getDocumentId());
							BigDecimal netto=poz.getNETTO().multiply(poz.getILOSC_ZREAL().setScale(4, BigDecimal.ROUND_HALF_UP).divide(poz.getILOSC(),BigDecimal.ROUND_HALF_UP));
							BigDecimal brutto=poz.getBRUTTO().multiply(poz.getILOSC_ZREAL().setScale(4, BigDecimal.ROUND_HALF_UP).divide(poz.getILOSC(),BigDecimal.ROUND_HALF_UP));
							BigDecimal vat=poz.getBRUTTO().subtract(poz.getNETTO()).multiply(poz.getILOSC_ZREAL().setScale(4, BigDecimal.ROUND_HALF_UP).divide(poz.getILOSC(),BigDecimal.ROUND_HALF_UP));

							fakpoz.setKwotaNetto(netto);
							fakpoz.setKwotaBrutto(brutto);
							fakpoz.setKwotaVat(vat);
							mpkDb.setKwotaNetto(netto);
							mpkDb.setKwotaVat(vat);
							//aktualizacja kwot pozostalych
							if(fakpoz.getKwotaBrutto()!=null){
								f.setKwotaPozostala(f.getKwotaPozostala().subtract(fakpoz.getKwotaBrutto()));
							}
							if(fakpoz.getKwotaVat()!=null){
								f.setKwotaPozostalaVAT(f.getKwotaPozostalaVAT().subtract(fakpoz.getKwotaVat()));
							}
							if(fakpoz.getKwotaNetto()!=null){
								f.setKwotaPozostalaNetto(f.getKwotaPozostalaNetto().subtract(fakpoz.getKwotaNetto()));
							}

							if(poz.getPRODUKT()!=null){
								fakpoz.setProduktyFaktury(CostInvoiceProductDB.find(poz.getPRODUKT()));
							}
							if(poz.getSTAWKA_VAT()!=null){
								fakpoz.setStawkaVat(VatRateDB.findByErpIDUnique(poz.getSTAWKA_VAT()));
								mpkDb.setStawkaVat(poz.getSTAWKA_VAT());
							}

							FakturaPozycjaDBDAO.save(fakpoz);
							pozycjeFakturyPoszczegolne.add(fakpoz);
							if(!InvoiceGeneralConstans.RodzajeFaktury.FAKTMAG.zwrocId().equals(f.getRodzajFaktury())){
								BudzetPozycjaDB.save(mpkDb);
								mpkPoszczegolne.add(mpkDb);
							}
						}
					}
					DSApi.closeContextIfNeeded(ctx);
					ctx=false;
					if(!bladPozycjeZam){
						i=1;
						Map<String, FieldData> mapa =new HashMap<String,FieldData>();
						for(FakturaPozycjaDB pozycjaFaktury: pozycjeFakturyPoszczegolne){
							mapa.put(POZYCJE_FAKTURY_CN+"_"+POZYCJE_FAKTURY_ID_CN+"_"+(i++), new FieldData(Type.LONG, pozycjaFaktury.getId()));
						}
						pozycjeFaktury.setDictionaryData(mapa);
						
						i=1;
						mapa =new HashMap<String,FieldData>();
						for(BudzetPozycjaDB mpk: mpkPoszczegolne){
							mapa.put(MPK_CN+"_"+MPK_ID_CN+"_"+(i++), new FieldData(Type.LONG, mpk.getId()));
						}
						mpkFaktury.setDictionaryData(mapa);
					}

				}
				else{
					pozycjeZamJuzZaakceptowane=true;
				}
			}
			
		}
		else if(values.get("DWR_"+AKCEPTACJA_POZYCJI_BUTTON_CN) != null && values.get("DWR_"+AKCEPTACJA_POZYCJI_BUTTON_CN).isSender()&&f.getDocumentIdZamowienia()==null){
			msgBuilder.append("Nie wskazano numeru zam闚ienia");
		}
		
        if (values.containsKey(DWR_WNIOSKI_O_REZERWACJE_CN) && values.get(DWR_WNIOSKI_O_REZERWACJE_CN).isSender()) {
        	List<Long>reservationIds = getWnioskiORezerwacjeIds(values);
        	if (!reservationIds.isEmpty()) {
        		setPozycjeRezerwacji(fm, values, reservationIds);
        	}
        }
        if (values.containsKey(DWR_UZUPELNIJ_POZYCJE_BUTTON_CN) && values.get(DWR_UZUPELNIJ_POZYCJE_BUTTON_CN).isSender()) {
        	List<Long>reservationPositionIds = getWnioskiORezerwacjePozycjeIds(values);
        	if (!reservationPositionIds.isEmpty()) {
        		uzupelnijSlownikiMPKiPozycjiFaktury(fm, reservationPositionIds, values,f);
        	} else {
    			msgBuilder.append( sm.getString("NotSelectedRequstForReservation"));
        	}
        }
		
		}catch(Exception e){
			log.error(e.getMessage(),e);
		}finally{
			DSApi.closeContextIfNeeded(ctx);
		}
		
		FMToModelMapper.przepiszWartosciZFakturyDoValuesDWR(f, values);
		
		if(!AvailabilityManager.isAvailable("invoiceGeneral.bezWalidacji")&&bladPozycjeZam){
			msgBuilder.append(sm.getString("fakturaBladPozycjiZam")+"; ");
		}
		
		if(!AvailabilityManager.isAvailable("invoiceGeneral.bezWalidacji")&&pozycjeZamJuzZaakceptowane){
			msgBuilder.append(sm.getString("fakturaPozycjeZamJuzZaakceptowane")+"; ");
		}
		if(!AvailabilityManager.isAvailable("invoiceGeneral.bezWalidacji")&&f.getKwotaVat()!=null){
			if(f.getKwotaVat().compareTo(new BigDecimal(0))<0){
				msgBuilder.append(sm.getString("fakturaBladVATMniejszyZero")+"; ");
			}
		}
		if(!AvailabilityManager.isAvailable("invoiceGeneral.bezWalidacji")&&f.getKwotaNetto()!=null){
			if(f.getKwotaNetto().compareTo(new BigDecimal(0))<0){
				msgBuilder.append(sm.getString("fakturaBladNettoMniejszeZero")+"; ");
			}
		}
		
		if(!AvailabilityManager.isAvailable("invoiceGeneral.bezWalidacji")&&!AvailabilityManager.isAvailable("invoiceGeneral.DataWystawieniaMozeBycPrzedDataWplywu")&&f.getDataWplywu()!=null&&f.getDataWystawienia()!=null){
			if(f.getDataWplywu().compareTo(f.getDataWystawienia())<0){
				msgBuilder.append(sm.getString("fakturaBladDat2")+"; ");
			}
		}
		if(f.getDataWplywu()!=null&&f.getDataWystawienia()!=null&&f.getTerminPlatnosci()!=null){
			if(!AvailabilityManager.isAvailable("invoiceGeneral.bezWalidacji")&&AvailabilityManager.isAvailable("invoiceGeneral.terminPlatnosciMozeBycPrzedDataWplywu")){
				if(f.getTerminPlatnosci().compareTo(f.getDataWystawienia())<0)
					msgBuilder.append(sm.getString("fakturaBladDat3")+"; ");

			}
			else{
				if(!AvailabilityManager.isAvailable("invoiceGeneral.bezWalidacji")&&f.getTerminPlatnosci().compareTo(f.getDataWplywu())<0||f.getTerminPlatnosci().compareTo(f.getDataWystawienia())<0){
					msgBuilder.append(sm.getString("fakturaBladDat")+"; ");
				}
			}
		}
		
		if (msgBuilder.length()>0)
		{
			values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
			return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
		}
		
		return null;
	}



	private PozycjaZamowieniaFakturaDB convertZamowienieSimplePozycjeDBToPozycjaZamowieniaDB(
			Long id, ZamowienieSimplePozycjeDB pozDB, NumeryZamowienDB zamowienie) {
		PozycjaZamowieniaFakturaDB poz= new PozycjaZamowieniaFakturaDB();
		poz.setBRUTTO(pozDB.getKwotvat().add(pozDB.getKwotnett()));
		poz.setDOCUMENT_ID(id);
		poz.setDOCUMENT_ID_ZAMOWIENIA(pozDB.getSupplier_order_id());
		poz.setIDENTYFIKATOR_BUDZETU(pozDB.getBdBudzetKoId());
		poz.setIDENTYFIKATOR_PLANU(pozDB.getBdPlanZamId());
		poz.setILOSC(pozDB.getIlosc());
		poz.setKOMORKA_BUDZETOWA(pozDB.getBdBudzetKoId());
		poz.setNETTO(pozDB.getKwotnett());
		poz.setOKRES_BUDZETOWY(pozDB.getBudzetId());
		poz.setPOZYCJA_BUDZETU(pozDB.getBdBudzetKoIdm());
		poz.setILOSC_ZREAL(pozDB.getIloscZreal()==null?pozDB.getIlosc():pozDB.getIloscZreal());
		poz.setPROJEKT(pozDB.getProjektId());
		poz.setOPIS(zamowienie.getUwagi());
	//	poz.setPOZYCJA_PLANU_ZAKUPOW(pozDB.getBdPlanZamId());
		poz.setSTAWKA_VAT(pozDB.getVatStawId());
		if(StringUtils.isNotBlank(pozDB.getWytwor_idm())){
			List<CostInvoiceProductDB> produkt;
			try {
				produkt = CostInvoiceProductDB.findByProductIDM(pozDB.getWytwor_idm());

				if(produkt.size()>0){
					poz.setPRODUKT(produkt.get(0).getId());
					List<UnitOfMeasureDB> jm=UnitOfMeasureDB.findByIdn(produkt.get(0).getJmIdn());
					if(jm.size()>0){
						poz.setJM(jm.get(0).getId());
					}
				}
			} catch (EdmException e) {
				log.error(e.getMessage(),e);
			}
		}
		poz.setZAMPOZ_ID(pozDB.getZamdospoz_id());
		return poz;
	}


	@Override
	public void validate(Map<String, Object> values, DocumentKind kind,
			Long documentId) throws EdmException {
		
		
		Faktura f=FMToModelMapper.zwrocModelZValues(values);
		FakturaDB faktDB=FakturaDBDAO.findModelById(documentId);
		if(StatusFaktury.wydruk_metryczki.zwrocId().equals(f.getStatus())&&faktDB!=null&&(faktDB.getWygenerowanoMetryczke()==null||!faktDB.getWygenerowanoMetryczke())){
			throw new EdmException(sm.getString("nieWygenerowanoMetryczki"));
		}
		
		
		//sprawdzenie kodu kreskowego, zy nie byl juz uzyty
		if (!StringUtils.isBlank(f.getKodKreskowy())&&documentId==null)
		{
			String barc = f.getKodKreskowy();
			List<OfficeDocument> doc = OfficeDocument.findAllByBarcode(barc);
			List list = OfficeFolder.findByBarcode(barc);
			List<FakturaDB> faktury=FakturaDBDAO.findByBarcode(barc);
			if (list != null && list.size()>0){
				throw new EdmException("Wprowadzony kod kreskowy = " + barc + " ju� istnieje w systemie !");
			}
			if(doc!=null&&doc.size()>0){
				throw new EdmException("Wprowadzony kod kreskowy = " + barc + " ju� istnieje w systemie !");
			}
			if(faktury!=null&&faktury.size()>0){
				throw new EdmException("Wprowadzony kod kreskowy = " + barc + " ju� istnieje w systemie !");
			}
		}
		
		//ustawienie polskiego zlotego jako default wartosci po utworzeniu dokumentu
		if(f.getStatus()==StatusFaktury.process_creation.zwrocId()){
			values.put(WALUTA_CN, WALUTA_PLN_ID);
		}
		
		if(!AvailabilityManager.isAvailable("invoiceGeneral.bezWalidacji")&&f.getKwotaVat()!=null){
			if(f.getKwotaVat().compareTo(new BigDecimal(0))<0){
				throw new EdmException(sm.getString("fakturaBladVATMniejszyZero"));
			}
		}
		if(!AvailabilityManager.isAvailable("invoiceGeneral.bezWalidacji")&&f.getKwotaNetto()!=null){
			if(f.getKwotaNetto().compareTo(new BigDecimal(0))<0){
				throw new EdmException(sm.getString("fakturaBladNettoMniejszeZero"));
			}
		}
		
		if(!AvailabilityManager.isAvailable("invoiceGeneral.bezWalidacji")&&!AvailabilityManager.isAvailable("invoiceGeneral.DataWystawieniaMozeBycPrzedDataWplywu")&&f.getDataWplywu()!=null&&f.getDataWystawienia()!=null){
			if(f.getDataWplywu().compareTo(f.getDataWystawienia())<0){
				throw new EdmException(sm.getString("fakturaBladDat2"));
			}
		}
		if(f.getDataWplywu()!=null&&f.getDataWystawienia()!=null&&f.getTerminPlatnosci()!=null){
			if(!AvailabilityManager.isAvailable("invoiceGeneral.bezWalidacji")&&AvailabilityManager.isAvailable("invoiceGeneral.terminPlatnosciMozeBycPrzedDataWplywu")){
				if(f.getTerminPlatnosci().compareTo(f.getDataWystawienia())<0)
					throw new EdmException(sm.getString("fakturaBladDat3"));

			}
			else{
				if(!AvailabilityManager.isAvailable("invoiceGeneral.bezWalidacji")&&f.getTerminPlatnosci().compareTo(f.getDataWplywu())<0||f.getTerminPlatnosci().compareTo(f.getDataWystawienia())<0){
					throw new EdmException(sm.getString("fakturaBladDat"));
				}
			}
		}

		
		if((!AvailabilityManager.isAvailable("invoiceGeneral.bezWalidacji")&&(f.getCzySzablon()==null||!f.getCzySzablon()))
				&&f.getStatus()!=StatusFaktury.process_creation.zwrocId()&&f.getStatus()!=StatusFaktury.rejestracja_wstepna.zwrocId()&&f.getStatus()!=StatusFaktury.uzup_rejestracji.zwrocId()&&
				f.getStatus()!=StatusFaktury.uzup_rejestracji2.zwrocId()){
			if(f.getKwotaPozostala().compareTo(new BigDecimal(0))!=0){
				throw new EdmException(sm.getString("fakturaBladKwoty"));
			}
			//niezgadzajace sie pola budzetowe
			BigDecimal sumaVAT=new BigDecimal(0);
			BigDecimal sumaBruttoDlaMagazynowejBadzProjektowej=new BigDecimal(0);
			BigDecimal sumaNettoDlaMagazynowejBadzProjektowej=new BigDecimal(0);
			
			Boolean rozliczenieMediow=false;
			if(f.getCzyRozliczenieMediow()!=null&&f.getCzyRozliczenieMediow())rozliczenieMediow=true;
			else if(f.getCzyRozliczenieMediow()==null&&documentId!=null){
				FakturaDB fakDB=FakturaDBDAO.findModelById(documentId);
				if(fakDB!=null&&fakDB.getCzyRozliczenieMediow()!=null)rozliczenieMediow=fakDB.getCzyRozliczenieMediow();
			}
			
			if(!rozliczenieMediow&&f.getPozycjeBudzetowe()!=null&&f.getPozycjeFaktury()!=null&&f.getPozycjeBudzetowe().size()>0&&
					!RodzajeFaktury.FAKTMAG.zwrocId().equals(f.getRodzajFaktury())&&!RodzajeFaktury.FAKTPROJEKT.zwrocId().equals(f.getRodzajFaktury())){
				ArrayList<BudzetPozycja> pozycjeZSumowaneDlaTychSamychBudzetow=new ArrayList<BudzetPozycja>();
				HashMap<Long,BudzetPozycja> mapBudzetyZsumowane=new HashMap<Long,BudzetPozycja>();
				for(BudzetPozycja bp:f.getPozycjeBudzetowe()){
					if(bp.getBudzet()!=null){
						if(mapBudzetyZsumowane.containsKey(bp.getBudzet().getId())){
							BudzetPozycja bptmp=mapBudzetyZsumowane.get(bp.getBudzet().getId());
							bptmp.setKwotaNetto(bptmp.getKwotaNetto().add(bp.getKwotaNetto()));
							bptmp.setKwotaVat(bptmp.getKwotaVat().add(bp.getKwotaVat()));
						}
						else{
							mapBudzetyZsumowane.put(bp.getBudzet().getId(), bp);
						}
					}
				}
				for(BudzetPozycja bp:mapBudzetyZsumowane.values()){
					pozycjeZSumowaneDlaTychSamychBudzetow.add(bp);
				}
				for(BudzetPozycja bp:pozycjeZSumowaneDlaTychSamychBudzetow){
					BigDecimal sumaKosztowNettoDlaBudzetu=new BigDecimal(0);
					BigDecimal sumaKosztowVATDlaBudzetu=new BigDecimal(0);
					for(PozycjaFaktury pf:f.getPozycjeFaktury()){
						if(pf.getBudzetPole()==null||pf.getBudzetPole().getBudzet()==null||bp.getBudzet()==null){
							//nie wybrano budzetu
							throw new EdmException(sm.getString("fakturaNieWybranoBudzetow"));
						}
						if(pf.getBudzetPole()!=null&&pf.getBudzetPole().getBudzet()!=null){
							if(pf.getBudzetPole().getBudzet().getId().compareTo(bp.getBudzet().getId())==0){
								if(pf.getKwotaNetto()!=null)
									sumaKosztowNettoDlaBudzetu=sumaKosztowNettoDlaBudzetu.add(pf.getKwotaNetto());
								if(pf.getKwotaVat()!=null)
									sumaKosztowVATDlaBudzetu=sumaKosztowVATDlaBudzetu.add(pf.getKwotaVat());
							}
						}
						if((pf.getZlecenie()==null||pf.getZlecenie().getId()==null)&&(pf.getProjekt()==null||pf.getProjekt().getId()==null)){
							throw new EdmException(sm.getString("fakturaNieWybranyFunduszAlboZlecenie"));
						}
						if((StatusFaktury.wprowadzenie_budzetow_pelnomocnik.zwrocId().equals(f.getStatus())||StatusFaktury.uzup_opisu.zwrocId().equals(f.getStatus()))&&pf.getIdPozycji()!=null){
							FakturaPozycjaDB pfDB=FakturaPozycjaDBDAO.findModelById(Long.parseLong(pf.getIdPozycji().toString()));
							if(pfDB.getWniosekORezerwacje()!=null){
								if(pf.getZrodlo()==null||
										(pfDB.getWniosekORezerwacje().getZrodloId().compareTo(pf.getZrodlo().getZrodlo_id().longValue())!=0))
									throw new EdmException(sm.getString("fakturaNieZgadzajaceSieZrodla"));
							}
						}
						
					}
					sumaVAT=sumaVAT.add(sumaKosztowVATDlaBudzetu);
					if(bp.getKwotaNetto().compareTo(sumaKosztowNettoDlaBudzetu)<0){
						//mniejsze
						throw new EdmException(sm.getString("fakturaKwotaNettoBudzetuMniejszaNizPozycji",bp.getBudzet().getNazwa()));
					}
					if(bp.getKwotaNetto().compareTo(sumaKosztowNettoDlaBudzetu)>0){
						//wieksze
						throw new EdmException(sm.getString("fakturaKwotaNettoBudzetuWiekszaNizPozycji",bp.getBudzet().getNazwa()));
					}
					if(bp.getKwotaVat().setScale(2,BigDecimal.ROUND_HALF_UP).compareTo(sumaKosztowVATDlaBudzetu.setScale(2,BigDecimal.ROUND_HALF_UP))<0){
						throw new EdmException(sm.getString("fakturaKwotaVATBudzetuMniejszaNizPozycji",bp.getBudzet().getNazwa()));
					}
					if(bp.getKwotaVat().compareTo(sumaKosztowVATDlaBudzetu)>0){
						throw new EdmException(sm.getString("fakturaKwotaVATBudzetuWiekszaNizPozycji",bp.getBudzet().getNazwa()));
					}
					

				}
			}
			else if(RodzajeFaktury.FAKTMAG.zwrocId().equals(f.getRodzajFaktury())||rozliczenieMediow){
				for(PozycjaFaktury pf:f.getPozycjeFaktury()){
					if(pf.getKwotaVat()!=null)sumaVAT=sumaVAT.add(pf.getKwotaVat());
					if(pf.getKwotaBrutto()!=null)sumaBruttoDlaMagazynowejBadzProjektowej=sumaBruttoDlaMagazynowejBadzProjektowej.add(pf.getKwotaBrutto());
					if((StatusFaktury.wprowadzenie_budzetow_pelnomocnik.zwrocId().equals(f.getStatus())||StatusFaktury.uzup_opisu.zwrocId().equals(f.getStatus()))&&pf.getIdPozycji()!=null){
						FakturaPozycjaDB pfDB=FakturaPozycjaDBDAO.findModelById(Long.parseLong(pf.getIdPozycji().toString()));
						if(pfDB.getWniosekORezerwacje()!=null){
							if(pf.getZrodlo()==null||
									(pfDB.getWniosekORezerwacje().getZrodloId().compareTo(pf.getZrodlo().getZrodlo_id().longValue())!=0))
								throw new EdmException(sm.getString("fakturaNieZgadzajaceSieZrodla"));
						}
					}
				}
			}
			else if(RodzajeFaktury.FAKTPROJEKT.zwrocId().equals(f.getRodzajFaktury())){
				HashMap<Long,BudzetPozycja> mapProjektyZsumowane=new HashMap<Long,BudzetPozycja>();
				for(BudzetPozycja bp:f.getPozycjeBudzetowe()){
					if(bp.getProjekt()!=null){
						if(mapProjektyZsumowane.containsKey(bp.getProjekt().getId())){
							BudzetPozycja bptmp=mapProjektyZsumowane.get(bp.getProjekt().getId());
							bptmp.setKwotaNetto(bptmp.getKwotaNetto().add(bp.getKwotaNetto()));
							bptmp.setKwotaVat(bptmp.getKwotaVat().add(bp.getKwotaVat()));
						}
						else{
							mapProjektyZsumowane.put(bp.getProjekt().getId(), bp);
						}
					}
				}
				ArrayList<BudzetPozycja> pozycjeZSumowaneDlaTychSamychProjektow=new ArrayList<BudzetPozycja>();
				for(BudzetPozycja bp:mapProjektyZsumowane.values()){
					pozycjeZSumowaneDlaTychSamychProjektow.add(bp);
				}
				for(BudzetPozycja bp:pozycjeZSumowaneDlaTychSamychProjektow){
					BigDecimal sumaKosztowNettoDlaProjektu=new BigDecimal(0);
					BigDecimal sumaKosztowVATDlaProjektu=new BigDecimal(0);
					for(PozycjaFaktury pf:f.getPozycjeFaktury()){
						if(pf.getProjekt()==null||pf.getProjekt().getId()==null||bp.getProjekt()==null){
							//nie wybrano budzetu
							throw new EdmException(sm.getString("fakturaNieWybranoProjektow"));
						}
						if(pf.getProjekt()!=null&&pf.getProjekt()!=null){
							if(pf.getProjekt().getId().compareTo(bp.getProjekt().getId())==0){
								if(pf.getKwotaNetto()!=null)
									sumaKosztowNettoDlaProjektu=sumaKosztowNettoDlaProjektu.add(pf.getKwotaNetto());
								if(pf.getKwotaVat()!=null)
									sumaKosztowVATDlaProjektu=sumaKosztowVATDlaProjektu.add(pf.getKwotaVat());
							}
						}
						if((StatusFaktury.wprowadzenie_budzetow_pelnomocnik.zwrocId().equals(f.getStatus())||StatusFaktury.uzup_opisu.zwrocId().equals(f.getStatus()))&&pf.getIdPozycji()!=null){
							FakturaPozycjaDB pfDB=FakturaPozycjaDBDAO.findModelById(Long.parseLong(pf.getIdPozycji().toString()));
							if(pfDB.getWniosekORezerwacje()!=null){
								if(pf.getZrodlo()==null||
										(pfDB.getWniosekORezerwacje().getZrodloId().compareTo(pf.getZrodlo().getZrodlo_id().longValue())!=0))
									throw new EdmException(sm.getString("fakturaNieZgadzajaceSieZrodla"));
							}
						}
					}
					sumaVAT=sumaVAT.add(sumaKosztowVATDlaProjektu);
					if(bp.getKwotaNetto().compareTo(sumaKosztowNettoDlaProjektu)<0){
						//mniejsze
						throw new EdmException(sm.getString("fakturaKwotaNettoProjektuMniejszaNizPozycji",bp.getProjekt().getTitle()));
					}
					if(bp.getKwotaNetto().compareTo(sumaKosztowNettoDlaProjektu)>0){
						//wieksze
						throw new EdmException(sm.getString("fakturaKwotaNettoProjektuWiekszaNizPozycji",bp.getProjekt().getTitle()));
					}
					if(bp.getKwotaVat().setScale(2,BigDecimal.ROUND_HALF_UP).compareTo(sumaKosztowVATDlaProjektu.setScale(2,BigDecimal.ROUND_HALF_UP))<0){
						throw new EdmException(sm.getString("fakturaKwotaVATProjektuMniejszaNizPozycji",bp.getProjekt().getTitle()));
					}
					if(bp.getKwotaVat().compareTo(sumaKosztowVATDlaProjektu)>0){
						throw new EdmException(sm.getString("fakturaKwotaVATProjektyWiekszaNizPozycji",bp.getProjekt().getTitle()));
					}
				}
				
				
				
			}
			if(f.getWydatekJestStrukturalny()!=null&&f.getWydatekJestStrukturalny()){
				if(f.getWydatkiStrukturalne()!=null){
					BigDecimal sumaWydatkowStr=new BigDecimal(0.0);
					for(WydatekStrukturalny ws:f.getWydatkiStrukturalne()){
						if(ws.getKwotaBrutto()!=null)sumaWydatkowStr=sumaWydatkowStr.add(ws.getKwotaBrutto());
					}
					if(sumaWydatkowStr.compareTo(f.getKwotaBrutto())>0)throw new EdmException(sm.getString("fakturaBladKwotWydatkiStrukturalne"));
				}
				else throw new EdmException(sm.getString("fakturaBladKwotWydatkiStrukturalne"));
			}
			if(f.getKwotaVat().compareTo(sumaVAT.setScale(2, BigDecimal.ROUND_HALF_UP))!=0){
				throw new EdmException(sm.getString("fakturaBladKwotyVAT"));
			}
			if((RodzajeFaktury.FAKTMAG.zwrocId().equals(f.getRodzajFaktury()))
					&&f.getKwotaBrutto().compareTo(sumaBruttoDlaMagazynowejBadzProjektowej.setScale(2, BigDecimal.ROUND_HALF_UP))!=0){
				throw new EdmException(sm.getString("fakturaBladKwotyBrutto"));
			}
			if((f.getCzySzablon()==null||!f.getCzySzablon())
					&&(f.getPozycjeBudzetowe()==null||(f.getPozycjeBudzetowe()!=null&&f.getPozycjeBudzetowe().size()==0))
					&&(f.getRodzajFaktury().equals(RodzajeFaktury.FAKTKOSZT.zwrocId())||f.getRodzajFaktury().equals(RodzajeFaktury.FAKTPROJEKT.zwrocId()))
					&&f.getStatus().equals(StatusFaktury.uzup_opisu.zwrocId())){
				//pozycje budzetowe nie moga byc puste oprocz faktury magazynowej, gdzie ich nie ma
				throw new EdmException(sm.getString("fakturaNieWybranoBudzetu"));
			}
		

		}
		if(documentId!=null){
			boolean ctx=DSApi.openContextIfNeeded();
			if(f.getZaakceptowanoWnioskiORez()!=null&&f.getZaakceptowanoWnioskiORez()){//"zapisanie do bazy" recznie, bo jest disabled w droolsach i inaczej by sie nie zapisalo			
				values.put(CZYZWNIOSKU_CN, true);
				List<WniosekORezerwacjePozycjaDB>zapisaneWBazie=WniosekORezerwacjePozycjaDBDAO.findby_Document_id(documentId);
				HashSet<Long>naFakturze=getWnioskiORezerwacjePozycjeIdsValidate(values);
				for(WniosekORezerwacjePozycjaDB pozDB:zapisaneWBazie){
					if(naFakturze.contains(pozDB.getId()))continue;
					else Persister.delete(pozDB);
				}
			}
			if(f.getZAAKCEPTOWANOPOZZAM()!=null&&f.getZAAKCEPTOWANOPOZZAM()){
				values.put(CZYZZAMOWIENIA_CN, true);
				List<PozycjaZamowieniaFakturaDB> zapisaneWBazie=PozycjaZamowieniaFakturaDBDAO.findByDocumentId(documentId);
				HashSet<Long>naFakturze=getZamowieniaIdsValidate(values);
				for(PozycjaZamowieniaFakturaDB pozDB:zapisaneWBazie){
					if(naFakturze.contains(pozDB.getID()))continue;
					else Persister.delete(pozDB);
				}
			}
			DSApi.closeContextIfNeeded(ctx);


			
		}


	}


	private Map<String, Object> ustawPolaZalezne(String dictionaryName,
			Map<String, FieldData> values, Map<String, Object> fieldsValues,
			Long documentId,Map<String, Object> results) {
		if(dictionaryName.equals(MPK_CN+"_")){
			DwrUtils.setRefValueEnumOptions(values, results, MPK_OKRESBUDZETOWY_CN, dictionaryName, MPK_BUDZET_CN, WIDOK_MPK_BUDZET, null, EMPTY_ENUM_VALUE);
			DwrUtils.setRefValueEnumOptions(values, results, MPK_BUDZET_CN, dictionaryName, MPK_POZYCJA_CN, WIDOK_MPK_BUDZET_POZYCJA, null, EMPTY_ENUM_VALUE);	
			DwrUtils.setRefValueEnumOptions(values, results, MPK_PROJEKT_CN, dictionaryName, MPK_BUDZET_PROJEKTU_CN, WIDOK_MPK_BUDZET_PROJEKTU, null, EMPTY_ENUM_VALUE);	
			DwrUtils.setRefValueEnumOptions(values, results, MPK_BUDZET_PROJEKTU_CN, dictionaryName, MPK_ETAP_CN, WIDOK_MPK_ETAP, null, EMPTY_ENUM_VALUE);	
			DwrUtils.setRefValueEnumOptions(values, results, MPK_ETAP_CN, dictionaryName, MPK_ZASOB_CN, WIDOK_MPK_ZASOB, null, EMPTY_ENUM_VALUE);	
			DwrUtils.setRefValueEnumOptions(values, results, MPK_BUDZET_CN, dictionaryName, MPK_DYSPONENT_CN, WIDOK_MPK_DYSPONENT, null, EMPTY_ENUM_VALUE);	
			DwrUtils.setRefValueEnumOptions(values, results, MPK_PROJEKT_CN, dictionaryName, MPK_DYSPONENT_PROJ_CN, WIDOK_MPK_DYSPONENT_PROJEKTOW, null, EMPTY_ENUM_VALUE);

		}
		else if(dictionaryName.equals(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD)){
			DwrUtils.setRefValueEnumOptions(values, results, POZYCJE_FAKTURY_PROJEKT_CN, dictionaryName, POZYCJE_FAKTURY_ZRODLO_CN, WIDOK_POZYCJE_ZRODLO, null, EMPTY_ENUM_VALUE);	
			DwrUtils.setRefValueEnumOptions(values, results, POZYCJE_FAKTURY_PROJEKT_CN, dictionaryName, POZYCJE_FAKTURY_ZADANIA_CN, WIDOK_POZYCJE_FAKTURY_ZADANIA, null, EMPTY_ENUM_VALUE);
			//DwrUtils.setRefValueEnumOptions(values, results, POZYCJE_FAKTURY_ZRODLO_ZAW_PROJ_CN, dictionaryName,POZYCJE_FAKTURY_PROJEKT_DO_ZRODLA_CN , WIDOK_POZYCJE_PPROJEKT_PO_ZRODLE, null, EMPTY_ENUM_VALUE);	

		} else if (dictionaryName.equals(WYDATEKSTRUKTPOZ_CN + "_")) {
			DwrUtils.setRefValueEnumOptions(values, results, WYDATEKSTRUKTPOZ_OBSZAR_CN, dictionaryName, WYDATEKSTRUKTPOZ_KOD_CN, WIDOK_WYDATKI_STRU_KOD , null, EMPTY_ENUM_VALUE);	
			DwrUtils.setRefValueEnumOptions(values, results, WYDATEKSTRUKTPOZ_KOD_CN, dictionaryName, WYDATEKSTRUKTPOZ_WYDATEK_STRUKT_CN, WIDOK_WYDATKI_STRU_WYD, null, EMPTY_ENUM_VALUE);
		}

		return results;

	}
	
	
	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values, Map<Class, Format> defaultFormatters) {
		try
		{	
			FakturaDB fDB=FakturaDBDAO.findModelById(docId);
			Faktura f = HibernateToCanonicalMapper.MapFakturaDBToFaktura(fDB, docId);
			Document doc = Document.find(docId);
			FieldsManager fm = doc.getFieldsManager();			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			
			Kontrahent kontrahent = f.getWystawcaFaktury();
			
			values.put("NRFAKTURY", f.getNumerFaktury());
			values.put("KONTRAHENT", kontrahent.getNazwa());
			values.put("NIP", kontrahent.getNip());
			values.put("IDEN", docId);
			String senderT = (kontrahent.getMiejscowosc()).trim();
			senderT += ", " + (kontrahent.getUlica()).trim();
			senderT += ", " + (kontrahent.getKodpocztowy()).trim();
			values.put("ADRES",senderT);

			// czas wygenerowania metryki
			values.put("DATA_BIEZACA", DateUtils.formatCommonDate(new java.util.Date()).toString());
			values.put("DRUKUJACY", DSApi.context().getDSUser().asFirstnameLastname());
			
			//uwagi
			String uwagi_tekst = "";
			
			List<Remark> uwagi = doc.getRemarks();
			
			for(Remark uwaga : uwagi){
				uwagi_tekst += uwaga.getAuthor()+"["+ dateFormat.format(uwaga.getCtime())+"]: "+uwaga.getContent()+" \r\n";
			}
			values.put("UWAGI", OfficeUtils.isValidString(uwagi_tekst)?uwagi_tekst:"--- Brak uwag ---");
			
			//pozycje
			String emptyCellValue = "---";
			List<PozycjaFaktury> pozycjaFakturies = f.getPozycjeFaktury();
			Map<String,Object> wartosci;
			List<Map> listDyn = (List<Map>) fm.getValue("POZYCJE_FAKTURY");
			String pozycje = "\\trowd " +
			"\\clvertalc\\qc\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx300 " + 	// Lp 3
			"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx1300 " +				// Budzet 1k
			"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx2300 " +				// Fundusz projektowy 1k					
			"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx3300 " +				// 毒鏚這 finansowania 1k
			"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx3900 " + 				// st vat 6
			"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx4500 " +				// netto 6
			"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx5100 " +				// vat 6
			"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx5700 " +				// brutto 6
			"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx6700 " +				// opk 1k
			"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx7400 " +				// poz. faktury 7
			"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx8100 " +				// przezn. zakup 7
			"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx8800 " +				// zlecenie 7
			"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx9400 " +				// koszty ds 6
			"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx10400 " +				// bezpo�rednie/po�rednie 1k
			"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx11200 " +				// system studi闚 8
			"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx12300 " +				// Kwalifikowalne/niekwalifikowalne 1.1k
			"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx13400 " +				// Rodzaj koszt闚 zw. z PR 1.1k
			"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx14400 " +				// Zadania do projekt闚 1k
			"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx15200 " +				// GUS	 8
					"Lp.\\intbl\\cell " +
					"Budzet\\intbl\\cell " +
					"Fundusz projektowy\\intbl\\cell " +
					"毒鏚這 finansowania\\intbl\\cell " +
					"St. VAT\\intbl\\cell " +
					"Netto\\intbl\\cell " +
					"Vat\\intbl\\cell " +
					"Brutto\\intbl\\cell " +
					"OPK\\intbl\\cell " +
					"Poz. faktury\\intbl\\cell " +
					"Przezn. zakupu\\intbl\\cell " +
					"Zlecenie\\intbl\\cell " +
					"Koszty ds\\intbl\\cell " +
					"Bezpo\u015Brednie/po\u015Brednie\\intbl\\cell " +
					"System studi闚\\intbl\\cell " +
					"Kwalifikowalne/niekwalifikowalne\\intbl\\cell " +
					"Rodzaj koszt闚 zw. z PR\\intbl\\cell " +
					"Zadania do projekt闚\\intbl\\cell " +
					"GUS\\intbl\\cell " +
					"\\row";
			int i = 0;
			for (PozycjaFaktury pf : pozycjaFakturies) {
				i++;
				pozycje+="\\trowd " +
				"\\clvertalc\\qc\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx300 " + 	// Lp 3
				"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx1300 " +				// Budzet 1k
				"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx2300 " +				// Fundusz projektowy 1k					
				"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx3300 " +				// 毒鏚這 finansowania 1k
				"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx3900 " + 				// st vat 6
				"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx4500 " +				// netto 6
				"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx5100 " +				// vat 6
				"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx5700 " +				// brutto 6
				"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx6700 " +				// opk 1k
				"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx7400 " +				// poz. faktury 7
				"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx8100 " +				// przezn. zakup 7
				"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx8800 " +				// zlecenie 7
				"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx9400 " +				// koszty ds 6
				"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx10400 " +				// bezpo�rednie/po�rednie 1k
				"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx11200 " +				// system studi闚 8
				"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx12300 " +				// Kwalifikowalne/niekwalifikowalne 1.1k
				"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx13400 " +				// Rodzaj koszt闚 zw. z PR 1.1k
				"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx14400 " +				// Zadania do projekt闚 1k
				"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs\\cellx15200 " +				// GUS	 8
					i+"\\intbl\\cell ";
				
//				wartosci = dict.getValues(klucz.toString());
				pozycje += (pf.getBudzetPole()!=null?pf.getBudzetPole().getBudzet().getBudzetIdn()+ " : " + pf.getBudzetPole().getBudzet().getNazwa():emptyCellValue) + "\\intbl\\cell ";
				pozycje += (pf.getProjekt()!=null?pf.getProjekt().getTitle():emptyCellValue) + "\\intbl\\cell ";
				pozycje += (pf.getZrodlo()!=null?pf.getZrodlo().getZrodlo_nazwa():emptyCellValue) + "\\intbl\\cell ";
				pozycje += (pf.getStawkaVat()!=null?pf.getStawkaVat().getNazwa().trim():emptyCellValue) + "\\intbl\\cell ";
				pozycje += (pf.getKwotaNetto()!=null?pf.getKwotaNetto().toString():emptyCellValue) + "\\intbl\\cell ";
				pozycje += (pf.getKwotaVat()!=null?pf.getKwotaVat().toString():emptyCellValue) + "\\intbl\\cell ";
				pozycje += (pf.getKwotaBrutto()!=null?pf.getKwotaBrutto().toString():emptyCellValue) + "\\intbl\\cell ";
				pozycje += (pf.getOpk()!=null?pf.getOpk().getTitle():emptyCellValue) + "\\intbl\\cell ";
				pozycje += (pf.getProduktFaktury()!=null?pf.getProduktFaktury().getNazwa():emptyCellValue) + "\\intbl\\cell ";
				pozycje += (pf.getPrzeznaczenieZakupu()!=null?pf.getPrzeznaczenieZakupu().getTitle():emptyCellValue) + "\\intbl\\cell ";
				pozycje += (pf.getZlecenie()!=null?pf.getZlecenie().getTitle():emptyCellValue) + "\\intbl\\cell ";
				//slowniki dynamiczne
				for(Map m:listDyn){
					EnumValues koszty = (EnumValues) m.get("POZYCJE_FAKTURY_DSD_REPO166_KOSZTY_DS");
					if(koszty != null){
						if(!koszty.getSelectedId().isEmpty()){
						EnumItem kosztyDS = DataBaseEnumField.getEnumItemForTable("dbo.dsd_repo166_koszty_ds", 
								Integer.valueOf(koszty.getSelectedId()));
						pozycje += kosztyDS.getTitle() + "\\intbl\\cell ";
					} else {
						pozycje += emptyCellValue + "\\intbl\\cell ";
					}
					} else {
						pozycje += emptyCellValue + "\\intbl\\cell ";
					}

					EnumValues bezposr = (EnumValues) m.get("POZYCJE_FAKTURY_DSD_REPO116_BEZPOSREDNIEPOSREDNIE");
					if(bezposr != null){
						if(!bezposr.getSelectedId().isEmpty()){
						EnumItem bezposrednie = DataBaseEnumField.getEnumItemForTable("dbo.dsd_repo116_bezposrednieposrednie", Integer.valueOf(bezposr.getSelectedId()));
						pozycje += bezposrednie.getTitle() + "\\intbl\\cell ";
					} else {
						pozycje += emptyCellValue + "\\intbl\\cell ";
					}
					} else {
						pozycje += emptyCellValue + "\\intbl\\cell ";
					}

					EnumValues sys = (EnumValues) m.get("POZYCJE_FAKTURY_DSD_REPO115_SYSTEM_STUDIOW");
					if(sys != null){
						if(!sys.getSelectedId().isEmpty()){
						EnumItem sysSt = DataBaseEnumField.getEnumItemForTable("dbo.dsd_repo115_system_studiow", Integer.valueOf(sys.getSelectedId()));
						pozycje += sysSt.getTitle() + "\\intbl\\cell ";
					} else {
						pozycje += emptyCellValue + "\\intbl\\cell ";
					}
					} else {
						pozycje += emptyCellValue + "\\intbl\\cell ";
					}

					EnumValues kwali = (EnumValues) m.get("POZYCJE_FAKTURY_DSD_REPO119_KWALIFIKOWALNENIEKWALIFIKOWALNE");
					if(kwali != null){
						if(!kwali.getSelectedId().isEmpty()){
						EnumItem kwalifikacje = DataBaseEnumField.getEnumItemForTable("dbo.dsd_repo119_kwalifikowalneniekwalifikowalne", 
								Integer.valueOf(kwali.getSelectedId()));
						pozycje += kwalifikacje.getTitle() + "\\intbl\\cell ";
					} else {
						pozycje += emptyCellValue + "\\intbl\\cell ";
					}
					} else {
						pozycje += emptyCellValue + "\\intbl\\cell ";
					}

					EnumValues rodzaj = (EnumValues) m.get("POZYCJE_FAKTURY_DSD_REPO118_RODZAJ_KOSZTOW_ZW_Z_PR");
					if(rodzaj != null){
						if(!rodzaj.getSelectedId().isEmpty()){
						EnumItem rodzajKoszt = DataBaseEnumField.getEnumItemForTable("dbo.dsd_repo118_rodzaj_kosztow_zw_z_pr", Integer.valueOf(rodzaj.getSelectedId()));
						pozycje += rodzajKoszt.getTitle() + "\\intbl\\cell ";
						} else {
							pozycje += emptyCellValue + "\\intbl\\cell ";
						}
					} else {
						pozycje += emptyCellValue + "\\intbl\\cell ";
					}

					EnumValues zadania = (EnumValues) m.get("POZYCJE_FAKTURY_DSD_REPO173_ZADANIA_DO_PROJEKTOW");
					if(zadania != null){
						if(!zadania.getSelectedId().isEmpty()){
						EnumItem zadaniaProjektowe = DataBaseEnumField.getEnumItemForTable("dbo.dsd_repo173_zadania_do_projektow", Integer.valueOf(zadania.getSelectedId()));
						pozycje += zadaniaProjektowe.getTitle() + "\\intbl\\cell ";
					} else {
						pozycje += emptyCellValue + "\\intbl\\cell ";
					}
					} else {
						pozycje += emptyCellValue + "\\intbl\\cell ";
					}

					EnumValues g = (EnumValues) m.get("POZYCJE_FAKTURY_DSD_REPO67_GUS");
					if(g != null){
						if(!g.getSelectedId().isEmpty()){
						EnumItem gus = DataBaseEnumField.getEnumItemForTable("dbo.dsd_repo67_gus", Integer.valueOf(g.getSelectedId()));
						pozycje += gus.getTitle() + "\\intbl\\cell\\row ";
					} else {
						pozycje += emptyCellValue + "\\intbl\\cell\\row ";
					}
					} else {
						pozycje += emptyCellValue + "\\intbl\\cell\\row ";
					}
					listDyn.remove(0);
					break;
				}
			}
			values.put("POZYCJE", pozycje);
			
			//akceptacje
			PreparedStatement ps;
			ResultSet rs;
			ps = DSApi.context().prepareStatement("select sourceuser, status, ctime from dso_document_asgn_history where targetuser = 'x' and targetguid = 'x' and type != 1 and document_id=?");
			ps.setLong(1, docId);
			rs = ps.executeQuery();
			DSUser user = null;
			while(rs.next()){
				if(rs.getString(2).equalsIgnoreCase("Rejestracja wst瘼na faktury")){
					user = DSUser.findByUsername(rs.getString(1));
				values.put("REJ_WSTEPNA", user.asFirstnameLastname());
				values.put("REJ_WSTEPNA_DATA", rs.getString(3));

				//czesc przez sm, bo inaczej jest niepoprawne kodowanie polskich znakow
				} else if(rs.getString(2).equalsIgnoreCase("Weryfikacja merytoryczna")){
					user = DSUser.findByUsername(rs.getString(1));
					values.put("WER_MER", user.asFirstnameLastname());
					values.put("WER_MER_DATA", rs.getString(3));
				} else if(rs.getString(2).equalsIgnoreCase(sm.getString("uzupelnieniePrzezPelnomocnika"))){
					user = DSUser.findByUsername(rs.getString(1));
					values.put("UZUP_PELNO", user.asFirstnameLastname());
					values.put("UZUP_PELNO_DATA", rs.getString(3));
				} else if(rs.getString(2).equalsIgnoreCase(sm.getString("wprowadzenieBudzetowPrzezPelnomocnika"))){
					user = DSUser.findByUsername(rs.getString(1));
					values.put("WPROW_BUDZETOW", user.asFirstnameLastname());
					values.put("WPROW_BUDZETOW_DATA", rs.getString(3));
				} else if(rs.getString(2).equalsIgnoreCase(sm.getString("akceptacjaDysponentaSrodkow"))){
					user = DSUser.findByUsername(rs.getString(1));
					values.put("AKC_DYSP", user.asFirstnameLastname());
					values.put("AKC_DYSP_DATA", rs.getString(3));
				} else if(rs.getString(2).equalsIgnoreCase(sm.getString("weryfikacjaPoprawnosciOpisu"))){
					user = DSUser.findByUsername(rs.getString(1));
					values.put("WER_POPRA", user.asFirstnameLastname());
					values.put("WER_POPRA_DATA", rs.getString(3));
				} else if(rs.getString(2).equalsIgnoreCase("Akceptacja merytoryczna")){
					user = DSUser.findByUsername(rs.getString(1));
					values.put("AKC_MER", user.asFirstnameLastname());
					values.put("AKC_MER_DATA", rs.getString(3));
				} else if(rs.getString(2).equalsIgnoreCase("Akceptacja formalno-rachunkowa")){
					user = DSUser.findByUsername(rs.getString(1));
					values.put("AKC_FORM_RACH", user.asFirstnameLastname());
					values.put("AKC_FORM_RACH_DATA", rs.getString(3));
				} else if(rs.getString(2).equalsIgnoreCase(sm.getString("akceptacjaIModyfikacjaDekretuKsiegowego"))){
					user = DSUser.findByUsername(rs.getString(1));
					values.put("AKC_DEKRETU", user.asFirstnameLastname());
					values.put("AKC_DEKRETU_DATA", rs.getString(3));
				} else if(rs.getString(2).equalsIgnoreCase("Ostateczna akceptacja faktury")){
					user = DSUser.findByUsername(rs.getString(1));
					values.put("OST_AKC", user.asFirstnameLastname());
					values.put("OST_AKC_DATA", rs.getString(3));
				} else if(rs.getString(2).equalsIgnoreCase("Wydruk metryczki")){
					user = DSUser.findByUsername(rs.getString(1));
					values.put("WYDRUK", user.asFirstnameLastname());
					values.put("WYDRUK_DATA", rs.getString(3));
				} 	
			}
	
			//Wydatki strukturalne
			List<Map<String, Object>> wydatkiPoz = Lists.newArrayList();
			
			List<WydatekStrukturalny> wydPozycje = f.getWydatkiStrukturalne();
			for(WydatekStrukturalny w:wydPozycje) {
				Map<String, Object> wydatki = Maps.newHashMap();
				wydatki.put("lp", Integer.toString(i));
				if(w.getObszar() != null){
					EnumItem obszar = DataBaseEnumField.getEnumItemForTable(WIDOK_WYDATKI_STRU_OBSZAR,  w.getObszar().intValue());
					wydatki.put("obszar", obszar.getTitle());
				} else {
					wydatki.put("obszar", emptyCellValue);
				}
				if(w.getKod() != null){
					EnumItem kod = DataBaseEnumField.getEnumItemForTable(WIDOK_WYDATKI_STRU_KOD,  w.getKod().intValue());
					wydatki.put("kod", kod.getTitle());
				} else {
					wydatki.put("kod", emptyCellValue);
				}
				if(w.getWydatekStrukturalny() != null){
					EnumItem wyd = DataBaseEnumField.getEnumItemForTable(WIDOK_WYDATKI_STRU_WYD,  w.getWydatekStrukturalny().intValue());
					wydatki.put("wyd", wyd.getTitle());
				} else {
					wydatki.put("wyd", emptyCellValue);
				}
				wydatki.put("kwota", w.getKwotaBrutto().toString());
				wydatkiPoz.add(wydatki);
				i++;
			}	
			values.put("wydPoz", wydatkiPoz);
			fDB.setWygenerowanoMetryczke(true);
			FakturaDBDAO.update(fDB);
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		} catch (Exception e){
			log.error("B陰d podczas generownia metryki faktury.", e);
		}
	}

	/**
	 * Zwraca list� u篡tkownik闚 kt鏎zy maja dost瘼 do dokumentu
	 * Na podstawie:
	 * 	ds_permission - Subjecttyp brany pod uwag� to user i group.
	 * 	tabela DSW_JBPM_TASKLIST - U kogo aktualnie znajduje si� pismo.
	 * @param documentId
	 * @return
	 */
	@Override
	public List<DSUser> getUserWithAccessToDocument(Long documentId){
		try {
			Set<PermissionBean> docperm = DSApi.context().getDocumentPermissions(Document.find(documentId));
			HashMap<String, PermissionBean> test = new HashMap<String, PermissionBean>();
			for (PermissionBean permissionBean : docperm) {
				if(!test.containsKey(permissionBean.getSubject())){
					test.put(permissionBean.getSubject(), permissionBean);					
				}
			}
			
			Set<String> keys = test.keySet();
			List<DSUser> dsUsers = new ArrayList<DSUser>();
			for (String string : keys) {
				if(test.get(string).getSubjectType().equalsIgnoreCase("user"))
					dsUsers.add(DSUser.findByUsername(string));
			}
			
			return dsUsers;
			
		} catch (EdmException e) {
			log.debug("b章d podczas szukania dokumentu.");
			return null;
		}
	}
	
    public void wyczyscPoleDataBase(Map<String, Object> results,String fieldWithPrefix){
		HashMap< String, String> mapa=new HashMap<String, String>();
		mapa.put("", "--wybierz--");
		List<Map<String,String>>itemy = new ArrayList<Map<String,String>>();
		itemy.add(mapa);
		EnumValues evDoWst=new EnumValues();
		evDoWst.setAllOptions(itemy);
		evDoWst.setSelectedId("");
		results.put(fieldWithPrefix,evDoWst);
    }
    
    public Map<String, Object> wyczyscZeWzglNaKontoFunduszu(Map<String, FieldData> values, Map<String, Object> fieldsValues,
			Long documentId,Map<String, Object> results){
    	FieldData proj=values.get(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+POZYCJE_FAKTURY_PROJEKT_CN);
    	if(proj.getData()!=null&&!("").equals(proj.getData())&&
    			proj.getEnumValuesData()!=null&&!("").equals(proj.getEnumValuesData().getSelectedId())){
    		try {
				List<ProjectAccountPiatkiDB> piatki=ProjectAccountPiatkiDBDAO.findByKontraktIdAndAvailable(Long.parseLong(proj.getEnumValuesData().getSelectedId()));
				EnumValues evDoWstNieDostepne=new EnumValues();
				List<Map<String,String>>pustaLista = new ArrayList<Map<String,String>>();
				HashMap< String, String> mapa=new HashMap<String, String>();
				mapa.put("0", "--Pozycja niedost瘼na dla wybranego funduszu--");
				pustaLista.add(mapa);
				evDoWstNieDostepne.setAllOptions(pustaLista);
				evDoWstNieDostepne.setSelectedId("0");
				
				if(piatki.size()>0){
					String konto=piatki.get(0).getKonto_ido();
					if(konto!=null&&konto.length()>3){
						Integer numer=Integer.parseInt(konto.substring(0, 3));
						if((numer>=500&&numer<=508)||(numer>=510&&numer<=515)||(numer==521)||(numer>=530&&numer<=535)||(numer==555)){
							results.put(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+SLOWNIK_DYNAMICZNY_KWALIFIKACJA_KOSZTOW, evDoWstNieDostepne);
							results.put(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+SLOWNIK_DYNAMICZNY_ZADANIA_PROJEKTOWE, evDoWstNieDostepne);
							
							results.put(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+SLOWNIK_DYNAMICZNY_BEZPOSREDNIE_POSREDNIE, evDoWstNieDostepne);
							results.put(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+SLOWNIK_DYNAMICZNY_GUS, evDoWstNieDostepne);
							results.put(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+SLOWNIK_DYNAMICZNY_RODZAJ_KOSZTOW_ZW_Z_PR, evDoWstNieDostepne);
						}
						else if(numer==509||numer==516||numer==517||numer==518||numer==519||numer==529){
							results.put(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+SLOWNIK_DYNAMICZNY_SYSTEMY_STUDIOW, evDoWstNieDostepne);
							results.put(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+SLOWNIK_DYNAMICZNY_KOSZTY_DS, evDoWstNieDostepne);
							
							results.put(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+SLOWNIK_DYNAMICZNY_BEZPOSREDNIE_POSREDNIE, evDoWstNieDostepne);
							results.put(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+SLOWNIK_DYNAMICZNY_GUS, evDoWstNieDostepne);
							results.put(POZYCJE_FAKTURY_WITH_SUFFIX_FIELD+SLOWNIK_DYNAMICZNY_RODZAJ_KOSZTOW_ZW_Z_PR, evDoWstNieDostepne);
						}
					}
				}
			} catch (NumberFormatException e) {
				log.error(e.getMessage(),e);
			} catch (EdmException e) {
				log.error(e.getMessage(),e);
			}
    	}
				return results;
    	
    }
    
    



    @Override
    public Map<String, String> modifyEnumValues(Map<String, String> enumValues,
    		ArrayList<String> enumSelectedSetm, String cn, Long docId) throws EdmException {
    	Map<String, String> newValues = Maps.newHashMap();
    	String budId = null;
    	boolean select = false;
    	try{
    		Map<String, Object> sessionValues = SessionUtils.getFromSession(docId, "MPKBUDID", new HashMap<String, Object>());
    		if(sessionValues.size() != 0){
    			budId = String.valueOf(sessionValues.get("BUDZETID"));
    		}
    		DSUser user = DSUser.findByUsername(DSApi.context().getPrincipalName());
    		DataBaseEnumField uzytkDoBudzetu = DataBaseEnumField.getEnumFiledForTable(WIDOK_UZYTKOWNICY_WYP_DEKRET);
    		List<EnumItem> ei = uzytkDoBudzetu.getSortedEnumItems();
    		if(cn.startsWith("BUDZETID") && enumValues.size() != 0){
    			for(Map.Entry<String, String> elem:enumValues.entrySet()){
    				for(String s:enumSelectedSetm){
    					if(elem.getKey().equalsIgnoreCase(s)){
    						newValues.put(elem.getKey(), elem.getValue());
    					} else if(s.equalsIgnoreCase("")){
    						select = true;
    					}
    				}
    				if(select && budId != null){
    					if(elem.getKey().equals(budId))
    						newValues.put(elem.getKey(), elem.getValue());
    				}
    				for(EnumItem e:ei){
    					if(e.getCn().equalsIgnoreCase(user.getCn())){
    						if(elem.getKey().equalsIgnoreCase(e.getRefValue()) || elem.getKey().equalsIgnoreCase("")){
    							newValues.put(elem.getKey(), elem.getValue());
    						}
    					}
    				}
    			}
    			return newValues;
    		} else if(cn.startsWith("POZYCJAFAKPOLEBUDZET") && enumValues.size() != 0){
    			for(Map.Entry<String, String> elem:enumValues.entrySet()){
    				for(String s:enumSelectedSetm){
    					if(elem.getKey().equalsIgnoreCase(s)){
    						newValues.put(elem.getKey(), elem.getValue());
    					}
    				}
    				for(EnumItem e:ei){
    					if(e.getCn().equalsIgnoreCase(user.getCn())){
    						if(elem.getKey().equalsIgnoreCase(e.getRefValue()) || elem.getKey().equalsIgnoreCase("")){
    							newValues.put(elem.getKey(), elem.getValue());
    						}
    					}
    				}
    			}
    			return newValues;
    		}
    		else if(cn.startsWith(NRZAMOWIENIA_CN)&&enumValues.size()!=0){
    			Long erpIdWystawca=(Long) SessionUtils.getFromSession(docId, NRZAMOWIENIA_CN+"_ERPID");
    			if(erpIdWystawca==null){
    				LinkedHashMap<String,Object> mapaWystawca=(LinkedHashMap<String, Object>) Document.find(docId).getFieldsManager().getValue(InvoiceGeneralConstans.WYSTAWCA_CN);
    				if(mapaWystawca!=null&&mapaWystawca.containsKey(InvoiceGeneralConstans.WYSTAWCA_CN+"_ERPID")){
    					if((mapaWystawca.get(InvoiceGeneralConstans.WYSTAWCA_CN+"_ERPID") instanceof BigDecimal) && mapaWystawca.get(InvoiceGeneralConstans.WYSTAWCA_CN+"_ERPID")!=null){
    						erpIdWystawca=Long.parseLong(((BigDecimal)mapaWystawca.get(InvoiceGeneralConstans.WYSTAWCA_CN+"_ERPID")).toString());
    					}
    				}
    			}
    			if(erpIdWystawca!=null){
    				DataBaseEnumField dbefNRZam=DataBaseEnumField.getEnumFiledForTable(WIDOK_NUMERY_ZAMOWIEN);
    				Map<String,String>lista = new HashMap<String,String>();
    				lista.put("", "--wybierz--");
    				for(EnumItem enumitem: dbefNRZam.getAvailableItems()){
    					String ref=enumitem.getRefValue();
    					if(!StringUtils.isBlank(ref)){
    						Double erpIdik=Double.parseDouble(ref);
    						if(enumitem.getRefValue()!=null&&(erpIdWystawca.compareTo(erpIdik.longValue())==0)&&enumitem.isAvailable()){
    							lista.put(enumitem.getId().toString(), enumitem.getTitle());
    						}
    					}
    				}
    				return lista;
    			}
    			else{
    				Map<String,String>lista = new HashMap<String,String>();
    				lista.put("", "--wybierz--");
    				return lista;
    			}
    			//}
    		}
    	} catch(Exception e) {
    		log.error("", e);
    	} 
    	return enumValues;
    }
    @Override
    public void modifyDwrFields(List<Field> dwrFields,
    		Map<String, Object> dwrFieldsValues, FieldsManager fm)
    				throws EdmException {

      	if(dwrFieldsValues.get("DWR_"+CZYROZLICZENIEMEDIOW_CN)!=null&&(Boolean)dwrFieldsValues.get("DWR_"+CZYROZLICZENIEMEDIOW_CN)){
    		for(Field fd:dwrFields){
    			if(fd.getCn().equals("DWR_"+POZYCJE_FAKTURY_CN)){
    				List<String> buttons=new ArrayList<String>();
    				fd.getDictionary().setDicButtons(buttons);
    			}
    		}
    	}

    }
    private List<Long> getWnioskiORezerwacjeIds(Map<String, FieldData> values) {
    	List<Long> reservationIds = new ArrayList<Long>();
    	if (values.containsKey(DWR_WNIOSKI_O_REZERWACJE_CN) && values.get(DWR_WNIOSKI_O_REZERWACJE_CN) != null) {
    		Map<String, FieldData> reservationDictionary = ((FieldData) values.get(DWR_WNIOSKI_O_REZERWACJE_CN)).getDictionaryData();
    		for (String key : reservationDictionary.keySet()) {
    			if (key.contains(WNIOSKI_O_REZERWACJE_REZERWACJA_IDM_CN)) {
    				String selectedId = reservationDictionary.get(key).getEnumValuesData().getSelectedId();
    				if (StringUtils.isNotBlank(selectedId)) {
    					reservationIds.add(Long.valueOf(selectedId));
    				}
    			}
    		}
    	}
    	return reservationIds;
    }
    private List<Long> getWnioskiORezerwacjePozycjeIds(Map<String, FieldData> values) {
    	List<Long> reservationIds = new ArrayList<Long>();
    	if (values.containsKey(DWR_POZYCJE_REZERWACJI_CN) && values.get(DWR_POZYCJE_REZERWACJI_CN) != null) {
    		Map<String, FieldData> reservationPositionDictionary = ((FieldData) values.get(DWR_POZYCJE_REZERWACJI_CN)).getDictionaryData();
    		for (String key : reservationPositionDictionary.keySet()) {
    			if (key.contains(POZYCJE_REZERWACJI_ID_CN)) {
    				Integer id = reservationPositionDictionary.get(key).getIntegerData();
    				if (id!=null) {
    					reservationIds.add(Long.valueOf(id));
    				}
    			}
    		}
    	}
    	return reservationIds;
    }
	private HashSet<Long> getWnioskiORezerwacjePozycjeIdsValidate(
			Map<String, Object> values) {
		HashSet<Long> idikiPozycjiRezerwacji=new HashSet<Long>();
		HashMap<String,Object>slowniki=(HashMap<String, Object>) values.get("M_DICT_VALUES");
		int i=1;
		while(slowniki.containsKey(POZYCJE_REZERWACJI_CN+"_"+i)){
			HashMap<String,Object> pozycjaSlownika=(HashMap<String, Object>) slowniki.get(POZYCJE_REZERWACJI_CN+"_"+i);
			if(pozycjaSlownika!=null&&pozycjaSlownika.get(POZYCJE_REZERWACJI_ONLY_ID_CN)!=null){
				idikiPozycjiRezerwacji.add(Long.parseLong(pozycjaSlownika.get(POZYCJE_REZERWACJI_ONLY_ID_CN).toString()));
			}
			i++;
		}
		return idikiPozycjiRezerwacji;
	}
	private HashSet<Long> getZamowieniaIdsValidate(Map<String, Object> values){
		HashSet<Long> idikiPozycjiZam=new HashSet<Long>();
		HashMap<String,Object>slowniki=(HashMap<String, Object>) values.get("M_DICT_VALUES");
		int i=1;
		while(slowniki.containsKey(POZYCJE_ZAMOW_CN+"_"+i)){
			HashMap<String,Object> pozycjaSlownika=(HashMap<String, Object>) slowniki.get(POZYCJE_ZAMOW_CN+"_"+i);
			if(pozycjaSlownika!=null&&pozycjaSlownika.get(POZYCJE_REZERWACJI_ONLY_ID_CN)!=null){
				idikiPozycjiZam.add(Long.parseLong(pozycjaSlownika.get(POZYCJE_REZERWACJI_ONLY_ID_CN).toString()));
			}
			i++;
		}
		return idikiPozycjiZam;
	}
    private void setPozycjeRezerwacji(FieldsManager fm, Map<String, FieldData> values, List<Long> reservationIds) {
    	Map<String, FieldData> positionValues = new LinkedHashMap<String, FieldData>();
    	try {
	    	DSApi.openAdmin();
	    	List<WniosekORezerwacjeDB> requestsForReservationList = (List<WniosekORezerwacjeDB>) DSApi.context().session().createCriteria(WniosekORezerwacjeDB.class)
	    		.add(Restrictions.in("id", reservationIds))
	    		.list();
	    	if (!requestsForReservationList.isEmpty()) {
	    		int index = 1;
	    		List<Long> ids = new ArrayList<Long>();
	    		for (WniosekORezerwacjeDB wniosek : requestsForReservationList) {
	    			DSApi.context().begin();
	    			CostInvoiceProductDB product = (CostInvoiceProductDB) DSApi.context().session().createCriteria(CostInvoiceProductDB.class)
	        				.add(Restrictions.eq("wytworId", wniosek.getWytworId()))
	        				.uniqueResult();
	    			
	    			String productIdm = null;
	    			String productName = null;
	    			if (product != null) {
	    				productIdm = product.getCn();
	    				productName = product.getTitle();
	    			}
	    			BigDecimal brutto=wniosek.getCena();
	    			if(wniosek.getVatstawId()!=null&&wniosek.getCena()!=null&&wniosek.getIlosc()!=null){
	    				brutto=brutto.multiply(wniosek.getIlosc());
	    				brutto=VatRateDB.findByErpIDUnique(wniosek.getVatstawId())!=null?brutto.multiply((new BigDecimal(0.01)).multiply(new BigDecimal(VatRateDB.findByErpIDUnique(wniosek.getVatstawId()).getCn()))).add(brutto):brutto;
	    			}
	    			ZrodloDB zr=null;
	    			if(wniosek.getZrodloId()!=null){
	    				zr=ZrodloDBDAO.findByErpIDUnique(wniosek.getZrodloId());
	    			}
	    			Long zrodloId=null;
	    			if(zr!=null)zrodloId=zr.getErpId();
	    			WniosekORezerwacjePozycjaDB pozycjaRezerwacji = new WniosekORezerwacjePozycjaDB(wniosek.getBudzetId(),wniosek.getBdBudzetKoId(),wniosek.getBdRezerwacjaId(),wniosek.getBdRezerwacjaIdm(), wniosek.getNrpoz(), productIdm, productName, wniosek.getIlosc(), wniosek.getCena(), null, wniosek.getKoszt(), null, null,wniosek.getVatstawId(),brutto,fm.getDocumentId(),zrodloId);
	    			Persister.create(pozycjaRezerwacji);
	    			DSApi.context().commit();
	    			positionValues.put(POZYCJE_REZERWACJI_ID_CN + "_" + index, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, pozycjaRezerwacji.getId()));
	    			ids.add(pozycjaRezerwacji.getId());
	    			index++;
	    		}
	    		values.get(DWR_POZYCJE_REZERWACJI_CN).setDictionaryData(positionValues);
	    		//fm.getDocumentKind().setOnly(fm.getDocumentId(), Collections.singletonMap(POZYCJE_REZERWACJI_FIELD, ids));
	    		DSApi.close();
	    	}
    	} catch (EdmException e) {
    		log.error(e.getMessage(), e);
    	} catch (Exception e) {
    		log.error(e.getMessage(), e);
    	}
    }
    
    private void uzupelnijSlownikiMPKiPozycjiFaktury(FieldsManager fm, List<Long> reservationPositionIds, Map<String, FieldData> values,Faktura f) {
    	Map<String, FieldData> mpkValues = new LinkedHashMap<String, FieldData>();
    	Map<String, FieldData> pozFaktValues = new LinkedHashMap<String, FieldData>();
    	List<Long> dekretyIds = new ArrayList<Long>();
    	try {
	    	DSApi.openAdmin();
	    	ArrayList<WniosekORezerwacjeDB> wnioski=new ArrayList<WniosekORezerwacjeDB>();
	    	
	    	List<WniosekORezerwacjePozycjaDB> reservationPositions = (List<WniosekORezerwacjePozycjaDB>) DSApi.context().session().createCriteria(WniosekORezerwacjePozycjaDB.class)
					.add(Restrictions.in("id", reservationPositionIds)).list();
	    	
	    	for(WniosekORezerwacjePozycjaDB reservation :reservationPositions){
	    		WniosekORezerwacjeDB wn=(WniosekORezerwacjeDB) WniosekORezerwacjeDBDAO.findby_bd_rezerwacja_id_unique(reservation.getRezerwacjaId());
	    		if(wn!=null)wnioski.add(wn);
	    	}
	    	f.setKwotaPozostala(f.getKwotaBrutto());
	    	f.setKwotaPozostalaNetto(f.getKwotaNetto());
	    	f.setKwotaPozostalaVAT(f.getKwotaVat());
	    	
			int index = 1;
			for (WniosekORezerwacjeDB wniosek : wnioski) {
				BudzetPozycjaDB mpkItem = new BudzetPozycjaDB();
				FakturaPozycjaDB pozycjaDB=new FakturaPozycjaDB();
				mpkItem.setOkresBudzetowy(wniosek.getBudzetId());
				mpkItem.setKwotaNetto(wniosek.getKoszt());
				pozycjaDB.setKwotaNetto(wniosek.getKoszt());
				
				
				if(VatRateDB.findByErpIDUnique(wniosek.getVatstawId())!=null){
					BigDecimal vat=new BigDecimal(VatRateDB.findByErpIDUnique(wniosek.getVatstawId()).getCn());
					vat=vat.multiply(new BigDecimal(0.01));
					vat=vat.multiply(wniosek.getKoszt());
					mpkItem.setStawkaVat(wniosek.getVatstawId());
					pozycjaDB.setStawkaVat(VatRateDB.findByErpIDUnique(wniosek.getVatstawId()));
					mpkItem.setKwotaVat(vat);
					pozycjaDB.setKwotaVat(vat);
					pozycjaDB.setKwotaBrutto(wniosek.getKoszt().add(vat).setScale(4, BigDecimal.ROUND_HALF_UP));
				}
				if(wniosek.getWytworId()!=null&&wniosek.getWytworId()!=0){
					pozycjaDB.setProduktyFaktury(CostInvoiceProductDB.findByERPIDUnique(wniosek.getWytworId()));
				}
			//	if(wniosek.getZrodloId()!=null){
				//	pozycjaDB.setZrodloFinansowania(SourcesOfProjectFundingDBDAO.findByErpIDUnique(Double.valueOf(wniosek.getZrodloId().toString())));
//pozycjaDB.setZrodloZawProj(ZrodloDBDAO.findByErpIDUnique(wniosek.getZrodloId()));
			//	}
				Long budzet=null;
				Long pozycjaBudzetu=null;
				/* planowane */
				if (StringUtils.isNotBlank(wniosek.getBdPlanZamIdm()) && StringUtils.isNotBlank(wniosek.getPzpNazwa())) {
					Long bdPlanZamId = wniosek.getBdPlanZamId();
					PlanZakupowDB plan=PlanZakupowDBDAO.findByErpIDUnique(bdPlanZamId);
					if(plan!=null)budzet=BudzetKOKomorkaDBDAO.findByBudzetIdUnique(plan.getBudzet_id()).getErpId();
				/* nieplanowane */
				} else if (StringUtils.isNotBlank(wniosek.getBdBudzetKoIdm()) && StringUtils.isNotBlank(wniosek.getBkNazwa())) {
					budzet = wniosek.getBdBudzetKoId();
				}
				if(wniosek.getBdRodzajKoId()!=null){
					pozycjaBudzetu=wniosek.getBdRodzajKoId();
				}
				
				if (budzet != null) {
					mpkItem.setBudzetId(budzet);
					mpkItem.setPozycjaId(pozycjaBudzetu);
					pozycjaDB.setPozycjaBudzetu(budzet);
					pozycjaDB.setWniosekORezerwacje(wniosek);
					DSApi.context().begin();
					Persister.create(mpkItem);
					Persister.create(pozycjaDB);
					DSApi.context().commit();
					
					f.setKwotaPozostala(f.getKwotaPozostala().subtract(pozycjaDB.getKwotaBrutto()));
					f.setKwotaPozostalaNetto(f.getKwotaPozostalaNetto().subtract(pozycjaDB.getKwotaNetto()));
					f.setKwotaPozostalaVAT(f.getKwotaPozostalaVAT().subtract(pozycjaDB.getKwotaVat()));
					
					mpkValues.put(MPK_CN+ "_"+MPK_ID_CN+"_" + index, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, mpkItem.getId()));
					pozFaktValues.put(POZYCJE_FAKTURY_CN+ "_"+POZYCJE_FAKTURY_ID_CN+"_" + index, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, pozycjaDB.getId()));
				}
				
				index++;
			}
			values.get(DWR_MPK_CN).setDictionaryData(mpkValues);
			values.get(DWR_POZYCJE_FAKTURY_CN).setDictionaryData(pozFaktValues);
			f.setZaakceptowanoWnioskiORez(true);
			//fm.getDocumentKind().setOnly(fm.getDocumentId(), Collections.singletonMap(DEKRETY_FIELD, dekretyIds));
			DSApi.close();
    	} catch (EdmException e) {
    		log.error(e.getMessage(), e);
    	}
	}
}
