package pl.compan.docusafe.parametrization.invoice.jbpm;

import java.util.List;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.parametrization.invoice.InvoiceGeneralConstans;

public class DysponenciLoopDecision implements DecisionHandler{
	
	private static final long serialVersionUID = 1L;

	@Override
	public String decide(OpenExecution execution) {
		Object proc_var = execution.getVariable(InvoiceGeneralConstans.JBPM_DYSPONENCI_VAR);
		if (proc_var instanceof List)
		if (((List) proc_var).isEmpty())
			return "true";
		return "false";
	}
}
