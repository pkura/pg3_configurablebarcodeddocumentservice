package pl.compan.docusafe.parametrization.invoice.jbpm;

import java.util.List;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.invoice.InvoiceGeneralConstans;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Odpowiada za dekretacj� na dysponenta budzetow w fakturze.
 */
public class Dysponenci_AssignmentHandler implements AssignmentHandler {
	
	private static final long serialVersionUID = 1L;
	protected static Logger log = LoggerFactory.getLogger(Dysponenci_AssignmentHandler.class);


	@Override
	public void assign(Assignable assignable, OpenExecution execution) throws Exception {
		
		Object dysponenci = execution.getVariable(InvoiceGeneralConstans.JBPM_DYSPONENCI_VAR);
		
		if (dysponenci instanceof List) {
			List<Long> list = (List) dysponenci;
			if (list != null && !list.isEmpty()) {
				Long id = (Long) list.get(0);
				DSUser user = DSUser.findById(id);
				if (assignToUser(Jbpm4Utils.getDocument(execution), assignable, execution, user)) {
					execution.setVariable(
							InvoiceGeneralConstans.JBPM_DYSPONENCI_AKTUALNY,
							user.getId());
					list.remove(0);
				}
				execution.setVariable(InvoiceGeneralConstans.JBPM_DYSPONENCI_VAR, list);
			}
		}
	}
	
	/**
	 * Przypisuje osob� do kt�rej zostanie skierowane pismo oraz dodaje do histori dekretacji informacje o tej czynno�ci.
	 * 
	 * @param doc
	 * @param assignable
	 * @param openExecution
	 * @param user
	 * @return true je�li nie by�o b��d�w. False je�li wyst�pi� wyj�tek.
	 */
	protected static boolean assignToUser(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, DSUser user) {
        try {
            assignable.addCandidateUser(user.getName());
            AssigneeHandler.addToHistory(openExecution, doc, user.getName(), null);
            return true;
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            return false;
        }
    }
}
