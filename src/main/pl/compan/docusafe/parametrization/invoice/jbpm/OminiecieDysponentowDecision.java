package pl.compan.docusafe.parametrization.invoice.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import org.jfree.util.Log;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.invoice.InvoiceGeneralConstans;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Ominięcie dysponentów dla faktury (invoiceGeneral) występuje gdy faktura magazynowa,
 * rozlicza zamówienie lub zapotrzebownie, faktura za media
 *
 */
public class OminiecieDysponentowDecision implements DecisionHandler {
	private final static Logger LOG = LoggerFactory.getLogger(OminiecieDysponentowDecision.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String FAKTURAMAGAZYNOWA_ID = "901";
	private static final String ERROR = "error";

	@Override
	public String decide(OpenExecution openExecution) {
		try {
			Log.debug("OminiecieDysponentowDecision - START");
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            boolean isOpened = false;

            try {
                if (!DSApi.isContextOpen()) {
                    DSApi.openAdmin();
                    isOpened = true;
                }

                OfficeDocument doc = OfficeDocument.find(docId);
                FieldsManager fm = doc.getFieldsManager();
                
                boolean fakturaMagazynowa = czyFakturaMagazynowa(fm);
				boolean zZamowienia = czyZZamowienia(fm);
				boolean zZapotrzebowania = czyZZapotrzebowania(fm);
				boolean zaMedia = czyZaMedia(fm);
				
				Log.debug("fakturaMagazynowa: " + fakturaMagazynowa);
				Log.debug("zZamowienia: " + zZamowienia);
				Log.debug("zZapotrzebowania: " + zZapotrzebowania);
				Log.debug("zaMedia: " + zaMedia);
				
				if (fakturaMagazynowa || zZamowienia || zZapotrzebowania || zaMedia) {
					Log.debug("return: " + CzyOminac.tak);
					return CzyOminac.tak;
				}
                
				Log.debug("return: " + CzyOminac.nie);
                return CzyOminac.nie;
                
            } finally {
                if (isOpened && DSApi.isContextOpen()) {
                    DSApi.close();
                }
            }

        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            return ERROR;
        } finally {
        	Log.debug("OminiecieDysponentowDecision - KONIEC");
        }
	}
	
	private boolean czyZaMedia(FieldsManager fm) throws ClassCastException, EdmException {
		return fm.getBoolean(InvoiceGeneralConstans.CZYROZLICZENIEMEDIOW_CN);
	}

	private boolean czyZZapotrzebowania(FieldsManager fm) throws ClassCastException, EdmException {
		return fm.getBoolean(InvoiceGeneralConstans.CZYZWNIOSKU_CN);
	}

	private boolean czyZZamowienia(FieldsManager fm) throws ClassCastException, EdmException {
		return fm.getBoolean(InvoiceGeneralConstans.CZYZZAMOWIENIA_CN);
	}

	private boolean czyFakturaMagazynowa(FieldsManager fm) throws EdmException {
		return fm.getStringKey(InvoiceGeneralConstans.RODZAJFAKTURY_CN).equalsIgnoreCase(FAKTURAMAGAZYNOWA_ID);
	}

	private static class CzyOminac{
		public static String tak = "true";
		public static String nie = "false";
	}

}
