package pl.compan.docusafe.parametrization.invoice.jbpm;

import java.util.LinkedList;
import java.util.List;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Lists;

/**
 * Dekretuje na u�ytkownika z grupy kt�ry jest na ga��zi u�ytkownika z dockindu.
 * Nazwa myl�ca bo mo�na wykorzysta� do innych cel�w.
 *
 */
public class KierownikJednostkiAssignmentHandler implements AssignmentHandler{
	
	private static final Logger log = LoggerFactory.getLogger(KierownikJednostkiAssignmentHandler.class);
	private static final long serialVersionUID = 1L;
	String field;
	String guidFromAdditionProperty;
	
	@Override
	public void assign(Assignable assignable, OpenExecution execution) throws Exception {
		OfficeDocument doc = Jbpm4Utils.getDocument(execution);
		FieldsManager fm = doc.getFieldsManager();
		String guid = Docusafe.getAdditionProperty(guidFromAdditionProperty);

		//wszyscy uzytkownicy z dzialu guidFromAdditionProperty
		DSDivision divi = DSDivision.find(guid);
		List<DSUser> usersFromGuid = Lists.newArrayList(divi.getUsers(true));

		DSUser pelnomocnik = DSUser.findByUsername((String) execution.getVariable("currentAssignee"));
		
		DSDivision [] dzialyPelnomocnika = pelnomocnik.getDivisionsWithoutGroup();
		
		LinkedList<DSUser> usersForPelnomocnik = Lists.newLinkedList();
		for (DSUser user : usersFromGuid)
			if(CzyJestKierownikiemDzialuPelnomocnika(dzialyPelnomocnika, user))
				usersForPelnomocnik.add(user);
		
		if(usersForPelnomocnik.isEmpty()){
			log.info("Nie znaleziono kierownika dzialu dla " + pelnomocnik.getFirstnameLastnameName() + ". Dekretacja na Admina." );
			assignable.addCandidateUser("admin");
		} else {
			assignable.addCandidateUser(usersForPelnomocnik.getFirst().getName());
		}
	}
	
	private boolean CzyJestKierownikiemDzialuPelnomocnika(DSDivision[] dzialy,
			DSUser user) {
		try {
			DSDivision root = DSDivision.find("rootdivision");
			while(root.getChildren().length==1){
				root = root.getChildren()[0];
			}
			log.info("Dzia� g��wny(root): " + root.getName());
			
			for (DSDivision dsDivision : dzialy) {
				DSDivision div = dsDivision;
				while(!div.getGuid().equals(root.getGuid())){
					if(user.inDivision(div)){
						return true;
					}
					div = div.getParent();
					log.info("Wchodze do dzialu nadrzednego: " + div.getName());
				}
			}
		} catch (EdmException e) {
			log.error("", e);
		}
		return false;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}
	
	public String getGuid() {
		return guidFromAdditionProperty;
	}

	public void setGuid(String guid) {
		this.guidFromAdditionProperty = guid;
	}
}
