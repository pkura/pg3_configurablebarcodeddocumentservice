package pl.compan.docusafe.parametrization.invoice.jbpm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.invoice.InvoiceGeneralConstans;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class UstawienieDysponentow extends AbstractEventListener{
	
	private static final long serialVersionUID = 1L;
	public static final Logger log = LoggerFactory.getLogger(UstawienieDysponentow.class);
	@Override
	public void notify(EventListenerExecution execution) throws Exception {
		OfficeDocument document = Jbpm4Utils.getDocument(execution);
		FieldsManager fm = document.getFieldsManager();
		EnumItem rodzaj=FieldsManagerUtils.getEnumItem(fm, InvoiceGeneralConstans.RODZAJFAKTURY_CN);
		String dysponentCN="";
		List<Map<String, Object>> dysponenci=null;
		if(InvoiceGeneralConstans.RodzajeFaktury.FAKTPROJEKT.zwrocId().equals(rodzaj.getId())){
			dysponentCN=InvoiceGeneralConstans.MPK_CN+"_"+InvoiceGeneralConstans.MPK_DYSPONENT_PROJ_CN;
			dysponenci = FieldsManagerUtils.getDictionaryItems(
					fm,
					"MPK",
					new FieldsManagerUtils.FieldProperty[]{
							new FieldsManagerUtils.FieldProperty(InvoiceGeneralConstans.MPK_CN+"_"+InvoiceGeneralConstans.MPK_DYSPONENT_PROJ_CN, EnumValues.class),
							new FieldsManagerUtils.FieldProperty(InvoiceGeneralConstans.MPK_CN+"_"+InvoiceGeneralConstans.MPK_AKCEPTACJA_CN, EnumValues.class)}
					);
		}
		else{
			dysponentCN=InvoiceGeneralConstans.MPK_CN+"_"+InvoiceGeneralConstans.MPK_DYSPONENT_CN;
			dysponenci = FieldsManagerUtils.getDictionaryItems(
					fm,
					"MPK",
					new FieldsManagerUtils.FieldProperty[]{
							new FieldsManagerUtils.FieldProperty(InvoiceGeneralConstans.MPK_CN+"_"+InvoiceGeneralConstans.MPK_DYSPONENT_CN, EnumValues.class),
							new FieldsManagerUtils.FieldProperty(InvoiceGeneralConstans.MPK_CN+"_"+InvoiceGeneralConstans.MPK_AKCEPTACJA_CN, EnumValues.class)}
					);
		}
		String accept = "1";
		if(dysponenci==null)log.error("NIE USTAWIONO POPRAWNIE DYSPONENTOW!!!!!");
		else{
			List<Long> proc_var = new LinkedList<Long>();
			/*if (execution.getVariable(InvoiceGeneralConstans.JBPM_DYSPONENCI_VAR) != null && !((List)execution.getVariable(InvoiceGeneralConstans.JBPM_DYSPONENCI_VAR)).isEmpty()) {
			proc_var = (List<Long>) execution.getVariable(InvoiceGeneralConstans.JBPM_DYSPONENCI_VAR);
		} else {*/
			
			if(AvailabilityManager.isAvailable("invoiceGeneral.dysponenciAkceptowanieWszystkichBudzetowPrzypisanychDoDysponenta")){
				//usuniecie powtarzajacych sie( aby nie szlo dwa razy do tego samego dysponenta)
				HashMap<String,Map<String,Object>> dysponenciPojedynczo=new HashMap<String, Map<String,Object>>();
				for (Map<String, Object> map : dysponenci) {
					EnumValues dysponent = (EnumValues) map.get(dysponentCN);
					if(dysponenciPojedynczo.containsKey(dysponent.getSelectedId()))continue;
					else{
						dysponenciPojedynczo.put(dysponent.getSelectedId(),map );
					}
				}
				dysponenci=new ArrayList<Map<String, Object>>(dysponenciPojedynczo.values());
			}

			for (Map<String, Object> map : dysponenci) {
				EnumValues dysponent = (EnumValues) map.get(dysponentCN);
				EnumValues akceptacja = (EnumValues) map.get("MPK_AKCEPTACJA");
				/*if (!akceptacja.getSelectedId().equalsIgnoreCase(accept)
						&& akceptacja.getSelectedId().isEmpty())*/
				proc_var.add(Long.parseLong(dysponent.getSelectedId()));
			}
			//		}
			execution.setVariable(InvoiceGeneralConstans.JBPM_DYSPONENCI_VAR, proc_var);
			System.out.println("Ustawiono zmienna proc_var="+proc_var+"dla procesu: "+ execution.getId());
		}
	}
}
