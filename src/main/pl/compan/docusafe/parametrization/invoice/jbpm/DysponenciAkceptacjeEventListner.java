package pl.compan.docusafe.parametrization.invoice.jbpm;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.invoice.InvoiceGeneralConstans;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class DysponenciAkceptacjeEventListner extends AbstractEventListener {
	
	private Long czyZaakceptowano;
	
	private static final long serialVersionUID = 1L;
	protected static Logger log = LoggerFactory.getLogger(DysponenciAkceptacjeEventListner.class);
	
	@Override
	public void notify(EventListenerExecution execution) throws Exception {
		OfficeDocument document = Jbpm4Utils.getDocument(execution);
		FieldsManager fm = document.getFieldsManager();
		String dysponentPoleCn=InvoiceGeneralConstans.MPK_CN+"_"+InvoiceGeneralConstans.MPK_DYSPONENT_CN;
		if(fm.getEnumItem(InvoiceGeneralConstans.RODZAJFAKTURY_CN).getId().equals(InvoiceGeneralConstans.RodzajeFaktury.FAKTPROJEKT.zwrocId())){
			dysponentPoleCn=InvoiceGeneralConstans.MPK_CN+"_"+InvoiceGeneralConstans.MPK_DYSPONENT_PROJ_CN;
		}
		List<Map<String, Object>> dysponenci = FieldsManagerUtils
				.getDictionaryItems(fm, InvoiceGeneralConstans.MPK_CN,
						new FieldsManagerUtils.FieldProperty[] {
								new FieldsManagerUtils.FieldProperty("MPK_ID"),
								new FieldsManagerUtils.FieldProperty(
										dysponentPoleCn, EnumValues.class),
								new FieldsManagerUtils.FieldProperty(
										"MPK_AKCEPTACJA", EnumValues.class) });

		try {
			DSUser current = getUserFromExecution(execution, InvoiceGeneralConstans.JBPM_DYSPONENCI_AKTUALNY);
			Long id;
			
			for (Map<String, Object> map : dysponenci) {
				Long mpkId = ((Integer) map.get("MPK_ID")).longValue();
				EnumValues dysponent = (EnumValues) map.get(dysponentPoleCn);
				EnumValues akceptacja = (EnumValues) map.get("MPK_AKCEPTACJA");
				id = Long.parseLong(dysponent.getSelectedId());
				if (id == current.getId().longValue()) {
					DictionaryField slownik = (DictionaryField) fm.getField(InvoiceGeneralConstans.MPK_CN);
					aktualizujMultiDysopnentowDB(slownik.getTable(), InvoiceGeneralConstans.MPK_AKCEPTACJA_DB, mpkId, czyZaakceptowano);
				}
			}
		} catch (UserNotFoundException userex) {
			log.error("Nie znaleziono uzytkownika.", userex);
		} catch (Exception ex) {
			log.error("Nieznany b��d.", ex);
		}
	}

	/**
	 * Wyci�ga uzytkownika z zmiennej procesu ustawionej w poprzednich krokach procesu.
	 * Zmienna ustawiona w procesie powinna by� typu Long.
	 * 
	 * @param execution
	 * @param executionVariableName 
	 * @return
	 * @throws EdmException
	 * @throws UserNotFoundException
	 */
	private DSUser getUserFromExecution(EventListenerExecution execution, String executionVariableName)
			throws EdmException, UserNotFoundException {
		Long userID = (Long) execution.getVariable(executionVariableName);
		DSUser current = DSUser.findById(userID);
		return current;
	}
	
	private void aktualizujMultiDysopnentowDB(String tableName, String column, Long id, Long value){
		try {
			String sql = "update " + tableName + " set " + column + " = ?, DATA_AKCEPT_DYSP = ? where id = ?";
			PreparedStatement ps = DSApi.context().prepareStatement(sql);
			ps.setLong(1, value);
			ps.setDate(2, new Date(Calendar.getInstance().getTimeInMillis()));
			ps.setLong(3, id);
			ps.execute();
			ps.close();
		} catch (Exception e) {
			log.error("[Faktura] Blad zapisu do bazy danych",e);
		}
	}

	public Long getCzyZaakceptowano() {
		return czyZaakceptowano;
	}

	public void setCzyZaakceptowano(Long czyZaakceptowano) {
		this.czyZaakceptowano = czyZaakceptowano;
	}
	
	/*private void aktualizujMultiDysopnentow(OfficeDocument document, FieldsManager fm, String dictionCn, Long dysponentDoEdycji, boolean czyZaakceptowal) throws Exception{
		Object o = fm.getValue(dictionCn);
		if(o instanceof List){
			for (Object wiersz : (List)o) {
				if(wiersz instanceof Map){
					EnumValues ev = (EnumValues)(((Map) wiersz).get("MPK_DYSPONENT"));
					Long dysponent = Long.parseLong(ev.getSelectedId());
					if(dysponent == dysponentDoEdycji){
						ev.setSelectedId(czyZaakceptowal?"1":"0");
					}
					((Map<String,Object>)wiersz).put("MPK_DYSPONENT", ev);
				}
			}
		}
		try {
			aktualizujMultiDysopnentowDB();
		} catch (Exception e) {
			Log.error("B��d podczas zapisywania multi slownika", e);
		}
	}*/
}
