package pl.compan.docusafe.parametrization.invoice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import pl.compan.docusafe.core.dockinds.dwr.EnumValues;

/**
 * 
 *interfejs gdzie znajduja sie wszystkie stale
 *potrzebne do Faktury(InvoiceGeneral.xml)
 *
 */
public interface InvoiceGeneralConstans {
	//krytyki(mozliwe zle dzialanie, jesli nie beda poprawnie ustawione)
	public static final String DOC_KIND_CN = "InvoiceGeneral";
	//ID WALUTA PLN
	public static final String WALUTA_PLN_ID="6";
	
	
	//nazwa_procesu
	public static final String PROCESS_NAME = "invoiceGeneral";
	public static final String STATUS_ERP = "status_erp";
	public static final String GD_DOCUMENT_TYPE = "zamdost";
	
	//rozliczenie za media
	public static final String MEDIA="MEDIA";
	public static final String MEDIA_ROZLICZENIE="rozliczenie za media";
	//tabele
	public static final String WIDOK_SIMPLE_WALUTA="simple_erp_per_docusafe_waluta_view";
	public static final String WIDOK_SIMPLE_TYPPLATNOSCI="simple_erp_per_docusafe_warpla_view";
	public static final String WIDOK_SIMPLE_VAT="simple_erp_per_docusafe_vatstaw_view";
	public static final String WIDOK_POZYCJE_DLA_BUDZETU="dsg_invoice_general_budzet_poz";
	public static final String DSG_INVOICE_GENERAL_MULTIPLE="dsg_invoice_general_multiple";
	public static final String DSG_INVOICE_GENERAL_MPK="DSG_INVOICE_GENERAL_MPK";
	public static final String WIDOK_POZYCJE_FAKTURY_BUDZETY="POZYCJE_FAKTURY_BUDZETY";
	public static final String WIDOK_MPK_PROJEKT="simple_erp_per_projekty_view";
	public static final String WIDOK_POZYCJE_FUNDUSZ="simple_erp_per_projekty_view";
	public static final String WIDOK_POZYCJE_ZLECENIA="simple_erp_per_docusafe_zlecenia_prod";
	public static final String WIDOK_MPK_BUDZET_PROJEKTU="simple_erp_per_budzet_projektu_view";
	public static final String WIDOK_MPK_ETAP="simple_erp_per_etap_projektu";
	public static final String WIDOK_MPK_ZASOB="simple_erp_per_zasoby_projektu_view";
	public static final String WIDOK_MPK_BUDZET="dsg_invoice_general_budzet";
	public static final String WIDOK_MPK_BUDZET_POZYCJA="dsg_invoice_general_budzet_poz";
	public static final String WIDOK_POZYCJE_FAKTURY_ZADANIA="simple_erp_per_docusafe_zadania";
	public static final String WIDOK_POZYCJE_ZRODLO="simple_erp_per_docusafe_zrodla";
	public static final String DSG_INVOICE_GENERAL_WYDATKI_STRUKTURALNE="dsg_INVOICE_GENERAL_WYD_STR_POZ";
	public static final String WIDOK_MPK_DYSPONENT="INVOICE_GENERAL_DYSPONENCI";
	public static final String WIDOK_MPK_DYSPONENT_PROJEKTOW="INVOICE_GENERAL_DYSPON_PROJ";
	public static final String WIDOK_UZYTKOWNICY_WYP_DEKRET="INVOICE_GENERAL_UZYTKOWNIK_WYP_DEKRET";
	public static final String WIDOK_WYDATKI_STRU_OBSZAR="dsg_invoice_general_wydPoz_obszar";
	public static final String WIDOK_WYDATKI_STRU_KOD="dsg_invoice_general_wydPoz_kod";
	public static final String WIDOK_WYDATKI_STRU_WYD="dsg_invoice_general_wydPoz_wydStruct";
	public static final String WIDOK_NUMERY_ZAMOWIEN="dsg_invoice_general_zamowienia";
	public static final String TABELA_POZYCJE_ZAMOWIENIA="dsg_invoice_general_poz_zam";
	public static final String TABELA_POZYCJE_FAKTURY="DSG_INVOICE_GENERAL_POZFAKT";
	public static final String TABELA_WNIOSKI_O_REZERWACJE = "dsg_invoice_general_wn_o_rezer";
	public static final String TABELA_WNIOSKI_O_REZERWACJE_POZYCJE="dsg_invoice_gen_wn_o_rez_pozycje";
	public static final String WIDOK_POZYCJE_PPROJEKT_PO_ZRODLE="simple_erp_per_proj_po_zrodl_view";
	
	public static final EnumValues EMPTY_ENUM_VALUE = new EnumValues(new HashMap<String,String>(), new ArrayList<String>());
	//statusy wraz z odpowiadajacymi id
	//ewentualne zmiany nazw statusow nalezy uwzglednic w cn-ach i w enumie
	public static String STATUS_CN="STATUS";
	public enum StatusFaktury{
		process_creation(100),
		rejestracja_wstepna(114),
		uzup_rejestracji(115),
		uzup_rejestracji2(116),
		uzup_opisu(117),
		akceptacja_dysp_srodk(118),
		weryfik_tryb_zam_publ(119),
		weryfik_popr_opis(120),
		akceptacja_merytoryczna(102),
		wpr_numer_tryb_postep(121),
		akceptacja_formalnorachunkowa(103),
		akceptacja_formalnorachunkowa2(104),
		akceptacja_formalnorachunkowa3(123),
		akceptacja_ostateczna(124),
		correction_by_author(101),
		zatwierdzenie_do_wyplaty(105),
		zatwierdzenie_do_wyplaty2(106),
		wyslanePrzezESB(107),
		ERP_rejected(108),
		ERP_accepted(109),
		gotowe_do_wyplaty(110),
		zaplacone(111),
		wydruk_metryczki(112),
		zakonczone(113),
		odrzucone(149),
		akceptacja_sekcji_ewidencji_majatku(127),
		akceptacja_sekcji_ewidencji_majatku_fakt_proj(128),
		wprowadzenie_budzetow_pelnomocnik(126);
		
		private final Integer id;	
		
		StatusFaktury(Integer id){
			 this.id=id;
		 }
		public Integer zwrocId(){
			return id;
		
		}

	}
	
	public static String RODZAJFAKTURY_CN="RODZAJFAKTURY";
	public enum RodzajeFaktury{
		FAKTMAG(901),
		FAKTKOSZT(902),
		FAKTPROJEKT(903),
		FAKTZASRODKI(904);
		private final Integer id;
		
		RodzajeFaktury(Integer id){
			 this.id=id;
		 }
		public Integer zwrocId(){
			return id;
		
		}
	}

	
	//statusy cn

	public static String STATUS_UTW_DOKUMENTU_CN="process_creation";
	public static String STATUS_REJ_WSTEPNA_CN="rejestracja_wstepna";
	public static String STATUS_UZUP_REJ_CN="uzup_rejestracji";
	public static String STATUS_UZUP_REJ_2_CN="uzup_rejestracji2";
	public static String STATUS_UZUP_OPISU_CN="uzup_opisu";
	public static String STATUS_AKCEPT_DYSP_SRODKOW_CN="akceptacja_dysp_srodk";
	public static String STATUS_WERYF_TRYB_ZAM_PUBL_CN="weryfik_tryb_zam_publ";
	public static String STATUS_WERYF_POPR_OPISU_CN="weryfik_popr_opis";
	public static String STATUS_AKCEPT_MERYTORYCZNA_CN="akceptacja_merytoryczna";
	public static String STATUS_WPR_NUMER_TRYB_POSTEP_CN="wpr_numer_tryb_postep";
	public static String STATUS_AKCEPT_FORM_RACH_CN="akceptacja_formalnorachunkowa";
	public static String STATUS_AKCEPT_FORM_RACH_2_CN="akceptacja_formalnorachunkowa2";
	public static String STATUS_AKCEPT_FORM_RACH_POD_WZGL_KSIEGOWYM_CN="akceptacja_formalnorachunkowa3";
	public static String STATUS_AKCEPT_OSTATECZNA_CN="akceptacja_ostateczna";
	public static String STATUS_KORETKA_AUTORA_CN="correction_by_author";
	public static String STATUS_ZATW_DO_WYPL_CN="zatwierdzenie_do_wyplaty";
	public static String STATUS_ZATW_DO_WYPL_2_CN="zatwierdzenie_do_wyplaty2";
	public static String STATUS__WYSLANE_PRZEZ_SZYNE_ESB_CN="wyslanePrzezESB";
	public static String STATUS_ODRZUCONE_ERP_CN="ERP_rejected";
	public static String STATUS_ZAKCEPT_ERP_CN="ERP_accepted";
	public static String STATUS_GOTOWE_DO_WYPLATY_ID="gotowe_do_wyplaty";
	public static String STATUS_ZAPLACONE_CN="zaplacone";
	public static String STATUS_WYDRUK_METRYCZKI_CN="wydruk_metryczki";
	public static String STATUS_ZAKONCZONE_CN="zakonczone";
	public static String STATUS_ODRZUCONE_CN="odrzucone";
	

	//cn-y slownikow dynamicznych
	public static String SLOWNIK_DYNAMICZNY_BEZPOSREDNIE_POSREDNIE="DSD_REPO116_BEZPOSREDNIEPOSREDNIE";
	public static String SLOWNIK_DYNAMICZNY_KOM_KOSZT="DSD_REPO39_KOMORKA_KOSZTOWA";
	public static String SLOWNIK_DYNAMICZNY_GUS="DSD_REPO67_GUS";
	public static String SLOWNIK_DYNAMICZNY_RODZAJ_KOSZTOW_ZW_Z_PR="DSD_REPO118_RODZAJ_KOSZTOW_ZW_Z_PR";
	public static String SLOWNIK_DYNAMICZNY_ZADANIA_PROJEKTOWE="DSD_REPO173_ZADANIA_DO_PROJEKTOW";
	public static String SLOWNIK_DYNAMICZNY_SYSTEMY_STUDIOW="DSD_REPO115_SYSTEM_STUDIOW";
	public static String SLOWNIK_DYNAMICZNY_KOSZTY_DS="DSD_REPO166_KOSZTY_DS";
	public static String SLOWNIK_DYNAMICZNY_KWALIFIKACJA_KOSZTOW="DSD_REPO119_KWALIFIKOWALNENIEKWALIFIKOWALNE";
	
	public static final long REPOSITORY_POSITION_CONTEXT_ID = 6l;
	//cn-y pol nieslownikowoych dla InvoiceGeneral 
	public static String DATA_WPLYWU_CN="F_DATA_WPLYWU";
	public static String KWOTA_NETTO_CN="KWOTA_NETTO";
	public static String KWOTA_VAT_CN="VAT";
	public static String KWOTA_BRUTTO_CN="KWOTA_BRUTTO";
	public static String WALUTA_CN="WALUTA";
	public static String TERMIN_PLATNOSCI_CN="TERMIN_PLATNOSCI";
	public static String FAKTNAWYDZIALE_CN="FAKTNAWYDZIALE";
	public static String PELNOMOCNIK_CN="PELNOMOCNIK";
	public static String NR_FAKTURY_CN="NR_FAKTURY";
	public static String BARCODE_CN="BARCODE";
	public static String BARCODE_FAKTURADB_CN="kodKreskowy";
	public static String DATA_WYSTAWIENIA_CN="DATA_WYSTAWIENIA";
	public static String KWOTA_W_WALUCIE_CN="KWOTA_W_WALUCIE";
	public static String KURS_CN="KURS";
	public static String TYPPLATNOSCI_CN="TYPPLATNOSCI";
	public static String NAZWATOWUSL_CN="NAZWATOWUSL";
	public static String DODATKOWEINFO_CN="DODATKOWEINFO";
	public static String WYDATEKSTRUKT_CN="WYDATEKSTRUKT";
	public static String KOD_KRESKOWY_CN="BARCODE";
	
	public static String WYDATEKSTRUKTPOZ_CN="WYDATEKSTRUKTPOZ";
	public static String WYDATEKSTRUKTPOZ_WYDATEK_STRUKT_CN="WYDATEK_STRUKT";
	public static String WYDATEKSTRUKTPOZ_KWOTA_BRUTTO_WYD_CN="KWOTA_BRUTTO_WYD";
	public static String WYDATEKSTRUKTPOZ_KOD_CN="KOD";
	public static String WYDATEKSTRUKTPOZ_OBSZAR_CN="OBSZAR";
	
	public static String SKIERUJUZUPOPIS_CN="SKIERUJUZUPOPIS";
	public static String OSOBAUZUPOPIS_CN="OSOBAUZUPOPIS";
	public static String NUMERPOSTPRZETARG_CN="NUMERPOSTPRZETARG";
	public static String TRYBPOSTEP_CN="TRYBPOSTEP";
	public static String WYNIKAZPOSTEPPRZETARG_CN="WYNIKAZPOSTEPPRZETARG";
	public static String PRACKSIEGOWOSC_CN="PRACKSIEGOWOSC";
	public static String GLOWNYKSIEG_CN="GLOWNYKSIEG";
	public static String FAKTZAKOSZTYTRWALE_CN="FAKTKOSZTTRWALE";
	
	public static String ZAMNAMAG_CN="ZAMNAMAG";
	public static String PRACOWNIK_CN="PRACOWNIK";
	public static String ZALICZKA_CN="ZALICZKA";
	public static String KOREKTA_CN="KOREKTA";	
	public static String KOREKTA_NUMER_CN="NUMER_FAKTURY_KOREKT";	
	
	public static String JOURNAL_CN="JOURNAL";
	public static String POSTAL_REG_NR_CN="POSTAL_REG_NR";
	public static String STANOWISKO_DOSTAW_CN="STANOWISKO_DOSTAW";
	public static String OPISMERYT_CN="OPISMERYT";
	public static String PRZEZNIUWAGI_CN="PRZEZNIUWAGI";
	public static String CZYSZABLON_CN="CZYSZABLON";
	public static String SZABLONY_CN="SZABLON";

	public static String ZAPISZ_BUTTON_CN="ZAPISZ";
	public static String KWOTAPOZOSTNETTO_CN="KWOTAPOZOSTNETTO";
	public static String KWOTAPOZOSTVAT_CN="KWOTAPOZOSTVAT";
	public static String KWOTAPOZOST_CN="KWOTAPOZOST";
	public static String POTWODZGZUMOW_CN="POTWODZGZUMOW";
	public static String POTWODZGZFAKT_CN="POTWODZGZFAKT";
	public static String SLOWNIKPROJEKTOW_CN="SLOWNIKPROJEKTOW";
	public static String SLOWNIKZADANPROJEKTOWYCH_CN="SLOWNIKZADANPROJEKTOWYCH";		
	public static String FAKTURA_SKAN_CN="FAKTURA_SKAN";
	public static String TYP_FAKTURY_WAL_CN="TYP_FAKTURY_WAL";
	public static String TYP_FAKTURY_PLN_CN="TYP_FAKTURY_PLN";
	public static String FAKE_FIELD_CN="FAKE_FIELD";
	public static String NR_WNIOSKU_CN="NR_WNIOSKU";
	public static String PRACOWNIK_DELEGACJA_CN="PRACOWNIK_DELEGACJA";
	public static String UYTKOWNIK_WYP_DEKRET="UZYTKOWNIKWYPELNIAJACYDEKRET";
	public static String NRZAMOWIENIA_CN="NRZAMOWIENIA";
	public static String CZYZZAMOWIENIA_CN="CZYZZAMOWIENIA";
	public static String AKCEPTACJA_POZYCJI_BUTTON_CN="AKCEPTACJA_POZYCJI";
	public static String UZUPELNIJ_POZYCJE_BUTTON_CN="uzupelnijPozycjeButton";
	public static String DWR_UZUPELNIJ_POZYCJE_BUTTON_CN="DWR_uzupelnijPozycjeButton";
	public static String ZAAKCEPTOWANOPOZZAM_CN="ZAAKCEPTOWANOPOZZAM";
	public static String CZYROZLICZENIEMEDIOW_CN="CZYROZLICZENIEMEDIOW";
	public static String CZYZWNIOSKU_CN="CZYZWNIOSKU";
	public static String DWR_CZYZWNIOSKU_CN="DWR_CZYZWNIOSKU";
	public static String ZAAKCEPTOWANO_WNIOSKI_O_REZERWACJE_CN="ZaakceptowanoWnioskiORez";
	
	public static String DSG_INVOICE_GENERAL_MULTIPLE_DOCUMENT_ID="DOCUMENT_ID";
	public static String DSG_INVOICE_GENERAL_MULTIPLE_ID="ID";
	public static String DSG_INVOICE_GENERAL_MULTIPLE_FIELD_CN="FIELD_CN";
	public static String DSG_INVOICE_GENERAL_MULTIPLE_FIELD_VAL="FIELD_VAL";

	
	
	//:::::::slowniki::::::
	//adresat
	public static String ADRESAT_CN="RECIPIENT_HERE";
	public static String ADRESAT_FIRSTNAME_CN="FIRSTNAME";
	public static String ADRESAT_LASTNAME_CN="LASTNAME";
	public static String ADRESAT_DIVISION_CN="DIVISION";
	
	//nadawca  
	public static String WYSTAWCA_CN="WYSTAWCA";
//	public static String WYSTAWCA_TITLE_CN="TITLE";	
	public static String WYSTAWCA_IMIE_CN="IMIE";	
	public static String WYSTAWCA_NAZWISKO_CN="NAZWISKO";	
	public static String WYSTAWCA_NAZWA_CN="NAZWA";	
	public static String WYSTAWCA_ULICA_CN="ULICA";	
	public static String WYSTAWCA_NRDOMU_CN="NRDOMU";
	public static String WYSTAWCA_NRMIESZKANIA_CN="NRMIESZKANIA";	
	public static String WYSTAWCA_MIEJSCOWSC_CN="MIEJSCOWSC";
	public static String WYSTAWCA_NIP_CN="NIP";
	public static String WYSTAWCA_REGON_CN="REGON";
	public static String WYSTAWCA_KRAJ_CN="KRAJ";
	public static String WYSTAWCA_EMAIL_CN="EMAIL";
	public static String WYSTAWCA_PESEL_CN="PESEL";	
	public static String WYSTAWCA_FAKS_CN="FAKS";
	public static String WYSTAWCA_TELEFON_CN="TELEFON";
	public static String WYSTAWCA_NRKONTA_CN="NRKONTA";
	public static String WYSTAWCA_ERPID_CN="ERPID";
	public static String WYSTAWCA_NOWY_CN="NOWY";
//	public static String WYSTAWCA_ZIP_CN="ZIP";

	
	//pozycje zamowienia
	public static String POZYCJE_ZAMOW_CN="POZYCJE_ZAMOW";
	public static String POZYCJE_ZAMOW__ID_CN="ID";
	public static String POZYCJE_ZAMOW_POZYCJAID_CN="POZYCJAID_ZAM";
	public static String POZYCJE_ZAMOW_OKRES_CN="OKRES_BUDZETOWY_ZAM";
	public static String POZYCJE_ZAMOW_KOMORKA_BUDZETOWA_ZAM_CN="KOMORKA_BUDZETOWA_ZAM";
	public static String POZYCJE_ZAMOW_IDENTYFIK_PLAN_CN="IDENTYFIKATOR_PLANU_ZAM";
	public static String POZYCJE_ZAMOW_IDENTYFIKATOR_BUDZETU_ZAM="IDENTYFIKATOR_BUDZETU_ZAM";
	public static String POZYCJE_ZAMOW_PRODUKT_CN="PRODUKT_ZAM";
	public static String POZYCJE_ZAMOW_OPIS_CN="OPIS_ZAM";
	public static String POZYCJE_ZAMOW_ILOSC_CN="ILOSC_ZAM";
	public static String POZYCJE_ZAMOW_ILOSC_ZREAL_CN="ILOSC_ZREAL_ZAM";
	public static String POZYCJE_ZAMOW_JM_CN="JM_ZAM";
	public static String POZYCJE_ZAMOW_STAWKA_VAT_CN="STAWKA_VAT_ZAM";
	public static String POZYCJE_ZAMOW_NETTO_CN="NETTO_ZAM";
	public static String POZYCJE_ZAMOW_BRUTTO_CN="BRUTTO_ZAM";
	public static String POZYCJE_ZAMOW_UWAGI_CN="UWAGI_ZAM";

	
	//MPK   
	public static String MPK_CN="MPK";
	public static String DWR_MPK_CN="DWR_MPK";
	public static String MPK_ID_CN="ID";
	public static String MPK_OKRESBUDZETOWY_CN="OKRESBUDZET";
	public static String MPK_BUDZET_CN="BUDZETID";
	public static String MPK_POZYCJA_CN="POZYCJAID";
	public static String MPK_PRZEZNACZENIE_CN="PRZEZNACZENIE";
	public static String MPK_KWOTANETTO_CN="KWOTANETTO";
	public static String MPK_KWOTAVAT_CN="KWOTAVAT";
	public static String MPK_PROJEKT_CN="PROJEKT";
	public static String MPK_DYSPONENT_CN="DYSPONENT";
	public static String MPK_DYSPONENT_PROJ_CN="DYSPONENT_PROJ";
	public static String MPK_ZASOB_CN="ZASOB";
	public static String MPK_ETAP_CN="ETAP";
	public static String MPK_BUDZET_PROJEKTU_CN="BUDZET_PROJ";
	public static String MPK_AKCEPTACJA_CN="AKCEPTACJA";
	public static String MPK_AKCEPTACJA_DB="AKCEPTACJA";
	public static String MPK_STAWKA_VAT="STAWKA_BUDZET";
	public static String MPK_DATA_AKCEPT_DYSPONENTA="DATA_AKCEPT_DYSP";
	
	
	//REALIZACJA BUDZETU  
	public static String REALIZACJA_BUDZETU_CN="REALIZACJA_BUDZETU";
	public static String REALIZACJA_BUDZETU_ID_CN="ID";
	public static String REALIZACJA_BUDZETU_NAZWABUDZETU_CN="NAZWABUDZETU";
	public static String REALIZACJA_BUDZETU_NAZWAPOZYCJI_CN="NAZWAPOZYCJI";
	public static String REALIZACJA_BUDZETU_PLANPOZYCJI_CN="PLANPOZYCJI";
	public static String REALIZACJA_BUDZETU_WYKONANIEPOZYCJI_CN="WYKONANIEPOZYCJI";
	public static String REALIZACJA_BUDZETU_POZOSTALO_CN="POZOSTALO";
	
	//POZYCJE FAKTURY  
	public static String POZYCJE_FAKTURY_CN="POZYCJE_FAKTURY";
	public static String DWR_POZYCJE_FAKTURY_CN="DWR_POZYCJE_FAKTURY";
	public static final String POZYCJE_FAKTURY_WITH_SUFFIX_FIELD = POZYCJE_FAKTURY_CN + "_";
	public static String POZYCJE_FAKTURY_ID_CN="ID";
	public static String POZYCJE_FAKTURY_ZADANIA_CN="ZADANIA";
	public static String POZYCJE_FAKTURY_KWALIFIKACJA_CN="KWALIFIKACJA";
	public static String POZYCJE_FAKTURY_ZADANIA_FINANSOWE_CN="ZADANIA";
	public static String POZYCJE_FAKTURY_STAWKA_CN="STAWKA";
	public static String POZYCJE_FAKTURY_KWOTANETTO_CN="KWOTANETTO";
	public static String POZYCJE_FAKTURY_KWOTAVAT_CN="KWOTAVAT";
	public static String POZYCJE_FAKTURY_KWOTABRUTTO_CN="KWOTABRUTTO";
	public static String POZYCJE_FAKTURY_OPK_CN="OPK";
	public static String POZYCJE_FAKTURY_POZFAKTURY_CN="POZFAKTURY";
	public static String POZYCJE_FAKTURY_PRZENZACZENIE_CN="PRZENZACZENIE";
	public static String POZYCJE_FAKTURY_POLE_BUDZETU_CN="POZYCJAFAKPOLEBUDZET";
	public static String POZYCJE_FAKTURY_PROJEKT_CN="POZYCJA_PROJEKT";
	public static String POZYCJE_FAKTURY_ZLECENIE_CN="ZLECENIE";
	public static String POZYCJE_FAKTURY_ZRODLO_CN="ZRODLO";
	public static String POZYCJE_FAKTURY_ZRODLO_ZAW_PROJ_CN="ZRODLOZAWPROJ";
	public static String POZYCJE_FAKTURY_PROJEKT_DO_ZRODLA_CN="POZYCJAPROJDOZROD";
	
	public static final String WNIOSKI_O_REZERWACJE_CN="WNIOSKI_O_REZERWACJE";
	public static final String DWR_WNIOSKI_O_REZERWACJE_CN="DWR_WNIOSKI_O_REZERWACJE";
	public static final String DWR_WNIOSKI_O_REZERWACJE_REZERWACJA_IDM_CN="DWR_WNIOSKI_O_REZERWACJE_REZERWACJA_IDM";
	public static final String WNIOSKI_O_REZERWACJE_REZERWACJA_IDM_CN="WNIOSKI_O_REZERWACJE_REZERWACJA_IDM";
	
	public static final String POZYCJE_REZERWACJI_CN="POZYCJE_REZERWACJI";
	public static final String POZYCJE_REZERWACJI_ID_CN="POZYCJE_REZERWACJI_ID";
	public static final String POZYCJE_REZERWACJI_ONLY_ID_CN="ID";
	public static final String DWR_POZYCJE_REZERWACJI_CN="DWR_POZYCJE_REZERWACJI";
	public static final String DWR_POZYCJE_REZERWACJI_ID_CN="DWR_POZYCJE_REZERWACJI_ID";
	//Sta�e dla JBPM
	public static String JBPM_DYSPONENCI_VAR = "proc_var";
	public static String JBPM_DYSPONENCI_AKTUALNY = "aktualnyDysponent";
	
	//Sposoby wyplaty
	public static String SPOSOBWYPLATY_CN = "SPOSOBWYPLATY";
	public enum SposobyWyplaty{
		ZwrotDoKasy(0, "Zwrot do kasy"),
		ZwrotNaKonto(1, "Zwrot na konto");

		private final Integer id;
		private final String title;
		SposobyWyplaty(Integer id, String title){
			this.id=id;
			this.title=title;
		}
		
		public Integer zwrocId(){
			return id;
		}
		
		public String zwrocTitle(){
			return title;
		}
	}
}
