package pl.compan.docusafe.parametrization.ifpan;

import java.math.BigDecimal;
import java.sql.Date;

public class IfpanDelegationAdvance {
	
	public static enum Type { DP,DK };
	
	private Type type;
	private String advanceNr;
	private Date registerDate;
	private Long worker;
	private String country;
	private String organizator;
	private String description;
	private BigDecimal amount;
	private BigDecimal amountCurr;
	private BigDecimal currencyRate;
	private String currency;
	private Long currencyErpId;
	private Integer delType;
	
	public IfpanDelegationAdvance(){};
	
	
	public Type getType() {
		return type;
	}
	
	public void setType(Type type) {
		this.type = type;
	}
	
	public String getAdvanceNr() {
		return advanceNr;
	}
	
	public void setAdvanceNr(String advanceNr) {
		this.advanceNr = advanceNr;
	}
	
	public Date getRegisterDate() {
		return registerDate;
	}
	
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	
	public Long getWorker() {
		return worker;
	}
	
	public void setWorker(Long worker) {
		this.worker = worker;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}
	
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public String getOrganizator() {
		return organizator;
	}


	public void setOrganizator(String organizator) {
		this.organizator = organizator;
	}


	public BigDecimal getAmountCurr() {
		return amountCurr;
	}


	public void setAmountCurr(BigDecimal amountCurr) {
		this.amountCurr = amountCurr;
	}


	public BigDecimal getCurrencyRate() {
		return currencyRate;
	}


	public void setCurrencyRate(BigDecimal currencyRate) {
		this.currencyRate = currencyRate;
	}


	public Long getCurrencyErpId() {
		return currencyErpId;
	}


	public void setCurrencyErpId(Long currencyErpId) {
		this.currencyErpId = currencyErpId;
	}


	public Integer getDelType() {
		return delType;
	}


	public void setDelType(Integer delType) {
		this.delType = delType;
	}

}
