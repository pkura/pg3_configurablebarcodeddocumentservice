package pl.compan.docusafe.parametrization.ifpan;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

/**
 * Created by Kuba-T on 11.02.14.
 */
public class FinishOrder implements ExternalActivityBehaviour
{

    @Override
    public void signal(ActivityExecution activityExecution, String s, Map<String, ?> stringMap) throws Exception {

    }

    @Override
    public void execute(ActivityExecution activityExecution) throws Exception
    {

        OfficeDocument doc = Jbpm4Utils.getDocument(activityExecution);
       
        try { 
        	doc.getFieldsManager().getEnumItemCn("OSTATNIA_FAKTURA");
        } catch (NullPointerException e)  {
        	return; 
        }; 
        if ("TAK".equalsIgnoreCase(doc.getFieldsManager().getEnumItemCn("OSTATNIA_FAKTURA")))
        {
            List<Long> zapoIds = (List<Long>) doc.getFieldsManager().getKey("ZAPOTRZEBOWANIE");

            for (Long zapoId : zapoIds)
            {
                PreparedStatement ps = DSApi.context().prepareStatement("select activity_key from DSW_JBPM_TASKLIST where document_id = ?");
                ps.setLong(1, zapoId);
                ResultSet rs = ps.executeQuery();
                rs.next();
                String zapoActivityId = rs.getString(1);
                try {
                    WorkflowFactory.getInstance().manualFinish(zapoActivityId, true);
                }
                catch (EdmException e)
                {
                    WorkflowFactory.getInstance().manualFinish(zapoActivityId, false);
                }

            }
        }
    }
}
