package pl.compan.docusafe.parametrization.ifpan;

public class Inwentarz
{
	private String name;
	private String nr;
	private String firstLastName;
	
	public Inwentarz(String name, String nr, String firstLastName)
	{
		this.name = name;
		this.nr = nr;
		this.firstLastName = firstLastName;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getNr()
	{
		return nr;
	}

	public void setNr(String nr)
	{
		this.nr = nr;
	}

	public String getFirstLastName()
	{
		return firstLastName;
	}

	public void setFirstLastName(String firstLastName)
	{
		this.firstLastName = firstLastName;
	}
	
}
