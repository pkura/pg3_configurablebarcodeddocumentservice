package pl.compan.docusafe.parametrization.ifpan;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.office.Person;

public class ErpPerson {
	private Long id;
	private Long erpId;
	private String organization;
	private String street;
	private String zip;
	private String location;
	private Integer countryId;
	private String nip;
	
	public ErpPerson() {
		
	}
	public ErpPerson(Long erpId, String organization, String street, String zip, String location, Integer countryId, String nip) {
		this.erpId = erpId;
		this.organization = organization;
		this.street = street;
		this.zip = zip;
		this.location = location;
		this.countryId = countryId;
		this.nip = nip;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getErpId() {
		return erpId;
	}
	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Integer getCountryId() {
		return countryId;
	}
	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}
	public String getNip() {
		return nip;
	}
	public void setNip(String nip) {
		this.nip = nip;
	}
	
	protected static ErpPerson find(Long id) throws EdmException {
		try
		{
			Criteria criteria = DSApi.context().session().createCriteria(ErpPerson.class);
			if (id != null)
				criteria.add(Restrictions.eq("id", id));

			List<ErpPerson> list = criteria.list();

			if (list.size() > 0)
			{
				return list.get(0);
			}

			return null;
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}
	
	protected static ErpPerson findByErpId(Long id) throws EdmException {
		try
		{
			Criteria criteria = DSApi.context().session().createCriteria(ErpPerson.class);
			if (id != null)
				criteria.add(Restrictions.eq("erpId", id));
			
			List<ErpPerson> list = criteria.list();
			
			if (list.size() > 0)
			{
				return list.get(0);
			}
			
			return null;
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}
	
	protected static ErpPerson find(String organization, String street, String zip, String location, String nip, Integer countryId) throws EdmException {
		try
		{
			Criteria criteria = DSApi.context().session().createCriteria(ErpPerson.class);
			
			criteria.add(organization!=null?Restrictions.eq("organization", organization):Restrictions.isNull("organization"));
			criteria.add(street!=null?Restrictions.eq("street", street):Restrictions.isNull("street"));
			criteria.add(zip!=null?Restrictions.eq("zip", zip):Restrictions.isNull("zip"));
			criteria.add(location!=null?Restrictions.eq("location", location):Restrictions.isNull("location"));
			criteria.add(nip!=null?Restrictions.eq("nip", nip):Restrictions.isNull("nip"));
			criteria.add(countryId!=null?Restrictions.eq("countryId", countryId):Restrictions.isNull("countryId"));
			
			List<ErpPerson> list = criteria.list();
			
			if (list.size() > 0)
			{
				return list.get(0);
			}
			
			return null;
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}
	
	public void save() throws EdmException
	{
		try
		{
			DSApi.context().session().saveOrUpdate(this);
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}

	public void updatePersonFields(Person p) {
		p.setOrganization(this.getOrganization());
		p.setNip(this.getNip());
		p.setCountry(this.getCountryId()!=null?this.getCountryId().toString():null);
		p.setWparam(this.getErpId());
		p.setStreet(this.getStreet());
		p.setZip(this.getZip());
		p.setLocation(this.getLocation());
	}
}
