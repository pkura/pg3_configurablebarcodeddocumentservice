package pl.compan.docusafe.parametrization.ifpan;

import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.jbpm4.SaveAttachmentOnDiskListener;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.service.tasklist.RefreshTaskList;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class WaitForAcceptingResearchDicrector implements ExternalActivityBehaviour
{

	private final static Logger LOG = LoggerFactory.getLogger(WaitForAcceptingResearchDicrector.class);

	@Override
	public void execute(ActivityExecution openExecution) throws Exception
	{
		boolean isOpened = true;
		try
		{
			if (!DSApi.isContextOpen())
			{
				isOpened = false;
				DSApi.openAdmin();
			}

			OfficeDocument doc = Jbpm4Utils.getDocument(openExecution);
			FieldsManager fm = doc.getFieldsManager();
			Long delegacjaID = fm.getLongKey("INSTRUKCJA_WYJAZDOWA");
			OfficeDocument delegacja = OfficeDocument.find(delegacjaID);
			FieldsManager delegacjaFM = delegacja.getFieldsManager();
			delegacjaFM.initialize();
			Integer status = delegacjaFM.getEnumItem("STATUS").getId();
			if (status != null && status != 1410 && status <= 32)
			{
				openExecution.waitForSignal();
			}
			else
			{
				openExecution.take("success");
				RefreshTaskList.addDocumentToRefresh(doc.getId());
			}
		}
		catch (Exception e)
		{
			LOG.error(e.getMessage(), e);
		}
		finally
		{
			if (DSApi.isContextOpen() && !isOpened)
			{
				DSApi.close();
			}
		}
	}

	@Override
	public void signal(ActivityExecution openExecution, String arg1, Map<String, ?> arg2) throws Exception
	{
		openExecution.take("success");
		OfficeDocument doc = Jbpm4Utils.getDocument(openExecution);
		RefreshTaskList.addDocumentToRefresh(doc.getId());
	}
}
