package pl.compan.docusafe.parametrization.ifpan;

import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import org.jbpm.api.listener.EventListenerExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.jbpm.api.activity.ActivityExecution;

import java.util.HashMap;
import java.util.Map;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class EmptyListener implements ExternalActivityBehaviour 
{
	
	public void execute(ActivityExecution execution) throws Exception {
		
	}
	
	public void signal(ActivityExecution execution, String signalName, Map<String, ?> parameters)
	throws Exception {
}
}
