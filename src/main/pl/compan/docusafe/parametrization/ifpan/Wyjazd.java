package pl.compan.docusafe.parametrization.ifpan;

import java.math.BigDecimal;

public class Wyjazd
{
	private String typ;
	private BigDecimal ilosc;
	public Wyjazd(String typ, BigDecimal ilosc)
	{
		this.typ = typ;
		this.ilosc = ilosc;
	}
	public Wyjazd()
	{
		// TODO Auto-generated constructor stub
	}
	public String getTyp()
	{
		return typ;
	}
	public void setTyp(String typ)
	{
		this.typ = typ;
	}
	public BigDecimal getIlosc()
	{
		return ilosc;
	}
	public void setIlosc(BigDecimal ilosc)
	{
		this.ilosc = ilosc;
	}

}
