package pl.compan.docusafe.parametrization.ifpan;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.record.projects.ProjectEntry;

public class ChangeBlocade extends AbstractEventListener {

	public void notify(EventListenerExecution execution) throws Exception {
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Document document = Document.find(docId);
		
		for (ProjectEntry entry : ProjectEntry.getProjectEntryByDocumentId(document.getId().toString(),null,false))
		{
			/*entry.setEntryType(ProjectEntry.COST);
			entry.setCostDate(new Date());
			entry.update();
			 */
			if (entry.getEntryType().equals(ProjectEntry.BLOCADE))
				entry.delete();
			
		}
		
	}

}
