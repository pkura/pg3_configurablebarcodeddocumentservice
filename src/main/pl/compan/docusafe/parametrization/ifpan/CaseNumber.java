package pl.compan.docusafe.parametrization.ifpan;

/**
 * Created by slim on 19.12.13.
 */
public class CaseNumber {

    private String demandNumber, itemDescription, zrodla;
    private String brutto;

    public CaseNumber()
    {

    }

    public CaseNumber(String demandNumber, String itemDescription, String zrodla, String brutto) {
        this.demandNumber = demandNumber;
        this.itemDescription = itemDescription;
        this.zrodla = zrodla;
        this.brutto = brutto;
    }

    public String getDemandNumber() {
        return demandNumber;
    }

    public void setDemandNumber(String demandNumber) {
        this.demandNumber = demandNumber;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getZrodla() {
        return zrodla;
    }

    public void setZrodla(String zrodla) {
        this.zrodla = zrodla;
    }

    public String getBrutto() {
        return brutto;
    }

    public void setBrutto(String brutto) {
        this.brutto = brutto;
    }
}
