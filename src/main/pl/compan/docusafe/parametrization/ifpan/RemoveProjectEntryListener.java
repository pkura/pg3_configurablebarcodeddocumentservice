package pl.compan.docusafe.parametrization.ifpan;

import java.util.List;
import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.record.projects.ProjectEntry;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class RemoveProjectEntryListener implements ExternalActivityBehaviour
{
	private static final Logger log = LoggerFactory.getLogger(RemoveProjectEntryListener.class);

	public void execute(ActivityExecution execution) throws Exception
	{
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		
		try {
			//String dicName = "DSG_CENTRUM_KOSZTOW_FAKTURY";
			//List<Long> dictionaryIds = (List<Long>) Document.find(docId).getFieldsManager().getKey(dicName);
			
			List<ProjectEntry> projectEntries = ProjectEntry.findByDocNr(docId);

			for (ProjectEntry entry : projectEntries)
			{
				if (entry.getDocNr().equals(docId.toString()))
					entry.delete();
			}
		} catch (Exception e) {
			log.error(e.toString());
		}
		
		execution.takeDefaultTransition();
	}


	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		
	}
}
