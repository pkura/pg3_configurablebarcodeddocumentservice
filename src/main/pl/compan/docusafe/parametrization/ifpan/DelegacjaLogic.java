package pl.compan.docusafe.parametrization.ifpan;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.dbutils.DbUtils;
import org.directwebremoting.WebContextFactory;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.chrono.BaseChronology;
import com.google.common.collect.Maps;
import org.joda.time.chrono.GregorianChronology;
import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DSUserEnumField;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import com.google.common.collect.Maps;

public class DelegacjaLogic extends AbstractDocumentLogic
{
	private static Logger log = LoggerFactory.getLogger(DelegacjaLogic.class);

	/*
	 * Lista akceptacji specjalnych
	 */
	private String PROJECT_MANAGER = "project-manager";
	private String DELEGATION = "delegation";

	private static DelegacjaLogic instance;

	public static DelegacjaLogic getInstance()
	{
		if (instance == null)
			instance = new DelegacjaLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn() + "_DOCUMENT_READ", ObjectPermission.GROUP, "Wyjazd s�u�bowy - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Wyjazd s�u�bowy  zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn() + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Wyjazd s�u�bowy  - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Wyjazd s�u�bowy  zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn() + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Wyjazd s�u�bowy  - usuwanie"));
		this.setUpPermission(document, perms);
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
		Map<String, Object> toReload = Maps.newHashMap();
		for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
		{
			if (f.getDefaultValue() != null)
			{
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
			}
		}
		DSUser user = DSApi.context().getDSUser();
		toReload.put("WORKER", user.getId());
		if (user.getDivisionsWithoutGroup().length > 0)
			toReload.put("WORKER_DIVISION", fm.getField("WORKER_DIVISION").getEnumItemByCn(user.getDivisionsWithoutGroupPosition()[0].getGuid()).getId());
		fm.reloadValues(toReload);
	}
	
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Wyjazd s�u�bowy");
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(document.getCtime()));
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(document.getCtime()));
		document.setFolder(folder);

		document.setTitle("Wyjazd s�u�bowy");
		document.setDescription("Wyjazd s�u�bowy");
	}

	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, java.util.Map<String, ?> fieldValues)
	{
		Long id = null;
		//  <enum-item id="25" cn="sdsd-decision" title="Wyliczenie kwoty zaliczki oraz jej formy"/>
		if ((fieldValues.get("STATUS") != null) && (Integer.parseInt(fieldValues.get("STATUS").toString()) == 25))
		{
			try
			{
				Map<Integer, Map<String, BigDecimal>> valuesToGenerate = new HashMap<Integer, Map<String, BigDecimal>>();

				String tableName = ((DictionaryField) DwrDictionaryFacade.dictionarieObjects.get("WYDATKOWANE_SRODKI").getOldField()).getTable();
				NativeCriteria nc = DSApi.context().createNativeCriteria(tableName, "d");
				nc.setProjection(NativeExps.projection().addProjection("d.id")).add(NativeExps.eq("d.automatic", documentId));

				CriteriaResult cr = nc.criteriaResult();
				
				while (cr.next())
				{
					id = cr.getLong(0, null);
					DwrDictionaryFacade.dictionarieObjects.get("WYDATKOWANE_SRODKI").remove(id.toString());
					log.info("DELEGACJALOGIC Usuwamy wpis slownikowyo dodany automatycznie: " + cr.getLong(0, null));
				}
				if ((fieldValues.get("ENTRY") != null) && (Integer.parseInt(fieldValues.get("ENTRY").toString()) == 1303))
				{
					log.info("ENTRY jest gotwka");
					boolean addCosts = false;
					BigDecimal sumCS = new BigDecimal(0);
					if ((fieldValues.get("DELEGATE_COSTS") != null))
					{
						ArrayList<Long> dictionaryIds = (ArrayList<Long>) fieldValues.get("DELEGATE_COSTS");

						for (Long idD : dictionaryIds)
						{
							Integer typId = Integer.parseInt(((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DELEGATE_COSTS").getValues(idD.toString()).get("DELEGATE_COSTS_TYP")).getSelectedOptions().get(0));
							Integer walutaId = Integer.parseInt(((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DELEGATE_COSTS").getValues(idD.toString()).get("DELEGATE_COSTS_WALUTA")).getSelectedOptions().get(0));

							if (typId == 11023)
							{
								BigDecimal amount = (BigDecimal) DwrDictionaryFacade.dictionarieObjects.get("DELEGATE_COSTS").getValues(idD.toString()).get("DELEGATE_COSTS_CURRENCY_COST");
								BigDecimal kurs = (BigDecimal) DwrDictionaryFacade.dictionarieObjects.get("DELEGATE_COSTS").getValues(idD.toString()).get("DELEGATE_COSTS_RATE");
								if (!valuesToGenerate.containsKey(walutaId))
								{
									Map<String, BigDecimal> values = new HashMap<String, BigDecimal>();
									values.put("AMOUNT", amount);
									values.put("KURS", kurs);
									valuesToGenerate.put(walutaId, values);
								}
								else
								{
									BigDecimal temp = valuesToGenerate.get(walutaId).get("AMOUNT").add(amount);
									valuesToGenerate.get(walutaId).put("AMOUNT", temp);
								}

							}
						}

					}
					if (!valuesToGenerate.isEmpty())
					{
						for (Integer a : valuesToGenerate.keySet())
						{
							Map<String, FieldData> valuesdic = new HashMap<String, FieldData>();
							FieldData automatic = new FieldData(Field.Type.LONG, documentId);
							valuesdic.put("AUTOMATIC", automatic);
							valuesdic.put("AMOUNT", new FieldData(Field.Type.MONEY, valuesToGenerate.get(a).get("AMOUNT")));
							valuesdic.put("KURS", new FieldData(Field.Type.MONEY, valuesToGenerate.get(a).get("KURS")));
							valuesdic.put("WALUTA", new FieldData(Field.Type.ENUM, String.valueOf(a)));
							valuesdic.put("PLN", new FieldData(Field.Type.MONEY, valuesToGenerate.get(a).get("KURS").multiply(valuesToGenerate.get(a).get("AMOUNT"))));
							valuesdic.put("TYP", new FieldData(Field.Type.ENUM, String.valueOf(19064)));

							id = DwrDictionaryFacade.dictionarieObjects.get("WYDATKOWANE_SRODKI").add(valuesdic);
							
							PreparedStatement ps = null;
							String ins = "INSERT INTO " + kind.getMultipleTableName() + " (document_id,field_cn,field_val) values (" + documentId + " , 'WYDATKOWANE_SRODKI' , " + id + " )";
							log.info("DELEGACJALOGIC INSERT INTO " + kind.getMultipleTableName() + " (document_id,field_cn,field_val) values (" + documentId + " , 'WYDATKOWANE_SRODKI' , " + id + " )");
							ps = DSApi.context().prepareStatement(ins);
							ps.execute();
							DSApi.context().closeStatement(ps);
						}
					}
				}
			}
			catch (EdmException e1)
			{
				log.error(e1.getMessage(), e1);
			}
			catch (SQLException e1)
			{
				log.error(e1.getMessage(), e1);
			}
		}
	}

	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		log.info("validateDwr.values: \n {} ", values);

		DocumentKind kind = fm.getDocumentKind();
		Field msg = null;
		try
		{
			HttpServletRequest req = WebContextFactory.get().getHttpServletRequest();
			DSApi.open(AuthUtil.getSubject(req));
			
			if (values != null)
			{
				try
				{
					List<Otrzymane> otrzymaneList = new ArrayList<Otrzymane>();
					if (DwrUtils.isNotNull(values,"DWR_ROZLICZENIE_SRO_IN"))
					{
						//advanceSumPrim = advanceSumPrim.add(getOtrzymane(values,"DWR_ROZLICZENIE_SRO_IN", otrzymaneList));
						getOtrzymane(values,"ROZLICZENIE_SRO_IN", otrzymaneList);
					}
					
					if (DwrUtils.isNotNull(values, "DWR_ROZLICZENIE_SRO_ABROAD"))
					{
						//advanceSumPrim = advanceSumPrim.add(getOtrzymane(values,"DWR_ROZLICZENIE_SRO_ABROAD"));
						getOtrzymane(values,"ROZLICZENIE_SRO_ABROAD", otrzymaneList);
					}
					
					ArrayList<DelegateCost> wydaneList = new ArrayList<DelegateCost>(); 
					getWydane(values, wydaneList);
					
					Map<String, Object> valuesForCalucalte = new HashMap<String, Object>();
					
					valuesForCalucalte.put("OTRZYMANE", otrzymaneList);
					valuesForCalucalte.put("WYDANE", wydaneList);
					
					List<Rozliczenie> rozliczenia = getRozliczeniaList(valuesForCalucalte);
					
					BigDecimal sumRozliczenieWydalemPln = new BigDecimal(0).setScale(2);
					BigDecimal sumRozliczenieDysponowalemPln = new BigDecimal(0).setScale(2);
					BigDecimal sumRozliczeniePozostaloPln = new BigDecimal(0).setScale(2);
					BigDecimal sumRozliczenieBrakujePln = new BigDecimal(0).setScale(2);
					
					Map<String, Rozliczenie> rozliczenia13 = new HashMap<String, Rozliczenie>();
					
					for (Rozliczenie roz : rozliczenia)
					{
						sumRozliczenieWydalemPln = sumRozliczenieWydalemPln.add(roz.getWydalemPln());
						sumRozliczenieDysponowalemPln = sumRozliczenieDysponowalemPln.add(roz.getDysponowalemPln());
						
						if (rozliczenia13.containsKey(roz.getWaluta()))
						{
							Rozliczenie rozz= rozliczenia13.get(roz.getWaluta());
							
							rozz.setWydalem(rozz.getWydalem().add(roz.getWydalem()));
							rozz.setWydalemPln(rozz.getWydalemPln().add(roz.getWydalemPln()));
							rozz.setDysponowalem(rozz.getDysponowalem().add(roz.getDysponowalem()));
							rozz.setDysponowalemPln(rozz.getDysponowalemPln().add(roz.getDysponowalemPln()));
						}
						else
						{
							Rozliczenie rozz =  new Rozliczenie();
							rozz.setWaluta(roz.getWaluta());
							rozz.setWydalem(roz.getWydalem());
							rozz.setWydalemPln(roz.getWydalemPln());
							rozz.setDysponowalem(roz.getDysponowalem());
							rozz.setDysponowalemPln(roz.getDysponowalemPln());

							rozliczenia13.put(roz.getWaluta(), rozz);
						}
					}
					
					BigDecimal  zero = new BigDecimal(0).setScale(2);
					BigDecimal sumaRozliczeniaPozostaloTylkoPln = new BigDecimal(0);
					BigDecimal sumaRozliczeniaPozostaloTylkoWalutoObca = new BigDecimal(0);
					for (String key : rozliczenia13.keySet())
					{
						Rozliczenie roz = rozliczenia13.get(key);
						BigDecimal dysponowlaem = roz.getDysponowalem();
						BigDecimal wydalem = roz.getWydalem();
						BigDecimal rozliczenie = dysponowlaem.subtract(wydalem).setScale(2, RoundingMode.HALF_UP);

						if (rozliczenie.compareTo(BigDecimal.ZERO) == 0)
						{
							roz.setBrakuje(zero);
							roz.setPozostalo(zero);
						}
						else if (rozliczenie.compareTo(BigDecimal.ZERO) == -1)
						{
							roz.setBrakuje(rozliczenie.abs());
							roz.setPozostalo(zero);
						}
						else
						{
							roz.setBrakuje(zero);
							roz.setPozostalo(rozliczenie.abs());									
						}
						
						BigDecimal dysponowlaemPln = roz.getDysponowalemPln();
						BigDecimal wydalemPln = roz.getWydalemPln();
						BigDecimal rozliczeniePln = dysponowlaemPln.subtract(wydalemPln).setScale(2, RoundingMode.HALF_UP);
						if (rozliczeniePln.compareTo(BigDecimal.ZERO) == 0)
						{
							roz.setBrakujePln(zero);
							roz.setPozostaloPln(zero);
						}
						else if (rozliczeniePln.compareTo(BigDecimal.ZERO) == -1)
						{
							roz.setBrakujePln(rozliczeniePln.abs());
							roz.setPozostaloPln(zero);
						}
						else
						{
							roz.setBrakujePln(zero);
							roz.setPozostaloPln(rozliczeniePln.abs());
						}
						
						sumRozliczenieBrakujePln = sumRozliczenieBrakujePln.add(roz.getBrakujePln());
						sumRozliczeniePozostaloPln = sumRozliczeniePozostaloPln.add(roz.getPozostaloPln());
						if (key.contains("PLN") || wydalem.equals(dysponowlaem))
							sumaRozliczeniaPozostaloTylkoPln = sumaRozliczeniaPozostaloTylkoPln.add(roz.getPozostaloPln());
						else
							sumaRozliczeniaPozostaloTylkoWalutoObca = sumaRozliczeniaPozostaloTylkoWalutoObca.add(roz.getPozostalo());
					}
					
					log.debug("sumaRozliczeniaPozosta�oTylkoPln: {}", sumaRozliczeniaPozostaloTylkoPln.setScale(2, RoundingMode.HALF_UP));
					log.debug("sumaRozliczeniaPozostaloTylkoWalutoObca: {}", sumaRozliczeniaPozostaloTylkoWalutoObca.setScale(2, RoundingMode.HALF_UP));
					
					values.put("DWR_POSIADANA", new FieldData(Field.Type.MONEY, sumRozliczenieDysponowalemPln.setScale(2, RoundingMode.HALF_UP)));
					values.put("DWR_WYDANA", new FieldData(Field.Type.MONEY, sumRozliczenieWydalemPln.setScale(2, RoundingMode.HALF_UP)));
					
					if ((values.get("DWR_KOMPENSATA") == null || values.get("DWR_KOMPENSATA").getBooleanData()) && (values.get("DWR_NADPLATA_WALUTA") == null || !values.get("DWR_NADPLATA_WALUTA").getEnumValuesData().getSelectedId().equals("2101")))
					{
						// pozostalo > brakuje
						if (sumRozliczeniePozostaloPln.abs().setScale(2, RoundingMode.HALF_UP).compareTo(sumRozliczenieBrakujePln.abs().setScale(2, RoundingMode.HALF_UP)) == 1)
						{
							sumRozliczeniePozostaloPln = sumRozliczeniePozostaloPln.abs().subtract(sumRozliczenieBrakujePln.abs());
							sumRozliczenieBrakujePln = BigDecimal.ZERO.setScale(2);
						}
						// pozostalo < brakuje
						else if (sumRozliczeniePozostaloPln.abs().setScale(2, RoundingMode.HALF_UP).compareTo(sumRozliczenieBrakujePln.abs().setScale(2, RoundingMode.HALF_UP)) == -1)
						{
							sumRozliczenieBrakujePln = sumRozliczenieBrakujePln.abs().subtract(sumRozliczeniePozostaloPln.abs());
							sumRozliczeniePozostaloPln = BigDecimal.ZERO.setScale(2);
						}
					}
					// Pozota�o (Zwrot do IFPAN) w przeliczeniu na PLN
					values.put("DWR_ROZLICZENIE_IFPAN", new FieldData(Field.Type.MONEY, sumRozliczeniePozostaloPln.abs().setScale(2, RoundingMode.HALF_UP)));
					// Brakuje (Zwrot dla delegowanego) w przeliczeniu na PLN
					values.put("DWR_ROZLICZENIE_DELEGOWANY", new FieldData(Field.Type.MONEY, sumRozliczenieBrakujePln.abs().setScale(2, RoundingMode.HALF_UP)));
					
					// Pozota�o (Zwrot do IFPAN) dotyczy pln - ROZLICZENIE_IFPAN_PLN - to co pozosta�o
					// ROZLICZENIE_IFPAN_PLN
					//setZwrotDoIfpan(sumaRozliczeniaPozostaloTylkoPln, 1, fm.getDocumentId());
					if (values.get("DWR_ROZLICZENIE_IFPAN_PLN") != null)
					{
						values.put("DWR_ROZLICZENIE_IFPAN_PLN", new FieldData(Field.Type.MONEY, sumaRozliczeniaPozostaloTylkoPln.abs().setScale(2, RoundingMode.HALF_UP)));
					}
					// Pozota�o (Zwrot do IFPAN) dotyczy waluty obcej w przeliczeniu na pln - ROZLICZENIE_IFPAN_WALUTA - to co pozosta�o
					// ROZLICZENIE_IFPAN_WALUTA
					if (values.get("DWR_ROZLICZENIE_IFPAN_WALUTA") != null) 
					{
						//setZwrotDoIfpan(sumaRozliczeniaPozostaloTylkoWalutoObca, 2, fm.getDocumentId());
						values.put("DWR_ROZLICZENIE_IFPAN_WALUTA", new FieldData(Field.Type.MONEY, sumaRozliczeniaPozostaloTylkoWalutoObca.abs().setScale(2, RoundingMode.HALF_UP)));
					}
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
				}
				try
				{
					if (values.get("DWR_START_DATE") != null && values.get("DWR_FINISH_DATE").getData() != null)
					{
						Date startDate = values.get("DWR_START_DATE").getDateData();
						Date finishDate = values.get("DWR_FINISH_DATE").getDateData();

						if (finishDate.before(startDate))
						{
							msg = new Field("messageField", "Data powrotu nie mo�e by� wcze�niejsza od daty wyjazdu!!! Data powrotu ustawiona na dat� wyjazdu.", Field.Type.BOOLEAN);
							values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
							values.get("DWR_FINISH_DATE").setData(startDate);
						}
					}
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
				}
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			DSApi._close();
		}
		return msg;
	}

	private void getWydane(Map<String, FieldData> values, List<DelegateCost> costs)
	{
		String dictionaryName = "WYDATKOWANE_SRODKI";
		if (!values.containsKey("DWR_" + dictionaryName)) return; 
		Map<String, FieldData> dvalues = values.get("DWR_" + dictionaryName).getDictionaryData();
		int indexOf = 1;
		
		while (index(indexOf, dvalues))
		{
			DelegateCost cost = new DelegateCost();
			String typId = null;
			if (dvalues.get(dictionaryName + "_TYP_" + indexOf).getData() != null 
					&& dvalues.get(dictionaryName + "_TYP_" + indexOf).getEnumValuesData().getSelectedId() != null 
					&& !dvalues.get(dictionaryName + "_TYP_" + indexOf).getEnumValuesData().getSelectedId().equalsIgnoreCase(""))
			{
				typId = dvalues.get(dictionaryName + "_TYP_" + indexOf).getEnumValuesData().getSelectedId();
			}
			else
			{
				++indexOf;
				continue;
			}
			
			if (dvalues.get(dictionaryName + "_WALUTA_" + indexOf).getData() != null 
					&& dvalues.get(dictionaryName + "_WALUTA_" + indexOf).getEnumValuesData().getSelectedId() != null 
					&& !dvalues.get(dictionaryName + "_WALUTA_" + indexOf).getEnumValuesData().getSelectedId().equalsIgnoreCase(""))
			{
				try
				{
					String walutaId = dvalues.get(dictionaryName + "_WALUTA_" + indexOf).getEnumValuesData().getSelectedId();
					String walutaCn = DataBaseEnumField.getEnumItemForTable("dsg_currency", Integer.parseInt(walutaId)).getCn();
					cost.setWaluta(walutaCn);
				}
				catch(EdmException e)
				{
					log.error(e.getMessage(), e);
					++indexOf;
					continue;
				}
			}
			else
			{
				++indexOf;
				continue;
			}
			
			if (dvalues.get(dictionaryName + "_KURS_" + indexOf).getData() != null)
			{
				cost.setKurs(dvalues.get(dictionaryName + "_KURS_" + indexOf).getMoneyData());
			}
			else
			{
				++indexOf;
				continue;
			}
			
			if (cost.getKurs() != null)
			{
				if (dvalues.get(dictionaryName + "_STAWKA_" + indexOf).getData() != null)
				{
					BigDecimal stawka = dvalues.get(dictionaryName + "_STAWKA_" + indexOf).getMoneyData();
					BigDecimal ilosc, procent;
					ilosc = dvalues.get(dictionaryName + "_ILOSC_" + indexOf).getData() != null ? dvalues.get(dictionaryName + "_ILOSC_" + indexOf).getMoneyData() : BigDecimal.ONE.setScale(2);
					procent = dvalues.get(dictionaryName + "_ILOSC_" + indexOf).getData() != null ? dvalues.get(dictionaryName + "_PROCENT_" + indexOf).getMoneyData() : new BigDecimal(100).setScale(2);
					BigDecimal amount = ilosc.multiply(stawka).multiply(procent.divide(new BigDecimal(100), RoundingMode.HALF_UP));
					cost.setKosztWaluta(amount.setScale(2, RoundingMode.HALF_UP));
				}
				
			}
			else
			{
				++indexOf;
				continue;
			}
			
			if (cost.getKurs() != null && cost.getKosztWaluta() != null)
			{
				BigDecimal kosztPln = cost.getKurs().multiply(cost.getKosztWaluta());
				cost.setKosztPLN(kosztPln.setScale(2, RoundingMode.HALF_UP));
			}
			else
			{
				++indexOf;
				continue;
			}
			
			if (costs.contains(cost))
			{
				DelegateCost costFromList = costs.get(costs.indexOf(cost));
				setValueDeleagetCostInDwr(cost, typId, dvalues, indexOf);
				costFromList.setKosztWaluta(costFromList.getKosztWaluta().add(cost.getKosztWaluta()));
				costFromList.setKosztPLN(costFromList.getKosztPLN().add(cost.getKosztPLN()));
			}
			else
			{
				setValueDeleagetCostInDwr(cost, typId, dvalues, indexOf);
				costs.add(cost);
			}
			++indexOf;
		}
	}
	
	private BigDecimal getWydatkowaneSrodki(Map<String, FieldData> values)
	{
		BigDecimal wydRate = new BigDecimal(0);
		BigDecimal wydAmount = new BigDecimal(0);
		HashMap<Integer, BigDecimal> wydRates = new HashMap<Integer, BigDecimal>();
		HashMap<Integer, BigDecimal> wydAmounts = new HashMap<Integer, BigDecimal>();
		Set<Integer> keysWydCosts = new HashSet<Integer>();
		String key = "";
		BigDecimal rozSum = new BigDecimal(0);
		Map<String, FieldData> dvalues = values.get("DWR_WYDATKOWANE_SRODKI").getDictionaryData();
		
		for (String o : dvalues.keySet())
		{
			if (o.contains("AMOUNT"))
			{
				key = o.substring(o.lastIndexOf('_') + 1);
				keysWydCosts.add(new Integer(Integer.parseInt(key)));
				wydAmount = dvalues.get(o).getMoneyData();
				wydAmounts.put(Integer.parseInt(key), wydAmount);
			}
			if (o.contains("KURS"))
			{
				key = o.substring(o.lastIndexOf('_') + 1);
				keysWydCosts.add(new Integer(Integer.parseInt(key)));
				wydRate = dvalues.get(o).getMoneyData();
				wydRates.put(Integer.parseInt(key), wydRate);
			}
		}

		for(Integer i: keysWydCosts){
			log.trace("OBROT PETLI");
			if(wydRates.containsKey(i) && 
			   wydAmounts.containsKey(i) && 
			   wydRates.get(i) != null &&
			   wydAmounts.get(i) != null &&
			   wydRates.get(i).compareTo(BigDecimal.ZERO) > 0 &&
			   wydAmounts.get(i).compareTo(BigDecimal.ZERO) > 0)
			{
				BigDecimal wynik = new BigDecimal(0);
				wynik = wydRates.get(i).multiply(wydAmounts.get(i));
				rozSum = rozSum.add(wynik);
				log.trace("OBLICZENIA: " + rozSum +"+" +wynik + "=" + rozSum.add(wynik));
				log.trace("SUMA=" + rozSum.toPlainString());
			}
		}
		return rozSum;
	}

	private boolean index(int indexOf, Map<String, FieldData> dvalues)
	{
		for (String o : dvalues.keySet())
		{
			if (o.contains("_"+indexOf))
				return true;
		}
		return false;
	}
	
	private void getOtrzymane(Map<String, FieldData> values, String dictionaryName, List<Otrzymane> otrzymaneList)
	{
		
		Map<String, FieldData> dvalues = values.get("DWR_" + dictionaryName).getDictionaryData();
		int indexOf = 1;
		
		while (index(indexOf, dvalues))
		{
			DelegateCost cost = new DelegateCost();
			
			if (dvalues.get(dictionaryName + "_WALUTA_" + indexOf).getData() != null 
					&& dvalues.get(dictionaryName + "_WALUTA_" + indexOf).getEnumValuesData().getSelectedId() != null 
					&& !dvalues.get(dictionaryName + "_WALUTA_" + indexOf).getEnumValuesData().getSelectedId().equalsIgnoreCase(""))
			{
				try
				{
					String walutaId = dvalues.get(dictionaryName + "_WALUTA_" + indexOf).getEnumValuesData().getSelectedId();
					String walutaCn = DataBaseEnumField.getEnumItemForTable("dsg_currency", Integer.parseInt(walutaId)).getCn();
					cost.setWaluta(walutaCn);	
				}
				catch (EdmException e)
				{
					log.error(e.getMessage(), e);
					++indexOf;
					continue;
				}
			}
			else
			{
				++indexOf;
				continue;
			}
			
			if (dvalues.get(dictionaryName + "_KURS_" + indexOf).getData() != null)
			{
				cost.setKurs(dvalues.get(dictionaryName + "_KURS_" + indexOf).getMoneyData());
			}
			else
			{
				++indexOf;
				continue;
			}
			
			if (dvalues.get(dictionaryName + "_AMOUNT_" + indexOf).getData() != null)
			{
				cost.setKosztWaluta(dvalues.get(dictionaryName + "_AMOUNT_" + indexOf).getMoneyData());
			}
			else
			{
				++indexOf;
				continue;
			}
			
			if (cost.getKurs() != null && cost.getKosztWaluta() != null)
			{
				cost.setKosztPLN(cost.getKurs().multiply(cost.getKosztWaluta()));
			}
			else
			{
				++indexOf;
				continue;
			}
			
			Otrzymane otrz = new Otrzymane();
			otrz.setKurs(cost.getKurs());
			otrz.setWaluta(cost.getWaluta());
			if (dictionaryName.equals("ROZLICZENIE_SRO_ABROAD"))
			{
				if (otrzymaneList.contains(otrz))
				{
					BigDecimal oldZaGranica = otrzymaneList.get(otrzymaneList.indexOf(otrz)).getZaGranica();
					otrzymaneList.get(otrzymaneList.indexOf(otrz)).setZaGranica(oldZaGranica.add(cost.getKosztWaluta()));
					BigDecimal oldZaGranicaPln = otrzymaneList.get(otrzymaneList.indexOf(otrz)).getZaGranicaPln();
					otrzymaneList.get(otrzymaneList.indexOf(otrz)).setZaGranicaPln(oldZaGranicaPln.add(cost.getKosztWaluta().multiply(otrz.getKurs()).setScale(2, RoundingMode.HALF_UP)));
				}
				else
				{
					otrz.setZaGranica(cost.getKosztWaluta());
					otrz.setZaGranicaPln(cost.getKosztWaluta().multiply(otrz.getKurs()).setScale(2, RoundingMode.HALF_UP));
					otrzymaneList.add(otrz);
				}
			}
			else
			{
				if (otrzymaneList.contains(otrz))
				{
					BigDecimal oldWKraju = otrzymaneList.get(otrzymaneList.indexOf(otrz)).getwKraju();
					otrzymaneList.get(otrzymaneList.indexOf(otrz)).setwKraju(oldWKraju.add(cost.getKosztWaluta()));
					BigDecimal oldWKrajuPln = otrzymaneList.get(otrzymaneList.indexOf(otrz)).getwKrajuPln();
					otrzymaneList.get(otrzymaneList.indexOf(otrz)).setwKrajuPln(oldWKrajuPln.add(cost.getKosztWaluta().multiply(otrz.getKurs()).setScale(2, RoundingMode.HALF_UP)));
				}
				else
				{
					otrz.setwKraju(cost.getKosztWaluta());
					otrz.setwKrajuPln(cost.getKosztWaluta().multiply(otrz.getKurs()).setScale(2, RoundingMode.HALF_UP));
					otrzymaneList.add(otrz);
				}
			}
			++indexOf;
		}
	}


	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		log.info("--- DelegacjaLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			
			String docId = document.getId().toString();
			String year =  DateUtils.formatYear(new Date());
			String delegationNr = docId + "/" + year;
			Map<String, Object> fieldValues = new HashMap<String, Object>();
			fieldValues.put("DELEGATION_NR", delegationNr);
			document.getDocumentKind().setOnly(document.getId(), fieldValues);
			
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

			DSUser author = DSApi.context().getDSUser();
			String user = author.getName();
			String fullName = author.getLastname() + " " + author.getFirstname();

			perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

			Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
			perms.addAll(documentPermissions);
			this.setUpPermission(document, perms);
			
			
			
			
			DSApi.context().watch(URN.create(document));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			FieldsManager fm = dockind.getFieldsManager(id);
			params.setStatus((String) fm.getValue("STATUS"));
			params.setDocumentNumber(fm.getDocumentId().toString());

			List<Long> ids = (List<Long>) fm.getKey("DELEGATE_COSTS");

			String projectsNumerList = "";
			Iterator itr = ids.iterator();
			while (itr.hasNext())
			{
				String pojId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DELEGATE_COSTS").getValues(itr.next().toString()).get("DELEGATE_COSTS_PLATNIK")).getSelectedOptions().get(0);

				if (pojId != null && !pojId.equals(""))
				{
					projectsNumerList = Project.find(Long.valueOf(pojId)).getNrIFPAN();

					if (itr.hasNext())
					{
						projectsNumerList += "...";
						break;
					}
				}
			}
			params.setAttribute(projectsNumerList, 0);
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
		return params;
	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn)
	{
		
		boolean assigned = false;

		try
		{
			if (accpetionCn.equals(PROJECT_MANAGER))
			{
				String centrumId = openExecution.getVariable("costCenterId").toString();
				String userName = Project.find(Long.valueOf(centrumId)).getProjectManager();

				log.debug("Dla ID centrum: " + centrumId + " akceptaja na: " + userName);
				assignable.addCandidateUser(userName);
				assigned = true;
			}
			else if (accpetionCn.equals(DELEGATION))
			{
				String delegationUser = doc.getFieldsManager().getEnumItem(enumCn).getCn();
				assignable.addCandidateUser(delegationUser);
				assigned = true;
			}
		}
		catch (Exception ee)
		{
			log.error(ee.getMessage() + ee);
		}
		return assigned;
	}

	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values)
	{
		Map<String, Object> results = new HashMap<String, Object>();
		try
		{
			if (dictionaryName.equals("DELEGATE_COSTS"))
			{
				try 
				{
					String projectId = ((FieldData) values.get("DELEGATE_COSTS_PLATNIK")).getEnumValuesData().getSelectedOptions().get(0);
					if (!projectId.equals(""))
					{
						String projectManager = Project.find(Long.parseLong(projectId)).getProjectManager();
						results.put("DELEGATE_COSTS_PROJECTMANAG", projectManager);
					}
				} 
				catch (NullPointerException e) 
				{
					log.debug(e.getMessage(), e);
				}
				
				BigDecimal costsCount = ((FieldData) values.get("DELEGATE_COSTS_AMOUNT")).getMoneyData();
				BigDecimal oneCostAmount = ((FieldData) values.get("DELEGATE_COSTS_COST_AMOUNT")).getMoneyData();
				BigDecimal calculatedCosts = oneCostAmount.multiply(costsCount);
				results.put("DELEGATE_COSTS_CURRENCY_COST", calculatedCosts);
				BigDecimal rate = (BigDecimal) ((FieldData) values.get("DELEGATE_COSTS_RATE")).getData();
				calculatedCosts = calculatedCosts.multiply(rate);
				results.put("DELEGATE_COSTS_COST_PLN", calculatedCosts.setScale(2, RoundingMode.HALF_UP));
			}
			else if (dictionaryName.equals("ROZLICZENIE_SRO_IN"))
			{

				BigDecimal oneCostAmount = ((FieldData) values.get("ROZLICZENIE_SRO_IN_AMOUNT")).getMoneyData();
				BigDecimal rate = ((FieldData) values.get("ROZLICZENIE_SRO_IN_KURS")).getMoneyData();
				BigDecimal calculatedCosts = oneCostAmount.multiply(rate);
				results.put("ROZLICZENIE_SRO_IN_COST_PLN", calculatedCosts.setScale(2, RoundingMode.HALF_UP));
			}
			else if (dictionaryName.equals("ROZLICZENIE_SRO_ABROAD"))
			{

				BigDecimal oneCostAmount = ((FieldData) values.get("ROZLICZENIE_SRO_ABROAD_AMOUNT")).getMoneyData();
				BigDecimal rate = ((FieldData) values.get("ROZLICZENIE_SRO_ABROAD_KURS")).getMoneyData();
				BigDecimal calculatedCosts = oneCostAmount.multiply(rate);
				results.put("ROZLICZENIE_SRO_ABROAD_PLN", calculatedCosts.setScale(2, RoundingMode.HALF_UP));
			}
			else if (dictionaryName.equals("WYDATKOWANE_SRODKI"))
			{
				BigDecimal amount = BigDecimal.ZERO.setScale(2);
				try
				{
					BigDecimal stawka = ((FieldData) values.get("WYDATKOWANE_SRODKI_STAWKA")).getMoneyData().setScale(2);
					BigDecimal ilosc = new BigDecimal(1).setScale(2);
					if (((FieldData) values.get("WYDATKOWANE_SRODKI_ILOSC")).getMoneyData() == null)
						results.put("WYDATKOWANE_SRODKI_ILOSC", ilosc);
					else
						ilosc = ((FieldData) values.get("WYDATKOWANE_SRODKI_ILOSC")).getMoneyData().setScale(2);
					
					BigDecimal procent = new BigDecimal(100).setScale(2);
					
					if (((FieldData) values.get("WYDATKOWANE_SRODKI_PROCENT")).getMoneyData() == null)
						results.put("WYDATKOWANE_SRODKI_PROCENT", procent);
					else
						procent = ((FieldData) values.get("WYDATKOWANE_SRODKI_PROCENT")).getMoneyData().setScale(2);
					
					amount = ilosc.multiply(stawka).multiply(procent.divide(new BigDecimal(100), RoundingMode.HALF_UP));
					results.put("WYDATKOWANE_SRODKI_AMOUNT", amount.setScale(2, RoundingMode.HALF_UP));
				}
				catch(Exception e)
				{
					log.error(e.getMessage(), e);
				}
				BigDecimal oneCostAmount = (BigDecimal) results.get("WYDATKOWANE_SRODKI_AMOUNT");
				BigDecimal rate = ((FieldData) values.get("WYDATKOWANE_SRODKI_KURS")).getMoneyData();
				BigDecimal calculatedCosts = oneCostAmount.multiply(rate);
				calculatedCosts = calculatedCosts.setScale(2, RoundingMode.HALF_DOWN);
				results.put("WYDATKOWANE_SRODKI_PLN", calculatedCosts);
			}
			else if (dictionaryName.equals("DEL_COUNTRY"))
			{
				Date fromDate = ((FieldData) values.get("DEL_COUNTRY_FROM_DATE")).getDateData();
				Date toDate = ((FieldData) values.get("DEL_COUNTRY_TO_DATE")).getDateData();
				if (toDate != null && fromDate != null)
				if (toDate.before(fromDate))
					results.put("DEL_COUNTRY_TO_DATE", fromDate);
			}
		}
		catch (NullPointerException e)
		{
			log.error("Lapiemy to: " + e.getMessage(), e);
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			return results;
		}
	}

	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		log.info("generate contents Delegacja ifpan: \n {} ", values);

		try
		{
			Document doc = Document.find(docId);
			FieldsManager fm = doc.getFieldsManager();

			// Wystepuje prawie w wszystkich dokumentach
			values.put("GENERATE_DATE", DateUtils.formatCommonDate(new java.util.Date()).toString());
			log.debug("GENERATE_DATE : {}", values.get("GENERATE_DATE"));
			values.put("YEAR", DateUtils.formatYear(doc.getCtime()).toString());
			log.debug("YEAR : {}", values.get("YEAR"));
			values.put("DATE", DateUtils.formatCommonDate(doc.getCtime()));
			log.debug("DATE : {}", values.get("DATE"));
			values.put("WORKER", fm.getEnumItem("WORKER").getTitle());
			log.debug("WORKER : {}", values.get("WORKER"));
			values.put("WORKER_DIVISION", fm.getEnumItem("WORKER_DIVISION").getTitle());
			log.debug("WORKER_DIVISION : {}", values.get("WORKER_DIVISION"));
			values.put("DELEGATION_NR", doc.getId());
			log.debug("DELEGATION_NR : {}", values.get("DELEGATION_NR"));

			Object countries = fm.getKey("DEL_COUNTRY");
			List<CountryDelegation> countryList = new ArrayList<CountryDelegation>();
			Map<String, Object> countryValues = new HashMap<String, Object>();
			CountryDelegation countryD = null;
			StringBuilder countryNames = new StringBuilder();
			for (Long advnaceId : (List<Long>) countries)
			{
				if (countryNames.length() != 0)
					countryNames.append(", ");
				countryD = new CountryDelegation();
				countryValues = DwrDictionaryFacade.dictionarieObjects.get("DEL_COUNTRY").getValues(advnaceId.toString());
				countryD.setCountry(((EnumValues) countryValues.get("DEL_COUNTRY_COUNTRY")).getSelectedValue());
				countryD.setCity((String) countryValues.get("DEL_COUNTRY_CITY"));
				countryD.setOrganizer((String) countryValues.get("DEL_COUNTRY_ORGANIZER_NAME"));
				countryD.setStart(DateUtils.formatJsDate((Date) countryValues.get("DEL_COUNTRY_FROM_DATE")));
				countryD.setEnd(DateUtils.formatJsDate((Date) countryValues.get("DEL_COUNTRY_TO_DATE")));
				countryD.setOrganizerCost((String) countryValues.get("DEL_COUNTRY_ORGANIZER_COST"));
				countryList.add(countryD);
				countryNames.append(countryD.getCountry());
			}
			values.put("COUNTRY", countryList);
			log.debug("COUNTRY : {}", values.get("COUNTRY"));
			values.put("COUNTRY_NAMES", countryNames);
			log.debug("COUNTRY_NAMES : {}", values.get("COUNTRY_NAMES"));
			// Wniosek Kredytowy - ��czna kwota kredytu w PLN -
			// WniosekCzasFinansowanieDelZ.rtf
			values.put("TOTAL_COST_IN_PLN", getTotalCreditCost((List<Long>) fm.getKey("DELEGATE_COSTS")));
			log.debug("TOTAL_COST_IN_PLN : {}", values.get("TOTAL_COST_IN_PLN"));
			int b = fm.getEnumItem("STATUS").getId().compareTo(65);
			int a = fm.getEnumItem("STATUS").getId().compareTo(100);

			// Lista wszystkich kosztow
			ArrayList<DelegateCost> costs = getDelegateCosts(fm, null, null);
			// Termin rozpoczecia i termin zakonczenia
			values.put("START_DATE", DateUtils.formatCommonDate((Date) fm.getValue("START_DATE")).toString());
			log.debug("START_DATE : {}", values.get("START_DATE"));
			values.put("FINISH_DATE", DateUtils.formatCommonDate((Date) fm.getValue("FINISH_DATE")).toString());
			log.debug("FINISH_DATE : {}", values.get("FINISH_DATE"));
			Object tripTypes = fm.getKey("TRIP_TYPE");
			Map<String, Object> tripValues = new HashMap<String, Object>();
			List<Wyjazd> wyjazdy = new ArrayList<Wyjazd>();
			for (Long advnaceId : (List<Long>) tripTypes)
			{
				tripValues = DwrDictionaryFacade.dictionarieObjects.get("TRIP_TYPE").getValues(advnaceId.toString());
				Wyjazd wyjazd = new Wyjazd();
				wyjazd.setIlosc((BigDecimal) tripValues.get("TRIP_TYPE_DAYS_AMOUNT"));
				wyjazd.setTyp(((EnumValues) tripValues.get("TRIP_TYPE_TYP")).getSelectedValue());
				wyjazdy.add(wyjazd);
			}
			values.put("WYJAZDY", wyjazdy);
			log.debug("WYJAZDY : {}", values.get("WYJAZDY"));
			values.put("KOSZTY", costs);
			log.debug("KOSZTY : {}", values.get("KOSZTY"));
			/**
			 * Zlecenie platnicze na odbior waluty - ZakupDewizBank.rtf
			 */
			if (fm.getEnumItem("STATUS").getId().equals(50))
			{
				List<CurrencyForRaport> currences = new ArrayList<CurrencyForRaport>();
				List<Long> delegationsAdvnacesIds = new ArrayList<Long>();
				currences = getTotalCurrencyCost(fm, "12040", delegationsAdvnacesIds);
				values.put("ZLECENIE_ID", delegationsAdvnacesIds);
				//log.debug("ZLECENIE_ID : {}", values.get("ZLECENIE_ID"));
				values.put("WALUTY", currences);

			}
			/**
			 * Potwierdzenie przekazania polecenia kasa i przelew -
			 * ZaliczkaPLWyplatKas.rtf, ZaliczkaPLWyplatKonto.rtf
			 */
			if (fm.getEnumItem("STATUS").getId().equals(110))
			{

				Object advances = fm.getKey("DELEGATE_COSTS");
				Map<String, Object> advancesValues = new HashMap<String, Object>();
				List<String> foundingSources = new ArrayList<String>();

				for (Long advnaceId : (List<Long>) advances)
				{
					advancesValues = DwrDictionaryFacade.dictionarieObjects.get("DELEGATE_COSTS").getValues(advnaceId.toString());
					String projId = ((EnumValues) advancesValues.get("DELEGATE_COSTS_PLATNIK")).getSelectedOptions().get(0);

					if (projId != null && !projId.equals(""))
					{
						String nrIfpan = Project.find(Long.valueOf(projId)).getNrIFPAN();
						if (!foundingSources.contains(nrIfpan))
							foundingSources.add(nrIfpan);
					}
				}
				// Zrodla finansowania
				values.put("FOUNDING_SOURCE", foundingSources);

				// Zaliczka wyplata w kasie PLN
				List<Long> delegationsAdvnacesIds = new ArrayList<Long>();
				List<DelegateCost> delCosts = getDelegateCosts(fm, "12041", delegationsAdvnacesIds);
				values.put("KASA_ZLECENIE_ID", delegationsAdvnacesIds);
				Map<String, DelegateCost> zaliczki = new HashMap<String, DelegateCost>();
				BigDecimal totalKasaPLN = new BigDecimal(0);
				for (DelegateCost c : delCosts)
				{
					if (zaliczki.containsKey(c.getWaluta()))
					{
						BigDecimal koszt = zaliczki.get(c.getWaluta()).getKosztWaluta();
						zaliczki.get(c.getWaluta()).setKosztWaluta(koszt.add(c.getKosztWaluta()));
					}
					else
						zaliczki.put(c.getWaluta(), c);

					totalKasaPLN = totalKasaPLN.add(c.getKosztPLN());
				}
				delCosts = new ArrayList<DelegateCost>();

				for (String key : zaliczki.keySet())
				{
					delCosts.add(zaliczki.get(key));
				}
				values.put("KASA_ZALICZKI", delCosts);
				values.put("KASA_TOTAL_COST_PLN", totalKasaPLN);
				values.put("KASA_WYDATKI", getDelegateCosts(fm, "12041", null));

				// Zaliczka wyplata przelewem PLN
				List<Long> delegationsAdvnacesIds2 = new ArrayList<Long>();
				delCosts = getDelegateCosts(fm, "12042", delegationsAdvnacesIds2);
				values.put("PRZELEW_ZLECENIE_ID", delegationsAdvnacesIds2);
				zaliczki = new HashMap<String, DelegateCost>();
				totalKasaPLN = new BigDecimal(0);

				for (DelegateCost c : delCosts)
				{
					if (zaliczki.containsKey(c.getWaluta()))
					{
						BigDecimal koszt = zaliczki.get(c.getWaluta()).getKosztWaluta();
						zaliczki.get(c.getWaluta()).setKosztWaluta(koszt.add(c.getKosztWaluta()));
					}
					else
						zaliczki.put(c.getWaluta(), c);
					totalKasaPLN = totalKasaPLN.add(c.getKosztPLN());
				}
				delCosts = new ArrayList<DelegateCost>();

				for (String key : zaliczki.keySet())
				{
					delCosts.add(zaliczki.get(key));
				}
				values.put("PRZELEW_ZALICZKI", delCosts);
				values.put("PRZELEW_TOTAL_COST_PLN", totalKasaPLN);
				values.put("PRZELEW_WYDATKI", getDelegateCosts(fm, "12042", null));

			}
			/**
			 * Rozliczenie koszt�w zagranicznej delegacji -
			 * RozliczenieKosztowZagrDelegaSluzb.rtf
			 */
			Map<String, Rozliczenie> rozliczenia13a = new HashMap<String, Rozliczenie>();
			if (fm.getEnumItem("STATUS").getId().compareTo(65) >= 0 && fm.getEnumItem("STATUS").getId().compareTo(100) <= 0)
			{
				Map<String, String> descriptions = new HashMap<String, String>();
				values.put("OTRZYMANE", getOtrzymane(fm, null, descriptions));
				values.put("WYDANE", getWydatki(fm, null, descriptions));
				
				BigDecimal sumOtrzymaneKrajPln = new BigDecimal(0);
				BigDecimal sumOtrzymaneGranicaPln = new BigDecimal(0);
				BigDecimal sumOtrzymanePln = new BigDecimal(0);

				for (Otrzymane otrz : (List<Otrzymane>) values.get("OTRZYMANE"))
				{
					sumOtrzymaneKrajPln = sumOtrzymaneKrajPln.add(otrz.getwKrajuPln());
					sumOtrzymaneGranicaPln = sumOtrzymaneGranicaPln.add(otrz.getZaGranicaPln());
				}
				values.put("SUM_OTRZYMANE_KRAJ", sumOtrzymaneKrajPln.setScale(2, RoundingMode.HALF_UP));
				values.put("SUM_OTRZYMANE_GRANICA", sumOtrzymaneGranicaPln.setScale(2, RoundingMode.HALF_UP));
				values.put("SUM_OTRZYMANE_PLN", sumOtrzymaneKrajPln.add(sumOtrzymaneGranicaPln).setScale(2, RoundingMode.HALF_UP));

				for (String key : descriptions.keySet())
				{
					values.put("OPIS_" + key, descriptions.get(key));
				}

				BigDecimal sumWydaneDiet = new BigDecimal(0);
				BigDecimal sumWydaneNoclegi = new BigDecimal(0);
				BigDecimal sumWydaneKosztPrzejazdu = new BigDecimal(0);
				BigDecimal sumWydaneDietyDojazdowe = new BigDecimal(0);
				BigDecimal sumWydaneWpisowe = new BigDecimal(0);
				BigDecimal sumWydaneWizy = new BigDecimal(0);
				BigDecimal sumWydaneInne = new BigDecimal(0);
				BigDecimal sumWydatkowanePln = new BigDecimal(0);

				for (DelegateCost wydany : (List<DelegateCost>) values.get("WYDANE"))
				{
					sumWydaneDiet = sumWydaneDiet.add(wydany.getDiety().multiply(wydany.getKurs()));
					sumWydaneNoclegi = sumWydaneNoclegi.add(wydany.getNoclegi().multiply(wydany.getKurs()));
					sumWydaneKosztPrzejazdu = sumWydaneKosztPrzejazdu.add(wydany.getKosztyPrzejazdu().multiply(wydany.getKurs()));
					sumWydaneDietyDojazdowe = sumWydaneDietyDojazdowe.add(wydany.getDietyDojazdowe().multiply(wydany.getKurs()));
					sumWydaneWpisowe = sumWydaneWpisowe.add(wydany.getWpisowe().multiply(wydany.getKurs()));
					sumWydaneWizy = sumWydaneWizy.add(wydany.getWizy().multiply(wydany.getKurs()));
					sumWydaneInne = sumWydaneInne.add(wydany.getInne().multiply(wydany.getKurs()));
				}

				values.put("SUM_WYDANE_DIET", sumWydaneDiet.setScale(2, RoundingMode.HALF_UP));
				values.put("SUM_WYDANE_NOCLEGI", sumWydaneNoclegi.setScale(2, RoundingMode.HALF_UP));
				values.put("SUM_WYDANE_KOSZT_PRZEJAZDU", sumWydaneKosztPrzejazdu.setScale(2, RoundingMode.HALF_UP));
				values.put("SUM_WYDANE_DIETY_DOJAZDOWE", sumWydaneDietyDojazdowe.setScale(2, RoundingMode.HALF_UP));
				values.put("SUM_WYDANE_WPISOWE", sumWydaneWpisowe.setScale(2, RoundingMode.HALF_UP));
				values.put("SUM_WYDANE_WIZY", sumWydaneWizy.setScale(2, RoundingMode.HALF_UP));
				values.put("SUM_WYDANE_INNE", sumWydaneInne.setScale(2, RoundingMode.HALF_UP));

				sumWydatkowanePln = sumWydaneDiet.setScale(2, RoundingMode.HALF_UP)
								.add(sumWydaneNoclegi.setScale(2, RoundingMode.HALF_UP))
								.add(sumWydaneKosztPrzejazdu.setScale(2, RoundingMode.HALF_UP))
								.add(sumWydaneDietyDojazdowe.setScale(2, RoundingMode.HALF_UP))
								.add(sumWydaneWpisowe.setScale(2, RoundingMode.HALF_UP))
								.add(sumWydaneWizy.setScale(2, RoundingMode.HALF_UP))
								.add(sumWydaneInne.setScale(2, RoundingMode.HALF_UP));
				
				values.put("SUM_WYDATKOWANE_PLN", sumWydatkowanePln.setScale(2, RoundingMode.HALF_UP));
				
				List<Rozliczenie> rozliczenia = getRozliczeniaList(values);
				
				BigDecimal sumRozliczenieWydalemPln = new BigDecimal(0);
				BigDecimal sumRozliczenieDysponowalemPln = new BigDecimal(0);
				BigDecimal sumRozliczeniePozostaloPln = new BigDecimal(0);
				BigDecimal sumRozliczenieBrakujePln = new BigDecimal(0);
				
				Map<String, Rozliczenie> rozliczenia13 = new HashMap<String, Rozliczenie>();
				//Map<String, Rozliczenie> rozliczenia14 = new HashMap<String, Rozliczenie>();
				
				for (Rozliczenie roz : rozliczenia)
				{
					sumRozliczenieWydalemPln = sumRozliczenieWydalemPln.add(roz.getWydalemPln());
					sumRozliczenieDysponowalemPln = sumRozliczenieDysponowalemPln.add(roz.getDysponowalemPln());
					
					if (rozliczenia13.containsKey(roz.getWaluta()))
					{
						Rozliczenie rozz= rozliczenia13.get(roz.getWaluta());
						
						rozz.setWydalem(rozz.getWydalem().add(roz.getWydalem()));
						rozz.setWydalemPln(rozz.getWydalemPln().add(roz.getWydalemPln()));
						rozz.setDysponowalem(rozz.getDysponowalem().add(roz.getDysponowalem()));
						rozz.setDysponowalemPln(rozz.getDysponowalemPln().add(roz.getDysponowalemPln()));
					}
					else
					{
						Rozliczenie rozz =  new Rozliczenie();
						rozz.setWaluta(roz.getWaluta());
						rozz.setWydalem(roz.getWydalem());
						rozz.setWydalemPln(roz.getWydalemPln());
						rozz.setDysponowalem(roz.getDysponowalem());
						rozz.setDysponowalemPln(roz.getDysponowalemPln());

						rozliczenia13.put(roz.getWaluta(), rozz);
					}
				}
				
				BigDecimal  zero = new BigDecimal(0).setScale(2);
				for (String key : rozliczenia13.keySet())
				{
					Rozliczenie roz = rozliczenia13.get(key);
					BigDecimal dysponowlaem = roz.getDysponowalem();
					BigDecimal wydalem = roz.getWydalem();
					BigDecimal rozliczenie = dysponowlaem.subtract(wydalem).setScale(2, RoundingMode.HALF_UP);

					if (rozliczenie.compareTo(BigDecimal.ZERO) == 0)
					{
						roz.setBrakuje(zero);
						roz.setPozostalo(zero);
					}
					else if (rozliczenie.compareTo(BigDecimal.ZERO) == -1)
					{
						roz.setBrakuje(rozliczenie.abs());
						roz.setPozostalo(zero);
					}
					else
					{
						roz.setBrakuje(zero);
						roz.setPozostalo(rozliczenie.abs());
					}
					
					BigDecimal dysponowlaemPln = roz.getDysponowalemPln();
					BigDecimal wydalemPln = roz.getWydalemPln();
					BigDecimal rozliczeniePln = dysponowlaemPln.subtract(wydalemPln).setScale(2, RoundingMode.HALF_UP);
					if (rozliczeniePln.compareTo(BigDecimal.ZERO) == 0)
					{
						roz.setBrakujePln(zero);
						roz.setPozostaloPln(zero);
					}
					else if (rozliczeniePln.compareTo(BigDecimal.ZERO) == -1)
					{
						roz.setBrakujePln(rozliczeniePln.abs());
						roz.setPozostaloPln(zero);
					}
					else
					{
						roz.setBrakujePln(zero);
						roz.setPozostaloPln(rozliczeniePln.abs());
					}
					
					sumRozliczenieBrakujePln = sumRozliczenieBrakujePln.add(roz.getBrakujePln());
					sumRozliczeniePozostaloPln = sumRozliczeniePozostaloPln.add(roz.getPozostaloPln());
				}
				
				List<Rozliczenie> rozliczenie13List = new ArrayList<Rozliczenie>();
				for (String key : rozliczenia13.keySet())
					rozliczenie13List.add(rozliczenia13.get(key));
				
				values.put("ROZLICZENIE", rozliczenie13List);

				values.put("SUM_ROZLICZENIE_WYDALEM", sumRozliczenieWydalemPln.setScale(2, RoundingMode.HALF_UP));
				values.put("SUM_ROZLICZENIE_DYSPONOWALEM", sumRozliczenieDysponowalemPln.setScale(2, RoundingMode.HALF_UP));
				values.put("SUM_ROZLICZENIE_POZOSTALO", sumRozliczeniePozostaloPln.setScale(2, RoundingMode.HALF_UP));
				values.put("SUM_ROZLICZENIE_BRAKUJE", sumRozliczenieBrakujePln.setScale(2, RoundingMode.HALF_UP));

				List<CountryDelegation> kraje = new ArrayList<CountryDelegation>();
				Integer minutes = new Integer(0);
				for (Long countryId : (List<Long>) countries)
				{
					countryValues = DwrDictionaryFacade.dictionarieObjects.get("DEL_COUNTRY").getValues(countryId.toString());
					String country = ((EnumValues) countryValues.get("DEL_COUNTRY_COUNTRY")).getSelectedValue();
					Date odData = (Date) countryValues.get("DEL_COUNTRY_FROM_DATE");
					Date doData = (Date) countryValues.get("DEL_COUNTRY_TO_DATE");
					Period h = new Period(odData.getTime(), doData.getTime(), PeriodType.minutes(), GregorianChronology.getInstance());
					minutes += h.getMinutes();
					Period houMin = new Period(odData.getTime(), doData.getTime(), PeriodType.time(), GregorianChronology.getInstance());
					String suma = String.valueOf(houMin.getHours()) + ":" + String.valueOf(houMin.getMinutes()) + ":" + String.valueOf(houMin.getSeconds());
					CountryDelegation kraj = new CountryDelegation();// new
															// KrajDelegacji(country,
															// DateUtils.formatJsDateTimeWithSeconds(odData),
															// DateUtils.formatJsDateTimeWithSeconds(doData),
															// suma);
					kraj.setCountry(country);
					kraj.setStart(DateUtils.formatJsDateTimeWithSeconds(odData));
					kraj.setEnd(DateUtils.formatJsDateTimeWithSeconds(doData));
					kraj.setCity((String) countryValues.get("DEL_COUNTRY_CITY"));
					kraj.setOrganizer((String) countryValues.get("DEL_COUNTRY_ORGANIZER_NAME"));
					kraj.setOrganizerCost((String) countryValues.get("DEL_COUNTRY_ORGANIZER_COST"));
					kraj.setSuma(suma);
					kraje.add(kraj);
				}

				values.put("DAYS_TIME", minutes / 60 % 24);
				values.put("DAYS_COUNT", minutes / 24 / 60);
				values.put("KRAJ", kraje);

				// Koszty poniesionew przez ifpan = Wydane - otrzymane za
				// granica

				BigDecimal SUM_KOSZT_PLN = new BigDecimal(0).setScale(2);
				/*for (String rozKey : rozliczenia13.keySet())
				{
					Rozliczenie roz = rozliczenia13.get(rozKey);
					DelegateCost cost = new DelegateCost();
					cost.setWaluta(roz.getWaluta());
					cost.setKoszt(roz.getWydalem().setScale(2, RoundingMode.HALF_UP));
					cost.setKosztPLN(roz.getWydalemPln().setScale(2, RoundingMode.HALF_UP));
					for (Otrzymane otrz : (List<Otrzymane>) values.get("OTRZYMANE"))
					{
						if (otrz.getWaluta().equals(roz.getWaluta()))
						{
							cost.setKoszt(cost.getKoszt().subtract(otrz.getZaGranica()));
							cost.setKosztPLN(cost.getKosztPLN().subtract(otrz.getZaGranica().multiply(otrz.getKurs())).setScale(2, RoundingMode.HALF_UP));
						}
					}
					SUM_KOSZT_PLN = SUM_KOSZT_PLN.add(cost.getKosztPLN());
				}
				
				
				values.put("SUM_KOSZT_PLN", SUM_KOSZT_PLN.setScale(2, RoundingMode.HALF_UP));
				*/
				setKosztyIfpan(values, fm);
				Rozliczenie rozliczenie1 = new Rozliczenie();
				
				rozliczenie1.setWaluta("PLN");
				rozliczenie1.setKurs(BigDecimal.ONE.setScale(4, RoundingMode.HALF_UP));
				
				boolean kompensata = fm.getBoolean("KOMPENSATA");
				
				if (kompensata)
				{
					// pozostalo > brakuje
					if (sumRozliczeniePozostaloPln.abs().setScale(2, RoundingMode.HALF_UP).compareTo(sumRozliczenieBrakujePln.abs().setScale(2, RoundingMode.HALF_UP)) == 1)
					{
						sumRozliczeniePozostaloPln = sumRozliczeniePozostaloPln.abs().subtract(sumRozliczenieBrakujePln.abs());
						sumRozliczenieBrakujePln = BigDecimal.ZERO.setScale(2);
					}
					// pozostalo < brakuje
					else if (sumRozliczeniePozostaloPln.abs().setScale(2, RoundingMode.HALF_UP).compareTo(sumRozliczenieBrakujePln.abs().setScale(2, RoundingMode.HALF_UP)) == -1)
					{
						sumRozliczenieBrakujePln = sumRozliczenieBrakujePln.abs().subtract(sumRozliczeniePozostaloPln.abs());
						sumRozliczeniePozostaloPln = BigDecimal.ZERO.setScale(2);
					}
				}
				if (sumRozliczenieBrakujePln.compareTo(zero) == 1)
				{
					// sumRozliczeniePozostaloPln dla delegowanego
					rozliczenie1.setBrakuje(sumRozliczenieBrakujePln.setScale(2, RoundingMode.HALF_UP).abs());
					rozliczenie1.setDysponowalem(sumRozliczenieBrakujePln.setScale(2, RoundingMode.HALF_UP).abs());
					List<Rozliczenie> rozliczenia3 = new ArrayList<Rozliczenie>();
					rozliczenia3.add(rozliczenie1);
					if (fm.getEnumItem("ZWROT").getCn().equals("PRZELEW"))
						values.put("PRZELEW_ROZLICZENIE", rozliczenia3);
					else
						values.put("KASA_ROZLICZENIE", rozliczenia3);
				}
				
				if (sumRozliczeniePozostaloPln.compareTo(zero) == 1)
				{
					List<Rozliczenie> zwrotZaliczkiWBanku = new ArrayList<Rozliczenie>();
					List<Rozliczenie> zwrotZaliczkiWKasie = new ArrayList<Rozliczenie>();
					
					BigDecimal pozostaloWaluta = (BigDecimal)fm.getKey("ROZLICZENIE_IFPAN_WALUTA");
					BigDecimal pozostaloPln = (BigDecimal)fm.getKey("ROZLICZENIE_IFPAN_PLN");
					
					if (pozostaloWaluta.compareTo(zero) == 1 && fm.getEnumItem("NADPLATA_WALUTA") != null && fm.getEnumItem("NADPLATA_WALUTA").getCn().equals("DEWIZA_WALUTA"))
					{
						for (String key : rozliczenia13.keySet())
						{
							Rozliczenie roz = rozliczenia13.get(key);
							BigDecimal rozliczenie = roz.getPln();
							Rozliczenie rozTmp = new Rozliczenie();
							if (roz.getWaluta().contains("PLN"))
								continue;
							rozTmp.setWaluta(roz.getWaluta());
							if (zwrotZaliczkiWBanku.contains(rozTmp))
							{
								if (roz.getPozostalo().compareTo(BigDecimal.ZERO) == 1)
								{
									int indexOf = zwrotZaliczkiWBanku.indexOf(rozTmp);
									zwrotZaliczkiWBanku.get(indexOf).setPozostalo(zwrotZaliczkiWBanku.get(indexOf).getPozostalo().add(roz.getPozostalo()));
								}
							}
							else
							{
								if (roz.getPozostalo().compareTo(BigDecimal.ZERO) == 1)
								{
									rozTmp.setPozostalo(roz.getPozostalo());
									zwrotZaliczkiWBanku.add(rozTmp);
								}
							}
						}
						values.put("NADPLATA_WALUTA", zwrotZaliczkiWBanku);
					}
					else if (pozostaloWaluta.compareTo(zero) == 1 && fm.getEnumItem("NADPLATA_WALUTA") != null &&  fm.getEnumItem("NADPLATA_WALUTA").getCn().equals("KASA"))
					{
						for (String key : rozliczenia13.keySet())
						{
							Rozliczenie roz = rozliczenia13.get(key);
							BigDecimal rozliczenie = roz.getPln();
							Rozliczenie rozTmp = new Rozliczenie();
							if (roz.getWaluta().contains("PLN"))
								continue;
							rozTmp.setWaluta("PLN");
							rozTmp.setKurs(BigDecimal.ONE.setScale(4));
							
							if (zwrotZaliczkiWKasie.contains(rozTmp))
							{
								if (roz.getPozostaloPln().compareTo(BigDecimal.ZERO) == 1)
								{
									int indexOf = zwrotZaliczkiWKasie.indexOf(rozTmp);
									zwrotZaliczkiWKasie.get(indexOf).setPozostalo(zwrotZaliczkiWKasie.get(indexOf).getPozostaloPln().add(roz.getPozostaloPln()));
									zwrotZaliczkiWKasie.get(indexOf).setPozostaloPln(zwrotZaliczkiWKasie.get(indexOf).getPozostaloPln().add(roz.getPozostaloPln()));
								}
							}
							else
							{
								if (roz.getPozostaloPln().compareTo(BigDecimal.ZERO) == 1)
								{
									rozTmp.setPozostalo(roz.getPozostaloPln());
									rozTmp.setPozostaloPln(roz.getPozostaloPln());
									zwrotZaliczkiWKasie.add(rozTmp);
								}
							}
						}
					}
					
					if (pozostaloPln.compareTo(zero) == 1 && fm.getEnumItem("NADPLATA_PLN") != null &&  fm.getEnumItem("NADPLATA_PLN").getCn().equals("KASA"))
					{
						for (String key : rozliczenia13.keySet())
						{
							Rozliczenie roz = rozliczenia13.get(key);
							BigDecimal rozliczenie = roz.getPln();
							if (!roz.getWaluta().contains("PLN"))
								continue;
							Rozliczenie rozTmp = new Rozliczenie();
							rozTmp.setWaluta("PLN");
							rozTmp.setKurs(BigDecimal.ONE.setScale(4));
							
							if (zwrotZaliczkiWKasie.contains(rozTmp))
							{
								if (roz.getPozostaloPln().compareTo(BigDecimal.ZERO) == 1)
								{
									int indexOf = zwrotZaliczkiWKasie.indexOf(rozTmp);
									zwrotZaliczkiWKasie.get(indexOf).setPozostalo(zwrotZaliczkiWKasie.get(indexOf).getPozostaloPln().add(roz.getPozostaloPln()));
									zwrotZaliczkiWKasie.get(indexOf).setPozostaloPln(zwrotZaliczkiWKasie.get(indexOf).getPozostaloPln().add(roz.getPozostaloPln()));
								}
							}
							else
							{
								if (roz.getPozostaloPln().compareTo(BigDecimal.ZERO) == 1)
								{
									rozTmp.setPozostalo(roz.getPozostaloPln());
									rozTmp.setPozostaloPln(roz.getPozostaloPln());
									zwrotZaliczkiWKasie.add(rozTmp);
								}
							}
						}
					}
					if (kompensata && sumRozliczeniePozostaloPln.compareTo(BigDecimal.ZERO.setScale(2)) == 1)
					{
						Rozliczenie rozTmp = new Rozliczenie();
						rozTmp.setWaluta("PLN");
						rozTmp.setKurs(BigDecimal.ONE.setScale(4));
						rozTmp.setPozostalo(sumRozliczeniePozostaloPln.setScale(2, RoundingMode.HALF_UP));
						rozTmp.setPozostaloPln(sumRozliczeniePozostaloPln.setScale(2, RoundingMode.HALF_UP));
						List<Rozliczenie> zwrotZaliczkiWKasie2 = new ArrayList<Rozliczenie>();
						zwrotZaliczkiWKasie2.add(rozTmp);
						values.put("NADPLATA_KASA", zwrotZaliczkiWKasie2);
					}
					else
					{
						values.put("NADPLATA_KASA", zwrotZaliczkiWKasie);
					}
				}
			}
            if (!values.containsKey("fields"))
            {
                DocFacade df= Documents.document(docId);
                values.put("fields", df.getFields());
            }
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	public static ArrayList<Rozliczenie> getRozliczeniaList(Map<String, Object> values)
	{
		ArrayList<Rozliczenie> rozliczenia = new ArrayList<Rozliczenie>();

		for (Otrzymane otrz : (List<Otrzymane>) values.get("OTRZYMANE"))
		{
			Rozliczenie rozliczenie = new Rozliczenie();
			rozliczenie.setWaluta(otrz.getWaluta());
			rozliczenie.setKurs(otrz.getKurs());
			rozliczenie.setDysponowalem(otrz.getwKraju().add(otrz.getZaGranica()));
			rozliczenie.setDysponowalemPln(otrz.getwKrajuPln().add(otrz.getZaGranicaPln()));
			
			if (rozliczenia.contains(rozliczenie))
			{
				int indexOf = rozliczenia.indexOf(rozliczenie);
				BigDecimal dyspo = rozliczenia.get(indexOf).getDysponowalem();
				rozliczenia.get(indexOf).setDysponowalem(dyspo.add(rozliczenie.getDysponowalem()));
				rozliczenia.get(indexOf).setDysponowalemPln(rozliczenia.get(indexOf).getDysponowalemPln().add(rozliczenie.getDysponowalemPln()));
			}
			else
			{
				rozliczenia.add(rozliczenie);
			}

		}

		for (DelegateCost wyd : (ArrayList<DelegateCost>) values.get("WYDANE"))
		{
			Rozliczenie rozliczenie = new Rozliczenie();
			rozliczenie.setWaluta(wyd.getWaluta());
			rozliczenie.setKurs(wyd.getKurs());
			rozliczenie.setWydalem(wyd.getKosztWaluta());
			rozliczenie.setWydalemPln(wyd.getKosztPLN());
			
			if (rozliczenia.contains(rozliczenie))
			{
				int indexOf = rozliczenia.indexOf(rozliczenie);
				BigDecimal wyda = rozliczenia.get(indexOf).getWydalem();
				rozliczenia.get(indexOf).setWydalem(wyda.add(rozliczenie.getWydalem()));
				rozliczenia.get(indexOf).setWydalemPln(rozliczenia.get(indexOf).getWydalemPln().add(rozliczenie.getWydalemPln()));
			}
			else
			{
				rozliczenia.add(rozliczenie);
			}
		}

		BigDecimal zero = new BigDecimal(0).setScale(2);

		for (Rozliczenie roz : rozliczenia)
		{
			BigDecimal dysponowalem = roz.getDysponowalem();
			BigDecimal wydalem = roz.getWydalem();

			BigDecimal rozliczenie = dysponowalem.subtract(wydalem);
			if (rozliczenie.compareTo(zero) == 0)
			{
				roz.setBrakuje(zero);
				roz.setPozostalo(zero);
			}
			else if (rozliczenie.compareTo(zero) == -1)
			{
				roz.setBrakuje(rozliczenie.abs());
				roz.setPozostalo(zero);
			}
			else
			{
				roz.setBrakuje(zero);
				roz.setPozostalo(rozliczenie.abs());
			}
		}

		return rozliczenia;
	}

	public static List<Otrzymane> getOtrzymane(FieldsManager fm, String type, Map<String, String> descriptions) throws EdmException
	{
		ArrayList<DelegateCost> costs = new ArrayList<DelegateCost>();
		Map<String, Object> advancesValues = new HashMap<String, Object>();
		List<Otrzymane> otrzymaneDlaSzablonu = new ArrayList<Otrzymane>();
		Object advances = fm.getKey("ROZLICZENIE_SRO_ABROAD");
		if (advances != null)
		{
			for (Long advnaceId : (List<Long>) advances)
			{
				if (!(advnaceId == -1))
				{
					advancesValues = DwrDictionaryFacade.dictionarieObjects.get("ROZLICZENIE_SRO_ABROAD").getValues(advnaceId.toString());

					DelegateCost cost = new DelegateCost();
					String walutaId = ((EnumValues) advancesValues.get("ROZLICZENIE_SRO_ABROAD_WALUTA")).getSelectedId();
					String walutaCn = DataBaseEnumField.getEnumItemForTable("dsg_currency", Integer.parseInt(walutaId)).getCn();
					cost.setWaluta(walutaCn);
					cost.setKurs((BigDecimal) advancesValues.get("ROZLICZENIE_SRO_ABROAD_KURS"));
					cost.setKosztWaluta((BigDecimal) advancesValues.get("ROZLICZENIE_SRO_ABROAD_AMOUNT"));
					cost.setKosztPLN((BigDecimal) advancesValues.get("ROZLICZENIE_SRO_ABROAD_PLN"));
					cost.setOpis((String) advancesValues.get("ROZLICZENIE_SRO_ABROAD_DESCRIPTION") != null ? (String) advancesValues.get("ROZLICZENIE_SRO_ABROAD_DESCRIPTION") : "");

					if (descriptions != null)
					{
						if (descriptions.containsKey("ABROAD"))
							descriptions.put("ABROAD", descriptions.get("ABROAD") + "," + cost.getOpis());
						else
							descriptions.put("ABROAD", cost.getOpis());
					}
					
					Otrzymane otrz = new Otrzymane();
					otrz.setKurs(cost.getKurs());
					otrz.setWaluta(cost.getWaluta());
					if (otrzymaneDlaSzablonu.contains(otrz))
					{
						BigDecimal oldZaGranica = otrzymaneDlaSzablonu.get(otrzymaneDlaSzablonu.indexOf(otrz)).getZaGranica();
						otrzymaneDlaSzablonu.get(otrzymaneDlaSzablonu.indexOf(otrz)).setZaGranica(oldZaGranica.add(cost.getKosztWaluta()));
						BigDecimal oldZaGranicaPln = otrzymaneDlaSzablonu.get(otrzymaneDlaSzablonu.indexOf(otrz)).getZaGranicaPln();
						otrzymaneDlaSzablonu.get(otrzymaneDlaSzablonu.indexOf(otrz)).setZaGranicaPln(oldZaGranicaPln.add(cost.getKosztWaluta().multiply(otrz.getKurs()).setScale(2, RoundingMode.HALF_UP)));
					}
					else
					{
						otrz.setZaGranica(cost.getKosztWaluta());
						otrz.setZaGranicaPln(cost.getKosztWaluta().multiply(otrz.getKurs()).setScale(2, RoundingMode.HALF_UP));
						otrzymaneDlaSzablonu.add(otrz);
					}
				}

			}
		}

		advances = fm.getKey("ROZLICZENIE_SRO_IN");
		if (advances != null)
		{
			for (Long advnaceId : (List<Long>) advances)
			{
				if (!(advnaceId == -1))
				{
					advancesValues = DwrDictionaryFacade.dictionarieObjects.get("ROZLICZENIE_SRO_IN").getValues(advnaceId.toString());

					DelegateCost cost = new DelegateCost();

					String walutaId = ((EnumValues) advancesValues.get("ROZLICZENIE_SRO_IN_WALUTA")).getSelectedId();
					String walutaCn = DataBaseEnumField.getEnumItemForTable("dsg_currency", Integer.parseInt(walutaId)).getCn();
					cost.setWaluta(walutaCn);
				
					cost.setKurs((BigDecimal) advancesValues.get("ROZLICZENIE_SRO_IN_KURS"));
					cost.setKosztWaluta((BigDecimal) advancesValues.get("ROZLICZENIE_SRO_IN_AMOUNT"));
					cost.setKosztPLN((BigDecimal) advancesValues.get("ROZLICZENIE_SRO_IN_COST_PLN"));

					Otrzymane otrz = new Otrzymane();
					otrz.setKurs(cost.getKurs());
					otrz.setWaluta(cost.getWaluta());
					if (otrzymaneDlaSzablonu.contains(otrz))
					{
						BigDecimal oldWKraju = otrzymaneDlaSzablonu.get(otrzymaneDlaSzablonu.indexOf(otrz)).getwKraju();
						otrzymaneDlaSzablonu.get(otrzymaneDlaSzablonu.indexOf(otrz)).setwKraju(oldWKraju.add(cost.getKosztWaluta()));
						BigDecimal oldWKrajuPln = otrzymaneDlaSzablonu.get(otrzymaneDlaSzablonu.indexOf(otrz)).getwKrajuPln();
						otrzymaneDlaSzablonu.get(otrzymaneDlaSzablonu.indexOf(otrz)).setwKrajuPln(oldWKrajuPln.add(cost.getKosztWaluta().multiply(otrz.getKurs()).setScale(2, RoundingMode.HALF_UP)));
					}
					else
					{
						otrz.setwKraju(cost.getKosztWaluta());
						otrz.setwKrajuPln(cost.getKosztWaluta().multiply(otrz.getKurs()).setScale(2, RoundingMode.HALF_UP));
						otrzymaneDlaSzablonu.add(otrz);
					}
				}
			}
		}

		return otrzymaneDlaSzablonu;
	}

	/** Lista wszystko kosztow delegacji */
	private static ArrayList<DelegateCost> getDelegateCosts(FieldsManager fm, String type, List<Long> delegationsAdvnacesIds) throws EdmException
	{
		Object advances = fm.getKey("DELEGATE_COSTS");
		Map<String, Object> advancesValues = new HashMap<String, Object>();
		ArrayList<DelegateCost> costs = new ArrayList<DelegateCost>();

		for (Long advnaceId : (List<Long>) advances)
		{
			advancesValues = DwrDictionaryFacade.dictionarieObjects.get("DELEGATE_COSTS").getValues(advnaceId.toString());

			DelegateCost cost = new DelegateCost();
			if (type == null || ((EnumValues) advancesValues.get("DELEGATE_COSTS_ADVANCE_KIND")).getSelectedOptions().get(0).equals(type)
					|| (((EnumValues) advancesValues.get("DELEGATE_COSTS_ADVANCE_KIND")).getSelectedOptions().get(0).equals("") && fm.getEnumItem("FORM_ADVANCE").getId().toString().equals(type)))
			{
				if (delegationsAdvnacesIds != null)
					delegationsAdvnacesIds.add(advnaceId);
				cost.setTyp(((EnumValues) advancesValues.get("DELEGATE_COSTS_TYP")).getSelectedValue());
				cost.setWaluta(((EnumValues) advancesValues.get("DELEGATE_COSTS_WALUTA")).getSelectedValue());
				cost.setKurs((BigDecimal) advancesValues.get("DELEGATE_COSTS_RATE"));
				cost.setIlosc((BigDecimal) advancesValues.get("DELEGATE_COSTS_AMOUNT"));
				cost.setKoszt((BigDecimal) advancesValues.get("DELEGATE_COSTS_COST_AMOUNT"));
				cost.setKosztWaluta((BigDecimal) advancesValues.get("DELEGATE_COSTS_CURRENCY_COST"));
				cost.setKosztPLN((BigDecimal) advancesValues.get("DELEGATE_COSTS_COST_PLN"));
				cost.setPlatnik(((EnumValues) advancesValues.get("DELEGATE_COSTS_PLATNIK")).getSelectedValue());
				cost.setAkcept((String) advancesValues.get("DELEGATE_COSTS_PROJECTMANAG"));
				if (cost.getKurs() != null)
				{
					String iloscKosztKurs = cost.getIlosc().toString().replace(".00", "") + " x " + cost.getKoszt().toString() + " x " + cost.getKurs().toString();
					cost.setIloscKosztKurs(iloscKosztKurs);
				}
				costs.add(cost);
			}
		}

		return costs;
	}

	/** Lista wszystko kosztow delegacji */
	public static ArrayList<DelegateCost> getWydatki(FieldsManager fm, String type, Map<String, String> descriptions) throws EdmException
	{
		Object advances = fm.getKey("WYDATKOWANE_SRODKI");

		Map<String, Object> advancesValues = new HashMap<String, Object>();
		ArrayList<DelegateCost> costs = new ArrayList<DelegateCost>();

		if (advances != null)
		{
			for (Long advnaceId : (List<Long>) advances)
			{
				if (!(advnaceId == -1))
				{
					advancesValues = DwrDictionaryFacade.dictionarieObjects.get("WYDATKOWANE_SRODKI").getValues(advnaceId.toString());

					DelegateCost cost = new DelegateCost();
					String typId = ((EnumValues) advancesValues.get("WYDATKOWANE_SRODKI_TYP")).getSelectedOptions().get(0);
					String walutaId = ((EnumValues) advancesValues.get("WYDATKOWANE_SRODKI_WALUTA")).getSelectedId();
					String walutaCn = DataBaseEnumField.getEnumItemForTable("dsg_currency", Integer.parseInt(walutaId)).getCn();
					cost.setWaluta(walutaCn);
					cost.setKosztWaluta((BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_AMOUNT") != null ? (BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_AMOUNT") : new BigDecimal(0));
					cost.setKosztPLN((BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_PLN") != null ? (BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_PLN") : new BigDecimal(0));
					cost.setOpis((String) advancesValues.get("WYDATKOWANE_SRODKI_DESCRIPTION") != null ? (String) advancesValues.get("WYDATKOWANE_SRODKI_DESCRIPTION") : "");
					cost.setKurs((BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_KURS") != null ? (BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_KURS") : new BigDecimal(0));
					if(typId.equals("19060") || typId.equals("19061"))
					{
						String oldOpis = cost.getOpis();
						String ilosc = advancesValues.get("WYDATKOWANE_SRODKI_ILOSC").toString();
						String stawka = advancesValues.get("WYDATKOWANE_SRODKI_STAWKA").toString();
						String procent = advancesValues.get("WYDATKOWANE_SRODKI_PROCENT").toString();
						
						cost.setOpis(oldOpis + "(" + ilosc + "x" + stawka + "x" + procent +"%)");
					}
					if (descriptions != null)
					{
						if (descriptions.containsKey(typId))
							descriptions.put(typId, descriptions.get(typId) + "," + cost.getOpis());
						else
							descriptions.put(typId, cost.getOpis());
					}
					if (costs.contains(cost))
					{
						DelegateCost costFromList = costs.get(costs.indexOf(cost));
						setValueDeleagetCost(costFromList, typId, advancesValues);
						costFromList.setKosztWaluta(costFromList.getKosztWaluta().add(cost.getKosztWaluta()));
						costFromList.setKosztPLN(costFromList.getKosztPLN().add(cost.getKosztPLN()));
					}
					else
					{
						setValueDeleagetCost(cost, typId, advancesValues);
						costs.add(cost);
					}
				}
			}
		}

		return costs;
	}

	private static void setValueDeleagetCost(DelegateCost cost, String typId, Map<String, Object> advancesValues)
	{
		if (typId.equals("19060"))
			cost.setDiety((BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_AMOUNT"));
		else if (typId.equals("19061"))
			cost.setNoclegi((BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_AMOUNT"));
		else if (typId.equals("19062"))
			cost.setKosztyPrzejazdu((BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_AMOUNT"));
		else if (typId.equals("19063"))
			cost.setDietyDojazdowe((BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_AMOUNT"));
		else if (typId.equals("19064"))
			cost.setWpisowe((BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_AMOUNT"));
		else if (typId.equals("19065"))
			cost.setWizy((BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_AMOUNT"));
		else if (typId.equals("19066"))
			cost.setInne((BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_AMOUNT"));
	}
	
	private static void setValueDeleagetCostInDwr(DelegateCost cost, String typId, Map<String, FieldData> advancesValues, int indexOf)
	{
		if (typId.equals("19060"))
			cost.setDiety(advancesValues.get("WYDATKOWANE_SRODKI_AMOUNT_"+indexOf).getMoneyData());
		else if (typId.equals("19061"))
			cost.setNoclegi(advancesValues.get("WYDATKOWANE_SRODKI_AMOUNT_"+indexOf).getMoneyData());
		else if (typId.equals("19062"))
			cost.setKosztyPrzejazdu(advancesValues.get("WYDATKOWANE_SRODKI_AMOUNT_"+indexOf).getMoneyData());
		else if (typId.equals("19063"))
			cost.setDietyDojazdowe(advancesValues.get("WYDATKOWANE_SRODKI_AMOUNT_"+indexOf).getMoneyData());
		else if (typId.equals("19064"))
			cost.setWpisowe(advancesValues.get("WYDATKOWANE_SRODKI_AMOUNT_"+indexOf).getMoneyData());
		else if (typId.equals("19065"))
			cost.setWizy(advancesValues.get("WYDATKOWANE_SRODKI_AMOUNT_"+indexOf).getMoneyData());
		else if (typId.equals("19066"))
			cost.setInne(advancesValues.get("WYDATKOWANE_SRODKI_AMOUNT_"+indexOf).getMoneyData());
	}

	/**
	 * Zwraca mape WALUTA -> ��czna suma w walucie, dla kosztow ktore maja
	 * zostac przekazane jako wyplata w banku w walucie
	 */
	private List<CurrencyForRaport>  getTotalCurrencyCost(FieldsManager fm, String type, List<Long> delegationsId) throws EdmException
	{
		// currences.add(new CurrencyForRaport(key, currencyValues.get(key), TextUtils.numberToText(currencyValues.get(key))));
		
		List<CurrencyForRaport> currences = new ArrayList<CurrencyForRaport>();
		Map<String, BigDecimal> currencyValues = new HashMap<String, BigDecimal>();
		Map<String, Object> advancesValues = new HashMap<String, Object>();

		Object advances = fm.getKey("DELEGATE_COSTS");

		if (advances == null)
		{
			log.error("EXPORT DELEGATE_COSTS document.getDELEGATE_COSTS{}, docId : {}", fm.getDocumentId());
			return null;
		}

		for (Long advnaceId : (List<Long>) advances)
		{
			advancesValues = DwrDictionaryFacade.dictionarieObjects.get("DELEGATE_COSTS").getValues(advnaceId.toString());

			String typId = ((EnumValues) advancesValues.get("DELEGATE_COSTS_ADVANCE_KIND")).getSelectedOptions().get(0);
			boolean waluta = false;
			EnumItem item = fm.getEnumItem("FORM_ADVANCE");
			if (typId.equals(type))
			{
				waluta = true;
			}
			else if (typId.equals("") && fm.getEnumItem("FORM_ADVANCE").getId().toString().equals(type))
			{
				waluta = true;
			}

			if (waluta)
			{
				delegationsId.add(advnaceId);
				String walutaCode = ((EnumValues) advancesValues.get("DELEGATE_COSTS_WALUTA")).getSelectedValue();
				CurrencyForRaport currency = new CurrencyForRaport();
				currency.setCurrency(walutaCode);
				BigDecimal currencyCost = (BigDecimal) advancesValues.get("DELEGATE_COSTS_CURRENCY_COST");
				String desc =  ((EnumValues) advancesValues.get("DELEGATE_COSTS_TYP")).getSelectedValue();
				if (currences.contains(currency))
				{
					int indexOf = currences.indexOf(currency);
					currences.get(indexOf).setTotal(currences.get(indexOf).getTotal().add(currencyCost));
					currences.get(indexOf).setTotalInText(TextUtils.numberToText(currences.get(indexOf).getTotal().add(currencyCost)));
					currences.get(indexOf).setDescription(currences.get(indexOf).getDescription()+", "+desc);	
				}
				else
				{
					currency.setTotal(currencyCost);
					currency.setTotalInText(TextUtils.numberToText(currencyCost));
					currency.setDescription(desc);
					currences.add(currency);
				}
			}
		}

		return currences;
	}

	/**
	 * @param rozliczenie_ifpan
	 * @param type 1 - pln, 2 - waluta
	 * @param docId
	 */
	private void setZwrotDoIfpan(BigDecimal rozliczenie_ifpan, int type, long docId)
	{
			PreparedStatement ps = null;
			try
			{
				boolean isopen = true;
				if (!DSApi.context().isTransactionOpen())
				{
					isopen = false;
					DSApi.context().begin();
				}
				if (type == 1)
					ps = DSApi.context().prepareStatement("update dsg_ifpan_delegacja set rozliczenie_ifpan_pln = ? where document_id = ?");
				else
					ps = DSApi.context().prepareStatement("update dsg_ifpan_delegacja set rozliczenie_ifpan_waluta = ? where document_id = ?");
				ps.setBigDecimal(1, rozliczenie_ifpan);
				ps.setLong(2, docId);
				int i = ps.executeUpdate();
				ps.close();
				if (!isopen)
				{
					DSApi.context().commit();
				}
			}
			catch (Exception e)
			{
				log.error("", e);
			}
			finally
			{
				DbUtils.closeQuietly(ps);
			}
	}
	
	/** ��czna kwota kredytu w PLN */
	private BigDecimal getTotalCreditCost(List<Long> costsId) throws EdmException
	{
		if (costsId == null)
			throw new EdmException("Brak kosztow delegacji");

		BigDecimal totalCreditCost = new BigDecimal(0);
		Map<String, Object> advancesValues = new HashMap<String, Object>();
		Project project = null;

		for (Long advnaceId : costsId)
		{
			advancesValues = DwrDictionaryFacade.dictionarieObjects.get("DELEGATE_COSTS").getValues(advnaceId.toString());
			String typId = ((EnumValues) advancesValues.get("DELEGATE_COSTS_PLATNIK")).getSelectedOptions().get(0);
			try
			{
				project = Project.find(Long.valueOf(typId));
				if (project.getProjectType().equals(Project.PROJECT_TYPE_KREDYT))
				{
					totalCreditCost = totalCreditCost.add((BigDecimal) advancesValues.get("DELEGATE_COSTS_COST_PLN"));
				}
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
			}
		}
		return totalCreditCost;
	}
	
	private void setKosztyIfpan(Map<String, Object> values, FieldsManager fm) throws EdmException
	{
		List<Koszt> koszty = new ArrayList<Koszt>();
	
		Object advances = fm.getKey("WYDATKOWANE_SRODKI");
		BigDecimal SUM_KOSZT_PLN = new BigDecimal(0).setScale(2);
		Map<String, Object> advancesValues = new HashMap<String, Object>();
		
		if (advances != null)
		{
			for (Long advnaceId : (List<Long>) advances)
			{
				if (!(advnaceId == -1))
				{
					advancesValues = DwrDictionaryFacade.dictionarieObjects.get("WYDATKOWANE_SRODKI").getValues(advnaceId.toString());
					
					String walutaId = ((EnumValues) advancesValues.get("WYDATKOWANE_SRODKI_WALUTA")).getSelectedId();
					String walutaCn = DataBaseEnumField.getEnumItemForTable("dsg_currency", Integer.parseInt(walutaId)).getCn();
					String platnik =  ((EnumValues) advancesValues.get("WYDATKOWANE_SRODKI_WYD_PLATNIK")).getSelectedValue();
					
					Koszt koszt = new Koszt();
					koszt.setPlatnik(platnik);
					koszt.setWaluta(walutaCn);
					
					BigDecimal kosztWaluta = (BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_AMOUNT") != null ? (BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_AMOUNT") : new BigDecimal(0);
					BigDecimal kosztPln = (BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_PLN") != null ? (BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_PLN") : new BigDecimal(0);
					if (koszty.contains(koszt))
					{
						int indexOf = koszty.indexOf(koszt);
						koszty.get(indexOf).setKoszt(kosztWaluta);
						koszty.get(indexOf).setKosztPln(kosztPln);
					}
					else
					{
						koszt.setKoszt(kosztWaluta);
						koszt.setKosztPln(kosztPln);
						koszty.add(koszt);
					}
					SUM_KOSZT_PLN = SUM_KOSZT_PLN.add(kosztPln);
				}
			}
		}

		values.put("SUM_KOSZT_PLN", SUM_KOSZT_PLN.setScale(2, RoundingMode.HALF_UP));
		values.put("KOSZT", koszty);
	}
}
