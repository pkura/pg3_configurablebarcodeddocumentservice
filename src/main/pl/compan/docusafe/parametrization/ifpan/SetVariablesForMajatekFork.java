package pl.compan.docusafe.parametrization.ifpan;

import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SetVariablesForMajatekFork implements ExternalActivityBehaviour
{
	private static final Logger log = LoggerFactory.getLogger(SetVariablesForMajatekFork.class);

	public void execute(ActivityExecution execution) throws Exception
	{
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Document document = Document.find(docId);
		List<String> argSets = new LinkedList<String>();
		List<Long> dictionaryIds = (List<Long>) document.getFieldsManager().getKey("DSG_CENTRUM_KOSZTOW_FAKTURY");
		
		// Je�eli dokument zawiera pozycje kosztowe
		if (dictionaryIds != null)
		{
			List<String> toAccept = new LinkedList<String>();
			
			boolean forceContinue = false;
			
			Integer majatekId = null;
			CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance();
			for (Long di : dictionaryIds)
			{				
				// pobranie id_majatku z ktorego pokrywany jest koszt
				majatekId = mpk.find(di).getRealAcceptingCentrumId(); //((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(di.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_ACCEPTINGCENTRUMID")).getSelectedOptions().get(0);
				String acceptCn = null;
				// brak "podw�jnych" przypisa� dla jednego u�ytkownika
				if (majatekId != null && majatekId != 9) {
					acceptCn = DataBaseEnumField.getEnumItemForTable("dsg_ifpan_rodzaj_majatku_trwalego", majatekId).getCn();
					for (AcceptanceCondition accept : AcceptanceCondition.find(acceptCn)) {
						if (accept.getUsername() != null && !toAccept.contains(accept.getUsername())) {
							toAccept.add(accept.getUsername());
						} else if (accept.getUsername() != null && toAccept.contains(accept.getUsername())){
							forceContinue = true;
							break;
						}
						
						if (accept.getDivisionGuid() != null && !toAccept.contains(accept.getDivisionGuid())) {
							toAccept.add(accept.getDivisionGuid());
						} else if (accept.getDivisionGuid() != null && toAccept.contains(accept.getDivisionGuid())) {
							forceContinue = true;
							break;
						}
					}
				}
				
				if (forceContinue) {
					forceContinue = false;
					continue;
				}
				
				if (majatekId != null  && majatekId != 9 && !acceptCn.equals("ND") && !argSets.contains(majatekId.toString()))
				{
					argSets.add(majatekId.toString());
					log.info("DokumentKind: {}, DocumentId: {}, Pole: 'DSG_CENTRUM_KOSZTOW_FAKTURY', Id Kosztu: {}, MajatekId: {}", document.getDocumentKind().getCn(), document.getId(), di, majatekId);
				}
				else
				{
					setNrInwentarz(di, "Nie dotyczy");
				}
			}
		}
		// ids - id projektow
		execution.setVariable("majatekIds", argSets);
		// liczba subtaskow na ptorzeby forka
		execution.setVariable("majatekCount", argSets.size());
		log.info("DokumentKind: {}, DocumentId: {},Liczba subtaskow na potrzeby forka: {}", document.getDocumentKind().getCn(), document.getId(),argSets.size());
		if (argSets.size() == 0)
			execution.take("to setCostsIds");
		else
			execution.take("to dynamic_fork1");
	} 

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		// TODO Auto-generated method stub

	}
	
	private void setNrInwentarz(Long di, String nrInwentarzowy)
	{
		Statement stat = null;
		try
		{
			stat = DSApi.context().createStatement();
			int a = stat.executeUpdate("UPDATE DSG_CENTRUM_KOSZTOW_FAKTURY SET INWENTARZ = '" + nrInwentarzowy + "' where ID = " + di);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			DSApi.context().closeStatement(stat);
		}
	}
}
