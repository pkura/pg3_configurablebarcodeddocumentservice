/**
 * 
 */
package pl.compan.docusafe.parametrization.ifpan;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.process.InstantiationParameters;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.jbpm4.ActivitiesExtensionProcessView;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessInstance;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.process.form.ActivityController;
import pl.compan.docusafe.process.form.SelectItem;
import pl.compan.docusafe.web.common.ExtendedRenderBean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 09-01-2014
 * DS_REPO
 * AcceptancesSelectProcessViewIfpan.java
 */
public class AcceptancesSelectProcessViewIfpan extends ActivitiesExtensionProcessView{

	public AcceptancesSelectProcessViewIfpan(InstantiationParameters ip) throws Exception {
		super(ip);
	}

    @Override
    public ExtendedRenderBean render(ProcessInstance pi, ProcessActionContext context) throws EdmException {
        return render((Jbpm4ProcessInstance) pi, context);
    }
    
    private ExtendedRenderBean render(Jbpm4ProcessInstance pi, ProcessActionContext context) throws EdmException {
    	
		ExtendedRenderBean ret = super.render(pi,context);
		 
		Long docId = Jbpm4ProcessLocator.documentIdForExtenedProcesssId(((Jbpm4ProcessInstance)pi).getProcessInstanceId());
		
		List<DocumentAcceptance> acceptances = DocumentAcceptance.find(docId);

		Field docProcesses = Document.find(docId).getDocumentKind().getFieldByCn("STATUS");
		
		String docStatus = Document.find(docId).getFieldsManager().getEnumItemCn("STATUS");
		
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		for (DocumentAcceptance itemAcceptance: acceptances) {
		    itemAcceptance.initDescription();
		    // nie pokazywanie na li�cie akceptacji dla takiego samego etapu, co aktualnie wykonywany
		    if (!itemAcceptance.getAcceptanceCn().equals(docStatus))
		    	selectItems.add(ActivityController.createSelectItem(itemAcceptance.getId(), itemAcceptance.getAsFirstnameLastname(), itemAcceptance.getUsername(),
		    			docProcesses.getEnumItemByCn(itemAcceptance.getAcceptanceCn()).getTitle(), itemAcceptance.getAcceptanceCn()));
		}
		
		// Wyciagam akceptacje z zapotrzebowan dolaczonych do faktury kosztowej / zapopotrzebowania
		FieldsManager fm = OfficeDocument.find(docId).getFieldsManager();
		
		List<Long> idsZapotrzebowanie = new ArrayList<Long>();

		List<Map<String, Object>> dictionaryMap = (List<Map<String, Object>>) fm.getValue("ZAPOTRZEBOWANIE");
		
		if (dictionaryMap != null) {
			 for (Map<String, Object> row : dictionaryMap) {
				 if (row.get("ZAPOTRZEBOWANIE_ID") != null) {
					 BigDecimal id = (BigDecimal)row.get("ZAPOTRZEBOWANIE_ID");
					 idsZapotrzebowanie.add(id.longValue());
				 }
			 }
		}
		
		HashMap <Long, Field> docZapotrzProcess = new HashMap <Long, Field>(); 
		
		HashMap <Long, List<DocumentAcceptance>> acceptanceMap = new HashMap <Long, List<DocumentAcceptance>>(); 
		
		for (Long idZpotrzebowanie : idsZapotrzebowanie) {
			acceptanceMap.put(idZpotrzebowanie, DocumentAcceptance.find(idZpotrzebowanie));
			docZapotrzProcess.put(idZpotrzebowanie, Document.find(idZpotrzebowanie, false).getDocumentKind().getFieldByCn("STATUS"));
		}
		
		for (Long idZpotrzebowanie : idsZapotrzebowanie) {
			for (DocumentAcceptance itemAcceptance : acceptanceMap.get(idZpotrzebowanie)){
				if(itemAcceptance.getAsFirstnameLastname() == null){
					if(itemAcceptance.getUsername() != null){
						
						selectItems.add(ActivityController.createSelectItem(itemAcceptance.getId(), DSUser.findByUsername(itemAcceptance.getUsername()).asFirstnameLastnameName(), itemAcceptance.getUsername(), 
								docZapotrzProcess.get(idZpotrzebowanie).getEnumItemByCn(itemAcceptance.getAcceptanceCn()).getTitle() + " - Zapotrzebowanie", "taskCorrectionForZapotrz"));
					}
				} else {
					selectItems.add(ActivityController.createSelectItem(itemAcceptance.getId(), itemAcceptance.getAsFirstnameLastname(), itemAcceptance.getUsername(), 
							docZapotrzProcess.get(idZpotrzebowanie).getEnumItemByCn(itemAcceptance.getAcceptanceCn()).getTitle() + " - Zapotrzebowanie", "taskCorrectionForZapotrz"));
				}
				
			}
		}
		
		
	        ret.putValue("selectItems", selectItems);
	
	        return ret;

    }

}
