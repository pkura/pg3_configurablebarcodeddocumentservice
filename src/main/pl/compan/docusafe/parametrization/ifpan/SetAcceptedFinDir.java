package pl.compan.docusafe.parametrization.ifpan;

import edu.emory.mathcs.backport.java.util.Collections;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: slim
 * Date: 27.11.13
 * Time: 14:20
 * To change this template use File | Settings | File Templates.
 */
public class SetAcceptedFinDir implements ExternalActivityBehaviour
{
    private static Logger log = LoggerFactory.getLogger(SetAcceptedTDir.class);

    @Override
    public void signal(ActivityExecution activityExecution, String s, Map<String, ?> stringMap) throws Exception {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void execute(ActivityExecution execution) throws Exception {
//        boolean isOpened = true;
//        if (!DSApi.isContextOpen())
//        {
//            isOpened = false;
//            DSApi.openAdmin();
//        }
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        Document document = Document.find(docId);
//        Statement st = DSApi.context().createStatement();
//        String query = "update dsg_ifpan_zapotrz2 set accepted1=1 where document_id="+document.getId();
//        st.executeUpdate(query);
//        DSApi.context().commit();

        document.getFieldsManager().reloadValues(Collections.singletonMap("ACCEPTED", new Boolean(true)));

//        if (DSApi.isContextOpen() && !isOpened)
//        {
//            DSApi.close();
//        }
    }
}
