package pl.compan.docusafe.parametrization.ifpan;

public class CountryDelegation
{
	private String country;
	private String city;
	private String organizer;
	private String start;
	private String end;
	private String suma;
	private String organizerCost;
	
	public String getCountry()
	{
		return country;
	}
	public void setCountry(String country)
	{
		this.country = country;
	}
	public String getCity()
	{
		return city;
	}
	public void setCity(String city)
	{
		this.city = city;
	}
	public String getOrganizer()
	{
		return organizer;
	}
	public void setOrganizer(String organizer)
	{
		this.organizer = organizer;
	}
	public String getStart()
	{
		return start;
	}
	public void setStart(String start)
	{
		this.start = start;
	}
	public String getEnd()
	{
		return end;
	}
	public void setEnd(String end)
	{
		this.end = end;
	}
	public String getOrganizerCost()
	{
		return organizerCost;
	}
	public void setOrganizerCost(String organizerCost)
	{
		this.organizerCost = organizerCost;
	}
	public String getSuma()
	{
		return suma;
	}
	public void setSuma(String suma)
	{
		this.suma = suma;
	}
	
	

}
