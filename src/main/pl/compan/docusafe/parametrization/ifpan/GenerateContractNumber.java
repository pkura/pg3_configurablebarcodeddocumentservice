package pl.compan.docusafe.parametrization.ifpan;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.jbpm.pvm.internal.wire.binding.StringBinding;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class GenerateContractNumber implements ExternalActivityBehaviour
{
	private static final Logger log = LoggerFactory.getLogger(GenerateContractNumber.class);
	private String dzGUID;
	private String dpizeGUID;
	private String generateNumber;

	public void execute(ActivityExecution execution) throws Exception
	{
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Document document = Document.find(docId);
		
		if (generateNumber == null || !generateNumber.equals("false")) {
			
			Statement stat = null;
			ResultSet result = null;
			String caseNumber;

			String year = DateUtils.formatYear(new java.util.Date());

			try {

				stat = DSApi.context().createStatement();
				result = stat
						.executeQuery("select COUNT(*) from dsg_ifpan_contract c join ds_document d on c.DOCUMENT_ID = d.ID where year(d.CTIME) = YEAR (getdate())");
				if (!result.next()) {
					stat.executeUpdate("UPDATE dsg_ifpan_contract SET contract_nr = '1/" + year + "' WHERE DOCUMENT_ID = " + docId);
				} else {
					Integer oldContractNr = result.getInt(1);
					if (oldContractNr == null) {
						log.debug("Brak wynikow ustawiamy numer umowy na 1/" + year);
						stat.executeUpdate("UPDATE dsg_ifpan_contract SET contract_nr = '1/" + year + "' WHERE DOCUMENT_ID = " + docId);
					} else {
						// Dzia� generuj�cy umow�/b.znacznik zam�wienia/c.numer
						// kolejny/d.rok
						DSDivision[] userDivisions = DSApi.context().getDSUser().getDivisionsWithoutGroup();
						String dzial = "-";

						if (userDivisions != null && userDivisions.length > 0)
							dzial = userDivisions[0].getCode() == null ? "-" : userDivisions[0].getCode();

						StringBuilder znacznikiZamowien = new StringBuilder();
						if (document.getFieldsManager().getKey("ZAPOTRZEBOWANIE") != null) {
							List<Long> zapotrzebownieIDs = (List<Long>) document.getFieldsManager().getKey("ZAPOTRZEBOWANIE");
							for (Long id : zapotrzebownieIDs) {
								EnumItem orderItem = Document.find(id).getFieldsManager().getEnumItem("ORDERSIGN");
								if (orderItem != null && !znacznikiZamowien.toString().contains(orderItem.getTitle()))
								{
									znacznikiZamowien.append(orderItem.getTitle()).append(",");
								}
							}
							if (znacznikiZamowien.lastIndexOf(",") != -1)
								znacznikiZamowien.deleteCharAt(znacznikiZamowien.lastIndexOf(","));
						}
						Integer newContractNr = oldContractNr + 1;
						String znaczniki = "-";
						if (znacznikiZamowien.length() > 0) {
							znaczniki = znacznikiZamowien.toString();
						}
						caseNumber = dzial + "/" + znaczniki + "/" + newContractNr.toString() + "/" + year;
						log.info("Numer sprawy ustawiamy na: " + caseNumber);
						log.info("UPDATE dsg_ifpan_contract SET contract_nr = '" + caseNumber + "' WHERE DOCUMENT_ID = " + docId);
						stat.executeUpdate("UPDATE dsg_ifpan_contract SET contract_nr = '" + caseNumber + "' WHERE DOCUMENT_ID = " + docId);
					}
				}

			} catch (Exception e) {
				log.error(e.getMessage(), e);
			} finally {
				stat.close();
				result.close();
			}
		}
		
		DSUser user = DSUser.findByUsername(document.getAuthor());
		if (user.getDivisionsWithoutGroup().length > 0) {
			for (DSDivision division : user.getDivisionsWithoutGroup()) {
				if (division.getGuid().equals(dzGUID)) {
					execution.take("to DZauthorDecision");
					break;
				} else if (division.getGuid().equals(dpizeGUID)) {
					execution.take("to DPIZEauthorDecision");
					break;
				}
			}
		} else {
			execution.take("to DPIZEauthorDecision");
		}
		
		execution.setVariable("count", 2);
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
	}

}
