package pl.compan.docusafe.parametrization.ifpan;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Map;

public class GenReportPOIG implements ExternalActivityBehaviour 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;

	
	/**
	 * Logger
	 */ 
	private static final Logger log = LoggerFactory.getLogger(AddProjectEntryListener.class);
	
	public void execute(ActivityExecution execution) throws Exception 
	{		
//    	Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
//		Document document = Document.find(docId);
//		log.info("validate contents for gen raport fields \n {} ", document.getFieldsManager());
//		log.info("Start Generowanie protoko�u odbioru towaru");
//		
//       // Long di = new Long( (String)document.getFieldsManager().getValue("SENDER"));
////System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA: " + Dictionary.dictionarieObjects.get("SENDER").getFieldsValues("SENDER", di.toString()).get("SENDER_ORGANIZATION").toString());
//		Map<String, Object> values = new HashMap<String, Object>();
//		/*
//		 * Wartosci przesylane do generowania dokumentu
//		 * INVOICEN  - numer faktury
//		 * DESCRIPTION    - opis
//		 * CONTRACTOR   - kontrahent
//		 * DATE    - data odbioru
//		 * NR    - numer zapotrzenowania
//		 */
//		if(document.getFieldsManager().getValue("DESCRIPTION") != null)
//		{
//			values.put("DESCRIPTION", document.getFieldsManager().getValue("DESCRIPTION").toString());
//		}
//		else
//		{
//			values.put("DESCRIPTION", "-");
//		}
//		
//		
//		values.put("DATE", DateUtils.formatCommonDate(new java.util.Date()).toString());
//		//values.put("ODBIOR", DSUser.findByUsername(DSApi.context().getPrincipalName().toString()).asFirstnameLastname()); 
//		values.put("ZAPOTRZ", document.getFieldsManager().getValue("ZAPOTRZEBOWANIE").toString());
//		// pobieramy wartosc aby uzyskac shorSumarry z tego dokumentu, aby pobrac id sendera nalezy pobrac key
//		values.put("CONTRACTOR", (String)document.getFieldsManager().getValue("SENDER"));//Dictionary.dictionarieObjects.get("SENDER").getFieldsValues("SENDER", di.toString()).get("SENDER_ORGANIZATION").toString());
//		values.put("INVOICEN", document.getFieldsManager().getValue("NUMER_FAKTURY").toString());
//		values.put("fields", Documents.document(docId).getFields());
//		log.info("data to gener document \n {} ", values);
//
//		//Long 
//    	//argSets.add( ( (EnumValues)Dictionary.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getFieldsValues("DSG_CENTRUM_KOSZTOW_FAKTURY", di.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMID")).getSelectedOptions().get(0) );
//
//		//projectEntry.setGross(new BigDecimal(document.getFieldsManager().getValue("DESCRIPTION").toString().replace(" ", "")));
//		
//		log.info("Wartosci p�l do wygenerowania dokumentu przyj�cia towaru: '{}'", values);
//		
//        RepositoryProvider.createDocument(values, "ProtokOdb");
//		File file = new File(Docusafe.getHome().getPath() + "/templates/ProtokolOdbioruTowaru.rtf");
//		
//
//        try
//	    {
//			//read this file into InputStream
//			InputStream inputStream= RepositoryProvider.getRepositoryFile("ProtokOdbResult.rtf");
//			if(inputStream == null)
//			{
//				throw new Exception("Dokument nie zosta� wygenerowany i nie mo�na go pobra�");
//			}
//			//write the inputStream to a FileOutputStream
//			OutputStream out = new FileOutputStream(file);
//	 
//			int read=0;
//			byte[] bytes = new byte[2048];
//	 
//			while((read = inputStream.read(bytes))!= -1){
//				out.write(bytes, 0, read);
//			}
//	 
//			inputStream.close();
//			out.flush();
//			out.close();	
//	//        ServletActionContext.getResponse().sendRedirect(ServletActionContext.getRequest().getContextPath()+"office/incoming/document-archive.action?documentId="+ docId +"&activity=internal,820010&setInitialValues=&openViewer=");
//	
//		    //ServletUtils.streamFile(ServletActionContext.getResponse(), file, "application/msword", "Accept-Charset: iso-8859-2","Content-Disposition: attachment; filename=\"Protokol odbioru.rtf\"");
//		    //file.delete();
//			log.info("New file created!");
//	    }
//        catch (IOException e){
//        	log.error(e.getMessage(),e);
//	  }
//	  catch (Exception e){
//		  log.error(e.getMessage(),e);
//	  }
		//##################################
//        System.out.println(ServletActionContext.getResponse().toString());//sendRedirect();
//
//		Long idZapotrz = (Long)document.getFieldsManager().getValue("ZAPOTRZEBOWANIE");
//		System.out.println("Id zapotrzebowania do ktorego dodamy id umowy: " + idZapotrz);
//		
//		BufferedWriter out = null;
		
//		try 
//		{	
//		 File file = new File("raportFakturaBeck.cvs");
//         file.deleteOnExit(); 
//		 FileWriter fstream = new FileWriter("raportFakturaBeck.cvs");
//   		 out = new BufferedWriter(fstream);
//   		 out.write("Numer faktury;Kwota;Kontrahent\n");
//
//   		 
////			//DSApi.context().begin();
////			File file = File.createTempFile("raportFakturaBeck", "importBeck");
////            file.deleteOnExit();                
////            OutputStream os = new FileOutputStream(file);
////            StringBuilder sb = new StringBuilder();
////            sb.append("Numer faktury;Kwota;Kontrahent\n");
//			Statement stat = DSApi.context().createStatement();	
//		    ResultSet result = stat.executeQuery("SELECT f.invoice_number, f.gross_amount, p.organization FROM dsg_beck_coinvoice f INNER JOIN dso_person p ON f.document_id=p.document_id");
//		    while(result.next())
//		    {
//		    	String temp = result.getString(1)+";";
//		    	temp += result.getFloat(2)+";";
//		    	temp += result.getString(3);		    	
//		    	temp += "\n";
//		     	out.write(temp);
//		     	log.info("Linia eksportu faktury beck" + temp);
//		    }
//	   		 out.close();
//
//			//DSApi.context().commit();
//            ServletUtils.streamFile(ServletActionContext.getResponse(), file, "application/vnd.ms-excel", "Accept-Charset: iso-8859-2","Content-Disposition: attachment; filename=\"LOG.csv\"");
//		} 
//		catch(IOException e)
//		{
//			log.error(e.getMessage(), e);
//			event.addActionError(e.getMessage() + e);
//			DSApi.context()._rollback();
//		}
//		catch (Exception e) 
//		{
//			log.error(e.getMessage(), e);
//			DSApi.context()._rollback();
//		}
	}
	
	public void signal(ActivityExecution execution, String signalName, Map<String, ?> parameters)
	throws Exception {
		
	}
}
