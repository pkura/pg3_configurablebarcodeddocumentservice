package pl.compan.docusafe.parametrization.ifpan;

import java.math.BigDecimal;

public class Koszt
{
	private String platnik;
	private String waluta;
	private BigDecimal koszt;
	private BigDecimal kosztPln;
	public String getPlatnik()
	{
		return platnik;
	}
	public void setPlatnik(String platnik)
	{
		this.platnik = platnik;
	}
	public String getWaluta()
	{
		return waluta;
	}
	public void setWaluta(String waluta)
	{
		this.waluta = waluta;
	}
	public BigDecimal getKoszt()
	{
		return koszt;
	}
	public void setKoszt(BigDecimal koszt)
	{
		this.koszt = koszt;
	}
	public BigDecimal getKosztPln()
	{
		return kosztPln;
	}
	public void setKosztPln(BigDecimal kosztPln)
	{
		this.kosztPln = kosztPln;
	}
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((platnik == null) ? 0 : platnik.hashCode());
		result = prime * result + ((waluta == null) ? 0 : waluta.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Koszt other = (Koszt) obj;
		if (platnik == null)
		{
			if (other.platnik != null)
				return false;
		}
		else if (!platnik.equals(other.platnik))
			return false;
		if (waluta == null)
		{
			if (other.waluta != null)
				return false;
		}
		else if (!waluta.equals(other.waluta))
			return false;
		return true;
	}
	public Koszt(String platnik, String waluta, BigDecimal koszt, BigDecimal kosztPln)
	{
		this.platnik = platnik;
		this.waluta = waluta;
		this.koszt = koszt;
		this.kosztPln = kosztPln;
	}
	public Koszt()
	{
	}
}
