package pl.compan.docusafe.parametrization.ifpan;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class RemoveBlocadeCost implements ExternalActivityBehaviour
{

	private final static Logger LOG = LoggerFactory.getLogger(RemoveBlocadeCost.class);

	@Override
	public void execute(ActivityExecution openExecution) throws Exception
	{
		boolean isOpened = true;
		try
		{
			if (!DSApi.isContextOpen())
			{
				isOpened = false;
				DSApi.openAdmin();
			}
			
			OfficeDocument doc = Jbpm4Utils.getDocument(openExecution);
			FieldsManager fm = doc.getFieldsManager();
			
			try 
			{
				List<Long> ids = (List<Long>) fm.getKey("ZRODLO_FINANSOWANIA");
				PreparedStatement ps;
				for (Long id : ids)
				{
					ps = DSApi.context().prepareStatement("SELECT * FROM dsg_ifpan_delegate_costs where bilet_id = ?");
					ps.setLong(1, id);
					ResultSet rs = ps.executeQuery();
					rs.next();

					Long delegateCostId = rs.getLong("ID");
					BigDecimal kosztBiletuPln = rs.getBigDecimal("COST_PLN");
					Integer projectId = rs.getInt("platnik");
					String nrBlocade = rs.getString("nrBlocade");
					
					// aktualizacja blokady
					ps = DSApi.context().prepareStatement("update dsr_project_entry set gross = gross - ? where projectId = ? and entryNumber = ?");
					ps.setBigDecimal(1, kosztBiletuPln);
					ps.setLong(2, projectId);
					ps.setString(3, nrBlocade);
					ps.executeUpdate();
					
					// usuniecie z kosztow jednostki delegujacejz tabeli multiple
					ps = DSApi.context().prepareStatement("delete from dsg_ifpan_delegacja_multiple_value where FIELD_CN='DELEGATE_COSTS' and FIELD_VAL = ?");
					ps.setLong(1, delegateCostId);
					ps.executeUpdate();
					// usuniecie z kosztow jednostki delegujacej 
					ps = DSApi.context().prepareStatement("delete from dsg_ifpan_delegate_costs where id = ?");
					ps.setLong(1, delegateCostId);
					ps.executeUpdate();
					/*// pobranie id wydatkowanego srodka
					ps = DSApi.context().prepareStatement("SELECT * FROM dsg_ifpan_wydatkowane_srodki where bilet_id = ?");
					ps.setLong(1, id);
					 rs = ps.executeQuery();
					rs.next();
					Long wydatkownyId = rs.getLong("ID");
					// usuniecie z wydatkownay srodkow z tabeli multiple
					ps = DSApi.context().prepareStatement("delete from dsg_ifpan_delegacja_multiple_value where FIELD_CN='WYDATKOWANE_SRODKI' and FIELD_VAL = ?");
					ps.setLong(1, wydatkownyId);
					ps.executeUpdate();
					// usuniecie z wydatkownay srodkow
					ps = DSApi.context().prepareStatement("delete from dsg_ifpan_wydatkowane_srodki where id = ?");
					ps.setLong(1, wydatkownyId);
					ps.executeUpdate();*/
				}
			}
			catch (SQLException e) 
			{
				LOG.error(e.getMessage(), e);
				throw new EdmException(e.getMessage());
			}
			openExecution.take("rejected-author");
		}
		catch (Exception e)
		{
			LOG.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
		finally
		{
			if (DSApi.isContextOpen() && !isOpened)
			{
				DSApi.close();
			}
		}
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		// TODO Auto-generated method stub

	}
}
