package pl.compan.docusafe.parametrization.ifpan;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.parametrization.AbstractParametrization;

public class IfPanParametrization extends AbstractParametrization
{
	/***
	 * Metoda wywoływana po utowrzeniu persona
	 * 
	 * @throws EdmException
	 */
	public void setAfterCreatePerson(Person person) throws EdmException
	{
		IfpanUtils.exportPerson(person, false);
	}
}
