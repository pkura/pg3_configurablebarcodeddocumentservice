package pl.compan.docusafe.parametrization.ifpan;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class PrepareProcessListener implements ExternalActivityBehaviour
{
	private String dictionaryField;
	private String enumIdField;

	protected static Logger log = LoggerFactory.getLogger(PrepareProcessListener.class);

	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);

		List<Long> dictionaryIds = (List<Long>) doc.getFieldsManager().getKey(dictionaryField.toUpperCase());

		final List<Long> argSets = new LinkedList<Long>();

		for (Long di : dictionaryIds)
		{
			String projId  = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get(dictionaryField.toUpperCase()).getValues(di.toString()).get(dictionaryField + "_" + enumIdField.toUpperCase())).getSelectedOptions().get(0);
			if (!argSets.contains(Long.valueOf(projId)))
				argSets.add(Long.valueOf(projId));
		}
		execution.setVariable("ids", argSets);
		execution.setVariable("count", argSets.size());
		execution.setVariable("count2", 2);

	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		// TODO Auto-generated method stub

	}

}
