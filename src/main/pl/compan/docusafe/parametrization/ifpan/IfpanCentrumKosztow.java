package pl.compan.docusafe.parametrization.ifpan;

import java.math.BigDecimal;

public class IfpanCentrumKosztow
{
	String projekt;
	String akcept;
	String kategoria;
	String majatek;
	String inwentarz;
	String ksiegowaKategoria;
	BigDecimal  kwota = BigDecimal.ZERO;
	
	public String getProjekt()
	{
		return projekt;
	}
	public void setProjekt(String projekt)
	{
		this.projekt = projekt;
	}
	public String getAkcept()
	{
		return akcept;
	}
	public void setAkcept(String akcept)
	{
		this.akcept = akcept;
	}
	public String getKategoria()
	{
		return kategoria;
	}
	public void setKategoria(String kategoria)
	{
		this.kategoria = kategoria;
	}
	public String getMajatek()
	{
		return majatek;
	}
	public void setMajatek(String majatek)
	{
		this.majatek = majatek;
	}
	public String getInwentarz()
	{
		return inwentarz;
	}
	public void setInwentarz(String inwentarz)
	{
		this.inwentarz = inwentarz;
	}
	public String getKsiegowaKategoria()
	{
		return ksiegowaKategoria;
	}
	public void setKsiegowaKategoria(String ksiegowaKategoria)
	{
		this.ksiegowaKategoria = ksiegowaKategoria;
	}
	public BigDecimal getKwota()
	{
		return kwota;
	}
	public void setKwota(BigDecimal kwota)
	{
		this.kwota = kwota;
	}
	
}
