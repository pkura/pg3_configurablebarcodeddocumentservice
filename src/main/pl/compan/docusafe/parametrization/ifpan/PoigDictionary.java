package pl.compan.docusafe.parametrization.ifpan;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Map;

public class PoigDictionary extends DwrDictionaryBase {

    private static Logger log = LoggerFactory.getLogger(PoigDictionary.class);

    @Override
    public int remove(String id) throws EdmException {
	return -1;
    }

    public int update(Map<String, FieldData> values) throws EdmException {
	log.debug("update");
	long ret = -1;
	try {
	    boolean contextOpened = true;
	    Statement stat = null;
	    ResultSet result = null;
	    if (values.get("ID") != null) {
		if (!DSApi.isContextOpen()) {
		    DSApi.openAdmin();
		    contextOpened = false;
		}
		stat = DSApi.context().createStatement();

		StringBuilder select = new StringBuilder("select base from dsg_ifpan_cost_category_poig where id = ").append(values.get("ID"));

		result = stat.executeQuery(select.toString());
		while (result.next()) {
		    Integer base = result.getInt("base");
		    if (base.equals(1)) 
		    {
		    	long newId = super.add(values);
		    	values.get("ID").setData(newId);
		    	return Integer.parseInt(newId + "");
		    } 
		    else if (base == null || base.equals(0))
		    {
		    	return super.update(values);
		    }
		}
	    }
	} catch (Exception e) {
	    return -1;
	}
	return -1;
    }

    @Override
    public long add(Map<String, FieldData> values) {
	log.debug("add");
	return -1;
    }
}
