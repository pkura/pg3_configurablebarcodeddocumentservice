package pl.compan.docusafe.parametrization.ifpan.jbpm;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.zbp.FakturaZakupowaLogic;
import pl.compan.docusafe.parametrization.zbp.WniosekUrlopowyLogic;

public class PrepareProcessListener implements ExternalActivityBehaviour {

    private static final long serialVersionUID = 1L;
    private String dictionaryField;
    public static final String CENTRA_KOSZTOW_TABLENAME = "dsg_zbp_centra_kosztow";
    public List<String> setValue = new ArrayList<String>();

    // private String enumIdField;
    // public static final List<String> guids = new LinkedList<String>();

    @Override
    public void execute(ActivityExecution execution) throws Exception {
	
    	
    	
    if("PRZEKAZANIE".equalsIgnoreCase(dictionaryField)){
    	
    }
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
	OfficeDocument doc = OfficeDocument.find(docId);
	final List<Long> argSets = new LinkedList<Long>();

	if ("SUPERVISOR".equalsIgnoreCase(dictionaryField)) {
	    DSUser user = DSUser.findById(((Integer) doc.getFieldsManager().getKey(WniosekUrlopowyLogic.WNIOSKODAWCA_CN)).longValue());
	    for (DSUser supervisor : user.getImmediateSupervisors()) {
		argSets.add(supervisor.getId());
	    }
	} else {
	    @SuppressWarnings("unchecked")

	    List<Long> dictionaryIds = (List<Long>) doc.getFieldsManager().getKey(dictionaryField.toUpperCase());

	    //argSets.addAll(dictionaryIds);
	    for(Long ids : dictionaryIds){
	    	CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(ids);
	        String centrumCn = DataBaseEnumField.getEnumItemForTable(CENTRA_KOSZTOW_TABLENAME, mpk.getCentrumId()).getCn();
	        List<String> users = new ArrayList<String>();
	        
	        for (AcceptanceCondition accept : AcceptanceCondition.find(centrumCn, null))
			{
				if (accept.getUsername() != null && !users.contains(accept.getUsername()))
					if(!setValue.contains(accept.getUsername())){
						users.add(accept.getUsername());
						argSets.add(DSUser.findByUsername(accept.getUsername()).getId());
						setValue.add(accept.getUsername());
					}
			}
	    }
	    
	}

	execution.setVariable("ids", argSets);
	execution.setVariable("count", argSets.size());
	execution.setVariable("count2", 2);
    }

    @Override
    public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
	// TODO Auto-generated method stub

    }

}
