package pl.compan.docusafe.parametrization.ifpan;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * s�ownik "�rodki otrzymane w kraju" jest uzupe�niany na podstawie s�ownika
 * "Koszty jednostki deleguj�cej"
 * 
 * @author kk
 */
@SuppressWarnings("serial")
public class PrepareRozliczenieInDic implements ExternalActivityBehaviour {
	private static final String TSQL_CODE = "begin transaction; begin try DECLARE @inserted_ids TABLE ([id] INT); insert into dsg_ifpan_srodki_internal (amount,waluta,kurs,COST_PLN,platnik,typ) OUTPUT INSERTED.[id] INTO @inserted_ids select cost_amount*amount as amount, waluta, rate as kurs, COST_PLN, platnik, typ from dsg_ifpan_delegate_costs del_costs JOIN dsg_ifpan_delegacja_multiple_value mu ON del_costs.ID=mu.FIELD_VAL where mu.FIELD_CN='DELEGATE_COSTS' and mu.DOCUMENT_ID=?;  insert into dsg_ifpan_delegacja_multiple_value (DOCUMENT_ID,FIELD_CN,FIELD_VAL) select ? as DOCUMENT_ID, 'ROZLICZENIE_SRO_IN' as FIELD_CN, id as FIELD_VAL from @inserted_ids; commit; end try begin catch rollback; end catch";
	public static final Logger log = LoggerFactory.getLogger(PrepareRozliczenieInDic.class);

	@Override
	public void execute(ActivityExecution execution) throws Exception {
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		log.debug("docId: {}", docId);
		
		try {
			if (Document.find(docId).getFieldsManager().getKey("ROZLICZENIE_SRO_IN") == null || ((Collection<?>)Document.find(docId).getFieldsManager().getKey("ROZLICZENIE_SRO_IN")).size() == 0) {
				PreparedStatement ps = null;
				int rs;
				try {
					ps = DSApi.context().prepareStatement(TSQL_CODE);
					ps.setLong(1, docId);
					ps.setLong(2, docId);

					rs = ps.executeUpdate();
					if (rs == 0) {
						log.error("ERROR, inserted" + rs);
					} else {
						log.error("inserted: " + rs);
					}
				} catch (SQLException e) {
					throw new EdmSQLException(e);
				} finally {
					DSApi.context().closeStatement(ps);
				}
			}
		} catch (Exception e) {
			log.error(e.toString());
		}
		execution.takeDefaultTransition();
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
	}
}
