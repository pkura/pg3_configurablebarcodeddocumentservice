package pl.compan.docusafe.parametrization.ifpan;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public abstract class AbstractIfpanDocumentLogic extends AbstractDocumentLogic {

	private static Logger log = LoggerFactory.getLogger(AbstractIfpanDocumentLogic.class);	
	
	@Override
	public void documentPermissions(Document document) throws EdmException {
		// TODO Auto-generated method stub

	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
		// TODO Auto-generated method stub

	}

	public void setContractorFlag(Map<String, FieldData> values, String contractorFieldName) {
		FieldData contractorCountry = values.get("DWR_"+contractorFieldName).getDictionaryData().get(contractorFieldName+"_COUNTRY");
		if (contractorCountry != null && contractorCountry.getData() != null) {
			//polska id=2
			if (contractorCountry.getData().toString().equals("2")) {
				values.get("DWR_"+contractorFieldName+"NATIONALITY").getEnumValuesData().setSelectedOptions(new ArrayList<String>(){{add("1");}});//1 dla dostawcy z polski
			} else {
				values.get("DWR_"+contractorFieldName+"NATIONALITY").getEnumValuesData().setSelectedOptions(new ArrayList<String>(){{add("2");}});
			}
		} else {
			values.get("DWR_"+contractorFieldName+"NATIONALITY").getEnumValuesData().setSelectedOptions(new ArrayList<String>(){{add("");}});
		}
	}	

	public void createPersonFromChoosenErpId(Map<String, FieldData> values, String contractorFieldName) throws EdmException {
		FieldData contractorFields = values.get("DWR_"+contractorFieldName);
		Map<String,FieldData> contractorFieldsMap = contractorFields.getDictionaryData();
		
		ErpPerson erpContractor = null;
		
		try {
			DSApi.openAdmin();
			if (contractorFieldsMap != null && contractorFieldsMap.get("id") != null && StringUtils.isNotEmpty(contractorFieldsMap.get("id").toString()) && contractorFieldsMap.get("id").toString().contains("-")) {
				try {
					Long id = Long.valueOf(contractorFieldsMap.get("id").toString().substring(1));
					erpContractor = ErpPerson.findByErpId(id);
				} catch (NumberFormatException e) {
					log.error(e.toString());
				}
				//nie zawsze znajduje si� id w values, mimo, �e na formatce jest, dlatego szukam po innych parametrach
			} else {
				if (DwrUtils.getNotEmptyValue(contractorFieldsMap, "id")==null) {
					String organization = DwrUtils.getNotEmptyValue(contractorFieldsMap, contractorFieldName+"_ORGANIZATION");
					if (organization != null && organization.contains(ContractorDictionary.ERP_FLAG)) {
						organization = StringUtils.remove(organization, ContractorDictionary.ERP_FLAG);
						String location = DwrUtils.getNotEmptyValue(contractorFieldsMap, contractorFieldName+"_LOCATION");
						String zip = DwrUtils.getNotEmptyValue(contractorFieldsMap, contractorFieldName+"_ZIP");
						Integer countryId = DwrUtils.getNotEmptyValue(contractorFieldsMap, contractorFieldName+"_COUNTRY")!=null?Integer.valueOf(DwrUtils.getNotEmptyValue(contractorFieldsMap, contractorFieldName+"_COUNTRY")):null;
						String street = DwrUtils.getNotEmptyValue(contractorFieldsMap, contractorFieldName+"_STREET");
						String nip = DwrUtils.getNotEmptyValue(contractorFieldsMap, contractorFieldName+"_NIP");
						erpContractor = ErpPerson.find(organization, street, zip, location, nip, countryId);
					}
				}
			}
			
			if (erpContractor != null) {
				Person contractor = new Person();
				erpContractor.updatePersonFields(contractor);
				contractor.create();
				
			    FieldData temp;
			    Map<String,FieldData> contractorMod = new HashMap<String,FieldData>();
			    for (String field:contractorFieldsMap.keySet()){
				try {
				    temp = contractorFieldsMap.get(field);
				    temp.setData(contractor.getParam(field,contractorFieldName+"_"));
				    contractorMod.put(field,temp);
				} catch (EdmException e) {
				    log.error("",e);
				}
			    }
			    contractorFields.setDictionaryData(contractorMod);
			    values.put("DWR_"+contractorFieldName,contractorFields);
			}
			
		} catch (EdmException e1) {
			log.error(e1.toString());
		} finally {
			DSApi.close();
		}
	}	

	public void tryCreateSenderPersonOnSaveDoc(DocumentKind kind, Long documentId, Map<String, ?> fieldValues, String contractorFieldName) throws EdmException, EdmSQLException {
		Long contractorId = (Long)fieldValues.get(contractorFieldName);

		if (contractorId != null) {
			Person contractor = Person.find(contractorId.longValue());
			if (!(contractor instanceof Sender) || !Person.DICTIONARY_SENDER.equals(contractor.getDictionaryType())) {
				Map<String,String> personMap = contractor.toMap();
				Person sender = new Sender();
				sender.fromMap(personMap);
				sender.setDictionaryType(Person.DICTIONARY_SENDER);
				sender.setBasePersonId(contractorId);
				sender.create();

				StringBuilder query = new StringBuilder("update ").append(kind.getTablename()).append(" set ").append(contractorFieldName.toLowerCase(Locale.ENGLISH)).append("=? where document_id=?");
				
				PreparedStatement ps = null;
				ResultSet rs = null;
				try {
					ps = DSApi.context().prepareStatement(query.toString());
					ps.setLong(1, sender.getId());
					ps.setLong(2, documentId);
					ps.executeUpdate();
				} catch (SQLException e) {
					throw new EdmSQLException(e);
				} finally {
					DSApi.context().closeStatement(ps);
					DbUtils.closeQuietly(rs);
				}
			}
		}
	}	
	
}
