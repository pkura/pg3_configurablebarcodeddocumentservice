package pl.compan.docusafe.parametrization.ifpan;

import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SendToErp implements ExternalActivityBehaviour
{

	public static final Logger log = LoggerFactory.getLogger(SendToErp.class);

	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		log.trace("Rozpoczecie wysylanie zaliczek do erp");
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		log.debug("docId: {}", docId);
		OfficeDocument doc = OfficeDocument.find(docId);

		if (doc.getFieldsManager().getEnumItem("STATUS").getCn().equals("sdsd"))
		{
			if (IfpanUtils.exportAdvance(doc, false))
				execution.take("to sdsdZaliczka");
			else
				execution.take("to delegation-finish");
		}
		else if (doc.getFieldsManager().getEnumItem("STATUS").getCn().equals("sdsd-back-to-delegation") || doc.getFieldsManager().getEnumItem("STATUS").getCn().equals("sdsd-back-to-ifpan-delegation"))
		{
			IfpanUtils.exportRozliczenia(doc);
			execution.takeDefaultTransition();
		}
		else if (doc.getFieldsManager().getEnumItem("STATUS").getCn().equals("accounting") || (doc.getDocumentKind().getCn().equals("fakt_zap_ifpan") && doc.getFieldsManager().getEnumItem("STATUS").getCn().equals("accounting2")))
		{
			IfpanUtils.exportInvoice(doc, false);
			execution.takeDefaultTransition();
		}
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{

	}

}
