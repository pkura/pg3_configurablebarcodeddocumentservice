package pl.compan.docusafe.parametrization.ifpan;

import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.jbpm.api.activity.ActivityExecution;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import java.util.List;
import java.util.Map;
import java.sql.Statement;

import pl.compan.docusafe.core.DSApi;

public class ExportFKListener implements ExternalActivityBehaviour
{
	private static final Logger log = LoggerFactory.getLogger(AddProjectEntryListener.class);

	public void execute(ActivityExecution execution) throws Exception
	{

		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Document document = Document.find(docId);
		Object ids = document.getFieldsManager().getKey("ZAPOTRZEBOWANIE");
		if (ids != null)
		{
			List<Long> idsZapotrz = (List<Long>) ids;
			for (Long idZapotrz : idsZapotrz)
			{
				log.info("Id zapotrzebowania do ktorego dodamy id umowy: " + idZapotrz);

				Statement stat = null;
				try
				{
					stat = DSApi.context().createStatement();
					int a = stat.executeUpdate("UPDATE dsg_ifpan_zapotrz SET contract = " + docId + " WHERE DOCUMENT_ID = " + idZapotrz);
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
				}
				finally
				{
					DSApi.context().closeStatement(stat);
				}
			}
		}
	}

	public void signal(ActivityExecution execution, String signalName, Map<String, ?> parameters) throws Exception
	{

	}
}
