package pl.compan.docusafe.parametrization.ifpan;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SetRateInWydatkowaneDic implements ExternalActivityBehaviour 
{
	private static final String TSQL_CODE = "begin transaction; begin try DECLARE @waluty_kursy TABLE ([waluta_id] INT, kurs numeric (18,4)) insert into @waluty_kursy " +
			"select waluta, rate as kurs from dsg_ifpan_delegate_costs del_costs JOIN dsg_ifpan_delegacja_multiple_value mu ON del_costs.ID=mu.FIELD_VAL where mu.FIELD_CN='DELEGATE_COSTS' " +
			"and mu.DOCUMENT_ID=?; update dsg_ifpan_wydatkowane_srodki set kurs = (select kurs from @waluty_kursy where waluta_id = dsg_ifpan_wydatkowane_srodki.waluta),  " +
			"pln = (select kurs from @waluty_kursy where waluta_id = dsg_ifpan_wydatkowane_srodki.waluta) * amount where id in " +
			"( select w.ID from dsg_ifpan_wydatkowane_srodki w join dsg_ifpan_delegacja_multiple_value mu ON w.ID=mu.FIELD_VAL  where mu.FIELD_CN='WYDATKOWANE_SRODKI' and mu.DOCUMENT_ID=?);" +
			"DECLARE @dysponowane numeric (18,2) select @dysponowane = SUM(COST_PLN) from dsg_ifpan_srodki_internal w join dsg_ifpan_delegacja_multiple_value mu ON w.ID=mu.FIELD_VAL " +
			"where mu.FIELD_CN='ROZLICZENIE_SRO_IN' and mu.DOCUMENT_ID=?;select @dysponowane = @dysponowane + isnull (SUM(pln), 0) from dsg_ifpan_srodki_abroad w join dsg_ifpan_delegacja_multiple_value mu ON " +
			"w.ID=mu.FIELD_VAL where mu.FIELD_CN='ROZLICZENIE_SRO_ABROAD' and mu.DOCUMENT_ID=?; update dsg_ifpan_delegacja set POSIADANA = @dysponowane where DOCUMENT_ID=?; DECLARE @wydane numeric (18,2)" +
			"select @wydane = SUM(pln) from dsg_ifpan_wydatkowane_srodki w join dsg_ifpan_delegacja_multiple_value mu ON w.ID=mu.FIELD_VAL where mu.FIELD_CN='WYDATKOWANE_SRODKI' and mu.DOCUMENT_ID=?;" +
			"update dsg_ifpan_delegacja set WYDANA = @wydane where DOCUMENT_ID=?;	commit; end try begin catch rollback; end catch;";
	
	public static final Logger log = LoggerFactory.getLogger(SetRateInWydatkowaneDic.class);

	@Override
	public void execute(ActivityExecution execution) throws Exception {
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		log.debug("docId: {}", docId);
		
		try {
				PreparedStatement ps = null;
				int rs;
				try {
					ps = DSApi.context().prepareStatement(TSQL_CODE);
					ps.setLong(1, docId);
					ps.setLong(2, docId);
					ps.setLong(3, docId);
					ps.setLong(4, docId);
					ps.setLong(5, docId);
					ps.setLong(6, docId);
					ps.setLong(7, docId);
					rs = ps.executeUpdate();
					if (rs == 0) {
						log.error("ERROR, inserted " + rs);
					} else {
						log.error("inserted: " + rs);
					}
				} catch (SQLException e) {
					throw new EdmSQLException(e);
				} finally {
					DSApi.context().closeStatement(ps);
				}

		} catch (Exception e) {
			log.error(e.toString());
		}
		execution.takeDefaultTransition();
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception 
	{
	}
}