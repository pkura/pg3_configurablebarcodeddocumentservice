package pl.compan.docusafe.parametrization.ifpan;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.record.projects.AccountingCostKind;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.core.record.projects.ProjectEntry;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import sun.plugin.util.PluginSysUtil;

public class AddProjectEntryListenerNew implements ExternalActivityBehaviour
{
    private static final Logger log = LoggerFactory.getLogger(AddProjectEntryListener.class);

    public void execute(ActivityExecution execution) throws Exception
    {
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        Document document = Document.find(docId);

        Statement stat = null;
        ResultSet result = null;

        try
        {
            stat = DSApi.context().createStatement();
            String caseNumber = getCaseNumber(docId, stat, result);
            setBlocade(docId, caseNumber, document, stat, result);
            execution.takeDefaultTransition();
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
        }
        finally
        {
            stat.close();
        }
    }

    private String getCaseNumber(Long docId, Statement stat, ResultSet result) throws SQLException, EdmException
    {
        String caseNumber = null;
        String year = DateUtils.formatYear(new java.util.Date());
        try
        {
            result = stat.executeQuery("SELECT DEMAND_NUMBER2 FROM dsg_ifpan_zapotrz2 WHERE DOCUMENT_ID = " + docId);
            if (result.next())
                caseNumber = result.getString(1);
            if (caseNumber != null)
                return caseNumber;

//            result = stat.executeQuery("SELECT MAX(CAST(SUBSTRING(caseNumber2,1,LEN(caseNumber2)-5) AS INT)) from dsg_ifpan_zapotrz2 where caseNumber like '%/" + year + "'");
//            result.next();
//
//            Integer lastCaseNumber = result.getInt(1);
//            if (lastCaseNumber == null)
//                caseNumber = "1/" + year;
//            else
//                caseNumber =  new Integer(lastCaseNumber) + 1 + "/" + year;
//
//            stat.executeUpdate("UPDATE dsg_ifpan_zapotrz2 SET caseNumber2 = '" + caseNumber + "' WHERE DOCUMENT_ID = " + docId);
//            return caseNumber;
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
            throw new EdmException(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
    {
    }

    private void setBlocade(Long docId, String caseNumber, Document document, Statement stat, ResultSet result) throws DocumentNotFoundException, DocumentLockedException, AccessDeniedException, EdmException, SQLException
    {

        String dicName = "DSG_CENTRUM_KOSZTOW_FAKTURY";
        List<Long> dictionaryIds = (List<Long>) Document.find(docId).getFieldsManager().getKey(dicName);
        FieldsManager fm = document.getFieldsManager();
        Integer entryNumber = 0;
        for (Long dicId : dictionaryIds)
        {
            CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(dicId);
            //String projectId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get(dicName).getValues(dicId.toString()).get(dicName + "_CENTRUMID")).getSelectedOptions().get(0);
            Integer projectId = mpk.getCentrumId();
            BigDecimal costPln = mpk.getRealAmount();//new BigDecimal(DwrDictionaryFacade.dictionarieObjects.get(dicName).getValues(dicId.toString()).get(dicName + "_AMOUNT").toString());
            Project project = Project.find(Long.valueOf(projectId));
            ProjectEntry projectEntry = ProjectEntry.findByCostId(dicId);
            if (projectEntry == null)
            {
                projectEntry = new ProjectEntry();
                projectEntry.setDateEntry(new java.util.Date());
                projectEntry.setCostId(dicId);
                projectEntry.setDocNr(docId.toString());
                projectEntry.setCurrency("PLN");
                projectEntry.setRate(BigDecimal.valueOf(1.0000D));
                projectEntry.setDemandNr(caseNumber);
            }
            if (projectEntry.getDemandNr() != null)
                projectEntry.setDemandNr(caseNumber);

            projectEntry.setEntryType("BLOCADE");
            projectEntry.setGross(costPln);
            projectEntry.setMtime(new java.util.Date());

            //if (!((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(dicId.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_ACCOUNTNUMBER")).getSelectedOptions().get(0).equals(""))
            //	projectEntry.setCostKindId(new Integer(((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(dicId.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_ACCOUNTNUMBER")).getSelectedOptions().get(0)));
            if (mpk.getAccountNumber() != null && !mpk.getAccountNumber().equals("") && !mpk.getAccountNumber().equals("dummy"))
                projectEntry.setCostKindId(Integer.valueOf(mpk.getAccountNumber()));

            //if (DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(dicId.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_AMOUNT") != null)
            //	projectEntry.setGross(new BigDecimal(DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(dicId.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_AMOUNT").toString().replace(" ", "")));

            String centrumCode = mpk.getCentrumCode(); //((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(dicId.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMCODE")).getSelectedOptions().get(0);
            if (centrumCode != null && !centrumCode.equals("") && !centrumCode.equals("dummy"))
            {
                AccountingCostKind accountingCostKind = AccountingCostKind.find(Integer.valueOf(centrumCode));
                log.debug("accountingCostKind.name: {}", accountingCostKind.getTitle());
                projectEntry.setAccountingCostKind(accountingCostKind);
            }

            if (fm.getValue("ITEM_DESCRIPTION") != null && !fm.getValue("ITEM_DESCRIPTION").equals(""))
                projectEntry.setDescription(fm.getValue("ITEM_DESCRIPTION").toString());
            else
                projectEntry.setDescription(" - ");

            if (projectEntry.getEntryNumber() == null)
            {
                entryNumber = getEntryNumber(caseNumber, stat, result, entryNumber);
                projectEntry.setEntryNumber(caseNumber + "/"+ entryNumber);
            }

            projectEntry.setCostDate(new java.util.Date());
            project.addEntry(projectEntry);
        }

        List<ProjectEntry> projectEntries = ProjectEntry.findByDocNr(docId);

        for (ProjectEntry entry : projectEntries)
        {
//            if (!dictionaryIds.contains(entry.getCostId()))
//                entry.delete();
        }
    }

    private Integer getEntryNumber(String caseNumber, Statement stat, ResultSet result, Integer entryNumber) throws SQLException
    {
        result = stat.executeQuery("SELECT MAX(CAST(SUBSTRING(REVERSE(entryNumber),1,LEN(entryNumber)-LEN(demandNr)-1) AS INT)) FROM dsr_project_entry where demandNr = '" + caseNumber + "'");
        result.next();
        Integer lastEntryNumber = result.getInt(1);
        if (lastEntryNumber == null)
            return entryNumber;
        else if (lastEntryNumber > 0)
        {
            Integer newEntryNumber = lastEntryNumber + 1;
            return newEntryNumber;
        }
        else
        {
            return ++entryNumber;
        }
    }

}
