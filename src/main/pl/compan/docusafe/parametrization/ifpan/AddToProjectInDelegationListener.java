package pl.compan.docusafe.parametrization.ifpan;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.core.record.projects.ProjectEntry;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class AddToProjectInDelegationListener implements ExternalActivityBehaviour
{

	private static final Logger log = LoggerFactory.getLogger(AddToProjectInDelegationListener.class);

	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Document document = Document.find(docId);
		Integer entryInde = 1;
		Map<String, BigDecimal> blocadeAmount = new HashMap<String, BigDecimal>();
		Boolean toCorrection = false;
		setBlocadeAmount("DELEGATE_COSTS", blocadeAmount, docId, toCorrection, document);
		setBlocadeAmount("ROZLICZENIE_SRO_IN", blocadeAmount, docId, toCorrection, document);
		
		log.debug("toCorrection = {}", toCorrection);
		toCorrection = setBlocade(blocadeAmount, docId, document, entryInde);
		if (toCorrection)
			execution.take("to delegation");
		else
			execution.take("to fork3");

	}

	private void setBlocadeAmount(String dictionaryName, Map<String, BigDecimal> blocadeAmount, Long docId, Boolean toCorrection, Document document) throws EdmException, SQLException
	{
		List<Long> dictionaryIds = (List<Long>) document.getFieldsManager().getKey(dictionaryName);

		for (ProjectEntry entry : ProjectEntry.getProjectEntryByDocumentId(docId.toString(), null, false))
			if (entry.getDocNr() != null && entry.getDocNr().equals(docId.toString()))
				entry.delete();
		if (dictionaryIds != null)
		for (Long dicId : dictionaryIds)
		{
			String centrumId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get(dictionaryName).getValues(dicId.toString()).get(dictionaryName + "_PLATNIK")).getSelectedOptions().get(0);

			Object costPln = DwrDictionaryFacade.dictionarieObjects.get(dictionaryName).getValues(dicId.toString()).get(dictionaryName + "_COST_PLN");
			if (costPln != null)
			{
				if (blocadeAmount.containsKey(centrumId))
				{
					BigDecimal newAmount = blocadeAmount.get(centrumId).add(new BigDecimal(costPln.toString().replace(" ", "")));
					blocadeAmount.put(centrumId, newAmount);
				}
				else
					blocadeAmount.put(centrumId, (BigDecimal) costPln);
			}
		}
	}

	private boolean setBlocade(Map<String, BigDecimal> blocadeAmount, Long docId, Document document, Integer entryInde) throws EdmException, SQLException
	{
		boolean toCorrection = false;
		for (String key : blocadeAmount.keySet())
		{
			Project project = Project.find(Long.valueOf(key));
			BigDecimal toSpend = project.getToSpend();

			ProjectEntry projectEntry = new ProjectEntry();
			projectEntry.setDocNr(docId.toString());
			String caseNumber = docId + "";
			projectEntry.setCaseNumber(caseNumber);

			String year = DateUtils.formatYear(new java.util.Date());

			projectEntry.setEntryNumber(caseNumber + "/" + year + "/" + entryInde++);

			projectEntry.setGross(blocadeAmount.get(key));

			if (document.getFieldsManager().getValue("TRIP_PURPOSE") != null && !document.getFieldsManager().getValue("TRIP_PURPOSE").equals(""))
			{
				projectEntry.setDescription(document.getFieldsManager().getValue("TRIP_PURPOSE").toString());
			}
			else
			{
				projectEntry.setDescription(" - ");
			}

			projectEntry.setEntryType("BLOCADE");
			projectEntry.setCurrency("PLN");
			projectEntry.setDateEntry(new java.util.Date());
			projectEntry.setCostKindId(6);
			projectEntry.setCostDate(new Date());

			log.debug("Project: {}, To spend: {}, projectEntry.getGross(): {}", key, toSpend, projectEntry.getGross());
			if (toSpend.compareTo(projectEntry.getGross()) == -1)
			{
				log.debug("toSpend.compareTo(projectEntry.getGross()) = {}", toSpend.compareTo(projectEntry.getGross()));
				toCorrection = true;
				
				//TODO zopytmalizowaŠ - najpierw dodajemy, potem usuwamy
				for (ProjectEntry entry : ProjectEntry.getProjectEntryByDocumentId(docId.toString(), null, false))
					if (entry.getDocNr() != null && entry.getDocNr().equals(docId.toString()))
						entry.delete();
				
				return toCorrection;
			}
			
			project.addEntry(projectEntry);
			String dictionaryName = "DELEGATE_COSTS";

			List<Long> dictionaryIds = (List<Long>) document.getFieldsManager().getKey(dictionaryName);
			if (dictionaryIds != null)
			for (Long dicId : dictionaryIds)
			{
				String centrumId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get(dictionaryName).getValues(dicId.toString()).get(dictionaryName + "_PLATNIK")).getSelectedOptions().get(0);
				if (centrumId.equals(key))
				{
					FieldData fieldId = new FieldData(Field.Type.STRING, dicId);
					Map<String, FieldData> values = new HashMap<String, FieldData>();
					values.put("id", fieldId);
					FieldData nrBlocade = new FieldData(Field.Type.STRING, projectEntry.getEntryNumber());
					values.put("NRBLOCADE", nrBlocade);
					DwrDictionaryFacade.dictionarieObjects.get(dictionaryName).update(values);
				}
			}

			dictionaryName = "ROZLICZENIE_SRO_IN";

			dictionaryIds = (List<Long>) document.getFieldsManager().getKey(dictionaryName);
			if (dictionaryIds != null)
			for (Long dicId : dictionaryIds)
			{
				String centrumId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get(dictionaryName).getValues(dicId.toString()).get(dictionaryName + "_PLATNIK")).getSelectedOptions().get(0);
				if (centrumId.equals(key))
				{
					FieldData fieldId = new FieldData(Field.Type.STRING, dicId);
					Map<String, FieldData> values = new HashMap<String, FieldData>();
					values.put("id", fieldId);
					FieldData nrBlocade = new FieldData(Field.Type.STRING, projectEntry.getEntryNumber());
					values.put("NRBLOCADE", nrBlocade);
					DwrDictionaryFacade.dictionarieObjects.get(dictionaryName).update(values);
				}
			}
		}
		return toCorrection;
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{

	}

}
