package pl.compan.docusafe.parametrization.ifpan;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.PersonDictionary;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.LogicUtils;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionType;

import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.*;

/**
 * ZamowieniaUmowaPzpLogic
 *
 * @version $Id$
 */
public class ZamowieniaUmowaPzpLogic extends AbstractIfpanDocumentLogic {
	private static final String CONTRACTOR_FIELD_NAME = "CONTRACTOR";
	
	private static Logger log = LoggerFactory.getLogger(ZamowieniaUmowaPzpLogic.class);

	@Override
	public void archiveActions(Document documen, int type, ArchiveSupport as) throws EdmException {
		boolean ok = true;
		if (documen != null) {
			OfficeDocument doc = (OfficeDocument) documen;
			try {
				String nrSprawy = (String) doc.getFieldsManager().getValue("NR_SPRAWY");
				String[] czesci = nrSprawy.split("/");

				String query = "select count(1) from dsg_ifpan_zamowienia_umowa_pzp where nr_sprawy like('NZU/" + czesci[1] + "%/%');";
				Statement stat = DSApi.context().createStatement();
				ResultSet resSet = stat.executeQuery(query);

				while (resSet.next()) {
					int count = resSet.getInt(1);
					if (count == 1) //pierwszy klon
					{
					} else if (count > 1)//kolejny klon
					{
						try {
							Integer i = Integer.parseInt(czesci[1]);
							ok = false;
						} catch (NumberFormatException e) {

						}

					}
				}

			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
		if (!ok)
			throw new EdmException("Nie mozna procesowa? sklonowanego pisma");
	}

    /*
    @Override
    public void documentPermissions(Document document) throws EdmException {
        //To change body of implemented methods use File | Settings | File Templates.
        OfficeDocument od = (OfficeDocument) document;



    }
    */

	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException {
		TaskListParams params = new TaskListParams();
		try {
			FieldsManager fm = dockind.getFieldsManager(id);

			// Ustawienie statusy dokumentu
			params.setStatus(fm.getValue("STATUS") != null ? (String) fm.getValue("STATUS") : null);

			// Ustawienie nuemru sprawy jako numeru dokumentu
			params.setDocumentNumber(fm.getValue("NR_SPRAWY") != null ? (String) fm.getValue("NR_SPRAWY") : null);

			// ustawienie Nazwy kontrahenta
			if (fm.getKey("C") != null) {
                List<Long> cIds = (List<Long>) fm.getKey("C");
                String organizator = "";
                for (Long cId : cIds)
                {
                    organizator+=(String) DwrDictionaryFacade.dictionarieObjects.get("C").getValues(cId.toString()).get("C_ORGANIZATION");
                }
				params.setAttribute(organizator != null ? organizator : "", 1);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return params;
	}

	/**
	 * @param docId
	 * @param values
	 */
	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values) {

		try {
			// generowanie daty
			values.put("DATE", DateUtils.formatCommonDate(new java.util.Date()).toString());

			Document doc = Document.find(docId);
			FieldsManager fm = doc.getFieldsManager();

			List<Person> contractorList = new ArrayList<Person>();
			List<Long> contractorIds = (List<Long>) fm.getKey("C");
			for (long contractorId : contractorIds) {
				Person contractor = Person.find(contractorId);
				if (contractor != null) {
					contractorList.add(contractor);
				}
			}
			values.put("contractorList", contractorList);
			values.put("CONTRACTOR", contractorList.get(0));
            DSUser user = DSApi.context().getDSUser();
            values.put("USER_Firstname",user.getFirstname());
            values.put("USER_Lastname",user.getLastname());
            values.put("USER_Email",user.getEmail());
			Long totalPrice = 0L;
			List<Map<String, Object>> articleList = new ArrayList<Map<String, Object>>();
			List<Long> caseNumberIds = (List<Long>) fm.getKey("CASE_NUMBER");
			FieldsManagerUtils.FieldValuesGetter getter = new FieldsManagerUtils.DateEnumValuesGetter(false);
			for (Long caseNumberId : caseNumberIds) {
				Document document = Document.find(caseNumberId);
				if (document != null) {
					FieldsManager caseFieldsManager = document.getFieldsManager();
					
					List<Map<String, Object>> dictTemplateValues = FieldsManagerUtils.getMultiDictionaryExtractedValues(caseFieldsManager, "TOWAR", getter);

					for (Map<String, Object> dictValues : dictTemplateValues) {
						totalPrice += ((BigDecimal) dictValues.get("TOWAR_TOTALPRICE")).longValue();
					}

					articleList.addAll(dictTemplateValues);
				}
			}
			values.put("articleList", articleList);
			values.put("totalPrice", totalPrice);

			values.put("SLOWVALUE", TextUtils.numberToText(new BigDecimal(fm.getValue("SZACOWANA_WARTOSC").toString().replace(" ", ""))));

//			####################################################################

//            /*for(String key: fm.getFieldValues().keySet())
//            {
//                values.put(key, fm.getFieldValues().get(key));
//            } */
//			LinkedList<LinkedHashMap<String, Object>> casenum = (LinkedList) fm.getValue("CASE_NUMBER");
//
//			LinkedList<LinkedHashMap<String, Object>> pers = (LinkedList) fm.getValue("CONTRACTOR");
//
//
//			ArrayList<Person> persons = new ArrayList<Person>();
//			ArrayList<CaseNumber> caseNumbers = new ArrayList<CaseNumber>();
//
//			if (pers != null) {
//				for (LinkedHashMap temp : pers) {
//					Person p = Person.find(Long.parseLong((String) temp.get("id")));
//					persons.add(p);
//				}
//			}
////
//			if (casenum != null) {
////				for (LinkedHashMap temp2 : casenum) {
////					String demNum = (String) temp2.get("CASE_NUMBER_DEMAND_NUMBER2");
////					String itemDesc = (String) temp2.get("CASE_NUMBER_ITEM_DESCRIPTION");
////					String zrodlaFin = (String) temp2.get("CASE_NUMBER_ZRODLA_FIN");
////					String brutto = ((String) temp2.get("CASE_NUMBER_DEMAND_NUMBER2"));
////					CaseNumber cn = new CaseNumber(demNum, itemDesc, zrodlaFin, brutto);
////					caseNumbers.add(cn);
////				}
//			}
//
//

//			values.put("CONTRACTORS", persons);
//			values.put("CASE_NUMBERS", caseNumbers);
//


			values.put("TRYB_POSTEPOWANIA", fm.getEnumItem("TRYB_POSTEPOWANIA").getTitle());
			values.put("RODZAJ_ZAMOWIENIA", fm.getEnumItem("RODZAJ_ZAMOWIENIA").getTitle());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn() + "_DOCUMENT_READ", ObjectPermission.GROUP, "ZamowieniaUmowaPzp - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "ZamowieniaUmowaPzp zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn() + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "ZamowieniaUmowaPzp - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "ZamowieniaUmowaPzp zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn() + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "ZamowieniaUmowaPzp - usuwanie"));
		Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
		perms.addAll(documentPermissions);
		this.setUpPermission(document, perms);
	}


	@Override
	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {


		if (acceptationCn.equals("user_accept")) {
			String test = "test";
			String test2 = "test";
			assignable.addCandidateUser(doc.getAuthor());
			return true;
		}
		return false;
	}

	@Override
	public void onAcceptancesListener(Document doce, String acceptationCn) throws EdmException {
        boolean ok = true;
        if (doce != null) {
            OfficeDocument doc = (OfficeDocument) doce;
            try {
                String nrSprawy = (String) doc.getFieldsManager().getValue("NR_SPRAWY");
                String[] czesci = nrSprawy.split("/");

                String query = "select count(1) from dsg_ifpan_zamowienia_umowa_pzp where nr_sprawy like('NZU/" + czesci[1] + "%/%');";
                Statement stat = DSApi.context().createStatement();
                ResultSet resSet = stat.executeQuery(query);

                while (resSet.next()) {
                    int count = resSet.getInt(1);
                    if (count == 1) //pierwszy klon
                    {
                    } else if (count > 1)//kolejny klon
                    {
                        try {
                            Integer i = Integer.parseInt(czesci[1]);
                            ok = false;
                        } catch (NumberFormatException e) {

                        }

                    }
                }

            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        if (!ok)
            throw new EdmException("Nie mozna procesowa� sklonowanego pisma");
	}

	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException {
		OfficeDocument doc = OfficeDocument.find(documentId);
		FieldsManager fm = doc.getFieldsManager();
		Integer status = (Integer) fm.getKey("STATUS");

//        if((status != null && status==10) && (OfficeDocument.find(documentId).getAttachments()== null || OfficeDocument.find(documentId).getAttachments().size()==0)) throw new EdmException("Aby przekaza? pismno dalej nale?y za??czyc plik PDF");
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) {

		try {
			super.onStartProcess(document, event);
			FieldsManager fm = document.getFieldsManager();

			List<Long> suggContractors = new LinkedList<Long>();
			LinkedList<LinkedHashMap> caseNumbers = (LinkedList<LinkedHashMap>) fm.getValue("CASE_NUMBER");

//            Object obj = fm.getValue("CONTRACTOR");


			if (caseNumbers != null) {
				for (LinkedHashMap caseNumber : caseNumbers) {
					OfficeDocument doc = OfficeDocument.find(Long.parseLong((String) caseNumber.get("id")));
					FieldsManager fman = doc.getFieldsManager();

					HashMap<String, Object> tempContractor = new LinkedHashMap<String, Object>();


					LinkedHashMap ids = (LinkedHashMap) fman.getValue("SUG_CONTRACTOR");
					suggContractors.add(Long.parseLong((String) ids.get("id")));

					HashMap<String, Object> mapa = new HashMap<String, Object>();
					mapa.put("CASENUMBER2", document.getId());
					fman.reloadValues(mapa);
					fman.getDocumentKind().setOnly(fman.getDocumentId(), mapa);
				}
			}

//            HashMap<String, Object> mapa = new HashMap<String, Object>();
//            mapa.put("CONTRACTOR", suggContractors);
//            fm.reloadValues(mapa);
//            fm.getDocumentKind().setOnly(document.getId(), mapa);

//            String query = "insert into dsg_ifpan_zapotrz2(document_id, field_cn, field_val) values(?,?,?)";
//            PreparedStatement st = DSApi.context().prepareStatement(query);
//            st.setLong(1, document.getId());
//            st.setString(2, "CONTRACTOR");
//            for(Long id: suggContractors)
//            {
//                st.setLong(3, id);
//                st.execute();
//            }

			Long journalId = Long.parseLong(Docusafe.getAdditionProperty("umowa_pzp_journal_id"));
			Date entryDate = new Date();

			Integer sequenceId = Journal.TX_newEntry2(journalId, document.getId(), entryDate);

			((OutOfficeDocument) document).bindToJournal(journalId, sequenceId, entryDate);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		} finally {

		}
	}

	public void specialSetDictionaryValues(Map<String, ?> dockindFields) {
		log.info("Special dictionary values: {}", dockindFields);
		if (dockindFields.keySet().contains("M_DICT_VALUES")) {
			Map<String, Map> mMaps = (Map<String, Map>) dockindFields.get("M_DICT_VALUES");
			for (String key : mMaps.keySet()) {
				if (key.contains("C_")) {
					mMaps.get(key).put("DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
					mMaps.get(key).put("ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
				}
			}

		}
	}

	public void addSpecialInsertDictionaryValues(String dictionaryName, Map<String, FieldData> values) {
		log.info("Before Insert - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.equals("C")) {
			Person p = new Person();
			p.createBasePerson(dictionaryName, values);
			try {
				p.create();
			} catch (EdmException e) {
				log.error("", e);
			}
			PersonDictionary.prepareFieldNamesForSenderPerson(p.getId(), dictionaryName, values);
		}
	}

	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields) {
		log.info("Before Search - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.equals("C")) {
			values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
		//super.addSpecialSearchDictionaryValues(dictionaryName, values);
	}

	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {

		if (values.get("DWR_CASE_NUMBER") != null && values.get("DWR_CASE_NUMBER").isSender()) {
			int size = values.get("DWR_CASE_NUMBER").getDictionaryData().size();
			BigDecimal suma = BigDecimal.ZERO;
            BigDecimal sumaFaktur = BigDecimal.ZERO;
			for (int i = 1; i <= size / 6; i++)
            {
				String zapoId = values.get("DWR_CASE_NUMBER").getDictionaryData().get("CASE_NUMBER_ID_" + i).getStringData();
                String subZapo = values.get("DWR_CASE_NUMBER").getDictionaryData().get("CASE_NUMBER_SUGG_BRUTTO_" + i).getStringData();
				boolean isOpened = true;
				if (!DSApi.isContextOpen())
                {
					isOpened = false;
					DSApi.openAdmin();
				}
				try
                {
                    suma = suma.add(new BigDecimal(subZapo));
                    Statement st2 = DSApi.context().createStatement();
                    String query2 = "select document_id from dsg_ifpan_faktura_multiple_value where field_cn = 'ZAPOTRZEBOWANIE' and field_val = " + zapoId;
                    ResultSet rs2 = st2.executeQuery(query2);

                    while(rs2.next())
                    {
                        Document fakZapo = Document.find(rs2.getLong("document_id"));
                        BigDecimal brutto = (BigDecimal) fakZapo.getFieldsManager().getKey("KWOTA_BRUTTO");
                        sumaFaktur = sumaFaktur.add(brutto);
                    }
				} catch (Exception e) {
                     log.error(e.getMessage(), e);
				} finally {
					if (DSApi.isContextOpen() && !isOpened)
						DSApi.close();
				}
			}
			values.put("DWR_SZACOWANA_WARTOSC", new FieldData(Field.Type.MONEY, suma.setScale(2,4)));
			Double kurs = Double.parseDouble(Docusafe.getAdditionProperty("kurs_euro"));
			BigDecimal kursBigDecimal = new BigDecimal(kurs);
			BigDecimal podatek = new BigDecimal(0.77);

			BigDecimal sumaNetto = suma.multiply(podatek);
			if (kursBigDecimal.compareTo(BigDecimal.ZERO) > 0 && sumaNetto.compareTo(BigDecimal.ZERO) > 0) {
				BigDecimal nettoEuro = sumaNetto.divide(kursBigDecimal, MathContext.DECIMAL128);
				values.put("DWR_SZACOWANA_EURO", new FieldData(Field.Type.MONEY, nettoEuro));
			}
			values.put("DWR_SUMA_FAKTUR", new FieldData(Field.Type.MONEY, sumaFaktur.setScale(2,4)));

		}
		return null;
	}


	@Override
	public List<String> getActionMessage(ActionType source, Document doc) {
		List<String> messages = new ArrayList<String>();
		try {
			OfficeDocument od = OfficeDocument.find(doc.getId());
			FieldsManager fm = od.getFieldsManager();
			fm.initialize();
			fm.initializeValues();
			String znacznikZamowienia = (String) fm.getValue("ZNACZNIK_ZAMOWIENIA");
			String format = DateFormat.getDateInstance().format(new Date());
			String nrSprawy = "NZU" + "/" + od.getOfficeNumber() + "/" + format.substring(0, 4);
			Map<String, Object> toReload = new HashMap<String, Object>();
			toReload.put("NR_SPRAWY", nrSprawy);
			doc.getDocumentKind().setOnly(doc.getId(), toReload);
			switch (source) {
				case OFFICE_CREATE_DOCUMENT:
					messages.add("Przyj�to pismo o numerze " + nrSprawy);
					break;
				default:
					break;
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return messages;
	}
	
   public void onLoadData(FieldsManager fm) throws EdmException {
	   LogicUtils.setPopupButtonsOnPersonDictionaryByPermission(fm, CONTRACTOR_FIELD_NAME);			
   }
}
