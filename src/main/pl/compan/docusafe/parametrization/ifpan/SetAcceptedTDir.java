package pl.compan.docusafe.parametrization.ifpan;

import edu.emory.mathcs.backport.java.util.Collections;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: slim
 * Date: 27.11.13
 * Time: 14:20
 * To change this template use File | Settings | File Templates.
 */
public class SetAcceptedTDir implements ExternalActivityBehaviour
{
    private static Logger log = LoggerFactory.getLogger(SetAcceptedTDir.class);

    @Override
    public void signal(ActivityExecution activityExecution, String s, Map<String, ?> stringMap) throws Exception {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void execute(ActivityExecution execution) throws Exception {
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        Document document = Document.find(docId);
        document.getFieldsManager().reloadValues(Collections.singletonMap("ACCEPTED", new Boolean(true)));
        document.getDocumentKind().setOnly(document.getId(), Collections.singletonMap("ACCEPTED", new Boolean(true)));
    }
}
