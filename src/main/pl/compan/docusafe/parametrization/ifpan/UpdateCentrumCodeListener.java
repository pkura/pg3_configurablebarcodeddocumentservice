package pl.compan.docusafe.parametrization.ifpan;

import java.util.List;
import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.record.projects.AccountingCostKind;
import pl.compan.docusafe.core.record.projects.ProjectEntry;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class UpdateCentrumCodeListener  implements ExternalActivityBehaviour
{
	private Logger log = LoggerFactory.getLogger(UpdateCentrumCodeListener.class);
	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		updateCentrumCode(docId);
		execution.takeDefaultTransition();
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		// TODO Auto-generated method stub
	}

	private void updateCentrumCode(Long docId) throws DocumentNotFoundException, DocumentLockedException, AccessDeniedException, EdmException
	{
		String dicName = "DSG_CENTRUM_KOSZTOW_FAKTURY";
		List<Long> dictionaryIds = (List<Long>) Document.find(docId).getFieldsManager().getKey(dicName);
		for (Long dicId : dictionaryIds)
		{
			CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(dicId);
			ProjectEntry projectEntry = ProjectEntry.findByCostId(dicId);
			if (projectEntry == null) continue;
			String centrumCode = mpk.getCentrumCode();
			if (centrumCode != null && !centrumCode.equals("") && !centrumCode.equals("dummy"))
			{
				AccountingCostKind accountingCostKind = AccountingCostKind.find(Integer.valueOf(centrumCode));
				log.debug("accountingCostKind.name: {}", accountingCostKind.getTitle());
				projectEntry.setAccountingCostKind(accountingCostKind);
				projectEntry.update();
			}

		}
		
		List<ProjectEntry> projectEntries = ProjectEntry.findByDocNr(docId);

		for (ProjectEntry entry : projectEntries)
		{
			if (!dictionaryIds.contains(entry.getCostId()))
				entry.delete();
		}
	}
}
