package pl.compan.docusafe.parametrization.ifpan;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ContractMoneyDecision implements ExternalActivityBehaviour
{
	private static final Logger log = LoggerFactory.getLogger(ContractMoneyDecision.class);

	public void execute(ActivityExecution execution) throws Exception {
		BigDecimal rate;
		
		try {
			String rateRaw = Docusafe.getAdditionProperty("kurs_euro");
			rate = new BigDecimal(rateRaw).setScale(4, RoundingMode.HALF_UP);
		} catch (Exception e) {
			log.error("B��d w pobieraniu kursu euro, u�yto warto�ci 4.0000");
			rate = new BigDecimal("4").setScale(4, RoundingMode.HALF_UP);
		}
		
		try {
			Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			Document document = Document.find(docId);
			FieldsManager fm = document.getFieldsManager();
			String amountRaw = fm.getStringValue("CONTRACT_VAL").replace(" ", "").replace(",",".");
			BigDecimal contractVal = new BigDecimal(amountRaw);
			
			contractVal = contractVal.divide(rate, 0, RoundingMode.HALF_UP);
			
			if (contractVal.intValueExact() > 13500) {
				execution.take("to DPIZEauthorDecision");
			} else {
				execution.take("to DZauthorDecision");
			}
			
		} catch (Exception e) {
			execution.take("to DPIZEauthorDecision");
		} finally {
			execution.setVariable("count", 2);
		}

	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
	}

}
