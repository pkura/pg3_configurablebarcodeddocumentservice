package pl.compan.docusafe.parametrization.ifpan;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import org.jfree.util.Log;

public class SecondIsCorrectionAfterForkDecision implements DecisionHandler
{

	public String decide(OpenExecution openExecution)
	{
		Integer cor = (Integer) openExecution.getVariable("correction");
		if (cor != null && cor == 11)
		{
			openExecution.removeVariable("correction");
			openExecution.setVariable("correction", 2);
			openExecution.setVariable("count2", 2);
			return "correction";
		}
		else
		{
			return "normal";
		}
	}
}
