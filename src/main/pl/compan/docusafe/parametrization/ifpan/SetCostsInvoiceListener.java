package pl.compan.docusafe.parametrization.ifpan;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.catalina.tribes.util.Arrays;
import org.drools.util.LinkedList;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.record.projects.AccountingCostKind;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.core.record.projects.ProjectEntry;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SetCostsInvoiceListener implements ExternalActivityBehaviour
{
	private Logger log = LoggerFactory.getLogger(SetCostsInvoiceListener.class);
	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		setCosts(docId);
		execution.takeDefaultTransition();
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		// TODO Auto-generated method stub
	}

	private void setCosts(Long docId) throws DocumentNotFoundException, DocumentLockedException, AccessDeniedException, EdmException
	{
		String dicName = "DSG_CENTRUM_KOSZTOW_FAKTURY";
		List<Long> dictionaryIds = (List<Long>) Document.find(docId).getFieldsManager().getKey(dicName);
		FieldsManager fm = Document.find(docId).getFieldsManager();
		List<Long> classTpeIds = new ArrayList();
		for (Long dicId : dictionaryIds)
		{
			CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(dicId);
			Integer projectId = mpk.getCentrumId();
			BigDecimal costPln = mpk.getRealAmount();
			Project project = Project.find(Long.valueOf(projectId));
			Long mpkId = mpk.getClassTypeId() != null?mpk.getClassTypeId().longValue():mpk.getId();
			ProjectEntry projectEntry = ProjectEntry.findByCostId(mpkId);
			classTpeIds.add(mpkId);
			if (projectEntry == null)
			{
				projectEntry = new ProjectEntry();
				projectEntry.setDateEntry(new java.util.Date());
				projectEntry.setCostId(dicId);
				projectEntry.setCurrency("PLN");
				projectEntry.setRate(BigDecimal.valueOf(1.0000D));
				if (fm.getValue("DESCRIPTION") != null && !fm.getValue("DESCRIPTION").equals(""))
					projectEntry.setDescription(fm.getValue("DESCRIPTION").toString());
				else 
					projectEntry.setDescription("-");
			}
			if (fm.getValue("DESCRIPTION") != null && !fm.getValue("DESCRIPTION").equals(""))
				projectEntry.setDescription(fm.getValue("DESCRIPTION").toString());
			projectEntry.setEntryType("COST");
			projectEntry.setGross(costPln);
			projectEntry.setMtime(new java.util.Date());
			projectEntry.setDocNr(docId.toString());
			String costKindID = mpk.getAccountNumber();
			if (costKindID != null && !costKindID.equals("") && !costKindID.equals("dummy"))
			{
				projectEntry.setCostKindId(Integer.valueOf(costKindID));
			}
			String centrumCode = mpk.getCentrumCode();
			if (centrumCode != null && !centrumCode.equals("") && !centrumCode.equals("dummy"))
			{
				AccountingCostKind accountingCostKind = AccountingCostKind.find(Integer.valueOf(centrumCode));
				log.debug("accountingCostKind.name: {}", accountingCostKind.getTitle());
				projectEntry.setAccountingCostKind(accountingCostKind);
			}
			projectEntry.setCostDate(new java.util.Date());
			project.addEntry(projectEntry);
		}
		
		List<ProjectEntry> projectEntries = ProjectEntry.findByDocNr(docId);

		for (ProjectEntry entry : projectEntries)
		{
			if (!classTpeIds.contains(entry.getCostId()))
				entry.delete();
		}
	}
}
