package pl.compan.docusafe.parametrization.ifpan;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.core.record.projects.ProjectEntry;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SetCostsListener implements ExternalActivityBehaviour
{
	
	private static final Logger log = LoggerFactory.getLogger(SetCostsListener.class);
	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Document document = Document.find(docId);
		String dicName = "WYDATKOWANE_SRODKI";
		int entryInde = 0;
		Map<String, ProjectEntry> projectEntryCosts = new HashMap<String, ProjectEntry>();
		
		List<Long> dictionaryIds = (List<Long>) document.getFieldsManager().getKey(dicName);
		
		for (Long dicId : dictionaryIds)
		{
			String centrumId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get(dicName).getValues(dicId.toString()).get(dicName + "_WYD_PLATNIK")).getSelectedOptions().get(0);
			
			Object costPln = DwrDictionaryFacade.dictionarieObjects.get(dicName).getValues(dicId.toString()).get(dicName + "_PLN");
			Object description = DwrDictionaryFacade.dictionarieObjects.get(dicName).getValues(dicId.toString()).get(dicName + "_DESCRIPTION");
			entryInde = ProjectEntry.getProjectEntryByDocumentId(docId.toString(), null, false).size() + 1;
			if (costPln != null)
			{
				BigDecimal newAmount = new BigDecimal(costPln.toString().replace(" ", ""));
				if (projectEntryCosts.containsKey(centrumId))
				{
					ProjectEntry entry = projectEntryCosts.get(centrumId);
					entry.setGross(entry.getGross().add(newAmount));
					entry.setDescription(entry.getDescription()+", "+description);
					projectEntryCosts.put(centrumId, entry);
				}
				else
				{
					ProjectEntry projectEntry = new ProjectEntry();
					projectEntry.setDateEntry(new java.util.Date());
					projectEntry.setCostDate(new Date());
					projectEntry.setDocNr(docId.toString());
					projectEntry.setEntryType(ProjectEntry.COST);
					projectEntry.setCurrency("PLN");
					projectEntry.setGross(newAmount);
					projectEntry.setCostKindId(6);
					String caseNumber = docId + "";
					projectEntry.setCaseNumber(caseNumber);
					String year = DateUtils.formatYear(new java.util.Date());
					projectEntry.setEntryNumber(caseNumber + "/" + year + "/" + entryInde);
					projectEntry.setDescription(description== null ? "" : (String)description);					
					projectEntryCosts.put(centrumId, projectEntry);
				}
			}
		}
		
		for (String key : projectEntryCosts.keySet())
		{
			Project project = Project.find(Long.valueOf(key));
			boolean setCost = false;
			for (ProjectEntry entry : project.getEntries())
			{
				setCost = false;
				if (entry.getDocNr() != null && entry.getDocNr().equals(docId.toString()) && entry.getEntryType().equals(ProjectEntry.BLOCADE))
				{
					entry.setEntryType(ProjectEntry.COST);
					entry.setCurrency("PLN");
					entry.setCostDate(new Date());
					entry.setDescription(projectEntryCosts.get(key).getDescription());
					entry.setGross(projectEntryCosts.get(key).getGross());
					entry.update();
					setCost = true;
					break;
				}
			}
			
			if (!setCost)
			{
				ProjectEntry projectEntry = projectEntryCosts.get(key);
				projectEntry.setDateEntry(new java.util.Date());
				projectEntry.setDocNr(docId.toString());
				projectEntry.setEntryType(ProjectEntry.COST);
				projectEntry.setCurrency("PLN");
				project.addEntry(projectEntry);
			}
			
			/*for (Long dicId : dictionaryIds)
			{
				String centrumId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get(dicName).getValues(dicId.toString()).get(dicName + "_WYD_PLATNIK")).getSelectedOptions().get(0);
				if (centrumId.equals(key))
				{
					FieldData fieldId = new FieldData(Field.Type.STRING, dicId);
					Map<String, FieldData> values = new HashMap<String, FieldData>();
					values.put("id", fieldId);
					FieldData nrBlocade = new FieldData(Field.Type.STRING, projectEntry.getEntryNumber());
					values.put("NR_COST", nrBlocade);
					DwrDictionaryFacade.dictionarieObjects.get(dicName).update(values);
				}
			}*/
		}
		
		execution.takeDefaultTransition();
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		// TODO Auto-generated method stub
		
	}

}
