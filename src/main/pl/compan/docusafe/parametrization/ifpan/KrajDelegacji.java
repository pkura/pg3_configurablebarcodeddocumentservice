package pl.compan.docusafe.parametrization.ifpan;

public class KrajDelegacji
{
	private String nazwa;
	private String odData;
	private String doData;
	private String suma;
	
	public KrajDelegacji(String nazwa, String odData, String doData, String suma)
	{
		this.nazwa = nazwa;
		this.odData = odData;
		this.doData = doData;
		this.suma = suma;
	}
	public String getNazwa()
	{
		return nazwa;
	}
	public void setNazwa(String nazwa)
	{
		this.nazwa = nazwa;
	}
	public String getOdData()
	{
		return odData;
	}
	public void setOdData(String odData)
	{
		this.odData = odData;
	}
	public String getDoData()
	{
		return doData;
	}
	public void setDoData(String doData)
	{
		this.doData = doData;
	}
	public String getSuma()
	{
		return suma;
	}
	public void setSuma(String suma)
	{
		this.suma = suma;
	}
	
	
}
