package pl.compan.docusafe.parametrization.ifpan;

import java.math.BigDecimal;

public class Rozliczenie
{
	private String waluta;
	private BigDecimal kurs;
	private BigDecimal wydalem = new BigDecimal(0).setScale(2);
	private BigDecimal wydalemPln = new BigDecimal(0).setScale(2);
	private BigDecimal dysponowalem = new BigDecimal(0).setScale(2);
	private BigDecimal dysponowalemPln = new BigDecimal(0).setScale(2);
	private BigDecimal pozostalo = new BigDecimal(0).setScale(2);
	private BigDecimal pozostaloPln = new BigDecimal(0).setScale(2);
	private BigDecimal brakuje = new BigDecimal(0).setScale(2);
	private BigDecimal brakujePln = new BigDecimal(0).setScale(2); 
	private BigDecimal pln;
	
	public String getWaluta()
	{
		return waluta;
	}
	public void setWaluta(String waluta)
	{
		this.waluta = waluta;
	}
	public BigDecimal getWydalem()
	{
		return wydalem;
	}
	public void setWydalem(BigDecimal wydalem)
	{
		this.wydalem = wydalem;
	}
	public BigDecimal getDysponowalem()
	{
		return dysponowalem;
	}
	public void setDysponowalem(BigDecimal dysponowalem)
	{
		this.dysponowalem = dysponowalem;
	}
	public BigDecimal getPozostalo()
	{
		return pozostalo;
	}
	public void setPozostalo(BigDecimal pozostalo)
	{
		this.pozostalo = pozostalo;
	}
	public BigDecimal getBrakuje()
	{
		return brakuje;
	}
	public void setBrakuje(BigDecimal brakuje)
	{
		this.brakuje = brakuje;
	}
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kurs == null) ? 0 : kurs.hashCode());
		result = prime * result + ((waluta == null) ? 0 : waluta.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rozliczenie other = (Rozliczenie) obj;
		if (kurs == null)
		{
			if (other.kurs != null)
				return false;
		}
		else if (!kurs.equals(other.kurs))
			return false;
		if (waluta == null)
		{
			if (other.waluta != null)
				return false;
		}
		else if (!waluta.equals(other.waluta))
			return false;
		return true;
	}
	public BigDecimal getKurs()
	{
		return kurs;
	}
	public void setKurs(BigDecimal kurs)
	{
		this.kurs = kurs;
	}
	public BigDecimal getPln()
	{
		return pln;
	}
	public void setPln(BigDecimal pln)
	{
		this.pln = pln;
	}
	public BigDecimal getWydalemPln()
	{
		return wydalemPln;
	}
	public void setWydalemPln(BigDecimal wydalemPln)
	{
		this.wydalemPln = wydalemPln;
	}
	public BigDecimal getDysponowalemPln()
	{
		return dysponowalemPln;
	}
	public void setDysponowalemPln(BigDecimal dysponowalemPln)
	{
		this.dysponowalemPln = dysponowalemPln;
	}
	public BigDecimal getPozostaloPln()
	{
		return pozostaloPln;
	}
	public void setPozostaloPln(BigDecimal pozostaloPln)
	{
		this.pozostaloPln = pozostaloPln;
	}
	public BigDecimal getBrakujePln()
	{
		return brakujePln;
	}
	public void setBrakujePln(BigDecimal brakujePln)
	{
		this.brakujePln = brakujePln;
	}

}
