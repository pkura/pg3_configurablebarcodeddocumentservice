package pl.compan.docusafe.parametrization.ifpan;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class IsCreditProject implements DecisionHandler
{
	private static final Logger log = LoggerFactory.getLogger(IsCreditProject.class);

	public String decide(OpenExecution execution)
	{
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");

		try
		{
			Document document = Document.find(docId);
			if (isCredit(document.getFieldsManager()))
				return "credit";
			else
				return "normal";
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}

		return "credit";
	}

	private boolean isCredit(FieldsManager fm) throws EdmException
	{
		List<Long> costsId = (List<Long>) fm.getKey("DELEGATE_COSTS");

		if (costsId == null)
			throw new EdmException("Brak kosztow delegacji");

		Map<String, Object> advancesValues = new HashMap<String, Object>();
		Project project = null;
		for (Long advnaceId : costsId)
		{
			advancesValues = DwrDictionaryFacade.dictionarieObjects.get("DELEGATE_COSTS").getValues(advnaceId.toString());
			String typId = ((EnumValues) advancesValues.get("DELEGATE_COSTS_PLATNIK")).getSelectedOptions().get(0);
			try
			{
				project = Project.find(Long.valueOf(typId));
				if (project.getProjectType().equals(Project.PROJECT_TYPE_KREDYT))
				{
					return true;
				}
			}
			catch (EntityNotFoundException e)
			{
				log.error(e.getMessage(), e);
			}
			catch (EdmException e)
			{
				log.error(e.getMessage(), e);
			}
		}
		return false;
	}
}
