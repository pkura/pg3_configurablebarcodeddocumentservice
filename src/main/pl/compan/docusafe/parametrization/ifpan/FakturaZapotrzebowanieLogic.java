package pl.compan.docusafe.parametrization.ifpan;

import com.google.common.collect.Maps;

import org.hibernate.criterion.Restrictions;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.*;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.LogicUtils;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.*;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionType;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.*;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

public class FakturaZapotrzebowanieLogic extends AbstractIfpanDocumentLogic
{
	private static FakturaZapotrzebowanieLogic instance;
	private static Logger log = LoggerFactory.getLogger(FakturaZapotrzebowanieLogic.class);
	private static final StringManager sm = GlobalPreferences.loadPropertiesFile(FakturaZapotrzebowanieLogic.class.getPackage().getName(), null);
	private final String MAJATEK = "akceptacja_majatku";
	private final String RESTDIR = "restDirs";
	private static final String CONTRACTOR_FIELD_NAME = "CONTRACTOR";
	public final static String DPIR_PERSON = "dpirPerson";
	
	public static FakturaZapotrzebowanieLogic getInstance()
	{
		if (instance == null)
			instance = new FakturaZapotrzebowanieLogic();
		return instance;
	}

	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
	{
		Object o = Document.find(documentId).getFieldsManager().getKey("STATUS");
		//<enum-item id="5" cn="delivery_division" title="Akceptacja Dzia�u Zaopatrzenia"/>
		if (fieldValues.get("KWOTA_NETTO") != null && fieldValues.get("KWOTA_BRUTTO") != null && fieldValues.get("DSG_CENTRUM_KOSZTOW_FAKTURY") != null && o != null && ((Integer) o).equals(5)) {
            checkSumCosts(fieldValues);
        }
		if (fieldValues.get(CONTRACTOR_FIELD_NAME) != null) {
			tryCreateSenderPersonOnSaveDoc(kind, documentId, fieldValues, CONTRACTOR_FIELD_NAME);
		}

	}

	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values)
	{

		Map<String, Object> results = new HashMap<String, Object>();
		if (dictionaryName.equals("DSG_CENTRUM_KOSZTOW_FAKTURY"))
		{
			
			if (values.get("DSG_CENTRUM_KOSZTOW_FAKTURY_AMOUNT").getData() != null)
			{
				BigDecimal kwota = ((FieldData) values.get("DSG_CENTRUM_KOSZTOW_FAKTURY_AMOUNT")).getMoneyData();
				results.put("DSG_CENTRUM_KOSZTOW_FAKTURY_AMOUNT", kwota);
			}
		}
		
		return results;
	}
	    
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		log.info("DokumentKind: {}, DocumentId: {}, Values from JavaScript: {}", fm.getDocumentKind().getCn(), fm.getDocumentId(), values);
        boolean isOpened = true;
		try
		{

//            DSContext cont = DSApi.openAdmin();
//            boolean test =   cont.isAdmin();
//            boolean test2 = cont.session().isOpen();


            if (!DSApi.isContextOpen())
            {
                isOpened = false;
                DSApi.openAdmin();
            }
//                    ((Mailer) ServiceManager.getService(Mailer.NAME)).send("filip.test@filip.pl", "filip.test@filip.pl", "xxxxxx", "yyyyyyyyy");
			// automatycznie wyliczanie kwoty brutto jako sumy wartosci pozycji kosztowych - nie wiem po co to jest;/??
			//if (values.get("DWR_DSG_CENTRUM_KOSZTOW_FAKTURY").getDictionaryData() != null)
				//setBrutto(values);
			
			// automatycznie wylcizanie kwoty netto na podstawie wzor: KwotaWWalucie*Kurs
			if (values.get("DWR_KWOTA_W_WALUCIE").getMoneyData()!= null && values.get("DWR_KURS").getData()!= null)
				setNetto(values);

			// autoamtycznie wylcizenie kwoty brutto wg wzoru: netto+vat
			if (values.get("DWR_KWOTA_NETTO") != null && values.get("DWR_KWOTA_NETTO").getData() != null)
				setBruttoAsNettoPlusVat(values);
			
			// uzupelnienie centrom koszto faktury na podstawie podlaczonego zapotrzebowania
			if (values.get("DWR_ZAPOTRZEBOWANIE") != null)
				setCostCenters(values);

			StringBuilder procedureMsg = new StringBuilder();

//            if (values.get("DWR_"+CONTRACTOR_FIELD_NAME)!= null && values.get("DWR_"+CONTRACTOR_FIELD_NAME).getDictionaryData() != null) {
//				setContractorFlag(values, CONTRACTOR_FIELD_NAME);
//				createPersonFromChoosenErpId(values, CONTRACTOR_FIELD_NAME);
//			}


			
			if (!procedureMsg.toString().equals("")) {
			    values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
			    return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", procedureMsg.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		} finally {
            if (DSApi.isContextOpen() && !isOpened)
                DSApi.close();
		}

		return null;
	}

	
	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn() + "_DOCUMENT_READ", ObjectPermission.GROUP, "Faktura kosztowa/Zapotrzebowanie - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Faktura kosztowa/Zapotrzebowanie zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn() + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Faktura kosztowa/Zapotrzebowanie - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Faktura kosztowa/Zapotrzebowanie zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn() + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Faktura kosztowa/Zapotrzebowanie - usuwanie"));
		Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
		perms.addAll(documentPermissions);
		this.setUpPermission(document, perms);
	}

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Faktury kosztowe powi�zane z Zapotrzebowaniem");
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(fm.getValue("DATA_WYSTAWIENIA")));
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(fm.getValue("DATA_WYSTAWIENIA")));
		document.setFolder(folder);

		String strFakturaNumer = "Faktura kosztowa nr " + String.valueOf(fm.getValue("NUMER_FAKTURY")) + " " + sm.getString("PowiazanaZZapotrzebowaniem"); 
		document.setTitle(strFakturaNumer);
		document.setDescription(strFakturaNumer);
	
		if(fm.containsField("CONTRACTOR"))
		{
			Long contractorId = (Long)fm.getKey("CONTRACTOR");
			Person p = new Person();
			Person contractor = p.find(contractorId);
			((OfficeDocument)document).setSender((Sender)contractor);
		}
	}

    @Override
    public List<String> getActionMessage(ActionType source, Document doc) {
        List<String> messages = new ArrayList<String>();
        try {
            OfficeDocument od = OfficeDocument.find(doc.getId());
            FieldsManager fm = od.getFieldsManager();
            fm.initialize();
            fm.initializeValues();
            String format = DateFormat.getDateInstance().format(new Date());
            String nrSprawy = "NF/"+od.getOfficeNumber()+"/"+format.substring(0, 4);
            Map<String, Object> toReload = new HashMap<String, Object>();
            toReload.put("NUMER_FAKTURY_KO", nrSprawy);
            doc.getDocumentKind().setOnly(doc.getId(), toReload);
            switch (source) {
                case OFFICE_CREATE_DOCUMENT:
                    messages.add("Przyj�to pismo o numerze " + nrSprawy);
                    break;
                default:
                    break;
            }
        }catch(Exception e)
        {
            log.error(e.getMessage(), e);
        }
        return messages;
    }

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn)
	{
		boolean assigned = false;
		try
		{
			log.info("DokumentKind: {}, DokumentId: {}', acceptationCn: {}, enumCn: {}", doc.getDocumentKind().getCn(), doc.getId(), accpetionCn, enumCn);

			// akcpetacja majatku trwa�ego
			if (accpetionCn.equals(MAJATEK)){
				assigned = assigneToMajatekTrwaly(doc, assignable, openExecution);
			}
			// akceptacja drugiego dyrektora
			else if (accpetionCn.equals(RESTDIR)){
				assigned = assigneToRestDirs(doc, assignable, openExecution);
			}
			else if(accpetionCn.equals(DPIR_PERSON))
			{
				String zrodloId = (String) openExecution.getVariable("secCostCenterId");
				Project project = Project.find(Long.valueOf(zrodloId));
				if(project != null && project.getResponsiblePerson() != null){
					assignable.addCandidateUser(project.getResponsiblePerson().getName());
					assigned = true;
				} else {
					assignable.addCandidateUser("aradomyska");
					assigned = true;
				}
				
			}
		}
		catch (Exception ee)
		{
			log.error(ee.getMessage(), ee);
		}
		return assigned;
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		log.info("--- FakturaZapotrzebowaniaLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			
			java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

			DSUser author = DSApi.context().getDSUser();
			String user = author.getName();
			String fullName = author.getLastname() + " " + author.getFirstname();

			perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

			Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
			perms.addAll(documentPermissions);
			this.setUpPermission(document, perms);
			
			DSApi.context().watch(URN.create(document));

            Long journalId = Long.parseLong(Docusafe.getAdditionProperty("costinvoice_journal_id"));
            Date entryDate = new Date();

            Integer sequenceId = Journal.TX_newEntry2(journalId, document.getId(), entryDate);

            ((InOfficeDocument) document).bindToJournal(journalId, sequenceId);

		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}

	}

	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		try
		{
			Document doc = Document.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			
			// Document_ID
			values.put("DOCUMENT_ID", doc.getId());

			// Ustawienie kontraktora
			setContractor(fm, values);
			
			// Uzytwkonik generujacy
			DSUser user = DSApi.context().getDSUser();
			values.put("USER", user.asFirstnameLastname());
			values.put("USER_Firstname",user.getFirstname());
			values.put("USER_Lastname",user.getLastname());
			
			// data wystawienia
			values.put("DATA_WYSTAWIENIA", DateUtils.formatCommonDate((Date) doc.getFieldsManager().getValue("DATA_WYSTAWIENIA")).toString());
			
			// data generowania
			values.put("DATE", DateUtils.formatCommonDate(new java.util.Date()).toString());
			
			// ustawienie centrum kosztow ZRODLA
			setProjectsManager(fm, values);
			
			// ustawienie terminu platnosci sqlDateFormat
			values.put("TERMIN_PLATNOSCI", DateUtils.formatSqlDate((java.util.Date)fm.getKey("TERMIN_PLATNOSCI")));	
						
			// ustawienie zmiennej DESCRIPTION
			//String desc = fm.getValue("DESCRIPTION") != null ? (String) doc.getFieldsManager().getValue("DESCRIPTION") : "";
			
			// ustawienie zmiennej CPV
			setCPVNumbers(fm, values);
			
			// ustawienie zmiennej BUY_KIND
			String buyKind = fm.getEnumItem("BUY_KIND") != null ? doc.getFieldsManager().getEnumItem("BUY_KIND").getTitle() : "";
			values.put("BUY_KIND", buyKind);
			
			// Nazwa inwentarza, numer, Osoba odpowiedzialna
			setInwentarz(fm, values);
			
			// Set POIG Table - strasznie muli
			setPoig(fm, values);

			// Set Zadania Table - strasznie muli
			setZadania(fm, values);


			// Zestawienie akceptacji
			for (DocumentAcceptance docAcc : DocumentAcceptance.find(docId))
				values.put(docAcc.getAcceptanceCn(), DSUser.safeToFirstnameLastname(docAcc.getUsername()));
			if (values.containsKey("accounting"))
				values.put("verification", values.get("accounting"));
			
			// Ustawnieni zapotrzebowania i umowy, oraz nazwy towaru - strasznie muli
			setZapotrzebowaniContract(fm, values);

            if (!values.containsKey("fields"))
            {
                DocFacade df= Documents.document(docId);
                values.put("fields", df.getFields());
            }

		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

    private String kwotaSlownie(BigDecimal amount)
    {
        String result = "";
        try
        {
            result =  new String((TextUtils.numberToText(amount.setScale(0, RoundingMode.DOWN))).getBytes("ISO-8859-2"), "Cp1250") + " " + amount.remainder(BigDecimal.ONE).movePointRight(2).setScale(0, RoundingMode.HALF_UP) + "/100";
        }
        catch (UnsupportedEncodingException e)
        {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    private void setZadania(FieldsManager fm, Map<String, Object> values)
	{
		ArrayList<Zadanie> poigs = new ArrayList<Zadanie>();
		try
		{
            List<CostKindPoig> poig= (List<CostKindPoig>) values.get("POIG");
            if (poig == null || poig.isEmpty())
                return;
            List<Long> ids = (List<Long>) fm.getKey("ZADANIE");

			if (ids != null && !ids.isEmpty())
			{
				Iterator itr = ids.iterator();
				Map<String, Object> zadDic = null;
				while (itr.hasNext())
				{
					Object o = itr.next();
					zadDic = DwrDictionaryFacade.dictionarieObjects.get("ZADANIE").getValues(o.toString());
					String nrprojektu = zadDic.get("ZADANIE_NRPROJEKTU") != null ? (String) zadDic.get("ZADANIE_NRPROJEKTU") : "";
                    if (!nrProjektuZadanieInPoig(nrprojektu, poig))
                        continue;
					String zadanie = zadDic.get("ZADANIE_ZADANIE") != null ? (String) zadDic.get("ZADANIE_ZADANIE") : "";
					String tytulZadania = zadDic.get("ZADANIE_TYTULZAD") != null ? (String)zadDic.get("ZADANIE_TYTULZAD") : "" ;
					poigs.add(new Zadanie(nrprojektu, zadanie, tytulZadania));
				}
			}
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
		finally
		{
			values.put("ZADANIE", poigs);	
		}
		
	}

    private boolean nrProjektuZadanieInPoig(String nrprojektu, List<CostKindPoig> values)
    {
        boolean result = false;
        if (nrprojektu == null || nrprojektu.isEmpty())
            return result;
        for (CostKindPoig costPoig : values)
        {
            if (costPoig.getNrIfpan().equalsIgnoreCase(nrprojektu))
            {
                result = true;
                break;
            }
        }
        return result;
    }

    private void setPoig(FieldsManager fm, Map<String, Object> values)
	{
		ArrayList<CostKindPoig> poigs = new ArrayList<CostKindPoig>();
		try
		{		
			List<Long> ids = (List<Long>) fm.getKey("POIG");
			Iterator itr = ids.iterator();
			Map<String, Object> poigDic = null;
			while (itr.hasNext())
			{
				Object o = itr.next();
                Map<String, Object> fieldsValues = Maps.newHashMap();
                fieldsValues.put("document_id_for_ref", fm.getDocumentId());
				poigDic = DwrDictionaryFacade.dictionarieObjects.get("POIG").getValues(o.toString(), fieldsValues);
                String toTempalte = poigDic.get("POIG_TO_TEMPLATE") != null ? ((EnumValues)poigDic.get("POIG_TO_TEMPLATE")).getSelectedId() : "0";
                if ("0".equals(toTempalte))
                    continue;
                String nrIfpan = poigDic.get("POIG_CN") != null ? (String) poigDic.get("POIG_CN") : "";
				String nrProjektu = poigDic.get("POIG_NRPROJEKT") != null ? (String)poigDic.get("POIG_NRPROJEKT") : "" ;
				Date dataUmowy = poigDic.get("POIG_UMOWA") != null ?  (Date) poigDic.get("POIG_UMOWA") : null;
				String nazwaProjektu = poigDic.get("POIG_PROJEKT") != null ? (String) poigDic.get("POIG_PROJEKT") : "" ;
				String nazwaKategori = poigDic.get("POIG_TITLE") != null ? (String) poigDic.get("POIG_TITLE") : "" ;
				String nazwaPodkategorii = poigDic.get("POIG_PODKATEGORIA") != null ? (String) poigDic.get("POIG_PODKATEGORIA") : "" ;
				poigs.add(new CostKindPoig(nrIfpan, nrProjektu,  DateUtils.formatCommonDate(dataUmowy), nazwaProjektu, nazwaKategori, nazwaPodkategorii));
                String stawkaVat = poigDic.get("POIG_KOD_PODATKU") != null ? ((EnumValues)poigDic.get("POIG_KOD_PODATKU")).getSelectedId() : "0";
                String pozycjaId = poigDic.get("POIG_POZYCJAID") != null ? ((EnumValues)poigDic.get("POIG_POZYCJAID")).getSelectedId() : "";
                if (!pozycjaId.isEmpty())
                {
                    CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(Long.parseLong(pozycjaId));
                    BigDecimal kwota = mpk.getRealAmount();
                    values.put("KWOTA", kwota.setScale(2,RoundingMode.HALF_UP));
                    BigDecimal vat = calculateVat(kwota, stawkaVat);
                    values.put("VAT", vat);
                    BigDecimal efrr = kwota.subtract(kwota.multiply(new BigDecimal(0.15).setScale(2, BigDecimal.ROUND_HALF_UP)).setScale(2, BigDecimal.ROUND_HALF_UP));
                    values.put("EFRR", efrr.setScale(2,RoundingMode.HALF_UP));
                    BigDecimal krajwklad = kwota.multiply(new BigDecimal(0.15).setScale(2, BigDecimal.ROUND_HALF_UP)).setScale(2, BigDecimal.ROUND_HALF_UP);
                    values.put("KRAJWKLAD", krajwklad.setScale(2,RoundingMode.HALF_UP));

                    String bruttoSlownie = kwotaSlownie(kwota);
                    values.put("BRUTTO_SLOWNIE", bruttoSlownie);

                }
			}
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
		finally
		{
			values.put("POIG", poigs);
		}
	}

    private BigDecimal calculateVat(BigDecimal kwota, String stawkaVat)
    {
        BigDecimal result = BigDecimal.ZERO.setScale(2);

        if (stawkaVat != null && !stawkaVat.isEmpty() && !stawkaVat.equals("0"))
        {
            BigDecimal stawka = new BigDecimal(stawkaVat).divide(new BigDecimal(100));
            result = kwota.multiply(stawka).setScale(2, RoundingMode.HALF_UP);
        }

        return result;
    }

    private void setInwentarz(FieldsManager fm, Map<String, Object> values)
	{
		ArrayList<Inwentarz> inwentarze = new ArrayList<Inwentarz>();
		try
		{	
			List<Long> ids = (List<Long>) fm.getKey("DSG_CENTRUM_KOSZTOW_FAKTURY");
			Iterator itr = ids.iterator();
			while (itr.hasNext())
			{
				Object o = itr.next();
				String majatekId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(o.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_ACCEPTINGCENTRUMID")).getSelectedOptions().get(0);
				if (!majatekId.equals("0") && !majatekId.equals("") && !majatekId.equals("9"))
				{
					String inwentarz = (String) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(o.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_INWENTARZ");
					String title = DataBaseEnumField.getEnumItemForTable("dsg_ifpan_rodzaj_majatku_trwalego", Integer.parseInt(majatekId)).getTitle();
					DocumentAcceptance docAcc = DocumentAcceptance.find(fm.getDocumentId(), null, "akceptacja_majatku", Long.parseLong(majatekId)).get(0);
						inwentarze.add(new Inwentarz(title, inwentarz, DSUser.findByUsername(docAcc.getUsername()).asFirstnameLastname()));
				}
			}
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
		finally
		{
			if (inwentarze.size() == 0)
				inwentarze.add(new Inwentarz("Brak", "Brak", "Brak"));
			values.put("INWENTARZ", inwentarze);
		}
	}
	
	private void setZapotrzebowaniContract(FieldsManager fm, Map<String, Object> values)
	{
		String casuNumbers = "";
		String contractNrs = "";
		String contractDates = "";
		String descriptions = "";
		try
		{
			List<Long> ids = new LinkedList<Long>();
			if (fm.getKey("ZAPOTRZEBOWANIE") != null)
				ids = (List<Long>) fm.getKey("ZAPOTRZEBOWANIE");
			for (Long id : ids)
			{
                boolean oldDockind = Document.find(id, false).getDocumentKind().getCn().equals("zapotrzeb_ifpan");
                Document zapoDoc = Document.find(id, false);
                FieldsManager zapoFm = zapoDoc.getFieldsManager();
                if (oldDockind)
                {
                    if (casuNumbers.length() > 0)
                        casuNumbers+=", ";

                    casuNumbers+=zapoFm.getKey("CASENUMBER");
                    if (descriptions.length() > 0)
                    {
                        descriptions+=", ";
                    }

                    descriptions+=zapoFm.getKey("DESCRIPTION");

                    String umowaId = (String)zapoFm.getKey("CONTRACT");
                    if (umowaId != null && Long.valueOf(umowaId) != -1)
                    {
                        Document umowa = Document.find(Long.valueOf(umowaId));
                        if (contractNrs.length() > 0)
                            contractNrs+=", ";
                        contractNrs+=umowa.getFieldsManager().getKey("CONTRACT_NR");
                        if (contractDates.length() > 0)
                            contractDates+=", ";
                        contractDates+=umowa.getFieldsManager().getKey("CONTRACT_DATE").toString();

                    }
                }
                else
                {
                    List<Map<String, Object>> towary = (List<Map<String, Object>>) zapoFm.getValue("TOWAR");
                    for (Map<String, Object> towar : towary)
                    {
                        String desc = (String) towar.get("TOWAR_ITEM_DESCRIPTION");
                        if (descriptions.length() > 0)
                        {
                            descriptions+=", ";
                        }
                        descriptions+=desc;
                    }
                }
			}
		}
		catch (EdmException e)
		{
			log.warn(e.getMessage(), e);
		}
		finally
		{
			values.put("CASENUMBERS", casuNumbers);
			values.put("caseNumber", casuNumbers);
			values.put("CONTRACTNRS", contractNrs);
			values.put("CONTRACT_DATES", contractDates);
			values.put("DESCRIPTIONS", descriptions);
		}
	}

	public void specialSetDictionaryValues(Map<String, ?> dockindFields)
	{
		log.info("Special dictionary values: {}", dockindFields);
		if (dockindFields.keySet().contains("CONTRACTOR_DICTIONARY"))
		{
			Map<String, FieldData> m = (Map<String, FieldData>) dockindFields.get("CONTRACTOR_DICTIONARY");
			m.put("DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			m.put("ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}

	public void addSpecialInsertDictionaryValues(String dictionaryName,Map<String, FieldData> values) {
		log.info("Before Insert - Special dictionary: '{}' values: {}",
				dictionaryName, values);
		if (dictionaryName.contains("CONTRACTOR")) {
			Person p = new Person();
			p.createBasePerson(dictionaryName, values);
			try {
				p.create();
			} catch (EdmException e) {
				log.error("", e);
			}
			PersonDictionary.prepareFieldNamesForSenderPerson(p.getId(),dictionaryName, values);
		}
	}

	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		log.info("Before Search - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.contains("CONTRACTOR"))
		{
			values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}

        if (dictionaryName.contains("POIG"))
        {
            if (values.get("id") != null && !values.get("id").equals(""))
                return;
            values.put(dictionaryName + "_BASE", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.INTEGER, 1));
            if (values.get("POIG_POZYCJAID") != null && !values.get("POIG_POZYCJAID").equals(""))
            {
                values.remove("POIG_POZYCJAID");
            }
            if (values.get("POIG_KOD_PODATKU") != null && !values.get("POIG_KOD_PODATKU").equals(""))
            {
                values.remove("POIG_KOD_PODATKU");
            }
            if (values.get("POIG_TO_TEMPLATE") != null && !values.get("POIG_TO_TEMPLATE").equals(""))
            {
                values.remove("POIG_TO_TEMPLATE");
            }
        }

		if (dictionaryName.contains("CPVNR"))
		{
		    if (values.get("id") != null && !values.get("id").equals(""))
			    return;
			values.put(dictionaryName + "_BASE", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.INTEGER, 1));
		}
	}

	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			FieldsManager fm = dockind.getFieldsManager(id);
			
			// Ustawienie statusy dokumentu
			params.setStatus(fm.getValue("STATUS") != null ? (String) fm.getValue("STATUS") : null);
			
			// Ustawienie list projektow z ktorych pokyrwana jest faktura
			setProjectsNumberList(fm, params);
			
			// Ustawienie numeru faktury jako numery dokumentu
			params.setDocumentNumber(fm.getValue("NUMER_FAKTURY") != null ? (String) fm.getValue("NUMER_FAKTURY") : null);
			
			// Ustawienie Nazwy kontraktora
			if (fm.getKey("CONTRACTOR") != null)
			{
				String organizator = (String) DwrDictionaryFacade.dictionarieObjects.get("CONTRACTOR").getValues(fm.getKey("CONTRACTOR").toString()).get("CONTRACTOR_ORGANIZATION");
				params.setAttribute(organizator != null ? organizator : "", 1);
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return params;
	}

	private void setContractor(FieldsManager fm, Map<String, Object> values)
	{
		try
		{
			Long personId = (Long) fm.getKey("CONTRACTOR");
			Person oragnization = Person.find(personId.longValue());
			values.put("CONTRACTOR", oragnization);
		}
		catch (EdmException e)
		{
			log.warn(e.getMessage(), e);
		}
	}

	private void setProjectsManager(FieldsManager fm, Map<String, Object> values)
	{
		List <IfpanCentrumKosztow> centrums = new ArrayList<IfpanCentrumKosztow>();
		String projectsNumerList = "";
		try
		{
			List<Long> ids = (List<Long>) fm.getKey("DSG_CENTRUM_KOSZTOW_FAKTURY");
			Iterator itr = ids.iterator();
			Map <String, Object> dicValues = null;
			while (itr.hasNext())
			{
				IfpanCentrumKosztow centrum = new IfpanCentrumKosztow();
				dicValues = DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(itr.next().toString());
				String pojId = ((EnumValues)dicValues.get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMID")).getSelectedOptions().get(0);
				String nrIfpan = Project.find(Long.valueOf(pojId)).getNrIFPAN();
				
				if (!projectsNumerList.contains(nrIfpan))
				{
					if (projectsNumerList.length() > 0)
						projectsNumerList += ", ";
					projectsNumerList += nrIfpan;
				}
				
				centrum.setProjekt(nrIfpan);
				String inwentarz = (String)dicValues.get("DSG_CENTRUM_KOSZTOW_FAKTURY_INWENTARZ");
				centrum.setInwentarz(inwentarz);
				String kategoria = ((EnumValues)dicValues.get("DSG_CENTRUM_KOSZTOW_FAKTURY_ACCOUNTNUMBER")).getSelectedValue();
				centrum.setKategoria(kategoria);
				String ksiegowaKategoria = ((EnumValues)dicValues.get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMCODE")).getSelectedValue();
				centrum.setKsiegowaKategoria(ksiegowaKategoria);
				BigDecimal kwota = new BigDecimal((Double)dicValues.get("DSG_CENTRUM_KOSZTOW_FAKTURY_AMOUNT")).setScale(2,RoundingMode.HALF_UP);
				centrum.setKwota(kwota);
				String majatek = ((EnumValues)dicValues.get("DSG_CENTRUM_KOSZTOW_FAKTURY_ACCEPTINGCENTRUMID")).getSelectedValue();
				centrum.setMajatek(majatek);
				DSUser user = DSUser.findByUsername(Project.find(Long.valueOf(pojId)).getProjectManager());
				if (user != null)
				{
					centrum.setAkcept(user.asFirstnameLastname());
				}
				centrums.add(centrum);
			}
		}
		catch (EdmException e)
		{
			log.warn(e.getMessage(), e);
		}
		finally
		{
			values.put("ZRODLA", centrums);
			values.put("PROJECTS", projectsNumerList);
		}
	}

	private void setCPVNumbers(FieldsManager fm, Map<String, Object> values)
	{
		String cpvCns = "";
		try
		{
			List<Long> ids = (List<Long>) fm.getKey("CPVNR");
			Iterator itr = ids.iterator();
			while (itr.hasNext())
			{
				String cpvCn = (String) DwrDictionaryFacade.dictionarieObjects.get("CPVNR").getValues(itr.next().toString()).get("CPVNR_CN");
				cpvCns += cpvCn;
				if (itr.hasNext())
					cpvCns += ", ";
			}
			
		}
		catch (Exception e)
		{
			log.warn("Lapiemy to - " + e.getMessage(), e);
		}
		finally
		{
			if (cpvCns.length() == 0)
				cpvCns = "Brak";
			values.put("CPV", cpvCns);
		}
	}
	
	private void setProjectsNumberList(FieldsManager fm, TaskListParams params)
	{
		try
		{
			String projectsNumerList = "";
			List<Long> ids = (List<Long>) fm.getKey("DSG_CENTRUM_KOSZTOW_FAKTURY");
			if (ids != null) {
				Iterator itr = ids.iterator();
				while (itr.hasNext()) {
					String projectId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(itr.next().toString())
							.get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMID")).getSelectedOptions().get(0);
					if (projectId != null && !projectId.equals("")) {
						projectsNumerList = Project.find(Long.valueOf(projectId)).getNrIFPAN();
						if (itr.hasNext()) {
							projectsNumerList += "...";
							break;
						}
					}
				}
			}
			params.setAttribute(projectsNumerList, 0);
		}
		catch (EdmException e)
		{
			log.warn(e.getMessage(), e);
		}
	}
	
	private boolean assigneToMajatekTrwaly(OfficeDocument doc, Assignable assignable, OpenExecution openExecution) throws EdmException
	{
		boolean assigned = false;
		log.info("DokumentKind: {}, DocumentId: {}, OpenExecutionVariable: majatekId, Wartosc: {}", doc.getDocumentKind().getCn(), doc.getId(), openExecution.getVariable("majatekId"));
		String majatekId = openExecution.getVariable("majatekId").toString();
		String acceptCn = DataBaseEnumField.getEnumItemForTable("dsg_ifpan_rodzaj_majatku_trwalego", Integer.parseInt(majatekId)).getCn();
		for (AcceptanceCondition accept : AcceptanceCondition.find(acceptCn))
		{
			if (accept.getUsername() != null)
			{
				assignable.addCandidateUser(accept.getUsername());
				assigned = true;
				log.info("DokumentKind: {}, DocumentId: {}, Assigne to {}", doc.getDocumentKind().getCn(), doc.getId(), accept.getUsername());
			}
			if (accept.getDivisionGuid() != null)
			{
				assignable.addCandidateGroup(accept.getDivisionGuid());
				assigned = true;
				log.info("DokumentKind: {}, DocumentId: {}, Assigne to {}", doc.getDocumentKind().getCn(), doc.getId(), accept.getDivisionGuid());
			}
		} 
		return assigned;
	}
	
	private boolean assigneToRestDirs(OfficeDocument doc, Assignable assignable, OpenExecution openExecution) throws EdmException
	{
		boolean assigned = false;
		log.info("DokumentKind: {}, DocumentId: {}, OpenExecutionVariable: majatekId, Wartosc: {}", doc.getDocumentKind().getCn(), doc.getId(), openExecution.getVariable("majatekId"));

		String acceptCn = doc.getFieldsManager().getEnumItem("AKCEPTACJA_WYDATKOW").getCn();
		for (AcceptanceCondition accept : AcceptanceCondition.find(acceptCn))
		{
			if (accept.getUsername() != null)
			{
				assignable.addCandidateUser(accept.getUsername());
				assigned = true;
				log.info("DokumentKind: {}, DocumentId: {}, Assigne to {}", doc.getDocumentKind().getCn(), doc.getId(), accept.getUsername());
			}
			if (accept.getDivisionGuid() != null)
			{
				assignable.addCandidateGroup(accept.getDivisionGuid());
				assigned = true;
				log.info("DokumentKind: {}, DocumentId: {}, Assigne to {}", doc.getDocumentKind().getCn(), doc.getId(), accept.getDivisionGuid());
			}
		}
		return assigned;
	}
	
	private void setNrInwentarz(Long di, String nrInwentarzowy)
	{
		Statement stat = null;
		try
		{
			stat = DSApi.context().createStatement();
			int a = stat.executeUpdate("UPDATE DSG_CENTRUM_KOSZTOW_FAKTURY SET INWENTARZ = '" + nrInwentarzowy + "' where ID = " + di);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			DSApi.context().closeStatement(stat);
		}
	}

	private void checkSumCosts(Map<String, ?> fieldValues) throws EdmException
	{
		BigDecimal brutto = (BigDecimal) fieldValues.get("KWOTA_BRUTTO");
        BigDecimal netto = (BigDecimal) fieldValues.get("KWOTA_NETTO");
		BigDecimal sumCentrum = new BigDecimal(0);
		List<Long> dictionaryIds = (List<Long>) fieldValues.get("DSG_CENTRUM_KOSZTOW_FAKTURY");
		BigDecimal oneAmount = null;
		for (Long di : dictionaryIds)
		{
			oneAmount = new BigDecimal((Double) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(di.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_AMOUNT"));
			sumCentrum = sumCentrum.add(oneAmount);
		}
        if (netto.abs().compareTo(sumCentrum.abs()) > 0 || brutto.abs().compareTo(sumCentrum.abs()) < 0) {
           throw new EdmException("Suma kwot pozycji budżetowych musi być równa lub większa od kwoty netto i nie większa niż kwota brutto faktury.");
        }
		
	}


	private void setCostCenters(Map<String, FieldData> values) throws EdmException
	{
		CentrumKosztowDlaFaktury mpkInstance = CentrumKosztowDlaFaktury.getInstance();
		CentrumKosztowDlaFaktury mpk;
		
//		DSApi.openAdmin();
		
		// utworzenie listy id wybranych zapotrzebowa� z formatki
		List<Long> zapotrzIdValues = DwrUtils.extractIdFromDictonaryField(values, "ZAPOTRZEBOWANIE");
		
		// utworzenie listy id wybranych pozycji zapotrzebowa� z formatki
		FieldData zapotrzItemField = values.get("DWR_DSG_CENTRUM_KOSZTOW_FAKTURY");
		List<Long> zapotrzMpkIdsValues = DwrUtils.extractIdFromDictonaryField(values, "DSG_CENTRUM_KOSZTOW_FAKTURY");
		
		//zbi�r id zapotrzebowa� z mpk'�w na formatce
		Set<Long> zapotrzFromMpk = new HashSet<Long>();
		for (Long id : zapotrzMpkIdsValues) {
			mpk = mpkInstance.find(id);
			if (mpk.getAnalyticsId() != null && !mpk.getAnalyticsId().toString().equals("")) {
				zapotrzFromMpk.add(Long.valueOf(mpk.getAnalyticsId().toString()));
			}
		}
		
		List<Long> createdMpk = fillMpkTable(zapotrzIdValues, zapotrzMpkIdsValues, zapotrzFromMpk, mpkInstance);
		List<Long> mpkForShow = new ArrayList<Long>();
		
		StringBuilder idList = new StringBuilder();
		for (Long id : createdMpk) {
			if (!idList.toString().equals("")) {
				idList.append(",");
			}
			idList.append(id.toString());
			mpkForShow.add(id);
		}

		for (Long id : zapotrzMpkIdsValues) {
			mpk = mpkInstance.find(id);
			if (mpk.getAnalyticsId() == null || (mpk.getAnalyticsId() != null && zapotrzIdValues.contains(Long.valueOf(mpk.getAnalyticsId().toString())))) {
				if (!idList.toString().equals("")) {
					idList.append(",");
				}
				idList.append(id.toString());
				mpkForShow.add(id);
			}
		}
		
		updateMpks(zapotrzItemField, idList);
		if (!mpkForShow.containsAll(zapotrzMpkIdsValues) || !zapotrzMpkIdsValues.containsAll(mpkForShow)) {		
			values.put("DWR_DSG_CENTRUM_KOSZTOW_FAKTURY", new FieldData(Field.Type.STRING, idList));
		}
		
//		DSApi.close();
		
		
		//--------------------------------
		/*Map<String, FieldData> dvalues = values.get("DWR_ZAPOTRZEBOWANIE").getDictionaryData();
		String idsss = "";
		int i = 1;
		for (String o : dvalues.keySet())
		{
			if (o.contains("_ID_") && !dvalues.get(o).getData().toString().equals(""))
			{	
				Long idZap = new Long(dvalues.get(o).getData().toString());
				Statement stat = null;
				ResultSet result = null;
				try
				{
					DSApi.openAdmin();
					stat = DSApi.context().createStatement();
					result = stat.executeQuery("SELECT FIELD_VAL FROM dsg_ifpan_faktura_multiple_value where (DOCUMENT_ID = " + idZap + ") AND (FIELD_CN ='DSG_CENTRUM_KOSZTOW_FAKTURY')");
					
					//CentrumKosztowDlaFaktury mpkInstance = CentrumKosztowDlaFaktury.getInstance();
					while (result.next())
					{
						Long id = result.getLong(1);
						CentrumKosztowDlaFaktury baseMpk = mpkInstance.find(id);
						CentrumKosztowDlaFaktury newMpk = isAlreadyAdd(values, id.intValue());
						if (newMpk == null)
						{
							newMpk = createMpk(baseMpk);
						}
						if (i == 1)
							idsss += newMpk.getId().toString();
						else
							idsss = idsss + "," + newMpk.getId().toString();
						++i;
					}
				}
				catch (Exception ie)
				{
					log.error(ie.getMessage(), ie);
				}
				finally
				{
					DSApi.context().closeStatement(stat);
					DSApi.close();
				}
			}
		}
		values.put("DWR_DSG_CENTRUM_KOSZTOW_FAKTURY", new FieldData(Field.Type.STRING, idsss));
		*/
	}

	private void updateMpks(FieldData zapotrzItemField, StringBuilder idList) throws EdmException
	{
		String[] ids = idList.toString().split(",");
		Map<String, FieldData> dvalues = zapotrzItemField.getDictionaryData();
		for (String o : dvalues.keySet())
		{
			if (o.contains("DSG_CENTRUM_KOSZTOW_FAKTURY_ID_") && dvalues.get(o).getData()!= null && !dvalues.get(o).getData().toString().equals(""))
			{
				int index = Integer.parseInt(o.split("_ID_")[1]);
				Long mpkId = new Long(dvalues.get(o).getData().toString());
				if (Arrays.asList(ids).contains(mpkId.toString()))
				{
					CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(mpkId);
					if (mpk != null)
					{
						updateMpk(dvalues, index, mpk);
					}
				}
			}
		}

	}

	private void updateMpk(Map<String, FieldData> dvalues, int index, CentrumKosztowDlaFaktury mpk) throws EdmException
	{
		DSApi.context().begin();
		try
		{
			Object centrumId = dvalues.get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMID_" + index).getData();
			if (centrumId != null && !centrumId.equals(""))
				mpk.setCentrumId(Integer.parseInt(centrumId.toString()));
		}
		catch (Exception e)
		{
			log.warn("LAPIEMY :" + e.getMessage());
		}
		try
		{
			Object amount = dvalues.get("DSG_CENTRUM_KOSZTOW_FAKTURY_AMOUNT_" + index).getData();
			if (amount != null)
				mpk.setRealAmount(new BigDecimal(amount.toString()).setScale(2, RoundingMode.HALF_UP));
		}
		catch (Exception e)
		{
			log.warn("LAPIEMY :" + e.getMessage());
		}
		try
		{
			Object accepCentrumId = dvalues.get("DSG_CENTRUM_KOSZTOW_FAKTURY_ACCEPTINGCENTRUMID_" + index).getData();
			if (accepCentrumId != null && !accepCentrumId.equals(""))
				mpk.setAcceptingCentrumId(Integer.parseInt(accepCentrumId.toString()));
		}
		catch (Exception e)
		{
			log.warn("LAPIEMY :" + e.getMessage());
		}
		try
		{
			Object centrumCode = dvalues.get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMCODE_" + index).getData();
			if (centrumCode != null && !centrumCode.equals(""))
				mpk.setCentrumCode(centrumCode.toString());
		}
		catch (Exception e)
		{
			log.warn("LAPIEMY :" + e.getMessage());
		}
		try
		{
			Object accountNumber = dvalues.get("DSG_CENTRUM_KOSZTOW_FAKTURY_ACCOUNTNUMBER_" + index).getData();
			if (accountNumber != null && !accountNumber.equals(""))
				mpk.setAccountNumber(accountNumber.toString());
		}
		catch (Exception e)
		{
			log.warn("LAPIEMY :" + e.getMessage());
		}
		try
		{
			Object inwentarz = dvalues.get("DSG_CENTRUM_KOSZTOW_FAKTURY_INWENTARZ_" + index).getData();
			if (inwentarz != null && !inwentarz.equals("")) {
				updateInwentarzNo(mpk, inwentarz);
			}
		}
		catch (Exception e)
		{
			log.warn("LAPIEMY :" + e.getMessage());
		}
		DSApi.context().commit();
	}

	private void updateInwentarzNo(CentrumKosztowDlaFaktury mpk, Object inwentarz) throws EdmException, EdmSQLException {
		PreparedStatement ps = null;
		try {
			ps = DSApi.context().prepareStatement("update DSG_CENTRUM_KOSZTOW_FAKTURY set inwentarz=? where id=?");

			ps.setString(1, inwentarz.toString());
			ps.setLong(2, mpk.getId());

			if (ps.executeUpdate() != 1) {
				log.error("B��d poczas update'u nr inwentarzowego, mpk id:" + mpk.getId());
			}

		} catch (SQLException e) {
			throw new EdmSQLException(e);
		} finally {
			DSApi.context().closeStatement(ps);
		}
	}
	
	private List<Long> fillMpkTable(List<Long> zapotrzIdValues, List<Long> zapotrzItemIdsValues, Set<Long> zapotrzFromMpk, CentrumKosztowDlaFaktury mpkInstance) throws EdmException {
		List<Long> createdMpk = new ArrayList<Long>();
		
		for (Long zapotrzId : zapotrzIdValues) {
			if (zapotrzFromMpk.contains(zapotrzId)) {
				continue;
			}
			
			List<Long> foundMpk = new ArrayList<Long>();
			Statement stat = null;
			ResultSet result = null;
			try {
				stat = DSApi.context().createStatement();
                StringBuilder sql = new StringBuilder("SELECT FIELD_VAL FROM dsg_ifpan_faktura_multiple_value where (DOCUMENT_ID = ")
                        .append(zapotrzId).append(" AND (FIELD_CN ='DSG_CENTRUM_KOSZTOW_FAKTURY'))")
                        .append(" UNION ")
                        .append("SELECT FIELD_VAL FROM dsg_ifpan_zapotrz2_multiple_value where (DOCUMENT_ID = ")
                        .append(zapotrzId).append(" AND (FIELD_CN ='DSG_CENTRUM_KOSZTOW_FAKTURY'))");

				result = stat.executeQuery(sql.toString());


				while (result.next()) {
					Long id = result.getLong(1);
					foundMpk.add(id);
				}

				for (Long id : foundMpk) {
					CentrumKosztowDlaFaktury source = mpkInstance.find(id);
					CentrumKosztowDlaFaktury newMpk = new CentrumKosztowDlaFaktury();
					newMpk.copyValueFrom(source);
					// id zapotrzebowania od ktorego pochodzi bazowy mpk
					newMpk.setAnalyticsId(Integer.valueOf(zapotrzId.toString()));
					// id mpka bazowego - potrzebne przy zmianie blokady w koszt, po akcpetacji kosztu (przez dpir)
					newMpk.setClassTypeId(source.getId().intValue());
					Persister.create(newMpk);
					createdMpk.add(newMpk.getId());
				}

			} catch (Exception e) {
				log.error(e.getMessage(), e);
			} finally {
				DSApi.context().closeStatement(stat);
			}
		}
		return createdMpk;
	}
	
	private void checkRequiredFields(Map<String, FieldData> contractor, StringBuilder procedureMsg) {
		FieldData temp;
		boolean isUpdatingMsg = false;
		for (String field : contractor.keySet()) {
		 	temp = contractor.get(field);
		 	if (temp != null) {
		 		 if (temp.getData() == null && !isUpdatingMsg) {
		  			procedureMsg.append("Prosz� uzupe�ni� obowiazkowe pola istniej�cego wpisu przez u�ycie s�ownika. ");
		  			isUpdatingMsg = true;
		  	    }
		 	}
		}
	}
	
	private void updateContractorFields(Map<String, FieldData> values, Map<String, FieldData> contractor, FieldData contractorFields, String dwrPrefix,
			StringBuilder procedureMsg, Map<String, Object> externalContractor, Person newContractor) {
		FieldData temp;
		for (String field : externalContractor.keySet()) {
		 	temp = contractor.get(dwrPrefix + field.toUpperCase());
		 	if (temp != null) {
		 	    temp.setData(externalContractor.get(field));
		 	    contractor.put(dwrPrefix + field.toUpperCase(), temp);
		 	}
		}
		temp = contractor.get("id");
		temp.setData(newContractor.getId());
		contractor.put("id", temp);
		
		FieldData selectedCountry = contractor.get(dwrPrefix+"COUNTRY");
		selectedCountry.setData(contractor.get(dwrPrefix+"COUNTRY"));
		contractor.put(dwrPrefix + "COUNTRY", selectedCountry);
 	   
		contractorFields.setDictionaryData(contractor);
		values.put("DWR_CONTRACTOR",contractorFields);
	}
	
	private CentrumKosztowDlaFaktury createMpk(CentrumKosztowDlaFaktury baseMpk) throws EdmException
	{
		CentrumKosztowDlaFaktury newMpk;
		newMpk = new CentrumKosztowDlaFaktury();
		// ustawienie baseParentId
		newMpk.setClassTypeId(baseMpk.getId().intValue());
		newMpk.setCentrumId(baseMpk.getCentrumId());
		newMpk.setRealAmount(baseMpk.getRealAmount());
		newMpk.setAcceptingCentrumId(baseMpk.getRealAcceptingCentrumId());
		newMpk.setCentrumCode(baseMpk.getCentrumCode());
		newMpk.setAccountNumber(baseMpk.getAccountNumber());
		Persister.create(newMpk);
		return newMpk;
	}

	
/*	private CentrumKosztowDlaFaktury isAlreadyAdd(Map<String, FieldData> values, Integer baseMpkId) throws EdmException
	{
		Map<String, FieldData> dvalues = values.get("DWR_DSG_CENTRUM_KOSZTOW_FAKTURY").getDictionaryData();
		for (String o : dvalues.keySet())
		{
			if (o.contains("_ID_") && !dvalues.get(o).getData().toString().equals(""))
			{
				int index = Integer.parseInt(o.split("_ID_")[1]);
				Long mpkId = new Long(dvalues.get(o).getData().toString());

				CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(mpkId);
				if (mpk != null && mpk.getClassTypeId() != null && mpk.getClassTypeId().equals(baseMpkId))
				{
					updateMpk(dvalues, index, mpk);
					return mpk;
				}
			}
		}
		return null;
	}
	*/
	private void setBruttoAsNettoPlusVat(Map<String, FieldData> values)
	{
		BigDecimal netto = values.get("DWR_KWOTA_NETTO").getMoneyData();
		if (values.get("DWR_VAT").getData() != null)
		{
			BigDecimal brutto = values.get("DWR_VAT").getMoneyData();
			//BigDecimal kurs = ((BigDecimal)values.get("DWR_KURS").getData()).setScale(4, RoundingMode.HALF_UP);
			//brutto = brutto.multiply(kurs).setScale(2, RoundingMode.HALF_UP);
			brutto = brutto.add(netto).setScale(2, RoundingMode.HALF_UP);
			values.get("DWR_KWOTA_BRUTTO").setMoneyData(brutto.setScale(2, RoundingMode.HALF_UP));
		}
	}



	private void setNetto(Map<String, FieldData> values)
	{
		BigDecimal kwotaWWalucie = values.get("DWR_KWOTA_W_WALUCIE").getMoneyData();
		BigDecimal kurs = (BigDecimal)values.get("DWR_KURS").getData();
		BigDecimal netto = kwotaWWalucie.multiply(kurs).setScale(2, RoundingMode.HALF_UP);
		values.get("DWR_KWOTA_NETTO").setMoneyData(netto.setScale(2, RoundingMode.HALF_UP));
	}

	private void setBrutto(Map<String, FieldData> values)
	{
		BigDecimal sumCS = BigDecimal.ZERO;
		Map<String, FieldData> dvalues = values.get("DWR_DSG_CENTRUM_KOSZTOW_FAKTURY").getDictionaryData();
		for (String o : dvalues.keySet())
		{
			if (o.contains("AMOUNT") && dvalues.get(o).getMoneyData() != null)
				sumCS = sumCS.add(dvalues.get(o).getMoneyData());
		}
		if (!sumCS.equals(BigDecimal.ZERO))
			values.get("DWR_KWOTA_BRUTTO").setMoneyData(sumCS);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException {
		if (MAJATEK.equals(acceptationCn)) {
			List<Long> ids = new ArrayList<Long>();
			try {
				PreparedStatement ps;
				ps = DSApi.context().prepareStatement(
						"select FIELD_VAL from dsg_ifpan_faktura_multiple_value where DOCUMENT_ID=? and FIELD_CN=?");
				ps.setLong(1, doc.getId());
				ps.setString(2, "DSG_CENTRUM_KOSZTOW_FAKTURY");
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					ids.add(rs.getLong(1));
				}
				rs.close();
				ps.close();
			} catch (SQLException e) {
				log.error(e.getMessage());
			}

			List<CentrumKosztowDlaFaktury> cfk = (List<CentrumKosztowDlaFaktury>) DSApi.context().session()
					.createCriteria(CentrumKosztowDlaFaktury.class)
					.add(Restrictions.in("id", ids))
					.list();
			List<DocumentAcceptance> da = (List<DocumentAcceptance>) DSApi.context().session()
					.createCriteria(DocumentAcceptance.class)
					.add(Restrictions.eq("documentId", doc.getId()))
					.add(Restrictions.eq("acceptanceCn", "akceptacja_majatku"))
					.list();
			
			for (CentrumKosztowDlaFaktury c : cfk) {
				boolean isPresent = false;
				Integer aci = c.getAcceptingCentrumId();
				for (DocumentAcceptance d : da) {
					if (d.getObjectId().equals(aci.longValue())) {
						isPresent = true;
					}
				}
				if (!isPresent) {
					String majatekCN = DataBaseEnumField.getEnumItemForTable("dsg_ifpan_rodzaj_majatku_trwalego", aci).getCn();
					List<String> toAccept = new LinkedList<String>();
					for (AcceptanceCondition accept : AcceptanceCondition.find(majatekCN)) {
						if (accept.getUsername() != null && !toAccept.contains(accept.getUsername())) {
							toAccept.add(accept.getUsername());
						} 
						
						if (accept.getDivisionGuid() != null && !toAccept.contains(accept.getDivisionGuid())) {
							for (DSUser user : DSDivision.find(accept.getDivisionGuid()).getUsers()) {
								toAccept.add(user.getName());
							}
						}	
					}
					for (String username : toAccept) {
						DocumentAcceptance.createAndSave(doc, username, acceptationCn, aci.longValue());
					}
				}
			}
			
		}
        else if (acceptationCn.equals("dpzie"))
        {
            checkSumCpv(doc);

        }
	}

    private void checkSumCpv(Document doc) throws EdmException {
        FieldsManager fm = doc.getFieldsManager();
        BigDecimal netto = (BigDecimal) fm.getKey("KWOTA_NETTO");
        BigDecimal cpvSum = BigDecimal.ZERO.setScale(2);
        List<Map<String, Object>> cpvs = (List<Map<String, Object>>) fm.getValue("CPVNR");
        if (cpvs != null && !cpvs.isEmpty())
        {

            for (Map<String, Object> cpv : cpvs)
            {
                if (cpv.get("CPVNR_KWOTA_NETTO") != null)
                {
                    BigDecimal cpvNetto = (BigDecimal) cpv.get("CPVNR_KWOTA_NETTO");
                    cpvSum = cpvSum.add(cpvNetto);
                }
            }

        }
        if (netto.compareTo(cpvSum) != 0)
            throw new EdmException("Suma koszt�w z kod�w CPV musi by� r�wna kwocie netto w faktury");
    }
    
    public void onLoadData(FieldsManager fm) throws EdmException {
		LogicUtils.setPopupButtonsOnPersonDictionaryByPermission(fm, CONTRACTOR_FIELD_NAME);			
	}
}
