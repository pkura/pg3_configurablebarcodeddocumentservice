package pl.compan.docusafe.parametrization.ifpan;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.PersonDictionary;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.LogicUtils;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

public class UmowaZamowienieLogic extends AbstractIfpanDocumentLogic
{
	private static UmowaZamowienieLogic instance;
	protected static Logger log = LoggerFactory.getLogger(UmowaZamowienieLogic.class);
	private static final String CONTRACTOR_FIELD_NAME = "CONTRACTOR";
	
	/**
	 * Lista akcetacji dodatkowych
	 */
	public final static String CORRECTION = "correction";
	public final static String ACCEPTATION = "acceptation";
	
	public String costCenterId;

	public static UmowaZamowienieLogic getInstance()
	{
		if (instance == null)
			instance = new UmowaZamowienieLogic();
		return instance;
	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn)
	{
		log.info("DokumentKind: {}, DokumentId: {}', acceptationCn: {}, enumCn: {}", doc.getDocumentKind().getCn(), doc.getId(), accpetionCn, enumCn);
		
		boolean assigned = false;
		 
		// przekazanie do popoprawy autora - za�atwiamy to ju� w jpdl: correction, user, author
		if (accpetionCn.equals(CORRECTION))
			assigned = assigneeToCorrectionByAuthor(assignable, doc);	
		
		// przekazanie infomracji dla kierownika projektu uzytego w umowie
		else if (accpetionCn.equals(ACCEPTATION))
			assigned = assigneeToAcceptationCost(openExecution, doc);
			
		return assigned;
	}

	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		log.info("DokumentKind: {}, DocumentId: {}, Values from JavaScript: {}", fm.getDocumentKind().getCn(), fm.getDocumentId(), values);

		DocumentKind kind = fm.getDocumentKind();

		try
		{
			if (values.get("DWR_"+CONTRACTOR_FIELD_NAME)!= null && values.get("DWR_"+CONTRACTOR_FIELD_NAME).getDictionaryData() != null) {
				setContractorFlag(values, CONTRACTOR_FIELD_NAME);
				createPersonFromChoosenErpId(values, CONTRACTOR_FIELD_NAME);
			}
			
			// pobranie id zrodel finansowania zapotrzebowania
			setCentraKosztow(values);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}

		return null;
	}

	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException {
		if (fieldValues.get(CONTRACTOR_FIELD_NAME) != null) {
			tryCreateSenderPersonOnSaveDoc(kind, documentId, fieldValues, CONTRACTOR_FIELD_NAME);
		}
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn() + "_DOCUMENT_READ", ObjectPermission.GROUP, "Umowa - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Umowa zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn() + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Umowa - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Umowa zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn() + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Umowa - usuwanie"));
		this.setUpPermission(document, perms);
	}

	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		
		try
		{
			FieldsManager fm = dockind.getFieldsManager(id);

			// ustawienie zrodel finansowania na liste zadan(jezeli wiecej niz jeden, to wyswietlany jest pierwszy...)
			setZrodlaFinansowania(params, fm);
			
			// ustawienie Statusu
			params.setStatus((String) fm.getValue("STATUS"));
			
			// nr umowy jako nr dokumentu
			params.setDocumentNumber(fm.getValue("CONTRACT_NR") != null ? (String) fm.getValue("CONTRACT_NR") : null);
			
			// kontrahent
			setContracotr(params, fm);

		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
		return params;
	}

	

	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		try
		{
			Document doc = Document.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			
			// Dostawca
			Long personId = (Long) fm.getKey("CONTRACTOR");
			Person person = Person.find(personId.longValue());
			values.put("CONTRACTOR", person);
			
			// zmianna date - data ganerwoania dokumentu
			values.put("DATE", DateUtils.formatCommonDate(new java.util.Date()));
			
			// slownie wartosc, ale chyba nie dziala
			values.put("SLOWVALUE", TextUtils.numberToText(new BigDecimal(fm.getValue("CONTRACT_VAL").toString().replace(" ", ""))));

		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Umowy");
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(document.getCtime()));
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(document.getCtime()));
		document.setFolder(folder);

		document.setTitle(String.valueOf(fm.getValue("CONTRACT_NR")));
		document.setDescription(String.valueOf(fm.getValue("CONTRACT_NR")));
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- UmowaZamowienie : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

			java.util.Date cTime = document.getCtime();
			((OutOfficeDocument)document).setDocumentDate(cTime);
			
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	public void specialSetDictionaryValues(Map<String, ?> dockindFields)
	{
		log.info("Special dictionary values: {}", dockindFields);
		if (dockindFields.keySet().contains("CONTRACTOR_DICTIONARY"))
		{
			Map<String, FieldData> m = (Map<String, FieldData>) dockindFields.get("CONTRACTOR_DICTIONARY");
			m.put("DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			m.put("ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}

	public void addSpecialInsertDictionaryValues(String dictionaryName, Map<String, FieldData> values)
	{
		log.info("Before Insert - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.contains("CONTRACTOR") && !values.containsKey("DISCRIMINATOR"))
		{
			Person p = new Person();
			p.createBasePerson(dictionaryName, values);
			try {
				p.create();
			} catch (EdmException e) {
				log.error("", e);
			}
			PersonDictionary.prepareFieldNamesForSenderPerson(p.getId(),dictionaryName, values);
		}
	}

	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		log.info("Before Search - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.contains("CONTRACTOR"))
		{
			values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}
	
	private boolean assigneeToAcceptationCost(OpenExecution openExecution, OfficeDocument doc)
	{
		boolean assigned = false;
		try
		{
			String centrumId = openExecution.getVariable("costCenterId").toString();
			String userName = Project.find(Long.valueOf(centrumId)).getProjectManager();
			log.debug("Dla ID centrum: " + centrumId + " informacja do kierownika projektu w umowaie: " + userName);

			doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(doc, Collections.<String, Object> singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY, userName));
			openExecution.getActivity().getDefaultOutgoingTransition();

			assigned = true;
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return assigned;
	}

	private boolean assigneeToCorrectionByAuthor(Assignable assignable, OfficeDocument doc)
	{
		assignable.addCandidateUser(doc.getAuthor());
		log.debug("Assigne to correction by auhor, docID: {}", doc.getId());
		return true;
	}
	
	private void setCentraKosztow(Map<String, FieldData> values) throws EdmException
	{
		String idsss = "";
		Map<String, FieldData> zapotrzebowania = (Map<String, FieldData>) values.get("DWR_ZAPOTRZEBOWANIE").getData();

		for (String cn : zapotrzebowania.keySet())
		{
			if (cn.contains("ZAPOTRZEBOWANIE_ID") && !zapotrzebowania.get(cn).getStringData().equals(""))
			{
				Long idZap = new Long(zapotrzebowania.get(cn).getStringData());
				Statement stat = null;
				ResultSet result = null;
				try
				{
					DSApi.openAdmin();
					stat = DSApi.context().createStatement();
					result = stat.executeQuery("SELECT FIELD_VAL FROM dsg_ifpan_faktura_multiple_value where (DOCUMENT_ID = " + idZap + ") AND (FIELD_CN ='DSG_CENTRUM_KOSZTOW_FAKTURY')");
					log.info("SELECT FIELD_VAL FROM dsg_ifpan_faktura_multiple_value where (DOCUMENT_ID = " + idZap + ") AND (FIELD_CN ='DSG_CENTRUM_KOSZTOW_FAKTURY')");
					while (result.next())
					{
						Integer id = result.getInt(1);
						if (idsss.equals(""))
						{
							values.remove("DWR_DSG_CENTRUM_KOSZTOW_FAKTURY");
							idsss += id.toString();
						}
						else
						{
							idsss = idsss + "," + id.toString();
						}
						log.debug("ID slownikow zap: " + id);
					}
				}
				catch (Exception ie)
				{
					log.error(ie.getMessage(), ie);
				}
				finally
				{
					DSApi.context().closeStatement(stat);
					DSApi.close();
				}
			}
		}
		log.debug("ID centrow kosztowych z zapotrzebowan: " + idsss);
		values.put("DWR_DSG_CENTRUM_KOSZTOW_FAKTURY", new FieldData(Field.Type.STRING, idsss));
	}
	
	private void setContracotr(TaskListParams params, FieldsManager fm) throws EdmException
	{
		String kontrahent = "";
		if (fm.getKey("CONTRACTOR") != null)
		{
			String organizator = (String) DwrDictionaryFacade.dictionarieObjects.get("CONTRACTOR").getValues(fm.getKey("CONTRACTOR").toString()).get("CONTRACTOR_ORGANIZATION");
			kontrahent = organizator != null ? organizator : "";
		}
		params.setAttribute(kontrahent, 1);

	}

	private void setZrodlaFinansowania(TaskListParams params, FieldsManager fm) throws Exception
	{
		List<Long> ids = (List<Long>) fm.getKey("DSG_CENTRUM_KOSZTOW_FAKTURY");

		String projectsNumerList = "";
		if (ids != null)
		{
			Iterator itr = ids.iterator();
			while (itr.hasNext())
			{
				String pojId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(itr.next().toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMID")).getSelectedOptions().get(0);
				if (pojId != null && !pojId.equals(""))
				{
					projectsNumerList = Project.find(Long.valueOf(pojId)).getNrIFPAN();

					if (itr.hasNext())
					{
						projectsNumerList += "...";
						break;
					}
				}
			}
			params.setAttribute(projectsNumerList, 0);
		}
	}
	
	public void onLoadData(FieldsManager fm) throws EdmException {
		LogicUtils.setPopupButtonsOnPersonDictionaryByPermission(fm, CONTRACTOR_FIELD_NAME);			
	}
}
