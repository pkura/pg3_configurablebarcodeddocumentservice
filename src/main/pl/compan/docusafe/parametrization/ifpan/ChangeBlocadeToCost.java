package pl.compan.docusafe.parametrization.ifpan;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.record.projects.AccountingCostKind;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.core.record.projects.ProjectEntry;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class ChangeBlocadeToCost implements ExternalActivityBehaviour
{
	private static final Logger log = LoggerFactory.getLogger(AddProjectEntryListener.class);
	FieldsManager fm = null;
	
	public void execute(ActivityExecution execution) throws Exception
	{
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		setCosts(docId);
		execution.takeDefaultTransition();
	}
	
	private void setCosts(Long docId) throws DocumentNotFoundException, DocumentLockedException, AccessDeniedException, EdmException
	{
		String dicName = "DSG_CENTRUM_KOSZTOW_FAKTURY";
		List<Long> dictionaryIds = (List<Long>) Document.find(docId).getFieldsManager().getKey(dicName);

		for (Long dicId : dictionaryIds)
		{
			String projectId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get(dicName).getValues(dicId.toString()).get(dicName + "_CENTRUMID")).getSelectedOptions().get(0);
			BigDecimal costPln = new BigDecimal(DwrDictionaryFacade.dictionarieObjects.get(dicName).getValues(dicId.toString()).get(dicName + "_AMOUNT").toString());
			Project project = Project.find(new Long(projectId));
			ProjectEntry projectEntry = ProjectEntry.findByCostId(dicId);
			if (projectEntry == null)
			{
				projectEntry = new ProjectEntry();
				projectEntry.setDateEntry(new java.util.Date());
				projectEntry.setCostId(dicId);
				projectEntry.setCurrency("PLN");
				projectEntry.setRate(new BigDecimal(1.0000));
				projectEntry.setDescription("-");
			}
			projectEntry.setDocNr(docId.toString());
			projectEntry.setEntryType("COST");
			projectEntry.setGross(costPln);
			projectEntry.setMtime(new java.util.Date());
			if (!((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(dicId.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_ACCOUNTNUMBER")).getSelectedOptions().get(0).equals(""))
			{
				projectEntry.setCostKindId(new Integer(((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(dicId.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_ACCOUNTNUMBER")).getSelectedOptions().get(0)));
			}
			if (!((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(dicId.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMCODE")).getSelectedOptions().get(0).equals(""))
			{
				AccountingCostKind accountingCostKind = AccountingCostKind.find(new Integer(((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(dicId.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMCODE")).getSelectedOptions().get(0)));
				projectEntry.setAccountingCostKind(accountingCostKind);
			}
			projectEntry.setCostDate(new java.util.Date());
			project.addEntry(projectEntry);
		}
		
		List<ProjectEntry> projectEntries = ProjectEntry.findByDocNr(docId);

		for (ProjectEntry entry : projectEntries)
		{
			if (!dictionaryIds.contains(entry.getCostId()))
				entry.delete();
		}
	}
	
	public void signal(ActivityExecution execution, String signalName, Map<String, ?> parameters) throws Exception
	{

	}
}
