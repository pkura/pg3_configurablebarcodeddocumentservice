package pl.compan.docusafe.parametrization.ifpan;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

public class UmowaIfpanLogic extends AbstractIfpanDocumentLogic
{
	private static UmowaIfpanLogic instance;
	protected static Logger log = LoggerFactory.getLogger(UmowaIfpanLogic.class);
	private static final String CONTRACTOR_FIELD_NAME = "CONTRACTOR";
	
	private static final String ZNACZNIK = "ZNACZNIK";
	private static final String NR_SPRAWY = "NR_SPRAWY";
	private static final String ORDER_SIGN_TABLE = "dsg_ifpan_order_sign";
	

	public static UmowaIfpanLogic getInstance()
	{
		if (instance == null)
			instance = new UmowaIfpanLogic();
		return instance;
	}


	public void documentPermissions(Document document) throws EdmException
	{
	}


	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- UmowaZamowienie : START PROCESS !!! ---- {}", event);
		try
		{   
			/**
		     * Automatyczne nadanie numeru zamówienia (NZU) znacznik_zamowienia/id/rok
		     */
			StringBuilder sb = new StringBuilder();
			Long docId = document.getId();
			FieldsManager fm = document.getDocumentKind().getFieldsManager(docId);
			EnumItem dbEnumField = DataBaseEnumField.getEnumItemForTable(ORDER_SIGN_TABLE, fm.getEnumItem(ZNACZNIK).getId());
			sb.append(dbEnumField.getTitle()).append("/").append(docId)
			  .append("/").append(now("yyyy"));
			Map<String, Object> fieldValues = new HashMap<String, Object>();
			fieldValues.put(NR_SPRAWY, sb);
			document.getDocumentKind().setOnly(document.getId(), fieldValues);
			
			
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	
	public String now(String dateFormat) {
    	Calendar cal = Calendar.getInstance();
    	SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
    	return sdf.format(cal.getTime());
    	}
	
	
	
	
	

	
}
