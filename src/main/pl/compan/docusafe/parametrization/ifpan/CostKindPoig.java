package pl.compan.docusafe.parametrization.ifpan;

import java.util.Date;

public class CostKindPoig
{
	private String nrIfpan;
	private String nrProjektu;
	private String dataUmowy;
	private String nazwaProjektu;
	private String nazwaKategori;
	private String nazwaPodkategorii;

	public CostKindPoig(String nrIfpan, String nrProjektu, String dataUmowy, String nazwaProjektu, String nazwaKategori, String nazwaPodkategorii)
	{
		this.nrIfpan = nrIfpan;
		this.nrProjektu = nrProjektu;
		this.dataUmowy = dataUmowy;
		this.nazwaProjektu = nazwaProjektu;
		this.nazwaKategori = nazwaKategori;
		this.nazwaPodkategorii = nazwaPodkategorii;
	}
	public String getNrIfpan()
	{
		return nrIfpan;
	}
	public void setNrIfpan(String nrIfpan)
	{
		this.nrIfpan = nrIfpan;
	}
	public String getNrProjektu()
	{
		return nrProjektu;
	}
	public void setNrProjektu(String nrProjektu)
	{
		this.nrProjektu = nrProjektu;
	}
	public String getDataUmowy()
	{
		return dataUmowy;
	}
	public void setDataUmowy(String dataUmowy)
	{
		this.dataUmowy = dataUmowy;
	}
	public String getNazwaProjektu()
	{
		return nazwaProjektu;
	}
	public void setNazwaProjektu(String nazwaProjektu)
	{
		this.nazwaProjektu = nazwaProjektu;
	}
	public String getNazwaKategori()
	{
		return nazwaKategori;
	}
	public void setNazwaKategori(String nazwaKategori)
	{
		this.nazwaKategori = nazwaKategori;
	}
	public String getNazwaPodkategorii()
	{
		return nazwaPodkategorii;
	}
	public void setNazwaPodkategorii(String nazwaPodkategorii)
	{
		this.nazwaPodkategorii = nazwaPodkategorii;
	}
	
	
}
