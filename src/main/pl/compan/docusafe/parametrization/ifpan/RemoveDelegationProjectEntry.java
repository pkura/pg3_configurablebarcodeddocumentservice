package pl.compan.docusafe.parametrization.ifpan;

import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.record.projects.ProjectEntry;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class RemoveDelegationProjectEntry implements ExternalActivityBehaviour
{

	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		
		for (ProjectEntry entry : ProjectEntry.getProjectEntryByDocumentId(docId.toString(), null, false))
			if (entry.getDocNr() != null && entry.getDocNr().equals(docId.toString()))
				entry.delete();
		
		execution.takeDefaultTransition();
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{

	}

}
