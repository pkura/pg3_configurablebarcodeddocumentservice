package pl.compan.docusafe.parametrization.ifpan;

import java.math.BigDecimal;

public class Otrzymane
{
	private String waluta;
	private BigDecimal kurs;
	private BigDecimal wKraju = new BigDecimal(0).setScale(2);
	private BigDecimal wKrajuPln = new BigDecimal(0).setScale(2);
	private BigDecimal zaGranica = new BigDecimal(0).setScale(2);
	private BigDecimal zaGranicaPln = new BigDecimal(0).setScale(2);
	
	public String getWaluta()
	{
		return waluta;
	}
	public void setWaluta(String waluta)
	{
		this.waluta = waluta;
	}
	public BigDecimal getKurs()
	{
		return kurs!=null?kurs:BigDecimal.ONE;
	}
	public void setKurs(BigDecimal kurs)
	{
		this.kurs = kurs;
	}
	public BigDecimal getwKraju()
	{
		return wKraju;
	}
	public void setwKraju(BigDecimal wKraju)
	{
		this.wKraju = wKraju;
	}
	public BigDecimal getZaGranica()
	{
		return zaGranica;
	}
	public void setZaGranica(BigDecimal zaGranica)
	{
		this.zaGranica = zaGranica;
	}
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kurs == null) ? 0 : kurs.hashCode());
		result = prime * result + ((waluta == null) ? 0 : waluta.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Otrzymane other = (Otrzymane) obj;
		if (kurs == null)
		{
			if (other.kurs != null)
				return false;
		}
		else if (!kurs.equals(other.kurs))
			return false;
		if (waluta == null)
		{
			if (other.waluta != null)
				return false;
		}
		else if (!waluta.equals(other.waluta))
			return false;
		return true;
	}
	public BigDecimal getwKrajuPln()
	{
		return wKrajuPln;
	}
	public void setwKrajuPln(BigDecimal wKrajuPln)
	{
		this.wKrajuPln = wKrajuPln;
	}
	public BigDecimal getZaGranicaPln()
	{
		return zaGranicaPln;
	}
	public void setZaGranicaPln(BigDecimal zaGranicaPln)
	{
		this.zaGranicaPln = zaGranicaPln;
	}
}
