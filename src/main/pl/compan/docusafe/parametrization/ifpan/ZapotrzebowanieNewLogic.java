package pl.compan.docusafe.parametrization.ifpan;

import com.google.common.collect.Maps;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.dwr.*;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.field.DocumentPersonField;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.LogicUtils;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionType;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.*;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

public class ZapotrzebowanieNewLogic extends AbstractIfpanDocumentLogic
{
    private static final String CONTRACTOR_FIELD_NAME = "CONTRACTOR";
    private static final String SUG_CONTRACTOR_FIELD_NAME = "SUG_CONTRACTOR";
    private static final String SUGG_BRUTTO = "SUGG_BRUTTO";
    private static Logger log = LoggerFactory.getLogger(ZapotrzebowanieLogic.class);
    private static ZapotrzebowanieLogic instance;

    public final static String DIVORPROJ = "div_or_proj_acc";
    public final static String CORRECTION = "correction";
    public final static String ACCEPTATION = "acceptation";

    public static ZapotrzebowanieLogic getInstance()
    {
        if (instance == null)
            instance = new ZapotrzebowanieLogic();
        return instance;
    }

    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues,Long documentId)
    {
        Map<String, Object> result = Maps.newHashMap();
        BigDecimal count = values.get("TOWAR_COUNT").getMoneyData();
        BigDecimal price = values.get("TOWAR_PRICE").getMoneyData();

        if (count != null && price != null)
        {

                BigDecimal totalProce = count.multiply(price).setScale(2, 4);
                result.put("TOWAR_TOTALPRICE", totalProce);
        }
        return result;
    }


    @Override
    public List<String> getActionMessage(ActionType source, Document doc) {
        List<String> messages = new ArrayList<String>();
        try {



            OfficeDocument od = OfficeDocument.find(doc.getId());
            FieldsManager fm = od.getFieldsManager();
            fm.initialize();
            fm.initializeValues();
            String format = DateFormat.getDateInstance().format(new Date());
            String nrSprawy = "NZ/"+od.getOfficeNumber()+"/"+format.substring(0, 4);
            Map<String, Object> toReload = new HashMap<String, Object>();
            toReload.put("DEMAND_NUMBER2", nrSprawy);
            od.setDescription("Zapotrzebowanie (NOWE)");
            od.setTitle("Zapotrzebowanie (NOWE)");
            doc.getDocumentKind().setOnly(doc.getId(), toReload);
            switch (source) {
                case OFFICE_CREATE_DOCUMENT:
                    messages.add("Przyjęto pismo o numerze " + nrSprawy);
                    break;
                default:
                    break;
            }
        }catch(Exception e)
        {
            log.error(e.getMessage(), e);
        }
        return messages;
    }

    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();
        for(pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields()){
            if(f.getDefaultValue() != null){
                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
            }
        }

        DSUser user = DSApi.context().getDSUser();
        toReload.put("DS_USER", user.getId());
        log.info("toReload = {}", toReload);
        fm.reloadValues(toReload);
    }

    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn)
    {
        boolean assigned = false;
        try
        {
            log.info("DokumentKind: {}, DokumentId: {}', acceptationCn: {}, enumCn: {}", doc.getDocumentKind().getCn(), doc.getId(), accpetionCn, enumCn);

            if (accpetionCn.equals(DIVORPROJ))
                assigned = assigneToDivOrProj(assignable, openExecution);
            else if (accpetionCn.equals(CORRECTION))
            {
                assignable.addCandidateUser(doc.getAuthor());
                assigned = true;
            }
            // akceptacja drugiego dyrektora
            else if (accpetionCn.equals("T_DIR"))
                assigned = assigneToRestDirs(doc, assignable, openExecution);

        }
        catch (Exception ee)
        {
            log.error(ee.getMessage(), ee);
            return false;
        }
        return assigned;
    }

    public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
    {
        log.info("DokumentKind: {}, DocumentId: {}, Values from JavaScript: {}", fm.getDocumentKind().getCn(), fm.getDocumentId(), values);

        if (DwrUtils.isNotNull(values, "DWR_KWOTA_BRUTTO")
                && values.get("DWR_CONTRACTEVALUE") != null
                && values.get("DWR_KWOTA_BRUTTO").isSender())
        {
            BigDecimal grossPln = values.get("DWR_KWOTA_BRUTTO").getMoneyData();
            String ratioString = Docusafe.getAdditionProperty("kurs_euro") != null ? Docusafe.getAdditionProperty("kurs_euro") : "3.839";
            BigDecimal ratio = new BigDecimal(ratioString).setScale(4, RoundingMode.DOWN);
            BigDecimal euro = grossPln.divide(ratio, 2, RoundingMode.HALF_UP);
            values.get("DWR_CONTRACTEVALUE").setMoneyData(euro);
        }


        FieldData sugerowneKosztyBrutto = values.get(DwrUtils.dwr(SUGG_BRUTTO));
        if (sugerowneKosztyBrutto != null) {
            BigDecimal sugerowneKosztyBruttoValue = sumTowarPrice(values);
            sugerowneKosztyBrutto.setMoneyData(sugerowneKosztyBruttoValue);
        }

        if (values.get("DWR_"+CONTRACTOR_FIELD_NAME)!= null && values.get("DWR_"+CONTRACTOR_FIELD_NAME).getDictionaryData() != null) {
            setContractorFlag(values, CONTRACTOR_FIELD_NAME);
            createPersonFromChoosenErpId(values, CONTRACTOR_FIELD_NAME);
        }

        if (values.get("DWR_"+SUG_CONTRACTOR_FIELD_NAME)!= null && values.get("DWR_"+SUG_CONTRACTOR_FIELD_NAME).getDictionaryData() != null) {
            setContractorFlag(values, SUG_CONTRACTOR_FIELD_NAME);
            createPersonFromChoosenErpId(values, SUG_CONTRACTOR_FIELD_NAME);
        }

        return null;
    }

    public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
    {
        TaskListParams params = new TaskListParams();
        try
        {
            FieldsManager fm = dockind.getFieldsManager(id);

            // Ustawienie statusy dokumentu
            params.setStatus(fm.getValue("STATUS") != null ? (String) fm.getValue("STATUS") : null);

            // Ustawienie nuemru sprawy jako numeru dokumentu
            params.setDocumentNumber(fm.getValue("DEMAND_NUMBER2") != null ? (String) fm.getValue("DEMAND_NUMBER2") : null);

            // Ustawienie list projektow z ktorych pokyrwana jest faktura
            setProjectsNumberList(fm, params);

        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
        }
        return params;
    }

    @Override
    public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException {
        if (fieldValues.get(CONTRACTOR_FIELD_NAME) != null) {
            tryCreateSenderPersonOnSaveDoc(kind, documentId, fieldValues, CONTRACTOR_FIELD_NAME);
        }
        if (fieldValues.get(SUG_CONTRACTOR_FIELD_NAME) != null) {
            tryCreateSenderPersonOnSaveDoc(kind, documentId, fieldValues, SUG_CONTRACTOR_FIELD_NAME);
        }
    }

    public void documentPermissions(Document document) throws EdmException
    {
        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
        perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn() + "_DOCUMENT_READ", ObjectPermission.GROUP, "Zapotrzebowanie2 - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Zapotrzebowanie2 zalacznik - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn() + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Zapotrzebowanie2 - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Zapotrzebowanie2 zalacznik - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn() + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Zapotrzebowanie2 - usuwanie"));
        Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
        perms.addAll(documentPermissions);
        this.setUpPermission(document, perms);
    }

    public void specialSetDictionaryValues(Map<String, ?> dockindFields)
    {
        log.info("Special dictionary values: {}", dockindFields);
        if (dockindFields.keySet().contains("SUG_CONTRACTOR_DICTIONARY"))
        {
            Map<String, FieldData> m = (Map<String, FieldData>) dockindFields.get("SUG_CONTRACTOR_DICTIONARY");
            m.put("SUG_CONTRACTOR_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
            m.put("SUG_CONTRACTOR_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
        }
        else if (dockindFields.keySet().contains("CONTRACTOR_DICTIONARY"))
        {
            Map<String, FieldData> m = (Map<String, FieldData>) dockindFields.get("CONTRACTOR_DICTIONARY");
            m.put("CONTRACTOR_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
            m.put("CONTRACTOR_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
        }
    }

    public void addSpecialInsertDictionaryValues(String dictionaryName,	Map<String, FieldData> values) {
        log.info("Before Insert - Special dictionary: '{}' values: {}",
                dictionaryName, values);
        if (dictionaryName.contains("CONTRACTOR")) {
            Person p = new Person();
            p.createBasePerson(dictionaryName, values);
            try {
                p.create();
            } catch (EdmException e) {
                log.error("", e);
            }
            PersonDictionary.prepareFieldNamesForSenderPerson(p.getId(),dictionaryName, values);
        }
    }

    @Override
    public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
    {
        log.info("Before Search - Special dictionary: '{}' values: {}", dictionaryName, values);
        if (dictionaryName.contains("CONTRACTOR"))
        {
            values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
            values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
        }
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent("Zapotrzebowania");
        folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(document.getCtime()));
        folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(document.getCtime()));
        document.setFolder(folder);

        document.setTitle("Zapotrzebowanie (NOWE)");
        document.setDescription("Zapotrzebowanie (NOWE)");

        String zrodlaFin = "";


        LinkedList<LinkedHashMap> obj = (LinkedList<LinkedHashMap>) fm.getValue("DSG_CENTRUM_KOSZTOW_FAKTURY");
        for(LinkedHashMap mapa: obj)
        {
            EnumValues values = (EnumValues) mapa.get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMID");
            String selected = values.getSelectedValue();
            zrodlaFin += selected + ", ";
        }
        HashMap<String, Object> toReload = new HashMap<String, Object>();
        toReload.put("ZRODLA_FIN", zrodlaFin);
        document.getDocumentKind().setOnly(document.getId(), toReload);
    }

    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event)
    {
        log.info("--- ZapotrzebowanieLogic : START PROCESS !!! ---- {}", event);
        try
        {
            Map<String, Object> map = Maps.newHashMap();
            map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
            map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

            java.util.Date cTime = document.getCtime();
            ((OutOfficeDocument)document).setDocumentDate(cTime);

            java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

            DSUser author = DSApi.context().getDSUser();
            String user = author.getName();
            String fullName = author.getLastname() + " " + author.getFirstname();

            perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

            Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
            perms.addAll(documentPermissions);
            this.setUpPermission(document, perms);

            DSApi.context().watch(URN.create(document));

            Long journalId = Long.parseLong(Docusafe.getAdditionProperty("zapotrzebowanie_journal_id"));
            Date entryDate = new Date();

            Integer sequenceId = Journal.TX_newEntry2(journalId, document.getId(), entryDate);

            ((OutOfficeDocument) document).bindToJournal(journalId, sequenceId, entryDate);

        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
    {
        ProcessCoordinator ret = new ProcessCoordinator();
        ret.setUsername(document.getAuthor());
        return ret;
    }

    public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
    {
        try
        {
            Document doc = Document.find(docId);
            FieldsManager fm = doc.getFieldsManager();

            // ustawienie Konrahenta
            setContractor(fm, values);

            // ustawienie list porjektow
            setProjects(fm, values);
            putExtractedValues(values, fm);
            // uzytkownik generujący
            DSUser user = DSApi.context().getDSUser();
            values.put("USER_Firstname",user.getFirstname());
            values.put("USER_Lastname",user.getLastname());
            values.put("USER_Email",user.getEmail());
            String userDivisions = "";
            List<DSDivision> divisions = Arrays.asList(user.getDivisionsWithoutGroupPosition());
            Iterator<DSDivision> divITer = divisions.iterator();
             DSDivision  div = null;
           while(divITer.hasNext() && (div = divITer.next()) != null)
           {
               userDivisions += div.getName();
               if (divITer.hasNext())
                   userDivisions+= ", ";
           }


            values.put("USER_Divisions", userDivisions);
            values.put("DS_USER", fm.getEnumItem("DS_USER").getTitle());
            // data generowani
            values.put("GENERATED_DATE", DateUtils.formatCommonDate(new java.util.Date()).toString());

            //akceptacje
            for (DocumentAcceptance docAcc : DocumentAcceptance.find(docId))
                values.put(docAcc.getAcceptanceCn(), DSUser.safeToFirstnameLastname(docAcc.getUsername()));

        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
        }
    }

    public void putExtractedValues(Map<String, Object> values, FieldsManager fm) {
        FieldsManagerUtils.FieldValuesGetter getter = new FieldsManagerUtils.DateEnumValuesGetter(false);
        FieldsManagerUtils.FieldValuesGetter getterNullable = new FieldsManagerUtils.DateEnumValuesGetter(true);
        try {
            for (pl.compan.docusafe.core.dockinds.field.Field field : fm.getFields()) {
                try {
                    String fieldCn = field.getCn();
                    if (field instanceof DictionaryField && field.isMultiple()) {
                        List<Map<String, Object>> dictTemplateValues = FieldsManagerUtils.getMultiDictionaryExtractedValues(fm, fieldCn, getter);
                        values.put(fieldCn, dictTemplateValues);
                    } else if (field instanceof DocumentPersonField) {
                        Long personId = (Long) fm.getKey(field.getCn());
                        Person person = Person.find(personId.longValue());
                        Map<String, Object> templatePerson = toTemplate(person);
                        values.put(fieldCn, templatePerson);
                    } else if (field instanceof DataBaseEnumField) {
                        Object fieldValue = fm.getValue(fieldCn);
                        values.put(fieldCn, fieldValue);
                    } else {
                        Object fieldValue = fm.getValue(fieldCn);
                        Object extractedValue = getterNullable.getValue(fieldValue);
                        if (extractedValue != null)
                            values.put(fieldCn, extractedValue);
                        if (fieldValue instanceof List)
                            values.put(fieldCn, fieldValue);
                    }
                } catch (EdmException e) {
                    log.error(e.getMessage(), e);
                }
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }

    private List<Map<String, Object>> toTemplate(List<Object> list) {
        List<Map<String, Object>> template = new ArrayList<Map<String, Object>>();
        for (Object obj : list)
            template.add(Collections.singletonMap("VALUE", obj));
        return template;
    }

    private Map<String, Object> toTemplate(Person person) {
        Map<String, Object> template = new HashMap<String, Object>();
        template.put("ORGANIZATION", person.getOrganization());
        template.put("STREET", person.getStreet());
        template.put("ZIP", person.getZip());
        template.put("LOCATION", person.getLocation());
        try
        {
            String country = DataBaseEnumField.getEnumItemForTable("dsr_country", Integer.valueOf(person.getCountry())).getTitle();
            template.put("COUNTRY", country);
        }
        catch (Exception e)
        {
            log.error(e.getMessage());
            template.put("COUNTRY","---");
        }
        template.put("NIP", person.getNip());
        template.put("REGON", person.getRegon());
        return template;
    }

    private boolean assigneToDivOrProj(Assignable assignable, OpenExecution openExecution) throws NumberFormatException, EdmException
    {
        String centrumId = openExecution.getVariable("costCenterId").toString();
        String userName = Project.find(Long.valueOf(centrumId)).getProjectManager();
        assignable.addCandidateUser(userName);
        return true;
    }

    private void setProjectsNumberList(FieldsManager fm, TaskListParams params)
    {
        try
        {
            String projectsNumerList = "";
            List<Long> ids = (List<Long>) fm.getKey("DSG_CENTRUM_KOSZTOW_FAKTURY");
            Iterator itr = ids.iterator();
            while (itr.hasNext())
            {
                String projectId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(itr.next().toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMID")).getSelectedOptions().get(0);
                if (projectId != null && !projectId.equals(""))
                {
                    projectsNumerList = Project.find(Long.valueOf(projectId)).getNrIFPAN();
                    if (itr.hasNext())
                    {
                        projectsNumerList += "...";
                        break;
                    }
                }
            }
            params.setAttribute(projectsNumerList, 0);
        }
        catch (Exception e)
        {
            log.warn(e.getMessage(), e);
        }
    }

    private void setProjects(FieldsManager fm, Map<String, Object> values)
    {
        try
        {
            String projectsNumerList = "";
            String kategorieKosztów = "";
            List<Long> ids = (List<Long>) fm.getKey("DSG_CENTRUM_KOSZTOW_FAKTURY");
            Iterator itr = ids.iterator();
            while (itr.hasNext())
            {
                String pojId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(itr.next().toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMID")).getSelectedOptions().get(0);
                String nrIfpan = Project.find(Long.valueOf(pojId)).getNrIFPAN();
                if (!projectsNumerList.contains(nrIfpan))
                {
                    if (projectsNumerList.length() > 0)
                        projectsNumerList += ", ";
                    projectsNumerList += Project.find(Long.valueOf(pojId)).getNrIFPAN();
                }
            }
            values.put("PROJECTS", projectsNumerList);
        }
        catch (Exception e)
        {
            log.warn(e.getMessage(), e);
        }
    }

    private void setContractor(FieldsManager fm, Map<String, Object> values)
    {
        try
        {
            Long personId = (Long) fm.getKey("SUG_CONTRACTOR");
            Person oragnization = Person.find(personId.longValue());
            values.put("SUG_CONTRACTOR", oragnization);
        }
        catch (Exception e)
        {
            log.warn(e.getMessage(), e);
        }
    }

    public BigDecimal sumTowarPrice(Map<String, FieldData> values) {
        BigDecimal amountSum = new BigDecimal(0);
        FieldData dictionaryField = values.get("DWR_TOWAR");
        if (dictionaryField != null && dictionaryField.getType().equals(pl.compan.docusafe.core.dockinds.dwr.Field.Type.DICTIONARY)) {
            Map<String,FieldData> dictionary = dictionaryField.getDictionaryData();

            for (int i=1; true; i++) {
                String processedField = "TOWAR_COUNT_"+i;
                if (!dictionary.containsKey(processedField)) {
                    break;
                }

                String processedField2 = "TOWAR_PRICE_"+i;
                if (!dictionary.containsKey(processedField2)) {
                    break;
                }

                if (dictionary.get(processedField) != null
                        && dictionary.get(processedField).getType().equals(pl.compan.docusafe.core.dockinds.dwr.Field.Type.MONEY)
                        && dictionary.get(processedField).getMoneyData() != null
                        &&
                        dictionary.get(processedField2) != null
                        && dictionary.get(processedField2).getType().equals(pl.compan.docusafe.core.dockinds.dwr.Field.Type.MONEY)
                        && dictionary.get(processedField2).getMoneyData() != null)
                {
                    amountSum = amountSum.add(dictionary.get(processedField).getMoneyData().multiply(dictionary.get(processedField2).getMoneyData()));
                }
            }
        }

        return amountSum;
    }

    private boolean assigneToRestDirs(OfficeDocument doc, Assignable assignable, OpenExecution openExecution) throws EdmException
    {
        boolean assigned = false;

        String acceptCn = doc.getFieldsManager().getEnumItem("AKCEPTACJA_WYDATKOW").getCn();
        for (AcceptanceCondition accept : AcceptanceCondition.find(acceptCn))
        {
            if (accept.getUsername() != null)
            {
                assignable.addCandidateUser(accept.getUsername());
                assigned = true;
                log.info("DokumentKind: {}, DocumentId: {}, Assigne to {}", doc.getDocumentKind().getCn(), doc.getId(), accept.getUsername());
            }
            if (accept.getDivisionGuid() != null)
            {
                assignable.addCandidateGroup(accept.getDivisionGuid());
                assigned = true;
                log.info("DokumentKind: {}, DocumentId: {}, Assigne to {}", doc.getDocumentKind().getCn(), doc.getId(), accept.getDivisionGuid());
            }
        }
        return assigned;
    }
    
    public void onLoadData(FieldsManager fm) throws EdmException {
 		LogicUtils.setPopupButtonsOnPersonDictionaryByPermission(fm, CONTRACTOR_FIELD_NAME, SUG_CONTRACTOR_FIELD_NAME);			
 	}
}