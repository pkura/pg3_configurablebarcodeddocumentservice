package pl.compan.docusafe.parametrization.ifpan;

import java.math.BigDecimal;

public class DelegateCost
{
	private String typ;
	private String waluta;
	private String iloscKosztKurs;
	private BigDecimal kurs = new BigDecimal(0);
	private BigDecimal ilosc;
	private BigDecimal koszt;
	private BigDecimal kosztWaluta;
	private BigDecimal kosztPLN;
	private String platnik;
	private String akcept;
	private String opis = "";
	
	private BigDecimal diety = new BigDecimal(0);
	private BigDecimal noclegi = new BigDecimal(0);
	private BigDecimal kosztyPrzejazdu = new BigDecimal(0);
	private BigDecimal dietyDojazdowe = new BigDecimal(0);
	private BigDecimal wpisowe = new BigDecimal(0);
	private BigDecimal wizy = new BigDecimal(0);
	private BigDecimal inne = new BigDecimal(0);
	
	public String getTyp()
	{
		return typ;
	}

	public void setTyp(String typ)
	{
		this.typ = typ;
	}

	public String getWaluta()
	{
		return waluta;
	}

	public void setWaluta(String waluta)
	{
		this.waluta = waluta;
	}

	public BigDecimal getKurs()
	{
		return kurs;
	}

	public void setKurs(BigDecimal kurs)
	{
		this.kurs = kurs;
	}

	public BigDecimal getIlosc()
	{
		return ilosc;
	}

	public void setIlosc(BigDecimal ilosc)
	{
		this.ilosc = ilosc;
	}

	public BigDecimal getKoszt()
	{
		return koszt;
	}

	public void setKoszt(BigDecimal koszt)
	{
		this.koszt = koszt;
	}

	public BigDecimal getKosztWaluta()
	{
		return kosztWaluta;
	}

	public void setKosztWaluta(BigDecimal kosztWaluta)
	{
		this.kosztWaluta = kosztWaluta;
	}

	public BigDecimal getKosztPLN()
	{
		return kosztPLN;
	}

	public void setKosztPLN(BigDecimal kosztPLN)
	{
		this.kosztPLN = kosztPLN;
	}

	public String getIloscKosztKurs()
	{
		return iloscKosztKurs;
	}

	public void setIloscKosztKurs(String iloscKosztKurs)
	{
		this.iloscKosztKurs = iloscKosztKurs;
	}

	public String getPlatnik()
	{
		return platnik;
	}

	public void setPlatnik(String platnik)
	{
		this.platnik = platnik;
	}

	public String getAkcept()
	{
		return akcept;
	}

	public void setAkcept(String akcept)
	{
		this.akcept = akcept;
	}

	public BigDecimal getDiety()
	{
		return diety;
	}

	public void setDiety(BigDecimal diety)
	{
		this.diety = diety;
	}

	public BigDecimal getNoclegi()
	{
		return noclegi;
	}

	public void setNoclegi(BigDecimal noclegi)
	{
		this.noclegi = noclegi;
	}

	public BigDecimal getKosztyPrzejazdu()
	{
		return kosztyPrzejazdu;
	}

	public void setKosztyPrzejazdu(BigDecimal kosztyPrzejazdu)
	{
		this.kosztyPrzejazdu = kosztyPrzejazdu;
	}

	public BigDecimal getDietyDojazdowe()
	{
		return dietyDojazdowe;
	}

	public void setDietyDojazdowe(BigDecimal dietyDojazdowe)
	{
		this.dietyDojazdowe = dietyDojazdowe;
	}

	public BigDecimal getWpisowe()
	{
		return wpisowe;
	}

	public void setWpisowe(BigDecimal wpisowe)
	{
		this.wpisowe = wpisowe;
	}

	public BigDecimal getWizy()
	{
		return wizy;
	}

	public void setWizy(BigDecimal wizy)
	{
		this.wizy = wizy;
	}

	public BigDecimal getInne()
	{
		return inne;
	}

	public void setInne(BigDecimal inne)
	{
		this.inne = inne;
	}

	public String getOpis()
	{
		return opis;
	}

	public void setOpis(String opis)
	{
		this.opis = opis;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kurs == null) ? 0 : kurs.hashCode());
		result = prime * result + ((waluta == null) ? 0 : waluta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DelegateCost other = (DelegateCost) obj;
		if (kurs == null)
		{
			if (other.kurs != null)
				return false;
		}
		else if (!kurs.equals(other.kurs))
			return false;
		if (waluta == null)
		{
			if (other.waluta != null)
				return false;
		}
		else if (!waluta.equals(other.waluta))
			return false;
		return true;
	}

}
