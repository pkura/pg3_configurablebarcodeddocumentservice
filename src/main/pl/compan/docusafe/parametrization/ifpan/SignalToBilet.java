package pl.compan.docusafe.parametrization.ifpan;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.service.tasklist.RefreshTaskList;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SignalToBilet implements ExternalActivityBehaviour
{

	private final static Logger LOG = LoggerFactory.getLogger(SignalToBilet.class);

	@Override
	public void execute(ActivityExecution openExecution) throws Exception
	{
		boolean isOpened = true;
		try
		{
			if (!DSApi.isContextOpen())
			{
				isOpened = false;
				DSApi.openAdmin();
			}

			OfficeDocument doc = Jbpm4Utils.getDocument(openExecution);
			
			PreparedStatement ps = DSApi.context().prepareStatement("SELECT DOCUMENT_ID FROM dsg_ifpan_bilety where instr_wyjazdowa = ?");
			ps.setLong(1, doc.getId());
			ResultSet rs = ps.executeQuery();
			if (rs.next())
			{
				Long biletyId = rs.getLong("DOCUMENT_ID");
	
				ps = DSApi.context().prepareStatement("select ID_ from JBPM4_EXECUTION ex join JBPM4_VARIABLE va on ex.DBID_ = va.EXECUTION_ " +
					"where va.KEY_ = 'doc-id' and va.LONG_VALUE_ = ?");
				ps.setLong(1, biletyId);
				rs = ps.executeQuery();
				rs.next();
				String biletActivityId = rs.getString(1);
			
				Jbpm4Provider.getInstance().getExecutionService().signalExecutionById(biletActivityId);
			}
			openExecution.take("success");
			RefreshTaskList.addDocumentToRefresh(doc.getId());
			
		}
		catch (Exception e)
		{
			LOG.error(e.getMessage(), e);
		}
		finally
		{
			if (DSApi.isContextOpen() && !isOpened)
			{
				DSApi.close();
			}
		}
	}

	@Override
	public void signal(ActivityExecution openExecution, String arg1, Map<String, ?> arg2) throws Exception
	{
		openExecution.take("success");
		OfficeDocument doc = Jbpm4Utils.getDocument(openExecution);
		RefreshTaskList.addDocumentToRefresh(doc.getId());
		
	}
}