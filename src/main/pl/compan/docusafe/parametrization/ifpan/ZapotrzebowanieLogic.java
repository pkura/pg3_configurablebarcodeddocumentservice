package pl.compan.docusafe.parametrization.ifpan;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.dwr.*;
import pl.compan.docusafe.core.dockinds.field.AbstractDictionaryField;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.LogicUtils;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.imgwwaw.WniosekZakupowyLogic;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

public class ZapotrzebowanieLogic extends AbstractIfpanDocumentLogic
{
	private static final String CONTRACTOR_FIELD_NAME = "CONTRACTOR";
	private static final String SUG_CONTRACTOR_FIELD_NAME = "SUG_CONTRACTOR";
	private static Logger log = LoggerFactory.getLogger(ZapotrzebowanieLogic.class);
	private static ZapotrzebowanieLogic instance;

	public final static String DIVORPROJ = "div_or_proj_acc";
	public final static String DPIR_PERSON = "dpirPerson";
	public final static String CORRECTION = "correction";
	public final static String ACCEPTATION = "acceptation";
	
	public final static String FINANCE_SOURCE = "secCostCenterId";

	public static ZapotrzebowanieLogic getInstance()
	{
		if (instance == null)
			instance = new ZapotrzebowanieLogic();
		return instance;
	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn)
	{
		boolean assigned = false;
		try
		{
			log.info("DokumentKind: {}, DokumentId: {}', acceptationCn: {}, enumCn: {}", doc.getDocumentKind().getCn(), doc.getId(), accpetionCn, enumCn);

			if (accpetionCn.equals(DIVORPROJ))
				assigned = assigneToDivOrProj(assignable, openExecution);
			else if (accpetionCn.equals(CORRECTION))
			{
				assignable.addCandidateUser(doc.getAuthor());
				assigned = true;
			}
			else if(accpetionCn.equals(DPIR_PERSON)){
				String zrodloId = (String) openExecution.getVariable("secCostCenterId");
				Project project = Project.find(Long.valueOf(zrodloId));
				if(project != null && project.getResponsiblePerson() != null){
					assignable.addCandidateUser(project.getResponsiblePerson().getName());
					assigned = true;
				} else {
					assignable.addCandidateUser("aradomyska");
					assigned = true;
				}
				
			}
		}
		catch (Exception ee)
		{
			log.error(ee.getMessage(), ee);
			return false;
		}
		return assigned;
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		log.info("DokumentKind: {}, DocumentId: {}, Values from JavaScript: {}", fm.getDocumentKind().getCn(), fm.getDocumentId(), values);
 
		if (DwrUtils.isNotNull(values, "DWR_KWOTA_BRUTTO")
                && values.get("DWR_CONTRACTEVALUE") != null
                && values.get("DWR_KWOTA_BRUTTO").isSender())
		{
			BigDecimal grossPln = values.get("DWR_KWOTA_BRUTTO").getMoneyData();
			String ratioString = Docusafe.getAdditionProperty("kurs_euro") != null ? Docusafe.getAdditionProperty("kurs_euro") : "3.839";
			BigDecimal ratio = new BigDecimal(ratioString).setScale(4, RoundingMode.DOWN);
			BigDecimal euro = grossPln.divide(ratio, 2, RoundingMode.HALF_UP);
			values.get("DWR_CONTRACTEVALUE").setMoneyData(euro);
		}
		
		if (values.get("DWR_"+CONTRACTOR_FIELD_NAME)!= null && values.get("DWR_"+CONTRACTOR_FIELD_NAME).getDictionaryData() != null) {
			setContractorFlag(values, CONTRACTOR_FIELD_NAME);
			createPersonFromChoosenErpId(values, CONTRACTOR_FIELD_NAME);
		}
		
		if (values.get("DWR_"+SUG_CONTRACTOR_FIELD_NAME)!= null && values.get("DWR_"+SUG_CONTRACTOR_FIELD_NAME).getDictionaryData() != null) {
			//setContractorFlag(values, SUG_CONTRACTOR_FIELD_NAME);
			createPersonFromChoosenErpId(values, SUG_CONTRACTOR_FIELD_NAME);
		}
		
		return null;
	}
	    
	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException {
		if (fieldValues.get(CONTRACTOR_FIELD_NAME) != null) {
			tryCreateSenderPersonOnSaveDoc(kind, documentId, fieldValues, CONTRACTOR_FIELD_NAME);
		}
		if (fieldValues.get(SUG_CONTRACTOR_FIELD_NAME) != null) {
			tryCreateSenderPersonOnSaveDoc(kind, documentId, fieldValues, SUG_CONTRACTOR_FIELD_NAME);
		}
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn() + "_DOCUMENT_READ", ObjectPermission.GROUP, "Zapotrzebowanie2 - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Zapotrzebowanie2 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn() + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Zapotrzebowanie2 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Zapotrzebowanie2 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn() + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Zapotrzebowanie2 - usuwanie"));
		Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
		perms.addAll(documentPermissions);
		this.setUpPermission(document, perms);
	}

	public void specialSetDictionaryValues(Map<String, ?> dockindFields)
	{
		log.info("Special dictionary values: {}", dockindFields);
		if (dockindFields.keySet().contains("SUG_CONTRACTOR_DICTIONARY"))
		{
			Map<String, FieldData> m = (Map<String, FieldData>) dockindFields.get("SUG_CONTRACTOR_DICTIONARY");
			m.put("SUG_CONTRACTOR_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			m.put("SUG_CONTRACTOR_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
		else if (dockindFields.keySet().contains("CONTRACTOR_DICTIONARY"))
		{
			Map<String, FieldData> m = (Map<String, FieldData>) dockindFields.get("CONTRACTOR_DICTIONARY");
			m.put("CONTRACTOR_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			m.put("CONTRACTOR_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}

	public void addSpecialInsertDictionaryValues(String dictionaryName,	Map<String, FieldData> values) {
		log.info("Before Insert - Special dictionary: '{}' values: {}",
				dictionaryName, values);
		if (dictionaryName.contains("CONTRACTOR")) {
			Person p = new Person();
			p.createBasePerson(dictionaryName, values);
			try {
				p.create();
			} catch (EdmException e) {
				log.error("", e);
			}
			PersonDictionary.prepareFieldNamesForSenderPerson(p.getId(),dictionaryName, values);
			}
	}

	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		log.info("Before Search - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.contains("CONTRACTOR"))
		{
			values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Zapotrzebowania");
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(document.getCtime()));
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(document.getCtime()));
		document.setFolder(folder);

		document.setTitle(String.valueOf(fm.getValue("DESCRIPTION")));
		document.setDescription(String.valueOf(fm.getValue("DESCRIPTION")));
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- ZapotrzebowanieLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			
			java.util.Date cTime = document.getCtime();
			((OutOfficeDocument)document).setDocumentDate(cTime);
        	
			java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

			DSUser author = DSApi.context().getDSUser();
			String user = author.getName();
			String fullName = author.getLastname() + " " + author.getFirstname();

			perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

			Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
			perms.addAll(documentPermissions);
			this.setUpPermission(document, perms);
			
			DSApi.context().watch(URN.create(document));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}
	
	public void onLoadData(FieldsManager fm) throws EdmException {
		LogicUtils.setPopupButtonsOnPersonDictionaryByPermission(fm, CONTRACTOR_FIELD_NAME, SUG_CONTRACTOR_FIELD_NAME);			
	}

	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		try
		{
			Document doc = Document.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			
			// ustawienie Konrahenta
			setContractor(fm, values);
			
			// ustawienie list porjektow
			setProjects(fm, values);
			
			// uzytkownik generujący
			values.put("USER", DSApi.context().getDSUser());
			
			// data generowani
			values.put("DATE", DateUtils.formatCommonDate(new java.util.Date()).toString());
			
			//akceptacje
			for (DocumentAcceptance docAcc : DocumentAcceptance.find(docId))
				values.put(docAcc.getAcceptanceCn(), DSUser.safeToFirstnameLastname(docAcc.getUsername()));

            if (!values.containsKey("fields"))
            {
                DocFacade df= Documents.document(docId);
                values.put("fields", df.getFields());
            }
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			FieldsManager fm = dockind.getFieldsManager(id);
			
			// Ustawienie list projektow z ktorych pokyrwana jest faktura
			setProjectsNumberList(fm, params);
			
			// Ustawienie statusy dokumentu
			params.setStatus(fm.getValue("STATUS") != null ? (String) fm.getValue("STATUS") : null);
			
			// Ustawienie nuemru sprawy jako numeru dokumentu
			params.setDocumentNumber(fm.getValue("CASENUMBER") != null ? (String) fm.getValue("CASENUMBER") : null);
			
			// ustawienie Nazwy kontrahenta
			if (fm.getKey("CONTRACTOR") != null)
			{
				String organizator = (String) DwrDictionaryFacade.dictionarieObjects.get("CONTRACTOR").getValues(fm.getKey("CONTRACTOR").toString()).get("CONTRACTOR_ORGANIZATION");
				params.setAttribute(organizator != null ? organizator : "", 1);
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return params;
	}

	private boolean assigneToDivOrProj(Assignable assignable, OpenExecution openExecution) throws NumberFormatException, EdmException
	{
		String centrumId = openExecution.getVariable("costCenterId").toString();
		String userName = Project.find(Long.valueOf(centrumId)).getProjectManager();
		assignable.addCandidateUser(userName);
		return true;
	}

	private void setProjectsNumberList(FieldsManager fm, TaskListParams params)
	{
		try
		{
			String projectsNumerList = "";
			List<Long> ids = (List<Long>) fm.getKey("DSG_CENTRUM_KOSZTOW_FAKTURY");
			Iterator itr = ids.iterator();
			while (itr.hasNext())
			{
				String projectId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(itr.next().toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMID")).getSelectedOptions().get(0);
				if (projectId != null && !projectId.equals(""))
				{
					projectsNumerList = Project.find(Long.valueOf(projectId)).getNrIFPAN();
					if (itr.hasNext())
					{
						projectsNumerList += "...";
						break;
					}
				}
			}
			params.setAttribute(projectsNumerList, 0);
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
	}

	private void setProjects(FieldsManager fm, Map<String, Object> values)
	{
		try
		{
			String projectsNumerList = "";
			List<Long> ids = (List<Long>) fm.getKey("DSG_CENTRUM_KOSZTOW_FAKTURY");
			Iterator itr = ids.iterator();
			while (itr.hasNext())
			{
				String pojId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(itr.next().toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMID")).getSelectedOptions().get(0);
				String nrIfpan = Project.find(Long.valueOf(pojId)).getNrIFPAN();
				if (!projectsNumerList.contains(nrIfpan))
				{
					if (projectsNumerList.length() > 0)
						projectsNumerList += ", ";
					projectsNumerList += Project.find(Long.valueOf(pojId)).getNrIFPAN();
				}
			}
			values.put("PROJECTS", projectsNumerList);
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
	}

	private void setContractor(FieldsManager fm, Map<String, Object> values)
	{
		try
		{
			Long personId = (Long) fm.getKey("CONTRACTOR");
			Person oragnization = Person.find(personId.longValue());
			values.put("CONTRACTOR", oragnization);
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
	}
}