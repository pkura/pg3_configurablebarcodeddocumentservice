package pl.compan.docusafe.parametrization.ifpan;

import java.math.BigDecimal;
import java.util.ArrayList;

public class CurrencyForRaport
{
	public CurrencyForRaport()
	{
		super();
	}
	
	private String currency;
	private BigDecimal rate;
	private BigDecimal total;
	private String totalInText;
	private String description;
	private ArrayList<DelegateCost> costs;

	public String getCurrency()
	{
		return currency;
	}
	public void setCurrency(String currency)
	{
		this.currency = currency;
	}
	public BigDecimal getTotal()
	{
		return total;
	}
	public void setTotal(BigDecimal total)
	{
		this.total = total;
	}

	public CurrencyForRaport(String currency, BigDecimal rate, BigDecimal total, String totalInText, String description, ArrayList<DelegateCost> costs)
	{
		super();
		this.currency = currency;
		this.rate = rate;
		this.total = total;
		this.totalInText = totalInText;
		this.description = description;
		this.costs = costs;
	}
	public String getTotalInText()
	{
		return totalInText;
	}

	public void setTotalInText(String totalInText)
	{
		this.totalInText = totalInText;
	}
	public ArrayList<DelegateCost> getCosts()
	{
		return costs;
	}
	public void setCosts(ArrayList<DelegateCost> costs)
	{
		this.costs = costs;
	}
	public BigDecimal getRate()
	{
		return rate;
	}
	public void setRate(BigDecimal rate)
	{
		this.rate = rate;
	}
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currency == null) ? 0 : currency.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CurrencyForRaport other = (CurrencyForRaport) obj;
		if (currency == null)
		{
			if (other.currency != null)
				return false;
		}
		else if (!currency.equals(other.currency))
			return false;
		return true;
	}
	
	

}
