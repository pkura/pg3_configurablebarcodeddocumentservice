package pl.compan.docusafe.parametrization.ifpan;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class AuthorGuidDecisionHandler implements DecisionHandler 
{
	private final static Logger log = LoggerFactory.getLogger(AuthorGuidDecisionHandler.class);
	
	private String dzGUID;
	private String dpizeGUID;

	public String decide(OpenExecution exeution) 
	{
		Long docId = Long.valueOf(exeution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        
		try {
			OfficeDocument doc = OfficeDocument.find(docId);
			DSUser user = DSUser.findByUsername(doc.getAuthor());
		    if (user.getDivisionsWithoutGroup().length>0)
		    {
		    	for (DSDivision division : user.getDivisionsWithoutGroup())
		    	{
		    		if (division.getGuid().equals(dzGUID))
		    			return "DZ";
		    		if (division.getGuid().equals(dpizeGUID))
		    			return "DPIZE";
		    	}
		    }
		    else
		    	return "DPIZE";
		} 
		catch (DocumentNotFoundException e) 
		{
			log.error(e.getMessage());
			return "error";
		} 
		catch (EdmException e) 
		{
			log.error(e.getMessage());
			return "error";
		}
		return "DPIZE";
	}

	public String getDzGUID() {
		return dzGUID;
	}

	public void setDzGUID(String dzGUID) {
		this.dzGUID = dzGUID;
	}

	public String getDpizeGUID() {
		return dpizeGUID;
	}

	public void setDpizeGUID(String dpizeGUID) {
		this.dpizeGUID = dpizeGUID;
	}

	
}
