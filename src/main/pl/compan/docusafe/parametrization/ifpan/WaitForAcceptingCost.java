package pl.compan.docusafe.parametrization.ifpan;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.service.tasklist.RefreshTaskList;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class WaitForAcceptingCost implements ExternalActivityBehaviour
{

	private final static Logger LOG = LoggerFactory.getLogger(WaitForAcceptingCost.class);

	@Override
	public void execute(ActivityExecution openExecution) throws Exception
	{
		boolean isOpened = true;
		try
		{
			if (!DSApi.isContextOpen())
			{
				isOpened = false;
				DSApi.openAdmin();
			}

			OfficeDocument doc = Jbpm4Utils.getDocument(openExecution);
			FieldsManager fm = doc.getFieldsManager();
			
			PreparedStatement ps = DSApi.context().prepareStatement("SELECT DOCUMENT_ID FROM dsg_ifpan_bilety where instr_wyjazdowa = ?");
			ps.setLong(1, doc.getId());
			ResultSet rs = ps.executeQuery();
			if (!rs.next())
			{
				openExecution.take("success");
				RefreshTaskList.addDocumentToRefresh(doc.getId());
				return;
			}
			Long biletyId = rs.getLong("DOCUMENT_ID");
			OfficeDocument bilety = (OfficeDocument) pl.compan.docusafe.core.base.Document.find(biletyId, false);
			FieldsManager biletyFM = bilety.getFieldsManager();
			
			Integer status = biletyFM.getIntegerKey("STATUS");
			if (status != null && status != 1410 && status < 17)
			{
				//openExecution.take("repeat");
				openExecution.waitForSignal();
			}
			else
			{
				ps = DSApi.context().prepareStatement("select ID_ from JBPM4_EXECUTION ex join JBPM4_VARIABLE va on ex.DBID_ = va.EXECUTION_ " +
					"where va.KEY_ = 'doc-id' and va.LONG_VALUE_ = ? and ex.ACTIVITYNAME_ = 'wait_for_accepting_all_costs'");
				ps.setLong(1, biletyId);
				rs = ps.executeQuery();
				rs.next();
				String biletActivityId = rs.getString(1);
			
				openExecution.take("success");
				Jbpm4Provider.getInstance().getExecutionService().signalExecutionById(biletActivityId);
				RefreshTaskList.addDocumentToRefresh(doc.getId());
			}
		}
		catch (Exception e)
		{
			LOG.error(e.getMessage(), e);
		}
		finally
		{
			if (DSApi.isContextOpen() && !isOpened)
			{
				DSApi.close();
			}
		}
	}

	@Override
	public void signal(ActivityExecution openExecution, String arg1, Map<String, ?> arg2) throws Exception
	{
		openExecution.take("success");
		OfficeDocument doc = Jbpm4Utils.getDocument(openExecution);
		RefreshTaskList.addDocumentToRefresh(doc.getId());
		
	}
}