package pl.compan.docusafe.parametrization.ifpan;

public class Zadanie
{
	private String nrprojektu;
	private String zadanie;
	private String tytulZadania;
	
	public Zadanie(String nrprojektu, String zadanie, String tytulZadania)
	{
		this.nrprojektu = nrprojektu;
		this.zadanie = zadanie;
		this.tytulZadania = tytulZadania;
	}

	public String getZadanie()
	{
		return zadanie;
	}

	public void setZadanie(String zadanie)
	{
		this.zadanie = zadanie;
	}

	public String getTytulZadania()
	{
		return tytulZadania;
	}

	public void setTytulZadania(String tytulZadania)
	{
		this.tytulZadania = tytulZadania;
	}

	public String getNrprojektu()
	{
		return nrprojektu;
	}

	public void setNrprojektu(String nrprojektu)
	{
		this.nrprojektu = nrprojektu;
	}
	
	
}
