package pl.compan.docusafe.parametrization.ifpan;

import java.math.BigDecimal;
import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class RozliczenieDecision implements DecisionHandler
{
	private final String RETURN_TO_IFPAN = "ifpan";
	private final String RETURN_TO_DELEGATE = "delegate";
	private final String RETURN_TO_IFPAN_AND_DELEGATE = "ifpan-delegate";
	private final String RETURN_TO_NOBODY = "zero";
	
	private static final Logger log = LoggerFactory.getLogger(RozliczenieDecision.class);

	public String decide(OpenExecution openExecution)

	{
		Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		try
		{
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			
			boolean toIfpan = false;
			boolean toDelegate = false;
			
			// ROZLICZENIE_IFPAN_WALUTA
			BigDecimal pozostalaWaluta = (BigDecimal)fm.getKey("ROZLICZENIE_IFPAN_WALUTA");
			// ROZLICZENIE_IFPAN_PLN
			BigDecimal pozostalaPln = (BigDecimal)fm.getKey("ROZLICZENIE_IFPAN_PLN");
			// ROZLICZENIE_IFPAN
			BigDecimal pozostala = (BigDecimal)fm.getKey("ROZLICZENIE_IFPAN");
			// ROZLICZENIE_DELEGOWANY
			BigDecimal brakujeDlaDelegowango = (BigDecimal)fm.getKey("ROZLICZENIE_DELEGOWANY");
			
			if (brakujeDlaDelegowango.compareTo(BigDecimal.ZERO) == 1 && pozostala.compareTo(BigDecimal.ZERO) == 0)
				toDelegate = true;
			else if (pozostala.compareTo(BigDecimal.ZERO) == 1 && brakujeDlaDelegowango.compareTo(BigDecimal.ZERO) == 0)
				toIfpan = true;
			else if (brakujeDlaDelegowango.compareTo(BigDecimal.ZERO) == 1 && pozostala.compareTo(BigDecimal.ZERO) == 1)
				toIfpan = toDelegate = true;
			
			if (toIfpan && toDelegate)
				return RETURN_TO_IFPAN_AND_DELEGATE;
			else if (toIfpan && !toDelegate)
				return RETURN_TO_IFPAN;
			else if (toDelegate && !toIfpan)
				return RETURN_TO_DELEGATE;
			else 
				return RETURN_TO_NOBODY;
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			return RETURN_TO_IFPAN_AND_DELEGATE;
		}
	}

}
