package pl.compan.docusafe.parametrization.ifpan;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.dbutils.DbUtils;
import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.parametrization.polcz.jbpm.CancelReservation;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class RozliczDelegacjeListener extends AbstractEventListener
{
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(CancelReservation.class);

	public void notify(EventListenerExecution execution) throws Exception
	{

		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Document doc = Document.find(docId);
		Connection con = null;
		CallableStatement call = null;
		try
		{
			String erpUser, erpPassword, erpUrl, erpDataBase, erpSchema, up_gsd_DelegacjaRozlicznie, up_gsd_DelegacjaRozliczenieKwota;
			erpUser = Docusafe.getAdditionProperty("erp.database.user");
			erpPassword = Docusafe.getAdditionProperty("erp.database.password");
			erpUrl = Docusafe.getAdditionProperty("erp.database.url");
			erpDataBase = Docusafe.getAdditionProperty("erp.database.dataBase");
			erpSchema = Docusafe.getAdditionProperty("erp.database.schema");
			// up_gsd_DelegacjaRozlicznie
			up_gsd_DelegacjaRozlicznie = Docusafe.getAdditionProperty("erp.database.delegacja.rozliczenia");
			// up_gsd_DelegacjaRozliczenieKwota
			up_gsd_DelegacjaRozliczenieKwota = Docusafe.getAdditionProperty("erp.database.delegacja.rozliczeniaKwota");

			if (up_gsd_DelegacjaRozlicznie == null)
				up_gsd_DelegacjaRozlicznie = "up_gsd_DelegacjaRozlicznie";
			if (up_gsd_DelegacjaRozliczenieKwota == null)
				up_gsd_DelegacjaRozliczenieKwota = "up_gsd_DelegacjaRozliczenieKwota";

			if (erpUser == null || erpPassword == null || erpUrl == null)
				throw new EdmException("Błąd eksportu faktury, brak parametrów połaczenia do bazy ");
			log.info("user: {}, password: {}, url: {}", erpUser, erpPassword, erpUrl);
			BasicDataSource ds = new BasicDataSource();
			ds.setDriverClassName(Docusafe.getDefaultDriver("sqlserver_2000"));
			ds.setUrl(erpUrl);
			ds.setUsername(erpUser);
			ds.setPassword(erpPassword);
			con = ds.getConnection();

			Long ddfin_id = rozliczDelegacje(doc, con, erpDataBase, erpSchema, up_gsd_DelegacjaRozlicznie);

			if (ddfin_id == null)
				throw new EdmException("B��d wywo�ania procedury rozlcizaj�cej delegacji: " + up_gsd_DelegacjaRozlicznie);

			Object advances = doc.getFieldsManager().getKey("WYDATKOWANE_SRODKI");

			Map<String, Object> advancesValues = new HashMap<String, Object>();
			List<Cost> delegateCosts = new ArrayList<Cost>();
			
			if (advances != null)
			{
				for (Long advnaceId : (List<Long>) advances)
				{
					if (!(advnaceId == -1))
					{
						advancesValues = DwrDictionaryFacade.dictionarieObjects.get("WYDATKOWANE_SRODKI").getValues(advnaceId.toString());

						//DelegateCost cost = new DelegateCost();
						//cost.setKosztWaluta((BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_AMOUNT") != null ? (BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_AMOUNT") : new BigDecimal(0));
						//cost.setKosztPLN((BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_PLN") != null ? (BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_PLN") : new BigDecimal(0));
						//cost.setKurs((BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_KURS") != null ? (BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_KURS") : new BigDecimal(0));
						
						Cost cost = new Cost();
						String platnikId = ((pl.compan.docusafe.core.dockinds.dwr.EnumValues) advancesValues.get("WYDATKOWANE_SRODKI_WYD_PLATNIK")).getSelectedId();
						cost.setZlecprod_id(platnikId);
						cost.setKoszt((BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_PLN") != null ? (BigDecimal) advancesValues.get("WYDATKOWANE_SRODKI_PLN") : new BigDecimal(0));
						if (delegateCosts.contains(cost))
						{
							int indexOf = delegateCosts.indexOf(cost);
							delegateCosts.get(indexOf).setKoszt(delegateCosts.get(indexOf).getKoszt().add(cost.getKoszt()));
						}
						else
							delegateCosts.add(cost);
					}
				}
			}
			
			for (Cost  cost : delegateCosts)
				rozliczenieKwoty(doc, con, erpDataBase, erpSchema, ddfin_id, cost, up_gsd_DelegacjaRozliczenieKwota);
			setDdFinId(doc, ddfin_id);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e);
		}
		finally
		{
			if (con != null)
			{
				try
				{
					con.close();
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
					throw new EdmException("Błąd akcpetacji wniosku :" + e.getMessage());
				}
			}
		}

	}

	private class Cost 
	{
		BigDecimal koszt;
		String zlecprod_id;
		public BigDecimal getKoszt()
		{
			return koszt;
		}
		public void setKoszt(BigDecimal koszt)
		{
			this.koszt = koszt;
		}
		public String getZlecprod_id()
		{
			return zlecprod_id;
		}
		public void setZlecprod_id(String zlecprod_id)
		{
			this.zlecprod_id = zlecprod_id;
		}
		@Override
		public int hashCode()
		{
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((zlecprod_id == null) ? 0 : zlecprod_id.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj)
		{
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Cost other = (Cost) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (zlecprod_id == null)
			{
				if (other.zlecprod_id != null)
					return false;
			}
			else if (!zlecprod_id.equals(other.zlecprod_id))
				return false;
			return true;
		}
		private RozliczDelegacjeListener getOuterType()
		{
			return RozliczDelegacjeListener.this;
		}

		
	}
	
	private void setDdFinId(Document doc, Long ddfin_id) 
	{
		PreparedStatement ps = null;
		try
		{
			String sql = "update dsg_ifpan_delegacja set ddfin_id = ? where document_id = ?";
			log.debug("ddfin_id.sql: {}", sql);
			ps = DSApi.context().prepareStatement(sql);
			ps.setLong(1, ddfin_id);
			ps.setLong(2, doc.getId());
			ps.executeUpdate();
			ps.close();
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		finally
		{
			DbUtils.closeQuietly(ps);
		}
		
	}
	
	/*
	 * CREATE procedure [dbo].[up_gsd_DelegacjaRozliczenieKwota]
	 * 
	 * @ddfin_id bigint , -- z poprzednie proc
	 * 
	 * @kwota money , --
	 * 
	 * @kurs money ,
	 * 
	 * @kwotawalbaz money , --
	 * 
	 * @zlecprod_id bigint , --
	 * 
	 * @wytwor_id bigint -- '1'
	 * 
	 * @author jtarasiuk
	 */
	private void rozliczenieKwoty(Document doc, Connection con, String erpDataBase, String erpSchema, Long ddfin_id, Cost cost, String up_gsd_DelegacjaRozliczenieKwota) throws EdmException
	{
		try
		{
			// @ddfin_id bigint , -- z poprzednie proc = Long ddfin_id
			// @kwota money , --
			BigDecimal kwota = cost.getKoszt();
			// @kurs money ,
			BigDecimal kurs = BigDecimal.ONE.setScale(4);
			// @kwotawalbaz money , --
			BigDecimal kwotawalbaz = cost.getKoszt();
			// @zlecprod_id bigint , --
			Long zlecprod_id = getZlecProdId(cost);
			// @wytwor_id bigint -- '1'
			Long wytwor_id = new Long(93);
			CallableStatement call = con.prepareCall("{call [" + erpDataBase + "].[" + erpSchema + "].[" + up_gsd_DelegacjaRozliczenieKwota + "] (?,?,?,?,?,?)}");

			for (int i = 1; i <= 6; i++)
				call.setObject(i, null);

			call.setLong(1, ddfin_id);
			call.setBigDecimal(2, kwota);
			call.setBigDecimal(3, kurs.setScale(4));
			call.setBigDecimal(4, kwotawalbaz);
			call.setLong(5, zlecprod_id);
			call.setLong(6, wytwor_id);

			int resultNum = 0;

			while (true)
			{
				boolean queryResult;
				int rowsAffected;

				if (1 == ++resultNum)
				{
					try
					{
						queryResult = call.execute();

						log.debug("EXPORT koszt (deleacja_id) : {}", doc.getId());

					}
					catch (Exception e)
					{
						// Process the error
						log.error("Result " + resultNum + " is an error: " + e.getMessage());

						// When execute() throws an exception, it may just
						// be that the first statement produced an error.
						// Statements after the first one may have
						// succeeded. Continue processing results until
						// there
						// are no more.
						continue;
					}
				}
				else
				{
					try
					{
						queryResult = call.getMoreResults();
					}
					catch (Exception e)
					{
						// Process the error
						log.error("1 != ++resultNum: Result " + resultNum + " is an error: " + e.getMessage());

						// When getMoreResults() throws an exception, it
						// may just be that the current statement produced
						// an error.
						// Statements after that one may have succeeded.
						// Continue processing results until there
						// are no more.
						continue;
					}
				}

				if (queryResult)
				{
					ResultSet rs = call.getResultSet();

					// Process the ResultSet
					log.debug("Result " + resultNum + " is a ResultSet: " + rs);
					ResultSetMetaData md = rs.getMetaData();

					int count = md.getColumnCount();
					log.debug("<table border=1>");
					log.debug("<tr>");
					for (int i = 1; i <= count; i++)
					{
						log.debug("<th>");
						log.debug("GEtColumnLabeL: {}", md.getColumnLabel(i));
					}
					log.debug("</tr>");
					while (rs.next())
					{
						log.debug("<tr>");
						for (int i = 1; i <= count; i++)
						{
							log.debug("<td>");
							log.debug("Get.Object: {}", rs.getObject(i));
						}
						log.debug("</tr>");
					}
					log.debug("</table>");
					rs.close();
				}
				else
				{
					rowsAffected = call.getUpdateCount();

					// No more results
					if (-1 == rowsAffected)
					{
						--resultNum;
						break;
					}

					// Process the update count
					log.debug("Result " + resultNum + " is an update count: " + rowsAffected);
				}
			}

			log.debug("Done processing " + resultNum + " results");
			if (resultNum == 0)
			{

			}

		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException("Błąd akcpetacji wniosku :" + e.getMessage());
		}
	}

	private Long getZlecProdId(Cost cost)
	{
		// pobranie z wiodoku
		EnumItem zlecprodEnumItem;
		try
		{
			zlecprodEnumItem = DataBaseEnumField.getEnumItemForTable("dsg_ifpan_founding_source_erp_view", Integer.parseInt(cost.getZlecprod_id()));
			String platnik = zlecprodEnumItem.getRefValue().replace(".0", "");
			return Long.valueOf(platnik);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			return null;
		}
	}

	/**
	 * CREATE procedure [dbo].[up_gsd_DelegacjaRozlicznie]
	 * 
	 * @ddfin_typ_id bigint , -- 5 zagrauv=cza 6 - krajo
	 * @user_id numeric(10,0) , -- 61
	 * @pracownik_nrewid numeric(10,0) , --
	 * @numerdok char (20) ,
	 * @data_rej date , --
	 * @datadok date , -- data roz
	 * @opis varchar(60) ,
	 * @ddfin_id bigint out , -- przechwytuje
	 * @result varchar(max) out
	 */
	private Long rozliczDelegacje(Document doc, Connection con, String erpDataBase, String erpSchema, String up_gsd_DelegacjaRozlicznie) throws EdmException
	{
		Long ddfin_id = null;
		// @ddfin_typ_id - 5 zagraniczna, 6 krajowa
		Long ddfin_typ_id = new Long(5);
		// @user_id - 61 Docusafe
		Long user_id = new Long(61);
		// @pracownik_nrewid
		Long pracownik_nrewid = IfpanUtils.getWorkerId(doc);
		// @numerdok
		String numerdok = getNumerDok(doc);
		// @data_rej date
		java.sql.Date data_rej = new java.sql.Date(new Date().getTime());
		// @datadok
		java.sql.Date datadok = data_rej;
		// @opis
		String opis = getOpis(doc);

		try
		{
			CallableStatement call = con.prepareCall("{call [" + erpDataBase + "].[" + erpSchema + "].[" + up_gsd_DelegacjaRozlicznie + "] (?,?,?,?,?,?,?,?,?)}");

			for (int i = 1; i <= 7; i++)
				call.setObject(i, null);
			call.setLong(1, ddfin_typ_id);
			call.setLong(2, user_id);
			call.setLong(3, pracownik_nrewid);
			call.setString(4, numerdok);
			call.setDate(5, (java.sql.Date) data_rej);
			call.setDate(6, datadok);
			call.setString(7, opis);

			call.registerOutParameter(8, java.sql.Types.BIGINT);
			call.registerOutParameter(9, java.sql.Types.VARCHAR);

			int resultNum = 0;

			while (true)
			{
				boolean queryResult;
				int rowsAffected;

				if (1 == ++resultNum)
				{
					try
					{
						queryResult = call.execute();

						log.error("ExportDelegationToSimpleERP: ");
						ddfin_id = call.getLong(8);
						String result = call.getString(9);
						log.error("ExportAdvanceToSimpleERP: ddfin_id: {}", ddfin_id);
						log.error("ExportAdvanceToSimpleERP: result : {}", result);

						log.debug("EXPORT Remove application_number (deleacja_id) : {}", doc.getId());

					}
					catch (Exception e)
					{
						// Process the error
						log.error("Result " + resultNum + " is an error: " + e.getMessage());

						// When execute() throws an exception, it may just
						// be that the first statement produced an error.
						// Statements after the first one may have
						// succeeded. Continue processing results until
						// there
						// are no more.
						continue;
					}
				}
				else
				{
					try
					{
						queryResult = call.getMoreResults();
					}
					catch (Exception e)
					{
						// Process the error
						log.error("1 != ++resultNum: Result " + resultNum + " is an error: " + e.getMessage());

						// When getMoreResults() throws an exception, it
						// may just be that the current statement produced
						// an error.
						// Statements after that one may have succeeded.
						// Continue processing results until there
						// are no more.
						continue;
					}
				}

				if (queryResult)
				{
					ResultSet rs = call.getResultSet();

					// Process the ResultSet
					log.debug("Result " + resultNum + " is a ResultSet: " + rs);
					ResultSetMetaData md = rs.getMetaData();

					int count = md.getColumnCount();
					log.debug("<table border=1>");
					log.debug("<tr>");
					for (int i = 1; i <= count; i++)
					{
						log.debug("<th>");
						log.debug("GEtColumnLabeL: {}", md.getColumnLabel(i));
					}
					log.debug("</tr>");
					while (rs.next())
					{
						log.debug("<tr>");
						for (int i = 1; i <= count; i++)
						{
							log.debug("<td>");
							log.debug("Get.Object: {}", rs.getObject(i));
						}
						log.debug("</tr>");
					}
					log.debug("</table>");
					rs.close();
				}
				else
				{
					rowsAffected = call.getUpdateCount();

					// No more results
					if (-1 == rowsAffected)
					{
						--resultNum;
						break;
					}

					// Process the update count
					log.debug("Result " + resultNum + " is an update count: " + rowsAffected);
				}
			}

			log.debug("Done processing " + resultNum + " results");
			if (resultNum == 0)
			{

			}

		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException("Brozlcizenia delegacji :" + e.getMessage());
		}
		return ddfin_id;
	}

	private String getOpis(Document doc)
	{
		try
		{
			List<Long> countries = (List<Long>) doc.getFieldsManager().getKey("DEL_COUNTRY");
			return IfpanUtils.prepareDescritpion(countries, doc);
		}
		catch (EdmException e)
		{

		}
		return "Rozliczenie delegacji";
	}

	private String getNumerDok(Document doc)
	{
		String year = DateUtils.formatYear(new java.util.Date());
		return doc.getId().toString() + "/" + year;
	}

	private Long getZlecProd(String project_id)
	{
		PreparedStatement ps = null;
		Long zlec_prod_id = null;
		try
		{

			ps = DSApi.context().prepareStatement("select zledprodid from dsr_ifpan_project_erp where id = ?");
			ps.setLong(1, Long.valueOf(project_id));
			ResultSet rs = ps.executeQuery();
			while (rs.next())
			{
				zlec_prod_id=  rs.getLong(1);
				break;
			}
			rs.close();
			ps.close();

		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		DbUtils.closeQuietly(ps);
		return zlec_prod_id;
	
	}
}
