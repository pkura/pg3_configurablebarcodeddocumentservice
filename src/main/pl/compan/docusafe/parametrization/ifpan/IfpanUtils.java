package pl.compan.docusafe.parametrization.ifpan;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.DSCountry;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IfpanUtils
{
	public static final Logger log = LoggerFactory.getLogger(IfpanUtils.class);
	
	private static Connection erpConnection;

	
	public static void updateOrExportPerson(Person person) throws EdmException
	{
		try
		{
			updatePerson(person);
		}
		catch(Exception e)
		{
			exportPerson(person, false);
			updatePerson(person);
		}
	}
	
	public static void updatePerson(Person person) throws EdmException
	{
		Connection con = null;
		CallableStatement stmt = null;
		log.debug("UPDATE PERSON person.getOrganization(): {},", person.getOrganization());
		if (StringUtils.isEmpty(person.getOrganization()))
			throw new EdmException("Brak nazwy kontrahenta");
		
		try
		{
			String erpUser, erpPassword, erpUrl, erpDataBase, erpSchema, procUpdateDostawcy;
			erpUser = Docusafe.getAdditionProperty("erp.database.user");
			erpPassword = Docusafe.getAdditionProperty("erp.database.password");
			erpUrl = Docusafe.getAdditionProperty("erp.database.url");
			erpDataBase = Docusafe.getAdditionProperty("erp.database.dataBase");
			erpSchema = Docusafe.getAdditionProperty("erp.database.schema");
			// up_gsd_zmiendanedost
			procUpdateDostawcy = Docusafe.getAdditionProperty("erp.database.procUpdateDostawcy");
			con = connectToSimpleErpDataBase(erpUser, erpPassword, erpUrl);

			stmt = con.prepareCall("{? = call [" + erpDataBase + "].[" + erpSchema + "].[" + procUpdateDostawcy + "] " +
					"(?, ?, ?, ?, ?, ?, ?) }");

			stmt.setString(2, person.getWparam().toString()); 
			stmt.setString(3, person.getOrganization());
			stmt.setString(4, person.getStreet());
			stmt.setString(5, person.getZip());
			stmt.setString(6, person.getLocation());
			if (person.getCountry() != null) {
				Integer countryId = 2;
				try 
				{
					countryId = Integer.valueOf(person.getCountry());
				} 
				catch (NumberFormatException e) 
				{
					log.error("B��d w parsowaniu id kraju dla dso_person");
				}
				String countryCn = "PL";
				if (DSCountry.findBy(countryId)!= null) 
				{
					countryCn = DSCountry.findBy(countryId).getCn();
				} else {
					log.error("B��d w parsowaniu w znalezieniu cn kraju "+countryId);
				}
				stmt.setString(7, countryCn);
			} else {
				stmt.setString(7, null);
			}
 
			stmt.registerOutParameter(1, java.sql.Types.INTEGER);
			stmt.registerOutParameter(8, java.sql.Types.VARCHAR);
			
			stmt.execute();
			
			log.debug("UPDATE PERSON execute: {}");
			
			if (stmt.getInt(1) == 0 && stmt.getString(8) == null) {
				log.debug("UPDATE PERSON successfull");
			} else {
				
					log.error("UPDATE PERSON fail: "+stmt.getInt(1)+", "+stmt.getString(8));
					throw new EdmException("B��d aktualizacji kontrahenta :" + stmt.getInt(1));
				
			}
			
		}
		catch (Exception e)
		{
			log.error("EXPORT PERSON ERROR : " + e.getMessage(), e);
			throw new EdmException("B��d aktualizacji kontrahenta :" + e.getMessage());
		}
		finally
		{
			if (con != null)
			{
				try
				{
//					con.close();
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
					throw new EdmException("B��d eksportu kontrahenta :" + e.getMessage());
				}
			} 
		}
	}	
	
	public static void exportPerson(Person person, boolean repeat) throws EdmException
	{
		Connection con = null;
		CallableStatement stmt = null;
		log.debug("EXPORT PERSON person.getOrganization(): {},", person.getOrganization());
		if (StringUtils.isEmpty(person.getOrganization()))
			throw new EdmException("Brak nazyw kontrahenta");
		
		try
		{
			String erpUser, erpPassword, erpUrl, erpDataBase, erpSchema, procImportKontrahentDostawca;
			erpUser = Docusafe.getAdditionProperty("erp.database.user");
			erpPassword = Docusafe.getAdditionProperty("erp.database.password");
			erpUrl = Docusafe.getAdditionProperty("erp.database.url");
			erpDataBase = Docusafe.getAdditionProperty("erp.database.dataBase");
			erpSchema = Docusafe.getAdditionProperty("erp.database.schema");
			// erp_import_kontrahent_dostawca_pub
			procImportKontrahentDostawca = Docusafe.getAdditionProperty("erp.database.procImportKontrahentDostawca");
			con = connectToSimpleErpDataBase(erpUser, erpPassword, erpUrl);

			String klasaDost = Docusafe.getAdditionProperty("erp.database.klasaDost");
			if (StringUtils.isEmpty(klasaDost))
				klasaDost = "KRAJOWY";

			stmt = con.prepareCall("{? = call [" + erpDataBase + "].[" + erpSchema + "].[" + procImportKontrahentDostawca + "] " +
					"(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + 
					"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + 
					"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + 
					"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + 
					"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + 
					"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + 
					"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + 
					"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + 
					" ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }");

			for (int i = 2; i <= 161; i++)
				stmt.setObject(i, null);
			// @dostawca_idn VARCHAR(15)
			String dostawca_idn = person.getId().toString();
			stmt.setString(2, dostawca_idn);
			// @klasdost_idn VARCHAR(15) ,
			String klasadost_id = "KRAJOWY";
			String kraj_rejpod_idn = "PL";
			if  (person.getCountry()!=null) {
				try {
					if (DSCountry.findBy(Integer.valueOf(person.getCountry())) != null) {
						kraj_rejpod_idn = DSCountry.findBy(Integer.valueOf(person.getCountry())).getCn();
						if (!"PL".equalsIgnoreCase(kraj_rejpod_idn)) {
							klasadost_id = "ZAGRANICZNY";
						}
					}
				} catch (NumberFormatException e) {
					log.error(e.toString());
				}
			}
			
			stmt.setString(3, klasadost_id);
			// @kraj_rejpod_idn VARCHAR(15) = NULL ,
			//String kraj_rejpod_idn = "Polska";
			stmt.setString(6, kraj_rejpod_idn);
			// @fax VARCHAR(30) = NULL ,
			String fax = person.getFax();
			stmt.setString(12, fax);
			// @email VARCHAR(50) = NULL ,
			String email = person.getEmail();
			stmt.setString(13, email);
			//  @stanowisko VARCHAR(40) = NULL ,
			String stanowisko = person.getTitle();
			stmt.setString(14, stanowisko);
			//   @nip CHAR(13) = NULL ,
			String nip = person.getNip();
			stmt.setString(15, nip);
			//  @regon VARCHAR(14) = NULL ,
			String regon = person.getRegon();
			stmt.setString(18,regon);
			//  @nazwa VARCHAR(60) = NULL ,
			String nazwa = "";
			if (person.getOrganization() != null)
				nazwa = nazwa + person.getOrganization();
			if (person.getFirstname() != null)
				nazwa = nazwa + " " + person.getFirstname();
			if (person.getLastname() != null)
				nazwa = nazwa + " " + person.getLastname();
			stmt.setString(19, nazwa);
			// @kodpoczt VARCHAR(10) = NULL ,
			String kodpocztowy = person.getZip();
			stmt.setString(22, kodpocztowy);
			//  @miasto VARCHAR(30) = NULL ,
			String miasto = person.getLocation();
			stmt.setString(23, miasto);
			//  @ulica CHAR(40) = NULL ,
			String ulica = person.getStreet();
			stmt.setString(25, ulica);
			//  @status INT = 1 ,
			int _status = 1;
			stmt.setInt(28, _status);
			// @nazwa_adr
			String nazwa_adr = "Podstawowy";
			stmt.setString(42, nazwa_adr);
			// @czyzabl = 0 ,
			int czyzabl = 0;
			stmt.setInt(60, czyzabl);
			for (int i = 71; i <= 139; i++)
				stmt.setInt(i, 0);
			
			//  @status_czy_update INT , 
			int status_czy_update = 0;
			stmt.setInt(161, status_czy_update);
			BigDecimal wparam = null;
			stmt.setBigDecimal(162, wparam);
			
			stmt.registerOutParameter(1, java.sql.Types.INTEGER);
			stmt.registerOutParameter(162, java.sql.Types.NUMERIC);
			
			log.debug("EXPORT PERSON: @dostawca_idn= {}, @klasdost_idn= {},  @kraj_rejpod_idn= {}, @fax= {},  @email= {}, @stanowisko= {}, @nip= {}, @regon= {}, @nazwa= {}, @kodpoczt= {}, @miasto= {}, @ulica= {},  @status= {},  @status_czy_update= {}",
					dostawca_idn, klasadost_id, kraj_rejpod_idn, fax, email, stanowisko, nip, regon, nazwa, kodpocztowy, miasto, ulica, _status, status_czy_update);
			log.debug("EXPORT PERSON execute: {}", stmt.execute());
			Integer status = stmt.getInt(1);
			log.debug("EXPORT PERSON STATUS: {}", status);
			wparam = stmt.getBigDecimal(162);
			log.debug("EXPORT PERSON WPRAM: {}", wparam);
			person.setWparam(wparam.longValue());
			person.update();
			
			if (person.getBasePersonId() != null)
			{
				try
				{
					Person basePerson = Person.find(person.getBasePersonId().longValue());  
					basePerson.setWparam(wparam.longValue());
					basePerson.update();
					
					//tutaj ustawiamy wparam (z erpa) dla wszystkich personow ktorych base person to basePerson
					/*for(Person per: Person.getByBasePerson(basePerson.getId()))
					{
						per.setWparam(wparam.longValue());
						per.update();
					} */
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
				}
			}
			
			if (status < 0 && repeat)
			{
				throw new EdmException("B��d eksportu kontrahenta :" + status);
			}
		}
		catch (Exception e)
		{
			log.error("EXPORT PERSON ERROR : " + e.getMessage(), e);
			throw new EdmException("B��d eksportu kontrahenta :" + e.getMessage());
		}
		finally
		{
			if (con != null)
			{
				try
				{
//					con.close();
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
					throw new EdmException("B��d eksportu kontrahenta :" + e.getMessage());
				}
			}
		}
	}

	public static Connection connectToSimpleErpDataBase(String erpUser, String erpPassword, String erpUrl) throws SQLException, EdmException
	{
		return connectToSimpleErpDataBase(erpUser, erpPassword, erpUrl, null);
	}
	
	public static Connection connectToSimpleErpDataBase(String erpUser, String erpPassword, String erpUrl, String sqlServerName) throws SQLException, EdmException
	{
		
		if(erpConnection==null || erpConnection.isClosed()){
			if (erpUser == null || erpPassword == null || erpUrl == null)
				throw new EdmException("Brak parametr�w po�czenia do bazy ERP");
			log.debug("user: {}, password: {}, url: {}", erpUser, erpPassword, erpUrl);
			BasicDataSource ds = new BasicDataSource();
			ds.setDriverClassName(Docusafe.getDefaultDriver(sqlServerName!=null?sqlServerName:"sqlserver_2000"));
			ds.setUrl(erpUrl);
			ds.setUsername(erpUser);
			ds.setPassword(erpPassword);
			erpConnection = ds.getConnection();
		}
		synchronized(erpConnection){
		
			return erpConnection;
		}
	}

	public static void exportInvoice(Document doc, boolean repeat) throws EdmException
	{
		Connection con = null;
		CallableStatement call = null;
		log.info("EXPORT INVOICE ID: {}", doc.getId());
		try
		{
			FieldsManager fm = doc.getDocumentKind().getFieldsManager(doc.getId());
			String erpUser, erpPassword, erpUrl, erpDataBase, erpSchema, procKsiegaPodawcza;
			erpUser = Docusafe.getAdditionProperty("erp.database.user");
			erpPassword = Docusafe.getAdditionProperty("erp.database.password");
			erpUrl = Docusafe.getAdditionProperty("erp.database.url");
			erpDataBase = Docusafe.getAdditionProperty("erp.database.dataBase");
			erpSchema = Docusafe.getAdditionProperty("erp.database.schema");
			// up_gsd_ksiegapodawcza
			procKsiegaPodawcza = Docusafe.getAdditionProperty("erp.database.procKsiegaPodawcza");
			
			con = connectToSimpleErpDataBase(erpUser, erpPassword, erpUrl); 
			// 2@dok_idm varchar (20) , id_doc
			// 3@dokobcy varchar (80) , numer_faktury
			// 4@dostawca_id numeric (10,0) ,
			// 5@komorka numeric (10,0) ,
			// 6@netto money ,
			// 7@vat money ,
			// 8@data_rej date ,
			// 9@data_faktury date ,
			// 10@termin_plat date ,
			// 11@result varchar(max) out
			call = con.prepareCall("{? = call [" + erpDataBase + "].[" + erpSchema + "].[" + procKsiegaPodawcza + "] (?,?,?,?,?,?,?,?,?,?,?,?,?,? )}");
			for (int i = 2; i <= 10; i++)
				call.setObject(i, null);
			for (int i = 12; i <= 15; i++)
				call.setObject(i, null);

			
			call.registerOutParameter(1, java.sql.Types.INTEGER);
			call.registerOutParameter(11, java.sql.Types.VARCHAR);
			call.setString(2, "" + doc.getId().toString());
			call.setString(3, "" + fm.getKey("NUMER_FAKTURY"));
			Long conId = (Long) doc.getFieldsManager().getKey("CONTRACTOR");
			Person pen = Person.find(conId.longValue());
				try 
				{
					if (pen.getWparam() != null) {
						//updatePerson(pen);
						updateOrExportPerson(pen);
					} else {
						exportPerson(pen, false);
					}
				}
				catch (Exception e)
				{
					throw new EdmException("B��d eksportu kontrahenta:" + e);
				}
			
			call.setLong(4, pen.getWparam());
			call.setInt(5, 2);
			// 6@netto money , - kwota w walucie
			call.setObject(6, fm.getKey("KWOTA_W_WALUCIE"));
			
			// 7@vat money , - vat w walucie
			BigDecimal kurs = new BigDecimal(fm.getKey("KURS").toString());
			kurs = kurs.setScale(4, RoundingMode.HALF_UP);
			BigDecimal vat =new BigDecimal(fm.getKey("VAT").toString()).setScale(4, RoundingMode.HALF_UP);
			vat =  vat.divide(kurs, 2, RoundingMode.HALF_UP);
			
			call.setObject(7, vat);
			call.setDate(8, new Date((doc.getCtime().getTime())));
			call.setDate(9, new Date(((java.util.Date) fm.getKey("DATA_WYSTAWIENIA")).getTime()));
			call.setDate(10, new Date(((java.util.Date) fm.getKey("TERMIN_PLATNOSCI")).getTime()));
			
			BigDecimal vatBaz = new BigDecimal(fm.getKey("VAT").toString());
			
			String currency = fm.getEnumItemCn("WALUTA");
			
			// @waluta_id int , --symbol waluty
			call.setObject(12, currency);
			
			// @kurs_waluty money ,
			call.setObject(13, kurs);
			
			// @nettowalbaz money waluta bazowa pln
			call.setObject(14, fm.getKey("KWOTA_NETTO"));
			
			// @vatwalutbaz money vat bazowy w pln
			call.setObject(15, vatBaz);

			log.debug("dok_idm {}, dokobcy {}, dostawca_id {}, komorka {}, netto {}, vat{}, data_rej{}, data_faktury {}, termin_plat{}", doc.getId(),fm.getKey("NUMER_FAKTURY"), pen.getWparam(), 2, fm.getKey("KWOTA_NETTO"), fm.getKey("VAT"),  new Date((doc.getCtime().getTime())), new Date(((java.util.Date) fm.getKey("DATA_WYSTAWIENIA")).getTime()),  new Date(((java.util.Date) fm.getKey("TERMIN_PLATNOSCI")).getTime()));

            int resultNum = 0;
			
            while (true)
            {
                boolean queryResult;
                int rowsAffected;
 
                if (1 == ++resultNum)
                {
                    try
                    {
                    	log.error("!!!!!!!!!!!!!!!!!!!!!!! ");
                        queryResult = call.execute();
                        //queryResult = call.getMoreResults();
                        
                        String 	msg = call.getString(11);
                        log.debug("EXPORT INVOICE msg : {}", msg);

                        if (msg != null)
                        	throw new EdmException(msg);
                    }
                    catch (EdmException e)
                    {
                    	log.error(e.getMessage(), e);
                    	throw e;
                    }
                    catch (Exception e)
                    {
                        // Process the error
                       log.error("Result " + resultNum + " is an error: " + e.getMessage());
 
                        // When execute() throws an exception, it may just be that the first statement produced an error.
                        // Statements after the first one may have succeeded.  Continue processing results until there
                        // are no more.
                        continue;
                    }
                }
                else
                {
                    try
                    {
                        queryResult = call.getMoreResults();
                    }
                    catch (Exception e)
                    {
                        // Process the error
                        log.error("1 != ++resultNum: Result " + resultNum + " is an error: " + e.getMessage());
 
                        // When getMoreResults() throws an exception, it may just be that the current statement produced an error.
                        // Statements after that one may have succeeded.  Continue processing results until there
                        // are no more.
                        continue;
                    }
                }
 
                if (queryResult)
                {
                    ResultSet rs = call.getResultSet();
 
                    // Process the ResultSet
                   log.debug("Result " + resultNum + " is a ResultSet: " + rs);
                	   ResultSetMetaData md = rs.getMetaData();
 
                	    int count = md.getColumnCount();
                	    log.debug("<table border=1>");
                	    log.debug("<tr>");
                	    for (int i=1; i<=count; i++) {
                	      log.debug("<th>");
                	      log.debug("GEtColumnLabeL: {}",  md.getColumnLabel(i));
                	    }
                	    log.debug("</tr>");
                	    while (rs.next()) {
                	      log.debug("<tr>");
                	      for (int i=1; i<=count; i++) {
                	        log.debug("<td>");
                	        log.debug("Get.Object: {}", rs.getObject(i));
                	      }
                	      log.debug("</tr>");
                	    }
                	    log.debug("</table>");
                		//Integer status = rs.getInt(1);
               			//log.debug("EXPORT INVOICE status : {}", status);
               			//String 	msg = rs.getString(11);
               			//log.debug("EXPORT INVOICE msg : {}", msg);
                    rs.close();
                }
                else
                {
                    rowsAffected = call.getUpdateCount();
 
                    // No more results
                    if (-1 == rowsAffected)
                    {
                        --resultNum;
                        break;
                    }
 
                    // Process the update count
                  log.debug("Result " + resultNum + " is an update count: " + rowsAffected);
                }
            }
 
            log.debug("Done processing " + resultNum + " results");
            if (resultNum == 0)
            {

            	
			}
           // throw new EdmException("Message: ");
			//
			
			//log.debug("EXPORT INVOICE: {}", res);
			//Boolean 
			//log.debug("call.getMoreResults(): {}", call.getMoreResults());
			//if ()
			//Integer status = call.getInt(1);
			//log.debug("EXPORT INVOICE status : {}", status);
			
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException("B��d eksportu faktury :" + e.getMessage());
		}
		finally
		{
			if (con != null)
			{
				try
				{
//					con.close();
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
					throw new EdmException("B��d eksportu faktury :" + e.getMessage());
				}
			}
		}
	}

	public static Map<String,Object> verifyContractor(String nip) throws EdmException
	{
	    	Map<String,Object> foundContractor = new HashMap<String,Object>();
		Connection con = null;
		CallableStatement call = null;
		log.info("SEARCH CONTRACTOR NIP: {}", nip);
		try
		{
			String erpUser, erpPassword, erpUrl, erpDataBase, erpSchema, procWeryfikacjaDostawcy;
			erpUser = Docusafe.getAdditionProperty("erp.database.user");
			erpPassword = Docusafe.getAdditionProperty("erp.database.password");
			erpUrl = Docusafe.getAdditionProperty("erp.database.url");
			erpDataBase = Docusafe.getAdditionProperty("erp.database.dataBase");
			erpSchema = Docusafe.getAdditionProperty("erp.database.schema");
			// up_gsd_weryfikacja_dost
			procWeryfikacjaDostawcy = Docusafe.getAdditionProperty("erp.database.procWeryfikacjaDostawcy");
			
			con = connectToSimpleErpDataBase(erpUser, erpPassword, erpUrl);
			
			call = con.prepareCall("{? = call [" + erpDataBase + "].[" + erpSchema + "].[" + procWeryfikacjaDostawcy + "] (?,?,?,?,?,?,?,?,?,?,? )}");
			call.setObject(2, null);			
			call.registerOutParameter(1, java.sql.Types.INTEGER);
			call.registerOutParameter(3, java.sql.Types.VARCHAR);
			call.registerOutParameter(4, java.sql.Types.NUMERIC);
			
			for (int i=5;i<=12;i++)
			    call.registerOutParameter(i, java.sql.Types.VARCHAR);

			call.setObject(2, nip);

			log.warn("nip {}", nip);

			boolean queryResult;
			try {
			    queryResult = call.execute();
			    
			    if (!queryResult) {
				String result = call.getString(3);
				
				if (result == null)
	                        	throw new EdmException(result);
				
				foundContractor.put("wparam", call.getLong(4));
				foundContractor.put("email", call.getString(5));
				foundContractor.put("organization", call.getString(6));
				foundContractor.put("regon", call.getString(8));
				foundContractor.put("zip", call.getString(9));
				foundContractor.put("location", call.getString(10));
				foundContractor.put("street", call.getString(11));
				
				if (call.getString(12) != null && !call.getString(12).equals("")) {
					Integer id = null;
					if (DSCountry.find(null, call.getString(12), null) != null) {
						id = DSCountry.find(null, call.getString(12), null).get(0).getId();
						foundContractor.put("country", id.toString());
					}			
				}

				foundContractor.put("nip", nip);
				log.warn("Wartosci pobrane z erp: {}", foundContractor);
			    } else throw new EdmException("B��d w znalezieniu dostawcy");
			    
			    
			} catch (EdmException e) {
			    log.error(e.getMessage(), e);
			    throw e;
			} catch (Exception e) {
			    log.error("Result is an error: " + e.getMessage());
			    throw e;
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new EdmException("B��d w znalezieniu dostawcy :" + e.getMessage());
		} finally {
			if (con != null)
			{
				try
				{
//					con.close();
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
					throw new EdmException("B��d w znalezieniu dostawcy:" + e.getMessage());
				}
			}
		}
		return foundContractor;
		
	}
	
	

	/** Export polecenia zaliczki do Simple.ERP */
	public static boolean exportAdvance(Document doc, boolean repeat) throws EdmException
	{
		boolean dkDp = false;
		List<IfpanDelegationAdvance> advances = new ArrayList<IfpanDelegationAdvance>();

		/**********************
		 * Koszty jednostki idelegujacej
		 *********************/
		Object advancesValuesId = doc.getFieldsManager().getKey("DELEGATE_COSTS");
		//Integer delTypeId = doc.getFieldsManager().getEnumItem("DEL_TYPE") != null?doc.getFieldsManager().getEnumItem("DEL_TYPE").getId():291;
		//delTypeId -= 290;
		if (advancesValuesId == null)
		{
			log.error("ExportAdvanceToSimpleERP: DELEGATE_COSTS == null !!!!");
			throw new EdmException("Brak zaliczek");
		}

		log.debug("ExportAdvanceToSimpleERP: DELEGATE_COSTS != null {}", advancesValuesId);
		
		for (Long advnaceId : (List<Long>) advancesValuesId)
		{
			if (isSent(advnaceId))
				continue;

			Map<String, Object> advanceValues = DwrDictionaryFacade.dictionarieObjects.get("DELEGATE_COSTS").getValues(advnaceId.toString());
			String typId = ((EnumValues) advanceValues.get("DELEGATE_COSTS_ADVANCE_KIND")).getSelectedOptions().get(0);
			IfpanDelegationAdvance.Type typ = null;
			/**
			 * <enum-item id="12040" cn="WALUTA" title="Wyp�ata walutowa w banku"/>
			 * DK - "Wyp�ata w z�otych w kasie IF PAN
			 * DP = "Przelew na konto delegowanego"
			 */
			if (isDK(doc, typId))
				typ = IfpanDelegationAdvance.Type.DK;
			else if (isDP(doc, typId))
				typ = IfpanDelegationAdvance.Type.DP;
			else
				continue;
			
			dkDp = true;
			IfpanDelegationAdvance advance = new IfpanDelegationAdvance();
			advance.setWorker(getWorkerId(doc));
			List<Long> countries = (List<Long>)doc.getFieldsManager().getKey("DEL_COUNTRY");
			advance.setCountry(getCountryCode(countries));
			advance.setDescription(prepareDescritpion(countries, doc));
			advance.setOrganizator(getOrganizator(countries));
			advance.setCurrency(getCurrencyCode(advanceValues));
			Long currErpId = null;
			try {
				currErpId = Long.valueOf(getCurrency(advanceValues, "refValue"));
			} catch (Exception e) {
				throw new EdmException("Nie mo�na pobra� identyfikatora dla wybranej waluty, id delegacji: "+advnaceId);
			}
			advance.setCurrencyErpId(currErpId);
			advance.setAmount((BigDecimal) advanceValues.get("DELEGATE_COSTS_COST_PLN"));
			advance.setAmountCurr(((BigDecimal) advanceValues.get("DELEGATE_COSTS_CURRENCY_COST")).multiply(advanceValues.get("DELEGATE_COSTS_AMOUNT")!=null?(BigDecimal) advanceValues.get("DELEGATE_COSTS_AMOUNT"):BigDecimal.ONE).setScale(2, RoundingMode.HALF_UP));
			advance.setCurrencyRate((BigDecimal) advanceValues.get("DELEGATE_COSTS_RATE"));
			//advance.setDelType(delTypeId); 
			advance.setType(typ);
			advance.setAdvanceNr(getAdvanceNr(doc, advnaceId));
			advance.setRegisterDate(new java.sql.Date(new java.util.Date().getTime()));
			advances.add(advance);
		}
		sendAdvancesToErp(advances, "DELEGATE_COSTS", doc.getCtime());
		return dkDp;
	}

	private static Boolean isSent(Long advnaceId) throws EdmException
	{
		NativeCriteria nc1 = DSApi.context().createNativeCriteria("dsg_ifpan_delegate_costs", "d").setProjection(NativeExps.projection().addProjection("SENDED_TO_ERP", "SENDED_TO_ERP"));
		nc1.add(NativeExps.eq("id", advnaceId));
		CriteriaResult cr1 = nc1.criteriaResult();
		Boolean SENDED_TO_ERP = null;

		while (cr1.next())
		{
			SENDED_TO_ERP = cr1.getBoolean("SENDED_TO_ERP", false);
		}
		return SENDED_TO_ERP;
	}

	public static Long getWorkerId(Document doc) throws EdmException, UserNotFoundException
	{
		String worker = doc.getFieldsManager().getEnumItemCn("WORKER");
		Long simpleErpWorkerId = DSUser.findByUsername(worker).getExternalName() != null ? Long.valueOf(DSUser.findByUsername(worker).getExternalName()) : null;
		if (simpleErpWorkerId == null)
			throw new EdmException("Brak zdefiniowanego loginu ERP dla u�ytkownika o loginie: " + doc.getFieldsManager().getEnumItemCn("WORKER"));
		return simpleErpWorkerId;
	}

	private static boolean isDP(Document doc, String typId) throws EdmException
	{
		return typId.equals("12042") || (typId.equals("") && doc.getFieldsManager().getEnumItem("FORM_ADVANCE").getId().toString().equals("12042"));
	}

	private static boolean isDK(Document doc, String typId) throws EdmException
	{
		return typId.equals("12041") || (typId.equals("") && doc.getFieldsManager().getEnumItem("FORM_ADVANCE").getId().toString().equals("12041"));
	}

	private static String getAdvanceNr(Document doc, Long advnaceId)
	{
		String year = DateUtils.formatYear(new java.util.Date());
		return advnaceId.toString() + "/" + doc.getId().toString() + "/" + year;
	}

	private static String getCurrencyCode(Map<String, Object> advanceValues)
	{
		return getCurrency(advanceValues, null);
	}
	
	private static String getCurrency(Map<String, Object> advanceValues, String resultColumn)
	{
		String currencyCn = "-";
		try
		{
			String currentyId = ((EnumValues) advanceValues.get("DELEGATE_COSTS_WALUTA")).getSelectedOptions().get(0);
			NativeCriteria nc;
			if (resultColumn == null) {
				nc = DSApi.context().createNativeCriteria("dsg_currency", "d").setProjection(NativeExps.projection().addProjection("cn", "cn"));
			} else {
				nc = DSApi.context().createNativeCriteria("dsg_currency", "d").setProjection(NativeExps.projection().addProjection(resultColumn, resultColumn));
			}
			
			nc.add(NativeExps.eq("id", currentyId));
			CriteriaResult cr = nc.criteriaResult();
			
			while (cr.next())
			{
				if (resultColumn == null) {
					currencyCn = cr.getString("cn", null);
				} else {
					currencyCn = cr.getString(resultColumn, null);
				}
			}
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		return currencyCn;
	}

	private static String getCountryCode(List<Long> countries)
	{
		Map<String, Object> countryValues = new HashMap<String, Object>();
		String countryCn = "-";
		try 
		{
			for (Long cId : countries)
			{
				countryValues = DwrDictionaryFacade.dictionarieObjects.get("DEL_COUNTRY").getValues(cId.toString());
				String countryId = ((EnumValues) countryValues.get("DEL_COUNTRY_COUNTRY")).getSelectedOptions().get(0);
				
				NativeCriteria nc = DSApi.context().createNativeCriteria("dsr_country", "d").setProjection(NativeExps.projection().addProjection("cn", "cn"));
				nc.add(NativeExps.eq("id", countryId));
				CriteriaResult cr = nc.criteriaResult();
				
				while (cr.next())
				{
					countryCn = cr.getString("cn", null);
				}
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return countryCn;
	}

	private static String getOrganizator(List<Long> countries)
	{
		try 
		{
			Map<String, Object> countryValues = new HashMap<String, Object>();
			for (Long cId : (List<Long>) countries)
			{
				countryValues = DwrDictionaryFacade.dictionarieObjects.get("DEL_COUNTRY").getValues(cId.toString());
				return countryValues.get("DEL_COUNTRY_ORGANIZER_NAME").toString();
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return "-";
	}

	public static String prepareDescritpion(List<Long> countries, Document doc)
	{
		try
		{
			Map<String, Object> countryValues = new HashMap<String, Object>();
	
			for (Long cId : (List<Long>) countries)
			{
				countryValues = DwrDictionaryFacade.dictionarieObjects.get("DEL_COUNTRY").getValues(cId.toString());
				String organizerName =  (String)countryValues.get("DEL_COUNTRY_ORGANIZER_NAME");
				String tripPurpose = "Cel wyjazdu: " + doc.getFieldsManager().getValue("TRIP_PURPOSE") != null ? (String) doc.getFieldsManager().getValue("TRIP_PURPOSE") : "-";		
				return organizerName + ", " + tripPurpose;
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return "-";
	}

	private static void sendAdvancesToErp(List<IfpanDelegationAdvance> advances, String dictionaryName, java.util.Date docCTime) throws EdmException
	{
		Connection con = null;
		CallableStatement call = null;

		try
		{
			String erpUser, erpPassword, erpUrl, erpDataBase, erpSchema, procGotowka, procPrzelew;
			erpUser = Docusafe.getAdditionProperty("erp.database.user");
			erpPassword = Docusafe.getAdditionProperty("erp.database.password");
			erpUrl = Docusafe.getAdditionProperty("erp.database.url");
			erpDataBase = Docusafe.getAdditionProperty("erp.database.dataBase");
			erpSchema = Docusafe.getAdditionProperty("erp.database.schema");
			procGotowka = Docusafe.getAdditionProperty("erp.database.procGotowka");
			procPrzelew = Docusafe.getAdditionProperty("erp.database.procPrzelew");
			con = connectToSimpleErpDataBase(erpUser, erpPassword, erpUrl);
			for (IfpanDelegationAdvance delAdvance : advances)
			{
				if (delAdvance.getType().equals(IfpanDelegationAdvance.Type.DK))
					call = con.prepareCall("{? = call [" + erpDataBase + "].[" + erpSchema + "].[" + procGotowka + "] (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }");
				else if (delAdvance.getType().equals(IfpanDelegationAdvance.Type.DP))
					call = con.prepareCall("{? = call [" + erpDataBase + "].[" + erpSchema + "].[" + procPrzelew + "] (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }");

				call.registerOutParameter(1, java.sql.Types.INTEGER);
				call.registerOutParameter(13, java.sql.Types.VARCHAR);

				for (int i = 2; i <= 12; i++)
					call.setObject(i, null);
/*
DECLARE @RC int
DECLARE @user_id numeric(10,0)
DECLARE @pracownik_nrewid numeric(10,0)
DECLARE @numerdok char(20)
DECLARE @data_rej date
DECLARE @datadok date
DECLARE @kwota money
DECLARE @kwotawalbaz money
DECLARE @kurs money
DECLARE @waluta_id bigint
DECLARE @czy_krajowa bigint
DECLARE @opis varchar(60)
DECLARE @result varchar

EXECUTE @RC = [wb_test].[dbo].[up_gsd_DelegacjaGotowka] 
   @user_id
  ,@pracownik_nrewid
  ,@numerdok
  ,@data_rej
  ,@datadok
  ,@kwota
  ,@kwotawalbaz
  ,@kurs
  ,@waluta_id
  ,@czy_krajowa
  ,@opis
  ,@result OUTPUT
GO
 */
				call.setLong(3, delAdvance.getWorker());
				call.setString(4, delAdvance.getAdvanceNr());
				call.setDate(5, delAdvance.getRegisterDate());
				call.setDate(6, new java.sql.Date(docCTime.getTime()));
				call.setBigDecimal(7, delAdvance.getAmount());
				call.setBigDecimal(8, delAdvance.getAmountCurr());
				call.setBigDecimal(9, delAdvance.getCurrencyRate());
				call.setLong(10, delAdvance.getCurrencyErpId());
				call.setLong(11, 2); //delegacja zagraniczna - 2
				call.setString(12, delAdvance.getDescription());

				log.error("ExportAdvanceToSimpleERP: {}", call.execute());
				Integer status = call.getInt(1);
				String msg = call.getString(13);
				log.error("ExportAdvanceToSimpleERP: result: {}", msg);
				log.error("ExportAdvanceToSimpleERP: status : {}", status);

				if (status < 0)
					throw new EdmException("ExportAdvanceToSimpleERP: B��d eksportu zaliczki :" + msg);

				if (dictionaryName != null)
				{
					Map<String, FieldData> addAdvancesValues = new HashMap<String, FieldData>();
					String advanceId = delAdvance.getAdvanceNr().split("/")[1];
					addAdvancesValues.put("id", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, advanceId));
					addAdvancesValues.put("SENDED_TO_ERP", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
					DwrDictionaryFacade.dictionarieObjects.get(dictionaryName).update(addAdvancesValues);
				}
			}

		}
		catch (Exception e)
		{
			log.error(e.getMessage());
			throw new EdmException("ExportAdvanceToSimpleERP: B��d eksportu zaliczki :" + e.getMessage());
		}
		finally
		{
			if (con != null)
			{
				try
				{
//					con.close();
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
					throw new EdmException("ExportAdvanceToSimpleERP: B��d eksportu zaliczki :" + e.getMessage());
				}
			}
		}
	}

	public static void exportRozliczenia(OfficeDocument doc) throws EdmException
	{
		try
		{			
			List<IfpanDelegationAdvance> advances = new ArrayList<IfpanDelegationAdvance>();
			IfpanDelegationAdvance advance = new IfpanDelegationAdvance();
			IfpanDelegationAdvance.Type typ = null;

			if (doc.getFieldsManager().getEnumItem("ZWROT").getId().toString().equals("2201"))
				typ = IfpanDelegationAdvance.Type.DP; // Przelew
			else if (doc.getFieldsManager().getEnumItem("ZWROT").getId().toString().equals("2202"))
				typ = IfpanDelegationAdvance.Type.DK; // Kasa
			
			if (typ == null)
				throw new EdmException("Brak okre�lonej formy rozliczenia IF PAN z Delegowanym");
			List<Long> countries = (List<Long>) doc.getFieldsManager().getKey("DEL_COUNTRY");

			advance.setCountry(getCountryCode(countries));
			
			Map<String, Object> values = new HashMap<String, Object>();
			
			values.put("OTRZYMANE", DelegacjaLogic.getOtrzymane(doc.getFieldsManager(), null, null));
			values.put("WYDANE", DelegacjaLogic.getWydatki(doc.getFieldsManager(), null, null));
			
			List<Rozliczenie> rozliczenia = DelegacjaLogic.getRozliczeniaList(values);
			
			BigDecimal sumRozliczenieWydalemPln = new BigDecimal(0);
			BigDecimal sumRozliczenieDysponowalemPln = new BigDecimal(0);
			BigDecimal sumRozliczeniePozostaloPln = new BigDecimal(0);
			BigDecimal sumRozliczenieBrakujePln = new BigDecimal(0);
			
			Map<String, Rozliczenie> rozliczenia13 = new HashMap<String, Rozliczenie>();
			//Map<String, Rozliczenie> rozliczenia14 = new HashMap<String, Rozliczenie>();
			
			for (Rozliczenie roz : rozliczenia)
			{
				sumRozliczenieWydalemPln = sumRozliczenieWydalemPln.add(roz.getWydalemPln());
				sumRozliczenieDysponowalemPln = sumRozliczenieDysponowalemPln.add(roz.getDysponowalemPln());
				
				if (rozliczenia13.containsKey(roz.getWaluta()))
				{
					Rozliczenie rozz= rozliczenia13.get(roz.getWaluta());
					
					rozz.setWydalem(rozz.getWydalem().add(roz.getWydalem()));
					rozz.setWydalemPln(rozz.getWydalemPln().add(roz.getWydalemPln()));
					rozz.setDysponowalem(rozz.getDysponowalem().add(roz.getDysponowalem()));
					rozz.setDysponowalemPln(rozz.getDysponowalemPln().add(roz.getDysponowalemPln()));
				}
				else
				{
					Rozliczenie rozz =  new Rozliczenie();
					rozz.setWaluta(roz.getWaluta());
					rozz.setWydalem(roz.getWydalem());
					rozz.setWydalemPln(roz.getWydalemPln());
					rozz.setDysponowalem(roz.getDysponowalem());
					rozz.setDysponowalemPln(roz.getDysponowalemPln());

					rozliczenia13.put(roz.getWaluta(), rozz);
				}
			}
			
			BigDecimal  zero = new BigDecimal(0).setScale(2);
			for (String key : rozliczenia13.keySet())
			{
				Rozliczenie roz = rozliczenia13.get(key);
				BigDecimal dysponowlaem = roz.getDysponowalem();
				BigDecimal wydalem = roz.getWydalem();
				BigDecimal rozliczenie = dysponowlaem.subtract(wydalem).setScale(2, RoundingMode.HALF_UP);

				if (rozliczenie.compareTo(BigDecimal.ZERO) == 0)
				{
					roz.setBrakuje(zero);
					roz.setPozostalo(zero);
				}
				else if (rozliczenie.compareTo(BigDecimal.ZERO) == -1)
				{
					roz.setBrakuje(rozliczenie.abs());
					roz.setPozostalo(zero);
				}
				else
				{
					roz.setBrakuje(zero);
					roz.setPozostalo(rozliczenie.abs());
				}
				
				BigDecimal dysponowlaemPln = roz.getDysponowalemPln();
				BigDecimal wydalemPln = roz.getWydalemPln();
				BigDecimal rozliczeniePln = dysponowlaemPln.subtract(wydalemPln).setScale(2, RoundingMode.HALF_UP);
				if (rozliczeniePln.compareTo(BigDecimal.ZERO) == 0)
				{
					roz.setBrakujePln(zero);
					roz.setPozostaloPln(zero);
				}
				else if (rozliczeniePln.compareTo(BigDecimal.ZERO) == -1)
				{
					roz.setBrakujePln(rozliczeniePln.abs());
					roz.setPozostaloPln(zero);
				}
				else
				{
					roz.setBrakujePln(zero);
					roz.setPozostaloPln(rozliczeniePln.abs());
				}
				
				sumRozliczenieBrakujePln = sumRozliczenieBrakujePln.add(roz.getBrakujePln());
				sumRozliczeniePozostaloPln = sumRozliczeniePozostaloPln.add(roz.getPozostaloPln());
			}
			if (doc.getFieldsManager().getBoolean("KOMPENSATA"))
			{
				// pozostalo > brakuje
				if (sumRozliczeniePozostaloPln.abs().setScale(2, RoundingMode.HALF_UP).compareTo(sumRozliczenieBrakujePln.abs().setScale(2, RoundingMode.HALF_UP)) == 1)
				{
					sumRozliczeniePozostaloPln = sumRozliczeniePozostaloPln.abs().subtract(sumRozliczenieBrakujePln.abs());
					sumRozliczenieBrakujePln = BigDecimal.ZERO.setScale(2);
				}
				// pozostalo < brakuje
				else if (sumRozliczeniePozostaloPln.abs().setScale(2, RoundingMode.HALF_UP).compareTo(sumRozliczenieBrakujePln.abs().setScale(2, RoundingMode.HALF_UP)) == -1)
				{
					sumRozliczenieBrakujePln = sumRozliczenieBrakujePln.abs().subtract(sumRozliczeniePozostaloPln.abs());
					sumRozliczeniePozostaloPln = BigDecimal.ZERO.setScale(2);
				}
			}
			advance.setAmount(sumRozliczenieBrakujePln.setScale(2, RoundingMode.HALF_UP));
			advance.setAmountCurr(sumRozliczenieBrakujePln.setScale(2, RoundingMode.HALF_UP));
			advance.setCurrencyRate(BigDecimal.ONE.setScale(4));
			advance.setCurrencyErpId(Long.valueOf(1));
			advance.setDescription("-");
			advance.setOrganizator(getOrganizator(countries));
			advance.setWorker(getWorkerId(doc));
			advance.setType(typ);
			advance.setAdvanceNr(getAdvanceNr(doc, 1l));
			advance.setRegisterDate(new java.sql.Date(new java.util.Date().getTime()));
			
			advances.add(advance);
			
			sendAdvancesToErp(advances, null, doc.getCtime());
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException("B��d przy eksprotowaniu rozliczenia");
		}
	}
}
