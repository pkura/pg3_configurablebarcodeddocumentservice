package pl.compan.docusafe.parametrization.ifpan;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SetEachForkCostCVariables implements ExternalActivityBehaviour
{

	private static final Logger log = LoggerFactory.getLogger(SetEachForkCostCVariables.class);

	@SuppressWarnings("unchecked")
	public void execute(ActivityExecution execution) throws Exception
	{
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Document document = Document.find(docId);
		List<String> argSets = new LinkedList<String>();
		List<Long> dictionaryIds = (List<Long>) document.getFieldsManager().getKey("DSG_CENTRUM_KOSZTOW_FAKTURY");
		// Je�eli dokument zawiera pozycje kosztowe
		if (dictionaryIds != null)
		{
			Integer projectId = null;
			CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance();
			for (Long di : dictionaryIds)
			{
				// pobranie id_projektu z ktorego pokrywany jest koszt
				projectId = mpk.find(di).getCentrumId();//((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(di.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMID")).getSelectedOptions().get(0);
				if (projectId != null && !argSets.contains(projectId.toString()))
				{
					argSets.add(projectId.toString());
					log.info("DokumentKind: {}, DocumentId: {}, Pole: 'DSG_CENTRUM_KOSZTOW_FAKTURY', Id Kosztu: {}', ProjektId: {}'", document.getDocumentKind().getCn(), document.getId(), di, projectId);
				}
			}
		}
		// ids - id projektow
		execution.setVariable("ids", argSets);
		// liczba subtaskow na ptorzeby forka
		execution.setVariable("count", argSets.size());
		log.info("DokumentKind: {}, DocumentId: {},Liczba subtaskow na potrzeby forka: {}", document.getDocumentKind().getCn(), document.getId(),argSets.size());
		// pomocnicza zmienna okreslajac licze akceptacji dyrektorow
		execution.setVariable("count2", 2 );
		execution.setVariable("twoAcc", 2 );
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		// TODO Auto-generated method stub

	}
}
