package pl.compan.docusafe.parametrization.ifpan;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.bouncycastle.crypto.DSA;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.core.record.projects.ProjectEntry;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

public class BiletyLogic extends AbstractDocumentLogic {

	private static final long serialVersionUID = 1L;

	private static Logger log = LoggerFactory.getLogger(BiletyLogic.class);

	private static BiletyLogic instance;
	private String PROJECT_MANAGER = "project-manager";
	
	public static BiletyLogic getInstance() {
		if (instance == null)
			instance = new BiletyLogic();
		return instance;
	}
		
	@Override
	public TaskListParams getTaskListParams(DocumentKind kind, long documentId) throws EdmException {
		TaskListParams params = new TaskListParams();
		try {
			FieldsManager fm = kind.getFieldsManager(documentId);

			// Ustawienie statusy dokumentu
			params.setStatus(fm.getValue("STATUS") != null ? (String) fm.getValue("STATUS") : null);
			// Ustawienie list projektow z ktorych finansowane s� bilety
			setProjectsNumberList(fm, params);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return params;

	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn)
	{
		
		boolean assigned = false;
		String dictionaryField = "ZRODLO_FINANSOWANIA";
		try
		{
			if (accpetionCn.equals(PROJECT_MANAGER))
			{
				List<Long> dictionaryIds = (List<Long>) doc.getFieldsManager().getKey(dictionaryField);

				for (Long di : dictionaryIds)
				{
					String projId  = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get(dictionaryField).getValues(di.toString()).get(dictionaryField + "_PLATNIK")).getSelectedOptions().get(0);
					String userName = Project.find(Long.valueOf(projId)).getProjectManager();

					assignable.addCandidateUser(userName);
					return true;
				}
			}
		}
		catch (Exception ee)
		{
			log.error(ee.getMessage() + ee);
		}
		return assigned;
	}
	
	@Override
	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException {
		if ("sdsd".equals(acceptationCn))
		{
			FieldsManager fm = doc.getFieldsManager();
			Long delegacjaID = fm.getLongKey("INSTRUKCJA_WYJAZDOWA");
			OfficeDocument delegacja = (OfficeDocument) Document.find(delegacjaID, false);
			FieldsManager delegacjaFM = delegacja.getFieldsManager();
			try 
			{
				List<Long> ids = (List<Long>) fm.getKey("ZRODLO_FINANSOWANIA");
				PreparedStatement ps;
				for (Long id : ids)
				{
					ps = DSApi.context().prepareStatement("SELECT * FROM dsg_ifpan_bilety_costs where ID = ?");
					ps.setLong(1, id);
					ResultSet rs = ps.executeQuery();
					rs.next();
					
					String typ = rs.getString("typ");
					if (typ.equals("11026"))
						typ = "11024";
					Long platnik = rs.getLong("PLATNIK");
					BigDecimal kosztSwiadczenia = new BigDecimal(rs.getInt("cost_amount"));
					Long waluta = rs.getLong("waluta");
					Integer iloscBiletow = rs.getInt("amount");
					BigDecimal currencyCost = new BigDecimal(rs.getInt("currency_cost"));
					String projectManag = rs.getString("projectManag");
					
					// wydatkowane srodki
					ps = DSApi.context()
							.prepareStatement("insert into dsg_ifpan_wydatkowane_srodki" +
									"(typ,waluta,stawka,ilosc,procent,amount,description,wyd_platnik,bilet_id) " +
									"values (?, ?, ?, ?, ?, ?, ?, ?, ?");
					ps.setBigDecimal(1, new BigDecimal("19062"));
					ps.setLong(2, waluta);
					ps.setBigDecimal(3, kosztSwiadczenia.setScale(2, RoundingMode.HALF_UP));
					ps.setBigDecimal(4, new BigDecimal(iloscBiletow));
					ps.setBigDecimal(5, new BigDecimal(100).setScale(2));
					ps.setBigDecimal(6, currencyCost);
					ps.setString(7, "Bilet - Nr rezerwacji: " + fm.getStringValue("NR_REZERWACJI"));
					ps.setLong(8, platnik);
					ps.setLong(9, id);
					ps.executeUpdate();
					
					ps = DSApi.context().prepareStatement("select MAX(id) from dsg_ifpan_wydatkowane_srodki");
					rs = ps.executeQuery();
					rs.next();
					Long wydId = rs.getLong(1);
					
					ps = DSApi.context()
							.prepareStatement("insert into dsg_ifpan_delegacja_multiple_value (DOCUMENT_ID,FIELD_CN,FIELD_VAL) values (?,'WYDATKOWANE_SRODKI',?)");
					ps.setLong(1, delegacjaID);
					ps.setLong(2, wydId);
					ps.executeUpdate();
				}
			}
			catch (SQLException e) 
			{
				log.error(e.getMessage(), e);
			}
		}
	}

	@Override
	public void validateAcceptances(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException {
		/*FieldsManager fm = kind.getFieldsManager(documentId);
		Long delegacjaId = fm.getLongKey("INSTRUKCJA_WYJAZDOWA");
		Document delegacja = Document.find(delegacjaId, false, null);
		if (delegacja != null) {
			FieldsManager delegacjaFM = delegacja.getFieldsManager();
			Integer status = delegacjaFM.getIntegerKey("STATUS");
			if (status != null && status != 1410 && status <= 15) {
				throw new EdmException("Instrukcja wyjazdowa czeka na akceptacj� �r�d�a finansowania");
			}
		}*/
	}

	@Override
	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn() + "_DOCUMENT_READ", ObjectPermission.GROUP,
				"Zam�wienie bilet�w - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_READ",
				ObjectPermission.GROUP, "Zam�wienie bilet�w  zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn() + "_DOCUMENT_READ_MODIFY",
				ObjectPermission.GROUP, "Zam�wienie bilet�w  - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_MODIFY",
				ObjectPermission.GROUP, "Zam�wienie bilet�w  zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn() + "_DOCUMENT_READ_DELETE",
				ObjectPermission.GROUP, "Zam�wienie bilet�w  - usuwanie"));
		this.setUpPermission(document, perms);
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Zam�wienie bilet�w");
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(document.getCtime()));
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(document.getCtime()));
		document.setFolder(folder);

		document.setTitle("Zam�wienie bilet�w");
		document.setDescription("Zam�wienie bilet�w");
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
		log.info("--- BiletyLogic : START PROCESS !!! ---- {}", event);
		try {
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage(), e);
		}
	}

	private void setProjectsNumberList(FieldsManager fm, TaskListParams params) {
		try {
			String projectsNumberList = "";
			List<Long> ids = (List<Long>) fm.getKey("ZRODLO_FINANSOWANIA");
			Iterator itr = ids.iterator();
			while (itr.hasNext()) {
				String projectId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("ZRODLO_FINANSOWANIA")
						.getValues(itr.next().toString()).get("ZRODLO_FINANSOWANIA_PLATNIK")).getSelectedOptions().get(0);
				if (projectId != null && !projectId.equals("")) {
					projectsNumberList = Project.find(Long.valueOf(projectId)).getNrIFPAN();
					if (itr.hasNext()) {
						projectsNumberList += "...";
						break;
					}
				}
			}
			params.setAttribute(projectsNumberList, 0);
		} catch (Exception e) {
			log.warn(e.getMessage(), e);
		}
	}

	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		log.info("DokumentKind: {}, DocumentId: {}, Values from JavaScript: {}", fm.getDocumentKind().getCn(), fm.getDocumentId(), values);

		try 
		{
			if (values.get("DWR_INSTRUKCJA_WYJAZDOWA").getDictionaryData() != null 
					&& !"".equals(values.get("DWR_INSTRUKCJA_WYJAZDOWA").getDictionaryData().get("id").getData()))
			{
				try {
					DSApi.openAdmin();
					String delegacjaId = values.get("DWR_INSTRUKCJA_WYJAZDOWA").getDictionaryData().get("id").toString();
					Long oldDelegationId = null;
					try
					{
						oldDelegationId = fm.getLongKey("INSTRUKCJA_WYJAZDOWA");
					}
					catch (Exception e)
					{
						log.warn(e.getMessage(), e);
					}
					if ((delegacjaId!= null  && oldDelegationId == null) || (oldDelegationId != null && !oldDelegationId.toString().equals(delegacjaId)))
					{
						OfficeDocument delegacja = OfficeDocument.find(Long.valueOf(delegacjaId));
						FieldsManager delFm = delegacja.getFieldsManager();
						Integer userId = delFm.getEnumItem("WORKER").getId();
						List<String> selectedUser = new ArrayList<String>();
						selectedUser.add(userId.toString());
						values.get("DWR_PRACOWNIK_DELEGOWANY").getEnumValuesData().setSelectedOptions(selectedUser);
						Integer divisionId = delFm.getEnumItem("WORKER_DIVISION").getId();
						List<String> selectedDivision = new ArrayList<String>();
						selectedDivision.add(divisionId.toString());
						values.get("DWR_WORKER_DIVISION").getEnumValuesData().setSelectedOptions(selectedDivision);
						Date startDate = (Date) delFm.getKey("START_DATE");
						values.get("DWR_START_DATE").setDateData(startDate);
						Date finishDate = (Date) delFm.getKey("FINISH_DATE");
						values.get("DWR_FINISH_DATE").setDateData(finishDate);
						String tripPurpose = (String) delFm.getKey("TRIP_PURPOSE");
						values.get("DWR_TRIP_PURPOSE").setStringData(tripPurpose);
					}
					
					BigDecimal suma = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);
					Map<String, FieldData> ticketCosts = values.get("DWR_ZRODLO_FINANSOWANIA").getDictionaryData();
					if (ticketCosts != null) {
						for (String cn : ticketCosts.keySet()){
							if(cn.contains("CURRENCY_COST_") && ticketCosts.get(cn).getData() != null)
								suma = suma.add(ticketCosts.get(cn).getMoneyData());
						}
						values.get("DWR_KOSZT").setMoneyData(suma.setScale(2, RoundingMode.HALF_UP));
					}
					
				} catch (EdmException e) {
					log.error(e.getMessage(), e);
				} finally {
					DSApi._close();
				}
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return null;
	}


	@SuppressWarnings("finally")
	@Override
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) {
		
		Map<String, Object> results = new HashMap<String, Object>();
		try {

			if (dictionaryName.equals("ZRODLO_FINANSOWANIA"))
			{
				BigDecimal costsCount = ((FieldData) values.get("ZRODLO_FINANSOWANIA_AMOUNT")).getMoneyData();
				BigDecimal oneCostAmount = ((FieldData) values.get("ZRODLO_FINANSOWANIA_COST_AMOUNT")).getMoneyData();
				BigDecimal calculatedCosts = oneCostAmount.multiply(costsCount);
				results.put("ZRODLO_FINANSOWANIA_CURRENCY_COST", calculatedCosts);
				
				String projectId = ((FieldData) values.get("ZRODLO_FINANSOWANIA_PLATNIK")).getEnumValuesData().getSelectedOptions().get(0);
				if (!projectId.equals(""))
				{
					String projectManager = Project.find(Long.parseLong(projectId)).getProjectManager();
					results.put("ZRODLO_FINANSOWANIA_PROJECTMANAG", projectManager);
				}
			}
		} catch (NullPointerException e)
		{
			log.error("lapiemy:NullPointerException: " + e.getMessage(), e);
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			return results;
		}
	}
}
