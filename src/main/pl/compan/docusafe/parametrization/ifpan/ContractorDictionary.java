package pl.compan.docusafe.parametrization.ifpan;

import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.hibernate.SQLQuery;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.db.criteria.NativeProjection;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.PersonDictionary;
import pl.compan.docusafe.core.dockinds.field.AbstractDictionaryField;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.field.DocumentField;
import pl.compan.docusafe.core.dockinds.field.NonColumnField;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.util.DSCountry;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class ContractorDictionary extends DwrDictionaryBase {

	protected static final String ERP_FLAG = " [erp]";
	private static Logger log = LoggerFactory.getLogger(ContractorDictionary.class);	
	
	@Override
	public int remove(String id) throws EdmException {
		if (id != null && id.contains("-")) {
			return -1;
		}
		return super.remove(id);
	}

	@Override
	public int update(Map<String, FieldData> values) throws EdmException {
		PreparedStatement ps = null;

		String tableName = "";

		if (getOldField() instanceof DictionaryField)
		{
			tableName = ((DictionaryField) getOldField()).getTable();
		}
		else if (getOldField() instanceof DocumentField)
		{
			tableName = ((DocumentField) getOldField()).getTable();
		}

		if (isCantUpdate())
			return -1;
		
		int i = 1;
		if (!values.containsKey("id") && !values.containsKey("ID"))
			throw new NullPointerException("No id key found in values map");
		String sId = null;
		
		if (values.containsKey("id"))
			sId = "id";
		else
			sId = "ID";
		
		
		if (values.get(sId) != null && values.get(sId).getStringData().contains("-")) {
			try {
				String id = values.get(sId).getStringData().substring(1);
				values.remove(sId);
				values.put(getName()+"_WPARAM", new FieldData(Field.Type.LONG, new Long(id)));
				values.put(sId,new FieldData(Field.Type.STRING,String.valueOf(super.add(values))));
				values.put(getName()+"_ID",new FieldData(Field.Type.STRING,values.get(sId).getStringData()));
				return 1;
			} catch (Exception e) {
				return -1;
			}
		}
		
		StringBuilder query = new StringBuilder("update ").append(tableName).append(" set ");
		
		FieldData fieldId = values.get(sId);
		Object id = values.get(sId).getData();
		values.remove(sId);
		
		int size = values.size();
		for (String cn : values.keySet())
		{
			if (values.get(cn).getData() == null)
			{
				if (i == size)
				{
					int indexOf = query.toString().lastIndexOf(",");
					if ((indexOf + 2) == (query.length()))
						query = new StringBuilder(query.toString().substring(0, indexOf));
				}
				i++;
				continue;
			}
			query.append(cn.replace(getName() + "_", "")).append("=").append("?");
			if (i++ != size)
				query.append(", ");
		}
		
		query.append(" where id=").append(id);
		
		log.debug(query.toString());
		try
		{
			ps = DSApi.context().prepareStatement(query);
			int psi = 1;
			for (String cn : values.keySet())
			{
				if (values.get(cn).getData() == null)
				{
					i++;
					continue;
				}
				FieldData fd = values.get(cn);
				if (fd.getData() != null)
					fd.getData(ps, psi);
				psi++;
			}
			values.put(sId, fieldId);
			return ps.executeUpdate();
		}
		catch (SQLException e)
		{
			log.error(e.getMessage(), e);
			return -1;
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
	}

	@Override
	public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset) throws EdmException {
		List<Map<String, Object>> ret = Lists.newArrayList();
		String tableName;
		String id = "id";
		
		tableName = ((DictionaryField) getOldField()).getTable();

		if (values != null && values.get("id") != null && StringUtils.isNotEmpty(values.get("id").toString()) && values.get("id").toString().contains("-")) {
			//createPersonFromChoosenErpId(values);
			//przeniesione do validateDwr
		} else {
			runErpPersonProcedure(values);
			if (values == null || values.get("id") == null || StringUtils.isEmpty(values.get("id").toString())) {
				values.put(getName()+"_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
				values.put(getName()+"_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
			}
		}
		
		NativeCriteria nc = DSApi.context().createNativeCriteria(tableName, "d");
		
		NativeProjection np = NativeExps.projection();
		for (pl.compan.docusafe.core.dockinds.field.Field f : getOldField().getFields())
		{
			String column = f.getColumn();

			if ((!(f instanceof NonColumnField) && !f.getCn().equals("ID") && !f.getCn().equals("ID_1")))
				np.addProjection("d." + column.replace("_1",""), column);
		}
		np.addProjection("d." + id, "id");
		nc.setProjection(np);
		
		
		List<Object> parametersValues = new ArrayList<Object>();
		
		for (pl.compan.docusafe.core.dockinds.field.Field f : getOldField().getFields())
		{
			String column = f.getColumn();
			String fCn = f.getCn().toUpperCase();

			//nazwa pola
			String nameCn = new StringBuilder(getName()).append("_").append(fCn).toString().replace("_1", "");


			if (values.containsKey(nameCn) && !(f instanceof NonColumnField) && values.get(nameCn).getData() != null)
			{
				if (values.get(nameCn).getType().equals(Field.Type.ENUM) 
						|| values.get(nameCn).getType().equals(Field.Type.ENUM_AUTOCOMPLETE)
						|| values.get(nameCn).getType().equals(Field.Type.INTEGER)
						|| values.get(nameCn).getType().equals(Field.Type.LONG))
						{
							if(!values.get(nameCn).getData().equals(NULLABLE)){
								nc.add(NativeExps.eq(column, values.get(nameCn).getData()));
								parametersValues.add(values.get(nameCn).getData());
							} else {
								nc.add(NativeExps.isNull(column));
							}
						}
				else if (!values.get(nameCn).getStringData().equals("") && !(column.equals("ID")))
				{
					try {
						String valueDate = (String) values.get(nameCn).getData();
						if (column.equalsIgnoreCase("nip"))
							valueDate = valueDate.replace("-", "").replace(" ", "");

						nc.add(NativeExps.like(column.replace("_1", ""), "%" + valueDate + "%"));
						parametersValues.add("%" + valueDate + "%");
						
					}
					catch (Exception e)
					{
						log.debug(e);
					}
				}
			}
		}
		// ograniczenie za pomoc� atrybutu not-nullable-fields
		if (this.getOldField() instanceof AbstractDictionaryField && ((AbstractDictionaryField)this.getOldField()).getNotNullableFields() != null) {
			for (String notNullField : ((AbstractDictionaryField)this.getOldField()).getNotNullableFields()){
				if (StringUtils.isNotEmpty(notNullField)) {
					nc.add(NativeExps.isNotNull(notNullField));
				}
			}
		}
		
		if (values.containsKey("id") && !(values.get("id").getData().toString()).equals("")) {
			nc.add(NativeExps.eq("d." + id, values.get("id").getData()));
			parametersValues.add(values.get("id").getData());
		}

		SQLQuery query = nc.buildCriteriaQuery();
		
		String queryString = query.getQueryString();
		
		for (String param : query.getNamedParameters()) {
			queryString=queryString.replace(":"+param, "?");
		}
		
		StringBuilder unionQuery = new StringBuilder(" UNION select CAST(e.country_id as varchar(5)) as country, NULL as title, NULL as firstname, NULL as lastname, e.organization+'"+ERP_FLAG+"' as organization, NULL as organizationDivision, e.street as street, e.zip as zip, e.location as location, NULL as email, NULL as fax, e.nip as nip, NULL as pesel, NULL as regon, NULL as DISCRIMINATOR, NULL as ANONYMOUS, NULL as BASEPERSONID, e.erp_id as wparam, '-'+CAST(e.erp_id as varchar(50)) as id from dsg_ifpan_erp_person e WHERE ");
		
		String searchOrganization = values.get(getName()+"_ORGANIZATION") != null ? values.get(getName()+"_ORGANIZATION").toString() : null;
		String searchNip = values.get(getName()+"_NIP") != null ? values.get(getName()+"_NIP").toString() : null;
		String searchCountry = values.get(getName()+"_COUNTRY") != null ? values.get(getName()+"_COUNTRY").toString() : null;

//		where (@dostawcanazwa is  null or @dostawcanazwa = '' OR UPPER ( k.nazwa ) like '%' + UPPER ( @dostawcanazwa ) + '%' )
//		AND (@nip is  null or @nip = '' or REPLACE (k.nip , '-' , '' ) like '%' + @nip + '%')
//		AND (@kraj_nazwa is  null or @kraj_nazwa = '' or UPPER ( kraj.nazwa ) like '%' + UPPER ( @kraj_nazwa) + '%')

		String orderBy = "organization";
        if (isMultiple())
            orderBy = orderBy + "_1";
		StringBuilder mainResultQuery = new StringBuilder("select * from (select *, ROW_NUMBER() OVER (ORDER BY ").append(orderBy).append(" asc) AS RowNum from(");
		if (StringUtils.isNotEmpty(searchNip) && searchNip.length()>5 && StringUtils.isEmpty(searchOrganization) && StringUtils.isEmpty(searchCountry)) {
			unionQuery.append("REPLACE (e.nip , '-' , '' ) like '%"+searchNip+"%'");
			mainResultQuery.append(queryString).append(unionQuery).append(" ) as person) as per where per.RowNum BETWEEN "+ (offset+1)+" AND "+(offset+limit));
		} else if (StringUtils.isEmpty(searchNip) && StringUtils.isNotEmpty(searchOrganization) && StringUtils.isNotEmpty(searchCountry)) {
			unionQuery.append("UPPER ( e.organization ) like '%' + UPPER ('"+ searchOrganization +"') + '%'");
			unionQuery.append(" AND e.country_id = "+searchCountry);
			mainResultQuery.append(queryString).append(unionQuery).append(" ) as person) as per where per.RowNum BETWEEN "+ (offset+1)+" AND "+(offset+limit));
		} else if (values.get("id") == null || StringUtils.isEmpty(values.get("id").toString())) {
			mainResultQuery.append(queryString).append(" ) as person) as per where per.RowNum BETWEEN "+ (offset+1)+" AND "+(offset+limit));
		} else {
			mainResultQuery.append(queryString).append(" ) as person) as per");
		}
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = DSApi.context().prepareStatement(mainResultQuery.toString());

			for (int i=0;i<parametersValues.size();i++) {
				ps.setObject(i+1, parametersValues.get(i));
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				Map<String, Object> row = Maps.newLinkedHashMap();
				
				row.put("id", rs.getString("id"));
				for (pl.compan.docusafe.core.dockinds.field.Field f : getOldField().getFields())
				{
					String fCn = f.getCn();
					String column = f.getColumn();

                    Object a = null;
                    if (!fCn.startsWith("ID_"))
                        a = rs.getObject(column);
					if ((!isMultiple() || !fCn.startsWith("ID_")) && !(f instanceof NonColumnField) && rs.getObject(column) != null)
					{
						row.put(getName().toUpperCase() + "_" + fCn.toUpperCase().replace("_1", ""), rs.getObject(column));
					}
				}
				ret.add(row);
				
			}
		} catch (SQLException e) {
			throw new EdmSQLException(e);
		} finally {
			DSApi.context().closeStatement(ps);
			DbUtils.closeQuietly(rs);
		}
		
		return ret;
	}

	@Override
	public long add(Map<String, FieldData> values)
	{
		// add wywoa�ane w momencie zapisywania dokumentu
		if (values.get("DISCRIMINATOR") != null && "PERSON".equals(values.get("DISCRIMINATOR").getStringData()) &&
				values.get("ANONYMOUS") != null	&& "0".equals(values.get("ANONYMOUS").getStringData())) {
			Person person = new Person();
			Sender sender = new Sender();
			PersonDictionary.setPersonValue(null, person, values);
			try {
				person.create();
				PersonDictionary.setPersonValue(null, sender, values);
				sender.setBasePersonId(person.getId());
				sender.setDictionaryType(Person.DICTIONARY_SENDER);
				sender.create();
			} catch (EdmException e1) {
				log.error(e1.getMessage());
				return -1;
			}

			return sender.getId();
			
			// wywo�ywane w momencie tworzenie nowego wpisu z poziomu popup'a
		} else {
			Person person = new Person();
			PersonDictionary.setPersonValue(getName(), person, values);
			try {
				DSApi.context().begin();
				person.create();
				DSApi.context().commit();
			} catch (EdmException e1) {
				log.error(e1.getMessage());
				return -1;
			}

			return person.getId();
		}
	}

	public List<ErpPerson> findPersonInErp(String organization, String countryId, String nip) throws EdmException
	{
		List<ErpPerson> foundContractor = new ArrayList<ErpPerson>();
		Connection con = null;
		CallableStatement call = null;
		String countryCn = null;
		try {
			if (countryId != null && DSCountry.find(Integer.valueOf(countryId), null, null)!=null) {
				countryCn = DSCountry.find(Integer.valueOf(countryId), null, null).get(0).getCn();
			}
			String erpUser, erpPassword, erpUrl, erpDataBase, erpSchema, procWeryfikacjaDostawcy;
			erpUser = Docusafe.getAdditionProperty("erp.database.user");
			erpPassword = Docusafe.getAdditionProperty("erp.database.password");
			erpUrl = Docusafe.getAdditionProperty("erp.database.url_MSSQL");//"jdbc:sqlserver://localhost:1433";
			erpDataBase = Docusafe.getAdditionProperty("erp.database.dataBase");
			erpSchema = Docusafe.getAdditionProperty("erp.database.schema");
			// [up_gsd_szukaj_dostawcy]
			procWeryfikacjaDostawcy = Docusafe.getAdditionProperty("erp.database.procSzukajDostawcy");
			
			con = IfpanUtils.connectToSimpleErpDataBase(erpUser, erpPassword, erpUrl, "sqlserver_2008");
			
//			EXEC   @return_value = [dbo].[up_gsd_szukaj_dostawcy]
//                    @dostawcanazwa = N'',
//                    @nip = N'''''',
//                    @kraj_nazwa = N'',
//                    @wynik = @wynik OUTPUT
			call = con.prepareCall("{? = call [" + erpDataBase + "].[" + erpSchema + "].[" + procWeryfikacjaDostawcy + "] (?,?,?,?)}");
			call.registerOutParameter(1, java.sql.Types.NUMERIC);
			
			if (StringUtils.isNotEmpty(nip)) {
				call.setString(3, nip.replace("-", "").replace(" ",""));
				call.setString(2, null);	
				call.setString(4, null);	
			} else if (StringUtils.isNotEmpty(organization) && StringUtils.isNotEmpty(countryCn)) {
				call.setString(2, organization);	
				call.setString(4, countryCn);	
				call.setString(3, null);
			} else {
				call.setString(2, null);	
				call.setString(4, null);	
				call.setString(3, null);
			}
			
			call.registerOutParameter(5, java.sql.Types.SQLXML);
			
			boolean queryResult;
			boolean updatingErp = false;
			try {
				queryResult = call.execute();
				
				if (!queryResult) {
					Object result = call.getObject(5);
					if (result != null && result instanceof SQLXML) {
						SQLXML xmlResult = (SQLXML)result;
						InputStream stream = xmlResult.getBinaryStream();
						org.dom4j.Document doc = new SAXReader().read(stream);
						
						for(Element el : (Collection<Element>)doc.getRootElement().elements("dostawca")){
							Long erpId = null;
							if (StringUtils.isNotEmpty(el.attributeValue("dostawca_id"))) {
								try {
									erpId = Long.valueOf(el.attributeValue("dostawca_id"));
									ErpPerson contractor;
									if (ErpPerson.findByErpId(erpId) != null) {
										updatingErp = true;
										DSApi.context().begin();
										contractor = ErpPerson.findByErpId(erpId);
									} else {
										contractor = new ErpPerson();
									}
									
									contractor.setErpId(Long.valueOf(el.attributeValue("dostawca_id")));
									contractor.setOrganization(el.attributeValue("dostawca_nazwa"));
									if (StringUtils.isNotEmpty(el.attributeValue("kraj_kod")) && DSCountry.find(null, el.attributeValue("kraj_kod"), null) != null) {
										contractor.setCountryId(DSCountry.find(null, el.attributeValue("kraj_kod"), null).get(0).getId());
									}
									contractor.setStreet(el.attributeValue("ulica"));
									contractor.setLocation(el.attributeValue("miasto"));
									contractor.setZip(el.attributeValue("zip"));
									contractor.setNip(el.attributeValue("nip"));
									contractor.save();
									
									if (updatingErp) {
										DSApi.context().commit();
										updatingErp = false;
									}
									foundContractor.add(contractor);
								} catch (NumberFormatException e) {
									log.error("ID dostawcy z simple.erp nie jest liczb�");
								}
							}
						}
					}
					
				} else throw new EdmException("B��d w znalezieniu dostawcy");
				
			} catch (Exception e) {
				if (updatingErp) {
					DSApi.context().rollback();
				}
				log.error(e.getMessage(),e);
				throw e;
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new EdmException("B��d w znalezieniu dostawcy :" + e.getMessage());
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					log.error(e.getMessage(), e);
					throw new EdmException("B��d w znalezieniu dostawcy:" + e.getMessage());
				}
			}
		}
		return foundContractor;
		
	}
	
	public void runErpPersonProcedure(Map<String, FieldData> values) {
		try {
			String searchOrganization = values.get(getName()+"_ORGANIZATION") != null ? values.get(getName()+"_ORGANIZATION").toString() : null;
			String searchNip = values.get(getName()+"_NIP") != null ? values.get(getName()+"_NIP").toString() : null;
			String searchCountry = values.get(getName()+"_COUNTRY") != null ? values.get(getName()+"_COUNTRY").toString() : null;

			if (StringUtils.isNotEmpty(searchNip) || (StringUtils.isNotEmpty(searchOrganization) && StringUtils.isNotEmpty(searchCountry))) {
				findPersonInErp(searchOrganization, searchCountry, searchNip);
			}
		} catch (EdmException e) {
			log.error(e.toString());
		}
	}

	public void createPersonFromChoosenErpId(Map<String, FieldData> values) {
		try {
			Long id = Long.valueOf(values.get("id").toString().substring(1));

			ErpPerson erpContractor = ErpPerson.findByErpId(id);
			if (erpContractor != null) {
				Person contractor = new Person();
				erpContractor.updatePersonFields(contractor);
				contractor.create();

				Sender sender = new Sender();
				erpContractor.updatePersonFields(sender);
				sender.setDictionaryGuid("rootdivision");
				sender.setDictionaryType(Person.DICTIONARY_SENDER);
				sender.setBasePersonId(contractor.getId());
				sender.create();

				values.get("id").setStringData(sender.getId().toString());
				//values.put("CONTRACTOR_ID", new FieldData(Field.Type.STRING,sender.getId().toString()));
			}

		} catch (NumberFormatException e) {
			log.error(e.toString());
		} catch (EdmException e1) {
			log.error(e1.toString());
		}
	}
}
