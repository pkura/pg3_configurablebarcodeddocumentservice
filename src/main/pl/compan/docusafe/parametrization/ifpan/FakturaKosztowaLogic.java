package pl.compan.docusafe.parametrization.ifpan;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContextFactory;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.LogicUtils;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.*;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

import pl.compan.docusafe.webwork.event.ActionType;

public class FakturaKosztowaLogic extends AbstractIfpanDocumentLogic
{
	private static final String CONTRACTOR_FIELD_NAME = "CONTRACTOR";
	private static Logger log = LoggerFactory.getLogger(FakturaKosztowaLogic.class);
	private static FakturaKosztowaLogic instance;
    private static final StringManager sm =
            GlobalPreferences.loadPropertiesFile(FakturaKosztowaLogic.class.getPackage().getName(), null);
	/*
	 * Lista akceptacji specjalnych
	 */
    public final static String DWR_ADDINFO = "DWR_ADDINFO";
    public final static String DWR_KURS = "DWR_KURS";
    public final static String DWR_OPIS_TOWARU = "DWR_OPIS_TOWARU";
    public final static String DWR_DSG_CENTRUM_KOSZTOW_FAKTURY = "DWR_DSG_CENTRUM_KOSZTOW_FAKTURY";
    public final static String DWR_DSG_CENTRUM_KOSZTOW_FAKTURY_AMOUNT = "DSG_CENTRUM_KOSZTOW_FAKTURY_AMOUNT";
    public final static String DWR_DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMID = "DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMID";

    public final static String DPIR_PERSON = "dpirPerson";
	public final static String ACCEPTATION = "acceptation";
	public final static String CORRECTION = "correction";
	public final static String DELIVERYDIV = "delivery_division";
	public final static String COUNTRY_TABLE_NAME = "dsr_country";
	private final String MAJATEK = "akceptacja_majatku";
	private final String RESTDIR = "restDirs";

	public static FakturaKosztowaLogic getInstance()
	{
		if (instance == null)
			instance = new FakturaKosztowaLogic();
		return instance;
	}

    @Override
    public List<String> getActionMessage(ActionType source, Document doc) {
        List<String> messages = new ArrayList<String>();
        try {
            OfficeDocument od = OfficeDocument.find(doc.getId());
            FieldsManager fm = od.getFieldsManager();
            fm.initialize();
            fm.initializeValues();
            String format = DateFormat.getDateInstance().format(new Date());
            String nrSprawy = "NF/"+od.getOfficeNumber()+"/"+format.substring(0, 4);
            Map<String, Object> toReload = new HashMap<String, Object>();
            toReload.put("NUMER_FAKTURY_KO", nrSprawy);
            doc.getDocumentKind().setOnly(doc.getId(), toReload);
            switch (source) {
                case OFFICE_CREATE_DOCUMENT:
                    messages.add(sm.getString("PrzyjetoPismoONrSprawy", nrSprawy));
                    break;
                default:
                    break;
            }
        }catch(Exception e)
        {
            log.error(e.getMessage(), e);
        }
        return messages;
    }

	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
	{		

        if (fieldValues.get("KWOTA_NETTO") != null && fieldValues.get("KWOTA_BRUTTO") != null && fieldValues.get("DSG_CENTRUM_KOSZTOW_FAKTURY") != null) {
            checkSumCosts(fieldValues);
        }
		
		if (fieldValues.get(CONTRACTOR_FIELD_NAME) != null) {
			tryCreateSenderPersonOnSaveDoc(kind, documentId, fieldValues, CONTRACTOR_FIELD_NAME);
		}
	}

    private void checkSumCosts(Map<String, ?> fieldValues) throws EdmException
    {
        BigDecimal brutto = (BigDecimal) fieldValues.get("KWOTA_BRUTTO");
        BigDecimal netto = (BigDecimal) fieldValues.get("KWOTA_NETTO");
        BigDecimal sumCentrum = new BigDecimal(0);
        List<Long> dictionaryIds = (List<Long>) fieldValues.get("DSG_CENTRUM_KOSZTOW_FAKTURY");
        BigDecimal oneAmount = null;
        for (Long di : dictionaryIds)
        {
            oneAmount = new BigDecimal((Double) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(di.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_AMOUNT"));
            sumCentrum = sumCentrum.add(oneAmount);
        }
        if (netto.abs().compareTo(sumCentrum.abs()) > 0 || brutto.abs().compareTo(sumCentrum.abs()) < 0) {
            throw new EdmException(sm.getString("SumaMpkWiekszaNizNettoMniejszaNizBrutto"));
        }

    }

    public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException {
        if (acceptationCn.equals("dpzie"))
        {
            checkSumCpv(doc);

        }
    }
	private boolean checkSumNettoFromCpv(Map<String, ?> values){
		Map<String, Object> centrumy = (Map<String, Object>) values.get("M_DICT_VALUES");
		BigDecimal brutto = (BigDecimal) values.get("KWOTA_NETTO");
		brutto.setScale(2, brutto.ROUND_HALF_UP);
		BigDecimal suma = BigDecimal.ZERO;
		suma.setScale(2, suma.ROUND_HALF_UP);
		for(String key: centrumy.keySet()){
			if(key.contains("CPVNR_1")){
				Map<String, Object> centrum = (Map<String, Object>) centrumy.get(key);
				FieldData fd = (FieldData) centrum.get("KWOTA_NETTO");
				BigDecimal kwota = fd.getMoneyData();
				if (kwota != null) {
					kwota.setScale(2, kwota.ROUND_HALF_UP);
					suma = suma.add(kwota);
				} else
					return true;
			}
		}
		log.error("!@#$:" + suma + ";" + brutto + ";" + suma.setScale(2, suma.ROUND_UP).equals(brutto));
		if(suma.setScale(2, suma.ROUND_UP).equals(brutto)) return true;
		return false;
	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn)
	{
		boolean assigned = false;
		try
		{
			log.info("DokumentKind: {}, DokumentId: {}', acceptationCn: {}, enumCn: {}", doc.getDocumentKind().getCn(), doc.getId(), accpetionCn, enumCn);
			
			// akceptacja dzia?u zaopatrzenia
			if (accpetionCn.equals(DELIVERYDIV))
				assigned = assigneToDeliverDivisionAcceptance(doc, assignable, openExecution, accpetionCn, enumCn);
			
			// akceptacja kosztu
			else if (accpetionCn.equals(ACCEPTATION))
				assigned = assigneToAcceptationAcceptance(doc, assignable, openExecution, accpetionCn, enumCn);
			
			// akcepacja maj?tku trwa?ego
			else if (accpetionCn.equals(MAJATEK))
				assigned = assigneToMajatekTrwaly(doc, assignable, openExecution);
			
			// akceptacja drugiego dyrektora
			else if (accpetionCn.equals(RESTDIR))
				assigned = assigneToRestDirs(doc, assignable, openExecution);
			
			// przekanie do poprawy autora
			else if (accpetionCn.equals(CORRECTION))
			{
				assignable.addCandidateUser(doc.getAuthor());
				log.info("DokumentKind: {}, DocumentId: {}, Assigne to {}", doc.getDocumentKind().getCn(), doc.getId(), doc.getAuthor());
				assigned = true;
			}
			else if(accpetionCn.equals(DPIR_PERSON)){
				String zrodloId = (String) openExecution.getVariable("secCostCenterId");
				Project project = Project.find(Long.valueOf(zrodloId));
				if(project != null && project.getResponsiblePerson() != null){
					assignable.addCandidateUser(project.getResponsiblePerson().getName());
					assigned = true;
				} else {
					assignable.addCandidateUser("aradomyska");
					assigned = true;
				}
				
			}
		}
		catch (Exception ee)
		{
			log.error(ee.getMessage(), ee);
		}
		return assigned;
	}
	
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		log.info("DokumentKind: {}, DocumentId: {}, Values from JavaScript: {}", fm.getDocumentKind().getCn(), fm.getDocumentId(), values);
		
		try 
		{
			// automatycznie wyliczanie kwoty brutto najko sumy wartosci pozycji kosztowych - nie wiem po co to jest;/??
			//if (values.get("DWR_DSG_CENTRUM_KOSZTOW_FAKTURY").getDictionaryData() != null)
			//	setBrutto(values);
	
			// automatycznie wylcizanie kwoty netto na podstawie wzor: KwotaWWalucie*Kurs
			if (values.get("DWR_KWOTA_W_WALUCIE").getMoneyData()!= null && values.get("DWR_KURS").getData()!= null)
				setNetto(values);



			// autoamtycznie wylcizenie kwoty brutto wg wzoru: netto+vat
			if (values.get("DWR_KWOTA_NETTO").getData() != null)
				setBruttoAsNettoPlusVat(values);
			
			StringBuilder procedureMsg = new StringBuilder();
		
			if (values.get("DWR_"+CONTRACTOR_FIELD_NAME)!= null && values.get("DWR_"+CONTRACTOR_FIELD_NAME).getDictionaryData() != null) {
				setContractorFlag(values, CONTRACTOR_FIELD_NAME);
				createPersonFromChoosenErpId(values, CONTRACTOR_FIELD_NAME);
			}

            Long docId = fm.getDocumentId();

            Object addinfo = values.get(DWR_ADDINFO);
            Object kurs = values.get(DWR_KURS);
            Object opisTowaru = values.get(DWR_OPIS_TOWARU);
            Object mpk2 = values.get(DWR_DSG_CENTRUM_KOSZTOW_FAKTURY);

            boolean allOk = true;
            //sprawdzenie czy wszystkie pola opcjonalne zosta?y wype?nione
            if
                    (
                        values.get(DWR_ADDINFO)!=null &&
                        values.get(DWR_KURS)!=null &&
                        values.get(DWR_OPIS_TOWARU)!=null &&
                        values.get(DWR_DSG_CENTRUM_KOSZTOW_FAKTURY)!=null
                    )
            {
                String addInfoString = ((FieldData) values.get(DWR_ADDINFO)).getStringData();
                int len = addInfoString.length();
                BigDecimal kursBigDecimal = ((FieldData) values.get(DWR_KURS)).getMoneyData();
                Map<String, FieldData> mpk = ((FieldData) values.get(DWR_DSG_CENTRUM_KOSZTOW_FAKTURY)).getDictionaryData();
                String opisTow = ((FieldData) values.get(DWR_OPIS_TOWARU)).getStringData();
                int len2 = opisTow.length();

                if(len > 0 && len2 > 0 && kursBigDecimal!=null)
                {

                for(int i = 1; i <= mpk.keySet().size() / 7; i++)
                {
                    FieldData bd = mpk.get(DWR_DSG_CENTRUM_KOSZTOW_FAKTURY_AMOUNT+"_"+i);
                    FieldData fd2 = mpk.get(DWR_DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMID+"_"+i);
                    String test = fd2.getEnumValuesData().getSelectedValue();
                    if(bd==null && fd2==null || bd.getMoneyData()==null || fd2.getEnumValuesData().getSelectedValue()==null || fd2.getEnumValuesData().getSelectedValue().equals(""))//nie wszystkie pola wypelnione
                    {
                        allOk=false;
                    }
                }

                }
            }
            if(allOk)
            {
                boolean isOpened = true;

                if (!DSApi.isContextOpen()) {
                    isOpened = false;
                    DSApi.openAdmin();
                }

                HashMap<String, Object> mapa2 = new HashMap<String, Object>();
                mapa2.put("WSZYSTKIE_WYPELNIONE", new Boolean(true));
                fm.getDocumentKind().setOnly(fm.getDocumentId(), mapa2);

                if (DSApi.isContextOpen() && !isOpened) {
                    try {
                        DSApi.close();
                    } catch (EdmException e) {
                        log.error(e.getMessage(), e);
                    }
                }
            }
            else
            {
                boolean isOpened = true;

                if (!DSApi.isContextOpen()) {
                   isOpened = false;
                   DSApi.openAdmin();
                }

                HashMap<String, Object> mapa2 = new HashMap<String, Object>();
                mapa2.put("WSZYSTKIE_WYPELNIONE", new Boolean(false));
                fm.getDocumentKind().setOnly(fm.getDocumentId(), mapa2);

                if (DSApi.isContextOpen() && !isOpened) {
                    try {
                        DSApi.close();
                    } catch (EdmException e) {
                        log.error(e.getMessage(), e);
                    }
                }
            }

			if (!procedureMsg.toString().equals("")) {
			    values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
			    return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", procedureMsg.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		} finally {
		    DSApi.close();
		}
		
		return null;
	}

	static void checkContractor(Map<String, FieldData> values, String dwrPrefix, StringBuilder procedureMsg) throws EdmException {
		String nipString;
		
		FieldData contractorFields = values.get("DWR_CONTRACTOR");
		Map<String, FieldData> contractor = values.get("DWR_CONTRACTOR").getDictionaryData();
		FieldData nip = contractor.get(dwrPrefix+"NIP");
		
		if (nip != null) {
		    	nipString=nip.toString().replace("-", "").replace(" ", "");
		    	   	 
		    	if (nipString.length()>0 && ((!Character.isDigit(nipString.substring(0,1).toCharArray()[0]) && nipString.length()>9) || (Character.isDigit(nipString.substring(0,1).toCharArray()[0]) && nipString.length()==10))) {
		    		Person p = null;
		    		try {
		    		    HttpServletRequest req = WebContextFactory.get().getHttpServletRequest();
		    		    DSApi.open(AuthUtil.getSubject(req));
		    		    p = Person.findPersonByNip(nipString);
		    		} catch (Exception e) {
		    		    log.error("",e);
		    		    DSApi.close();
		    		} 
		    		
		    		//aktualizacja wy?wietlanych p?l zgodnie z warto?ciami z bazy wg podanego nr nip
		    		if (p != null && p.getWparam() != null) {
		    		    FieldData temp;
		    		    Map<String,FieldData> contractorMod = new HashMap<String,FieldData>();
		    		    for (String field:contractor.keySet()){
		    			try {
		    			    temp = contractor.get(field);
		    			    temp.setData(p.getParam(field,dwrPrefix));
		    			    contractorMod.put(field,temp);
		    			} catch (EdmException e) {
		    			    log.error("",e);
		    			}
		    		    }
		    		    contractorFields.setDictionaryData(contractorMod);
		    		    values.put("DWR_CONTRACTOR",contractorFields);
		    		    checkRequiredFields(contractor, procedureMsg);
		    		    
		    		  
		    		//tworzymy Person'y wg danych z procedury
		    		} else if (p ==null) {
		    		    try {
		    			Map<String,Object> externalContractor = IfpanUtils.verifyContractor(nipString);
		    			
		    			if (!externalContractor.get("wparam").equals((long) 0)) {
				    			DSApi.context().begin();
				    			Person newContractor = new Person();
				    			newContractor.fromMap(externalContractor);
				    			newContractor.create();
				    			DSApi.context().commit();
				    			
							Sender sender = new Sender();
							sender.fromMap(newContractor.toMap());
							sender.setWparam((Long)externalContractor.get("wparam"));
							sender.setDictionaryGuid("rootdivision");
							sender.setDictionaryType(Person.DICTIONARY_SENDER);
							sender.setBasePersonId(newContractor.getId());
							sender.create();
							 // TODO nie powinien byc zamiast sender newContractor?
		                    updateContractorFields(values, contractor, contractorFields, dwrPrefix, procedureMsg, externalContractor, newContractor);
							procedureMsg.append("Pobrano dane kontrahenta z bazy simple.erp oraz utworzono nowy wpis do s?ownika. ");
		    			}
		    		    } catch (Exception e) {
		    			log.error("",e);
		    		    } finally {
		    		    	checkRequiredFields(contractor, procedureMsg);
		    		    }
		    		    
		    		//aktualizacja wszystkich Person'ow o danym nip'ie
		    		} else if (p != null && p.getWparam() == null)
                    {
		    		    List<Person> personList = Person.findAllByNip(nipString);
		    		    try {
		    			Map<String,Object> externalContractor = IfpanUtils.verifyContractor(nipString);
		    			if (!externalContractor.get("wparam").equals((long)0)) {
				    			DSApi.context().begin();
				    			Person newContractor = Person.findByNip(nipString);
				    			for (Person pItem:personList) {
				    			    pItem.fromMap(externalContractor);
				    			    pItem.update();
				    			}
						
				    			DSApi.context().commit();
		                        updateContractorFields(values, contractor, contractorFields, dwrPrefix, procedureMsg, externalContractor, newContractor);
					    		procedureMsg.append("Zaaktualizowane dane kontrahenta wg danych z bazy simple.erp. ");
		    			}
		    		    } catch (Exception e) {
		    			log.error("",e);
		    		    } finally {
		    		    	checkRequiredFields(contractor, procedureMsg);
		    		    }
		    		}
		    	}
		}
	}

	static void checkRequiredFields(Map<String, FieldData> contractor, StringBuilder procedureMsg) {
		FieldData temp;
		boolean isUpdatingMsg = false;
		for (String field : contractor.keySet()) {
		 	temp = contractor.get(field);
		 	if (temp != null) {
		 		 if (temp.getData() == null && !isUpdatingMsg) {
		  			procedureMsg.append("Prosz� uzupe�ni� obowiazkowe pola istniej�cego wpisu przy u�ycie s�ownika. ");
		  			isUpdatingMsg = true;
		  	    }
		 	}
		}
	}

	static void updateContractorFields(Map<String, FieldData> values, Map<String, FieldData> contractor, FieldData contractorFields, String dwrPrefix,
			StringBuilder procedureMsg, Map<String, Object> externalContractor, Person newContractor) {
		FieldData temp;
		for (String field : externalContractor.keySet()) {
		 	temp = contractor.get(dwrPrefix + field.toUpperCase());
		 	if (temp != null) {
		 	    temp.setData(externalContractor.get(field));
		 	    contractor.put(dwrPrefix + field.toUpperCase(), temp);
		 	}
		}
		temp = contractor.get("id");
		temp.setData(newContractor.getId());
		contractor.put("id", temp);
		
		FieldData selectedCountry = contractor.get(dwrPrefix+"COUNTRY");
		selectedCountry.setData(contractor.get(dwrPrefix+"COUNTRY"));
		contractor.put(dwrPrefix + "COUNTRY", selectedCountry);
 	   
		contractorFields.setDictionaryData(contractor);
		values.put("DWR_CONTRACTOR",contractorFields);
	}
	

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn() + "_DOCUMENT_READ", ObjectPermission.GROUP, "Faktura kosztowa - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Faktura kosztowa zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn() + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Faktura kosztowa - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Faktura kosztowa zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn() + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Faktura kosztowa - usuwanie"));
		Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
		perms.addAll(documentPermissions);
		this.setUpPermission(document, perms);
	}

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Faktury kosztowe");
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(fm.getValue("DATA_WYSTAWIENIA")));
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(fm.getValue("DATA_WYSTAWIENIA")));
		document.setFolder(folder);

		document.setTitle("Faktura kosztowa nr " + String.valueOf(fm.getValue("NUMER_FAKTURY")));
		document.setDescription("Faktura kosztowa nr " + String.valueOf(fm.getValue("NUMER_FAKTURY")));
		if(fm.containsField("CONTRACTOR"))
		{
			Long contractorId = (Long)fm.getKey("CONTRACTOR");
			Person p = new Person();
			Person contractor = p.find(contractorId);
			((OfficeDocument)document).setSender((Sender)contractor);
		}
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event)  throws EdmException
	{
		log.info("--- FakturaKosztowaLogic : START PROCESS !!! ---- {}", event);
        if (document.getFieldsManager().getKey("KWOTA_NETTO") != null)
        {
            BigDecimal netto = (BigDecimal) document.getFieldsManager().getKey("KWOTA_NETTO");
            if (netto.compareTo(new BigDecimal(500)) == 1)
                throw new EdmException(sm.getString("fakturaKosztowaMuciBycPowiazanaZZapotrzebowaniem"));
        }

        Map<String, Object> map = Maps.newHashMap();
        map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
        map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
        document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);


        DSApi.context().watch(URN.create(document));

        Long journalId = Long.parseLong(Docusafe.getAdditionProperty("costinvoice_journal_id"));
        Date entryDate = new Date();

        Integer sequenceId = Journal.TX_newEntry2(journalId, document.getId(), entryDate);

        ((InOfficeDocument) document).bindToJournal(journalId, sequenceId);
    }

	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}
	
	public void specialSetDictionaryValues(Map<String, ?> dockindFields)
	{
		log.info("Special dictionary values: {}", dockindFields);
		if (dockindFields.keySet().contains("CONTRACTOR_DICTIONARY"))
		{
			Map<String, FieldData> m = (Map<String, FieldData>) dockindFields.get("CONTRACTOR_DICTIONARY");
			m.put("DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			m.put("ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}
	
	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		log.info("Before Search - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.contains(CONTRACTOR_FIELD_NAME))
		{
			values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
		if (dictionaryName.contains("CPVNR"))
		{
		    if (values.get("id") != null && !values.get("id").equals(""))
			return;
			values.put(dictionaryName + "_BASE", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.INTEGER, 1));
		}
		//super.addSpecialSearchDictionaryValues(dictionaryName, values);
	}

	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		try
		{
			Document doc = Document.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			
			// Document_ID
			values.put("DOCUMENT_ID", doc.getId());
			
			// Set numerSprawy - potrzbny tylko dla FK/Z
			values.put("caseNumber", "");
			
			// Ustawienie zmiennej date
			values.put("DATE",  DateUtils.formatCommonDate(new java.util.Date()).toString());
			
			// ustawienie terminu platnosci sqlDateFormat
			values.put("TERMIN_PLATNOSCI", DateUtils.formatSqlDate((java.util.Date)fm.getKey("TERMIN_PLATNOSCI")));			
			
			// ustawienie zmiennych ZRODLA
			setProjectsManager(fm, values);
			
			// ustawienie zmiennej CPV
			setCPVNumbers(fm, values);
			
			// ustawienie zmiennej DESCRIPTION
			String desc = fm.getValue("DESCRIPTION") != null ? (String) doc.getFieldsManager().getValue("DESCRIPTION") : "";
			
			values.put("DESCRIPTION", desc);
			
			// ustawienie zmiennej BUY_KIND
			String buyKind = fm.getEnumItem("BUY_KIND") != null ? doc.getFieldsManager().getEnumItem("BUY_KIND").getTitle() : "";
			values.put("BUY_KIND", buyKind);
			
			// Dane kontrahenta
			setContractor(fm, values);
			
			// Nazwa inwentarza, numer, Osoba odpowiedzialna
			setInwentarz(fm, values);
			
			// Zestawienie akceptacji
			for (DocumentAcceptance docAcc : DocumentAcceptance.find(docId))
				values.put(docAcc.getAcceptanceCn(), DSUser.safeToFirstnameLastname(docAcc.getUsername()));
			
			// zmiana ze wzgledu na poprzednie wersje procesu
			if (values.containsKey("cost_registration"))
				values.put("dpir", values.get("cost_registration"));
            if (!values.containsKey("fields"))
            {
                DocFacade df= Documents.document(docId);
                values.put("fields", df.getFields());
            }
		}
		catch (EdmException e)
		{
			log.error(e.getMessage());
		}
	}

	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			FieldsManager fm = dockind.getFieldsManager(id);
			
			// Ustawienie statusy dokumentu
			params.setStatus(fm.getValue("STATUS") != null ? (String) fm.getValue("STATUS") : null);
			
			// Ustawienie list projektow z ktorych pokyrwana jest faktura
			setProjectsNumberList(fm, params);
			
			// Ustawienie numeru faktury jako numery dokumentu
			params.setDocumentNumber(fm.getValue("NUMER_FAKTURY") != null ? (String) fm.getValue("NUMER_FAKTURY") : null);
			
			// Ustawienie Nazwy kontraktora
			if (fm.getKey(CONTRACTOR_FIELD_NAME) != null)
			{
				String organizator = (String) DwrDictionaryFacade.dictionarieObjects.get(CONTRACTOR_FIELD_NAME).getValues(fm.getKey(CONTRACTOR_FIELD_NAME).toString()).get("CONTRACTOR_ORGANIZATION");
				params.setAttribute(organizator != null ? organizator : "", 1);
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return params;
	}
	
	private void setInwentarz(FieldsManager fm, Map<String, Object> values)
	{
		ArrayList<Inwentarz> inwentarze = new ArrayList<Inwentarz>();
		try
		{
			List<Long> ids = (List<Long>) fm.getKey("DSG_CENTRUM_KOSZTOW_FAKTURY");
			Iterator itr = ids.iterator();

			for (Long id : ids)
			{
				CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(id);
				Integer majatekId = mpk.getRealAcceptingCentrumId();
				if (majatekId != null)
				{
					EnumItem item = DataBaseEnumField.getEnumItemForTable("dsg_ifpan_rodzaj_majatku_trwalego", majatekId.intValue());
					String inwentarz = (String) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(id.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_INWENTARZ");
					List<DocumentAcceptance> accList = DocumentAcceptance.find(fm.getDocumentId(), null, "akceptacja_majatku", majatekId.longValue());
					if (accList.size() > 0)
					{
						DocumentAcceptance docAcc = accList.get(0);
						inwentarze.add(new Inwentarz(item.getTitle(), inwentarz, DSUser.findByUsername(docAcc.getUsername()).asFirstnameLastname()));
					}
					else
					{
						String acceptCn = DataBaseEnumField.getEnumItemForTable("dsg_ifpan_rodzaj_majatku_trwalego", majatekId).getCn();
						for (AcceptanceCondition accept : AcceptanceCondition.find(acceptCn))
						{
							if (accept.getUsername() != null)
							{
								inwentarze.add(new Inwentarz(item.getTitle(), inwentarz, DSUser.findByUsername(accept.getUsername()).asFirstnameLastname()));
								break;
							}
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
		finally
		{
			if (inwentarze.size() == 0)
				inwentarze.add(new Inwentarz("Brak", "Brak", "Brak"));
			values.put("INWENTARZ", inwentarze);
		}
	}

	private boolean assigneToDeliverDivisionAcceptance(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn) throws EdmException
	{
		boolean assigned = false;
		List<String> argSets = new LinkedList<String>();
		List<Long> dictionaryIds = (List<Long>) doc.getFieldsManager().getKey("DSG_CENTRUM_KOSZTOW_FAKTURY");
		String projectId = null;
		for (Long di : dictionaryIds)
		{
			// pobranie id_projektu z ktorego pokrywany jest koszt
			projectId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(di.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMID")).getSelectedOptions().get(0);
			if (!argSets.contains(projectId))
			{
				argSets.add(projectId);
				log.info("DokumentKind: {}, DocumentId: {}, Pole: 'DSG_CENTRUM_KOSZTOW_FAKTURY', Id Kosztu: {}', ProjektId: {}'", doc.getDocumentKind().getCn(), doc.getId(), di, projectId);
			}
		}
		// ids - id projektow
		openExecution.setVariable("ids", argSets);
		// liczba subtaskow na ptorzeby forka
		openExecution.setVariable("count", argSets.size());
		log.info("DokumentKind: {}, DocumentId: {},Liczba subtaskow na potrzeby forka: {}", doc.getDocumentKind().getCn(), doc.getId(), argSets.size());

		for (AcceptanceCondition accept : AcceptanceCondition.find(accpetionCn))
		{
			if (accept.getUsername() != null)
			{
				assignable.addCandidateUser(accept.getUsername());
				assigned = true;
			}
			
			if (accept.getDivisionGuid() != null)
			{
				assignable.addCandidateGroup(accept.getDivisionGuid());
				assigned = true;
			}
		}
		return assigned;
	}

	private boolean assigneToAcceptationAcceptance(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn) throws NumberFormatException, EdmException
	{
		log.info("DokumentKind: {}, DocumentId: {}, OpenExecutionVariable: costCenterId, Wartosc: {}", doc.getDocumentKind().getCn(), doc.getId(), openExecution.getVariable("costCenterId"));
		String centrumId = openExecution.getVariable("costCenterId").toString();
		String userName = Project.find(Long.valueOf(centrumId)).getProjectManager();
		assignable.addCandidateUser(userName);
		log.info("DokumentKind: {}, DocumentId: {}, Assigne to {}", doc.getDocumentKind().getCn(), doc.getId(), userName);
		return true;
	}

	private void setCPVNumbers(FieldsManager fm, Map<String, Object> values)
	{
		String cpvCns = "";
		try
		{
			List<Long> ids = (List<Long>)fm.getKey("CPVNR");
			Iterator itr = ids.iterator();
			while (itr.hasNext())
			{
				String cpvCn = (String) DwrDictionaryFacade.dictionarieObjects.get("CPVNR").getValues(itr.next().toString()).get("CPVNR_CN");
				cpvCns += cpvCn;
				if (itr.hasNext())
					cpvCns += ", ";
			}
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
		finally
		{
			if (cpvCns.length() == 0)
				cpvCns = "Brak";
			values.put("CPV", cpvCns);
		}
	}

	private void setProjectsManager(FieldsManager fm, Map<String, Object> values)
	{
		List <IfpanCentrumKosztow> centrums = new ArrayList<IfpanCentrumKosztow>();
		String projectsNumerList = "";
		try
		{
			List<Long> ids = (List<Long>) fm.getKey("DSG_CENTRUM_KOSZTOW_FAKTURY");
			Iterator itr = ids.iterator();
			Map <String, Object> dicValues = null;
			while (itr.hasNext())
			{
				IfpanCentrumKosztow centrum = new IfpanCentrumKosztow();
				dicValues = DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(itr.next().toString());
				String pojId = ((EnumValues)dicValues.get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMID")).getSelectedOptions().get(0);
				String nrIfpan = Project.find(Long.valueOf(pojId)).getNrIFPAN();
				
				if (!projectsNumerList.contains(nrIfpan))
				{
					if (projectsNumerList.length() > 0)
						projectsNumerList += ", ";
					projectsNumerList += nrIfpan;
				}
				
				centrum.setProjekt(nrIfpan);
				String inwentarz = (String)dicValues.get("DSG_CENTRUM_KOSZTOW_FAKTURY_INWENTARZ");
				centrum.setInwentarz(inwentarz);
				String kategoria = ((EnumValues)dicValues.get("DSG_CENTRUM_KOSZTOW_FAKTURY_ACCOUNTNUMBER")).getSelectedValue();
				centrum.setKategoria(kategoria);
				String ksiegowaKategoria = ((EnumValues)dicValues.get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMCODE")).getSelectedValue();
				centrum.setKsiegowaKategoria(ksiegowaKategoria);
				BigDecimal kwota = new BigDecimal((Double)dicValues.get("DSG_CENTRUM_KOSZTOW_FAKTURY_AMOUNT")).setScale(2,RoundingMode.HALF_UP);
				centrum.setKwota(kwota);
				String majatek = ((EnumValues)dicValues.get("DSG_CENTRUM_KOSZTOW_FAKTURY_ACCEPTINGCENTRUMID")).getSelectedValue();
				centrum.setMajatek(majatek);
				DSUser user = DSUser.findByUsername(Project.find(Long.valueOf(pojId)).getProjectManager());
				if (user != null)
				{
					centrum.setAkcept(user.asFirstnameLastname());
				}
				centrums.add(centrum);
			}
		}
		catch (EdmException e)
		{
			log.warn(e.getMessage(), e);
		}
		finally
		{
			values.put("ZRODLA", centrums);
			values.put("PROJECTS", projectsNumerList);
		}
	}

	private void setProjectsNumberList(FieldsManager fm, TaskListParams params)
	{
		try
		{
			String projectsNumerList = "";
			List<Long> ids = (List<Long>) fm.getKey("DSG_CENTRUM_KOSZTOW_FAKTURY");
			if(ids != null){
				Iterator itr = ids.iterator();
				while (itr.hasNext())
				{
					String projectId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(itr.next().toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMID")).getSelectedOptions().get(0);
					if (projectId != null && !projectId.equals(""))
					{
						projectsNumerList = Project.find(Long.valueOf(projectId)).getNrIFPAN();
						if (itr.hasNext())
						{
							projectsNumerList += "...";
							break;
						}
					}
				}
			}
			params.setAttribute(projectsNumerList, 0);
		}
		catch (EdmException e)
		{
			log.warn(e.getMessage(), e);
		}
	}

	private void setContractor(FieldsManager fm, Map<String, Object> values)
	{
		try
		{
			Long personId = (Long) fm.getKey(CONTRACTOR_FIELD_NAME);
			Person oragnization = Person.find(personId.longValue());
			values.put(CONTRACTOR_FIELD_NAME, oragnization);
		}
		catch (EdmException e)
		{
			log.warn(e.getMessage(), e);
		}
	}
	
	private boolean assigneToRestDirs(OfficeDocument doc, Assignable assignable, OpenExecution openExecution) throws EdmException
	{
		boolean assigned = false;
		log.info("DokumentKind: {}, DocumentId: {}, OpenExecutionVariable: majatekId, Wartosc: {}", doc.getDocumentKind().getCn(), doc.getId(), openExecution.getVariable("majatekId"));

		String acceptCn = doc.getFieldsManager().getEnumItem("AKCEPTACJA_WYDATKOW").getCn();
		for (AcceptanceCondition accept : AcceptanceCondition.find(acceptCn))
		{
			if (accept.getUsername() != null)
			{
				assignable.addCandidateUser(accept.getUsername());
				assigned = true;
				log.info("DokumentKind: {}, DocumentId: {}, Assigne to {}", doc.getDocumentKind().getCn(), doc.getId(), accept.getUsername());
			}
			if (accept.getDivisionGuid() != null)
			{
				assignable.addCandidateGroup(accept.getDivisionGuid());
				assigned = true;
				log.info("DokumentKind: {}, DocumentId: {}, Assigne to {}", doc.getDocumentKind().getCn(), doc.getId(), accept.getDivisionGuid());
			}
		}
		return assigned;
	}

	private boolean assigneToMajatekTrwaly(OfficeDocument doc, Assignable assignable, OpenExecution openExecution) throws EdmException
	{
		boolean assigned = false;
		log.info("DokumentKind: {}, DocumentId: {}, OpenExecutionVariable: majatekId, Wartosc: {}", doc.getDocumentKind().getCn(), doc.getId(), openExecution.getVariable("majatekId"));
		String majatekId = openExecution.getVariable("majatekId").toString();
		String acceptCn = DataBaseEnumField.getEnumItemForTable("dsg_ifpan_rodzaj_majatku_trwalego", Integer.parseInt(majatekId)).getCn();
		for (AcceptanceCondition accept : AcceptanceCondition.find(acceptCn))
		{
			if (accept.getUsername() != null)
			{
				assignable.addCandidateUser(accept.getUsername());
				assigned = true;
				log.info("DokumentKind: {}, DocumentId: {}, Assigne to {}", doc.getDocumentKind().getCn(), doc.getId(), accept.getUsername());
			}
			if (accept.getDivisionGuid() != null)
			{
				assignable.addCandidateGroup(accept.getDivisionGuid());
				assigned = true;
				log.info("DokumentKind: {}, DocumentId: {}, Assigne to {}", doc.getDocumentKind().getCn(), doc.getId(), accept.getDivisionGuid());
			}
		}
		return assigned;
	}
	
	private void setBruttoAsNettoPlusVat(Map<String, FieldData> values)
	{
		BigDecimal netto = values.get("DWR_KWOTA_NETTO").getMoneyData();
		if (values.get("DWR_VAT").getData() != null)
		{
			BigDecimal brutto = values.get("DWR_VAT").getMoneyData();
			brutto = brutto.add(netto).setScale(2, RoundingMode.HALF_UP);
			values.get("DWR_KWOTA_BRUTTO").setMoneyData(brutto.setScale(2, RoundingMode.HALF_UP));
		}
	}
	


	private void setNetto(Map<String, FieldData> values)
	{
		BigDecimal kwotaWWalucie = values.get("DWR_KWOTA_W_WALUCIE").getMoneyData();
		BigDecimal kurs = (BigDecimal)values.get("DWR_KURS").getData();
		BigDecimal netto = kwotaWWalucie.multiply(kurs).setScale(2, RoundingMode.HALF_UP);
		values.get("DWR_KWOTA_NETTO").setMoneyData(netto.setScale(2, RoundingMode.HALF_UP));
	}

    private void checkSumCpv(Document doc) throws EdmException {
        FieldsManager fm = doc.getFieldsManager();
        BigDecimal netto = (BigDecimal) fm.getKey("KWOTA_NETTO");
        BigDecimal cpvSum = BigDecimal.ZERO.setScale(2);
        List<Map<String, Object>> cpvs = (List<Map<String, Object>>) fm.getValue("CPVNR");
        if (cpvs != null && !cpvs.isEmpty())
        {

            for (Map<String, Object> cpv : cpvs)
            {
                if (cpv.get("CPVNR_KWOTA_NETTO") != null)
                {
                    BigDecimal cpvNetto = (BigDecimal) cpv.get("CPVNR_KWOTA_NETTO");
                    cpvSum = cpvSum.add(cpvNetto);
                }
            }

        }
        if (netto.compareTo(cpvSum) != 0)
            throw new EdmException(sm.getString("SumaCpvRowneNetto"));
    }
    
	public void onLoadData(FieldsManager fm) throws EdmException {
		LogicUtils.setPopupButtonsOnPersonDictionaryByPermission(fm, CONTRACTOR_FIELD_NAME);			
	}
}
