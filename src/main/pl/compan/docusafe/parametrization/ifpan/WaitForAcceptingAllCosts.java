package pl.compan.docusafe.parametrization.ifpan;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.jbpm4.Jbpm4SignalListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.jbpm4.SaveAttachmentOnDiskListener;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.service.tasklist.RefreshTaskList;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class WaitForAcceptingAllCosts implements ExternalActivityBehaviour
{

	private final static Logger LOG = LoggerFactory.getLogger(SaveAttachmentOnDiskListener.class);

	@Override
	public void execute(ActivityExecution openExecution) throws Exception
	{
		boolean isOpened = true; 
		try
		{
			if (!DSApi.isContextOpen())
			{   
				isOpened = false; 
				DSApi.openAdmin();    
			}
			
		OfficeDocument doc = Jbpm4Utils.getDocument(openExecution);
		FieldsManager fm = doc.getFieldsManager();
		Long delegacjaID = fm.getLongKey("INSTRUKCJA_WYJAZDOWA");
		OfficeDocument delegacja = OfficeDocument.find(delegacjaID);
		FieldsManager delegacjaFm = delegacja.getFieldsManager();
		delegacjaFm.initialize();
		Integer status = delegacjaFm.getEnumItem("STATUS").getId();
		if (status != null && status != 1410 && status <= 15) {
			openExecution.waitForSignal();
		}
		else
		{
			try 
			{
				List<Long> ids = (List<Long>) fm.getKey("ZRODLO_FINANSOWANIA");
				PreparedStatement ps;
				ResultSet rs;
				for (Long id : ids)
				{
					ps = DSApi.context().prepareStatement("SELECT * FROM dsg_ifpan_bilety_costs where ID = ?");
					ps.setLong(1, id);
					rs = ps.executeQuery();
					rs.next();
					
					String typ = rs.getString("typ");
					if (typ.equals("11026"))
						typ = "11024";
					Long platnik = rs.getLong("PLATNIK");
					BigDecimal kosztSwiadczenia = rs.getBigDecimal("cost_amount");
					Long waluta = rs.getLong("waluta");
					Integer iloscBiletow = rs.getInt("amount");
					BigDecimal currencyCost = rs.getBigDecimal("currency_cost");
					String projectManag = rs.getString("projectManag");
					
					ps = DSApi.context()
							.prepareStatement("insert into dsg_ifpan_delegate_costs(typ,platnik,cost_amount,waluta,amount,currency_cost,projectManag,ADVANCE_KIND,bilet_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
					ps.setString(1, typ);
					ps.setLong(2, platnik);
					ps.setBigDecimal(3, kosztSwiadczenia);
					ps.setLong(4, waluta);
					ps.setInt(5, iloscBiletow);
					ps.setBigDecimal(6, currencyCost);
					ps.setString(7, projectManag);
					ps.setLong(8, 12043);
					ps.setLong(9, id);
					ps.executeUpdate();
					
					ps = DSApi.context().prepareStatement("select MAX(id) from dsg_ifpan_delegate_costs");
					rs = ps.executeQuery();
					rs.next();
					Long costId = rs.getLong(1);
					
					ps = DSApi.context()
							.prepareStatement("insert into dsg_ifpan_delegacja_multiple_value (DOCUMENT_ID,FIELD_CN,FIELD_VAL) values (?,'DELEGATE_COSTS',?)");
					ps.setLong(1, delegacjaID);
					ps.setLong(2, costId);
					ps.executeUpdate();
				}
				
				ps = DSApi.context().prepareStatement("select ID_ from JBPM4_EXECUTION ex join JBPM4_VARIABLE va on ex.DBID_ = va.EXECUTION_ " +
						"where va.KEY_ = 'doc-id' and va.LONG_VALUE_ = ? and ex.ACTIVITYNAME_ = 'checkBiletyStatus'");
				ps.setLong(1, delegacjaID);
				rs = ps.executeQuery();
				rs.next();
				String delegacjaActivityId = rs.getString(1);
				
				Jbpm4Provider.getInstance().getExecutionService().signalExecutionById(
						delegacjaActivityId);
			}
			catch (SQLException e) 
			{
				LOG.error(e.getMessage(), e);
				//throw new EdmException(e.getMessage());
			}
			
			openExecution.take("success");
			
			
		}
		}
		catch (Exception e)
		{
			LOG.error(e.getMessage(), e);
		}
		finally
		{
			if (DSApi.isContextOpen() && !isOpened) 
			{ 
				DSApi.close();    
			}
		}
	}

	@Override
	public void signal(ActivityExecution openExecution, String arg1, Map<String, ?> arg2) throws Exception
	{
		boolean isOpened = true; 
		try
		{
			if (!DSApi.isContextOpen())
			{   
				isOpened = false; 
				DSApi.openAdmin();    
			}
			
			OfficeDocument doc = Jbpm4Utils.getDocument(openExecution);
			FieldsManager fm = doc.getFieldsManager();
			Long delegacjaID = fm.getLongKey("INSTRUKCJA_WYJAZDOWA");
			OfficeDocument delegacja = OfficeDocument.find(delegacjaID);
			FieldsManager delegacjaFm = delegacja.getFieldsManager();
			delegacjaFm.initialize();
			Integer status = delegacjaFm.getEnumItem("STATUS").getId();
			try 
			{
				List<Long> ids = (List<Long>) fm.getKey("ZRODLO_FINANSOWANIA");
				PreparedStatement ps;
				for (Long id : ids)
				{
					ps = DSApi.context().prepareStatement("SELECT * FROM dsg_ifpan_bilety_costs where ID = ?");
					ps.setLong(1, id);
					ResultSet rs = ps.executeQuery();
					rs.next();
					
					String typ = rs.getString("typ");
					if (typ.equals("11026"))
						typ = "11024";
					Long platnik = rs.getLong("PLATNIK");
					BigDecimal kosztSwiadczenia = rs.getBigDecimal("cost_amount");
					Long waluta = rs.getLong("waluta");
					Integer iloscBiletow = rs.getInt("amount");
					BigDecimal currencyCost = rs.getBigDecimal("currency_cost");
					String projectManag = rs.getString("projectManag");
					
					ps = DSApi.context()
							.prepareStatement("insert into dsg_ifpan_delegate_costs(typ,platnik,cost_amount,waluta,amount,currency_cost,projectManag,ADVANCE_KIND,bilet_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
					ps.setString(1, typ);
					ps.setLong(2, platnik);
					ps.setBigDecimal(3, kosztSwiadczenia);
					ps.setLong(4, waluta);
					ps.setInt(5, iloscBiletow);
					ps.setBigDecimal(6, currencyCost);
					ps.setString(7, projectManag);
					ps.setLong(8, 12043);
					ps.setLong(9, id);
					ps.executeUpdate();
					
					ps = DSApi.context().prepareStatement("select MAX(id) from dsg_ifpan_delegate_costs");
					rs = ps.executeQuery();
					rs.next();
					Long costId = rs.getLong(1);
					
					ps = DSApi.context()
							.prepareStatement("insert into dsg_ifpan_delegacja_multiple_value (DOCUMENT_ID,FIELD_CN,FIELD_VAL) values (?,'DELEGATE_COSTS',?)");
					ps.setLong(1, delegacjaID);
					ps.setLong(2, costId);
					ps.executeUpdate();
				}
			}
			catch (SQLException e) 
			{
				LOG.error(e.getMessage(), e);
				//throw new EdmException(e.getMessage());
			}
			
			openExecution.take("success");
		}
		catch (Exception e)
		{
			LOG.error(e.getMessage(), e);
		}
		finally
		{
			if (DSApi.isContextOpen() && !isOpened) 
			{ 
				DSApi.close();    
			}
		}
		
	}
}
