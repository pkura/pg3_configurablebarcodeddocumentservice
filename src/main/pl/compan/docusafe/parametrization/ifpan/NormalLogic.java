package pl.compan.docusafe.parametrization.ifpan;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.PersonDictionary;
import pl.compan.docusafe.core.dockinds.field.DocumentPersonField;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.PersonDirectoryAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.core.office.*;

import java.util.HashMap;
import java.util.Map;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.*;

public class NormalLogic extends AbstractDocumentLogic
{

	private static final String RECIPIENT_CN = "RECIPIENT";
	private static final String SENDER_CN = "SENDER";
	
	private static NormalLogic instance;
	protected static Logger log = LoggerFactory.getLogger(NormalLogic.class);

	public static NormalLogic getInstance()
	{
		if (instance == null)
			instance = new NormalLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk�y- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk�y - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk�y - usuwanie"));
		this.setUpPermission(document, perms);
	}

	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		log.info("type: {}", type);
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("TYPE", type);
		DSUser user = DSApi.context().getDSUser();
		fm.reloadValues(values);
	}
	
	public Map<String, String> setInitialValuesForReplies(Long inDocumentId) throws EdmException
	{
		Map<String, String> dependsDocs = new HashMap<String, String>();
		Long recipientChildId = OfficeDocument.find(inDocumentId).getSender().getId();
		Long recipientBaseId = OfficeDocument.find(inDocumentId).getSender().getBasePersonId();
		Long recipientId;
		
		recipientId = (recipientBaseId != null && Person.findIfExist(recipientBaseId) != null)?recipientBaseId:recipientChildId;

		dependsDocs.put("RECIPIENT", String.valueOf(recipientId));
		//czy user ma swoj dzial
		if (DSApi.context().getDSUser().getDivisionsWithoutGroup().length>0) {
			String sender = "u:"+DSApi.context().getDSUser().getName();
			sender+=(";d:"+DSApi.context().getDSUser().getDivisionsWithoutGroup()[0].getGuid());
			dependsDocs.put("SENDER_HERE", sender);	
		}
		return dependsDocs;
	}

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		as.useFolderResolver(document);
		as.useAvailableProviders(document);
		log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			/*if (document instanceof InOfficeDocument)
			{
				map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
			}
			else
			{*/
				map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
				map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			//}
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}
	/*
	 * private final static OcrSupport OCR_SUPPORT =
	 * OcrUtils.getFileOcrSupport( new File(Docusafe.getHome(), "ocr-dest"),
	 * new File(Docusafe.getHome(), "ocr-get"), Collections.singleton("tiff")
	 * );
	 * 
	 * @Override public OcrSupport getOcrSupport() { return OCR_SUPPORT; }
	 */
}
