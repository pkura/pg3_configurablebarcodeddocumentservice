package pl.compan.docusafe.parametrization.opzz;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;
import static pl.compan.docusafe.parametrization.opzz.OpzzCn._CASES_FOLDER;
import static pl.compan.docusafe.parametrization.opzz.OpzzCn._DOCS_FOLDER;
import static pl.compan.docusafe.parametrization.opzz.OpzzCn._DS_OPZZ_CASES_CN;
import static pl.compan.docusafe.parametrization.opzz.OpzzCn._DS_OPZZ_DOC_CN;
import static pl.compan.docusafe.parametrization.opzz.OpzzCn._TYTUL_CN;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Maps;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.registry.EntryHistoryAction.WorkHistoryBean;
import pl.compan.docusafe.webwork.event.ActionEvent;

import static pl.compan.docusafe.parametrization.opzz.OpzzCn.*;

public class OpzzQuestLogic extends AbstractDocumentLogic
{
	private final static Logger log = LoggerFactory.getLogger(OpzzQuestLogic.class);
	public static OpzzQuestLogic instance;
	public static OpzzQuestLogic getInstance()
	{
		if(instance == null)
			instance = new OpzzQuestLogic();
		return instance;
	}
	
	@Override
	public void documentPermissions(Document document) throws EdmException
	{
		Set<PermissionBean> perms = new HashSet<PermissionBean>();
		
		perms.add(new PermissionBean(ObjectPermission.READ, "OPZZ_QUEST_READ", ObjectPermission.GROUP, "Zadania sprawy - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "OPZZ_QUEST_READ_ATT_READ", ObjectPermission.GROUP, "Zadania sprawy za��cznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "OPZZ_QUEST_READ_MODIFY", ObjectPermission.GROUP, "Zadania sprawy - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "OPZZ_QUEST_READ_ATT_MODIFY", ObjectPermission.GROUP, "Zadania sprawy zal�cznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "OPZZ_QUEST_READ_DELETE", ObjectPermission.GROUP, "Zadania sprawy - usuwanie"));
		
		createOrFind(document, perms);
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException
	{
		Folder rootFolder = Folder.getRootFolder();
		FieldsManager fm = document.getFieldsManager();
		OfficeDocument documentCase = OfficeDocument.find(fm.getLongValue(_DS_OPZZ_CASES_CN));
		
		Folder folder = rootFolder.
				createSubfolderIfNotPresent(_CASES_FOLDER).
				createSubfolderIfNotPresent(documentCase.getTitle()).
				createSubfolderIfNotPresent(_QUEST_FOLDER);
		
		document.setFolder(folder);
		document.setTitle(fm.getStringValue(_TYTUL_CN));
		
	}
	
	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		log.info("--- OpzzDocLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			FieldsManager fm = document.getFieldsManager();
			
			//przypisuje uzytkownikowi task
			DSUser user = DSUser.findById(fm.getLongKey(_DS_OPZZ_ODPOWIEDZIALNY_CN));
			map.put(ASSIGN_USER_PARAM, user.getName());
			
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
			Long caseId = fm.getLongKey(_DS_OPZZ_CASES_CN);
//			DwrDictionaryFacade.dictionarieObjects.get(_DS_OPZZ_CASES_CN).getValues(caseId.toString()).get("DOCUMET_ID");
			OpzzDataMart.addQuestToCase(document.getId(), caseId);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
	{
		Document document = Document.find(documentId);
		
//		Long caseId = kind.getFieldsManager(documentId).getLongKey(_DS_OPZZ_CASES_CN);
//		OpzzDataMart.addQuestToCase(documentId, caseId);
	}

	@Override
	public void specialCompareData(FieldsManager newFm, FieldsManager oldFm) throws EdmException
	{
		Long documentId = newFm.getDocumentId();
		Long newCaseId = newFm.getLongKey(_DS_OPZZ_CASES_CN);
		Long oldCaseId = oldFm.getLongKey(_DS_OPZZ_CASES_CN);
		
		if(!newCaseId.equals(oldCaseId))
		{
			OpzzDataMart.removeQuestToCase(documentId, oldCaseId);
			OpzzDataMart.addQuestToCase(documentId, newCaseId);
		}
		
		
	}
	
    public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			FieldsManager fm = dockind.getFieldsManager(id);
			
			params.setAttribute(fm.getStringValue(_TYTUL_CN), 0);
			
		}
		catch (Exception e)
		{
			log.error("",e);
		}
		return params;
	}
	
	

}
