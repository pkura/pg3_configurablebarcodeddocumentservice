package pl.compan.docusafe.parametrization.opzz;

import java.util.*;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;

import static pl.compan.docusafe.parametrization.opzz.OpzzCn.*;

public class OpzzCzlonkostwoLogic extends AbstractDocumentLogic
{
	public static OpzzCzlonkostwoLogic instance;
	
	public static OpzzCzlonkostwoLogic getInstance()
	{
		if(instance == null)
			instance = new OpzzCzlonkostwoLogic();
		
		return instance;
	}
	
	@Override
	public void documentPermissions(Document document) throws EdmException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException
	{
		// TODO Auto-generated method stub
		
	}

}
