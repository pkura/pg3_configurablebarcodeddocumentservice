package pl.compan.docusafe.parametrization.opzz.dictionary;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @see OrganizationUserDictionary Dao do klasy
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class OrganizationUserDao
{
	/**
	 * Znajduje organizacj� u�ytkownika na podstawie username i organization_id
	 * @param username
	 * @param organizationKey
	 * @return
	 */
	public static OrganizationUserDictionary find(String username, Long organizationKey) throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(OrganizationUserDictionary.class);

		if (username != null && organizationKey != null)
		{
			criteria.add(Restrictions.eq("username", username));
			criteria.add(Restrictions.eq("organizationKey", organizationKey));
			
			List<OrganizationUserDictionary> results = criteria.list();
			
			if(results != null && results.size() > 0)
				return results.get(0);
			else
				return null;
		}
		else
			return null;
	}
	
	/**
	 * Znajduje wpis w s�owniku po id
	 * @param id
	 * @return
	 * @throws EdmException
	 */
	public static OrganizationUserDictionary find(Long id) throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(OrganizationUserDictionary.class);

		if (id != null)
		{
			criteria.add(Restrictions.eq("id", id));
			
			List<OrganizationUserDictionary> results = criteria.list();
			
			if(results != null && results.size() > 0)
				return results.get(0);
			else
				return null;
		}
		else
			return null;
	}
}
