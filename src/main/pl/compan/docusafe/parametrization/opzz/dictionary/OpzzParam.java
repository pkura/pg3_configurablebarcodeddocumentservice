package pl.compan.docusafe.parametrization.opzz.dictionary;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DS_OPZZ_PARAM")
public class OpzzParam
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String name;
	
	private String value;
	
	public OpzzParam(){};
	public OpzzParam(String name, String value)
	{
		this();
		this.name = name;
		this.value = value;
	}
	
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getValue()
	{
		return value;
	}
	public void setValue(String value)
	{
		this.value = value;
	}
	
	
}
