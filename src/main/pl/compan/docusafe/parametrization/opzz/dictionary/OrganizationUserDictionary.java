package pl.compan.docusafe.parametrization.opzz.dictionary;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Tabela przechowuje informacje o organizacjach uzytkownika
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
@Entity
@Table(name="DS_OPZZ_ORG_USER")
public class OrganizationUserDictionary
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id", nullable=false)
	private Long id;
	
	private String username;
	
	@Column(name="organization_key")
	private Long organizationKey;
	
	@Column(name="organization_name")
	private String organizationName;

	private Boolean hidden = false;
	
	public OrganizationUserDictionary()
	{
		
	}
	
	public OrganizationUserDictionary(String username, Long organizationKey, String organizationName)
	{
		this();
		this.username = username;
		this.organizationKey = organizationKey;
		this.organizationName = organizationName;
	}
	
	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public Long getOrganizationKey()
	{
		return organizationKey;
	}

	public void setOrganizationKey(Long organizationKey)
	{
		this.organizationKey = organizationKey;
	}

	public String getOrganizationName()
	{
		return organizationName;
	}

	public void setOrganizationName(String organizationName)
	{
		this.organizationName = organizationName;
	}

	public Boolean isHidden()
	{
		return hidden;
	}

	public void setHidden(Boolean hidden)
	{
		this.hidden = hidden;
	}
	
}
