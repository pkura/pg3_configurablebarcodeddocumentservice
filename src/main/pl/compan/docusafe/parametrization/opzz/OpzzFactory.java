package pl.compan.docusafe.parametrization.opzz;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.handlers.OpzzMailHandler;
import pl.compan.docusafe.parametrization.opzz.dictionary.OrganizationUserDao;
import pl.compan.docusafe.parametrization.opzz.dictionary.OrganizationUserDictionary;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import static pl.compan.docusafe.parametrization.opzz.OpzzCn.*;

/**
 * Fabryka funkcjonalno�ci do Opzz
 * 
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 */
public class OpzzFactory
{
	private final static Logger log = LoggerFactory.getLogger(OpzzFactory.class);
	
	/** definicje istniej�cych maili, EventHandler nie pozwalal na posiadanie enuma 
	 * @see OpzzMailHandler*/
	public enum MAIL_TYPE
	{
		DODANY_WLASCICEL("DODANY_WLASCICEL"),
		USUNIETY_WLASCICIEL("USUNIETY_WLASCICIEL"),
		DODANY_PRZYDZIELONY("DODANY_PRZYDZIELONY"),
		USUNIETY_PRZYDZIELONY("USUNIETY_PRZYDZIELONY"),
		OPINIA_KONIEC("OPINIA_KONIEC"),
		OPINIA_ANKIETA_KONIEC("OPINIA_ANKIETA_KONIEC"),
		ZATWIERDZANIE_KONIEC("ZATWIERDZANIE_KONIEC"),
		ZADANIE_KONIEC("ZADANIE_KONIEC"),
		DODANY_KANDYDAT("DODANY_KANDYDAT"),
		USUNIETY_KANDYDAT("USUNIETY_KANDYDAT"),
		DODANY_DELEGAT("DODANY_DELEGAT"),
		USUNIETY_DELEGAT("USUNIETY_DELEGAT");
		
		private final String name;
		
		private MAIL_TYPE(String s) {name = s; };
		
		@Override
		public String toString() { return name; };
		
	}
	
	/**
	 * Funkcja pobiera uczestnik�w spotkania
	 * 
	 * @param id
	 * @return
	 */
	public static String[] getMembersMeeting(Long id)
	{
		String[] result = new String[10];
		return result;
	}

	/**
	 * Zwraca przypisanych i usuni�tych u�ytkownik�w z listy
	 * 
	 * @param assigns
	 * @param last
	 * @return
	 */
	public static List<Set<Long>> getSimilarAndDiffrent(List<Long> assigns, List<Long> last)
	{
		assigns = removeBadDictionaryElement(assigns);
		last = removeBadDictionaryElement(last);
		
		List<Set<Long>> results = new ArrayList<Set<Long>>();
		
		Set<Long> similar;
		
		if(assigns != null && !assigns.isEmpty())
			similar = new HashSet<Long>((Collection<Long>) assigns);
		else
			similar = new HashSet<Long>();
			
		Set<Long> diffrent = new HashSet<Long>();

		if(assigns != null && !assigns.isEmpty())
		diffrent.addAll(assigns);
		diffrent.addAll(last);

		similar.retainAll(last);
		diffrent.removeAll(similar);

		log.info("Assign:" + assigns);
		log.info("Last" + last);
		log.info("Similar: " + similar);
		log.info("Diffrent:" + diffrent);

		// nowych
		Set<Long> assignSet;
		if(assigns != null && !assigns.isEmpty())
			assignSet = new HashSet<Long>(assigns);
		else
			assignSet = new HashSet<Long>();
		
		// starych
		Set<Long> lastSet = new HashSet<Long>(last);
		// tworz� tablic� set�w przypisanych nowych i usuni�tych starych
		assignSet.removeAll(similar);
		lastSet.removeAll(similar);

		results.add(0, assignSet);
		results.add(1, lastSet);

		log.info("AssignSet:" + assignSet);
		log.info("LastSet : " + lastSet);

		return results;
	}

	
	/**
	 * Usuwa wpisy z listy Long�w mniejsze od 0. G��wnie u�ywane do listy gdzie s� id
	 * @param lists
	 */
	public static List<Long> removeBadDictionaryElement(List<Long> lists)
	{
		if(lists == null)
			return new ArrayList<Long>();
		
		ListIterator<Long> it = lists.listIterator();
		
		while(it.hasNext())
		{
			Object el = it.next();
			if(Double.parseDouble(el.toString()) < 0D)
				it.remove();
		}
		
		return lists;
		
	}
	
	/**
	 * Funkcja sprawdza czy zmieni�y si� warto�ci na pomi�dzy dwoma listami
	 * @param newVar
	 * @param oldVar
	 * @return
	 */
	public static boolean isChange(List<Long> newVar, List<Long> oldVar)
	{
		//jesli ilosc jest r�na zmieni�o si�
		
		if(newVar != null && oldVar != null && newVar.size() != oldVar.size())
			return true;
		
		List<Set<Long>> results = getSimilarAndDiffrent(newVar, oldVar);
		//dodani
		Set<Long> added = results.get(0);
		//usuni�ci
		Set<Long> removed = results.get(1);
		
		//jesli jest jakas wartosc w usunietych lub dodanych wartosci zmieni�y si�
		if((added != null && !added.isEmpty()) || (removed != null && !removed.isEmpty()))
			return true;
		
		return false;
	}
	/**
	 * Wysyla u�ytkownikowi maila i dodaje mu obserwowany
	 * 
	 * @param added
	 * @param removed
	 * @throws Exception
	 */
	public static void casesAssign(Document document, List<Long> added,	List<Long> removed) throws EdmException
	{
		if(added == null )
			added = new ArrayList<Long>();
		
		if(removed == null)
			removed = new ArrayList<Long>();
		
		//zwraca liste z dwoma setami pierwszy to dodani drugi to usuni�ci
		List<Set<Long>> results = getSimilarAndDiffrent(added,removed);

		casesAssign(document, results.get(0), results.get(1));
	}

	/**
	 * Wysyla u�ytkownikowi maila i dodaje mu obserwowany
	 * 
	 * @param added
	 * @param removed
	 * @throws Exception
	 */
	public static void casesAssign(Document document, Set<Long> added, Set<Long> removed) throws EdmException
	{
		if(added == null )
			added = new HashSet<Long>();
		
		if(removed == null)
			removed = new HashSet<Long>();
		
		for (Long addId : added)
		{
			if(addId < 0)
				continue;
			
			DSUser user = DSUser.findById(addId);
//			DSApi.context().watch(URN.create(document), user.getName());
			new Mailer().sendAddedAssign(user, document);
		}

		for (Long removeId : removed)
		{
			if(removeId < 0)
				continue;
			
			DSUser user = DSUser.findById(removeId);
//			DSApi.context().unwatch(URN.create(document), user.getName());
			new Mailer().sendRemoveAssign(user, document);
		}
	}

	/**
	 * Obsluga w�a�cicieli sprawy
	 * 
	 * @param document
	 * @param added
	 * @param removed
	 * @throws EdmException
	 */
	public static void casesOwner(Document document, List<Long> added, List<Long> removed) throws EdmException
	{
		if(added == null )
			added = new ArrayList<Long>();
		
		if(removed == null)
			removed = new ArrayList<Long>();
		
		//zwraca liste z dwoma setami pierwszy to dodani drugi to usuni�ci
		List<Set<Long>> results = getSimilarAndDiffrent(added,removed);
		
		Set<Long> addedSet = results.get(0);
		Set<Long> removedSet = results.get(1);
		
		for (Long addId : addedSet)
		{
			if(addId < 0)
				continue;
			
			DSUser user = DSUser.findById(addId);
			new Mailer().sendAddedOwner(user, document);
		}

		for (Long removeId : removedSet)
		{
			if(removeId < 0)
				continue;
			
			DSUser user = DSUser.findById(removeId);
			new Mailer().sendRemoveOwner(user, document);
		}
	}
	
	public static class Mailer
	{
		/**
		 * Wysy�a informacj� �e uzytkownik zosta� dodany do dokumentu jako
		 * przydzielony
		 * 
		 * @param mail
		 * @param document
		 */
		void sendAddedAssign(DSUser user, Document document)
		{
//			if (user.getEmail() == null)
//				return;
			
			String mailType = MAIL_TYPE.DODANY_PRZYDZIELONY.toString();
			try
			{
				OpzzMailHandler.createEvent(document.getId().toString(), user.getName(), mailType);
			} catch (DocusafeEventException e)
			{
				log.error("blad w wysylaniu maila " + mailType+":" +document.getId() + ","+ user.getName());
			}
		}

		/**
		 * Wysyla maila ze uzytkownik jest wlascicielem
		 * 
		 * @param user
		 * @param document
		 */
		public void sendAddedOwner(DSUser user, Document document)
		{
//			if (user.getEmail() == null)
//				return;

			String mailType = MAIL_TYPE.DODANY_WLASCICEL.toString();
			try
			{
				OpzzMailHandler.createEvent(document.getId().toString(), user.getName(), mailType);
			} catch (DocusafeEventException e)
			{
				log.error("blad w wysylaniu maila " + mailType+":" +document.getId() + ","+ user.getName());
			}
		}

		/**
		 * Wysyla maila ze uzytkownik zosta� usuniety jako wlasciciel
		 * 
		 * @param user
		 * @param document
		 */
		public void sendRemoveOwner(DSUser user, Document document)
		{
//			if (user.getEmail() == null)
//				return;
			
			String mailType = MAIL_TYPE.USUNIETY_WLASCICIEL.toString();
			try
			{
				OpzzMailHandler.createEvent(document.getId().toString(), user.getName(), mailType);
			} catch (DocusafeEventException e)
			{
				log.error("blad w wysylaniu maila " + mailType+":" +document.getId() + ","+ user.getName());
			}

		}

		/**
		 * Wysyla maila ze uzytkownik zosta� usuni�ty jako przydzielony
		 */
		void sendRemoveAssign(DSUser user, Document document)
		{
//			if (user.getEmail() == null)
//				return;
			
			String mailType = MAIL_TYPE.USUNIETY_PRZYDZIELONY.toString();
			try
			{
				OpzzMailHandler.createEvent(document.getId().toString(), user.getName(), mailType);
			} catch (DocusafeEventException e)
			{
				log.error("blad w wysylaniu maila " + mailType+":" +document.getId() + ","+ user.getName());
			}
		}
	}

	/**
	 * Dodaje delegata webserwis
	 * @param delegate
	 * @param eventId
	 * @param organizationId
	 * @throws EdmException
	 */
	public static void addDelegate(String delegate, Long eventId, Long organizationId) throws EdmException 
	{
		Document document = getDocumentByEventId(eventId);
		FieldsManager fm = document.getFieldsManager();

		List<Long> delegates = (List<Long>) fm.getKey(_DS_OPZZ_DELEGATE_CN);
		OrganizationUserDictionary dictionary = OrganizationUserDao.find(delegate, organizationId);
		
		if(delegates == null)
			delegates = new ArrayList<Long>();
		
		if(!delegates.contains(dictionary.getId()))
			delegates.add(dictionary.getId());
		
		Map<String, Object> values = new HashMap<String, Object>();
		values.put(_DS_OPZZ_DELEGATE_CN, delegates);
		
		fm.getDocumentKind().setOnly(document.getId(), values);
	}
	
	/**
	 * Usuwa delegata, webserwis
	 * @param delegate
	 * @param eventId
	 * @throws EdmException
	 */
	public static void removeDelegate(String delegate, Long eventId) throws EdmException 
	{
		log.info("removeDelegate: JESTEM NA POCZATKU");
		Document document = getDocumentByEventId(eventId);
		FieldsManager fm = document.getFieldsManager();
		// TODO Stworzy�
		List<Long> delegates = (LinkedList<Long>) fm.getKey(_DS_OPZZ_DELEGATE_CN);
		log.info("usuwam delegata:" + delegate + ":lista ma element�w:" +delegates.size());
		Long idRemoved = null; 
		for(Long id : delegates)
		{
			if(id < 0)
				continue;
			
			log.info("id znalezione:" + id);
			OrganizationUserDictionary delegateDoc = OrganizationUserDao.find(id);
			log.info("username znalezione" + delegateDoc.getUsername());
			
			if(delegateDoc.getUsername().equals(delegate))
			{
				idRemoved = id;
			}
		}
		
		if(idRemoved != null)
			delegates.remove(idRemoved);
	
		log.info("po ustawieniu:" + delegate);
		Map<String, Object> values = new HashMap<String, Object>();
		values.put(_DS_OPZZ_DELEGATE_CN, delegates);
		
		fm.getDocumentKind().setOnly(document.getId(), values);
	}

	/**
	 * Dodaje kandydata do dokumentu webserwis
	 * @param candidate
	 * @param eventId
	 * @throws EdmException
	 */
	public static void addCandidate(String candidate, Long eventId) throws EdmException 
	{
		// TODO sprawdzi�
		Document document = getDocumentByEventId(eventId);
		FieldsManager fm = document.getFieldsManager();
		
		List<Long> candidates = (LinkedList<Long>) fm.getKey(_DS_OPZZ_CANDIDATE_CN);
		
		if(candidates == null)
			candidates = new ArrayList<Long>();
		
		DSUser userCandidate = DSUser.findByUsername(candidate);
		
		if(!candidates.contains(userCandidate.getId()))
			candidates.add(userCandidate.getId());
		
		Map<String, Object> values = new HashMap<String, Object>();
		values.put(_DS_OPZZ_CANDIDATE_CN, candidates);
		
		fm.getDocumentKind().setOnly(document.getId(), values);
	}

	/**
	 * Usuwa kandydata z dokumentu webserwis
	 * @param candidate
	 * @param eventId
	 * @throws EdmException
	 */
	public static void removeCandidate(String candidate, Long eventId) throws EdmException 
	{
		// TODO sprawdzi�
		Document document = getDocumentByEventId(eventId);
		FieldsManager fm = document.getFieldsManager();
		List<Long> candidates = (LinkedList<Long>) fm.getKey(_DS_OPZZ_CANDIDATE_CN);
		
		candidates.remove(DSUser.findByUsername(candidate).getId());
		
		Map<String, Object> values = new HashMap<String, Object>();
		values.put(_DS_OPZZ_CANDIDATE_CN, candidates);
		
		fm.getDocumentKind().setOnly(document.getId(), values);
	}
	
	/**
	 * 
	 * @param eventId
	 * @return
	 * @throws EdmException 
	 */
	public static Document getDocumentByEventId(Long eventId) throws EdmException
	{
		Document document = null;
		
		try
		{
			Long id = null;
			PreparedStatement ps = DSApi.context().prepareStatement("SELECT DOCUMENT_ID FROM DS_OPZZ_CASES where event_id = ?");
			ps.setLong(1, eventId);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next())
			{
				id = rs.getLong(1);
			}
			
			document = Document.find(id);
		}
		catch (SQLException e) {
			log.error("",e);
		} catch (EdmException e){
			log.error("",e);
			throw new EdmException(e.getMessage());
		}
		
		return document;
	}
	
	/**
	 * Zapisuje dokumenty sprawy
	 * @param newFm
	 * @param oldFm
	 * @throws EdmException 
	 * @throws AccessDeniedException 
	 * @throws DocumentLockedException 
	 * @throws DocumentNotFoundException 
	 */
	public static void saveCasesDocuments(FieldsManager newFm, FieldsManager oldFm) throws DocumentNotFoundException, DocumentLockedException, AccessDeniedException, EdmException
	{
		Long documentId = newFm.getDocumentId();
		List<Long> newDocs = (List<Long>)newFm.getKey(_DS_OPZZ_DOC_CN);
		List<Long> oldDocs = (List<Long>)oldFm.getKey(_DS_OPZZ_DOC_CN);

		saveCasesDocuments(newDocs, oldDocs, documentId);
	}
	/**
	 * Zapisuje dokumenty sprawy
	 * @param newFm
	 * @param oldFm
	 * @throws EdmException 
	 * @throws AccessDeniedException 
	 * @throws DocumentLockedException 
	 * @throws DocumentNotFoundException 
	 */
	public static void saveCasesDocuments(List<Long> newDocs, List<Long> oldDocs, Long documentId) throws DocumentNotFoundException, DocumentLockedException, AccessDeniedException, EdmException
	{
		if(newDocs == null)
			newDocs = new ArrayList<Long>();
		
		if(oldDocs == null)
			oldDocs = new ArrayList<Long>();
		
		List<Set<Long>> results = getSimilarAndDiffrent(newDocs, oldDocs);
		
		Set<Long> added = results.get(0);
		Set<Long> removed = results.get(1);
		
		//dodaje sprawy do document�w dodanych na formatce
		for(Long docId : added)
		{
			Document caseDoc = Document.find(docId);
			Map<String, Object> values = new HashMap<String, Object>();
			values.put(_DS_OPZZ_CASES_CN, documentId);
			
			caseDoc.getFieldsManager().getDocumentKind().setWithHistory(docId, values, false);
			
			//XXX: doda� info ze zosta� dodany do sprawy dokument
		}
		
		//usuwam z dokument�w 
 		for(Long docId : removed)
		{
 			Document caseDoc = Document.find(docId);
			Map<String, Object> values = new HashMap<String, Object>();
			values.put(_DS_OPZZ_CASES_CN, null);
				
			caseDoc.getFieldsManager().getDocumentKind().setWithHistory(docId, values, false);
			//XXX: doda� info ze zost�� usuni�ty ze sprawy
		}
	}
	/**
	 * Updatuje pola spraw dokument�w
	 * @param newFm
	 * @param oldFm
	 * @throws EdmException
	 */
	public static void saveDocumentCase(FieldsManager newFm, FieldsManager oldFm) throws EdmException
	{
		Long newCaseId = newFm.getLongKey(_DS_OPZZ_CASES_CN);
		Long oldCaseId = oldFm.getLongKey(_DS_OPZZ_CASES_CN);
		
		saveDocumentCase(newCaseId, oldCaseId, newFm.getDocumentId());
	}
	/**
	 * Updatuje pola spraw dokument�w
	 * @param newFm
	 * @param oldFm
	 * @throws EdmException
	 */
	public static void saveDocumentCase(Long newCaseId, Long oldCaseId, Long documentId) throws EdmException
	{
		Map<String,Object> newValues = new HashMap<String, Object>();
		
		if(oldCaseId != null && !oldCaseId.equals(newCaseId))
		{
			//stara sprawa usuwam
			Document oldCase = Document.find(oldCaseId);
			List<Long> oldDocsId = (List<Long>) oldCase.getFieldsManager().getKey(_DS_OPZZ_DOC_CN);
			
			if(oldDocsId != null)
			{
				oldDocsId.remove(documentId);
			}
			
			newValues = new HashMap<String, Object>();
			newValues.put(_DS_OPZZ_DOC_CN, oldDocsId);
			oldCase.getDocumentKind().setOnly(oldCaseId, newValues);
		}
		
		//nowa sprawa dodaje 
		if(newCaseId != null && !newCaseId.equals(oldCaseId))
		{
			Document newCase = Document.find(newCaseId);
			
			//duplikaty?
			List<Long> docsId = (List<Long>) newCase.getFieldsManager().getKey(_DS_OPZZ_DOC_CN);
			
			if(docsId == null)
				docsId = new ArrayList<Long>();
			
			//nie dodaje je�li ju� zawiera
			if(!docsId.contains(documentId))
			{
				docsId.add(documentId);
				newValues.put(_DS_OPZZ_DOC_CN, docsId);
			
				newCase.getDocumentKind().setOnly(newCaseId, newValues);
			}
		}
	}
	
	/**
	 * Czyszcz� dokumenty spraw i dopiero potem zapisuje te nowe przydzielone
	 * @param opzzCase
	 */
	public static void clearCasesDocuments(Document opzzCase)
	{
		try
		{
			PreparedStatement ps = DSApi.context().prepareStatement("UPDATE DS_OPZZ_DOC SET OPZZ_CASE_ID = NULL where OPZZ_CASE_ID = ?");
			ps.setLong(1, opzzCase.getId());
			
			int rs = ps.executeUpdate();
			
			log.info("Wst�pny update dokument�w spraw wyczyszczono najpierw: " + rs);
			log.info("Przyst�puj� do zapisu");
		}catch (Exception e) {
			log.error("",e);
		}
	}

}
