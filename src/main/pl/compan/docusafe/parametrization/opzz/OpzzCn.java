package pl.compan.docusafe.parametrization.opzz;

/**
 * interfejs ze sta�ymi uzywanymi w klasach logicznych Opzz
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 */
public interface OpzzCn
{
	String _TYTUL_CN = "TYTUL";
	String _TYP_CN = "TYP";
	String _OPIS_CN = "OPIS";
	String _MIEJSCE_CN = "MIEJSCE";
	String _TERMIN_CN = "TERMIN";
	String _DATA_KONCOWA_CN = "DATA_KONCOWA";
	String _EVENT_ID_CN = "EVENT_ID";
	
	String _DS_OPZZ_DELEGATE_CN = "DS_OPZZ_DELEGATE";
	String _DS_OPZZ_CANDIDATE_CN = "DS_OPZZ_CANDIDATE";
	String _DS_OPZZ_DOC_CN = "DS_OPZZ_DOC";
	String _DS_OPZZ_CASES_CN = "DS_OPZZ_CASES";
	String _DS_OPZZ_ASSIGNED_CN = "DS_OPZZ_ASSIGNED";
	String _DS_OPZZ_OWNER_CN = "DS_OPZZ_OWNER";
	String _DS_OPZZ_ODPOWIEDZIALNY_CN = "ODPOWIEDZIALNY";
	String _CZAS_TRWANIA_CN = "CZAS_TRWANIA";
	
	String _CASES_FOLDER = "Sprawy";
	String _DOCS_FOLDER = "Dokumenty";
	String _QUEST_FOLDER = "Zadanie";
	String _DOCS_OPINION_FOLDER  ="Opiniowanie/Zatwierdzanie Dokument�w";
	
	
	//JBPM assingee acceptionCn
	
	String _OWNER = "owner";
	String _ASSIGNER = "assigner";
	String _AUTHOR_END = "autor-end";
	
}
