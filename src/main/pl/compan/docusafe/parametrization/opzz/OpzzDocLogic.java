package pl.compan.docusafe.parametrization.opzz;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.sql.SQLException;
import java.util.*;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import static pl.compan.docusafe.parametrization.opzz.OpzzCn.*;

import com.google.common.collect.Maps;

/**
 * Logika dokumentu opzz
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 */
public class OpzzDocLogic extends AbstractDocumentLogic
{
	private static OpzzDocLogic instance;
	private final static Logger log = LoggerFactory.getLogger(OpzzDocLogic.class);
	private static StringManager sm = StringManager.getManager(OpzzCasesLogic.class.getPackage().getName());
	
	public static OpzzDocLogic getInstance()
	{
		if(instance == null)
			instance = new OpzzDocLogic();
		
		return instance;
	}
	
	@Override
	public void documentPermissions(Document document) throws EdmException
	{
		Set<PermissionBean> perms = new HashSet<PermissionBean>();
		
		perms.add(new PermissionBean(ObjectPermission.READ, "OPZZ_DOC_READ", ObjectPermission.GROUP, "Dokumenty Sprawy - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "OPZZ_DOC_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty Sprawy za��cznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "OPZZ_DOC_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty Sprawy - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "OPZZ_DOC_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty Sprawy zal�cznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "OPZZ_DOC_READ_DELETE", ObjectPermission.GROUP, "Dokumenty Sprawy - usuwanie"));

		createOrFind(document, perms);
	}

	
//	@Override
//	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue)
//	{
//			Boolean assignee = false;
//		
//		try
//		{
//			if(acceptationCn.equals("doc_owner"))
//			{
//				Long userId = Long.parseLong(openExecution.getVariable("userId").toString());
//				String username = DSUser.findById(userId).getName();
//				assignable.addCandidateUser(username);
//				assignee = true;
//			}
//		}catch(Exception e){
//			log.error(e.getMessage(), e);
//		}
//		
//		
//		return assignee;
//	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		Folder rootFolder = Folder.getRootFolder();
		FieldsManager fm = document.getFieldsManager();
		Long caseId = fm.getLongValue(_DS_OPZZ_CASES_CN);
		Folder folder;
		if(caseId != null)
		{
			OfficeDocument documentCase = OfficeDocument.find(fm.getLongValue(_DS_OPZZ_CASES_CN));

			folder = rootFolder.createSubfolderIfNotPresent(_CASES_FOLDER).
					createSubfolderIfNotPresent(documentCase.getTitle())
					.createSubfolderIfNotPresent(_DOCS_FOLDER);
			
			//dodaje powiazanie do documentu sprawy
			OpzzFactory.saveDocumentCase(caseId, null, document.getId());
		}
		else
		{
			folder = rootFolder.createSubfolderIfNotPresent(_DOCS_FOLDER + " Nieprzydzielone");
		}
		
		
		document.setFolder(folder);
		document.setTitle(fm.getStringValue(_TYTUL_CN));
		String description = fm.getStringValue(_OPIS_CN);
		if(description.length() > 500)
			description = description.substring(0, 500);
		document.setDescription(description);
		
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("ID", document.getId());
		document.getDocumentKind().setOnly(document.getId(), values);
		
	}
	
	
	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		super.setInitialValues(fm, type);

	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		log.info("--- OpzzDocLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
				map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
	{
		
	}

	@Override
	public void specialCompareData(FieldsManager newFm, FieldsManager oldFm) throws EdmException
	{
		OpzzFactory.saveDocumentCase(newFm, oldFm);
		
		
	}
	
    public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			FieldsManager fm = dockind.getFieldsManager(id);
			
			params.setAttribute(fm.getStringValue(_TYTUL_CN), 0);
			
		}
		catch (Exception e)
		{
			log.error("",e);
		}
		return params;
	}
	
	
	

}
