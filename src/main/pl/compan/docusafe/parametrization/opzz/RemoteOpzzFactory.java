package pl.compan.docusafe.parametrization.opzz;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.handlers.OpzzMailHandler;
import pl.compan.docusafe.parametrization.opzz.dictionary.OrganizationUserDao;
import pl.compan.docusafe.parametrization.opzz.dictionary.OrganizationUserDictionary;
import pl.compan.docusafe.util.*;
import pl.eo.opzz.portlet.service.dto.*;
import pl.eo.opzz.portlet.service.http.*;

import static pl.compan.docusafe.parametrization.opzz.OpzzCn.*;

/**
 * Klasa s�u�y do obs�ugi webserwis�w OPZZ
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class RemoteOpzzFactory
{
	/**
	 * Zmienna do s�u��ca do przeprowadzenia testu bez wysy�ania na webserwis
	 */
	public static final Boolean _TEST_WS = false;
	private static final Logger log = LoggerFactory.getLogger(RemoteOpzzFactory.class);
	
	/**
	 * dodaje i usuwa delegat�w
	 * @param eventId
	 * @param candidates
	 * @param lastCandidates
	 * @throws Exception
	 */
	public static void addAndRemoveCandidate(Long eventId, List<Long> candidates, List<Long> lastCandidates) throws Exception
	{
		//zwraca liste z dwoma setami pierwszy to dodani drugi to usuni�ci
		List<Set<Long>> results = OpzzFactory.getSimilarAndDiffrent(candidates, lastCandidates);
		List<Long> newCandidate = new ArrayList<Long>(results.get(0));
		List<Long> removeCandidate = new ArrayList<Long>(results.get(1));
		
		log.info("metoda addAndRemoveCandidate");
		
		if(newCandidate != null && newCandidate.size() > 0)
			addCandidates(eventId, newCandidate);
		
		if(removeCandidate != null && removeCandidate.size() > 0)
			removeCandidates(eventId, removeCandidate);
	}
	
	/**
	 * Dodaje kandydat�w do zdarzenia
	 */
	public static void addCandidates(Long eventId, List<Long> candidates) throws Exception
	{
		List<String> usernames = DSUser.getUsernameArrayById(candidates);
		
		if(!_TEST_WS && usernames != null && usernames.size() > 0)
		{
			String[] usernameArray = usernames.toArray(new String[usernames.size()]);
			addCandidates(eventId, usernameArray);
		}
		
		for(String username : usernames)
		{
			Document document = OpzzFactory.getDocumentByEventId(eventId);
			OpzzMailHandler.createEvent(document.getId().toString(), username, OpzzFactory.MAIL_TYPE.DODANY_KANDYDAT.toString());
		}
		
		log.info("Dodaje kandydat�w w evencie:" + eventId + "-" + usernames.toString());
	}
	
	/**
	 * Dodaje kandydat�w do zdarzenia
	 */
	public static void addCandidates(Long eventId, String[] candidates) throws Exception
	{
		EventMembersElectionServiceSoap service = new EventMembersElectionServiceSoapServiceLocator().
									getPlugin_EO_Calendar_EventMembersElectionService(getElectionEndPoint());
		
		AddCandidatesDto addCandidates = new AddCandidatesDto();
		addCandidates.setEventId(eventId);
		addCandidates.setUser(DSApi.context().getPrincipalName());
		addCandidates.setCandidates(candidates);
		
		WsResponseDto response = service.addCandidates(addCandidates);
		
		checkResponse(response);
	}
	
	/**
	 * Usuwa kandydat�w ze zdarzenia
	 * @param eventId
	 * @param candidates
	 * @throws Exception
	 */
	public static void removeCandidates(Long eventId, List<Long> candiates) throws Exception
	{
		List<String> usernames = DSUser.getUsernameArrayById(candiates);
		
		if(!_TEST_WS && usernames != null && usernames.size() > 0)
		{
			String[] usernameArray = usernames.toArray(new String[usernames.size()]);
			removeCandidates(eventId, usernameArray);
		}
		
		for(String username : usernames)
		{
			Document document = OpzzFactory.getDocumentByEventId(eventId);
			OpzzMailHandler.createEvent(document.getId().toString(), username, OpzzFactory.MAIL_TYPE.USUNIETY_KANDYDAT.toString());
		}
		
		log.info("Usuwam kandydat�w w evencie:" + eventId + "-" + usernames.toString());
	}
	/**
	 * Usuwa kandydat�w ze zdarzenia
	 * @param eventId
	 * @param candidates
	 * @throws Exception
	 */
	public static void removeCandidates(Long eventId, String[] candidates) throws Exception
	{
		EventMembersElectionServiceSoap service = new EventMembersElectionServiceSoapServiceLocator().
				getPlugin_EO_Calendar_EventMembersElectionService(getElectionEndPoint());
		
		RemoveCandidatesDto removeCandidates = new RemoveCandidatesDto();
		removeCandidates.setEventId(eventId);
		removeCandidates.setUser(DSApi.context().getPrincipalName());
		removeCandidates.setCandidates(candidates);
		
		WsResponseDto response = service.removeCandidates(removeCandidates);
		
		checkResponse(response);
	}
	
	/**
	 * Dodaje i usuwa delegat�w
	 * @param eventId
	 * @param delegates
	 * @param lastDelegates
	 */
	public static void addAndRemoveDelegates(Long eventId, List<Long> delegates, List<Long> lastDelegates) throws Exception
	{
		// zwraca liste z dwoma setami pierwszy to dodani drugi to usuni�ci
		List<Set<Long>> results = OpzzFactory.getSimilarAndDiffrent(delegates, lastDelegates);
		List<Long> newDelegates =  new ArrayList<Long>(results.get(0));
		List<Long> removeDelegate =  new ArrayList<Long>(results.get(1));
		log.info("metoda addAndRemoveDelegate");

		if(newDelegates != null && newDelegates.size() > 0)
			addDelegates(eventId, newDelegates);
		
		if(removeDelegate != null && removeDelegate.size() > 0)
			removeDelegates(eventId, removeDelegate);
	}
	
	/**
	 * Id s� to wpisy s�ownika delegat�w
	 * @param eventId
	 * @param delegates
	 * @throws Exception
	 */
	public static void addDelegates(Long eventId, List<Long> delegates) throws Exception
	{
		OpzzFactory.removeBadDictionaryElement(delegates);
		DelegateDto[] delegateArray = new DelegateDto[delegates.size()];
		int i = 0;
		//id jest id w OrganizationUserDictionary wpisem
		for(Long id : delegates)
		{
			if(id < 0)
				continue;
			
			OrganizationUserDictionary dictionary = OrganizationUserDao.find(id);
			DelegateDto delegateDto = new DelegateDto(dictionary.getUsername(), dictionary.getOrganizationKey());
			delegateArray[i] = delegateDto;
			i++;
			log.info("dodaje delegata w evencie:" + eventId + "-" + dictionary.getUsername() +"-"+ dictionary.getOrganizationKey());
		}
		
		if(!_TEST_WS)
			addDelegates(eventId, delegateArray);
		
		for(DelegateDto delegate : delegateArray)
		{
			Document document = OpzzFactory.getDocumentByEventId(eventId);
			OpzzMailHandler.createEvent(document.getId().toString(), delegate.getDelegate(), OpzzFactory.MAIL_TYPE.DODANY_DELEGAT.toString());
		}
	}
	/**
	 * Dodaje delegata do zdarzenia
	 * @throws Exception
	 */
	public static void addDelegates(Long eventId, DelegateDto[] delegates) throws Exception
	{
		EventMembersElectionDelegateServiceSoap service = new EventMembersElectionDelegateServiceSoapServiceLocator().
				getPlugin_EO_Calendar_EventMembersElectionDelegateService(getElectionDelegateEndpoint());
		
		AddDelegatesDto addDelegates = new AddDelegatesDto();
		addDelegates.setEventId(eventId);
		addDelegates.setUser(DSApi.context().getPrincipalName());
		addDelegates.setDelegates(delegates);
		
		WsResponseDto response = service.addDelegates(addDelegates);
		checkResponse(response);
	}
	
	/**
	 * Id s� to wpisy w s�owniku delegat�w
	 * @throws Exception
	 */
	public static void removeDelegates(Long eventId, List<Long> delegates) throws Exception
	{
		String[] delegateArray = new String[delegates.size()];
		int i = 0;
		//id jest id w OrganizationUserDictionary wpisem
		for(Long id : delegates)
		{
			OrganizationUserDictionary dictionary = OrganizationUserDao.find(id);
			delegateArray[i] = dictionary.getUsername();
			i++;
			log.info("usuwam delegata w evencie:" + eventId + "-" + dictionary.getUsername() +"-"+ dictionary.getOrganizationKey());
		}
		
		if(!_TEST_WS)
			removeDelegates(eventId, delegateArray);
		
		for(String delegate : delegateArray)
		{
			Document document = OpzzFactory.getDocumentByEventId(eventId);
			OpzzMailHandler.createEvent(document.getId().toString(), delegate, OpzzFactory.MAIL_TYPE.USUNIETY_DELEGAT.toString());
		}
		
	}
	
	/**
	 * Usuwa delegata ze eventu
	 * @param eventId
	 * @param usernames
	 * @throws Exception
	 */
	public static void removeDelegates(Long eventId, String[] delegates) throws Exception
	{
		EventMembersElectionDelegateServiceSoap service = new EventMembersElectionDelegateServiceSoapServiceLocator().
				getPlugin_EO_Calendar_EventMembersElectionDelegateService(getElectionDelegateEndpoint());
		
		RemoveDelegatesDto addDelegates = new RemoveDelegatesDto();
		addDelegates.setEventId(eventId);
		addDelegates.setUser(DSApi.context().getPrincipalName());
		addDelegates.setDelegates(delegates);
		
		WsResponseDto response = service.removeDelegates(addDelegates);
		checkResponse(response);
	}
	
	/**
	 * Pobiera organizacj� u�ytkownika
	 * @param username
	 * @return
	 * @throws Exception
	 */
	public static OrganizationDto[] getOrganizationsForUser(String username) throws Exception
	{
		EventMembersElectionDelegateServiceSoap service = new EventMembersElectionDelegateServiceSoapServiceLocator().
				getPlugin_EO_Calendar_EventMembersElectionDelegateService(getElectionDelegateEndpoint());

		GetOrganizationsDto organizationDto = new GetOrganizationsDto();
		organizationDto.setUser(username);
		GetOrganizationsResponseDto response = service.getOrganizationsForUser(organizationDto);
		
		checkResponse(response);
		
		log.info("Pobralem organizacje dla usera: "+ username +"," + response.getOrganizations().length);
		return response.getOrganizations();
	}
	
	/**
	 * Dodaje wydarzenie kalendarzowe values s� z documentu OpzzCasesLogic
	 * @throws Exception
	 */
	public static Long addMembersElectionEvent(Map<String,Object> values) throws Exception
	{
		CalEventDto event = getEventDto(values);
		Long eventId = null;
		
		if(!_TEST_WS)
			eventId = addMembersElectionEvent(event);
		else
			eventId = new Random().nextLong();
		
		log.info("Dodaje wydarzenie kalendarzowe o id eventu:" +eventId);
		return eventId;
	}
	
	/**
	 * updatuje wydarzenie kalendarzowe values s� z documentu OpzzCasesLogic
	 * @throws Exception
	 */
	public static void updateMembersElectionEvent(Map<String,Object> values) throws Exception
	{
		CalEventDto event = getEventDto(values);
		String eventId = values.get(_EVENT_ID_CN).toString();
		if(eventId == null)
		{
			
		}
		else
		{
			event.setEventId(Long.parseLong(eventId));
		
			log.info("Updatuje wydarzenie kalendarzowe o id =" + values.get(_EVENT_ID_CN).toString());
		
			if(!_TEST_WS)
				updateMembersElectionEvent(event);
		}
	}
	
	/**
	 * Zwraca obiekt event uzywany w webserwisach
	 * @return
	 */
	private static CalEventDto getEventDto(Map<String, Object> values) 
	{
		CalEventDto event = new CalEventDto();
		
//		Calendar startDate = Calendar.getInstance(new SimpleTimeZone(0, "GMT"));
		Calendar startDate = Calendar.getInstance();
		startDate.setTime((Date)values.get(_TERMIN_CN));
//		startDate.add(Calendar.HOUR, 1);
		
//		Calendar endDate = Calendar.getInstance(new SimpleTimeZone(0, "GMT"));
		Calendar endDate = Calendar.getInstance();
		endDate.setTime((Date)values.get(_DATA_KONCOWA_CN));
//		endDate.add(Calendar.HOUR, 1);
		
		SimpleDateFormat startFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat endFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		startFormat.setCalendar(startDate);
		endFormat.setCalendar(endDate);
		
		Object duration = values.get(_CZAS_TRWANIA_CN);
		String[] durArray = null;
		
		if(duration != null)
		{
			durArray = duration.toString().split(":");
			if(new Integer(durArray[0]) == 0 && new Integer(durArray[1]) == 0)
			{
				event.setDurationHour(24);
				event.setDurationMinute(00);
				event.setAllDay(Boolean.TRUE);
			}
			event.setDurationHour(new Integer(durArray[0]));
			event.setDurationMinute(new Integer(durArray[1]));
			event.setAllDay(Boolean.FALSE);
		}
		else
		{
			
			event.setDurationHour(24);
			event.setDurationMinute(00);
			event.setAllDay(Boolean.TRUE);
		}
		
		event.setDescription(values.get(_OPIS_CN).toString());
		event.setTitle(values.get(_TYTUL_CN).toString());
		event.setLocation(values.get(_MIEJSCE_CN).toString());
		event.setUser(DSApi.context().getPrincipalName());
		event.setStartDate(startFormat.getCalendar());
		event.setEndDate(endFormat.getCalendar());
		
		log.info("startDate:" + startFormat.format(startDate.getTime()));
		log.info("endDate:" + endFormat.format(endDate.getTime()));
		return event;
	}
	
	/**
	 * Dodaje wydarzenie kalendarzowe
	 * @throws Exception
	 */
	public static Long addMembersElectionEvent(CalEventDto calEvent) throws Exception
	{
		EventMembershipServiceSoap service = new EventMembershipServiceSoapServiceLocator().
				getPlugin_EO_Calendar_EventMembershipService(getServiceEndpoint());
		
		AddCalEventResponseDto response = service.addMembersElectionEvent(calEvent);
		
		checkResponse(response);
		
		return response.getCalEventId();
	}
	
	/**
	 * Updatuje wydarzenie kalendarzowe
	 * @throws Exception
	 */
	public static void updateMembersElectionEvent(CalEventDto calEvent) throws Exception
	{
		EventMembershipServiceSoap service = new EventMembershipServiceSoapServiceLocator().
				getPlugin_EO_Calendar_EventMembershipService(getServiceEndpoint());
		
		WsResponseDto response = service.updateMembersElectionEvent(calEvent);
		
		checkResponse(response);
	}
	
	/**
	 * Sprawdza czy odpowiedz nie jest bledna
	 * @param response
	 */
	private static void checkResponse(WsResponseDto response) throws EdmException
	{
		if(!response.isSuccess())
		{
			log.error("B��dny  request");
			log.error(response.getErrorMessage());
			throw new EdmException(response.getErrorMessage());
		}
		else
			log.error("request zakonczony sukcesem");
		
	}
	
	private static URL getServiceEndpoint()
	{
		URL url = null;
		try
		{
			url = new URL(Docusafe.getAdditionProperty("EventMembershipService.endPointReference"));
		} catch (MalformedURLException e)
		{
			log.error("", e);
		}
		
		return url;
	}
	
	private static URL getElectionEndPoint()
	{
		URL url = null;
		try
		{
			url = new URL(Docusafe.getAdditionProperty("EventMembershipElectionService.endPointReference"));
		} catch (MalformedURLException e)
		{
			log.error("", e);
		}
		
		return url;
	}
	
	private static URL getElectionDelegateEndpoint()
	{
		URL url = null;
		try
		{
			url = new URL(Docusafe.getAdditionProperty("EventMembershipElectionDelegateService.endPointReference"));
		} catch (MalformedURLException e)
		{
			log.error("", e);
		}
		
		return url;
	}

}
