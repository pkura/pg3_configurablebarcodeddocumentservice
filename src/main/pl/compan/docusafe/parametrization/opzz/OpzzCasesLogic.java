package pl.compan.docusafe.parametrization.opzz;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.DwrSearchable;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.Field.Type;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.office.workflow.JBPMFactory;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TimeUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.sql.SQLException;
import java.util.*;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.Task;

import pl.compan.docusafe.core.dockinds.process.ProcessDefinition;

import static pl.compan.docusafe.parametrization.opzz.OpzzCn.*;

import com.google.common.collect.Maps;

/**
 * Logika spraw dokumentu w OPZZ
 * 
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 * 
 */
public class OpzzCasesLogic extends AbstractDocumentLogic
{
	private static OpzzCasesLogic instance;
	private static StringManager sm = StringManager.getManager(OpzzCasesLogic.class.getPackage().getName());
	protected static Logger log = LoggerFactory.getLogger(OpzzCasesLogic.class);

	public static OpzzCasesLogic getInstance()
	{
		if (instance == null)
			instance = new OpzzCasesLogic();

		return instance;
	}

	/**
	 * Permission dokumentu
	 */
	@Override
	public void documentPermissions(Document document) throws EdmException
	{
		Set<PermissionBean> perms = new HashSet<PermissionBean>();

		
		perms.add(new PermissionBean(ObjectPermission.READ, "OPZZ_CASES_READ",
				ObjectPermission.GROUP, "Sprawy - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,
				"OPZZ_CASES_READ_ATT_READ", ObjectPermission.GROUP,
				"Sprawy za��cznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY,
				"OPZZ_CASES_READ_MODIFY", ObjectPermission.GROUP,
				"Sprawy - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,
				"OPZZ_CASES_READ_ATT_MODIFY", ObjectPermission.GROUP,
				"Sprawy zal�cznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE,
				"OPZZ_CASES_READ_DELETE", ObjectPermission.GROUP,
				"Sprawy - usuwanie"));

		createOrFind(document, perms);
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException
	{
		Folder rootFolder = Folder.getRootFolder();
		FieldsManager fm = document.getFieldsManager();
		String title = fm.getStringValue(_TYTUL_CN);

		Folder folder = rootFolder.createSubfolderIfNotPresent(_CASES_FOLDER)
				.createSubfolderIfNotPresent(title);

		document.setFolder(folder);
		document.setTitle(title);
		
		List<Long> newDocs = (List<Long>)fm.getKey(_DS_OPZZ_DOC_CN);
		
		OpzzFactory.saveCasesDocuments(newDocs, new ArrayList<Long>(), document.getId());
	}

	
	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		super.setInitialValues(fm, type);
		
		Map<String, Object> values = new HashMap<String, Object>();
//		values.put(_CZAS_TRWANIA_CN, "01:00");
		fm.reloadValues(values);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		log.info("--- OpzzCasesLogic : START PROCESS !!! ---- {}", event);
		try
		{	
			FieldsManager fm = document.getFieldsManager();
			Map<String, Object> map = Maps.newHashMap();
			
			List<Long> assigns = (LinkedList<Long>) fm.getKey(_DS_OPZZ_ASSIGNED_CN);
			List<Long> owners = (LinkedList<Long>) fm.getKey(_DS_OPZZ_OWNER_CN);
			List<Long> candidates = (LinkedList<Long>)fm.getKey(_DS_OPZZ_CANDIDATE_CN);
			List<Long> delegates = (LinkedList<Long>)fm.getKey(_DS_OPZZ_DELEGATE_CN);

			map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

			if(assigns != null && !assigns.isEmpty())
				OpzzFactory.casesAssign(document, new HashSet<Long>(assigns), new HashSet<Long>());
			
			if(owners != null && !owners.isEmpty())
				OpzzFactory.casesOwner(document, owners, new ArrayList<Long>());
		
			
			//nowo tworzony dokument
			if(isElection(fm.getStringKey(_TYP_CN)))
			{
				//TODO: sprawdzi� bez webserwsiu
				Long eventId = RemoteOpzzFactory.addMembersElectionEvent(fm.getFieldValues());
				
				Map<String, Object> values = new HashMap<String, Object>();
				values.put(_EVENT_ID_CN, eventId);
				fm.getDocumentKind().setOnly(document.getId(), values);
				
				// dodaje kandydat�w do wydarzenia
				if(candidates != null && candidates.size() > 0)
					RemoteOpzzFactory.addCandidates(eventId, candidates);
				
				//dodaje delegat�w do wydarzenia
				if(delegates != null && delegates.size() > 0)
					RemoteOpzzFactory.addDelegates(eventId, delegates);
			}
			
			DSApi.context().watch(URN.create(document));
			
		} catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

	@SuppressWarnings("unused")
	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
	{
//		Document document = Document.find(documentId);
	}

	
	@Override
	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue)
	{
		Boolean assignee = false;
		
		try
		{
			if(acceptationCn.equals(_OWNER))
			{
				Long userId = Long.parseLong(openExecution.getVariable("userId").toString());
				String username = DSUser.findById(userId).getName();
				assignable.addCandidateUser(username);
				assignee = true;
			}
			
			if(acceptationCn.equals(_ASSIGNER))
			{
				Long userId = Long.parseLong(openExecution.getVariable("userId").toString());
				String username = DSUser.findById(userId).getName();
				assignable.addCandidateUser(username);
				assignee = true;
			}
			
			if(acceptationCn.equals(_AUTHOR_END))
			{
				List<Long> owners = (List<Long>) doc.getFieldsManager().getKey(_DS_OPZZ_OWNER_CN);
				if(owners != null && !owners.isEmpty())
					for(Long owner : owners)
					{
						String username = DSUser.findById(owner).getName();
						assignable.addCandidateUser(username);
						assignee = true;
					}
				else
				{
					assignable.addCandidateUser(doc.getAuthor());
					assignee = true;
				}
				
//				TaskSnapshot.updateByDocument(doc);
			}
			
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
		
		
		return assignee;
	}

	@Override
	@SuppressWarnings({"unchecked","unused"})
	public void specialCompareData(FieldsManager newFm, FieldsManager oldFm)  throws EdmException
	{
		log.info("OpzzCasesLogic : specialCompareData");
		Document document = Document.find(newFm.getDocumentId());
		
		List<Long> assigns = (LinkedList<Long>) newFm.getKey(_DS_OPZZ_ASSIGNED_CN);
		List<Long> lastAssigned = (LinkedList<Long>) oldFm.getKey(_DS_OPZZ_ASSIGNED_CN); //poprzednio przypisani
		
		
		List<Long> owners = (LinkedList<Long>) newFm.getKey(_DS_OPZZ_OWNER_CN);
		List<Long> lastOwnered = (LinkedList<Long>) oldFm.getKey(_DS_OPZZ_OWNER_CN);
		
		//sprawdza czy warto�ci zosta�y zmienione przez osob� nie upowaznion� - nie w�a�ciciel
		boolean isChange = OpzzFactory.isChange(assigns, lastAssigned) || OpzzFactory.isChange(owners, lastOwnered);
		
		if( isChange && (owners == null || !owners.contains(DSApi.context().getDSUser().getId())))
			throw new EdmException("Tylko w�a�ciciel mo�e zmienia� te warto�ci");
		
		if(assigns != null && !assigns.isEmpty())
			OpzzFactory.casesAssign(document, assigns, lastAssigned);
		log.info("po przydzielonych");
		
		if(owners != null && !owners.isEmpty())
			OpzzFactory.casesOwner(document, owners, lastOwnered);
		log.info("po wlascicielach");
		
		if(isElection(newFm.getStringKey(_TYP_CN)))
		{
			List<Long> candidates = (LinkedList<Long>)newFm.getKey(_DS_OPZZ_CANDIDATE_CN);
			List<Long> lastCandidates = (LinkedList<Long>)oldFm.getKey(_DS_OPZZ_CANDIDATE_CN);
			
			List<Long> delegates = (LinkedList<Long>)newFm.getKey(_DS_OPZZ_DELEGATE_CN);
			List<Long> lastDelegates = (LinkedList<Long>)oldFm.getKey(_DS_OPZZ_DELEGATE_CN);
			try
			{
				Long eventId = newFm.getLongKey(_EVENT_ID_CN);
				//TODO: sprawdzi� bez webserwisu
				RemoteOpzzFactory.updateMembersElectionEvent(newFm.getFieldValues());
				
				//zapisywanie nowych kandydat�w
				//usuwanie starych kandydat�w
				RemoteOpzzFactory.addAndRemoveCandidate(eventId, candidates, lastCandidates);
				
				//zapisywanie delegat�w
				// usuwanie delegat�w
				RemoteOpzzFactory.addAndRemoveDelegates(eventId, delegates, lastDelegates);
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
				throw new EdmException(e.getMessage());
			}
		}
		
		OpzzFactory.saveCasesDocuments(newFm, oldFm);
	}

//	private void startNewProcess(Document document, List<Long> assigns, List<Long> lastAssigned) throws EdmException
//	{
//		log.info("OpzzCasesLogic: startNewPRocess");
//		//zwraca liste z dwoma setami pierwszy to dodani drugi to usuni�ci
//		List<Set<Long>> results = OpzzFactory.getSimilarAndDiffrent(assigns,lastAssigned);
//		
//		List<String> newAssign = DSUser.getUsernameArrayById(new ArrayList<Long>(results.get(0))); //dodani
//		List<String> removedAssign = DSUser.getUsernameArrayById(new ArrayList<Long>(results.get(1))); //usunieci
//		
//		ProcessDefinition pdef = document.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess("manual");
//		List<String> tasks = (List<String>) Jbpm4ProcessLocator.taskIds(document.getId());
//		
//		//zamykam zadania uzytkownikom nie przydzielonym, usuni�tym z listy
//		for(String taskName : tasks)
//		{
//			Task task = Jbpm4Provider.getInstance().getTaskService().getTask(taskName);
//			if(removedAssign.contains(task.getAssignee()))
//				WorkflowFactory.getInstance().manualFinish(taskName, false);
//		}
//		
//		//Tworz� nowe procesy dla przydzielonych
//		for(String username : newAssign)
//		{
//			Map<String, Object> map = Maps.newHashMap();
//			map.put(ASSIGN_USER_PARAM, username);
//			map.put(ASSIGN_DIVISION_GUID_PARAM, null);
//			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart((OfficeDocument)document, map);
//			TaskSnapshot.updateByDocument(document);
//		}
//		
//		log.error("Uruchomi�em si�");
//	}

	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		FieldData fd = values.get("DWR_"+_CZAS_TRWANIA_CN);
		Field msg = null;
		
		if(fd == null)
			return msg;
		
		String duration = fd.getStringData();
		
		//ustawiam poprawny czas trwania 
		if(duration != null && !duration.isEmpty())
		{
			try
			{
				 if(!TimeUtils.checkStringTimes(duration))
				 {
					 msg = new Field("messageField", sm.getString("BlednyCzas"), Field.Type.BOOLEAN);
					 values.put("DWR_" +_CZAS_TRWANIA_CN, new FieldData(Field.Type.STRING, "01:00"));
				 }
			}catch(ArrayIndexOutOfBoundsException e){
				msg = new Field("messageField", sm.getString("BlednyCzas"), Field.Type.BOOLEAN);
				values.put("DWR_" +_CZAS_TRWANIA_CN, new FieldData(Field.Type.STRING, "01:00"));
				log.error("",e);
			}
		}
		
		return msg;
	}
	
	private boolean isElection(String type)
	{
		if(type.equals("6"))
			return true;
			
		return false;
	}
	
    public void onEndManualAssignment(OfficeDocument doc,  String fromUsername, String toUsername)
    {
    	log.info("OpzzCasesLogic: onEndManualAssignment");
    	FieldsManager fm = doc.getFieldsManager();
    	List<Long> owners = new ArrayList<Long>();
    	try
		{
			owners.add(DSUser.findByUsername(toUsername).getId());
			
			Map<String, Object> values = new HashMap<String, Object>();
			values.put(_DS_OPZZ_OWNER_CN, owners);
			values.put(_DS_OPZZ_ASSIGNED_CN, new ArrayList<Long>());
			
			doc.getDocumentKind().setWithHistory(doc.getId(), values, false);
			TaskSnapshot.updateByDocument(doc);
		} catch (UserNotFoundException e1)
		{
			log.error("", e1);
		} catch (EdmException e1)
		{
			log.error("", e1);
		}
    }
    
    @Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		if (dictionaryName.contains(_DS_OPZZ_DOC_CN))
		{
			if(values.get("id") == null)
				values.put(dictionaryName + "_CASE", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.LONG, DwrSearchable.NULLABLE));
		}
		
		if (dictionaryName.contains(_DS_OPZZ_DELEGATE_CN))
		{
			if(values.get("id") == null)
				values.put(dictionaryName + "_HIDDEN", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, "0"));
		}
	}

    
    public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			FieldsManager fm = dockind.getFieldsManager(id);
			
			params.setAttribute(fm.getStringValue(_TYTUL_CN), 0);
			
		}
		catch (Exception e)
		{
			log.error("",e);
		}
		return params;
	}
    
}
