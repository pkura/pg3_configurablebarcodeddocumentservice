package pl.compan.docusafe.parametrization.opzz;

import pl.compan.docusafe.core.users.ad.ActiveDirectoryFactory;
import pl.compan.docusafe.core.users.ad.ActiveDirectoryManager;

public class OpzzActiveDirectoryFactory extends ActiveDirectoryFactory
{

	@Override
	public String getPrincipalForPasswordCheck(ActiveDirectoryManager ad, String domainPrefix, String username)
	{
		return  "cn="+ username + ","+ad.getUsersPrincipal();
	}

}
