package pl.compan.docusafe.parametrization.opzz;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;

public class OpzzOdznakaLogic extends AbstractDocumentLogic
{
	private static OpzzOdznakaLogic instance;
	public static OpzzOdznakaLogic getInstance()
	{
		if(instance == null)
			instance = new OpzzOdznakaLogic();
		
		return instance;
	}
	
	@Override
	public void documentPermissions(Document document) throws EdmException
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException
	{
		// TODO Auto-generated method stub

	}

}
