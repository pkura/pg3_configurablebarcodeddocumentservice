package pl.compan.docusafe.parametrization.opzz;

import java.util.*;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;

import static pl.compan.docusafe.parametrization.opzz.OpzzCn.*;

public class OpzzProblemLogic extends AbstractDocumentLogic
{

	public static OpzzProblemLogic instance;
	
	public static OpzzProblemLogic getInstance()
	{
		if(instance == null)
			instance = new OpzzProblemLogic();
		
		return instance;
	}
	
	@Override
	public void documentPermissions(Document document) throws EdmException
	{

	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException
	{
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		
		super.setInitialValues(fm, type);
		
		
	}

	@Override
	public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
	{
	}
	
	

}
