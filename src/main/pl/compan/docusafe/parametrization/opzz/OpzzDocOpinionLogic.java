package pl.compan.docusafe.parametrization.opzz;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.*;

import java.sql.SQLException;
import java.util.*;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import static pl.compan.docusafe.parametrization.opzz.OpzzCn.*;

public class OpzzDocOpinionLogic extends AbstractDocumentLogic
{
	private static StringManager sm = StringManager.getManager(OpzzDocOpinionLogic.class.getPackage().getName());
	private final static Logger log = LoggerFactory.getLogger(OpzzDocOpinionLogic.class);
	private static OpzzDocOpinionLogic instance;
	private static final String OPINION_CN ="opinion";
	
	private String assinedId;
	public static OpzzDocOpinionLogic getInstance()
	{
		if (instance == null)
			instance = new OpzzDocOpinionLogic();

		return instance;
	}

	@Override
	public void documentPermissions(Document document) throws EdmException
	{
		Set<PermissionBean> perms = new HashSet<PermissionBean>();
		
		perms.add(new PermissionBean(ObjectPermission.READ, "OPZZ_DOC_OPINION_READ", ObjectPermission.GROUP, "Opiniowanie dokument�w Sprawy - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "OPZZ_DOC_OPINION_READ_ATT_READ", ObjectPermission.GROUP, "Opiniowanie dokument�w Sprawy za��cznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "OPZZ_DOC_OPINION_READ_MODIFY", ObjectPermission.GROUP, "Opiniowanie dokument�w Sprawy - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "OPZZ_DOC_OPINION_READ_ATT_MODIFY", ObjectPermission.GROUP, "Opiniowanie dokument�w Sprawy zal�cznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "OPZZ_DOC_OPINION_READ_DELETE", ObjectPermission.GROUP, "Opiniowanie dokument�w Sprawy - usuwanie"));
		
		createOrFind(document, perms);
	}
	
	
	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		super.setInitialValues(fm, type);
		
		//dodaje dokumenty sprawy do formatki
		String caseId = System.getProperty("caseId_" + DSApi.context().getPrincipalName());
		if(caseId != null && !caseId.equals("-1"))
		{
			log.error("Pobralem caseId i ustawiam:" + caseId);
			
			System.clearProperty("caseId_" + DSApi.context().getPrincipalName());
			Map<String, Object> values = new HashMap<String, Object>();
			
			//sprawa powiazana
			Document  docCase = Document.find(Long.parseLong(caseId));
			FieldsManager caseFm = docCase.getFieldsManager();
			caseFm.initialize();
			
		    List<Long> casesDoc = (List<Long>)caseFm.getKey(_DS_OPZZ_DOC_CN); 
			List<Long> docs = new ArrayList<Long>();
			
			if(casesDoc != null && !casesDoc.isEmpty())
				for(Long entry : casesDoc)
				{
					if(!entry.equals(-1L) && !docs.contains(entry))
						docs.add(entry);
				}
			
			if(docs != null && !docs.isEmpty())
				values.put(_DS_OPZZ_DOC_CN, docs);
			
			fm.reloadValues(values);
		}
	}

	@Override
	public boolean assignee(OfficeDocument doc, Assignable assignable,OpenExecution execution, String acceptationCn, String fieldCnValue)
	{
		boolean assigned = false;
		
		try
		{
			log.info("AcceptationCm: {}, enumCn: {}", acceptationCn, fieldCnValue);
			
			if(acceptationCn.equals(OPINION_CN))
			{
				List<Long> assigneds = (List<Long>) doc.getFieldsManager().getKey(_DS_OPZZ_ASSIGNED_CN);
				
				log.info("ustawiamy id opiniujacych" + assigneds);
				execution.setVariable("ids", assigneds);
				execution.setVariable("count", assigneds.size());
				execution.setVariable("count2", 2 );
				execution.setVariable("twoAcc", 2 );
				
				String assignedId = execution.getVariable("assinedId").toString();
				if(assignedId != null)
				{
					DSUser user = DSUser.findById(Long.parseLong(assignedId));
					assignable.addCandidateUser(user.getName());
					log.error("Ustawi�em uzytkownika do wpisu" + user.getName());
					assigned = true;
				}
			}
		}catch(Exception e){
			log.error("",e);
		}
		
		return assigned;
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		log.info("--- OpzzDocOpinionLogic : START PROCESS !!! ---- {}", event);
		try
		{
			FieldsManager fm = document.getFieldsManager();
			List<Long> docsId = (List<Long>) fm.getKey(_DS_OPZZ_DOC_CN);
			
			if(docsId != null && !docsId.isEmpty())
			{
				Document opzzDoc = Document.find(docsId.get(0));
				Long caseId = opzzDoc.getFieldsManager().getLongKey(_DS_OPZZ_CASES_CN);
				
				if(caseId == null)
					throw new EdmException("Dokument nie ma ustawionej sprawy!");
				
				OpzzDataMart.addOpinionToCase(document.getId(), caseId);
			}
				Map<String, Object> map = Maps.newHashMap();
				map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
				map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
				
				document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException
	{
		Folder rootFolder = Folder.getRootFolder();
		Folder folder = rootFolder.createSubfolderIfNotPresent(_DOCS_OPINION_FOLDER);
		document.setFolder(folder);
		
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("SAVED", true);
		document.getDocumentKind().setOnly(document.getId(), values);
		
//		Pismo nie ma tytu�u
//		FieldsManager fm = document.getFieldsManager();
//		document.setTitle(fm.getStringValue(_TYTUL_CN));
	}
	
	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
	{

	}

	public String getAssinedId()
	{
		return assinedId;
	}

	public void setAssinedId(String assinedId)
	{
		this.assinedId = assinedId;
	}
	
    public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			FieldsManager fm = dockind.getFieldsManager(id);
			String name = fm.getStringValue(_OPIS_CN);
			
			if(name.length() > 100)
				name = fm.getStringValue(_OPIS_CN).substring(0, 100) + "...";
			
			params.setAttribute(name, 0);
			
		}
		catch (Exception e)
		{
			log.error("",e);
		}
		return params;
	}
	
	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		FieldData fd = values.get("DWR_LINK");
		Field msg = null;
		
		if(fd == null)
			return msg;
		
		String link = fd.getStringData();
		
		//ustawiam poprawny czas trwania 
		if(link != null && !link.isEmpty() && !link.startsWith("http://"))
		{
			msg = new Field("messageField", "B��dny link do ankiety", Field.Type.BOOLEAN);
			values.put("DWR_LINK", new FieldData(Field.Type.STRING, "http://" + link.replace(" ", "")));
		}
		
		//ustawianie wartosci dla link podczas przeladowywania dokumentu,
		values.put("DWR_LINK_SHOW", new FieldData(Field.Type.HTML, "http://" + link.replace(" ", "")));
		
		return msg;
	}

	@Override
	public void onRejectedListener(Document document)
	{
		FieldsManager fm = document.getFieldsManager();
		List<Long> docsId;
		try
		{
			docsId = (List<Long>) fm.getKey(_DS_OPZZ_DOC_CN);
		
			if(docsId != null && !docsId.isEmpty())
			{
				Document opzzDoc = Document.find(docsId.get(0));
				Long caseId = opzzDoc.getFieldsManager().getLongKey(_DS_OPZZ_CASES_CN);
				
				if(caseId == null)
					throw new EdmException("Dokument nie ma ustawionej sprawy!");
				
				OpzzDataMart.addOpinionInformation(document, caseId ,"rejected");
			}
		} catch (EdmException e)
		{
			log.error("",e);
		}
	}

	@Override
	public void onAcceptancesListener(Document document, String acceptationCn)
	{
		FieldsManager fm = document.getFieldsManager();
		List<Long> docsId;
		try
		{
			docsId = (List<Long>) fm.getKey(_DS_OPZZ_DOC_CN);
		
			if(docsId != null && !docsId.isEmpty())
			{
				Document opzzDoc = Document.find(docsId.get(0));
				Long caseId = opzzDoc.getFieldsManager().getLongKey(_DS_OPZZ_CASES_CN);
				
				if(caseId == null)
					throw new EdmException("Dokument nie ma ustawionej sprawy!");
				
				OpzzDataMart.addOpinionInformation(document, caseId ,"accepted");
			}
		} catch (EdmException e)
		{
			log.error("",e);
		}
	}

	@Override
	public void specialCompareData(FieldsManager newFm, FieldsManager oldFm) throws EdmException
	{
		List<Long> assigns = (List<Long>)newFm.getKey(_DS_OPZZ_ASSIGNED_CN);
		List<Long> oldAssigns = (List<Long>)oldFm.getKey(_DS_OPZZ_ASSIGNED_CN);
		
		//sprawdza czy warto�ci zosta�y zmienione przez osob� nie upowaznion� - nie w�a�ciciel
		boolean isChange = OpzzFactory.isChange(assigns, oldAssigns);
		
		if( isChange )
			throw new EdmException("Dokument jest ju� w procesie opiniowania/zatwierdzania!");
	}
	
}
