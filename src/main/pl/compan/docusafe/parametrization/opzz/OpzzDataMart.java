package pl.compan.docusafe.parametrization.opzz;

import static pl.compan.docusafe.parametrization.opzz.OpzzCn._TYP_CN;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.HtmlUtils;

public class OpzzDataMart
{
	private final static Logger log = LoggerFactory.getLogger(OpzzDataMart.class);
	
	public static void addQuestToCase(Long questId, Long caseId) throws EdmException
	{
		//dodanie wpisu do sprawy
		StringBuilder sb2 = new StringBuilder("Dodano zadanie: ");
		Document zadanie = Document.find(questId);
		String tytul = zadanie.getFieldsManager().getStringKey(OpzzCn._TYTUL_CN);

		sb2.append(HtmlUtils.toHtmlAnchorWithName("link",Document.getArchiveUrl(questId) , questId.toString(), "history_url"));
		sb2.append(", tre�� zadania: (" + zadanie.getFieldsManager().getStringKey(OpzzCn._OPIS_CN)+ ")");
		
		DataMartManager.addHistoryEntry(caseId, Audit.create("containingCase", DSApi.context().getPrincipalName(), sb2.toString(), "caseId", questId));
	}
	
	
	public static void removeQuestToCase(Long questId, Long caseId) throws EdmException
	{
		//dodanie wpisu do sprawy
		StringBuilder sb2 = new StringBuilder("Usuni�to zadanie: ");
		Document zadanie = Document.find(questId);
		String tytul = zadanie.getFieldsManager().getStringKey(OpzzCn._TYTUL_CN);
		
		sb2.append(HtmlUtils.toHtmlAnchorWithName("link",Document.getArchiveUrl(questId) , questId.toString(), "history_url"));
		
		DataMartManager.addHistoryEntry(caseId, Audit.create("containingCase", DSApi.context().getPrincipalName(), sb2.toString(), "caseId", questId));
	}

	/**
	 * Dodanie zakonczenie zadania do sprawy
	 * @param document
	 * @param longKey
	 * @throws EdmException 
	 */
	public static void addFinishQuestToCase(Document document, Long caseId) throws EdmException
	{
		StringBuilder sb2 = new StringBuilder("Zako�czono zadanie: ");
		String tytul = document.getFieldsManager().getStringKey(OpzzCn._TYTUL_CN);
		
		sb2.append(HtmlUtils.toHtmlAnchorWithName("link", Document.getArchiveUrl(document.getId()) , document.getId().toString(), "history_url"));
		
		DataMartManager.addHistoryEntry(caseId, Audit.create("containingCase", DSApi.context().getPrincipalName(), sb2.toString(), "caseId", document.getId()));
	}
	
	/**
	 * Dodaje do sprawy informacje ze dokument jest do zaopiniowania
	 * @param opinionId
	 * @param caseId
	 * @throws EdmException
	 */
	public static void addOpinionToCase(Long opinionId, Long caseId) throws EdmException
	{
		StringBuilder sb2 = new StringBuilder("Dodano dokumenty do zaopiniowania/zatwierdzenia: ");
		String baseUrl = Document.getArchiveUrl(opinionId);
		Document opinia = Document.find(opinionId);
		String opis = opinia.getFieldsManager().getStringValue(OpzzCn._OPIS_CN);
		//typ opiniowania zatwierdzania
		String type = opinia.getFieldsManager().getStringKey(_TYP_CN);
		
		sb2.append(HtmlUtils.toHtmlAnchorWithName("link",baseUrl , opinionId.toString() , "history_url"));
		
		if(type.equals("1"))
		{
			String link = opinia.getFieldsManager().getStringKey("LINK");
			sb2.append(", " +HtmlUtils.toHtmlAnchor(link, "link do ankiety"));
		}
		
		sb2.append(", tre�� pro�by: " + opis);
		
		try
		{
			DataMartManager.addHistoryEntry(caseId, Audit.create("containingCase", DSApi.context().getPrincipalName(), sb2.toString(), "caseId", opinia.getId()));
		} catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
	}
	
	/**
	 *  Dodaje informacje do sprawy o wydarzeniu
	 * @throws EdmException 
	 * @throws UserNotFoundException 
	 */
	public static void addOpinionInformation(Document opinion, Long caseId, String type)
	{
		try
		{
			StringBuilder sb = new StringBuilder();
			
			if(type.equals("rejected"))
			{
				sb.append("Odrzucenie dokument�w: ")
				   .append(HtmlUtils.toHtmlAnchorWithName("link",Document.getArchiveUrl(opinion.getId()) , opinion.getId().toString() , "history_url"))
				   .append(", przez u�ytkownika:" + DSApi.context().getDSUser().asLastnameFirstname());
			}
			else if(type.equals("accepted")) //accepted
			{
				sb.append("Akceptacja dokument�w: ")
				   .append(HtmlUtils.toHtmlAnchorWithName("link",Document.getArchiveUrl(opinion.getId()) , opinion.getId().toString() , "history_url"))
				   .append(", przez u�ytkownika:" + DSApi.context().getDSUser().asLastnameFirstname());
			}
			else if(type.equals("opinion"))
			{
				sb.append("Zaopiniowano dokumenty: ")
				   .append(HtmlUtils.toHtmlAnchorWithName("link",Document.getArchiveUrl(opinion.getId()) , opinion.getId().toString() , "history_url"))
				   .append(", przez u�ytkownika:" + DSApi.context().getDSUser().asLastnameFirstname())
				   .append(" o tre�ci: " + opinion.getLastUserRemark(DSApi.context().getPrincipalName()).getContent());
			}else //opinia ankiet
			{
				String link = opinion.getFieldsManager().getStringKey("LINK");
				sb.append("Zaopiniowano poprzez ankiet� dokumenty: ")
				   .append(HtmlUtils.toHtmlAnchorWithName("link",Document.getArchiveUrl(opinion.getId()) , opinion.getId().toString() , "history_url"))
				   .append(", " +HtmlUtils.toHtmlAnchor(link, "link do ankiety"))
				   .append(", przez u�ytkownika:" + DSApi.context().getDSUser().asLastnameFirstname());
			}
			
			DataMartManager.addHistoryEntry(caseId, Audit.create("containingCase", DSApi.context().getPrincipalName(), sb.toString(), "caseId", opinion.getId()));
			
		}catch(EdmException e){
			log.error("", e);
		}
	}
}
