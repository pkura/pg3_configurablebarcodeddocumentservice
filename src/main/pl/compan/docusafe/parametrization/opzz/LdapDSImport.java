package pl.compan.docusafe.parametrization.opzz;

import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchResult;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.ad.ActiveDirectoryManager;
import static pl.compan.docusafe.service.Console.*;
import pl.compan.docusafe.parametrization.opzz.dictionary.OrganizationUserDao;
import pl.compan.docusafe.parametrization.opzz.dictionary.OrganizationUserDictionary;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.mail.MailerDriver;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.eo.opzz.portlet.service.dto.OrganizationDto;

/**
 * Serwis do pobierania uzytkownik�w i struktury organizacyjnej
 * Podzia� ldap jest podzielony na People oraz Groups
 * 
 * Przyk�ad wymagany w addsach:
 * active.directory.url=ldap://77.252.99.34:389
	active.directory.on = true
active.directory.addCnUsers = false
active.directory.mainDomainPrefix=opzz-dev.demo.eo.pl
active.directory.adminUsername=adminrw
active.directory.adminPass=OhGhee3a
active.directory.mainDomainTag=o
active.directory.addCnUsersName=People
active.directory.addCnUsersTag=ou
active.directory.addUserLoginTag=uid
active.directory.addCnGroupsName=Groups
active.directory.addCnGroupsTag=ou
active.directory.factory=pl.compan.docusafe.parametrization.opzz.OpzzActiveDirectoryFactory
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 */
public class LdapDSImport extends ServiceDriver implements Service
{
	private final static Logger log = LoggerFactory.getLogger(LdapDSImport.class);
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(MailerDriver.class.getPackage().getName(),null);

	private Timer timer;
	private Property[] properties;
	private Long period = 25L;
	private String filter = "";
	private String divisionAttrName = "";
	private String emailAttrName = "mail";
	
    class LdapTimesProperty extends Property
    {
        public LdapTimesProperty()
        {
            super(SIMPLE, PERSISTENT, LdapDSImport.this, "period", "Czestotliwo�c synchronizacji w minutach", String.class);
        }

        protected Object getValueSpi()
        {
            return period;
        } 

        protected void setValueSpi(Object object) throws ServiceException
        {
            synchronized (LdapDSImport.this)
            {
            	if(object != null)
            	{
            		period = Long.parseLong((String) object);
            	}
            	else
            	{
            		period = 240L;
            	}
            
            }
        }
    }
    
    class MailAttrNameProperty extends Property{
        public MailAttrNameProperty(){
            super(SIMPLE, PERSISTENT, LdapDSImport.this, "emailAttrName", "emailAttrName", String.class);
        }
        protected Object getValueSpi(){
            return emailAttrName;
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (LdapDSImport.this){
            	if(object == null)
            		emailAttrName = "mail";
            	else
            		emailAttrName = (String) object;
            }
        }
    }
    
    class DivisionAttrNameProperty extends Property{
        public DivisionAttrNameProperty(){
            super(SIMPLE, PERSISTENT, LdapDSImport.this, "divisionAttrName", "divisionAttrName", String.class);
        }
        protected Object getValueSpi(){
            return divisionAttrName;
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (LdapDSImport.this){
            	if(object == null)
            		divisionAttrName = "divisionName";
            	else
            		divisionAttrName = (String) object;
            }
        }
    }
    
    class FilterProperty extends Property
    {
        public FilterProperty()
        {
            super(SIMPLE, PERSISTENT, LdapDSImport.this, "filter", "Filtr", String.class);
        }

        protected Object getValueSpi()
        {
            return filter;
        } 

        protected void setValueSpi(Object object) throws ServiceException
        {
            synchronized (LdapDSImport.this)
            {
            	filter = (String) object;
            }
        }
    }
    
    public LdapDSImport()
    {
    	properties = new Property[] { new LdapTimesProperty() ,new FilterProperty(),new DivisionAttrNameProperty(),new MailAttrNameProperty()};
    }
    
	@Override
	protected void start() throws ServiceException
	{
		log.info("Start us�ugi LdapDSImport");
		timer = new Timer(true);
		timer.schedule(new Import(), (long) 2*1000 ,(long) period*DateUtils.MINUTE);
		console(INFO, "Start us�ugi LdapDSImport");
	}

	@Override
	protected void stop() throws ServiceException
	{
		log.info("stop uslugi");
		console(INFO, sm.getString("StopUslugi"));
		if(timer != null) timer.cancel();
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	/**
	 * Importer do LDapa
	 */
	class Import extends TimerTask
	{

		@Override
		public synchronized void run()
		{
			ActiveDirectoryManager ad = null;
			DirContext context = null;
			
			try
			{
				console(INFO, "START import ds");
				DSApi.openAdmin();
				console(INFO, "Laczy z AD");
				ad = new ActiveDirectoryManager();
				context = ad.LdapConnectAsAdmin();
				console(INFO, "Polaczyl sie z AD");
				
				//dodaje uzytkownik�w
				NamingEnumeration<NameClassPair> attrs = context.list(ad.getUsersPrincipal());
				console(INFO, "Listuje user�w");
				while(attrs.hasMore())
				{
					try
					{
						DSApi.context().begin();
						NameClassPair element = attrs.next();
						Attributes elementAttribute = context.getAttributes(element.getName() + "," + ad.getUsersPrincipal());
						DSUser user = getUser(elementAttribute);
						
						saveOrganization(user.getName());
						
						DSApi.context().session().saveOrUpdate(user);
						DSApi.context().commit();
					}
					catch (Exception e) 
					{
						log.error("",e);
						console(Console.ERROR, "B��d [ "+e+" ]");
						DSApi.context().rollback();
					}
					finally{
						try {
							DSApi.context().session().flush();
						}catch(Exception e) {
							log.error("B��d czyszczenia sesji");
						}
					}
				}
				
				DSApi.close();
				DSApi.openAdmin();
				console(INFO, "koniec listowania user�w");
				//dodaje grupy
				NamingEnumeration<NameClassPair> attrGroup = context.list(ad.getGroupsPrincipal());
				console(INFO, "listuje grupy");
				while(attrGroup.hasMore())
				{
					try
					{
						DSApi.context().begin();
						NameClassPair element = attrGroup.next();
						Attributes elementAttribute = context.getAttributes(element.getName() + "," + ad.getGroupsPrincipal());
						DSDivision division = getGroup(elementAttribute, context);
						
						DSApi.context().session().save(division);
						DSApi.context().commit();
					}
					catch (Exception e) 
					{
						log.error("",e);
						console(Console.ERROR, "B��d [ "+e+" ]");
						DSApi.context().rollback();
					}
				}
				
				console(INFO, "Koniec listowania grup");
				
			}catch(Exception e)
			{
				log.error("",e);
				console(ERROR, "B��d [ "+e+" ]");
			}
			finally
			{
				
				log.trace("Import STOP");
				console(INFO, "Koniec synchronizacji");
				if(context != null)
					try {
						context.close();
					} catch (NamingException e) {
						log.error(e.getMessage(),e);
					}
				DSApi._close();
			}
		}

		private String getStringAttr(Attributes element, String name) throws NamingException
		{
			if(element.get(name) != null)
			{
				return (String) element.get(name).get();
			}
			
			return null;
		}
		
		//Tworzy usera jesli jest updatuje go
		private DSUser getUser(Attributes element) throws Exception
		{
			DSUser user = null;
			String name = getStringAttr(element, "cn");
			String firstname = getStringAttr(element, "givenName");
			String lastname = getStringAttr(element, "sn");
			String email = getStringAttr(element, "mail");
			try
			{
				user = DSUser.findByUsername(name);
				console(INFO, "Znalazl "+ user.asFirstnameLastname());
				user.setFirstname(firstname);
				user.setLastname(lastname);
				user.setEmail(email);
				user.update();
				
				try
				{
					//TODO: sprawdzi�
					Role role = Role.find(5L);
					if(role == null)
						role = Role.findByName(Docusafe.getAdditionProperty("pracownik.kancelarii.rola.nazwa"));
					role.addUser(name);
				    
				    DSApi.context().session().saveOrUpdate(role);
				    
					console(INFO, "Dodaje role Pracownik kancelarii: " + name);
				}catch(EdmException e){
					console(INFO, "blad dodaniawa roli:" + name);
				}
			}
			catch (UserNotFoundException e)
			{
				console(INFO, "nie znalazl usera: "+name);
				user = UserFactory.getInstance().createUser(name, firstname, lastname);
				user.setEmail(email);
				user.setCtime(new Timestamp(new Date().getTime()));
				user.setAdUser(true);
				user.setLoginDisabled(false);								
				console(INFO, "Dodal "+user.asFirstnameLastname());
			}
			
			return user;
		}
		
		/**
		 * Zapisuje organizacje u�ytkownika
		 * @param username
		 * @throws Exception
		 */
		public void saveOrganization(String username) throws Exception
		{
			try
			{
				updateUserOrganization(username);
				OrganizationDto[] organizations = RemoteOpzzFactory.getOrganizationsForUser(username);
				for(OrganizationDto organization : organizations)
				{
					/** odnajduj� organizacj� usera w bazie */
					OrganizationUserDictionary orgnDictionary = OrganizationUserDao.find(username, organization.getId());
					
					if(orgnDictionary != null) //istnieje w bazie aktualizuj� nazw� organizacji
					{
						DSApi.context().session().refresh(orgnDictionary);
						orgnDictionary.setOrganizationName(organization.getName());
						console(INFO,"ORGANIZATION_USER save: [" + username + ","+ organization.getId()+"]");
						orgnDictionary.setHidden(false);
						
					}
					else // puste tworze now�
					{
						orgnDictionary = new OrganizationUserDictionary(username, organization.getId(), organization.getName()); 
						console(INFO,"ORGANIZATION_USER update: [" + username + ","+ organization.getId()+"]");
						orgnDictionary.setHidden(false);
					}
					
					DSApi.context().session().saveOrUpdate(orgnDictionary);
				}
			}catch(Exception e){
				log.error("", e);
				console(INFO, "blad w ustawianiu organizacji usera");
			}
		}
		
		public void updateUserOrganization(String username) throws Exception
		{
			PreparedStatement ps = DSApi.context().prepareStatement("UPDATE DS_OPZZ_ORG_USER SET hidden = 1 where username =?");
			ps.setString(1, username);
			try
			{
				int rows = ps.executeUpdate();
				console(INFO, "ustawiam hidden wpisy u�ytkowniki: " + username + ":" + rows);
			}finally{
				ps.close();
			}
		}
		
		//tworzy grupe jesli nie ma jej
		private DSDivision getGroup(Attributes element, DirContext context) throws Exception
		{
			String divisionName = TextUtils.latinize(getStringAttr(element,"cn"));
			String divisionDesc = getStringAttr(element,"description");
			
			DSDivision division = null;
			try 
			{
				
				division = DSDivision.find(divisionName);
				console(INFO, "Znalazlem grupe: " + divisionName);
			}
			catch (Exception e) 
			{
				try
				{
					//wyszukuje po externalId w oraclu co� si� jako� inaczej doda�o
					division = DSDivision.findByExternalId(divisionName);
					console(INFO, "Znalazlem grupe po externalId: " + divisionName);
				}catch(Exception e2)
				{
					try{
						console(INFO, "brak grupy: " + divisionName);
						DSDivision parent = DSDivision.find(DSDivision.ROOT_GUID);
						division = parent.createDivision(divisionDesc == null ? divisionName : divisionDesc, null,divisionName);
						console(INFO, "tworz�: " + divisionName);
					}catch (Exception ex) {
							log.error("",ex);
					}
				}
			}
			
			@SuppressWarnings("unchecked")
			NamingEnumeration<String> members = (NamingEnumeration<String>) element.get("member").getAll();

			while (members.hasMore())
			{
				String member = members.next();
				Attributes ctxUser = null;
				try
				{
					//context user
					ctxUser = context.getAttributes(member);
					DSUser user = DSUser.findByUsername(getStringAttr(ctxUser,"cn"));
					DSApi.context().session().refresh(division);
					DSApi.context().session().refresh(user);
					division.addUser(user);
					console(INFO, "dodalem [user, grupa]:" + user.getName() + "," + divisionName);
					
				}catch (UserNotFoundException e) {
					console(INFO, "Brak u�ytkownika:" + getMemberName(member)+","+divisionName);
					log.error("",e);
				}catch(Exception e) {
					console(INFO, "B��d w dodawaniu do grupy" + getMemberName(member) + "," + divisionName);
					log.error("",e);
				}finally{
					try{
						DSApi.context().session().flush();
					}catch(Exception e){ log.error("Blad czyszczenia sesji");}
				}
				
			}
			
			return division;
		}
		
		/**
		 * Zwraca nazw� u�ytkownika. member ma posta� : cn=test.1,ou=People,o=opzz-dev.demo.eo.pl
		 * @param member
		 */
		private String getMemberName(String member)
		{
			String[] memberTable = member.split(",");
			String username = null;
			//ma format "cn=name"
			if(memberTable[0] != null)
			{
				String[] userName = memberTable[0].split("="); 
				
				if(userName[1] != null)
					username = userName[1];
			}
			return username;
		}
	}

	public boolean hasConsole()
	{
		return true;
	}
	
	public Property[] getProperties() {
		return properties;
	}

	public void setProperties(Property[] properties) {
		this.properties = properties;
	}
	
}
