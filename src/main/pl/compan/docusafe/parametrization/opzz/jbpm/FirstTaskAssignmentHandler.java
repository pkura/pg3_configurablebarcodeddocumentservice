package pl.compan.docusafe.parametrization.opzz.jbpm;

import java.util.List;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class FirstTaskAssignmentHandler implements AssignmentHandler
{
	private final static Logger LOG = LoggerFactory.getLogger(FirstTaskAssignmentHandler.class);
	@Override
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception
	{
		LOG.info("assign !!");
        String assignee = (String) openExecution.getVariable(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY);
        if(assignee != null){
            assignable.addCandidateUser(assignee);
            assignable.setAssignee(assignee);
        } else {
            List<String> targetUsernames = (List<String>) openExecution.getVariable(Jbpm4Constants.READ_ONLY_ASSIGNMENT_USERNAMES);
            List<String> targetGuids = (List<String>) openExecution.getVariable(Jbpm4Constants.READ_ONLY_ASSIGNMENT_GUIDS);
            if(targetGuids != null){
                for(String guid: targetGuids){
                    assignable.addCandidateGroup(guid);
                }
            }
            
            if(targetUsernames != null){
                for(String username: targetUsernames){
                    assignable.addCandidateUser(username);
                }
            }
        }
	}

}
