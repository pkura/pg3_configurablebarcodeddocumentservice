package pl.compan.docusafe.parametrization.opzz.jbpm.cases;

import java.util.List;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.parametrization.opzz.OpzzCn;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa sprawdza na kt�rej li�cice znajduje si� u�ytkownik.
 * Je�li jest na li�cie w�a�cicieli przekazuje go na task jako w�a�ciciela.
 * @author �ukasz
 *
 */
public class CheckCasesLists implements DecisionHandler 
{
	
	private static final String OWNER_TASK = "to owner-task";
	private static final String ASSIGN_TASK = "to assign-task";
	private final static Logger log = LoggerFactory.getLogger(CheckCasesLists.class);
	
	@Override
	public String decide(OpenExecution execution)
	{
		log.info("podejmuje decyzje w sprawie zadania");
		String results = ASSIGN_TASK;
		
		try {
			//u�ytkownik przekazany w danej petli
			String userId = execution.getVariable("userId").toString();
			Document document = Jbpm4Utils.getDocument(execution);
			List<Long> owners = (List<Long>) document.getFieldsManager().getKey(OpzzCn._DS_OPZZ_OWNER_CN);
			List<Long> assigns = (List<Long>) document.getFieldsManager().getKey(OpzzCn._DS_OPZZ_ASSIGNED_CN);
			
			Boolean assignee = false;
			//czy jest na liscie w�ascicieli
			if(owners != null && !owners.isEmpty())
				for(Long owner : owners)
				{
					if(owner.equals(Long.valueOf(userId)))
					{
						//TODO: doda� usuwanie z listy przydzielonych
						results = OWNER_TASK;
						assignee = true;
					}
				}
			
			//XXX: czy sprawdza� czy jest na li�cie przydzielonych
			
		} catch (Exception e) {
			log.error("", e);
		}
		
		return results;
	}

}
