package pl.compan.docusafe.parametrization.opzz.jbpm.cases;

import java.util.Map;

import org.jbpm.api.Execution;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Sprawdza czy w�a�ciciel zako�czy� czy przydzieli� na nowo
 * @author �ukasz
 *
 */
public class CheckBothForkVariables implements ExternalActivityBehaviour
{
	private final static Logger log = LoggerFactory.getLogger(CheckBothForkVariables.class);
	
	@Override
	public void execute(ActivityExecution execution) throws Exception 
	{
//		Boolean startFromBegin = Boolean.parseBoolean(execution.getVariable("startFromBegin").toString());
//		log.error("wartosc startFromBegin: " + startFromBegin);
		execution.setVariable("counts",2);
		boolean isAccepted = false;
		
		for (Execution ex : execution.getProcessInstance().getExecutions())
		{
			if (ex.getState().equals("inactive-join"))
				isAccepted = true;
		}
		
//		if (!isAccepted || ( startFromBegin != null && startFromBegin))
		if (!isAccepted)
		{
			log.info("ustawiam counts na 1");
			execution.setVariable("counts", 1);
			
		}
		
		log.info("Zamykam wszystkie procesy w forku");
	}

	@Override
	public void signal(ActivityExecution execution, String arg1, Map<String, ?> arg2)
			throws Exception {
		
	}
	
}
