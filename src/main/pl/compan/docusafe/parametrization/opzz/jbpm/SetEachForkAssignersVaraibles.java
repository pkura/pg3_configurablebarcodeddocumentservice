package pl.compan.docusafe.parametrization.opzz.jbpm;

import static pl.compan.docusafe.parametrization.opzz.OpzzCn.*;

import java.util.*;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessDefinition;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa tworzy parametry dla forka, bazujac na przydzielonych
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 */
public class SetEachForkAssignersVaraibles implements ExternalActivityBehaviour
{
	private final static Logger log = LoggerFactory.getLogger(SetEachForkAssignersVaraibles.class);
	private static final long serialVersionUID = 1496156413078756158L;

	@SuppressWarnings("unchecked")
	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		OfficeDocument document = Jbpm4Utils.getDocument(execution);
		finishTask(document.getId());
		
		List<Long> assigned = (List<Long>)document.getFieldsManager().getKey(_DS_OPZZ_ASSIGNED_CN);
		
		final Set<Long> args = new HashSet<Long>();
		
		if(assigned != null && !assigned.isEmpty())
			for(Long id : assigned)
			{
				if(id > 0)
				{
					args.add(id);
				}
			}

		if(args != null)
		{
			execution.setVariable("ids_assigns", args);
			execution.setVariable("count_assigns", args.size());
			execution.setVariable("count2_assigns", 2);
		}
		else
		{
			execution.setVariable("ids_assigns", null);
			execution.setVariable("count_assigns", 0);
			execution.setVariable("count2_assigns", 2);
		}
			
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		
	}
	
	public static boolean finishTask(Long documentId) throws EdmException
	{
		List<Long> ids = new ArrayList<Long>();
		ids.add(documentId);
		return finishTask(ids);
	}
	
	public static boolean finishTask(List<Long> ids) throws EdmException
	{
		for (Long id : ids)
		{
			WorkflowFactory wf = WorkflowFactory.getInstance();
            if(wf instanceof InternalWorkflowFactory){
                String activityId = wf.findManualTask(id);
                wf.manualFinish(activityId, false);
            } else if(wf instanceof Jbpm4WorkflowFactory) {
            	try {
            		Iterable<String> tasks = Jbpm4ProcessLocator.taskIds(id);
            		OfficeDocument doc = OfficeDocument.find(id);
        			ProcessDefinition pdef = doc.getDocumentKind().getDockindInfo()
        					.getProcessesDeclarations().getProcess("opzz_cases_2");
        			List<? extends ProcessInstance> pi = new ArrayList<ProcessInstance>(pdef.getLocator().lookupForDocument(doc));
                    //log.info("begin context");
                    pl.compan.docusafe.core.dockinds.process.ProcessAction pa;
                    
                    for(String task : tasks)
                    {
                    	try
                    	{
                    		pdef.getLogic().process(
                    				pi.get(0),
                    				pa = ProcessActionContext
                                    	.action("to changeMulti assigners")
                                    	.document(doc)
                                    	.stored(true)
                                    	.activityId(task));
                    	}catch (Exception e) {
                    		log.error("assign blad",e);
						}
                    }
            	}
            	catch (NoSuchElementException e) {
            		log.warn("Dokument {} jest juz zamkniety",id);
            	}
            } else {
                throw new IllegalStateException("nieznany WorkflowFactory");
            }
		}
		return true;
	}

}
