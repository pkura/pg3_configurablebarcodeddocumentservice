package pl.compan.docusafe.parametrization.opzz.jbpm.cases;

import java.util.Collections;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * sprawdza czy u�ytkownik zamknal zadanie czy przydzieli� na nowo
 * @author �ukasz
 *
 */
public class EndCaseDecisiton implements DecisionHandler 
{
	private final static Logger log = LoggerFactory.getLogger(EndCaseDecisiton.class);
	@Override
	public String decide(OpenExecution execution) 
	{
		
		Boolean startFromBegin = Boolean.parseBoolean(execution.getVariable("startFromBegin").toString());
		OfficeDocument document;
		try {
			document = Jbpm4Utils.getDocument(execution);
			
		log.error("EndCaseDecision: " + startFromBegin);
		log.error("EndCaseDecision: " + (startFromBegin != null && startFromBegin));
		if(startFromBegin != null && startFromBegin)
			return "to all-users";
		
		} catch (EdmException e) {
			log.error("", e);
		}
		
		return "to end";
		
	}

}
