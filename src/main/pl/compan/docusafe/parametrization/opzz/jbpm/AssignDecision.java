package pl.compan.docusafe.parametrization.opzz.jbpm;

import java.util.*;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import static pl.compan.docusafe.parametrization.opzz.OpzzCn.*;

public class AssignDecision  implements DecisionHandler
{
	private static Logger log = LoggerFactory.getLogger(ProcessParamsAssignmentHandler.class);

	@Override
	public String decide(OpenExecution execution)
	{
		String result = "to assigners-dynamic";
		try
		{
			Document document = Jbpm4Utils.getDocument(execution);
			List<Long> assigns = (List<Long>) document.getFieldsManager().getKey(_DS_OPZZ_ASSIGNED_CN);
			
			final Set<Long> args = new HashSet<Long>();
			
			if(assigns != null && !assigns.isEmpty())
				for(Long assign : assigns)
				{
					if(assign > 0)
						args.add(assign);
				}
			
			log.error("wielkosc assigns w decyzji: {}" + args);
			if(args == null || args.isEmpty())
			{
				execution.setVariable("count_assigns", 1);
				result = "to changeMulti finish fork";
			}
			
		}catch(Exception e)
		{
			log.error("blad w podejmowaniu decyzji", e);
		}
		
		return result;
	}

}
