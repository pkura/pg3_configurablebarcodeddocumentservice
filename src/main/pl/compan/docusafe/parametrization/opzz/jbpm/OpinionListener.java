package pl.compan.docusafe.parametrization.opzz.jbpm;

import static pl.compan.docusafe.parametrization.opzz.OpzzCn._DS_OPZZ_CASES_CN;
import static pl.compan.docusafe.parametrization.opzz.OpzzCn._DS_OPZZ_DOC_CN;

import java.util.Date;
import java.util.List;

import org.jbpm.api.listener.EventListenerExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.opzz.OpzzCn;
import pl.compan.docusafe.parametrization.opzz.OpzzDataMart;

public class OpinionListener  extends AbstractEventListener
{
	private static final Logger log = LoggerFactory.getLogger(OpinionListener.class);
	@Override
	public void notify(EventListenerExecution openExecution) throws Exception
	{
		OfficeDocument doc =Jbpm4Utils.getDocument(openExecution);
		
		// Potwierdzenie przeczytania - infomracja dla użytkownika
		AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
		ahe.setSourceUser(DSApi.context().getPrincipalName());
		ahe.setProcessName(openExecution.getProcessDefinitionId());
		ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_CONFIRMED);
		String divisions = "";
		for (DSDivision a : DSUser.findByUsername(DSApi.context().getPrincipalName()).getOriginalDivisionsWithoutGroup())
		{
			if (!divisions.equals(""))
				divisions = divisions + " / " + a.getName();
			else
				divisions = a.getName();
		}
		ahe.setSourceGuid(divisions);
//		ahe.setStatus(doc.getFieldsManager().getValue("STATUS").toString());
		
		ahe.setCtime(new Date());
//		ahe.setFinished(true);
		ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_CC);
		doc.addAssignmentHistoryEntry(ahe);
		
		FieldsManager fm = doc.getFieldsManager();
		List<Long> docsId;
		try
		{
			docsId = (List<Long>) fm.getKey(_DS_OPZZ_DOC_CN);
		
			if(docsId != null && !docsId.isEmpty())
			{
				Document opzzDoc = Document.find(docsId.get(0));
				Long caseId = opzzDoc.getFieldsManager().getLongKey(_DS_OPZZ_CASES_CN);
				
				if(caseId == null)
					throw new EdmException("Dokument nie ma ustawionej sprawy!");
				
				String type = fm.getStringKey(OpzzCn._TYP_CN);
				
				if(type.equals("1")) //ankieta
					OpzzDataMart.addOpinionInformation(doc, caseId ,"opinion_link");
				else
					OpzzDataMart.addOpinionInformation(doc, caseId ,"opinion");
					
			}
		} catch (EdmException e)
		{
			log.error("",e);
		}
	}

}
