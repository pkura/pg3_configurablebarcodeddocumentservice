package pl.compan.docusafe.parametrization.opzz.jbpm;

import java.util.Map;

import org.jbpm.api.Execution;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class FromBeginMultplicityValueForJoin  implements ExternalActivityBehaviour 
{
	
	private final static Logger log = LoggerFactory.getLogger(FromBeginMultplicityValueForJoin.class);
	
	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		execution.setVariable("correction", 11 );
		boolean isAccepted = false;
		for (Execution ex : execution.getProcessInstance().getExecutions())
		{
			if (ex.getState().equals("inactive-join"))
				isAccepted = true;
		}
		if (!isAccepted)
		{
			execution.setVariable("count_owners", 1);
			execution.setVariable("count_assigns", 1);
		}
			

		log.info("Zamykamy wszystkie subprocesy utworzone w fork'u i ustawiamy warto�� 'correction' na 11");
		
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
	}

}
