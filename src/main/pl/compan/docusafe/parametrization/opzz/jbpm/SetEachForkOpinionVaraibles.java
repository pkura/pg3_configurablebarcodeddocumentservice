package pl.compan.docusafe.parametrization.opzz.jbpm;

import java.util.*;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import static pl.compan.docusafe.parametrization.opzz.OpzzCn.*;

public class SetEachForkOpinionVaraibles implements ExternalActivityBehaviour
{
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(SetEachForkOpinionVaraibles.class);
	
	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		Document document = Jbpm4Utils.getDocument(execution);
		
		log.error("ustawiam dynamicznego forka");
		//lista uzytkownik�w do kt�ych jest przekazany dokument do wiadomo�ci
		List<Long> assigneds = (List<Long>) document.getFieldsManager().getKey(_DS_OPZZ_ASSIGNED_CN);
		
		final Set<Long> args = new HashSet<Long>();
		
		for(Long assign : assigneds)
		{
			if(assign > 0)
				args.add(assign);
		}
		execution.setVariable("ids", args);
		execution.setVariable("count", args.size());
		execution.setVariable("count2", 2 );
		execution.setVariable("twoAcc", 2 );
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception
	{
	}

}
