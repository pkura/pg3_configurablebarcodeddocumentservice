package pl.compan.docusafe.parametrization.opzz.jbpm;

import java.util.*;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessDefinition;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import static pl.compan.docusafe.parametrization.opzz.OpzzCn.*;

/**
 * Klasa tworzy parametry dla forka bazuj�c ja ilo�ci w�a�cicieli
 * jesli jest brak w�a�cicieli ustawia autora 
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class SetEachForkOwnersVaraibles  implements ExternalActivityBehaviour
{
	private static final long serialVersionUID = 6852101128366071247L;
	private final static Logger log = LoggerFactory.getLogger(SetEachForkOwnersVaraibles.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		OfficeDocument document = Jbpm4Utils.getDocument(execution);
		finishTask(document.getId());
		
		List<Long> owners = (List<Long>)document.getFieldsManager().getKey(_DS_OPZZ_OWNER_CN);
		
		final Set<Long> args = new HashSet<Long>();
		
		if(owners != null && owners.isEmpty())
		for(Long id : owners)
		{
			if(id > 0)
			{
				args.add(id);
			}
		}
		
		//jesli nie ma wlascicieli wybranych jest tylko autor
		if(args.isEmpty())
		{
			args.add(DSUser.findByUsername(document.getAuthor()).getId());
			//TODO: dopisac zapis w pismie autora jako wla�ciciela
		}
		
		execution.setVariable("ids_owners", args);
		execution.setVariable("count_owners", args.size());
		execution.setVariable("count2_owners", 2);
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
	}
	
	public static boolean finishTask(Long documentId) throws EdmException
	{
		List<Long> ids = new ArrayList<Long>();
		ids.add(documentId);
		return finishTask(ids);
	}
	
	public static boolean finishTask(List<Long> ids) throws EdmException
	{
//		for (Long id : ids)
//		{
//			WorkflowFactory wf = WorkflowFactory.getInstance();
//            if(wf instanceof InternalWorkflowFactory){
//                String activityId = wf.findManualTask(id);
//                wf.manualFinish(activityId, false);
//            } else if(wf instanceof Jbpm4WorkflowFactory) {
//            	try {
//            		Iterable<String> tasks = Jbpm4ProcessLocator.taskIds(id);
//            		OfficeDocument doc = OfficeDocument.find(id);
//        			ProcessDefinition pdef = doc.getDocumentKind().getDockindInfo()
//        					.getProcessesDeclarations().getProcess("opzz_cases_2");
//        			List<? extends ProcessInstance> pi = new ArrayList<ProcessInstance>(pdef.getLocator().lookupForDocument(doc));
//                    //log.info("begin context");
//                    pl.compan.docusafe.core.dockinds.process.ProcessAction pa;
//                    
//                    for(String task : tasks)
//                    {
//                    	try
//                    	{
//                    		pdef.getLogic().process(
//                    				pi.get(0),
//                    				pa = ProcessActionContext
//                                    	.action("to changeMulti owners")
//                                    	.document(doc)
//                                    	.stored(true)
//                                    	.activityId(task));
//                    	}catch (Exception e) {
//                    		log.error("assign blad",e);
//						}
//                    }
//            	}
//            	catch (NoSuchElementException e) {
//            		log.warn("Dokument {} jest juz zamkniety",id);
//            	}
//            } else {
//                throw new IllegalStateException("nieznany WorkflowFactory");
//            }
//		}
		return true;
	}

}
