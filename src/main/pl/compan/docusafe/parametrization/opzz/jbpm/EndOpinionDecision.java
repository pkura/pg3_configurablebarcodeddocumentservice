package pl.compan.docusafe.parametrization.opzz.jbpm;

import java.util.List;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class EndOpinionDecision implements DecisionHandler
{
	private final static Logger log = LoggerFactory.getLogger(EndOpinionDecision.class);
	
	public String decide(OpenExecution openExecution)
	{
		Long documentId = Jbpm4Utils.getDocumentId(openExecution);
		String decision = "dalej";
		
		try
		{
			List<JBPMTaskSnapshot> tasks = JBPMTaskSnapshot.findByDocumentId(documentId);
			int position = -1;
			
			for(JBPMTaskSnapshot task : tasks)
			{
				if(task.getDsw_resource_res_key().equals(DSApi.context().getPrincipalName()))
					position = tasks.indexOf(task);
			}
			
			if(position >= 0)
				tasks.remove(position);
			
			if(tasks != null && tasks.size() == 0)
			{
				//TODO: wys�a� maila
				decision = "koniec";
			}
			
		}catch (Exception e) {
			log.error("blad przy decyzji",e);
		}
		
		return decision;
	}
}