package pl.compan.docusafe.parametrization.opzz.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import static pl.compan.docusafe.parametrization.opzz.OpzzCn.*;

/**
 * Klasa podejmuje decyzje jakiego typu jest dokument
 * @author �ukasz Wo�niak
 */
public class CheckOpinionDocumentType implements DecisionHandler
{
	private final static Logger log = LoggerFactory.getLogger(CheckOpinionDocumentType.class);
	
	public String decide(OpenExecution openExecution)
	{
		String decision = "opinia";
		try
		{
			Document document = Jbpm4Utils.getDocument(openExecution);
			String type = document.getFieldsManager().getStringKey(_TYP_CN);
			
			log.info("typ dokumenty jest r�wny" + type);
			
			if(type.equals("1"))
				decision = "opinia z ankieta";
			
			if(type.equals("2"))
				decision="zatwierdzanie";
			
		}catch (Exception e) {
			log.error("blad przy decyzji",e);
		}
		
		return decision;
	}
    
}
