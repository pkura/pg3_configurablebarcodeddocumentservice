package pl.compan.docusafe.parametrization.opzz.jbpm;

import org.jbpm.api.Execution;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLogicSupport;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import java.util.Map;

/**
 *  Zmienia warto�� dla forka
 * @author �ukasz
 */
public class ChangeMultiplicityValueForJoin implements ExternalActivityBehaviour 
{
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ChangeMultiplicityValueForJoin.class);
	
	public void execute(ActivityExecution execution) throws Exception 
	{		
//		execution.setVariable("correction", 11);
//		boolean isAccepted = false;
//		for (Execution ex : execution.getProcessInstance().getExecutions())
//		{
//			if (ex.getState().equals("inactive-join"))
//				isAccepted = true;
//		}
//		if (!isAccepted)
//			execution.setVariable("count", 1);
//
//		log.info("Zamykamy wszystkie subprocesy utworzone w fork'u i ustawiamy warto�� 'correction' na 11");
//		
	}
	
	public void signal(ActivityExecution execution, String signalName, Map<String, ?> parameters) throws Exception {
		
	}
}
