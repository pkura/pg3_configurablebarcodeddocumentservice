package pl.compan.docusafe.parametrization.opzz.jbpm.cases;

import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.jbpm4.AbstractEventListener;

/**
 * Klasa ustawia zmienn� kt�ra okre�la ze przydzieli� na nowo user�w
 * @author �ukasz
 */
public class FromBeginListener extends AbstractEventListener 
{
	@Override
	public void notify(EventListenerExecution execution) throws Exception 
	{
		execution.setVariable("startFromBegin", 10);
	}

}
