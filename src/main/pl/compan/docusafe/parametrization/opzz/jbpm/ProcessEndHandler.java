package pl.compan.docusafe.parametrization.opzz.jbpm;

import java.util.List;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;
import org.jfree.util.Log;

import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ProcessEndHandler implements AssignmentHandler
{
	private static Logger log = LoggerFactory.getLogger(ProcessParamsAssignmentHandler.class);

 	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception
	{
		OfficeDocument document = Jbpm4Utils.getDocument(openExecution);
		
		log.info("ustawiam assignee na autora");
		
		assignable.setAssignee(document.getAuthor());
		
		//TODO: sprawdzi� to
		TaskSnapshot.updateByDocumentId(document.getId(),"anyType");
	}
}