package pl.compan.docusafe.parametrization.opzz.jbpm;

import java.util.*;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import static pl.compan.docusafe.parametrization.opzz.OpzzCn.*;

/**
 * Klasa przekazuje w�a�cicieli zapisanych na dokumencie i przypisuje im zadanie na list�
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class SetEachCasesVariables implements ExternalActivityBehaviour
{
	private static final long serialVersionUID = 1L;
	private final static Logger log = LoggerFactory.getLogger(SetEachCasesVariables.class);
	
	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		OfficeDocument document = Jbpm4Utils.getDocument(execution);
		FieldsManager fm = document.getFieldsManager();
		
		List<Long> owners = (List<Long>)fm.getKey(_DS_OPZZ_OWNER_CN);
		
		execution.setVariable("ids", owners);
		execution.setVariable("count", owners.size());
		execution.setVariable("count2", 2);
		execution.setVariable("twoAcc", 2);
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		
	}

}
