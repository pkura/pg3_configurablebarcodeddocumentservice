package pl.compan.docusafe.parametrization.opzz.jbpm;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.events.handlers.OpzzMailHandler;
import pl.compan.docusafe.parametrization.opzz.OpzzCn;
import pl.compan.docusafe.parametrization.opzz.OpzzFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Handler obs�uguj�cy ostatni krok w ankietach
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class OpinionEndHandler implements AssignmentHandler
{
	private final static Logger log = LoggerFactory.getLogger(OpinionEndHandler.class);
	
	@Override
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception
	{
		Document document = Jbpm4Utils.getDocument(openExecution);
		
		try
		{
			FieldsManager fm = document.getFieldsManager();
			
			Integer type = fm.getIntegerKey(OpzzCn._TYP_CN);
			
			String mailType = OpzzFactory.MAIL_TYPE.OPINIA_KONIEC.toString();
			
			switch(type)
			{
				case 1: mailType = OpzzFactory.MAIL_TYPE.OPINIA_ANKIETA_KONIEC.toString(); break;
				case 2: mailType = OpzzFactory.MAIL_TYPE.ZATWIERDZANIE_KONIEC.toString(); break;
			}
			
			assignable.addCandidateUser(document.getAuthor());
			
			OpzzMailHandler.createEvent(document.getId().toString(), document.getAuthor(), mailType);
		}catch (Exception e) {
			log.error("blad w wysylaniu maila do autora", e);
		}
		
	}

}
