package pl.compan.docusafe.parametrization.opzz.jbpm.cases;

import static pl.compan.docusafe.parametrization.opzz.OpzzCn._DS_OPZZ_ASSIGNED_CN;

import java.util.*;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessDefinition;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowFactory;
import pl.compan.docusafe.core.office.workflow.snapshot.JBPMSnapshotProvider;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.opzz.OpzzCn;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import static pl.compan.docusafe.parametrization.opzz.OpzzCn.*;

public class SetBothForkVariables implements ExternalActivityBehaviour
{
	private final static Logger log = LoggerFactory.getLogger(SetBothForkVariables.class);
	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		Document document = Jbpm4Utils.getDocument(execution);
		
		log.error("ustawiam dynamicznego forka");
		//lista uzytkownik�w do kt�ych jest przekazany dokument do wiadomo�ci
		FieldsManager fm = document.getFieldsManager();
		fm.initializeValues();
		List<Long> owners = (List<Long>)fm.getKey(_DS_OPZZ_OWNER_CN);
		List<Long> assigns = (List<Long>) fm.getKey(_DS_OPZZ_ASSIGNED_CN);
		
		final Set<Long> ids = new HashSet<Long>();
		
		//dodaje z listy w�a�cicieli
		if(owners != null && !owners.isEmpty())
		{
			for(Long owner : owners)
				ids.add(owner);
		}
		else
		{
			//dodaje autora do listy i zapisuje go jako wlasciciela na dokumencie
			DSUser author = DSUser.findByUsername(document.getAuthor());
			ids.add(author.getId());
			List<Long> oneOwner = new ArrayList<Long>();
			oneOwner.add(author.getId());
			Map<String, Object> values = new HashMap<String, Object>();
			values.put(OpzzCn._DS_OPZZ_OWNER_CN, oneOwner);
			
			document.getFieldsManager().getDocumentKind().setOnly(document.getId(), values);
		}
		
		//dodaj� przydzielonych do listy
		if(assigns != null && !assigns.isEmpty())
		{
			for(Long assign : assigns)
				ids.add(assign);
		}
		
		List<Long> docs = new ArrayList<Long>();
		docs.add(document.getId());
		finishTask(docs);
		execution.setVariable("ids", ids);
		execution.setVariable("counts", ids.size());
		execution.setVariable("count2", 2);
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		
	}
	
	public static boolean finishTask(List<Long> ids) throws EdmException
	{
		for (Long id : ids)
		{
//			WorkflowFactory wf = WorkflowFactory.getInstance();
//            if(wf instanceof InternalWorkflowFactory){
//                String activityId = wf.findManualTask(id);
//                wf.manualFinish(activityId, false);
//            } else if(wf instanceof Jbpm4WorkflowFactory) {
//            	try {
//            		Iterable<String> tasks = Jbpm4ProcessLocator.taskIds(id);
//            		OfficeDocument doc = OfficeDocument.find(id);
//        			ProcessDefinition pdef = doc.getDocumentKind().getDockindInfo()
//        					.getProcessesDeclarations().getProcess("opzz_cases_3");
//        			List<? extends ProcessInstance> pi = new ArrayList<ProcessInstance>(pdef.getLocator().lookupForDocument(doc));
//                    //log.info("begin context");
//                    pl.compan.docusafe.core.dockinds.process.ProcessAction pa;
//                    
//                    for(String task : tasks)
//                    {
//                    	try
//                    	{
//                    		pdef.getLogic().process(
//                    				pi.get(0),
//                    				pa = ProcessActionContext
//                                    	.action("to assign-end")
//                                    	.document(doc)
//                                    	.stored(false)
//                                    	.activityId(task));
//                    	}catch (Exception e) {
//                    		log.error("assign blad",e);
//						}
//                    }
//            	}
//            	catch (NoSuchElementException e) {
//            		log.warn("Dokument {} jest juz zamkniety",id);
//            	}
//            } else {
//                throw new IllegalStateException("nieznany WorkflowFactory");
//            }
		}
		return true;
	}

}
