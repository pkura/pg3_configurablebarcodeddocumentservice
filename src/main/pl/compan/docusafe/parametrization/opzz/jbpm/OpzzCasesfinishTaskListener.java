package pl.compan.docusafe.parametrization.opzz.jbpm;

import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.parametrization.opzz.OpzzCn;
import java.util.*;

/** Kontroluje element zakoncz prace z dokumentem na sprawie */
public class OpzzCasesfinishTaskListener extends AbstractEventListener
{

	@SuppressWarnings("unchecked")
	@Override
	public void notify(EventListenerExecution openExecution) throws Exception
	{
		Document document = Jbpm4Utils.getDocument(openExecution);
		List<Long> assigns = (LinkedList<Long>) document.getFieldsManager().getKey(OpzzCn._DS_OPZZ_ASSIGNED_CN);
		//Usuwa go z listy przydzielonych
		if(assigns.contains(DSApi.context().getDSUser().getId()))
			assigns.remove(DSApi.context().getDSUser().getId());
		
		Map<String, Object> values = new HashMap<String, Object>();
		values.put(OpzzCn._DS_OPZZ_ASSIGNED_CN, assigns);
		document.getDocumentKind().setOnly(document.getId(), values);
	}

}
