package pl.compan.docusafe.parametrization.opzz.jbpm;

import static pl.compan.docusafe.parametrization.opzz.OpzzCn._DS_OPZZ_CASES_CN;
import static pl.compan.docusafe.parametrization.opzz.OpzzCn._DS_OPZZ_DOC_CN;

import java.util.Date;
import java.util.List;

import org.jbpm.api.listener.EventListenerExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.field.EnumerationField;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.opzz.OpzzDataMart;

public class AcceptancesListener extends AbstractEventListener
{

	private static final Logger log = LoggerFactory.getLogger(AcceptancesListener.class);
	private String acception;
	private String status;
	private String sourceGuid;
	
	@Override
	public void notify(EventListenerExecution openExecution) throws Exception
	{
		Long docId = Jbpm4Utils.getDocumentId(openExecution);
		OfficeDocument doc = OfficeDocument.find(docId);

		// Zapis akceptacji
		DocumentAcceptance.createAndSave(doc, DSApi.context().getPrincipalName(), acception, null);

		// Dokonanie akpcetacji - informacja dla użytkownika
		AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
		ahe.setSourceUser(DSApi.context().getPrincipalName());
		ahe.setProcessName(openExecution.getProcessDefinitionId());
		ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_ACCEPTED);
		String divisions = "";
		
		for (DSDivision a : DSUser.findByUsername(DSApi.context().getPrincipalName()).getOriginalDivisionsWithoutGroup())
		{
			if (!divisions.equals(""))
				divisions = divisions + " / " + a.getName();
			else
				divisions = a.getName();
		}
//		ahe.setSourceGuid(divisions);
		
//		String status = ((EnumerationField)doc.getFieldsManager().getField("STATUS")).getEnumItemByCn(acception).getTitle();
		
		if(status != null)
			ahe.setStatus(status);
		
		ahe.setCtime(new Date());

		ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
		doc.addAssignmentHistoryEntry(ahe);
                
		if (AvailabilityManager.isAvailable("addToWatch"))
			DSApi.context().watch(URN.create(Document.find(docId)));
		
		doc.getDocumentKind().logic().onAcceptancesListener(doc, acception);
	}

	public String getAcception()
	{
		return acception;
	}

	public void setAcception(String acception)
	{
		this.acception = acception;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getSourceGuid()
	{
		return sourceGuid;
	}

	public void setSourceGuid(String sourceGuid)
	{
		this.sourceGuid = sourceGuid;
	}
	
	
}
