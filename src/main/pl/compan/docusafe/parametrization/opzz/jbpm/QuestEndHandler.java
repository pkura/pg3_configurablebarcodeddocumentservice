package pl.compan.docusafe.parametrization.opzz.jbpm;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.events.handlers.OpzzMailHandler;
import pl.compan.docusafe.parametrization.opzz.OpzzFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class QuestEndHandler implements AssignmentHandler
{
	private final static Logger log = LoggerFactory.getLogger(QuestEndHandler.class);
	
	@Override
	public void assign(Assignable arg0, OpenExecution arg1) throws Exception
	{
		Document document = Jbpm4Utils.getDocument(arg1);
		
		try
		{
			String mailType = OpzzFactory.MAIL_TYPE.ZADANIE_KONIEC.toString();
			
			OpzzMailHandler.createEvent(document.getId().toString(), document.getAuthor(), mailType);
		}catch (Exception e) {
			log.error("blad w wysylaniu maila do autora", e);
		}
		
	}

}