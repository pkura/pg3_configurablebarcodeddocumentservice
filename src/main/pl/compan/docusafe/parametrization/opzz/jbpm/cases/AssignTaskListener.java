package pl.compan.docusafe.parametrization.opzz.jbpm.cases;

import java.util.*;

import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.parametrization.opzz.OpzzCn;

/**
 * Listener wykonywany podczas wcisniecia przycisku "zakoncz prace z dokumentem" u przydzielonego
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class AssignTaskListener extends AbstractEventListener 
{

	@Override
	public void notify(EventListenerExecution exception) throws Exception
	{
		Document document = Jbpm4Utils.getDocument(exception);
		
		List<Long> assigns = (List<Long>)document.getFieldsManager().getKey(OpzzCn._DS_OPZZ_ASSIGNED_CN);
		boolean removedEnabled = AvailabilityManager.isAvailable("cases.assigns.remove.enable");
		
		if(!removedEnabled)
			return;
		
		if(assigns != null && !assigns.isEmpty())
		{
			//osoba kt�ra klikne�a przycisk "Zako�cz jest usuwana z listy
			Long userId = DSApi.context().getDSUser().getId();
			if(assigns.contains(userId))
			{
				assigns.remove(userId);
			}
				
			Map<String, Object> values = new HashMap<String, Object>();
			values.put(OpzzCn._DS_OPZZ_ASSIGNED_CN, assigns);
			document.getDocumentKind().setOnly(document.getId(), values);
		}
	}

}
