package pl.compan.docusafe.parametrization.opzz.jbpm;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.DSApi;

public class OpzzDocAssignmentHandler implements AssignmentHandler
{

	@Override
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception
	{
		assignable.addCandidateUser(DSApi.context().getPrincipalName());
	}

}
