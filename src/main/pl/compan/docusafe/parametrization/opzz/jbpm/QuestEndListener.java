package pl.compan.docusafe.parametrization.opzz.jbpm;

import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.events.handlers.OpzzMailHandler;
import pl.compan.docusafe.parametrization.opzz.OpzzCn;
import pl.compan.docusafe.parametrization.opzz.OpzzDataMart;
import pl.compan.docusafe.parametrization.opzz.OpzzFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Listener po zakonczeniu zadania
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class QuestEndListener extends AbstractEventListener
{
	private final static Logger log = LoggerFactory.getLogger(QuestEndListener.class);
	@Override
	public void notify(EventListenerExecution arg0) throws Exception
	{
		Document document = Jbpm4Utils.getDocument(arg0);
		FieldsManager fm = document.getFieldsManager();
		try
		{
			String mailType = OpzzFactory.MAIL_TYPE.ZADANIE_KONIEC.toString();
			
			OpzzDataMart.addFinishQuestToCase(document, fm.getLongKey(OpzzCn._DS_OPZZ_CASES_CN));
			OpzzMailHandler.createEvent(document.getId().toString(), document.getAuthor(), mailType);
		}catch (Exception e) {
			log.error("blad w wysylaniu maila do autora", e);
		}
		
	}

}