package pl.compan.docusafe.parametrization.pgSzkolenie;

import com.google.common.base.Optional;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.FolderResolver;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.util.DateUtils;

import java.util.Date;
import java.util.Map;

import static pl.compan.docusafe.core.dockinds.dwr.DwrUtils.dwr;

/**
 * Created by kk on 2015-09-23.
 */
public class AbsenceRequestDocumentLogic extends AbstractDocumentLogic {

    @Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        int dateDifference = countDateDifference(values);

        values.put(dwr("dni_robocze"), new FieldData(Field.Type.INTEGER, dateDifference));

        Optional<String> msgContent = validateDates(values);

        if (msgContent.isPresent()) {
            values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
            return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgContent.get(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
        }

        return super.validateDwr(values, fm);
    }

    private Optional<String> validateDates(Map<String, FieldData> values) {
        Optional<Date> fromDate = getDateFromFieldData(values, "data_od");
        Optional<Date> toDate = getDateFromFieldData(values, "data_do");

        if (fromDate.isPresent() && toDate.isPresent()) {
            boolean correctDate = fromDate.get().before(toDate.get());

            if (!correctDate) {
                Map.Entry<String, FieldData> senderField = DwrUtils.getSenderField(values);

                if (senderField != null) {
//                    String dwrFieldCn = senderField.getKey();
                    senderField.getValue().setData(null);
                }

                return Optional.of("Nieprawidłowa data");
            }

        }

        return Optional.absent();
    }

    Optional<Date> getDateFromFieldData(Map<String, FieldData> values, String fieldCn) {
        FieldData fieldData = values.get(dwr(fieldCn));

        if (fieldData != null) {
            Date date = fieldData.getDateData();
            if (date != null) {
                return Optional.of(date);
            }
        }

        return Optional.absent();
    }

    /**
     * count absolute days differences beetwen dates
     * @param values
     * @return
     */
    private int countDateDifference(Map<String, FieldData> values) {
        Optional<Date> fromDate = getDateFromFieldData(values, "data_od");
        Optional<Date> toDate = getDateFromFieldData(values, "data_do");

        if (fromDate.isPresent() && toDate.isPresent()) {
            return DateUtils.difference(fromDate.get(), toDate.get());
        }

        return 0;
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        FolderResolver fr = document.getDocumentKind().getDockindInfo().getFolderResolver();
        if (fr != null) {
            as.useFolderResolver(document);
        }
        as.useAvailableProviders(document);
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {

    }
}
