package pl.compan.docusafe.parametrization.ul;

public class ActivityKindAcceptance {

	private String activity;
	private String person;
	private String amount;
	private String amountString;
	
	
	public ActivityKindAcceptance(String activity, String person,
			String amount, String amountString) {
		super();
		this.activity = activity;
		this.person = person;
		this.amount = amount;
		this.amountString = amountString;
	}
	
	public String getActivity() {
		return activity;
	}
	
	public void setActivity(String activity) {
		this.activity = activity;
	}
	
	public String getPerson() {
		return person;
	}
	
	public void setPerson(String person) {
		this.person = person;
	}
	
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	public String getAmountString() {
		return amountString;
	}

	public void setAmountString(String amountString) {
		this.amountString = amountString;
	}

	public String toString(){
		return "działalność: " + getActivity() + " dysponent: " + getPerson() + " amount: " + amount + " " + amountString;
	}

}
