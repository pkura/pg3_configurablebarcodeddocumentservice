package pl.compan.docusafe.parametrization.ul;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jbpm.api.task.Participation;
import org.jbpm.api.task.Task;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;

public class NormalLogic extends AbstractDocumentLogic {

	private static final long serialVersionUID = 1L;
	
	private static final String PRZYCHODZACE = "normal";
	private static final String WYCHODZACE = "normal_out";
	private static final String WEWNETRZNE = "normal_int";
	private static final String KANCELARIA_GUID = Docusafe.getAdditionProperty("kancelaria.guid");
	
	private static NormalLogic instance;
	protected static Logger log = LoggerFactory.getLogger(NormalLogic.class);

	public static NormalLogic getInstance() {
		if (instance == null)
			instance = new NormalLogic();
		return instance;
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event) {
		log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
		try {
			Map<String, Object> map = Maps.newHashMap();
			if (WYCHODZACE.equals(document.getDocumentKind().getCn())) {
				map.put(ASSIGN_USER_PARAM, null);
				map.put(ASSIGN_DIVISION_GUID_PARAM, KANCELARIA_GUID);
			} else {
				map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
				map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			}
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP,
				"Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP,
				"Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP,
				"Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));
		List<String> users = new ArrayList<String>();
		users.add(document.getAuthor());
		List<String> divs = new ArrayList<String>();
		
    	List<String>  list = (List<String>) Jbpm4ProcessLocator.taskIds(document.getId());
    	for (String activityId : list) {
    		 Task task = Jbpm4Provider.getInstance().getTaskService().getTask(activityId);

             if(task.getAssignee() != null){
                 users.add(task.getAssignee());
             } else {
                 List<Participation> ps = Jbpm4Provider.getInstance().getTaskService().getTaskParticipations(activityId);
                 if(ps.size() == 1) {
                     Participation p = ps.get(0);
                     if(p.getUserId() != null){
                         users.add(p.getUserId());
                     } else {
                    	 divs.add(p.getGroupId());
                     }
                 } else if (ps.size() > 1) {
                     for(Participation p : ps){
                    	 if(p.getUserId() != null){
                    		 users.add(p.getUserId());
                         } else {
                        	 divs.add(p.getGroupId());
                         }
                     }
                 }
             }
		}
    	for (String un : users)
    	{
			DSUser user = DSUser.findByUsername(un);
			divs.addAll(java.util.Arrays.asList(user.getDivisionsGuid()));
		}
    	
    	
		for (String division : divs) 
		{
			perms.add(new PermissionBean(ObjectPermission.READ, division, ObjectPermission.GROUP));
	        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, division, ObjectPermission.GROUP));
		}
		for (String user : users) {
       	 	perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER));
		}

		for (PermissionBean perm : perms) {
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP)) {
				String groupName = perm.getGroupName();
				if (groupName == null) {
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}


	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
		as.useFolderResolver(document);
		as.useAvailableProviders(document);
		if(document instanceof OutOfficeDocument)
		{
			FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
			((OutOfficeDocument)document).setZpo((Boolean) fm.getKey("ZPD"));
		}
		log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
		Map<String, Object> toReload = Maps.newHashMap();
		for (Field f : fm.getFields())
			if (f.getDefaultValue() != null)
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
		
		DSUser user = DSApi.context().getDSUser();
		toReload.put("NADAWCA", user.getId());
		
		if (DSApi.context().getDSUser().getDivisionsWithoutGroup().length>0) {
			String sender = "u:"+DSApi.context().getDSUser().getName();
			sender+=(";d:"+DSApi.context().getDSUser().getDivisionsWithoutGroup()[0].getGuid());
			toReload.put("SENDER_HERE", sender);	
		}
		
		fm.reloadValues(toReload);
	}

	@Override
	public TaskListParams getTaskListParams(DocumentKind kind, long documentId, TaskSnapshot task) throws EdmException {
		if (Strings.isNullOrEmpty(task.getRealDescription())) {
			task.setDescription("Brak");
		}
		FieldsManager fm = kind.getFieldsManager(documentId);
		TaskListParams params = getTaskListParams(kind, documentId);
		String status = null;
		try {
			status = (String) fm.getValue("STATUS");
		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}
		params.setStatus(Strings.isNullOrEmpty(status)?"Brak":status);
		
		String guidLabel = "-";
		String guid = OfficeDocument.find(documentId).getDivisionGuid();
		if (!Strings.isNullOrEmpty(guid)) {
			try {
				guidLabel = DSDivision.find(guid).getName();
			} catch (EdmException e) {
				log.error(e.getMessage(),e);
			}
		}
		
		params.setAttribute(guidLabel, 4);
		
		return params;
	}

}
