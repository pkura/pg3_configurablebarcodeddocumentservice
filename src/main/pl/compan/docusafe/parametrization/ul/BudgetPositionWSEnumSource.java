package pl.compan.docusafe.parametrization.ul;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.enums.EnumDataSourceLogic;
import pl.compan.docusafe.core.dockinds.field.enums.EnumSourceException;
import pl.compan.docusafe.core.dockinds.field.enums.ExternalSourceEnumItem;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.util.axis.AxisClientUtils;
import pl.compan.docusafe.ws.ul.DictionaryServiceStub;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * Created by kk on 23.02.14.
 */
public class BudgetPositionWSEnumSource implements EnumDataSourceLogic {

    private static final Logger log = LoggerFactory.getLogger(BudgetPositionWSEnumSource.class);


    @Override
    public List<EnumItem> getEnumItemsFromSource(String refFieldId) throws EnumSourceException {
        final List<EnumItem> enumItems = Lists.newLinkedList();
        if (Strings.isNullOrEmpty(refFieldId)) {
            return enumItems;
        }

        final Long parsedRefFieldId;
        try {
            parsedRefFieldId = Long.parseLong(refFieldId);
        } catch (NumberFormatException e) {
            log.error(e.getMessage(), e);
            return enumItems;
        }


        Callable<List<EnumItem>> dateSourceDownload = new Callable<List<EnumItem>>() {

            @Override
            public List<EnumItem> call() throws Exception {
                long start = System.currentTimeMillis();
                AxisClientConfigurator axisClientConfigurator = new AxisClientConfigurator();
                DictionaryServiceStub stub = new DictionaryServiceStub(axisClientConfigurator.getAxisConfigurationContext());
                axisClientConfigurator.setUpHttpParameters(stub, "/services/DictionaryService");
                axisClientConfigurator.setHttpClient(AxisClientUtils.createHttpClient());
                DictionaryServiceStub.GetUnitBudgetKoKindByParentID params = new DictionaryServiceStub.GetUnitBudgetKoKindByParentID();
                params.setId(parsedRefFieldId);
                params.setPage(1);
                params.setPageSize(1000);
                DictionaryServiceStub.UnitBudgetKoKind[] wsItems = stub.getUnitBudgetKoKindByParentID(params).get_return();
                if (wsItems != null) {
                    for (DictionaryServiceStub.UnitBudgetKoKind wsItem : wsItems) {
                        enumItems.add(new ExternalSourceEnumItem((int)wsItem.getId() , String.valueOf(wsItem.getId()), makeEnumTitle(wsItem)));
                    }
                }
                log.error("BudgetPositionWSEnumSource for {} total {}" + parsedRefFieldId, (System.currentTimeMillis() - start));
                return enumItems;
            }
        };

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<List<EnumItem>> takenResults = executorService.submit(dateSourceDownload);

        try {
            takenResults.get(Ints.tryParse(Docusafe.getAdditionPropertyOrDefault("ws-external-get-timeout", "5")), TimeUnit.SECONDS);
        } catch (Exception e) {
            throw new EnumSourceException(e);
        }

        return enumItems;
/*
        try {
            long start = System.currentTimeMillis();
            AxisClientConfigurator axisClientConfigurator = new AxisClientConfigurator();
            DictionaryServiceStub stub = new DictionaryServiceStub(axisClientConfigurator.getAxisConfigurationContext());
            axisClientConfigurator.setUpHttpParameters(stub, "/services/DictionaryService");
            axisClientConfigurator.setHttpClient(AxisClientUtils.createHttpClient(30, 30));
            DictionaryServiceStub.GetUnitBudgetKoKindByParentID params = new DictionaryServiceStub.GetUnitBudgetKoKindByParentID();
            params.setId(parsedRefFieldId);
            params.setPage(1);
            params.setPageSize(1000);
            DictionaryServiceStub.UnitBudgetKoKind[] wsItems = stub.getUnitBudgetKoKindByParentID(params).get_return();
            if (wsItems != null) {
                for (DictionaryServiceStub.UnitBudgetKoKind wsItem : wsItems) {
                    enumItems.add(ExternalSourceEnumItem.makeEnumItem("" + (long)wsItem.getId() + ExternalSourceEnumItem.SPLIT_CHAR + (long)wsItem.getId() + ExternalSourceEnumItem.SPLIT_CHAR + makeEnumTitle(wsItem)));
                }
            }
            log.error("ProjectBudgetPositionWSEnumSource for {} total {}", parsedRefFieldId, (System.currentTimeMillis()-start));
        } catch (Exception e) {
            throw new EnumSourceException(e.getMessage(), e);
        }
        return enumItems;*/
    }

    private String makeEnumTitle(DictionaryServiceStub.UnitBudgetKoKind wsItem) {
        StringBuilder sb = new StringBuilder();
        return sb.append(StringUtils.left(wsItem.getNazwa(), 100))
                .append(" (plan-wykonanie: ")
                .append(BigDecimal.valueOf(wsItem.getP_koszt()-wsItem.getR_koszt()).setScale(2, RoundingMode.HALF_UP))
                .append("; rezerwacje: ")
                .append(BigDecimal.valueOf(wsItem.getRez_koszt()).setScale(2, RoundingMode.HALF_UP))
                .append(")")
                .toString();
    }

    @Override
    public List<EnumItem> getEnumItemsFromSource() throws EnumSourceException {
        return Lists.newArrayList();
    }
}
