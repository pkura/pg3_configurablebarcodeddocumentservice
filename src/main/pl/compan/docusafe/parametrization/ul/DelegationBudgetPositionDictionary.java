package pl.compan.docusafe.parametrization.ul;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.imports.simple.repository.CacheHandler;
import pl.compan.docusafe.core.imports.simple.repository.Dictionary;
import pl.compan.docusafe.core.imports.simple.repository.DictionaryMap;

import java.util.List;
import java.util.Map;

import static pl.compan.docusafe.parametrization.ul.CostInvoiceLogic.*;

/**
 * User: kk
 * Date: 06.12.13
 * Time: 17:56
 */
public class DelegationBudgetPositionDictionary extends BudgetPositionDictionary {

    @Override
    public void filterAfterGetValues(Map<String, Object> dictionaryFilteredFieldsValues, String dicitonaryEnrtyId, Map<String, Object> fieldsValues, Map<String, Object> documentValues) {
        super.filterAfterGetValues(dictionaryFilteredFieldsValues, dicitonaryEnrtyId, fieldsValues, documentValues);
        Map<String, Object> newResults = Maps.newHashMap();
        String dicNameWithSuffix = getName() + "_";


        clearAllDictionaresFields(DOC_KIND_CN, dicNameWithSuffix, newResults);

        DwrUtils.setRefValueEnumOptions(dictionaryFilteredFieldsValues, newResults, "I_GROUP", dicNameWithSuffix, RODZAJ_DZIALALNOSCI_FIELD_NAME, RODZAJ_DZIALALNOSCI_TABLE_NAME, null, EMPTY_ENUM_VALUE, "_1");
        DwrUtils.setRefValueEnumOptions(dictionaryFilteredFieldsValues, newResults, "I_GROUP", dicNameWithSuffix, TYPY_DZIALALNOSCI_FIELD_NAME, TYPY_DZIALALNOSCI_TABLE_NAME, null, EMPTY_ENUM_VALUE, "_1");

        String controlDictCnFromResults = BP_FIELD_WITH_LINK + "I_KIND";

        if (newResults.get(controlDictCnFromResults) != null && newResults.get(controlDictCnFromResults) instanceof EnumValues) {
            String selectedId = ((EnumValues) newResults.get(controlDictCnFromResults)).getSelectedId();

            if (StringUtils.isNotEmpty(selectedId)) {
                Integer rodzajId = Integer.valueOf(selectedId);

                String controlValue = null;
                try {
                    EnumItem rodzajEnum = DataBaseEnumField.getEnumItemForTable(RODZAJ_DZIALALNOSCI_TABLE_NAME, rodzajId);
                    controlValue = rodzajEnum.getCn();
                } catch (EdmException e) {
                    log.error("CAUGHT: " + e.getMessage(), e);
                }

                if (controlValue != null) {
                    List<DictionaryMap> dictToShow = CacheHandler.getDictionariesMappings(controlValue, DOC_KIND_CN);

                    for (DictionaryMap dictMap : dictToShow) {
                        EnumValues defaultEnum = setDefaultEmptyEnum(dictMap);

                        Dictionary dict = dictMap.getDictionary();
                        if (dict.getDeleted() == false) {
                            if ("C_USER".equals(dict.getCn())) {
                                if (!isBudgetOwnerAccept(dictionaryFilteredFieldsValues)) {
                                    continue;
                                }
                            }

                            if (dict.getDictSource() == Dictionary.SourceType.SERVICE.value || dict.getDictSource() == Dictionary.SourceType.MANUAL.value) {
                                setServiceDict(dictionaryFilteredFieldsValues, newResults, dicNameWithSuffix, dictMap, defaultEnum, dict);
                            }
                        }
                    }
                    dictionaryFilteredFieldsValues.putAll(newResults);
                }
            }
        }
    }

    protected void setServiceDict(Map<String, Object> values, Map<String, Object> results, String dicNameWithSuffix,
                                  DictionaryMap dictMap, EnumValues defaultEnum, Dictionary dict) {
        if (dict.getRefDict() == null) {
            try {
                DataBaseEnumField base = DataBaseEnumField.tableToField.get(dict.getCn().equals("C_TYPE") ?  AbstractDelegationLogic.PRODUCT_DELEGATION_TABLE_NAME : dict.getTablename()).iterator().next();
                String fieldSelectedId = ((EnumValues)values.get(dicNameWithSuffix + StringUtils.remove(base.getCn(), "_1"))).getSelectedId();
                results.put(dicNameWithSuffix + dict.getCn(), base.getEnumValuesForForcedPush(fieldSelectedId));
            } catch (Exception e) {
                log.error("CATCH " + e.getMessage(), e);
            }
        } else {
            if (dictMap.isShowAllReferenced()) {
                DwrUtils.setRefValueEnumOptions(values, results, dict.getRefDict().getCn(), dicNameWithSuffix, dict.getCn(), dict.getTablename(), DataBaseEnumField.REF_ID_FOR_ALL_FIELD, defaultEnum, "_1");
            } else {
                DwrUtils.setRefValueEnumOptions(values, results, dict.getRefDict().getCn(), dicNameWithSuffix, dict.getCn(), dict.getTablename(), null, defaultEnum, "_1");
            }
        }
    }
}
