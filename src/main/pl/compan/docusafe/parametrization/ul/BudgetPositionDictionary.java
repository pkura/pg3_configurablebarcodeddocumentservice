package pl.compan.docusafe.parametrization.ul;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.imports.simple.repository.CacheHandler;
import pl.compan.docusafe.core.imports.simple.repository.Dictionary;
import pl.compan.docusafe.core.imports.simple.repository.DictionaryMap;
import pl.compan.docusafe.core.imports.simple.repository.SimpleRepositoryAttribute;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static pl.compan.docusafe.core.dockinds.field.DataBaseEnumField.REPO_REF_FIELD_PREFIX;
import static pl.compan.docusafe.parametrization.ul.CostInvoiceLogic.*;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 04.07.13
 * Time: 12:53
 * To change this template use File | Settings | File Templates.
 */
public class BudgetPositionDictionary extends DwrDictionaryBase {

    protected static final Logger log = LoggerFactory.getLogger(BudgetPositionDictionary.class);

    protected static final EnumValues EMPTY_ENUM_VALUE = new EnumValues(new HashMap<String,String>(), new ArrayList<String>());

    @Override
    public void filterBeforeUpdateValues(Map<String, FieldData> dictionaryFilteredFieldData) {
        // clearDictionaresFieldsDataIfNull(DOCKIND_CN, dictionaryFilteredFieldData);
    }

    @Override
    public void filterAfterGetValues(Map<String, Object> dictionaryFilteredFieldsValues, String dicitonaryEnrtyId, Map<String, Object> fieldsValues, Map<String, Object> documentValues) {
        super.filterAfterGetValues(dictionaryFilteredFieldsValues, dicitonaryEnrtyId, fieldsValues, documentValues);
        Map<String,Object> newResults = Maps.newHashMap();
        String dicNameWithSuffix = BP_FIELD + "_";
        Integer docTypeId = getDocTypeId(documentValues);

        if (docTypeId != null) {
            EnumItem docTypeEnum = null;
            try {
                docTypeEnum = DataBaseEnumField.getEnumItemForTable(DOCUMENT_TYPES_TABLE_NAME, docTypeId);
            } catch (EdmException e) {
                log.error("CAUGHT: "+e.getMessage(), e);
            }

            clearAllDictionaresFields(DOC_KIND_CN, dicNameWithSuffix, newResults);

            if (docTypeEnum != null) {
                Integer docTypeObjectId = docTypeEnum.getCentrum();
                DwrUtils.setRefValueEnumOptions(dictionaryFilteredFieldsValues, newResults, "C_KIND", BP_FIELD_WITH_LINK, "I_GROUP", GRUPY_DZIALALNOSCI_TABLE_NAME, null, EMPTY_ENUM_VALUE, "_1");
                DwrUtils.setRefValueEnumOptions(dictionaryFilteredFieldsValues, newResults, "I_GROUP", BP_FIELD_WITH_LINK, RODZAJ_DZIALALNOSCI_FIELD_NAME, RODZAJ_DZIALALNOSCI_TABLE_NAME, null, EMPTY_ENUM_VALUE, "_1");
                DwrUtils.setRefValueEnumOptions(dictionaryFilteredFieldsValues, newResults, "I_GROUP", BP_FIELD_WITH_LINK, TYPY_DZIALALNOSCI_FIELD_NAME, TYPY_DZIALALNOSCI_TABLE_NAME, null, EMPTY_ENUM_VALUE, "_1");

                String controlDictCnFromResults = BP_FIELD_WITH_LINK + "I_KIND";

                if (newResults.get(controlDictCnFromResults) != null && newResults.get(controlDictCnFromResults) instanceof EnumValues) {
                    String selectedId = ((EnumValues) newResults.get(controlDictCnFromResults)).getSelectedId();

                    if (StringUtils.isNotEmpty(selectedId)) {
                        Set<String> repositoryDictsIds = getRepositoryDictsIds(docTypeObjectId, (int) INVOICE_POSITION_CONTEXT_ID);
                        Integer rodzajId = Integer.valueOf(selectedId);

                        String controlValue = null;
                        try {
                            EnumItem rodzajEnum = DataBaseEnumField.getEnumItemForTable(RODZAJ_DZIALALNOSCI_TABLE_NAME, rodzajId);
                            controlValue = rodzajEnum.getCn();
                        } catch (EdmException e) {
                            log.error("CAUGHT: " + e.getMessage(), e);
                        }

                        if (controlValue != null) {
                            List<DictionaryMap> dictToShow = CacheHandler.getDictionariesMappings(controlValue, DOC_KIND_CN);

                            for (DictionaryMap dictMap : dictToShow) {
                                EnumValues defaultEnum = setDefaultEmptyEnum(dictMap);

                                Dictionary dict = dictMap.getDictionary();
                                if (dict.getDeleted() == false) {
                                    if ("C_TYPE".equals(dict.getCn())) {
                                        if (isFixedAsset(dictionaryFilteredFieldsValues)) {
                                            continue;
                                        }
                                    }

                                    if ("C_USER".equals(dict.getCn())) {
                                        if (!isBudgetOwnerAccept(dictionaryFilteredFieldsValues)) {
                                            continue;
                                        }
                                    }

                                    if (dict.getDictSource() == Dictionary.SourceType.REPOSITORY.value) {
                                        setRepositoryDict(dictionaryFilteredFieldsValues, newResults, dicNameWithSuffix, docTypeObjectId, (int) INVOICE_POSITION_CONTEXT_ID, repositoryDictsIds,
                                                dictMap, defaultEnum, dict, null);
                                    }
                                    if (dict.getDictSource() == Dictionary.SourceType.SERVICE.value || dict.getDictSource() == Dictionary.SourceType.MANUAL.value) {
                                        setServiceDict(dictionaryFilteredFieldsValues, newResults, dicNameWithSuffix, dictMap, defaultEnum, dict);
                                    }

                                    if (dict.getDictSource() == Dictionary.SourceType.SERVICE_ADHOC.value) {
                                        setServiceAdhocDict(dictionaryFilteredFieldsValues, newResults, BP_FIELD, dictMap, defaultEnum, dict);
                                    }
                                }
                            }
                            dictionaryFilteredFieldsValues.putAll(newResults);
                        }
                    }
                }
            }
        }
    }


    private Integer getDocTypeId(Map<String, Object> fieldsValues) {
        if (fieldsValues != null) {
            Object docType = fieldsValues.get("TYP_FAKTURY");
            if (docType != null) {
                try {
                    return Integer.valueOf(docType.toString());
                } catch (NumberFormatException e) {
                }
            }
        }
        return null;
    }

    protected EnumValues setDefaultEmptyEnum(DictionaryMap dictMap) {
        EnumValues defaultEnum = dictMap.isMustShow() ? EMPTY_NOT_HIDDEN_ENUM_VALUE : EMPTY_ENUM_VALUE;
        return defaultEnum;
    }

    protected void clearAllDictionaresFields(String dockindCn, String dicNameWithSuffix, Map<String, Object> results) {
        for (Dictionary dictionaryToClear : CacheHandler.getAvailableDicts(dockindCn)) {
            results.put(dicNameWithSuffix+dictionaryToClear.getCn(), EMPTY_ENUM_VALUE);
        }
    }

    private void clearDictionaresFieldsDataIfNull(String dockindCn, Map<String, FieldData> dictionaryFilteredFieldData) {
        Set<String> cnsToClear = Sets.newLinkedHashSet();
        for (Dictionary dictionaryToClear : CacheHandler.getAvailableDicts(dockindCn)) {
            if (dictionaryFilteredFieldData.get(dictionaryToClear.getCn()).getData() == null) {
                cnsToClear.add(dictionaryToClear.getCn());
            }
        }

        // Omini�cie problemu, na etapie akceptacji dzia��w finansowych faktury zagranicznej
        if (CacheHandler.getAvailableDicts(dockindCn).size() > cnsToClear.size()) {
            for (String cn : cnsToClear) {
                dictionaryFilteredFieldData.get(cn).setData("");
            }
        }
    }


    protected Set<String> getRepositoryDictsIds(Integer docTypeObjectId, Integer contextId) {
        List<SimpleRepositoryAttribute> repositoryDicts = CacheHandler.getRepositoryDicts(docTypeObjectId, contextId);
        Set<String> repositoryDictsIds = Sets.newHashSet();
        for (SimpleRepositoryAttribute repositoryDict : repositoryDicts) {
            repositoryDictsIds.add(repositoryDict.getRepKlasaId().toString());
        }
        return repositoryDictsIds;
    }

    protected void setRepositoryDict(Map<String, Object> values, Map<String, Object> results, String dicNameWithSuffix,
                                     Integer docTypeObjectId, Integer contextId, Set<String> repositoryDictsIds, DictionaryMap dictMap, EnumValues defaultEnum, Dictionary dict, Logger log) {
        if (dictMap != null && dictMap.isShowAllReferenced()) {
            DwrUtils.setRefValueEnumOptions(values, results, DataBaseEnumField.REPO_REF_FIELD_PREFIX + dict.getCn(), dicNameWithSuffix, dict.getCn(),
                    dict.getTablename(), DataBaseEnumField.REF_ID_FOR_ALL_FIELD, defaultEnum, "_1");
        } else {
            if (repositoryDictsIds.contains(dict.getCode())) {
                try {
                    Long dictObjectId = Long.valueOf(dict.getCode());
                    SimpleRepositoryAttribute attr = CacheHandler.getRepostioryDict(dictObjectId, docTypeObjectId, contextId);
                    if (attr != null) {
                        DwrUtils.setRefValueEnumOptions(values, results, REPO_REF_FIELD_PREFIX + dict.getCn(), dicNameWithSuffix, dict.getCn(),
                                dict.getTablename(), attr.getRepAtrybutId().toString(), defaultEnum, "_1");
                    }
                } catch (NumberFormatException e) {
                    log.error("CAUGHT: "+e.getMessage(),e);
                }
            }
        }
    }

    private void setServiceAdhocDict(Map<String, ?> values, Map<String, Object> results, String dictionaryCn, DictionaryMap dictMap, EnumValues defaultEnum, Dictionary dict) {
        reloadRefEnumItems(values, results, dictionaryCn, dict.getCn(), dict.getRefDict().getCn(), dict.getDockindCn(), defaultEnum);
    }

    protected void setServiceDict(Map<String, Object> values, Map<String, Object> results, String dicNameWithSuffix,
                                DictionaryMap dictMap, EnumValues defaultEnum, Dictionary dict) {
        if (dict.getRefDict() == null) {
            DataBaseEnumField base = DataBaseEnumField.tableToField.get(dict.getTablename()).iterator().next();
            String fieldSelectedId = ((EnumValues) values.get(dicNameWithSuffix + StringUtils.remove(base.getCn(), "_1"))).getSelectedId();
            results.put(dicNameWithSuffix + dict.getCn(), base.getEnumValuesForForcedPush(fieldSelectedId));
        } else {
            if (dictMap.isShowAllReferenced()) {
                DwrUtils.setRefValueEnumOptions(values, results, dict.getRefDict().getCn(), dicNameWithSuffix, dict.getCn(), dict.getTablename(), DataBaseEnumField.REF_ID_FOR_ALL_FIELD, defaultEnum, "_1");
            } else {
                DwrUtils.setRefValueEnumOptions(values, results, dict.getRefDict().getCn(), dicNameWithSuffix, dict.getCn(), dict.getTablename(), null, defaultEnum, "_1");
            }
        }
    }

    private boolean isFixedAsset(Map<String, Object> values) {
        try {
            String purchaseTypeId = ((EnumValues)values.get(BP_FIELD_WITH_LINK+"C_KIND")).getSelectedId();
            if (SRODEK_TRWALY_ENUM_ID.equals(purchaseTypeId)) {
                return true;
            }
        } catch (Exception e) {
            log.error("CAUGHT: "+e.getMessage(), e);
        }
        return false;
    }

    protected boolean isBudgetOwnerAccept(Map<String, Object> values) {
        try {
            String itemGroup = ((EnumValues)values.get(BP_FIELD_WITH_LINK + "I_GROUP")).getSelectedId();

            if (StringUtils.isNotBlank(itemGroup) && StringUtils.isNumeric(itemGroup)) {
                Integer itemGroupId = Integer.valueOf(itemGroup);
                EnumItem item = DataBaseEnumField.getEnumItemForTable(GRUPY_DZIALALNOSCI_TABLE_NAME, itemGroupId);
                if (SOCIAL_ITEM_GROUP_CNS.contains(item.getCn())) {
                    return false;
                }
            }
        } catch (NullPointerException e) {
        } catch (EdmException e) {
            log.error("CAUGHT: "+e.getMessage(), e);
        }
        try {
            String purchaseTypeId = ((EnumValues)values.get(BP_FIELD_WITH_LINK + "S_KIND")).getSelectedId();
            if (SOCIAL_ID_FOR_NONE_BUDGET_OWNER_ACCEPT.contains(purchaseTypeId)) {
                return false;
            }
        } catch (NullPointerException e) {
        }
        return true;
    }
}
