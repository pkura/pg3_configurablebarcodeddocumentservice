package pl.compan.docusafe.parametrization.ul.hib;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Date;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class UlObszaryMapping implements java.io.Serializable {
	
	private static final String ERROR_LABEL = "--b��d--";

	private static Logger LOG = LoggerFactory.getLogger(UlObszaryMapping.class);

	private long id;
	private String CMpk;
	private String IKind;
	private String CProject;
	private String CType;
	private String CInventory;
	private String CFundsource;
	private Integer dsuser;
	private Date createTime;
	private Integer createUser;
	private Date modifyTime;
	private Integer modifyUser;
	private String remark;

	public UlObszaryMapping() {
	}

	public UlObszaryMapping(long id, Integer dsuser) {
		this.id = id;
		this.dsuser = dsuser;
	}

	public UlObszaryMapping(long id, String CMpk, String IKind, String CProject, Integer dsuser, Date createTime, Integer createUser,
			Date modifyTime, Integer modifyUser) {
		this.id = id;
		this.CMpk = CMpk;
		this.IKind = IKind;
		this.CProject = CProject;
		this.dsuser = dsuser;
		this.createTime = createTime;
		this.createUser = createUser;
		this.modifyTime = modifyTime;
		this.modifyUser = modifyUser;
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCMpk() {
		return this.CMpk;
	}

	public void setCMpk(String CMpk) {
		this.CMpk = CMpk;
	}

	public String getIKind() {
		return this.IKind;
	}

	public void setIKind(String IKind) {
		this.IKind = IKind;
	}

	public String getCProject() {
		return this.CProject;
	}

	public void setCProject(String CProject) {
		this.CProject = CProject;
	}

	public String getCType() {
		return CType;
	}

	public void setCType(String cType) {
		CType = cType;
	}

	public String getCInventory() {
		return CInventory;
	}

	public void setCInventory(String cInventory) {
		CInventory = cInventory;
	}

	public String getCFundsource() {
		return CFundsource;
	}

	public void setCFundsource(String cFundsource) {
		CFundsource = cFundsource;
	}

	public Integer getDsuser() {
		return this.dsuser;
	}

	public Long getDsuserLong() {
		return this.dsuser != null ? this.dsuser.longValue() : null;
	}

	public void setDsuser(Integer dsuser) {
		this.dsuser = dsuser;
	}

	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(Integer createUser) {
		this.createUser = createUser;
	}

	public Date getModifyTime() {
		return this.modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public Integer getModifyUser() {
		return this.modifyUser;
	}

	public void setModifyUser(Integer modifyUser) {
		this.modifyUser = modifyUser;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

    @Deprecated
	private String getEnumName(String tableName, Integer id, boolean cn, boolean title) {
		if (id != null) {
			try {
				EnumItem item = DataBaseEnumField.getEnumItemForTable(tableName, id);
				if (cn && title) {
					return item.getCn() + ": "+ item.getTitle();
				} else if (cn && !title) {
					return item.getCn();
				} else if (!cn && title) {
					return item.getTitle();
				} else {
					return item.getId().toString();
				}
			} catch (EdmException e) {
				LOG.error(e.getMessage(), e);
				return ERROR_LABEL;
			}
		} else {
			return ".";
		}
	}
	
	public String getUserFullName() {
		try {
			try {
				DSApi.openAdmin();
				DSUser user = DSUser.findById(getDsuser().longValue());
				return user.getName()+": "+ user.asFirstnameLastname();
			} finally {
				DSApi.close();
			}
		} catch (EdmException e) {
			LOG.error(e.getMessage(), e);
		}
		return ERROR_LABEL;
	}
	
	public String getKindCnName() {
		return getIKind();
	}
	
	public String getMpkCnName() {
		return getCMpk();
	}
	
	public String getProjectCnName() {
		return getCProject();
	}
	
	public String getTypeCnName() {
		return getCType();
	}

	public String getInventoryCnName() {
		return getCInventory();
	}

	public String getFundsourceCnName() {
		return getCFundsource();
	}

	public static UlObszaryMapping find(Long id) throws HibernateException, EdmException {
		return (UlObszaryMapping) DSApi.context().session().get(UlObszaryMapping.class, id);
	}
	
	public static List<UlObszaryMapping> findWithMappingSettings(String condS, String[] fieldKeys, Map<String, String> fieldsValues) throws EdmException {
		Criteria criteria = DSApi.context().session().createCriteria(UlObszaryMapping.class);
		
		boolean runQuery = true;
		int i = 0;
		for (String field : fieldKeys) {
			if ('1' == condS.charAt(i++)) {
				if (fieldsValues.get(field) == null) {
					runQuery = false;
					break;
				}
                criteria.add(Restrictions.sqlRestriction("? like REPLACE(LTRIM(RTRIM("+field+")),'*','%')", fieldsValues.get(field).trim(), StandardBasicTypes.STRING));
			} else {
                criteria.add(Restrictions.sqlRestriction(field+" is null"));
			}
		}
		
		if (runQuery) {
			return criteria.list();
		} else {
			return null;
		}
	}
	
	public static List<UlObszaryMapping> findWithMappingSettings(int condition, String[] fieldKeys, Map<String, String> fieldsValues) throws EdmException {
		return findWithMappingSettings(StringUtils.leftPad(String.valueOf(condition), fieldsValues.size(), '0'), fieldKeys, fieldsValues);
	}
	
	public static SearchResults<UlObszaryMapping> list(String sortField, Boolean ascending, Integer offset, Integer limit) throws EdmException {
		int fullSize = DSApi.context().session().createCriteria(UlObszaryMapping.class).list().size();
		
		Criteria criteria = DSApi.context().session().createCriteria(UlObszaryMapping.class);
		if (sortField != null && ascending != null) {
			criteria.addOrder(ascending == true ? Order.asc(sortField) : Order.desc(sortField));
		}
		if (limit > 0) {
			criteria.setFirstResult(offset);
			criteria.setMaxResults(limit);
		}
		List<UlObszaryMapping> results = criteria.list();
		
		if (results != null) {
			return new SearchResultsAdapter<UlObszaryMapping>(results, offset, fullSize, UlObszaryMapping.class);
		} else {
			return null;
		}
	}
	
	public static List<UlObszaryMapping> list(String sortField, Boolean ascending) throws EdmException {
		Criteria criteria = DSApi.context().session().createCriteria(UlObszaryMapping.class);
		if (sortField != null && ascending != null) {
			criteria.addOrder(ascending == true ? Order.asc(sortField) : Order.desc(sortField));
		}
		return criteria.list();
	}
	public static List<UlObszaryMapping> find(String CMpk, String IKind, String CProject, String CType, String CInventory, String CFundsource) throws EdmException {
		Criteria criteria = DSApi.context().session().createCriteria(UlObszaryMapping.class);
		
		setEqCriteria("CMpk", CMpk, criteria);
		setEqCriteria("IKind", IKind, criteria);
		setEqCriteria("CProject", CProject, criteria);
		setEqCriteria("CType", CType, criteria);
		setEqCriteria("CInventory", CInventory, criteria);
		setEqCriteria("CFundsource", CFundsource, criteria);
			
		return criteria.list();
	}

	protected static void setEqCriteria(String property, String value, Criteria criteria) {
		criteria.add(value != null ? Restrictions.eq(property, value) : Restrictions.isNull(property));
	}
	
	public void save() throws EdmException {
		try {
			DSApi.context().session().save(this);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public void delete() throws EdmException {
		DSApi.context().session().delete(this);
	}

}
