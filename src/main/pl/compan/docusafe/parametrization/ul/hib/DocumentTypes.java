package pl.compan.docusafe.parametrization.ul.hib;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.imports.simple.repository.SimpleRepositoryAttribute;
import pl.compan.docusafe.ws.simple.ul.DictionaryServicesDocumentTypesStub.PurchaseDocumentType;

public class DocumentTypes implements java.io.Serializable {
	private Integer id;
	private boolean available;
	private Integer czyAktywny;
	private Integer czywal;
	private String nazwa;
	private String typdokIdn;
	private String typdokIds;
	private Long typdokzakId;

	public DocumentTypes() {
	}
	
	public DocumentTypes(Integer id, boolean available) {
		this.id = id;
		this.available = available;
	}

	public DocumentTypes(Integer id, boolean available, Integer czyAktywny, Integer czywal, String nazwa,
			String typdokIdn, String typdokIds, Long typdokzakId) {
		this.id = id;
		this.available = available;
		this.czyAktywny = czyAktywny;
		this.czywal = czywal;
		this.nazwa = nazwa;
		this.typdokIdn = typdokIdn;
		this.typdokIds = typdokIds;
		this.typdokzakId = typdokzakId;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isAvailable() {
		return this.available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public Integer getCzyAktywny() {
		return this.czyAktywny;
	}

	public void setCzyAktywny(Integer czyAktywny) {
		this.czyAktywny = czyAktywny;
	}

	public Integer getCzywal() {
		return this.czywal;
	}

	public void setCzywal(Integer czywal) {
		this.czywal = czywal;
	}

	public String getNazwa() {
		return this.nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getTypdokIdn() {
		return this.typdokIdn;
	}

	public void setTypdokIdn(String typdokIdn) {
		this.typdokIdn = typdokIdn;
	}

	public String getTypdokIds() {
		return this.typdokIds;
	}

	public void setTypdokIds(String typdokIds) {
		this.typdokIds = typdokIds;
	}

	public Long getTypdokzakId() {
		return this.typdokzakId;
	}

	public void setTypdokzakId(Long typdokzakId) {
		this.typdokzakId = typdokzakId;
	}

	public void save() throws EdmException {
		try {
			DSApi.context().session().save(this);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public void setAllFieldsFromServiceObject(PurchaseDocumentType item) {
		this.typdokzakId = item.getTypdokzakId();
		this.czyAktywny = item.getCzyAktywny();
		this.czywal = item.getCzywal();
		this.nazwa = item.getNazwa();
		this.typdokIdn = item.getTypdokIdn();
		this.typdokIds = item.getTypdokIds();
	}	
	
	public static List<DocumentTypes> findAvailable(boolean available) throws EdmException {
		try {
			Criteria criteria = DSApi.context().session().createCriteria(DocumentTypes.class);
			criteria.add(Restrictions.eq("available", available));
			return criteria.list();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
}
