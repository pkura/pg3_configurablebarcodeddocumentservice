package pl.compan.docusafe.parametrization.ul.hib;

import java.math.BigDecimal;

import org.hibernate.HibernateException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;


//Generated 2013-02-05 12:40:06 by Hibernate Tools 3.4.0.CR1

public class UlBudgetPosition implements java.io.Serializable {

	private Long id;
	private Integer INo;
	private Integer IGroup;
	private Integer IKind;
	private Integer IType;
	private BigDecimal IAmount;
	private Integer IField;
	private Integer CMpk;
	private Integer CUser;
	private Integer CProject;
	private Integer CBudget;
	private Integer CStructureBudgetKo;
	private Integer CStructureBudgetPosition;
	private String CStructureBudgetPositionRaw;
	private Integer CResource;
	private String CResourceRaw;
	private Integer CPosition;
	private Integer CFundSource;
	private Integer CKind;
	private Integer CType;
	private Integer CMt08;
	private Integer CInventory;
	private Integer COrder;
	private Integer CGroup;
	private Integer CCar;
	private Integer SZfssWydatki;
	private Integer SFpmsid852120;
	private Integer SFpmsid852;
	private Integer SFpmsidTypPowierzchni;
	private Integer SFpmsidTypPowierzchniDs852;
	private Integer SFpmsidTypPowierzStolowki;
	private Integer SKosztyRodzajoweZespol8;
	private Integer SFpmsidGrupaKosztu00;
	private Integer SFpmsidGrupaKosztuStolowki;
	private Integer SFpmsidGrupaKosztuDs;
	private Integer SKind;
    private String CKindName;
    private String BReservationCode;
    private Long BReservationPositionId;

    private static UlBudgetPosition instance;
	
    public static synchronized UlBudgetPosition getInstance() {
        if (instance == null)
            instance = new UlBudgetPosition();
        return instance;
    }	
	
	public UlBudgetPosition() {
	}

	public UlBudgetPosition(Long id) {
		this.id = id;
	}

	public UlBudgetPosition(Long id, Integer INo, Integer IGroup, Integer IKind, Integer IType, BigDecimal IAmount, Integer IField, Integer CMpk,
			Integer CUser, Integer CProject, Integer CBudget, Integer CResource, Integer CPosition, Integer CFundSource, Integer CKind, Integer CType,
			Integer CMt08, Integer CInventory, Integer COrder, Integer CGroup, Integer CCar, Integer SZfssWydatki, Integer SFpmsid852120, Integer SFpmsid852,
			Integer SFpmsidTypPowierzchni, Integer SFpmsidTypPowierzchniDs852, Integer SFpmsidTypPowierzStolowki, Integer SKosztyRodzajoweZespol8,
			Integer SFpmsidGrupaKosztu00, Integer SFpmsidGrupaKosztuStolowki, Integer SFpmsidGrupaKosztuDs) {
		this.id = id;
		this.INo = INo;
		this.IGroup = IGroup;
		this.IKind = IKind;
		this.IType = IType;
		this.IAmount = IAmount;
		this.IField = IField;
		this.CMpk = CMpk;
		this.CUser = CUser;
		this.CProject = CProject;
		this.CBudget = CBudget;
		this.CResource = CResource;
		this.CPosition = CPosition;
		this.CFundSource = CFundSource;
		this.CKind = CKind;
		this.CType = CType;
		this.CMt08 = CMt08;
		this.CInventory = CInventory;
		this.COrder = COrder;
		this.CGroup = CGroup;
		this.CCar = CCar;
		this.SZfssWydatki = SZfssWydatki;
		this.SFpmsid852120 = SFpmsid852120;
		this.SFpmsid852 = SFpmsid852;
		this.SFpmsidTypPowierzchni = SFpmsidTypPowierzchni;
		this.SFpmsidTypPowierzchniDs852 = SFpmsidTypPowierzchniDs852;
		this.SFpmsidTypPowierzStolowki = SFpmsidTypPowierzStolowki;
		this.SKosztyRodzajoweZespol8 = SKosztyRodzajoweZespol8;
		this.SFpmsidGrupaKosztu00 = SFpmsidGrupaKosztu00;
		this.SFpmsidGrupaKosztuStolowki = SFpmsidGrupaKosztuStolowki;
		this.SFpmsidGrupaKosztuDs = SFpmsidGrupaKosztuDs;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getINo() {
		return this.INo;
	}

	public void setINo(Integer INo) {
		this.INo = INo;
	}

	public Integer getIGroup() {
		return this.IGroup;
	}

	public void setIGroup(Integer IGroup) {
		this.IGroup = IGroup;
	}

	public Integer getIKind() {
		return this.IKind;
	}

	public void setIKind(Integer IKind) {
		this.IKind = IKind;
	}

	public Integer getIType() {
		return this.IType;
	}

	public void setIType(Integer IType) {
		this.IType = IType;
	}

	public BigDecimal getIAmount() {
		return this.IAmount;
	}

	public BigDecimal getIAmountOrDefault() {
		return this.IAmount != null ? this.IAmount : BigDecimal.ZERO;
	}

	public void setIAmount(BigDecimal IAmount) {
		this.IAmount = IAmount;
	}

	public Integer getIField() {
		return this.IField;
	}

	public void setIField(Integer IField) {
		this.IField = IField;
	}

	public Integer getCMpk() {
		return this.CMpk;
	}

	public void setCMpk(Integer CMpk) {
		this.CMpk = CMpk;
	}

	public Integer getCUser() {
		return this.CUser;
	}

	public void setCUser(Integer CUser) {
		this.CUser = CUser;
	}

	public Integer getCProject() {
		return this.CProject;
	}

	public void setCProject(Integer CProject) {
		this.CProject = CProject;
	}

	public Integer getCBudget() {
		return this.CBudget;
	}

	public void setCBudget(Integer CBudget) {
		this.CBudget = CBudget;
	}

	public Integer getCResource() {
		return this.CResource;
	}

	public void setCResource(Integer CResource) {
		this.CResource = CResource;
	}

	public Integer getCPosition() {
		return this.CPosition;
	}

	public void setCPosition(Integer CPosition) {
		this.CPosition = CPosition;
	}

	public Integer getCFundSource() {
		return this.CFundSource;
	}

	public void setCFundSource(Integer CFundSource) {
		this.CFundSource = CFundSource;
	}

	public Integer getCKind() {
		return this.CKind;
	}

	public void setCKind(Integer CKind) {
		this.CKind = CKind;
	}

	public Integer getCType() {
		return this.CType;
	}

	public void setCType(Integer CType) {
		this.CType = CType;
	}

	public Integer getCMt08() {
		return this.CMt08;
	}

	public void setCMt08(Integer CMt08) {
		this.CMt08 = CMt08;
	}

	public Integer getCInventory() {
		return this.CInventory;
	}

	public void setCInventory(Integer CInventory) {
		this.CInventory = CInventory;
	}

	public Integer getCOrder() {
		return this.COrder;
	}

	public void setCOrder(Integer COrder) {
		this.COrder = COrder;
	}

	public Integer getCGroup() {
		return this.CGroup;
	}

	public void setCGroup(Integer CGroup) {
		this.CGroup = CGroup;
	}

	public Integer getCCar() {
		return this.CCar;
	}

	public void setCCar(Integer CCar) {
		this.CCar = CCar;
	}

	public Integer getSZfssWydatki() {
		return this.SZfssWydatki;
	}

	public void setSZfssWydatki(Integer SZfssWydatki) {
		this.SZfssWydatki = SZfssWydatki;
	}

	public Integer getSFpmsid852120() {
		return this.SFpmsid852120;
	}

	public void setSFpmsid852120(Integer SFpmsid852120) {
		this.SFpmsid852120 = SFpmsid852120;
	}

	public Integer getSFpmsid852() {
		return this.SFpmsid852;
	}

	public void setSFpmsid852(Integer SFpmsid852) {
		this.SFpmsid852 = SFpmsid852;
	}

	public Integer getSFpmsidTypPowierzchni() {
		return this.SFpmsidTypPowierzchni;
	}

	public void setSFpmsidTypPowierzchni(Integer SFpmsidTypPowierzchni) {
		this.SFpmsidTypPowierzchni = SFpmsidTypPowierzchni;
	}

	public Integer getSFpmsidTypPowierzchniDs852() {
		return this.SFpmsidTypPowierzchniDs852;
	}

	public void setSFpmsidTypPowierzchniDs852(Integer SFpmsidTypPowierzchniDs852) {
		this.SFpmsidTypPowierzchniDs852 = SFpmsidTypPowierzchniDs852;
	}

	public Integer getSFpmsidTypPowierzStolowki() {
		return this.SFpmsidTypPowierzStolowki;
	}

	public void setSFpmsidTypPowierzStolowki(Integer SFpmsidTypPowierzStolowki) {
		this.SFpmsidTypPowierzStolowki = SFpmsidTypPowierzStolowki;
	}

	public Integer getSKosztyRodzajoweZespol8() {
		return this.SKosztyRodzajoweZespol8;
	}

	public void setSKosztyRodzajoweZespol8(Integer SKosztyRodzajoweZespol8) {
		this.SKosztyRodzajoweZespol8 = SKosztyRodzajoweZespol8;
	}

	public Integer getSFpmsidGrupaKosztu00() {
		return this.SFpmsidGrupaKosztu00;
	}

	public void setSFpmsidGrupaKosztu00(Integer SFpmsidGrupaKosztu00) {
		this.SFpmsidGrupaKosztu00 = SFpmsidGrupaKosztu00;
	}

	public Integer getSFpmsidGrupaKosztuStolowki() {
		return this.SFpmsidGrupaKosztuStolowki;
	}

	public void setSFpmsidGrupaKosztuStolowki(Integer SFpmsidGrupaKosztuStolowki) {
		this.SFpmsidGrupaKosztuStolowki = SFpmsidGrupaKosztuStolowki;
	}

	public Integer getSFpmsidGrupaKosztuDs() {
		return this.SFpmsidGrupaKosztuDs;
	}

	public void setSFpmsidGrupaKosztuDs(Integer SFpmsidGrupaKosztuDs) {
		this.SFpmsidGrupaKosztuDs = SFpmsidGrupaKosztuDs;
	}
	
    public Integer getSKind() {
		return this.SKind;
	}

	public void setSKind(Integer sKind) {
		this.SKind = sKind;
	}

    public void save() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

	public UlBudgetPosition find(Long id) throws EdmException
    {
        return Finder.find(UlBudgetPosition.class, id);
    }

    public Integer getCStructureBudgetKo() {
        return CStructureBudgetKo;
    }

    public void setCStructureBudgetKo(Integer CStructureBudgetKo) {
        this.CStructureBudgetKo = CStructureBudgetKo;
    }

    public Integer getCStructureBudgetPosition() {
        return CStructureBudgetPosition;
    }

    public void setCStructureBudgetPosition(Integer CStructureBudgetPosition) {
        this.CStructureBudgetPosition = CStructureBudgetPosition;
    }

    public String getCKindName() {
        return CKindName;
    }

    public void setCKindName(String CKindName) {
        this.CKindName = CKindName;
    }

    public String getCResourceRaw() {
        return CResourceRaw;
    }

    public void setCResourceRaw(String CResourceRaw) {
        this.CResourceRaw = CResourceRaw;
    }

    public String getCStructureBudgetPositionRaw() {
        return CStructureBudgetPositionRaw;
    }

    public void setCStructureBudgetPositionRaw(String CStructureBudgetPositionRaw) {
        this.CStructureBudgetPositionRaw = CStructureBudgetPositionRaw;
    }

    public String getBReservationCode() {
        return BReservationCode;
    }

    public void setBReservationCode(String BReservationCode) {
        this.BReservationCode = BReservationCode;
    }

    public Long getBReservationPositionId() {
        return BReservationPositionId;
    }

    public void setBReservationPositionId(Long BReservationPositionId) {
        this.BReservationPositionId = BReservationPositionId;
    }
}