package pl.compan.docusafe.parametrization.ul.hib;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

public class CostInvoiceDzialalnosc
{

	private Long id;
	private String grupaDzialIdn;
	private String rodzajDzialIdn;
	private Boolean slownikKomorek;
	private Boolean numerProjektu;
	private Boolean numerZlecenia;
	private Boolean grupyMt08;
	private Boolean samochody;
	private Boolean numerInwentarzowy;
	private Boolean oznaczenieGrupyPomieszczen;
	private Boolean rodzajKosztu;
	private Boolean zrodloFinansowania;
	
    private Boolean zfssWydatki;
    private Boolean fpmsid_852_1_20;
    private Boolean fpmsid_852;
    private Boolean fpmsidGrupaKosztuDs;
    private Boolean fpmsidTypPowierzchni;
    private Boolean fpmsidGrupaKosztu_00;
    private Boolean fpmsidTypPowierzchni_ds852;
    private Boolean fpmsidGrupaKosztuStolowki;
    private Boolean fpmsidTypPowierzStolowki;
    private Boolean kosztyRodzajoweZespol_8;
	
	public Long getId()
	{
		return id;
	}
	public void setId(Long id)
	{
		this.id = id;
	}
	public String getGrupaDzialIdn()
	{
		return grupaDzialIdn;
	}
	public void setGrupaDzialIdn(String grupaDzialIdn)
	{
		this.grupaDzialIdn = grupaDzialIdn;
	}
	public String getRodzajDzialIdn()
	{
		return rodzajDzialIdn;
	}
	public void setRodzajDzialIdn(String rodzajDzialIdn)
	{
		this.rodzajDzialIdn = rodzajDzialIdn;
	}
	public Boolean getSlownikKomorek()
	{
		return slownikKomorek;
	}
	public void setSlownikKomorek(Boolean slownikKomorek)
	{
		this.slownikKomorek = slownikKomorek;
	}
	public Boolean getNumerProjektu()
	{
		return numerProjektu;
	}
	public void setNumerProjektu(Boolean numerProjektu)
	{
		this.numerProjektu = numerProjektu;
	}
	public Boolean getNumerZlecenia()
	{
		return numerZlecenia;
	}
	public void setNumerZlecenia(Boolean numerZlecenia)
	{
		this.numerZlecenia = numerZlecenia;
	}
	public Boolean getGrupyMt08() {
		return grupyMt08;
	}
	public void setGrupyMt08(Boolean grupyMt08) {
		this.grupyMt08 = grupyMt08;
	}
	public Boolean getSamochody() {
		return samochody;
	}
	public void setSamochody(Boolean samochody) {
		this.samochody = samochody;
	}
	public Boolean getNumerInwentarzowy()
	{
		return numerInwentarzowy;
	}
	public void setNumerInwentarzowy(Boolean numerInwentarzowy)
	{
		this.numerInwentarzowy = numerInwentarzowy;
	}
	public Boolean getOznaczenieGrupyPomieszczen()
	{
		return oznaczenieGrupyPomieszczen;
	}
	public void setOznaczenieGrupyPomieszczen(Boolean oznaczenieGrupyPomieszczen)
	{
		this.oznaczenieGrupyPomieszczen = oznaczenieGrupyPomieszczen;
	}
	public Boolean getRodzajKosztu()
	{
		return rodzajKosztu;
	}
	public void setRodzajKosztu(Boolean rodzajKosztu)
	{
		this.rodzajKosztu = rodzajKosztu;
	}
	public Boolean getZrodloFinansowania()
	{
		return zrodloFinansowania;
	}
	public void setZrodloFinansowania(Boolean zrodloFinansowania)
	{
		this.zrodloFinansowania = zrodloFinansowania;
	}
	
	public Boolean getZfssWydatki() {
		return zfssWydatki;
	}
	public void setZfssWydatki(Boolean zfssWydatki) {
		this.zfssWydatki = zfssWydatki;
	}
	public Boolean getFpmsid_852_1_20() {
		return fpmsid_852_1_20;
	}
	public void setFpmsid_852_1_20(Boolean fpmsid_852_1_20) {
		this.fpmsid_852_1_20 = fpmsid_852_1_20;
	}
	public Boolean getFpmsid_852() {
		return fpmsid_852;
	}
	public void setFpmsid_852(Boolean fpmsid_852) {
		this.fpmsid_852 = fpmsid_852;
	}
	public Boolean getFpmsidGrupaKosztuDs() {
		return fpmsidGrupaKosztuDs;
	}
	public void setFpmsidGrupaKosztuDs(Boolean fpmsidGrupaKosztuDs) {
		this.fpmsidGrupaKosztuDs = fpmsidGrupaKosztuDs;
	}
	public Boolean getFpmsidTypPowierzchni() {
		return fpmsidTypPowierzchni;
	}
	public void setFpmsidTypPowierzchni(Boolean fpmsidTypPowierzchni) {
		this.fpmsidTypPowierzchni = fpmsidTypPowierzchni;
	}
	public Boolean getFpmsidGrupaKosztu_00() {
		return fpmsidGrupaKosztu_00;
	}
	public void setFpmsidGrupaKosztu_00(Boolean fpmsidGrupaKosztu_00) {
		this.fpmsidGrupaKosztu_00 = fpmsidGrupaKosztu_00;
	}
	public Boolean getFpmsidTypPowierzchni_ds852() {
		return fpmsidTypPowierzchni_ds852;
	}
	public void setFpmsidTypPowierzchni_ds852(Boolean fpmsidTypPowierzchni_ds852) {
		this.fpmsidTypPowierzchni_ds852 = fpmsidTypPowierzchni_ds852;
	}
	public Boolean getFpmsidGrupaKosztuStolowki() {
		return fpmsidGrupaKosztuStolowki;
	}
	public void setFpmsidGrupaKosztuStolowki(Boolean fpmsidGrupaKosztuStolowki) {
		this.fpmsidGrupaKosztuStolowki = fpmsidGrupaKosztuStolowki;
	}
	public Boolean getFpmsidTypPowierzStolowki() {
		return fpmsidTypPowierzStolowki;
	}
	public void setFpmsidTypPowierzStolowki(Boolean fpmsidTypPowierzStolowki) {
		this.fpmsidTypPowierzStolowki = fpmsidTypPowierzStolowki;
	}
	public Boolean getKosztyRodzajoweZespol_8() {
		return kosztyRodzajoweZespol_8;
	}
	public void setKosztyRodzajoweZespol_8(Boolean kosztyRodzajoweZespol_8) {
		this.kosztyRodzajoweZespol_8 = kosztyRodzajoweZespol_8;
	}
	public static CostInvoiceDzialalnosc findByCode(String rodzajCode) throws EdmException {
		try {
			Criteria criteria = DSApi.context().session().createCriteria(CostInvoiceDzialalnosc.class);
			
			if (StringUtils.isNotEmpty(rodzajCode)) {
				criteria.add(Restrictions.eq("rodzajDzialIdn", rodzajCode));
			} else {
				return null;
			}
			
			if (!criteria.list().isEmpty()) {
				return (CostInvoiceDzialalnosc) criteria.list().get(0);
			}
			
			return null;
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
}
