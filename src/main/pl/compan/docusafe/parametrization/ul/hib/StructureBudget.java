package pl.compan.docusafe.parametrization.ul.hib;

import org.hibernate.HibernateException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgetsStub;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * User: kk
 * Date: 23.12.13
 * Time: 12:37
 */

@Entity
@Table(name = StructureBudget.TABLE_NAME)
public class StructureBudget {

    public static final String TABLE_NAME = "dsg_ul_services_structure_budgets";
    public static final String STRUCTURE_BUDGET_KO_VIEW_NAME = "dsg_ul_structure_ko_budget";
    public static final String STRUCTURE_BUDGET_POSITION_VIEW_NAME = "dsg_ul_structure_budget_position";

    private Long id;
    private Boolean available;
    private Long bdBudzetId;
    private String bdBudzetIdm;
    private Long bdBudzetRodzajId;
    private Long bdBudzetRodzajZrodlaId;
    private Long bdGrupaRodzajId;
    private String bdRodzaj;
    private String bdStrBudzetIdn;
    private String bdStrBudzetIds;
    private Long bdSzablonPozId;
    private String budWOkrNazwa;
    private Long budzetId;
    private String budzetIdm;
    private String budzetIds;
    private String budzetNazwa;
    private Integer czyAktywna;
    private String dokBdStrBudzetIdn;
    private String dokKomNazwa;
    private BigDecimal dokPozLimit;
    private String dokPozNazwa;
    private Integer dokPozNrpoz;
    private BigDecimal dokPozPIlosc;
    private BigDecimal dokPozPKosz;
    private BigDecimal dokPozRIlosc;
    private BigDecimal dokPozRKoszt;
    private BigDecimal dokPozRezIlosc;
    private BigDecimal dokPozRezKoszt;
    private Long okrrozlId;
    private Long pozycjaPodrzednaId;
    private String strBudNazwa;
    private Integer wspolczynnikPozycji;
    private Long wytworId;
    private Long zrodloId;
    private String zrodloIdn;
    private String zrodloNazwa;
    private BigDecimal zrodloPKoszt;
    private BigDecimal zrodloRKoszt;
    private BigDecimal zrodloRezKoszt;

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    public Long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "available", nullable = false)
    public Boolean getAvailable() {
        return this.available;
    }

    public StructureBudget setAvailable(Boolean available) {
        this.available = available;
        return this;
    }

    @Column(name = "bd_budzet_id")
    public Long getBdBudzetId() {
        return this.bdBudzetId;
    }

    public void setBdBudzetId(Long bdBudzetId) {
        this.bdBudzetId = bdBudzetId;
    }

    @Column(name = "bd_budzet_idm", length = 100)
    public String getBdBudzetIdm() {
        return this.bdBudzetIdm;
    }

    public void setBdBudzetIdm(String bdBudzetIdm) {
        this.bdBudzetIdm = bdBudzetIdm;
    }

    @Column(name = "bd_budzet_rodzaj_id")
    public Long getBdBudzetRodzajId() {
        return this.bdBudzetRodzajId;
    }

    public void setBdBudzetRodzajId(Long bdBudzetRodzajId) {
        this.bdBudzetRodzajId = bdBudzetRodzajId;
    }

    @Column(name = "bd_budzet_rodzaj_zrodla_id")
    public Long getBdBudzetRodzajZrodlaId() {
        return this.bdBudzetRodzajZrodlaId;
    }

    public void setBdBudzetRodzajZrodlaId(Long bdBudzetRodzajZrodlaId) {
        this.bdBudzetRodzajZrodlaId = bdBudzetRodzajZrodlaId;
    }

    @Column(name = "bd_grupa_rodzaj_id")
    public Long getBdGrupaRodzajId() {
        return this.bdGrupaRodzajId;
    }

    public void setBdGrupaRodzajId(Long bdGrupaRodzajId) {
        this.bdGrupaRodzajId = bdGrupaRodzajId;
    }

    @Column(name = "bd_rodzaj", length = 100)
    public String getBdRodzaj() {
        return this.bdRodzaj;
    }

    public void setBdRodzaj(String bdRodzaj) {
        this.bdRodzaj = bdRodzaj;
    }

    @Column(name = "bd_str_budzet_idn", length = 100)
    public String getBdStrBudzetIdn() {
        return this.bdStrBudzetIdn;
    }

    public void setBdStrBudzetIdn(String bdStrBudzetIdn) {
        this.bdStrBudzetIdn = bdStrBudzetIdn;
    }

    @Column(name = "bd_str_budzet_ids", length = 100)
    public String getBdStrBudzetIds() {
        return this.bdStrBudzetIds;
    }

    public void setBdStrBudzetIds(String bdStrBudzetIds) {
        this.bdStrBudzetIds = bdStrBudzetIds;
    }

    @Column(name = "bd_szablon_poz_id")
    public Long getBdSzablonPozId() {
        return this.bdSzablonPozId;
    }

    public void setBdSzablonPozId(Long bdSzablonPozId) {
        this.bdSzablonPozId = bdSzablonPozId;
    }

    @Column(name = "bud_w_okr_nazwa", length = 100)
    public String getBudWOkrNazwa() {
        return this.budWOkrNazwa;
    }

    public void setBudWOkrNazwa(String budWOkrNazwa) {
        this.budWOkrNazwa = budWOkrNazwa;
    }

    @Column(name = "budzet_id")
    public Long getBudzetId() {
        return this.budzetId;
    }

    public void setBudzetId(Long budzetId) {
        this.budzetId = budzetId;
    }

    @Column(name = "budzet_idm", length = 100)
    public String getBudzetIdm() {
        return this.budzetIdm;
    }

    public void setBudzetIdm(String budzetIdm) {
        this.budzetIdm = budzetIdm;
    }

    @Column(name = "budzet_ids", length = 100)
    public String getBudzetIds() {
        return this.budzetIds;
    }

    public void setBudzetIds(String budzetIds) {
        this.budzetIds = budzetIds;
    }

    @Column(name = "budzet_nazwa", length = 100)
    public String getBudzetNazwa() {
        return this.budzetNazwa;
    }

    public void setBudzetNazwa(String budzetNazwa) {
        this.budzetNazwa = budzetNazwa;
    }

    @Column(name = "czy_aktywna")
    public Integer getCzyAktywna() {
        return this.czyAktywna;
    }

    public void setCzyAktywna(Integer czyAktywna) {
        this.czyAktywna = czyAktywna;
    }

    @Column(name = "dok_bd_str_budzet_idn", length = 100)
    public String getDokBdStrBudzetIdn() {
        return this.dokBdStrBudzetIdn;
    }

    public void setDokBdStrBudzetIdn(String dokBdStrBudzetIdn) {
        this.dokBdStrBudzetIdn = dokBdStrBudzetIdn;
    }

    @Column(name = "dok_kom_nazwa", length = 100)
    public String getDokKomNazwa() {
        return this.dokKomNazwa;
    }

    public void setDokKomNazwa(String dokKomNazwa) {
        this.dokKomNazwa = dokKomNazwa;
    }

    @Column(name = "dok_poz_limit", precision = 15, scale = 4)
    public BigDecimal getDokPozLimit() {
        return this.dokPozLimit;
    }

    public void setDokPozLimit(BigDecimal dokPozLimit) {
        this.dokPozLimit = dokPozLimit;
    }

    @Column(name = "dok_poz_nazwa", length = 100)
    public String getDokPozNazwa() {
        return this.dokPozNazwa;
    }

    public void setDokPozNazwa(String dokPozNazwa) {
        this.dokPozNazwa = dokPozNazwa;
    }

    @Column(name = "dok_poz_nrpoz")
    public Integer getDokPozNrpoz() {
        return this.dokPozNrpoz;
    }

    public void setDokPozNrpoz(Integer dokPozNrpoz) {
        this.dokPozNrpoz = dokPozNrpoz;
    }

    @Column(name = "dok_poz_p_ilosc", precision = 15, scale = 4)
    public BigDecimal getDokPozPIlosc() {
        return this.dokPozPIlosc;
    }

    public void setDokPozPIlosc(BigDecimal dokPozPIlosc) {
        this.dokPozPIlosc = dokPozPIlosc;
    }

    @Column(name = "dok_poz_p_kosz", precision = 15, scale = 4)
    public BigDecimal getDokPozPKosz() {
        return this.dokPozPKosz;
    }

    public void setDokPozPKosz(BigDecimal dokPozPKosz) {
        this.dokPozPKosz = dokPozPKosz;
    }

    @Column(name = "dok_poz_r_ilosc", precision = 15, scale = 4)
    public BigDecimal getDokPozRIlosc() {
        return this.dokPozRIlosc;
    }

    public void setDokPozRIlosc(BigDecimal dokPozRIlosc) {
        this.dokPozRIlosc = dokPozRIlosc;
    }

    @Column(name = "dok_poz_r_koszt", precision = 15, scale = 4)
    public BigDecimal getDokPozRKoszt() {
        return this.dokPozRKoszt;
    }

    public void setDokPozRKoszt(BigDecimal dokPozRKoszt) {
        this.dokPozRKoszt = dokPozRKoszt;
    }

    @Column(name = "dok_poz_rez_ilosc", precision = 15, scale = 4)
    public BigDecimal getDokPozRezIlosc() {
        return this.dokPozRezIlosc;
    }

    public void setDokPozRezIlosc(BigDecimal dokPozRezIlosc) {
        this.dokPozRezIlosc = dokPozRezIlosc;
    }

    @Column(name = "dok_poz_rez_koszt", precision = 15, scale = 4)
    public BigDecimal getDokPozRezKoszt() {
        return this.dokPozRezKoszt;
    }

    public void setDokPozRezKoszt(BigDecimal dokPozRezKoszt) {
        this.dokPozRezKoszt = dokPozRezKoszt;
    }

    @Column(name = "okrrozl_id")
    public Long getOkrrozlId() {
        return this.okrrozlId;
    }

    public void setOkrrozlId(Long okrrozlId) {
        this.okrrozlId = okrrozlId;
    }

    @Column(name = "pozycja_podrzedna_id")
    public Long getPozycjaPodrzednaId() {
        return this.pozycjaPodrzednaId;
    }

    public void setPozycjaPodrzednaId(Long pozycjaPodrzednaId) {
        this.pozycjaPodrzednaId = pozycjaPodrzednaId;
    }

    @Column(name = "str_bud_nazwa", length = 100)
    public String getStrBudNazwa() {
        return this.strBudNazwa;
    }

    public void setStrBudNazwa(String strBudNazwa) {
        this.strBudNazwa = strBudNazwa;
    }

    @Column(name = "wspolczynnik_pozycji")
    public Integer getWspolczynnikPozycji() {
        return this.wspolczynnikPozycji;
    }

    public void setWspolczynnikPozycji(Integer wspolczynnikPozycji) {
        this.wspolczynnikPozycji = wspolczynnikPozycji;
    }

    @Column(name = "wytwor_id")
    public Long getWytworId() {
        return this.wytworId;
    }

    public void setWytworId(Long wytworId) {
        this.wytworId = wytworId;
    }

    @Column(name = "zrodlo_id")
    public Long getZrodloId() {
        return this.zrodloId;
    }

    public void setZrodloId(Long zrodloId) {
        this.zrodloId = zrodloId;
    }

    @Column(name = "zrodlo_idn", length = 100)
    public String getZrodloIdn() {
        return this.zrodloIdn;
    }

    public void setZrodloIdn(String zrodloIdn) {
        this.zrodloIdn = zrodloIdn;
    }

    @Column(name = "zrodlo_nazwa", length = 100)
    public String getZrodloNazwa() {
        return this.zrodloNazwa;
    }

    public void setZrodloNazwa(String zrodloNazwa) {
        this.zrodloNazwa = zrodloNazwa;
    }

    @Column(name = "zrodlo_p_koszt", precision = 15, scale = 4)
    public BigDecimal getZrodloPKoszt() {
        return this.zrodloPKoszt;
    }

    public void setZrodloPKoszt(BigDecimal zrodloPKoszt) {
        this.zrodloPKoszt = zrodloPKoszt;
    }

    @Column(name = "zrodlo_r_koszt", precision = 15, scale = 4)
    public BigDecimal getZrodloRKoszt() {
        return this.zrodloRKoszt;
    }

    public void setZrodloRKoszt(BigDecimal zrodloRKoszt) {
        this.zrodloRKoszt = zrodloRKoszt;
    }

    @Column(name = "zrodlo_rez_koszt", precision = 15, scale = 4)
    public BigDecimal getZrodloRezKoszt() {
        return this.zrodloRezKoszt;
    }

    public void setZrodloRezKoszt(BigDecimal zrodloRezKoszt) {
        this.zrodloRezKoszt = zrodloRezKoszt;
    }

    public StructureBudget setAllFieldsFromServiceObject(DictionaryServicesUniversityStructureBudgetsStub.UniversityStructureBudget item) {
        this.setBdBudzetId(item.getBd_budzet_id());
        this.setBdBudzetIdm(item.getBd_budzet_idm());
        this.setBdBudzetRodzajId(item.getBd_budzet_rodzaj_id());
        this.setBdBudzetRodzajZrodlaId(item.getBd_budzet_rodzaj_zrodla_id());
        this.setBdGrupaRodzajId(item.getBd_grupa_rodzaj_id());
        this.setBdRodzaj(item.getBd_rodzaj());
        this.setBdStrBudzetIdn(item.getBd_str_budzet_idn());
        this.setBdStrBudzetIds(item.getBd_str_budzet_ids());
        this.setBdSzablonPozId(item.getBd_szablon_poz_id());
        this.setBudWOkrNazwa(item.getBud_w_okr_nazwa());
        this.setBudzetId(item.getBudzet_id());
        this.setBudzetIdm(item.getBudzet_idm());
        this.setBudzetIds(item.getBudzet_ids());
        this.setBudzetNazwa(item.getBudzet_nazwa());
        this.setCzyAktywna(item.getCzy_aktywna());
        this.setDokBdStrBudzetIdn(item.getDok_bd_str_budzet_idn());
        this.setDokKomNazwa(item.getDok_kom_nazwa());
        this.setDokPozLimit(item.getDok_poz_limit());
        this.setDokPozNazwa(item.getDok_poz_nazwa());
        this.setDokPozNrpoz(item.getDok_poz_nrpoz());
        this.setDokPozPIlosc(item.getDok_poz_p_ilosc());
        this.setDokPozPKosz(item.getDok_poz_p_kosz());
        this.setDokPozRIlosc(item.getDok_poz_r_ilosc());
        this.setDokPozRKoszt(item.getDok_poz_r_koszt());
        this.setDokPozRezIlosc(item.getDok_poz_rez_ilosc());
        this.setDokPozRezKoszt(item.getDok_poz_rez_koszt());
        this.setOkrrozlId(item.getOkrrozl_id());
        this.setPozycjaPodrzednaId(item.getPozycja_podrzedna_id());
        this.setStrBudNazwa(item.getStr_bud_nazwa());
        this.setWspolczynnikPozycji(item.getWspolczynnik_pozycji());
        this.setWytworId(item.getWytwor_id());
        this.setZrodloId(item.getZrodlo_id());
        this.setZrodloIdn(item.getZrodlo_idn());
        this.setZrodloNazwa(item.getZrodlo_nazwa());
        this.setZrodloPKoszt(item.getZrodlo_p_koszt());
        this.setZrodloRKoszt(item.getZrodlo_r_koszt());
        this.setZrodloRezKoszt(item.getZrodlo_rez_koszt());
        return this;
    }

    public void save() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }
}

