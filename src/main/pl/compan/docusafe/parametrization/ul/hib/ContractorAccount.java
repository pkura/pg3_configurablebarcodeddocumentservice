package pl.compan.docusafe.parametrization.ul.hib;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.ws.ul.DictionaryServicesSupplierStub.SupplierBankAccount;

public class ContractorAccount implements java.io.Serializable {


	private int id;
	private boolean available;
	private long bankId;
	private String bankIdn;
	private String bankNazwa;
	private Integer czyIban;
	private long dostawcaId;
	private String dostawcaIdn;
	private Integer kolejnoscUzycia;
	private String kontoIdm;
	private long kontobankoweId;
	private String nazwa;
	private String numerKonta;
	private String symbolwaluty;
	private long typkursId;
	private String typkursIdn;
	private long walutaId;
	private long zrodkurswalId;
	private String zrodkurswalIdn;
	private String zrodkurswalNazwa;

	public ContractorAccount() {
	}

	public ContractorAccount(int id) {
		this.id = id;
	}

	public ContractorAccount(int id, boolean available, long bankId, String bankIdn, String bankNazwa, Integer czyIban, long dostawcaId,
			String dostawcaIdn, Integer kolejnoscUzycia, String kontoIdm, long kontobankoweId, String nazwa, String numerKonta, String symbolwaluty,
			long typkursId, String typkursIdn, long walutaId, long zrodkurswalId, String zrodkurswalIdn, String zrodkurswalNazwa) {
		this.id = id;
		this.available = available;
		this.bankId = bankId;
		this.bankIdn = bankIdn;
		this.bankNazwa = bankNazwa;
		this.czyIban = czyIban;
		this.dostawcaId = dostawcaId;
		this.dostawcaIdn = dostawcaIdn;
		this.kolejnoscUzycia = kolejnoscUzycia;
		this.kontoIdm = kontoIdm;
		this.kontobankoweId = kontobankoweId;
		this.nazwa = nazwa;
		this.numerKonta = numerKonta;
		this.symbolwaluty = symbolwaluty;
		this.typkursId = typkursId;
		this.typkursIdn = typkursIdn;
		this.walutaId = walutaId;
		this.zrodkurswalId = zrodkurswalId;
		this.zrodkurswalIdn = zrodkurswalIdn;
		this.zrodkurswalNazwa = zrodkurswalNazwa;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isAvailable() {
		return this.available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public long getBankId() {
		return this.bankId;
	}

	public void setBankId(long bankId) {
		this.bankId = bankId;
	}

	public String getBankIdn() {
		return this.bankIdn;
	}

	public void setBankIdn(String bankIdn) {
		this.bankIdn = bankIdn;
	}

	public String getBankNazwa() {
		return this.bankNazwa;
	}

	public void setBankNazwa(String bankNazwa) {
		this.bankNazwa = bankNazwa;
	}

	public Integer getCzyIban() {
		return this.czyIban;
	}

	public void setCzyIban(Integer czyIban) {
		this.czyIban = czyIban;
	}

	public long getDostawcaId() {
		return this.dostawcaId;
	}

	public void setDostawcaId(long dostawcaId) {
		this.dostawcaId = dostawcaId;
	}

	public String getDostawcaIdn() {
		return this.dostawcaIdn;
	}

	public void setDostawcaIdn(String dostawcaIdn) {
		this.dostawcaIdn = dostawcaIdn;
	}

	public Integer getKolejnoscUzycia() {
		return this.kolejnoscUzycia;
	}

	public void setKolejnoscUzycia(Integer kolejnoscUzycia) {
		this.kolejnoscUzycia = kolejnoscUzycia;
	}

	public String getKontoIdm() {
		return this.kontoIdm;
	}

	public void setKontoIdm(String kontoIdm) {
		this.kontoIdm = kontoIdm;
	}

	public long getKontobankoweId() {
		return this.kontobankoweId;
	}

	public void setKontobankoweId(long kontobankoweId) {
		this.kontobankoweId = kontobankoweId;
	}

	public String getNazwa() {
		return this.nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getNumerKonta() {
		return this.numerKonta;
	}

	public void setNumerKonta(String numerKonta) {
		this.numerKonta = numerKonta;
	}

	public String getSymbolwaluty() {
		return this.symbolwaluty;
	}

	public void setSymbolwaluty(String symbolwaluty) {
		this.symbolwaluty = symbolwaluty;
	}

	public long getTypkursId() {
		return this.typkursId;
	}

	public void setTypkursId(long typkursId) {
		this.typkursId = typkursId;
	}

	public String getTypkursIdn() {
		return this.typkursIdn;
	}

	public void setTypkursIdn(String typkursIdn) {
		this.typkursIdn = typkursIdn;
	}

	public long getWalutaId() {
		return this.walutaId;
	}

	public void setWalutaId(long walutaId) {
		this.walutaId = walutaId;
	}

	public long getZrodkurswalId() {
		return this.zrodkurswalId;
	}

	public void setZrodkurswalId(long zrodkurswalId) {
		this.zrodkurswalId = zrodkurswalId;
	}

	public String getZrodkurswalIdn() {
		return this.zrodkurswalIdn;
	}

	public void setZrodkurswalIdn(String zrodkurswalIdn) {
		this.zrodkurswalIdn = zrodkurswalIdn;
	}

	public String getZrodkurswalNazwa() {
		return this.zrodkurswalNazwa;
	}

	public void setZrodkurswalNazwa(String zrodkurswalNazwa) {
		this.zrodkurswalNazwa = zrodkurswalNazwa;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ContractorAccount))
			return false;
		ContractorAccount castOther = (ContractorAccount) other;

		return (this.getId() == castOther.getId())
				&& (this.isAvailable() == castOther.isAvailable())
				&& (this.getBankId() == castOther.getBankId())
				&& ((this.getBankIdn() == castOther.getBankIdn()) || (this.getBankIdn() != null && castOther.getBankIdn() != null && this.getBankIdn().equals(
						castOther.getBankIdn())))
				&& ((this.getBankNazwa() == castOther.getBankNazwa()) || (this.getBankNazwa() != null && castOther.getBankNazwa() != null && this
						.getBankNazwa().equals(castOther.getBankNazwa())))
				&& ((this.getCzyIban() == castOther.getCzyIban()) || (this.getCzyIban() != null && castOther.getCzyIban() != null && this.getCzyIban().equals(
						castOther.getCzyIban())))
				&& (this.getDostawcaId() == castOther.getDostawcaId())
				&& ((this.getDostawcaIdn() == castOther.getDostawcaIdn()) || (this.getDostawcaIdn() != null && castOther.getDostawcaIdn() != null && this
						.getDostawcaIdn().equals(castOther.getDostawcaIdn())))
				&& ((this.getKolejnoscUzycia() == castOther.getKolejnoscUzycia()) || (this.getKolejnoscUzycia() != null
						&& castOther.getKolejnoscUzycia() != null && this.getKolejnoscUzycia().equals(castOther.getKolejnoscUzycia())))
				&& ((this.getKontoIdm() == castOther.getKontoIdm()) || (this.getKontoIdm() != null && castOther.getKontoIdm() != null && this.getKontoIdm()
						.equals(castOther.getKontoIdm())))
				&& (this.getKontobankoweId() == castOther.getKontobankoweId())
				&& ((this.getNazwa() == castOther.getNazwa()) || (this.getNazwa() != null && castOther.getNazwa() != null && this.getNazwa().equals(
						castOther.getNazwa())))
				&& ((this.getNumerKonta() == castOther.getNumerKonta()) || (this.getNumerKonta() != null && castOther.getNumerKonta() != null && this
						.getNumerKonta().equals(castOther.getNumerKonta())))
				&& ((this.getSymbolwaluty() == castOther.getSymbolwaluty()) || (this.getSymbolwaluty() != null && castOther.getSymbolwaluty() != null && this
						.getSymbolwaluty().equals(castOther.getSymbolwaluty())))
				&& (this.getTypkursId() == castOther.getTypkursId())
				&& ((this.getTypkursIdn() == castOther.getTypkursIdn()) || (this.getTypkursIdn() != null && castOther.getTypkursIdn() != null && this
						.getTypkursIdn().equals(castOther.getTypkursIdn())))
				&& (this.getWalutaId() == castOther.getWalutaId())
				&& (this.getZrodkurswalId() == castOther.getZrodkurswalId())
				&& ((this.getZrodkurswalIdn() == castOther.getZrodkurswalIdn()) || (this.getZrodkurswalIdn() != null && castOther.getZrodkurswalIdn() != null && this
						.getZrodkurswalIdn().equals(castOther.getZrodkurswalIdn())))
				&& ((this.getZrodkurswalNazwa() == castOther.getZrodkurswalNazwa()) || (this.getZrodkurswalNazwa() != null
						&& castOther.getZrodkurswalNazwa() != null && this.getZrodkurswalNazwa().equals(castOther.getZrodkurswalNazwa())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getId();
		result = 37 * result + (this.isAvailable() ? 1 : 0);
		result = 37 * result + (int) this.getBankId();
		result = 37 * result + (getBankIdn() == null ? 0 : this.getBankIdn().hashCode());
		result = 37 * result + (getBankNazwa() == null ? 0 : this.getBankNazwa().hashCode());
		result = 37 * result + (getCzyIban() == null ? 0 : this.getCzyIban().hashCode());
		result = 37 * result + (int) this.getDostawcaId();
		result = 37 * result + (getDostawcaIdn() == null ? 0 : this.getDostawcaIdn().hashCode());
		result = 37 * result + (getKolejnoscUzycia() == null ? 0 : this.getKolejnoscUzycia().hashCode());
		result = 37 * result + (getKontoIdm() == null ? 0 : this.getKontoIdm().hashCode());
		result = 37 * result + (int) this.getKontobankoweId();
		result = 37 * result + (getNazwa() == null ? 0 : this.getNazwa().hashCode());
		result = 37 * result + (getNumerKonta() == null ? 0 : this.getNumerKonta().hashCode());
		result = 37 * result + (getSymbolwaluty() == null ? 0 : this.getSymbolwaluty().hashCode());
		result = 37 * result + (int) this.getTypkursId();
		result = 37 * result + (getTypkursIdn() == null ? 0 : this.getTypkursIdn().hashCode());
		result = 37 * result + (int) this.getWalutaId();
		result = 37 * result + (int) this.getZrodkurswalId();
		result = 37 * result + (getZrodkurswalIdn() == null ? 0 : this.getZrodkurswalIdn().hashCode());
		result = 37 * result + (getZrodkurswalNazwa() == null ? 0 : this.getZrodkurswalNazwa().hashCode());
		return result;
	}
	
	public void setAllFieldsFromServiceObject (SupplierBankAccount fromService) {
		this.setBankId(fromService.getBankId());
		this.setBankIdn(fromService.getBankIdn());
		this.setBankNazwa(fromService.getBankNazwa());
		this.setCzyIban(fromService.getCzyIban());
		this.setDostawcaId(fromService.getDostawcaId());
		this.setDostawcaIdn(fromService.getDostawcaIdn());
		this.setKolejnoscUzycia(fromService.getKolejnoscUzycia());
		this.setKontoIdm(fromService.getKontoIdm());
		this.setKontobankoweId(fromService.getKontobankoweId());
		this.setNazwa(fromService.getNazwa());
		this.setNumerKonta(fromService.getNumerKonta());
		this.setSymbolwaluty(fromService.getSymbolwaluty());
		this.setTypkursId(fromService.getTypkursId());
		this.setTypkursIdn(fromService.getTypkursIdn());
		this.setWalutaId(fromService.getWalutaId());
		this.setZrodkurswalId(fromService.getZrodkurswalId());
		this.setZrodkurswalIdn(fromService.getZrodkurswalIdn());
		this.setZrodkurswalNazwa(fromService.getZrodkurswalNazwa());
	}

	public void save() throws EdmException {
		try {
			DSApi.context().session().save(this);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}	
	
}
