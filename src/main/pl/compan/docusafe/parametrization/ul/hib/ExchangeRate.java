package pl.compan.docusafe.parametrization.ul.hib;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

import pl.compan.docusafe.ws.ul.DictionaryServicesExchangeRateTablesStub.ExchangeRateTable;

import com.google.common.collect.Lists;

public class ExchangeRate implements java.io.Serializable {

	private long id;
	private Long bankId;
	private Integer czymod;
	private Date dataczasoglosz;
	private Date datdo;
	private Date datod;
	private BigDecimal kurs;
	private Integer nominal;
	private Long tabkursId;
	private String tabkursIdn;
	private BigDecimal tolerancja;
	private Long typkursId;
	private String typkursIdn;
	private String walutaBazNazwa;
	private String walutaBazNazwgrosz;
	private Integer walutaBazPrecyzja;
	private Integer walutaBazPrecyzjaCeny;
	private String walutaBazSymbolwaluty;
	private Long walutaBazWalutaId;
	private Long walutaNomId;
	private String walutaNomNazwa;
	private String walutaNomNazwgrosz;
	private Integer walutaNomPrecyzja;
	private Integer walutaNomPrecyzjaCeny;
	private String walutaNomSymbolwaluty;
	private Long zrodkurswalId;
	private String zrodkurswalIdn;

	public ExchangeRate() {
	}

	public ExchangeRate(long id) {
		this.id = id;
	}

	public ExchangeRate(long id, Long bankId, Integer czymod, Date dataczasoglosz, Date datdo, Date datod, BigDecimal kurs, Integer nominal,
			Long tabkursId, String tabkursIdn, BigDecimal tolerancja, Long typkursId, String typkursIdn, String walutaBazNazwa, String walutaBazNazwgrosz,
			Integer walutaBazPrecyzja, Integer walutaBazPrecyzjaCeny, String walutaBazSymbolwaluty, Long walutaBazWalutaId, Long walutaNomId,
			String walutaNomNazwa, String walutaNomNazwgrosz, Integer walutaNomPrecyzja, Integer walutaNomPrecyzjaCeny, String walutaNomSymbolwaluty,
			Long zrodkurswalId, String zrodkurswalIdn) {
		this.id = id;
		this.bankId = bankId;
		this.czymod = czymod;
		this.dataczasoglosz = dataczasoglosz;
		this.datdo = datdo;
		this.datod = datod;
		this.kurs = kurs;
		this.nominal = nominal;
		this.tabkursId = tabkursId;
		this.tabkursIdn = tabkursIdn;
		this.tolerancja = tolerancja;
		this.typkursId = typkursId;
		this.typkursIdn = typkursIdn;
		this.walutaBazNazwa = walutaBazNazwa;
		this.walutaBazNazwgrosz = walutaBazNazwgrosz;
		this.walutaBazPrecyzja = walutaBazPrecyzja;
		this.walutaBazPrecyzjaCeny = walutaBazPrecyzjaCeny;
		this.walutaBazSymbolwaluty = walutaBazSymbolwaluty;
		this.walutaBazWalutaId = walutaBazWalutaId;
		this.walutaNomId = walutaNomId;
		this.walutaNomNazwa = walutaNomNazwa;
		this.walutaNomNazwgrosz = walutaNomNazwgrosz;
		this.walutaNomPrecyzja = walutaNomPrecyzja;
		this.walutaNomPrecyzjaCeny = walutaNomPrecyzjaCeny;
		this.walutaNomSymbolwaluty = walutaNomSymbolwaluty;
		this.zrodkurswalId = zrodkurswalId;
		this.zrodkurswalIdn = zrodkurswalIdn;
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getBankId() {
		return this.bankId;
	}

	public void setBankId(Long bankId) {
		this.bankId = bankId;
	}

	public Integer getCzymod() {
		return this.czymod;
	}

	public void setCzymod(Integer czymod) {
		this.czymod = czymod;
	}

	public Date getDataczasoglosz() {
		return this.dataczasoglosz;
	}

	public void setDataczasoglosz(Date dataczasoglosz) {
		this.dataczasoglosz = dataczasoglosz;
	}

	public Date getDatdo() {
		return this.datdo;
	}

	public void setDatdo(Date datdo) {
		this.datdo = datdo;
	}

	public Date getDatod() {
		return this.datod;
	}

	public void setDatod(Date datod) {
		this.datod = datod;
	}

	public BigDecimal getKurs() {
		return this.kurs;
	}

	public void setKurs(BigDecimal kurs) {
		this.kurs = kurs;
	}

	public Integer getNominal() {
		return this.nominal;
	}

	public void setNominal(Integer nominal) {
		this.nominal = nominal;
	}

	public Long getTabkursId() {
		return this.tabkursId;
	}

	public void setTabkursId(Long tabkursId) {
		this.tabkursId = tabkursId;
	}

	public String getTabkursIdn() {
		return this.tabkursIdn;
	}

	public void setTabkursIdn(String tabkursIdn) {
		this.tabkursIdn = tabkursIdn;
	}

	public BigDecimal getTolerancja() {
		return this.tolerancja;
	}

	public void setTolerancja(BigDecimal tolerancja) {
		this.tolerancja = tolerancja;
	}

	public Long getTypkursId() {
		return this.typkursId;
	}

	public void setTypkursId(Long typkursId) {
		this.typkursId = typkursId;
	}

	public String getTypkursIdn() {
		return this.typkursIdn;
	}

	public void setTypkursIdn(String typkursIdn) {
		this.typkursIdn = typkursIdn;
	}

	public String getWalutaBazNazwa() {
		return this.walutaBazNazwa;
	}

	public void setWalutaBazNazwa(String walutaBazNazwa) {
		this.walutaBazNazwa = walutaBazNazwa;
	}

	public String getWalutaBazNazwgrosz() {
		return this.walutaBazNazwgrosz;
	}

	public void setWalutaBazNazwgrosz(String walutaBazNazwgrosz) {
		this.walutaBazNazwgrosz = walutaBazNazwgrosz;
	}

	public Integer getWalutaBazPrecyzja() {
		return this.walutaBazPrecyzja;
	}

	public void setWalutaBazPrecyzja(Integer walutaBazPrecyzja) {
		this.walutaBazPrecyzja = walutaBazPrecyzja;
	}

	public Integer getWalutaBazPrecyzjaCeny() {
		return this.walutaBazPrecyzjaCeny;
	}

	public void setWalutaBazPrecyzjaCeny(Integer walutaBazPrecyzjaCeny) {
		this.walutaBazPrecyzjaCeny = walutaBazPrecyzjaCeny;
	}

	public String getWalutaBazSymbolwaluty() {
		return this.walutaBazSymbolwaluty;
	}

	public void setWalutaBazSymbolwaluty(String walutaBazSymbolwaluty) {
		this.walutaBazSymbolwaluty = walutaBazSymbolwaluty;
	}

	public Long getWalutaBazWalutaId() {
		return this.walutaBazWalutaId;
	}

	public void setWalutaBazWalutaId(Long walutaBazWalutaId) {
		this.walutaBazWalutaId = walutaBazWalutaId;
	}

	public Long getWalutaNomId() {
		return this.walutaNomId;
	}

	public void setWalutaNomId(Long walutaNomId) {
		this.walutaNomId = walutaNomId;
	}

	public String getWalutaNomNazwa() {
		return this.walutaNomNazwa;
	}

	public void setWalutaNomNazwa(String walutaNomNazwa) {
		this.walutaNomNazwa = walutaNomNazwa;
	}

	public String getWalutaNomNazwgrosz() {
		return this.walutaNomNazwgrosz;
	}

	public void setWalutaNomNazwgrosz(String walutaNomNazwgrosz) {
		this.walutaNomNazwgrosz = walutaNomNazwgrosz;
	}

	public Integer getWalutaNomPrecyzja() {
		return this.walutaNomPrecyzja;
	}

	public void setWalutaNomPrecyzja(Integer walutaNomPrecyzja) {
		this.walutaNomPrecyzja = walutaNomPrecyzja;
	}

	public Integer getWalutaNomPrecyzjaCeny() {
		return this.walutaNomPrecyzjaCeny;
	}

	public void setWalutaNomPrecyzjaCeny(Integer walutaNomPrecyzjaCeny) {
		this.walutaNomPrecyzjaCeny = walutaNomPrecyzjaCeny;
	}

	public String getWalutaNomSymbolwaluty() {
		return this.walutaNomSymbolwaluty;
	}

	public void setWalutaNomSymbolwaluty(String walutaNomSymbolwaluty) {
		this.walutaNomSymbolwaluty = walutaNomSymbolwaluty;
	}

	public Long getZrodkurswalId() {
		return this.zrodkurswalId;
	}

	public void setZrodkurswalId(Long zrodkurswalId) {
		this.zrodkurswalId = zrodkurswalId;
	}

	public String getZrodkurswalIdn() {
		return this.zrodkurswalIdn;
	}

	public void setZrodkurswalIdn(String zrodkurswalIdn) {
		this.zrodkurswalIdn = zrodkurswalIdn;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ExchangeRate))
			return false;
		ExchangeRate castOther = (ExchangeRate) other;

		return (this.getId() == castOther.getId())
				&& ((this.getBankId() == castOther.getBankId()) || (this.getBankId() != null && castOther.getBankId() != null && this.getBankId().equals(
						castOther.getBankId())))
				&& ((this.getCzymod() == castOther.getCzymod()) || (this.getCzymod() != null && castOther.getCzymod() != null && this.getCzymod().equals(
						castOther.getCzymod())))
				&& ((this.getDataczasoglosz() == castOther.getDataczasoglosz()) || (this.getDataczasoglosz() != null && castOther.getDataczasoglosz() != null && this
						.getDataczasoglosz().equals(castOther.getDataczasoglosz())))
				&& ((this.getDatdo() == castOther.getDatdo()) || (this.getDatdo() != null && castOther.getDatdo() != null && this.getDatdo().equals(
						castOther.getDatdo())))
				&& ((this.getDatod() == castOther.getDatod()) || (this.getDatod() != null && castOther.getDatod() != null && this.getDatod().equals(
						castOther.getDatod())))
				&& ((this.getKurs() == castOther.getKurs()) || (this.getKurs() != null && castOther.getKurs() != null && this.getKurs().equals(
						castOther.getKurs())))
				&& ((this.getNominal() == castOther.getNominal()) || (this.getNominal() != null && castOther.getNominal() != null && this.getNominal().equals(
						castOther.getNominal())))
				&& ((this.getTabkursId() == castOther.getTabkursId()) || (this.getTabkursId() != null && castOther.getTabkursId() != null && this
						.getTabkursId().equals(castOther.getTabkursId())))
				&& ((this.getTabkursIdn() == castOther.getTabkursIdn()) || (this.getTabkursIdn() != null && castOther.getTabkursIdn() != null && this
						.getTabkursIdn().equals(castOther.getTabkursIdn())))
				&& ((this.getTolerancja() == castOther.getTolerancja()) || (this.getTolerancja() != null && castOther.getTolerancja() != null && this
						.getTolerancja().equals(castOther.getTolerancja())))
				&& ((this.getTypkursId() == castOther.getTypkursId()) || (this.getTypkursId() != null && castOther.getTypkursId() != null && this
						.getTypkursId().equals(castOther.getTypkursId())))
				&& ((this.getTypkursIdn() == castOther.getTypkursIdn()) || (this.getTypkursIdn() != null && castOther.getTypkursIdn() != null && this
						.getTypkursIdn().equals(castOther.getTypkursIdn())))
				&& ((this.getWalutaBazNazwa() == castOther.getWalutaBazNazwa()) || (this.getWalutaBazNazwa() != null && castOther.getWalutaBazNazwa() != null && this
						.getWalutaBazNazwa().equals(castOther.getWalutaBazNazwa())))
				&& ((this.getWalutaBazNazwgrosz() == castOther.getWalutaBazNazwgrosz()) || (this.getWalutaBazNazwgrosz() != null
						&& castOther.getWalutaBazNazwgrosz() != null && this.getWalutaBazNazwgrosz().equals(castOther.getWalutaBazNazwgrosz())))
				&& ((this.getWalutaBazPrecyzja() == castOther.getWalutaBazPrecyzja()) || (this.getWalutaBazPrecyzja() != null
						&& castOther.getWalutaBazPrecyzja() != null && this.getWalutaBazPrecyzja().equals(castOther.getWalutaBazPrecyzja())))
				&& ((this.getWalutaBazPrecyzjaCeny() == castOther.getWalutaBazPrecyzjaCeny()) || (this.getWalutaBazPrecyzjaCeny() != null
						&& castOther.getWalutaBazPrecyzjaCeny() != null && this.getWalutaBazPrecyzjaCeny().equals(castOther.getWalutaBazPrecyzjaCeny())))
				&& ((this.getWalutaBazSymbolwaluty() == castOther.getWalutaBazSymbolwaluty()) || (this.getWalutaBazSymbolwaluty() != null
						&& castOther.getWalutaBazSymbolwaluty() != null && this.getWalutaBazSymbolwaluty().equals(castOther.getWalutaBazSymbolwaluty())))
				&& ((this.getWalutaBazWalutaId() == castOther.getWalutaBazWalutaId()) || (this.getWalutaBazWalutaId() != null
						&& castOther.getWalutaBazWalutaId() != null && this.getWalutaBazWalutaId().equals(castOther.getWalutaBazWalutaId())))
				&& ((this.getWalutaNomId() == castOther.getWalutaNomId()) || (this.getWalutaNomId() != null && castOther.getWalutaNomId() != null && this
						.getWalutaNomId().equals(castOther.getWalutaNomId())))
				&& ((this.getWalutaNomNazwa() == castOther.getWalutaNomNazwa()) || (this.getWalutaNomNazwa() != null && castOther.getWalutaNomNazwa() != null && this
						.getWalutaNomNazwa().equals(castOther.getWalutaNomNazwa())))
				&& ((this.getWalutaNomNazwgrosz() == castOther.getWalutaNomNazwgrosz()) || (this.getWalutaNomNazwgrosz() != null
						&& castOther.getWalutaNomNazwgrosz() != null && this.getWalutaNomNazwgrosz().equals(castOther.getWalutaNomNazwgrosz())))
				&& ((this.getWalutaNomPrecyzja() == castOther.getWalutaNomPrecyzja()) || (this.getWalutaNomPrecyzja() != null
						&& castOther.getWalutaNomPrecyzja() != null && this.getWalutaNomPrecyzja().equals(castOther.getWalutaNomPrecyzja())))
				&& ((this.getWalutaNomPrecyzjaCeny() == castOther.getWalutaNomPrecyzjaCeny()) || (this.getWalutaNomPrecyzjaCeny() != null
						&& castOther.getWalutaNomPrecyzjaCeny() != null && this.getWalutaNomPrecyzjaCeny().equals(castOther.getWalutaNomPrecyzjaCeny())))
				&& ((this.getWalutaNomSymbolwaluty() == castOther.getWalutaNomSymbolwaluty()) || (this.getWalutaNomSymbolwaluty() != null
						&& castOther.getWalutaNomSymbolwaluty() != null && this.getWalutaNomSymbolwaluty().equals(castOther.getWalutaNomSymbolwaluty())))
				&& ((this.getZrodkurswalId() == castOther.getZrodkurswalId()) || (this.getZrodkurswalId() != null && castOther.getZrodkurswalId() != null && this
						.getZrodkurswalId().equals(castOther.getZrodkurswalId())))
				&& ((this.getZrodkurswalIdn() == castOther.getZrodkurswalIdn()) || (this.getZrodkurswalIdn() != null && castOther.getZrodkurswalIdn() != null && this
						.getZrodkurswalIdn().equals(castOther.getZrodkurswalIdn())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (int) this.getId();
		result = 37 * result + (getBankId() == null ? 0 : this.getBankId().hashCode());
		result = 37 * result + (getCzymod() == null ? 0 : this.getCzymod().hashCode());
		result = 37 * result + (getDataczasoglosz() == null ? 0 : this.getDataczasoglosz().hashCode());
		result = 37 * result + (getDatdo() == null ? 0 : this.getDatdo().hashCode());
		result = 37 * result + (getDatod() == null ? 0 : this.getDatod().hashCode());
		result = 37 * result + (getKurs() == null ? 0 : this.getKurs().hashCode());
		result = 37 * result + (getNominal() == null ? 0 : this.getNominal().hashCode());
		result = 37 * result + (getTabkursId() == null ? 0 : this.getTabkursId().hashCode());
		result = 37 * result + (getTabkursIdn() == null ? 0 : this.getTabkursIdn().hashCode());
		result = 37 * result + (getTolerancja() == null ? 0 : this.getTolerancja().hashCode());
		result = 37 * result + (getTypkursId() == null ? 0 : this.getTypkursId().hashCode());
		result = 37 * result + (getTypkursIdn() == null ? 0 : this.getTypkursIdn().hashCode());
		result = 37 * result + (getWalutaBazNazwa() == null ? 0 : this.getWalutaBazNazwa().hashCode());
		result = 37 * result + (getWalutaBazNazwgrosz() == null ? 0 : this.getWalutaBazNazwgrosz().hashCode());
		result = 37 * result + (getWalutaBazPrecyzja() == null ? 0 : this.getWalutaBazPrecyzja().hashCode());
		result = 37 * result + (getWalutaBazPrecyzjaCeny() == null ? 0 : this.getWalutaBazPrecyzjaCeny().hashCode());
		result = 37 * result + (getWalutaBazSymbolwaluty() == null ? 0 : this.getWalutaBazSymbolwaluty().hashCode());
		result = 37 * result + (getWalutaBazWalutaId() == null ? 0 : this.getWalutaBazWalutaId().hashCode());
		result = 37 * result + (getWalutaNomId() == null ? 0 : this.getWalutaNomId().hashCode());
		result = 37 * result + (getWalutaNomNazwa() == null ? 0 : this.getWalutaNomNazwa().hashCode());
		result = 37 * result + (getWalutaNomNazwgrosz() == null ? 0 : this.getWalutaNomNazwgrosz().hashCode());
		result = 37 * result + (getWalutaNomPrecyzja() == null ? 0 : this.getWalutaNomPrecyzja().hashCode());
		result = 37 * result + (getWalutaNomPrecyzjaCeny() == null ? 0 : this.getWalutaNomPrecyzjaCeny().hashCode());
		result = 37 * result + (getWalutaNomSymbolwaluty() == null ? 0 : this.getWalutaNomSymbolwaluty().hashCode());
		result = 37 * result + (getZrodkurswalId() == null ? 0 : this.getZrodkurswalId().hashCode());
		result = 37 * result + (getZrodkurswalIdn() == null ? 0 : this.getZrodkurswalIdn().hashCode());
		return result;
	}

	public static List<ExchangeRate> find(long walutaNomId, Date from, Date to) throws EdmException {
		List<ExchangeRate> list = Lists.newArrayList();
		try {
			Criteria criteria = DSApi.context().session().createCriteria(ExchangeRate.class);
			
			if (walutaNomId != 0l) {
				criteria.add(Restrictions.eq("walutaNomId", walutaNomId));
			} else {
				return list;
			}
			
			if (from == null && to == null) {
				return list;
			}
			
			if (from != null) {
				criteria.add(Restrictions.eq("datod", from));
			} else {
				criteria.add(Restrictions.isNull("datod"));
			}
			if (to == null) {
				criteria.add(Restrictions.isNull("datdo"));
			} else {
				criteria.add(Restrictions.or(Restrictions.eq("datdo", to), Restrictions.isNull("datdo")));
			}
			
			list = criteria.list();
			
			return list;
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public void setAllFieldsFromServiceObject (ExchangeRateTable fromService) {
		this.setBankId(fromService.getBankId());
		this.setCzymod(fromService.getCzymod());
		this.setDataczasoglosz(fromService.getDataczasoglosz());
		this.setDatdo(fromService.getDatdo());
		this.setDatod(fromService.getDatod());
		this.setKurs(fromService.getKurs());
		this.setNominal(fromService.getNominal());
		this.setTabkursId(fromService.getTabkursId());
		this.setTabkursIdn(fromService.getTabkursIdn());
		this.setTolerancja(fromService.getTolerancja());
		this.setTypkursId(fromService.getTypkursId());
		this.setTypkursIdn(fromService.getTypkursIdn());
		this.setWalutaBazNazwa(fromService.getWalutaBazNazwa());
		this.setWalutaBazNazwgrosz(fromService.getWalutaBazNazwgrosz());
		this.setWalutaBazPrecyzja(fromService.getWalutaBazPrecyzja());
		this.setWalutaBazPrecyzjaCeny(fromService.getWalutaBazPrecyzjaCeny());
		this.setWalutaBazSymbolwaluty(fromService.getWalutaBazSymbolwaluty());
		this.setWalutaBazWalutaId(fromService.getWalutaBazWalutaId());
		this.setWalutaNomId(fromService.getWalutaNomId());
		this.setWalutaNomNazwa(fromService.getWalutaNomNazwa());
		this.setWalutaNomNazwgrosz(fromService.getWalutaNomNazwgrosz());
		this.setWalutaNomPrecyzja(fromService.getWalutaNomPrecyzja());
		this.setWalutaNomPrecyzjaCeny(fromService.getWalutaNomPrecyzjaCeny());
		this.setWalutaNomSymbolwaluty(fromService.getWalutaNomSymbolwaluty());
		this.setZrodkurswalId(fromService.getZrodkurswalId());
		this.setZrodkurswalIdn(fromService.getZrodkurswalIdn());
	}

	public void save() throws EdmException {
		try {
			DSApi.context().session().save(this);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}	
	
}
