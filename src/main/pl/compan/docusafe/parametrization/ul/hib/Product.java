package pl.compan.docusafe.parametrization.ul.hib;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

@SuppressWarnings("serial")
public class Product implements java.io.Serializable {

	private long id;
	private Boolean available;
	private String produktGrupa;
	private String produktIdn;
	private String produktNazwa;
	private Long wytworId;

	public Product() {
	}

	public Product(long id) {
		this.id = id;
	}

	public Product(long id, Boolean available, String produktGrupa, String produktIdn, String produktNazwa, Long wytworId) {
		this.id = id;
		this.available = available;
		this.produktGrupa = produktGrupa;
		this.produktIdn = produktIdn;
		this.produktNazwa = produktNazwa;
		this.wytworId = wytworId;
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Boolean getAvailable() {
		return this.available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}

	public String getProduktGrupa() {
		return this.produktGrupa;
	}

	public void setProduktGrupa(String produktGrupa) {
		this.produktGrupa = produktGrupa;
	}

	public String getProduktIdn() {
		return this.produktIdn;
	}

	public void setProduktIdn(String produktIdn) {
		this.produktIdn = produktIdn;
	}

	public String getProduktNazwa() {
		return this.produktNazwa;
	}

	public void setProduktNazwa(String produktNazwa) {
		this.produktNazwa = produktNazwa;
	}

	public Long getWytworId() {
		return this.wytworId;
	}

	public void setWytworId(Long wytworId) {
		this.wytworId = wytworId;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof Product))
			return false;
		Product castOther = (Product) other;

		return (this.getId() == castOther.getId())
				&& ((this.getAvailable() == castOther.getAvailable()) || (this.getAvailable() != null && castOther.getAvailable() != null && this
						.getAvailable().equals(castOther.getAvailable())))
				&& ((this.getProduktGrupa() == castOther.getProduktGrupa()) || (this.getProduktGrupa() != null && castOther.getProduktGrupa() != null && this
						.getProduktGrupa().equals(castOther.getProduktGrupa())))
				&& ((this.getProduktIdn() == castOther.getProduktIdn()) || (this.getProduktIdn() != null && castOther.getProduktIdn() != null && this
						.getProduktIdn().equals(castOther.getProduktIdn())))
				&& ((this.getProduktNazwa() == castOther.getProduktNazwa()) || (this.getProduktNazwa() != null && castOther.getProduktNazwa() != null && this
						.getProduktNazwa().equals(castOther.getProduktNazwa())))
				&& ((this.getWytworId() == castOther.getWytworId()) || (this.getWytworId() != null && castOther.getWytworId() != null && this.getWytworId()
						.equals(castOther.getWytworId())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (int) this.getId();
		result = 37 * result + (getAvailable() == null ? 0 : this.getAvailable().hashCode());
		result = 37 * result + (getProduktGrupa() == null ? 0 : this.getProduktGrupa().hashCode());
		result = 37 * result + (getProduktIdn() == null ? 0 : this.getProduktIdn().hashCode());
		result = 37 * result + (getProduktNazwa() == null ? 0 : this.getProduktNazwa().hashCode());
		result = 37 * result + (getWytworId() == null ? 0 : this.getWytworId().hashCode());
		return result;
	}
	
	public void setAllFieldsFromServiceObject(pl.compan.docusafe.ws.ul.DictionaryServicesProductStub.Product item) {
		this.setProduktGrupa(item.getProduktGrupa());
		this.setProduktIdn(item.getProduktIdn());
		this.setProduktNazwa(item.getProduktNazwa());
		this.setWytworId(item.getWytworId());
	}
	
	public void save() throws EdmException {
		try {
			DSApi.context().session().save(this);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
}
