package pl.compan.docusafe.parametrization.ul.hib;

import org.hibernate.HibernateException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.ws.ul.DictionaryServicesSupplierStub;

import javax.persistence.*;

@Entity
@Table(name = UlContractor.TABLE_NAME)
public class UlContractor implements java.io.Serializable {

    public static final String TABLE_NAME = "dsg_ul_contractor";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "dsg_ul_contractor_seq")
    @SequenceGenerator(name = "dsg_ul_contractor_seq", sequenceName = "dsg_ul_contractor_seq")
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "available", nullable = false)
    private boolean available;
    @Column(name = "dostawca_id")
    private long dostawcaId;
    @Column(name = "dostawca_idn", length = 50)
    private String dostawcaIdn;
    @Column(name = "identyfikator_num")
    private long identyfikatorNum;
    @Column(name = "nazwa", length = 300)
    private String nazwa;
    @Column(name = "nip", length = 50)
    private String nip;
    @Column(name = "miasto", length = 50)
    private String miasto;
    @Column(name = "kod_pocztowy", length = 50)
    private String kodPocztowy;
    @Column(name = "ulica", length = 50)
    private String ulica;
    @Column(name = "nr_domu", length = 50)
    private String nrDomu;
    @Column(name = "nr_mieszkania", length = 50)
    private String nrMieszkania;

    public UlContractor(long dostawcaId) {
        this.dostawcaId = dostawcaId;
        this.available = true;
    }

    public static UlContractor find(Integer id) throws EdmException {
        return Finder.find(UlContractor.class, id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public long getDostawcaId() {
        return dostawcaId;
    }

    public void setDostawcaId(long dostawcaId) {
        this.dostawcaId = dostawcaId;
    }

    public String getDostawcaIdn() {
        return dostawcaIdn;
    }

    public void setDostawcaIdn(String dostawcaIdn) {
        this.dostawcaIdn = dostawcaIdn;
    }

    public long getIdentyfikatorNum() {
        return identyfikatorNum;
    }

    public void setIdentyfikatorNum(long identyfikatorNum) {
        this.identyfikatorNum = identyfikatorNum;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getMiasto() {
        return miasto;
    }

    public void setMiasto(String miasto) {
        this.miasto = miasto;
    }

    public String getKodPocztowy() {
        return kodPocztowy;
    }

    public void setKodPocztowy(String kodPocztowy) {
        this.kodPocztowy = kodPocztowy;
    }

    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public String getNrDomu() {
        return nrDomu;
    }

    public void setNrDomu(String nrDomu) {
        this.nrDomu = nrDomu;
    }

    public String getNrMieszkania() {
        return nrMieszkania;
    }

    public void setNrMieszkania(String nrMieszkania) {
        this.nrMieszkania = nrMieszkania;
    }

    public void setAllFieldsFromServiceObject(DictionaryServicesSupplierStub.Supplier fromService) {
        this.setIdentyfikatorNum(fromService.getIdentyfikatorNum());
        this.setDostawcaIdn(fromService.getDostawcaIdn());
        this.setNazwa(fromService.getDostawcaNazwa());
        this.setKodPocztowy(fromService.getDostawcaKodPoczt());
        this.setMiasto(fromService.getDostawcaMiasto());
        this.setNip(fromService.getDostawcaNip());
        this.setNrDomu(fromService.getDostawcaNrDomu());
        this.setNrMieszkania(fromService.getDostawcaNrMieszkania());
        this.setUlica(fromService.getDostawcaUlica());
    }

    public void save() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

}