package pl.compan.docusafe.parametrization.ul.hib;

import org.hibernate.HibernateException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.ws.ul.DictionaryServiceStub;

import javax.persistence.*;

/**
 * Created by kk on 23.02.14.
 */
@Entity
@Table(name = UlBudgetsFromUnits.TABLE_NAME)
public class UlBudgetsFromUnits {
    public static final String TABLE_NAME = "dsg_ul_services_budgets_from_units";
    public static final String VIEW_NAME = "dsg_ul_budgets_from_units";


    private Long id;
    private boolean available;
    private Long bdStrBudzetId;
    private String bdStrBudzetIdn;
    private Long budzetId;
    private String budzetIdm;
    private Long czyAgregat;
    private Long erpId;
    private String idm;
    private String nazwa;

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "available", nullable = false)
    public boolean getAvailable() {
        return this.available;
    }

    public UlBudgetsFromUnits setAvailable(boolean available) {
        this.available = available;
        return this;
    }

    @Column(name = "bd_str_budzet_id", scale = 0)
    public Long getBdStrBudzetId() {
        return this.bdStrBudzetId;
    }

    public void setBdStrBudzetId(Long bdStrBudzetId) {
        this.bdStrBudzetId = bdStrBudzetId;
    }

    @Column(name = "bd_str_budzet_idn", length = 256)
    public String getBdStrBudzetIdn() {
        return this.bdStrBudzetIdn;
    }

    public void setBdStrBudzetIdn(String bdStrBudzetIdn) {
        this.bdStrBudzetIdn = bdStrBudzetIdn;
    }

    @Column(name = "budzet_id", scale = 0)
    public Long getBudzetId() {
        return this.budzetId;
    }

    public void setBudzetId(Long budzetId) {
        this.budzetId = budzetId;
    }

    @Column(name = "budzet_idm", length = 256)
    public String getBudzetIdm() {
        return this.budzetIdm;
    }

    public void setBudzetIdm(String budzetIdm) {
        this.budzetIdm = budzetIdm;
    }

    @Column(name = "czy_agregat", scale = 0)
    public Long getCzyAgregat() {
        return this.czyAgregat;
    }

    public void setCzyAgregat(Long czyAgregat) {
        this.czyAgregat = czyAgregat;
    }

    @Column(name = "id_", scale = 0)
    public Long getErpId() {
        return this.erpId;
    }

    public void setErpId(Long erpId) {
        this.erpId = erpId;
    }

    @Column(name = "idm", length = 256)
    public String getIdm() {
        return this.idm;
    }

    public void setIdm(String idm) {
        this.idm = idm;
    }

    @Column(name = "nazwa", length = 256)
    public String getNazwa() {
        return this.nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }


    public UlBudgetsFromUnits setAllFieldsFromServiceObject(DictionaryServiceStub.UnitBudgetKo item) {
        this.setBdStrBudzetId((long)item.getBd_str_budzet_id());
        this.setBdStrBudzetIdn(item.getBd_str_budzet_idn());
        this.setBudzetId((long)item.getBudzet_id());
        this.setBudzetIdm(item.getBudzet_idm());
        this.setCzyAgregat((long)item.getCzy_agregat());
        this.setErpId((long)item.getId());
        this.setIdm(item.getIdm());
        this.setNazwa(item.getNazwa());

        return this;
    }

    public void save() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }
}
