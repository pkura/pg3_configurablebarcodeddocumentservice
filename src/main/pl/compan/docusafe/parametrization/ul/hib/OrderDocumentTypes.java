package pl.compan.docusafe.parametrization.ul.hib;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.ws.simple.ul.DictionaryServicesDocumentTypesStub.DeliveryRequestType;

import java.util.List;

public class OrderDocumentTypes implements java.io.Serializable {
	private Integer id;
	private boolean available;
	private Integer czyAktywny;
	private Integer czywal;
	private String nazwa;
	private String typdokIdn;
	private String typdokIds;

    private Long typZapdostId;


	public OrderDocumentTypes() {
	}

	public OrderDocumentTypes(Integer id, boolean available) {
		this.id = id;
		this.available = available;
	}

	public OrderDocumentTypes(Integer id, boolean available, Integer czyAktywny, Integer czywal, String nazwa,
                              String typdokIdn, String typdokIds, Long typZapdostId) {
		this.id = id;
		this.available = available;
		this.czyAktywny = czyAktywny;
		this.czywal = czywal;
		this.nazwa = nazwa;
		this.typdokIdn = typdokIdn;
		this.typdokIds = typdokIds;
		this.typZapdostId = typZapdostId;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isAvailable() {
		return this.available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public Integer getCzyAktywny() {
		return this.czyAktywny;
	}

	public void setCzyAktywny(Integer czyAktywny) {
		this.czyAktywny = czyAktywny;
	}

	public Integer getCzywal() {
		return this.czywal;
	}

	public void setCzywal(Integer czywal) {
		this.czywal = czywal;
	}

	public String getNazwa() {
		return this.nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getTypdokIdn() {
		return this.typdokIdn;
	}

	public void setTypdokIdn(String typdokIdn) {
		this.typdokIdn = typdokIdn;
	}

	public String getTypdokIds() {
		return this.typdokIds;
	}

	public void setTypdokIds(String typdokIds) {
		this.typdokIds = typdokIds;
	}

    public Long getTypZapdostId() {
        return typZapdostId;
    }

    public void setTypZapdostId(Long typZapdostId) {
        this.typZapdostId = typZapdostId;
    }
	public void save() throws EdmException {
		try {
			DSApi.context().session().save(this);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

	public void setAllFieldsFromServiceObject(DeliveryRequestType item) {
		this.typZapdostId = item.getTypZapdostId();
		this.czyAktywny = item.getCzyAktywny();
		this.czywal = item.getCzywal();
		this.nazwa = item.getNazwa();
		this.typdokIdn = item.getTypdokIdn();
		this.typdokIds = item.getTypdokIds();
	}	
	
	public static List<OrderDocumentTypes> findAvailable(boolean available) throws EdmException {
		try {
			Criteria criteria = DSApi.context().session().createCriteria(OrderDocumentTypes.class);
			criteria.add(Restrictions.eq("available", available));
			return criteria.list();
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
}
