package pl.compan.docusafe.parametrization.ul.hib;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

import java.util.List;
import java.util.Map;

public class DictionaryUtils {
    public static <E> List<E> findByGivenFieldValue(Class<E> clz, String field, Object value) throws EdmException {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(clz);
            if (StringUtils.isNotEmpty(field)) {
                criteria.add(Restrictions.eq(field, value));
            } else {
                return Lists.newArrayList();
            }
            return criteria.list();
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    /**
     * Metoda zwraca list� obiekt�w podanej klasy w parametrze clz. Wyszukiwanie po w�a�ciwo�ciach podanych w mapie fieldValues,
     * gdzie klucz to nazwa w�a�ciwo�ci, warto�� to warto�� w���ciwo�ci, po kt�rej szukamy
     *
     * @param clz
     * @param fieldValues
     * @param <E>
     * @return
     * @throws EdmException
     */
    public static <E> List<E> findByGivenFieldValues(Class<E> clz, Map<String, ? extends Object> fieldValues) throws EdmException {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(clz);
            for (String fieldName : fieldValues.keySet()) {
                if (StringUtils.isNotEmpty(fieldName)) {
                    criteria.add(Restrictions.eq(fieldName, fieldValues.get(fieldName)));
                }
            }
            return criteria.list();
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }
}
