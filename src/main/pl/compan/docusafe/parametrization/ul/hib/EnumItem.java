package pl.compan.docusafe.parametrization.ul.hib;

import java.sql.PreparedStatement;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.dbutils.DbUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;

public class EnumItem {

	public enum AcceptType {
		MANAGER_OF_UNIT, MANAGER_OF_DEPARTMENT, FINANCIAL_SECTION
	}
	
	Long id;
	Long erpId;
	String cn;
	String title;
	Integer centrum;
	Long refValue;
	Boolean available;

	private static EnumItem instance;

	public static synchronized EnumItem getInstance() {
		if (instance == null)
			instance = new EnumItem();
		return instance;
	}

	public String toString()
	{
		return "id="+id+" cn="+cn+" erpId="+erpId + " title="+title;
	}
	
	
	public EnumItem() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getErpId() {
		return erpId;
	}

	public void setErpId(Long erpId) {
		this.erpId = erpId;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getCentrum() {
		return centrum;
	}

	public void setCentrum(Integer centrum) {
		this.centrum = centrum;
	}

	public Long getRefValue() {
		return refValue;
	}

	public void setRefValue(Long refValue) {
		this.refValue = refValue;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}

	public EnumItem findErpId(Class<?> enumClass, Long erpId) throws EdmException {
		try {
			Criteria criteria = DSApi.context().session().createCriteria(enumClass);
			if (erpId != null) {
				criteria.add(Restrictions.eq("erpId", erpId));
			} else {
				throw new EntityNotFoundException(enumClass, erpId);
			}
			if (!criteria.list().isEmpty()) {
				return (EnumItem) criteria.list().get(0);
			} else {
				throw new EntityNotFoundException(enumClass, erpId);
			}
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public List<EnumItem> find(Class<?> enumClass, String cn, Long refValue) throws EdmException {
		List<EnumItem> list = Lists.newArrayList();
		try {
			Criteria criteria = DSApi.context().session().createCriteria(enumClass);
			if (cn == null && refValue == null)
				return list;

			if (cn != null) {
				criteria.add(Restrictions.eq("cn", cn));
			}
			if (refValue != null) {
				criteria.add(Restrictions.eq("refValue", refValue));
			}
			
			list = criteria.list();

			return list;
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public List<EnumItem> findByErpId(Class<?> enumClass, Collection<?> erpId, boolean isNotIn) throws EdmException {
		List<EnumItem> list = Lists.newArrayList();
		try {
			Criteria criteria = DSApi.context().session().createCriteria(enumClass);
			
			if (erpId != null) {
				if (isNotIn) {
					criteria.add(Restrictions.not(Restrictions.in("erpId", erpId)));
				} else {
					criteria.add(Restrictions.in("erpId", erpId));
				}
			} else {
				return list;
			}
			
			list = criteria.list();
			
			return list;
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public List<EnumItem> findByRefValue(Class<?> enumClass, Collection<?> refValue, boolean isNotIn) throws EdmException {
		List<EnumItem> list = Lists.newArrayList();
		try {
			Criteria criteria = DSApi.context().session().createCriteria(enumClass);
			
			if (refValue != null) {
				if (isNotIn) {
					criteria.add(Restrictions.not(Restrictions.in("refValue", refValue)));
				} else {
					criteria.add(Restrictions.in("refValue", refValue));
				}
			} else {
				return list;
			}
			
			list = criteria.list();
			
			return list;
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public void save() throws EdmException {
		try {
			DSApi.context().session().save(this);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
}
