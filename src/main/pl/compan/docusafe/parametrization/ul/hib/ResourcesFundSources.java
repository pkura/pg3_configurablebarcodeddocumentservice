package pl.compan.docusafe.parametrization.ul.hib;

import java.math.BigDecimal;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub;

public class ResourcesFundSources implements java.io.Serializable {

	private long id;
	private Long bpZasobZrodlaId;
	private Long identyfikatorNum;
	private BigDecimal pkoszt;
	private BigDecimal rkoszt;
	private BigDecimal wspProc;
	private Long zasobId;
	private Long zrodloId;
	private String zrodloIdn;
	private String zrodloNazwa;

	public ResourcesFundSources() {
	}

	public ResourcesFundSources(long id) {
		this.id = id;
	}

	public ResourcesFundSources(long id, Long bpZasobZrodlaId, Long identyfikatorNum, BigDecimal pkoszt, BigDecimal rkoszt, BigDecimal wspProc,
			Long zasobId, Long zrodloId, String zrodloIdn, String zrodloNazwa) {
		this.id = id;
		this.bpZasobZrodlaId = bpZasobZrodlaId;
		this.identyfikatorNum = identyfikatorNum;
		this.pkoszt = pkoszt;
		this.rkoszt = rkoszt;
		this.wspProc = wspProc;
		this.zasobId = zasobId;
		this.zrodloId = zrodloId;
		this.zrodloIdn = zrodloIdn;
		this.zrodloNazwa = zrodloNazwa;
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getBpZasobZrodlaId() {
		return this.bpZasobZrodlaId;
	}

	public void setBpZasobZrodlaId(Long bpZasobZrodlaId) {
		this.bpZasobZrodlaId = bpZasobZrodlaId;
	}

	public Long getIdentyfikatorNum() {
		return this.identyfikatorNum;
	}

	public void setIdentyfikatorNum(Long identyfikatorNum) {
		this.identyfikatorNum = identyfikatorNum;
	}

	public BigDecimal getPkoszt() {
		return this.pkoszt;
	}

	public void setPkoszt(BigDecimal pkoszt) {
		this.pkoszt = pkoszt;
	}

	public BigDecimal getRkoszt() {
		return this.rkoszt;
	}

	public void setRkoszt(BigDecimal rkoszt) {
		this.rkoszt = rkoszt;
	}

	public BigDecimal getWspProc() {
		return this.wspProc;
	}

	public void setWspProc(BigDecimal wspProc) {
		this.wspProc = wspProc;
	}

	public Long getZasobId() {
		return this.zasobId;
	}

	public void setZasobId(Long zasobId) {
		this.zasobId = zasobId;
	}

	public Long getZrodloId() {
		return this.zrodloId;
	}

	public void setZrodloId(Long zrodloId) {
		this.zrodloId = zrodloId;
	}

	public String getZrodloIdn() {
		return this.zrodloIdn;
	}

	public void setZrodloIdn(String zrodloIdn) {
		this.zrodloIdn = zrodloIdn;
	}

	public String getZrodloNazwa() {
		return this.zrodloNazwa;
	}

	public void setZrodloNazwa(String zrodloNazwa) {
		this.zrodloNazwa = zrodloNazwa;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ResourcesFundSources))
			return false;
		ResourcesFundSources castOther = (ResourcesFundSources) other;

		return (this.getId() == castOther.getId())
				&& ((this.getBpZasobZrodlaId() == castOther.getBpZasobZrodlaId()) || (this.getBpZasobZrodlaId() != null
						&& castOther.getBpZasobZrodlaId() != null && this.getBpZasobZrodlaId().equals(castOther.getBpZasobZrodlaId())))
				&& ((this.getIdentyfikatorNum() == castOther.getIdentyfikatorNum()) || (this.getIdentyfikatorNum() != null
						&& castOther.getIdentyfikatorNum() != null && this.getIdentyfikatorNum().equals(castOther.getIdentyfikatorNum())))
				&& ((this.getPkoszt() == castOther.getPkoszt()) || (this.getPkoszt() != null && castOther.getPkoszt() != null && this.getPkoszt().equals(
						castOther.getPkoszt())))
				&& ((this.getRkoszt() == castOther.getRkoszt()) || (this.getRkoszt() != null && castOther.getRkoszt() != null && this.getRkoszt().equals(
						castOther.getRkoszt())))
				&& ((this.getWspProc() == castOther.getWspProc()) || (this.getWspProc() != null && castOther.getWspProc() != null && this.getWspProc().equals(
						castOther.getWspProc())))
				&& ((this.getZasobId() == castOther.getZasobId()) || (this.getZasobId() != null && castOther.getZasobId() != null && this.getZasobId().equals(
						castOther.getZasobId())))
				&& ((this.getZrodloId() == castOther.getZrodloId()) || (this.getZrodloId() != null && castOther.getZrodloId() != null && this.getZrodloId()
						.equals(castOther.getZrodloId())))
				&& ((this.getZrodloIdn() == castOther.getZrodloIdn()) || (this.getZrodloIdn() != null && castOther.getZrodloIdn() != null && this
						.getZrodloIdn().equals(castOther.getZrodloIdn())))
				&& ((this.getZrodloNazwa() == castOther.getZrodloNazwa()) || (this.getZrodloNazwa() != null && castOther.getZrodloNazwa() != null && this
						.getZrodloNazwa().equals(castOther.getZrodloNazwa())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (int) this.getId();
		result = 37 * result + (getBpZasobZrodlaId() == null ? 0 : this.getBpZasobZrodlaId().hashCode());
		result = 37 * result + (getIdentyfikatorNum() == null ? 0 : this.getIdentyfikatorNum().hashCode());
		result = 37 * result + (getPkoszt() == null ? 0 : this.getPkoszt().hashCode());
		result = 37 * result + (getRkoszt() == null ? 0 : this.getRkoszt().hashCode());
		result = 37 * result + (getWspProc() == null ? 0 : this.getWspProc().hashCode());
		result = 37 * result + (getZasobId() == null ? 0 : this.getZasobId().hashCode());
		result = 37 * result + (getZrodloId() == null ? 0 : this.getZrodloId().hashCode());
		result = 37 * result + (getZrodloIdn() == null ? 0 : this.getZrodloIdn().hashCode());
		result = 37 * result + (getZrodloNazwa() == null ? 0 : this.getZrodloNazwa().hashCode());
		return result;
	}
	
	public void save() throws EdmException {
		try {
			DSApi.context().session().save(this);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}

    public void setAllFieldsFromServiceObject(DictionaryServicesProjectsStub.ProjectBudgetPositionResourceFundSource item) {
        this.bpZasobZrodlaId = item.getBpZasobZrodlaId();
        this.identyfikatorNum = item.getIdentyfikatorNum();
        this.pkoszt = item.getPKoszt();
        this.rkoszt = item.getRKoszt();
        this.wspProc = item.getWspProc();
        this.zasobId = item.getZasobId();
        this.zrodloId = item.getZrodloId();
        this.zrodloIdn = item.getZrodloIdn();
        this.zrodloNazwa = item.getZrodloNazwa();
    }
	
}
