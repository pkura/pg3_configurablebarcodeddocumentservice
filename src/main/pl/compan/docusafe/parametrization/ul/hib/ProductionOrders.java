package pl.compan.docusafe.parametrization.ul.hib;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.ws.ul.DictionaryServicesProductionOrdersStub.ProductionOrder;

public class ProductionOrders implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private boolean available;
	private String nazwa;
	private Integer zlecStatus;
	private Long zlecprodId;
	private String zlecprodIdm;

	public ProductionOrders() {
	}

	public ProductionOrders(Long id) {
		this.id = id;
	}

	public ProductionOrders(Long id, boolean available, String nazwa, Integer zlecStatus, Long zlecprodId, String zlecprodIdm) {
		this.id = id;
		this.available = available;
		this.nazwa = nazwa;
		this.zlecStatus = zlecStatus;
		this.zlecprodId = zlecprodId;
		this.zlecprodIdm = zlecprodIdm;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isAvailable() {
		return this.available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public String getNazwa() {
		return this.nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Integer getZlecStatus() {
		return this.zlecStatus;
	}

	public void setZlecStatus(Integer zlecStatus) {
		this.zlecStatus = zlecStatus;
	}

	public Long getZlecprodId() {
		return this.zlecprodId;
	}

	public void setZlecprodId(Long zlecprodId) {
		this.zlecprodId = zlecprodId;
	}

	public String getZlecprodIdm() {
		return this.zlecprodIdm;
	}

	public void setZlecprodIdm(String zlecprodIdm) {
		this.zlecprodIdm = zlecprodIdm;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ProductionOrders))
			return false;
		ProductionOrders castOther = (ProductionOrders) other;

		return ((this.getId() == castOther.getId()) || (this.getId() != null && castOther.getId() != null && this.getId().equals(castOther.getId())))
				&& (this.isAvailable() == castOther.isAvailable())
				&& ((this.getNazwa() == castOther.getNazwa()) || (this.getNazwa() != null && castOther.getNazwa() != null && this.getNazwa().equals(
						castOther.getNazwa())))
				&& ((this.getZlecStatus() == castOther.getZlecStatus()) || (this.getZlecStatus() != null && castOther.getZlecStatus() != null && this
						.getZlecStatus().equals(castOther.getZlecStatus())))
				&& ((this.getZlecprodId() == castOther.getZlecprodId()) || (this.getZlecprodId() != null && castOther.getZlecprodId() != null && this
						.getZlecprodId().equals(castOther.getZlecprodId())))
				&& ((this.getZlecprodIdm() == castOther.getZlecprodIdm()) || (this.getZlecprodIdm() != null && castOther.getZlecprodIdm() != null && this
						.getZlecprodIdm().equals(castOther.getZlecprodIdm())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getId() == null ? 0 : this.getId().hashCode());
		result = 37 * result + (this.isAvailable() ? 1 : 0);
		result = 37 * result + (getNazwa() == null ? 0 : this.getNazwa().hashCode());
		result = 37 * result + (getZlecStatus() == null ? 0 : this.getZlecStatus().hashCode());
		result = 37 * result + (getZlecprodId() == null ? 0 : this.getZlecprodId().hashCode());
		result = 37 * result + (getZlecprodIdm() == null ? 0 : this.getZlecprodIdm().hashCode());
		return result;
	}

	public void setAllFieldsFromServiceObject(ProductionOrder item) {
		this.nazwa = item.getNazwa();
		this.zlecprodId = item.getZlecprodId();
		this.zlecprodIdm = item.getZlecprodIdm();
		this.zlecStatus = item.getStatus();
	}

	public void save() throws EdmException {
		try {
			DSApi.context().session().save(this);
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
}
