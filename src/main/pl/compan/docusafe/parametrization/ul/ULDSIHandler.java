package pl.compan.docusafe.parametrization.ul;

import com.google.common.base.Preconditions;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.imports.dsi.DSIBean;
import pl.compan.docusafe.service.imports.dsi.DSIImportHandler;
import pl.compan.docusafe.service.imports.dsi.ImportHelper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Calendar;
import java.util.Date;

public class ULDSIHandler extends DSIImportHandler {
	
	private static final Logger log = LoggerFactory.getLogger(ULDSIHandler.class);
	
	@Override
	public String getDockindCn() {
		return "normal_out";
	}
	
	protected boolean isStartProces()
	{
		return true;
	}


    @Override
    protected void setUpDocumentAfterCreate(Document document) throws Exception {
        super.setUpDocumentAfterCreate(document);

        OfficeDocument officeDocument = (OfficeDocument)document;

        if (values.get("DOC_DESCRIPTION") != null) {
            officeDocument.setSummary(values.get("DOC_DESCRIPTION").toString());
        }

        if (values.get("SENDER_USER") != null) {
            String userName = DSUser.findByUsername(values.get("SENDER_USER").toString()).getName();
            document.setAuthor(userName);
            officeDocument.setCreatingUser(userName);
        } else {
            document.setAuthor(GlobalPreferences.getAdminUsername());
            officeDocument.setCreatingUser(GlobalPreferences.getAdminUsername());
        }

        if (values.get("SENDER_DIVISION_CODE") != null) {
            officeDocument.setDivisionGuid(DSDivision.findByCode("" + values.get("SENDER_DIVISION_CODE")).getGuid());
        }
    }

    @Override
	public void actionAfterCreate(Long documentId, DSIBean dsiB) throws Exception
	{	
		OutOfficeDocument doc = (OutOfficeDocument)OfficeDocument.find(documentId);

		Person person = new Person();
		person.setDictionaryGuid("rootdivision");
		person.setFirstname(""+values.get("RECIPIENT_FIRSTNAME"));
		person.setLastname(""+values.get("RECIPIENT_LASTNAME"));
		person.setLocation(""+values.get("RECIPIENT_LOCATION"));
		person.setStreet(""+values.get("RECIPIENT_STREET"));
		person.setZip(""+values.get("RECIPIENT_ZIP"));
        person.createIfNew();
        Recipient recipient = new Recipient();
        recipient.fromMap(person.toMap());
        recipient.setDictionaryGuid("rootdivision");
        recipient.setDictionaryType(Person.DICTIONARY_RECIPIENT);
        recipient.setBasePersonId(person.getId());
        recipient.setDocumentId(doc.getId());
        recipient.setDocument(doc);
        recipient.create();
        doc.addRecipient(recipient);
/*        Journal journal = Journal.getMainOutgoing();
        Long journalId;
        journalId = journal.getId();
        Integer sequenceId = null;
        sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), new Date());

        doc.bindToJournal(journalId, sequenceId);*/
    }
	
	@Override
	public boolean isRequiredAttachment() {
		return false; 
	}
	
	@Override
	public boolean isRequiredBox() {
		return false;
	}
	
	private void changeDate() throws Exception {
		Calendar c = Calendar.getInstance();
		c.setTime(GlobalPreferences.getCurrentDay());
		c.add(Calendar.DATE, 1);
		GlobalPreferences.setCurrentDay(c.getTime());
      if (c.get(Calendar.YEAR) != GlobalPreferences.getCurrentYear())
      {
    	  // zmiana roku
          GlobalPreferences.setCurrentYear(c.get(Calendar.YEAR));
      }
	}

	@Override
	protected void prepareImport() throws Exception 
	{
		ImportHelper.correctBigDecimal(values, "WAGA");
		ImportHelper.correctBigDecimal(values, "KOSZT_PRZESYLKI");
		ImportHelper.correctDateFormat(values, "DOC_DATE");
		StringBuffer sender = new StringBuffer();
		if(values.get("SENDER_USER") != null)
			sender.append("u:").append(values.get("SENDER_USER")).append(";");
		if(values.get("SENDER_DIVISION_CODE") != null)
		{
			sender.append("d:").append(DSDivision.findByCode(""+values.get("SENDER_DIVISION_CODE")).getGuid());
		}
		values.put("SENDER_HERE",sender.toString());
	}

}
