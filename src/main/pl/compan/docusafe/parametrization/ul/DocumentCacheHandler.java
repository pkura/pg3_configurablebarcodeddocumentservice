package pl.compan.docusafe.parametrization.ul;

import com.google.common.base.Objects;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.joda.time.DateMidnight;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ul.DictionaryServicesSettlementPeriodsStub;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class DocumentCacheHandler {
    private static final Logger log = LoggerFactory.getLogger(DocumentCacheHandler.class);

    private static final String CURRENCY_RATES_GET_SQL = "select kurs,tabkurs_idn,datod,datdo_fixed,waluta_nom_symbolwaluty,nominal " +
            "from " +
            "(select e1.id,e1.datod," +
            "ISNULL (e1.datdo, " +
            "(select min(e2.datod) from dsg_ul_services_exchange_rate as e2 where e1.waluta_nom_symbolwaluty=e2.waluta_nom_symbolwaluty and e1.waluta_baz_symbolwaluty=e2.waluta_baz_symbolwaluty and e2.datdo is null and e2.datod>e1.datod)" +
            ") as datdo_fixed, e1.waluta_nom_symbolwaluty,e1.tabkurs_idn,e1.kurs,e1.nominal " +
            "from dsg_ul_services_exchange_rate as e1 ) as subquery " +
            "where ((datod<=? and datdo_fixed>=?) " +
            "or (datod<=? and datdo_fixed is null)) " +
            "and waluta_nom_symbolwaluty = ? " +
            "order by datod desc";

    public static final int PLN_CURRENCY_ID = 0;
    public static final BigDecimal PLN_CURRENCY_DEFAULT_RATE = BigDecimal.ONE;
    public static final CurrencyRate PLN_CURRENCY_DEFAULT_EXCHANGE_RATE = new CurrencyRate(BigDecimal.ONE, CostInvoiceLogic.NIE_DOTYCZY);

    public static final CountryDelegaitonRates DEFAULT_DELEGATION_RATE = new CountryDelegaitonRates("EUR", new BigDecimal(41), new BigDecimal(140), true);

    private static class SettlementPeriodTable {

        private DictionaryServicesSettlementPeriodsStub.SettlementPeriod[] periods;

        private SettlementPeriodTable() {
            try {
                AxisClientConfigurator clientConf = new AxisClientConfigurator();
                DictionaryServicesSettlementPeriodsStub stub = new DictionaryServicesSettlementPeriodsStub(clientConf.getAxisConfigurationContext());
                clientConf.setUpHttpParameters(stub, "/services/DictionaryServicesSettlementPeriods");
                DictionaryServicesSettlementPeriodsStub.GetSettlementPeriodsResponse result = stub.getSettlementPeriods(new DictionaryServicesSettlementPeriodsStub.GetSettlementPeriods());
                periods = result.get_return();
                Arrays.sort(periods, new Comparator<DictionaryServicesSettlementPeriodsStub.SettlementPeriod>() {
                    @Override
                    public int compare(DictionaryServicesSettlementPeriodsStub.SettlementPeriod o1, DictionaryServicesSettlementPeriodsStub.SettlementPeriod o2) {
                        return o2.getDatod().compareTo(o1.getDatod());
                    }
                });
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

    }

    public static Long getSettlementPeriodId(Date date) {
        try {
            DictionaryServicesSettlementPeriodsStub.SettlementPeriod[] periods = periodTable.get().periods;
            for (DictionaryServicesSettlementPeriodsStub.SettlementPeriod period : periods) {
                if (new DateMidnight(date.getTime()).isAfter(period.getDatod().getTime())) {
                    return period.getPodokrrozlId();
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    public static final Supplier<SettlementPeriodTable> periodTable = Suppliers.memoizeWithExpiration(new Supplier<SettlementPeriodTable>() {
        @Override
        public SettlementPeriodTable get() {
            return new SettlementPeriodTable();
        }
    }, 1, TimeUnit.DAYS);

    public static final LoadingCache<String, BigDecimal> CURRENCY_RATES_CACHED = CacheBuilder.newBuilder()
            .softValues()
            .expireAfterWrite(15, TimeUnit.MINUTES)
            .build(new CacheLoader<String, BigDecimal>() {
                @Override
                public BigDecimal load(String key) throws Exception {
                    boolean isOpened = false;

                    PreparedStatement ps = null;
                    ResultSet rs;
                    try {
                        Date date = new Date();

                        if (date != null) {
                            int id = Integer.valueOf(key);
                            if (id == PLN_CURRENCY_ID) {
                                return PLN_CURRENCY_DEFAULT_RATE;
                            }

                            EnumItem current = DataBaseEnumField.getEnumItemForTable(CostInvoiceLogic.EXCHANGE_RATE_TABLE, id);
                            if (current != null) {
                                try {
                                    if (!DSApi.isContextOpen()) {
                                        DSApi.openAdmin();
                                        isOpened = true;
                                    }
                                } catch (Throwable e) {
                                }

                                ps = DSApi.context().prepareStatement(CURRENCY_RATES_GET_SQL);

                                ps.setString(4, current.getCn());

                                Timestamp timeStamp = new Timestamp(date.getTime());
                                ps.setTimestamp(1, timeStamp);
                                ps.setTimestamp(2, timeStamp);
                                ps.setTimestamp(3, timeStamp);

                                rs = ps.executeQuery();

                                if (rs.next()) {
                                    return Objects.firstNonNull(rs.getBigDecimal(1), BigDecimal.ONE);
                                }
                            }

                        }
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    } finally {
                        if (DSApi.isContextOpen()) {
                            DSApi.context().closeStatement(ps);
                        }

                        if (isOpened) {
                            DSApi.close();
                        }
                    }

                    return BigDecimal.ONE;
                }
            });

    public static final LoadingCache<CurrencyParam, BigDecimal> DATED_CURRENCY_RATES_CACHED = CacheBuilder.newBuilder()
            .softValues()
            .expireAfterWrite(15, TimeUnit.MINUTES)
            .build(new CacheLoader<CurrencyParam, BigDecimal>() {
                @Override
                public BigDecimal load(CurrencyParam key) throws Exception {
                    if (key.getCurrencyId() == PLN_CURRENCY_ID) {
                        return PLN_CURRENCY_DEFAULT_RATE;
                    }

                    boolean isOpened = false;

                    PreparedStatement ps = null;
                    ResultSet rs;
                    try {
                        Date date = key.getDate();

                        if (date != null) {
                            EnumItem current = DataBaseEnumField.getEnumItemForTable(CostInvoiceLogic.EXCHANGE_RATE_TABLE, key.getCurrencyId());
                            if (current != null) {
                                try {
                                    if (!DSApi.isContextOpen()) {
                                        DSApi.openAdmin();
                                        isOpened = true;
                                    }
                                } catch (Throwable e) {
                                }

                                ps = DSApi.context().prepareStatement(CURRENCY_RATES_GET_SQL);

                                ps.setString(4, current.getCn());

                                Timestamp timeStamp = new Timestamp(date.getTime());
                                ps.setTimestamp(1, timeStamp);
                                ps.setTimestamp(2, timeStamp);
                                ps.setTimestamp(3, timeStamp);

                                rs = ps.executeQuery();

                                if (rs.next()) {
                                    return Objects.firstNonNull(rs.getBigDecimal(1), BigDecimal.ONE);
                                }
                            }

                        }
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    } finally {
                        if (DSApi.isContextOpen()) {
                            DSApi.context().closeStatement(ps);
                        }

                        if (isOpened) {
                            DSApi.close();
                        }
                    }

                    return BigDecimal.ONE;
                }
            });


    private static final LoadingCache<CurrencyParam, CurrencyRate> DATED_CURRENCY_EXTENDED_RATES_CACHED = CacheBuilder.newBuilder()
            .softValues()
            .expireAfterWrite(15, TimeUnit.MINUTES)
            .build(new CacheLoader<CurrencyParam, CurrencyRate>() {
                @Override
                public CurrencyRate load(CurrencyParam key) throws Exception {
                    if (key.getCurrencyId() == PLN_CURRENCY_ID) {
                        return PLN_CURRENCY_DEFAULT_EXCHANGE_RATE;
                    }

                    boolean isOpened = false;

                    PreparedStatement ps = null;
                    ResultSet rs;
                    try {
                        Date date = key.getDate();

                        if (date != null) {
                            EnumItem current = DataBaseEnumField.getEnumItemForTable(CostInvoiceLogic.EXCHANGE_RATE_TABLE, key.getCurrencyId());
                            if (current != null) {
                                try {
                                    if (!DSApi.isContextOpen()) {
                                        DSApi.openAdmin();
                                        isOpened = true;
                                    }
                                } catch (Throwable e) {
                                }

                                ps = DSApi.context().prepareStatement(CURRENCY_RATES_GET_SQL);

                                ps.setString(4, current.getCn());

                                Timestamp timeStamp = new Timestamp(date.getTime());
                                ps.setTimestamp(1, timeStamp);
                                ps.setTimestamp(2, timeStamp);
                                ps.setTimestamp(3, timeStamp);

                                rs = ps.executeQuery();

                                if (rs.next()) {
                                    return new CurrencyRate(rs.getBigDecimal(1).divide(rs.getBigDecimal(6)), rs.getString(2));
                                }
                            }

                        }
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    } finally {
                        if (DSApi.isContextOpen()) {
                            DSApi.context().closeStatement(ps);
                        }

                        if (isOpened) {
                            DSApi.close();
                        }
                    }

                    return CurrencyRate.newPlain();
                }
            });

    static class CurrencyRate {
        private BigDecimal rate;
        private String rateTableCode;

        private CurrencyRate() {
        }

        CurrencyRate(BigDecimal rate, String rateTableCode) {
            this.rate = rate;
            this.rateTableCode = rateTableCode;
        }

        public BigDecimal getRateWithPlain() {
            return isPlain() ? BigDecimal.ZERO : rate;
        }

        public String getRateTableCodeWithPlain() {
            return isPlain() ? CostInvoiceLogic.NIE_ZNALEZIONO_W_ERP_MESSAGE : rateTableCode;
        }

        public boolean isPlain() {
            return rate == null || rateTableCode == null;
        }

        public static CurrencyRate newPlain() {
            return new CurrencyRate();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CurrencyRate that = (CurrencyRate) o;

            if (rate != null ? !rate.equals(that.rate) : that.rate != null) return false;
            if (rateTableCode != null ? !rateTableCode.equals(that.rateTableCode) : that.rateTableCode != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = rate != null ? rate.hashCode() : 0;
            result = 31 * result + (rateTableCode != null ? rateTableCode.hashCode() : 0);
            return result;
        }
    }

    static class CurrencyParam {
        private Integer currencyId;
        private Date date;

        private CurrencyParam() {
        }

        CurrencyParam(Integer currencyId, Date date) {
            this.currencyId = currencyId;
            this.date = date;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public Integer getCurrencyId() {
            return currencyId;
        }

        public void setCurrencyId(Integer currencyId) {
            this.currencyId = currencyId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CurrencyParam that = (CurrencyParam) o;

            if (!currencyId.equals(that.currencyId)) return false;
            if (!date.equals(that.date)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = currencyId.hashCode();
            result = 31 * result + date.hashCode();
            return result;
        }
    }

    public static CurrencyRate findCurrencyRate(Integer currencyId, Date date) {
        try {
            return DATED_CURRENCY_EXTENDED_RATES_CACHED.get(new CurrencyParam(currencyId, date));
        } catch (ExecutionException e) {
            log.error(e.getMessage(), e);
            return CurrencyRate.newPlain();
        }
    }

    private static final LoadingCache<String, CountryDelegaitonRates> DELEGATION_RATES = CacheBuilder.newBuilder()
            .softValues()
            .expireAfterWrite(15, TimeUnit.MINUTES)
            .build(new CacheLoader<String, CountryDelegaitonRates>() {
                @Override
                public CountryDelegaitonRates load(String key) throws Exception {
                    boolean isOpened = false;

                    PreparedStatement ps = null;
                    ResultSet rs;
                    try {
                        try {
                            if (!DSApi.isContextOpen()) {
                                DSApi.openAdmin();
                                isOpened = true;
                            }
                        } catch (Throwable e) {
                        }

                        ps = DSApi.context().prepareStatement("select currency_cn, diet_rate, acc_limit from dsg_ul_abroad_diet_rates where country_cn=?");
                        ps.setString(1, key);

                        rs = ps.executeQuery();

                        if (rs.next()) {
                            return new CountryDelegaitonRates(rs.getString(1), rs.getBigDecimal(2), rs.getBigDecimal(3));
                        }

                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    } finally {
                        if (DSApi.isContextOpen()) {
                            DSApi.context().closeStatement(ps);
                        }

                        if (isOpened) {
                            DSApi.close();
                        }
                    }
                    return DEFAULT_DELEGATION_RATE;
                }
            });

    public static CountryDelegaitonRates getCountryDelegaitonRates(String countryCn) {
        try {
            return DELEGATION_RATES.get(countryCn);
        } catch (ExecutionException e) {
            log.error(e.getMessage(), e);
            return DEFAULT_DELEGATION_RATE;
        }
    }

    static class CountryDelegaitonRates {
        private String currencyCn;
        private BigDecimal dietRate;
        private BigDecimal accLimit;
        private boolean empty;

        CountryDelegaitonRates(String currencyCn, BigDecimal dietRate, BigDecimal accLimit, boolean empty) {
            this.accLimit = accLimit;
            this.currencyCn = currencyCn;
            this.dietRate = dietRate;
            this.empty = empty;
        }

        CountryDelegaitonRates(String currencyCn, BigDecimal dietRate, BigDecimal accLimit) {
            this.accLimit = accLimit;
            this.currencyCn = currencyCn;
            this.dietRate = dietRate;
        }

        public BigDecimal getAccLimit() {
            return accLimit;
        }

        public void setAccLimit(BigDecimal accLimit) {
            this.accLimit = accLimit;
        }

        public String getCurrencyCn() {
            return currencyCn;
        }

        public void setCurrencyCn(String currencyCn) {
            this.currencyCn = currencyCn;
        }

        public BigDecimal getDietRate() {
            return dietRate;
        }

        public void setDietRate(BigDecimal dietRate) {
            this.dietRate = dietRate;
        }

        public boolean isEmpty() {
            return empty;
        }

        public void setEmpty(boolean empty) {
            this.empty = empty;
        }
    }
}


