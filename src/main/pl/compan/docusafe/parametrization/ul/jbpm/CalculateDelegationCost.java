package pl.compan.docusafe.parametrization.ul.jbpm;

import org.jbpm.api.activity.ActivityBehaviour;
import org.jbpm.api.activity.ActivityExecution;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.parametrization.ul.DelegationAbroadLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * User: kk
 * Date: 17.11.13
 * Time: 18:53
 */
public class CalculateDelegationCost implements ActivityBehaviour {

    protected static final Logger log = LoggerFactory.getLogger(CalculateDelegationCost.class);

    @Override
    public void execute(ActivityExecution activityExecution) throws Exception {
        try {
            Long docId = Long.valueOf(activityExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");

            DelegationAbroadLogic.calculateDelegationCost(docId);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            activityExecution.takeDefaultTransition();
        }
    }

}
