package pl.compan.docusafe.parametrization.ul.jbpm;

import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

@SuppressWarnings("serial")
@Deprecated
public class SendDocument implements ExternalActivityBehaviour {

	public static final String IS_EXPORT = "is-exporting";
	public static final String INVOICE_GUID = "export-guid";
	public static final String INVOICE_GUID_FAIL = "export-guid-fail";
	public static final String EXPORT_REPEAT_COUNTER = "export-repeat-counter";
	public static final String EXPORT_RESULT_STATE_NAME = "export-state-name";
	public static final String EXPORT_RESULT_STATE = "export-state";
	private static final Logger log = LoggerFactory.getLogger(SendDocument.class);

	@Override
	public void execute(ActivityExecution exec) throws Exception {
		exec.setVariable(IS_EXPORT, true);
		
		boolean isOpened = true;
		if (!DSApi.isContextOpen()) {
			isOpened = false;
			DSApi.openAdmin();
		}
		Long docId = Long.valueOf(exec.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");

		try {
			String guid = null;
			int i = 0;
			do {
				ExportFakturyKosztowej remoteInv = new ExportFakturyKosztowej();
				guid = remoteInv.wyslij(docId);
				Thread.sleep(500);
			} while (guid == null && i++ < 4);

			if (guid != null) {
				exec.setVariable(INVOICE_GUID, guid);
			}
		} catch (EdmException e) {
			//exec.setVariable(INVOICE_GUID_FAIL, true);
			log.error(e.getMessage(), e);
		} finally {
			if (DSApi.isContextOpen() && !isOpened)
				DSApi.close();
		}
		exec.takeDefaultTransition();
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
