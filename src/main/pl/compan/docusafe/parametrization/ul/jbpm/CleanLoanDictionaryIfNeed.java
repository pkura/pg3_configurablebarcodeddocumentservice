package pl.compan.docusafe.parametrization.ul.jbpm;

import com.beust.jcommander.internal.Lists;
import com.google.common.collect.ImmutableMap;
import org.jbpm.api.activity.ActivityBehaviour;
import org.jbpm.api.activity.ActivityExecution;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.parametrization.ul.DelegationAbroadLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Created by kk on 04.03.14.
 */
public class CleanLoanDictionaryIfNeed implements ActivityBehaviour {

    protected static final Logger log = LoggerFactory.getLogger(CleanLoanDictionaryIfNeed.class);

    @Override
    public void execute(ActivityExecution activityExecution) throws Exception {
        try {
            Long docId = Long.valueOf(activityExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            Document document = Document.find(docId);
            FieldsManager fm = document.getFieldsManager();

            if (!fm.getBoolean(DelegationAbroadLogic.ZALICZKA_WALUTOWA)) {
                document.getDocumentKind().setOnly(docId, ImmutableMap.of(DelegationAbroadLogic.ZALICZKA_WALUTOWA_DIC, Lists.newArrayList()));
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            activityExecution.takeDefaultTransition();
        }
    }
}
