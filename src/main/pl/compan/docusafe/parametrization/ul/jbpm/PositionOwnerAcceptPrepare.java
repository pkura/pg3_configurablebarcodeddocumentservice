package pl.compan.docusafe.parametrization.ul.jbpm;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.parametrization.ul.CostInvoiceLogic;
import pl.compan.docusafe.parametrization.ul.hib.UlBudgetPosition;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * User: kk
 * Date: 26.01.14
 * Time: 23:22
 */
public class PositionOwnerAcceptPrepare implements ExternalActivityBehaviour {
    private final static Logger log = LoggerFactory.getLogger(PositionOwnerAcceptPrepare.class);
    public static final String POSITION_IDS_SPLITTER = "#PID:";

    private String dictionaryField;

    @Override
    public void execute(ActivityExecution execution) throws Exception {
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        boolean wasOpened = false;
        try {
            if (!DSApi.isContextOpen()) {
                wasOpened = true;
                DSApi.openAdmin();
            }

            OfficeDocument doc = OfficeDocument.find(docId);

            FieldsManager fm = doc.getFieldsManager();

            List<Long> dictionaryIds = (List<Long>) fm.getKey(dictionaryField.toUpperCase());

            Multimap<String, Long> assigneeInfo = HashMultimap.create();

            if (dictionaryIds != null && !dictionaryIds.isEmpty()) {
                UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();


                for (Long bpId : dictionaryIds) {
                    UlBudgetPosition bp = bpInstance.find(bpId);

                    //brak akceptacji kierownika dla Funduszu Socjlano-Wychowywawczego orgranizacji studenckich oraz k� naukowych
                    if (bp.getSKind() != null && CostInvoiceLogic.SOCIAL_ID_FOR_NONE_BUDGET_OWNER_ACCEPT.contains(bp.getSKind().toString())) {
                        continue;
                    }

                    Set<String> usernameToAssignee = Sets.newTreeSet();
                    Set<String> guidToAssignee = Sets.newTreeSet();

                    try {
                        CostInvoiceLogic.findUnitsToPositionOwnerAssignee(bpId, usernameToAssignee, guidToAssignee);
                        if (usernameToAssignee.isEmpty() && guidToAssignee.isEmpty()) {
                            throw new EdmException("Nie znaleziono osoby do przypisania o id: " + bpId.toString() + ", przypisano do admin'a");
                        }
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                        log.error("Nie znaleziono osoby do przypisania, przypisano do admin'a");
                        String adminName = "admin";
                        if (e instanceof pl.compan.docusafe.core.jbpm4.AssigningHandledException) {
                            try {
                                adminName = GlobalPreferences.getAdminUsername();
                                doc.addRemark(new Remark(e.getMessage(), "admin"));
                            } catch (Exception e1) {
                                log.error(e1.getMessage(), e1);
                            }
                            usernameToAssignee.add(adminName);
                        }
                        usernameToAssignee.add(adminName);
                    }


                    Set<String> guidWithPrefix = Sets.newTreeSet(Iterables.transform(guidToAssignee, newPrefixFunction("d:")));
                    Set<String> usernameWithPrefix = Sets.newTreeSet(Iterables.transform(usernameToAssignee, newPrefixFunction("u:")));

                    assigneeInfo.put(Joiner.on(',').skipNulls().join(Sets.union(usernameWithPrefix, guidWithPrefix)), bpId);
                }

            }

            if (assigneeInfo.isEmpty()) {
                execution.take("omit");
            } else {
                List<String> assigneeProcessed = Lists.newArrayList(Iterables.transform(assigneeInfo.asMap().entrySet(), new Function<Map.Entry<String, Collection<Long>>, String>() {
                    @Override
                    public String apply(Map.Entry<String, Collection<Long>> input) {
                        return input.getKey() + POSITION_IDS_SPLITTER + Joiner.on(',').join(input.getValue());
                    }
                }));


                execution.setVariable("assignes", assigneeProcessed);
                execution.setVariable("count", assigneeProcessed.size());
                execution.take("to project-foreach");
            }
        } catch (EdmException ex) {
            log.error(ex.getMessage(), ex);
            execution.take("omit");
        } finally {
            if (DSApi.isContextOpen() && wasOpened)
                try {
                    DSApi.close();
                } catch (EdmException e) {
                    log.error(e.getMessage(), e);
                }
        }

    }

    private Function<String, String> newPrefixFunction(final String prefix) {
        return new Function<String, String>() {
            @Override
            public String apply(String input) {
                return prefix+input;
            }
        };
    }

    @Override
    public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
        // TODO Auto-generated method stub

    }

}
