package pl.compan.docusafe.parametrization.ul.jbpm;

import java.util.List;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ul.CostInvoiceLogic;
import pl.compan.docusafe.parametrization.ul.hib.UlBudgetPosition;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


public class InventoryDecisionHandler implements DecisionHandler {
    private final static Logger LOG = LoggerFactory.getLogger(InventoryDecisionHandler.class);

    public String decide(OpenExecution openExecution) {
        try{
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
    		String invoiceCn = fm.getEnumItemCn("RODZAJ_FAKTURY");
            if ("faktura_zagraniczna".equalsIgnoreCase(invoiceCn)) {
                return "true";
            } else {
                List<Long> dictionaryIds = (List<Long>) fm.getKey(CostInvoiceLogic.BP_FIELD);
                UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
                for (Long bpId : dictionaryIds) {
                    UlBudgetPosition bp = bpInstance.find(bpId);
                    if (!bp.getCKind().equals(1620)) {
                        return "true";
                    }
                }
            }
            return "false";
        } catch (EdmException ex) {
            LOG.error(ex.getMessage(), ex);
            return "error";
        }
    }
}

