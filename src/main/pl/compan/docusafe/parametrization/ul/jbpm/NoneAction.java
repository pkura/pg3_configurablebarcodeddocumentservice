package pl.compan.docusafe.parametrization.ul.jbpm;

import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

@Deprecated
@SuppressWarnings("serial")
public class NoneAction implements ExternalActivityBehaviour {

	public static final String INVOICE_GUID = "export-guid";
	public static final String INVOICE_GUID_FAIL = "export-guid-fail";
	public static final String EXPORT_REPEAT_COUNTER = "export-repeat-counter";
	public static final String EXPORT_RESULT_STATE_NAME = "export-state-name";
	public static final String EXPORT_RESULT_STATE = "export-state";

	@Override
	public void execute(ActivityExecution exec) throws Exception {
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
