package pl.compan.docusafe.parametrization.ul.jbpm;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.jbpm.api.activity.ActivityBehaviour;
import org.jbpm.api.activity.ActivityExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.parametrization.ul.DelegationAbroadLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * User: kk
 * Date: 27.10.13
 * Time: 20:48
 */
public class CopyDelegationPreCostDictionaries implements ActivityBehaviour {

    protected static final Logger log = LoggerFactory.getLogger(CopyDelegationPreCostDictionaries.class);

    public static final String MAIN_SQL =       "begin transaction; \n" +
                                                "begin try DECLARE @inserted_ids TABLE ([id] INT); \n" +
                                                "insert into dsg_ul_delegation_abroad_dic (enum,value,rate,description,money_value,money_value_pln,currency,currency_rate,currency_rate_date,bool1,bool2,bool3,bool4,country,add_money_value,add_money_value_pln,add_enum,int1,int2,int3,int4,int5,add_rate)\n" +
                                                "OUTPUT INSERTED.[id] INTO @inserted_ids \n" +
                                                "select enum,value,rate,description,money_value,money_value_pln,currency,currency_rate,currency_rate_date,bool1,bool2,bool3,bool4,country,add_money_value,add_money_value_pln,add_enum,int1,int2,int3,int4,int5,add_rate\n" +
                                                "from dsg_ul_delegation_abroad_dic dic \n" +
                                                "JOIN dsg_ul_delegation_abroad_multiple_value mu ON dic.ID=mu.FIELD_VAL where mu.FIELD_CN=? and mu.DOCUMENT_ID=?;\n" +
                                                "insert into dsg_ul_delegation_abroad_multiple_value (DOCUMENT_ID,FIELD_CN,FIELD_VAL) select ? as DOCUMENT_ID, ? as FIELD_CN, id as FIELD_VAL \n" +
                                                "from @inserted_ids; \n" +
                                                "commit; \n" +
                                                "end try\n" +
                                                "begin catch \n" +
                                                "rollback; \n" +
                                                "end catch";

    public static final String OTHER_COST_SQL = "begin transaction; \n" +
                                                "begin try DECLARE @inserted_ids TABLE ([id] INT); \n" +
                                                "insert into dsg_ul_delegation_abroad_dic (enum,value,rate,description,money_value,money_value_pln,currency,currency_rate,currency_rate_date,bool1,bool2,bool3,bool4,country,add_money_value,add_money_value_pln,add_enum,int1,int2,int3,int4,int5,add_rate)\n" +
                                                "OUTPUT INSERTED.[id] INTO @inserted_ids \n" +
                                                "select enum,value,rate,description,money_value,money_value_pln,currency,currency_rate,currency_rate_date,bool1,bool2,bool3,bool4,country,add_money_value,add_money_value_pln,add_enum,int1,int2,int3,int4,int5,add_rate\n" +
                                                "from dsg_ul_delegation_abroad_dic dic \n" +
                                                "JOIN dsg_ul_delegation_abroad_multiple_value mu ON dic.ID=mu.FIELD_VAL where mu.FIELD_CN=? and mu.DOCUMENT_ID=? and dic.enum="+DelegationAbroadLogic.OTHER_TRIP_COST_ID+";\n" +
                                                "insert into dsg_ul_delegation_abroad_multiple_value (DOCUMENT_ID,FIELD_CN,FIELD_VAL) select ? as DOCUMENT_ID, ? as FIELD_CN, id as FIELD_VAL \n" +
                                                "from @inserted_ids; \n" +
                                                "commit; \n" +
                                                "end try\n" +
                                                "begin catch \n" +
                                                "rollback; \n" +
                                                "end catch";

    public static final List<MapInfo> MAPPINGS = ImmutableList.<MapInfo>builder()   .add(new MapInfo(DelegationAbroadLogic.DIETA_ZAGRANICZNA, DelegationAbroadLogic.DIETA_ZAGRANICZNA+"_PRE", MAIN_SQL))
                                                                                    .add(new MapInfo(DelegationAbroadLogic.KOSZTY_NOCLEGU, DelegationAbroadLogic.KOSZTY_NOCLEGU+"_PRE", MAIN_SQL))
                                                                                    //.add(new MapInfo(DelegationAbroadLogic.KOSZTY_INNE, DelegationAbroadLogic.KALKULACJA_KOSZTOW, OTHER_COST_SQL))
                                                                                    .build();




    @Override
    public void execute(ActivityExecution activityExecution) throws Exception {
        try {
            Long docId = Long.valueOf(activityExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");

            for (MapInfo mapInfo : MAPPINGS) {
                PreparedStatement ps = null;
                try {
                    ps = DSApi.context().prepareStatement(mapInfo.getSql());
                    ps.setString(1, mapInfo.getPreCostDictCn());
                    ps.setLong(2, docId);
                    ps.setLong(3, docId);
                    ps.setString(4, mapInfo.getMainCostDictCn());

                    ps.executeUpdate();
                } catch (SQLException e) {
                    throw new EdmSQLException(e);
                } finally {
                    if (ps != null) {
                        DSApi.context().closeStatement(ps);
                    }
                }
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            activityExecution.takeDefaultTransition();
        }
    }

    private static class MapInfo {
        private String sql;
        private String preCostDictCn;
        private String mainCostDictCn;

        MapInfo(String mainCostDictCn, String preCostDictCn, String sql) {
            this.mainCostDictCn = mainCostDictCn;
            this.preCostDictCn = preCostDictCn;
            this.sql = sql;
        }

        public String getMainCostDictCn() {
            return mainCostDictCn;
        }

        public String getPreCostDictCn() {
            return preCostDictCn;
        }

        public String getSql() {
            return sql;
        }
    }
}
