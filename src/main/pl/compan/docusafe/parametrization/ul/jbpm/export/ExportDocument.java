package pl.compan.docusafe.parametrization.ul.jbpm.export;

import java.util.Collections;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.exports.ExportHandler;
import pl.compan.docusafe.core.exports.ExportedDocument;
import pl.compan.docusafe.core.exports.SimpleErpExportFactory;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.base.Strings;

@SuppressWarnings("serial")
public class ExportDocument implements ExternalActivityBehaviour {

	public static final String IS_EXPORT = "is-exporting";
	public static final String DOC_GUID = "export-guid";
	public static final String DOC_GUID_FAIL = "export-guid-fail";
	public static final String EXPORT_REPEAT_COUNTER = "export-repeat-counter";
	public static final String EXPORT_RESULT_STATE_NAME = "export-state-name";
	public static final String EXPORT_RESULT_STATE = "export-state";
	private static final Logger log = LoggerFactory.getLogger(ExportDocument.class);

	@Override
	public void execute(ActivityExecution exec) throws Exception {
		exec.setVariable(IS_EXPORT, true);
		
		boolean isOpened = true;
		if (!DSApi.isContextOpen()) {
			isOpened = false;
			DSApi.openAdmin();
		}
		Long docId = Long.valueOf(exec.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		
		try {
			OfficeDocument doc = OfficeDocument.find(docId);
			ExportedDocument exported = doc.getDocumentKind().logic().buildExportDocument(doc);
			if (exported != null) {
				ExportHandler handler = new ExportHandler(new SimpleErpExportFactory());
				String guid = handler.xmlExport(exported);
				if (Strings.isNullOrEmpty(guid)) {
					throw new EdmException("B�ad w eksporcie dokumentu, mo�liwy b��d w komunikacji, serwis nie zwr�ci� kodu guid dla dokumentu.");
				}
				exec.setVariable(DOC_GUID, guid);
				
				doc.getDocumentKind().setOnly(docId, Collections.singletonMap("STATUS", 1500), false);
			}
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
			throw new EdmException(e);
		} finally {
			if (DSApi.isContextOpen() && !isOpened)
				DSApi.close();
		}
		exec.takeDefaultTransition();
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
