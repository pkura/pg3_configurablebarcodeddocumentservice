package pl.compan.docusafe.parametrization.ul.jbpm;

import com.google.common.collect.ImmutableMap;
import org.jbpm.api.activity.ActivityBehaviour;
import org.jbpm.api.activity.ActivityExecution;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.parametrization.ul.DelegationLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * User: kk
 * Date: 24.10.13
 * Time: 12:48
 */
public class GenerateDelegationNumber implements ActivityBehaviour {

    protected static final Logger log = LoggerFactory.getLogger(GenerateDelegationNumber.class);

    @Override
    public void execute(ActivityExecution execution) throws Exception {
        try {
            Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");

            OfficeDocument doc = OfficeDocument.find(docId);
            String divGuid = doc.getFieldsManager().getEnumItemCn(DelegationLogic.WORKER_DIVISION);
            String divCode = DSDivision.find(divGuid).getCode();

            String delegationNumber = String.format("%d/%tm/%tY/%s",
                    doc.getOfficeNumber(), doc.getCtime(), doc.getCtime(), divCode);

            doc.getDocumentKind().setOnly(docId, ImmutableMap.of(DelegationLogic.NR_WYJAZDU_FIELD, delegationNumber), false);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            execution.takeDefaultTransition();
        }
    }
}
