package pl.compan.docusafe.parametrization.ul.jbpm;

import com.google.common.base.Joiner;
import com.google.common.collect.Sets;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 07.08.13
 * Time: 16:28
 * To change this template use File | Settings | File Templates.
 */
public class AddCGUsersToVariables implements ExternalActivityBehaviour {
    private final static Logger LOG = LoggerFactory.getLogger(AddCGUsersToVariables.class);

    @Override
    public void signal(ActivityExecution execution, String signalName, Map<String, ?> parameters) throws Exception {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void execute(ActivityExecution execution) throws Exception {
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        boolean wasOpened = false;
        try {
            if (!DSApi.isContextOpen()) {
                wasOpened = true;
                DSApi.openAdmin();
            }

            OfficeDocument doc = OfficeDocument.find(docId);

            FieldsManager fm = doc.getFieldsManager();

            List<Number> userIds = (List<Number>) fm.getKey("CG_USERS");

            Set<String> userNames = Sets.newLinkedHashSet();
            for (Number userId : userIds) {
                userNames.add(DSUser.findById(userId.longValue()).getName());
            }

            execution.setVariable("cg_users", Joiner.on(",").join(userNames));

        } catch (EdmException ex) {
            LOG.error(ex.getMessage(), ex);
        } finally {
            if (DSApi.isContextOpen() && wasOpened)
                try {
                    DSApi.close();
                } catch (EdmException e) {
                    LOG.error(e.getMessage(), e);
                }
        }
        execution.takeDefaultTransition();
    }
}
