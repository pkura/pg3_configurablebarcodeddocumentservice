package pl.compan.docusafe.parametrization.ul.jbpm;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.parametrization.ams.jbpm.ExportDocumentDecision;
import pl.compan.docusafe.parametrization.ul.jbpm.export.ExportDocument;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@SuppressWarnings("serial")
public class RefreshTaskListListener extends AbstractEventListener {
     private static final Set<Long> docIds = Collections.synchronizedSet(new HashSet<Long>());

	@Override
	public void notify(EventListenerExecution exec) throws Exception {
        Long docId = Long.valueOf(exec.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        boolean refresh = false;

        if ((exec.hasVariable(ExportDocument.IS_EXPORT) && (Boolean) exec.getVariable(ExportDocument.IS_EXPORT) == true) || (exec.hasVariable(ExportDocumentDecision.TO_CONFIRM) && (Boolean) exec.getVariable(ExportDocumentDecision.TO_CONFIRM) == true)) {
            synchronized (docIds) {
                if (docIds.add(docId)) {
                    refresh = true;
                }
            }
            if (refresh) {
                boolean hasOpen = true;
                try {
                    if (!DSApi.isContextOpen()) {
                        hasOpen = false;
                        DSApi.openAdmin();
                    }

                    DSApi.context().begin();
                    TaskSnapshot.updateByDocument(Document.find(docId));
                    DSApi.context().commit();

                    exec.removeVariable(ExportDocument.IS_EXPORT);

                    synchronized (docIds) {
                        docIds.remove(docId);
                    }
                } catch(Exception e){
                    LOG.error("RefreshTaskListListener: B��d podczas od�wie�ania listy zada� (docId= ? ): ?",docId,e);
                    LOG.error("Stacktrace: "+e.getStackTrace());
                } finally {
                    if (DSApi.isContextOpen() && !hasOpen) {
                        DSApi.close();
                    }
                }
            }
        }
    }

}
