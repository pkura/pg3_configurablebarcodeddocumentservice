package pl.compan.docusafe.parametrization.ul.jbpm;

/**
 * User: kk
 * Date: 03.01.14
 * Time: 12:15
 */

import com.google.common.collect.ImmutableSet;
import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ul.CostInvoiceLogic;
import pl.compan.docusafe.parametrization.ul.hib.UlBudgetPosition;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.List;
import java.util.Set;

public class InventoryDecision implements DecisionHandler {
    private final static Logger LOG = LoggerFactory.getLogger(InventoryDecision.class);

    public final static Set<Integer> INVENTORY_KIND_IDS = ImmutableSet.of(1624,1623,1626,1627,1621,1622,1628);

            /*
                        <enum-item id="1620" cn="ND" title="Nie dotyczy"/>
					    <enum-item id="1621" cn="SRODEK_TRWALY" title="�rodek trwa�y"/>
					    <enum-item id="1622" cn="NIEMATERIALNE" title="Warto�ci niematerialne i prawne"/>
						<enum-item id="1623" cn="NISKOCENNE" title="Niskocenne"/>
						<enum-item id="1624" cn="APARATURA" title="Aparatura"/>
						<enum-item id="1625" cn="INW_W_TOKU" title="Inwestycja w toku"/>
						<enum-item id="1626" cn="KSIEGOZBIOR" title="Ksi�gozbi�r"/>
						<enum-item id="1627" cn="REGLAMENTOWANE" title="Reglamentowane"/>
						<enum-item id="1628" cn="FAKTURA" title="Faktura magazynowa"/>
			*/

    public String decide(OpenExecution openExecution) {
        try{
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            List<Long> dictionaryIds = (List<Long>) fm.getKey(CostInvoiceLogic.BP_FIELD);
            UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
            for (Long bpId : dictionaryIds) {
                UlBudgetPosition bp = bpInstance.find(bpId);
                if (INVENTORY_KIND_IDS.contains(bp.getCKind())) {
                    return "true";
                }
            }
            return "false";
        } catch (EdmException ex) {
            LOG.error(ex.getMessage(), ex);
            return "error";
        }
    }
}

