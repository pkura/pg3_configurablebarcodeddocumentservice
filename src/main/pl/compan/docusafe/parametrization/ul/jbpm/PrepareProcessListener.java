package pl.compan.docusafe.parametrization.ul.jbpm;

import java.util.List;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ul.CostInvoiceLogic;
import pl.compan.docusafe.parametrization.ul.hib.UlBudgetPosition;

import com.google.common.collect.Lists;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

@SuppressWarnings("serial")
public class PrepareProcessListener implements ExternalActivityBehaviour {
    private final static Logger LOG = LoggerFactory.getLogger(PrepareProcessListener.class);

    private String dictionaryField;
	
	@Override
	public void execute(ActivityExecution execution) throws Exception {
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        boolean wasOpened = false;
        try {
            if (!DSApi.isContextOpen()) {
                wasOpened = true;
                DSApi.openAdmin();
            }

            OfficeDocument doc = OfficeDocument.find(docId);

            List<Long> argSets = Lists.newLinkedList();
            FieldsManager fm = doc.getFieldsManager();

            List<Long> dictionaryIds = (List<Long>) fm.getKey(dictionaryField.toUpperCase());

            if (dictionaryIds != null && !dictionaryIds.isEmpty()) {
                UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
                for (Long bpId : dictionaryIds) {
                    UlBudgetPosition bp = bpInstance.find(bpId);

                    //brak akceptacji kierownika dla Funduszu Socjlano-Wychowywawczego orgranizacji studenckich oraz k� naukowych
                    if (bp.getSKind() != null && CostInvoiceLogic.SOCIAL_ID_FOR_NONE_BUDGET_OWNER_ACCEPT.contains(bp.getSKind().toString())) {
                        continue;
                    }

                    argSets.add(bpId);
                }
            }

            if (argSets.isEmpty()) {
                execution.take("omit");
            } else {
                execution.setVariable("ids", argSets);
                execution.setVariable("count", argSets.size());
                execution.take("to project-foreach");
            }
        } catch (EdmException ex) {
            LOG.error(ex.getMessage(), ex);
            execution.take("omit");
        } finally {
            if (DSApi.isContextOpen() && wasOpened)
                try {
                    DSApi.close();
                } catch (EdmException e) {
                    LOG.error(e.getMessage(), e);
                }
        }

	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
		// TODO Auto-generated method stub

	}

}
