package pl.compan.docusafe.parametrization.ul.jbpm;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.jbpm.api.listener.EventListener;
import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ul.CostInvoiceLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

/**
 * Created by kk on 16.03.14.
 */
public class FillSpecialPaymentsDictionary implements EventListener {
    protected static final Logger log = LoggerFactory.getLogger(FillSpecialPaymentsDictionary.class);


    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        boolean hasOpen = true;
        if (!DSApi.isContextOpen()) {
            hasOpen = false;
            DSApi.openAdmin();
        }
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId, false);

        FieldsManager fm = doc.getFieldsManager();
        Date paymentDate = (Date) fm.getKey("TERMIN_PLATNOSCI");
        BigDecimal grossPlnAmount = (BigDecimal) fm.getKey("KWOTA_BRUTTO");
        Number currencyRatio = (Number) fm.getKey("KWOTA_KURS");

        if (!Boolean.TRUE.equals(fm.getBoolean("PLATNOSC_SZCZEGOLNA")) && grossPlnAmount != null & paymentDate != null) {
            BigDecimal grossAmount = grossPlnAmount.divide(new BigDecimal(Objects.firstNonNull(currencyRatio, 1d).doubleValue()).setScale(4, RoundingMode.HALF_UP), 2, RoundingMode.HALF_UP);
            DwrDictionaryBase dictionary = DwrDictionaryFacade.getDwrDictionary(CostInvoiceLogic.DOC_KIND_CN, "RACHUNKI");
            long id = dictionary.add(ImmutableMap.of("RACHUNKI_KWOTA", new FieldData(Field.Type.MONEY, grossAmount), "RACHUNKI_DATA", new FieldData(Field.Type.DATE, paymentDate)));

            if (id>0) {
                doc.getDocumentKind().setOnly(docId, ImmutableMap.of("RACHUNKI", ImmutableList.of(id)), false);
            }
        }
        if (DSApi.isContextOpen() && !hasOpen)
            DSApi.close();
    }
}
