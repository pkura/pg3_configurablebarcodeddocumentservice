package pl.compan.docusafe.parametrization.ul.jbpm.export;

import static pl.compan.docusafe.parametrization.ul.jbpm.export.ExportDocument.DOC_GUID;
import static pl.compan.docusafe.parametrization.ul.jbpm.export.ExportDocument.EXPORT_REPEAT_COUNTER;
import static pl.compan.docusafe.parametrization.ul.jbpm.export.ExportDocument.EXPORT_RESULT_STATE;
import static pl.compan.docusafe.parametrization.ul.jbpm.export.ExportDocument.EXPORT_RESULT_STATE_NAME;

import java.util.Collections;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.exports.ExportHandler;
import pl.compan.docusafe.core.exports.RequestResult;
import pl.compan.docusafe.core.exports.SimpleErpExportFactory;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

@SuppressWarnings("serial")
public class CheckExportDocumentTillEnd implements ExternalActivityBehaviour {
	
	private static final String FAST_REPEAT = "fast_repeat";
	private static final String REPEAT = "repeat";
	private static final String SUCCESS_TRANSITION = "success_transition";
	private static final String FAIL_TRANSITION = "fail_transition";
				//ZGL_BLEDU = ZGLOSZENIE_BLEDU, Formularz do przesyłania info o błędzie do admina (usługą lub przez człowieka)  ----------
				/** zgłoszenie błędu: ID statusu błędu nowego */				String ZGL_BLEDU_nowy = "11";
				/** zgłoszenie błędu: ID błędu będącego w obsłudze */ 			String ZGL_BLEDU_obslugiwany = "12";
				/** zgłoszenie błędu: ID błędu rozwiązanego */ 					String ZGL_BLEDU_rozwiazany = "13";
				/** zgłoszenie błędu: ID błędu odrzuconego */ 					String ZGL_BLEDU_odrzucony = "14";
				/** zgłoszenie błędu: ID błędu odsuniętego w czasie */ 			String ZGL_BLEDU_odsuniety= "15";
			
				/** Komunikat gotowy do pobrania przez szynę. */				String ERP_PRZESLANO_NA_SZYNE = "NEW"; 
				/** Komunikat został pobrany przez task */						String ESB_POBRANY_PRZEZ_SZYNE = "UPLOADED"; 
				/** Komunikat jest przetwarzany przez szynę danych */			String ESB_PRZETWARZANY_PRZEZ_SZYNE = "PROCESSING"; 
				/** Komunikat został poprawnie dodany do Simple.ERP */			String ESB_DODANO_DO_ERP = "FINISHED"; 
				/** Komunikat nie przeszedł wstępnej walidacji XSD */			String ESB_BLAD_WALIDACJI_PLIKU_XML = "INVALID"; 
				/** Komunikat nie został dodany do Simple.ERP system 
				 * zgłosił kod błędu */ 										String ESB_BLAD_WEWNETRZNY_ERP = "FAULT"; 
	
	private static final Logger log = LoggerFactory.getLogger(CheckExportDocumentTillEnd.class);

	private Integer fastRepeats;
	
	@Override
	public void execute(ActivityExecution exec) throws Exception {
		boolean isOpened = true;
		if (!DSApi.isContextOpen()) {
			isOpened = false;
			DSApi.openAdmin();
		}

		Long docId = Long.valueOf(exec.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		
		Integer counter = 1;
		if (exec.hasVariable(EXPORT_REPEAT_COUNTER)) {
			counter = (Integer) exec.getVariable(EXPORT_REPEAT_COUNTER);
		}
		if (counter + 1 > fastRepeats) {
			exec.take(REPEAT);
		} else {
			exec.take(FAST_REPEAT);
		}
		exec.setVariable(EXPORT_REPEAT_COUNTER, ++counter);

		String guid = "";
		try {
			if (exec.hasVariable(DOC_GUID)) {
				guid = exec.getVariable(DOC_GUID).toString();

				ExportHandler handler = new ExportHandler(new SimpleErpExportFactory());
				RequestResult result = handler.checkExport(guid);

				int state = result.getStateId();
				exec.setVariable(EXPORT_RESULT_STATE, state);

				String stateName = result.getStateName();
				exec.setVariable(EXPORT_RESULT_STATE_NAME, stateName);

				String requestResult = result.getResult();
				StringBuilder results = new StringBuilder();
				results.append("Guid dokumentu: " + guid + "\n");
				results.append("Nr stanu: " + state + "\n");
				results.append("Nazwa stanu: " + stateName + "\n");
				results.append("Odpowiedź serwera: " + requestResult + "\n");

				if (ESB_BLAD_WEWNETRZNY_ERP.equals(stateName) || ESB_BLAD_WALIDACJI_PLIKU_XML.equals(stateName)) {
					log.error("Błąd eksportu, counter: " + counter);
					log.error("Błąd eksportu, guid: " + guid);
					log.error("Błąd eksportu, docId: " + exec.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY));
					log.error("Błąd eksportu, state: " + state);
					log.error("Błąd eksportu, stateName: " + stateName);
					log.error("Błąd eksportu, request result: " + requestResult);
					exec.removeVariable(EXPORT_REPEAT_COUNTER);
					exec.take(FAIL_TRANSITION);

					results.append("Błąd eksportu\n\n");
					if (docId != null) {
						setResults(results.toString(), docId);
					}
				} else if (ESB_DODANO_DO_ERP.equals(stateName)) {
					exec.removeVariable(EXPORT_REPEAT_COUNTER);
					exec.take(SUCCESS_TRANSITION);
					

					results.append("Eksport zakończony pomyślnie\n\n");
					if (docId != null) {
						setFinishStatus(docId);
						setResults(results.toString(), docId);
					}
				}

			} else {
				log.error("Błąd eksportu, guid: brak");
				log.error("Błąd eksportu, docId: " + exec.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY));
				exec.removeVariable(EXPORT_REPEAT_COUNTER);
				exec.take(FAIL_TRANSITION);
				
				setResults("Błąd eksportu, guid: brak", docId);
			}
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
		} finally {
			if (DSApi.isContextOpen() && !isOpened)
				DSApi.close();
		}

	}

	protected void setResults(String result, Long docId) throws EdmException {
		try {
			DSApi.context().begin();
	        OfficeDocument doc = OfficeDocument.findOfficeDocument(docId);
	        doc.addRemark(new Remark(result, "admin"));
			DSApi.context().commit();
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
			DSApi.context().setRollbackOnly();
		}
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	protected void setFinishStatus(Long docId) throws EdmException {
		try {
			DSApi.context().begin();
			OfficeDocument.find(docId).getDocumentKind().setOnly(docId, Collections.singletonMap("STATUS", 2000), false);
			DSApi.context().commit();
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
			DSApi.context().setRollbackOnly();
		}
	}

}
