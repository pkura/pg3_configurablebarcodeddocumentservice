package pl.compan.docusafe.parametrization.ul.jbpm;

import java.util.Collection;
import java.util.List;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ul.CostInvoiceLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Lists;

public class IsQuestorEntitledDecision implements DecisionHandler {

	private static final long serialVersionUID = 1L;
	protected Logger log = LoggerFactory.getLogger(IsQuestorEntitledDecision.class);


	@Override
	public String decide(OpenExecution openExecution) {
		try {
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();

			Object dicRaw = fm.getKey(CostInvoiceLogic.UPOWAZNIONY_KWESTOR_DICTIONARY_FIELD_NAME);
			List<Integer> upowaznionyEnumIds = Lists.newArrayList();
			if (dicRaw instanceof Collection<?>) {
				Collection<Long> items = (Collection<Long>) dicRaw;
				
				if(!items.isEmpty()){
					return "questor_entitled";
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(),e);
			return "error";
		}
		
		return "questor";
	}

}
