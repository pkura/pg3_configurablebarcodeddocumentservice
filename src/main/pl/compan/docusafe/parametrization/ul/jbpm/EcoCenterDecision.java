package pl.compan.docusafe.parametrization.ul.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ul.CostInvoiceLogic;
import pl.compan.docusafe.parametrization.ul.hib.UlBudgetPosition;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.List;

/**
 * User: kk
 * Date: 03.01.14
 * Time: 12:26
 */
public class EcoCenterDecision implements DecisionHandler {
    private final static Logger LOG = LoggerFactory.getLogger(EcoCenterDecision.class);
    public static final Integer BOOK_KIND_ID = 1626;

    public String decide(OpenExecution openExecution) {
        try{
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            String invoiceCn = fm.getEnumItemCn("RODZAJ_FAKTURY");
            if ("faktura_zagraniczna".equalsIgnoreCase(invoiceCn)) {
                return "true";
            } else {
                List<Long> dictionaryIds = (List<Long>) fm.getKey(CostInvoiceLogic.BP_FIELD);
                UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
                for (Long bpId : dictionaryIds) {
                    UlBudgetPosition bp = bpInstance.find(bpId);
                    if (InventoryDecision.INVENTORY_KIND_IDS.contains(bp.getCKind()) && !BOOK_KIND_ID.equals(bp.getCKind())) {
                        return "true";
                    }
                }
            }
            return "false";
        } catch (EdmException ex) {
            LOG.error(ex.getMessage(), ex);
            return "error";
        }
    }
}

