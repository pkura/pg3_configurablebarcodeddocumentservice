package pl.compan.docusafe.parametrization.ul.jbpm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ul.CostInvoiceLogic;
import pl.compan.docusafe.parametrization.ul.hib.UlBudgetPosition;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

@SuppressWarnings("serial")
public class PrepareForBudgetTypeListener implements ExternalActivityBehaviour {

	private static final Logger log = LoggerFactory.getLogger(PrepareForBudgetTypeListener.class);
	
	@Override
	public void execute(ActivityExecution execution) throws Exception {
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");

		boolean isOpened = true;
		if (!DSApi.isContextOpen()) {
			isOpened = false;
			DSApi.openAdmin();
		}
		try {
			OfficeDocument doc = OfficeDocument.find(docId);

			final Set<Long> argSets = new HashSet<Long>();
			FieldsManager fm = doc.getFieldsManager();

			List<Long> dictionaryIds = (List<Long>) fm.getKey(CostInvoiceLogic.BP_FIELD);
			if (dictionaryIds != null) {
				UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
				for (Long mpkId : dictionaryIds) {
					UlBudgetPosition bp = bpInstance.find(mpkId);
					if (bp.getIField() != null) {
						argSets.add(bp.getIField().longValue());
					}
				}

				// if (fm.getKey("SRODEK_TRWALY") != null &&
				// StringUtils.isNotEmpty(fm.getKey("SRODEK_TRWALY").toString()))
				// {
				// if (!fm.getKey("SRODEK_TRWALY").toString().equals("1620")) {
				// argSets.add(0l);
				// }
				// }
				execution.setVariable("ids", new ArrayList<Long>(argSets));
				execution.setVariable("count", argSets.size());
				execution.take("budget_type");
			} else {
				execution.take("omit");
			}
		} catch (Exception e) {
			execution.take("omit");
			log.error(e.getMessage(),e);
		} finally {
			if (DSApi.isContextOpen() && !isOpened)
				DSApi.close();
		}
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
		// TODO Auto-generated method stub

	}

}
