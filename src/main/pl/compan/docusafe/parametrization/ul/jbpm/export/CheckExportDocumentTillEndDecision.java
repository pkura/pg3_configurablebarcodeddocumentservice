package pl.compan.docusafe.parametrization.ul.jbpm.export;

import static pl.compan.docusafe.parametrization.ul.jbpm.export.ExportDocument.DOC_GUID;
import static pl.compan.docusafe.parametrization.ul.jbpm.export.ExportDocument.EXPORT_REPEAT_COUNTER;
import static pl.compan.docusafe.parametrization.ul.jbpm.export.ExportDocument.EXPORT_RESULT_STATE;
import static pl.compan.docusafe.parametrization.ul.jbpm.export.ExportDocument.EXPORT_RESULT_STATE_NAME;

import java.util.Collections;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.exports.ExportHandler;
import pl.compan.docusafe.core.exports.RequestResult;
import pl.compan.docusafe.core.exports.SimpleErpExportFactory;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

@SuppressWarnings("serial")
public class CheckExportDocumentTillEndDecision implements DecisionHandler {
	
	private static final String FAST_REPEAT = "fast_repeat";
	private static final String REPEAT = "repeat";
	private static final String SUCCESS_TRANSITION = "success_transition";
	private static final String FAIL_TRANSITION = "fail_transition";
	private static final String DEFAULT_TRANSITION = FAIL_TRANSITION;
	
				//ZGL_BLEDU = ZGLOSZENIE_BLEDU, Formularz do przesyłania info o błędzie do admina (usługą lub przez człowieka)  ----------
				/** zgłoszenie błędu: ID statusu błędu nowego */				String ZGL_BLEDU_nowy = "11";
				/** zgłoszenie błędu: ID błędu będącego w obsłudze */ 			String ZGL_BLEDU_obslugiwany = "12";
				/** zgłoszenie błędu: ID błędu rozwiązanego */ 					String ZGL_BLEDU_rozwiazany = "13";
				/** zgłoszenie błędu: ID błędu odrzuconego */ 					String ZGL_BLEDU_odrzucony = "14";
				/** zgłoszenie błędu: ID błędu odsuniętego w czasie */ 			String ZGL_BLEDU_odsuniety= "15";
			
				/** Komunikat gotowy do pobrania przez szynę. */				String ERP_PRZESLANO_NA_SZYNE = "NEW"; 
				/** Komunikat został pobrany przez task */						String ESB_POBRANY_PRZEZ_SZYNE = "UPLOADED"; 
				/** Komunikat jest przetwarzany przez szynę danych */			String ESB_PRZETWARZANY_PRZEZ_SZYNE = "PROCESSING"; 
				/** Komunikat został poprawnie dodany do Simple.ERP */			String ESB_DODANO_DO_ERP = "FINISHED"; 
				/** Komunikat nie przeszedł wstępnej walidacji XSD */			String ESB_BLAD_WALIDACJI_PLIKU_XML = "INVALID"; 
				/** Komunikat nie został dodany do Simple.ERP system 
				 * zgłosił kod błędu */ 										String ESB_BLAD_WEWNETRZNY_ERP = "FAULT"; 
	
	private static final Logger log = LoggerFactory.getLogger(CheckExportDocumentTillEndDecision.class);

	private Integer fastRepeats;
	private String transitionToTake;
	

	protected void setResults(String result, Long docId) throws EdmException {
		try {
			DSApi.context().begin();
	        OfficeDocument doc = OfficeDocument.findOfficeDocument(docId);
	        doc.addRemark(new Remark(result, "admin"));
			DSApi.context().commit();
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
			DSApi.context().setRollbackOnly();
		}
	}
	
	protected void setFinishStatus(Long docId) throws EdmException {
		try {
			DSApi.context().begin();
			OfficeDocument.find(docId).getDocumentKind().setOnly(docId, Collections.singletonMap("STATUS", 2000), false);
			DSApi.context().commit();
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
			DSApi.context().setRollbackOnly();
		}
	}

	@Override
	public String decide(OpenExecution exec) {
		boolean isOpened = true;
		try {
			if (!DSApi.isContextOpen()) {
				isOpened = false;
				DSApi.openAdmin();
			}
			
			Long docId = Long.valueOf(exec.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			
			Integer counter = 1;
			if (exec.hasVariable(EXPORT_REPEAT_COUNTER)) {
				counter = (Integer) exec.getVariable(EXPORT_REPEAT_COUNTER);
			}
			if (counter + 1 > fastRepeats) {
				takeTransition(REPEAT);
			} else {
				takeTransition(FAST_REPEAT);
			}
			exec.setVariable(EXPORT_REPEAT_COUNTER, ++counter);

			String guid = "";
			
			if (exec.hasVariable(DOC_GUID)) {
				guid = exec.getVariable(DOC_GUID).toString();

				ExportHandler handler = new ExportHandler(new SimpleErpExportFactory());
				RequestResult result = handler.checkExport(guid);

				int state = result.getStateId();
				exec.setVariable(EXPORT_RESULT_STATE, state);

				String stateName = result.getStateName();
				exec.setVariable(EXPORT_RESULT_STATE_NAME, stateName);

				String requestResult = result.getResult();
				StringBuilder results = new StringBuilder();
				results.append("Guid dokumentu: " + guid + "\n");
				results.append("Nr stanu: " + state + "\n");
				results.append("Nazwa stanu: " + stateName + "\n");
				results.append("Odpowied� serwera: " + requestResult + "\n");

				if (ESB_BLAD_WEWNETRZNY_ERP.equals(stateName) || ESB_BLAD_WALIDACJI_PLIKU_XML.equals(stateName)) {
					log.error("B��d eksportu, counter: " + counter);
					log.error("B��d eksportu, guid: " + guid);
					log.error("B��d eksportu, docId: " + exec.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY));
					log.error("B��d eksportu, state: " + state);
					log.error("B��d eksportu, stateName: " + stateName);
					log.error("B��d eksportu, request result: " + requestResult);
					exec.removeVariable(EXPORT_REPEAT_COUNTER);
					
					takeTransition(FAIL_TRANSITION);

					results.append("B��d eksportu\n\n");
					if (docId != null) {
						setResults(results.toString(), docId);
					}
				} else if (ESB_DODANO_DO_ERP.equals(stateName)) {
					exec.removeVariable(EXPORT_REPEAT_COUNTER);
					
					takeTransition(SUCCESS_TRANSITION);
					
					setFinishStatus(docId);

					results.append("Eksport zako�czony pomy�lnie\n\n");
					if (docId != null) {
						setResults(results.toString(), docId);
					}
				}

			} else {
				log.error("B��d eksportu, guid: brak");
				log.error("B��d eksportu, docId: " + exec.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY));
				exec.removeVariable(EXPORT_REPEAT_COUNTER);
				takeTransition(FAIL_TRANSITION);
				
				setResults("B��d eksportu, guid: brak", docId);
			}
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
		} finally {
			if (DSApi.isContextOpen() && !isOpened)
				DSApi._close();
		}
		
		return getTakenTransition();
	}

	private void takeTransition(String transition) {
		transitionToTake = transition;
	}
	
	private String getTakenTransition() {
		return transitionToTake != null ? transitionToTake : DEFAULT_TRANSITION;
	}
}
