package pl.compan.docusafe.parametrization.ul.jbpm;

import com.google.common.collect.Maps;
import org.jbpm.api.activity.ActivityBehaviour;
import org.jbpm.api.activity.ActivityExecution;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.parametrization.ul.DelegationAbroadLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.HashMap;

/**
 * Created by kk on 05.03.14.
 */
public class CopyBudgetPositions implements ActivityBehaviour {
    protected static final Logger log = LoggerFactory.getLogger(CopyBudgetPositions.class);


    @Override
    public void execute(ActivityExecution execution) throws Exception {
        try {
            Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            HashMap<String, Object> ids = Maps.newHashMap();
            Document doc = Document.find(docId);
            ids.put(DelegationAbroadLogic.BP_ROZ_FIELD, doc.getFieldsManager().getKey(DelegationAbroadLogic.BP_FIELD));
            doc.getDocumentKind().setOnly(docId, ids);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            execution.takeDefaultTransition();
        }

    }
}
