package pl.compan.docusafe.parametrization.ul.jbpm;

import com.google.common.base.Joiner;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.ul.CostInvoiceLogic;
import pl.compan.docusafe.parametrization.ul.hib.UlBudgetPosition;
import pl.compan.docusafe.parametrization.ul.hib.UlObszaryMapping;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigInteger;
import java.util.*;

@SuppressWarnings("serial")
public class PrepareForMappedBudgetType implements ExternalActivityBehaviour {

	private static final Logger log = LoggerFactory.getLogger(PrepareForMappedBudgetType.class);
	
	@Override
	public void execute(ActivityExecution execution) throws Exception {
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");

		boolean isOpened = true;
		if (!DSApi.isContextOpen()) {
			isOpened = false;
			DSApi.openAdmin();
		}
		try {
			OfficeDocument doc = OfficeDocument.find(docId);

			Set<String> argSets = Sets.newHashSet();
			FieldsManager fm = doc.getFieldsManager();

			List<Long> dictionaryIds = (List<Long>) fm.getKey(CostInvoiceLogic.BP_FIELD);
			if (dictionaryIds != null) {
				UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
                StringBuilder info = new StringBuilder();
				for (Long mpkId : dictionaryIds) {
					UlBudgetPosition bp = bpInstance.find(mpkId);

					LinkedHashMap<String, String> fieldsValues = new LinkedHashMap<String, String>();
					fieldsValues.put("c_project", getCnFromFieldId(bp.getCProject(), CostInvoiceLogic.PROJECTS_TABLE_NAME));
					fieldsValues.put("c_type", getCnFromFieldId(bp.getCType(), CostInvoiceLogic.PRODUCT_TABLE_NAME));
					fieldsValues.put("i_kind", getCnFromFieldId(bp.getIKind(), CostInvoiceLogic.RODZAJ_DZIALALNOSCI_TABLE_NAME));
					fieldsValues.put("c_mpk", getCnFromFieldId(bp.getCMpk(), CostInvoiceLogic.ORGANIZATIONAL_UNIT_TABLE_NAME));
					fieldsValues.put("c_fundsource", getCnFromFieldId(bp.getCFundSource(), CostInvoiceLogic.FUND_SOURCES_TABLE_NAME));
					fieldsValues.put("c_inventory", getCnFromFieldId(bp.getCInventory(), CostInvoiceLogic.INVENTORY_REPO_TABLE_NAME));

                    info.append("BP id: "+mpkId);
                    info.append(fieldsValues);

					String[] fieldKeys = fieldsValues.keySet().toArray(new String[0]);
					
					Multimap<Integer, String> sequence = generateSequenceGrouped(fieldsValues.size());
					
					Set<String> foundUserNames = Sets.newTreeSet();

					for (int i = fieldsValues.size(); i > 0; i--) {
						if (sequence.get(i) != null) {
							for (String condition : sequence.get(i)) {
								List<UlObszaryMapping> results = UlObszaryMapping.findWithMappingSettings(condition, fieldKeys, fieldsValues);

								if (results != null) {
									for (UlObszaryMapping obszar : results) {
										if (obszar.getDsuserLong() == null) {
											log.error("Brak userId dla wpisu mapującego obszary faktury;");
										} else {
											try {
												String username = DSUser.findById(obszar.getDsuserLong()).getName();
												foundUserNames.add(username);
											} catch (EdmException e) {
												log.error(e.getMessage(), e);
											}
										}
									}
								}

							}

							if (!foundUserNames.isEmpty()) {
								argSets.add(Joiner.on(",").join(foundUserNames));
								break;
							}
						}
					}
					
				}
				
				if (argSets.isEmpty()) {
					throw new EdmException("Nie znaleziono żadnego użytkownika w tabelach mapujących na obszary faktury. Omijam krok. "+info);
				}

				execution.setVariable("budgetTypeUsers", new ArrayList<String>(argSets));
				execution.setVariable("count", argSets.size());
				execution.take("budget_type");
			} else {
				execution.take("omit");
			}
		} catch (Exception e) {
			execution.take("omit");
			log.error(e.getMessage(),e);
		} finally {
			if (DSApi.isContextOpen() && !isOpened)
				DSApi.close();
		}
	}

	private String getStringSeqValue(Integer value, int noOfParams) {
		return StringUtils.leftPad(Integer.toBinaryString(value), noOfParams, '0');
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
		// TODO Auto-generated method stub

	}
	
	private Integer getField(Integer field) {
		if (field != null && !field.equals(-1)) {
			return field;
		} else {
			return null;
		}
	}

	private String getCnFromFieldId(Integer field, String table) {
        if (field != null) {
            try {
                return DataBaseEnumField.getEnumItemForTable(table, field).getCn();
            } catch (Exception e) {
                log.error(e.getMessage(),e);
            }
        }

        return null;
    }
	
	private Multimap<Integer, String> generateSequenceGrouped(int noOfParams) {
		Multimap<Integer, String> result = ArrayListMultimap.create();
		int i = (1 << noOfParams);
		Integer[] table = new Integer[i-1];

		do {
			i--;
			table[i-1] = i;
		} while (i > 1);
		
		for (Integer val : table) {
			result.put(BigInteger.valueOf(val).bitCount(), getStringSeqValue(val,noOfParams));
		}
		
		return result;
	}
	
}
