package pl.compan.docusafe.parametrization.ul.jbpm;

import java.util.List;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ul.CostInvoiceLogic;
import pl.compan.docusafe.parametrization.ul.hib.UlBudgetPosition;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

@SuppressWarnings("serial")
public class IsCosRiTSAcceptDecisionHandler implements DecisionHandler {
	private final static Logger LOG = LoggerFactory.getLogger(IsCosRiTSAcceptDecisionHandler.class);

    public String decide(OpenExecution execution) {
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");

		try {
			boolean isOpened = true;
			if (!DSApi.isContextOpen()) {
				isOpened = false;
				DSApi.openAdmin();
			}
			try {
				OfficeDocument doc = OfficeDocument.find(docId);
				FieldsManager fm = doc.getFieldsManager();

				List<Long> dictionaryIds = (List<Long>) fm.getKey(CostInvoiceLogic.BP_FIELD);
				if (dictionaryIds != null) {
					UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
					for (Long bpId : dictionaryIds) {
						UlBudgetPosition bp = bpInstance.find(bpId);
						if (bp.getSKind() != null && bp.getSKind().equals(CostInvoiceLogic.FAKTURA_SOCIAL_KN_ID)) {
							return "true";
						}
					}
				} 
			} finally {
				if (DSApi.isContextOpen() && !isOpened) {
					DSApi.close();
				}
			}
		} catch (EdmException e) {
			LOG.error(e.getMessage(), e);
			return "error";
		}
		return "false";
	}
}

