package pl.compan.docusafe.parametrization.ul;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.process.InstantiationParameters;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.jbpm4.ActivitiesExtensionProcessView;
import pl.compan.docusafe.process.form.ActivityController;
import pl.compan.docusafe.process.form.SelectItem;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessInstance;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.web.common.ExtendedRenderBean;

import java.util.ArrayList;
import java.util.List;

public class AcceptancesSelectProcessViewForDelegation extends ActivitiesExtensionProcessView {

    public AcceptancesSelectProcessViewForDelegation(InstantiationParameters ip) throws Exception {
        super(ip);
    }

    @Override
    public ExtendedRenderBean render(ProcessInstance pi, ProcessActionContext context) throws EdmException {
        return render((Jbpm4ProcessInstance) pi, context);
    }

    private ExtendedRenderBean render(Jbpm4ProcessInstance pi, ProcessActionContext context) throws EdmException {
        ExtendedRenderBean ret = super.render(pi, context);

        List<String> mpkAcceptationNames = new ArrayList<String>();
        mpkAcceptationNames.add(CostInvoiceLogic.BUDGET_OWNER_ACCEPT_TASK_NAME);

        Long docId = Jbpm4ProcessLocator.documentIdForExtenedProcesssId(((Jbpm4ProcessInstance) pi).getProcessInstanceId());

        List<DocumentAcceptance> acceptances = DocumentAcceptance.find(docId);

        Field docProcesses = Document.find(docId).getDocumentKind().getFieldByCn("STATUS");

        String docStatus = Document.find(docId).getFieldsManager().getEnumItemCn("STATUS");
        boolean statementOut = docStatus.endsWith(AbstractDelegationLogic.STATEMENT_OUT_TASK_SUFFIX);
        List<SelectItem> selectItems = new ArrayList<SelectItem>();
        for (DocumentAcceptance itemAcceptance : acceptances) {
            itemAcceptance.initDescription();

            if (statementOut && !itemAcceptance.getAcceptanceCn().endsWith(AbstractDelegationLogic.STATEMENT_OUT_TASK_SUFFIX)) {
                continue;
            }

            if (itemAcceptance.getAcceptanceCn().equals(docStatus)) { // nie pokazywanie na li�cie akceptacji dla takiego samego etapu, co aktualnie wykonywany
                continue;
            }
            selectItems.add(ActivityController.createSelectItem(itemAcceptance.getId(), itemAcceptance.getAsFirstnameLastname(),
                    itemAcceptance.getUsername(), docProcesses.getEnumItemByCn(itemAcceptance.getAcceptanceCn()).getTitle(),
                    itemAcceptance.getAcceptanceCn()));
        }

        ret.putValue("selectItems", selectItems);

        return ret;
    }
}