package pl.compan.docusafe.parametrization.ul.services;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.Set;

import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.parametrization.ul.hib.EnumItem;
import pl.compan.docusafe.parametrization.ul.hib.ProjectsEnumItem;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.GetProjects;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.Project;

import com.google.common.collect.Sets;

public class ProjectsImport extends AbstractDictionaryImport {

	private static final String SERVICE_TABLE_NAME = "dsg_ul_services_projects";

	private static final String TOP_VALUE = "1000";

	protected static Logger log = LoggerFactory.getLogger(ProjectsImport.class);

	private static final String SERVICE_PATH = "/services/DictionaryServicesProjects";

	private static final String ENUM_VIEW_NAME = "dsg_ul_view_projects";
	private static final String ENUM_TABLE_NAME = "dsg_ul_projects";
	private DictionaryServicesProjectsStub stub;

	@Override
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServicesProjectsStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);

	}

	@Override
	public boolean doImport() throws RemoteException, EdmException {
		log.debug("UL:00011 Start IMPORT Projects");
		if (stub != null) {

			// PROJEKT
			GetProjects params = new GetProjects();
			// params.setTop(TOP_VALUE);
			pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.GetProjectsResponse response;
			if (params != null && (response = stub.getProjects(params)) != null) {
				Project[] items = response.get_return();
				if (items != null) {
					EnumItem finder = EnumItem.getInstance();
					Set<Long> itemIds = Sets.newHashSet();

					for (Project item : items) {
						if (item != null) {
							try {
								EnumItem found = finder.findErpId(ProjectsEnumItem.class, item.getKontraktId());
								log.debug("UL:00011 Start IMPORT Projects znalazl w bazie item" + found.toString());
								// aktualnie kod 6 cyfrowy, lecz na wszelki
								// zostawiam
								if ((long) Integer.MAX_VALUE < item.getKierownikProjektuId()) {
									log.error("Id kierownika projektu poza INTem");
									found.setCentrum(0);
								} else {
									found.setCentrum((int) item.getKierownikProjektuId());
								}
                                found.setRefValue(item.getKomorkaId());
								found.setAvailable(true);
								found.setCn(item.getKontraktIdm());
								found.setTitle(item.getNazwa() != null ? item.getNazwa().trim() : null);
								found.save();
								itemIds.add(found.getId());
							} catch (EntityNotFoundException e) {
								log.debug("UL:00014 Start IMPORT Projects nie znalazl w bazie item");
								EnumItem newItem = new ProjectsEnumItem();

								if ((long) Integer.MAX_VALUE < item.getKierownikProjektuId()) {
									log.error("Id kierownika projektu poza INTem");
									newItem.setCentrum(0);
								} else {
									newItem.setCentrum((int) item.getKierownikProjektuId());
								}

								newItem.setErpId(item.getKontraktId());
								newItem.setAvailable(true);
								newItem.setCn(item.getKontraktIdm());
								newItem.setTitle(item.getNazwa() != null ? item.getNazwa().trim() : null);
								newItem.setRefValue(item.getKomorkaId());
								newItem.save();
								itemIds.add(newItem.getId());
								log.debug("UL:00011 Start IMPORT Projects doda� item" + newItem.toString());
							}
						} else
							log.debug("UL:00013 Start IMPORT Projects item is null");
					}

					int updated = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME);
					log.debug("Projects disabled: " + updated);
				} else
					log.debug("UL:00012 Start IMPORT Projects items is null");
			}
		}
        return true;
	}
	@Override
	public void materializeView() throws EdmException, SQLException {
		//ServicesUtils.deleteRowsInsertFromEnumView(ENUM_TABLE_NAME, ENUM_VIEW_NAME);
		DataBaseEnumField.reloadForTable(ENUM_TABLE_NAME);
	}
}
