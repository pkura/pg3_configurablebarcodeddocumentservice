package pl.compan.docusafe.parametrization.ul.services;

import java.rmi.RemoteException;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.parametrization.ul.hib.ExchangeRate;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ul.DictionaryServicesExchangeRateTablesStub;
import pl.compan.docusafe.ws.ul.DictionaryServicesExchangeRateTablesStub.ExchangeRateTable;
import pl.compan.docusafe.ws.ul.DictionaryServicesExchangeRateTablesStub.GetExchangeRateTables;

public class ExchangeRateImport extends AbstractDictionaryImport {
	private static final String SERVICE_PATH = "/services/DictionaryServicesExchangeRateTables";
    private static final String ENUM_TABLE_NAME = "dsg_ul_exchange_rate";
    DictionaryServicesExchangeRateTablesStub stub;
	protected static Logger log = LoggerFactory.getLogger(ExchangeRateImport.class);
	
	@Override
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServicesExchangeRateTablesStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);
	}

    @Override
    public void finalizeImport() throws EdmException {
        DataBaseEnumField.reloadForTable(ENUM_TABLE_NAME);
    }

    @Override
	public boolean doImport() throws RemoteException, EdmException {
		if (stub != null) {
			GetExchangeRateTables params = new GetExchangeRateTables();
			//params.setTop("50");
			pl.compan.docusafe.ws.ul.DictionaryServicesExchangeRateTablesStub.GetExchangeRateTablesResponse response;
			if (params != null && (response = stub.getExchangeRateTables(params)) != null) {
				ExchangeRateTable[] items = response.get_return();
				if (items != null) {
					for (ExchangeRateTable item : items) {
						List<ExchangeRate> foundList = ExchangeRate.find(item.getWalutaNomId(), item.getDatod(), item.getDatdo());
						
						if (!foundList.isEmpty()) {
							if (foundList.size()>1) {
								log.error("B�ad, znaleziono wi�cej ni� 1 pozycj� kursow� przeznaczon� do zaaktualizowania");
							}
							for (ExchangeRate found : foundList) {
								found.setAllFieldsFromServiceObject(item);
								found.save();
							}
						} else {
							ExchangeRate newItem = new ExchangeRate();
							newItem.setAllFieldsFromServiceObject(item);
							newItem.save();
						}
					}
				}
			}
		}
        return true;
	}
}

