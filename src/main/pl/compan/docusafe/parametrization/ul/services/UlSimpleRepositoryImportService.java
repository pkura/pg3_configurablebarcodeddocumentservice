package pl.compan.docusafe.parametrization.ul.services;

import java.util.List;

import com.google.common.base.Strings;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.imports.simple.repository.SimpleRepositoryImportService;
import pl.compan.docusafe.parametrization.ul.CostInvoiceLogic;
import pl.compan.docusafe.parametrization.ul.hib.DocumentTypes;

import com.google.common.collect.Lists;

public class UlSimpleRepositoryImportService extends SimpleRepositoryImportService {

    @Override
	public String getDockindCn() throws EdmException {
		return CostInvoiceLogic.DOC_KIND_CN;
	}

	@Override
	public Long getContextId() throws EdmException {
		return CostInvoiceLogic.INVOICE_POSITION_CONTEXT_ID;
	}

    @Override
    public Long getDictIdToOmitDictionariesUpdate() throws EdmException {
        String raw = Docusafe.getAdditionProperty("import.repository.dict_to_omit");
        if (!Strings.isNullOrEmpty(raw)) {
           return Long.valueOf(raw);
        }

        return 187l;
    }

    @Override
	public List<Long> getObjectIds() throws EdmException {
		List<DocumentTypes> documentTypes = DocumentTypes.findAvailable(true);
		List<Long> objectIds = Lists.newLinkedList();
		
		for (DocumentTypes type : documentTypes) {
			objectIds.add(type.getTypdokzakId());
		}
		return objectIds;
	}

	@Override
	public String getDynamicDictionaryCn() throws EdmException {
		return CostInvoiceLogic.BP_FIELD;
	}
}
