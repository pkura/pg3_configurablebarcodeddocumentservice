package pl.compan.docusafe.parametrization.ul.services;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.parametrization.ul.hib.StructureBudget;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgetsStub;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

/**
 * User: kk
 * Date: 23.12.13
 * Time: 14:10
 */
public class StructureBudgetImport extends AbstractDictionaryImport {

    protected static Logger log = LoggerFactory.getLogger(StructureBudgetImport.class);

    private static final String SERVICE_PATH = "/services/DictionaryServicesUniversityStructureBudgets";
    private DictionaryServicesUniversityStructureBudgetsStub stub;
    private StringBuilder message;
    private int importPageSize;
    private int importPageNumber;
    private Set<Long> itemIds;

    @Override
    public void initImport() {
        try {
            importPageNumber = Integer.valueOf(Docusafe.getAdditionProperty("import.structure_budgets.start_page_number"));
        } catch (Exception e) {
            importPageNumber = 1;
        }

        try {
            importPageSize = Integer.valueOf(Docusafe.getAdditionProperty("import.structure_budgets.page_size"));
        } catch (Exception e) {
            importPageSize = 1000;
        }

        itemIds = Sets.newHashSet();
    }

    @Override
    public void initConfiguration(AxisClientConfigurator conf) throws Exception {
        stub = new DictionaryServicesUniversityStructureBudgetsStub(conf.getAxisConfigurationContext());
        conf.setUpHttpParameters(stub, SERVICE_PATH);
    }

    @Override
    public boolean doImport() throws RemoteException, EdmException {
        message = new StringBuilder();
        if (stub == null) {
            throw new EdmException("Null stub");
        }

        DictionaryServicesUniversityStructureBudgetsStub.GetUniversityStructureBudgets params = new DictionaryServicesUniversityStructureBudgetsStub.GetUniversityStructureBudgets();
        params.setPageSize(importPageSize);
        message.append("rozmiar strony: " + importPageSize);
        message.append(", nr strony: " + importPageNumber);
        params.setPage(importPageNumber++);
        DictionaryServicesUniversityStructureBudgetsStub.GetUniversityStructureBudgetsResponse response = stub.getUniversityStructureBudgets(params);

        Preconditions.checkNotNull(response, "ws doesnt response");

        DictionaryServicesUniversityStructureBudgetsStub.UniversityStructureBudget[] items = response.get_return();
        if (items != null && items.length > 0) {
            for (DictionaryServicesUniversityStructureBudgetsStub.UniversityStructureBudget item : items) {
                if (item != null) {
                    List<StructureBudget> founds = DictionaryUtils.findByGivenFieldValues(StructureBudget.class, ImmutableMap.of("bdBudzetId", (Object) item.getBd_budzet_id(), "bdBudzetRodzajId", item.getBd_budzet_rodzaj_id(), "dokBdStrBudzetIdn", item.getDok_bd_str_budzet_idn()));

                    if (!founds.isEmpty()) {
                        for (StructureBudget found : founds) {
                            found.setAllFieldsFromServiceObject(item).setAvailable(true).save();

                            itemIds.add(found.getId());
                        }
                    } else {
                        StructureBudget map = new StructureBudget();
                        map.setAllFieldsFromServiceObject(item).setAvailable(true).save();

                        itemIds.add(map.getId());
                    }

                }
            }

            return false; // import niezakończony
        } else {
            return true; // import zakończony
        }

    }

    @Override
    public String getMessage() {
        return message.toString();
    }

    @Override
    public void finalizeImport() throws EdmException {
        if (Docusafe.getAdditionProperty("import.structure_budgets.start_page_number") == null || "1".equals(Docusafe.getAdditionProperty("import.structure_budgets.start_page_number"))) {
            int updated = ServicesUtils.disableDeprecatedItem(itemIds, StructureBudget.TABLE_NAME);
            log.debug("Resources disabled: "+updated);
        }
    }

    @Override
    public void materializeView() throws EdmException, SQLException {
        DataBaseEnumField.reloadForTable(StructureBudget.STRUCTURE_BUDGET_KO_VIEW_NAME);
        DataBaseEnumField.reloadForTable(StructureBudget.STRUCTURE_BUDGET_POSITION_VIEW_NAME);
    }
}
