package pl.compan.docusafe.parametrization.ul.services;

import com.google.common.collect.Sets;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.parametrization.ul.hib.OrderDocumentTypes;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.simple.ul.DictionaryServicesDocumentTypesStub;
import pl.compan.docusafe.ws.simple.ul.DictionaryServicesDocumentTypesStub.DeliveryRequestType;
import pl.compan.docusafe.ws.simple.ul.DictionaryServicesDocumentTypesStub.GetDeliveryRequestType;
import pl.compan.docusafe.ws.simple.ul.DictionaryServicesDocumentTypesStub.GetDeliveryRequestTypeResponse;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;

public class OrderDocumentTypesImport extends AbstractDictionaryImport {
	private static final String SERVICE_PATH = "/services/DictionaryServicesDocumentTypes";
	private static final String SERVICE_TABLE_NAME = "dsg_ul_services_order_document_types";
    private static final String ENUM_TABLE_NAME = "dsg_ul_order_document_types";
    DictionaryServicesDocumentTypesStub stub;
	protected static Logger log = LoggerFactory.getLogger(OrderDocumentTypesImport.class);

	private Set<Long> itemIds;	
	
	@Override
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServicesDocumentTypesStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);
	}

	@Override
	public void initImport() {
		itemIds = Sets.newHashSet();
	}		
	
	@Override
	public boolean doImport() throws RemoteException, EdmException {
		if (stub != null) {
            GetDeliveryRequestType params = new GetDeliveryRequestType();
            GetDeliveryRequestTypeResponse response;
			if (params != null && (response = stub.getDeliveryRequestType(params)) != null) {
				DeliveryRequestType[] items = response.get_return();
				if (items != null) {
					for (DeliveryRequestType item : items) {
						if (item != null) {
							List<OrderDocumentTypes> found = DictionaryUtils.findByGivenFieldValue(OrderDocumentTypes.class, "typZapdostId", item.getTypZapdostId());
							if (!found.isEmpty()) {
								if (found.size() > 1) {
									log.error("Znaleziono wi�cej ni� 1 wpis o typdokzakId: " + item.getTypZapdostId());
								}
								for (OrderDocumentTypes type : found) {
									type.setAllFieldsFromServiceObject(item);
									type.setAvailable(true);
									type.save();

									itemIds.add(Long.valueOf(type.getId()));
								}
							} else {
                                OrderDocumentTypes type = new OrderDocumentTypes();
								type.setAllFieldsFromServiceObject(item);
								type.setAvailable(true);
								type.save();

								itemIds.add(Long.valueOf(type.getId()));
							}
						}
					}
				}
			}
		}
        return true;
	}
	@Override
	public void finalizeImport() throws EdmException {
		int updated = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME);
        DataBaseEnumField.reloadForTable(ENUM_TABLE_NAME);
	}
}