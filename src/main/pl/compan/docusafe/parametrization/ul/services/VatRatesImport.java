package pl.compan.docusafe.parametrization.ul.services;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.parametrization.ul.hib.EnumItem;
import pl.compan.docusafe.parametrization.ul.hib.VatRatesEnumItem;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ul.DictionaryServicesVatRatesStub;
import pl.compan.docusafe.ws.ul.DictionaryServicesVatRatesStub.GetVatRates;
import pl.compan.docusafe.ws.ul.DictionaryServicesVatRatesStub.VatRate;
import pl.compan.docusafe.ws.ul.DictionaryServicesVatRatesStub.GetVatRatesResponse;

import com.google.common.collect.Sets;

public class VatRatesImport extends AbstractDictionaryImport {
	private static final String SERVICE_PATH = "/services/DictionaryServicesVatRates";
    private static final String ENUM_TABLE_NAME = "dsg_ul_vat_rates";
    DictionaryServicesVatRatesStub stub;
	
	@Override
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServicesVatRatesStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);
	}

	@Override
	public boolean doImport() throws RemoteException, EdmException {
		if (stub != null) {
			GetVatRates params = new GetVatRates();
			GetVatRatesResponse response;
			if (params != null && (response = stub.getVatRates(params)) != null) {
				VatRate[] items = response.get_return();
				if (items != null) {
					EnumItem finder = EnumItem.getInstance();
					Set<Long> rateIds = Sets.newHashSet();

					for (VatRate item : items) {
						if (item != null) {
							rateIds.add(item.getVatstawId());
                            EnumItem enumItem = null;
                            try {
                                enumItem = finder.findErpId(VatRatesEnumItem.class, item.getVatstawId());
                            } catch (EdmException e) {
                                enumItem = new VatRatesEnumItem();
                                enumItem.setErpId(item.getVatstawId());
                            }

                            if (enumItem != null) {
                                enumItem.setAvailable(true);
                                enumItem.setCentrum((int) item.getIdentyfikatorNum());
                                enumItem.setCn(item.getVatstawIds() != null ? item.getVatstawIds().toString().trim() : null);
                                enumItem.setTitle(item.getNazwa() != null ? (item.getNazwa().trim() + " (" + item.getIdentyfikatorNum() + "%)") : null);
                                enumItem.save();
                            }
						}
					}

					// available na 0 dla pozycji nie wyst�puj�cych ju� w erpie
					List<EnumItem> itemsNotFromErp = finder.findByErpId(VatRatesEnumItem.class, rateIds, true);

					for (EnumItem item : itemsNotFromErp) {
						item.setAvailable(false);
						item.save();
					}
				}
			}
		}
        return true;
	}

    @Override
    public void finalizeImport() throws EdmException {
        DataBaseEnumField.reloadForTable(ENUM_TABLE_NAME);
    }
}
