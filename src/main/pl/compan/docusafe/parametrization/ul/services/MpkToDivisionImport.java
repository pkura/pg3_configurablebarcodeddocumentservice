package pl.compan.docusafe.parametrization.ul.services;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryWithoutWebserviceImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 12.08.13
 * Time: 15:31
 * To change this template use File | Settings | File Templates.
 */
public class MpkToDivisionImport extends AbstractDictionaryWithoutWebserviceImport {
    protected static Logger log = LoggerFactory.getLogger(MpkToDivisionImport.class);

    private Connection connection;

    @Override
    public void initImport() {
        try {
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://" + getHostname() + "/" + getDbName(), getUsername(), getPassword());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public boolean doImport() throws RemoteException, EdmException {
        if (connection != null) {
            PreparedStatement ps = null;
            Statement pgStmt = null;
            Statement stmt = null;
            try {
                pgStmt = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                ResultSet pgRs = pgStmt.executeQuery("select j.kod, m.kod from mpk_jednostki_eod mj join mpk m on mj.id_mpk=m.id join jednostki_eod j on mj.id_jednostki_eod=j.id");

                stmt = DSApi.context().createStatement();
                stmt.executeUpdate("delete from dsg_ul_mpk_mapping");
                ps = DSApi.context().prepareStatement("insert into dsg_ul_mpk_mapping (ds_div_cn, mpk_cn) values (?,?)");

                while (pgRs.next()) {
                   ps.setString(1, pgRs.getString(1));
                   ps.setString(2, pgRs.getString(2));

                   if (pgRs.getRow() % 2000 != 0 && !pgRs.isLast()) {
                       ps.addBatch();
                   } else {
                       ps.addBatch();
                       ps.executeBatch();
                   }
                }

            } catch (SQLException e) {
                log.error(e.getMessage(), e);
                throw new EdmException(e.getMessage());
            } finally {
                if (ps != null && stmt != null) {
                   DSApi.context().closeStatements(ps,stmt);
                }
                if (pgStmt != null) {
                    try {
                        pgStmt.close();
                    } catch (SQLException e) {
                        log.error(e.getMessage(), e);
                    }
                }
            }

        } else {
           throw new EdmException("Brak po��czenia z baz�");
        }

        return true;
    }

    @Override
    public void finalizeImport() throws EdmException {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    protected String getDbName() {
        return Docusafe.getAdditionProperty("import.mpk2division.dbName");
    }

    protected String getHostname() {
        return Docusafe.getAdditionProperty("import.mpk2division.hostname");
    }

    protected String getUsername() {
        return Docusafe.getAdditionProperty("import.mpk2division.username");
    }

    protected String getPassword() {
        return Docusafe.getAdditionProperty("import.mpk2division.password");
    }
}
