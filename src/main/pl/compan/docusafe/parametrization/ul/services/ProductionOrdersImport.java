package pl.compan.docusafe.parametrization.ul.services;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.parametrization.ul.hib.ProductionOrders;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ul.DictionaryServicesProductionOrdersStub;
import pl.compan.docusafe.ws.ul.DictionaryServicesProductionOrdersStub.GetProductionOrdersPagged;
import pl.compan.docusafe.ws.ul.DictionaryServicesProductionOrdersStub.ProductionOrder;

import com.google.common.collect.Sets;

public class ProductionOrdersImport extends AbstractDictionaryImport {
	
	private static final long PAGE_SIZE = 100;
	
	private int importPageNumber;
	private Set<Long> itemIds;

	protected static Logger log = LoggerFactory.getLogger(ProductionOrdersImport.class);
	
	private static final String SERVICE_PATH = "/services/DictionaryServicesProductionOrders";
	private static final String SERVICE_TABLE_NAME = "dsg_ul_services_production_orders";
	private static final String ENUM_VIEW_NAME = "dsg_ul_view_production_orders";
	private static final String ENUM_TABLE_NAME = "dsg_ul_production_orders";	
	private DictionaryServicesProductionOrdersStub stub;

	@Override
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServicesProductionOrdersStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);

	}

	@Override
	public boolean doImport() throws RemoteException, EdmException {
		log.debug("UL:00011 Start IMPORT GetProductionOrdersPagged");
		if (stub != null) {
			log.debug("UL:00003 START Dodaje Zasoby GetProductionOrdersPagged");
			
			GetProductionOrdersPagged params = new GetProductionOrdersPagged();
			params.setPageSize(PAGE_SIZE);
			
			params.setPage(importPageNumber++);
			pl.compan.docusafe.ws.ul.DictionaryServicesProductionOrdersStub.GetProductionOrdersPaggedResponse response;
			if ((response = stub.getProductionOrdersPagged(params)) != null) {
				ProductionOrder[] items = response.get_return();
				if (items != null && items.length > 0) {
					for (ProductionOrder item : items) {
						if (item != null) {
							List<ProductionOrders> found = DictionaryUtils.findByGivenFieldValue(ProductionOrders.class, "zlecprodId", item.getZlecprodId());
							if (!found.isEmpty()) {
								if (found.size() > 1) {
									log.error("Znaleziono wi�cej ni� 1 wpis o zlecprodId: " + item.getZlecprodId());
								}
								for (ProductionOrders order : found) {
									order.setAllFieldsFromServiceObject(item);
									order.setAvailable(true);
									order.save();

									itemIds.add(order.getId());
								}
							} else {
								ProductionOrders order = new ProductionOrders();
								order.setAllFieldsFromServiceObject(item);
								order.setAvailable(true);
								order.save();

								itemIds.add(order.getId());
							}
						}
					}
					return false; // import niezako�czony
				} else {
					return true; // import zako�czony
				}
			} else {
				return true;
			}
		}
		return true;
	}

	@Override
	public void initImport() {
		try {
			importPageNumber = Integer.valueOf(Docusafe.getAdditionProperty("import.orders.start_page_number"));
		} catch (Exception e) {
			importPageNumber = 1;
		}
		
		itemIds = Sets.newHashSet();
	}

	@Override
	public void finalizeImport() throws EdmException {
		int updated = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME);
		log.debug("Resources disabled: "+updated);
	}

	@Override
	public void materializeView() throws EdmException, SQLException {
		//ServicesUtils.deleteRowsInsertFromEnumView(ENUM_TABLE_NAME, ENUM_VIEW_NAME);
		DataBaseEnumField.reloadForTable(ENUM_TABLE_NAME);
	}
}
