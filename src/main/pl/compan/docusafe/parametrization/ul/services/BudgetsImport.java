package pl.compan.docusafe.parametrization.ul.services;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.Set;

import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.parametrization.ul.hib.EnumItem;
import pl.compan.docusafe.parametrization.ul.hib.ProjectBudgetsEnumItem;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.GetProjectBudgets;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.ProjectBudget;

import com.google.common.collect.Sets;

public class BudgetsImport extends AbstractDictionaryImport {
	
	private static final String SERVICE_TABLE_NAME = "dsg_ul_services_project_budgets";

	private static final String TOP_VALUE = "1000";

	protected static Logger log = LoggerFactory.getLogger(BudgetsImport.class);
	
	private static final String SERVICE_PATH = "/services/DictionaryServicesProjects";
	private static final String ENUM_VIEW_NAME = "dsg_ul_view_project_budgets";
	private static final String ENUM_TABLE_NAME = "dsg_ul_project_budgets";
	private DictionaryServicesProjectsStub stub;

	@Override
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServicesProjectsStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);
	}

	@Override
	public boolean doImport() throws RemoteException, EdmException {
		log.debug("UL:00011 Start IMPORT Projects");
		if (stub != null) {
			
			log.debug("UL:00011 Start IMPORT Budgets");
			//BUDZET
			GetProjectBudgets paramsBudgets = new GetProjectBudgets();
			//paramsBudgets.setTop(TOP_VALUE);
			pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.GetProjectBudgetsResponse response;
			if (paramsBudgets != null && (response = stub.getProjectBudgets(paramsBudgets)) != null) {
				ProjectBudget[] items = response.get_return();
				if (items != null) {
					EnumItem finder = EnumItem.getInstance();
					Set<Long> itemIds = Sets.newHashSet();

					for (ProjectBudget item : items) {
						if (item != null) {
							try {
								EnumItem found = finder.findErpId(ProjectBudgetsEnumItem.class, item.getProjektId());
								found.setAvailable(true);
								found.setCn(item.getProjektIdm());
								found.setTitle(item.getNazwa() != null ? item.getNazwa().trim() : null);
                                found.setRefValue(item.getKontraktId());
                                found.save();
								itemIds.add(found.getId());
								log.debug("UL:00021 Start IMPORT Budgets zmodyfikowa� "+found.toString());
							} catch (EntityNotFoundException e) {
								EnumItem newItem = new ProjectBudgetsEnumItem();
								newItem.setErpId(item.getProjektId());
								newItem.setAvailable(true);
								newItem.setCn(item.getProjektIdm());
								newItem.setTitle(item.getNazwa() != null ? item.getNazwa().trim() : null);
								newItem.setRefValue(item.getKontraktId());
								newItem.save();
								itemIds.add(newItem.getId());
								log.debug("UL:00021 Start IMPORT Budgets doda� "+newItem.toString());
							}
						}
					}
					
					int updated = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME);
					log.debug("Budgets disabled: "+updated);
				}
			}
			
		}
        return true;
	}

	@Override
	public void materializeView() throws EdmException, SQLException {
		//ServicesUtils.deleteRowsInsertFromEnumView(ENUM_TABLE_NAME, ENUM_VIEW_NAME);
		DataBaseEnumField.reloadForTable(ENUM_TABLE_NAME);
	}
}
