package pl.compan.docusafe.parametrization.ul.services;

import com.google.common.collect.Sets;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.ul.hib.ContractorAccount;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ul.DictionaryServicesSupplierStub;
import pl.compan.docusafe.ws.ul.DictionaryServicesSupplierStub.GetSuppliersBankAccounts;
import pl.compan.docusafe.ws.ul.DictionaryServicesSupplierStub.SupplierBankAccount;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

public class ContractorAccountImport extends AbstractDictionaryImport {
	private static final String SERVICE_PATH = "/services/DictionaryServicesSupplier";
	private static final String SERVICE_TABLE_NAME = "dsg_ul_services_contractor_account";
	public static final String ENUM_VIEW_NAME = "dsg_ul_view_contractor_account";
	private static final String ENUM_TABLE_NAME = "dsg_ul_contractor_account";	
	DictionaryServicesSupplierStub stub;
	protected Logger log = LoggerFactory.getLogger(ContractorAccountImport.class);

	private Set<Long> itemIds;	
	
	@Override
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServicesSupplierStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);
	}

	@Override
	public void initImport() {
		itemIds = Sets.newHashSet();
	}		
	
	@Override
	public boolean doImport() throws RemoteException, EdmException {
		if (stub != null) {
			stub._getServiceClient().cleanupTransport();
			GetSuppliersBankAccounts params = new GetSuppliersBankAccounts();
			//params.setTop("");
			pl.compan.docusafe.ws.ul.DictionaryServicesSupplierStub.GetSuppliersBankAccountsResponse response;
			if (params != null && (response = stub.getSuppliersBankAccounts(params)) != null) {
				SupplierBankAccount[] items = response.get_return();
				if (items != null) {
					for (SupplierBankAccount item : items) {
						if (item != null) {
							List<ContractorAccount> found = DictionaryUtils.findByGivenFieldValue(ContractorAccount.class, "kontobankoweId", item.getKontobankoweId());
							if (!found.isEmpty()) {
								if (found.size() > 1) {
									log.error("Znaleziono wi�cej ni� 1 wpis o wytworId: " + item.getKontobankoweId());
								}
								for (ContractorAccount resource : found) {
									resource.setAllFieldsFromServiceObject(item);
									resource.setAvailable(true);
									resource.save();

									itemIds.add(Long.valueOf(resource.getId()));
								}
							} else {
								ContractorAccount resource = new ContractorAccount();
								resource.setAllFieldsFromServiceObject(item);
								resource.setAvailable(true);
								resource.save();

								itemIds.add(Long.valueOf(resource.getId()));
							}
						}
					}
				}
			}
		}
        return true;
	}
	@Override
	public void finalizeImport() throws EdmException {
		int updated = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME);
		log.debug("ContractorAccount disabled: "+updated);
	}

	@Override
	public void materializeView() throws EdmException, SQLException {
        //pracujemy tylko na widokach
		//ServicesUtils.deleteRowsInsertFromEnumView(ENUM_TABLE_NAME, ENUM_VIEW_NAME);
		//DataBaseEnumField.reloadForTable(ENUM_TABLE_NAME);
	}
}
