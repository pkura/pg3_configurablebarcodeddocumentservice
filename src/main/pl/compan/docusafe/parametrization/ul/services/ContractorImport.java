package pl.compan.docusafe.parametrization.ul.services;

import java.rmi.RemoteException;
import java.util.List;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.parametrization.invoice.DictionaryUtils;
import pl.compan.docusafe.parametrization.ul.hib.UlContractor;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ul.DictionaryServicesSupplierStub;
import pl.compan.docusafe.ws.ul.DictionaryServicesSupplierStub.GetSuppliers;
import pl.compan.docusafe.ws.ul.DictionaryServicesSupplierStub.Supplier;

public class ContractorImport extends AbstractDictionaryImport {
	private static final String SERVICE_PATH = "/services/DictionaryServicesSupplier";
	DictionaryServicesSupplierStub stub;
    Supplier[] items;
    int itemPointer;
    List<Long> itemIds;
    private StringBuilder message;

    protected static Logger log = LoggerFactory.getLogger(ContractorImport.class);
	
	@Override
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServicesSupplierStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);
	}

    @Override
	public boolean doImport() throws RemoteException, EdmException {
        message = new StringBuilder();
        if (stub != null) {
            if (items == null) {
                GetSuppliers params = new GetSuppliers();
                pl.compan.docusafe.ws.ul.DictionaryServicesSupplierStub.GetSuppliersResponse response;
                if (params != null && (response = stub.getSuppliers(params)) != null) {
                    items = response.get_return();
                    if (items != null) {
                        message.append("Pobrano kontrahent�w do przetworzenia: "+items.length);

                        return false;
                    } else {
                        message.append("Nie pobrano, serwis zwraca pust� tablic�");
                        return true;
                    }
                }
            } else {
                if (items.length > itemPointer) {
                    Supplier personFromErp = items[itemPointer++];
                    //message.append("Przetwarzam pozycj� nr "+itemPointer+", erpId: "+personFromErp.getDostawcaId());
                    String streetBuilder = null;
                    if (StringUtils.isNotEmpty(personFromErp.getDostawcaUlica())) {
                        streetBuilder = personFromErp.getDostawcaUlica().trim() + (
                                StringUtils.isNotEmpty(personFromErp.getDostawcaNrDomu()) ?
                                        " "+personFromErp.getDostawcaNrDomu().trim() + (
                                                StringUtils.isNotEmpty(personFromErp.getDostawcaNrMieszkania()) ?
                                                        "/" + personFromErp.getDostawcaNrMieszkania().trim() :"")
                                        :"");
                    }

                    List<Person> found = Person.findByGivenFieldValue("wparam", personFromErp.getDostawcaId());
                    boolean createBasePerson = true;

                    if (!found.isEmpty()) {
                        //message.append(" Aktualizuj� istniej�cych kontrahent�w: "+found.size());
                        for (Person foundPerson : found) {

                            if (!(foundPerson instanceof Sender || foundPerson instanceof Recipient)) {
                                createBasePerson = false;
                            }

                            foundPerson.setZip(personFromErp.getDostawcaKodPoczt() != null ? StringUtils.left(personFromErp.getDostawcaKodPoczt().trim(), 14) : null);
                            foundPerson.setLocation(personFromErp.getDostawcaMiasto() != null ? StringUtils.left(personFromErp.getDostawcaMiasto().trim(), 50) : null);
                            foundPerson.setOrganization(personFromErp.getDostawcaNazwa() != null ? StringUtils.left(personFromErp.getDostawcaNazwa().trim(), 128) : null);
                            foundPerson.setNip(personFromErp.getDostawcaNip() != null ? StringUtils.left(personFromErp.getDostawcaNip().trim(), 15) : null);
                            foundPerson.setStreet(streetBuilder);
                            foundPerson.update();
                        }
                    }

                    if (createBasePerson) {
                        Person newPerson = new Person();
                        newPerson.setWparam(personFromErp.getDostawcaId());
                        newPerson.setZip(personFromErp.getDostawcaKodPoczt()!=null?StringUtils.left(personFromErp.getDostawcaKodPoczt().trim(),14):null);
                        newPerson.setLocation(personFromErp.getDostawcaMiasto()!=null?StringUtils.left(personFromErp.getDostawcaMiasto().trim(),50):null);
                        newPerson.setOrganization(personFromErp.getDostawcaNazwa()!=null?StringUtils.left(personFromErp.getDostawcaNazwa().trim(),128):null);
                        newPerson.setNip(personFromErp.getDostawcaNip()!=null?StringUtils.left(personFromErp.getDostawcaNip().trim(),15):null);
                        newPerson.setStreet(streetBuilder);
                        log.debug(newPerson.toString());
                        newPerson.create();
                        message.append(" Utworzono nowego kontrahenta w dso_person, id: "+newPerson.getId());
                    }

                    List<UlContractor> foundFromExternal = DictionaryUtils.findByGivenFieldValue(UlContractor.class, "dostawcaId", personFromErp.getDostawcaId());

                    if (!foundFromExternal.isEmpty()) {
                        for (UlContractor contractor : foundFromExternal) {
                            contractor.setAllFieldsFromServiceObject(personFromErp);
                            contractor.setUlica(streetBuilder);
                            contractor.setAvailable(true);
                            contractor.save();
                            itemIds.add(contractor.getId());
                        }
                    } else {
                        UlContractor contractor = new UlContractor(personFromErp.getDostawcaId());
                        contractor.setAllFieldsFromServiceObject(personFromErp);
                        contractor.setUlica(streetBuilder);
                        contractor.setAvailable(true);
                        contractor.save();
                        itemIds.add(contractor.getId());
                        message.append(" Utworzono nowego kontrahenta w slowniku zewnetrznym, id: " + contractor.getId());
                    }

                    return false;
                } else {
                    return true;
                }
            }
		}
        return true;
	}

    @Override
    public boolean isSleepAfterEachDoImport() {
        return false;
    }

    @Override
    public void finalizeImport() throws EdmException {
        items = null;

        int deleted = ServicesUtils.disableDeprecatedItem(itemIds, UlContractor.TABLE_NAME, false);
        message.append("Zablokowano: " + deleted);

        DataBaseEnumField.reloadForTable(UlContractor.TABLE_NAME);
    }

    @Override
    public void initImport() {
        items = null;
        itemPointer = 0;
        itemIds = Lists.newArrayList();
    }

    @Override
    public String getMessage() {
        return message.length()>0 ? message.toString() : null;
    }
}
