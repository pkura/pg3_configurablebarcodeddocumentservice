package pl.compan.docusafe.parametrization.ul.services;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.Set;

import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.parametrization.ul.hib.EnumItem;
import pl.compan.docusafe.parametrization.ul.hib.ProjectsFundSourcesEnumItem;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.FundSource;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.GetFundSources;

import com.google.common.collect.Sets;

public class FundSourcesImport extends AbstractDictionaryImport {
	
	private static final String TOP_VALUE = "1000";

	protected static Logger log = LoggerFactory.getLogger(FundSourcesImport.class);
	
	private static final String SERVICE_PATH = "/services/DictionaryServicesProjects";
	private static final String SERVICE_TABLE_NAME = "dsg_ul_services_projects_fund_sources";
	private static final String ENUM_VIEW_NAME = "dsg_ul_view_projects_fund_sources";
	private static final String ENUM_TABLE_NAME = "dsg_ul_projects_fund_sources";	
	private DictionaryServicesProjectsStub stub;

	@Override
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServicesProjectsStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);

	}

	@Override
	public boolean doImport() throws RemoteException, EdmException {
		log.debug("UL:00011 Start IMPORT Projects");
		if (stub != null) {
			//ZRODLA FINANSOWANIA
			GetFundSources paramsFund = new GetFundSources();
			//paramsFund.setTop(TOP_VALUE);
			pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.GetFundSourcesResponse response;
			if (paramsFund != null && (response = stub.getFundSources(paramsFund)) != null) {
				FundSource[] items = response.get_return();
				if (items != null) {
					EnumItem finder = EnumItem.getInstance();
					Set<Long> itemIds = Sets.newHashSet();

					for (FundSource item : items) {
						if (item != null) {
							try {
								EnumItem found = finder.findErpId(ProjectsFundSourcesEnumItem.class, item.getZrodloId());
								found.setAvailable(true);
								found.setCn(item.getZrodloIdn());
								found.setTitle(item.getZrodloNazwa() != null ? item.getZrodloNazwa().trim() : null);
								found.setRefValue(item.getIdentyfikatorNum()!=null?item.getIdentyfikatorNum().longValue():null);
								found.save();
								itemIds.add(found.getId());
							} catch (EntityNotFoundException e) {
								EnumItem newItem = new ProjectsFundSourcesEnumItem();
								newItem.setErpId(item.getZrodloId());
								newItem.setAvailable(true);
								newItem.setCn(item.getZrodloIdn());
								newItem.setTitle(item.getZrodloNazwa() != null ? item.getZrodloNazwa().trim() : null);
								newItem.setRefValue(item.getIdentyfikatorNum()!=null?item.getIdentyfikatorNum().longValue():null);
								newItem.save();
								itemIds.add(newItem.getId());
							}
						}
					}

					int updated = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME);
					log.debug("OrganizationalUnit disabled: "+updated);
				}
			}
		}
        return true;
	}

    @Override
    public void finalizeImport() throws EdmException {
        DataBaseEnumField.reloadForTable(ENUM_TABLE_NAME);
    }

    @Override
	public void materializeView() throws EdmException, SQLException {
//		ServicesUtils.deleteRowsInsertFromEnumView(ENUM_TABLE_NAME, ENUM_VIEW_NAME);
//		DataBaseEnumField.reloadForTable(ENUM_TABLE_NAME);
	}
}
