package pl.compan.docusafe.parametrization.ul.services;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.parametrization.ul.hib.DocumentTypes;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.simple.ul.DictionaryServicesDocumentTypesStub;
import pl.compan.docusafe.ws.simple.ul.DictionaryServicesDocumentTypesStub.GetPurchaseDocumentType;
import pl.compan.docusafe.ws.simple.ul.DictionaryServicesDocumentTypesStub.GetPurchaseDocumentTypeResponse;
import pl.compan.docusafe.ws.simple.ul.DictionaryServicesDocumentTypesStub.PurchaseDocumentType;

import com.google.common.collect.Sets;

public class DocumentTypesImport extends AbstractDictionaryImport {
	private static final String SERVICE_PATH = "/services/DictionaryServicesDocumentTypes";
	private static final String SERVICE_TABLE_NAME = "dsg_ul_services_document_types";
    private static final String ENUM_TABLE_NAME = "dsg_ul_document_types";
    DictionaryServicesDocumentTypesStub stub;
	protected static Logger log = LoggerFactory.getLogger(DocumentTypesImport.class);

	private Set<Long> itemIds;	
	
	@Override
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServicesDocumentTypesStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);
	}

	@Override
	public void initImport() {
		itemIds = Sets.newHashSet();
	}		
	
	@Override
	public boolean doImport() throws RemoteException, EdmException {
		if (stub != null) {
			GetPurchaseDocumentType params = new GetPurchaseDocumentType();
			GetPurchaseDocumentTypeResponse response;
			if (params != null && (response = stub.getPurchaseDocumentType(params)) != null) {
				PurchaseDocumentType[] items = response.get_return();
				if (items != null) {
					for (PurchaseDocumentType item : items) {
						if (item != null) {
							List<DocumentTypes> found = DictionaryUtils.findByGivenFieldValue(DocumentTypes.class, "typdokzakId", item.getTypdokzakId());
							if (!found.isEmpty()) {
								if (found.size() > 1) {
									log.error("Znaleziono wi�cej ni� 1 wpis o typdokzakId: " + item.getTypdokzakId());
								}
								for (DocumentTypes type : found) {
									type.setAllFieldsFromServiceObject(item);
									type.setAvailable(true);
									type.save();

									itemIds.add(Long.valueOf(type.getId()));
								}
							} else {
								DocumentTypes type = new DocumentTypes();
								type.setAllFieldsFromServiceObject(item);
								type.setAvailable(true);
								type.save();

								itemIds.add(Long.valueOf(type.getId()));
							}
						}
					}
				}
			}
		}
        return true;
	}
	@Override
	public void finalizeImport() throws EdmException {
		int updated = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME);
        DataBaseEnumField.reloadForTable(ENUM_TABLE_NAME);
	}
}