package pl.compan.docusafe.parametrization.ul.services;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.Set;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.ul.hib.EnumItem;
import pl.compan.docusafe.parametrization.ul.hib.OrganizationalUnitEnumItem;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ul.DictionaryServicesOrganizationalUnitStub;
import pl.compan.docusafe.ws.ul.DictionaryServicesOrganizationalUnitStub.GetOrganizationalUnits;
import pl.compan.docusafe.ws.ul.DictionaryServicesOrganizationalUnitStub.OrganizationalUnit;

import com.google.common.collect.Sets;

public class OrganizationalUnitImport extends AbstractDictionaryImport {
	private static final String SERVICE_TABLE_NAME = "dsg_ul_services_organizational_unit";
	private static final String ENUM_VIEW_NAME = "dsg_ul_view_organizational_unit";
	private static final String ENUM_TABLE_NAME = "dsg_ul_organizational_unit";
	
	private static final String UPOWAZNIONA_ENUM_VIEW_NAME = "dsg_ul_view_project_osoba_upowazniona";
	private static final String UPOWAZNIONA_ENUM_TABLE_NAME = "dsg_ul_project_osoba_upowazniona";
	
	
	public static final Logger log = LoggerFactory.getLogger(OrganizationalUnitImport.class);
	private static final String SERVICE_PATH = "/services/DictionaryServicesOrganizationalUnit";
	DictionaryServicesOrganizationalUnitStub stub;
	
	@Override
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServicesOrganizationalUnitStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);
	}

	@Override
	public boolean doImport() throws RemoteException, EdmException {
		log.debug("UL:00001 Start IMPORT OrganizationalUnit");
		if (stub != null) {
			GetOrganizationalUnits params = new GetOrganizationalUnits();
			//params.setTop("50");
			pl.compan.docusafe.ws.ul.DictionaryServicesOrganizationalUnitStub.GetOrganizationalUnitsResponse response;
			if (params != null && (response = stub.getOrganizationalUnits(params)) != null) {
				OrganizationalUnit[] items = response.get_return();
				if (items != null) {
					EnumItem finder = EnumItem.getInstance();
					Set<Long> itemIds = Sets.newHashSet();

					for (OrganizationalUnit item : items) {
						
						if (item != null) {
							log.debug("Pobra� OrganizationalUnit "+item.getKomorkaId() + " "+item.getNazwa());
							try {
								EnumItem found = finder.findErpId(OrganizationalUnitEnumItem.class, item.getKomorkaId());
								found.setAvailable(true);
								found.setCn(item.getKomorkaIdn());
								found.setTitle(item.getNazwa() != null ? item.getNazwa().trim() : null);
								found.save();
								itemIds.add(found.getId());
								log.debug("Modyfikuje OrganizationalUnit "+item.getKomorkaId() + " "+item.getNazwa());
							} catch (EntityNotFoundException e) {
								EnumItem newItem = new OrganizationalUnitEnumItem();
								newItem.setErpId(item.getKomorkaId());
								newItem.setAvailable(true);
								newItem.setCn(item.getKomorkaIdn());
								newItem.setTitle(item.getNazwa() != null ? item.getNazwa().trim() : null);
								newItem.setRefValue(null);
								newItem.save();
								itemIds.add(newItem.getId());
								log.debug("Dodaje OrganizationalUnit "+item.getKomorkaId() + " "+item.getNazwa());
							}
							
						}
						else{
							log.debug("Pobra� OrganizationalUnit  = NULL");
						}
					}

					int updated = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME);
					log.debug("OrganizationalUnit disabled: "+updated);
				}
			}
		}
		
		log.debug("UL:00002 KONIEC IMPORT OrganizationalUnit");
        return true;
	}

	@Override
	public void materializeView() throws EdmException, SQLException {
		//ServicesUtils.deleteRowsInsertFromEnumView(ENUM_TABLE_NAME, ENUM_VIEW_NAME);
		//ServicesUtils.deleteRowsInsertFromEnumView(UPOWAZNIONA_ENUM_TABLE_NAME, UPOWAZNIONA_ENUM_VIEW_NAME);
		DataBaseEnumField.reloadForTable(ENUM_TABLE_NAME);
		DataBaseEnumField.reloadForTable(UPOWAZNIONA_ENUM_TABLE_NAME);
	}
}
