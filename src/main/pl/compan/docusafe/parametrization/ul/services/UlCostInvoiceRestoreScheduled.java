package pl.compan.docusafe.parametrization.ul.services;

import java.security.Principal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import javax.security.auth.Subject;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.auth.RolePrincipal;
import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.permissions.PermissionCache;
import pl.compan.docusafe.service.permissions.PermissionsDriver;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class UlCostInvoiceRestoreScheduled extends ServiceDriver{

	private final static Logger log = LoggerFactory.getLogger(UlCostInvoiceRestoreScheduled.class);
	private final static int sprawdzZagubioneFakturyCoIle = Integer.parseInt(Docusafe.getAdditionProperty("UlCostInvoiceRestoreScheduled.sprawdzFaktury.coIle"));
	
	private Timer sprawdzCzySaZagubioneFakturyTimer;
	
	class SprawdzZagubioneFakturyTask extends TimerTask{

		@Override
		public void run() {		
			List<Long> fakturyKtoreZniklyNaKroku140 = getFakturyKtoreZniklyNaKroku140();
			
			for(Long documentId: fakturyKtoreZniklyNaKroku140){
				try{
	        		TaskSnapshot.updateByDocument(Document.find(documentId));
        		}catch(Exception e){}
			}
			
			List<Long> fakturyKtoreZniklyNaKroku140PoOdswiezaniu = getFakturyKtoreZniklyNaKroku140();
	        
	        try{
	        	openContext();
	        
				for(Long documentId: fakturyKtoreZniklyNaKroku140){
					if(fakturyKtoreZniklyNaKroku140PoOdswiezaniu.contains(documentId))
						restoreInvoice(documentId);
				}
	        }
	        catch(Exception e){
	        	log("UlCostInvoiceRestoreScheduled - Blad");
	        }
		}
		
		private void openContext() throws EdmException{
			Set<Principal> principals = new HashSet<Principal>();
	        principals.add(new UserPrincipal("dev123"));
	        principals.add(new RolePrincipal("admin"));
	        
	        Subject subject = new Subject(true, principals, Collections.emptySet(), Collections.emptySet());
	        
        	if(DSApi.isContextOpen())
        		DSApi.close();

	        DSApi.open(subject);
	        
        	PermissionCache cache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);
        	cache.update("dev123");
		}
		
		private static final String sqlFakturyKtoreZniklyNaKroku140 = "SELECT DISTINCT c.DOCUMENT_ID FROM dsg_ul_costinvoice c WHERE status=140 EXCEPT SELECT DISTINCT c.DOCUMENT_ID FROM dsg_ul_costinvoice c JOIN DSW_JBPM_TASKLIST t ON t.document_id=c.DOCUMENT_ID where status=140";
		
		private List<Long> getFakturyKtoreZniklyNaKroku140(){
			List<Long> wynik = new ArrayList<Long>();

			boolean contextOpened = false;
			
			try {
				contextOpened = DSApi.openContextIfNeeded();
				ResultSet resultSet = null;
				resultSet = DSApi.context().prepareStatement(sqlFakturyKtoreZniklyNaKroku140).executeQuery();
				
				while(resultSet.next()){
					wynik.add(resultSet.getLong(1));
				}
			} catch (Exception e){
				log.error(e.getMessage(), e);
			}
			finally{
				DSApi.closeContextIfNeeded(contextOpened);
			}
	        
	        return wynik;
		}
		
		private void restoreInvoice(Long documentId){
	        try{
	        	DSApi.context().begin();
	        	
	            WorkflowFactory.reopenWf(documentId);
	            OfficeDocument document = OfficeDocument.findOfficeDocument(documentId);
	            TaskSnapshot.updateByDocument(document);
	           
	            DSApi.context().commit();
	            
	            log("UlCostInvoiceRestoreScheduled - Przywrocono dokument o id: "+documentId);
	        }
	        catch(Exception e){
	        	log("UlCostInvoiceRestoreScheduled - Blad przywracania dokumentu o id: "+documentId);
	        }
		}
	}
	
	@Override
	protected void start() throws ServiceException {
		sprawdzCzySaZagubioneFakturyTimer = new Timer("sprawdzCzySaZagubioneFakturyTimer",true);
		
		log("UlCostInvoiceRestoreScheduled - start");
		sprawdzCzySaZagubioneFakturyTimer.schedule(new SprawdzZagubioneFakturyTask(), 0L, UlCostInvoiceRestoreScheduled.sprawdzZagubioneFakturyCoIle*DateUtils.MINUTE);
	}

	@Override
	protected void stop() throws ServiceException {
		if (sprawdzCzySaZagubioneFakturyTimer != null)
			sprawdzCzySaZagubioneFakturyTimer.cancel();
		
		log("UlCostInvoiceRestoreScheduled - stop");
	}

	@Override
	protected boolean canStop() {
		return false;
	}

	private void log(String message){
		log.info(message);
		console(Console.INFO, message);
	}
}
