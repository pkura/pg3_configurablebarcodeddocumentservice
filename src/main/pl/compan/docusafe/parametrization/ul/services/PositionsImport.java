package pl.compan.docusafe.parametrization.ul.services;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.Set;

import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.parametrization.ul.hib.EnumItem;
import pl.compan.docusafe.parametrization.ul.hib.ProjectBudgetPositionsEnumItem;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.GetProjectBudgetPositions;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.ProjectBudgetPosition;

import com.google.common.collect.Sets;

public class PositionsImport extends AbstractDictionaryImport {
	
	private static final String SERVICE_TABLE_NAME = "dsg_ul_services_project_budget_positions";

	private static final String TOP_VALUE = "1000";

	protected static Logger log = LoggerFactory.getLogger(PositionsImport.class);
	
	private static final String SERVICE_PATH = "/services/DictionaryServicesProjects";
	private static final String ENUM_VIEW_NAME = "dsg_ul_view_project_budget_positions";
	private static final String ENUM_TABLE_NAME = "dsg_ul_project_budget_positions";	
	private DictionaryServicesProjectsStub stub;

	@Override
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServicesProjectsStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);

	}

	@Override
	public boolean doImport() throws RemoteException, EdmException {
		log.debug("UL:00011 Start IMPORT Projects");
		if (stub != null) {
			
			log.debug("UL:00031 Start IMPORT Etap");
			//ETAP
			GetProjectBudgetPositions paramsPostion = new GetProjectBudgetPositions();
			//paramsPostion.setTop(TOP_VALUE);
			pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.GetProjectBudgetPositionsResponse respone;
			if (paramsPostion != null && (respone = stub.getProjectBudgetPositions(paramsPostion)) != null) {
				ProjectBudgetPosition[] items = respone.get_return();
				if (items != null) {
					EnumItem finder = EnumItem.getInstance();
					Set<Long> itemIds = Sets.newHashSet();
					
					for (ProjectBudgetPosition item : items) {
						if (item != null) {
							try {
								EnumItem found = finder.findErpId(ProjectBudgetPositionsEnumItem.class, item.getEtapId());
								found.setAvailable(true);
								found.setCn(String.valueOf(item.getEtapId()));
								found.setTitle(item.getNazwa() != null ? item.getNazwa().trim() : null);
                                found.setRefValue(item.getProjektId());
                                found.save();
								itemIds.add(found.getId());
								log.debug("UL:00032 Start IMPORT Etap zmodyfikowal "+found.toString());
							} catch (EntityNotFoundException e) {
								EnumItem newItem = new ProjectBudgetPositionsEnumItem();
								newItem.setErpId(item.getEtapId());
								newItem.setAvailable(true);
								newItem.setCn(String.valueOf(item.getEtapId()));
								newItem.setTitle(item.getNazwa() != null ? item.getNazwa().trim() : null);
								newItem.setRefValue(item.getProjektId());
								newItem.save();
								itemIds.add(newItem.getId());
								log.debug("UL:00033 Start IMPORT Etap doda� "+newItem.toString());
							}
						}
					}
					
					int updated = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME);
					log.debug("Positions disabled: "+updated);
				}
			}
		}
        return true;
	}
	@Override
	public void materializeView() throws EdmException, SQLException {
		//ServicesUtils.deleteRowsInsertFromEnumView(ENUM_TABLE_NAME, ENUM_VIEW_NAME);
		DataBaseEnumField.reloadForTable(ENUM_TABLE_NAME);
	}	
}
