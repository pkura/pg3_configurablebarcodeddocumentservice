package pl.compan.docusafe.parametrization.ul.services;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.parametrization.ul.hib.ProjectBudgetPositionResources;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.GetProjectBudgetPositionResourcesPagged;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.ProjectBudgetPositionResource;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.GetProjectBudgetPositionResourcesPaggedResponse;

import com.google.common.collect.Sets;

public class ResourcesPaggedImport extends AbstractDictionaryImport {
	

	private int importPageSize;
	private int importPageNumber;
	private Set<Long> itemIds;
	
	private StringBuilder message;

	protected static Logger log = LoggerFactory.getLogger(ResourcesPaggedImport.class);
	
	private static final String SERVICE_PATH = "/services/DictionaryServicesProjects";
	private static final String SERVICE_TABLE_NAME = "dsg_ul_services_project_budget_position_resources";
	private static final String ENUM_VIEW_NAME = "dsg_ul_view_project_budget_position_resources";
	private static final String ENUM_TABLE_NAME = "dsg_ul_project_budget_position_resources";	
	private DictionaryServicesProjectsStub stub;

	@Override
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServicesProjectsStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);

	}

	@Override
	public boolean doImport() throws RemoteException, EdmException {
		message = new StringBuilder();
		log.debug("UL:00011 Start IMPORT Projects");
		if (stub != null) {
			log.debug("UL:00003 START Dodaje Zasoby ProjectBudgetPositionResources");
			//ZASOB
			GetProjectBudgetPositionResourcesPagged paramsResource = new GetProjectBudgetPositionResourcesPagged();
			paramsResource.setPageSize(importPageSize);
			message.append("rozmiar strony: "+importPageSize);
			message.append(", nr strony: "+importPageNumber);
			paramsResource.setPage(importPageNumber++);
			GetProjectBudgetPositionResourcesPaggedResponse response;
			if ((response = stub.getProjectBudgetPositionResourcesPagged(paramsResource)) != null) {
				ProjectBudgetPositionResource[] items = response.get_return();
				if (items != null && items.length > 0) {
					for (ProjectBudgetPositionResource item : items) {
						if (item != null) {
							List<ProjectBudgetPositionResources> found = ProjectBudgetPositionResources.findByGivenFieldValue("zasobId", item.getZasobId());
							if (!found.isEmpty()) {
								if (found.size() > 1) {
									log.error("Znaleziono wi�cej ni� 1 wpis o zasob_id: " + item.getZasobId());
									message.append(", Znaleziono wi�cej ni� 1 wpis o zasob_id: " + item.getZasobId());
								}
								for (ProjectBudgetPositionResources resource : found) {
									resource.setAllFieldsFromServiceObject(item);
									resource.setAvailable(true);
									resource.save();

									itemIds.add(resource.getId());
								}
							} else {
								ProjectBudgetPositionResources resource = new ProjectBudgetPositionResources();
								resource.setAllFieldsFromServiceObject(item);
								resource.setAvailable(true);
								resource.save();

								itemIds.add(resource.getId());
							}
						}
					}
					return false; // import niezako�czony
				} else {
					return true; // import zako�czony
				}
			} else {
				return true;
			}
		}
		return true;
	}

	@Override
	public void initImport() {
		try {
			importPageNumber = Integer.valueOf(Docusafe.getAdditionProperty("import.resources.start_page_number"));
		} catch (Exception e) {
			importPageNumber = 1;
		}
		
		try {
			importPageSize = Integer.valueOf(Docusafe.getAdditionProperty("import.resources.page_size"));
		} catch (Exception e) {
			importPageSize = 1000;
		}
		
		itemIds = Sets.newHashSet();
	}

	@Override
	public String getMessage() {
		return message.toString();
	}

	@Override
	public void finalizeImport() throws EdmException {
		if (Docusafe.getAdditionProperty("import.resources.start_page_number") == null || "1".equals(Docusafe.getAdditionProperty("import.resources.start_page_number"))) {
			int updated = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME);
			log.debug("Resources disabled: "+updated);
		}
    }

	@Override
	public void materializeView() throws EdmException, SQLException {
//		ServicesUtils.deleteRowsInsertFromEnumView(ENUM_TABLE_NAME, ENUM_VIEW_NAME);
		DataBaseEnumField.reloadForTable(ENUM_TABLE_NAME);
	}
}
