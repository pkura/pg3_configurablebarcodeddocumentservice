package pl.compan.docusafe.parametrization.ul.services;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.imports.simple.repository.SimpleRepositoryImportService;
import pl.compan.docusafe.parametrization.ul.OrderLogic;
import pl.compan.docusafe.parametrization.ul.hib.DocumentTypes;
import pl.compan.docusafe.parametrization.ul.hib.OrderDocumentTypes;

import java.util.List;

public class UlOrderSimpleRepositoryImportService extends SimpleRepositoryImportService {

	@Override
	public String getDockindCn() throws EdmException {
		return OrderLogic.DOC_KIND_CN;
	}

	@Override
	public Long getContextId() throws EdmException {
		return OrderLogic.ORDER_POSITION_CONTEXT_ID;
	}

    @Override
    public Long getDictIdToOmitDictionariesUpdate() throws EdmException {
        return null;
    }

	@Override
	public List<Long> getObjectIds() throws EdmException {
		List<OrderDocumentTypes> documentTypes = OrderDocumentTypes.findAvailable(true);
		List<Long> objectIds = Lists.newLinkedList();
		
		for (OrderDocumentTypes type : documentTypes) {
			objectIds.add(type.getTypZapdostId());
		}
		return objectIds;
	}

	@Override
	public String getDynamicDictionaryCn() throws EdmException {
		return OrderLogic.BP_FIELD;
	}
}
