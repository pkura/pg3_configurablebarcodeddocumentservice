package pl.compan.docusafe.parametrization.ul.services;

import com.google.common.collect.ImmutableList;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImportService;
import pl.compan.docusafe.service.dictionaries.DictionaryImport;
import pl.compan.docusafe.util.DateUtils;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 12.08.13
 * Time: 21:48
 * To change this template use File | Settings | File Templates.
 */
public class UlSimpleImportResourcesService extends AbstractDictionaryImportService {

    private static final ImmutableList<DictionaryImport> AVAILABLE_IMPORTS = ImmutableList.<DictionaryImport>builder()
            .add(new ResourcesPaggedImport())
            .add(new StructureBudgetImport())
            //.add(new ResourcesToFundSourcesImport())
            .build();

    @Override
    protected ImmutableList<DictionaryImport> getAvailableImports() {
        return AVAILABLE_IMPORTS;
    }

    @Override
    protected long getTimerDefaultPeriod() {
        return 13 * DateUtils.HOUR;
    }

    @Override
    protected long getTimerStartDelay() {
        return 10 * DateUtils.SECOND;
    }
}
