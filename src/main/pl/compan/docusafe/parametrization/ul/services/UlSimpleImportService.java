package pl.compan.docusafe.parametrization.ul.services;

import com.google.common.collect.ImmutableList;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImportService;
import pl.compan.docusafe.service.dictionaries.DictionaryImport;
import pl.compan.docusafe.util.DateUtils;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 12.08.13
 * Time: 21:46
 * To change this template use File | Settings | File Templates.
 */
public class UlSimpleImportService extends AbstractDictionaryImportService {

    private static final ImmutableList<DictionaryImport> AVAILABLE_IMPORTS = ImmutableList.<DictionaryImport>builder()
            .add(new VatRatesImport())
            .add(new OrganizationalUnitImport())
            .add(new MpkToDivisionImport())
            .add(new ProjectsImport())
            .add(new BudgetsImport())
            .add(new PositionsImport())
            .add(new ProjectsToFundSourcesImport())
            .add(new FundSourcesImport())
            .add(new ExchangeRateImport())
            .add(new ProductImport())
            .add(new ProductionOrdersImport())
            .add(new DocumentTypesImport())
            .add(new OrderDocumentTypesImport())
            .add(new BudgetsFromUnitsImport())
            .build();

    @Override
    protected ImmutableList<DictionaryImport> getAvailableImports() {
        return AVAILABLE_IMPORTS;
    }

    @Override
    protected long getTimerDefaultPeriod() {
        return 1800l;
    }

    @Override
    protected long getTimerStartDelay() {
        return 15 * DateUtils.SECOND;
    }
}
