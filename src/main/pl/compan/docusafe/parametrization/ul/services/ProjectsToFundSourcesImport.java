package pl.compan.docusafe.parametrization.ul.services;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub;

import java.rmi.RemoteException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * User: kk
 * Date: 29.10.13
 * Time: 13:56
 */
public class ProjectsToFundSourcesImport extends AbstractDictionaryImport {

    protected static Logger log = LoggerFactory.getLogger(ProjectsToFundSourcesImport.class);

    private static final String SERVICE_PATH = "/services/DictionaryServicesProjects";
    private static final String SERVICE_TABLE_NAME = "dsg_ul_services_project2fundsources";
    private DictionaryServicesProjectsStub stub;
    private StringBuilder message;
    private int importPageSize;
    private int importPageNumber;
    private List<P2FS> results = Lists.newArrayList();


    @Override
    public void initConfiguration(AxisClientConfigurator conf) throws Exception {
        stub = new DictionaryServicesProjectsStub(conf.getAxisConfigurationContext());
        conf.setUpHttpParameters(stub, SERVICE_PATH);

    }

    @Override
    public void finalizeImport() throws EdmException {
        PreparedStatement ps = null;
        try {
            ps = DSApi.context().prepareStatement("delete from " + SERVICE_TABLE_NAME);
            ps.executeUpdate();
            DSApi.context().closeStatement(ps);
            ps = DSApi.context().prepareStatement("insert into " + SERVICE_TABLE_NAME + "(kontrakt_id,zrodlo_id) values (?,?)");
            int i = 0;
            int size = results.size();
            for (P2FS item : results) {
                i++;
                ps.setLong(1, item.projectId);
                ps.setLong(2, item.fsId);

                if (i % 2000 != 0 && i < size) {
                    ps.addBatch();
                } else {
                    ps.addBatch();
                    ps.executeBatch();
                }
            }
            message.append(", pobrano do bazy: "+size);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new EdmException(e.getMessage());
        } finally {
            if (ps != null) {
                DSApi.context().closeStatement(ps);
            }
        }
        results.clear();
    }

    @Override
    public boolean doImport() throws RemoteException, EdmException {
        if (stub == null) {
            throw new EdmException("Null stub");
        }

        message = new StringBuilder();
        DictionaryServicesProjectsStub.GetProjectFundSourcesPagged params = new DictionaryServicesProjectsStub.GetProjectFundSourcesPagged();
        params.setPageSize(importPageSize);
        message.append("rozmiar strony: " + importPageSize);
        message.append(", nr strony: " + importPageNumber);
        params.setPage(importPageNumber++);

        DictionaryServicesProjectsStub.GetProjectFundSourcesPaggedResponse response = stub.getProjectFundSourcesPagged(params);
        Preconditions.checkNotNull(response, "ws doesnt response");

        DictionaryServicesProjectsStub.ProjectFundSource[] items = response.get_return();
        if (items != null && items.length > 0) {
            for (DictionaryServicesProjectsStub.ProjectFundSource item : items) {
                if (item != null) {
                    results.add(new P2FS(item.getZrodloId(), item.getKontraktId()));
                }
            }

            return false;
        } else {
            return true;
        }
    }

    @Override
    public void initImport() {
        importPageSize = 5000;
        importPageNumber = 1;
    }

    @Override
    public String getMessage() {
        return message != null ? message.toString() : null;
    }

    private static class P2FS {
        private long projectId;
        private long fsId;

        private P2FS(long fsId, long projectId) {
            this.fsId = fsId;
            this.projectId = projectId;
        }
    }
}
