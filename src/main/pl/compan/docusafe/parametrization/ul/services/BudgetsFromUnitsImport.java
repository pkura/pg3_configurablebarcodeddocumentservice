package pl.compan.docusafe.parametrization.ul.services;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.parametrization.ul.hib.UlBudgetsFromUnits;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ul.DictionaryServiceStub;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

/**
 * Created by kk on 23.02.14.
 */
public class BudgetsFromUnitsImport extends AbstractDictionaryImport {


    protected static Logger log = LoggerFactory.getLogger(BudgetsFromUnitsImport.class);

    private StringBuilder message;
    private int importPageSize;
    private int importPageNumber;
    private Set<Long> itemIds;

    private static final String SERVICE_PATH = "/services/DictionaryService";
    private DictionaryServiceStub stub;

    @Override
    public void initConfiguration(AxisClientConfigurator conf) throws Exception {
        stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
        conf.setUpHttpParameters(stub, SERVICE_PATH);
    }

    @Override
    public void initImport() {
        importPageNumber = 1;
        importPageSize = 1000;
        itemIds = Sets.newHashSet();
    }

    @Override
    public boolean doImport() throws RemoteException, EdmException {
        message = new StringBuilder();
        if (stub == null) {
            throw new EdmException("Null stub");
        }

        DictionaryServiceStub.GetUnitBudgetKo params = new DictionaryServiceStub.GetUnitBudgetKo();
        params.setPageSize(importPageSize);
        message.append("rozmiar strony: " + importPageSize);
        message.append(", nr strony: " + importPageNumber);
        params.setPage(importPageNumber++);
        DictionaryServiceStub.GetUnitBudgetKoResponse response = stub.getUnitBudgetKo(params);

        Preconditions.checkNotNull(response, "ws doesnt response");

        DictionaryServiceStub.UnitBudgetKo[] items = response.get_return();
        if (items != null && items.length > 0) {
            for (DictionaryServiceStub.UnitBudgetKo item : items) {
                if (item != null) {
                    List<UlBudgetsFromUnits> founds = DictionaryUtils.findByGivenFieldValues(UlBudgetsFromUnits.class, ImmutableMap.of("erpId", (long) item.getId()));

                    if (!founds.isEmpty()) {
                        for (UlBudgetsFromUnits found : founds) {
                            found.setAllFieldsFromServiceObject(item).setAvailable(true).save();

                            itemIds.add(found.getId());
                        }
                    } else {
                        UlBudgetsFromUnits map = new UlBudgetsFromUnits();
                        map.setAllFieldsFromServiceObject(item).setAvailable(true).save();

                        itemIds.add(map.getId());
                    }
                }
            }

            return false; // import niezakończony
        } else {
            return true; // import zakończony
        }
    }


    @Override
    public String getMessage() {
        return message.toString();
    }

    @Override
    public void finalizeImport() throws EdmException {
//        if (Docusafe.getAdditionProperty("import.structure_budgets.start_page_number") == null || "1".equals(Docusafe.getAdditionProperty("import.structure_budgets.start_page_number"))) {
            int updated = ServicesUtils.disableDeprecatedItem(itemIds, UlBudgetsFromUnits.TABLE_NAME);
            log.debug("Budgets disabled: "+updated);
//        }
    }

    @Override
    public void materializeView() throws EdmException, SQLException {
        DataBaseEnumField.reloadForTable(UlBudgetsFromUnits.VIEW_NAME);
    }
}
