package pl.compan.docusafe.parametrization.ul.services;

import com.google.common.collect.Lists;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.MultiFormatReader;
import pl.compan.docusafe.service.barcodes.BarcodedDocumentService;
import pl.compan.docusafe.service.barcodes.recognizer.Recognizer;
import pl.compan.docusafe.service.barcodes.recognizer.ZxingRecognizer;
import pl.compan.docusafe.service.barcodes.slicer.ImageSlicer;
import pl.compan.docusafe.service.barcodes.slicer.ImageSlicerImpl;

import java.util.Hashtable;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 01.07.13
 * Time: 09:50
 * To change this template use File | Settings | File Templates.
 */
public class UlBarcodedDocumentService extends BarcodedDocumentService {

    @Override
    protected String getScansFolder() {
        return "C://ocrs";
    }

    @Override
    protected String getAttachmentColumnCn() {
        return "FAKTURA_SKAN";
    }

    @Override
    protected String getAttachmentNameTemplate() {
        return "Skan dokumentu nr %file_no%";
    }

    @Override
    protected String getDockindCn() {
        return "ul_costinvoice";
    }

    @Override
    protected String getFinishedScansFolder() {
        return "C://ocrs//finished";  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    protected Recognizer getRecognizer() {
        Hashtable<DecodeHintType, Object> hints = new Hashtable<DecodeHintType, Object>();
        hints.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);
        hints.put(DecodeHintType.POSSIBLE_FORMATS, Lists.newArrayList(BarcodeFormat.CODE_128));
        return new ZxingRecognizer(new MultiFormatReader(), hints);
    }

    @Override
    protected Recognizer getAlternativeRecognizer() {
        Hashtable<DecodeHintType, Object> hints = new Hashtable<DecodeHintType, Object>();
        hints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
        hints.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);
        hints.put(DecodeHintType.POSSIBLE_FORMATS, Lists.newArrayList(BarcodeFormat.CODE_128));
        return new ZxingRecognizer(new MultiFormatReader(), hints);
    }

    @Override
    protected ImageSlicer getImageSlicer() {
        //return new ImageSlicerImpl(11, 32, 30, 15);
        return new ImageSlicerImpl(17, 48, 18, 12);
        //return new ImageSlicerImpl(48, 17, 15, 30);
    }

    @Override
    protected String getDebugModeFolder() {
        return "C://ocrs//debug";
    }
}
