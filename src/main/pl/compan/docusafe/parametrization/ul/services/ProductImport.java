package pl.compan.docusafe.parametrization.ul.services;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.parametrization.ul.hib.Product;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ul.DictionaryServicesProductStub;
import pl.compan.docusafe.ws.ul.DictionaryServicesProductStub.GetProducts;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class ProductImport extends AbstractDictionaryImport {

    private static final String TOP_VALUE = "1000";
    public static final String VIEW_PRODUCT_DELEGATION = "dsg_ul_view_product_delegation";

    protected static Logger log = LoggerFactory.getLogger(ProductImport.class);
	
	private static final String SERVICE_PATH = "/services/DictionaryServicesProduct";
	private static final String SERVICE_TABLE_NAME = "dsg_ul_services_product";
	private static final String ENUM_VIEW_NAME = "dsg_ul_view_product";
	private static final String ENUM_TABLE_NAME = "dsg_ul_product";	
	private static final String ENUM_SAMOCHODY_VIEW_NAME = "dsg_ul_view_samochody";
	private static final String ENUM_SAMOCHODY_TABLE_NAME = "dsg_ul_samochody";
	private DictionaryServicesProductStub stub;

	private Set<Long> itemIds;
	private List<String> productGroups;
	private int groupsPointer;
	
	@Override
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServicesProductStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);

	}

	@Override
	public void initImport() {
		groupsPointer = 0;
		itemIds = Sets.newHashSet();
		
		String groupsRaw = Docusafe.getAdditionProperty("import.product.groups_names");
		if (groupsRaw != null) {
			String[] table = StringUtils.split(groupsRaw, ',');
			if (table.length > 0) {
				productGroups = Lists.newLinkedList();
				productGroups.addAll(Arrays.asList(table));
			}
		}
		
		if (productGroups == null) {
			productGroups = Lists.newLinkedList();
			productGroups.add("z_ZAKUPY");
			productGroups.add("SAMOCHODY");
		}
	}	
	
	
	@Override
	public boolean doImport() throws RemoteException, EdmException {
		if (stub != null && groupsPointer < productGroups.size()) {
			GetProducts params = new GetProducts();
			params.setProduktGrupa(productGroups.get(groupsPointer++));
			pl.compan.docusafe.ws.ul.DictionaryServicesProductStub.GetProductsResponse response;
			if (params != null && (response = stub.getProducts(params)) != null) {
				pl.compan.docusafe.ws.ul.DictionaryServicesProductStub.Product[] items = response.get_return();
				if (items != null) {
					for (pl.compan.docusafe.ws.ul.DictionaryServicesProductStub.Product item : items) {
						if (item != null) {
							List<Product> found = DictionaryUtils.findByGivenFieldValue(Product.class, "wytworId", item.getWytworId());
							if (!found.isEmpty()) {
								if (found.size() > 1) {
									log.error("Znaleziono wi�cej ni� 1 wpis o wytworId: " + item.getWytworId());
								}
								for (Product resource : found) {
									resource.setAllFieldsFromServiceObject(item);
									resource.setAvailable(true);
									resource.save();

									itemIds.add(resource.getId());
								}
							} else {
								Product resource = new Product();
								resource.setAllFieldsFromServiceObject(item);
								resource.setAvailable(true);
								resource.save();

								itemIds.add(resource.getId());
							}
						}
					}
				}
			}
			return false;
		} else {
			return true;
		}
	}

	@Override
	public void finalizeImport() throws EdmException {
		int updated = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME);
		log.debug("Product disabled: "+updated);
	}

	@Override
	public void materializeView() throws EdmException, SQLException {
		//ServicesUtils.deleteRowsInsertFromEnumView(ENUM_TABLE_NAME, ENUM_VIEW_NAME);
		DataBaseEnumField.reloadForTable(ENUM_TABLE_NAME);
        DataBaseEnumField.reloadForTable(VIEW_PRODUCT_DELEGATION);
	}
}
