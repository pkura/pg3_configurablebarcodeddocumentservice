package pl.compan.docusafe.parametrization.ul.services;

import com.google.common.collect.Sets;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.parametrization.ul.hib.ResourcesFundSources;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.GetProjectBudgetPositionResourceFundSourcesPagged;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.GetProjectBudgetPositionResourceFundSourcesPaggedResponse;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.ProjectBudgetPositionResourceFundSource;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;

public class ResourcesToFundSourcesImport extends AbstractDictionaryImport {
	
	protected static Logger log = LoggerFactory.getLogger(ResourcesToFundSourcesImport.class);
	
	private static final String SERVICE_PATH = "/services/DictionaryServicesProjects";
    private static final String SERVICE_TABLE_NAME = "dsg_ul_services_resources_fund_sources";
    private DictionaryServicesProjectsStub stub;
	private StringBuilder message;
	private int importPageSize;
	private int importPageNumber;
    private Set<Long> itemIds;
    private Set<Long> erpIds;


	@Override
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServicesProjectsStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);

	}

	@Override
	public void finalizeImport() throws EdmException {
        int deleted = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME, false);
        message.append("Usuni�to: "+deleted);
	}

	@Override
	public boolean doImport() throws RemoteException, EdmException {
		message = new StringBuilder();
		if (stub != null) {
			GetProjectBudgetPositionResourceFundSourcesPagged paramsFundToResources = new GetProjectBudgetPositionResourceFundSourcesPagged();
			paramsFundToResources.setPageSize(importPageSize);
			message.append("rozmiar strony: "+importPageSize);
			message.append(", nr strony: "+importPageNumber);
			paramsFundToResources.setPage(importPageNumber++);
			GetProjectBudgetPositionResourceFundSourcesPaggedResponse response;
			if ((response = stub.getProjectBudgetPositionResourceFundSourcesPagged(paramsFundToResources)) != null) {
				ProjectBudgetPositionResourceFundSource[] items = response.get_return();
				if (items != null && items.length > 0) {
                    for (ProjectBudgetPositionResourceFundSource item : items) {
                        if (item != null) {
                            if (!erpIds.add(item.getBpZasobZrodlaId())) {
                                log.error("Znaleziono wi�cej ni� 1 wpis o bpZasobZrodlaId: " + item.getBpZasobZrodlaId());
                                message.append(", Znaleziono wi�cej ni� 1 wpis o bpZasobZrodlaId: " + item.getBpZasobZrodlaId());
                            }

                            List<ResourcesFundSources> founds = DictionaryUtils.findByGivenFieldValue(ResourcesFundSources.class, "bpZasobZrodlaId", item.getBpZasobZrodlaId());

                            if (!founds.isEmpty()) {
                                for (ResourcesFundSources found : founds) {
                                    found.setAllFieldsFromServiceObject(item);
                                    found.save();

                                    itemIds.add(found.getId());
                                }
                            } else {
                                ResourcesFundSources map = new ResourcesFundSources();
                                map.setAllFieldsFromServiceObject(item);
                                map.save();

                                itemIds.add(map.getId());
                            }

                        }
                    }

					return false; // import niezako�czony
				} else {
					return true; // import zako�czony
				}
			} else {
				return true;
			}
		}
		return true;
	}

	@Override
	public void initImport() {
		importPageNumber = 1;
		
		try {
			importPageSize = Integer.valueOf(Docusafe.getAdditionProperty("import.resources2fundsources.page_size"));
		} catch (Exception e) {
			importPageSize = 1000;
		}

        itemIds = Sets.newHashSet();
        erpIds = Sets.newHashSet();
	}

	@Override
	public String getMessage() {
		return message!=null?message.toString():null;
	}
}
