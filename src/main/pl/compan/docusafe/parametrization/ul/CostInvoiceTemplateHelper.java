package pl.compan.docusafe.parametrization.ul;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.ul.hib.UlBudgetPosition;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.TextUtils;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * User: kk
 * Date: 15.11.13
 * Time: 13:07
 */
public class CostInvoiceTemplateHelper {

    private FieldsManager fm;
    private OfficeDocument doc;
    private Logger log;

    CostInvoiceTemplateHelper(FieldsManager fm, OfficeDocument doc, Logger log) {
        this.fm = fm;
        this.doc = doc;
        this.log = log;
    }

    public String generateContractorInfo() {
        StringBuilder result = new StringBuilder();
        try {
            Sender sender = doc.getSender();
            result.append(sender.getOrganization());
            result.append(", ");
            result.append(sender.getNip());
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }

        return result.toString();
    }

    public Map<String, List<BudgetPositionItem>> generateAcceptances() {
        final Multimap<String,BudgetPositionItem> acceptances = LinkedListMultimap.create();

        try {
            BudgetPositionItem item;
            for (DocumentAcceptance acc : DocumentAcceptance.find(doc.getId())) {
                acceptances.put(acc.getAcceptanceCn(), item = new BudgetPositionItem());

                item.setAcceptDate(acc.getAcceptanceTime());
                DSUser user = DSUser.findByUsername(acc.getUsername());
                item.setAcceptUser(user.asFirstnameLastname());
                item.setAcceptDivision(getDivisionListString(user));
            }

        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }

        return new HashMap<String, List<BudgetPositionItem>>() {
            @Override
            public List<BudgetPositionItem> get(Object key) {
                final List<String> cns = Lists.newArrayList(((String) key).split(","));
                return Lists.newArrayList(Iterables.transform(Iterables.filter(acceptances.entries(), new Predicate<Map.Entry<String, BudgetPositionItem>>() {
                    @Override
                    public boolean apply(Map.Entry<String, BudgetPositionItem> input) {
                        return cns.contains(input.getKey());
                    }
                }), new Function<Map.Entry<String, BudgetPositionItem>, BudgetPositionItem>() {
                    @Override
                    public BudgetPositionItem apply(Map.Entry<String, BudgetPositionItem> input) {
                        return input.getValue();
                    }
                }));
            }
        };
    }

    private String getDivisionListString(DSUser user) throws EdmException {
        return Joiner.on(", ").join(Iterables.transform(Lists.newArrayList(user.getDivisionsWithoutGroup()), new Function<DSDivision, String>() {
            public String apply(DSDivision input) {
                return StringUtils.left(input.getName(), 75);
            }
        }));
    }

    List<BudgetPositionItem> generateBudgetPositionItems() {
        List<BudgetPositionItem> result = Lists.newArrayList();

        try {
            List<Long> positions = (List) fm.getKey(CostInvoiceLogic.BP_FIELD);
            if (positions != null) {
                List<DocumentAcceptance> acceptances = DocumentAcceptance.find(doc.getId());

                BudgetPositionItem item;
                UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
                for (Long bpId : positions) {
                    result.add(item = new BudgetPositionItem());

                    UlBudgetPosition bp = bpInstance.find(bpId);
                    item.setAmount(bp.getIAmount());

                    if (bp.getIKind() != null) {
                        EnumItem enumItem = DataBaseEnumField.getEnumItemForTable(CostInvoiceLogic.RODZAJ_DZIALALNOSCI_TABLE_NAME, bp.getIKind());
                        item.setCost(enumItem.getCn().trim() + ": " + enumItem.getTitle().trim());
                    }

                    if (bp.getCMpk() != null) {
                        EnumItem enumItem = DataBaseEnumField.getEnumItemForTable(CostInvoiceLogic.ORGANIZATIONAL_UNIT_TABLE_NAME, bp.getCMpk());
                        item.setMpk(enumItem.getCn().trim() + ": " + enumItem.getTitle().trim());
                    }
                    if (bp.getCType() != null) {
                        EnumItem enumItem = DataBaseEnumField.getEnumItemForTable(CostInvoiceLogic.PRODUCT_TABLE_NAME, bp.getCType());
                        item.setType(enumItem.getCn().trim() + ": " + enumItem.getTitle().trim());
                    }
                    if (bp.getCFundSource() != null) {
                        EnumItem enumItem = DataBaseEnumField.getEnumItemForTable(CostInvoiceLogic.FUND_SOURCES_TABLE_NAME, bp.getCFundSource());
                        item.setFs(enumItem.getCn().trim() + ": " + enumItem.getTitle().trim());
                    }

                    if (bp.getSKind() != null && CostInvoiceLogic.SOCIAL_ID_FOR_NONE_BUDGET_OWNER_ACCEPT.contains(bp.getSKind().toString())) {
                        String acceptanceName = getAcceptanceNameFromBudgetPosition(bp);
                        if (acceptanceName != null) {
                            for (DocumentAcceptance acc : acceptances) {
                                if (acceptanceName.equals(acc.getAcceptanceCn())) {
                                    item.setAcceptDate(acc.getAcceptanceTime());
                                    DSUser user = DSUser.findByUsername(acc.getUsername());
                                    item.setAcceptUser(user.asFirstnameLastname());
                                    item.setAcceptDivision(getDivisionListString(user));
                                }
                            }
                        }
                    } else {
                        for (DocumentAcceptance acc : acceptances) {
                            if (bp.getId().equals(acc.getObjectId())) {
                                item.setAcceptDate(acc.getAcceptanceTime());
                                DSUser user = DSUser.findByUsername(acc.getUsername());
                                item.setAcceptUser(user.asFirstnameLastname());
                                item.setAcceptDivision(getDivisionListString(user));
                            }
                        }
                    }
                }
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }

        return result;
    }


    public Map<String,AcceptInfoAttribute> generateAcceptInfo() {
        AcceptInfo result = new AcceptInfo();
        try {
            List<DocumentAcceptance> acceptances = DocumentAcceptance.find(doc.getId());
            for (DocumentAcceptance acc : acceptances) {
                result.put(acc.getAcceptanceCn(), DSUser.findByUsername(acc.getUsername()).asFirstnameLastname(), acc.getAcceptanceTime());
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    public class AcceptInfo extends HashMap<String, AcceptInfoAttribute> {

        public AcceptInfoAttribute put(String key, String name, Date date) {
            AcceptInfoAttribute attr;
            if (this.containsKey(key)) {
                attr = this.get(key);
            } else {
                this.put(key, attr = new AcceptInfoAttribute());
            }
            attr.names.add(name);
            attr.dates.add(date);
            return attr;
        }

    }

    public class AcceptInfoAttribute {
        List<String> names = Lists.newLinkedList();
        List<Date> dates = Lists.newLinkedList();
        String splitter = ", ";
        String dateSplitter = " dnia ";

        public String getName() {
            return Joiner.on(splitter).skipNulls().join(names);
        }

        public String getInfo() {
            Iterable<String> info = new Iterable<String>() {
                @Override
                public Iterator<String> iterator() {
                    return new Iterator<String>() {
                        int index = 0;
                        String name,date;
                        @Override
                        public boolean hasNext() {
                            name = names.size() > index ? names.get(index) : null;
                            date = dates.size() > index ? DateUtils.formatCommonDate(dates.get(index)) : "";
                            index++;
                            return name != null ? true : false;
                        }

                        @Override
                        public String next() {
                            return name + dateSplitter + date;
                        }

                        @Override
                        public void remove() {
                        }
                    };
                }
            };
            return Joiner.on(splitter).skipNulls().join(info);
        }
    }

    private String getAcceptanceNameFromBudgetPosition(UlBudgetPosition bp) {
        if (bp.getSKind() != null) {
            if (bp.getSKind().equals(CostInvoiceLogic.FAKTURA_SOCIAL_KN_ID)) {
                return "cos_rits_accept";
            }
            if (bp.getSKind().equals(CostInvoiceLogic.FAKTURA_SOCIAL_DS)) {
                return "cos_accept";
            }
        }
        return null;
    }

    public static class BudgetPositionItem {
        private String cost;
        private String type;
        private String mpk;
        private String fs;
        private String acceptUser;
        private String acceptDivision;
        private Date acceptDate;
        private BigDecimal amount;

        public Date getAcceptDate() {
            return acceptDate;
        }

        public void setAcceptDate(Date acceptDate) {
            this.acceptDate = acceptDate;
        }

        public String getAcceptUser() {
            return Strings.isNullOrEmpty(acceptUser) ? "-" : acceptUser;
        }

        public void setAcceptUser(String acceptUser) {
            this.acceptUser = acceptUser;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }

        public String getCost() {
            return Strings.isNullOrEmpty(cost) ? "-" : cost;
        }

        public void setCost(String cost) {
            this.cost = cost;
        }

        public String getAmountLiterally() throws UnsupportedEncodingException {
            return new String((TextUtils.numberToText(amount.setScale(0, RoundingMode.DOWN))).getBytes("ISO-8859-2"), "Cp1250") + " " + amount.remainder(BigDecimal.ONE).movePointRight(2).setScale(0, RoundingMode.HALF_UP) + "/100";
        }

        public String getAcceptDivision() {
            return acceptDivision;
        }

        public void setAcceptDivision(String acceptDivision) {
            this.acceptDivision = acceptDivision;
        }

        public String getFs() {
            return Strings.isNullOrEmpty(fs) ? "-" : fs;
        }

        public void setFs(String fs) {
            this.fs = fs;
        }

        public String getMpk() {
            return Strings.isNullOrEmpty(mpk) ? "-" : mpk;
        }

        public void setMpk(String mpk) {
            this.mpk = mpk;
        }

        public String getType() {
            return Strings.isNullOrEmpty(type) ? "-" : type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String toString() {
            return "acceptUser" + acceptUser;
        }
    }
}
