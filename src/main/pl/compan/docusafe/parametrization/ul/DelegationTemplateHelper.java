package pl.compan.docusafe.parametrization.ul;


import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.ObjectUtils;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

public class DelegationTemplateHelper {
    private FieldsManager fm;
    private OfficeDocument doc;
    private Logger log;
    private Format moneyFormatter;

    {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(',');
        moneyFormatter = new DecimalFormat("### ### ##0.00", symbols);
    }

    DelegationTemplateHelper(FieldsManager fm, OfficeDocument doc, Logger log) {
        this.fm = fm;
        this.doc = doc;
        this.log = log;
    }

    Delegated getDelegate() {
        Delegated delegated = new Delegated();
        try {
            String delegatedKind = fm.getStringKey(AbstractDelegationLogic.DELEGOWANY_WYBOR);
            if ("1".equals(delegatedKind)) {
                delegated.setFirstlastname(fm.getStringValue(AbstractDelegationLogic.WNIOSKODAWCA))
                         .setDescription(fm.getStringValue(AbstractDelegationLogic.WORKER_DIVISION));

            } else if ("2".equals(delegatedKind)) {
                if (!doc.getRecipients().isEmpty()) {
                    Recipient recipient = doc.getRecipients().get(0);
                    delegated.setFirstlastname(recipient.getFirstname()+ " "+recipient.getLastname())
                             .setDescription(recipient.getOrganizationDivision());
                }
            } else if ("3".equals(delegatedKind)) {
                EnumItem delegatedOtherKind = fm.getEnumItem(AbstractDelegationLogic.DELEGOWANY_INNY);
                delegated.setFirstlastname(fm.getStringValue(AbstractDelegationLogic.DELEGOWANY))
                         .setDescription(delegatedOtherKind != null ? delegatedOtherKind.getTitle() : "nie podano");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return delegated;
    }

    String getFormattedCurrency(EnumValues enumValue) {
        String currencySymbol = enumValue.getSelectedValue();
        String[] currency = currencySymbol.split(": ");
        if (currency.length > 1) {
            return currency[1];
        }
        return currencySymbol;
    }

    AbroadCostDict getAbroadStatementCostDict() {
        AbroadCostDict dict = new AbroadCostDict();
        try {
            List<Map<String, Object>> dicLists = (List<Map<String, Object>>) fm.getValue(DelegationAbroadLogic.DIETA_ZAGRANICZNA);
            if (dicLists != null) {
                for (Map<String, Object> item : dicLists) {
                    Object costEnum = item.get(AbstractDelegationLogic.KALKULACJA_KOSZTOW + "_ENUM");
                    Object currencyEnum = item.get(AbstractDelegationLogic.KALKULACJA_KOSZTOW + "_CURRENCY");
                    if (costEnum != null && currencyEnum != null) {
                        String enumId = ((EnumValues) costEnum).getSelectedId();
                        String currencySymbol = getFormattedCurrency((EnumValues) currencyEnum);

                        dict.addMoney(enumId, (BigDecimal) item.get(AbstractDelegationLogic.KALKULACJA_KOSZTOW + "_MONEY_VALUE"), currencySymbol);
                        dict.addMoneyPln(enumId, (BigDecimal) item.get(AbstractDelegationLogic.KALKULACJA_KOSZTOW + "_MONEY_VALUE_PLN"));
                    }
                }
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
        return dict;
    }

    BigDecimal getLoanReturnAmount() {
        try {
            List<Map<String, Object>> dicLists = (List<Map<String, Object>>) fm.getValue("ROZLICZENIE_ZALICZKI");
            if (dicLists != null) {
                for (Map<String, Object> item : dicLists) {
                    try {
                        if ("1".equals(((EnumValues) item.get("ROZLICZENIE_ZALICZKI_ENUM")).getSelectedId())) {
                            if (item.get("ROZLICZENIE_ZALICZKI_MONEY_VALUE") != null) {
                                return (BigDecimal) item.get("ROZLICZENIE_ZALICZKI_MONEY_VALUE");
                            }
                        }
                    } catch (RuntimeException er) {
                        log.error(er.getMessage(), er);
                    }
                }
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }

        return BigDecimal.ZERO;
    }

    MultipleAmountExtended getReturnAmounts(String returnTypeId) {
        MultipleAmountExtended result = new MultipleAmountExtended();
        try {
            List<Map<String, Object>> dicLists = (List<Map<String, Object>>) fm.getValue("ROZLICZENIE_ZALICZKI");
            if (dicLists != null) {
                for (Map<String, Object> item : dicLists) {
                    try {
                        if (returnTypeId.equals(((EnumValues) item.get("ROZLICZENIE_ZALICZKI_ENUM")).getSelectedId())) {
                            if (item.get("ROZLICZENIE_ZALICZKI_MONEY_VALUE") != null) {
                                BigDecimal amount = (BigDecimal) item.get("ROZLICZENIE_ZALICZKI_MONEY_VALUE");
                                String currency = "";
                                String info = item.get("ROZLICZENIE_ZALICZKI" + "_ADD_ENUM") != null ? Strings.nullToEmpty(((EnumValues) item.get("ROZLICZENIE_ZALICZKI" + "_ADD_ENUM")).getSelectedValue()) :"";
                                if (item.get("ROZLICZENIE_ZALICZKI_CURRENCY") != null && ((EnumValues) item.get("ROZLICZENIE_ZALICZKI_CURRENCY")).getSelectedValue() != null) {
                                     currency = getFormattedCurrency((EnumValues) item.get("ROZLICZENIE_ZALICZKI_CURRENCY"));
                                }
                                result.add(new AmountExtended(amount, (BigDecimal) item.get("ROZLICZENIE_ZALICZKI_MONEY_VALUE_PLN"), currency, info));
                            }
                        }
                    } catch (RuntimeException er) {
                        log.error(er.getMessage(), er);
                    }
                }
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }

        return result;
    }

    public static class MultipleAmountExtended extends ArrayList<AmountExtended> {
        public AmountExtended getSumOfPlnAmount() {
            BigDecimal result = BigDecimal.ZERO;
            for (AmountExtended amount : this) {
                result = result.add(Objects.firstNonNull(amount.getPlnAmount(), BigDecimal.ZERO));
            }
            return new AmountExtended().setPlnAmount(result);
        }
    }

    String getDictionaryEnumValues(String dicCn, String fieldCn) {
        Set<String> result = Sets.newTreeSet();
        try {
            List<Map<String, Object>> dicLists = (List<Map<String, Object>>) fm.getValue(dicCn);
            if (dicLists != null) {
                for (Map<String, Object> item : dicLists) {
                    try {
                        result.add(((EnumValues) item.get(dicCn + "_"+ fieldCn)).getSelectedValue());
                    } catch (RuntimeException er) {
                        log.error(er.getMessage(), er);
                    }
                }
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }

        return Joiner.on(", ").join(result);
    }

    String getFundSources() {
        return getDictionaryEnumValues(DelegationAbroadLogic.BP_FIELD, "C_FUND_SOURCE");
    }
    String getProjects() {
        return getDictionaryEnumValues(DelegationAbroadLogic.BP_FIELD, "C_PROJECT");
    }
    String getMpks() {
        return getDictionaryEnumValues(DelegationAbroadLogic.BP_FIELD, "C_MPK");
    }

    public String getBudgetPositionDescription(String dictionaryCn) {
        String dictionryCnWithPrefix = dictionaryCn + "_";
        List<List<String>> positions = Lists.newArrayList();
        try {
            List<String> position;
            List<Map<String, Object>> dicLists = (List<Map<String, Object>>) fm.getValue(dictionaryCn);
            if (dicLists != null) {
                for (Map<String, Object> item : dicLists) {
                    try {
                        positions.add(position = Lists.newArrayList());
                        position.add(((EnumValues) item.get(dictionryCnWithPrefix + "I_KIND")).getSelectedValue());
                        position.add(((EnumValues) item.get(dictionryCnWithPrefix + "C_MPK")).getSelectedValue());
                        position.add(((EnumValues) item.get(dictionryCnWithPrefix + "C_PROJECT")).getSelectedValue());
                        position.add(((EnumValues) item.get(dictionryCnWithPrefix + "C_FUND_SOURCE")).getSelectedValue());
                        position.add(moneyFormatter.format(new BigDecimal(Objects.firstNonNull((Number) item.get(dictionaryCn + "_" + "I_AMOUNT"), 0).floatValue()))+ "z�");
                    } catch (RuntimeException er) {
                        log.error(er.getMessage(), er);
                    }
                }
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }

        return Joiner.on("; ").skipNulls().join(Iterables.transform(positions, new Function<List<String>, String>() {
            @Override
            public String apply(List<String> position) {
                return Joiner.on(", ").join(Iterables.filter(position, new Predicate<String>() {
                    @Override
                    public boolean apply(String input) {
                        return !Strings.isNullOrEmpty(input);
                    }
                }));
            }
        }));
    }

    public String getFundSourcesWithAmount() {
        Map<String,BigDecimal> fs = Maps.newTreeMap();
        try {
            List<Map<String, Object>> dicLists = (List<Map<String, Object>>) fm.getValue(DelegationAbroadLogic.BP_FIELD);
            if (dicLists != null) {
                for (Map<String, Object> item : dicLists) {
                    try {
                        String selectedValue = ((EnumValues) item.get(DelegationAbroadLogic.BP_FIELD + "_" + "C_FUND_SOURCE")).getSelectedValue();
                        BigDecimal money = new BigDecimal(Objects.firstNonNull((Number) item.get(DelegationAbroadLogic.BP_FIELD + "_"+ "I_AMOUNT"), 0).floatValue());
                        if (fs.get(selectedValue) != null) {
                             money = fs.get(selectedValue).add(money);
                        }
                        fs.put(selectedValue, money);
                    } catch (RuntimeException er) {
                        log.error(er.getMessage(), er);
                    }
                }
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }

        return Joiner.on(", ").join(Iterables.transform(fs.entrySet(), new Function<Map.Entry<String, BigDecimal>, String>() {
            @Override
            public String apply(Map.Entry<String, BigDecimal> input) {
                return input.getKey() + ": " + moneyFormatter.format(input.getValue()) + " z�";
            }
        }));
    }

    Multimap<String, AbroadCost> getAbroadCostInfo() {
        Multimap<String,AbroadCost> result = LinkedListMultimap.<String,AbroadCost>create();

        generateFromDietDictionary(result, DelegationAbroadLogic.DIETA_ZAGRANICZNA_PRE, "_PRE");
        generateFromDietDictionary(result, DelegationAbroadLogic.DIETA_ZAGRANICZNA, "");
        generateFromAccommodiationDictionary(result, DelegationAbroadLogic.KOSZTY_NOCLEGU_PRE, "_PRE");
        generateFromAccommodiationDictionary(result, DelegationAbroadLogic.KOSZTY_NOCLEGU, "");
        generateFromPreCostDictionary(result);
        generateFromOtherCostDictionary(result);
        generateFromDelegationDictionary(result, DelegationAbroadLogic.ZALICZKA_WALUTOWA_DIC, "ZALICZKA");

        generateFromDelegationDictionary(result, DelegationAbroadLogic.DIETA_ZAGRANICZNA, "DIETA_ZAGRANICZNA");
        generateFromDelegationDictionary(result, DelegationAbroadLogic.KOSZTY_NOCLEGU, "KOSZTY_NOCLEGU");
        generateFromDelegationDictionary(result, DelegationAbroadLogic.KOSZTY_INNE, "KOSZTY_INNE");

        return result;
    }

    public BigDecimal getMoneyPlnFromAbroadCost(Multimap<String, AbroadCost> abroadCostInfo, String key) {
        BigDecimal result = BigDecimal.ZERO;
        for (AbroadCost abroadCost : abroadCostInfo.get(key)) {
            result = result.add(Objects.firstNonNull(abroadCost.getMoneyPln(), BigDecimal.ZERO));
        }
        return result;
    }

    public BigDecimal calcAllCost(BigDecimal loan, MultipleAmountExtended workerReturn, MultipleAmountExtended toWorkerReturn) {
        return Objects.firstNonNull(loan, BigDecimal.ZERO).subtract(Objects.firstNonNull(workerReturn.getSumOfPlnAmount().getPlnAmount(), BigDecimal.ZERO)).add(Objects.firstNonNull(toWorkerReturn.getSumOfPlnAmount().getPlnAmount(), BigDecimal.ZERO)).setScale(2, RoundingMode.HALF_UP);
    }

    private void generateFromDelegationDictionary(Multimap<String, AbroadCost> result, String dictionaryCn, String resultCn) {
        try {
            List<Map<String, Object>> dicLists = (List<Map<String, Object>>) fm.getValue(dictionaryCn);
            if (dicLists != null) {
                for (Map<String, Object> item : dicLists) {
                    try {
                        Object currencyEnum = item.get(dictionaryCn + "_CURRENCY");
                        String info = item.get(dictionaryCn + "_ENUM") != null ? Strings.nullToEmpty(((EnumValues) item.get(dictionaryCn + "_ENUM")).getSelectedValue()) :"";
                        String addInfo = item.get(dictionaryCn + "_ADD_ENUM") != null ? Strings.nullToEmpty(((EnumValues) item.get(dictionaryCn + "_ADD_ENUM")).getSelectedValue()) :"";
                        if (currencyEnum != null) {
                            String currencySymbol = getFormattedCurrency((EnumValues) currencyEnum);

                            result.put(resultCn, new AbroadCost(currencySymbol, (BigDecimal) item.get(dictionaryCn + "_MONEY_VALUE"), (BigDecimal) item.get(dictionaryCn + "_MONEY_VALUE_PLN"), info, addInfo));
                            //result.put(resultCn, new AbroadCost(currencySymbol, (BigDecimal) item.get(dictionaryCn + "_MONEY_VALUE"), (BigDecimal) item.get(dictionaryCn + "_MONEY_VALUE_PLN"), info, addInfo));
                        }
                    } catch (RuntimeException er) {
                        log.error(er.getMessage(), er);
                    }
                }
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }

    static BigDecimal safeAdd(BigDecimal e1, BigDecimal e2) {
        return Objects.firstNonNull(e1, BigDecimal.ZERO).add(Objects.firstNonNull(e2, BigDecimal.ZERO));
    }

    private void generateFromPreCostDictionary(Multimap<String, AbroadCost> result) {
        try {
            List<Map<String, Object>> dicLists = (List<Map<String, Object>>) fm.getValue(AbstractDelegationLogic.KALKULACJA_KOSZTOW);
            if (dicLists != null) {
                for (Map<String, Object> item : dicLists) {
                    try {
                        Object costEnum = item.get(AbstractDelegationLogic.KALKULACJA_KOSZTOW + "_ENUM");
                        Object currencyEnum = item.get(AbstractDelegationLogic.KALKULACJA_KOSZTOW + "_CURRENCY");

                        if (costEnum != null && currencyEnum != null) {
                            String enumId = ((EnumValues) costEnum).getSelectedId();
                            String currencySymbol = getFormattedCurrency((EnumValues) currencyEnum);

                            result.put("KK_" + enumId, new AbroadCost(currencySymbol, (BigDecimal) item.get(AbstractDelegationLogic.KALKULACJA_KOSZTOW + "_MONEY_VALUE")));
                        }
                    } catch (RuntimeException er) {
                        log.error(er.getMessage(), er);
                    }
                }
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }

    private void generateFromOtherCostDictionary(Multimap<String, AbroadCost> result) {
        try {
            List<Map<String, Object>> dicLists = (List<Map<String, Object>>) fm.getValue(DelegationAbroadLogic.KOSZTY_INNE);
            if (dicLists != null) {
                for (Map<String, Object> item : dicLists) {
                    try {
                        BigDecimal costEnum = (BigDecimal) item.get(DelegationAbroadLogic.KOSZTY_INNE + "_MONEY_VALUE");
                        Object currencyEnum = item.get(DelegationAbroadLogic.KOSZTY_INNE + "_CURRENCY");
                        if (currencyEnum != null) {
                            String currencySymbol = getFormattedCurrency((EnumValues) currencyEnum);
                            result.put("INNE", new AbroadCost(currencySymbol, costEnum));
                        }
                    } catch (RuntimeException er) {
                        log.error(er.getMessage(), er);
                    }
                }
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }

    private void generateFromAccommodiationDictionary(Multimap<String, AbroadCost> result, String dictionaryCn, String propertySuffix) {
        try {
            List<Map<String, Object>> dicLists = (List<Map<String, Object>>) fm.getValue(dictionaryCn);
            if (dicLists != null) {
                for (Map<String, Object> item : dicLists) {
                    try {
                        BigDecimal accRate = (BigDecimal) item.get(dictionaryCn + "_RATE");
                        BigDecimal accCost = (BigDecimal) item.get(dictionaryCn + "_MONEY_VALUE");
                        Number accCount = (Number) item.get(dictionaryCn + "_VALUE");
                        EnumValues accType = (EnumValues) item.get(dictionaryCn + "_ENUM");
                        EnumValues currency = (EnumValues) item.get(dictionaryCn + "_CURRENCY");

                        if (accType != null && "3".equals(accType.getSelectedId())) {
                            result.put("RYCZALT_HOTELOWY" + propertySuffix, new AbroadCost(accRate != null ? accRate.divide(new BigDecimal(4), 4, RoundingMode.HALF_UP) : accRate, accCount != null ? new BigDecimal(accCount.intValue()) : BigDecimal.ZERO, getFormattedCurrency(currency)));
                        } else {
                            result.put("POBYT" + propertySuffix, new AbroadCost(getFormattedCurrency(currency), accCost));
                        }
                    } catch (RuntimeException er) {
                        log.error(er.getMessage(), er);
                    }
                }
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }
    private void generateFromDietDictionary(Multimap<String, AbroadCost> result, String dictionaryCn, String propertySuffix) {
        try {
            String transport = Objects.firstNonNull(fm.getKey(DelegationAbroadLogic.TRANSPORT), "").toString();
            List<Map<String, Object>> dicLists = (List<Map<String, Object>>) fm.getValue(dictionaryCn);
            if (dicLists != null) {
                for (Map<String, Object> item : dicLists) {
                    try {
                        BigDecimal dietRate = (BigDecimal) item.get(dictionaryCn + "_RATE");
                        BigDecimal dietValue = DelegationAbroadLogic.prepareDietCount((BigDecimal) item.get(dictionaryCn + "_VALUE"));
                        Object dojazdowa = item.get(dictionaryCn + "_INT4");
                        int breakfasts = Optional.fromNullable((Integer) item.get(dictionaryCn + "_INT1")).or(0);  //15%
                        int middaymeals = Optional.fromNullable((Integer) item.get(dictionaryCn + "_INT2")).or(0); //30%
                        int dinners = Optional.fromNullable((Integer) item.get(dictionaryCn + "_INT3")).or(0); //30%
                        //Object kieszonkowe = item.get(dictionaryCn + "_ADD_RATE");
                        Object przejazdy = item.get(dictionaryCn + "_INT5");
                        EnumValues currency = (EnumValues) item.get(dictionaryCn + "_CURRENCY");

                        if (dojazdowa != null && (Integer) dojazdowa != 0) {
                            result.put("DOJAZDY" + propertySuffix, new AbroadCost(dietRate, new BigDecimal((Integer) dojazdowa), getFormattedCurrency(currency)));
                        }

                        if (przejazdy != null && (Integer) przejazdy != 0) {
                            result.put("PRZEJAZDY" + propertySuffix, new AbroadCost(dietRate, new BigDecimal((Integer) przejazdy).movePointLeft(1), getFormattedCurrency(currency)));
                        }
                        /*if (kieszonkowe != null && BigDecimal.ZERO.compareTo((BigDecimal) kieszonkowe) != 0) {
                            result.put("KIESZONKOWE" + propertySuffix, new AbroadCost(dietRate, (BigDecimal) kieszonkowe, currency.getSelectedValue()));
                        }*/

                        result.put("POBYTOWE" + propertySuffix, new AbroadCost(dietRate, dietValue.subtract(new BigDecimal(breakfasts * 15 + (middaymeals + dinners) * 30).movePointLeft(2)), getFormattedCurrency(currency)));

                    } catch (RuntimeException er) {
                        log.error(er.getMessage(), er);
                    }
                }
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }

    public AbroadCostDict getAbroadCostInfoSum(Multimap<String, AbroadCost> abroadCostInfo) {
        AbroadCostDict dict = new AbroadCostDict();
        for (Map.Entry<String,AbroadCost> item : abroadCostInfo.entries()) {
            dict.addMoney(item.getKey(), item.getValue().getMoney(), item.getValue().getCurr());
            dict.addMoneyPln(item.getKey(), item.getValue().getMoneyPln());
        }

        return dict;
    }

    public AmountExtended getLoanAmount() {
        try {
            return new AmountExtended((BigDecimal) fm.getKey("ZALICZKA_WALUTOWA_KWOTA"));
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
        return new AmountExtended();
    }

    public AmountExtended getAmountExtended(String fieldCn) {
        try {
            return new AmountExtended((BigDecimal) fm.getKey(fieldCn));
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
        return new AmountExtended();
    }

    public static class AbroadCost {
        private BigDecimal rate;
        private BigDecimal value;
        private String curr;
        private String info;
        private String addInfo;
        private BigDecimal money;
        private BigDecimal moneyPln;

        AbroadCost(BigDecimal rate, BigDecimal value, String curr) {
            this.rate = rate;
            this.value = value;
            this.curr = curr;
        }

        AbroadCost(String curr, BigDecimal money) {
            this.curr = curr;
            this.money = money;
        }

        AbroadCost(String curr, BigDecimal money, String info, String addInfo) {
            this.curr = curr;
            this.money = money;
            this.info = info;
            this.addInfo = addInfo;
        }

        AbroadCost(String curr, BigDecimal money, BigDecimal moneyPln, String info, String addInfo) {
            this.curr = curr;
            this.money = money;
            this.moneyPln = moneyPln;
            this.info = info;
            this.addInfo = addInfo;
        }

        public String getCurr() {
            return curr;
        }

        public void setCurr(String curr) {
            this.curr = curr;
        }

        public BigDecimal getMoney() {
            if (money == null && rate != null && value != null) {
                return rate.multiply(value).setScale(2, RoundingMode.HALF_UP);
            }
            return money;
        }

        public String getMoneyLiterally() {
            return AbstractDelegationLogic.moneyValueLiterally(getMoney());
        }

        public void setMoney(BigDecimal money) {
            this.money = money;
        }

        public BigDecimal getRate() {
            return rate;
        }

        public void setRate(BigDecimal rate) {
            this.rate = rate;
        }

        public BigDecimal getValue() {
            return value;
        }

        public void setValue(BigDecimal value) {
            this.value = value;
        }

        public String getAddInfo() {
            return addInfo;
        }

        public void setAddInfo(String addInfo) {
            this.addInfo = addInfo;
        }

        public String getInfo() {
            return info;
        }

        public void setInfo(String info) {
            this.info = info;
        }

        public BigDecimal getMoneyPln() {
            return moneyPln;
        }
    }

    AbroadCostDict getAbroadCostDict() {
        AbroadCostDict dict = new AbroadCostDict();
        try {
            List<Map<String, Object>> dicLists = (List<Map<String, Object>>) fm.getValue(AbstractDelegationLogic.KALKULACJA_KOSZTOW);
            if (dicLists != null) {
                for (Map<String, Object> item : dicLists) {
                    Object costEnum = item.get(AbstractDelegationLogic.KALKULACJA_KOSZTOW + "_ENUM");
                    Object currencyEnum = item.get(AbstractDelegationLogic.KALKULACJA_KOSZTOW + "_CURRENCY");
                    if (costEnum != null && currencyEnum != null) {
                        String enumId = ((EnumValues) costEnum).getSelectedId();
                        String currencySymbol = getFormattedCurrency((EnumValues) currencyEnum);

                        dict.addMoney(enumId, (BigDecimal) item.get(AbstractDelegationLogic.KALKULACJA_KOSZTOW + "_MONEY_VALUE"), currencySymbol);
                        dict.addMoneyPln(enumId, (BigDecimal) item.get(AbstractDelegationLogic.KALKULACJA_KOSZTOW + "_MONEY_VALUE_PLN"));
                        dict.addInfo(enumId, ObjectUtils.toString(item.get(AbstractDelegationLogic.KALKULACJA_KOSZTOW + "_DESCRIPTION")));
                    }
                }
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
        return dict;
    }

    public Map<String,AcceptInfoAttribute> getAcceptInfo() {
        AcceptInfo result = new AcceptInfo();
        try {
            List<DocumentAcceptance> acceptances = DocumentAcceptance.find(doc.getId());
            for (DocumentAcceptance acc : acceptances) {
                result.put(acc.getAcceptanceCn(), DSUser.findByUsername(acc.getUsername()).asFirstnameLastname(), acc.getAcceptanceTime());
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    public class AcceptInfo extends HashMap<String, AcceptInfoAttribute> {

        public AcceptInfoAttribute put(String key, String name, Date date) {
            AcceptInfoAttribute attr;
            if (this.containsKey(key)) {
                attr = this.get(key);
            } else {
                this.put(key, attr = new AcceptInfoAttribute());
            }
            attr.names.add(name);
            attr.dates.add(date);
            return attr;
        }

    }

    public class AcceptInfoAttribute {
        List<String> names = Lists.newLinkedList();
        List<Date> dates = Lists.newLinkedList();
        String splitter = ", ";
        String dateSplitter = " dnia ";

        public String getName() {
            return Joiner.on(splitter).skipNulls().join(names);
        }

        public String getInfo() {
            Iterable<String> info = new Iterable<String>() {
                @Override
                public Iterator<String> iterator() {
                    return new Iterator<String>() {
                        int index = 0;
                        String name,date;
                        @Override
                        public boolean hasNext() {
                            name = names.size() > index ? names.get(index) : null;
                            date = dates.size() > index ? DateUtils.formatCommonDate(dates.get(index)) : "";
                            index++;
                            return name != null ? true : false;
                        }

                        @Override
                        public String next() {
                            return name + dateSplitter + date;
                        }

                        @Override
                        public void remove() {
                        }
                    };
                }
            };
            return Joiner.on(splitter).skipNulls().join(info);
        }

    }

    public String getAbroadDestination() {
        StringBuilder result = new StringBuilder();
        try {
            List<Map<String, Object>> destinations = (List<Map<String, Object>>) fm.getValue("KRAJ_WYJAZDU");
            if (destinations != null) {
                for (Map<String, Object> destination : destinations) {
                    result.append(destination.get("KRAJ_WYJAZDU_CITY"));
                    result.append(" - ");
                    result.append(destination.get("KRAJ_WYJAZDU_ORGANIZATION"));
                    result.append(", ");
                }
                if (result.length() > 1) {
                    result.delete(result.length()-2, result.length());
                }
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return result.toString();
    }

    public class AbroadCostDict extends HashMap<String,AbroadPreCostItem> {
        boolean addMoney(String enumId, BigDecimal money, String currencySymbol) {
            if (containsKey(enumId)) {
                AbroadPreCostItem cost = get(enumId);
                cost.addMoney(money, currencySymbol);
                return true;
            } else {
                put(enumId, new AbroadPreCostItem().setEnumId(enumId).addMoney(money, currencySymbol));
                return false;
            }
        }

        boolean addInfo(String enumId, String info) {
            AbroadPreCostItem cost = get(enumId);
            if (cost != null) {
                cost.addInfo(info);
                return true;
            } else {
                put(enumId, new AbroadPreCostItem().setEnumId(enumId).addInfo(info));
                return false;
            }
        }

        boolean addMoneyPln(String enumId, BigDecimal money) {
            AbroadPreCostItem cost = get(enumId);
            if (cost != null) {
                cost.addMoneyPln(money);
                return true;
            } else {
                put(enumId, new AbroadPreCostItem().setEnumId(enumId).setMoneyPln(money));
                return false;
            }
        }

        public AbroadPreCostItem e(String keysRaw) {
            String[] keys = keysRaw.split(",");
            AbroadPreCostItem result = new AbroadPreCostItem();
            if (keys.length == 1) {
                AbroadPreCostItem abroadPreCostItem = super.get(keys[0]);
                if (abroadPreCostItem != null) {
                   result = abroadPreCostItem;
                }
            } else if (keys.length > 1) {
                for (String key : keys) {
                    AbroadPreCostItem abroadPreCostItem = super.get(key);
                    if (abroadPreCostItem != null) {
                        result.addMoneyValues(abroadPreCostItem);
                    }
                }
            }
            return result;
        }
    }

    public static class AmountExtended {
        private BigDecimal amount;
        private String currency;
        private BigDecimal plnAmount;
        private String info;


        AmountExtended(BigDecimal amount) {
            this.amount = amount;
        }

        public AmountExtended(BigDecimal amount, String currency) {
            this.amount = amount;
            this.currency = currency;
        }

        public AmountExtended(BigDecimal amount, String currency, String info) {
            this.amount = amount;
            this.currency = currency;
            this.info = info;
        }

        public AmountExtended(BigDecimal amount, BigDecimal plnAmount, String currency, String info) {
            this.amount = amount;
            this.plnAmount = plnAmount;
            this.currency = currency;
            this.info = info;
        }

        AmountExtended() {

        }

        public BigDecimal getAmount() {
            return amount;
        }

        public String getAmountLiterally() throws UnsupportedEncodingException {
            if (amount == null) {
                return "";
            }
            return AbstractDelegationLogic.moneyValueLiterally(amount);
        }

        public AmountExtended setPlnAmount(BigDecimal plnAmount) {
            this.plnAmount = plnAmount;
            return this;
        }

        public String getCurrency() {
            return currency;
        }

        public String getInfo() {
            return info;
        }

        public BigDecimal getPlnAmount() {
            return plnAmount;
        }
    }

    public class AbroadPreCostItem {
        private String enumId;
        private Map<String, BigDecimal> money = Maps.newTreeMap();
        private BigDecimal moneyPln;
        private List<String> info = Lists.newArrayList();

        public String getEnumId() {
            return enumId;
        }

        public AbroadPreCostItem setEnumId(String enumId) {
            this.enumId = enumId;
            return this;
        }

        public String getMoneyF() {
            StringBuilder sb = new StringBuilder();
            for (String symbol : money.keySet()) {
                if (money.get(symbol) != null) {
                    if (sb.length() > 0) {
                        sb.append("\\par ");
                    }
                    sb.append(moneyFormatter.format(money.get(symbol))).append(" ").append(symbol);
                }
            }
            return sb.length() == 0 ? "" : sb.toString();
        }

        public BigDecimal getMoneyPln() {
            return moneyPln;
        }

        public String getMoneyPlnF() {
            return moneyPln != null ? moneyFormatter.format(moneyPln) : "";
        }

        public String getInfo() {
            return Joiner.on(", ").skipNulls().join(info);
        }

        public AbroadPreCostItem setMoneyPln(BigDecimal moneyPln) {
            this.moneyPln = moneyPln;
            return this;
        }

        public void addMoneyPln(BigDecimal moneyPln) {
            if (getMoneyPln() != null) {
                setMoneyPln(moneyPln.add(getMoneyPln()));
            } else {
                setMoneyPln(moneyPln);

            }
        }

        public AbroadPreCostItem addMoney(BigDecimal moneyValue, String currencySymbol) {
            BigDecimal bigDecimal = money.get(currencySymbol);
            if (bigDecimal != null) {
                money.put(currencySymbol, bigDecimal.add(moneyValue));
            } else {
               money.put(currencySymbol, moneyValue);
            }
            return this;
        }

        public AbroadPreCostItem addInfo(String info) {
            this.info.add(info);
            return this;
        }

        public void addMoneyValues(AbroadPreCostItem abroadPreCostItem) {
            if (abroadPreCostItem.getMoneyPln() != null) {
                addMoneyPln(abroadPreCostItem.getMoneyPln());
            }
            for (String symbol : abroadPreCostItem.money.keySet()) {
                if (abroadPreCostItem.money.get(symbol) != null) {
                    addMoney(abroadPreCostItem.money.get(symbol), symbol);
                }
            }
        }
    }

    public class Delegated {
        private String firstlastname;
        private String description;

        public String getDescription() {
            return description;
        }

        public Delegated setDescription(String description) {
            this.description = description;
            return this;
        }

        public String getFirstlastname() {
            return firstlastname;
        }

        public Delegated setFirstlastname(String firstlastname) {
            this.firstlastname = firstlastname;
            return this;
        }
    }

    Date getDocTemplateCreateDate() {
        return new java.util.Date();
    }

    Date getDocCreateDate() {
        return doc.getCtime();
    }

    String getTimeFormatFromDate(String fieldCn) {
        try {
            Object data = fm.getKey(fieldCn);
            if (data != null && data instanceof Date) {
                return DateUtils.formatCommonTime((Date) data);
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }

        return "";
    }

    ArrayList<PodrozItem> getPodrozItems() {
        ArrayList<PodrozItem> ret = new ArrayList<PodrozItem>();
        try {
            List<HashMap<String, Object>> lista = (List<HashMap<String, Object>>) fm.getValue(AbstractDelegationLogic.KOSZTY_PODROZY);
            if (lista != null) {
                for (HashMap<String, Object> transport : lista) {
                    String from = (String) transport.get(AbstractDelegationLogic.KOSZTY_PODROZY + "_FROM_PLACE");
                    String to = (String) transport.get(AbstractDelegationLogic.KOSZTY_PODROZY + "_TO_PLACE");
                    String start = DateUtils.formatCommonDateTime((Date) transport.get(AbstractDelegationLogic.KOSZTY_PODROZY + "_FROM_DATE"));
                    String end = DateUtils.formatCommonDateTime((Date) transport.get(AbstractDelegationLogic.KOSZTY_PODROZY + "_TO_DATE"));

                    String srodek = transport.get(AbstractDelegationLogic.KOSZTY_PODROZY + "_TRANSPORT") != null ? ((EnumValues) transport.get(AbstractDelegationLogic.KOSZTY_PODROZY + "_TRANSPORT")).getSelectedValue() : "";

                    String opis = (String) transport.get(AbstractDelegationLogic.KOSZTY_PODROZY + "_OPIS");
                    String koszt = transport.get(AbstractDelegationLogic.KOSZTY_PODROZY + "_KOSZT") != null ? ((BigDecimal) transport.get(AbstractDelegationLogic.KOSZTY_PODROZY + "_KOSZT")).toPlainString() : "";
                    ret.add(new PodrozItem(from, to, srodek, start, end, opis, koszt));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ret;
    }
    public class PodrozItem {

        private String miWy, miPr, srodek;
        private String wyj, prz;
        private String opis, koszt;

        public PodrozItem() {

        }

        public PodrozItem(String miWy, String miPr, String srodek, String wyj, String prz, String opis, String koszt) {
            super();
            this.miWy = miWy;
            this.miPr = miPr;
            this.setSrodek(srodek);
            this.wyj = wyj;
            this.prz = prz;
            this.opis = opis;
            this.koszt = koszt;
        }

        public String getMiWy() {
            return miWy;
        }

        public void setMiWy(String miWy) {
            this.miWy = miWy;
        }

        public String getMiPr() {
            return miPr;
        }

        public void setMiPr(String miPr) {
            this.miPr = miPr;
        }

        public String getWyj() {
            return wyj;
        }

        public void setWyj(String wyj) {
            this.wyj = wyj;
        }

        public String getPrz() {
            return prz;
        }

        public void setPrz(String prz) {
            this.prz = prz;
        }

        public String getKoszt() {
            return koszt;
        }

        public void setKoszt(String koszt) {
            this.koszt = koszt;
        }

        public String getOpis() {
            return opis;
        }

        public void setOpis(String opis) {
            this.opis = opis;
        }

        public String getSrodek() {
            return srodek;
        }

        public void setSrodek(String srodek) {
            this.srodek = srodek;
        }

    }


}