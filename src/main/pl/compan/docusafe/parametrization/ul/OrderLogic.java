package pl.compan.docusafe.parametrization.ul;

import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.primitives.Ints;
import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.exports.*;
import pl.compan.docusafe.core.imports.simple.repository.CacheHandler;
import pl.compan.docusafe.core.imports.simple.repository.Dictionary;
import pl.compan.docusafe.core.imports.simple.repository.Dictionary.SourceType;
import pl.compan.docusafe.core.imports.simple.repository.SimpleRepositoryAttribute;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.parametrization.ul.hib.ProjectBudgetPositionResources;
import pl.compan.docusafe.parametrization.ul.hib.UlBudgetPosition;
import pl.compan.docusafe.parametrization.ul.hib.UlBudgetsFromUnits;
import pl.compan.docusafe.parametrization.ul.jbpm.export.ExportDocumentDecision;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OrderLogic extends AbstractUlDocumentLogic {

    private static final String KWOTA_EURO_FIELD = "KWOTA_EURO";
    public static final String TENDER_AUTHORIZED_ACCEPT_TASK_NAME = "tender_authorized_accept";

    private static final String ORDER_DOCUMENT_TYPES_TABLE_NAME = "dsg_ul_order_document_types";
    public static final long ORDER_POSITION_CONTEXT_ID = 32l;
    public static final String CASE_NUMBER_FIELD_CN = "NR_SPRAWY";

    protected static final Logger log = LoggerFactory.getLogger(OrderLogic.class);

    public static final String DOC_KIND_CN = "ul_order";
    public static final String BP_FIELD = "BP";

    private static final String EXCHANGE_RATE_TABLE = "dsg_ul_exchange_rate";
	private static final String EXCHANGE_RATE_SERVICE_TABLE = "dsg_ul_services_exchange_rate";

    private static final String BUDGET_TYPE_ID_VAR_NAME = "budgetTypeId";
	public static final String BP_VARIABLE_NAME = "costCenterId";
	
	
	
	
	static final String BUDGET_OWNER_ACCEPT_TASK_NAME = "budget_owner_accept";
	private static final String CHANCELLOR_TASK_NAME = "chancellor";
	private static final String QUESTOR_TASK_NAME = "questor";
	public static final String BUDGET_TYPE_ACCEPTANCE_TASK_NAME = "budget_type_accept";
	public static final String BUDGET_ACCEPTANCE_TASK_NAME = "budget_accept";
	public static final String BUDGET_ACCEPTANCE_ADD_TASK_NAME = "budget_accept_additional";
	
	public static final String UPOWAZNIONY_KWESTOR_DICTIONARY_FIELD_NAME = "UPOWAZNIONY_KWESTOR_DIC";
	private static final String TYPY_DZIALALNOSCI_FIELD_NAME = "I_TYPE";
	private static final String RODZAJ_DZIALALNOSCI_FIELD_NAME = "I_KIND";
	private static final String KWOTA_POZOSTALA_MPK_FIELD = "KWOTA_POZOSTALA_MPK";
	public static final String UPOWAZNIONY_PROJEKT_FIELD = "UPOWAZNIONY_PROJEKT";
	private static final String DWR_PREFIX = "DWR_";

	private static final String KWOTA_EURO_DWR_FIELD = DWR_PREFIX+KWOTA_EURO_FIELD;
	private static final String STAWKI_VAT_DWR_FIELD_NAME = DWR_PREFIX+"STAWKI_VAT";
	private static final String STAWKI_VAT_FIELD_NAME = "STAWKI_VAT";
	private static final String KWOTA_POZOSTALA_MPK_DWR_FIELD = DWR_PREFIX+KWOTA_POZOSTALA_MPK_FIELD;
	private static final String UPOWAZNIONY_KANCLERZ_FIELD = "UPOWAZNIONY_KANCLERZ";
	private static final String UPOWAZNIONY_KWESTOR_FIELD = "UPOWAZNIONY_KWESTOR";
	private static final String OBSZAR_FIELD = "OBSZAR";
    private static final String KWOTA_NETTO_FIELD = "KWOTA_NETTO";
	private static final String KWOTA_BRUTTO_FIELD = "KWOTA_BRUTTO";
	private static final String RODZAJ_DZIALALNOSCI_FIELD = "RODZAJ_DZIALALNOSCI";
	private static final String KWOTA_BRUTTO_SLOWNIE = "KWOTA_BRUTTO_SLOWNIE";
	private static final String TERMIN_PLATNOSCI_FIELD = "TERMIN_PLATNOSCI";
	private static final String DATA_WYSTAWIENIA_FIELD = "DATA_WYSTAWIENIA";
	private static final String DATA_WPLYWU_FIELD = "DATA_WPLYWU_FAKTURY";
	private static final String BP_DWR_FIELD = DWR_PREFIX+BP_FIELD;
	private static final String TERMIN_PLATNOSCI_DWR_FIELD = DWR_PREFIX+TERMIN_PLATNOSCI_FIELD;
	private static final String DATA_WPLYWU_DWR_FIELD = DWR_PREFIX+DATA_WPLYWU_FIELD;
	private static final String DATA_WYSTAWIENIA_DWR_FIELD = DWR_PREFIX+DATA_WYSTAWIENIA_FIELD;
	private static final String KWOTA_NETTO_DWR_FIELD = DWR_PREFIX+"KWOTA_NETTO";
	private static final String KWOTA_WALUTOWA_DWR_FIELD = DWR_PREFIX+"KWOTA_WALUTOWA";
	private static final String KWOTA_KURS_DWR_FIELD = DWR_PREFIX+"KWOTA_KURS";
	private static final String KWOTA_BRUTTO_DWR_FIELD = DWR_PREFIX+KWOTA_BRUTTO_FIELD;

    private static final String BP_SUM_ERROR_MSG = "Suma kwot brutto tabeli pozycji bud�etowych musi by� r�wna kwocie brutto zapotrzebowania. ";

    private static final EnumValues EMPTY_NOT_HIDDEN_ENUM_VALUE = new EnumValues(new HashMap<String,String>(){{put("","-- wybierz --");}}, new ArrayList<String>(){{add("");}});
	private static final EnumValues NOT_APPLICABLE_ENUM_VALUE = new EnumValues(new HashMap<String,String>(), new ArrayList<String>());

    protected static final SortedSet<Integer> CASE_NUMBERS = Sets.newTreeSet();
    public static final String GET_LAST_CASE_NUMBER_SQL = " select max(cast(SUBSTRING(nr_sprawy, 1, CHARINDEX('/',nr_sprawy)-1) as int))\n" +
            " from dsg_ul_order\n" +
            " where SUBSTRING(nr_sprawy, CHARINDEX('/',nr_sprawy)+1, LEN(nr_sprawy)) is not null and SUBSTRING(nr_sprawy, CHARINDEX('/',nr_sprawy)+1, LEN(nr_sprawy)) = ?;";
    public static final String ERP_STATUS = "ERP_STATUS";
    protected static final String NOT_APPLICABLE_LABEL = "nie dotyczy";

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();
        for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
            if (f.getDefaultValue() != null)
                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));

        String currentYear;
        try {
            currentYear = String.valueOf(GlobalPreferences.getCurrentYear());
        } catch (GlobalPreferences.CurrentYearNotOpenException e) {
            currentYear =  DateUtils.formatYear(new Date());
        }

        Integer lastCaseNumber = getCurrentCaseNumber(currentYear);

        toReload.put(CASE_NUMBER_FIELD_CN, lastCaseNumber+"/"+currentYear);

        fm.reloadValues(toReload);
    }

    private Integer getCurrentCaseNumber(String currentYear) throws EdmException {
        Integer lastCaseNumber = null;

        boolean isOpened = false;

        PreparedStatement ps = null;
        ResultSet rs;
        try {
            try {
                if (!DSApi.isContextOpen()) {
                    DSApi.openAdmin();
                    isOpened = true;
                }
            } catch (Throwable e) {
            }

            ps = DSApi.context().prepareStatement(GET_LAST_CASE_NUMBER_SQL);

            ps.setString(1, currentYear);

            rs = ps.executeQuery();

            if (rs.next()) {
                lastCaseNumber = Integer.valueOf(Objects.firstNonNull(rs.getString(1), "0"));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            if (DSApi.isContextOpen()) {
                DSApi.context().closeStatement(ps);
            }

            if (isOpened) {
                DSApi.close();
            }
        }

        synchronized (CASE_NUMBERS) {
            if (lastCaseNumber == null) {
                lastCaseNumber = CASE_NUMBERS.isEmpty() ? 0 : CASE_NUMBERS.last();
            }
            while (!CASE_NUMBERS.add(++lastCaseNumber));
        }

        return lastCaseNumber;
    }

    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
        String caseNumberWithYear = document.getFieldsManager().getStringKey(CASE_NUMBER_FIELD_CN);
        if (Strings.isNullOrEmpty(caseNumberWithYear)) {
            String caseNumber = caseNumberWithYear.split("/")[0];
            synchronized (CASE_NUMBERS) {
                CASE_NUMBERS.remove(caseNumber);
            }
        }

        super.onStartProcess(document, event);
    }

    @Override
    public ExportedDocument buildExportDocument(OfficeDocument doc) throws EdmException {
        FieldsManager fm = doc.getFieldsManager();

        ExportedDocument exported = buildReservationDocument(doc, fm);

/*        List<Long> pozycjeFaktury = (List) fm.getKey(BP_FIELD);
        if (pozycjeFaktury != null) {
            UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
            for (Long bpId : pozycjeFaktury) {
                UlBudgetPosition bp = bpInstance.find(bpId);

                if (bp.getCStructureBudgetKo() != null) {
                    exported = buildReservationDocument(doc, fm);
                }
                break;
            }
        }

        if (exported == null) {
            exported = buildOrderDocument(doc, fm);

            DSUser cUser = DSUser.findByUsername(doc.getCreatingUser());
            if (cUser.getExtension() != null) {
                exported.setCreatingUser(cUser.getExtension());
            }
        }*/



/*        if (fm.getKey(ERP_STATUS) != null) {
            exported = buildReservationDocument(doc, fm);
        } else {
            exported = buildOrderDocument(doc, fm);

            DSUser cUser = DSUser.findByUsername(doc.getCreatingUser());
            if (cUser.getExtension() != null) {
                exported.setCreatingUser(cUser.getExtension());
            }
        }*/


        String systemPath;

        if (Docusafe.getAdditionProperty("erp.faktura.link_do_skanu") != null) {
            systemPath = Docusafe.getAdditionProperty("erp.faktura.link_do_skanu");
        } else {
            systemPath = Docusafe.getBaseUrl();
        }

        for (Attachment zal : doc.getAttachments()) {
            AttachmentInfo info = new AttachmentInfo();

            String path = systemPath+ "/repository/view-attachment-revision.do?id=" + zal.getMostRecentRevision().getId();

            info.setName(zal.getTitle());
            info.setUrl(path);

            exported.addAttachementInfo(info);
        }

        return exported;
    }

    private OrderDocument buildOrderDocument(OfficeDocument doc, FieldsManager fm) throws EdmException {
        boolean processNotApplicableValue = false;
        Integer notApplicableValue = -1;

        OrderDocument exported = new OrderDocument();

        exported.setPurchaseType(fm.getEnumItem("DOC_TYPE") != null ? fm.getEnumItem("DOC_TYPE").getCn() : "ZPD");

        exported.setContractor(doc.getSenderId());
        exported.setCreated(doc.getCtime());

        exported.setExcution((Date) fm.getKey("REALIZACJA"));

        exported.setMpkCode(fm.getEnumItem("MPK") != null ? fm.getEnumItem("MPK").getCn() : null);
        exported.setOrderDescription(fm.getEnumItem("SZCZEGOLY_ZAMOWIENIA") != null ? fm.getEnumItem("SZCZEGOLY_ZAMOWIENIA").getTitle() : null);
        exported.setOrderKind(fm.getEnumItem("RODZAJ_ZAMOWIENIA") != null ? fm.getEnumItem("RODZAJ_ZAMOWIENIA").getTitle() : null);
        exported.setOrderKind(fm.getEnumItem("TRYB_ZAMOWIENIA") != null ? fm.getEnumItem("TRYB_ZAMOWIENIA").getTitle() : null);
        exported.setOrderName(fm.getStringValue("NAZWA_ZAMOWIENIA"));
        exported.setOrderSubjectDescription(fm.getStringValue("SZCZEGOLOWY_OPIS"));
        exported.setTitle(fm.getStringValue("NR_SPRAWY"));

        exported.setDescription(getAdditionFieldsDescription(doc, fm));

        Integer docTypeObjectId = getObjectIdFromFM(fm);
        List<SimpleRepositoryAttribute> repositoryDicts = CacheHandler.getRepositoryDicts(docTypeObjectId, (int) ORDER_POSITION_CONTEXT_ID);

        List<Long> pozycjeFaktury = (List) fm.getKey(BP_FIELD);
        if (pozycjeFaktury != null) {
            int i = 0;
            List<BudgetItem> budgetItems = Lists.newArrayList();
            UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
            for (Long bpId : pozycjeFaktury) {
                UlBudgetPosition bp = bpInstance.find(bpId);
                if (bp.getINo() != null) {
                    BudgetItem bItem = new BudgetItem();
                    int nrp = i + 1;
                    bItem.setItemNo(nrp);
                    bItem.setName("-");
                    bItem.setUnit("szt");
                    bItem.setQuantity(BigDecimal.ONE);

                    bItem.setNetAmount(bp.getIAmount().setScale(2));

                    // kontrakt_id - projekt
                    bItem.setProjectCode(getCnValue(PROJECTS_TABLE_NAME, bp.getCProject(), processNotApplicableValue, notApplicableValue));

                    // komorka_id
                    bItem.setMpkCode(getCnValue(ORGANIZATIONAL_UNIT_TABLE_NAME, bp.getCMpk(), processNotApplicableValue, notApplicableValue));

                    if (bp.getCType() != null && -1 != bp.getCType()) {
                        Integer id = DataBaseEnumField.getEnumItemForTable(CostInvoiceLogic.PRODUCT_TABLE_NAME, bp.getCType()).getCentrum();
                        if (id != null && 0 != id) {
                            bItem.setCostKindId(id.longValue());
                        }
                    }

                    // projekt_id
                    bItem.setBudgetCode(getCnValue(CostInvoiceLogic.PROJECT_BUDGETS_TABLE_NAME, bp.getCBudget(), processNotApplicableValue, notApplicableValue));

                    // etap_id
                    if (bp.getCPosition() != null) {
                        bItem.setPositionId(Long.valueOf((Integer) bp.getCPosition()));
                    }

                    // zasob_id
                    if (bp.getCResource() != null) {
                        try {
                            ProjectBudgetPositionResources resource = ProjectBudgetPositionResources.findByErpId(bp.getCResource().longValue());
                            bItem.setResourceId(resource.getZasobId());
                        } catch (EntityNotFoundException e) {
                            log.error("CAUGHT " + e.getMessage(), e);
                        }
                    }

                    if (bp.getCFundSource() != null) {
                        FundSource fItem = new FundSource();
                        fItem.setFundPercent(new BigDecimal(100));

                        // zrodlo finansowania
                        // je?li -1, czyli "nie dotyczy" to nie exportuj
                        fItem.setCode(getCnValue(FUND_SOURCES_TABLE_NAME, bp.getCFundSource(), processNotApplicableValue, notApplicableValue));

                        bItem.setFundSources(Lists.newArrayList(fItem));
                    }

                    for (SimpleRepositoryAttribute dict : repositoryDicts) {
                        Long value = getRepositoryAttributeValue(bpId, dict, log);
                        if (value != null) {
                            bItem.addRepositoryItem(new RepositoryItem(dict.getRepAtrybutId(), value));
                        }
                    }

                    budgetItems.add(bItem);
                    i++;
                }
            }
            exported.setBudgetItems(budgetItems);
        }
        return exported;
    }

    private ReservationDocument buildReservationDocument(OfficeDocument doc, FieldsManager fm) throws EdmException{
        ReservationDocument exported = new ReservationDocument();

        boolean processNotApplicableValue = false;
        Integer notApplicableValue = -1;

        exported.setReservationStateFromId(fm.getIntegerKey(ERP_STATUS));

        exported.setReservationNumber(fm.getStringKey(ExportDocumentDecision.ERP_DOCUMENT_IDM));

        //String mpkCode = fm.getEnumItem("MPK") != null ? fm.getEnumItem("MPK").getArg(0) : null;

        exported.setBudgetReservationTypeCode(fm.getEnumItemCn("TYP_REZERWACJI"));
        //exported.setBudgetReservationTypeCode(Docusafe.getAdditionPropertyOrDefault("export.reservation_type.code", "WR_PK"));
        exported.setReservationMethod(ReservationDocument.ReservationMethod.AUTO);


        List<Long> pozycjeFaktury = (List) fm.getKey(BP_FIELD);
        if (pozycjeFaktury != null) {
            int i = 0;
            List<BudgetItem> budgetItems = Lists.newArrayList();
            UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
            for (Long bpId : pozycjeFaktury) {
                UlBudgetPosition bp = bpInstance.find(bpId);
                if (bp.getINo() != null) {
                    BudgetItem bItem = new BudgetItem();
                    int nrp = i + 1;
                    bItem.setItemNo(nrp);
                    bItem.setQuantity(BigDecimal.ONE);

                    bItem.setNetAmount(bp.getIAmount().setScale(2));

                    // kontrakt_id - projekt
                    bItem.setProjectCode(getCnValue(PROJECTS_TABLE_NAME, bp.getCProject(), processNotApplicableValue, notApplicableValue));

                    // komorka_id
                    bItem.setMpkCode(getCnValue(ORGANIZATIONAL_UNIT_TABLE_NAME, bp.getCMpk(), processNotApplicableValue, notApplicableValue));

                    if (bp.getCType() != null && -1 != bp.getCType()) {
                        Integer id = DataBaseEnumField.getEnumItemForTable(CostInvoiceLogic.PRODUCT_TABLE_NAME, bp.getCType()).getCentrum();
                        if (id != null && 0 != id) {
                            bItem.setCostKindId(id.longValue());
                            exported.setCostKindCodeAllowed(true);
                        }
                    } else {
                        bItem.setCostKindName(bp.getCKindName());
                    }


                    // projekt_id
                    bItem.setBudgetCode(getCnValue(CostInvoiceLogic.PROJECT_BUDGETS_TABLE_NAME, bp.getCBudget(), processNotApplicableValue, notApplicableValue));

                    // etap_id
                    if (bp.getCPosition() != null) {
                        bItem.setPositionId(Long.valueOf(bp.getCPosition()));
                    }


                    if (bp.getCStructureBudgetPositionRaw() != null) {
                        try {
                            Optional<pl.compan.docusafe.core.dockinds.field.Field> field = findField("ul_order", BP_FIELD, "C_STRUCTURE_BUDGET_POSITION_RAW_1");

                            if (field.isPresent()) {
                                field.get().getEnumItemByCn(bp.getCStructureBudgetPositionRaw());
                                if (field.get().getEnumItemByCn(bp.getCStructureBudgetPositionRaw()).getId() !=null) {
                                    bItem.setBudgetKindId(0l+field.get().getEnumItemByCn(bp.getCStructureBudgetPositionRaw()).getId());
                                }
                            } else {
                                log.error("cant getn C_STRUCTURE_BUDGET_POSITION_RAW_1 field");
                            }

                        } catch (EntityNotFoundException e) {
                            log.error("CAUGHT " + e.getMessage(), e);
                        }
                    }

                    bItem.setBudgetKoId(bp.getCStructureBudgetKo() != null ? bp.getCStructureBudgetKo().longValue() : null);
                    if (exported.getHeaderBudget().getBudgetKoId() == null && bItem.getBudgetKoId() != null) {
                        exported.getHeaderBudget().setBudgetKoId(bItem.getBudgetKoId());
                    }

                    if (bItem.getBudgetKoId() != null) {
                        List<UlBudgetsFromUnits> items = DictionaryUtils.findByGivenFieldValue(UlBudgetsFromUnits.class, "erpId", bItem.getBudgetKoId());
                        UlBudgetsFromUnits item = Iterables.getFirst(items, null);
                        Preconditions.checkNotNull(item, "cant find BudgetsFromUnitsImport, erpId"+bItem.getBudgetKoId());
                        bItem.setBudgetDirectCode(item.getBudzetIdm());
                    }

                    if (exported.getHeaderBudget().getBudgetDirectCode() == null) {
                        exported.getHeaderBudget().setBudgetDirectCode(bItem.getBudgetDirectCode());
                    }

/*
                    bItem.setBudgetKindId(bp.getCStructureBudgetPosition() != null ? bp.getCStructureBudgetPosition().longValue() : null);
                    if (exported.getHeaderBudget().getBudgetKindId() == null && bItem.getBudgetKindId() != null) {
                        exported.getHeaderBudget().setBudgetKindId(bItem.getBudgetKindId());
                    }
*/

                    //List<StructureBudget> founds = DictionaryUtils.findByGivenFieldValues(StructureBudget.class, ImmutableMap.of("bdBudzetId", bItem.getBudgetKoId()));
                    //bItem.setBudgetCellCode(founds.get(0).getBdStrBudzetIdn());

                    // zasob_id
                    if (bp.getCResourceRaw() != null) {
                        try {
                            Optional<pl.compan.docusafe.core.dockinds.field.Field> field = findField("ul_order", BP_FIELD, "C_RESOURCE_RAW_1");

                            if (field.isPresent()) {
                                field.get().getEnumItemByCn(bp.getCResourceRaw());
                                if (field.get().getEnumItemByCn(bp.getCResourceRaw()).getId() !=null) {
                                    bItem.setResourceId(0l+field.get().getEnumItemByCn(bp.getCResourceRaw()).getId());
                                }
                                //ProjectBudgetPositionResources resource = ProjectBudgetPositionResources.findByErpId(bp.getCResource().longValue());
                                //bItem.setResourceId(resource.getZasobId());
                            } else {
                                log.error("cant getn RESOURCE field");
                            }

                        } catch (EntityNotFoundException e) {
                            log.error("CAUGHT " + e.getMessage(), e);
                        }
                    }

                    if (bp.getCFundSource() != null) {
                        FundSource fItem = new FundSource();
                        fItem.setFundPercent(new BigDecimal(100));

                        // zrodlo finansowania
                        // je?li -1, czyli "nie dotyczy" to nie exportuj
                        fItem.setCode(getCnValue(FUND_SOURCES_TABLE_NAME, bp.getCFundSource(), processNotApplicableValue, notApplicableValue));
                        fItem.setKind(1);

                        bItem.setFundSources(Lists.newArrayList(fItem));
                    }

                    budgetItems.add(bItem);
                    i++;
                }
            }
            exported.setPostionItems(budgetItems);
        }

/*        if (fm.getKey("SUPPLIER") != null) {
            exported.setExternalContractor(AMSSupplier.find(((Number) fm.getKey("SUPPLIER")).longValue()).getErpId().longValue());
        }*/

        exported.setCreated(doc.getCtime());

/*        exported.setOrderKind(fm.getEnumItem("RODZAJ_ZAMOWIENIA") != null ? fm.getEnumItem("RODZAJ_ZAMOWIENIA").getTitle() : null);
        exported.setOrderKind(fm.getEnumItem("TRYB_ZAMOWIENIA") != null ? fm.getEnumItem("TRYB_ZAMOWIENIA").getTitle() : null);
        exported.setOrderName(fm.getStringValue("NAZWA_ZAMOWIENIA"));
        exported.setOrderSubjectDescription(fm.getStringValue("SZCZEGOLOWY_OPIS"));*/
        exported.setTitle(fm.getStringValue("NR_SPRAWY"));

        exported.setDescription(getAdditionFieldsDescription(doc, fm));

        return exported;
    }

    private String getAdditionFieldsDescription(OfficeDocument doc, FieldsManager fm) throws EdmException {
        Map<String,String> additionFields = Maps.newLinkedHashMap();
        additionFields.put("Nr KO",doc.getOfficeNumber() != null ? doc.getOfficeNumber().toString() : "brak");
        additionFields.put("Numer sprawy",fm.getStringValue("NR_SPRAWY"));
        additionFields.put("Nazwa zam�wienia",fm.getStringValue("NAZWA_ZAMOWIENIA"));
        additionFields.put("Szczeg�owy opis przedmiotu zam�wienia",fm.getStringValue("SZCZEGOLOWY_OPIS"));
        additionFields.put("Uzasadnienie zakupu",fm.getStringValue("UZASADNIENIE"));
        additionFields.put("Rodzaj zam�wienia",fm.getStringValue("RODZAJ_ZAMOWIENIA"));
        additionFields.put("Tryb zam�wienia",fm.getStringValue("TRYB_ZAMOWIENIA"));
        additionFields.put("Szczeg�y zam�wienia",fm.getStringValue("SZCZEGOLY_ZAMOWIENIA"));

        StringBuilder description = new StringBuilder();
        for (String label : additionFields.keySet()) {
            if (!Strings.isNullOrEmpty(additionFields.get(label))) {
                description.append(label)
                        .append(": ")
                        .append(additionFields.get(label))
                        .append("; ");
            }
        }
        return description.toString();
    }

    private Integer getObjectIdFromFM(FieldsManager fm) throws EdmException {
        Integer docTypeObjectId = 1;
        if (fm.getEnumItem("DOC_TYPE") != null) {
            try {
                docTypeObjectId = fm.getEnumItem("DOC_TYPE").getCentrum();
            } catch (NumberFormatException e) {
                log.error("CAUGHT: "+e.getMessage(),e);
            }
        }
        return docTypeObjectId;
    }

    @Override
    public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException {
/*      notatka 01.08
        StringBuilder msgBuilder = new StringBuilder();

        BigDecimal totalVatAmount = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
        // sumowanie kwot poszczegolnych pozycji budzetowych
        if (fieldValues.get("M_DICT_VALUES") != null)
            totalVatAmount = sumDicItems(fieldValues, BP_FIELD, "I_AMOUNT");

        // suma kwot pozycji budzetowych musi byc rowna kwocie wniosku
        if (fieldValues.get(KWOTA_BRUTTO_FIELD) != null && fieldValues.get(BP_FIELD) != null && totalVatAmount != null) {
            if (((BigDecimal) fieldValues.get(KWOTA_BRUTTO_FIELD)).compareTo(totalVatAmount) != 0)
            {
                msgBuilder.append(BP_SUM_ERROR_MSG);
            }
        }

        if (msgBuilder.length() > 0)
            throw new EdmException(msgBuilder.toString());
            */
    }

    @Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
		FieldsManager fm = document.getFieldsManager();

		try {
            List<Long> pozycjeFaktury = (List) fm.getKey(BP_FIELD);
            if (pozycjeFaktury != null) {
                UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
                int maxItemNo = 0;
                Set<Integer> items = Sets.newHashSet();
                for (Long bpId : pozycjeFaktury) {
                    UlBudgetPosition bp = bpInstance.find(bpId);

                    if (bp.getINo() != null) {
                        items.add(bp.getINo());
                        if (bp.getINo() > maxItemNo) {
                            maxItemNo = bp.getINo();
                        }
                    }
                }
                int i = 1;
                for (Long bpId : pozycjeFaktury) {
                    UlBudgetPosition bp = bpInstance.find(bpId);
                    if (bp.getINo() == null) {
                        while (items.contains(i)) {
                            i++;
                        }
                        items.add(i);
                        bp.setINo(i);
                        DSApi.context().session().save(bp);
                        i++;
                    }
                }
            }
        } catch (Exception e) {
			log.error(e.getMessage(),e);
		}
	}

	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
		try {
            checkFilledAmountByAvailableLimits(values);
		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}

/*          ZMIANA z notatki 15.05, pkt.3
			if (values.get("DWR_KWOTA_WALUTA_DATA") != null) {
				setCurrencyParams(values);
			}*/
/*			if (values.get(KWOTA_BRUTTO_DWR_FIELD) != null) {
				values.get(KWOTA_EURO_DWR_FIELD).setMoneyData(DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.DIVIDE, 2, false, values.get(KWOTA_BRUTTO_DWR_FIELD), values.get(KWOTA_KURS_DWR_FIELD)));
			}

            if (values.get(BP_DWR_FIELD) != null && values.get(KWOTA_POZOSTALA_MPK_DWR_FIELD) != null) {
                if (values.get(BP_DWR_FIELD).getType().equals(Field.Type.DICTIONARY) && values.get(BP_DWR_FIELD).getDictionaryData() != null) {
                    setRemainingAmount(values,new StringBuilder());
                }
            }*/
		return null;
	}

    private void checkFilledAmountByAvailableLimits(Map<String, FieldData> values) {
        FieldData positionInfo = values.get("DWR_DOSTEPNE_SRODKI_INFO");
        FieldData dictionaryFieldData = values.get(BP_DWR_FIELD);

        if (positionInfo != null && dictionaryFieldData != null) {
            positionInfo.setStringData("Kwota pozycji jest dost�pna.");

            Map<String, FieldData> dictionaryData = dictionaryFieldData.getDictionaryData();
            Pattern pattern = Pattern.compile(".*(plan-wykonanie: )([0-9]+.[0-9]+)(; )(rezerwacje: )([0-9]+.[0-9]+)");

            for(int i = 1;;i++) {
                FieldData resourceField = dictionaryData.get(BP_FIELD_WITH_LINK + "C_RESOURCE_RAW" + "_" + i);
                FieldData positionField = dictionaryData.get(BP_FIELD_WITH_LINK + "C_STRUCTURE_BUDGET_POSITION_RAW" + "_" + i);
                FieldData amount = dictionaryData.get(BP_FIELD_WITH_LINK + "I_AMOUNT" + "_" + i);

                if ((resourceField == null && positionField == null) || amount == null) {
                    break;
                }

                BigDecimal filledAmount = amount.getMoneyData();

                if (filledAmount == null) {
                    continue;
                }

                BigDecimal positionRemainAmount = null;

                String selectedValue = Iterables.getFirst(resourceField.getEnumValuesData().getSelectedOptions(), null);
                if (selectedValue == null) {
                    selectedValue = Iterables.getFirst(positionField.getEnumValuesData().getSelectedOptions(), null);
                }

                if (!Strings.isNullOrEmpty(selectedValue)) {
                    Matcher matcher = pattern.matcher(selectedValue);
                    if (matcher.find()) {
                        positionRemainAmount = new BigDecimal(matcher.group(2)).subtract(new BigDecimal(matcher.group(5)));
                    }

                    if (positionRemainAmount != null) {
                        if (filledAmount.compareTo(positionRemainAmount) >= 1) {
                            positionInfo.setStringData("Zosta�y przekroczone dost�pne �rodki.");
                            return;
                        }
                    }
                }

            }
        }
    }

    //		column.dockindBusinessAtr3 = Pozcyja bud\u017cetowa: projekt
	//		column.dockindBusinessAtr4 = Pozcyja bud\u017cetowa: kwota
	@Override
	public TaskListParams getTaskListParams(DocumentKind kind, long documentId, TaskSnapshot task) throws EdmException {
		TaskListParams params = super.getTaskListParams(kind, documentId, task);
		
		List<String> mpkAcceptationNames = new ArrayList<String>();
		mpkAcceptationNames.add(BUDGET_OWNER_ACCEPT_TASK_NAME);
		String taskId = task.getActivityKey();
		
		FieldsManager fm = kind.getFieldsManager(documentId);
		
		params.setAttribute("Nie dotyczy", 2);
		params.setAttribute("Nie dotyczy", 3);
		
		UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
		
		if (!mpkAcceptationNames.contains(Jbpm4Provider.getInstance().getTaskService().getTask(taskId).getName())) {
			if (fm != null && fm.getKey(BP_FIELD) != null) {
				Object mpkRaw = fm.getKey(BP_FIELD);
				if (mpkRaw != null && mpkRaw instanceof List<?> && ((List<?>)mpkRaw).size()>0) {
					try {
						String mpkRawId = ((List<?>)mpkRaw).get(0).toString();
						Long firstMpkId = Long.valueOf(mpkRawId);

						UlBudgetPosition bp = bpInstance.find(firstMpkId);

						if (bp != null && bp.getCProject() != null) {
							EnumItem field = DataBaseEnumField.getEnumItemForTable(PROJECTS_TABLE_NAME, bp.getCProject());

							String postfix = ((List<?>) mpkRaw).size() > 1 ? ", ..." : "";

							params.setAttribute(field.getTitle() + postfix, 2);
						}
					} catch (NumberFormatException e) {
						log.error(e.getMessage(),e);
					} catch (EdmException e1) {
						log.error(e1.getMessage(),e1);
					}
				}
			}
		} else {
			String mpkRawId = Jbpm4Provider.getInstance().getTaskService().getVariable(taskId, BP_VARIABLE_NAME) != null ? 
					Jbpm4Provider.getInstance().getTaskService().getVariable(taskId, BP_VARIABLE_NAME).toString() : "";
			if (!mpkRawId.equals("")) {
				try {
					Long bpId = Long.valueOf(mpkRawId);
					UlBudgetPosition bp = bpInstance.find(bpId);

					EnumItem field = DataBaseEnumField.getEnumItemForTable(PROJECTS_TABLE_NAME, Integer.valueOf(bp.getCProject()));

					params.setAttribute(field.getTitle(), 2);

				} catch (EdmException e) {
					log.error(e.getMessage(),e);
				} catch (NumberFormatException e1) {
					log.error(e1.getMessage(),e1);
				}
			}
		}
		// Status
		params.setStatus((String) fm.getValue("STATUS"));
		
		if (Strings.isNullOrEmpty(task.getRealDescription())) {
			task.setDescription("Brak");
		}

		params.setAttribute("Nie dotyczy", 1);
		
		return params;
	}

	@Override
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) {
		Map<String, Object> results = new HashMap<String, Object>();

		try {
			if (dictionaryName.equals(BP_FIELD)) {
                setBudgetItemsRefValuesWithBudgets(dictionaryName, values, results);
                setDictionaries(dictionaryName, values, results);

                String productRawId = Objects.firstNonNull(DwrUtils.getStringValue(values, BP_FIELD_WITH_LINK + "C_TYPE"), "");
                Integer productId = Ints.tryParse(productRawId);
                if (productId != null) {
                    results.put(BP_FIELD_WITH_LINK + "C_KIND_NAME", NOT_APPLICABLE_LABEL);
                }

            }
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		return results;
	}

    private Integer getDocTypeId() {
        WebContext dwrFactory = WebContextFactory.get();
        if (dwrFactory.getSession().getAttribute("dwrSession") instanceof Map<?, ?>) {
            Map<String, Object> dwrSession = (Map<String, Object>) dwrFactory.getSession().getAttribute("dwrSession");
            if (dwrSession.get("FIELDS_VALUES") instanceof Map<?, ?>) {
                Map<String,Object> fieldsValues = (Map<String, Object>) dwrSession.get("FIELDS_VALUES");

                Object docType = fieldsValues.get("DOC_TYPE");
                if (docType != null) {
                    try {
                        return Integer.valueOf(docType.toString());
                    } catch (NumberFormatException e) {
                    }
                }
            }
        }
        return null;
    }

    private void setDictionaries(String dictionaryName, Map<String, FieldData> values, Map<String, Object> results) throws EdmException {
        String dicNameWithSuffix = dictionaryName + "_";
        Integer docTypeId = getDocTypeId();

        if (docTypeId != null) {
            EnumItem docTypeEnum = DataBaseEnumField.getEnumItemForTable(ORDER_DOCUMENT_TYPES_TABLE_NAME, docTypeId);

            clearAllDictionaresFields(DOC_KIND_CN, results);

            if (docTypeEnum != null) {
                Integer docTypeObjectId = docTypeEnum.getCentrum();

                Set<String> repositoryDictsIds = getRepositoryDictsIds(docTypeObjectId, (int)ORDER_POSITION_CONTEXT_ID);

                for (Dictionary dict : CacheHandler.getAvailableDicts(DOC_KIND_CN)) {
                    EnumValues defaultEnum = EMPTY_ENUM_VALUE;

                    if (dict.getDeleted() == false) {
                        if (dict.getDictSource() == SourceType.REPOSITORY.value) {
                            setRepositoryDict(values, results, dicNameWithSuffix, docTypeObjectId, (int)ORDER_POSITION_CONTEXT_ID, repositoryDictsIds,
                                    null, defaultEnum, dict, log);
                        }
                    }
                }
            }
        }
    }

    private void clearAllDictionaresFields(String docKindCn, Map<String, Object> results) {
        for (Dictionary dictionaryToClear : CacheHandler.getAvailableDicts(docKindCn)) {
            results.put(BP_FIELD_WITH_LINK+dictionaryToClear.getCn(), EMPTY_ENUM_VALUE);
        }
    }

    @Override
	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
		if (BUDGET_OWNER_ACCEPT_TASK_NAME.equals(acceptationCn)) {
			try {
				Long bpId = (Long) openExecution.getVariable(BP_VARIABLE_NAME);

				UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
				UlBudgetPosition bp = bpInstance.find(bpId);

				//Z PROJEKTU
				Integer projectId = bp.getCProject();
				if (projectId != null && projectId != -1) {
					Integer userId = DataBaseEnumField.getEnumItemForTable(PROJECTS_TABLE_NAME, projectId).getCentrum();
					try {
						DSUser user = DSUser.findAllByExtension(userId.toString());
						if (!user.isDeleted()) {
							assignable.addCandidateUser(user.getName());
							AssigneeHandler.addToHistory(openExecution, doc, user.getName(), null);
							return true;
						} else {
                            throw new AssigningHandledException("Przypisany kierownik projektu zosta� usuni�ty z DS, kod u�ytkownika: "+userId+", dla pozycji l.p. "+bp.getINo());
						}
					} catch (UserNotFoundException e) {
                        throw new AssigningHandledException("Nieprawid�owy kod kierownika projektu: "+userId+", dla pozycji l.p. "+bp.getINo());
					}
				}
				
				//Z POLA OSOBA UPOWAZNIONA/KIEROWNIK
				Integer upowaznionyId = bp.getCUser();
				if (upowaznionyId != null) {
					String username = DataBaseEnumField.getEnumItemForTable(UPOWAZNIONY_TABLE_NAME, upowaznionyId).getCn();
					if (StringUtils.isNotEmpty(username)) {
						assignable.addCandidateUser(username);
						AssigneeHandler.addToHistory(openExecution, doc, username, null);
						return true;
					}
				}

				throw new AssigningHandledException("Nie znaleziono osoby do przypisania o id: " + bpId.toString() + ", przypisano do admin'a");
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				log.error("Nie znaleziono osoby do przypisania, przypisano do admin'a");
                if (e instanceof AssigningHandledException) {
                    addRemarkAndAssignToAdmin(doc, assignable, e.getMessage(), log);
                } else {
                    assignable.addCandidateUser("admin");
                }
				return true;
			}
		}
		
		return false;
	}
}
