package pl.compan.docusafe.parametrization.ul;


import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DuplicateNameException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.Profile;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ULImportOrganization extends ServiceDriver implements Service
{
	public static final Logger log = LoggerFactory.getLogger(ULImportOrganization.class);
	private Property[] properties;
	private Timer timer;
	private Integer period = 10;
	private String hostname;
	private String port;
	private String dbName;
	private String username;
	private String password;
	private static Profile profile; 

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, ULImportOrganization.this, "period", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (ULImportOrganization.this)
			{
				if (object != null)
					period = (Integer) object;
				else
					period = 10;
			}
		}
	}
	
	class PortProperty extends Property
	{
		public PortProperty()
		{
			super(SIMPLE, PERSISTENT, ULImportOrganization.this, "port", "port", String.class);
		}

		protected Object getValueSpi()
		{
			return port;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (ULImportOrganization.this)
			{
				if (object != null)
					port = (String) object;
				else
					port = "";
			}
		}
	}
	
	class DbNameProperty extends Property
	{
		public DbNameProperty()
		{
			super(SIMPLE, PERSISTENT, ULImportOrganization.this, "dbName", "Baza", String.class);
		}

		protected Object getValueSpi()
		{
			return dbName;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (ULImportOrganization.this)
			{
				if (object != null)
					dbName = (String) object;
				else
					dbName = "";
			}
		}
	}
	
	class UsernameProperty extends Property
	{
		public UsernameProperty()
		{
			super(SIMPLE, PERSISTENT, ULImportOrganization.this, "username", "Uzytkownik", String.class);
		}

		protected Object getValueSpi()
		{
			return username;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (ULImportOrganization.this)
			{
				if (object != null)
					username = (String) object;
				else
					username = "eod";
			}
		}
	}
	
	class HostnameProperty extends Property
	{
		public HostnameProperty()
		{
			super(SIMPLE, PERSISTENT, ULImportOrganization.this, "hostname", "Adres serwera", String.class);
		}

		protected Object getValueSpi()
		{
			return hostname;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (ULImportOrganization.this)
			{
				if (object != null)
					hostname = (String) object;
				else
					hostname = "";
			}
		}
	}
	
	class PasswordProperty extends Property
	{
		public PasswordProperty()
		{
			super(SIMPLE, PERSISTENT, ULImportOrganization.this, "password", "Has�o", String.class);
		}

		protected Object getValueSpi()
		{
			return password;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (ULImportOrganization.this)
			{
				if (object != null)
					password = (String) object;
				else
					password = "";
			}
		}
	}



	public ULImportOrganization()
	{
		properties = new Property[] { new ImportTimesProperty(),new HostnameProperty(), new PortProperty(),new DbNameProperty(),new UsernameProperty(),new PasswordProperty()};
	}

	@Override
	protected void start() throws ServiceException
	{
		log.info("Start uslugi ULImportOrganization");
		timer = new Timer(true);
		timer.schedule(new Import(), 0, period * DateUtils.HOUR);
		console(Console.INFO, "System ULImportOrganization wystartowal");
	}

	@Override
	protected void stop() throws ServiceException
	{
		log.info("Stop uslugi ULImportOrganization");
		console(Console.INFO, "System ULImportOrganization zako�czy� dzia�anie");
		try
		{
			DSApi.close();
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	@Override
	protected boolean canStop()
	{
		return true;
	}
	
	private static Connection  connect(String hostname, String port, String dbName, String username, String password) throws Exception
	{
		Driver d = new org.postgresql.Driver();
		System.out.println(d);
		Class.forName("org.postgresql.Driver");
		Connection connection = null;
		connection = DriverManager.getConnection(
		   "jdbc:postgresql://"+hostname+":"+port+"/"+dbName,username,password);
		return connection;
	}

	class Import extends TimerTask
	{
		@Override
		public void run()
		{
			Connection conn = null;
			PreparedStatement ps = null;
			ResultSet rs= null;
			try
			{
				conn = connect(hostname, port, dbName, username, password);
				DSApi.openAdmin();

				ps = conn.prepareStatement("SELECT id, kod, nazwa, id_nadrzedna, aktywne, stan_pobrania, wersja FROM jednostki_eod order by id");
				rs = ps.executeQuery();
				String externalId =null;
				String nazwaKomorki = null;
				String idDzialuNadrzednego = null;
				String code = null;
                boolean active;
                while (rs.next())
				{
					DSDivision division = null;
					externalId = rs.getString("ID");
					nazwaKomorki = StringUtils.left(rs.getString("NAZWA"),240);
					idDzialuNadrzednego = rs.getString("id_nadrzedna");
					code = rs.getString("kod");
                    active = !"N".equalsIgnoreCase(rs.getString("aktywne"));
//					nazwaKomorki = code+" "+nazwaKomorki;
                    if (active) {
                        console(Console.INFO, "Importuje dzia� "+nazwaKomorki);
                        try
                        {

                            DSDivision div = DSDivision.findByExternalId(externalId);
                            DSApi.context().begin();
                            div.setHidden(false);
                            div.setName(nazwaKomorki);
                            if (!StringUtils.isEmpty(idDzialuNadrzednego))
                            {
                                division = DSDivision.findByExternalId(idDzialuNadrzednego);
                                div.setParent(division);
                            }
//						else
//						{
//							division = DSDivision.find(DSDivision.ROOT_GUID);
//						}

                            DSApi.context().commit();
                        }
                        catch (DivisionNotFoundException de)
                        {

                            try
                            {
                                DSApi.context().begin();
                                // pobieram bie��cy dzia� (o ile istnieje)
                                if (!StringUtils.isEmpty(idDzialuNadrzednego))
                                {
                                    division = DSDivision.findByExternalId(idDzialuNadrzednego);
                                }
                                else
                                {
                                    division = DSDivision.find(DSDivision.ROOT_GUID);
                                }

                                DSDivision newDivision = division.createDivisionWithExternal(nazwaKomorki,code , externalId);
                                newDivision.setDescription("PIG IMPORTED");
                                newDivision.update();
                                DSApi.context().commit();

                                console(Console.INFO, "System UsersIntegrationService zaimportowa� dzia� " + nazwaKomorki);
                                log.error("System UsersIntegrationService zmodyfikowa� dzia� " + nazwaKomorki);
                            }
                            catch (DuplicateNameException dne)
                            {
                                DSApi.context().rollback();
                                DSApi.context().begin();
                                try
                                {
                                    DSDivision parentdivision = null;
                                    if (!StringUtils.isEmpty(idDzialuNadrzednego))
                                    {
                                        parentdivision = DSDivision.findByExternalId(idDzialuNadrzednego);
                                    }
                                    else
                                    {
                                        parentdivision = DSDivision.find(DSDivision.ROOT_GUID);
                                    }
                                    division = DSDivision.findByName(nazwaKomorki);
                                    division.setCode(code);
                                    division.setExternalId(externalId);
                                    division.setParent(parentdivision);
                                    division.setHidden(false);
                                    division.update();
                                    DSApi.context().commit();
                                    console(Console.INFO, "System UsersIntegrationService zmodyfikowa� dzia� " + nazwaKomorki);
                                    log.error("System UsersIntegrationService zmodyfikowa� dzia� " + nazwaKomorki);
                                }
                                catch (Exception exc)
                                {
                                    log.error("B��d dodania externalId {},code {},nazwaKomorki {},idDzialuNadrzednego {}",externalId,code,nazwaKomorki,idDzialuNadrzednego);
                                    log.error(exc.getMessage(), exc);
                                    DSApi.context().rollback();
                                }

                            }
                            catch (Exception exc)
                            {
                                log.error("B��d dodania externalId {},code {},nazwaKomorki {},idDzialuNadrzednego {}",externalId,code,nazwaKomorki,idDzialuNadrzednego);
                                log.error(exc.getMessage(), exc);
                                DSApi.context().rollback();
                            }
                        }
                    } else {
                        console(Console.INFO, "Pomijam nieaktywny dzia� "+nazwaKomorki);
                    }
                }

				DSDivision[]  divisions = DSDivision.getOnlyDivisions(false);
				console(Console.INFO, "Usuwam dzia�y");
				for (DSDivision dsDivision : divisions) 
				{
					if(DSDivision.TYPE_DIVISION.equals(dsDivision.getDivisionType()))
					{
						try
						{
							
							DSApi.context().begin();
							if(StringUtils.isEmpty(dsDivision.getExternalId()) && !DSDivision.ROOT_GUID.equals(dsDivision.getGuid()))
							{
								log.debug(" NIE USUWAM (dzia� dodany r�cznie) DZIA� dsdivision {}", dsDivision.getName()+" "+dsDivision.getGuid()+" "+dsDivision.getExternalId());
//								UserFactory.getInstance().deleteDivision(dsDivision.getGuid(), DSDivision.ROOT_GUID);
							}
							else if(StringUtils.isNotEmpty(dsDivision.getExternalId()) && !DSDivision.ROOT_GUID.equals(dsDivision.getGuid()))
							{
								ps = conn.prepareStatement("SELECT id, kod, nazwa, id_nadrzedna, aktywne, stan_pobrania, wersja FROM jednostki_eod where id = ? and aktywne<>'N' order by id");
								ps.setInt(1, Integer.parseInt(dsDivision.getExternalId()));
								rs = ps.executeQuery();
								if(!rs.next())
								{
                                    String substGuid = findSubstitutionGuid(dsDivision);
                                    console(Console.INFO, "USUWAM DZIA� "+ dsDivision.getName() + " " + dsDivision.getGuid() + " " + dsDivision.getExternalId()+" Zast�pstwo guid: "+substGuid);
                                    UserFactory.getInstance().deleteDivision(dsDivision.getGuid(), substGuid);

									log.debug("USUWAM DZIA� dsdivision {}", dsDivision.getName()+" "+dsDivision.getGuid()+" "+dsDivision.getExternalId());
								}
								else
								{
									log.debug("NIE usuwa dzia�u " + dsDivision.getName() + " " + dsDivision.getGuid() + " " + dsDivision.getExternalId());
								}
								
								ps.close();
							}
							else
							{
								log.error("DZIA� G��WNY NIE USUWAM");
							}
							DSApi.context().commit();
						}
						catch (Exception se) {
                            console(Console.ERROR, se.getMessage());
                            log.error(se.getMessage(), se);
							DbUtils.closeQuietly(ps);
							DSApi.context().rollback();
						}
					}
				}
                console(Console.INFO, "Zako�czono synchronizacj�");
            }
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
				console(Console.INFO, e.toString());
				try
				{
					DSApi.context().rollback();
				}
				catch (EdmException e1)
				{
					log.error(e.getMessage(), e1);
				}
			}
			finally
			{
				try
				{
					DbUtils.closeQuietly(ps);
					DbUtils.closeQuietly(rs);
					DbUtils.closeQuietly(conn);
                    DataBaseEnumField.reloadForTable(CostInvoiceLogic.JEDNOSTKI_VIEW_NAME);
                    DSApi.close();
                }
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
				}
			}

		}

        private String findSubstitutionGuid(DSDivision dsDivision) {
            try {
                DSDivision substDiv;
                while ((substDiv = dsDivision.getParent()) != null && substDiv.isHidden());
                if (substDiv != null) {
                    return substDiv.getGuid();
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
            return DSDivision.ROOT_GUID;
        }
    }

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}
}

