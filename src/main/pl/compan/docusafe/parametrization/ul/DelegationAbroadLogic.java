package pl.compan.docusafe.parametrization.ul;

import com.google.common.base.*;
import com.google.common.collect.*;
import com.google.common.primitives.Ints;
import org.apache.commons.lang.ObjectUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.parametrization.ul.DocumentCacheHandler.CountryDelegaitonRates;
import pl.compan.docusafe.parametrization.ul.hib.UlBudgetPosition;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

public class DelegationAbroadLogic extends AbstractDelegationLogic {
    protected static final Logger log = LoggerFactory.getLogger(DelegationAbroadLogic.class);

    public static final String DWR_WNIOSEK_WYJAZDOWY = "DWR_WNIOSEK_WYJAZDOWY";
    public static final String DWR_TERMIN_WYJAZDU = "DWR_TERMIN_WYJAZDU";
    public static final String DWR_TERMIN_POWROTU = "DWR_TERMIN_POWROTU";
    private static final String OD_DO_ERROR_MSG = "Termin wyjazdu nie mo�e by� p�niej ni� termin powrotu";
    private final static String RYCZALTY_DOJAZDY_KOM_DICT = "RYCZALTY_DOJAZDY_KOM";
    private final static String KOSZTY_NOCLEGU_RYCZALTY_DICT = "RYCZALTY_NOCLEG";
    private final static String KOSZTY_PRZEJAZDU_DICT = "KOSZTY_PRZEJAZDU_SAM";
    private final static String KOSZTY_OGOLEM_DICT = "OGOLEM_KOSZTY";
    public static final BigDecimal PROCENT_DIETY_ZA_PRZEJAZDY = new BigDecimal(0.1);
    public static final BigDecimal PROCENT_RYCZALTU_LIMITU_HOTELOWEGO = new BigDecimal(0.25);
    public static final String KOSZTY_DOJAZDY = "KOSZTY_DOJAZDY";
    public static final String KOSZTY_DOJAZDY_S = KOSZTY_DOJAZDY + "_";
    public static final String DWR_PREFIX = "DWR_";
    public static final String DWR_KOSZTY_DOJAZDY = DWR_PREFIX + KOSZTY_DOJAZDY + "_";
    public static final String KOSZTY_INNE = "KOSZTY_INNE";
    public static final String KOSZTY_INNE_S = KOSZTY_INNE + "_";
    public static final String KOSZTY_NOCLEGU_PRE = "KOSZTY_NOCLEGU_PRE";
    public static final String KOSZTY_NOCLEGU = "KOSZTY_NOCLEGU";
    public static final String KOSZTY_NOCLEGU_S = KOSZTY_NOCLEGU + "_";
    public static final String DIETA_ZAGRANICZNA = "DIETA_ZAGRANICZNA";
    public static final String DIETA_ZAGRANICZNA_PRE = "DIETA_ZAGRANICZNA_PRE";
    public static final String PRE_COST_DICTIONARY_SQL_CODE_IN_PART = Joiner.on(",").join(Iterables.transform(Lists.newArrayList(KALKULACJA_KOSZTOW, DIETA_ZAGRANICZNA_PRE, KOSZTY_NOCLEGU_PRE), new Function<String, Object>() {
        @Override
        public String apply(String s) {
            return "'" + s + "'";
        }
    }));
    public static final String DIETA_ZAGRANICZNA_PRE_S = DIETA_ZAGRANICZNA_PRE + "_";
    public static final String DIETA_ZAGRANICZNA_S = DIETA_ZAGRANICZNA + "_";
    public static final String DIETA_KRAJOWA = "DIETA_KRAJOWA";
    public static final String DIETA_KRAJOWA_S = DIETA_KRAJOWA + "_";
    public static final String KALKULACJA_KOSZTOW_S = KALKULACJA_KOSZTOW + "_";
    public static final String CURRENCY_RATE = "CURRENCY_RATE";
    public static final String CURRENCY_RATE_DATE = "CURRENCY_RATE_DATE";
    public static final String CURRENCY = "CURRENCY";
    public static final String MONEY_VALUE = "MONEY_VALUE";
    public static final String MONEY_VALUE_PLN = "MONEY_VALUE_PLN";
    public static final String ENUM = "ENUM";
    public static final String VALUE = "VALUE";
    public static final String RATE = "RATE";
    public static final String BOOL4 = "BOOL4";
    public static final String ADD_MONEY_VALUE = "ADD_MONEY_VALUE";
    public static final String ADD_RATE = "ADD_RATE";
    public static final String COUNTRY = "COUNTRY";
    public static final String ADD_MONEY_VALUE_PLN = "ADD_MONEY_VALUE_PLN";
    public static final String BOOL1 = "BOOL1";
    public static final String BOOL2 = "BOOL2";
    public static final String BOOL3 = "BOOL3";
    public static final String NADPLATA = "NADPLATA";
    public static final String NADPLATA_S = NADPLATA + "_";
    public static final String ZAPLATA = "ZAPLATA";
    public static final String ZAPLATA_S = ZAPLATA + "_";
    public static final String COUNTRY_TABLE_NAME = "dsr_country";
    public static final String DELEGATION_EXCHANGE_RATE_TABLE_NAME = "dsg_ul_delegation_exchange_rate";
    public static final String DOC_KIND_CN = "ul_delegation_abroad";
    public static final String INT1 = "INT1";
    public static final String INT2 = "INT2";
    public static final String INT3 = "INT3";
    public static final String INT4 = "INT4";
    private static final String INT5 = "INT5";
    private static final String DWR_KALKULACJA_KOSZTOW = DWR_PREFIX + KALKULACJA_KOSZTOW;
    public static final String FLIGHT_TRIP_COST_ID = "1";
    public static final String DWR_ZAKUP_BILETU_CZY = "DWR_ZAKUP_BILETU_CZY";
    private static final String DWR_KOSZTY_NOCLEGU = DWR_PREFIX + KOSZTY_NOCLEGU;
    private static final String DWR_RACHUNKI_ZA_NOCLEG_CZY = DWR_PREFIX + "RACHUNKI_ZA_NOCLEG_CZY";
    public static final String BILL_CONFIRMED_ACCOMODATION_ID = "1";
    public static final String BILL_CONFIRMED_ACCOMODATION_BELLOW_LIMIT_ID = "2";
    private static final String KOSZTY = "KOSZTY";
    public static final String KRAJ_WYJAZDU = "KRAJ_WYJAZDU";
    public static final String GROUP_COST_DICTIONARIES_SQL = "DECLARE @inserted_ids TABLE ([id] INT);\n" +
            "DECLARE @doc_id bigint = ?;\n" +
            "delete from dsg_ul_delegation_abroad_multiple_value where document_id=@doc_id and field_cn='"+ KOSZTY + "';\n" +
            "insert into dsg_ul_delegation_abroad_dic (currency,money_value)\n" +
            "OUTPUT INSERTED.[id] INTO @inserted_ids \n" +
            "select d.currency, sum(money_value) \n" +
            "from dsg_ul_delegation_abroad_multiple_value mu\n" +
            "left join dsg_ul_delegation_abroad_dic d on mu.field_val=d.id\n" +
            "where mu.field_cn in (" + PRE_COST_DICTIONARY_SQL_CODE_IN_PART + ")\n" +
            "and mu.document_id=@doc_id\n" +
            "group by d.currency;\n" +
            "insert into dsg_ul_delegation_abroad_multiple_value (DOCUMENT_ID,FIELD_CN,FIELD_VAL) select @doc_id as DOCUMENT_ID, '" + KOSZTY + "' as FIELD_CN, id as FIELD_VAL \n" +
            "from @inserted_ids;";
/*    public static final String CALCULATE_DELEGATION_COST_SQL = "DECLARE @doc_id bigint = ?;\n" +
            "DECLARE @inserted_ids TABLE ([id] INT);\n" +
            "begin transaction; \n" +
            "begin try \n" +
            "delete from dsg_ul_delegation_abroad_multiple_value\n" +
            "where DOCUMENT_ID = @doc_id and FIELD_CN = 'ROZLICZENIE_ZALICZKI';\n" +
            "insert into dsg_ul_delegation_abroad_dic (currency,enum,money_value)\n" +
            "OUTPUT INSERTED.[id] INTO @inserted_ids \n" +
            "select ISNULL(zaliczka_walutowa_waluta,cost.currency) as currency,\n" +
            "CASE \n" +
            "\tWHEN ISNULL(zaliczka_walutowa_kwota,0)-ISNULL(cost.money_sum,0)>0 THEN 1\n" +
            "\tWHEN ISNULL(zaliczka_walutowa_kwota,0)-ISNULL(cost.money_sum,0)<0 THEN 2\n" +
            "\tWHEN ISNULL(zaliczka_walutowa_kwota,0)-ISNULL(cost.money_sum,0)=0 THEN 3\n" +
            "END as enum,\n" +
            "ABS(ISNULL(zaliczka_walutowa_kwota,0)-ISNULL(cost.money_sum,0)) as money_value\n" +
            "from \n" +
            "\t(select zaliczka_walutowa_waluta,zaliczka_walutowa_kwota from dsg_ul_delegation_abroad where document_id=@doc_id)\n" +
            "as a\n" +
            "full outer join \n" +
            "\t(select currency,sum(ISNULL(money_value,0)+ISNULL(add_money_value,0)) as money_sum\n" +
            "\tfrom dsg_ul_delegation_abroad_dic dic \n" +
            "\tjoin dsg_ul_delegation_abroad_multiple_value mu ON dic.ID=mu.FIELD_VAL \n" +
            "\twhere mu.FIELD_CN IN ('DIETA_ZAGRANICZNA','KOSZTY_NOCLEGU','KOSZTY_INNE') and mu.DOCUMENT_ID=@doc_id \n" +
            "\tgroup by currency) \n" +
            "as cost on cost.currency=a.zaliczka_walutowa_waluta;\n" +
            "insert into dsg_ul_delegation_abroad_multiple_value (DOCUMENT_ID,FIELD_CN,FIELD_VAL) select @doc_id as DOCUMENT_ID, 'ROZLICZENIE_ZALICZKI' as FIELD_CN, id as FIELD_VAL \n" +
            "from @inserted_ids; \n" +
            "commit; \n" +
            "end try\n" +
            "begin catch \n" +
            "rollback; \n" +
            "end catch;";*/
    public static final String CALCULATE_DELEGATION_COST_SQL = "DECLARE @doc_id bigint = ?;\n" +
            "DECLARE @inserted_ids TABLE ([id] INT);\n" +
            "begin transaction; \n" +
            "begin try \n" +
            "delete from dsg_ul_delegation_abroad_multiple_value\n" +
            "where DOCUMENT_ID = @doc_id and FIELD_CN = 'ROZLICZENIE_ZALICZKI';\n" +
            "insert into dsg_ul_delegation_abroad_dic (currency,enum,money_value)\n" +
            "OUTPUT INSERTED.[id] INTO @inserted_ids \n" +
            "select ISNULL(zaliczka_walutowa_waluta,cost.currency) as currency,\n" +
            "CASE \n" +
            "\tWHEN ISNULL(zaliczka_walutowa_kwota,0)-ISNULL(cost.money_sum,0)>0 THEN 1\n" +
            "\tWHEN ISNULL(zaliczka_walutowa_kwota,0)-ISNULL(cost.money_sum,0)<0 THEN 2\n" +
            "\tWHEN ISNULL(zaliczka_walutowa_kwota,0)-ISNULL(cost.money_sum,0)=0 THEN 3\n" +
            "END as enum,\n" +
            "ABS(ISNULL(zaliczka_walutowa_kwota,0)-ISNULL(cost.money_sum,0)) as money_value\n" +
            "from \n" +
            "\t--(select zaliczka_walutowa_waluta,zaliczka_walutowa_kwota from dsg_ul_delegation_abroad where document_id=@doc_id)\n" +
            "\t(select currency as zaliczka_walutowa_waluta,sum(ISNULL(money_value,0)) as zaliczka_walutowa_kwota\n" +
            "\tfrom dsg_ul_delegation_abroad_dic dic \n" +
            "\tjoin dsg_ul_delegation_abroad_multiple_value mu ON dic.ID=mu.FIELD_VAL \n" +
            "\twhere mu.FIELD_CN IN ('ZALICZKA_WALUTOWA_DIC') and mu.DOCUMENT_ID=@doc_id \n" +
            "\tgroup by currency) \n" +
            "as a\n" +
            "full outer join \n" +
            "\t(select currency,sum(ISNULL(money_value,0)+ISNULL(add_money_value,0)) as money_sum\n" +
            "\tfrom dsg_ul_delegation_abroad_dic dic \n" +
            "\tjoin dsg_ul_delegation_abroad_multiple_value mu ON dic.ID=mu.FIELD_VAL \n" +
            "\twhere mu.FIELD_CN IN ('DIETA_ZAGRANICZNA','KOSZTY_NOCLEGU','KOSZTY_INNE') and mu.DOCUMENT_ID=@doc_id \n" +
            "\tgroup by currency) \n" +
            "as cost on cost.currency=a.zaliczka_walutowa_waluta;\n" +
            "insert into dsg_ul_delegation_abroad_multiple_value (DOCUMENT_ID,FIELD_CN,FIELD_VAL) select @doc_id as DOCUMENT_ID, 'ROZLICZENIE_ZALICZKI' as FIELD_CN, id as FIELD_VAL \n" +
            "from @inserted_ids; \n" +
            "commit; \n" +
            "end try\n" +
            "begin catch \n" +
            "rollback; \n" +
            "end catch;";

    public static final String SUM_PRE_COST_SQL = "select sum(ISNULL(d.money_value_pln,ISNULL(d.money_value,0))) from dsg_ul_delegation_abroad_dic d\n" +
            "join dsg_ul_delegation_abroad_multiple_value mu on d.id=mu.FIELD_VAL\n" +
            "where mu.DOCUMENT_ID=? and mu.FIELD_CN in (" + PRE_COST_DICTIONARY_SQL_CODE_IN_PART + ");";

    private static final String SUM_PRE_COST_TO_LOAN_SQL = "DECLARE @inserted_ids TABLE ([id] INT);\n" +
            "DECLARE @doc_id bigint = ?;\n" +
            "delete from dsg_ul_delegation_abroad_multiple_value where document_id=@doc_id and field_cn='ZALICZKA_WALUTOWA_DIC';\n" +
            "insert into dsg_ul_delegation_abroad_dic (currency,money_value)\n" +
            "OUTPUT INSERTED.[id] INTO @inserted_ids \n" +
            "select d.currency, sum(money_value) \n" +
            "from dsg_ul_delegation_abroad_multiple_value mu\n" +
            "left join dsg_ul_delegation_abroad_dic d on mu.field_val=d.id\n" +
            "where (\n" +
            "mu.field_cn in ('DIETA_ZAGRANICZNA_PRE','KOSZTY_NOCLEGU_PRE')\n" +
            "or (mu.FIELD_CN = 'KALKULACJA_KOSZTOW' and d.enum in (102,103,112,110,111))\n" +
            ") \n" +
            "and mu.document_id=@doc_id\n" +
            "group by d.currency;\n" +
            "insert into dsg_ul_delegation_abroad_multiple_value (DOCUMENT_ID,FIELD_CN,FIELD_VAL) select @doc_id as DOCUMENT_ID, 'ZALICZKA_WALUTOWA_DIC' as FIELD_CN, id as FIELD_VAL \n" +
            "from @inserted_ids;";

    public static final String TRANSPORT = "TRANSPORT";
    public static final String CAR_TRIP_COST_ID = "3";
    public static final String CAR_PRIVATE_TRIP_COST_ID = "6";
    public static final String OTHER_TRIP_COST_ID = "11";
    public static final BigDecimal BIG_DECIMAL_100 = BigDecimal.ONE.movePointRight(2);
    public static final String ROZLICZENIE_ZALICZKI = "ROZLICZENIE_ZALICZKI";
    private static final String DOKUMENTY_FIELD = "DOKUMENTY";
    public static final String ZALICZKA_WALUTOWA_DIC = "ZALICZKA_WALUTOWA_DIC";
    public static final String ZALICZKA_WALUTOWA = "ZALICZKA_WALUTOWA";

    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
        super.onStartProcess(document, event);

        generateNewBudgetPositionFromPreCosts(document);

        generateLoanDictionaryFromPreCosts(document);
    }

    private void generateLoanDictionaryFromPreCosts(OfficeDocument document) throws EdmException {
        PreparedStatement ps = null;
        try {
            ps = DSApi.context().prepareStatement(Docusafe.getAdditionPropertyOrDefault("delegation_abroad.sql.sum_pre_cost_to_loan", SUM_PRE_COST_TO_LOAN_SQL));
            ps.setLong(1, document.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            if (ps != null) {
                DSApi.context().closeStatement(ps);
            }
        }
    }

    private void generateNewBudgetPositionFromPreCosts(OfficeDocument document) throws EdmException {
        PreparedStatement ps = null;
        try {
            ps = DSApi.context().prepareStatement(Docusafe.getAdditionPropertyOrDefault("delegation_abroad.sql.sum_pre_cost", SUM_PRE_COST_SQL));
            ps.setLong(1, document.getId());

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                BigDecimal sum = rs.getBigDecimal(1);

                UlBudgetPosition bp = new UlBudgetPosition();
                bp.setIAmount(sum);
                bp.save();

                document.getDocumentKind().setOnly(document.getId(), ImmutableMap.of(BP_FIELD, Lists.newArrayList(bp.getId())));
            }
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            if (ps != null) {
                DSApi.context().closeStatement(ps);
            }
        }
    }

    @Override
    public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException {
        super.onAcceptancesListener(doc, acceptationCn);

        if ("author_roz".equals(acceptationCn)) {
            calculateDelegationCost(doc.getId());
        }

    }

    @Override
    public String onAlternativeUpdate(Document document, Map<String, Object> values, String activity) throws EdmException {
        calculateDelegationCost(document.getId());
        setSummaryCostType(document, document.getFieldsManager());
        return "Przeliczono rozliczenie zaliczki";
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        FieldsManager fm = document.getFieldsManager();
        try {
            assignOrdinalNumber(fm, BP_ROZ_FIELD);
            reloadDBEnumFieldData("dsg_ul_delegation_abroad_budget_position_number");
            setSummaryCostType(document, fm);

            if (fm.getValue(DOKUMENTY_FIELD) != null) {
                linkDocuments(document, fm);
            }
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
    }

    private void linkDocuments(Document document, FieldsManager fm) {
        try {
            for (Long docId : (List<Long>) fm.getKey(DOKUMENTY_FIELD)) {
                DocumentKind otherDockind = Document.find(docId, false).getDocumentKind();
                FieldsManager otherFm = otherDockind.getFieldsManager(docId);
                List<Long> otherAssignedIds = Lists.newArrayList();

                if (otherFm.getKey(DOKUMENTY_FIELD) != null) {
                    otherAssignedIds.addAll((Collection<Long>) otherFm.getKey(DOKUMENTY_FIELD));
                }

                if (!otherAssignedIds.contains(document.getId())) {
                    otherAssignedIds.add(document.getId());
                    otherDockind.setOnly(docId, ImmutableMap.of(DOKUMENTY_FIELD, otherAssignedIds), false);
                }
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public static void calculateDelegationCost(Long docId) throws EdmException {
        PreparedStatement ps = null;
        try {
            ps = DSApi.context().prepareStatement(CALCULATE_DELEGATION_COST_SQL);
            ps.setLong(1, docId);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            if (ps != null) {
                DSApi.context().closeStatement(ps);
            }
        }
    }

    private void setSummaryCostType(Document document, FieldsManager fm) throws EdmException {
        List<Map<String, Object>> rozMap = (List<Map<String, Object>>) fm.getValue(ROZLICZENIE_ZALICZKI);
        Integer summaryCostType = null;
        if (rozMap != null) {
            Set<String> costTypes = Sets.newHashSet();
            for (Map<String, Object> item : rozMap) {
                EnumValues costType = (EnumValues) item.get("ROZLICZENIE_ZALICZKI_ENUM");

                if (costType != null) {
                    costTypes.add(costType.getSelectedId());
                }
            }

            summaryCostType = 4;
            if (costTypes.contains("1") && costTypes.contains("2")) {
                summaryCostType = 3;
            } else if (costTypes.contains("1")) {
                summaryCostType = 1;
            } else if (costTypes.contains("2")) {
                summaryCostType = 2;
            }
            document.getDocumentKind().setOnly(document.getId(), ImmutableMap.of("ROZLICZENIE_TYP", summaryCostType));
        }
    }

    @Override
    void getAddtitionFieldsDescription(FieldsManager fm, Map<String, String> additionFields) throws EdmException {
        additionFields.put("Data wype�nienia", fm.getStringValue("DATA_WYPELNIENIA"));
        additionFields.put("Wyjazd anulowany", fm.getBoolean("WYJAZD_ANULOWANY") ? "Tak" : null);
        additionFields.put("Kraje wyjazdu", fm.getStringValue(KRAJ_WYJAZDU));
        additionFields.put("Wyjazd ze skierowaniem - na podstawie osobnego rozporz�dzewnia MNiSW", fm.getStringValue("CZY_SKIEROWANIE"));
        additionFields.put("Cel wyjazdu", fm.getStringValue("CEL"));
        additionFields.put("Dodatkowe informacje", fm.getStringValue("ADD_INFO"));
        additionFields.put("�rodek lokomocji", fm.getStringValue("TRANSPORT"));
        additionFields.put("�rodek lokomocji, szczeg�y *", fm.getStringValue("TRANSPORT_DESC"));
        additionFields.put("Kraj/miasto/instytucja przyjmuj�ca", fm.getStringValue("WYJAZD_INFO"));
        additionFields.put("Wnioskuje o urlop", fm.getStringValue("CZY_URLOP"));
        additionFields.put("Urlop", fm.getStringValue("URLOP"));
        additionFields.put("Od", fm.getStringValue("URLOP_OD"));
        additionFields.put("Do", fm.getStringValue("URLOP_DO"));
        additionFields.put("Wyjazd", fm.getStringValue("WYJAZD_GRANICA"));
        additionFields.put("Data", fm.getStringValue("TERMIN_WYJAZDU"));
        additionFields.put("Przyjazd", fm.getStringValue("PRZYJAZD_GRANICA"));
        additionFields.put("Data", fm.getStringValue("TERMIN_POWROTU"));
        additionFields.put("Pro�ba o zaliczk�", fm.getStringValue("ZALICZKA_BOOL"));
        additionFields.put("Forma przekazania zaliczki", fm.getStringValue("FORMA_ZALICZKI"));
        additionFields.put("Przelew zaliczki na konto", fm.getStringValue("PRZELEW_NA"));
        additionFields.put("��czna kwota kalkulacji (PLN)", fm.getStringValue("PRZEWIDYWANY_KOSZT"));
        additionFields.put("Czas podr�y - wyjazd", fm.getStringValue("CZAS_PODR_WYJAZD"));
        additionFields.put("Miejscowo�c wyjazdu", fm.getStringValue("CZAS_PODR_WYJAZD_MIASTO"));
        additionFields.put("Data i godzina wyjazdu", fm.getStringValue("CZAS_PODR_WYJAZD_DATA"));
        additionFields.put("Data i godzina przekroczenia granicy", fm.getStringValue("CZAS_PODR_WYJAZD_DATA_GR"));
        additionFields.put("Czas podr�y - powr�t", fm.getStringValue("CZAS_PODR_POWROT"));
        additionFields.put("Miejscowo�c przyjazdu", fm.getStringValue("CZAS_PODR_POWROT_MIASTO"));
        additionFields.put("Data i godzina przyjazdu", fm.getStringValue("CZAS_PODR_POWROT_DATA"));
        additionFields.put("Data i godzina przekroczenia granicy", fm.getStringValue("CZAS_PODR_POWROT_DATA_GR"));
        additionFields.put("Czas podr�y krajowej", fm.getStringValue("CZAS_PODR_KRAJ"));
        additionFields.put("Czas podr�y zagranicznej", fm.getStringValue("CZAS_PODR_ZAGR"));
        additionFields.put("Suma koszt�w z rozliczenia w PLN", fm.getStringValue("ROZLICZENIE_SUMA_PLN"));
        additionFields.put("Spos�b rozliczenia", fm.getStringValue("ZAPLATA_CZY_NADPLATA"));
    }

    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) {
        Map<String, Object> results = new HashMap<String, Object>();

        try {
/*            if (dictionaryName.equals(BP_FIELD) || dictionaryName.equals(BP_ROZ_FIELD)) {
                setBudgetItemsRefValues(dictionaryName, values, results);
            }*/

            if (dictionaryName.equals(CostInvoiceLogic.STAWKI_VAT_FIELD_NAME)) {
                calcStawkiVat(CostInvoiceLogic.STAWKI_VAT_FIELD_NAME, values, results);
            }

            if (dictionaryName.equals(KALKULACJA_KOSZTOW)) {
                evaluatePreCostDictionary(values, results);
            }

/*            if (dictionaryName.equals("DIETA_KRAJOWA")) {
                evaluateInternalDietDictionary(values, results);
            }*/

            if (dictionaryName.equals(DIETA_ZAGRANICZNA) || dictionaryName.equals(DIETA_ZAGRANICZNA_PRE)) {
                evaluateAbroadDietDictionary(dictionaryName+"_", values, results);
            }

            if (dictionaryName.equals(KOSZTY_NOCLEGU) || dictionaryName.equals(KOSZTY_NOCLEGU_PRE)) {
                evaluateAccommodationDictionary(dictionaryName + "_", values, results);
            }

            if (dictionaryName.equals(KOSZTY_INNE) || dictionaryName.equals(ROZLICZENIE_ZALICZKI) || dictionaryName.equals(ZALICZKA_WALUTOWA_DIC)) {
                evaluateOtherCostDictionary(values, results, dictionaryName + "_");
            }


        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }


        try {
            if (dictionaryName.equals(BP_FIELD) || dictionaryName.equals(BP_ROZ_FIELD)) {
                setDictionaries(dictionaryName, values, results);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return results;
    }

    private void evaluateOtherCostDictionary(Map<String, FieldData> values, Map<String, Object> results, String dictionaryCn) throws ExecutionException {
        if (DwrUtils.isNotNull(values, dictionaryCn + CURRENCY_RATE_DATE, dictionaryCn + CURRENCY) && DwrUtils.isSenderField(values, dictionaryCn + CURRENCY_RATE_DATE)) {
            Integer currencyId = null;
            try {
                currencyId = Integer.valueOf(values.get(dictionaryCn + CURRENCY).getData().toString());
            } catch (NumberFormatException e) {
            }
            if (currencyId != null) {
                BigDecimal rate = DocumentCacheHandler.DATED_CURRENCY_RATES_CACHED.get(new DocumentCacheHandler.CurrencyParam(currencyId, values.get(dictionaryCn + CURRENCY_RATE_DATE).getDateData()));
                if (rate != BigDecimal.ONE) {
                    values.get(dictionaryCn + CURRENCY_RATE).setMoneyData(rate);
                    results.put(dictionaryCn + CURRENCY_RATE, rate);
                }
            }
        }

        if (DwrUtils.isNotNull(values, dictionaryCn + MONEY_VALUE, dictionaryCn + CURRENCY_RATE) && !DwrUtils.isSenderField(values, dictionaryCn + MONEY_VALUE_PLN)) {
            BigDecimal plnValue = DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.MULTIPLY, 2, false, values.get(dictionaryCn + MONEY_VALUE), values.get(dictionaryCn + CURRENCY_RATE)).setScale(2, RoundingMode.HALF_UP);
            results.put(dictionaryCn + MONEY_VALUE_PLN, plnValue);
        }
    }

    private void evaluateAccommodationDictionary(String dictionaryNameWithSuffix, Map<String, FieldData> values, Map<String, Object> results) throws ExecutionException {
        Integer currencyId = null;
        try {
            if (DwrUtils.isSenderField(values, dictionaryNameWithSuffix + COUNTRY)) {
                String countryCn = DwrUtils.getSelectedEnumCn(values, dictionaryNameWithSuffix + COUNTRY, COUNTRY_TABLE_NAME);

                CountryDelegaitonRates delegationRates = DocumentCacheHandler.getCountryDelegaitonRates(countryCn);
                results.put(dictionaryNameWithSuffix + RATE, delegationRates.getAccLimit());
                values.get(dictionaryNameWithSuffix + RATE).setMoneyData(delegationRates.getAccLimit());
                //results.put(dictionaryNameWithSuffix + ADD_RATE, delegationRates.getDietRate());
                //values.get(dictionaryNameWithSuffix + ADD_RATE).setMoneyData(delegationRates.getDietRate());

                DataBaseEnumField currencyField = DataBaseEnumField.getEnumFiledForTable(DELEGATION_EXCHANGE_RATE_TABLE_NAME);
                currencyId = searchEnumIdByCn(delegationRates.getCurrencyCn(), currencyField);
                results.put(dictionaryNameWithSuffix + CURRENCY, currencyField.getEnumValuesForForcedPush(currencyId != null ? currencyId.toString() : ""));
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }

        if (DwrUtils.isNotNull(values, dictionaryNameWithSuffix + ENUM, dictionaryNameWithSuffix + VALUE)) {
            String selectedKind = values.get(dictionaryNameWithSuffix + ENUM).getData().toString();
            if (("1".equals(selectedKind) || "3".equals(selectedKind)) && DwrUtils.isNotNull(values, dictionaryNameWithSuffix + RATE)) {
                BigDecimal accommodationLimit = BigDecimal.ONE;
                try {
                    accommodationLimit = values.get(dictionaryNameWithSuffix + RATE).getMoneyData();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
                BigDecimal rate = "3".equals(selectedKind) ? PROCENT_RYCZALTU_LIMITU_HOTELOWEGO : BigDecimal.ONE;
                if (("1".equals(selectedKind) && !DwrUtils.isNotNull(values, dictionaryNameWithSuffix + MONEY_VALUE))
                        || "3".equals(selectedKind)) {
                    BigDecimal moneyValue = new BigDecimal(values.get(dictionaryNameWithSuffix + VALUE).getIntegerData()).multiply(accommodationLimit).multiply(rate).setScale(2, RoundingMode.HALF_UP);
                    values.get(dictionaryNameWithSuffix + MONEY_VALUE).setMoneyData(moneyValue);
                    results.put(dictionaryNameWithSuffix + MONEY_VALUE, moneyValue);
                }
            }
        }


//      ryczalt kosztow dojazdow z/do dworca w wysokosci jednej diety, przyapdajacy na ka�de miejsce noclegu
/*        if (values.get(dictionaryNameWithSuffix + BOOL4) != null && values.get(dictionaryNameWithSuffix + BOOL4).getBooleanData()) {
            values.get(dictionaryNameWithSuffix + ADD_MONEY_VALUE).setMoneyData(BigDecimal.ZERO);
            results.put(dictionaryNameWithSuffix + ADD_MONEY_VALUE, BigDecimal.ZERO);
        } else {
            if (DwrUtils.isNotNull(values, dictionaryNameWithSuffix + ADD_RATE)) {
                BigDecimal moneyValue = values.get(dictionaryNameWithSuffix + ADD_RATE).getMoneyData();
                values.get(dictionaryNameWithSuffix + ADD_MONEY_VALUE).setMoneyData(moneyValue);
                results.put(dictionaryNameWithSuffix + ADD_MONEY_VALUE, moneyValue);
            }
        }*/

        if (DwrUtils.isNotNull(values, dictionaryNameWithSuffix + CURRENCY)) {

            if (values.containsKey(dictionaryNameWithSuffix + CURRENCY_RATE_DATE)) {

                if (DwrUtils.isNotNull(values, dictionaryNameWithSuffix + CURRENCY_RATE_DATE)
                        && DwrUtils.isSenderField(values, dictionaryNameWithSuffix + CURRENCY_RATE,
                        dictionaryNameWithSuffix + CURRENCY_RATE_DATE,
                        dictionaryNameWithSuffix + COUNTRY)) {
                    if (currencyId == null) {
                        currencyId = Ints.tryParse(values.get(dictionaryNameWithSuffix + CURRENCY).getData().toString());
                    }
                }
                if (currencyId != null) {
                    BigDecimal rate = currencyId > 0
                            ? DocumentCacheHandler.DATED_CURRENCY_RATES_CACHED.get(new DocumentCacheHandler.CurrencyParam(currencyId, values.get(dictionaryNameWithSuffix + CURRENCY_RATE_DATE).getDateData()))
                            : BigDecimal.ONE;

                    if (rate != BigDecimal.ONE) {
                        values.get(dictionaryNameWithSuffix + CURRENCY_RATE).setMoneyData(rate);
                        results.put(dictionaryNameWithSuffix + CURRENCY_RATE, rate);
                    }
                }
            }

            if (DwrUtils.isNotNull(values, dictionaryNameWithSuffix + MONEY_VALUE, dictionaryNameWithSuffix + CURRENCY_RATE) && !DwrUtils.isSenderField(values, dictionaryNameWithSuffix + MONEY_VALUE_PLN)) {
                BigDecimal plnValue = DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.MULTIPLY, 2, false, values.get(dictionaryNameWithSuffix + MONEY_VALUE), values.get(dictionaryNameWithSuffix + CURRENCY_RATE)).setScale(2, RoundingMode.HALF_UP);
                results.put(dictionaryNameWithSuffix + MONEY_VALUE_PLN, plnValue);
            }
/*            if (DwrUtils.isNotNull(values, dictionaryNameWithSuffix + ADD_MONEY_VALUE, dictionaryNameWithSuffix + CURRENCY_RATE)) {
                BigDecimal plnValue = DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.MULTIPLY, 2, false, values.get(dictionaryNameWithSuffix + ADD_MONEY_VALUE), values.get(dictionaryNameWithSuffix + CURRENCY_RATE)).setScale(2, RoundingMode.HALF_UP);
                results.put(dictionaryNameWithSuffix + ADD_MONEY_VALUE_PLN, plnValue);
            } else {
                results.put(dictionaryNameWithSuffix + ADD_MONEY_VALUE_PLN, BigDecimal.ZERO);
            }*/
        } else {
            results.put(dictionaryNameWithSuffix + CURRENCY_RATE, BigDecimal.ONE);
            results.put(dictionaryNameWithSuffix + MONEY_VALUE_PLN, BigDecimal.ZERO);
            //results.put(dictionaryNameWithSuffix + ADD_MONEY_VALUE_PLN, BigDecimal.ZERO);

        }


    }

    private void evaluateAbroadDietDictionary(String dictionaryNameWithSuffix, Map<String, FieldData> values, Map<String, Object> results) throws ExecutionException {
        if (DwrUtils.isNotNull(values, dictionaryNameWithSuffix + COUNTRY)) {
            try {
                Integer currencyId = null;
                BigDecimal abroadDiet = BigDecimal.ZERO;

                if (DwrUtils.isSenderField(values, dictionaryNameWithSuffix + COUNTRY)) {
                    String countryCn = DwrUtils.getSelectedEnumCn(values, dictionaryNameWithSuffix + COUNTRY, COUNTRY_TABLE_NAME);

                    CountryDelegaitonRates delegationRates = DocumentCacheHandler.getCountryDelegaitonRates(countryCn);
                    abroadDiet = delegationRates.getDietRate();
                    results.put(dictionaryNameWithSuffix + RATE, abroadDiet);

                    DataBaseEnumField currencyField = DataBaseEnumField.getEnumFiledForTable(DELEGATION_EXCHANGE_RATE_TABLE_NAME);
                    currencyId = searchEnumIdByCn(delegationRates.getCurrencyCn(), currencyField);
                    results.put(dictionaryNameWithSuffix + CURRENCY, currencyField.getEnumValuesForForcedPush(currencyId != null ? currencyId.toString() : ""));
                } else {
                    if (DwrUtils.isNotNull(values, dictionaryNameWithSuffix + RATE)) {
                        abroadDiet = values.get(dictionaryNameWithSuffix + RATE).getMoneyData();
                    }
                }

                if (DwrUtils.isNotNull(values, dictionaryNameWithSuffix + VALUE) && values.get(dictionaryNameWithSuffix + MONEY_VALUE) != null) {
                    int breakfasts = Optional.fromNullable(values.get(dictionaryNameWithSuffix + INT1).getIntegerData()).or(0);  //15%
                    int middaymeals = Optional.fromNullable(values.get(dictionaryNameWithSuffix + INT2).getIntegerData()).or(0); //30%
                    int dinners = Optional.fromNullable(values.get(dictionaryNameWithSuffix + INT3).getIntegerData()).or(0); //30%
                    //int pockets = Sets.newTreeSet(Lists.newArrayList(breakfasts, middaymeals, dinners)).first();

                    /*if (values.get(dictionaryNameWithSuffix + ADD_RATE) != null) {
                        BigDecimal pocketsDiet = new BigDecimal(pockets*0.25).setScale(2, RoundingMode.HALF_UP);
                        values.get(dictionaryNameWithSuffix + ADD_RATE).setData(pocketsDiet);
                        results.put(dictionaryNameWithSuffix + ADD_RATE, pocketsDiet);
                    }*/

                    BigDecimal baseDiets = prepareDietCount(values.get(dictionaryNameWithSuffix + VALUE).getMoneyData());

                    BigDecimal moneyValue = baseDiets
                            .add(new BigDecimal(Optional.fromNullable(values.get(dictionaryNameWithSuffix + INT5).getIntegerData()).or(0)).movePointLeft(1))
                            .add(new BigDecimal(Optional.fromNullable(values.get(dictionaryNameWithSuffix + INT4).getIntegerData()).or(0)))
                            .subtract(new BigDecimal(breakfasts * 15 + (middaymeals + dinners) * 30).movePointLeft(2))
                            .multiply(abroadDiet).setScale(2, RoundingMode.HALF_UP);
                    values.get(dictionaryNameWithSuffix + MONEY_VALUE).setMoneyData(moneyValue);
                    results.put(dictionaryNameWithSuffix + MONEY_VALUE, moneyValue);
                }

                /*
                if (values.get(dictionaryNameWithSuffix + BOOL4) != null && values.get(dictionaryNameWithSuffix + BOOL4).getBooleanData()) {
                    values.get(dictionaryNameWithSuffix + ADD_MONEY_VALUE).setMoneyData(BigDecimal.ZERO);
                    results.put(dictionaryNameWithSuffix + ADD_MONEY_VALUE, BigDecimal.ZERO);
                } else {
                    if (DwrUtils.isNotNull(values, dictionaryNameWithSuffix + VALUE)) {
//                    przybli�one wyliczenie ilo�ci rozpocz�tych d�b podr�y
//                    iloczyn z powy�szego i 10% stawki diety, daje rycza�t koszt�w za przejazdy �rodkami komunikacji miejskiej
                        BigDecimal moneyValue = values.get(dictionaryNameWithSuffix + VALUE).getMoneyData()
                                .setScale(0,RoundingMode.UP)
                                .movePointLeft(1)
                                .multiply(abroadDiet)
                                .setScale(2,RoundingMode.HALF_UP);

                        values.get(dictionaryNameWithSuffix + ADD_MONEY_VALUE).setMoneyData(moneyValue);
                        results.put(dictionaryNameWithSuffix + ADD_MONEY_VALUE, moneyValue);
                    }
                }
                */

                if (DwrUtils.isNotNull(values, dictionaryNameWithSuffix + CURRENCY)) {

                    if (values.containsKey(dictionaryNameWithSuffix + CURRENCY_RATE_DATE)) {

                        if (DwrUtils.isNotNull(values, dictionaryNameWithSuffix + CURRENCY_RATE_DATE)
                                && DwrUtils.isSenderField(values,   dictionaryNameWithSuffix + CURRENCY_RATE,
                                                                    dictionaryNameWithSuffix + CURRENCY_RATE_DATE,
                                                                    dictionaryNameWithSuffix + COUNTRY)) {

                            if (currencyId == null) {
                                currencyId = Ints.tryParse(values.get(dictionaryNameWithSuffix + CURRENCY).getData().toString());
                            }
                            if (currencyId != null) {
                                BigDecimal rate = currencyId > 0
                                        ? DocumentCacheHandler.DATED_CURRENCY_RATES_CACHED.get(new DocumentCacheHandler.CurrencyParam(currencyId, values.get(dictionaryNameWithSuffix + CURRENCY_RATE_DATE).getDateData()))
                                        : BigDecimal.ONE;

                                if (rate != BigDecimal.ONE) {
                                    values.get(dictionaryNameWithSuffix + CURRENCY_RATE).setMoneyData(rate);
                                    results.put(dictionaryNameWithSuffix + CURRENCY_RATE, rate);
                                }
                            }
                        }
                    }


                    if (DwrUtils.isNotNull(values, dictionaryNameWithSuffix + MONEY_VALUE, dictionaryNameWithSuffix + CURRENCY_RATE) && values.get(dictionaryNameWithSuffix + MONEY_VALUE_PLN) != null && !DwrUtils.isSenderField(values, dictionaryNameWithSuffix + MONEY_VALUE_PLN)) {
                        BigDecimal plnValue = DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.MULTIPLY, 2, false, values.get(dictionaryNameWithSuffix + MONEY_VALUE), values.get(dictionaryNameWithSuffix + CURRENCY_RATE));
                        results.put(dictionaryNameWithSuffix + MONEY_VALUE_PLN, plnValue);
                    }

                    if (DwrUtils.isNotNull(values, dictionaryNameWithSuffix + ADD_MONEY_VALUE, dictionaryNameWithSuffix + CURRENCY_RATE) && values.get(dictionaryNameWithSuffix + ADD_MONEY_VALUE_PLN) != null && !DwrUtils.isSenderField(values, dictionaryNameWithSuffix + ADD_MONEY_VALUE_PLN)) {
                        BigDecimal plnValue = DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.MULTIPLY, 2, false, values.get(dictionaryNameWithSuffix + ADD_MONEY_VALUE), values.get(dictionaryNameWithSuffix + CURRENCY_RATE));
                        results.put(dictionaryNameWithSuffix + ADD_MONEY_VALUE_PLN, plnValue);
                    }


                } else {
                    results.put(dictionaryNameWithSuffix + CURRENCY_RATE, BigDecimal.ONE);
                    results.put(dictionaryNameWithSuffix + MONEY_VALUE_PLN, BigDecimal.ZERO);
                    results.put(dictionaryNameWithSuffix + ADD_MONEY_VALUE_PLN, BigDecimal.ZERO);
                }


            } catch (EdmException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    static BigDecimal prepareDietCount(BigDecimal baseDiets) {
        //obs�uga rozwini�cia dziesi�tnego 0.3(3)
        float frac = baseDiets.remainder(BigDecimal.ONE).floatValue();
        float gross = (float) (int) (baseDiets.floatValue() / 1);
        if (frac == 0.33f) {
            baseDiets = new BigDecimal(gross + 1d/3);
        }
        return baseDiets;
    }

    private Integer searchEnumIdByCn(String currencyCn, DataBaseEnumField field) {
        Collection<EnumItem> currencyEnumItems = field.getAvailableItems();
        for (EnumItem item : currencyEnumItems) {
             if (currencyCn.equals(item.getCn())) {
                 return item.getId();
            }
        }
        return null;
    }

    private void evaluateInternalDietDictionary(Map<String, FieldData> values, Map<String, Object> results) {
        BigDecimal localDietRate = BigDecimal.ONE;
        try {
            localDietRate = new BigDecimal(getValueFromSession("DIETA_STAWKA_PL"));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }


        if (DwrUtils.isNotNull(values, DIETA_KRAJOWA_S + VALUE, DIETA_KRAJOWA_S + RATE) && values.get(DIETA_KRAJOWA_S + MONEY_VALUE_PLN) != null) {
            BigDecimal currencyValue = DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.MULTIPLY, 2, false, values.get(DIETA_KRAJOWA_S + VALUE), values.get(DIETA_KRAJOWA_S + RATE))
                    .divide(new BigDecimal(100), 2, RoundingMode.HALF_UP)
                    .multiply(localDietRate).setScale(2, RoundingMode.HALF_UP);
            results.put(DIETA_KRAJOWA_S + MONEY_VALUE_PLN, currencyValue);
        }
    }

    private void evaluatePreCostDictionary(Map<String, FieldData> values, Map<String, Object> results) throws EdmException, ExecutionException {
/*        try {
            currencyId = Integer.valueOf(getValueFromSession("KRAJ_WYJAZDU_WALUTA"));
        } catch (NumberFormatException e) {
        }*/

        if (DwrUtils.isSenderField(values, KALKULACJA_KOSZTOW_S + CURRENCY,  KALKULACJA_KOSZTOW_S + CURRENCY_RATE_DATE) && DwrUtils.isNotNull(values, KALKULACJA_KOSZTOW_S + CURRENCY, KALKULACJA_KOSZTOW_S + CURRENCY_RATE_DATE)) {
            Integer currencyId = null;
            try {
                currencyId = Integer.valueOf(values.get(KALKULACJA_KOSZTOW_S + CURRENCY).getData().toString());
            } catch (NumberFormatException e) {
            }

            if (currencyId != null) {
                BigDecimal rate = currencyId > 0
                        ? DocumentCacheHandler.DATED_CURRENCY_RATES_CACHED.get(new DocumentCacheHandler.CurrencyParam(currencyId, values.get(KALKULACJA_KOSZTOW_S + CURRENCY_RATE_DATE).getDateData()))
                        : BigDecimal.ONE;

                if (rate != BigDecimal.ONE) {
                    values.get(KALKULACJA_KOSZTOW_S + CURRENCY_RATE).setMoneyData(rate);
                    results.put(KALKULACJA_KOSZTOW_S + CURRENCY_RATE, rate);
                }
            }

        }
        if (DwrUtils.isNotNull(values, KALKULACJA_KOSZTOW_S + MONEY_VALUE, KALKULACJA_KOSZTOW_S + CURRENCY_RATE) && !DwrUtils.isSenderField(values, KALKULACJA_KOSZTOW_S + MONEY_VALUE_PLN)) {
            results.put(KALKULACJA_KOSZTOW_S + MONEY_VALUE_PLN, DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.MULTIPLY, 2, false, values.get(KALKULACJA_KOSZTOW_S + CURRENCY_RATE), values.get(KALKULACJA_KOSZTOW_S + MONEY_VALUE)));
        }
    }

    private String getValueFromSession(String fieldCn) {
        WebContext dwrFactory = WebContextFactory.get();
        if (dwrFactory.getSession().getAttribute("dwrSession") instanceof Map<?, ?>) {
            Map<String, Object> dwrSession = (Map<String, Object>) dwrFactory.getSession().getAttribute("dwrSession");
            if (dwrSession.get("FIELDS_VALUES") instanceof Map<?, ?>) {
                Map<String,Object> fieldsValues = (Map<String, Object>) dwrSession.get("FIELDS_VALUES");

                Object field = fieldsValues.get(fieldCn);
                if (field != null) {
                     return field.toString();
                }
            }
        }
        return null;
    }

    private BigDecimal calcDietMealRates(Map<String, FieldData> values) {
        List<Object> breakfasts = DwrUtils.getValueListFromDictionary(values, DIETA_KRAJOWA, true, BOOL1);
        List<Object> middayMeals = DwrUtils.getValueListFromDictionary(values, DIETA_KRAJOWA, true, BOOL2);
        List<Object> dinners = DwrUtils.getValueListFromDictionary(values, DIETA_KRAJOWA, true, BOOL3);
        BigDecimal dietMealRates = new BigDecimal(0);
        BigDecimal breakFastRate = new BigDecimal(15);
        BigDecimal middayMealRate = new BigDecimal(30);
        BigDecimal dinnerRate = new BigDecimal(30);
        for (Object bool : breakfasts) {
            if (bool.equals(Boolean.TRUE)) {
                dietMealRates = dietMealRates.add(breakFastRate);
            }
        }
        for (Object bool : middayMeals) {
            if (bool.equals(Boolean.TRUE)) {
                dietMealRates = dietMealRates.add(middayMealRate);
            }
        }
        for (Object bool : dinners) {
            if (bool.equals(Boolean.TRUE)) {
                dietMealRates = dietMealRates.add(dinnerRate);
            }
        }
        return dietMealRates;
    }

    @Override
    public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException {
        PreparedStatement ps = null;
        try {
            ps = DSApi.context().prepareStatement(GROUP_COST_DICTIONARIES_SQL);
            ps.setLong(1, documentId);
            int i = ps.executeUpdate();
            if (i == 0) {
                log.error("Cant group pre cost dicitonaries");
            }
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            if (ps != null) {
                DSApi.context().closeStatement(ps);
            }
        }
    }

    @Override
    public pl.compan.docusafe.core.dockinds.dwr.Field
    validateDwr(Map<String, FieldData> values, FieldsManager fm) {
        pl.compan.docusafe.core.dockinds.dwr.Field msgField = new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", "", pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);

        StringBuilder msgBuilder = new StringBuilder();

        if (msgField != null)
            msgBuilder.append(msgField.getLabel());

        try {
            if (DwrUtils.isSenderField(values, DWR_KALKULACJA_KOSZTOW) && values.get(DWR_ZAKUP_BILETU_CZY) != null) {
                values.get(DWR_ZAKUP_BILETU_CZY).setData(isAnyFlightTripCost(values));
            }

            if (DwrUtils.isSenderField(values, DWR_KOSZTY_NOCLEGU) && values.get(DWR_RACHUNKI_ZA_NOCLEG_CZY) != null) {
                values.get(DWR_RACHUNKI_ZA_NOCLEG_CZY).setData(isAnyBillConfirmedAccomodation(values));
            }

            BigDecimal bpSum = null;
            if (values.get("DWR_SUMA_POZYCJI") != null) {
                values.get("DWR_SUMA_POZYCJI").setMoneyData(bpSum = DwrUtils.sumDictionaryMoneyFields(values, "BP", "I_AMOUNT"));
            }
            if (values.get("DWR_SUMA_POZYCJI_ROZ") != null) {
                values.get("DWR_SUMA_POZYCJI_ROZ").setMoneyData(bpSum = DwrUtils.sumDictionaryMoneyFields(values, "BP_ROZ", "I_AMOUNT"));
            }

            if (values.get("DWR_PRZEWIDYWANY_KOSZT") != null) {
                values.get("DWR_PRZEWIDYWANY_KOSZT").setMoneyData(sumPreCosts(values, KALKULACJA_KOSZTOW, MONEY_VALUE, CURRENCY_RATE));
            }


            if (DwrUtils.isSenderField(values, "DWR_SUMA_ROZLICZENIA_PLN_BUTTON")) {
                if (values.get("DWR_SUMA_ROZLICZENIA_PLN") != null) {
                    values.get("DWR_SUMA_ROZLICZENIA_PLN").setMoneyData(sumPreCosts(values, ROZLICZENIE_ZALICZKI, MONEY_VALUE, CURRENCY_RATE, ENUM, "2"));
                }

                if (values.get("DWR_SUMA_KOSZTOW_PLN") != null) {

                    FieldData type = values.get("DWR_ROZLICZENIE_TYP");

                    String typeId = type != null && type.getEnumValuesData() != null ? type.getEnumValuesData().getSelectedId() : null;
                    BigDecimal totalCost;

                    /*Ca�kowity koszt wyjazdu w pln - je�li jest do zwrotu dla pracownika - liczymy sum�: Kwota pobranej zaliczki w pln + do zwrotu pracownikowi w pln + koszty dodatkowe nie uwzgl�dnine w zaliczce*/
                    if ("2".equals(typeId) || "3".equals(typeId)) {
                        totalCost = sumPreCosts(values, ZALICZKA_WALUTOWA_DIC, MONEY_VALUE, CURRENCY_RATE)
                                .add(sumPreCosts(values, KALKULACJA_KOSZTOW, MONEY_VALUE, CURRENCY_RATE, ENUM, "1"))
                                .add(sumPreCosts(values, KALKULACJA_KOSZTOW, MONEY_VALUE, CURRENCY_RATE, ENUM, "2"))
                                .add(sumPreCosts(values, KALKULACJA_KOSZTOW, MONEY_VALUE, CURRENCY_RATE, ENUM, "3"))
                                .add(sumPreCosts(values, KALKULACJA_KOSZTOW, MONEY_VALUE, CURRENCY_RATE, ENUM, "4"))
                                .add(sumPreCosts(values, KALKULACJA_KOSZTOW, MONEY_VALUE, CURRENCY_RATE, ENUM, "10"))
                                .add(sumPreCosts(values, KALKULACJA_KOSZTOW, MONEY_VALUE, CURRENCY_RATE, ENUM, "11"))
                                .add(sumPreCosts(values, KALKULACJA_KOSZTOW, MONEY_VALUE, CURRENCY_RATE, ENUM, "12"))
                                .add(Objects.firstNonNull(values.get("DWR_SUMA_ROZLICZENIA_PLN") != null ? values.get("DWR_SUMA_ROZLICZENIA_PLN").getMoneyData() : null, BigDecimal.ZERO));
                    } else {
                        totalCost = sumPreCosts(values, KALKULACJA_KOSZTOW, MONEY_VALUE, CURRENCY_RATE, ENUM, "1")
                                    .add(sumPreCosts(values, KALKULACJA_KOSZTOW, MONEY_VALUE, CURRENCY_RATE, ENUM, "2"))
                                    .add(sumPreCosts(values, KALKULACJA_KOSZTOW, MONEY_VALUE, CURRENCY_RATE, ENUM, "3"))
                                    .add(sumPreCosts(values, KALKULACJA_KOSZTOW, MONEY_VALUE, CURRENCY_RATE, ENUM, "4"))
                                    .add(sumPreCosts(values, KALKULACJA_KOSZTOW, MONEY_VALUE, CURRENCY_RATE, ENUM, "10"))
                                    .add(sumPreCosts(values, KALKULACJA_KOSZTOW, MONEY_VALUE, CURRENCY_RATE, ENUM, "11"))
                                    .add(sumPreCosts(values, KALKULACJA_KOSZTOW, MONEY_VALUE, CURRENCY_RATE, ENUM, "12"))
                                    .add(sumPreCosts(values, DIETA_ZAGRANICZNA, MONEY_VALUE, CURRENCY_RATE))
                                    .add(sumPreCosts(values, DIETA_ZAGRANICZNA, ADD_MONEY_VALUE, CURRENCY_RATE))
                                    .add(sumPreCosts(values, KOSZTY_NOCLEGU, MONEY_VALUE, CURRENCY_RATE))
                                    .add(sumPreCosts(values, KOSZTY_NOCLEGU, ADD_MONEY_VALUE, CURRENCY_RATE))
                                    .add(sumPreCosts(values, KOSZTY_INNE, MONEY_VALUE, CURRENCY_RATE))
                                    ;
                    }

                    values.get("DWR_SUMA_KOSZTOW_PLN").setMoneyData(totalCost);
                }
            }

            if (values.get("DWR_POZOSTALO_KWOTA") != null && bpSum != null) {
                BigDecimal preCost = sumPreCosts(values, KALKULACJA_KOSZTOW, MONEY_VALUE, CURRENCY_RATE)
                                .add(sumPreCosts(values, DIETA_ZAGRANICZNA_PRE, MONEY_VALUE, CURRENCY_RATE))
                                .add(sumPreCosts(values, DIETA_ZAGRANICZNA_PRE, ADD_MONEY_VALUE, CURRENCY_RATE))
                                .add(sumPreCosts(values, KOSZTY_NOCLEGU_PRE, MONEY_VALUE, CURRENCY_RATE))
                                .add(sumPreCosts(values, KOSZTY_NOCLEGU_PRE, ADD_MONEY_VALUE, CURRENCY_RATE))
                                ;

                values.get("DWR_POZOSTALO_KWOTA").setMoneyData(preCost.subtract(bpSum));
            }

            if (values.get("DWR_POZOSTALO") != null) {
                if (bpSum != null && DwrUtils.isNotNull(values, "DWR_POZOSTALO_KWOTA") && values.get("DWR_POZOSTALO_KWOTA").getMoneyData().compareTo(BigDecimal.ZERO) == 0) {
                    values.get("DWR_POZOSTALO").setStringData("Rozpisano ca�\u0105 kwot\u0119");
                } else {
                    values.get("DWR_POZOSTALO").setData("Prosz\u0119 rozpisa� ca�y koszt delegacji");
                }
            }

            if (DwrUtils.isNotNull(values, /*"DWR_CZAS_PODR_WYJAZD_DATA", "DWR_CZAS_PODR_POWROT_DATA",*/ "DWR_CZAS_PODR_WYJAZD_DATA_GR", "DWR_CZAS_PODR_POWROT_DATA_GR")) {
                setTripTimeAndDietAmount(values);
            }

            if (values.get(DWR_PREFIX + DIETA_KRAJOWA_S + "SUMA") != null && values.get(DWR_PREFIX + DIETA_KRAJOWA_S + "MONEY") != null) {
                evaluateLocalDiet(values);
            }

            if (values.get(DWR_PREFIX + DIETA_KRAJOWA_S + "ROZLICZENIE") != null) {
                values.get(DWR_PREFIX + DIETA_KRAJOWA_S + "ROZLICZENIE").setMoneyData(sumDietFromDictionary(values, DIETA_KRAJOWA_S + "SUMA", DIETA_KRAJOWA, VALUE));
            }

            if (values.get(DWR_PREFIX + DIETA_ZAGRANICZNA_S + "ROZLICZENIE") != null) {
                values.get(DWR_PREFIX + DIETA_ZAGRANICZNA_S + "ROZLICZENIE").setMoneyData(sumDietFromDictionary(values, DIETA_ZAGRANICZNA_S + "SUMA", DIETA_ZAGRANICZNA, VALUE));
            }

/*            // wyliczenie koszt�w przejazd�w komunikacj� miejsk�
            if (DwrUtils.isNotNull(values, "DWR_TRANSPORT", "DWR_CZAS_PODR_WYJAZD_DATA_GR", "DWR_CZAS_PODR_POWROT_DATA_GR", "DWR_DIETA_STAWKA")
                    && values.get("DWR_TRANSPORT").getData().toString().equals("3")
                    && currencyId != null) {
                evaluatePublicTransportTripCosts(values, currencyId);
            }

            // wyliczenie koszt�w dojazd�w z/do dworca...
            if (DwrUtils.isNotNull(values, "DWR_TRANSPORT", "DWR_DIETA_STAWKA")
                    && values.get("DWR_TRANSPORT").getData().toString().equals("3")
                    && currencyId != null) {
                evaluateCommutingCosts(values, currencyId);
            }*/

            if (DwrUtils.isSenderField(values, "DWR_ROZLICZENIE_SUMA_PLN_BUTTON")) {
                sumWholeStatementSummary(values);
            }

            if (DwrUtils.isSenderField(values, "DWR_ROZLICZENIE_ZALICZKI_GENERUJ")) {
                boolean wasOpened = true;
                if (!DSApi.isContextOpen()) {
                    wasOpened = false;
                    DSApi.openAdmin();
                }
                try {
                    calculateDelegationCost(fm.getDocumentId());
                } finally {
                    if (DSApi.isContextOpen() && !wasOpened)
                        DSApi.close();
                }
            }

            if (msgBuilder.length() > 0) {
                values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
                return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    private boolean isAnyBillConfirmedAccomodation(Map<String, FieldData> values) {
        List<Object> enums = DwrUtils.getValueListFromDictionary(values, KOSZTY_NOCLEGU, true, "ENUM");
        for (Object enumItem : enums) {
            if (enumItem != null) {
                if (BILL_CONFIRMED_ACCOMODATION_ID.equals(enumItem.toString()) || BILL_CONFIRMED_ACCOMODATION_BELLOW_LIMIT_ID.equals(enumItem.toString())) {
                    return true;
                }
            }
        }

        return false;
    }

    private boolean isAnyFlightTripCost(Map<String, FieldData> values) {
        List<Object> enums = DwrUtils.getValueListFromDictionary(values, KALKULACJA_KOSZTOW, true, "ENUM");
        for (Object enumItem : enums) {
            if (enumItem != null) {
                if (FLIGHT_TRIP_COST_ID.equals(enumItem.toString())) {
                    return true;
                }
            }
        }
        return false;
    }

    private void evaluateLocalDiet(Map<String, FieldData> values) {
        try {
            BigDecimal localDietMoneyRate = getValueFromDelgationCostSystemDic(PL_DIET_RATE_ID, 30);
            Optional<BigDecimal> baseDiets = Optional.of(values.get(DWR_PREFIX + DIETA_KRAJOWA_S + "SUMA").getMoneyData());
            BigDecimal mealDiets = calcDietMealRates(values).movePointLeft(2);

            values.get(DWR_PREFIX + DIETA_KRAJOWA_S + "SUMA").setMoneyData(baseDiets.or(BigDecimal.ZERO).subtract(mealDiets));
            values.get(DWR_PREFIX + DIETA_KRAJOWA_S + "MONEY").setMoneyData(values.get(DWR_PREFIX + DIETA_KRAJOWA_S + "SUMA").getMoneyData().multiply(localDietMoneyRate).setScale(2, RoundingMode.HALF_UP));

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private Integer getDelegationCurrencyId(Map<String, FieldData> values) {
        Integer currencyId = null;
        if (DwrUtils.isNotNull(values, "DWR_KRAJ_WYJAZDU_WALUTA")) {
            String currencyIdRaw = values.get("DWR_KRAJ_WYJAZDU_WALUTA").getData().toString();
            try {
                currencyId = Integer.valueOf(currencyIdRaw);
            } catch (NumberFormatException e) {
            }
        }
        return currencyId;
    }

    private void sumWholeStatementSummary(Map<String, FieldData> values) {
        BigDecimal sum = new BigDecimal(0);

        List<String> statementDictionariesCns = Lists.newArrayList(DIETA_KRAJOWA, DIETA_ZAGRANICZNA, KOSZTY_NOCLEGU, KOSZTY_INNE);
        List<String> statementFieldsCns = Lists.newArrayList(KOSZTY_DOJAZDY_S + "MONEY_PLN", "KOSZTY_PRZEJAZDY_MONEY_PLN");

        for (String dictCn : statementDictionariesCns) {
            if (values.get(DWR_PREFIX+dictCn) != null) {
                sum = sum.add(DwrUtils.sumDictionaryMoneyFields(values, dictCn, MONEY_VALUE_PLN));
                sum = sum.add(DwrUtils.sumDictionaryMoneyFields(values, dictCn, ADD_MONEY_VALUE_PLN));
            }
        }

        for (String fieldCn : statementFieldsCns) {
            if (values.get(DWR_PREFIX+fieldCn) != null && values.get(DWR_PREFIX+fieldCn).getMoneyData() != null) {
                sum = sum.add(values.get(DWR_PREFIX+fieldCn).getMoneyData());
            }
        }

        if (values.get("DWR_ROZLICZENIE_SUMA_PLN") != null) {
            values.get("DWR_ROZLICZENIE_SUMA_PLN").setMoneyData(sum);
        }
    }

    private void evaluateCommutingCosts(Map<String, FieldData> values, Integer currencyId) throws ExecutionException {
        if (DwrUtils.isNotNull(values, DWR_KOSZTY_DOJAZDY + CURRENCY_RATE_DATE) && values.get(DWR_KOSZTY_DOJAZDY + CURRENCY_RATE) != null) {
            Date date = values.get(DWR_KOSZTY_DOJAZDY + CURRENCY_RATE_DATE).getDateData();
            BigDecimal rate = DocumentCacheHandler.DATED_CURRENCY_RATES_CACHED.get(new DocumentCacheHandler.CurrencyParam(currencyId, date));
            values.get(DWR_KOSZTY_DOJAZDY + CURRENCY_RATE).setMoneyData(rate);
        }
        if (DwrUtils.isNotNull(values, DWR_KOSZTY_DOJAZDY + "EDITABLE") && values.get(DWR_KOSZTY_DOJAZDY + "MONEY") != null) {
            Integer accomodationAmount = null;
            if (values.get(DWR_KOSZTY_DOJAZDY + "EDITABLE").getData().toString().equals("2")) {
                if (values.get("DWR_" + KOSZTY_NOCLEGU) != null) {
                    accomodationAmount = DwrUtils.countNoOfItemsInDictionary(values, KOSZTY_NOCLEGU);
                    values.get(DWR_KOSZTY_DOJAZDY + "AMOUNT").setIntegerData(accomodationAmount);
                }
            } else {
                if (DwrUtils.isNotNull(values, DWR_KOSZTY_DOJAZDY + "AMOUNT")) {
                    accomodationAmount = values.get(DWR_KOSZTY_DOJAZDY + "AMOUNT").getIntegerData();
                }
            }
            if (accomodationAmount != null && DwrUtils.isNotNull(values, DWR_KOSZTY_DOJAZDY + CURRENCY_RATE)) {
                BigDecimal rate = values.get(DWR_KOSZTY_DOJAZDY + CURRENCY_RATE).getMoneyData();
                BigDecimal money = values.get("DWR_DIETA_STAWKA").getMoneyData().multiply(new BigDecimal(accomodationAmount)).multiply(rate).setScale(2, RoundingMode.HALF_UP);
                values.get(DWR_KOSZTY_DOJAZDY + "MONEY").setMoneyData(money);
            }
        }
        if (DwrUtils.isNotNull(values, DWR_KOSZTY_DOJAZDY + "MONEY", DWR_KOSZTY_DOJAZDY + CURRENCY_RATE) && values.get(DWR_KOSZTY_DOJAZDY + "MONEY_PLN") != null) {
            values.get(DWR_KOSZTY_DOJAZDY + "MONEY_PLN").setMoneyData(DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.MULTIPLY, 2, false, values.get(DWR_KOSZTY_DOJAZDY + CURRENCY_RATE), values.get(DWR_KOSZTY_DOJAZDY + "MONEY")));
        }
    }

    private void evaluatePublicTransportTripCosts(Map<String, FieldData> values, Integer currencyId) throws ExecutionException {
        if (values.get("DWR_KOSZTY_PRZEJAZDY_MONEY") != null) {
            LocalDate begin = new LocalDate(values.get("DWR_CZAS_PODR_WYJAZD_DATA_GR").getDateData());
            LocalDate end = new LocalDate(values.get("DWR_CZAS_PODR_POWROT_DATA_GR").getDateData());
            int daysBetweenDates = Math.abs(Days.daysBetween(end, begin).getDays())+1;
            BigDecimal money = values.get("DWR_DIETA_STAWKA").getMoneyData()
                    .multiply(PROCENT_DIETY_ZA_PRZEJAZDY).multiply(new BigDecimal(daysBetweenDates)).setScale(2, RoundingMode.HALF_UP);
            values.get("DWR_KOSZTY_PRZEJAZDY_MONEY").setMoneyData(money);

            if (DwrUtils.isNotNull(values, "DWR_KOSZTY_PRZEJAZDY_CURRENCY_RATE_DATE") && currencyId != null && values.get("DWR_KOSZTY_PRZEJAZDY_CURRENCY_RATE") != null) {
                Date date = values.get("DWR_KOSZTY_PRZEJAZDY_CURRENCY_RATE_DATE").getDateData();
                BigDecimal rate = DocumentCacheHandler.DATED_CURRENCY_RATES_CACHED.get(new DocumentCacheHandler.CurrencyParam(currencyId, date));
                values.get("DWR_KOSZTY_PRZEJAZDY_CURRENCY_RATE").setMoneyData(rate);
            }

            if (DwrUtils.isNotNull(values, "DWR_KOSZTY_PRZEJAZDY_CURRENCY_RATE", "DWR_KOSZTY_PRZEJAZDY_MONEY") && values.get("DWR_KOSZTY_PRZEJAZDY_MONEY_PLN") != null) {
                values.get("DWR_KOSZTY_PRZEJAZDY_MONEY_PLN").setMoneyData(DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.MULTIPLY, 2, false, values.get("DWR_KOSZTY_PRZEJAZDY_CURRENCY_RATE"), values.get("DWR_KOSZTY_PRZEJAZDY_MONEY")));
            }
        }
    }

    private BigDecimal sumDietFromDictionary(Map<String, FieldData> values, String expectedSumFieldCn, String dictionaryCn, String dictionaryMoneyFieldCn) {
        if (DwrUtils.isNotNull(values, DWR_PREFIX+expectedSumFieldCn, DWR_PREFIX+dictionaryCn)) {
            BigDecimal expectedSum = values.get(DWR_PREFIX+expectedSumFieldCn).getMoneyData();
            BigDecimal realSum = DwrUtils.sumDictionaryMoneyFields(values, dictionaryCn, dictionaryMoneyFieldCn);

            return expectedSum.subtract(realSum);
        }
        return BigDecimal.ZERO;
    }

    private void setTripTimeAndDietAmount(Map<String, FieldData> values) {
        try {
            //Date wyjazd = values.get("DWR_CZAS_PODR_WYJAZD_DATA").getDateData();
            Date wyjazdGr = values.get("DWR_CZAS_PODR_WYJAZD_DATA_GR").getDateData();
            //Date powrot = values.get("DWR_CZAS_PODR_POWROT_DATA").getDateData();
            Date powrotGr = values.get("DWR_CZAS_PODR_POWROT_DATA_GR").getDateData();

            Date czasPoza = new Date(powrotGr.getTime() - wyjazdGr.getTime());
           //Date czasPl = new Date(((powrot.getTime() - wyjazd.getTime()) - czasPoza.getTime()));

            //values.get(DWR_PREFIX + DIETA_KRAJOWA_S + "SUMA").setMoneyData(new BigDecimal(DelegationLogic.evaluateDietAmount(czasPl)).setScale(2,RoundingMode.HALF_UP));
            values.get(DWR_PREFIX + DIETA_ZAGRANICZNA_S + "SUMA").setMoneyData(new BigDecimal(evaluateDietAmount(czasPoza)).setScale(2,RoundingMode.HALF_UP));

            //values.put("DWR_CZAS_PODR_KRAJ", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, getTimeString(czasPl.getTime())));
            values.put("DWR_CZAS_PODR_ZAGR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, getTimeString(czasPoza.getTime())));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public static double evaluateDietAmount(Date diff) {
        double hours = DateUtils.hours(diff.getTime());

        double diet = (long) (hours / 24);
        double rest = hours % 24;

        if (rest > 12) {
            diet++;
        } else if (rest > 8) {
            diet+=0.5;
        } else if (rest > 0) {
            diet+=(double)1/3;
        }
        return diet;
    }

    public static String getTimeString(long time){
        String ret="";
        long dni = time/(1000*60*60*24);
        long godzin = (time - (dni*1000*60*60*24))/(1000*60*60);
        long minut = (time - (dni*1000*60*60*24) - (godzin*1000*60*60))/(1000*60);
        ret = (dni > 0 ? dni + " " + (dni > 1 ? "dni" : "dzie�") + ", " : "") + (godzin > 0 ? godzin + " godzin" + (godzin < 5 ? "y" : "") + ", " : "") + (minut > 0 ? minut + " minut" + (minut < 5 ? "y" : "") : "");
        return ret.endsWith(", ") ? ret.substring(0, ret.length() - 2) : ret;
    }

    private BigDecimal sumPreCosts(Map<String, FieldData> values, String dictionaryCn, String moneyFieldCn, String rateFieldCn) {
        return sumPreCosts(values, dictionaryCn, moneyFieldCn, rateFieldCn, null ,null);
    }

    private BigDecimal sumPreCosts(Map<String, FieldData> values, String dictionaryCn, String moneyFieldCn, String rateFieldCn, String enumFieldCn, String enumIdCondition) {
        BigDecimal amountSum = BigDecimal.ZERO;
        if (values.get(DWR_PREFIX + dictionaryCn) != null && values.get(DWR_PREFIX + dictionaryCn).getData() instanceof Map<?, ?>) {
            Map<String, FieldData> dictionary = values.get(DWR_PREFIX + dictionaryCn).getDictionaryData();

            for (int i = 1; true; i++) {
                String processedField = dictionaryCn + "_" + moneyFieldCn + "_" + i;
                String processedRateField = dictionaryCn + "_" + rateFieldCn + "_" + i;
                String processedEnumField = dictionaryCn + "_" + enumFieldCn + "_" + i;
                if (!dictionary.containsKey(processedField) || !dictionary.containsKey(processedRateField)) {
                    break;
                }

                if (dictionary.get(processedField) != null && dictionary.get(processedRateField) != null
                        && dictionary.get(processedField).getType().equals(pl.compan.docusafe.core.dockinds.dwr.Field.Type.MONEY) && dictionary.get(processedRateField).getType().equals(pl.compan.docusafe.core.dockinds.dwr.Field.Type.MONEY)
                        && dictionary.get(processedField).getMoneyData() != null && dictionary.get(processedRateField).getMoneyData() != null
                        && (enumFieldCn == null || (dictionary.get(processedEnumField) != null && ObjectUtils.toString(dictionary.get(processedEnumField).getData()).equals(enumIdCondition)))) {
                    amountSum = amountSum.add(dictionary.get(processedField).getMoneyData().multiply(dictionary.get(processedRateField).getMoneyData()).setScale(2, RoundingMode.HALF_UP));
                }
            }
        }

        return amountSum;
    }

    @Override
    public void setAdditionalTemplateValues(long docId, Map<String, Object> values, Map<Class, Format> defaultFormatters) {
        try {
            Document doc = Document.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            fm.initialize();

            DelegationTemplateHelper th = new DelegationTemplateHelper(fm, (OfficeDocument) doc, log);

            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setDecimalSeparator(',');
            DecimalFormat moneyFormatter = new DecimalFormat("### ### ##0.00", symbols);
            defaultFormatters.put(BigDecimal.class, moneyFormatter);
            defaultFormatters.put(Date.class, new SimpleDateFormat("dd-MM-yyyy"));

            values.put("DELEGOWANY", th.getDelegate());
            values.put(GENERATE_DATE_TEMPLATE_VAR, th.getDocTemplateCreateDate());
            values.put(DOC_DATE_TEMPLATE_VAR, th.getDocCreateDate());
            values.put("KP", th.getPodrozItems());
            values.put("KK", th.getAbroadCostDict());
            values.put("ACC", th.getAcceptInfo());
            Multimap<String,DelegationTemplateHelper.AbroadCost> abroadCostInfo = th.getAbroadCostInfo();
            values.put("K", abroadCostInfo);
            values.put("K_SUMA", th.getAbroadCostInfoSum(abroadCostInfo));
            values.put("FUND_SOURCES_WITH_AMOUNT", th.getFundSourcesWithAmount());
            values.put("BP_DESCRIPTION", th.getBudgetPositionDescription(BP_FIELD));
            values.put("PROJEKT", th.getProjects());
            values.put("ZF", th.getFundSources());
            values.put("MPK", th.getMpks());
            values.put("KWOTA_ZWROTU_ZALICZKI", th.getLoanReturnAmount());
            DelegationTemplateHelper.MultipleAmountExtended toPay = th.getReturnAmounts("2");
            values.put("WYPLATA_Z_KASY", toPay);
            DelegationTemplateHelper.MultipleAmountExtended workerReturn = th.getReturnAmounts("1");
            values.put("ZWROT_PRACOWNIKA", workerReturn);
            //values.put("ZALICZKA_WALUTOWA_KWOTA", th.getLoanAmount());

            BigDecimal loan = th.getMoneyPlnFromAbroadCost(abroadCostInfo, "ZALICZKA");
            values.put("ZALICZKA", loan);

            values.put("WYDATKI", th.calcAllCost(loan, workerReturn, toPay));

            for (String fieldCn : Lists.newArrayList("CZAS_PODR_WYJAZD_DATA","CZAS_PODR_POWROT_DATA","CZAS_PODR_WYJAZD_DATA_GR","CZAS_PODR_POWROT_DATA_GR")) {
               values.put(fieldCn, th.getTimeFormatFromDate(fieldCn));
            }

            putDictionaryValues(values, fm, "ZALICZKA_NR_KONTA");
            putDictionaryValues(values, fm, KALKULACJA_KOSZTOW);
            putDictionaryValues(values, fm, KRAJ_WYJAZDU);




        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public TaskListParams getTaskListParams(DocumentKind kind, long documentId, TaskSnapshot task) throws EdmException {
        TaskListParams params = new TaskListParams();
        FieldsManager fm = kind.getFieldsManager(documentId);

        // Status
        params.setStatus((String) fm.getValue("STATUS"));

        if (Strings.isNullOrEmpty(task.getRealDescription())) {
            task.setDescription("Brak");
        }

        try {
            Object amount = fm.getKey("SUMA_POZYCJI");
            params.setAmount((BigDecimal) amount);
        } catch (Exception e1) {
            log.error("B��d przy ustawianiu kolumny kwota, listy zada�");
            log.error(e1.getMessage(),e1);
        }

        params.setAttribute("Nie dotyczy", 1);
        params.setAttribute("Nie dotyczy", 2);
        params.setAttribute("Nie dotyczy", 3);

        params.setAttribute(ObjectUtils.toString(fm.getValue("WORKER_DIVISION"), "-"), 4);

        params.setAttribute(getDelegationTargetCountry(fm), 5);

        return params;
    }

    private String getDelegationTargetCountry(FieldsManager fm) {
        try {
            List<Map<String, Object>> dicLists = (List<Map<String, Object>>) fm.getValue(KRAJ_WYJAZDU);
            if (dicLists != null) {
                for (Map<String, Object> item : dicLists) {
                    try {
                        if (item.get(KRAJ_WYJAZDU + "_DESTINATION") != null && "9041".equals(((EnumValues) item.get(KRAJ_WYJAZDU + "_DESTINATION")).getSelectedId())) {
                            if (item.get(KRAJ_WYJAZDU + "_COUNTRY") != null && ((EnumValues) item.get(KRAJ_WYJAZDU + "_COUNTRY")).getSelectedValue() != null) {
                                return ((EnumValues) item.get(KRAJ_WYJAZDU + "_COUNTRY")).getSelectedValue();
                            }
                        }
                    } catch (RuntimeException er) {
                        log.error(er.getMessage(), er);
                    }
                }
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }
}
