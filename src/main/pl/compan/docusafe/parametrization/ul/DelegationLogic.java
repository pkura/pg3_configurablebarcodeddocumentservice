package pl.compan.docusafe.parametrization.ul;

import com.google.common.base.*;
import com.google.common.collect.*;
import com.google.common.primitives.Ints;
import org.apache.commons.lang.ObjectUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.parametrization.ul.hib.Product;
import pl.compan.docusafe.parametrization.ul.hib.UlBudgetPosition;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;

import static pl.compan.docusafe.parametrization.ul.CostInvoiceLogic.VAT_RATES_TABLE_NAME;

public class DelegationLogic extends AbstractDelegationLogic {
    protected static final Logger log = LoggerFactory.getLogger(DelegationLogic.class);

    public static final String DWR_WNIOSEK_WYJAZDOWY = "DWR_WNIOSEK_WYJAZDOWY";
    public static final String DWR_TERMIN_WYJAZDU = "DWR_TERMIN_WYJAZDU";
    public static final String DWR_TERMIN_POWROTU = "DWR_TERMIN_POWROTU";
    public static final String NR_WYJAZDU_FIELD = "NR_WYJAZDU";
    private static final String OD_DO_ERROR_MSG = "Termin wyjazdu nie mo�e by� p�niej ni� termin powrotu";
    public static final String DOC_KIND_CN = "ul_delegation";
    public static final BigDecimal DEFAULT_TRIP_COST_RATE = new BigDecimal(0.8358d);
    public static final String DWR_PRZEWIDYWANY_KOSZT = "DWR_PRZEWIDYWANY_KOSZT";
    public static final String DWR_ZALICZKA_KWOTA = "DWR_ZALICZKA_KWOTA";
    public static final String DOJAZDY_UDOKUMENTOWANE_DWR = "DWR_DOJAZDY_UDOKUMENTOWANE";
    public static final String RYCZALT_ZA_DOJAZDY_DWR = "DWR_RYCZALT_COST_TO_SUM";
    public static final String DIETA_PLN_DWR = "DWR_DIETA_KRAJOWA_MONEY";
    public static final String NOCLEGI_WG_RACHUNKOW_DWR = "DWR_NOCLEGI_COST_TO_SUM";
    public static final String INNE_WYDATKI_DWR = "DWR_INNE_WYDATKI_COST_TO_SUM";
    public static final String NOCLEGI_RYCZALT_DWR = "DWR_NOCLEGI_RYCZALT_COST_TO_SUM";

    public static final ImmutableList<CostToProductMapping> COST_TO_PRODUCT_MAPPINGS = ImmutableList.<CostToProductMapping>builder()
            //.add(new CostToProductMapping(DOJAZDY_UDOKUMENTOWANE_DWR)                           .addProduct(1, "460-04-0-0-xx").addProduct(2, "460-04-0-0-xx").addProduct(3, "460-04-0-0-xx"))
            .add(new CostToProductMapping(RYCZALT_ZA_DOJAZDY_DWR).addProduct(1, "460-01-1-5-00").addProduct(2, "460-01-3-5-00").addProduct(3, "460-01-2-5-00"))
            .add(new CostToProductMapping(NOCLEGI_WG_RACHUNKOW_DWR).addProduct(1, "460-01-1-4-xx").addProduct(2, "460-01-3-4-xx").addProduct(3, "460-01-2-4-xx"))
            .add(new CostToProductMapping(NOCLEGI_RYCZALT_DWR).addProduct(1, "460-01-1-5-00").addProduct(2, "460-01-3-5-00").addProduct(3, "460-01-2-5-00"))
            .add(new CostToProductMapping(DIETA_PLN_DWR).addProduct(1, "460-01-1-5-00").addProduct(2, "460-01-3-5-00").addProduct(3, "460-01-2-5-00"))
            .add(new CostToProductMapping(INNE_WYDATKI_DWR).addProduct(1, "460-01-1-9-xx").addProduct(2, "460-01-3-9-xx").addProduct(3, "460-01-2-9-xx"))
                    //samoch�d
            .add(new CostToProductMapping(KOSZTY_PODROZY, "KOSZT", "TRANSPORT", "3", "6").addProduct(1, "460-01-1-2-00").addProduct(2, "460-01-3-2-00").addProduct(3, "460-01-2-2-00"))
                    //poci�g
            .add(new CostToProductMapping(KOSZTY_PODROZY, "KOSZT", "TRANSPORT", "1", "4", "8").addProduct(1, "460-01-1-3-xx").addProduct(2, "460-01-3-3-xx").addProduct(3, "460-01-2-3-xx"))
                    //samolot
            .add(new CostToProductMapping(KOSZTY_PODROZY, "KOSZT", "TRANSPORT", "2").addProduct(1, "460-01-1-1-xx").addProduct(2, "460-01-3-1-xx").addProduct(3, "460-01-2-1-xx"))
            .build();

    public static final String GENERUJ_POZYCJE_ID = "GENERUJ_POZYCJE_ID";

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        FieldsManager fm = document.getFieldsManager();
        try {
            assignOrdinalNumber(fm, BP_ROZ_FIELD);
            reloadDBEnumFieldData("dsg_ul_delegation_budget_position_number");
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
    }

    @Override
    void getAddtitionFieldsDescription(FieldsManager fm, Map<String,String> additionFields) throws EdmException{
        additionFields.put("Data wype�nienia", fm.getStringValue("DATA_WYPELNIENIA"));
        additionFields.put("Cel wyjazdu", fm.getStringValue("CEL_WYJAZDU"));
        additionFields.put("Termin wyjazdu", fm.getStringValue("TERMIN_WYJAZDU"));
        additionFields.put("Termin powrotu", fm.getStringValue("TERMIN_POWROTU"));
        additionFields.put("Nazwa jednostki do kt�rej udaje si� delegowany", fm.getStringValue("JEDNOSTKA_DELEGACJI"));
        additionFields.put("Miejscowo�� delegacji", fm.getStringValue("MIEJSCOWOSC_WYJAZDU"));
        additionFields.put("Dodatkowe informacje", fm.getStringValue("ADD_INFO"));
        additionFields.put("Podr� samochodem prywatnym", fm.getStringValue("SRODEK_LOKOMOCJI_SAMOCHOD"));
        additionFields.put("�rodek lokomocji", fm.getStringValue("SRODEK_LOKOMOCJI"));
        additionFields.put("Przewidywany koszt delegacji", fm.getStringValue("PRZEWIDYWANY_KOSZT"));
        additionFields.put("Kwota zaliczki", fm.getStringValue("ZALICZKA_KWOTA "));
        additionFields.put("Pro�ba o zaliczk�", fm.getStringValue("ZALICZKA"));
        additionFields.put("Forma przekazania zaliczki", fm.getStringValue("FORMA_ZALICZKI"));
        additionFields.put("Przelew zaliczki na konto", fm.getStringValue("PRZELEW_NA"));
        additionFields.put("Czy zapewniono refundacja koszt�w", fm.getStringValue("REFUNDACJA_KOSZTY_CZY"));
        additionFields.put("Kto refunduje koszty", fm.getStringValue("REFUNDACJA_KOSZTY_OPIS"));
        additionFields.put("Rycza�ty za dojazdy (ilo��)", fm.getStringValue("DOJAZDY_RYCZALT_ILOSC"));
        additionFields.put("Rycza�ty za dojazdy (PLN)", fm.getStringValue("RYCZALT_COST_TO_SUM"));
        additionFields.put("Dojazdy udokumentowane", fm.getStringValue("DOJAZDY_UDOKUMENTOWANE"));
        additionFields.put("Razem przyjazdy, dojazdy (PLN)", fm.getStringValue("SUMA_KOSZTOW_PODROZY_COST_TO_SUM"));
        additionFields.put("Diety (ilo��)", fm.getStringValue("DIETA_KRAJOWA_ILOSC"));
        additionFields.put("Diety (PLN)", fm.getStringValue("DIETA_KRAJOWA_MONEY"));
        additionFields.put("Noclegi wg rachunk�w (PLN)", fm.getStringValue("NOCLEGI_COST_TO_SUM"));
        additionFields.put("Noclegi - rycza�t (ilo��)", fm.getStringValue("NOCLEGI_RYCZALT_ILOSC"));
        additionFields.put("Noclegi - rycza�t (PLN)", fm.getStringValue("NOCLEGI_RYCZALT_COST_TO_SUM"));
        additionFields.put("Inne wydatki wg za��cznik�w (PLN)", fm.getStringValue("INNE_WYDATKI_COST_TO_SUM"));
        additionFields.put("Kwota og�em wydana (PLN)", fm.getStringValue("SUMA"));
        additionFields.put("Pobrana zaliczka", fm.getStringValue("POBRANA_ZALICZKA"));
        additionFields.put("Do wyp�aty", fm.getStringValue("DO_WYPLATY"));
        additionFields.put("Do zwrotu", fm.getStringValue("DO_ZWROTU"));
        additionFields.put("Forma rozliczenia delegacji", fm.getStringValue("FORMA_ZWROTU"));
        additionFields.put("Przelew na konto", fm.getStringValue("ROZLICZENIE_PRZELEW_NA"));
    }

    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) {
        Map<String, Object> results = new HashMap<String, Object>();

        try {
            if (dictionaryName.equals(CostInvoiceLogic.STAWKI_VAT_FIELD_NAME)) {
                calcStawkiVat(CostInvoiceLogic.STAWKI_VAT_FIELD_NAME, values, results);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        try {
            if (dictionaryName.equals(BP_FIELD)) {
                setDictionaries(dictionaryName, values, results);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        try {
            if (dictionaryName.equals(KOSZTY_PODROZY)) {
                setKoszt(values, results);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return results;
    }

    private void setKoszt(Map<String, FieldData> values, Map<String, Object> result) {
        BigDecimal rate;
        if (/*!DwrUtils.isSenderField(values, KOSZTY_PODROZY + "_STAWKA") && */!DwrUtils.isNotNull(values, KOSZTY_PODROZY + "_STAWKA")) {
            rate = DEFAULT_TRIP_COST_RATE;
            result.put(KOSZTY_PODROZY + "_STAWKA", rate);
        } else {
            rate = values.get(KOSZTY_PODROZY + "_STAWKA").getMoneyData();
        }

        if (rate.compareTo(BigDecimal.ZERO) != 0) {
            result.put(KOSZTY_PODROZY + "_KOSZT", DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.MULTIPLY, 2, false, values, KOSZTY_PODROZY + "_ILOSC_KM", KOSZTY_PODROZY + "_STAWKA"));
        }
    }

    public static double evaluateDietAmount(Date diff) {
        double hours = DateUtils.hours(diff.getTime());

        double diet = (long) (hours / 24);
        double rest = hours % 24;

        if (diet == 0) {
            if (rest < 8) {
            } else if (rest <= 12) {
                diet+=0.5;
            } else {
                diet++;
            }
        } else {
            if (rest > 8) {
                diet++;
            } else if (rest > 0 && rest <= 8) {
                diet+=0.5;
            }
        }
        return diet;
    }

    @Override
    public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) {
        pl.compan.docusafe.core.dockinds.dwr.Field msgField = new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", "", pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);

        StringBuilder msgBuilder = new StringBuilder();

        msgBuilder.append(msgField.getLabel());

        try {
            BigDecimal bpSum = null;
            if (values.get(DWR_PRZEWIDYWANY_KOSZT) != null) {
                values.get(DWR_PRZEWIDYWANY_KOSZT).setMoneyData(bpSum = DwrUtils.sumDictionaryMoneyFields(values, "BP", "I_AMOUNT"));
            }

            if (values.get("DWR_POZOSTALO") != null) {
                if (bpSum != null && DwrUtils.isNotNull(values, DWR_PRZEWIDYWANY_KOSZT) && values.get(DWR_PRZEWIDYWANY_KOSZT).getMoneyData().compareTo(bpSum) == 0) {
                    values.get("DWR_POZOSTALO").setStringData("Rozpisano ca�\u0105 kwot\u0119");
                } else {
                    values.get("DWR_POZOSTALO").setData("Prosz\u0119 rozpisa� ca�y koszt delegacji");
                }
            }

            if (DwrUtils.isNotNull(values, DWR_PRZEWIDYWANY_KOSZT) && values.get(DWR_ZALICZKA_KWOTA) != null && !DwrUtils.isNotNull(values, DWR_ZALICZKA_KWOTA)) {
                values.get(DWR_ZALICZKA_KWOTA).setData(values.get(DWR_PRZEWIDYWANY_KOSZT).getData());
            }

            if (DwrUtils.isNotNull(values, DWR_TERMIN_WYJAZDU, DWR_TERMIN_POWROTU)) {
                msgBuilder.append(validateDate(values.get(DWR_TERMIN_WYJAZDU), values.get(DWR_TERMIN_POWROTU), OD_DO_ERROR_MSG).toString());
            }

            // wyliczanie kosztow ryczaltu dojazdowych
            calculateDelegationCost(values, "DWR_DOJAZDY_RYCZALT_ILOSC", 4, RYCZALT_ZA_DOJAZDY_DWR, 6);


            // wyliczanie kosztow diety
            //calculateDelegationCost(values, "DWR_DIETY_ILOSC", PL_DIET_RATE_ID, "DWR_DIETY_COST_TO_SUM", 30);

            if (values.get("DWR_DIETA_KRAJOWA_ILOSC") != null && values.get(DIETA_PLN_DWR) != null) {
                evaluateLocalDiet(values);
            }

            // wyliczanie ryczaltow noclegow
            calculateDelegationCost(values, "DWR_NOCLEGI_RYCZALT_ILOSC", 3, NOCLEGI_RYCZALT_DWR, 45);

            sumTripCosts(values);

            setLoanField(values);

            sumDelegationCosts(values);

            setPayOrReturnFields(values);

            if (DwrUtils.isSenderField(values, "DWR_GENERUJ_POZYCJE")) {
                generatePosition(values);
            }

            if (msgBuilder.length() > 0) {
                values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
                return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public void correctValues(Map<String, Object> values, DocumentKind kind) throws EdmException {
        if (values.get(GENERUJ_POZYCJE_ID) != null && !Strings.isNullOrEmpty(values.get(GENERUJ_POZYCJE_ID).toString())) {
            values.put(BP_ROZ_FIELD, Lists.newArrayList(Iterables.transform(Splitter.on(',').split(values.get(GENERUJ_POZYCJE_ID).toString()),
                    new Function<String, Long>() {
                @Override
                public Long apply(String input) {
                    return Long.parseLong(input);
                }
            })));
        }
    }

    private void generatePosition(Map<String, FieldData> values) {
        boolean txOpened = false;
        try {
            Integer costType = getBaseCostTypeId(values);

            List<Object> ids = DwrUtils.getValueListFromDictionary(values, BP_FIELD, true, "ID");
            List<Object> lps = Lists.newArrayList();
            //List<Object> lps = DwrUtils.getValueListFromDictionary(values, BP_FIELD, true, "I_NO");

            UlBudgetPosition finder = UlBudgetPosition.getInstance();

            Integer id = null;
            for (Object idRaw : ids) {
                id = Ints.tryParse(ObjectUtils.toString(idRaw));
                if (id != null) {
                    break;
                }
            }
            ids.clear();
            Preconditions.checkNotNull(id, "none of budget positions");

            DSApi.openAdmin();

            UlBudgetPosition bp = finder.find(id.longValue());

            Integer fundSource = bp.getCFundSource();
            Integer project = bp.getCProject();
            Integer budget = bp.getCBudget();
            Integer position = bp.getCPosition();
            Integer resource = bp.getCResource();
            Integer costKind = bp.getIKind();
            Integer costGroup = bp.getIGroup();
            Integer costGroupType = bp.getIType();
            Integer mpk = bp.getCMpk();

            UlBudgetPosition newBp;
            List<UlBudgetPosition> newPositions = Lists.newArrayList();
            int iNo = 0;
            for (CostToProductMapping cost : COST_TO_PRODUCT_MAPPINGS) {
                BigDecimal amount = BigDecimal.ZERO;

                if (cost.isEnumMultipleCondition()) {
                    FieldData dictionaryField = values.get("DWR_" + cost.getDictCn());

                    if (dictionaryField != null && dictionaryField.getType().equals(pl.compan.docusafe.core.dockinds.dwr.Field.Type.DICTIONARY)) {
                        Map<String,FieldData> dictionary = dictionaryField.getDictionaryData();

                        String dictFieldCn = cost.getDictCn() + "_" + cost.getFieldEnumCn();
                        String moneyFieldCn = cost.getDictCn() + "_" + cost.getAmountFieldCn();
                        int i = 1;
                        do {
                            String processedField = dictFieldCn + "_" + i;
                            String processedMoneyField = moneyFieldCn + "_" + i;
                            if (!dictionary.containsKey(processedField) || !dictionary.containsKey(processedMoneyField)) {
                                break;
                            }

                            FieldData fieldData = dictionary.get(processedField);
                            if (fieldData != null && fieldData.getData() != null) {
                                if (cost.getFieldEnumIds().contains(fieldData.getEnumValuesData().getSelectedId()) && dictionary.get(processedMoneyField).getMoneyData() != null) {
                                     amount = amount.add(dictionary.get(processedMoneyField).getMoneyData());
                                }
                            }
                            i++;
                        } while (true);
                    }
                } else {
                    FieldData field = values.get(cost.getAmountFieldCn());
                    if (field != null && field.getType().equals(Field.Type.MONEY) && field.getMoneyData() != null) {
                        amount = field.getMoneyData();
                    }
                }

                if (BigDecimal.ZERO.compareTo(amount) != 0) {
                    newPositions.add(newBp = new UlBudgetPosition());

                    newBp.setCType(cost.getProductId(costType).intValue());
                    newBp.setIAmount(amount);

                    newBp.setCProject(project);
                    newBp.setCBudget(budget);
                    newBp.setCPosition(position);
                    newBp.setCResource(resource);
                    newBp.setCFundSource(fundSource);
                    newBp.setIGroup(costGroup);
                    newBp.setIKind(costKind);
                    newBp.setIType(costGroupType);
                    newBp.setCMpk(mpk);

                    while (lps.contains(++iNo));
                    newBp.setINo(iNo);
                }
            }

            if (!newPositions.isEmpty()) {
                DSApi.context().begin();
                txOpened = true;

                for (UlBudgetPosition newBpToSave : newPositions) {
                    newBpToSave.save();
                    ids.add(newBpToSave.getId().toString());
                }

                DSApi.context().commit();
                //values.put("DWR_" + BP_FIELD, new FieldData(Field.Type.STRING, Joiner.on(",").join(ids)));
                values.put("DWR_" + GENERUJ_POZYCJE_ID, new FieldData(Field.Type.STRING, Joiner.on(",").join(ids)));
            }
        } catch (Exception e) {
            try {
                if (txOpened) {
                    DSApi.context().rollback();
                }
            } catch (EdmException e1) {
                log.error(e1.getMessage(), e1);
            }
            log.error(e.getMessage(), e);
        } finally {
            try {
                DSApi.close();
            } catch (EdmException e1) {
                log.error(e1.getMessage(), e1);
            }
        }
    }

    private Integer getBaseCostTypeId(Map<String, FieldData> values) {
        FieldData kind = values.get("DWR_"+DELEGOWANY_WYBOR);
        if (kind != null) {
            if ("1".equals(kind.getEnumValuesData().getSelectedId()) || "2".equals(kind.getEnumValuesData().getSelectedId())) {
                return 1;
            }
            if ("3".equals(kind.getEnumValuesData().getSelectedId())) {
                FieldData otherKindType = values.get("DWR_"+DELEGOWANY_INNY);
                if (otherKindType != null) {
                    if ("1".equals(otherKindType.getEnumValuesData().getSelectedId()) || "2".equals(otherKindType.getEnumValuesData().getSelectedId())) {
                        return 2;
                    }
                    if ("3".equals(otherKindType.getEnumValuesData().getSelectedId())) {
                        return 3;
                    }
                }
            }
        }

        return 1;
    }

    static class CostToProductMapping {
        private String amountFieldCn;
        private String fieldEnumCn;
        private String dictCn;
        private Set<String> fieldEnumIds;
        private Map<Integer, String> products;

        CostToProductMapping(String amountFieldCn) {
            this.amountFieldCn = amountFieldCn;
            products = Maps.newHashMap();
        }

        CostToProductMapping(String dictCn, String amountFieldCn, String fieldEnumCn, String... enumIds) {
            this.dictCn = dictCn;
            this.amountFieldCn = amountFieldCn;
            this.fieldEnumCn = fieldEnumCn;
            fieldEnumIds = Sets.newHashSet(enumIds);
            products = Maps.newHashMap();
        }

        CostToProductMapping addProduct(Integer type, String productCn) {
            products.put(type, productCn);
            return this;
        }

        boolean isEnumMultipleCondition() {
            return amountFieldCn != null && fieldEnumIds != null;
        }

        public String getDictCn() {
            return dictCn;
        }

        public String getAmountFieldCn() {
            return amountFieldCn;
        }

        public String getFieldEnumCn() {
            return fieldEnumCn;
        }

        public Set<String> getFieldEnumIds() {
            return fieldEnumIds;
        }

        public Map<Integer, String> getProducts() {
            return products;
        }

        Long getProductId(int type) throws EdmException {
            List<Product> found = DictionaryUtils.findByGivenFieldValue(Product.class, "produktIdn", products.get(type));

            Preconditions.checkState(found.size() == 1, "wrong product");

            return found.get(0).getId();
        }
    }

    private void setLoanField(Map<String, FieldData> values) {
        if (DwrUtils.isNotNull(values, "DWR_ZALICZKA", DWR_ZALICZKA_KWOTA) && values.get("DWR_POBRANA_ZALICZKA") != null) {
            values.get("DWR_POBRANA_ZALICZKA").setMoneyData(values.get(DWR_ZALICZKA_KWOTA).getMoneyData());
        }
    }

    private void sumTripCosts(Map<String, FieldData> values) {
        if (values.get("DWR_SUMA_KOSZTOW_PODROZY_COST_TO_SUM") != null) {
            if (values.get(DOJAZDY_UDOKUMENTOWANE_DWR) != null) {
                values.get(DOJAZDY_UDOKUMENTOWANE_DWR).setMoneyData(DwrUtils.sumDictionaryMoneyFields(values, KOSZTY_PODROZY, "KOSZT"));
            }
            values.get("DWR_SUMA_KOSZTOW_PODROZY_COST_TO_SUM").setMoneyData(DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.ADD, 2, true, values, DOJAZDY_UDOKUMENTOWANE_DWR, RYCZALT_ZA_DOJAZDY_DWR));
        }
    }

    private void setPayOrReturnFields(Map<String, FieldData> values) {
        BigDecimal sum = DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.SUBTRACT, 2, true, values, "DWR_SUMA", "DWR_POBRANA_ZALICZKA");
        if (values.get("DWR_DO_ZWROTU") != null && values.get("DWR_DO_WYPLATY") != null) {
            if (sum.compareTo(BigDecimal.ZERO) == -1) {
                values.get("DWR_DO_ZWROTU").setMoneyData(sum.abs().setScale(2, RoundingMode.HALF_UP));
                values.get("DWR_DO_WYPLATY").setMoneyData(BigDecimal.ZERO);
            } else {
                values.get("DWR_DO_WYPLATY").setMoneyData(sum.abs().setScale(2, RoundingMode.HALF_UP));
                values.get("DWR_DO_ZWROTU").setMoneyData(BigDecimal.ZERO);
            }
        }
    }

    private void sumDelegationCosts(Map<String, FieldData> values) {
        BigDecimal spend = DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.ADD, 2, true, values,
                "DWR_SUMA_KOSZTOW_PODROZY_COST_TO_SUM",
                NOCLEGI_RYCZALT_DWR,
                NOCLEGI_WG_RACHUNKOW_DWR,
                INNE_WYDATKI_DWR,
                DIETA_PLN_DWR);
        if (values.get("DWR_SUMA") != null) {
            values.get("DWR_SUMA").setMoneyData(spend);
        }
    }

    private void evaluateLocalDiet(Map<String, FieldData> values) {
        try {
            if (DwrUtils.isSenderField(values, "DWR_KOSZTY_PODROZY", "DWR_DIETA_KRAJOWA")) {
                Date from = new Date();
                Date to = from;
                List<Date> datesFrom = DwrUtils.getValueListFromDictionary(values, "KOSZTY_PODROZY", true, "FROM_DATE");
                List<Date> datesTo = DwrUtils.getValueListFromDictionary(values, "KOSZTY_PODROZY", true, "TO_DATE");
                if (!datesFrom.isEmpty() && !datesTo.isEmpty()) {
                    from = Collections.min(datesFrom);
                    to = Collections.max(datesTo);
                }
    //                from = values.get("DWR_TERMIN_WYJAZDU").getDateData();
    //                to = values.get("DWR_TERMIN_POWROTU").getDateData();

                Date diff = new Date(to.getTime() - from.getTime());

                BigDecimal baseDiets = new BigDecimal(evaluateDietAmount(diff));


                BigDecimal mealDiets = calcDietMealRates(values);

                BigDecimal diets = baseDiets.subtract(mealDiets);
                values.get("DWR_DIETA_KRAJOWA_ILOSC").setMoneyData(diets.compareTo(BigDecimal.ZERO) == 1 ? diets : BigDecimal.ZERO);
            }
            BigDecimal localDietMoneyRate = getValueFromDelgationCostSystemDic(PL_DIET_RATE_ID, 30);
            values.get(DIETA_PLN_DWR).setMoneyData(values.get("DWR_DIETA_KRAJOWA_ILOSC").getMoneyData().multiply(localDietMoneyRate).setScale(2, RoundingMode.HALF_UP));

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private BigDecimal calcDietMealRates(Map<String, FieldData> values) {
        FieldData breakfasts = (FieldData) DwrUtils.getDictionaryValue(values, "DIETA_KRAJOWA", "SNIADANIE");
        FieldData middayMeals = (FieldData) DwrUtils.getDictionaryValue(values, "DIETA_KRAJOWA", "OBIAD");
        FieldData dinners = (FieldData) DwrUtils.getDictionaryValue(values, "DIETA_KRAJOWA", "KOLACJA");
        BigDecimal dietMealRates = new BigDecimal(0);
        BigDecimal breakFastRate = new BigDecimal(25);
        BigDecimal middayMealRate = new BigDecimal(50);
        BigDecimal dinnerRate = new BigDecimal(25);

        if (breakfasts != null && breakfasts.getIntegerData() != null) {
            dietMealRates = dietMealRates.add(breakFastRate.multiply(new BigDecimal(breakfasts.getIntegerData())));
        }
        if (middayMeals != null && middayMeals.getIntegerData() != null) {
            dietMealRates = dietMealRates.add(middayMealRate.multiply(new BigDecimal(middayMeals.getIntegerData())));
        }
        if (dinners != null && dinners.getIntegerData() != null) {
            dietMealRates = dietMealRates.add(dinnerRate.multiply(new BigDecimal(dinners.getIntegerData())));
        }

        return dietMealRates.movePointLeft(2);
    }

    private void calculateDelegationCost(Map<String, FieldData> values, String fieldCnIlosc, int typeOfDelegationCost, String fieldCnSum, int defaultValue) {
        if (DwrUtils.isNotNull(values, fieldCnIlosc)) {
            BigDecimal ilosc = values.get(fieldCnIlosc).getMoneyData();

            BigDecimal koszt = getValueFromDelgationCostSystemDic(typeOfDelegationCost, defaultValue);
            koszt = ilosc.multiply(koszt);
            values.get(fieldCnSum).setMoneyData(koszt);
        }
    }

    @Override
    public void setAdditionalTemplateValues(long docId, Map<String, Object> values, Map<Class, Format> defaultFormatters) {
        try {
            Document doc = Document.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            fm.initialize();

            DelegationTemplateHelper th = new DelegationTemplateHelper(fm, (OfficeDocument) doc, log);
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setDecimalSeparator(',');
            defaultFormatters.put(BigDecimal.class, new DecimalFormat("### ### ##0.00", symbols));
            defaultFormatters.put(Date.class, new SimpleDateFormat("dd-MM-yyyy"));


            values.put("DELEGOWANY", th.getDelegate());
            values.put(GENERATE_DATE_TEMPLATE_VAR, th.getDocTemplateCreateDate());
            values.put(DOC_DATE_TEMPLATE_VAR, th.getDocCreateDate());
            values.put("ACC", th.getAcceptInfo());
            values.put("KP", th.getPodrozItems());
            values.put("PROJEKT", th.getProjects());
            values.put("ZF", th.getFundSources());
            values.put("MPK", th.getMpks());
            values.put("SUMA", th.getAmountExtended("SUMA"));
            values.put("ZALICZKA_KWOTA", th.getAmountExtended("ZALICZKA_KWOTA"));
            values.put("DO_WYPLATY", th.getAmountExtended("DO_WYPLATY"));
            values.put("BP_DESCRIPTION", th.getBudgetPositionDescription(BP_ROZ_FIELD));

        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }

}
