package pl.compan.docusafe.parametrization.ul;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.google.common.primitives.Ints;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.field.DSUserEnumField;
import pl.compan.docusafe.core.users.*;
import pl.compan.docusafe.core.users.ad.ActiveDirectoryManager;
import pl.compan.docusafe.service.*;
import pl.compan.docusafe.service.mail.MailerDriver;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.Control;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.PagedResultsControl;
import javax.naming.ldap.PagedResultsResponseControl;
import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 *
 */
public class LdapUserImport extends ServiceDriver implements Service
{
	public static final Logger log = LoggerFactory.getLogger(LdapUserImport.class);
    public static final String GET_SUPERVISOR_ID_SQL = "select z2.id_pracownika from zatrudnienia z left outer join zatrudnienia z2 on z.id_przelozonego=z2.id where z.id_pracownika=?";

    private Timer timer;
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(MailerDriver.class.getPackage().getName(),null);
	private Property[] properties;
	private Long period = 240L;
	private String filter = "";
	private String emailAttrName = "mail";
	private String hostname;
	private String port;
	private String dbName;
	private String username;
	private String password;
    private String divisionCodesGetBasicStatement;
    private String divisionCodesGetStatementWithAdCondition;


    protected boolean canStop()
	{
		return true;
	}

	protected void start() throws ServiceException
	{
		log.info("start uslugi LdapUserImport");
		timer = new Timer(true);
		timer.schedule(new Import(), (long) 2*1000 ,(long) period*DateUtils.MINUTE);
		console(Console.INFO, sm.getString("StartUslugiLdapUserImport"));
	}

	protected void stop() throws ServiceException
	{
		log.info("stop uslugi");
		console(Console.INFO, sm.getString("StopUslugi"));
		if(timer != null) timer.cancel();
	}

	public boolean hasConsole()
	{
		return true;
	}
	
	private static Connection connect(String hostname, String port, String dbName, String username, String password) throws Exception
	{
        return DriverManager.getConnection(
		   "jdbc:postgresql://"+hostname+":"+port+"/"+dbName,username,password);
	}
	class Import extends TimerTask
	{
		public void run()
		{
            setUserToDivisionsSqls();

            ActiveDirectoryManager ad = null;
			LdapContext context = null;
			Connection conn= null;
			try
			{
				console(Console.INFO, "Start");
				console(Console.INFO, "Tworz� po��czenie z BD");

                try {
                    conn = connect(hostname, port, dbName, username, password);
                } catch (Exception e) {
                    console(Console.ERROR, "B��d: "+e.getMessage());
                    log.error(e.getMessage(),e);
                }

                DSApi.openAdmin();
				console(Console.INFO, "Laczy z AD");
				ad = new ActiveDirectoryManager();
				context = ad.LdapConnectAsAdminInLdapContext();
				console(Console.INFO, "Polaczyl sie z AD");			
				int pageSize = 100;
			    byte[] cookie = null; 
				context.setRequestControls(new Control[]{new javax.naming.ldap.PagedResultsControl(pageSize, Control.CRITICAL)});				
				String tmpfilter = "(&(objectClass=user)(objectClass=person)(objectClass=organizationalPerson))";
				if(!StringUtils.isEmpty(filter))
				{
					tmpfilter = filter;
				}
				console(Console.INFO, "Szuka uzytkownikow " + tmpfilter);
				log.error("Szuka uzytkownikow " + tmpfilter);
				int j = 0;
				while(true)
				{
					SearchControls ctls = new SearchControls();
					ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);
					ctls.setCountLimit(0);
					NamingEnumeration<SearchResult> results = context.search("",tmpfilter, ctls);
					while (results != null && results.hasMore()) 
					{
						try
						{
							j++;
							log.debug("Next "+j);
							console(Console.INFO, "Next "+j);
							DSApi.context().begin();
							SearchResult entry = (SearchResult)results.next();
							DSUser user = getUserFromAd(entry);
							DSApi.context().session().save(user);
							DSApi.context().commit();

                            if (conn != null && !user.isDeleted()) {
                                DSUser supervisor = getSupervisor(user, conn);
                                if (supervisor != null) {
                                    DSApi.context().begin();
                                    user.setSupervisor(supervisor);
                                    DSApi.context().commit();
                                }
                                Collection<String> divisionCodes = getUserPropertyFromDB(user, Long.parseLong(user.getExtension()), conn);
                                if (divisionCodes != null) {
                                    DSApi.context().begin();
                                    addInDivisions(user, divisionCodes);
                                    removeFromInvalidDivisions(user, divisionCodes);
                                    DSApi.context().commit();
                                }
                            }
                        }
						catch (Exception e) 
						{
							LdapUserImport.log.error("",e);
							console(Console.ERROR, "B��d [ "+e+" ]");
							DSApi.context()._rollback();
						}
						if(j % 100 == 0)
						{
							log.error("Wyczyszczenie sesji na idx: " + j);
							DSApi.context().session().flush();
							DSApi.context().session().clear();
							DSApi.close();
							DSApi.openAdmin();
						}
					}
					Control[] controls = context.getResponseControls();
					if(controls!=null)
					{
						for(int k = 0; k<controls.length; k++)
						{
							if(controls[k] instanceof PagedResultsResponseControl)
							{
								PagedResultsResponseControl prrc = (PagedResultsResponseControl)controls[k];
								prrc.getResultSize();
								cookie = prrc.getCookie();
						    }
							else
							{
								throw new EdmException("Niespodziewany ResponseControls");
						    }
						}
					}
					if(cookie==null)
						break;
					context.setRequestControls(new Control[]{new PagedResultsControl(pageSize, cookie, Control.CRITICAL)});
				}
			}
			catch(Exception e)
			{
				LdapUserImport.log.error("",e);
				console(Console.ERROR, "B��d [ "+e+" ]");
			}
			finally
			{
				
				log.trace("Import  STOP");
				console(Console.INFO, "Koniec synchronizacji");
				if(context != null)
					try {
						context.close();
					} catch (NamingException e) {
						log.error(e.getMessage(),e);
					}
				DbUtils.closeQuietly(conn);
				DSApi._close();
			}
		}

        private void setUserToDivisionsSqls() {
            if (Strings.isNullOrEmpty(Docusafe.getAdditionProperty("import.users_to_divisions.get_division_codes_sql"))) {
                divisionCodesGetBasicStatement = "select j.kod from pracownicy_jednostki_eod pj join jednostki_eod j on pj.id_jednostki_eod=j.id where pj.id_pracownika=?";
            } else {
                divisionCodesGetBasicStatement = Docusafe.getAdditionProperty("import.users_to_divisions.get_division_codes_sql");
            }

            if (Strings.isNullOrEmpty(Docusafe.getAdditionProperty("import.users_to_divisions.get_division_codes_sql_with_ad_condition"))) {
                divisionCodesGetStatementWithAdCondition = "select j.kod from pracownicy_jednostki_eod pj join jednostki_eod j on pj.id_jednostki_eod=j.id where pj.id_pracownika=? and (j.ad=? or j.ad is null)";
            } else {
                divisionCodesGetStatementWithAdCondition = Docusafe.getAdditionProperty("import.users_to_divisions.get_division_codes_sql_with_ad_condition");
            }
        }
    }

    private DSUser getSupervisor(DSUser user, Connection conn) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String extensionRaw = user.getExtension();
        if (extensionRaw != null && Ints.tryParse(extensionRaw) != null) {
            try {
                ps = conn.prepareStatement(GET_SUPERVISOR_ID_SQL);
                ps.setInt(1, Ints.tryParse(extensionRaw));

                rs = ps.executeQuery();
                while (rs.next()) {
                    String supervisorId = rs.getString(1);
                    if (!Strings.isNullOrEmpty(supervisorId)) {
                        try {
                            return DSUser.findAllByExtension(supervisorId);
                        } catch (UserNotFoundException e) {
                            log.error(e.getMessage(), e);
                            console(Console.ERROR, "Nie znaleziono przelozonego id: " + supervisorId + ", dla uzytkownika:" + user.getName());
                        }
                    }
                }

            } catch (Exception exc) {
                log.error(exc.getMessage(), exc);
                console(Console.ERROR, "Blad podczas ustawiania przelozonego dla uzytkownika:"+user.getName());
            } finally {
                DbUtils.closeQuietly(ps);
                DbUtils.closeQuietly(rs);
            }
        } else {
            log.error("Brak id pracownika");
            console(Console.ERROR, "Brak id pracownika");
        }

        return null;
    }

    private String getDomainFromDivision(SearchResult element) {
        String name = element.getName();

        //name = OU=Dzia� Studi�w i Analiz,OU=Pion bezpo�rednio podleg�y Rektorowi,OU=Administracja,DC=adm
        Set<String> invalidDomains = Sets.newHashSet("uni","lodz","pl");

        for (String nameAttr : Splitter.on(',').split(name)) {
             if (nameAttr.startsWith("DC")) {
                 String[] domainAttrs = nameAttr.split("=");
                 if (domainAttrs.length>1 && !invalidDomains.contains(domainAttrs[1])) {
                     return domainAttrs[1];
                 }
             }
        }
        return null;
    }


	private DSUser getUserFromAd(SearchResult element) throws Exception
	{
		DSUser user = null;				
		String nameExternal = getStringAttr(element,"userPrincipalName");
		log.debug(" nameext "+ nameExternal +" ");
		String name = null;
		if(nameExternal.contains("@"))
			name  = nameExternal.substring(0,nameExternal.indexOf("@")).replaceAll("\\.","");
		else
			name  = nameExternal.replaceAll("\\.","");

        name = normalizeUsername(name);

        String firstname = getStringAttr(element,"givenName");
        String lastname = getStringAttr(element,"sn");
        String email =  getStringAttr(element,"mail");
        String kodJednostki = getStringAttr(element, "jednostkaKod");
        String idpracownika = getStringAttr(element,"pracownikIdentyfikator");

        Boolean disabled = isDisabledAccount(element);
        if (disabled == null) {
            console(Console.INFO, "Nie mo�na odczyta� statusu wa�no�ci konta");
            log.error("Nie mo�na odczyta� statusu wa�no�ci konta, name " + name + " nameext " + nameExternal + " ");
            disabled = false;
        }

        if(StringUtils.isBlank(idpracownika))
        	throw new EdmException("Brak idPracownika");
        /**Jesli nie ma to uzywamy userPrincipalName*/
        if(StringUtils.isEmpty(email))
        {
        	email = getStringAttr(element,"userPrincipalName");
        }
        log.debug("name " + name +" nameext "+ nameExternal +" ");
		if(name != null && firstname != null && lastname != null)
		{
			console(Console.INFO, "Szuka uzytkownika");
			try
			{
				String orginalName = name;
			
				for (int i = 1; i < 10; i++)
				{
					user = DSUser.findByExternalName(nameExternal);
					if(!user.getName().equalsIgnoreCase(name))
					{
						console(Console.INFO, "Znalazl ten sam nameExternal ale inny name "+ name);
						log.error("name " + name +" nameext "+ nameExternal +" ");
					}
					else
						break;
					name  = orginalName+i;
				}
				
				console(Console.INFO, "Znalazl "+ user.asFirstnameLastname());
                if (disabled && !user.isDeleted()) {
                    log.error("Usuwam konto "+ user.getName());
                    UserFactory.getInstance().deleteUser(user.getName());
                    DSUserEnumField.reloadForAll();
                } else if (!disabled) {
                    if (user.isDeleted()) {
                        UserFactory.getInstance().revertUser(user.getName());
                    }
                    user.setLoginDisabled(disabled);
                    user.setFirstname(firstname);
                    user.setExternalName(nameExternal);
                    user.setLastname(lastname);
                    user.setEmail(email);
                    user.setExtension(idpracownika);
                }
				user.update();
			}
			catch (UserNotFoundException e)
			{
				if (!disabled) {
                    try
                    {
                        String orginalName = name;
                        for (int i = 1; i < 10; i++)
                        {
                            user = DSUser.findByUsername(name);
                            name  = orginalName+i;
                        }
                        throw new EdmException("Nie ustalil name");
                    }
                    catch (UserNotFoundException e2)
                    {
                        console(Console.INFO, "Nie znalazl dodaje "+name);
                        user = UserFactory.getInstance().createUser(name, firstname, lastname);
                        user.setEmail(email);
                        user.setCtime(new Timestamp(new Date().getTime()));
                        user.setAdUser(true);
                        user.setExtension(idpracownika);
                        user.setExternalName(nameExternal);
                        console(Console.INFO, "Dodal "+user.asFirstnameLastname());
                        log.error("Dodal "+user.asFirstnameLastname());
                    }
                }
			}	
		}
		else
		{
			throw new EdmException("Brak podstawowych parametr�w "+name+","+firstname+","+lastname+","+email);
		}
		//addInDivision(user, kodJednostki);
		return user;
	}

    /**
     * Usuwa ogonki z znak�w diakratycznych, nast�pnie wycina wszystkie znaki, kt�re nie s� [A-z] lub [0-9].
     * Wynik jest konwertowany do ma�ych liter.
     * @param name
     * @return
     */
    private String normalizeUsername(String name) {
        name = TextUtils.normalizeString(name);
        name = CharMatcher.JAVA_LETTER_OR_DIGIT.retainFrom(name);
        return name.toLowerCase(Locale.ENGLISH);
    }


    private void addInDivision(DSUser user,String kodjednostki)
	{
		PreparedStatement ps = null;
		ResultSet rs= null;
		try
		{
			DSDivision division = null;
			try
			{
				division= DSDivision.findByCode(kodjednostki);
			}
			catch (DivisionNotFoundException dnfe)
			{
				console(Console.INFO, "Nie znaleziono dzia�u kod = "+kodjednostki);
				log.error("Nie znaleziono dzia�u kod = "+kodjednostki);
			}
			if(division != null)
			{
				LdapUserImport.log.debug("Znalazl dzia�u "+ kodjednostki + " dodaje "+user.asFirstnameLastname());
				console(Console.INFO, "Znalazl dzia�u "+ kodjednostki + " dodaje "+user.asFirstnameLastname());
				division.addUser(user,"AD");
			}
			else
			{
				LdapUserImport.log.debug("nie znalazl dzia�u "+ kodjednostki);
				console(Console.INFO, "Nie znalazl dzialu  "+ kodjednostki);
			}
//			String[] sources = division.getUserSources(user);
//			for (String string : sources) {
//				System.out.println(string);
//			}
			if(division != null)
				for(DSDivision div :user.getDivisions())
				{
					if(div.getExternalId()!= null && div.getExternalId().length() > 1 && !kodjednostki.equalsIgnoreCase(div.getCode()))
					{
//						log.error("Division "+ div.getName()+ " getCode "+ div.getCode());
//						for (String string : div.getUserSources(user)) {
//							log.error(string);
//						}
						if(Arrays.asList(div.getUserSources(user)).contains("AD"))
						{
							log.error("USUWA Z DZIA�U "+div.getName()+" "+div.getCode());
							for (String string : div.getUserSources(user)) {
								log.error(string);
							}
							div.removeUser(user,"AD");
						}
					}
				}
			
		}
		catch (Exception exc)
		{
			log.error(exc.getMessage(), exc);
		}
		finally
		{
			DbUtils.closeQuietly(ps);
			DbUtils.closeQuietly(rs);
		}
		
	}
	
	
	private void updateUserPropertyFromDB(DSUser user,Long idpracownika,Connection conn)
	{
		PreparedStatement ps = null;
		ResultSet rs= null;
		try
		{
			LdapUserImport.log.debug("Szuka danych w bazie "+ idpracownika);
			console(Console.INFO, "Szuka danych w bazie dla pracownika id= "+ idpracownika);
			String kodjednostki = null;
			ps = conn.prepareStatement("select p.imie, p.nazwisko , z.kod_jednostki_ul from pracownicy p, ZATRUDNIENIA  z where p.id = z.id_pracownika and p.id = ?");
			ps.setLong(1,idpracownika );
			rs = ps.executeQuery();
			while(rs.next())
			{
				LdapUserImport.log.debug("Znalazl "+ idpracownika);
				console(Console.INFO, "Znalazl dla pracownika id= "+ idpracownika);
				user.setFirstname(rs.getString(1));
				user.setLastname(rs.getString(2));
				kodjednostki = rs.getString(3);
				LdapUserImport.log.debug("KOD dzia�u "+ kodjednostki);
				console(Console.INFO, "KOD dzia�u  "+ kodjednostki);
				DSDivision division = null;
				try
				{
					division= DSDivision.findByCode(kodjednostki);
				}
				catch (DivisionNotFoundException dnfe)
				{
					console(Console.INFO, "Nie znaleziono dzia�u kod = "+kodjednostki);
					log.error("Nie znaleziono dzia�u kod = "+kodjednostki);
				}
				if(division != null)
				{
					LdapUserImport.log.debug("Znalazl dzia�u "+ kodjednostki + " dodaje "+user.asFirstnameLastname());
					console(Console.INFO, "Znalazl dzia�u "+ kodjednostki + " dodaje "+user.asFirstnameLastname());
					division.addUser(user);
				}
				else
				{
					LdapUserImport.log.debug("nie znalazl dzia�u "+ kodjednostki);
					console(Console.INFO, "Nie znalazl dzialu  "+ kodjednostki);
				}
				
			}
		}
		catch (Exception exc)
		{
			log.error(exc.getMessage(), exc);
		}
		finally
		{
			DbUtils.closeQuietly(ps);
			DbUtils.closeQuietly(rs);
		}
		
	}

    private int addInDivisions(DSUser user, Collection<String> divisionCodes) {
        int noOfAdd = 0;
        for (String divCode : divisionCodes) {
            try {
                DSDivision division = null;
                try {
                    division = DSDivision.findByCode(divCode);
                } catch (DivisionNotFoundException dnfe) {
                    console(Console.INFO, "Nie znaleziono dzia�u kod = " + divCode);
                    log.error("Nie znaleziono dzia�u kod = " + divCode);
                }
                if (division != null) {
                    LdapUserImport.log.debug("Znalazl dzia�u " + divCode + " dodaje " + user.asFirstnameLastname());
                    console(Console.INFO, "Znalazl dzia�u " + divCode + " dodaje " + user.asFirstnameLastname());
                    division.addUser(user, "AD");
                    noOfAdd++;
                } else {
                    LdapUserImport.log.debug("nie znalazl dzia�u " + divCode);
                    console(Console.INFO, "Nie znalazl dzialu  " + divCode);
                }
            } catch (Exception exc) {
                log.error(exc.getMessage(), exc);
            }
        }
        return noOfAdd;
    }

    private int removeFromInvalidDivisions(DSUser user, Collection<String> divisionCodes) {
        int noOfRemoved = 0;
        try {
            for (DSDivision div : user.getDivisions()) {
                String[] userSources = div.getUserSources(user);
                if (userSources != null) {
                    Collection<String> sources = Arrays.asList(userSources);
                    if (!divisionCodes.contains(div.getCode()) && sources.contains("AD")) {
                        log.error("USUWA Z DZIA�U " + div.getName() + " " + div.getCode());
                        for (String string : div.getUserSources(user)) {
                            log.error(string);
                        }
                        div.removeUser(user, "AD");
                        noOfRemoved++;
                    }
                } else {
                    log.error("userSources=null login:"+user.getName()+" div: "+div.getGuid());
                }
            }
        } catch (Exception exc) {
            log.error(exc.getMessage(), exc);
        }
        return noOfRemoved;
    }


    private Collection<String> getUserPropertyFromDB(DSUser user, Long idpracownika, Connection conn) {
        Set<String> divisionCodes = Sets.newLinkedHashSet();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            LdapUserImport.log.debug("Szuka danych w bazie " + idpracownika);
            console(Console.INFO, "Szuka danych w bazie dla pracownika id= " + idpracownika);
            String kodjednostki = null;
/*
            select j.kod from pracownicy_jednostki_eod pj
            join jednostki_eod j on pj.id_jednostki_eod=j.id
            where pj.id_pracownika=832188 and j.ad='biol.uni.lodz.pl'
                */
            String domainCode = null;

            try {
                domainCode = user.getExternalName().split("@")[1];
            } catch (Exception e) {
                console(Console.ERROR, "Nie znaleziono domeny dla u�ytkownika: " + idpracownika + ", principalName: " + user.getExternalName());
                log.error("Nie znaleziono domeny dla u�ytkownika: " + idpracownika + ", principalName: " + user.getExternalName());
            }

            if (Strings.isNullOrEmpty(domainCode)) {
                ps = conn.prepareStatement(divisionCodesGetBasicStatement);
            } else {
                ps = conn.prepareStatement(divisionCodesGetStatementWithAdCondition);
                ps.setString(2, domainCode);
            }
            ps.setLong(1, idpracownika);

            rs = ps.executeQuery();
            while (rs.next()) {
                LdapUserImport.log.debug("Znalazl " + idpracownika);
                console(Console.INFO, "Znalazl dla pracownika id= " + idpracownika);
                kodjednostki = rs.getString(1);
                LdapUserImport.log.debug("KOD dzia�u " + kodjednostki);
                console(Console.INFO, "KOD dzia�u  " + kodjednostki);

                divisionCodes.add(kodjednostki);
            }

        } catch (Exception exc) {
            log.error(exc.getMessage(), exc);
            console(Console.ERROR, "Pracownika id= " + idpracownika+", "+exc.getMessage());

            return null;
        } finally {
            DbUtils.closeQuietly(ps);
            DbUtils.closeQuietly(rs);
        }
        return divisionCodes;
    }

    private String getStringAttr(SearchResult element,String name) throws NamingException
	{
		if(element.getAttributes().get(name) != null)
		{
			return (String) element.getAttributes().get(name) .get();
		}
		return null;
	}

    private Boolean isDisabledAccount(SearchResult element) throws NamingException {
        Attribute userAccountControl = element.getAttributes().get("userAccountControl");
        if (userAccountControl != null && userAccountControl.get() != null) {
            int mixedValue = Integer.valueOf(userAccountControl.get().toString());

            return ((mixedValue >> 1) & 1) == 1;
        }
        return null;
    }

	public Property[] getProperties() {
		return properties;
	}

	public void setProperties(Property[] properties) {
		this.properties = properties;
	}
	
	 class LdapTimesProperty extends Property
	    {
	        public LdapTimesProperty()
	        {
	            super(SIMPLE, PERSISTENT, LdapUserImport.this, "period", "Czestotliwo�c synchronizacji w minutach", String.class);
	        }

	        protected Object getValueSpi()
	        {
	            return period;
	        } 

	        protected void setValueSpi(Object object) throws ServiceException
	        {
	            synchronized (LdapUserImport.this)
	            {
	            	if(object != null)
	            	{
	            		period = Long.parseLong((String) object);
	            	}
	            	else
	            	{
	            		period = 240L;
	            	}
	            
	            }
	        }
	    }
	    
	    class MailAttrNameProperty extends Property{
	        public MailAttrNameProperty(){
	            super(SIMPLE, PERSISTENT, LdapUserImport.this, "emailAttrName", "emailAttrName", String.class);
	        }
	        protected Object getValueSpi(){
	            return emailAttrName;
	        } 
	        protected void setValueSpi(Object object) throws ServiceException{
	            synchronized (LdapUserImport.this){
	            	if(object == null)
	            		emailAttrName = "mail";
	            	else
	            		emailAttrName = (String) object;
	            }
	        }
	    }
	    
	    class PortProperty extends Property
		{
			public PortProperty()
			{
				super(SIMPLE, PERSISTENT, LdapUserImport.this, "port", "port", String.class);
			}

			protected Object getValueSpi()
			{
				return port;
			}

			protected void setValueSpi(Object object) throws ServiceException
			{
				synchronized (LdapUserImport.this)
				{
					if (object != null)
						port = (String) object;
					else
						port = "";
				}
			}
		}
		
		class DbNameProperty extends Property
		{
			public DbNameProperty()
			{
				super(SIMPLE, PERSISTENT, LdapUserImport.this, "dbName", "Baza", String.class);
			}

			protected Object getValueSpi()
			{
				return dbName;
			}

			protected void setValueSpi(Object object) throws ServiceException
			{
				synchronized (LdapUserImport.this)
				{
					if (object != null)
						dbName = (String) object;
					else
						dbName = "";
				}
			}
		}
		
		class UsernameProperty extends Property
		{
			public UsernameProperty()
			{
				super(SIMPLE, PERSISTENT, LdapUserImport.this, "username", "Uzytkownik", String.class);
			}

			protected Object getValueSpi()
			{
				return username;
			}

			protected void setValueSpi(Object object) throws ServiceException
			{
				synchronized (LdapUserImport.this)
				{
					if (object != null)
						username = (String) object;
					else
						username = "eod";
				}
			}
		}
		
		class HostnameProperty extends Property
		{
			public HostnameProperty()
			{
				super(SIMPLE, PERSISTENT, LdapUserImport.this, "hostname", "Adres serwera", String.class);
			}

			protected Object getValueSpi()
			{
				return hostname;
			}

			protected void setValueSpi(Object object) throws ServiceException
			{
				synchronized (LdapUserImport.this)
				{
					if (object != null)
						hostname = (String) object;
					else
						hostname = "";
				}
			}
		}
		
		class PasswordProperty extends Property
		{
			public PasswordProperty()
			{
				super(SIMPLE, PERSISTENT, LdapUserImport.this, "password", "Has�o", String.class);
			}

			protected Object getValueSpi()
			{
				return password;
			}

			protected void setValueSpi(Object object) throws ServiceException
			{
				synchronized (LdapUserImport.this)
				{
					if (object != null)
						password = (String) object;
					else
						password = "";
				}
			}
		}
	    
	    class FilterProperty extends Property
	    {
	        public FilterProperty()
	        {
	            super(SIMPLE, PERSISTENT, LdapUserImport.this, "filter", "Filtr", String.class);
	        }

	        protected Object getValueSpi()
	        {
	            return filter;
	        } 

	        protected void setValueSpi(Object object) throws ServiceException
	        {
	            synchronized (LdapUserImport.this)
	            {
	            	filter = (String) object;
	            }
	        }
	    }


		public LdapUserImport()
		{
			properties = new Property[] { new LdapTimesProperty() ,new FilterProperty(),new MailAttrNameProperty(),new HostnameProperty(), new PortProperty(),new DbNameProperty(),new UsernameProperty(),new PasswordProperty()};
		}
}
