package pl.compan.docusafe.parametrization.ul;

import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.joda.time.DateMidnight;
import pl.compan.docusafe.api.Fields;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.exports.AttachmentInfo;
import pl.compan.docusafe.core.exports.BudgetItem;
import pl.compan.docusafe.core.exports.ExportedDocument;
import pl.compan.docusafe.core.exports.FundSource;
import pl.compan.docusafe.core.exports.PurchasingDocument;
import pl.compan.docusafe.core.exports.RepositoryItem;
import pl.compan.docusafe.core.imports.simple.repository.CacheHandler;
import pl.compan.docusafe.core.imports.simple.repository.Dictionary;
import pl.compan.docusafe.core.imports.simple.repository.DictionaryMap;
import pl.compan.docusafe.core.jbpm4.AssignUtils;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.AssigneeLogic;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.ul.hib.ProjectBudgetPositionResources;
import pl.compan.docusafe.parametrization.ul.hib.UlBudgetPosition;
import pl.compan.docusafe.parametrization.ul.services.ProductImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static pl.compan.docusafe.parametrization.ul.CostInvoiceLogic.*;

public abstract class AbstractDelegationLogic extends AbstractUlDocumentLogic {


    public static final String STATEMENT_OUT_TASK_SUFFIX = "_roz";
    public static final int PL_DIET_RATE_ID = 2;
    public static final String DELEGOWANY_WYBOR = "DELEGOWANY_WYBOR";
    public static final String WNIOSKODAWCA = "WNIOSKODAWCA";
    public static final String WORKER_DIVISION = "WORKER_DIVISION";
    public static final String DELEGOWANY_INNY = "DELEGOWANY_INNY";
    public static final String DELEGOWANY = "DELEGOWANY";
    public static final String KALKULACJA_KOSZTOW = "KALKULACJA_KOSZTOW";
    public static final String KOSZTY_PODROZY = "KOSZTY_PODROZY";
    public static final String BP_ROZ_FIELD = BP_FIELD + "_ROZ";

    protected static final Logger log = LoggerFactory.getLogger(AbstractDelegationLogic.class);
    public static final String QUESTOR_TASK_NAME = "questor";
    public static final int ZALICZKA_PRZELEW_ID = 2502;
    public static final long UL_CONTRACTOR_ID = 1040l;

    public static final String PRODUCT_DELEGATION_TABLE_NAME = "dsg_ul_view_product_delegation";
    public static final HashSet<String> ACCOUNTANCY_ACCEPT_STATES = Sets.newHashSet("budget_type_accept_roz", "budget_type_accept_default_roz", "budget_type_accept_correction_roz");


    @Override
    public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException {
        StringBuilder msgBuilder = new StringBuilder();

        if (ACCOUNTANCY_ACCEPT_STATES.contains(acceptationCn)) {

            FieldsManager fm = doc.getFieldsManager();


            if ("TAK".equalsIgnoreCase(fm.getEnumItemCn("ROZLICZENIE_VAT"))) {
                Object vatValues = fm.getValue("STAWKI_VAT");
                Preconditions.checkState(vatValues != null && vatValues instanceof Iterable);

                List<Long> pozycjeFaktury = (List) fm.getKey(getPositionFieldCn());
                Preconditions.checkState(pozycjeFaktury != null && pozycjeFaktury instanceof Iterable);
                if (pozycjeFaktury != null) {
                    int i = 0;
                    UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
                    for (Long bpId : pozycjeFaktury) {
                        UlBudgetPosition bp = bpInstance.find(bpId);
                        List<VatRate> vatRates = getVatRateCode(doc.getId(), bp.getId(), getMultipleValueTable());

                        if(vatRates.isEmpty()) {
                            continue;
                        }

                        BigDecimal vatGrossAmountSum = BigDecimal.ZERO;

                        for (VatRate rate : vatRates) {
                            vatGrossAmountSum = vatGrossAmountSum.add(rate.getGrossAmount());
                        }

                        if (bp.getIAmount().compareTo(vatGrossAmountSum) != 0) {
                            msgBuilder.append(String.format(POSITION_BRUTTO_SUM_VATS_ERROR_MSG, bp.getINo()));
                        }
                    }
                }
            } else {
                doc.getDocumentKind().setOnly(doc.getId(), ImmutableMap.of("STAWKI_VAT", Lists.newArrayList()));
            }

            if (msgBuilder.length() > 0) {
                throw new EdmException(msgBuilder.toString());
            }
        }
    }

    private String getPositionFieldCn() {
        return /*this instanceof DelegationLogic ? BP_FIELD : */BP_ROZ_FIELD;
    }

    @Override
    public ExportedDocument buildExportDocument(OfficeDocument doc) throws EdmException {
        PurchasingDocument exported = new PurchasingDocument();
        FieldsManager fm = doc.getFieldsManager();

        exported.setPaymentMethod(Objects.equal(fm.getIntegerKey("FORMA_ZALICZKI"), ZALICZKA_PRZELEW_ID) ? "Przelew 7" : "Got�wka");
        exported.setPurchaseType(Optional.fromNullable(fm.getEnumItem("TYP_DOKUMENTU").getCn()).or("DPP"));
        exported.setExternalContractor(UL_CONTRACTOR_ID);
        exported.setIssued(new DateMidnight(fm.getKey("DATA_WYPELNIENIA")).toDate());
        exported.setRecieved(new DateMidnight(fm.getKey("DATA_WYPELNIENIA")).toDate());

        String delegatedKind = fm.getStringKey(AbstractDelegationLogic.DELEGOWANY_WYBOR);

        DSUser workerAsUser = null;
        if ("2".equals(delegatedKind)) {
            try {
                List<Recipient> recipients = doc.getRecipients();
                for (Recipient worker : recipients) {
                    String username = getUsernameFromRecipient(worker);
                    workerAsUser = DSUser.findByUsername(username);
                    break;
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        } else {
            workerAsUser = DSUser.findByUsername(doc.getCreatingUser());
        }

        if (!Boolean.TRUE.equals(AvailabilityManager.isAvailable("export.worker.disable"))) {
            if (workerAsUser.getExtension() != null) {
                exported.setCreatingUser(workerAsUser.getExtension());
            }
        }

        exported.setAddInfo(fm.getStringValue("ADD_INFO"));
        exported.setCode(fm.getStringValue("NR_WYJAZDU"));

        exported.setDescription(getAdditionFieldsDescription(doc, fm));

        List<Long> pozycjeFaktury = (List) fm.getKey(getPositionFieldCn());
        if (pozycjeFaktury != null) {
            int i = 0;
            List<BudgetItem> budgetItems = Lists.newArrayList();
            UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();


            for (Long bpId : pozycjeFaktury) {
                UlBudgetPosition bp = bpInstance.find(bpId);

                List<VatRate> vatRates = getVatRateCode(doc.getId(), bp.getId(), getMultipleValueTable());

                // je�li nie ma przy najmniej jedenj stawki vat dla danej
                // pozycji, dodaj jedn�
                if (vatRates.isEmpty()) {
                    vatRates.add(VatRate.newBlank());
                } else {
                    BigDecimal sum = BigDecimal.ZERO;
                    for (VatRate vatRate : vatRates) {
                        sum = sum.add(vatRate.getGrossAmount());
                    }

                    Preconditions.checkState(sum.compareTo(bp.getIAmountOrDefault()) == 0, String.format(CostInvoiceLogic.POSITION_BRUTTO_SUM_VATS_ERROR_MSG, bp.getINo()));
                }


                for (VatRate vatRate : vatRates) {
                    BudgetItem bItem = new BudgetItem();
                    int nrp = i + 1;

                    bItem.setItemNo(nrp);
                    bItem.setUnit("szt");
                    bItem.setQuantity(BigDecimal.ONE);

                    bItem.setVatRateCode(vatRate.getRateCodeOrDefault());

                    bItem.setTaxType(vatRate.getTaxType());

                    if (vatRate.getTaxId() != null) {
                        if (!Boolean.TRUE.equals(AvailabilityManager.isAvailable("export.vat_rate.disable"))) {
                            Optional<pl.compan.docusafe.core.dockinds.field.Field> vatRateTypeField = findField(DOC_KIND_CN, STAWKI_VAT_FIELD_NAME, "DZIALALNOSC_1");
                            if (vatRateTypeField.isPresent()) {
                                EnumItem rateItem = vatRateTypeField.get().getEnumItem(vatRate.getTaxId());
                                bItem.setTaxRatio(rateItem.getArg(1));
                            } else {
                                log.error("cant find DZIALALNOSC_1");
                            }
                        }
                    }

                    if (bp.getCType() != null) {
                        Integer id = DataBaseEnumField.getEnumItemForTable(ProductImport.VIEW_PRODUCT_DELEGATION, bp.getCType()).getCentrum();
                        if (id != null && 0 != id) {
                            bItem.setCostKindId(id.longValue());
                        }
                    }

                    BigDecimal netAmount;
                    if (vatRate.isFilled()) {
                        netAmount = vatRate.getNetAmount().setScale(2);
                    } else {
                        netAmount = bp.getIAmountOrDefault().setScale(2);
                    }
                    bItem.setNetAmount(netAmount);

                    if (vatRate.isFilled()) {
                        BigDecimal vatAmount = vatRate.getVatAmount().setScale(2);
                        bItem.setVatAmount(vatAmount);
                    } else {
                        bItem.setVatAmount(BigDecimal.ZERO);
                    }


                    bItem.setProjectCode(getCnValue(PROJECTS_TABLE_NAME, bp.getCProject(), false, null));
                    bItem.setBudgetCode(getCnValue(CostInvoiceLogic.PROJECT_BUDGETS_TABLE_NAME, bp.getCBudget(), false, null));

                    if (bp.getCPosition() != null) {
                        bItem.setPositionId(bp.getCPosition().longValue());
                    }

                    if (bp.getCResource() != null) {
                        try {
                            ProjectBudgetPositionResources resource = ProjectBudgetPositionResources.findByErpId(bp.getCResource().longValue());
                            bItem.setResourceId(resource.getZasobId());
                        } catch (EntityNotFoundException e) {
                            log.error("CAUGHT " + e.getMessage(), e);
                        }
                    }

                    // komorka_id
                    bItem.setMpkCode(getCnValue(ORGANIZATIONAL_UNIT_TABLE_NAME, bp.getCMpk(), false, null));

                    if (bp.getCFundSource() != null) {
                        FundSource fItem = new FundSource();

                        // zrodlo finansowania
                        // je?li -1, czyli "nie dotyczy" to nie exportuj
                        fItem.setCode(getCnValue(FUND_SOURCES_TABLE_NAME, bp.getCFundSource(), false, null));

                        bItem.setFundSources(Lists.newArrayList(fItem));
                    }

                    //REPOSITORY ATTRIBUTES
                    if (bp.getIKind() != null) {
                        EnumItem kind = DataBaseEnumField.getEnumItemForTable(CostInvoiceLogic.RODZAJ_DZIALALNOSCI_TABLE_NAME, bp.getIKind());
                        if (kind.getCn().startsWith(KOSZTY_CN_PREFIX) || kind.getCn().startsWith(KOSZTY2_CN_PREFIX) || kind.getCn().startsWith(KOSZTY3_CN_PREFIX)) {
                            if (fm.getEnumItem("TYP_DOKUMENTU") != null) {
                                try {
                                    Long attribute = Long.valueOf(fm.getEnumItem("TYP_DOKUMENTU").getRefValue());
                                    if (kind.getCentrum() != null && kind.getCentrum() != 0 && attribute != null) {
                                        bItem.getRepositoryItems().add(new RepositoryItem(attribute, kind.getCentrum().longValue()));
                                    }
                                } catch (NumberFormatException e) {
                                    log.error("CAUGHT: " + e.getMessage(), e);
                                }
                            }
                        } else {
                            if (kind.getCentrum() != null && kind.getCentrum() != 0) {
                                bItem.setCostKindId(kind.getCentrum().longValue());
                            }
                        }

                    }

                    budgetItems.add(bItem);
                    i++;
                }
            }
            exported.setBudgetItems(budgetItems);
        }

        String systemPath = null;

        if (Docusafe.getAdditionProperty("erp.faktura.link_do_skanu") != null) {
            systemPath = Docusafe.getAdditionProperty("erp.faktura.link_do_skanu");
        } else {
            systemPath = Docusafe.getBaseUrl();
        }

        for (Attachment zal : doc.getAttachments()) {
            AttachmentInfo info = new AttachmentInfo();

            String path = systemPath+ "/repository/view-attachment-revision.do?id=" + zal.getMostRecentRevision().getId();

            info.setName(zal.getTitle());
            info.setUrl(path);

            exported.addAttachementInfo(info);
        }

        return exported;
    }

    private String getMultipleValueTable() {
        return this instanceof DelegationLogic ? "dsg_ul_delegation_multiple_value" : "dsg_ul_delegation_abroad_multiple_value";
    }

    abstract void getAddtitionFieldsDescription(FieldsManager fm, Map<String,String> additionFields) throws EdmException;

    private String getAdditionFieldsDescription(OfficeDocument doc, FieldsManager fm) throws EdmException {
        Map<String,String> additionFields = Maps.newLinkedHashMap();
        additionFields.put("Nr KO",doc.getOfficeNumber() != null ? doc.getOfficeNumber().toString() : "brak");
        additionFields.put("Numer wyjazdu s�u�bowego", fm.getStringValue("NR_WYJAZDU"));
        additionFields.put("Wnioskodawca", fm.getStringValue("WNIOSKODAWCA"));
        additionFields.put("Nazwa jednostki", fm.getStringValue("WORKER_DIVISION"));
        additionFields.put("Delegowany", fm.getStringValue("DELEGOWANY_WYBOR"));
        additionFields.put("Delegowany (Imi� i nazwisko)", fm.getStringValue("DELEGOWANY"));
        additionFields.put("Status", fm.getStringValue("DELEGOWANY_INNY"));
        additionFields.put("Delegowany", fm.getStringValue("RECIPIENT_HERE"));

        getAddtitionFieldsDescription(fm, additionFields);

        StringBuilder description = new StringBuilder();
        for (String label : additionFields.keySet()) {
            if (!Strings.isNullOrEmpty(additionFields.get(label))) {
                description.append(label)
                        .append(": ")
                        .append(additionFields.get(label))
                        .append("; ");
            }
        }
        return description.toString();
    }

    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
        if (CostInvoiceLogic.BUDGET_OWNER_ACCEPT_TASK_NAME.equals(acceptationCn) || "budget_owner_accept_roz".equals(acceptationCn)) {
            try {
                Long bpId = (Long) openExecution.getVariable(CostInvoiceLogic.BP_VARIABLE_NAME);
                UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
                UlBudgetPosition bp = bpInstance.find(bpId);

                //Z PROJEKTU
                Integer projectId = bp.getCProject();
                if (projectId != null && projectId != -1) {
                    Integer userId = DataBaseEnumField.getEnumItemForTable(PROJECTS_TABLE_NAME, projectId).getCentrum();
                    try {
                        DSUser user = DSUser.findAllByExtension(userId.toString());
                        if (!user.isDeleted()) {
                            assignable.addCandidateUser(user.getName());
                            AssigneeHandler.addToHistory(openExecution, doc, user.getName(), null);
                            return true;
                        } else {
                            throw new AssigningHandledException("Przypisany kierownik projektu zosta� usuni�ty z DS, kod u�ytkownika: " + userId + ", dla pozycji l.p. " + bp.getINo());
                        }
                    } catch (UserNotFoundException e) {
                        throw new AssigningHandledException("Nieprawid�owy kod kierownika projektu: " + userId + ", dla pozycji l.p. " + bp.getINo());
                    }
                }

                //Z POLA OSOBA UPOWAZNIONA/KIEROWNIK
                Integer upowaznionyId = bp.getCUser();
                if (upowaznionyId != null) {
                    String username = DataBaseEnumField.getEnumItemForTable(UPOWAZNIONY_TABLE_NAME, upowaznionyId).getCn();
                    if (StringUtils.isNotEmpty(username)) {
                        assignable.addCandidateUser(username);
                        AssigneeHandler.addToHistory(openExecution, doc, username, null);
                        return true;
                    }
                }

                throw new EdmException("Nie znaleziono osoby do przypisania o id: " + bpId.toString() + ", przypisano do admin'a");
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                log.error("Nie znaleziono osoby do przypisania, przypisano do admin'a");
                if (e instanceof AssigningHandledException) {
                    addRemarkAndAssignToAdmin(doc, assignable, e.getMessage(), DelegationAbroadLogic.log);
                } else {
                    assignable.addCandidateUser("admin");
                }

                return true;
            }
        }
        if ("supervisor_accept".equals(acceptationCn)) {
            try {
                FieldsManager fm = doc.getFieldsManager();
                String delegatedKind = fm.getStringKey("DELEGOWANY_WYBOR");
                DSUser user = null;
                if ("2".equals(delegatedKind)) {
                    Recipient recipient = doc.getRecipients().get(0);
                    String key = recipient.getLparam();
                    String[] ids = key.split(";");
                    if (ids[0].contains("u:")) {
                        String username = ids[0].split(":")[1];
                        user = DSUser.findByUsername(username);
                    }
                } else {
                    Long userId = fm.getLongKey("WNIOSKODAWCA");
                    user = DSUser.findById(userId);
                }

                if (user != null) {
                    if (user.getSupervisor() != null) {
                        assignable.addCandidateUser(user.getSupervisor().getName());
                        return true;
                    } else {
                        throw new AssigningHandledException("U�ytkownik " + user.getName() + " nie posiada okre�lonego bezpo�redniego prze�o�onego");
                    }
                } else {
                    throw new EdmException("Nie okre�lono delegowanego");
                }
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
                log.error("Nie znaleziono osoby do przypisania, przypisano do admin'a");
                if (e instanceof AssigningHandledException) {
                    addRemarkAndAssignToAdmin(doc, assignable, e.getMessage(), DelegationAbroadLogic.log);
                } else {
                    assignable.addCandidateUser("admin");
                }
                return true;
            }
        }

        if (QUESTOR_TASK_NAME.equals(acceptationCn)) {
            try {
                FieldsManager fm = doc.getFieldsManager();

                Object dicRaw = fm.getKey(UPOWAZNIONY_KWESTOR_DICTIONARY_FIELD_NAME);
                List<Integer> upowaznionyEnumIds = Lists.newArrayList();
                if (dicRaw instanceof Collection<?>) {
                    Collection<Long> items = (Collection<Long>) dicRaw;
                    for (Long id : items) {
                        NativeCriteria nc = DSApi.context().createNativeCriteria("dsg_ul_costinvoice_kwestor_upowazniony", "t");

                        CriteriaResult cr = nc.setProjection(NativeExps.projection().addProjection("t.UPOWAZNIONY")).add(NativeExps.eq("t.id", id)).criteriaResult();
                        if (cr.next()) {
                            upowaznionyEnumIds.add(cr.getInteger(0, 0));
                            if (cr.next()) {
                                log.error("Znaleziono kilka wpis�w w tabeli dsg_ul_costinvoice_kwestor_upowazniony dla id:" + id);
                            }
                        }
                    }
                    Set<String> userToAssigne = Sets.newHashSet();
                    for (Integer id : upowaznionyEnumIds) {
                        userToAssigne.add(DataBaseEnumField.getEnumItemForTable(KWESTOR_UPOWAZNIONY_TABLE_NAME, id).getCn());
                    }
                    if (!userToAssigne.isEmpty()) {
                        for (String userName : userToAssigne) {
                            assignable.addCandidateUser(userName);
                            AssigneeHandler.addToHistory(openExecution, doc, userName, null);
                        }
                        return true;
                    }
                }
            } catch (Exception e) {
                log.debug("Nie znaleziono osoby upowaznionej przez kwestora/kancelerza, przypisuje do kwestora/kancelerza");
                return false;
            }
        }

        if ("budget_type_accept2_roz".equals(acceptationCn)) {
            try {
                List<DocumentAcceptance> acceptances = DocumentAcceptance.find(doc.getId());
                for (DocumentAcceptance acceptance : acceptances) {
                    HashSet<String> FINANCIAL_UNIT_ACCEPTANCE_CNS = Sets.newHashSet("budget_type_accept_correction_roz", "budget_type_accept_default_roz", "budget_type_accept_roz");
                    if (FINANCIAL_UNIT_ACCEPTANCE_CNS.contains(acceptance.getAcceptanceCn())) {
                        assignable.addCandidateUser(acceptance.getUsername());
                        AssigneeHandler.addToHistory(openExecution, doc, acceptance.getUsername(), null);
                        return true;
                    }
                }
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
            }
        }

        return false;
    }

    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();
        for (Field f : fm.getFields())
            if (f.getDefaultValue() != null)
                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));

        DSUser user = DSApi.context().getDSUser();
        toReload.put(WNIOSKODAWCA, user.getId());
        //toReload.put(DELEGOWANY, user.asFirstnameLastname());
        //  if (user.getDivisionsWithoutGroup().length > 0)
        //     toReload.put(WORKER_DIVISION_DOCKIND_CN, fm.getField(WORKER_DIVISION_DOCKIND_CN).getEnumItemByCn(user.getDivisionsWithoutGroupPosition()[0].getGuid()).getId());
        toReload.put("DATA_WYPELNIENIA", new Date());

        fm.reloadValues(toReload);
    }

    protected BigDecimal getValueFromDelgationCostSystemDic(int typeOfDelegationCost, int defaultValue) {
        BigDecimal koszt = null;
        try {
            EnumItem stawkaEnumItem = DataBaseEnumField.getEnumItemForTable("dso_delegate_cost", typeOfDelegationCost);
            String stawka = stawkaEnumItem.getRefValue();
            koszt = new BigDecimal(stawka);
        } catch (EdmException e) {
            koszt = new BigDecimal(defaultValue);
            DelegationLogic.log.error(e.getMessage(), e);
        }
        return koszt;
    }


    protected void setDictionaries(String dictionaryName, Map<String, FieldData> values, Map<String, Object> results) throws EdmException {
        String dicNameWithSuffix = dictionaryName + "_";

        clearAllDictionaresFields(CostInvoiceLogic.DOC_KIND_CN, dicNameWithSuffix, results);

        DwrUtils.setRefValueEnumOptions(values, results, "I_GROUP", dicNameWithSuffix, RODZAJ_DZIALALNOSCI_FIELD_NAME, RODZAJ_DZIALALNOSCI_TABLE_NAME, null, EMPTY_ENUM_VALUE);
        DwrUtils.setRefValueEnumOptions(values, results, "I_GROUP", dicNameWithSuffix, TYPY_DZIALALNOSCI_FIELD_NAME, TYPY_DZIALALNOSCI_TABLE_NAME, null, EMPTY_ENUM_VALUE);

        String controlDictCnFromResults = dicNameWithSuffix+"I_KIND";

        if (results.get(controlDictCnFromResults) != null && results.get(controlDictCnFromResults) instanceof EnumValues) {
            String selectedId = ((EnumValues) results.get(controlDictCnFromResults)).getSelectedId();

            if (StringUtils.isNotEmpty(selectedId)) {

                Integer rodzajId = Integer.valueOf(selectedId);

                EnumItem rodzajEnum = DataBaseEnumField.getEnumItemForTable(CostInvoiceLogic.RODZAJ_DZIALALNOSCI_TABLE_NAME, rodzajId);
                String controlValue = rodzajEnum.getCn();

                if (controlValue != null) {
                    List<DictionaryMap> dictToShow = CacheHandler.getDictionariesMappings(controlValue, CostInvoiceLogic.DOC_KIND_CN);

                    for (DictionaryMap dictMap : dictToShow) {
                        EnumValues defaultEnum = setDefaultEmptyEnum(dictMap);

                        Dictionary dict = dictMap.getDictionary();
                        if (dict.getDeleted() == false) {
                            if ("C_USER".equals(dict.getCn())) {
                                if (!isBudgetOwnerAccept(values, dicNameWithSuffix)) {
                                    continue;
                                }
                            }

                            if (dict.getDictSource() == Dictionary.SourceType.SERVICE.value || dict.getDictSource() == Dictionary.SourceType.MANUAL.value) {
                                setServiceDict(values, results, dicNameWithSuffix, dictMap, defaultEnum, dict);
                            }
                        }
                    }
                }
            }
        }
    }

    private boolean isBudgetOwnerAccept(Map<String, FieldData> values, String dicNameWithSuffix) {
        try {
            String itemGroup = values.get(dicNameWithSuffix + "I_GROUP").getEnumValuesData().getSelectedOptions().get(0);

            if (StringUtils.isNotBlank(itemGroup) && StringUtils.isNumeric(itemGroup)) {
                Integer itemGroupId = Integer.valueOf(itemGroup);
                EnumItem item = DataBaseEnumField.getEnumItemForTable(CostInvoiceLogic.GRUPY_DZIALALNOSCI_TABLE_NAME, itemGroupId);
                if (SOCIAL_ITEM_GROUP_CNS.contains(item.getCn())) {
                    return false;
                }
            }
        } catch (NullPointerException e) {
        } catch (EdmException e) {
            log.error(e.getMessage(),e);
        }
        return true;
    }

    private void setServiceDict(Map<String, FieldData> values, Map<String, Object> results, String dicNameWithSuffix,
                                DictionaryMap dictMap, EnumValues defaultEnum, Dictionary dict) {
        if (dict.getRefDict() == null) {
            try {
                DataBaseEnumField base = DataBaseEnumField.tableToField.get(dict.getCn().equals("C_TYPE") ?  AbstractDelegationLogic.PRODUCT_DELEGATION_TABLE_NAME : dict.getTablename()).iterator().next();
                results.put(dicNameWithSuffix + dict.getCn(), base.getEnumValuesForForcedPush(values, dicNameWithSuffix));
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        } else {
            if (dictMap.isShowAllReferenced()) {
                DwrUtils.setRefValueEnumOptions(values, results, dict.getRefDict().getCn(), dicNameWithSuffix, dict.getCn(), dict.getTablename(), DataBaseEnumField.REF_ID_FOR_ALL_FIELD, defaultEnum);
            } else {
                DwrUtils.setRefValueEnumOptions(values, results, dict.getRefDict().getCn(), dicNameWithSuffix, dict.getCn(), dict.getTablename(), null, defaultEnum);
            }
        }
    }

    private EnumValues setDefaultEmptyEnum(DictionaryMap dictMap) {
        EnumValues defaultEnum = dictMap.isMustShow() ? EMPTY_NOT_HIDDEN_ENUM_VALUE : EMPTY_ENUM_VALUE;
        return defaultEnum;
    }

    private void clearAllDictionaresFields(String dockindCn, String dicNameWithSuffix, Map<String, Object> results) {
        for (Dictionary dictionaryToClear : CacheHandler.getAvailableDicts(dockindCn)) {
            results.put(dicNameWithSuffix+dictionaryToClear.getCn(), EMPTY_ENUM_VALUE);
        }
    }

    static String moneyValueLiterally(BigDecimal amount) {
        try {
            return new String((TextUtils.numberToText(amount.setScale(0, RoundingMode.DOWN))).getBytes("ISO-8859-2"), "Cp1250") + " " + amount.remainder(BigDecimal.ONE).movePointRight(2).setScale(0, RoundingMode.HALF_UP) + "/100";
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }
}
