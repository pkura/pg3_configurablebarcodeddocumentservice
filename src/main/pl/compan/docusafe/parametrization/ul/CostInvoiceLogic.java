package pl.compan.docusafe.parametrization.ul;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.ValueNotFoundException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.exports.*;
import pl.compan.docusafe.core.imports.simple.repository.CacheHandler;
import pl.compan.docusafe.core.imports.simple.repository.Dictionary;
import pl.compan.docusafe.core.imports.simple.repository.Dictionary.SourceType;
import pl.compan.docusafe.core.imports.simple.repository.DictionaryMap;
import pl.compan.docusafe.core.imports.simple.repository.SimpleRepositoryAttribute;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.ReferenceToHimselfException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.ul.hib.CostInvoiceDzialalnosc;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.parametrization.ul.hib.ProjectBudgetPositionResources;
import pl.compan.docusafe.parametrization.ul.hib.UlBudgetPosition;
import pl.compan.docusafe.parametrization.ul.hib.UlBudgetsFromUnits;
import pl.compan.docusafe.parametrization.ul.hib.UlContractor;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.primitives.Ints;

public class CostInvoiceLogic extends AbstractUlDocumentLogic {

    public static final long INVOICE_POSITION_CONTEXT_ID = 6l;
    protected static final Logger log = LoggerFactory.getLogger(CostInvoiceLogic.class);
	
    public static final String DOC_KIND_CN = "ul_costinvoice";
	
	static final String DOCUMENT_TYPES_TABLE_NAME = "dsg_ul_document_types";
	private static final String BARCODE_FIELD = "BARCODE";
	private static final String SOCIAL_BUDGET_ACCEPT_NAME = "social_budget_accept";
	private static final String SOCIAL_852_BUDGET_ACCEPT_NAME = "social_852_accept";
	private static final String ZLECENIA_TABLE_NAME = "dsg_ul_production_orders";
	private static final String NUMER_INWENTARZOWY_TABLE_NAME = "dsg_ul_numer_inwentarzowy";
	private static final String MT08_TABLE_NAME = "dsg_ul_mt08";
	static final String SRODEK_TRWALY_ENUM_ID = "1621";
	public static final String KWESTOR_UPOWAZNIONY_TABLE_NAME = "dsg_ul_kwestor_upowazniony";
	public static final String COST_INVOICE_TABLE = "dsg_ul_costinvoice";
	static final String EXCHANGE_RATE_TABLE = "dsg_ul_exchange_rate";
	private static final String EXCHANGE_RATE_SERVICE_TABLE = "dsg_ul_services_exchange_rate";
	public static final String FUND_SOURCES_TABLE_NAME = "dsg_ul_projects_fund_sources";
	private static final String COST_KIND_TABLE_NAME = "dsg_ul_product";
	public static final String ORGANIZATIONAL_UNIT_TABLE_NAME = "dsg_ul_organizational_unit";
	public static final String PROJECT_BUDGET_POSITIONS_TABLE_NAME = "dsg_ul_project_budget_positions";
	protected static final String PROJECT_BUDGET_POSITION_RESOURCES_TABLE_NAME = "dsg_ul_project_budget_position_resources";
	public static final String PROJECT_BUDGETS_TABLE_NAME = "dsg_ul_project_budgets";
	static final String TYPY_DZIALALNOSCI_TABLE_NAME = "dsg_ul_costinvoice_typy_dzialalnosci";
	private static final String UPOWAZNIONY_TABLE_NAME = "dsg_ul_project_osoba_upowazniona";
	public static final String RODZAJ_DZIALALNOSCI_TABLE_NAME = "dsg_ul_costinvoice_rodzaj_dzialalnosci";
	public static final String GRUPY_DZIALALNOSCI_TABLE_NAME = "dsg_ul_costinvoice_grupy_dzialalnosci";
	public static final String PRODUCT_TABLE_NAME = "dsg_ul_product";
	public static final String PROJECTS_TABLE_NAME = "dsg_ul_projects";
	public static final String INVENTORY_TABLE_NAME = "dsg_ul_numer_inwentarzowy";
	public static final String INVENTORY_REPO_TABLE_NAME = "dsd_repo80_numer_inwentarzowy";
	public static final String OBSZARY_TABLE_NAME = "dsg_ul_invoice_fields";
	public static final String VAT_RATES_TABLE_NAME = "dsg_ul_vat_rates";
	public static final String STAWKI_VAT_TABLE_NAME = "dsg_stawki_vat_faktury";
	public static final String OBSZARY_MAPPING_TABLE_NAME = "dsg_ul_obszary_mapping_table";
	private static final String KOSZTY_RODZAJOWE_ZESPOL_VIEW_NAME = "dsg_ul_view_koszty_rodzajowe_zespol";
	public static final String JEDNOSTKI_VIEW_NAME = "wydzialy_view";

	private static final String BUDGET_TYPE_ID_VAR_NAME = "budgetTypeId";
	public static final String BP_VARIABLE_NAME = "costCenterId";
	
	static final String BUDGET_OWNER_ACCEPT_TASK_NAME = "budget_owner_accept";
	private static final String CHANCELLOR_TASK_NAME = "chancellor";
	private static final String QUESTOR_TASK_NAME = "questor";
	public static final String BUDGET_TYPE_ACCEPTANCE_TASK_NAME = "budget_type_accept";
	public static final String BUDGET_ACCEPTANCE_TASK_NAME = "budget_accept";
	public static final String BUDGET_ACCEPTANCE_ADD_TASK_NAME = "budget_accept_additional";
	
	public static final String UPOWAZNIONY_KWESTOR_DICTIONARY_FIELD_NAME = "UPOWAZNIONY_KWESTOR_DIC";
	static final String TYPY_DZIALALNOSCI_FIELD_NAME = "I_TYPE";
	static final String RODZAJ_DZIALALNOSCI_FIELD_NAME = "I_KIND";
	private static final String KWOTA_POZOSTALA_MPK_FIELD = "KWOTA_POZOSTALA_MPK";
	public static final String UPOWAZNIONY_PROJEKT_FIELD = "UPOWAZNIONY_PROJEKT";
	private static final String DWR_PREFIX = "DWR_";
	private static final String STAWKI_VAT_DWR_FIELD_NAME = DWR_PREFIX+"STAWKI_VAT";
	public static final String KWOTA_POZOSTALA_MPK_DWR_FIELD = DWR_PREFIX+KWOTA_POZOSTALA_MPK_FIELD;
	private static final String UPOWAZNIONY_KANCLERZ_FIELD = "UPOWAZNIONY_KANCLERZ";
	private static final String UPOWAZNIONY_KWESTOR_FIELD = "UPOWAZNIONY_KWESTOR";
	private static final String OBSZAR_FIELD = "OBSZAR";
	public static final String BP_FIELD = "BP";
	static final String BP_FIELD_WITH_LINK = BP_FIELD+"_";
	private static final String KWOTA_NETTO_FIELD = "KWOTA_NETTO";
	private static final String KWOTA_BRUTTO_FIELD = "KWOTA_BRUTTO";
	private static final String RODZAJ_DZIALALNOSCI_FIELD = "RODZAJ_DZIALALNOSCI";
	private static final String KWOTA_BRUTTO_SLOWNIE = "KWOTA_BRUTTO_SLOWNIE";
	private static final String TERMIN_PLATNOSCI_FIELD = "TERMIN_PLATNOSCI";
	private static final String DATA_WYSTAWIENIA_FIELD = "DATA_WYSTAWIENIA";
	private static final String DATA_WPLYWU_FIELD = "DATA_WPLYWU_FAKTURY";
	static final String BP_DWR_FIELD = DWR_PREFIX+BP_FIELD;
	private static final String TERMIN_PLATNOSCI_DWR_FIELD = DWR_PREFIX+TERMIN_PLATNOSCI_FIELD;
	private static final String DATA_WPLYWU_DWR_FIELD = DWR_PREFIX+DATA_WPLYWU_FIELD;
	private static final String DATA_WYSTAWIENIA_DWR_FIELD = DWR_PREFIX+DATA_WYSTAWIENIA_FIELD;
	private static final String KWOTA_NETTO_DWR_FIELD = DWR_PREFIX+"KWOTA_NETTO";
	private static final String KWOTA_WALUTOWA_DWR_FIELD = DWR_PREFIX+"KWOTA_WALUTOWA";
    public static final String KWOTA_KURS_FIELD = "KWOTA_KURS";
    private static final String KWOTA_KURS_DWR_FIELD = DWR_PREFIX+ KWOTA_KURS_FIELD;
	static final String KWOTA_BRUTTO_DWR_FIELD = DWR_PREFIX+KWOTA_BRUTTO_FIELD;
	private static final String KWOTA_VAT_DWR_FIELD = DWR_PREFIX+"KWOTA_VAT";
	private static final Object RODZAJ_DZIALALNOSCI_DWR_FIELD = DWR_PREFIX+"RODZAJ_DZIALALNOSCI";

    private static final String SUMA_VATS_ERROR_MSG = "Suma kwot netto s�ownika stawek VAT musi by� r�wna kwocie netto faktury. ";
    private static final String SUMA_VATS_BRUTTO_ERROR_MSG = "Suma kwot brutto s�ownika stawek VAT musi by� r�wna kwocie brutto faktury.";
    public static final String POSITION_BRUTTO_SUM_VATS_ERROR_MSG = "Suma kwot brutto s�ownika stawek VAT dla pozycji lp %d musi by� r�wna kwocie tej pozycji. ";
    private static final String DATA_PLATNOSCI_ERROR_MSG = "Data p�atno�ci nie mo�e by� wcze�niejsza ni� data wystawienia. ";
    private static final String DATA_WPLYWU_ERROR_MSG = "Data wp�ywu nie mo�e by� wcze�niejsza ni� data wystawienia. ";
    private static final String DATA_WPLYWU_TERAZ_ERROR_MSG = "Data wp�ywu nie mo�e by� p�niejsza od dzisiejszego dnia. ";
    public static final String WYSTAWCA_FAKTURY_PIERWOTNEJ_ERROR_MSG = "Wystawca faktury pierwotnej/korygowanej nieodpowiada wystawcy wskazanym na tym dokumencie. ";

	private static final String NIEPRZEKAZANO_STRING_FLAG = "nieprzekazano";

//	private static final EnumValues NOT_APPLICABLE_ENUM_VALUE = new EnumValues(new HashMap<String,String>(){{put("-1","nie dotyczy");put("","");}}, new ArrayList<String>(){{add("-1");}});
//	private static final EnumValues EMPTY_ENUM_VALUE = new EnumValues(new HashMap<String,String>(){{put("","-- wybierz --");}}, new ArrayList<String>(){{add("");}});
	static final EnumValues EMPTY_NOT_HIDDEN_ENUM_VALUE = new EnumValues(new HashMap<String,String>(){{put("","-- wybierz --");}}, new ArrayList<String>(){{add("");}});
	private static final EnumValues NOT_APPLICABLE_ENUM_VALUE = new EnumValues(new HashMap<String,String>(), new ArrayList<String>());
	static final EnumValues EMPTY_ENUM_VALUE = new EnumValues(new HashMap<String,String>(), new ArrayList<String>());

	public static final Integer ZAKUP_KSIEGOZBIOR_ID = 1626;

	public static final Integer FAKTURA_SOCIAL_DS = 3;
	public static final Integer FAKTURA_SOCIAL_KN_ID = 4;
	public static final Integer FAKTURA_SOCIAL_OS_ID = 5;
	public static final Set<String> SOCIAL_ID_FOR_NONE_BUDGET_OWNER_ACCEPT = Sets.newHashSet();
	static {
		SOCIAL_ID_FOR_NONE_BUDGET_OWNER_ACCEPT.add(FAKTURA_SOCIAL_OS_ID.toString());
		SOCIAL_ID_FOR_NONE_BUDGET_OWNER_ACCEPT.add(FAKTURA_SOCIAL_DS.toString());
	}

	public static final Set<String> SOCIAL_ITEM_GROUP_CNS = Sets.newHashSet();

    public static final String GROUP_851_CN = "851-X";
    public static final String GROUP_852_CN = "852-X";

    static {
		SOCIAL_ITEM_GROUP_CNS.add(GROUP_851_CN);
		SOCIAL_ITEM_GROUP_CNS.add(GROUP_852_CN);
	}

    public static final String KWOTA_KURS_TABELA_ERP_FIELD = KWOTA_KURS_FIELD + "_TABELA_ERP";
    public static final String NIE_ZNALEZIONO_W_ERP_MESSAGE = "Nie znaleziono w ERP";
    public static final String BUDGET_ACCEPT_STATUS_ID = "100";
    private static final String BUDGET_ACCEPT_ADDITIONAL_STATUS_ID = "101";
    public static final HashSet<String> ACCOUNTANCY_ACCEPT_STATES = Sets.newHashSet("budget_type_accept", "budget_type_accept_default", "budget_type_accept_correction");
    public static final String NIE_DOTYCZY = "Nie dotyczy";
    public static final String SPOSOB_FIELD = "SPOSOB";
    public static final String PRZEDPLATA_FIELD = "PRZEDPLATA";
    public static final String DATA_OBOWIAZYWANIA_KURSU_FIELD = "KWOTA_WALUTA_DATA";
    public static final String STATUS_FIELD = "STATUS";
    public static final Set<Integer> AUTHOR_TASK_STATUS_IDS = Sets.newHashSet(null, 10, 20);
    public static final String FIND_SECREATARY_SQL = "DECLARE @role_id int = ?, @div_id int = ?;\n" +
            "select distinct u.NAME from DS_DIVISION d \n" +
            "join DS_USER_TO_DIVISION ud on d.ID=ud.DIVISION_ID \n" +
            "join DS_USER u on ud.USER_ID=u.ID join dso_role_usernames rd on u.NAME=rd.username\n" +
            "join ds_profile_to_role pr on pr.role_id=rd.role_id\n" +
            "join ds_profile_to_user pu on pr.profile_id=pu.profile_id and pu.user_id=u.id and (pu.USERKEY is null or pu.USERKEY like '' or pu.USERKEY like '#' or pu.USERKEY like d.GUID)\n" +
            "where rd.role_id=@role_id and d.ID=@div_id\n" +
            "union\n" +
            "select u.NAME from DS_DIVISION d \n" +
            "join DS_USER_TO_DIVISION ud on d.ID=ud.DIVISION_ID \n" +
            "join DS_USER u on ud.USER_ID=u.ID join dso_role_usernames rd on u.NAME=rd.username and rd.source ='own'\n" +
            "where rd.role_id=@role_id and d.ID=@div_id";
    public static final String BUDGET_OWNER_ACCEPT_NEW_TASK_NAME = "budget_owner_accept-new";


    @Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
		FieldsManager fm = document.getFieldsManager();
        DocumentKind docKind = fm.getDocumentKind();
		try {
            BigDecimal positionSum = assignOrdinalNumberAndGetPositionSum(fm);

            BigDecimal grossAmount = fm.getKey(KWOTA_BRUTTO_FIELD) != null ? new BigDecimal(fm.getKey(KWOTA_BRUTTO_FIELD).toString()) : BigDecimal.ZERO;

            Map<String, Object> dockindSetOnlyMap = Maps.newHashMap();

            if (AUTHOR_TASK_STATUS_IDS.contains(fm.getIntegerKey(STATUS_FIELD)) && fm.getKey(DATA_WYSTAWIENIA_FIELD) != null) {
                findCurrencyParameters(fm, dockindSetOnlyMap);
            }

            dockindSetOnlyMap.put(KWOTA_POZOSTALA_MPK_FIELD, grossAmount.subtract(positionSum).setScale(2, RoundingMode.DOWN));
            docKind.setOnly(document.getId(), dockindSetOnlyMap, false);

            reloadDBEnumFieldData("pozycje_budzetowe_doc_view");
            //reloadDBEnumFieldData(document.getDocumentKind(), "KONTRAHENT_NR_KONTA_ID");
		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}
	}

    private void findCurrencyParameters(FieldsManager fm, Map<String, Object> forDockindSet) throws EdmException {
        Date currencyDate;
        if (fm.getKey(DATA_OBOWIAZYWANIA_KURSU_FIELD) == null) {
            DateTime date = new DateTime(((Date)fm.getKey(DATA_WYSTAWIENIA_FIELD)).getTime());
            DateTime dateBefore = date.minusDays(1);
            if (dateBefore.getDayOfWeek() > DateTimeConstants.FRIDAY) {
                dateBefore = dateBefore.withDayOfWeek(DateTimeConstants.FRIDAY);
            }
            currencyDate = dateBefore.toDate();
        } else {
            currencyDate = (Date) fm.getKey(DATA_OBOWIAZYWANIA_KURSU_FIELD);
        }
        DocumentCacheHandler.CurrencyRate currencyRate = getCurrencyParam(fm.getKey("KWOTA_WALUTA"), currencyDate);

        forDockindSet.put(KWOTA_KURS_FIELD, currencyRate.getRateWithPlain());
        forDockindSet.put(KWOTA_KURS_TABELA_ERP_FIELD, currencyRate.getRateTableCodeWithPlain());
        forDockindSet.put(DATA_OBOWIAZYWANIA_KURSU_FIELD, currencyDate);
    }

    @Override
	public ExportedDocument buildExportDocument(OfficeDocument doc) throws EdmException {
		boolean processNotApplicableValue = false;
		Integer notApplicableValue = -1;

		PurchasingDocument exported = new PurchasingDocument();
		FieldsManager fm = doc.getFieldsManager();

		exported.setPaymentMethod("Got�wka".equalsIgnoreCase(fm.getStringValue(SPOSOB_FIELD)) ? "Got�wka" : "Przelew 7");
		exported.setPaymentMethodId("Przelew".equalsIgnoreCase(fm.getStringValue(SPOSOB_FIELD)) ? 1 : 0);

		exported.setCurrencyCode(fm.getEnumItem("KWOTA_WALUTA") != null ? fm.getEnumItem("KWOTA_WALUTA").getCn() : "PLN");
		
		exported.setPurchaseType(fm.getEnumItem("TYP_FAKTURY") != null ? fm.getEnumItem("TYP_FAKTURY").getCn() : "FZP");

        exported.setExternalContractor(UlContractor.find(fm.getIntegerKey("CONTRACTOR")).getDostawcaId());
		
		if (fm.getEnumItem("KONTRAHENT_NR_KONTA_ID") != null) {
			exported.setContractorAccountId(fm.getEnumItem("KONTRAHENT_NR_KONTA_ID").getCentrum());
		}

        try {
            List<Recipient> recipients = doc.getRecipients();
            for (Recipient worker : recipients) {
                String username = getUsernameFromRecipient(worker);
                DSUser user = DSUser.findByUsername(username);
                exported.setCreatingUser(user.getExtension());
                break;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        if (fm.getBoolean("PLATNOSC_SZCZEGOLNA")) {
            List<Map<String,Object>> payments = (List<Map<String, Object>>) fm.getValue("RACHUNKI");
            InstallmentItem item;

            if (payments != null) {
                for (Map<String, Object> payment : payments) {
                    exported.addInstallmentItem(item = new InstallmentItem());

                    item.setTypPlatnosci(0);
                    item.setSposobZaplaty(1);
                    item.setCzyProcent(false);
                    item.setKwota((BigDecimal) payment.get("RACHUNKI_KWOTA"));
                    item.setTerminPlatnosci((Date) payment.get("RACHUNKI_DATA"));
                }
            }
        } else {
            exported.setExpired((Date) fm.getKey("TERMIN_PLATNOSCI"));
        }


		exported.setRecieved((Date) fm.getKey("DATA_WPLYWU_FAKTURY"));
		exported.setIssued((Date) fm.getKey("DATA_WYSTAWIENIA"));
        exported.setOperation(exported.getIssued());

        exported.setSettlementTaxPeriodId(DocumentCacheHandler.getSettlementPeriodId(exported.getRecieved()));
        // nie dziala w erpie
        // exported.setSettlementPeriodId(DocumentCacheHandler.getSettlementPeriodId(exported.getIssued()));

        exported.setExchangeRate(fm.getKey(KWOTA_KURS_FIELD) != null ? new BigDecimal(fm.getKey(KWOTA_KURS_FIELD).toString()) : null);

        String currencyRateCode = fm.getStringKey(KWOTA_KURS_TABELA_ERP_FIELD);
        if (!Strings.isNullOrEmpty(currencyRateCode) && !NIE_DOTYCZY.equals(currencyRateCode)) {
            exported.setCurrencyRateCode(currencyRateCode);
        }

		exported.setBarcode(fm.getStringValue(BARCODE_FIELD));
		
        exported.setAddInfo(fm.getStringValue("ADDINFO"));
		exported.setCode(fm.getStringValue("NR_FAKTURY"));

		exported.setDescription(getAdditionFieldsDescription(doc, fm));

		Integer docTypeObjectId = 1;
		if (fm.getEnumItem("TYP_FAKTURY") != null) {
			try {
				docTypeObjectId = fm.getEnumItem("TYP_FAKTURY").getCentrum();
			} catch (NumberFormatException e) {
				log.error("CAUGHT: "+e.getMessage(),e);
			}
		}

		List<SimpleRepositoryAttribute> repositoryDicts = CacheHandler.getRepositoryDicts(docTypeObjectId, (int)INVOICE_POSITION_CONTEXT_ID);
		
		List<Long> pozycjeFaktury = (List) fm.getKey(BP_FIELD);
        if (pozycjeFaktury != null) {
            int i = 0;
            List<BudgetItem> budgetItems = Lists.newArrayList();
            UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();

            BigDecimal bpAmountSum = BigDecimal.ZERO;
            for (Long bpId : pozycjeFaktury) {
                int positionType = 2;
                UlBudgetPosition bp = bpInstance.find(bpId);

                bpAmountSum = bpAmountSum.add(bp.getIAmountOrDefault());

                List<VatRate> vatRates = getVatRateCode(doc.getId(), bp.getId());

                // je�li nie ma przy najmniej jedenj stawki vat dla danej
                // pozycji, dodaj jedn�
                if (vatRates.isEmpty()) {
                    vatRates.add(VatRate.newBlank());
                } else {
                    BigDecimal sum = BigDecimal.ZERO;
                    for (VatRate vatRate : vatRates) {
                        sum = sum.add(vatRate.getGrossAmount());
                    }

                    Preconditions.checkState(sum.compareTo(bp.getIAmountOrDefault()) == 0, String.format(POSITION_BRUTTO_SUM_VATS_ERROR_MSG, bp.getINo()));
                }

                for (VatRate vatRate : vatRates) {
                    BudgetItem bItem = new BudgetItem();
                    int nrp = i + 1;

                    bItem.setItemNo(nrp);
                    bItem.setUnit("szt");
                    bItem.setQuantity(BigDecimal.ONE);

                    if (bp.getCKind() != null && bp.getCKind().compareTo(1621) == 0) {
                        bItem.setFixedAsset(true);
                    }

                    bItem.setTaxType(vatRate.getTaxType());

                    if (!Boolean.TRUE.equals(AvailabilityManager.isAvailable("export.vat_rate.disable"))) {
                        Optional<pl.compan.docusafe.core.dockinds.field.Field> vatRateTypeField = findField(DOC_KIND_CN, STAWKI_VAT_FIELD_NAME, "DZIALALNOSC_1");
                        if (vatRateTypeField.isPresent()) {
                            EnumItem rateItem = vatRateTypeField.get().getEnumItem(vatRate.getTaxId());
                            bItem.setTaxRatio(rateItem.getArg(1));
                        } else {
                            log.error("cant find DZIALALNOSC_1");
                        }
                    }

                    bItem.setVatRateCode(vatRate.getRateCodeOrDefault());

                    if (bp.getCType() != null && -1 != bp.getCType()) {
                        Integer id = DataBaseEnumField.getEnumItemForTable(CostInvoiceLogic.PRODUCT_TABLE_NAME, bp.getCType()).getCentrum();
                        if (id != null && 0 != id) {
                            bItem.setCostKindId(id.longValue());
                        }
                    }

                    BigDecimal netAmount;
                    if (vatRate.isFilled()) {
                        netAmount = vatRate.getNetAmount().setScale(2);
                    } else {
                        netAmount = bp.getIAmountOrDefault().setScale(2);
                    }

                    BigDecimal currencyRate = exported.getExchangeRate();
                    if (currencyRate != null) {
                        netAmount = netAmount.divide(currencyRate, 2, RoundingMode.HALF_UP);
                    }
                    bItem.setNetAmount(netAmount);

                    if (vatRate.isFilled()) {
                        BigDecimal vatAmount = vatRate.getVatAmount().setScale(2);
                        if (currencyRate != null) {
                            vatAmount = vatAmount.divide(currencyRate, 2, RoundingMode.HALF_UP);
                        }
                        bItem.setVatAmount(vatAmount);
                    } else {
                        bItem.setVatAmount(BigDecimal.ZERO);
                    }

                    // kontrakt_id - projekt
                    // je�li -1, czyli "nie dotyczy" to nie exportuj
                    // projektu
                    bItem.setProjectCode(getCnValue(PROJECTS_TABLE_NAME, bp.getCProject(), processNotApplicableValue, notApplicableValue));

                    // projekt_id
                    bItem.setBudgetCode(getCnValue(PROJECT_BUDGETS_TABLE_NAME, bp.getCBudget(), processNotApplicableValue, notApplicableValue));

                    bItem.setBudgetKoId(bp.getCStructureBudgetKo() != null ? bp.getCStructureBudgetKo().longValue() : null);
                    bItem.setBudgetKindId(bp.getCStructureBudgetPosition() != null ? bp.getCStructureBudgetPosition().longValue() : null);

                    //etap_id
                    bItem.setPositionId(bp.getCPosition() != null ? bp.getCPosition().longValue() : null);

                    // zasob_id
                    if (bp.getCResource() != null) {
                        try {
                            ProjectBudgetPositionResources resource = ProjectBudgetPositionResources.findByErpId(bp.getCResource().longValue());
                            bItem.setResourceId(resource.getZasobId());
                        } catch (EntityNotFoundException e) {
                            log.error("CAUGHT " + e.getMessage(), e);
                        }
                    }

                    // komorka_id
                    bItem.setMpkCode(getCnValue(ORGANIZATIONAL_UNIT_TABLE_NAME, bp.getCMpk(), processNotApplicableValue, notApplicableValue));

                    // zlecprod_id
                    bItem.setOrderCode(getCnValue(ZLECENIA_TABLE_NAME, bp.getCOrder(), processNotApplicableValue, notApplicableValue));

                    //REPOSITORY ATTRIBUTES
                    if (bp.getIKind() != null) {
                        EnumItem kind = DataBaseEnumField.getEnumItemForTable(CostInvoiceLogic.RODZAJ_DZIALALNOSCI_TABLE_NAME, bp.getIKind());
                        if (kind.getCn().startsWith(KOSZTY_CN_PREFIX) || kind.getCn().startsWith(KOSZTY2_CN_PREFIX) || kind.getCn().startsWith(KOSZTY3_CN_PREFIX)) {
                            if (fm.getEnumItem("TYP_FAKTURY") != null) {
                                try {
                                    Long attribute = Long.valueOf(fm.getEnumItem("TYP_FAKTURY").getRefValue());
                                    if (kind.getCentrum() != null && kind.getCentrum() != 0 && attribute != null) {
                                        bItem.getRepositoryItems().add(new RepositoryItem(attribute, kind.getCentrum().longValue()));
                                    }
                                } catch (NumberFormatException e) {
                                    log.error("CAUGHT: " + e.getMessage(), e);
                                }
                            }
                        } else {
                            if (kind.getCentrum() != null && kind.getCentrum() != 0) {
                                bItem.setCostKindId(kind.getCentrum().longValue());
                            }
                        }

                        budgetItems.add(bItem);
                    }


                    if (bp.getCStructureBudgetPositionRaw() != null) {
                        try {
                            Optional<pl.compan.docusafe.core.dockinds.field.Field> field = findField(DOC_KIND_CN, BP_FIELD, "C_STRUCTURE_BUDGET_POSITION_RAW_1");

                            if (field.isPresent()) {
                                field.get().getEnumItemByCn(bp.getCStructureBudgetPositionRaw());
                                if (field.get().getEnumItemByCn(bp.getCStructureBudgetPositionRaw()).getId() !=null) {
                                    bItem.setBudgetKindId(0l+field.get().getEnumItemByCn(bp.getCStructureBudgetPositionRaw()).getId());
                                }
                            } else {
                                log.error("cant getn C_STRUCTURE_BUDGET_POSITION_RAW_1 field");
                            }

                        } catch (EntityNotFoundException e) {
                            log.error("CAUGHT " + e.getMessage(), e);
                        }
                    }

                    bItem.setBudgetKoId(bp.getCStructureBudgetKo() != null ? bp.getCStructureBudgetKo().longValue() : null);

                    if (bItem.getBudgetKoId() != null) {
                        positionType = 1;
                        List<UlBudgetsFromUnits> items = DictionaryUtils.findByGivenFieldValue(UlBudgetsFromUnits.class, "erpId", bItem.getBudgetKoId());
                        UlBudgetsFromUnits item = Iterables.getFirst(items, null);
                        Preconditions.checkNotNull(item, "cant find BudgetsFromUnitsImport, erpId"+bItem.getBudgetKoId());
                        bItem.setBudgetDirectCode(item.getBudzetIdm());
                    }

                    // zasob_id
                    if (bp.getCResourceRaw() != null) {
                        try {
                            Optional<pl.compan.docusafe.core.dockinds.field.Field> field = findField(DOC_KIND_CN, BP_FIELD, "C_RESOURCE_RAW_1");

                            if (field.isPresent()) {
                                field.get().getEnumItemByCn(bp.getCResourceRaw());
                                if (field.get().getEnumItemByCn(bp.getCResourceRaw()).getId() !=null) {
                                    bItem.setResourceId(0l+field.get().getEnumItemByCn(bp.getCResourceRaw()).getId());
                                }
                            } else {
                                log.error("cant getn RESOURCE field");
                            }

                        } catch (EntityNotFoundException e) {
                            log.error("CAUGHT " + e.getMessage(), e);
                        }
                    }

                    if (bp.getCFundSource() != null) {
                        FundSource fItem = new FundSource();
                        fItem.setFundPercent(new BigDecimal(100));

                        // zrodlo finansowania
                        // je�li -1, czyli "nie dotyczy" to nie exportuj
                        fItem.setCode(getCnValue(FUND_SOURCES_TABLE_NAME, bp.getCFundSource(), processNotApplicableValue, notApplicableValue));
                        fItem.setKind(positionType);
                        bItem.setFundSources(Lists.newArrayList(fItem));
                    }

                    bItem.setReservationCode(bp.getBReservationCode());
                    bItem.setReservationPositionId(bp.getBReservationPositionId());

                    for (SimpleRepositoryAttribute dict : repositoryDicts) {
                        Long value = getRepositoryAttributeValue(bpId, dict, log);
                        if (value != null) {
                            bItem.addRepositoryItem(new RepositoryItem(dict.getRepAtrybutId(), value));
                        }
                    }

                    i++;
                }
            }
            exported.setBudgetItems(budgetItems);

            Preconditions.checkState(bpAmountSum.compareTo((BigDecimal) fm.getKey(KWOTA_BRUTTO_FIELD)) == 0, "Kwota brutto faktury nie r�wna si� sumie pozycji bud�etowych");
		}
		
		String systemPath = null;

		if (Docusafe.getAdditionProperty("erp.faktura.link_do_skanu") != null) {
			systemPath = Docusafe.getAdditionProperty("erp.faktura.link_do_skanu");
		} else {
			systemPath = Docusafe.getBaseUrl();
		}
		
		for (Attachment zal : doc.getAttachments()) {
			AttachmentInfo info = new AttachmentInfo();
			
			String path = systemPath+ "/repository/view-attachment-revision.do?id=" + zal.getMostRecentRevision().getId();
			
			info.setName(zal.getTitle());
			info.setUrl(path);
			
			exported.addAttachementInfo(info);
		}
		
		return exported;
	}

    /**
     * build description from addition fields
     * @param doc
     * @param fm
     * @return
     * @throws EdmException
     */
    private String getAdditionFieldsDescription(OfficeDocument doc, FieldsManager fm) throws EdmException {
        Map<String,String> additionFields = Maps.newLinkedHashMap();
        additionFields.put("Nr KO",doc.getOfficeNumber() != null ? doc.getOfficeNumber().toString() : "brak");
        additionFields.put("Numer(y) spraw(y) zapotrzebowania", Joiner.on(", ").skipNulls().join(FieldsManagerUtils.getDictionaryItems(fm, "ZAPOTRZEBOWANIE", "ZAPOTRZEBOWANIE_NR_SPRAWY", String.class)));
        additionFields.put("Nazwa towaru/us�ugi",fm.getStringValue("OPIS"));
        additionFields.put("Rodzaj zam�wienia",fm.getStringValue("RODZAJ_ZAMOWIENIA"));
        additionFields.put("Tryb zam�wienia",fm.getStringValue("TRYB_ZAMOWIENIA"));
        additionFields.put("Szczeg�y zam�wienia",fm.getStringValue("SZCZEGOLY_ZAMOWIENIA"));
        additionFields.put("Nr post�powania",fm.getStringValue("POSTEPOWANIE_NR"));
        additionFields.put("Data zawarcia umowy", DateUtils.formatJsDateOrEmptyString((Date) fm.getKey("POSTEPOWANIE_DATA")));
        additionFields.put("Wydatek zakwalifikowany do wydatku strukturalnego",fm.getStringValue("WYDATEK_STRUKTURALNY"));
        additionFields.put("Wydatek w wysoko�ci",fm.getStringValue("WYDATEK_KWOTA"));
        additionFields.put("Kod wydatku strukturalnego",fm.getStringValue("WYDATEK_KOD"));
        additionFields.put("Obszar tematyczny wydatku strukturalnego",fm.getStringValue("WYDATEK_OBSZAR"));
        additionFields.put("Konto ksi�gowe",fm.getStringValue("KONTO_POZABILANSOWE"));
        additionFields.put("Zlecenie",fm.getStringValue("ZLECENIE"));
        additionFields.put("Spos�b zwrotu",fm.getStringValue("SPOSOB_ZWROTU"));
        additionFields.put("Pracownik dla zwrotu",fm.getStringValue("RECIPIENT_HERE"));
        additionFields.put("Imi� / nazwisko / nazwa organizacji dla zwrotu p�atno�ci",fm.getStringValue("NAZWISKO_DLA_ZWROTU"));
        additionFields.put("Nr rach. bankowego dla zwrotu",fm.getStringValue("PRACOWNIK_NR_KONTA"));


        StringBuilder description = new StringBuilder();
        for (String label : additionFields.keySet()) {
            if (!Strings.isNullOrEmpty(additionFields.get(label))) {
                description.append(label)
                        .append(": ")
                        .append(additionFields.get(label))
                        .append("; ");
            }
        }
        return description.toString();
    }

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
		Map<String, Object> toReload = Maps.newHashMap();
		for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
			if (f.getDefaultValue() != null)
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));

		toReload.put("DATA_WPLYWU_FAKTURY", new Date());
		fm.reloadValues(toReload);
	}

	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
		try {
			StringBuilder msgBuilder = new StringBuilder();

            String documentStatusId = DwrUtils.getStringValue(values, "DWR_STATUS");
			
			if ((BUDGET_ACCEPT_STATUS_ID.equals(documentStatusId) || BUDGET_ACCEPT_ADDITIONAL_STATUS_ID.equals(documentStatusId))&& DwrUtils.isNotNull(values, "DWR_KWOTA_WALUTA") && values.get("DWR_" + DATA_OBOWIAZYWANIA_KURSU_FIELD) != null && values.get("DWR_" + DATA_OBOWIAZYWANIA_KURSU_FIELD).getDateData() != null && values.get(DWR_PREFIX + KWOTA_KURS_TABELA_ERP_FIELD) != null) {
                DocumentCacheHandler.CurrencyRate currencyRate = getCurrencyParam(values.get("DWR_KWOTA_WALUTA").getData(), values.get("DWR_" + DATA_OBOWIAZYWANIA_KURSU_FIELD).getDateData());

                values.get(DWR_PREFIX + KWOTA_KURS_FIELD).setMoneyData(currencyRate.getRateWithPlain());
                values.get(DWR_PREFIX + KWOTA_KURS_TABELA_ERP_FIELD).setStringData(currencyRate.getRateTableCodeWithPlain());
            }
			
			if (values.get(BP_DWR_FIELD) != null && values.get(KWOTA_POZOSTALA_MPK_DWR_FIELD) != null && values.get(KWOTA_BRUTTO_DWR_FIELD) != null) {
				if (values.get(BP_DWR_FIELD).getType().equals(Field.Type.DICTIONARY) && values.get(BP_DWR_FIELD).getDictionaryData() != null) {
					setRemainingAmount(values,msgBuilder);
				}
			}
			
			if (values.get(KWOTA_NETTO_DWR_FIELD) != null) {
				values.get(KWOTA_NETTO_DWR_FIELD).setMoneyData(DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.MULTIPLY, 2, true, values.get(KWOTA_WALUTOWA_DWR_FIELD),values.get(KWOTA_KURS_DWR_FIELD)));
			}
			if (values.get(KWOTA_BRUTTO_DWR_FIELD) != null) {
				values.get(KWOTA_BRUTTO_DWR_FIELD).setMoneyData(DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.ADD, 2, false, values.get(KWOTA_NETTO_DWR_FIELD),values.get(KWOTA_VAT_DWR_FIELD)));
			}

  /*          if (DwrUtils.isSenderField(values, "DWR_GENERUJ_POZYCJE")) {
                generatePositionFromReservation(values, fm, msgBuilder);
            }*/
			
			// data platnosci nie moze byc wczesniej niz data wystawienia
            if (values.get(DWR_PREFIX+PRZEDPLATA_FIELD) == null || !Boolean.TRUE.equals(values.get(DWR_PREFIX+PRZEDPLATA_FIELD).getBooleanData())) {
			    msgBuilder.append(validateDate(values.get(DATA_WYSTAWIENIA_DWR_FIELD), values.get(TERMIN_PLATNOSCI_DWR_FIELD), DATA_PLATNOSCI_ERROR_MSG));
            }

			// data wp�ywu nie mo�e by� wcze�niejsza ni� data wystawienia
			msgBuilder.append(validateDate(values.get(DATA_WYSTAWIENIA_DWR_FIELD), values.get(DATA_WPLYWU_DWR_FIELD), DATA_WPLYWU_ERROR_MSG));
			
			// data wp�ywu nie mo�e by� wcze�niejsza ni� data rejestracji
			//msgBuilder.append(validateDate(values.get(DATA_WPLYWU_DWR_FIELD), new Date(), DATA_WPLYWU_TERAZ_ERROR_MSG));
			
			if (msgBuilder.length()>0)
			{
				values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
				return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
			}
		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}
		return null;
	}

    @Override
    public String onAlternativeUpdate(Document document, Map<String, Object> values, String activity) throws EdmException {
        StringBuilder sb = new StringBuilder();
        FieldsManager fm = document.getFieldsManager();
        List<Long> createdBps = generatePositionFromReservation(fm, sb);
        Integer docTypeToSet = fm.getKey("TYP_FAKTURY") != null ? fm.getIntegerKey("TYP_FAKTURY") : document.getDocumentKind().getFieldByCn("TYP_FAKTURY").getEnumItemByCn("FZP").getId();
        Map<String, Object> toSet = Maps.newHashMap();
        toSet.put(BP_FIELD, createdBps);
        toSet.put("TYP_FAKTURY", docTypeToSet);
        document.getDocumentKind().setOnly(document.getId(), toSet);

        return sb.length() == 0 ? "Pod��czono pozycje wniosku" : sb.toString();
    }

    private List<Long> generatePositionFromReservation(/*Map<String, FieldData> values, */FieldsManager fm, StringBuilder msgBuilder) throws EdmException {
        List<Long> createdBp = Lists.newArrayList();
        try {
            List<Number> orderDocumentIds = (List<Number>) fm.getKey("ZAPOTRZEBOWANIE");
            //List<Number> orderDocumentIds = DwrUtils.getValueListFromDictionary(values, "ZAPOTRZEBOWANIE", true, "ID");

            UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
            for (Number documentId : orderDocumentIds) {
                FieldsManager orderFm = Document.find(documentId.longValue()).getFieldsManager();
                List<Number> orderPositions = (List<Number>) orderFm.getKey(BP_FIELD);
                String erpDocumentIdm = orderFm.getStringKey("ERP_DOCUMENT_IDM");

                Preconditions.checkState(!Strings.isNullOrEmpty(erpDocumentIdm), "Brak IDM wniosku rezerwacji");

                for (Number bpId : orderPositions) {
                    UlBudgetPosition bp = bpInstance.find(bpId.longValue());

                    Long erpPositionId = getErpPositionId(bp, erpDocumentIdm);

                    UlBudgetPosition newBp = new UlBudgetPosition();
                    newBp.setCKind(1620);
                    newBp.setSKind(1);

                    newBp.setCMpk(bp.getCMpk());
                    newBp.setCFundSource(bp.getCFundSource());
                    newBp.setCType(bp.getCType());

                    newBp.setCProject(bp.getCProject());

                    if (newBp.getCProject() != null) {
                        newBp.setIGroup(DataBaseEnumField.getEnumFiledForTable(GRUPY_DZIALALNOSCI_TABLE_NAME).getEnumItemByCn("50X").getId());
                        newBp.setIKind(DataBaseEnumField.getEnumFiledForTable(RODZAJ_DZIALALNOSCI_TABLE_NAME).getEnumItemByCn("501").getId());
                    }

                    newBp.setCBudget(bp.getCBudget());
                    newBp.setCPosition(bp.getCPosition());
                    newBp.setCResourceRaw(bp.getCResourceRaw());

                    newBp.setCUser(bp.getCUser());
                    newBp.setCStructureBudgetKo(bp.getCStructureBudgetKo());
                    newBp.setCStructureBudgetPositionRaw(bp.getCStructureBudgetPositionRaw());

                    if (newBp.getCStructureBudgetKo() != null) {
                        newBp.setIGroup(DataBaseEnumField.getEnumFiledForTable(GRUPY_DZIALALNOSCI_TABLE_NAME).getEnumItemByCn("55X").getId());
                        newBp.setIKind(DataBaseEnumField.getEnumFiledForTable(RODZAJ_DZIALALNOSCI_TABLE_NAME).getEnumItemByCn("551").getId());
                    }

                    newBp.setBReservationCode(erpDocumentIdm);
                    newBp.setBReservationPositionId(erpPositionId);

                    newBp.save();

                    createdBp.add(newBp.getId());
                }
            }

            //values.get(BP_DWR_FIELD).setStringData(Joiner.on(',').skipNulls().join(createdBp));
        } catch (Exception e) {
            msgBuilder.append(e.getMessage()).append("; ");
            log.error(e.getMessage(), e);
        } finally {
            //DSApi._close();
        }

        return createdBp;
    }

    private Long getErpPositionId(UlBudgetPosition bp, String reservationCode) throws SQLException {

        PreparedStatement ps = null;
        ResultSet rs;
        try {
            String GET_POSITIONS_FROM_ERP = "select p.bd_rezerwacja_poz_id, p.nrpoz\n" +
                    "\n" +
                    "from bd_rezerwacja_poz p with(nolock)\n" +
                    "\n" +
                    "left join bd_rezerwacja b with(nolock) on p.bd_rezerwacja_id = b.bd_rezerwacja_id\n" +
                    "\n" +
                    "where b.bd_rezerwacja_idm = ?";

            Connection connection = DriverManager.getConnection(
                    "jdbc:jtds:sqlserver://172.18.9.230/simpleERP", "Admin", "Simple123");
            ps = connection.prepareStatement(Docusafe.getAdditionPropertyOrDefault("export.get_reservation_positions", GET_POSITIONS_FROM_ERP));
            ps.setString(1, reservationCode);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getLong(1);
            }
        } finally {
            DbUtils.closeQuietly(ps);
        }

        return null;
    }

    protected DocumentCacheHandler.CurrencyRate getCurrencyParam(Object currencyRawId, Date currencyDate) throws EdmException {
        DocumentCacheHandler.CurrencyRate currencyRate = DocumentCacheHandler.CurrencyRate.newPlain();

        try {
            Integer currencyId = Ints.tryParse(currencyRawId.toString());
            currencyDate.setTime(currencyDate.getTime()+DateUtils.HOUR*12);

            if (currencyId != null) {
                currencyRate = DocumentCacheHandler.findCurrencyRate(currencyId, currencyDate);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return currencyRate;
    }

//		column.dockindBusinessAtr3 = Pozcyja bud\u017cetowa: projekt
//		column.dockindBusinessAtr4 = Pozcyja bud\u017cetowa: kwota
	@Override
	public TaskListParams getTaskListParams(DocumentKind kind, long documentId, TaskSnapshot task) throws EdmException {
		TaskListParams params = new TaskListParams();
		
		List<String> mpkAcceptationNames = new ArrayList<String>();
		mpkAcceptationNames.add(BUDGET_OWNER_ACCEPT_TASK_NAME);
		String taskId = task.getActivityKey();
		
		FieldsManager fm = Documents.document(documentId).getFieldsManager();
		
		params.setAttribute("Nie dotyczy", 2);
		params.setAttribute("Nie dotyczy", 3);
		
		UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
		
		if (!mpkAcceptationNames.contains(Jbpm4Provider.getInstance().getTaskService().getTask(taskId).getName())) {
			if (fm != null && fm.getKey(BP_FIELD) != null) {
				Object mpkRaw = fm.getKey(BP_FIELD);
				if (mpkRaw != null && mpkRaw instanceof List<?> && ((List<?>)mpkRaw).size()>0) {
					try {
						String mpkRawId = ((List<?>)mpkRaw).get(0).toString();
						Long firstMpkId = Long.valueOf(mpkRawId);


						UlBudgetPosition bp = bpInstance.find(firstMpkId);

						if (bp != null && bp.getCProject() != null) {
							EnumItem field = DataBaseEnumField.getEnumItemForTable(PROJECTS_TABLE_NAME, bp.getCProject());

							String postfix = ((List<?>) mpkRaw).size() > 1 ? ", ..." : "";

							params.setAttribute(field.getTitle() + postfix, 2);
							params.setAttribute(bp.getIAmount().toString() + postfix, 3);
						}
					} catch (NumberFormatException e) {
						log.error(e.getMessage(),e);
					} catch (EdmException e1) {
						log.error(e1.getMessage(),e1);
					}
				}
			}
		} else {
			String mpkRawId = Jbpm4Provider.getInstance().getTaskService().getVariable(taskId, BP_VARIABLE_NAME) != null ? 
					Jbpm4Provider.getInstance().getTaskService().getVariable(taskId, BP_VARIABLE_NAME).toString() : "";
			if (!mpkRawId.equals("")) {
				try {
					Long bpId = Long.valueOf(mpkRawId);
					UlBudgetPosition bp = bpInstance.find(bpId);

					EnumItem field = DataBaseEnumField.getEnumItemForTable(PROJECTS_TABLE_NAME, Integer.valueOf(bp.getCProject()));

					params.setAttribute(field.getTitle(), 2);
					params.setAttribute(bp.getIAmount().toString(), 3);

				} catch (EdmException e) {
					log.error(e.getMessage(),e);
				} catch (NumberFormatException e1) {
					log.error(e1.getMessage(),e1);
				}
			}
		}
		// Status
		params.setStatus((String) fm.getValue("STATUS"));
		
		// Kategoria - rodzaj faktury
		params.setCategory((String) fm.getValue("RODZAJ"));
		
		if (Strings.isNullOrEmpty(task.getRealDescription())) {
			task.setDescription("Brak");
		}

		try {
			Object amount = Objects.firstNonNull(fm.getKey("KWOTA_BRUTTO"), fm.getKey("KWOTA_WALUTOWA"));
			params.setAmount(new BigDecimal(amount.toString()));
		} catch (Exception e1) {
			log.error("B��d przy ustawianiu kolumny kwota, listy zada�");
			log.error(e1.getMessage(),e1);
		}
		
		// Nr faktury jako nr dokumentu
		params.setDocumentNumber((String) fm.getValue("NR_FAKTURY"));

        // Termin platnosci
        try {
            params.setDocumentDate((Date) fm.getValue("TERMIN_PLATNOSCI"));
        } catch (Exception e) {
            log.error("B��d przy ustawianiu kolumny Termin p�atno�ci, listy zada�");
        }

        return params;
	}

	@Override
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) {
		Map<String, Object> results = new HashMap<String, Object>();

		if (dictionaryName.equals(STAWKI_VAT_FIELD_NAME)) {
			calcStawkiVat(STAWKI_VAT_FIELD_NAME, values, results);
		}

		try {
			if (dictionaryName.equals(BP_FIELD)) {
				//setBudgetItemsDynamicDicts(dictionaryName, values, results);
				//setBudgetItemsRefValues(dictionaryName, values, results);
				setDictionaries(dictionaryName, values, results);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		return results;
	}

	private void setDictionaries(String dictionaryName, Map<String, FieldData> values, Map<String, Object> results) throws EdmException {
		String dicNameWithSuffix = dictionaryName + "_";
		Integer docTypeId = getDocTypeId();
        Integer docTypeObjectId = getDocTypeObjectId(docTypeId);
		
			//simple,simple,simple....!
			//Dictionary controlDict = CacheHandler.getControlDict();

			//if (controlDict != null) {

				clearAllDictionaresFields(DOC_KIND_CN, results);
	
//					Long controlDictId;
//					try {
//						controlDictId = Long.valueOf(controlDict.getCode());
//					} catch (NumberFormatException e) {
//						controlDictId = 187l;
//					}
	
//	
//					SimpleRepositoryAttribute controlDictRepository = CacheHandler.getRepostioryDict(controlDictId, docTypeObjectId);
//	
//					if (controlDictRepository != null) {
//						Long refValue = controlDictRepository.getRepAtrybutId();
//	
//						DwrUtils.setRefValueEnumOptions(values, results, REPO_REF_FIELD_PREFIX + controlDict.getCn(), dicNameWithSuffix, controlDict.getCn(),
//								controlDict.getTablename(), refValue.toString(), EMPTY_NOT_HIDDEN_ENUM_VALUE);
//	
//						String controlDictCnFromResults = BP_FIELD_WITH_LINK + controlDict.getCn();

						DwrUtils.setRefValueEnumOptions(values, results, "C_KIND", BP_FIELD_WITH_LINK, "I_GROUP", GRUPY_DZIALALNOSCI_TABLE_NAME, null, EMPTY_ENUM_VALUE);
						DwrUtils.setRefValueEnumOptions(values, results, "I_GROUP", BP_FIELD_WITH_LINK, RODZAJ_DZIALALNOSCI_FIELD_NAME, RODZAJ_DZIALALNOSCI_TABLE_NAME, null, EMPTY_ENUM_VALUE);
						DwrUtils.setRefValueEnumOptions(values, results, "I_GROUP", BP_FIELD_WITH_LINK, TYPY_DZIALALNOSCI_FIELD_NAME, TYPY_DZIALALNOSCI_TABLE_NAME, null, EMPTY_ENUM_VALUE);					
					
						String controlDictCnFromResults = BP_FIELD_WITH_LINK+"I_KIND";
						
						if (results.get(controlDictCnFromResults) != null && results.get(controlDictCnFromResults) instanceof EnumValues) {
							String selectedId = ((EnumValues) results.get(controlDictCnFromResults)).getSelectedId();
	
							if (StringUtils.isNotEmpty(selectedId)) {
								Set<String> repositoryDictsIds = docTypeObjectId != null ? getRepositoryDictsIds(docTypeObjectId, (int)INVOICE_POSITION_CONTEXT_ID) : null;

								Integer rodzajId = Integer.valueOf(selectedId);
								//EnumItem rodzajEnum = DataBaseEnumField.getEnumItemForTable(controlDict.getTablename(), rodzajId);
								//Integer controlValue = rodzajEnum.getCentrum();
	
								EnumItem rodzajEnum = DataBaseEnumField.getEnumItemForTable(RODZAJ_DZIALALNOSCI_TABLE_NAME, rodzajId);
								String controlValue = rodzajEnum.getCn();
								
								if (controlValue != null) {
									List<DictionaryMap> dictToShow = CacheHandler.getDictionariesMappings(controlValue, DOC_KIND_CN);
									
									for (DictionaryMap dictMap : dictToShow) {
										EnumValues defaultEnum = setDefaultEmptyEnum(dictMap);
										
										Dictionary dict = dictMap.getDictionary();
										if (dict.getDeleted() == false) {
											if ("C_TYPE".equals(dict.getCn())) {
												if (isFixedAsset(values)) {
													continue;
												}
											}
											
											if ("C_USER".equals(dict.getCn())) {
												if (!isBudgetOwnerAccept(values)) {
													continue;
												}
											}
											
											if (dict.getDictSource() == SourceType.REPOSITORY.value && docTypeObjectId != null) {
												setRepositoryDict(values, results, dicNameWithSuffix, docTypeObjectId, (int)INVOICE_POSITION_CONTEXT_ID, repositoryDictsIds,
														dictMap, defaultEnum, dict, log);
											}
											if (dict.getDictSource() == SourceType.SERVICE.value || dict.getDictSource() == SourceType.MANUAL.value) {
												setServiceDict(values, results, dicNameWithSuffix, dictMap, defaultEnum, dict);
											}

                                            if (dict.getDictSource() == SourceType.SERVICE_ADHOC.value) {
                                                setServiceAdhocDict(values, results, dictionaryName, dictMap, defaultEnum, dict);
                                            }
										}
									}
								}
							}
						}
					//}
			//}
	}

    private void setServiceAdhocDict(Map<String, FieldData> values, Map<String, Object> results, String dictionaryCn, DictionaryMap dictMap, EnumValues defaultEnum, Dictionary dict) {
        reloadRefEnumItems(values, results, dictionaryCn, dict.getCn(), dict.getRefDict().getCn(), dict.getDockindCn(), defaultEnum);
    }

    private Integer getDocTypeObjectId(Integer docTypeId) {
        try {
            if (docTypeId != null) {
                EnumItem docTypeEnum = DataBaseEnumField.getEnumItemForTable(DOCUMENT_TYPES_TABLE_NAME, docTypeId);

                if (docTypeEnum != null) {
                    return docTypeEnum.getCentrum();
                }
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    private boolean isFixedAsset(Map<String, FieldData> values) {
		try {
			String purchaseTypeId = values.get(BP_FIELD_WITH_LINK+"C_KIND").getEnumValuesData().getSelectedOptions().get(0);
			if (SRODEK_TRWALY_ENUM_ID.equals(purchaseTypeId)) {
				return true;
			}
		} catch (Exception e) {
			log.error("CAUGHT: "+e.getMessage(), e);
		}
		return false;
	}

	private void setServiceDict(Map<String, FieldData> values, Map<String, Object> results, String dicNameWithSuffix,
			DictionaryMap dictMap, EnumValues defaultEnum, Dictionary dict) {
		if (dict.getRefDict() == null) {
			DataBaseEnumField base = DataBaseEnumField.tableToField.get(dict.getTablename()).iterator().next();
			results.put(dicNameWithSuffix + dict.getCn(), base.getEnumValuesForForcedPush(values, dicNameWithSuffix));
		} else {
			if (dictMap.isShowAllReferenced()) {
				DwrUtils.setRefValueEnumOptions(values, results, dict.getRefDict().getCn(), dicNameWithSuffix, dict.getCn(), dict.getTablename(), DataBaseEnumField.REF_ID_FOR_ALL_FIELD, defaultEnum);
			} else {
				DwrUtils.setRefValueEnumOptions(values, results, dict.getRefDict().getCn(), dicNameWithSuffix, dict.getCn(), dict.getTablename(), null, defaultEnum);
			}
		}
	}

    private EnumValues setDefaultEmptyEnum(DictionaryMap dictMap) {
		EnumValues defaultEnum = dictMap.isMustShow() ? EMPTY_NOT_HIDDEN_ENUM_VALUE : EMPTY_ENUM_VALUE;
		return defaultEnum;
	}

    private void clearAllDictionaresFields(String dockindCn, Map<String, Object> results) {
		for (Dictionary dictionaryToClear : CacheHandler.getAvailableDicts(dockindCn)) {
			results.put(BP_FIELD_WITH_LINK+dictionaryToClear.getCn(), EMPTY_ENUM_VALUE);
		}
	}

	private void setBudgetItemsDynamicDicts(String dictionaryName, Map<String, FieldData> values, Map<String, Object> results) throws EdmException {
		String dic = dictionaryName+"_";
		Integer docTypeId = getDocTypeId();
		
		if (docTypeId != null) {
			EnumItem docTypeEnum = DataBaseEnumField.getEnumItemForTable(DOCUMENT_TYPES_TABLE_NAME, docTypeId);
			if (docTypeEnum != null) {
				Integer docTypeObjectId = docTypeEnum.getCentrum();
				loadDynamicDicts(values, results, dic, docTypeObjectId);
			}
		}
	}

	private Integer getDocTypeId() {
		WebContext dwrFactory = WebContextFactory.get();
		if (dwrFactory.getSession().getAttribute("dwrSession") instanceof Map<?, ?>) {
			Map<String, Object> dwrSession = (Map<String, Object>) dwrFactory.getSession().getAttribute("dwrSession");
			if (dwrSession.get("FIELDS_VALUES") instanceof Map<?, ?>) {
				Map<String,Object> fieldsValues = (Map<String, Object>) dwrSession.get("FIELDS_VALUES");
				
				Object docType = fieldsValues.get("TYP_FAKTURY");
				if (docType != null) {
					try {
						return Integer.valueOf(docType.toString());
					} catch (NumberFormatException e) {
					}
				}
			}
		}
		return null;
	}

	private void loadDynamicDicts(Map<String, FieldData> values, Map<String, Object> results, String dic, Integer docTypeObjectId) throws EdmException {
		List<SimpleRepositoryAttribute> allDicts = SimpleRepositoryAttribute.getInstance().findUsed(true);
		
		for (SimpleRepositoryAttribute dict : allDicts) {
			boolean show = dict.getObjectId().equals(docTypeObjectId);
			putToEnumWithLoadCheck(values, results, dic, dict.getTableName().toUpperCase(Locale.ENGLISH), dict.getTableName(), show);
		}
	}

	private boolean isBudgetOwnerAccept(Map<String, FieldData> values) {
		try {
			String itemGroup = values.get(BP_FIELD_WITH_LINK + "I_GROUP").getEnumValuesData().getSelectedOptions().get(0);
			
			if (StringUtils.isNotBlank(itemGroup) && StringUtils.isNumeric(itemGroup)) {
				Integer itemGroupId = Integer.valueOf(itemGroup);
				EnumItem item = DataBaseEnumField.getEnumItemForTable(GRUPY_DZIALALNOSCI_TABLE_NAME, itemGroupId);
				if (SOCIAL_ITEM_GROUP_CNS.contains(item.getCn())) {
					return false;
				}
			}
		} catch (NullPointerException e) {
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
		}
		try {
			String purchaseTypeId = values.get(BP_FIELD_WITH_LINK + "S_KIND").getEnumValuesData().getSelectedOptions().get(0);
			if (SOCIAL_ID_FOR_NONE_BUDGET_OWNER_ACCEPT.contains(purchaseTypeId)) {
				return false;
			}
		} catch (NullPointerException e) {
		}
		return true;
	}
	
	protected void setBudgetItemsRefValues(String dictionaryName, Map<String, FieldData> values, Map<String, Object> results) throws EntityNotFoundException, EdmException {
		DwrUtils.setRefValueEnumOptions(values, results, "C_KIND", BP_FIELD_WITH_LINK, "I_GROUP", GRUPY_DZIALALNOSCI_TABLE_NAME, null);
		DwrUtils.setRefValueEnumOptions(values, results, "I_GROUP", BP_FIELD_WITH_LINK, RODZAJ_DZIALALNOSCI_FIELD_NAME, RODZAJ_DZIALALNOSCI_TABLE_NAME, null);
		DwrUtils.setRefValueEnumOptions(values, results, "I_GROUP", BP_FIELD_WITH_LINK, TYPY_DZIALALNOSCI_FIELD_NAME, TYPY_DZIALALNOSCI_TABLE_NAME, null);
		
		boolean budgetPositionOwnerAccept = isBudgetOwnerAccept(values);
		
		String dic = dictionaryName+"_";
		
		String purchaseTypeId = getPurchaseTypeAndSetCostTypeEnum(values, results);
		
		if (results.get(BP_FIELD_WITH_LINK+"I_KIND") != null && results.get(BP_FIELD_WITH_LINK+"I_KIND") instanceof EnumValues) {
			String selectedId = ((EnumValues) results.get(BP_FIELD_WITH_LINK+"I_KIND")).getSelectedId();
			if (StringUtils.isNotEmpty(selectedId)) {
				Integer grupaId = Integer.valueOf(selectedId);
				EnumItem rodzajEnum = DataBaseEnumField.getEnumItemForTable(RODZAJ_DZIALALNOSCI_TABLE_NAME, grupaId);
				if (rodzajEnum != null) {
					String rodzajCn = rodzajEnum.getCn();
					CostInvoiceDzialalnosc map = CostInvoiceDzialalnosc.findByCode(rodzajCn);
					if (map != null) {
						putToEnumWithLoadCheck(values, results, dic, "C_CAR", "dsg_ul_samochody", map.getSamochody());
						putToEnumWithLoadCheck(values, results, dic, "C_MT08", MT08_TABLE_NAME, map.getGrupyMt08());
						putToEnumWithLoadCheck(values, results, dic, "C_INVENTORY", NUMER_INWENTARZOWY_TABLE_NAME, map.getNumerInwentarzowy());
						putToEnumWithLoadCheck(values, results, dic, "C_ORDER", ZLECENIA_TABLE_NAME, map.getNumerZlecenia());
						
						//SOCIAL
						putToEnumWithLoadCheck(values, results, dic, "S_ZFSS_WYDATKI", "dsg_ul_zfss_wydatki", map.getZfssWydatki());
						putToEnumWithLoadCheck(values, results, dic, "S_FPMSID_852_20", "dsg_ul_fpmsid_852_1_20", map.getFpmsid_852_1_20());
						putToEnumWithLoadCheck(values, results, dic, "S_FPMSID_852", "dsg_ul_fpmsid_852", map.getFpmsid_852());
						putToEnumWithLoadCheck(values, results, dic, "S_FPMSID_GRUPA_KOSZTU_DS", "dsg_ul_fpmsid_grupa_kosztu_ds", map.getFpmsidGrupaKosztuDs());
						putToEnumWithLoadCheck(values, results, dic, "S_FPMSID_TYP_POWIERZCHNI", "dsg_ul_fpmsid_typ_powierzchni", map.getFpmsidTypPowierzchni());
						putToEnumWithLoadCheck(values, results, dic, "S_FPMSID_GRUPA_KOSZTU_00", "dsg_ul_fpmsid_grupa_kosztu_00", map.getFpmsidGrupaKosztu_00());
						putToEnumWithLoadCheck(values, results, dic, "S_FPMSID_TYP_POWIERZCHNI_DS852", "dsg_ul_fpmsid_typ_powierzchni_ds852", map.getFpmsidTypPowierzchni_ds852());
						putToEnumWithLoadCheck(values, results, dic, "S_FPMSID_GRUPA_KOSZTU_STOLOWKI", "dsg_ul_fpmsid_grupa_kosztu_stolowki", map.getFpmsidGrupaKosztuStolowki());
						putToEnumWithLoadCheck(values, results, dic, "S_FPMSID_TYP_POWIERZ_STOLOWKI", "dsg_ul_fpmsid_typ_powierz_stolowki", map.getFpmsidTypPowierzStolowki());
						putToEnumWithLoadCheck(values, results, dic, "S_KOSZTY_RODZAJOWE_ZESPOL_8", "dsg_ul_koszty_rodzajowe_zespol_8", map.getKosztyRodzajoweZespol_8());
						
						//ZRODLO FINANSOWANIA
						if (!map.getZrodloFinansowania()) {
							results.put(BP_FIELD_WITH_LINK+"C_FUND_SOURCE", EMPTY_ENUM_VALUE);
						} else {
							if (!map.getNumerProjektu()) {
								DwrUtils.setRefValueEnumOptions(values, results, "C_RESOURCE", dic, "C_FUND_SOURCE", FUND_SOURCES_TABLE_NAME, DataBaseEnumField.REF_ID_FOR_ALL_FIELD);
							} else {
								DwrUtils.setRefValueEnumOptions(values, results, "C_RESOURCE", dic, "C_FUND_SOURCE", FUND_SOURCES_TABLE_NAME, null, EMPTY_ENUM_VALUE);
							}
						}
						
						//RODZAJ KOSZTU
						if (!map.getRodzajKosztu() || SRODEK_TRWALY_ENUM_ID.equals(purchaseTypeId)) {
							results.put(BP_FIELD_WITH_LINK+"C_TYPE", EMPTY_ENUM_VALUE);
						} else {
							DataBaseEnumField base = DataBaseEnumField.tableToField.get(COST_KIND_TABLE_NAME).iterator().next();
							results.put(dic + "C_TYPE", base.getEnumValuesForForcedPush(values, dic));
						}
						
						//PROJEKT
						if (!map.getNumerProjektu()) {
							results.put(BP_FIELD_WITH_LINK+"C_PROJECT", EMPTY_ENUM_VALUE);
							results.put(BP_FIELD_WITH_LINK+"C_BUDGET", EMPTY_ENUM_VALUE);
							results.put(BP_FIELD_WITH_LINK+"C_POSITION", EMPTY_ENUM_VALUE);
							results.put(BP_FIELD_WITH_LINK+"C_RESOURCE", EMPTY_ENUM_VALUE);
						} else {
							if (budgetPositionOwnerAccept) {
								if (map.getSlownikKomorek()) {
									DwrUtils.setRefValueEnumOptions(values, results, "C_MPK", dic, "C_PROJECT", PROJECTS_TABLE_NAME, null, EMPTY_NOT_HIDDEN_ENUM_VALUE);
								} else {
									DataBaseEnumField base = DataBaseEnumField.tableToField.get(PROJECTS_TABLE_NAME).iterator().next();
									results.put(dic + "C_PROJECT", base.getEnumValuesForForcedPush(values, dic));
								}
							}
							DwrUtils.setRefValueEnumOptions(values, results, "C_PROJECT", BP_FIELD_WITH_LINK, "C_BUDGET", PROJECT_BUDGETS_TABLE_NAME, null);
							DwrUtils.setRefValueEnumOptions(values, results, "C_BUDGET", BP_FIELD_WITH_LINK, "C_POSITION", PROJECT_BUDGET_POSITIONS_TABLE_NAME, null);
							DwrUtils.setRefValueEnumOptions(values, results, "C_POSITION", BP_FIELD_WITH_LINK, "C_RESOURCE", PROJECT_BUDGET_POSITION_RESOURCES_TABLE_NAME, null);
						}
						
						//JEDNOSTKA (MPK)
						if (!map.getSlownikKomorek()) {
							results.put(BP_FIELD_WITH_LINK+"C_MPK", NOT_APPLICABLE_ENUM_VALUE);
							if (budgetPositionOwnerAccept && !map.getNumerProjektu()) {
								DwrUtils.setRefValueEnumOptions(values, results, "C_MPK", dictionaryName + "_", "C_USER", UPOWAZNIONY_TABLE_NAME, "0");
							}
						} else {
							DataBaseEnumField base = DataBaseEnumField.tableToField.get(ORGANIZATIONAL_UNIT_TABLE_NAME).iterator().next();
							results.put(dictionaryName + "_C_MPK", base.getEnumValuesForForcedPush(values, dictionaryName + "_"));
							if (budgetPositionOwnerAccept && !map.getNumerProjektu()) {
								DwrUtils.setRefValueEnumOptions(values, results, "C_MPK", dictionaryName + "_", "C_USER", UPOWAZNIONY_TABLE_NAME, null, EMPTY_NOT_HIDDEN_ENUM_VALUE);
							} else {
								results.put(BP_FIELD_WITH_LINK+"C_USER", EMPTY_ENUM_VALUE);
							}
						}
					}
				}
			} else {
				putEmptyValues(results);
			}
		}
	}

	protected String getPurchaseTypeAndSetCostTypeEnum(Map<String, FieldData> values, Map<String, Object> results) {
		String purchaseTypeId = "";
		try {
			purchaseTypeId = values.get(BP_FIELD_WITH_LINK+"C_KIND").getEnumValuesData().getSelectedOptions().get(0);
			if (SRODEK_TRWALY_ENUM_ID.equals(purchaseTypeId)) {
				results.put(BP_FIELD_WITH_LINK+"C_TYPE", EMPTY_ENUM_VALUE);
			}
		} catch (Exception e) {
			log.error("CAUGHT: "+e.getMessage(), e);
		}
		return purchaseTypeId;
	}

	private void putEmptyValues(Map<String, Object> results) {
		results.put(BP_FIELD_WITH_LINK+"I_TYPE", EMPTY_ENUM_VALUE);
		results.put(BP_FIELD_WITH_LINK+"C_POSITION", EMPTY_ENUM_VALUE);
		results.put(BP_FIELD_WITH_LINK+"C_MPK", EMPTY_ENUM_VALUE);
		results.put(BP_FIELD_WITH_LINK+"C_USER", EMPTY_ENUM_VALUE);
		results.put(BP_FIELD_WITH_LINK+"C_PROJECT", EMPTY_ENUM_VALUE);
		results.put(BP_FIELD_WITH_LINK+"C_BUDGET", EMPTY_ENUM_VALUE);
		results.put(BP_FIELD_WITH_LINK+"C_RESOURCE", EMPTY_ENUM_VALUE);
		results.put(BP_FIELD_WITH_LINK+"C_FUND_SOURCE", EMPTY_ENUM_VALUE);
		results.put(BP_FIELD_WITH_LINK+"C_TYPE", EMPTY_ENUM_VALUE);
		results.put(BP_FIELD_WITH_LINK+"C_MT08", EMPTY_ENUM_VALUE);
		results.put(BP_FIELD_WITH_LINK+"C_INVENTORY", EMPTY_ENUM_VALUE);
		results.put(BP_FIELD_WITH_LINK+"C_ORDER", EMPTY_ENUM_VALUE);
//		results.put(BP_FIELD_WITH_LINK+"C_GROUP", EMPTY_ENUM_VALUE);
		results.put(BP_FIELD_WITH_LINK+"C_CAR", EMPTY_ENUM_VALUE);
		
		//SOCIAL
		results.put(BP_FIELD_WITH_LINK+"S_ZFSS_WYDATKI", EMPTY_ENUM_VALUE);
		results.put(BP_FIELD_WITH_LINK+"S_FPMSID_852_20", EMPTY_ENUM_VALUE);
		results.put(BP_FIELD_WITH_LINK+"S_FPMSID_852", EMPTY_ENUM_VALUE);
		results.put(BP_FIELD_WITH_LINK+"S_FPMSID_GRUPA_KOSZTU_DS", EMPTY_ENUM_VALUE);
		results.put(BP_FIELD_WITH_LINK+"S_FPMSID_TYP_POWIERZCHNI", EMPTY_ENUM_VALUE);
		results.put(BP_FIELD_WITH_LINK+"S_FPMSID_GRUPA_KOSZTU_00", EMPTY_ENUM_VALUE);
		results.put(BP_FIELD_WITH_LINK+"S_FPMSID_TYP_POWIERZCHNI_DS852", EMPTY_ENUM_VALUE);
		results.put(BP_FIELD_WITH_LINK+"S_FPMSID_GRUPA_KOSZTU_STOLOWKI", EMPTY_ENUM_VALUE);
		results.put(BP_FIELD_WITH_LINK+"S_FPMSID_TYP_POWIERZ_STOLOWKI", EMPTY_ENUM_VALUE);
		results.put(BP_FIELD_WITH_LINK+"S_KOSZTY_RODZAJOWE_ZESPOL_8", EMPTY_ENUM_VALUE);
	}

	private void putToEnumWithLoadCheck(Map<String, FieldData> values, Map<String, Object> results, String dicNameWithPostFix, String fieldName, String tableName, boolean load) {
		if (!load) {
			results.put(BP_FIELD_WITH_LINK+fieldName, new EnumValues(new HashMap<String,String>(), new ArrayList<String>()));
		} else {
			DataBaseEnumField base = DataBaseEnumField.tableToField.get(tableName).iterator().next();
			results.put(dicNameWithPostFix + fieldName, base.getEnumValuesForForcedPush(values, dicNameWithPostFix));
		}
	}
	
	@Override 
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values, Map<Class, Format> defaultFormatters){
		try{
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setDecimalSeparator(',');
            defaultFormatters.put(BigDecimal.class, new DecimalFormat("### ### ##0.00", symbols));
            defaultFormatters.put(Date.class, new SimpleDateFormat("dd-MM-yyyy"));
            defaultFormatters.put(Timestamp.class, new SimpleDateFormat("dd-MM-yyyy"));



            OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
            CostInvoiceTemplateHelper builder = new CostInvoiceTemplateHelper(fm, doc, log);
            values.put("BP", builder.generateBudgetPositionItems());
            values.put("ACC", builder.generateAcceptInfo());
            values.put("akceptacje", builder.generateAcceptances());
            values.put("CONTRACTOR", builder.generateContractorInfo());
            values.put(GENERATE_DATE_TEMPLATE_VAR, new Date());

        }catch(Exception e){
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
		if (BUDGET_ACCEPTANCE_TASK_NAME.equals(acceptationCn) || BUDGET_ACCEPTANCE_ADD_TASK_NAME.equals(acceptationCn)) {
			try {
				FieldsManager fm = doc.getFieldsManager();
				EnumItem divField = fm.getEnumItem(BUDGET_ACCEPTANCE_TASK_NAME.equals(acceptationCn) ? "WYDZIAL" : "WYDZIAL_DODATKOWY");
				if (divField == null) {
					throw new ValueNotFoundException("Pole WYDZIAL lub WYDZIAL_DODATKOWY jest nullem");
				}
				Set<String> userNames = Sets.newHashSet();
				
				/*
					select * from DS_DIVISION d
					join DS_USER_TO_DIVISION ud on d.ID=ud.DIVISION_ID
					join DS_USER u on ud.USER_ID=u.ID
					join  dso_role_usernames rd on u.NAME=rd.username
					where rd.id=9 and d.ID=3628
				 */
				
				Integer secretaryRoleId = 12;
				if (Docusafe.getAdditionProperty("role.id.secretary") != null) {
					try {
						secretaryRoleId = Integer.valueOf(Docusafe.getAdditionProperty("role.id.secretary"));
					} catch (Exception e) {
						log.error(e.getMessage(), e);
					}
				}

                DSDivision div = DSDivision.findById(divField.getId());
                /*Role secretaryRole = Role.find(secretaryRoleId);

                for (DSUser user : div.getUsers()) {
                    Map<String, String> profile2Keys = user.getProfilesWithKeys();

                    if (Arrays.asList(secretaryRole.getUserSources(user)).contains("own")) {
                        userNames.add(user.getName());
                    } else {
                        for (String profile : secretaryRole.getUserSources(user)) {
                            if (profile2Keys.containsKey(profile) && (profile2Keys.get(profile).equals("") || profile2Keys.get(profile).equals(Profile.emptyKey) || profile2Keys.get(profile).equals(div.getGuid()))) {
                                userNames.add(user.getName());
                                break;
                            }
                        }
                    }
                }*/

				//String sqlQuery = "select u.NAME from DS_DIVISION d join DS_USER_TO_DIVISION ud on d.ID=ud.DIVISION_ID join DS_USER u on ud.USER_ID=u.ID join dso_role_usernames rd on u.NAME=rd.username where rd.role_id=? and d.ID=?";

				PreparedStatement ps;
				ResultSet rs;
				
				ps = DSApi.context().prepareStatement(getSecretaryFindSql());
				ps.setInt(1, secretaryRoleId);
				ps.setInt(2, divField.getId());
				rs = ps.executeQuery();
				while (rs.next()) {
					userNames.add(rs.getString(1));
				}
				if (!userNames.isEmpty()) {
					for (String user : userNames) {
						assignable.addCandidateUser(user);
						AssigneeHandler.addToHistory(openExecution, doc, user, null);
					}
					return true;
				} else {
					while (div.getOriginalUsers().length == 0) {
						div = div.getParent();
                        if (div == null) {
                            throw new DivisionNotFoundException("null");
                        }
					}
					assignable.addCandidateGroup(div.getGuid());
					AssigneeHandler.addToHistory(openExecution, doc, null, div.getGuid());
					return true;
				}
				
			} catch (Exception e) {
				log.error("Nie znaleziono osoby do przypisania dla: " + acceptationCn + ", przypisano do admin'a, error: "+e.getMessage());
                if (e instanceof ReferenceToHimselfException || e instanceof DivisionNotFoundException) {
                    addRemarkAndAssignToAdmin(doc, assignable, "Nie znaleziono osoby do przypisania na etap akceptacji sekretariatu jednostki. W �adnym z dzia��w nie by�o u�ytkownik�w.", log);
                } else if (e instanceof ValueNotFoundException) {
                    addRemarkAndAssignToAdmin(doc, assignable, "Nie znaleziono osoby do przypisania na etap akceptacji sekretariatu jednostki. Nie wybrano pola Jednostka lub Przeka� do dalszego rozpisania do jednostki.", log);
                } else {
                    assignable.addCandidateUser("admin");
                }
				return true;
			}
		}
		
		
		//--------------STARA METODA-------------------
		if (BUDGET_TYPE_ACCEPTANCE_TASK_NAME.equals(acceptationCn)) {
			try {
				if (openExecution.getVariable(BUDGET_TYPE_ID_VAR_NAME) != null && StringUtils.isNumeric(openExecution.getVariable(BUDGET_TYPE_ID_VAR_NAME).toString())) {
					EnumItem item = DataBaseEnumField.getEnumItemForTable(OBSZARY_TABLE_NAME, Integer.valueOf(openExecution.getVariable(BUDGET_TYPE_ID_VAR_NAME).toString()));
					String acceptCn = item.getCn();
					
					if (acceptCn != null) {
						List<String> users = new ArrayList<String>();
						List<String> divisions = new ArrayList<String>();
						for (AcceptanceCondition accept : AcceptanceCondition.find(acceptCn, null)) {
							if (accept.getUsername() != null && !users.contains(accept.getUsername()))
								users.add(accept.getUsername());
							if (accept.getDivisionGuid() != null && !divisions.contains(accept.getDivisionGuid()))
								divisions.add(accept.getDivisionGuid());
						}
						for (String user : users) {
							assignable.addCandidateUser(user);
							AssigneeHandler.addToHistory(openExecution, doc, user, null);
						}
						for (String division : divisions) {
							assignable.addCandidateGroup(division);
							AssigneeHandler.addToHistory(openExecution, doc, null, division);
						}
						if (users.isEmpty() && divisions.isEmpty()) {
							throw new EdmException("Brak osoby na drzewie akceptacji");
						}
						return true;
					}
				} else {
					throw new EdmException("brak obszaru");
				}
			} catch (EdmException e) {
				log.error("Nie znaleziono osoby do przypisania dla: " + acceptationCn + ", przypisano do admin'a, error: "+e.getMessage());
				assignable.addCandidateUser("admin");
				return true;
			}
		}

        //stara metoda
		if (BUDGET_OWNER_ACCEPT_TASK_NAME.equals(acceptationCn)) {
            try {
                Long bpId = (Long) openExecution.getVariable(BP_VARIABLE_NAME);
                UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
                UlBudgetPosition bp = bpInstance.find(bpId);

                //Z PROJEKTU
                Integer projectId = bp.getCProject();
                if (projectId != null && projectId != -1) {
                    Integer userId = DataBaseEnumField.getEnumItemForTable(PROJECTS_TABLE_NAME, projectId).getCentrum();
                    try {
                        DSUser user = DSUser.findAllByExtension(userId.toString());
                        if (!user.isDeleted()) {
                            assignable.addCandidateUser(user.getName());
                            AssigneeHandler.addToHistory(openExecution, doc, user.getName(), null);
                            return true;
                        } else {
                            throw new AssigningHandledException("Przypisany kierownik projektu zosta� usuni�ty z DS, kod u�ytkownika: "+userId+", dla pozycji l.p. "+bp.getINo());
                        }
                    } catch (UserNotFoundException e) {
                        throw new AssigningHandledException("Nieprawid�owy kod kierownika projektu: "+userId+", dla pozycji l.p. "+bp.getINo());
                    }
                }

                //Z POLA OSOBA UPOWAZNIONA/KIEROWNIK
                Integer upowaznionyId = bp.getCUser();
                if (upowaznionyId != null) {
                    String username = DataBaseEnumField.getEnumItemForTable(UPOWAZNIONY_TABLE_NAME, upowaznionyId).getCn();
                    if (StringUtils.isNotEmpty(username)) {
                        assignable.addCandidateUser(username);
                        AssigneeHandler.addToHistory(openExecution, doc, username, null);
                        return true;
                    }
                }

                //Z DRZEWA AKCEPTACJI, akceptacja social_budget_accept
                Integer itemGroup = bp.getIGroup();
                if (itemGroup != null) {
                    String itemGroupCn = DataBaseEnumField.getEnumItemForTable(GRUPY_DZIALALNOSCI_TABLE_NAME, itemGroup).getCn();

                    if (SOCIAL_ITEM_GROUP_CNS.contains(itemGroupCn)) {
                        String acceptName = SOCIAL_BUDGET_ACCEPT_NAME;

                        if (GROUP_851_CN.equals(itemGroupCn)) {
                        } else if (GROUP_852_CN.equals(itemGroupCn)) {
                            acceptName = SOCIAL_852_BUDGET_ACCEPT_NAME;
                        }

                        List<String> users = new ArrayList<String>();
                        List<String> divisions = new ArrayList<String>();
                        for (AcceptanceCondition accept : AcceptanceCondition.find(acceptName, null)) {
                            if (accept.getUsername() != null && !users.contains(accept.getUsername()))
                                users.add(accept.getUsername());
                            if (accept.getDivisionGuid() != null && !divisions.contains(accept.getDivisionGuid()))
                                divisions.add(accept.getDivisionGuid());
                        }
                        for (String user : users) {
                            assignable.addCandidateUser(user);
                            AssigneeHandler.addToHistory(openExecution, doc, user, null);
                        }
                        for (String division : divisions) {
                            assignable.addCandidateGroup(division);
                            AssigneeHandler.addToHistory(openExecution, doc, null, division);
                        }
                        if (users.isEmpty() && divisions.isEmpty()) {
                            throw new AssigningHandledException("Brak osoby na drzewie akceptacji, akceptacja pozycji bud�etowych dla kont 851-X oraz 852-X");
                        }
                        return true;
                    }
                }

                throw new EdmException("Nie znaleziono osoby do przypisania o id: " + bpId.toString() + ", przypisano do admin'a");
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                log.error("Nie znaleziono osoby do przypisania, przypisano do admin'a");
                if (e instanceof AssigningHandledException) {
                    addRemarkAndAssignToAdmin(doc, assignable, e.getMessage(), log);
                } else {
                    assignable.addCandidateUser("admin");
                }

                return true;
            }



			/*try {
				Long bpId = (Long) openExecution.getVariable(BP_VARIABLE_NAME);

                Set<String> usernameToAssignee = Sets.newHashSet();
                Set<String> guidToAssignee = Sets.newHashSet();

                findUnitsToPositionOwnerAssignee(bpId, usernameToAssignee, guidToAssignee);

                if (usernameToAssignee.isEmpty() && guidToAssignee.isEmpty()) {
                    throw new EdmException("Nie znaleziono osoby do przypisania o id: " + bpId.toString() + ", przypisano do admin'a");
                } else {
                    for (String guid : guidToAssignee) {
                        assignable.addCandidateGroup(guid);
                        AssigneeHandler.addToHistory(openExecution, doc, null, guid);
                    }

                    for (String username : usernameToAssignee) {
                        assignable.addCandidateUser(username);
                        AssigneeHandler.addToHistory(openExecution, doc, username, null);
                    }
                }

			} catch (Exception e) {
				log.error(e.getMessage(), e);
				log.error("Nie znaleziono osoby do przypisania, przypisano do admin'a");
                if (e instanceof AssigningHandledException) {
                    addRemarkAndAssignToAdmin(doc, assignable, e.getMessage(), log);
                } else {
                    assignable.addCandidateUser("admin");
                }

				return true;
			}*/
		}

        if (BUDGET_OWNER_ACCEPT_NEW_TASK_NAME.equals(acceptationCn)) {
            try {
                String[] assigneInfo = {};//ObjectUtils.toString(openExecution.getVariable("assigneeInfo")).split(PositionOwnerAcceptPrepare.POSITION_IDS_SPLITTER);

                openExecution.createVariable("PID", Splitter.on(',').split(assigneInfo[1]));

                Iterable<String> unitsToAssignee = Splitter.on(',').split(assigneInfo[0]);

                for (String unit : unitsToAssignee) {
                    if (unit.startsWith("u:")) {
                        assignable.addCandidateUser(unit.substring(2));
                        AssigneeHandler.addToHistory(openExecution, doc, unit.substring(2), null);
                    }
                    if (unit.startsWith("d:")) {
                        assignable.addCandidateGroup(unit.substring(2));
                        AssigneeHandler.addToHistory(openExecution, doc, null, unit.substring(2));
                    }
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                assignable.addCandidateUser("admin");
            }
        }
		
		if (QUESTOR_TASK_NAME.equals(acceptationCn)) {
			try {
				FieldsManager fm = doc.getFieldsManager();

				Object dicRaw = fm.getKey(UPOWAZNIONY_KWESTOR_DICTIONARY_FIELD_NAME);
				List<Integer> upowaznionyEnumIds = Lists.newArrayList();
				if (dicRaw instanceof Collection<?>) {
					Collection<Long> items = (Collection<Long>) dicRaw;
					for (Long id : items) {
						NativeCriteria nc = DSApi.context().createNativeCriteria("dsg_ul_costinvoice_kwestor_upowazniony", "t");

						CriteriaResult cr = nc.setProjection(NativeExps.projection().addProjection("t.UPOWAZNIONY")).add(NativeExps.eq("t.id", id)).criteriaResult();
						if (cr.next()) {
							upowaznionyEnumIds.add(cr.getInteger(0, 0));
							if (cr.next()) {
								log.error("Znaleziono kilka wpis�w w tabeli dsg_ul_costinvoice_kwestor_upowazniony dla id:" + id);
							}
						}
					}
					Set<String> userToAssigne = Sets.newHashSet();
					for (Integer id : upowaznionyEnumIds) {
						userToAssigne.add(DataBaseEnumField.getEnumItemForTable(KWESTOR_UPOWAZNIONY_TABLE_NAME, id).getCn());
					}
					if (!userToAssigne.isEmpty()) {
						for (String userName : userToAssigne) {
							assignable.addCandidateUser(userName);
							AssigneeHandler.addToHistory(openExecution, doc, userName, null);
						}
						return true;
					}
				}
				
				//nie aktualne
//				String acceptCn = null;
//				if (StringUtils.isNotEmpty((acceptCn = fm.getEnumItemCn(QUESTOR_TASK_NAME.equals(acceptationCn)?UPOWAZNIONY_KWESTOR_FIELD:UPOWAZNIONY_KANCLERZ_FIELD)))) {
//					assignable.addCandidateUser(acceptCn);
//					AssigneeHandler.addToHistory(openExecution, doc, acceptCn, null);
//					return true;
//				}
			} catch (Exception e) {
				log.debug("Nie znaleziono osoby upowaznionej przez kwestora/kancelerza, przypisuje do kwestora/kancelerza");
				return false;
			}
		}

		return false;
	}

    public static boolean findUnitsToPositionOwnerAssignee(Long bpId, Set<String> usernameToAssignee, Set<String> guidToAssignee) throws EdmException {
        UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
        UlBudgetPosition bp = bpInstance.find(bpId);

        //Z PROJEKTU
        Integer projectId = bp.getCProject();
        if (projectId != null && projectId != -1) {
            Integer userId = DataBaseEnumField.getEnumItemForTable(PROJECTS_TABLE_NAME, projectId).getCentrum();
            try {
                DSUser user = DSUser.findAllByExtension(userId.toString());
                if (!user.isDeleted()) {
                    usernameToAssignee.add(user.getName());
                    return true;
                } else {
                    throw new AssigningHandledException("Przypisany kierownik projektu zosta� usuni�ty z DS, kod u�ytkownika: " + userId + ", dla pozycji l.p. " + bp.getINo());
                }
            } catch (UserNotFoundException e) {
                throw new AssigningHandledException("Nieprawid�owy kod kierownika projektu: " + userId + ", dla pozycji l.p. " + bp.getINo());
            }
        }

        //Z POLA OSOBA UPOWAZNIONA/KIEROWNIK
        Integer upowaznionyId = bp.getCUser();
        if (upowaznionyId != null) {
            String username = DataBaseEnumField.getEnumItemForTable(UPOWAZNIONY_TABLE_NAME, upowaznionyId).getCn();
            if (StringUtils.isNotEmpty(username)) {
                usernameToAssignee.add(username);
                return true;
            }
        }

        //Z DRZEWA AKCEPTACJI, akceptacja social_budget_accept
        Integer itemGroup = bp.getIGroup();
        if (itemGroup != null) {
            String itemGroupCn = DataBaseEnumField.getEnumItemForTable(GRUPY_DZIALALNOSCI_TABLE_NAME, itemGroup).getCn();

            if (SOCIAL_ITEM_GROUP_CNS.contains(itemGroupCn)) {
                String acceptName = SOCIAL_BUDGET_ACCEPT_NAME;

                if (GROUP_851_CN.equals(itemGroupCn)) {
                } else if (GROUP_852_CN.equals(itemGroupCn)) {
                    acceptName = SOCIAL_852_BUDGET_ACCEPT_NAME;
                }

                List<String> users = new ArrayList<String>();
                List<String> divisions = new ArrayList<String>();
                for (AcceptanceCondition accept : AcceptanceCondition.find(acceptName, null)) {
                    if (accept.getUsername() != null && !users.contains(accept.getUsername()))
                        users.add(accept.getUsername());
                    if (accept.getDivisionGuid() != null && !divisions.contains(accept.getDivisionGuid()))
                        divisions.add(accept.getDivisionGuid());
                }
                for (String user : users) {
                    usernameToAssignee.add(user);
                }
                for (String division : divisions) {
                    guidToAssignee.add(division);
                }
                if (users.isEmpty() && divisions.isEmpty()) {
                    throw new AssigningHandledException("Brak osoby na drzewie akceptacji, akceptacja pozycji bud�etowych dla kont 851-X oraz 852-X");
                }
                return true;
            }
        }
        return false;
    }

    private String getSecretaryFindSql() {
        String sql = null;
        try {
            sql = Docusafe.getAdditionProperty("role.secretary.sql");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return Strings.isNullOrEmpty(sql) ? FIND_SECREATARY_SQL : sql;
    }

    @Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException {
		StringBuilder msgBuilder = new StringBuilder();

/*		BigDecimal totalVatAmount = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
		// sumowanie kwot poszczegolnych pozycji budzetowych
		if (fieldValues.get("M_DICT_VALUES") != null)
			totalVatAmount = sumDicItems(fieldValues, STAWKI_VAT_FIELD_NAME, "NETTO");

		// suma kwot pozycji budzetowych musi byc rowna kwocie wniosku
		if (fieldValues.get(KWOTA_NETTO_FIELD) != null && fieldValues.get(STAWKI_VAT_FIELD_NAME) != null && totalVatAmount != null) {
			if (((BigDecimal) fieldValues.get(KWOTA_NETTO_FIELD)).compareTo(totalVatAmount) != 0)
			{
				msgBuilder.append(SUMA_VATS_ERROR_MSG);
			}
		}
		
		BigDecimal totalVatAmountBrutto = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
		// sumowanie kwot poszczegolnych pozycji budzetowych
		if (fieldValues.get("M_DICT_VALUES") != null)
			totalVatAmountBrutto = sumDicItems(fieldValues, STAWKI_VAT_FIELD_NAME, "BRUTTO");
		
		// suma kwot pozycji budzetowych musi byc rowna kwocie wniosku
		if (fieldValues.get(KWOTA_BRUTTO_FIELD) != null && fieldValues.get(STAWKI_VAT_FIELD_NAME) != null && totalVatAmountBrutto != null) {
			if (((BigDecimal) fieldValues.get(KWOTA_BRUTTO_FIELD)).compareTo(totalVatAmountBrutto) != 0)
			{
				msgBuilder.append(SUMA_VATS_BRUTTO_ERROR_MSG);
			}
		}*/


        if (Boolean.TRUE.equals(fieldValues.get("KOREKTA")) && fieldValues.get("KOREKTA_FAKTURA") != null) {
            validateContractorInCorrectedDocument(documentId, fieldValues, msgBuilder);
        }
			
		// data platnosci nie moze byc wczesniej niz data wystawienia
        if (fieldValues.get(PRZEDPLATA_FIELD) == null || !Boolean.TRUE.equals(fieldValues.get(PRZEDPLATA_FIELD))) {
		    msgBuilder.append(validateDate(fieldValues.get(DATA_WYSTAWIENIA_FIELD), fieldValues.get(TERMIN_PLATNOSCI_FIELD), DATA_PLATNOSCI_ERROR_MSG));
        }

		// data wp�ywu nie mo�e by� wcze�niejsza ni� data wystawienia
		msgBuilder.append(validateDate(fieldValues.get(DATA_WYSTAWIENIA_FIELD), fieldValues.get(DATA_WPLYWU_FIELD), DATA_WPLYWU_ERROR_MSG));
		
		// data wp�ywu nie mo�e by� wcze�niejsza ni� data rejestracji
		//msgBuilder.append(validateDate(fieldValues.get(DATA_WPLYWU_FIELD), new Date(), DATA_WPLYWU_TERAZ_ERROR_MSG));
		
		if (fieldValues.get(BARCODE_FIELD) != null) {
			validateBarcode(documentId, fieldValues, msgBuilder);
		}

        if (msgBuilder.length() > 0)
			throw new EdmException(msgBuilder.toString());
	}

    private void validateContractorInCorrectedDocument(Long documentId, Map<String, ?> fieldValues, StringBuilder msgBuilder) throws EdmException {
        Long correctedDocumentId = (Long) fieldValues.get("KOREKTA_FAKTURA");

        Long documentWparam = getSenderWparamId(documentId);
        Long correctedDocumentWparam = getSenderWparamId(correctedDocumentId);

        if (documentWparam != null && correctedDocumentWparam != null && !documentWparam.equals(correctedDocumentWparam)) {
            msgBuilder.append(WYSTAWCA_FAKTURY_PIERWOTNEJ_ERROR_MSG);
        }
    }

    private Long getSenderWparamId(Long documentId) throws EdmException {
        OfficeDocument document = OfficeDocument.find(documentId);
        return document.getSender() != null ? document.getSender().getWparam() : null;
    }

    @Override
    public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException {
        StringBuilder msgBuilder = new StringBuilder();

        if (ACCOUNTANCY_ACCEPT_STATES.contains(acceptationCn)) {
            BigDecimal netValueSum = new BigDecimal(0);
            BigDecimal grossValueSum = new BigDecimal(0);

            Object vatValues = doc.getFieldsManager().getValue("STAWKI_VAT");
            Preconditions.checkState(vatValues != null && vatValues instanceof Iterable);

            List<Long> pozycjeFaktury = (List) doc.getFieldsManager().getKey(BP_FIELD);
            Preconditions.checkState(pozycjeFaktury != null && pozycjeFaktury instanceof Iterable);
            if (pozycjeFaktury != null) {
                int i = 0;
                UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
                for (Long bpId : pozycjeFaktury) {
                    UlBudgetPosition bp = bpInstance.find(bpId);
                    List<VatRate> vatRates = getVatRateCode(doc.getId(), bp.getId());

                    Preconditions.checkState(!vatRates.isEmpty(), "Nie wype�niono stawki vat");

                    BigDecimal vatGrossAmountSum = BigDecimal.ZERO;

                    for (VatRate rate : vatRates) {
                        vatGrossAmountSum = vatGrossAmountSum.add(rate.getGrossAmount());
                    }

                    if (bp.getIAmount().compareTo(vatGrossAmountSum) != 0) {
                        msgBuilder.append(String.format(POSITION_BRUTTO_SUM_VATS_ERROR_MSG, bp.getINo()));
                    }
                }
            }

            Object grossValue = doc.getFieldsManager().getKey(KWOTA_BRUTTO_FIELD);
            Preconditions.checkState(grossValue != null && grossValue instanceof BigDecimal);

            Object netValue = doc.getFieldsManager().getKey(KWOTA_NETTO_FIELD);
            Preconditions.checkState(netValue != null && netValue instanceof BigDecimal);

            for (Object value : (Iterable) vatValues) {
                Preconditions.checkState(value != null && value instanceof Map);
                Map<String, Object> item = (Map<String, Object>) value;

                Object dicNetValue = item.get(STAWKI_VAT_FIELD_NAME + "_NETTO");
                if (dicNetValue != null) {
                    Preconditions.checkState(dicNetValue instanceof BigDecimal);
                    netValueSum = netValueSum.add((BigDecimal) dicNetValue);
                } else {
                    log.error("Pusta kwota netto s�ownika stawek vat dla pozycji: "+item);
                }

                Object dicGrossValue = item.get(STAWKI_VAT_FIELD_NAME + "_BRUTTO");
                if (dicGrossValue != null) {
                    Preconditions.checkState(dicGrossValue instanceof BigDecimal);
                    grossValueSum = grossValueSum.add((BigDecimal) dicGrossValue);
                }
            }

            if (((BigDecimal) grossValue).compareTo(grossValueSum) != 0) {
                msgBuilder.append(SUMA_VATS_BRUTTO_ERROR_MSG);
            }

            if (((BigDecimal) netValue).compareTo(netValueSum) != 0) {
                msgBuilder.append(SUMA_VATS_ERROR_MSG);
            }

            if (msgBuilder.length() > 0) {
                throw new EdmException(msgBuilder.toString());
            }
        }
    }

    private void validateBarcode(Long documentId, Map<String, ?> fieldValues, StringBuilder msgBuilder) {
		String barCode = fieldValues.get(BARCODE_FIELD).toString();
/*		if (!Strings.isNullOrEmpty(barCode)) {
            try {
                List<OfficeDocument> documents = OfficeDocument.findAllByBarcode(barCode);

                if (documents != null) {
                    for (OfficeDocument document : documents) {
                         if (!document.getId().equals(documentId)) {
                             msgBuilder.append("Podany kod kreskowy zosta� ju� wykorzystany w dokumencie o nr KO: ");
                             msgBuilder.append(document.getOfficeNumber());
                             msgBuilder.append(". ");
                             return;
                         }
                    }
                }

            } catch (EdmException e) {
                log.error(e.getMessage(), e);
            }
		}*/

        if (!Strings.isNullOrEmpty(barCode)) {
            StringBuilder sql = new StringBuilder("select OFFICENUMBER from dso_in_document where id = (select top 1 document_id from " + CostInvoiceLogic.COST_INVOICE_TABLE + " where barcode = ? and status<>1000");
            if (documentId != null) {
                sql.append(" and document_id <> ");
                sql.append(documentId);
            }
            sql.append(")");
            Integer officeNumber = null;
            PreparedStatement ps = null;
            try {
                ps = DSApi.context().prepareStatement(sql);
                ps.setString(1, barCode);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    officeNumber = rs.getInt(1);
                }
                rs.close();
                ps.close();

            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
            DbUtils.closeQuietly(ps);

            if (officeNumber != null && officeNumber.compareTo(0) != 0) {
                msgBuilder.append("Podany kod kreskowy zosta� ju� wykorzystany w dokumencie o nr KO: ");
                msgBuilder.append(officeNumber);
                msgBuilder.append(". ");
            }
        }
	}
}
