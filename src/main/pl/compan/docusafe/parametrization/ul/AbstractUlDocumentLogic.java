package pl.compan.docusafe.parametrization.ul;

import static pl.compan.docusafe.core.dockinds.field.DataBaseEnumField.REPO_REF_FIELD_PREFIX;
import static pl.compan.docusafe.parametrization.ul.CostInvoiceLogic.RODZAJ_DZIALALNOSCI_FIELD_NAME;
import static pl.compan.docusafe.parametrization.ul.CostInvoiceLogic.RODZAJ_DZIALALNOSCI_TABLE_NAME;
import static pl.compan.docusafe.parametrization.ul.CostInvoiceLogic.VAT_RATES_TABLE_NAME;
import static pl.compan.docusafe.parametrization.ul.CostInvoiceLogic.log;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.ObjectUtils;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.exports.BudgetItem;
import pl.compan.docusafe.core.imports.simple.repository.CacheHandler;
import pl.compan.docusafe.core.imports.simple.repository.Dictionary;
import pl.compan.docusafe.core.imports.simple.repository.DictionaryMap;
import pl.compan.docusafe.core.imports.simple.repository.SimpleRepositoryAttribute;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.parametrization.ul.hib.UlBudgetPosition;
import pl.compan.docusafe.parametrization.ul.hib.UlBudgetsFromUnits;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;

import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class AbstractUlDocumentLogic extends AbstractDocumentLogic {

    public static final String DOC_DATE_TEMPLATE_VAR = "DOC_DATE";
    public static final String GENERATE_DATE_TEMPLATE_VAR = "GENERATE_DATE";


    static final String STAWKI_VAT_FIELD_NAME = "STAWKI_VAT";
    static final String KOSZTY3_CN_PREFIX = "85";
    static final String KOSZTY2_CN_PREFIX = "6";
    static final String KOSZTY_CN_PREFIX = "5";
    public static final String BP_FIELD = "BP";
    public static final String FUND_SOURCES_TABLE_NAME = "dsg_ul_projects_fund_sources";
    public static final String ORGANIZATIONAL_UNIT_TABLE_NAME = "dsg_ul_organizational_unit";
    public static final String PROJECTS_TABLE_NAME = "dsg_ul_projects";
    protected static final String BP_FIELD_WITH_LINK = BP_FIELD+"_";
    protected static final String UPOWAZNIONY_TABLE_NAME = "dsg_ul_project_osoba_upowazniona";
    protected static final EnumValues EMPTY_ENUM_VALUE = new EnumValues(new HashMap<String,String>(), new ArrayList<String>());
    public static final EnumValues EMPTY_NOT_HIDDEN_ENUM_VALUE = new EnumValues(new HashMap<String,String>(){{put("","-- wybierz --");}}, new ArrayList<String>(){{add("");}});
    public static final String POZYCJE_BUDZETOWE_DOC_VIEW = "pozycje_budzetowe_doc_view";
    public static final String COSTINVOICE_MULTIPLE_VALUE_TABLE = "dsg_ul_costinvoice_multiple_value";

    @Override
    public boolean canChangeDockind(Document document) throws EdmException {
        return false;
    }

    @Override
	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
		String documentKindName = document.getDocumentKind().getName();

		perms.add(new PermissionBean(ObjectPermission.READ, "UL_" + documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, "UL - " + documentKindName + " - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "UL_" + documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "UL - " + documentKindName + " zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "UL_" + documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "UL - " + documentKindName + " - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "UL_" + documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "UL - " + documentKindName + " zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "UL_" + documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "UL - " + documentKindName + " - usuwanie"));
		Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
		perms.addAll(documentPermissions);
		this.setUpPermission(document, perms);
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException 
	{
		String docKindTitle = document.getDocumentKind().getName();
		FieldsManager fm = document.getFieldsManager();
		
		Folder folder = Folder.getRootFolder();
		
		folder = folder.createSubfolderIfNotPresent(docKindTitle);
		
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(document.getCtime()));
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(document.getCtime()));
		
		String docNumber;
		if (fm.getValue("NR_FAKTURY") != null) {
			docNumber = String.valueOf(fm.getValue("NR_FAKTURY"));
		} else {
			docNumber = "";
		}
		
		document.setTitle(docKindTitle + " " + docNumber);
		document.setDescription(docKindTitle + " " + docNumber);
		
		document.setFolder(folder);
	}

	protected StringBuilder validateDate(Object start, Object finish, String errorMsg)
	{
		StringBuilder msg = new StringBuilder();
		try {
			if (start != null && finish != null) {
				Date startDate = null;
				Date finishDate = null;
				
				if (start instanceof Date) {
					startDate = (Date)start;
				} else if (start instanceof FieldData && ((FieldData)start).getData() instanceof Date) {
					startDate = ((FieldData)start).getDateData();
				}
				
				if (finish instanceof Date) {
					finishDate = (Date)finish;
				} else if (finish instanceof FieldData && ((FieldData)finish).getData() instanceof Date) {
					finishDate = ((FieldData)finish).getDateData();
				}
				
				if (startDate!=null && finishDate!=null && startDate.after(finishDate)) {
					msg.append(errorMsg);
				}
					
			}
		} catch (Exception e) {
			//TODO wylapany
		}
		return msg;
	}

    protected void setRemainingAmount(Map<String, FieldData> values, StringBuilder msgBuilder) {
        Map<String,FieldData> mpkDic = values.get(CostInvoiceLogic.BP_DWR_FIELD).getDictionaryData();
        BigDecimal amountSum = new BigDecimal(0);
        for (String key : mpkDic.keySet()) {
            if (key.contains(CostInvoiceLogic.BP_FIELD+"_I_AMOUNT")) {
                if (mpkDic.get(key) != null && mpkDic.get(key).getType().equals(Field.Type.MONEY) && mpkDic.get(key).getMoneyData() != null) {
                    amountSum = amountSum.add(mpkDic.get(key).getMoneyData());
                }
            }
        }
        amountSum = amountSum.setScale(2, RoundingMode.HALF_UP);
        if (values.get(CostInvoiceLogic.KWOTA_POZOSTALA_MPK_DWR_FIELD).getType().equals(Field.Type.MONEY) && values.get(CostInvoiceLogic.KWOTA_BRUTTO_DWR_FIELD).getType().equals(Field.Type.MONEY)) {
            if (values.get(CostInvoiceLogic.KWOTA_BRUTTO_DWR_FIELD).getMoneyData() != null) {
                BigDecimal remain = values.get(CostInvoiceLogic.KWOTA_BRUTTO_DWR_FIELD).getMoneyData().setScale(2, RoundingMode.HALF_UP).subtract(amountSum);
                values.get(CostInvoiceLogic.KWOTA_POZOSTALA_MPK_DWR_FIELD).setData(values.get(CostInvoiceLogic.KWOTA_BRUTTO_DWR_FIELD).getMoneyData().setScale(2, RoundingMode.HALF_UP).subtract(amountSum));
                if (remain.compareTo(BigDecimal.ZERO) == -1) {
                    msgBuilder.append("Suma rozpisanych kwot nie mo�e by� wi�ksza od kwoty brutto faktury. ");
                }
            }
        }
    }

    public String getCnValue(String tableName, Integer enumId, boolean processNotApplicableValue, Integer notApplicableValue) throws EdmException {
        if (enumId != null && (processNotApplicableValue || !Objects.equal(notApplicableValue,enumId))) {
            return DataBaseEnumField.getEnumItemForTable(tableName, Integer.valueOf(enumId)).getCn();
        }
        return null;
    }

    protected BigDecimal sumDicItems(Map<String, ?> fieldValues, String dicName, String moneyFieldName) throws EdmException {
        BigDecimal totalCentrum = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
        Map<String, Object> mpkValues = (Map<String, Object>) fieldValues.get("M_DICT_VALUES");
        boolean atLeastOne = false;
        for (String mpkCn : mpkValues.keySet()) {
            if (mpkCn.contains(dicName + "_")) {
                Map<String, Object> mpkVal = (Map<String, Object>) mpkValues.get(mpkCn);
                BigDecimal oneAmount = ((FieldData) mpkVal.get(moneyFieldName)).getMoneyData();
                if (oneAmount != null) {
                    atLeastOne = true;
                    totalCentrum = totalCentrum.add(oneAmount);
                }
            }
        }
        if (!atLeastOne) {
            return null;
        }
        return totalCentrum;
    }

    protected Set<String> getRepositoryDictsIds(Integer docTypeObjectId, Integer contextId) {
        List<SimpleRepositoryAttribute> repositoryDicts = CacheHandler.getRepositoryDicts(docTypeObjectId, contextId);
        Set<String> repositoryDictsIds = Sets.newHashSet();
        for (SimpleRepositoryAttribute repositoryDict : repositoryDicts) {
            repositoryDictsIds.add(repositoryDict.getRepKlasaId().toString());
        }
        return repositoryDictsIds;
    }

    protected void setRepositoryDict(Map<String, FieldData> values, Map<String, Object> results, String dicNameWithSuffix,
                                     Integer docTypeObjectId, Integer contextId, Set<String> repositoryDictsIds, DictionaryMap dictMap, EnumValues defaultEnum, Dictionary dict, Logger log) {
        if (dictMap != null && dictMap.isShowAllReferenced()) {
            DwrUtils.setRefValueEnumOptions(values, results, DataBaseEnumField.REPO_REF_FIELD_PREFIX + dict.getCn(), dicNameWithSuffix, dict.getCn(),
                    dict.getTablename(), DataBaseEnumField.REF_ID_FOR_ALL_FIELD, defaultEnum);
        } else {
            if (repositoryDictsIds.contains(dict.getCode())) {
                try {
                    Long dictObjectId = Long.valueOf(dict.getCode());
                    SimpleRepositoryAttribute attr = CacheHandler.getRepostioryDict(dictObjectId, docTypeObjectId, contextId);
                    if (attr != null) {
                        DwrUtils.setRefValueEnumOptions(values, results, REPO_REF_FIELD_PREFIX + dict.getCn(), dicNameWithSuffix, dict.getCn(),
                                dict.getTablename(), attr.getRepAtrybutId().toString(), defaultEnum);
                    }
                } catch (NumberFormatException e) {
                    log.error("CAUGHT: "+e.getMessage(),e);
                }
            }
        }
    }

    protected Long getRepositoryAttributeValue(Long bpId, SimpleRepositoryAttribute dict, Logger log) {
		PreparedStatement ps = null;
		Long result = null;
		try {
			ps = DSApi.context().prepareStatement("select centrum from dsg_ul_budget_position bp join "+dict.getTableName()+" r on bp."+ dict.getTableName() +"=r.id where bp.id = ?");
			ps.setLong(1, bpId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				result = rs.getLong(1) != 0l ? rs.getLong(1) : null;
				if (rs.next()) {
					log.error("Found more than one repository item to budget position, bp id: "+bpId+", attribute: "+dict.getTableName());
				}
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		DbUtils.closeQuietly(ps);
		return result;
	}

    /**
     * Add message to document remarks and assigne to admin
     * @param doc
     * @param assignable
     * @param message
     */
    protected void addRemarkAndAssignToAdmin(OfficeDocument doc, Assignable assignable, String message, Logger log) {
        String adminName = "admin";
        try {
            adminName = GlobalPreferences.getAdminUsername();
            doc.addRemark(new Remark(message,"admin"));
        } catch (Exception e1) {
            log.error(e1.getMessage(), e1);
        }
        assignable.addCandidateUser(adminName);
    }

    protected void setBudgetItemsRefValuesWithBudgets(String dictionaryName, Map<String, FieldData> values, Map<String, Object> results) throws EdmException {
		String dic = dictionaryName+"_";
		String projectId = null;
		String userId = null;
		String budgetKoId = null;

        DwrUtils.setRefValueEnumOptions(values, results, "I_GROUP", dic, RODZAJ_DZIALALNOSCI_FIELD_NAME, RODZAJ_DZIALALNOSCI_TABLE_NAME, null, EMPTY_ENUM_VALUE, "_1");

		DwrUtils.setRefValueEnumOptions(values, results, "C_MPK", dic, "C_STRUCTURE_BUDGET_KO", UlBudgetsFromUnits.VIEW_NAME, null, EMPTY_ENUM_VALUE);
		DwrUtils.setRefValueEnumOptions(values, results, "C_MPK", dic, "C_PROJECT", PROJECTS_TABLE_NAME, null, EMPTY_ENUM_VALUE);

		if (results.get(dic+"C_USER") != null && results.get(dic+"C_USER") instanceof EnumValues) {
			userId = ((EnumValues) results.get(dic+"C_USER")).getSelectedId();
		}

        if (results.get(dic+"C_STRUCTURE_BUDGET_KO") != null && results.get(dic+"C_STRUCTURE_BUDGET_KO") instanceof EnumValues) {
            budgetKoId = ((EnumValues) results.get(dic+"C_STRUCTURE_BUDGET_KO")).getSelectedId();
        }

		if (results.get(dic+"C_PROJECT") != null && results.get(dic+"C_PROJECT") instanceof EnumValues) {
			projectId = ((EnumValues) results.get(dic+"C_PROJECT")).getSelectedId();
		}

        reloadRefEnumItems(values, results, dictionaryName, "C_STRUCTURE_BUDGET_POSITION_RAW", "C_STRUCTURE_BUDGET_KO", "ul_order", EMPTY_NOT_HIDDEN_ENUM_VALUE);
        DwrUtils.setRefValueEnumOptions(values, results, "C_MPK", dic, "C_USER", UPOWAZNIONY_TABLE_NAME, null, EMPTY_NOT_HIDDEN_ENUM_VALUE);
        DwrUtils.setRefValueEnumOptions(values, results, "C_PROJECT", dic, "C_BUDGET", CostInvoiceLogic.PROJECT_BUDGETS_TABLE_NAME, null, EMPTY_NOT_HIDDEN_ENUM_VALUE);
        DwrUtils.setRefValueEnumOptions(values, results, "C_BUDGET", dic, "C_POSITION", CostInvoiceLogic.PROJECT_BUDGET_POSITIONS_TABLE_NAME, null, EMPTY_NOT_HIDDEN_ENUM_VALUE);
        reloadRefEnumItems(values, results, dictionaryName, "C_RESOURCE_RAW", "C_POSITION", "ul_order", EMPTY_NOT_HIDDEN_ENUM_VALUE);

        if (Strings.isNullOrEmpty(projectId) && Strings.isNullOrEmpty(budgetKoId)) {
            results.put(dic + "C_STRUCTURE_BUDGET_POSITION", EMPTY_ENUM_VALUE);
            results.put(dic + "C_STRUCTURE_BUDGET_POSITION_RAW", EMPTY_ENUM_VALUE);
            results.put(dic + "C_BUDGET", EMPTY_ENUM_VALUE);
            results.put(dic + "C_POSITION", EMPTY_ENUM_VALUE);
            results.put(dic + "C_RESOURCE_RAW", EMPTY_ENUM_VALUE);
            results.put(dic + "C_FUND_SOURCE", EMPTY_ENUM_VALUE);
        } else if (Strings.isNullOrEmpty(projectId)) {
            results.put(dic + "C_PROJECT", EMPTY_ENUM_VALUE);
            results.put(dic + "C_BUDGET", EMPTY_ENUM_VALUE);
            results.put(dic + "C_POSITION", EMPTY_ENUM_VALUE);
            results.put(dic + "C_RESOURCE", EMPTY_ENUM_VALUE);
            results.put(dic + "C_RESOURCE_RAW", EMPTY_ENUM_VALUE);
            DwrUtils.setRefValueEnumOptions(values, results, "C_PROJECT", dic, "C_FUND_SOURCE", FUND_SOURCES_TABLE_NAME, DataBaseEnumField.REF_ID_FOR_ALL_FIELD, EMPTY_NOT_HIDDEN_ENUM_VALUE);
        } else {
            results.put(dic + "C_USER", EMPTY_ENUM_VALUE);
            results.put(dic + "C_STRUCTURE_BUDGET_KO", EMPTY_ENUM_VALUE);
            results.put(dic + "C_STRUCTURE_BUDGET_POSITION", EMPTY_ENUM_VALUE);
            results.put(dic + "C_STRUCTURE_BUDGET_POSITION_RAW", EMPTY_ENUM_VALUE);
            DwrUtils.setRefValueEnumOptions(values, results, "C_PROJECT", dic, "C_FUND_SOURCE", FUND_SOURCES_TABLE_NAME, null, EMPTY_NOT_HIDDEN_ENUM_VALUE);
        }
        /*        if (Strings.isNullOrEmpty(projectId) && Strings.isNullOrEmpty(budgetKoId)) {
            results.put(dic + "C_STRUCTURE_BUDGET_POSITION", EMPTY_ENUM_VALUE);
            results.put(dic + "C_STRUCTURE_BUDGET_POSITION_RAW", EMPTY_ENUM_VALUE);
            results.put(dic + "C_BUDGET", EMPTY_ENUM_VALUE);
            results.put(dic + "C_POSITION", EMPTY_ENUM_VALUE);
            results.put(dic + "C_RESOURCE", EMPTY_ENUM_VALUE);
            results.put(dic + "C_FUND_SOURCE", EMPTY_ENUM_VALUE);
            results.put(dic + "C_RESOURCE_RAW", EMPTY_ENUM_VALUE);
        } else if (Strings.isNullOrEmpty(projectId)) {
            results.put(dic + "C_PROJECT", EMPTY_ENUM_VALUE);
            results.put(dic + "C_BUDGET", EMPTY_ENUM_VALUE);
            results.put(dic + "C_POSITION", EMPTY_ENUM_VALUE);
            results.put(dic + "C_RESOURCE", EMPTY_ENUM_VALUE);
            results.put(dic + "C_RESOURCE_RAW", EMPTY_ENUM_VALUE);
            //DwrUtils.setRefValueEnumOptions(values, results, "C_STRUCTURE_BUDGET_KO", dic, "C_STRUCTURE_BUDGET_POSITION", StructureBudget.STRUCTURE_BUDGET_POSITION_VIEW_NAME, null, EMPTY_ENUM_VALUE);
            reloadRefEnumItems(values, results, dic, "C_STRUCTURE_BUDGET_POSITION_RAW", "C_STRUCTURE_BUDGET_KO");
            DwrUtils.setRefValueEnumOptions(values, results, "C_MPK", dic, "C_USER", UPOWAZNIONY_TABLE_NAME, null, EMPTY_NOT_HIDDEN_ENUM_VALUE);
            DwrUtils.setRefValueEnumOptions(values, results, "C_PROJECT", dic, "C_FUND_SOURCE", FUND_SOURCES_TABLE_NAME, DataBaseEnumField.REF_ID_FOR_ALL_FIELD, EMPTY_NOT_HIDDEN_ENUM_VALUE);
        } else {
            results.put(dic + "C_USER", EMPTY_ENUM_VALUE);
            results.put(dic + "C_STRUCTURE_BUDGET_KO", EMPTY_ENUM_VALUE);
            results.put(dic + "C_STRUCTURE_BUDGET_POSITION", EMPTY_ENUM_VALUE);
            results.put(dic + "C_STRUCTURE_BUDGET_POSITION_RAW", EMPTY_ENUM_VALUE);
            DwrUtils.setRefValueEnumOptions(values, results, "C_PROJECT", dic, "C_FUND_SOURCE", FUND_SOURCES_TABLE_NAME, null, EMPTY_NOT_HIDDEN_ENUM_VALUE);
            DwrUtils.setRefValueEnumOptions(values, results, "C_PROJECT", dic, "C_BUDGET", CostInvoiceLogic.PROJECT_BUDGETS_TABLE_NAME, null, EMPTY_NOT_HIDDEN_ENUM_VALUE);
            DwrUtils.setRefValueEnumOptions(values, results, "C_BUDGET", dic, "C_POSITION", CostInvoiceLogic.PROJECT_BUDGET_POSITIONS_TABLE_NAME, null, EMPTY_NOT_HIDDEN_ENUM_VALUE);
            //DwrUtils.setRefValueEnumOptions(values, results, "C_POSITION", dic, "C_RESOURCE", CostInvoiceLogic.PROJECT_BUDGET_POSITION_RESOURCES_TABLE_NAME, null, EMPTY_ENUM_VALUE);

            reloadRefEnumItems(values, results, dic, "C_RESOURCE_RAW", "C_POSITION");
        }*/

	}

    static void reloadRefEnumItems(Map<String, ?> values, Map<String, Object> results, String dictionaryCn, final String fieldCn, final String refFieldCn, String dockindCn, EnumValues emptyHiddenValue) {
        try {
            Optional<pl.compan.docusafe.core.dockinds.field.Field> resourceField = findField(dockindCn, dictionaryCn, fieldCn + "_1");

            if (resourceField.isPresent()) {
                String dicWithSuffix = dictionaryCn + "_";
                String refFieldSelectedId = getFieldSelectedId(values, results, dicWithSuffix, refFieldCn);

                String fieldSelectedId = "";
                EnumValues enumValue = getEnumValues(values, dicWithSuffix + fieldCn);
                if (enumValue != null) {
                    if (!enumValue.getSelectedOptions().isEmpty()) {
                        fieldSelectedId = enumValue.getSelectedOptions().get(0);
                    }
                }

                EnumValues enumValues = resourceField.get().getEnumItemsForDwr(ImmutableMap.<String, Object>of(refFieldCn + "_1", ObjectUtils.toString(refFieldSelectedId), fieldCn + "_1", ObjectUtils.toString(fieldSelectedId)));

                if (isEmptyEnumValues(enumValues)) {
                    results.put(dicWithSuffix + fieldCn, emptyHiddenValue);
                } else {
                    results.put(dicWithSuffix + fieldCn, enumValues);
                }
            } else {
                throw new EdmException("cant find " + fieldCn + " field");
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }

    static boolean isEmptyEnumValues(EnumValues enumValues) {
        return enumValues.getAllOptions().isEmpty() || (enumValues.getAllOptions().size() == 1 && enumValues.getAllOptions().get(0).containsKey(""));
    }

    static String getFieldSelectedId(Map<String, ?> values, Map<String, Object> results, String dic, String fieldCn) {
        String fieldSelectedId = "";
        if (results.containsKey(dic + fieldCn)) {
            try {
                fieldSelectedId = ((EnumValues)results.get(dic + fieldCn)).getSelectedId();
            } catch (Exception e) {
                log.error("CAUGHT "+e.getMessage(),e);
            }
        } else {
            try {
                EnumValues enumValue = getEnumValues(values, dic + fieldCn);
                if (enumValue != null) {
                    fieldSelectedId = enumValue.getSelectedOptions().get(0);
                }
            } catch (Exception e) {
                log.error("CAUGHT " + e.getMessage(), e);
            }
        }

        return fieldSelectedId;
    }

    public static EnumValues getEnumValues(Map<String, ?> values, String fieldName) {
        Object value = values.get(fieldName);
        if (value instanceof FieldData) {
            return ((FieldData) value).getEnumValuesData();
        } else if (value instanceof EnumValues) {
            return (EnumValues) value;
        }
        return null;
    }

    static Optional<pl.compan.docusafe.core.dockinds.field.Field> findField(String dockindCn, String dictionaryCn, final String dictionaryFieldCn) throws EdmException {
        return Iterables.tryFind(DocumentKind.findByCn(dockindCn).getFieldsMap().get(dictionaryCn).getFields(), new Predicate<pl.compan.docusafe.core.dockinds.field.Field>() {
            @Override
            public boolean apply(pl.compan.docusafe.core.dockinds.field.Field input) {
                return dictionaryFieldCn.equals(input.getCn());
            }
        });
    }

    protected void setBudgetItemsRefValues(String dictionaryName, Map<String, FieldData> values, Map<String, Object> results) throws EdmException {
		String dic = dictionaryName+"_";
		String projectId = null;
		String userId = null;

        DwrUtils.setRefValueEnumOptions(values, results, "I_GROUP", dic, RODZAJ_DZIALALNOSCI_FIELD_NAME, RODZAJ_DZIALALNOSCI_TABLE_NAME, null, EMPTY_ENUM_VALUE, "_1");

        DwrUtils.setRefValueEnumOptions(values, results, "C_MPK", dic, "C_USER", UPOWAZNIONY_TABLE_NAME, null, EMPTY_ENUM_VALUE);
		DwrUtils.setRefValueEnumOptions(values, results, "C_MPK", dic, "C_PROJECT", PROJECTS_TABLE_NAME, null, EMPTY_ENUM_VALUE);

		if (results.get(dic+"C_USER") != null && results.get(dic+"C_USER") instanceof EnumValues) {
			userId = ((EnumValues) results.get(dic+"C_USER")).getSelectedId();
		}

		if (results.get(dic+"C_PROJECT") != null && results.get(dic+"C_PROJECT") instanceof EnumValues) {
			projectId = ((EnumValues) results.get(dic+"C_PROJECT")).getSelectedId();
		}

		if (Strings.isNullOrEmpty(projectId) && Strings.isNullOrEmpty(userId)) {
            results.put(dic+"C_BUDGET", EMPTY_ENUM_VALUE);
            results.put(dic+"C_POSITION", EMPTY_ENUM_VALUE);
            results.put(dic+"C_RESOURCE", EMPTY_ENUM_VALUE);
            results.put(dic+"C_FUND_SOURCE", EMPTY_ENUM_VALUE);
		} else if (Strings.isNullOrEmpty(projectId)) {
			results.put(dic+"C_PROJECT", EMPTY_ENUM_VALUE);
			results.put(dic+"C_BUDGET", EMPTY_ENUM_VALUE);
			results.put(dic+"C_POSITION", EMPTY_ENUM_VALUE);
			results.put(dic+"C_RESOURCE", EMPTY_ENUM_VALUE);
            DwrUtils.setRefValueEnumOptions(values, results, "C_PROJECT", dic, "C_FUND_SOURCE", FUND_SOURCES_TABLE_NAME, DataBaseEnumField.REF_ID_FOR_ALL_FIELD, EMPTY_ENUM_VALUE);
        } else {
			results.put(dic + "C_USER", EMPTY_ENUM_VALUE);
            DwrUtils.setRefValueEnumOptions(values, results, "C_PROJECT", dic, "C_FUND_SOURCE", FUND_SOURCES_TABLE_NAME, null, EMPTY_ENUM_VALUE);
            DwrUtils.setRefValueEnumOptions(values, results, "C_PROJECT", dic, "C_BUDGET", CostInvoiceLogic.PROJECT_BUDGETS_TABLE_NAME, null, EMPTY_ENUM_VALUE);
            DwrUtils.setRefValueEnumOptions(values, results, "C_BUDGET", dic, "C_POSITION", CostInvoiceLogic.PROJECT_BUDGET_POSITIONS_TABLE_NAME, null, EMPTY_ENUM_VALUE);
            DwrUtils.setRefValueEnumOptions(values, results, "C_POSITION", dic, "C_RESOURCE", CostInvoiceLogic.PROJECT_BUDGET_POSITION_RESOURCES_TABLE_NAME, null, EMPTY_ENUM_VALUE);
        }

	}

    protected void putDictionaryValues(Map<String, Object> values, FieldsManager fm, String dicFieldCn) throws EdmException {
        values.put(dicFieldCn, fm.getValue(dicFieldCn));
    }

    public BigDecimal assignOrdinalNumberAndGetPositionSum(FieldsManager fm) throws EdmException {
        return assignOrdinalNumberAndGetPositionSum(fm, BP_FIELD);
    }

    public void assignOrdinalNumber(FieldsManager fm, String dictionaryCn) throws EdmException {
        assignOrdinalNumberAndGetPositionSum(fm, dictionaryCn);
    }

    public BigDecimal assignOrdinalNumberAndGetPositionSum(FieldsManager fm, String dictionaryCn) throws EdmException {
        List<Long> pozycjeFaktury = (List) fm.getKey(dictionaryCn);
        BigDecimal positionSum = BigDecimal.ZERO;
        if (pozycjeFaktury != null) {
            UlBudgetPosition bpInstance = UlBudgetPosition.getInstance();
            int maxItemNo = 0;
            Set<Integer> items = Sets.newHashSet();
            for (Long bpId : pozycjeFaktury) {
                UlBudgetPosition bp = bpInstance.find(bpId);

                if (bp.getIAmount() != null) {
                    positionSum = positionSum.add(bp.getIAmount());
                }

                if (bp.getINo() != null) {
                    items.add(bp.getINo());
                    if (bp.getINo() > maxItemNo) {
                        maxItemNo = bp.getINo();
                    }
                }
            }
            int i = 1;
            for (Long bpId : pozycjeFaktury) {
                UlBudgetPosition bp = bpInstance.find(bpId);
                if (bp.getINo() == null) {
                    while (items.contains(i)) {
                        i++;
                    }
                    items.add(i);
                    bp.setINo(i);
                    DSApi.context().session().save(bp);
                    i++;
                }
            }
        }
        return positionSum;
    }

    protected void reloadDBEnumFieldData(DocumentKind kind, String fieldCn) {
        try {
            pl.compan.docusafe.core.dockinds.field.Field accountField = kind.getFieldByCn(fieldCn);
            reloadDBEnumFieldData(((DataBaseEnumField) accountField).getTableName());
        } catch (Exception e) {
            CostInvoiceLogic.log.error(e.getMessage(), e);
        }
    }

    protected void reloadDBEnumFieldData(String tableName) {
        try {
            DataBaseEnumField.reloadForTable(tableName);
        } catch (Exception e) {
            CostInvoiceLogic.log.error(e.getMessage(), e);
        }
    }

    protected void calcStawkiVat(String dicName, Map<String, FieldData> values, Map<String, Object> results) {
        Object netto = values.get(dicName + "_NETTO").getData();

        if (netto == null)
            return;

        Object stawkaVat = values.get(dicName + "_STAWKA").getData();
        if (stawkaVat == null)
            return;

        try {
            Object brutto = values.get(dicName + "_BRUTTO").getData();
            EnumItem stawkaVatItem = DataBaseEnumField.getEnumItemForTable(VAT_RATES_TABLE_NAME, Integer.parseInt(stawkaVat.toString()));
            BigDecimal stawkVatB = new BigDecimal(stawkaVatItem.getCentrum());
            BigDecimal nettoB = new BigDecimal(netto.toString());
            BigDecimal kwotaVatB = null;
            BigDecimal bruttoB = nettoB;
            if (stawkVatB.compareTo(BigDecimal.ZERO) == 1) {
                kwotaVatB = nettoB.multiply(stawkVatB.divide(new BigDecimal(100))).setScale(2, RoundingMode.HALF_UP);
                bruttoB = nettoB.add(kwotaVatB);
            } else
                kwotaVatB = BigDecimal.ZERO;



            if (brutto == null || Strings.isNullOrEmpty(brutto.toString())) {
                results.put(dicName + "_BRUTTO", bruttoB);
            } else {
                BigDecimal bruttoBFromForm = new BigDecimal(brutto.toString());
                kwotaVatB = bruttoBFromForm.subtract(nettoB);
            }

            results.put(dicName + "_KWOTA_VAT", kwotaVatB);

            Object dzialalnosc = values.get(dicName + "_DZIALALNOSC").getData();

            if (nettoB != null && dzialalnosc != null) {
                BigDecimal ratio = values.get(dicName + "_OBCIAZENIE_RATIO")!=null && values.get(dicName + "_OBCIAZENIE_RATIO").getMoneyData()!=null?values.get(dicName + "_OBCIAZENIE_RATIO").getMoneyData():BigDecimal.ZERO;

                if (dzialalnosc.equals("343")) {
                    ratio = new BigDecimal(0.00);
                }
                if (dzialalnosc.equals("342")) {
                    ratio = new BigDecimal(100.00);
                }
                if (dzialalnosc.equals("340")) {
                    ratio = new BigDecimal(11.00);
                }
                if (dzialalnosc.equals("344")) {
                    ratio = new BigDecimal(90.00);
                }
                results.put(dicName + "_OBCIAZENIE_RATIO", ratio);

                BigDecimal obciazenie = nettoB.add(kwotaVatB.multiply(BigDecimal.ONE.subtract(ratio.divide(new BigDecimal(100)))).setScale(2, RoundingMode.HALF_UP));
                results.put("STAWKI_VAT_OBCIAZENIE", obciazenie);
            }
            if (dzialalnosc == null) {
                results.put(dicName + "_OBCIAZENIE", "");
                results.put(dicName + "_OBCIAZENIE_RATIO", "");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    String getUsernameFromRecipient(Recipient worker) {
        if (worker.getLparam() != null) {
            Pattern pattern = Pattern.compile("(u:)?([^;]+).*");
            Matcher matcher = pattern.matcher(worker.getLparam());
            while(matcher.matches()) {
                return matcher.group(2);
            }
        }
        return "";
    }

    static class VatRate {
        private Integer rateId;
        private Integer taxId;
        private BigDecimal netAmount;
        private BigDecimal grossAmount;
        private BigDecimal vatAmount;
        private boolean blank;

        public VatRate(boolean blank) {
            this.blank = blank;
        }

        public VatRate(Integer rateId, BigDecimal netAmount, BigDecimal grossAmount, BigDecimal vatAmount, Integer taxId) {
            Preconditions.checkNotNull(rateId, "Stawka VAT nie zosta�a wybrana");
            Preconditions.checkNotNull(netAmount, "Kwota netto nie zosta�a okre�lona");
            Preconditions.checkNotNull(grossAmount, "Kwota brutto nie zosta�a okre�lona");
            Preconditions.checkNotNull(vatAmount, "Kwota VAT nie zosta�a okre�lona");
            this.rateId = rateId;
            this.taxId = taxId;
            this.netAmount = netAmount;
            this.grossAmount = grossAmount;
            this.vatAmount = vatAmount;
            this.blank = false;
        }

        public Integer getRateId() {
            return rateId;
        }

        public void setRateId(Integer rateId) {
            this.rateId = rateId;
        }

        public Integer getTaxId() {
            return taxId;
        }

        public BudgetItem.TaxType getTaxType() {
            if (((Integer)340).equals(taxId)) {
                return BudgetItem.TaxType.UNDEFINED;
            } else if (((Integer)341).equals(taxId)) {
                return BudgetItem.TaxType.UNDEFINED;
            } else if (((Integer)344).equals(taxId)) {
                return BudgetItem.TaxType.UNDEFINED;
            } else if (((Integer)342).equals(taxId)) {
                return BudgetItem.TaxType.TAXABLE;
            } else if (((Integer)343).equals(taxId)) {
                return BudgetItem.TaxType.TAXFREE;
            }
            return null;
        }

        public void setTaxId(Integer taxId) {
            this.taxId = taxId;
        }

        public BigDecimal getNetAmount() {
            return netAmount;
        }

        public void setNetAmount(BigDecimal netAmount) {
            this.netAmount = netAmount;
        }

        public BigDecimal getGrossAmount() {
            return grossAmount;
        }

        public void setGrossAmount(BigDecimal grossAmount) {
            this.grossAmount = grossAmount;
        }

        public boolean isBlank() {
            return blank;
        }

        public boolean isFilled() {
            return !blank;
        }

        public void setBlank(boolean blank) {
            this.blank = blank;
        }

        /**
         * @return the vatAmount
         */
        public BigDecimal getVatAmount() {
            return vatAmount;
        }

        /**
         * @param vatAmount the vatAmount to set
         */
        public void setVatAmount(BigDecimal vatAmount) {
            this.vatAmount = vatAmount;
        }

        public String getRateCodeOrDefault() throws EdmException {
            return isBlank() ? "0" : DataBaseEnumField.getEnumItemForTable(CostInvoiceLogic.VAT_RATES_TABLE_NAME, getRateId()).getCn();
        }

        public static VatRate newBlank() {
            return new VatRate(true);
        }
    }

    List<VatRate> getVatRateCode(Long docId, Long bpId) {
        return getVatRateCode(docId, bpId, COSTINVOICE_MULTIPLE_VALUE_TABLE);
    }
    List<VatRate> getVatRateCode(Long docId, Long bpId, String multipleValueTable) {
        List<VatRate> rates = Lists.newArrayList();
        PreparedStatement ps = null;
        try {

            ps = DSApi.context().prepareStatement("select stawka, netto, brutto, kwota_vat, dzialalnosc from dsg_stawki_vat_faktury v join "+multipleValueTable+" mu on mu.field_val=v.id and mu.field_cn=? and mu.document_id=? where v.POZYCJAID = ?");
            ps.setString(1, STAWKI_VAT_FIELD_NAME);
            ps.setLong(2, docId);
            ps.setLong(3, bpId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                rates.add(new VatRate(rs.getInt(1), rs.getBigDecimal(2), rs.getBigDecimal(3), rs.getBigDecimal(4), rs.getInt(5)));
            }
            rs.close();
            ps.close();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        DbUtils.closeQuietly(ps);
        return rates;
    }
}
