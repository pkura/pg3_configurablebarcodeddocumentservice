package pl.compan.docusafe.parametrization.ul;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.parametrization.ul.hib.UlBudgetsFromUnits;

import java.util.Map;

import static pl.compan.docusafe.parametrization.ul.CostInvoiceLogic.BP_FIELD;
import static pl.compan.docusafe.parametrization.ul.CostInvoiceLogic.EMPTY_ENUM_VALUE;
import static pl.compan.docusafe.parametrization.ul.CostInvoiceLogic.FUND_SOURCES_TABLE_NAME;
import static pl.compan.docusafe.parametrization.ul.OrderLogic.UPOWAZNIONY_TABLE_NAME;

/**
 * Created by kk on 20.02.14.
 */
public class OrderBudgetPositionDicitionary extends DwrDictionaryBase {

    @Override
    public void filterAfterGetValues(Map<String, Object> dictionaryFilteredFieldsValues, String dicitonaryEntryId, Map<String, Object> fieldsValues, Map<String, Object> documentValues) {
        super.filterAfterGetValues(dictionaryFilteredFieldsValues, dicitonaryEntryId, fieldsValues, documentValues);
        Map<String,Object> newResults = Maps.newHashMap();

        String dic = BP_FIELD+"_";
        String projectId = null;
        String userId = null;
        String budgetKoId = null;

        DwrUtils.setRefValueEnumOptions(dictionaryFilteredFieldsValues, newResults, "C_MPK", dic, "C_STRUCTURE_BUDGET_KO", UlBudgetsFromUnits.VIEW_NAME, null, AbstractUlDocumentLogic.EMPTY_NOT_HIDDEN_ENUM_VALUE, "_1");
        DwrUtils.setRefValueEnumOptions(dictionaryFilteredFieldsValues, newResults, "C_MPK", dic, "C_PROJECT", CostInvoiceLogic.PROJECTS_TABLE_NAME, null, AbstractUlDocumentLogic.EMPTY_NOT_HIDDEN_ENUM_VALUE, "_1");

        if (newResults.get(dic+"C_STRUCTURE_BUDGET_KO") != null && newResults.get(dic+"C_STRUCTURE_BUDGET_KO") instanceof EnumValues) {
            budgetKoId = ((EnumValues) newResults.get(dic+"C_STRUCTURE_BUDGET_KO")).getSelectedId();
        }

        if (newResults.get(dic+"C_PROJECT") != null && newResults.get(dic+"C_PROJECT") instanceof EnumValues) {
            projectId = ((EnumValues) newResults.get(dic+"C_PROJECT")).getSelectedId();
        }

        if (Strings.isNullOrEmpty(projectId) && Strings.isNullOrEmpty(budgetKoId)) {
            newResults.put(dic + "C_STRUCTURE_BUDGET_POSITION", EMPTY_ENUM_VALUE);
            newResults.put(dic + "C_STRUCTURE_BUDGET_POSITION_RAW", EMPTY_ENUM_VALUE);
            newResults.put(dic + "C_BUDGET", EMPTY_ENUM_VALUE);
            newResults.put(dic + "C_POSITION", EMPTY_ENUM_VALUE);
            newResults.put(dic + "C_RESOURCE", EMPTY_ENUM_VALUE);
            newResults.put(dic + "C_RESOURCE_RAW", EMPTY_ENUM_VALUE);
            newResults.put(dic + "C_FUND_SOURCE", EMPTY_ENUM_VALUE);
        } else if (Strings.isNullOrEmpty(projectId)) {
            newResults.put(dic + "C_PROJECT", EMPTY_ENUM_VALUE);
            newResults.put(dic + "C_BUDGET", EMPTY_ENUM_VALUE);
            newResults.put(dic + "C_POSITION", EMPTY_ENUM_VALUE);
            newResults.put(dic + "C_RESOURCE", EMPTY_ENUM_VALUE);
            newResults.put(dic + "C_RESOURCE_RAW", EMPTY_ENUM_VALUE);
            //DwrUtils.setRefValueEnumOptions(dictionaryFilteredFieldsValues, newResults, "C_STRUCTURE_BUDGET_KO", dic, "C_STRUCTURE_BUDGET_POSITION", StructureBudget.STRUCTURE_BUDGET_POSITION_VIEW_NAME, null, EMPTY_ENUM_VALUE, "_1");
            AbstractUlDocumentLogic.reloadRefEnumItems(dictionaryFilteredFieldsValues, newResults, BP_FIELD, "C_STRUCTURE_BUDGET_POSITION_RAW", "C_STRUCTURE_BUDGET_KO", "ul_order", AbstractUlDocumentLogic.EMPTY_NOT_HIDDEN_ENUM_VALUE);
            DwrUtils.setRefValueEnumOptions(dictionaryFilteredFieldsValues, newResults, "C_MPK", dic, "C_USER", UPOWAZNIONY_TABLE_NAME, null, AbstractUlDocumentLogic.EMPTY_NOT_HIDDEN_ENUM_VALUE, "_1");
            DwrUtils.setRefValueEnumOptions(dictionaryFilteredFieldsValues, newResults, "C_PROJECT", dic, "C_FUND_SOURCE", FUND_SOURCES_TABLE_NAME, DataBaseEnumField.REF_ID_FOR_ALL_FIELD, AbstractUlDocumentLogic.EMPTY_NOT_HIDDEN_ENUM_VALUE, "_1");
        } else {
            newResults.put(dic + "C_USER", EMPTY_ENUM_VALUE);
            newResults.put(dic + "C_STRUCTURE_BUDGET_KO", EMPTY_ENUM_VALUE);
            newResults.put(dic + "C_STRUCTURE_BUDGET_POSITION", EMPTY_ENUM_VALUE);
            newResults.put(dic + "C_STRUCTURE_BUDGET_POSITION_RAW", EMPTY_ENUM_VALUE);
            DwrUtils.setRefValueEnumOptions(dictionaryFilteredFieldsValues, newResults, "C_PROJECT", dic, "C_FUND_SOURCE", FUND_SOURCES_TABLE_NAME, null, AbstractUlDocumentLogic.EMPTY_NOT_HIDDEN_ENUM_VALUE, "_1");
            DwrUtils.setRefValueEnumOptions(dictionaryFilteredFieldsValues, newResults, "C_PROJECT", dic, "C_BUDGET", CostInvoiceLogic.PROJECT_BUDGETS_TABLE_NAME, null, AbstractUlDocumentLogic.EMPTY_NOT_HIDDEN_ENUM_VALUE, "_1");
            DwrUtils.setRefValueEnumOptions(dictionaryFilteredFieldsValues, newResults, "C_BUDGET", dic, "C_POSITION", CostInvoiceLogic.PROJECT_BUDGET_POSITIONS_TABLE_NAME, null, AbstractUlDocumentLogic.EMPTY_NOT_HIDDEN_ENUM_VALUE, "_1");
            //DwrUtils.setRefValueEnumOptions(values, results, "C_POSITION", dic, "C_RESOURCE", CostInvoiceLogic.PROJECT_BUDGET_POSITION_RESOURCES_TABLE_NAME, null, EMPTY_ENUM_VALUE);

            AbstractUlDocumentLogic.reloadRefEnumItems(dictionaryFilteredFieldsValues, newResults, BP_FIELD, "C_RESOURCE_RAW", "C_POSITION", "ul_order", AbstractUlDocumentLogic.EMPTY_NOT_HIDDEN_ENUM_VALUE);
        }
        dictionaryFilteredFieldsValues.putAll(newResults);
    }
}
