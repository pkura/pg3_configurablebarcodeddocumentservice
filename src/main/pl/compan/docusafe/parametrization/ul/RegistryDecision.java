package pl.compan.docusafe.parametrization.ul;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class RegistryDecision implements DecisionHandler {

	private static final long serialVersionUID = 1L;
	protected static Logger log = LoggerFactory.getLogger(RegistryDecision.class);
	
	private static final String DOKUMENT_PAPIEROWY = "PAPIER"; 

	@Override
	public String decide(OpenExecution openExecution) {
		try {
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			
			if (fm.getBoolean(DOKUMENT_PAPIEROWY))
				return "registry";
			else
				return "recipient";
			
		} catch (DocumentNotFoundException e) {
			log.error(e.getMessage(), e);
			return "error";
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
			return "error";
		}
	}

}
