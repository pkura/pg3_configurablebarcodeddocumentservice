package pl.compan.docusafe.parametrization.pika.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.pika.NoEmailException;
import pl.compan.docusafe.parametrization.pika.PikaLogic;
import pl.compan.docusafe.parametrization.plg.AbstractPlgLogic;
import pl.compan.docusafe.parametrization.plg.jbpm.PlgDocumentDecision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class PikaDocumentDecision implements DecisionHandler
{
	public static final String TO_USER = "to user";
	public static final String TO_GUID = "to guid";
	public static final String TO_END = "to end";
	private final static Logger log = LoggerFactory.getLogger(PikaDocumentDecision.class);
	
	public String decide(OpenExecution execution)
	{
		try
		{
			Document document = Jbpm4Utils.getDocument(execution);
			FieldsManager fm = document.getFieldsManager();
			
			DSUser user = PikaLogic.sendDecisionEmail(document);
			
			if(user != null)
				return TO_USER;
			
			log.info("tick");
		}catch(NoEmailException e){
			log.info("tick2 guid");
			return TO_GUID; 
		}
		catch(Exception e ){
			log.error("", e);
			
		}
		
		return TO_END;
	}

}
