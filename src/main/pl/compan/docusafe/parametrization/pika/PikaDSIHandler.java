package pl.compan.docusafe.parametrization.pika;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.service.imports.dsi.DSIAttribute;
import pl.compan.docusafe.service.imports.dsi.DSIBean;
import pl.compan.docusafe.service.imports.dsi.DSIImportHandler;
import pl.compan.docusafe.service.imports.dsi.ImportValidationException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PikaDSIHandler extends DSIImportHandler {

	private final static Logger log = LoggerFactory.getLogger(PikaDSIHandler.class);
	private final static String _SQL = "SELECT DOCUMENT_ID FROM DSG_PIKA WHERE KOD_PIKA = ?";
	public final static String KOD_PIKA = PikaLogic._KOD_PIKA;
	public final static String TYP = PikaLogic._TYP;
	
	public PikaDSIHandler() {
		super();
	}

	@Override
	protected boolean isStartProces()
	{
		return true;
	}
	
	@Override
	protected void prepareImport() throws Exception 
	{
		Long documentId = getDocumentIdByCode();
		if(documentId != null)
			importDocument = Document.find(documentId);
	}

	@Override
	public String getDockindCn() {
		return DocumentLogicLoader.PIKA_DOCKIND;
	}

	@Override
	public void actionAfterCreate(Long documentId, DSIBean dsiB) throws Exception {
		try
		{
			String filePath = Docusafe.getAdditionProperty("importDocument.filePath");
			String destFilePath = Docusafe.getAdditionProperty("importDocument.moveFile");
			
			File file = new File(filePath + "\\" + dsiB.getFilePath());
			
			if(!file.exists())
				return;
			
			File destFolder = new File(destFilePath + "\\" + DateUtils.folderDateFormat.format(new Date()));
			
			if(!destFolder.exists())
				if(!destFolder.mkdir())
				{
					log.error("can't create folder");
					return;
				}
			
			File destFile = new File(destFolder.getPath() + "\\" + dsiB.getFilePath());
			
			if(file.renameTo(destFile)){
				log.error("MOVED DSIbean: " + dsiB.getImportId() + "," + dsiB.getFilePath());
			}
			else
			{
				log.error("ERROR DSIbean: " + dsiB.getImportId() + "," + dsiB.getFilePath());
			}
			
		}catch(Exception e){
			log.error("Nie uda�o si� przeniesc", e);
		}
	}

	
	@Override
	protected void setUpDocumentBeforeCreate(Document document) throws Exception
	{
		if(importDocument == null)
			super.setUpDocumentBeforeCreate(document);
	}

	@Override
	public void supplyImport(File file, DSIBean dsiBean) throws Exception {
		String fileName = file.getName();
		List<DSIAttribute> dsiAttributes = new ArrayList<DSIAttribute>();
		
		String[] fileNames = fileName.split("\\.");
		
		if(fileNames.length > 0 )
		{
			dsiAttributes.add(new DSIAttribute("KOD_PIKA", fileNames[0]));
			Long documentId = getDocumentIdByCode(fileNames[0]);
			if(documentId == null)
			{
				throw new ImportValidationException("Nie ma dokumentu o barkodzie: " + fileNames[0]);
			}
		}
		
		dsiBean.setAttributes(dsiAttributes);
		dsiBean.setBoxCode("noneed");
		dsiBean.setDocumentKind(getDockindCn());
		dsiBean.setImageArray(null);
		dsiBean.setFilePath(file.getName());
		dsiBean.setImportKind(1);
		dsiBean.setSubmitDate(new Date());
		dsiBean.setUserImportId(null);
		dsiBean.setImageName(null);
		dsiBean.setImportStatus(1);
		dsiBean.setImportId(null);
	}

	
	@Override
	public boolean supplyFileExist(File file) throws Exception
	{
		boolean ok = false;
		
		Attachment att = Attachment.findByName(file.getName());
		
		if(att != null)
		{
			att.createRevision(file, true);
			log.info("Rewizja zapisana {} " + file.getName());
		}
		else
		{
			String fileName = file.getName();
			log.info("Brak rewizji za��cznika {} " + fileName);
			
			String[] fileNames = fileName.split("\\.");
			
			if(fileNames.length > 0 )
			{
				Long documentId = getDocumentIdByCode(fileNames[0]);
				Document document = null;
				if(documentId != null)
					document = Document.find(documentId);
				
				if(document != null)
				{
					log.info("Tworz� rewizje za��cznika {} " + fileName);
                    Attachment attachment = new Attachment(file.getName());
                    document.createAttachment(attachment);
                    attachment.createRevision(file).setOriginalFilename(fileName);
				}
			}
		}
		return ok;
	}

	/**
	 * Pobiera document po kodzie pika
	 * @return
	 */
	private Long getDocumentIdByCode()
	{
		return getDocumentIdByCode((String)values.get("KOD_PIKA"));
	}
	
	private Long getDocumentIdByCode(String code)
	{
		Long docId = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			ps = DSApi.context().prepareStatement(_SQL);
			if(code == null)
				return docId;
			
			ps.setString(1, code);
			
			rs = ps.executeQuery();
			
			if(rs.next())
			{
				docId = Long.valueOf(rs.getLong(1));
			}
		}catch(Exception e){
			log.error("", e);
		}finally{
			try
			{
				if(ps !=null)
					ps.close();
				if(rs != null)
					rs.close();
			} catch (SQLException e){
				log.error(e.getMessage());
			}

		}
		return docId;
	}
}
