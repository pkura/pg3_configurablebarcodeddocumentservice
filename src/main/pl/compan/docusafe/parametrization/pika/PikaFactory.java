package pl.compan.docusafe.parametrization.pika;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jfree.util.Log;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSException;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.events.handlers.PikaMailHandler;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa zarzadzaj�ca Elementami PIKA
 * @author �ukasz Wo�niak<lukasz.wozniak@docusafe.pl>
 *
 */
public class PikaFactory implements Serializable 
{
	private static final String _ADRESAT_ID_SQL = 
			"select adresat_id from DSG_PIKA_MIASTO where ID = ?";
	private final static Logger log = LoggerFactory.getLogger(PikaFactory.class);
	/**
	 * Zwraca u�ytkownika o podanym id
	 * @param id
	 * @return
	 * @throws DSException
	 */
	public static PikaExternalUser findExternalUser(Long id)
	{
		try
		{
			PikaExternalUser user = (PikaExternalUser)DSApi.context().session().load(PikaExternalUser.class, id);
			return user;
		}catch(Exception e){
			Log.error("",e);
			return null;
		}
		
	}

	public static Long findAdresatIdByMiasto(Long id)
	{
		
		Long adresatId = null;
		try
		{
			DSApi.openAdmin();
			
			PreparedStatement ps = DSApi.context().prepareStatement(_ADRESAT_ID_SQL);
			ps.setLong(1, id);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				adresatId = rs.getLong(1);
			}
			
			ps.close();
			rs.close();
			
			DSApi._close();
		}catch(Exception e){
			log.error("", e);
		}
		return adresatId;
		
	}
	
	/**
	 * Zwraca email doradcy w s�owniku w dokumencie
	 * @param document
	 * @return
	 * @throws EdmException
	 */
	public static String getDoradcaEmail(Document document) throws EdmException
	{
		FieldsManager fm = document.getFieldsManager();
		Long nadawcaId = fm.getLongKey(PikaLogic._NADAWCA);
		
		String email = null;
		if(nadawcaId != null)
		{
			Map<String,Object> nadawca = DwrDictionaryFacade.dictionarieObjects.get(PikaLogic._NADAWCA).getValues(nadawcaId.toString());
			email = (String) nadawca.get(PikaLogic._NADAWCA + "_DORADCAEMAIL");
		}
		
		return email;
	}
	
	/**
	 * Zwraca email doradcy w s�owniku w dokumencie
	 * @param document
	 * @return
	 * @throws EdmException
	 */
	public static String getWsparcieEmail(Document document) throws EdmException
	{
		FieldsManager fm = document.getFieldsManager();
		Long nadawcaId = fm.getLongKey(PikaLogic._NADAWCA);
		
		String email = null;
		if(nadawcaId != null)
		{
			Map<String,Object> nadawca = DwrDictionaryFacade.dictionarieObjects.get(PikaLogic._NADAWCA).getValues(nadawcaId.toString());
			email = (String) nadawca.get(PikaLogic._NADAWCA + "_WSPARCIEMAIL");
		}
		
		return email;
	}
	
	/**
	 * Wysy�a wiadomo�� na wsparcie
	 * @param document
	 * @throws EdmException
	 */
	public static void addWsparcieEmail(Document document,Map<String,String[]> events) throws Exception
	{
		String email = getWsparcieEmail(document);
		if(email == null)
			email = "WSPARCIE";
		PikaLogic.addToEventList(document, PikaMailHandler.WSPARCIE, email, events);
		
	}
	
	/**
	 * Zwraca email adresata w s�owniku w dokumencie
	 * @param document
	 * @return
	 * @throws EdmException
	 */
	public static String getAdresatEmail(Document document) throws EdmException
	{
		FieldsManager fm = document.getFieldsManager();
		Long nadawcaId = fm.getLongKey(PikaLogic._ADRESAT);
		
		String email = null;
		if(nadawcaId != null)
		{
			Map<String,Object> nadawca = DwrDictionaryFacade.dictionarieObjects.get(PikaLogic._ADRESAT).getValues(nadawcaId.toString());
			email = (String) nadawca.get(PikaLogic._ADRESAT + "_EMAIL");
		}
		
		return email;
	}
	
	public static void addAdresatEmail(Document document,Map<String,String[]> events) throws Exception
	{
		String email = getAdresatEmail(document);
		
		if(email != null)
		{
			PikaLogic.addToEventList(document, PikaMailHandler.ADRESAT, email, events);
		}
	}
	
	/**
	 * Zwraca email kierownika ze slownika
	 * @param document
	 * @return
	 * @throws EdmException
	 */
	public static String getKierownikEmail(Document document) throws EdmException
	{
		FieldsManager fm = document.getFieldsManager();
		Long nadawcaId = fm.getLongKey(PikaLogic._NADAWCA);
		
		String email = null;
		if(nadawcaId != null)
		{
			Map<String,Object> nadawca = DwrDictionaryFacade.dictionarieObjects.get(PikaLogic._NADAWCA).getValues(nadawcaId.toString());
			email = (String) nadawca.get(PikaLogic._NADAWCA + "_KIEROWNIK_EMAIL");
		}
		
		return email;
	}
	/**
	 * Wysy�a email do odbiorcy
	 */
	public static void addDoradcaEmail(Document document,Map<String,String[]> events) throws Exception
	{
		String email = getDoradcaEmail(document);
		if(email == null)
			email = "DORADCA";
			
		PikaLogic.addToEventList(document, PikaMailHandler.DORADCA, email, events);
		
	}
	/** 
	 * Email do kierownika. Jesli jest taki u�ytkownik wysyla do niego link do dokumentu
	 * @param document
	 * @throws EdmException
	 */
	public static void addKierownikEmail(Document document,Map<String,String[]> events) throws Exception
	{
		String email = getKierownikEmail(document);
		if(email == null)
			email = "KIEROWNIK";
		PikaLogic.addToEventList(document, PikaMailHandler.KIEROWNIK, email, events);
		
	}
	
	/**
	 * Sprawdza jaki email wys�a� i zwraca u�ytkownika jesli istnieje w systemie 
	 * istniej�cego w systemie.
	 * @param document
	 * @param email
	 */
	public static DSUser getByEmail(String email) throws EdmException
	{
		DSUser user = null;
		if(email != null && !email.isEmpty())
		{
			try
			{
				user = DSUser.findByEmail(email);
			}catch(UserNotFoundException e){
				log.error(e.getMessage());
			}
		}
		else
		{
			log.error("adres email pusty");
		}
		
		return user;
	}
	
	public static String getOdpowiedzialnyEmail(Document document) throws EdmException
	{
		FieldsManager fm = document.getFieldsManager();
		Long nadawcaId = fm.getLongKey(PikaLogic._ODPOWIEDZIALNY);
		
		String email = null;
		if(nadawcaId != null)
		{
			Map<String,Object> nadawca = DwrDictionaryFacade.dictionarieObjects.get(PikaLogic._ODPOWIEDZIALNY).getValues(nadawcaId.toString());
			email = (String) nadawca.get(PikaLogic._ODPOWIEDZIALNY + "_EMAIL");
		}
		
		return email;
	}
	
	/** 
	 * Email do osoby odpowiedzialnej. Jesli jest taki u�ytkownik wysyla do niego link do dokumentu
	 * @param document
	 * @throws EdmException
	 */
	public static void addOdpowiedzialnyEmail(Document document,Map<String,String[]> events) throws Exception
	{
		String email = getOdpowiedzialnyEmail(document);
		
		if(email != null)
		{
			PikaLogic.addToEventList(document, PikaMailHandler.ODPOWIEDZIALNY, email, events);
		}
	}

	/**
	 * Zwraca email u�ytkownika zewn�trznego
	 * @param doc
	 * @return
	 */
	public static String getUserZewnEmail(Document doc)
	{
		FieldsManager fm = doc.getFieldsManager();
		String email = null;

		try
		{
			//id u�ytkownika zewnetrzne
			Long uzewId= fm.getLongKey(PikaLogic._UZEW);
			
			if(uzewId == null)
				throw new EdmException("Nie wybrano u�ytkownika, kt�remu ma zostac wys�ana wiadomo�� e-mail!");
			
			//pobiera obiekt u�ytkownika zewnetrznego.
			PikaExternalUser user =  PikaFactory.findExternalUser(uzewId);
			
			if(StringUtils.isEmpty(user.getEmail()))
				throw new EdmException("Wybrany u�ytkownik zewn�trzny nie ma ustawionego adresu e-mail!");
			
			email = user.getEmail();
		
		} catch (EdmException e)
		{
			log.error("", e);
		}
		
		return email;
	}
}
