package pl.compan.docusafe.parametrization.pika;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.axis.utils.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.Field.Type;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Slownik nadawc�w : KLIENT, unikatowy klucz
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 */
public class NadawcaDictionary extends DwrDictionaryBase
{
	private final static Logger log = LoggerFactory.getLogger(NadawcaDictionary.class);
	private final static String _SQL = "SELECT ID from dsg_PIKA_NADAWCA WHERE KLIENT = ?";
	
	@Override
	public long add(Map<String, FieldData> values) throws EdmException
	{
		try
		{
			String email = Docusafe.getAdditionProperty("pika.kancelariaEmail");
			
			if(email == null)
			{
				return super.add(values);
			}
			
			if(values.containsKey("NADAWCA_DICT_KLIENT") || values.containsKey("KLIENT") )
			{
				FieldData klient = values.containsKey("NADAWCA_DICT_KLIENT") ? values.get("NADAWCA_DICT_KLIENT") : values.get("KLIENT");
				String sId = values.containsKey("NADAWCA_DICT_KLIENT") ? null : "ID";
				if(klient != null && klient.getData() != null && !StringUtils.isEmpty(klient.getData().toString()))
				{
					log.info("numer kontrahenta {}", klient.getData().toString());
									
					Long id = findNadawcaByNumer(klient.getData().toString());
					if(id != null)
					{
						log.info("zwracam id {}", id);
						if(sId != null)
						{
							values.put("ID", new FieldData(Type.LONG, id));
							int i = update(values);
							if(i > 0)
								log.info("update nadawcy {}", id);
							
							values.remove("ID");
						}
						return id;
					}
				}
			}
			
		}catch(Exception e){
			log.error("", e);
		}
		return super.add(values);
	}

	/** 
	 * Zwraca numer klienta jesli o takim numerze istnieje w bazie
	 * @param numer
	 * @return
	 */
	private Long findNadawcaByNumer(String numer)
	{
		Long nadawcaId = null;
		try
		{
			PreparedStatement ps = DSApi.context().prepareStatement(_SQL);
			if(numer == null)
				return nadawcaId;
			
			ps.setString(1, numer);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next())
			{
				nadawcaId = Long.valueOf(rs.getLong(1));
			}
		}catch(Exception e){
			log.error("", e);
		}
		return nadawcaId;
	}
	
	
	private static void sendNadawcaInfoEmail()
	{
//		if(!params.containsKey("NADAWCA_DICT_FIRMA"))
//			params.put("NADAWCA_DICT_FIRMA", "");
//		params.put("DODAJACY", DSApi.context().getDSUser().asFirstnameLastnameName() + " " + DSApi.context().getDSUser().getEmail() );
//		
//		log.info("wysylanie maila o nowym kontrahencie");
//        mailer.send(email,email, null,
//                Configuration.getMail(Mail.getInstance("newNadawca.txt")), params, true);
	}
	
}
