package pl.compan.docusafe.parametrization.pika;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;
import static pl.compan.docusafe.parametrization.opzz.OpzzCn._TYTUL_CN;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.crypto.Cipher;

import org.apache.commons.lang.StringUtils;
import org.directwebremoting.util.Base64;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import com.google.common.collect.Maps;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.crypto.CryptographicFactory;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.events.handlers.PikaMailHandler;
import pl.compan.docusafe.parametrization.polcz.AssingUtils;
import pl.compan.docusafe.parametrization.wssk.MinimalLogic;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.AttachmentUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

/**
 * G��wna klasa dokumentu korespondencji
 * @author programmer
 *
 */
public class PikaLogic extends AbstractDocumentLogic {

	private final static Logger log = LoggerFactory.getLogger(PikaLogic.class);
	
	public final static String _DATA_WPLYWU = "PIKA_DATA";
	public final static String _ODBIORCA = "PIKAODBIO";
	public final static String _ADRESAT = "ADRESAT";
	public final static String _KATEGORIA = "KATEGORIA";
	public final static String _POD_KATEGORIA = "POD_KATEGORIA";
	public final static String _NADAWCA = "NADAWCA_DICT";
	public final static String _ODPOWIEDZIALNY = "ODPOWIEDZIALNY_DICT";
	public final static String _KOD_PIKA = "KOD_PIKA";
	public final static String _UZEW = "UZEW";
	public final static String _TYP = "TYP";
	public final static String _TYP_PRZESYLKI = "TYP_PRZESYLKI";
	
	public final static int skarga = 6;
	public final static int skarga_pracownik = 1;
	public final static int skarga_firma = 2;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static PikaLogic instance;
	

    public static PikaLogic getInstance() {
        if (instance == null)
            instance = new PikaLogic();
        return instance;
    }

	@Override
	public void documentPermissions(Document document) throws EdmException 
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ,"DOCUMENT_READ",ObjectPermission.GROUP,"Dokumenty - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"DOCUMENT_READ_ATT_READ",ObjectPermission.GROUP,"Dokumenty zalacznik - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY,"DOCUMENT_READ_MODIFY",ObjectPermission.GROUP,"Dokumenty - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"DOCUMENT_READ_ATT_MODIFY",ObjectPermission.GROUP,"Dokumenty zalacznik - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.DELETE,"DOCUMENT_READ_DELETE",ObjectPermission.GROUP,"Dokumenty - usuwanie"));
   	 	
   	 	for (PermissionBean perm : perms)    		
   	 	{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
   	 	}
	}

	
	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException 
	{
		document.setFolder(Folder.getRootFolder());
	}

	
	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
		log.error("--- PikaLogic : START PROCESS !!! ---- {}", event);
		try
		{
			FieldsManager fm = document.getFieldsManager();
			Map<String, Object> map = Maps.newHashMap();
			
			if(fm.getLongKey(_TYP) == 2) // typ: bez_otwierania - kierujemy na specjalny dzia�
			{
				log.error("Dekretuje bez otwierania");
				map.put(ASSIGN_DIVISION_GUID_PARAM, Docusafe.getAdditionProperty("pika.ZARZADZ_BEZ_OTWIERANIA"));
			}
			else
			{
				map.put(ASSIGN_USER_PARAM, document.getAuthor());
				map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			}
			
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
						
		}catch(Exception e)
		{
			log.error("", e);
			throw new EdmException(e.getMessage());
		}
	}
	
	private static DSUser getUserToSend(OfficeDocument document) throws NoEmailException
	{
		DSUser user = null;
		String email = null;
		try
		{
			FieldsManager fm = document.getFieldsManager();
			Long typId = fm.getLongKey(_TYP);
			Long nadawcaId = fm.getLongKey(_NADAWCA);
			Long kategoriaId = fm.getLongKey(_KATEGORIA);
			Long podKateId = fm.getLongKey(_POD_KATEGORIA);
			Long odpowiedzialnyId = fm.getLongKey(_ODPOWIEDZIALNY);
			String kancelariaEmail = Docusafe.getAdditionProperty("pika.kancelariaEmail");
			
			if(typId.equals(3L))
			{
				user = findTPADecretationUsername();
				return user;
			}else if(odpowiedzialnyId !=null && odpowiedzialnyId > 0)
			{
				email = PikaFactory.getOdpowiedzialnyEmail(document);
				if(email == null || kancelariaEmail.equalsIgnoreCase(email))
				{
					throw new NoEmailException();
				}
				user = PikaFactory.getByEmail(email);
				
				return user;
			}
			else
			if(nadawcaId != null && nadawcaId > 0)
			{
				if(kategoriaId != null && kategoriaId.intValue() == skarga)
				{
//					if(podKateId != null && podKateId.intValue() == skarga_pracownik) //skarga na pracownika
//					{
						email = PikaFactory.getKierownikEmail(document);
						if(email == null || kancelariaEmail.equalsIgnoreCase(email))
						{
							throw new NoEmailException();
						}
						user = PikaFactory.getByEmail(email);
						
						return user;
//					}
				}
				else
				{
					//pozosta�e na doradce;
					email = PikaFactory.getDoradcaEmail(document);
					if(email == null || kancelariaEmail.equalsIgnoreCase(email))
					{
						throw new NoEmailException();
					}
					user = PikaFactory.getByEmail(email);
					return user;
				}
			}
		}catch(NoEmailException e){
			throw e;
		}
		catch(Exception e){
			log.error("", e);
		}
		return user;
	}
	/**
	 * Wysy�a email powiadomienia w zale�no�ci od wartosci s�ownikowych
	 * @param document
	 * @throws DocusafeEventException 
	 */
	private static DSUser sendConfirmationEmail(Document document) throws NoEmailException, Exception
	{
		DSUser user = null;
		String email = null;
		//jesli ustawione tzn. Ze pismo idzie na na kancelarie
		boolean naKancelarie = false;
		Map<String,String[]> events = new HashMap<String, String[]>();
		try
		{
			FieldsManager fm = document.getFieldsManager();
			Long typId = fm.getLongKey(_TYP);
			Long nadawcaId = fm.getLongKey(_NADAWCA);
			Long kategoriaId = fm.getLongKey(_KATEGORIA);
			Long podKateId = fm.getLongKey(_POD_KATEGORIA);
			Long odpowiedzialnyId = fm.getLongKey(_ODPOWIEDZIALNY);
			String kancelariaEmail = Docusafe.getAdditionProperty("pika.kancelariaEmail");
			if(typId.equals(3L))
			{
				user = findTPADecretationUsername();
				if(nadawcaId != null && nadawcaId > 0 && AvailabilityManager.isAvailable("pika.tpa.notification"))
				{
					PikaFactory.addDoradcaEmail(document, events);
					PikaFactory.addWsparcieEmail(document, events);					
					PikaFactory.addAdresatEmail(document, events);
					PikaFactory.addKierownikEmail(document, events);
				}
			}else if(odpowiedzialnyId !=null && odpowiedzialnyId > 0)
			{
				email = PikaFactory.getOdpowiedzialnyEmail(document);
				if(email == null || kancelariaEmail.equalsIgnoreCase(email))
				{
					naKancelarie = true;
				}
				else
				{
					user = addToEventList(document, PikaMailHandler.ODPOWIEDZIALNY, email, events);
				}
				PikaFactory.addAdresatEmail(document, events);
			}
			else
			if(nadawcaId != null && nadawcaId > 0)
			{
				if(kategoriaId != null && kategoriaId.intValue() == skarga)
				{
					
//					if(podKateId != null && podKateId.intValue() == skarga_pracownik) //skarga na pracownika
//					{
						email = PikaFactory.getKierownikEmail(document);
						if(email == null || kancelariaEmail.equalsIgnoreCase(email))
						{
							naKancelarie=true;
						}
						else
						{
							user = addToEventList(document, PikaMailHandler.KIEROWNIK, email,events);
						}
						PikaFactory.addDoradcaEmail(document, events);
						PikaFactory.addWsparcieEmail(document, events);					
						PikaFactory.addAdresatEmail(document, events);
//					}
				}
				else
				{
					//pozosta�e na doradce;
					email = PikaFactory.getDoradcaEmail(document);
					if(email == null || kancelariaEmail.equalsIgnoreCase(email))
					{
						naKancelarie=true;
					}
					else
					{
						user = addToEventList(document, PikaMailHandler.DORADCA, email, events);
					}
					PikaFactory.addWsparcieEmail(document, events);					
					PikaFactory.addAdresatEmail(document, events);
					PikaFactory.addKierownikEmail(document, events);
				}
			}
			else{
				throw new NoEmailException();
			}
			
			if(naKancelarie)
				throw new NoEmailException();
			
		}catch(NoEmailException e){
			log.error("Brak adresu email lub adres kancelarii - dekretuje na kancelarie {}" + document.getId());
			throw e;
		}
		catch(Exception e){
			log.error("", e);
			throw e;
		}finally{
			registerEvents(document, events);
		}
		
		return user;
	}

	/**
	 * Rejestruje eventy do wys�ania wiadomosci email
	 * @param document
	 * @param events
	 */
	private static void registerEvents(Document document, Map<String, String[]> events)
	{
		log.info("dokument_id: {}, events {}",document.getId(), events.size());
			try
			{
				log.info("event: {}, doc: {}", "10", document.getId());
				if(!events.isEmpty())
					PikaMailHandler.createEvent(document.getId(), document.getId().toString(), "10", "","false");
			}catch(Exception e){ log.error("", e);}
	}

	/**
	 * Sprawdza czy uzytkownik istnieje w systemie i czy nalezy do grupy KancelariiKEOB lub KancelariaPIKA
	 * @param user
	 * @param events
	 */
	public static DSUser addToEventList(Document document, Integer typ,String email, Map<String, String[]> events) throws Exception
	{
		DSUser user = PikaFactory.getByEmail(email);
		boolean inKancelariaKEOB = user== null ? false : user.inDivisionByGuid(Docusafe.getAdditionProperty("pika.kancelariaGUID"));
		boolean inKancelariaPIKA = user == null ? false : user.inDivisionByGuid(Docusafe.getAdditionProperty("pika.kancelariaPIKA"));
		if(user == null)
		{
			if(!events.containsKey(email.trim()))
				events.put(email.trim(), new String[]{document.getId().toString(), typ.toString(), email.trim(), "false"});
		}else if(!inKancelariaKEOB && !inKancelariaPIKA){
			if(!events.containsKey(email.trim()))
				events.put(email.trim(), new String[]{document.getId().toString(), typ.toString(), email.trim(), "true"});
		}
		
		return user;
	}

	
	@Override
	public void onStartProcess(OfficeDocument document) throws EdmException {
		log.error("--- PikaLogic : START PROCESS !!! ---- {}");
		try
		{
			FieldsManager fm = document.getFieldsManager();
			Map<String, Object> map = Maps.newHashMap();
			
//			map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
			map.put(ASSIGN_DIVISION_GUID_PARAM, Docusafe.getAdditionProperty("pika.importGUID"));
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			
		}catch(Exception e)
		{
			log.error("", e);
			throw new EdmException(e.getMessage());
		}
	}
	
	
	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException 
	{
		log.info("DokumentKind: {}, DocumentId: {}, Values from JavaScript: {}", fm.getDocumentKind().getCn(), fm.getDocumentId(), values);
		
		//jesli wybral miasto w DOZIE to zwraca przydzielony mu id adresata.
		FieldData dozField = values.get("DWR_MIASTO_DOZ");
		
		if(dozField != null && dozField.getData() != null)
		{
			Long miastoDoz = Long.valueOf(dozField.getData().toString());			
			Long adresatId = PikaFactory.findAdresatIdByMiasto(miastoDoz);
			if(adresatId != null)
			{
				values.put("DWR_ADRESAT", new FieldData(Field.Type.STRING, adresatId.toString()));
			}
		}
		else
		{
			values.put("DWR_ADRESAT", null);
		}
		
		return null;
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException 
	{	
		super.setInitialValues(fm, type);
		
		Map<String, Object> values = new HashMap<String, Object>();
		values.put(_DATA_WPLYWU, Calendar.getInstance().getTime());
		fm.reloadValues(values);
	}

	@Override
	public void correctImportValues(Document document, Map<String, Object> values, StringBuilder builder)
			throws EdmException {
		super.correctImportValues(document, values, builder);
		
		
		values.put(_DATA_WPLYWU, Calendar.getInstance().getTime());
	}

	@Override
	public boolean sendMailNotification(FieldsManager fm) throws EdmException 
	{
		boolean ok = true;
		
			//id u�ytkownika zewnetrzne
			Long uzewId = fm.getLongKey(_UZEW);
			
			if(uzewId == null)
				throw new EdmException("Nie wybrano u�ytkownika, kt�remu ma zostac wys�ana wiadomo�� e-mail!");
			
			//pobiera obiekt u�ytkownika zewnetrznego.
			PikaExternalUser user =  PikaFactory.findExternalUser(uzewId);
			
			if(StringUtils.isEmpty(user.getEmail()))
				throw new EdmException("Wybrany u�ytkownik zewn�trzny nie ma ustawionego adresu e-mail!");
			
			log.error("send email:" +user);
			
			sendMail(fm, user);
		
		return ok;
	}
	
	/**
	 * Wysy�a wiadomo�� e-mail wraz za��cznikami
	 * @param fm
	 * @param user
	 * @throws EdmException
	 */
	private void sendMail(FieldsManager fm, PikaExternalUser user) throws EdmException
	{
		try
		{
			Document doc = Document.find(fm.getDocumentId());

			List<Attachment> docAtta = doc.getAttachments();
			
			if(docAtta == null || docAtta.isEmpty())
			{
				throw new EdmException("Brak za��cznik�w w dokumencie!");
			}
	        
	        PikaMailHandler.createEvent(doc.getId(),fm.getDocumentId().toString(), PikaMailHandler.USERZEWNETRZNY.toString(), user.getEmail(),"false");
	        
		}catch(EdmException e){
			throw e;
		}catch(Exception e){
			log.error("", e);
			throw new EdmException("Nie uda�o wys�a� wiadomo�ci e-mail!");
		}
	}

	@Override
	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution,
			String acceptationCn, String fieldCnValue)
	{
		boolean isOk = false;
		
		try
		{
			if("ustalenie-dekretacji".equals(acceptationCn))
			{
				try
				{
					DSUser user = getUserToSend(doc);
					
					if(user == null)
						assignable.addCandidateUser(doc.getAuthor());
					else
					{
						assignable.addCandidateUser(user.getName());
						log.error("user {}", user.getName());
					}
					
					isOk = true;
				}catch(NoEmailException e){
					String email = Docusafe.getAdditionProperty("pika.kancelariaEmail");
					log.info("dekretuje na kancelarie");
					assignable.addCandidateGroup(Docusafe.getAdditionProperty("pika.kancelariaGUID"));
					isOk = true;
				}
			}
			
		}catch(Exception e){
			log.error("", e);
		}
		return isOk;
	}
	
	public static final DSUser sendDecisionEmail(Document doc) throws EdmException, NoEmailException
	{
		DSUser user = null;
		try
		{
			user = sendConfirmationEmail(doc);
		}catch(NoEmailException e){
			String email = Docusafe.getAdditionProperty("pika.kancelariaEmail");
			log.info("dekretuje na kancelarie");
//			if(AvailabilityManager.isAvailable("pika.kancelaria.powiadomienie"))
//				PikaMailHandler.createEvent(doc.getId().toString(), PikaMailHandler.DORADCA.toString(), email,"true");
			throw e;
		}catch(Exception e)
		{
			log.error("", e);
		}
		
		return user;
	}
	
	/**
	 * Zwraca u�ytkownika na kt�rego maj� byc dekretowane pisma TPA
	 * @return
	 */
	private static DSUser findTPADecretationUsername()
	{
		try
		{
			String username =Docusafe.getAdditionProperty("typ.tpa.dekretacja.username");
			DSUser user = DSUser.findByUsername(username);
			
			return user;
		}catch(Exception e){
			log.error("problem z dekretacj� TPA", e);
		}
		
		return null;
	}
	
	public TaskListParams getTaskListParams(DocumentKind dockind, long documentId) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			FieldsManager fm = dockind.getFieldsManager(documentId);
			
			Long nadawcaId = fm.getLongKey(PikaLogic._NADAWCA);
			EnumItem kategoria = fm.getEnumItem(_KATEGORIA);
			Long podKateId = fm.getLongKey(_POD_KATEGORIA);

            setDoradcaEmailParams(params, nadawcaId, kategoria != null ? kategoria.getId().longValue(): null, podKateId); //0
            setKategoriaParams(params, kategoria);     //1
        }
		catch (Exception e)
		{
			log.error("",e);
		}
		return params;
	}

    /**
     * Ustawia nazw� kategorii na taskliste
     * @param params
     * @param kategoria
     */
    private void setKategoriaParams(TaskListParams params, EnumItem kategoria) {
        if(kategoria != null)
            params.setAttribute(kategoria.getTitle(), 1);
    }

    /**
     * Ustawia email doradcy na taskliste
     * @param params
     * @param nadawcaId
     * @param kategoriaId
     * @param podKateId
     * @throws EdmException
     */
    private void setDoradcaEmailParams(TaskListParams params, Long nadawcaId, Long kategoriaId, Long podKateId) throws EdmException {
        if(kategoriaId != null && kategoriaId.intValue() == skarga &&
            podKateId != null && podKateId.intValue() == skarga_pracownik)
        {
            params.setAttribute("", 0);
        }
        else if(nadawcaId != null)
        {
            Map<String,Object> nadawca = DwrDictionaryFacade.dictionarieObjects.get(PikaLogic._NADAWCA).getValues(nadawcaId.toString());

            String email = (String) nadawca.get(PikaLogic._NADAWCA + "_DORADCAEMAIL");

            params.setAttribute(email, 0);
        }
    }


}
