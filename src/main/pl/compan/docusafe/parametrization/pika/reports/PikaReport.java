package pl.compan.docusafe.parametrization.pika.reports;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.parametrization.pika.PikaLogic;
import pl.compan.docusafe.parametrization.plg.PlgLogic;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.SqlUtils;

public class PikaReport extends Report
{
	private final static Logger log = LoggerFactory.getLogger(PikaReport.class);
	UniversalTableDumper dumper;
	
	private Date dateFrom;
	private Date dateTo;
	
	@Override
	public void doReport() throws Exception
	{
		log.error("Pika Report");
		try
		{
			this.initValues();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		dumper = new UniversalTableDumper();
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "raport.xls");
		poiDumper.openFile(xlsFile);
		dumper.addDumper(poiDumper);
		
		/** NAGLOWEK KO, data wprowadzenia, skr�cony opis, pud�o, nadawca, odbiorca**/
		dumper.newLine();
		dumper.addText("Typ");
		dumper.addText("Typ przesy�ki");
		dumper.addText("Ilo��");
		dumper.dumpLine();
		/** NAGLOWEK:END **/
		
		PreparedStatement ps = null;
		try
		{
			int count = 0;
			ps = DSApi.context().prepareStatement(getSQL());
			ps.setDate(++count, new java.sql.Date(dateFrom.getTime()));
			Calendar cal2 = Calendar.getInstance();
			cal2.setTime(dateTo);
			cal2.add(Calendar.DAY_OF_MONTH,1);
			
			ps.setDate(++count, new java.sql.Date(cal2.getTime().getTime()));
			
			ResultSet rs = ps.executeQuery();
			DocumentKind dk = DocumentKind.findByCn(DocumentLogicLoader.PIKA_DOCKIND);
			while(rs.next())
			{
				dumper.newLine();
				dumper.addText(dk.getFieldByCn(PikaLogic._TYP).getEnumItem(rs.getInt("typ")).getTitle());
				dumper.addText(dk.getFieldByCn(PikaLogic._TYP_PRZESYLKI).getEnumItem(rs.getInt("typ_przesylki")).getTitle());
				dumper.addText(rs.getString("ilosc"));
				dumper.dumpLine();
			}
		}
		catch (Exception e) 
		{
			throw new EdmException(e);
			//e.printStackTrace();
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		dumper.closeFileQuietly();
	}
	
	private String getSQL()
	{
		StringBuffer sql = new StringBuffer();
		
		sql.append("select pika.typ 'typ' , pika.typ_przesylki 'typ_przesylki', count(*) 'ilosc' FROM DSG_PIKA pika ");
		sql.append("left join DS_DOCUMENT d ON d.id = pika.DOCUMENT_ID ");
		sql.append("WHERE d.CTIME >= ? AND d.CTIME <= ? ");
		sql.append("group by pika.typ, pika.typ_przesylki order by pika.typ, pika.typ_przesylki");
		
		return sql.toString();
	}
	
	private void initValues() throws Exception
	{
		for(ReportParameter param : getParams())
		{
			if(param.getType().equals("break-line"))
				continue;
			
			if(param.getFieldCn().equals("dateFrom"))
			{
				dateFrom = DateUtils.jsDateFormat.parse((String) param.getValue());
			}
			
			if(param.getFieldCn().equals("dateTo"))
			{
				dateTo = DateUtils.jsDateFormat.parse((String) param.getValue());
			}
			
		}
	}
}
