package pl.compan.docusafe.parametrization.pika.reports;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class PikaEnergaReport extends Report {

	private final static Logger log = LoggerFactory.getLogger(PikaEnergaReport.class);
	UniversalTableDumper dumper;
	
	private Date dateFrom;
	private Date dateTo;
	private String split;
	Map<String, String> splitChoices = new HashMap<String, String>();
	
    public void initParametersAvailableValues() throws ReportException
    {
        super.initParametersAvailableValues();
        splitChoices.put("Tak", "Tak");
        splitChoices.put("Nie", "Nie");
        
        try
        {
        	log.info("inicjalizacja parametr�w");
            for( ReportParameter rp : params )
            {
                if( rp.getFieldCn().equals("split") )
                {
                    rp.setAvailableValues(splitChoices);
                	split = (String) rp.getValue();
                }
                
                if (rp.getFieldCn().equals("dateFrom") && rp.getValue() != null) {
                    dateFrom = DateUtils.jsDateFormat.parse((String) rp.getValue());
                }

                if (rp.getFieldCn().equals("dateTo") && rp.getValue() != null) {
                    dateTo = DateUtils.jsDateFormat.parse((String) rp.getValue());
                }
            }
            
        }
        catch ( Exception e )
        {
            throw new ReportException(e);
        }
    }
    
    @Override
    public void doReport() throws Exception
    {
   	
    	log.info("Zaczynam raport pikaEnerga");
    	log.info("split = " + split);
    	log.info("dateFrom = " + new java.sql.Date(dateFrom.getTime()));
    	log.info("dateTo = " + new java.sql.Date(dateTo.getTime()));
        
        dumper = new UniversalTableDumper();
        XlsPoiDumper poiDumper = new XlsPoiDumper();
        File xlsFile = new File(this.getDestination(), "raport.xls");
        poiDumper.openFile(xlsFile);
        dumper.addDumper(poiDumper);

        dumper.newLine();
		dumper.addText("Liczba przyj�tych dokument�w przez PIKA:");
		dumper.addText(getGroupDocuments("pika", dateFrom, dateTo).toString());
		dumper.addText("Data przyj�cia od:");
		dumper.addDate(dateFrom);
		dumper.addText("Data przyj�cia do:");
		dumper.addDate(dateTo);
		dumper.dumpLine();
		
		dumper.newLine();
		dumper.addText("Liczba przyj�tych dokument�w przez Energa");
		dumper.addText(getGroupDocuments("energa", dateFrom, dateTo).toString());
		dumper.addText("Data przyj�cia od:");
		dumper.addDate(dateFrom);
		dumper.addText("Data przyj�cia do:");
		dumper.addDate(dateTo);
		dumper.dumpLine();
		
		//je�li podzia� na u�ytkownik�w
		if("Tak".equals(split)) {
			log.info("jest podzia� na u�ytkownik�w");
			dumpEntriesForGroup("pika", dateFrom, dateTo);
			dumpEntriesForGroup("energa", dateFrom, dateTo);
		}
        

        dumper.closeFileQuietly();
    }
    
    
    /**
     * zwraca liczb� dokument�w przyj�tych przez user'a
     */
    public Integer getUserDocuments(String username, Date dateFrom, Date dateTo) {
    	PreparedStatement ps = null;
    	try 
    	{
    		String sql = "select count(id) as ile from ds_document where author = ? and ctime >= ? and ctime <= ?";
    		ps = DSApi.context().prepareStatement(sql);
    		ps.setString(1, username);
    		log.info("user = " + username);
    		ps.setDate(2, new java.sql.Date(dateFrom.getTime()));
    		ps.setDate(3, new java.sql.Date(dateTo.getTime()));
    		log.info("dateFrom = " + new java.sql.Date(dateFrom.getTime()));
    		log.info("dateTo = " + new java.sql.Date(dateTo.getTime()));
    		
    		ResultSet rs = ps.executeQuery();
    		while(rs.next()) 
    		{
    			return rs.getInt("ile");
    		}
    	} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		} finally
		{
			DSApi.context().closeStatement(ps);
			dumper.closeFileQuietly();
		}
		return 0;
    }
        
    
    /**
     * zwraca liczb� dokument�w przyj�tych przez u�ytkownik�w nale��cych do danej grupy
     */
    private Integer getGroupDocuments(String groupName, Date dateFrom, Date dateTo) {
    	Integer counter = 0;
    	try {
			DSDivision group = DSDivision.findByName(groupName);
			DSUser[] users = group.getUsers();
			
			for(DSUser user : users) {
				counter += getUserDocuments(user.getName(), dateFrom, dateTo);
			}
    	} catch (EdmException ex) {
			log.error(ex.getMessage(), ex);
		}
    	
		return counter;
    }
    
    
    /**
     * wypisuje do pliku u�ytkownik�w z podanej grupy oraz liczb� przyj�tych przez nich dokument�w
     */
    private void dumpEntriesForGroup(String groupName, Date dateFrom, Date dateTo ) {
    	try {
    		//linia przerwy
    		dumper.newLine();
			dumper.dumpLine();
			
			dumper.newLine();
			dumper.addText(groupName);
			dumper.dumpLine();
			
			dumper.newLine();
			dumper.addText("Imi� i nazwisko:");
			dumper.addText("Liczba dokument�w");
			dumper.dumpLine();
			
		    DSDivision group = DSDivision.findByName(groupName);
		    log.info("znaleziona grupa = " + group.getName());
			DSUser[] users = group.getUsers();
			
			for(DSUser user : users) {
				log.info("user = " + user.getFirstnameLastnameName());
				log.info("przyj�te dokumenty = " + getUserDocuments(user.getName(), dateFrom, dateTo).toString());
				dumper.newLine();
				dumper.addText(user.getFirstnameLastnameName());
				dumper.addText(getUserDocuments(user.getName(), dateFrom, dateTo).toString());
				dumper.dumpLine();
			}
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		} 
    }
		    
}
