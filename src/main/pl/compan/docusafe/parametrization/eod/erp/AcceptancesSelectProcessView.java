package pl.compan.docusafe.parametrization.eod.erp;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.process.InstantiationParameters;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.jbpm4.ActivitiesExtensionProcessView;
import pl.compan.docusafe.process.form.ActivityController;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessInstance;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.web.common.ExtendedRenderBean;
import pl.compan.docusafe.process.form.SelectItem;

public class AcceptancesSelectProcessView extends ActivitiesExtensionProcessView {
	
	public AcceptancesSelectProcessView(InstantiationParameters ip) throws Exception {
	super(ip);
    }

    @Override
	public ExtendedRenderBean render(ProcessInstance pi, ProcessActionContext context) throws EdmException {
		return render((Jbpm4ProcessInstance) pi, context);
	}

	private ExtendedRenderBean render(Jbpm4ProcessInstance pi, ProcessActionContext context) throws EdmException {
		ExtendedRenderBean ret = super.render(pi, context);

		List<String> mpkAcceptationNames = new ArrayList<String>();
		mpkAcceptationNames.add(CostInvoiceLogic.BUDGET_ACCEPTANCE);

		Long docId = Jbpm4ProcessLocator.documentIdForExtenedProcesssId(((Jbpm4ProcessInstance) pi).getProcessInstanceId());

		List<DocumentAcceptance> acceptances = DocumentAcceptance.find(docId);

		Field docProcesses = Document.find(docId).getDocumentKind().getFieldByCn("STATUS");

		String docStatus = Document.find(docId).getFieldsManager().getEnumItemCn("STATUS");

		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		for (DocumentAcceptance itemAcceptance : acceptances) {
			itemAcceptance.initDescription();
			
			String executionInfo = processAndExecutionIdFromProcessInstance(pi.getId());
			if (executionInfo != null && mpkAcceptationNames.contains(itemAcceptance.getAcceptanceCn())) {
				String[] exec = executionInfo.split("#");

				String processName = "";
				Long executionId = null;

				if (exec.length == 2) {
					processName = exec[0];
					executionId = Long.valueOf(exec[1]);
				}

				String mpkIdRaw = Jbpm4Utils.getVariable(executionId, CostInvoiceLogic.MPK_VARIABLE_NAME);
				if (mpkIdRaw != null) {
					Long mpkId = null;
					try {
						mpkId = Long.valueOf(mpkIdRaw);
					} catch (NumberFormatException e) {
						log.error("B��d w parsowaniu zmiennej procesowej dla: " + executionInfo);
					}
					if (mpkId != null && !itemAcceptance.getObjectId().equals(mpkId)) {
						continue;
					}
				}
				
			} else if (itemAcceptance.getAcceptanceCn().equals(docStatus)) { // nie pokazywanie na li�cie akceptacji dla takiego samego etapu, co aktualnie wykonywany
				continue;
			}
			selectItems.add(ActivityController.createSelectItem(itemAcceptance.getId(), itemAcceptance.getAsFirstnameLastname(),
					itemAcceptance.getUsername(), docProcesses.getEnumItemByCn(itemAcceptance.getAcceptanceCn()).getTitle(),
					itemAcceptance.getAcceptanceCn()));
		}

		ret.putValue("selectItems", selectItems);

		return ret;
    }

	private String processAndExecutionIdFromProcessInstance(String processInstanceId) {
		Pattern p = Pattern.compile("(\\w+)(.)(\\d+)(.)(\\d+)(.)(\\d+)");
		Matcher m = p.matcher(processInstanceId);

		if (m.find()) {
			processInstanceId = m.group(1) + "#" + m.group(7);
		} else {
			p = Pattern.compile("(\\w+)(.)(\\d+)");
			m = p.matcher(processInstanceId);
			if (m.find()) {
				processInstanceId = m.group(1) + "#" + m.group(3);
			} else {
				return null;
			}
		}
		
		return processInstanceId;
	}
    /*
    private String getVariable (Long executionId, String variableName) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String result = null;
		
		try {
			ps = DSApi.context().prepareStatement("select double_value_,long_value_,string_value_,text_value_ from jbpm4_variable where EXECUTION_=? and KEY_=?");
			ps.setLong(1, executionId);
			ps.setString(2, variableName);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				if (rs.getDouble(1) != 0) {
					result = String.valueOf(rs.getDouble(1));
				} else if (rs.getLong(2) != 0) {
					result = String.valueOf(rs.getLong(2));
				} else if (rs.getString(3) != null) {
					result = rs.getString(3);
				} else if (rs.getString(4) != null) {
					result = rs.getString(4);
				}
			}
			
			if (rs.next()) {
				log.error("Znaleziono kilka wpis�w dla zmiennej: "+variableName+" o executionId: "+ executionId.toString());
			}
			
		} catch (EdmException e) {
			log.error(e.toString());
		} catch (SQLException eS) {
			log.error(eS.toString());
		} finally {
			DSApi.context().closeStatement(ps);
			DbUtils.closeQuietly(rs);
		}
    	
    	return result;
    }
     */
}
