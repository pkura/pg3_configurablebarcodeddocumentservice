package pl.compan.docusafe.parametrization.eod.erp;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.listener.EventListenerExecution;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.model.OpenProcessInstance;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.field.EnumerationField;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessDefinition;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Lists;

public class AcceptancesListener extends AbstractEventListener
{

	private static final Logger log = LoggerFactory.getLogger(AcceptancesListener.class);
	private String acception;
	private String var;
	private String type;
	private String clearVariables;
	private String takenTransition;
	
	
	class TaskInfo {
		String assigned;
		Long procInstId;
		Long execution;
		String executionId;
		Long activityId;
		
		String getAssigned() {
			return assigned;
		}
		void setAssigned(String assigned) {
			this.assigned = assigned;
		}
		Long getProcInstId() {
			return procInstId;
		}
		void setProcInstId(Long procInstId) {
			this.procInstId = procInstId;
		}
		public Long getExecution() {
			return execution;
		}
		public void setExecution(Long execution) {
			this.execution = execution;
		}
		Long getActivityId() {
			return activityId;
		}
		void setActivityId(Long activityId) {
			this.activityId = activityId;
		}
		String getExecutionId() {
			return executionId;
		}
		void setExecutionId(String executionId) {
			this.executionId = executionId;
		}
		@Override
		public String toString() {
			return "TaskInfo [assigned=" + assigned + ", procInstId=" + procInstId + ", execution=" + execution + ", executionId=" + executionId
					+ ", activityId=" + activityId + "]";
		}
	}
	
	public TaskInfo getTaskInfo(String executionId) throws EdmException {
		TaskInfo info = new TaskInfo();
		if (executionId != null) {
			NativeCriteria nc = DSApi.context().createNativeCriteria("JBPM4_TASK", "jt");

			CriteriaResult cr = nc.setProjection(NativeExps.projection().addProjection("jt.ASSIGNEE_").addProjection("jt.PROCINST_"))
					.add(NativeExps.like("jt.EXECUTION_ID_", executionId+"%")).criteriaResult();
			System.out.println("TASK");
			if (cr.next()) {
				info.setAssigned(cr.getString(0, null));
				info.setProcInstId(cr.getLong(1, null));
				if (cr.next()) {
					throw new EdmException("Znaleziono przypisanych kilku u�ytkownik�w do jednego execution'a: " + executionId);
				}
			}
		}
		return info;
	}
	
	public TaskInfo getProcInstId(String executionId) throws EdmException {
		TaskInfo info = new TaskInfo();
		if (executionId != null) {
			NativeCriteria nc = DSApi.context().createNativeCriteria("JBPM4_TASK", "jt");
			
			CriteriaResult cr = nc.setProjection(NativeExps.projection().addProjection("jt.ASSIGNEE_").addProjection("jt.PROCINST_"))
					.add(NativeExps.like("jt.EXECUTION_ID_", executionId+"%")).criteriaResult();
			System.out.println("TASK");
			if (cr.next()) {
				info.setAssigned(cr.getString(0, null));
				info.setProcInstId(cr.getLong(1, null));
				if (cr.next()) {
					throw new EdmException("Znaleziono przypisanych kilku u�ytkownik�w do jednego execution'a: " + executionId);
				}
			} else {
				//String 
			}
		}
		return info;
	}
	
	public List<TaskInfo> getActivityId(String procInstId, String userName, String actualTask) throws EdmException {
		List<TaskInfo> tasks = Lists.newArrayList();
		NativeCriteria nc = DSApi.context().createNativeCriteria("JBPM4_TASK", "jt");

		CriteriaResult cr = 
								nc.setProjection(NativeExps.projection()
									.addProjection("jt.DBID_")
									.addProjection("jt.EXECUTION_ID_")
									.addProjection("jt.EXECUTION_"))
								.addJoin(NativeExps.leftJoin("dsw_jbpm_tasklist", "t", "t.activity_key", "jt.DBID_"))
								.add(NativeExps.like("jt.EXECUTION_ID_", procInstId+"%"))
								.add(NativeExps.eq("jt.ACTIVITY_NAME_", actualTask))
								//.add(NativeExps.isNull("jt.ASSIGNEE_"))
								.add(NativeExps.eq("t.assigned_resource", userName))
								.criteriaResult();
		while (cr.next()) {
			TaskInfo item = new TaskInfo();
			item.setActivityId(cr.getLong(0, null));
			item.setExecutionId(cr.getString(1, null));
			item.setExecution(cr.getLong(2, null));
			tasks.add(item);
		}
		
		return tasks;
	}
	
	public void notify(EventListenerExecution execution) throws Exception {
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		
		OpenProcessInstance processInstance;
		String procInstId = null;
		if ((processInstance = execution.getProcessInstance()) != null) {
			procInstId = processInstance.getId();
		}
		
		//znalezienie i pchni�cie pozosta�ych execution'�w
		String executionId = execution.getId();
		List<TaskInfo> activityToComplete = getActivityId(procInstId, DSApi.context().getDSUser().getName(), acception);

//		TaskInfo actualTask = new TaskInfo();
//		actualTask.setExecutionId(executionId);
//		System.out.println("-----"+actualTask);
//		List<TaskInfo> allUserTasks = activityToComplete;
//		allUserTasks.add(actualTask);
		for (TaskInfo task : activityToComplete) {
			if (StringUtils.isNotEmpty(task.getExecutionId())) {
				String[] taskName = task.getExecutionId().split("\\.");
				if (taskName.length>=4) {
					if (taskName.length!=4) {
						task.setExecutionId(taskName[0]+"."+taskName[1]+"."+taskName[2]+"."+taskName[3]);
					}
				} else {
					throw new EdmException("B�ad w pobraniu executionID: "+task.getExecutionId()+", "+taskName+", "+taskName.length);
				}
			}
		}

		if (acception != null) {
			if (var != null && execution.getVariable(var) != null) {
				Object varObject = execution.getVariable(var);
				if (varObject instanceof Long)
					DocumentAcceptance.createAndSave(doc, DSApi.context().getPrincipalName(), acception, (Long) execution.getVariable(var));
				else if (varObject instanceof String)
					DocumentAcceptance.createAndSave(doc, DSApi.context().getPrincipalName(), acception, Long.valueOf((String) execution.getVariable(var)));
				else
					DocumentAcceptance.createAndSave(doc, DSApi.context().getPrincipalName(), acception, null);
			} else
				DocumentAcceptance.createAndSave(doc, DSApi.context().getPrincipalName(), acception, null);
		}

		// Dokonanie akpcetacji - informacja dla u�ytkownika
		AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
		ahe.setSourceUser(DSApi.context().getPrincipalName());
		ahe.setProcessName(execution.getProcessDefinitionId());
		if (type != null && "end-task".equals(type))
			ahe.setType(AssignmentHistoryEntry.EntryType.JBPM_END_TASK);
		else
			ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_ACCEPTED);
		String divisions = "";

		for (DSDivision a : DSUser.findByUsername(DSApi.context().getPrincipalName()).getOriginalDivisionsWithoutGroup()) {
			if (!divisions.equals(""))
				divisions = divisions + " / " + a.getName();
			else
				divisions = a.getName();
		}
		ahe.setSourceGuid(divisions);
		String status = null;
		try {
			status = ((EnumerationField) doc.getFieldsManager().getField("STATUS")).getEnumItemByCn(acception).getTitle();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			status = doc.getFieldsManager().getEnumItem("STATUS").getTitle();
		}

		if (status != null)
			ahe.setStatus(status);

		ahe.setCtime(new Date());

		ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
		doc.addAssignmentHistoryEntry(ahe);

		if (clearVariables != null && !clearVariables.equals("")) {
			String[] vars = clearVariables.split(",");

			for (String var : vars) {
				if (var != null && !var.equals(Jbpm4Constants.DOCUMENT_ID_PROPERTY))
					execution.removeVariable(var);
			}
		}
		
		
		ProcessDefinition pdef = doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess("read-only");//tak by�o w akcji transition'a
		for (TaskInfo task : activityToComplete) {
			if (task.getActivityId()!=null && task.getExecutionId()!=null) {
				ProcessInstance pi = pdef.getLocator().lookupForId(task.getExecutionId());
				pdef.getLogic().process(pi, ProcessActionContext.action(takenTransition).document(doc).stored(false).activityId("internal," + task.getActivityId()));
			}
		}
		
		if (AvailabilityManager.isAvailable("addToWatch")) {
			DSApi.context().watch(URN.create(Document.find(docId)));
		}
	}

	public String getAcception()
	{
		return acception;
	}

	public void setAcception(String acception)
	{
		this.acception = acception;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
