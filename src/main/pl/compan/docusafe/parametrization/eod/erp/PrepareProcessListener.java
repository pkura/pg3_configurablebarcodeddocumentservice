package pl.compan.docusafe.parametrization.eod.erp;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;

public class PrepareProcessListener implements ExternalActivityBehaviour {

	private String dictionaryField;
	
	@Override
	public void execute(ActivityExecution execution) throws Exception {
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);

		final List<Long> argSets = new LinkedList<Long>();
		FieldsManager fm = doc.getFieldsManager();

		List<Long> dictionaryIds = (List<Long>) fm.getKey(dictionaryField.toUpperCase());
		if (dictionaryIds == null || dictionaryIds.isEmpty()) {
			argSets.add(0l);
		} else {
			argSets.addAll(dictionaryIds);
		}
			
		execution.setVariable("ids", argSets);
		execution.setVariable("count", argSets.size());
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
		// TODO Auto-generated method stub

	}

}
