package pl.compan.docusafe.parametrization.eod.erp;

import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;

@SuppressWarnings("serial")
public class RefreshTaskListListener extends AbstractEventListener {

	@Override
	public void notify(EventListenerExecution exec) throws Exception {
		if (exec.hasVariable(SendDocument.IS_EXPORT) && (Boolean) exec.getVariable(SendDocument.IS_EXPORT) == true) {
			Long docId = Long.valueOf(exec.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			DSApi.openAdmin();
			DSApi.context().begin();
			OfficeDocument doc = OfficeDocument.findOfficeDocument(docId);
			Remark remark = new Remark("Faktura wyeksportowana", "admin");
			doc.addRemark(remark);
			TaskSnapshot.updateByDocument(Document.find(docId));
			exec.removeVariable(SendDocument.IS_EXPORT);
			DSApi.context().commit();
			DSApi.close();
		}
	}

}
