package pl.compan.docusafe.parametrization.eod.erp;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.PersonDictionary;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.ul.ActivityKindAcceptance;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

public class CostInvoiceLogic extends AbstractDocumentLogic {
	
	private static final long serialVersionUID = 1L;
	protected Logger log = LoggerFactory.getLogger(CostInvoiceLogic.class);
	
	public static final String BUDGET_ACCEPTANCE = "budget_acceptance";
	public static final String MPK_VARIABLE_NAME = "costCenterId";
	public static final String JEDNOSTKI_TABLE_NAME = "dsg_ul_organizational_unit";
	@Override
	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
		
		boolean assigned = false;

		// akceptacja pozycji budzetowych
		if (BUDGET_ACCEPTANCE.equals(acceptationCn))
			assigned = assignToBudgetDivisions(doc, openExecution, assignable, acceptationCn);
		
		return assigned;
	}
	public void specialSetDictionaryValues(Map<String, ?> dockindFields)
	{
		log.info("Special dictionary values: {}", dockindFields);
		if (dockindFields.keySet().contains("CONTRACTOR_DICTIONARY"))
		{
			Map<String, FieldData> m = (Map<String, FieldData>) dockindFields.get("CONTRACTOR_DICTIONARY");
			m.put("DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			m.put("ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}

	public void addSpecialInsertDictionaryValues(String dictionaryName,Map<String, FieldData> values) {
		log.info("Before Insert - Special dictionary: '{}' values: {}",
				dictionaryName, values);
	}

	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		log.info("Before Search - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.contains("CONTRACTOR"))
		{
			values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}
	
	private boolean assignToBudgetDivisions(OfficeDocument doc, OpenExecution openExecution, Assignable assignable, String acceptationCn) {
		boolean assigned = false;
		try {
			Long mpkId = null;
			mpkId = (Long) openExecution.getVariable(MPK_VARIABLE_NAME);
			
			CentrumKosztowDlaFaktury mpkInstance = CentrumKosztowDlaFaktury.getInstance();
			CentrumKosztowDlaFaktury mpk = mpkInstance.find(mpkId);

			Integer divisionID = mpk.getSubgroupId();
			if (divisionID != null && divisionID != -1) {
				String divisionCode = DataBaseEnumField.getEnumItemForTable(JEDNOSTKI_TABLE_NAME, divisionID).getCn();
				try {
					DSDivision division = DSDivision.findByCode(divisionCode);
					assignable.addCandidateGroup(division.getGuid());
					AssigneeHandler.addToHistory(openExecution, doc, null, division.getGuid());
					return true;
				} catch (DivisionNotFoundException e) {
					log.error("Nie znaleziono dzia�u o kodzie z wybranego bud�etu");
				}
			}
			
		} catch (Exception e) {
			log.error("Nie znaleziono osoby do przypisania, przypisano do admin'a");
			assignable.addCandidateUser("admin");
			return true;
		}
		return assigned;
	}

	@Override
	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
		String documentKindName = document.getDocumentKind().getName();

		perms.add(new PermissionBean(ObjectPermission.READ, documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, documentKindName + " - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, documentKindName + " zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, documentKindName + " - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, documentKindName + " zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, documentKindName + " - usuwanie"));
		Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
		perms.addAll(documentPermissions);
		this.setUpPermission(document, perms);
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException 
	{
		String docKindTitle = document.getDocumentKind().getName();
		FieldsManager fm = document.getFieldsManager();
		
		Folder folder = Folder.getRootFolder();
		
		folder = folder.createSubfolderIfNotPresent(docKindTitle);
		
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(document.getCtime()));
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(document.getCtime()));
		
		String docNumber;
		if (fm.getValue("NR_FAKTURY") != null) {
			docNumber = String.valueOf(fm.getValue("NR_FAKTURY"));
		} else {
			docNumber = "";
		}
		
		document.setTitle(docKindTitle + " " + docNumber);
		document.setDescription(docKindTitle + " " + docNumber);
		
		document.setFolder(folder);
	}
	
	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
		Map<String, Object> toReload = Maps.newHashMap();
		for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
			if (f.getDefaultValue() != null)
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));

		toReload.put("DATA_WPLYWU_FAKTURY", new Date());
		fm.reloadValues(toReload);
	}

	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
		try {
			
			StringBuilder msgBuilder = new StringBuilder();
			
			
			if (msgBuilder.length()>0)
			{
				values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
				return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
			}
		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}
		return null;
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
		try {
			Map<String, Object> processParams = Maps.newHashMap();
			processParams.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			processParams.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, processParams);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
}
