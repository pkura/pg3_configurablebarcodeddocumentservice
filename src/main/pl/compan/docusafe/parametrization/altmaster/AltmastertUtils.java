package pl.compan.docusafe.parametrization.altmaster;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import net.sourceforge.jtds.jdbc.Driver;
import oracle.jdbc.driver.OracleDriver;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.crm.Region;
import pl.compan.docusafe.core.crm.Role;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.parametrization.ilpol.DlApplicationDictionary;
import pl.compan.docusafe.parametrization.ilpol.DlContractDictionary;
import pl.compan.docusafe.util.LoggerFactory;

public class AltmastertUtils 
{
	private static final Log log = LogFactory.getLog("agent_log");
	private static Integer time;
	/** 
	 * Propertiesy przekazane do bazy danych
	 */
	private static Properties configuration = null;
	/**
	 * Czy agent jest wlaczony?
	 */
	private static boolean serviceOn = false;
	
	public static void initImport()
	{
        try
        {
        	Driver driver = new Driver();
        	DriverManager.registerDriver(driver);	            	
        }
        catch (SQLException e)
        {
        	log.error("Blad w czasie incjowania drivera",e);
        }
	}
	
    public static Connection connectToMSSQL(String url,String user,String pass) throws SQLException
    {   	
    	if(url == null)
    		initImport();
    	return DriverManager.getConnection(url, user, pass);
    }
    
    public static void disconnectMSSQL(Connection con)
    {
    	if (con!=null)
    	{
    		try
    		{ 
    			con.close();
    		} catch (Exception e) { log.error("",e);}
    	}
    }
    
    public static String getKontrahentSQL()
    {
    	return 
    		" SELECT " +
	    		" cicmpy.ID ,"+
		    	" cicmpy.cmp_code ,"+
		    	" cicmpy.cmp_name ,"+
		    	" cicmpy.VatNumber ,"+
		    	" cicmpy.cmp_tel," +
		    	" cicntp.cnt_f_tel," +
		    	" cicntp.cnt_f_mobile,"+
		    	" cicmpy.cmp_Status, " +
		    	" cicmpy.debnr, " +
		    	" cicmpy.crdnr "+
	    	" FROM " +
	    		" cicmpy LEFT JOIN cicntp ON cicmpy.cmp_wwn = cicntp.cmp_wwn "+
	    	" WHERE " +
	    		" cicmpy.sysmodified > ?";
    }
}
