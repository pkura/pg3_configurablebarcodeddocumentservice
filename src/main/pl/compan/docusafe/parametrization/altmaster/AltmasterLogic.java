package pl.compan.docusafe.parametrization.altmaster;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentWatch;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.DocumentWatch.Type;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.events.EventFactory;

public class AltmasterLogic extends AbstractDocumentLogic
{

	private static final AltmasterLogic instance = new AltmasterLogic();
	
	public static final String INDEKS_TABLE = "dsg_indeks";
	public static final String NAZWA_PRODUKTU_TABLE = "dsg_nazwa";
	
	public static final String OS_DOTYCZY_FIELD = "OS_DOTYCZY";
	public static final String OS_NADZORUJACA_FIELD = "OS_NADZORUJACA";
	
	public static AltmasterLogic getInstance() 
	{
		return instance;
	}

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		Folder folder = Folder.getRootFolder();
    	folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
    	
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(document.getCtime());
    	
    	FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
    	Set<String> watchUsers = new HashSet<String>();
    	List<Integer> osobyDotyczy = (List<Integer>) fm.getKey(OS_DOTYCZY_FIELD);
    	if (osobyDotyczy != null)
	    	for (Integer itemId : osobyDotyczy)
	    	{
	    		String username = fm.getDocumentKind().getFieldByCn(OS_DOTYCZY_FIELD).getEnumItem(itemId).getCn();
	    		watchUsers.add(username);
	    		if(!document.hasWatchAny(username))
	    		{
	    			document.addWatchUser(DocumentWatch.Type.ANY, username);
	    			EventFactory.registerEvent("immediateTrigger", "DocumentWatchSender",document.getId()+";"+username+";"+Type.DOCUMENT_MODIFIED.getValue(),null, new Date());
	    		}
	    			
			}
    	Integer osobyNad = (Integer) fm.getKey(OS_NADZORUJACA_FIELD);
    	if(osobyNad != null)
    	{
    		String username = fm.getDocumentKind().getFieldByCn(OS_NADZORUJACA_FIELD).getEnumItem(osobyNad).getCn();
    		watchUsers.add(username);
    		if(!document.hasWatchAny(username))
    		{
    			document.addWatchUser(DocumentWatch.Type.ANY, username);
    			EventFactory.registerEvent("immediateTrigger", "DocumentWatchSender",document.getId()+";"+username+";"+Type.DOCUMENT_MODIFIED.getValue(),null, new Date());
    		}
    	}
    	for(DocumentWatch watch :new HashSet<DocumentWatch>(document.getWatches()))
    	{
    		if(!watchUsers.contains(watch.getUsername()))
    		{
    			document.deleteWatch(watch);
    		}
    	}
    	folder = folder.createSubfolderIfNotPresent((String)fm.getValue("RODZAJ"));
    	folder = folder.createSubfolderIfNotPresent(""+calendar.get(Calendar.YEAR));
    	folder = folder.createSubfolderIfNotPresent(""+(calendar.get(Calendar.MONTH)+1));
    	document.setFolder(folder);
	}

   // protected String getBaseGUID(Document document) throws EdmException
    {
  //  	return;
    }
	
	public void documentPermissions(Document document) throws EdmException
	{		
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();		
		perms.add(new PermissionBean(ObjectPermission.READ,"ALTMASTER_READ",ObjectPermission.GROUP,"Dokument Altmaster - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"ALTMASTER_ATT_READ",ObjectPermission.GROUP,"Dokument Altmaster zalacznik - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY,"ALTMASTER_MODIFY",ObjectPermission.GROUP,"Dokument Altmaster - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"ALTMASTER_ATT_MODIFY",ObjectPermission.GROUP,"Dokument Altmaster zalacznik - modyfikacja"));  	 	
   	 	FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
   	    if(fm.getKey("RODZAJ")!= null)
   	    {
   	    	String preffix = null;
   	    	String dzial = null;
   	    	if(fm.getKey("POD_TYP") != null)
   	    	{
   	    		preffix = "DZIAL_PT_"+fm.getEnumItemCn("POD_TYP");
   	    		dzial = (String) fm.getValue("POD_TYP");
   	    	}
   	    	else if(fm.getKey("TYP") != null)
   	    	{
   	    		preffix = "DZIAL_T_"+fm.getEnumItemCn("TYP");
   	    		dzial = (String) fm.getValue("TYP");
   	    	}
   	    	else
   	    	{
   	    		preffix = "DZIAL_R_"+fm.getEnumItemCn("RODZAJ");
   	    		dzial = (String) fm.getValue("RODZAJ");
   	    	}
   	    	perms.add(new PermissionBean(ObjectPermission.READ,preffix+"_READ",ObjectPermission.GROUP,""+dzial+" - odczyt"));
   	   	 	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,preffix+"_ATT_READ",ObjectPermission.GROUP,""+dzial+" zalacznik - odczyt"));
   	   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY,preffix+"_MODIFY",ObjectPermission.GROUP,""+dzial+" - modyfikacja"));
   	   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,preffix+"_ATT_MODIFY",ObjectPermission.GROUP,""+dzial+" zalacznik - modyfikacja"));
   	    }
   	 	this.setUpPermission(document, perms);
	}
	
    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
        Map<String,Object> values = new HashMap<String,Object>();
        values.put("STATUS", 20);
        fm.reloadValues(values);
    }

}
