package pl.compan.docusafe.parametrization.altmaster;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;


import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.admin.dictionaries.DockindDictionaries;

/**
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 *
 */
public class ExactImport extends ServiceDriver implements Service
{
	public static final Logger log = LoggerFactory.getLogger(ExactImport.class);

	private Timer timer;
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(ExactImport.class.getPackage().getName(),null);
	private Property[] properties;
	private Integer period =5;
	private String user;
	private String pass;
	private String url;
	
    class MailTimesProperty extends Property {
        public MailTimesProperty(){
            super(SIMPLE, PERSISTENT, ExactImport.this, "period", "Co ile minut", Integer.class);
        }
        protected Object getValueSpi(){
            return period;
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (ExactImport.this){
            	if(object != null)
            		period = (Integer) object;
            	else
            		period = 10;
            }
        }
    }

    class UserProperty extends Property {
        public UserProperty(){
            super(SIMPLE, PERSISTENT, ExactImport.this, "user", "User", String.class);
        }
        protected Object getValueSpi(){
            return user;
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (ExactImport.this){
            	if(object != null)
            		user = (String) object;
            	else
            		user = "";
            }
        }
    }

    class PassProperty extends Property {
        public PassProperty(){
            super(SIMPLE, PERSISTENT, ExactImport.this, "pass", "Haslo", String.class);
        }
        protected Object getValueSpi(){
            return pass;
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (ExactImport.this){
            	if(object != null)
            		pass = (String) object;
            	else
            		pass = "";
            }
        }
    }
    
    class UrlProperty extends Property {
        public UrlProperty(){
            super(SIMPLE, PERSISTENT, ExactImport.this, "url", "URL", String.class);
        }
        protected Object getValueSpi(){
            return url;
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (ExactImport.this){
            	if(object != null)
            		url = (String) object;
            	else
            		url = "";
            }
        }
    }

	public ExactImport()
	{
		properties = new Property[] { new MailTimesProperty(),new UserProperty(), new PassProperty(),new UrlProperty() };
	}

	protected boolean canStop()
	{
		return false;
	}

	protected void start() throws ServiceException
	{
		log.info("start uslugi ExactImport");
		timer = new Timer(true);
		timer.schedule(new MailSender(), (long) 2*1000 ,period*DateUtils.MINUTE);
		console(Console.INFO, sm.getString("SystemWystartowal"));
	}

	protected void stop() throws ServiceException
	{
		log.info("stop uslugi ExactImport");
		console(Console.INFO, sm.getString("StopUslugi"));
		if(timer != null) timer.cancel();
	}

	public boolean hasConsole()
	{
		return true;
	}	
	
	
	
	
	
	class MailSender extends TimerTask
	{
		Long lastImportTime = new Long(0);
		public void run()
		{
			try
			{		
				DSApi.openAdmin();
				Long actionTime = new Date().getTime();
				lastImportTime = DSApi.context().systemPreferences().node("ExactImport").getLong("time", 10*1000)-10*1000;
				EX_import();
				DSApi.context().begin();
				DSApi.context().systemPreferences().node("ExactImport").putLong("time",actionTime);
				DSApi.context().commit();
			}
			catch(Exception e)
			{
				ExactImport.log.error("",e);
				console(Console.ERROR, "B��d [ "+e+" ]");
			}
			finally
			{
				log.trace("Petla STOP");
				DSApi._close();
			}
		}
		
		private void EX_import() throws EdmException
		{
			PreparedStatement ps = null;
			ResultSet rs = null;
			Connection con = null;
			CallableStatement stmt = null;
			try
			{
				console(Console.INFO, "Szuka indeksy i produkty");
				
				ps = con.prepareStatement("SELECT items.ID ,items.ItemCode ,items.Description,items.Condition " +
						"FROM items WHERE items.sysmodified > ? and items.Type IN ('S','B') ");
				ps.setTimestamp(1, new java.sql.Timestamp(lastImportTime));
				rs = ps.executeQuery();
				while (rs.next())
				{
					try
					{
						DSApi.context().begin();
						Long id = rs.getLong(1);
						String index = rs.getString(2);
						String nazwa = rs.getString(3);
						String status = rs.getString(4);
						console(Console.INFO, "UPDATE "+ nazwa);
						if(id == null || StringUtils.isEmpty(index) || StringUtils.isEmpty(nazwa) || StringUtils.isEmpty(status))
							throw new EdmException("Brak podstawowych parametrow");
						DockindDictionaries dd = addIndeks(index, id, status);
						updateExactId(dd.getId(), id, AltmasterLogic.INDEKS_TABLE);
						dd = addProdukt(index, nazwa, id, status);
						updateExactId(dd.getId(), id, AltmasterLogic.NAZWA_PRODUKTU_TABLE);
						DSApi.context().commit();
					}
					catch (Exception e) 
					{
						log.error("",e);
						console(Console.ERROR, e.getMessage());
						DSApi.context().rollback();
					}
				}
				DataBaseEnumField.reloadForTable(AltmasterLogic.INDEKS_TABLE);
				DataBaseEnumField.reloadForTable(AltmasterLogic.NAZWA_PRODUKTU_TABLE);
				rs.close();
				ps.close();
				console(Console.INFO, "Zakonczyl UPDATE indeks�w i produkt�w ");
				console(Console.INFO, "Szuka kontrahent�w ");
				ps = con.prepareStatement(AltmastertUtils.getKontrahentSQL());
				ps.setTimestamp(1, new java.sql.Timestamp(lastImportTime));
				rs = ps.executeQuery();
				while (rs.next())
				{
					try
					{
						DSApi.context().begin();
						Long exactId = rs.getLong(1);
						String numer = rs.getString(2);
						String nazwa = rs.getString(3);
						String nip = rs.getString(4);
						String tel1 = rs.getString(5);
						String tel2 = rs.getString(6);
						String tel3 = rs.getString(7);
						String status = rs.getString(8);
						String numerDb = rs.getString(9);
						String numerCrd = rs.getString(10);
						if(exactId == null || StringUtils.isEmpty(nazwa))
							throw new EdmException("Brak podstawowych parametrow");
						DicInvoice dic = addOrUpdateDic(exactId,numer, nazwa, nip, tel1, tel2, tel3, status,numerDb,numerCrd);
						if(dic.getId() == null)
						{
							DSApi.context().session().flush();
							DSApi.context().session().refresh(dic);
						}
						DSApi.context().commit();
						updateDicExactId(dic.getId(),exactId );
						console(Console.INFO, "KONIEC ");
					}
					catch (Exception e) 
					{
						log.error("",e);
						console(Console.ERROR, e.getMessage());
						DSApi.context().rollback();
					}
				}
			}
			catch (Exception e) 
			{
				log.error("",e);
				console(Console.ERROR, e.getMessage());
			}
			finally
			{
				DbUtils.closeQuietly(rs);
				DbUtils.closeQuietly(ps);
				console(Console.INFO, "DISCONNECT ");
				AltmastertUtils.disconnectMSSQL(con);
			}
		}
	}
	
	private void updateDicExactId(Long dicId,Long exactId) throws SQLException, EdmException
	{
		PreparedStatement psDS = null;
		try
		{
			psDS = DSApi.context().prepareStatement(" update DF_DICINVOICE set exactID = ? where id = ? ");
			psDS.setLong(1, exactId);
			psDS.setLong(2, dicId);
			psDS.executeUpdate();
		}
		finally
		{
			DbUtils.closeQuietly(psDS);
		}
	}
	
	private void updateExactId(Integer id,Long exactId,String tableName) throws SQLException, EdmException
	{
		PreparedStatement psDS = null;
		try
		{
			psDS = DSApi.context().prepareStatement(" update "+tableName+" set exactID = ? where id = ? ");
			psDS.setLong(1, exactId);
			psDS.setInt(2, id);
			psDS.executeUpdate();
		}
		finally
		{
			DbUtils.closeQuietly(psDS);
		}
	}
	
	private DicInvoice addOrUpdateDic(Long exactId,String numer,String nazwa,String nip,String tel1,String tel2,String tel3 ,String status, String numerDb, String numerCrd) throws SQLException, EdmException
	{
		PreparedStatement psDS = null;
		ResultSet rsDS = null;
		Long dsId = null;
		DicInvoice dic = null;
		try
		{
			psDS = DSApi.context().prepareStatement(" select * from DF_DICINVOICE where exactID = ? ");
			psDS.setLong(1, exactId);
			rsDS = psDS.executeQuery();
			if(rsDS.next())
			{
				console(Console.INFO, "UPDATE istniejacego "+ nazwa);
				dsId = rsDS.getLong("ID");
				dic = DicInvoice.getInstance().find(dsId);
			}
			if(dic == null)
			{
				console(Console.INFO, "Dodaje nowego "+ nazwa);
				dic = new DicInvoice();
			}
			dic.setNip(nip);
			dic.setName(nazwa);
			dic.setTelefon(getTelefon(tel1, tel2, tel3));
			String numerKontrahenta = numerDb != null ? numerDb : (numerCrd != null ? numerCrd : ""); 
			dic.setNumerKontrahenta(numerKontrahenta);
			dic.create();
			return dic;
		}
		finally
		{
			DbUtils.closeQuietly(rsDS);
			DbUtils.closeQuietly(psDS);
		}
	}
	
	private String getTelefon(String tel1,String tel2,String tel3)
	{
		return (notEmpty(tel1) ? tel1+";":"")+ (notEmpty(tel2) ? tel2+";":"")+ (notEmpty(tel3) ? tel3+";":"");
	}
	
	private boolean notEmpty(String string)
	{
		return (StringUtils.isNotEmpty(string) && !"null".equalsIgnoreCase(string) ? true:false);
	}
	
	
	/**Dodanie indeksu
	 * @throws EdmException 
	 * @throws SQLException */
	private DockindDictionaries addIndeks(String index,Long id,String status) throws SQLException, EdmException
	{
		PreparedStatement psDS = null;
		ResultSet rsDS = null;
		Integer dsId = null;
		DockindDictionaries dd = null;
		try
		{
			psDS = DSApi.context().prepareStatement("select * from "+AltmasterLogic.INDEKS_TABLE+" where exactID = ?");
			psDS.setLong(1, id);
			rsDS = psDS.executeQuery();
			if(rsDS.next())
			{
				dsId = rsDS.getInt("ID");
				dd = DockindDictionaries.find(dsId, AltmasterLogic.INDEKS_TABLE);
			}
			if(dd != null)
			{
	        	dd.setCn(index);
	        	dd.setTitle(index);
	        	dd.setCentrum(null);
	        	dd.setRefValue(null);
	        	dd.setAvailable(availableByStatus(status));
	        	dd.update(AltmasterLogic.INDEKS_TABLE); 
			}
			else
			{
				dd = new DockindDictionaries(null,index,index,null,null,availableByStatus(status));
	            dd.create(AltmasterLogic.INDEKS_TABLE);
			}
		}
		finally
		{
			DbUtils.closeQuietly(rsDS);
			DbUtils.closeQuietly(psDS);
		}
		return dd;
	}
	
	/**Dodanie indeksu
	 * @throws EdmException 
	 * @throws SQLException */
	private DockindDictionaries addProdukt(String index,String nazwa,Long id,String status) throws SQLException, EdmException
	{
		PreparedStatement psDS = null;
		ResultSet rsDS = null;
		Integer dsId = null;
		DockindDictionaries dd = null;
		try
		{
			psDS = DSApi.context().prepareStatement("select * from "+AltmasterLogic.NAZWA_PRODUKTU_TABLE+" where exactID = ?");
			psDS.setLong(1, id);
			rsDS = psDS.executeQuery();
			if(rsDS.next())
			{
				dsId = rsDS.getInt("ID");
				dd = DockindDictionaries.find(dsId, AltmasterLogic.NAZWA_PRODUKTU_TABLE);
			}
			if(dd != null)
			{
	        	dd.setCn(index);
	        	dd.setTitle(nazwa);
	        	dd.setCentrum(null);
	        	dd.setRefValue(null);
	        	dd.setAvailable(availableByStatus(status));
	        	dd.update(AltmasterLogic.NAZWA_PRODUKTU_TABLE); 
			}
			else
			{
				dd = new DockindDictionaries(null,index,nazwa,null,null,availableByStatus(status));
	            dd.create(AltmasterLogic.NAZWA_PRODUKTU_TABLE);
			}
		}
		finally
		{
			DbUtils.closeQuietly(rsDS);
			DbUtils.closeQuietly(psDS);
		}
		return dd;
	}
	
	private boolean availableByStatus(String status)
	{
		return !"E".equals(status);
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties) 
	{
		this.properties = properties;
	}
}
