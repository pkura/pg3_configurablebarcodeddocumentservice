package pl.compan.docusafe.parametrization.polcz;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ExportApplication {
	public static final String MAIN_QUERY_TEMPLATE = "select document_id, nr_wniosek, data_wypelnienia, temat, rodzaj, tryb_zamowienia, SUMA_BRUTTO, SUMA_NETTO, data_ust_wart, podst_wartosci, wnioskodawca from dsg_pcz_application_wzp where (status = 185 or status = 2000) and data_wypelnienia >= ?";
	public static final String MPK_QUERY_TEMPLATE = "select wzp.DOCUMENT_ID, v.title,mpk.amount from dsg_pcz_application_wzp as wzp JOIN dsg_pcz_application_wzp_multiple_value m ON m.DOCUMENT_ID=wzp.DOCUMENT_ID JOIN DSG_CENTRUM_KOSZTOW_FAKTURY mpk ON m.FIELD_VAL=mpk.id JOIN simple_erp_per_docusafe_bd_zrodla_view v ON mpk.ACCEPTINGCENTRUMID = v.id WHERE m.FIELD_CN = 'MPK' AND wzp.DOCUMENT_ID=?";
	public static final String CPV_QUERY_TEMPLATE = "select wzp.DOCUMENT_ID, cpv.cn from dsg_pcz_application_wzp as wzp JOIN dsg_pcz_application_wzp_multiple_value m ON m.DOCUMENT_ID=wzp.DOCUMENT_ID JOIN dsg_pcz_cpv cpv ON m.FIELD_VAL=cpv.id WHERE m.FIELD_CN = 'CPVNR' AND wzp.DOCUMENT_ID= ?";
	
	protected static Logger log = LoggerFactory.getLogger(ExportApplication.class);
	
	public Application[] getApplicationFromDate (String fromDate) {
		StringBuilder query, mpkQuery, cpvQuery, mpkRaw, cpv;
		List<Application> apps = new ArrayList<Application>();
		Application tempApp = null;
		PreparedStatement ps = null, psMpk = null, psCpv = null;
		ResultSet rs = null, rsMpk = null, rsCpv = null;
		Long documentId;
		List<String> mpkTitle = null;
		List<BigDecimal> mpkAmount = null;
		BigDecimal mpkAmountSum = null, mpkAmountTemp = null;
		
		/*
		Application[] test = new Application[2];
		test[0] = new Application();
		test[0].setAcceptPerson("Zatwierdzaj�cy");
		test[0].setApplicant("Wnioskodawca");
		test[0].setSetupAmountPerson("UsobaDokonujacaUstaleniaWartosciZamowienia");
		test[0].setApplicationNo("NrWniosku");
		test[0].setAmountForRealization(new BigDecimal(3).toString());
		test[0].setBaseForAmount("PodstawaUstaleniaWartosci");
		test[0].setCPV("CPV");
		test[0].setFundingSources("ZrodlaFinansowania");
		test[0].setOrderKind(1);
		test[0].setOrderSubject("PrzedmiotZamowienia");
		test[0].setSetupAmountDate(new Timestamp(1347141600000l));
		test[0].setStartDate(new Timestamp(1347141600000l));
		test[0].setTenderProcedure(16);
		test[1] = new Application();
		test[1].setAcceptPerson("Zatwierdzaj�cy2");
		test[1].setApplicant("Wnioskodawca2");
		test[1].setSetupAmountPerson("UsobaDokonujacaUstaleniaWartosciZamowienia2");
		test[1].setApplicationNo("NrWniosku2");
		test[1].setAmountForRealization(new BigDecimal(3).toString());
		test[1].setBaseForAmount("PodstawaUstaleniaWartosci2");
		test[1].setCPV("CPV2");
		test[1].setFundingSources("ZrodlaFinansowania2");
		test[1].setOrderKind(1);
		test[1].setOrderSubject("PrzedmiotZamowienia2");
		test[1].setSetupAmountDate(new Timestamp(1347141600000l));
		test[1].setStartDate(new Timestamp(1347141600000l));
		test[1].setTenderProcedure(16);
		return test;
		
		*/
		log.debug("getApplicationFromDate run!");
		if (fromDate != null && !fromDate.equals(""))
			log.debug("date: "+fromDate);
		else {
			log.error("Null date for ExportApplication");
			return null;
		}

		query = new StringBuilder(MAIN_QUERY_TEMPLATE);

		try {
			DSApi.openAdmin();
			DSApi.context().begin();
			log.debug("OTWARTO context");
			ps = DSApi.context().prepareStatement(query);
			ps.setString(1, fromDate);
			rs = ps.executeQuery();
			while (rs.next()) {
				tempApp = new Application();
				documentId = rs.getLong(1);
				
				// nr wniosku
				tempApp.setApplicationNo(rs.getString(2));

				//data wype�enienia
				if (rs.getDate(3) != null) {
					tempApp.setStartDate(new Timestamp(rs.getDate(3).getTime()));
				}
				
				//temat
				tempApp.setOrderSubject(rs.getString(4));

				//rodzaj
				// 0 - roboty budowlane
		        // 1 - dostawy
		        // 2 - us�ugi
				// gdzie enum'y przyjmuj� 800, 801 i 802
				if (rs.getInt(5) != 0) {
					tempApp.setOrderKind(rs.getInt(5) - 800);
				}
				//tryb zam�wienia
		        // 10 - Przetarg nieograniczony
		        // 11 - Przetarg ograniczony
		        // 12 - Negocjacje bez og�oszenia
		        // 13 - Negocjacje z og�oszeniem
		        // 14 - Zapytanie o cen�
		        // 15 - Zapytanie z wolnej r�ki
		        // 16 - Poni�ej 14000 euro
		        // 17 - Licytacja elektroniczna
		        // 18 - Dialog konkurencyjny
				// gdzie enum'y przyjmuj� 2710, 2711, 2712, ..., 2718
				if (rs.getInt(6) != 0) {
					//tempApp.setTenderProcedure(rs.getInt(6) - 2700);
					tempApp.setTenderProcedure(10); //wymuszamy, poniewa� nie ma aktualnie pola wyboru trybu zam�wienia na dockinddzie
				}
				
				//kwota przeznaczona na realizacje
				if (rs.getBigDecimal(7) != null) { //wyb�r kwoty brutto
					tempApp.setAmountForRealization(rs.getBigDecimal(7).toString());
				} else {
					tempApp.setAmountForRealization(BigDecimal.ZERO.toString());
				}
					
				//TO-DO jak ma by� zwracana data?????
				//data ustalenia warto�ci
				if (rs.getDate(9) != null) {
					tempApp.setSetupAmountDate(new Timestamp(rs.getDate(9).getTime()));
				}
				
				//podstawa ustalenia warto�ci
				tempApp.setBaseForAmount(rs.getString(10));
				
				//osoba dokonuj�ca ustalenia == wnioskodawca ????
				// Format: Nazwisko;Imie (oddzielone �rednikiem) np:
		        // Mickiewicz;Adam
		        // Nowak-Jeziora�ski;Jan
				if (rs.getLong(11) != 0) {
					DSUser user = DSUser.findById(rs.getLong(11));
					
					tempApp.setApplicant(user.getLastname()+";"+user.getFirstname());
					tempApp.setSetupAmountPerson(user.getLastname()+";"+user.getFirstname());
				}

				
				//zatwierdzaj�cy
				List<DocumentAcceptance> acceptances = DocumentAcceptance.find(documentId);
				
				for (DocumentAcceptance acc : acceptances) {
					if (acc.getAcceptanceCn().equals("quaestor")) {
						DSUser accUser  = DSUser.findByUsername(acc.getUsername());
						tempApp.setAcceptPerson(accUser.getLastname()+";"+accUser.getFirstname());
						break;
					}
				}
				
				try {
					// ---------------MPK----------------
					// ---------------MPK----------------
					// ---------------MPK----------------
					mpkTitle = new ArrayList<String>();
					mpkAmount = new ArrayList<BigDecimal>();

					mpkQuery = new StringBuilder(MPK_QUERY_TEMPLATE);
					psMpk = DSApi.context().prepareStatement(mpkQuery);
					psMpk.setLong(1,documentId);
					rsMpk = psMpk.executeQuery();

					// �r�d�a finanoswania / MPK
					// Oddzielone �rednikiem �r�d�a finansowania.
					// Propublico mo�e przyj�� dla ka�dego �r�d�a
					// jego procentowy udzia� w finansowaniu.
					// W przypadku wyst�puj�cego w nazwie �rednika, zast�pujemy
					// go �;;�
					// np.: "Zrodlo 1;20;Zrodlo 2;80" (20 i 80 to procentowy
					// udzia� �r�de�)
					while (rsMpk.next()) {
						mpkTitle.add(rsMpk.getString(2).replace(";", ";;"));
						mpkAmount.add(new BigDecimal(rsMpk.getDouble(3)));
					}

					mpkAmountSum = new BigDecimal(0);

					for (BigDecimal val : mpkAmount) {
						mpkAmountSum = mpkAmountSum.add(val);
					}

					mpkAmountTemp = new BigDecimal(0);
					mpkRaw = new StringBuilder();

					for (int i = 0; i < mpkTitle.size(); i++) {
						if (!mpkRaw.toString().equals("")) {
							mpkRaw.append(";");
						}
						mpkAmountTemp = new BigDecimal(0);
						if (!mpkAmountSum.equals(new BigDecimal(0))) {
							mpkAmountTemp = mpkAmount.get(i).divide(mpkAmountSum, 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100)).setScale(2);
						}
						log.debug(mpkAmount.toString() + " | " + mpkAmountTemp.toString());
						mpkRaw.append(mpkTitle.get(i)).append(";").append(mpkAmountTemp.toString());
					}
					tempApp.setFundingSources(mpkRaw.toString());
				} catch (Exception e) {
					log.error(e.toString());
				} finally {
					DSApi.context().closeStatement(psMpk);
				}
				
				try {
					// ---------------CPV----------------
					// ---------------CPV----------------
					// ---------------CPV----------------
					cpv = new StringBuilder();

					cpvQuery = new StringBuilder(CPV_QUERY_TEMPLATE);
					psCpv = DSApi.context().prepareStatement(cpvQuery);
					psCpv.setLong(1, documentId);
					rsCpv = psCpv.executeQuery();

					// Przyk�adowe formaty (oddzielane �rednikami dla wielu
					// kod�w cpv)
					// 32354800-0
					// 73000000-0;32354800-0
					// 32354800-0;E000-0;32354800-0
					while (rsCpv.next()) {
						if (!cpv.toString().equals("")) {
							cpv.append(";");
						}
						cpv.append(rsCpv.getString(2));
					}

					tempApp.setCPV(cpv.toString());
				} catch (Exception e) {
					log.error(e.toString());
				} finally {
					DSApi.context().closeStatement(psCpv);
				}
				log.debug(tempApp);
				apps.add(tempApp);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}   finally {
			DSApi.context().closeStatement(ps);
		}
		try {
			DSApi.context().rollback();
			DSApi.close();
		} catch (EdmException e) {
			log.error(e.toString());
		}
		
		return apps.toArray(new Application[apps.size()]);
		
	}
}
;