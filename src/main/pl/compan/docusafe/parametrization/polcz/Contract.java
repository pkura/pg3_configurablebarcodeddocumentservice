package pl.compan.docusafe.parametrization.polcz;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

public class Contract {

	private Long id;
	private String contractNo;
	private String tenderSignature;
	private boolean used; //umowa u�yta w dokumencie, brak mo�liwo�ci jej edycji
	private Set<ContractItem> contractItems = new HashSet<ContractItem>(0);

	public Contract() {

	}

	public Contract(String contractNo, String tenderSignature) {
		this.contractNo = contractNo;
		this.tenderSignature = tenderSignature;
		this.used = false;
	}

	public Contract(String contractNo, String tenderSignature, Set<ContractItem> contractItems) {
		this.contractNo = contractNo;
		this.tenderSignature = tenderSignature;
		this.contractItems = contractItems;
		this.used = false;
	}

	@Override
	public String toString() {
		return "Contract [contractNo=" + contractNo + ", tenderSignature=" + tenderSignature + ", contractItems=" + contractItems + "]";
	}

	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public String getTenderSignature() {
		return tenderSignature;
	}

	public void setTenderSignature(String tenderSignature) {
		this.tenderSignature = tenderSignature;
	}

	public Set<ContractItem> getContractItems() {
		return contractItems;
	}

	public void setContractItems(Set<ContractItem> contractItems) {
		this.contractItems = contractItems;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isUsed() {
		return used;
	}

	public void setUsed(boolean used) {
		this.used = used;
	}

	public List<Long> getContractItemsId() {
		List<Long> contractItemsId = new ArrayList<Long>();
		for (ContractItem item : contractItems) {
			contractItemsId.add(item.getId());
		}
		return contractItemsId;
	}
	
	protected static Contract find(Long id) throws EdmException {
		try
		{
			Criteria criteria = DSApi.context().session().createCriteria(Contract.class);
			if (id != null)
				criteria.add(Restrictions.eq("id", id));

			List<Contract> list = criteria.list();

			if (list.size() > 0)
			{
				return list.get(0);
			}

			return null;
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}
	
	protected static List<Contract> find(String contractNo, String tenderSignature) throws EdmException {
		try
		{
			Criteria criteria = DSApi.context().session().createCriteria(Contract.class);
			if (contractNo != null && !contractNo.equals(""))
				criteria.add(Restrictions.eq("contractNo", contractNo));
			
			if (tenderSignature != null && !tenderSignature.equals(""))
				criteria.add(Restrictions.eq("tenderSignature", tenderSignature));
			
			List<Contract> list = criteria.list();

			if (list.size() > 0)
			{
				return list;
			}

			return null;
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}
	
}
