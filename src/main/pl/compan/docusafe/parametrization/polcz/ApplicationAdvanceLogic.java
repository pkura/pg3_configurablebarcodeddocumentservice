package pl.compan.docusafe.parametrization.polcz;

public class ApplicationAdvanceLogic extends AbstractApplicationLogic
{
	private static ApplicationAdvanceLogic instance;
	
	public static ApplicationAdvanceLogic getInstance()
	{
		if (instance == null)
			instance = new ApplicationAdvanceLogic();
		return instance;
	}
}
