package pl.compan.docusafe.parametrization.polcz;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.absences.Absence;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.base.absences.AbsenceReportEntry;
import pl.compan.docusafe.core.base.absences.EmployeeAbsenceEntry;
import pl.compan.docusafe.core.base.absences.FreeDay;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.db.criteria.NativeProjection.AggregateProjection;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DSDivisionEnumField;
import pl.compan.docusafe.core.dockinds.field.DSUserEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbsenceLogic;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import com.google.common.collect.Maps;

public class WniosekUrlopLogic extends AbstractDocumentLogic implements AbsenceLogic
{
	private static WniosekUrlopLogic instance;
	protected static Logger log = LoggerFactory.getLogger(WniosekUrlopLogic.class);

	public static final String AVAILABLE_DAYS_CN = "POZOSTALO_DNI";
	public static final String FROM_CN = "DATA_OD";
	public static final String TO_CN = "DATA_DO";
	private static String linkedServer = Docusafe.getAdditionProperty("erp.database.linkedserver");
	private static String dbName = Docusafe.getAdditionProperty("erp.database.dataBase");
	private static String schema = Docusafe.getAdditionProperty("erp.database.schema");
	private static String nieobecnosciTabela = Docusafe.getAdditionProperty("erp.database.nieobecnosci");
	private static String urlopyTabela = Docusafe.getAdditionProperty("erp.database.urlopy");

	/**
	 * Lista dni wolnych
	 */
	protected List<FreeDay> freeDays;
	private List<EmployeeAbsenceEntry> emplAbsences;
	private EmployeeCard employeeCard;

	public static WniosekUrlopLogic getInstance()
	{
		if (instance == null)
			instance = new WniosekUrlopLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn() + "_DOCUMENT_READ", ObjectPermission.GROUP, "Wniosek urlopowy - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Wniosek urlopowy zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn() + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Wniosek urlopowy - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Wniosek urlopowy zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn() + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Wniosek urlopowy - usuwanie"));
		Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
		perms.addAll(documentPermissions);
		this.setUpPermission(document, perms);
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Wnioski urlopowe");
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(fm.getValue("CTIME")));
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(fm.getValue("CTIME")));
		document.setFolder(folder);

		document.setTitle("Wniosek urlopowy - " + fm.getEnumItem("WNIOSKODAWCA").getTitle());
		document.setDescription("Wniosek urlopowy - " + fm.getEnumItem("WNIOSKODAWCA").getTitle());
	}
	
	private EmployeeCard getEmployeeCard(FieldsManager fm) throws EdmException
	{
//		String author = null;
//		if (fm.getDocumentId() != null)
//			author = Document.find(fm.getDocumentId()).getAuthor();
		// pobranie obiektu u�ytkownika zglaszaj�cego wniosek
		UserImpl user;
//		if (author != null && !author.equals(""))
//			user = (UserImpl) DSUser.findByUsername(author);
//		else
			user = (UserImpl) DSApi.context().getDSUser();
		
		EmployeeCard employeeCard = (EmployeeCard) DSApi.context().session().createQuery(
				"from " + EmployeeCard.class.getName() + " em where em.user = ?")
				.setParameter(0, user)
				.setMaxResults(1)
				.uniqueResult();
		
		return employeeCard;
	}
	
	@Override
	public void onLoadData(FieldsManager fm)throws EdmException
	{
		employeeCard = getEmployeeCard(fm);
		// wyliczenie dostepnych dni
		int sumDays = 0;
		if (employeeCard != null)
		{
			try {
				Criteria criteria = DSApi.context().session().createCriteria(Absence.class);
				criteria.add(Restrictions.eq("year", GregorianCalendar.getInstance().get(GregorianCalendar.YEAR)))
						.add(Restrictions.eq("employeeCard", employeeCard));
				@SuppressWarnings("unchecked")
				List<Absence> absences = (List<Absence>) criteria.list();

				for (Absence absence : absences) {
					if (absence.getAbsenceType().isAdditional()) {
						sumDays += absence.getDaysNum();
					}
					if (absence.getAbsenceType().isNegative()) {
						sumDays -= absence.getDaysNum();
					}
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
		
		Map<String,Object> values = new HashMap<String,Object>();
		values.put(AVAILABLE_DAYS_CN, sumDays);
		fm.reloadValues(values);
		freeDays = AbsenceFactory.getFreeDays();
		int currentYear = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
		emplAbsences = AbsenceFactory.getEmployeeAbsences(employeeCard, currentYear);
	}

	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		Map<String, Object> toReload = Maps.newHashMap();
		for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
		{
			if (f.getDefaultValue() != null)
			{
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
			}
		}

//        
		DSUser user = DSApi.context().getDSUser();
		toReload.put("WNIOSKODAWCA",user.getId());
		toReload.put("LOGGED_USER", user.getId());
		try
		{
			toReload.put("WORKER_DIVISION",fm.getField("WORKER_DIVISION").getEnumItemByCn(user.getDivisionsWithoutGroupPosition()[0].getGuid()).getId());
		}
		catch(Exception e) 
		{
			log.error(e.getMessage());
		}
//		DSUserEnumField dsef = (DSUserEnumField)fm.getField("WNIOSKODAWCA");
//		if(DSApi.context().getDSUser().getDivisionsWithoutGroupPosition().length > 0)
//		{
//			dsef.setRefGuid(DSApi.context().getDSUser().getDivisionsWithoutGroupPosition()[0].getGuid());
//		}

		
		toReload.put("CTIME", new Date());
		/** wyliczenie dostepnych dni */
		int availableDays = 0;
		
		/**
		 * Opieramy sie tylko na info z
		 * SimpleErp Pobranie info z simpleErp
		 */
		availableDays = getAvailableDayFromSimpleERP(user.getExtension(), null, toReload);
		toReload.put(AVAILABLE_DAYS_CN, availableDays);
		fm.reloadValues(toReload);
	}
	
	@Override
	public List<EmployeeAbsenceEntry> getEmplAbsences() 
	{
		try
		{
			return emplAbsences;
		}
		catch(Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return null;
	}

	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		log.info("validate contents wniosek urlopowy: \n {} ", values);
		Field msg = null;

		if (!DSApi.isContextOpen())
		{
			DSApi.openAdmin();
		}
		
//		DSUserEnumField dsef = (DSUserEnumField)fm.getField("WNIOSKODAWCA");
//		if(DSApi.context().getDSUser().getDivisionsWithoutGroupPosition().length > 0)
//		{
//			dsef.setRefGuid(DSDivision.findByName(values.get("DWR_WORKER_DIVISION").getEnumValuesData().getSelectedValue()).getGuid());
//		}
		
		try
		{
//			emplAbsences = AbsenceFactory.getEmployeeAbsences(getEmployeeCard(fm), GregorianCalendar.getInstance().get(GregorianCalendar.YEAR));
			if (fm.getDocumentId() == null)
			{
				if (values.get("DWR_DATA_OD") != null && values.get("DWR_DATA_DO").getData() != null)
				{
					Date startDate = values.get("DWR_DATA_OD").getDateData();
					Date finishDate = values.get("DWR_DATA_DO").getDateData();

					if (finishDate != null && finishDate.before(startDate))
					{
						return new Field("messageField", "Data rozpocz�cia urlopu jest p�niejsza ni� data uko�czenia urlopu", Field.Type.BOOLEAN);
					}
				}

//				if (values.get("DWR_WNIOSKODAWCA") != null && values.get("DWR_WNIOSKODAWCA").getData() != null)
//				{
//					List<String> selected = new LinkedList<String>();
//					selected.add(UserImpl.find(Long.valueOf(values.get("DWR_WNIOSKODAWCA").getEnumValuesData().getSelectedOptions().get(0))).getDivisionsWithoutGroupPosition()[0].getId().toString());
//					values.get("DWR_WORKER_DIVISION").getEnumValuesData().setSelectedOptions(selected);
//					values.get("DWR_WORKER_DIVISION").setEnumValuesData(values.get("DWR_WORKER_DIVISION").getEnumValuesData());
//				}

				if (values.get("DWR_WNIOSKODAWCA") != null && values.get("DWR_WNIOSKODAWCA").getData() != null)
				{
					/**
					 *sprawdzenie czy osoba zast�puj�ca 
					 *nie jest ustawiona na osob� wnioskuj�c�
					 */
					if (values.get("DWR_OSOBA_ZASTEPUJ") != null && values.get("DWR_OSOBA_ZASTEPUJ").getData() != null)
					{
						int wnioskodawcaID = Long.valueOf(values.get("DWR_WNIOSKODAWCA").getEnumValuesData().getSelectedOptions().get(0)).intValue();
						int zastepstwoID = Long.valueOf(values.get("DWR_OSOBA_ZASTEPUJ").getEnumValuesData().getSelectedOptions().get(0)).intValue();
						if (wnioskodawcaID == zastepstwoID)
						{
							List<String> selected = new LinkedList<String>(); selected.add("");
							values.get("DWR_OSOBA_ZASTEPUJ").getEnumValuesData().setSelectedOptions(selected);
							return new Field("messageField", "Osob� zast�puj�c� nie mo�e by� osoba wnioskuj�ca", Field.Type.BOOLEAN);
						}
								
					}
					/** wyliczenie dostepnych dni */
					int availableDays = 0;
					
					/**
					 * opieramy sie tylko na info z
					 * SimpleErp Pobranie info z simpleErp
					 */
					if (values.get("DWR_WNIOSKODAWCA") != null) {
						DSUser user  = DSUser.findById(Long.valueOf(values.get("DWR_WNIOSKODAWCA").getEnumValuesData().getSelectedOptions().get(0)));
						Map<String, Object> toReload = Maps.newHashMap();
						availableDays = getAvailableDayFromSimpleERP(user.getExtension(), values, null);
						fm.reloadValues(toReload);
					}
					try
					{
						log.info("Wnio urlo: Suma dni: " + availableDays);
						values.put("DWR_" + AVAILABLE_DAYS_CN, new FieldData(Field.Type.INTEGER, availableDays));
					}
					catch (Exception e)
					{
						log.error(e.getMessage(), e);
					}
				}
				if (values.get("DWR_URLOPRODZAJ").getEnumValuesData().getSelectedId() != null && !"".equals(values.get("DWR_URLOPRODZAJ").getEnumValuesData().getSelectedId())) {
					Integer id = Integer.valueOf(values.get("DWR_URLOPRODZAJ").getEnumValuesData().getSelectedId().trim());
					if (id == 1934 || id == 1935 || id == 1937 || id == 1941) {
						if (values.get("DWR_ILOSC_DNI") != null && values.get("DWR_ILOSC_DNI").getData() != null) {
							if (values.get("DWR_ILOSC_DNI").getIntegerData() > values.get("DWR_POZOSTALO_DNI").getIntegerData()) {
								return new Field("messageField", "Liczba dni roboczych przekracza dost�pn� ilo�� dni", Field.Type.BOOLEAN);
							}
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			DSApi.close();
		}

		return msg;
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		//super.onStartProcess(document, event);
		log.info("--- DelegacjaLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
			
			// SPRAWDZENIE CZY PRZYPADKIEM NIE ISTNIEJE JU� URLOP W PODANYM
			// PRZEDZIALE DAT
			FieldsManager fm = document.getFieldsManager();
			Date startDate = (Date) fm.getValue(FROM_CN);
			Date endDate = (Date) fm.getValue(TO_CN);
			String userName = fm.getEnumItem("WNIOSKODAWCA").getCn();
			DSUser user = DSUser.findByUsername(userName);
			if (hasAbsenceInSimpleErp(user, startDate, endDate))
				throw new EdmException("Ju� istnieje wpis urlopowy w podanym zakresie dat w  Simple.ERP !");
			if (fm.getIntegerKey("URLOPRODZAJ") == 1934 || fm.getIntegerKey("URLOPRODZAJ") == 1935 || fm.getIntegerKey("URLOPRODZAJ") == 1937 || fm.getIntegerKey("URLOPRODZAJ") == 1941) {
				if (fm.getIntegerValue(AVAILABLE_DAYS_CN) <= 0)
					throw new EdmException("B��dna liczba dost�pnych dni urlopu");
			}

		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
	{

		if (fieldValues.get("DATA_OD") != null && fieldValues.get("DATA_DO") != null)
		{
			Date startDate = (Date) fieldValues.get("DATA_OD");
			Date finishDate = (Date) fieldValues.get("DATA_DO");

			if (finishDate != null && finishDate.before(startDate))
			{
				throw new EdmException("Data rozpocz�cia urlopu nie mo�e by� dat� p�niejsz� ni� data uko�czenia urlopu.");
			}
		}

		System.out.println("Rodzja urlop: " + fieldValues.get("URLOPRODZAJ") + " numer: " + fieldValues.get("NR_WNIOSKU"));
		if (fieldValues.get("URLOPRODZAJ") != null && fieldValues.get("NR_WNIOSKU") == null)
		{
			NativeCriteria nc = DSApi.context().createNativeCriteria("dsg_pcz_wni_urlop", "d");
			System.out.println(fieldValues.get("URLOPRODZAJ"));
			CriteriaResult cr = nc.setProjection(NativeExps.projection().addAggregateProjection("d.rodzaj_url", AggregateProjection.COUNT)).add(NativeExps.eq("d.rodzaj_url", fieldValues.get("URLOPRODZAJ"))).criteriaResult();
			int count = 0;
			while (cr.next())
			{
				count = cr.getInteger(0, 0);
			}
			++count;
			Statement stat = null;
			try
			{
				stat = DSApi.context().createStatement();
				stat.executeUpdate("UPDATE dsg_pcz_wni_urlop SET nr_wniosku = '" + count + "/" + DateUtils.formatYear(new java.util.Date()) + "' WHERE DOCUMENT_ID = " + documentId);
			}
			catch (SQLException e)
			{
				throw e;
			}
			finally
			{
				stat.close();
			}
		}
	}

	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			FieldsManager fm = dockind.getFieldsManager(id);
			params.setStatus((String) fm.getValue("STATUS"));
			params.setDocumentNumber(fm.getDocumentId().toString());

			TaskListParams ret = new TaskListParams();
			String wnioskujacy = fm.getValue("WNIOSKODAWCA").toString();

			params.setAttribute(wnioskujacy, 0);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return params;
	}
	


	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn)
	{
		boolean assigned = false;

		return assigned;
	}

	public boolean hasAbsenceProcess(Long userId, Date from, Date to)
	{
		return false;
	}
	
	public static int getAvailableDayFromSimpleERP(String nrEwid, Map<String, FieldData> values, Map<String, Object> toReload)
	{
		try
		{
			NativeCriteria nc = DSApi.context().createNativeCriteria(linkedServer + "." + dbName + "." + schema + "." + urlopyTabela, "d");
			nc.setProjection(NativeExps.projection()
			// ustalenie kolumn
					.addProjection("d.NrEwid") 	// >Numer Pracownika [ 6 znak�w]
					.addProjection("d.Zalegly") 	// >Zalegly - Ilo�� dni url.zaleglego [ warto��]
					.addProjection("d.Wymiar") 	// >Wymiar - Ilo�� dni url.wymiaru [ warto��]
					.addProjection("d.Nalezny")
					.addProjection("d.Pozostalo")) 	// >Nalezny - Ilo�� dni rul.naleznego [ warto��]
					// warunek where
					.add(NativeExps.eq("d.NrEwid", nrEwid));

			// pobranie i wy�wietlenie wynik�w
			CriteriaResult cr = nc.criteriaResult();
			int pozostaloDni = 0;
			BigDecimal x;
			String y;
			
			if (cr.getRowsNumber() == 0) {
				if (toReload != null)
					toReload.put("WARN", "Nie mo�na pobra� dost�pnych dni urlopu z bazy danych ERP dla danego u�ytkownika");
				if (values != null)
					values.put("DWR_WARN", new FieldData(Field.Type.STRING, "Nie mo�na pobra� dost�pnych dni urlopu z bazy danych ERP dla danego u�ytkownika"));
				return -1;
			} else if (cr.getRowsNumber() > 1) {
				if (toReload != null)
					toReload.put("WARN", "W bazie danych ERP istnieje wi�cej ni� jeden wpis dotycz�cy dostepnych dni urlopu dla danego u�ytkownika");
				if (values != null)
					values.put("DWR_WARN", new FieldData(Field.Type.STRING, "W bazie danych ERP istnieje wi�cej ni� jeden wpis dotycz�cy dostepnych dni urlopu dla danego u�ytkownika"));
			} else {
				if (toReload != null)
					toReload.put("WARN", null);
				if (values != null)
					values.put("DWR_WARN", new FieldData(Field.Type.STRING, null));
			}
			
			cr.next();
			x = (BigDecimal) cr.getValue(4, 0);
			y = x.toPlainString();
			pozostaloDni = Integer.valueOf(y.split("\\.")[0]);
			return pozostaloDni;
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			return -1;
		}
	}

	public static boolean hasAbsenceInSimpleErp(DSUser user, Date startDate, Date endDate) throws EdmException
	{
		NativeCriteria nc = DSApi.context().createNativeCriteria(linkedServer + "." + dbName + "." + schema + "." + nieobecnosciTabela, "d");
		nc.setProjection(NativeExps.projection()
		// ustalenie kolumn
				.addProjection("d.IDWpisu") 	// >IDWpisu - Identyfikator wpisu [ 7 znak�w]
				.addProjection("d.NrEwid") 	// >NrEwid - Numer Pracownika [ 6 znak�w]
				.addProjection("d.Rok") 		// >DataOd - Data od nieobecnosci[ datetime]
				.addProjection("d.DataOd") 	// >DataDo - Data do nieobecnosci[ datetime]
				.addProjection("d.DataDo") 	// >DataDo - Data do nieobecnosci[ datetime]
				.addProjection("d.TypNieob") 	// >TypNieob - Typ nieobenosci [ 2 znaki]
				.addProjection("d.DniRob")  // >IloscDni - Ilosc dni roboczych nieobec. [ wartosc]
				.addProjection("d.DataMod")) // >DataMod - Data modyfikacji [ datetime]
		// warunek where
			.add(NativeExps.eq("d.NrEwid", user.getExtension()))
			.add(NativeExps.gte("d.DataOd", startDate))
			.add(NativeExps.lte("d.DataDo", endDate));
		// pobranie i wy�wietlenie wynik�w
		CriteriaResult cr = nc.criteriaResult();
		// utworzenie wpisu urlopu
		while (cr.next())
		{
			return true;
		}
		return false;
	}

	@Override
	public List<FreeDay> getFreeDays() {
		return freeDays;
	}

	@Override
	public EmployeeCard getEmployeeCard() {
		return employeeCard;
	}
}
