package pl.compan.docusafe.parametrization.polcz;

import java.sql.Date;
import java.util.List;

import javax.swing.Action;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.webwork.event.ActionListener;

public class WniosekUrlopowyPAA
{
private String  wnioskujacy;
private Date  UrlopDataOo;
private Date  UrlopDataDo;
private String  opis;
private Date  OdbiorNadgodzinOd;
private Date  OdbiorNadgodzinDo;
private Integer  typUrlopu;
private List sliwnikZastepstw;
private List pracownicyZastepujacy;
private Date  ZastepstwoUrlopDataOd;
private Date  ZastepstwoUrlopDataDo;
private String zalogowanyUzytkownik;

private DSDivision dzialUzytkownika;
private DSDivision departamentUzytkownika;

private DSUser uzytkownik;
private DSUser uzytkownikNadzorujacy;
private List   osobZastepujacych;


	//ActionListener 

	public WniosekUrlopowyPAA nowyWniosek = new WniosekUrlopowyPAA();
	{	
		// TODO Auto-generated constructor stub
	}










public String getWnioskujacy()
{
	return wnioskujacy;
}


public void setWnioskujacy(String wnioskujacy)
{
	this.wnioskujacy= wnioskujacy;
}


public Date getUrlopDataOo()
{
	return UrlopDataOo;
}


public void setUrlopDataOo(Date urlopDataOo)
{
	UrlopDataOo= urlopDataOo;
}


public Date getUrlopDataDo()
{
	return UrlopDataDo;
}


public void setUrlopDataDo(Date urlopDataDo)
{
	UrlopDataDo= urlopDataDo;
}


public String getOpis()
{
	return opis;
}


public void setOpis(String opis)
{
	this.opis= opis;
}


public Date getOdbiorNadgodzinOd()
{
	return OdbiorNadgodzinOd;
}


public void setOdbiorNadgodzinOd(Date odbiorNadgodzinOd)
{
	OdbiorNadgodzinOd= odbiorNadgodzinOd;
}


public Date getOdbiorNadgodzinDo()
{
	return OdbiorNadgodzinDo;
}


public void setOdbiorNadgodzinDo(Date odbiorNadgodzinDo)
{
	OdbiorNadgodzinDo= odbiorNadgodzinDo;
}


public Integer getTypUrlopu()
{
	return typUrlopu;
}


public void setTypUrlopu(Integer typUrlopu)
{
	this.typUrlopu= typUrlopu;
}


public List getSliwnikZastepstw()
{
	return sliwnikZastepstw;
}


public void setSliwnikZastepstw(List sliwnikZastepstw)
{
	this.sliwnikZastepstw= sliwnikZastepstw;
}


public List getPracownicyZastepujacy()
{
	return pracownicyZastepujacy;
}


public void setPracownicyZastepujacy(List pracownicyZastepujacy)
{
	this.pracownicyZastepujacy= pracownicyZastepujacy;
}


public Date getZastepstwoUrlopDataOd()
{
	return ZastepstwoUrlopDataOd;
}


public void setZastepstwoUrlopDataOd(Date zastepstwoUrlopDataOd)
{
	ZastepstwoUrlopDataOd= zastepstwoUrlopDataOd;
}


public Date getZastepstwoUrlopDataDo()
{
	return ZastepstwoUrlopDataDo;
}


public void setZastepstwoUrlopDataDo(Date zastepstwoUrlopDataDo)
{
	ZastepstwoUrlopDataDo= zastepstwoUrlopDataDo;
}


public String getZalogowanyUzytkownik()
{
	return zalogowanyUzytkownik;
}


public void setZalogowanyUzytkownik(String zalogowanyUzytkownik)
{
	this.zalogowanyUzytkownik= zalogowanyUzytkownik;
}


public DSDivision getDzialUzytkownika()
{
	return dzialUzytkownika;
}


public void setDzialUzytkownika(DSDivision dzialUzytkownika)
{
	this.dzialUzytkownika= dzialUzytkownika;
}


public DSDivision getDepartamentUzytkownika()
{
	return departamentUzytkownika;
}


public void setDepartamentUzytkownika(DSDivision departamentUzytkownika)
{
	this.departamentUzytkownika= departamentUzytkownika;
}


public DSUser getUzytkownik()
{
	return uzytkownik;
}


public void setUzytkownik(DSUser uzytkownik)
{
	this.uzytkownik= uzytkownik;
}


public DSUser getUzytkownikNadzorujacy()
{
	return uzytkownikNadzorujacy;
}


public void setUzytkownikNadzorujacy(DSUser uzytkownikNadzorujacy)
{
	this.uzytkownikNadzorujacy= uzytkownikNadzorujacy;
}


public List getOsobZastepujacych()
{
	return osobZastepujacych;
}


public void setOsobZastepujacych(List osobZastepujacych)
{
	this.osobZastepujacych= osobZastepujacych;
}





	


}
