package pl.compan.docusafe.parametrization.polcz;

public class ApplicationWBGLogic extends AbstractApplicationLogic
{
	private static ApplicationWBGLogic instance;
	
	public static ApplicationWBGLogic getInstance()
	{
		if (instance == null)
			instance = new ApplicationWBGLogic();
		return instance;
	}
}