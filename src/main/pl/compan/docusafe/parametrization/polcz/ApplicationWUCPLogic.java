package pl.compan.docusafe.parametrization.polcz;

import java.util.Date;
import java.util.Map;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;

public class ApplicationWUCPLogic extends AbstractBudgetApplicationLogic
{
	private static ApplicationWUCPLogic instance;
	
	public static ApplicationWUCPLogic getInstance()
	{
		if (instance == null)
			instance = new ApplicationWUCPLogic();
		return instance;
	}
	
	public void specialSetDictionaryValues(Map<String, ?> dockindFields)
	{
		log.info("specialSetDictionaryValues: \n {} ", dockindFields);

		if (dockindFields.keySet().contains("CONTRACTOR_DICTIONARY"))
		{
			Map<String, FieldData> m = (Map<String, FieldData>) dockindFields.get("CONTRACTOR_DICTIONARY");
			m.put("CONTRACTOR_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			m.put("CONTRACTOR_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}

	public void addSpecialInsertDictionaryValues(String dictionaryName, Map<String, FieldData> values)
	{
		if (dictionaryName.contains("CONTRACTOR"))
		{
			values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}

	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		if (dictionaryName.contains("CONTRACTOR"))
		{
			values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
	{
		pl.compan.docusafe.core.dockinds.dwr.Field msgField = super.validateDwr(values, fm);
		
		StringBuilder msgBuilder = new StringBuilder();
		
		if (msgField != null)
			msgBuilder.append(msgField.getLabel());

		// sprawdzi� czy data zawarcia nie jest p�niejsza ni� data realizacji
		validateDate(values, msgBuilder);
		
		if (msgBuilder.length() > 0)
		{
			values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
			return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
		}
		
		return null;
	}
	
	private void validateDate(Map<String, FieldData> values, StringBuilder msgBuilder)
	{
		
			Date start = null, finish = null;
			
				if (values.get("DWR_START_DATE") != null && values.get("DWR_FINISH_DATE")!= null)
				{
					start = values.get("DWR_START_DATE").getDateData();
					finish = values.get("DWR_FINISH_DATE").getDateData();
				}
			
			
			if (start != null && finish != null && start.after(finish))
			{
				if (msgBuilder.length() == 0)
					msgBuilder.append("B��d: Data zako�czenia nie mo�e by� wcze�niejsza ni� data rozpocz�cia.");
				else
					msgBuilder.append(";  Data zako�czenia nie mo�e by� wcze�niejsza ni� data rozpocz�cia.");
			}
		
	}

}
