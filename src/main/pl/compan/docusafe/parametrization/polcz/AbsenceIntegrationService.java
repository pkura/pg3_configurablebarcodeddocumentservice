package pl.compan.docusafe.parametrization.polcz;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.absences.Absence;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.base.absences.AbsenceType;

import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

public class AbsenceIntegrationService extends ServiceDriver implements Service
{
	public static final Logger log = LoggerFactory.getLogger(AbsenceIntegrationService.class);
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(AbsenceIntegrationService.class.getPackage().getName(), null);

	private Property[] properties;
	private Timer timer;
	private Integer periodWww = 10;
	private String url =  Docusafe.getAdditionProperty("erp.database.url");
	private String username = Docusafe.getAdditionProperty("erp.database.user");
	private String password = Docusafe.getAdditionProperty("erp.database.password");
	private static String linkedServer = Docusafe.getAdditionProperty("erp.database.linkedserver");
	private static String dbName = Docusafe.getAdditionProperty("erp.database.dataBase");
	private static String schema = Docusafe.getAdditionProperty("erp.database.schema");
	private static String kodyTabela = Docusafe.getAdditionProperty("erp.database.kody");
	private String niebecnosciTab = Docusafe.getAdditionProperty("erp.database.nieobecnosci");
	
	private static HashMap<String, AbsenceType> typesMap = null;
	private static HashMap<String, String> codesMap = null;
	private static AbsenceType wuzAb, wunAb;
	
	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, AbsenceIntegrationService.this, "periodWww", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return periodWww;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (AbsenceIntegrationService.this)
			{
				if (object != null)
					periodWww = (Integer) object;
				else
					periodWww = 10;
			}
		}
	}

	class URLProperty extends Property
	{
		public URLProperty()
		{
			super(SIMPLE, PERSISTENT, AbsenceIntegrationService.this, "url", "�cie�ka do bazy", String.class);
		}

		protected Object getValueSpi()
		{
			return url;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (AbsenceIntegrationService.this)
			{
				if (object != null)
					url = (String) object;
				else
					url = Docusafe.getAdditionProperty("erp.database.url");
			}
		}
	}

	class UsernameProperty extends Property
	{
		public UsernameProperty()
		{
			super(SIMPLE, PERSISTENT, AbsenceIntegrationService.this, "username", "Nazwa u�ytkownika", String.class);
		}

		protected Object getValueSpi()
		{
			return username;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (AbsenceIntegrationService.this)
			{
				if (object != null)
					username = object.toString();
				else
					username = Docusafe.getAdditionProperty("erp.database.user");
			}
		}
	}

	class PasswordProperty extends Property
	{
		public PasswordProperty()
		{
			super(SIMPLE, PERSISTENT, AbsenceIntegrationService.this, "password", "Has�o", String.class);
		}

		protected Object getValueSpi()
		{
			return password;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (AbsenceIntegrationService.this)
			{
				if (object != null)
					password = (String) object;
				else
					password = Docusafe.getAdditionProperty("erp.database.password");
			}
		}
	}

	public AbsenceIntegrationService()
	{
		properties = new Property[] { new ImportTimesProperty(), new URLProperty(), new UsernameProperty(), new PasswordProperty() };
	}

	@Override
	protected void start() throws ServiceException
	{
		log.info("Start uslugi AbsenceIntegrationService");
		timer = new Timer(true);
		timer.schedule(new Import(), 0, periodWww * DateUtils.HOUR);
		console(Console.INFO, sm.getString("System AbsenceIntegrationService wystartowal"));
	}

	@Override
	protected void stop() throws ServiceException
	{
		log.info("Stop uslugi AbsenceIntegrationService");
		console(Console.INFO, sm.getString("System AbsenceIntegrationService zako�czy� dzia�anie"));
		try
		{
			DSApi.close();
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask
	{
		/* ========per_docusafe_nieobecnosci================== */
		/* View: Integracja nieobecnosci z bazy Simple z DocuSafe */
		/* Wy�wietla liste zarejestrowanych nieobecnosci */
		/* === Robert Miernik + 696-011-843 + rmi@simple.com.pl === */
		/* Zwraca: */
		/* >IDWpisu - Identyfikator wpisu [ 7 znak�w] */
		/* >NrEwid - Numer Pracownika [ 6 znak�w] */
		/* >Rok - Rok [ 4 znaki] */
		/* >DataOd - Data od nieobecnosci[ datetime] */
		/* >DataDo - Data do nieobecnosci[ datetime] */
		/* >TypNieob - Typ nieobenosci [ 2 znaki] */
		/* >DniRob - Ilosc dni roboczych nieobec. [ wartosc] */
		/* >DataMod - Data modyfikacji [ datetime] */
		/* ==== ERP_DB.simpleERP.dbo.per_docusafe_nieobecnosci========== */

		@Override
		public void run()
		{
			
			try
			{
				log.debug("ABSENCEINTEGRATIONSERVICE START");
//				DSApi.open(AuthUtil.getSubject(WebContextFactory.get().getHttpServletRequest()));
				DSApi.openAdmin();
				
				
				String linkedServer = Docusafe.getAdditionProperty("erp.database.linkedserver");
				String dbName = Docusafe.getAdditionProperty("erp.database.dataBase");
				String schema = Docusafe.getAdditionProperty("erp.database.schema");
				String nieobecnosciTabela = Docusafe.getAdditionProperty("erp.database.nieobecnosci");
				String urlopyTabela = Docusafe.getAdditionProperty("erp.database.urlopy");
				int year = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
				StringBuilder sb2 = new StringBuilder(linkedServer);
				String fullUrl2 = sb2.append(".").append(dbName).append(".").append(schema).append(".").append(urlopyTabela).toString();
				NativeCriteria nc2 = DSApi.context().createNativeCriteria(fullUrl2, "u");
				nc2.setProjection(NativeExps.projection()
						// ustalenie kolumn
								.addProjection("u.NrEwid") 	// >IDWpisu - Identyfikator wpisu [ 7 znak�w]
								.addProjection("u.Nalezny") 	// >NrEwid - Numer Pracownika [ 6 znak�w]
								.addProjection("u.Zalegly"));
				CriteriaResult cr2 = nc2.criteriaResult();
			
				UserImpl userImpl2 = null;
				log.debug("wchodze do petli");
				
				while(cr2.next())
				{
					log.debug("obrot petli");
					try 
					{
						log.debug("SZUKAM USERA:" + cr2.getString(0, null));
						userImpl2 = (UserImpl) DSUser.findAllByExtension(cr2.getString(0, null));
					}
					catch (UserNotFoundException e)
					{
						log.debug("Brak u�ytkonika o nrEwid: {}, w docusafe" ,cr2.getString(0, null));
//						log.error(e.getMessage(), e);
						continue;
					}
					EmployeeCard empCard = AbsenceFactory.getActiveEmployeeCardByUser(userImpl2);
					
					if (empCard == null)
					{
						log.error("Brak karty pracownika: {}! Nie mo�na przypisa� urlopu!", userImpl2.getName());
						continue;
					}
					GregorianCalendar.getInstance().set(year, 0, 31);
					Date dod = GregorianCalendar.getInstance().getTime();
					GregorianCalendar.getInstance().set(year, 11, 31);
					Date ddo = GregorianCalendar.getInstance().getTime();
					log.debug("zaleglywymiar");
					String ile1 = cr2.getString(1, null);
					Double di1 = Double.parseDouble(ile1);
					int int1 = di1.intValue();
					String ile2 = cr2.getString(2, null);
					Double di2 = Double.parseDouble(ile2);
					int int2 = di2.intValue();
					log.debug("AbsenceType.getWymiarType():" + getWymiarType());
					log.debug("AbsenceType.getZaleglyType():" + getZaleglyType());
					Absence wymiarAb = hasWymiar(empCard);
					log.debug("hasWymiar" + wymiarAb);
					if(wymiarAb != null)
					{
						wymiarAb.setYear(year);
						wymiarAb.setStartDate(dod);
						wymiarAb.setEndDate(ddo);
						wymiarAb.setAbsenceType(getWymiarType());
						wymiarAb.setDaysNum(int1);
						wymiarAb.setInfo("Imported from simpleERP");
						wymiarAb.setCtime(new Date());
						DSApi.context().begin();
						try {
							DSApi.context().session().update(wymiarAb);
							DSApi.context().commit();
						} catch (Exception e) {
							log.error(e.getMessage(), e);
							DSApi.context().rollback();
						}
					}
					else
					{
						wymiarAb = new Absence();
						wymiarAb.setYear(year);
						wymiarAb.setStartDate(dod);
						wymiarAb.setEndDate(ddo);
						wymiarAb.setAbsenceType(getWymiarType());
						wymiarAb.setDaysNum(int1);
						wymiarAb.setInfo("Imported from simpleERP");
						wymiarAb.setCtime(new Date());
						empCard.getAbsences().add(wymiarAb);
						wymiarAb.setEmployeeCard(empCard);
						DSApi.context().session().save(wymiarAb);
					}
					Absence zaleglyAb = hasZalegly(empCard);
					log.debug("hasZalegly" + zaleglyAb);
					if(zaleglyAb !=null)
					{
						zaleglyAb.setYear(year);
						zaleglyAb.setStartDate(dod);
						zaleglyAb.setEndDate(ddo);
						zaleglyAb.setAbsenceType(getZaleglyType());
						zaleglyAb.setDaysNum(int2);
						zaleglyAb.setInfo("Imported from simpleERP");
						zaleglyAb.setCtime(new Date());
						DSApi.context().session().save(zaleglyAb);
					}
					else
					{
						zaleglyAb = new Absence();
						zaleglyAb.setYear(year);
						zaleglyAb.setStartDate(dod);
						zaleglyAb.setEndDate(ddo);
						zaleglyAb.setAbsenceType(getZaleglyType());
						zaleglyAb.setDaysNum(int2);
						zaleglyAb.setInfo("Imported from simpleERP");
						zaleglyAb.setCtime(new Date());
						empCard.getAbsences().add(zaleglyAb);
						zaleglyAb.setEmployeeCard(empCard);
						DSApi.context().session().save(zaleglyAb);
					}
				}
				
				StringBuilder sb = new StringBuilder(linkedServer);
				String fullUrl = sb.append(".").append(dbName).append(".").append(schema).append(".").append(nieobecnosciTabela).toString();
				NativeCriteria nc = DSApi.context().createNativeCriteria(fullUrl, "d");
				
				try {
					Criteria criteria = DSApi.context().session().createCriteria(Absence.class);
//					criteria.add(Restrictions.eq("year", GregorianCalendar.getInstance().get(GregorianCalendar.YEAR)));
					@SuppressWarnings("unchecked")
					List<Absence> absences = (List<Absence>) criteria.list();

					Connection conn = null;
					PreparedStatement ps = null;
					ResultSet rs = null;
					String tableName = null;
					conn = DriverManager.getConnection(url, username, password);
					for (Absence abs : absences) {
						if ("WUN".equals(abs.getAbsenceType().getCode()) || "WUZ".equals(abs.getAbsenceType().getCode())) {
							continue;
						}
						tableName = dbName + "." + schema + "." + nieobecnosciTabela;
						ps = conn.prepareStatement("select * from " + tableName + " where DataOd=? and DataDo=? and Rok=? and NrEwid=?");
						ps.setTimestamp(1, (java.sql.Timestamp) abs.getStartDate());
						ps.setTimestamp(2, (java.sql.Timestamp) abs.getEndDate());
						ps.setInt(3, abs.getYear());
						ps.setInt(4, Integer.valueOf(abs.getEmployeeCard().getUser().getExtension()));
						rs = ps.executeQuery();
						if (!rs.next()) {
							DSApi.context().begin();
							log.debug("AbsenceIntegrationService usun�� wpis: StartDate {}, EndDate {}, useername {} ", abs.getStartDate(), abs.getEndDate(), abs.getEmployeeCard().getUser().getName());
							try {
								abs.delete();
								DSApi.context().commit();
							} catch (Exception e) {
								DSApi.context().rollback();
							}
						}
						ps.close();
						rs.close();
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
				
				
				nc.setProjection(NativeExps.projection()
				// ustalenie kolumn
						.addProjection("d.IDWpisu") 	// >IDWpisu - Identyfikator wpisu [ 7 znak�w]
						.addProjection("d.NrEwid") 	// >NrEwid - Numer Pracownika [ 6 znak�w]
						.addProjection("d.Rok") 		// >DataOd - Data od nieobecnosci[ datetime]
						.addProjection("d.DataOd") 	// >DataDo - Data do nieobecnosci[ datetime]
						.addProjection("d.DataDo") 	// >DataDo - Data do nieobecnosci[ datetime]
						.addProjection("cast(d.TypNieob as int)", "typnieob") 	// >TypNieob - Typ nieobenosci [ 2 znaki]
						.addProjection("d.DniRob")  // >DniRob - Ilosc dni roboczych nieobec. [ wartosc]
						.addProjection("d.DataMod"));	// >DataMod - Data modyfikacji [ datetime]

				nc.add(NativeExps.eq("d.Rok", GregorianCalendar.getInstance().get(GregorianCalendar.YEAR)));
				// pobranie i wy�wietlenie wynik�w
				CriteriaResult cr = nc.criteriaResult();
				// utworzenie wpisu urlopu
				Absence absence = null;
				UserImpl userImpl = null;
				
				while (cr.next())
				{
					log.debug("zxcvbn:" + cr.getString(5, null));
					try 
					{
						log.debug("yyySZUKAM USERA:" + cr.getString(1, null));
						userImpl = (UserImpl) DSUser.findAllByExtension(cr.getString(1, null));
						log.debug("xxxJEST USER:" + userImpl.getName());
					}
					catch (UserNotFoundException e)
					{
						log.debug("Brak u�ytkonika o nrEwid: {}, w docusafe" ,cr.getString(1, null));
//						log.error(e.getMessage(), e);
						continue;
					}

					EmployeeCard empCard = AbsenceFactory.getActiveEmployeeCardByUser(userImpl);
					if (empCard == null)
					{
						log.debug("Brak karty pracownika: {}! Nie mo�na przypisa� urlopu!", userImpl.getName());
						continue;
					}
//					if (WniosekUrlopLogic.getAvailableDayFromSimpleERP(empCard.getUser().getExtension(), null, null) == -1)
//					{
//						log.error("Brak przypisanego nale�nego wymiaru urlopowego na bierz�cy rok w SimpleErp dla nrEwid: {}!", empCard.getUser().getExtension());
//						continue;
//					}
					
					// ustawienie dat
					Date from = (Date) cr.getDate(3, null);
					Date to = (Date) cr.getDate(4, null);
//					if (!AbsenceFactory.hasAbsenceBetween(empCard, from, to) && !WniosekUrlopLogic.hasAbsenceInSimpleErp(userImpl, from, to))
					
					if (!AbsenceFactory.hasAbsenceBetween(empCard, from, to))
					{	
						log.debug("AbsenceIntegrationService importuje wpis urlopu o id: {} dla u�ytkownika {}", cr.getLong(0, null), userImpl.getName());
						absence = new Absence();
						year = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
						absence.setYear(year);
						absence.setStartDate(from);
						absence.setEndDate(to);
						AbsenceType absenceType = null;
						log.debug("newabsenceint:" + cr.getString(5, null));
						absenceType = findByCode(cr.getString(5, null));
						if (absenceType == null)
							continue;
						absence.setAbsenceType(absenceType);
//	
						// ustawienie liczby dni
						String numDaysString = cr.getString(6, null);
						Double numDaysDouble = Double.parseDouble(numDaysString);
						int numDays = numDaysDouble.intValue();
						absence.setDaysNum(numDays);
//						// ustawienie opisu
						absence.setInfo("Imported from simpleERP");
//	
//						// data utworzenia wpisu
						absence.setCtime(cr.getDate(7, new Date()));
						empCard.getAbsences().add(absence);
						absence.setEmployeeCard(empCard);
						DSApi.context().session().save(absence);
						DSApi.context().session().update(empCard);
					}
				}
				
				//Import ilosci urlopu naleznego i zaleglego
				
				
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
			}
			finally
			{
				try {
					DSApi.close();
				} catch (EdmException e) {
					log.error(e.getMessage(), e);
				}
			}
		}

		
	}
	
	private static AbsenceType findByCode(String code){
		if(typesMap == null) initMap();
		if(typesMap.containsKey(code)) return typesMap.get(code);
		return null;
//		try{
//			java.util.List lista = DSApi.context().classicSession().find("from " + AbsenceType.class.getName() + " ab where ab.code = '" + code + "'" );
//			if(lista.size() > 0) return (AbsenceType) lista.get(0);
//		}catch(Exception e){
//			log.error(e.getMessage(), e);
//		}
//		return null;
	}
	
	private static void initCodesMap() throws EdmException {
		log.debug("odpalaminitcodesmap");
		codesMap = new HashMap<String, String>();
		StringBuilder sb = new StringBuilder(linkedServer);
		String fullUrl = sb.append(".").append(dbName).append(".").append(schema).append(".").append(kodyTabela).toString();
		NativeCriteria nc = DSApi.context().createNativeCriteria(fullUrl, "d");
		nc.setProjection(NativeExps.projection()
		// ustalenie kolumn
				.addProjection("d.KodNieob")
				.addProjection("d.Kod"));
		CriteriaResult cr = nc.criteriaResult();
		while(cr.next())
		{
			codesMap.put(cr.getString(1, null), cr.getString(0, null));
		}
		log.debug("initCodesMap():" + codesMap);
	}



	private static void initMap() {
		
		try {
			log.debug("odpalaminitmap");
			if(codesMap == null) initCodesMap();
			typesMap = new HashMap<String, AbsenceType>();
			List list = DSApi.context().classicSession().find("from " + AbsenceType.class.getName());
			log.debug("INITMAP!!! SIZE=" + list.size());
			for(int i=0; i<list.size(); i++)
			{
				AbsenceType at = (AbsenceType) list.get(i);
				log.debug("WUZWUNABSENCE:" + at);
				log.debug("WUZWUN:" + at.getCode());
				if(at.getCode().equalsIgnoreCase("WUN")) wunAb = (AbsenceType) list.get(i);
				if(at.getCode().equalsIgnoreCase("WUZ")) wuzAb = (AbsenceType) list.get(i);
				//jezeli mamy w bazie mapowanie tego kodu liczbowego na literowy i w typesMap nie ma jeszcze tego wpisu
				
				if(codesMap.containsKey(at.getCode()) && !typesMap.containsKey(codesMap.get(at.getCode())))typesMap.put(codesMap.get(at.getCode()), at);
			}
			log.debug("initMap():" + typesMap);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
	
	private static AbsenceType getWymiarType()
	{
		try{
			java.util.List lista = DSApi.context().classicSession().find("from " + AbsenceType.class.getName() + " ab where ab.code = 'WUN'" );
			if(lista.size() > 0) return (AbsenceType) lista.get(0);
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
		return null;
	}
	
	private static AbsenceType getZaleglyType()
	{
		try{
			java.util.List lista = DSApi.context().classicSession().find("from " + AbsenceType.class.getName() + " ab where ab.code = 'WUZ'" );
			if(lista.size() > 0) return (AbsenceType) lista.get(0);
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
		return null;
	}
	
	private static Absence hasWymiar(EmployeeCard empCard){
		try{
			
			java.util.List lista = DSApi.context().classicSession().find("from " + Absence.class.getName() + " ab where ab.absenceType.code = 'WUN' and ab.employeeCard.id = " + empCard.getId());
			if(lista.size() > 0) return (Absence) lista.get(0);
		}
		catch(Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return null;
	}
	
	private static Absence hasZalegly(EmployeeCard empCard){
		try{
			java.util.List lista = DSApi.context().classicSession().find("from " + Absence.class.getName() + " ab where ab.absenceType.code = 'WUZ' and ab.employeeCard.id = " + empCard.getId());
			if(lista.size() > 0) return (Absence) lista.get(0);
		}
		catch(Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return null;
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}
}
