package pl.compan.docusafe.parametrization.polcz;

public class ApplicationWAHLogic extends AbstractApplicationLogic
{
	private static ApplicationWAHLogic instance;
	
	public static ApplicationWAHLogic getInstance()
	{
		if (instance == null)
			instance = new ApplicationWAHLogic();
		return instance;
	}
}

