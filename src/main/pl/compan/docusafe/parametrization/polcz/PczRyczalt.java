package pl.compan.docusafe.parametrization.polcz;

import java.math.BigDecimal;

public class PczRyczalt
{

	private Long id;
	private Long currency;
	private BigDecimal currency_value;
	private BigDecimal rate;
	private BigDecimal value;
	
	public Long getId()
	{
		return id;
	}

	public Long getCurrency()
	{
		return currency;
	}
	public void setCurrency(Long currency)
	{
		this.currency = currency;
	}
	public BigDecimal getCurrency_value()
	{
		return currency_value;
	}
	public void setCurrency_value(BigDecimal currency_value)
	{
		this.currency_value = currency_value;
	}
	public BigDecimal getRate()
	{
		return rate;
	}
	public void setRate(BigDecimal rate)
	{
		this.rate = rate;
	}
	public BigDecimal getValue()
	{
		return value;
	}
	public void setValue(BigDecimal value)
	{
		this.value = value;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currency == null) ? 0 : currency.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PczRyczalt other = (PczRyczalt) obj;
		if (currency == null)
		{
			if (other.currency != null)
				return false;
		}
		else if (!currency.equals(other.currency))
			return false;
		return true;
	}

	public void setId(Long id)
	{
		this.id = id;
	}
	
	
}
