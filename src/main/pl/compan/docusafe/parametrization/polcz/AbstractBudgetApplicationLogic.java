package pl.compan.docusafe.parametrization.polcz;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Session;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.parametrization.polcz.MpkAccept.AcceptType;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class AbstractBudgetApplicationLogic extends AbstractApplicationLogic
{

	private static final String MPK_DICTIONARY_CN = "MPK";
	private static final String WSTEPNIE_POZOSTALA_FIELD_CN = "WSTEPNIE_POZOSTALA";
	public static final String REALIZACJA_BUDZETU_DICTIONARY_CN = "REALIZACJA_BUDZETU";
	
	// ladowanie kierownik�w oraz sekcji finansowej
	protected static final List<String> divShortcut = new ArrayList<String>();
	static
	{
		divShortcut.add("WE"); // Wydzia� Elektryczny
		divShortcut.add("WB"); // Wydzia� Budownictwa
		divShortcut.add("WIMiI"); // Wydzia� In�ynierii Mechanicznej
									// i Informatyki
		divShortcut.add("WIPMiFS"); // Wydzia� In�ynierii
									// Procesowej, Materia�owej i
									// Fizyki Stosowanej
		divShortcut.add("WIiO\u015a"); // Wydzia� In�ynierii i
										// Ochrony �rodowiska
		divShortcut.add("WZ"); // Wydzia� Zarz�dzania
	}
	
	protected static final List<String> rectorsShortcut = new ArrayList<String>();
	static {
		rectorsShortcut.add("RO"); // Prorektor
		rectorsShortcut.add("RK");
		rectorsShortcut.add("RN");
		rectorsShortcut.add("RD");
		rectorsShortcut.add("RR");
		// rectorsShortcut.add("R"); //Rektor
	}
	
	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue)
	{
		log.debug("Start special assignee operations for acceptationCn: {}, fieldCnValue: {}", acceptationCn, fieldCnValue);

		boolean assigned = super.assignee(doc, assignable, openExecution, acceptationCn, fieldCnValue);

		if (!assigned)
		{
			if (AssingUtils.FINANCIAL_SECTION.equals(acceptationCn) || AssingUtils.MANAGER_OF_UNIT.equals(acceptationCn) || AssingUtils.MANAGER_OF_DEPARTMENT.equals(acceptationCn))
				assigned = AssingUtils.assigneeForMpkAcceptances(doc, assignable, openExecution, acceptationCn);
		}
		
		return assigned;
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
	{
		pl.compan.docusafe.core.dockinds.dwr.Field msgField = super.validateDwr(values, fm);

		StringBuilder msgBuilder = new StringBuilder();

		try 
		{
			if (msgField != null)
				msgBuilder.append(msgField.getLabel());

			Set<String> pozycjeId = new HashSet<String>();
			if (values.get("DWR_MPK") != null && values.get("DWR_REALIZACJA_BUDZETU") != null)
			{
				DSApi.openAdmin();
				// za dlugo tutaj w przypadkyu kiedy nie wybralismy jeszcze nic
				loadBudgetRealizationItems(values,"MPK", pozycjeId);
				checkRealizacjeBudzetow(values, fm, msgBuilder);
				DSApi.close();
			}
			
			if (values.get("DWR_ADD_MPK") != null && values.get("DWR_REALIZACJA_BUDZETU") != null && (fm.getKey("brak_srodkow") != null && fm.getBoolean("brak_srodkow"))) 
			{
				DSApi.openAdmin();
				// za dlugo tutaj w przypadkyu kiedy nie wybralismy jeszcze nic
				loadBudgetRealizationItems(values, "ADD_MPK", pozycjeId);
				checkRealizacjeBudzetow(values, fm, msgBuilder);
				DSApi.close();
			}
			
			StringBuilder ids = new StringBuilder();
			
			for (String pozycja : pozycjeId) {
				if (ids.length() != 0) {
					ids.append(",");
				}
				ids.append(pozycja);
			}
			values.put("DWR_REALIZACJA_BUDZETU", new FieldData(Field.Type.STRING, ids.toString()));
		} catch (EdmException edm) {
			log.error(edm.getMessage(), edm);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		finally
		{
			if (DSApi.isContextOpen())
				DSApi._close();
		}

		if (msgBuilder.length() > 0)
		{
			values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
			return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
		}

		return null;
	}

	protected void checkRealizacjeBudzetow(Map<String, FieldData> values, FieldsManager fm, StringBuilder msgBuilder) throws EdmException {
		Map<String, FieldData> mpkSlownik = values.get("DWR_MPK").getDictionaryData();
		List<Long> realizacjeIds = (List<Long>) fm.getKey(REALIZACJA_BUDZETU_DICTIONARY_CN);
		if (realizacjeIds != null) {

			for (Long id : realizacjeIds) {
				String pozycjaID = (String) DwrDictionaryFacade.dictionarieObjects.get(REALIZACJA_BUDZETU_DICTIONARY_CN)
						.getValues(id.toString()).get(REALIZACJA_BUDZETU_DICTIONARY_CN + "_" + "POZYCJA");
				BigDecimal wartoscWstepniePozostala = (BigDecimal) DwrDictionaryFacade.dictionarieObjects.get(REALIZACJA_BUDZETU_DICTIONARY_CN)
						.getValues(id.toString()).get(REALIZACJA_BUDZETU_DICTIONARY_CN + "_" + WSTEPNIE_POZOSTALA_FIELD_CN);
				for (String cn : mpkSlownik.keySet()) {
					if (cn.contains("ACCOUNTNUMBER") && mpkSlownik.get(cn).getData() != null) {
						if (mpkSlownik.get(cn).getEnumValuesData().getSelectedValue().equals(pozycjaID)) {
							String suffix = cn.substring(cn.lastIndexOf("_"));
							BigDecimal kwotaMPK = mpkSlownik.get("MPK_AMOUNT".concat(suffix)).getMoneyData();
							if (kwotaMPK != null && kwotaMPK.compareTo(wartoscWstepniePozostala) > 0) {
								if (msgBuilder.length() == 0)
									msgBuilder
											.append("B��d: Kwota jednej z pozycji bud�etowych jest wi�ksza ni� warto�� wst�pnie pozosta�a do wykonania dla tej pozycji.\n");
								else
									msgBuilder
											.append(";  Kwota jednej z pozycji bud�etowych jest wi�ksza ni� warto�� wst�pnie pozosta�a do wykonania dla tej pozycji.\n");
							}
						}
					}
				}
			}
		}
	}
	
	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {

		FieldsManager fm = document.getFieldsManager();

		List<Long> mpkIds = (List<Long>) fm.getKey(MPK_DICTIONARY_CN);
		List<Long> realizacjeIds = (List<Long>) fm.getKey(REALIZACJA_BUDZETU_DICTIONARY_CN);
		if (realizacjeIds != null) {

			for (Long id : realizacjeIds) {
				String pozycjaID = (String) DwrDictionaryFacade.dictionarieObjects.get(REALIZACJA_BUDZETU_DICTIONARY_CN).getValues(id.toString())
						.get(REALIZACJA_BUDZETU_DICTIONARY_CN + "_" + "POZYCJA");
				BigDecimal wartoscWstepniePozostala = (BigDecimal) DwrDictionaryFacade.dictionarieObjects.get(REALIZACJA_BUDZETU_DICTIONARY_CN)
						.getValues(id.toString()).get(REALIZACJA_BUDZETU_DICTIONARY_CN + "_" + WSTEPNIE_POZOSTALA_FIELD_CN);
				for (Long x : mpkIds) {
					String pozycjaMPK = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get(MPK_DICTIONARY_CN).getValues(x.toString())
							.get(MPK_DICTIONARY_CN + "_" + "ACCOUNTNUMBER")).getSelectedValue();
					if (pozycjaID.equals(pozycjaMPK)) {
						BigDecimal kwotaMPK = BigDecimal.valueOf(((Double) DwrDictionaryFacade.dictionarieObjects.get(MPK_DICTIONARY_CN).getValues(x.toString())
								.get(MPK_DICTIONARY_CN + "_" + "AMOUNT")));
						if (kwotaMPK.compareTo(wartoscWstepniePozostala) > 0) {
							throw new EdmException("Kwota pozycji bud�etowej \"" + pozycjaMPK
									+ "\" jest wi�ksza ni� warto�� wst�pnie pozosta�a do wykonania dla tej pozycji.");
						}
					}
				}
			}
		}

		// wystartowanie procesu
		super.onStartProcess(document, event);
	}
	
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) 
	{
		Map<String, Object> results = super.setMultipledictionaryValue(dictionaryName, values);
	 
		try 
		{
			if (dictionaryName.equals(MPK_DICTIONARY_CN) || dictionaryName.equals("ADD_MPK")) 
			{
				// budzetu
				Map<String, Object> fieldsValues = new HashMap<String, Object>();
				EnumValues centrumIdEnum = values.get(dictionaryName + "_CENTRUMID").getEnumValuesData();
				String budgetId = centrumIdEnum.getSelectedOptions().get(0);
		
				if (budgetId != null && !budgetId.equals("")) 
				{
					Long budgetIdLong = new Long(budgetId);
		
					String projectDiv = null;
		
					DataBaseEnumField.reloadForTable(CENTRUM_ID_TABLE_NAME);
					EnumItem projekt = DataBaseEnumField.getEnumItemForTable(CENTRUM_ID_TABLE_NAME, Integer.parseInt(budgetId));
					String projectName = projekt.getTitle();
					String externalId = projekt.getRefValue();
		
					// 1. wyszukiwanie skr�tu nazwy wydzia�u w nazwie projektu
					if (projectName != null && !projectName.equals("")) 
					{
						int foundCount = 0;
						String found = "";
						String[] proNames = projectName.split(" ");
		
						for (String part : proNames) {
							if (divShortcut.contains(part)) 
							{
								foundCount++;
								found = part;
								break;
							}
						}
		
						if (foundCount == 1) 
						{
							projectDiv = found;
						}
					}
		
					// 2. skojarzenie wydzia�u z externalId, np. R WE, W WB, W
					// WIPMiFS
					if (projectDiv == null && externalId != null && !externalId.equals("")) 
					{
						String[] extParts = externalId.split(" ");
						if (extParts.length == 2) {
							if (extParts[0].equals("R") || extParts[0].equals("W")) // R** dladziekan�w,W** dlaog�lnowydzia�owych
							{ 
								if (divShortcut.contains(extParts[1])) 
								{
									projectDiv = extParts[1];
								}
							}
						}
					}
					if (projectDiv != null && !projectDiv.equals("")) 
					{
						DSDivision divGuid = null;
						try 
						{
							// ------------------------------------------------
							// omini�cie liter�wki w strukturze organizacyjnej
							// dla dziekana wydzia�u In�ynierii i Ochrony
							// �rodowiska: WIiO�!!!!!!!!
							// ------------------------------------------------
							if (projectDiv.equals("WIiO\u015a")) 
							{
								projectDiv = "WliO\u015a";
							}
							divGuid = DSDivision.findByCode("R-" + projectDiv);
						} 
						catch (DivisionNotFoundException e) 
						{
		
						}
						DSDivision[] departments = divGuid.getChildren();
		
						List<String> enumSelected = new ArrayList<String>();
		
						Map<String, String> enumValuesMD = findIdForAccept(departments, "manager_of_department", false, null, false);
						Map<String, String> enumValuesMU = findIdForAccept(departments, "manager_of_unit", true, null, false);
						Map<String, String> enumValuesSF = findIdForAccept(departments, "financial_section", false, null, true);
		
						MpkAccept mpkInstance = MpkAccept.getInstance();
						enumValuesMD = insertEnumItemIntoTable(enumValuesMD, AcceptType.MANAGER_OF_DEPARTMENT, budgetIdLong, mpkInstance);
		
						if (values.get(dictionaryName+"_SUBGROUPID") != null && values.get(dictionaryName+"_SUBGROUPID").getEnumValuesData() != null) 
						{
							if (enumValuesMD.containsKey(values.get(dictionaryName+"_SUBGROUPID").getEnumValuesData().getSelectedOptions().get(0))) 
							{
								enumSelected.add(values.get(dictionaryName+"_SUBGROUPID").getEnumValuesData().getSelectedOptions().get(0));
							}
						}
						if (enumSelected.size() < 1) 
						{
							enumSelected.add("");
						}
		
						results.put(dictionaryName+"_SUBGROUPID", new EnumValues(enumValuesMD, enumSelected));
		
						enumSelected = new ArrayList<String>();
						enumValuesMU = insertEnumItemIntoTable(enumValuesMU, AcceptType.MANAGER_OF_UNIT, budgetIdLong, mpkInstance);
						results.put(dictionaryName+"_CLASS_TYPE_ID", new EnumValues(enumValuesMU, enumSelected));
		
						enumSelected = new ArrayList<String>();
						enumValuesSF = insertEnumItemIntoTable(enumValuesSF, AcceptType.FINANCIAL_SECTION, budgetIdLong, mpkInstance);
						results.put(dictionaryName+"_CUSTOMSAGENCYID", new EnumValues(enumValuesSF, enumSelected));
					} 
					else 
					{
						// 3. skr�t nazwy projektu w externalId ds_division
						if (projectDiv == null) 
						{
							projectDiv = externalId;
						}
		
						DSDivision[] divGuid = new DSDivision[1];
						try 
						{
							if (externalId != null) 
							{
								divGuid[0] = DSDivision.findByExternalId(externalId);
							} 
							else 
							{
								divGuid[0] = null;
							}
						}
						catch (DivisionNotFoundException e) 
						{
		
						}
						Map<String, String> enumValuesMD = findIdForAccept(divGuid, "manager_of_department", false, null, false);
						Map<String, String> enumValuesMU = findIdForAccept(divGuid, "manager_of_unit", false, null, false);
						Map<String, String> enumValuesSF = findIdForAccept(divGuid, "financial_section", true, null, true);
		
						if (enumValuesMU.size() < 2) 
						{
							enumValuesMU = new LinkedHashMap<String, String>();
							for (String rectors : rectorsShortcut) 
							{
								DSDivision rectorsDiv = DSDivision.findByCode(rectors);
								enumValuesMU = findIdForAccept(rectorsDiv.getChildren(), "manager_of_unit", true, enumValuesMU, false);
							}
							// wymuszone dodania rektora
							try 
							{
								DSDivision rector = DSDivision.findByName("Kierownik dzia�u Rektor"); // TODO
								enumValuesMU.put(rector.getId().toString(), rector.getName());
		
							} 
							catch (Exception e) 
							{
								log.error("Nie znaleziono Rektora w strukturze organizacyjnej");
							}
						}
		
						MpkAccept mpkInstance = MpkAccept.getInstance();
		
						enumValuesMD = insertEnumItemIntoTable(enumValuesMD, AcceptType.MANAGER_OF_DEPARTMENT, budgetIdLong, mpkInstance);
		
						List<String> enumSelected = new ArrayList<String>();
						if (values.get(dictionaryName+"_SUBGROUPID") != null && values.get(dictionaryName+"_SUBGROUPID").getEnumValuesData() != null) 
						{
							if (enumValuesMD.containsKey(values.get(dictionaryName+"_SUBGROUPID").getEnumValuesData().getSelectedOptions().get(0))) 
							{
								enumSelected.add(values.get(dictionaryName+"_SUBGROUPID").getEnumValuesData().getSelectedOptions().get(0));
							}
						}
						if (enumSelected.size() < 1) 
						{
							enumSelected.add("");
						}
		
						results.put(dictionaryName+"_SUBGROUPID", new EnumValues(enumValuesMD, enumSelected));
		
						enumValuesMU = insertEnumItemIntoTable(enumValuesMU, AcceptType.MANAGER_OF_UNIT, budgetIdLong, mpkInstance);
		
						enumSelected = new ArrayList<String>();
						if (values.get(dictionaryName+"_CLASS_TYPE_ID") != null && values.get(dictionaryName+"_CLASS_TYPE_ID").getEnumValuesData() != null) 
						{
							if (enumValuesMU.containsKey(values.get(dictionaryName+"_CLASS_TYPE_ID").getEnumValuesData().getSelectedOptions().get(0)))
							{
								enumSelected.add(values.get(dictionaryName+"_CLASS_TYPE_ID").getEnumValuesData().getSelectedOptions().get(0));
							}
						}
						enumValuesMU.remove("");
						if (enumValuesMU.size() < 1) 
						{
							try
							{
								DSDivision rector = DSDivision.findByName("Kierownik dzia�u Rektor"); // TODO
								enumValuesMU.put(rector.getId().toString(), rector.getName());
		
							} 
							catch (Exception e) 
							{
								log.error("Nie znaleziono Rektora w strukturze organizacyjnej");
							}
						}
		
						results.put(dictionaryName+"_CLASS_TYPE_ID", new EnumValues(enumValuesMU, enumSelected));
		
						enumSelected = new ArrayList<String>();
						enumValuesSF = insertEnumItemIntoTable(enumValuesSF, AcceptType.FINANCIAL_SECTION, budgetIdLong, mpkInstance);
						results.put(dictionaryName+"_CUSTOMSAGENCYID", new EnumValues(enumValuesSF, enumSelected));
					}
		
					// id wybranego budzetu na podstawie, ktorego zostanie
					// pobrana lista jego pozycji budzetowych
					fieldsValues.put("CENTRUMID_1", budgetId);
					for (DataBaseEnumField d : DataBaseEnumField.tableToField.get("simple_erp_per_docusafe_bd_pozycje_view")) 
					{
						// pozycje budzetowe dla wybranego budzetu
						// (budgetID)
						EnumValues val = d.getEnumItemsForDwr(fieldsValues);
						results.put(dictionaryName+"_ACCOUNTNUMBER", val);
						break;
					}
					// pozycja budzetowa
					String pozycjaId = values.get(dictionaryName+"_ACCOUNTNUMBER").getEnumValuesData().getSelectedOptions().get(0);
					if (pozycjaId != null && !pozycjaId.equals("")) 
					{
						// wybrana pozycja budzetowa
						List<String> selectedPositionId = new ArrayList<String>();
						selectedPositionId.add(pozycjaId);
						((EnumValues) results.get(dictionaryName+"_ACCOUNTNUMBER")).setSelectedOptions(selectedPositionId);
		
						// wybrane id pozycji budzetowej na podstawie,
						// ktorej zostanie pobrana lista jej zrodel
						fieldsValues.put("ACCOUNTNUMBER_1", pozycjaId);
						for (DataBaseEnumField d : DataBaseEnumField.tableToField.get("simple_erp_per_docusafe_bd_zrodla_view")) 
						{
							EnumValues val = d.getEnumItemsForDwr(fieldsValues);
							results.put(dictionaryName+"_ACCEPTINGCENTRUMID", val);
							break;
						}
		
						// wybrane zrodlo
						String zrodloId = values.get(dictionaryName+"_ACCEPTINGCENTRUMID").getEnumValuesData().getSelectedOptions().get(0);
						if (zrodloId != null && !zrodloId.equals("")) 
						{
							// wybrana pozcyaj zrodla
							List<String> selectedSoruceId = new ArrayList<String>();
							selectedSoruceId.add(zrodloId);
							((EnumValues) results.get(dictionaryName+"_ACCEPTINGCENTRUMID")).setSelectedOptions(selectedSoruceId);
						}
					} 
					else 
					{
						EnumValues val = new EnumValues(Collections.EMPTY_MAP, Collections.EMPTY_LIST);
						results.put(dictionaryName+"_ACCEPTINGCENTRUMID", val);
					}
				} 
				else 
				{
					EnumValues val = new EnumValues(Collections.EMPTY_MAP, Collections.EMPTY_LIST);
					results.put(dictionaryName+"_ACCOUNTNUMBER", val);
					results.put(dictionaryName+"_ACCEPTINGCENTRUMID", val);
				}

				results.put(dictionaryName+"_CENTRUMID", centrumIdEnum);
			}
		}
		catch (Exception e) 
		{
			log.warn(e.getMessage(), e);
		}
		return results;
	}
	
	private Map<String, String> findIdForAccept(DSDivision[] departments, String acceptanceName, boolean single,
			Map<String, String> enumValues, boolean financialSection) throws EdmException
		{

		if (enumValues == null) 
		{
			enumValues = new LinkedHashMap<String, String>();
		}

		if (!single)
		{
			enumValues.put("", sm.getString("select.wybierz"));
		}

		for (DSDivision div : departments) 
		{
			if (div == null) 
			{
				continue;
			}
			List<AcceptanceCondition> manDeptAcc = AcceptanceCondition.find(acceptanceName, div.getGuid());

			// id o warto�ci dodatniej definiuj� id tabeli ds_division, a id o warto�ci ujemnej definiuj� id tabeli ds_user
			int chooserAccSign = 0; 

			DSDivision manDept = null;
			DSUser manDeptUser = null;

			try 
			{
				if (manDeptAcc != null && manDeptAcc.size() > 0) 
				{
					if (manDeptAcc.get(0).getDivisionGuid() != null && !manDeptAcc.get(0).getDivisionGuid().equals("")) 
					{
						manDept = DSDivision.find(manDeptAcc.get(0).getDivisionGuid());
						chooserAccSign = 1;
					} 
					else if (manDeptAcc.get(0).getUsername() != null && !manDeptAcc.get(0).getUsername().equals("")) 
					{
						manDeptUser = DSUser.findByUsername(manDeptAcc.get(0).getUsername());
						chooserAccSign = -1;
					}
				}
			} catch (EdmException e) 
			{
				log.error("Nieznaleziono ds_division lub ds_user, id");
			}

			if (chooserAccSign != 0)
			{
				if (chooserAccSign > 0 && !manDept.isHidden())
				{
					enumValues.put(manDept.getId().toString(), manDept.getName());
				} 
				else if (!manDept.isHidden())
				{
					enumValues.put("-" + manDeptUser.getId().toString(), manDeptUser.getFirstname() + " " + manDeptUser.getLastname());
				}
			}

			if (single && enumValues.size() > 1) 
			{
				break;
			}
		}

		if (financialSection) 
		{
			if (enumValues.size() == 0) 
			{
				enumValues.put("", "brak");
			}
			else 
			{
				for (String key : enumValues.keySet())
				{
					if (!key.equals("")) 
					{
						String oneKey = key;
						enumValues = new LinkedHashMap<String, String>();
						enumValues.put(oneKey, "znaleziono");
						break;
					}
				}
			}
		}

		return enumValues;
	}

	private Map<String, String> insertEnumItemIntoTable(Map<String, String> enumValues, AcceptType type, Long centrumId, MpkAccept instance) 
	{
		int l = enumValues.containsKey("") ? 1 : 0;

		if (enumValues.size() > l) 
		{

			Session session = null;
			try 
			{
				session = DSApi.context().session();
				try 
				{
					DSApi.context().begin();
				} 
				catch (Exception e) 
				{
					// log.error(e.toString());
				}

				Map<String, String> enumValuesFinal = new LinkedHashMap<String, String>();

				if (l == 1) 
				{
					enumValuesFinal.put("", enumValues.get(""));
					enumValues.remove("");
				}

				for (String key : enumValues.keySet()) 
				{
					List<MpkAccept> found = instance.find(type, key, centrumId);

					if (found == null) 
					{
						MpkAccept toAdd = null;
						
						if (type == AcceptType.FINANCIAL_SECTION)
						{
							toAdd = new MpkFinancialSection();
						} 
						else if (type == AcceptType.MANAGER_OF_DEPARTMENT) 
						{
							toAdd = new MpkManagerOfDepartment();
						} 
						else if (type == AcceptType.MANAGER_OF_UNIT) 
						{
							toAdd = new MpkManagerOfUnit();
						}

						toAdd.setAvailable(true);
						toAdd.setCentrum(0);
						toAdd.setCn(key);
						toAdd.setRefValue(centrumId);
						toAdd.setTitle(enumValues.get(key));
						session.save(toAdd);

						enumValuesFinal.put(toAdd.getId().toString(), toAdd.getTitle());
					} 
					else 
					{
						MpkAccept toShow;
						toShow = found.get(0);
						enumValuesFinal.put(toShow.getId().toString(), toShow.getTitle());
					}
				}

				DSApi.context().commit();
				DSApi.context().begin();

				return enumValuesFinal;
			} 
			catch (Exception e)
			{
				try 
				{
					DSApi.context().rollback();
				} 
				catch (EdmException e1) 
				{
					log.error(e.getMessage(), e1);
				}
				log.error(e.getMessage(), e);
			}
		}
		return enumValues;
	}

	protected void checkTerminRealizacji(Map<String, FieldData> values, StringBuilder msgBuilder)
	{
		if (DwrUtils.isNotNull(values, "DWR_ROK_BUDZETOWY")) {
			String field;
			if (DwrUtils.isNotNull(values, "DWR_TERMIN_OD_DO")) {
				Map<String, FieldData> termin = values.get("DWR_TERMIN_OD_DO").getDictionaryData();
				field = "TERMIN_OD_DO_DO";
				if (termin.get(field).getDateData() != null) {
					isRokBeforeTermin(values, msgBuilder, field, termin);
				}
			} else if (DwrUtils.isNotNull(values, "DWR_TERMIN_DATA_ZAKONCZENIA")) {
				Map<String, FieldData> termin = values.get("DWR_TERMIN_DATA_ZAKONCZENIA").getDictionaryData();
				field = "TERMIN_DATA_ZAKONCZENIA_OD";
				if (termin.get(field).getDateData() != null) {
					isRokBeforeTermin(values, msgBuilder, field, termin);
				}
			}
		}
	}

	protected void isRokBeforeTermin(Map<String, FieldData> values, StringBuilder msgBuilder, String field, Map<String, FieldData> termin) {
		Integer rok = values.get("DWR_ROK_BUDZETOWY").getIntegerData() + 1;
		GregorianCalendar rokDate = new GregorianCalendar();
		rokDate.setTime(DateUtils.parseDateFromYear(rok.toString()));
		rokDate.set(GregorianCalendar.MILLISECOND, 0);
		GregorianCalendar terminDo = new GregorianCalendar();
		terminDo.setTime(termin.get(field).getDateData());
		if (rokDate.before(terminDo) || rokDate.equals(terminDo)) {
			if (msgBuilder.length() == 0)
				msgBuilder.append("B��d: Termin realizacji nie mie�ci si� w podanym roku bud�etowym.");
			else
				msgBuilder.append(";  Termin realizacji nie mie�ci si� w podanym roku bud�etowym.");
		}
	}
}
