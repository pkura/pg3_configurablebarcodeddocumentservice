package pl.compan.docusafe.parametrization.polcz;

import java.io.File;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.jbpm.api.listener.EventListenerExecution;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ExportInvoiceProcessListener extends AbstractEventListener
{
	private static final Logger log = LoggerFactory.getLogger(ExportInvoiceProcessListener.class);

	@Override
	public void notify(EventListenerExecution execution) throws Exception
	{

		OfficeDocument document = Jbpm4Utils.getDocument(execution);
		FieldsManager fm = document.getFieldsManager();

		try {
			 
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	 
			// root elements
			Document doc = docBuilder.newDocument();
			Element dzk = doc.createElement("DZK");
			doc.appendChild(dzk);
	 
			String typFaktury = null;
			// ustawienie typu faktury
			if (fm.getEnumItem("WALUTA").getId() == 1)
			{
				typFaktury = fm.getEnumItemCn("TYP_FAKTURY_PLN");
			}
			else
			{
				typFaktury = fm.getEnumItemCn("TYP_FAKTURY_WAL");
			}
			dzk.setAttribute("typdok_idn", typFaktury);
			
			//  
			
			
			// ustawienie id dokumentu
			String idDocument = document.getId().toString();
			dzk.setAttribute("dok_idm", idDocument);
						
			// ustawienie daty wystawienia
			java.util.Date datDok = (java.util.Date)fm.getKey("DATA_WYSTAWIENIA");
			dzk.setAttribute("datdok", DateUtils.mssqlDateFormat102.format(datDok));
			
			// ustawienie daty wpływu
			java.util.Date  dataodb = (java.util.Date) fm.getKey("F_DATA_WPLYWU");
			dzk.setAttribute("datodb", DateUtils.mssqlDateFormat102.format(dataodb));
			
			// ustawienie daty rejestracji
			java.util.Date datprzyj  = document.getCtime();
			dzk.setAttribute("datprzyj", DateUtils.mssqlDateFormat102.format(datprzyj));
			
			// ustawienie daty platnosci - termin platnosci
			java.util.Date data_zaplaty =  fm.getKey("TERMIN_PLATNOSCI_Z_UMOWY") != null ? (java.util.Date) fm.getKey("TERMIN_PLATNOSCI_Z_UMOWY") : (java.util.Date) fm.getKey("TERMIN_PLATNOSCI");
			dzk.setAttribute("data_zaplaty", DateUtils.mssqlDateFormat102.format(data_zaplaty));
			
			// ustawienie nr faktury
			String dokobcy = fm.getKey("NR_FAKTURY").toString();
			dzk.setAttribute("dokobcy", dokobcy);
				
			// ustawienie dostawcy id
			String dostawca_idn = document.getSender().getWparam().toString();
			dzk.setAttribute("dostawca_idn", dostawca_idn);
						
			// ustawienie sposobu platnosi
			String warplat_idn = fm.getEnumItem("SPOSOB").getTitle();
			dzk.setAttribute("warplat_idn", warplat_idn);
			
			// ustawienie komorka_id
			//dzk.setAttribute("komorka_idn", "PCz");
			
			// ustawienie komorka_id
			String addinfo = fm.getKey("ADDINFO") != null ? fm.getKey("ADDINFO").toString() : "Brak";
			dzk.setAttribute("uwagi", addinfo);
			
			List<Long> stawki = (List<Long>) fm.getKey("STAWKI_VAT");
			
			Map<String, Object> stawkaValues = new HashMap<String, Object>();
			for (Long stawka : (List<Long>) stawki)
			{
				Element dzk_poz = doc.createElement("DZK_POZ");
				stawkaValues = DwrDictionaryFacade.dictionarieObjects.get("STAWKI_VAT").getValues(stawka.toString());
				
				// ustawienie nr konta
				String wytwor_idm = DataBaseEnumField.getEnumItemForTable("simple_erp_per_docusafe_wytwor_view", Integer.parseInt(((EnumValues) stawkaValues.get("STAWKI_VAT_POZYCJA")).getSelectedId())).getCn();
				dzk_poz.setAttribute("wytwor_idm", wytwor_idm);				
				
				// ustawienie komroki_idn
				String komorka_idk = null;
				if (((EnumValues) stawkaValues.get("STAWKI_VAT_KOMORKA")).getSelectedValue().length() > 0)
					komorka_idk = DataBaseEnumField.getEnumItemForTable("simple_erp_per_docusafe_komorka_view", Integer.parseInt(((EnumValues) stawkaValues.get("STAWKI_VAT_KOMORKA")).getSelectedId())).getCn();
				dzk_poz.setAttribute("komorka_idn",  komorka_idk);
				
				// ustawienie zlecprod_idm
				String zlecprod_idm = null;
				if ((((EnumValues) stawkaValues.get("STAWKI_VAT_ZLECENIE")).getSelectedValue().length() > 0))
					DataBaseEnumField.getEnumItemForTable("simple_erp_per_docusafe_zlecprod_view", Integer.parseInt(((EnumValues) stawkaValues.get("STAWKI_VAT_ZLECENIE")).getSelectedId())).getCn();
				dzk_poz.setAttribute("zlecprod_idm", zlecprod_idm);
				
				// ustawienie ilosci
				dzk_poz.setAttribute("ilosc", "1");
				
				// ustawienie ceny
				BigDecimal netto = new BigDecimal(stawkaValues.get("STAWKI_VAT_NETTO").toString()).setScale(2, RoundingMode.HALF_UP);
				dzk_poz.setAttribute("cena", netto.toString());
				
				// ustawienie jednostki
				dzk_poz.setAttribute("jm_idn", "szt");
				
				// ustawienie stawki vat
				EnumItem vatItem = DataBaseEnumField.getEnumItemForTable("simple_erp_per_docusafe_vatstaw_view", Integer.parseInt(((EnumValues) stawkaValues.get("STAWKI_VAT_STAWKA")).getSelectedId()));
				String vat = vatItem.getTitle();
				dzk_poz.setAttribute("vatstaw_ids", vat);
				
				// ustawienie kwoty brutto
				BigDecimal brutto = new BigDecimal(stawkaValues.get("STAWKI_VAT_BRUTTO").toString()).setScale(2, RoundingMode.HALF_UP);
				dzk_poz.setAttribute("kwota_brutto",  brutto.toString());

				// ustawienie przeznaczenia
				String przeznaczenie_id = ((EnumValues) stawkaValues.get("STAWKI_VAT_DZIALALNOSC")).getSelectedId();
				if (przeznaczenie_id.equalsIgnoreCase("342"))
				{
					dzk_poz.setAttribute("przeznaczenie_id",  "2");
				}
				else if (przeznaczenie_id.equalsIgnoreCase("340") || przeznaczenie_id.equalsIgnoreCase("341") )
				{
					dzk_poz.setAttribute("przeznaczenie_id",  "1");
				}
				else if (przeznaczenie_id.equalsIgnoreCase("343"))
				{
					dzk_poz.setAttribute("przeznaczenie_id",  "0");
				}
				
				// ustawienie uwagi
				dzk_poz.setAttribute("uwagi",  "Brak");
				
				// ID pozycji
				// proste zapytanie, wyciągnięcie wbranych kolumn z warunkiem where
				NativeCriteria nc = DSApi.context().createNativeCriteria("DSG_STAWKI_VAT_FAKTURY", "d");
				nc.setProjection(NativeExps.projection()
						// ustalenie kolumn
						.addProjection("d.POZYCJAID")
						.addProjection("d.ID"))
					// warunek where
					.add(NativeExps.eq("d.ID", stawka));
				
				Integer mpkID = null;
				// pobranie i wyświetlenie wyników
				CriteriaResult cr = nc.criteriaResult();
				while (cr.next())
				{
					Long pozycjaId = cr.getLong(0, null); 
					mpkID = pozycjaId != null ? pozycjaId.intValue() : null;
				}
				if (mpkID == null)
					throw new EdmException("BRAK mpkID!!!)");
				
				DataBaseEnumField.reloadForTable("pozycje_budzetowe_doc_view");
				EnumItem docItem = DataBaseEnumField.getEnumItemForTable("pozycje_budzetowe_doc_view", mpkID);
				
				CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(mpkID.longValue());
				Integer blokadaId = mpk.getAnalyticsId();
				Integer baseDocumnetId = mpk.getVatTypeId();
				Integer wniosekId = null;
				if (baseDocumnetId != null)
				{
					pl.compan.docusafe.core.base.Document oDoc = OfficeDocument.find(baseDocumnetId.longValue(), false);
					wniosekId = (Integer)oDoc.getFieldsManager().getKey("bd_rezerwacja_id");
				}
				else
					wniosekId = (Integer) fm.getKey("bd_rezerwacja_id");
				// ustawienie rezerwacja_id
				dzk_poz.setAttribute("bd_rezerwacja_id",  wniosekId.toString());
				
				// ustawienie bd_budzet_ko_id
				dzk_poz.setAttribute("bd_rezerwacja_poz_id",  blokadaId.toString());
				
				dzk.appendChild(dzk_poz);
			}
			
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			DOMSource source = new DOMSource(doc);

			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);
			StringWriter writer = new StringWriter();
			transformer.transform(source, new StreamResult(writer));
			String invoiceXml = writer.getBuffer().toString().replaceAll("\n|\r", "");
			
			//String file_path_name = Docusafe.getAdditionProperty("faktura_xml_path");
			//StreamResult result = new StreamResult(new File(file_path_name));
			//transformer.transform(source, result);
	 
			ExportInvoiceToSimpleERP.export(invoiceXml);
	 
		  } catch (ParserConfigurationException pce) {
			log.error(pce.getMessage(), pce);
			throw new EdmException(pce.getMessage());
		  } catch (TransformerException tfe) {
			  log.error(tfe.getMessage(), tfe);
			  throw new EdmException(tfe.getMessage());
		  }
	}
	
}
