package pl.compan.docusafe.parametrization.polcz;

import com.google.common.collect.Maps;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.*;

public class NormalLogic extends AbstractDocumentLogic
{

	private static NormalLogic instance;
	protected static Logger log = LoggerFactory.getLogger(NormalLogic.class);

	public static NormalLogic getInstance()
	{
		if (instance == null)
			instance = new NormalLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
	{
		Field msg = null;

		if (values.get("DWR_DOC_DATE") != null && values.get("DWR_DOC_DATE").getData() != null)
		{
			Date startDate = new Date();
			Date finishDate = values.get("DWR_DOC_DATE").getDateData();

			if (finishDate.after(startDate))
			{
				msg = new Field("messageField", "Data pisma nie mo瞠 by� dat� z przysz這軼i.", Field.Type.BOOLEAN);
				values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
			}
		}
		return msg;

	}

	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		log.info("type: {}", type);
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("TYPE", type);
		DSUser user = DSApi.context().getDSUser();

		fm.reloadValues(values);
	}

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		as.useFolderResolver(document);
		as.useAvailableProviders(document);
		log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
	{
		System.out.println("data pisma onsavaaction" + fieldValues.get("DOC_DATE"));

		if ((kind.getCn().equals("normal") || kind.getCn().equals("normal_int")) && fieldValues.get("RECIPIENT_HERE") == null)
		{
			throw new EdmException("Nie istnieje taki odbiorca w systemie.");
		}

		if (kind.getCn().equals("normal_out") && fieldValues.get("SENDER_HERE") == null)
		{
			throw new EdmException("Nie istnieje taki nadawca w systemie.");

		}

		if (fieldValues.get("DOC_DATE") != null)
		{
			System.out.println("data pisma onsavaaction" + fieldValues.get("DOC_DATE"));
			Date startDate = new Date();
			Date finishDate = (Date) fieldValues.get("DOC_DATE");

			if (finishDate.after(startDate))
			{
				throw new EdmException("Data pisma nie mo瞠 by� dat� z przysz這軼i.");
			}
		}
	}

	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

}
