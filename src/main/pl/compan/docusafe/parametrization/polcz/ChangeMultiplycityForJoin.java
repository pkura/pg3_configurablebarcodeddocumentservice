package pl.compan.docusafe.parametrization.polcz;

import java.util.Map;
import org.jbpm.api.Execution;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ChangeMultiplycityForJoin implements ExternalActivityBehaviour
{
	String changeReason = "";
	
	private static final Logger log = LoggerFactory.getLogger(ChangeMultiplycityForJoin.class);
	
	public void execute(ActivityExecution execution) throws Exception 
	{
		if (changeReason.equals("finish"))
			execution.setVariable("changeReason", Jbpm4Constants.CHANGE_REASON_FINISH);
		else
			execution.setVariable("changeReason", Jbpm4Constants.CHANGE_REASON_CORRECTION);
		
		int count = 1;
		for (Execution ex : execution.getProcessInstance().getExecutions())
		{
			if (ex.getState().equals("inactive-join"))
				count++;
		}

		execution.setVariable("count", count);
		
	}
	
	public void signal(ActivityExecution execution, String signalName, Map<String, ?> parameters) throws Exception 
	{
		execution.takeDefaultTransition();
	}
}
