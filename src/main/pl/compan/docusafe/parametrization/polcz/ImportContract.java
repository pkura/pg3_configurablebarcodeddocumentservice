package pl.compan.docusafe.parametrization.polcz;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ImportContract {
	protected static Logger log = LoggerFactory.getLogger(ImportContract.class);
	
	public static ImportAnswer newContract(String tenderSignature, String contractNo, ContractItem[] items) {
		log.debug("newContract webService RUN!");
		Session session = null;
		int exist = 0;
		
		try {
			DSApi.openAdmin();
			session = DSApi.context().session();

			if (tenderSignature == null || tenderSignature.equals("") || contractNo == null || contractNo.equals("")) {
				return new ImportAnswer(new Date().getTime(), 6, "IMPORT FAILED!");
			}
			
	        session.beginTransaction();
	        
	        List<Contract> contracts = Contract.find(contractNo, tenderSignature);
	        
			if (contracts != null) {
				if (contracts.size() > 1) {
					return new ImportAnswer(new Date().getTime(), 7, "IMPORT FAILED!");
				} else {
					Contract found = contracts.get(0);
					if (!found.isUsed()) {
						session.delete(found);
						exist = 1;
					} else {
						return new ImportAnswer(new Date().getTime(), 8, "IMPORT FAILED!");
					}
				}
			}
	        
	        Contract contract = new Contract(contractNo, tenderSignature);
	        log.debug(contract);
	        session.save(contract);
	   
	        for (ContractItem i : items) {
	        	i.setContract(contract);
	        	log.debug(i);
	        	session.save(i);
	        }
	   
	        session.getTransaction().commit();
	        session.close();
	        DSApi.close();
	        return new ImportAnswer(new Date().getTime(), exist, "IMPORT DONE!");
	        
		} catch (Exception e) {
			try {
				session.getTransaction().rollback();
				DSApi.close();
			} catch (EdmException e1) {
				log.error(e.getMessage(), e1);
			}
			log.error(e.getMessage(), e);
			return new ImportAnswer(new Date().getTime(), 9, "IMPORT FAILED!");
		} 
	}
}
