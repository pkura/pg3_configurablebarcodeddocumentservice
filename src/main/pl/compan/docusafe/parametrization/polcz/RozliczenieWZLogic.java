package pl.compan.docusafe.parametrization.polcz;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DSUserEnumField;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class RozliczenieWZLogic extends AbstractBudgetApplicationLogic
{
	
	private static final String CENTRUM_ID_TABLE_NAME = "simple_erp_per_docusafe_bd_str_budzet_view";
	
	StringManager sm = GlobalPreferences.loadPropertiesFile(ApplicationWZPLogic.class.getPackage().getName(), null);
	
	private static ApplicationWZPLogic instance;

	public static ApplicationWZPLogic getInstance() 
	{
		if (instance == null)
			instance = new ApplicationWZPLogic();
		return instance;
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException 
	{

		super.setInitialValues(fm, type);
		
		DSUserEnumField dsef = (DSUserEnumField) fm.getField("KOORDYNATOR");
		if (fm.getEnumItem("WORKER_DIVISION") != null) 
		{
			dsef.setRefGuid(DSDivision.findById(fm.getEnumItem("WORKER_DIVISION").getId()).getGuid());
		}
	}
	
	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) 
	{
		
		pl.compan.docusafe.core.dockinds.dwr.Field msgField = super.validateDwr(values, fm);
		
		StringBuilder msgBuilder = new StringBuilder();
		
		if (msgField != null)
			msgBuilder.append(msgField.getLabel());
		
					
		if (msgBuilder.length() > 0) 
		{
			values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
			return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(),
					pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
		}
		
		return null;
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException 
	{
		// wystartowanie procesu
		super.onStartProcess(document, event);

		FieldsManager fm = document.getFieldsManager();

		// Sprawdzenie czy loczna kwota brutto rowna sie sumie kwot wszsytkich pozycji
		isKwotaBruttoEqSumBudgetPositions(fm);

	}

	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) 
	{
		Map<String, Object> results = super.setMultipledictionaryValue(dictionaryName, values);

		
		return results;
	}
	
	private void isKwotaBruttoEqSumBudgetPositions(FieldsManager fm) throws EdmException
	{
		BigDecimal netto = (BigDecimal) fm.getFieldValues().get("SUMA_BRUTTO");
		List<Long> ids = (List<Long>) fm.getKey("MPK");
		BigDecimal suma = BigDecimal.ZERO;

		for (Long id : ids) 
		{
			Map<String, Object> mpk = DwrDictionaryFacade.dictionarieObjects.get("MPK").getValues(id.toString());
			BigDecimal kwota = BigDecimal.valueOf((Double) mpk.get("MPK_AMOUNT"));
			suma = suma.add(kwota);
		}
		if (!netto.equals(suma.setScale(2)))
		{
			throw new EdmException("��czna kwota brutto musi by� r�wna sumie kwot wszystkich pozycji bud�etowych.");
		}
	}
}