package pl.compan.docusafe.parametrization.polcz;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.FolderResolver;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.users.DSUser;

import com.google.common.collect.Maps;

public class ApplicationWSPWLogic extends AbstractApplicationLogic {
	private static ApplicationWSPWLogic instance;

	public static ApplicationWSPWLogic getInstance() {
		if (instance == null)
			instance = new ApplicationWSPWLogic();
		return instance;
	}

	/**
	 * @param user
	 * @return
	 * @throws EdmException
	 */
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		try 
		{
			FolderResolver fr = document.getDocumentKind().getDockindInfo().getFolderResolver();
			if (fr != null)
			{
				as.useFolderResolver(document);
			}
			Map<String, Object> id = Maps.newHashMap();
				id.put("WNIOSKODAWCA", getUserId());
			document.getDocumentKind().setOnly(document.getId(), id);
		} 
		catch (SQLException e) 
		{
			log.error(e.getMessage(), e);
		}
	}
	
	/**
	 * Pobiera id studenta zaimportowanego z zewn�trz na podstawie numeru peselu
	 * @return
	 * @throws SQLException
	 * @throws EdmException
	 */
	private int getUserId() throws SQLException, EdmException 
	{
		DSUser user = DSApi.context().getDSUser();
		log.info("ApplicationWSPWLogic.getUserId(): user.getName(): " + user.getName());
		PreparedStatement ps =
				DSApi.context().prepareStatement("SELECT id FROM dsg_pcz_application_wspw_students where pesel = ?");
		ps.setString(1, user.getName());
		ResultSet rs = ps.executeQuery();
		rs.next();
		int id = rs.getInt("id");
		log.info("Pobranie id user'a: "	+ id);
		rs.close();
		ps.close();
		return id;
	}
}