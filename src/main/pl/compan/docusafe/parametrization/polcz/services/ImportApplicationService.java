package pl.compan.docusafe.parametrization.polcz.services;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

public class ImportApplicationService extends ServiceDriver implements Service
{
	public static final Logger log = LoggerFactory.getLogger(ImportApplicationService.class);
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(ImportApplicationService.class.getPackage().getName(), null);

	private Property[] properties;
	private Timer timer;
	private Integer periodWww = 10;
	private String url = Docusafe.getAdditionProperty("erp.database.url");
	private String username = Docusafe.getAdditionProperty("erp.database.user");
	private String password = Docusafe.getAdditionProperty("erp.database.password");

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, ImportApplicationService.this, "periodWww", "Co ile minut", Integer.class);
		}

		protected Object getValueSpi()
		{
			return periodWww;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (ImportApplicationService.this)
			{
				if (object != null)
					periodWww = (Integer) object;
				else
					periodWww = 10;
			}
		}
	}

	class URLProperty extends Property
	{
		public URLProperty()
		{
			super(SIMPLE, PERSISTENT, ImportApplicationService.this, "url", "�cie�ka do bazy", String.class);
		}

		protected Object getValueSpi()
		{
			return url;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (ImportApplicationService.this)
			{
				if (object != null)
					url = (String) object;
				else
					url = Docusafe.getAdditionProperty("erp.database.url");
			}
		}
	}

	class UsernameProperty extends Property
	{
		public UsernameProperty()
		{
			super(SIMPLE, PERSISTENT, ImportApplicationService.this, "username", "Nazwa u�ytkownika", String.class);
		}

		protected Object getValueSpi()
		{
			return username;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (ImportApplicationService.this)
			{
				if (object != null)
					username = object.toString();
				else
					username = Docusafe.getAdditionProperty("erp.database.user");
			}
		}
	}

	class PasswordProperty extends Property
	{
		public PasswordProperty()
		{
			super(SIMPLE, PERSISTENT, ImportApplicationService.this, "password", "Has�o", String.class);
		}

		protected Object getValueSpi()
		{
			return password;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (ImportApplicationService.this)
			{
				if (object != null)
					password = (String) object;
				else
					password = Docusafe.getAdditionProperty("erp.database.password");
			}
		}
	}

	public ImportApplicationService()
	{
		properties = new Property[] { new ImportTimesProperty(), new URLProperty(), new UsernameProperty(), new PasswordProperty() };
	}

	@Override
	protected void start() throws ServiceException
	{
		log.info("Start uslugi ImportApplicationService");
		timer = new Timer(true);
		timer.schedule(new Import(), 0, periodWww * DateUtils.MINUTE);
		console(Console.INFO, sm.getString("System ImportApplicationService wystartowal"));
	}

	@Override
	protected void stop() throws ServiceException
	{
		log.info("Stop uslugi UsersIntegrationService");
		console(Console.INFO, sm.getString("System ImportApplicationService zako�czy� dzia�anie"));
		try
		{
			DSApi.close();
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask
	{
		@Override
		public void run()
		{

			Connection conn = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			List<ApplicationSimple> applications = new ArrayList<ApplicationSimple>();
			try
			{
				DSApi.openAdmin();
				
				conn = DriverManager.getConnection(url, username, password);

				ps = DSApi.context().prepareStatement("select MAX(bd_rezerwacja_id) from rezerwacje_view where simple_imported = 1");

				rs = ps.executeQuery();

				if (rs.next() && rs.getObject(1) != null)
				{
					ps = conn.prepareStatement("select * from uv_gsd_budzetowanie where bd_rezerwacja_id > ? order by bd_rezerwacja_id");
					ps.setLong(1, rs.getLong(1));
					rs = ps.executeQuery();
					importFromSimple(rs, applications);
				}
				else
				{
					ps = conn.prepareStatement("select * from uv_gsd_budzetowanie order by bd_rezerwacja_id");
					rs = ps.executeQuery();
					importFromSimple(rs, applications);
				}
				
				createDocuments(applications);
				
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
				try
				{
					DSApi.context().rollback();
				}
				catch (Exception e1)
				{
					log.error(e.getMessage(), e1);
				}
			}
			finally
			{
				try
				{
					DSApi.context().closeStatement(ps);
					DSApi.close();
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
				}
			}
		}
	}

	private void importFromSimple(ResultSet rs, List<ApplicationSimple> applications) throws SQLException
	{
		while (rs.next())
		{
			if (!applicationCreatedInDocusafe(rs.getLong(1)))
			{
				ApplicationSimple applicationSimple = createApplication(rs);
				console(Console.INFO, "Przygotowanie wniosku z simple.erp: " + applicationSimple.toString());
				MpkSimple mpkSimple = createMpk(rs);
				console(Console.INFO, "Przygotowanie mpk (" + mpkSimple.toString() + ") dla wniosku " + applicationSimple.getBd_rezerwacja_imd());
				if (applications.contains(applicationSimple))
				{
					int indexOf = applications.indexOf(applicationSimple);
					applications.get(indexOf).addMpk(mpkSimple);
				}
				else
				{
					applicationSimple.addMpk(mpkSimple);
					applications.add(applicationSimple);
				}
			}
		}
	}
	
	public void createDocuments(List<ApplicationSimple> applications) throws EdmException
	{
		for (ApplicationSimple app : applications)
		{
			console(Console.INFO, "Utworzenie wniosku w Docusafe na podstawie wniosku z Sinple.ERP (" + app.toString() + ") : " + SimpleApplicationCreator.getInstance().create(app));
		}
	}

	private ApplicationSimple createApplication(ResultSet rs)
	{
		ApplicationSimple app = null;
		try
		{
			app = new ApplicationSimple();

			Long bd_rezerwacja_id = rs.getLong("bd_rezerwacja_id");
			app.setBd_rezerwacja_id(bd_rezerwacja_id);

			String bd_rezerwacja_idm = rs.getString("bd_rezerwacja_idm");
			app.setBd_rezerwacja_imd(bd_rezerwacja_idm);
			
			String bd_typ_rezerwacja_ids = rs.getString("bd_typ_rezerwacja_ids");
			app.setBd_typ_rezerwacja_ids(bd_typ_rezerwacja_ids);

			Integer status = rs.getInt("status");
			app.setStatus(status);
			
			Long bd_typ_rezerwacja_id = rs.getLong("bd_typ_rezerwacja_id");
			app.setBd_typ_rezerwacja_id(bd_typ_rezerwacja_id);

			Date datutw = rs.getDate("datutw");
			app.setDatautw(datutw);

			Date datzatw = rs.getDate("datzatw");
			app.setDatzatw(datzatw);

			Date datres = rs.getDate("datrez");
			app.setDatres(datres);

			Long pracownik_id = rs.getLong("pracownik_id");
			app.setPracownik_id(pracownik_id);

			String imie = rs.getString("imie");
			app.setImie(imie);

			String nazwisko = rs.getString("nazwisko");
			app.setNazwisko(nazwisko);

			String utw = rs.getString("utw");
			app.setUtw(utw);

			String utw_imie = rs.getString("utw_imie");
			app.setUtw_imie(utw_imie);

			String utw_nazwisko = rs.getString("utw_nazwisko");
			app.setUtw_nazwisko(utw_nazwisko);

			String zatw = rs.getString("zatw");
			app.setZatw(zatw);

			String zatw_imie = rs.getString("zatw_imie");
			app.setZatw_imie(zatw_imie);

			String zatw_nazwisko = rs.getString("zatw_nazwisko");
			app.setZatw_nazwisko(zatw_nazwisko);

			String nazwa = rs.getString("nazwa");
			app.setNazwa(nazwa);

			String opis = rs.getString("opis");
			app.setOpis(opis);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}

		return app;
	}

	private MpkSimple createMpk(ResultSet rs)
	{
		MpkSimple mpk = null;
		try
		{
			mpk = new MpkSimple();

			Long bd_rezerwacja_poz_id = rs.getLong("bd_rezerwacja_poz_id");
			mpk.setBd_rezerwacja_poz_id(bd_rezerwacja_poz_id);

			Long bd_budzet_ko_id = rs.getLong("bd_budzet_ko_id");
			mpk.setBd_budzet_ko_id(bd_budzet_ko_id);

			Long bd_str_budzet_id = rs.getLong("bd_str_budzet_id");
			mpk.setBd_str_budzet_id(bd_str_budzet_id);
			
			Long bd_rodzaj_ko_id = rs.getLong("bd_rodzaj_ko_id");
			mpk.setBd_rodzaj_ko_id(bd_rodzaj_ko_id);

			Long zrodlo_id = getZrodloId(bd_rodzaj_ko_id, rs.getLong("zrodlo_id"));
		
			mpk.setZrodlo_id(zrodlo_id);

			BigDecimal koszt = rs.getBigDecimal("koszt");
			mpk.setKoszt(koszt);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return mpk;
	}

	private Long getZrodloId(Long pozycja_id, Long bd_zrodlo_id)
	{
		try
		{
			PreparedStatement ps = DSApi.context().prepareStatement("select ID from simple_erp_per_docusafe_bd_zrodla_view where refValue = ? and cn = ?");
			ps.setString(1, pozycja_id.toString());
			ps.setString(2, "0" + bd_zrodlo_id);
			ResultSet rs = ps.executeQuery();
	
			if (rs.next())
				return rs.getLong(1);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return null;
	}

	private boolean applicationCreatedInDocusafe(long bd_rezerwacja_id)
	{
		try
		{
			PreparedStatement ps = DSApi.context().prepareStatement("select bd_rezerwacja_id from rezerwacje_view where simple_imported = 0 and bd_rezerwacja_id = " + bd_rezerwacja_id);
	
			ResultSet rs = ps.executeQuery();
	
			return rs.next();
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			return false;
		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}

}
