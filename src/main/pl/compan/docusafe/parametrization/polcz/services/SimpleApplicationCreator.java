package pl.compan.docusafe.parametrization.polcz.services;

import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.dbutils.DbUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.parametrization.polcz.jbpm.CreateApplicationInErpListener;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Maps;

import edu.emory.mathcs.backport.java.util.LinkedList;

public class SimpleApplicationCreator
{
	public static final Logger log = LoggerFactory.getLogger(SimpleApplicationCreator.class);
	private static SimpleApplicationCreator instance;
	
	public boolean create(ApplicationSimple appSimple)
	{

		try
		{
			if (appSimple.getStatus() == null || appSimple.getStatus() == 0)
				return false;
			
			String dockindCn = CreateApplicationInErpListener.WNIOSKI_SIMPLE_TO_DOCUSAFE_CN.get(appSimple.getBd_typ_rezerwacja_ids());
			DocumentKind applicationDockind = DocumentKind.findByCn(dockindCn);
			if (applicationDockind == null)
				applicationDockind = DocumentKind.findByCn("application_general"); 
			
			OutOfficeDocument newDocument = new OutOfficeDocument();
			Document a = new Document(applicationDockind.getName(), applicationDockind.getName());
			newDocument.setCreatingUser(DSApi.context().getPrincipalName());
			newDocument.setSummary(applicationDockind.getName());
			newDocument.setDocumentKind(applicationDockind);

			newDocument.create();
			Long newDocumentID = newDocument.getId();

			setAfterCreate(newDocument);
			
			Map<String, Object> valuesToSet = Maps.newHashMap();

			valuesToSet.put("STATUS", 2005);
			valuesToSet.put("WNIOSKODAWCA", getUserId(appSimple.getPracownik_id(), appSimple.getImie(), appSimple.getNazwisko(), appSimple.getUtw(), appSimple.getUtw_imie(), appSimple.getUtw_nazwisko()));
			valuesToSet.put("NR_WNIOSEK", appSimple.getBd_rezerwacja_imd());
			valuesToSet.put("DATA_WYPELNIENIA", appSimple.getDatautw());
			String temat = appSimple.getNazwa();
			if (temat != null && !temat.equals(""))
			{
				temat +=  appSimple.getOpis() != null ? " / " + appSimple.getOpis() : "";
			}
			else
				temat = appSimple.getOpis() != null ? " / " + appSimple.getOpis() : "";
				
			valuesToSet.put("TEMAT", temat);

			List<Long> mpkIds = new LinkedList();
			List<Long> realizationIds = new LinkedList();
			for (MpkSimple mpk : appSimple.getMpks())
			{

				CentrumKosztowDlaFaktury newMpk = new CentrumKosztowDlaFaktury();

				newMpk.setCentrumId(mpk.getBd_str_budzet_id().intValue());
				newMpk.setAccountNumber(mpk.getBd_rodzaj_ko_id().toString());
				realizationIds.add(mpk.getBd_rodzaj_ko_id());
				if (mpk.getZrodlo_id() != null)
					newMpk.setAcceptingCentrumId(mpk.getZrodlo_id().intValue());
				newMpk.setAnalyticsId(mpk.getBd_rezerwacja_poz_id().intValue());
				newMpk.setAmount(mpk.getKoszt());
				if (appSimple.getStatus() == 1)
					newMpk.setLocationId(16060);
				else
					newMpk.setLocationId(16061);
				Persister.create(newMpk);
				mpkIds.add(newMpk.getId());
			}
			valuesToSet.put("MPK", mpkIds);
			valuesToSet.put("REALIZACJA_BUDZETU", realizationIds);
			applicationDockind.setOnly(newDocumentID, valuesToSet);
			setApplicationNumber(newDocument, appSimple.getBd_rezerwacja_id().intValue());
			return true;
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return false;
	}

	private void setApplicationNumber(Document doc, Integer applicationNumber)
	{
		PreparedStatement ps = null;
		try
		{

			String sql = "update " + doc.getDocumentKind().getTablename() + " set bd_rezerwacja_id = ?, simple_imported = 1 where document_id = ?";
			ps = DSApi.context().prepareStatement(sql);
			ps.setInt(1, applicationNumber);
			ps.setLong(2, doc.getId());
			ps.executeUpdate();
			ps.close();
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			DbUtils.closeQuietly(ps);
		}

	}

	private Long getUserId(Long pracownik_id, String imie, String nazwisko, String utw, String utw_imie, String utw_nazwisko)
	{
		Long userId = null;
		if (pracownik_id != null)
		{
			try
			{
				DSUser user = DSUser.findByExtension(pracownik_id.toString());
				userId = user.getId();
				return userId;
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
			}
		}
		
		if (userId == null)
		{
			DSUser user = null;
			try
			{
				user = DSUser.findByFirstnameLastname(utw_imie.trim(), utw_nazwisko.trim());
				userId = user.getId();
			}
			catch (Exception e)
			{
				try
				{
					user = UserFactory.getInstance().createUser(utw.trim(), utw_imie.trim(), utw_nazwisko.trim());
					user.setCtime(new Timestamp(new Date().getTime()));
					userId = user.getId();
				}
				catch (EdmException e1)
				{
					log.error(e.getMessage(), e);
				}
			}
		}
		return userId;
	}

	private void setAfterCreate(OfficeDocument document) throws EdmException
	{
		((OutOfficeDocument) document).setInternal(true);
		try
		{
			java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

			DSUser author = DSApi.context().getDSUser();
			String user = author.getName();
			String fullName = author.getLastname() + " " + author.getFirstname();

			perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

			Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
			perms.addAll(documentPermissions);
			((AbstractDocumentLogic) document.getDocumentKind().logic()).setUpPermission(document, perms);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	public static SimpleApplicationCreator getInstance()
	{
		if (instance == null)
			instance = new SimpleApplicationCreator();
		
		return instance;
	}
}
