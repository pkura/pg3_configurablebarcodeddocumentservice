package pl.compan.docusafe.parametrization.polcz.services;

import java.math.BigDecimal;

public class MpkSimple
{
	Long bd_rezerwacja_poz_id;
	Long bd_budzet_ko_id;
	Long bd_str_budzet_id;
	Long bd_rodzaj_ko_id;
	Long zrodlo_id;
	BigDecimal koszt;
	
	public Long getBd_rezerwacja_poz_id()
	{
		return bd_rezerwacja_poz_id;
	}
	public void setBd_rezerwacja_poz_id(Long bd_rezerwacja_poz_id)
	{
		this.bd_rezerwacja_poz_id = bd_rezerwacja_poz_id;
	}
	public Long getBd_budzet_ko_id()
	{
		return bd_budzet_ko_id;
	}
	public void setBd_budzet_ko_id(Long bd_budzet_ko_id)
	{
		this.bd_budzet_ko_id = bd_budzet_ko_id;
	}
	public Long getBd_rodzaj_ko_id()
	{
		return bd_rodzaj_ko_id;
	}
	public void setBd_rodzaj_ko_id(Long bd_rodzaj_ko_id)
	{
		this.bd_rodzaj_ko_id = bd_rodzaj_ko_id;
	}
	public Long getZrodlo_id()
	{
		return zrodlo_id;
	}
	public void setZrodlo_id(Long zrodlo_id)
	{
		this.zrodlo_id = zrodlo_id;
	}
	public BigDecimal getKoszt()
	{
		return koszt;
	}
	public void setKoszt(BigDecimal koszt)
	{
		this.koszt = koszt;
	}
	public Long getBd_str_budzet_id()
	{
		return bd_str_budzet_id;
	}
	public void setBd_str_budzet_id(Long bd_str_budzet_id)
	{
		this.bd_str_budzet_id = bd_str_budzet_id;
	}
	@Override
	public String toString()
	{
		return "MpkSimple [bd_rezerwacja_poz_id=" + bd_rezerwacja_poz_id + ", bd_str_budzet_id=" + bd_str_budzet_id + ", bd_rodzaj_ko_id=" + bd_rodzaj_ko_id + ", zrodlo_id=" + zrodlo_id + ", koszt=" + koszt + "]";
	}
	
}
