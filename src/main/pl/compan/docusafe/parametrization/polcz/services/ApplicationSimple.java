package pl.compan.docusafe.parametrization.polcz.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ApplicationSimple
{
	private List<MpkSimple> mpks;
	private Long bd_rezerwacja_id;
	private Long bd_typ_rezerwacja_id;
	private String bd_typ_rezerwacja_ids;
	private Date datautw;
	private Date datzatw;
	private Date datres;
	private Long pracownik_id;
	private String imie;
	private String nazwisko;
	private String utw;
	private String utw_imie;
	private String utw_nazwisko;
	private String zatw;
	private String zatw_imie;
	private String zatw_nazwisko;
	private String bd_rezerwacja_imd;
	private String opis;
	private String nazwa;
	private Integer status;
	
	public List<MpkSimple> getMpks()
	{
		return mpks;
	}
	public void setMpks(List<MpkSimple> mpks)
	{
		this.mpks = mpks;
	}
	public Long getBd_rezerwacja_id()
	{
		return bd_rezerwacja_id;
	}
	public void setBd_rezerwacja_id(Long bd_rezerwacja_id)
	{
		this.bd_rezerwacja_id = bd_rezerwacja_id;
	}
	/**
	 * Data uwtorzenia
	 * @return
	 */
	public Date getDatautw()
	{
		return datautw;
	}
	public void setDatautw(Date datautw)
	{
		this.datautw = datautw;
	}
	public Date getDatzatw()
	{
		return datzatw;
	}
	public void setDatzatw(Date datzatw)
	{
		this.datzatw = datzatw;
	}
	public Date getDatres()
	{
		return datres;
	}
	public void setDatres(Date datres)
	{
		this.datres = datres;
	}
	public Long getPracownik_id()
	{
		return pracownik_id;
	}
	public void setPracownik_id(Long pracownik_id)
	{
		this.pracownik_id = pracownik_id;
	}
	public String getImie()
	{
		return imie;
	}
	public void setImie(String imie)
	{
		this.imie = imie;
	}
	public String getNazwisko()
	{
		return nazwisko;
	}
	public void setNazwisko(String nazwisko)
	{
		this.nazwisko = nazwisko;
	}
	public String getUtw()
	{
		return utw;
	}
	public void setUtw(String utw)
	{
		this.utw = utw;
	}
	public String getUtw_imie()
	{
		return utw_imie;
	}
	public void setUtw_imie(String utw_imie)
	{
		this.utw_imie = utw_imie;
	}
	public String getUtw_nazwisko()
	{
		return utw_nazwisko;
	}
	public void setUtw_nazwisko(String utw_nazwisko)
	{
		this.utw_nazwisko = utw_nazwisko;
	}
	public String getZatw()
	{
		return zatw;
	}
	public void setZatw(String zatw)
	{
		this.zatw = zatw;
	}
	public String getZatw_imie()
	{
		return zatw_imie;
	}
	public void setZatw_imie(String zatw_imie)
	{
		this.zatw_imie = zatw_imie;
	}
	public String getZatw_nazwisko()
	{
		return zatw_nazwisko;
	}
	public void setZatw_nazwisko(String zatw_nazwisko)
	{
		this.zatw_nazwisko = zatw_nazwisko;
	}
	/**
	 * Nr wniosku
	 * @return
	 */
	public String getBd_rezerwacja_imd()
	{
		return bd_rezerwacja_imd;
	}
	public void setBd_rezerwacja_imd(String bd_rezerwacja_imd)
	{
		this.bd_rezerwacja_imd = bd_rezerwacja_imd;
	}
	public String getOpis()
	{
		return opis;
	}
	public void setOpis(String opis)
	{
		this.opis = opis;
	}
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bd_rezerwacja_id == null) ? 0 : bd_rezerwacja_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApplicationSimple other = (ApplicationSimple) obj;
		if (bd_rezerwacja_id == null)
		{
			if (other.bd_rezerwacja_id != null)
				return false;
		}
		else if (!bd_rezerwacja_id.equals(other.bd_rezerwacja_id))
			return false;
		return true;
	}
	public String getBd_typ_rezerwacja_ids()
	{
		return bd_typ_rezerwacja_ids;
	}
	public void setBd_typ_rezerwacja_ids(String bd_typ_rezerwacja_ids)
	{
		this.bd_typ_rezerwacja_ids = bd_typ_rezerwacja_ids;
	}
	public String getNazwa()
	{
		return nazwa;
	}
	public void setNazwa(String nazwa)
	{
		this.nazwa = nazwa;
	}
	public void addMpk(MpkSimple mpkSimple)
	{
		if (mpks == null)
			mpks = new ArrayList<MpkSimple>();
	
		mpks.add(mpkSimple);
	}
	public Long getBd_typ_rezerwacja_id()
	{
		return bd_typ_rezerwacja_id;
	}
	public void setBd_typ_rezerwacja_id(Long bd_typ_rezerwacja_id)
	{
		this.bd_typ_rezerwacja_id = bd_typ_rezerwacja_id;
	}
	@Override
	public String toString()
	{
		return "ApplicationSimple [bd_rezerwacja_id=" + bd_rezerwacja_id + ", bd_typ_rezerwacja_ids=" + bd_typ_rezerwacja_ids + ", datautw=" + datautw + ", bd_rezerwacja_imd=" + bd_rezerwacja_imd + "]";
	}
	public Integer getStatus()
	{
		return status;
	}
	public void setStatus(Integer status)
	{
		this.status = status;
	}

}
