package pl.compan.docusafe.parametrization.polcz;

public class ApplicationWBWLogic extends AbstractApplicationLogic
{
	private static ApplicationWBWLogic instance;
	
	public static ApplicationWBWLogic getInstance()
	{
		if (instance == null)
			instance = new ApplicationWBWLogic();
		return instance;
	}
}