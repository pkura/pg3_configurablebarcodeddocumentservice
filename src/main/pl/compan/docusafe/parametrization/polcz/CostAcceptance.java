package pl.compan.docusafe.parametrization.polcz;

public class CostAcceptance
{

	private String budzet;
	private String pozycja;
	private String zrodlo;
	
	private String kwota;
	
	private String sekcjaFinansowa;
	private String sekcjaFinansowaData;
	private String kierownikJednostki;
	private String kierownikJednostkiData;
	private String kierownikZakladu;
	private String kierownikZakladuData;
	
	public String getBudzet()
	{
		return budzet;
	}
	
	public void setBudzet(String budzet)
	{
		this.budzet = budzet;
	}
	
	public String getPozycja()
	{
		return pozycja;
	}
	
	public void setPozycja(String pozycja)
	{
		this.pozycja = pozycja;
	}
	public String getZrodlo()
	{
		return zrodlo;
	}
	public void setZrodlo(String zrodlo)
	{
		this.zrodlo = zrodlo;
	}
	public String getKwota()
	{
		return kwota;
	}
	public void setKwota(String kwota)
	{
		this.kwota = kwota;
	}
	public String getSekcjaFinansowa()
	{
		return sekcjaFinansowa;
	}
	public void setSekcjaFinansowa(String sekcjaFinansowa)
	{
		this.sekcjaFinansowa = sekcjaFinansowa;
	}
	public String getKierownikJednostki()
	{
		return kierownikJednostki;
	}
	public void setKierownikJednostki(String kierownikJednostki)
	{
		this.kierownikJednostki = kierownikJednostki;
	}
	public String getKierownikZakladu()
	{
		return kierownikZakladu;
	}
	public void setKierownikZakladu(String kierownikZakladu)
	{
		this.kierownikZakladu = kierownikZakladu;
	}

	public String getSekcjaFinansowaData()
	{
		return sekcjaFinansowaData;
	}

	public void setSekcjaFinansowaData(String sekcjaFinansowaData)
	{
		this.sekcjaFinansowaData = sekcjaFinansowaData;
	}

	public String getKierownikJednostkiData()
	{
		return kierownikJednostkiData;
	}

	public void setKierownikJednostkiData(String kierownikJednostkiData)
	{
		this.kierownikJednostkiData = kierownikJednostkiData;
	}

	public String getKierownikZakladuData()
	{
		return kierownikZakladuData;
	}

	public void setKierownikZakladuData(String kierownikZakladuData)
	{
		this.kierownikZakladuData = kierownikZakladuData;
	}
	
	
}
