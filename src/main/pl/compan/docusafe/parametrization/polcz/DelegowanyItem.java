package pl.compan.docusafe.parametrization.polcz;

public class DelegowanyItem {

	String imie, nazwisko, dzial, nrTel, stanowisko;

	public DelegowanyItem() {

	}

	public DelegowanyItem(String imie, String nazwisko, String dzial, String nrTel, String stanowisko) {
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.dzial = dzial;
		this.nrTel = nrTel;
		this.stanowisko = stanowisko;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getDzial() {
		return dzial;
	}

	public void setDzial(String dzial) {
		this.dzial = dzial;
	}

	public String getNrTel() {
		return nrTel;
	}

	public void setNrTel(String nrTel) {
		this.nrTel = nrTel;
	}

	public String getStanowisko() {
		return stanowisko;
	}

	public void setStanowisko(String stanowisko) {
		this.stanowisko = stanowisko;
	}
}
