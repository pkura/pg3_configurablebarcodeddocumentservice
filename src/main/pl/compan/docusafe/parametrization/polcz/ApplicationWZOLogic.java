package pl.compan.docusafe.parametrization.polcz;

public class ApplicationWZOLogic extends AbstractApplicationLogic
{
	private static ApplicationWZOLogic instance;
	
	public static ApplicationWZOLogic getInstance()
	{
		if (instance == null)
			instance = new ApplicationWZOLogic();
		return instance;
	}
	
}
