package pl.compan.docusafe.parametrization.polcz;

import java.util.Map;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;

import com.google.common.collect.Maps;

public class ApplicationWPZLogic extends AbstractApplicationLogic
{
	private static ApplicationWPZLogic instance;
	
	public static ApplicationWPZLogic getInstance()
	{
		if (instance == null)
			instance = new ApplicationWPZLogic();
		return instance;
	}
	
	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		try
		{
			super.setAdditionalTemplateValues(docId, values);
			
			Document doc = Document.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			
			values.put("WNIOSKODAWCA", fm.getEnumItem("WNIOSKODAWCA").getTitle());
			
			values.put("WORKER_DIVISION", fm.getEnumItem("WORKER_DIVISION").getTitle());
			
			values.put("TYP_ZAPOMOGI", fm.getEnumItem("TYP_ZAPOMOGI").getTitle());
			
		}
		catch(Exception e)
		{
			log.warn(e.getMessage());
		}
	
	}
	
}