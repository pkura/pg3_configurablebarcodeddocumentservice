package pl.compan.docusafe.parametrization.polcz;

public class ApplicationWZALogic extends AbstractBudgetApplicationLogic
{
	private static ApplicationWZALogic instance;
	
	public static ApplicationWZALogic getInstance()
	{
		if (instance == null)
			instance = new ApplicationWZALogic();
		return instance;
	}

}
