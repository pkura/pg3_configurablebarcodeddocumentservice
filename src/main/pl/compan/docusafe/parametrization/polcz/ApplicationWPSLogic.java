package pl.compan.docusafe.parametrization.polcz;

import java.util.Map;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;

public class ApplicationWPSLogic extends AbstractApplicationLogic
{
	private static ApplicationWPSLogic instance;
	
	public static ApplicationWPSLogic getInstance()
	{
		if (instance == null)
			instance = new ApplicationWPSLogic();
		return instance;
	}
	

	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		try
		{
			super.setAdditionalTemplateValues(docId, values);
			
			
			Document doc = Document.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			values.put("WNIOSKODAWCA", fm.getEnumItem("WNIOSKODAWCA").getTitle());
			
			values.put("WORKER_DIVISION", fm.getEnumItem("WORKER_DIVISION").getTitle());
			
			putDictionaryValuesWithDateTimeFormat(values, fm, "OSOBY", false);
			
		}
		catch(Exception e)
		{
			log.warn(e.getMessage());	
		}
	
	}
	
}