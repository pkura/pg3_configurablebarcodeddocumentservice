package pl.compan.docusafe.parametrization.polcz;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class ApplicationWDELogic extends AbstractBudgetApplicationLogic
{
	private static ApplicationWDELogic instance;
	
	public static ApplicationWDELogic getInstance()
	{
		if (instance == null)
			instance = new ApplicationWDELogic();
		return instance;
	}

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) 
	{

		pl.compan.docusafe.core.dockinds.dwr.Field msgField = super.validateDwr(values, fm);
		
		StringBuilder msgBuilder = new StringBuilder();

		if (msgField != null)
			msgBuilder.append(msgField.getLabel());
		
		try 
		{
			BigDecimal suma = new BigDecimal(0);
			
			if (values.get("DWR_KOSZTY_PODROZY") != null) {
				
			Map<String, FieldData> kosztyPodrozy = values.get("DWR_KOSZTY_PODROZY").getDictionaryData();
			if (kosztyPodrozy != null) {
				for (String cn : kosztyPodrozy.keySet()){
					if(cn.contains("KOSZT_") && kosztyPodrozy.get(cn).getData() != null)
						suma = suma.add(kosztyPodrozy.get(cn).getMoneyData());
				}
				values.get("DWR_SUMA_KOSZTOW_PODROZY_COST_TO_SUM").setMoneyData(suma);
			}
				
			suma = new BigDecimal(0);
			for (String cn : values.keySet()) {
				if (cn.contains("COST_TO_SUM") && values.get(cn).getData() != null)
					suma = suma.add(values.get(cn).getMoneyData());
			}
			values.get("DWR_SUMA").setMoneyData(suma);
			}
			
			if (values.get("DWR_SUMA") != null && values.get("DWR_SUMA").getData() != null)
				setAmountUsed(fm, suma);
			
			if (values.get("DWR_ZALICZKA") != null && values.get("DWR_POBRANA_ZALICZKA") != null) {
				if (!values.get("DWR_ZALICZKA").getBooleanData()) {
					values.get("DWR_POBRANA_ZALICZKA").setMoneyData(BigDecimal.ZERO);
				} else  if (values.get("DWR_PRZEWIDYWANY_KOSZT").getData() != null) {
					values.get("DWR_POBRANA_ZALICZKA").setMoneyData(values.get("DWR_PRZEWIDYWANY_KOSZT").getMoneyData());
				}

				if (values.get("DWR_SUMA").getData() != null && values.get("DWR_POBRANA_ZALICZKA").getData() != null) {
					suma = values.get("DWR_POBRANA_ZALICZKA").getMoneyData().subtract(values.get("DWR_SUMA").getMoneyData());
//					values.get("DIFF").setMoneyData(suma);
					if (suma.compareTo(BigDecimal.ZERO) == 1) {
						values.get("DWR_DO_ZWROTU").setMoneyData(suma.abs().setScale(2, RoundingMode.HALF_UP));
						values.get("DWR_DO_WYPLATY").setMoneyData(BigDecimal.ZERO);
					} else {
						values.get("DWR_DO_WYPLATY").setMoneyData(suma.abs().setScale(2, RoundingMode.HALF_UP));
						values.get("DWR_DO_ZWROTU").setMoneyData(BigDecimal.ZERO);
					}
				}
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return null;
	}

	protected void setAmountUsed(FieldsManager fm, BigDecimal suma) {
		boolean isOpened = true;
		try
		{
			if (!DSApi.isContextOpen())
			{   
				isOpened = false; 
				DSApi.openAdmin();    
			}
			DSApi.context().begin();
			List<Long> mpkIds = (List<Long>) fm.getKey("MPK");
			CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(mpkIds.get(0));
			mpk.setAmountUsed(suma);
			DSApi.context().commit();

		}catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			if (DSApi.isContextOpen() && !isOpened) 
			{ 
				DSApi._close();    
			}
		}
	}
	
	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException 
	{

		// wystartowanie procesu
		super.onStartProcess(document, event);
		FieldsManager fm = document.getFieldsManager();

		BigDecimal koszt = (BigDecimal) fm.getFieldValues().get("PRZEWIDYWANY_KOSZT");
		List<Long> ids = (List<Long>) fm.getKey("MPK");
		BigDecimal suma = BigDecimal.ZERO;
		
		for (Long id : ids) {
			Map<String, Object> mpk = DwrDictionaryFacade.dictionarieObjects.get("MPK").getValues(id.toString());
			BigDecimal kwota = BigDecimal.valueOf((Double) mpk.get("MPK_AMOUNT"));
			suma = suma.add(kwota);
		}

		if (!koszt.equals(suma.setScale(2))) 
		{
			throw new EdmException("Przewidywany koszt delegacji musi by� r�wny kwocie pozycji bud�etowej.");
		}
		
	}
	
	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn,
			String fieldCnValue) 
	{
		if (acceptationCn.equals("applicant_manager") && AssingUtils.assigneToApplicantManager(doc, assignable, openExecution)) {
			return true;
		}
		
		log.debug("Start special assignee operations for acceptationCn: {}, fieldCnValue: {}", acceptationCn, fieldCnValue);
		
		boolean assigned = super.assignee(doc, assignable, openExecution, acceptationCn, fieldCnValue);
		
		if (!assigned)
		{
			// akceptacja vice kwestora
			if (AssingUtils.DELEGOWANY.equals(acceptationCn))
				assigned = AssingUtils.assignToDelegowany(doc, assignable, openExecution);
		}
		
		return assigned;
	}
	
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		try
		{
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			fm.initialize();
			
			// czas wygenerowania metryki
			values.put("GENERATE_DATE", DateUtils.formatCommonDate(new java.util.Date()).toString());
			
			// generowanie akceptacji
			setAcceptances(docId, values);
			setAcceptancesInRozliczenie(docId, values);
			
			// data dokumentu
			values.put("DOC_DATE", DateUtils.formatCommonDate(doc.getCtime()));
			values.put("TERMIN_WYJAZDU", DateUtils.formatCommonDate((Date) fm.getValue("TERMIN_WYJAZDU")));
			values.put("TERMIN_POWROTU", DateUtils.formatCommonDate((Date) fm.getValue("TERMIN_POWROTU")));
			
			values.put("DELEGOWANY", getDelegowanyItems(fm));
			
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
	}
	
}