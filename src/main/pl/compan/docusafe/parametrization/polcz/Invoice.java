package pl.compan.docusafe.parametrization.polcz;

public class Invoice {
    public String tenderSignature;
    public String contractNo;

    //ID Wys�ane do docusafe z ProPublico
    private Long itemId; 
    private String invoiceNo;
    private Long quantity;
    private String unit;
    private String netAmount;
    private String vatAmount;
    
	@Override
	public String toString() {
		return "Invoice [tenderSignature=" + tenderSignature + ", contractNo=" + contractNo + ", itemId=" + itemId + ", invoiceNo=" + invoiceNo + ", quantity="
				+ quantity + ", unit=" + unit + ", netAmount=" + netAmount + ", vatAmount=" + vatAmount + "]";
	}

	public Invoice() {
		
	}
	
	public Invoice(String tenderSignature, String contractNo, Long itemId, String invoiceNo, Long quantity, String unit, String netAmount, String vatAmount) {
		this.tenderSignature = tenderSignature;
		this.contractNo = contractNo;
		this.itemId = itemId;
		this.invoiceNo = invoiceNo;
		this.quantity = quantity;
		this.unit = unit;
		this.netAmount = netAmount;
		this.vatAmount = vatAmount;
	}

	public String getTenderSignature() {
		return tenderSignature;
	}

	public void setTenderSignature(String tenderSignature) {
		this.tenderSignature = tenderSignature;
	}

	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(String netAmount) {
		this.netAmount = netAmount;
	}

	public String getVatAmount() {
		return vatAmount;
	}

	public void setVatAmount(String vatAmount) {
		this.vatAmount = vatAmount;
	}
    
}
