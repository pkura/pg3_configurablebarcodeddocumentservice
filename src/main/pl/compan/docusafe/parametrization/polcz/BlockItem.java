package pl.compan.docusafe.parametrization.polcz;

public class BlockItem {
	
	private String wniosek;
	private Integer nrBlokady;
	
	public BlockItem(){
		
	}

	public BlockItem(String wniosek, Integer nrBlokady) {
		super();
		this.wniosek = wniosek;
		this.nrBlokady = nrBlokady;
	}

	public String getWniosek() {
		return wniosek;
	}

	public void setWniosek(String wniosek) {
		this.wniosek = wniosek;
	}

	public Integer getNrBlokady() {
		return nrBlokady;
	}

	public void setNrBlokady(Integer nrBlokady) {
		this.nrBlokady = nrBlokady;
	}

	
	
	

}
