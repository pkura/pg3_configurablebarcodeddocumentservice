package pl.compan.docusafe.parametrization.polcz;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

public class ContractItemInvoice extends ContractItem {
	private Long id;
	private Long baseId,documentId;
	
	public ContractItemInvoice () {
		
	}
	
	public ContractItemInvoice (Long documentId, ContractItem baseItem) {
		this.documentId = documentId;
		this.baseId = baseItem.getId();
		setBaseContractItemFields(baseItem);
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getBaseId() {
		return baseId;
	}

	public void setBaseId(Long baseId) {
		this.baseId = baseId;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}
	
	protected static ContractItemInvoice find(Long id) throws EdmException {
		return find(id, null);
	}
	
	protected static ContractItemInvoice find(Long id, Long docId) throws EdmException {
		try
		{
			Criteria criteria = DSApi.context().session().createCriteria(ContractItemInvoice.class);
			if (id != null)
				criteria.add(Restrictions.eq("id", id));
			if (docId != null)
				criteria.add(Restrictions.eq("documentId", docId));

			List<ContractItemInvoice> list = criteria.list();

			if (list.size() > 0)
			{
				return list.get(0);
			}

			return null;
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}

	
}
