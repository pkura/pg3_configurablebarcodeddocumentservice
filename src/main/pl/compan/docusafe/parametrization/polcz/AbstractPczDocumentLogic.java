package pl.compan.docusafe.parametrization.polcz;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.polcz.MpkAccept.AcceptType;
import pl.compan.docusafe.util.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractPczDocumentLogic extends AbstractDocumentLogic {

	private static final String MPK_FIELD_CN = "MPK";
	static final String CENTRUM_ID_TABLE_NAME = "simple_erp_per_docusafe_bd_str_budzet_view";
	static final String MANAGER_OF_DEPARTMENT_TABLE_NAME = "dsg_pcz_mpk_manager_of_department";
	static final String MANAGER_OF_UNIT_TABLE_NAME = "dsg_pcz_mpk_manager_of_unit";
	static final String FINANCIAL_SECTION_TABLE_NAME = "dsg_pcz_mpk_financial_section";
	public static final String FINANCIAL_SECTION_TASK_NAME = "financial_section";
	public static final String MANAGER_OF_DEPARTMENT_TASK_NAME = "manager_of_department";
	public static final String MANAGER_OF_UNIT_TASK_NAME = "manager_of_unit";
	public static final String ROZLICZENIE_FINANCIAL_SECTION_TASK_NAME = "roz_financial_section";
	public static final String ROZLICZENIE_MANAGER_OF_DEPARTMENT_TASK_NAME = "roz_manager_of_department";
	public static final String ROZLICZENIE_MANAGER_OF_UNIT_TASK_NAME = "roz_manager_of_unit";
	public static final String MPK_VARIABLE_NAME = "costCenterId";
	public static final Object PROCESS_NAME = "CostInvoice";
	
	protected Logger log = LoggerFactory.getLogger(AbstractPczDocumentLogic.class);
	final StringManager sm = GlobalPreferences.loadPropertiesFile(AbstractPczDocumentLogic.class.getPackage().getName(), null);
	
	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException 
	{
		String docKindTitle = document.getDocumentKind().getName();
		FieldsManager fm = document.getFieldsManager();
		
		Folder folder = Folder.getRootFolder();
		
		folder = folder.createSubfolderIfNotPresent(docKindTitle);
		
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(document.getCtime()));
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(document.getCtime()));
		
		String docNumber;
		if (fm.getValue("NR_WNIOSEK") != null) {
			docNumber = String.valueOf(fm.getValue("NR_WNIOSEK"));
		} else if (fm.getValue("NR_FAKTURY") != null) {
			docNumber = String.valueOf(fm.getValue("NR_FAKTURY"));
		} else {
			docNumber = "";
		}
		
		document.setTitle(docKindTitle + " " + docNumber);
		document.setDescription(docKindTitle + " " + docNumber);
		((OfficeDocument)document).setSummaryWithoutHistory((docKindTitle + " " + docNumber));
		document.setFolder(folder);
	}

	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values) {
		try
		{
			Document doc = Document.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			fm.initialize();
			//--------------------------
			//DO USUNIECIA
			//w rtfie dajemy $fields.WNIOSKODAWCA.title
			// wnioskodawca
			if (fm.containsField("WNIOSKODAWCA")) {
				values.put("WNIOSKODAWCA", fm.getEnumItem("WNIOSKODAWCA").getTitle());
			}
			// jednostka wnioskuj�cego
			if (fm.containsField("WORKER_DIVISION")) {
				values.put("WORKER_DIVISION", fm.getEnumItem("WORKER_DIVISION").getTitle());
			}
			//------------------------------
			
			// czas wygenerowania metryki
			values.put("GENERATE_DATE", DateUtils.formatCommonDate(new java.util.Date()).toString());
			
			// data dokumentu
			values.put("DOC_DATE", DateUtils.formatCommonDate(doc.getCtime()));
			
			// generowanie akceptacji
			setAcceptances(docId, values);
			
			
			
			
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
	}
	
	@Override
	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
		String documentKindName = document.getDocumentKind().getName();

		perms.add(new PermissionBean(ObjectPermission.READ, "PCZ_" + documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, "PCz - " + documentKindName + " - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "PCZ_" + documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "PCz - " + documentKindName + " zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "PCZ_" + documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "PCz - " + documentKindName + " - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "PCZ_" + documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "PCz - " + documentKindName + " zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "PCZ_" + documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "PCz - " + documentKindName + " - usuwanie"));
		Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
		perms.addAll(documentPermissions);
		this.setUpPermission(document, perms);
	}

	@Override
	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
		boolean assigned = false;

		
		return assigned;
	}	
	
	//		column.dockindBusinessAtr3 = Pozcyja bud\u017cetowa: bud\u017cet
	//		column.dockindBusinessAtr4 = Pozcyja bud\u017cetowa: kwota
	@Override
	public TaskListParams getTaskListParams(DocumentKind kind, long documentId, TaskSnapshot task) throws EdmException {
		TaskListParams params = new TaskListParams();
		
		List<String> mpkAcceptationNames = new ArrayList<String>();
		mpkAcceptationNames.add(AssingUtils.MANAGER_OF_DEPARTMENT);
		mpkAcceptationNames.add(AssingUtils.MANAGER_OF_UNIT);
		mpkAcceptationNames.add(AssingUtils.FINANCIAL_SECTION);
		String taskId = task.getActivityKey();
		
		if (!mpkAcceptationNames.contains(Jbpm4Provider.getInstance().getTaskService().getTask(taskId).getName())) {
			params.setAttribute("Nie dotyczy", 2);
			params.setAttribute("Nie dotyczy", 3);
			
			FieldsManager fm = kind.getFieldsManager(documentId);
			
			if (fm != null && fm.getKey(MPK_FIELD_CN) != null) {
				Object mpkRaw = fm.getKey(MPK_FIELD_CN);
				if (mpkRaw != null && mpkRaw instanceof List<?> && ((List<?>)mpkRaw).size()>0) {
					try {
						String mpkRawId = ((List<?>)mpkRaw).get(0).toString();
						Long firstMpkId = Long.valueOf(mpkRawId);

						CentrumKosztowDlaFaktury mpkInstance = CentrumKosztowDlaFaktury.getInstance();
						CentrumKosztowDlaFaktury mpk = mpkInstance.find(firstMpkId);
						if (mpk != null && mpk.getCentrumId() != null)
						{
						EnumItem field = DataBaseEnumField.getEnumItemForTable(CENTRUM_ID_TABLE_NAME, mpk.getCentrumId());
						
						String postfix = ((List<?>)mpkRaw).size() > 1 ? ", ..." : "";

						params.setAttribute(field.getTitle()+postfix, 2);
						params.setAttribute(mpk.getRealAmount().toString()+postfix, 3);
						}
					} catch (NumberFormatException e) {
						log.error(e.toString());
					} catch (EdmException e1) {
						log.error(e1.toString());
					}
				}
			}
		} else {
			String mpkRawId = Jbpm4Provider.getInstance().getTaskService().getVariable(taskId, MPK_VARIABLE_NAME) != null ? 
					Jbpm4Provider.getInstance().getTaskService().getVariable(taskId, MPK_VARIABLE_NAME).toString() : "";
			if (!mpkRawId.equals("")) {
				try {
					Long mpkId = Long.valueOf(mpkRawId);
					CentrumKosztowDlaFaktury mpkInstance = CentrumKosztowDlaFaktury.getInstance();
					CentrumKosztowDlaFaktury mpk = mpkInstance.find(mpkId);

					EnumItem field = DataBaseEnumField.getEnumItemForTable(CENTRUM_ID_TABLE_NAME, mpk.getCentrumId());

					params.setAttribute(field.getTitle(), 2);
					params.setAttribute(mpk.getRealAmount().toString(), 3);

				} catch (EdmException e) {
					log.error(e.toString());
				}
			}
		}
		FieldsManager fm = kind.getFieldsManager(documentId);

		// Status
		params.setStatus((String) fm.getValue("STATUS"));
		
		// Spos�b p�atno�ci
		try
		{
			params.setAttribute(fm.getEnumItem("SPOSOB") != null ? fm.getEnumItem("SPOSOB").getTitle() : "Nie okre�lony", 4);
		}
		catch (Exception e)
		{
			log.error("B��d przy ustawianiu kolumny Spos�b p�atno�ci, listy zada�");
			params.setAttribute("Nie dotyczy", 1);
		}
		return params;
	}

	@Override
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) {
		Map<String, Object> results = new HashMap<String, Object>();
		

		return results;
	}

	public void setAcceptances(long docId, Map<String, Object> values) throws EdmException, UserNotFoundException {
		setAcceptances(docId, values, FINANCIAL_SECTION_TASK_NAME, MANAGER_OF_DEPARTMENT_TASK_NAME, MANAGER_OF_UNIT_TASK_NAME);
	}
	
	public void setAcceptancesInRozliczenie(long docId, Map<String, Object> values) throws EdmException, UserNotFoundException {
		setAcceptances(docId, values, ROZLICZENIE_FINANCIAL_SECTION_TASK_NAME, ROZLICZENIE_MANAGER_OF_DEPARTMENT_TASK_NAME, ROZLICZENIE_MANAGER_OF_UNIT_TASK_NAME);
	}
	
	public void setAcceptances(long docId, Map<String, Object> values, String fsTaskName, String mdTaskName, String muTaskName) throws EdmException, UserNotFoundException
	{
		Map<Long, CostAcceptance> costAcceptancesMap = new HashMap<Long, CostAcceptance>();
		CentrumKosztowDlaFaktury mpkInstance = CentrumKosztowDlaFaktury.getInstance();
		
		for (DocumentAcceptance dacc : DocumentAcceptance.find(docId))
		{
			DSUser acceptedUser = DSUser.findByUsername(dacc.getUsername());
			if (dacc.getObjectId() != null) {
				if (dacc.getAcceptanceCn().equals(fsTaskName)) {
					if (costAcceptancesMap.containsKey(dacc.getObjectId())) {
						costAcceptancesMap.get(dacc.getObjectId()).setSekcjaFinansowa(acceptedUser.asFirstnameLastname());
						costAcceptancesMap.get(dacc.getObjectId()).setSekcjaFinansowaData(DateUtils.formatJsDateTime((dacc.getAcceptanceTime())));
					} else {
						CostAcceptance costAcc = new CostAcceptance();
						setMpkValues(dacc, costAcc, mpkInstance);

						// akceptacja sekcjii finansowej
						costAcc.setSekcjaFinansowa(acceptedUser.asFirstnameLastname());
						costAcc.setSekcjaFinansowaData(DateUtils.formatJsDateTime((dacc.getAcceptanceTime())));

						costAcceptancesMap.put(dacc.getObjectId(), costAcc);
					}
				}
				if (dacc.getAcceptanceCn().equals(mdTaskName)) {
					if (costAcceptancesMap.containsKey(dacc.getObjectId())) {
						costAcceptancesMap.get(dacc.getObjectId()).setKierownikZakladu((acceptedUser.asFirstnameLastname()));
						costAcceptancesMap.get(dacc.getObjectId()).setKierownikZakladuData(DateUtils.formatJsDateTime((dacc.getAcceptanceTime())));
					} else {
						CostAcceptance costAcc = new CostAcceptance();
						setMpkValues(dacc, costAcc, mpkInstance);
						// akceptacja kierownika zak�adu
						costAcc.setKierownikZakladu(acceptedUser.asFirstnameLastname());
						costAcc.setKierownikZakladuData(DateUtils.formatJsDateTime((dacc.getAcceptanceTime())));

						costAcceptancesMap.put(dacc.getObjectId(), costAcc);
					}
				}
				if (dacc.getAcceptanceCn().equals(muTaskName)) {
					if (costAcceptancesMap.containsKey(dacc.getObjectId())) {
						costAcceptancesMap.get(dacc.getObjectId()).setKierownikJednostki(acceptedUser.asFirstnameLastname());
						costAcceptancesMap.get(dacc.getObjectId()).setKierownikJednostkiData(DateUtils.formatJsDateTime((dacc.getAcceptanceTime())));
					} else {
						CostAcceptance costAcc = new CostAcceptance();
						setMpkValues(dacc, costAcc, mpkInstance);
						// akceptacja kierownika jednostki
						costAcc.setKierownikJednostki(acceptedUser.asFirstnameLastname());
						costAcc.setKierownikJednostkiData(DateUtils.formatJsDateTime((dacc.getAcceptanceTime())));

						costAcceptancesMap.put(dacc.getObjectId(), costAcc);
					}
				}
			}
			else
			{
				values.put(dacc.getAcceptanceCn(), acceptedUser.getLastname() + " " + acceptedUser.getFirstname());
				values.put(dacc.getAcceptanceCn() + "_DATE", dacc.getAcceptanceTime());
			}
		}
		if (!costAcceptancesMap.isEmpty() && fsTaskName.startsWith("roz")) {
			values.put("POZYCJE_BUDZETOWE_ROZLICZENIE", costAcceptancesMap.values());
		} else if (!costAcceptancesMap.isEmpty()) {
			values.put("POZYCJE_BUDZETOWE", costAcceptancesMap.values());
		}
	}
	
	public Set<String> loadBudgetRealizationItems(Map<String, FieldData> values, String cn, Set<String> pozycjeId) 
	{
		Map<String,FieldData> mpkValues = DwrUtils.getDictionaryValue(values, "DWR_"+cn);
				
		if (mpkValues != null && mpkValues.size() > 0) {
			
			for (String key : mpkValues.keySet()) {
				if (key.contains(cn+"_ACCOUNTNUMBER_")) {

					EnumValues pozycjeDwr = mpkValues.get(key).getEnumValuesData();
					if (pozycjeDwr.getSelectedId() != null) {
						pozycjeId.add(pozycjeDwr.getSelectedId());
					}
				}
			}
		}
		

		
//				dobry kod, lecz nie dzia�a	
//				zostawiam, bo by� mo�e trzbe b�dzie do tego wr�ci�
//				KK
//
//				StringBuilder ids = new StringBuilder();
//				
//				if (mpkValues != null && mpkValues.size()>0) {
//					Set<String> pozycjeId = new HashSet<String>();
//					
//					Map<String,BigDecimal> pozycjeKwotaMap = new HashMap<String,BigDecimal>();
//					
//					for (String key : mpkValues.keySet()) {
//						if (key.contains("MPK_ACCOUNTNUMBER_")) {
//							String index = key.replace("MPK_ACCOUNTNUMBER_", "");
//							BigDecimal kwota = mpkValues.get("MPK_AMOUNT_"+index)!=null?mpkValues.get("MPK_AMOUNT_"+index).getMoneyData():null;
//							
//							EnumValues pozycjeDwr = mpkValues.get(key).getEnumValuesData();
//							if (pozycjeDwr.getSelectedId()!=null) {
//								BigDecimal sumaKwot = pozycjeKwotaMap.get(pozycjeDwr.getSelectedId()) != null?pozycjeKwotaMap.get(pozycjeDwr.getSelectedId()).add(kwota):kwota;
//								pozycjeKwotaMap.put(pozycjeDwr.getSelectedId(), sumaKwot);
//							}
//						}
//					}
//					
//					for (String key : realizacjeValues.keySet()) {
//						if (key.contains("REALIZACJA_BUDZETU_ID_") && realizacjeValues.get(key) != null && realizacjeValues.get(key).getStringData() != null) {
//							String index = key.replace("REALIZACJA_BUDZETU_ID_", "");
//							
//							BigDecimal kwota = realizacjeValues.get("REALIZACJA_BUDZETU_WSTEPNA_"+index)!=null?realizacjeValues.get("REALIZACJA_BUDZETU_WSTEPNA_"+index).getMoneyData():null;
//							
//							if (kwota != null) {
//								if (pozycjeKwotaMap.get(realizacjeValues.get(key).getStringData()) != null) {
//									realizacjeValues.get("REALIZACJA_BUDZETU_WSTEPNA_"+index).setMoneyData(
//											kwota.add(pozycjeKwotaMap.get(realizacjeValues.get(key).getStringData()))
//											);
//								}
//							}
//						}
//					}
//					
//					for (String pozycja : pozycjeId) {
//						if (ids.length()!=0) {
//							ids.append(",");
//						}
//						ids.append(pozycja);
//					}
//				}
		
		
		
		return pozycjeId;
	}
	
	protected void checkTotalCentrum(String fieldName, BigDecimal totalCentrum, Map<String, ?> fieldValues, StringBuilder msgBuilder)
	{
		if (!((BigDecimal) fieldValues.get(fieldName)).equals(totalCentrum))
		{
			if (msgBuilder.length() == 0)
				msgBuilder.append("B��d: Suma kwot pozycji bud�etowych musi by� rowna kwocie wniosku.");
			else
				msgBuilder.append(";  Suma kwot pozycji bud�etowych musi by� rowna kwocie wniosku.");
		}
	}

	protected BigDecimal sumBudgetItems(Map<String, ?> fieldValues, String budgetName) throws EdmException
	{
		BigDecimal totalCentrum = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
		Map<String, Object> mpkValues = (Map<String, Object>) fieldValues.get(budgetName);
		for (String mpkCn : mpkValues.keySet())
		{
			if (mpkCn.contains("MPK_"))
			{
				Map<String, Object> mpkVal = (Map<String, Object>) mpkValues.get(mpkCn);
				BigDecimal oneAmount = ((FieldData) mpkVal.get("AMOUNT")).getMoneyData();
				if (oneAmount != null)
					totalCentrum = totalCentrum.add(oneAmount);
			}
		}
		return totalCentrum;
	}
	
	protected Integer validateCheckboxField(Map<String, ?> fieldValues, String... fields) {
		boolean isAnyField = false;
		for (String field : fields) {
			if (fieldValues.containsKey(field)) {
				isAnyField = true;
				break;
			}
		}
		if (isAnyField) {
			int checkedBoxCount = 0;
			
			for (String field : fields) {
				if (fieldValues.get(field) != null && (Boolean) fieldValues.get(field) == true) {
					checkedBoxCount++;
				}
			}

			return Integer.valueOf(checkedBoxCount);
		}
		
		return null;
	}

	protected StringBuilder validateDate(Object start, Object finish, String errorMsg)
	{
		StringBuilder msg = new StringBuilder();
		try {
			if (start != null && finish != null) {
				Date startDate = null;
				Date finishDate = null;
				
				if (start instanceof Date) {
					startDate = (Date)start;
				} else if (start instanceof FieldData && ((FieldData)start).getData() instanceof Date) {
					startDate = ((FieldData)start).getDateData();
				}
				
				if (finish instanceof Date) {
					finishDate = (Date)finish;
				} else if (finish instanceof FieldData && ((FieldData)finish).getData() instanceof Date) {
					finishDate = ((FieldData)finish).getDateData();
				}
				
				if (startDate!=null && finishDate!=null && startDate.after(finishDate)) {
					msg.append(errorMsg+"; ");
				}
					
			}
		} catch (Exception e) {
			//TODO wylapany
		}
		return msg;
	}

	protected ArrayList<BudgetItem> getBudgetItems(FieldsManager fm)
	{
		ArrayList<BudgetItem> ret = new ArrayList<BudgetItem>();
		try
		{
			List<Long> budzet = (List<Long>) fm.getKey(MPK_FIELD_CN);
			log.debug("********LISTA=" + budzet);
			if (budzet != null) {
				for (Long idMpk : budzet) {

					CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(idMpk);
					EnumItem centrum = DataBaseEnumField.getEnumItemForTable(CENTRUM_ID_TABLE_NAME, mpk.getCentrumId());
					EnumItem account = DataBaseEnumField.getEnumItemForTable("simple_erp_per_docusafe_bd_pozycje_view",
							Integer.parseInt(mpk.getAccountNumber()));
					EnumItem accept = DataBaseEnumField.getEnumItemForTable("simple_erp_per_docusafe_bd_zrodla_view", mpk.getAcceptingCentrumId());
					BigDecimal kwota = mpk.getRealAmount();
					int idblok = 0;
					String status = "";
					if (mpk.getAnalyticsId() != null)
						idblok = mpk.getAnalyticsId();
					if (mpk.getLocationId() != null) {
						switch (mpk.getLocationId()) {
						case 16060:
							status = "Wst�pna";
							break;
						case 16061:
							status = "Ostateczna";
							break;
						}

					}
					BudgetItem newItem = new BudgetItem(centrum.getTitle(), account.getTitle(), accept.getTitle(), status, kwota, idblok);
					ret.add(newItem);
				}
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return ret;
	}	

	
	private Map<String, String> insertEnumItemIntoTable(Map<String, String> enumValues, AcceptType type, Long centrumId, MpkAccept instance) {
		int l = enumValues.containsKey("") ? 1 : 0;

		if (enumValues.size() > l) {

			Session session = null;
			try {
				session = DSApi.context().session();
				try {
					DSApi.context().begin();
				} catch (Exception e) {
					// log.error(e.toString());
				}

				Map<String, String> enumValuesFinal = new LinkedHashMap<String, String>();

				if (l == 1) {
					enumValuesFinal.put("", enumValues.get(""));
					enumValues.remove("");
				}

				for (String key : enumValues.keySet()) {
					List<MpkAccept> found = instance.find(type, key, centrumId);

					if (found == null) {
						MpkAccept toAdd = null;
						if (type == AcceptType.FINANCIAL_SECTION) {
							toAdd = new MpkFinancialSection();
						} else if (type == AcceptType.MANAGER_OF_DEPARTMENT) {
							toAdd = new MpkManagerOfDepartment();
						} else if (type == AcceptType.MANAGER_OF_UNIT) {
							toAdd = new MpkManagerOfUnit();
						}

						toAdd.setAvailable(true);
						toAdd.setCentrum(0);
						toAdd.setCn(key);
						toAdd.setRefValue(centrumId);
						toAdd.setTitle(enumValues.get(key));
						session.save(toAdd);

						enumValuesFinal.put(toAdd.getId().toString(), toAdd.getTitle());
					} else {
						MpkAccept toShow;
						toShow = found.get(0);
						enumValuesFinal.put(toShow.getId().toString(), toShow.getTitle());
					}
				}

				DSApi.context().commit();
				DSApi.context().begin();

				return enumValuesFinal;
			} catch (Exception e) {
				try {
					DSApi.context().rollback();
				} catch (EdmException e1) {
					log.error(e.getMessage(), e1);
				}
				log.error(e.getMessage(), e);
			}
		}
		return enumValues;
	}

	private void setMpkValues(DocumentAcceptance dacc, CostAcceptance costAcc, CentrumKosztowDlaFaktury mpkInstance) throws EdmException
	{
		CentrumKosztowDlaFaktury mpk = mpkInstance.find(dacc.getObjectId());

		// Budzet
		String budzet = DataBaseEnumField.getEnumItemForTable(CENTRUM_ID_TABLE_NAME,mpk.getCentrumId()).getTitle();
		costAcc.setBudzet(budzet);
		// Pozycja
		String pozycja = DataBaseEnumField.getEnumItemForTable("simple_erp_per_docusafe_bd_pozycje_view",Integer.parseInt(mpk.getAccountNumber())).getTitle();
		costAcc.setPozycja(pozycja);
		// Zrodlo
		String zrodlo = DataBaseEnumField.getEnumItemForTable("simple_erp_per_docusafe_bd_zrodla_view",mpk.getAcceptingCentrumId()).getTitle();
		costAcc.setZrodlo(zrodlo);
		// Kwota
		BigDecimal kwota = mpk.getRealAmount().setScale(2, RoundingMode.HALF_UP);
		costAcc.setKwota(kwota.toString());
	}
	
	private Map<String, String> findIdForAccept(DSDivision[] departments, String acceptanceName, boolean single, Map<String,String> enumValues, boolean financialSection) throws EdmException {
		
		if (enumValues == null) {
			enumValues = new LinkedHashMap<String, String>();
		}
		
		if (!single) {
			enumValues.put("", sm.getString("select.wybierz"));
		}
		
		for (DSDivision div : departments) {
			if (div == null) {
				continue;
			}
			List<AcceptanceCondition> manDeptAcc = AcceptanceCondition.find(acceptanceName, div.getGuid());
			
			int chooserAccSign = 0; //id o warto�ci dodatniej definiuj� id tabeli ds_division, a id o warto�ci ujemnej definiuj� id tabeli ds_user
			
			DSDivision manDept = null;
			DSUser manDeptUser = null;
			
			try {
				if (manDeptAcc != null && manDeptAcc.size() > 0) {
					if (manDeptAcc.get(0).getDivisionGuid() != null && !manDeptAcc.get(0).getDivisionGuid().equals("")) {
						manDept = DSDivision.find(manDeptAcc.get(0).getDivisionGuid());
						chooserAccSign = 1;
					} else if (manDeptAcc.get(0).getUsername() != null && !manDeptAcc.get(0).getUsername().equals("")) {
						manDeptUser = DSUser.findByUsername(manDeptAcc.get(0).getUsername());
						chooserAccSign = -1;
					}
				}
			} catch (EdmException e) {
				log.error("Nieznaleziono ds_division lub ds_user, id");
			}
			
			if (chooserAccSign != 0) {
				if (chooserAccSign > 0) {
					enumValues.put(manDept.getId().toString(),manDept.getName());
				} else {
					enumValues.put("-"+manDeptUser.getId().toString(),manDeptUser.getFirstname()+" "+manDeptUser.getLastname());
				}
			}
			
			if (single && enumValues.size()>1) {
				break;
			}
		}
		
		if (financialSection) {
			if (enumValues.size() == 0) {
				enumValues.put("", "brak");
			} else {
				for (String key : enumValues.keySet()) {
					if (!key.equals("")) {
						String oneKey = key;
						enumValues = new LinkedHashMap<String, String>();
						enumValues.put(oneKey, "znaleziono");
						break;
					}
				}
			}
		}
		
		return enumValues;
	}
		protected void setBudgetIems(Map<String, FieldData> values, FieldsManager fm, Set<String> budgetRealizationId) throws EdmException
		{

			CentrumKosztowDlaFaktury mpkInstance = CentrumKosztowDlaFaktury.getInstance();
			CentrumKosztowDlaFaktury mpk;

			DSApi.openAdmin();

			// utworzenie listy id wybranych blokad z formatki
			List<Long> blokadyIdValues = DwrUtils.extractIdFromDictonaryField(values, "BLOKADY");

			if (blokadyIdValues.size() == 0)
			{
				DSApi.close();
				return;
			}

			// utworzenie listy id wybranych pozycji budzetowych z formatki
			FieldData blokadyItemField = values.get("DWR_MPK");
			List<Long> blokadyMpkIdsValues = DwrUtils.extractIdFromDictonaryField(values, MPK_FIELD_CN);

			// zbi�r id blokad z mpk'�w na formatce
			Set<Long> blokadyFromMpk = new HashSet<Long>();
			for (Long id : blokadyMpkIdsValues)
			{
				mpk = mpkInstance.find(id);
				if (mpk.getVatTypeId() != null && !mpk.getVatTypeId().toString().equals(""))
				{
					blokadyFromMpk.add(Long.valueOf(mpk.getVatTypeId().toString()));
				}
			}

			List<Long> createdMpk = fillMpkTable(blokadyIdValues, blokadyFromMpk, mpkInstance);
			List<Long> mpkForShow = new ArrayList<Long>();

			StringBuilder idList = new StringBuilder();
			for (Long id : createdMpk)
			{
				if (!idList.toString().equals(""))
				{
					idList.append(",");
				}
				idList.append(id.toString());
				mpkForShow.add(id);
			}

			for (Long id : blokadyMpkIdsValues)
			{
				mpk = mpkInstance.find(id);
				if (mpk.getVatTaxTypeId() != null)
				{
					if (blokadyIdValues.contains(Long.valueOf(mpk.getVatTypeId().toString())))
					{
						if (!idList.toString().equals(""))
						{
							idList.append(",");
						}
						idList.append(id.toString());
						mpkForShow.add(id);
					}
				}
			}

			updateMpks(blokadyItemField, idList);
			if (!mpkForShow.containsAll(blokadyMpkIdsValues) || !blokadyMpkIdsValues.containsAll(mpkForShow))
			{
				values.put("DWR_MPK", new FieldData(Field.Type.STRING, idList));

				Set<String> budgetRealizationToShow = new HashSet<String>();
				if (budgetRealizationId != null && budgetRealizationId.size() > 0)
				{
					budgetRealizationToShow.addAll(budgetRealizationId);
				}

				for (Long mpkItem : mpkForShow)
				{
					CentrumKosztowDlaFaktury foundMpk = mpkInstance.find(mpkItem);
					if (foundMpk != null && foundMpk.getAccountNumber() != null)
					{
						budgetRealizationToShow.add(foundMpk.getAccountNumber());
					}
				}
				StringBuilder ids = new StringBuilder();

				for (String pozycja : budgetRealizationToShow)
				{
					if (ids.length() != 0)
					{
						ids.append(",");
					}
					ids.append(pozycja);
				}

				values.put("DWR_REALIZACJA_BUDZETU", new FieldData(Field.Type.STRING, ids.toString()));
			}

			DSApi.close();
		}

		private List<Long> fillMpkTable(List<Long> blokadyIdValues, Set<Long> blokadyFromMpk, CentrumKosztowDlaFaktury mpkInstance) throws EdmException
		{
			List<Long> createdMpk = new ArrayList<Long>();
			List<Long> foundMpk;

			for (Long blokadaId : blokadyIdValues)
			{
				if (blokadyFromMpk.contains(blokadaId))
				{
					continue;
				}

				foundMpk = new ArrayList<Long>();
				Statement stat = null;
				ResultSet result = null;
				try
				{
					stat = DSApi.context().createStatement();
					result = stat.executeQuery("SELECT id FROM cost_invoice_app_blocades_view where DOCUMENT_ID = " + blokadaId);
					while (result.next())
					{
						Long id = result.getLong(1);
						foundMpk.add(id);
					}

					for (Long id : foundMpk)
					{
						CentrumKosztowDlaFaktury source = mpkInstance.find(id);
						CentrumKosztowDlaFaktury newMpk = new CentrumKosztowDlaFaktury();
						newMpk.copyValueFrom(source);
						// id blokady od ktorego pochodzi bazowy mpk
						newMpk.setVatTypeId(Integer.valueOf(blokadaId.toString()));
						// id mpka bazowego
						newMpk.setVatTaxTypeId(source.getId().intValue());
						Persister.create(newMpk);
						createdMpk.add(newMpk.getId());
					}
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
				}
				finally
				{
					DSApi.context().closeStatement(stat);
				}
			}
			return createdMpk;
		}

		private void updateMpks(FieldData zapotrzItemField, StringBuilder idList) throws EdmException
		{
			String[] ids = idList.toString().split(",");
			List<String> idsList = new ArrayList<String>();

			Collections.addAll(idsList, ids);
			Pattern p = Pattern.compile("MPK_ID_\\d+");
			Matcher m = null;
			Map<String, FieldData> dvalues = zapotrzItemField.getDictionaryData();
			for (String o : dvalues.keySet())
			{
				m = p.matcher(o);
				if (m.find() && dvalues.get(o).getData() != null && StringUtils.isNotEmpty(dvalues.get(o).getData().toString()))
				{
					int index = Integer.parseInt(o.split("_ID_")[1]);
					Long mpkId = new Long(dvalues.get(o).getData().toString());
					if (idsList.contains(mpkId.toString()))
					{
						CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(mpkId);
						if (mpk != null)
						{
							updateMpk(dvalues, index, mpk);
						}
					}
				}
			}

		}

    public void addSpecialValueForProcessMailNotifier(FieldsManager fm, Map<String, Object> ctext) throws EdmException
    {
        String nr_wniosek = "NR_WNIOSEK";
        ctext.put("document-number", fm.getValue(nr_wniosek) != null ? (String) fm.getValue(nr_wniosek) : "");
        ctext.put("document-type", fm.getEnumItem("TYP") != null ? fm.getEnumItem("TYP").getTitle() : "");
        String pracownik_odpowiedzialny = "PRACOWNIK_ODPOWIEDZIALNY";
        ctext.put("pracownik-odpowiedzialny", fm.getEnumItem(pracownik_odpowiedzialny) != null ? DSUser.findById(fm.getEnumItem(pracownik_odpowiedzialny).getId().longValue()).asFirstnameLastname() : "");
    }
		private void updateMpk(Map<String, FieldData> dvalues, int index, CentrumKosztowDlaFaktury mpk) throws EdmException
		{
			DSApi.context().begin();
			try
			{
				Object centrumId = dvalues.get("MPK_CENTRUMID_" + index).getData();
				if (centrumId != null && !centrumId.equals(""))
					mpk.setCentrumId(Integer.parseInt(centrumId.toString()));
			}
			catch (Exception e)
			{
				log.warn("LAPIEMY :" + e.getMessage());
			}
			try
			{
				Object amount = dvalues.get("MPK_AMOUNT_" + index).getData();
				if (amount != null)
					mpk.setRealAmount(new BigDecimal(amount.toString()).setScale(2, RoundingMode.HALF_UP));
			}
			catch (Exception e)
			{
				log.warn("LAPIEMY :" + e.getMessage());
			}
			try
			{
				Object accountnumber = dvalues.get("MPK_ACCOUNTNUMBER_" + index).getData();
				if (accountnumber != null && !accountnumber.equals(""))
					mpk.setAccountNumber(accountnumber.toString());
			}
			catch (Exception e)
			{
				log.warn("LAPIEMY :" + e.getMessage());
			}
			try
			{
				Object accepCentrumId = dvalues.get("MPK_ACCEPTINGCENTRUMID_" + index).getData();
				if (accepCentrumId != null && !accepCentrumId.equals(""))
					mpk.setAcceptingCentrumId(Integer.parseInt(accepCentrumId.toString()));
			}
			catch (Exception e)
			{
				log.warn("LAPIEMY :" + e.getMessage());
			}
			try
			{
				Object analyticsId = dvalues.get("MPK_analytics_id_" + index).getData();
				if (analyticsId != null && !analyticsId.equals(""))
					mpk.setAnalyticsId(Integer.parseInt(analyticsId.toString()));
			}
			catch (Exception e)
			{
				log.warn("LAPIEMY :" + e.getMessage());
			}
			try
			{
				Object customysAgencyId = dvalues.get("MPK_CUSTOMSAGENCYID_" + index).getData();
				if (customysAgencyId != null && !customysAgencyId.equals(""))
					mpk.setCustomsAgencyId(Integer.parseInt(customysAgencyId.toString()));
			}
			catch (Exception e)
			{
				log.warn("LAPIEMY :" + e.getMessage());
			}
			try
			{
				Object subgroupId = dvalues.get("MPK_SUBGROUPID_" + index).getData();
				if (subgroupId != null && !subgroupId.equals(""))
					mpk.setSubgroupId(Integer.parseInt(subgroupId.toString()));
			}
			catch (Exception e)
			{
				log.warn("LAPIEMY :" + e.getMessage());
			}
			try
			{
				Object classTypeId = dvalues.get("MPK_CLASS_TYPE_ID_" + index).getData();
				if (classTypeId != null && !classTypeId.equals(""))
					mpk.setClassTypeId(Integer.parseInt(classTypeId.toString()));
			}
			catch (Exception e)
			{
				log.warn("LAPIEMY :" + e.getMessage());
			}
			try
			{
				Object connectedTypeId = dvalues.get("MPK_CONNECTED_TYPE_ID_" + index).getData();
				if (connectedTypeId != null && !connectedTypeId.equals(""))
					mpk.setConnectedTypeId(Integer.parseInt(connectedTypeId.toString()));
			}
			catch (Exception e)
			{
				log.warn("LAPIEMY :" + e.getMessage());
			}

			DSApi.context().commit();
		}
	}
