package pl.compan.docusafe.parametrization.polcz;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

public class MpkAccept {

	public enum AcceptType {
		MANAGER_OF_UNIT, MANAGER_OF_DEPARTMENT, FINANCIAL_SECTION
	}
	
	Long id;
	String cn;
	String title;
	Integer centrum;
	Long refValue;
	Boolean available;

	private static MpkAccept instance;

	public static synchronized MpkAccept getInstance() {
		if (instance == null)
			instance = new MpkAccept();
		return instance;
	}

	public MpkAccept() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getCentrum() {
		return centrum;
	}

	public void setCentrum(Integer centrum) {
		this.centrum = centrum;
	}

	public Long getRefValue() {
		return refValue;
	}

	public void setRefValue(Long refValue) {
		this.refValue = refValue;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}

	public static MpkAccept find(AcceptType type, Long id) throws EdmException {
		try {
			Class hibClass = null;
			if (type != null) {
				if (type == AcceptType.FINANCIAL_SECTION) {
					hibClass = MpkFinancialSection.class;
				} else if (type == AcceptType.MANAGER_OF_DEPARTMENT) {
					hibClass = MpkManagerOfDepartment.class;
				} else if (type == AcceptType.MANAGER_OF_UNIT) {
					hibClass = MpkManagerOfUnit.class;
				}
			}
			if (hibClass == null) {
				return null;
			}
			
			Criteria criteria = DSApi.context().session().createCriteria(hibClass);
			if (id == null)
				return null;
			
			criteria.add(Restrictions.eq("id", id));
			
			List<MpkAccept> list = criteria.list();
			
			if (list.size() > 0) {
				return list.get(0);
			}
			
			return null;
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
	
	public List<MpkAccept> find(AcceptType type, String cn, Long refValue) throws EdmException {
		try {
			Class hibClass = null;
			if (type != null) {
				if (type == AcceptType.FINANCIAL_SECTION) {
					hibClass = MpkFinancialSection.class;
				} else if (type == AcceptType.MANAGER_OF_DEPARTMENT) {
					hibClass = MpkManagerOfDepartment.class;
				} else if (type == AcceptType.MANAGER_OF_UNIT) {
					hibClass = MpkManagerOfUnit.class;
				}
			}
			if (hibClass == null) {
				return null;
			}

			Criteria criteria = DSApi.context().session().createCriteria(hibClass);
			if (cn == null && refValue == null)
				return null;

			criteria.add(Restrictions.eq("cn", cn));
			criteria.add(Restrictions.eq("refValue", refValue));

			List<MpkAccept> list = criteria.list();

			if (list.size() > 0) {
				return list;
			}

			return null;
		} catch (HibernateException e) {
			throw new EdmHibernateException(e);
		}
	}
}
