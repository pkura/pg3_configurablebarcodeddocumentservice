package pl.compan.docusafe.parametrization.polcz.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class CoordinatorManagerUnit  implements DecisionHandler
{

	private static final Logger LOG = LoggerFactory.getLogger(CoordinatorManagerUnit.class);
	
	@Override
	public String decide(OpenExecution openExecution)
	{
		try
		{
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			
			// WNIOSKODAWCA, WORKER_DIVISION
			String wnioskodawca = fm.getEnumItemCn("WNIOSKODAWCA");
			String koordynator = fm.getEnumItemCn("KOORDYNATOR");
			
			if (wnioskodawca.equals(koordynator))
				return "manager_of_unit";
			else
				return "coordinator";
		}
		catch (EdmException ex)
		{
			LOG.error(ex.getMessage(), ex);
			return "error";
		}
	}

}
