package pl.compan.docusafe.parametrization.polcz.jbpm;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.jfree.util.Log;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.process.InstantiationParameters;
import pl.compan.docusafe.core.jbpm4.EnumCnDecisionYesOrNoHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessInstance;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLogicSupport;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class EndWZPProcess implements ExternalActivityBehaviour {
	
	private final static Logger LOG = LoggerFactory.getLogger(EndWZPProcess.class);

	@Override
	public void execute(ActivityExecution execution) throws Exception {
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Document doc = Document.find(docId); //faktura
		
//		String decyzja = (String) doc.getFieldsManager().getValue("BLOKADY");
		LOG.error("slimitest2: " + doc.getFieldsManager().getValue("BLOKADY"));
		Map<String, Object> mapa = (Map<String, Object>) ((LinkedList)doc.getFieldsManager().getValue("BLOKADY")).get(0);
		String temp  = (String) mapa.get("BLOKADY_NR_WNIOSEK");
		Long wzpId = Long.parseLong(temp.substring(0, temp.indexOf("/")));
		Document docWzp = Document.find(wzpId);
//		docWzp.get
		Iterable<String> taskIds = Jbpm4ProcessLocator.taskIds(wzpId);
		LOG.error("slimitest2: " + taskIds);
		for (String taskId : taskIds) {
			LOG.error("slimitest2taskId: {}", taskId);
			String procInstance = Jbpm4Provider.getInstance().getExecutionService()
					.findExecutionById(Jbpm4Provider.getInstance().getTaskService().getTask(taskId).getExecutionId())
					.getProcessInstance().getId();
			LOG.error("slimitest3 getState(): " + Jbpm4Provider.getInstance().getExecutionService()
					.findExecutionById(Jbpm4Provider.getInstance().getTaskService().getTask(taskId).getExecutionId())
					.getProcessInstance().getState());
			
			LOG.error("slimitest3: getExecutions()" + Jbpm4Provider.getInstance().getExecutionService()
					.findExecutionById(Jbpm4Provider.getInstance().getTaskService().getTask(taskId).getExecutionId())
					.getProcessInstance().getExecutions());
			
			LOG.error("slimitest3: getKey()" + Jbpm4Provider.getInstance().getExecutionService()
					.findExecutionById(Jbpm4Provider.getInstance().getTaskService().getTask(taskId).getExecutionId())
					.getProcessInstance().getKey());
			
			LOG.error("slimitest3: getName()" + Jbpm4Provider.getInstance().getExecutionService()
					.findExecutionById(Jbpm4Provider.getInstance().getTaskService().getTask(taskId).getExecutionId())
					.getProcessInstance().getName());
			
			LOG.error("slimitest3: getExecutionsMap()" + Jbpm4Provider.getInstance().getExecutionService()
					.findExecutionById(Jbpm4Provider.getInstance().getTaskService().getTask(taskId).getExecutionId())
					.getProcessInstance().getExecutionsMap());
			LOG.error("slimitest2procInstance: {}", procInstance);
			if (procInstance.startsWith("ApplicationWZP")) {
				
				Jbpm4Provider.getInstance().getExecutionService().signalExecutionById(procInstance, "to author_and_financial_section_message");
				JBPMTaskSnapshot.updateByDocumentId(docId, "anyType");
			}
		}
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
		// TODO Auto-generated method stub

	}
}
