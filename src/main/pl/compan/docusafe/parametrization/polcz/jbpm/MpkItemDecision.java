package pl.compan.docusafe.parametrization.polcz.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.parametrization.polcz.AbstractPczDocumentLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class MpkItemDecision implements DecisionHandler{

	private final static Logger LOG = LoggerFactory.getLogger(MpkItemDecision.class);
	
	private String startPath;

	public String decide(OpenExecution openExecution) 
	{
		try
		{
			if (startPath == null) {
				startPath = "before_financial_section";
			}
			
			String correctTaskStateName = (String) openExecution.getVariable("cState");
			
			if (correctTaskStateName != null && !correctTaskStateName.equals("")) {
				return correctTaskStateName;
			}
			
			Long mpkId = (Long) openExecution.getVariable(AbstractPczDocumentLogic.MPK_VARIABLE_NAME);
			
			CentrumKosztowDlaFaktury mpkInstance = CentrumKosztowDlaFaktury.getInstance();
			CentrumKosztowDlaFaktury mpk = mpkInstance.find(mpkId);

			if (mpk == null) {
				LOG.error("Nie znaleziono mpk dla id: " + mpkId);
				return "error";
			}
			
			if (startPath.equals("before_financial_section") && mpk.getCustomsAgencyId() != null) {
				return AbstractPczDocumentLogic.FINANCIAL_SECTION_TASK_NAME;
			} else if ((startPath.equals("before_manager_of_department") || startPath.equals("before_financial_section")) && mpk.getSubgroupId() != null) {
				return AbstractPczDocumentLogic.MANAGER_OF_DEPARTMENT_TASK_NAME;
			} else if ((startPath.equals("before_manager_of_unit") || startPath.equals("before_manager_of_department") || startPath.equals("before_financial_section")) && mpk.getClassTypeId() != null) {
				return AbstractPczDocumentLogic.MANAGER_OF_UNIT_TASK_NAME;
			} else {
				LOG.error("Nie znaleziono �adnego dzia�u do akceptacji dla mpk: " + mpkId);
				return "error";
			}
		} 
		catch (EdmException ex) 
		{
			LOG.error(ex.getMessage(), ex);
			return "error";
		}
	}
}
