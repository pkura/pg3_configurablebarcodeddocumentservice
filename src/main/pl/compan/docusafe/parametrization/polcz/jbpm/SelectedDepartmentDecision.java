package pl.compan.docusafe.parametrization.polcz.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ul.hib.EnumItem;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SelectedDepartmentDecision implements DecisionHandler
{

	private static final Logger log = LoggerFactory.getLogger(SelectedDepartmentDecision.class);
	
	@Override
	public String decide(OpenExecution execution)
	{
		OfficeDocument doc;
		try
		{
			doc = Jbpm4Utils.getDocument(execution);
			FieldsManager fm = doc.getFieldsManager();
			
			String sekcjaDekretacji = fm.getEnumItemCn("SEKCJA_DEKRETACJI");
			String wybranyDzial = fm.getEnumItemCn("WYBRANY_DZIAL");
			
			if (sekcjaDekretacji.equalsIgnoreCase(wybranyDzial))
				return "createApplication";
			
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			return "selected_department";
		}	
		
		return "selected_department";
		
	}
}
