package pl.compan.docusafe.parametrization.polcz.jbpm;

import java.sql.PreparedStatement;

import org.apache.commons.dbutils.DbUtils;
import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SetBrakSrodkowValue extends  AbstractEventListener
{

	private String brak_srodkow;
	private final static Logger LOG = LoggerFactory.getLogger(SetBrakSrodkowValue.class);
	
	@Override
	public void notify(EventListenerExecution execution) throws Exception
	{
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Document doc = Document.find(docId);
		boolean isopen = true;
		if (!DSApi.context().isTransactionOpen())
		{
			isopen = false;
			DSApi.context().begin();
		}
		if (brak_srodkow == null)
			brak_srodkow = "true";
		
			
			setBrakSrodkow(doc, Boolean.parseBoolean(brak_srodkow));
		if (!isopen)
		{
			DSApi.context().commit();
		}
	}

	private void setBrakSrodkow(Document doc, boolean brak_srodkow) 
	{
		PreparedStatement ps = null;
		try
		{
			String sql = "update " + doc.getDocumentKind().getTablename() +" set brak_srodkow = ? where document_id = ?";
			LOG.debug("sql: {}, brak_srodkow: {}, document_id: {}", sql, brak_srodkow, doc.getId());
			ps = DSApi.context().prepareStatement(sql);
			ps.setBoolean(1, brak_srodkow);
			ps.setLong(2, doc.getId());
			ps.executeUpdate();
			ps.close();
		}
		catch (Exception e)
		{
			LOG.error("", e);
		}
		finally
		{
			DbUtils.closeQuietly(ps);
		}
	}
}
