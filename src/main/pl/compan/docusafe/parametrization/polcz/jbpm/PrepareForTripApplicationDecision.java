package pl.compan.docusafe.parametrization.polcz.jbpm;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.parametrization.polcz.MpkAccept;
import pl.compan.docusafe.parametrization.polcz.MpkAccept.AcceptType;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class PrepareForTripApplicationDecision implements DecisionHandler {

	private static final String MANAGER_FLAG_IN_DIVISION_NAME = "kierownik";
	public static final String MANAGER_GUID_JBPM_VARIABLE = "managerGuid";
	private final static Logger LOG = LoggerFactory.getLogger(PrepareForTripApplicationDecision.class);

	public String decide(OpenExecution execution) {
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		
		boolean isOpened = true;
		
		try {
			OfficeDocument doc = OfficeDocument.find(docId);

			List<Long> dictionaryIds = (List<Long>) doc.getFieldsManager().getKey("MPK");
			Object wnioskodawcaRaw = doc.getFieldsManager().getKey("DELEGOWANY");
			
			Long delegowanyId = null;
			
			if (wnioskodawcaRaw instanceof String) {
				delegowanyId = Long.valueOf((String)wnioskodawcaRaw);
			} else if (wnioskodawcaRaw instanceof Long) {
				delegowanyId = (Long)wnioskodawcaRaw;
			} else if (wnioskodawcaRaw instanceof List<?> && ((List<?>)wnioskodawcaRaw).size()>0) {
				String delegowanyString = ((List<?>)wnioskodawcaRaw).get(0).toString();
				try {
					delegowanyId = Long.valueOf(delegowanyString);
				} catch (NumberFormatException e) {
					LOG.error(e.toString());
					return "error";
				}
			} else {
				LOG.error("Nie mo�na ustali� delegowanego");
				return "error";
			}

			if (!DSApi.isContextOpen())
			{
				isOpened = false;
				DSApi.openAdmin();
			}
			
			NativeCriteria nc = DSApi.context().createNativeCriteria("user_position_division", "t");
			nc.setProjection(NativeExps.projection()
					.addProjection("t.USERNAME")
					.addProjection("t.DIVISION_GUID"))
				.add(NativeExps.eq("t.id", delegowanyId))
				.setLimit(1);
			CriteriaResult cr = nc.criteriaResult();
			//String userName = null;
			String divGuid = null;
			while (cr.next()) {
				//userName = cr.getString(0, null);
				divGuid = cr.getString(1, null);
			}
			
			if (divGuid == null || divGuid == null) {
				return "error";
			}
			
			DSDivision userDiv = DSDivision.find(divGuid);
			CentrumKosztowDlaFaktury mpkInstance = CentrumKosztowDlaFaktury.getInstance();
			
			for (Long mpkId : dictionaryIds) {
				DSDivision div = null;
				
				CentrumKosztowDlaFaktury mpk = mpkInstance.find(mpkId);
				
				if (mpk.getClassTypeId() != null) {
					Long mdId = mpk.getClassTypeId().longValue();
					Integer divOrUserId = Integer.valueOf(MpkAccept.find(AcceptType.MANAGER_OF_UNIT, mdId).getCn());
					
					if (divOrUserId > 0) {
						div = DSDivision.findById(divOrUserId)!=null?DSDivision.findById(divOrUserId).getParent():null;
					} else {
						// TODO brak obs�u�onych id od user'�w
					}
				}

				if (div != null) {
					try {
						if (div.isInAnyChildDivision(userDiv)) {
							return "normal_accept";
						}
					} catch (EdmException e) {
						LOG.error(e.toString());
					}
				}
			}

			//szukamy kierownika dla danego dzia�u
			do {
				int countPositions=0; 
				for (DSDivision potentialManager : userDiv.getChildren()) {
					if (potentialManager.isPosition()) {
						countPositions++;
					}
				}
				for (DSDivision potentialManager : userDiv.getChildren()) {
					if (potentialManager.isPosition() && (countPositions==1 || (countPositions>1 && StringUtils.contains(StringUtils.lowerCase(potentialManager.getName()), MANAGER_FLAG_IN_DIVISION_NAME)))) {
						execution.setVariable(MANAGER_GUID_JBPM_VARIABLE, potentialManager.getGuid());
						// wez kierownika do przypisania
						return "alternative_accept";
					}
				}
			} while ((userDiv = userDiv.getParent()) != null);
			
			return "error";
			
		} catch (EdmException e) {
			LOG.error(e.toString());
			return "error";
		} 	finally	{
			if (DSApi.isContextOpen() && !isOpened)
				try {
					DSApi.close();
				} catch (EdmException e) {
					LOG.error(e.toString());
				}
		}
	}

}
