package pl.compan.docusafe.parametrization.polcz.jbpm;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Date;
import org.apache.commons.dbcp.BasicDataSource;
import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class UtworzPrzelew extends  AbstractEventListener
{
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(UtworzPrzelew.class);

	public void notify(EventListenerExecution execution) throws Exception
	{

		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		
		Connection con = null;
		CallableStatement call = null;
		try
		{
			String erpUser, erpPassword, erpUrl, erpDataBase, erpSchema, utworz_przelew;
			erpUser = Docusafe.getAdditionProperty("erp.database.user");
			erpPassword = Docusafe.getAdditionProperty("erp.database.password");
			erpUrl = Docusafe.getAdditionProperty("erp.database.url");
			erpDataBase = Docusafe.getAdditionProperty("erp.database.dataBase");
			erpSchema = Docusafe.getAdditionProperty("erp.database.schema");
			// up_gsd_tworz_przelew
			utworz_przelew = Docusafe.getAdditionProperty("erp.database.utowrz.przelew");
			
			if (utworz_przelew == null)
				utworz_przelew = "up_gsd_tworz_przelew";
			
			if (erpUser == null || erpPassword == null || erpUrl == null)
				throw new EdmException("Błąd eksportu faktury, brak parametrów połaczenia do bazy ");
			log.info("user: {}, password: {}, url: {}", erpUser, erpPassword, erpUrl);
			BasicDataSource ds = new BasicDataSource();
			ds.setDriverClassName(Docusafe.getDefaultDriver("sqlserver_2000"));
			ds.setUrl(erpUrl);
			ds.setUsername(erpUser);
			ds.setPassword(erpPassword);
			con = ds.getConnection();

			/*
			 * @return_value = [dbo].[up_gsd_tworz_przelew]
 		   @konto_z = 11, wysylam id konta -- wyswietla� konto_idm + ' ' + numerkonta
             @konto_do = N'78878787878787878787', -- nr konta
             @dostawca_id = 5899,
             @kwotaprzel = -- 1332 , kwota przelwu w walicue
             @kwotaprzelwalbaz =  1332 , -- kwota w walcuie bazowej czyli PLN
             @kurs = 1, -- kurs waluty
             @waluta_id = 1, -- id waluty
             @dataprzelewu = N'2012-09-01', - data utowrzenia w systemie
             @data_zaplaty = N'2012-09-01', -- termin platnosci
             @typ_dokplat_id = 5, -- select typ_dokplat_id , typ_dokplat_idn , nazwa_typdokplat from fk_typdokplat
             @komorka_id = 2, -- zawsze 2
             @uzytk_id = 13, -- id uzytkownika
             @opis_pozycji = N'Opis pozycji ', --  ''
             @opis_poz1 = N'Opis pozycji 1', -- tytul, opis przelewu
             @opis_poz2 = N'Opis pozycji 2', -- tytul, opis przelwu
             @opis_poz3 = N'Opis pozycji 3', -- 
             @opis_poz4 = N'Opis pozycji 4',
             @kraj_banku_kontr_id = 1, -- narazie 1
             @kod_swift = N'DFREWSQ', 
             @result = @result OUTPUT
			 */
			call = con.prepareCall("{? = call [" + erpDataBase + "].[" + erpSchema + "].[" + utworz_przelew + "] (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			for (int i = 2; i <= 20; i++)
				call.setObject(i, null);
			
			Integer return_value = null;
			String result = null;
			
			// @return_value
			call.registerOutParameter(1, java.sql.Types.INTEGER);

			// @result
			call.registerOutParameter(21, java.sql.Types.VARCHAR);
			
			FieldsManager fm = doc.getFieldsManager();
			
			// @konto_z = 11, wysylam id konta -- wyswietla� konto_idm + ' ' + numerkonta
			Integer konto_z = fm.getEnumItem("PRZELAC_Z_KONTA").getId();
			call.setInt(2, konto_z);
			
			// @konto_do = N'78878787878787878787', -- nr konta
			String konto_do = fm.getKey("PRZELAC_NA_KONTO").toString();
			call.setString(3, konto_do);
			
			// @dostawca_id = 5899,
			Long dostawca_id = doc.getSender().getWparam();
			call.setLong(4, dostawca_id);
			
			// @kwotaprzel = -- 1332 , kwota przelwu w walicue
			BigDecimal kwotaprzel = null;
			call.setBigDecimal(5, kwotaprzel);
			
			// @kwotaprzelwalbaz =  1332 , -- kwota w walcuie bazowej czyli PLN
			BigDecimal kwotaprzelwabaz = ((BigDecimal)fm.getKey("KWOTA_PLN")).setScale(2, RoundingMode.HALF_UP);
			call.setBigDecimal(6, kwotaprzelwabaz);
			
			// @kurs = 1, -- kurs waluty
			BigDecimal kurs = getKurs(fm);
			call.setBigDecimal(7, kurs);
			
			// @waluta_id = 1, -- id waluty
			Integer waluta_id = getWaluta(fm);
			call.setInt(8, waluta_id);
			
			// @dataprzelewu = N'2012-09-01', - data utowrzenia w systemie
			String dataprzelewu = DateUtils.sqlDateFormat.format(new Date());
			call.setString(9, dataprzelewu);
			
			// @data_zaplaty = N'2012-09-01', -- termin platnosci
			String data_zaplaty = DateUtils.sqlDateFormat.format((Date)fm.getKey("TERMIN_PLATNOSCI"));
			call.setString(10, data_zaplaty);
			
			// @typ_dokplat_id = 5, -- select typ_dokplat_id , typ_dokplat_idn , nazwa_typdokplat from fk_typdokplat
			Integer typ_dokplat_id = fm.getEnumItem("TYP_DOKPLAT").getId();
			call.setInt(11, typ_dokplat_id);   
			
			// @komorka_id = 2, -- zawsze 2
			call.setInt(12, 2); 
			
			// @uzytk_id = 13, -- id uzytkownika -- na stale 4 - ADmisnitrator simple
			//String uzytk_id = DSApi.context().getDSUser().getExtension();
			// call.setInt(13, Integer.parseInt(uzytk_id));
			 call.setInt(13, 4);
			
			// @opis_pozycji = N'Opis pozycji ', --  ''
			call.setString(14, "");
			
			// @opis_poz1 = N'Opis pozycji 1', -- tytul, opis przelewu
			String opis_pozycji_1 = "";
			if (fm.getBoolean("UCZESTNICTWO"))
				opis_pozycji_1 = "Uczestnictwo";
			call.setString(15, opis_pozycji_1);
			
			// @opis_poz2 = N'Opis pozycji 2', -- tytul, opis przelwu
			String opis_pozycji_2 = "";
			if (fm.getBoolean("PUBLIKACJA"))
				opis_pozycji_1 = "Publikacja";
			call.setString(16, opis_pozycji_2);
			
			// @opis_poz3 = N'Opis pozycji 3', -- 
			String opis_pozycji_3 = "";
			if (fm.getKey("NAZWA_KONFERENCJI") != null)
				opis_pozycji_3 = fm.getKey("NAZWA_KONFERENCJI").toString();
			call.setString(17, opis_pozycji_3);
			
			// @opis_poz4 = N'Opis pozycji 4',
			call.setString(18, "");
			
			// @kraj_banku_kontr_id = 1, -- narazie 1
			call.setInt(19, 1);
			
			// @kod_swift = N'DFREWSQ', 
			if (fm.getKey("NR_KONTA_SWIFT") != null)
				call.setString(20, fm.getKey("NR_KONTA_SWIFT").toString());
				
			int resultNum = 0;

			while (true)
			{
				boolean queryResult;
				int rowsAffected;

				if (1 == ++resultNum)
				{
					try
					{
						log.debug(call.toString());
						queryResult = call.execute();

						return_value = call.getInt(1);

						result = call.getString(21);
						
						log.debug("Utworzenie przelewu: return_value: {}, result: {}", return_value, result);
					}
					catch (Exception e)
					{
						// Process the error
						log.error("Result " + resultNum + " is an error: " + e.getMessage());

						// When execute() throws an exception, it may just
						// be that the first statement produced an error.
						// Statements after the first one may have
						// succeeded. Continue processing results until
						// there
						// are no more.
						if (result != null)
							throw new EdmException(result);
						continue;
					}
				}
				else
				{
					try
					{
						queryResult = call.getMoreResults();
					}
					catch (Exception e)
					{
						// Process the error
						log.error("1 != ++resultNum: Result " + resultNum + " is an error: " + e.getMessage());

						// When getMoreResults() throws an exception, it
						// may just be that the current statement produced
						// an error.
						// Statements after that one may have succeeded.
						// Continue processing results until there
						// are no more.
						continue;
					}
				}

				if (queryResult)
				{
					ResultSet rs = call.getResultSet();

					// Process the ResultSet
					log.debug("Result " + resultNum + " is a ResultSet: " + rs);
					ResultSetMetaData md = rs.getMetaData();

					int count = md.getColumnCount();
					log.debug("<table border=1>");
					log.debug("<tr>");
					for (int i = 1; i <= count; i++)
					{
						log.debug("<th>");
						log.debug("GEtColumnLabeL: {}", md.getColumnLabel(i));
					}
					log.debug("</tr>");
					while (rs.next())
					{
						log.debug("<tr>");
						for (int i = 1; i <= count; i++)
						{
							log.debug("<td>");
							log.debug("Get.Object: {}", rs.getObject(i));
						}
						log.debug("</tr>");
					}
					log.debug("</table>");
					rs.close();
				}
				else
				{
					rowsAffected = call.getUpdateCount();

					// No more results
					if (-1 == rowsAffected)
					{
						--resultNum;
						break;
					}

					// Process the update count
					log.debug("Result " + resultNum + " is an update count: " + rowsAffected);
				}
			}

			log.debug("Done processing " + resultNum + " results");
			if (resultNum == 0)
			{

			}

		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException("Błąd akcpetacji wniosku :" + e.getMessage());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					con.close();
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
					throw new EdmException("Błąd akcpetacji wniosku :" + e.getMessage());
				}
			}
		}
		
	}

	private BigDecimal getKurs(FieldsManager fm) throws EdmException
	{
		BigDecimal kurs;
		if (fm.getKey("PUBLIKACJA_KURS") != null)
		{
			kurs = new BigDecimal((Double)fm.getKey("PUBLIKACJA_KURS")).setScale(4, RoundingMode.HALF_UP);
		}
		else
		{
			kurs = new BigDecimal((Double)fm.getKey("UCZESTNICTWO_KURS")).setScale(4, RoundingMode.HALF_UP);
		}
		return kurs;
	}
	
	private Integer getWaluta(FieldsManager fm) throws EdmException
	{
		Integer walutaId = null;
		if (fm.getKey("PUBLIKACJA_WALUTA") != null)
		{
			walutaId = fm.getEnumItem("PUBLIKACJA_WALUTA").getId();
		}
		else
		{
			walutaId = fm.getEnumItem("UCZESTNICTWO_WALUTA").getId();
		}
		return walutaId;
	}
	
}
