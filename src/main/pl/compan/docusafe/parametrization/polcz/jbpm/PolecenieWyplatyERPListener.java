package pl.compan.docusafe.parametrization.polcz.jbpm;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.commons.dbcp.BasicDataSource;
import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class PolecenieWyplatyERPListener extends AbstractEventListener {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(PolecenieWyplatyERPListener.class);
	private static final String wdeCN = "application_wde";
	private static final String wgzCN = "application_wgz";

	public void notify(EventListenerExecution execution) throws Exception {

		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);

		Connection con = null;
		CallableStatement call = null;

		try {
			String erpUser, erpPassword, erpUrl, erpDataBase, erpSchema, wyplataPrzelew, wyplataKasa;
			erpUser = Docusafe.getAdditionProperty("erp.database.user");
			erpPassword = Docusafe.getAdditionProperty("erp.database.password");
			erpUrl = Docusafe.getAdditionProperty("erp.database.url");
			erpDataBase = Docusafe.getAdditionProperty("erp.database.dataBase");
			erpSchema = Docusafe.getAdditionProperty("erp.database.schema");
			// up_gsd_tworz_przelew
			wyplataPrzelew = Docusafe.getAdditionProperty("erp.database.polecenie.wyplaty.przelew");
			wyplataKasa = Docusafe.getAdditionProperty("erp.database.polecenie.wyplaty.kasa");

			if (wyplataPrzelew == null)
				wyplataPrzelew = "up_gsd_tworz_przelew";
			if (wyplataKasa == null)
				wyplataKasa = "";

			if (erpUser == null || erpPassword == null || erpUrl == null)
				throw new EdmException("B��d przekazania polecenia wyp�aty, brak parametrów połaczenia do bazy ");
			log.info("user: {}, password: {}, url: {}", erpUser, erpPassword, erpUrl);
			BasicDataSource ds = new BasicDataSource();
			ds.setDriverClassName(Docusafe.getDefaultDriver("sqlserver_2000"));
			ds.setUrl(erpUrl);
			ds.setUsername(erpUser);
			ds.setPassword(erpPassword);
			con = ds.getConnection();

			FieldsManager fm = doc.getFieldsManager();
			if (wgzCN.equals(doc.getDocumentKind().getCn())) {
				executeWyplataKasa();
			} else if (fm.getEnumItem("FORMA_ZWROTU") != null) {
				if (fm.getEnumItem("FORMA_ZWROTU").getId() == 1301) {
					executeWyplataPrzelew(call, con, erpDataBase, erpSchema, wyplataPrzelew, doc, fm);
				} else
					executeWyplataKasa();
			} else if (fm.getEnumItem("FORMA_ZALICZKI") != null) {
				if (fm.getEnumItem("FORMA_ZALICZKI").getId() == 2502) {
					executeWyplataPrzelew(call, con, erpDataBase, erpSchema, wyplataPrzelew, doc, fm);
				} else
					executeWyplataKasa();
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new EdmException("B��d akcpetacji polecenia wyp�aty :" + e.getMessage());
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					log.error(e.getMessage(), e);
					throw new EdmException("B��d akcpetacji polecenia wyp�aty :" + e.getMessage());
				}
			}
		}
	}

	private void executeWyplataPrzelew(CallableStatement call, Connection con, String erpDataBase, String erpSchema, String wyplataPrzelew,
			OfficeDocument doc, FieldsManager fm) throws SQLException, EdmException {
		/* *
		 * 
		 * @return_value = [dbo].[kp_fk_gen_przelew]
		 * 
		 * @pracownik_nrewid = 792,
		 * 
		 * @symbolwaluty = N'PLN',
		 * 
		 * @adres_kontrah_1 = N'Ades',
		 * 
		 * @adres_kontrah_2 = N'Ades2',
		 * 
		 * @opis_platnosci_1 = N'Opis1',
		 * 
		 * @opis_platnosci_2 = N'Opis2',
		 * 
		 * @opis_platnosci_3 = N'Opis3',
		 * 
		 * @opis_platnosci_4 = N'Opis4',
		 * 
		 * @data_realizacji = N'2012-11-05',
		 * 
		 * @kwota = 1000,
		 * 
		 * @numer_rach_kontr = N'12345678901234567890',
		 * 
		 * @numer_rach_firmy_ids = N'7528',
		 * 
		 * @datautw = N'2012-11-05',
		 * 
		 * @komorka_idn = N'r',
		 * 
		 * @czy_iban = 1,
		 * 
		 * @kurs = 1,
		 * 
		 * @numer_banku_kontr = N'QWERTY',
		 * 
		 * @dane_dod = N'ASDFGHJKL',
		 * 
		 * @uzytk_id = 4
		 */
		call = con.prepareCall("{? = call [" + erpDataBase + "].[" + erpSchema + "].[" + wyplataPrzelew
				+ "] (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

		for (int i = 2; i <= 20; i++)
			call.setObject(i, null);

		Integer return_value = null;

		// @return_value
		call.registerOutParameter(1, java.sql.Types.INTEGER);

		@SuppressWarnings("unchecked")
		String id = ((List<Long>) fm.getKey("DELEGOWANY")).get(0).toString();
		Long userId = Long.valueOf(id.substring(0, (id.length() - 7)));
		DSUser pracownik = DSUser.findById(userId);

		// @pracownik_nrewid int,
		Integer nrEwid = Integer.valueOf(pracownik.getExtension());
		call.setInt(2, nrEwid);

		// @symbolwaluty = N'PLN',
		String symbolWaluty = "PLN";
		call.setString(3, symbolWaluty);

		// @adres_kontrah_1 = N'Ades',
		String adres1 = "";
		call.setString(4, adres1);

		// @adres_kontrah_2 = N'Ades2',
		String adres2 = "";
		call.setString(5, adres2);

		// @opis_platnosci_1 = N'Opis1',
		String opisPlatnosci1 = "";
		call.setString(6, opisPlatnosci1);

		// @opis_platnosci_2 = N'Opis2',
		String opisPlatnosci2 = "";
		call.setString(7, opisPlatnosci2);

		// @opis_platnosci_3 = N'Opis3',
		String opisPlatnosci3 = "";
		call.setString(8, opisPlatnosci3);

		// @opis_platnosci_4 = N'Opis4',
		String opisPlatnosci4 = "";
		call.setString(9, opisPlatnosci4);

		// @data_realizacji = N'2012-11-05',
		java.sql.Date data_realizacji = new java.sql.Date(new Date().getTime());
		call.setDate(10, data_realizacji);

		// @kwota = 1000,
		BigDecimal kwota = BigDecimal.ZERO;
		if (fm.getEnumItem("FORMA_ZWROTU") != null && fm.getEnumItem("FORMA_ZWROTU").getId() == 1301) {
			kwota = ((BigDecimal) fm.getKey("DO_WYPLATY")).setScale(2, RoundingMode.HALF_UP);
		} else if (fm.getValue("PRZEWIDYWANY_KOSZT") != null)
			kwota = ((BigDecimal) fm.getKey("PRZEWIDYWANY_KOSZT")).setScale(2, RoundingMode.HALF_UP);
		call.setBigDecimal(11, kwota);

		String przelewZ = "";
		String przelewNa = "";
		if (fm.getEnumItem("FORMA_ZWROTU") != null && fm.getEnumItem("FORMA_ZWROTU").getId() == 1301
				&& fm.getKey("ROZLICZENIE_PRZELEW_Z") != null && fm.getKey("ROZLICZENIE_PRZELEW_NA") != null) {
			przelewNa = (String) fm.getKey("ROZLICZENIE_PRZELEW_NA");
			przelewZ = fm.getEnumItemCn("ROZLICZENIE_PRZELEW_Z");
		} else if (fm.getKey("PRZELEW_Z") != null && fm.getKey("PRZELEW_NA") != null) {
			przelewNa = (String) fm.getKey("PRZELEW_NA");
			przelewZ = fm.getEnumItemCn("PRZELEW_Z");
		}
		// @numer_rach_kontr = N'12345678901234567890',
		call.setString(12, przelewNa);
		// @numer_rach_firmy_ids = N'7528',
		call.setString(13, przelewZ);

		// @datautw = N'2012-11-05',
		java.sql.Date data_utworzenia = new java.sql.Date(doc.getCtime().getTime());
		call.setDate(14, data_utworzenia);

		// @komorka_idn = N'r',
		String komorkaIDN = "R";
		call.setString(15, komorkaIDN);

		// @czy_iban = 1,
		call.setInt(16, 1);

		// @kurs = 1,
		call.setInt(17, 1);

		// @numer_banku_kontr = N'QWERTY',
		String numerBanku = "";
		call.setString(18, numerBanku);

		// @dane_dod = N'ASDFGHJKL',
		call.setString(19, "");

		// @uzytk_id = 4
		call.setLong(20, 4);

		int resultNum = 0;

		while (true) {
			boolean queryResult;
			int rowsAffected;

			if (1 == ++resultNum) {
				try {
					log.debug(call.toString());
					queryResult = call.execute();

//					return_value = call.getInt(1);

					log.debug("Utworzenie przelewu: return_value: {}", return_value);
				} catch (Exception e) {
					// Process the error
					log.error("Result " + resultNum + " is an error: " + e.getMessage());

					// When execute() throws an exception, it may just
					// be that the first statement produced an error.
					// Statements after the first one may have
					// succeeded. Continue processing results until
					// there
					// are no more.
					continue;
				}
			} else {
				try {
					queryResult = call.getMoreResults();
				} catch (Exception e) {
					// Process the error
					log.error("1 != ++resultNum: Result " + resultNum + " is an error: " + e.getMessage());

					// When getMoreResults() throws an exception, it
					// may just be that the current statement produced
					// an error.
					// Statements after that one may have succeeded.
					// Continue processing results until there
					// are no more.
					continue;
				}
			}

			if (queryResult) {
				log.debug("call.getWarnings(): {} ", call.getWarnings());
				ResultSet rs = call.getResultSet();

				// Process the ResultSet
				log.debug("Result " + resultNum + " is a ResultSet: " + rs);
				ResultSetMetaData md = rs.getMetaData();

				int count = md.getColumnCount();
				log.debug("<table border=1>");
				log.debug("<tr>");
				for (int i = 1; i <= count; i++) {
					log.debug("<th>");
					log.debug("GEtColumnLabeL: {}", md.getColumnLabel(i));
				}
				log.debug("</tr>");
				while (rs.next()) {
					log.debug("<tr>");
					for (int i = 1; i <= count; i++) {
						log.debug("<td>");
						log.debug("Get.Object: {}", rs.getObject(i));
					}
					log.debug("</tr>");
				}
				log.debug("</table>");
				rs.close();
			} else {
				rowsAffected = call.getUpdateCount();

				// No more results
				if (-1 == rowsAffected) {
					--resultNum;
					break;
				}

				// Process the update count
				log.debug("Result " + resultNum + " is an update count: " + rowsAffected);
			}
		}

		log.debug("Done processing " + resultNum + " results");
		if (resultNum == 0) {

		}
	}

	private void executeWyplataKasa() {

	}
}
