package pl.compan.docusafe.parametrization.polcz.jbpm;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp.BasicDataSource;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class AcceptReservation implements ExternalActivityBehaviour
{
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(AcceptReservation.class);

	public void execute(ActivityExecution execution) throws Exception
	{

		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Document doc = Document.find(docId);
		Connection con = null;
		CallableStatement call = null;
		try
		{
			String erpUser, erpPassword, erpUrl, erpDataBase, erpSchema, zaakceptuj;
			erpUser = Docusafe.getAdditionProperty("erp.database.user");
			erpPassword = Docusafe.getAdditionProperty("erp.database.password");
			erpUrl = Docusafe.getAdditionProperty("erp.database.url");
			erpDataBase = Docusafe.getAdditionProperty("erp.database.dataBase");
			erpSchema = Docusafe.getAdditionProperty("erp.database.schema");
			// up_gsd_ksiegapodawcza
			zaakceptuj = Docusafe.getAdditionProperty("erp.database.rezerwacja.zaakceptuj");
			if (erpUser == null || erpPassword == null || erpUrl == null)
				throw new EdmException("Błąd eksportu faktury, brak parametrów połaczenia do bazy ");
			log.info("user: {}, password: {}, url: {}", erpUser, erpPassword, erpUrl);
			BasicDataSource ds = new BasicDataSource();
			ds.setDriverClassName(Docusafe.getDefaultDriver("sqlserver_2000"));
			ds.setUrl(erpUrl);
			ds.setUsername(erpUser);
			ds.setPassword(erpPassword);
			con = ds.getConnection();

			call = con.prepareCall("{call [" + erpDataBase + "].[" + erpSchema + "].[" + zaakceptuj + "] (?)}");
			
			// @bd_rezerwacja_id numeric (10,0)
			Integer bd_rezerwacja_id = (Integer) doc.getFieldsManager().getKey("bd_rezerwacja_id");
			call.setInt(1, bd_rezerwacja_id);
			
			int resultNum = 0;

			while (true)
			{
				boolean queryResult;
				int rowsAffected;

				if (1 == ++resultNum)
				{
					try
					{
						queryResult = call.execute();
					
						log.debug("EXPORT Accepting application_number (bd_rezerwacja_id) : {}", bd_rezerwacja_id);
						
					}
					catch (Exception e)
					{
						// Process the error
						log.error("Result " + resultNum + " is an error: " + e.getMessage());

						// When execute() throws an exception, it may just
						// be that the first statement produced an error.
						// Statements after the first one may have
						// succeeded. Continue processing results until
						// there
						// are no more.
						continue;
					}
				}
				else
				{
					try
					{
						queryResult = call.getMoreResults();
					}
					catch (Exception e)
					{
						// Process the error
						log.error("1 != ++resultNum: Result " + resultNum + " is an error: " + e.getMessage());

						// When getMoreResults() throws an exception, it
						// may just be that the current statement produced
						// an error.
						// Statements after that one may have succeeded.
						// Continue processing results until there
						// are no more.
						continue;
					}
				}

				if (queryResult)
				{
					ResultSet rs = call.getResultSet();

					// Process the ResultSet
					log.debug("Result " + resultNum + " is a ResultSet: " + rs);
					ResultSetMetaData md = rs.getMetaData();

					int count = md.getColumnCount();
					log.debug("<table border=1>");
					log.debug("<tr>");
					for (int i = 1; i <= count; i++)
					{
						log.debug("<th>");
						log.debug("GEtColumnLabeL: {}", md.getColumnLabel(i));
					}
					log.debug("</tr>");
					while (rs.next())
					{
						log.debug("<tr>");
						for (int i = 1; i <= count; i++)
						{
							log.debug("<td>");
							log.debug("Get.Object: {}", rs.getObject(i));
						}
						log.debug("</tr>");
					}
					log.debug("</table>");
					rs.close();
				}
				else
				{
					rowsAffected = call.getUpdateCount();

					// No more results
					if (-1 == rowsAffected)
					{
						--resultNum;
						break;
					}

					// Process the update count
					log.debug("Result " + resultNum + " is an update count: " + rowsAffected);
				}
			}

			log.debug("Done processing " + resultNum + " results");
			if (resultNum == 0)
			{

			}
			

		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException("Błąd akcpetacji wniosku :" + e.getMessage());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					con.close();
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
					throw new EdmException("Błąd akcpetacji wniosku :" + e.getMessage());
				}
			}
		}
		
		boolean isopen = true;
		if (!DSApi.context().isTransactionOpen())
		{
			isopen = false;
			DSApi.context().begin();
		}
		
		FieldsManager fm = doc.getFieldsManager();
		List<Long> mpkIds = null;
		if (fm.getKey("brak_srodkow")  != null && fm.getBoolean("brak_srodkow"))
			mpkIds = (List<Long>)fm.getKey("ADD_MPK");
		else if (fm.getKey("MPK") != null)
			mpkIds = (List<Long>)fm.getKey("MPK");
		
		for (Long mpkId : mpkIds) {
			changeStatus(mpkId);
		}
		
		if (!isopen)
		{
			DSApi.context().commit();
		}
		execution.takeDefaultTransition();
	}


	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	private void changeStatus(Long mpkId) 
	{
		CentrumKosztowDlaFaktury mpk;
		try {
			mpk = CentrumKosztowDlaFaktury.getInstance().find(mpkId);
			mpk.setLocationId(16060);
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}
	}
}
