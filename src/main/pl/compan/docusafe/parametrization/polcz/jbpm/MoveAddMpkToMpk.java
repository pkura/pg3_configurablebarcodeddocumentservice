package pl.compan.docusafe.parametrization.polcz.jbpm;

import java.sql.PreparedStatement;
import java.util.List;
import org.apache.commons.dbutils.DbUtils;
import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.BooleanDecisionHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class MoveAddMpkToMpk extends AbstractEventListener 
{

	private final static Logger LOG = LoggerFactory.getLogger(BooleanDecisionHandler.class);
	@Override
	public void notify(EventListenerExecution arg0) throws Exception
	{
		Long docId = Long.valueOf(arg0.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		
		if (fm.getKey("ADD_MPK") == null)
			return;
		
		List<Long> addMpkIds = (List<Long>) fm.getKey("ADD_MPK");
		
		boolean isopen = true;
		if (!DSApi.context().isTransactionOpen())
		{
			isopen = false;
			DSApi.context().begin();
		}
		
		for (Long addMpkId : addMpkIds)
		{
			moveAddMPkToMpk(doc.getId(), addMpkId);
			
		}

		//removeAddMpkFromWzp(doc.getId());
		if (!isopen)
		{
			DSApi.context().commit();
		}
		
	}
	
	private void removeAddMpkFromWzp(Long id)
	{
		PreparedStatement ps = null;
		try
		{
			String sql = "delete dsg_pcz_application_wzp_multiple_value where document_id = ? and field_cn = 'ADD_MPK'";
			LOG.debug("sql: {}, document_id: {}", sql, id);
			ps = DSApi.context().prepareStatement(sql);
			ps.setLong(1, id);
			ps.executeUpdate();
			ps.close();
		}
		catch (Exception e)
		{
			LOG.error("", e);
		}
		finally
		{
			DbUtils.closeQuietly(ps);
		}
		
	}
	
	private void moveAddMPkToMpk(Long id, Long addMpkId)
	{
		PreparedStatement ps = null;
		try
		{
			String sql = "update dsg_pcz_application_wzp_multiple_value set field_cn = 'MPK' where document_id = ? and field_val = ?";
			LOG.debug("sql: {}, field_value: {}, document_id: {}", sql, addMpkId, id);
			ps = DSApi.context().prepareStatement(sql);
			ps.setString(2, addMpkId.toString());
			ps.setLong(1, id);
			ps.executeUpdate();
			ps.close();
		}
		catch (Exception e)
		{
			LOG.error("", e);
		}
		finally
		{
			DbUtils.closeQuietly(ps);
		}
		
	}
	
}