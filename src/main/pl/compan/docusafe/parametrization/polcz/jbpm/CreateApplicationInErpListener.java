package pl.compan.docusafe.parametrization.polcz.jbpm;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.dbutils.DbUtils;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class CreateApplicationInErpListener implements ExternalActivityBehaviour
{
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(CreateApplicationInErpListener.class);
	private static final List<String> INVOICE_CN = new ArrayList<String>() 
		{{
		    add("pcz_costinvoice");
		}};
		// tabela w erpie z list� wnioskow: bd_typ_rezerwacja
	public static final Map<String, String> WNIOSKI_DOCUSAFE_TO_SIMPLE = new HashMap<String, String>() 
			{{
			    put("wz", "WZAG"); // wyjazd zagraniczny
			    put("wza", "WZAL"); // zaliczka - nie na docusafe, pcz zrezygnowalo z tego, ale zostawiam to tutaj
			    put("wde", "WDEL"); // wyjazd krajowy
			    put("wze", "WZE"); // zamowienie elektroniczne
			    put("wmu", "WZP"); // zamowienie na mala umowe
			    put("wko", "WKON");// wniosek na konferencje
			}};	

	public static final Map<String, String> WNIOSKI_SIMPLE_TO_DOCUSAFE_CN = new HashMap<String, String>() 
			{{
			    put("WZP", "application_wzp"); // Wniosek zgodny z PZP
			    put("WZPD", "application_wzp"); // Wniosek na dostawy og�lne
			    put("WZPK", "application_wzp"); // Wniosek na dostawy komputerowe
			    put("WZPA", "application_wzp"); // Wniosek na dostawy aparatury
			    put("WZPU", "application_wzp"); // Wniosek na us�ugi
			    put("WZPB", "application_wzp"); // Wniosek na roboty budowlane
			    put("WZE", "application_wze"); // Wniosek w proc.zlecenia elektronicznego
			    put("WRK", "application_general"); // Wniosek w procedurze rozeznania cenowego
			    put("WZAG", "application_wz"); // Wniosek na wyjazd zagraniczny
			    put("WDEL", "application_wde"); // Wniosek na wyjazd krajowy
			    put("WKON", "application_wko"); // Wniosek na konferencje
			    put("WUCP", "application_wucp"); // Wniosek na zaw. umowy cywilno-prawnej
			    put("WAH", "application_general"); // Wniosek na zakup ad hoc 
			    put("WZAL", "application_general"); // Wniosek na zaliczk�
			    put("WGZ", "application_wgz"); // Wniosek na wizyt� go�ci zagranicznych
			    put("WNA", "application_general"); // Wniosek inny np. fv proforma
			    put("WPP", "application_general"); // Wniosek o prefinans. projektu unijnego
			    put("WFA", "application_general"); // Wniosek z faktury bez rezerwacji
			}};	
	public void execute(ActivityExecution execution) throws Exception
	{
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Document doc = Document.find(docId);
		
		Connection con = null;
		try
		{
			FieldsManager fm = doc.getFieldsManager();


			List<Long> mpkIds = null;
			if (fm.getKey("brak_srodkow")  != null && fm.getBoolean("brak_srodkow"))
				mpkIds = (List<Long>)fm.getKey("ADD_MPK");
			else if (fm.getKey("MPK") != null)
				mpkIds = (List<Long>)fm.getKey("MPK");
			if (mpkIds.size() == 0)
				return;
			
			String erpUser, erpPassword, erpUrl, erpDataBase, erpSchema, proceRezerwacjaWniosek, proceRezerwacjaPozycjaWniosek;
			erpUser = Docusafe.getAdditionProperty("erp.database.user");
			erpPassword = Docusafe.getAdditionProperty("erp.database.password");
			erpUrl = Docusafe.getAdditionProperty("erp.database.url");
			erpDataBase = Docusafe.getAdditionProperty("erp.database.dataBase");
			erpSchema = Docusafe.getAdditionProperty("erp.database.schema");
			// up_gsd_ksiegapodawcza
			proceRezerwacjaWniosek = Docusafe.getAdditionProperty("erp.database.rezerwacja.wniosek");
			proceRezerwacjaPozycjaWniosek = Docusafe.getAdditionProperty("erp.database.rezerwacja.pozycjawniosek");
			if (erpUser == null || erpPassword == null || erpUrl == null)
				throw new EdmException("Błąd eksportu faktury, brak parametrów połaczenia do bazy ");
			log.info("user: {}, password: {}, url: {}", erpUser, erpPassword, erpUrl);
			BasicDataSource ds = new BasicDataSource();
			ds.setDriverClassName(Docusafe.getDefaultDriver("sqlserver_2000"));
			ds.setUrl(erpUrl);
			ds.setUsername(erpUser);
			ds.setPassword(erpPassword);
			con = ds.getConnection();

			boolean isopen = true;
			if (!DSApi.context().isTransactionOpen())
			{
				isopen = false;
				DSApi.context().begin();
			}
			
			Integer applicationNumber = null;
			// dokument: wniosek, faktura zostal juz alozony w erpie
			if (fm.getKey("bd_rezerwacja_id") != null)
			{
				applicationNumber = (Integer) fm.getKey("bd_rezerwacja_id");
			}
			// wniosek (!doc.getDocumentKind().getCn().equals(INVOICE_CN)= nie zostal zalozony w erpie
			// faktura nie jest powiazana z zadn� blokad� (fm.getKey("BLOKADY"))
			else if (!doc.getDocumentKind().getCn().equals(INVOICE_CN) || fm.getKey("BLOKADY") == null)
			{
				applicationNumber = createOnlyApplication(doc, con, fm, erpDataBase, erpSchema, proceRezerwacjaWniosek, mpkIds);
				setApplicationNumber(doc, applicationNumber);
			}
				
			Integer invoiceApplicationNumber = null; 
			for (Long mpkId : mpkIds)
			{
				// nie zaostala zalozona bloakda na dana pozycja budzetowa
				if (getFromMpk("analytics_id_1", mpkId) == null)
				{
					Integer applicationPositionNumber = null;
					// faktura jest powiazana z wnioskiem(blokadami), ale ta, kontrketnie ta (mpkId) pozycja zostala dodana recznie, czyli nie jest powiazana z zadnyna blokada, 
					// nalezy najpeirw stowrzy wniosek w erpei dla tej faktury, aby poalczyc z ta blokada
					if (applicationNumber == null)
					{
						if (invoiceApplicationNumber == null)
							invoiceApplicationNumber = createOnlyApplication(doc, con, fm, erpDataBase, erpSchema, proceRezerwacjaWniosek, mpkIds);
						applicationPositionNumber = addPositionToApplication(doc, con, fm, erpDataBase, erpSchema, proceRezerwacjaPozycjaWniosek, mpkId, invoiceApplicationNumber);
					}
					else
						applicationPositionNumber = addPositionToApplication(doc, con, fm, erpDataBase, erpSchema, proceRezerwacjaPozycjaWniosek, mpkId, applicationNumber);
					
					setBlocadeNumber(mpkId, applicationPositionNumber);
				}
			}
			if (!isopen)
			{
				DSApi.context().commit();
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException("Błąd eksportu wniosku :" + e.getMessage());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					con.close();
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
					throw new EdmException("Błąd eksportu wniosku :" + e.getMessage());
				}
			}
		}
		execution.takeDefaultTransition();
	}

	private void setBlocadeNumber(Long mpkId, Integer applicationPositionNumber) 
	{
		CentrumKosztowDlaFaktury mpk;
		try {
			mpk = CentrumKosztowDlaFaktury.getInstance().find(mpkId);
			mpk.setAnalyticsId(applicationPositionNumber);
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}
	}

	private void setApplicationNumber(Document doc, Integer applicationNumber) 
	{
		PreparedStatement ps = null;
		try
		{
			String sql = "update " + doc.getDocumentKind().getTablename() +" set bd_rezerwacja_id = ? where document_id = ?";
			log.debug("setApplicationNumber.sql: {}", sql);
			ps = DSApi.context().prepareStatement(sql);
			ps.setInt(1, applicationNumber);
			ps.setLong(2, doc.getId());
			ps.executeUpdate();
			ps.close();
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		finally
		{
			DbUtils.closeQuietly(ps);
		}
		
	}

	private void setNumerWnioskuDlaFaktury(Document doc, String nrWniosku) 
	{
		PreparedStatement ps = null;
		try
		{
			String sql = "update dsg_pcz_costinvoice set nr_wniosku = ? where document_id = ?";
			log.debug("setNumerWnioskuDlaFaktury.sql: {}", sql);
			ps = DSApi.context().prepareStatement(sql);
			ps.setString(1, nrWniosku);
			ps.setLong(2, doc.getId());
			ps.executeUpdate();
			ps.close();
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		finally
		{
			DbUtils.closeQuietly(ps);
		}
		
	}
	/**
	* Procedutra do zakladania wniosku bez pozycji
	[dbo].[up_gsd_rezerwacja_utworz] 
		@budzet_id numeric (10,0),  	
			ten parametr oznacza okres bud�etowy (w domy�le rok), w widoku jest to kolumna okr_bud_id. 
			W bazie danych jest to tabela bd_budzet - tam mo�e by� wiele okres�w bud�etowych dotycz�cych jednego roku. 
			Najlepiej jest to przeczyta� z widoku - po wybraniu bud�etu.
		@bd_typ_rezerwacja_id numeric  (10,0), -- wniosek_rodziaj_id
			 to jest s�ownik wniosk�w o rezerwacje zawarty w naszym systemie w tabeli bd_typ_rezerwacja - pierwsza  kolumna
		@bd_str_budzet_id numeric  (10,0),
		 	kom�rka sk�adaj�ca wniosek o rezerwacj� mo�e by� r�na - s�ownik wszystkich kom�rek bud zerowych znajduje si� w tabeli bd_str_budzet
		@bd_budzet_ko_id numeric  (10,0),
			
		@bd_rezerwacja_idm varchar(20), -- numer_wniosku
			numer wniosku z docusafe
		@nazwa varchar(128) ,
			np. "Wniosek o rezerwacj� �rodk�w na zawarcie umowy cywilno-prawnej (WUCP)"
		@datutw date ,
			Data utorzenia
		@datrez date ,
			Data rezerwacji (data wywolanai procedury)
		@result varchar (max) output , 
		@bd_rezerwacja_id numeric  (10,0) output 
			numer wniosku
	 */
	private Integer createOnlyApplication(Document doc, Connection con, FieldsManager fm, String erpDataBase, String erpSchema, String proceRezerwacjaWniosku, List<Long> mpkIds) throws SQLException, EdmException
	{
		CallableStatement call;
		String linkedServer = Docusafe.getAdditionProperty("erp.database.linkedserver");
		call = con.prepareCall("{call [" + erpDataBase + "].[" + erpSchema + "].[" + proceRezerwacjaWniosku + "] (?,?,?,?,?,?,?,?,?,?)}");
		for (int i = 1; i <= 8; i++)
			call.setObject(i, null);

		call.registerOutParameter(9, java.sql.Types.VARCHAR);
		call.registerOutParameter(10, java.sql.Types.NUMERIC);
		
		// @budzet_id numeric (10,0)
		Integer budzet_id = getFromSimpleErp(linkedServer + "." + erpDataBase+ "." + erpSchema + ".v_bd_budzetowanie_uczelni", "okr_bud_id", "kom_bud_id", getFromMpk("CENTRUMID", mpkIds.get(0)));
		call.setInt(1, budzet_id);
		
		// @bd_typ_rezerwacja_id numeric  (10,0)
		String applicationType = null;
		boolean invoice = false;
		if (INVOICE_CN.contains(doc.getDocumentKind().getCn()))
		{				
			applicationType = "WFA";
			invoice = true;
		}
		else
			applicationType = doc.getDocumentKind().getCn().split("_")[1];
		
		if (WNIOSKI_DOCUSAFE_TO_SIMPLE.containsKey(applicationType))
			applicationType = WNIOSKI_DOCUSAFE_TO_SIMPLE.get(applicationType);
			
		Integer bd_typ_rezerwacja_id = getFromSimpleErp(linkedServer + "." + erpDataBase + "." + erpSchema + ".bd_typ_rezerwacja", "bd_typ_rezerwacja_id", "bd_typ_rezerwacja_ids", applicationType.toUpperCase());
		call.setInt(2, bd_typ_rezerwacja_id);
		
		// @bd_str_budzet_id numeric  (10,0)
		Integer bd_str_budzet_id = (Integer)getFromMpk("CENTRUMID", mpkIds.get(0));
		call.setInt(3, bd_str_budzet_id);
		
		// @bd_budzet_ko_id numeric  (10,0)
		Integer bd_budzet_ko_id = getFromSimpleErp(linkedServer + "." + erpDataBase+ "." + erpSchema + ".v_bd_budzetowanie_uczelni", "bd_budzet_id", "kom_bud_id", getFromMpk("CENTRUMID", mpkIds.get(0)));
		call.setInt(4, bd_budzet_ko_id);
		
		// &bd_rezerwacja_idm varchar(20)
		String bd_rezerwacja_idm = null;
		if (invoice)
		{
			String divisionCn = null;
			DSDivision[] userDivisions = DSApi.context().getDSUser().getDivisionsWithoutGroupPosition();
			if (userDivisions.length == 0)
				divisionCn = DSDivision.find(DSDivision.ROOT_GUID).getCode();
			else
				divisionCn = userDivisions[0].getCode();
			bd_rezerwacja_idm = doc.getId() + "/" + "WFA" + "-" + divisionCn.toUpperCase() + "/" + DateUtils.formatYear(new java.util.Date());
			setNumerWnioskuDlaFaktury(doc, bd_rezerwacja_idm);
			//bd_rezerwacja_idm = (String)fm.getKey("NR_FAKTURY");
		}	
		else
			bd_rezerwacja_idm = (String)fm.getKey("NR_WNIOSEK");
		
		call.setString(5, bd_rezerwacja_idm);
		
		// @nazwa varchar(128)
		String nazwa =  null;
		if (invoice)
		{
			nazwa = "Wniosek Faktura kosztowa";
		}
		else
			nazwa = fm.getField("NAZWA_WNIOSKU").getDefaultValue();
		call.setString(6, nazwa);
		
		// @datutw date 
		Date datutw = null;
		if (invoice)
		{
			datutw = new Date(doc.getCtime().getTime());
		}
		else
			datutw = new Date(((java.util.Date) fm.getKey("DATA_WYPELNIENIA")).getTime());
		call.setDate(7, datutw);
		
		// @datrez date
		Date datrez = new Date(new java.util.Date().getTime());
		call.setDate(8, datrez);
		
		String resultMsg = "";
		call.setString(9, resultMsg);
		
		Integer bd_rezerwacja_id = null;
		int resultNum = 0;

		log.debug("Export application docusafe.id: {}, @budzet_id: {}, @bd_typ_rezerwacja_id: {}, @bd_str_budzet_id: {}, @bd_budzet_ko_id: {}, &bd_rezerwacja_idm: {}, @nazwa: {}, @datutw: {}, @datrez: {}",
								doc.getId(), 	budzet_id, 		bd_typ_rezerwacja_id, 	bd_str_budzet_id,		bd_budzet_ko_id,	bd_rezerwacja_idm, 		nazwa,	datutw, 		datrez);
		
		while (true)
		{
			boolean queryResult;
			int rowsAffected;

			if (1 == ++resultNum)
			{
				try
				{
					queryResult = call.execute();
					log.debug("ResultMsg: {}", resultMsg);
					resultMsg = call.getString(9);
					log.debug("EXPORT Application resultMsg : {}", resultMsg);
					bd_rezerwacja_id = call.getInt(10);
					log.debug("EXPORT Application application_number (bd_rezerwacja_id) : {}", bd_rezerwacja_id);
					if (resultMsg != null && resultMsg.length() > 0)
						throw new EdmException(resultMsg);
				}
				catch (EdmException e)
				{
					log.error(e.getMessage(), e);
					throw e;
				}
				catch (Exception e)
				{
					// Process the error
					log.error("Result " + resultNum + " is an error: " + e.getMessage());

					// When execute() throws an exception, it may just
					// be that the first statement produced an error.
					// Statements after the first one may have
					// succeeded. Continue processing results until
					// there
					// are no more.
					continue;
				}
			}
			else
			{
				try
				{
					queryResult = call.getMoreResults();
				}
				catch (Exception e)
				{
					// Process the error
					log.error("1 != ++resultNum: Result " + resultNum + " is an error: " + e.getMessage());

					// When getMoreResults() throws an exception, it
					// may just be that the current statement produced
					// an error.
					// Statements after that one may have succeeded.
					// Continue processing results until there
					// are no more.
					continue;
				}
			}

			if (queryResult)
			{
				ResultSet rs = call.getResultSet();

				// Process the ResultSet
				log.debug("Result " + resultNum + " is a ResultSet: " + rs);
				ResultSetMetaData md = rs.getMetaData();

				int count = md.getColumnCount();
				log.debug("<table border=1>");
				log.debug("<tr>");
				for (int i = 1; i <= count; i++)
				{
					log.debug("<th>");
					log.debug("GEtColumnLabeL: {}", md.getColumnLabel(i));
				}
				log.debug("</tr>");
				while (rs.next())
				{
					log.debug("<tr>");
					for (int i = 1; i <= count; i++)
					{
						log.debug("<td>");
						log.debug("Get.Object: {}", rs.getObject(i));
					}
					log.debug("</tr>");
				}
				log.debug("</table>");
				rs.close();
			}
			else
			{
				rowsAffected = call.getUpdateCount();

				// No more results
				if (-1 == rowsAffected)
				{
					--resultNum;
					break;
				}

				// Process the update count
				log.debug("Result " + resultNum + " is an update count: " + rowsAffected);
			}
		}

		log.debug("Done processing " + resultNum + " results");
		if (resultNum == 0)
		{

		}
		return bd_rezerwacja_id;
	}

	/**
	 * Procedure do rejestracji poszcegolnych pozycji kosztowych w budzecie
	 * [dbo].[up_gsd_rezerwacja_poz_utworz] 
	@budzet_id numeric (10,0),
	@bd_rezerwacja_id numeric (10,0) ,
	@bd_budzet_ko_id numeric  (10,0) , --buzdet_id
	@bd_str_budzet_id numeric  (10,0),
	@bd_rodzaj_ko_id numeric  (10,0), --pozycja budzetu select bd_rodzaj_ko_id from bd_budzet_ko_rodzaj
	@zrodlo_id numeric  (10,0),
	@wytwor_edit_idm varchar(20),
	@ilosc decimal (10,3),
	@cena money ,
	@daturuch date ,
	@datdost date , 
	@bd_rezerwacjapoz_id numeric (10,0) output , 
	@result varchar (max) output */
	private Integer addPositionToApplication(Document doc, Connection con, FieldsManager fm, String erpDataBase, String erpSchema, String proceRezerwacjaPozycjaWniosek, Long mpkId, Integer applicationNumber) throws SQLException, EdmException
	{
		String linkedServer = Docusafe.getAdditionProperty("erp.database.linkedserver");
		CallableStatement call;
		call = con.prepareCall("{call [" + erpDataBase + "].[" + erpSchema + "].[" + proceRezerwacjaPozycjaWniosek + "] (?,?,?,?,?,?,?,?,?,?,?,?,?)}");
		for (int i = 1; i <= 11; i++)
			call.setObject(i, null);

		call.registerOutParameter(12, java.sql.Types.NUMERIC);
		call.registerOutParameter(13, java.sql.Types.VARCHAR);
		
		// @budzet_id numeric (10,0)
		Integer budzet_id =  getFromSimpleErp(linkedServer + "." + erpDataBase+ "." + erpSchema + ".v_bd_budzetowanie_uczelni", "okr_bud_id", "kom_bud_id", getFromMpk("CENTRUMID", mpkId));
		call.setInt(1, budzet_id);
		
		// @bd_rezerwacja_id numeric  (10,0)
		Integer bd_rezerwacja_id = applicationNumber;
		call.setInt(2, bd_rezerwacja_id);
		
		// @bd_budzet_ko_id numeric  (10,0)
		Integer bd_budzet_ko_id =  getFromSimpleErp(linkedServer + "." + erpDataBase+ "." + erpSchema + ".v_bd_budzetowanie_uczelni", "bd_budzet_id", "kom_bud_id", getFromMpk("CENTRUMID", mpkId));
		call.setInt(3, bd_budzet_ko_id);
		
		// @bd_str_budzet_id numeric  (10,0)
		Integer bd_str_budzet_id = getFromMpk("CENTRUMID", mpkId) != null ? (Integer) getFromMpk("CENTRUMID", mpkId) : null;
		call.setInt(4, bd_str_budzet_id);
		
		// @bd_rodzaj_ko_id numeric  (10,0)
		Integer bd_rodzaj_ko_id = getFromMpk("ACCOUNTNUMBER", mpkId) != null ? Integer.valueOf(((String) getFromMpk("ACCOUNTNUMBER", mpkId))) : null;
		call.setInt(5, bd_rodzaj_ko_id);
		
		// @zrodlo_id numeric  (10,0)
		Integer zrodlo_id = getFromMpk("ACCEPTINGCENTRUMID", mpkId) != null ? (Integer)getFromMpk("ACCEPTINGCENTRUMID", mpkId) : null;
		call.setInt(6, zrodlo_id);
		
		// @wytwor_edit_idm varchar(20)
		String wytwor_edit_idm =  fm.getDocumentKind().getName();
		call.setString(7, null);
		
		// @ilosc decimal (10,3)
		Integer  ilosc = new Integer(1);
		call.setInt(8, ilosc);
		
		// @cena money AMOUNT_1
		BigDecimal cena = getFromMpk("AMOUNT", mpkId) != null ? (BigDecimal) getFromMpk("AMOUNT", mpkId) : null;
		call.setBigDecimal(9, cena);
		
		// @daturuch date
		Date daturuch = new Date(new java.util.Date().getTime());
		call.setDate(10, daturuch);
		
		// @datdost date 
		Date datdost = new Date(new java.util.Date().getTime());
		call.setDate(11, datdost);
		
		String resultMsg = "";
		call.setString(13, resultMsg);
		
		Integer bd_rezerwacjapoz_id = null;
		int resultNum = 0;

		log.debug("Export application position docusafe.id: {}, docusafe.mpk.Id: {},  @budzet_id: {}, @bd_rezerwacja_id: {}, @bd_budzet_ko_id: {}, @bd_str_budzet_id: {}, &bd_rodzaj_ko_id: {}, @zrodlo_id: {}, @wytwor_edit_idm: {}, @ilosc: {}, @cena: {}, @daturuch: {},  @datdost: {}",
										doc.getId(), 		mpkId,			budzet_id, 	bd_rezerwacja_id, 	bd_budzet_ko_id,		bd_str_budzet_id,	bd_rodzaj_ko_id, 		zrodlo_id,	wytwor_edit_idm, ilosc, 	cena, 		daturuch, 	datdost);
		
		while (true)
		{
			boolean queryResult;
			int rowsAffected;

			if (1 == ++resultNum)
			{
				try
				{
					queryResult = call.execute();
					log.debug("ResultMsg: {}", resultMsg);
					resultMsg = call.getString(13);
					log.debug("EXPORT Application position resultMsg : {}", resultMsg);
					bd_rezerwacjapoz_id = call.getInt(12);
					log.debug("EXPORT Application position application_position_number (@bd_rezerwacjapoz_id) : {}", bd_rezerwacjapoz_id);
					if (resultMsg != null && resultMsg.length() > 0)
						throw new EdmException(resultMsg);
				}
				catch (EdmException e)
				{
					log.error(e.getMessage(), e);
					throw e;
				}
				catch (Exception e)
				{
					// Process the error
					log.error("Result " + resultNum + " is an error: " + e.getMessage());

					// When execute() throws an exception, it may just
					// be that the first statement produced an error.
					// Statements after the first one may have
					// succeeded. Continue processing results until
					// there
					// are no more.
					continue;
				}
			}
			else
			{
				try
				{
					queryResult = call.getMoreResults();
				}
				catch (Exception e)
				{
					// Process the error
					log.error("1 != ++resultNum: Result " + resultNum + " is an error: " + e.getMessage());

					// When getMoreResults() throws an exception, it
					// may just be that the current statement produced
					// an error.
					// Statements after that one may have succeeded.
					// Continue processing results until there
					// are no more.
					continue;
				}
			}

			if (queryResult)
			{
				ResultSet rs = call.getResultSet();

				// Process the ResultSet
				log.debug("Result " + resultNum + " is a ResultSet: " + rs);
				ResultSetMetaData md = rs.getMetaData();

				int count = md.getColumnCount();
				log.debug("<table border=1>");
				log.debug("<tr>");
				for (int i = 1; i <= count; i++)
				{
					log.debug("<th>");
					log.debug("GEtColumnLabeL: {}", md.getColumnLabel(i));
				}
				log.debug("</tr>");
				while (rs.next())
				{
					log.debug("<tr>");
					for (int i = 1; i <= count; i++)
					{
						log.debug("<td>");
						log.debug("Get.Object: {}", rs.getObject(i));
					}
					log.debug("</tr>");
				}
				log.debug("</table>");
				rs.close();
			}
			else
			{
				rowsAffected = call.getUpdateCount();

				// No more results
				if (-1 == rowsAffected)
				{
					--resultNum;
					break;
				}

				// Process the update count
				log.debug("Result " + resultNum + " is an update count: " + rowsAffected);
			}
		}

		log.debug("Done processing " + resultNum + " results");
		if (resultNum == 0)
		{

		}
		return bd_rezerwacjapoz_id;
		
	}
	
	//"v_bd_budzetowanie_uczelni", "okr_bud_id", "kom_bud_id" getFromMpk("CENTRUMID", mpkIds.get(0))
	private Integer getFromSimpleErp(String tableName, String columnName, String columnWhere, Object columnWhereValue)
	{
		PreparedStatement ps = null;
		try
		{
			String sql = "select " + columnName + " from " + tableName + " where " + columnWhere + " = ?";
			log.debug("getFromSimpleErp.sql: {} {}", sql, columnWhereValue);
			ps = DSApi.context().prepareStatement(sql);
			ps.setObject(1, columnWhereValue);
			ResultSet rs = ps.executeQuery();
			while (rs.next())
			{
				log.debug("{} = {}", columnName,  rs.getObject(1));
				return rs.getInt(1);
			}
			rs.close();
			ps.close();

		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		DbUtils.closeQuietly(ps);
		
		return null;
	}

	private Object getFromMpk(String fieldCn, Long mpkId)
	{
		try
		{
			CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(mpkId);
			if ("CENTRUMID".equals(fieldCn))
				return mpk.getCentrumId();
			else if ("ACCEPTINGCENTRUMID".equals(fieldCn))
			{
				Integer acceptingCentrumId = mpk.getAcceptingCentrumId();
				if (acceptingCentrumId != null)
				{
					EnumItem enumField = DataBaseEnumField.getEnumItemForTable("simple_erp_per_docusafe_bd_zrodla_view", acceptingCentrumId);
					if (enumField != null)
						acceptingCentrumId = Integer.parseInt(enumField.getCn());
					else
						acceptingCentrumId = null;
				}
				return acceptingCentrumId;
				
			}
			else if ("ACCOUNTNUMBER".equals(fieldCn))
				return mpk.getAccountNumber();
			else if ("AMOUNT".equals(fieldCn))
				return mpk.getRealAmount();
			else if ("analytics_id_1".equals(fieldCn))
				return mpk.getAnalyticsId();
			
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		
		return null;
	}

	public void signal(ActivityExecution execution, String signalName, Map<String, ?> parameters) throws Exception
	{

	}
}