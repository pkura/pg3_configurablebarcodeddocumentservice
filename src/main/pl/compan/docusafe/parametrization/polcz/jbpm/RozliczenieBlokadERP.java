package pl.compan.docusafe.parametrization.polcz.jbpm;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.commons.dbcp.BasicDataSource;
import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class RozliczenieBlokadERP extends AbstractEventListener {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(RozliczenieBlokadERP.class);

	@Override
	public void notify(EventListenerExecution execution) throws Exception {
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);

		Connection con = null;
		CallableStatement call = null;

		try {
			String erpUser, erpPassword, erpUrl, erpDataBase, erpSchema, rozliczenieBlokad;
			erpUser = Docusafe.getAdditionProperty("erp.database.user");
			erpPassword = Docusafe.getAdditionProperty("erp.database.password");
			erpUrl = Docusafe.getAdditionProperty("erp.database.url");
			erpDataBase = Docusafe.getAdditionProperty("erp.database.dataBase");
			erpSchema = Docusafe.getAdditionProperty("erp.database.schema");
			// up_gsd_tworz_przelew
			rozliczenieBlokad = Docusafe.getAdditionProperty("erp.database.rozliczenie.blokad");

			if (rozliczenieBlokad == null)
				rozliczenieBlokad = "up_gsd_rozlicz_blokade";

			if (erpUser == null || erpPassword == null || erpUrl == null)
				throw new EdmException("B��d przekazania polecenia rozliczenia blokad, brak parametrów połaczenia do bazy ");
			log.info("user: {}, password: {}, url: {}", erpUser, erpPassword, erpUrl);
			BasicDataSource ds = new BasicDataSource();
			ds.setDriverClassName(Docusafe.getDefaultDriver("sqlserver_2000"));
			ds.setUrl(erpUrl);
			ds.setUsername(erpUser);
			ds.setPassword(erpPassword);
			con = ds.getConnection();
			FieldsManager fm = doc.getFieldsManager();
			List<Long> mpkIds = null;
			if (fm.getKey("MPK") != null)
				mpkIds = (List<Long>)fm.getKey("MPK");
			if (mpkIds == null || mpkIds.size() == 0)
				return;
			
			executeRozliczenieBlokad(call, con, erpDataBase, erpSchema, rozliczenieBlokad, doc, fm, mpkIds);
			

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new EdmException("B��d rozliczenia blokad:" + e.getMessage());
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					log.error(e.getMessage(), e);
					throw new EdmException("B��d rozliczenia blokad:" + e.getMessage());
				}
			}
		}
	}
	
	private void executeRozliczenieBlokad (CallableStatement call, Connection con, String erpDataBase, String erpSchema, String rozliczenieBlokad,
			OfficeDocument doc, FieldsManager fm, List<Long> mpkIds) throws SQLException, EdmException {
		/* *
		 * 
		 * @return_value = [dbo].[up_gsd_rozlicz_blokade]
		 * 
		 * @bd_rodzaj_ko_id   bigint
		 * 
		 * @dok_poz_koszt    money
		 * 
		 * @bd_rezerwacja_poz_id	 bigint
		 * 
		 * @data_rej 	date 
		 * 
		 * @result 		varchar(max)
		 * 
		 */
		call = con.prepareCall("{? = call [" + erpDataBase + "].[" + erpSchema + "].[" + rozliczenieBlokad
				+ "] (?,?,?,?,?)}");

		for (int i = 2; i <= 5; i++)
			call.setObject(i, null);

		Integer return_value = null;

		// @return_value, @result
		call.registerOutParameter(1, java.sql.Types.INTEGER);
		call.registerOutParameter(6, java.sql.Types.VARCHAR);

		// @bd_rodzaj_ko_id  bigint
		Integer rodzaj_ko_id = 0;
		if ("application_wz".equals(doc.getDocumentKind().getCn())) {
			rodzaj_ko_id = 9;
		} else if ("application_wde".equals(doc.getDocumentKind().getCn())) {
			rodzaj_ko_id = 10;
		}
		call.setInt(2, rodzaj_ko_id);

		// @dok_poz_koszt	money
		BigDecimal poz_koszt = (BigDecimal) getFromMpk("AMOUNTUSED", mpkIds.get(0));
		call.setBigDecimal(3, poz_koszt);

		// @bd_rezerwacja_poz_id	bigint
		Integer rezerwacja_poz_id = (Integer) getFromMpk("analytics_id", mpkIds.get(0));
		call.setInt(4, rezerwacja_poz_id);

		// @data_rej	date
		java.sql.Date data_rej = new java.sql.Date(new Date().getTime());
		call.setDate(5, data_rej);

		int resultNum = 0;
		String result = null;

		while (true)
		{
			boolean queryResult;
			int rowsAffected;

			if (1 == ++resultNum)
			{
				try
				{
					log.debug(call.toString());
					queryResult = call.execute();

					return_value = call.getInt(1);
					result = call.getString(6);
					
					log.debug("Utworzenie przelewu: return_value: {}, result: {}", return_value, result);
				}
				catch (Exception e)
				{
					// Process the error
					log.error("Result " + resultNum + " is an error: " + e.getMessage());

					// When execute() throws an exception, it may just
					// be that the first statement produced an error.
					// Statements after the first one may have
					// succeeded. Continue processing results until
					// there
					// are no more.
					if (result != null)
						throw new EdmException(result);
					continue;
				}
			}
			else
			{
				try
				{
					queryResult = call.getMoreResults();
				}
				catch (Exception e)
				{
					// Process the error
					log.error("1 != ++resultNum: Result " + resultNum + " is an error: " + e.getMessage());

					// When getMoreResults() throws an exception, it
					// may just be that the current statement produced
					// an error.
					// Statements after that one may have succeeded.
					// Continue processing results until there
					// are no more.
					continue;
				}
			}

			if (queryResult)
			{
				ResultSet rs = call.getResultSet();

				// Process the ResultSet
				log.debug("Result " + resultNum + " is a ResultSet: " + rs);
				ResultSetMetaData md = rs.getMetaData();

				int count = md.getColumnCount();
				log.debug("<table border=1>");
				log.debug("<tr>");
				for (int i = 1; i <= count; i++)
				{
					log.debug("<th>");
					log.debug("GEtColumnLabeL: {}", md.getColumnLabel(i));
				}
				log.debug("</tr>");
				while (rs.next())
				{
					log.debug("<tr>");
					for (int i = 1; i <= count; i++)
					{
						log.debug("<td>");
						log.debug("Get.Object: {}", rs.getObject(i));
					}
					log.debug("</tr>");
				}
				log.debug("</table>");
				rs.close();
			}
			else
			{
				rowsAffected = call.getUpdateCount();

				// No more results
				if (-1 == rowsAffected)
				{
					--resultNum;
					break;
				}

				// Process the update count
				log.debug("Result " + resultNum + " is an update count: " + rowsAffected);
			}
		}

		log.debug("Done processing " + resultNum + " results");
		if (resultNum == 0) {

		}
	}
	
	private Object getFromMpk(String fieldCn, Long mpkId)
	{
		try
		{
			CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(mpkId);
			if ("CENTRUMID".equals(fieldCn))
				return mpk.getCentrumId();
			else if ("ACCEPTINGCENTRUMID".equals(fieldCn))
			{
				Integer acceptingCentrumId = mpk.getAcceptingCentrumId();
				if (acceptingCentrumId != null)
				{
					EnumItem enumField = DataBaseEnumField.getEnumItemForTable("simple_erp_per_docusafe_bd_zrodla_view", acceptingCentrumId);
					if (enumField != null)
						acceptingCentrumId = Integer.parseInt(enumField.getCn());
					else
						acceptingCentrumId = null;
				}
				return acceptingCentrumId;
				
			}
			else if ("ACCOUNTNUMBER".equals(fieldCn))
				return mpk.getAccountNumber();
			else if ("AMOUNT".equals(fieldCn))
				return mpk.getRealAmount();
			else if ("analytics_id".equals(fieldCn))
				return mpk.getAnalyticsId();
			else if ("AMOUNTUSED".equals(fieldCn))
				return mpk.getAmountUsed();
			
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		
		return null;
	}
}
