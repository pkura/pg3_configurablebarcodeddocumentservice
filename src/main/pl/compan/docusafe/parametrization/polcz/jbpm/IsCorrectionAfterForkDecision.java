package pl.compan.docusafe.parametrization.polcz.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

public class IsCorrectionAfterForkDecision implements DecisionHandler {

	private static final long serialVersionUID = 1L;

	@Override
	public String decide(OpenExecution openExecution) {
		Integer cor = (Integer) openExecution.getVariable("correction");

		if (cor != null && cor == 11) {
			openExecution.removeVariable("correction");
			openExecution.setVariable("correction", 2);
			openExecution.setVariable("count", 2);
			return "correction";
		} else if (cor != null && cor == 12) {
			openExecution.removeVariable("correction");
			openExecution.removeVariable("count");
			return "reject_author";
		} else if (cor != null && cor == 13) {
			openExecution.removeVariable("correction");
			openExecution.removeVariable("count");
			return "reject_author_and_fs";
		} else if (cor != null && cor == 14) {
			openExecution.removeVariable("correction");
			openExecution.removeVariable("count");
			return "reject_author_and_rkff";
		}else if (cor != null && cor == 15) {
			openExecution.removeVariable("correction");
			openExecution.removeVariable("count");
			return "reject_author_and_chancellor";
		}
		else
		{
			return "normal";
		}
	}

}
