package pl.compan.docusafe.parametrization.polcz;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Application {
	private String applicationNo;
	private Timestamp startDate;
	private String orderSubject;

    // 0 - roboty budowlane
    // 1 - dostawy
    // 2 - us�ugi
	private int orderKind;

    // 10 - Przetarg nieograniczony
    // 11 - Przetarg ograniczony
    // 12 - Negocjacje bez og�oszenia
    // 13 - Negocjacje z og�oszeniem
    // 14 - Zapytanie o cen�
    // 15 - Zapytanie z wolnej r�ki
    // 16 - Poni�ej 14000 euro
    // 17 - Licytacja elektroniczna
    // 18 - Dialog konkurencyjny
	private int tenderProcedure;

	private String amountForRealization;

	private Timestamp setupAmountDate;
	private String baseForAmount;

    // Format: Nazwisko;Imie (oddzielone �rednikiem) np:
    // Mickiewicz;Adam
    // Nowak-Jeziora�ski;Jan
	private String setupAmountPerson;
	private String acceptPerson;

    //Pole koordynator nie jest u�ywane w ProPublico
    //public string Koordynator;

    // ProPublico przyjmuje po prostu wnioskodawc�, 
    // niekoniecznie jest to osoba (zwykle nazwa jednostki
    // zamawiaj�cej)
	private String applicant;

    // Oddzielone �rednikiem �r�d�a finansowania.
    // Propublico mo�e przyj�� dla ka�dego �r�d�a 
    // jego procentowy udzia� w finansowaniu.
	// W przypadku wyst�puj�cego w nazwie �rednika, zast�pujemy go �;;� podw�jnym �rednikiem
    // np.: "Zrodlo 1;20;Zrodlo 2;80" (20 i 80 to procentowy udzia� �r�de�)
	private String fundingSources;

    // Przyk�adowe formaty (oddzielane �rednikami dla wielu kod�w cpv)
    //   32354800-0
    //   73000000-0;32354800-0
    //   32354800-0;E000-0;32354800-0
	private String CPV;

	public Application() {
	}
	
	public Application(String applicationNo, Timestamp startDate, String orderSubject, int orderKind, int tenderProcedure, String amountForRealization,
			Timestamp setupAmountDate, String baseForAmount, String setupAmountPerson, String acceptPerson, String applicant, String fundingSources, String cPV) {
		this.applicationNo = applicationNo;
		this.startDate = startDate;
		this.orderSubject = orderSubject;
		this.orderKind = orderKind;
		this.tenderProcedure = tenderProcedure;
		this.amountForRealization = amountForRealization;
		this.setupAmountDate = setupAmountDate;
		this.baseForAmount = baseForAmount;
		this.setupAmountPerson = setupAmountPerson;
		this.acceptPerson = acceptPerson;
		this.applicant = applicant;
		this.fundingSources = fundingSources;
		CPV = cPV;
	}

	@Override
	public String toString() {
		return "Application [applicationNo=" + applicationNo + ", startDate=" + startDate + ", orderSubject=" + orderSubject + ", orderKind=" + orderKind
				+ ", tenderProcedure=" + tenderProcedure + ", amountForRealization=" + amountForRealization + ", setupAmountDate=" + setupAmountDate
				+ ", baseForAmount=" + baseForAmount + ", setupAmountPerson=" + setupAmountPerson + ", acceptPerson=" + acceptPerson + ", applicant="
				+ applicant + ", fundingSources=" + fundingSources + ", CPV=" + CPV + "]";
	}

	public String getApplicationNo() {
		return applicationNo;
	}

	public void setApplicationNo(String applicationNo) {
		this.applicationNo = applicationNo;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public String getOrderSubject() {
		return orderSubject;
	}

	public void setOrderSubject(String orderSubject) {
		this.orderSubject = orderSubject;
	}

	public int getOrderKind() {
		return orderKind;
	}

	public void setOrderKind(int orderKind) {
		this.orderKind = orderKind;
	}

	public int getTenderProcedure() {
		return tenderProcedure;
	}

	public void setTenderProcedure(int tenderProcedure) {
		this.tenderProcedure = tenderProcedure;
	}

	public String getAmountForRealization() {
		return amountForRealization;
	}

	public void setAmountForRealization(String amountForRealization) {
		this.amountForRealization = amountForRealization;
	}

	public Timestamp getSetupAmountDate() {
		return setupAmountDate;
	}

	public void setSetupAmountDate(Timestamp setupAmountDate) {
		this.setupAmountDate = setupAmountDate;
	}

	public String getBaseForAmount() {
		return baseForAmount;
	}

	public void setBaseForAmount(String baseForAmount) {
		this.baseForAmount = baseForAmount;
	}

	public String getSetupAmountPerson() {
		return setupAmountPerson;
	}

	public void setSetupAmountPerson(String setupAmountPerson) {
		this.setupAmountPerson = setupAmountPerson;
	}

	public String getAcceptPerson() {
		return acceptPerson;
	}

	public void setAcceptPerson(String acceptPerson) {
		this.acceptPerson = acceptPerson;
	}

	public String getApplicant() {
		return applicant;
	}

	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}

	public String getFundingSources() {
		return fundingSources;
	}

	public void setFundingSources(String fundingSources) {
		this.fundingSources = fundingSources;
	}

	public String getCPV() {
		return CPV;
	}

	public void setCPV(String cPV) {
		CPV = cPV;
	}


	
}
