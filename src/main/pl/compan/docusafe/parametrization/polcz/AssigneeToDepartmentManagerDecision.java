package pl.compan.docusafe.parametrization.polcz;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class AssigneeToDepartmentManagerDecision implements DecisionHandler {

	private static final Logger LOG = LoggerFactory.getLogger(AssigneeToDepartmentManagerDecision.class);
	private final static String KOLUMNA_KZ = "subgroupID";
	private final static String KOLUMNA_KJ = "class_type_id";

	@Override
	public String decide(OpenExecution openExecution) {
		try {

			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			String manager_of_department = "manager_of_department";
			String manager_of_unit = "manager_of_unit";
			String omit = null;

			if (openExecution.getVariable("onlyDepartmentManagerDecision") != null) {
				omit = openExecution.getVariable("onlyDepartmentManagerDecision").toString();
			}

			if (omit == null || !omit.equals("true")) {
				/*
				 * przypisanie zadania do kierownika zak�adu lub kierownika
				 * jednostki zale�nie od wybranych os�b na formularzu na etapie
				 * Sekcji Finansowej
				 */
				if (doc.getFieldsManager().getKey("MPK") != null) {
					@SuppressWarnings("unchecked")
					List<Long> dictionaryIds = (List<Long>) doc.getFieldsManager().getKey("MPK");
					PreparedStatement ps;
					ResultSet rs;
					Long centrumId = (Long) openExecution.getVariable("costCenterId");
					Integer kz = null;
					Integer kj = null;

					for (Long id : dictionaryIds) {
						ps = DSApi.context().prepareStatement(
								"SELECT " + KOLUMNA_KZ + " FROM DSG_CENTRUM_KOSZTOW_FAKTURY WHERE centrumId = ? AND id = ?");
						ps.setLong(1, centrumId);
						ps.setLong(2, id);
						rs = ps.executeQuery();
						if (rs.next())
							kz = (Integer) rs.getObject(1);
					}
					for (Long id : dictionaryIds) {
						ps = DSApi.context().prepareStatement(
								"SELECT " + KOLUMNA_KJ + " FROM DSG_CENTRUM_KOSZTOW_FAKTURY WHERE centrumId = ? AND id = ?");
						ps.setLong(1, centrumId);
						ps.setLong(2, id);
						rs = ps.executeQuery();
						if (rs.next())
							kj = (Integer) rs.getObject(1);
					}
					if (kj != null) {
						if (kz != null)
							return "department";
						else
							return "unit";
					}
				}
			}
			// WNIOSKODAWCA, WORKER_DIVISION
			String author = doc.getAuthor();
			String guidWnioskodawca = fm.getEnumItemCn("WORKER_DIVISION");
			String wnioskodawca = fm.getEnumItemCn("WNIOSKODAWCA");

			List<String> users = new ArrayList<String>();
			List<String> divisions = new ArrayList<String>();
			boolean find = false;
			for (AcceptanceCondition accept : AcceptanceCondition.find(manager_of_unit, guidWnioskodawca)) {
				find = true;
				if (accept.getUsername() != null && !users.contains(accept.getUsername()))
					users.add(accept.getUsername());
				if (accept.getDivisionGuid() != null && !divisions.contains(accept.getDivisionGuid()))
					divisions.add(accept.getDivisionGuid());
			}

			for (String guid : divisions) {
				for (DSUser user : Arrays.asList(DSDivision.find(guid).getUsers())) {
					if (wnioskodawca.equals(user.getName()))
						return "rector";
				}
			}

			for (String user : users) {
				if (wnioskodawca.equals(user))
					return "rector";
			}

			users = new ArrayList<String>();
			divisions = new ArrayList<String>();
			for (AcceptanceCondition accept : AcceptanceCondition.find(manager_of_department, guidWnioskodawca)) {
				find = true;
				if (accept.getUsername() != null && !users.contains(accept.getUsername()))
					users.add(accept.getUsername());
				if (accept.getDivisionGuid() != null && !divisions.contains(accept.getDivisionGuid()))
					divisions.add(accept.getDivisionGuid());
			}

			for (String guid : divisions) {
				for (DSUser user : Arrays.asList(DSDivision.find(guid).getUsers())) {
					if (wnioskodawca.equals(user.getName()))
						return "unit";
				}
			}

			for (String user : users) {
				if (wnioskodawca.equals(user))
					return "unit";
			}

			if (!find)
				return "rector";

			return "department";
		} catch (EdmException ex) {
			LOG.error(ex.getMessage(), ex);
			return "error";
		} catch (SQLException e) {
			LOG.error(e.getMessage(), e);
			return "error";
		}
	}

}
