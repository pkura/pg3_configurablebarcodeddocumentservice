package pl.compan.docusafe.parametrization.polcz;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.Map;

import com.google.gson.FieldNamingPolicy;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;

public class ApplicationWKOLogic extends AbstractBudgetApplicationLogic
{
	private static final String DATY_KONFERENCJI_ERROR_MSG = "Data zako�czenia konferencji nie mo�e by� wcze�niejsza od daty rozpocz�cia; ";
	
	private static final String UCZESTNICTWO_KURS_FIELD = "DWR_UCZESTNICTWO_KURS";
	private static final String PUBLIKACJA_KURS_FIELD = "DWR_PUBLIKACJA_KURS";
	private static final String UCZESTNICTWO_KWOTA_FIELD = "DWR_UCZESTNICTWO_KWOTA";
	private static final String PUBLIKACJA_KWOTA_FIELD = "DWR_PUBLIKACJA_KWOTA";
	private static final String UCZESTNICTWO_KWOTA_PLN_FIELD = "DWR_UCZESTNICTWO_KWOTA_PLN";
	private static final String PUBLIKACJA_KWOTA_PLN_FIELD = "DWR_PUBLIKACJA_KWOTA_PLN";
	private static final String KWOTA_PLN_FIELD = "DWR_KWOTA_PLN";
	private static final String UCZESTNICTWO_FIELD = "UCZESTNICTWO";
	private static final String PUBLIKCJA_FIELD = "PUBLIKACJA";
	private static ApplicationWKOLogic instance;
	
	public static ApplicationWKOLogic getInstance()
	{
		if (instance == null)
			instance = new ApplicationWKOLogic();
		return instance;
	}

	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) {

		pl.compan.docusafe.core.dockinds.dwr.Field msgField = super.validateDwr(values, fm);
		
		StringBuilder msgBuilder = new StringBuilder();

		if (msgField != null)
			msgBuilder.append(msgField.getLabel());
		
		if (values.get(UCZESTNICTWO_KWOTA_PLN_FIELD) != null) {
			values.get(UCZESTNICTWO_KWOTA_PLN_FIELD).setMoneyData(DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.MULTIPLY, 2, false, values.get(UCZESTNICTWO_KWOTA_FIELD),values.get(UCZESTNICTWO_KURS_FIELD)));
		}
		if (values.get(PUBLIKACJA_KWOTA_PLN_FIELD) != null) {
			values.get(PUBLIKACJA_KWOTA_PLN_FIELD).setMoneyData(DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.MULTIPLY, 2, false, values.get(PUBLIKACJA_KWOTA_FIELD),values.get(PUBLIKACJA_KURS_FIELD)));
		}
		if (values.get(KWOTA_PLN_FIELD) != null) {
			values.get(KWOTA_PLN_FIELD).setMoneyData(DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.ADD, 2, true, values.get(UCZESTNICTWO_KWOTA_PLN_FIELD),values.get(PUBLIKACJA_KWOTA_PLN_FIELD)));
		}
		
		// data zako�czenia konferencji nie mo�e by� wcze�niejsza od daty rozpocz�cia
		msgBuilder.append(validateDate(values.get("DWR_ROZPOCZECIE_KONFERENCJI"), values.get("DWR_ZAKONCZENIE_KONFERENCJI"), DATY_KONFERENCJI_ERROR_MSG));
		
		if (msgBuilder.length()>0)
		{
			values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
			return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
		}
		
		return null;
	}
	
	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values) {
		setAdditionalTemplateValuesBasic(docId, values);
	}

	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException {
		StringBuilder msgBuilder = new StringBuilder();
		
		try {
			super.onSaveActions(kind, documentId, fieldValues);
		} catch (EdmException e) {
			msgBuilder.append(e.getMessage());
		}
		
		if (validateCheckboxField(fieldValues, UCZESTNICTWO_FIELD, PUBLIKCJA_FIELD) != null && validateCheckboxField(fieldValues, UCZESTNICTWO_FIELD, PUBLIKCJA_FIELD) < 1) {
			msgBuilder.append("Prosz� zaznaczy� przy najmniej jeden rodzaj wniosku; ");
		}
		
		if (msgBuilder.length() > 0)
			throw new EdmException(msgBuilder.toString());
	}

	private void sumCosts(Map<String, FieldData> values) {
		BigDecimal uczestnictwoKwota = values.get(UCZESTNICTWO_KWOTA_PLN_FIELD).getMoneyData();
		BigDecimal publikacjaKwota = values.get(PUBLIKACJA_KWOTA_PLN_FIELD).getMoneyData();
		values.get(KWOTA_PLN_FIELD).setMoneyData(uczestnictwoKwota.add(publikacjaKwota).setScale(2, RoundingMode.HALF_UP));
	}
	
}
