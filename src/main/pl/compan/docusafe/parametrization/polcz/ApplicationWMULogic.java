package pl.compan.docusafe.parametrization.polcz;

import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DSUserEnumField;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class ApplicationWMULogic extends AbstractBudgetApplicationLogic
{
	private static ApplicationWMULogic instance;
	protected Logger log = LoggerFactory.getLogger(ApplicationWMULogic.class);
	
	public static ApplicationWMULogic getInstance()
	{
		if (instance == null)
			instance = new ApplicationWMULogic();
		return instance;
	}
	
	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException 
	{

		super.setInitialValues(fm, type);

		DSUserEnumField dsef = (DSUserEnumField) fm.getField("KOORDYNATOR");
		if (fm.getEnumItem("WORKER_DIVISION") != null)
		{
			dsef.setRefGuid(DSDivision.findById(fm.getEnumItem("WORKER_DIVISION").getId()).getGuid());
		}
	}
	
	public void specialSetDictionaryValues(Map<String, ?> dockindFields)
	{
		log.info("specialSetDictionaryValues: \n {} ", dockindFields);

		if (dockindFields.keySet().contains("CONTRACTOR_DICTIONARY"))
		{
			Map<String, FieldData> m = (Map<String, FieldData>) dockindFields.get("CONTRACTOR_DICTIONARY");
			m.put("CONTRACTOR_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			m.put("CONTRACTOR_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}
	
	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values) {
		try{
			super.setAdditionalTemplateValues(docId, values);
			for (DocumentAcceptance dacc : DocumentAcceptance.find(docId))
			{
				DSUser acceptedUser = DSUser.findByUsername(dacc.getUsername());
				values.put(dacc.getAcceptanceCn(), acceptedUser.asFirstnameLastname());
				values.put(dacc.getAcceptanceCn()+"_date", DateUtils.formatCommonDateTime(dacc.getAcceptanceTime()));
				log.error("sprawdzam akceptacje:" + dacc.getAcceptanceCn() + " - " + acceptedUser.asFirstnameLastname());
				log.error("sprawdzam akceptacje:" + dacc.getAcceptanceCn()+"_date" + " - " + DateUtils.formatCommonDateTime(dacc.getAcceptanceTime()));
			}
			values.put("GENERATE_DATE", DateUtils.formatCommonDate(new Date()));
			setAcceptances(docId, values);
			FieldsManager fm = Document.find(docId).getFieldsManager();
			if(fm.containsField("WYKONAWCA_NAZWA") && fm.getValue("WYKONAWCA_NAZWA") != null){
				values.put("WYKONAWCA_NAZWA", fm.getValue("WYKONAWCA_NAZWA"));
			}
			if(fm.containsField("WYKONAWCA_ADRES") && fm.getValue("WYKONAWCA_ADRES") != null){
				values.put("WYKONAWCA_ADRES", fm.getValue("WYKONAWCA_ADRES"));
			}
			if(fm.containsField("WYKONAWCA_KOD") && fm.getValue("WYKONAWCA_KOD") != null){
				values.put("WYKONAWCA_KOD", fm.getValue("WYKONAWCA_KOD"));
			}
			if(fm.containsField("WYKONAWCA_MIASTO") && fm.getValue("WYKONAWCA_MIASTO") != null){
				values.put("WYKONAWCA_MIASTO", fm.getValue("WYKONAWCA_MIASTO"));
			}
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
			
	}
	

	public void addSpecialInsertDictionaryValues(String dictionaryName, Map<String, FieldData> values)
	{
		if (dictionaryName.contains("CONTRACTOR"))
		{
			values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}

	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		if (dictionaryName.contains("CONTRACTOR"))
		{
			values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}
	
	
	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		FieldsManager fm = document.getFieldsManager();
		
		double kurs = Double.parseDouble(Docusafe.getAdditionProperty("kurs_euro"));

		BigDecimal suma = BigDecimal.ZERO;
		for (BudgetItem bi : getBudgetItems(fm)) {
			suma = suma.add(bi.getKwota());
		}
		String kwotaNettoStr = (String) fm.getValue("KWOTA_NETTO");
		double kwotaNettoDouble = Double.parseDouble(kwotaNettoStr.replace(" ", ""));
		if (kwotaNettoDouble > (14000.0*kurs))
			throw new EdmException("Kwota netto nie mo�e przekraczac kwoty 14000 euro ("+14000.0*kurs+" z� kurs: " + kurs + ").");
		if (kwotaNettoDouble < 10000)
			throw new EdmException("Kwota netto nie mo�e byc mniejsza od  kwoty 10000 z�.");
		
		
		String kwotaStr = (String) fm.getValue("KWOTA_BRUTTO");
		log.error("KWBRUTTO string=" + kwotaStr);
		
		double doub = Double.parseDouble(kwotaStr.replace(" ", ""));
		log.error("KWBRUTTO double=" + doub);
		log.error("KWOTA BRUTTO suma="+suma);
		if (suma.doubleValue() != doub)
			throw new EdmException("Suma pozycji bud�etowych musi byc r�wna kwocie brutto.");
		if (checkTermin(fm))
			throw new EdmException("Termin realizacji wykracza poza podany rok budzetowy");

		super.onStartProcess(document, event);
	}
	
	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) 
	{
		
		pl.compan.docusafe.core.dockinds.dwr.Field msgField = super.validateDwr(values, fm);
		
		StringBuilder msgBuilder = new StringBuilder();
		
		if (msgField != null)
			msgBuilder.append(msgField.getLabel());
		
		// Sprawdzenie czy termin realizacji mie�ci w podanym roku
		checkTerminRealizacji(values, msgBuilder);
					
		if (msgBuilder.length() > 0) 
		{
			values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
			return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(),
					pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
		}
		
		return null;
	}
	
	private boolean checkTermin(FieldsManager fm){
		try {
			int idTypu = fm.getEnumItem("TYP_TERMINU").getId();
			switch(idTypu){
			case 1601: return checkOdPodpisania(fm);
			case 1602: return checkOdDo(fm);
			case 1603: return checkDataZakonczenia(fm);
			}
		} catch (EdmException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private boolean checkDataZakonczenia(FieldsManager fm) throws EdmException{
			Map<String, Object> mapa = (Map<String, Object>) fm.getValue("TERMIN_DATA_ZAKONCZENIA");
			Date termin = (Date) mapa.get("TERMIN_DATA_ZAKONCZENIA_OD");
			int rokBudzetowy = (Integer) fm.getValue("ROK_BUDZETOWY");
			
			GregorianCalendar cal = new GregorianCalendar(rokBudzetowy, 11, 31);
			Date dat = cal.getTime();
			
			return dat.before(termin);
			
	}
	
	

	private boolean checkOdDo(FieldsManager fm) throws EdmException {
		Map<String, Object> mapa = (Map<String, Object>) fm.getValue("TERMIN_OD_DO");
		int rokBudzetowy = (Integer) fm.getValue("ROK_BUDZETOWY");
		log.error("terminmapa:" + mapa);
		Date terminOd = (Date) mapa.get("TERMIN_OD_DO_OD");
		Date terminDo = (Date) mapa.get("TERMIN_OD_DO_DO");
//		log.error("terminytest:" + terminOd + "-------------" + terminDo);
//		log.error("terminytest:" + terminOd + "-------------" + terminDo);
		GregorianCalendar cal = new GregorianCalendar(rokBudzetowy, 11, 31);
		GregorianCalendar cal2 = new GregorianCalendar(rokBudzetowy, 0, 1);
		Date dat = cal.getTime();
		Date dat2 = cal2.getTime();
		log.error("terminoddo" + terminOd.after(dat) + "---" + terminDo.after(dat));
		if(terminOd.after(dat) || terminDo.after(dat) || terminOd.before(dat2) || terminDo.before(dat2)) return true;
		return false;
	}

	private boolean checkOdPodpisania(FieldsManager fm) {
		try{
			Map<String, Object> mapa = (Map<String, Object>) fm.getValue("TERMIN_OD_PODPISANIA");
			EnumValues ev = (EnumValues) mapa.get("TERMIN_OD_PODPISANIA_JM");
			String temp = ev.getSelectedValue();
			GregorianCalendar gc = new GregorianCalendar();
			int ile = (Integer) mapa.get("TERMIN_OD_PODPISANIA_ILE");
			int rokBudzetowy = (Integer) fm.getValue("ROK_BUDZETOWY");
			GregorianCalendar cal = new GregorianCalendar(rokBudzetowy, 11, 31);
			Date dat = cal.getTime();
			if(temp.equals("dni"))
			{
				gc.add(gc.DAY_OF_MONTH, ile);
			}
			else if(temp.equals("miesi�cy"))
			{
				gc.add(gc.MONTH, ile);
			}
			else if(temp.equals("lat"))
			{
				gc.add(gc.YEAR, ile);
			}
			Date test = gc.getTime();
			if(test.after(dat)) return true;
			else return false;
		}catch(Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return false;
	}
}