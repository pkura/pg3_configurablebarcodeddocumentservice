package pl.compan.docusafe.parametrization.polcz;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;

public class CorrectionFinishNormalDecision implements DecisionHandler
{
	@Override
	public String decide(OpenExecution openExecution)
	{
		Integer cor = (Integer) openExecution.getVariable("changeReason");

		if (cor != null && cor.equals(Jbpm4Constants.CHANGE_REASON_CORRECTION))
		{
			openExecution.removeVariable("changeReason");
			openExecution.setVariable("changeReason", Jbpm4Constants.CHANGE_REASON_NONE);
			openExecution.setVariable("count", 3);

			return "correction";
		}
		else if (cor != null && cor.equals(Jbpm4Constants.CHANGE_REASON_FINISH))
		{
			openExecution.removeVariable("changeReason");
			openExecution.setVariable("correction", Jbpm4Constants.CHANGE_REASON_NONE);
			openExecution.setVariable("count", 3);
			
			return "finish";
		}
		else
		{
			return "normal";
		}
	}
	

}
