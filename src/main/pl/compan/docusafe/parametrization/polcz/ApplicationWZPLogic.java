package pl.compan.docusafe.parametrization.polcz;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DSUserEnumField;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class ApplicationWZPLogic extends AbstractBudgetApplicationLogic
{
	
	private static final String CENTRUM_ID_TABLE_NAME = "simple_erp_per_docusafe_bd_str_budzet_view";
	
	StringManager sm = GlobalPreferences.loadPropertiesFile(ApplicationWZPLogic.class.getPackage().getName(), null);
	
	private static ApplicationWZPLogic instance;

	public static ApplicationWZPLogic getInstance() 
	{
		if (instance == null)
			instance = new ApplicationWZPLogic();
		return instance;
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException 
	{

		super.setInitialValues(fm, type);
		
		DSUserEnumField dsef = (DSUserEnumField) fm.getField("KOORDYNATOR");
		if (fm.getEnumItem("WORKER_DIVISION") != null) 
		{
			dsef.setRefGuid(DSDivision.findById(fm.getEnumItem("WORKER_DIVISION").getId()).getGuid());
		}
	}

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) 
	{
		
		pl.compan.docusafe.core.dockinds.dwr.Field msgField = super.validateDwr(values, fm);
		
		StringBuilder msgBuilder = new StringBuilder();
		
		if (msgField != null)
			msgBuilder.append(msgField.getLabel());
		
		// Na podstawie s�ownika "Stawki Vat" wyliczane s� warto�ci p�l "Laczna kwota netto" i "Laczna kwota brutto"
		setNettoBrutto(values);

		// Sprawdzenie czy data zako�czenia nie jest wcze�niejsza ni� data wype�nienia wniosku
		checkDataZakonczenia(values, fm, msgBuilder);
		
		// Sprawdzenie czy termin realizacji mie�ci w podanym roku
		checkTerminRealizacji(values, msgBuilder);
					
		if (msgBuilder.length() > 0) 
		{
			values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
			return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(),
					pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
		}
		
		return null;
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException 
	{
		// wystartowanie procesu
		super.onStartProcess(document, event);

		FieldsManager fm = document.getFieldsManager();
		
		// Sprawdzenie czy laczna kwota netto wniosku musi byc wieksza niz 14 tys eur - NA PROSBE PANA PIOTRA - WYLACZONE!!!
		//checkKwotaBruttOver14kEUR(fm);

		// Sprawdzenie czy loczna kwota brutto rowna sie sumie kwot wszsytkich pozycji
		isKwotaBruttoEqSumBudgetPositions(fm);

	}

	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) 
	{
		Map<String, Object> results = super.setMultipledictionaryValue(dictionaryName, values);
		
		// ustawienie kwoty brutto w s�owniku Stawki VAT
		setBruttoInStawkiVat(dictionaryName, values, results);
		
		return results;
	}

	private void setBruttoInStawkiVat(String dictionaryName, Map<String, FieldData> values, Map<String, Object> results)
	{
		if (dictionaryName.equals("KWOTY_STAWKI_VAT") && values.get("KWOTY_STAWKI_VAT_NETTO").getData() != null
				&& values.get("KWOTY_STAWKI_VAT_VAT").getData() != null) 
		{
			BigDecimal netto = values.get("KWOTY_STAWKI_VAT_NETTO").getMoneyData();
			BigDecimal vat = new BigDecimal(values.get("KWOTY_STAWKI_VAT_VAT").getIntegerData());
			BigDecimal brutto = BigDecimal.ZERO;
			BigDecimal hundred = new BigDecimal(100);
			BigDecimal a = vat.setScale(2).divide(hundred, RoundingMode.HALF_UP);
			BigDecimal b = BigDecimal.ONE.add(a);

			brutto = netto.multiply(b);
			results.put("KWOTY_STAWKI_VAT_BRUTTO", brutto);
		}
	}

	private void checkDataZakonczenia(Map<String, FieldData> values, FieldsManager fm, StringBuilder msgBuilder)
	{
		if (values.get("DWR_TERMIN_DATA_ZAKONCZENIA") != null) 
		{
			Map<String, FieldData> slownikTR = values.get("DWR_TERMIN_DATA_ZAKONCZENIA").getDictionaryData();
			if (slownikTR != null) 
			{
				Date start;
				try 
				{
					start = (Date) fm.getValue("DATA_WYPELNIENIA");
					Date finish = slownikTR.get("TERMIN_DATA_ZAKONCZENIA_OD").getDateData();
					if (start != null && finish != null && start.after(finish)) 
					{
						if (msgBuilder.length() == 0)
							msgBuilder.append("B��d: Termin realizacji nie mo�e by� wcze�niejszy ni� data z�o�enia wniosku");
						else
							msgBuilder.append(";  Termin realizacji nie mo�e by� wcze�niejszy ni� data z�o�enia wniosku");
					}
				} 
				catch (EdmException e)
				{
					log.error(e.getMessage());
				}
			}
		}
	}
	
	private void setNettoBrutto(Map<String, FieldData> values)
	{
		Map<String, FieldData> slownikKS = values.get("DWR_KWOTY_STAWKI_VAT").getDictionaryData();
		String suffix = null;
		if (slownikKS != null) 
		{
			BigDecimal sumaNetto = new BigDecimal(0);
			BigDecimal sumaBrutto = new BigDecimal(0);
			String nettoCn = null;
			String vatCN = null;
			String bruttoCN = null;
			for (String cn : slownikKS.keySet()) 
			{
				if (cn.contains("NETTO_") && slownikKS.get(cn).getData() != null) 
				{
					suffix = cn.substring(cn.lastIndexOf("_"));
					nettoCn = "KWOTY_STAWKI_VAT_NETTO".concat(suffix);
					vatCN = "KWOTY_STAWKI_VAT_VAT".concat(suffix);
					bruttoCN = "KWOTY_STAWKI_VAT_BRUTTO".concat(suffix);
					if (slownikKS.get(vatCN).getData() != null) {
						setBruttoBasedOnNetAndVat(values, slownikKS, nettoCn, vatCN, bruttoCN);
						sumaNetto = sumaNetto.add(slownikKS.get(cn).getMoneyData());
						sumaBrutto = sumaBrutto.add(slownikKS.get(bruttoCN).getMoneyData());
					}
				}
			}
			values.get("DWR_SUMA_NETTO").setMoneyData(sumaNetto);
			values.get("DWR_SUMA_BRUTTO").setMoneyData(sumaBrutto);
		}
	}

	private void setBruttoBasedOnNetAndVat(Map<String, FieldData> values, Map<String, FieldData> dict, String nettoCn, String vatCN, String bruttoCN)
	{
		BigDecimal netto = dict.get(nettoCn).getMoneyData();
		BigDecimal vat = new BigDecimal(dict.get(vatCN).getIntegerData());
		BigDecimal brutto = BigDecimal.ZERO;
		BigDecimal hundred = new BigDecimal(100);
		BigDecimal a = vat.setScale(2).divide(hundred, RoundingMode.HALF_UP);
		BigDecimal b = BigDecimal.ONE.add(a);

		brutto = netto.multiply(b);
		values.get("DWR_KWOTY_STAWKI_VAT").getDictionaryData().get(bruttoCN).setMoneyData(brutto);
	}
	
	private void isKwotaBruttoEqSumBudgetPositions(FieldsManager fm) throws EdmException
	{
		BigDecimal netto = (BigDecimal) fm.getFieldValues().get("SUMA_BRUTTO");
		List<Long> ids = (List<Long>) fm.getKey("MPK");
		BigDecimal suma = BigDecimal.ZERO;

		for (Long id : ids) 
		{
			Map<String, Object> mpk = DwrDictionaryFacade.dictionarieObjects.get("MPK").getValues(id.toString());
			BigDecimal kwota = BigDecimal.valueOf((Double) mpk.get("MPK_AMOUNT"));
			suma = suma.add(kwota);
		}
		if (!netto.equals(suma.setScale(2)))
		{
			throw new EdmException("��czna kwota brutto musi by� r�wna sumie kwot wszystkich pozycji bud�etowych.");
		}
	}

	private void checkKwotaBruttOver14kEUR(FieldsManager fm) throws EdmException
	{
		BigDecimal netto = (BigDecimal) fm.getFieldValues().get("SUMA_NETTO");

		String kurs_euro = Docusafe.getAdditionProperty("kurs.euro") != null ? Docusafe.getAdditionProperty("kurs.euro") : "4.2345";
		BigDecimal kurs = new BigDecimal(kurs_euro).setScale(2, BigDecimal.ROUND_HALF_DOWN);
		BigDecimal limit = new BigDecimal(14000);
		if (netto.divide(kurs, 2, RoundingMode.HALF_UP).compareTo(limit) <= 0)
			throw new EdmException("��czna kwota netto wniosku o udzielenie zam�wienia publicznego musi przekracza� warto�� 14 000 EUR.");
	}
}
