package pl.compan.docusafe.parametrization.polcz;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.commons.codec.digest.DigestUtils;
import org.directwebremoting.jsp.ConvertTag;

import com.lowagie.tools.plugins.treeview.UpdateableTreeNode;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.parametrization.AbstractParametrization;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.util.Base64;
import pl.compan.docusafe.web.common.form.UpdateUser;

public class PCZParametrization extends AbstractParametrization {
	private static final String USOS_URL = "connection.pcz.url";
	private static final String USOS_USERNAME = "connection.pcz.username";
	private static final String USOS_PASSWORD = "connection.pcz.password";
	private String urlUSOS = Docusafe.getAdditionProperty(USOS_URL);
	private String usernameUSOS = Docusafe.getAdditionProperty(USOS_USERNAME);
	private String passwordUSOS = Docusafe.getAdditionProperty(USOS_PASSWORD);

	@Override
	public DSUser addUserIsNotExist(String username) throws EdmException, SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		UserImpl user = null;
		try {
			ps = DSApi.context().prepareStatement("select * from dsg_pcz_application_wspw_students where pesel = ?");
			ps.setString(1, username);
			rs = ps.executeQuery();
			if (rs.next()) {
				user = (UserImpl) UserFactory.getInstance().createUser(rs.getString("pesel"), rs.getString("imie"),
						rs.getString("nazwisko"));
				user.setCtime(new Timestamp(new Date().getTime()));
				user.setAdUser(false);
				user.setHashedPassword(rs.getString("haslo"));
				user.setIdentifier("USOS");

				Role.findByName("Student").addUser(user.getName());

				String kodWydzialu = null;
				String wydzial = rs.getString("wydzial");
				if ("WIM".equals(wydzial))
					kodWydzialu = "R-WIMiI";
				if ("WIP".equals(wydzial))
					kodWydzialu = "R-WIPMiFS";
				if ("WE".equals(wydzial))
					kodWydzialu = "R-WE";
				if ("WIS".equals(wydzial))
					kodWydzialu = "R-WliO�";
				if ("WB".equals(wydzial))
					kodWydzialu = "R-WB";
				if ("WZ".equals(wydzial))
					kodWydzialu = "R-WZ";

				DSDivision.findByCode(kodWydzialu).addUser(user);
				log.info("DSDivision.find(\"STUDENCI\"): " + DSDivision.find("STUDENCI"));
				DSDivision.find("STUDENCI").addUser(user);

				DSApi.context().session().save(user);
				DSApi.context().commit();

			} else {
				throw new UserNotFoundException("Brak studenta o podanym peselu");
			}
		} catch (DivisionNotFoundException e) {
			throw new DivisionNotFoundException("Nie znaleziono takiego wydzia�u: " + rs.getString("wydzial"));
		} catch (Exception e) {
			throw new EdmException(e);
		} finally {
			rs.close();
			ps.close();
		}
		return user;

	}

	@Override
	public boolean verifyPassword(DSUser user, String password) {
		if (user.getIdentifier() != null && user.getIdentifier().equals("USOS")) {
			Connection conn = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			String passwordFromUsos = null;
			try {
				conn = DriverManager.getConnection(urlUSOS, usernameUSOS, passwordUSOS);
				ps = conn
						.prepareStatement("SELECT k.haslo as password from DZ_KONTA k, DZ_OSOBY o, DZ_STUDENCI s " +
								"where o.id = k.os_id and o.id = s.os_id and o.pesel = ?");
				ps.setString(1, user.getName());
				rs = ps.executeQuery();
				rs.next();
				passwordFromUsos = rs.getString("password");
			} catch (SQLException e) {
				log.error(e.getMessage(), e);
			} finally {
				try {
					rs.close();
					ps.close();
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			return passwordFromUsos != null && passwordFromUsos.equals(hashPassword(password));
		} else
			return user.verifyPassword(password);
	}

	/**
	 * Zwraca hash podanego has�a.
	 * 
	 * @param password
	 *            Has�o w postaci jawnego tekstu.
	 * @throws RuntimeException
	 *             Je�eli biblioteka nie udost�pnia algorytmu MD5 lub kodowania
	 *             UTF-8.
	 */
	private String hashPassword(String password) {

		log.info("hashowanie has�a u�ytkownika USOS");

		String md5 = DigestUtils.md5Hex(password);
		return md5;
	}
}
