package pl.compan.docusafe.parametrization.polcz;

import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.jbpm.api.activity.ActivityExecution;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.parametrization.ifpan.IfpanUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Map;

public class RejestrujWUERP implements ExternalActivityBehaviour
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger
	 */
	private static final Logger log = LoggerFactory.getLogger(RejestrujWUERP.class);

	public void execute(ActivityExecution execution) throws Exception
	{

//		Parametry procedury :
//			exec up_pml_dopisz_absencje
//			nr pracownika (zgodny z SIMPLE),
//			kod absencji liczba (patrz � kody absencji SIMPLE),
//			data od,
//			data do,
//			czy rozliczac,
//			czy zatwierdzic
//
//			Kody absencji SIMPLE :
//			-- 70 Urlop wypoczynkowy 24784
//			-- 71 Urlop wyp. na ssdanie 24785
//			-- 72 Urlop okolicznosciowy 24786
//			-- 73 Urlop dodatkowy 24787
//			-- 74 Urlop szkoleniowy 24788
//			-- 75 Opieka nad dzieckiem art.188Kp 24789
//			-- 76 Poszukiwanie pracy art.37Kp 24790
//			-- 77 Inne nieobecnodci uspr. pdatne 24791
//			-- 78 Urlop wyr�wnawczy 24799
//			-- 79 Urlop zdrowotny 24800
//
//			Pola czy rozlicza� i czy zatwierdzi� - 1 - tak, 0 - nie
//			Proponuje przy wywo�aniu procedury wpisywa� tam 0 (zera), ale szc zeg�y prosz� uzgodni� z konsultantem wdra�aj�cym ze strony SIMPLE.
//
//			Przyk�ad wywo�ania :
//			exec up_pml_dopisz_absencje 17, 70, '2012-06-17', '2012-06-23', 0, 0

		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Document document = Document.find(docId);
		FieldsManager fm = document.getFieldsManager();
		Connection con = null;
		CallableStatement call = null;
		try
		{
			log.info("Rejestracja Wniosku urlopowego w ERP - START");
			String erpUser, erpPassword, erpUrl, erpDataBase, erpSchema, rejestracja;
			erpUser = Docusafe.getAdditionProperty("erp.database.user");
			erpPassword = Docusafe.getAdditionProperty("erp.database.password");
			erpUrl = Docusafe.getAdditionProperty("erp.database.url");
			erpDataBase = Docusafe.getAdditionProperty("erp.database.dataBase");
			erpSchema = Docusafe.getAdditionProperty("erp.database.schema");
			// up_pml_dopisz_absencje
			rejestracja = Docusafe.getAdditionProperty("erp.database.urlop.rejestracja");

			con = IfpanUtils.connectToSimpleErpDataBase(erpUser, erpPassword, erpUrl);

			call = con.prepareCall("{call [" + erpDataBase + "].[" + erpSchema + "].[" + rejestracja + "] (?,?,?,?,?,?)}");
			//@pracownik_nrewid int - nr pracownika (zgodny z SIMPLE),
			Integer pracownik_nrewid = getPracownik_nrewid(fm);
			call.setInt(1, pracownik_nrewid);
			//@kod_simple int  - kod absencji liczba (patrz � kody absencji SIMPLE),
			Integer kod_simple = getKod_simple(fm);
			call.setInt(2, kod_simple);
			//@data_po datatime
			Date data_po = getData_po(fm);
			call.setDate(3, data_po);
			//@data_ko datatime
			Date data_ko = getData_ko(fm);
			call.setDate(4, data_ko);
			//@czy rozliczyc int
			Integer czy_rozliczyc = 0;
			call.setInt(5, czy_rozliczyc);
			//@czy_zatwierdzic int
			Integer czy_zatwierdzic = 0;
			call.setInt(6, czy_zatwierdzic);
			
			log.debug(call.toString());
			log.debug("Parametry procedury up_pml_dopisz_absencje: pracownik_nrewid-{}, kod_simple-{}, data_od-{}, data_do-{}, czy_rozliczyc-{}, czy_zatwierdzic-{}",
			 pracownik_nrewid, kod_simple, data_po, data_ko, czy_rozliczyc, czy_zatwierdzic);
			
			int resultNum = 0;

			while (true)
			{
				boolean queryResult;
				int rowsAffected;

				if (1 == ++resultNum)
				{
					try
					{
						queryResult = call.execute();
					
						log.debug("Udany export wniosku urlopowego o id: {}: pracownik_nrewid: {}, kod_simple: {}, data_po: {}, data_ko: {}, czy_rozliczyc: {}, czy_zatwierdzic: {}",
								docId, pracownik_nrewid, kod_simple, data_po, data_ko, czy_rozliczyc, czy_zatwierdzic);
						
					}
					catch (Exception e)
					{
						// Process the error
						log.error("Result " + resultNum + " is an error: " + e.getMessage());

						// When execute() throws an exception, it may just
						// be that the first statement produced an error.
						// Statements after the first one may have
						// succeeded. Continue processing results until
						// there
						// are no more.
						continue;
					}
				}
				else
				{
					try
					{
						queryResult = call.getMoreResults();
					}
					catch (Exception e)
					{
						// Process the error
						log.error("1 != ++resultNum: Result " + resultNum + " is an error: " + e.getMessage());

						// When getMoreResults() throws an exception, it
						// may just be that the current statement produced
						// an error.
						// Statements after that one may have succeeded.
						// Continue processing results until there
						// are no more.
						continue;
					}
				}

				if (queryResult)
				{
					ResultSet rs = call.getResultSet();

					// Process the ResultSet
					log.debug("Result " + resultNum + " is a ResultSet: " + rs);
					ResultSetMetaData md = rs.getMetaData();

					int count = md.getColumnCount();
					log.debug("<table border=1>");
					log.debug("<tr>");
					for (int i = 1; i <= count; i++)
					{
						log.debug("<th>");
						log.debug("GEtColumnLabeL: {}", md.getColumnLabel(i));
					}
					log.debug("</tr>");
					while (rs.next())
					{
						log.debug("<tr>");
						for (int i = 1; i <= count; i++)
						{
							log.debug("<td>");
							log.debug("Get.Object: {}", rs.getObject(i));
						}
						log.debug("</tr>");
					}
					log.debug("</table>");
					rs.close();
				}
				else
				{
					rowsAffected = call.getUpdateCount();

					// No more results
					if (-1 == rowsAffected)
					{
						--resultNum;
						break;
					}

					// Process the update count
					log.debug("Result " + resultNum + " is an update count: " + rowsAffected);
				}
			}

			log.debug("Done processing " + resultNum + " results");
			if (resultNum == 0)
			{

			}
			
			log.info("Rejestracja Wniosku urlopowego w ERP - KONIEC");
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException("B��d eksportu wniosku urlopowego:" + e.getMessage());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					con.close();
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
					throw new EdmException("B��d eksportu wniosku urlopowego:" + e.getMessage());
				}
			}
		}
		execution.takeDefaultTransition();

	}
	
	private Date getData_ko(FieldsManager fm) throws EdmException
	{
		java.sql.Date sqlDate = null;
		try
		{
			java.util.Date utilDate = (java.util.Date)fm.getValue("DATA_DO");
			sqlDate = new java.sql.Date(utilDate.getTime());
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException("Problem z ustaleniem daty zako�czenia urlopu. Prosz� o kontakt z Administratorem Systemu Docusafe");
		}
		
		return sqlDate;
	}

	private Date getData_po(FieldsManager fm) throws EdmException
	{
		java.sql.Date sqlDate = null;
		try
		{
			java.util.Date utilDate = (java.util.Date)fm.getValue("DATA_OD");
			sqlDate = new java.sql.Date(utilDate.getTime());
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException("Problem z ustaleniem daty rozpocz�cia urlopu. Prosz� o kontakt z Administratorem Systemu Docusafe");
		}
		
		return sqlDate;
	}

	private Integer getKod_simple(FieldsManager fm) throws EdmException
	{
		try
		{
			return Integer.valueOf(fm.getEnumItem("URLOPRODZAJ").getCn());
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException("Problem z ustaleniem rodzaju urlopu. Prosz� o kontakt z Administratorem Systemu Docusafe");
		}
	}

	private Integer getPracownik_nrewid(FieldsManager fm) throws EdmException
	{
		Long userId = Long.valueOf((Integer)fm.getKey("WNIOSKODAWCA"));
		DSUser user = null;
		try
		{
			user = DSUser.findById(userId);
			return Integer.valueOf(user.getExtension());
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException("Problem z ustaleniem wnioskodawcy. Prosz� o kontakt z Administratorem Systemu Docusafe");
		}
	}

	public void signal(ActivityExecution execution, String signalName, Map<String, ?> parameters) throws Exception
	{

	}
}
