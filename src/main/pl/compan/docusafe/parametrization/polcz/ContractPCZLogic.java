package pl.compan.docusafe.parametrization.polcz;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;
import java.util.Map;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import com.google.common.collect.Maps;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class ContractPCZLogic extends AbstractDocumentLogic
{
	private static ContractPCZLogic instance;
	
	protected static Logger log = LoggerFactory.getLogger(ContractPCZLogic.class);
	
	/**
	 * Akceptacje
	 */
	private final String FINANCIAL_SECTION = "financial_section";
	private final String MANAGER_OF_UNIT = "manager_of_unit";
	private final String MANAGER_OF_DEPARTMENT = "manager_of_department";
	
	public String costCenterId;

	public static ContractPCZLogic getInstance()
	{
		if (instance == null)
			instance = new ContractPCZLogic();
		return instance;
	}

	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();

		FieldsManager fm = dockind.getFieldsManager(id);
		params.setStatus((String) fm.getValue("STATUS"));
		
		return params;
	}

	
	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue)
	{
		boolean assigned = false;

		if (FINANCIAL_SECTION.equals(acceptationCn) || MANAGER_OF_DEPARTMENT.equals(acceptationCn) || MANAGER_OF_UNIT.equals(acceptationCn))
		{
			try
			{
				String guid = doc.getFieldsManager().getEnumItemCn("WORKER_DIVISION");
				for (AcceptanceCondition accept : AcceptanceCondition.find(acceptationCn, guid))
				{
					if (accept.getUsername() != null)
						assignable.addCandidateUser(accept.getUsername());
					if (accept.getDivisionGuid() != null)
						assignable.addCandidateGroup(accept.getDivisionGuid());
				}
				assigned = true;
			}
			catch (EdmException e)
			{
				log.error(e.getMessage(), e);
			}
		}
		return assigned;
	}

	public void specialSetDictionaryValues(Map<String, ?> dockindFields)
	{
		log.info("specialSetDictionaryValues: \n {} ", dockindFields);

		if (dockindFields.keySet().contains("CONTRACTOR_DICTIONARY"))
		{
			Map<String, FieldData> m = (Map<String, FieldData>) dockindFields.get("CONTRACTOR_DICTIONARY");
			m.put("CONTRACTOR_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			m.put("CONTRACTOR_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}

	}

	public void addSpecialInsertDictionaryValues(String dictionaryName, Map<String, FieldData> values)
	{
		if (dictionaryName.contains("CONTRACTOR"))
		{
			values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}

	}

	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		if (dictionaryName.contains("CONTRACTOR"))
		{
			values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{

		
	}
	
	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

}
