package pl.compan.docusafe.parametrization.polcz;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

public class StudentIntegrationService extends ServiceDriver implements Service {
	public static final Logger log = LoggerFactory.getLogger(StudentIntegrationService.class);
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(StudentIntegrationService.class.getPackage().getName(), null);

	private Property[] properties;
	private Timer timer;
	private Integer periodWww = 24;
	private static final String USOS_URL = "connection.pcz.url";
	private static final String USOS_USERNAME = "connection.pcz.username";
	private static final String USOS_PASSWORD = "connection.pcz.password";
	private String url = Docusafe.getAdditionProperty(USOS_URL);
	private String username = Docusafe.getAdditionProperty(USOS_USERNAME);
	private String password = Docusafe.getAdditionProperty(USOS_PASSWORD);

	class ImportTimesProperty extends Property {
		public ImportTimesProperty() {
			super(SIMPLE, PERSISTENT, StudentIntegrationService.this,
					"periodWww", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi() {
			return periodWww;
		}

		protected void setValueSpi(Object object) throws ServiceException {
			synchronized (StudentIntegrationService.this) {
				if (object != null)
					periodWww = (Integer) object;
				else
					periodWww = 24;
			}
		}
	}

	class URLProperty extends Property {
		public URLProperty() {
			super(SIMPLE, PERSISTENT, StudentIntegrationService.this, "url",
					"�cie�ka do bazy", String.class);
		}

		protected Object getValueSpi() {
			return url;
		}

		protected void setValueSpi(Object object) throws ServiceException {
			synchronized (StudentIntegrationService.this) {
				if (object != null)
					url = (String) object;
				else
					url = Docusafe.getAdditionProperty(USOS_URL);
			}
		}
	}

	class UsernameProperty extends Property {
		public UsernameProperty() {
			super(SIMPLE, PERSISTENT, StudentIntegrationService.this,
					"username", "Nazwa u�ytkownika", String.class);
		}

		protected Object getValueSpi() {
			return username;
		}

		protected void setValueSpi(Object object) throws ServiceException {
			synchronized (StudentIntegrationService.this) {
				if (object != null)
					username = object.toString();
				else
					username = Docusafe.getAdditionProperty(USOS_USERNAME);
			}
		}
	}

	class PasswordProperty extends Property {
		public PasswordProperty() {
			super(SIMPLE, PERSISTENT, StudentIntegrationService.this,
					"password", "Has�o", String.class);
		}

		protected Object getValueSpi() {
			return password;
		}

		protected void setValueSpi(Object object) throws ServiceException {
			synchronized (StudentIntegrationService.this) {
				if (object != null)
					password = (String) object;
				else
					password = Docusafe.getAdditionProperty(USOS_PASSWORD);
			}
		}
	}

	public StudentIntegrationService() {
		properties = new Property[] { new ImportTimesProperty(),
				new URLProperty(), new UsernameProperty(),
				new PasswordProperty() };
	}

	@Override
	protected void start() throws ServiceException {
		log.info("Start uslugi StudentIntegrationService");
		timer = new Timer(true);
		timer.schedule(new Import(), 0, periodWww * DateUtils.HOUR);
		console(Console.INFO,
				sm.getString("System StudentIntegrationService wystartowal"));
	}

	@Override
	protected void stop() throws ServiceException {
		log.info("Stop uslugi StudentIntegrationService");
		console(Console.INFO,
				sm.getString("System StudentIntegrationService zako�czy� dzia�anie"));
		try {
			DSApi.close();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	@Override
	protected boolean canStop() {
		return false;
	}

	class Import extends TimerTask {
		@Override
		public void run() {

			Connection conn = null;
			PreparedStatement selectStatementUsos = null;
			ResultSet resultSelectFromUsos = null;
			try {
				log.info("Uruchamiam import z USOS");

				conn = DriverManager.getConnection(url, username, password);
				selectStatementUsos = conn
						.prepareStatement("select o.id, o.nazwisko, o.imie, o.pesel, s.indeks, j.skrot_nazwy as wydzial, k.haslo " +
								"from DZ_OSOBY o, DZ_STUDENCI s, DZ_JEDNOSTKI_ORGANIZACYJNE j, DZ_KONTA k " +
								"where o.id = k.os_id and j.kod = o.jed_org_kod and o.id = s.os_id");
				resultSelectFromUsos = selectStatementUsos.executeQuery();

				try {
					log.info("Rozpoczynam update tabeli dsg_pcz_application_wspw_students");
					DSApi.openAdmin();
					ResultSet resultCount = null;
					DSApi.context().begin();
					
					PreparedStatement insertStatement = DSApi
							.context()
							.prepareStatement(
									"INSERT INTO dsg_pcz_application_wspw_students values(?,?,?,?,null,null,null,?,?,?)");
					PreparedStatement updateStatement = DSApi
							.context()
							.prepareStatement(
									"UPDATE dsg_pcz_application_wspw_students SET nazwisko=?, imie=?, indeks=?, haslo=?, wydzial=?, pesel=? where id=?");
					PreparedStatement countStatement = DSApi
							.context()
							.prepareStatement(
									"select COUNT(id) AS count from dsg_pcz_application_wspw_students where id=?");
					
					while (resultSelectFromUsos.next()) {
						countStatement.setInt(1, resultSelectFromUsos.getInt("id"));
						resultCount = countStatement.executeQuery();
						resultCount.next();
						int countId = resultCount.getInt("count");
						if (countId == 0) {
							insertStatement.setInt(1, resultSelectFromUsos.getInt("id"));
							insertStatement.setString(2, resultSelectFromUsos.getString("nazwisko"));
							insertStatement.setString(3, resultSelectFromUsos.getString("imie"));
							insertStatement.setString(4, resultSelectFromUsos.getString("indeks"));
							insertStatement.setString(5, resultSelectFromUsos.getString("haslo"));
							insertStatement.setString(6, resultSelectFromUsos.getString("wydzial"));
							insertStatement.setString(7, resultSelectFromUsos.getString("pesel"));
							insertStatement.execute();
							console(Console.INFO,
									sm.getString("System StudentIntegrationService zaimportowa� studenta "
											+ resultSelectFromUsos.getString("imie") + " " + resultSelectFromUsos.getString("nazwisko")));
						} else {
							updateStatement.setString(1, resultSelectFromUsos.getString("nazwisko"));
							updateStatement.setString(2, resultSelectFromUsos.getString("imie"));
							updateStatement.setString(3, resultSelectFromUsos.getString("indeks"));
							updateStatement.setString(4, resultSelectFromUsos.getString("haslo"));
							updateStatement.setString(5, resultSelectFromUsos.getString("wydzial"));
							updateStatement.setString(6, resultSelectFromUsos.getString("pesel"));
							updateStatement.setInt(7, resultSelectFromUsos.getInt("id"));
							updateStatement.executeUpdate();
						}
					}
					DSApi.context().commit();
					resultCount.close();
					insertStatement.close();
					updateStatement.close();
					countStatement.close();
					log.info("Koniec importu !!!!!!!!!!");

				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}

			} catch (Exception e) {
				log.error(e.getMessage(), e);
			} finally {
				try {
					resultSelectFromUsos.close();
					selectStatementUsos.close();
					conn.close();
					DSApi.close();
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}
		}
	}

	public Property[] getProperties() {
		return properties;
	}

	public void setProperties(Property[] properties) {
		this.properties = properties;
	}
}
