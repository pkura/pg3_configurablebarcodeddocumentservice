package pl.compan.docusafe.parametrization.polcz;

import java.util.Map;
import com.google.common.collect.Maps;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.DSUserEnumField;
import pl.compan.docusafe.core.users.DSUser;

public class ApplicationWZSLogic extends AbstractApplicationLogic
{
	private static final long serialVersionUID = 1L;
	private static ApplicationWZSLogic instance;
	
	public static ApplicationWZSLogic getInstance()
	{
		if (instance == null)
			instance = new ApplicationWZSLogic();
		return instance;
	}
	
	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
		
		super.setInitialValues(fm, type);
		
		Map<String, Object> toReload = Maps.newHashMap();
		
		DSUser user = DSApi.context().getDSUser();
		
		DSUserEnumField osobaEf = (DSUserEnumField)fm.getField("OSOBA");
		if(DSApi.context().getDSUser().getDivisionsWithoutGroupPosition().length > 0)
			osobaEf.setRefGuid(DSApi.context().getDSUser().getDivisionsWithoutGroupPosition()[0].getGuid());
		toReload.put("OSOBA", user.getId());
		fm.reloadValues(toReload);
	}
}