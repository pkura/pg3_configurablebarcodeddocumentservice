package pl.compan.docusafe.parametrization.polcz;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.HTMLFieldLogic;

public class HtmlGetPdf implements HTMLFieldLogic {
	

	@Override
	public String getValue(String docId) throws EdmException  {

		return getValue();
	}

	@Override
	public String getValue() {
		StringBuilder sb = new StringBuilder();
        	sb.append("<input type=\"button\" onclick=\"document.getElementById('dockindEventValue').value = 'to_PDF'; document.getElementById('doDockindEvent').value = true; ");
        	sb.append(" document.forms[0].submit();\" value=\"Generuj zestawienie akceptacji\" class=\"btn\" name=\"doDockindEventProcessAction\">");

		return sb.toString();
	}
}
