package pl.compan.docusafe.parametrization.polcz;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DSUserEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class ApplicationWZELogic extends AbstractBudgetApplicationLogic 
{
	private static ApplicationWZELogic instance;
	StringManager sm = GlobalPreferences.loadPropertiesFile(ApplicationWZELogic.class.getPackage().getName(), null);

	public static ApplicationWZELogic getInstance()
	{
		if (instance == null)
			instance = new ApplicationWZELogic();
		return instance;
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException 
	{

		super.setInitialValues(fm, type);

		DSUserEnumField dsef = (DSUserEnumField) fm.getField("KOORDYNATOR");
		if (fm.getEnumItem("WORKER_DIVISION") != null)
		{
			dsef.setRefGuid(DSDivision.findById(fm.getEnumItem("WORKER_DIVISION").getId()).getGuid());
		}
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException 
	{

		FieldsManager fm = document.getFieldsManager();
		
		// Sprawdzenie czy kwota netto nie przekracza 10tys. zl
		checkKWotaBruttoLess10kPLN(fm);

		// Sprawdzenie czy loczna kwota brutto rowna sie sumie kwot wszsytkich pozycji
		isKwotaBruttoEqSumBudgetPositions(fm);
		
		// wystartowanie procesu
		super.onStartProcess(document, event);
	}
	
	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn,
			String fieldCnValue) 
	{
		log.debug("Start special assignee operations for acceptationCn: {}, fieldCnValue: {}", acceptationCn, fieldCnValue);
		
		boolean assigned = super.assignee(doc, assignable, openExecution, acceptationCn, fieldCnValue);
		
		if (!assigned)
		{
			// akceptacja vice kwestora
			if (AssingUtils.VCE_KWESTOR.equals(acceptationCn))
				assigned = AssingUtils.assignToVceKwestor(doc, assignable, openExecution);
		}
		
		return assigned;
	}

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) 
	{
		pl.compan.docusafe.core.dockinds.dwr.Field msgField = super.validateDwr(values, fm);
		
		StringBuilder msgBuilder = new StringBuilder();

		if (msgField != null)
			msgBuilder.append(msgField.getLabel());
		
		// kwota netto nie moze byc wieksza niz kwota brutto. !!!! To jest juz w Abstract ApplicationLogic, ale tutaj sa inne cn
		validateNettoBrutto2(values, msgBuilder);

		// Sprawdzenie czy termin realizacji mie�ci w podanym roku
		checkTerminRealizacji(values, msgBuilder);
				
		if (msgBuilder.length() > 0) 
		{
			values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
			return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(),
					pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
		}
		return null;
	}

	private void isKwotaBruttoEqSumBudgetPositions(FieldsManager fm) throws EdmException
	{
		BigDecimal brutto = (BigDecimal) fm.getFieldValues().get("SUMA_BRUTTO");
		@SuppressWarnings("unchecked")
		List<Long> ids = (List<Long>) fm.getKey("MPK");
		BigDecimal suma = BigDecimal.ZERO;

		for (Long id : ids) 
		{
			Map<String, Object> mpk = DwrDictionaryFacade.dictionarieObjects.get("MPK").getValues(id.toString());
			BigDecimal kwota = BigDecimal.valueOf((Double) mpk.get("MPK_AMOUNT"));
			suma = suma.add(kwota);
		}
		if (!brutto.equals(suma.setScale(2))) 
		{
			throw new EdmException("Kwota brutto musi by� r�wna sumie kwot wszystkich pozycji bud�etowych.");
		}
	}

	private void checkKWotaBruttoLess10kPLN(FieldsManager fm) throws EdmException
	{
		BigDecimal netto = (BigDecimal) fm.getFieldValues().get("KWOTA_NETTO");
		BigDecimal upLimit = new BigDecimal(10000);
		BigDecimal downLimit = new BigDecimal(1000);
		
		if (netto.compareTo(upLimit) >= 0 || netto.compareTo(downLimit) < 0)
			throw new EdmException("Kwota netto wniosku o rezerwacj� �rodk�w podlegaj�cych procedurze zlece� elektronicznych musi mie�ci� si� w przedziale 1 000 z� - 9 999 z�");
	}
	
	private void validateNettoBrutto2(Map<String, FieldData> values, StringBuilder msgBuilder)
	{
		if (DwrUtils.isNotNull(values, "DWR_KWOTA_NETTO") && DwrUtils.isNotNull(values, "DWR_SUMA_BRUTTO")) 
		{
			BigDecimal netto = values.get("DWR_KWOTA_NETTO").getMoneyData();
			BigDecimal brutto = values.get("DWR_SUMA_BRUTTO").getMoneyData();
			if (netto.setScale(2, RoundingMode.HALF_UP).compareTo(brutto.setScale(2, RoundingMode.HALF_UP)) == 1) 
			{
				if (msgBuilder.length() == 0)
					msgBuilder.append("B��d: Kwota brutto nie mo�e by� mniejsza ni� kwota netto.");
				else
					msgBuilder.append(";  Kwota brutto nie mo�e by� mniejsza ni� kwota netto.");
			}
		}
	}
	
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		try
		{
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			fm.initialize();
			
			// czas wygenerowania metryki
			values.put("GENERATE_DATE", DateUtils.formatCommonDate(new java.util.Date()).toString());
			
			// generowanie akceptacji
			setAcceptances(docId, values);
			
			// pozycje budzetu
			values.put("BUDZET", getBudgetItems(fm));

			if (fm.containsField("TYP_TERMINU") && fm.getValue("TYP_TERMINU") != null) {
				HashMap<String, Object> termin = new HashMap<String, Object>();
				EnumItem typTerminu = fm.getEnumItem("TYP_TERMINU");
				switch (typTerminu.getId()) {
				case 1151:
					termin = (HashMap<String, Object>) fm.getValue("TERMIN_OD_PODPISANIA");
					Integer terminIle = (Integer) termin.get("TERMIN_OD_PODPISANIA_ILE");
					log.error("TERMIN_OD_PODPISANIA=" + termin.get("TERMIN_OD_PODPISANIA_JM"));
					EnumValues tempValues = (EnumValues) termin.get("TERMIN_OD_PODPISANIA_JM");
					String terminCzego = tempValues.getSelectedOptions().get(0);
					int idTer = Integer.parseInt(terminCzego);
					switch (idTer) {
					case 20121:
						terminCzego = "dni";
						break;
					case 20122:
						terminCzego = "miesi�cy";
						break;
					case 20123:
						terminCzego = "lat";
						break;
					default:
						terminCzego = "";
					}
					values.put("TERMIN", terminIle + " " + terminCzego + " od podpisania umowy");
					break;
				case 1152:
					termin = (HashMap<String, Object>) fm.getValue("TERMIN_OD_DO");
					Date from = (Date) termin.get("TERMIN_OD_DO_OD");
					Date to = (Date) termin.get("TERMIN_OD_DO_DO");
					values.put("TERMIN", "od " + DateUtils.formatCommonDate(from) + " do " + DateUtils.formatCommonDate(to));
					break;
				case 1153:
					termin = (HashMap<String, Object>) fm.getValue("TERMIN_DATA_ZAKONCZENIA");
					Date koniec = (Date) termin.get("TERMIN_DATA_ZAKONCZENIA_OD");
					values.put("TERMIN", DateUtils.formatCommonDate(koniec));
					break;
				case 1154:
					termin = (HashMap<String, Object>) fm.getValue("TERMIN_INNY");
					String inny = (String) termin.get("TERMIN_INNY_INNY");
					values.put("TERMIN", inny);
					break;
				}
			}
			
			if (fm.containsField("KOORDYNATOR") && fm.getEnumItem("KOORDYNATOR")!= null)
			{
				EnumItem enumItem = fm.getEnumItem("KOORDYNATOR");
				DSUser koordynator = DSUser.findById(enumItem.getId().longValue());
				values.put("KOORDYNATOR", koordynator.getWholeName());
			}
			
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
	}
	
}