package pl.compan.docusafe.parametrization.polcz;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.db.criteria.NativeOrderExp.OrderType;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogicUtils;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessDefinition;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.Jbpm4SnapshotProvider;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.parametrization.polcz.MpkAccept.AcceptType;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.opensymphony.webwork.ServletActionContext;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

public class CostInvoiceLogic extends AbstractPczDocumentLogic
{
	// ladowanie kierownik�w oraz sekcji finansowej
	protected static final List<String> divShortcut = new ArrayList<String>();
	static
	{
		divShortcut.add("WE"); // Wydzia� Elektryczny
		divShortcut.add("WB"); // Wydzia� Budownictwa
		divShortcut.add("WIMiI"); // Wydzia� In�ynierii Mechanicznej
									// i Informatyki
		divShortcut.add("WIPMiFS"); // Wydzia� In�ynierii
									// Procesowej, Materia�owej i
									// Fizyki Stosowanej
		divShortcut.add("WIiO\u015a"); // Wydzia� In�ynierii i
										// Ochrony �rodowiska
		divShortcut.add("WZ"); // Wydzia� Zarz�dzania
	}
	
	protected static final List<String> rectorsShortcut = new ArrayList<String>();
	static {
		rectorsShortcut.add("RO"); // Prorektor
		rectorsShortcut.add("RK");
		rectorsShortcut.add("RN");
		rectorsShortcut.add("RD");
		rectorsShortcut.add("RR");
		// rectorsShortcut.add("R"); //Rektor
	}
	
	private static final String DATA_PLATNOSCI_ERROR_MSG = "Data p�atno�ci nie mo�e by� wcze�niejsza ni� data wystawienia";
	private static final String DATA_WPLYWU_ERROR_MSG = "Data wp�ywu nie mo�e by� wcze�niejsza ni� data wystawienia";
	private static final String DATA_WPLYWU_TERAZ_ERROR_MSG = "Data wp�ywu nie mo�e by� p�niejsza od dzisiejszego dnia";
	protected static Logger log = LoggerFactory.getLogger(CostInvoiceLogic.class);
	StringManager sm = GlobalPreferences.loadPropertiesFile(CostInvoiceLogic.class.getPackage().getName(), null);
	
	private static CostInvoiceLogic instance;
	
	public static CostInvoiceLogic getInstance()
	{
		if (instance == null)
			instance = new CostInvoiceLogic();
		return instance;
	}
	
	public void doDockindEvent(DockindButtonAction eventActionSupport, ActionEvent event, Document document, String activity, Map<String, Object> values, Map<String, Object> dockindKeys) throws EdmException
	{
		 ServletActionContext.getResponse().setContentType("text/rtf"); 
           Map<String, Object> params = new HashMap<String, Object>();
           DocFacade df= Documents.document(document.getId());
           params.put("fields", df.getFields());
           df.getDocument().getDocumentKind().logic().setAdditionalTemplateValues(df.getId(), params);
           String templateName = "FakturaKosztowaAkceptacje.rtf";
           ServletActionContext.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + templateName + "\"");
           try
           { 
          	 pl.compan.docusafe.core.templating.Templating.rtfTemplate(templateName, params, ServletActionContext.getResponse().getOutputStream(),"true");
          	 if (document.getFieldsManager().getEnumItem("STATUS").getCn().equals("rkff-finish"))
          	 {
          		
      			ProcessDefinition pdef = document.getDocumentKind().getDockindInfo()
      					.getProcessesDeclarations().getProcess("CostInvoice");
      			String taskId = activity.substring(activity.lastIndexOf(",")+1, activity.length());
      			ProcessInstance pi = pdef.getLocator().lookupForTaskId(taskId);
      			
      			DSApi.context().begin();
                  pl.compan.docusafe.core.dockinds.process.ProcessAction pa;
      			pdef.getLogic().process(
      					pi,
      					pa = ProcessActionContext
                                  .action("to rkff-finish-export")
                                  .document((OfficeDocument)document)
                                  .stored(false)
      							.activityId(activity));

      			DSApi.context().commit();
                  

                    event.setResult("task-list");
          	 }
           }
           catch (Exception e)
           {
          	 throw new EdmException(e.getMessage());
           }
	}
	
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values)
	{
		return setMultipledictionaryValue(dictionaryName, values, null);
	}
	
	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		log.info("Before Search - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.contains("CPVNR"))
		{
			values.put(dictionaryName + "BASE", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.INTEGER, 1));
		}
	}
	
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fValues)
	{
		Map<String, Object> results = super.setMultipledictionaryValue(dictionaryName, values);
		
		try
		{
			if (dictionaryName.equals("MPK"))
			{
				Map<String, Object> fieldsValues = new HashMap<String, Object>();
				EnumValues centrumIdEnum = values.get("MPK_CENTRUMID").getEnumValuesData();
//				centrumIdEnum.set
//				EnumValues newEnVa = new EnumValues(values, selected)
//				centrumIdEnum.setAllOptions(null);
				String budgetId = centrumIdEnum.getSelectedOptions().get(0);
				
				HashMap<String, String> allVal = new HashMap<String, String>();
				ArrayList<String> selVal = new ArrayList<String>();
				selVal.add(budgetId);
				
				for (DataBaseEnumField d : DataBaseEnumField.tableToField.get("simple_erp_per_docusafe_bd_str_budzet_view"))
				{
					for(EnumItem enumTemp:  d.getAvailableItems()){
						allVal.put(String.valueOf(enumTemp.getId()), enumTemp.getTitle());
					}
				}
				
				centrumIdEnum = new EnumValues(allVal, selVal);
				
				if (budgetId != null && !budgetId.equals(""))
				{
					Long budgetIdLong = new Long(budgetId);
					
					String projectDiv = null;
					
					//DataBaseEnumField.reloadForTable(CENTRUM_ID_TABLE_NAME);
					EnumItem projekt = DataBaseEnumField.getEnumItemForTable(CENTRUM_ID_TABLE_NAME,Integer.parseInt(budgetId));
					String projectName = projekt.getTitle();
					String externalId = projekt.getCn();
					
					// 1. wyszukiwanie skr�tu nazwy wydzia�u w nazwie projektu
					if (projectName != null && !projectName.equals("")) {
						int foundCount = 0;
						String found = "";
						String[] proNames = projectName.split(" ");
						
						for (String part : proNames) {
							if (divShortcut.contains(part)) {
								foundCount++;
								found = part;
								break;
							}
						}
						
						if (foundCount == 1) {
							projectDiv=found;
						}
					}
					
					// 2. skojarzenie wydzia�u z externalId, np. R WE, W WB, W WIPMiFS
					if (projectDiv == null && externalId != null && !externalId.equals("")) {
						String[] extParts = externalId.split(" ");
						if (extParts.length == 2) {
							if (extParts[0].equals("R") || extParts[0].equals("W")) { // R ** dla dziekan�w, W ** dla og�lno wydzia�owych
								if (divShortcut.contains(extParts[1])) {
									projectDiv = extParts[1];
								}
							}
						}
					}
					if (projectDiv != null && !projectDiv.equals("")) {
						DSDivision divGuid = null;
						try {
							//------------------------------------------------
							//omini�cie liter�wki w strukturze organizacyjnej dla dziekana wydzia�u In�ynierii i Ochrony �rodowiska: WIiO�!!!!!!!!
							//------------------------------------------------
							if (projectDiv.equals("WIiO\u015a")) {
								projectDiv = "WliO\u015a";
							}
							divGuid = DSDivision.findByCode("R-"+projectDiv);
						} catch (DivisionNotFoundException e) {
							
						}
						DSDivision[] departments = divGuid.getChildren();
						
						List<String> enumSelected = new ArrayList<String>();
						
						Map<String, String> enumValuesMD = findIdForAccept(departments, "manager_of_department", false, null, false);
						Map<String, String> enumValuesMU = findIdForAccept(departments, "manager_of_unit", true, null, false);
						Map<String, String> enumValuesSF = findIdForAccept(departments, "financial_section", false, null, true);
						
						MpkAccept mpkInstance = MpkAccept.getInstance();
						enumValuesMD = insertEnumItemIntoTable(enumValuesMD, AcceptType.MANAGER_OF_DEPARTMENT, budgetIdLong, mpkInstance);
						
						if (values.get("MPK_SUBGROUPID") != null && values.get("MPK_SUBGROUPID").getEnumValuesData() != null) {
							if (enumValuesMD.containsKey(values.get("MPK_SUBGROUPID").getEnumValuesData().getSelectedOptions().get(0))) {
								enumSelected.add(values.get("MPK_SUBGROUPID").getEnumValuesData().getSelectedOptions().get(0));
							}
						}
						if (enumSelected.size() < 1) {
							enumSelected.add("");
						}
						
						results.put("MPK_SUBGROUPID", new EnumValues(enumValuesMD, enumSelected));
						
						enumSelected = new ArrayList<String>();
						enumValuesMU = insertEnumItemIntoTable(enumValuesMU, AcceptType.MANAGER_OF_UNIT, budgetIdLong, mpkInstance);
						results.put("MPK_CLASS_TYPE_ID", new EnumValues(enumValuesMU, enumSelected));
						
						enumSelected = new ArrayList<String>();
						enumValuesSF = insertEnumItemIntoTable(enumValuesSF, AcceptType.FINANCIAL_SECTION, budgetIdLong, mpkInstance);
						results.put("MPK_CUSTOMSAGENCYID", new EnumValues(enumValuesSF, enumSelected));
					} else {
						// 3. skr�t nazwy projektu w externalId ds_division
						if (projectDiv == null) {
							projectDiv = externalId;
						}
						
						DSDivision[] divGuid = new DSDivision[1];
						try {
							if (externalId != null) {
								divGuid[0] = DSDivision.findByExternalId(externalId);
							} else {
								divGuid[0] = null;
							}
						} catch (DivisionNotFoundException e) {
							
						}
						Map<String, String> enumValuesMD = findIdForAccept(divGuid, "manager_of_department", false, null, false);
						Map<String, String> enumValuesMU = findIdForAccept(divGuid, "manager_of_unit", false, null, false);
						Map<String, String> enumValuesSF = findIdForAccept(divGuid, "financial_section", true, null, true);
						
						if (enumValuesMU.size()<2) {
							enumValuesMU = new LinkedHashMap<String, String>();
							for (String rectors : rectorsShortcut) {
								DSDivision rectorsDiv = DSDivision.findByCode(rectors);
								enumValuesMU = findIdForAccept(rectorsDiv.getChildren(), "manager_of_unit", true, enumValuesMU, false);
							}
							// wymuszone dodania rektora
							try {
								DSDivision rector = DSDivision.findByName("Kierownik dzia�u Rektor"); // TODO
								enumValuesMU.put(rector.getId().toString(),rector.getName());
								
							} catch (Exception e) {
								log.error("Nie znaleziono Rektora w strukturze organizacyjnej");
							}
						}
						
						MpkAccept mpkInstance = MpkAccept.getInstance();
						
						enumValuesMD = insertEnumItemIntoTable(enumValuesMD, AcceptType.MANAGER_OF_DEPARTMENT, budgetIdLong, mpkInstance);
						
						
						List<String> enumSelected = new ArrayList<String>();
						if (values.get("MPK_SUBGROUPID") != null && values.get("MPK_SUBGROUPID").getEnumValuesData() != null) {
							if (enumValuesMD.containsKey(values.get("MPK_SUBGROUPID").getEnumValuesData().getSelectedOptions().get(0))) {
								enumSelected.add(values.get("MPK_SUBGROUPID").getEnumValuesData().getSelectedOptions().get(0));
							}
						}
						if (enumSelected.size() < 1) {
							enumSelected.add("");
						}
						
						results.put("MPK_SUBGROUPID", new EnumValues(enumValuesMD, enumSelected));
						
						enumValuesMU = insertEnumItemIntoTable(enumValuesMU, AcceptType.MANAGER_OF_UNIT, budgetIdLong, mpkInstance);
						
						enumSelected = new ArrayList<String>();
						if (values.get("MPK_CLASS_TYPE_ID") != null && values.get("MPK_CLASS_TYPE_ID").getEnumValuesData() != null) {
							if (enumValuesMU.containsKey(values.get("MPK_CLASS_TYPE_ID").getEnumValuesData().getSelectedOptions().get(0))) {
								enumSelected.add(values.get("MPK_CLASS_TYPE_ID").getEnumValuesData().getSelectedOptions().get(0));
							}
						}
						enumValuesMU.remove("");
						if (enumValuesMU.size() < 1) {
							try {
								DSDivision rector = DSDivision.findByName("Kierownik dzia�u Rektor"); // TODO
								enumValuesMU.put(rector.getId().toString(),rector.getName());
								
							} catch (Exception e) {
								log.error("Nie znaleziono Rektora w strukturze organizacyjnej");
							}
						}
						
						
						results.put("MPK_CLASS_TYPE_ID", new EnumValues(enumValuesMU, enumSelected));
						
						enumSelected = new ArrayList<String>();
						
						enumValuesSF = insertEnumItemIntoTable(enumValuesSF, AcceptType.FINANCIAL_SECTION, budgetIdLong, mpkInstance);
						results.put("MPK_CUSTOMSAGENCYID", new EnumValues(enumValuesSF, enumSelected));
					}
					
					
					// pozycja budzetowa
					EnumValues test1111 = values.get("MPK_ACCOUNTNUMBER").getEnumValuesData();
					String pozycjaId = test1111.getSelectedOptions().get(0);
					
					// id wybranego budzetu na podstawie, ktorego zostanie
					// pobrana lista jego pozycji budzetowych
					fieldsValues.put("CENTRUMID_1", budgetId);
					
					boolean containsPozycjaId = false;
					
					for (DataBaseEnumField d : DataBaseEnumField.tableToField.get("simple_erp_per_docusafe_bd_pozycje_view"))
					{
						// pozycje budzetowe dla wybranego budzetu
						// (budgetID)
						EnumValues val = d.getEnumItemsForDwr(fieldsValues);
						results.put("MPK_ACCOUNTNUMBER", val);
						
						for (Map<String,String> map : val.getAllOptions()) {
							if(map.containsKey(pozycjaId)) {
								containsPozycjaId = true;
								break;
							}
						}
						break;
					}
					
					if (pozycjaId != null && !pozycjaId.equals(""))
					{

						// wybrana pozycja budzetowa
						List<String> selectedPositionId = new ArrayList<String>();
						if (containsPozycjaId) {
							selectedPositionId.add(pozycjaId);
						}
						((EnumValues) results.get("MPK_ACCOUNTNUMBER")).setSelectedOptions(selectedPositionId);

						// wybrane id pozycji budzetowej na podstawie,
						// ktorej zostanie pobrana lista jej zrodel
						fieldsValues.put("ACCOUNTNUMBER_1", pozycjaId);
						
						for (DataBaseEnumField d : DataBaseEnumField.tableToField.get("simple_erp_per_docusafe_bd_zrodla_view"))
						{
							EnumValues val = d.getEnumItemsForDwr(fieldsValues);
							results.put("MPK_ACCEPTINGCENTRUMID", val);
							break;
						}

						// wybrane zrodlo
						String zrodloId = values.get("MPK_ACCEPTINGCENTRUMID").getEnumValuesData().getSelectedOptions().get(0);
						if (zrodloId != null && !zrodloId.equals(""))
						{
							// wybrana pozcyaj zrodla
							List<String> selectedSoruceId = new ArrayList<String>();
							selectedSoruceId.add(zrodloId);
							((EnumValues) results.get("MPK_ACCEPTINGCENTRUMID")).setSelectedOptions(selectedSoruceId);
						}
					}
					else
					{
						EnumValues val = new EnumValues(Collections.EMPTY_MAP, Collections.EMPTY_LIST);
						results.put("MPK_ACCEPTINGCENTRUMID", val);
					}
				}
				else
				{
					EnumValues val = new EnumValues(Collections.EMPTY_MAP, Collections.EMPTY_LIST);
					results.put("MPK_ACCOUNTNUMBER", val);
					results.put("MPK_ACCEPTINGCENTRUMID", val);
				}
				
				CentrumKosztowDlaFaktury mpk = null; 
				if (values.get("MPK_ID") != null && values.get("MPK_ID").getData() != null)
				{
					try
					{
						Integer mpkId = values.get("MPK_ID").getIntegerData();
						mpk = CentrumKosztowDlaFaktury.getInstance().find(mpkId.longValue());
						
						// centrumid enum
						EnumValues centrumEnum = values.get("MPK_CENTRUMID").getEnumValuesData();
						String centrumId = centrumIdEnum.getSelectedOptions().get(0);
						if (centrumId != null && !centrumId.equals(""))
							mpk.setCentrumId(Integer.valueOf(centrumId));
						else
							mpk.setCentrumId(null);
						// accountnumber enum
						EnumValues accountNumber = values.get("MPK_ACCOUNTNUMBER").getEnumValuesData();
						String accountNumberId = accountNumber.getSelectedOptions().get(0);
						if (accountNumberId != null && !accountNumberId.equals(""))
							mpk.setAccountNumber(accountNumberId);
						else
							mpk.setAccountNumber(null);
						// acceptingcentrumid enum
						EnumValues acceptingCentrum = values.get("MPK_ACCEPTINGCENTRUMID").getEnumValuesData();
						String acceptingCentrumId = acceptingCentrum.getSelectedOptions().get(0);
						if (acceptingCentrumId != null && !acceptingCentrumId.equals(""))
							mpk.setAcceptingCentrumId(Integer.valueOf(acceptingCentrumId));
						else
							mpk.setAcceptingCentrumId(null);
						// amount money
						if (values.get("MPK_AMOUNT").getData() != null)
							mpk.setAmount(values.get("MPK_AMOUNT").getMoneyData());
						// lokalizacjia enum
						
						// analitycis_id
						
						//class_type_id
						EnumValues classType = values.get("MPK_CLASS_TYPE_ID").getEnumValuesData();
						String classTypeId = classType.getSelectedOptions().get(0);
						if (classTypeId != null && !classTypeId.equals(""))
							mpk.setClassTypeId(Integer.valueOf(classTypeId));
						else
							mpk.setClassTypeId(null);
						// subgroupid
						EnumValues subgroup = values.get("MPK_SUBGROUPID").getEnumValuesData();
						String subgroupId = subgroup.getSelectedOptions().get(0);
						if (subgroupId != null && !subgroupId.equals(""))
							mpk.setSubgroupId(Integer.valueOf(subgroupId));
						else
							mpk.setSubgroupId(null);
						// customagencyid
						EnumValues customAgenct = values.get("MPK_CUSTOMSAGENCYID").getEnumValuesData();
						String customAgenctId = customAgenct.getSelectedOptions().get(0);
						if (customAgenctId != null && !customAgenctId.equals(""))
							mpk.setCustomsAgencyId(Integer.valueOf(customAgenctId));
						else
							mpk.setCustomsAgencyId(null);
						// connected_type_id
						EnumValues connectedType = values.get("MPK_CONNECTED_TYPE_ID").getEnumValuesData();
						String connectedTypeId = connectedType.getSelectedOptions().get(0);
						if (connectedTypeId != null && !connectedTypeId.equals(""))
							mpk.setConnectedTypeId(Integer.valueOf(connectedTypeId));
						else
							mpk.setConnectedTypeId(null);
						
					}
					catch (Exception e)
					{
						log.warn(e.getMessage(), e);
					}
					
				}
				
				results.put("MPK_CENTRUMID",centrumIdEnum);
			}
			else if(dictionaryName.equalsIgnoreCase("STAWKI_VAT"))
			{
				if(values.containsKey("STAWKI_VAT_POZYCJAID") && values.get("STAWKI_VAT_POZYCJAID")!=null)
				{
					Long posId = Long.valueOf(values.get("STAWKI_VAT_POZYCJAID").getEnumValuesData().getSelectedOptions().get(0)); 
					CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(posId);
					Integer destiny = mpk.getAdditionalId();
					
					ArrayList<String> lista = new ArrayList<String>();
					String addId = String.valueOf(destiny);
					lista.add(addId.substring(0, 3));					
					
					EnumValues dzialalnosci = (EnumValues) fValues.get("STAWKI_VAT_DZIALALNOSC");
					
					HashMap<String, String> allVal2 = new HashMap<String, String>();
		
					dzialalnosci.setSelectedOptions(lista);
		
					
					results.put("STAWKI_VAT_DZIALALNOSC", dzialalnosci);
				}
			}
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
		
		// wylczenie obciazenia dla danej stawki vat, w s�owniku Stawki VAT
		fillStawkiVat_Obciazenie(dictionaryName, values, results);		
		
		return results;
	}
	
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
	{
		log.info("DokumentKind: {}, DocumentId: {}, Values from JavaScript: {}", fm.getDocumentKind().getCn(), fm.getDocumentId(), values);
 		StringBuilder msgBuilder = new StringBuilder();
		try
		{
			Set<String> budgetRealizationId = new HashSet<String>();
			if (values.get("DWR_MPK") != null && values.get("DWR_REALIZACJA_BUDZETU") != null) {
				loadBudgetRealizationItems(values, "MPK", budgetRealizationId);
				
				StringBuilder ids = new StringBuilder();
				
				for (String pozycja : budgetRealizationId) {
					if (ids.length() != 0) {
						ids.append(",");
					}
					ids.append(pozycja);
				}
				values.put("DWR_REALIZACJA_BUDZETU", new FieldData(Field.Type.STRING, ids.toString()));
			}
			if (fm.getEnumItemCn("STATUS").equals("opis_merytoryczny") && (DwrUtils.isNotNull(values, "DWR_RECIPIENT_HERE") || fm.getKey("RECIPIENT_HERE") != null))
			{
				if ( (String) fm.getKey("RECIPIENT_HERE") != null && "".equals(values.get("DWR_RECIPIENT_HERE").getDictionaryData().get("id").getStringData()))
				{
					values.put("DWR_DZIAL", new FieldData(Field.Type.STRING,"-"));
					if (values.get("DWR_MPK") != null) 
					{
						List<Long> mpkIds = DwrUtils.extractIdFromDictonaryField(values, "MPK");
						if (mpkIds.size() == 0)
							values.put("DWR_MPK", new FieldData(Field.Type.STRING, ""));
					}
				}
				else
				{
					String recipientId = (String) fm.getKey("RECIPIENT_HERE");
					if (recipientId == null)
					{
						recipientId = values.get("DWR_RECIPIENT_HERE").getDictionaryData().get("id").getStringData();
						if (recipientId.equals(""))
							recipientId = "n:"+values.get("DWR_RECIPIENT_HERE").getDictionaryData().get("RECIPIENT_HERE_DIVISION").getStringData();
					}
					DSApi.openAdmin();
					try
					{
						String code = "-";
						if (recipientId.contains("d:"))
						{
							int indexOf = recipientId.indexOf("d:");
							String guid = recipientId.substring(indexOf+2);
							code = DSDivision.find(guid).getParent().getCode();
						}
						else if (recipientId.contains("n:"))
						{
							int indexOf = recipientId.indexOf("n:");
							String name = recipientId.substring(indexOf+2);
							code = DSDivision.findByName(name).getParent().getCode();
						}
						else
						{
							Person adresatFaktury = Person.find(Long.parseLong(recipientId));
							DSDivision division = DSDivision.findByName(adresatFaktury.getOrganizationDivision());
							code = division.getParent().getCode();
						}
						if (values.get("DWR_DZIAL") != null && (!values.get("DWR_DZIAL").getStringData().equals(code) || code.equals("-") || values.get("DWR_DZIAL").getStringData().equals("-")))
						{
							// dodanie psutek stringa jako wartosc slownika, wymusza pobranie dla niego ponownie wartosi -> Dictionary.getFieldsValues, tymczasowe obejscie.
							if (values.get("DWR_MPK") != null) 
							{
								List<Long> mpkIds = DwrUtils.extractIdFromDictonaryField(values, "MPK");
								if (mpkIds.size() == 0)
									values.put("DWR_MPK", new FieldData(Field.Type.STRING, ""));
							}
						}
						values.put("DWR_DZIAL", new FieldData(Field.Type.STRING, code));
					}
					catch (Exception e)
					{
						values.put("DWR_DZIAL", new FieldData(Field.Type.STRING,"-"));
						if (values.get("DWR_MPK") != null) 
						{
							List<Long> mpkIds = DwrUtils.extractIdFromDictonaryField(values, "MPK");
							if (mpkIds.size() == 0)
								values.put("DWR_MPK", new FieldData(Field.Type.STRING, ""));
						}
					}
					finally
					{
						DSApi.close();
					}
				}
			}
			else
			{
				if (values.get("DWR_MPK") != null) 
				{
					List<Long> mpkIds = DwrUtils.extractIdFromDictonaryField(values, "MPK");
					if (mpkIds.size() == 0)
						values.put("DWR_MPK", new FieldData(Field.Type.STRING, ""));
				}
			}
			
			if (values.get("DWR_CONTRACT") != null)
			{
				// ustawienie pozycji umowy
				setContractItems(values, fm);
				// powiazenie umowy przetargowej z wnioskiem wzp
				setBlocadesWZP_WMU(values,fm, "wzp", "NR_SYGNATURY");
			}
				
			// powiazanie maleuj umowy (nr RK) z wniosekm WMU
			if (DwrUtils.isNotNull(values, "DWR_NR_UMOWY_RK"))
				setBlocadesWZP_WMU(values,fm, "wmu", "nr_umowy_rk");
			
			// ustawienie pozycji budzetowych na podstawie wybranych blokad
			if (values.get("DWR_BLOKADY") != null)
				setBudgetIems(values, fm, budgetRealizationId);

			// automatycznie wyliczanie kwoty netto na podstawie wzoru:
			// KwotaWWalucie*Kurs
			if (values.get("DWR_KWOTA_W_WALUCIE") != null && values.get("DWR_KURS") != null && values.get("DWR_KWOTA_W_WALUCIE").getMoneyData() != null && values.get("DWR_KURS").getData() != null)
				InvoiceLogicUtils.setNetto(values);

			// autoamtycznie wyliczenie kwoty brutto wg wzoru: netto+vat
			if (values.get(InvoiceLogicUtils.DWR_KWOTA_NETTO) != null && values.get(InvoiceLogicUtils.DWR_VAT).getData() != null)
				InvoiceLogicUtils.setBruttoAsNettoPlusVat(values);

			// data platnosci nie moze byc wczesniej niz data wystawienia
			msgBuilder.append(validateDate(values.get("DWR_DATA_WYSTAWIENIA"), values.get("DWR_TERMIN_PLATNOSCI"), DATA_PLATNOSCI_ERROR_MSG));
			
			// data wp�ywu nie mo�e by� wcze�niejsza ni� data wystawienia
			msgBuilder.append(validateDate(values.get("DWR_DATA_WYSTAWIENIA"), values.get("DWR_F_DATA_WPLYWU"), DATA_WPLYWU_ERROR_MSG));
			
			// data wp�ywu nie mo�e by� wcze�niejsza ni� data rejestracji
			msgBuilder.append(validateDate(values.get("DWR_F_DATA_WPLYWU"), new Date(), DATA_WPLYWU_TERAZ_ERROR_MSG));
			
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}

		if (msgBuilder.length()>0)
		{
			values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
			return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
		}
		return null;
	}
	

	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException {
		StringBuilder msgBuilder = new StringBuilder();
		
		BigDecimal totalCentrum = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
		// sumowanie kwot poszczegolnych pozycji budzetowych
		if (fieldValues.get("M_DICT_VALUES") != null)
			totalCentrum = sumBudgetItems(fieldValues, "M_DICT_VALUES");

		// suma kwot pozycji budzetowych musi byc rowna kwocie wniosku
		if (fieldValues.get("KWOTA_BRUTTO") != null && fieldValues.get("MPK") != null)
			checkTotalCentrum("KWOTA_BRUTTO", totalCentrum, fieldValues, msgBuilder);
		
		// data platnosci nie moze byc wczesniej niz data wystawienia
		msgBuilder.append(validateDate(fieldValues.get("DATA_WYSTAWIENIA"), fieldValues.get("TERMIN_PLATNOSCI"), DATA_PLATNOSCI_ERROR_MSG));
		
		// data wp�ywu nie mo�e by� wcze�niejsza ni� data wystawienia
		msgBuilder.append(validateDate(fieldValues.get("DATA_WYSTAWIENIA"), fieldValues.get("F_DATA_WPLYWU"), DATA_WPLYWU_ERROR_MSG));
		
		// data wp�ywu nie mo�e by� wcze�niejsza ni� data rejestracji
		msgBuilder.append(validateDate(fieldValues.get("F_DATA_WPLYWU"), new Date(), DATA_WPLYWU_TERAZ_ERROR_MSG));
		
		if (msgBuilder.length() > 0)
			throw new EdmException(msgBuilder.toString());
		
	}

	//	adds:
	// 		column.dockindBusinessAtr1 = Wnioskuj\u0105cy
	//		column.dockindBusinessAtr2 = Termin platnosci
	//		column.dockindBusinessAtr3 = Pozcyja bud\u017cetowa: bud\u017cet
	//		column.dockindBusinessAtr4 = Pozcyja bud\u017cetowa: kwota
	//		column.dockindBusinessAtr5 = Spos�b p�atno�ci
	public TaskListParams getTaskListParams(DocumentKind dockind, long id, TaskSnapshot task) throws EdmException
	{
		TaskListParams params = super.getTaskListParams(dockind, id, task);
		
		FieldsManager fm = dockind.getFieldsManager(id);
		
		// Kategoria - rodzaj faktury
		//params.setCategory((String) fm.getValue("RODZAJ"));

		// Nr faktury jako nr dokumentu
		params.setDocumentNumber((String) fm.getValue("NR_FAKTURY"));

		// Termin platnosci
		try
		{
			params.setAttribute(DateUtils.formatCommonDate((Date)fm.getValue("TERMIN_PLATNOSCI")).toString(), 1);
		}
		catch (Exception e)
		{
			log.error("B��d przy ustawianiu kolumny Termin p�atno�ci, listy zada�");
			params.setAttribute("Nie dotyczy", 1);
		}
		
		// Spos�b p�atno�ci
		try
		{
			params.setAttribute(fm.getEnumItem("SPOSOB") != null ? fm.getEnumItem("SPOSOB").getTitle() : "Nie okre�lony", 4);
		}
		catch (Exception e)
		{
			log.error("B��d przy ustawianiu kolumny Spos�b p�atno�ci, listy zada�");
			params.setAttribute("Nie dotyczy", 4);
		}
		return params;
	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue)
	{
		boolean assigned = false;

		// opis merytoryczny
		if (AssingUtils.OPIS_MERYTORYCZNY.equals(acceptationCn))
			assigned = assigneToEssentailDescription(doc, openExecution, assignable);
		// akceptacja pozycji budzetowych
		else if (AssingUtils.FINANCIAL_SECTION.equals(acceptationCn) || AssingUtils.MANAGER_OF_UNIT.equals(acceptationCn) || AssingUtils.MANAGER_OF_DEPARTMENT.equals(acceptationCn))
			assigned = AssingUtils.assigneeForMpkAcceptances(doc, assignable, openExecution, acceptationCn);
		// akceptacja DZP
		else if (AssingUtils.DZP_DEPARTMENT.equals(acceptationCn))
			assigned = AssingUtils.assigneeForDzpAcceptance(doc, assignable, openExecution, acceptationCn);
		
		return assigned;
	}

	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		super.setAdditionalTemplateValues(docId, values);
		/*try
		{
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();

			// document_id
			values.put("DOCUMENT_ID", doc.getId());

			// nr KO
			values.put("DOCUMENT_KO", doc.getOfficeNumber());
						
			// wystawca faktury
			values.put("SENDER", doc.getSender());

			// odbiorca faktury
			values.put("RECIPIENT", doc.getRecipients().get(0));

			// osoba generujaca
			values.put("USER", DSApi.context().getDSUser());

			// data wystawienia
			values.put("DATA_WYSTAWIENIA", DateUtils.formatCommonDate((Date) fm.getValue("DATA_WYSTAWIENIA")));

			// termin platnosci
			values.put("TERMIN_PLATNOSCI", DateUtils.formatCommonDate((Date) fm.getValue("TERMIN_PLATNOSCI")));

			// sender
			values.put("SENDER", doc.getSender());

			// recipient
			values.put("RECIPIENT", doc.getRecipients().get(0));

			// blokady
			values.put("BLOKADY", getBlockItems(fm));

			// pozycje budzetu
			values.put("BUDZET", getBudgetItems(fm));
			
			// pozycje umowy
			values.put("UMOWA", getContractItems(fm));

		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}*/
	}

	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	protected void checkTotalCentrum(String fieldName, BigDecimal totalCentrum, Map<String, ?> fieldValues, StringBuilder msgBuilder)
	{
		if (!((BigDecimal) fieldValues.get(fieldName)).equals(totalCentrum))
		{
			if (msgBuilder.length() == 0)
				msgBuilder.append("B��d: Suma kwot pozycji bud�etowych musi by� rowna kwocie brutto faktury.");
			else
				msgBuilder.append(";  Suma kwot pozycji bud�etowych musi by� rowna kwocie brutto faktury.");
		}
	}
	
	private List<ContractItem> getContractItems(FieldsManager fm)
	{
		ArrayList<ContractItem> ret = new ArrayList<ContractItem>();
		try
		{
			List<HashMap<String, Object>> pozycje = (List<HashMap<String, Object>>) fm.getValue("CONTRACT_ITEM");
			log.debug("******** * ** * * POZYCJE=" + pozycje);
			for (HashMap<String, Object> pozycja : pozycje)
			{
				log.debug("***********KeySet=" + pozycja.keySet());
				String nazwa = (String) pozycja.get("CONTRACT_ITEM_NAME");
				String opis = (String) pozycja.get("CONTRACT_ITEM_DESCR");
				String jednostka = (String) pozycja.get("CONTRACT_ITEM_UNIT");
				BigDecimal ilosc = (BigDecimal) pozycja.get("CONTRACT_ITEM_QUANTITY");
				BigDecimal netto = (BigDecimal) pozycja.get("CONTRACT_ITEM_NETAMOUNT");
				BigDecimal vat = (BigDecimal) pozycja.get("CONTRACT_ITEM_VATAMOUNT");

				log.debug("--------DODAJE:" + nazwa + " - " + opis + " - " + jednostka + " - " + ilosc + " - " + netto + " - " + vat);
				ContractItem newItem = new ContractItem(nazwa, opis, jednostka, ilosc.longValue(), netto, vat);
				ret.add(newItem);
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return ret;
	}

	private List<BlockItem> getBlockItems(FieldsManager fm)
	{

		ArrayList<BlockItem> ret = new ArrayList<BlockItem>();
		try
		{
			LinkedList<HashMap<String, Object>> blokady = (LinkedList<HashMap<String, Object>>) fm.getValue("BLOKADY");
			if (blokady == null)
				return ret;
			for (HashMap<String, Object> blokada : blokady)
			{

				BlockItem newItem = new BlockItem((String) blokada.get("BLOKADY_NR_WNIOSEK"), (Integer) blokada.get("BLOKADY_ANALYTICS_ID"));
				ret.add(newItem);
			}

		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return ret;
	}

	
	private void sumNettoVat(Map<String, FieldData> values)
	{
		BigDecimal netto = values.get("DWR_KWOTA_NETTO").getMoneyData();
		BigDecimal vat = values.get("DWR_VAT").getMoneyData();
		values.put("DWR_KWOTA_BRUTTO", new FieldData(Field.Type.MONEY, netto.add(vat)));

	}

	private List<Long> fillTableContractItemForContract(Long contractId, List<Long> contractItemIdsFromForm, Long docId) throws EdmException
	{
		
		Session session = null;
		Contract contract = null;
		boolean foundContractItem = false;
		
		try {
			session = DSApi.context().session();
			
	        session.beginTransaction();
	        
			contract = Contract.find(contractId);
			
			if (contract != null) {
				contract.setUsed(true);
				session.save(contract);
				
				List<Long> contractItemIds = contract.getContractItemsId();

				for(Long id : contractItemIdsFromForm) {
					ContractItemInvoice itemInv = ContractItemInvoice.find(id,docId);
					if (contractItemIds.contains(itemInv.getBaseId())) {
						foundContractItem = true; 
						break;
					}
				}
				List<Long> contractItemCreated = new ArrayList<Long>();

				if (!foundContractItem || contractItemIdsFromForm == null || contractItemIdsFromForm.size()==0) {
					for (ContractItem item : contract.getContractItems()) {
						ContractItemInvoice newItem = new ContractItemInvoice(docId,item);
						session.save(newItem);
						contractItemCreated.add(newItem.getId());
					}
				}
				session.getTransaction().commit();
				return contractItemCreated;
			}
		
		} catch (Exception e) {
			session.getTransaction().rollback();
			log.error(e.getMessage(), e);
		} 
	
		return null;

	}

	private void setContractItems(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		DSApi.openAdmin();
		
		// utworzenie listy id wybranych um�w z formatki
		List<Long> contractIdsValues = DwrUtils.extractIdFromDictonaryField(values, "CONTRACT");

		
		// utworzenie listy id wybranych pozycji umowy z formatki
		FieldData contractItemField = values.get("DWR_CONTRACT_ITEM");
		List<Long> contractItemIdsValues = DwrUtils.extractIdFromDictonaryField(values, "CONTRACT_ITEM");
		
		Long docId = fm.getDocumentId();
		
		
		if (contractIdsValues.size()>0) {
			List<Long> createdContractItemsId = fillTableContractItemForContract(contractIdsValues.get(0), contractItemIdsValues, docId);
			
			if (createdContractItemsId != null && createdContractItemsId.size()>0) {
				StringBuilder itemIds = new StringBuilder("");
				for (Long item : createdContractItemsId)
				{
					if (!itemIds.toString().equals(""))
					{
						itemIds.append(",");
					}
					itemIds.append(item);
				}

				values.put("DWR_CONTRACT_ITEM", new FieldData(Field.Type.STRING, itemIds.toString()));
			} else {
				updateContractItems(contractItemField,contractItemIdsValues);
			}
		} else {
			values.put("DWR_CONTRACT_ITEM", new FieldData(Field.Type.STRING, ""));
		}
		DSApi.close();
	}

	private void updateContractItems(FieldData contractItemField, List<Long> contractItemIdsValues) throws EdmException {
		Map<String,FieldData> dic = contractItemField.getDictionaryData();
		Session session = DSApi.context().session();
		Transaction tx = session.beginTransaction();
		tx.begin();
		
		try {
			Pattern p = Pattern.compile("CONTRACT_ITEM_ID_(\\d+)");	
			Matcher m = null;
			for (String fName : dic.keySet()) {
				 m = p.matcher(fName);
				 if (m.find() && !dic.get(fName).getData().toString().equals("")) {
					 String fieldId = m.group(1);
					 
					 Long itemId = Long.valueOf(dic.get(fName).getData().toString());
					 
					 ContractItemInvoice itemToUpdate = ContractItemInvoice.find(itemId);
					 if (dic.get("CONTRACT_ITEM_UNIT_"+fieldId).getData() != null) {
						 itemToUpdate.setUnit(dic.get("CONTRACT_ITEM_UNIT_"+fieldId).getData().toString());
					 }
					 if (dic.get("CONTRACT_ITEM_QUANTITY_"+fieldId).getIntegerData() != null) {
						 itemToUpdate.setQuantity(dic.get("CONTRACT_ITEM_QUANTITY_"+fieldId).getIntegerData().longValue());
					 }
					 if (dic.get("CONTRACT_ITEM_NETAMOUNT_"+fieldId).getMoneyData() != null) {
						 itemToUpdate.setNetAmount(dic.get("CONTRACT_ITEM_NETAMOUNT_"+fieldId).getMoneyData());
					 }
					 if (dic.get("CONTRACT_ITEM_VATAMOUNT_"+fieldId).getMoneyData() != null) {
						 itemToUpdate.setVatAmount(dic.get("CONTRACT_ITEM_VATAMOUNT_"+fieldId).getMoneyData());
					 }
					 session.update(itemToUpdate);
				 }
			}
			tx.commit();
		} catch (NumberFormatException e) {
			tx.rollback();
			log.error(e.toString());
		} catch (HibernateException e) {
			tx.rollback();
			log.error(e.toString());
		}
		session.close();
	}

	

	private void updateMpks(FieldData zapotrzItemField, StringBuilder idList) throws EdmException
	{
		String[] ids = idList.toString().split(",");
		List<String> idsList = new ArrayList<String>();

		Collections.addAll(idsList, ids); 
		Pattern p = Pattern.compile("MPK_ID_\\d+");
		Matcher m = null;
		Map<String, FieldData> dvalues = zapotrzItemField.getDictionaryData();
		for (String o : dvalues.keySet())
		{
			m = p.matcher(o);
			if (m.find() && dvalues.get(o).getData() != null  && StringUtils.isNotEmpty(dvalues.get(o).getData().toString()))
			{
				int index = Integer.parseInt(o.split("_ID_")[1]);
				Long mpkId = new Long(dvalues.get(o).getData().toString());
				if (idsList.contains(mpkId.toString()))
				{
					CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(mpkId);
					if (mpk != null)
					{
						updateMpk(dvalues, index, mpk);
					}
				}
			}
		}

	}
	
	private void updateMpk(Map<String, FieldData> dvalues, int index, CentrumKosztowDlaFaktury mpk) throws EdmException
	{
		DSApi.context().begin();
		try
		{
			Object centrumId = dvalues.get("MPK_CENTRUMID_" + index).getData();
			if (centrumId != null && !centrumId.equals(""))
				mpk.setCentrumId(Integer.parseInt(centrumId.toString()));
		}
		catch (Exception e)
		{
			log.warn("LAPIEMY :" + e.getMessage());
		}
		try
		{
			Object amount = dvalues.get("MPK_AMOUNT_" + index).getData();
			if (amount != null)
				mpk.setRealAmount(new BigDecimal(amount.toString()).setScale(2, RoundingMode.HALF_UP));
		}
		catch (Exception e)
		{
			log.warn("LAPIEMY :" + e.getMessage());
		}
		try
		{
			Object accountnumber = dvalues.get("MPK_ACCOUNTNUMBER_" + index).getData();
			if (accountnumber != null && !accountnumber.equals(""))
				mpk.setAccountNumber(accountnumber.toString());
		}
		catch (Exception e)
		{
			log.warn("LAPIEMY :" + e.getMessage());
		}
		try
		{
			Object accepCentrumId = dvalues.get("MPK_ACCEPTINGCENTRUMID_" + index).getData();
			if (accepCentrumId != null && !accepCentrumId.equals(""))
				mpk.setAcceptingCentrumId(Integer.parseInt(accepCentrumId.toString()));
		}
		catch (Exception e)
		{
			log.warn("LAPIEMY :" + e.getMessage());
		}
		try
		{
			Object analyticsId = dvalues.get("MPK_analytics_id_" + index).getData();
			if (analyticsId != null && !analyticsId.equals(""))
				mpk.setAnalyticsId(Integer.parseInt(analyticsId.toString()));
		}
		catch (Exception e)
		{
			log.warn("LAPIEMY :" + e.getMessage());
		}
		try
		{
			Object customysAgencyId = dvalues.get("MPK_CUSTOMSAGENCYID_" + index).getData();
			if (customysAgencyId != null && !customysAgencyId.equals(""))
				mpk.setCustomsAgencyId(Integer.parseInt(customysAgencyId.toString()));
		}
		catch (Exception e)
		{
			log.warn("LAPIEMY :" + e.getMessage());
		}
		try
		{
			Object subgroupId = dvalues.get("MPK_SUBGROUPID_" + index).getData();
			if (subgroupId != null && !subgroupId.equals(""))
				mpk.setSubgroupId(Integer.parseInt(subgroupId.toString()));
		}
		catch (Exception e)
		{
			log.warn("LAPIEMY :" + e.getMessage());
		}
		try
		{
			Object classTypeId = dvalues.get("MPK_CLASS_TYPE_ID_" + index).getData();
			if (classTypeId != null && !classTypeId.equals(""))
				mpk.setClassTypeId(Integer.parseInt(classTypeId.toString()));
		}
		catch (Exception e)
		{
			log.warn("LAPIEMY :" + e.getMessage());
		}
		try
		{
			Object connectedTypeId = dvalues.get("MPK_CONNECTED_TYPE_ID_" + index).getData();
			if (connectedTypeId != null && !connectedTypeId.equals(""))
				mpk.setConnectedTypeId(Integer.parseInt(connectedTypeId.toString()));
		}
		catch (Exception e)
		{
			log.warn("LAPIEMY :" + e.getMessage());
		}
		
		DSApi.context().commit();
	}	
	
	
	
	private boolean assigneToEssentailDescription(OfficeDocument doc, OpenExecution openExecution, Assignable assignable)
	{
		boolean assigned = false;
		try
		{
			if (!doc.getRecipients().isEmpty())
			{
				Long recipientId = doc.getRecipients().get(doc.getRecipients().size() - 1).getId();
				Person recipient = Person.find(recipientId.longValue());

				if (recipient.getFirstname() != null && recipient.getLastname() != null)
				{
					DSUser user = DSUser.findByFirstnameLastname(recipient.getFirstname(), recipient.getLastname());
					if (user != null)
					{
						assignable.addCandidateUser(user.getName());
						assigned = true;
						AssigneeHandler.addToHistory(openExecution, doc, user.getName(), null);
					}
				}
				else if (recipient.getOrganizationDivision() != null)
				{
					DSDivision division = DSDivision.findByName(recipient.getOrganizationDivision());
					if (division != null)
					{
						assignable.addCandidateGroup(division.getGuid());
						assigned = true;
						AssigneeHandler.addToHistory(openExecution, doc, null, division.getGuid());
					}
				}
			}
			if (!assigned)
			{
				Object recipient = openExecution.getVariable(ASSIGN_USER_PARAM);

				if (recipient != null)
				{
					assignable.addCandidateUser((String) recipient);
					assigned = true;
					AssigneeHandler.addToHistory(openExecution, doc, (String) recipient, null);
				}
				else
				{
					recipient = openExecution.getVariable(ASSIGN_DIVISION_GUID_PARAM);
					assignable.addCandidateGroup((String) recipient);
					assigned = true;
					AssigneeHandler.addToHistory(openExecution, doc, null, (String) recipient);
				}
			}
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		return assigned;
	}
	/** zwraca mape wartosci do wypelnienia comboboxu. Mapuje ID na nazwe uzytkownika (ujemne) lub dzialu (dodatnie). */
	private Map<String, String> findIdForAccept(DSDivision[] departments, String acceptanceName, boolean single,
			Map<String, String> enumValues, boolean financialSection) throws EdmException {

		if (enumValues == null) 
		{
			enumValues = new LinkedHashMap<String, String>();
		}

		if (!single)
		{
			enumValues.put("", sm.getString("select.wybierz"));
		}

		for (DSDivision div : departments) 
		{
			if (div == null) 
			{
				continue;
			}
			List<AcceptanceCondition> manDeptAcc = AcceptanceCondition.find(acceptanceName, div.getGuid());

			// id o warto�ci dodatniej definiuj� id tabeli ds_division, a id o warto�ci ujemnej definiuj� id tabeli ds_user
			int chooserAccSign = 0; 

			DSDivision manDept = null;
			DSUser manDeptUser = null;

			try 
			{
				if (manDeptAcc != null && manDeptAcc.size() > 0) 
				{
					if (manDeptAcc.get(0).getDivisionGuid() != null && !manDeptAcc.get(0).getDivisionGuid().equals("")) 
					{
						manDept = DSDivision.find(manDeptAcc.get(0).getDivisionGuid());
						chooserAccSign = 1;
					} 
					else if (manDeptAcc.get(0).getUsername() != null && !manDeptAcc.get(0).getUsername().equals("")) 
					{
						manDeptUser = DSUser.findByUsername(manDeptAcc.get(0).getUsername());
						chooserAccSign = -1;
					}
				}
			} catch (EdmException e) 
			{
				log.error("Nie znaleziono ds_division lub ds_user, id");
			}

			if (chooserAccSign != 0)
			{
				if (chooserAccSign > 0 && !manDept.isHidden())
				{
					enumValues.put(manDept.getId().toString(), manDept.getName());
				} 
				else if (!manDept.isHidden())
				{
					enumValues.put("-" + manDeptUser.getId().toString(), manDeptUser.getFirstname() + " " + manDeptUser.getLastname());
				}
			}

			if (single && enumValues.size() > 1) 
			{
				break;
			}
		}

		if (financialSection) 
		{
			if (enumValues.size() == 0) 
			{
				enumValues.put("", "brak");
			}
			else 
			{
				for (String key : enumValues.keySet())
				{
					if (!key.equals("")) 
					{
						String oneKey = key;
						enumValues = new LinkedHashMap<String, String>();
						enumValues.put(oneKey, "znaleziono");
						break;
					}
				}
			}
		}

		return enumValues;
	}
	
	private Map<String, String> insertEnumItemIntoTable(Map<String, String> enumValues, AcceptType type, Long centrumId, MpkAccept instance) 
	{
		int l = enumValues.containsKey("") ? 1 : 0;

		if (enumValues.size() > l) 
		{

			Session session = null;
			try 
			{
				session = DSApi.context().session();
				try 
				{
					DSApi.context().begin();
				} 
				catch (Exception e) 
				{
					// log.error(e.toString());
				}

				Map<String, String> enumValuesFinal = new LinkedHashMap<String, String>();

				if (l == 1) 
				{
					enumValuesFinal.put("", enumValues.get(""));
					enumValues.remove("");
				}

				for (String key : enumValues.keySet()) 
				{
					List<MpkAccept> found = instance.find(type, key, centrumId);

					if (found == null) 
					{
						MpkAccept toAdd = null;
						
						if (type == AcceptType.FINANCIAL_SECTION)
						{
							toAdd = new MpkFinancialSection();
						} 
						else if (type == AcceptType.MANAGER_OF_DEPARTMENT) 
						{
							toAdd = new MpkManagerOfDepartment();
						} 
						else if (type == AcceptType.MANAGER_OF_UNIT) 
						{
							toAdd = new MpkManagerOfUnit();
						}

						toAdd.setAvailable(true);
						toAdd.setCentrum(0);
						toAdd.setCn(key);
						toAdd.setRefValue(centrumId);
						toAdd.setTitle(enumValues.get(key));
						session.save(toAdd);

						enumValuesFinal.put(toAdd.getId().toString(), toAdd.getTitle());
					} 
					else 
					{
						MpkAccept toShow;
						toShow = found.get(0);
						enumValuesFinal.put(toShow.getId().toString(), toShow.getTitle());
					}
				}

				DSApi.context().commit();
				DSApi.context().begin();

				return enumValuesFinal;
			} 
			catch (Exception e)
			{
				try 
				{
					DSApi.context().rollback();
				} 
				catch (EdmException e1) 
				{
					log.error(e.getMessage(), e1);
				}
				log.error(e.getMessage(), e);
			}
		}
		return enumValues;
	}
	
	private void fillStawkiVat_Obciazenie(String dictionaryName, Map<String, FieldData> values, Map<String, Object> results)
	{
		if (dictionaryName.equals("STAWKI_VAT"))
		{
			Object netto = values.get("STAWKI_VAT_NETTO").getData();
			if (netto == null)
				return;
			
			Object stawkaVat = values.get("STAWKI_VAT_STAWKA").getData();
			if (stawkaVat == null)
				return;
			
			try
			{
				EnumItem stawkaVatItem = DataBaseEnumField.getEnumItemForTable("simple_erp_per_docusafe_vatstaw_view", Integer.parseInt(stawkaVat.toString()));
				BigDecimal stawkVatB = new BigDecimal (stawkaVatItem.getCn());
				BigDecimal nettoB = new BigDecimal (netto.toString());
				BigDecimal kwotaVatB = null;
				BigDecimal bruttoB = nettoB;
				if (stawkVatB.compareTo(BigDecimal.ZERO) == 1)
				{
					kwotaVatB = nettoB.multiply(stawkVatB.divide(new BigDecimal(100).setScale(2), RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_UP)).setScale(2, RoundingMode.HALF_UP);
					bruttoB = nettoB.add(kwotaVatB);
				}
				else
					kwotaVatB = BigDecimal.ZERO;
				
				results.put("STAWKI_VAT_KWOTA_VAT", kwotaVatB);
				results.put("STAWKI_VAT_BRUTTO",  bruttoB);
				
				Object dzialalnosc = values.get("STAWKI_VAT_DZIALALNOSC").getData();
				if (nettoB != null && dzialalnosc != null) 
				{
					if (kwotaVatB != null && dzialalnosc.equals("343"))
					{
						results.put("STAWKI_VAT_OBCIAZENIE", nettoB.setScale(2, RoundingMode.HALF_UP));
					}
					else if (dzialalnosc.equals("340") || dzialalnosc.equals("341"))
					{
						results.put("STAWKI_VAT_OBCIAZENIE", bruttoB.setScale(2, RoundingMode.HALF_UP));
					}
					else
					{
						String wskaznik = Docusafe.getAdditionProperty("stawkavat.wskaznik");
						if (StringUtils.isBlank(wskaznik))
							wskaznik = "86";
						BigDecimal wskaznikBigDecimal = new BigDecimal(wskaznik).setScale(2,RoundingMode.HALF_UP).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
						BigDecimal vatDlaWskaznika = kwotaVatB.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ONE : kwotaVatB;
						//86%vatu
						BigDecimal procentVatuStawka = vatDlaWskaznika.multiply(wskaznikBigDecimal);
						BigDecimal obciazenie = nettoB.add(procentVatuStawka);
						results.put("STAWKI_VAT_OBCIAZENIE", obciazenie.setScale(2, RoundingMode.HALF_UP));
					}
				}
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
			}

		}
	}
	
	private void setBlocadesWZP_WMU(Map<String, FieldData> values, FieldsManager fm, String type, String numberName) throws Exception
	{
		String number = null;
		if (type.equalsIgnoreCase("wzp"))
		{
			number = getContractTenderSignature(values);
			if (number == null) return;
		}
		else if (type.equalsIgnoreCase("wmu"))
			number = values.get("DWR_NR_UMOWY_RK").getStringData();
		else
			return;
		
		boolean isOpened = true;
		try
		{
			if (!DSApi.isContextOpen())
			{   
				isOpened = false; 
				DSApi.openAdmin();    
			}
			
			NativeCriteria nc = preapreCriteria(type, numberName, number);
			if (nc == null)
				return;
			// pobranie i wy�wietlenie wynik�w
			CriteriaResult cr = nc.criteriaResult();
			while (cr.next())
			{
				setBlocades(values, cr);
				break;
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		
		finally
		{
			if (DSApi.isContextOpen() && !isOpened) 
			{ 
				DSApi.close();    
			}
		}
	}

	private NativeCriteria preapreCriteria(String type, String numberName, String number)  throws Exception
	{
		
		NativeCriteria nc = DSApi.context().createNativeCriteria("dsg_pcz_application_" + type.toLowerCase(), "d");
			nc.setProjection(NativeExps.projection()
					// ustalenie kolumn
					.addProjection("d.document_id")
					.addProjection("d." + numberName.toLowerCase()))
				// warunek where
				.add(NativeExps.eq("d." + numberName.toLowerCase(), number))
				// order by i limit
				.addOrder(NativeExps.order().add("d.document_id", OrderType.DESC));
		
		return nc;
	}

	private void setBlocades(Map<String, FieldData> values, CriteriaResult cr) throws EdmException, DocumentNotFoundException
	{
		Long documentId =  cr.getLong(0, null);
		
		OfficeDocument doc = OfficeDocument.find(documentId);
		
		List<Long> mpkIds = (List<Long>)doc.getFieldsManager().getKey("MPK");
					
		String ids = DwrUtils.longListToComaString(mpkIds);
		
		values.put("DWR_BLOKADY", new FieldData(Field.Type.STRING, ids));
	}

	private String getContractTenderSignature(Map<String, FieldData> values) throws EdmException
	{
		String number;
		List<Long> contractIdsValues = DwrUtils.extractIdFromDictonaryField(values, "CONTRACT");
		
		if (contractIdsValues.size() == 0 )
			return null;
			
		Long contractId = contractIdsValues.get(0);
		Contract contract = Contract.find(contractId);
		number = contract.getTenderSignature();
		return number;
	}
}
