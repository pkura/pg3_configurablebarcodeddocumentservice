package pl.compan.docusafe.parametrization.polcz;

public class ApplicationWMTLogic  extends AbstractApplicationLogic
{
	private static ApplicationWMTLogic instance;
	
	public static ApplicationWMTLogic getInstance()
	{
		if (instance == null)
			instance = new ApplicationWMTLogic();
		return instance;
	}
}
