package pl.compan.docusafe.parametrization.polcz;

public class ImportAnswer {
	
	private Long applicationId;
	private Integer result;
	private String error;
	
	ImportAnswer(Long applicationId, Integer result, String error) {
		this.applicationId = applicationId;
		this.result = result;
		this.error = error;
	}
	public Long getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}
	public Integer getResult() {
		return result;
	}
	public void setResult(Integer result) {
		this.result = result;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	

}
