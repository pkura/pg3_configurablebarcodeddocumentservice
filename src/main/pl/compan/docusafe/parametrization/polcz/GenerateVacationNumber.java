package pl.compan.docusafe.parametrization.polcz;

import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.jbpm.api.activity.ActivityExecution;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import java.util.Map;

public class GenerateVacationNumber implements ExternalActivityBehaviour 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;

	
	/**
	 * Logger
	 */ 
	private static final Logger log = LoggerFactory.getLogger(GenerateVacationNumber.class);
	
	public void execute(ActivityExecution execution) throws Exception 
	{
		
    	Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Document document = Document.find(docId);
		
	}
	
	public void signal(ActivityExecution execution, String signalName, Map<String, ?> parameters)
	throws Exception {
		
	}
}
