package pl.compan.docusafe.parametrization.polcz;

public class ApplicationWZWKLogic extends AbstractApplicationLogic
{
	private static ApplicationWZWKLogic instance;
	
	public static ApplicationWZWKLogic getInstance()
	{
		if (instance == null)
			instance = new ApplicationWZWKLogic();
		return instance;
	}
	
}