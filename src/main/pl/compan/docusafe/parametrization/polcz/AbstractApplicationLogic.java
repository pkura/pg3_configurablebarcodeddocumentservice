package pl.compan.docusafe.parametrization.polcz;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.FolderResolver;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

public abstract class AbstractApplicationLogic extends AbstractPczDocumentLogic
{
	protected static Logger log = LoggerFactory.getLogger(AbstractApplicationLogic.class);

	/*
	 * Lista akceptacji specjalnych
	 */
	protected final static String ESSENTIAL_DESCRIPTION = "essential_description";
	protected final static String FINANCIAL_CONFIRMATION = "financial_confirmation";
	protected final static String CORRECTION_AUTHOR_COORDINATOR = "correctionAuthorOrCoordinator";
	protected final String DEANERY = "deanery";

	protected final static String DWR_KWOTA_NETTO = "DWR_KWOTA_NETTO";
	protected final static String DWR_KWOTA_BRUTTO = "DWR_KWOTA_BRUTTO";
	protected final static String DWR_STAWKA_VAT = "DWR_STAWKA_VAT";

	protected static final String DWR_KWOTA_W_WALUCIE = "DWR_KWOTA_W_WALUCIE";
	protected static final String DWR_RATE = "DWR_RATE";
	protected static final String DWR_KWOTA_PLN = "DWR_KWOTA_PLN";
	
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		Map<String, Object> toReload = Maps.newHashMap();
		for (Field f : fm.getFields())
			if (f.getDefaultValue() != null)
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));

		DSUser user = DSApi.context().getDSUser();
		toReload.put("WNIOSKODAWCA", user.getId());
		if (user.getDivisionsWithoutGroup().length > 0)
			toReload.put("WORKER_DIVISION", fm.getField("WORKER_DIVISION").getEnumItemByCn(user.getDivisionsWithoutGroupPosition()[0].getGuid()).getId());
		toReload.put("DATA_WYPELNIENIA", new Date());
		fm.reloadValues(toReload);
	}

	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
	{
		StringBuilder msgBuilder = new StringBuilder();
		BigDecimal totalCentrum = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);

		// kwota netto nie moze byc wieksza niz kwota brutto
		if (fieldValues.get("KWOTA_NETTO") != null && fieldValues.get("KWOTA_BRUTTO") != null)
			checkNettoOverBrutto(fieldValues, msgBuilder);

		// sumowanie kwot poszczegolnych pozycji budzetowych dnb
		if (fieldValues.get("M_DICT_VALUES") != null)
			totalCentrum = sumBudgetItems(fieldValues, "M_DICT_VALUES");

		// suma kwot pozycji budzetowych musi byc rowna kwocie wniosku
		if (fieldValues.get("KWOTA") != null && fieldValues.containsKey("MPK") && fieldValues.get("MPK") != null)
			checkTotalCentrum("KWOTA", totalCentrum, fieldValues, msgBuilder);
		else if (fieldValues.get("KWOTA_PLN") != null && fieldValues.containsKey("MPK") && fieldValues.get("MPK") != null)
			checkTotalCentrum("KWOTA_PLN", totalCentrum, fieldValues, msgBuilder);
		else if (fieldValues.get("KWOTA_BRUTTO") != null)
			checkTotalCentrum("KWOTA_BRUTTO", totalCentrum, fieldValues, msgBuilder);

		// w przypadku wniosku utworzenia.zmiany um�w cywilno-prawnych
		// potrzeba
		// sprawdzi� czy data zawarcia nie jest p�niejsza ni� data realizacji
		if (kind.getCn().equals("application_wucp"))
			checkDate(fieldValues, msgBuilder);

		if (msgBuilder.length() > 0)
			throw new EdmException(msgBuilder.toString());
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
	{
		StringBuilder msgBuilder = new StringBuilder();

		// kwota netto nie moze byc wieksza niz kwota brutto
		if (DwrUtils.isNotNull(values, DWR_KWOTA_NETTO, DWR_KWOTA_BRUTTO))
			validateNettoBrutto(values, msgBuilder);
		
		if (msgBuilder.length() > 0)
		{
			values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
			return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
		}
		return null;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
		String documentKindName = document.getDocumentKind().getName();

		perms.add(new PermissionBean(ObjectPermission.READ, "PCZ_" + documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, "PCz - " + documentKindName + " - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "PCZ_" + documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "PCz - " + documentKindName + " zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "PCZ_" + documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "PCz - " + documentKindName + " - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "PCZ_" + documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "PCz - " + documentKindName + " zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "PCZ_" + documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "PCz - " + documentKindName + " - usuwanie"));
		Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
		perms.addAll(documentPermissions);
		this.setUpPermission(document, perms);
	}

	public TaskListParams getTaskListParams(DocumentKind dockind, long id, TaskSnapshot task) throws EdmException
	{
		TaskListParams params = super.getTaskListParams(dockind, id, task);
		FieldsManager fm = dockind.getFieldsManager(id);

		// wnioskujacy
		if (fm.getValue("WNIOSKODAWCA") != null) {
			params.setAttribute((String)fm.getValue("WNIOSKODAWCA"), 0);
		}
		
		// Nr wniosku jako nr dokumentu
		params.setDocumentNumber((String) fm.getValue("NR_WNIOSEK"));

		return params;
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		log.info("--- AbstractApplicationLogic : START PROCESS !!! ---- {}", event);
		
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			String divisionCn = document.getFieldsManager().getEnumItemCn("WORKER_DIVISION");
			divisionCn = DSDivision.find(divisionCn).getCode();
			String documentCn = document.getDocumentKind().getCn().split("_")[1];
			String nrWniosek = document.getId() + "/" + documentCn.toUpperCase() + "-" + divisionCn + "/" + DateUtils.formatYear(new Date());
			Map<String, Object> fieldValues = new HashMap<String, Object>();
			fieldValues.put("NR_WNIOSEK", nrWniosek);
			document.getDocumentKind().setOnly(document.getId(), fieldValues);
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

			java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

			DSUser author = DSApi.context().getDSUser();
			String user = author.getName();
			String fullName = author.getLastname() + " " + author.getFirstname();

			perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

			Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
			perms.addAll(documentPermissions);
			this.setUpPermission(document, perms);

			DSApi.context().watch(URN.create(document));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue)
	{
		boolean assigned = false;
		log.debug("Start special assignee operations for acceptationCn: {}, fieldCnValue: {}", acceptationCn, fieldCnValue);

		// tylko WAH, WBW, WUCP, WW
		if (CORRECTION_AUTHOR_COORDINATOR.equalsIgnoreCase(acceptationCn))
		{
			assigned = assigneeToAuthorOrCoordinator(doc, assignable, openExecution, acceptationCn);
		}
		// dziekanat
		if (DEANERY.equals(acceptationCn))
		{

			String guidDziekan;
			try
			{
				guidDziekan = doc.getFieldsManager().getEnumItemCn(fieldCnValue);
				log.info("guidDziekan = " + guidDziekan);
				String guidDziekanat = null;
				for (DSDivision division : DSDivision.find(guidDziekan).getChildren())
				{
					log.info("division.getCode() = {}", division.getCode());
					if (division.getCode() != null && division.getCode().endsWith("-D"))
						guidDziekanat = division.getGuid();
				}
				log.info("guidDziekanat = {}", guidDziekanat);
				for (AcceptanceCondition accept : AcceptanceCondition.find(acceptationCn, guidDziekanat))
				{
					if (accept.getUsername() != null)
						assignable.addCandidateUser(accept.getUsername());
					else if (accept.getDivisionGuid() != null)
						assignable.addCandidateGroup(accept.getDivisionGuid());

					assigned = true;
				}
			}
			catch (EdmException e)
			{
				log.error(e.getMessage(), e);
			}
		}
		return assigned;
	}

	// dop�ki nie zostanie naprawiony podstawowy setAdditionalTemplateValues
	// doda�em now� metod� wywo�uj�c� setAdditionalTemplateValues z AbstractPczDocumentLogic
	public void setAdditionalTemplateValuesBasic(long docId, Map<String, Object> values) {
		super.setAdditionalTemplateValues(docId, values);
	}
	
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		try
		{
			Document doc = Document.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			OfficeDocument dokument = (OfficeDocument) doc;
			// czas wygenerowania metryki
			values.put("GENERATE_DATE", DateUtils.formatCommonDate(new java.util.Date()).toString());

			// rok dokumentu
			values.put("YEAR", DateUtils.formatYear(doc.getCtime()).toString());

			// data dokumentu
			values.put("DATE", DateUtils.formatCommonDate(doc.getCtime()));

			// osoba generujaca wniosek
			values.put("USER", DSApi.context().getDSUser());

			// wnioskodawca
			values.put("WNIOSKODAWCA", fm.getEnumItem("WNIOSKODAWCA").getTitle());
			
			
			// jednostka wnioskuj�cego
			values.put("WORKER_DIVISION", fm.getEnumItem("WORKER_DIVISION").getTitle());
			
//			//osoba zamiast usera
//			values.put("USER", DSApi.context().getDSUser());

			if (fm.containsField("DATA_UST_WART") && fm.getValue("DATA_UST_WART") != null)
			{
				// data ustalenia wartosci zamowienia

				values.put("DATA_UST", DateUtils.formatCommonDate((Date) fm.getValue("DATA_UST_WART")));
			}

			// czy zmiana czy umowa
			if (fm.containsField("RODZAJ") && fm.getEnumItem("RODZAJ")!= null)
			{
				values.put("RODZAJ", fm.getEnumItem("RODZAJ").getTitle());
				log.error("rodzaj: " + fm.getEnumItem("RODZAJ"));
				log.error("rodzaj: " + fm.getEnumItem("RODZAJ").getTitle());

				// czy "aneksu" czy "umowy"
				String rodzaj = fm.getEnumItem("RODZAJ").getTitle();
				if (rodzaj.equals("Zmiana"))
				{
					values.put("RODZAJ_ANEKS", "aneksu");
					values.put("KWOTA_UMOWA_ANEKS", fm.getValue("KWOTA_ANEKSU"));
					values.put("START_UMOWA_ANEKS", DateUtils.formatCommonDate((Date) fm.getValue("ANEKS_START_DATE")).toString());
					values.put("END_UMOWA_ANEKS", DateUtils.formatCommonDate((Date) fm.getValue("ANEKS_FINISH_DATE")).toString());
				}
				else if (rodzaj.equals("Umowa"))
				{
					values.put("RODZAJ_ANEKS", "umowy");
					values.put("KWOTA_UMOWA_ANEKS", fm.getValue("KWOTA_UMOWY"));
					values.put("START_UMOWA_ANEKS", DateUtils.formatCommonDate((Date) fm.getValue("START_DATE")).toString());
					values.put("END_UMOWA_ANEKS", DateUtils.formatCommonDate((Date) fm.getValue("FINISH_DATE")).toString());
				}
				else
				{
					values.put("RODZAJ_ANEKS", " ");
					values.put("KWOTA_UMOWA_ANEKS", 0);
					values.put("START_UMOWA_ANEKS", " ");
					values.put("END_UMOWA_ANEKS", " ");
				}
			}

			// rodzaj procedury wewn�trznej
			if (fm.containsField("RODZAJ_PROC_WEW"))
			{
				values.put("RODZAJ_WEW", fm.getEnumItem("RODZAJ_PROC_WEW").getTitle());
			}

			if (fm.containsField("CONTRACTOR"))
			{
				ContractorItem ci = getContractor(fm);
				values.put("CONTRACTOR_XXX", ci);
			}

			// pozycje bud�etowe
			if (fm.containsField("MPK"))
			{
				List<BudgetItem> budz = getBudgetItems(fm);
				values.put("BUDGET", budz);
			}

			// koordynator
			if (fm.containsField("KOORDYNATOR") && fm.getEnumItem("KOORDYNATOR")!= null)
			{
				EnumItem enumItem = fm.getEnumItem("KOORDYNATOR");
				DSUser koordynator = DSUser.findById(enumItem.getId().longValue());
				values.put("KOORDYNATOR", koordynator.getWholeName());
			}

			// termin realizacji
			try
			{
				if (fm.containsField("TYP_TERMINU") && fm.getValue("TYP_TERMINU") != null) {
					HashMap<String, Object> termin = new HashMap<String, Object>();
					EnumItem typTerminu = fm.getEnumItem("TYP_TERMINU");
					switch (typTerminu.getId()) {
					case 1151:
						termin = (HashMap<String, Object>) fm.getValue("TERMIN_OD_PODPISANIA");
						BigDecimal terminIle = (BigDecimal) termin.get("TERMIN_OD_PODPISANIA_ILE");
						log.error("TERMIN_OD_PODPISANIA=" + termin.get("TERMIN_OD_PODPISANIA_JM"));
						EnumValues tempValues = (EnumValues) termin.get("TERMIN_OD_PODPISANIA_JM");
						String terminCzego = tempValues.getSelectedOptions().get(0);
						int idTer = Integer.parseInt(terminCzego);
						switch (idTer) {
						case 20121:
							terminCzego = "dni";
							break;
						case 20122:
							terminCzego = "miesi�cy";
							break;
						case 20123:
							terminCzego = "lat";
							break;
						default:
							terminCzego = "";
						}
						values.put("TERMIN", terminIle + " " + terminCzego + " od podpisania umowy");
						break;
					case 1152:
						termin = (HashMap<String, Object>) fm.getValue("TERMIN_OD_DO");
						Date from = (Date) termin.get("TERMIN_OD_DO_OD");
						Date to = (Date) termin.get("TERMIN_OD_DO_DO");
						values.put("TERMIN", "od " + DateUtils.formatCommonDate(from) + " do " + DateUtils.formatCommonDate(to));
						break;
					case 1153:
						termin = (HashMap<String, Object>) fm.getValue("TERMIN_DATA_ZAKONCZENIA");
						Date koniec = (Date) termin.get("TERMIN_DATA_ZAKONCZENIA_OD");
						values.put("TERMIN", DateUtils.formatCommonDate(koniec));
						break;
					case 1154:
						termin = (HashMap<String, Object>) fm.getValue("TERMIN_INNY");
						String inny = (String) termin.get("TERMIN_INNY_INNY");
						values.put("TERMIN", inny);
						break;
					}
				}
				
				// czas realizacji
//				if (fm.containsField("TERMIN") && fm.getValue("TERMIN") != null)
//				{
//					log.error("TERMIN=" + fm.getValue("TERMIN"));
//
//					HashMap<String, Object> termin = (HashMap<String, Object>) fm.getValue("TERMIN");
//					BigDecimal terminIle = (BigDecimal) termin.get("TERMIN_ILE");
//					log.error("TERMIN=" + termin.get("TERMIN_JM"));
//					EnumValues tempValues = (EnumValues) termin.get("TERMIN_JM");
//					String terminCzego = tempValues.getSelectedOptions().get(0);
//					int idTer = Integer.parseInt(terminCzego);
//					switch (idTer)
//					{
//						case 20121:
//							terminCzego = "dni";
//							break;
//						case 20122:
//							terminCzego = "miesi�cy";
//							break;
//						case 20123:
//							terminCzego = "lat";
//							break;
//						default:
//							terminCzego = "";
//					}
//					values.put("TERMIN", terminIle + " " + terminCzego + " od podpisania umowy");
//				}
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
			}
			if (fm.containsField("TERMIN_REALIZACJI"))
			{
				values.put("TERMIN_REALIZACJI", DateUtils.formatCommonDate((Date) fm.getValue("TERMIN_REALIZACJI")));
			}

			// Numer cpv
			if (fm.containsField("CPVNR"))
			{
				values.put("NUMER", getCpvItems(fm));
			}

			// kraj
			if (fm.containsField("KRAJ"))
			{
				values.put("KRAJ", fm.getValue("KRAJ"));
			}

			// uczestnicy
			if (fm.containsField("UCZESTNICY"))
			{
				// EnumItem kraj =
				// DataBaseEnumField.getEnumItemForTable("dsr, id)
				List<UczestnikItem> uczestnicy = new ArrayList<UczestnikItem>();
				for (String uczestnik : (List<String>) fm.getValue("UCZESTNICY"))
					uczestnicy.add(new UczestnikItem(uczestnik));
				values.put("UCZESTNICY", uczestnicy);
				log.error("UCZESTNICY" + uczestnicy);
			}

			// przewidywany termin rozliczenia
			if (fm.containsField("PRZE_TERMIN_ROZLICZENIA"))
			{
				values.put("PRZE_TERMIN_ROZLICZENIA", DateUtils.formatCommonDate((Date) fm.getValue("PRZE_TERMIN_ROZLICZENIA")));
			}

			// rodzaj umowy
			if (fm.containsField("RODZAJ_UMOWA"))
			{
				values.put("RODZAJ_UMOWA", fm.getValue("RODZAJ_UMOWA"));

			}
			// numer projektu NR_PROJEKTU
			if (fm.containsField("NR_PROJEKTU"))
			{
				values.put("NR_PROJEKTU", fm.getValue("NR_PROJEKTU"));
			}

			// grupa GRUPA_UMOWA
			if (fm.containsField("GRUPA_UMOWA"))
			{
				values.put("GRUPA_UMOWA", fm.getValue("GRUPA_UMOWA"));
			}

			// wniosek wyjazdowy
			if (fm.containsField("WNIOSEK_WYJAZDOWY"))
			{
				HashMap<String, Object> wniosek = (HashMap<String, Object>) fm.getValue("WNIOSEK_WYJAZDOWY");
				Document docWniosek = Document.find(Long.parseLong((String) wniosek.get("id")));
				FieldsManager fmw = docWniosek.getFieldsManager();
				String numerWniosek = (String) fmw.getValue("NR_WNIOSEK");
				String typWniosek = docWniosek.getTitle();

				values.put("WNIOSEK_WYJ_NR", numerWniosek);
				values.put("WNIOSEK_WYJ_TYP", typWniosek);

				log.error("**************" + numerWniosek + " " + typWniosek);
			}

			// wniosek o zaliczk�
			if (fm.containsField("WNIOSEK_ZALICZKA"))
			{
				HashMap<String, Object> zaliczka = (HashMap<String, Object>) fm.getValue("WNIOSEK_ZALICZKA");
				Document docZaliczka = Document.find(Long.parseLong((String) zaliczka.get("id")));
				FieldsManager fmz = docZaliczka.getFieldsManager();
				String numerZaliczka = (String) fmz.getValue("NR_WNIOSEK");
				String typZaliczka = docZaliczka.getTitle();

				values.put("WNIOSEK_ZAL_NR", numerZaliczka);
				values.put("WNIOSEK_ZAL_TYP", typZaliczka);

				log.error("**************" + numerZaliczka + " " + typZaliczka);
			}

			// srodki transportu
			if (fm.containsField("KOSZTY_PODROZY"))
			{
				log.error("KOSZTY PODROZY:" + getPodrozItems(fm).get(0).getMiPr());
				values.put("ST", getPodrozItems(fm));
			}

			// osoba
			if (fm.containsField("OSOBA"))
			{
				try
				{
					HashMap<String, Object> osoba = (HashMap<String, Object>) fm.getValue("OSOBA");
					log.error("OSOBA=" + osoba);
					values.put("O_IMIE", osoba.get("OSOBA_IMIE"));
					values.put("O_NAZWISKO", osoba.get("OSOBA_NAZWISKO"));
					values.put("O_OJCIEC", osoba.get("OSOBA_OJCIEC"));
					values.put("O_MATKA", osoba.get("OSOBA_MATKA"));
					values.put("O_URODZENIE", osoba.get("OSOBA_URODZENIE"));
					values.put("O_DATA_URODZENIA", DateUtils.formatCommonDate((Date) osoba.get("OSOBA_DATA_URODZENIA")));
					values.put("O_PESEL_NIP", osoba.get("OSOBA_PESEL_NIP"));
					values.put("O_ZAMIESZKALY", osoba.get("OSOBA_ZAMIESZKALY"));
					values.put("O_ULICA", osoba.get("OSOBA_ULICA"));
					values.put("O_NR_DOMU", osoba.get("OSOBA_NR_DOMU"));
					values.put("O_NR_MIESZKANIA", osoba.get("OSOBA_NR_MIESZKANIA"));
					values.put("O_GMINA", osoba.get("OSOBA_GMINA"));
					values.put("O_KOD_POCZTOWY", osoba.get("OSOBA_KOD_POCZTOWY"));
					values.put("O_MIASTO", osoba.get("OSOBA_MIASTO"));
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
				}
			}
			
			// akceptacje
			setAcceptances(docId, values);
			
			//zleceniobiorca
			if(fm.containsField("SENDER"))
			{
				Sender s = dokument.getSender();
//				HashMap<String, Object> sender = (HashMap<String, Object>) fm.getValue("SENDER");
				values.put("SENDER", s);
				values.put("S_ORGANIZATION", s.getOrganization());
				values.put("S_STREET", s.getStreet());
				values.put("S_ZIP", s.getZip());
				values.put("S_LOCATION", s.getLocation());
				
				log.error("SENDER: " + fm.getValue("SENDER"));
				log.error("CN's: " + fm.getFieldsCns());
				log.error("SENDER KEY " + fm.getFieldsCns());
				log.error("CN's: " + fm.getFieldsCns());
			}
			
//			if(fm.containsField("WARUNKI")){
			try{
				log.error("WARUNKI: " + fm.getValue("WARUNKI"));
				log.error("WARUNKI: " + fm.getField("WARUNKI"));
				String warunki = (String) fm.getValue("WARUNKI");
				log.error("WARUNKI: " + warunki);
				values.put("WAR", warunki);
			}catch(Exception e){
				log.error(e.getMessage(), e);
			}
//			}
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}

	}

	public ArrayList<PodrozItem> getPodrozItems(FieldsManager fm)
	{
		ArrayList<PodrozItem> ret = new ArrayList<PodrozItem>();
		try
		{
			List<HashMap<String, Object>> lista = (List<HashMap<String, Object>>) fm.getValue("KOSZTY_PODROZY");
			for (HashMap<String, Object> transport : lista)
			{
				String from = (String) transport.get("KOSZTY_PODROZY_FROM_PLACE");
				String to = (String) transport.get("KOSZTY_PODROZY_TO_PLACE");
				String start = DateUtils.formatCommonDateTime((Date) transport.get("KOSZTY_PODROZY_FROM_DATE"));
				String end = DateUtils.formatCommonDateTime((Date) transport.get("KOSZTY_PODROZY_TO_DATE"));
				String srodki = (String) transport.get("KOSZTY_PODROZY_TRANSPORT");
				ret.add(new PodrozItem(from, to, srodki, start, end));
				log.error("transport" + transport);
			}

		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return ret;
	}

	public ArrayList<NumerCpvItem> getCpvItems(FieldsManager fm)
	{
		try
		{
			ArrayList<NumerCpvItem> ret = new ArrayList<NumerCpvItem>();
			List<HashMap<String, Object>> numery = (List<HashMap<String, Object>>) fm.getValue("CPVNR");
			for (HashMap<String, Object> numer : numery)
			{
				ret.add(new NumerCpvItem((String) numer.get("CPVNR_CN"), (String) numer.get("CPVNR_TITLE")));
				log.error("NUMER CPV=" + numer);
			}
			return ret;
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return null;

	}

	public ArrayList<BudgetItem> getBudgetItems(FieldsManager fm)
	{
		ArrayList<BudgetItem> ret = new ArrayList<BudgetItem>();
		try
		{
			List<Long> budzet = (List<Long>) fm.getKey("MPK");
			if (budzet != null)
			{
				log.error("********LISTA=" + budzet);
				for (Long idMpk : budzet)
				{
					CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(idMpk);
					EnumItem centrum = DataBaseEnumField.getEnumItemForTable("simple_erp_per_docusafe_bd_str_budzet_view", mpk.getCentrumId());
					EnumItem account = DataBaseEnumField.getEnumItemForTable("simple_erp_per_docusafe_bd_pozycje_view", Integer.parseInt(mpk.getAccountNumber()));
					EnumItem accept = DataBaseEnumField.getEnumItemForTable("simple_erp_per_docusafe_bd_zrodla_view", mpk.getAcceptingCentrumId());
					BigDecimal kwota = mpk.getRealAmount();
					// int location = mpk.getLocationId();
					log.error("***************POZYCJA CENTRUM:" + centrum.getTitle());
					log.error("***************POZYCJA ACCOUNT:" + account.getTitle());
					log.error("***************POZYCJA ACCEPT:" + accept.getTitle());
					log.error("***************POZYCJA KWOTA:" + kwota);
					log.error("***************POZYCJA LOCATION:" + mpk.getLocationId());
					log.error("***************POZYCJA ANALYTICS:" + mpk.getAnalyticsId());
					BudgetItem newItem = new BudgetItem(centrum.getTitle(), account.getTitle(), accept.getTitle(), "", kwota, 0);
					ret.add(newItem);
				}
			}
			// CentrumKosztowDlaFaktury mpk =
			// CentrumKosztowDlaFaktury.getInstance().find(1l);
			// mpk.getCentrumId();
			// EnumItem ei =
			// DataBaseEnumField.getEnumItemForTable("simple_erp_per_docusafe_bd_str_budzet_view",
			// mpk.getCentrumId());
			// ei.getTitle();
			//
			// mpk.getAccountNumber();
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return ret;
	}

	public ArrayList<DelegowanyItem> getDelegowanyItems(FieldsManager fm) {

		ArrayList<DelegowanyItem> ret = new ArrayList<DelegowanyItem>();
		try {
			@SuppressWarnings("unchecked")
			List<Map<String, Object>> delegowani = (List<Map<String, Object>>) fm.getValue("DELEGOWANY");
			for (Map<String, Object> delegowany : delegowani) {
				if (delegowany != null) {
					DelegowanyItem di = new DelegowanyItem();
					if (delegowany.get("DELEGOWANY_FIRSTNAME") != null)
						di.setImie((String) delegowany.get("DELEGOWANY_FIRSTNAME"));
					if (delegowany.get("DELEGOWANY_LASTNAME") != null)
						di.setNazwisko((String) delegowany.get("DELEGOWANY_LASTNAME"));
					if (delegowany.get("DELEGOWANY_DIVISION_NAME") != null)
						di.setDzial((String) delegowany.get("DELEGOWANY_DIVISION_NAME"));
					if (delegowany.get("DELEGOWANY_MOBILE_PHONE_NUM") != null)
						di.setNrTel((String) delegowany.get("DELEGOWANY_MOBILE_PHONE_NUM"));
					if (delegowany.get("DELEGOWANY_POSITION") != null)
						di.setStanowisko((String) delegowany.get("DELEGOWANY_POSITION"));
					ret.add(di);
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return ret;
	}
	
	public ContractorItem getContractor(FieldsManager fm)
	{
		try
		{
			HashMap<String, Object> contr = (HashMap<String, Object>) fm.getValue("CONTRACTOR");
			// log.error("*****************"+contr.keySet());
			// log.error("1*********"+contr.get("CONTRACTOR_ORGANIZATION"));
			// log.error("2*********"+contr.get("CONTRACTOR_STREET"));
			// log.error("3*********"+contr.get("CONTRACTOR_ZIP"));
			// log.error("4*********"+contr.get("CONTRACTOR_LOCATION"));
			// log.error("5*********"+contr.get("CONTRACTOR_NIP"));
			if (contr != null)
			{
				ContractorItem co = new ContractorItem(" ", " ", " ", " ", " ");
				if (contr.get("CONTRACTOR_ORGANIZATION") != null)
					co.setCompany((String) contr.get("CONTRACTOR_ORGANIZATION"));
				if (contr.get("CONTRACTOR_STREET") != null)
					co.setStreet((String) contr.get("CONTRACTOR_STREET"));
				if (contr.get("CONTRACTOR_ZIP") != null)
					co.setZip((String) contr.get("CONTRACTOR_ZIP"));
				if (contr.get("CONTRACTOR_LOCATION") != null)
					co.setLocation((String) contr.get("CONTRACTOR_LOCATION").toString());
				if (contr.get("CONTRACTOR_NIP") != null)
					co.setNip((String) contr.get("CONTRACTOR_NIP"));

				return co;
			}
			return null;

		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return null;
	}

	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret = new ProcessCoordinator();
		String coordinator = document.getFieldsManager().getEnumItemCn("KOORDYNATOR");
		ret.setUsername(coordinator);
		return ret;
	}

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FolderResolver fr = document.getDocumentKind().getDockindInfo().getFolderResolver();
		if (fr != null)
		{
			as.useFolderResolver(document);
		}
	}

	protected boolean assigneeToAuthorOrCoordinator(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn)
	{
		FieldsManager fm = doc.getFieldsManager();
		try{
			fm.initialize();
		}catch(Exception e){
			log.error(e.getMessage(), e);
			return false;
		}
		
		
		if (fm.getFieldsCns().contains("KOORDYNATOR"))
		{
			try
			{
				assignable.addCandidateUser(fm.getEnumItemCn("KOORDYNATOR"));
				return true;
			}
			catch (EdmException e)
			{
				log.error(e.getMessage(), e);
				return false;
			}
		}
		else
		{
			assignable.addCandidateUser(doc.getAuthor());
			return true;
		}
		
	}
	
	protected boolean assigneeToFinancialConfirmation(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn)
	{
		boolean assigned = false;
		try
		{
			String centrumId = openExecution.getVariable("costCenterId").toString();
			List<String> users = new ArrayList<String>();
			List<String> divisions = new ArrayList<String>();
			for (AcceptanceCondition accept : AcceptanceCondition.find(acceptationCn, centrumId))
			{
				if (accept.getUsername() != null && !users.contains(accept.getUsername()))
					users.add(accept.getUsername());
				if (accept.getDivisionGuid() != null && !divisions.contains(accept.getDivisionGuid()))
					divisions.add(accept.getDivisionGuid());
			}
			for (String user : users)
			{
				assignable.addCandidateUser(user);
				AssigneeHandler.addToHistory(openExecution, doc, user, null);
				assigned = true;
			}
			for (String division : divisions)
			{
				assignable.addCandidateGroup(division);
				AssigneeHandler.addToHistory(openExecution, doc, null, division);
				assigned = true;
			}
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		return assigned;
	}

	protected boolean assigneeToEssentialDescription(Assignable assignable, OpenExecution openExecution)
	{
		Object recipient = openExecution.getVariable(ASSIGN_USER_PARAM);

		if (recipient != null)
			assignable.addCandidateUser((String) recipient);
		else
		{
			recipient = openExecution.getVariable(ASSIGN_DIVISION_GUID_PARAM);
			assignable.addCandidateGroup((String) recipient);
		}
		return true;
	}
	
	private void checkDate(Map<String, ?> fieldValues, StringBuilder msgBuilder)
	{
		if (fieldValues.get("RODZAJ") != null)
		{
			Object o = fieldValues.get("RODZAJ");
			Date start = null, finish = null;
			if (o != null)
			{
				if (fieldValues.get("START_DATE") != null && fieldValues.get("FINISH_DATE") != null)
				{
					start = (Date) fieldValues.get("START_DATE");
					finish = (Date) fieldValues.get("FINISH_DATE");
				}
			}
			else
			{
				if (fieldValues.get("ANEKS_START_DATE") != null && fieldValues.get("ANEKS_FINISH_DATE") != null)
				{
					start = (Date) fieldValues.get("ANEKS_START_DATE");
					finish = (Date) fieldValues.get("ANEKS_FINISH_DATE");
				}
			}
			if (start != null && finish != null && start.after(finish))
			{
				if (msgBuilder.length() == 0)
					msgBuilder.append("B��d: Data zako�czenia nie mo�e by� wcze�niejsza ni� data rozpocz�cia.");
				else
					msgBuilder.append(";  Data zako�czenia nie mo�e by� wcze�niejsza ni� data rozpocz�cia");
			}
		}
	}

	private void checkNettoOverBrutto(Map<String, ?> fieldValues, StringBuilder msgBuilder)
	{
		BigDecimal netto = (BigDecimal) fieldValues.get("KWOTA_NETTO");
		BigDecimal brutto = (BigDecimal) fieldValues.get("KWOTA_BRUTTO");

		if (netto.setScale(2, RoundingMode.HALF_UP).compareTo(brutto.setScale(2, RoundingMode.HALF_UP)) == 1)
		{
			if (msgBuilder != null && msgBuilder.length() == 0)
				msgBuilder.append("B��d: Kwota brutto nie mo�e by� mniejsza ni� kwota netto.");
			else
				msgBuilder.append(";  Kwota brutto nie mo�e by� mniejsza ni� kwota netto.");
		}
	}

	private void validateNettoBrutto(Map<String, FieldData> values, StringBuilder msgBuilder)
	{
		BigDecimal netto = values.get("DWR_KWOTA_NETTO").getMoneyData();
		BigDecimal brutto = values.get("DWR_KWOTA_BRUTTO").getMoneyData();
		if (netto.setScale(2, RoundingMode.HALF_UP).compareTo(brutto.setScale(2, RoundingMode.HALF_UP)) == 1)
		{
			if (msgBuilder.length() == 0)
				msgBuilder.append("B��d: Kwota brutto nie mo�e by� mniejsza ni� kwota netto.");
			else
				msgBuilder.append(";  Kwota brutto nie mo�e by� mniejsza ni� kwota netto.");
		}
	}
	
	private void setPln(Map<String, FieldData> values)
	{
		BigDecimal wWalucie = values.get(DWR_KWOTA_W_WALUCIE).getMoneyData();
		BigDecimal rate = values.get(DWR_RATE).getMoneyData();
		BigDecimal pln = BigDecimal.ZERO;
		pln = wWalucie.multiply(rate);
		values.get(DWR_KWOTA_PLN).setMoneyData(pln.setScale(2, RoundingMode.HALF_UP));
	}
	
	protected void putDictionaryValues(Map<String, Object> values, FieldsManager fm, String dicFieldCn) throws EdmException {
		List<Map<String,Object>> dicLists = (List<Map<String, Object>>) fm.getValue(dicFieldCn);
		if (dicLists != null) {
			values.put(dicFieldCn, dicLists);
		}
	}
	
	protected void putDictionaryValuesWithDateTimeFormat(Map<String, Object> values, FieldsManager fm, String dicFieldCn, boolean isTimeDateFormat) throws EdmException {
		List<Map<String,Object>> dicLists = (List<Map<String, Object>>) fm.getValue(dicFieldCn);
		if (dicLists != null) {
			for (Map<String,Object> field : dicLists) {
				if (field != null) {
					String[] keys =  field.keySet().toArray(new String[0]);
					for (String key : keys) {
						if (field.get(key) != null && field.get(key) instanceof Date) {
							if (isTimeDateFormat) {
								field.put(key+"_DATE",DateUtils.formatJsDateTime((Date) field.get(key)));
							} else {
								field.put(key+"_DATE",DateUtils.formatJsDate((Date) field.get(key)));
							}
							
						}
						
					}
				}
			}
			values.put(dicFieldCn, dicLists);
		}
	}
	
	protected void putDictionaryValues(Map<String, Object> values, FieldsManager fm, String dicFieldCn, int percent) throws EdmException {
		List<Map<String,Object>> dicLists = (List<Map<String, Object>>) fm.getValue(dicFieldCn);
		if (dicLists != null) {
			BigDecimal multiplier = new BigDecimal(percent).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
			for (Map<String,Object> field : dicLists) {
				if (field != null) {
					String[] keys =  field.keySet().toArray(new String[0]);
					for (String key : keys) {
						if (field.get(key) != null) {
							//wypuszczenie na rtf'a warto�ci przemno�onych przez dan� procentow� warto��
							if (field.get(key) instanceof BigDecimal) {
								field.put(key + "_" + percent, ((BigDecimal) field.get(key)).multiply(multiplier).setScale(2, RoundingMode.HALF_UP));
							}
							if (field.get(key) instanceof Double) {
								field.put(key + "_" + percent, new BigDecimal(((Double) field.get(key))).multiply(multiplier).setScale(2, RoundingMode.HALF_UP));
							}
						}
					}
				}
			}
			values.put(dicFieldCn, dicLists);
		}
	}

	protected void convertDateFieldToDateTime(Map<String, Object> values, FieldsManager fm, String dateFieldCn) throws EdmException {
		if (fm.getValue(dateFieldCn) != null && fm.getValue(dateFieldCn) instanceof Date) {
			values.put(dateFieldCn,DateUtils.formatJsDateTime((Date)fm.getValue(dateFieldCn)));
		}
	}
	
}
