package pl.compan.docusafe.parametrization.polcz;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ExportInvoice {
	public static final String MAIN_QUERY_TEMPLATE = "select document_id, nr_faktury from dsg_pcz_costinvoice where data_wystawienia >= ?";
	protected static Logger log = LoggerFactory.getLogger(ExportInvoice.class);
	
	public static Invoice[] getInvoiceFromDate (String fromDate) {
        StringBuilder query;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Long> documentIds;
		List<String> documentIdsInvoiceNo;
		List<Invoice> invoices = new ArrayList<Invoice>();
		
		/*
		Invoice[] test = new Invoice[2];
		test[0] = new Invoice();
		test[0].setContractNo("nr umowy");
		test[0].setInvoiceNo("nr faktury");
		test[0].setItemId(new Long(32));
		test[0].setNetAmount("432.32");
		test[0].setQuantity(new Long(5435));
		test[0].setVatAmount("432.32");
		test[0].setTenderSignature("sgynatura przetargu");
		test[0].setUnit("jednostka");
		test[1] = new Invoice();
		test[1].setContractNo("nr umowy2");
		test[1].setInvoiceNo("nr faktury2");
		test[1].setItemId(new Long(32));
		test[1].setNetAmount("432.32");
		test[1].setQuantity(new Long(5435));
		test[1].setVatAmount("432.32");
		test[1].setTenderSignature("sgynatura przetargu2");
		test[1].setUnit("jednostka2");
		
		return test;
		*/
		//----------------------------------------------
		//----------------------------------------------
		//----------------------------------------------
		//----------------------------------------------
		//----------------------------------------------
		//----------------------------------------------
		log.debug("getInvoiceFromDate run!");
		if (fromDate != null && !fromDate.equals("")) 
			log.debug("date: "+fromDate);
		else {
			log.error("Null date for ExportApplication");
			return null;
		}
		
		query = new StringBuilder(MAIN_QUERY_TEMPLATE);
		documentIds = new ArrayList<Long>();
		documentIdsInvoiceNo = new ArrayList<String>();
		try {
			DSApi.openAdmin();
			DSApi.context().begin();
			log.debug("OTWARTO context");
			
			// wyszukuje faktury wystawione po danej dacie >=
			ps = DSApi.context().prepareStatement(query);
			ps.setString(1, fromDate);
			rs = ps.executeQuery();
			while (rs.next()) {
				documentIds.add(rs.getLong(1));
				documentIdsInvoiceNo.add(rs.getString(2));
			}
			DSApi.context().closeStatement(ps);
			Set<Long> contractIds = new HashSet<Long>();
			
			// wyszukuj� dla danych faktur, pozycje umowy oraz umowy przypisanej do tych pozycji
			int i = 0;
			for (Long id : documentIds) {
				
				query = new StringBuilder("select item.contractItem_id, item.unit, item.quantity, item.netAmount, item.vatAmount, contract.contract_no, contract.tender_signature, item.contractId from (select * from dsg_pcz_costinvoice_multiple_value where field_cn='CONTRACT_ITEM' and document_id=?) as multi JOIN dsg_pcz_contract_item_invoice item ON multi.field_val = item.id JOIN dsg_pcz_contract contract ON item.contractId = contract.ID");
				ps = DSApi.context().prepareStatement(query);
				ps.setLong(1, id);
    			rs = ps.executeQuery();
				
    			while (rs.next()) {
    				Invoice inv = new Invoice();
    				inv.setItemId(rs.getLong(1));
    				inv.setUnit(rs.getString(2));
    				inv.setQuantity(rs.getLong(3));
    				inv.setNetAmount(rs.getBigDecimal(4).toString());
    				inv.setVatAmount(rs.getBigDecimal(5).toString());
    				inv.setContractNo(rs.getString(6));
    				inv.setTenderSignature(rs.getString(7));
    				inv.setInvoiceNo(documentIdsInvoiceNo.get(i));
    				contractIds.add(rs.getLong(8));
    				invoices.add(inv);
    			}
    			
				i++;
				DSApi.context().closeStatement(ps);
			}
			
			// uzupe�nienie o faktury zawieraj�ce umowy, a nie zawieraj�ce �adnej pozycji dla tych um�w
			// w tym przypadku pola id pozycji, jednostka, ilo��, warto�� netto, warto�� vat s� puste
			i = 0;
			for (Long id : documentIds) {
				query = new StringBuilder("select contract.id, contract.contract_no, contract.tender_signature from (select * from dsg_pcz_costinvoice_multiple_value where field_cn='CONTRACT' and document_id=?) as multi JOIN dsg_pcz_contract contract ON multi.field_val = contract.id");
				ps = DSApi.context().prepareStatement(query);
				ps.setLong(1, id);
				rs = ps.executeQuery();

				while (rs.next()) {
					if (!contractIds.contains(rs.getLong(1))) {
						Invoice inv = new Invoice();
						inv.setContractNo(rs.getString(2));
						inv.setTenderSignature(rs.getString(3));
						inv.setInvoiceNo(documentIdsInvoiceNo.get(i));
						invoices.add(inv);
					}
				}
				
				i++;
				DSApi.context().closeStatement(ps);
			}
			
			
		} catch (Exception e) {
			DSApi.context().closeStatement(ps);
			log.error(e.getMessage(), e);
			return null;
		} finally
		{
			try {
				DSApi.context().rollback();
				DSApi.close();
			} catch (EdmException e) {
				log.error(e.getMessage(), e);
				return null;
			}
		}
		
		for (Invoice i : invoices)
			log.debug(i);
		
		return invoices.toArray(new Invoice[invoices.size()]);
		
	}
}
