package pl.compan.docusafe.parametrization.polcz;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.SubstituteHistory;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ChargeAbsenceListener extends AbstractEventListener
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;

	private static final String FROM_CN = "DATA_OD";
	private static final String TO_CN = "DATA_DO";
	private static final String WORKING_DAYS_CN = "ILOSC_DNI";
	private static final String WNIOSKODAWCA_CN = "WNIOSKODAWCA";
	private static final String DESCRIPTION_CN = "TYTUL";
	private static final String KIND_CN = "URLOPRODZAJ";

	private static final Map<Integer, String> absenceTypes = new HashMap<Integer, String>();

	private static final Logger LOG = LoggerFactory.getLogger(ChargeAbsenceListener.class);

	public void notify(EventListenerExecution eventListenerExecution) throws Exception
	{
		Long docId = Long.parseLong(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		LOG.info("doc id = " + docId);

		// pobranie obiektu dokumentu
		Document document = Document.find(docId);
		FieldsManager fm = document.getFieldsManager();
		
		// pobranie wnioskodawcy, z pola WNIOSKODAWCA!!!, a nie autor dokumentu, po to jest to pole aby mo�na by�o kresli� wnioskdoawca, mo�e by� inny ni� autor
		UserImpl userImpl = (UserImpl) DSUser.findByUsername(fm.getEnumItemCn(WNIOSKODAWCA_CN));
		
		if (WniosekUrlopLogic.getAvailableDayFromSimpleERP(userImpl.getExtension(), null, null) == -1)
			throw new IllegalStateException("Simple.ERP: Brak przypisanego nale�nego wymiaru urlopowego na bierz�cy rok!");

		// ustawienie dat
		Date from = (Date) fm.getValue(FROM_CN);
		Date to = (Date) fm.getValue(TO_CN);

		// Zastepstwa
		Calendar cal = Calendar.getInstance();
		cal.setTime(to);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);

          SubstituteHistory history = new SubstituteHistory(userImpl.getName(), from, cal.getTime(), fm.getEnumItemCn("OSOBA_ZASTEPUJ"));
          userImpl.setSubstitutionFromHistory(history);
          DSApi.context().session().save(history);
          SubstituteHistory sh = SubstituteHistory.findActualSubstitute(userImpl.getName());
          userImpl.setSubstitutionFromHistory(sh);
          
	}

	
}
