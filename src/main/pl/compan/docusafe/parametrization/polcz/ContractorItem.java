package pl.compan.docusafe.parametrization.polcz;

public class ContractorItem {

	/**
	 * @param args
	 */
	
	private String company, street, zip, location, nip;

	public ContractorItem(String company, String street, String zip,
			String city, String nip) {
		super();
		this.company = company;
		this.street = street;
		this.zip = zip;
		this.location = city;
		this.nip = nip;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String city) {
		this.location = city;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}
	
}
