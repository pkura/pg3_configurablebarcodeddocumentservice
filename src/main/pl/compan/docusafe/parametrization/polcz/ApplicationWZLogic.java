package pl.compan.docusafe.parametrization.polcz;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.Field.Type;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class ApplicationWZLogic extends AbstractBudgetApplicationLogic
{
	private static ApplicationWZLogic instance;
	protected Logger log = LoggerFactory.getLogger(ApplicationWZLogic.class);
	private final static String RYCZALTY_DOJAZDY_KOM_DICT = "RYCZALTY_DOJAZDY_KOM";
	private final static String KALKULACJA_KOSZTOW_DICT = "KALKULACJA_KOSZTOW"; 
	private final static String KOSZTY_NOCLEGU_RYCZALTY_DICT = "RYCZALTY_NOCLEG";
	private final static String KOSZTY_PRZEJAZDU_DICT = "KOSZTY_PRZEJAZDU_SAM";
	private final static String KOSZTY_OGOLEM_DICT = "OGOLEM_KOSZTY";
	
	public static ApplicationWZLogic getInstance()
	{
		if (instance == null)
			instance = new ApplicationWZLogic();
		return instance;
	}
	
	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
	{
		super.onSaveActions(kind, documentId, fieldValues);
		
	}
	
	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		FieldsManager fm = document.getFieldsManager();
		if (fm.getBoolean("ZALICZKA_BOOL"))
			setPobranaZaliczka(document, (BigDecimal) fm.getKey("PRZEWIDYWANY_KOSZT"));
		else
			setPobranaZaliczka(document, new BigDecimal(0).setScale(2));
		
		super.onStartProcess(document, event);
	}

	private void setPobranaZaliczka(OfficeDocument document, BigDecimal kwotaZaliczki) 
	{
		PreparedStatement ps = null;
		try
		{
			String sql = "update " + document.getDocumentKind().getTablename() +" set pobrana_zaliczka = ? where document_id = ?";
			log.debug("setkwotaZaliczki.sql: {}", sql);
			ps = DSApi.context().prepareStatement(sql);
			ps.setBigDecimal(1, kwotaZaliczki);
			ps.setLong(2, document.getId());
			ps.executeUpdate();
			ps.close();
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		finally
		{
			DbUtils.closeQuietly(ps);
		}
		
	}
	
	public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
	{
		pl.compan.docusafe.core.dockinds.dwr.Field msgField = super.validateDwr(values, fm);
		
		StringBuilder msgBuilder = new StringBuilder();
		
		if (msgField != null)
			msgBuilder.append(msgField.getLabel());
		
		Map<String, FieldData> kalkulacja = values.get("DWR_KALKULACJA_KOSZTOW").getDictionaryData();
		
		if(values.containsKey("DWR_CZAS_PODR_WYJAZD_DATA") && 
		   values.get("DWR_CZAS_PODR_WYJAZD_DATA") != null &&
		   values.containsKey("DWR_CZAS_PODR_WYJAZD_DATA_GR") && 
		   values.get("DWR_CZAS_PODR_WYJAZD_DATA_GR") != null &&
		   values.containsKey("DWR_CZAS_PODR_POWROT_DATA") && 
		   values.get("DWR_CZAS_PODR_POWROT_DATA") != null &&
		   values.containsKey("DWR_CZAS_PODR_POWROT_DATA_GR") && 
		   values.get("DWR_CZAS_PODR_POWROT_DATA_GR") != null)
		{
			setTripTime(values);
		}
		
		podsumowaniePozycjiBuzdetowych(values, fm);
		
		podsumowanieKalkulacjiKosztow(values, fm);
		
		checkDepartureArrivalTime(values, fm, msgBuilder);
		
		checkDrpartureArrivalTimeInClearance(values, fm, msgBuilder);
		
		try
		{
			if (fm.getEnumItem("STATUS") != null && fm.getEnumItem("STATUS").getId() == 240)
				kosztyOgolem(values, fm);
		}
		catch (EdmException e)
		{
			log.warn(e.getMessage(), e);
		}
		
		if (msgBuilder.length() > 0)
		{
			values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
			return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
		}
		
		return null;
	}
	
	
	private void checkDepartureArrivalTime(Map<String, FieldData> values,
			FieldsManager fm, StringBuilder msgBuilder) {
		try{
		if(fm.containsField("WYJAZD_DATE") && fm.containsField("PRZYJAZD_DATE") && fm.getValue("WYJAZD_DATE") !=null && fm.getValue("PRZYJAZD_DATE") !=null){
			Date dep = (Date) fm.getValue("WYJAZD_DATE");
			Date arr = (Date) fm.getValue("PRZYJAZD_DATE");
			log.error("sprawdzeniedat: " + dep);
			log.error("sprawdzeniedat: " + arr);
			if(dep.after(arr)) msgBuilder.append("Data wyjazdu nie mo�e by� po�niejsza ni� data powrotu");
		}
		}catch(Exception e){
			log.error(e.getMessage(), e);	
		}
	}
	
	private void checkDrpartureArrivalTimeInClearance(Map<String, FieldData> values,
			FieldsManager fm, StringBuilder msgBuilder){
		try{
			if(fm.containsField("CZAS_PODR_WYJAZD_DATA") && fm.getValue("CZAS_PODR_WYJAZD_DATA") != null &&
			   fm.containsField("CZAS_PODR_WYJAZD_DATA_GR") && fm.getValue("CZAS_PODR_WYJAZD_DATA_GR") != null &&
			   fm.containsField("CZAS_PODR_POWROT_DATA") && fm.getValue("CZAS_PODR_POWROT_DATA") != null &&
			   fm.containsField("CZAS_PODR_POWROT_DATA_GR") && fm.getValue("CZAS_PODR_POWROT_DATA_GR") != null)
			{
				
				Date d1 = (Date) fm.getValue("CZAS_PODR_WYJAZD_DATA");
				Date d2 = (Date) fm.getValue("CZAS_PODR_WYJAZD_DATA_GR");
				Date d3 = (Date) fm.getValue("CZAS_PODR_POWROT_DATA_GR");
				Date d4 = (Date) fm.getValue("CZAS_PODR_POWROT_DATA");
				
				if(!d1.before(d2)) msgBuilder.append("Data wyjazdu nie mo�e by� p�niejsza ni� data przekroczenia granicy");
				if(!d2.before(d3)) msgBuilder.append("Data przekroczenia granicy podczas wyjazdu nie mo�e by� p�niejsza ni� data przekroczenia granicy podczas powrotu");
				if(!d3.before(d4)) msgBuilder.append("Data przekroczenia granicy podczas powrotu nie mo�e by� p�niejsza ni� data powrotu");
			}
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values) {
		try {
			FieldsManager fm = Document.find(docId).getFieldsManager();
			values.put("DELEGOWANY", getDelegowanyItems(fm));

			setDestinationCountry(values, fm, "KRAJ_WYJAZDU", "DOCELOWE_PANSTWO_PODROZY");
			
			convertDateFieldToDateTime(values, fm, "CZAS_PODR_WYJAZD_DATA");
			convertDateFieldToDateTime(values, fm, "CZAS_PODR_WYJAZD_DATA_GR");
			convertDateFieldToDateTime(values, fm, "CZAS_PODR_POWROT_DATA_GR");
			convertDateFieldToDateTime(values, fm, "CZAS_PODR_POWROT_DATA");
			convertDateFieldToDateTime(values, fm, "WYJAZD_DATE");
			convertDateFieldToDateTime(values, fm, "PRZYJAZD_DATE");
			putDictionaryValues(values, fm, "DIETY");
			putDictionaryValues(values, fm, "RYCZALTY_DOJAZDY_KOM",10);
			putDictionaryValues(values, fm, "RYCZALTY_DOJAZDY_DW");
			putDictionaryValues(values, fm, "RYCZALTY_NOCLEG",25);
			putDictionaryValues(values, fm, "RYCZALTY_NOCLEG_RACHUNKI");
			putDictionaryValues(values, fm, "KOSZTY_PRZEJAZDU_OKR");
			putDictionaryValues(values, fm, "KOSZTY_PRZEJAZDU_SAM");
			putDictionaryValues(values, fm, "INNE_WYDATKI");
			putDictionaryValues(values, fm, "OGOLEM_KOSZTY");
			putDictionaryValues(values, fm, "KALKULACJA_KOSZTOW");
			putDictionaryValues(values, fm, "KRAJ_WYJAZDU");
			
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}
		
		super.setAdditionalTemplateValuesBasic(docId, values);
	}

	protected void setDestinationCountry(Map<String, Object> values, FieldsManager fm, String countryDicCn, String setFieldName) throws EdmException {
		List<Map<String,Object>> delegatePlaces = (List<Map<String, Object>>) fm.getValue(countryDicCn);
		if (delegatePlaces != null) {
			for (Map<String,Object> delegatePlace : delegatePlaces) {
				if (delegatePlace.get(countryDicCn+"_DESTINATION") != null && 
						((EnumValues)delegatePlace.get(countryDicCn+"_DESTINATION")).getSelectedId()!=null && 
						((EnumValues)delegatePlace.get(countryDicCn+"_DESTINATION")).getSelectedId().equals("9041")) {
					
					EnumValues countriesEnum = (EnumValues)delegatePlace.get(countryDicCn+"_COUNTRY");
					if (countriesEnum != null && countriesEnum.getSelectedValue() != null) {
						
						values.put(setFieldName, countriesEnum.getSelectedValue());
					}
				}
			}
		}
	}

	private void podsumowanieKalkulacjiKosztow(Map<String, FieldData> values,
			FieldsManager fm) {
		try{
		Map<String, FieldData> kalkulacja = values.get("DWR_KALKULACJA_KOSZTOW").getDictionaryData();
		BigDecimal suma = new BigDecimal(0).setScale(2);
		int size = kalkulacja.keySet().size()/7;
		log.error("przedpetla: " + kalkulacja);
		for(int i = 1; i <= size; i++)
		{
			log.error("wpetli: " + kalkulacja);
			BigDecimal kwota = kalkulacja.get("KALKULACJA_KOSZTOW_CURRENCY_VALUE_"+i).getMoneyData().multiply(kalkulacja.get("KALKULACJA_KOSZTOW_RATE_"+i).getMoneyData());
			suma = suma.add(kwota);
		}
		FieldData fd = new FieldData(Type.MONEY, suma);
		values.put("DWR_PRZEWIDYWANY_KOSZT", fd);
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
	}

	private void podsumowaniePozycjiBuzdetowych(Map<String, FieldData> values,
			FieldsManager fm) 
	{
		BigDecimal suma = new BigDecimal(0).setScale(2);
		try
		{
			Map<String, FieldData> mpk = values.get("DWR_MPK").getDictionaryData();
			int size = mpk.keySet().size()/10;
			for (int i = 1; i <= size; i++)
			{
				if (mpk.get("MPK_AMOUNT_"+i).getData() != null)
					suma = suma.add(mpk.get("MPK_AMOUNT_"+i).getMoneyData());
			}
		}
		catch(Exception e)
		{
			log.error(e.getMessage(), e);
		}
		FieldData fd = new FieldData(Type.MONEY, suma);
		values.put("DWR_SUMA_POZYCJI", fd);
	}

	private void kosztyOgolem(Map<String, FieldData> values, FieldsManager fm) 
	{
		// TODO Auto-generated method stub
		HashMap<String, Double> kursy = new HashMap<String, Double>();
		HashMap<String, BigDecimal> koszty = new HashMap<String, BigDecimal>();
		HashMap<String, Integer> ids = new HashMap<String, Integer>();
		int size;
		try
		{
			
			Map<String, FieldData> kalkulacja = values.get("DWR_KALKULACJA_KOSZTOW").getDictionaryData();
			size = kalkulacja.keySet().size()/7;
			for(int i = 1; i <= size; i++)
			{
				
				String waluta = kalkulacja.get("KALKULACJA_KOSZTOW_CURRENCY_"+i).getEnumValuesData().getSelectedId();
				double kurs = Double.parseDouble(kalkulacja.get("KALKULACJA_KOSZTOW_RATE_"+i).getStringData());
				int idkurs = Integer.parseInt(kalkulacja.get("KALKULACJA_KOSZTOW_CURRENCY_"+i).getEnumValuesData().getSelectedId());
				
				kursy.put(waluta, kurs);
				ids.put(waluta, idkurs);
			}
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
		
		//DIETY
		try
		{
			Map<String, FieldData> diety = values.get("DWR_DIETY").getDictionaryData();
			size = diety.keySet().size()/3;
			for(int i=1; i <= size; i++){
				String wal = diety.get("DIETY_CURRENCY_" + i).getEnumValuesData().getSelectedId();
				if (wal.equals(""))
					continue;
				BigDecimal kwota = diety.get("DIETY_CURRENCY_VALUE_" + i).getMoneyData();
				if(koszty.containsKey(wal)) 
					koszty.put(wal, koszty.get(wal).add(kwota));
				else 
					koszty.put(wal, kwota);
			}
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
		
		//RYCZA�TY NA DOJAZDY
		try
		{
			Map<String, FieldData> rd = values.get("DWR_RYCZALTY_DOJAZDY_KOM").getDictionaryData();
			size = rd.keySet().size()/5;
			for(int i=1; i <= size; i++){
				String wal = rd.get("RYCZALTY_DOJAZDY_KOM_CURRENCY_" + i).getEnumValuesData().getSelectedId();
				if (wal.equals(""))
					continue;
				BigDecimal kwotaWWalucie = rd.get("RYCZALTY_DOJAZDY_KOM_CURRENCY_VALUE_" + i).getMoneyData();
			//	koszt.multiply(new BigDecimal(ile)).multiply(new BigDecimal(0.1)).setScale(2, RoundingMode.HALF_UP));
				int dni = rd.get("RYCZALTY_DOJAZDY_KOM_DAYS_" + i).getIntegerData();
				BigDecimal kwota = kwotaWWalucie.multiply(new BigDecimal(dni)).multiply(new BigDecimal(0.1)).setScale(2, RoundingMode.HALF_UP);
				if(koszty.containsKey(wal)) 
					koszty.put(wal, koszty.get(wal).add(kwota));
				else 
					koszty.put(wal, kwota);
			}
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
			
		//RYCZ�TY NA DOJAZDY DWORZEC
		try
		{
			Map<String, FieldData> rdw = values.get("DWR_RYCZALTY_DOJAZDY_DW").getDictionaryData();
			size = rdw.keySet().size()/5;
			for(int i=1; i <= size; i++){
				String wal = rdw.get("RYCZALTY_DOJAZDY_DW_CURRENCY_" + i).getEnumValuesData().getSelectedId();
				if (wal.equals(""))
					continue;
				BigDecimal kwota = rdw.get("RYCZALTY_DOJAZDY_DW_CURRENCY_VALUE_" + i).getMoneyData();
				if(koszty.containsKey(wal)) koszty.put(wal, koszty.get(wal).add(kwota));
				else koszty.put(wal, kwota);
			}
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
		
		//RYCZA�TY NOCLEGU RACHUNKI RYCZALTY_NOCLEG
		try
		{
			Map<String, FieldData> rdn = values.get("DWR_RYCZALTY_NOCLEG").getDictionaryData();
			size = rdn.keySet().size()/5;
			for(int i=1; i <= size; i++){
				String wal = rdn.get("RYCZALTY_NOCLEG_CURRENCY_" + i).getEnumValuesData().getSelectedId();
				if (wal.equals(""))
					continue;
				BigDecimal kwotaWWalucie = rdn.get("RYCZALTY_NOCLEG_CURRENCY_VALUE_" + i).getMoneyData();
				int dni = rdn.get("RYCZALTY_NOCLEG_DAYS_" + i).getIntegerData();
				BigDecimal kwota = kwotaWWalucie.multiply(new BigDecimal(dni)).multiply(new BigDecimal(0.25)).setScale(2,  RoundingMode.HALF_UP);
				if(koszty.containsKey(wal)) koszty.put(wal, koszty.get(wal).add(kwota));
				else koszty.put(wal, kwota);
			}
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
		
		//RYCZALTY_NOCLEG_RACHUNKI
		try
		{
			Map<String, FieldData> rdr = values.get("DWR_RYCZALTY_NOCLEG_RACHUNKI").getDictionaryData();
			size = rdr.keySet().size()/4;
			for(int i=1; i <= size; i++){
				String wal = rdr.get("RYCZALTY_NOCLEG_RACHUNKI_CURRENCY_" + i).getEnumValuesData().getSelectedId();
				if (wal.equals(""))
					continue;
				BigDecimal kwota = rdr.get("RYCZALTY_NOCLEG_RACHUNKI_CURRENCY_VALUE_" + i).getMoneyData();
				if(koszty.containsKey(wal)) koszty.put(wal, koszty.get(wal).add(kwota));
				else koszty.put(wal, kwota);
			}
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
			
		//KOSZTY_PRZEJAZDU_OKR
		try
		{
			Map<String, FieldData> rdro = values.get("DWR_KOSZTY_PRZEJAZDU_OKR").getDictionaryData();
			size = rdro.keySet().size()/4;
			for(int i=1; i <= size; i++){
				String wal = rdro.get("KOSZTY_PRZEJAZDU_OKR_CURRENCY_" + i).getEnumValuesData().getSelectedId();
				if (wal.equals(""))
					continue;
				BigDecimal kwota = rdro.get("KOSZTY_PRZEJAZDU_OKR_CURRENCY_VALUE_" + i).getMoneyData();
				if(koszty.containsKey(wal)) koszty.put(wal, koszty.get(wal).add(kwota));
				else koszty.put(wal, kwota);
			}
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
			
		//KOSZTY_PRZEJAZDU_SAM
		try
		{
			Map<String, FieldData> rdro = values.get("DWR_KOSZTY_PRZEJAZDU_SAM").getDictionaryData();
			size = rdro.keySet().size()/6;
			for(int i=1; i <= size; i++){
				String wal = "1";
				if (wal.equals(""))
					continue;
				kursy.put("1", 1D);
				BigDecimal stawka = new BigDecimal(1.0);
				BigDecimal koszt = new BigDecimal(0).setScale(2);
				boolean doAdd = false;
				if(rdro.get("KOSZTY_PRZEJAZDU_SAM_ENUM_"+i).getEnumValuesData().getSelectedId().equals("55011") && rdro.containsKey("KOSZTY_PRZEJAZDU_SAM_ENUM2_"+i) && rdro.get("KOSZTY_PRZEJAZDU_SAM_ENUM2_"+i)!=null)
				{
					if(rdro.get("KOSZTY_PRZEJAZDU_SAM_ENUM2_"+i).getEnumValuesData().getSelectedId().equals("55041") || rdro.get("KOSZTY_PRZEJAZDU_SAM_ENUM2_"+i).getEnumValuesData().getSelectedId().equals("55042")){
						if(rdro.get("KOSZTY_PRZEJAZDU_SAM_ENUM2_"+i).getEnumValuesData().getSelectedId().equals("55041_"+i)) stawka = new BigDecimal(0.4);
						int ile = rdro.get("KOSZTY_PRZEJAZDU_SAM_DAYS_"+i).getIntegerData();
						koszt = rdro.get("KOSZTY_PRZEJAZDU_SAM_VALUE_"+i).getMoneyData();
						koszt = stawka.multiply(new BigDecimal(ile)).multiply(koszt);
						doAdd = true;
					}
				}
				else if(rdro.get("KOSZTY_PRZEJAZDU_SAM_ENUM_"+i).getEnumValuesData().getSelectedId().equals("55012"))
				{
					int ile = rdro.get("KOSZTY_PRZEJAZDU_SAM_DAYS_"+i).getIntegerData();
					koszt = rdro.get("KOSZTY_PRZEJAZDU_SAM_VALUE_"+i).getMoneyData();
					koszt = stawka.multiply(new BigDecimal(ile)).multiply(koszt);
					doAdd = true;
				}
				if(doAdd){
					if(koszty.containsKey(wal)) 
						koszty.put(wal, koszty.get(wal).add(koszt));
					else 
						koszty.put(wal, koszt);
				}
			}
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
				
		//INNE_WYDATKI
		try
		{
			Map<String, FieldData> inne = values.get("DWR_INNE_WYDATKI").getDictionaryData();
			size = inne.keySet().size()/4;
			for(int i=1; i <= size; i++){
				String wal = inne.get("INNE_WYDATKI_CURRENCY_" + i).getEnumValuesData().getSelectedId();
				if (wal.equals(""))
					continue;
				BigDecimal kwota = inne.get("INNE_WYDATKI_VALUE_" + i).getMoneyData();
				if(koszty.containsKey(wal)) koszty.put(wal, koszty.get(wal).add(kwota));
				else koszty.put(wal, kwota);
			}
			
		}
		catch(Exception e)
		{
			log.error(e.getMessage(), e);
		}
		
		//Og�em koszty podr�y
		try
		{
			Map<String, FieldData> inne = values.get("DWR_OGOLEM_KOSZTY").getDictionaryData();
			size = inne.keySet().size()/5;
			for(int i=1; i <= size; i++){
				String wal = inne.get("OGOLEM_KOSZTY_CURRENCY_" + i).getEnumValuesData().getSelectedId();
				if (wal.equals(""))
					continue;
				double kurs = Double.parseDouble(inne.get("OGOLEM_KOSZTY_RATE_"+i).getStringData());
				int idkurs = Integer.parseInt(wal);
				kursy.put(wal, kurs);
				ids.put(wal, idkurs);
			}
			
		}
		catch(Exception e)
		{
			log.error(e.getMessage(), e);
		}
		List<Long> ryczaltIDs = new ArrayList<Long>();
		log.error("sumowaniekosztow: " + koszty);
		
		List<Long> kosztyOgolemIds = DwrUtils.extractIdFromDictonaryField(values, "OGOLEM_KOSZTY");
		List<PczRyczalt> pczRyczalts = new ArrayList<PczRyczalt>();
		
		BigDecimal kwotaOgolemWydana = new BigDecimal(0);
		boolean isOpened = true;
		try
		{
			if (!DSApi.isContextOpen())
			{   
				isOpened = false; 
				DSApi.openAdmin();    
			}
		
			for (Long id : kosztyOgolemIds)
			{
				try
				{
					PczRyczalt rycz = Finder.find(PczRyczalt.class, id);
					pczRyczalts.add(rycz);
				}
				catch (EdmException e)
				{
					log.warn(e.getMessage(), e);
				}
			}
			
			for (String kosztKey : koszty.keySet())
			{				
				PczRyczalt ryczalt = new PczRyczalt();
				ryczalt.setCurrency(Long.valueOf(kosztKey));
				ryczalt.setCurrency_value(koszty.get(kosztKey).setScale(2, RoundingMode.HALF_UP));		
				
				if (kursy.get(kosztKey) != null)
				{
					ryczalt.setRate(BigDecimal.valueOf(kursy.get(kosztKey)).setScale(4, RoundingMode.HALF_UP));
					ryczalt.setValue(ryczalt.getCurrency_value().multiply(ryczalt.getRate()).setScale(2, RoundingMode.HALF_UP));
					kwotaOgolemWydana = kwotaOgolemWydana.add(ryczalt.getValue());
				}
				Long ryczId = null;
				if (pczRyczalts.contains(ryczalt))
				{
					pczRyczalts.get(pczRyczalts.indexOf(ryczalt)).setCurrency_value(ryczalt.getCurrency_value());
					pczRyczalts.get(pczRyczalts.indexOf(ryczalt)).setRate(ryczalt.getRate());
					pczRyczalts.get(pczRyczalts.indexOf(ryczalt)).setValue(ryczalt.getValue());
					ryczId = pczRyczalts.get(pczRyczalts.indexOf(ryczalt)).getId();
					DSApi.context().begin();
					Persister.update(pczRyczalts.get(pczRyczalts.indexOf(ryczalt)));
					DSApi.context().commit();
				}
				else
				{
					Persister.create(ryczalt);
					ryczId = ryczalt.getId();
				}
				ryczaltIDs.add(ryczId);	
			}
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			if (DSApi.isContextOpen() && !isOpened) 
			{ 
				DSApi._close();    
			}
		}
		StringBuilder idList = new StringBuilder();
		for (Long id : ryczaltIDs) {
			if (!idList.toString().equals("")) {
				idList.append(",");
			}
			idList.append(id.toString());
		}
		values.put("DWR_OGOLEM_KOSZTY", new FieldData(Field.Type.STRING, idList));
		values.put("DWR_KWOTA_OGOLEM_WYDANA", new FieldData(Field.Type.MONEY, kwotaOgolemWydana.setScale(2, RoundingMode.HALF_UP)));
		
		BigDecimal kwotaZaliczki = new BigDecimal(0);
		try
		{
			if (fm.getKey("POBRANA_ZALICZKA") != null)
				kwotaZaliczki = (BigDecimal) fm.getKey("POBRANA_ZALICZKA");
			
		}
		catch (EdmException e)
		{
			log.warn(e.getMessage(), e);
		}
		
		BigDecimal roznica = kwotaZaliczki.subtract(kwotaOgolemWydana);
		
		if (roznica.compareTo(BigDecimal.ZERO) == 1)
		{
			values.put("DWR_DO_WYPLATY", new FieldData(Field.Type.MONEY, BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP)));
			values.put("DWR_DO_ZWROTU", new FieldData(Field.Type.MONEY, roznica.setScale(2, RoundingMode.HALF_UP)));
		}
		else if (roznica.compareTo(BigDecimal.ZERO) == -1)
		{
			values.put("DWR_DO_WYPLATY", new FieldData(Field.Type.MONEY, roznica.abs().setScale(2, RoundingMode.HALF_UP)));
			values.put("DWR_DO_ZWROTU", new FieldData(Field.Type.MONEY, BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP)));
		}
		else
		{
			values.put("DWR_DO_WYPLATY", new FieldData(Field.Type.MONEY, BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP)));
			values.put("DWR_DO_ZWROTU", new FieldData(Field.Type.MONEY, BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP)));
		}
	}
	
	
	@Override
	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue)
	{
		if (acceptationCn.equals("applicant_manager") && AssingUtils.assigneToApplicantManager(doc, assignable, openExecution)) {
			return true;
		}
		if (acceptationCn.equals("delegowany"))
			return AssingUtils.assignToDelegowany(doc, assignable, openExecution);
		return super.assignee(doc, assignable, openExecution, acceptationCn, fieldCnValue);
	}

	private void setTripTime(Map<String, FieldData> values) 
	{
		try
		{
			if (!DwrUtils.isNotNull(values,"DWR_CZAS_PODR_WYJAZD_DATA", "DWR_CZAS_PODR_WYJAZD_DATA_GR", 
					"DWR_CZAS_PODR_POWROT_DATA", "DWR_CZAS_PODR_POWROT_DATA_GR"))
				return;
			Date wyjazd = values.get("DWR_CZAS_PODR_WYJAZD_DATA").getDateData();
			Date wyjazdGr = values.get("DWR_CZAS_PODR_WYJAZD_DATA_GR").getDateData();
			Date powrot = values.get("DWR_CZAS_PODR_POWROT_DATA").getDateData();
			Date powrotGr = values.get("DWR_CZAS_PODR_POWROT_DATA_GR").getDateData();
			
			Date czasPoza = new Date(powrotGr.getTime() - wyjazdGr.getTime());
			Date czasPl = new Date(((powrot.getTime() - wyjazd.getTime()) - czasPoza.getTime()));
			FieldData fd1 = new FieldData(Field.Type.STRING, getTimeString(czasPl.getTime()));
			FieldData fd2 = new FieldData(Field.Type.STRING, getTimeString(czasPoza.getTime()));
			values.put("DWR_CZAS_PODR_KRAJ", fd1);
			values.put("DWR_CZAS_PODR_ZAGR", fd2);
		}
		catch(Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	
	public String getTimeString(long time){
		String ret="";
		long dni = time/(1000*60*60*24);
		long godzin = (time - (dni*1000*60*60*24))/(1000*60*60);
		long minut = (time - (dni*1000*60*60*24) - (godzin*1000*60*60))/(1000*60);
		ret= dni + " dni, " + godzin + " godzin, " + minut + " minut";
		return ret;
	}
	@Override
	public Map<String, Object> setMultipledictionaryValue(
			String dictionaryName, Map<String, FieldData> values) {
		Map<String, Object> ret = super.setMultipledictionaryValue(dictionaryName, values);
		log.error("validatedwrtestdictname: " + dictionaryName);
		if(dictionaryName.equals(KALKULACJA_KOSZTOW_DICT))
		{
			kalkulacjaKosztow(values, ret);
		}
		if(dictionaryName.equals(RYCZALTY_DOJAZDY_KOM_DICT))
		{
			ryczaltyZaDojazdy(values, ret);
		}
		if(dictionaryName.equals(KOSZTY_NOCLEGU_RYCZALTY_DICT))
		{
			ryczaltyZaNocleg(values, ret);
		}
		if(dictionaryName.equals(KOSZTY_PRZEJAZDU_DICT))
		{
			log.error("kutafix koszty przejazdu samochodu");
			log.error("kutafix " + values);
			kosztyPrzejazduSamochodem(values, ret);
		}
		if(dictionaryName.equals(KOSZTY_OGOLEM_DICT))
		{
			kosztyOgolem(values, ret);
		}
		log.error("validatedwrtestmapa: " + values);
		
		
		return ret;
	}

	private void kosztyOgolem(Map<String, FieldData> values,
			Map<String, Object> ret) {
		log.error("testpodliczaniakosztow: " + values);
		if(values.containsKey("OGOLEM_KOSZTY_CURRENCY_VALUE") && values.get("OGOLEM_KOSZTY_CURRENCY_VALUE") != null && values.containsKey("OGOLEM_KOSZTY_RATE") && values.get("OGOLEM_KOSZTY_RATE") != null){
			BigDecimal currval = values.get("OGOLEM_KOSZTY_CURRENCY_VALUE").getMoneyData();
			BigDecimal rate = values.get("OGOLEM_KOSZTY_RATE").getMoneyData();
			ret.put("OGOLEM_KOSZTY_VALUE", rate.multiply(currval));
		}
	}

	private void kosztyPrzejazduSamochodem(Map<String, FieldData> values,
			Map<String, Object> ret) {
		log.error("kutafixtest:", values);
		if( values.containsKey("KOSZTY_PRZEJAZDU_SAM_ENUM") && values.containsKey("KOSZTY_PRZEJAZDU_SAM_DAYS") && values.containsKey("KOSZTY_PRZEJAZDU_SAM_VALUE") &&
		    values.get("KOSZTY_PRZEJAZDU_SAM_ENUM")!=null && values.get("KOSZTY_PRZEJAZDU_SAM_DAYS")!=null && values.get("KOSZTY_PRZEJAZDU_SAM_VALUE")!=null)
		{
			log.error("kutafixtestpoif:", values);
			BigDecimal stawka = new BigDecimal(1.0);
			if(values.get("KOSZTY_PRZEJAZDU_SAM_ENUM").getEnumValuesData().getSelectedId().equals("55011") && values.containsKey("KOSZTY_PRZEJAZDU_SAM_ENUM2") && values.get("KOSZTY_PRZEJAZDU_SAM_ENUM2")!=null)
			{
				if(values.get("KOSZTY_PRZEJAZDU_SAM_ENUM2").getEnumValuesData().getSelectedId().equals("55041") || values.get("KOSZTY_PRZEJAZDU_SAM_ENUM2").getEnumValuesData().getSelectedId().equals("55042")){
					if(values.get("KOSZTY_PRZEJAZDU_SAM_ENUM2").getEnumValuesData().getSelectedId().equals("55041")) stawka = new BigDecimal(0.4);
					int ile = values.get("KOSZTY_PRZEJAZDU_SAM_DAYS").getIntegerData();
					BigDecimal koszt = values.get("KOSZTY_PRZEJAZDU_SAM_VALUE").getMoneyData();
					ret.put("KOSZTY_PRZEJAZDU_SAM_CURRENCY_VALUE", stawka.multiply(new BigDecimal(ile)).multiply(koszt));
				}
			}
			else if(values.get("KOSZTY_PRZEJAZDU_SAM_ENUM").getEnumValuesData().getSelectedId().equals("55012"))
			{
				int ile = values.get("KOSZTY_PRZEJAZDU_SAM_DAYS").getIntegerData();
				BigDecimal koszt = values.get("KOSZTY_PRZEJAZDU_SAM_VALUE").getMoneyData();
				ret.put("KOSZTY_PRZEJAZDU_SAM_CURRENCY_VALUE", stawka.multiply(new BigDecimal(ile)).multiply(koszt));
				List<String> lista = new ArrayList<String>();
				lista.add("55042");
				EnumValues ev = values.get("KOSZTY_PRZEJAZDU_SAM_ENUM2").getEnumValuesData();
				ev.setSelectedOptions(lista);
				ret.put("KOSZTY_PRZEJAZDU_SAM_ENUM2", ev);
			}
		}
	}

	private void ryczaltyZaNocleg(Map<String, FieldData> values,
			Map<String, Object> ret) 
	{
		try
		{
			if(values.containsKey("RYCZALTY_NOCLEG_DAYS") && values.containsKey("RYCZALTY_NOCLEG_CURRENCY_VALUE") && values.get("RYCZALTY_NOCLEG_DAYS")!=null && values.get("RYCZALTY_NOCLEG_CURRENCY_VALUE")!=null){
				int ile = values.get("RYCZALTY_NOCLEG_DAYS").getIntegerData();
				BigDecimal koszt = values.get("RYCZALTY_NOCLEG_CURRENCY_VALUE").getMoneyData();
				
				ret.put("RYCZALTY_NOCLEG_VALUE", koszt.multiply(new BigDecimal(ile)).multiply(new BigDecimal(0.25)).setScale(2,  RoundingMode.HALF_UP));
			}
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
		
	}

	private void ryczaltyZaDojazdy(Map<String, FieldData> values,
			Map<String, Object> ret) 
	{
		try
		{
			if(values.containsKey("RYCZALTY_DOJAZDY_KOM_DAYS") && values.containsKey("RYCZALTY_DOJAZDY_KOM_CURRENCY_VALUE") && values.get("RYCZALTY_DOJAZDY_KOM_DAYS")!=null && values.get("RYCZALTY_DOJAZDY_KOM_CURRENCY_VALUE")!=null){
				int ile = values.get("RYCZALTY_DOJAZDY_KOM_DAYS").getIntegerData();
				BigDecimal koszt = values.get("RYCZALTY_DOJAZDY_KOM_CURRENCY_VALUE").getMoneyData();
				ret.put("RYCZALTY_DOJAZDY_KOM_VALUE", koszt.multiply(new BigDecimal(ile)).multiply(new BigDecimal(0.1)).setScale(2, RoundingMode.HALF_UP));
			}
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
		
	}

	private void kalkulacjaKosztow(Map<String, FieldData> values, Map<String, Object> ret) 
	{
		try
		{
			if(values.containsKey("KALKULACJA_KOSZTOW_RATE") && values.containsKey("KALKULACJA_KOSZTOW_CURRENCY_VALUE") && values.get("KALKULACJA_KOSZTOW_RATE") != null && values.get("KALKULACJA_KOSZTOW_CURRENCY_VALUE")!=null){
				BigDecimal rate = values.get("KALKULACJA_KOSZTOW_RATE").getMoneyData();
				BigDecimal currencyValue = values.get("KALKULACJA_KOSZTOW_CURRENCY_VALUE").getMoneyData();
				ret.put("KALKULACJA_KOSZTOW_VALUE", rate.multiply(currencyValue).setScale(2, RoundingMode.HALF_UP));
			}
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
	}
}