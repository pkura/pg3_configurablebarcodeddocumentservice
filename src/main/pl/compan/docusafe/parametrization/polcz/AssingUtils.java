package pl.compan.docusafe.parametrization.polcz;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.polcz.MpkAccept.AcceptType;
import pl.compan.docusafe.parametrization.polcz.jbpm.PrepareForTripApplicationDecision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class AssingUtils
{
	private static final String BLOKADY_DIC_NAME = "BLOKADY";
	private static final String WZP_TABLE_NAME = "dsg_pcz_application_wzp";
	private static final String MULTIPLEVALUE_TABLE_NAME = "dsg_pcz_document_multiple_value_view";
	public static final String DZP_DEPARMENT_GUID = "BFA7C9163F7A04AF135AEF383B9BA1877C8";
	public final static String FINANCIAL_CONFIRMATION = "financial_confirmation";
	public final static String KIEROWNIK_ZAKLADU = "kierownik_zakladu";
	public final static String KIEROWNIK_JEDNOSTKI = "kierownik_jednostki";
	public final static String KOLUMNA_KZ = "subgroupID";
	public final static String KOLUMNA_KJ = "class_type_id";
	public final static String OPIS_MERYTORYCZNY = "opis_merytoryczny";
	public final static String FINANCIAL_SECTION = "financial_section";
	public final static String MANAGER_OF_UNIT = "manager_of_unit";
	public final static String MANAGER_OF_DEPARTMENT = "manager_of_department";
	public final static String DZP_DEPARTMENT = "dzp_department";
	public final static String VCE_KWESTOR = "VCE_KWESTOR";
	public final static String PROREKTOR = "PROREKTOR";
	public final static String DELEGOWANY = "DELEGOWANY";
	
	protected static Logger log = LoggerFactory.getLogger(AssingUtils.class);
	
	public static boolean assigneeForMpkAcceptances(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn)
	{
		boolean assigned = false;
		try
		{
			Long mpkId = (Long) openExecution.getVariable(AbstractPczDocumentLogic.MPK_VARIABLE_NAME);
			
			CentrumKosztowDlaFaktury mpkInstance = CentrumKosztowDlaFaktury.getInstance();
			CentrumKosztowDlaFaktury mpk = mpkInstance.find(mpkId);
			
			Integer divOrUserId = null;
			
			if (acceptationCn != null) {
				if (acceptationCn.equals(FINANCIAL_SECTION)) {
					Long fsId = mpk.getCustomsAgencyId().longValue();
					divOrUserId = Integer.valueOf(MpkAccept.find(AcceptType.FINANCIAL_SECTION, fsId).getCn());
				} else if (acceptationCn.equals(MANAGER_OF_DEPARTMENT)) {
					Long mdId = mpk.getSubgroupId().longValue();
					divOrUserId = Integer.valueOf(MpkAccept.find(AcceptType.MANAGER_OF_DEPARTMENT, mdId).getCn());
				} else if (acceptationCn.equals(MANAGER_OF_UNIT)) {
					Long muId = mpk.getClassTypeId().longValue();
					divOrUserId = Integer.valueOf(MpkAccept.find(AcceptType.MANAGER_OF_UNIT, muId).getCn());
				}
			}	
			
			try {
				if (divOrUserId != null) {
					if (divOrUserId > 0) {
						String division = DSDivision.findById(divOrUserId).getGuid();
						assignable.addCandidateGroup(division);
						AssigneeHandler.addToHistory(openExecution, doc, null, division);
						assigned = true;
					} else if (divOrUserId <0) {
						Long userId = Long.valueOf(divOrUserId*(-1));
						String user = DSUser.findById(userId).getName();
						assignable.addCandidateUser(user);
						AssigneeHandler.addToHistory(openExecution, doc, user, null);
						assigned = true;
					}
				}
			} catch (EdmException e) {
				log.error("Nie znaleziono osoby do przypisania o id:"+divOrUserId+" przypisano do admin'a");
				assignable.addCandidateUser("admin");
				assigned = true;
			}
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		
		return assigned;
	}
	
	
	public static boolean assigneeForDzpAcceptance(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn)
	{
		boolean assigned = false;
		try	{
			Long docId = doc.getId();
			
//			select wzp.DOCUMENT_ID,OSOBA_MERYTORYCZNA 
//			from dsg_pcz_document_multiple_value mul
//			JOIN dsg_pcz_application_wzp wzp ON mul.field_cn = 'BLOKADY' and mul.field_val=wzp.DOCUMENT_ID
			
			Set<Long> users = new HashSet<Long>();
			try {
				NativeCriteria nc = DSApi.context().createNativeCriteria(MULTIPLEVALUE_TABLE_NAME, "mul");
				
				CriteriaResult cr = 
					nc.setProjection(NativeExps.projection()
						.addProjection("wzp.OSOBA_MERYTORYCZNA"))
					.addJoin(NativeExps.leftJoin(WZP_TABLE_NAME, "wzp", "mul.field_val", "wzp.document_id"))
					.add(NativeExps.eq("mul.field_cn", BLOKADY_DIC_NAME))
					.add(NativeExps.eq("mul.document_id", docId))
					.criteriaResult();
				
				while (cr.next()) {
					users.add(cr.getLong(0, null));
				}
			} catch (EdmException e) {
				log.error(e.toString());
			}

			String user = null;
			
			if (!users.isEmpty()) {
				for (Long userId : users) {
					if (userId != null && (user = DSUser.findById(userId) != null ? DSUser.findById(userId).getName() : null)!=null) {
						assignable.addCandidateUser(user);
						AssigneeHandler.addToHistory(openExecution, doc, user, null);
						assigned = true;
					}
				}
			}

			if (!assigned) {
				assignable.addCandidateGroup(DZP_DEPARMENT_GUID);
				AssigneeHandler.addToHistory(openExecution, doc, null, DZP_DEPARMENT_GUID);
				assigned = true;
			}
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		
		return assigned;
	}
	
	

	public static boolean assigneeToFinancialConfirmation(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn)
	{
		boolean assigned = false;
		try
		{
			Long centrumId = (Long) openExecution.getVariable("costCenterId");
			PreparedStatement ps;
			ResultSet rs;
			ps = DSApi.context().prepareStatement("SELECT title FROM simple_erp_per_docusafe_bd_str_budzet_view WHERE id = ?");
			ps.setLong(1, centrumId);
			rs = ps.executeQuery();
			rs.next();
			String guid = DSDivision.findByName(rs.getString(1)).getGuid();

			List<String> users = new ArrayList<String>();
			List<String> divisions = new ArrayList<String>();
			if (AcceptanceCondition.find(acceptationCn, guid).size() == 0)
				acceptationCn = "financial_section";
			for (AcceptanceCondition accept : AcceptanceCondition.find(acceptationCn, guid))
			{
				if (accept.getUsername() != null && !users.contains(accept.getUsername()))
					users.add(accept.getUsername());
				if (accept.getDivisionGuid() != null && !divisions.contains(accept.getDivisionGuid()))
					divisions.add(accept.getDivisionGuid());
			}
			for (String user : users)
			{
				assignable.addCandidateUser(user);
				AssigneeHandler.addToHistory(openExecution, doc, user, null);
				assigned = true;
			}
			for (String division : divisions)
			{
				assignable.addCandidateGroup(division);
				AssigneeHandler.addToHistory(openExecution, doc, null, division);
				assigned = true;
			}
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		catch (SQLException e)
		{
			log.error(e.getMessage(), e);
		}
		return assigned;
	}

	public static boolean assignToVceKwestor (OfficeDocument doc, Assignable assignable, OpenExecution openExecution) {
		boolean assigned = false;
		try
		{
			Integer userId = (Integer) doc.getFieldsManager().getKey("VCE_KWESTOR");

			if (userId != null)
			{
				String username = DSUser.findById(userId.longValue()).getName();
				assignable.addCandidateUser(username);
				assigned = true;
				AssigneeHandler.addToHistory(openExecution, doc, username, null);
			}

		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		return assigned;
	}
	
	public static boolean assignToProrektor (OfficeDocument doc, Assignable assignable, OpenExecution openExecution) {
		boolean assigned = false;
		try
		{
			Integer userId = (Integer) doc.getFieldsManager().getKey("PRO_REKTOR");

			if (userId != null)
			{
				String username = DSUser.findById(userId.longValue()).getName();
				assignable.addCandidateUser(username);
				assigned = true;
				AssigneeHandler.addToHistory(openExecution, doc, username, null);
			}

		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		return assigned;
	}
	
	public static boolean assignToManager(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String columnName)
	{
		boolean assigned = false;
		try
		{
			@SuppressWarnings("unchecked")
			List<Long> dictionaryIds = (List<Long>) doc.getFieldsManager().getKey("MPK");
			PreparedStatement ps;
			ResultSet rs;
			Long centrumId = (Long) openExecution.getVariable("costCenterId");
			Integer userId = null;

			for (Long id : dictionaryIds)
			{
				ps = DSApi.context().prepareStatement("SELECT " + columnName + " FROM DSG_CENTRUM_KOSZTOW_FAKTURY WHERE centrumId = ? AND id = ?");
				ps.setLong(1, centrumId);
				ps.setLong(2, id);
				rs = ps.executeQuery();
				if (rs.next())
					userId = (Integer) doc.getFieldsManager().getKey("VCW_KWESTOR");
			}
			if (userId != null)
			{
				String username = DSUser.findById(userId.longValue()).getName();
				assignable.addCandidateUser(username);
				assigned = true;
				AssigneeHandler.addToHistory(openExecution, doc, username, null);
			}

		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		catch (SQLException e)
		{
			log.error(e.getMessage(), e);
		}
		return assigned;
	}


	public static boolean assignToDelegowany(OfficeDocument doc, Assignable assignable, OpenExecution openExecution) {
		boolean assigned = false;
		try
		{
			FieldsManager fm = doc.getFieldsManager();
			String id = null;
			if (fm.getDocumentKind().getCn().equals("application_wde") || fm.getDocumentKind().getCn().equals("application_wz")) {
				id = ((List<Long>) doc.getFieldsManager().getKey("DELEGOWANY")).get(0).toString();
			}
			Long userId = Long.valueOf(id.substring(0, (id.length() - 7)));

			if (userId != null)
			{
				String username = DSUser.findById(userId).getName();
				assignable.addCandidateUser(username);
				assigned = true;
				AssigneeHandler.addToHistory(openExecution, doc, username, null);
			}

		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		return assigned;
	}
	
	public static boolean assigneToApplicantManager(OfficeDocument doc, Assignable assignable, OpenExecution openExecution) {
		Object guid = openExecution.getVariable(PrepareForTripApplicationDecision.MANAGER_GUID_JBPM_VARIABLE);
		
		if (guid!=null) {
			openExecution.removeVariable(PrepareForTripApplicationDecision.MANAGER_GUID_JBPM_VARIABLE);
			if (StringUtils.isNotEmpty(guid.toString())) {
				try {
					assignable.addCandidateGroup(guid.toString());
					AssigneeHandler.addToHistory(openExecution, doc, null, guid.toString());
					return true;
				} catch (EdmException e) {
					log.error(e.toString());
				}
			}
		}
		return false;
	}
	
}
