package pl.compan.docusafe.parametrization.polcz;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import org.apache.commons.dbcp.BasicDataSource;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ExportInvoiceToSimpleERP
{
	private static final Logger log = LoggerFactory.getLogger(ExportInvoiceToSimpleERP.class);

	static public void export(String xml) throws EdmException
	{

		log.info("EXPORT invoice xml: {}", xml);
		Connection con = null;
		CallableStatement call = null;
		try
		{
			String erpUser, erpPassword, erpUrl, erpDataBase, erpSchema, proc_export;
			erpUser = Docusafe.getAdditionProperty("erp.database.user");
			erpPassword = Docusafe.getAdditionProperty("erp.database.password");
			erpUrl = Docusafe.getAdditionProperty("erp.database.url");
			erpDataBase = Docusafe.getAdditionProperty("erp.database.dataBase");
			erpSchema = Docusafe.getAdditionProperty("erp.database.schema");
			// up_gsd_ksiegapodawcza
			proc_export = Docusafe.getAdditionProperty("erp.database.export.faktura");
			if (proc_export == null)
				proc_export = "docusafe_imp_dokzak2";
			
			if (erpUser == null || erpPassword == null || erpUrl == null)
				throw new EdmException("Błąd eksportu faktury, brak parametrów połaczenia do bazy ");
			
			log.info("user: {}, password: {}, url: {}", erpUser, erpPassword, erpUrl);
			BasicDataSource ds = new BasicDataSource();
			ds.setDriverClassName(Docusafe.getDefaultDriver("sqlserver_2000"));
			ds.setUrl(erpUrl);
			ds.setUsername(erpUser);
			ds.setPassword(erpPassword);
			con = ds.getConnection();

			call = con.prepareCall("{? = call [" + erpDataBase + "].[" + erpSchema + "].[" + proc_export + "] (?,?,?,?)}");

			Integer return_value = null;
			BigDecimal dokzak_id = null, error_id = null;
			String error_description = null;
			
			// return_value
			call.registerOutParameter(1, java.sql.Types.INTEGER);
			// @xml
			call.setObject(2, xml);
			// dokzak_id
			call.registerOutParameter(3, java.sql.Types.VARCHAR);
			// error_id
			call.registerOutParameter(4, java.sql.Types.VARCHAR);
			// error_Description
			call.registerOutParameter(5, java.sql.Types.VARCHAR);

			
			int resultNum = 0;

			while (true)
			{
				boolean queryResult;
				int rowsAffected;

				if (1 == ++resultNum)
				{
					try
					{
						queryResult = call.execute();
						return_value = call.getInt(1);
						dokzak_id = call.getBigDecimal(3);
						error_id = call.getBigDecimal(4);
						error_description = call.getString(5);
						
						log.debug("EXPORT invoice xml: return_value: {}, dokzak_id: {}, error_id: {}, error_description: {}", return_value, dokzak_id, error_id, error_description);

						
							
					}
					catch (Exception e)
					{
						// Process the error
						log.error("Result " + resultNum + " is an error: " + e.getMessage());

						// When execute() throws an exception, it may just
						// be that the first statement produced an error.
						// Statements after the first one may have
						// succeeded. Continue processing results until
						// there
						// are no more.
						continue;
					}
				}
				else
				{
					try
					{
						queryResult = call.getMoreResults();
					}
					catch (Exception e)
					{
						// Process the error
						log.error("1 != ++resultNum: Result " + resultNum + " is an error: " + e.getMessage());

						// When getMoreResults() throws an exception, it
						// may just be that the current statement produced
						// an error.
						// Statements after that one may have succeeded.
						// Continue processing results until there
						// are no more.
						continue;
					}
				}

				if (queryResult)
				{
					ResultSet rs = call.getResultSet();

					// Process the ResultSet
					log.debug("Result " + resultNum + " is a ResultSet: " + rs);
					ResultSetMetaData md = rs.getMetaData();

					int count = md.getColumnCount();
					log.debug("<table border=1>");
					log.debug("<tr>");
					for (int i = 1; i <= count; i++)
					{
						log.debug("<th>");
						log.debug("GEtColumnLabeL: {}", md.getColumnLabel(i));
					}
					log.debug("</tr>");
					while (rs.next())
					{
						log.debug("<tr>");
						for (int i = 1; i <= count; i++)
						{
							log.debug("<td>");
							log.debug("Get.Object: {}", rs.getObject(i));
						}
						log.debug("</tr>");
					}
					log.debug("</table>");
					rs.close();
				}
				else
				{
					rowsAffected = call.getUpdateCount();

					// No more results
					if (-1 == rowsAffected)
					{
						--resultNum;
						break;
					}

					// Process the update count
					log.debug("Result " + resultNum + " is an update count: " + rowsAffected);
				}
			}

			log.debug("Done processing " + resultNum + " results");
			if (resultNum == 0)
			{

			}
			if (dokzak_id == null)
				throw new EdmException("Procedura importujaca fatkure (" + proc_export + ") do Simple.ERP nie zwrocila identyfikatora dokzak_id !!! " +
						" Faktura: " + xml + ", Zwrocne wartosci z procedury: return_value: " + return_value + ", dokzak_id: " + dokzak_id + ", error_id: " + error_id + ", error_description: " + error_description);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException("Błąd akcpetacji wniosku :" + e.getMessage());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					con.close();
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
					throw new EdmException("Błąd akcpetacji wniosku :" + e.getMessage());
				}
			}
		}
		
			

	}

}
