package pl.compan.docusafe.parametrization.polcz;

import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.hibernate.SQLQuery;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.db.criteria.NativeProjection;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.PersonDictionary;
import pl.compan.docusafe.core.dockinds.field.AbstractDictionaryField;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.field.DocumentField;
import pl.compan.docusafe.core.dockinds.field.NonColumnField;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.util.DSCountry;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class CpvDictionary extends DwrDictionaryBase {

	protected static final String ERP_FLAG = " [erp]";
	private static Logger log = LoggerFactory.getLogger(CpvDictionary.class);	
	
	@Override
	public int remove(String id) throws EdmException {
		return -1;
	}

	public int update(Map<String, FieldData> values) throws EdmException 
	{
		log.debug("update");
		long ret = -1;
		try
		{
			boolean contextOpened = true;
			Statement stat = null;
			ResultSet result = null;
			if (values.get("ID") != null)
			{
				if (!DSApi.isContextOpen())
				{
					DSApi.openAdmin();
					contextOpened = false;
				}
				stat = DSApi.context().createStatement();
	
				StringBuilder select = new StringBuilder("select base from dsg_cpv_kod_nazwa where id = ").append(values.get("ID"));
	
				result = stat.executeQuery(select.toString());
				while (result.next())
				{
					Integer base = result.getInt("base");
					if (base.equals(1))
					{
						long newId =  super.add(values);
						values.get("ID").setData(newId);
						return Integer.parseInt(newId+"");
					}
					else if (base == null || base.equals(0))
					{
						return super.update(values);
					}
				}
			}
		}
		catch (Exception e)
		{
			return -1;
		}
		return -1;
	}

	@Override
	public long add(Map<String, FieldData> values)
	{
		log.debug("add");
		return -1;
	}

}
