package pl.compan.docusafe.parametrization.polcz;

import java.util.Date;

public class PodrozItem {
	
	private String miWy, miPr, srodki;
	private String wyj, prz;
	
	
	public PodrozItem(){
		
	}
	
	public PodrozItem(String miWy, String miPr, String srodki, String wyj,
			String prz) {
		super();
		this.miWy = miWy;
		this.miPr = miPr;
		this.srodki = srodki;
		this.wyj = wyj;
		this.prz = prz;
	}
	public String getMiWy() {
		return miWy;
	}
	public void setMiWy(String miWy) {
		this.miWy = miWy;
	}
	public String getMiPr() {
		return miPr;
	}
	public void setMiPr(String miPr) {
		this.miPr = miPr;
	}
	public String getSrodki() {
		return srodki;
	}
	public void setSrodki(String srodki) {
		this.srodki = srodki;
	}
	public String getWyj() {
		return wyj;
	}
	public void setWyj(String wyj) {
		this.wyj = wyj;
	}
	public String getPrz() {
		return prz;
	}
	public void setPrz(String prz) {
		this.prz = prz;
	}
	
	

}
