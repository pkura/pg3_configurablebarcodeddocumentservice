package pl.compan.docusafe.parametrization.polcz;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SetEachForkCostVaraibles implements ExternalActivityBehaviour
{
	private static final Logger log = LoggerFactory.getLogger(SetEachForkCostVaraibles.class);
	
	public void execute(ActivityExecution execution) throws Exception
	{
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Document document = Document.find(docId);
		List<String> argSets = new LinkedList<String>();
		List<Long> dictionaryIds = (List<Long>) document.getFieldsManager().getKey("DSG_CENTRUM_KOSZTOW_FAKTURY_DD");
		if (dictionaryIds != null)
		{
			String guid = (String)execution.getVariable(ASSIGN_DIVISION_GUID_PARAM);
			argSets.add("manager_of_unit:"+guid);
		}
		dictionaryIds = (List<Long>) document.getFieldsManager().getKey("DSG_CENTRUM_KOSZTOW_FAKTURY_DNB");
		if (dictionaryIds != null)
		{
			try
			{
				String projectId = null;
				for (Long di : dictionaryIds)
				{
					// pobranie id_projektu z ktorego pokrywany jest koszt
					projectId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY_DNB").getValues(di.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_DNB_CENTRUMID")).getSelectedOptions().get(0);
					
					if (!argSets.contains(projectId))
						argSets.add(projectId);
				}
			}
			catch (EdmException e)
			{
				log.error(e.getMessage(), e);
			}
		}	
		if (argSets.size() == 0)
			throw new EdmException("Brak pozycji budzetowych!!!");
		execution.setVariable("ids", argSets);
		execution.setVariable("count", argSets.size());
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		// TODO Auto-generated method stub

	}
}