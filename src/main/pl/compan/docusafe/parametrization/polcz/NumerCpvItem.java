package pl.compan.docusafe.parametrization.polcz;

public class NumerCpvItem {
	
	private String numer, nazwa;
	
	

	public NumerCpvItem(String numer, String nazwa) {
		super();
		this.numer = numer;
		this.nazwa = nazwa;
	}
	
	public NumerCpvItem(){
		
	}

	public String getNumer() {
		return numer;
	}

	public void setNumer(String numer) {
		this.numer = numer;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	
	

}
