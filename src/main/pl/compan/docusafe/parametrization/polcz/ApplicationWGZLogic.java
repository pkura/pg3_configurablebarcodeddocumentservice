package pl.compan.docusafe.parametrization.polcz;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;


public class ApplicationWGZLogic extends AbstractBudgetApplicationLogic
{	
	private static final long serialVersionUID = 1L;
	private static ApplicationWGZLogic instance;
	
	public static ApplicationWGZLogic getInstance()
	{
		if (instance == null)
			instance = new ApplicationWGZLogic();
		return instance;
	}
	
	@Override
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) {
		Map<String, Object> result = super.setMultipledictionaryValue(dictionaryName, values);

		if (dictionaryName.equals("UCZESTNICY")) {
			setKwotaBasedOnStawkaAndIlosc(dictionaryName, values, result);
		}
		return result;
	}

	private void setKwotaBasedOnStawkaAndIlosc(String dictionaryName, Map<String, FieldData> values, Map<String, Object> result) {
		if (dictionaryName.equals("UCZESTNICY") && values.get("UCZESTNICY_STAWKA_DZIENNA").getData() != null
				&& values.get("UCZESTNICY_ILOSC_DNI").getData() != null) {
			BigDecimal stawka = values.get("UCZESTNICY_STAWKA_DZIENNA").getMoneyData();
			BigDecimal iloscDni = new BigDecimal(values.get("UCZESTNICY_ILOSC_DNI").getIntegerData());
			BigDecimal kwota = stawka.multiply(iloscDni);
			result.put("UCZESTNICY_KWOTA", kwota);
		}
	}
	
	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) {
		
		pl.compan.docusafe.core.dockinds.dwr.Field msgField = super.validateDwr(values, fm);
		StringBuilder msgBuilder = new StringBuilder();
		if (msgField != null)
			msgBuilder.append(msgField.getLabel());

		setSumaKosztow(values);
		
		if (msgBuilder.length() > 0) 
		{
			values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
			return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(),
					pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
		}
		return null;
		
	}

	protected void setSumaKosztow(Map<String, FieldData> values) {
		try 
		{
			Map<String, FieldData> uczestnicy = values.get("DWR_UCZESTNICY").getDictionaryData();
			String suffix = null;
			BigDecimal suma = new BigDecimal(0);
			if (uczestnicy != null) {
//				String stawkaCN = null;
//				String iloscDniCN = null;
//				String kwotaCN = null;
				for (String cn : uczestnicy.keySet()){
					if(cn.contains("KWOTA") && uczestnicy.get(cn).getData() != null) {
//						suffix = cn.substring(cn.lastIndexOf("_"));
//						stawkaCN = "UCZESTNICY_STAWKA_DZIENNA".concat(suffix);
//						iloscDniCN = "UCZESTNICY_ILOSC_DNI".concat(suffix);
//						kwotaCN = "UCZESTNICY_KWOTA".concat(suffix);
//						if (uczestnicy.get(iloscDniCN).getData() != null) {
							BigDecimal kwota = uczestnicy.get(cn).getMoneyData();
							suma = suma.add(kwota);
//						}
					}
				}
				values.get("DWR_SUMA").setMoneyData(suma);
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}
	
	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn,
			String fieldCnValue) 
	{
		log.debug("Start special assignee operations for acceptationCn: {}, fieldCnValue: {}", acceptationCn, fieldCnValue);
		
		boolean assigned = super.assignee(doc, assignable, openExecution, acceptationCn, fieldCnValue);
		
		if (!assigned)
		{
			// akceptacja vice kwestora
			if (AssingUtils.PROREKTOR.equals(acceptationCn))
				assigned = AssingUtils.assignToProrektor(doc, assignable, openExecution);
		}
		
		return assigned;
	}
	
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		try
		{
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			fm.initialize();
			
			// czas wygenerowania metryki
			values.put("GENERATE_DATE", DateUtils.formatCommonDate(new java.util.Date()).toString());
			
			// generowanie akceptacji
			setAcceptances(docId, values);
			
			// pozycje budzetu
			values.put("BUDZET", getBudgetItems(fm));
			
			// data dokumentu
			values.put("DOC_DATE", DateUtils.formatCommonDate(doc.getCtime()));
			
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
	}
	
}