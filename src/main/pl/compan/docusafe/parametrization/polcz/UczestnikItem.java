package pl.compan.docusafe.parametrization.polcz;

public class UczestnikItem {
	
	String imieNazwisko;
	
	public UczestnikItem(){
		
	}

	public UczestnikItem(String imieNazwisko) {
		super();
		this.imieNazwisko = imieNazwisko;
	}

	public String getImieNazwisko() {
		return imieNazwisko;
	}

	public void setImieNazwisko(String imieNazwisko) {
		this.imieNazwisko = imieNazwisko;
	}
	
	

}
