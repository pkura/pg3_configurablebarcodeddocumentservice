package pl.compan.docusafe.parametrization.umwawa;

import java.util.Date;

public class ImportedRequest {

	private String imageArray;
	private String name;
	private String orginalPath;
	private Date submitDate;
	public String getImageArray() {
		return imageArray;
	}
	public void setImageArray(String imageArray) {
		this.imageArray = imageArray;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOrginalPath() {
		return orginalPath;
	}
	public void setOrginalPath(String orginalPath) {
		this.orginalPath = orginalPath;
	}
	public Date getSubmitDate() {
		return submitDate;
	}
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}
	public ImportedRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
