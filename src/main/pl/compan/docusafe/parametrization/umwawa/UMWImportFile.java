package pl.compan.docusafe.parametrization.umwawa;

import java.io.File;
import java.util.Date;

import org.apache.commons.codec.binary.Base64;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.service.imports.dsi.DSIAttribute;
import pl.compan.docusafe.service.imports.dsi.DSIBean;
import pl.compan.docusafe.service.imports.dsi.DSIDefinitions;
import pl.compan.docusafe.service.imports.dsi.DSIImportHandler;
import pl.compan.docusafe.service.imports.dsi.DSIManager;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;



public class UMWImportFile 
{
	private static final Logger log = LoggerFactory.getLogger(UMWImportFile.class); 
	public ImportFileResponse exportFileDoDS(ImportedRequest request)
	{		
		try
		{
			log.debug("START IMPORT UMW");
			DSIAttribute attr = new DSIAttribute();
			attr.setAttributeCn(AktNotarialnyLogic.PRZED_OCR_CN);
			attr.setAttributeValue("1");
			DSApi.openAdmin();
			DSApi.context().begin();
	        DSIBean bean = new DSIBean();								
			bean.setDocumentKind(DocumentLogicLoader.AKT_NOTARIALNY);
			bean.setImportKind(DSIImportHandler.ARCHIVE_ONLY_IMPORT);
			bean.setImportStatus(1);
			bean.setSubmitDate(new Date());
			bean.setBoxCode(DSIDefinitions.CODE_NO_NEED_BOX);
			int l = request.getName().length();
			bean.setUserImportId(request.getName().substring(0,l-4));
			bean.setErrCode("");
			bean.setAttributes(java.util.Arrays.asList(attr));
			bean.setImageName(request.getName());
			bean.setImageArray(Base64.decodeBase64(request.getImageArray()));
			log.debug("Start plik do folderu ORC-a");
			File file = FileUtils.decodeByBase64ToFile(request.getImageArray());
			File to = new File(Docusafe.getAdditionProperty("ocr.toOcr"),request.getName());
			log.debug("Kopiuje plik do folderu ORC-a to "+ to.getAbsolutePath());
			FileUtils.copyFileToFile(file, to);
			
			DSIManager manager = new DSIManager();
			manager.saveDSIBean(bean, DSApi.context().session().connection());	
			log.error("SAVE BEAN file name : {}",request.getName());
	        DSApi.context().commit();
		}
		catch(Exception e)
		{
			log.error(e.getMessage(), e);
			return new ImportFileResponse("FATAL ERROR",0,e.getMessage());
		}
		finally
		{
			try 
			{
				DSApi.close();
			} catch (EdmException e) {
				log.error(e.getMessage(), e);
			}
		}
		return new ImportFileResponse(null,1,"IMPORT Zakonczony pomyslnie");
	}

}
