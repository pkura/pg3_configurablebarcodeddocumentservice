package pl.compan.docusafe.parametrization.umwawa;

public class ImportFileResponse 
{
	public ImportFileResponse(String errCode, Integer importStatus,
			String errMsg) {
		super();
		this.errCode = errCode;
		this.importStatus = importStatus;
		this.msg = errMsg;
	}
	private String errCode;
	private Integer importStatus;
	private String msg;
	public ImportFileResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getErrCode() {
		return errCode;
	}
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
	public Integer getImportStatus() {
		return importStatus;
	}
	public void setImportStatus(Integer importStatus) {
		this.importStatus = importStatus;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
    
}
