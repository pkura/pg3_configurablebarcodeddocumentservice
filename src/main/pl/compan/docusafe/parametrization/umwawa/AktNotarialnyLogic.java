package pl.compan.docusafe.parametrization.umwawa;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.DfInst;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.service.imports.dsi.ImportHelper;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import warszawa.um.signum.Service1Locator;
import warszawa.um.signum.Service1SoapStub;
import warszawa.um.signum.Sprawa;

public class AktNotarialnyLogic extends AbstractDocumentLogic {

	private static final Logger log = LoggerFactory.getLogger(AktNotarialnyLogic.class);
	public final static String NUMER_KANCELARYJNY_CN = "NUMER_KANCELARYJNY";
	public final static String DZIELNICA_CN = "DZIELNICA";
	public final static String DATA_WPLYWU_CN = "DATA_WPLYWU";
	public final static String DATA_ZAWARCIA_CN = "DATA_ZAWARCIA";
	public final static String REPORTORIUM_AKTU_CN = "REPORTORIUM_AKTU";
	public final static String BARCODE_CN = "BARCODE";
	public final static String NAZWA_KANCELARII_CN = "NAZWA_KANCELARII";
	public final static String PRZED_OCR_CN = "PRZED_OCR";
	private static String signumLogin = null;
	private  static String signumHaslo = null;
	
	private static final AktNotarialnyLogic instance = new AktNotarialnyLogic();
	
	public static AktNotarialnyLogic getInstance() 
	{
		return instance;
	}
	
	public static String getSignumLogin()
	{
		if(signumLogin == null)
			signumLogin = Docusafe.getAdditionProperty("signumLogin");
		return signumLogin;
	}
	
	public static String getSignumHaslo()
	{
		if(signumHaslo == null)
			signumHaslo = Docusafe.getAdditionProperty("signumHaslo");
		return signumHaslo;
	}
	
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
		
		if(fm.getValue(DATA_WPLYWU_CN) != null)
		{
			folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(fm.getValue(DATA_WPLYWU_CN)));
			folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(fm.getValue(DATA_WPLYWU_CN)));
		}
		
		document.setFolder(folder);
		
		if(fm.getValue(REPORTORIUM_AKTU_CN) != null)
		{
			StringBuffer sb = new StringBuffer();
			sb.append(fm.getValue(REPORTORIUM_AKTU_CN).toString());
			document.setTitle(sb.toString());
		//	document.setDescription(sb.toString());
		}
		else if(fm.getValue(BARCODE_CN) != null)
		{
			document.setTitle((String) fm.getValue(BARCODE_CN));
			//document.setDescription((String) fm.getValue(BARCODE_CN));
		}
		else
		{
			document.setTitle("B��D BARCODU");
		//	document.setDescription("B��D BARCODU");
		}
		log.error("WYKONUJE fireArchiveListeners");
		as.fireArchiveListeners(document);
	}

	public void documentPermissions(Document document) throws EdmException 
	{
		/** ogolne */
		
		Set<PermissionBean> perms = new HashSet<PermissionBean>();
		
		perms.add(new PermissionBean(ObjectPermission.READ, "AKT_READ", ObjectPermission.GROUP, document.getDocumentKind().getName()+" - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "AKT_READ_ATT", ObjectPermission.GROUP, document.getDocumentKind().getName()+" - odczyt (zalacznik)"));
        
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "AKT_MODIFY", ObjectPermission.GROUP, document.getDocumentKind().getName()+" - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "AKT_MODIFY_ATT", ObjectPermission.GROUP, document.getDocumentKind().getName()+" - modyfikacja (zalacznik)"));
        
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        
        if(fm.getKey(DZIELNICA_CN) != null)
        {
	        for(Integer id : (List<Integer>)fm.getKey(DZIELNICA_CN))
	        {
	        	/** uprawnienia dla dzielnic */
	        	
	        	String dzielnicaCn = fm.getField(DZIELNICA_CN).getEnumItem(id).getCn();
	        	String dzielnicaTitle = fm.getField(DZIELNICA_CN).getEnumItem(id).getTitle();
	        	
	        	perms.add(new PermissionBean(ObjectPermission.READ, "AKT_READ_"+dzielnicaCn, ObjectPermission.GROUP, document.getDocumentKind().getName()+" "+dzielnicaTitle+" - odczyt"));
	            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "AKT_READ_ATT_"+dzielnicaCn, ObjectPermission.GROUP, document.getDocumentKind().getName()+" "+dzielnicaTitle+" - odczyt (zalacznik)"));
	            
	            perms.add(new PermissionBean(ObjectPermission.MODIFY, "AKT_MODIFY_"+dzielnicaCn, ObjectPermission.GROUP, document.getDocumentKind().getName()+" "+dzielnicaTitle+" - modyfikacja"));
	            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "AKT_MODIFY_ATT_"+dzielnicaCn, ObjectPermission.GROUP, document.getDocumentKind().getName()+" "+dzielnicaTitle+" - modyfikacja (zalacznik)"));
	            
	        }
        }
		
		setUpPermission(document, perms);
	}
	private String left(String value,Integer length)
	{
		if(value == null)
			return value;
		else
			return StringUtils.left(value, length);
	}
    @Override
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException 
    {
    	
        if(documentId != null)
        {
            FieldsManager fm = kind.getFieldsManager(documentId);
           if(values.get(BARCODE_CN) != null && !values.get(BARCODE_CN).equals(fm.getValue(BARCODE_CN)))
            {
        		try
        		{
        			Service1SoapStub binding = (Service1SoapStub) new Service1Locator().getService1Soap();

        			String login = AktNotarialnyLogic.getSignumLogin();
        			String haslo =AktNotarialnyLogic.getSignumHaslo();
        			Sprawa spr = binding.getSprawaZaSkan((String) values.get("BARCODE")," ", login, haslo);			
        			values.put(AktNotarialnyLogic.DATA_WPLYWU_CN, spr.getPis_dataWplywu()); 
        			values.put(AktNotarialnyLogic.REPORTORIUM_AKTU_CN, left(spr.getPis_tresc(),50));
        			values.put(AktNotarialnyLogic.NUMER_KANCELARYJNY_CN, spr.getPis_nrKancelaryjny());			
        			ImportHelper.correctDateFormat(values, AktNotarialnyLogic.DATA_WPLYWU_CN);
        			
        			
        			//kancelaria
        			final String miasto = spr.getKor_miasto();
        			final String imie = spr.getKor_imie();
        			final String nazwisko = spr.getKor_nazwisko();
        			final String firma = spr.getKor_nazwaFirmy();
        			final String nip = spr.getKor_nip();
        			final String ulica = spr.getKor_ulica();
        			
        			Map<String,Object> searchValues = new HashMap<String, Object>();
        			searchValues.put("imie", imie);
        			searchValues.put("nazwisko", nazwisko);
        			searchValues.put("miejscowosc", miasto);
        			searchValues.put("name", firma);
        			searchValues.put("nip", nip);
        			searchValues.put("ulica", ulica);
        			
        			List<DfInst> kancelarie = DfInst.find(searchValues);
        			if(kancelarie != null && !kancelarie.isEmpty())
        			{
        				values.put(AktNotarialnyLogic.NAZWA_KANCELARII_CN, kancelarie.get(0).getId());
        			}
        			else
        			{
        				DfInst kan = new DfInst();
        				kan.setMiejscowosc(miasto);
        				kan.setImie(imie);
        				kan.setNazwisko(nazwisko);
        				kan.setName(firma);
        				kan.setNip(nip);
        				kan.setUlica(ulica);				
        				kan.create();
        				values.put(AktNotarialnyLogic.NAZWA_KANCELARII_CN, kan.getId());
        			}			
        		}
        		catch (Exception e) {
        			log.error(e.getMessage(),e);
					throw new EdmException("B��d po��czenia do SIGNUM");
				}
            }
        }
    }
    
    
}
