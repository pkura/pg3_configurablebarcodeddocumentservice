package pl.compan.docusafe.parametrization.umwawa;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.dictionary.DfInst;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.imports.dsi.DSIBean;
import pl.compan.docusafe.service.imports.dsi.DSIImportHandler;
import pl.compan.docusafe.service.imports.dsi.DSIManager;
import pl.compan.docusafe.service.imports.dsi.ImportHelper;
import pl.compan.docusafe.webwork.FormFile;
import warszawa.um.signum.Service1Locator;
import warszawa.um.signum.Service1SoapStub;
import warszawa.um.signum.Sprawa;


/** to naprawde wczesna wersja tej klasy, nie wiemy do konca jakie dane zwraca ws
 * klasa zostanie poprawiona po ustaleniu tej informacjii
 * nie zworowac sie tym kodem*/
public class UMWawaDSIHandler extends DSIImportHandler {

	private static final Log LOG = LogFactory.getLog(UMWawaDSIHandler.class);
	
	public String getDockindCn() 
	{
		return DocumentLogicLoader.AKT_NOTARIALNY;
	}
	
	protected boolean isStartProces()
	{
		return true;
	}
	
	private String left(String value,Integer length)
	{
		if(value == null)
			return value;
		else
			return StringUtils.left(value, length);
	}

	protected void prepareImport() throws Exception 
	{
		
		//webservice
		Service1SoapStub binding = (Service1SoapStub) new Service1Locator().getService1Soap(); 
		try
		{
			log.debug("Sprawdza rodzaj importu"); 
			log.debug("PRZED_OCR = "+Boolean.parseBoolean(((String)values.get(AktNotarialnyLogic.PRZED_OCR_CN))));
			if(values.get(AktNotarialnyLogic.PRZED_OCR_CN) != null)
			{
				System.out.println(values.get(AktNotarialnyLogic.PRZED_OCR_CN).getClass());
			}
			if(values.get(AktNotarialnyLogic.PRZED_OCR_CN) != null && 
					Integer.parseInt((String)values.get(AktNotarialnyLogic.PRZED_OCR_CN)) > 0)
			{
				log.debug("IMPORT PRZED OCR");
				values.put(AktNotarialnyLogic.PRZED_OCR_CN,true);
				return;
			}
			else
			{
				log.debug("IMPORT PO OCR set OCR na false");
				values.put(AktNotarialnyLogic.PRZED_OCR_CN,false);
			}
			DSIManager manager = new DSIManager();
			log.debug("SZUKA DOKUMENTU POWIAZANEGO USER_IMPORT_ID"+dsiBean.getUserImportId());
			 Collection<DSIBean> dsbs = manager.findDSIBeanByUserImportId(DSApi.context().session().connection(),dsiBean.getUserImportId(),true);
			 for (DSIBean dsiBean : dsbs) {
				 if(dsiBean.getDocumentId()!= null && dsiBean.getDocumentId() > 1)
				 {
					 System.out.println("Docid "+dsiBean.getDocumentId());
					 importDocument = Document.find(dsiBean.getDocumentId());
				 }
			}
			 
			 if(true)
			 {
				String login = AktNotarialnyLogic.getSignumLogin();
				String haslo =AktNotarialnyLogic.getSignumHaslo();
				log.debug("Pobiera sprawe dla BRACODE =" + values.get("BARCODE"));
				Sprawa spr = binding.getSprawaZaSkan((String) values.get("BARCODE")," ", login, haslo);			
				values.put(AktNotarialnyLogic.DATA_WPLYWU_CN, spr.getPis_dataWplywu()); 
				values.put(AktNotarialnyLogic.REPORTORIUM_AKTU_CN, left(spr.getPis_tresc(),50));
				values.put(AktNotarialnyLogic.NUMER_KANCELARYJNY_CN, spr.getPis_nrKancelaryjny());	
				log.debug("POBRAL SPRAWE data {} , nr.Rep {}, nr. Kanc {}",spr.getPis_dataWplywu(),left(spr.getPis_tresc(),50),spr.getPis_nrKancelaryjny());
	//			values.put(AktNotarialnyLogic.DATA_WPLYWU_CN, "12-12-2011"); 
	//			values.put(AktNotarialnyLogic.REPORTORIUM_AKTU_CN, "test");
	//			values.put(AktNotarialnyLogic.NUMER_KANCELARYJNY_CN, "123");	
				ImportHelper.correctDateFormat(values, AktNotarialnyLogic.DATA_WPLYWU_CN);
				
				
				//kancelaria
				
				final String miasto = spr.getKor_miasto();
				final String imie = spr.getKor_imie();
				final String nazwisko = spr.getKor_nazwisko();
				final String firma = spr.getKor_nazwaFirmy();
				final String nip = spr.getKor_nip();
				final String ulica = spr.getKor_ulica();
	//			final String miasto = "miasti";
	//			final String imie = "imie";
	//			final String nazwisko = "test";
	//			final String firma = "test";
	//			final String nip = "test";
	//			final String ulica = "test";
				
				Map<String,Object> searchValues = new HashMap<String, Object>();
				searchValues.put("imie", imie);
				searchValues.put("nazwisko", nazwisko);
				searchValues.put("miejscowosc", miasto);
				searchValues.put("name", firma);
				searchValues.put("nip", nip);
				searchValues.put("ulica", ulica);
				log.debug("SZUKA KANCELARII {}",searchValues);
				List<DfInst> kancelarie = DfInst.find(searchValues);
				if(kancelarie != null && !kancelarie.isEmpty())
				{
					values.put(AktNotarialnyLogic.NAZWA_KANCELARII_CN, kancelarie.get(0).getId());
				}
				else
				{
					DfInst kan = new DfInst();
					kan.setMiejscowosc(miasto);
					kan.setImie(imie);
					kan.setNazwisko(nazwisko);
					kan.setName(firma);
					kan.setNip(nip);
					kan.setUlica(ulica);				
					kan.create();
					values.put(AktNotarialnyLogic.NAZWA_KANCELARII_CN, kan.getId());
				}	
			}
			
			
		}
		catch (Exception e) 
		{
			LOG.error(e.getMessage(),e);
		}
	}
	
	public void actionAfterCreate(Long documentId, DSIBean dsiB)throws Exception
	{
		Document doc = Document.find(documentId);
		String name = (String)values.get("NAME");
		if(name == null)
		{
			name  = dsiB.getUserImportId();
			System.out.println("name 2 "+ name);
		}
		if(name != null)
		{
			String autor = name.split("_")[0];
			//Sprawdzenie czy istnieje w systemie
			DSUser.findByUsername(autor);
			
			doc.setAuthor(autor);
			if(doc.getAttachments().size() > 0)
			{
				doc.getAttachments().get(0).setAuthor(autor);
				doc.getAttachments().get(0).getMostRecentRevision().setAuthor(autor);
			}
		}
		if(values.get(AktNotarialnyLogic.PRZED_OCR_CN) != null && ((Boolean)values.get(AktNotarialnyLogic.PRZED_OCR_CN)))
		{
			LabelsManager.addLabel(documentId, 2L, Label.SYSTEM_LABEL_OWNER);
		}
		else if(values.get("BARCODE") == null)
		{
			LabelsManager.addLabel(documentId, 1L, Label.SYSTEM_LABEL_OWNER);
		}
		else
		{
			LabelsManager.removeLabel(documentId, 1L);
			LabelsManager.removeLabel(documentId, 2L);
			
		}
	}
	
	protected void setUpDocumentAfterCreate(Document document) throws Exception
	{
		
		dsiBean.setDocumentId(document.getId());
		
		if(this.attachmentsFile != null)
		{
			List<Attachment>  attas = document.getAttachments();
			
			for(FormFile attachmentFile : this.attachmentsFile)
			{
				Attachment attachment = null;
				if(attas != null && attas.size() > 0)
				{
					attachment = attas.get(0);
					attachment.createRevision(attachmentFile.getFile()).setOriginalFilename(attachmentFile.getName());
				}
				else
				{
					attachment = new Attachment(attachmentFile.getName());
					document.createAttachment(attachment);
					attachment.createRevision(attachmentFile.getFile()).setOriginalFilename(attachmentFile.getName());
				}
				
			}
		}
		
		if (archiveBox != null) 
		{
			document.setBox(archiveBox);
		}
		documentKind.setOnly(document.getId(), values);
        documentKind.logic().archiveActions(document,DocumentLogic.TYPE_IN_OFFICE);
        documentKind.logic().documentPermissions(document);
        
	}
}
