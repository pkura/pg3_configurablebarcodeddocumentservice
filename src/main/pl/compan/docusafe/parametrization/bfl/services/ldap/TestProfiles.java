package pl.compan.docusafe.parametrization.bfl.services.ldap;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.Profile;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.util.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
@SuppressWarnings("unused")
public class TestProfiles {

    private static final String PREFIX_DEBUG_MSG = "TEST-PROFILES - DEBUG: ";
    private final Logger log;
    private DSContext ctx;


    public TestProfiles(Logger log) {
        this.log = log;
    }

    public static boolean tryToRun(Logger log) {
        if (!"true".equals(Docusafe.getAdditionProperty("active.directory.service.__test__")))
            return false;

        debug(log,"TESTY - URUCHOMIONE");

        new TestProfiles(log).run();
        return true;
    }

    private static String getProfilename(String p) {
        return "test_profilename_" + p;
    }

    public static String getUsername(int num) {
        return "test_username_" + Integer.toString(num);
    }

    private void run() {
        try {
            ctx = DSApi.openAdmin();

            ctx.begin();

            clearProfiles();
            addUsers();
            addProfiles();
            removeProfiles();

            ctx.commit();
        } catch (Exception e) {
            debug(e);
        } finally {
            DSApi._close();
        }
    }

    private void addUsers() {
        if (!"true".equals(Docusafe.getAdditionProperty("active.directory.service.__test__.users.add")))
            return;


        String range = Docusafe.getAdditionProperty("active.directory.service.__test__.users.add.range");
        if (StringUtils.isEmpty(range))
            return;

        String[] us = StringUtils.split(range, '-');
        if (us.length == 2) {
            int from = Integer.parseInt(us[0]);
            int to = Integer.parseInt(us[1]);
            for (int num = from; num <= to; ++num) {
                String username = getUsername(num);
                try {
                    if (!DSUser.isUserExists(username)) {
                        DSUser user = UserFactory.getInstance().createUser(username, "firstname", "lastname");
                        ctx.session().save(user);
                        debug("Użytkownik dodany: " + username);
                    }
                } catch (Exception e) {
                    debug(e, "nieudane  utworzenie użytkownika: " + username);
                }
            }
        }
    }

    private void clearProfiles() {
        if (!"true".equals(Docusafe.getAdditionProperty("active.directory.service.__test__.profiles.clear")))
            return;

        try {
            Profile[] profiles = Profile.getProfiles();
            for (Profile profile : profiles) {
                if (profile != null)
                    try {
                        profile.remove();
                    } catch (Exception e) {
                        debug(e, "nieudane czyszczenie profilu: " + profile.getName());
                    }
            }
        } catch (Exception e) {
            debug(e, "nieudane czyszczenie profilów");
        }
    }

    private void addProfiles() {
        if (!"true".equals(Docusafe.getAdditionProperty("active.directory.service.__test__.profiles.add")))
            return;

        boolean addWithNull = "true".equals(Docusafe.getAdditionProperty("active.directory.service.__test__.profiles.add.user.withNull"));
        String range = Docusafe.getAdditionProperty("active.directory.service.__test__.profiles.add.user.range");
        handleProfiles(addWithNull ? ProfileHandle.ADD_USER_WITH_NULL : ProfileHandle.ADD_USER, range);
    }

    private void handleProfiles(ProfileHandle profileHandle, String range) {
        if (StringUtils.isEmpty(range))
            return;

        boolean createIfNotExist = "true".equals(Docusafe.getAdditionProperty("active.directory.service.__test__.profiles.addIfNotExist"));
        String[] uProfiles = StringUtils.split(range, ';');

        for (String uProfile : uProfiles) {
            String[] up = StringUtils.split(uProfile, ':');
            if (up.length == 2) {
                String[] us = StringUtils.split(up[0], '-');
                String p = up[1];
                String profilename = getProfilename(p);

                if (us.length == 2) {
                    Profile profile = getOrCreateProfile(profilename, createIfNotExist);
                    if (profile != null) {
                        int from = Integer.parseInt(us[0]);
                        int to = Integer.parseInt(us[1]);
                        for (int num = from; num <= to; ++num) {
                            String username = getUsername(num);
                            DSUser user = getOrCreateUser(username);
                            if (user != null) {
                                switch (profileHandle) {
                                    case ADD_USER:
                                        try {
                                            profile.addUser(user);
                                            debug("dodanie profilu: " + profilename + " dla: " + username);
                                        } catch (Exception e) {
                                            debug(e, "nieudane dodanie użytkownika: " + username + " dla: " + profilename);
                                        }
                                        break;
                                    case ADD_USER_WITH_NULL:
                                        try {
                                            profile.addUser(user);
                                            debug("dodanie profilu: " + profilename + " dla: " + username);
                                        } catch (Exception e) {
                                            debug(e, "nieudane dodanie użytkownika: " + username + " dla: " + profilename);
                                        }
                                        break;
                                    case REMOVE_USER:
                                        try {
                                            profile.removeUser(user);
                                            debug("usunięcie profilu: " + profilename + " dla: " + username);
                                        } catch (Exception e) {
                                            debug(e, "nieudane usunięcie użytkownika: " + username + " dla: " + profilename);
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private Profile getOrCreateProfile(String profilename, boolean createIfNotExist) {
        try {
            return Profile.findByProfilename(profilename);
        } catch (Exception e) {
            debug(e, "nieudane odnalezienie profilu: " + profilename);
        }

        if (createIfNotExist) {
            try {
                Profile profile = Profile.create(profilename);
                debug("utworzony profil: " + profilename);
                return profile;
            } catch (Exception e) {
                debug(e, "nieudane utworzenie profilu: " + profilename);
            }
        }

        return null;
    }

    private DSUser getOrCreateUser(String username) {
        try {
            if (DSUser.isUserExists(username)) {
                return DSUser.findByUsername(username);
            }
        } catch (Exception e) {
            debug(e, "nieudane  odnalezienie użytkownika: " + username);
        }

        try {
            DSUser user = UserFactory.getInstance().createUser(username, "firstname", "lastname");
            ctx.session().save(user);
            debug("użytkownik dodany: " + username);
            return user;
        } catch (Exception e) {
            debug(e, "nieudane  utworzenie użytkownika: " + username);
        }

        return null;
    }

    private void removeProfiles() {
        if (!"true".equals(Docusafe.getAdditionProperty("active.directory.service.__test__.profiles.remove")))
            return;

        String range = Docusafe.getAdditionProperty("active.directory.service.__test__.profiles.remove.range");
        handleProfiles(ProfileHandle.REMOVE_USER, range);
    }

    private void debug(Exception e) {
        debug(log,e);
    }

    private void debug(Exception e, String msg) {
        debug(log,e,msg);
    }

    private void debug(final String msg) {
        debug(log,msg);
    }

    private enum ProfileHandle {
        ADD_USER, ADD_USER_WITH_NULL, REMOVE_USER
    }

    private static void debug(Logger log, final String msg){
        log.error(PREFIX_DEBUG_MSG + msg);
    }
    private static void debug(Logger log, Exception e){
        log.error(PREFIX_DEBUG_MSG + e.getMessage(), e);
    }
    private void debug(Logger log, Exception e, String msg) {
        debug(log,msg);
        debug(log,e);
    }

    public static boolean tryAddAllProfilesToAllUsers(DSContext dsContext, ConsoleLogProvider log, UserProfilesStructure upStructure, ImportedUsersCollection iuCollection, ArrayList<AdProfilesDN> adProfilesDNs){

        if ( "true".equals(Docusafe.getAdditionProperty("active.directory.service.__test__.profiles.all")) &&
             "true".equals(Docusafe.getAdditionProperty("active.directory.service.__test__.profiles.addAllProfilesToAllUsers"))) {
            for (AdProfilesDN adProfilesDN : adProfilesDNs) {
                try {
                    Profile profile = Profile.findByProfilename(adProfilesDN.dbProfileName);
                    Collection<UserProfiles> ups = upStructure.getAllUserProfiles();
                    for (UserProfiles up : ups) {
                        DSUser user = null;
                        try {
                            try {
                                user = DSUser.findByUsername(up.getUsername());
                            } catch (Exception e) {
                                log.toDebug(e, "Nie odnaleziono użytkownika " + up.getUsername());
                            }
                            if (user != null) {
                                dsContext.begin();
                                profile.addUser(user);
                                dsContext.session().flush();
                                dsContext.commit();
                            }

                        } catch (Exception e) {
                            dsContext._rollback();
                            log.toDebug(e, "Nie dodano użytkownika " + up.getUsername());
                        }
                    }
                } catch (Exception e) {
                    log.toDebug(e, "Nie odnaleziono profilu " + adProfilesDN.dbProfileName);
                }
            }
            return true;
        }
        return false;
    }

    public static boolean tryRemoveAllProfilesFromAllUsers(DSContext dsContext, ConsoleLogProvider log, UserProfilesStructure upStructure, ImportedUsersCollection iuCollection, ArrayList<AdProfilesDN> adProfilesDNs) {

        boolean isInIUCollection;

        if ( "true".equals(Docusafe.getAdditionProperty("active.directory.service.__test__.profiles.all")) &&
             "true".equals(Docusafe.getAdditionProperty("active.directory.service.__test__.profiles.removeAllProfilesFromAllUsers"))) {
            for (AdProfilesDN adProfilesDN : adProfilesDNs) {
                try {
                    Profile profile = Profile.findByProfilename(adProfilesDN.dbProfileName);
                    Set<DSUser> users = profile.getUsers();

                    ArrayList<Long> usersToRemove = new ArrayList<Long>();
                    for(DSUser user : users){
                        isInIUCollection = iuCollection.contains(user);
                        if (isInIUCollection)
                            usersToRemove.add(user.getId());
                    }
                    if (!usersToRemove.isEmpty()) {
                        for (Long id : usersToRemove) {
                            try {
                                DSUser user = DSUser.findById(id);
                                final String msg = "Usuwanie użytkownikowi [" + user.getName() + "] profilu [" + profile.getName() + "]";
                                try {
                                    dsContext.begin();
                                    profile.removeUser(user);
                                    dsContext.session().flush();
                                    dsContext.commit();
                                } catch (EdmException e) {
                                    dsContext._rollback();
                                }
                            } catch (Exception e) {
                                log.toDebug(e, "Nie odnaleziono użytkownika o id=" + (id == null ? "NULL" : id));
                            }
                        }
                    }
                } catch (Exception e) {
                    log.toDebug(e, "Nie odnaleziono profilu " + adProfilesDN.dbProfileName);
                }
            }
            return true;
        }
        return false;
    }
}
