package pl.compan.docusafe.parametrization.bfl.services.ldap;

import pl.compan.docusafe.core.users.DSUser;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class SpecialistUserRole {

    private final SpecialistUserRoleEnum sur;

    public SpecialistUserRole(SpecialistUserRoleEnum sur) {
        this.sur = sur;
    }

    public static SpecialistUserRole create(String surName) {
        SpecialistUserRoleEnum sur = findSUR_orNull(surName);
        return sur == null ? null : new SpecialistUserRole(sur);
    }

    public static SpecialistUserRoleEnum findSUR_orNull(String surName) {
        SpecialistUserRoleEnum[] surs = SpecialistUserRoleEnum.values();
        for (SpecialistUserRoleEnum sur : surs)
            if (sur.name.equals(surName))
                return sur;
        return null;
    }

    @SuppressWarnings("unused")
    public void updateUserBySpecialistRole(DSUser user) {
        switch (sur) {
            case DSI:
                // no special function
                break;
            case KONTA_WYLACZONE:
                // no special function
                break;
            case KONTA_ZEWNETRZNE:
                // no special function
                break;
            case TEST:
                // no special function
                break;
            case ODDZIALY:
                // no special function
                break;
            case NIEKAKTYWNE:
                // no special function
                break;
        }
    }


    private enum SpecialistUserRoleEnum {
        DSI("DSI"),
        KONTA_WYLACZONE("Konta wylaczone"),
        KONTA_ZEWNETRZNE("Konta zewnetrzne"),
        TEST("Test"),
        ODDZIALY("Oddzialy"),
        NIEKAKTYWNE("nieaktywne");
        public final String name;

        SpecialistUserRoleEnum(String name) {
            this.name = name;
        }
    }
}
