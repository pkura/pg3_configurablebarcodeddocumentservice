package pl.compan.docusafe.parametrization.bfl.services.ldap;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class LdapImportServiceStats {


    public int uImported = 0;
    public int uInvalidExisted = 0;
    public int uInvalid = 0;
    //
    /**Liczba u�ytkownik�w, kt�rzy zostali utworzeni*/
    public int uCreated = 0;
    /**Liczba u�ytkownik�w, kt�rych nie udalo si� utworzy�*/
    public int uErrorCreated = 0;
    //
    /**Liczba u�ytkownik�w, kt�rych parametry zosta�y zaktualizowane*/
    public int uUpdated = 0;
    /**Liczba u�ytkownik�w, kt�rych parametry nie wymaga�y aktualizacji*/
    public int uNotUpdated = 0;
    //
    /**Liczba u�ytkownik�w, kt�rzy zostali usuni�cie*/
    public int uDeleted = 0;
    /**Liczba u�ytkownik�w, kt�rzy nie zostali usuni�ci - s� w bazie*/
    public int uNotDeleted = 0;
    /**Liczba u�ytkownik�w, kt�rych nie udalo si� usun��*/
    public int uErrorDeleted = 0;

    /**Liczba u�ytkownik�w, kt�rzy maj� co najmniej jeden profil*/
    public int upHaveProfiles = 0;
    /**Liczba u�ytkownik�w, kt�rych profile zosta�y niezmienione*/
    //public int upUnchanged = 0;
    /**Liczba u�ytkownik�w, kt�rym zosta�y dodane profile*/
    public int upAdd = 0;
    /**Liczba u�ytkownik�w, kt�rym zosta�y usuni�te profile*/
    public int upRemove = 0;

    /**Liczba profil�w, kt�re zosta�y dodane u�ytkownikom*/
    public int pAdd = 0;
    /**Liczba profil�w, kt�re zosta�y usuni�te u�ytkownikom*/
    public int pRemove = 0;
    /**Liczba profil�w, kt�rych nie uda�o si� doda� danemu u�ytkownikowi*/
    public int pErrorAdd = 0;
    /**Liczba profil�w, kt�rych nie uda�o si� usun�� danemu u�ytkownikowi*/
    public int pErrorRemove = 0;

    /**Liczba u�ytkownik�w, kt�rym nie zmieniono dzia��w*/
    public int udUnchanged = 0;
    /**Liczba dzia��w, kt�re zosta�y usuni�te*/
    public int dRemove = 0;
    /**Liczba dzia��w, kt�re zosta�y dodane*/
    public int dAdd = 0;
    /**Liczba nie udanych wyszuka� dzia��w*/
    public int dErrorNoDivision = 0;
    /**Liczba dzia��w, kt�rych nie uda�o sie doda�*/
    public int dErrorAdd = 0;
    /**Liczba dzia��w, kt�rych nie uda�o sie usun��*/
    public int dErrorRemove = 0;
    /**Liczba u�ytkownik�w, kt�rym nie zosta�y zaktualizowane dzia�y z powodu b��du*/
    public int udError = 0;
    /**Liczba u�ytkownik�w, kt�rzy nie posiadali �adnego dzia�u*/
    public int udNoDivisions = 0;

//    public void updateStats(LdapImportService.UserDatabaseWrittenResult result){
//        switch (result) {
//            case UPDATED:
//                ++uUpdated;
//                break;
//            case CREATED:
//                ++uCreated;
//                break;
//            case NO_CHANGED:
//                ++uNotUpdated;
//                break;
//        }
//    }
}
