package pl.compan.docusafe.parametrization.bfl.services.ldap;

import pl.compan.docusafe.core.users.DSUser;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Collection contains map of user distinguished name and object of UserProfiles class
 *
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class UserProfilesStructure {
    private Map<String, UserProfiles> collectionByMember = new HashMap<String, UserProfiles>();
    private Map<String, UserProfiles> collectionByUsername = new HashMap<String, UserProfiles>();

    public UserProfilesStructure() {
    }

    public UserProfiles findByMemberOrPut_orNull(String member, ImportedUsersCollection iuCollection) {
        UserProfiles up = findByMember_orNull(member);
        if (up == null) {
            ImportedUsersCollection.IUser iu = iuCollection.findByDistinguishedName_orNull(member);
            if (iu != null) {
                up = new UserProfiles(iu);
                putByMember(member, up);
            }
        }
        return up;
    }

    public UserProfiles findByMember_orNull(String member) {
        if (member == null) return null;
        return collectionByMember.get(member);
    }

    /**
     * @return true; W przypadku, gdy istnieje u�ytkownik o podanym member, a nazwy u�ytkownik�w s� r�ne, zostanie zwr�cone false
     */
    public boolean putByMember(String member, UserProfiles up) {
        UserProfiles userProfiles = collectionByMember.get(member);
        if (userProfiles != null) {
            if (!userProfiles.equalsUsername(up))
                return false;
            userProfiles.addProfile(up.getProfilesNames());
        } else {
            collectionByMember.put(member, up);
            collectionByUsername.put(up.getUsername(), up);
        }
        return true;
    }

    public Collection<UserProfiles> getAllUserProfiles() {
        return collectionByMember.values();
    }

    public int size() {
        return collectionByMember.size();
    }

    public boolean contains(DSUser user, String profileName) {
        return containsByUsername(user.getName(), profileName);
    }

    public boolean containsByUsername(String username, String profileName) {
        UserProfiles profile = collectionByUsername.get(username);
        return (profile != null && profile.contains(profileName));
//        Collection<UserProfiles> userProfiles = getAllUserProfiles();
//        for (UserProfiles up : userProfiles)
//            if (up.equals(user.getName()))
//                return true;
//        return false;
    }

    public UserProfiles findByUsername_orNull(String username) {
        if (username == null) return null;
        return collectionByUsername.get(username);
    }
}
