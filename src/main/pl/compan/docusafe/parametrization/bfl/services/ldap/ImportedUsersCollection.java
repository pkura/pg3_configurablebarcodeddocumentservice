package pl.compan.docusafe.parametrization.bfl.services.ldap;

import pl.compan.docusafe.core.users.DSUser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ImportedUsersCollection {
    private Map<String, IUser> users = new HashMap<String, IUser>();

    public ImportedUsersCollection() {
    }

    public void add(ImportedUser importedUser) {
        IUser user = users.get(importedUser.getUsername());
        if (user != null)
            user.addDivisionGUID(importedUser.divisionGUID);
        else
            users.put(importedUser.getUsername(), new IUser(importedUser.distinguishedName, importedUser.getUsername(), importedUser.divisionGUID));
    }

    public boolean contains(IUser user) {
        return containsByDistinguishedName(user.distinguishedName);
    }

    public boolean containsByDistinguishedName(String userDistinguishedName) {
        Collection<IUser> iUsers = users.values();
        for (IUser user : iUsers)
            if (user.distinguishedName.equals(userDistinguishedName))
                return true;
        return false;
    }

    @SuppressWarnings("unused")
    public IUser findByName_orNull(String name) {
        Collection<IUser> iUsers = users.values();
        for (IUser user : iUsers)
            if (user.name.equals(name))
                return user;
        return null;
    }

    @SuppressWarnings("unused")
    public IUser findByDistinguishedName_orNull(String distinguishedName) {
        Collection<IUser> iUsers = users.values();
        for (IUser user : iUsers)
            if (user.distinguishedName.equals(distinguishedName))
                return user;
        return null;
    }

    public boolean contains(DSUser dsUser) {
        return users.get(dsUser.getName()) != null;
//        Collection<IUser> iUsers = users.values();
//        for (IUser user : iUsers)
//            if (user.name.equals(dsUser.getName()))
//                return true;
//        return false;
    }

    @SuppressWarnings("unused")
    public String getAllUsersNamesAsString() {
        StringBuilder builder = new StringBuilder();
        Collection<IUser> iUsers = users.values();
        for (IUser user : iUsers)
            builder.append(user.name).append(',');
        int lastCharIdx = builder.length() - 1;
        if (lastCharIdx >= 0)
            builder.deleteCharAt(builder.length() - 1);
        return builder.toString();
    }

    public Collection<IUser> getUsers() {
        return users.values();
    }

//    @SuppressWarnings("unused")
//    public String[] getAllUsersNames() {
//        String[] usersNames = new String[users.size()];
//        Collection<IUser> iUsers = users.values();
//        for (int i = 0; i < iUsers.size(); ++i)
//            usersNames[i] = users.get(i).name;
//        return usersNames;
//    }

    public static class IUser {
        public final String distinguishedName;
        public final String name;
        public ArrayList<String> divisionGUIDs = new ArrayList<String>();

        public IUser(String distinguishedName, String name, String divisionGUID) {
            this.distinguishedName = distinguishedName;
            this.name = name;
            if (divisionGUID != null)
                divisionGUIDs.add(divisionGUID);
        }

        public void addDivisionGUID(String divisionGUID) {
            if (divisionGUID != null)
                divisionGUIDs.add(divisionGUID);
        }

        public boolean containsAnyGUIDs() {
            return divisionGUIDs.size() > 0;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null)
                return false;
            if (getClass() != o.getClass()) {
                if (o instanceof String) {
                    String username = (String) o;
                    return username.equals(name);
                }
                return false;
            }
            IUser iUser = (IUser) o;
            if (distinguishedName != null ? !distinguishedName.equals(iUser.distinguishedName) : iUser.distinguishedName != null)
                return false;
            if (divisionGUIDs != null ? !divisionGUIDs.equals(iUser.divisionGUIDs) : iUser.divisionGUIDs != null)
                return false;
            if (name != null ? !name.equals(iUser.name) : iUser.name != null) return false;

            return true;
        }
    }
}
