package pl.compan.docusafe.parametrization.bfl.services.ldap;

 /**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public interface ConsoleLogProvider {
    public void toPrint(int logLevel, String message);

     public void toPrint(Exception e);

     public void toPrint(Exception e, String message);

    @SuppressWarnings("unused")
    public void toLog(int logLevel, String message);

     void toDebug(Exception e, String s);
 }
