package pl.compan.docusafe.parametrization.bfl.services.ldap;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;

import javax.naming.directory.Attribute;
import javax.naming.directory.SearchResult;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

// Komentarze zaczynaj�ce sie od "UWAGA!" zaznaczaj� regu�y biznesowe

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ImportedUser {
    public final String firstname;
    public final String lastname;
    public final boolean isDisabledAccount;
    public final String externalName;
    public final String email;// optional
    public final String phone;// optional telephoneNumber: +48 11 111 1111
    public final String mobilePhone;// optional mobile: +48 111 111 111
    //
    public final String distinguishedName;
    public final String divisionGUID;
    //private final ArrayList<SpecialistUserRole> specialistGroups;
    private final String name;//ds identifying param


    public ImportedUser(SearchResult userSR, String userSourceDirectory, ConsoleLogProvider clp) {

        this.distinguishedName = LdapImportService.getStringAttr_orNull(userSR, "distinguishedName", clp);

        this.name = LdapImportService.getStringAttr_orNull(userSR, "sAMAccountName", clp);
        this.firstname = LdapImportService.getStringAttr_orNull(userSR, "givenName", clp);
        this.lastname = LdapImportService.getStringAttr_orNull(userSR, "sn", clp);
        this.email = LdapImportService.getStringAttr_orNull(userSR, "mail", clp);
        this.phone = LdapImportService.getStringAttr_orNull(userSR, "telephoneNumber", clp);
        this.mobilePhone = LdapImportService.getStringAttr_orNull(userSR, "mobile", clp);
        this.divisionGUID = getDivisionGUID(userSourceDirectory, distinguishedName, clp);
        this.externalName = LdapImportService.getStringAttr_orNull(userSR, "userPrincipalName", clp);
        this.isDisabledAccount = isDisabledAccount(userSR, clp);
//        this.specialistGroups = findSpecialistGroups(userSourceDirectory, distinguishedName, clp);
    }

    private boolean isDisabledAccount(SearchResult element, ConsoleLogProvider clp) {
        try {
            Attribute userAccountControl = element.getAttributes().get("userAccountControl");
            if (userAccountControl != null && userAccountControl.get() != null) {
                int mixedValue = Integer.valueOf(userAccountControl.get().toString());

                return ((mixedValue >> 1) & 1) == 1;
            }
        } catch (Exception e) {
            clp.toPrint(e);
        }
        return false;
    }

    private String getDivisionGUID(String userSourceDirectory, String distinguishedName, ConsoleLogProvider clp) {
        ImportedDivisionPath id = ImportedDivisionPath.createDivisionPath_orNull(userSourceDirectory, distinguishedName);
        if (id == null)
            return null;
        DSDivision d = ImportedDivisionPath.getOrCreateDivision_orNull_safe(id, clp);
        if (d == null)
            return null;
        return d.getGuid();
    }

    @SuppressWarnings("unused")
    public static boolean isListContains(List<ImportedUser> importedUsers, String dsIdentifyingUsername) {
        for (ImportedUser iUser : importedUsers)
            if (iUser.getUsername().equals(dsIdentifyingUsername))
                return true;
        return false;
    }

    public String getUsername() {
        return name;
    }

    public String getReasonOfInvalid() {
        String[] invalids = getInvalidParamsAsArray();
        return "Brak parametr�w dla importowanego u�ytkownika: [" + StringUtils.join(invalids, "][") + "]";
    }

    public String[] getInvalidParamsAsArray() {
        ArrayList<String> invalids = getInvalidParams();
        return invalids.toArray(new String[invalids.size()]);
    }

    public ArrayList<String> getInvalidParams() {
        ArrayList<String> invalids = new ArrayList<String>();

//        if (StringUtils.isEmpty(divisionName))
//            invalids.add("divisionName");
        if (StringUtils.isEmpty(name))
            invalids.add("name");
        if (StringUtils.isEmpty(firstname))
            invalids.add("firstname");
        if (StringUtils.isEmpty(lastname))
            invalids.add("lastname");
        if (StringUtils.isEmpty(externalName))
            invalids.add("externalName");

        return invalids;
    }

    public boolean isRemovableFromDS() {
        return StringUtils.isEmpty(name);
    }

    public boolean isDifferent(DSUser user) {
        return user == null ||
                (isDisabledAccount != user.isLoginDisabled()) ||
                !user.isAdUser() ||
                !StringUtils.equals(firstname, user.getFirstname()) ||
                !StringUtils.equals(lastname, user.getLastname()) ||
                !StringUtils.equals(externalName, user.getExternalName()) ||
                !StringUtils.equals(email, user.getEmail()) ||
                !StringUtils.equals(phone, user.getPhoneNum()) ||
                !StringUtils.equals(mobilePhone, user.getMobileNum());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImportedUser that = (ImportedUser) o;
        return name.equals(that.name);
    }

    public String getDifference(DSUser user) {
        if (user == null)
            return "null->" + firstname + " null->" + lastname + " null->" + email;

        StringBuilder builder = new StringBuilder();
        if (!StringUtils.equals(firstname, user.getFirstname()))
            builder.append(user.getFirstname()).append("->").append(firstname).append("||");
        if (!StringUtils.equals(lastname, user.getLastname()))
            builder.append(user.getLastname()).append("->").append(lastname).append("||");
        if (!user.isAdUser())
            builder.append("notAD->AD").append("||");
        if (isDisabledAccount != user.isLoginDisabled())
            builder.append("LoginDisabled:").append(user.isLoginDisabled()).append("->").append(isDisabledAccount).append("||");
        if (!StringUtils.equals(externalName, user.getExternalName()))
            builder.append(user.getExternalName()).append("->").append(externalName).append("||");
        if (!StringUtils.equals(email, user.getEmail()))
            builder.append(user.getEmail()).append("->").append(email).append("||");
        return builder.toString();
    }

    /**
     * user.update() is invoked
     *
     * @return informacja czy zaktualizowano u�ytkownika
     */
    public boolean update(DSUser user) throws EdmException {
        if (!isValid())
            return false; //UWAGA! Je�eli niepoprawne parametry -> nie aktualizuj� u�ytkownika

        user.setFirstname(firstname);
        user.setLastname(lastname);

        user.setLoginDisabled(isDisabledAccount);
        user.setExternalName(externalName);
        user.setAdUser(true);

        user.setEmail(email);
        user.setPhoneNum(phone);
        user.setMobileNum(mobilePhone);

//        for (SpecialistUserRole specialistGroup : specialistGroups)
//            specialistGroup.updateUserBySpecialistRole(user);

        user.update();

        return true;
    }

    public boolean isValid() {
        return getInvalidParams().size() == 0;
    }

    /**
     * Method requires DSApi.openAdmin() and DSApi.context().begin()
     */
    public DSUser createUser() throws EdmException {
        DSUser user = UserFactory.getInstance().createUser(getUsername(), firstname, lastname);

        user.setExternalName(externalName);
        user.setAdUser(true);

        user.setEmail(email);
        user.setPhoneNum(phone);
        user.setMobileNum(mobilePhone);

        user.setCtime(new Timestamp(new Date().getTime()));
        user.setAdUser(true);
        user.setLoginDisabled(true);
        return user;
    }

//    public static ArrayList<SpecialistUserRole> findSpecialistGroups(String userSourceDirectory, String distinguishedName, ConsoleLogProvider clp) {
//        ArrayList<SpecialistUserRole> groups;
//        if (distinguishedName.endsWith(userSourceDirectory)) {
//            String groupsString = distinguishedName.substring(0, distinguishedName.length() - userSourceDirectory.length());
//            ArrayList<String> groupsS = AdUtitls.getValuesFromMultiAttrs("OU", groupsString);
//
//            groups = new ArrayList<SpecialistUserRole>();
//            for (String gs : groupsS) {
//                SpecialistUserRole sur = SpecialistUserRole.create(gs);
//                if (sur != null)
//                    groups.add(sur);
//            }
//        } else {
//            groups = new ArrayList<SpecialistUserRole>();
//            clp.toPrint(Console.ERROR, "U�ytkownik nie pochodzi ze zdefiniowanej �cie�ki : [" + userSourceDirectory + "] nie zawiera si� w [" + distinguishedName + "]");
//        }
//        return groups;
//    }
}