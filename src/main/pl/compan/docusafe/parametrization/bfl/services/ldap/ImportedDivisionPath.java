package pl.compan.docusafe.parametrization.bfl.services.ldap;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.service.Console;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ImportedDivisionPath {
    private final ArrayList<String> divisionDir;

    private ImportedDivisionPath(ArrayList<String> dir) {
        this.divisionDir = dir;
    }

    public static ImportedDivisionPath createDivisionPath_orNull(String userSourceDirectory, String distinguishedName) {
        if (distinguishedName.endsWith(userSourceDirectory)) {
            String multiDivisions = distinguishedName.substring(0, distinguishedName.length() - userSourceDirectory.length());
            return ImportedDivisionPath.createDivision_orNull(multiDivisions);
        }
        return null;
    }

    /**
     * @param multiDivisions like "CN=*,OU=Child,OU=Parent,OU=Root"
     */
    public static ImportedDivisionPath createDivision_orNull(String multiDivisions) {
        //dir like Child,Parent,Root
        ArrayList<String> dirReverse = AdUtitls.getValuesFromMultiAttrs("OU", multiDivisions);
        //dir like Root,Parent,Child
        ArrayList<String> dir = new ArrayList<String>();
        for (String d : dirReverse)
            dir.add(0, d);
        if (dirReverse.size() > 0)
            return new ImportedDivisionPath(dir);
        return null;
    }

    public static DSDivision getOrCreateDivision_orNull_safe(ImportedDivisionPath id, ConsoleLogProvider clp) {
        try {
            return findDivision_orNull(id, clp);
        } catch (Exception e) {
            clp.toPrint(e);
            return null;
        }
    }

    /**
     * Method requires DSApi.openAdmin() and DSApi.context().begin()
     */
    public static DSDivision findDivision_orNull(ImportedDivisionPath iDivision, ConsoleLogProvider clp) throws EdmException {

        if (iDivision == null)
            return null;

        DSDivision dsDivision = null;
        String iDivisionName = iDivision.getDivisionName();

        try {
            List<DSDivision> dList = DSDivision.findByLikeName(iDivisionName);
            for (DSDivision d : dList)
                if (iDivision.isThisDivision(d))
                    return d;
        } catch (Exception e) {
            clp.toPrint(e, "Wyszukiwanie dzia�u : " + iDivision.getNamesDir());
        }

        return dsDivision;
    }

    public String getNamesDir() {
        StringBuilder builder = new StringBuilder();
        for (String d : divisionDir)
            builder.append(d).append('/');
        return builder.toString();
    }

    /**
     * Method requires DSApi.openAdmin() and DSApi.context().begin()
     */
    public boolean isThisDivision(DSDivision dsDivision) {
        String iDivisionName;
        DSDivision compDivision = dsDivision;

        for (int iDivisionIdx = divisionDir.size() - 1; iDivisionIdx >= 0; --iDivisionIdx) {
            iDivisionName = divisionDir.get(iDivisionIdx);

            if (compDivision.getName().equalsIgnoreCase(iDivisionName)) {
                iDivisionName = getDivisionName_orNull(iDivisionIdx);
                compDivision = getParentDivision_orNull(compDivision);

                if (iDivisionName != null && compDivision == null)
                    return false;
            } else
                return false;
        }
        return true;
    }

    public static DSDivision getParentDivision_orNull(DSDivision dsDivision) {
        try {
            return dsDivision.getParent();
        } catch (EdmException e) {
            return null;
        }
    }

//    @SuppressWarnings("unused")
//    public final String getParentDivisionName_orNull(String divisionName) {
//        String parent = null;
//        for (String d : divisionDir) {
//            if (d == divisionName)//compare references
//                return parent;
//            parent = d;
//        }
//        return parent;
//    }

    public final String getDivisionName_orNull(int idx) {
        if (idx < 0 || idx >= divisionDir.size())
            return null;
        return divisionDir.get(idx);
    }

    public final String getDivisionName() {
        return divisionDir.get(divisionDir.size() - 1);
    }

    /**
     * Method requires DSApi.openAdmin() and DSApi.context().begin()
     */
    @SuppressWarnings("unused")
    public static DSDivision getOrCreateDivision_orNull(ImportedDivisionPath iDivision, ConsoleLogProvider clp) throws EdmException {

        if (iDivision == null)
            return null;

        DSDivision dsDivision = null;
        String iDivisionName = iDivision.getDivisionName();

        try {
            dsDivision = findDivision_orNull(iDivision, clp);
            if (dsDivision == null) {
                //not found - create
                dsDivision = iDivision.getOrCreateDivisions();
                clp.toPrint(Console.WARN, "Nie odnaleziono dzia�u [" + iDivision.getNamesDir() + "] - dzia� utworzony");
            }
        } catch (Exception e) {
            clp.toPrint(e, "Wyszukiwanie dzia�u : " + iDivision.getNamesDir());
        }

        return dsDivision;
    }

    /**
     * Method requires DSApi.openAdmin() and DSApi.context().begin()
     */
    @SuppressWarnings("unused")
    public DSDivision getOrCreateDivisions() throws EdmException {
        DSDivision dsDivisionRoot = getRootOfImportedDivision();
        DSDivision dsDivisionChild;
        for (int i = 1; i < divisionDir.size(); ++i) {
            DSDivision[] children;
            try {
                children = dsDivisionRoot.getChildren();
            } catch (Exception e) {
                return null;
            }
            dsDivisionChild = findChildDivision_orNull(children, divisionDir.get(i));
            if (dsDivisionChild == null)
                return createDivisionsDir(i, dsDivisionRoot);
            dsDivisionRoot = dsDivisionChild;
        }
        return dsDivisionRoot;
    }

    /**
     * Method requires DSApi.openAdmin() and DSApi.context().begin()
     */
    private DSDivision createDivisionsDir(int dirFrom_include, DSDivision dsDivisionRoot) throws EdmException {
        DSDivision divisionChild = null;
        DSDivision divisionRoot = dsDivisionRoot;
        for (int i = dirFrom_include; i < divisionDir.size(); ++i) {
            String dName = divisionDir.get(i);
            divisionChild = divisionRoot.createDivision(dName, getNewDivisionCode(dName));
            //divisionChild.setParent(divisionRoot);
            DSApi.context().session().refresh(divisionChild);

            divisionRoot = divisionChild;
        }

        return divisionChild;
    }

    /**
     * Method requires DSApi.openAdmin() and DSApi.context().begin()
     */
    private DSDivision getRootOfImportedDivision() throws EdmException {
        String iRootDName = divisionDir.get(0);
        List<DSDivision> divisions = null;
        try {
            divisions = DSDivision.findByLikeName(iRootDName);
        } catch (Exception e) {
            //do nothing
        }
        if (divisions != null) {
            if (divisions.size() == 1)
                return divisions.get(0);
            if (divisions.size() == 0) {
                DSDivision root = DSDivision.find(DSDivision.ROOT_GUID);
                DSDivision division = root.createDivision(iRootDName, getNewDivisionCode(iRootDName));
                DSApi.context().session().refresh(division);
            }
        }
        throw new UnequivocalDivision(iRootDName);
    }

    @SuppressWarnings("unused")
    private String getNewDivisionCode(String divisionName) {
        return "NEW";
        //return divisionName + "_code_" + Integer.toString(divisionName.hashCode());
    }

    private DSDivision findChildDivision_orNull(DSDivision[] children, String divisionName) throws UnequivocalDivision {
        ArrayList<DSDivision> divisions = new ArrayList<DSDivision>();
        for (DSDivision child : children)
            if (child.getName().equalsIgnoreCase(divisionName))
                divisions.add(child);
        if (divisions.size() > 1)
            throw new UnequivocalDivision(divisionName);
        if (divisions.size() == 0)
            return divisions.get(0);
        return null;  //To change body of created methods use File | Settings | File Templates.
    }

    /**
     * Method requires DSApi.openAdmin() and DSApi.context().begin()
     * PRZE�ADOWUJE SESJ� DSApi!!
     */
    @SuppressWarnings("unused")
    private DSDivision GetOrCreateDivision_simple(String divisionName) throws EdmException {
        if (StringUtils.isEmpty(divisionName))
            return null;

        DSDivision division;
        try {
//                if (Docusafe.getAdditionProperty("active.directory.root.division") != null)
//                    division = DSDivision.findByName(Docusafe.getAdditionProperty("active.directory.root.division"));
//                else
            division = DSDivision.findByName(divisionName);
        } catch (DivisionNotFoundException e) {
            DSDivision parent = DSDivision.find(DSDivision.ROOT_GUID);
            division = parent.createDivision(divisionName, "NEW");
            DSApi.context().commit();
            DSApi.close();
            DSApi.openAdmin();
            DSApi.context().begin();
            DSApi.context().session().refresh(division);
        }
        return division;
    }
}
