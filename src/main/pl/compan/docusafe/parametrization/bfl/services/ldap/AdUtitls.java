package pl.compan.docusafe.parametrization.bfl.services.ldap;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class AdUtitls {

    @SuppressWarnings("unused")
    public static String getValueFromAttr_orNull(String attr) {
        int idx = attr.indexOf('=');
        if (idx < 0)
            return null;
        return attr.substring(idx + 1);
    }

    @SuppressWarnings("unused")
    public static String getFirstFromAttrValue_orNull(String attrHead, String value) {
        String val = null;
        if (StringUtils.isNotEmpty(value) && StringUtils.isNotEmpty(attrHead)) {
            String[] tab = value.split(",");
            for (String el : tab) {
                if (el.toUpperCase().startsWith(attrHead)) {
                    val = getValueFromAttr_orNull(attrHead, el);
                    break;
                }
            }
        }
        return val;
    }

    public static String getValueFromAttr_orNull(String head, String attr) {
        if (attr.length() < head.length() + 1)//head=
            return null;
        return attr.substring(head.length() + 1);
    }

    public static ArrayList<String> getValuesFromMultiAttrs(String valuesHead, String multiattrs) {
        ArrayList<String> values = new ArrayList<String>();
        if (StringUtils.isNotEmpty(multiattrs) && StringUtils.isNotEmpty(valuesHead)) {
            String[] tab = multiattrs.split(",");
            String v;
            for (String el : tab) {
                if (el.toUpperCase().startsWith(valuesHead)) {
                    v = getValueFromAttr_orNull(valuesHead, el);
                    if (v != null) {
                        values.add(v);
                    }
                    //else impossible
                }
            }
        }
        return values;
    }

//        public DSUser findUser_orNull(String uCN, List<ImportedUser> importedUsersNames) {
//            DSUser user = null;
//            for (ImportedUser iUser : importedUsersNames) {
//                if (iUser.distinguishedName != null && iUser.distinguishedName.equals(uCN)) {
//                    try {
//                        DSApi.context().begin();
//                        user = DSUser.findByUsername(iUser.getUsername());
//                        DSApi.context().commit();
//                    } catch (/*UserNotFound, Edm*/Exception e) {
//                        DSApi.context()._rollback();
//                        print(e);
//                    }
//                    break;
//                }
//            }
//
//            return user;
//        }
//
//    public interface AdAttributes{
//        public String getAttrValue(String attrName);
//    }
}
