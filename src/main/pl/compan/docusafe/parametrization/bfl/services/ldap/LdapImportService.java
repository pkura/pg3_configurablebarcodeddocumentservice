package pl.compan.docusafe.parametrization.bfl.services.ldap;

import org.apache.commons.lang.StringUtils;
import org.jfree.util.Log;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.Profile;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.ad.ActiveDirectoryManager;
import pl.compan.docusafe.service.*;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.*;
import java.util.*;

import static pl.compan.docusafe.parametrization.bfl.services.ldap.LdapImportTaskParams.*;

// Komentarze zaczynaj�ce sie od "UWAGA!" zaznaczaj� regu�y biznesowe

/**
 * Serwis wykonuj�cy:
 * - import u�ytkownik�w (wraz z modyfikacj�, blokowaniem itp.)
 * - import struktury organizacyjnej - ostatecznie nie
 * - import uprawnie�
 *
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class LdapImportService extends ServiceDriver implements ConsoleLogProvider, Service {

    public static final Logger log = LoggerFactory.getLogger(LdapImportService.class);
    private static final String TAG = " service LdapImportService by W.O.";
    private static final int SEARCH_PAGE_SIZE = 850;
    private static final int MIN_PERIOD_SYNC_IN_MILLIS = 1;
    private static final long DELAY_IN_MILLIS = 2 * DateUtils.SECOND;
    private static final long DEFAULT_PERIOD_IN_MILLIS;

    static {
        long defaultValue = 12 * DateUtils.HOUR;
        Long value = null;
        String valueTxt = Docusafe.getAdditionProperty("active.directory.service.defaultPeriodSyncInMinutes");
        if (valueTxt != null) {
            try {
                value = Long.parseLong(valueTxt) * DateUtils.MINUTE;
            } catch (Exception e) {
                Log.error(e.getMessage(), e);
            }
        }
        DEFAULT_PERIOD_IN_MILLIS = value != null ? value : defaultValue;
    }

    private static long lastSyncTimeInMillis = 0;
    private static long periodSyncInMillis = DEFAULT_PERIOD_IN_MILLIS;
    //private static final StringManager stringManager = GlobalPreferences.loadPropertiesFile(MailerDriver.class.getPackage().getName(), null);
    private Property[] properties;
    private Timer timer;


    public LdapImportService() {
        properties = new Property[]{new PeriodSyncProperty()};
    }

    public static <T> boolean isArrayContains(final T[] array, final T value) {
        if (array != null && array.length > 0)
            for (T v : array)
                if (v != null && v.equals(value))
                    return true;
        return false;

    }

    @SuppressWarnings("unused")
    public String getStringAttr_orNull(SearchResult element, String name) {
        return getStringAttr_orNull(element, name, this);
    }

    public static String getStringAttr_orNull(SearchResult element, String name, ConsoleLogProvider clp) {
        try {
            return element.getAttributes().get(name) != null ? (String) element.getAttributes().get(name).get() : null;
        } catch (NamingException e) {
            clp.toPrint(e);
            return null;
        }
    }

    public ArrayList<String> getAllStringAttrs(SearchResult element, String name) {
        ArrayList<String> attrsList = new ArrayList<String>();
        try {
            Attribute attr = element.getAttributes().get(name);
            NamingEnumeration myAttrs = attr.getAll();
            while (myAttrs.hasMore()) {
                String myAttrE = (String) myAttrs.next();
                attrsList.add(myAttrE);
            }
        } catch (NamingException e) {
            print(e);
        }
        return attrsList;
    }

    private void print(Exception e) {
        //LdapImportService.log.error(e.getMessage(), e);
        console(Console.ERROR, "B��d : [ " + e + " ]");
    }

    @Override
    protected void start() throws ServiceException {
        log.info("start " + TAG);
        console(Console.INFO, "start " + TAG);

        timer = new Timer(true);
        timer.schedule(new ImportTask(), DELAY_IN_MILLIS, DEFAULT_PERIOD_IN_MILLIS);
    }

    @Override
    protected void stop() throws ServiceException {
        log.info("stop " + TAG);
        console(Console.INFO, "stop " + TAG);

        if (timer != null)
            timer.cancel();
    }

    @Override
    protected boolean canStop() {
        return true;
    }

    /**
     * Zwraca parametry serwisu dla widoku
     */
    @Override
    public Property[] getProperties() {
        return properties;
    }

    @Override
    public void toPrint(int logLevel, String message) {
        print(logLevel, message);
    }

    private void print(int logLevel, String message) {
        console(logLevel, message);
    }

    @Override
    public void toPrint(Exception e) {
        print(e);
    }

    @Override
    public void toPrint(Exception e, String message) {
        print(e, message);
    }

    private void print(Exception e, String message) {
        console(Console.ERROR, "B��d :" + message + " : [ " + e + " ]");
    }

    @Override
    public void toLog(int logLevel, String message) {
        log(logLevel, message);
    }

    @Override
    public void toDebug(Exception e, String s) {
        debug(e, s);
    }

    @SuppressWarnings("unused")
    private void debug(Exception e, String msg) {
        log(Console.ERROR, "DEBUD ERROR :: " + msg);
    }

    public static void log(int logLevel, String message) {
        switch (logLevel) {
            case Console.INFO:
                LdapImportService.log.info(message);
                break;
            case Console.DEBUG:
                LdapImportService.log.debug(message);
                break;
            case Console.ERROR:
                LdapImportService.log.error(message);
                break;
            case Console.WARN:
                LdapImportService.log.warn(message);
                break;
        }
    }

    @SuppressWarnings("unused")
    private void debug(String msg) {
        log(Console.ERROR, "DEBUD :: " + msg);
    }

    class PeriodSyncProperty extends Property {
        public PeriodSyncProperty() {
            super(SIMPLE, PERSISTENT, LdapImportService.this, "periodSyncInMillis", "Interwa� uruchamiania serwisu (w minutach)", String.class);
        }

        @Override
        protected Object getValueSpi() {
            return periodSyncInMillis / DateUtils.MINUTE;
        }

        @Override
        protected void setValueSpi(Object object) throws ServiceException {
            synchronized (LdapImportService.this) {
                try {
                    Long periodSyncNewInMinutes = Long.parseLong((String) object);
                    if (periodSyncNewInMinutes >= MIN_PERIOD_SYNC_IN_MILLIS) {
                        periodSyncInMillis = periodSyncNewInMinutes * DateUtils.MINUTE;

                        long currentTimeInMillis = System.currentTimeMillis();
                        long delayInMillis = periodSyncInMillis - (currentTimeInMillis - lastSyncTimeInMillis);

                        if (timer != null)
                            timer.cancel();
                        timer = new Timer(true);
                        timer.schedule(new ImportTask(), delayInMillis < 0 ? 0 : delayInMillis, periodSyncInMillis);
                    }
                } catch (Exception e) {
                    print(e);
                }
            }
        }
    }

    class ImportTask extends TimerTask {

        private final boolean LOG_IMPORTED_PROFILES_DIRS = "true".equals(Docusafe.getAdditionProperty("active.directory.service.logImportedProfilesDirs"));
        private final boolean LOG_IMPORTED_USERS_DIRS = "true".equals(Docusafe.getAdditionProperty("active.directory.service.logImportedUsersDirs"));
        private final boolean LOG_USERS_STRUCTURE = "true".equals(Docusafe.getAdditionProperty("active.directory.service.logUsersStructure"));
        private final boolean LOG_USER_PROFILES_STRUCTURE = "true".equals(Docusafe.getAdditionProperty("active.directory.service.logUserProfilesStructure"));
        private final boolean LOG_USER_PROFILES_IN_DS = "true".equals(Docusafe.getAdditionProperty("active.directory.service.logUserProfilesInDS"));
        private final boolean LOG_JUST_IMPORTED_USER_PROFILES = "true".equals(Docusafe.getAdditionProperty("active.directory.service.logJustImportedUserProfiles"));
        private final boolean LOG_USER_TO_REMOVE_FROM_PROFILE = "true".equals(Docusafe.getAdditionProperty("active.directory.service.logUserToRemoveFromProfile"));
        private final boolean LOG_USER_TO_LEAVE_IN_PROFILE = "true".equals(Docusafe.getAdditionProperty("active.directory.service.logUserToLeaveInProfile"));
        private DSContext dsContext = null;

        public void run() {

            lastSyncTimeInMillis = System.currentTimeMillis();
            LdapContext context = null;

            if (TestProfiles.tryToRun(log))
                return;

            try {
                print(Console.INFO, "Otwieranie kontekstu bazy danych");
                dsContext = DSApi.openAdmin();

                if (dsContext == null) {
                    print(Console.INFO, "Nieudane otwarcie kontekstu bazy danych");
                    return;//goto finally
                }

                try {
                    context = getLdapContext();
                    if (context == null)
                        return;//goto finally

                    ArrayList<AdUsersDN> adUsersDNs = getAdUsersDNs();
                    ArrayList<AdProfilesDN> adProfilesDNs = getAdProfilesDNs_safe(LdapImportService.this);
                    LdapImportServiceStats stats = new LdapImportServiceStats();

                    ImportedUsersCollection iuCollection = importUsers_andAddToDatabase(context, adUsersDNs, stats);
                    UserProfilesStructure upStructure = importProfiles(context, iuCollection, adProfilesDNs, stats);

                    //disableActiveDirectoryUsersNotOnList(iuCollection, stats);
                    //updateUsersDivisions(iuCollection, stats);
                    updateUsersProfilesStructure(upStructure, iuCollection, adProfilesDNs, stats);

                    logImportedUsersDirs(adUsersDNs);
                    logImportedProfiles(adProfilesDNs);

                    print(Console.INFO, "STATYSTYKI - IMPORT U�YTKOWNIK�W");
                    print(Console.INFO, "Liczba zaimportowanych u�ytkownik�w = " + stats.uImported);
                    print(Console.INFO, "Liczba u�ytkownik�w zaimportowanych o niepoprawnych parametrach zawieraj�cych nazw� u�ytkownika = " + stats.uInvalidExisted);
                    print(Console.INFO, "Liczba u�ytkownik�w zaimportowanych o niepoprawnych parametrach = " + stats.uInvalid);
                    print(Console.INFO, "DODAWANIE");
                    print(Console.INFO, "Liczba u�ytkownik�w, kt�rzy zostali utworzeni = " + stats.uCreated);
                    print(Console.INFO, "Liczba u�ytkownik�w, kt�rych nie udalo si� utworzy� = " + stats.uErrorCreated);
                    print(Console.INFO, "AKTUALIZACJA");
                    print(Console.INFO, "Liczba u�ytkownik�w, kt�rych parametry zosta�y zaktualizowane = " + stats.uUpdated);
                    print(Console.INFO, "Liczba u�ytkownik�w, kt�rych parametry nie wymaga�y aktualizacji = " + stats.uNotUpdated);
                    print(Console.INFO, "UNIEWA�NIANIE DOST�PU DO AD");
                    print(Console.INFO, "Liczba u�ytkownik�w, kt�rym odebrany zosta� dost�p do AD = " + stats.uDeleted);
                    print(Console.INFO, "Liczba u�ytkownik�w, kt�rzy maja dost�p do AD = " + stats.uNotDeleted);
                    print(Console.INFO, "Liczba u�ytkownik�w, kt�rym nie uda�o si� odebran� dost�pu do AD = " + stats.uErrorDeleted);
//                print(Console.INFO, "DODAWANIE, USUWANIE DO DZIA��W");
//                print(Console.INFO, "Liczba u�ytkownik�w, kt�rym nie zmieniono dzia��w = " + stats.udUnchanged);
//                print(Console.INFO, "Liczba dzia��w, kt�re zosta�y usuni�te = " + stats.dRemove);
//                print(Console.INFO, "Liczba dzia��w, kt�re zosta�y dodane = " + stats.dAdd);
//                print(Console.INFO, "Liczba nie udanych wyszuka� dzia��w = " + stats.dErrorNoDivision);
//                print(Console.INFO, "Liczba dzia��w, kt�rych nie uda�o sie doda� = " + stats.dErrorAdd);
//                print(Console.INFO, "Liczba dzia��w, kt�rych nie uda�o sie usun�� = " + stats.dErrorRemove);
//                print(Console.INFO, "Liczba u�ytkownik�w, kt�rym nie zosta�y zaktualizowane dzia�y z powodu b��du = " + stats.udError);
//                print(Console.INFO, "Liczba u�ytkownik�w, kt�rzy nie posiadali �adnego dzia�u = " + stats.udNoDivisions);
                    print(Console.INFO, "STATYSTYKI - IMPORT STRUKTURY");
                    print(Console.INFO, "Liczba u�ytkownik�w, kt�rzy maj� co najmniej jeden profil = " + stats.upHaveProfiles);
//                    print(Console.INFO, "Liczba u�ytkownik�w, kt�rych profile zosta�y niezmienione = " + stats.upUnchanged);
                    print(Console.INFO, "Liczba u�ytkownik�w, kt�rym zosta�y dodane profile = " + stats.upAdd);
                    print(Console.INFO, "Liczba u�ytkownik�w, kt�rym zosta�y usuni�te profile = " + stats.upRemove);
                    print(Console.INFO, "Liczba profil�w, kt�re zosta�y dodane u�ytkownikom = " + stats.pAdd);
                    print(Console.INFO, "Liczba profil�w, kt�re zosta�y usuni�te u�ytkownikom = " + stats.pRemove);
                    print(Console.INFO, "Liczba profil�w, kt�rych nie uda�o si� usun�� u�ytkownikom = " + stats.pErrorRemove);
                    print(Console.INFO, "Liczba profil�w, kt�rych nie uda�o si� doda� u�ytkownikom = " + stats.pErrorAdd);

                } catch (Exception e) {
                    print(e);
                } finally {
                    log.trace("Koniec synchronizacji");
                    print(Console.INFO, "Koniec synchronizacji");

                    if (context != null) {
                        try {
                            context.close();
                        } catch (NamingException e) {
                            print(e);
                        }
                    }
                }
            } catch (Exception e) {
                print(e);
            } finally {
                log.trace("Zamkni�cie po��czenia z DS");
                DSApi._close();
            }
            lastSyncTimeInMillis = System.currentTimeMillis();
        }

        /**
         * @return ldap context connected as admin or null if not established connection
         */
        private LdapContext getLdapContext() throws Exception {
            print(Console.INFO, "��czenie do serwera AD");

            ActiveDirectoryManager ad = new ActiveDirectoryManager();
            LdapContext context = ad.LdapConnectAsAdminInLdapContext();

            if (context == null)
                print(Console.INFO, "Nie nawi�zano po��czenia z serwerem AD");
            else
                print(Console.INFO, "Nawi�zano po��czenia z serwerem AD");
            return context;
        }

        /**
         * @param ldapUrl   ldap://host:port
         * @param principal user@domain
         */
        @SuppressWarnings("unused")
        private LdapContext getLdapContext(String ldapUrl, String principal, String pass) throws Exception {
            LdapContext context = null;

            print(Console.INFO, "��czenie do serwera AD (" + ldapUrl + ")");

            Hashtable<String, String> env = new Hashtable<String, String>();
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            env.put(Context.PROVIDER_URL, ldapUrl);
            env.put(Context.SECURITY_AUTHENTICATION, "simple");
            env.put(Context.SECURITY_PRINCIPAL, principal);
            env.put(Context.SECURITY_CREDENTIALS, pass);
            try {
                context = new InitialLdapContext(env, null);
                print(Console.INFO, "Nawi�zano po��czenia z serwerem AD (" + ldapUrl + ")");
            } catch (NamingException e) {
                //do nothing
                print(Console.INFO, "Nie nawi�zano po��czenia z serwerem AD (" + ldapUrl + ")");
            }

            return context;
        }

        /**
         * Method requires DSApi.openAdmin() and ad.LdapConnectAsAdmin()
         */
        private ImportedUsersCollection importUsers_andAddToDatabase(LdapContext context, ArrayList<AdUsersDN> adUsersDNs, LdapImportServiceStats stats) {
            print(Console.INFO, "ROZPOCZ�CIE IMPORTOWANIA U�YTKOWNIK�W");

            ImportedUsersCollection iuCollection = new ImportedUsersCollection();
            ArrayList<AdUsersDN> errorNames = new ArrayList<AdUsersDN>();

            for (AdUsersDN adUsersDN : adUsersDNs) {
                boolean dnOk = importUsers_andAddToDatabase_search(context, iuCollection, adUsersDN, stats);
                if (!dnOk)
                    errorNames.add(adUsersDN);
            }

            for (AdUsersDN adUsersDN : errorNames)
                adUsersDNs.remove(adUsersDN);

            print(Console.INFO, "ZAKO�CZENIE IMPORTOWANIA U�YTKOWNIK�W");

            return iuCollection;
        }

        /**
         * Method requires DSApi.openAdmin() and ad.LdapConnectAsAdmin()
         */
        private boolean importUsers_andAddToDatabase_search(LdapContext context, ImportedUsersCollection iuCollection, AdUsersDN adUsersDN, LdapImportServiceStats stats) {
            print(Console.INFO, "Wyszukiwanie u�ytkownik�w, DN : " + adUsersDN.dn + " [" + (adUsersDN.searchScope == SearchControls.ONELEVEL_SCOPE ? "ONELEVE" : "SUBTREE") + "]");

            byte[] cookie;
            SearchControls searchControls = new SearchControls();
            searchControls.setSearchScope(adUsersDN.searchScope);

            try {
                // Activate paged results
                context.setRequestControls(new Control[]{new PagedResultsControl(SEARCH_PAGE_SIZE, Control.NONCRITICAL)});

                do {
                    NamingEnumeration<SearchResult> pSREnum = context.search(adUsersDN.dn, userFilter, searchControls);

                    if (pSREnum != null) {
                        importUsers_andAddToDatabase_searchPage(pSREnum, iuCollection, adUsersDN.dn, stats);
                    }

                    cookie = getPagedSearchCookie(context);
                    // Re-activate paged results
                    context.setRequestControls(new Control[]{new PagedResultsControl(SEARCH_PAGE_SIZE, cookie, Control.CRITICAL)});
                } while (cookie != null);

                //} catch (javax.naming.NameNotFoundException e) {
            } catch (Exception e) {
                print(e, "NIEPOPRAWNIE ZAIMPORTOWANI U�YTKOWNICY. �CIE�KA DO KATALOGU: " + adUsersDN.dn);
                return false;
            }

            return true;
        }

        private void importUsers_andAddToDatabase_searchPage(NamingEnumeration<SearchResult> userSREnum, ImportedUsersCollection iuCollection, String userSourceDirectory, LdapImportServiceStats stats) {
            print(Console.INFO, "Przetwa�anie pobranych danych o u�ytkownikach - SEARCH-PAGE");
            while (userSREnum.hasMoreElements()) {
                SearchResult userSR = userSREnum.nextElement();

                ImportedUser iUser = new ImportedUser(userSR, userSourceDirectory, LdapImportService.this);
                if (iUser.isValid()) {
                    iuCollection.add(iUser);
                    writeToDatabase(iUser, stats);

                    logImportedUser(iUser);

                } else {
                    //log reason
                    String reason = iUser.getReasonOfInvalid();

                    if (!iUser.isRemovableFromDS()) {
                        print(Console.WARN, "NIEPOPRAWNE INFORMACJE O U�YTKOWNIKU " + reason + ". U�YTKOWNIK " + iUser.getUsername() + " NIE ZOSTANIE USUNI�TY");

                        ++stats.uInvalidExisted;
                        iuCollection.add(iUser); // UWAGA! jest username, wi�c nie b�dzie taki u�ytkownik usuwany
                    } else {
                        print(Console.WARN, "NIEPOPRAWNE INFORMACJE O U�YTKOWNIKU " + reason);

                        ++stats.uInvalid;
                    }
                }

                ++stats.uImported;
            }

            refreshSession_safe();
        }

        /**
         * Method requires DSApi.openAdmin()
         */
        @SuppressWarnings("unused")
        private void disableActiveDirectoryUsersNotOnList(ImportedUsersCollection iuCollection, LdapImportServiceStats stats) {
            print(Console.INFO, "Blokowanie u�ytkownik�w");

            try {
                dsContext.begin();
                List<DSUser> users = DSUser.list(DSUser.SORT_FIRSTNAME_LASTNAME);
                for (DSUser dsUser : users) {
                    if (!iuCollection.contains(dsUser)) {
                        if (!isDSAdmin(dsUser)) { //UWAGA! "admin" jest omijany
                            String msg = "Wy��czanie active_directory_user dla " + dsUser.getName();

                            try {
                                //UserFactory.getInstance().deleteUser(dsUser.getName());
                                dsUser.setAdUser(false);
                                dsUser.update();

                                ++stats.uDeleted;
                                print(Console.WARN, msg);

                            } catch (Exception e) { //np. pr�ba usuni�cia zalogowanego u�ytkownika
                                ++stats.uErrorDeleted;
                                print(e, msg);
                            }
                        }
                    } else {
                        ++stats.uNotDeleted;
                    }
                }

                dsContext.commit();

            } catch (Exception e) {
                dsContext._rollback();
                print(e);
                return;
            }

            refreshSession_safe();
        }

        private boolean isDSAdmin(DSUser dsUser) {
            return dsUser.getName().equalsIgnoreCase("admin");
        }

        /**
         * Method requires DSApi.openAdmin()
         */
        private void refreshSession_safe() {
            try {
                refreshSession();
            } catch (Exception e) {
                print(e);
            }
        }

        /**
         * Method requires DSApi.openAdmin()
         */
        private void refreshSession() throws EdmException {
            try {
                dsContext.begin();
                dsContext.session().flush();
                dsContext.session().clear();
                dsContext.commit();
                DSApi.close();
            } catch (Exception e) {
                //do nothing
                print(e, "Prze�adowanie sesji");
            }
            dsContext = DSApi.openAdmin();
        }

        /**
         * Method requires DSApi.openAdmin() and ad.LdapConnectAsAdmin()
         */
        private UserProfilesStructure importProfiles(LdapContext context, ImportedUsersCollection iuCollection, ArrayList<AdProfilesDN> adProfilesDNs, LdapImportServiceStats stats) {
            print(Console.INFO, "ROZPOCZ�CIE IMPORTOWANIA STRUKTURY");

            //utworzenie mapy u�ytkownik�w i nale��cych do nich profil�w
            UserProfilesStructure upStructure = new UserProfilesStructure();
            ArrayList<AdProfilesDN> errorNames = new ArrayList<AdProfilesDN>();

            print(Console.INFO, "Tworzenie struktury profil�w u�ytkownik�w");
            for (AdProfilesDN profile : adProfilesDNs) {
                boolean dnOk = importProfiles_search(context, iuCollection, profile.adProfileDistinguishedName, profile.dbProfileName, upStructure);
                if (!dnOk)
                    errorNames.add(profile);
            }

            for (AdProfilesDN profile : errorNames)
                adProfilesDNs.remove(profile);

            stats.upHaveProfiles = upStructure.size();

            logUserProfilesStructure(upStructure);

            print(Console.INFO, "ZAKO�CZENIE IMPORTOWANIA STRUKTURY");
            return upStructure;
        }

        private byte[] getPagedSearchCookie(LdapContext context) {
            byte[] cookie = null;
            // Examine the paged results control response
            try {
                Control[] controls = context.getResponseControls();
                if (controls != null) {
                    for (Control control : controls) {
                        if (control instanceof PagedResultsResponseControl) {
                            cookie = ((PagedResultsResponseControl) control).getCookie();
                            break;
                        }
                    }
                } else {
                    print(Console.WARN, "Brak informacji zwrotnych (Control-s) od serwera");
                }
            } catch (Exception e) {
                print(e);
            }
            return cookie;
        }

        /**
         * Method requires DSApi.openAdmin() and ad.LdapConnectAsAdmin()
         *
         * @param profileAdDN         ad profile dn
         * @param profileDatabaseName database profile name
         * @param upStructure         extending structure
         */
        private boolean importProfiles_search(LdapContext context, ImportedUsersCollection iuCollection, String profileAdDN, String profileDatabaseName, UserProfilesStructure upStructure) {
            print(Console.INFO, "Wyszukiwanie u�ytkownik�w dla profilu " + profileDatabaseName);

            byte[] cookie;
            SearchControls searchControls = new SearchControls();
            searchControls.setSearchScope(profileScope);

            try {
                // Activate paged results
                context.setRequestControls(new Control[]{new PagedResultsControl(SEARCH_PAGE_SIZE, Control.NONCRITICAL)});

                do {
                    NamingEnumeration<SearchResult> pSREnum = context.search(profileAdDN, profileFilter, searchControls);

                    if (pSREnum != null) {
                        importProfiles_searchPage(pSREnum, iuCollection, profileDatabaseName, upStructure);
                    }

                    cookie = getPagedSearchCookie(context);
                    // Re-activate paged results
                    context.setRequestControls(new Control[]{new PagedResultsControl(SEARCH_PAGE_SIZE, cookie, Control.CRITICAL)});
                } while (cookie != null);

                //} catch (javax.naming.NameNotFoundException e) {
            } catch (Exception e) {
                print(e, "NIEPOPRAWNIE ZAIMPORTOWANE PROFILE. �CIE�KA DO GRUPY: " + profileAdDN);
                return false;
            }

            return true;
        }

        private void importProfiles_searchPage(NamingEnumeration<SearchResult> pSREnum, ImportedUsersCollection iuCollection, String profileDatabaseName, UserProfilesStructure upStructure) {
            print(Console.INFO, "Przetwa�anie pobranych danych dla profilu " + profileDatabaseName + " - SEARCH-PAGE");

            while (pSREnum.hasMoreElements()) {
                SearchResult pSR = pSREnum.nextElement(); // NoSuchElementException - impossible - pSREnum.hasMoreElements() above
                ArrayList<String> members = getAllStringAttrs(pSR, profilesAdAttrUserFirstnameLastName);
                for (String member : members) {
                    //znajd� u�ytkownika lub dodaj do mapy nowego
                    UserProfiles up = upStructure.findByMemberOrPut_orNull(member, iuCollection);
                    if (up != null) {
                        up.addProfile(profileDatabaseName);

                        logImportedUserProfiles(profileDatabaseName, up.getUsername());

                    } else { // impossible - user ought to be added during importing users
                        print(Console.ERROR, "U�ytkownik " + member + " nie zosta� dodany do struktury!");
                    }
                }
            }

            refreshSession_safe();
        }

        /**
         * Method requires DSApi.openAdmin()
         */
        private void updateUsersProfilesStructure(UserProfilesStructure upStructure, ImportedUsersCollection iuCollection, ArrayList<AdProfilesDN> adProfilesDNs, LdapImportServiceStats stats) {
            print(Console.INFO, "Przetwarzanie struktury profil�w u�ytkownik�w");

            logUserProfilesInDS(upStructure);

            boolean isInIUCollection;
            boolean isInUPCollection;

            if (TestProfiles.tryAddAllProfilesToAllUsers(dsContext, LdapImportService.this, upStructure, iuCollection, adProfilesDNs))
                return;
            if (TestProfiles.tryRemoveAllProfilesFromAllUsers(dsContext, LdapImportService.this, upStructure, iuCollection, adProfilesDNs))
                return;

            //usuwanie profil�w danemu u�ytkownikowi
            ArrayList<Long> usersToRemove = new ArrayList<Long>();
            for (AdProfilesDN adProfilesDN : adProfilesDNs) {
                try {

                    Profile profile = Profile.findByProfilename(adProfilesDN.dbProfileName);
                    Set<DSUser> users = profile.getUsers();

                    //debug("profil [" + adProfilesDN.dbProfileName + "] odnaleziony, liczba u�ytkownik�w = " + Integer.toString(users.size()));

                    usersToRemove.clear();
                    for (DSUser user : users) {
                        isInIUCollection = iuCollection.contains(user);
                        isInUPCollection = upStructure.contains(user, adProfilesDN.dbProfileName);
                        if (isInIUCollection && !isInUPCollection) {
                            logUserToRemoveFromProfile(adProfilesDN.dbProfileName, user.getName());
                            usersToRemove.add(user.getId());
                        } else
                            logUserToLeaveInProfile(adProfilesDN.dbProfileName, user.getName(), isInIUCollection, isInUPCollection);
                    }
                    if (!usersToRemove.isEmpty()) {
                        for (Long id : usersToRemove) {
                            try {
                                DSUser user = DSUser.findById(id);
                                final String msg = "Usuwanie u�ytkownikowi [" + user.getName() + "] profilu [" + profile.getName() + "]";
                                try {
                                    dsContext.begin();

                                    profile.removeUser(user);
                                    refreshProfile(profile);

                                    print(Console.WARN, msg);
                                    ++stats.pRemove;

                                    dsContext.commit();
                                } catch (EdmException e) {
                                    print(e, msg);
                                    ++stats.pErrorRemove;
                                    dsContext._rollback();
                                }
                            } catch (Exception e) {
                                print(e, "Nie odnaleziono u�ytkownika o id=" + (id == null ? "NULL" : id));
                            }
                        }
                    }

                } catch (Exception e) {
                    print(e);
                }
            }

            refreshSession_safe();

            //dodawanie profil�w danemu u�ytkownikowi
            Collection<UserProfiles> usersProfiles = upStructure.getAllUserProfiles();
            for (UserProfiles up : usersProfiles) {
                try {
                    dsContext.begin();
                    DSUser dsUser = DSUser.findByUsername(up.getUsername());

                    //AdProfilesDN[] adProfilesDNs = getAdProfilesDNs_safe(LdapImportService.this);
                    String[] dbProfilesNames = dsUser.getProfileNames();
                    String[] iProfilesNames = up.getProfilesNames();

                    for (String iPN : iProfilesNames) {
                        if (!isArrayContains(dbProfilesNames, iPN)) {
                            String msg = "Dodawanie u�ytkownikowi [" + dsUser.getName() + "] profilu [" + iPN + "]";

                            try {
                                Profile profile = Profile.findByProfilename(iPN);
                                profile.addUser(dsUser);
                                refreshProfile(profile);

                                print(Console.WARN, msg);
                                ++stats.pAdd;

                            } catch (Exception e) {
                                print(e, msg);
                                ++stats.pErrorAdd;
                            }
                        }
                    }
                    dsContext.commit();

                } catch (Exception e) {
                    print(e);
                    dsContext._rollback();
                }
            }

            refreshSession_safe();

            logUserProfilesInDS(upStructure);
        }

        /**
         * Method requires DSApi.openAdmin() and ad.LdapConnectAsAdmin()
         */
        @SuppressWarnings("unused")
        private Profile findOrCreateProfile(String profileName) throws Exception {
            Profile profile = null;

            try {
                Profile.findByProfilename(profileName);
            } catch (EdmException e) {

                print(Console.WARN, "Wyszukiwanie profilu : " + profileName);
                e.printStackTrace();
                try {
                    profile = Profile.create(profileName); //UWAGA! Je�eli nie ma profilu, jest tworzony
                } catch (EdmException e1) {
                    //do nothing
                    print(e, "Tworzenie profilu : " + profileName);
                }
            }
            if (profile == null)
                throw new Exception("Nie mo�na utworzy� profilu : " + profileName);

            return profile;
        }

        /**
         * Method requires DSApi.openAdmin()
         */
        private void writeToDatabase(ImportedUser iUser, LdapImportServiceStats stats) {
            try {
                dsContext.begin();

                boolean toSaveSession = false;
                DSUser user;
                try {
                    user = DSUser.findByUsername(iUser.getUsername());

                    if (iUser.isDifferent(user) && iUser.update(user)) {

                        toSaveSession = true;
                        log(Console.DEBUG, "Aktualizacja u�ytkownika " + iUser.getUsername() + " : " + iUser.getDifference(user));
                        ++stats.uUpdated;
                    } else
                        ++stats.uNotUpdated;

                } catch (UserNotFoundException e) {
                    user = createUser_orNull(iUser);
                    if (user != null) {
                        print(Console.WARN, "Utworzono nowego u�ytkownika : " + iUser.getUsername());
                        toSaveSession = true;
                        ++stats.uCreated;
                    } else {
                        print(Console.ERROR, "Nie uda�o si� utworzy� nowego u�ytkownika : " + iUser.getUsername());
                        ++stats.uErrorCreated;
                    }
                }

                if (user != null && toSaveSession)
                    saveUser_safe(user);

                dsContext.commit();
            } catch (Exception e) {
                print(e, "Zapisywanie uzytkownika " + iUser.getUsername() + " do bazy");
                dsContext._rollback();
            }

            refreshSession_safe();
        }

        /**
         * Method requires DSApi.openAdmin()
         */
        @SuppressWarnings("unused")
        private void updateUsersDivisions(ImportedUsersCollection iuCollection, LdapImportServiceStats stats) {

            boolean added;
            boolean removed;
            final int REFRESH_SESSION = 50;
            int working = 0;

            print(Console.INFO, "Dodawanie, usuwanie dzia��w zaimportowanym u�ytkownikom");

            Collection<ImportedUsersCollection.IUser> iUsers = iuCollection.getUsers();
            for (ImportedUsersCollection.IUser iu : iUsers) {
                if (iu.containsAnyGUIDs()) {

                    try {
                        dsContext.begin();
                        DSUser dsUser = DSUser.findByUsername(iu.name);

                        String[] dsGUIDs = dsUser.getDivisionsGuid();
                        ArrayList<String> iGUIDs = iu.divisionGUIDs;

                        //dodawanie dzia��w danemu u�ytkownikowi
                        added = false;
                        for (String idGUID : iGUIDs)
                            if (!isArrayContains(dsGUIDs, idGUID)) {
                                try {
                                    DSDivision division = DSDivision.find(idGUID);
                                    if (division != null) {
                                        division.addUser(dsUser);
                                        refreshDivision_safe(division);

                                        print(Console.WARN, "U�ytkownik " + iu.name + " zosta� dodany do dzia�u : " + division.getName());
                                        added = true;
                                        ++stats.dAdd;
                                    } else {
                                        print(Console.WARN, "Nieodnalzeiony dzia� " + idGUID);
                                        ++stats.dErrorNoDivision;
                                    }
                                } catch (Exception e) {
                                    print(e, "Dodawanie u�ytkownika " + iu.name + " do dzia�u " + idGUID);
                                    ++stats.dErrorAdd;
                                }
                            }

                        //usuwanie dzia��w danemu u�ytkownikowi - je�eli ds(aktualnego) nie ma w zaimportowanych(i)
                        removed = false;
                        for (String dsGUID : dsGUIDs)
                            if (!iGUIDs.contains(dsGUID)) {
                                try {
                                    DSDivision division = DSDivision.find(dsGUID);
                                    if (division != null) {
                                        division.removeUser(dsUser);
                                        refreshDivision_safe(division);

                                        print(Console.WARN, "U�ytkownik " + iu.name + " zosta� usuniety z dzia�u : " + division.getName());
                                        added = true;
                                        ++stats.dRemove;
                                    } else {
                                        print(Console.WARN, "Nieodnalzeiony dzia� " + dsGUID);
                                        ++stats.dErrorNoDivision;
                                    }
                                } catch (Exception e) {
                                    print(e, "Dodawanie u�ytkownika " + iu.name + " do dzia�u " + dsGUID);
                                    ++stats.dErrorRemove;
                                }
                            }

                        if (!removed && !added)
                            ++stats.udUnchanged;

                        dsContext.commit();

                    } catch (Exception e) {
                        print(e);
                        ++stats.udError;
                        dsContext._rollback();
                    }

                    if ((++working) % REFRESH_SESSION == 0)
                        refreshSession_safe();

                } else {
                    print(Console.ERROR, "U�ytkownik " + iu.name + " nie nale�y do �adnego dzia�u");
                    ++stats.udNoDivisions;
                }
            }

            refreshSession_safe();
        }

        /**
         * Method requires DSApi.openAdmin() and DSApi.context().begin()
         */
        private void refreshDivision_safe(DSDivision division) {
            try {
                dsContext.session().refresh(division);
            } catch (EdmException e) {
                print(e, "Prze�adowanie dzia�u");
            }
            try {
                dsContext.session().flush();
            } catch (EdmException e) {
                print(e, "Prze�adowanie dzia�u");
            }
        }

        /**
         * Method requires DSApi.openAdmin() and DSApi.context().begin()
         */
        @SuppressWarnings("unused")
        private void saveDivision_safe(DSDivision division) {
            try {
                dsContext.session().save(division);
            } catch (EdmException e) {
                print(e, "Prze�adowanie dzia�u");
            }
        }

        /**
         * Method requires DSApi.openAdmin() and DSApi.context().begin()
         */
        private void saveUser_safe(DSUser user) {
            try {
                dsContext.session().save(user);
            } catch (EdmException e) {
                print(e, "Prze�adowanie dzia�u");
            }
        }

        @SuppressWarnings("unused")
        private void refreshProfile(Profile profile) throws EdmException {
            //dsContext.session().refresh(profile);
            dsContext.session().flush();
        }

        /**
         * Method requires DSApi.openAdmin() and DSApi.context().begin()
         */
        private DSUser createUser_orNull(ImportedUser iUser) {
            DSUser user = null;
            try {
                user = iUser.createUser();
                print(Console.WARN, "U�ytkownik " + iUser.getUsername() + " dodany : " + user.asFirstnameLastname());
            } catch (Exception e) {
                print(e);
            }
            return user;
        }

        private void logImportedUser(ImportedUser user) {
            if (!LOG_USERS_STRUCTURE) return;
            StringBuilder txt = new StringBuilder();
            if (user.getUsername() != null) txt.append(user.getUsername()).append(" ");
            if (user.firstname != null) txt.append(user.firstname).append(" ");
            if (user.lastname != null) txt.append(user.lastname).append(" ");
            //if (user.divisionName != null) txt.append(user.divisionName).append(" ");
            if (user.email != null) txt.append(user.email).append(" ");
            log(Console.DEBUG, "[AD] " + txt.toString());
        }

        private void logUserProfilesStructure(UserProfilesStructure structure) {
            if (!LOG_USER_PROFILES_STRUCTURE) return;
            log(Console.DEBUG, "STRUKTURA - START");
            Collection<UserProfiles> aups = structure.getAllUserProfiles();
            for (UserProfiles up : aups)
                log(Console.DEBUG, "[AD] " + up.getUsername() + " : " + StringUtils.join(up.getProfilesNames(), "; "));
            log(Console.DEBUG, "STRUKTURA-END");
        }

        private void logImportedUserProfiles(String profileName, String username) {
            if (!LOG_JUST_IMPORTED_USER_PROFILES) return;
            log(Console.DEBUG, profileName + " : " + username);
        }

        private void logUserProfilesInDS(UserProfilesStructure structure) {
            if (!LOG_USER_PROFILES_IN_DS) return;
            Collection<UserProfiles> ups = structure.getAllUserProfiles();
            for (UserProfiles up : ups)
                logUserProfilesInDS(up.getUsername());
        }

        private void logUserProfilesInDS(String username) {

            try {
                dsContext.begin();
                DSUser dsUser = DSUser.findByUsername(username);
                String[] dbProfilesNames = dsUser.getProfileNames();
                log(Console.DEBUG, "[DS] " + username + " : " + StringUtils.join(dbProfilesNames, ", "));
                dsContext.commit();
            } catch (Exception e) {
                print(e, "Nieudany log z profilami [DS] dla u�ytkownika " + username);
                dsContext._rollback();
            }
        }

        private void logImportedProfiles(ArrayList<AdProfilesDN> adProfilesDNs) {
            if (!LOG_IMPORTED_PROFILES_DIRS) return;
            for (AdProfilesDN a : adProfilesDNs)
                log(Console.INFO, "Importowany profil: " + a.dbProfileName + " : " + a.adProfileDistinguishedName);
        }

        private void logImportedUsersDirs(ArrayList<AdUsersDN> adUsersDNs) {
            if (!LOG_IMPORTED_USERS_DIRS) return;
            for (AdUsersDN a : adUsersDNs)
                log(Console.INFO, "Importowana �cie�ka u�ytkownik�w: " + a.dn + " :" + a.searchScope);
        }

        private void logUserToRemoveFromProfile(String dbProfileName, String name) {
            if (!LOG_USER_TO_REMOVE_FROM_PROFILE) return;
            log(Console.INFO, "Profile: " + dbProfileName + ", removing user: " + name);
        }

        private void logUserToLeaveInProfile(String dbProfileName, String name, boolean inIUCollection, boolean inUPCollection) {
            if (!LOG_USER_TO_LEAVE_IN_PROFILE) return;
            log(Console.INFO, "Profile: " + dbProfileName + ", leaving user: " + name + (inIUCollection ? " [zaimportowany]" : " [NIEZAIMPORTOWANY]") + (inUPCollection ? " [w strukturze]" : " [BRAK W STRUKTURZE]"));
        }
//
//        private void logUserProfilesSQL(DSUser user, String head) {
//
//            PreparedStatement ps = null;
//            ResultSet rs = null;
//
//            StringBuilder sb = new StringBuilder();
//
//            try {
//                ps = DSApi.context().prepareStatement("select * from ds_profile_to_user with (nolock) where user_id=" + user.getId());
//                rs = ps.executeQuery();
//                while (rs.next()) {
//                    try {
//                        sb.append(rs.getString("profile_id")).append(", ");
//                    } catch (Exception e) {
//                        print(e);
//                    }
//
//                }
//                ps.close();
//                rs.close();
//            } catch (Exception e) {
//                print(e);
//            }
//
//            print(Console.WARN, head + " : " + user.getName() + " : " + sb.toString());
//        }
    }
}
