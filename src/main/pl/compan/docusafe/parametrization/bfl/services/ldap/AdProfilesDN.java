package pl.compan.docusafe.parametrization.bfl.services.ldap;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class AdProfilesDN {
    public final String dbProfileName;
    public final String adProfileDistinguishedName;

    public AdProfilesDN(String dbProfileName, String adProfileDN) {
        this.dbProfileName = dbProfileName;
        this.adProfileDistinguishedName = adProfileDN;
    }

    @SuppressWarnings("unused")
    public static boolean isDbProfileNameContains (AdProfilesDN[] adProfilesDNs, String dbProfileName){
        for (AdProfilesDN p : adProfilesDNs)
            if (p.dbProfileName.equalsIgnoreCase(dbProfileName))
                return true;
        return false;
    }
}
