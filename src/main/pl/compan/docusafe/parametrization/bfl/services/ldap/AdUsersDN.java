package pl.compan.docusafe.parametrization.bfl.services.ldap;

import javax.naming.directory.SearchControls;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class AdUsersDN {
    public final String dn;
    public final int searchScope;

    public AdUsersDN(String exDN) {
        this(exDN, SearchControls.ONELEVEL_SCOPE);
    }

    public AdUsersDN(String dn, int searchScope) {
        this.dn = dn;
        this.searchScope = searchScope;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdUsersDN adUsersDN = (AdUsersDN) o;

        if (searchScope != adUsersDN.searchScope) return false;
        return !(dn != null ? !dn.equals(adUsersDN.dn) : adUsersDN.dn != null);
    }
}
