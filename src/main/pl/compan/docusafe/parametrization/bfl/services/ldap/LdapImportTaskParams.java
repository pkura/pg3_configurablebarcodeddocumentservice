package pl.compan.docusafe.parametrization.bfl.services.ldap;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.boot.Docusafe;

import javax.naming.directory.SearchControls;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class LdapImportTaskParams {

    //
    // LDAP parameters
    //
    public static final String userFilter = "(&(objectClass=user)(objectClass=person)(objectClass=organizationalPerson))";
    public static final String profileFilter = "(&(objectClass=group))";
    public static final int profileScope = SearchControls.SUBTREE_SCOPE;
    public static final String profilesAdAttrUserFirstnameLastName = "member";
    private static final Map<String, Integer> searchScopeLevels;

    static {
        searchScopeLevels = new HashMap<String, Integer>();
        searchScopeLevels.put(":SUBTREE_SCOPE", SearchControls.SUBTREE_SCOPE);
        searchScopeLevels.put(":ONELEVEL_SCOPE", SearchControls.ONELEVEL_SCOPE);
        searchScopeLevels.put(":OBJECT_SCOPE", SearchControls.OBJECT_SCOPE);
    }

    public static ArrayList<AdUsersDN> getAdUsersDNs() {
        ArrayList<AdUsersDN> adUsersDNs = new ArrayList<AdUsersDN>();

        String adds = Docusafe.getAdditionProperty("active.directory.service.import.users.dirs");
        if (adds == null)
            return adUsersDNs;

        String[] exDNs = StringUtils.split(adds, ';');
//        AdUsersDN[] dns = new AdUsersDN[exDNs.length];
//        for (int i = 0; i < exDNs.length; ++i)
//            dns[i] = getAdUserDN(exDNs[i]);
//        return dns;
        for (String exDN : exDNs)
            adUsersDNs.add(getAdUserDN(exDN));

        return adUsersDNs;
    }

    private static AdUsersDN getAdUserDN(String exDN) {
        Set<Map.Entry<String, Integer>> ssls = searchScopeLevels.entrySet();
        for (Map.Entry<String, Integer> ssl : ssls)
            if (exDN.endsWith(ssl.getKey())) {
                String dn = exDN.substring(0, exDN.length() - ssl.getKey().length());
                return new AdUsersDN(dn, ssl.getValue());
            }
        return new AdUsersDN(exDN);
    }


//    public static AdProfilesDN[] getAdProfilesDNs_safe2(ConsoleLogProvider clp) {
//        ArrayList <AdProfilesDN> adProfilesDNs = new ArrayList<AdProfilesDN>();
//
//        adProfilesDNs.add(new AdProfilesDN("Administrator", "CN=DocuSafe_Administrator,OU=Grupy,OU=BFL,OU=Regiony,DC=BFL,DC=bankowyleasing,DC=pl"));
//        adProfilesDNs.add(new AdProfilesDN("Administrator danych", "CN=DocuSafe_Administrator_danych,OU=Grupy,OU=BFL,OU=Regiony,DC=BFL,DC=bankowyleasing,DC=pl"));
//        adProfilesDNs.add(new AdProfilesDN("II. Dyrektorzy i radcowie", "CN=DocuSafe_Dyrektorzy_i_radcowie,OU=Grupy,OU=BFL,OU=Regiony,DC=BFL,DC=bankowyleasing,DC=pl"));
//        adProfilesDNs.add(new AdProfilesDN("I. Obowi�zuj�ce odczyt - wszystko", "CN=DocuSafe_Obowi�zuj�ce_odczyt_wszystko,OU=Grupy,OU=BFL,OU=Regiony,DC=BFL,DC=bankowyleasing,DC=pl"));//CN=DocuSafe_Obowi%C4%85zuj%C4%85ce_odczyt_wszystko,OU=Grupy,OU=BFL,OU=Regiony,DC=BFL,DC=bankowyleasing,DC=pl
//        adProfilesDNs.add(new AdProfilesDN("III. Odczyt ograniczony", "CN=DocuSafe_Odczyt_ograniczony,OU=Grupy,OU=BFL,OU=Regiony,DC=BFL,DC=bankowyleasing,DC=pl"));
//
//        return adProfilesDNs.toArray(new AdProfilesDN[adProfilesDNs.size()]);
//    }

    //
    //    I WERSJA ODNAJDOWANIA PROFIL�W NA SERWERZE AD -> KONWERTOWANIE NAZWY PROFIL�W W BAZIE AD (PRZEDROSTEK + ZAMIANA ' ' NA '_')
    //
    @SuppressWarnings("unused")
    public static ArrayList<AdProfilesDN> getAdProfilesDNs_safe(ConsoleLogProvider clp) {
        ArrayList<AdProfilesDN> adProfilesDNs = new ArrayList<AdProfilesDN>();

        String mapTxt = Docusafe.getAdditionProperty("active.directory.users.profiles");
        if (mapTxt != null) {
            String[] pairsTxt = StringUtils.splitByWholeSeparator(mapTxt, " ||| ");
            for (String pairTxt : pairsTxt) {
                String[] pair = StringUtils.splitByWholeSeparator(pairTxt, " &&& ");
                if (pair.length == 2)
                    adProfilesDNs.add(new AdProfilesDN(pair[0], pair[1]));
            }
        }

        return adProfilesDNs/*.toArray(new AdProfilesDN[adProfilesDNs.size()])*/;
    }

    //
    //    II WERSJA ODNAJDOWANIA PROFIL�W NA SERWERZE AD -> KONWERTOWANIE NAZWY PROFIL�W W BAZIE AD (PRZEDROSTEK + ZAMIANA ' ' NA '_')
    //
    //    public static AdProfilesDN[] getAdProfilesDNs_safe(ConsoleLogProvider clp) {
    //        try {
    //            return getAdProfilesDNs();
    //        } catch (Exception e) {
    //            clp.toPrint(e);
    //            return new AdProfilesDN[0];
    //        }
    //    }
    //
    //    public static AdProfilesDN[] getAdProfilesDNs() throws EdmException {
    //        String[] dsProfilesNames = Profile.getProfilesNames();
    //        AdProfilesDN[] adProfilesDNs = new AdProfilesDN[dsProfilesNames.length];
    //        for (int i = 0; i < dsProfilesNames.length; ++i)
    //            adProfilesDNs[i] = getAdProfilesDN(dsProfilesNames[i]);
    //        return adProfilesDNs;
    //    }
    //
    //    private static AdProfilesDN getAdProfilesDN(String dsProfileName) {
    //        return new AdProfilesDN(dsProfileName, convertToAdProfileDN(convertToAdProfileName(dsProfileName)));
    //    }
    //
    //    private static String convertToAdProfileName(String dsProfileName) {
    //        return StringUtils.replaceChars("DocuSafe_" + dsProfileName, ' ', '_');
    //    }
    //
    //    private static String convertToAdProfileDN(String adProfileName) {
    //        return "CN=" + adProfileName + ",OU=Grupy,OU=BFL,OU=Regiony,DC=BFL,DC=bankowyleasing,DC=pl";
    //    }
}
