package pl.compan.docusafe.parametrization.bfl.services.ldap;

import pl.compan.docusafe.core.users.DSUser;

import java.util.ArrayList;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class UserProfiles {
    private final UPUser user;
    private ArrayList<UPProfile> profiles = new ArrayList<UPProfile>();

    @SuppressWarnings("unused")
    public UserProfiles(DSUser user) {
        this.user = new UPUser(user.getName());
    }

    public UserProfiles(ImportedUsersCollection.IUser user) {
        this.user = new UPUser(user.name);
    }

    public UserProfiles(String name) {
        this.user = new UPUser(name);
    }

    public String[] getProfilesNames() {
        String[] profilesNames = new String[profiles.size()];
        for (int i = 0; i < profiles.size(); ++i)
            profilesNames[i] = profiles.get(i).name;
        return profilesNames;
    }

    public boolean equals(String username) {
        return username != null && !user.username.equals(username);
    }

    public boolean contains(UPProfile profile) {
        return contains(profile.name);
    }

    public boolean contains(String profileName) {
        for (UPProfile upP : profiles)
            if (upP.name.equals(profileName))
                return true;
        return false;
    }

    public void addProfile(String[] profilesNames) {
        for (String profileName : profilesNames)
            addProfile(profileName);
    }

    public void addProfile(String profileName) {
        profiles.add(new UPProfile(profileName));
    }

    public boolean equalsUsername(UserProfiles up) {
        return getUsername().equalsIgnoreCase(up.getUsername());
    }

    public String getUsername() {
        return user.username;
    }

    public static class UPUser {
        public final String username;

        public UPUser(String username) {
            this.username = username;
        }
    }

    public static class UPProfile {
        public final String name;

        public UPProfile(String name) {
            this.name = name;
        }
    }
}
