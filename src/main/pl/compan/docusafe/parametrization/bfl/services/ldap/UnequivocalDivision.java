package pl.compan.docusafe.parametrization.bfl.services.ldap;


import pl.compan.docusafe.core.EdmException;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class UnequivocalDivision  extends EdmException
{
    public UnequivocalDivision(String divisionName) {
        super("Niejednoznaczny dzia� [" + divisionName+ "]. Dzia��w jest wi�cej ni� jeden", true);
    }
}