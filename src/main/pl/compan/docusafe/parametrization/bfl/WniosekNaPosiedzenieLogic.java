package pl.compan.docusafe.parametrization.bfl;

import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.FolderResolver;
import pl.compan.docusafe.core.dockinds.field.FieldValue;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;

public class WniosekNaPosiedzenieLogic extends AbstractBFLLogic
{
	private static final WniosekNaPosiedzenieLogic instance = new WniosekNaPosiedzenieLogic();
	private static final Logger log = LoggerFactory.getLogger(WniosekNaPosiedzenieLogic.class);
	
	public static WniosekNaPosiedzenieLogic getInstance() 
	{
		return instance;
	}

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException 
    {
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Wnioski na posiedzenie");
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(fm.getValue("Z_DNIA")));
		document.setFolder(folder);

		document.setTitle("Wniosek na posiedzenie nr " + String.valueOf(fm.getValue("NUMER")));
		document.setDescription("Wniosek na posiedzenie nr " + String.valueOf(fm.getValue("NUMER")));
		as.fireArchiveListeners(document);
	}
    
}
