package pl.compan.docusafe.parametrization.bfl;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

public class UchwalaLogic extends AbstractBFLLogic
{
	private static final UchwalaLogic instance = new UchwalaLogic();
	private static final Logger log = LoggerFactory.getLogger(UchwalaLogic.class);
	
	public static UchwalaLogic getInstance() 
	{
		return instance;
	}

	@Override
	public void onSaveActions(DocumentKind kind, Long documentId,
			Map<String, ?> fieldValues) throws SQLException, EdmException {

	}
	
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Uchwa�y");
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(fm.getValue("DNIA")));
		document.setFolder(folder);

		document.setTitle("Uchwa�a nr " + String.valueOf(fm.getValue("NUMER")));
		document.setDescription("Uchwa�a nr " + String.valueOf(fm.getValue("NUMER")));
		
		

			Statement stat = null;

			try
			{
				stat = DSApi.context().createStatement();
				
				stat.executeUpdate("DELETE FROM dsg_bfl_multiple_value WHERE FIELD_CN = 'DSG_BFL_UCHWALA' AND FIELD_VAL = " + document.getId());
				
				if (fm.getValue("DSG_BFL_PROTOKOL") != null)
				{
					Object a = fm.getValue("DSG_BFL_PROTOKOL");
                    Object aDocId = (a instanceof Map && ((Map)a).containsKey("id")) ? ((Map)a).get("id") : a;
					stat.execute("INSERT INTO dsg_bfl_multiple_value (document_id,field_cn,field_val) values ( " + aDocId + ", 'DSG_BFL_UCHWALA' ," + document.getId() + ")");
				}
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
			}
			finally
			{
				DSApi.context().closeStatement(stat);
			}
			as.fireArchiveListeners(document);
		}
    

}
