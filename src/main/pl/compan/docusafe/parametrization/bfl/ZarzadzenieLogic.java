package pl.compan.docusafe.parametrization.bfl;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.Statement;
import java.util.List;
import java.util.Map;

public class ZarzadzenieLogic extends AbstractBFLLogic
{
	private static final ZarzadzenieLogic instance = new ZarzadzenieLogic();
	private static final Logger log = LoggerFactory.getLogger(ZarzadzenieLogic.class);
	
	public static ZarzadzenieLogic getInstance() 
	{
		return instance;
	}

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Zarządzenia");
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(fm.getValue("DNIA")));
		document.setFolder(folder);
		String title = getTitle(fm);
		document.setTitle(title);
		document.setDescription(title);
		Statement stat = null;

		try
		{
			stat = DSApi.context().createStatement();
			
			stat.executeUpdate("DELETE FROM dsg_bfl_multiple_value WHERE FIELD_CN = 'dsg_bfl_legislacyjny' AND DOCUMENT_ID = " + document.getId());
			
			if (fm.getValue("DSG_BFL_LEGISLACYJNY") != null)
			{
				List ids = (List)fm.getValue("DSG_BFL_LEGISLACYJNY");
				for(Object a : ids)
				{
                    Object aDocId = (a instanceof Map && ((Map)a).containsKey("id")) ? ((Map)a).get("id") : a;
					stat.execute("INSERT INTO dsg_bfl_multiple_value (document_id,field_cn,field_val) values ( " + document.getId() + ", 'dsg_bfl_legislacyjny' ," + aDocId + ")");
					stat.executeUpdate("DELETE FROM dsg_bfl_multiple_value WHERE FIELD_CN = 'dsg_bfl_zarzadzenie' AND DOCUMENT_ID = " + aDocId +"AND FIELD_VAL="+document.getId());
					stat.execute("INSERT INTO dsg_bfl_multiple_value (document_id,field_cn,field_val) values ( " + aDocId + ", 'dsg_bfl_zarzadzenie' ," + document.getId() + ")");
				}
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			DSApi.context().closeStatement(stat);
		}
		
		log.error("WYKONUJE fireArchiveListeners");
		as.fireArchiveListeners(document);
	}
    
    private String getTitle(FieldsManager fm) throws EdmException
    {
    	String title = "Zarządzenie";
    	if(fm.getValue("NUMER") != null && ((String)fm.getValue("NUMER")).length() > 0 )
		{
    		if(fm.getValue("opis") != null)
    			title = "Zarządzenie nr " + String.valueOf(fm.getValue("NUMER")) +String.valueOf(fm.getValue("opis")) ;
    		else 
    			title = "Zarządzenie nr " + String.valueOf(fm.getValue("NUMER"));
		}
		else if (fm.getValue("opis") != null )
		{
			title = "Zarządzenie " +String.valueOf(fm.getValue("opis")) ;
		}
    	return title;
    }
    

}
