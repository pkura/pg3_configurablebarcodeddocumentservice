package pl.compan.docusafe.parametrization.bfl;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.Statement;
import java.util.List;
import java.util.Map;

public class LegislacyjnyLogic extends AbstractBFLLogic
{
	private static final LegislacyjnyLogic instance = new LegislacyjnyLogic();
	private static final Logger log = LoggerFactory.getLogger(LegislacyjnyLogic.class);
	
	public static LegislacyjnyLogic getInstance() 
	{
		return instance;
	}

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
		//FolderInserter.root().subfolder("Zarządzenia").insert(document);
    	
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Dokumenty legislacyjne");
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(document.getCtime()));
		document.setFolder(folder);

		String title = getTitle(fm,document);
		document.setTitle(title);
		document.setDescription(title);
		
		document.setTitle("Dokument legislacyjny utworzony " + DateUtils.formatCommonDate(document.getCtime()));
		document.setDescription("Dokument legislacyjny utworzony " + DateUtils.formatCommonDate(document.getCtime()));
		
		Statement stat = null;

		try
		{
			stat = DSApi.context().createStatement();
			
			stat.executeUpdate("DELETE FROM dsg_bfl_multiple_value WHERE FIELD_CN = 'DSG_BFL_ZARZADZENIE' AND DOCUMENT_ID = " + document.getId());
			
			if (fm.getValue("DSG_BFL_ZARZADZENIE") != null)
			{
				List ids = (List)fm.getValue("DSG_BFL_ZARZADZENIE");
				for(Object a : ids)
				{
                    Object aDocId = (a instanceof Map && ((Map)a).containsKey("id")) ? ((Map)a).get("id") : a;
					stat.execute("INSERT INTO dsg_bfl_multiple_value (document_id,field_cn,field_val) values ( " + document.getId() + ", 'dsg_bfl_zarzadzenie', " + aDocId + ")");
					stat.executeUpdate("DELETE FROM dsg_bfl_multiple_value WHERE FIELD_CN = 'dsg_bfl_legislacyjny' AND DOCUMENT_ID = " + aDocId +" AND FIELD_VAL = "+document.getId());
					stat.execute("INSERT INTO dsg_bfl_multiple_value (document_id,field_cn,field_val) values ( " + aDocId + ", 'dsg_bfl_legislacyjny', " + document.getId() + ")");
 				}
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			DSApi.context().closeStatement(stat);
		}
		
		as.fireArchiveListeners(document);
	}
    private String getTitle(FieldsManager fm, Document document) throws EdmException
    {
    	String title = "Dokument legislacyjny";
    	if(fm.getValue("opis") != null)
		{
    		title = "Dokument legislacyjny " +String.valueOf(fm.getValue("opis")) ;
		}
		else if (fm.getValue("typ") != null )
		{
			title = "Dokument legislacyjny " +fm.getStringValue("typ");
		}
		else
		{
			title = "Dokument legislacyjny utworzony " + DateUtils.formatCommonDate(document.getCtime());
		}
    	return title;
    }
    
}
