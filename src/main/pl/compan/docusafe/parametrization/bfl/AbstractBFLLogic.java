package pl.compan.docusafe.parametrization.bfl;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.PdfLayerParams;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.ocr.FileToOcrSupport;
import pl.compan.docusafe.core.ocr.OcrSupport;
import pl.compan.docusafe.util.TextUtils;

public abstract class AbstractBFLLogic extends AbstractDocumentLogic
{
	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn() +document.getFieldsManager().getKey("status")+ "_DOCUMENT_READ", ObjectPermission.GROUP, 
				document.getDocumentKind().getName()+" " + TextUtils.nullSafeObject(document.getFieldsManager().getValue("status")) + " - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn() + "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP,
				document.getDocumentKind().getName()+" zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn() +document.getFieldsManager().getKey("status")+  "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP,
				document.getDocumentKind().getName()+" " + TextUtils.nullSafeObject(document.getFieldsManager().getValue("status")) + " - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn() + "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, 
				document.getDocumentKind().getName()+" zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn() +document.getFieldsManager().getKey("status")+ "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, 
				document.getDocumentKind().getName()+" " + TextUtils.nullSafeObject(document.getFieldsManager().getValue("status")) + " - usuwanie"));
		perms.add(new PermissionBean(ObjectPermission.READ_ORGINAL_ATTACHMENTS, document.getDocumentKind().getCn() + "_READ_ORG", ObjectPermission.GROUP,
				document.getDocumentKind().getName()+" - odczyt orgina�u"));
   	 	this.setUpPermission(document, perms);

	}
	
    public PdfLayerParams getPdfLayerParams(Document document) throws EdmException
    {
    	PdfLayerParams params = null;
    	FieldsManager fm = document.getFieldsManager();
    	Object status = fm.getKey("status");
    	Integer intStatus = null;
    	if(status != null)
    		intStatus = (Integer) status;
    	if(intStatus != null && (intStatus.equals(22)|| intStatus.equals(23)|| intStatus.equals(24)))
    	{
	    	params = new PdfLayerParams();
	    	params.setLltext(fm.getValue("status").toString());
    	}
    	return params;
    }
    
	@Override
	public OcrSupport getOcrSupport()
	{
		return FileToOcrSupport.get();
	}
}
