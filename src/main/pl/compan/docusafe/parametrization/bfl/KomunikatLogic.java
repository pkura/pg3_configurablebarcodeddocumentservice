package pl.compan.docusafe.parametrization.bfl;

import java.util.Date;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class KomunikatLogic extends AbstractBFLLogic
{
	private static final KomunikatLogic instance = new KomunikatLogic();
	private static final Logger log = LoggerFactory.getLogger(KomunikatLogic.class);
	
	public static KomunikatLogic getInstance() 
	{
		return instance;
	}

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Komunikaty");
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(fm.getValue("Z_DNIA")));
		document.setFolder(folder);
		document.setTitle("Komunikat utworzony " + FolderInserter.toYearMonthDay(fm.getValue("Z_DNIA")));
		document.setDescription("Komunikat utworzony " + FolderInserter.toYearMonthDay(fm.getValue("Z_DNIA")));
		as.fireArchiveListeners(document);
	}
}
