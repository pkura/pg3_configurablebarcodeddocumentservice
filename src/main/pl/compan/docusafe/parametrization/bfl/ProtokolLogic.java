package pl.compan.docusafe.parametrization.bfl;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

public class ProtokolLogic extends AbstractBFLLogic
{
	private static final ProtokolLogic instance = new ProtokolLogic();
	private static final Logger log = LoggerFactory.getLogger(ProtokolLogic.class);
	
	public static ProtokolLogic getInstance() 
	{
		return instance;
	}

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
		FieldsManager fm  = document.getDocumentKind().getFieldsManager(document.getId());

		//FolderInserter.root().subfolder("Zarz�dzenia").insert(document);
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Protoko�y");
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(fm.getValue("Z_DNIA")));
		document.setFolder(folder);

		document.setTitle("Protok� nr " + String.valueOf(fm.getValue("NUMER")));
		document.setDescription("Protok� nr " + String.valueOf(fm.getValue("NUMER")));
		
		


		if (fm.getValue("DSG_BFL_UCHWALA") != null)
		{
			System.out.println(fm.getValue("DSG_BFL_UCHWALA"));
			
			List ids = (List)fm.getValue("DSG_BFL_UCHWALA");
			Statement stat = null;

			try
			{
				stat = DSApi.context().createStatement();
				stat.executeUpdate("UPDATE DSG_BFL_UCHWALA SET protokol_nr = null WHERE protokol_nr = " + document.getId());

				for(Object a : ids)
				{
                    Object aDocId = (a instanceof Map && ((Map)a).containsKey("id")) ? ((Map)a).get("id") : a;
					stat.executeUpdate("UPDATE DSG_BFL_UCHWALA SET protokol_nr = " + document.getId() + " WHERE DOCUMENT_ID = " + aDocId);
				}
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
			}
			finally
			{
				DSApi.context().closeStatement(stat);
			}
			
		}
		as.fireArchiveListeners(document);
	}
    
    public void onSaveActions(DocumentKind kind, Long documentId,
    		Map<String, ?> fieldValues) throws SQLException, EdmException 
    {

	}
    	    
    
}
