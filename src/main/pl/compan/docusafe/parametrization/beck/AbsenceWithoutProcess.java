package pl.compan.docusafe.parametrization.beck;

import static pl.compan.docusafe.util.FolderInserter.root;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.absences.Absence;
import pl.compan.docusafe.core.base.absences.AbsenceType;
import pl.compan.docusafe.core.calendar.CalendarFactory;
import pl.compan.docusafe.core.calendar.sql.SqlCalendarFactory;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.web.common.form.UpdateUser;

/**
 * Klasa AbsenceWithoutProcess.java
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 */
public class AbsenceWithoutProcess extends AbsenceRequestLogic {

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		if(!(document instanceof OfficeDocument))
			throw new EdmException("Nie mo�na doda� Wniosku w archiwum");
        DocumentKind dk = document.getDocumentKind();
        FieldsManager fm = dk.getFieldsManager(document.getId());
		UserImpl author =  (UserImpl)DSUser.findByUsername(fm.getEnumItemCn("USER"));
		List<UserImpl> supervisors = author.getSupervisorsHierarchy();
		if (supervisors == null || supervisors.size() == 0)
		{
			throw new EdmException("Brak prze�o�onego. Prosz� zdefiniowa� prze�o�onego a nast�pnie spr�bowa� ponownie");
		}
		String from = pl.compan.docusafe.util.DateUtils.formatJsDate(((Date) fm.getValue("FROM")));//; 
    	String to = pl.compan.docusafe.util.DateUtils.formatJsDate(((Date) fm.getValue("TO")));
    	if(((Date) fm.getValue("FROM")).getTime() > ((Date) fm.getValue("TO")).getTime())
    	{
    		throw new EdmException("Data od jest wi�ksza od daty do");
    	}
		String user = author.asLastnameFirstname();//""+document.getFieldsManager().getValue("USER");
		root().subfolder("Dokumenty pracownicze").subfolder("Nieobecno��")
		.subfolder(FolderInserter.to2Letters(user)).subfolder(user).insert(document);

		document.setTitle("Nieobecno�� ("+from +" "+to+")");
	//	((OfficeDocument) document).setSummaryWithoutHistory("Wniosek urlopowy : " + document.getFieldsManager().getValue("STATUS") + "");
		document.setDescription("Nieobecno�� ("+from +" "+to+")");
		
	}
	
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
        Map<String,Object> values = new HashMap<String,Object>();
        values.put("USER",fm.getField("USER").getEnumItemByCn(DSApi.context().getPrincipalName()).getId());
        fm.reloadValues(values);
    }
	
    public void onStartProcess(OfficeDocument document)
    {
        DocumentKind dk = document.getDocumentKind();
        FieldsManager fm = dk.getFieldsManager(document.getId());
        try
        {
        	Date from = (Date) fm.getKey("FROM");
        	Date to = (Date) fm.getKey("TO");
        	Calendar cal = Calendar.getInstance();
        	cal.setTime(to);
        	cal.set(Calendar.HOUR_OF_DAY, 23);
        	cal.set(Calendar.MINUTE, 59);
        	DSUser user = DSUser.findByUsername(fm.getEnumItemCn("USER"));
			//CalendarFactory.getInstance().createEvents(user, from, to,"[ Nieobecny ]");
			addAbsence(user, from, to);
            WorkflowFactory.getInstance().manualFinish(document.getId(), false);
            user.setSubstitution(DSUser.findByUsername(document.getFieldsManager().getEnumItemCn("SUBSTITUTION")), from, cal.getTime());
        } 
        catch (Exception e) {
        	LogFactory.getLog("eprint").error("", e);
		}
    }
    
    public void addAbsence(DSUser user,Date from, Date to) throws Exception
    {
		EmployeeCard empCard = getEmployeeCard(user);
		if (empCard == null)
			throw new IllegalStateException("Brak karty pracownika! Nie mo�na przypisa� urlopu!");
		
		int year = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
		
		// utworzenie wpisu urlopu
		Absence absence = new Absence();
		
		absence.setYear(year);
		absence.setEmployeeCard(empCard);

		absence.setStartDate(from);
		absence.setEndDate(to);

		AbsenceType absenceType = Finder.find(AbsenceType.class, "NP");
		absence.setAbsenceType(absenceType);
		
		// ustawienie liczby dni
		Integer numDays = DateUtils.substract(to, from);
		absence.setDaysNum(numDays);
		
		// ustawienie opisu
		absence.setInfo("Nieobecno��");
		
		// data utworzenia wpisu
		absence.setCtime(new Date());
		empCard.getAbsences().add(absence);
		DSApi.context().session().update(empCard);
		DSApi.context().session().save(absence);
        if(AvailabilityManager.isAvailable("absence.createEventOnCreate"))
        {
        	CalendarFactory.getInstance().createEvents(user,from, to,"[ Nieobecny ]",absence.getId());
        }
    }
    
	/**
	 * Zwraca kart� pracownika
	 * 
	 * @param userImpl
	 * @return
	 * @throws Exception
	 */
	private EmployeeCard getEmployeeCard(DSUser userImpl) throws Exception
	{
		return (EmployeeCard) DSApi.context().session().createQuery(
				"from " + EmployeeCard.class.getName() + " e where e.user = ?")
				.setParameter(0, userImpl)
				.setMaxResults(1)
				.uniqueResult();
	}
}

