package pl.compan.docusafe.parametrization.beck;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class DivisionAssigmentListener extends AbstractEventListener
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	String division;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(DivisionAssigmentListener.class);
	
	public void notify(EventListenerExecution eventListenerExecution) throws Exception 
	{
		if (StringUtils.isBlank(division))
		{
			division = DSDivision.ROOT_GUID;
		}
		
		LOG.info("Dekretacja na dzia�: " + division);
		
		String activityId = eventListenerExecution.getVariable(Jbpm4Constants.ACTIVITY_ID_PROPERTY).toString();
        LOG.info("activityId = " + activityId);
		WorkflowFactory.simpleAssignment(activityId,
				division,
				null,
				DSApi.context().getPrincipalName(),
				null,
				WorkflowFactory.SCOPE_DIVISION,
				"do archiwum",
				null);
		
		eventListenerExecution.setVariable(Jbpm4Constants.REASSIGNMENT_PROPERTY, true);
	}
}
