package pl.compan.docusafe.parametrization.beck;

import java.util.List;

import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Listener przypisujący kolejnych przełożonych
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class SupervisorAssigmentListener extends AbstractEventListener 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(SupervisorAssigmentListener.class);
	
	private static final String SUPERVISOR_ACCEPTANCE_CN = "S_A_CN";
	
	/**
	 * Domyślny konstruktor
	 */
	public SupervisorAssigmentListener() 
	{
		// ---
	}
	
	public void notify(EventListenerExecution eventListenerExecution) throws Exception 
	{
		Long docId = Long.parseLong(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		LOG.info("doc id = " + docId);
		
		// pobranie obiektu dokumentu
		Document document = Document.find(docId);
		
		// pobranie usera i jego przełożonych
		UserImpl author = (UserImpl) DSUser.findByUsername(document.getAuthor());
		List<UserImpl> supervisors = author.getSupervisorsHierarchy();
		if (supervisors == null || supervisors.size() == 0)
		{
			LOG.info("Nie zdefiniowana żadnego przełożonego dla użytkownika o nazwie: " + author.getName());
			throw new IllegalStateException("Brak zdefiniowanych przełożonych");
		}
		
		UserImpl supervisor = null;
		
		// sprawdzenie czy istnieje wpis w DocumentAcceptance
		DocumentAcceptance acceptance = DocumentAcceptance.find(docId, SUPERVISOR_ACCEPTANCE_CN, null);
		if (acceptance != null)
		{
			LOG.info("SupervisorAssigment.notify acceptance != null");
			// jeśli istnieje to pobranie kolejnego przełożonego
			String nextSupervisorName = null;
			for (int i = 0; i < supervisors.size(); i++)
			{
				if (supervisors.get(i).getName().equals(acceptance.getUsername()) 
						&& i + 1 < supervisors.size()) 
				{
					nextSupervisorName = supervisors.get(i + 1).getName();
				}
			}
			
			LOG.info("SupervisorAssigment.notify acceptance != null " + nextSupervisorName);
			
			if (nextSupervisorName == null)
			{
				LOG.info("Brak zdefiniowanych wiecej przelozonych");
				throw new IllegalStateException("Brak zdefiniowanych przełożonych");
			}
			supervisor = (UserImpl) DSUser.findByUsername(nextSupervisorName);
		}
		else
		{
			// brak wpisu w DocuemntAcceptance, pobranie pierwszego przełożonego
			supervisor = supervisors.iterator().next();
		}
		
		LOG.info("Dekretacja na: " + supervisor.getName());
		
        // pobranie activityId
		String activityId = eventListenerExecution.getVariable(Jbpm4Constants.ACTIVITY_ID_PROPERTY).toString();
        LOG.info("activityId = " + activityId);
        
        WorkflowFactory.simpleAssignment(
                activityId,
				supervisor.getAllDivisions()[0].getGuid(),
				supervisor.getName(),
				author.getName(),
				null,
				WorkflowFactory.SCOPE_ONEUSER,
				"do procesowania",
				null);
        
        eventListenerExecution.setVariable(Jbpm4Constants.REASSIGNMENT_PROPERTY, true);
	}
	
	
}
