package pl.compan.docusafe.parametrization.beck;

import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class NormalLogic extends AbstractDocumentLogic
{
    private static NormalLogic instance;

    public static NormalLogic getInstance()
    {
        if (instance == null)
            instance = new NormalLogic();
        return instance;
    }

    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
    {

    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {

    }

	public void documentPermissions(Document document) throws EdmException 
	{	
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ,"DOCUMENT_READ",ObjectPermission.GROUP,"Dokumenty zwyk造- odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"DOCUMENT_READ_ATT_READ",ObjectPermission.GROUP,"Dokumenty zwyk造 zalacznik - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY,"DOCUMENT_READ_MODIFY",ObjectPermission.GROUP,"Dokumenty zwyk造 - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"DOCUMENT_READ_ATT_MODIFY",ObjectPermission.GROUP,"Dokumenty zwyk造 zalacznik - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.DELETE,"DOCUMENT_READ_DELETE",ObjectPermission.GROUP,"Dokumenty zwyk造 - usuwanie"));
   	 	
   	 	this.setUpPermission(document, perms);	
	}

    /**
     * Dla zwyk造ch dokument闚 musi by� r璚zny wyb鏎 rodzaju pisma przychodz帷ego - dlatego zwracam null.
     */
  //  public String getInOfficeDocumentKind()
    {
 //       return null;
    }
    
    /**
     * Zwraca polityk� bezpiecze雟twa dla dokument闚 tego rodzaju.
     */
    public int getPermissionPolicyMode()
    {
        return PermissionManager.NORMAL_POLICY;
    }
    
    @Override
    public boolean isPersonalRightsAllowed()
    {
    	return true;
    }

}
