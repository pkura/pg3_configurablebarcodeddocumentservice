package pl.compan.docusafe.parametrization.beck;

import static pl.compan.docusafe.util.FolderInserter.root;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.absences.Absence;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.base.absences.EmployeeAbsenceEntry;
import pl.compan.docusafe.core.base.absences.FreeDay;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbsenceLogic;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.parametrization.aegon.AbstractAbsenceRequestLogic;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Logika wniosku urlopowego Beck
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class AbsenceRequestLogic extends AbstractDocumentLogic implements AbsenceLogic
{
	private static final Logger LOG = LoggerFactory.getLogger(AbsenceRequestLogic.class);
	
	private static final String KIND_CN = "KIND";
	private static final String RECREATIONAL_CN = "RECREATIONAL";
	private static final String AVAILABLE_DAYS_CN = "AVAILABLE_DAYS";
	private static final String FROM_CN = "FROM";
	private static final String TO_CN = "TO";
	
	private AbstractAbsenceRequestLogic requestLogic;
	
	/**
	 * Konstruktor
	 */
	public AbsenceRequestLogic() 
	{
		// ustalenie typu logiki dokumentu, lokalna lub zdalna
		requestLogic = AbstractAbsenceRequestLogic.getNewInstance();
	}
	
	/**
	 * Lista dni wolnych 
	 */
	protected List<FreeDay> freeDays;
	
	/**
	 * Lista urlop�w pracownika
	 */
	protected List<EmployeeAbsenceEntry> emplAbsences;
	
	protected EmployeeCard employeeCard;

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		if(!(document instanceof OfficeDocument))
			throw new EdmException("Nie mo�na doda� Wniosku w archiwum");
		Integer kind = (Integer) document.getFieldsManager().getKey("KIND");
		//if(kind.equals(20) && DSApi.context().hasPermission(DSPermission.AR_MENAGMENT))
		{
	//		throw new EdmException("Nie masz uprawnie� do wystawiania wniosku o urlop na ��danie.");
		}
			
		UserImpl author =  (UserImpl)DSUser.findByUsername(document.getAuthor());
		List<UserImpl> supervisors = author.getSupervisorsHierarchy();
		if (supervisors == null || supervisors.size() == 0)
		{
			throw new EdmException("Brak prze�o�onego. Prosz� zdefiniowa� prze�o�onego a nast�pnie spr�bowa� ponownie");
		}
		
		//validateData(document);
		
		String user = author.asLastnameFirstname();//""+document.getFieldsManager().getValue("USER");
		root().subfolder("Dokumenty pracownicze").subfolder("Wnioski o urlop")
		.subfolder(FolderInserter.to2Letters(user)).subfolder(user).insert(document);

		document.setTitle("Wniosek o urlop");
		((OfficeDocument) document).setSummaryWithoutHistory("Wniosek urlopowy : " + document.getFieldsManager().getValue("STATUS") + "");
		document.setDescription(document.getFieldsManager().getValue("STATUS") + "");
	}
	
	
	
	
//	/**
//	 * Sprawdzenie poprawno�ci danych
//	 * 
//	 * @param document
//	 * @throws EdmException
//	 */
//	private void validateData(Document document) throws EdmException
//	{
//		try
//		{
//			String s= null;
//			s.length();
//		}
//		catch (Exception e) {
//			e.printStackTrace();
//		}
//		FieldsManager fm = document.getFieldsManager();
//		loadEmployeeCard(requestLogic.getDocumentAuthor(fm.getField("USER").getEnumItem((Integer)fm.getKey("USER")).getCn()));
//		 
//		// sprawdzenie typu wniosku urlopu
//		Integer kindValue = (Integer) fm.getKey(KIND_CN);
//		
//		// SPRAWDZENIE CZY PRZYPADKIEM NIE ISTNIEJE JU� URLOP W PODANYM PRZEDZIALE DAT
//		Date startDate = (Date) fm.getValue(FROM_CN);
//		Date endDate = (Date) fm.getValue(TO_CN);
//		if (requestLogic.hasAbsenceBetween(employeeCard, startDate, endDate))
//		{
//			throw new EdmException("Ju� istnieje wpis urlopowy w podanym zakresie dat!");
//		}
//
//		if (kindValue.equals(20) /*URLOP NA ��DANIE*/)
//		{
//			// SPRAWDZENIE CZY TYP URLOPU JEST 'U�' I NIE PRZEKRACZA 4 DNI URLOPOWYCH W DANYM ROKU KALENDARZOWYM
//			Long uzSum = requestLogic.getAbsenceSumDaysFromCurrentYear(employeeCard, "U�");
//			if (uzSum == null) uzSum = 0l;
//			Integer numDays = (Integer) fm.getValue("WORKING_DAYS");
//			uzSum += numDays;
//			if (uzSum > 4)
//				throw new EdmException("Nie mo�na wzi�� wi�cej ni� 4 dni urlopowe na ��danie w roku kalendarzowym!");
//		}
//		
//		// SPRAWDZENIE CZY DATA POCZATKOWA URLOPU NIE JEST PRZED DATA ZATRUDNIENIA
//		if (startDate.before(employeeCard.getEmploymentStartDate()))
//			throw new EdmException("Nie mo�na wzi�� urlopu przed dat� zatrudnienia wpisanej w karcie pracownika!");
//	}
	
	
	public boolean hasAbsenceProcess(Long userId,Date from, Date to)
	{
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try
    	{ 

    		ps = DSApi.context().prepareStatement("select count(document_id) from dsg_absence_request where DSUSER = ? and status <> 40" +
    				"((PERIOD_FROM <= ?  and PERIOD_TO >= ?)"+ //start
    				" or " +
    				" (PERIOD_FROM <= ? and PERIOD_TO >= ?)" +
    				" or " +
    				" (PERIOD_FROM >= ? and PERIOD_TO <= ?))"); //stop
    		ps.setLong(1, userId);
    		ps.setDate(2, new java.sql.Date(from.getTime()));
    		ps.setDate(3, new java.sql.Date(from.getTime()));
    		ps.setDate(4, new java.sql.Date(to.getTime()));
    		ps.setDate(5, new java.sql.Date(to.getTime()));
    		
    		ps.setDate(6, new java.sql.Date(from.getTime()));
    		ps.setDate(7, new java.sql.Date(to.getTime()));
    		rs = ps.executeQuery();
    		Integer count =0;
    		if(rs.next())
    			count = rs.getInt(1);
    		return count > 0;
    		
    	}
    	catch (Exception e)
    	{
			LOG.error("",e);
		}
    	finally
    	{
    		DbUtils.closeQuietly(ps);
    	}
    	return false;
	}
	
	
	public void onStartProcess(OfficeDocument document) throws EdmException
	{
		Integer kind = (Integer) document.getFieldsManager().getKey("KIND");
		if(kind.equals(20) && DSApi.context().hasPermission(DSPermission.AR_MENAGMENT))
		{
			throw new EdmException("Nie masz uprawnie� do wystawiania wniosku o urlop na ��danie.");
		}
		// SPRAWDZENIE CZY PRZYPADKIEM NIE ISTNIEJE JU� URLOP W PODANYM PRZEDZIALE DAT
		FieldsManager fm = document.getFieldsManager();
		Date startDate = (Date) fm.getValue(FROM_CN);
		Date endDate = (Date) fm.getValue(TO_CN);
		Integer hasAbsence = (Integer) DSApi.context().session().createQuery("select 1 from " + Absence.class.getName() 
				+ " a where a.employeeCard = :empCard and a.absenceType.code not like :WU and " 
				+ "((a.startDate <= :startDate  and a.endDate >= :startDate) "
				+ "or (a.startDate <= :endDate and a.endDate >= :endDate))")
				.setString("WU", "WU%")
				.setParameter("empCard", getEmployeeCard(document.getFieldsManager()))
				.setDate("startDate", startDate)
				.setDate("endDate", endDate)
				.setMaxResults(1)
				.uniqueResult();
		if (hasAbsence != null)
			throw new EdmException("Ju� istnieje wpis urlopowy w podanym zakresie dat!");
		
		employeeCard = getEmployeeCard(document.getFieldsManager());
		if(hasAbsenceProcess(employeeCard.getUser().getId(), startDate, endDate))
		{
			throw new EdmException("Ju� istnieje wniosek urlopowy w podanym zakresie dat!");
		} 
		
		
		LOG.info("START PROCESS !!!");
        try {
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
	}
	
	public void documentPermissions(Document document) throws EdmException 
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		
		perms.add(new PermissionBean(ObjectPermission.READ,DocumentLogicLoader.ABSENCE_REQUEST+"_READ",ObjectPermission.GROUP,"Dokumenty - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,DocumentLogicLoader.ABSENCE_REQUEST+"_ATT_READ",ObjectPermission.GROUP,"Dokumenty zalacznik - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY,DocumentLogicLoader.ABSENCE_REQUEST+"_MODIFY",ObjectPermission.GROUP,"Dokumenty - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,DocumentLogicLoader.ABSENCE_REQUEST+"_ATT_MODIFY",ObjectPermission.GROUP,"Dokumenty zalacznik - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.DELETE,DocumentLogicLoader.ABSENCE_REQUEST+"_DELETE",ObjectPermission.GROUP,"Dokumenty - usuwanie"));
   	 	this.setUpPermission(document, perms);
	}
	
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
        Map<String,Object> values = new HashMap<String,Object>();
        values.put(KIND_CN, fm.getField(KIND_CN).getEnumItemByCn(RECREATIONAL_CN).getId());
        fm.reloadValues(values);
    }
	
	public void onLoadData(FieldsManager fm) throws EdmException 
	{
		employeeCard = getEmployeeCard(fm);
		// wyliczenie dostepnych dni
		int sumDays = 0;
		if (employeeCard != null)
		{
			try
			{
				List<Absence> absences = DSApi.context().session().createQuery("from " + Absence.class.getName() + " a "
						+ "where a.year = ? and a.employeeCard = ?")
						.setInteger(0, GregorianCalendar.getInstance().get(GregorianCalendar.YEAR))
						.setParameter(1, employeeCard)
						.list(); 
				
				for (Absence absence : absences)
				{
					if (absence.getAbsenceType().isAdditional())
					{
						sumDays += absence.getDaysNum();
					}
					if (absence.getAbsenceType().isNegative())
					{
						sumDays -= absence.getDaysNum();
					}
				}
			}
			catch (Exception e) {
				LOG.error(e.getMessage(),e);
			}
		}
		
		Map<String,Object> values = new HashMap<String,Object>();
		values.put(AVAILABLE_DAYS_CN, sumDays);
		fm.reloadValues(values);
		freeDays = AbsenceFactory.getFreeDays();
		int currentYear = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
		emplAbsences = AbsenceFactory.getEmployeeAbsences(employeeCard, currentYear);
	}
	
	/**
	 * �aduje kart� pracownika
	 * 
	 * @param userImpl
	 * @return
	 * @throws Exception
	 */
	private void loadEmployeeCard(UserImpl userImpl) throws EdmException
	{
		employeeCard = AbsenceFactory.getActiveEmployeeCardByUser(userImpl);
		
		if (employeeCard == null)
			throw new EdmException("Brak aktualnej karty pracownika!");
	}
	
	public List<FreeDay> getFreeDays()
	{
		return freeDays;
	}
	
	/**
	 * Zwraca list� urlop�w pracownika w bierzacym roku, dla widoku w dockind-specific-additions.jsp
	 * 
	 * @return
	 */
	public List<EmployeeAbsenceEntry> getEmplAbsences() 
	{
		return emplAbsences;
	}
	
	/**
	 * Zwraca kart� pracownika
	 * 
	 * @return
	 */
	public EmployeeCard getEmployeeCard()
	{
		return employeeCard;
	}
	
	@Override
	public TaskListParams getTaskListParams(DocumentKind dockind, long id)throws EdmException
	{
    	TaskListParams params = new TaskListParams();
    	try
    	{
			FieldsManager fm = dockind.getFieldsManager(id);
			params.setCategory((String) fm.getValue("KIND"));
			params.setStatus((String) fm.getValue("STATUS"));
			params.setAttribute((String) fm.getValue("USER"), 0);
			params.setAttribute((String)fm.getValue("SUBSTITUTION"), 1);
    	}
    	catch (Exception e) 
    	{
			e.printStackTrace();
		}
		return params;
	}
	
	private EmployeeCard getEmployeeCard(FieldsManager fm) throws EdmException
	{
		String author = null;
		//if(fm.get)
		if (fm.getDocumentId() != null)
			author = Document.find(fm.getDocumentId()).getAuthor();
		// pobranie obiektu u�ytkownika zglaszaj�cego wniosek
		UserImpl user;
		if (author != null && !author.equals(""))
			user = (UserImpl) DSUser.findByUsername(author);
		else
			user = (UserImpl) DSApi.context().getDSUser();
		
		EmployeeCard employeeCard = (EmployeeCard) DSApi.context().session().createQuery(
				"from " + EmployeeCard.class.getName() + " em where em.user = ?")
				.setParameter(0, user)
				.setMaxResults(1)
				.uniqueResult();
		
		return employeeCard;
	}
}
