package pl.compan.docusafe.parametrization.beck;

import java.util.List;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class NextSupervisorDecisionHandler implements DecisionHandler
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(NextSupervisorDecisionHandler.class);
	
	private static final String SUPERVISOR_ACCEPTANCE_CN = "S_A_CN";
	
	private static final String TO_DIVISION = "to-division";
	
	private static final String NEXT_SUPERVISOR = "next-supervisor";
	
	
	public String decide(OpenExecution openExecution) 
	{
		Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		
		try
		{
			LOG.info("doc id = " + docId);
			Document document = Document.find(docId);
			
			UserImpl author = (UserImpl) DSUser.findByUsername(Document.find(docId).getAuthor());
			List<UserImpl> supervisors = author.getSupervisorsHierarchy();
			
//			for (UserImpl u : supervisors)
//				System.out.println(u.getName());
			
			if (supervisors == null || supervisors.size() == 0)
			{
				LOG.info("Nie zdefiniowana �adnego prze�o�onego dla u�ytkownika o nazwie: " + author.getName());
				throw new IllegalStateException("Brak zdefiniowanych prze�o�onych");
			}
			
			String retValue;
			
			DocumentAcceptance acceptance = DocumentAcceptance.find(docId, SUPERVISOR_ACCEPTANCE_CN, null);
			
			if (supervisors.size() == 1)
			{
				LOG.info("notify -> supervisors.size() == 1");
				
				// jest tylko jeden prze�o�ony, utworzenie wpisu DocumentAcceptance
				DocumentAcceptance.createAndSave(document, supervisors.get(0).getName(), SUPERVISOR_ACCEPTANCE_CN, null);
				
				// przej�cie do stanu: personal-division
				retValue = TO_DIVISION;
			}
			else if (acceptance == null)
			{
				LOG.info("notify -> acceptance == null");
				
				// zaakceptowa� pierwszy prze�o�ony, utworzenie nowego wpisu w DocumentAcceptance
				DocumentAcceptance.createAndSave(document, supervisors.get(0).getName(), SUPERVISOR_ACCEPTANCE_CN, null);
				
				// powr�t do stanu: supervisor-decision
				retValue = NEXT_SUPERVISOR;
			}
			else 
			{
				LOG.info("notify -> acceptance != null");
				
				// istnieje ju� wpis w DocumentAcceptance, ustalenie, kt�ry teraz zaakceptowal
				boolean isLast = false;
				String nextSupervisorName = null;
				for (int i = 0; i < supervisors.size(); i++)
				{
					if (supervisors.get(i).getName().equals(acceptance.getUsername()) 
							&& i + 1 < supervisors.size()) 
					{
						nextSupervisorName = supervisors.get(i + 1).getName();
						if (i + 2 == supervisors.size())
							isLast = true;
					}
				}
				
				if (nextSupervisorName == null)
					// usuniecie bledu zwiazanego z modyfikacja przelozonych po wyslaniu wniosku ale przed jego akceptacja
					nextSupervisorName = supervisors.get(0).getName();
				
				LOG.info("notify enter: " + nextSupervisorName);
				
				//System.out.println("SUP NAME: " + nextSupervisorName);
				// zaaktualizowanie wpisu DocumentAcceptance
				acceptance.setUsername(nextSupervisorName);
				DSApi.context().session().update(acceptance);

				
				if (isLast )
					// to by�a ostatnia akceptacja, przej�cie do dzia�u personalnego
					retValue = TO_DIVISION;
				else
					// to nie ostatni przelozony, przejscie do: supervisor-decision
					retValue = NEXT_SUPERVISOR;
			}
			
			return retValue;
		}
		catch (Exception ex)
		{
			LOG.error(ex.getMessage(), ex);
		}
		
		return null;
	}
}
