package pl.compan.docusafe.parametrization.beck;

import java.util.List;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class AssignToSupervisorTask implements AssignmentHandler 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AssignToSupervisorTask.class);
	
	private static final String SUPERVISOR_ACCEPTANCE_CN = "S_A_CN";
	
	public void assign(org.jbpm.api.task.Assignable assignable,
			OpenExecution execution) throws Exception 
	{
		Long docId = Long.parseLong(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		LOG.info("doc id = " + docId);
		
		// pobranie obiektu dokumentu
		Document document = Document.find(docId);
		
		// pobranie usera i jego przełożonych
		UserImpl author = (UserImpl) DSUser.findByUsername(document.getAuthor());
		List<UserImpl> supervisors = author.getSupervisorsHierarchy();
		if (supervisors == null || supervisors.size() == 0)
		{
			LOG.info("Nie zdefiniowana żadnego przełożonego dla użytkownika o nazwie: " + author.getName());
			throw new IllegalStateException("Brak zdefiniowanych przełożonych");
		}
		
		UserImpl supervisor = null;
		
		// sprawdzenie czy istnieje wpis w DocumentAcceptance
		DocumentAcceptance acceptance = DocumentAcceptance.find(docId, SUPERVISOR_ACCEPTANCE_CN, null);
		if (acceptance != null)
		{
			LOG.info("acceptance != null");
			// jeśli istnieje to pobranie kolejnego przełożonego
			String nextSupervisorName = null;
			for (int i = 0; i < supervisors.size(); i++)
			{
				if (supervisors.get(i).getName().equals(acceptance.getUsername()) 
						&& i + 1 < supervisors.size()) 
				{
					nextSupervisorName = supervisors.get(i + 1).getName();
				}
			}
			
			LOG.info("acceptance != null: " + nextSupervisorName);
			
			if (nextSupervisorName == null)
			{
				LOG.info("Brak zdefiniowanych wiecej przelozonych");
				throw new IllegalStateException("Brak zdefiniowanych przełożonych");
			}
			supervisor = (UserImpl) DSUser.findByUsername(nextSupervisorName);
		}
		else
		{
			// brak wpisu w DocuemntAcceptance, pobranie pierwszego przełożonego
			supervisor = supervisors.iterator().next();
		}
		
		LOG.info("Dekretacja na: " + supervisor.getName());
		assignable.addCandidateUser(supervisor.getName());
		
		execution.setVariable(Jbpm4Constants.REASSIGNMENT_PROPERTY, true);
	}
}
