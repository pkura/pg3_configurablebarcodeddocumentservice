package pl.compan.docusafe.parametrization.beck;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Collections;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class FillSubmitterDataListener extends AbstractEventListener {
    private final static Logger LOG = LoggerFactory.getLogger(FillSubmitterDataListener.class);

    public void notify(EventListenerExecution eventListenerExecution) throws Exception {
        Long docId = Long.valueOf(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);
        EnumItem ei = doc.getFieldsManager().getField("USER").getEnumItemByCn(doc.getAuthor());
        doc.getDocumentKind().setOnly(docId,
            Collections.singletonMap("USER", ei.getId()));
        doc.getDocumentKind().setOnly(docId,
            Collections.singletonMap("POSITION", DSApi.context().getDSUser().getAllDivisions()[0].getName()));
    }
}
