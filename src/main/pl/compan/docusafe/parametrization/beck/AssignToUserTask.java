package pl.compan.docusafe.parametrization.beck;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class AssignToUserTask implements AssignmentHandler 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AssignToUserTask.class);
	
	/**
	 * Nazwa pola, z kt�rego zostanie pobrana nazwa u�ytkownika do przypisania
	 */
	private String assignee;

	public void assign(Assignable assignable, OpenExecution execution)
			throws Exception 
	{
		Long docId = Long.parseLong(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY)+"");
		LOG.info("Document ID: " + docId);
		if (assignee == null || assignee.equals("")) {
			throw new IllegalStateException("Musi by� wype�niony w definicji procesu atrybut: assigne");
		}
		// assignee to user
		String username = Document.find(docId).getFieldsManager().getEnumItemCn(assignee);
		
		LOG.info("Dekretacja na: " + username);
		assignable.addCandidateUser(username);
		
		execution.setVariable(Jbpm4Constants.REASSIGNMENT_PROPERTY, true);
	}
}
