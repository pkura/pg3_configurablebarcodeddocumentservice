package pl.compan.docusafe.parametrization.ilpoldwr;


import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import oracle.sql.TIMESTAMP;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.EmailConstants;
import org.directwebremoting.WebContextFactory;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.DictionaryUtils;
import pl.compan.docusafe.core.dockinds.dwr.*;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.attachments.DwrAttachmentStorage;
import pl.compan.docusafe.core.dockinds.dwr.attachments.DwrFileItem;
import pl.compan.docusafe.core.dockinds.field.*;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.mail.DSEmailChannel;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.sql.AcceptanceDivisionImpl;
import pl.compan.docusafe.parametrization.wssk.LogicExtension_Reopen;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.imports.ImpulsImportUtils;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.service.upload.UploadDriver;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class IlpolCokImportedLogic extends AbstractDocumentLogic implements LogicExtension_Reopen{

    private final static Logger LOG = LoggerFactory.getLogger(IlpolCokImportedLogic.class);
    private final static StringManager SM = GlobalPreferences.loadPropertiesFile(IlpolCokImportedLogic.class.getPackage().getName(), null);

    public static final String DOCKIND_NAME = "cok_imported_doc";
    public static final String DOCKIND_TABLE = "DSC_ILPOL_COK_IMPORTED_DOC";

    /*
        WLASCIWOSCI DO UWSPOLNIENIA
    */
    private final static String DWR_SEND_BUTTON = "DWR_SEND_BUTTON";
    private final static String DWR_SEND_FORWARD_BUTTON = "DWR_SEND_FORWARD_BUTTON";
    private final static String DWR_RESPONSE_BUTTON = "DWR_RESPONSE_BUTTON";
    private final static String DWR_FORWARD_RESPONSE_BUTTON = "DWR_FORWARD_RESPONSE_BUTTON";
    private final static String ORIGINAL_MSG = "<br /><br />- - - Oryginalna wiadomo�� - - -<br /><br />";
    private final static String DWR_CREATE_CASE_SHOW_POPUP_BUTTON = "DWR_CREATE_CASE_SHOW_POPUP_BUTTON";
    private final static String DWR_CREATE_TASK_SHOW_POPUP_BUTTON = "DWR_CREATE_TASK_SHOW_POPUP_BUTTON";
    private static final String TASK_DICTIONARY_FIELD_CN = "TASK_DOCUMENT";

    private Object assignee;

    @Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        Field field = super.validateDwr(values, fm);

        Map.Entry<String, FieldData> senderField = DwrUtils.getSenderField(values);

        if(senderField != null){
            if(senderField.getKey().equals(DWR_RESPONSE_BUTTON)){
                if(!isLoggedUserFromCokGroup()){
                    throw new EdmException("Funkcjonalno�� dost�pna wy��cznie dla pracownik�w COK");
                }
                FieldData contractorData = values.get("DWR_CONTRACTOR");
                if(contractorData == null || contractorData.getDictionaryData().get("CONTRACTOR_NIP").getStringData().isEmpty() || contractorData.getDictionaryData().get("CONTRACTOR_NAME").getStringData().isEmpty()){
                    throw new EdmException("Nie znaleziono danych kontrahenta");
                }else{
                    String toEmail = values.get("DWR_CONTRACTOR").getDictionaryData().get("CONTRACTOR_EMAIL").getStringData();
                    if(!toEmail.isEmpty()){
                        values.put("DWR_RESPONSE_RECEIVER", new FieldData(Field.Type.STRING, toEmail));
                    }
                }
            }else   if(senderField.getKey().equals(DWR_SEND_BUTTON)) {
                // KLIKNI�TO ODPOWIEDZ
                return sendMail(DWR_SEND_BUTTON, values, fm);
            }else   if(senderField != null && senderField.getKey().equals(Field.DWR_PREFIX + ClientContactHistoryDictionary.DICTIONARY_NAME)) {
                // zmodyfikowano slownik historii kontaktow z klientem
//                return synchronizeClientContactHistoryDictionaryModelWithView((Map)WebContextFactory.get().getSession().getAttribute(DwrFacade.DWR_SESSION_NAME), fm, values.get(Field.DWR_PREFIX + ClientContactHistoryDictionary.DICTIONARY_NAME).getDictionaryData());
            }else if(senderField != null && senderField.getKey().equals(DWR_CREATE_TASK_SHOW_POPUP_BUTTON)) {
                // otworzono popup do tworzenia zadania
                CokUtils.copyAreaValues(values, "DWR_NEW_TASK_");
                DwrUtils.setCurrentDate(values, "DWR_NEW_TASK_CREATION_DATETIME");
                String contractorId = values.get("DWR_CONTRACTOR").getDictionaryData().get("id").getStringData();
                if(StringUtils.isNotEmpty(contractorId)) {
                    values.get("DWR_NEW_TASK_CONTRACTOR").getDictionaryData().put("id", values.get("DWR_CONTRACTOR").getDictionaryData().get("id"));
                }
            }else   if(senderField != null && senderField.getKey().equals("DWR_CREATE_NEW_TASK_BUTTON")){
                // klikni�to Stw�rz zadanie
                InOfficeDocument taskDoc = null;
                Document importedDoc = null;
                long importedDocId = fm.getDocumentId();
                String recipient = null;
                try{
                    taskDoc = (InOfficeDocument) CokUtils.createDocumentFromPopup(fm, values, CokUtils.COK_TASK);
                    recipient = taskDoc.getRecipientUser();
                    DSApi.open(AuthUtil.getSubject(WebContextFactory.get().getHttpServletRequest()));
                    DSApi.context().begin();
                    Document doc = Document.find(fm.getDocumentId());
                    DictionaryUtils.addToMultipleTable(doc, TASK_DICTIONARY_FIELD_CN, taskDoc.getId().toString());
                    DataMartManager.addHistoryEntry(importedDocId, Audit.create(DataMartDefs.DOCUMENT_CREATE, DSApi.context().getPrincipalName(), SM.getString("UtworzonoZadanieICS")));
                    DSApi.context().commit();
                    DSApi._close();
                    LoggerFactory.getLogger("TESTING_PHASE").info("Utworzono nowe zadanie dekretuj�c na u�ytkownika " + recipient);
                    addCreatedEntryToDictionaryView(values, TASK_DICTIONARY_FIELD_CN, taskDoc.getId());
//                    synchronizeTaskDictionaryModelWithView((Map)WebContextFactory.get().getSession().getAttribute(DwrFacade.DWR_SESSION_NAME), values, fm, true);
                }catch(EdmException e){
                    DSApi.context()._rollback();
                    return new Field("Wyst�pi� b��d w trakcie tworzenia zadania", e.getMessage() + "\n\n" + e.getStackTrace());
                }
                return new Field("cn", "Utworzono nowe zadanie dekretuj�c na u�ytkownika " + recipient, Field.Type.STRING);
            }else   if(senderField.getKey().equals("DWR_CREATE_CASE_SHOW_POPUP_BUTTON")) {
                // otwarto popup do tworzenia sprawy
                CokUtils.copyAreaValues(values, "DWR_NEW_CASE_");
                CokUtils.initCurrentDateForDWRField(values, "DWR_NEW_CASE_CREATION_DATETIME");
                CokUtils.initStatusForDWRField(values, "DWR_NEW_CASE_STATUS");

                String contractorId = values.get("DWR_CONTRACTOR").getDictionaryData().get("id").getStringData();
                if(StringUtils.isNotEmpty(contractorId)) {
                    values.get("DWR_NEW_CASE_CONTRACTOR").getDictionaryData().put("id", values.get("DWR_CONTRACTOR").getDictionaryData().get("id"));
                }
            }else   if(senderField.getKey().equals("DWR_NEW_CASE_GENERATE_INTERNAL_ID_BUTTON")) {
                // klikni�to generuj numer sprawy na popupie do tworzenia sprawy
                String selectedAreaId = DwrUtils.getStringValue(values, "DWR_NEW_CASE_AREA");
                if (StringUtils.isEmpty(selectedAreaId)) {
                    return new Field(SM.getString("SelectAreaToGenerateCaseNumber"), "MSG");
                } else {
                    int areaId = Integer.parseInt(selectedAreaId);
                    try {
                        String caseString = DwrUtils.getStringValue(values, "DWR_NEW_CASE_INTERNAL_ID");
                        CaseNumber caseNumber;
                        try {
                            caseNumber = new CaseNumber(caseString);
                        } catch (IllegalArgumentException e) {
                            DSApi.openAdmin();
                            caseNumber = CokUtils.generateCaseNumber(areaId);
                            DSApi._close();
                        }
                        values.get("DWR_NEW_CASE_INTERNAL_ID").setStringData(caseNumber.toString());
                    } catch (SQLException e) {
                        LOG.error(e.getMessage(), e);
                    } catch (EdmException e) {
                        LOG.error(e.getMessage(), e);
                    }
                }
            }
        }
        return field;
    }

    private Field sendMail(String actionButtonType, Map<String, FieldData> values, FieldsManager fm) throws EntityNotFoundException, EdmException{
        StringBuilder sb = new StringBuilder("Wys�ano wiadomo�� do ");
        String subject;
        String toEmail;
        String [] multipleEmailAddresses = null;
        Field msg = new Field("cn", sb.toString(), Field.Type.STRING);
        msg.setKind(Field.Kind.MESSAGE);
        String messageReceipt = null;
        DSEmailChannel emailChannel = null;
        boolean contextOpened = false;
        // tworzymy tymczasowe pliki z zwartoscia zalacznikow dodanych z poziomu formatki
        List<File> attachedFiles = prepareAttachments(values, fm);
        if(actionButtonType.equals(DWR_SEND_BUTTON)){
            try {
                contextOpened = DSApi.openContextIfNeeded();
                String messageResponse = DwrUtils.getStringValue(values, "DWR_RESPONSE_CONTENT");
                FieldData contractorData = values.get("DWR_CONTRACTOR");
                if(contractorData == null || contractorData.getDictionaryData().get("CONTRACTOR_NIP").getStringData().isEmpty() || contractorData.getDictionaryData().get("CONTRACTOR_NAME").getStringData().isEmpty()){
                    throw new EdmException("Nie znaleziono danych kontrahenta");
                }else{
                    toEmail = String.valueOf(values.get("DWR_RESPONSE_RECEIVER"));
                    if(toEmail.isEmpty()){
                        throw new EdmException("Nie znaleziono adresu e-mail klienta");
                    }else if(toEmail.contains(";")){
                        //moze wystepowac wielu adresatow oddzielonych znakiem srednika
                        multipleEmailAddresses = toEmail.split("\\s*;\\s*");
                    }
                }
                subject = values.get("DWR_RESPONSE_SUBJECT").getStringData();
                messageReceipt = DwrUtils.getStringValue(values, "DWR_RESPONSE_CONTENT");
                if(multipleEmailAddresses != null){
                    for(int i = 0; i < multipleEmailAddresses.length; i++){
                        InternetAddress addr = new InternetAddress(multipleEmailAddresses[i]);
                        multipleEmailAddresses[i] = addr.getAddress();
                        sb.append(StringEscapeUtils.escapeHtml(multipleEmailAddresses[i]) + "; ");
                    }
                    sb.deleteCharAt(sb.toString().length()-1);
                    sb.deleteCharAt(sb.toString().length()-1);
                }else{
                    sb.append(StringEscapeUtils.escapeHtml(toEmail));
                }

                msg.setLabel(sb.toString());
                String channelId = Docusafe.getAdditionPropertyOrDefault("monitoringPlatnosciEmailChannelId", "7").trim();
                try{
                    emailChannel = DSEmailChannel.findById(Long.valueOf(channelId));
                }catch(NumberFormatException e){
                    emailChannel = DSEmailChannel.findById(7);
                }
                if(emailChannel != null){
                    if(multipleEmailAddresses == null){
                        LoggerFactory.getLogger("MONITORING_PLATNOSCI").info("multipleEmailAddresses == null");
                        LoggerFactory.getLogger("MONITORING_PLATNOSCI").info(toEmail + " | " + toEmail + " | " + subject  + " | " +  messageReceipt  + " | " +  false  + " | " +  EmailConstants.TEXT_HTML  + " | " +  emailChannel  + " | " +  null);
                        ((Mailer) ServiceManager.getService(Mailer.NAME)).send(toEmail, toEmail, subject, messageReceipt, false, EmailConstants.TEXT_HTML, emailChannel, attachedFiles);
                    }else{
                        LoggerFactory.getLogger("MONITORING_PLATNOSCI").info("multipleEmailAddresses != null");
                        LoggerFactory.getLogger("MONITORING_PLATNOSCI").info(toEmail + " | " + toEmail + " | " + subject  + " | " +  messageReceipt  + " | " +  false  + " | " +  EmailConstants.TEXT_HTML  + " | " +  emailChannel  + " | " +  null);
                        for(String emailAddress : multipleEmailAddresses){
                            LoggerFactory.getLogger("MONITORING_PLATNOSCI").info("emailAddress: " + emailAddress);
                            ((Mailer) ServiceManager.getService(Mailer.NAME)).send(emailAddress, emailAddress, subject, messageReceipt, false, EmailConstants.TEXT_HTML, emailChannel, attachedFiles);
                        }
                    }
                }else{
                    throw new EdmException("Nie znaleziono adresu e-mail nadawcy");
                }

                /*
                if(getStoredDWRAttachments().size() > 0){
                    if(!DSApi.isContextOpen())
                        DSApi.open(AuthUtil.getSubject(WebContextFactory.get().getHttpServletRequest()));
                    DSApi.context().begin();
                    Map<String, Object> fieldValues = new HashMap<String, Object>();
                    fieldValues.put("EMAIL_ATTACHMENT", getStoredDWRAttachments());
                    fm.getDocumentKind().setWithHistory(fm.getDocumentId(), fieldValues, false);
                    DSApi.context().commit();
                    DSApi._close();
                }
                */

                long newEntryId = addEntryToClientContactHistory(values, fm, messageResponse);
                addCreatedEntryToDictionaryView(values, TASK_DICTIONARY_FIELD_CN, newEntryId);
            } catch(AddressException e) {
                LOG.error(e.getMessage(), e);
                msg.setLabel("Nieprawid�owy format adresu e-mail: " + e.getMessage());
            } catch (EdmException e) {
                LOG.error(e.getMessage(), e);
                msg.setLabel("Wyst�pi� b��d: " + e.getMessage());
            } catch (SQLException e) {
                msg.setLabel("Nie powiod�o si� dodanie wpisu do historii kontakt�w z klientem: " + e.getMessage());
                if(DSApi.isContextOpen() && DSApi.context().inTransaction()){
                    DSApi.context()._rollback();
                }
            }finally{
                if(DSApi.isContextOpen()){
                    DSApi.closeContextIfNeeded(contextOpened);
                }
            }
        }
        removeTemporaryFiles(attachedFiles);
        return msg;
    }

    private boolean isLoggedUserFromCokGroup() throws EdmException {
        boolean hasPermission = false;

        if(!DSApi.isContextOpen()){
            DSApi.openAdmin();
        }
        String loggedUserLogin = CokUtils.retriveUserLogin();
        DSUser user = DSUser.findByUsername(loggedUserLogin);
        String cokGUID = Docusafe.getAdditionProperty("cok.guid");

        if(cokGUID != null && !cokGUID.equals(StringUtils.EMPTY)){
            for(String divisionGuid : user.getDivisionsGuid()){
                if(divisionGuid.equals(cokGUID)){
                    hasPermission = true;
                }
            }
        }

        if(DSApi.isContextOpen()){
            DSApi._close();
        }
        return hasPermission;
    }

    private List<DwrFileItem> getStoredDWRAttachments(){
        return DwrAttachmentStorage.getInstance().getFiles(WebContextFactory.get().getSession(false), "EMAIL_ATTACHMENT");
    }

    /**
     * Metoda dodaje wpis do tabeli slownika historii kontakt�w z klientem
     * @param values
     */
    private long addEntryToClientContactHistory(Map<String, FieldData> values, FieldsManager fm, String mailMessageContent) throws EdmException, SQLException {
        if(!DSApi.isContextOpen()){
            DSApi.open(AuthUtil.getSubject(WebContextFactory.get().getHttpServletRequest()));
        }
        DSApi.context().begin();

        EnumValues contactTypeData = new EnumValues();
        String selectedId = "";
        for(pl.compan.docusafe.core.dockinds.field.Field field : fm.getField(ClientContactHistoryDictionary.DICTIONARY_NAME).getFields()){
            if(field.getCn().contains(ClientContactHistoryDictionary.CONTACT_TYPE)){
                List<Map<String, String>> allOptions = new ArrayList<Map<String, String>>();
                Map<String, String> option;
                for(EnumItem item : ((DataBaseEnumField)field).getEnumItems()){
                    option = new HashMap<String, String>();
                    option.put(String.valueOf(item.getId()), String.valueOf(item.getTitle()));
                    allOptions.add(option);
                }
                contactTypeData.setAllOptions(allOptions);
                contactTypeData.setSelectedId(String.valueOf(((DataBaseEnumField)field).getEnumItem(ClientContactHistoryDictionary.CONTACT_TYPE_MAIL).getId()));
                selectedId = String.valueOf(((DataBaseEnumField)field).getEnumItem(ClientContactHistoryDictionary.CONTACT_TYPE_MAIL).getId());
            }
        }

        /*
            pole AUTHOR jest dodawane automatycznie w logice s�ownika
            pole REPAYMENT_DATE wypelniane jest wylacznie gdy statusem kontaktu jest wartosc "Deklaracja sp�aty"
            pole CONTACT_STATUS ma pozosta� puste
        */
        Map<String, FieldData> updatedDictionaryData = new HashMap<String, FieldData>();
        updatedDictionaryData.put(ClientContactHistoryDictionary.CREATION_DATE, new FieldData(Field.Type.TIMESTAMP, new Date(System.currentTimeMillis())));
        updatedDictionaryData.put(ClientContactHistoryDictionary.CONTACT_DATE, new FieldData(Field.Type.TIMESTAMP, new Date(System.currentTimeMillis())));
        updatedDictionaryData.put(ClientContactHistoryDictionary.CONTACT_TYPE, new FieldData(Field.Type.ENUM, selectedId));
        updatedDictionaryData.put(ClientContactHistoryDictionary.DESCRIPTION, new FieldData(Field.Type.STRING, mailMessageContent.replaceAll("</?\\w+>", "")));
        updatedDictionaryData.put(ClientContactHistoryDictionary.DOCUMENT_ID, new FieldData(Field.Type.LONG, fm.getDocumentId()));

        long createdEntryId = -1;
        //dodanie wpisu do tabeli slownika wielowartosciowego
        if(!((createdEntryId = DwrDictionaryFacade.getDwrDictionary(this.DOCKIND_NAME, ClientContactHistoryDictionary.DICTIONARY_NAME).add(updatedDictionaryData)) > 0)){
            throw new EdmException("Nie powiod�o si� dodanie wpisu do historii kontakt�w z klientem");
        }else{
            Document doc = Document.find(fm.getDocumentId()); //dodanie wpisu do tabeli ascocjacyjnej multiple wiazacej dokument ze wspisem tabeli z kontaktami z klientami
            DictionaryUtils.addToMultipleTable(doc, ClientContactHistoryDictionary.DICTIONARY_NAME, String.valueOf(createdEntryId));
//            DataMartManager.addHistoryEntry(importedDocId, Audit.create(DataMartDefs.DOCUMENT_CREATE, DSApi.context().getPrincipalName(), SM.getString("UtworzonoZadanieCOK")));
        }

        DSApi.context().commit();
        DSApi._close();
        return createdEntryId;
    }

    private List<File> prepareAttachments(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        List<DwrFileItem> mailAttachments = getStoredDWRAttachments("EMAIL_ATTACHMENT");
        File tempFile;
        List<File> attachedFiles = new ArrayList<File>();
        try {
            for(FileItem fileItem : mailAttachments){
                tempFile = new File(UploadDriver.tempDir + "" + fileItem.getSize() + "_ORGNAME_" + fileItem.getName());
                if(tempFile.exists()){
                    fileItem.write(tempFile);
                    attachedFiles.add(tempFile);
                }else if(tempFile.createNewFile()){
                    fileItem.write(tempFile);
                    attachedFiles.add(tempFile);
                }
            }
        } catch (Exception e) {
            LOG.error(e.getMessage());
            return null;
        }
        return attachedFiles;
    }

    private List<DwrFileItem> getStoredDWRAttachments(String attachmentFieldCn){
        if(attachmentFieldCn != null)
            return DwrAttachmentStorage.getInstance().getFiles(WebContextFactory.get().getSession(false), attachmentFieldCn);
        else
            return null;
    }

    private void removeTemporaryFiles(List<File> files){
        for(File file : files){
            file.delete();
        }
    }
    /*
        KONIEC DEFINICJI METOD ZWIAZANYCH Z PRZESLANIEM WIADOMOSCI DO DALSZYCH ODBIORCOW
    */

    @Override
    public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException {
        super.onSaveActions(kind, documentId, fieldValues);
    }


    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {
        //To change body of implemented methods use File | Settings | File Templates.
    }


    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
        super.onStartProcess(document);    //To change body of overridden methods use File | Settings | File Templates.

        try
        {
            Map<String, Object> map = Maps.newHashMap();
            if(event != null){
//				map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
//				map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
            }
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

            java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

            DSUser author = DSUser.findByUsername(document.getCreatingUser());
            String user = author.getName();
            String fullName = author.getLastname() + " " + author.getFirstname();

            perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

            Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
            perms.addAll(documentPermissions);
            this.setUpPermission(document, perms);

            DSApi.context().watch(URN.create(document));
        }
        catch (Exception e)
        {
            LOG.error(e.getMessage(), e);
            throw new EdmException(e.getMessage());
        }
    }

    public void onStartProcess(OfficeDocument document, ActionEvent event, Object assignee) throws EdmException {
        this.assignee = assignee;
        onStartProcess(document, null);
    }

    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
        if (acceptationCn != null) {
            try {
                DocumentLogic logic = doc.getDocumentKind().logic();
                Object assignee = ((IlpolCokImportedLogic)logic).getAssignee();
                String id = null;

                try {
                    if(assignee instanceof DSUser){
                        id = ((DSUser) assignee).getName();
                        assignable.addCandidateUser(id);
                        addToHistory(openExecution, doc, id, null);
                    }else if(assignee instanceof DSDivision){
                        id = ((DSDivision) assignee).getGuid();
                        assignable.addCandidateGroup(id);
                        addToHistory(openExecution, doc, null, id);
                    }else if(assignee instanceof AcceptanceDivisionImpl){
                        return false;
                    }else if(assignee == null){
                        return false;
                    }
                    LOG.info("For fieldValue " + id);
                } catch (UserNotFoundException e) {
                    LOG.error(e.getMessage());
                }
                return true;
            } catch (EdmException e1) {
                LOG.error(e1.getMessage());
            }
        }
        return false;
    }

    public static void addToHistory(OpenExecution openExecution, OfficeDocument doc, String user, String guid) throws EdmException, UserNotFoundException {
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setSourceUser(DSApi.context().getPrincipalName());
        ahe.setProcessName(openExecution.getProcessDefinitionId());
        if (user == null) {
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_DIVISION);
            ahe.setTargetGuid(DSDivision.find(guid).getName());
        } else {
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_SINGLE_USER);
            ahe.setTargetUser(user);
        }
        DSDivision[] divs = DSApi.context().getDSUser().getOriginalDivisionsWithoutGroup();
        if (divs != null && divs.length > 0)
            ahe.setSourceGuid(divs[0].getName());
        ahe.setCtime(new Date());
        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
        try {
            String status = doc.getFieldsManager().getEnumItem("STATUS").getTitle();
            ahe.setStatus(status);
        } catch (Exception e) {
            LOG.warn(e.getMessage());
        }
        doc.addAssignmentHistoryEntry(ahe);
    }


    /**
     * Metoda synchronizuje warto�� pola Zaleg�o�� Aktualna z widoku z zewn�trznej bazy danych
     */
    private String synchronizeCurrentArrearValue(FieldsManager fm) throws EdmException {
        if(fm.getValue("CONTRACTOR") != null && fm.getValue("CONTRACTOR") instanceof Map && ((Map)fm.getValue("CONTRACTOR")).get("CONTRACTOR_NIP") != null && !String.valueOf(((Map)fm.getValue("CONTRACTOR")).get("CONTRACTOR_NIP")).isEmpty()){
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                LoggerFactory.getLogger("MONITORING_PLATNOSCI").info("Synchronizacja aktualnej zaleg�o�ci kontrahenta...");
                if(ImpulsImportUtils.vkontrahenci  == null){
                    ImpulsImportUtils.initImport();
                }

                ps = DSApi.context().prepareStatement("SELECT ZALEGLOSC_AKT FROM " + ImpulsImportUtils.vkontrahenci + " WHERE NIP LIKE ?");
                ps.setString(1, String.valueOf(((Map)fm.getValue("CONTRACTOR")).get("CONTRACTOR_NIP")));
                rs =  ps.executeQuery();
                Double currentArrear = null;
                LoggerFactory.getLogger("MONITORING_PLATNOSCI").info("SELECT ZALEGLOSC_AKT FROM " + ImpulsImportUtils.vkontrahenci + " WHERE NIP LIKE '" + String.valueOf(((Map)fm.getValue("CONTRACTOR")).get("CONTRACTOR_NIP"))+"'");

                if(rs.next()){
                    currentArrear = rs.getDouble("ZALEGLOSC_AKT");
                }
                LoggerFactory.getLogger("MONITORING_PLATNOSCI").info("Bie��ca zaleg�o�� aktualna:" + String.valueOf(fm.getValue("CURRENT_ARREAR")));
                LoggerFactory.getLogger("MONITORING_PLATNOSCI").info("Znaleziona zaleg�o�� aktualna: " + currentArrear);
                rs.close();

                if(currentArrear == null){
                    LoggerFactory.getLogger("MONITORING_PLATNOSCI").info("Warto�� po synchronizacji: brak");
                    //brak informacji - nie znaleziono w widoku konrahenta o podanym nipie
                    return "brak";
                }else if(fm.getField("CURRENT_ARREAR") != null && !currentArrear.equals(fm.getValue("CURRENT_ARREAR"))){
                    Map<String, String> arrearCurrentValue = new HashMap<String, String>(1);
                    arrearCurrentValue.put("CURRENT_ARREAR", currentArrear.toString());
                    DocumentKind.findByCn(DOCKIND_NAME).setOnly(fm.getDocumentId(), arrearCurrentValue, false);

                }
                //aktualizacja - wartosc z widoku jest rozna od tej z naszego systemu
                LoggerFactory.getLogger("MONITORING_PLATNOSCI").info("Warto�� po synchronizacji: " + currentArrear + "\n");
                return String.valueOf(currentArrear);
            } catch (SQLException e) {
                LOG.error(e.getMessage(), e);
            } finally{
                DSApi.context().closeStatement(ps);
            }
        }
        LoggerFactory.getLogger("MONITORING_PLATNOSCI").info("Warto�� po synchronizacji: " + String.valueOf(fm.getValue("CURRENT_ARREAR")));
        //brak zmian - wartosc z widoku jest taka sama jak wartosc w naszym systemie
        return String.valueOf(fm.getValue("CURRENT_ARREAR"));
    }

    /*
	    cok_imported_doc.column.dockindBusinessAtr1 = Temat
        cok_imported_doc.column.dockindBusinessAtr2 = Kontrahent
        cok_imported_doc.column.dockindBusinessAtr3 = Status kontaktu
        cok_imported_doc.column.dockindBusinessAtr4 = Opis kontaktu
        cok_imported_doc.column.dockindBusinessAtr5 = Zaleglosc pierwotna
        cok_imported_doc.column.dockindBusinessAtr6 = Zaleglosc aktualna
	 * @see pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic#getTaskListParams(pl.compan.docusafe.core.dockinds.DocumentKind, long, pl.compan.docusafe.core.office.workflow.TaskSnapshot)
	 */
    @SuppressWarnings("unchecked")
    @Override
    public TaskListParams getTaskListParams(DocumentKind kind, long documentId, TaskSnapshot task) throws EdmException {
        TaskListParams params = new TaskListParams();
        String importSubject = "brak";
//        String importRemark = "brak";
        String clientContactDescription = "brak";
//        String importInformation = "brak";
        String clientContactStatus = "brak";
        String contractorName = "brak";
//        String contractorNip = "brak";
        String currentArrear = "brak";
        String originalArrear = "brak";

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {LoggerFactory.getLogger("MONITORING_PLATNOSCI").info("Wczytywanie atrybut�w biznesowych dla dokumentu: " + documentId);
            FieldsManager fm = kind.getFieldsManager(documentId);
//            Document document = Document.find(documentId);

            /*
            if(fm.getValue("STATUS") != null)
                caseStatus = fm.getValue("STATUS").toString();
            if(fm.getValue("INTERNAL_ID") != null)
                caseInternalNumber = fm.getValue("INTERNAL_ID").toString();
            if(fm.getValue("COK_DOCUMENT") != null && ((List)fm.getValue("COK_DOCUMENT")).size() > 0)
                caseSubject = ((Map)((List)fm.getValue("COK_DOCUMENT")).get(0)).get("COK_DOCUMENT_SUBJECT").toString();
            */

            LoggerFactory.getLogger("MONITORING_PLATNOSCI").info("\nPobieranie informacji z ostatniego kontaktu z klientem...");
            StringBuilder query = new StringBuilder("SELECT " + ClientContactHistoryDictionary.CONTACT_STATUS + ", " + ClientContactHistoryDictionary.DESCRIPTION + " FROM " +  ((DictionaryField)fm.getField("CLIENT_CONTACT_HISTORY")).getTable());
            query.append(" WHERE DOCUMENT_ID = ? AND CONTACT_DATE IS NOT NULL ");
            query.append(" ORDER BY " + ClientContactHistoryDictionary.CONTACT_DATE + " DESC");
            ps = DSApi.context().prepareStatement(query);
            ps.setLong(1, documentId);
            LoggerFactory.getLogger("MONITORING_PLATNOSCI").info(query.toString());
            rs =  ps.executeQuery();
            LoggerFactory.getLogger("MONITORING_PLATNOSCI").info("Aktualny opis: " + clientContactDescription);
            LoggerFactory.getLogger("MONITORING_PLATNOSCI").info("Aktualny status: " + clientContactStatus);
            if(rs.next()){
                if(rs.getString(ClientContactHistoryDictionary.DESCRIPTION) != null){
                    clientContactDescription = rs.getString(ClientContactHistoryDictionary.DESCRIPTION);
                }
                for(pl.compan.docusafe.core.dockinds.field.Field field : fm.getField(ClientContactHistoryDictionary.DICTIONARY_NAME).getFields()){
                    if(field.getCn().contains(ClientContactHistoryDictionary.CONTACT_STATUS) && rs.getString(ClientContactHistoryDictionary.CONTACT_STATUS) != null){
                        if(((DataBaseEnumField)field).getEnumItem(rs.getString(ClientContactHistoryDictionary.CONTACT_STATUS)).getTitle() != null){
                            clientContactStatus = ((DataBaseEnumField)field).getEnumItem(rs.getString(ClientContactHistoryDictionary.CONTACT_STATUS)).getTitle();
                        }
                    }
                }
            }
            rs.close();
            LoggerFactory.getLogger("MONITORING_PLATNOSCI").info("Nowy opis: " + clientContactDescription);
            LoggerFactory.getLogger("MONITORING_PLATNOSCI").info("Nowy status: " + clientContactStatus);
/*
            //zastapiony: Komentarz - j.w. b�dzie to opis kontaktu (historia kontakt�w), ostatni kontakt.
            if(fm.getValue("REMARK") != null){
                importRemark = fm.getValue("REMARK").toString();
            }
            //zast�piony: Informacja - to zostanie usuni�te w nowej formatce Monitoringu p�atno�ci, Ta funkcja zostanie zast�piona: Statusem Kontaktu.
            if(fm.getValue("INFORMATION") != null){
                importInformation = fm.getField("INFORMATION").getEnumItemByTitle((String)fm.getValue("INFORMATION")).getTitle();
            }
*/
            if(fm.getValue("SUBJECT") != null){
                importSubject = fm.getValue("SUBJECT").toString();
            }
            if(fm.getValue("CONTRACTOR") != null) {
                try {
                    Map<String, Object> contractorDic = (Map<String, Object>) fm.getValue("CONTRACTOR");
                    if(contractorDic.get("CONTRACTOR_NAME") != null) {
                        contractorName = contractorDic.get("CONTRACTOR_NAME").toString();
                    }
                   /*
                    if(contractorDic.get("CONTRACTOR_NIP") != null) {
                        contractorNip = contractorDic.get("CONTRACTOR_NIP").toString();
                    }
                    */
                } catch(ClassCastException e) {
                    LOG.error(e.getMessage(), e);
                }
            }
            if(fm.getValue("ORIGINAL_ARREAR") != null){
                //wartosc importowana z pliku
                originalArrear = fm.getValue("ORIGINAL_ARREAR").toString();
            }

            //pobieramy aktualna wartosc zaleglosci klienta z bazy zewnetrznej i aktualizujemy nia baze systemowa
            currentArrear = synchronizeCurrentArrearValue(fm);

        } catch(Exception e) {
            LOG.error(e.getMessage(), e);
        } finally {
            params.setAttribute(importSubject, 0);
            params.setAttribute(contractorName, 1);
//            params.setAttribute(contractorNip, 2);
//            params.setAttribute(importInformation, 2);
            params.setAttribute(clientContactStatus, 2);
//            params.setAttribute(importRemark, 3);
            params.setAttribute(clientContactDescription, 3);
            params.setAttribute(originalArrear, 4);
            params.setAttribute(currentArrear, 5);
            DSApi.context().closeStatement(ps);
        }
        return params;
    }

    public Object getAssignee() {
        return assignee;
    }

    public void setAssignee(Object assignee) {
        this.assignee = assignee;
    }

    @Override
    public void modifyDwrFacade(HttpServletRequest req, Map<String, Object> dwrSessionValues, FieldsManager fieldsManager, DwrFacade dwrfacade) throws EdmException {
        filterEmptyClientContactHistoryEntries(dwrSessionValues, fieldsManager);
    }


    private void addCreatedEntryToDictionaryView(Map<String, FieldData> values, final String DICTIONARY_CN, long entryId){
        /*
            DictionaryValueHandler -> return (cn.contains(dicCn + "_ID") || cn.equals("id")) && ((Map<String, FieldData>) newValue).get(cn).getData() != null && !((Map<String, FieldData>) newValue).get(cn).getStringData().equals("");
            ids.add(((Map<String, FieldData>) newValue).get(cn).getStringData());
            wpisy do slownika sa weryfikowane na podstawie powyzszego sprawdzenia (w klasie DictionaryValueHandler) wystapienia id w slowniku

            sufix "_ADD_CREATED_ENTRY" ma na celu zapobiec zduplikowaniu juz istniejacych wpisow z id pozostalych elementow wystepujacych w slowniku o strukturze wpisu z id "CN_SLOWNIKA_ID_NUMER_WPISU" gdzie NUMER_WPISU > 0
        */
        ((FieldData)values.get(Field.DWR_PREFIX + DICTIONARY_CN)).getDictionaryData().put(DICTIONARY_CN + "_ID_ADD_CREATED_ENTRY", new FieldData(Field.Type.LONG, entryId));
    }

    /**
     * Metoda sprawdza czy wpis do s�ownika historii kontakt�w z klientem zosta� utworzony poprzez wys�anie wiadomo�ci email
     * @return
     */
    private boolean isEntryCreatedBySendingMail(PreparedStatement ps, ResultSet rs, StringBuilder query, long entryId) throws EdmException, SQLException {

        query = new StringBuilder("SELECT " + ClientContactHistoryDictionary.CONTACT_TYPE + " FROM " + ClientContactHistoryDictionary.TABLE_NAME + " WHERE ID = ?");
        ps = DSApi.context().prepareStatement(query);
        ps.setLong(1, entryId);
        rs = ps.executeQuery();

        if(rs.next()){
            int contactType = rs.getInt(ClientContactHistoryDictionary.CONTACT_TYPE);
            rs.close();
            DSApi.context().closeStatement(ps);
            return contactType == ClientContactHistoryDictionary.CONTACT_TYPE_MAIL;
        }else{
            return false;
        }
    }

    /**
     * Metoda usuwa z p�l wy�wietlanych na formatce puste wpisy z historii kontakt�w z klientem.
     * Puste wpisy powstaj� w wyniku wst�pnego filtrowania w logice s�ownika ClientContactHistoryDictionary
     */
    private void filterEmptyClientContactHistoryEntries(Map<String, Object> dwrSessionValues, FieldsManager fieldsManager) throws EdmException {
        List<Map> contactHistory = (List<Map>) fieldsManager.getValue("CLIENT_CONTACT_HISTORY");
        List<Map> validContactEntries = new ArrayList<Map>();
        List<Long> validContactsId = new ArrayList<Long>();
        if(contactHistory != null){
            for(Map contactEntries : contactHistory){
                if(contactEntries.get("CLIENT_CONTACT_HISTORY_AUTHOR") != null){
                    validContactEntries.add(contactEntries);
                    validContactsId.add(Long.parseLong(String.valueOf(contactEntries.get("id"))));
                }
            }
        }
        ((Map<String, Object>) dwrSessionValues.get("FIELDS_VALUES")).put("CLIENT_CONTACT_HISTORY", validContactsId);
    }



    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        super.setInitialValues(fm, type);
    }

    @Override
    public void beforeSetWithHistory(Document document, Map<String, Object> values, String activity) throws EdmException {
    }

    @Override
    public void setAdditionalValues(FieldsManager fm, Integer valueOf, String activity) throws EdmException {
    }

    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) {
        return super.setMultipledictionaryValue(dictionaryName, values);
    }

    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues, Long documentId) {
        Map<String, Object> result = super.setMultipledictionaryValue(dictionaryName, values);
        if(result == null){
            result = new HashMap<String, Object>();
        }

        if(dictionaryName.equals(ClientContactHistoryDictionary.DICTIONARY_NAME)){
            setCurrentDate(dictionaryName, values, result);
        }
        return result;
    }

    private void setCurrentDate(String dictionaryName, Map<String, FieldData> values, Map<String, Object> result){
        if(values.get(ClientContactHistoryDictionary.DICTIONARY_NAME + "_" + ClientContactHistoryDictionary.CREATION_DATE).getData() == null){
            //ustawiamy automatycznie date dla wpisu, ktory zaczal tworzyc uzytkownik
            result.put(ClientContactHistoryDictionary.DICTIONARY_NAME + "_" + ClientContactHistoryDictionary.CREATION_DATE, new Date(System.currentTimeMillis()));
        }
    }

    @Override
    public void onLoadData(FieldsManager fm) throws EdmException {
        LoggerFactory.getLogger("MONITORING_PLATNOSCI").info("Wczytywanie warto�ci dla dokumentu: " + fm.getDocumentId());
        synchronizeCurrentArrearValue(fm);
    }

    @Override
    public boolean canReopenNow() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void reopenNow(OfficeDocument doc) throws EdmException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setReopenMarkerOn(OfficeDocument doc) throws EdmException {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}