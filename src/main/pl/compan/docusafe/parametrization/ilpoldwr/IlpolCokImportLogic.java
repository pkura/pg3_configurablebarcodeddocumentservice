package pl.compan.docusafe.parametrization.ilpoldwr;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.ilpol.ServicesUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: KrzysiekM
 * Date: 17.06.13
 * Time: 10:33
 * To change this template use File | Settings | File Templates.
 */
public class IlpolCokImportLogic extends AbstractDocumentLogic {
    private final static Logger LOG = LoggerFactory.getLogger(IlpolCokImportLogic.class);
    private final static StringManager SM = GlobalPreferences.loadPropertiesFile(IlpolCokImportLogic.class.getPackage().getName(), null);
    public final static String ATTACHED_COK_DOCUMENT_FIELD_CN = "IMPORTED_DOCUMENT";
    public final static String USER = "USER";
    public final static String NIP = "NIP";
    public final static String OBSZAR = "OBSZAR";
    public final static String KATEGORIA = "KATEGORIA";
    public final static String PODKATEGORIA = "PODKATEGORIA";
    public final static String TEMAT = "TEMAT";
    public final static String TRESC = "TRESC";
    public final static String TERMIN_WYKONANIA = "TERMIN_WYKONANIA";
    public final static String ZALEGLOSC_PIERWOTNA = "ZALEGLOSC_PIERWOTNA";

    private final static String DWR_IMPORT_COK_DOCUMENT_BUTTON = "DWR_IMPORT_COK_DOCUMENT_BUTTON";
    private boolean documentImported = false;

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        String importSummary = null;
        boolean isError = false;
        if(!documentImported) {
            try {
                importSummary = createCokDocument(document);
            } catch (IOException e) {
                isError = true;
                importSummary = e.getMessage();
                LOG.error(importSummary);
            } catch (EdmException e) {
                isError = true;
                importSummary = e.getMessage();
                LOG.error(importSummary);
            } finally{
                if(isError){
                    importSummary = SM.getString("BladPodsumowanieImportCok") + "<br /><br />" + importSummary;
                    LoggerFactory.getLogger("ilpol_cok_doc_import").debug(importSummary);
                }
                Map<String, Object> values = new HashMap<String, Object>();
                values.put("IMPORT_SUMMARY", importSummary);
                document.getDocumentKind().setOnly(document.getId(), values);
                documentImported = true;
            }
        }
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {

    }

    @Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        return super.validateDwr(values, fm);
    }

    private static String createCokDocument(Document document) throws EdmException, IOException {
        Attachment importedDocument = null;
        XSSFWorkbook wb = null;
        try {
            importedDocument = ((Attachment) document.getFieldsManager().getValue(ATTACHED_COK_DOCUMENT_FIELD_CN));
            wb = new XSSFWorkbook(importedDocument.getMostRecentRevision().getAttachmentStream());
        } catch (EdmException e) {
            LOG.error(SM.getString("BladPodsumowanieImportCok") + "\n\n" + e.getMessage());
            throw new EdmException(e.getMessage());
        } catch (IOException e) {
            LOG.error(SM.getString("BladPodsumowanieImportCok") + "\n\n" + e.getMessage());
            throw new IOException(e.getMessage());
        }


        int sheetNumber = wb.getNumberOfSheets();
        //  mapa opisujaca zaleznosc pomiedzy numerem kolumny a typem przechowywanych w niej danych (np. nazwa uzytkownika, temat i zawartosc dokumentu etc.)
        Map<Integer, String> indexDataTypeTitle = new HashMap<Integer, String>();
        Map<String, Object> documentData = new HashMap<String, Object>();

        XSSFSheet sheet = wb.getSheetAt(0);
        Iterator<Row> iterRow = null;
        Row row = null;
        Iterator<Cell> iterCell = null;

        Cell cell = null;
        prepareDataMapping(indexDataTypeTitle, sheet);

        StringBuilder importSummary = new StringBuilder();
        int importsFailureCounter = 0;
        int foundDocumentsCounter = 0;
        for(int num = 0; num < sheetNumber; num++){
            sheet = wb.getSheetAt(num);
            iterRow = sheet.rowIterator();

            //  pomijamy pierwszy wiersz z naglowkami kolumn
            if(iterRow.hasNext())
                iterRow.next();
            AFTER_FAILURE:
            while(iterRow.hasNext()){
                //  czyscimy liste wartosci pozyskanych z wiersza
                documentData.clear();
                row = iterRow.next();
                iterCell = row.cellIterator();
                while(iterCell.hasNext()){
                    cell = iterCell.next();
                    if(!cell.toString().isEmpty()){
                        //  pobieramy wartosci z kolejnych kolumn wiersza wiazac je z nazwa typu danych, ktore opisuja
                        try{
                            if(cell.getCellType() == Cell.CELL_TYPE_STRING){
                                documentData.put(indexDataTypeTitle.get(cell.getColumnIndex()), cell.toString().trim());
                            }else if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
                                documentData.put(indexDataTypeTitle.get(cell.getColumnIndex()), cell.toString().trim());
                            }else{
                                documentData.put(indexDataTypeTitle.get(cell.getColumnIndex()), cell.getDateCellValue());
                            }
                        }catch(Exception e){
                            documentData.put(indexDataTypeTitle.get(cell.getColumnIndex()), cell.toString().trim());
                        }
                    }else if(cell.getColumnIndex() <= indexDataTypeTitle.size()-1){
                        ++importsFailureCounter;
                        ++foundDocumentsCounter;
                        LOG.error("ILPOL: B��d importu dokumentu COK - b��dnie wype�nione pole typu " + indexDataTypeTitle.get(cell.getColumnIndex()) + " w wierszu " + (row.getRowNum()+1));
                        LoggerFactory.getLogger("ilpol_cok_doc_import").debug("ILPOL: B��d importu dokumentu COK - b��dnie wype�nione pole typu " + indexDataTypeTitle.get(cell.getColumnIndex()) + " w wierszu " + (row.getRowNum()+1));
                        continue AFTER_FAILURE;
                    }
                }
                try {
                    createImportedCokDocument(documentData);
                    importSummary.append(SM.getString("SukcesImportDaneCok", (row.getRowNum()+1)) + "<br />");
                } catch (EdmException e) {
                    LOG.error(SM.getString("BladImportCokLog", (row.getRowNum()+1)) + "\n\n" + e.getMessage());
                    ++importsFailureCounter;
                }catch (SQLException e) {
                    LOG.error(SM.getString("BladImportCokLog", (row.getRowNum()+1)) + "\n\n" + e.getMessage());
                    ++importsFailureCounter;
                } finally{
                    ++foundDocumentsCounter;
                }
            }
        }
        importSummary.append("<br />" + SM.getString("PodsumowanieImportCok", (foundDocumentsCounter - importsFailureCounter), foundDocumentsCounter));
        return importSummary.toString();
    }

    private static void createImportedCokDocument(Map<String, Object> documentData) throws EdmException, SQLException {
        LoggerFactory.getLogger("ilpol_cok_doc_import").info(
                " - - - - - IMPORTED COK DOCUMENT DATA BY " + DSApi.context().getDSUser().getWholeName() + " - - - - - " + "\n" +
                documentData.get(USER) + " | " +
                documentData.get(NIP) + " | " +
                documentData.get(OBSZAR) + " | " +
                documentData.get(KATEGORIA) + " | " +
                documentData.get(PODKATEGORIA) + " | " +
                documentData.get(TEMAT) + " | " +
                documentData.get(TRESC) + " | " +
                documentData.get(TERMIN_WYKONANIA) + " | " +
                documentData.get(ZALEGLOSC_PIERWOTNA)
        );
        Object assignedUsername = documentData.get(USER);
        DSUser assignedUser = null;
        if(assignedUsername != null && assignedUsername instanceof String){
            try{
                assignedUser = DSUser.findByUsername((String) assignedUsername);
                LoggerFactory.getLogger("ilpol_cok_doc_import").debug("Dekretacja importowanego domkumentu na " + assignedUser.getWholeName());
            }catch(Exception e){
                LOG.debug("ILPOL: " + e.getMessage(), e);
                LoggerFactory.getLogger("ilpol_cok_doc_import").debug("Nie znaleziono u�ytkownika. Dekretacja importowanego dokumentu na grup� COK");
            }
        }
        CokUtils.createDocumentFromImport(new InOfficeDocument(), DocumentKind.findByCn(IlpolCokImportedLogic.DOCKIND_NAME), assignedUser, assignedUser, null, "Import dokumentu COK z pliku", "Import COK", "Import dokumentu COK z pliku", documentData);
    }

    private static void prepareDataMapping(Map<Integer, String> indexDataTypeTitle, XSSFSheet sheet){
        LoggerFactory.getLogger("ilpol_cok_doc_import").debug(" - - - - - DATA MAPPING - - - - - ");
        Iterator<Row> iterRow = null;
        Row row = null;
        Iterator<Cell> iterCell = null;
        Cell cell = null;
        row = sheet.getRow(0);
        if(row != null){
            iterCell = row.cellIterator();
            while(iterCell.hasNext()){
                cell = iterCell.next();
                indexDataTypeTitle.put(cell.getColumnIndex(), cell.toString());
                LoggerFactory.getLogger("ilpol_cok_doc_import").debug(cell.getColumnIndex() + " : " + cell.toString());
            }
        }
    }
}