package pl.compan.docusafe.parametrization.ilpoldwr;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang.StringUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.sql.AcceptanceDivisionImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.tiff.ImageKit;
import pl.compan.docusafe.webwork.event.ActionEvent;
import javax.servlet.http.HttpSession;
import java.util.*;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

public class NormalLogic extends AbstractDocumentLogic
{
    public final static String ILPOL_SPLITTED_TIFF = "ILPOL_SPLITTED_TIFF";
    public static final String ILPL_CLIENT = "ILPL_CLIENT";
    public static final String ILPOL_KIND = "ILPOL_KIND";
    public static final String DWR_ILPOL_KIND = "DWR_ILPOL_KIND";
    public static final String NORMAL_IN_DOCKIND_NAME = "normal";
    public static final String NORMAL_OUT_DOCKIND_NAME = "normal_out";

    private static NormalLogic instance;
	protected static Logger log = LoggerFactory.getLogger(NormalLogic.class);
	private final static StringManager sm = StringManager.getManager(NormalLogic.class.getPackage().getName());

    private final static String DWR_RECIPIENT_HERE = "DWR_RECIPIENT_HERE";
    private final static String DWR_SENDER_HERE = "DWR_SENDER_HERE";
    private final static String SENDER = "SENDER";
	private final static String SENDER_ZIP = "SENDER_ZIP";
	private final static String SENDER_LOCATION = "SENDER_LOCATION";
	private final static String SENDER_MULTIPLE_ZIP_CODES = "SENDER_MULTIPLE_ZIP_CODES";
	private final static String SENDER_HERE = "SENDER_HERE";
	private Boolean isCokDocumentCreated = Boolean.FALSE;

	public static NormalLogic getInstance()
	{
		if (instance == null)
			instance = new NormalLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException
	{
//        LoggerFactory.getLogger("krzysiekm").info("COK_IN: " + this + " :  documentPermissions(Document document)" + " " + document.getId());
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
		// this.setUpPermission(document, perms);
	}

	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		log.info("type: {}", type);
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("TYPE", type);
		values.put("DOC_DATE", new Date());
        values.put("DOLACZ_DOC", "10");
        values.put("ILPL_CLIENT", true);

		fm.reloadValues(values);
	}

	public int getPermissionPolicyMode()
	{
		return PermissionManager.NORMAL_POLICY;
	}

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
        List<Document> documents = Lists.newArrayList(document);
        if(!isCokDocumentCreated){
            Document cokDoc = prepareAndCreateCokDocument(document);
            if(cokDoc != null)
                documents.add(cokDoc);
        }

        if (document.getFolder() == null || document.getFolder().isRoot())
        {
            if (DocumentLogic.TYPE_ARCHIVE == type)
                document.setFolder(Folder.getRootFolder());
            else
                document.setFolder(Folder.findSystemFolder(Folder.SN_OFFICE));
        }
        document.setDoctypeOnly(null);

        HttpSession session = ServletActionContext.getRequest().getSession(false);
        Preconditions.checkNotNull(session);
        if(session.getAttribute(ILPOL_SPLITTED_TIFF) != null) {
            List<String> selectedFiles = (List<String>) session.getAttribute(ILPOL_SPLITTED_TIFF);
            try {
                ImageKit.addAttachments(selectedFiles.toArray(new String[selectedFiles.size()]), documents.toArray(new Document[documents.size()]), 1);
            } catch (Exception e) {
                throw new EdmException(e.getLocalizedMessage(), e);
            }

            session.removeAttribute(ILPOL_SPLITTED_TIFF);
        }
    }

    private Document prepareAndCreateCokDocument(Document document) throws EdmException {
        log.debug("Pr�ba utworzenia dokumentu Zg�oszenie COK");
        Map<String,Object> values = new HashMap<String, Object>();
        Document cokDoc = null;

        boolean client = document.getFieldsManager().getBoolean(ILPL_CLIENT);
        String docTypeCn;

        Long selectedDocType = document.getFieldsManager().getLongKey(ILPOL_KIND);
        // na wszelki wypadek, bo zawsze powinien by� wybrany DOC_TYPE
        if(selectedDocType != null) {
            EnumItem enumItem = document.getFieldsManager().getField(ILPOL_KIND).getEnumItem(selectedDocType.intValue());
            CokUtils.Assignment asgn = new CokUtils.Assignment(enumItem);
            if(asgn.guid != null && asgn.createCokDoc) {
                Map<String, String> senderData = null;
                Person sender = null;
                String senderNameData = document.getFieldsManager().getValue("SENDER").toString();
                //  na informacje o nadawcy skladaja sie dane o nazwie organizacji imieniu nazwisku, jesli wystepuja wszystkie te dane to sa one od siebie odseparowane przecinkiem
                if(senderNameData.lastIndexOf(",") != -1)
                    sender = Person.findPersonByOrganization(senderNameData.substring(0, senderNameData.lastIndexOf(",")));
                else
                    sender = Person.findPersonByOrganization(senderNameData);


                if(sender == null){
                    String[] senderPersonalData = senderNameData.split(" ");
                    senderData = new HashMap<String, String>();
                    senderData.put("FIRSTNAME", senderPersonalData[0]);
                    senderData.put("LASTNAME", senderPersonalData[senderPersonalData.length-1]);
                    sender = Person.findPerson(senderData);
                }

                if(sender != null && sender.getNip() != null){
                    List<Contractor> ctrList = Contractor.findByNIP(sender.getNip());
                    Contractor contractor = null;
                    if(ctrList != null && ctrList.size() > 0){
                        contractor = ctrList.get(0);
                        values.put("KLIENT", contractor.getId());
                    }else{

                    }
                } else{
                    log.debug("Nie znaleziono nadawcy o danych " + senderNameData);
                }

                values.put("CONVERSATION_DESCRIPTION", document.getFieldsManager().getValue("DOC_DESCRIPTION"));
                values.put("FAX_SEND_DATETIME", document.getFieldsManager().getValue("DOC_DATE"));
                CokUtils.createBaseDocument(new InOfficeDocument(), DocumentKind.findByCn(IlpolCokLogic.DOCKIND_NAME), CokUtils.retriveUserLogin(), values, "Dokument generowany z pisma przychodz�cego", "Zg�oszenie COK", "Dokument generowany z pisma przychodz�cego");
                DataMartManager.addHistoryEntry(document.getId(), Audit.create(DataMartDefs.DOCUMENT_CREATE, DSApi.context().getPrincipalName(), sm.getString("UtworzonoZgloszenieICS")));
                isCokDocumentCreated = Boolean.TRUE;
            }
        }
        return cokDoc;
    }

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
//        LoggerFactory.getLogger("krzysiekm").info("COK_IN: " + this + " :  onStartProcess(OfficeDocument document, ActionEvent event)" + " " + document.getId());

        try
		{
//	        System.out.println("KOOOOOOOOOOOOOO +"+document.getOfficeNumber());
	        Map<String, Object> values = new HashMap<String, Object>();
			values.put("KO", document.getOfficeNumber());
			document.getDocumentKind().setOnly(document.getId(), values);
			Map<String, Object> map = Maps.newHashMap();
            String assigneeName = null;
			if (document instanceof OutOfficeDocument)
			{
				ProcessCoordinator coor = getProcessCoordinator(document);
				if(coor != null)
				{
					map.put(ASSIGN_USER_PARAM, coor.getUsername());
					map.put(ASSIGN_DIVISION_GUID_PARAM, coor.getGuid());
				}
			}
			else
			{
				map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
				map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			}
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}
    
	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
        FieldsManager fm = document.getFieldsManager();
        String username = "";

        if(fm != null && fm.getField(SENDER_HERE) != null && fm.getField(ILPOL_KIND) != null) {
            ProcessCoordinator coordinator = new ProcessCoordinator();
//            System.out.println("new coord " + coordinator.getGuid());
            if(!document.getSource().equals(IlpolNormalOutImportLogic.NORMAL_OUT_IMPORT)){
                EnumItem enumItem = fm.getEnumItem(ILPOL_KIND);
                CokUtils.Assignment asgn = new CokUtils.Assignment(enumItem);
                Long personId = fm.getLongKey(SENDER_HERE);
                coordinator.setGuid(asgn.getDivisionGuid());
            }else{
                if(StringUtils.isBlank(document.getCurrentAssignmentGuid())){
                    coordinator.setUsername(CokUtils.retriveUserLogin());
                    coordinator.setGuid(null);
                }else{
                    coordinator.setUsername(null);
                    coordinator.setGuid(document.getCurrentAssignmentGuid());
                }
            }
            return coordinator;
        } else {
            String kind = "ILPL";
            String divsILII = document.getDocumentKind().getDockindInfo().getProperties().get("rootIIPL");
            String divsILPL = document.getDocumentKind().getDockindInfo().getProperties().get("rootILPL");
            String div = document.getDivisionGuid();
            boolean found = false;
            while (!found)
            {
                if (div.equals(divsILII))
                {
                    kind = "IIPL";
                    break;
                }
                else if (div.equals(divsILPL))
                {
                    kind = "ILPL";
                    break;

                }
                DSDivision parent = DSDivision.find(div).getParent();
                if (parent == null)
                {
                    throw new EdmException("Nie znaleziono dzia�u g��wnego");
                }
                div = parent.getGuid();
            }
            List<ProcessCoordinator> list = ProcessCoordinator.find(DocumentKind.NORMAL_KIND, kind);
//            System.out.println("old coord " + list.get(0).getGuid());
            if (list != null && list.size() > 0)
                return list.get(0);
            return null;
        }
	}
	
    @Override
    public void bindToJournal(OfficeDocument document,ActionEvent event) throws EdmException
    {
       Date entryDate = GlobalPreferences.getCurrentDay();
       String guid = document.getDivisionGuid();
       if(DSDivision.ROOT_GUID.equals(guid))
    	   throw new EdmException("Brak dzia�u");
       Journal journal = null;
       String journalType = DocumentType.OUTGOING.equals(document.getType()) ? "outgoing" : "incoming";

       ProcessCoordinator coordinator = this.getProcessCoordinator(document);
       if(coordinator != null) {
           if(document.getSource().equals(IlpolNormalOutImportLogic.NORMAL_OUT_IMPORT)){
               guid = document.getDivisionGuid();
           }else{
               guid = coordinator.getGuid();
           }
       }
       boolean found = false;
       while(!found)
       {
    	   List<Journal> journals = Journal.findByDivisionGuid(guid, journalType);
    	   if(journals != null && journals.size() > 0)
    	   {
               found = true;
    		   journal = journals.get(0);
    	   }
    	   else
    	   {
    		   DSDivision parent =  DSDivision.find(guid).getParent();
    		   if(parent == null)
    		   {
    			   throw new EdmException("Nie znaleziono dziennika");
    		   }
    		   guid = parent.getGuid();
    	   }
       }

        if (journal.findDocumentEntry(document.getId()) != null)
            throw new EdmException("Pismo znajduje si� ju� w dzienniku");

        Integer sequenceId = Journal.TX_newEntry2(journal.getId(), document.getId(), entryDate);
        if(event != null)
        	event.addActionMessage("Dodano do dziennika "+journal.getDescription()+" pod numerem "+sequenceId);

    }

    @Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        super.validateDwr(values, fm);
        Map.Entry<String, FieldData> senderField = DwrUtils.getSenderField(values);
        String senderFieldCn = senderField != null ? senderField.getKey() : "";

        if(senderFieldCn.equals(DWR_ILPOL_KIND)) {
            // zmiana rodzaju pisma
            Long selectedDocType = DwrUtils.getLongValue(values, senderFieldCn);

            String guid = "-1";
            if(selectedDocType != null) {
                EnumItem enumItem = fm.getField(DwrUtils.getCnWithoutPrefix(senderFieldCn)).getEnumItem(selectedDocType.intValue());
                CokUtils.Assignment asgn = new CokUtils.Assignment(enumItem);
                if(asgn.guid != null)
                    guid = asgn.guid;
            }
            String dockindCn = fm.getDocumentKind().getCn();
            Set<DocumentType> documentTypes = fm.getDocumentKind().getDockindInfo().getDocumentTypes();

            if(documentTypes.contains(DocumentType.INCOMING)) {
                values.get(DWR_RECIPIENT_HERE).getDictionaryData().put("id", new FieldData(Field.Type.STRING, guid));
            } else if(documentTypes.contains(DocumentType.OUTGOING)) {
                values.get(DWR_SENDER_HERE).getDictionaryData().put("id", new FieldData(Field.Type.STRING, guid));
            }
        }

        return null;
    }
    
    @Override
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValue, Long documentId) {
//        LoggerFactory.getLogger("krzysiekm").info("COK_IN: " + this + " :  setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValue, Long documentID)" + " " + documentId);
        Map<String, Object> ret = new HashMap<String, Object>();
		if(values != null) {
			FieldData zipFieldData = values.get(SENDER_ZIP);
			FieldData locationFieldData = values.get(SENDER_LOCATION);
			String zip = null;
			String location = null;
			
			if(zipFieldData != null) {
				zip = TextUtils.trimmedStringOrNull(zipFieldData.getStringData());
			}
			if(locationFieldData != null) {
				location = TextUtils.trimmedStringOrNull(locationFieldData.getStringData());
			}
			
			ret.put(SENDER_MULTIPLE_ZIP_CODES, "");
			// je�li u�ytkownik poda� tylko kod pocztowy, to uzupe�niamy miasto
			if(zip != null && location == null) {
				int code = TextUtils.parseLongSafe(zip).intValue();
				PostalCode postalCode = PostalCode.findByCode(code);
				if(postalCode != null) {
					ret.put(SENDER_LOCATION, postalCode.getCity());
				}
			} else if(zip == null && location != null) {
				List<PostalCode> postalCodes = PostalCode.findByLocation(location);
				if(postalCodes != null && !postalCodes.isEmpty()) {
					PostalCode postalCode = postalCodes.get(0);
					
					if(postalCode.getCode_from() < postalCode.getCode_to()) {
						// wi�cej niz jeden kod dla miasta
						String zipFrom = PostalCode.getFormattedCode(postalCode.getCode_from());
						String zipTo = PostalCode.getFormattedCode(postalCode.getCode_to());
						String msg = sm.getString("NieMaJednegoKoduPocztowego", zipFrom, zipTo);
						
						ret.put(SENDER_MULTIPLE_ZIP_CODES, msg);
						ret.put(SENDER_ZIP, "");
					} else {
						String formattedCode = PostalCode.getFormattedCode(postalCode.getCode_from());
						ret.put(SENDER_ZIP, formattedCode);
					}
					
				}
			}
		}
		
		return ret;
	}

    @Override
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException {
        super.validate(values, kind, documentId);
    }

    @Override
    public void validate(Map<String, Object> values, DocumentKind kind) throws EdmException
    {
        validate(values, kind, null);
    }

    @Override
    public void correctValues(Map<String,Object> values, DocumentKind kind) throws EdmException
    {
    }

    @Override
    public boolean validateSuccess(Map<String,Object> values, DocumentKind kind) throws EdmException
    {
        try
        {
            validate(values, kind, null);
            return true;
        }
        catch (ValidationException e)
        {
            return false;
        }
    }
}
