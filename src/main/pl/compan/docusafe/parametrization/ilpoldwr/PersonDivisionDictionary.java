package pl.compan.docusafe.parametrization.ilpoldwr;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dictionary.DictionaryUtils;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.UserDivisionDictionary;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.sql.DivisionImpl;

import java.util.List;
import java.util.Map;

public class PersonDivisionDictionary extends UserDivisionDictionary {
    public static final String DIVISION = "DIVISION";

    private List<Map<String, Object>> searchDivisions(String divisionName) throws EdmException {
        List<Map<String, Object>> results = Lists.newArrayList();
        String divisionFieldCn = DictionaryUtils.getFieldCnWithDictionaryPrefix(this.getName(), DIVISION);

        try {
            Criteria crit = DSApi.context().session().createCriteria(DivisionImpl.class);
            crit.add(Restrictions.and(Restrictions.ilike("name", '%' + divisionName + '%'), Restrictions.eq("hidden", false)));
            List<DSDivision> divs = crit.list();
            for(DSDivision div : divs) {
                Map<String, Object> singleResult = Maps.newHashMap();
                singleResult.put("id", ";d:" + div.getGuid());
                singleResult.put(divisionFieldCn, div.getName());

                results.add(singleResult);
            }

        } catch(HibernateException e) {
            throw new EdmException(e.getLocalizedMessage(), e);
        }

        return results;
    }

    @Override
    public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset, boolean fromDwrSearch,
    		Map<String, FieldData> dockindFields) throws EdmException {
        List<Map<String, Object>> results = Lists.newArrayList();
        String divisionFieldCn = DictionaryUtils.getFieldCnWithDictionaryPrefix(this.getName(), DIVISION);
        FieldData divFd = values.get(divisionFieldCn);
        String divisionName = null;
        if(divFd != null && divFd.getData() != null && !StringUtils.isEmpty(divFd.getStringData())) {
            divisionName = divFd.getStringData();
        }

        if(divisionName != null && offset == 0) {
            results.addAll(searchDivisions(divisionName));
        }
        results.addAll(super.search(values, limit, offset));

        return results;
    }
}
