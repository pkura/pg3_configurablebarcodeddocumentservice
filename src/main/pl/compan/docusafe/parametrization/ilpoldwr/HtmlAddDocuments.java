package pl.compan.docusafe.parametrization.ilpoldwr;



import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.HTMLFieldLogic;

public class HtmlAddDocuments implements HTMLFieldLogic {
	

	@Override
	public String getValue(String docId) throws EdmException  {

		return getValue();
	}

	@Override
	public String getValue() {
		String baseUrl = Docusafe.pageContext;
		StringBuilder sb = new StringBuilder();
        	sb.append("<input type=\"button\" class=\"btn\" value=\"Dodaj dokumenty\"	onclick=\"openToolWindow('/"+baseUrl+"/repository/new-portfolios-for-doc.action?doNew=true&binderId=' + $j('#documentId').val() +");
        	sb.append("'&id='+ $j('#DWR_KLIENT').val()+'&applicationId='+ $j('#DWR_NUMER_WNIOSKU').val()+'&contractId='+ $j('#DWR_NUMER_UMOWY').val(),'vs',800,600);\"/>");
        	
//        	<input type="button" class="btn" value="Dodaj dokumenty"	onclick=" return openToolWindow('/docusafe/repository/new-portfolios-for-doc.action?doNew=true&binderId=' + $j('#documentId').val() +
//            		'&id='+ $j('#DWR_KLIENT').val()+'&applicationId='+ $j('#DWR_NUMER_WNIOSKU').val()+'&contractId='+ $j('#DWR_NUMER_UMOWY').val(),'vs',800,600);"/>
		return sb.toString();
	}
}
