package pl.compan.docusafe.parametrization.ilpoldwr;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentNotFoundException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.crm.Contact;
import pl.compan.docusafe.core.crm.ContactKind;
import pl.compan.docusafe.core.crm.ContactStatus;
import pl.compan.docusafe.core.crm.LeafletKind;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.db.criteria.NativeOrderExp.OrderType;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.field.DocumentField;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.parametrization.ilpol.CrmMarketingTaskLogic;
import pl.compan.docusafe.parametrization.ilpol.DLBinder;
import pl.compan.docusafe.parametrization.ilpol.DlLogic;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author Kamil Skrzypek
 */
public class ContactDictionary extends DwrDictionaryBase
{

	private static final Logger log = LoggerFactory.getLogger(ContactDictionary.class);

	public long add(Map<String, FieldData> values) throws EdmException
	{

		boolean newDictionaryValueIsNotEmpty = false;
		// Na poczatku sprawdzamy, czy nie jest to przypadek, w ktorym slownik
		// jest caly pusty (nie wpisano zadnej wartosci)
		for (FieldData fieldData : values.values())
		{
			if ((fieldData.getData() != null) && (!fieldData.getData().equals("")))
			{
				newDictionaryValueIsNotEmpty = true;
				break;
			}
		}

		long ret = -1;

		if (!newDictionaryValueIsNotEmpty)
			return ret;

		try
		{
			//DSApi.context().begin();
			Contact contact = new Contact();
			setValues(contact, values);
			contact.create();
			ret = contact.getId();
			// updateDocument(true, contact);
			// documentReassigned = true;
			// if(goToTasklist || statusKontaktu == 115)
			// reloadLink =
			// "/office/tasklist/current-user-task-list.action?tab=in";
			//DSApi.context().commit();
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return ret;

	}

	private void setValues(Contact contact, Map<String, FieldData> values) throws EdmException
	{
		
		Date dataKontaktu = null;
		if (values.get("DATAKONTAKTU") != null && values.get("DATAKONTAKTU").getData() != null)
			dataKontaktu = values.get("DATAKONTAKTU").getDateData();
		contact.setDataKontaktu(dataKontaktu);
		
		Date dataNastepnegoKontaktu = null;
		if (values.get("DATANASTEPNEGOKONTAKTU") != null && values.get("DATANASTEPNEGOKONTAKTU").getData() != null)
			dataNastepnegoKontaktu = values.get("DATANASTEPNEGOKONTAKTU").getDateData();
		contact.setDataNastepnegoKontaktu(dataNastepnegoKontaktu);
		
		String celKontaktu = null;
		if (values.get("CELKONTAKTU") != null && values.get("CELKONTAKTU").getData() != null)
			celKontaktu = values.get("CELKONTAKTU").getStringData();
		 contact.setCelKontaktu(celKontaktu);
		 
		String opis = null;
		if (values.get("OPIS") != null && values.get("OPIS").getData() != null)
			opis = values.get("OPIS").getStringData();
		 contact.setOpis(opis);
		 
		Long rodzajKontaktu = null;
		if (values.get("RODZAJKONTAKTU") != null && values.get("RODZAJKONTAKTU").getData() != null && !values.get("RODZAJKONTAKTU").getData().equals(""))
		{
			rodzajKontaktu = Long.valueOf((String) values.get("RODZAJKONTAKTU").getData());
			contact.setRodzajkontaktu(ContactKind.find(rodzajKontaktu));
		}

		Long rodzajNastepnegoKontaktu = null;
		if (values.get("RODZAJNASTEPNEGOKONTAKTU") != null && values.get("RODZAJNASTEPNEGOKONTAKTU").getData() != null && !values.get("RODZAJNASTEPNEGOKONTAKTU").getData().equals(""))
		{
			rodzajNastepnegoKontaktu = Long.valueOf((String) values.get("RODZAJNASTEPNEGOKONTAKTU").getData());
			contact.setRodzajNastepnegoKontaktu(ContactKind.find(rodzajNastepnegoKontaktu));
		}
	        
		Long statusKontaktu = null;
		if (values.get("STATUS") != null && values.get("STATUS").getData() != null && !values.get("STATUS").getData().equals(""))
		{
			statusKontaktu = Long.valueOf((String) values.get("STATUS").getData());
			contact.setStatus(ContactStatus.find(statusKontaktu));
		}
	}
	
//	private void updateDocument(boolean isNew,Contact contactThis) throws EdmException
//    {
//    	if(documentId == null)
//    		throw new EdmException("Nie znaleziono dokumentu");
//    	Document document = Document.find(documentId);
//    	FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
//    	List<Contact> l = new ArrayList<Contact>();
//    	if(isNew)
//    	{
//    		l = new ArrayList<Contact>((List<Contact>) fm.getValue(CrmMarketingTaskLogic.KONTAKT_FIELD_CN));
//    		l.add(contactThis);
//    		Map<String, Object> values = new HashMap<String, Object>();
//    		List<Long> newList = new ArrayList<Long>(l.size());
//    		for (Contact con : l)
//    		{
//				newList.add(con.getId());
//			}
//    		values.put(CrmMarketingTaskLogic.KONTAKT_FIELD_CN,newList);
//    		document.getDocumentKind().setOnly(document.getId(), values);
//    	}
//         if(fm.getValue(CrmMarketingTaskLogic.KONTAKT_FIELD_CN) != null && !(fm.getValue(CrmMarketingTaskLogic.KONTAKT_FIELD_CN) instanceof Contact))
//         {
//         	l.addAll((List<Contact>) fm.getValue(CrmMarketingTaskLogic.KONTAKT_FIELD_CN));
//         }
//        
//     	if(l != null && l.size() > 0 )
//     	{
//     		
//     		Contact contact = l.get(l.size()-1);
// 			
//     		if(contact.getStatus().getId() == 115)
// 			{
// 				LabelsManager.removeLabelsByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAMES_TAB);
//     			String[] accs = WorkflowFactory.findDocumentTasks(document.getId(), DSApi.context().getPrincipalName());
//     			if(accs == null || accs.length < 1)
//     			{
//     				log.info("Brak zadania na li�cie zada�");
//     			}
//     			else
//     			{
//     				WorkflowFactory.getInstance().manualFinish(accs[0], false);
//     				documentReassigned = true;
//                    goToTasklist = true;
//                    reloadLink = "/office/tasklist/current-user-task-list.action?tab=in";
//     			}
//     			log.info("Zakonczono prace z dokumentem o ID: " + document.getId());
// 			}
//     		else if(contact.getStatus().getId() == 20)
// 			{
// 				int noAnswerCount = 0;
// 				for(Contact c : l)
// 				{
// 					if(c.getStatus().getId() == 20)
// 					{
// 						++noAnswerCount;
// 					}
// 					else
// 					{
// 						noAnswerCount=0;
// 					}
// 				}
// 				log.info("Ilosc kontaktow bez odbioru: " + noAnswerCount);
// 				if(noAnswerCount < 5)
// 				{
//     				LabelsManager.removeLabelsByName(document.getId(),CrmMarketingTaskLogic.LABEL_NAMES_TAB);    				
// 					LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_1_DNI, DSApi.context().getPrincipalName(),true);
// 				}
// 				else
// 				{
// 	    			String[] accs = WorkflowFactory.findDocumentTasks(document.getId(), DSApi.context().getPrincipalName());
// 	    			if(accs == null || accs.length < 1)
// 	    			{
// 	    				throw new EdmException("Brak zadania na li�cie zada�");
// 	    			}
// 	    			
// 		    		WorkflowFactory.getInstance().manualFinish(accs[0], false);
//     				documentReassigned = true;
//                    goToTasklist = true;
//                    reloadLink = "/office/tasklist/current-user-task-list.action?tab=in";
// 		    		log.info("Zakonczono prace z 5-cio krotnie ustawionym statusem 'nie dobiera'. ID dokumentu: " + document.getId());
// 				}
// 			}			
//     		else
// 	    	{
// 	    		
// 	    		Date d = contact.getDataNastepnegoKontaktu();
// 	    		if( d != null)
// 	    		{
// 	    			Integer i = pl.compan.docusafe.util.DateUtils.substract(d, new Date());
// 	    			log.info("XXXXXXXXXXXILOSC dni do kontaktu: " + i);
// 	    			if(i > 5)
// 	    			{
// 	    				LabelsManager.removeLabelsByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAMES_TAB);
// 	    				LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ODLEGLE, DSApi.context().getPrincipalName(),true);
// 	    				Date doDate = org.apache.commons.lang.time.DateUtils.addDays(new Date(), i-5);
// 	    				doDate = org.apache.commons.lang.time.DateUtils.setHours(doDate, 1);
// 	    				EventFactory.registerEvent("forOneDay", "crmMarketingTaskSetLabelEventHandler"
// 	    						,""+i+"|"+LabelsManager.findLabelByName(CrmMarketingTaskLogic.LABEL_NAME_ZA_5_DNI).getId()+"|"+document.getId(),null, doDate);
// 	    			}
// 	    			else
// 	    			{
// 	    				LabelsManager.removeLabelsByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAMES_TAB);    				
// 		    			switch (i)
// 		    			{
// 							case 0:
// 								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_DISIAJ, DSApi.context().getPrincipalName(),true);
// 								break;
// 							case 1:
// 								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_1_DNI, DSApi.context().getPrincipalName(),true);
// 								break;
// 							case 2:
// 								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_2_DNI, DSApi.context().getPrincipalName(),true);
// 								break;
// 							case 3:
// 								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_3_DNI, DSApi.context().getPrincipalName(),true);
// 								break;
// 							case 4:
// 								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_4_DNI, DSApi.context().getPrincipalName(),true);
// 								break;
// 							case 5:
// 								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_5_DNI, DSApi.context().getPrincipalName(),true);
// 								break;		
// 							default:
// 								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_PRZETERMINOWANE, DSApi.context().getPrincipalName(),true);
// 								break;
// 						}
// 	    			}
// 	    		}
// 	    	}
//     	}
//    	document.getDocumentKind().logic().archiveActions(document,CrmMarketingTaskLogic.UPDATE_DOCUMENT );
//		TaskSnapshot.updateByDocument(document);
//		
//    }
}
