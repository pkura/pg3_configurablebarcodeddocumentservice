package pl.compan.docusafe.parametrization.ilpoldwr;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import com.google.common.base.Preconditions;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.TextUtils;

public class CaseNumber {

    long generatedId;
	Date date;
	int areaId;
    private Long caseId;
    private String[] areas = {"", "UA", "ZU", "UB", "SZ", "RO", "MP", "WN"};

	public CaseNumber(long generatedId, Date date, int areaId) {
		this.generatedId = generatedId;
		this.date = date;
		this.areaId = areaId;
	}

    public CaseNumber(String generatedCase) throws IllegalArgumentException {
        Preconditions.checkNotNull(generatedCase);
        String[] splitted = generatedCase.split("/");
        if(splitted.length != 3) {
            throw new IllegalArgumentException();
        }

        String id = splitted[0];
        String date = splitted[1];
        String areaId = splitted[2];

        try {
            this.generatedId = Long.parseLong(id, 10);
        } catch(NumberFormatException e) {
            throw new IllegalArgumentException(e);
        }

        DateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        try {
            this.date = fmt.parse(date);
        } catch(ParseException e) {
            throw new IllegalArgumentException(e);
        }

        this.areaId = Arrays.asList(areas).indexOf(TextUtils.nullSafeString(areaId.toUpperCase()));
    }

    public Long generateCaseId() throws EdmException, SQLException {
        if(caseId == null) {
            PreparedStatement ps = DSApi.context().prepareStatement("select DSC_ILPOL_COK_CASE_ID.nextval from dual");
            ResultSet rs = ps.executeQuery();
            caseId = null;
            if(rs.next()) {
                caseId = rs.getLong(1);
            }
            DSApi.context().closeStatement(ps);
            return caseId;
        }else{
            return caseId;
        }
    }
	
	@Override
	public String toString() {
		// wygenerowane id
		StringBuilder sb = new StringBuilder();
		sb.append(StringUtils.leftPad(Long.toString(generatedId), 5, '0'));
		sb.append('/');
		
		// data
		DateFormat fmt = new SimpleDateFormat("yyyyMMdd");
		sb.append(fmt.format(date));
		sb.append('/');
		
		// nazwa obszaru
		String areaSymbol = "";
		switch(areaId) {
		case 1:
			areaSymbol = "UA";
			break;
		case 2:
			areaSymbol = "ZU";
			break;
		case 3:
			areaSymbol = "UB";
			break;
		case 4:
			areaSymbol = "SZ";
			break;
		case 5:
			areaSymbol = "RO";
			break;
		case 6:
			areaSymbol = "MP";
			break;
		case 7:
			areaSymbol = "WN";
			break;
		}
		sb.append(areaSymbol);
		
		return sb.toString();
	}

    public long getGeneratedId() {
        return generatedId;
    }

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
