package pl.compan.docusafe.parametrization.ilpoldwr;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.permission.DlPermissionManager;
import pl.compan.docusafe.core.base.permission.DlPermissionPolicy;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.base.permission.PermissionPolicy;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.crm.Patron;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.tiff.ImageKit;

public class LeasingLogic extends AbstractDocumentLogic
{
	private static final Logger log = LoggerFactory.getLogger(LeasingLogic.class);

	private static LeasingLogic instance;
	// nazwy kodowe poszczególnych pól dla tego rodzaju dokumentu
	public static final String KLIENT_CN = "KLIENT";
	public static final String NUMER_UMOWY_CN = "NUMER_UMOWY";
	public static final String NUMER_WNIOSKU_CN = "NUMER_WNIOSKU";
	public static final String RODZAJ_DOKUMENTU_CN = "RODZAJ_DOKUMENTU";
	public static final String TYP_DOKUMENTU_CN = "TYP_DOKUMENTU";
	public static final String DATA_DOKUMENTU_CN = "DATA_DOKUMENTU";
	public static final String STATUS_DOKUMENTU_CN = "STATUS_DOKUMENTU";
	public static final String NUMER_DOKUMENTU_CN = "NUMER_DOKUMENTU";
	public static final String KWOTA_CN = "KWOTA";

	public static final String RODZAJ_RAPORTU_CN = "RODZAJ_RAPORTU";
	public static final String RODZAJ_DW_CN = "RODZAJ_DW";
	public static final String RODZAJ_DL_CN = "RODZAJ_DL";
	public static final String RODZAJ_DZ_CN = "RODZAJ_DZ";
	public static final String RODZAJ_DRU_CN = "RODZAJ_DRU";
	public static final String RODZAJ_FILPOL_CN = "RODZAJ_FILPOL";
	public static final String RODZAJ_DEKLARACJI_CN = "RODZAJ_DEKLARACJI";
	public static final String RODZAJ_DOKUMENTU_FINANSOWEGO_CN = "RODZAJ_DOKUMENTU_FINANSOWEGO";
	public static final String RODZAJ_ZPL_CN = "RODZAJ_ZPL";

	// nazwy kodowe poszczególnych rodzajow dokumentu
	public static final String RD_DOKUMENT_REJESTROWY_CN = "DOKUMENT_REJESTROWY";
	public static final String RD_DOKUMENT_FINANSOWE_CN = "DOKUMENT_FINANSOWE";
	public static final String RD_WNIOSKI_I_FORMULARZE_CN = "WNIOSKI_I_FORMULARZE";
	public static final String RD_DOKUMENTY_DODATKOWE_CN = "DOKUMENTY_DODATKOWE";
	public static final String RD_DOKUMENTY_UMOWY_KLIENTA_CN = "DOKUMENTY_UMOWY_KLIENTA";

	public static synchronized LeasingLogic getInstance()
	{
		if (instance == null)
			instance = new LeasingLogic();
		return instance;
	}

	public void onLoadData(FieldsManager fm) throws EdmException
	{
		String umowaId = "", wniosekId = "";
		if (fm.getDocumentId() != null)
		{
			Map<String, Object> values = new HashMap<String, Object>();
			log.info("Start ADD {}", fm.getFieldValues());

			if (fm.getKey("NUMER_WNIOSKU") != null)
				wniosekId = fm.getKey("NUMER_WNIOSKU").toString();
			if (fm.getKey("NUMER_UMOWY") != null)
				umowaId = fm.getKey("NUMER_UMOWY").toString();

			Map<String, ArrayList<Long>> documentLeasing = new HashMap<String, ArrayList<Long>>();

			fm.reloadValues(values);

		}

	}

	public void documentPermissions(Document document) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		List<String> users = new ArrayList<String>();
		try
		{
			Long numerwniosku = null;
			Long numwerUmowy = null;
			String region = null;
			if (fm.getKey(NUMER_WNIOSKU_CN) != null)
			{
				numerwniosku = ((List<Long>) fm.getKey(NUMER_WNIOSKU_CN)).get(0);
			}
			if (fm.getKey(NUMER_UMOWY_CN) != null)
			{
				numwerUmowy = ((List<Long>) fm.getKey(NUMER_UMOWY_CN)).get(0);;
			}
			if (fm.getKey(KLIENT_CN) != null)
			{
				Contractor con = Contractor.findById((Long) fm.getKey(KLIENT_CN));
				if (con.getRegion() != null)
					region = con.getRegion().getCn();
				if (con.getPatrons()!= null)
				{
					for (Patron p : con.getPatrons())
					{
						users.add(p.getUsername());
					}
				}
			}
			users.add(document.getAuthor());

			DlPermissionManager.setupPermission(region, numerwniosku, numwerUmowy, perms, users,document.getAuthor());
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		this.setUpPermission(document, perms);
	}

	public boolean searchCheckPermissions(Map<String, Object> values)
	{
		return true;
	}

	public boolean isPersonalRightsAllowed()
	{
		return true;
	}

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		if (document.getDocumentKind() == null)
			throw new NullPointerException("document.getDocumentKind()");

		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

		if (fm.getValue(TYP_DOKUMENTU_CN) != null)
		{

			document.setTitle(fm.getValue(TYP_DOKUMENTU_CN).toString());
		}
		else
		{
			document.setTitle("Dokumenty leasingowe");
		}

		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Dokumenty leasingowe");

		Contractor con = null;
		if (fm.getKey(KLIENT_CN) != null)
		{
			con = Contractor.findById((Long) fm.getKey(KLIENT_CN));
		}
		String nazwa = "";
		if (con != null)
		{
			nazwa = con.getName();
			nazwa = nazwa.toLowerCase();
			folder = folder.createSubfolderIfNotPresent(FolderInserter.to1Letters(nazwa));
			folder = folder.createSubfolderIfNotPresent(con.getName());
			folder.setAnchor(true);
			if (fm.getValue(RODZAJ_DOKUMENTU_CN) != null)
			{
				folder = folder.createSubfolderIfNotPresent(fm.getValue(RODZAJ_DOKUMENTU_CN).toString());

				if (fm.getEnumItemCn(RODZAJ_DOKUMENTU_CN).toString().equals(RD_DOKUMENTY_UMOWY_KLIENTA_CN))
				{
					// List<DlContractDictionary> list =
					// (List<DlContractDictionary>)
					// fm.getValue(NUMER_UMOWY_CN);
					// if (list.size() > 0)
					// folder =
					// folder.createSubfolderIfNotPresent(list.get(0).getNumerUmowy());
				}
			}
		}
		document.setFolder(folder);
	}

	public boolean canReadDocumentDictionaries() throws EdmException
	{
		return DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_ODCZYTYWANIE);
	}

	public PermissionPolicy getPermissionPolicy()
	{
		return new DlPermissionPolicy();
	}

	public int getPermissionPolicyMode()
	{
		return PermissionManager.DL_POLICY;
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		log.info("DokumentKind: {}, DocumentId: {}, Values from JavaScript: {}", fm.getDocumentKind().getCn(), fm.getDocumentId(), values);

		try
		{

			String wniosekId = "", umowaId = "";
			if (values.get("DWR_NUMER_WNIOSKU") != null && values.get("DWR_NUMER_WNIOSKU").getData() != null)
			{
				Map<String, FieldData> dvalues = values.get("DWR_NUMER_WNIOSKU").getDictionaryData();
				for (String o : dvalues.keySet())
				{
					if (o.equals("id") && !dvalues.get(o).getData().toString().equals(""))
					{
						log.info("DWR_NUMER_WNIOSKU: {}", dvalues);
						wniosekId = dvalues.get(o).getData().toString();
						// set Klient
						DSApi.openAdmin();
						getFromDatabase(wniosekId, values, "IDKLIENTA", "DWR_KLIENT", "DL_APPLICATION_DICTIONARY");
						getFromDatabase(wniosekId, values, "CONTRACT", "DWR_NUMER_UMOWY", "DL_APPLICATION_DICTIONARY");
						DSApi.close();
					}
				}
			}
			if (values.get("DWR_NUMER_UMOWY") != null && values.get("DWR_NUMER_UMOWY").getData() != null && values.get("DWR_NUMER_UMOWY").getData() instanceof Map)
			{
				Map<String, FieldData> dvalues = values.get("DWR_NUMER_UMOWY").getDictionaryData();
				log.info("DWR_NUMER_UMOWY: {}", dvalues);
				for (String o : dvalues.keySet())
				{
					if (o.equals("id") && !dvalues.get(o).getData().toString().equals(""))
					{
						umowaId = dvalues.get(o).getData().toString();
						// set Klient
						DSApi.openAdmin();
						getFromDatabase(umowaId, values, "IDKLIENTA", "DWR_KLIENT", "DL_CONTRACT_DICTIONARY");
						getFromDatabase(umowaId, values, "APPLICATION", "DWR_NUMER_WNIOSKU", "DL_CONTRACT_DICTIONARY");
						DSApi.close();
					}
				}
			}

		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return null;
	}

	private void getFromDatabase(String baseId, Map<String, FieldData> values, String what, String dwrName, String tableName) throws EdmException
	{

		if (baseId == null)
			return;

		Statement stat = null;
		ResultSet result = null;
		try
		{

			stat = DSApi.context().createStatement();
			String select = "select " + what + " from " + tableName + " where id = " + baseId;
			log.info("getFromDatabase select " + select);
			result = stat.executeQuery(select);
			while (result.next())
			{
				Long id = result.getLong(1);
				log.info("znalezione id" + id.toString() + "ddd");
				values.put(dwrName, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.LONG, id));

			}
		}
		catch (Exception ie)
		{
			log.error(ie.getMessage(), ie);
		}
		finally
		{
			DSApi.context().closeStatement(stat);
		}
	}
	
	public static List<EnumItem> getEnumItms(DocumentKind kind) throws EdmException
	{
		DataBaseEnumField f = (DataBaseEnumField) kind.getFieldByCn(TYP_DOKUMENTU_CN);
		List<EnumItem> types = f.getEnumItems(true);
        
        return types;
	}
	
	public static Integer getImportKind(Integer type) throws EntityNotFoundException
	{
		DockindInfoBean dif = DocumentKind.getDockindInfo(DocumentLogicLoader.DL_KIND);
		String types = dif.getProperties().get("atta-ass-pdf");
		String cn = dif.getDockindFieldsMap().get(TYP_DOKUMENTU_CN).getEnumItem(type).getCn();
		if(types.contains(cn))
		{
			return ImageKit.IMPORT_KIND_PDF;
		}
		return ImageKit.IMPORT_KIND_GRAY_TIFF;
	}
    public String getInOfficeDocumentKind()
    {
        return "Dokument leasingowy";
    }

}
