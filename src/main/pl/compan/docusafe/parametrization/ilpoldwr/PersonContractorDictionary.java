package pl.compan.docusafe.parametrization.ilpoldwr;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dictionary.DictionaryUtils;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.PersonDictionary;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

public class PersonContractorDictionary extends PersonDictionary {
    private BiMap<String, String> personToContractor = HashBiMap.create(new HashMap<String, String>() {{
        put("ORGANIZATION", "NAME");
        put("ZIP", "CODE");
        put("LOCATION", "CITY");
    }});

    private enum KeyConversion {
        PERSON_TO_CONTRACTOR,
        CONTRACTOR_TO_PERSON
    }

    private <T> Map<String, T> renameKeys(Map<String, T> values, KeyConversion keyConversion, String personDicName, String contractorDicName) {
        checkNotNull(personDicName);
        checkNotNull(contractorDicName);
        Map<String, T> converted = Maps.newHashMap();

        for (Map.Entry<String, T> entry : values.entrySet()) {
            String cn = entry.getKey();
            T value = entry.getValue();

            switch (keyConversion) {
                case PERSON_TO_CONTRACTOR: {
                    String shortCn = DictionaryUtils.getFieldCnWithoutDictionaryPrefix(personDicName, cn);
                    if (personToContractor.containsKey(shortCn)) {
                        shortCn = personToContractor.get(shortCn);
                    }
                    cn = DictionaryUtils.getFieldCnWithDictionaryPrefix(contractorDicName, shortCn);
                }
                break;
                case CONTRACTOR_TO_PERSON: {
                    String shortCn = DictionaryUtils.getFieldCnWithoutDictionaryPrefix(contractorDicName, cn);
                    if (personToContractor.containsValue(shortCn)) {
                        shortCn = personToContractor.inverse().get(shortCn);
                    }
                    cn = DictionaryUtils.getFieldCnWithDictionaryPrefix(personDicName, shortCn);
                }
                break;
            }

            converted.put(cn, value);
        }

        return converted;
    }

    private void markAsContractor(List<Map<String, Object>> values) {
        String idWithPrefix = DictionaryUtils.getFieldCnWithDictionaryPrefix(this.getName(), "id");
        String lParamWithPrefix= DictionaryUtils.getFieldCnWithDictionaryPrefix(this.getName(), "LPARAM");
        for (Map<String, Object> singleResult : values) {
            singleResult.put(DictionaryUtils.getFieldCnWithDictionaryPrefix(this.getName(), "CONTRACTOR_PRESENT"), true);
            singleResult.put(lParamWithPrefix, singleResult.get(idWithPrefix));
        }
    }

    @Override
    public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset, boolean fromDwrSearch,
    		Map<String, FieldData> dockindFields) throws EdmException {
        if(fromDwrSearch) {
            DwrDictionaryBase contractorDic = DwrDictionaryFacade.getDwrDictionary(IlpolCokLogic.DOCKIND_NAME, "KLIENT");
            Map<String, FieldData> contractorVals = renameKeys(values, KeyConversion.PERSON_TO_CONTRACTOR, this.getName(), contractorDic.getName());
            List<Map<String, Object>> contractorResults = contractorDic.search(contractorVals, limit, offset, fromDwrSearch, dockindFields);
            if(!contractorResults.isEmpty()) {
                List<Map<String, Object>> results = Lists.newArrayList();
                for (Map<String, Object> singleResult : contractorResults) {
                    results.add(renameKeys(singleResult, KeyConversion.CONTRACTOR_TO_PERSON, this.getName(), contractorDic.getName()));
                }
                markAsContractor(results);
                return results;
            }
        }

        // puste lub chodzi o kontkretny wpis z id - szukamy w ds_person
        List<Map<String, Object>> results = super.search(values, limit, offset, fromDwrSearch, dockindFields);

        return results;
    }
}
