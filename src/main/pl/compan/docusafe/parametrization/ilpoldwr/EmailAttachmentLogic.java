package pl.compan.docusafe.parametrization.ilpoldwr;

import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.fileupload.FileItem;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.dwr.AttachmentValue;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DocumentMultipleAttachmentFieldLogic;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.field.FieldValue;
import pl.compan.docusafe.service.fax.IncomingFax;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EmailAttachmentLogic extends DocumentMultipleAttachmentFieldLogic {
    @Override
    public void persist(Document doc, Field field, List<FileItem> fileItems) throws EdmException, IOException {
        //  tworzymy nowe zalaczniki z pikow dodanych przez uzytkownika i wiazemy je z dokumentem
        super.persist(doc, field, fileItems);

        List<Long> attIds = new ArrayList<Long>();
        DwrDictionaryBase dic = DwrDictionaryFacade.getDwrDictionaryBase(ServletActionContext.getRequest(), "ATTACHMENT");
        for(Attachment att : savedAtts) {
            Map<String, FieldData> values = Maps.newHashMap();
            AttachmentValue attVal = new AttachmentValue(att);
            values.put("ATT", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.ATTACHMENT, attVal));
            long result = dic.add(values);
            // dodajemy do tabeli slownika zalacznikow te z nich, ktore zostaly utworzone i powiazane z dokumentem
            if(result != -1)
                attIds.add(result);
        }
        /*
        //  jesli utworzono zalaczniki i powiazano je z dokumentem
        if(!savedAtts.isEmpty()) {
            Map<String, Object> vals = Maps.newHashMap();
            vals.put("ATTACHMENT", attIds);
            //  zapisujemy informacje o wczesniej utworzonych zalacznikach do tabeli multiple powiazanej ze slownikiem zalacznikow
            doc.getFieldsManager().getDocumentKind().setOnly(doc.getId(), vals);
        }
        */
    }

}
