package pl.compan.docusafe.parametrization.ilpoldwr.jbpm;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.imports.ImportEntryStatus;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.ilpoldwr.CokUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NormalOutCompletionDecisionHandler implements DecisionHandler {

    private static final long serialVersionUID = 1L;

    private final static Logger LOG = LoggerFactory.getLogger(NormalOutCompletionDecisionHandler.class);
    private final String INCOMPLETE = "incomplete";
    private final String COMPLETE = "complete";

    private static List<String> fieldsToValidate;

    static{
        fieldsToValidate = new ArrayList<String>();
        fieldsToValidate.add("ILPOL_KIND");
        fieldsToValidate.add("DOC_DATE");
        fieldsToValidate.add("DOC_DESCRIPTION");
        fieldsToValidate.add("DOC_DELIVERY");
        fieldsToValidate.add("SENDER_HERE");
        fieldsToValidate.add("RECIPIENT");
    }

    @Override
    public String decide(OpenExecution openExecution) {
        try {
            //LOG.info("ILPOL_IMPORT_PROCESS: Decision handler");
            LoggerFactory.getLogger("ilpol_doc_out_import").info("ILPOL_IMPORT_PROCESS: Decision handler");
            Long docId = Long.valueOf(String.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY)));
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            //LOG.info("documentKind = " + fm.getDocumentKind().getCn());
            if(!validateDocumentCompletion(fm)){
               /*
                JBPMTaskSnapshot.updateByDocument(doc);
                addToHistory(openExecution, doc,
                        String.valueOf(openExecution.getVariable(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM)),
                        String.valueOf(openExecution.getVariable(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM)));
                */
                return INCOMPLETE;
            }else{
                updateAssignee(openExecution, fm);
                JBPMTaskSnapshot.updateByDocument(doc);
                return COMPLETE;
            }
        } catch (EdmException ex) {
            LOG.error(ex.getMessage(), ex);
            return INCOMPLETE;
        }
    }


    /**
     * Weryfkikujemy kompletno�� danych wprowadzonych na dokumencie
     * @param fm
     * @return true je�li poprawnie wype�niono wszystkie pola, false w przeciwnym razie
     * @throws EdmException
     */
    private boolean validateDocumentCompletion(FieldsManager fm) throws EdmException {
        //LOG.info("ILPOL_IMPORT_PROCESS: validateDocumentCompletion");
        LoggerFactory.getLogger("ilpol_doc_out_import").info("ILPOL_IMPORT_PROCESS: validateDocumentCompletion");
        for(Field dockindField : fm.getFields()){
            if(fieldsToValidate.contains(dockindField.getCn())){
                LoggerFactory.getLogger("ilpol_doc_out_import").info("ILPOL_IMPORT_PROCESS: " + dockindField.getCn() + " = " + fm.getValue(dockindField.getCn()));
                //LOG.info("ILPOL_IMPORT_PROCESS: " + dockindField.getCn() + " = " + fm.getValue(dockindField.getCn()));
                if(fm.getValue(dockindField.getCn()) == null || StringUtils.isBlank(String.valueOf(fm.getValue(dockindField.getCn())))){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * W przypadku gdy wszystkie pola dokumentu zosta�y wype�nione dekretujemy dokument z osoby, kt�ra wykona�a import na dzia�, kt�ry jest powi�zany z rodzajem pisma wybranym na dokumencie
     * @param openExecution
     * @param fm
     * @throws EdmException
     */
    private void updateAssignee(OpenExecution openExecution, FieldsManager fm) throws EdmException {
        EnumItem enumItem;
        String guid;
        if((enumItem = fm.getEnumItem("ILPOL_KIND")) != null){
            if((guid = new CokUtils.Assignment(enumItem).getDivisionGuid()) != null){
                openExecution.setVariable(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM, guid);
                openExecution.setVariable(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM, null);
            }
        }
    }

    public static void addToHistory(OpenExecution openExecution, OfficeDocument doc, String username, String guid) throws EdmException, UserNotFoundException {
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setSourceUser(username);
        ahe.setProcessName(openExecution.getProcessDefinitionId());
        //proces jest dekretowany albo na osobe importujaca dokument (w przypadku braku kompletnosci wszystkich danych) badz na dzial powiazany z wprowadzonym rodzajem pisma
        if (username == null) {
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_DIVISION);
            ahe.setTargetGuid(DSDivision.find(guid).getName());
        } else {
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_SINGLE_USER);
            ahe.setTargetUser(username);
        }
        DSDivision[] divs = DSUser.findByUsername(username).getOriginalDivisionsWithoutGroup();
        if (divs != null && divs.length > 0)
            ahe.setSourceGuid(divs[0].getName());
        ahe.setCtimeAndCdate(new Date());
        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
        /*
        try {
            String status = doc.getFieldsManager().getEnumItem("STATUS").getTitle();
            ahe.setStatus(status);
        } catch (Exception e) {
            LOG.warn(e.getMessage());
        }
        */
//        doc.addAssignmentHistoryEntry(ahe, true, openExecution.getActivity());
        doc.addAssignmentHistoryEntry(ahe);
    }
}
