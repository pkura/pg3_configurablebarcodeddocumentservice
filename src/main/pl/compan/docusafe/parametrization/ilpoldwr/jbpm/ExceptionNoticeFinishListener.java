package pl.compan.docusafe.parametrization.ilpoldwr.jbpm;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jbpm4.FinishListener;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 02.08.13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */
public class ExceptionNoticeFinishListener extends FinishListener {
    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        LoggerFactory.getLogger("krzysiekm").debug("NOTIFING ABOUT EXCEPTION");
        Object exceptionNotice = execution.getVariable(FinishProcessInterceptor.EXCEPTION_NOTICE);
        if(exceptionNotice != null && exceptionNotice instanceof Throwable)
            throw new EdmException((Throwable) execution.getVariable(FinishProcessInterceptor.EXCEPTION_NOTICE));
    }
}
