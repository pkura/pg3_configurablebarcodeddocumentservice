package pl.compan.docusafe.parametrization.ilpoldwr.jbpm;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import java.util.Date;


public class DocumentOutAssignmentHandler implements AssignmentHandler {
    private static Logger LOG = LoggerFactory.getLogger(DocumentOutAssignmentHandler.class);

    public final static String ASSIGN_USER_PARAM = "assignee";
    public final static String ASSIGN_DIVISION_GUID_PARAM = "guid";

    public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
        String username = (String) openExecution.getVariable(ASSIGN_USER_PARAM);
        String guid = (String) openExecution.getVariable(ASSIGN_DIVISION_GUID_PARAM);
        Long docId = Long.valueOf(String.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY)));
        OfficeDocument doc = OfficeDocument.find(docId);
        LOG.info("trying to assign, execution params : assignee = '{}', guid = '{}'", username, guid);

        if(username == null){
            if(guid == null) {
                LOG.error("fast assignment params not set - assigning 'admin'");
                assignable.addCandidateUser("admin");
            } else {
                for(String g: guid.split(",")){
                    try {
                        DSDivision.find(g);
                    } catch (DivisionNotFoundException ex) {
                        LOG.error("guid '{}' not found - assigning 'admin'", g);
                        assignable.addCandidateUser("admin");
                    }
                    assignable.addCandidateGroup(g);
                }
            }
        } else {
            try {
                DSUser.findByUsername(username);
                assignable.addCandidateUser(username);
            } catch (UserNotFoundException ex) {
                LOG.error("user '{}' not found - assigning 'admin'", username);
                assignable.addCandidateUser("admin");
            }
        }
        addToHistory(openExecution, doc, username, guid);
    }

    public static void addToHistory(OpenExecution openExecution, OfficeDocument doc, String username, String guid) throws EdmException, UserNotFoundException {
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setSourceUser(username);
        ahe.setProcessName(openExecution.getProcessDefinitionId());
        //proces jest dekretowany albo na osobe importujaca dokument (w przypadku braku kompletnosci wszystkich danych) badz na dzial powiazany z wprowadzonym rodzajem pisma
        if (username == null) {
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_DIVISION);
            ahe.setTargetGuid(DSDivision.find(guid).getName());
        } else {
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_SINGLE_USER);
            ahe.setTargetUser(username);
        }
        DSDivision[] divs = DSUser.findByUsername(username).getOriginalDivisionsWithoutGroup();
        if (divs != null && divs.length > 0)
            ahe.setSourceGuid(divs[0].getName());
        ahe.setCtimeAndCdate(new Date());
        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
        /*
        try {
            String status = doc.getFieldsManager().getEnumItem("STATUS").getTitle();
            ahe.setStatus(status);
        } catch (Exception e) {
            LOG.warn(e.getMessage());
        }
        */
        doc.addAssignmentHistoryEntry(ahe);
    }
}
