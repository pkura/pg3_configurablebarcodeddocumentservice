package pl.compan.docusafe.parametrization.ilpoldwr.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.ilpoldwr.IlpolCokCaseLogic;
import pl.compan.docusafe.parametrization.ilpoldwr.IlpolCokLogic;
import pl.compan.docusafe.parametrization.ilpoldwr.IlpolCokTaskLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 01.08.13
 * Time: 09:22
 * To change this template use File | Settings | File Templates.
 */
public class FinishProcessInterceptor implements DecisionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(FinishProcessInterceptor.class);
    public static final String EXCEPTION_NOTICE = "EXCEPTION_NOTICE";

    @Override
    public String decide(OpenExecution openExecution) {
        Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        String returnedDecision = null;
        Document document = null;
        try {
            document = Document.find(docId);
            FieldsManager fm = document.getFieldsManager();
            DocumentKind documentKind = document.getDocumentKind();
            DocumentLogic documentLogic = documentKind.logic();
            DSUser noticeReceiver = null;
            Map<String, Object> fieldValues = new HashMap<String, Object>();
            Integer finishStatus = null;

            //  jesli zakonczamy zadanie i jest ono powiazane ze sprawa, to identyfikujemy wlasciciela sprawy
            if(document.getDocumentKind().getCn().trim().equals(IlpolCokTaskLogic.DOCKIND_NAME)){
                Object caseId = ((Map)fm.getValue("CASE_ID")).get("id");
                if(caseId != null){
                    Document connectedCaseDocument = Document.find(Long.parseLong(caseId.toString()), false, null);
                    if(connectedCaseDocument != null){
                        try{
                            if(documentLogic instanceof IlpolCokTaskLogic){
                                noticeReceiver = DSUser.findByUsername(((OfficeDocument)document).getAssignmentHistory().get(((OfficeDocument)document).getAssignmentHistory().size()-1).getSourceUser());
                                ((IlpolCokTaskLogic)documentLogic).sendEmailNotice(docId, noticeReceiver.getWholeName());
//                                LoggerFactory.getLogger("krzysiekm").debug("ILPOL: Wys�ano powiadomienie email o zako�czeniu zadania o id " + docId);
                            }
                        }catch(Exception e){
                            LOG.debug("ILPOL: Niepowiod�o si� wys�anie powiadomienia o zako�czeniu zadania o id " + docId + ": " + e.getMessage(), e);
//                            LoggerFactory.getLogger("krzysiekm").debug("ILPOL: Niepowiod�o si� wys�anie powiadomienia o zako�czeniu zadania o id " + docId + ": " + e.getMessage());
                        }
                    }
                }
            }

            Field field = fm.getField("STATUS");
            if(field != null && field instanceof DataBaseEnumField){
                if(document.getDocumentKind().getCn().trim().equals(IlpolCokLogic.DOCKIND_NAME)){
                }else if(document.getDocumentKind().getCn().trim().equals(IlpolCokTaskLogic.DOCKIND_NAME)){
                    fieldValues.put("STATUS", IlpolCokTaskLogic.ZADANIE_ZAKONCZONE);
                }else if(document.getDocumentKind().getCn().trim().equals(IlpolCokCaseLogic.DOCKIND_NAME)){
                    fieldValues.put("STATUS", IlpolCokCaseLogic.SPRAWA_ZAKONCZONA);
                }
            }

            field = fm.getField("FINISH_DATETIME");
            if(field != null){
                fieldValues.put("FINISH_DATETIME", Calendar.getInstance().getTime());
            }

            fm.getDocumentKind().setWithHistory(fm.getDocumentId(), fieldValues, false);
            if(returnedDecision == null){
                returnedDecision = "toEnd";
                DataMartManager.addHistoryEntry(document.getId(), Audit.create(DataMartDefs.BIP_DOCUMENT_INFO, DSApi.context().getPrincipalName(), "Zako�czono prac� z dokumentem"));
            }
        } catch (EdmException e) {
            LOG.error(e.getMessage(), e);
            LoggerFactory.getLogger("krzysiekm").debug("EdmException " + e.getMessage(), e);
            openExecution.setVariable(EXCEPTION_NOTICE, e);
            returnedDecision = "toEndWithExceptionNotice";
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            LoggerFactory.getLogger("krzysiekm").debug("Exception " + e.getMessage(), e);
            openExecution.setVariable(EXCEPTION_NOTICE, e);
            returnedDecision = "toEndWithExceptionNotice";
        }finally{
            if(document != null){
                LoggerFactory.getLogger("krzysiekm").debug("Uko�czono zadanie dla " + document.getDocumentKind().getCn() + " o id " + document.getId());
                LOG.info("Uko�czono zadanie dla " + document.getDocumentKind().getCn() + " o id  " + document.getId());
            }
            return returnedDecision;
        }
    }
}
