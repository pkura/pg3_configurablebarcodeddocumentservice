package pl.compan.docusafe.parametrization.ilpoldwr.jbpm;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.jbpm4.FinishListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.util.StringManager;

import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

public class CokCaseFinishListener extends FinishListener {
    private final static StringManager sm = StringManager.getManager(CokCaseFinishListener.class.getPackage().getName());

    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        Long docId = Long.parseLong(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY).toString());
        checkNotNull(docId);
        Document doc = Document.find(docId);
        checkNotNull(doc);
        FieldsManager fm = doc.getFieldsManager();

        List<Map<String, Object>> values = (List<Map<String, Object>>) fm.getValue("TASK_DOCUMENT");

        boolean closeable = true;
        if(values != null) {
            for(Map<String, Object> dicItem : values) {
                EnumValues enumVals = (EnumValues) dicItem.get("TASK_DOCUMENT_STATUS");

                Long selectedId = Long.parseLong(enumVals.getSelectedId());
                if(!selectedId.equals(5L)) {
                    closeable = false;
                    break;
                }
            }
        }

        if(closeable) {
            super.notify(execution);
        } else
            throw new EdmException(sm.getString("CannotCloseCokCase"));
        //super.notify(execution);
    }
}
