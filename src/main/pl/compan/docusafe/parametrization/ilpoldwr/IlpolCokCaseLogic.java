package pl.compan.docusafe.parametrization.ilpoldwr;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContextFactory;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.DictionaryUtils;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.sql.AcceptanceDivisionImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.sql.SQLException;
import java.util.*;

public class IlpolCokCaseLogic extends AbstractDocumentLogic{
    private final static StringManager sm = StringManager.getManager(IlpolCokCaseLogic.class.getPackage().getName());
    private final static Logger log = LoggerFactory.getLogger(IlpolCokCaseLogic.class);

    public static final String DOCKIND_NAME = "cok_case_doc";
    public final static String DOCKIND_TABLE_NAME = "DSC_ILPOL_CASE_DOC";
    public static final String TASK_DICTIONARY_FIELD_CN = "TASK_DOCUMENT";
    public static final String TASK_DICTIONARY_FIELD_TABLE_NAME = "DSC_ILPOL_TASK_DOC";
    public static final String DWR_CREATE_TASK_BUTTON = "DWR_CREATE_TASK_BUTTON";
    public static final String DWR_CREATE_NEW_TASK_BUTTON = "DWR_CREATE_NEW_TASK_BUTTON";
    public static final Integer SPRAWA_ZAKONCZONA = new Integer(5);

    private Object assignee;


    @Override
    public void documentPermissions(Document document) throws EdmException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
        super.onStartProcess(document);    //To change body of overridden methods use File | Settings | File Templates.

        try
        {
            DSUser author = DSUser.findByUsername(document.getCreatingUser());
            String user = author.getName();
            String fullName = author.getLastname() + " " + author.getFirstname();

            Map<String, Object> map = Maps.newHashMap();
            //wartosci odpowiadajace za ustawienie osoby dekretujacej zadanie
            map.put("previousAssignee", user);
            map.put("currentAssignee", user);
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

            java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
            perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
            perms.addAll(documentPermissions);
            this.setUpPermission(document, perms);

            DSApi.context().watch(URN.create(document));
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
            throw new EdmException(e.getMessage());
        }
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
    }

    @Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        super.validateDwr(values, fm);

        Map.Entry<String, FieldData> sender = DwrUtils.getSenderField(values);
        String senderFieldCn = sender != null ? sender.getKey() : "";
        if(senderFieldCn.equals("DWR_GENERATE_INTERNAL_ID_BUTTON")) {
            String selectedAreaId = DwrUtils.getStringValue(values, "DWR_AREA");
            if (StringUtils.isEmpty(selectedAreaId)) {
                System.out.println("EMPTY");
                return new Field(sm.getString("SelectAreaToGenerateCaseNumber"), "MSG");
            } else {
                int areaId = Integer.parseInt(selectedAreaId);
                try {
                    String caseString = DwrUtils.getStringValue(values, "DWR_INTERNAL_ID");
                    CaseNumber caseNumber;
                    try {
                        caseNumber = new CaseNumber(caseString);

                    } catch (IllegalArgumentException e) {
                        DSApi.openAdmin();
                        caseNumber = CokUtils.generateCaseNumber(areaId);
                        DSApi._close();
                    }
                    FieldData internalId = values.get("DWR_INTERNAL_ID");
                    internalId.setStringData(caseNumber.toString());
                    FieldData ctime = values.get("DWR_CTIME");
                    ctime.setDateData(caseNumber.getDate());
                } catch (SQLException e) {
                    log.error(e.getMessage(), e);
                } catch (EdmException e) {
                    log.error(e.getMessage(), e);
                }
            }
        } else if(senderFieldCn.equals(DWR_CREATE_TASK_BUTTON)) {
            // otwarto popup Nowe zadanie
            CokUtils.copyAreaValues(values, "DWR_NEW_TASK_");
            CokUtils.copyContractorValue(values, "DWR_CONTRACTOR", "DWR_NEW_TASK_CONTRACTOR");
            setDeadlineDateAsLeatesTodaysTime(values, "DWR_NEW_TASK_");
            values.get("DWR_NEW_TASK_CREATION_DATETIME").setData(new Date());
        } else if(senderFieldCn.equals(DWR_CREATE_NEW_TASK_BUTTON)){
            // klikni�to Utw�rz zadanie na popupie
            Long caseDocId = fm.getDocumentId();

            if(caseDocId == null){
                return new Field("Nowe zadanie mo�na utworzy� wy��cznie w ju� istniej�cej sprawie", "MSG");
            }else{
                InOfficeDocument newTaskDoc = (InOfficeDocument)CokUtils.createDocumentFromPopup(fm, values, CokUtils.COK_TASK);
                DSApi.open(AuthUtil.getSubject(WebContextFactory.get().getHttpServletRequest()));
                DSApi.context().begin();
                Document doc = Document.find(fm.getDocumentId());
                DictionaryUtils.addToMultipleTable(doc, TASK_DICTIONARY_FIELD_CN, newTaskDoc.getId().toString());
                DataMartManager.addHistoryEntry(caseDocId,
                        Audit.create(DataMartDefs.DOCUMENT_CREATE, DSApi.context().getPrincipalName(), sm.getString("UtworzonoZadanieICS")));
                DSApi.context().commit();
                DSApi._close();
                return new Field("cn", "Utworzono nowe zadanie dekretuj�c na " + newTaskDoc.getRecipientUser(), Field.Type.STRING);
            }
        }
        return null;
    }

    public void onStartProcess(OfficeDocument document, ActionEvent event, Object assignee) throws EdmException {
        this.assignee = assignee;
        onStartProcess(document, null);
    }

    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
        if (acceptationCn != null) {
            try {
                Object assignee = null;
                DocumentLogic logic = doc.getDocumentKind().logic();
                if(logic instanceof IlpolCokCaseLogic){
                    assignee = ((IlpolCokCaseLogic)logic).getAssignee();
                }
                String id = null;

                try {
                    if(assignee instanceof DSUser){
                        id = ((DSUser) assignee).getName();
                        assignable.addCandidateUser(id);
                        addToHistory(openExecution, doc, id, null);
                    }else if(assignee instanceof DSDivision){
                        id = ((DSDivision) assignee).getGuid();
                        assignable.addCandidateGroup(id);
                        addToHistory(openExecution, doc, null, id);
                    }else if(assignee instanceof AcceptanceDivisionImpl){
                        return false;
                    }else if(assignee == null){
                        return false;
                    }
                    log.info("For fieldValue " + id);
                } catch (UserNotFoundException e) {
                    log.error(e.getMessage());
                }
                return true;
            } catch (EdmException e1) {
                log.error(e1.getMessage());
            }
        }
        return false;
    }

    public static void addToHistory(OpenExecution openExecution, OfficeDocument doc, String user, String guid) throws EdmException, UserNotFoundException {
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setSourceUser(user);
        ahe.setProcessName(openExecution.getProcessDefinitionId());
        if (user == null) {
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_DIVISION);
            ahe.setTargetGuid(DSDivision.find(guid).getName());
        } else {
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_SINGLE_USER);
            ahe.setTargetUser(user);
        }
        DSDivision[] divs = DSUser.findByUsername(user).getOriginalDivisionsWithoutGroup();
        if (divs != null && divs.length > 0)
            ahe.setSourceGuid(divs[0].getName());
        ahe.setCtime(new Date());
        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
        try {
            String status = doc.getFieldsManager().getEnumItem("STATUS").getTitle();
            ahe.setStatus(status);
        } catch (Exception e) {
            log.warn(e.getMessage());
        }
        doc.addAssignmentHistoryEntry(ahe);

    }

    private void setDeadlineDateAsLeatesTodaysTime(Map<String, FieldData> values, String newValPrefix){
        FieldData deadlineDateField = values.get(newValPrefix + "DEADLINE_DATETIME");
        Calendar endOfCurrentDayDate = Calendar.getInstance();
        endOfCurrentDayDate.clear(Calendar.HOUR_OF_DAY);
        endOfCurrentDayDate.clear(Calendar.MINUTE);
        endOfCurrentDayDate.set(
                endOfCurrentDayDate.get(Calendar.YEAR),
                endOfCurrentDayDate.get(Calendar.MONTH),
                endOfCurrentDayDate.get(Calendar.DAY_OF_MONTH),
                endOfCurrentDayDate.getMaximum(Calendar.HOUR_OF_DAY),
                endOfCurrentDayDate.getMaximum(Calendar.MINUTE)
        );
        Date deadlineDateTime = endOfCurrentDayDate.getTime();
        deadlineDateField.setDateData(deadlineDateTime);
    }


    /*
        cok_case_dok.column.dockindBusinessAtr1 = Status Sprawy
        cok_case_dok.column.dockindBusinessAtr2 = Numer Sprawy
        cok_case_dok.column.dockindBusinessAtr3 = Temat
        cok_case_dok.column.dockindBusinessAtr4 = Kontrahent
    */

    @Override
    public TaskListParams getTaskListParams(DocumentKind kind, long documentId, TaskSnapshot task) throws EdmException {
        TaskListParams params = new TaskListParams();
        String caseStatus = "brak";
        String caseInternalNumber = "brak";
        String caseSubject = "brak";
        String contractorName = "brak";
        String contractorNip = "brak";

        try {
            FieldsManager fm = kind.getFieldsManager(documentId);
            if(fm.getValue("STATUS") != null)
                caseStatus = fm.getValue("STATUS").toString();
            if(fm.getValue("INTERNAL_ID") != null)
                caseInternalNumber = fm.getValue("INTERNAL_ID").toString();
            if(fm.getValue("COK_DOCUMENT") != null && ((List)fm.getValue("COK_DOCUMENT")).size() > 0)
                caseSubject = ((Map)((List)fm.getValue("COK_DOCUMENT")).get(0)).get("COK_DOCUMENT_SUBJECT").toString();
            if(fm.getValue("CONTRACTOR") != null) {
                try {
                    Map<String, Object> contractorDic = (Map<String, Object>) fm.getValue("CONTRACTOR");
                    if(contractorDic.get("CONTRACTOR_NAME") != null) {
                        contractorName = contractorDic.get("CONTRACTOR_NAME").toString();
                    }
                    if(contractorDic.get("CONTRACTOR_NIP") != null) {
                        contractorNip = contractorDic.get("CONTRACTOR_NIP").toString();
                    }
                } catch(ClassCastException e) {
                    log.error(e.getMessage(), e);
                }
            }
                  } catch(Exception e) {
            log.error(e.getMessage(), e);
            LoggerFactory.getLogger("krzysiekm").debug("PARAMS ERROR " + e.getMessage());
        } finally {
            params.setAttribute(contractorName, 0);
            params.setAttribute(caseSubject, 1);
            params.setAttribute(contractorNip, 2);
            params.setAttribute(caseStatus, 3);
            params.setAttribute(caseInternalNumber, 4);
        }
        return params;
    }

    public Object getAssignee() {
        return assignee;
    }

    public void setAssignee(Object assignee) {
        this.assignee = assignee;
    }
}
