package pl.compan.docusafe.parametrization.ilpoldwr;

import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class ContractorDictionary extends DwrDictionaryBase{

	private static StringManager sm = GlobalPreferences.loadPropertiesFile(ContractorDictionary.class.getPackage().getName(),null);
	private static final Logger log = LoggerFactory.getLogger(LeasingDictionary.class);
	
	@Override
	public long add(Map<String, FieldData> values) throws EdmException
	{
		values.put("DSUSER", new FieldData(Field.Type.STRING, DSApi.context().getPrincipalName()));

		return super.add(values);
	}
	
	public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset) throws EdmException
	{
		List<Map<String, Object>> ret = Lists.newArrayList();

		if(values.containsKey("id") && values.size() == 1)
		{
			Contractor con = null;
			Long conId = Long.parseLong(values.get("id").getData().toString());
			con = Contractor.findById(conId);
    		if(con == null)
    			return ret;
    		Map<String, Object> row = Maps.newLinkedHashMap();
    		row.put("id", con.getId());
			row.put("KLIENT_NAME", con.getName());
			row.put("KLIENT_FULLNAME", con.getName());
			row.put("KLIENT_PHONENUMBER", con.getPhoneNumber());
			row.put("KLIENT_NIP", con.getNip());
			ret.add(row);
			return ret;
		}
		
		String nazwa = "";
		String phoneNumber = null;
		boolean contextOpened = true;
		try
		{
			if (values.containsKey("KLIENT_NAME") && !values.get("KLIENT_NAME").getData().toString().isEmpty())
				nazwa = values.get("KLIENT_NAME").getData().toString();
			if (values.containsKey("KLIENT_PHONENUMBER") && ! values.get("KLIENT_PHONENUMBER").getData().toString().isEmpty())
				phoneNumber = values.get("KLIENT_PHONENUMBER").getData().toString();
			
			if( (nazwa != null && nazwa.length() > 2) || (phoneNumber != null && phoneNumber.length() > 2))
			{
				if (!DSApi.isContextOpen())
				{
					DSApi.openAdmin();
					contextOpened = false;
				}
	            QueryForm form = new QueryForm(offset, 10);
	            if(nazwa != null)
	            	form.addProperty("name", nazwa);
	            if(phoneNumber != null)
	            	form.addProperty("phoneNumber", phoneNumber);
	            
	            form.addOrderAsc("name");
	            SearchResults<? extends Contractor> results = Contractor.search(form);
	            if (results != null && results.totalCount() > 0)
	            {
	            	while (results.hasNext())
	            	{
	            		Contractor con = (Contractor) results.next();
						Map<String, Object> row = Maps.newLinkedHashMap();
						row.put("id", con.getId());
						row.put("KLIENT_NAME", con.getName());
						row.put("KLIENT_FULLNAME", con.getName());
						row.put("KLIENT_PHONENUMBER", con.getPhoneNumber());
						row.put("KLIENT_NIP", con.getNip());
	    				ret.add(row);
					}
	            	return ret;
	            }
			}
		}
		catch (Exception ie)
		{
			log.error(ie.getMessage(), ie);
		}
		finally
		{
			if (!contextOpened && DSApi.isContextOpen())
				DSApi.close();
		}
		return ret;
	}
}

