package pl.compan.docusafe.parametrization.ilpoldwr;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContextFactory;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.dockinds.dwr.AttachmentValue;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;

import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class OrgAttachmentDictionary extends DwrDictionaryBase {
    private List<Long> getAttachmentIds() {
        List<Long> ids = Lists.newArrayList();

        return ids;
    }



    @Override
    public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset, boolean fromDwrSearch,
    		Map<String, FieldData> dockindFields) throws EdmException {
        System.out.println("Search " + values);

        return Collections.EMPTY_LIST;
        //return super.search(values, limit, offset, fromDwrSearch);
    }

    @Override
    public Map<String, Object> getValues(String id, Map<String, Object> fieldValues) throws EdmException {
        if(StringUtils.isEmpty(id)) {
            return Maps.newHashMap();
        }

        Attachment att = Attachment.find(Long.parseLong(id));
        Map<String, Object> ret = Maps.newHashMap();
        ret.put("id", att.getId());
        ret.put("ATT", new AttachmentValue(att));

        return ret;
    }
}
