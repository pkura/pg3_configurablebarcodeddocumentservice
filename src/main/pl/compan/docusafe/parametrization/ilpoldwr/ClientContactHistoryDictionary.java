package pl.compan.docusafe.parametrization.ilpoldwr;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.AbstractDictionaryField;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Krzysztof Modzelewski
 * Date: 29.11.13
 * Time: 09:33
 */
public class ClientContactHistoryDictionary extends DwrDictionaryBase {


    private final static Logger LOG = LoggerFactory.getLogger(ClientContactHistoryDictionary.class);


    /*
        Nazwy kolumn tabeli s�ownika
    */
    public static String ID = "ID";
    public static String AUTHOR = "AUTHOR";
    public static String CONTACT_TYPE = "CONTACT_TYPE";
    public static String CREATION_DATE = "CREATION_DATE";
    public static String CONTACT_DATE = "CONTACT_DATE";
    public static String DESCRIPTION = "DESCRIPTION";
    public static String CONTACT_STATUS = "CONTACT_STATUS";
    public static String REPAYMENT_DATE = "REPAYMENT_DATE";
    public static String DICTIONARY_NAME = "CLIENT_CONTACT_HISTORY";
    public static String DOCUMENT_ID = "DOCUMENT_ID";
    public static String EMAIL_ID = "EMAIL_ID";

    /*
        Warto�ci wyliczeniowe dla pola DataBaseEnum
    */
    public static int CONTACT_TYPE_FAKS = 1;
    public static int CONTACT_TYPE_MAIL = 2;
    public static int CONTACT_TYPE_TELEFON = 3;
    public static int CONTACT_TYPE_WIZYTA = 4;

    /*
        Dodatkowe wlasciwosci
    */
    public static String TABLE_NAME = "ILPL_COK_CLIENT_CONTACT_DIC";

    /**
     * Adres ustawiany jako e-mail adresata wiadomo�ci skierowanej z dokumentu Monitoring P�atno�ci
     */
//    public static String SENDER_EMAIL_ADDRESS = "compansys@wp.pl";

    /**
     * W trakcie wyszukiwania wpis�w w s�owniku kontakt�w sprawdzamy czy zostaly one utworzone przez aktualnie zalogowanego uzytkownika
     * jesli nie, to zwracamy pusta tablice dla tego wpisu, a nastepnie w metodzie modifyDwrFacade(...) logiki dokumentu usuwamy
     * informacje o pustym wpisie, aby nie zostal on wyswietlony na formatce uzytkownika
     * @param values
     * @param limit
     * @param offset
     * @return
     * @throws EdmException
     */
    public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset) throws EdmException
    {//sprawdzamy czy autorem wpisu w slowniku o podanym id jest uzytkownik aktualnie wczytujacy dokument z tym wpisem
        try {
            String currentLoggedUser = CokUtils.retriveUserLogin();
            PreparedStatement ps = DSApi.context().prepareStatement("SELECT AUTHOR FROM " + ((AbstractDictionaryField) getOldField()).getTable() + " WHERE ID = ?");
            ps.setString(1, values.get("id").getStringData());
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                if(currentLoggedUser.equals(rs.getString("AUTHOR"))){
                    return null;
                }else{
                    return new ArrayList<Map<String, Object>>();
                }
            }
            return null;
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public boolean filterBeforeGetValues(Map<String, Object> dictionaryFilteredFieldsValues, String dictionaryEntryId, Map<String, Object> documentValues) {
        return super.filterBeforeGetValues(dictionaryFilteredFieldsValues, dictionaryEntryId, documentValues);
    }

    @Override
    public long add(Map<String, FieldData> values) throws EdmException {
        //zapisujemy informacje o tym kto jest autorem (uzytkownikiem zalogowanym w trakcie dodawania wpisu do slownika) wpisu kontaktu do historii klienta
        //aby pozniej moc zrobic filtrowanie wyswietlanych wynikow w zaleznosci od tego kto jest autorem wpisu
        values.put(ClientContactHistoryDictionary.AUTHOR, new FieldData(Field.Type.STRING, CokUtils.retriveUserLogin()));
        return super.add(values);
    }
}
