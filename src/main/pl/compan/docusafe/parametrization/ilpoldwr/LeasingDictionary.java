package pl.compan.docusafe.parametrization.ilpoldwr;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.LinkValue;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.HTMLField;
import pl.compan.docusafe.core.dockinds.field.LinkField;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.parametrization.ilpol.DlApplicationDictionary;
import pl.compan.docusafe.parametrization.ilpol.DlLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 
 * @author Kamil Skrzypek
 */
public class LeasingDictionary extends DwrDictionaryBase
{

	private static final Logger log = LoggerFactory.getLogger(LeasingDictionary.class);
	private static String DSG_LEASING = "DSG_LEASING";
	
	public long add(Map<String, FieldData> values) throws EdmException
	{
		Map<String, Object> valuesForSet = new HashMap<String, Object>();
		boolean newDictionaryValueIsNotEmpty = false;
		// Na poczatku sprawdzamy, czy nie jest to przypadek, w ktorym slownik
		// jest caly pusty (nie wpisano zadnej wartosci)
		for (FieldData fieldData : values.values())
		{
			if ((fieldData.getData() != null) && (!fieldData.getData().equals("")))
			{
				newDictionaryValueIsNotEmpty = true;
				break;
			}
		}

		long ret = -1;

		if (!newDictionaryValueIsNotEmpty)
			return ret;

		for(String cn : values.keySet())
		{
			String newCn = cn.replace(getName(), "");
			// jesli pierwszy znak to _, np.: _STATUS_DOKUMENTU
			if(newCn.charAt(0) == '_') {
				newCn = newCn.substring(1);
			}
				
			valuesForSet.put(newCn, values.get(cn).getData());
		}
		Boolean isContextBegine = false;
		if(!DSApi.context().inTransaction())
		{
			DSApi.context().begin(); 
			isContextBegine =true;
		}
		ArrayList<Long> contracts = null;
		String numerwniosku =null;
		try
		{
			Long masterDocumentId = null;
			if(values.get("MASTER_DOC") != null && StringUtils.isNotEmpty(values.get("MASTER_DOC").getStringData()))
			{
				masterDocumentId = Long.parseLong(""+values.get("MASTER_DOC").getData());//values.get("MASTER_DOC_ID").getLongData();
				FieldsManager fmm = Document.find(masterDocumentId).getFieldsManager();
				fmm.initialize();
				numerwniosku = fmm.getKey("NUMER_WNIOSKU").toString();
				contracts = DlApplicationDictionary.getInstance().getContracts(Long.parseLong(numerwniosku));
				valuesForSet.put(LeasingLogic.NUMER_UMOWY_CN, contracts);
				valuesForSet.put(LeasingLogic.NUMER_WNIOSKU_CN, numerwniosku);
			}
		}
		catch (Exception ie)
		{
			log.error(ie.getMessage(), ie);
		}
		String rodzajDokumentu =  getName().replace("DSG_LEASING", "");
		DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.DL_KIND);
		DockindQuery dockindQuery = new DockindQuery(0, 2);
		dockindQuery.setDocumentKind(documentKind);
		dockindQuery.setCheckPermissions(false);
		dockindQuery.enumField(documentKind.getFieldByCn(DlLogic.RODZAJ_DOKUMENTU_CN), Integer.valueOf(rodzajDokumentu));
		if(valuesForSet.get("TYP_DOKUMENTU") != null	)
			dockindQuery.enumField(documentKind.getFieldByCn(DlLogic.TYP_DOKUMENTU_CN), Integer.parseInt((String) valuesForSet.get("TYP_DOKUMENTU")));
		if (numerwniosku != null)
			dockindQuery.field(documentKind.getFieldByCn(DlLogic.NUMER_WNIOSKU_CN), numerwniosku);
		if (contracts != null && !contracts.isEmpty())
			dockindQuery.field(documentKind.getFieldByCn(DlLogic.NUMER_UMOWY_CN), contracts.get(0)); 

		dockindQuery.setCheckPermissions(false);
		
		SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
		if (searchResults.count() != 0 && numerwniosku != null)
		{
			EnumItem eitm = DataBaseEnumField.getEnumItemForTable("dsg_typ", Integer.parseInt((String) valuesForSet.get("TYP_DOKUMENTU")));
			throw new EdmException("Nie mo�na doda� "+eitm.getTitle()+" (jest ju� taki dokument w teczce)");
			
		}
		
		
		String summary = "Dokument LEASINGOWY";
		Document document = null;
		document = new Document(summary, summary);
		document.setDocumentKind(documentKind);
		document.setForceArchivePermissions(false);
		document.setCtime(new Date());
		document.setFolder(Folder.getRootFolder());
		document.create();
		Long newDocumentId = document.getId();
		
		valuesForSet.put("RODZAJ_DOKUMENTU", Integer.valueOf(rodzajDokumentu));
		documentKind.setOnly(newDocumentId, valuesForSet);
		documentKind.logic().archiveActions(document, DocumentLogic.TYPE_ARCHIVE);
		documentKind.logic().documentPermissions(document);
		if(isContextBegine)
			DSApi.context().commit();
		
		ret = newDocumentId;
		return ret;
	}
	
	public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset) throws EdmException
	{
		List<Map<String, Object>> ret = Lists.newArrayList();
		// LEASING_DOC_1,DSG_LEASINGG_1,TYP_DOKUMENTU_1,STATUS_DOKUMENTU_1,PRZYCZYNA_1,OPIS_1,ATTACH_1
		String documentId = "";
		if (!(values.containsKey("id") && !(values.get("id").getData().toString()).equals("")))
			return ret;
			
		documentId = values.get("id").getData().toString();
		
		Statement stat = null;
		ResultSet result = null;
		String dictionaryName = getName();
		boolean contextOpened = true;
		try
		{
			if (!DSApi.isContextOpen())
			{
				DSApi.openAdmin();
				contextOpened = false;
			}
			stat = DSApi.context().createStatement();

			StringBuilder select = new StringBuilder("select TYP_DOKUMENTU, STATUS_DOKUMENTU, PRZYCZYNA, OPIS from DSG_LEASING where DOCUMENT_ID = ").append(documentId);
			log.info("select: " + select.toString());
			result = stat.executeQuery(select.toString());
			while (result.next())
			{
				Map<String, Object> row = Maps.newLinkedHashMap();
				Long typDokumentu = result.getLong(1);
				Long statusDokumentu = result.getLong(2);
				Long przyczyna = result.getLong(3);
				String opis = result.getString(4);
				row.put("id", documentId);

				row.put(dictionaryName + "_TYP_DOKUMENTU", typDokumentu);
				row.put(dictionaryName + "_STATUS_DOKUMENTU", statusDokumentu);
				row.put(dictionaryName + "_PRZYCZYNA", przyczyna);
				row.put(dictionaryName + "_OPIS", opis);
				ret.add(row);
			}
			
		}
		catch (Exception ie)
		{
			log.error(ie.getMessage(), ie);
		}
		finally
		{
			DSApi.context().closeStatement(stat);
			if (!contextOpened && DSApi.isContextOpen())
				DSApi.close();
		}
		return ret;
	}
	
	@Override
	public Map<String, Object> getValues(String id, Map<String, Object> fieldValues) throws EdmException
	{
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

		Map<String, Object> fieldsValues = new HashMap<String, Object>();
		try
		{
			Map<String, FieldData> values = new HashMap<String, FieldData>();
			if (id != null)
			{
				FieldData idData = new FieldData();
				idData.setData(id);
				idData.setType(Field.Type.STRING);
				values.put("id", idData);
				result = search(values, 1, 2);
			}

			for (pl.compan.docusafe.core.dockinds.field.Field field : getOldField().getFields())
			{
				if (field.getCn().equals("TYP_DOKUMENTU_1") || field.getCn().equals("STATUS_DOKUMENTU_1") || field.getCn().equals("PRZYCZYNA_1"))
				{
					Map<String, Object> enumVal = new HashMap<String, Object>();
					if (result.size() > 0 && result.get(0).containsKey(getName() + "_"+field.getCn().replace("_1", "")))
					{
						fieldsValues.put(field.getCn(), result.get(0).get(getName() + "_"+field.getCn().replace("_1", "")));
					}
					if (result.size() == 0)
					{
						enumVal.put(getName() + "_"+field.getCn().replace("_1", ""), field.getEnumItemsForDwr(fieldsValues));
						result.add(enumVal);
					}
					else
					{
						result.get(0).put(getName() + "_"+field.getCn().replace("_1", ""), field.getEnumItemsForDwr(fieldsValues));
					}
				}
				else if (field.getCn().equals("DSG_LEASINGG_1") && id != null)
				{
					Map<String, Object> enumVal = new HashMap<String, Object>();

					pl.compan.docusafe.core.dockinds.field.Field linkField  =  (LinkField)field;
					String label = "Przejd�";
					if (result.size() == 0)
					{
						enumVal.put(getName() + "_DSG_LEASINGG", new LinkValue(label, ((LinkField) linkField).getLogicField().createLink(id)));
						result.add(enumVal);
					}
					else
						result.get(0).put(getName() + "_DSG_LEASINGG", new LinkValue(label, ((LinkField) linkField).getLogicField().createLink(id)));
				}
				else if (field.getCn().equals("ATTACH_1") && id != null)
				{
					Map<String, Object> htmlVal = new HashMap<String, Object>();
					HTMLField htmlField = (HTMLField)field;

					if (result.size() == 0)
					{
						htmlVal.put(getName() + "_ATTACH_1", htmlField.getValue(id));
						result.add(htmlVal);
					}
					else
						result.get(0).put(getName()+"_ATTACH", htmlField.getValue(id));
				}
			}
			if (result.size() == 0)
				return new HashMap<String, Object>();
			return result.get(0);
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
			return null;
		}
	}
	
	
	@Override
	public int remove(String id) throws EdmException
	{
		System.out.println("USUWAM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		return 1;
	}

}
