package pl.compan.docusafe.parametrization.ilpoldwr;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.LinkValue;

public class CaseDictionary extends DwrDictionaryBase {

	@Override
	public Map<String, Object> getValues(String id, Map<String, Object> fieldValues) throws EdmException {
		Map<String, Object> vals = super.getValues(id, fieldValues);
		
		if(id == null) {
			vals.put(getName() + "_" + "CTIME", new Date());
		} else {
            if(vals.containsKey("CASE_DOC_LINK") && vals.containsKey("CASE_DOCUMENT_ID")) {
                LinkValue linkVal = (LinkValue) vals.get("CASE_DOC_LINK");
                Long documentId = ((BigDecimal)vals.get("CASE_DOCUMENT_ID")).longValue();
                Document doc = Document.find(documentId);
                linkVal.setLabel(doc.getDocumentKind().getName());

                StringBuilder link = new StringBuilder();
                String pageContext = Docusafe.getPageContext();
                if(pageContext.length() > 0)
                    link.append("/");
                link.append(pageContext).append("/office/incoming/document-archive.action?documentId=");
                link.append(String.valueOf(documentId));
                linkVal.setLink(link.toString());
            }
        }
		
		return vals;
	}

	@Override
	public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset, boolean fromDwrSearch,
			Map<String, FieldData> dockindFields) throws EdmException {
		values.remove("CASE_CTIME");
		return super.search(values, limit, offset, fromDwrSearch, dockindFields);
	}
	
}
