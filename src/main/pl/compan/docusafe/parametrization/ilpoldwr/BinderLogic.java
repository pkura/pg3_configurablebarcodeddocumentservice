package pl.compan.docusafe.parametrization.ilpoldwr;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContextFactory;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.Participation;
import org.jbpm.pvm.internal.task.TaskImpl;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.permission.DlPermissionManager;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.crm.Patron;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.jbpm4.MigrationWorkflow;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskListMarkerManager;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.parametrization.ilpol.DlContractDictionary;
import pl.compan.docusafe.parametrization.ilpol.DlLogic;
import pl.compan.docusafe.service.imports.ImpulsImportUtils;
import pl.compan.docusafe.util.*;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

public class BinderLogic extends AbstractDocumentLogic
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(BinderLogic.class);
	private static final Logger perlog = LoggerFactory.getLogger("BINDERPERLOG");

	private static BinderLogic instance;

	public static synchronized BinderLogic getInstance()
	{
		if (instance == null)
			instance = new BinderLogic();
		return instance;
	}

	public static String DS_DIVISION = "DL_BINDER_DS";
	public static String DR_DIVISION = "DL_BINDER_DR";
	public static String DO_DIVISION = "DL_BINDER_DO";
	public static String DK_DIVISION = "DL_BINDER_DK";

	public final static String[] divisionsGuid = { "DL_BINDER_DU", "DL_BINDER_DR", "DL_BINDER_DO", "DL_BINDER_DK" };
	public static String KLIENT_FIELD_CN = "KLIENT";
	public static String NUMER_UMOWY_FIELD_CN = "NUMER_UMOWY";
	public static String NUMER_WNIOSKU_FIELD_CN = "NUMER_WNIOSKU";
	public static String DOSTAWCA_FIELD_CN = "DOSTAWCA";

	// nazwy kodowe poszczeg�lnych p�l dla tego rodzaju dokumentu
	public static final String RODZAJ_DOKUMENTU_CN = "RODZAJ_DOKUMENTU";
	public static final String TYP_DOKUMENTU_CN = "TYP_DOKUMENTU";
	public static final String DATA_DOKUMENTU_CN = "DATA_DOKUMENTU";
	public static final String STATUS_DOKUMENTU_CN = "STATUS_DOKUMENTU";
	public static final String NUMER_DOKUMENTU_CN = "NUMER_DOKUMENTU";
	public static final String KWOTA_CN = "KWOTA";

	private static Long[] leasingDictionareisIndex = { 3L, 21L, 22L, 23L, 24L, 25L, 26L, 27L, 28L, 29L };

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		perlog.error("archiveActions IN");
		 if(AvailabilityManager.isAvailable("migrationWorkflow"))
			return;
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		if (fm.getKey(NUMER_WNIOSKU_FIELD_CN) == null)
			throw new EdmException("Musisz wybra� wniosek");
		String umowa = "brak";
		String wniosek = "brak";
		if (fm.getKey(NUMER_UMOWY_FIELD_CN) != null && ((Map<String, Object>) fm.getValue(NUMER_UMOWY_FIELD_CN)).get("NUMER_UMOWY_ID") != null)
		{
			umowa = ((Map<String, Object>) fm.getValue(NUMER_UMOWY_FIELD_CN)).get("NUMER_UMOWY_ID").toString();
		}
		if (fm.getKey(NUMER_WNIOSKU_FIELD_CN) != null)
		{
			Map mapa = ((Map<String, Object>) fm.getValue(NUMER_WNIOSKU_FIELD_CN));
			wniosek = ((Map<String, Object>) fm.getValue(NUMER_WNIOSKU_FIELD_CN)).get("NUMER_WNIOSKU_NUMERWNIOSKU").toString();
		}
		document.setTitle(umowa + "/" + wniosek);
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Teczki");
		Contractor con = Contractor.findById((Long) fm.getKey(KLIENT_FIELD_CN));
		if (con != null && con.getName() != null)
		{
			folder = folder.createSubfolderIfNotPresent(FolderInserter.to2Letters(con.getName()));
			folder = folder.createSubfolderIfNotPresent(con.getName());
		}
		else
		{
			folder = folder.createSubfolderIfNotPresent("Poczekalnia");
		}
		document.setFolder(folder);
		updateDocumentLeasing(fm, con.getId(), leasingDictionareisIndex);
//nie potrzebne ustawiamy numery umowy podczas tworzenia teczki, pozniej edycja numeru umowy powoduje wywalenie dokument�w  teczki 
//		if (fm.getKey(NUMER_UMOWY_FIELD_CN) != null)
//		{
//			// sprawdzic co jest potrzebne
//			ArrayList<Long> a = getNumeryUmowPowiazaneZWnioskiem(Long.parseLong("" + fm.getKey(NUMER_WNIOSKU_FIELD_CN)));
//			if (!a.contains(Long.valueOf(fm.getKey(NUMER_UMOWY_FIELD_CN).toString())))
//			{
//				a.add(Long.valueOf(fm.getKey(NUMER_UMOWY_FIELD_CN).toString()));
//			}
//			ArrayList<Long> leasingIDs = getDocumentLeasignOnlyForThisBinder(fm.getDocumentId());
//			setNumerUmowy2(Long.parseLong("" + fm.getKey(NUMER_UMOWY_FIELD_CN)), leasingIDs);
//		}
		if(getBinderByNumerWniosku((Long) fm.getKey(NUMER_WNIOSKU_FIELD_CN), null))
		{
			document.setDescription("n-Teczka");
		}
		perlog.error("archiveActions OUT");
	}
	
	
	public String getInOfficeDocumentKind()
    {
        return "Teczka";
    }
    
	
	@Override
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues,Long documentId)
	{
		perlog.error("setMultiV IN");
		Map<String, Object> results = new HashMap<String, Object>();

		try
		{
			results.put(dictionaryName+"_MASTER_DOC", documentId);
		}
		catch (Exception e)
		{
			log.error("Lapiemy to: " + e.getMessage(), e);
		}
		perlog.error("setMultiV OUT");
		return results;
	}

	/**
	 * Standardowa implementacja - wczytuje warto�ci z xml'a (szukaj
	 * 'default-value')
	 */
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		perlog.error("setInitV IN");
		Map<String, Object> toReload = Maps.newHashMap();
		for (Field f : fm.getFields())
		{
			if (f.getDefaultValue() != null)
			{
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
			}
		}
		log.info("toReload = {}", toReload);
		toReload.put("DOLACZ_DOC", "10");
		perlog.error("setInitV OUT");
		fm.reloadValues(toReload);
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		System.out.println(""+values.get("DWR_NUMER_UMOWY"));
		perlog.error("validateDWR IN");
		log.info("DokumentKind: {}, DocumentId: {}, Values from JavaScript: {}", fm.getDocumentKind().getCn(), fm.getDocumentId(), values);
		boolean isOpen = false;
		
		
		Set<Integer> selectedTypes = new HashSet<Integer>();
		Map<String,Integer> newTypes = new HashMap<String,Integer>();
		
		for (Long dicId : leasingDictionareisIndex)
		{
			Map<String, FieldData> dicdata = values.get("DWR_DSG_LEASING"+dicId).getDictionaryData();
			for (String kk : dicdata.keySet()) {
//				System.out.println(kk);
				if(!kk.contains("_TYP_DOKUMENTU_"))
					continue;
//				System.out.println("OK "+kk);
//				System.out.println("START");
//				System.out.println(kk);
////				System.out.println(dicdata.get(kk).getStringData());
//				System.out.println("DATA = " +dicdata.get(kk).getData());
				Object o = dicdata.get("DSG_LEASING"+dicId+"_ID_"+kk.substring(kk.length()-1)).getData();
				System.out.println();
				if(dicdata.get(kk).getData() != null && dicdata.get("DSG_LEASING"+dicId+"_ID_"+kk.substring(kk.length()-1)).getData() != null)
				{
//					System.out.println("DODAJE JUZ ISTNIEJACE" + dicdata.get(kk).getData());
					selectedTypes.add(Integer.parseInt((String) dicdata.get(kk).getData()));
				}
//				System.out.println("DSG_LEASING"+dicId+"_ID_"+kk.substring(kk.length()-1));
//				System.out.println("DATA = "+dicdata.get("DSG_LEASING"+dicId+"_ID_"+kk.substring(kk.length()-1)).getData());
				if(dicdata.get(kk).getData() != null && dicdata.get("DSG_LEASING"+dicId+"_ID_"+kk.substring(kk.length()-1)).getData() == null)
				{
					System.out.println("DODAJE JUZ NOWY" + dicdata.get(kk).getData() + "   "+kk);
					newTypes.put(dicId+"_"+kk.substring(kk.length()-1),Integer.parseInt((String) dicdata.get(kk).getData()));
//					newTypes.add(Integer.parseInt((String) dicdata.get(kk).getData()));
				}
			}//DSG_LEASING29_TYP_DOKUMENTU_1
		}
		for (String  key : newTypes.keySet()) 
		{
			if(selectedTypes.contains(newTypes.get(key)))
			{
				System.out.println("KEY "+key);
				String idRodzaju = key.split("_")[0];
				String idTypu = key.split("_")[1];
				System.out.println("DSG_LEASING"+idRodzaju+"_TYP_DOKUMENTU_"+idTypu);
				values.get("DWR_DSG_LEASING"+idRodzaju).getDictionaryData().put("DSG_LEASING"+idRodzaju+"_TYP_DOKUMENTU_"+idTypu, null);
//				values.get("DWR_DSG_LEASING"+dicId)
				values.put("DWR_"+key, null);
				values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
				return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", "Isteniej ju� dokument tego typu", pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
			}
				
		}
		
		
		try
		{
			
			if(!DSApi.isContextOpen())
			{
				HttpServletRequest req = WebContextFactory.get().getHttpServletRequest();
				DSApi.open(AuthUtil.getSubject(req));
				isOpen = true;
			}
			String wniosekId = "", umowaId = "";
			if (isNotNull(values, "DWR_NUMER_WNIOSKU"))
			{
				StringBuilder msgBuilder = new StringBuilder();
				Long nrUmowy = null;
				if (isNotNull(values, "DWR_NUMER_UMOWY") && values.get("DWR_NUMER_UMOWY").getData() instanceof Map && !((String) values.get("DWR_NUMER_UMOWY").getDictionaryData().get("id").getData()).equals("") )
				{
					nrUmowy = Long.parseLong((String) values.get("DWR_NUMER_UMOWY").getDictionaryData().get("id").getData());
					umowaId = (String) values.get("DWR_NUMER_UMOWY").getDictionaryData().get("id").getData();
				}
				
				Map<String, FieldData> dvalues = values.get("DWR_NUMER_WNIOSKU").getDictionaryData();
				for (String o : dvalues.keySet())
				{
					if (isIdAndNotEmpty(dvalues, o))
					{
						boolean duplicate = getBinderByNumerWniosku(Long.parseLong((String) values.get("DWR_NUMER_WNIOSKU").getDictionaryData().get("id").getData()), nrUmowy);
						if (duplicate)
						{
							msgBuilder.append("Istniej ju� teczka dla tego wniosku!!!");
							values.put("DWR_NUMER_WNIOSKU", null);
							values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
							return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
						}
						log.info("DWR_NUMER_WNIOSKU: {}", dvalues);
						wniosekId = dvalues.get(o).getData().toString();
						// set Klient
						getFromDatabase(wniosekId, values, "IDKLIENTA", "DWR_KLIENT", "DL_APPLICATION_DICTIONARY");
						// set Dostawca
						getFromDatabase(wniosekId, values, "IDDOSTAWCY", "DWR_DOSTAWCA", "DL_APPLICATION_DICTIONARY");
						// set Umowa
//						getFromDatabase(wniosekId, values, "CONTRACT", "DWR_NUMER_UMOWY", "DL_APPLICATION_DICTIONARY");
						
					}
				}
			}

			Map<String, SortedSet<Long>> documentLeasing = new HashMap<String, SortedSet<Long>>();
			getIdDocumentsLeasing(wniosekId, umowaId, documentLeasing);
			setLeasingDocumentsOnValidate(values, documentLeasing);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			if(isOpen)
				DSApi.close();
		}
		perlog.error("validateDWR OUT");
		return null;
	}

	private boolean isIdAndNotEmpty(Map<String, FieldData> dvalues, String o)
	{
		return o.equals("id") && !dvalues.get(o).getData().toString().equals("");
	}

	private boolean isNotNull(Map<String, FieldData> values, String fieldName)
	{
		return values.get(fieldName) != null && values.get(fieldName).getData() != null;
	}

	private boolean getBinderByNumerWniosku(long parseLong, Long nuUmowy) throws EdmException
	{
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			StringBuilder sql = new StringBuilder("select count(*) from dsg_dlbinder where numer_wniosku = ?");
			if (nuUmowy != null )
				sql.append(" and numer_umowy = ?");
			ps = DSApi.context().prepareStatement(sql); 
			ps.setLong(1, parseLong);
			if (nuUmowy != null )
				ps.setLong(2, nuUmowy);
			rs = ps.executeQuery();
			if (rs.next())
			{
				return rs.getInt(1) > 1;
			}
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
		return false;
	}

	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException
	{
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("DS_USER_" + acceptationCn, DSApi.context().getPrincipalName());
		doc.getDocumentKind().setOnly(doc.getId(), values);
	}

	private void getIdDocumentsLeasing(String wniosekId, String umowaId, Map<String, SortedSet<Long>> documentLeasing) throws EdmException
	{
		perlog.error("getIdDocumentsLeasing IN");
		if (wniosekId.equals("") && umowaId.equals(""))
			return;
		Statement stat = null;
		ResultSet result = null;

		boolean contextOpened = true;
		try
		{
			if (!DSApi.isContextOpen())
			{
				DSApi.openAdmin();
				contextOpened = false;
			}
			stat = DSApi.context().createStatement();

			StringBuilder select = new StringBuilder("select distinct doc.id, dsg.RODZAJ_DOKUMENTU from dsg_leasing dsg join ds_document doc on doc.id=dsg.document_id");
			if (!wniosekId.equals(""))
				select.append(" join dsg_leasing_multiple_value multi on  multi.document_id=doc.id and multi.field_cn = 'NUMER_WNIOSKU' and multi.field_val = ").append(wniosekId);
			if (!umowaId.equals("") && !umowaId.equals("0"))
				select.append(" join dsg_leasing_multiple_value multi2 on multi2.document_id=doc.id and multi2.field_cn = 'NUMER_UMOWY' and multi2.field_val = ").append(umowaId);
			select.append(" where doc.DELETED = 0");
			
			log.info("select: " + select.toString());
			result = stat.executeQuery(select.toString());
			while (result.next())
			{
				Long id = result.getLong(1);
				Integer rodzajDok = result.getInt(2);
				if (!documentLeasing.containsKey("DWR_DSG_LEASING" + rodzajDok))
					documentLeasing.put("DWR_DSG_LEASING" + rodzajDok, new TreeSet<Long>());
				documentLeasing.get("DWR_DSG_LEASING" + rodzajDok).add(id);
			}
		}
		catch (Exception ie)
		{
			log.error(ie.getMessage(), ie);
		}
		finally
		{
			DSApi.context().closeStatement(stat);
			if (!contextOpened && DSApi.isContextOpen())
				DSApi.close();
		}
		perlog.error("getIdDocumentsLeasing OUT");
	}

	
	private static void getDocumentTypes(Long wid, Long uid,  HashSet<Integer> documentTypes) throws EdmException
	{
		perlog.error("getIdDocumentsLeasing IN");
		if (wid.equals("") && uid.equals(""))
			return;
		Statement stat = null;
		ResultSet result = null;

		boolean contextOpened = true;
		try
		{
			if (!DSApi.isContextOpen())
			{
				DSApi.openAdmin();
				contextOpened = false;
			}
			stat = DSApi.context().createStatement();

			StringBuilder select = new StringBuilder("select distinct dsg.typ_dokumentu from dsg_leasing dsg join ds_document doc on doc.id=dsg.document_id");
			if (!wid.equals(""))
				select.append(" join dsg_leasing_multiple_value multi on  multi.document_id=doc.id and multi.field_cn = 'NUMER_WNIOSKU' and multi.field_val = ").append(wid);
			if (uid != null && !"".equals(uid) && !"0".equals(uid))
				select.append(" join dsg_leasing_multiple_value multi2 on multi2.document_id=doc.id and multi2.field_cn = 'NUMER_UMOWY' and multi2.field_val = ").append(uid);
			select.append(" where doc.DELETED = 0");

			log.info("select: " + select.toString());
			result = stat.executeQuery(select.toString());
			
			while (result.next())
			{
				Integer typ = result.getInt(1);
				documentTypes.add(typ);
			}
		}
		catch (Exception ie)
		{
			log.error(ie.getMessage(), ie);
		}
		finally
		{
			DSApi.context().closeStatement(stat);
			if (!contextOpened && DSApi.isContextOpen())
				DSApi.close();
		}
		perlog.error("getIdDocumentsLeasing OUT");
	}
	
	private void setLeasingDocumentsOnValidate(Map<String, FieldData> values, Map<String, SortedSet<Long>> documentLeasing)
	{
		for (String key : documentLeasing.keySet())
			values.put(key, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.DICTIONARY, documentLeasing.get(key)));
	}

	private void setOnLoadLeasingDocuments(Map<String, Object> values, Map<String, SortedSet<Long>> documentLeasing)
	{
		for (String key : documentLeasing.keySet())
			values.put(key.replace("DWR_", ""), documentLeasing.get(key));
	}

	private void getFromDatabase(String baseId, Map<String, FieldData> values, String what, String dwrName, String tableName) throws EdmException
	{
		perlog.error("getFromDatabase IN");
		if (baseId == null)
			return;

		Statement stat = null;
		ResultSet result = null;
		try
		{

			stat = DSApi.context().createStatement();
			String select = "select " + what + " from " + tableName + " where id = " + baseId;
			log.info("getFromDatabase select " + select);
			result = stat.executeQuery(select);
			while (result.next())
			{
				Long id = result.getLong(1);
				log.info("znalezione id" + id.toString() + "ddd");
				values.put(dwrName, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.LONG, id));

			}

		}
		catch (Exception ie)
		{
			log.error(ie.getMessage(), ie);
		}
		finally
		{
			DSApi.context().closeStatement(stat);
		}
		perlog.error("getFromDatabase OUT");
	}
	
	private ArrayList<Long> getBinderFromNRUmowyNRWniosku(Long idUmowy, Long idWniosku)
	{
		perlog.error("getBinderFromNRUmowyNRWniosku IN");
		PreparedStatement ps = null;
		ArrayList<Long> a = new ArrayList<Long>();
		try
		{

			log.error("getBinderFromNRUmowyNRWniosku idUmowy "+idUmowy);
			log.error("getBinderFromNRUmowyNRWniosku idUmowy "+idWniosku);
			StringBuilder sb = new StringBuilder("select document_id from dsg_dlbinder where numer_wniosku = ? ");
			if(idUmowy != null && idUmowy > 0)
				sb.append("and numer_umowy = ? ");
			System.out.println(sb.toString()+" "+idUmowy+" "+ idWniosku);
			ps = DSApi.context().prepareStatement(sb.toString());
			ps.setLong(1, idWniosku);
			if(idUmowy != null && idUmowy > 0)
				ps.setLong(2, idUmowy);
			ResultSet rs = ps.executeQuery();
			while (rs.next())
			{
				a.add(rs.getLong(1));
			}
			rs.close();
			ps.close();

		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		DbUtils.closeQuietly(ps);
		perlog.error("getBinderFromNRUmowyNRWniosku OUT");
		return a;
		
	}

	public void importContract(FieldsManager fm) throws EdmException
	{
		perlog.error("importContract IN");;
		String wniosekId = null;
		Long uid = null;
		if (fm.getDocumentId() != null)
		{
			if (fm.getKey("NUMER_WNIOSKU") != null) 
				wniosekId = fm.getKey("NUMER_WNIOSKU").toString();
			if (fm.getKey("NUMER_UMOWY") != null)
			{
				uid = (Long) fm.getKey("NUMER_UMOWY");
			}
			log.error("importContract fm.getKey(NUMER_UMOWY) "+fm.getKey("NUMER_UMOWY"));
			log.error("importContract fm.getKey(NUMER_WNIOSKU) "+fm.getKey("NUMER_WNIOSKU"));
			log.error("importContract fm.getDocumentId() "+fm.getDocumentId());
			if (AvailabilityManager.isAvailable("dlbinder.loadUmowa") && ( uid == null || uid < 1 ) && wniosekId != null
					&& fm.getValue(NUMER_WNIOSKU_FIELD_CN) != null && fm.getValue(NUMER_WNIOSKU_FIELD_CN) instanceof java.util.Map<?,?> &&
					((Map<String, Object>) fm.getValue(NUMER_WNIOSKU_FIELD_CN)).get("NUMER_WNIOSKU_NUMERWNIOSKU") != null)
			{
				log.error("importContract teczka bez umowy szukam w LEO");
				boolean isopen = true;
				if (!DSApi.context().isTransactionOpen())
				{
					isopen = false;
					DSApi.context().begin();
				}
				ArrayList<Long> contractIds = new ArrayList<Long>();
				String numerWniosku =((Map<String, Object>) fm.getValue(NUMER_WNIOSKU_FIELD_CN)).get("NUMER_WNIOSKU_NUMERWNIOSKU").toString();
				ArrayList<DlContractDictionary> dlc = ImpulsImportUtils.findContractByAppNumber(numerWniosku,Long.valueOf(wniosekId));

				for (DlContractDictionary d : dlc)
				{
					log.error("importContract znalza� umowedla wniosku ID importowanej umowy: " + d.getId());
					contractIds.add(d.getId());
				}

				if (dlc != null && dlc.size() > 0)
				{
					ArrayList<Long> binderList = getBinderFromNRUmowyNRWniosku(contractIds.get(0),Long.valueOf(wniosekId));
					if(binderList != null && binderList.size() > 0)
					{
						log.error("Pr�bowa� doda� teczke dla wniosku {} i umowy {} ktora juz istnieje {}",Long.valueOf(wniosekId), contractIds.get(0),binderList.get(0));
						return;
					}
					else
					{
						log.error("importContract Ustawiamy falge initial oraz wstawiamy numer umowy do dokument�e DL oraz do teczki");
						setInitialDLDokument(Long.valueOf(wniosekId), contractIds.get(0));
						setNumerUmowyInBinder(contractIds.get(0), fm.getDocumentId());
						Map<String, Object> values2 = new HashMap<String, Object>();
						values2.put(NUMER_UMOWY_FIELD_CN, contractIds.get(0));
						fm.reloadValues(values2);
					}
				}

				/**
				 *  jezeli wiecje to towrzy nowe teczki, zkolejnymi numerami umow
				 *  z tym wnioskiem. Robi kopie dokument�w leasingowych 
				 */

				if (dlc != null && dlc.size() > 1)
				{
					List<Document> dlInitialList = new ArrayList<Document>();
					for (Long dlId : getInitilaDLDocument(Long.valueOf(wniosekId))) {
						
						dlInitialList.add(Document.find(dlId));
					}
					for (int i = 1; i < dlc.size(); ++i)
					{
						createNewBinder(contractIds.get(i), fm,Long.valueOf(wniosekId),fm,dlInitialList);
					}
					TaskSnapshot.updateAllTasksByDocumentId(fm.getDocumentId(), "anyType");
				}
				if (!isopen)
				{
					DSApi.context().commit();
				}
			}
		}
		perlog.error("importContract OUT");;
	}

	// dopisuje nowa teczke dla danego wniosku i wielu umow
	private void createNewBinder(Long contractId, FieldsManager fm, Long wniosekId, FieldsManager orgFm, List<Document> dlInitialList) throws EdmException
	{
		log.error("createNewBinder PDodajemy teczka idwniosku {} idUmowy {}",wniosekId,contractId);
		ArrayList<Long> binderList = getBinderFromNRUmowyNRWniosku(contractId,Long.valueOf(wniosekId));
		if(binderList != null && binderList.size() > 0)
		{
			log.error("createNewBinder Pr�bowa� doda� teczke dla wniosku {} i umowy {} ktora juz istnieje {}",wniosekId, contractId,binderList.get(0));
			return;
		}
		Map<String, Object> values = new HashMap<String, Object>(fm.getFieldValues());
		DocumentKind documentKindDLB = DocumentKind.findByCn(DocumentLogicLoader.DL_BINDER);
		String summary = "Dokument Obowi�zkowy";
		InOfficeDocument document = null;

		String wniosek = "brak";
		if (fm.getValue(NUMER_WNIOSKU_FIELD_CN) != null)
		{
			wniosek = ((Map<String, Object>) fm.getValue(NUMER_WNIOSKU_FIELD_CN)).get("NUMER_WNIOSKU_NUMERWNIOSKU").toString();
		}
		document = new InOfficeDocument();
		document.setDescription(contractId + "/" + wniosek);
		document.setSummary(contractId + "/" + wniosek);
		document.setDocumentKind(documentKindDLB);
		document.setForceArchivePermissions(false);
		Document docTemp = Document.find(fm.getDocumentId());
		document.setFolder(docTemp.getFolder());
		document.setAuthor(DSApi.context().getPrincipalName());
		document.setCreatingUser(DSApi.context().getPrincipalName());
		document.setPermissionsOn(false);
		docTemp = null;
		document.create();
		document.setPermissionsOn(false);
		Long newDocumentId = document.getId();

		values.remove(NUMER_UMOWY_FIELD_CN);
		for (Long dicId : leasingDictionareisIndex)
		{
			values.remove("DSG_LEASING" + dicId);
		}
		values.put(NUMER_UMOWY_FIELD_CN, contractId);

		document.getDocumentKind().setOnly(newDocumentId, values);
		document.getDocumentKind().logic().archiveActions(document, TYPE_ARCHIVE);
		document.getDocumentKind().logic().documentPermissions(document);
		document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, new HashMap<String, Object>());
		TaskSnapshot.updateByDocument(document);
		log.error("Dodaje teczke " + document.getId());
		for (Document orgDocument : dlInitialList) 
		{
			createNewDlDocument(contractId, orgDocument);
		}
	}
	
	
	public void createNewDlDocument(Long contractId,Document orgDocument) throws EdmException
	{
		log.error("createNewDlDocument Dodajemy klon DL ID={} dla Umowy {}",orgDocument.getId(),contractId);
		Map<String, Object> values = orgDocument.getFieldsManager().getFieldValues();
		List<Long> attachmentsToClone = new ArrayList<Long>(orgDocument.getAttachments().size());
		for (Attachment atta : orgDocument.getAttachments()) {
			attachmentsToClone.add(atta.getId());
		}
		Document document = orgDocument.cloneObject((Long[]) attachmentsToClone.toArray(new Long[attachmentsToClone.size()]));
		DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.DL_KIND);
		List<Long> idUmowoLong = new ArrayList<Long>();
		idUmowoLong.add(contractId);
		values.put(DlLogic.NUMER_UMOWY_CN, idUmowoLong);
		documentKind.set(document.getId(), values);
		documentKind.logic().archiveActions(document, DocumentLogic.TYPE_ARCHIVE);
		documentKind.logic().documentPermissions(document);
	}

	/**
	 * Ustawia numer umowy dla teczeki o podanym ID
	 * @param contractId
	 * @param documentId
	 */
	public void setNumerUmowyInBinder(Long contractId, Long documentId)
	{
		PreparedStatement ps = null;
		try
		{
			boolean isopen = true;
			if (!DSApi.context().isTransactionOpen())
			{
				isopen = false;
				DSApi.context().begin();
			}
			ps = DSApi.context().prepareStatement("update dsg_dlbinder set numer_umowy = ? where document_id = ?");
			ps.setLong(1, contractId);
			ps.setLong(2, documentId);
			ps.executeUpdate();
			ps.close();
			if (!isopen)
			{
				DSApi.context().commit();
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			DbUtils.closeQuietly(ps);
		}
	}
	
	public void setInitialDLDokument(Long applicationId,Long contractId)
	{
		perlog.error("setInitialDLDokument IN");;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{					
			ps = DSApi.context().prepareStatement("select doc.id from dsg_leasing dsg join ds_document doc on doc.id=dsg.document_id "+
					"join dsg_leasing_multiple_value multi on multi.document_id=doc.id and multi.field_cn = 'NUMER_WNIOSKU' and multi.field_val = ? where dsg.init > 0 ");
			log.error("Sprawdzam czy dla wniosku IDWNIOSKU =  {} byly juz okreslone startowe dokumenty leasingowe ",applicationId);
			ps.setLong(1, applicationId);
			rs = ps.executeQuery();
			if (rs.next())
			{
				log.error("Byly juz ustawione dokumenty startowe dla idwniosku {}, zmieniam dokument� startowym numer umowy na {} ", applicationId,contractId);
				ps.close();	
				ps = DSApi.context().prepareStatement("update dsg_leasing set numer_umowy = ? where numer_wniosku = ? and init > 0");
				ps.setLong(1, contractId);
				ps.setLong(2, applicationId);
				ps.execute();
			}
			else
			{
				log.error("Nie by�o jeszcze ustawionych dokument�w startowych, ustawiamy wszystkie dokumenty leasingowe dla wnioskuid = {} jako startowe i numer uwmoy na {}", applicationId,contractId);
				ps.close();	
				ps = DSApi.context().prepareStatement("update dsg_leasing set init = 1 where document_id in (select doc.id from dsg_leasing dsg join ds_document doc" +
						" on doc.id=dsg.document_id join dsg_leasing_multiple_value multi on multi.document_id=doc.id and multi.field_cn = 'NUMER_WNIOSKU' and multi.field_val = ?)");
				ps.setLong(1, applicationId);
				ps.execute();
				ps.close();
				ps = DSApi.context().prepareStatement("insert into dsg_leasing_multiple_value(document_id, field_cn, field_val) " 
						+ "select document_id,'NUMER_UMOWY',? from dsg_leasing_multiple_value  where field_cn = 'NUMER_WNIOSKU' and field_val = ?");
				ps.setLong(1, contractId);
				ps.setLong(2, applicationId);
				ps.execute();
				ps.close();
			}
			
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			DbUtils.closeQuietly(ps);
		}
		perlog.error("setInitialDLDokument OUT");;
	}
	
	/**
	 * Zwraca list� id dokument�w leasingowych oznaczinych jako wyj�ciowe dla teczki
	 * @param applicationId
	 * @return
	 */
	public List<Long> getInitilaDLDocument(Long applicationId)
	{
		perlog.error("getInitilaDLDocument IN");;
		log.error("importContract getInitilaDLDocument Szukam dokumentow DL wniosku {} z ustawiona flaga initial " + applicationId);
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Long> list = new ArrayList<Long>();
		try
		{					
			
			ps = DSApi.context().prepareStatement("select doc.id from dsg_leasing dsg join ds_document doc on doc.id=dsg.document_id "+
					" join dsg_leasing_multiple_value multi on multi.document_id=doc.id and multi.field_cn = 'NUMER_WNIOSKU' and multi.field_val = ? where dsg.init > 0");
			ps.setLong(1, applicationId);
			rs = ps.executeQuery();
			while (rs.next())
			{
				log.error("importContract getInitilaDLDocument Znalazl DL dla wniosku {}. DL ID = {} " + applicationId,rs.getLong(1));
				list.add(rs.getLong(1));
			}
			
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		finally
		{
			DbUtils.closeQuietly(ps);
		}
		perlog.error("getInitilaDLDocument OUT");;
		return list;
	}
	
	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn)
	{
		String username = null;
        String guid = null;
		if(AvailabilityManager.isAvailable("migrationWorkflow"))
		{
			username = (String) openExecution.getVariable(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM);
	        guid = (String) openExecution.getVariable(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM);
		}
        System.out.println("BINDER USER STYART "+ username);
		TaskImpl task = (TaskImpl) assignable;
		boolean assigned = false;
		try
		{
			log.info("DokumentKind: {}, DokumentId: {}', acceptationCn: {}, enumCn: {}", doc.getDocumentKind().getCn(), doc.getId(), accpetionCn, enumCn);
			String user = doc.getFieldsManager().getStringValue("DS_USER_" + accpetionCn);
			if(StringUtils.isNotBlank(username))
			{
				task.addParticipation(username, MigrationWorkflow.getWorkFlowPlace(username), Participation.CANDIDATE);
				//task.addCandidateUser(user);
				assigned = true;
				addToHistory(openExecution, doc, username, null);
			}	
			else if (user != null)
			{
				task.addParticipation(user, "DL_BINDER_"+accpetionCn, Participation.CANDIDATE);
				//task.addCandidateUser(user);
				assigned = true;
				addToHistory(openExecution, doc, user, null);
			}
			// tylko dzial sprzedazy
			else if (accpetionCn.equals("DS"))
			{
				task.addParticipation(doc.getAuthor(), "DL_BINDER_"+accpetionCn, Participation.CANDIDATE);
				//task.addCandidateUser(user);
				assigned = true;
				addToHistory(openExecution, doc, user, null);
				//getSubDivision(task, accpetionCn,openExecution,doc);
			}
			
			

		}
		catch (Exception ee)
		{
			log.error(ee.getMessage(), ee);
		}
		return assigned;
	}
	
	/**
	 * @param openExecution
	 * @param doc
	 * @throws EdmException
	 * @throws UserNotFoundException
	 */
	public void addToHistory(OpenExecution openExecution, OfficeDocument doc, String user, String guid) throws EdmException, UserNotFoundException {
		AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
		ahe.setSourceUser(DSApi.context().getPrincipalName());
		ahe.setProcessName(openExecution.getProcessDefinitionId());
		if (user == null) {
			ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_DIVISION);
			ahe.setTargetGuid(guid);
		} else {
			ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_SINGLE_USER);
			ahe.setTargetUser(user);
		}
		DSDivision[] divs = DSApi.context().getDSUser().getOriginalDivisionsWithoutGroup();
		if (divs != null && divs.length > 0)
			ahe.setSourceGuid(divs[0].getName());
		ahe.setCtime(new Date());
		ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
		try{
			String status = doc.getFieldsManager().getEnumItem("STATUS").getTitle();
			ahe.setStatus(status);
		}
		catch (Exception e) {
			log.warn(e.getMessage());
		}
		doc.addAssignmentHistoryEntry(ahe);
	}

	private void getSubDivision(TaskImpl assignable, String accpetionCn, OpenExecution openExecution, OfficeDocument doc) throws EdmException, DivisionNotFoundException
	{
		//doc.setCurrentAssignmentGuid("DL_BINDER_" + accpetionCn);
		assignable.addCandidateGroup("DL_BINDER_" + accpetionCn);
		addToHistory(openExecution, doc, null, "DL_BINDER_" + accpetionCn);
		for (DSDivision division : DSDivision.find("DL_BINDER_" + accpetionCn).getChildren())
		{
			assignable.addCandidateGroup(division.getGuid());
			// makroregiony
			for (DSDivision division2 : DSDivision.find(division.getGuid()).getChildren())
			{
				assignable.addCandidateGroup(division2.getGuid());
				// regiony
				for (DSDivision division3 : DSDivision.find(division2.getGuid()).getChildren())
				{
					doc.setCurrentAssignmentGuid(division3.getGuid());
					assignable.addCandidateGroup(division3.getGuid());
				}
			}
		}
	}

	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			FieldsManager fm = dockind.getFieldsManager(id);
			Contractor conntra= null;
			if(fm.getKey(KLIENT_FIELD_CN) != null)
			{
				conntra = Contractor.findById((Long) fm.getKey(KLIENT_FIELD_CN));
			}

			if (conntra != null)
			{

				params.setAttribute(conntra.getName(), 1);
				if (conntra.getRegion() != null)
				{
					params.setAttribute(conntra.getRegion().getName(), 0);
				}
				else
				{
					params.setAttribute("brak", 0);
				}
//				String role = "";
//				if (conntra.getRole() != null)
//				{
//					for (Role r : conntra.getRole())
//					{
//						role += r.getName() + "|";
//					}
//				}
//				else
//				{
//					role = "Brak";
//				}
//				params.setAttribute(role, 2);
			}
			else
			{
				params.setAttribute("brak", 0);
				params.setAttribute("brak", 1);
			}
			String umowa = "brak";
			String wniosek = "brak";
			String typ = "brak";
			String stan = "brak";
			String nazwaPrzedmiotu = "brak";
			BigDecimal wartosc = null;
			if (fm.getKey(NUMER_UMOWY_FIELD_CN) != null && ((Map<String, Object>) fm.getValue(NUMER_UMOWY_FIELD_CN)).get("NUMER_UMOWY_NUMERUMOWY") != null)
			{
				umowa = ((Map<String, Object>) fm.getValue(NUMER_UMOWY_FIELD_CN)).get("NUMER_UMOWY_NUMERUMOWY").toString();

				if (fm.getValue(NUMER_UMOWY_FIELD_CN) != null && ((Map<String, Object>) fm.getValue(NUMER_UMOWY_FIELD_CN)).get("NUMER_UMOWY_LEASINGOBJECTTYPE") != null)
				{
					typ = ((pl.compan.docusafe.core.dockinds.dwr.EnumValues) ((Map<String, Object>) fm.getValue(NUMER_UMOWY_FIELD_CN)).get("NUMER_UMOWY_LEASINGOBJECTTYPE")).getSelectedValue();
				}
				if (fm.getValue(NUMER_UMOWY_FIELD_CN) != null && ((Map<String, Object>) fm.getValue(NUMER_UMOWY_FIELD_CN)).get("NUMER_UMOWY_WARTOSCUMOWY") != null)
				{
					System.out.println(((Map<String, Object>) fm.getValue(NUMER_UMOWY_FIELD_CN)).get("NUMER_UMOWY_WARTOSCUMOWY").getClass());
					wartosc =  (BigDecimal) ((Map<String, Object>) fm.getValue(NUMER_UMOWY_FIELD_CN)).get("NUMER_UMOWY_WARTOSCUMOWY");
				}
				if (fm.getValue(NUMER_UMOWY_FIELD_CN) != null && ((Map<String, Object>) fm.getValue(NUMER_UMOWY_FIELD_CN)).get("NUMER_UMOWY_STAN") != null)
				{
					stan = ((pl.compan.docusafe.core.dockinds.dwr.EnumValues) ((Map<String, Object>) fm.getValue(NUMER_UMOWY_FIELD_CN)).get("NUMER_UMOWY_STAN")).getSelectedValue();
				}
				if (fm.getValue(NUMER_UMOWY_FIELD_CN) != null && ((Map<String, Object>) fm.getValue(NUMER_UMOWY_FIELD_CN)).get("NUMER_UMOWY_OPISUMOWY") != null)
				{
					System.out.println(((Map<String, Object>) fm.getValue(NUMER_UMOWY_FIELD_CN)).get("NUMER_UMOWY_OPISUMOWY").getClass());
					nazwaPrzedmiotu =  (String) ((Map<String, Object>) fm.getValue(NUMER_UMOWY_FIELD_CN)).get("NUMER_UMOWY_OPISUMOWY");
				}
			}

			if (fm.getKey(NUMER_WNIOSKU_FIELD_CN) != null)
			{
				wniosek = ((Map<String, Object>) fm.getValue(NUMER_WNIOSKU_FIELD_CN)).get("NUMER_WNIOSKU_NUMERWNIOSKU").toString();

				if (fm.getValue(NUMER_WNIOSKU_FIELD_CN) != null && ((Map<String, Object>) fm.getValue(NUMER_WNIOSKU_FIELD_CN)).get("NUMER_WNIOSKU_LEASINGOBJECTTYPE") != null)
				{
					typ = ((pl.compan.docusafe.core.dockinds.dwr.EnumValues) ((Map<String, Object>) fm.getValue(NUMER_WNIOSKU_FIELD_CN)).get("NUMER_WNIOSKU_LEASINGOBJECTTYPE")).getSelectedValue();
				}

			}
			params.setAmount(wartosc);
			params.setAttribute(stan, 2);
			params.setAttribute(nazwaPrzedmiotu, 3);
			
			params.setDocumentNumber(umowa + "|" + wniosek);
			if (fm.getEnumItem("STATUS") != null)
			{
				String wfp = fm.getEnumItem("STATUS").getCn();
				String user = fm.getStringValue("DS_USER_" + wfp);
				if (StringUtils.isNotEmpty(user))
				{
					params.setMarkerClass(TaskListMarkerManager.PREFIX + "Alert");
				}
			}
			params.setCategory(typ);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return params;
	}

	public void acceptTask(Document document, ActionEvent event, String activity)
	{
		perlog.error("accTesk IN");;
		try
		{
			DSApi.context().watch(URN.create(document));

			FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
			String wfp = fm.getEnumItem("STATUS").getCn();
			Map<String, Object> fieldValues = new HashMap<String, Object>();
			if("AUTHOR".equals(wfp))
				wfp = "DS";

			DSDivision division = DSDivision.find("DL_BINDER_" + wfp);
			if(DSDivision.isInDivision(division,DSApi.context().getPrincipalName(),true))
			{
				fieldValues.put("DS_USER_" + wfp, DSApi.context().getPrincipalName());
			}
			else if( DSDivision.isInDivision(DSDivision.find("DL_BINDER_DO"),DSApi.context().getPrincipalName(),true))
			{
				fieldValues.put("DS_USER_DO", DSApi.context().getPrincipalName());
			}
			else if( DSDivision.isInDivision(DSDivision.find("DL_BINDER_DR"),DSApi.context().getPrincipalName(),true))
			{
				fieldValues.put("DS_USER_DR", DSApi.context().getPrincipalName());
			}
			else if( DSDivision.isInDivision(DSDivision.find("DL_BINDER_DU"),DSApi.context().getPrincipalName(),true))
			{
				fieldValues.put("DS_USER_DU", DSApi.context().getPrincipalName());
			}
			else if(!wfp.equals("DS") && DSDivision.isInDivision(DSDivision.find("DL_BINDER_DS"),DSApi.context().getPrincipalName(),true))
			{
				fieldValues.put("DS_USER_DS", DSApi.context().getPrincipalName());
			}
			else
			{
				fieldValues.put("DS_USER_" + wfp, DSApi.context().getPrincipalName());
			}
			document.getDocumentKind().setOnly(document.getId(), fieldValues);
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		perlog.error("accTesk OUT");;
	}

	/***
	 * Metoda sprawdza czy dokument by ju� w tym dziale
	 */
	public Integer assHistCounter(Long documentId, String wfp)
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			ps = DSApi.context().prepareStatement("select count(1) from dso_document_asgn_history where targetguid = ? and document_id =  ? and targetuser is not null");
			ps.setString(1, "DL_BINDER_" + wfp);
			ps.setLong(2, documentId);
			rs = ps.executeQuery();
			if (rs.next())
				return rs.getInt(1);
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		finally
		{
			DbUtils.closeQuietly(ps);
		}
		return 0;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		List<String> users = new ArrayList<String>();
		try
		{
			Long numerwniosku = null;
			Long numwerUmowy = null;
			String region = null;
			try
			{
				System.out.println(fm.getKey("DS_USER_DS"));
				if(fm.getKey("DS_USER_DS") != null)
				{
					users.add((String) fm.getKey("DS_USER_DS"));
				}
				if(fm.getKey("DS_USER_DR") != null)
				{
					users.add((String) fm.getKey("DS_USER_DR"));
				}
				if(fm.getKey("DS_USER_DO") != null)
				{
					users.add((String) fm.getKey("DS_USER_DO"));
				}
				if(fm.getKey("DS_USER_DU") != null)
				{
					users.add((String) fm.getKey("DS_USER_DU"));
				}
				if (fm.getKey(NUMER_WNIOSKU_FIELD_CN) != null)
				{
					Map<String, Object> map = (Map<String, Object>) fm.getValue(NUMER_WNIOSKU_FIELD_CN);
					String wniosek = ((Map<String, Object>) fm.getValue(NUMER_WNIOSKU_FIELD_CN)).get("id").toString();
					numerwniosku = Long.parseLong(wniosek);
				}
				

			}
			catch (Exception e)
			{
				log.error(e.getMessage());
			}
			try
			{
				if (fm.getKey(NUMER_UMOWY_FIELD_CN) != null && ((Map<String, Object>) fm.getValue(NUMER_UMOWY_FIELD_CN)).get("NUMER_UMOWY_ID") != null)
				{
					String umowa = ((Map<String, Object>) fm.getValue(NUMER_UMOWY_FIELD_CN)).get("NUMER_UMOWY_ID").toString();
					numwerUmowy = Long.parseLong(umowa);
				}
			}
			catch (Exception e)
			{
				log.error(e.getMessage());
			}
			try
			{
				if (fm.getKey(KLIENT_FIELD_CN) != null)
				{
					Contractor con = Contractor.findById((Long) fm.getKey(KLIENT_FIELD_CN));
					if (con.getRegion() != null)
						region = con.getRegion().getCn();
					if (con.getPatrons()!= null)
					{
						for (Patron p : con.getPatrons())
						{
							users.add(p.getUsername());
						}
					}
				}
			}
			catch (Exception e)
			{
				log.error(e.getMessage());
			}
			users.add(document.getAuthor());

			//DlPermissionManager.setupPermission(region, numerwniosku, numwerUmowy, perms, users);
			DlPermissionManager.setupPermission(region, numerwniosku, numwerUmowy, perms, ImmutableList.of(document.getAuthor()),document.getAuthor());
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		this.setUpPermission(document, perms);
	}

	public boolean searchCheckPermissions(Map<String, Object> values)
	{
		return true;
	}

	public boolean isPersonalRightsAllowed()
	{
		return true;
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		log.info("--- BinderLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			if(!AvailabilityManager.isAvailable("migrationWorkflow"))
			{
				Long nuUmowy = null;
				if (document.getFieldsManager().getKey("NUMER_UMOWY") != null)
					nuUmowy = (Long) document.getFieldsManager().getKey("NUMER_UMOWY"); 
				boolean duplicate = getBinderByNumerWniosku((Long) document.getFieldsManager().getKey("NUMER_WNIOSKU"), nuUmowy);
				if (duplicate)
					throw new EdmException("Istniej ju� teczka dla wybranego wniosku!!!");
			}
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			DSApi.context().watch(URN.create(document));

		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

	public void doDockindEvent(DockindButtonAction eventActionSupport, ActionEvent event, Document document, String activity, Map<String, Object> values, Map<String, Object> dockindKeys) throws EdmException
	{
		if ("DOCLIST".equals(eventActionSupport.getDockindEventValue()))
			newDocument(eventActionSupport, eventActionSupport.getDockindEventValue(), event, values,dockindKeys);
		else if (eventActionSupport.getDockindEventValue().startsWith("to_PDF"))
		{
			
		
			List<String> boxCns = new ArrayList<String>();
			for (String key : ((Map<String, Object>) dockindKeys.get("M_DICT_VALUES")).keySet())
			{
				boxCns.add(key);
			}
			List<Long> ids = new ArrayList<Long>();
			for (String cn : boxCns)
			{
				Object b = ((FieldData) ((Map<String, Object>) ((Map<String, Object>) dockindKeys.get("M_DICT_VALUES")).get(cn)).get("LEASING_DOC")).getData();
				if (b != null)
				{
					if (b.toString().equals("on"))
					{
						Long l = Long.parseLong((String) ((FieldData) ((Map<String, Object>) ((Map<String, Object>) dockindKeys.get("M_DICT_VALUES")).get(cn)).get("ID")).getData());
						ids.add(l);
						System.out.println(l);
					}
				}
			}
	
			try
			{
				Document[] docsView = new Document[ids.size()];
				for (int i = 0; i < ids.size(); i++)
				{
					docsView[i] = Document.find(ids.get(i));
				}
				File tmp = null;
				try
				{
					tmp = File.createTempFile("docusafe_", ".pdf");
					OutputStream os = new BufferedOutputStream(new FileOutputStream(tmp));
	
					PdfUtils.attachmentsAsPdf(docsView, true, os);
					os.close();
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
				}
				finally
				{
					DSApi.context().setRollbackOnly();
				}
	
				if (tmp != null)
				{
					try
					{
						HttpServletResponse resp = ServletActionContext.getResponse();
						resp.setContentType("application/pdf");
						resp.setHeader("Content-disposition", "attachment; filename=\"documents.pdf\"");
						ServletUtils.streamFile(resp, tmp, "application/pdf");
					}
					catch (IOException e)
					{
						log.error(e.getMessage(), e);
					}
					finally
					{
						tmp.delete();
					}
				}
	
				DSApi.context().rollback();
			}
			catch (EdmException e)
			{
				event.addActionError(e.getMessage());
			}
		}
	}
	
	
	private void newDocument(DockindButtonAction eventActionSupport, String dockingEvent, ActionEvent event, Map<String, Object> values, Map<String, Object> dockindKeys)
			throws EdmException
	{
		String[] enumTab;
		Long klient = null;
		try
		{
			enumTab = (String[]) values.get("DOCLISTnewDocument");
		}
		catch (ClassCastException e)
		{
			enumTab = new String[1];
			enumTab[0] = (String) values.get("DOCLISTnewDocument");
		}

		String newDokumentKind = (String) values.get("DOCLISTnewDocumentKind");
		Integer rodzajKontrahenta = 10;
		klient = (Long) dockindKeys.get("KLIENT");

		if (enumTab == null || enumTab.length < 1)
			throw new EdmException("Nie wybrano dokument�w");

		DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.DL_KIND);
		Map<String, EnumItem> enumMap = new HashMap<String, EnumItem>();
		for (EnumItem ei : DlLogic.getEnumItms(kind))
		{
			enumMap.put(ei.getCn(), ei);
		}		
		
		
		ArrayList<Long> umowa = new ArrayList<Long>();
		Long wniosek = null;
		if(dockindKeys.get("NUMER_WNIOSKU") != null)
			wniosek = (Long) dockindKeys.get("NUMER_WNIOSKU");
		if(dockindKeys.get("NUMER_UMOWY") != null)
			umowa.add((Long) dockindKeys.get("NUMER_UMOWY"));

		for (int i = 0; i < enumTab.length; i++)
		{
			EnumItem ei = enumMap.get(enumTab[i]);
			Field f = kind.getFieldByCn(ei.getFieldCn());
			Integer discriminator = Integer.parseInt(ei.getRefValue());
			createDoc(ei.getId(), discriminator, umowa, wniosek, null, null, klient, rodzajKontrahenta, event, ei);

		}
		
		 HttpSession session = ServletActionContext.getRequest().getSession();
        System.out.println(session.getAttribute(DwrFacade.DWR_SESSION_NAME));
		 session.removeAttribute(DwrFacade.DWR_SESSION_NAME);
		 System.out.println(session.getAttribute(DwrFacade.DWR_SESSION_NAME));
//		Map<String, Object> values = new HashMap<String, Object>();
//
//		if (fm.getKey("NUMER_WNIOSKU") != null)
//			wniosekId = fm.getKey("NUMER_WNIOSKU").toString();
//		if (fm.getKey("NUMER_UMOWY") != null)
//			umowaId = fm.getKey("NUMER_UMOWY").toString();
//
//		Map<String, SortedSet<Long>> documentLeasing = new HashMap<String, SortedSet<Long>>();
//		getIdDocumentsLeasing(wniosekId, umowaId, documentLeasing);
//
//		setOnLoadLeasingDocuments(values, documentLeasing);
//
//		fm.reloadValues(values);
	}
	
	private Document createDoc(Integer type, Integer parentField, ArrayList<Long> contract, Long application, Integer rodzaj, String cn, Long kontrahentId,
			Integer rodzajKontrahenta, ActionEvent event, EnumItem ei) throws EdmException
	{
		try
		{
			System.out.println("application "+ application + " contract "+ contract);
			Map<String, Object> values = new HashMap<String, Object>();
			DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.DL_KIND);
			DockindQuery dockindQuery = new DockindQuery(0, 2);
			dockindQuery.setDocumentKind(documentKind);
			dockindQuery.setCheckPermissions(false);
			dockindQuery.enumField(documentKind.getFieldByCn("RODZAJ_KONTRAHENTA"), rodzajKontrahenta);
			dockindQuery.enumField(documentKind.getFieldByCn(DlLogic.RODZAJ_DOKUMENTU_CN), parentField);
			dockindQuery.enumField(documentKind.getFieldByCn(DlLogic.TYP_DOKUMENTU_CN), type);
			if (application != null)
				dockindQuery.field(documentKind.getFieldByCn(DlLogic.NUMER_WNIOSKU_CN), application);
			if (contract != null && !contract.isEmpty())
				dockindQuery.field(documentKind.getFieldByCn(DlLogic.NUMER_UMOWY_CN), contract.get(0)); 
			if (cn != null && rodzaj != null)
			{
				dockindQuery.enumField(documentKind.getFieldByCn(cn), rodzaj);
			}
			dockindQuery.setCheckPermissions(false);
			SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
			if (searchResults.count() != 0)
			{
				event.addActionMessage("Istnieje ju� " + ei.getTitle());
				return null;
			}
			else
			{
				DSApi.context().begin(); 
				String summary = "Dokument LEASINGOWY";
				Document document = null;
				document = new Document(summary, summary);
				document.setDocumentKind(documentKind);
				document.setForceArchivePermissions(true);
				document.setCtime(new Date());
				document.setFolder(Folder.getRootFolder());
				document.create();
				Long newDocumentId = document.getId();
				values.put(DlLogic.RODZAJ_DOKUMENTU_CN, parentField);
				values.put(DlLogic.TYP_DOKUMENTU_CN, type);
				if (cn != null && rodzaj != null)
					values.put(cn, rodzaj);
				values.put(DlLogic.KLIENT_CN, kontrahentId);
				if (application != null)
					values.put(DlLogic.NUMER_WNIOSKU_CN, application);
				if (contract != null)
					values.put(DlLogic.NUMER_UMOWY_CN, contract);
				values.put("RODZAJ_KONTRAHENTA", rodzajKontrahenta);
				documentKind.setOnly(newDocumentId, values);
				documentKind.logic().archiveActions(document, DocumentLogic.TYPE_ARCHIVE);
				documentKind.logic().documentPermissions(document);
				DSApi.context().commit();
				log.trace("Dodaje dokument");
				event.addActionMessage("Dodano dokument " + ei.getTitle());
				return document;
			}
		}
		catch (Exception e)
		{
			DSApi.context().rollback();
			log.error( e.getMessage(),e);
			throw new EdmException("B��d dodania dokumentu " + ei.getTitle());
		}
	}

	public void removeValuesBeforeSet(Map<String, Object> dockindKeys)
	{
		for (Long leasingId : leasingDictionareisIndex)
			dockindKeys.remove("DSG_LEASING" + leasingId);
	}

	@Override
	public void setAdditionalValues(FieldsManager fm, Integer valueOf, String activity)
	{
		perlog.error("setAdditionalValues IN");;
		String wniosekId = "", umowaId = "";
		if (fm.getDocumentId() != null)
		{
			try
			{
				importContract(fm);
				Map<String, Object> values = new HashMap<String, Object>();

				if (fm.getKey("NUMER_WNIOSKU") != null)
					wniosekId = fm.getKey("NUMER_WNIOSKU").toString();
				if (fm.getKey("NUMER_UMOWY") != null)
					umowaId = fm.getKey("NUMER_UMOWY").toString();

				Map<String, SortedSet<Long>> documentLeasing = new HashMap<String, SortedSet<Long>>();
				getIdDocumentsLeasing(wniosekId, umowaId, documentLeasing);

				setOnLoadLeasingDocuments(values, documentLeasing);

				fm.reloadValues(values);
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
			}
		}
		perlog.error("setAdditionalValues OUT");;
	}
	
	
	private void updateDocumentLeasing(FieldsManager fm, Long clientId, Long[] leasingDictionareisIndex)
	{

		for (Long dicId : leasingDictionareisIndex)
		{
			try
			{
				if (fm.getKey("DSG_LEASING" + dicId) != null)
				{
					List<Long> leasingIds = (List<Long>) fm.getKey("DSG_LEASING" + dicId);
					for (Long leasingId : leasingIds)
					{
						System.out.println(leasingId+ " documentId "+fm.getDocumentId());
						updateDocumentLeasingOnDataBase(leasingId, clientId, 10);
						updateMultiValuesOfDocumentLeasingOnDataBase(leasingId, fm.getKey(NUMER_UMOWY_FIELD_CN), fm.getKey("NUMER_WNIOSKU"));
					}
				}
			}
			catch (Exception e)
			{
				log.warn(e.getMessage(), e);
			}
		}

	}

	private void updateMultiValuesOfDocumentLeasingOnDataBase(Long leasingId, Object umowaId, Object wniosekId)
	{
		PreparedStatement ps = null;
		if (wniosekId == null)
			return;

		try
		{
			boolean isopen = true;
			if (!DSApi.context().isTransactionOpen())
			{
				isopen = false;
				DSApi.context().begin();
			}

			deleteAndUpdate(ps, leasingId, "NUMER_WNIOSKU", wniosekId.toString());

			 if (umowaId != null && !umowaId.toString().isEmpty() && !umowaId.toString().equals("0") )
			 {
				 deleteAndUpdate(ps, leasingId, "NUMER_UMOWY",umowaId.toString());
			 }

			if (!isopen)
			{
				DSApi.context().commit();
			}
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		finally
		{
			DbUtils.closeQuietly(ps);
		}

	}

	private void deleteAndUpdate(PreparedStatement ps, Long documentId, String fieldCn, String fieldVal) throws SQLException, EdmException
	{
		ps = DSApi.context().prepareStatement("delete FROM dsg_leasing_multiple_value WHERE document_id = ? AND field_cn = ? AND field_val = ?");
		ps.setLong(1, documentId);
		ps.setString(2, fieldCn);
		ps.setString(3, fieldVal);
		ps.execute();
		ps.close();
		// dodajemy
		ps = DSApi.context().prepareStatement("insert into dsg_leasing_multiple_value values(?,?,?)");
		ps.setLong(1, documentId);
		ps.setString(2, fieldCn);
		ps.setString(3, fieldVal);
		ps.execute();
		ps.close();
	}

	private void updateDocumentLeasingOnDataBase(Long leasingId, Long clientId, Integer rodzajKontrahetna)
	{
		PreparedStatement ps = null;
		try
		{
			boolean isopen = true;
			if (!DSApi.context().isTransactionOpen())
			{
				isopen = false;
				DSApi.context().begin();
			}
			ps = DSApi.context().prepareStatement("update dsg_leasing set KLIENT = ?, RODZAJ_KONTRAHENTA = ? where document_id = ?");
			ps.setLong(1, clientId);
			ps.setLong(2, rodzajKontrahetna);
			ps.setLong(3, leasingId);
			int i = ps.executeUpdate();
			ps.close();
			if (!isopen)
			{
				DSApi.context().commit();
			}
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		finally
		{
			DbUtils.closeQuietly(ps);
		}
	}
	
	 public static ArrayList<EnumItem> getAvalilabeTypes(FieldsManager fm) throws EdmException
	 {
		 Long wid = null;
		 Long uid = null;
		 if(fm.getKey(NUMER_WNIOSKU_FIELD_CN) != null)
			 wid = (Long) fm.getKey(NUMER_WNIOSKU_FIELD_CN);
		 if(fm.getKey(NUMER_UMOWY_FIELD_CN) != null)
			 uid = (Long) fm.getKey(NUMER_UMOWY_FIELD_CN);

		 DataBaseEnumField en  = DataBaseEnumField.getEnumFiledForTable("dsg_typ");
		 
		 HashSet<Integer> documentTypes = new HashSet<Integer>();
		 getDocumentTypes(wid, uid, documentTypes);
		 ArrayList<EnumItem> availableEI = new ArrayList<EnumItem>(en.getAvailableItems());
		 System.out.println("ESIZE "+availableEI.size());
		 for (EnumItem itm : en.getAvailableItems()) 
		 {
			 if(documentTypes.contains(itm.getId()))
			 {
				 availableEI.remove(itm);
			 }
		 }
		 System.out.println("SIZE "+availableEI.size());
		 return availableEI;
	 }
}