package pl.compan.docusafe.parametrization.ilpoldwr;

import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContextFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.imports.*;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.admin.imports.ImportKind;

import javax.security.auth.Subject;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.*;

import static pl.compan.docusafe.parametrization.ilpoldwr.IlpolNormalOutImportLogic.ImportStatusDescription.*;

public class IlpolNormalOutImportLogic extends AbstractDocumentLogic {

    public final static String ATTACHED_NORMAL_OUT_DOCUMENT_FIELD_CN = "IMPORTED_DOCUMENT";
    private final Logger LOG = LoggerFactory.getLogger(IlpolNormalOutImportLogic.class);
    private boolean documentImported = false;
    private final String LINE_SEPARATOR = "<br />";
    public static final String NORMAL_OUT_IMPORT = "Import dokumentu przychodz�cego";

    private ImportKind importKind = null;
    private ImportResult importResult = null;
    private AddNormalOutDocument importConsumer = null;

    private String RODZAJ_PISMA = "RODZAJ_PISMA";
    private String DATA_PISMA = "DATA_PISMA";
    String OPIS = "OPIS";
    String RODZAJ_PRZESYLKI	= "RODZAJ_PRZESYLKI";
    String NIP = "NIP";
    String NADAWCA = "NADAWCA";

    private final static Function<String[], String[]> headerFilter = HeaderFilters.buildFilter()
            .possibleKeys("RODZAJ_PISMA", "DATA_PISMA", "OPIS", "RODZAJ_PRZESYLKI", "NIP")
            .requiredKeys("RODZAJ_PISMA", "DATA_PISMA", "OPIS", "RODZAJ_PRZESYLKI", "NIP")
            .alias("RODZAJ_PISMA", "Rodzaj pisma")
            .alias("DATA_PISMA", "Data pisma")
            .alias("OPIS", "Opis")
            .alias("RODZAJ_PRZESYLKI", "Rodzaj przesy�ki")
            .alias("NIP", "Numer NIP kontrahenta")
            .alias("NADAWCA", "Dzia� nadaj�cy pismo")
            .create();

    @Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        return super.validateDwr(values, fm);
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        //LOG.info("ILPOL_IMPORT: Pocz�tek importu pism wychodz�cych");
        LoggerFactory.getLogger("ilpol_doc_out_import").info("ILPOL_IMPORT: Pocz�tek importu pism wychodz�cych");
        if(!documentImported){
            try {
                //LOG.info("ILPOL_IMPORT: Konfiguracja importu");
                LoggerFactory.getLogger("ilpol_doc_out_import").info("ILPOL_IMPORT: Konfiguracja importu");
                initConfiguration(document);
                //LOG.info("ILPOL_IMPORT: Koniec importu");
                LoggerFactory.getLogger("ilpol_doc_out_import").info("ILPOL_IMPORT: Koniec importu");
                documentImported = true;
            } catch (EdmException e) {
                LoggerFactory.getLogger("ilpol_doc_out_import").debug(e.getMessage());
            } catch (IOException e) {
                LoggerFactory.getLogger("ilpol_doc_out_import").debug(e.getMessage());
            }

            Map<String, Object> values = new HashMap<String, Object>();
            //LOG.info("ILPOL_IMPORT: Tworzenie podsumowania importu");
            LoggerFactory.getLogger("ilpol_doc_out_import").info("ILPOL_IMPORT: Tworzenie podsumowania importu");
            values.put("IMPORT_SUMMARY", generateSummary());
            document.getDocumentKind().setOnly(document.getId(), values);
            //LOG.info("ILPOL_IMPORT: Koniec tworzenia podsumowania importu");
            LoggerFactory.getLogger("ilpol_doc_out_import").info("ILPOL_IMPORT: Koniec tworzenia podsumowania importu");

            ((OutOfficeDocument)document).setSummary("Podsumowanie importu pisma przychodz�cego z pliku");
            ((OutOfficeDocument)document).setSource("Podsumowanie importu pisma przychodz�cego z pliku");
            ((OutOfficeDocument)document).setSummary("Podsumowanie importu pisma przychodz�cego z pliku");
        }
    }

    private void initConfiguration(Document document) throws EdmException, IOException {
        //LOG.info("ILPOL_IMPORT:  Otwieranie sesji");
        LoggerFactory.getLogger("ilpol_doc_out_import").info("ILPOL_IMPORT: Otwieranie sesji");
        establishSession();
        //LOG.info("ILPOL_IMPORT: KKoniec otwierania sesji");
        LoggerFactory.getLogger("ilpol_doc_out_import").info("ILPOL_IMPORT: Koniec otwierania sesji");
        initImport(document);
        //LOG.info("ILPOL_IMPORT: Zamykanie sesji");
        LoggerFactory.getLogger("ilpol_doc_out_import").info("ILPOL_IMPORT: Zamykanie sesji");
        establishSession();
        //LOG.info("ILPOL_IMPORT: Koniec zamykania sesji");
        LoggerFactory.getLogger("ilpol_doc_out_import").info("ILPOL_IMPORT: Koniec zamykania sesji");
    }

    private void establishSession() throws EdmException {
        if(DSApi.isContextOpen()){
            if(DSApi.context().inTransaction()){
                DSApi.context().commit();
            }else{
                DSApi.context().begin();
            }
        }else{
            Subject subject = null;
            if(ServletActionContext.getRequest() != null){
                subject = AuthUtil.getSubject(ServletActionContext.getRequest());
            }else {
                subject = AuthUtil.getSubject(WebContextFactory.get().getHttpServletRequest());
            }
            DSApi.open(subject);
        }
    }

    private void initImportKind(){
        importConsumer = new AddNormalOutDocument();
        importKind = new ImportKind();
        importKind.setHeaderFilter(headerFilter);
        importKind.setPredicate(Predicates.<KeyValueEntity>alwaysTrue());
        importKind.setConsumer(importConsumer);
        importKind.setName("Import dokument�w przychodz�cych");
    }

    private void initImport(Document document) throws EdmException, IOException {
        initImportKind();

        Attachment importedAttachment = ((Attachment) document.getFieldsManager().getValue(ATTACHED_NORMAL_OUT_DOCUMENT_FIELD_CN));
        InputStream is = importedAttachment.getMostRecentRevision().getAttachmentStream();
        File attachment = new File(importedAttachment.getMostRecentRevision().getOriginalFilename());
        FileUtils.writeIsToFile(is, attachment);
        attachment.renameTo(new File(attachment.getParent() + File.separator + importedAttachment.getMostRecentRevision().getOriginalFilename()));
        XlsWithHeaderFileProvider xlsProvider = new XlsWithHeaderFileProvider(attachment);
        xlsProvider.setHeaderFilter(headerFilter);
        KeyValueImport importTask = new KeyValueImport(xlsProvider, importKind.getConsumer());

        try {
            LoggerFactory.getLogger("ilpol_doc_out_import").info("ILPOL_IMPORT: Rozpocz�cie importu");
            //LOG.info("ILPOL_IMPORT: Rozpocz�cie importu");
            this.importResult = importTask.call();
            //LOG.info("ILPOL_IMPORT: Koniec importu");
            LoggerFactory.getLogger("ilpol_doc_out_import").info("- - - - - - - - - - - - - - - - - - - I M P O R T  F I N I S H E D - - - - - - - - - - - - - - - - - - - - - - - ");
            LoggerFactory.getLogger("ilpol_doc_out_import").info("ILPOL_IMPORT: Koniec importu");
        } catch (Exception e) {
            LOG.error("ILPOL_IMPORT: " + e.getMessage(), e);
            LoggerFactory.getLogger("ilpol_doc_out_import").error("ILPOL_IMPORT: Koniec importu");
            throw new EdmException(e.getMessage(), e);
        }

    }

    private String generateSummary() {

        List<String> errorDescription = importConsumer.getErrorDescription();
        StringBuilder summary = new StringBuilder();

        summary.append("Ilo�� wszystkich zlokalizowanych wpis�w: " + this.importResult.getEntriesCounter() + LINE_SEPARATOR);
        summary.append("Ilo�� poprawnie wype�nionych dokument�w: " + this.importResult.getAddedCount() + LINE_SEPARATOR);
        summary.append("Ilo�� b��dnie wype�nionych dokument�w: " + this.importResult.getInvalidCount() + LINE_SEPARATOR);
        summary.append("Ilo�� zignorowanych (pustych) wpis�w: " + this.importResult.getIgnoredCount() + LINE_SEPARATOR);
        summary.append("Ilo�� b��dnych import�w: " + this.importResult.getErrorCount() + LINE_SEPARATOR);

        if(errorDescription.size() > 0){
            summary.append(LINE_SEPARATOR + "Komunikaty obs�u�onych b��d�w:" + LINE_SEPARATOR);
            for(String description : errorDescription){
                summary.append(description + LINE_SEPARATOR);
            }
        }

        if(this.importResult.getErrorMessages().size() > 0){
            summary.append("Komunikaty nieobs�u�onych b��d�w:" + LINE_SEPARATOR);
            for(String description : this.importResult.getErrorMessages()){
                summary.append(description + LINE_SEPARATOR);
            }
        }

        return summary.toString();
    }

    private class AddNormalOutDocument implements Function<KeyValueEntity, ImportEntryStatus> {

        private List<String> errorDescription = new ArrayList<String>();

        private ImportEntryStatus fillNormalOutDocument(ImportEntryStatus entryStatus, OutOfficeDocument document, FieldsManager fm, Map<String, Object> retrivedValues, String ilpolKind, String docDate, String docDescription, String docDelivery, String recipient, String assignedSender) throws EdmException {
            //LOG.info("ILPOL_IMPORT: Ilo�� pozyskanych danych wpisu: " + retrivedValues.size());
            LoggerFactory.getLogger("ilpol_doc_out_import").info("ILPOL_IMPORT: Ilo�� pozyskanych danych wpisu: " + retrivedValues.size());
            document.setSummary("Zaimportowany dokument przychodz�cy");
            document.setSource(NORMAL_OUT_IMPORT);
            document.setAssignedDivision(DSDivision.ROOT_GUID);
            document.setCurrentAssignmentAccepted(Boolean.FALSE);
            document.setFolder(Folder.getRootFolder());
            document.setCreatingUser(CokUtils.retriveUserLogin());
            document.setTitle("Zaimportowany dokument przychodz�cy");
            document.setAuthor(CokUtils.retriveUserLogin());
            document.setForceArchivePermissions(false);

            //jesli wpis jest bledny to nie dekretujemy dokumentu na dzial
            if(entryStatus == ImportEntryStatus.INVALID_INPUT){
                document.setCurrentAssignmentGuid(StringUtils.EMPTY);
                document.setDivisionGuid(StringUtils.EMPTY);
            }else{
                document.setCurrentAssignmentGuid(assignedSender.replace(";d:", StringUtils.EMPTY));
                document.setDivisionGuid(assignedSender.replace(";d:", StringUtils.EMPTY));
            }


            if(retrivedValues.size() == 1 && StringUtils.isBlank(String.valueOf(retrivedValues.get("DOC_DESCRIPTION")))){
                entryStatus = ImportEntryStatus.IGNORED;
                errorDescription.remove(errorDescription.size()-1);
                LoggerFactory.getLogger("ilpol_doc_out_import").info("ILPOL_IMPORT: ignorowanie pustego wpisu" );
            }else{
                document.create();
                document.getDocumentKind().setOnly(document.getId(), retrivedValues);
                Documents.flushChangesForDocument(document.getId());
                document.getDocumentKind().logic().onStartProcess(document, null);
                TaskSnapshot.updateByDocument(document);
            }
            return entryStatus;
        }


        @Override
        public ImportEntryStatus apply(KeyValueEntity from) {
            int currentErrorDescriptionIndex = errorDescription.size();
            LoggerFactory.getLogger("ilpol_doc_out_import").info("- - - - - - - - - - - - - - - - - - - N E W    E N T R Y - - - - - - - - - - - - - - - - - - - - - - - ");
            LoggerFactory.getLogger("ilpol_doc_out_import").info("Zlokalizowano wpis: " +
                    from.string(RODZAJ_PISMA) + " " +
                    from.string(DATA_PISMA) + " " +
                    from.string(OPIS) + " " +
                    from.string(RODZAJ_PRZESYLKI) + " " +
                    from.string(NIP) + " " +
                    from.string(NADAWCA));
            /*
            LOG.info("Zlokalizowano wpis: " +
                    from.string(RODZAJ_PISMA) + " " +
                    from.string(DATA_PISMA) + " " +
                    from.string(OPIS) + " " +
                    from.string(RODZAJ_PRZESYLKI) + " " +
                    from.string(NIP) + " " +
                    from.string(NADAWCA));
            */
            ImportEntryStatus entryStatus = ImportEntryStatus.ADDED;
            String ilpolKind = null;
            String docDate = null;
            String docDescription = null;
            String docDelivery = null;
            String recipientNip = null;
            String recipient = null;
            String assignedSenderGuid =  null;

            OutOfficeDocument document = new OutOfficeDocument();
            DocumentKind normalOutKind = null;
            Map<String, Object> retrivedValues = new HashMap<String, Object>();
            try {
                normalOutKind = DocumentKind.findByCn(NormalLogic.NORMAL_OUT_DOCKIND_NAME);
                document.setDocumentKind(normalOutKind);
            } catch (EdmException e) {
                LOG.error("ILPOL_IMPORT: " + e.getMessage(), e);
                LoggerFactory.getLogger("ilpol_doc_out_import").error("ILPOL_IMPORT: " + e.getMessage(), e);
                entryStatus = ImportEntryStatus.INVALID_INPUT;
                updateEntryStatusErrorDescription(errorDescription, currentErrorDescriptionIndex, e.getMessage(), SYSTEM_ERROR);
            }

            FieldsManager fm = document.getFieldsManager();
            //LOG.info("ILPOL_IMPORT: Rozpocz�cie zczytywania danych dokumentu wychodz�cego z wpisu pliku wsadowego");
            LoggerFactory.getLogger("ilpol_doc_out_import").info("ILPOL_IMPORT: Rozpocz�cie zczytywania danych dokumentu wychodz�cego z wpisu pliku wsadowego");
            if((docDate = from.string(DATA_PISMA)) == null){
                entryStatus = ImportEntryStatus.INVALID_INPUT;
                updateEntryStatusErrorDescription(errorDescription, currentErrorDescriptionIndex, DATA_PISMA + " (BRAK DANYCH)", ENTRY_ERROR);
            }else{
                try {
                    DateUtils.parseDateAnyFormat(docDate);
                } catch (ParseException e) {
                    LOG.debug("ILPOL_IMPORT: " + e.getMessage(), e);
                    LoggerFactory.getLogger("ilpol_doc_out_import").error("ILPOL_IMPORT: " + e.getMessage(), e);
                    updateEntryStatusErrorDescription(errorDescription, currentErrorDescriptionIndex, DATA_PISMA + " (OCZEKIWANY FORMAT [yyyy-MM-dd | dd-MM-yyyy | dd-MM-yy] NIEPRAWID�OWA WARTO��: " + docDate +")", ENTRY_ERROR);
                }
                retrivedValues.put("DOC_DATE", docDate);
            }

            if((docDescription = from.string(OPIS)) == null){
                entryStatus = ImportEntryStatus.INVALID_INPUT;
                updateEntryStatusErrorDescription(errorDescription, currentErrorDescriptionIndex, OPIS + " (BRAK DANYCH)", ENTRY_ERROR);
                retrivedValues.put("DOC_DESCRIPTION", " ");
            }else{
                retrivedValues.put("DOC_DESCRIPTION", docDescription);
            }

            if((docDelivery = from.string(RODZAJ_PRZESYLKI)) == null){
                entryStatus = ImportEntryStatus.INVALID_INPUT;
                updateEntryStatusErrorDescription(errorDescription, currentErrorDescriptionIndex, RODZAJ_PRZESYLKI + " (BRAK DANYCH)", ENTRY_ERROR);
            }else{
                try {
                    //null jesli nie znajdzie
                    if(OutOfficeDocumentDelivery.findByName(docDelivery) == null){
                        entryStatus = ImportEntryStatus.INVALID_INPUT;
                        updateEntryStatusErrorDescription(errorDescription, currentErrorDescriptionIndex, RODZAJ_PRZESYLKI + " (NIEPRAWID�OWA WARTO��: " + docDelivery +")", ENTRY_ERROR);
                    }else{
                        retrivedValues.put("DOC_DELIVERY", OutOfficeDocumentDelivery.findByName(docDelivery).getId());
                    }
                } catch (EdmException e) {
                    LOG.error("ILPOL_IMPORT: " + e.getMessage(), e);
                    LoggerFactory.getLogger("ilpol_doc_out_import").error("ILPOL_IMPORT: " + e.getMessage(), e);
                    entryStatus = ImportEntryStatus.INVALID_INPUT;
                    updateEntryStatusErrorDescription(errorDescription, currentErrorDescriptionIndex, e.getMessage(), SYSTEM_ERROR);
                }
            }

            if((recipientNip = from.string(NIP)) == null){
                entryStatus = ImportEntryStatus.INVALID_INPUT;
                updateEntryStatusErrorDescription(errorDescription, currentErrorDescriptionIndex, NIP, ENTRY_ERROR);
            }else{
                PersonContractorDictionary recipientDictionary = new PersonContractorDictionary();
                recipientDictionary.setName("RECIPIENT"); //RECIPIENT_NIP
                Map<String, FieldData> values = new HashMap<String, FieldData>();
                FieldData nipData = new FieldData(Field.Type.STRING, recipientNip);
                values.put(recipientDictionary.getName()+"_NIP", nipData);

                try {
                    recipient = ((Map)recipientDictionary.search(values, 11, 0, true, null).get(0)).get(recipientDictionary.getName() + "_id").toString();
                    retrivedValues.put("RECIPIENT", recipient);
                } catch (Exception e) {
                    entryStatus = ImportEntryStatus.INVALID_INPUT;
                    LOG.debug("ILPOL_IMPORT: b��dny numer NIP " + NIP);
                    LoggerFactory.getLogger("ilpol_doc_out_import").error("ILPOL_IMPORT: " + e.getMessage(), e);
                    updateEntryStatusErrorDescription(errorDescription, currentErrorDescriptionIndex, NIP + " (NIEPRAWID�OWA WARTO��: " + recipientNip +")", ENTRY_ERROR);
                }
            }

            if((ilpolKind = from.string(RODZAJ_PISMA)) == null){
                entryStatus = ImportEntryStatus.INVALID_INPUT;
                updateEntryStatusErrorDescription(errorDescription, currentErrorDescriptionIndex, RODZAJ_PISMA, ENTRY_ERROR);
            }else{
                //  dane zostaly wprowadzone poprawnie
                try {
                    //  sprawdzamy czy istnieje w bazie rodzaj pisma zczytany z pliku
                    EnumItem enumItem;
                    if((enumItem = document.getFieldsManager().getField("ILPOL_KIND").getEnumItemByTitle(ilpolKind)) != null){
                        retrivedValues.put("ILPOL_KIND", fm.getField("ILPOL_KIND").getEnumItemByTitle(ilpolKind).getId());
                        //  wnioskujemy na podstawie rodzaju pisma dzial, na ktory ma zostac zadekretowany dokument
                        assignedSenderGuid = new CokUtils.Assignment(enumItem).guid;
                        retrivedValues.put("SENDER_HERE", assignedSenderGuid);
                    }else{
                        entryStatus = ImportEntryStatus.INVALID_INPUT;
                        updateEntryStatusErrorDescription(errorDescription, currentErrorDescriptionIndex, RODZAJ_PISMA + " (NIEPRAWID�OWA WARTO��: " + ilpolKind +")", ENTRY_ERROR);
                    }
                } catch (EdmException e) {
                    entryStatus = ImportEntryStatus.INVALID_INPUT;
                    updateEntryStatusErrorDescription(errorDescription, currentErrorDescriptionIndex, e.getMessage(), SYSTEM_ERROR);
                    LOG.error("ILPOL_IMPORT: " + e.getMessage(), e);
                    LoggerFactory.getLogger("ilpol_doc_out_import").error("ILPOL_IMPORT: " + e.getMessage(), e);
                }
            }
            //LOG.info("ILPOL_IMPORT: Zako�czenie zczytywania danych dokumentu wychodz�cego z wpisu pliku wsadowego");
            LoggerFactory.getLogger("ilpol_doc_out_import").info("ILPOL_IMPORT: Zako�czenie zczytywania danych dokumentu wychodz�cego z wpisu pliku wsadowego");
            try {
                //LOG.info("ILPOL_IMPORT: Tworzenie zaimportowanego dokumentu wychodz�cego");
                LoggerFactory.getLogger("ilpol_doc_out_import").info("ILPOL_IMPORT: Tworzenie zaimportowanego dokumentu wychodz�cego");
                entryStatus = fillNormalOutDocument(entryStatus, document, fm, retrivedValues, ilpolKind, docDate, docDescription, docDelivery, recipient, assignedSenderGuid);
                //LOG.info("ILPOL_IMPORT: Koniec tworzenia zaimportowanego dokumentu wychodz�cego - status: " + entryStatus);
                LoggerFactory.getLogger("ilpol_doc_out_import").info("ILPOL_IMPORT: Koniec tworzenia zaimportowanego dokumentu wychodz�cego - status: " + entryStatus);
                if(entryStatus == ImportEntryStatus.INVALID_INPUT){
                    String finalizedDescription = StringUtils.leftPad(errorDescription.get(currentErrorDescriptionIndex), errorDescription.get(currentErrorDescriptionIndex).length() + String.valueOf(document.getId()).length()+5, "(ID " + String.valueOf(document.getId()) + ")");
                    errorDescription.add(currentErrorDescriptionIndex, finalizedDescription);
                }
                //LOG.info("ILPOL_IMPORT: Koniec importu");
                LoggerFactory.getLogger("ilpol_doc_out_import").info("ILPOL_IMPORT: Koniec importu");
            } catch (EdmException e) {
                entryStatus = ImportEntryStatus.INVALID_INPUT;
                updateEntryStatusErrorDescription(errorDescription, currentErrorDescriptionIndex, e.getMessage(), SYSTEM_ERROR);
                LOG.error("ILPOL_IMPORT: " + e.getMessage(), e);
                LoggerFactory.getLogger("ilpol_doc_out_import").error("ILPOL_IMPORT: " + e.getMessage(), e);
            }
            return entryStatus;
        }

        private void updateEntryStatusErrorDescription(List<String> errorDescription, Integer currentDescriptionIndex, String statusDescription, ImportStatusDescription importStatusDescription){
            String currentDescription = "";
            if(errorDescription.size() == currentDescriptionIndex){
                currentDescription = importStatusDescription.getLabel();
            }else{
                currentDescription = errorDescription.get(currentDescriptionIndex);
            }
            String systemErrorDescriptionPart = "";
            String entryErrorDescriptionPart = "";
            int entryErrorBeginPosition = -1;
            int systemErrorBeginPosition = -1;
            if(StringUtils.isBlank(currentDescription)){
                currentDescription = importStatusDescription.getLabel();
            }else{
                entryErrorBeginPosition = currentDescription.indexOf(ENTRY_ERROR.getLabel());
                systemErrorBeginPosition = currentDescription.indexOf(SYSTEM_ERROR.getLabel());

                if(systemErrorBeginPosition == 0){
                    //poczatek opisu stanowi opis bledu zwiazane z niepoprawnym dzialaniem systemu
                    if(entryErrorBeginPosition != -1){
                        systemErrorDescriptionPart = currentDescription.substring(systemErrorBeginPosition, entryErrorBeginPosition);
                        entryErrorDescriptionPart = currentDescription.substring(entryErrorBeginPosition, currentDescription.length());
                    }else{
                        systemErrorDescriptionPart = currentDescription;
                    }
                }else{
                    //poczatek opisu stanowi opis bledu zwiazanego z niepoprawnie wprowadzonymi danymi
                    if(systemErrorBeginPosition != -1){
                        entryErrorDescriptionPart = currentDescription.substring(entryErrorBeginPosition, systemErrorBeginPosition);
                        systemErrorDescriptionPart = currentDescription.substring(systemErrorBeginPosition, currentDescription.length());
                    }else{
                        entryErrorDescriptionPart = currentDescription;
                    }
                }

                if(ENTRY_ERROR == importStatusDescription){
                    entryErrorDescriptionPart += " " + statusDescription;
                }else if(SYSTEM_ERROR == importStatusDescription){
                    systemErrorDescriptionPart += " : " + statusDescription;
                }

            }
            currentDescription = systemErrorDescriptionPart + " " + entryErrorDescriptionPart;

            if(errorDescription.size() == currentDescriptionIndex){
                //nowy wpis
                errorDescription.add(currentDescription);
            }else{
                //modyfikacja istniejacego wpisu
                errorDescription.set(currentDescriptionIndex, currentDescription);
            }
        }

        public List<String> getErrorDescription() {
            return errorDescription;
        }
    }


    protected enum ImportStatusDescription{
        ENTRY_ERROR("B��d wprowadzonych danych w kolumnach: "),
        SYSTEM_ERROR("B��d systemowy: ");

        private String label;

        ImportStatusDescription(String label){
            this.label = label;
        }

        ImportStatusDescription(){}

        public String getLabel() {
            return label;
        }


    }

}
