package pl.compan.docusafe.parametrization.ilpoldwr;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.permission.DlPermissionManager;
import pl.compan.docusafe.core.crm.Contact;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.crm.Patron;
import pl.compan.docusafe.core.crm.Role;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.parametrization.ilpol.CrmMarketingTaskLogic;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

public class MarketingTaskLogic extends AbstractDocumentLogic
{
	private static MarketingTaskLogic instance = new MarketingTaskLogic();
	private static final Logger log = LoggerFactory.getLogger(MarketingTaskLogic.class);

	public static final int UPDATE_DOCUMENT = 13;
	public static final String LABEL_NAME_ZA_5_DNI = "Za pi�� dni";
	public static final String LABEL_NAME_ZA_4_DNI = "Za cztery dni";
	public static final String LABEL_NAME_ZA_3_DNI = "Za trzy dni";
	public static final String LABEL_NAME_ZA_2_DNI = "Za dwa dni";
	public static final String LABEL_NAME_ZA_1_DNI = "Za jeden dzie�";
	public static final String LABEL_NAME_DISIAJ = "Dzisiaj";
	public static final String LABEL_NAME_PRZETERMINOWANE = "Przeterminowane";
	public static final String LABEL_NAME_ODLEGLE = "Odleg�y termin";
	public static String[] LABEL_NAMES_TAB;
	public static final String STATUS_FIELD_CN = "STATUS";
	public static final String KATEGORIA_FIELD_CN = "KATEGORIA";
	public static final String GRUPA_FIELD_CN = "GRUPA";
	public static final String NUMER_KONTRAHENTA_FIELD_CN = "KLIENT";
	public static final String UZYTKOWNIK_FIELD_CN = "UZYTKOWNIK";
	public static final String KONTAKT_FIELD_CN = "KONTAKT";
	public static final String COUNTY_CN = "COUNTY";

	static
	{
		LABEL_NAMES_TAB = new String[8];
		LABEL_NAMES_TAB[0] = LABEL_NAME_ZA_5_DNI;
		LABEL_NAMES_TAB[1] = LABEL_NAME_ZA_4_DNI;
		LABEL_NAMES_TAB[2] = LABEL_NAME_ZA_3_DNI;
		LABEL_NAMES_TAB[3] = LABEL_NAME_ZA_2_DNI;
		LABEL_NAMES_TAB[4] = LABEL_NAME_ZA_1_DNI;
		LABEL_NAMES_TAB[5] = LABEL_NAME_DISIAJ;
		LABEL_NAMES_TAB[6] = LABEL_NAME_PRZETERMINOWANE;
		LABEL_NAMES_TAB[7] = LABEL_NAME_ODLEGLE;
	}

	public static MarketingTaskLogic getInstance()
	{
		if (instance == null)
			instance = new MarketingTaskLogic();
		return instance;
	}
	
	@Override
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues,Long documentId)
	{
		Map<String, Object> results = new HashMap<String, Object>();

		try
		{
			if (dictionaryName.equals("KONTAKT"))
			{
				if (values.get("KONTAKT_STATUS") != null && values.get("KONTAKT_STATUS").getData() != null && values.get("KONTAKT_STATUS").getData().equals("20"))
				{
					results.put("KONTAKT_DATANASTEPNEGOKONTAKTU", "not-required");
				}
			}

		}
		catch (Exception e)
		{
			log.error("Lapiemy to: " + e.getMessage(), e);
		}
		return results;
	}


	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		if(AvailabilityManager.isAvailable("migrationWorkflow"))
			return;
		try
		{
			if (document instanceof InOfficeDocument)
			{

				if (!((InOfficeDocument) document).getKind().getName().equals(getInOfficeDocumentKind()))
					((InOfficeDocument) document).setKind(InOfficeDocumentKind.findByPartialName(getInOfficeDocumentKind()).get(0));
			}
		}
		catch (Exception e)
		{
			log.error("", e);
		}

		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		String opis = "";
		opis += fm.getValue(STATUS_FIELD_CN);
		document.setDescription(opis);
		document.setSource("crm");

		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("CRM").createSubfolderIfNotPresent("Zadania Marketingowe");

		Contractor con = Contractor.findById((Long) fm.getKey(NUMER_KONTRAHENTA_FIELD_CN));
		String nazwa = "";
		if (con != null && con.getName() != null)
		{
			nazwa = con.getName().trim();
			nazwa = nazwa.toLowerCase();
			folder = folder.createSubfolderIfNotPresent(FolderInserter.to3Letters(nazwa));
			folder = folder.createSubfolderIfNotPresent(con.getName());
			folder.setAnchor(true);

			if(con.getRole() != null && DocumentLogic.TYPE_SAVE_ALL != type && ( 
					( fm.getKey(KATEGORIA_FIELD_CN) != null && !fm.getKey(KATEGORIA_FIELD_CN).equals(1) && !fm.getKey(KATEGORIA_FIELD_CN).equals(2) && !fm.getKey(KATEGORIA_FIELD_CN).equals(61) 
					&& !fm.getKey(KATEGORIA_FIELD_CN).equals(82)) || 
					fm.getKey(KATEGORIA_FIELD_CN) == null))
			{
					for (Role role : con.getRole()) 
					{
						if("LEO_2".equals(role.getCn()) || "LEO_3".equals(role.getCn()))
							throw new EdmException("Dla kontrahenta Dealer/Dostawca nale�y wybra� jedn� z czterech kategorii (HVPL, VBL, EBL, MEDICAL-EMPL)");
					}
			}
		}
		else
		{
			folder = folder.createSubfolderIfNotPresent("Brak kontrahenta");
		}
		document.setFolder(folder);
		updateDocument(document);

	}


	private void updateDocument(Document document) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		List<Long> l = new ArrayList<Long>();

		if (fm.getKey("KONTAKT") != null)
		{
			l.addAll((List<Long>) fm.getKey("KONTAKT"));
		}

		if (l != null && l.size() > 0)
		{
			Contact contact = Contact.findById(l.get(l.size() - 1));

			if (contact.getStatus().getId() == 115)
			{
				LabelsManager.removeLabelsByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAMES_TAB);
				log.info("Zakonczono prace z dokumentem o ID: " + document.getId());
			}
			else if (contact.getStatus().getId() == 20)
			{
				int noAnswerCount = 0;
				
				for (Long cID : l)
				{
					Contact c = Contact.findById(cID);
					if (c.getStatus().getId() == 20)
					{
						++noAnswerCount;
					}
					else
					{
						noAnswerCount = 0;
					}
				}
				log.info("Ilosc kontaktow bez odbioru: " + noAnswerCount);
				if (noAnswerCount < 5)
				{
					HashSet<String> set =  new HashSet<String>(Arrays.asList(CrmMarketingTaskLogic.LABEL_NAMES_TAB));
					set.remove(CrmMarketingTaskLogic.LABEL_NAME_ZA_1_DNI);
					LabelsManager.removeLabelsByName(document.getId(), set.toArray(new String[set.size()]));
					LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_1_DNI, DSApi.context().getPrincipalName(), true);
				}
			}
			else
			{

				Date d = contact.getDataNastepnegoKontaktu();
				if (d != null)
				{
					Integer i = pl.compan.docusafe.util.DateUtils.substract(d, new Date());
					log.info("XXXXXXXXXXXILOSC dni do kontaktu: " + i);
					if (i > 5)
					{
						LabelsManager.removeLabelsByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAMES_TAB);
						LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ODLEGLE, DSApi.context().getPrincipalName(), true);
						Date doDate = org.apache.commons.lang.time.DateUtils.addDays(new Date(), i - 5);
						doDate = org.apache.commons.lang.time.DateUtils.setHours(doDate, 1);
						EventFactory.registerEvent("forOneDay", "crmMarketingTaskSetLabelEventHandler", "" + i + "|" + LabelsManager.findLabelByName(CrmMarketingTaskLogic.LABEL_NAME_ZA_5_DNI).getId() + "|" + document.getId(), null, doDate);
					}
					else
					{
						
						switch (i)
						{
							case 0:
								
								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_DISIAJ, DSApi.context().getPrincipalName(), true);
								break;
							case 1:
								HashSet<String> set =  new HashSet<String>(Arrays.asList(CrmMarketingTaskLogic.LABEL_NAMES_TAB));
								set.remove(CrmMarketingTaskLogic.LABEL_NAME_ZA_1_DNI);
								LabelsManager.removeLabelsByName(document.getId(), set.toArray(new String[set.size()]));
								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_1_DNI, DSApi.context().getPrincipalName(), true);
								break;
							case 2:
								set =  new HashSet<String>(Arrays.asList(CrmMarketingTaskLogic.LABEL_NAMES_TAB));
								set.remove(CrmMarketingTaskLogic.LABEL_NAME_ZA_2_DNI);
								LabelsManager.removeLabelsByName(document.getId(), set.toArray(new String[set.size()]));
								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_2_DNI, DSApi.context().getPrincipalName(), true);
								break;
							case 3:
								set =  new HashSet<String>(Arrays.asList(CrmMarketingTaskLogic.LABEL_NAMES_TAB));
								set.remove(CrmMarketingTaskLogic.LABEL_NAME_ZA_3_DNI);
								LabelsManager.removeLabelsByName(document.getId(), set.toArray(new String[set.size()]));
								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_3_DNI, DSApi.context().getPrincipalName(), true);
								break;
							case 4:
								set =  new HashSet<String>(Arrays.asList(CrmMarketingTaskLogic.LABEL_NAMES_TAB));
								set.remove(CrmMarketingTaskLogic.LABEL_NAME_ZA_4_DNI);
								LabelsManager.removeLabelsByName(document.getId(), set.toArray(new String[set.size()]));
								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_4_DNI, DSApi.context().getPrincipalName(), true);
								break;
							case 5:
								set =  new HashSet<String>(Arrays.asList(CrmMarketingTaskLogic.LABEL_NAMES_TAB));
								set.remove(CrmMarketingTaskLogic.LABEL_NAME_ZA_5_DNI);
								LabelsManager.removeLabelsByName(document.getId(), set.toArray(new String[set.size()]));
								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_5_DNI, DSApi.context().getPrincipalName(), true);
								break;
							default:
								set =  new HashSet<String>(Arrays.asList(CrmMarketingTaskLogic.LABEL_NAMES_TAB));
								set.remove(CrmMarketingTaskLogic.LABEL_NAME_PRZETERMINOWANE);
								LabelsManager.removeLabelsByName(document.getId(), set.toArray(new String[set.size()]));
								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_PRZETERMINOWANE, DSApi.context().getPrincipalName(), true);
								break;
						}
					}
				}
			}
		}
		TaskSnapshot.updateByDocument(document);
	}

	public void documentPermissions(Document document) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		List<String> users = new ArrayList<String>();
		try
		{
			Long numerwniosku = null;
			Long numwerUmowy = null;
			String region = null;

			if (fm.getKey(NUMER_KONTRAHENTA_FIELD_CN) != null)
			{
				Contractor con = Contractor.findById((Long) fm.getKey(NUMER_KONTRAHENTA_FIELD_CN));
				if (con.getRegion() != null)
					region = con.getRegion().getCn();
				if (con.getPatrons()!= null)
				{
					for (Patron p : con.getPatrons())
					{
						users.add(p.getUsername());
					}
				}
					
			}
			users.add(document.getAuthor());

			DlPermissionManager.setupPermission(region, numerwniosku, numwerUmowy, perms, users,document.getAuthor());
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		this.setUpPermission(document, perms);
	}

	public boolean searchCheckPermissions(Map<String, Object> values)
	{
		return true;
	}

	public boolean isPersonalRightsAllowed()
	{
		return true;
	}

	public TaskListParams getTaskListParams(DocumentKind kind, long documentId) throws EdmException
	{
		TaskListParams ret = new TaskListParams();
		FieldsManager fm = kind.getFieldsManager(documentId);
		List<Long> contactIds = (List)fm.getKey(KONTAKT_FIELD_CN);

		if (contactIds != null && contactIds.size() > 0)
		{
			Contact contact = Contact.findById(contactIds.get(contactIds.size() - 1));
			Date d = contact.getDataNastepnegoKontaktu();
			if (d != null)
			{
				ret.setDocumentDate(d);
			}
			if (contact.getRodzajNastepnegoKontaktu() != null)
				ret.setAttribute(contact.getRodzajNastepnegoKontaktu().getName(), 2);
			if (contact.getStatus() != null)
				ret.setStatus(contact.getStatus().getName());
		}
		
		if (fm.getKey(GRUPA_FIELD_CN) != null)
		{
			ret.setAttribute(fm.getValue(GRUPA_FIELD_CN).toString(), 4);
		}
		String nazwa = "brak";
		String region = "brak";
		String role = "";
		String nip = "brak";
		if (fm.getKey(NUMER_KONTRAHENTA_FIELD_CN) != null)
		{
			Contractor con = Contractor.findById((Long)fm.getKey(NUMER_KONTRAHENTA_FIELD_CN));
			nazwa = con.getName();
			if (con.getNip() != null)
			{
				nip = con.getNip();
			}
			if (con.getRegion() != null)
			{
				region = con.getRegion().getName();
			}
			if (con.getRole() != null)
			{
				for (Role r : con.getRole())
				{
					role += r.getName() + "|";
				}
			}
			else
			{
				role = "Brak";
			}
		}
		if (role == null || role.length() < 2)
		{
			role = "brak";
		}
		ret.setAttribute(role, 3);
		ret.setAttribute(nazwa, 1);
		ret.setAttribute(region, 0);
		ret.setAttribute(nip, 5);
		return ret;
	}

	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		String channel = document.getDocumentKind().getChannel(document.getId());
		if (channel != null)
		{
			if (ProcessCoordinator.find(DocumentLogicLoader.CRM_MARKETING_TASK_KIND, channel) != null && ProcessCoordinator.find(DocumentLogicLoader.CRM_MARKETING_TASK_KIND, channel).size() > 0)
				return ProcessCoordinator.find(DocumentLogicLoader.CRM_MARKETING_TASK_KIND, channel).get(0);
			else
				return null;
		}
		return null;
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("UZYTKOWNIK", DSApi.context().getDSUser().getId());
		fm.reloadValues(values);
	}

	public String getInOfficeDocumentKind()
	{
		return "Zadanie marketingowe";
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- MarketingLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			if(!AvailabilityManager.isAvailable("migrationWorkflow"))
			{
				map.put(ASSIGN_USER_PARAM, document.getAuthor());
				if(event.getAttribute(ASSIGN_DIVISION_GUID_PARAM) != null || StringUtils.isEmpty((String) event.getAttribute(ASSIGN_DIVISION_GUID_PARAM)))
				{
					String[] divs = DSApi.context().getDSUser().getDivisionsGuidWithoutGroup();
					map.put(ASSIGN_DIVISION_GUID_PARAM,  divs.length > 0 ?  divs[0] : DSDivision.ROOT_GUID);
				}
				else			
					map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			}
			else
			{
				if(event.getAttribute(ASSIGN_USER_PARAM) != null || StringUtils.isEmpty((String) event.getAttribute(ASSIGN_USER_PARAM)))
				{
					map.put(ASSIGN_USER_PARAM,event.getAttribute(ASSIGN_USER_PARAM));
				}
				else
				{
					map.put(ASSIGN_USER_PARAM, document.getAuthor());
				}		
				map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			}
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

}
