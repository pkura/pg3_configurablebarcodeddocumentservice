package pl.compan.docusafe.parametrization.ilpoldwr;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.EmailConstants;
import org.directwebremoting.WebContextFactory;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.DictionaryUtils;
import pl.compan.docusafe.core.dockinds.dwr.*;
import pl.compan.docusafe.core.dockinds.dwr.Field.Kind;
import pl.compan.docusafe.core.dockinds.dwr.attachments.AttachmentRevisionFileItem;
import pl.compan.docusafe.core.dockinds.dwr.attachments.DwrAttachmentStorage;
import pl.compan.docusafe.core.dockinds.dwr.attachments.DwrFileItem;
import pl.compan.docusafe.core.dockinds.field.AbstractDictionaryField;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.FieldLogic;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentAttachmentFilterLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.mail.DSEmailChannel;
import pl.compan.docusafe.core.mail.MailMessage;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.sql.AcceptanceDivisionImpl;
import pl.compan.docusafe.core.users.sql.UserAdditionalData;
import pl.compan.docusafe.parametrization.ilpol.ServicesUtils;
import pl.compan.docusafe.parametrization.wssk.LogicExtension_Reopen;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.fax.IncomingFax;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.upload.UploadDriver;
import pl.compan.docusafe.util.EmailUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;

public class IlpolCokLogic extends AbstractDocumentLogic implements DocumentAttachmentFilterLogic, LogicExtension_Reopen {
    private final static Logger LOG = LoggerFactory.getLogger(IlpolCokLogic.class);
    private final static StringManager sm = StringManager.getManager(IlpolCokLogic.class.getPackage().getName());
    private final static String COK_GUID = Docusafe.getAdditionProperty("cok.guid");


    //ILPOL MPORTANT TABLE NAMES
    public static final String CONTRACTOR_COK_TABLE_NAME = "DSC_CONTRACTOR_COK";
    public static final String COK_CATEGORY_TABLE_NAME = "DSC_ILPOL_COK_CASE_CATEGORY";


    private final static String DWR_SEND_BUTTON = "DWR_SEND_BUTTON";
    private final static String DWR_SEND_FORWARD_BUTTON = "DWR_SEND_FORWARD_BUTTON";
    private final static String DWR_RESPONSE_BUTTON = "DWR_RESPONSE_BUTTON";
    private final static String DWR_FORWARD_RESPONSE_BUTTON = "DWR_FORWARD_RESPONSE_BUTTON";
    private final static String SEND_BUTTON = "SEND_BUTTON";
    private final static String RESPONSE_BUTTON = "RESPONSE_BUTTON";
    private final static String RESPONSE_CONTENT = "RESPONSE_CONTENT";
    private final static String CASE = "CASE";
    private final static String DSC_CASE_CAT_TABLE = "DSC_ILPOL_COK_CASE_CAT";
    private final static String DSC_CASE_SUBCAT_TABLE = "DSC_ILPOL_COK_CASE_SUBCAT";
    private final static String ILPL_CASE_INTERNAL_ID = "ILPL_CASE_INTERNAL_ID";
    private final static EnumValues EMPTY_NOT_HIDDEN_ENUM_VALUE = new EnumValues(new HashMap<String,String>(){{put("","-- wybierz --");}}, new ArrayList<String>(){{add("");}});

    public final static String DOCKIND_NAME = "cok_document";
    public final static String DOCKIND_TABLE_NAME = "DSC_ILPOL_COK_DOCUMENT";
    public final static int DELIVERY_TYPE_EMAIL = 700;
    public final static int DELIVERY_TYPE_MANUAL = 710;
    public final static int DELIVERY_TYPE_FAX = 720;
    public final static int DELIVERY_TYPE_IMPORT = 730;
    public final static int DELIVERY_TYPE_DOCUMENT_IN = 740;
    public final static int DELIVERY_TYPE_PORTAL_KLIENTA = 750;

    public final static String DWR_CREATE_TASK_BUTTON = "DWR_CREATE_TASK_BUTTON";
    public final static String DWR_CREATE_CASE_BUTTON = "DWR_CREATE_CASE_BUTTON";
    public final static String COK_ACCEPTATION_CN = "COK";
    private static final String ORG_EMAIL_ATTACHMENT = "ORG_EMAIL_ATTACHMENT";
    public static final String DWR_ORG_EMAIL_ATTACHMENT = "DWR_ORG_EMAIL_ATTACHMENT";
    private String ORIGINAL_MSG = "<br /><br />- - - Oryginalna wiadomo�� - - -";
    private String forwardTo;
    private String channelUserName;

    //podmiot (osoba/dzial/grupa akceptacji), na ktory ma zostac zadekretowany dokument
    private Object assignee;

    public static enum DeliveryType {
        EMAIL(DELIVERY_TYPE_EMAIL), MANUAL(DELIVERY_TYPE_MANUAL), FAX(DELIVERY_TYPE_FAX), IMPORT(DELIVERY_TYPE_IMPORT), DOCUMENT_IN(DELIVERY_TYPE_DOCUMENT_IN), PORTAL_KLIENTA(DELIVERY_TYPE_PORTAL_KLIENTA);

        private int value;

        private DeliveryType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        static DeliveryType fromValue(int value) {
            for(DeliveryType type : DeliveryType.values()) {
                if(type.getValue() == value) {
                    return type;
                }
            }

            return null;
        }
    };
    private DeliveryType deliveryType;

	@Override
	public void documentPermissions(Document document) throws EdmException {
		// TODO Auto-generated method stub

	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
    }

    @Override
    public boolean canChangeDockind(Document document) throws EdmException {
        return super.canChangeDockind(document);
    }

    /**
     * Metoda wykonywana na etapie zako�czania procesu jbpm...
     * @param document
     * @param isManual
     * @throws EdmException
     */
    @Override
    public void onEndProcess(OfficeDocument document, boolean isManual) throws EdmException {
        super.onEndProcess(document, isManual);
    }

    @Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        super.validateDwr(values, fm);
//        setCurrentUserAsDocumentAuthor(fm);
        if(!AvailabilityManager.isAvailable("activate.contractor")){
            List<Long> kontrahentListId = DwrUtils.extractIdFromDictonaryField(values, "KLIENT");
            if(kontrahentListId.size() != 0 && values.containsKey("DWR_UMOWA")){
                List<Long> umowaIdList = getUmowaListId(kontrahentListId.get(0));
                values.put("DWR_UMOWA", new FieldData(Field.Type.STRING, umowaIdList.toString().substring(1, umowaIdList.toString().lastIndexOf("]")).replace(", ", ",")));
            }
        }

        prepareDeliveryType(values);
        Map.Entry<String, FieldData> senderField = DwrUtils.getSenderField(values);
        String senderFieldCn = senderField != null ? senderField.getKey() : "";

        // kanal email
        if(deliveryType == DeliveryType.EMAIL){
            // klikni�to Odpowiedz
            if(values.containsKey(DWR_RESPONSE_BUTTON)) {
                FieldData fd = values.get(DWR_RESPONSE_BUTTON);
                if(fd.isSender()) {
                    setResponseContent(values, prepareQuotedMessage(values, fm, DWR_RESPONSE_BUTTON));
                    DSApi.open(AuthUtil.getSubject(WebContextFactory.get().getHttpServletRequest()));
                    initOriginalAttachments(fm, values);
                    DSApi._close();
                }
            }

            // klikni�to Prze�lij do
            if(values.containsKey(DWR_FORWARD_RESPONSE_BUTTON)) {
                FieldData fd = values.get(DWR_FORWARD_RESPONSE_BUTTON);
                if(fd.isSender()) {
                    setResponseContent(values, prepareQuotedMessage(values, fm, DWR_FORWARD_RESPONSE_BUTTON));
                    DSApi.open(AuthUtil.getSubject(WebContextFactory.get().getHttpServletRequest()));
                    initOriginalAttachments(fm, values);
                    DSApi._close();
                }
            }
        }else{
            if(senderField != null && senderField.getKey().equals(DWR_FORWARD_RESPONSE_BUTTON)) {
                String msg = prepareQuotedMessage(values, fm, DWR_FORWARD_RESPONSE_BUTTON);
                setResponseContent(values, msg);

                if(deliveryType == DeliveryType.FAX || deliveryType == DeliveryType.DOCUMENT_IN) {
                    DSApi.open(AuthUtil.getSubject(WebContextFactory.get().getHttpServletRequest()));
                    initOriginalAttachments(fm, values);
                    DSApi._close();
                }
            }
        }


        // klikni�to Wy�lij (Odpowied�)
        if(values.containsKey(DWR_SEND_BUTTON)) {
            FieldData fd = values.get(DWR_SEND_BUTTON);
            if(fd.isSender()) {
//                setCurrentUserAsDocumentAuthor(fm);
                //	ustawiamy zawartosc odpowiedzi aby byla widoczna na formatce
                return sendMail(DWR_SEND_BUTTON, values, fm);
            }
        } else {
        }

        // klikni�to Wy�lij (Prze�lij do)
        if(values.containsKey(DWR_SEND_FORWARD_BUTTON)) {
            FieldData fd = values.get(DWR_SEND_FORWARD_BUTTON);
            if(fd.isSender()) {
//                setCurrentUserAsDocumentAuthor(fm);
                //	ustawiamy zawartosc odpowiedzi aby byla widoczna na formatce
                return sendMail(DWR_SEND_FORWARD_BUTTON, values, fm);
            }
        } else {
        }
        // otworzono popup do tworzenia zadania
        if(senderField != null && senderField.getKey().equals(DWR_CREATE_TASK_BUTTON)) {
            CokUtils.copyAreaValues(values, "DWR_NEW_TASK_");
            CokUtils.setCreationDate(values, "DWR_NEW_TASK_");
            CokUtils.setDeadlineDateAsLeatesTodaysTime(values, "DWR_NEW_TASK_");
            String contractorId = values.get("DWR_KLIENT").getDictionaryData().get("id").getStringData();
            if(StringUtils.isNotEmpty(contractorId)) {
                values.get("DWR_NEW_TASK_CONTRACTOR").getDictionaryData().put("id", values.get("DWR_KLIENT").getDictionaryData().get("id"));
            }
        }

        // klikni�to Stw�rz zadanie
        if(senderField != null && senderField.getKey().equals("DWR_CREATE_NEW_TASK_BUTTON")){
            Document doc = null;
            String recipient = null;
            try{
                doc = CokUtils.createDocumentFromPopup(fm, values, CokUtils.COK_TASK);
                recipient = ((InOfficeDocument)doc).getRecipientUser();
            }catch(EdmException e){
                DSApi.context()._rollback();
                return new Field("cn", e.getMessage() + "\n\n" + e.getStackTrace());
            }
            LoggerFactory.getLogger("krzysiekm").debug("Utworzono nowe zadanie dekretuj�c na u�ytkownika " + recipient);
            return new Field("cn", "Utworzono nowe zadanie dekretuj�c na u�ytkownika " + recipient, Field.Type.STRING);
        }

        // otwarto popup do tworzenia sprawy
        if(senderField != null && senderField.getKey().equals(DWR_CREATE_CASE_BUTTON)) {
            values.put("DWR_NEW_CASE_INTERNAL_ID", new FieldData(Field.Type.STRING, ""));
            CokUtils.copyAreaValues(values, "DWR_NEW_CASE_");
            CokUtils.setCreationDate(values, "DWR_NEW_CASE_");
            CokUtils.setStatus(values, "DWR_NEW_CASE_");
            String contractorId = values.get("DWR_KLIENT").getDictionaryData().get("id").getStringData();
            if(StringUtils.isNotEmpty(contractorId)) {
                values.get("DWR_NEW_CASE_CONTRACTOR").getDictionaryData().put("id", values.get("DWR_KLIENT").getDictionaryData().get("id"));
            }
        }
        // klikni�to Stw�rz spraw� na popupie
        if(senderField != null && senderField.getKey().equals("DWR_CREATE_NEW_CASE_POPUP_BUTTON")){
            if(fm.getDocumentId() == null) {
                return new Field(sm.getString("NieMoznaUtworzycSprawyBezZgloszenia"), "msg");
            }
            Document doc = CokUtils.createDocumentFromPopup(fm, values, CokUtils.COK_CASE);
            values.put("DWR_NEW_CASE_INTERNAL_ID", new FieldData(Field.Type.STRING, ""));
            Long newCaseId = getCokCaseListId(doc.getId()).get(0);
            String recipient = null;
            if(newCaseId != null){
//                values.put("DWR_ADD_TO_CASE_AVAIL_CASE", new FieldData(Field.Type.LONG, newCaseId));
                if(!DSApi.isContextOpen())
                    DSApi.open(AuthUtil.getSubject(WebContextFactory.get().getHttpServletRequest()));
                DSApi.context().begin();
                updateCaseInternalId(fm, values, doc, "DWR_NEW_CASE_INTERNAL_ID");
                DataMartManager.addHistoryEntry(fm.getDocumentId(), Audit.create(DataMartDefs.DOCUMENT_CREATE, DSApi.context().getPrincipalName(), sm.getString("UtworzonoSpraweICS")));
                DSApi.context().commit();
                DSApi._close();
                recipient = ((InOfficeDocument)doc).getRecipientUser();
            }else{

            }
            LoggerFactory.getLogger("krzysiekm").debug("Utworzono now� spraw� dekretuj�c na u�ytkownika " + recipient);
            return new Field("cn", "Utworzono now� spraw� dekretuj�c na u�ytkownika " + recipient, Field.Type.STRING);
        }

        // klikni�to Dodaj do sprawy na popupie
        if(senderField != null && senderField.getKey().equals("DWR_ADD_TO_CASE_COMMIT_POPUP_BUTTON")) {
            List<Long> caseId = DwrUtils.extractIdFromDictonaryField(values, "DWR_ADD_TO_CASE_AVAIL_CASE");
            Preconditions.checkArgument(!caseId.isEmpty());
            DSApi.open(AuthUtil.getSubject(WebContextFactory.get().getHttpServletRequest()));
            DSApi.context().begin();
            Document doc = Document.find(caseId.get(0));
            updateCaseInternalId(fm, values, doc, "DWR_ADD_TO_CASE_AVAIL_CASE", "ADD_TO_CASE_AVAIL_CASE_INTERNAL_ID");
            DataMartManager.addHistoryEntry(fm.getDocumentId(), Audit.create(DataMartDefs.DOCUMENT_FIELD_CHANGE, DSApi.context().getPrincipalName(), sm.getString("PrzypisanoZgloszenieDoSprawyICS")));
            DSApi.context().commit();
            DSApi._close();
            return new Field("Powi�zano zg�oszenie COK ze spraw�", "MSG");
        }
        values.put("DWR_NEW_CASE_INTERNAL_ID", new FieldData(Field.Type.STRING, ""));
        // klikni�to generuj numer sprawy na popupie do tworzenia sprawy
        if(senderField != null && senderField.getKey().equals("DWR_NEW_CASE_GENERATE_INTERNAL_ID_BUTTON")) {
            String selectedAreaId = DwrUtils.getStringValue(values, "DWR_NEW_CASE_AREA");
            if (StringUtils.isEmpty(selectedAreaId)) {
                return new Field(sm.getString("SelectAreaToGenerateCaseNumber"), "MSG");
            } else {
                int areaId = Integer.parseInt(selectedAreaId);
                try {
                    String caseString = DwrUtils.getStringValue(values, "DWR_NEW_CASE_INTERNAL_ID");
                    CaseNumber caseNumber;
                    try {
                        caseNumber = new CaseNumber(caseString);

                    } catch (IllegalArgumentException e) {
                        DSApi.openAdmin();
                        caseNumber = CokUtils.generateCaseNumber(areaId);
                        DSApi._close();
                    }
                    values.get("DWR_NEW_CASE_INTERNAL_ID").setStringData(caseNumber.toString());
                } catch (SQLException e) {
                    LOG.error(e.getMessage(), e);
                } catch (EdmException e) {
                    LOG.error(e.getMessage(), e);
                }
            }
//            setCurrentUserAsDocumentAuthor(fm);
        }

        // u�ytkownik usun�� za��cznik do wys�ania
        if(senderFieldCn.equals(DWR_ORG_EMAIL_ATTACHMENT)) {
//            System.out.println("CHANGE! ");
            final List<Long> dicIds = DwrUtils.extractIdFromDictonaryField(values, ORG_EMAIL_ATTACHMENT);
//            System.out.println(dicIds);
            HttpSession session  = WebContextFactory.get().getSession();
            DwrAttachmentStorage storage = DwrAttachmentStorage.getInstance();

            // id-ki oryginalnych za��cznik�w
            DSApi.open(AuthUtil.getSubject(WebContextFactory.get().getHttpServletRequest()));
            List<Attachment> orgAtts = CokUtils.getOriginalAttachments(fm);
            DSApi._close();
            List<Long> orgIds = Lists.transform(orgAtts, new Function<Attachment, Long>() {
                @Override
                public Long apply(Attachment attachment) {
                    return attachment.getId();
                }
            });

            // usuwamy te id, kt�rych nie ma w li�cie wys�anej przez u�ytkownika
            Collection<Long> removedIds = Collections2.filter(orgIds, new Predicate<Long>() {
                @Override
                public boolean apply(Long attId) {
                    return !dicIds.contains(attId);
                }
            });

            for(Long id : removedIds) {
                storage.remove(session, ORG_EMAIL_ATTACHMENT, id.toString());
            }
        }

        prepareFinishDate(values, fm);

        return null;
    }

    private void updateCaseInternalId(FieldsManager fm, Map<String, FieldData> values, Document cokCaseDocument, String ... internalIdSourceFieldCn) throws EdmException {
        Map<String,Object> fieldValues = new HashMap<String, Object>();
        switch(internalIdSourceFieldCn.length){
            case 1:
                fieldValues.put("NEW_CASE_INTERNAL_ID", values.get(internalIdSourceFieldCn[0]).getStringData());
                break;
            case 2:
                fieldValues.put("NEW_CASE_INTERNAL_ID", ((Map)values.get(internalIdSourceFieldCn[0]).getData()).get(internalIdSourceFieldCn[1]).toString());
                break;
            default:
                return;
        }
        DictionaryUtils.addToMultipleTable(cokCaseDocument, "COK_DOCUMENT", fm.getDocumentId().toString());   //#TODO zrobic sprawdzenie czy istnieje Zgloszenie
        fm.getDocumentKind().setWithHistory(fm.getDocumentId(), fieldValues, false);
        TaskSnapshot.updateByDocument(Document.find(fm.getDocumentId()));
    }

    /**
     * Metoda sprawdza czy wybrano pracownika, dzia� b�d� nie zaznaczono nic(wtedy przyjmujemy, �e dekretujemy zadanie na ca�� sekcj� COK) i zwraca odpowiedniego assignee   id=u:ilplusr250;d:AC10011201EF6C2411B02B75B77E67B654D
     */
    public static Object retriveAssignee(Map<String, FieldData> recipientDictionaryData) throws EdmException {
        Object assignee = null;
        String firstName = null;
        String lastName = null;
        String divisionName = null;
        if(recipientDictionaryData != null){
            String [] choosenAssignee = recipientDictionaryData.get("id").getStringData().split(";");
            //  [0] = {java.util.HashMap$Entry@10053}"id" -> "u:kmalinowski;d:0A00000B013572B7113F7A7EB2CDF7734A7"

            if(choosenAssignee.length == 1){
                for(Object ass : recipientDictionaryData.keySet()){
                    if(ass.toString().toLowerCase().contains("firstname")){
                        firstName = recipientDictionaryData.get(ass).toString();
                    }else if(ass.toString().toLowerCase().contains("lastname")){
                        lastName = recipientDictionaryData.get(ass).toString();
                    }else if(ass.toString().toLowerCase().contains("division")){
                        divisionName = recipientDictionaryData.get(ass).toString();
                    }
                }
                try{
                    LoggerFactory.getLogger("krzysiekm").debug("ASSIGNATION PROCESS");
                    if(StringUtils.isNotEmpty(firstName) && StringUtils.isNotEmpty(lastName)){
                        LoggerFactory.getLogger("krzysiekm").debug("SEARCHING FOR PERSON: " + firstName + " " + lastName);
                        assignee = DSUser.findByFirstnameLastname(firstName, lastName);
                    }else if(StringUtils.isNotEmpty(divisionName)){
                        LoggerFactory.getLogger("krzysiekm").debug("SEARCHING FOR DIVISION: " + divisionName);
                        assignee = DSDivision.find(divisionName);
                    }
                }catch(EdmException e){
                    LoggerFactory.getLogger("krzysiekm").debug("DIDN'T FIND PERSON AND DIVISION");
                    assignee = AcceptanceDivisionImpl.findByCode(COK_ACCEPTATION_CN);
                }
            } else{
                for(String ass : choosenAssignee){
                    if(ass.contains("u:")){
                        DSUser assignedUser = DSUser.findByUsername(ass.split(":")[1]);
                        if(assignedUser != null){
                            assignee = assignedUser;
                            break;
                        }
                    }else if(ass.contains("d:")){
                        DSDivision assignedDivision = DSDivision.find(ass.split(":")[1]);
                        if(assignedDivision != null){
                            assignee = assignedDivision;
                            break;
                        }
                    }
                }
            }
        }
        return assignee;
    }

	private Field sendMail(String actionButtonType, Map<String, FieldData> values, FieldsManager fm) throws EntityNotFoundException, EdmException{

		StringBuilder sb = new StringBuilder("Wys�ano wiadomo�� do ");
		String subject;
		String toEmail;
		Field msg = new Field("cn", sb.toString(), Field.Type.STRING);
		msg.setKind(Kind.MESSAGE);
		boolean contextOpened = false;
		String messageReceipt = null;
		Long messageId;
        DSEmailChannel emailChannel;
		// tworzymy tymczasowe pliki z zwartoscia zalacznikow dodanych z poziomu formatki
		List<File> attachedFiles = prepareAttachments(values, fm);
		if(actionButtonType.equals(DWR_SEND_FORWARD_BUTTON)){

			FieldData receiverData = values.get("DWR_RECEIVER");
			Map<String, FieldData> receiverMap = (Map<String, FieldData>)receiverData.getData();

			boolean atLeastOneEmail = false;
			for(String key : receiverMap.keySet()){
				if(key.contains("RECEIVER_EMAIL")){
					messageReceipt = DwrUtils.getStringValue(values, "DWR_RESPONSE_CONTENT");
					toEmail = receiverMap.get(key).getStringData();
					if(!toEmail.isEmpty()){
						atLeastOneEmail = true;
						if(deliveryType == DeliveryType.EMAIL){
							subject = "FW: " + values.get("DWR_SUBJECT_EMAIL").getStringData();
						}else{
							subject = "FW: " + StringUtils.defaultIfEmpty(DwrUtils.getStringValue(values, "DWR_SUBJECT_MANUAL"), "");
						}
						try {
							InternetAddress addr = new InternetAddress(toEmail);
							toEmail = addr.getAddress();
							sb.append(toEmail + ", ");
						} catch(AddressException e) {
							LOG.error(e.getMessage(), e);
						}
						try {
							contextOpened = DSApi.openContextIfNeeded();
							// jak zapisany dokument
							if(fm.getDocumentId() != null){
								if(deliveryType == DeliveryType.EMAIL) {
                                    messageId = ((InOfficeDocument)InOfficeDocument.find(fm.getDocumentId())).getMessageId();
                                    emailChannel = MailMessage.find(messageId).getEmailChannel();
									if(EmailUtils.verifyEmailChannel(msg, messageId)){
                                        ((Mailer) ServiceManager.getService(Mailer.NAME)).send(toEmail, toEmail, subject, messageReceipt, true, EmailConstants.TEXT_HTML, emailChannel, attachedFiles);
									}
								} else {
                                    emailChannel = getValidEmailChannel();
                                    ((Mailer) ServiceManager.getService(Mailer.NAME)).send(toEmail, toEmail, subject, messageReceipt, true, EmailConstants.TEXT_HTML, emailChannel, attachedFiles);
								}
							}else{ // nie ma dokumentu
                                emailChannel = getValidEmailChannel();
								if(emailChannel != null){
                                    ((Mailer) ServiceManager.getService(Mailer.NAME)).send(toEmail, toEmail, subject, messageReceipt, true, EmailConstants.TEXT_HTML, emailChannel, attachedFiles);
								}else{
									msg.setLabel("Nie znaleziono w systemie �adnego aktywnego kana�u email");
								}
							}
							DSApi.closeContextIfNeeded(contextOpened);
						} catch (EdmException e) {
							msg.setLabel(e.getMessage());
							LOG.error(e.getMessage(), e);
							DSApi.closeContextIfNeeded(contextOpened);
						}
					}
				}
			}
			if(atLeastOneEmail) {
				try {
					msg = new Field("cn", sb.toString().substring(0, sb.toString().lastIndexOf(",")), Field.Type.STRING);
					msg.setKind(Kind.MESSAGE);
					msg.setLabel(sb.toString().substring(0, sb.toString().lastIndexOf(",")));
					forwardTo = sb.toString().substring(0, sb.toString().lastIndexOf(","));
					// ustawiamy tresc komunikatu, ktory pojawi sie w historii dokumneut po wyslaniu odpowiedzi na otrzymanego maila
					FieldLogic fieldLogic = fm.getDocumentKind().getFieldByCn("SENT_RESP").prepareFieldLogic();
					if(fieldLogic instanceof SentResponseHtmlEditorFieldLogic){
						String historyEntryMessage = "Wys�ano wiadomo�� do " + forwardTo;
						((SentResponseHtmlEditorFieldLogic)fieldLogic).setHistoryEntryMessage(historyEntryMessage);
					}
					// zapisujemy do bazy wartosc wyslanej odpowiedzi na otrzymanego maila
					if(fm.getDocumentId() != null){
						Map<String, Object> fieldValues = new HashMap<String, Object>();
						fieldValues.put("SENT_RESP", messageReceipt);
                        fieldValues.put("EMAIL_ATTACHMENT", getStoredDWRAttachments());
						contextOpened = DSApi.openContextIfNeeded();
						fm.getDocumentKind().setWithHistory(fm.getDocumentId(), fieldValues, false);
						DSApi.closeContextIfNeeded(contextOpened);
						//	wy�wietlenie na formatce aktualnie wys�anej wiadomo�ci
						setSentResponseContent(values, messageReceipt);
					}else{
						setSentResponseContent(values, messageReceipt);
					}
				} catch (EdmException e) {
					msg.setLabel(e.getMessage());
					LOG.error(e.getMessage(), e);
					DSApi.closeContextIfNeeded(contextOpened);
				}
			} else {
				msg = new Field("MESSAGE", "Nale�y wybra� przynajmniej jeden adres e-mail", Field.Type.STRING);
				msg.setKind(Kind.MESSAGE);
			}
		}else if(actionButtonType.equals(DWR_SEND_BUTTON)){
			String messageResponse = DwrUtils.getStringValue(values, "DWR_RESPONSE_CONTENT");
			toEmail = values.get("DWR_SENDER").getStringData();
			subject = "RE: " + values.get("DWR_SUBJECT_EMAIL").getStringData();
			sb.append(StringEscapeUtils.escapeHtml(values.get("DWR_SENDER").getStringData()));
			try {
				InternetAddress addr = new InternetAddress(toEmail);
				toEmail = addr.getAddress();
				msg.setLabel(sb.toString());
			} catch(AddressException e) {
				LOG.error(e.getMessage(), e);
			}

			try {
				contextOpened = DSApi.openContextIfNeeded();
				messageId = ((InOfficeDocument)InOfficeDocument.find(fm.getDocumentId())).getMessageId();
				if(EmailUtils.verifyEmailChannel(msg, messageId)){
                    emailChannel = MailMessage.find(messageId).getEmailChannel();
                    ((Mailer) ServiceManager.getService(Mailer.NAME)).send(toEmail, toEmail, subject, messageResponse, true, EmailConstants.TEXT_HTML, emailChannel, attachedFiles);
					//	ustawiamy tresc komunikatu, ktory pojawi sie w historii dokumneut po wyslaniu odpowiedzi na otrzymanego maila
					FieldLogic fieldLogic = fm.getDocumentKind().getFieldByCn("SENT_RESP").prepareFieldLogic();
					if(fieldLogic instanceof SentResponseHtmlEditorFieldLogic){
						String historyEntryMessage = "Wys�ano wiadomo�� do " + toEmail;
						((SentResponseHtmlEditorFieldLogic)fieldLogic).setHistoryEntryMessage(historyEntryMessage);
					}
					// zapisujemy do bazy wartosc wyslanej odpowiedzi na otrzymanego maila
					Map<String, Object> fieldValues = new HashMap<String, Object>();
					fieldValues.put("SENT_RESP", messageResponse);
                    fieldValues.put("EMAIL_ATTACHMENT", getStoredDWRAttachments());
					fm.getDocumentKind().setWithHistory(fm.getDocumentId(), fieldValues, false);
					//	wy�wietlenie na formatce aktualnie wys�anej wiadomo�ci
					setSentResponseContent(values, messageResponse);
					setResponseContent(values, prepareQuotedMessage(values, fm, DWR_SEND_BUTTON));
				}
				DSApi.closeContextIfNeeded(contextOpened);
			} catch (EdmException e) {
				LOG.error(e.getMessage(), e);
				msg.setLabel(e.getMessage());
			}

		}
		removeTemporaryFiles(attachedFiles);
		return msg;
	}

	private void removeTemporaryFiles(List<File> files){
		for(File file : files){
			file.delete();
		}
        DwrAttachmentStorage.getInstance().removeAll(WebContextFactory.get().getSession(), null);
	}

	private DSEmailChannel getValidEmailChannel() throws EdmException{
		List<DSEmailChannel> emailChannelList = DSEmailChannel.list();

		for(DSEmailChannel channel : emailChannelList){
			if(channel.isEnabled() && channel.isSmtpEnabled()){
				return channel;
			}
		}
		return null;
	}

    private String getUserMailFooter(){
        boolean contextOpened = DSApi.isContextOpen();
        try {
            if(!contextOpened){
                DSApi.open((javax.security.auth.Subject)WebContextFactory.get().getSession(false).getAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY));
                contextOpened = true;
            }
            String footer = null;

            if(UserAdditionalData.findByUserId(DSApi.context().getDSUser().getId()) != null)
                footer = UserAdditionalData.findByUserId(DSApi.context().getDSUser().getId()).getMailFooter();
            if(footer != null){
                footer = "<br /><br />" + UserAdditionalData.findByUserId(DSApi.context().getDSUser().getId()).getMailFooter().replace("\n", "<br/>").replace("\r", "");
            }else{
                footer = "";
            }
            return footer;
        } catch (EdmException e) {
            LOG.error(e.getMessage(), e);
            return "";
        } finally{
            if(contextOpened)
                DSApi._close();
        }
    }

	private String prepareQuotedMessage(Map<String, FieldData> values, FieldsManager fm, String clickedButtonType) throws DocumentNotFoundException, EdmException{

		String date = "";
		String header = "";
		String content = "";
		String quotedMessage = "";

		if(deliveryType == DeliveryType.EMAIL && clickedButtonType.equals(DWR_RESPONSE_BUTTON)){
			date = getLastQuotedMessageDate(values);
			header = prepareQuoteHeader(values, fm, date, clickedButtonType);
		}else if(clickedButtonType.equals(DWR_FORWARD_RESPONSE_BUTTON)){
//			content = "<br /><br />- - - Wiadomo�� przes�ana dalej - - -";
			content = ORIGINAL_MSG;
		}

		content += getLastQuotedMessageContent(values, fm);
		quotedMessage = header + content;

		return getUserMailFooter() + quotedMessage;
	}

	private String getLastQuotedMessageContent(Map<String, FieldData> values, FieldsManager fm) throws EdmException {

		StringBuilder sb = new StringBuilder();
		String quotedMessage;


		//	sprawdzamy czy uprzednio powstala inna odpowiedz na maila z zadania
        if(deliveryType == DeliveryType.EMAIL)
		    quotedMessage = DwrUtils.getStringValue(values, "DWR_SENT_RESP");
        else
            quotedMessage = DwrUtils.getStringValue(values, "DWR_CONVERSATION_DESCRIPTION");

        if(StringUtils.isEmpty(quotedMessage)) {
			if(deliveryType == DeliveryType.EMAIL){
                if(fm.getValue("CONTENTDATA") != null){
                    quotedMessage = fm.getValue("CONTENTDATA").toString();
                }else{
                    quotedMessage = "";
                    LOG.error("Nieudane pobranie wiadomo�ci e-mail dla dokumentu o id {}", fm.getDocumentId());
                }
			}else{
                if(fm.getValue("CONVERSATION_DESCRIPTION") != null){
                    quotedMessage = fm.getValue("CONVERSATION_DESCRIPTION").toString();
                }else{
                    quotedMessage = "";
                    LOG.error("Nieudane pobranie opisu rozmowy dla dokumentu o id {}", fm.getDocumentId());
                }
			}
		}

		if(quotedMessage == null)
			quotedMessage = "";

		sb.append(quotedMessage);
		return sb.toString();

	}

	private String getLastQuotedMessageDate(Map<String, FieldData> values){
//		String DATE_PATTERN = "\n'Dnia' dd - MM - yyyy 'o godzinie' HH:mm:ss 'napisano:'\n";
		String DATE_PATTERN = "\nEEEE, d MMMM yyyy, HH:mm:ss\n";
		SimpleDateFormat format = new SimpleDateFormat(DATE_PATTERN);
		String date = "";
		if(StringUtils.isEmpty(DwrUtils.getStringValue(values, "DWR_SENT_RESP"))) {
			date = format.format(values.get("DWR_SENTDATE").getDateData());
		}else{
			date = format.format(Calendar.getInstance().getTime());
		}
		return date;
	}

	private String prepareQuoteHeader(Map<String, FieldData> values, FieldsManager fm, String quotedMessageDate, String clickedButtonType) throws DocumentNotFoundException, EdmException{
		/*
		From: cok@impuls-leasing.pl [mailto:cok@impuls-leasing.pl]
		Sent: Thursday, April 25, 2013 11:19 PM
		To: darls@op.pl
		Subject: RE: Test z nocki
		 */
		String header = "";
		String from = "Od: ";
		String to = "Do: ";
		String subject = "Temat: ";
		try {
			if(clickedButtonType.equals(DWR_RESPONSE_BUTTON) || clickedButtonType.equals(DWR_FORWARD_RESPONSE_BUTTON)){
				InternetAddress addr;
				addr = new InternetAddress(values.get("DWR_SENDER").getStringData());
				from += addr.getAddress();
				to += getChannelUserName(fm);
				subject += values.get("DWR_SUBJECT_EMAIL").getStringData();
			}else if(clickedButtonType.equals(DWR_SEND_BUTTON)){
				from += getChannelUserName(fm);
				InternetAddress addr = new InternetAddress(values.get("DWR_SENDER").getStringData());
				to += addr.getAddress();
				subject += "RE: " + values.get("DWR_SUBJECT_EMAIL").getStringData();
			}else if(clickedButtonType.equals(DWR_SEND_FORWARD_BUTTON)){
				from += getChannelUserName(fm);
				to += forwardTo;
				subject += "FW: " + values.get("DWR_SUBJECT_EMAIL").getStringData();
			}
		} catch (AddressException e) {
			LOG.error(e.getMessage(), e);
		}
		header = "<br /><br />" + from + "<br />" + "Wys�ano: " + quotedMessageDate + "<br />" + to + "<br />" + subject + "<br />";

		return header;
	}

	private String getChannelUserName(FieldsManager fm) throws EdmException{
		if(channelUserName == null || channelUserName.equals("")){
			boolean contextOpened = DSApi.openContextIfNeeded();
			Long messageId = ((InOfficeDocument)InOfficeDocument.find(fm.getDocumentId())).getMessageId();
            if(messageId != null){
                MailMessage mail = MailMessage.find(messageId);
                DSEmailChannel channel = mail.getEmailChannel();
                DSApi.closeContextIfNeeded(contextOpened);
                return channelUserName = channel.getUser();
            }else{
                channelUserName = "";
            }
		}
		return channelUserName;
	}

	private void prepareDeliveryType(Map<String, FieldData> values) throws EdmException {
        Long longVal = DwrUtils.getLongValue(values, "DWR_DELIVERY_TYPE");
        if(longVal != null) {
            this.deliveryType = DeliveryType.fromValue(longVal.intValue());
        }

        if(longVal == null || this.deliveryType == null) {
            throw new EdmException(sm.getString("NieznanyTypPola") + " " + (longVal != null ? longVal : "[NULL]"));
        }
    }

    private void prepareDeliveryType(FieldsManager fm) throws EdmException {
        if(fm.getValue("DELIVERY_TYPE") != null) {
            String deliveryType = (String) fm.getValue("DELIVERY_TYPE");
            Integer deliveryVal = fm.getField("DELIVERY_TYPE").getEnumItemByTitle((String)fm.getValue("DELIVERY_TYPE")).getId();
            if(deliveryVal != null) {
                this.deliveryType = DeliveryType.fromValue(deliveryVal);
            }

            if(deliveryVal == null || this.deliveryType == null) {
                throw new EdmException(sm.getString("NieznanyTypPola") + " " + (deliveryVal != null ? deliveryVal : "[NULL]"));
            }
        }
    }

	private List<File> prepareAttachments(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
		List<DwrFileItem> mailAttachments = getStoredDWRAttachments();
		File tempFile;
		List<File> attachedFiles = new ArrayList<File>();
		try {
			for(FileItem fileItem : mailAttachments){
				tempFile = new File(UploadDriver.tempDir + "" + fileItem.getSize() + "_ORGNAME_" + fileItem.getName());
				if(tempFile.exists()){
					fileItem.write(tempFile);
					attachedFiles.add(tempFile);
				}else if(tempFile.createNewFile()){
					fileItem.write(tempFile);
					attachedFiles.add(tempFile);
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return null;
		}

		return attachedFiles;
	}

    private List<DwrFileItem> getStoredDWRAttachments(){
        return DwrAttachmentStorage.getInstance().getFiles(WebContextFactory.get().getSession(false), "EMAIL_ATTACHMENT", ORG_EMAIL_ATTACHMENT);
    }
	/**
	 * Metoda generuje dat� zako�czenia zadania w przypadku wybrania statusu Zako�czone lub Zg�oszenie nies�uszne,
	 * b�d� usuwa informacj� o dacie w przypadku zmiany statusu na dowoln� inn� warto��.
	 * @param values
	 * @param fm
	 * @throws EntityNotFoundException
	 * @throws EdmException
	 */
	private void prepareFinishDate(Map<String, FieldData> values, FieldsManager fm) throws EntityNotFoundException, EdmException{
		if(values.containsKey("DWR_TASK_STATUS") && values.get("DWR_TASK_STATUS").getEnumValuesData().getSelectedId().length() > 0){
			String selectedStatusId = values.get("DWR_TASK_STATUS").getEnumValuesData().getSelectedId();
			String statusValue = ((DataBaseEnumField)fm.getField("TASK_STATUS")).getEnumItem(selectedStatusId).getTitle();
			FieldData finishDate = values.get("DWR_FINISH_DATE");
			// ustawiono status na Zako�czone lub Zg�oszenie nies�uszne
			if(statusValue.contains("Zako�czone") || statusValue.contains("Zg�oszenie nies�uszne")){
				Date finishDateTime = Calendar.getInstance().getTime();
				finishDate.setDateData(finishDateTime);
			}else{
				if(values.get("DWR_FINISH_DATE") != null){
					finishDate.setDateData(null);
				}
			}
		}
	}

	private void setResponseContent(Map<String, FieldData> values, String responseContent){
		FieldData resp = values.get("DWR_RESPONSE_CONTENT");
		HtmlEditorValue val = new HtmlEditorValue(responseContent);
		resp.setHtmlEditorData(val);
	}

	private void setSentResponseContent(Map<String, FieldData> values, String responseContent){
		FieldData resp = values.get("DWR_SENT_RESP");
		HtmlEditorValue val = new HtmlEditorValue(responseContent);
        if(resp == null){
            resp = new FieldData();
        }
		resp.setHtmlEditorData(val);
	}

	private List<Long> getUmowaListId(Long contractorId) throws EdmException {
		boolean contextOpened = DSApi.openContextIfNeeded();
		List<Long> umowaIdList = new ArrayList<Long>();
		Statement statement = null;
		ResultSet result = null;
		StringBuilder sqlQuery = new StringBuilder("SELECT id FROM DSC_ILPOL_UMOWA_DICTIONARY WHERE (CONTRACTOR_ID = " + contractorId+ ")");
		try {
			statement = DSApi.context().createStatement();
			result = statement.executeQuery(sqlQuery.toString());
			while (result.next()) {
				Long id = result.getLong(1);
				umowaIdList.add(id);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} finally {
			DSApi.context().closeStatement(statement);
		}
		DSApi.closeContextIfNeeded(contextOpened);
		return umowaIdList;
	}

    private List<Long> getCokCaseListId(Long documentId) throws EdmException {
        boolean contextOpened = DSApi.openContextIfNeeded();
        List<Long> cokCaseIdList = new ArrayList<Long>();
        Statement statement = null;
        ResultSet result = null;
        StringBuilder sqlQuery = new StringBuilder("SELECT ID FROM DSC_ILPOL_CASE_DOC WHERE (DOCUMENT_ID = " + documentId + ")");
        try {
            statement = DSApi.context().createStatement();
            result = statement.executeQuery(sqlQuery.toString());
            while (result.next()) {
                Long id = result.getLong(1);
                cokCaseIdList.add(id);
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        } finally {
            DSApi.context().closeStatement(statement);
        }
        DSApi.closeContextIfNeeded(contextOpened);
        return cokCaseIdList;
    }

    public void onStartProcess(OfficeDocument document, ActionEvent event, Object assignee) throws EdmException {
        setAssignee(assignee);
        onStartProcess(document, event);
    }

	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		try
		{
            DSUser author = DSUser.findByUsername(document.getCreatingUser());
            String user = author.getName();
            String fullName = author.getLastname() + " " + author.getFirstname();

			Map<String, Object> map = Maps.newHashMap();
            //wartosci odpowiadajace za ustawienie osoby dekretujacej zadanie
            map.put("previousAssignee", user);
            map.put("currentAssignee", user);
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

            java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
            perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
            perms.addAll(documentPermissions);
            this.setUpPermission(document, perms);

			DSApi.context().watch(URN.create(document));
		}
		catch (Exception e)
		{
			LOG.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

	private void setCaseRefValues(String dictionaryName, Map<String, FieldData> values, Map<String, Object> results) {
		DwrUtils.setRefValueEnumOptions(values, results, "AREA", dictionaryName + "_", "CATEGORY", DSC_CASE_CAT_TABLE, null, EMPTY_NOT_HIDDEN_ENUM_VALUE, "");
		DwrUtils.setRefValueEnumOptions(values, results, "CATEGORY", dictionaryName + "_", "SUBCATEGORY", DSC_CASE_SUBCAT_TABLE, null, EMPTY_NOT_HIDDEN_ENUM_VALUE, "");
	}



	private boolean canChangeCaseNumber() {
		if(StringUtils.isEmpty(COK_GUID)) {
			return true;
		}
//		System.out.println("Checking " + principalName + " guid = " + COK_GUID);
		try {
			DSDivision cokDivision = DSDivision.find(COK_GUID);
			List<DSUser> cokUsers = Arrays.asList(cokDivision.getUsers(true));
			DSUser loggedUser = DSApi.context().getDSUser();
			boolean cont = cokUsers.contains(loggedUser);
//			for(DSUser usr : cokUsers) {
//				System.out.println(usr.getName());
//			}
//
//			System.out.println("Cont? " + cont);

			return cont;
		} catch (UserNotFoundException e) {
			LOG.error(e.getMessage(), e);
		} catch (EdmException e) {
			LOG.error(e.getMessage(), e);
		}
		return false;
	}

    /**
     * Metoda pobiera id zalacznikow powiazanych z mailami wyslanymi z poziomu dokumentu
     * @param fm
     * @return
     * @throws EdmException
     * @throws SQLException
     */
    private LinkedList<Long> getMailAttachments(FieldsManager fm) throws EdmException, SQLException {


        boolean openedContext = DSApi.openContextIfNeeded();
        StringBuilder query = new StringBuilder();
        String attachmentsTable = ((AbstractDictionaryField)fm.getField("ATTACHMENT")).getTable();
        String multipleTable = fm.getDocumentKind().getMultipleTableName();
        query.append("SELECT ID FROM " + attachmentsTable + " WHERE ATT IN (SELECT ID FROM DS_ATTACHMENT WHERE DOCUMENT_ID = " + fm.getDocumentId() + ")");
        PreparedStatement ps = DSApi.context().prepareStatement(query.toString());
        ResultSet rs = ps.executeQuery();
        LinkedList<Long> attachmentsId = new LinkedList<Long>();
        while(rs.next()){
            attachmentsId.add(rs.getLong(1));
        }
        DSApi.closeContextIfNeeded(openedContext);
        return attachmentsId;
    }


    private Long getFaxSourceAttachment(FieldsManager fm) throws EdmException, SQLException {
        boolean openedContext = DSApi.openContextIfNeeded();
        Document doc = Document.find(fm.getDocumentId());
        Preconditions.checkNotNull(doc);
        Attachment att = doc.getAttachmentByCn(IncomingFax.FAX_ATTACHMENT_CN);
        Preconditions.checkNotNull(att);

        StringBuilder query = new StringBuilder();
        String attachmentsTable = ((AbstractDictionaryField)fm.getField("FAX_ATTACHMENT")).getTable();
        query.append("SELECT ID FROM " + attachmentsTable + " WHERE ATT = ?");
        PreparedStatement ps = DSApi.context().prepareStatement(query.toString());
        ps.setLong(1, att.getId());
        ResultSet rs = ps.executeQuery();
        Long attId = null;
        while(rs.next()) {
            attId = rs.getLong(1);
            break;
        }
        DSApi.closeContextIfNeeded(openedContext);

        return attId;

    }

	/**
	 * Odfiltrowujemy za��czniki dokumentu z tych za��cznik�w, kt�re s� powi�zane z wiadomo�ciami e-mail i s� �r�d�owymi faksu
	 */
	@Override
	public List<Attachment> filterAttachments(Document document, List<Attachment> attachments) throws EdmException {
        List<Attachment> ret = new ArrayList<Attachment>();
        int deliveryType = document.getFieldsManager().getIntegerKey("DELIVERY_TYPE");

        for(Attachment att : attachments) {
            if(MailMessage.MAIL_ATTACHMENT_CN.equals(att.getCn()) ||
                    (!IncomingFax.FAX_ATTACHMENT_CN.equals(att.getCn()) && att.getCn() != null)
                    || deliveryType == DELIVERY_TYPE_DOCUMENT_IN) {
                ret.add(att);
            }
        }

        return ret;
    }

    @Override
    public boolean canReopenNow() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void reopenNow(OfficeDocument doc) throws EdmException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setReopenMarkerOn(OfficeDocument doc) throws EdmException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void modifyDwrFacade(HttpServletRequest req, Map<String, Object> dwrSessionValues, FieldsManager fieldsManager, DwrFacade dwrfacade) throws EdmException {
        prepareDeliveryType(fieldsManager);

        try {
            Attachment att;
            LinkedList<Long> attachmentsId = new LinkedList<Long>();
            Object deliveryType = ((Map<String, Object>)dwrSessionValues.get("FIELDS_VALUES")).get("DELIVERY_TYPE");
            if(deliveryType.equals(DELIVERY_TYPE_FAX)){
                List<Long> faxAttsId = new ArrayList<Long>();
                faxAttsId.add(getFaxSourceAttachment(fieldsManager));
//                LoggerFactory.getLogger("krzysiekm").debug("modifyDwrFacade: " + IncomingFax.FAX_ATTACHMENT_CN);
                ((Map)dwrSessionValues.get("FIELDS_VALUES")).put("FAX_ATTACHMENT", faxAttsId);
            }

//            LoggerFactory.getLogger("krzysiekm").debug("modifyDwrFacade: " + "MAIL");
            attachmentsId = getMailAttachments(fieldsManager);
            ((Map)dwrSessionValues.get("FIELDS_VALUES")).put("ATTACHMENT", attachmentsId);

            if(this.deliveryType == DeliveryType.EMAIL) {
                //  ustawienie informacji o adresie email, na podstawie ktorego zostalo utworzone aktualnie przetwarzane Zgloszenie COK
                String channelName = getChannelUserName(fieldsManager);
                ((Map)dwrSessionValues.get("FIELDS_VALUES")).put("EMAIL_CHANNEL_USERNAME", channelName);
            }
        } catch (SQLException e) {
            LOG.error("COK_LOGIC:" + e.getMessage());
        }
//        LoggerFactory.getLogger("krzysiekm").debug("modifyDwrFacade");
    }

    @Override
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException {
        super.validate(values, kind, documentId);
        values.put("FAX_SEND_DATETIME", Calendar.getInstance().getTime());
    }

    @Override
    public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException {
        super.onSaveActions(kind, documentId, fieldValues);
    }

    @Override
    public void correctValues(Map<String, Object> fieldValues, DocumentKind kind) throws EdmException {
        super.correctValues(fieldValues, kind);

        if(fieldValues.containsKey("CASE_DICTIONARY")) {
            // tworzony now� spraw�
            Map<String, FieldData> caseValues = (Map<String, FieldData>) fieldValues.get("CASE_DICTIONARY");
            String areaId = caseValues.get("AREA").getStringData();
            String categoryId = caseValues.get("CATEGORY").getStringData();
            String subcategoryId = caseValues.get("SUBCATEGORY").getStringData();
            String internalId = caseValues.get("INTERNAL_ID").getStringData();
            String ctime = caseValues.get("CTIME").getStringData();
            String description = "";
            if(caseValues.get("DESCRIPTION") != null)
                StringUtils.trimToEmpty(caseValues.get("DESCRIPTION").getStringData());


            Map<String, Object> newCaseValues = Maps.newHashMap();
            newCaseValues.put("AREA", areaId);
            newCaseValues.put("CATEGORY", categoryId);
            newCaseValues.put("SUBCATEGORY", subcategoryId);
            newCaseValues.put("INTERNAL_ID", internalId);
            newCaseValues.put("CTIME", ctime);
            newCaseValues.put("DESCRIPTION", description);

            DocumentKind caseDockind = DocumentKind.findByCn(IlpolCokCaseLogic.DOCKIND_NAME);
            Preconditions.checkNotNull(caseDockind, "Dockind not found %s", IlpolCokTaskLogic.DOCKIND_NAME);
            InOfficeDocument doc = new InOfficeDocument();
            CokUtils.createDocumentFromUserAction(doc, caseDockind, CokUtils.retriveLoggedUser(), null, newCaseValues, "Sprawa COK", "Sprawa COK", "Sprawa COK");

            FieldData docId = new FieldData(Field.Type.STRING, doc.getId().toString());
            ((Map<String, FieldData>) fieldValues.get("CASE_DICTIONARY")).put("DOCUMENT_ID", docId);
        }
    }

    @Override
    public void acceptTask(Document document, ActionEvent event, String activity) {
        if(document.getType() == DocumentType.INCOMING){
            ((InOfficeDocument)document).setCreatingUser(DSApi.context().getPrincipalName());
            try {
                LoggerFactory.getLogger("krzysiekm").debug("IlpolCokLogic.acceptTask() " + DSApi.context().getPrincipalName() + " " + document.getFieldsManager().getValue("DELIVERY_TYPE").toString());
                document.setAuthor(DSApi.context().getPrincipalName());
            } catch (EdmException e) {
                LOG.debug("ILPOL: Niepowiod�o si� ustawienie autora dla dokumentu o id " + document.getId() + ": " + e.getMessage(), e);
            }
        }
    }


    /*
	 * cok.column.dockindBusinessAtr1 = Nadawca (e-mail)
	 * cok.column.dockindBusinessAtr2 = Temat
	 * cok.column.dockindBusinessAtr3 = Numer sprawy
	 * cok.column.dockindBusinessAtr4 = Kanal email
	 * cok.column.dockindBusinessAtr5 = Rodzaj kontaktu
	 * cok.column.dockindBusinessAtr6 = Kontrahent
	 * @see pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic#getTaskListParams(pl.compan.docusafe.core.dockinds.DocumentKind, long, pl.compan.docusafe.core.office.workflow.TaskSnapshot)
	 */
    @SuppressWarnings("unchecked")
    @Override
    public TaskListParams getTaskListParams(DocumentKind kind, long documentId, TaskSnapshot task) throws EdmException {
        LoggerFactory.getLogger("krzysiekm").debug("COK LOGIC PARAM");
        TaskListParams params = new TaskListParams();
        String mailSender = "brak";
        String mailSubject = "brak";
        String caseNumber = "brak";
        String deliveryType = "brak";
        String contractor = "brak";
        String emailChannel = "brak";
//        DWR_ADD_TO_CASE_AVAIL_CASE ADD_TO_CASE_AVAIL_CASE_INTERNAL_ID DWR_NEW_CASE_INTERNAL_ID
        try {
            FieldsManager fm = kind.getFieldsManager(documentId);

            if(fm.getValue("SENDER") != null)
                mailSender = fm.getValue("SENDER").toString();

            if(fm.getValue("SUBJECT_EMAIL") != null){
                mailSubject = fm.getValue("SUBJECT_EMAIL").toString();
            }else if(fm.getValue("SUBJECT_MANUAL") != null){
                mailSubject = fm.getValue("SUBJECT_MANUAL").toString();
            }

            //jako alternatywe dla zapisywania wartosci numeru sprawy w kolumnie NEW_CASE_INTERNAL_ID dokumentu cok,
            // mozna zastosowac identyfikacje sprawy cok powiazanej z dokumentem cok a nastepnie pobranie informacji o internal_id ze sprawy cok,
            // ale wydaje sie, ze jest to mniej wydajne rozwiazanie
            List<String> caseInternalId = new ArrayList<String>();
            Statement statement = null;
            ResultSet result = null;
            StringBuilder sqlQuery = new StringBuilder(
                    "SELECT COK_CASE.INTERNAL_ID FROM DSC_ILPOL_CASE_DOC COK_CASE " +
                            "INNER JOIN DSC_ILPOL_CASE_MULTIPLE COK_CASE_COK_DOC_MLT ON COK_CASE.DOCUMENT_ID = COK_CASE_COK_DOC_MLT.DOCUMENT_ID " +
                            "WHERE COK_CASE_COK_DOC_MLT.FIELD_VAL = '" + documentId + "'");
            try {
                statement = DSApi.context().createStatement();
                result = statement.executeQuery(sqlQuery.toString());
                while (result.next()) {
                    String internalId = result.getString(1);
                    caseInternalId.add(internalId);
                }
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
            } finally {
                DSApi.context().closeStatement(statement);
            }

            if(caseInternalId.size() > 0){
                caseNumber = caseInternalId.get(0);
            }

            if(fm.getValue("KLIENT") != null) {
                try {
                    Map<String, Object> contractorDic = (Map<String, Object>) fm.getValue("KLIENT");
                    if(contractorDic.get("KLIENT_NAME") != null) {
                        contractor = contractorDic.get("KLIENT_NAME").toString();
                    }
                } catch(ClassCastException e) {
                    LOG.error(e.getMessage(), e);
                }
            }


            if(fm.getValue("DELIVERY_TYPE") != null) {
                deliveryType = (String) fm.getValue("DELIVERY_TYPE");
                Integer deliveryVal = fm.getField("DELIVERY_TYPE").getEnumItemByTitle((String)fm.getValue("DELIVERY_TYPE")).getId();
                if(deliveryVal != null) {
                    this.deliveryType = DeliveryType.fromValue(deliveryVal);
                }

                if(deliveryVal == null || this.deliveryType == null) {
                    throw new EdmException(sm.getString("NieznanyTypPola") + " " + (deliveryVal != null ? deliveryVal : "[NULL]"));
                }

                if(this.deliveryType == DeliveryType.EMAIL){
                    emailChannel = getChannelUserName(fm);
                    if(emailChannel.lastIndexOf("@") != -1)
                        emailChannel = emailChannel.substring(0, emailChannel.lastIndexOf("@"));
                }else if(this.deliveryType == DeliveryType.FAX){
                }else if(this.deliveryType == DeliveryType.IMPORT){
                }else if(this.deliveryType == DeliveryType.DOCUMENT_IN){
                }else if(this.deliveryType == DeliveryType.MANUAL){
                }

            }

        } catch(Exception e) {
            LOG.error(e.getMessage(), e);
        } finally {
            params.setAttribute(contractor, 0);
            params.setAttribute(mailSubject, 1);
            params.setAttribute(mailSender, 2);
            params.setAttribute(caseNumber, 3);
            params.setAttribute(emailChannel, 4);
            params.setAttribute(deliveryType, 5);
        }

        return params;
    }

    private void initOriginalAttachments(final FieldsManager fm, Map<String, FieldData> values) throws EdmException {
        HttpSession session = WebContextFactory.get().getSession();

        List<Attachment> attachments = CokUtils.getOriginalAttachments(fm);
        List<Long> attIds = Lists.transform(attachments, new Function<Attachment, Long>() {
            @Override
            public Long apply(Attachment attachment) {
                return attachment.getId();
            }
        });

        DwrAttachmentStorage storage = DwrAttachmentStorage.getInstance();
        List<DwrFileItem> dwrFileItems = Lists.transform(attachments, new Function<Attachment, DwrFileItem>() {
            @Override
            public DwrFileItem apply(Attachment attachment) {
                return new DwrFileItem(new AttachmentRevisionFileItem(attachment.getMostRecentRevision()), attachment.getId().toString());
            }
        });
        storage.removeAll(session, null);
        storage.addFiles(session, ORG_EMAIL_ATTACHMENT, dwrFileItems);

        Map <String, Object> fmValues = Maps.newHashMap();
        fmValues.put(ORG_EMAIL_ATTACHMENT, attIds);
        fm.reloadValues(fmValues);

        FieldData fd = new FieldData(Field.Type.STRING, attIds.toString());
        values.put(DWR_ORG_EMAIL_ATTACHMENT, fd);

    }

    @Override
    public void modifyTaskViewOnList(Document document, String tabName, Task task) {
        if(tabName.equals("watches")){
            LoggerFactory.getLogger("krzysiekm").debug("modifyTaskViewOnList");
            InOfficeDocument inDocument = null;
            FieldsManager fm = null;
            if(document instanceof InOfficeDocument){
                inDocument = (InOfficeDocument)document;
                fm = inDocument.getDocumentKind().getFieldsManager(inDocument.getId());
                try {
                    if(fm.getValue("KLIENT") != null) {
                        Map<String, Object> contractorDic = (Map<String, Object>) fm.getValue("KLIENT");
                        if(contractorDic.get("KLIENT_NIP") != null) {
                            task.setDockindBusinessAtr3(contractorDic.get("KLIENT_NIP").toString());
                            LoggerFactory.getLogger("krzysiekm").debug("NIP: " + task.getDockindBusinessAtr3());
                        }
                    }
                } catch (Exception e) {
                    LOG.debug("ILPOL: Niepowiod�a si� modyfikacja informacji o dokumencie o id " + inDocument.getId() + " wy�wietlanych dla li�cie zada� u�ytkownika");
                }
            }
        }
    }

    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
        String assigneeIdentity = "";
        //jesli ustawiono wczesniej (patrz metoda onStartProcess(...)) podmiot dekretacji
        if(this.assignee != null){
            try {
                String id = null;
                try {
                    if(assignee instanceof DSUser){
                        id = ((DSUser) assignee).getName();
                        assigneeIdentity = ((DSUser) assignee).getWholeName();
                        assignable.addCandidateUser(id);
                        addToHistory(openExecution, doc, id, null);
                    }else if(assignee instanceof DSDivision){
                        id = ((DSDivision) assignee).getGuid();
                        assigneeIdentity = ((DSDivision) assignee).getName();
                        assignable.addCandidateGroup(id);
                        addToHistory(openExecution, doc, null, id);
                    }else if(assignee instanceof AcceptanceDivisionImpl){
                        return false;
                    }else if(assignee == null){
                        return false;
                    }
                    LOG.info("Dekretacja Zg�oszenia ICS (" + doc.getId() +") na " + assigneeIdentity + " (" + id + ")");
                } catch (UserNotFoundException e) {
                    LOG.error(e.getMessage());
                }
                return true;
            } catch (EdmException e1) {
                LOG.error(e1.getMessage());
            }
        }else if (acceptationCn != null) {
            //przyjmujemy ze acceptationCn zawiera guid dzialu
            try {
                DSDivision assignee = null;
                assignee = DSDivision.find(acceptationCn);
                assigneeIdentity = assignee.getName();
                String id = null;
                try {
                    id = assignee.getGuid();
                    assignable.addCandidateGroup(id);
                    addToHistory(openExecution, doc, null, id);
                    LOG.info("Dekretacja Zg�oszenia ICS (" + doc.getId() +") na " + assigneeIdentity + " (" + id + ")");
                } catch (UserNotFoundException e) {
                    LOG.error(e.getMessage());
                }
                return true;
            } catch(DivisionNotFoundException e){
                LOG.error("Dekretacja Zg�oszenia ICS (" + doc.getId() + ") nie zosta�a uko�czona: brak dzia�u o guid " + acceptationCn);
            } catch(EdmException e) {
                LOG.error(e.getMessage());
            }
        }
        return false;
    }

    public static void addToHistory(OpenExecution openExecution, OfficeDocument doc, String user, String guid) throws EdmException, UserNotFoundException {
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setProcessName(openExecution.getProcessDefinitionId());
        if (user == null) {
            ahe.setSourceUser(DSApi.context().getPrincipalName());
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_DIVISION);
            ahe.setTargetGuid(DSDivision.find(guid).getName());
        } else {
            ahe.setSourceUser(user);
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_SINGLE_USER);
            ahe.setTargetUser(user);
            DSDivision[] divs = DSUser.findByUsername(user).getOriginalDivisionsWithoutGroup();
            if (divs != null && divs.length > 0){
                ahe.setSourceGuid(divs[0].getName());
            }
        }
        ahe.setCtime(new Date());
        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
        try {
            String status = doc.getFieldsManager().getEnumItem("STATUS").getTitle();
            ahe.setStatus(status);
        } catch (Exception e) {
            LOG.warn(e.getMessage());
        }
        doc.addAssignmentHistoryEntry(ahe);
    }

    public Object getAssignee() {
        return assignee;
    }

    public void setAssignee(Object assignee) {
        this.assignee = assignee;
    }
}