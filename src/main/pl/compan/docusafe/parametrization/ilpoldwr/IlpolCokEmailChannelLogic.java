package pl.compan.docusafe.parametrization.ilpoldwr;

import com.google.common.base.Preconditions;
import org.directwebremoting.WebContextFactory;
import org.directwebremoting.dwrp.CallBatch;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.EmailChannelLogic;
import pl.compan.docusafe.core.mail.DSEmailChannel;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.wssk.LogicExtension_Reopen;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import javax.servlet.http.HttpServletRequest;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.regex.*;
import static pl.compan.docusafe.core.dockinds.dwr.Field.DWR_PREFIX;

/**
 * Created with IntelliJ IDEA.
 * User: Krzysztof Modzelewski
 * Date: 13.11.13
 * Time: 12:36
 */
public class IlpolCokEmailChannelLogic extends AbstractDocumentLogic implements EmailChannelLogic, LogicExtension_Reopen {


    private final static Logger LOG = LoggerFactory.getLogger(IlpolCokEmailChannelLogic.class);
    private final static StringManager SM = StringManager.getManager(IlpolCokEmailChannelLogic.class.getPackage().getName());

    private final DocumentKind dockind;

    /*
        TABLE SCHEME

        CREATE TABLE ILPL_COK_DOC_EMAIL_CHANNEL (
            ID NUMBER(19,0),
            EMAIL_CHANNEL_ID NUMBER(19,0),
            DIVISION_ID NUMBER(19,0),
            AREA_ID NUMBER(19,0),
            CATEGORY_ID	NUMBER(19,0),
            SUBCATEGORY_ID	NUMBER(19,0)
        );
    */
    public static final String DOCKIND_TABLE_NAME = "ILPL_COK_DOC_EMAIL_CHANNEL";

    public static final String DOCKIND_NAME = "cok_email_channel";

    /*
        Specific for this dockind table columns name
    */
    public static final String DIVISION_ID = "DIVISION_ID";
    public static final String AREA_ID = "AREA_ID";
    public static final String CATEGORY_ID = "CATEGORY_ID";
    public static final String SUBCATEGORY_ID = "SUBCATEGORY_ID";

    /*
        Dockind fields cn
    */
    public static final String DIVISION_DIC_CN = "DIVISION";
    public static final String AREA_CN = "AREA";
    public static final String CATEGORY_CN = "CATEGORY";
    public static final String SUBCATEGORY_CN = "SUBCATEGORY";


    public IlpolCokEmailChannelLogic() throws EdmException {
        dockind = DocumentKind.findByCn(DOCKIND_NAME);
    }

    /**
     * Zwraca warto�� danego parametru wyszukiwania zawartego w ��daniu klienta (dla url docusafe/admin/edit-email-channel.action?id=8 i parametru id zwr�cona warto�� to "8")
     * @param req ��danie klienta
     * @param parameterName nazwa parametru, dla kt�rego ma zosta� zwr�cona jego warto�� zadeklarowana w ��daniu
     * @return warto�� parametru z url ��dania klienta b�d� null je�li nie zostanie odnaleziony wskazany parametr
     */
    private String retriveParameterValueFromActionURL(HttpServletRequest req, String parameterName){
        if(req != null && parameterName != null){
            String actionURL = ((CallBatch)req.getAttribute("org.directwebremoting.dwrp.batch")).getPage();
            Pattern pattern = Pattern.compile("(" + parameterName + "=)\\w+&?");
            Matcher matcher = pattern.matcher(actionURL);
            if(matcher.find()){
                return matcher.group().replaceFirst("(" + parameterName + "=)", "").replaceFirst("&", "");
            }else{
                return null;
            }
        }
        return null;
    }

    @Override
    public void onCreate(DSEmailChannel emailChannel, Map<String, String> dwrValues) throws EdmException, SQLException {
//        dwrValues.put(DWR_PREFIX + dockindField.getCn(), dwrValue);
        Preconditions.checkNotNull(emailChannel);
        Preconditions.checkNotNull(dwrValues);
        validateCategories(dwrValues);
        List<String> columnNamestList = new ArrayList<String>();
        List<String> columnValuesList = new ArrayList<String>();
        prepareSQLQueryData(columnNamestList, columnValuesList, dwrValues);
        String sqlQuery = createInsertQuery(columnNamestList, columnValuesList, emailChannel);
        PreparedStatement ps = DSApi.context().prepareStatement(sqlQuery);
        ps.execute();
        DSApi.context().closeStatement(ps);
    }

    @Override
    public void onUpdate(DSEmailChannel emailChannel, Map<String, String> dwrValues) throws EdmException, SQLException {
        Preconditions.checkNotNull(emailChannel);
        Preconditions.checkNotNull(dwrValues);
        validateCategories(dwrValues);
        List<String> columnNamestList = new ArrayList<String>();
        List<String> columnValuesList = new ArrayList<String>();
        prepareSQLQueryData(columnNamestList, columnValuesList, dwrValues);
        String sqlQuery = createUpdateQuery(columnNamestList, columnValuesList, emailChannel);
        PreparedStatement ps = DSApi.context().prepareStatement(sqlQuery);
        ps.execute();
        DSApi.context().closeStatement(ps);
    }

    public String createQueryFromDwr(Map<String, String> dwrValues, DSEmailChannel emailChannel) throws EdmException {
        List<String> columnNamestList = new ArrayList<String>();
        List<String> columnValuesList = new ArrayList<String>();

        prepareSQLQueryData(columnNamestList, columnValuesList, dwrValues);

        //jesli kanal email zostal powiazany z dockindem to aktualizujemy wartosci dockinda powiazanego z tym kanalem email
        if(emailChannel.getDocumentKindId() != null && emailChannel.getDocumentKindId() != 0){
            return createUpdateQuery(columnNamestList, columnValuesList, emailChannel);
        }else{
            return createInsertQuery(columnNamestList, columnValuesList, emailChannel);
        }
    }

    private void prepareSQLQueryData(List<String> columnNamestList, List<String> columnValuesList, Map<String, String> dwrValues) throws EdmException {
        String dwrFieldValue = null;
        for(Field dockindField : dockind.getFields()){
            if((dwrFieldValue = dwrValues.get(DWR_PREFIX + dockindField.getCn())) != null && dockindField.isPersistedInColumn()){
                columnNamestList.add(dockindField.getColumn());
                columnValuesList.add((wrapSqlValue(dockindField.getType(), dwrFieldValue)));
            }else if(dockindField.isPersistedInColumn()){
                columnNamestList.add(dockindField.getColumn());
                columnValuesList.add(null);
            }
        }
    }

    private String createUpdateQuery(List<String> columnNamestList, List<String> columnValuesList, DSEmailChannel emailChannel) throws EdmException {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append("UPDATE " + DOCKIND_TABLE_NAME + " SET ");
        for(int i = 0; i < columnNamestList.size(); i++){
            sqlQuery.append(columnNamestList.get(i) + " = " + columnValuesList.get(i) + ", ");
        }
        sqlQuery.replace(sqlQuery.length() - 2, sqlQuery.length(), " WHERE " + EMAIL_CHANNEL_ID_COLUMN_NAME + " = " + emailChannel.getId());
        return sqlQuery.toString();
    }

    private String createInsertQuery(List<String> columnNamestList, List<String> columnValuesList, DSEmailChannel emailChannel) throws EdmException {
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append("INSERT INTO " + DOCKIND_TABLE_NAME + " (");
        sqlQuery.append(EMAIL_CHANNEL_ID_COLUMN_NAME + ", " + columnNamestList.toString().substring(1, columnNamestList.toString().length()-1) + ") ");
        sqlQuery.append(" VALUES (" + emailChannel.getId() + ", " + columnValuesList.toString().substring(1, columnValuesList.toString().length()-1) + ")");
        return sqlQuery.toString();
    }

    //Opakowuje dane wartosci krotek w specyficzne dla db Oracle'a apostrofy dla wartosich tekstowych
    private String wrapSqlValue(String tupleDataType, String tupleValue) throws EdmException {
        if(tupleDataType.equals(Field.STRING)){
            return "'" + tupleValue + "'";
        }else if(tupleDataType.equals(Field.DATA_BASE) || tupleDataType.equals(Field.ENUM) || tupleDataType.equals(Field.INTEGER) || tupleDataType.equals(Field.DICTIONARY)){
            return tupleValue;
        }else{
            throw new EdmException("B�ad opakowywania warto�ci krotki dla Oracle'a: nieobs�ugiwany typ pola dockinda " + tupleDataType);
        }
    }

    /**
     * Sprawdzamy czy przy ustawienia warto�ci dla pola obszar, pola kategoria oraz podkategoria s� r�wnie� wype�nione
     * @param dwrValues
     * @throws EdmException je�li pole obszar jest wype�nione a pola kategoria i podkategoria przeciwnie
     */
    private void validateCategories(Map<String, String> dwrValues) throws EdmException {
        if(dwrValues.containsKey(DWR_PREFIX + AREA_CN)){
            if(!dwrValues.containsKey(DWR_PREFIX + CATEGORY_CN)){
                throw new EdmException("Nie ustawiono warto�ci dla pola kategoria - nie zapisano dokonanych zmian");
            }
            if(!dwrValues.containsKey(DWR_PREFIX + SUBCATEGORY_CN)){
                throw new EdmException("Nie ustawiono warto�ci dla pola podkategoria - nie zapisano dokonanych zmian");
            }
        }
    }

    @Override
    public void onDelete(DSEmailChannel emailChannel) throws EdmException, SQLException {
        Preconditions.checkNotNull(emailChannel.getDocumentKindId());
        DSApi.context().prepareStatement("DELETE FROM " + DOCKIND_TABLE_NAME + " WHERE " + EmailChannelLogic.EMAIL_CHANNEL_ID_COLUMN_NAME + " = " + emailChannel.getId()).execute();
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {
    }

    @Override
    public boolean canReopenNow() {
        return false;
    }

    @Override
    public void reopenNow(OfficeDocument doc) throws EdmException {
    }

    @Override
    public void setReopenMarkerOn(OfficeDocument doc) throws EdmException {
    }

    /**
     * Odpowiada za wczytywanie i zapisywanie warto�ci p�l dockinda (DWR) powi�zanych z kana�em email
     * @param req
     * @param dwrSessionValues
     * @param fieldsManager
     * @param dwrfacade
     * @throws EdmException
     */
    public void modifyDwrFacade(HttpServletRequest req, Map<String, Object> dwrSessionValues, FieldsManager fieldsManager, DwrFacade dwrfacade) throws EdmException {
        //jesli modyfikacja dwra nie nastepuje wylacznie w wyniku wprowadzenia/wyszukania wartosci pol dockinda
        if(!isDwrFieldModificationOnly(dwrSessionValues)){
            //url powi�zany z akcj� formularza z formatki kana�u email
            String emailChannelId = retriveParameterValueFromActionURL(req, "id");
            Map<String, Object> emailChannelDwrFieldValue = ((Map<String, Object>)dwrSessionValues.get("FIELDS_VALUES"));
            //pobieramy warto�ci p�l dockinda powi�zanego z wybranym kana�em email
            retriveDwrValues(emailChannelDwrFieldValue, emailChannelId);
        }
    }

    /**
     * Zwraca informacje czy operacje na polach dockinda (DWR) s� powi�zane wy��cznie z "ajaxow� modyfikacj�" warto�ci
     * @param dwrSessionValues
     * @return
     */
    private boolean isDwrFieldModificationOnly(Map<String, Object> dwrSessionValues){
        //klucz znajduje sie w sesji w momencie gdy sa wprowadzane/wyszukiwane wartosci dla pol dockinda
        return dwrSessionValues.containsKey("SELECTED_FIELDS_VALUES");
    }



    /**
     * Uzupe�nia warto�ci p�l dockinda (DWR) dla danego kana�u email
     * @param emailChannelDwrFieldValue
     * @param emailChannelId
     * @throws EdmException
     */
    private void retriveDwrValues(Map<String, Object> emailChannelDwrFieldValue, String emailChannelId) throws EdmException {
        Map<String, String> fieldCnColumn = new HashMap<String, String>(emailChannelDwrFieldValue.keySet().size());
        for(Field field : dockind.getFields()){
            if(emailChannelDwrFieldValue.keySet().contains(field.getCn()) && field.isPersistedInColumn()){
                fieldCnColumn.put(field.getCn(), field.getColumn());
            }
        }
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            StringBuilder sqlQuery = new StringBuilder("SELECT " + fieldCnColumn.values().toString().substring(1, fieldCnColumn.values().toString().length()-1) + " FROM " + DOCKIND_TABLE_NAME + " WHERE EMAIL_CHANNEL_ID = " + emailChannelId);
            ps = DSApi.context().prepareStatement(sqlQuery);
            rs = ps.executeQuery();
            if(rs.next()){
                for(String fieldCn : fieldCnColumn.keySet()){
                    emailChannelDwrFieldValue.put(fieldCn, rs.getObject(fieldCnColumn.get(fieldCn)));
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }finally{
            DSApi.context().closeStatement(ps);
        }
    }
}
