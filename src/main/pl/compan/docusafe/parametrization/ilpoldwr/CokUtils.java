package pl.compan.docusafe.parametrization.ilpoldwr;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContextFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.*;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.field.*;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.mail.DSEmailChannel;
import pl.compan.docusafe.core.mail.MailMessage;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.sql.AcceptanceDivisionImpl;
import pl.compan.docusafe.service.fax.IncomingFax;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class CokUtils {
    private final static Logger LOG = LoggerFactory.getLogger(CokUtils.class);
    private final static StringManager SM = GlobalPreferences.loadPropertiesFile(CokUtils.class.getPackage().getName(), null);

    public static final String COK_DOCUMENT = "COK_DOCUMENT";
    public static final String COK_CASE = "COK_CASE";
    public static final String COK_TASK = "COK_TASK";

    /**
     * Kopiuje ustawienia: Obszaru(DWR_AREA), Kategorii(DWR_CATEGORY) i Podkategorii(DWR_SUBCATEGORY) do p?l z prefiksem:<br />
     * DWR_AREA -> prefixAREA
     *
     * @param values Wartości kopiowane
     * @param newValPrefix Prefiks nazwy nowych wartości
     */
    public static void copyAreaValues(Map<String, FieldData> values, String newValPrefix) {
        String selected = values.get("DWR_AREA").getEnumValuesData().getSelectedId();
        values.get(newValPrefix + "AREA").getEnumValuesData().setSelectedId(selected);
        selected = values.get("DWR_CATEGORY").getEnumValuesData().getSelectedId();
        values.get(newValPrefix + "CATEGORY").getEnumValuesData().setSelectedId(selected);
        selected = values.get("DWR_SUBCATEGORY").getEnumValuesData().getSelectedId();
        values.get(newValPrefix + "SUBCATEGORY").getEnumValuesData().setSelectedId(selected);
    }

    public static boolean copyContractorValue(Map<String, FieldData> values, String srcContractorCn, String destContractorCn) {
        List<Long> contractorIds = DwrUtils.extractIdFromDictonaryField(values, srcContractorCn);
        if(!contractorIds.isEmpty()) {
            FieldData fieldData = new FieldData(Field.Type.STRING, contractorIds.get(0).toString());
            values.get(destContractorCn).getDictionaryData().put("id", fieldData);
            return true;
        }

        return false;
    }

    public static void setContractorForCokDocument(Object sourceData, Map<String,Object> values, String contractorDictionaryFieldCn){
        Contractor contractor;
        try{
            if(sourceData instanceof IncomingFax){
                IncomingFax fax = (IncomingFax)sourceData;
                contractor = Contractor.findByFaxNumber(fax.getDID()).get(0);
                values.put(contractorDictionaryFieldCn, contractor.getId());
            }else if(sourceData instanceof Map){
                Map<Object, Object> documentImportedData = (Map<Object, Object>)sourceData;
                String contractorNip = String.valueOf(documentImportedData.get(IlpolCokImportLogic.NIP));
                //  nip zczytywany z pliku jest konwertowany do postaci double'a stad musimy pozbyc sie czesci ulamkowej
                if(StringUtils.isNotEmpty(contractorNip)){
                    contractor = Contractor.findByNIP(contractorNip).get(0);
                    values.put(contractorDictionaryFieldCn, contractor.getId());
                }
            }
        }catch(IndexOutOfBoundsException e){
            LOG.debug("Nie znaleziono kontrahenta dla dokumentu COK tworzonego ze źródła typu " + sourceData.getClass());
        }
    }

    /**
     * Tworzy nowy dokumenty COKowskie o określonym typie. Metoda jest aktualnie uzależniona od statycznie
     * zdefiniowanych pól dockindów (pomyśleć nad zmianą).
     * @param fieldValues
     * @param cokDocumentType
     * @return
     * @throws pl.compan.docusafe.core.EdmException
     */
    public static Document createDocumentFromPopup(FieldsManager fieldsManager, Map<String, FieldData> fieldValues, String cokDocumentType) throws EdmException {
        HttpServletRequest req = WebContextFactory.get().getHttpServletRequest();
        DSApi.open(AuthUtil.getSubject(req));
        DSApi.context().begin();

        Map<String, Object> newDocumentValues = Maps.newHashMap();
        cokDocumentType = cokDocumentType.toUpperCase();
        InOfficeDocument newCokTypeDocument = new InOfficeDocument();
        String assigneeUserName = retriveUserLogin();
        Object assignee = null;
        FieldData fieldData = null;
        if(cokDocumentType.contains(COK_DOCUMENT)){

        }else if(cokDocumentType.contains(COK_CASE)){
            newDocumentValues.put("CREATION_DATETIME", fieldValues.get("DWR_NEW_CASE_CREATION_DATETIME").getDateData());
            newDocumentValues.put("FINISH_DATETIME", fieldValues.get("DWR_NEW_CASE_FINISH_DATETIME").getDateData());
            newDocumentValues.put("STATUS", fieldValues.get("DWR_NEW_CASE_STATUS").getEnumValuesData().getSelectedId());
            newDocumentValues.put("CONTRACTOR", StringUtils.stripToNull(fieldValues.get("DWR_NEW_CASE_CONTRACTOR").getDictionaryData().get("id").toString()));
            newDocumentValues.put("AREA",  fieldValues.get("DWR_NEW_CASE_AREA").getEnumValuesData().getSelectedId());
            newDocumentValues.put("CATEGORY",  fieldValues.get("DWR_NEW_CASE_CATEGORY").getEnumValuesData().getSelectedId());
            newDocumentValues.put("SUBCATEGORY",  fieldValues.get("DWR_NEW_CASE_SUBCATEGORY").getEnumValuesData().getSelectedId());
            newDocumentValues.put("INTERNAL_ID", fieldValues.get("DWR_NEW_CASE_INTERNAL_ID").getStringData());
            newDocumentValues.put("COMPLAINT", fieldValues.get("DWR_NEW_CASE_COMPLAINT").getBooleanData());
            newDocumentValues.put("DEALER", fieldValues.get("DWR_NEW_CASE_DEALER").getBooleanData());
            newDocumentValues.put("CASE_SOLUTION_DESCRIPTION", fieldValues.get("DWR_NEW_CASE_SOLUTION_DESCRIPTION").getStringData());
            if((fieldData = fieldValues.get("DWR_NEW_CASE_CONTRACT_NUMBER_ID")) != null && fieldData.getEnumValuesData().getSelectedId() != null && !fieldData.getEnumValuesData().getSelectedId().isEmpty()){
                newDocumentValues.put("CONTRACT_NUMBER_ID", fieldData.getEnumValuesData().getSelectedId());
            }
            assignee = CokUtils.retriveLoggedUser();
            CokUtils.createDocumentFromUserAction(newCokTypeDocument, DocumentKind.findByCn(IlpolCokCaseLogic.DOCKIND_NAME), (DSUser) assignee, assignee, newDocumentValues, "Formularz popup", "Sprawa COK", "Formularz popup");
            newCokTypeDocument.setRecipientUser(((DSUser) assignee).getWholeName());
        }else if(cokDocumentType.contains(COK_TASK)){
            assignee = IlpolCokLogic.retriveAssignee(fieldValues.get("DWR_RECIPIENT_HERE").getDictionaryData());
            String recipientName = null;
            //  jesli nie wybrano za slownika zadnego odbiorcy
            if(assignee == null){
                //mozliwe ze bedzie trzeba dodac sprawdzenie czy osoba dekretujaca jest z COKu, jesli tak, a nie wybrala odbiorcy to zadanie chyba powinno zostac zadekretowane na COK
                assignee = CokUtils.retriveLoggedUser();
                recipientName = ((DSUser)assignee).getWholeName();
            }else if(assignee instanceof DSUser){
                recipientName = ((DSUser)assignee).getWholeName();
                DSApi.context().watch(URN.create(newCokTypeDocument), recipientName);
            }else if(assignee instanceof DSDivision){
                recipientName = ((DSDivision)assignee).getName();
            }else if(assignee instanceof AcceptanceDivisionImpl){
                recipientName = ((AcceptanceDivisionImpl)assignee).getName();
            }
            newDocumentValues.put("DESCRIPTION", fieldValues.get("DWR_NEW_TASK_DESCRIPTION").getStringData());
            newDocumentValues.put("CREATION_DATETIME", fieldValues.get("DWR_NEW_TASK_CREATION_DATETIME").getDateData());
            newDocumentValues.put("DEADLINE_DATETIME", fieldValues.get("DWR_NEW_TASK_DEADLINE_DATETIME").getDateData());
            newDocumentValues.put("SENDER", CokUtils.retriveLoggedUser().getWholeName()); //do zmiany, informacje mozna pobrac z ds_document (creating user) i ustawic np. w ~setInitialValues()
            newDocumentValues.put("AREA",  fieldValues.get("DWR_NEW_TASK_AREA").getEnumValuesData().getSelectedId());
            newDocumentValues.put("CATEGORY",  fieldValues.get("DWR_NEW_TASK_CATEGORY").getEnumValuesData().getSelectedId());
            newDocumentValues.put("SUBCATEGORY",  fieldValues.get("DWR_NEW_TASK_SUBCATEGORY").getEnumValuesData().getSelectedId());
            newDocumentValues.put("CONTRACTOR", fieldValues.get("DWR_NEW_TASK_CONTRACTOR").getDictionaryData().get("id").toString());
            newDocumentValues.put("RECIPIENT", recipientName);
            newDocumentValues.put("COK_ID", fieldsManager.getDocumentId());
            CokUtils.createDocumentFromUserAction(newCokTypeDocument, DocumentKind.findByCn(IlpolCokTaskLogic.DOCKIND_NAME), (DSUser) assignee, assignee, newDocumentValues, "Formularz popup", "Zadanie ICS", "Formularz popup");
            newCokTypeDocument.setRecipientUser(recipientName);
        }else{
            throw new EdmException("System nie rozpoznaje dokumentu o typie " + cokDocumentType);
        }
        DSApi.context().commit();
        DSApi._close();
        return newCokTypeDocument;
    }

    public static CaseNumber generateCaseNumber(int areaId) throws SQLException, EdmException {
        Long generatedId = null;
        PreparedStatement ps = DSApi.context().prepareStatement("select DSC_ILPOL_COK_CASE_INTERNAL_ID.nextval from dual");
        ResultSet rs = ps.executeQuery();
        while(rs.next()) {
            generatedId = rs.getLong(1);
        }
        rs.close();
        DSApi.context().closeStatement(ps);

        return new CaseNumber(generatedId, GregorianCalendar.getInstance().getTime(), areaId);
    }

    public static List<Long> getCokCaseListId(Long documentId) throws EdmException {
        boolean contextOpened = DSApi.openContextIfNeeded();
        List<Long> cokCaseIdList = new ArrayList<Long>();
        Statement statement = null;
        ResultSet result = null;
        StringBuilder sqlQuery = new StringBuilder("SELECT ID FROM " + IlpolCokCaseLogic.DOCKIND_TABLE_NAME + " WHERE (DOCUMENT_ID = " + documentId + ")");
        try {
            statement = DSApi.context().createStatement();
            result = statement.executeQuery(sqlQuery.toString());
            while (result.next()) {
                Long id = result.getLong(1);
                cokCaseIdList.add(id);
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        } finally {
            DSApi.context().closeStatement(statement);
        }
        DSApi.closeContextIfNeeded(contextOpened);
        return cokCaseIdList;
    }

    public static void initCurrentDateForDWRField(Map<String, FieldData> values, String fieldCn){
        FieldData fieldDate = values.get(fieldCn);
        Date currentDateTime = DateUtils.getCurrentTime();
        fieldDate.setDateData(currentDateTime);
    }

    public static void initStatusForDWRField(Map<String, FieldData> values, String fieldCn){
        FieldData documentStatus = values.get(fieldCn);
        documentStatus.getEnumValuesData().setSelectedId("1");
    }

    private static void validateDocumentData(Document document, DocumentKind documentKind, DSUser creatingUser, Map<String,Object> values){
        Preconditions.checkNotNull(document);
        Preconditions.checkNotNull(documentKind);
        Preconditions.checkNotNull(creatingUser);
        Preconditions.checkNotNull(values);
    }

    public static Document createBaseDocument(InOfficeDocument cokDocument, DocumentKind documentKind, String creatingUserName, Map<String, Object> values, String source, String summary, String remark) throws EdmException {
        cokDocument.setDocumentKind(documentKind);
        cokDocument.setAssignedDivision(DSDivision.ROOT_GUID);
        cokDocument.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
        cokDocument.setDivisionGuid(DSDivision.ROOT_GUID);
        cokDocument.setCurrentAssignmentAccepted(Boolean.FALSE);
        cokDocument.setCreatingUser(creatingUserName);
        cokDocument.setSummary(summary);
        cokDocument.setSource(source);
        cokDocument.setForceArchivePermissions(false);
        cokDocument.setPermissionsOn(false);
        cokDocument.setClerk(DSApi.context().getPrincipalName());
        cokDocument.setSender(new Sender());
        cokDocument.setFolder(Folder.getRootFolder());

        List<InOfficeDocumentKind> kinds = InOfficeDocumentKind.list();
        String kindName = documentKind.logic().getInOfficeDocumentKind();
        boolean canChooseKind = (kindName == null);
        if (!canChooseKind)
        {
            for (InOfficeDocumentKind inKind : kinds)
            {
                if (kindName.toUpperCase().equals(inKind.getName().toUpperCase()))
                    cokDocument.setKind(InOfficeDocumentKind.find(inKind.getId()));
            }
        }
        if(cokDocument.getKind() == null)
        {
            cokDocument.setKind(InOfficeDocumentKind.find(1));
        }

        Calendar currentDay = Calendar.getInstance();
        currentDay.setTime(GlobalPreferences.getCurrentDay());
        cokDocument.setIncomingDate(currentDay.getTime());
        cokDocument.setDocumentDate(new Date());
        cokDocument.setCurrentAssignmentAccepted(Boolean.FALSE);
        cokDocument.setOriginal(true);
        cokDocument.setStatus(InOfficeDocumentStatus.findByCn("PRZYJETY"));
        cokDocument.create();

        if(StringUtils.isNotEmpty(remark))
        {
            try
            {
                if (remark.length()<=4000) {
                    Remark rem = new Remark(remark,creatingUserName);
                    cokDocument.addRemark(rem);
                } else {
                    for(int i=0; i<remark.length(); i=i+4000)
                    {
                        String spLine = StringUtils.substring(remark,i ,i+4000);
                        Remark rem = new Remark(spLine,creatingUserName);
                        cokDocument.addRemark(rem);
                    }
                }
            }
            catch (Exception e)
            {
                LOG.error(e.getMessage(),e);
                LOG.error("REMARK : "+ remark);
                throw new EdmException(e);
            }
        }

        Journal journal = Journal.getMainIncoming();
        Long journalId = journal.getId();
        Integer sequenceId = Journal.TX_newEntry2(journalId, cokDocument.getId(), new Date(currentDay.getTime().getTime()));
        cokDocument.bindToJournal(journalId, sequenceId);
        Long newDocumentId = cokDocument.getId();
        cokDocument.setDocumentKind(documentKind);
        values.put("DOCUMENT_CREATION_DATETIME", currentDay.getTime());
        documentKind.set(newDocumentId, values);
        documentKind.logic().documentPermissions(cokDocument);
        DSApi.context().session().flush();
        return cokDocument;
    }

    public static Document createDocumentFromEmail(InOfficeDocument importedEmail, DocumentKind documentKind, DSUser creatingUser, Object assignee, Map<String, Object> values, String source, String summary, String remark, String messageId, DSEmailChannel channel) throws EdmException, IOException, SQLException {
        if(values == null){
            values = new HashMap<String, Object>();
        }
        validateDocumentData(importedEmail, documentKind, creatingUser, values);

        Object dwrAssignee = retriveDwrValuesFromEmailChannelDockind(channel, values);
        values.put("DELIVERY_TYPE", IlpolCokLogic.DELIVERY_TYPE_EMAIL);

        if(assignee == null){
            assignee = dwrAssignee;
        }
        createBaseDocument(importedEmail, documentKind, creatingUser.getName(), values, source, summary, remark);

        MailMessage mailMessage = MailMessage.findByMessageId(String.valueOf(messageId), channel);
        importedEmail.setMessageId(mailMessage.getId());

        Long newDocumentId = importedEmail.getId();
        mailMessage.createAttachments(importedEmail);
        DSApi.context().session().save(importedEmail);
        DocumentLogic logic =  documentKind.logic();
        if(logic != null){
            ((IlpolCokLogic)documentKind.logic()).onStartProcess(importedEmail, null, assignee);
        }else{
            throw new EdmException("Nie uruchomiono procesu z powodu braku logiki dla dokumentu typu " + documentKind.getCn());
        }
        TaskSnapshot.updateByDocument(importedEmail);
        String senderEmailAddress = mailMessage.getFrom();

        try {
            InternetAddress addr = new InternetAddress(senderEmailAddress);
            senderEmailAddress = addr.getAddress();
        } catch(AddressException e) {
            LoggerFactory.getLogger("kamil").debug("Dziwny adres: " + senderEmailAddress, e);
            throw new EdmException(e.getLocalizedMessage(), e);
        }
        List<Contractor> ctrList = Contractor.findByEmail(senderEmailAddress);
        Long contractorId = null;
        PreparedStatement ps;
        StringBuilder query = new StringBuilder("UPDATE " + IlpolCokLogic.DOCKIND_TABLE_NAME + " SET MESSAGE_ID = ?, TASK_STATUS = ?");
        if(ctrList.size() > 0){
            Contractor contractor = Contractor.findByEmail(senderEmailAddress).get(0);
            contractorId = contractor.getId();
            query.append(", CONTRACTOR_ID = ? WHERE DOCUMENT_ID = ?");
            ps = DSApi.context().prepareStatement(query);
            ps.setLong(3, contractorId);
            ps.setLong(4, newDocumentId);
        }else{
            query.append(" WHERE DOCUMENT_ID = ?");
            ps = DSApi.context().prepareStatement(query);
            ps.setLong(3, newDocumentId);
        }
        ps.setLong(1, mailMessage.getId());
        ps.setLong(2, 1);
        ps.execute();
        DSApi.context().closeStatement(ps);
        return importedEmail;
    }

    /**
     * Uzupełnia wartości Zgłoszenia ICS wartościami pól dockinda (DWR) powiązanego z danym kanałem email oraz zwraca podmiot dekretacji
     * @param emailChannel
     * @param values
     * @return
     * @throws EdmException
     * @throws SQLException
     */
    private static Object retriveDwrValuesFromEmailChannelDockind(DSEmailChannel emailChannel, Map<String, Object> values) throws EdmException, SQLException {
        Object assignee = null;
        DocumentKind documentKind = null;
        //jesli kanal email jest powiazany z dockindem (DWR) to listujemy pola tego dockinda, pobieramy odpowiadajace im kolumny bazy danych
        //pobieramy wartosci z bazy danych dla tych pól dockinda i dodajemy je do mapy valuesow, ktore zostana przypisane do Zgloszenia ICS powstalego na podstawie emaila
        //kolumna division_id zawiera informacje o dziale, na ktore ma zostac zadekretowany tworzone Zgloszenie ICS
        if(emailChannel.getDocumentKindId() != null && (documentKind = DocumentKind.find(emailChannel.getDocumentKindId())) != null){
            Map<String, String> fieldCnColumn = new HashMap<String, String>();
            for(pl.compan.docusafe.core.dockinds.field.Field field : documentKind.getFields()){
                if(!field.getColumn().equals("*none*")){
                    fieldCnColumn.put(field.getCn(), field.getColumn());
                }
            }
            String selectedColumns = fieldCnColumn.values().toString().substring(1, fieldCnColumn.values().toString().length()-1);
            String sqlQuery = "SELECT " + selectedColumns + " FROM " + documentKind.getTablename() + " WHERE EMAIL_CHANNEL_ID = " + emailChannel.getId();
            PreparedStatement ps = DSApi.context().prepareStatement(sqlQuery);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                for(String fieldCn : fieldCnColumn.keySet()){
                    if(fieldCnColumn.get(fieldCn).equals("DIVISION_ID")){
                        DSDivision division = DSDivision.findById(rs.getInt("DIVISION_ID"));
                        if(division != null){
                            assignee = division;
                        }
                    }else{
                        try{
                            values.put(fieldCn, rs.getObject(fieldCnColumn.get(fieldCn)));
                        }catch(SQLException e){
                            LOG.debug(e.getMessage(), e);
                        }
                    }
                }
            }
        }
        return assignee;
    }

    public static Document createDocumentFromUserAction(InOfficeDocument manualDocument, DocumentKind documentKind, DSUser creatingUser, Object assignee, Map<String, Object> values, String source, String summary, String remark) throws EdmException{
        validateDocumentData(manualDocument, documentKind, creatingUser, values);
        createBaseDocument(manualDocument, documentKind, creatingUser.getName(), values, source, summary, remark);
        DSApi.context().session().save(manualDocument);

        DocumentLogic logic =  documentKind.logic();
        if(logic != null){
            if(logic instanceof IlpolCokTaskLogic){
                ((IlpolCokTaskLogic)documentKind.logic()).onStartProcess(manualDocument, null, assignee);
            }else if(logic instanceof IlpolCokCaseLogic){
                ((IlpolCokCaseLogic)documentKind.logic()).onStartProcess(manualDocument, null, assignee);
            }
        }else{
            throw new EdmException("Nie uruchomiono procesu z powodu braku logiki dla dokumentu typu " + documentKind.getCn());
        }

        TaskSnapshot.updateByDocument(manualDocument);
        return manualDocument;
    }

    public static Document createDocumentFromImport(InOfficeDocument importedDocument, DocumentKind documentKind, DSUser creatingUser, Object assignee, Map<String, Object> values, String source, String summary, String remark, Map<String, Object> documentImportedData) throws SQLException, EdmException {
        if(values == null){
            values = new HashMap<String, Object>();
            values.put("DELIVERY_TYPE", IlpolCokLogic.DELIVERY_TYPE_IMPORT);
            values.put("STATUS", 1);
            values.put("DEADLINE_DATETIME", documentImportedData.get(IlpolCokImportLogic.TERMIN_WYKONANIA));
            values.put("SUBJECT", documentImportedData.get(IlpolCokImportLogic.TEMAT));
            values.put("DESCRIPTION", documentImportedData.get(IlpolCokImportLogic.TRESC));
            values.put("ORIGINAL_ARREAR", documentImportedData.get(IlpolCokImportLogic.ZALEGLOSC_PIERWOTNA));
        }

        validateDocumentData(importedDocument, documentKind, creatingUser, values);

        PreparedStatement ps = null;
        StringBuilder query = null;
        query = new StringBuilder(
            "SELECT AREA.CN, CAT.CN, SUBCAT.CN FROM DSC_ILPOL_COK_CASE_AREA AREA " +
                "INNER JOIN DSC_ILPOL_COK_CASE_CAT CAT ON AREA.ID = CAT.REFVALUE " +
                "INNER JOIN DSC_ILPOL_COK_CASE_SUBCAT SUBCAT ON CAT.ID = SUBCAT.REFVALUE " +
                "WHERE " +
                "AREA.TITLE like '%" + documentImportedData.get(IlpolCokImportLogic.OBSZAR) + "%' AND " +
                "CAT.TITLE like '%" + documentImportedData.get(IlpolCokImportLogic.KATEGORIA) + "%' AND " +
                "SUBCAT.TITLE like '%" + documentImportedData.get(IlpolCokImportLogic.PODKATEGORIA) + "%'"
        );

        Long areaCn = null;
        Long catCn = null;
        Long subCatCn = null;

        try {
            ps = DSApi.context().prepareStatement(query);
            ps.execute();

            ResultSet rs = ps.getResultSet();
            try {
                if(rs.next()){
                    areaCn = ps.getResultSet().getLong(1);
                    catCn = ps.getResultSet().getLong(2);
                    subCatCn = ps.getResultSet().getLong(3);
                }
            } finally {
                rs.close();
            }
        } finally {
            DSApi.context().closeStatement(ps);
        }

        if(areaCn == null){
            throw new EdmException("Nie znaleziono obszaru o nazwie " + documentImportedData.get(IlpolCokImportLogic.OBSZAR));
        }else if(catCn == null){
            throw new EdmException("Nie znaleziono kategorii o nazwie " + documentImportedData.get(IlpolCokImportLogic.KATEGORIA));
        }else if(subCatCn == null){
            throw new EdmException("Nie znaleziono podkategorii o nazwie " + documentImportedData.get(IlpolCokImportLogic.PODKATEGORIA));
        }else{
            values.put("AREA", areaCn.toString());
            values.put("CATEGORY", catCn.toString());
            values.put("SUBCATEGORY", subCatCn.toString());
        }

        CokUtils.setContractorForCokDocument(documentImportedData, values, "CONTRACTOR");
        createBaseDocument(importedDocument, documentKind, creatingUser.getName(), values, source, summary, remark);
        DSApi.context().session().save(importedDocument);

        DocumentLogic logic =  documentKind.logic();
        if(logic != null && logic instanceof IlpolCokImportedLogic){
            ((IlpolCokImportedLogic)documentKind.logic()).onStartProcess(importedDocument, null, assignee);
        }else{
            throw new EdmException("Nie uruchomiono procesu z powodu braku logiki dla dokumentu typu " + documentKind.getCn());
        }
        TaskSnapshot.updateByDocument(importedDocument);
        return importedDocument;
    }

    public static Document createDocumentFromFax(InOfficeDocument importedFax, DocumentKind documentKind, DSUser creatingUser, Object assignee, Map<String, Object> values, String source, String summary, String remark, IncomingFax fax) throws EdmException, IOException, SQLException{
        if(values == null){
            values = new HashMap<String, Object>();
            values.put("DELIVERY_TYPE", IlpolCokLogic.DELIVERY_TYPE_FAX);
            values.put("FAX_SEND_DATETIME", importedFax.getCtime());
            values.put("FAX_NUMBER", fax.getDID());
            values.put("SUBJECT_MANUAL", "Faks z numeru " + fax.getRfaxid());
            values.put("TASK_STATUS", 1);
        }
        validateDocumentData(importedFax, documentKind, creatingUser, values);
        createBaseDocument(importedFax, documentKind, creatingUser.getName(), values, source, summary, remark);

        Long newDocumentId = importedFax.getId();
        List<Long> attsId = addFaxAttachmentsToDocument(importedFax, fax.getFiles());
        DwrDictionaryBase dic = DwrDictionaryFacade.getDwrDictionaryBase(ServletActionContext.getRequest(), "ATTACHMENT");
        List<Long> attIds = new ArrayList<Long>();
        Map<String, FieldData> dicValues = Maps.newHashMap();
        long result;
        FieldsManager fm = documentKind.getFieldsManager(importedFax.getId());
        String attachmentsTable = ((AbstractDictionaryField)fm.getField("FAX_ATTACHMENT")).getTable();
        String multipleTable = fm.getDocumentKind().getMultipleTableName();

        StringBuilder query = new StringBuilder("");
        String sql = null;
        for(Attachment scanAttachment : importedFax.getAttachments()){
            if(scanAttachment.getCn().contains(IncomingFax.FAX_ATTACHMENT_CN)){
//                LoggerFactory.getLogger("krzysiekm").debug("INSERT INTO " + attachmentsTable + " (ATT) VALUES (" + scanAttachment.getId() + ") ");
                query.append("INSERT INTO " + attachmentsTable + " (ATT) VALUES (" + scanAttachment.getId() + ") ");
                sql = "INSERT INTO " + attachmentsTable + " (ATT) VALUES (" + scanAttachment.getId() + ") ";
            }
        }
//        LoggerFactory.getLogger("krzysiekm").debug("QUERY: \n" + query.toString());
        boolean openContext = DSApi.openContextIfNeeded();
        PreparedStatement ps = DSApi.context().prepareStatement(sql);
        ps.execute();
        DSApi.context().closeStatement(ps);
        DSApi.closeContextIfNeeded(openContext);
        setContractorForCokDocument(fax, values);
        documentKind.setOnly(newDocumentId, values, false);
        DSApi.context().session().save(importedFax);
//        addFaxAttachmentsToDocument(doc, fax.getFiles());
        DocumentLogic logic =  documentKind.logic();
        if(logic != null){
            logic.onStartProcess(importedFax, null);
        }else{
            throw new EdmException("Nie uruchomiono procesu z powodu braku logiki dla dokumentu typu " + documentKind.getCn());
        }
        TaskSnapshot.updateByDocument(importedFax);
        return importedFax;
    }

    private static void setContractorForCokDocument(Object sourceData, Map<String,Object> values){
        Contractor contractor;
        try{
            if(sourceData instanceof IncomingFax){
                IncomingFax fax = (IncomingFax)sourceData;
                contractor = Contractor.findByFaxNumber(fax.getDID()).get(0);
                values.put("KLIENT", contractor.getId());
            }else if(sourceData instanceof Map){
                Map<Object, Object> documentImportedData = (Map<Object, Object>)sourceData;
                String contractorNip = (String) documentImportedData.get(IlpolCokImportLogic.NIP);
                //  nip zczytywany z pliku jest konwertowany do postaci double'a stad musimy pozbyc sie czesci ulamkowej
                if(contractorNip.contains("."))
                    contractorNip = contractorNip.substring(0, contractorNip.indexOf("."));
                contractor = Contractor.findByNIP(contractorNip).get(0);
                values.put("CONTRACTOR", contractor.getId());
            }
        }catch(IndexOutOfBoundsException e){
            LOG.debug("Nie znaleziono kontrahenta dla dokumentu COK tworzonego ze źródła typu " + sourceData.getClass());
        }
    }

    public static List<String> prepareValidFaxNumbersForCok() throws EdmException, SQLException {
        boolean openContext = DSApi.openContextIfNeeded();
        PreparedStatement ps = DSApi.context().prepareStatement("SELECT FAX_ACCEPTED_NUMBER FROM DSC_ILPOL_FAX_ACCEPTED_NUMBERS");
        ResultSet rs = ps.executeQuery();
        List<String> faxValidForCok = new ArrayList<String>();
        while(rs.next()) {
            faxValidForCok.add(rs.getString("FAX_ACCEPTED_NUMBER"));
        }
        rs.close();
        DSApi.context().closeStatement(ps);
        DSApi.closeContextIfNeeded(openContext);
        return faxValidForCok;
    }

    private static List<Long> addFaxAttachmentsToDocument(Document document, File[] files) throws EdmException {
        List<Long> attachmentsId = new ArrayList<Long>();
        if(files != null && files.length > 0){
            Attachment attachment = null;
            Long docId = document.getId();
            for(File file : files){
                attachment = new Attachment("skan_faksu");
                attachment.setWparam(docId);
                attachment.setCn(IncomingFax.FAX_ATTACHMENT_CN);
                document.addAttachment(attachment);
                document.createAttachment(attachment);
                attachment.createRevision(file, true);
                attachmentsId.add(attachment.getId());
            }
        }
        return attachmentsId;
    }

    public static List<Long> addAttachmentsToDocument(Document document, Collection<File> files, String attachmentCn) throws EdmException {
        List<Long> attachmentsId = new ArrayList<Long>();
        if(files != null && files.size() > 0){
            Attachment attachment = null;
            Long docId = document.getId();
            for(File file : files){
                attachment = new Attachment(file.getName());
                attachment.setWparam(docId);
                attachment.setCn(attachmentCn);
                document.addAttachment(attachment);
                document.createAttachment(attachment);
                attachment.createRevision(file, true);
                attachmentsId.add(attachment.getId());
                file.delete();
            }
        }
        return attachmentsId;
    }

    public static class Assignment {
        /**
         * GUID działu na ktory dekretujemy
         */
        String guid;
        /**
         * Tworzenie nowego zgłoszenia COK
         */
        boolean createCokDoc = false;

        /**
         * @param enumItem Rodzaj pisma (DOC_TYPE) w xml-u
         */
        public Assignment(EnumItem enumItem) {
            Preconditions.checkNotNull(enumItem);
            this.guid = StringUtils.defaultIfEmpty(enumItem.getArg(1), null);
            this.createCokDoc = Boolean.parseBoolean(enumItem.getArg(2));
        }

        public String getDivisionGuid() {
            if(guid == null){
                return guid;
            }else{
                return StringUtils.removeStart(guid, ";d:");
            }
        }
    }

    public static String retriveUserLogin() throws EdmException {
        try{
            String currentUserName = null;
            Iterator<Principal> iter = null;
            Principal principal = null;

            if(WebContextFactory.get() != null)
                iter = AuthUtil.getSubject(WebContextFactory.get().getHttpServletRequest()).getPrincipals().iterator();
            else
                iter = AuthUtil.getSubject(ServletActionContext.getRequest()).getPrincipals().iterator();

            while(iter.hasNext()){
                principal = iter.next();
                if(principal instanceof pl.compan.docusafe.core.users.auth.UserPrincipal){
                    currentUserName = principal.getName();
                    return currentUserName;
                }
            }
            return currentUserName;
        }catch(Exception e){
            throw new EdmException("Niepowiodło się pozyskanie loginu zalogowanego użytkownika");
        }
    }

    public static DSUser retriveLoggedUser() throws EdmException {
        String loggedUserLogin = retriveUserLogin();
        return DSUser.findByUsername(loggedUserLogin);
    }

    //METODY USTAWIAJACE WARTOSCI (UJEDNOLICONYCH) POL DOCKINDOW DLA DOKUMENTOW COKOWYCH
    public static void setCreationDate(Map<String, FieldData> values, String newValPrefix){
        FieldData creationDate = values.get(newValPrefix + "CREATION_DATETIME");
        Date finishDateTime = Calendar.getInstance().getTime();
        creationDate.setDateData(finishDateTime);
    }


    public static void setDeadlineDateAsLeatesTodaysTime(Map<String, FieldData> values, String newValPrefix){
        FieldData deadlineDateField = values.get(newValPrefix + "DEADLINE_DATETIME");
        Calendar endOfCurrentDayDate = Calendar.getInstance();
        endOfCurrentDayDate.clear(Calendar.HOUR_OF_DAY);
        endOfCurrentDayDate.clear(Calendar.MINUTE);
        endOfCurrentDayDate.set(
                endOfCurrentDayDate.get(Calendar.YEAR),
                endOfCurrentDayDate.get(Calendar.MONTH),
                endOfCurrentDayDate.get(Calendar.DAY_OF_MONTH),
                endOfCurrentDayDate.getMaximum(Calendar.HOUR_OF_DAY),
                endOfCurrentDayDate.getMaximum(Calendar.MINUTE)
        );
        Date deadlineDateTime = endOfCurrentDayDate.getTime();
        deadlineDateField.setDateData(deadlineDateTime);
    }

    public static void setStatus(Map<String, FieldData> values, String newValPrefix){
        FieldData documentStatus = values.get(newValPrefix + "STATUS");
        documentStatus.getEnumValuesData().setSelectedId("1");
    }

    /**
     * Zwraca listę załączników:
     * <ul>
     *     <li>Dla kanału wpływu faks: załączników z faksu</li>
     *     <li>Dla kanalu e-mail: załączników z którymi została wysłana wiadomość</li>
     *     <li>Dla kanału korespondenca: wszystkie załączniki dokumentu</li>
     * </ul>
     * @param fm
     * @return
     */
    public static List<Attachment> getOriginalAttachments(FieldsManager fm) throws EdmException {
        Long documentId = fm.getDocumentId();
        if(documentId == null)
            return Lists.newArrayList();

        List<Attachment> allAttachments = Document.find(documentId).getAttachments();

        Integer iDeliveryType = fm.getIntegerKey("DELIVERY_TYPE");
        IlpolCokLogic.DeliveryType deliveryType = IlpolCokLogic.DeliveryType.fromValue(iDeliveryType);

        class PredicateAttachmentCn implements Predicate<Attachment> {
            String cn;

            PredicateAttachmentCn(String cn) {
                this.cn = cn;
            }

            @Override
            public boolean apply(Attachment attachment) {
                return cn.equals(attachment.getCn());
            }
        }

        Predicate<Attachment> predicate = null;
        if(deliveryType == IlpolCokLogic.DeliveryType.FAX) {
            predicate = new PredicateAttachmentCn(IncomingFax.FAX_ATTACHMENT_CN);
        } else if(deliveryType == IlpolCokLogic.DeliveryType.EMAIL) {
            predicate = new PredicateAttachmentCn(MailMessage.MAIL_ATTACHMENT_CN);
        } else if(deliveryType == IlpolCokLogic.DeliveryType.DOCUMENT_IN) {
            predicate = Predicates.alwaysTrue();
        }

        if(predicate != null) {
            return Lists.newArrayList(Iterables.filter(allAttachments, predicate));
        }

        return Lists.newArrayList();
    }

}
