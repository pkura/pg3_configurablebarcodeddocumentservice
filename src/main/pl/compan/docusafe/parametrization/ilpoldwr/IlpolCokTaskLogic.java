package pl.compan.docusafe.parametrization.ilpoldwr;

import com.google.common.collect.Maps;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.sql.AcceptanceDivisionImpl;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class IlpolCokTaskLogic extends AbstractDocumentLogic {

    private static final Logger LOG = LoggerFactory.getLogger(IlpolCokTaskLogic.class);

    public static final String DOCKIND_NAME = "cok_task_doc";
    public static final String DOCKIND_TABLE_NAME = "DSC_ILPOL_TASK_DOC";
    public static final Integer ZADANIE_ZAKONCZONE = new Integer(5);
    private Object assignee;

    @Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        Field field = super.validateDwr(values, fm);

        if(isTaskFinished(values)){
            DwrUtils.setCurrentDate(values, "DWR_FINISH_DATETIME");
        } else{
            values.get("DWR_FINISH_DATETIME").setStringData("");
        }

        return field;
    }

    public boolean isTaskFinished(Map<String, FieldData> values) throws EdmException {
        FieldData documentStatus = values.get("DWR_STATUS");
        return DataBaseEnumField.getEnumItemForTable("DSC_ILPOL_COK_TASK_STATUS", Integer.parseInt(documentStatus.getEnumValuesData().getSelectedId())).getTitle().contains("Zakończone");
    }

    public void onStartProcess(OfficeDocument document, ActionEvent event, Object assignee) throws EdmException {
        setAssignee(assignee);
        onStartProcess(document, event);
    }

    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
        super.onStartProcess(document);

        try
        {
            DSUser author = DSUser.findByUsername(document.getCreatingUser());
            String user = author.getName();
            String fullName = author.getLastname() + " " + author.getFirstname();
            Map<String, Object> map = Maps.newHashMap();
            //wartosci odpowiadajace za ustawienie osoby dekretujacej zadanie
            map.put("previousAssignee", user);
            map.put("currentAssignee", user);
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

            java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
            perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
            perms.addAll(documentPermissions);
            this.setUpPermission(document, perms);

            DSApi.context().watch(URN.create(document));
        }
        catch (Exception e)
        {
            LOG.error(e.getMessage(), e);
            throw new EdmException(e.getMessage());
        }
    }

    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
        if (acceptationCn != null) {
            try {
                Object assignee = null;
                DocumentLogic logic = doc.getDocumentKind().logic();
                if(logic instanceof IlpolCokTaskLogic){
                    assignee = ((IlpolCokTaskLogic)logic).getAssignee();
                }
                String id = null;

                try {
                    if(assignee instanceof DSUser){
                        id = ((DSUser) assignee).getName();
                        assignable.addCandidateUser(id);
                        addToHistory(openExecution, doc, id, null);
                    }else if(assignee instanceof DSDivision){
                        id = ((DSDivision) assignee).getGuid();
                        assignable.addCandidateGroup(id);
                        addToHistory(openExecution, doc, null, id);
                    }else if(assignee instanceof AcceptanceDivisionImpl){
                        return false;
                    }
                    LOG.info("For fieldValue " + id);
                } catch (UserNotFoundException e) {
                    LOG.error(e.getMessage());
                }
                return true;
            } catch (EdmException e1) {
                LOG.error(e1.getMessage());
            }
        }
        return false;
    }

    public static void addToHistory(OpenExecution openExecution, OfficeDocument doc, String user, String guid) throws EdmException, UserNotFoundException {
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setSourceUser(DSApi.context().getPrincipalName());
        ahe.setProcessName(openExecution.getProcessDefinitionId());
        if (user == null) {
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_DIVISION);
            ahe.setTargetGuid(DSDivision.find(guid).getName());
        } else {
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_SINGLE_USER);
            ahe.setTargetUser(user);
        }
        DSDivision[] divs = DSApi.context().getDSUser().getOriginalDivisionsWithoutGroup();
        if (divs != null && divs.length > 0)
            ahe.setSourceGuid(divs[0].getName());
        ahe.setCtime(new Date());
        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
        try {
            String status = doc.getFieldsManager().getEnumItem("STATUS").getTitle();
            ahe.setStatus(status);
        } catch (Exception e) {
            LOG.warn(e.getMessage());
        }
        doc.addAssignmentHistoryEntry(ahe);
    }

    @Override
    public TaskListParams getTaskListParams(DocumentKind kind, long documentId, TaskSnapshot task) throws EdmException {
        TaskListParams params = new TaskListParams();
        String taskStatus = "brak";
        String taskSubject = "brak";
        String contractorName = "brak";
        String contractorNip = "brak";
        Document cokDocument;
        try {
            FieldsManager fm = kind.getFieldsManager(documentId);
            if(fm.getValue("STATUS") != null)
                taskStatus = fm.getValue("STATUS").toString();
            if(fm.getValue("COK_ID") != null){
                if(((Map)fm.getValue("COK_ID")).get("id") != null){
                    cokDocument = Document.find(Long.parseLong(((Map)fm.getValue("COK_ID")).get("id").toString()));
                    int deliveryVal = cokDocument.getFieldsManager().getField("DELIVERY_TYPE").getEnumItemByTitle(cokDocument.getFieldsManager().getValue("DELIVERY_TYPE").toString()).getId();
                    switch(deliveryVal){
                        case IlpolCokLogic.DELIVERY_TYPE_EMAIL:
                            taskSubject = (String) cokDocument.getFieldsManager().getValue("SUBJECT_EMAIL");
                            break;
                        case IlpolCokLogic.DELIVERY_TYPE_MANUAL:
                            taskSubject = (String) cokDocument.getFieldsManager().getValue("SUBJECT_MANUAL");
                            break;
                        case IlpolCokLogic.DELIVERY_TYPE_FAX:
                            taskSubject = (String) cokDocument.getFieldsManager().getValue("SUBJECT_MANUAL");
                            break;
                        case IlpolCokLogic.DELIVERY_TYPE_DOCUMENT_IN:
                            taskSubject = (String) cokDocument.getFieldsManager().getValue("SUBJECT_MANUAL");
                            break;
                        case IlpolCokLogic.DELIVERY_TYPE_IMPORT:
                            taskSubject = (String) cokDocument.getFieldsManager().getValue("SUBJECT_MANUAL");
                            break;
                        default:
                            break;
                    }
                }
            }
            if(fm.getValue("CONTRACTOR") != null) {
                try {
                    Map<String, Object> contractorDic = (Map<String, Object>) fm.getValue("CONTRACTOR");
                    if(contractorDic.get("CONTRACTOR_NAME") != null) {
                        contractorName = contractorDic.get("CONTRACTOR_NAME").toString();
                    }
                    if(contractorDic.get("CONTRACTOR_NIP") != null) {
                        contractorNip = contractorDic.get("CONTRACTOR_NIP").toString();
                    }
                } catch(ClassCastException e) {
                    LOG.error(e.getMessage(), e);
                }
            }
        } catch(Exception e) {
            LOG.error(e.getMessage(), e);
            LoggerFactory.getLogger("krzysiekm").debug("PARAMS ERROR " + e.getMessage());
        } finally {
            params.setAttribute(contractorName, 0);
            params.setAttribute(taskSubject, 1);
            params.setAttribute(contractorNip, 2);
            params.setAttribute(taskStatus, 3);
        }
        return params;
    }

    public void sendEmailNotice(Long documentId, String taskFinisherUsername) throws EdmException, IOException {
        Document document = Document.find(documentId, false, null);
        DocumentKind documentKind = document.getDocumentKind();
        DocumentLogic documentLogic = documentKind.logic();
        DSUser noticeReceiver = getEmailNoticeReceiver(document);

        Map<String, Object> emailMessageContent = new HashMap<String, Object>();
        emailMessageContent.put("subject", "Zakończono zadanie");
        emailMessageContent.put("documentType", "zadanie");
        emailMessageContent.put("task_finisher", taskFinisherUsername);
        emailMessageContent.put("datetime", DateUtils.formatCommonDateTime(DateUtils.getGregorianCalendar().getTime()));
        //  Docusafe.getBaseUrl() - wartość sczytywana z kat. HOME z pliku docusafe.config
        emailMessageContent.put("documentUrl", Docusafe.getBaseUrl() + "/repository/edit-dockind-document.action?id=" + documentId);
        ((Mailer) ServiceManager.getService(Mailer.NAME)).send(noticeReceiver.getEmail(), noticeReceiver.getEmail(), null, Configuration.getMail(Mail.getInstance("completion-notice.txt")), emailMessageContent);
    }

    private DSUser getEmailNoticeReceiver(Document document) throws EdmException {
        FieldsManager fm = document.getFieldsManager();
        DSUser noticeReceiver = null;
        //  jesli zakonczamy zadanie i jest ono powiazane ze sprawa, to identyfikujemy wlasciciela sprawy
        if(document.getDocumentKind().getCn().trim().equals(IlpolCokTaskLogic.DOCKIND_NAME)){
            Object caseId = ((Map)fm.getValue("CASE_ID")).get("id");
            if(caseId != null){
                Document connectedCaseDocument = Document.find(Long.parseLong(caseId.toString()));
                if(connectedCaseDocument != null){
                    noticeReceiver = DSUser.findByUsername(((InOfficeDocument)connectedCaseDocument).getCreatingUser());
                    if(noticeReceiver == null){
                        throw new EdmException("ILPOL: Nie znaleziono właściciela sprawy powiązanego z zadaniem o id " + document.getId());
                    }else{
                        return noticeReceiver;
                    }
                }
            }
        }
        return noticeReceiver;
    }
    
    @Override
    public void onCommitManualFinish(Document document, Boolean isDoWiadomosci) throws EdmException {
    }


    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onEndProcess(OfficeDocument document, boolean isManual) throws EdmException {
        super.onEndProcess(document, isManual);
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object getAssignee() {
        return assignee;
    }

    public void setAssignee(Object assignee) {
        this.assignee = assignee;
    }
}
