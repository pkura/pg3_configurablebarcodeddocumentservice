package pl.compan.docusafe.parametrization.ilpoldwr;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.parametrization.ilpol.DlApplicationDictionary;
import pl.compan.docusafe.service.imports.ImpulsImportUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class ApplicationDictionary extends DwrDictionaryBase{

	private static StringManager sm = GlobalPreferences.loadPropertiesFile(ApplicationDictionary.class.getPackage().getName(),null);
	private static final Logger log = LoggerFactory.getLogger(LeasingDictionary.class);
	
	
	public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset) throws EdmException
	{
		List<Map<String, Object>> ret = Lists.newArrayList();

		if(values.containsKey("id") && values.size() == 1)
		{
			DlApplicationDictionary app = null;
			Long appId = Long.parseLong(values.get("id").getData().toString());
			app = DlApplicationDictionary.getInstance().find(appId);
    		if(app == null)
    			return ret;
    		Map<String, Object> row = Maps.newLinkedHashMap();
    		row.put("id", app.getId());
			row.put("NUMER_WNIOSKU_NUMERWNIOSKU", app.getNumerWniosku());
			row.put("NUMER_WNIOSKU_OPIS", app.getOpis());
			row.put("NUMER_WNIOSKU_LEASINGOBJECTTYPE", app.getLeasingObjectType());
			ret.add(row);
			return ret;
		}
		
		String numerwniosku = "";
		boolean contextOpened = true;
		try
		{
		if (!(values.containsKey("NUMER_WNIOSKU_NUMERWNIOSKU") && !(values.get("NUMER_WNIOSKU_NUMERWNIOSKU").getData().toString()).equals("")))
			return ret;
			
		numerwniosku = values.get("NUMER_WNIOSKU_NUMERWNIOSKU").getData().toString();
		if(numerwniosku.length() < 4)
			return ret;
		
		
			if (!DSApi.isContextOpen())
			{
				DSApi.openAdmin();
				contextOpened = false;
			}
			
			
            QueryForm form = new QueryForm(offset, 10);
            form.addProperty("numerWniosku", numerwniosku);    
            form.addOrderAsc("numerWniosku");
            SearchResults<? extends DlApplicationDictionary> results = DlApplicationDictionary.search(form);
            if (results == null || results.totalCount() == 0)
            {
            	DlApplicationDictionary app = null;

            	 	DSApi.context().begin();
            		app = ImpulsImportUtils.findApplicationByNumber(numerwniosku);
            		DSApi.context().commit();
            		if(app == null)
            		{
            			return ret;
            		}
            		
//            		if(!DSApi.context().hasPermission(DSPermission.CONTRACTOR_ALL))
//                    {
//                    	if(!DSApi.context().hasPermission(DSPermission.CONTRACTOR_REGION))
//                    	{
//                    		if(!DSApi.context().hasPermission(DSPermission.CONTRACTOR_USER))
//                    		{
//                    			return ret;
//                    		}
//                    		else
//                    		{
//                    			Contractor contractor = DSApi.context().load(Contractor.class, app.getIdKlienta());
//                    			
//                    			if(!contractor.patronsContainsUser(DSApi.context().getPrincipalName()))
//                    				return ret;
//                    		}
//                    	}
//                    	else
//                    	{
//                    		Contractor contractor = DSApi.context().load(Contractor.class, app.getIdKlienta());
//                    		if(contractor.getRegion() != null)
//                    		{
//                    			HashSet<String> divisionSet = new HashSet<String>(Arrays.asList(DSApi.context().getDSUser().getDivisionsGuid()));
//                    			if(!divisionSet.contains("DV_"+contractor.getRegion().getCn()))
//                    				return ret;
//                    		}
//                    	}
//                    }
            		Map<String, Object> row = Maps.newLinkedHashMap();
    				row.put("id", app.getId());
    				row.put("NUMER_WNIOSKU_NUMERWNIOSKU", app.getNumerWniosku());
    				row.put("NUMER_WNIOSKU_OPIS", app.getOpis());
    				row.put("NUMER_WNIOSKU_LEASINGOBJECTTYPE", app.getLeasingObjectType());
    				ret.add(row);
        			return ret;
            	
            }
            else
            {
            	while (results.hasNext())
            	{
					DlApplicationDictionary app = (DlApplicationDictionary) results.next();
					Map<String, Object> row = Maps.newLinkedHashMap();
    				row.put("id", app.getId());
    				row.put("NUMER_WNIOSKU_NUMERWNIOSKU", app.getNumerWniosku());
    				row.put("NUMER_WNIOSKU_OPIS", app.getOpis());
    				row.put("NUMER_WNIOSKU_LEASINGOBJECTTYPE", app.getLeasingObjectType());
    				ret.add(row);
				}
            	return ret;
            }
			
		}
		catch (Exception ie)
		{
			log.error(ie.getMessage(), ie);
		}
		finally
		{
			if (!contextOpened && DSApi.isContextOpen())
				DSApi.close();
		}
		return ret;
	}
}

