package pl.compan.docusafe.parametrization.ilpoldwr;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.jbpm4.MigrationWorkflow;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class BinderDivisionDecision implements DecisionHandler
{

	private final static Logger LOG = LoggerFactory.getLogger(BinderDivisionDecision.class);
	
	@Override
	public String decide(OpenExecution execution)
	{
		try
		{
			String username = (String) execution.getVariable(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM);
			OfficeDocument document = Jbpm4Utils.getDocument(execution);
			String workFlowPlace = null;
			if(username != null)
			{
				workFlowPlace =   MigrationWorkflow.getWorkFlowPlace(username);
			}
			if(StringUtils.isBlank(workFlowPlace))
			{
				workFlowPlace = checkWorflowPlace(document);
				if (workFlowPlace != null)
					return workFlowPlace;
	//			
	//			String author = document.getAuthor();
	//			DSUser user = DSUser.findByUsername(author);
	//		
	//			if (user.getDivisionsWithoutGroup().length > 0 && user.getDivisionsWithoutGroup()[0].getGuid() != null)
	//			{
	//				String userGuid = user.getDivisionsWithoutGroup()[0].getGuid();
	//				if (execution.getActivity().findActivity(userGuid) != null)
	//					return userGuid;
	//			}
			}
			else
			{
				return workFlowPlace;
			}
			return "DL_BINDER_DS";
		}
		catch (Exception e)
		{
			LOG.error(e.getMessage(), e);
			return "DL_BINDER_DS";
		}	
	}

	private String checkWorflowPlace(OfficeDocument document)
	{
		String worklowPlace = null;
		PreparedStatement ps = null;
		try
		{			
			ps = DSApi.context().prepareStatement("select WORKFLOW_PLACE from dsg_dlbinder where document_id = ? ");
			ps.setLong(1,document.getId());
			ResultSet rs = ps.executeQuery();
			while (rs.next())
			{
				int workPlaceId = rs.getInt(1);
				switch (workPlaceId)
				{
					case 9:
						worklowPlace = "DL_BINDER_DS";
						break;
					case 19:
						worklowPlace = "DL_BINDER_DR";
						break;
					case 29:
						worklowPlace = "DL_BINDER_DO";
						break;
					case 39:
						worklowPlace = "DL_BINDER_DK";
						break;
					case 25:
						worklowPlace = "DL_BINDER_DU"; // nie obslugiujemy dzialu ubezpieczeni, ale na starej teczce byla mozliwosc wyboru tego dzialu
						break;
				}
			}
			rs.close();
			ps.close();

		}
		catch (Exception ie)
		{
			LOG.error(ie.getMessage(), ie);
		}
		finally
		{

			DbUtils.closeQuietly(ps);
		}
		return worklowPlace;
	}
}
