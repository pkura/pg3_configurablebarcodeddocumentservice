package pl.compan.docusafe.parametrization.ilpoldwr;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.dockinds.field.AbstractFieldLogic;
import pl.compan.docusafe.core.dockinds.field.HtmlEditorNonColumnField;

public class SentResponseHtmlEditorFieldLogic extends AbstractFieldLogic {
	
	/**
	 * komunikat, ktory zostanie wyswietlony w historii dokumentu w przypadku zmiany wartosci pola (odpowiedzenia na otrzymanego maila)
	 */
	private String historyEntryMessage;
	
	
	@Override
	public String prepareCustomFieldHistoryEntry(Object oldDesc, Object oldKey, Object newDesc, Object newKey) {
		if(field != null || field instanceof HtmlEditorNonColumnField){
			StringBuilder historyBuilder;
			if(StringUtils.isEmpty(historyEntryMessage))
				return null;
			else
				historyBuilder = new StringBuilder(getHistoryEntryMessage());
			return historyBuilder.toString();
		}else{
			return null;
		}
	}


	public String getHistoryEntryMessage() {
		return historyEntryMessage;
	}


	public void setHistoryEntryMessage(String historyEntryMessage) {
		this.historyEntryMessage = historyEntryMessage;
	}
	
	
}
