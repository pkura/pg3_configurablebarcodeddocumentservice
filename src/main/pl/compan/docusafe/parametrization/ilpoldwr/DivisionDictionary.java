package pl.compan.docusafe.parametrization.ilpoldwr;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.sql.DivisionImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Krzysztof Modzelewski
 * Date: 14.11.13
 * Time: 11:00
 */
public class DivisionDictionary extends DwrDictionaryBase {

    public static final String DIVISION = "DIVISION";

    @Override
    public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset) throws EdmException
    {
        List<Map<String, Object>> results = Lists.newArrayList();
        String dictionaryFieldCn = this.getName();
        String divisionFieldCn = null;
        List<DSDivision> divisions = null;
        try {
            if(values.containsKey("id") && !values.get("id").getStringData().equals(StringUtils.EMPTY)){
                DSDivision division = DSDivision.findById(Integer.parseInt(values.get("id").getStringData()));
                if(division != null){
                    divisionFieldCn = "NAME";
                    divisions = new ArrayList<DSDivision>(1);
                    divisions.add(division);
                }
            }else if(!values.containsKey("id")){
                Criteria crit = DSApi.context().session().createCriteria(DivisionImpl.class);
                for(String fieldName : this.getSearchableFieldsCns()){
                    fieldName = fieldName.replaceFirst(dictionaryFieldCn + "_", "");
                    if(fieldName.toLowerCase().equals("name")){
                        divisionFieldCn = fieldName;
                        crit.add(Restrictions.or(
                                (Restrictions.ilike("name", String.valueOf(values.get(dictionaryFieldCn + "_" + divisionFieldCn)).toLowerCase(), MatchMode.ANYWHERE)),
                                (Restrictions.ilike("name", String.valueOf(values.get(dictionaryFieldCn + "_" + divisionFieldCn)).toUpperCase(), MatchMode.ANYWHERE))
                        ));
                    }
                }
                crit.add((Restrictions.eq("hidden", false)));
                crit.add(Restrictions.or(
                        (Restrictions.ilike("divisionType", "division")),
                        (Restrictions.ilike("divisionType", "position"))
                ));
                divisions = crit.list();
            }

            if(divisions != null){
                for(DSDivision division : divisions) {
                    Map<String, Object> singleResult = Maps.newHashMap();
                    singleResult.put("id", division.getId());
                    singleResult.put(dictionaryFieldCn + "_" + divisionFieldCn, division.getName());
                    results.add(singleResult);
                }
            }
        } catch(HibernateException e) {
            throw new EdmException(e.getLocalizedMessage(), e);
        }

        return results;
    }
}
