package pl.compan.docusafe.parametrization.paa;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class DocWychodzacyWyborWyslijDo_DecisionHandler implements DecisionHandler
{
	private static final Logger log = LoggerFactory.getLogger(DocWychodzacyWyborWyslijDo_DecisionHandler.class);

	public String decide(OpenExecution openExecution)
	{
		log.info("DocWychodzacyWyborWyslijDo_DecisionHandler");
		Map<Integer,String> statusy = new HashMap<Integer, String>();
		
		statusy.put(1, "wyslijDoDyrektor");
		statusy.put(2, "zwroconoWyslijDoDyrektor");
		statusy.put(3, "akceptacjaDyrektora");
		statusy.put(4, "sekretariat");
		statusy.put(5, "kancelaria");
		statusy.put(6, "kancelariaKoniec");
		statusy.put(10, "wyslijDoSekretariatu");
		statusy.put(11, "konsultacja");
		statusy.put(12, "sekretariatPrezesa");
		statusy.put(13, "kancelariaOgolna_wyslaniePisma");
		statusy.put(14, "wyslijDoPrezes");
		statusy.put(15, "zwroconoWyslijDoPrezes");
		
		
	
		
		String decision = "do_dyrektor";
		
		decision = "do_prezes";
		
		try {
			Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			DSUser user =DSUser.findByUsername(doc.getAuthor());
			String data = Calendar.getInstance().getTime().toString();
			String rodzajDokumentu = doc.getDocumentKind().getCn();
			log.info("Dokument  id = "+docId+"Rodzaj = " +rodzajDokumentu+ " czas zajscia procesu "+data);
			/**
			 * 2. sprawdz czy wnioskodawca to pracownik czy dyrektor
			 */
			// ustawianie statusu procesu dla pism powstałych z podzielenia pisma multi odbiorców 
			if (fm.getBoolean("PODZIELONE")){
			Integer status = fm.getEnumItem("STATUSDOKUMENTU").getId();
			for (Integer key : statusy.keySet()){
				if (key == status){
					decision = (String) statusy.get(key);
					break;
				}
			}
			return decision;
			}
				
			
			if (user.getName().equals("wlodarski")){
				decision = "do_sekretariat";
				return decision;
			}
			boolean isPracownik = !CzyOdbiorcaToDyrDepartamentu_DecisionHandler.czyUserToDyr(user);
		
			
			{
				// pracownik
				if(isPracownik)
					decision= "do_dyrektor";
				else
					decision= "do_prezes";
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		//dyrektor, prezes, wiceprezes, naczelnik
		return decision;
	}
}
