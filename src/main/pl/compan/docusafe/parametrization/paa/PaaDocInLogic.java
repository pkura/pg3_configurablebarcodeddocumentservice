package pl.compan.docusafe.parametrization.paa;
import com.google.common.collect.Maps;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.DwrDocumentHelper;
import pl.compan.docusafe.webwork.event.ActionEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.*;

@SuppressWarnings("serial")
public class PaaDocInLogic extends AbstractDocumentLogic
{
	public final static String _USER = "USER";
	public final static String _DIVISION = "DSDIVISION";
	public final static String _stampDate = "stampDate";
	public final static String _znak_pisma = "znak_pisma";
	public final static String _SENDER_HERE = "SENDER_HERE";
	public final static String _POSITION = "POSITION";
	public final static String _NUMERPISMA = "NUMERPISMA";
	public final static String _AVAILABLE_DAYS ="AVAILABLE_DAYS";
	public final static String _PELENADRES = "PELENADRES";
	public static final String  KIND_EXECUTE = "Do Realizacji";
	public static final String _multiOdbiorca = "M_DICT_VALUES";
	public static final String _pelenAdres = "PELENADRES";
	public static final String _RECIPIENT = "RECIPIENT";
	public static final String _REPILES_FOR = "REPILES_FOR";
	public static final String _RODZAJ_PRZESYLKI = "RODZAJ_PRZESYLKI";	
	public static final String _SPOSOB_DOSTARCZENIA = "SPOSOB_DOSTARCZENIA";
	public static final String _idMultiPerson = "ID";	
	public static final String _kosztJednostkowy = "KWOTA";	
	public static final String _rodzajPrzesylkiWagaKrajowa = "RODZAJ_PRZESYΘI_TYPE_WEIGHT_COUNTRY";	
	public static final String _rodzajPrzesylkiWagaSwiat = "RODZAJ_PRZESYΘI_TYPE_WEIGHT_COUNTRY";
	public static final String _GABARYT = "GABARYT";
	public static final String _dataDokumentu = "DOC_DATE";
	public static final String _odpowiedzNa = "REPILES_FOR";
	public static final String _biuroDyrGeneralnego = "WBDG";
	public static final String _KoszListu = "KOSZT_PRZESYLKI";
	
	public  boolean WalidacjaDatWniosku ;
	protected static DwrDocumentHelper dwrDocumentHelper = new DwrDocumentHelper();
	public boolean isWalidacjaDatWniosku()
	{
		return WalidacjaDatWniosku;
	}

	private static PaaDocInLogic instance;
	protected static Logger log = LoggerFactory.getLogger(PaaDocInLogic.class);

	public static PaaDocInLogic getInstance()
	{
		if (instance == null)
			instance = new PaaDocInLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}
	//nowa kolumna na liscie zadan 
	public TaskListParams getTaskListParams(DocumentKind dockind, long id)
			throws EdmException {
		TaskListParams params = new TaskListParams();

		try {
			FieldsManager fm = dockind.getFieldsManager(id);

				if (fm.getValue("realizationDate") != null)
					params.setAttribute(DateUtils.formatCommonDate((Date) fm
							.getValue("realizationDate")), 0);

				if (fm.getValue("znak_pisma") != null)
					params.setAttribute((String) fm.getValue("znak_pisma"), 1);

				if (fm.getValue(_pelenAdres) != null)
					params.setAttribute((String) fm.getValue(_pelenAdres), 2);

			
		} catch (Exception e) {
			log.error("", e);
		}
		return params;
	}
	
	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
	{
		Field msg = null;
		
		if (values.get("DWR_DOC_DATE") != null && values.get("DWR_DOC_DATE").getData() != null)
		{
			Date startDate = new Date();
			Date finishDate = values.get("DWR_DOC_DATE").getDateData();
			
			if (finishDate.after(startDate))
			{
				msg = new Field("messageField", "Data pisma nie mo瞠 by� dat� z przysz這�ci.", Field.Type.BOOLEAN);
				values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
			}
			
		}
		

		return msg;
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		
		log.info("type: {}", type);
		Map<String, Object> values= new HashMap<String, Object>();
		values.put("TYPE", type);

		values.put(_stampDate, Calendar.getInstance().getTime());

		fm.reloadValues(values);
			
	}
	
	/*@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
			try
			{
				Document doc = Document.find(docId);
				FieldsManager fm = doc.getFieldsManager();
				fm.initialize();

				
				String odbiorcy = fm.getStringKey(_RECIPIENT);
				String[] ids;
				
				if (odbiorcy.startsWith("[")) {
					odbiorcy= odbiorcy.substring(1, odbiorcy.length() - 1);
					ids= odbiorcy.split(",");
				} else {
					ids= new String[1];
					ids[0]= odbiorcy;
				}
				for (String id : ids) {
				Map<String,Object> odbiorca = DwrDictionaryFacade.dictionarieObjects.get(_RECIPIENT).getValues(id.trim());
				
				values.put("TITLE", odbiorca.get("RECIPIENT_TITLE")!=null ? odbiorca.get("RECIPIENT_TITLE") : "" );
				values.put("FIRSTAME", odbiorca.get("RECIPIENT_FIRSTNAME")!=null ? odbiorca.get("RECIPIENT_FIRSTNAME") : "" );
				values.put("LASTNAME", odbiorca.get("RECIPIENT_LASTNAME")!=null ? odbiorca.get("RECIPIENT_LASTNAME") : "" );
				values.put("ORGANIZATION", odbiorca.get("RECIPIENT_ORGANIZATION")!=null ? odbiorca.get("RECIPIENT_ORGANIZATION") : "" );
				values.put("ZIP", odbiorca.get("RECIPIENT_ZIP")!=null ? odbiorca.get("RECIPIENT_ZIP") : "" );
				values.put("LOCATION", odbiorca.get("RECIPIENT_LOCATION")!=null ? odbiorca.get("RECIPIENT_LOCATION") : "" );
				}
		
			
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
	}*/
	
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		as.useFolderResolver(document);
		as.useAvailableProviders(document);
		log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
	}

	@Override
	public String onStartManualAssignment(OfficeDocument doc, String fromUsername, String toUsername)
	{
		log.info("--- PaaDocInLogic : onStartManualAssignment  dekretacja reczna dokument闚 :: dokument ID = "+doc.getId()+" : pomiedzy nadawc� -->"+fromUsername +" a odbiorc� ->"+toUsername );
		log.info("--- PaaDocInLogic : Sprawdzam Czy s� w tym samym departamencie ");
		if(!NormalLogic.inDepIsOutDep(fromUsername, toUsername)){
				log.info("--- NormalLogic : Wynik ->  Nie s� w tym samym  pobieram dyrektora departamentu  ");
				toUsername = NormalLogic.getDyrektorDepartamentu(toUsername ,doc);
				/*if (toUsername.contains(_biuroDyrGeneralnego))
					try {
						toUsername = NormalLogic.getDyrektorGeneralny();
					} catch (Exception e) {
						log.error("", e);
					}*/
				log.info("--- NormalLogic : Wynik -> pobra貫m dyrektora -> " + toUsername);
				//if(toUsername.contains(staryOdbiorca)){
				//	toUsername =fromUsername;
			//	}
				
			}
	
		log.info("--- PaaDocInLogic : WYnik ->  ostateczny rezultat  dekretacja na -> "+ toUsername);
		return toUsername;
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- PaaDocInLogic : START PROCESS !!! ---- {}", event);
		try {

			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM,event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind()
					.getDockindInfo()
					.getProcessesDeclarations()
					.onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));

				String ZnakPisma = document.getFieldsManager().getStringValue(_znak_pisma);
				InOfficeDocument od = (InOfficeDocument) document;
				if (ZnakPisma != null) {
					od.setReferenceId(ZnakPisma);
				} else {
					od.setReferenceId("Brak");
				}
			

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

	}
	 
	@Override
	public void validate(Map<String, Object> values, DocumentKind kind,Long documentId) throws EdmException {
		
		System.out.println(kind.getCn());
		System.out.println(values);

			if (documentId != null) {
				OfficeDocument doc = OfficeDocument.find(documentId);
				if (doc.getOfficeNumber() != null) {
					String wpis = "PAA/";
					int rok = Calendar.getInstance().get(Calendar.YEAR);
					values.put(_NUMERPISMA, wpis + doc.getOfficeNumber() + "/"
							+ rok);
				}
			}
		if (documentId != null) {
			StringBuilder sb = new StringBuilder();
			OfficeDocument doc = OfficeDocument.find(documentId);
			if (doc.getSender() != null) {
				Sender sender = doc.getSender();
				sb.append(sender.getOrganization() != null ? sender.getOrganization() : "");
				sb.append(",");
				sb.append(sender.getFirstname() != null ? sender.getFirstname(): "");
				sb.append(" ");
				sb.append(sender.getLastname() != null ? sender.getLastname(): "");
				sb.append(", ");
				sb.append(sender.getStreet() != null ? sender.getStreet() : "");
				sb.append(" ");
				sb.append(sender.getZip() != null ? sender.getZip() : "");
				sb.append(", ");
				sb.append(sender.getLocation() != null ? sender.getLocation(): "");
				values.put("PELENADRES", sb.toString());
			}
		}
	}
	
	

	@Override
	public void onStartProcess(OfficeDocument document)
	{
	
		try {
			Map<String, Object> map= Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, document.getAuthor());
			map.put(ASSIGN_DIVISION_GUID_PARAM, document.getDivisionGuid());
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}


	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret= new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}
	/*@Override
	public boolean canSearchAllJournals(){
		return false;
	}*/
	@Override
	 public List<Journal> getJournalsPermissed(String journalType) throws EdmException {
		
		List<Journal> docJournals = new ArrayList<Journal>();
		if (DSApi.context().hasPermission(DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD))
        {
			docJournals = Journal.findByType(journalType);
    		docJournals.addAll(Journal.findByType(journalType, true));
    		
        }
    	else
    	{
            for (DSDivision division : DSApi.context().getDSUser().getDivisions())
            {
        		docJournals = Journal.findByDivisionGuid(division.getGuid(),journalType);
        		docJournals.addAll(Journal.findByDivisionGuid(division.getGuid(),journalType,true));
            }
        }
        if (DSApi.context().hasPermission(DSPermission.DZIENNIK_GLOWNY_PODGLAD)&& !DSApi.context().hasPermission(DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD)){
        	
        		docJournals.add(Journal.getMainJournal(journalType));
    		
        	 
        }
		return docJournals;
		
	}
	
	@Override
	public void onRejectedListener(Document doc) throws EdmException
	{
	}

	@Override
	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException
	{
	}
	
	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException,
			EdmException
	{ 
		
		
	}

}

	