package pl.compan.docusafe.parametrization.paa;

import java.util.Calendar;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** Asygnuje dokument na osobe zapamietana we wskazanej zmiennej procesu */
@SuppressWarnings("serial")
public class ZapamietanaOsoba_AsgnHandler implements AssignmentHandler{
	private static final Logger log = LoggerFactory.getLogger(WniosekUrlopowyWyborAkceptacji_DecisionHandler.class);

	private String nazwaZmiennej;
	
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {

		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		String data = Calendar.getInstance().getTime().toString();
		OfficeDocument doc = OfficeDocument.find(docId);
		String rodzajDokumentu = doc.getDocumentKind().getCn();
		log.info("Dokument  id = "+docId+"Rodzaj =" +rodzajDokumentu+ " czas zajscia procesu "+data);
		String asygnowanyUzytkownik = (String) openExecution.getVariable(nazwaZmiennej); // gotowe
		
		//dekretowanie
		log.info(" DekretujÍ na "+asygnowanyUzytkownik);
		assignable.addCandidateUser(asygnowanyUzytkownik);
		
		AssignmentHistoryEntry entry = new AssignmentHistoryEntry();
		
		entry.setSourceUser(DSApi.context().getDSUser().getName());
		entry.setTargetUser(asygnowanyUzytkownik);
		String dzialNad =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(DSApi.context().getDSUser().getName());
		entry.setSourceGuid(dzialNad);
		String dzialOdb =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(asygnowanyUzytkownik);
		entry.setTargetGuid(dzialOdb);
		String statusDokumentu =  doc.getFieldsManager().getEnumItem("STATUSDOKUMENTU").getTitle();
		entry.setObjective(statusDokumentu);
		entry.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
		doc.addAssignmentHistoryEntry(entry);
		
		
		// dodac recznie do historii (w WSSK robilem to tak:) 
		// Util_AHE.addToHistory(openExecution, doc, asygnowanyUzytkownik, null, null, log);
	}
}
