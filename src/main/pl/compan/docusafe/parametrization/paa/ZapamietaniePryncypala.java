package pl.compan.docusafe.parametrization.paa;

import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import pl.compan.docusafe.core.DSApi;

/** zapamietuje w zmiennej procesu,
 * kto jako ostatnio edytowal pismo - potem bedzie mozna je do niego zwrocic */
@SuppressWarnings("serial")
public class ZapamietaniePryncypala implements ExternalActivityBehaviour  {
	Logger log = LoggerFactory.getLogger(ZapamietaniePryncypala.class);
	
	private String nazwaZmiennej;
	
	@Override
	public void execute(ActivityExecution execution) throws Exception {

		execution.setVariable(nazwaZmiennej, DSApi.context().getPrincipalName()); // gotowe :)
		
		if (execution.getActivity().getDefaultOutgoingTransition() != null) // tego nie kasowac !!!
			execution.takeDefaultTransition();
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
	}
}
