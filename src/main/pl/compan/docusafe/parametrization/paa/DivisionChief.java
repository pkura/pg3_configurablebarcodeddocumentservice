package pl.compan.docusafe.parametrization.paa;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;

/**
 * klasa do "dyrektor dzialu"
 * 
 * @author bkaliszewski
 * 
 */

public class DivisionChief
{
	/*
	public static final String[] dyrektorSynonimy= { "Prezes", "Wiceprezes", "Dyrektor", "Naczelnik"
		//, "p.o. Dyrektor", "Dyrektor Generalny" 
		};
	
	public static final String[] dyrektorGeneralnySynonimy= { "Dyrektor Generalny" };

	/**
	 * wyszukuje kierownika w dziale lub w dziale wyzej
	 * 
	 * @param division
	 * @return
	 * @throws EdmException
	 * /
	public static DSDivision findDyrektor(DSDivision division, String[] synonimy) throws EdmException
	{
		DSDivision managerDivision= null;
		out: if (division.getDivisionType().equals("division")) {
			// 1. dla dzialu: jesli jest stanowisko kierownik, to dla niego
			managerDivision= DivisionChief.findManagerChilds(division, synonimy);
			if (managerDivision != null)
				break out;

			// 2. dla dzialu: jesli dzia� wy�ej ma stanowisko kierownik, to
			// dla niego
			managerDivision= DivisionChief.findManagerChilds(division.getParent(), synonimy);
			if (managerDivision != null)
				break out;

		}

		out: if (division.getDivisionType().equals("position")) {
			// 3. dla stanowiska: przechodzi do dzialu i szuka stanowiska
			// "kierownik"
			managerDivision= DivisionChief.findManagerChilds(division.getParent(), synonimy);
			if (managerDivision != null)
				break out;

			// 4. je�eli nie znajdzie kierownika, to na u�ytkownika w tym dziale
			// wy�szym (o ile jest tam jakis uzytkownik)
			if( division.getParent()!=null && division.getParent().getOriginalUsers().length>0)
				managerDivision= division.getParent();
		}
		
		// jesli nie znalezlismy kierownika i mamy pusty dzial, to probujemy wyzej
		if( managerDivision==null && division.getOriginalUsers().length==0 && division.getParent()!=null )
			return findDyrektor(division.getParent(), synonimy);

		return (managerDivision != null) ? managerDivision : division;
	}

	/**
	 * funkcja wyszukuje stanowisko kierownika
	 * /
	private static DSDivision findManagerChilds(DSDivision division, String[] synonimy) throws EdmException
	{
		if( division==null)
			return null;
		
		DSDivision childs[]= division.getChildren("position");

		for (DSDivision ch : childs)
			for (String kierownik : synonimy)
				if (ch.getName().equalsIgnoreCase(kierownik))
					return ch;

		return null;
	}
	*/
}
