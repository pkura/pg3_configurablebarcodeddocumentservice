package pl.compan.docusafe.parametrization.paa;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.LinkValue;
import pl.compan.docusafe.core.dockinds.field.HTMLField;
import pl.compan.docusafe.core.dockinds.field.LinkField;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class PlanZadanDictionary extends DwrDictionaryBase
{

	private static final Logger log = LoggerFactory.getLogger(PlanZadanDictionary.class);
	
	@Override
	public long add(Map<String, FieldData> values) throws EdmException
	{
		Map<String, Object> valuesForSet = new HashMap<String, Object>();
		boolean newDictionaryValueIsNotEmpty = false;
		// Na poczatku sprawdzamy, czy nie jest to przypadek, w ktorym slownik
		// jest caly pusty (nie wpisano zadnej wartosci)
		for (FieldData fieldData : values.values())
		{
			if ((fieldData.getData() != null) && (!fieldData.getData().equals("")))
			{
				newDictionaryValueIsNotEmpty = true;
				break;
			}
		}

		long ret = -1;

		if (!newDictionaryValueIsNotEmpty)
			return ret;
		
		String typ = values.get("TYP_OBIEKTU").getStringData();
		
		String divisionId_komorkaWiodaca = values.get("KOMORKA_WIODACA").getStringData();
		if( divisionId_komorkaWiodaca==null || divisionId_komorkaWiodaca.isEmpty() )
			return ret;
		
		Long divisionId = Long.valueOf(divisionId_komorkaWiodaca);		// zabezpieczenie
		
		
		try
		{
			DSApi.beginTransacionSafely();
			
			OutOfficeDocument document= null;
			String komorkaWiodaca= null;
			Long newDocumentId= null;
		
			// wczytanie guid komorki wiodacej
			Statement stat = null;
			ResultSet result = null;
			try{
				stat = DSApi.context().createStatement();
	
				String select = "select GUID from DS_DIVISION where ID = "+divisionId.toString();
				log.info("select: " + select.toString());
				result = stat.executeQuery(select);
				while (result.next()){
					komorkaWiodaca = result.getString(1);
				}
			}catch (Exception ie){
				log.error(ie.getMessage(), ie);
			}
			finally{
				DSApi.context().closeStatement(stat);
			}
			
			if( komorkaWiodaca==null || komorkaWiodaca.isEmpty() )
				return ret;
			
	
			for(String cn : values.keySet())
			{
				String newCn = cn;
				// jesli pierwszy znak to _, np.: _STATUS_DOKUMENTU
				if(newCn.charAt(0) == '_') {
					newCn = newCn.substring(1);
				}
					
				if( values.get(cn).getData() != null )
					valuesForSet.put(newCn, values.get(cn).getData());
			}
			
			/*
			try
			{
				Long masterDocumentId = null;
				if(values.get("MASTER_DOC") != null && values.get("MASTER_DOC").getData() != null)
				{
					masterDocumentId = Long.parseLong(""+values.get("MASTER_DOC").getData());//values.get("MASTER_DOC_ID").getLongData();
					FieldsManager fmm = Document.find(masterDocumentId).getFieldsManager();
					fmm.initialize();
					//String numerwniosku = fmm.getKey("NUMER_WNIOSKU").toString();
					//ArrayList<Long> contracts = DlApplicationDictionary.getInstance().getContracts(Long.parseLong(numerwniosku));
					//valuesForSet.put(LeasingLogic.NUMER_UMOWY_CN, contracts);
				}
			}
			catch (Exception ie)
			{
				log.error(ie.getMessage(), ie);
			}
			*/
			String summary = typ;//"Zadanie";
			document =  new OutOfficeDocument();
			document.setSummary(summary);
			DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.PAA_PLAN_PRACY_ZAD);
			document.setDocumentKind(documentKind);
			document.setForceArchivePermissions(false);
			document.setCtime(new Date());
			document.setFolder(Folder.getRootFolder());
			
			//
			String osoba = DSApi.context().getPrincipalName();
			String divisionGuid = "rootdivision";
			//
			document.setCreatingUser(osoba);
			document.setAuthor(osoba);
			document.setInternal(true);
			document.setCurrentAssignmentGuid(divisionGuid);
			document.setDivisionGuid(divisionGuid);
			document.setCurrentAssignmentAccepted(Boolean.FALSE);
			document.setAssignedDivision(divisionGuid);
			//document.setSender(new Sender());
			
			document.create();
			newDocumentId = document.getId();
			//String rodzajDokumentu =  getName().replace("DSG_LEASING", "");
			//valuesForSet.put("RODZAJ_DOKUMENTU", Integer.valueOf(rodzajDokumentu));
			documentKind.setOnly(newDocumentId, valuesForSet);
			documentKind.logic().archiveActions(document, DocumentLogic.TYPE_INTERNAL_OFFICE);
			documentKind.logic().documentPermissions(document);
			
			DSApi.context().commit();
		
		
			DSApi.context().session().save(document);
			
			//if( typ.equals("Zadanie kom�rki") )
			//	;
			
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_DIVISION_GUID_PARAM, komorkaWiodaca);
			//map.put(ASSIGN_USER_PARAM, "zagrajek");
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			TaskSnapshot.updateByDocument(document);
			
			ret = newDocumentId;
		
		}catch(Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return ret;
	}
	
	@Override
	public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset) throws EdmException
	{
		List<Map<String, Object>> ret = Lists.newArrayList();
		
		String documentId = "";
		if (!(values.containsKey("id") && !(values.get("id").getData().toString()).equals("")))
			return ret;
			
		documentId = values.get("id").getData().toString();
		
		Statement stat = null;
		ResultSet result = null;
		String dictionaryName = getName();
		boolean contextOpened = true;
		try
		{
			if (!DSApi.isContextOpen())
			{
				DSApi.openAdmin();
				contextOpened = false;
			}
			stat = DSApi.context().createStatement();

			StringBuilder select = new StringBuilder("select OPIS, DATAOD, DATADO, STAN, TYP_OBIEKTU,TYP_ZADANIA,KOMORKA_WIODACA "
					+" from DS_PAA_PLAN_PRACY_ZAD where DOCUMENT_ID = ").append(documentId);
			log.info("select: " + select.toString());
			result = stat.executeQuery(select.toString());
			while (result.next())
			{
				Map<String, Object> row = Maps.newLinkedHashMap();
				String opis = result.getString(1);
				java.sql.Date dataOd = result.getDate(2);
				java.sql.Date dataDo = result.getDate(3);
				String stan = result.getString(4);
				Integer typObiektu = result.getInt(5);
				Integer typZadania = result.getInt(6);
				Long komorkaWiodaca = result.getLong(7);
				row.put("id", documentId);

				row.put(dictionaryName + "_OPIS", opis);
				row.put(dictionaryName + "_DATAOD", dataOd);
				row.put(dictionaryName + "_DATADO", dataDo);
				row.put(dictionaryName + "_STAN", stan);
				row.put(dictionaryName + "_TYP_OBIEKTU", typObiektu);
				row.put(dictionaryName + "_TYP_ZADANIA", typZadania);
				row.put(dictionaryName + "_KOMORKA_WIODACA", komorkaWiodaca);
				ret.add(row);
			}
			
		}
		catch (Exception ie)
		{
			log.error(ie.getMessage(), ie);
		}
		finally
		{
			DSApi.context().closeStatement(stat);
			if (!contextOpened && DSApi.isContextOpen())
				DSApi.close();
		}
		return ret;
	}
	
	@Override
	public Map<String, Object> getValues(String id) throws EdmException
	{
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

		Map<String, Object> fieldsValues = new HashMap<String, Object>();
		try
		{
			Map<String, FieldData> values = new HashMap<String, FieldData>();
			if (id != null)
			{
				FieldData idData = new FieldData();
				idData.setData(id);
				idData.setType(Field.Type.STRING);
				values.put("id", idData);
				result = search(values, 1, 2);
			}

			for (pl.compan.docusafe.core.dockinds.field.Field field : getOldField().getFields())
			{
				// poprawka enumow i dsdivision
				if (field.getCn().equals("TYP_ZADANIA_1") || field.getCn().equals("TYP_OBIEKTU_1") || field.getCn().equals("KOMORKA_WIODACA_1"))
				{
					Map<String, Object> enumVal = new HashMap<String, Object>();
					if (result.size() > 0 && result.get(0).containsKey(getName() + "_"+field.getCn().replace("_1", "")))
					{
						fieldsValues.put(field.getCn(), result.get(0).get(getName() + "_"+field.getCn().replace("_1", "")));
					}
					if (result.size() == 0)
					{
						enumVal.put(getName() + "_"+field.getCn().replace("_1", ""), field.getEnumItemsForDwr(fieldsValues));
						result.add(enumVal);
					}
					else
					{
						result.get(0).put(getName() + "_"+field.getCn().replace("_1", ""), field.getEnumItemsForDwr(fieldsValues));
					}
				}
				else if (field.getCn().equals("DSG_LEASINGG_1") && id != null)
				{
					Map<String, Object> enumVal = new HashMap<String, Object>();

					pl.compan.docusafe.core.dockinds.field.Field linkField  =  (LinkField)field;
					String label = "Przejd�";
					if (result.size() == 0)
					{
						enumVal.put(getName() + "_DSG_LEASINGG", new LinkValue(label, ((LinkField) linkField).getLogicField().createLink(id)));
						result.add(enumVal);
					}
					else
						result.get(0).put(getName() + "_DSG_LEASINGG", new LinkValue(label, ((LinkField) linkField).getLogicField().createLink(id)));
				}
				else if (field.getCn().equals("ATTACH_1") && id != null)
				{
					Map<String, Object> htmlVal = new HashMap<String, Object>();
					HTMLField htmlField = (HTMLField)field;

					if (result.size() == 0)
					{
						htmlVal.put(getName() + "_ATTACH_1", htmlField.getValue(id));
						result.add(htmlVal);
					}
					else
						result.get(0).put(getName()+"_ATTACH", htmlField.getValue(id));
				}
			}
			if (result.size() == 0)
				return new HashMap<String, Object>();
			return result.get(0);
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
			return null;
		}
	}
	

}
