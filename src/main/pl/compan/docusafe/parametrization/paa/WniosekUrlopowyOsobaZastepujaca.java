package pl.compan.docusafe.parametrization.paa;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Map;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


public class WniosekUrlopowyOsobaZastepujaca  implements AssignmentHandler{
	
	private String poleZastepstwa; //to jest pole typu private ale mimo to pojawi si� tu warto�� z zewn�trz.
	// JBPM wstawi tu warto�� podan� w XML:
	// <field name="poleNadawcy"><string value="nazwa pola Nadawca"/></field>
	// czyli wpisze warto�� "nazwa pola Nadawca" do pola "poleNadawcy" powy�ej.
	// Nale�y to traktowa� jako dodatkowy argument wywo�ania asygnatora (tej metody w tej klasie) 

	private static final Logger log = LoggerFactory.getLogger(WniosekUrlopowyOsobaZastepujaca.class);
	
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
		
		log.info("WniosekUrlopowyOsobaZastepujaca");
		
		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		
		String asd = doc.getFieldsManager().getEnumItem("STATUSDOKUMENTU").getRefValue();
		String data = Calendar.getInstance().getTime().toString();
		String rodzajDokumentu = doc.getDocumentKind().getCn();
		log.info("Dokument  id = "+docId+"Rodzaj = " +rodzajDokumentu+ " czas zajscia procesu "+data);
		
		String useridstl=fm.getStringKey(poleZastepstwa);
		String[] ids;
		if (useridstl.startsWith("[")){
			useridstl= useridstl.substring(1, useridstl.length()-1);
			ids  = useridstl.split(",");
		}else{
			ids = new String[1];
			ids[0] = useridstl;
		}
		for(String id : ids){
			
			
			 PreparedStatement pst = null;
		   	   
		   	   String Userid ;
		          pst = DSApi.context().prepareStatement("select title from DS_PAA_WNIOSEK_ZAST where id = ?");
		          pst.setLong(1, Long.parseLong(id) );
		          ResultSet rs = pst.executeQuery();
				  
				   if (rs.next()){
					   Userid  =  rs.getString(1);
				    }
				   pst.close();
				
				DSUser user = DSUser.findById(Long.parseLong(id));
				assignable.addCandidateUser(user.getName());
				
				AssignmentHistoryEntry entry = new AssignmentHistoryEntry();
				entry.setSourceUser(DSApi.context().getDSUser().getName());
				String dzial =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(DSApi.context().getDSUser().getName());
				entry.setSourceGuid(dzial);
				entry.setTargetUser(user.getName());


				String dzialNad =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(DSApi.context().getDSUser().getName());
				entry.setSourceGuid(dzialNad);
				String dzialOdb =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(user.getName());
				entry.setTargetGuid(dzialOdb);
				
				String statusDokumentu =  doc.getFieldsManager().getEnumItem("STATUSDOKUMENTU").getTitle();
				entry.setObjective(statusDokumentu);
				entry.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
				doc.addAssignmentHistoryEntry(entry);
			
			
		}
		
		
		log.info("pobra��m user id = " +useridstl);
		long userId = 0;
		try{
			try{
				userId = Long.parseLong(useridstl);											// pierwsza proba
			} catch (NumberFormatException nfe){
				userId = Long.parseLong(useridstl.substring(1, useridstl.length()-1));		// druga proba
			}
		} catch (NumberFormatException nfe){
			log.info("podane ID u�ytkownika (" +useridstl+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
			throw new IllegalStateException("podane ID u�ytkownika (" +useridstl+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
		}
		
		  PreparedStatement pst = null;
   	   
   	   String id = "";
          pst = DSApi.context().prepareStatement("select title from DS_PAA_WNIOSEK_ZAST where id = ?");
          pst.setLong(1, userId );
          ResultSet rs = pst.executeQuery();
		  
		   if (rs.next()){
		     id  =  rs.getString(1);
		    }
		   pst.close();
		
		/*Person person =Person.find(userId);
		String personfirstname = person.getFirstname();
		String personLastname = person.getLastname();
		DSUser user = DSUser.findByFirstnameLastname(personfirstname, personLastname);
		//dekretowanie
		 * 
*/		
		DSUser user = DSUser.findById(Long.parseLong(id));
		assignable.addCandidateUser(user.getName());
		
		log.info("WniosekUrlopowyOsobaZastepujaca dekretuje na " +user.getName());
		
		// dodac recznie do historii (w WSSK robilem to tak:) 
		// Util_AHE.addToHistory(openExecution, doc, asygnowanyUzytkownik, null, null, log);
	}
}
