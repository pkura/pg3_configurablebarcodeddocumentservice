package pl.compan.docusafe.parametrization.paa;

import java.util.Calendar;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;

/** Zakonczenie pracy z pisme w trakcie procesu
  */
@SuppressWarnings("serial")
public class KoniecPracyZpismem implements ExternalActivityBehaviour  {
	Logger log = LoggerFactory.getLogger(KoniecPracyZpismem.class);
	
	private String nazwaZmiennej;
	
	@Override
	public void execute(ActivityExecution execution) throws Exception {

		
		Long docId = Long.valueOf( execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		
	
		String data = Calendar.getInstance().getTime().toLocaleString();
	
		String nazawadokumentu = doc.getDocumentKind().getMainFieldCn();
		log.info("Dokument  id = "+docId+" Rodzaj = " +nazawadokumentu+ " czas zajscia procesu "+data);
		DSUser user = DSApi.context().getDSUser();
		doc.addWorkHistoryEntry(Audit.create(DSApi.context().getPrincipalName(),user.getName(),
                " Prace nad dokumentem  zosta�y zako�czone przez   : "+user.asLastnameFirstname() + " w dniu : " + data));
		
		
		if (execution.getActivity().getDefaultOutgoingTransition() != null) // tego nie kasowac !!!
			execution.takeDefaultTransition();
	}
	@Override
	public void signal(ActivityExecution execution, String signalName, Map<String, ?> arg2) throws Exception {
		if (signalName != null)
			execution.take(signalName);
		else 
			execution.takeDefaultTransition();
	}

}
