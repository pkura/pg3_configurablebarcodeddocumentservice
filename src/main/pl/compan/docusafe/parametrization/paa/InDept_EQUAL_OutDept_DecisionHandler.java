package pl.compan.docusafe.parametrization.paa;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** ocenia, czy nadawca dokumentu i odbiorca dokumentu sa w tym samym departamencie */
@SuppressWarnings("serial")
public class InDept_EQUAL_OutDept_DecisionHandler implements DecisionHandler {
	private static final Logger log = LoggerFactory.getLogger(InDept_EQUAL_OutDept_DecisionHandler.class);
	
	
	public String decide(OpenExecution openExecution) {
		
		log.info("InDept_EQUAL_OutDept_DecisionHandler");
		
		
		
		try {
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			
			
			String data = Calendar.getInstance().getTime().toString();
			String rodzajDokumentu = doc.getDocumentKind().getCn();
			log.info("Dokument  id = "+docId+"Rodzaj = " +rodzajDokumentu+ " czas zajscia procesu "+data);
			
			// pobieranie  nadawcy 
			long useridNadawca=fm.getLongKey("SENDER_HERE");
			Person person =Person.find(useridNadawca);
			String personfirstname = person.getFirstname();
			String personLastname = person.getLastname();
			DSUser nadawca = DSUser.findByFirstnameLastname(personfirstname, personLastname);
			
			//pobieranie odbiorcy 

			String useridOdbiorca =fm.getStringKey("RECIPIENT");
			long userId = 0;
			try{
				// pierwsza proba
				userId = Long.parseLong(useridOdbiorca);
			} catch (NumberFormatException nfe){
				// druga proba
				userId = Long.parseLong(useridOdbiorca.substring(1, useridOdbiorca.length()-1));
			}
			DSUser odbiorca = DSUser.findById(userId);
			
			//weryfikacja czy s� w tym samym departamencie
			List<String> nazwadivNadawcy= new ArrayList<String>();
			List<String> nazwadivOdbiorcy= new ArrayList<String>();	
			
		   // pobieram taki division kt�ry ma parenta kt�rego parentem jest root 
			for( DSDivision current : nadawca.getOriginalDivisions() )
				while(current.isGrandsonInTree()){
					if (current.isDivision() && current.getParent().getParent().getGuid().contains(DSDivision.ROOT_GUID)) {
						nazwadivNadawcy.add( current.getName() );
						break;
					}else
						current= current.getParent();
				}
			
			// to samo odbiorca 
			for( DSDivision current : odbiorca.getOriginalDivisions() )
				while(current.isGrandsonInTree()){
					if (current.isDivision() && current.getParent().getParent().getGuid().contains(DSDivision.ROOT_GUID)) {
						nazwadivOdbiorcy.add( current.getName() );
						break;
					}else
						current= current.getParent();
				}
			
			//sprawdzam czy nadawca i odbiorca s� w ty msamym departamencie 
			for (String x :nazwadivNadawcy)
				for (String y :nazwadivNadawcy)
				{
					if (x.equals(y))
					return "deptsEqual";
					
				}
			return "wskazanaInnaOsoba";

			//decision = "deptsEqual";
			
			//mozliwe decyzje:
			
			//deptsEqual			- ten sam departament
			//wskazanaInnaOsoba		- rozne departamenty

		} catch (Exception e) {
			log.error("", e);
			return "deptsEqual";
		}
	}
}
