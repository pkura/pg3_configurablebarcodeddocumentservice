package pl.compan.docusafe.parametrization.paa;

import com.google.common.collect.Maps;




import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.DwrDocumentHelper;
import pl.compan.docusafe.webwork.event.ActionEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

import java.util.List;

import java.util.Map;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.*;

@SuppressWarnings("serial")
public class PaaWniosekUrlopowyLogic extends AbstractDocumentLogic
{
	public final static String _USER = "USER";
	public final static String _DIVISION = "DSDIVISION";
	public final static String _stampDate = "stampDate";
	public final static String _znak_pisma = "znak_pisma";
	public final static String _SENDER_HERE = "SENDER_HERE";
	public final static String _POSITION = "POSITION";
	public final static String _NUMERPISMA = "NUMERPISMA";
	public final static String _AVAILABLE_DAYS ="AVAILABLE_DAYS";
	public final static String _PELENADRES = "PELENADRES";
	public static final String  KIND_EXECUTE = "Do Realizacji";
	public static final String _multiOdbiorca = "M_DICT_VALUES";
	public static final String _pelenAdres = "PELENADRES";
	public static final String _RECIPIENT = "RECIPIENT";
	public static final String _REPILES_FOR = "REPILES_FOR";
	public static final String _RODZAJ_PRZESYLKI = "RODZAJ_PRZESYLKI";	
	public static final String _SPOSOB_DOSTARCZENIA = "SPOSOB_DOSTARCZENIA";
	public static final String _idMultiPerson = "ID";	
	public static final String _kosztJednostkowy = "KWOTA";	
	public static final String _rodzajPrzesylkiWagaKrajowa = "RODZAJ_PRZESYΘI_TYPE_WEIGHT_COUNTRY";	
	public static final String _rodzajPrzesylkiWagaSwiat = "RODZAJ_PRZESYΘI_TYPE_WEIGHT_COUNTRY";
	public static final String _GABARYT = "GABARYT";
	public static final String _dataDokumentu = "DOC_DATE";
	public static final String _odpowiedzNa = "REPILES_FOR";
	public static final String _biuroDyrGeneralnego = "WBDG";
	public static final String _KoszListu = "KOSZT_PRZESYLKI";
	
	public  boolean WalidacjaDatWniosku ;

	protected static DwrDocumentHelper dwrDocumentHelper = new DwrDocumentHelper();
	public boolean isWalidacjaDatWniosku()
	{
		return WalidacjaDatWniosku;
	}


	private static PaaWniosekUrlopowyLogic instance;
	protected static Logger log = LoggerFactory.getLogger(PaaWniosekUrlopowyLogic.class);

	 
	public static PaaWniosekUrlopowyLogic getInstance()
	{
		if (instance == null)
			instance = new PaaWniosekUrlopowyLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	
	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
	{
		Field msg = null;
		
		
			try {
				
				if (values.get("DWR_KIND") != null
						&& values.get("DWR_KIND").getEnumValuesData().getSelectedId().equals("20")
						|| values.get("DWR_KIND") != null
						&& values.get("DWR_KIND").getEnumValuesData().getSelectedId().equals("25")) {

					fm.getField("ZASTEPSTWO").setHidden(true);
					fm.getField("ZASTEPSTWO").setRequired(false);
				} else {
					fm.getField("ZASTEPSTWO").setHidden(false);
					fm.getField("ZASTEPSTWO").setRequired(true);
				}
			} catch (EdmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// dane do typu urlopu odbi鏎 godzin

			try {

				if (values.get("DWR_KIND") != null
						&& values.get("DWR_KIND").getEnumValuesData().getSelectedId().equals("40")) {

					fm.getField("GODZ_FROM").setHidden(false);
					fm.getField("GODZ_FROM").setRequired(true);
					fm.getField("GODZ_TO").setHidden(false);
					fm.getField("GODZ_TO").setRequired(true);
				} else {
					fm.getField("GODZ_FROM").setHidden(true);
					fm.getField("GODZ_FROM").setRequired(false);
					fm.getField("GODZ_TO").setHidden(true);
					fm.getField("GODZ_TO").setRequired(false);
				}

			} catch (EdmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		

		return msg;
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		
		log.info("type: {}", type);
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("TYPE", type);

		Long userDivision = (long) 1;


		fm.getField("ZASTEPSTWO").setHidden(false);
		fm.getField("ZASTEPSTWO").setRequired(true);

		DSUser user = DSApi.context().getDSUser();

		DSDivision[] div = user.getDivisions();

		DSDivision[] divisions = user.getOriginalDivisions();

		for (int i = 0; i < divisions.length; i++)
		{
			if (divisions[i].isPosition())
			{
				if (divisions[i].getDescription() != null
						&& (divisions[i].getDescription().equals("PREZES") || divisions[i].getDescription().equals("VCEPREZES")))
				{
					userDivision = divisions[i].getId();
				}
				DSDivision parent = divisions[i].getParent();
				if (parent != null)
				{
					userDivision = parent.getId();
					break;
				} else
				{
					userDivision = divisions[i].getId();
				}
			} else if (divisions[i].isDivision())
			{
				userDivision = divisions[i].getId();
				break;
			}
		}

		String a = Docusafe.getAdditionProperty("PAA.naczelnik");
		for (String naczelnik : a.split(","))
		{
			if (user.getName().equals(naczelnik))
			{
				fm.getField("ZASTEPSTWO").setHidden(true);
				fm.getField("ZASTEPSTWO").setRequired(false);
				fm.getField("GODZ_FROM").setHidden(true);
				fm.getField("GODZ_FROM").setRequired(false);
				fm.getField("GODZ_TO").setHidden(true);
				fm.getField("GODZ_TO").setRequired(false);

				//	fm.getEnumItem("KIND").setTmp("20");
				values.put("KIND", "25");
				//	fm.getField("KIND").setDefaultValue("20");
				fm.getField("KIND").setReadOnly(false);
				break;

			} else
			{
				fm.getField("ZASTEPSTWO").setHidden(false);
				fm.getField("ZASTEPSTWO").setRequired(true);
				fm.getField("GODZ_FROM").setHidden(true);
				fm.getField("GODZ_FROM").setRequired(false);
				fm.getField("GODZ_TO").setHidden(true);
				fm.getField("GODZ_TO").setRequired(false);
				fm.getField("KIND").setReadOnly(false);
				values.put("KIND", "10");
			}
		}

		values.put(_USER, DSApi.context().getDSUser().getId());
		values.put(_DIVISION, userDivision);


		// wpisanie nale積ych dni urlopu

		int liczbaDni = 0;

		UserImpl userWnioskujacy = (UserImpl) DSUser.findByUsername(DSApi.context().getDSUser().getName());

		EmployeeCard empCard;
		try
		{
			empCard = getEmployeeCard(userWnioskujacy);

			if (empCard == null)
				throw new IllegalStateException("Brak karty pracownika! Nie mo積a przypisa� urlopu!");

			liczbaDni = AbsenceFactory.getAvailableDaysForCurrentYear(empCard);

		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		values.put(_AVAILABLE_DAYS, liczbaDni);

		fm.reloadValues(values);
			
	}
	
	
	
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		as.useFolderResolver(document);
		as.useAvailableProviders(document);
		log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
	}

	@Override
	public String onStartManualAssignment(OfficeDocument doc, String fromUsername, String toUsername)
	{
		log.info("--- PaaWniosekUrlopowyLogic : onStartManualAssignment  dekretacja reczna dokument闚 :: dokument ID = "+doc.getId()+" : pomiedzy nadawc� -->"+fromUsername +" a odbiorc� ->"+toUsername );
		log.info("--- PaaWniosekUrlopowyLogic : Sprawdzam Czy s� w tym samym departamencie ");
		if(!NormalLogic.inDepIsOutDep(fromUsername, toUsername)){
				log.info("--- PaaWniosekUrlopowyLogic : Wynik ->  Nie s� w tym samym  pobieram dyrektora departamentu  ");
				toUsername = NormalLogic.getDyrektorDepartamentu(toUsername ,doc);
				/*if (toUsername.contains(_biuroDyrGeneralnego))
					try {
						toUsername = NormalLogic.getDyrektorGeneralny();
					} catch (Exception e) {
						log.error("", e);
					}*/
				log.info("--- PaaWniosekUrlopowyLogic : Wynik -> pobra貫m dyrektora -> " + toUsername);
				//if(toUsername.contains(staryOdbiorca)){
				//	toUsername =fromUsername;
			//	}
				
			}
	
		log.info("--- PaaWniosekUrlopowyLogic : WYnik ->  ostateczny rezultat  dekretacja na -> "+ toUsername);
		return toUsername;
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- PaaWniosekUrlopowyLogic : START PROCESS !!! ---- {}", event);
		try {

			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM,event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind()
					.getDockindInfo()
					.getProcessesDeclarations()
					.onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

	}
	 
	@Override
	public void validate(Map<String, Object> values, DocumentKind kind,Long documentId) throws EdmException {
		
		System.out.println(kind.getCn());
		System.out.println(values);

		

	if ( values.get("FROM") == null || values.get("TO") == null) {
			throw new EdmException("Daty we wniosku osoby wnioskujacej sa nieprawid這we");
		} else { 
			if (values.get("FROM") != null && values.get("TO") != null){
				List listaDatZast瘼stw = new ArrayList<Date>();
		
				Date UrlopOd = (Date) values.get("FROM");
				Date UrlopDo = (Date) values.get("TO");
				if (UrlopOd.after(UrlopDo)) {
					throw new EdmException(
							"Daty we wniosku osoby wnioskujacej sa nieprawid這we");
				} else {
					Map<String, FieldData> wartosci = (Map<String, FieldData>) values.get(_multiOdbiorca);
					if (wartosci != null) {
						for (String key : wartosci.keySet()) {
								Map<String, FieldData> wpis = (Map<String, FieldData>) wartosci.get(key);
								FieldData fd =  wpis.get("ZAST_PERIOD_FROM");
								Date ZastOd = fd.getDateData();
								listaDatZast瘼stw.add(ZastOd);
								FieldData fd2 = wpis.get("ZAST_PERIOD_TO");
								Date ZastDo = fd2.getDateData();
								listaDatZast瘼stw.add(ZastDo);
						}

						// sortowanie po dacie dokumentu
						if (!listaDatZast瘼stw.isEmpty()) {
							Collections.sort(listaDatZast瘼stw,
									new Comparator<Date>() {
										public int compare(Date listaDatZast瘼stw1,Date listaDatZast瘼stw2) {
											return (listaDatZast瘼stw1.before(listaDatZast瘼stw2)) ? -1: 1;
										}
									});

							Date dataZastPocz = (Date) listaDatZast瘼stw.get(0);
							Date dataZastKonc = (Date) listaDatZast瘼stw
									.get(listaDatZast瘼stw.size() - 1);

							if (!UrlopOd.equals(dataZastPocz)
									&& !UrlopDo.equals(dataZastKonc)) {
								throw new EdmException("Daty we wniosku osoby/os鏏 zast瘼uj鉍ych s� nieprawid這we");
							}
						}
					}
				}
			}
		}

	}

	@Override
	public void onStartProcess(OfficeDocument document)
	{
	
		try {
			Map<String, Object> map= Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, document.getAuthor());
			map.put(ASSIGN_DIVISION_GUID_PARAM, document.getDivisionGuid());
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	private EmployeeCard getEmployeeCard(UserImpl userImpl) throws Exception
	{
		return (EmployeeCard) DSApi.context().session().createQuery(
				"from " + EmployeeCard.class.getName() + " e where e.user = ?")
				.setParameter(0, userImpl)
				.setMaxResults(1)
				.uniqueResult();
	}
	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret= new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	@Override
	public void onRejectedListener(Document doc) throws EdmException
	{
	}

	@Override
	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException
	{
	}
	
	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException,
			EdmException
	{ 
		
		
	}
	
}

	