package pl.compan.docusafe.parametrization.paa;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ZbiorZastepcow implements ExternalActivityBehaviour {

	private int iloscOsobZastepujacych = 0 ;
	private List<String> usernames = new ArrayList();
	protected void executionInsideContext(ActivityExecution execution) throws Exception {}


	//private String poleZastepstwa; //to jest pole typu private ale mimo to pojawi si� tu warto�� z zewn�trz.
	// JBPM wstawi tu warto�� podan� w XML:
	// <field name="poleNadawcy"><string value="nazwa pola Nadawca"/></field>
	// czyli wpisze warto�� "nazwa pola Nadawca" do pola "poleNadawcy" powy�ej.
	// Nale�y to traktowa� jako dodatkowy argument wywo�ania asygnatora (tej metody w tej klasie) 
	

	private static final Logger log = LoggerFactory.getLogger(ZbiorZastepcow.class);
	
	
	private void tablicauserow(OpenExecution openExecution) throws Exception
	{

		log.info("ZbiorZastepcow");
		Long docId= Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc= OfficeDocument.find(docId);
		FieldsManager fm= doc.getFieldsManager();
	
		String data= Calendar.getInstance().getTime().toString();
		String rodzajDokumentu= doc.getDocumentKind().getCn();
		log.info("Dokument  id = " + docId + "Rodzaj =" + rodzajDokumentu + " czas zajscia procesu " + data);

		String useridstl= fm.getStringKey("ZASTEPSTWO");
		String[] ids;
		if (useridstl.startsWith("[")) {
			useridstl= useridstl.substring(1, useridstl.length() - 1);
			ids= useridstl.split(",");
		} else {
			ids= new String[1];
			ids[0]= useridstl;
		}
		usernames.clear();
		iloscOsobZastepujacych= 0;
		for (String id : ids) {

			PreparedStatement pst= null;

			String Userid;
			pst= DSApi.context().prepareStatement("select title from DS_PAA_WNIOSEK_ZAST where id = ?");
			pst.setLong(1, Long.parseLong(id.trim()));
			ResultSet rs= pst.executeQuery();

			if (rs.next()) {
				Userid= rs.getString(1);
				DSUser user= DSUser.findById(Long.parseLong(Userid));
				usernames.add(user.getName());
				iloscOsobZastepujacych++;
				
				AssignmentHistoryEntry entry = new AssignmentHistoryEntry();
				entry.setSourceUser(DSApi.context().getDSUser().getName());
				entry.setTargetUser(user.getName());
				String dzialNad =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(DSApi.context().getDSUser().getName());
				entry.setSourceGuid(dzialNad);
				String dzialOdb =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(user.getName());
				entry.setTargetGuid(dzialOdb);
				String statusDokumentu =  doc.getFieldsManager().getEnumItem("STATUSDOKUMENTU").getTitle();
				entry.setObjective(statusDokumentu);
				entry.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
				doc.addAssignmentHistoryEntry(entry);
			
			}
			pst.close();

		}

	}
	
		


	@Override
	public void execute(ActivityExecution execution) throws Exception {

		tablicauserow(execution);
		
			
		// liczba subtaskow na potrzeby forka
		execution.setVariable("count", iloscOsobZastepujacych);
		
		// id osob przynaleznych do grupy
		execution.setVariable("usernames", usernames); //wynik to tablica albo lista login�w
		
		execution.setVariable("odmowa", 0); //p�ki jest 0 to nikt nie odm�wi�
		
		if (execution.getActivity().getDefaultOutgoingTransition() != null)
			execution.takeDefaultTransition();
	}
	
	
	
	@Override
	public void signal(ActivityExecution execution, String signalName, Map<String, ?> arg2) throws Exception {
		if (signalName != null)
			execution.take(signalName);
		else 
			execution.takeDefaultTransition();
	}


}