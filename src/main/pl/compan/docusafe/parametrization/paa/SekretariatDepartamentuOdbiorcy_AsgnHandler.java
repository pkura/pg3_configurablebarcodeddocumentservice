package pl.compan.docusafe.parametrization.paa;

import java.util.Calendar;
import java.util.Date;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** Asygnuje dokument na Sekretariat Departamentu osoby wskazanej jako odbiorca */
@SuppressWarnings("serial")
public class SekretariatDepartamentuOdbiorcy_AsgnHandler implements AssignmentHandler {
	private final static Logger log= LoggerFactory.getLogger(SekretariatDepartamentuOdbiorcy_AsgnHandler.class);
	
	private String poleOdbiorcy; //to jest pole typu private ale mimo to pojawi si� tu warto�� z zewn�trz.
	
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
		
		log.info("SekretariatDepartamentuOdbiorcy_AsgnHandler");
		
		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		
		String data = Calendar.getInstance().getTime().toString();
		String rodzajDokumentu = doc.getDocumentKind().getCn();
		log.info("Dokument  id = "+docId+"Rodzaj = " +rodzajDokumentu+ " czas zajscia procesu "+data);
		
		String useridstl=fm.getStringKey(poleOdbiorcy);
		log.info("pobra�em user id = " +useridstl);
		long userId = 0;
		try{
			userId = Long.parseLong(useridstl);
		} catch (NumberFormatException nfe){
			log.info("podane ID u�ytkownika (" +useridstl+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
			throw new IllegalStateException("podane ID u�ytkownika (" +useridstl+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
		}
		
		Person person =Person.find(userId);
		String personfirstname = person.getFirstname();
		String personLastname = person.getLastname();
		DSUser user = DSUser.findByFirstnameLastname(personfirstname, personLastname);
		String sekretariat ="";
		String   dyrektor ="";
		String dekretacja="";
		//jesli adresatem jest sobiecka to do niej trafia 
		if (user.getName().equals(Docusafe.getAdditionProperty("PAA.GlownySekretariat"))) 
		{
			assignable.addCandidateUser(user.getName());
			sekretariat = user.getName();
			dekretacja = user.getName();
		} else {
		//pisma przyhcodz�ce 
	//	if (doc.getDocumentKind().getCn().equals("paadocin"))
		
		log.info("Wyszukuje dyrektora departamentu u�ytkownika "+user.getName());
		   dyrektor = pl.compan.docusafe.parametrization.paa.NormalLogic.getDyrektorDepartamentu(user.getName(), doc);
			if(dyrektor.equals(user.getName())){
				log.info("Nie znalaz�em dyrektora departamentu  wyszukuj� po dziale u�ytkownika "+user.getName()+"- pracownika sekretariatu ");
				
				 sekretariat = pl.compan.docusafe.parametrization.paa.NormalLogic.getUserPelniocyRoleSekretariatu(user.getName());
				 if(sekretariat.equals(user.getName())){
						log.info("Nie znalaz�em  pracownika sekretariatu po dziale u�ytkownika "+user.getName()+" Dekretuje na u�ytkownika");
						assignable.addCandidateUser(user.getName());
						dekretacja=user.getName();
				 }else {
					 assignable.addCandidateUser(sekretariat);
					 dekretacja=sekretariat;
				 }
				 
				
			}else {// jesli pracownik jest pod dyr generalnym 
				 /*if (dyrektor.equals("WBDG")){
					 log.info("Znalaz�em dyrektora "+dyrektor+ "pobieram pracownika sekretariatu  bo jes w BDG");
					 dyrektor = pl.compan.docusafe.parametrization.paa.NormalLogic.getDyrektorGeneralny();
					 sekretariat = pl.compan.docusafe.parametrization.paa.NormalLogic.getUserPelniocyRoleSekretariatu(dyrektor);
					 log.info("Znalaz�em pracownika "+sekretariat+ "dekretuje na niego -> "+sekretariat);
					 assignable.addCandidateUser(sekretariat);
					 dekretacja=sekretariat;
				 } else {*/
					 // prac nie jest pod dyr generalnym 
				log.info("Znalaz�em dyrektora "+dyrektor+ " Pobieram Pracownika sekretariatu po dyrektorze");
				 sekretariat = pl.compan.docusafe.parametrization.paa.NormalLogic.getUserPelniocyRoleSekretariatu(dyrektor);
				 
				 if(sekretariat.equals(dyrektor)){
						log.info("Nie znalaz�em  pracownika sekretariatu po Dyrektorze "+user.getName()+" Dekretuje na u�ytkownika"+user.getName());
						assignable.addCandidateUser(user.getName());
						 dekretacja=user.getName();
				 }else {
					 log.info(" dekretuj� na Sekretariat " +sekretariat + "oraz na u�ytkownika " +user.getName());
						assignable.addCandidateUser(sekretariat);
						dekretacja = sekretariat;
						assignable.addCandidateUser(user.getName());
						
				 } 
				}
			}
			
			AssignmentHistoryEntry entry = new AssignmentHistoryEntry();
			entry.setSourceUser(DSApi.context().getDSUser().getName());
			entry.setTargetUser(dyrektor);
			entry.setTargetUser(sekretariat);
			String dzialNad =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(DSApi.context().getDSUser().getName());
			entry.setSourceGuid(dzialNad);
			String dzialOdb =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(sekretariat);
			entry.setTargetGuid(dzialOdb);
			String statusDokumentu =  doc.getFieldsManager().getEnumItem("STATUSDOKUMENTU").getTitle();
			entry.setObjective(statusDokumentu);
			
			entry.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
			doc.addAssignmentHistoryEntry(entry);
			
			Date date = (Date) Calendar.getInstance().getTime();
			DateUtils data2 = new DateUtils();
	    	String dataa = data2.formatCommonDateTime(date);
			doc.addWorkHistoryEntry(Audit.create(DSApi.context().getPrincipalName(),DSApi.context().getPrincipalName(),
	                "Dokument zosta� zdekretowany na  : "+dekretacja+ " w dniu : " +dataa));
		
		/*	//wniosek urlopowy 
		else 
		
		
		
		
		
		
		
		log.info("Znalaz�em dyrektora "+dyrektor+ " Pobieram Pracownika sekretariatu po dyrektorze");
		 sekretariat = pl.compan.docusafe.parametrization.paa.NormalLogic.getUserPelniocyRoleSekretariatu(dyrektor);
		
		log.info("sprawdzam czy znalaz�em pracownika sekretariatu  po dyrektorze");
		if (sekretariat.contains(dyrektor)){
			log.info("NIE znalaz�em pracownika Sekretariatu  - wyszukuj� po dziale u�ytkownika "+user.getName()+"- pracownika sekretariatu ");
			 sekretariat = pl.compan.docusafe.parametrization.paa.NormalLogic.getUserPelniocyRoleSekretariatu(user.getName());
			
			 if (sekretariat.contains(user.getName())){
				 log.info("Nieznalaz�em pracownika sekretariatu po dizale u�ytkownika "+user.getName()+" dekretuj� na Dyrektora "+dyrektor);
				 assignable.addCandidateUser(dyrektor);
			 }
		}else {
			log.info(" dekretuj� na Sekretariat" +sekretariat + "oraz na u�ytkownika " +user.getName());
			assignable.addCandidateUser(sekretariat);
			assignable.addCandidateUser(user.getName());
		}
		
		*/
		
		 
		
		//dekretacja na  u�ytkownika 
		//assignable.addCandidateUser(asygnowanyUzytkownik);
		
/*		// TODO 
		//dekretacja na dzia� u�ytkownika w kt�rym si� znajduje  zamiast sekretariatu 
		//Dzia� nadrz�dny dla u�ytkownika  
		//zak�adam �e u�ytkownik jest tylko w jednym wydziale
		String guidUzytkownika= "";
		for (DSDivision userDivision : user.getOriginalDivisions())
			if (userDivision.getParent() != null && userDivision.getParent().getGuid() != null) {
				// jezeli jest na stanowisku to na parenta stanowiska
				if (userDivision.isPosition()) {
					guidUzytkownika= userDivision.getParent().getGuid();
				}// w przeciwnym wypadku na dzia� u�ytkownika
				else {
					guidUzytkownika= userDivision.getGuid();
				}
				log.info("SekretariatDepartamentuOdbiorcy_AsgnHandler dekretuje na GUID U�YTKOWNIKA NAZWA = "
						+ userDivision.getParent().getName() + "GUID = " + guidUzytkownika);
				break;
			} else {
				assignable.addCandidateUser(asygnowanyUzytkownik);
				log.info("SekretariatDepartamentuOdbiorcy_AsgnHandler dekretuje na " + asygnowanyUzytkownik);

			}
			
				
		assignable.addCandidateGroup(guidUzytkownika); //podac guid, np. guidAsygnowanegoDzialu = "agdajkdgadgajksgdak"
		*/
		
		// napisac

		//dekretowanie
	
		//albo

		// dodac recznie do historii (w WSSK robilem to tak:) 
		// Util_AHE.addToHistory(openExecution, doc, asygnowanyUzytkownik, null, null, log);
	}
}
