package pl.compan.docusafe.parametrization.paa;


import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import com.google.common.base.Preconditions;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


public class ChiefDecreateAsgnHandler  implements AssignmentHandler {

	private static final long serialVersionUID = 1L;
	
	protected String userRodzaj;
	
	private static final Logger log = LoggerFactory.getLogger(ChiefDecreateAsgnHandler.class);

	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
		
		log.info("ChiefDecreateAsgnHandler dla  userRodzaj="+userRodzaj);
	    
		/*boolean isOpened = true;
		if (!DSApi.isContextOpen()) {
			isOpened = false;
			DSApi.openAdmin();
		}*/
		
		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		
	
		String data = Calendar.getInstance().getTime().toString();
		String rodzajDokumentu = doc.getDocumentKind().getCn();
		log.info("Dokument  id = "+docId+"Rodzaj = " +rodzajDokumentu+ " czas zajscia procesu "+data);
		
		
		
		String useridstl=fm.getStringKey("USER");
		log.info("pobra��m user id = " +useridstl);
		long userId = 0;
		try{
			userId = Long.parseLong(useridstl);
		} catch (NumberFormatException nfe){
			log.info("podane ID u�ytkownika (" +useridstl+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
			throw new IllegalStateException("podane ID u�ytkownika (" +useridstl+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
		}

		
		DSUser nadawca = DSUser.findById(userId);
		// jezeli uzytkownik jest w biurze dyrektora generalnego to 
	/*	if (nadawca.inDivisionByGuid("BCBD4A1A-2FB9-4851-BA96-01EEB27C17CD"))
			userRodzaj = "dyr_generalny";*/
		/*OfficeDocument doc = Jbpm4Utils.getDocument(openExecution);
		Long docId= doc.getId();*/
		//Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		//OfficeDocument doc = OfficeDocument.find(docId);
		
		try {
			// String divisionGuid = doc.getDivisionGuid();
			// DSDivision division= DSDivision.find(divisionGuid);
			String userDocelowy= null;

			List<DSDivision> divisions= null;

			if (userRodzaj.equals("naczelnik")) {
				// divisions= DSDivision.findByDescription("NAC");
				nadawca.getDivisionsAsList();

				for (DSDivision divUsera : nadawca.getDivisionsAsList()) {
					if (divUsera.getParent() != null && divUsera.getParent().getDescription() != null
							&& divUsera.getChildren() != null && divUsera.getChildren("position") != null) {
						DSDivision[] div2= divUsera.getChildren("position");
						for (DSDivision pos : div2) {
							if (pos.getDescription() != null && pos.getDescription().equals("NAC")) {
								userDocelowy= pos.getUsers()[0].getName();
								break;
							}
						}
						if (userDocelowy==null && divUsera.getParent().getDescription().equals("DEP")) {
							DSDivision tempDiv= divUsera.getParent();
							DSDivision[] div= tempDiv.getChildren("position");
							for (DSDivision pos : div) {
								if (pos.getDescription() != null && pos.getDescription().equals("NAC")) {
									userDocelowy= pos.getUsers()[0].getName();
									break;
								}
							}
						}
					}

					if (userDocelowy == null && divUsera.getDescription() != null
							&& divUsera.getDescription().equals("NAC")) {
						// jesli stanowisko naczelnik, to wez osobe naczelnika
						userDocelowy= divUsera.getUsers()[0].getName();
						break;
					}

				}
				boolean UzytkownikGP= (Boolean) openExecution.getVariable("UzytkownikGP");
				// nieznaleziono naczelnika
				if (userDocelowy == null && UzytkownikGP) {
					//  dyrektora GP
					userDocelowy= pl.compan.docusafe.parametrization.paa.NormalLogic.getDyrektorDepartamentu(nadawca.getName(), doc);
				}
			
			}else if(userRodzaj.equals("dyr_departamentu")){
				userDocelowy = pl.compan.docusafe.parametrization.paa.NormalLogic.getDyrektorDepartamentu(nadawca.getName(), doc);
				 // jezeli nie znaleziono dyrektora departamentu  a uzytkownik jest w biurze dyr generalnego pobiez dyrektora generalnego 
				if (userDocelowy.equals("wlodarski"))
					 userDocelowy =pl.compan.docusafe.parametrization.paa.NormalLogic.getDyrektorGeneralny();
			}else if(userRodzaj.equals("dyr_generalny")){
				userDocelowy =pl.compan.docusafe.parametrization.paa.NormalLogic.getDyrektorGeneralny();
						
			}else if(userRodzaj.equals("prezes")){
				divisions= DSDivision.findByDescription("PREZES");
				// stanowisko prezes - pierwsze z brzegu, pierwszy user
				userDocelowy= divisions.get(0).getUsers()[0].getName();
			}else if(userRodzaj.equals("wiceprezes")){
				divisions= DSDivision.findByDescription("VCEPREZES");
				// stanowisko vce-prezes - pierwsze z brzegu, pierwszy user
				userDocelowy= divisions.get(0).getUsers()[0].getName();
			}else{
				log.error("Nieprawidlowy parametr user, oczekiwano dyr_departamentu, dyr_generalny, naczelnik, prezes lub wiceprezes");
			}
			
				/*DSDivision managerDivision = DivisionChief.findDyrektor(division, synonimy);
		
				if( managerDivision.equals(division)){
					log.error("Nie znaleziono kierownika");
				}
				
				userDocelowy = (managerDivision.getOriginalUsers()[0]).getIdentifier();*/
			
			if(userDocelowy==null){
				userDocelowy = pl.compan.docusafe.parametrization.paa.NormalLogic.getDyrektorDepartamentu(nadawca.getName(), doc);
				log.error("userDocelowy==null! Dekretuje dokument "+docId+" na dyrektora depart "+userDocelowy);
			}else
				log.info("Dekretuje dokument "+docId+" na uzytkownika "+userDocelowy);
			
			if (userDocelowy.equals(Docusafe.getAdditionProperty("PAA.DyrektorDEB")))
			{
				String users = Docusafe.getAdditionProperty("PAA.doKsiegowej");
				for(String user : users.split(",")){
					if (nadawca.getName().equals(user)){
					assignable.addCandidateUser(Docusafe.getAdditionProperty("PAA.Ksiegowa"));
					userDocelowy=Docusafe.getAdditionProperty("PAA.Ksiegowa");
					break;
				}
			}
			}
			assignable.addCandidateUser(userDocelowy);
			//assignable.setAssignee(userDocelowy);
			//TODO: sprawdzi� to
			TaskSnapshot.updateByDocumentId(docId,"anyType");
		
			
			AssignmentHistoryEntry entry = new AssignmentHistoryEntry();
			
			entry.setSourceUser(DSApi.context().getDSUser().getName());
			entry.setTargetUser(userDocelowy);
			String dzialNad =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(DSApi.context().getDSUser().getName());
			entry.setSourceGuid(dzialNad);
			String dzialOdb =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(userDocelowy);
			entry.setTargetGuid(dzialOdb);
			String statusDokumentu =  doc.getFieldsManager().getEnumItem("STATUSDOKUMENTU").getTitle();
			entry.setObjective(statusDokumentu);
			entry.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
			doc.addAssignmentHistoryEntry(entry);
			
			
			Date date = (Date) Calendar.getInstance().getTime();
			DateUtils data2 = new DateUtils();
	    	String dataa = data2.formatCommonDateTime(date);
			doc.addWorkHistoryEntry(Audit.create(DSApi.context().getPrincipalName(),nadawca.getName(),
	                "Dokument zosta� zdekretowany na  : "+userDocelowy+ " w dniu : " +dataa));
		}
		catch (Exception e) {
			log.error("Blad dla dokumentu "+docId+" - "+e.getMessage(), e);
			throw e;
		}
		finally {
			/*if (DSApi.isContextOpen() && !isOpened)
				DSApi.close();*/
		}
	}
	
}
