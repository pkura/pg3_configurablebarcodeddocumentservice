package pl.compan.docusafe.parametrization.paa;




import java.util.Calendar;
import java.util.Date;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;
import org.jfree.util.Log;


import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** asygnuje na sekretariat dyrektora użytkownika zapamiętanego w zmiennej procesu */
@SuppressWarnings("serial")
public class SekretariatDyrektoraKOzapamietanejOsoby_AsgnHandler implements AssignmentHandler
{
	private final static Logger log= LoggerFactory.getLogger(SekretariatDyrektoraKOzapamietanejOsoby_AsgnHandler.class);
	
	private String nazwaZmiennej;

	@Override
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception
	{

		log.info("SekretariatDyrektoraKOzapamietanejOsoby_AsgnHandler");
		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		
		OfficeDocument doc = OfficeDocument.find(docId);
		String data = Calendar.getInstance().getTime().toString();
		String rodzajDokumentu = doc.getDocumentKind().getCn();
		String dekretacja="";
		log.info("Dokument  id = "+docId+"Rodzaj =" +rodzajDokumentu+ " czas zajscia procesu "+data);
		// user zapamietanyy
		String zapamietanyUzytkownik= (String) openExecution.getVariable(nazwaZmiennej); // gotowe

		// String asygnowanyUzytkownik = ".....okreslic......"; //sekretariat dyrektora tego uzytkownika
		
		DSUser user= DSUser.findByUsername(zapamietanyUzytkownik);
		String userInSekretarit ="";
		// dyrektor departamentu
		String dyrektor= pl.compan.docusafe.parametrization.paa.NormalLogic.getDyrektorDepartamentu(user.getName(), doc);

		if (dyrektor.contains(user.getName())) {
			log.error("SekretariatDyrektoraKOzapamietanejOsoby_AsgnHandler- Dyrektor Departamentu nie został odnaleziony!");
			//throw new Exception(" Dyrektor Departamentu użytkownika nie został odnaleziony! ");
			userInSekretarit= pl.compan.docusafe.parametrization.paa.NormalLogic
					.getUserPelniocyRoleSekretariatu(user.getName());
		} else{
		/*	if (dyrektor.equals("WBDG")){
				dyrektor =pl.compan.docusafe.parametrization.paa.NormalLogic.getDyrektorGeneralny();
			}*/
		// wyszukiwanie użytkownika posiadającego role sekretariat w departamencie dyrektora po id roli
			userInSekretarit= pl.compan.docusafe.parametrization.paa.NormalLogic
				.getUserPelniocyRoleSekretariatu(dyrektor);
		}
		// weryfikacja czy zostął odnaleziony użytkownik sekretariatu dyrektora
		if (!userInSekretarit.contains(dyrektor) ||!userInSekretarit.contains(user.getName()) ) {
			// dekretowanie
			Log.info("SekretariatDyrektoraKOzapamietanejOsoby_AsgnHandler- Dekretuje na Użytkownika pełniącego role Sekretariatu Dyrektora - nazwa użytkownika : "
					+ userInSekretarit);
			assignable.addCandidateUser(userInSekretarit);

		} else {
			dekretacja = "admin";
			assignable.addCandidateUser(dekretacja);
			log.info("DyrektorKOzapamietanejOsoby_AsgnHandler - Pracownik sekretariatu nie został odnaleziony  - dokument trafia do administratora i sie zawiesza ID "
					+openExecution.getProcessDefinitionId()+" ; KEY "+openExecution.getKey()+";NAME "+openExecution.getName()+" STATE "+openExecution.getState());
			//assignable.addCandidateUser(dyrektor);
		}
		
		AssignmentHistoryEntry entry = new AssignmentHistoryEntry();

		entry.setSourceUser(DSApi.context().getDSUser().getName());
		entry.setTargetUser(userInSekretarit);
		String dzialNad =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(DSApi.context().getDSUser().getName());
		entry.setSourceGuid(dzialNad);
		String dzialOdb =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(userInSekretarit);
		entry.setTargetGuid(dzialOdb);
		String statusDokumentu =  doc.getFieldsManager().getEnumItem("STATUSDOKUMENTU").getTitle();
		entry.setObjective(statusDokumentu);
		entry.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
		doc.addAssignmentHistoryEntry(entry);
		
		/*Date date = (Date) Calendar.getInstance().getTime();
		DateUtils data2 = new DateUtils();
    	String dataa = data2.formatCommonDateTime(date);
		doc.addWorkHistoryEntry(Audit.create(DSApi.context().getPrincipalName(),DSApi.context().getPrincipalName(),
                "Dokument został zdekretowany na  : "+userInSekretarit+ " w dniu : " +dataa));
*/
		// dodac recznie do historii (w WSSK robilem to tak:)
		// Util_AHE.addToHistory(openExecution, doc, asygnowanyUzytkownik, null, null, log);
	}

}
