package pl.compan.docusafe.parametrization.paa;

import com.google.common.collect.Maps;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.DwrDocumentHelper;
import pl.compan.docusafe.webwork.event.ActionEvent;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;





import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.*;

@SuppressWarnings("serial")
public class PaaDocOutLogic extends AbstractDocumentLogic
{
	public final static String _USER = "USER";
	public final static String _DIVISION = "DSDIVISION";
	public final static String _stampDate = "stampDate";
	public final static String _znak_pisma = "znak_pisma";
	public final static String _PELENADRES = "PELENADRES";
	public final static String _SENDER_HERE = "SENDER_HERE";
	public final static String _POSITION = "POSITION";
	public final static String _NUMERPISMA = "NUMERPISMA";
	public final static String _AVAILABLE_DAYS ="AVAILABLE_DAYS";
	public static final String KIND_EXECUTE = "Do Realizacji";
	public static final String _multiOdbiorca = "M_DICT_VALUES";
	public static final String _pelenAdres = "PELENADRES";
	public static final String _RECIPIENT = "RECIPIENT";
	public static final String _DWR_RECIPIENT = "DWR_RECIPIENT";
	public static final String _REPILES_FOR = "REPILES_FOR";
	public static final String _RODZAJ_PRZESYLKI = "RODZAJ_PRZESYLKI";	
	public static final String _SPOSOB_DOSTARCZENIA = "SPOSOB_DOSTARCZENIA";
	public static final String _DOC_DELIVERY = "DOC_DELIVERY";
	public static final String _idMultiPerson = "ID";	
	public static final String _kosztJednostkowy = "KWOTA";	
	public static final String _rodzajPrzesylkiWagaKrajowa = "RODZAJ_PRZESYΘI_TYPE_WEIGHT_COUNTRY";	
	public static final String _rodzajPrzesylkiWagaSwiat = "RODZAJ_PRZESYΘI_TYPE_WEIGHT_WORLD";
	public static final String _GABARYT = "GABARYT";
	public static final String _dataDokumentu = "DOC_DATE";
	public static final String _odpowiedzNa = "REPILES_FOR";
	public static final String _biuroDyrGeneralnego = "WBDG";
	public static final String _KoszListu = "KOSZT_PRZESYLKI";
	public static final String _pierwszyOdbiorca = "RECIPIENT_1";
	private static final String PodzielonyDokument = "DOCIDZPODZIELONEGO";
	private static final String PODZIELONE = "PODZIELONE";
	public static final String _DWRGABARYT = "DWR_GABARYT";
	public static final String _DWRPACZKA = "DWR_PACZKA";
	public static final String _PACZKA = "PACZKA";
	public static final String _PODZIELPISMA = "PODZIELPISMA";

	private int count =0;
	private static Map <Integer ,Integer> gabaryty = new HashMap<Integer, Integer>();
	
	protected static DwrDocumentHelper dwrDocumentHelper = new DwrDocumentHelper();
	
	//public final static String _ADRESAT = "ADRESAT_DICT";
	//public final static String _KOD_PIKA = "KOD_PIKA";
	private static PaaDocOutLogic instance;
	protected static Logger log = LoggerFactory.getLogger(PaaDocOutLogic.class);
	 
	 
	public static PaaDocOutLogic getInstance()
	{
		if (instance == null)
			instance = new PaaDocOutLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	//nowa kolumna na liscie zadan 
	public TaskListParams getTaskListParams(DocumentKind dockind, long id)
			throws EdmException {
		TaskListParams params = new TaskListParams();

			FieldsManager fm = dockind.getFieldsManager(id);

				if (fm.getValue(_znak_pisma) != null)
					params.setAttribute((String) fm.getValue(_znak_pisma), 1);
		
				if (fm.getValue(_PELENADRES) != null)
					params.setAttribute((String) fm.getValue(_PELENADRES), 2);

			
		
		return params;
	}
	/*@Override
	public void onLoadData(FieldsManager fm) throws EdmException
	{
		// TODO Auto-generated method stub
		if (fm.getDocumentId() != null && fm.getField("RECIPIENT") != null && fm.getFieldValues().get("RECIPIENT") != null)
		{
			fm.getValue("RECIPIENT");
			Object obj = fm.getFieldValues().get("RECIPIENT");
			if (obj instanceof ArrayList<?>){
				ArrayList<Long> tabl = (ArrayList<Long>) fm.getFieldValues().get("RECIPIENT");
			tabl.clear();
			tabl.add(Long.parseLong("593"));
			tabl.add(Long.parseLong("131"));
			tabl.add(Long.parseLong("578"));

			fm.getFieldValues().put("RECIPIENT", tabl.toArray());
			}
		}
	}
*/
	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
	{
		Field msg = null;
		List<Integer> selectetRec = new ArrayList();
		int count = 0;
		values.get(_DWR_RECIPIENT).getDictionaryData();
		Map<String, FieldData> mapa = values.get(_DWR_RECIPIENT).getDictionaryData();
		// mapa.get("RECIPIENT_ID_1");

		// System.out.println("fsdfsdf");
		// String s= "";
		for (String key : mapa.keySet())
		{

			if (key.contains("RECIPIENT_ID_"))
			{
				if (mapa.get(key).getIntegerData() != null)
				{
					count++;
					int temp = mapa.get(key).getIntegerData();
					if (selectetRec.contains(temp))
					{
						//s = (String) key.subSequence(key.lastIndexOf("_"), key.length());
						msg = new Field("messageField", "Istniejacego juz taki odbiorce w pismie nalezy go  usu寞 - jego ID to  : " + temp, Field.Type.BOOLEAN);
						values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
						return msg;
					} else
					{
						selectetRec.add(temp);
					}
					if (values.get(_DWRGABARYT) != null && values.get(_DWRGABARYT).getEnumValuesData().getSelectedId() != null
							&& !values.get(_DWRGABARYT).getEnumValuesData().getSelectedId().isEmpty())
					{
						int gab = Integer.parseInt(values.get(_DWRGABARYT).getEnumValuesData().getSelectedId());
						if ((gab == 7 || gab == 8) && count > 1)
						{
							msg = new Field("messageField", "Przesy趾a to Paczka -  mo瞠 by� tylko jeden odbiorca dodatkowych nalezy usun寞  ",
									Field.Type.BOOLEAN);
							values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
						} else
						{
						}
					}

				}
			}
		}



		return msg;

	}
	
	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		
		log.info("type: {}", type);
		Map<String, Object> values= new HashMap<String, Object>();
		values.put("TYPE", type);
		values.put("ROK", Calendar.getInstance().get(Calendar.YEAR));
		
		fm.getField("JOURNAL").setRequired(true);
			values.put(_dataDokumentu, new Date());
			
			if (fm.getValue(_odpowiedzNa)!=null ) {
				fm.getField(_odpowiedzNa).setHidden(false);
				fm.getField(_odpowiedzNa).setReadOnly(true);
				
		}else {
			fm.getField(_odpowiedzNa).setHidden(true);
			fm.getField(_odpowiedzNa).setReadOnly(false);
		}
		
		
		fm.reloadValues(values);
			
	}
	
	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
			try
			{
				String[] ids;
				Document doc = Document.find(docId);
				FieldsManager fm = doc.getFieldsManager();
				fm.initialize();

				String odbiorcy = fm.getStringKey(_RECIPIENT);
	    		if (odbiorcy.startsWith("[")) {
	    			odbiorcy= odbiorcy.substring(1, odbiorcy.length() - 1);
	    			if (odbiorcy.contains(","))
	    				ids= odbiorcy.split(",");
	    			else ids= new String[1];
	    			ids[0]= odbiorcy;
	    			
	    		} else {
	    			ids= new String[1];
	    			ids[0]= odbiorcy;
	    		}
	    		for (String id : ids) {
				Map<String,Object> odbiorca = DwrDictionaryFacade.dictionarieObjects.get(_RECIPIENT).getValues(id.trim());
				
				if (odbiorca.get("RECIPIENT_TITLE")!=null)
				values.put("TITLE", odbiorca.get("RECIPIENT_TITLE")+" ");
				values.put("FIRSTAME", odbiorca.get("RECIPIENT_FIRSTNAME")!=null ? odbiorca.get("RECIPIENT_FIRSTNAME") : "" );
				values.put("LASTNAME", odbiorca.get("RECIPIENT_LASTNAME")!=null ? odbiorca.get("RECIPIENT_LASTNAME") : "" );
				values.put("ORGANIZATION", odbiorca.get("RECIPIENT_ORGANIZATION")!=null ? odbiorca.get("RECIPIENT_ORGANIZATION") : "" );
				values.put("STREET", odbiorca.get("RECIPIENT_STREET")!=null ? odbiorca.get("RECIPIENT_STREET") : "" );
				values.put("ZIP", odbiorca.get("RECIPIENT_ZIP")!=null ? odbiorca.get("RECIPIENT_ZIP") : "" );
				values.put("LOCATION", odbiorca.get("RECIPIENT_LOCATION")!=null ? odbiorca.get("RECIPIENT_LOCATION") : "" );
				EnumValues ev  = (EnumValues) odbiorca.get("RECIPIENT_COUNTRY");
				if (ev!=null && ev.getSelectedValue()!=null  && !ev.getSelectedValue().isEmpty())
				values.put("COUNTRY", ev.getSelectedValue());
				}
			
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
	}
	
		
	public Map<String, String> setInitialValuesForReplies(Long inDocumentId) throws EdmException
	{
		Map<String, String> dependsDocs = new HashMap<String, String>();
	
		dependsDocs.put(_REPILES_FOR, OfficeDocument.find(inDocumentId).getOfficeNumber().toString());

		
		return dependsDocs;
	}
	
	
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		as.useFolderResolver(document);
		as.useAvailableProviders(document);
		log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
	}

	@Override
	public String onStartManualAssignment(OfficeDocument doc, String fromUsername, String toUsername)
	{
		
		//log.info("--- PaaDocOutLogic : onStartManualAssignment  dekretacja reczna dokument闚 : dokument ID = "+doc.getId()+" : pomiedzy nadawc� -->"+fromUsername +" a odbiorc� ->"+toUsername );
		log.info("--- PaaDocOutLogic : Sprawdzam Czy s� w tym samym departamencie ");
		if (DSApi.context().isAdmin())
			return toUsername;
		if(!NormalLogic.inDepIsOutDep(fromUsername, toUsername)){
	        	log.info("--- PaaDocOutLogic : Wynik ->  Nie s� w tym samym  pobieram dyrektora departamentu  ");
				toUsername = NormalLogic.getDyrektorDepartamentu(toUsername ,doc);
				//czy uzytkownik zanjduje sie w Biurze Dyrektora generalnego
				/*if (toUsername.contains(_biuroDyrGeneralnego))
					try {
						toUsername = NormalLogic.getDyrektorGeneralny();
					} catch (Exception e) {
						log.error("", e);
					}*/
				log.info("--- PaaDocOutLogic : Wynik -> pobra貫m dyrektora -> " + toUsername);
	    	}
	
		log.info("--- PaaDocOutLogic : WYnik ->  ostateczny rezultat  dekretacja na -> "+ toUsername);
		return toUsername;
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- PaaDocOutLogic : START PROCESS !!! ---- {}", event);
		try {

			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM,event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind()
					.getDockindInfo()
					.getProcessesDeclarations()
					.onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

	}
	 
	@Override
	public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
	{

		System.out.println(kind.getCn());
		System.out.println(values);
		setPaczka(values);

		if (values.get(_znak_pisma) == null && values.get("JOURNAL") != null)
		{

			String dzialUzytkownika = "brak";
			int rok = Calendar.getInstance().get(Calendar.YEAR);
			long idDziennika = (Long) values.get("JOURNAL");
			Journal journal = Journal.find(idDziennika);
			long sequenceID = journal.getSequenceId();
			String symbolDiennika = journal.getSymbol();

			long nrpismaWychOgolnie = calculateWychOgolnie(documentId);


			dzialUzytkownika = symbolDiennika;
			long nrpismaWychWdziale = sequenceID;

			String wartoscPola = dzialUzytkownika + "/" + nrpismaWychWdziale + "/" + nrpismaWychOgolnie + "/" + rok;
			values.put(_znak_pisma, wartoscPola);
		}

		if (documentId != null)
		{
			OutOfficeDocument doc = (OutOfficeDocument) OfficeDocument.find(documentId);
			if (/*doc.getOfficeNumber() != null &&*/ sprawdzCzyPojedynczyOdbiorca(values))
				obliczPojedynczyKoszt(values, doc);
		}

	
		///			 MULTI ODBIORCA 
		if (documentId != null && (values.get(_PODZIELPISMA) != null && ((Boolean) values.get(_PODZIELPISMA))))
		{
			OutOfficeDocument doc = (OutOfficeDocument) OfficeDocument.find(documentId);
			if (doc.getOfficeNumber() != null)
			{
				podzielPisma(values, doc);
			} else
				throw new EdmException("Aby podzieli� pisma musi by� nadany numer KO");

		}


		// wyliczenie znaku pisma 
		if (values.get(_znak_pisma) == null && values.get("JOURNAL") != null)
		{
			String znakPisma = calculateZnakPisma(values, count, documentId);
			values.put(_znak_pisma, znakPisma);
		}

		//Okreslanie pola pe貫n adres
		if (sprawdzCzyPojedynczyOdbiorca(values))
			uzupe軟ijPelenAdres(values, documentId);
		else if (sprawdzCzyMultiOdbiorca(values))
			values.put(_pelenAdres, "Multi odbiorca");
		else if (documentId == null)
			values.put(_pelenAdres, "");
		else
		{
			uzupe軟ijPelenAdres(values, documentId);


		}
		/*if (AvailabilityManager.isAvailable("updateoutdoc")){
			zaktualizuj2();
		}
		String a = Docusafe.getAdditionProperty("PAA.aktualizujdocumentPersonoPrzedMulti");
		if (a.equals("true"))
		{
			zaktualizuj();

		}*/
	}

	private void setPaczka(Map<String, Object> values) throws EdmException
	{

		if (values.get(_GABARYT) != null)
		{
			int gab = (Integer) values.get(_GABARYT);
			if (gab == 7 || gab == 8)
			{
				if (!sprawdzCzyPojedynczyOdbiorca(values))
					throw new EdmException("Przesy趾a jako Paczka mo瞠 miec tylko jednego odbiorce! ");
				else
					values.put(_PACZKA, true);
			} else
			{
				values.put(_PACZKA, false);
			}
		}
	}

	private void uzupe軟ijPelenAdres(Map<String, Object> values, Long documentId) throws DocumentNotFoundException, EdmException
	{


		if (documentId == null || values.get(_multiOdbiorca) != null)
		{
			Map<String, FieldData> wartosci = (Map<String, FieldData>) values.get(_multiOdbiorca);
			for (String key : wartosci.keySet())
			{
				Map<String, FieldData> wpisMulti = (Map<String, FieldData>) wartosci.get(key);
				values.put(_pelenAdres, getNewPelenAdresFromMap(wpisMulti));
			}
		} else
		{
			values.put(_pelenAdres, getNewPelenAdresFromRecipientInDoument(documentId));
		}
	}

	private String getNewPelenAdresFromRecipientInDoument(Long documentId) throws DocumentNotFoundException, EdmException
	{
		StringBuilder sb = new StringBuilder();
		List<Recipient> list = OutOfficeDocument.find(documentId).getRecipients();
		for (Recipient recipient : list)
		{
			sb.append(recipient.getOrganization() != null ? recipient.getOrganization() + ", " : "");
			sb.append(recipient.getFirstname() != null ? recipient.getFirstname() + " " : "");
			sb.append(recipient.getLastname() != null ? recipient.getLastname() + ", " : "");
			sb.append(recipient.getStreet() != null ? recipient.getStreet() + " " : "");
			sb.append(recipient.getZip() != null ? recipient.getZip() + ", " : "");
			sb.append(recipient.getLocation() != null ? recipient.getLocation() : "");

		}
		return sb.toString();
	}

	private String getNewPelenAdresFromMap(Map<String, FieldData> wpisMulti)
	{
		StringBuilder sb = new StringBuilder();
		Person person = utworzPersona(wpisMulti);
		sb.append(person.getOrganization() != null ? person.getOrganization() + ", " : "");
		sb.append(person.getFirstname() != null ? person.getFirstname() + " " : "");
		sb.append(person.getLastname() != null ? person.getLastname() + ", " : "");
		sb.append(person.getStreet() != null ? person.getStreet() + " " : "");
		sb.append(person.getZip() != null ? person.getZip() + ", " : "");
		sb.append(person.getLocation() != null ? person.getLocation() : "");
		
		return sb.toString();
	}

	private void validateRecipientInDocument(Map<String, Object> values, Long documentId) throws DocumentNotFoundException, EdmException
	{

		OutOfficeDocument doc = null;
		doc = (OutOfficeDocument) OutOfficeDocument.find(documentId);
		List<Long> selectetPersonIds = new ArrayList();
		Map<String, FieldData> odbiorcy = (Map<String, FieldData>) values.get(_multiOdbiorca);
		if (odbiorcy != null)
		{
			for (String key : odbiorcy.keySet())
			{
				Map<String, FieldData> wpisMulti = (Map<String, FieldData>) odbiorcy.get(key);
				selectetPersonIds.add(wpisMulti.get(_idMultiPerson).getLongData());
			}
			if (isAnyInvalidRecipients(selectetPersonIds, doc))
				createValidRecipient(doc, selectetPersonIds);
		}
	}

	private boolean isAnyInvalidRecipients(List<Long> selectetPersonIds, OutOfficeDocument doc) throws EdmException
	{
		List<Long> wybrane = new ArrayList();
		wybrane.addAll(selectetPersonIds);
		List<Long> basePersonIdsRecipients = getListBasePersonIdRecipiets(doc);
		basePersonIdsRecipients.removeAll(wybrane);
		if (!basePersonIdsRecipients.isEmpty())
			return true;
		else
		{
			wybrane.removeAll(getListBasePersonIdRecipiets(doc));
			if (!wybrane.isEmpty())
				return true;
		}

		return false;


	}

	private static void removeDuplicates(List<Long> recipientsToRemove) {
        HashSet set = new HashSet(recipientsToRemove);
        recipientsToRemove.clear();
        recipientsToRemove.addAll(set);
}
	
	private void createValidRecipient(OutOfficeDocument doc, List<Long> selectetPersonIds) throws EdmException
	{

		//selectetPersonIds - id perso闚 z dokinda w s這wniku multi

		List<Recipient> newListRecipient = new ArrayList();
		//lista person闚 z kt鏎ych utworzeni zostali odbiorcy danego dokumentu
		List<Long> basePersonIdsRecipients = getListBasePersonIdRecipiets(doc);
		//lista id person z kt鏎eg otrzeba utworzy� odbiorce i doda� do dokumentu 
		List<Long> personToCreate = new ArrayList();
		//lista odbiorc闚 dokumentu do usuni璚ia 
		List<Long> recipientsToRemove = getListBasePersonIdRecipiets(doc);

		//wyciagniecie recipienta kt鏎ego trezeaba usun寞 bo nie jest zgodny z dokumentem
		recipientsToRemove.removeAll(selectetPersonIds);

		//usuniecie wszytkich id wybranych person闚 z kt鏎ych utworzeni s� juz odbiorcy kt鏎zy s� zgodni z tymi co na dokumencie 
		selectetPersonIds.removeAll(basePersonIdsRecipients);

		for (Long l : selectetPersonIds)
			personToCreate.add(l);

		removeDuplicates(recipientsToRemove);

		Iterator<Recipient> iter = doc.getRecipients().listIterator();
		while (iter.hasNext())
		{
			Recipient rec = iter.next();
			for (Long l : recipientsToRemove)
			{
				if (rec.getBasePersonId().compareTo(l) == 0)
				{
					iter.remove();
					rec.delete();
				}
			}
		}

		for (Long l : personToCreate)
		{
			doc.addRecipient(utworzRecipient(Person.findIfExist(l)));
		}

	}

	private List<Long> getListBasePersonIdRecipiets(OutOfficeDocument doc) throws EdmException
	{
		List<Long> list = new ArrayList();
		for (Recipient rec : doc.getRecipients()){
			list.add(rec.getBasePersonId());
			}
		return list;
	}

/*	
	private void zaktualizuj() throws EdmException
	{
		String donumeru = Docusafe.getAdditionProperty("PAA.donumeru");
		 log.info("PAA.donumeru - Okreslono wyszukiwanie do ID  dokumentu mniejszego ni�  : "+donumeru);
		 
		// String odnumeru = Docusafe.getAdditionProperty("PAA.Odnumeru");
		List<Long> listaIDDoc= new ArrayList<Long>();

		long countwszystkie = 0;
		long countPrawidlowe = 0;
		long countbezRodzajuPrzesy趾i = 0;
		long countBezSposobuDostarczenia = 0 ;
		String  selectDocuments = "Select DOCUMENT_ID from DS_PAA_OUT_DOC  where DOCUMENT_ID <"+donumeru;
	
		 PreparedStatement pst = null;
		   try {
			pst = DSApi.context().prepareStatement(selectDocuments);
			ResultSet rs = pst.executeQuery();
			
		   while (rs.next()){
			   listaIDDoc.add(rs.getLong(1));
			}
		   pst.close();
		   log.info("Znaleziono dokument闚 : "+listaIDDoc.size());
		   
			String  selectbasePerson = "Select basePersonId  from DSO_PERSON where DISCRIMINATOR='RECIPIENT' and DOCUMENT_ID =";
			String insertToMultiple = "insert into DS_PAA_MULTIPLE_ODBIORCA  VALUES(?,?,?)";
		
			String selectRodzajPrzesylkiPaaoutdoc =" Select RODZAJ_PRZESYlKI from DS_PAA_OUT_DOC  where DOCUMENT_ID=";
			
			for (Long idDok : listaIDDoc){
				long basePersonID = 0;
				int rodzajprzesPAA =0;
				int outdelivery =0;
				log.info("Update pism z sigle do multi - idok "+idDok);
				
				
				OutOfficeDocument outod = (OutOfficeDocument) OutOfficeDocument.find(idDok);
				if (outod.getDelivery()!=null && outod.getDelivery().getId()!=null) {
				outdelivery = outod.getDelivery().getId();
				}
				log.info("Update pism z sigle do multi - outofficedeliveryID "+outdelivery);
				
				 pst = null;
				 pst = DSApi.context().prepareStatement(selectRodzajPrzesylkiPaaoutdoc+idDok);
					 rs = pst.executeQuery();
						if (rs.next()) {
							rodzajprzesPAA = rs.getInt(1);
						}
						pst.close();
						log.info("Update pism z sigle do multi - Rodzaj przesy趾i DS_PAA_DOC "+rodzajprzesPAA);
						pst = null;
						pst = DSApi.context().prepareStatement(selectbasePerson+idDok);
						rs = pst.executeQuery();
						if (rs.next()) {
							basePersonID=rs.getLong(1);
							pst.close();
							log.info("Update pism z sigle do multi - Rodzaj base person id dla dokumentu  "+basePersonID);
							pst = null;
							pst = DSApi.context().prepareStatement(insertToMultiple);
							pst.setLong(1,idDok );					
							pst.setString(2,"RECIPIENT");
							pst.setLong(3,basePersonID );	
							pst.execute();
							pst.close();
							log.info("Update pism z sigle do multi Insrt into DSPAAmultiple values  , id dok "+idDok+" VALUE - RECIPIENT , BasepersonID "+basePersonID);
							pst = null;

							String updatePAAODBIORCA= "update DS_PAA_ODBIORCA set SPOSOB_DOSTARCZENIA=?, RODZAJ_PRZESYLKI =?  where id=?";
							if (outdelivery != 0){
							
							pst = DSApi.context().prepareStatement(updatePAAODBIORCA);
							pst.setInt(1,outdelivery );	
							if (rodzajprzesPAA!=0)
							pst.setInt(2,rodzajprzesPAA);
							else{
							pst.setInt(2,5);
							log.info("Dokument ID = " +idDok +"Brak wybranego rodzaju wysy趾i (krajowa ekonomiczna , priorytetowa , swait ekonomy , priorytet) ustawiam wartos� inne ");
							}
							countbezRodzajuPrzesy趾i++;
							
							pst.setLong(3,basePersonID);
							pst.execute();
							pst.close();
							countPrawidlowe++;
							log.info("Prawid這wy update DS_PAA_ODBIORCA" + updatePAAODBIORCA+ "warto�ci  sp dost : "+outdelivery+"Rodzaj Przes"+rodzajprzesPAA);
							}
							else {
								log.info("Update pism z sigle do multi - Bez updata DS_PAA_ODBIORCA bo brak sposobu dostarczenia");
								countBezSposobuDostarczenia++;
							}
						}
						
			
			countwszystkie++;
			}
			log.info("Wszystkie dokumenty ilo�� "+countwszystkie);
			log.info("Wszystkie countBezSposobuDostarczenia ilo�� "+countBezSposobuDostarczenia+"Pozosta造 bez zmian ");
			
			log.info("Wszystkie countbezRodzajuPrzesy趾i ilo�� "+countbezRodzajuPrzesy趾i );
			log.info("Wszystkie Prawid這we ilo�� "+countPrawidlowe);
			
			} catch (SQLException e) {
				log.error("Jaki� b豉d z sqlami ", e);
			}
		
	}
	
	
	private void zaktualizuj2() throws EdmException
	{
		
		try {
		List<Long> listaIDDoc= new ArrayList<Long>();
		List<Long> gabaryts= new ArrayList<Long>();
		//long gabaryt= 0L;
		long dociD= 0L;
		long country= 0L;
		long world= 0L;
		long idDok = 0L;
		long gabaryt = 0L;
		long countbezRodzajuPrzesy趾i = 0L;
		long countBezSposobuDostarczenia = 0L ;
		Object 	priority = null;
	//	boolean countryl = false
		//	boolean worldl = false;
		PreparedStatement pst = null;
		PreparedStatement pst2 = null;
	
			String selectRodzajPrzesylkiPaaoutdoc =" Select document_id, GABARYT, RODZAJ_PRZESYΘI_TYPE_WEIGHT_COUNTRY,RODZAJ_PRZESYΘI_TYPE_WEIGHT_WORLD from DS_PAA_OUT_DOC  where GABARYT is not null ";
	
				 pst = null;
				 pst = DSApi.context().prepareStatement(selectRodzajPrzesylkiPaaoutdoc);
					ResultSet  rs = pst.executeQuery();
					int i = 0 ;
						while (rs.next()) {
							idDok= (rs.getLong(1));
							gabaryt = rs.getLong(2);
							if (rs.getObject(3)!=null)
								country= 1;
								else 
								country=0;
							
							if (rs.getObject(4)!=null)
								world= 1;
							else 
								world=0;
						
						
							//idDok = rs.getLong(1);
						//	gabaryt = rs.getLong(2);
						//pst.close();
					//	log.info("Update pism z sigle do multi - Rodzaj przesy趾i DS_PAA_DOC "+);
						pst2 = null;
						
						OutOfficeDocument doc =(OutOfficeDocument) OutOfficeDocument.find(idDok);
				if (doc.getDelivery()==null)
					priority = null;
				else {
						int  deliveryid = 	doc.getDelivery().getId();
					if (deliveryid == 1)
						priority = 0 ;
						
					else if (deliveryid == 19)
						priority = 1 ;
					else
						priority = null ;
				}
						String upd = "Update  DSO_OUT_DOCUMENT set gauge = ? , priority =?, countryLetter =?  where id=?"; 
						//for(int x = 0 ; x< listaIDDoc.size(); x++){
							pst2 = null;
							pst2 = DSApi.context().prepareStatement(upd);
							pst2.setLong(1, gabaryt);
						if (country == 1)
							pst2.setLong(3,country);
						else if (world == 1)
								pst2.setLong(3,world);
						else
							pst2.setObject(3,null);
						
							pst2.setObject(2,priority);
							
							pst2.setLong(4, idDok);
							pst2.execute();
							pst2.close();
						}
						
						
			
		
			
		
			
			} catch (SQLException e) {
				log.error("Jaki� b豉d z sqlami ", e);
			}
		
	}
	
	private void zaktualizuj3() throws EdmException
	{
		
		try {
		List<Long> listaIDDoc= new ArrayList<Long>();
		List<Long> gabaryts= new ArrayList<Long>();
		//long gabaryt= (long) 0;
		long dociD= (long) 0;
		long country= (long) 0;
		long world= (long) 0;
		long idDok = 0;
		long gabaryt = 0;
		long countbezRodzajuPrzesy趾i = 0;
		long countBezSposobuDostarczenia = 0 ;
		Object 	priority = null;
	//	boolean countryl = false
		//	boolean worldl = false;
		PreparedStatement pst = null;
		PreparedStatement pst2 = null;
	
			String selectRodzajPrzesylkiPaaoutdoc =" Select document_id, GABARYT, RODZAJ_PRZESYΘI_TYPE_WEIGHT_COUNTRY,RODZAJ_PRZESYΘI_TYPE_WEIGHT_WORLD from DS_PAA_OUT_DOC  where GABARYT is not null ";
	
				 pst = null;
				 pst = DSApi.context().prepareStatement(selectRodzajPrzesylkiPaaoutdoc);
					ResultSet  rs = pst.executeQuery();
					int i = 0 ;
						while (rs.next()) {
							idDok= (rs.getLong(1));
							gabaryt = rs.getLong(2);
							if (rs.getObject(3)!=null)
								country= 1;
								else 
								country=0;
							
							if (rs.getObject(4)!=null)
								world= 1;
							else 
								world=0;
						
						
							//idDok = rs.getLong(1);
						//	gabaryt = rs.getLong(2);
						//pst.close();
					//	log.info("Update pism z sigle do multi - Rodzaj przesy趾i DS_PAA_DOC "+);
						pst2 = null;
						
						OutOfficeDocument doc =(OutOfficeDocument) OutOfficeDocument.find(idDok);
				if (doc.getDelivery()==null)
					priority = null;
				else {
						int  deliveryid = 	doc.getDelivery().getId();
					if (deliveryid == 1)
						priority = 0 ;
						
					else if (deliveryid == 19)
						priority = 1 ;
					else
						priority = null ;
				}
						String upd = "Update  DSO_OUT_DOCUMENT set gauge = ? , priority =?, countryLetter =?  where id=?"; 
						//for(int x = 0 ; x< listaIDDoc.size(); x++){
							pst2 = null;
							pst2 = DSApi.context().prepareStatement(upd);
							pst2.setLong(1, gabaryt);
						if (country == 1)
							pst2.setLong(3,country);
						else if (world == 1)
								pst2.setLong(3,world);
						else
							pst2.setObject(3,null);
						
							pst2.setObject(2,priority);
							
							pst2.setLong(4, idDok);
							pst2.execute();
							pst2.close();
						}
						
						
			
		
			
		
			
			} catch (SQLException e) {
				log.error("Jaki� b豉d z sqlami ", e);
			}
		
	}*/

	private boolean sprawdzCzyPojedynczyOdbiorca(Map<String, Object> values)
	{
		Map<String, FieldData> wartosci = (Map<String, FieldData>) values.get(_multiOdbiorca);
		if (wartosci != null && wartosci.size() == 1)
			return true;

		return false;
	}

	private boolean sprawdzCzyMultiOdbiorca(Map<String, Object> values)
	{
		Map<String, FieldData> wartosci = (Map<String, FieldData>) values.get(_multiOdbiorca);
		if (wartosci != null && wartosci.size() > 1)
			return true;

		return false;
	}

	
	
	
	private String calculateZnakPisma(Map<String, Object> values, int count2, Long documentId)
	{


		String znakPisma = "Brak";
		if (values.get("JOURNAL") != null)
		{
			long idDziennika = 0;
			try
			{
				String dzialUzytkownika = "brak";
				int rok = Calendar.getInstance().get(Calendar.YEAR);

				try
				{
					idDziennika = (Long) values.get("JOURNAL");
				} catch (Exception e)
				{
					idDziennika = Long.parseLong((String) values.get("JOURNAL").toString());
				}

				Journal journal = Journal.find(idDziennika);
				long sequenceID = journal.getSequenceId();
				String symbolDiennika = journal.getSymbol();
				long nrpismaWychOgolnie = calculateWychOgolnie(documentId);

				//long  = calculateDzial(journal);

				dzialUzytkownika = symbolDiennika;
				long nrpismaWychWdziale = sequenceID + count;

				znakPisma = dzialUzytkownika + "/" + nrpismaWychWdziale + "/" + nrpismaWychOgolnie + "/" + rok;

			} catch (EdmException e)
			{
				log.error("", e);
			}
		}
		return znakPisma;
	}


	private void podzielPisma(Map<String, Object> values, OutOfficeDocument doc) throws EdmException
	{

		//pobieranie wartosci pul multi*/	
		getGabaryty();
		Map<String, FieldData> wartosci = (Map<String, FieldData>) values.get(_multiOdbiorca);
		if (wartosci != null)
		{
			count = 0;
			for (String key : wartosci.keySet())
			{
				Map<String, FieldData> wpisMulti = (Map<String, FieldData>) wartosci.get(key);
				if ((wpisMulti.get(_RODZAJ_PRZESYLKI) == null) && (wpisMulti.get(_SPOSOB_DOSTARCZENIA) == null))
				{
					pojedynczyOdbiorca(wartosci, doc, key);
					obliczPojedynczyKoszt(values, doc);
				} else
				{
					int rodzajPrzesylkiId = Integer.parseInt(wpisMulti.get(_RODZAJ_PRZESYLKI).getStringData());
					int sposobDostarczeniaId = Integer.parseInt(wpisMulti.get(_SPOSOB_DOSTARCZENIA).getStringData());
					
					FieldData polekwota = wpisMulti.get(_kosztJednostkowy);
					double kwota = NormalLogic.policzKwote(polekwota, values, sposobDostarczeniaId, rodzajPrzesylkiId);
					// dla pierwszego odbiorcy
					if (key.equals(_pierwszyOdbiorca))
					{
						pierwszyOdbiorca(values, wpisMulti, doc);
						doc.setStampFee(BigDecimal.valueOf(kwota));
						okreslCzyPriorytetiKrajowa(rodzajPrzesylkiId,doc);
						doc.setGauge(gabaryty.get(values.get("GABARYT")));
						
						wpisMulti.put(_kosztJednostkowy, polekwota);
						DSApi.context().session().saveOrUpdate(doc);
					} else
					{
						wpisMulti.put(_kosztJednostkowy, polekwota);
						// dla pozosta造ch odbiorc闚
						Map<String, Object> mapaOldValuesKopia = new HashMap<String, Object>();
						for (String _key : values.keySet())
							mapaOldValuesKopia.put(_key, values.get(_key));

						//Tworzenie nowego dokumentu wychodz鉍ego 
						OutOfficeDocument nowyDocument = NormalLogic.createDokument(doc, mapaOldValuesKopia, wpisMulti);
						nowyDocument.setStampFee(BigDecimal.valueOf(kwota));
						okreslCzyPriorytetiKrajowa(rodzajPrzesylkiId,nowyDocument);
					
						nowyDocument.setGauge(gabaryty.get(values.get("GABARYT")));
						
						
						List<Journal> journals = Journal.findByDocumentId(doc.getId());
						if (!journals.isEmpty())
						{
							for (Journal journal : journals)
							{
								NormalLogic.bindToJournal(nowyDocument, journal, null);
								doc.setJournal(journal);
								nowyDocument.setJournal(journal);
							}
						}

						// po wpisaniu do dzinenika innego ni� g堯wny okreslamy znak pisma i pozosta貫 warto�ci dokumentu
						Map<String, Object> fieldValues = new HashMap<String, Object>();


						//zakomentowane pozostawiamy numer pisma stary z glownego dokumentu
						//aby wszyckie pisma mialy taki sam (ale pisam nie beda wpisane do dziennika )
						/*if (nowyDocument.getFieldsManager().getKey("JOURNAL")!=null){
							
						 fieldValues.put("JOURNAL", nowyDocument.getFieldsManager().getKey("JOURNAL"));
						 fieldValues.put(_znak_pisma, calculateZnakPisma(fieldValues, count, doc.getId()));
						 count++;
						}*/

						fieldValues.put(_znak_pisma, (String) mapaOldValuesKopia.get(_znak_pisma));
						fieldValues.put(PODZIELONE, true);
						fieldValues.put(PodzielonyDokument, doc.getId().toString());

						fieldValues.put(_pelenAdres, getNewPelenAdresFromMap(wpisMulti));


						nowyDocument.getDocumentKind().setOnly(nowyDocument.getId(), fieldValues);
						Person person = utworzPersona(wpisMulti);
						nowyDocument.addRecipient(utworzRecipient(person));

						onStartProcess(nowyDocument);
						copyhistory(doc, nowyDocument);
						TaskSnapshot.updateByDocumentId(nowyDocument.getId(), "anyType");
						DSApi.context().session().saveOrUpdate(nowyDocument);
					}

				}

			}

			//Usuwanie odbiorc闚 poza pierwszym 
			Iterator<String> it = wartosci.keySet().iterator();
			while (it.hasNext())
			{
				String key = it.next();
				if (!key.equals(_pierwszyOdbiorca))
					it.remove();
			}
			
			doc.getDocumentKind().saveDictionaryValues(doc, values);
			doc.getDocumentKind().saveMultiDictionaryValues(doc, values);
			doc.getDocumentKind().set(doc.getId(), values);
			DSApi.context().session().save(doc);


		}
	}




	private void okreslCzyPriorytetiKrajowa(int rodzajPrzesylkiId, OutOfficeDocument doc)
	{
		if (rodzajPrzesylkiId==1){
			doc.setPriority(false);
			doc.setCountryLetter(true);
		}else if (rodzajPrzesylkiId==2){
			doc.setPriority(true);
			doc.setCountryLetter(true);
		}else if (rodzajPrzesylkiId==3){
			doc.setPriority(false);
			doc.setCountryLetter(false);
		}else if (rodzajPrzesylkiId==4){
			doc.setPriority(true);
			doc.setCountryLetter(false);
		}
			
		
	}

	/**
	 * Oblicza  kwote przesy趾i dla pojedynczego oodbiorcy pisma oraz dodaje do pisma odbiorce 
	 * @param values
	 * @param doc
	 * @throws EdmException
	 */
	private void obliczPojedynczyKoszt(Map<String, Object> values, OutOfficeDocument doc) throws EdmException
	{
		getGabaryty();
		double oplata = 0;
		Map<String, FieldData> wartosci = (Map<String, FieldData>) values.get(_multiOdbiorca);

		for (String key : wartosci.keySet())
		{
			Map<String, FieldData> wpisMulti = (Map<String, FieldData>) wartosci.get(key);
			FieldData polekwota = wpisMulti.get(_kosztJednostkowy);
			int sposobDostarczeniaId = Integer.parseInt(wpisMulti.get(_SPOSOB_DOSTARCZENIA).getStringData());
			if (!(sposobDostarczeniaId == 1 || sposobDostarczeniaId == 19))
			{
				polekwota.setStringData("0");
			} else
			{
				int rodzajPrzesylkiId = Integer.parseInt(wpisMulti.get(_RODZAJ_PRZESYLKI).getStringData());
				if (values.get(_GABARYT) != null 
						&& (values.get(_rodzajPrzesylkiWagaKrajowa) != null || values.get(_rodzajPrzesylkiWagaSwiat) != null))
				{
					boolean country = true;
					if (values.get(_rodzajPrzesylkiWagaKrajowa) == null)
						country = false;
					doc.setGauge(gabaryty.get(values.get(_GABARYT)));
					
					
					// wyliczanie koszt闚 i wpisanie do fielda
					oplata = NormalLogic.calculateCosts(values, rodzajPrzesylkiId, sposobDostarczeniaId, country, polekwota);
					polekwota.setData(String.valueOf(oplata));
					doc.setStampFee(BigDecimal.valueOf(oplata));
					doc.setCountryLetter(country);
					okreslCzyPriorytetiKrajowa(rodzajPrzesylkiId, doc);
						
					if (doc.getRecipients().isEmpty())
					{
						Person person = utworzPersona(wpisMulti);

						doc.addRecipient(utworzRecipient(person));

					}
				}
			}
			List<Journal> journals = Journal.findByDocumentId(doc.getId());
			for (Journal j : journals)
				doc.setJournal(j);
			values.put(_DOC_DELIVERY, String.valueOf(wpisMulti.get(_SPOSOB_DOSTARCZENIA)));
			DSApi.context().session().save(doc);
		}
	}

private Recipient utworzRecipient(Person person)
	{
	Recipient recipient = new Recipient();
	
	try
	{
		recipient.fromMap(person.toMap());
		recipient.setBasePersonId(person.getId());
		recipient.create();
		
	} catch (EdmException e)
	{
		log.error("", e);
	}
	
	return recipient;
		
	}

// tworzy recipienta z przekazanego id z tabeli multiOdbiorc闚
private void pojedynczyOdbiorca(Map<String, FieldData> wartosci, OutOfficeDocument doc, String key) throws NumberFormatException, EdmException
	{
	Map<String, FieldData> wpisSingle =  (Map<String, FieldData>) wartosci.get(key);

	Person person = utworzPersona(wpisSingle);
	doc.addRecipient(utworzRecipient(person));
	uzupe軟ijPelenAdresZDokumentu(wartosci, doc.getId());
	
	}

private void uzupe軟ijPelenAdresZDokumentu(Map<String, FieldData> wartosci, Long documentId) throws DocumentNotFoundException, EdmException
{
	StringBuilder sb = new StringBuilder();
		List<Recipient> list = OutOfficeDocument.find(documentId).getRecipients();
	for (Recipient recipient :list){
	sb.append(recipient.getOrganization() != null ? recipient.getOrganization() + ", " : "");
	sb.append(recipient.getFirstname() != null ? recipient.getFirstname()+" " : "");
	sb.append(recipient.getLastname() != null ? recipient.getLastname() +", " : "");
	sb.append(recipient.getStreet() != null ? recipient.getStreet() +" ": "");
	sb.append(recipient.getZip() != null ? recipient.getZip()+", " : "");
	sb.append(recipient.getLocation() != null ? recipient.getLocation() : "");
	}
	FieldData fd = wartosci.get(_pelenAdres);
	fd.setStringData(sb.toString());
	wartosci.put(_pelenAdres, fd) ;
	
}

private Person utworzPersona(Map<String, FieldData> wpis)
{
	Person person = new Person();
	//	Person oldp =  Person.findPersonByFirstanemLastnameStreetLocationZip(wpis);
	for (String cn : wpis.keySet())
	{
		String type ="";
		
		if (wpis.get(cn).getData()!=null ){
			if (cn.equalsIgnoreCase(type + "title"))
				person.setTitle(wpis.get(type + "title".toUpperCase()).getStringData());
			if (cn.equalsIgnoreCase(type + "firstname"))
				person.setFirstname(wpis.get(type + "firstname".toUpperCase()).getStringData());
			if (cn.equalsIgnoreCase(type + "lastname"))
				person.setLastname(wpis.get(type + "lastname".toUpperCase()).getStringData());
			if (cn.equalsIgnoreCase(type + "organization"))
				person.setOrganization(wpis.get(type + "organization".toUpperCase()).getStringData());
			if (cn.equalsIgnoreCase(type + "street"))
				person.setStreet(wpis.get(type + "street".toUpperCase()).getStringData());
			if (cn.equalsIgnoreCase(type + "location"))
				person.setLocation(wpis.get(type + "location".toUpperCase()).getStringData());
			if (cn.equalsIgnoreCase(type + "zip"))
				person.setZip(wpis.get(type + "zip".toUpperCase()).getStringData());
			if (cn.equalsIgnoreCase(type + "country"))
				person.setCountry(wpis.get(type + "country".toUpperCase()).getData().toString());
			if (cn.equalsIgnoreCase(type + "email"))
				person.setEmail(wpis.get(type + "email".toUpperCase()).getStringData());
			if (cn.equalsIgnoreCase(type + "fax"))
				person.setFax(wpis.get(type + "fax".toUpperCase()).getStringData());
			if (cn.equalsIgnoreCase(type + "nip"))
				person.setNip(wpis.get(type + "nip".toUpperCase()).getStringData().replace("-", "").replace(" ", ""));
			if (cn.equalsIgnoreCase(type + "pesel"))
				person.setPesel(wpis.get(type + "pesel".toUpperCase()).getStringData());
			if (cn.equalsIgnoreCase(type + "regon"))
				person.setRegon(wpis.get(type + "regon".toUpperCase()).getStringData());
			if (cn.equalsIgnoreCase(type + "lparam"))
				person.setLparam(wpis.get(type + "lparam".toUpperCase()).getStringData());
			if (cn.equalsIgnoreCase(type + "organizationDivision"))
				person.setOrganizationDivision(wpis.get(type + "organizationDivision".toUpperCase()).getStringData());
			
			if (cn.equalsIgnoreCase(type + "adresskrytkiepuap"))
				person.setAdresSkrytkiEpuap(wpis.get(type + "adresskrytkiepuap".toUpperCase()).getStringData());
			if (cn.equalsIgnoreCase(type + "identifikatorepuap"))
				person.setIdentifikatorEpuap(wpis.get(type + "identifikatorepuap".toUpperCase()).getStringData());
		}
	
	}
	
	return person;
}
/**
 * Metoda tworzy kopie histori dekretacji oraz histori pisma z starego dokumentu kt鏎a do豉cza do nowego dokumentu 
 * @param doc
 * @param nowyDocument
 * @throws EdmException
 */
private void copyhistory(OutOfficeDocument doc, OutOfficeDocument nowyDocument) throws EdmException
	{
	// przepisywanie historu dekretacji 
	 for ( AssignmentHistoryEntry  entry :  doc.getAssignmentHistory()){
		 DSApi.beginTransacionSafely();
	
		 AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
		 
		 ahe.setDocumentId(nowyDocument.getId());
		 
		 if (entry.getCtime() !=null)
			 ahe.setCtime(entry.getCtime());
		 if (entry.getCdate() !=null)
			 ahe.setCdate(entry.getCdate());
		 
		 if (entry.getClerk() !=null)
			 ahe.setClerk(entry.getClerk());
		 
		 if (entry.getTargetUser() !=null)
			 ahe.setTargetUser(entry.getTargetUser());
		 
		 if (entry.getTargetGuid() !=null)
			 ahe.setTargetGuid(entry.getTargetGuid());
		 
		 if (entry.getDeadline() !=null)
			 ahe.setDeadline(entry.getDeadline());
		 
		 if (entry.getObjective() !=null)
			 ahe.setObjective(entry.getObjective());	
		
		 if (entry.getProcessType() !=null)
			 ahe.setProcessType(entry.getProcessType());
		
		 if (entry.getProcessName() !=null)
			 ahe.setProcessName(entry.getProcessName());
		 
		 if (entry.getSourceGuid() !=null)
			 ahe.setSourceGuid(entry.getSourceGuid());
		 if (entry.getSourceUser() !=null)
			 ahe.setSourceUser(entry.getSourceUser());
		 
		 if (entry.getType() !=null)
			 ahe.setType(entry.getType());
		
		nowyDocument.addAssignmentHistoryEntry(ahe);
		DSApi.context().session().saveOrUpdate(nowyDocument);
	
	}
	 
	List<Audit> listaAudit = new ArrayList<Audit>();
	listaAudit.clear();
	
	//wpis o utworzneiu pisma z multi odbiorc闚 
	Audit audNew = new Audit();
	audNew.setCtime(Calendar.getInstance().getTime());
	audNew.setDescription("Pismo utworzono z pisma  Multi Odbiorc闚 o numerze KO : "+doc.getOfficeNumber());
	audNew.setUsername(DSApi.context().getPrincipalName());
	audNew.setPriority(2);
	nowyDocument.addWorkHistoryEntry(audNew);
	//kopiowanie histori pisma starego 
	for (Audit audit :doc.getWorkHistory()){
		if( audit.getProperty()!=null && !audit.getProperty().contains("officeNumber"))
		nowyDocument.addWorkHistoryEntry(audit);
	}
	nowyDocument.setWorkHistory(listaAudit);
	
	//Odnotowanie faktu wodr瑿nienia dokument闚 z pisma 
	Audit aud = new Audit();
	aud.setCtime(Calendar.getInstance().getTime());
	aud.setDescription("Z pisma Multi Odbiorc闚 o numerze KO : "+doc.getOfficeNumber()+"  wyodrebniono nowy dokument o ID "
			+nowyDocument.getId() +" oraz nadano mu  numer KO :" +nowyDocument.getOfficeNumber());
	aud.setUsername(DSApi.context().getPrincipalName());
	aud.setPriority(2);
	doc.addWorkHistoryEntry(aud);
	DSApi.context().session().saveOrUpdate(doc);

	}

private void pierwszyOdbiorca(Map<String, Object> values, Map<String, FieldData> wpisMulti, OutOfficeDocument doc) throws EdmException
	{
	//	FieldData IdPerson = wpisMulti.get("ID");
	//	Person person = Person.findIfExist(Long.parseLong(IdPerson.getStringData()));
	//	if (person==null)
		Person person = utworzPersona(wpisMulti);
		if (doc.getRecipients().isEmpty())
			doc.addRecipient(utworzRecipient(person));
		
	values.put(_DOC_DELIVERY, String.valueOf(wpisMulti.get(_SPOSOB_DOSTARCZENIA)));
	}

	private static long calculateWychOgolnie(Long documentId) throws EdmException
	{
		PreparedStatement pst = null;
		long nr = 0;
		long podzielone = 0;
		// numer ogulnego pisma wychodzacego
		try
		{	
		/*	pst = DSApi.context().prepareStatement("SELECT count(*)  FROM DS_PAA_OUT_DOC where rok = ?");
			//	"SELECT count(*)  FROM DSO_OUT_DOCUMENT  where podzielone is null");
			pst.setInt(1, 0);
			pst.setInt(2,Calendar.getInstance().get(Calendar.YEAR));
			ResultSet rs = pst.executeQuery();

			if (rs.next())
			{
				nr = rs.getLong(1);
			}
			if (documentId == null)
				nr++;
			pst.close();  select COUNT (*) from DS_PAA_OUT_DOC where podzielone is null and rok = 2014
			pst = null;*/
			pst = DSApi.context().prepareStatement("select COUNT (*) from DS_PAA_OUT_DOC " + "where podzielone is null and rok = ?");
			pst.setInt(1,Calendar.getInstance().get(Calendar.YEAR));
			ResultSet rs  = pst.executeQuery();
			if (rs.next())
			{
				nr = rs.getLong(1);
			}
			pst.close();
			
			if (documentId == null)
				nr++;

		} catch (SQLException e)
		{
			log.error("", e);
			e.printStackTrace();
		}
		return nr; //- podzielone;
	}

	@Override
	public void onStartProcess(OfficeDocument document)
	{
	
		try {
			Map<String, Object> map= Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, document.getAuthor());
			map.put(ASSIGN_DIVISION_GUID_PARAM, document.getDivisionGuid());
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	
	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret= new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	@Override
	public void onRejectedListener(Document doc) throws EdmException
	{
	}

	@Override
	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException
	{
	}
	
	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException,
			EdmException
	{ 
		
		
	}
	
	
	
	
	public static void utworzOdbiorce(Person person,
			Map<String, FieldData> values) throws EdmException {

		try {
			PreparedStatement ps = null;
			
			ps = DSApi.context().prepareStatement(
							"INSERT INTO DS_PAA_ODBIORCA "
									+ "(DISCRIMINATOR,TITLE,FIRSTNAME,LASTNAME ,ORGANIZATION,ORGANIZATIONDIVISION,STREET "
									+ ",ZIP,LOCATION,PESEL,NIP ,REGON,COUNTRY,EMAIL ,FAX ,identifikatorEpuap,adresSkrytkiEpuap,"
									+ "basePersonId,PHONENUMBER ,KOMNUMBER ,RODZAJ_PRZESYLKI ,KOSZT_PRZESYLKI,SPOSOB_DOSTARCZENIA ,KWOTA,ANONYMOUS )"
									+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

			{
				// disrciminator
				ps.setString(1, "PERSON");
				// title
				ps.setString(2, person.getTitle());
				// firstname
				ps.setString(3, person.getFirstname());

				ps.setString(4, person.getLastname());
				ps.setString(5, person.getOrganization());
				ps.setString(6, person.getOrganizationDivision());
				ps.setString(7, person.getStreet());
				ps.setString(8, person.getZip());
				ps.setString(9, person.getLocation());
				ps.setString(10, person.getPesel());
				ps.setString(11, person.getNip());
				ps.setString(12, person.getRegon());
				ps.setString(13, person.getCountry());
				ps.setString(14, person.getEmail());
				ps.setString(15, person.getFax());
				ps.setString(16, person.getIdentifikatorEpuap());
				ps.setString(17, person.getAdresSkrytkiEpuap());
				ps.setLong(18, person.getId());
				ps.setString(19, null);
				ps.setString(20, null);
				ps.setString(21, null);
				ps.setString(22, null);
				ps.setString(23, null);
				ps.setString(24, null);
				ps.setInt(25, 0);
				ps.execute();
				
				
				ps.close();
				DSApi.context().commit();
				// if (rs.next()){
				// kwota = rs.getDouble(1);

				// }
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(" ", e);
			throw new EdmException(
					"INSERT INTO DS_PAA_ODBIORCA B豉d ");
		}
	}

	@Override
	public void addWeightAndCostsLettersSelectedByValues(OutOfficeDocument document, Integer rodzajGabaryt, boolean listyKrajowe, boolean listEkonomiczny,
			HashMap<Integer, Map<Integer, BigDecimal>> mapaKosztow, Map<Integer, String> mapaRodzajowKraj,
			HashMap<Integer, Map<Integer, Integer>> tempIloscPoszczegolnychWag)
	{

		Integer rodzaj;
		Map<Integer, Integer> rodzajeWagC = getTemMapC();
		Map<Integer, Integer> rodzajeWagW = getTemMapW();
		FieldsManager fm = document.getFieldsManager();
		if (fm != null)
		{
		
			Map<Integer, Integer> tempIlosctWagMap = tempIloscPoszczegolnychWag.get(rodzajGabaryt);
			Map<Integer, BigDecimal> tempKosztMap = mapaKosztow.get(rodzajGabaryt);

			if (tempKosztMap != null && tempIlosctWagMap != null)
			{
				try
				{
					if (fm.getValue(_rodzajPrzesylkiWagaKrajowa) != null && fm.getEnumItem(_rodzajPrzesylkiWagaKrajowa).getId() != null)
					{
						rodzaj = fm.getEnumItem(_rodzajPrzesylkiWagaKrajowa).getId();
						if (rodzaj != null)
						{
							tempIlosctWagMap.put(rodzajeWagC.get(rodzaj), tempIlosctWagMap.get(rodzajeWagC.get(rodzaj)) + 1);
							if (document.getStampFee() != null)
							{
								BigDecimal tempBd = tempKosztMap.get(rodzajeWagC.get(rodzaj));
								BigDecimal bd = document.getStampFee();
								bd = bd.add(tempBd);
								tempKosztMap.put(rodzajeWagC.get(rodzaj), bd);
							}
						}

					}

				} catch (EdmException e)
				{
					log.error("", e);
				}
			}
		}


	}
	
	private void getGabaryty()
	{
		this.gabaryty.put(1, 1);
		this.gabaryty.put(2, 2);
		this.gabaryty.put(3, 1);
		this.gabaryty.put(4, 2);
		this.gabaryty.put(5, 3);
		this.gabaryty.put(6, 4);
		this.gabaryty.put(7, 1);
		this.gabaryty.put(8, 2);
	}
	private Map<Integer, Integer> getTemMapC()
	{
		Map<Integer, Integer> rodzajeWagC = new HashMap();
		rodzajeWagC.put(1, 1);
		rodzajeWagC.put(4, 1);
		rodzajeWagC.put(2, 2);
		rodzajeWagC.put(5, 2);
		rodzajeWagC.put(3, 3);
		rodzajeWagC.put(6, 3);
		return rodzajeWagC;
		
	}
	
	private Map<Integer, Integer> getTemMapW()
	{
		Map<Integer, Integer> rodzajeWagW = new HashMap();

		rodzajeWagW.put(1, 1);
		rodzajeWagW.put(7, 1);
		rodzajeWagW.put(13, 1);
		rodzajeWagW.put(19, 1);
		rodzajeWagW.put(2, 2);
		rodzajeWagW.put(8, 2);
		rodzajeWagW.put(14, 2);
		rodzajeWagW.put(20, 2);
		rodzajeWagW.put(3, 3);
		rodzajeWagW.put(9, 3);
		rodzajeWagW.put(15, 3);
		rodzajeWagW.put(21, 3);
		rodzajeWagW.put(4, 4);
		rodzajeWagW.put(10, 4);
		rodzajeWagW.put(16, 4);
		rodzajeWagW.put(22, 4);
		rodzajeWagW.put(5, 5);
		rodzajeWagW.put(11, 5);
		rodzajeWagW.put(17, 5);
		rodzajeWagW.put(23, 5);
		rodzajeWagW.put(6, 6);
		rodzajeWagW.put(12, 6);
		rodzajeWagW.put(18, 6);
		rodzajeWagW.put(24, 6);

		return rodzajeWagW;
		
	}

}
	