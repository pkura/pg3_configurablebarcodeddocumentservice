package pl.compan.docusafe.parametrization.paa;

import java.util.Calendar;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class CzyOdmowiono implements DecisionHandler
{
	private static final Logger log = LoggerFactory.getLogger(CzyOdmowiono.class);

	public String decide(OpenExecution openExecution)
	{
		String data = Calendar.getInstance().getTime().toString();
		
		log.info("CzyOdmowiono");
		
		String decision = "nie";
		 Object o =  openExecution.getVariable("odmowa");
		 
		 if (o.toString().equals("1"))
		 	decision = "tak";
		 	
		 log.info("czas zajscia procesu "+data +" Odmuwiono -> "+decision);
		 	return decision;
	}
}
			
				
			
		