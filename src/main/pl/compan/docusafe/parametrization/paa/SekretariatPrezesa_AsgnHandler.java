package pl.compan.docusafe.parametrization.paa;

import java.util.Calendar;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** asygnuje na sekretariat prezesa */
@SuppressWarnings("serial")
public class SekretariatPrezesa_AsgnHandler implements AssignmentHandler {
	private final static Logger log= LoggerFactory.getLogger(SekretariatPrezesa_AsgnHandler.class);
	@Override
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
		log.info("SekretariatPrezesa_AsgnHandler"); 
		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		String data = Calendar.getInstance().getTime().toString();
		String rodzajDokumentu = doc.getDocumentKind().getCn();
		log.info("Dokument  id = "+docId+"Rodzaj = " +rodzajDokumentu+ " czas zajscia procesu "+data);
		
		
		//E2329459-60E8-482D-8776-46412DCB9FFE
		String SekretariatPrezesa = "gajda";
		String asygnowanyDzial ="E2329459-60E8-482D-8776-46412DCB9FFE";
		
		assignable.addCandidateGroup(asygnowanyDzial);
		assignable.addCandidateUser(SekretariatPrezesa);
		assignable.addCandidateUser("igareszke");
		
		
		log.info("SekretariatPrezesa_AsgnHandler -- dekretuje na  Igareszke,  gabinet prezesa  oraz na "+SekretariatPrezesa); 
		//String asygnowanyUzytkownik = ".....okreslic......";
		
		AssignmentHistoryEntry entry = new AssignmentHistoryEntry();
		String dzialNad =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(DSApi.context().getDSUser().getName());
		entry.setSourceGuid(dzialNad);
		entry.setSourceUser(DSApi.context().getDSUser().getName());
		entry.setTargetGuid(asygnowanyDzial);
		String statusDokumentu =  doc.getFieldsManager().getEnumItem("STATUSDOKUMENTU").getTitle();
		entry.setObjective(statusDokumentu);
		entry.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
		doc.addAssignmentHistoryEntry(entry);
		
		//dekretowanie
		//assignable.addCandidateUser(asygnowanyUzytkownik);
		
		// dodac recznie do historii (w WSSK robilem to tak:) 
		// Util_AHE.addToHistory(openExecution, doc, asygnowanyUzytkownik, null, null, log);
	}
}
