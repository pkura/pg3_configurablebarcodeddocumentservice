package pl.compan.docusafe.parametrization.paa;


import java.util.Calendar;
import java.util.Date;

import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;

public class Listener_Akceptacja extends AbstractEventListener {
	
	public void notify(EventListenerExecution eventListenerExecution) throws Exception {
		//eventListenerExecution.setVariable("odmowa", 1); //ktos odm�wi�
		String userName =  DSApi.context().getPrincipalName();
		DSUser user = DSUser.findByUsername(userName);
		
		Long docId = Long.valueOf( eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		Date date = (Date) Calendar.getInstance().getTime();
		DateUtils data = new DateUtils();
    	String dataa = data.formatCommonDateTime(date);
	       doc.addWorkHistoryEntry(Audit.create(DSApi.context().getPrincipalName(),user.getName(),
	                "Zast�pstwo zosta�o zaakceptowane przez : "+user.asLastnameFirstname() + " w dniu : " +dataa));
    }
}