package pl.compan.docusafe.parametrization.paa;

import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;
import org.jbpm.api.task.Participation;
import org.jbpm.api.task.Task;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** Asygnuje dokument na osobe wskazana jako nadawca */
public class Nadawca_AsgnHandler  implements AssignmentHandler {
	
	private String poleNadawcy; //to jest pole typu private ale mimo to pojawi si� tu warto�� z zewn�trz.
	// JBPM wstawi tu warto�� podan� w XML:
	// <field name="poleNadawcy"><string value="nazwa pola Nadawca"/></field>
	// czyli wpisze warto�� "nazwa pola Nadawca" do pola "poleNadawcy" powy�ej.
	// Nale�y to traktowa� jako dodatkowy argument wywo�ania asygnatora (tej metody w tej klasie) 

	private static final Logger log = LoggerFactory.getLogger(Nadawca_AsgnHandler.class);
	
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
		
		log.info("Nadawca_AsgnHandler");
		
		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		
		
		String data = Calendar.getInstance().getTime().toString();
		String rodzajDokumentu = doc.getDocumentKind().getCn();
		log.info("Dokument  id = "+docId+"Rodzaj = " +rodzajDokumentu+ " czas zajscia procesu "+data);
		
		String useridstl=fm.getStringKey(poleNadawcy);
		log.info("pobra��m user id = " +useridstl);
		long userId = 0;
		try{
			userId = Long.parseLong(useridstl);
		} catch (NumberFormatException nfe){
			log.info("podane ID u�ytkownika (" +useridstl+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
			throw new IllegalStateException("podane ID u�ytkownika (" +useridstl+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
		}
		
		Person person =Person.find(userId);
		String personfirstname = person.getFirstname();
		String personLastname = person.getLastname();
		DSUser user = DSUser.findByFirstnameLastname(personfirstname, personLastname);
		//dekretowanie
		assignable.addCandidateUser(user.getName());
		
		log.info("Nadawca_AsgnHandler dekretuje na " +user.getName());
		
		AssignmentHistoryEntry entry = new AssignmentHistoryEntry();
		entry.setSourceUser(DSApi.context().getDSUser().getName());
		entry.setTargetUser(user.getName());
		String dzialNad =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(DSApi.context().getDSUser().getName());
		entry.setSourceGuid(dzialNad);
		String dzialOdb =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(user.getName());
		entry.setTargetGuid(dzialOdb);
		String statusDokumentu =  doc.getFieldsManager().getEnumItem("STATUSDOKUMENTU").getTitle();
		entry.setObjective(statusDokumentu);
		entry.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
		doc.addAssignmentHistoryEntry(entry);
		// dodac recznie do historii (w WSSK robilem to tak:) 
		// Util_AHE.addToHistory(openExecution, doc, asygnowanyUzytkownik, null, null, log);
	}
}
