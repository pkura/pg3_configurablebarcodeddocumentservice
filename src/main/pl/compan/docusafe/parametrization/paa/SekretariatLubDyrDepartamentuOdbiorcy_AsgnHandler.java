package pl.compan.docusafe.parametrization.paa;

import java.util.Calendar;
import java.util.logging.ErrorManager;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;
import com.opensymphony.xwork.ActionSupport;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** Asygnuje dokument na Sekretariat i dyrektora  Departamentu lub Dyrektora Departamentu */
@SuppressWarnings("serial")
public class SekretariatLubDyrDepartamentuOdbiorcy_AsgnHandler implements AssignmentHandler {
	private final static Logger log= LoggerFactory.getLogger(SekretariatLubDyrDepartamentuOdbiorcy_AsgnHandler.class);
	
	private String poleOdbiorcy; //to jest pole typu private ale mimo to pojawi si� tu warto�� z zewn�trz.
	
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
		
		log.info("SekretariatLubDyrDepartamentuOdbiorcy_AsgnHandler");
		
		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();

		String useridstl=fm.getStringKey(poleOdbiorcy);
		log.info("pobra�em user id = " +useridstl);
		long userId = 0;
		try{
			userId = Long.parseLong(useridstl);
		} catch (NumberFormatException nfe){
			log.info("podane ID u�ytkownika (" +useridstl+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
			throw new IllegalStateException("podane ID u�ytkownika (" +useridstl+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
		}
		
		Person person =Person.find(userId);
		String personfirstname = person.getFirstname();
		String personLastname = person.getLastname();
		DSUser user = DSUser.findByFirstnameLastname(personfirstname, personLastname);

		
		//dekretacja na dyrektora departamentu 
		
		String  dyrektor ="";
		String  sekretariat ="";
		
		
		boolean odbiorcaToDyr = (Boolean) openExecution.getVariable("odbiorcaToDyrDept");
		//sprawdza czy nadawca jest pacownikiem sekretariatu 
		boolean pracSekretariatu = NormalLogic.czyUserToPracSekretariatu(DSApi.context().getDSUser().getName());
		boolean dekretacja = false;
		
		log.info("SekretariatLubDyrDepartamentuOdbiorcy_AsgnHandler " +
				" Nadawca to ->" +DSApi.context().getDSUser().getName()+
				"  Odbiorca to ->  " +user.getName());
		
		
		// jezeli odbiorc� jesy sobiecka topisma id� tylk odo niej 
		if(user.getName().equals("sobiecka")){
			assignable.addCandidateUser(user.getName());
			dyrektor = user.getName();
			dekretacja = true;
		}
		//jesli odbiorca to  prezes vceprezes dekretujemy na prezesa i pracownika sekretariatu kt�rym jest reszka na sztywno 
		else if(user.getName().equals("wlodarski") || user.getName().equals("jurkowski")){
			log.info("SekretariatLubDyrDepartamentuOdbiorcy_AsgnHandler -Odbiorca to prezes lub viceeprezes - Dekretuj� na  igareszke oraz na "+user.getName());
			assignable.addCandidateUser("igareszke");
			assignable.addCandidateUser(user.getName());
			dyrektor = user.getName();
			dekretacja = true;
			
			// jezeli nadawc� jest pracownik sekretariatu 
		} else if (odbiorcaToDyr) {
			
			log.info("SekretariatLubDyrDepartamentuOdbiorcy_AsgnHandler - Sprawdzam czy  Nadawca  -> " +DSApi.context().getDSUser().getName()+" to pracownik sekretariatu dyrektora -> "+user.getName() );
			 sekretariat = NormalLogic.getUserPelniocyRoleSekretariatu(user.getName());
			 // czy nadawca to pracownik sekretariatu dyrektora na kt�rego jest dekretacja
			 	if (DSApi.context().getDSUser().getName().equals(sekretariat)){
			 		log.info(" nadawca to pracownik sekretariatu dyrektora na kt�rego jest dekretacja  dyrektor -> "+user.getName() +"Dekretuje tylko Na Dyrektora");
			 		assignable.addCandidateUser(user.getName());
			 		dyrektor = user.getName();
			 		dekretacja = true;
			 		
			 	} else{
			 	// jezeli nadawca jest pacownikiem sekretariatu innego dyrektora niz odbiorca to idzie do sekretariatu i dyrektora 
				
			 		 if (user.getName().equals("kaczynska"))
			 				sekretariat = "gajda";
			 		 
			 	assignable.addCandidateUser(user.getName());
				assignable.addCandidateUser(sekretariat);
				dyrektor = user.getName();
				dekretacja = true;
				log.info(" nadawca to pracownik sekretariatu INNEGO  dyrektora na kt�rego jest dekretacja  dyrektor -> "+user.getName() );
				log.info("dekretuj� na  dyrektor -> "+user.getName()+" Oraz na Pracownika sekretariatu "+ sekretariat);
		}
		
		
		
			 //odbiorca to dyrektor  departamentu ale nie prezez lub vceprezes 
		
/*			if (!dekretacja){
				
			log.info("Wyszukuje dyrektora departamentu u�ytkownika "+user.getName());
			   dyrektor = user.getName();
				if(dyrektor.equals(user.getName())){
					log.info("Nie znalaz�em dyrektora departamentu  wyszukuj� po dziale u�ytkownika "+user.getName()+"- pracownika sekretariatu ");
					
					 sekretariat = NormalLogic.getUserPelniocyRoleSekretariatu(user.getName());
					 if(sekretariat.equals(user.getName())){
							log.info("Nie znalaz�em  pracownika sekretariatu po dziale u�ytkownika "+user.getName()+" Dekretuje na u�ytkownika");
							assignable.addCandidateUser(user.getName());
					 }else {
						 log.info(" dekretuj� na Sekretariat" +sekretariat + "oraz na u�ytkownika " +user.getName());
						 assignable.addCandidateUser(sekretariat);
						 assignable.addCandidateUser(user.getName());
					 }
					 
					
				}else {
					dyrektor = user.getName();
					log.info("Znalaz�em dyrektora "+dyrektor+ " Pobieram Pracownika sekretariatu po dyrektorze");
					 sekretariat = pl.compan.docusafe.parametrization.paa.NormalLogic.getUserPelniocyRoleSekretariatu(dyrektor);
					 
					 if(sekretariat.equals(dyrektor)){
							log.info("Nie znalaz�em  pracownika sekretariatu po Dyrektorze "+user.getName()+" Dekretuje na Dyrektora"+user.getName());
							assignable.addCandidateUser(user.getName());
					 }else {
						 log.info(" dekretuj� na Sekretariat" +sekretariat + "oraz na u�ytkownika " +user.getName());
							assignable.addCandidateUser(sekretariat);
							assignable.addCandidateUser(user.getName());
					 }
				}
		}*/
		}
		
		AssignmentHistoryEntry entry = new AssignmentHistoryEntry();
		
		entry.setSourceUser(DSApi.context().getDSUser().getName());
		entry.setTargetUser(dyrektor);
		String dzialNad =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(DSApi.context().getDSUser().getName());
		entry.setSourceGuid(dzialNad);
		String dzialOdb =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(dyrektor);
		entry.setTargetGuid(dzialOdb);
		String statusDokumentu =  doc.getFieldsManager().getEnumItem("STATUSDOKUMENTU").getTitle();
		entry.setObjective(statusDokumentu);
		entry.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
		doc.addAssignmentHistoryEntry(entry);
		
	}
}
		
			
		/*}
			log.info("SekretariatLubDyrDepartamentuOdbiorcy_AsgnHandler - Wyszukuje dyrektora departamentu u�ytkownika "+user.getName());
			
			 dyrektor = pl.compan.docusafe.parametrization.paa.NormalLogic.getDyrektorDepartamentu(user.getName() ,doc);
		
		log.info("SekretariatLubDyrDepartamentuOdbiorcy_AsgnHandler -  Znalaz�em dyrektora"+dyrektor+ "Pobieram Pracownika sekretariatu po dyrektorze");
		String sekretariat = pl.compan.docusafe.parametrization.paa.NormalLogic.getUserPelniocyRoleSekretariatu(dyrektor);
		
		log.info("SekretariatLubDyrDepartamentuOdbiorcy_AsgnHandler - sprawdzam czy znalaz�em pracownika innego ni� dyrektor");
		if (sekretariat.contains(dyrektor)){
			log.info("SekretariatLubDyrDepartamentuOdbiorcy_AsgnHandler - Dyrektor i pracownik  to ta sam osoba - wyszukuj� po dziale - pracownika sekretariatu ");
			 sekretariat = pl.compan.docusafe.parametrization.paa.NormalLogic.getUserPelniocyRoleSekretariatu(user.getName());
			
			 if (sekretariat.contains(dyrektor)){
				 log.info("SekretariatLubDyrDepartamentuOdbiorcy_AsgnHandler- Nieznalaz�em pracownika sekretariatu dekretuj� na "+dyrektor);
				 assignable.addCandidateUser(dyrektor);
			 }
		}else {
			log.info(" SekretariatLubDyrDepartamentuOdbiorcy_AsgnHandler - dekretuj� na " +sekretariat +"oraz na dyrektora"+dyrektor );
			assignable.addCandidateUser(sekretariat);
			assignable.addCandidateUser(dyrektor);
		}
		*/
		//albo
		//assignable.addCandidateGroup(guidAsygnowanegoDzialu); //podac guid, np. guidAsygnowanegoDzialu = "agdajkdgadgajksgdak"
		
		
		// dodac recznie do historii (w WSSK robilem to tak:) 
		// Util_AHE.addToHistory(openExecution, doc, asygnowanyUzytkownik, null, null, log);
	

