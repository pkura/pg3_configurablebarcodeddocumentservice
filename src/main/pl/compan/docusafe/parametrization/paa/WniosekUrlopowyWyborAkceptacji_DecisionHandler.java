package pl.compan.docusafe.parametrization.paa;

import java.util.ArrayList;
import java.util.Calendar;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class WniosekUrlopowyWyborAkceptacji_DecisionHandler implements DecisionHandler
{
	private static final Logger log = LoggerFactory.getLogger(WniosekUrlopowyWyborAkceptacji_DecisionHandler.class);
	private String dyrektory = "DYR";
	private String naczelnicy = "NAC";
	private String departamenty = "DEP";
	private String gabinetPrezesa = "GP";
	
	//mozliwe decyzje 
	private String _dyrGeneralny = "dyrGeneralny";
	private String _prezes = "prezes";
	private String _wiceprezes = "wiceprezes";
	private String _dyrektor = "dyrektor";
	private String _naczelnik = "naczelnik";
	
	
	
	

	public String decide(OpenExecution openExecution)
	{
		log.info("WniosekUrlopowyWyborAkceptacji_DecisionHandler");

		String decision = "autor";
		
		/**
		 * 1. sprawdz jaki departament, jesli bezpiecz. jadrowego to bierze pod uwage naczelnika
		 */
		
		
		
		
		boolean isDepartBezpJadr = false;
		
		
		log.info("WniosekUrlopowyWyborAkceptacji_DecisionHandler ");
		try {
			Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			//FieldsManager fm = doc.getFieldsManager();
			DSUser user =DSUser.findByUsername(doc.getAuthor());
			
			String data = Calendar.getInstance().getTime().toString();
			String rodzajDokumentu = doc.getDocumentKind().getCn();
			log.info("Dokument  id = "+docId+"Rodzaj = " +rodzajDokumentu+ " czas zajscia procesu "+data);
			
			for (DSDivision divodb : user.getDivisionsAsList()) {
				DSDivision currentOdb =divodb;
				while (currentOdb != null){
					 if(currentOdb.getGuid().equals("C51B4836-0EFD-4878-8263-7C2BF733B21C")	// -Departament Bezpiecze�stwa J�drowego
							 || currentOdb.getGuid().equals("D73ACC0C-9AED-4BED-B3C7-0CC944494AA9")//Centrum ds. Zdarze� Radiacyjnych
							 || currentOdb.getGuid().equals("71B13FC1-1B94-4851-8958-1C4BCADA3FFB")) // departament ochrony radiacyjnej
					 {
						 isDepartBezpJadr = true;
						break;
					} else {
						currentOdb= currentOdb.getParent();
					}
			}
			}
			
			 
			
			/*String useridstl=fm.getStringKey("author");
			log.info("pobra��m user id = " +useridstl);
			long userId = 0;
			try{
				userId = Long.parseLong(useridstl);
			} catch (NumberFormatException nfe){
				log.info("podane ID u�ytkownika (" +useridstl+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
				throw new IllegalStateException("podane ID u�ytkownika (" +useridstl+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
			}
			
			Person person =Person.find(userId);
			String personfirstname = person.getFirstname();
			String personLastname = person.getLastname();
			DSUser user = DSUser.findByFirstnameLastname(personfirstname, personLastname);
			//dekretowanie
			assignable.addCandidateUser(user.getName());
			
			log.info("Nadawca_AsgnHandler dekretuje na " +user.getName());*/
			
			
			
			
			
			
			
			
			/**
			 * 2. sprawdz czy wnioskodawca to pracownik czy dyrektor
			 */
			
			boolean isPracownik = !CzyOdbiorcaToDyrDepartamentu_DecisionHandler.czyUserToDyr(user);
		
			
			/**
			 * 3. sprawdz czy prezes jest obecny
			 */
			boolean isPrezesObecny= true;
			boolean isWicePrezesObecny= true;
			
			
			openExecution.setVariable("wniosekViceprezesa", false);
			openExecution.setVariable("wniosekDyrGEneralnego", false);
			openExecution.setVariable("wniosekNaczDBJ", false);
			openExecution.setVariable("wniosekPrezesa", false);
			openExecution.setVariable("wniosekPracStrukturaBDG", false);
			
			// urzytkownik gabinetu prezesa 
			openExecution.setVariable("UzytkownikGP", false);
			
			/*uzytkownik nie figuruj�cy w strukturze wydzia�owej
			oznacza pracownika, kt�ry jest pracownikiem departamentu podzielonego na wydzia�y,
			ale nie jest przypisany do �adnego z tych wydzia��w.*/
			openExecution.setVariable("wniosekNieFigWStrukturze", false);
			
			
			if (user.getName().equals("wlodarski")){
				openExecution.setVariable("wniosekPrezesa", true);
					decision= _dyrGeneralny;
					return decision;
			}
				
			if (user.getName().equals("czarnecki")) {

				openExecution.setVariable("wniosekDyrGEneralnego", true);
				if (isPrezesObecny) {
					decision= _prezes;
					return decision;
				} else {
					decision= _wiceprezes;
					return decision;
				}

			}
			if (user.getName().equals("jurkowski")){
				openExecution.setVariable("wniosekViceprezesa", true);
					decision= _prezes;
					return decision;
				
			}//audytor wewnetrzny jak dyrektor wniosek
			if (user.getName().equals("klepacz")){
				decision= _prezes;
				return decision;
			}
			//
			
			/**
			 * podejmij decyzje
			 */
			if (!isPracownik) {
				// dyrektor
				if (isDepartBezpJadr) {

					// depart bezpieczenstwa lub depart centrum zdarzen radiac.
					if (isWicePrezesObecny){
						decision= _wiceprezes;
						return decision;
					}
					else{
						decision= _prezes;
						return decision;
					}
				} else {
					if (isPrezesObecny){
						decision= _prezes;
						return decision;
					}
					else{
						decision= _wiceprezes;
					return decision;
					}
				}
			} else {
				// Sciezka pracownik�w pracownik pod DBJ
				if (isDepartBezpJadr) {
					// user to naczelnik - naczelnicy DBJ
					String naczelnicyPodDbj = 	Docusafe.getAdditionProperty("PAA.naczelnicyDBJ");
					for (String naczelnik : naczelnicyPodDbj.split(","))
					{
						if (user.getName().equals(naczelnik)){
							decision= _dyrektor;
							openExecution.setVariable("wniosekNaczDBJ", true);
							return decision;
							}
					}
					boolean isBezposrednioWdepartamencie= pl.compan.docusafe.parametrization.paa.NormalLogic
								.isBezposrednioWdepartamencie(user);
					if (isBezposrednioWdepartamencie) {
							decision= _dyrektor;
							openExecution.setVariable("wniosekNieFigWStrukturze", true);
					} else{
						// jezeli nie jest bezposrednio w departamencie to do naczelnika
						decision= _naczelnik;
					}
					
				} else {
					//pracownicy pozaDBJ
					 //pracownicy w strukturze pod biurem dyrektora generalnego
					String pracBDGwStrukturze = Docusafe.getAdditionProperty("PAA.pracStrukturaBDG");
					for (String naczelnik : pracBDGwStrukturze.split(","))
					{
						if (user.getName().equals(naczelnik)){
							decision= _naczelnik;
							openExecution.setVariable("wniosekPracStrukturaBDG", true);
							return decision;
							}
					}
					// uzytkownicy nie w DBJ
					//naczelnicy w GP 
					String naczelnicyGP = 	Docusafe.getAdditionProperty("PAA.naczelnicyGP");
					for (String naczelnik : naczelnicyGP.split(","))
					{
						if (user.getName().equals(naczelnik)){
							decision= _dyrektor;
							return decision;
							}
					}
					
					//POzostali pracownikcy
					if (!user.getDivisionsAsList().isEmpty()) {
						
						DSDivision[] div= user.getDivisions();
						for (DSDivision d : div) {
							// jezeli user to naczelnik i jest w Gabinecie prezesa
							if (d.getCode() != null && d.getCode().equals(gabinetPrezesa) && d.getDescription() != null
									&& d.getDescription().equals(naczelnicy)) {
								decision= _dyrektor;
								openExecution.setVariable("UzytkownikGP", true);
								break;

								// jezeli user to nie naczelnik
							} else if (d.getCode() != null && d.getCode().equals(gabinetPrezesa)) {
								decision= _naczelnik;
								// user to nie naczelnik - czy jest bezposrednio w departamencie - tak -> do _dyrektora
								boolean isBezposrednioWdepartamencie= pl.compan.docusafe.parametrization.paa.NormalLogic
										.isBezposrednioWdepartamencie(user);
								if (isBezposrednioWdepartamencie) {
									decision= _dyrektor;
									openExecution.setVariable("wniosekNieFigWStrukturze", true);
									break;
								}else{
								openExecution.setVariable("UzytkownikGP", true);
								break;
								}
							} else if (d.getParent() != null && d.getParent().getDescription()!=null && d.getParent().getDescription().equals(departamenty) ){
								decision= _naczelnik;
								openExecution.setVariable("UzytkownikGP", true);
							}
							// u�ytkownik nie w gabinecie prezesa i nie bedacy w dbj
							else {
								 if (decision.equals("autor"))
									 decision= _dyrektor;
							}
						}
					} else
						decision= _dyrektor;
				}
			} 
		} catch (Exception e) {
			// TODO: handle exception
		}

		// dyrektor, prezes, wiceprezes, naczelnik
		return decision;
	}
}
