package pl.compan.docusafe.parametrization.paa;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.absences.Absence;
import pl.compan.docusafe.core.base.absences.AbsenceType;
import pl.compan.docusafe.core.calendar.CalendarFactory;

import pl.compan.docusafe.core.dockinds.FieldsManager;

import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;

import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.SubstituteHistory;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class Wpisanie_urlopu implements ExternalActivityBehaviour
{

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;

	private static final String _FROM = "FROM";
	private static final String _TO = "TO";
	private static final String _WORKING_DAYS_CN = "WORKING_DAYS_CN";
	private static final String _KIND = "KIND";
	private static final String _DOC_DESCRIPTION = "DOC_DESCRIPTION";

	private int iloscOsobZastepujacych = 0;
	private List<String> usernames = new ArrayList();

	private static final Map<Integer, String> absenceTypes = new HashMap<Integer, String>();

	static
	{
		absenceTypes.put(10, "UW");
		absenceTypes.put(20, "U�");
		absenceTypes.put(25, "UBZ");
		absenceTypes.put(30, "188");
		absenceTypes.put(40, "OG");
		absenceTypes.put(50, "UOK");
		absenceTypes.put(60, "UB");
		absenceTypes.put(70, "INW");
		absenceTypes.put(80, "UM");
		absenceTypes.put(90, "UMD");
		absenceTypes.put(100, "UJ");
		absenceTypes.put(110, "NN");
		absenceTypes.put(120, "NUN");
		absenceTypes.put(130, "NUP");
		absenceTypes.put(140, "US");
	}

	/**
	 * Logger
	 */
	private static final Logger log = LoggerFactory.getLogger(Wpisanie_urlopu.class);


	@SuppressWarnings("deprecation")
	public void notify(OpenExecution openExecution) throws Exception
	{
		Long docId = Long.parseLong(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		log.info("Wpisanie_urlopu - doc id = " + docId);

		// pobranie obiektu dokumentu
		Document document = Document.find(docId);

		// pobranie wnioskodawcy
		UserImpl userWnioskujacy = (UserImpl) DSUser.findByUsername(document.getAuthor());

		EmployeeCard empCard = getEmployeeCard(userWnioskujacy);
		if (empCard == null)
			throw new IllegalStateException("Brak karty pracownika! Nie mo�na przypisa� urlopu!");

		int year = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
		if (!hasChargedAbsencesInCurrentYear(year, empCard))
			throw new IllegalStateException("Brak przypisanego nale�nego wymiaru urlopowego na bierz�cy rok!");

		// utworzenie wpisu urlopu
		Absence absence = new Absence();

		absence.setYear(year);
		absence.setEmployeeCard(empCard);

		// ustawienie dat
		Date from = (Date) document.getFieldsManager().getValue(_FROM);
		Date to = (Date) document.getFieldsManager().getValue(_TO);
		to.setHours(23);
		to.setMinutes(59);

		absence.setStartDate(from);
		absence.setEndDate(to);

		// ustawienie typu urlopu
		int typeId = document.getFieldsManager().getField(_KIND).getEnumItemByTitle((String) document.getFieldsManager().getValue(_KIND)).getId();
		AbsenceType absenceType = Finder.find(AbsenceType.class, absenceTypes.get(typeId));
		absence.setAbsenceType(absenceType);

		// ustawienie liczby dni
		int numDays = (Integer) (document.getFieldsManager().getValue(_WORKING_DAYS_CN));
		absence.setDaysNum(numDays);

		// ustawienie opisu
		absence.setInfo((String) document.getFieldsManager().getValue(_DOC_DESCRIPTION));

		// data utworzenia wpisu
		absence.setCtime(new Date());
		absence.setDocumentId(document.getId());
		//osoba wnioskuj�ca 
		absence.setEditor(userWnioskujacy);
		//absence.setEditorFullName(userWnioskujacy.asFirstnameLastname());
		empCard.getAbsences().add(absence);
		absence.setEmployeeCard(empCard);


		// zapisanie do bazy
		DSApi.context().session().update(empCard);
		DSApi.context().session().save(absence);
		if (AvailabilityManager.isAvailable("absence.createEventOnCreate"))
		{
			CalendarFactory.getInstance().createEvents(userWnioskujacy, from, to, "[ Na urlopie ]", absence.getId());
		}
		//Zastepstwa

		log.info("Wpisanie_urlopu - ZbiorZastepcow");

		FieldsManager fm = document.getFieldsManager();
		String data = Calendar.getInstance().getTime().toString();
		String rodzajDokumentu = document.getDocumentKind().getCn();
		log.info("Dokument  id = " + docId.toString() + "Rodzaj =" + rodzajDokumentu + " czas zajscia procesu " + data);



		if (fm.getStringKey("ZASTEPSTWO") != null)
		{
			String useridstl = fm.getStringKey("ZASTEPSTWO");
			String[] ids;
			if (useridstl.startsWith("["))
			{
				useridstl = useridstl.substring(1, useridstl.length() - 1);
				ids = useridstl.split(",");
			} else
			{
				ids = new String[1];
				ids[0] = useridstl;
			}
			usernames.clear();
			iloscOsobZastepujacych = 0;

			for (String id : ids)
			{

				PreparedStatement pst = null;
				Date zastOd = null;
				Date zastDo = null;
				String Userid;
				pst = DSApi.context().prepareStatement(" select title ,zast_period_from ,zast_period_to from DS_PAA_WNIOSEK_ZAST where id = ?");
				pst.setLong(1, Long.parseLong(id.trim()));
				ResultSet rs = pst.executeQuery();

				if (rs.next())
				{
					Userid = rs.getString(1);
					zastOd = rs.getDate(2);
					zastDo = rs.getDate(3);
					// zastDo.setMinutes(59);
					// zastDo.setHours(23);

					Calendar cal = Calendar.getInstance();
					cal.setTime(zastDo);
					cal.set(Calendar.HOUR_OF_DAY, 23);
					cal.set(Calendar.MINUTE, 59);
					DSUser user = DSUser.findById(Long.parseLong(Userid));

					//dodanie zastepstwa
					//userWnioskujacy.setSubstitution(user, zastOd , zastDo);
					//wpisanie do histori zastepstw
					SubstituteHistory history = new SubstituteHistory(userWnioskujacy.getName(), zastOd, cal.getTime(), user.getName(),DSApi.context().getDSUser().getName());
					DSApi.context().session().save(history);



				}
				pst.close();

			}
			//	DSApi.context().commit();
			//jezeli zastepstwo jest na dzisiaj to od razu wpisujemy do ds_user 

			Date now = new Date();
			if (now.after(from))
			{
				//DSApi.context().begin();
				List<SubstituteHistory> history2 = SubstituteHistory.listActualSubstitutes(userWnioskujacy.getName());
				// TO DOOO  cos sie nie wpisuja wpisy do histori 

				if (!history2.isEmpty())
				{

					Collections.sort(history2, new Comparator<SubstituteHistory>() {

						public int compare(SubstituteHistory o1, SubstituteHistory o2)
						{
							return o1.getSubstitutedFrom().before(o2.getSubstitutedFrom()) ? -1 : 1;
						}
					});
				}




				userWnioskujacy.setSubstitutionFromHistory(history2.get(0));
				DSApi.context().session().save(history2.get(0));
			}

		}

	}

	/**
	 * Sprawdza czy w danym roku jest przypisany nale�ny urlop dla karty pracownika
	 * 
	 * @param year
	 * @param empCard
	 * @return
	 * @throws Exception
	 */
	private boolean hasChargedAbsencesInCurrentYear(int year, EmployeeCard empCard) throws Exception
	{
		String hql = "select 1 from " + Absence.class.getName() + " a where a.year = ? and a.absenceType.code like ? and a.employeeCard = ?";
		Integer has = (Integer) DSApi.context().session().createQuery(hql).setInteger(0, year).setString(1, "WUN").setParameter(2, empCard).setMaxResults(1)
				.uniqueResult();
		if (has == null)
			return false;

		return true;
	}

	/**
	 * Zwraca kart� pracownika
	 * 
	 * @param userImpl
	 * @return
	 * @throws Exception
	 */

	private EmployeeCard getEmployeeCard(UserImpl userImpl) throws Exception
	{
		return (EmployeeCard) DSApi.context().session().createQuery("from " + EmployeeCard.class.getName() + " e where e.user = ?").setParameter(0, userImpl)
				.setMaxResults(1).uniqueResult();
	}

	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		notify(execution);

		if (execution.getActivity().getDefaultOutgoingTransition() != null)
			execution.takeDefaultTransition();

	}

	@Override
	public void signal(ActivityExecution execution, String signalName, Map<String, ?> arg2) throws Exception
	{
		if (signalName != null)
			execution.take(signalName);
		else
			execution.takeDefaultTransition();
	}
}
