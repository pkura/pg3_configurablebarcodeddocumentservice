package pl.compan.docusafe.parametrization.paa;

import java.util.Calendar;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** asygnuje na prezesa */
@SuppressWarnings("serial")
public class Prezes_AsgnHandler implements AssignmentHandler {
	private static final Logger log = LoggerFactory.getLogger(Prezes_AsgnHandler.class);
	@Override
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
		log.info("Prezes_AsgnHandler");
		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		String data = Calendar.getInstance().getTime().toString();
		String rodzajDokumentu = doc.getDocumentKind().getCn();
		log.info("Dokument  id = "+docId+"Rodzaj =" +rodzajDokumentu+ " czas zajscia procesu "+data);
		
		String asygnowanyDzial ="20E9F35C-DB46-4560-8F43-141844A32086";
		
		assignable.addCandidateGroup(asygnowanyDzial);
		
		
		
		AssignmentHistoryEntry entry = new AssignmentHistoryEntry();
		entry.setSourceUser(DSApi.context().getDSUser().getName());
		String dzialNad =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(DSApi.context().getDSUser().getName());
		entry.setSourceGuid(dzialNad);
		entry.setTargetGuid(asygnowanyDzial);
		String statusDokumentu =  doc.getFieldsManager().getEnumItem("STATUSDOKUMENTU").getTitle();
		entry.setObjective(statusDokumentu);
		entry.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
		doc.addAssignmentHistoryEntry(entry);
		//String asygnowanyUzytkownik = ".....okreslic......";
		
		
		
		
		//dekretowanie
		//assignable.addCandidateUser(asygnowanyUzytkownik);
		
		// dodac recznie do historii (w WSSK robilem to tak:) 
		// Util_AHE.addToHistory(openExecution, doc, asygnowanyUzytkownik, null, null, log);
	}
}
