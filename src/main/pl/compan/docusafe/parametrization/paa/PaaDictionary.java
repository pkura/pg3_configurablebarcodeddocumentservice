package pl.compan.docusafe.parametrization.paa;

import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContextFactory;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Distinct;
import org.hibernate.criterion.Projections;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExp;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.db.criteria.NativeProjection;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.LinkValue;
import pl.compan.docusafe.core.dockinds.dwr.PersonDictionary;
import pl.compan.docusafe.core.dockinds.field.AbstractDictionaryField;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.field.DocumentField;
import pl.compan.docusafe.core.dockinds.field.LinkField;
import pl.compan.docusafe.core.dockinds.field.NonColumnField;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.parametrization.ifpan.ErpPerson;
import pl.compan.docusafe.util.DSCountry;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class PaaDictionary extends DwrDictionaryBase {

	protected static final String ERP_FLAG = " [erp]";
	private static Logger log = LoggerFactory.getLogger(PaaDictionary.class);	
	
	@Override
	public int remove(String id) throws EdmException {
		if (id != null && id.contains("-")) {
			return -1;
		}
		return super.remove(id);
	}

	@Override
	public int update(Map<String, FieldData> values) throws EdmException {
		PreparedStatement ps = null;

		String tableName = "";

		if (getOldField() instanceof DictionaryField)
		{
			tableName = ((DictionaryField) getOldField()).getTable();
		}
		else if (getOldField() instanceof DocumentField)
		{
			tableName = ((DocumentField) getOldField()).getTable();
		}

		if (isCantUpdate())
			return -1;
		
		int i = 1;
		if (!values.containsKey("id") && !values.containsKey("ID"))
			throw new NullPointerException("No id key found in values map");
		String sId = null;
		
		if (values.containsKey("id"))
			sId = "id";
		else
			sId = "ID";
		
		
		if (values.get(sId) != null && values.get(sId).getStringData().contains("-")) {
			try {
				String id = values.get(sId).getStringData().substring(1);
				values.remove(sId);
				values.put(getName()+"_WPARAM", new FieldData(Field.Type.LONG, new Long(id)));
				values.put(sId,new FieldData(Field.Type.STRING,String.valueOf(super.add(values))));
				values.put(getName()+"_ID",new FieldData(Field.Type.STRING,values.get(sId).getStringData()));
				return 1;
			} catch (Exception e) {
				return -1;
			}
		}
		
		StringBuilder query = new StringBuilder("update ").append(tableName).append(" set ");
		
		FieldData fieldId = values.get(sId);
		Object id = values.get(sId).getData();
		values.remove(sId);
		
		int size = values.size();
		for (String cn : values.keySet())
		{
			if (values.get(cn).getData() == null)
			{
				if (i == size)
				{
					int indexOf = query.toString().lastIndexOf(",");
					if ((indexOf + 2) == (query.length()))
						query = new StringBuilder(query.toString().substring(0, indexOf));
				}
				i++;
				continue;
			}
			query.append(cn.replace(getName() + "_", "")).append("=").append("?");
			if (i++ != size)
				query.append(", ");
		}
		
		query.append(" where id=").append(id);
		
		log.debug(query.toString());
		try
		{
			ps = DSApi.context().prepareStatement(query);
			int psi = 1;
			for (String cn : values.keySet())
			{
				if (values.get(cn).getData() == null)
				{
					i++;
					continue;
				}
				FieldData fd = values.get(cn);
				if (fd.getData() != null)
					fd.getData(ps, psi);
				psi++;
			}
			values.put(sId, fieldId);
			return ps.executeUpdate();
		}
		catch (SQLException e)
		{
			log.error(e.getMessage(), e);
			return -1;
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
	}

	@Override
	public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset) throws EdmException {
		List<Map<String, Object>> result;
		
		String filteringDictionary = ((AbstractDictionaryField) getOldField()).getFilteringDictionaryCn();
		Map<String, String> filteredColumns = new HashMap<String, String>();
		Map<String, FieldData> filteringValues = null;
		//	jesli slownik jest slownikiem filtrowanym
		if(filteringDictionary != null){
			//	mapa filtrowanych kolumn wraz z odpowiadajacymi im kolumnami filtrujacymi
			filteredColumns = ((AbstractDictionaryField) getOldField()).getFieldsAttributeValue().get("filteringColumn");
			//	wartosci kolumn filtrujacych
			filteringValues = (Map<String, FieldData>) WebContextFactory.get().getHttpServletRequest().getSession().getAttribute("DIC_FILTER_"+filteringDictionary);
		}
		Map<String, String> whereExpsToValue = new HashMap<String, String>();
		List<Map<String, Object>> ret = Lists.newArrayList();
		String tableName;
		String resourceName = null;
		String id = "id";
		FieldData fieldData = null;
		boolean isDocumentField = getOldField().getType().equals("document-field");
		if (isDocumentField)
		{
			tableName = ((DocumentField) getOldField()).getTable();
			id = "document_id";
			((DocumentField) getOldField()).getDocumentKind().logic().addSpecialSearchDictionaryValues(getName(), values, null);
		}
		else
		{
			tableName = ((AbstractDictionaryField) getOldField()).getTable();
			resourceName = ((AbstractDictionaryField) getOldField()).getResource(); 
			((AbstractDictionaryField) getOldField()).getDocumentKind().logic().addSpecialSearchDictionaryValues(getName(), values, null);
		}
		
		NativeCriteria nc = DSApi.context().createNativeCriteria(tableName, "d");
		NativeProjection np = NativeExps.projection();
		nc.setDistinct(true);
		NativeExp expr = NativeExps.eq("DISCRIMINATOR", "PERSON");
		nc.add(expr);
		/*expr = NativeExps.isNotNull("firstname");
		nc.add(expr);
		expr = NativeExps.isNotNull("lastname");
		nc.add(expr);*/
		expr = NativeExps.isNotNull("street");
		nc.add(expr);
		expr = NativeExps.isNotNull("zip");
		nc.add(expr);
		
		for (pl.compan.docusafe.core.dockinds.field.Field f : getOldField().getFields()) // dodaje do projekcji pola posiadaj±ce swe kolumny
		{
			if ((!(f instanceof NonColumnField) && !f.getCn().equals("ID") && !f.getCn().equals("ID_1")))
			{
				String column = f.getColumn();
				if (isMultiple() && !(f instanceof NonColumnField))
					column = column.substring(0, column.lastIndexOf("_"));
				np.addProjection("d." + column, column);
			}
		}
		np.addProjection("d." + id, "id");
		nc.setProjection(np);
		for (pl.compan.docusafe.core.dockinds.field.Field f : getOldField().getFields())
		{
			String column = f.getColumn();
			String fCn = f.getCn().toUpperCase();
			if (isMultiple() && !(f instanceof NonColumnField))
			{
				column = column.substring(0, column.lastIndexOf("_"));
				fCn = fCn.substring(0, fCn.lastIndexOf("_")).toUpperCase();
			}

			//nazwa pola
			String nameCn = new StringBuilder(getName()).append("_").append(fCn).toString();

			if (values.containsKey(nameCn) && !(f instanceof NonColumnField) && values.get(nameCn).getData() != null)
			{
				if (values.get(nameCn).getType().equals(Field.Type.DATE))
				{
					if (values.get(nameCn).getData() instanceof Date)
					{
						java.sql.Date sqlDate = new java.sql.Date(values.get(nameCn).getDateData().getTime());
						NativeExp exp = NativeExps.gte(column, sqlDate);
						nc.add(exp);
						whereExpsToValue.put(exp.toSQL(), "'"+sqlDate.toString()+"'");
						Calendar c = Calendar.getInstance();
						c.setTime(sqlDate); 
						c.add(Calendar.DATE, 1);
						exp = NativeExps.lte(column, c.getTime());
						nc.add(exp);
						sqlDate = new java.sql.Date(c.getTime().getTime());
						whereExpsToValue.put(exp.toSQL(), "'"+sqlDate.toString()+"'");
						
						if(filteredColumns.containsKey(column) && (fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column))) != null){
							sqlDate = new java.sql.Date(fieldData.getDateData().getTime());
							exp = NativeExps.gte(column, sqlDate);
							nc.add(exp);
							whereExpsToValue.put(exp.toSQL(), "'"+sqlDate.toString()+"'");
							c = Calendar.getInstance();
							c.setTime(sqlDate); 
							c.add(Calendar.DATE, 1);
							exp = NativeExps.lte(column, c.getTime());
							nc.add(exp);
							sqlDate = new java.sql.Date(c.getTime().getTime());
							whereExpsToValue.put(exp.toSQL(), "'"+sqlDate.toString()+"'");
						}
					}
				}
				else if (values.get(nameCn).getType().equals(Field.Type.MONEY))
				{
					NativeExp exp = NativeExps.like(column, "%" + values.get(getName() + "_" + fCn).getMoneyData() +"%" );
					nc.add(exp);
					whereExpsToValue.put(exp.toSQL(), "'%" + values.get(getName() + "_" + fCn).getMoneyData() +"%'"); 
					if(filteredColumns.containsKey(column) && (fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column))) != null){
						fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column));
						exp = NativeExps.like(column, "%" +fieldData.getMoneyData() +"%" );
						nc.add(exp);
						whereExpsToValue.put(exp.toSQL(), "'%" + fieldData.getMoneyData() +"%'"); 
					}
				}
				else if (values.get(nameCn).getType().equals(Field.Type.ENUM) 
						|| values.get(nameCn).getType().equals(Field.Type.ENUM_AUTOCOMPLETE)
						|| values.get(nameCn).getType().equals(Field.Type.INTEGER)
						|| values.get(nameCn).getType().equals(Field.Type.LONG))
						{
							if(!values.get(nameCn).getData().equals(NULLABLE)){
								NativeExp exp = NativeExps.eq(column, values.get(nameCn).getData());
								nc.add(exp);
								whereExpsToValue.put(exp.toSQL(), values.get(nameCn).getData().toString()); 
								if(filteredColumns.containsKey(column) && (fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column))) != null){
									fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column));
									exp = NativeExps.eq(column, fieldData.getData());
									nc.add(exp);
									whereExpsToValue.put(exp.toSQL(), fieldData.getData().toString()); 
								}
							}else{
								NativeExp exp = NativeExps.isNull(column);
								nc.add(exp);
								whereExpsToValue.put(exp.toSQL(), values.get(nameCn).getData().toString());
								if(filteredColumns.containsKey(column) && (fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column))) != null){
									fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column));
									exp = NativeExps.isNull(column);
									nc.add(exp);
									whereExpsToValue.put(exp.toSQL(), fieldData.getData().toString());
								}
							}
						}
				//wartosc dla pola boolean typu smallint null OR wartosc
				else if(values.get(nameCn).getType().equals(Field.Type.BOOLEAN))
				{
					NativeExp nativeValue = NativeExps.eq(column, values.get(nameCn).getData());
					NativeExp nativeNull = NativeExps.isNull(column);
					
					List<NativeExp> exps = new ArrayList<NativeExp>();
					exps.add(nativeNull);
					exps.add(nativeValue);
					NativeExp exp = NativeExps.disjunction(exps);
					nc.add(exp);
					if(values.get(nameCn).getData().toString().toLowerCase().equals("true")){
						whereExpsToValue.put(exp.toSQL(), 1+")"); 	
					}else{
						whereExpsToValue.put(exp.toSQL(), 0+")"); 
					}
					if(filteredColumns.containsKey(column) && (fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column))) != null){
						fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column));
						nativeValue = NativeExps.eq(column, fieldData.getData());
						nativeNull = NativeExps.isNull(column);
						exps = new ArrayList<NativeExp>();
						exps.add(nativeNull);
						exps.add(nativeValue);
						exp = NativeExps.disjunction(exps);
						nc.add(exp);
						if(fieldData.toString().toLowerCase().equals("true")){
							whereExpsToValue.put(exp.toSQL(), 1+")"); 	
						}else{
							whereExpsToValue.put(exp.toSQL(), 0+")"); 
						}
					}
				}
				else if (!values.get(nameCn).getStringData().equals("") && !(column.equals("ID") && isDocumentField))
				{
					try {
						String valueData = (String) values.get(nameCn).getData();
						if (column.equalsIgnoreCase("nip"))
							valueData = valueData.replace("-", "").replace(" ", "");
						NativeExp exp = NativeExps.likeCaseInsensitive(column, "%" + valueData + "%");
						nc.add(exp);
						whereExpsToValue.put(exp.toSQL(), "'%" + valueData +"%'");
						if(filteredColumns.containsKey(column) && (fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column))) != null){
							fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column));
							valueData = (String) fieldData.getData();
							if (column.equalsIgnoreCase("nip"))
								valueData = valueData.replace("-", "").replace(" ", "");
							exp = NativeExps.like(column, "%" + valueData + "%");
							nc.add(exp);
							whereExpsToValue.put(exp.toSQL(), "'%" + valueData +"%'");
						}
					}
					catch (Exception e)
					{
						log.debug(e);
					}
				}
			}
		}
		// ograniczenie za pomocą atrybutu not-nullable-fields
		if (this.getOldField() instanceof AbstractDictionaryField && ((AbstractDictionaryField)this.getOldField()).getNotNullableFields() != null) {
			for (String notNullField : ((AbstractDictionaryField)this.getOldField()).getNotNullableFields()){
				if (StringUtils.isNotEmpty(notNullField)) {
					NativeExp exp = NativeExps.isNotNull(notNullField);
					nc.add(exp);
					whereExpsToValue.put(exp.toSQL(), notNullField.toString()); 
				}
			}
		}
		
		if (values.containsKey("id") && !(values.get("id").getData().toString()).equals("")){
			NativeExp exp = NativeExps.eq("d." + id, values.get("id").getData());
			nc.add(exp);
			whereExpsToValue.put(exp.toSQL(), values.get("id").getData().toString()); 
		}else
			nc.setLimit(limit).setOffset(offset);
		try
		{
			//	gdy zrodlem pol slownika jest zewnetrzna baza danych
			if(resourceName != null){/*
				String sqlQuery = getQuery(nc.buildCriteriaQuery().getQueryString(), filteredColumns, filteringValues, whereExpsToValue, limit, offset);
				DictionaryField dic = (DictionaryField) getOldField();
				Map<String, Object> row = Maps.newLinkedHashMap();
				Context initCtx = new InitialContext();
				DataSource dataSource = (DataSource) initCtx.lookup("java:comp/env/jdbc/" + resourceName);
				Connection connection = dataSource.getConnection();
				ResultSet resultSet = connection.prepareStatement(sqlQuery).executeQuery();
            	String colVal = "";
            	while(resultSet.next()){
                	row = Maps.newLinkedHashMap();
                	row.put("id", resultSet.getObject("ID"));
                	row.put(getName().toUpperCase() + "_ID", resultSet.getObject("ID"));
            		for(String columnName : dic.getSearchFields()){
            			Object oVal = resultSet.getObject(columnName);
            			if(oVal != null){
            				colVal += columnName + " = " + resultSet.getObject(columnName) + " (" + resultSet.getObject(columnName).getClass() + ")\n";
            				row.put(getName().toUpperCase() + "_" + columnName, resultSet.getObject(columnName));
            			}else{
            				colVal += columnName + " = " + "" + " (" + "" + ")\n";
            				row.put(getName().toUpperCase() + "_" + columnName, "");
            			}
            		}
                	ret.add(row);
            	}
            	connection.close();*/
			}else{ 
				CriteriaResult cr = nc.criteriaResult();
				while (cr.next())
				{
					Map<String, Object> row = Maps.newLinkedHashMap();
					String entryId =  cr.getString("id", null);
					/* Jezeli jest polem dokumentu (document-field) to bierzemy tylko te, do ktorych uzytkownik ma uprawienia.
					Trzeba to bedzie przerobic, zeby sprawdzac w momencie wyszukania, a nie dopiero po*/
					if (isDocumentField)
					{
						boolean checkPermission = true;
						try {
							Document.find(Long.valueOf(entryId), checkPermission);
						}
						catch(DocumentNotFoundException e1)
						{
							continue;
						}
						catch(AccessDeniedException e2)
						{
							continue;
						}
					}
					row.put("id", cr.getString("id", null));
					for (pl.compan.docusafe.core.dockinds.field.Field f : getOldField().getFields())
					{
						String fCn = f.getCn();
						String column = f.getColumn();
						// dla multiple ucina _(id wpisu) z CN
						if (isMultiple())
						{
							fCn = fCn.substring(0, fCn.lastIndexOf("_"));
							if (!(f instanceof NonColumnField))
								column = column.substring(0, column.lastIndexOf("_"));
						}
	
						if (!(f instanceof NonColumnField) && cr.getValue(column, null) != null)
						{
							if (f.getType().equals(f.DATE))
							{
								if (cr.getValue(column, null) instanceof String)
								{
									DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
									try
									{
										Date date = df.parse(cr.getString(column, null));
										row.put(getName().toUpperCase() + "_" + fCn.toUpperCase(), date);
									}
									catch (ParseException e)
									{
										log.error(e.getMessage());
									}
								}
								else
									row.put(getName().toUpperCase() + "_" + fCn.toUpperCase(), cr.getDate(column, null));
							}
							else
								row.put(getName().toUpperCase() + "_" + fCn.toUpperCase(), cr.getValue(column, null));
						}
						if (f.getType().equals(pl.compan.docusafe.core.dockinds.field.Field.LINK))
						{
							String fieldLabel = ((LinkField) f).getLinkLabel();
							Document doc =  null;

							String label = null;
							if (fieldLabel.equals("document-title"))
							{
								doc = Document.find(Long.parseLong((String) row.get("id")), false);
								label = doc.getTitle();
							}
							else if (fieldLabel.equals("document-description"))
							{
								doc = Document.find(Long.parseLong((String) row.get("id")), false);
								label = doc.getDescription();
							}
							else if (fieldLabel.equals("link-value"))
								label =  ((LinkField)f).getLogicField().getLinkLabel();
							else if (fieldLabel.equals("column"))
								label =  cr.getString(((LinkField) f).getLabelColumn(), "-");
							else
							{
								FieldsManager fm = null;
								try {
									doc = Document.find(Long.parseLong((String) row.get("id")), false);
									fm = doc.getFieldsManager();
									label = (String)fm.getValue(fieldLabel);
								} catch (Exception e){
									doc = doc == null ? Document.find(Long.parseLong((String) row.get("id")), false) : doc;
									fm =  fm == null ? doc.getFieldsManager() : fm;
									label = fm.getValue(fieldLabel).toString();
								}
							}
							row.put(getName().toUpperCase() + "_" + fCn.toUpperCase(), new LinkValue(label, ((LinkField)f).getLogicField().createLink((String) row.get("id"))));
						}
					}
					ret.add(row);
				}
			}
		}
		catch (Exception a)
		{
			log.error(a.getMessage(), a);
			throw new EdmException(a);
		}
		return ret;
	}

	@Override
	public long add(Map<String, FieldData> values) throws EdmException
	{
		// add wywoa�ane w momencie zapisywania dokumentu
		if (values.get("DISCRIMINATOR") != null && "PERSON".equals(values.get("DISCRIMINATOR").getStringData()) &&
				values.get("ANONYMOUS") != null	&& "0".equals(values.get("ANONYMOUS").getStringData())) {
			Person person = new Person();
			Sender sender = new Sender();
			PersonDictionary.setPersonValue(null, person, values);
			try {
				person.create();
				PersonDictionary.setPersonValue(null, sender, values);
				sender.setBasePersonId(person.getId());
				sender.setDictionaryType(Person.DICTIONARY_SENDER);
				sender.create();
			} catch (EdmException e1) {
				log.error(e1.getMessage());
				return -1;
			}

			return person.getId();
			
			// wywo�ywane w momencie tworzenie nowego wpisu z poziomu popup'a
		} else {
			Person person = new Person();
			long  resultID = 0l;
			//values.keySet();
			
/*			Map<String, Object> wartosci = (Map<String, Object>) values.get("M_DICT_VALUES");
			if (wartosci != null) {
				for (String key : wartosci.keySet()) {
					if (key.startsWith("RECIPIENT")) {
						Map<String, Object> wpis = (Map<String, Object>) wartosci.get(key);
						if (wpis.get("RECIPIENT")!=null && wpis.get("RECIPIENT")!=null){
							FieldData fd = (FieldData) wpis.get("RECIPIENT");
							person.find(fd.getLongData());
							int x = 1;
						}
					}
					}
				}*/
			/*try {
				if(person.createIfNew()){
				NormalLogic.utworzOdbiorce(person, values);
				}
			} catch (EdmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			values.keySet();
			PersonDictionary.setPersonValue(getName(), person, values);
			try {
				DSApi.context().begin();
				person.create();
				  resultID = NormalLogic.utworzOdbiorce(person, values);
				DSApi.context().commit();
			} catch (EdmException e1) {
				log.error(e1.getMessage());
				return -1;
			}
			
			
			
			if (resultID > 0 )
				return resultID;
				else 
			return person.getId();
		}
	}

	/*public List<Person> findPersonInErp(String organization, String countryId, String nip) throws EdmException
	{
		List<Person> foundContractor = new ArrayList<Person>();
		Connection con = null;
		CallableStatement call = null;
		String countryCn = null;
		try {
			if (countryId != null && DSCountry.find(Integer.valueOf(countryId), null, null)!=null) {
				countryCn = DSCountry.find(Integer.valueOf(countryId), null, null).get(0).getCn();
			}
			String erpUser, erpPassword, erpUrl, erpDataBase, erpSchema, procWeryfikacjaDostawcy;
			erpUser = Docusafe.getAdditionProperty("erp.database.user");
			erpPassword = Docusafe.getAdditionProperty("erp.database.password");
			erpUrl = Docusafe.getAdditionProperty("erp.database.url_MSSQL");//"jdbc:sqlserver://localhost:1433";
			erpDataBase = Docusafe.getAdditionProperty("erp.database.dataBase");
			erpSchema = Docusafe.getAdditionProperty("erp.database.schema");
			// [up_gsd_szukaj_dostawcy]
			procWeryfikacjaDostawcy = Docusafe.getAdditionProperty("erp.database.procSzukajDostawcy");
			
			con = IfpanUtils.connectToSimpleErpDataBase(erpUser, erpPassword, erpUrl, "sqlserver_2008");
			
//			EXEC   @return_value = [dbo].[up_gsd_szukaj_dostawcy]
//                    @dostawcanazwa = N'',
//                    @nip = N'''''',
//                    @kraj_nazwa = N'',
//                    @wynik = @wynik OUTPUT
			call = con.prepareCall("{? = call [" + erpDataBase + "].[" + erpSchema + "].[" + procWeryfikacjaDostawcy + "] (?,?,?,?)}");
			call.registerOutParameter(1, java.sql.Types.NUMERIC);
			
			if (StringUtils.isNotEmpty(nip)) {
				call.setString(3, nip.replace("-", "").replace(" ",""));
				call.setString(2, null);	
				call.setString(4, null);	
			} else if (StringUtils.isNotEmpty(organization) && StringUtils.isNotEmpty(countryCn)) {
				call.setString(2, organization);	
				call.setString(4, countryCn);	
				call.setString(3, null);
			} else {
				call.setString(2, null);	
				call.setString(4, null);	
				call.setString(3, null);
			}
			
			call.registerOutParameter(5, java.sql.Types.SQLXML);
			
			boolean queryResult;
			boolean updatingErp = false;
			try {
				queryResult = call.execute();
				
				if (!queryResult) {
					Object result = call.getObject(5);
					if (result != null && result instanceof SQLXML) {
						SQLXML xmlResult = (SQLXML)result;
						InputStream stream = xmlResult.getBinaryStream();
						org.dom4j.Document doc = new SAXReader().read(stream);
						
						for(Element el : (Collection<Element>)doc.getRootElement().elements("dostawca")){
							Long erpId = null;
							if (StringUtils.isNotEmpty(el.attributeValue("dostawca_id"))) {
								try {
									erpId = Long.valueOf(el.attributeValue("dostawca_id"));
									ErpPerson contractor;
									if (ErpPerson.findByErpId(erpId) != null) {
										updatingErp = true;
										DSApi.context().begin();
										contractor = ErpPerson.findByErpId(erpId);
									} else {
										contractor = new ErpPerson();
									}
									
									contractor.setErpId(Long.valueOf(el.attributeValue("dostawca_id")));
									contractor.setOrganization(el.attributeValue("dostawca_nazwa"));
									if (StringUtils.isNotEmpty(el.attributeValue("kraj_kod")) && DSCountry.find(null, el.attributeValue("kraj_kod"), null) != null) {
										contractor.setCountryId(DSCountry.find(null, el.attributeValue("kraj_kod"), null).get(0).getId());
									}
									contractor.setStreet(el.attributeValue("ulica"));
									contractor.setLocation(el.attributeValue("miasto"));
									contractor.setZip(el.attributeValue("zip"));
									contractor.setNip(el.attributeValue("nip"));
									contractor.save();
									
									if (updatingErp) {
										DSApi.context().commit();
										updatingErp = false;
									}
									foundContractor.add(contractor);
								} catch (NumberFormatException e) {
									log.error("ID dostawcy z simple.erp nie jest liczb�");
								}
							}
						}
					}
					
				} else throw new EdmException("B��d w znalezieniu dostawcy");
				
			} catch (Exception e) {
				if (updatingErp) {
					DSApi.context().rollback();
				}
				log.error(e.getMessage(),e);
				throw e;
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new EdmException("B��d w znalezieniu dostawcy :" + e.getMessage());
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					log.error(e.getMessage(), e);
					throw new EdmException("B��d w znalezieniu dostawcy:" + e.getMessage());
				}
			}
		}
		return foundContractor;
		
	}*/
	
	public void runErpPersonProcedure(Map<String, FieldData> values) {
		/*	try {
			String searchOrganization = values.get(getName()+"_ORGANIZATION") != null ? values.get(getName()+"_ORGANIZATION").toString() : null;
			String searchNip = values.get(getName()+"_NIP") != null ? values.get(getName()+"_NIP").toString() : null;
			String searchCountry = values.get(getName()+"_COUNTRY") != null ? values.get(getName()+"_COUNTRY").toString() : null;

			if (StringUtils.isNotEmpty(searchNip) || (StringUtils.isNotEmpty(searchOrganization) && StringUtils.isNotEmpty(searchCountry))) {
				findPersonInErp(searchOrganization, searchCountry, searchNip);
			}
		} catch (EdmException e) {
			log.error(e.toString());
		}*/
	}

	public void createPersonFromChoosenErpId(Map<String, FieldData> values) {
		try {
			Long id = Long.valueOf(values.get("id").toString().substring(1));

			Person erpContractor = Person.findIfExist(id);
			if (erpContractor != null) {
				Person contractor = new Person();
				//erpContractor.updatePersonFields(contractor);
				contractor.create();

				Sender sender = new Sender();
				//erpContractor.updatePersonFields(sender);
				sender.setDictionaryGuid("rootdivision");
				sender.setDictionaryType(Person.DICTIONARY_SENDER);
				sender.setBasePersonId(contractor.getId());
				sender.create();

				values.get("id").setStringData(sender.getId().toString());
				//values.put("CONTRACTOR_ID", new FieldData(Field.Type.STRING,sender.getId().toString()));
			}

		} catch (NumberFormatException e) {
			log.error(e.toString());
		} catch (EdmException e1) {
			log.error(e1.toString());
		}
	}
}
