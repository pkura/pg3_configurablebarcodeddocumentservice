package pl.compan.docusafe.parametrization.paa;

import java.util.Calendar;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class AkceptacjaPrezesaAOsobaWNioskujaca implements DecisionHandler
{
	private static final Logger log = LoggerFactory.getLogger(AkceptacjaPrezesaAOsobaWNioskujaca.class);

	public String decide(OpenExecution openExecution)
	{
		log.info("AkceptacjaPrezesaAOsobaWNioskujaca");

		String decision = "autor";
	
		try {
			Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			//FieldsManager fm = doc.getFieldsManager();
			DSUser user =DSUser.findByUsername(doc.getAuthor());
			
			
			String data = Calendar.getInstance().getTime().toString();
			String rodzajDokumentu = doc.getDocumentKind().getCn();
			log.info("Dokument  id = "+docId+"Rodzaj = " +rodzajDokumentu+ " czas zajscia procesu "+data);
			
			boolean wniosekDyrGEneralnego = (Boolean) openExecution.getVariable("wniosekDyrGEneralnego");
			
			boolean wniosekViceprezesa = (Boolean) openExecution.getVariable("wniosekViceprezesa");
			boolean wniosekPrezesa = (Boolean) openExecution.getVariable("wniosekPrezesa");
			
			
			
			
			if (wniosekDyrGEneralnego){
				decision = "WniosekDyrGEneralnego";
				return decision;
			}
			else if(wniosekViceprezesa){
				decision = "wniosekViceprezesa";
				return decision;
				}
			
			else {
				decision = "NieWniosekDyrGEneralnego";
			}
			
			
		}catch (Exception e) {
			log.info("",e);
		}
		return decision;
	}
}
