package pl.compan.docusafe.parametrization.paa;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.OfficeDocument;

import pl.compan.docusafe.parametrization.wssk.SwimLane_AsgnHandler;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** Asygnuje zadanie na uzytkownika podanego po loginie
 * w zmiennej o nazwie zawartej w var (chodzi o zmienn± procesu).
 * Można podać także status, który zostanie wpisany do historii
 * zwłaszcza, że jest to do wykorzystania wew. forka. */
@SuppressWarnings("serial")
public class AsgnHandler_Foreach implements AssignmentHandler {
	
	/** nazwa zmiennej procesu, pod jak± jest zapamiętany user, który ma przyj±ć to zadanie */
	protected String var;
	
	/** status, który będzie wpisany na sztywno do historii. Może byc null. Używane wewn±trz forka. */
	protected String status;
	
	private static final Logger log = LoggerFactory.getLogger(SwimLane_AsgnHandler.class);

	
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
		Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);

		if (var==null) {
			log.error("nie podano zmiennej procesu zawieraj±cej użytkownika - asygnacja niemożliwa ");
			throw new EdmException ("nie podano zmiennej procesu zawieraj±cej użytkownika - asygnacja niemożliwa ");
		}
		
		Object usero = openExecution.getVariable(var);
		if (usero == null) {
			log.error("zmienna procesu podana w polu var jest pusta - asygnacja niemożliwa ");
			throw new EdmException ("zmienna procesu podana w polu var jest pusta - asygnacja niemożliwa ");
		}
		
		String user = (String) usero;
		
		try {
			if (user.equals("author")) {
				assignable.addCandidateUser(doc.getAuthor());
				AssigneeHandler.addToHistory(openExecution, doc, doc.getAuthor(), null);
			} else {
				assignable.addCandidateUser(user);
				AssigneeHandler.addToHistory(openExecution, doc, user, null);
			}
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
			throw e;
		}
	}
}
