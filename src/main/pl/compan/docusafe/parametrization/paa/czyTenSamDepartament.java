package pl.compan.docusafe.parametrization.paa;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;


public class czyTenSamDepartament implements DecisionHandler
{
	private static final Logger log = LoggerFactory.getLogger(czyTenSamDepartament.class);

	private String poleOdbiorcy;
	
	public String decide(OpenExecution openExecution)
	{
		log.info("czyTenSamDepartament decision");

		String decision = "autor";
	
		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		try{
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		
		String data = Calendar.getInstance().getTime().toString();
		String rodzajDokumentu = doc.getDocumentKind().getCn();
		log.info("Dokument  id = "+docId+"Rodzaj = " +rodzajDokumentu+ " czas zajscia procesu "+data);
		
		String odbiorcaIdStr=fm.getStringKey(poleOdbiorcy);
		log.info("pobra��m odbiorca id = " +odbiorcaIdStr+" - "+poleOdbiorcy);
				
		String[] ids;
		if (odbiorcaIdStr.startsWith("[")){
			odbiorcaIdStr= odbiorcaIdStr.substring(1, odbiorcaIdStr.length()-1);
			ids  = odbiorcaIdStr.split(",");
		}else{
			ids = new String[1];
			ids[0] = odbiorcaIdStr;
		}
		//usernames.clear();
		//iloscOsobZastepujacych = 0;
		int liczbakandydatow=0;
		List<String> listaUserowIn = new ArrayList<String>();
		List<String> listaUserowOther = new ArrayList<String>();
		
		
		for (String id : ids) {
				id = id.trim();
			long odbiorcaId= 0;
			try {
				try {
					odbiorcaId= Long.parseLong(id); // pierwsza proba
				} catch (NumberFormatException nfe) {
					odbiorcaId= Long.parseLong(id.substring(1, id.length() - 1)); // druga proba
				}
			} catch (NumberFormatException nfe) {
				log.info("podane ID u�ytkownika (" + odbiorcaIdStr
						+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
				throw new IllegalStateException("podane ID u�ytkownika (" + odbiorcaIdStr
						+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
			}

			DSUser odbiorca= null;

			if (poleOdbiorcy.equalsIgnoreCase("RECIPIENT_HERE")) {
				// person
				Person person= Person.find(odbiorcaId);
				String personfirstname= person.getFirstname();
				String personLastname= person.getLastname();

				odbiorca= DSUser.findByFirstnameLastname(personfirstname, personLastname);
			} else if (poleOdbiorcy.equalsIgnoreCase("RECIPIENT")) {
				// user
				odbiorca= DSUser.findById(odbiorcaId);
			} else {
				throw new Exception("poleOdbiorcy ma byc RECIPIENT lub RECIPIENT_HERE");
			}
			String nadawca= DSApi.context().getDSUser().getName();
			if(NormalLogic.isDocumentFromSubstitutedUser(openExecution))
				nadawca = (String)openExecution.getVariable("currentAssignee");
		
	
			
			if (nadawca.equals(odbiorca.getName())) {
				log.info("czyTenSamDepartament decision   - Odbiorca w tym samym departamencie - dekretuje na odbiorc� : "
						+ odbiorca.getName());
				
			} else {
				boolean tenSam= pl.compan.docusafe.parametrization.paa.NormalLogic.inDepIsOutDep(nadawca, odbiorca.getName());
				if (tenSam)
					listaUserowIn.add(odbiorca.getName());
				else 
					listaUserowOther.add(odbiorca.getName());
		
		
		}
			liczbakandydatow ++;
		}
		if (liczbakandydatow==1){
			if(!listaUserowOther.isEmpty()){
				openExecution.setVariable("odbiorca", listaUserowOther.get(0));
				return decision ="inny";
			}else {
				decision = "tenSam";
			}
			
		}else
			return decision="tenSam";
		
		
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return decision;
	
	}
}

