package pl.compan.docusafe.parametrization.paa;

import com.google.common.collect.Maps;
import com.lowagie.text.Phrase;

import edu.emory.mathcs.backport.java.util.concurrent.LinkedBlockingDeque;

import pl.compan.docusafe.api.Fields;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.ValidationException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.absences.Absence;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.Dictionary;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;

import pl.compan.docusafe.core.dockinds.dwr.valuehandler.DictionaryValueHandler;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.field.LinkField;
import pl.compan.docusafe.core.dockinds.field.reader.DictionaryFieldReader;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.process.ProcessAction;
import pl.compan.docusafe.core.dockinds.process.ProcessManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.mail.MailMessage;
import pl.compan.docusafe.core.mail.MessageAttachment;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.ReferenceToHimselfException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.parametrization.pig.NextTask;
import pl.compan.docusafe.parametrization.pig.PigParametrization;
import pl.compan.docusafe.parametrization.wssk.WSSK_CN;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import org.jbpm.api.model.OpenExecution;
import org.jfree.util.Log;

import pl.compan.docusafe.web.admin.employee.EmployeeCardAction;
import pl.compan.docusafe.web.archive.repository.NewDocumentAction;
import pl.compan.docusafe.web.office.common.DwrDocument;
import pl.compan.docusafe.web.office.common.DwrDocumentHelper;
import pl.compan.docusafe.web.office.common.DwrDocumentMainTabAction;
import pl.compan.docusafe.web.office.tasklist.TaskListAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;

import org.apache.commons.lang.StringUtils;

import org.drools.lang.DRLParser.date_effective_key_return;


import java_.lang.Exception_;


import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.*;

@SuppressWarnings("serial")
public class NormalLogic extends AbstractDocumentLogic
{
	public final static String _USER = "USER";
	public final static String _DIVISION = "DSDIVISION";
	public final static String _stampDate = "stampDate";
	public final static String _znak_pisma = "znak_pisma";
	public final static String _SENDER_HERE = "SENDER_HERE";
	public final static String _POSITION = "POSITION";
	public final static String _NUMERPISMA = "NUMERPISMA";
	public final static String _AVAILABLE_DAYS ="AVAILABLE_DAYS";
	public final static String _PELENADRES = "PELENADRES";
	public static final String  KIND_EXECUTE = "Do Realizacji";
	public static final String _multiOdbiorca = "M_DICT_VALUES";
	public static final String _pelenAdres = "PELENADRES";
	public static final String _RECIPIENT = "RECIPIENT";
	public static final String _REPILES_FOR = "REPILES_FOR";
	public static final String _RODZAJ_PRZESYLKI = "RODZAJ_PRZESYLKI";	
	public static final String _SPOSOB_DOSTARCZENIA = "SPOSOB_DOSTARCZENIA";
	public static final String _idMultiPerson = "ID";	
	public static final String _kosztJednostkowy = "KWOTA";	
	public static final String _rodzajPrzesylkiWagaKrajowa = "RODZAJ_PRZESYΘI_TYPE_WEIGHT_COUNTRY";	
	public static final String _rodzajPrzesylkiWagaSwiat = "RODZAJ_PRZESYΘI_TYPE_WEIGHT_COUNTRY";
	public static final String _GABARYT = "GABARYT";
	public static final String _dataDokumentu = "DOC_DATE";
	public static final String _odpowiedzNa = "REPILES_FOR";
	public static final String _biuroDyrGeneralnego = "WBDG";
	public static final String _KoszListu = "KOSZT_PRZESYLKI";
	
	public  boolean WalidacjaDatWniosku ;
	private Document document;
	//  protected String targetUsers[]= new String[2];
	   protected static String targetGuids[];
		protected static DwrDocumentHelper dwrDocumentHelper = new DwrDocumentHelper();
	public boolean isWalidacjaDatWniosku()
	{
		return WalidacjaDatWniosku;
	}

	//public final static String _ADRESAT = "ADRESAT_DICT";
	//public final static String _KOD_PIKA = "KOD_PIKA";
	private static NormalLogic instance;
	protected static Logger log = LoggerFactory.getLogger(NormalLogic.class);

	 
	public static NormalLogic getInstance()
	{
		if (instance == null)
			instance = new NormalLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	//nowa kolumna na liscie zadan 
	public TaskListParams getTaskListParams(DocumentKind dockind, long id)
			throws EdmException {
		TaskListParams params = new TaskListParams();

		try {
			FieldsManager fm = dockind.getFieldsManager(id);

			if (fm.getDocumentKind().getCn().equals("paadocint")
					|| fm.getDocumentKind().getCn().equals("paadocin")) {
				if (fm.getValue("realizationDate") != null)
					params.setAttribute(DateUtils.formatCommonDate((Date) fm
							.getValue("realizationDate")), 0);
				// params.setAttribute(
				// DateUtils.formatCommonDate((Date)fm.getValue("realizationDate")).toString(),
				// 1);
				// params.setAttribute((String) fm.getValue("realizationDate"),
				// 0);
			}

			if (fm.getDocumentKind().getCn().equals("paadocout")
					|| fm.getDocumentKind().getCn().equals("normal_out")
					|| fm.getDocumentKind().getCn().equals("paadocin")) {
				if (fm.getValue("znak_pisma") != null)
					params.setAttribute((String) fm.getValue("znak_pisma"), 1);
			}

			if (fm.getDocumentKind().getCn().equals("paadocout")
					|| fm.getDocumentKind().getCn().equals("normal_out")
					|| fm.getDocumentKind().getCn().equals("paadocin")
					|| fm.getDocumentKind().getCn().equals("normal")
					|| fm.getDocumentKind().getCn().equals("normal_in")) {

				if (fm.getValue(_pelenAdres) != null)
					params.setAttribute((String) fm.getValue(_pelenAdres), 2);

			}
		} catch (Exception e) {
			log.error("", e);
		}
		return params;
	}
	
	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
	{
		Field msg = null;
		
		if (values.get("DWR_DOC_DATE") != null && values.get("DWR_DOC_DATE").getData() != null)
		{
			Date startDate = new Date();
			Date finishDate = values.get("DWR_DOC_DATE").getDateData();
			
		//	if (fm.getDocumentKind().getCn().equals("normal_out") || !fm.getDocumentKind().getCn().equals("paadocout") || !fm.getDocumentKind().getCn().equals("paadocint")){
		if (fm.getDocumentKind().getCn().equals("paadocin")){
			if (finishDate.after(startDate))
			{
				msg = new Field("messageField", "Data pisma nie mo瞠 by� dat� z przysz這�ci.", Field.Type.BOOLEAN);
				values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
			}
			}
		}
		
		/*if (fm.getDocumentKind().getCn().equals("paa_wniosek_urlopowy")) {
			
			
			if (values.get("DWR_FROM") != null && values.get("DWR_TO") != null) {
				Date UrlopOd= values.get("DWR_FROM").getDateData();
				Date UrlopDo= values.get("DWR_TO").getDateData();
				if (UrlopOd.after(UrlopDo)) { 
					msg= new Field("messageField", "Data Wniosku nie jest poprawna.", Field.Type.BOOLEAN);
					values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
				//	FieldData poleWalidacja =  values.get("DWR_WALIDACJADAT");
				//	poleWalidacja.setBooleanData(false);
				}
			}
		}*/
	
		if (fm.getDocumentKind().getCn().equals("paa_wniosek_urlopowy")) {
			// ukrywanie pola zawtepstwo
			try {
				
				if (values.get("DWR_KIND") != null
						&& values.get("DWR_KIND").getEnumValuesData().getSelectedId().equals("20")
						|| values.get("DWR_KIND") != null
						&& values.get("DWR_KIND").getEnumValuesData().getSelectedId().equals("25")) {

					fm.getField("ZASTEPSTWO").setHidden(true);
					fm.getField("ZASTEPSTWO").setRequired(false);
				} else {
					fm.getField("ZASTEPSTWO").setHidden(false);
					fm.getField("ZASTEPSTWO").setRequired(true);
				}
			} catch (EdmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// dane do typu urlopu odbi鏎 godzin

			try {

				if (values.get("DWR_KIND") != null
						&& values.get("DWR_KIND").getEnumValuesData().getSelectedId().equals("40")) {

					fm.getField("GODZ_FROM").setHidden(false);
					fm.getField("GODZ_FROM").setRequired(true);
					fm.getField("GODZ_TO").setHidden(false);
					fm.getField("GODZ_TO").setRequired(true);
				} else {
					fm.getField("GODZ_FROM").setHidden(true);
					fm.getField("GODZ_FROM").setRequired(false);
					fm.getField("GODZ_TO").setHidden(true);
					fm.getField("GODZ_TO").setRequired(false);
				}

			} catch (EdmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return msg;
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		
		log.info("type: {}", type);
		Map<String, Object> values= new HashMap<String, Object>();
		values.put("TYPE", type);
		
		Long userDivision = (long) 1;
		
		if( fm.getDocumentKind().getCn().equals("paa_wniosek_urlopowy") ){
			fm.getField("ZASTEPSTWO").setHidden(false);
			fm.getField("ZASTEPSTWO").setRequired(true);
	
			DSUser user= DSApi.context().getDSUser();
	
			DSDivision[] div= user.getDivisions();
	
			DSDivision[] divisions= user.getOriginalDivisions();
		
			for (int i= 0; i < divisions.length; i++) 
			{ 
				
				
				if (divisions[i].isPosition()) 
				{  if (divisions[i].getDescription()!=null && (divisions[i].getDescription().equals("PREZES")|| divisions[i].getDescription().equals("VCEPREZES"))){userDivision = divisions[i].getId();}
					DSDivision parent= divisions[i].getParent();
					if (parent != null) {
						userDivision= parent.getId();
						break;
					} else {
						userDivision= divisions[i].getId();
					}
				}
				else if (divisions[i].isDivision())
				{
					userDivision = divisions[i].getId();
					break;
				}
			}
			
			if (hasRoleRadca(user)){
				String a = "";
			}
			String a = Docusafe.getAdditionProperty("PAA.naczelnik");
			for(String naczelnik : a.split(",")){
				if (user.getName().equals(naczelnik)){
					fm.getField("ZASTEPSTWO").setHidden(true);
					fm.getField("ZASTEPSTWO").setRequired(false);
					fm.getField("GODZ_FROM").setHidden(true);
					fm.getField("GODZ_FROM").setRequired(false);
					fm.getField("GODZ_TO").setHidden(true);
					fm.getField("GODZ_TO").setRequired(false);
				
				//	fm.getEnumItem("KIND").setTmp("20");
					values.put("KIND", "25");
				//	fm.getField("KIND").setDefaultValue("20");
					fm.getField("KIND").setReadOnly(false);
					break;
					
				}else {
					fm.getField("ZASTEPSTWO").setHidden(false);
					fm.getField("ZASTEPSTWO").setRequired(true);
					fm.getField("GODZ_FROM").setHidden(true);
					fm.getField("GODZ_FROM").setRequired(false);
					fm.getField("GODZ_TO").setHidden(true);
					fm.getField("GODZ_TO").setRequired(false);
					fm.getField("KIND").setReadOnly(false);
					values.put("KIND", "10");
				}
			}
			
			values.put(_USER, DSApi.context().getDSUser().getId());
			values.put(_DIVISION, userDivision);
			
			
			// wpisanie nale積ych dni urlopu
			
			int liczbaDni = 0;
			  
				UserImpl userWnioskujacy = (UserImpl) DSUser.findByUsername(DSApi.context().getDSUser().getName());
				
				EmployeeCard empCard;
				try {
					empCard = getEmployeeCard(userWnioskujacy);
				
				if (empCard == null)
					throw new IllegalStateException("Brak karty pracownika! Nie mo積a przypisa� urlopu!");
				
				liczbaDni = AbsenceFactory.getAvailableDaysForCurrentYear(empCard);
			   
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			   values.put(_AVAILABLE_DAYS, liczbaDni); 
		}
	
		
		


		if( fm.getDocumentKind().getCn().equals("paadcoin") )
		values.put(_stampDate, Calendar.getInstance().getTime());
		if( fm.getDocumentKind().getCn().equals("paadocint") )
		values.put(_dataDokumentu, new Date());
		
	/*
		// wstawianie numeru pisma wychodz鉍ego z automatu jako  /departament/nr pisma Wych Wdziale /nnr pisma Wych Ogolnie/ rok 
		if( fm.getDocumentKind().getCn().equals("paadocout") || fm.getDocumentKind().getCn().equals("normal_out")){
			values.put(_dataDokumentu, new Date());
			String dzialUzytkownika = "brak";
			String nrPismaWdziale = "";
			String OgolnyNrPismaWych ="";
			int rok =  Calendar.getInstance().get(Calendar.YEAR);
			
			
			
			DSUser user= DSApi.context().getDSUser();
			
			DSDivision[] div= user.getDivisions();
	
			DSDivision[] divisions= user.getOriginalDivisions();
			String userdivisionguid ="";
			for (int i= 0; i < divisions.length; i++)
			{
				System.out.println(divisions[i].getName());
				if (divisions[i].isPosition()) 
				{
					DSDivision parent= divisions[i].getParent();

						 dzialUzytkownika = parent.getCode();
						 userdivisionguid = parent.getGuid();
						 break;
					
				}
				else if(divisions[i].isDivision())
				{
					
						dzialUzytkownika = divisions[i].getCode();
						 userdivisionguid = divisions[i].getGuid();
						 break;
				}
					
			}
			
			
			 PreparedStatement pst = null;
			   
			   long nrpismaWychOgolnie = 0;
			   long nrpismaWychWdziale = 0;
			   
			   
			   //numer ogulnego pisma wychodzacego 
			   try {
				pst = DSApi.context().prepareStatement("SELECT count(*)  FROM DSO_OUT_DOCUMENT");
			   ResultSet rs = pst.executeQuery();
			 
			   if (rs.next()){
				   nrpismaWychOgolnie  = rs.getLong(1) +1;
				}
			   pst.close();
			   } catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			   // numer pisma w dziale 
			   pst = null;
			   try {
					pst = DSApi.context().prepareStatement("SELECT count(*)  FROM DSO_OUT_DOCUMENT  where divisionguid = ?");
					pst.setString(1, userdivisionguid);
					ResultSet rs = pst.executeQuery();
					 
					   if (rs.next()){
						   nrpismaWychWdziale  = rs.getLong(1) +1;
						}
					   pst.close();
					   } catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					
					
				
			String wartoscPola = dzialUzytkownika +"/" +nrpismaWychWdziale+"/"+nrpismaWychOgolnie+ "/"+rok ;
			
			   
		//	values.put(_znak_pisma, wartoscPola);
			
		*/
		

		if( fm.getDocumentKind().getCn().equals("paadocout") || fm.getDocumentKind().getCn().equals("normal_out")){

			if (fm.getValue("REPILES_FOR")!=null ) {
				fm.getField("REPILES_FOR").setHidden(false);
				fm.getField("REPILES_FOR").setReadOnly(true);
		}else {
			fm.getField("REPILES_FOR").setHidden(true);
			fm.getField("REPILES_FOR").setReadOnly(false);
		}
		}
		
		fm.reloadValues(values);
			
	}
	
	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
			try
			{
				Document doc = Document.find(docId);
				FieldsManager fm = doc.getFieldsManager();
				fm.initialize();

				
				String odbiorcy = fm.getStringKey(_RECIPIENT);
				String[] ids;
				
				if (odbiorcy.startsWith("[")) {
					odbiorcy= odbiorcy.substring(1, odbiorcy.length() - 1);
					ids= odbiorcy.split(",");
				} else {
					ids= new String[1];
					ids[0]= odbiorcy;
				}
				for (String id : ids) {
				Map<String,Object> odbiorca = DwrDictionaryFacade.dictionarieObjects.get(_RECIPIENT).getValues(id.trim());
				
				String asd = (String) odbiorca.get("RECIPIENT_FIRSTNAME");
				
				values.put("TITLE", odbiorca.get("RECIPIENT_TITLE")!=null ? odbiorca.get("RECIPIENT_TITLE") : "" );
				values.put("FIRSTAME", odbiorca.get("RECIPIENT_FIRSTNAME")!=null ? odbiorca.get("RECIPIENT_FIRSTNAME") : "" );
				values.put("LASTNAME", odbiorca.get("RECIPIENT_LASTNAME")!=null ? odbiorca.get("RECIPIENT_LASTNAME") : "" );
				values.put("ORGANIZATION", odbiorca.get("RECIPIENT_ORGANIZATION")!=null ? odbiorca.get("RECIPIENT_ORGANIZATION") : "" );
				values.put("ZIP", odbiorca.get("RECIPIENT_ZIP")!=null ? odbiorca.get("RECIPIENT_ZIP") : "" );
				values.put("LOCATION", odbiorca.get("RECIPIENT_LOCATION")!=null ? odbiorca.get("RECIPIENT_LOCATION") : "" );
				}
		
			
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
	}
	
	
	
	public Map<String, String> setInitialValuesForReplies(Long inDocumentId) throws EdmException
	{
		Map<String, String> dependsDocs = new HashMap<String, String>();
		
		 OfficeDocument doc = OfficeDocument.find(inDocumentId);
		FieldsManager fm =   doc.getFieldsManager();
		
		
		// puki co zakomentowane moze rozszerzenie :)
		/*Long recipientChildId = OfficeDocument.find(inDocumentId).getSender().getId();
		Long recipientBaseId = OfficeDocument.find(inDocumentId).getSender().getBasePersonId();
		Long recipientId;
		
		recipientId = (recipientBaseId != null && Person.findIfExist(recipientBaseId) != null)?recipientBaseId:recipientChildId;

		dependsDocs.put("RECIPIENT", String.valueOf(recipientId));
		//czy user ma swoj dzial
		if (DSApi.context().getDSUser().getDivisionsWithoutGroup().length>0) {
			String sender = "u:"+DSApi.context().getDSUser().getName();
			sender+=(";d:"+DSApi.context().getDSUser().getDivisionsWithoutGroup()[0].getGuid());
			dependsDocs.put("SENDER_HERE", sender);	
		}*/
		String description = "Odpowiedz na pismo o numerze KO: " +OfficeDocument.find(inDocumentId).getOfficeNumber();

		dependsDocs.put(_REPILES_FOR, OfficeDocument.find(inDocumentId).getOfficeNumber().toString());
	
		return dependsDocs;
	}
	
	
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		as.useFolderResolver(document);
		as.useAvailableProviders(document);
		log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
	}

	@Override
	public String onStartManualAssignment(OfficeDocument doc, String fromUsername, String toUsername)
	{
		log.info("--- NormalLogic : onStartManualAssignment  dekretacja reczna dokument闚 :: dokument ID = "+doc.getId()+" : pomiedzy nadawc� -->"+fromUsername +" a odbiorc� ->"+toUsername );
	
		if(!inDepIsOutDep(fromUsername, toUsername)){
				
				log.info("--- NormalLogic : Wynik ->  Nie s� w tym samym  pobieram dyrektora departamentu  ");
				toUsername = getDyrektorDepartamentu(toUsername ,doc);
			/*	if (toUsername.contains(_biuroDyrGeneralnego))
					try {
						toUsername = getDyrektorGeneralny();
					} catch (Exception e) {
						log.error("", e);
					}*/
				log.info("--- NormalLogic : Wynik -> pobra貫m dyrektora -> " + toUsername);
				//if(toUsername.contains(staryOdbiorca)){
				//	toUsername =fromUsername;
			//	}
				
			}
	
		log.info("--- NormalLogic : WYnik ->  ostateczny rezultat  dekretacja na -> "+ toUsername);
		return toUsername;
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
		try {

			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM,event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind()
					.getDockindInfo()
					.getProcessesDeclarations()
					.onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));

			if (document.getFieldsManager().getDocumentKind().getCn()
					.equals("paadocin")|| document.getFieldsManager().getDocumentKind().getCn().equals("normal")
					|| document.getFieldsManager().getDocumentKind().getCn().equals("normal_in")) {
				
				String ZnakPisma = document.getFieldsManager().getStringValue(_znak_pisma);
				InOfficeDocument od = (InOfficeDocument) document;
				if (ZnakPisma != null) {
					od.setReferenceId(ZnakPisma);
				} else {
					od.setReferenceId("Brak");
				}
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

	}
	 
	@Override
	public void validate(Map<String, Object> values, DocumentKind kind,Long documentId) throws EdmException {
		
		System.out.println(kind.getCn());
		System.out.println(values);

		if (kind.getCn().equals("paadocin") || kind.getCn().equals("normal")
				|| kind.getCn().equals("normal_in")) {

			if (documentId != null) {
				OfficeDocument doc = OfficeDocument.find(documentId);
				if (doc.getOfficeNumber() != null) {
					String wpis = "PAA/";
					int rok = Calendar.getInstance().get(Calendar.YEAR);
					values.put(_NUMERPISMA, wpis + doc.getOfficeNumber() + "/"
							+ rok);
				}
			}
		}

	if (kind.getCn().equals("paa_wniosek_urlopowy") && (values.get("FROM") == null || values.get("TO") == null)) {
			throw new EdmException("Daty we wniosku osoby wnioskujacej sa nieprawid這we");
		} else { 
			if (values.get("FROM") != null && values.get("TO") != null){
				List listaDatZast瘼stw = new ArrayList<Date>();
		
				Date UrlopOd = (Date) values.get("FROM");
				Date UrlopDo = (Date) values.get("TO");
				if (UrlopOd.after(UrlopDo)) {
					throw new EdmException(
							"Daty we wniosku osoby wnioskujacej sa nieprawid這we");
				} else {
					Map<String, FieldData> wartosci = (Map<String, FieldData>) values.get(_multiOdbiorca);
					if (wartosci != null) {
						for (String key : wartosci.keySet()) {
								Map<String, FieldData> wpis = (Map<String, FieldData>) wartosci.get(key);
								FieldData fd =  wpis.get("ZAST_PERIOD_FROM");
								Date ZastOd = fd.getDateData();
								listaDatZast瘼stw.add(ZastOd);
								FieldData fd2 = wpis.get("ZAST_PERIOD_TO");
								Date ZastDo = fd2.getDateData();
								listaDatZast瘼stw.add(ZastDo);
						}

						// sortowanie po dacie dokumentu
						if (!listaDatZast瘼stw.isEmpty()) {
							Collections.sort(listaDatZast瘼stw,
									new Comparator<Date>() {
										public int compare(Date listaDatZast瘼stw1,Date listaDatZast瘼stw2) {
											return (listaDatZast瘼stw1.before(listaDatZast瘼stw2)) ? -1: 1;
										}
									});

							Date dataZastPocz = (Date) listaDatZast瘼stw.get(0);
							Date dataZastKonc = (Date) listaDatZast瘼stw
									.get(listaDatZast瘼stw.size() - 1);

							if (!UrlopOd.equals(dataZastPocz)
									&& !UrlopDo.equals(dataZastKonc)) {
								throw new EdmException("Daty we wniosku osoby/os鏏 zast瘼uj鉍ych s� nieprawid這we");
							}
						}
					}
				}
			}
		}

		///*			 MULTI ODBIORCA
	
	/*	if (kind.getCn().equals("paadocout") || kind.getCn().equals("normal_out"))
		{

			if (documentId != null && (values.get("PODZIELPISMA") != null && ((Boolean)values.get("PODZIELPISMA"))))
			{
				OutOfficeDocument doc = (OutOfficeDocument) OfficeDocument.find(documentId);
				if (doc.getOfficeNumber() != null)
				{
					
				
					FieldsManager fm = doc.getFieldsManager();
					Map<String, Object> values2 = fm.getDocumentKind().getf;
					//pobieranie wartosci pul multi	
				Map<String, FieldData> wartosci = (Map<String, FieldData>) values.get("M_DICT_VALUES");
					// jesli jest wiecej ni� jeden odbiorca dzielimy pismo na kilka 
					if (wartosci != null) //&& wartosci.keySet().size()>1
					{
						for (String key : wartosci.keySet() )
						{  

							Map<String, FieldData> wpisMulti = (Map<String, FieldData>) wartosci.get(key);
							if ( wpisMulti.get("RODZAJ_PRZESYLKI") != null && wpisMulti.get("SPOSOB_DOSTARCZENIA") != null)
							{

								int rodzajPrzesylkiId = Integer.parseInt(wpisMulti.get("RODZAJ_PRZESYLKI").getStringData());
								int sposobDostarczeniaId = Integer.parseInt(wpisMulti.get("SPOSOB_DOSTARCZENIA").getStringData());
								FieldData IdPerson = wpisMulti.get("ID");
								FieldData polekwota = wpisMulti.get("KWOTA");
								// dla pierwszego odbiorcy
								if (key.equals("RECIPIENT_1")){
								
									double kwota = policzKwote(polekwota, values, sposobDostarczeniaId, rodzajPrzesylkiId);
									Person person = Person.findIfExist(Long.parseLong(IdPerson.getStringData()));
									Recipient recipient = new Recipient();
									recipient.fromMap(person.toMap());
									recipient.setBasePersonId(person.getId());
									recipient.create();
									if (doc.getRecipients().isEmpty())
									doc.addRecipient(recipient);
									doc.setStampFee(BigDecimal.valueOf(kwota));
									values.put("DOC_DELIVERY", String.valueOf(wpisMulti.get("SPOSOB_DOSTARCZENIA")));
								} else {

								Map<String, Object> mapaOldValuesKopia = new HashMap<String, Object>();
								for (String _key : values.keySet())
									mapaOldValuesKopia.put(_key, values.get(_key));
								//Tworzenie nowego dokumentu wychodz鉍ego 
								OutOfficeDocument nowyDocument = createDokument(doc, mapaOldValuesKopia, wpisMulti);

								List<Journal> journals = Journal.findByDocumentId(doc.getId());
								if (!journals.isEmpty()){
								for (Journal journal : journals)
								{
									bindToJournal(nowyDocument, journal, null);
									doc.setJournal(journal);
								}
								}


								double kwota = policzKwote(polekwota, values, sposobDostarczeniaId, rodzajPrzesylkiId);
								nowyDocument.setStampFee(BigDecimal.valueOf(kwota));

								// po wpisaniu do dzinenika innego ni� g堯wny okreslamy znak pisma
								if (nowyDocument.getFieldsManager().getKey("JOURNAL")!=null){
								 Map<String, Object> fieldValues = new HashMap<String, Object>();
							     fieldValues.put("JOURNAL", nowyDocument.getFieldsManager().getKey("JOURNAL"));
							     fieldValues.put(_znak_pisma, calculateZnakPisma(fieldValues));
								nowyDocument.getDocumentKind().setOnly(nowyDocument.getId(), fieldValues);
								}
								DSApi.context().session().save(nowyDocument);
								DSApi.context().session().update(nowyDocument);
							//	DSApi.context().session().update(nowyDocument);
								Person person = Person.findIfExist(Long.parseLong(IdPerson.getStringData()));
								Recipient recipient = new Recipient();
								recipient.fromMap(person.toMap());
								//String pid = Integer.toString(basePersonId);
								recipient.setBasePersonId(person.getId());
								recipient.create();

								nowyDocument.addRecipient(recipient);
								startNewManualProces(nowyDocument);
								TaskSnapshot.updateByDocumentId(nowyDocument.getId(), "anyType");


							}
							}
							else {
								Map<String, FieldData> wpisSingle =  (Map<String, FieldData>) wartosci.get(key);
								FieldData IdPerson = wpisSingle.get("ID");
								Person person = Person.findIfExist(Long.parseLong(IdPerson.getStringData()));
								Recipient recipient = new Recipient();
								recipient.fromMap(person.toMap());
								//String pid = Integer.toString(basePersonId);
								recipient.setBasePersonId(person.getId());
								recipient.create();
								doc.addRecipient(recipient);
							}

						}

						//	usunOdbiorcow(doc, values);
						Iterator<String> it = wartosci.keySet().iterator();
						while (it.hasNext())
						{
							String key = it.next();
							if (!key.equals("RECIPIENT_1"))
								it.remove();
						}
						
						doc.getDocumentKind().saveDictionaryValues(doc, values);
						doc.getDocumentKind().saveMultiDictionaryValues(doc, values);
						doc.getDocumentKind().set(doc.getId(), values);
						DSApi.context().session().save(doc);


					}
				}
			}
		}

		//KOniec MULTI ODBIORCA
*/
		
	
/*	
	//  STARY KOSZT  POJEDYNCZY ODBIORCA			
				// ustawianie koszt闚 przesy趾i tylko w przypadku gdy wszystkie pola s� wype軟ione
	if (kind.getCn().equals("paadocout")|| kind.getCn().equals("normal_out")) {
		if (values.get(_RODZAJ_PRZESYLKI) != null && values.get(_GABARYT) != null
					&& (values.get(_rodzajPrzesylkiWagaKrajowa) != null || values.get(_rodzajPrzesylkiWagaSwiat) != null))
				{
					boolean country = true;
					if (values.get(_rodzajPrzesylkiWagaKrajowa) == null){
						country = false;
					}
						Integer rodzaj = (Integer) values.get(_RODZAJ_PRZESYLKI);
						Integer gabaryt =(Integer) values.get(_GABARYT);
						// jezeli jest wybrany rodzaj  przesylki krajowej (id <2) a gabaryt okre�lony jako swiat (id > 2) - error
						if ((rodzaj <= 2 && gabaryt >= 3 )|| (rodzaj >= 3 && gabaryt <= 2)){
							throw new EdmException("Pole 'Rodzaj przesy趾i' oraz 'Gabaryt' s� niepoprawnie okre�lone");
						}
					
					List<Integer> cnList = new ArrayList();
					cnList.add((Integer) (values.get(_RODZAJ_PRZESYLKI)));
					cnList.add((Integer) (values.get(_GABARYT)));
					cnList.add((Integer) (values.get("DOC_DELIVERY")));
					if(country)
					cnList.add((Integer) (values.get(_rodzajPrzesylkiWagaKrajowa)));
					else
					cnList.add((Integer) (values.get(_rodzajPrzesylkiWagaSwiat)));
					
					//lista string闚 do selecta 
					List<String> selects = new ArrayList();
					
					List<Integer> intList= new ArrayList();
					
					double kwota= 0.0;
					selects.add( "select poziomPrzesy趾i from DS_OUT_DELIVERY_DOC_RODZAJ where cn = ?");
					selects.add( "select poziomGabarytu from DS_OUT_DELIVERY_GABARYT where cn = ?");
					selects.add( "select poziomListu from dso_out_document_delivery where id = ?");
					if(country)
					selects.add( "select poziomWagi from DS_OUT_DELIVERY_COUNTRY_WEIGHT where cn = ?");
					else
					selects.add( "select poziomWagi from DS_OUT_DELIVERY_WORLD_WEIGHT where cn = ?");
					 int id = 0;
					for ( String select : selects){
						Integer cn = cnList.get(id);
						id++;
						 PreparedStatement pst = null;
						   try {
							pst = DSApi.context().prepareStatement(select);
							pst.setInt(1,cn);
							ResultSet rs = pst.executeQuery();
							
						   if (rs.next()){
							   intList.add(rs.getInt(1));
							}
						   pst.close();
						   } catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
					}
					 PreparedStatement pst = null;
					 String selectKwota = "select kwota from DS_STAWKI_OPLAT_POCZTOWYCH where poziomPrzesy趾i = ? and poziomGabarytu = ? and poziomListu = ? and poziomWagi = ?";
					 try {
						pst = DSApi.context().prepareStatement(selectKwota);
						int rodzajListu = intList.get(2);
						if(values.get("ZPO") != null && ((Boolean)values.get("ZPO")))
							rodzajListu = rodzajListu+1;							
						
						 pst.setInt(1,intList.get(0));
						 pst.setInt(2,intList.get(1));
						 pst.setInt(3,rodzajListu);
						 pst.setInt(4,intList.get(3));
						 ResultSet rs = pst.executeQuery();
						 if (rs.next()){
							   kwota = rs.getDouble(1);
							   
							}
					 } catch (SQLException e) {
							// TODO Auto-generated catch block
							log.error(" ", e);
							throw new EdmException("Nieznaleziono kwoty dla podanych parametr闚 typu przesy趾i");
						}
					
				values.put("KOSZT_PRZESYLKI" ,(BigDecimal.valueOf(kwota)));	
				}
			} 
// KONIEC STARY KOSZT 
		
	*/
				
				
		/*if (kind.getCn().equals("paadocout") || kind.getCn().equals("normal_out"))
		{

			if (values.get(_znak_pisma) == null && values.get("JOURNAL") != null)
			{int count = 0;
				String znakPisma = calculateZnakPisma(values,count );
				values.put(_znak_pisma, znakPisma);
			}
		}*/
		
	if (documentId != null &&( kind.getCn().equals("paadocout")||  kind.getCn().equals("normal_out"))) {
			StringBuilder sb = new StringBuilder();
			OfficeDocument doc = OfficeDocument.find(documentId);
			List<Recipient> list = doc.getRecipients();
			if (!list.isEmpty() && list.size() == 1){
				for (Recipient r : list) {
					sb.append(r.getOrganization() != null ? r.getOrganization(): "");
					sb.append(", ");
					sb.append(r.getFirstname() != null ? r.getFirstname() : "");
					sb.append(" ");
					sb.append(r.getLastname() != null ? r.getLastname() : "");
					sb.append(", ");
					sb.append(r.getStreet() != null ? r.getStreet() : "");
					sb.append(" ");
					sb.append(r.getZip() != null ? r.getZip() : "");
					sb.append(", ");
					sb.append(r.getLocation() != null ? r.getLocation() : "");
					
					values.put("PELENADRES", sb.toString());
				}
			} else if (!list.isEmpty())
				values.put("PELENADRES", "Multi odbiorca");
			else 
				values.put("PELENADRES", "");

		}
	if (documentId != null && (kind.getCn().equals("normal")|| kind.getCn().equals("paadocin")
				|| kind.getCn().equals("normal_in"))) {
			StringBuilder sb = new StringBuilder();
			OfficeDocument doc = OfficeDocument.find(documentId);
			if (doc.getSender() != null) {
				Sender sender = doc.getSender();
				sb.append(sender.getOrganization() != null ? sender.getOrganization() : "");
				sb.append(",");
				sb.append(sender.getFirstname() != null ? sender.getFirstname(): "");
				sb.append(" ");
				sb.append(sender.getLastname() != null ? sender.getLastname(): "");
				sb.append(", ");
				sb.append(sender.getStreet() != null ? sender.getStreet() : "");
				sb.append(" ");
				sb.append(sender.getZip() != null ? sender.getZip() : "");
				sb.append(", ");
				sb.append(sender.getLocation() != null ? sender.getLocation(): "");
				values.put("PELENADRES", sb.toString());
			}
		}
	}
	 

	/*public void podzielDokumenty(OutOfficeDocument doc) throws EdmException
	{
		if (doc.getOfficeNumber() != null)
		{


			FieldsManager fm = doc.getFieldsManager();
			Map<String, Object> values = fm.getFieldValues();
			//pobieranie wartosci pul multi
			Map<String, FieldData> wartosci = (Map<String, FieldData>) values.get("RECIPIENT");

			// jesli jest wiecej ni� jeden odbiorca dzielimy pismo na kilka 
			if (wartosci != null) //&& wartosci.keySet().size()>1
			{
				for (String key : wartosci.keySet())
				{

					Map<String, FieldData> wpisMulti = (Map<String, FieldData>) wartosci.get(key);
					if (wpisMulti.get("RODZAJ_PRZESYLKI") != null && wpisMulti.get("SPOSOB_DOSTARCZENIA") != null)
					{

						int rodzajPrzesylkiId = Integer.parseInt(wpisMulti.get("RODZAJ_PRZESYLKI").getStringData());
						int sposobDostarczeniaId = Integer.parseInt(wpisMulti.get("SPOSOB_DOSTARCZENIA").getStringData());
						FieldData IdPerson = wpisMulti.get("ID");
						FieldData polekwota = wpisMulti.get("KWOTA");

						// dla pierwszego odbiorcy
						if (key.equals("RECIPIENT_1"))
						{

							double kwota = policzKwote(polekwota, values, sposobDostarczeniaId, rodzajPrzesylkiId);
							Person person = Person.findIfExist(Long.parseLong(IdPerson.getStringData()));
							Recipient recipient = new Recipient();
							recipient.fromMap(person.toMap());
							recipient.setBasePersonId(person.getId());
							recipient.create();
							if (doc.getRecipients().isEmpty())
								doc.addRecipient(recipient);
							doc.setStampFee(BigDecimal.valueOf(kwota));
							values.put("DOC_DELIVERY", String.valueOf(wpisMulti.get("SPOSOB_DOSTARCZENIA")));
						} else
						{

							Map<String, Object> mapaOldValuesKopia = new HashMap<String, Object>();
							for (String _key : values.keySet())
								mapaOldValuesKopia.put(_key, values.get(_key));
							//Tworzenie nowego dokumentu wychodz鉍ego 
							OutOfficeDocument nowyDocument = createDokument(doc, mapaOldValuesKopia, wpisMulti);

							List<Journal> journals = Journal.findByDocumentId(doc.getId());
							for (Journal journal : journals)
							{
								bindToJournal(nowyDocument, journal, null);
								doc.setJournal(journal);
							}


							double kwota = policzKwote(polekwota, values, sposobDostarczeniaId, rodzajPrzesylkiId);
							nowyDocument.setStampFee(BigDecimal.valueOf(kwota));

							// po wpisaniu do dzinenika okreslamy znak pisma

							Map<String, Object> fieldValues = new HashMap<String, Object>();
							fieldValues.put("JOURNAL", nowyDocument.getFieldsManager().getKey("JOURNAL"));

							fieldValues.put(_znak_pisma, calculateZnakPisma(fieldValues));

							nowyDocument.getDocumentKind().setOnly(nowyDocument.getId(), fieldValues);

							DSApi.context().session().save(nowyDocument);
							DSApi.context().session().update(nowyDocument);
							//	DSApi.context().session().update(nowyDocument);
							Person person = Person.findIfExist(Long.parseLong(IdPerson.getStringData()));
							Recipient recipient = new Recipient();
							recipient.fromMap(person.toMap());
							//String pid = Integer.toString(basePersonId);
							recipient.setBasePersonId(person.getId());
							recipient.create();

							nowyDocument.addRecipient(recipient);
							startNewManualProces(nowyDocument);
							TaskSnapshot.updateByDocumentId(nowyDocument.getId(), "anyType");
							DSApi.context().session().save(nowyDocument);

						}
					} else
					{
						Map<String, FieldData> wpisSingle = (Map<String, FieldData>) wartosci.get(key);
						FieldData IdPerson = wpisSingle.get("ID");
						Person person = Person.findIfExist(Long.parseLong(IdPerson.getStringData()));
						Recipient recipient = new Recipient();
						recipient.fromMap(person.toMap());
						//String pid = Integer.toString(basePersonId);
						recipient.setBasePersonId(person.getId());
						recipient.create();
						doc.addRecipient(recipient);
					}

				}

				//	usunOdbiorcow(doc, values);
				Iterator<String> it = wartosci.keySet().iterator();
				while (it.hasNext())
				{
					String key = it.next();
					if (!key.equals("RECIPIENT_1"))
						it.remove();
				}

				doc.getDocumentKind().saveDictionaryValues(doc, values);
				doc.getDocumentKind().saveMultiDictionaryValues(doc, values);
				doc.getDocumentKind().set(doc.getId(), values);
				DSApi.context().session().save(doc);


			}
		}

	}*/

	/*static String calculateZnakPisma(Map<String, Object> values ,int count, Long documentId)
	{
		
		String znakPisma = "Brak";
		if (values.get("JOURNAL")!=null){
		long idDziennika = 0;
		try
		{
			String dzialUzytkownika = "brak";
			int rok = Calendar.getInstance().get(Calendar.YEAR);
			
			try {
			 idDziennika = (Long) values.get("JOURNAL");
			}catch (Exception e) {
				idDziennika = Long.parseLong((String) values.get("JOURNAL").toString());
				}
			
			Journal journal = Journal.find(idDziennika);
			long sequenceID = journal.getSequenceId();
			String symbolDiennika = journal.getSymbol();
			long nrpismaWychOgolnie = calculateWychOgolnie(documentId);

			//long  = calculateDzial(journal);

			dzialUzytkownika = symbolDiennika;
			long nrpismaWychWdziale = sequenceID + count;

			znakPisma = dzialUzytkownika + "/" + nrpismaWychWdziale + "/" + nrpismaWychOgolnie + "/" + rok;

		} catch (EdmException e)
		{
			log.error("", e);
		}
		}
		return znakPisma;
	}*/

	static double policzKwote(FieldData polekwota, Map<String, Object> values, int sposobDostarczeniaId, int rodzajPrzesylkiId) throws EdmException
	{
		double oplata = 0;
		if (!(sposobDostarczeniaId == 1 || sposobDostarczeniaId == 19))
		{
			polekwota.setStringData("0");
		} else
		{
			// ustawianie koszt闚 przesy趾i tylko w przypadku gdy wszystkie pola s�  wype軟ione
			if (values.get("GABARYT") != null
					&& (values.get("RODZAJ_PRZESYΘI_TYPE_WEIGHT_COUNTRY") != null || values.get("RODZAJ_PRZESYΘI_TYPE_WEIGHT_WORLD") != null))
			{
				boolean country = true;
				if (values.get("RODZAJ_PRZESYΘI_TYPE_WEIGHT_COUNTRY") == null)
					country = false;
				
			// wyliczanie koszt闚 i wpisanie do fielda
			 oplata = calculateCosts(values, rodzajPrzesylkiId, sposobDostarczeniaId, country, polekwota);
			 polekwota.setData(String.valueOf(oplata));
			}
		}
		return oplata;
	}

	static void startNewManualProces(OfficeDocument document) {
		
		 try {
		Map<String,Object> params = Maps.newHashMap();
			params.put(Jbpm4Constants.KEY_PROCESS_NAME,Jbpm4Constants.MANUAL_PROCESS_NAME);
			params.put(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM,DSApi.context().getPrincipalName());
			params.put(Jbpm4WorkflowFactory.DOCDESCRIPTION,KIND_EXECUTE);
		   // params.put(Jbpm4WorkflowFactory.OBJECTIVE, );
		   
			document.getDocumentKind()
				.getDockindInfo()
				.getProcessesDeclarations()
				.getProcess(Jbpm4Constants.MANUAL_PROCESS_NAME)
				.getLogic()
				.startProcess(document, params);
		 
		   // doc.getDocumentKind().logic().onStartProcess(doc);
		 } catch (EdmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	/*private OutOfficeDocument setValuesOnNewDocument(OutOfficeDocument document,
			Map<String, Object> OldValues, Map<String, FieldData> mDictValues, double oplata) {

		try {
			
			FieldsManager fm = document.getFieldsManager();
			fm.initialize();
			
			Map<String, Object> newDocValues = new HashMap<String, Object>(); 
			newDocValues.putAll(OldValues);
			// DwrDictionaryBase odbiorca = DwrDictionaryFacade.getDwrDictionary(document.getDocumentKind().getCn(), "_RECIPIENT");
			//newDocValues.put(_kosztJednostkowy, String.valueOf(oplata));
			//OldValues.entrySet();
		
		//	newDocValues.putAll(mDictValues);
			Map<String, FieldData> kopiowanaMapa = ((Map<String, FieldData>)newDocValues.get(_multiOdbiorca));
			Map<String, FieldData> noweWartosci = new HashMap<String, FieldData>();
			for( String _key : kopiowanaMapa.keySet())
				noweWartosci.put(_key, kopiowanaMapa.get(_key));
			 int ilosc = noweWartosci.size();
			
			 Iterator<String> it = noweWartosci.keySet().iterator();
			 while(it.hasNext()){
				 String key= it.next();
				 Map<String, FieldData> newWpisMulti = new  HashMap<String, FieldData>();
				 newWpisMulti  = (Map<String, FieldData>) noweWartosci.get(key);
				 if (!newWpisMulti.toString().contains(mDictValues.toString()) )
					
					 it.remove();
			 }
			 newDocValues.put(_multiOdbiorca,noweWartosci);
			 fm.reloadValues(newDocValues);
			// document.getFieldsManager().getFieldValues().putAll(noweWartosci);
			 document.setStampFee(BigDecimal.valueOf(oplata));
			 for (String key  : noweWartosci.keySet())
			 {
				 noweWartosci.containsKey(key);
			
			 }
			 DSApi.context().session().update(document);

			 TaskSnapshot.updateByDocumentId(document.getId(), "anyType");
			
		} catch (EdmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return document;
	}*/

private static long calculateWychOgolnie() throws EdmException {
	 PreparedStatement pst = null;
	 long nr = 0;
		// numer ogulnego pisma wychodzacego
			try {
				pst = DSApi.context().prepareStatement(
						"SELECT count(*)  FROM DSO_OUT_DOCUMENT");
				ResultSet rs = pst.executeQuery();

				if (rs.next()) {
					 nr = rs.getLong(1) + 1;
				}
				pst.close();
			} catch (SQLException e) {
				log.error("", e);
				e.printStackTrace();
			}
		return nr;
	}

 private long calculateDzial(Journal journal) throws EdmException {
	PreparedStatement pst = null;
	long nr = 0;
	try {
		pst = DSApi.context().prepareStatement(
						"SELECT count(*)  FROM DSO_OUT_DOCUMENT  where divisionguid = ?");
		pst.setString(1, journal.getOwnerGuid());
		ResultSet rs = pst.executeQuery();
		if (rs.next()) {
			nr = rs.getLong(1) + 1;
		}
		pst.close();
	} catch (SQLException e) {
		log.error("", e);
		e.printStackTrace();
	}
	return nr;
	}


 static double calculateCosts(Map<String, Object> values,
			int rodzajPrzesylkiId, int sposobDostarczeniaId, boolean country, FieldData polekwota) throws EdmException {
		
		List<Integer> cnList = new ArrayList();
		cnList.add(rodzajPrzesylkiId);
		cnList.add((Integer) (values.get("GABARYT")));
		cnList.add(sposobDostarczeniaId);
		if(country)
		cnList.add((Integer) (values.get("RODZAJ_PRZESYΘI_TYPE_WEIGHT_COUNTRY")));
		else
		cnList.add((Integer) (values.get("RODZAJ_PRZESYΘI_TYPE_WEIGHT_WORLD")));
		//lista string闚 do selecta 
		List<String> selects = new ArrayList();
		List<Integer> intList= new ArrayList();
		
		selects.add( "select poziomPrzesy趾i from DS_OUT_DELIVERY_DOC_RODZAJ where cn = ?");
		selects.add( "select poziomGabarytu from DS_OUT_DELIVERY_GABARYT where cn = ?");
		selects.add( "select poziomListu from dso_out_document_delivery where id = ?");
		if(country)
		selects.add( "select poziomWagi from DS_OUT_DELIVERY_COUNTRY_WEIGHT where cn = ?");
		else
		selects.add( "select poziomWagi from DS_OUT_DELIVERY_WORLD_WEIGHT where cn = ?");
		 int id = 0;
		for ( String select : selects){
			Integer cn = cnList.get(id);
			id++;
			 PreparedStatement pst = null;
			   try {
				pst = DSApi.context().prepareStatement(select);
				pst.setInt(1,cn);
				ResultSet rs = pst.executeQuery();
				
			   if (rs.next()){
				   intList.add(rs.getInt(1));
				}
			   pst.close();
			   } catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		double oplata= 0.0;
		 PreparedStatement pst = null;
		 String selectKwota = "select kwota from DS_STAWKI_OPLAT_POCZTOWYCH where poziomPrzesy趾i = ? and poziomGabarytu = ? and poziomListu = ? and poziomWagi = ?";
		 try {
			pst = DSApi.context().prepareStatement(selectKwota);
			int rodzajListu = intList.get(2);
			if(values.get("ZPO") != null && ((Boolean)values.get("ZPO")))
				rodzajListu = rodzajListu+1;							
			
			 pst.setInt(1,intList.get(0));
			 if(isPackage(values))
				 pst.setInt(2,cnList.get(1));
			 else
				 pst.setInt(2,intList.get(1));
			 pst.setInt(3,rodzajListu);
			 pst.setInt(4,intList.get(3));
			 ResultSet rs = pst.executeQuery();
			 if (rs.next()){
				 oplata = rs.getDouble(1);
				 polekwota.setStringData (String.valueOf(oplata));
				}
			} catch (SQLException e) {
				log.error(" ", e);
				throw new EdmException("Nieznaleziono kwoty dla podanych parametr闚 typu przesy趾i");
			}
			values.put("KOSZT_PRZESYLKI" ,(BigDecimal.valueOf(oplata)));
			return oplata;
	}



	private static boolean isPackage(Map<String, Object> values) throws EdmException {
		if (values.get(_GABARYT) != null)
		{
			int gab = (Integer) values.get(_GABARYT);
			if (gab == 7 || gab == 8)
			{
				if (!sprawdzCzyPojedynczyOdbiorca(values))
					throw new EdmException("Przesy趾a jako Paczka mo瞠 miec tylko jednego odbiorce! ");
				else
					return true;
			} else
			{
				return false;
			}
		}
	return false;
	}
	
	private static boolean sprawdzCzyPojedynczyOdbiorca(Map<String, Object> values)
	{
		Map<String, FieldData> wartosci = (Map<String, FieldData>) values.get(_multiOdbiorca);
		if (wartosci != null && wartosci.size() == 1)
			return true;

		return false;
	}

	/*public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
	{
		System.out.println("data pisma onsavaaction" + fieldValues.get("DOC_DATE"));

		if ((kind.getCn().equals("normal") || kind.getCn().equals("normal_int")) && fieldValues.get("RECIPIENT_HERE") == null)
		{
			throw new EdmException("Nie istnieje taki odbiorca w systemie.");
		}

		if (kind.getCn().equals("normal_out") && fieldValues.get("SENDER_HERE") == null)
		{
			throw new EdmException("Nie istnieje taki nadawca w systemie.");

		}

		if (fieldValues.get("DOC_DATE") != null)
		{
			System.out.println("data pisma onsavaaction" + fieldValues.get("DOC_DATE"));
			Date startDate = new Date();
			Date finishDate = (Date) fieldValues.get("DOC_DATE");

			if (finishDate.after(startDate))
			{
				throw new EdmException("Data pisma nie mo瞠 by� dat� z przysz這�ci.");
			}
		}
	}

	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}
	*/
	@Override
	public void onStartProcess(OfficeDocument document)
	{
	
		try {
			Map<String, Object> map= Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, document.getAuthor());
			map.put(ASSIGN_DIVISION_GUID_PARAM, document.getDivisionGuid());
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	private EmployeeCard getEmployeeCard(UserImpl userImpl) throws Exception
	{
		return (EmployeeCard) DSApi.context().session().createQuery(
				"from " + EmployeeCard.class.getName() + " e where e.user = ?")
				.setParameter(0, userImpl)
				.setMaxResults(1)
				.uniqueResult();
	}
	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret= new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	@Override
	public void onRejectedListener(Document doc) throws EdmException
	{
	}

	@Override
	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException
	{
	}
	
	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException,
			EdmException
	{ 
		
		
	}
	
	 public static String getDyrektorGeneralny() throws Exception
		{
			log.info("Pobieram dyrektora generalnego ");
				List<DSDivision> divisions= DSDivision.findByDescription("DYR");
				for( DSDivision div : divisions ){
					// dyrektor generalny to ten, ktorego nadrzednym jest PREZES
					if( div.getParent()!=null && !div.getParent().isHidden() 
							&& !StringUtils.isEmpty(div.getParent().getDescription())
							&& div.getParent().getDescription().equals("PREZES")){
						return div.getUsers()[0].getName();
					}
				}
				return null;
		}
		/**
		 * Wyszukiwanie  dyrektora departamentu nadawcy - 
		 * je瞠li znajdzie to dekretuje na dyrektora 
		 * w przeciwnym wypadku  dekretuje na nadawce 
		 * @param userNameNadawcyPisma
		 * @param doc 
		 * @return nazwaDyrektora
		 */

		public static String getDyrektorDepartamentu(String userNameNadawcyPisma, OfficeDocument doc)
		{
			
			try {
				DSUser odbiorca= DSUser.findByUsername(userNameNadawcyPisma);
				ArrayList<DSDivision> listaDzia這wOdbiorcy= odbiorca.getDivisionsAsList();
				List<String> nazwaDivOdbiorcy= new ArrayList<String>();
				List<String> nazwaDivPrezesa = new ArrayList<String>();
				ArrayList<DSUser> listaUserow= new ArrayList<DSUser>();
				DSDivision division= null;
				boolean podDyrGeneralnym = false;
				
			for (DSDivision divodb : listaDzia這wOdbiorcy) {
				if (!divodb.isGroup()) {
					DSDivision currentOdb = divodb;
					while (currentOdb.getParent() != null) {
						if (currentOdb.getDescription() != null && currentOdb.getDescription().contains("DEP")) {
							nazwaDivOdbiorcy.add(currentOdb.getGuid());
							break;
						} else if (CzyJestPodDyrgeneralnym(currentOdb))
								 {
							podDyrGeneralnym = true;
							break;
						} else {
							currentOdb = currentOdb.getParent();
						}
					}
				}
			}
				 //wyszukiwanie stanowiska dydektora departamentu 
				for (String y : nazwaDivOdbiorcy) {
					division= DSDivision.find(y);
					DSDivision[] stanowiska = null;
					try {
					stanowiska = division.getChildren("position");
					} catch (EdmException e) {
						throw new EdmException("Nie znaleziono stanowiska Dyrektora Departamentu "+ division.getName() != null ? division
								.getName() : "Brak dzia逝  - lub Dyrektor nie zosta� zdefiniowany w bazie danych" 
								+ "Dekretuj� pismo na U篡tkownika dekretuj鉍ego ");
					}
					for (DSDivision stanowisko : stanowiska) {
						if (stanowisko.getId() != null && stanowisko.getDescription()!=null && stanowisko.getDescription().contains("DYR")) {
							DSUser[] users= stanowisko.getUsers();
							if (users != null) {
								for (DSUser user : users) {
									listaUserow.add(user);
								}
							}
						}
					}
				}

				if (!listaUserow.isEmpty()) {
					for (DSUser user : listaUserow) {
						userNameNadawcyPisma= user.getName();
					}
					
				} else if (!nazwaDivPrezesa.isEmpty()){
						 userNameNadawcyPisma = "wlodarski";
				}
				else {
					log.error("Nie znaleziono dyrektora Departamentu - Sprawdzam czy u篡tkownik znajduje sie w biurze dyrektora generalnego ");
					
							try {
								if (podDyrGeneralnym){
									log.error("urzytkownik w BDG do  dekretacji na dyrektora generalnego");
									userNameNadawcyPisma = getDyrektorGeneralny();
								}
								
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
					
					}
			} catch (EdmException e) {
			
				// TODO Auto-generated catch block
				e.printStackTrace();
				return userNameNadawcyPisma;
			}

			return userNameNadawcyPisma;
		}
		/**
		 * Sprawdza czy uzytkownik jest bezposrednio pod dyrektorem generalnym
		 * @param currentOdb
		 * @return
		 * @throws EdmException
		 */

private static boolean CzyJestPodDyrgeneralnym(DSDivision currentOdb) throws  EdmException {
			
			if(currentOdb.getParent().getDescription() != null&& currentOdb.getParent().isDivision()
			&& currentOdb.getParent().getCode() != null && currentOdb.getParent().getCode().equals("DG")
			&& currentOdb.getParent().getDescription().equals("DYR"))
			return true;
			else
			 return false;
	}

		/**
		 * Pobiera dyrektora departamentu przy pismach przychodzacych 
		 * @param userNameNadawcyPisma
		 * @return dyrektor  albo odbiorca
		 *//*
		public static String pismaPrzychgetDyrektorDepartamentu(String userNameNadawcyPisma)
		{
			try {
				DSUser odbiorca= DSUser.findByUsername(userNameNadawcyPisma);
				ArrayList<DSDivision> listaDzia這wOdbiorcy= odbiorca.getDivisionsAsList();
				List<String> nazwaDivOdbiorcy= new ArrayList<String>();
				List<String> nazwaDivPrezesa = new ArrayList<String>();
				ArrayList<DSUser> listaUserow= new ArrayList<DSUser>();
				DSDivision division= null;
				boolean podDyrGeneralnym = false;
				
				for (DSDivision divodb : listaDzia這wOdbiorcy) {
					DSDivision currentOdb =divodb;
					while (currentOdb.getParent() != null){
							
							if (currentOdb.getParent().getDescription()!=null && currentOdb.getParent().isDivision() 
									&& currentOdb.getParent().getCode()!=null && currentOdb.getParent().getCode().equals("DG") 
									&& currentOdb.getParent().getDescription().equals("DYR")){
								podDyrGeneralnym = true;
							break;	
							}
						if (doc.getDocumentKind().getCn().equals("paadocin") && currentOdb.getCode()!=null 
								&& currentOdb.getCode().equals("GP") && currentOdb.getParent().getDescription().equals("PREZES")){
							nazwaDivPrezesa.add(currentOdb.getParent().getGuid());
						break;
						}
						if (currentOdb.getDescription() != null && currentOdb.getDescription().contains("DEP")) {
							nazwaDivOdbiorcy.add(currentOdb.getGuid());
							break;
						} else {
							currentOdb= currentOdb.getParent();
						}
				}
				}
				 
				for (String y : nazwaDivOdbiorcy) {
					division= DSDivision.find(y);
					DSDivision[] stanowiska = null;
					try {
					stanowiska = division.getChildren("position");
					} catch (EdmException e) {
						throw new EdmException("Nie znaleziono stanowiska Dyrektora Departamentu "+ division.getName() != null ? division
								.getName() : "Brak dzia逝  - lub Dyrektor nie zosta� zdefiniowany w bazie danych" 
								+ "Dekretuj� pismo na U篡tkownika dekretuj鉍ego ");
					}
					for (DSDivision stanowisko : stanowiska) {
						if (stanowisko.getId() != null && stanowisko.getDescription()!=null && stanowisko.getDescription().contains("DYR")) {
							DSUser[] users= stanowisko.getUsers();
							if (users != null) {
								for (DSUser user : users) {
									listaUserow.add(user);
								}
							}
						}
					}
				}

				if (!listaUserow.isEmpty()) {
					for (DSUser user : listaUserow) {
						userNameNadawcyPisma= user.getName();
					}
					
				} else if (!nazwaDivPrezesa.isEmpty()){
						 userNameNadawcyPisma = "wlodarski";
				}
				else {
					log.error("Nie znaleziono dyrektora Departamentu - Sprawdzam czy u篡tkownik znajduje sie w biurze dyrektora generalnego ");
					
							try {
								if (podDyrGeneralnym){
									log.error("urzytkownik w BDG do  dekretacji na dyrektora generalnego");
								userNameNadawcyPisma="WBDG";
								}
								
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
					
					}
			} catch (EdmException e) {
			
				// TODO Auto-generated catch block
				e.printStackTrace();
				return userNameNadawcyPisma;
			}

			return userNameNadawcyPisma;
		}
		*/
	  
		/**
		 * sprawdza czy uzytkownik znajduje si�  bezpo�rednio w departamencie
		 * 
		 * @param user
		 * @return
		 */
		public static boolean  isBezposrednioWdepartamencie (DSUser user){
			DSDivision[] divisions= null;
			try {
				divisions= user.getOriginalDivisionsWithoutGroup();
			} catch (EdmException e) {
				// TODO Auto-generated catch block
				return false;
			}
			for (DSDivision div  : divisions){
				if(div.getDescription()!=null && div.getDescription().equals("DEP"))
					return true;
				}
			return false;
		}
		
		/**
		 * Weryfikacja czy nadawca jest w tym samym departamencie co odbiorca 
		 * @param fromUsername
		 * @param toUsername
		 * @return boolean
		*/
		public static boolean inDepIsOutDep(String fromUsername, String toUsername)
		{
		
			try {
				// pobieranie nadawcy
				DSUser nadawca= DSUser.findByUsername(fromUsername);
				// pobieranie odbiorcy
				DSUser odbiorca= DSUser.findByUsername(toUsername);

				// tworze listy dzia堯w nadawcy i odbiorcy
				List<String> nazwaDivNadawcy= new ArrayList<String>();
				List<String> nazwaDivOdbiorcy= new ArrayList<String>();
				ArrayList<DSDivision> listaDzia這wNadawcy= nadawca.getDivisionsAsList();
				ArrayList<DSDivision> listaDzia這wOdbiorcy= odbiorca.getDivisionsAsList();
				
				// pobieram lista dzia堯w nadawcy
				for (DSDivision currentNad : listaDzia這wNadawcy) {
					if (!currentNad.isGroup()) {
						while (currentNad.getParent() != null) {
							if (currentNad.getCode() != null&& currentNad.getCode().equals("GP")) {
								nazwaDivNadawcy.add(currentNad.getGuid());
								break;
							} else if (currentNad.getDescription() != null&& currentNad.getDescription().contains("DEP")) {
								nazwaDivNadawcy.add(currentNad.getGuid());
								break;
							} else if (currentNad.getCode() != null&& currentNad.getCode().equals("DG")) {
								nazwaDivNadawcy.add(currentNad.getGuid());
								break; 
							} else {
								currentNad = currentNad.getParent();
							}
						}
					}
				}
					// pobieram list� dzia堯w odbiorcy
					for (DSDivision currentOdb : listaDzia這wOdbiorcy) {
						if (!currentOdb.isGroup()) {
						while (currentOdb.getParent() != null)
							if (currentOdb.getCode()!=null && currentOdb.getCode().equals("GP")){
								nazwaDivOdbiorcy.add(currentOdb.getGuid());
							break;
							}else if (currentOdb.getDescription() != null && currentOdb.getDescription().contains("DEP")) {
								nazwaDivOdbiorcy.add(currentOdb.getGuid());
								break;
							}else if (currentOdb.getCode()!=null && currentOdb.getCode().equals("DG")){
								nazwaDivOdbiorcy.add(currentOdb.getGuid());
								break;
							} else {
								currentOdb= currentOdb.getParent();
							}
					}
						}

					// sprawdzam czy nadawca i odbiorca s� w ty msamym departamencie
					boolean wTymSamym= false;
					for (String x : nazwaDivNadawcy){
						for (String y : nazwaDivOdbiorcy) {
							if (x.contains(y)) {
								wTymSamym= true;
								break;
							}

						}
					}
					if (wTymSamym)
					log.info("--- NormalLogic : s� w tym samym departamencie ");
					return (wTymSamym);

				
			} catch (Exception e) {}
			return false;
		}
		
		/**
		 * Sprawdza czy user to pracownik sekretariatu
		 * i zwrata true/ false
		 * @param username
		 * @return
		 */
	public static boolean czyUserToPracSekretariatu(String username)
		{
		boolean pracSekret = false;
			try {
				// pobieranie nadawcy
				DSUser nadawca= DSUser.findByUsername(username);
				
				List<Long> listaIdRulUzytkownika= Role.listUserRoles(nadawca.getName());
				Role rola= Role.findByName("Sekretar%");
				if (listaIdRulUzytkownika.contains(rola.getId())) 
					pracSekret = true;
			}catch (Exception e) {
			log.error("B豉d przy sprawdzaniu czy user to pracownik sekretariatu", e);
			}
		return pracSekret;
	}
		public static List<String> getDepartamentUzytkownika(DSUser user, List<String> listaDzialow ){
		
			// tworze listy dzia堯w  bedacych w departamencie u篡tkownika
			//List<String> nazwaDivNadawcy= new ArrayList<String>();
			try {
			ArrayList<DSDivision> listaDzia這wNadawcy = user.getDivisionsAsList();
			
			
			// pobieram lista dzia堯w nadawcy
			for (DSDivision currentNad : listaDzia這wNadawcy) {
				if (!currentNad.isGroup()){
				while (currentNad.getParent() != null){
					if (currentNad.getCode()!=null && currentNad.getCode().equals("GP")){
						listaDzialow.add(currentNad.getGuid());
						break;
					}
					else if (currentNad.getCode()!=null && currentNad.getCode().equals("DG")){
							listaDzialow.add(currentNad.getGuid());
							break;
						}
						
					else if (currentNad.getDescription() != null && currentNad.getDescription().contains("DEP")) {
						listaDzialow.add(currentNad.getGuid());
						break;
					} else {
						currentNad= currentNad.getParent();
					}
			}
				}
				
			}
			if (!listaDzialow.isEmpty()){
				DSDivision div =DSDivision.find(listaDzialow.get(0));
				
				DSDivision[] divs = div.getChildren("division");
				for( DSDivision  d :divs){
					listaDzialow.add(d.getGuid());
				}
			}
			
			}catch (EdmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return listaDzialow;
		}
		
		
public static boolean hasRoleRadca(DSUser user) throws EdmException{
			 
			
			  List<Long> listaIdRulUzytkownika= Role.listUserRoles(user.getName());
			  
			 Role rola= null;
			try {
				rola= Role.findByName("Radca%prezesa");
			} catch (EdmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 if (rola!=null && listaIdRulUzytkownika!=null && listaIdRulUzytkownika.contains(rola.getId())) {
				 return true; 
			 } else
			return false;
		}
		
		/**
		 *  wyszukiwanie u篡tkownika posiadaj鉍ego role sekretariat w departamencie dyrektora po id roli
		 * @param dyrektor lub username
		 * @return sekretariat dyrektora( osoba pe軟i鉍a funkcje sekretariat)
		 * @throws UserNotFoundException
		 * @throws EdmException
		 */
		
		public static String getUserPelniocyRoleSekretariatu(String dyrektor) throws UserNotFoundException, EdmException
		{
			String retStr= dyrektor;
			boolean zawiera= false;
			DSUser dyrektorDep= DSUser.findByUsername(dyrektor);
			
			 if (dyrektorDep.getName().equals("czarnecki")){
				 retStr = "sobiecka";
				 return retStr;
				}
			 if (dyrektorDep.getName().equals("wlodarski")){
				 retStr = "igareszke";
				 return retStr;
				}
			// departament dyrektora
			DSDivision[] departamentDyr= dyrektorDep.getDivisions();
			
			for (DSDivision division : departamentDyr) {
				// lista uzytkownik闚 z departametu
				if(!division.isGroup())
				{
				DSUser[] listaUzytk= division.getUsers(); // pobranie wszystkich uzytkownik闚 z dzia逝 

				
				// szukam uzytkownika kt鏎y posiada role sekretariat
				for (DSUser userOnList : listaUzytk) {
					// pobieram liste idkow rol danego u篡tkownika
					List<Long> listaIdRulUzytkownika= Role.listUserRoles(userOnList.getName());
					try { // sprawdzam czy posiada rol�
						long id= 8;
						if (listaIdRulUzytkownika.contains(id)) {
							retStr= userOnList.getName();
							Log.info("Dekretuj� na u篡tkownika " + retStr);
							zawiera= true;
							break;
						} else {
							// wyszukuje roli Sekretariatu i sprawdzam
							Role rola= Role.findByName("Sekretar%");
							if (listaIdRulUzytkownika.contains(rola.getId())) {
								retStr= userOnList.getName();
								zawiera= true;
								Log.info("Dekretuj� na u篡tkownika " + retStr);
								break;
							}
						}
					} catch (Exception e) {
						Log.error("B章d przy wyszukiwaniu roli Sekretariatu dla u篡tkownika " + userOnList.getName(), e);
						e.printStackTrace();
					}
				}
			}
			}
			if (!zawiera) {
				// dekretowanie
				Log.info("Nie znaleziono U篡tkownika pe軟i鉍ego role Sekretariatu Dyrektora ");
				/*if(retStr.equals("wlodarski") || retStr.equals("jurkowski")){
					retStr = "gajda";
				}*/
			}
			return retStr;
		}
		
	public static String getGuidDzialUzytkownika(String username)
			throws UserNotFoundException, EdmException {
		String dzial = "";
		DSUser user = DSUser.findByUsername(username);
		DSDivision[] division = user.getOriginalDivisions();
		for (DSDivision d : division) {
			if (d.isDivision()) {
				dzial = d.getGuid();
				return dzial;
			}
			else {
				 if(d.isPosition()){
					 dzial= d.getGuid();
				 }
			}
		}
		return dzial;
	}	
	
	
	
	public static  long  utworzOdbiorce(Person person,
			Map<String, FieldData> values) throws EdmException {
		int  resultsID = 0 ;
		String GETMAXID ="select max (ID) FROM DS_PAA_ODBIORCA";
		try {
			PreparedStatement ps = null;
			
			ps = DSApi.context().prepareStatement(
							"INSERT INTO DS_PAA_ODBIORCA "
									+ "(DISCRIMINATOR,TITLE,FIRSTNAME,LASTNAME ,ORGANIZATION,ORGANIZATIONDIVISION,STREET "
									+ ",ZIP,LOCATION,PESEL,NIP ,REGON,COUNTRY,EMAIL ,FAX ,identifikatorEpuap,adresSkrytkiEpuap,"
									+ "basePersonId,PHONENUMBER ,KOMNUMBER ,RODZAJ_PRZESYLKI ,KOSZT_PRZESYLKI,SPOSOB_DOSTARCZENIA ,KWOTA,ANONYMOUS )"
									+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

			{
				// disrciminator
				ps.setString(1, "PERSON");
				// title
				ps.setString(2, person.getTitle());
				// firstname
				ps.setString(3, person.getFirstname());

				ps.setString(4, person.getLastname());
				ps.setString(5, person.getOrganization());
				ps.setString(6, person.getOrganizationDivision());
				ps.setString(7, person.getStreet());
				ps.setString(8, person.getZip());
				ps.setString(9, person.getLocation());
				ps.setString(10, person.getPesel());
				ps.setString(11, person.getNip());
				ps.setString(12, person.getRegon());
				ps.setString(13, person.getCountry());
				ps.setString(14, person.getEmail());
				ps.setString(15, person.getFax());
				ps.setString(16, person.getIdentifikatorEpuap());
				ps.setString(17, person.getAdresSkrytkiEpuap());
				ps.setLong(18, person.getId());
				ps.setString(19, null);
				ps.setString(20, null);
				if (values.get("RECIPIENT_RODZAJ_PRZESYLKI")!=null && values.get("RECIPIENT_RODZAJ_PRZESYLKI").getEnumValuesData().getSelectedId()!=null)
					ps.setString(21, values.get("RECIPIENT_RODZAJ_PRZESYLKI").getEnumValuesData().getSelectedId());
				else
					ps.setString(21, null);
			
				ps.setString(22, null);
				if (values.get("RECIPIENT_RODZAJ_PRZESYLKI")!=null && values.get("RECIPIENT_SPOSOB_DOSTARCZENIA").getEnumValuesData().getSelectedId()!=null)
					ps.setString(23, values.get("RECIPIENT_SPOSOB_DOSTARCZENIA").getEnumValuesData().getSelectedId());
				else 
					ps.setString(23, null);
				
				ps.setString(24, null);
				ps.setInt(25, 0);
				ps.execute();
				ps = DSApi.context().prepareStatement(GETMAXID);
				
				ResultSet rs = ps.executeQuery();
				if (rs.next()){
				    // Retrieve the auto generated key(s).
				     resultsID = rs.getInt(1);
				}
				
				
				ps.close();
				return Long.parseLong(Integer.toString(resultsID));
				//DSApi.context().commit();
				// if (rs.next()){
				// kwota = rs.getDouble(1);

				// }
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error(" ", e);
			throw new EdmException(
					"Nieznaleziono kwoty dla podanych parametr闚 typu przesy趾i");
		}
		
	}
	
	
	/** Nadanie numeru KO i przypinanie dokumentu do dziennika dokument闚 wewn皻rznych, wychodz鉍ych lub przychodz鉍ych.
	* @param document - dokument, kt鏎ego operacja dotyczy
	* @param event - mo瞠 by� null
	* @param typDziennika - sta豉 tekstowa z klasy Journal (INTERNAL,INCOMING,OUTGOING) 
	 * @param journalId */
   public static void bindToJournal(OfficeDocument document, Journal journal,ActionEvent event) throws EdmException {
	   
	  
	   Date entryDate = new Date();
	   String typDziennika = journal.getJournalType();
	   DSApi.context().session().flush();
	   
		 /*if( journal.getId() >3 ||typDziennika ){
			 // nie wpisuje do dziennika 
				//Journal.TX_newEntry2(journal.getId(), document.getId(),entryDate);
	        	//if (document instanceof OfficeDocument) 
	        	//    ((OfficeDocument) document).setJournal(journal);
		 } else {*/
	   
			 if (typDziennika==null)
		   journal = Journal.getMainInternal();
	   
	   else if (typDziennika.equals(Journal.INTERNAL) )
		   journal = Journal.getMainInternal();
	   
	   else if (typDziennika.equals(Journal.INCOMING) )
		   journal = Journal.getMainIncoming();
	   
	   else if (typDziennika.equals(Journal.OUTGOING) )
		   journal = Journal.getMainOutgoing();
	   
			
	 
	
	   
	   Integer sequenceId = Journal.TX_newEntry2(journal.getId(), document.getId(), entryDate);
			   
	   if (typDziennika.equals(Journal.INCOMING) )
	   {
		   InOfficeDocument doc = (InOfficeDocument) document;
		   doc.bindToJournal(journal.getId(), sequenceId);
	   }
	   else
	   {
		   OutOfficeDocument doc = (OutOfficeDocument) document;
		   doc.bindToJournal(journal.getId(), sequenceId, null, event);
		   doc.setOfficeDate(entryDate);
	   }
	}
   
	
   public static boolean isDocumentFromSubstitutedUser(OpenExecution openExecution) throws UserNotFoundException, EdmException
	{
		DSUser users[] = DSApi.context().getDSUser().getSubstitutedUsers();
		for (DSUser  u : users){
			if(u.getName().equals((String)openExecution.getVariable("currentAssignee")))
				return true;
				}
		return false;
	}
	
	protected static OutOfficeDocument createDokument(OfficeDocument starydoc, Map<String, Object> OldValues, Map<String, FieldData> mDictValues)
			throws EdmException
	{
		
		OutOfficeDocument document = new OutOfficeDocument();
		document = addWartosci(document, starydoc);
		try
		{
			FieldsManager fm = document.getFieldsManager();
			fm.initialize();
			Map<String, Object> newDocValues = new HashMap<String, Object>();
			
			//wrzucienie wszystkich warto�ci z dokinda starego dokumentu do nowego 
			newDocValues.putAll(OldValues);
			// kopia mapy aby nie usun寞 warto�ci dla kolejnych dokument闚 
			Map<String, Object> kopiowanaMapa = ((Map<String, Object>) newDocValues.get(_multiOdbiorca));
			Map<String, Object> noweWartosci = new HashMap<String, Object>();
			for (String _key : kopiowanaMapa.keySet())
				noweWartosci.put(_key, kopiowanaMapa.get(_key));
			// iteracja po elelemtach wartosci multi dictionay aby usun鉍 pozosta造ch odbiorc闚
			Iterator<String> it = noweWartosci.keySet().iterator();
			while (it.hasNext())
			{
				String key = it.next();
				Map<String, Object> newWpisMulti = (Map<String, Object>) noweWartosci.get(key);
				// usuwanie starych warto�ci multi recipienta  
				if (!newWpisMulti.toString().contains(mDictValues.toString()))
					it.remove(); 
				else 
				newDocValues.put("DOC_DELIVERY", String.valueOf(newWpisMulti.get("SPOSOB_DOSTARCZENIA")));
			}
			// pobranie aktualnego klucza recipienta kt鏎y ma byc dodany do noweg odokumentu 
			//zmiana jego warto�ci na recipient_1
			String _key = noweWartosci.keySet().iterator().next();
			Object tmp = noweWartosci.get(_key);
			noweWartosci.remove(_key);
			// odcinam RECIPIENT_ i dodaje na koncu 1
			_key = _key.substring(0, (_key.lastIndexOf('_'))) + "_1";
			noweWartosci.put(_key, tmp);
			// dodanie recipienta do mapy 
			newDocValues.put(_multiOdbiorca, noweWartosci);
			fm.reloadValues(newDocValues);

			document.create();

			Long newDocumentId = document.getId();
		
			document.getDocumentKind().saveDictionaryValues(document, newDocValues);
			document.getDocumentKind().saveMultiDictionaryValues(document, newDocValues);
			document.getDocumentKind().logic().removeValuesBeforeSet(newDocValues);
			document.getDocumentKind().set(newDocumentId, newDocValues);
			document.getDocumentKind().logic().archiveActions(document, 0);
			document.getDocumentKind().logic().documentPermissions(document);
			DSApi.context().session().saveOrUpdate(document);
			/*DSApi.context().commit();
			DSApi.context().begin();*/
			
		
		} catch (EdmException e)
		{
			log.error("", e);
		}

		return document;
	}
	
	
	private static OutOfficeDocument addWartosci(OutOfficeDocument document, OfficeDocument starydoc) throws EdmException
	{
		document.setPermissionsOn(false);
		
		/* Atrybuty office */
		document.setSummary(starydoc.getSummary());
		document.setCurrentAssignmentGuid(starydoc.getCurrentAssignmentGuid());
		document.setDivisionGuid(starydoc.getDivisionGuid());
		document.setCurrentAssignmentAccepted(Boolean.FALSE);
		document.setCreatingUser(DSApi.context().getPrincipalName());
		document.setCurrentAssignmentUsername(DSApi.context().getPrincipalName());
		if(((OutOfficeDocument)starydoc).getInDocumentId()!=null)
		document.setInDocumentId(((OutOfficeDocument)starydoc).getInDocumentId());
		//for ( Attachment at : starydoc.getAttachments()){
		//	document.addAttachment(at);
	//	}
		//document.setAssignmentHistory(starydoc.getAssignmentHistory());
		document.setDescription(starydoc.getDescription());
		document.setDocumentKind(starydoc.getDocumentKind());
		document.setFolderId(starydoc.getFolderId());
		document.setSender(starydoc.getSender());
		document.setForceArchivePermissions(false);
		document.setAssignedDivision(DSDivision.ROOT_GUID);
		document.setClerk(DSApi.context().getPrincipalName());
		return document;

	}
	
/*	public static  void paaAssignmentPush(OfficeDocument od, String activityId, Map<String, Object> params, String[] targetUsers) throws UserNotFoundException, EdmException
	{
		// PAA  weryfikacja nadawcy i odbiorcy po przynaleznosci do departament闚
		// jezeli r騜ne to zwraca dyrektora nadawcy departamentu na kt鏎ego jest dekretacja 
	
	String toUsername =  (String) params.get(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM);
	String fromUsername  = DSApi.context().getPrincipalName();
	String dyrektor ="";
	String TEmpactivityId= new String();
	TEmpactivityId = activityId;
	boolean  canAssignToRecipient = false;
	
	boolean  hasSubstitutedUsers = DSApi.context().getDSUser().getSubstitutedUsersForNow(new Date()).length>0 ;
	log.info("--- NormalLogic.paaAssignmentPush :  dekretacja reczna dokument闚 :: dokument ID = "+od.getId()+" : pomiedzy nadawc� --> "+fromUsername +" a odbiorc� -> "+toUsername );
	
	
	if(hasSubstitutedUsers){
		log.info("--- NormalLogic.paaAssignmentPush : Zastepuje osobe/y sprawdzam uprawnienia do dekretacji");		
		canAssignToRecipient = checkAssigners(TEmpactivityId,fromUsername,toUsername);
	}
	
	if (canAssignToRecipient){
		log.info("--- NormalLogic.paaAssignmentPush : Moze dekretowac do odbiorcy na departament osoby zastepujacej");		
		dekretujDoOdbiorcy(od ,activityId, params);
	}
	else if (inDepIsOutDep(fromUsername, toUsername) || toUsername.equals(Docusafe.getAdditionProperty("PAA.GlownySekretariat"))){
		log.info("--- NormalLogic.paaAssignmentPush : ten sam departament");		
		dekretujDoOdbiorcy(od ,activityId, params);
	}
	else{
		log.info("--- NormalLogic.paaAssignmentPush : inny departament");
		dyrektor = od.getDocumentKind().logic().onStartManualAssignment(od, fromUsername, toUsername);
		
		if (!toUsername.contains(dyrektor)){
			dekretujDoDyrektoraOrazSekretariatu(od ,activityId ,dyrektor , params,targetUsers);
		}
		else
			dekretujDoOdbiorcy(od ,activityId, params);
	  }
		
		
	
		// PAA  weryfikacja nadawcy i odbiorcy po przynaleznosci do departament闚
		// jezeli r騜ne to zwraca dyrektora nadawcy departamentu na kt鏎ego jest dekretacja 
		String toUsername = (String) params.get(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM);
		String fromUsername = DSApi.context().getPrincipalName();
		String newToUsername = od.getDocumentKind().logic().onStartManualAssignment(od, fromUsername, toUsername);
		//je�li odbiorca to sobiecka to wszystkie pisma id� tylko do niej - bez wzgl璠u na departamenty 
		if (toUsername.equals("sobiecka"))
		{
			Jbpm4WorkflowFactory.reassign(activityId, od.getId(), DSApi.context().getPrincipalName(),
					(String) params.get(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM), params);
		}
		//jeslei odbiorta to nie sobiecka  i inny departament
		else if (!toUsername.equals(newToUsername))
		{
			String sekretariat = NormalLogic.getUserPelniocyRoleSekretariatu(newToUsername);

			targetUsers[0] = newToUsername;
			targetUsers[1] = sekretariat;
			NormalLogic.getGuidDzialUzytkownika(DSApi.context().getDSUser().getName());
			//targetGuids[0] = "";
			//	targetGuids[1] = "";

			Jbpm4WorkflowFactory.reassign(activityId, od.getId(), DSApi.context().getPrincipalName(), targetUsers, targetGuids);

		} else
		{
			Jbpm4WorkflowFactory.reassign(activityId, od.getId(), DSApi.context().getPrincipalName(),
					(String) params.get(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM), params);
		}*/
//	}

	
	
	/*public static void paaAssignmentPush(OfficeDocument od, String activityId, Map<String, Object> params) throws UserNotFoundException, EdmException
	{
		// PAA  weryfikacja nadawcy i odbiorcy po przynaleznosci do departament闚
		// jezeli r騜ne to zwraca dyrektora nadawcy departamentu na kt鏎ego jest dekretacja 
	
	String toUsername =  (String) params.get(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM);
	String fromUsername  = DSApi.context().getPrincipalName();
	String dyrektor ="";
	String TEmpactivityId= new String();
	TEmpactivityId = activityId;
	boolean  canAssignToRecipient = false;
	
	boolean  hasSubstitutedUsers = DSApi.context().getDSUser().getSubstitutedUsersForNow(new Date()).length>0 ;
	log.info("--- NormalLogic.paaAssignmentPush :  dekretacja reczna dokument闚 :: dokument ID = "+od.getId()+" : pomiedzy nadawc� --> "+fromUsername +" a odbiorc� -> "+toUsername );
	
	DSApi.context().session().beginTransaction();
	if(hasSubstitutedUsers){
		log.info("--- NormalLogic.paaAssignmentPush : Zastepuje osobe/y sprawdzam uprawnienia do dekretacji");		
		canAssignToRecipient = checkAssigners(TEmpactivityId,fromUsername,toUsername);
	}
	
	if (canAssignToRecipient){
		log.info("--- NormalLogic.paaAssignmentPush : Moze dekretowac do odbiorcy na departament osoby zastepujacej");		
		dekretujDoOdbiorcy(od ,activityId, params);
	}
	else if (inDepIsOutDep(fromUsername, toUsername) || toUsername.equals(Docusafe.getAdditionProperty("PAA.GlownySekretariat"))){
		log.info("--- NormalLogic.paaAssignmentPush : ten sam departament");		
		dekretujDoOdbiorcy(od ,activityId, params);
	}
	else{
		log.info("--- NormalLogic.paaAssignmentPush : inny departament");
		dyrektor = od.getDocumentKind().logic().onStartManualAssignment(od, fromUsername, toUsername);
		dekretujDoDyrektoraOrazSekretariatu(od ,activityId ,dyrektor , params);
	  }*/
		
	
		
	

	/*if (toUsername.equals(Docusafe.getAdditionProperty("PAA.GlownySekretariat"))){
		Jbpm4WorkflowFactory.reassign(
                activityId,
                od.getId(),
                DSApi.context().getPrincipalName(),
                (String) params.get(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM),
                params);
	}
	//jeslei odbiorta to nie sobiecka  i inny departament
	else if (!toUsername.equals(newToUsername)) {
			String sekretariat = NormalLogic.getUserPelniocyRoleSekretariatu(newToUsername);
			
			targetUsers[0] = newToUsername;
			targetUsers[1] = sekretariat;
			

			
		 Jbpm4WorkflowFactory.reassign(activityId, od.getId(), DSApi.context().getPrincipalName(), targetUsers, targetGuids);
			
		}else {
			Jbpm4WorkflowFactory.reassign(
                    activityId,
                    od.getId(),
                    DSApi.context().getPrincipalName(),
                    (String) params.get(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM),
                    params);
		}
		*/
	//}
	private static void dekretujDoGlowSekretariatu(String activityId, OfficeDocument od, Map<String, Object> params)
	{
		// TODO Auto-generated method stub
		
	}

	public static  void dekretujDoDyrektoraOrazSekretariatu(OfficeDocument od, String activityId, String dyrektor, Map<String, Object> params, String[] targetUsers) throws UserNotFoundException, EdmException
	{
		String sekretariat = NormalLogic.getUserPelniocyRoleSekretariatu(dyrektor);
		targetUsers = new String[2];
		targetUsers[0] = dyrektor;
		targetUsers[1] = "gajda";
		
	 Jbpm4WorkflowFactory.reassign(activityId, od.getId(), DSApi.context().getPrincipalName(), targetUsers, targetGuids,null);
		
		
	}

	public static void dekretujDoOdbiorcy(OfficeDocument od, String activityId, Map<String, Object> params) throws EdmException
	{
		
		Jbpm4WorkflowFactory.reassign(
                activityId,
                od.getId(),
                DSApi.context().getPrincipalName(),
                (String) params.get(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM),
                params);
		
	}

	/**
	 * Metoda sprawdza czy odbiorca znajduje sie w departamencie osoby zastepowanej
	 * je瞠li tak to pismo jest dekretowanie do odbiorcy w przeciwnym wyapdku do dyrektora departamentu oraz pracownika sekretariatu o ile jest on okre�lony 
	 * @param activityId
	 * @param fromUsername
	 * @param toUsername
	 * @return
	 * @throws EdmException
	 */
	 public static boolean checkAssigners(String activityId, String fromUsername, String toUsername) throws EdmException
	{
		 activityId = cleanActivityId(activityId);
		 String asiggne = TaskSnapshot.getInstance().findByActivityKey(activityId).getDsw_resource_res_key();
		
			if (!asiggne.equals(fromUsername))
			return true;
			if (DSApi.context().getDSUser().getSubstitutedUsers().length>0)
			{
			DSUser users[] = DSApi.context().getDSUser().getSubstitutedUsers();
				for (DSUser  u : users){
					if (inDepIsOutDep(u.getName(), toUsername)){
						return true;
						}
					}
			}
			return false;
			
		
	}

	private static String cleanActivityId(String activityId){
	        return activityId.replaceAll("[^,]*,","");
	    }
}

	