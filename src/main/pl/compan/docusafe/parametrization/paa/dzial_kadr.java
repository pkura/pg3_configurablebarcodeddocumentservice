package pl.compan.docusafe.parametrization.paa;

import java.util.Calendar;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** asygnuje na dzia� kadr*/
@SuppressWarnings("serial")
public class dzial_kadr implements AssignmentHandler {
	private final static Logger log= LoggerFactory.getLogger(dzial_kadr.class);
	@Override
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
		
		log.info("dzial_kadr");
		
		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		
		OfficeDocument doc = OfficeDocument.find(docId);
		String data = Calendar.getInstance().getTime().toString();
		String rodzajDokumentu = doc.getDocumentKind().getCn();
		log.info("Dokument  id = "+docId+"Rodzaj = " +rodzajDokumentu+ " czas zajscia procesu "+data);
		
		//dzia� kadr
		String asygnowanyDzial ="CBC52154-8D3F-41EC-9B6C-67A290682D6D";
		
		assignable.addCandidateGroup(asygnowanyDzial);
		
		AssignmentHistoryEntry entry = new AssignmentHistoryEntry();
		entry.setSourceUser(DSApi.context().getDSUser().getName());
		entry.setTargetGuid(asygnowanyDzial);
		String dzialNad =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(DSApi.context().getDSUser().getName());
		entry.setSourceGuid(dzialNad);
		String statusDokumentu =  doc.getFieldsManager().getEnumItem("STATUSDOKUMENTU").getTitle();
		entry.setObjective(statusDokumentu);
		entry.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
		doc.addAssignmentHistoryEntry(entry);
		//dekretowanie
		//assignable.addCandidateUser(asygnowanyUzytkownik);
		//String asygnowanyUzytkownik = ".....okreslic......";
		// dodac recznie do historii (w WSSK robilem to tak:) 
		// Util_AHE.addToHistory(openExecution, doc, asygnowanyUzytkownik, null, null, log);
	}
	
}