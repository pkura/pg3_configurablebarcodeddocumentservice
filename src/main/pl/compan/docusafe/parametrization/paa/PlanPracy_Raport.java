package pl.compan.docusafe.parametrization.paa;

import java.io.File;
import pl.compan.docusafe.core.dockinds.field.Field;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.DocumentField;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;

import com.google.common.collect.Maps;
import com.lowagie.text.Cell;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.webwork.ServletActionContext;

public class PlanPracy_Raport
{
	static final Logger log = LoggerFactory.getLogger(PlanPracy_Raport.class);
	
	static public class PlanPracyRaportAction implements ActionListener
	{
		Long documentId=0L;

		public void actionPerformed(ActionEvent event)
		{
			try {
				ServletUtils.streamFile(ServletActionContext.getResponse(), generateRaport(), "application/vnd.ms-excel",
						"Content-Disposition: attachment; filename=\"Raport planu pracy.xls\"");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private File generateRaport() throws EdmException, IOException
		{
			documentId= 118L;
			///
			
			
			String reportName= "Raport planu pracy";
			String columns[] = {"Typ", "Opis", "Kom�rka wiod�ca", "Data od", "Data do", "Stan", ""};
			int columnsLen[] = {13*256, 45*256, 35*256, 12*256, 12*256, 15*256, 14*256};
			
			List<PlanPracyZadanieBean> data = prepareData(documentId);
			
			// XLS
			UniversalTableDumper dumper= new UniversalTableDumper();
			XlsPoiDumper poiDumper= new XlsPoiDumper();
			File xlsFile= File.createTempFile("docusafe_", "_tmp");
			xlsFile.deleteOnExit();
			poiDumper.openFile(xlsFile);
			FileOutputStream fis= new FileOutputStream(xlsFile);

			// arkusz
			HSSFWorkbook workbook= new HSSFWorkbook();
			HSSFSheet sheet= workbook.createSheet(reportName);
			
			// headers
			HSSFRow headerRow = sheet.createRow(0);
			for (int i = 0; i < columns.length; i++)
			{
				headerRow.createCell(i)
					.setCellValue(new HSSFRichTextString(columns[i]));
				//headerRow.getCell(i).setCellStyle(headerStyle);
			}
			
			// rows
			int rowCount = 1;
			for(PlanPracyZadanieBean bean : data){
				HSSFRow row = sheet.createRow(rowCount++);
				int cellCout = 0;
				row.createCell(cellCout++).setCellValue(new HSSFRichTextString(bean.getTypString()));
				row.createCell(cellCout++).setCellValue(new HSSFRichTextString(bean.opis));
				row.createCell(cellCout++).setCellValue(new HSSFRichTextString(bean.komorkaWiodaca));
				row.createCell(cellCout++).setCellValue(new HSSFRichTextString(bean.dataOd));
				row.createCell(cellCout++).setCellValue(new HSSFRichTextString(bean.dataDo));
				row.createCell(cellCout++).setCellValue(new HSSFRichTextString(bean.stan));
				row.createCell(cellCout++).setCellValue(new HSSFRichTextString(bean.getTypObiektuString()));
			}
			
			for (int i = 0; i < columns.length; i++)
				sheet.setColumnWidth(i, columnsLen[i]);
			
			// end
			workbook.write(fis);
			fis.close();
			dumper.addDumper(poiDumper);

			return xlsFile;
		}

		static final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		
		class PlanPracyZadanieBean{
			public Integer typObiektu;
			public Integer typZadania;
			public String opis;
			public String dataOd;
			public String dataDo;
			public String komorkaWiodaca;
			public String stan;
			
			public PlanPracyZadanieBean(ResultSet result) throws SQLException{
				typObiektu = result.getInt(1);
				typZadania = result.getInt(2);
				opis = result.getString(3);
				dataOd = dateFormat.format(result.getDate(4));
				dataDo = dateFormat.format(result.getDate(5));
				stan = result.getString(6);
				komorkaWiodaca = result.getString(7);
			}

			public String getTypObiektuString()
			{
				switch(typObiektu){
				case 701: return "Zadanie g��wne";
				// 702 komorki
				default: return "";
				}
			}

			public String getTypString()
			{
				switch(typZadania){
				case 801: return "legislacyjne";
				case 802: return "priorytetowe";
				case 803: return "cykliczne";
				case 804: return "wydarzenia";
				case 805: return "inne";
				default: return "brak";
				}
			}
		}
		
		
		private List<PlanPracyZadanieBean> prepareData(Long documentId2)
		{
			Statement stat = null;
			boolean contextOpened= false;
			try
			{
				contextOpened= DSApi.openContextIfNeeded();
				stat = DSApi.context().createStatement();
				
				List<PlanPracyZadanieBean> zadania= new ArrayList();
				
				ResultSet result = stat.executeQuery(
						"select Z.TYP_OBIEKTU, Z.TYP_ZADANIA, Z.OPIS, Z.DATAOD, Z.DATADO, Z.STAN, DIV.NAME from DS_PAA_PLAN_PRACY_ZAD Z"+
						" join DS_DIVISION DIV on DIV.id=Z.KOMORKA_WIODACA" +
						" join DS_PAA_ZADANIA_MULTIPLE M on Z.DOCUMENT_ID=M.FIELD_VAL"+
						" WHERE M.DOCUMENT_ID="+documentId);
						
				while (result.next()){
					zadania.add(new PlanPracyZadanieBean(result));
				}
				
				return zadania;
			}
			catch (Exception ie){
				log.error(ie.getMessage(), ie);
				return null;
			}
			finally{
				DSApi.context().closeStatement(stat);
				DSApi.closeContextIfNeeded(contextOpened);
			}
		}
	}
	
}
