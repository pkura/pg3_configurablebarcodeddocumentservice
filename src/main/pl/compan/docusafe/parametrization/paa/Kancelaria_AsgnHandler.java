package pl.compan.docusafe.parametrization.paa;

import java.util.Calendar;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** asygnuje na og�ln� kancelari� */
@SuppressWarnings("serial")
public class Kancelaria_AsgnHandler implements AssignmentHandler {
	private static final Logger log = LoggerFactory.getLogger(Kancelaria_AsgnHandler.class);
	@Override
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
		log.info("Kancelaria_AsgnHandler");
		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		String data = Calendar.getInstance().getTime().toString();
		String rodzajDokumentu = doc.getDocumentKind().getCn();
		log.info("Dokument  id = "+docId+"Rodzaj = " +rodzajDokumentu+ " czas zajscia procesu "+data);
		
		
	//	String asd = doc.getFieldsManager().getEnumItem("STATUSDOKUMENTU").getRefValue()
	
		String asygnowanyDzial ="BFA84270006CA0FC13C494D6196C496AC05";
		
		assignable.addCandidateGroup(asygnowanyDzial);
	
		AssignmentHistoryEntry entry = new AssignmentHistoryEntry();
		entry.setSourceUser(DSApi.context().getDSUser().getName());
		String dzialNad =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(DSApi.context().getDSUser().getName());
		entry.setSourceGuid(dzialNad);
		entry.setTargetGuid(asygnowanyDzial);
		String statusDokumentu =  doc.getFieldsManager().getEnumItem("STATUSDOKUMENTU").getTitle();
		entry.setObjective(statusDokumentu);
		entry.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
		doc.addAssignmentHistoryEntry(entry);
		
		//dekretowanie
		//assignable.addCandidateUser(asygnowanyUzytkownik);
		//String asygnowanyUzytkownik = ".....okreslic......";
		// dodac recznie do historii (w WSSK robilem to tak:) 
		// Util_AHE.addToHistory(openExecution, doc, asygnowanyUzytkownik, null, null, log);
	}
}