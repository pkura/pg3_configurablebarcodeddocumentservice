package pl.compan.docusafe.parametrization.paa;

import java.util.Calendar;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
public class RodzajWniosku_DecisionHandler implements DecisionHandler
{
	private static final Logger log = LoggerFactory.getLogger(RodzajWniosku_DecisionHandler.class);

	public String decide(OpenExecution openExecution)
	{
		log.info("RodzajWniosku_DecisionHandler");

		String decision = "";
		
		try {
			Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			DSUser user =DSUser.findByUsername(doc.getAuthor());
			
			String data = Calendar.getInstance().getTime().toString();
			String rodzajDokumentu = doc.getDocumentKind().getCn();
			log.info("Dokument  id = "+docId+"Rodzaj = " +rodzajDokumentu+ " czas zajscia procesu "+data);
			

			if (user.getName().equals("mikulski") ||user.getName().equals("merta"))
			{
				return decision ="dyrektor";
			}
			int id = fm.getEnumItem("KIND").getId();
			
			if (id == 20 ){ // urlop na ��danie
				 decision = "naZadanie";
				ustawZastzaNiewymagane(doc, fm);
			}
			else if (id == 25 )
			{//urlopbez sast�pstwa 
				decision = "bezZastepstwa";
				ustawZastzaNiewymagane(doc, fm);
			}
			else
				decision="pozostale";
			
		}catch (Exception e) {
			log.info("RodzajWniosku_DecisionHandler - b�ad w podj�ciu decyzji ");
		}
		
		log.info("RodzajWniosku_DecisionHandler - zwracam decyzje -> "+decision);
			
			return decision;

		}

	private void ustawZastzaNiewymagane(OfficeDocument doc, FieldsManager fm)
	{
		try
		{
			fm.getField("ZASTEPSTWO").setHidden(true);
			fm.getField("ZASTEPSTWO").setRequired(false);
			fm.getField("GODZ_FROM").setHidden(true);
			fm.getField("GODZ_FROM").setRequired(false);
			fm.getField("GODZ_TO").setHidden(true);
			fm.getField("GODZ_TO").setRequired(false);
			
		} catch (Exception e) {
		log.error("", e);
		
		}
	}
	
}