package pl.compan.docusafe.parametrization.paa;

import java.util.Calendar;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** Asygnuje dokument na dyrektora departamentu, w ktorym pracuje osoba wskazana jako odbiorca pisma */
public class DyrektorDepartamentuOdbiorcy_AsgnHandler implements AssignmentHandler {
	
	private static final Logger log = LoggerFactory.getLogger(DyrektorDepartamentuOdbiorcy_AsgnHandler.class);
	private String poleOdbiorcy; //to jest pole typu private ale mimo to pojawi si� tu warto�� z zewn�trz.
	// JBPM wstawi tu warto�� podan� w XML:
	// <field name="poleNadawcy"><string value="nazwa pola Nadawca"/></field>
	// czyli wpisze warto�� "nazwa pola Nadawca" do pola "poleNadawcy" powy�ej.
	// Nale�y to traktowa� jako dodatkowy argument wywo�ania asygnatora (tej metody w tej klasie) 

	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
		
		log.info("DyrektorDepartamentuOdbiorcy_AsgnHandler");
		String odbiorca = (String) openExecution.getVariable("odbiorca");
		
		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		
	
		String data = Calendar.getInstance().getTime().toString();
		String rodzajDokumentu = doc.getDocumentKind().getCn();
		
		String dyrektor= pl.compan.docusafe.parametrization.paa.NormalLogic.getDyrektorDepartamentu(odbiorca, doc);
		if(odbiorca.equals("sobiecka")){
			assignable.addCandidateUser(odbiorca);
			dyrektor = odbiorca;
		
		}
		/*else if  (dyrektor.equals("WBDG")){
			 log.info("Znalaz�em dyrektora "+dyrektor+ "pobieram pracownika sekretariatu  bo jes w BDG");
			 dyrektor = pl.compan.docusafe.parametrization.paa.NormalLogic.getDyrektorGeneralny();
		}*/
			
		
		log.info("DyrektorDepartamentuOdbiorcy_AsgnHandler -> Dokument  id = "+docId+ " Rodzaj = " +rodzajDokumentu+ " czas zajscia procesu "+data);
		//DSUser user = ...
		log.info("DyrektorDepartamentuOdbiorcy_AsgnHandler = dekretuje na -> "+dyrektor);
		assignable.addCandidateUser(dyrektor);
		
		
		AssignmentHistoryEntry entry = new AssignmentHistoryEntry();
		entry.setSourceUser(DSApi.context().getDSUser().getName());
		entry.setTargetUser(dyrektor);
		String dzialNad =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(DSApi.context().getDSUser().getName());
		entry.setSourceGuid(dzialNad);
		String dzialOdb =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(dyrektor);
		entry.setTargetGuid(dzialOdb);
		String statusDokumentu =  doc.getFieldsManager().getEnumItem("STATUSDOKUMENTU").getTitle();
		entry.setObjective(statusDokumentu);
		entry.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
		doc.addAssignmentHistoryEntry(entry);
		
		// dodac recznie do historii (w WSSK robilem to tak:) 
		// Util_AHE.addToHistory(openExecution, doc, asygnowanyUzytkownik, null, null, log);
	}

}
