package pl.compan.docusafe.parametrization.paa;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** Asygnuje dokument na osobe wskazana jako odbiorca */
public class Odbiorca_AsgnHandler  implements AssignmentHandler{
	
	private static final long serialVersionUID= 1736530136286509050L;
	
	
	private String poleOdbiorcy; //to jest pole typu private ale mimo to pojawi si� tu warto�� z zewn�trz.
	// JBPM wstawi tu warto�� podan� w XML:
	// <field name="poleNadawcy"><string value="nazwa pola Nadawca"/></field>
	// czyli wpisze warto�� "nazwa pola Nadawca" do pola "poleNadawcy" powy�ej.
	// Nale�y to traktowa� jako dodatkowy argument wywo�ania asygnatora (tej metody w tej klasie) 
	
	private static final Logger log = LoggerFactory.getLogger(Odbiorca_AsgnHandler.class);
	
	
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
		
		log.info("Odbiorca_AsgnHandler");
		
		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
	
		
		
	
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		
		String data = Calendar.getInstance().getTime().toString();
		String rodzajDokumentu = doc.getDocumentKind().getCn();
		log.info("Dokument  id = "+docId+"Rodzaj = " +rodzajDokumentu+ " czas zajscia procesu "+data);
		
		String odbiorcaIdStr=fm.getStringKey(poleOdbiorcy);
		log.info("pobra��m odbiorca id = " +odbiorcaIdStr+" - "+poleOdbiorcy);
		
		
		
		String[] ids;
		if (odbiorcaIdStr.startsWith("[")){
			odbiorcaIdStr= odbiorcaIdStr.substring(1, odbiorcaIdStr.length()-1);
			ids  = odbiorcaIdStr.split(",");
		}else{
			ids = new String[1];
			ids[0] = odbiorcaIdStr;
		}
		//usernames.clear();
		//iloscOsobZastepujacych = 0;
		for (String id : ids) {
				id = id.trim();
			long odbiorcaId= 0;
			try {
				try {
					odbiorcaId= Long.parseLong(id); // pierwsza proba
				} catch (NumberFormatException nfe) {
					odbiorcaId= Long.parseLong(id.substring(1, id.length() - 1)); // druga proba
				}
			} catch (NumberFormatException nfe) {
				log.info("podane ID u�ytkownika (" + odbiorcaIdStr
						+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
				throw new IllegalStateException("podane ID u�ytkownika (" + odbiorcaIdStr
						+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
			}

			DSUser odbiorca= null;

			if (poleOdbiorcy.equalsIgnoreCase("RECIPIENT_HERE")) {
				// person
				Person person= Person.find(odbiorcaId);
				String personfirstname= person.getFirstname();
				String personLastname= person.getLastname();

				odbiorca= DSUser.findByFirstnameLastname(personfirstname, personLastname);
			} else if (poleOdbiorcy.equalsIgnoreCase("RECIPIENT")) {
				// user
				odbiorca= DSUser.findById(odbiorcaId);
			} else {
				throw new Exception("poleOdbiorcy ma byc RECIPIENT lub RECIPIENT_HERE");
			}
			String nadawca= DSApi.context().getDSUser().getName();
			
			if(NormalLogic.isDocumentFromSubstitutedUser(openExecution))
			nadawca = (String)openExecution.getVariable("currentAssignee");
			
			String dyrektor= "";
			String sekretariat= "";
			// sprawdzam czy odbiorca jest w departamencie nadawcy
			// nadawca to osoba wykonuj�ca dan� akcj�
			boolean tenSam= true;
			boolean pracSekret=false;
			
			// jezeli odbiorc� jest sobiecka topisma id� tylko do niej 
			if(odbiorca.getName().equals("sobiecka")){
				assignable.addCandidateUser(odbiorca.getName());
				dyrektor = odbiorca.getName();
			
			}
			else if (nadawca.equals(odbiorca.getName())) {
				log.info("Odbiorca_AsgnHandler  - Odbiorca w tym samym departamencie - dekretuje na odbiorc� : "
						+ odbiorca.getName());
				assignable.addCandidateUser(odbiorca.getName());
				sekretariat = odbiorca.getName();
				
			} else {
				tenSam= pl.compan.docusafe.parametrization.paa.NormalLogic.inDepIsOutDep(nadawca, odbiorca.getName());
				if (tenSam) {
					log.info("Odbiorca_AsgnHandler  - Odbiorca w tym samym departamencie - dekretuje na odbiorc� : "
							+ odbiorca.getName());
					assignable.addCandidateUser(odbiorca.getName());
					
					sekretariat = odbiorca.getName();
				} else {
					log.info("Odbiorca_AsgnHandler  Odbiorca w innym departamencie -  wyszukuje dyrektora tego departamentu   odbiorca -> "
							+ odbiorca.getName());

					dyrektor= pl.compan.docusafe.parametrization.paa.NormalLogic.getDyrektorDepartamentu(odbiorca
							.getName(), doc);
					if (dyrektor.equals(odbiorca.getName())) {
						assignable.addCandidateUser(dyrektor);
						log.info("Nie znalaz�em dyrektora departamentu odbiorcy : odbiorca " + odbiorca.getName()
								+ "to dyr");
						sekretariat = dyrektor;
						// throw new
						// Exception("Nie znalaz�em dyrektora departamentu odbiorcy , odbiorca : "+odbiorca.getName());
						// log.info("Nie znalaz�em dyrektora departamentu  wyszukuj� po dziale u�ytkownika " +
						// odbiorca.getName()+ "- pracownika sekretariatu ");
						// sekretariat=
						// pl.compan.docusafe.parametrization.paa.NormalLogic.getUserPelniocyRoleSekretariatu(odbiorca.getName());
						// if (sekretariat.equals(odbiorca.getName())) {
						// log.info("Nie znalaz�em  pracownika sekretariatu po dziale u�ytkownika " +
						// odbiorca.getName()+ " Dekretuje na u�ytkownika");
						// assignable.addCandidateUser(odbiorca.getName());
						// } else {
						// assignable.addCandidateUser(sekretariat);

						/*
						 * log.info("Nie znalaz�em  pracownika sekretariatu po dziale u�ytkownika " +
						 * odbiorca.getName()+ " Dekretuje na Dyrektora Generalnego"); String dyrGeneralny=
						 * pl.compan.docusafe.parametrization.paa.NormalLogic.getDyrektorGeneralny();
						 * assignable.addCandidateUser(dyrGeneralny);
						 */

						// znaleziony dyrektor departamentu odbiorcy
					} else {
						
						 log.info("w kroku - znaleziony dyrektor departamentu odbiorcy ");
					/*	if  (dyrektor.equals("WBDG")){
							 log.info("Znalaz�em dyrektora "+dyrektor+ "pobieram pracownika sekretariatu  bo jes w BDG");
							 dyrektor = pl.compan.docusafe.parametrization.paa.NormalLogic.getDyrektorGeneralny();
							 sekretariat = pl.compan.docusafe.parametrization.paa.NormalLogic.getUserPelniocyRoleSekretariatu(dyrektor);
							 log.info("Znalaz�em pracownika "+sekretariat+ "dekretuje na niego -> "+sekretariat +" oraz na dyrektora - >"+dyrektor);
							 assignable.addCandidateUser(dyrektor);
							 assignable.addCandidateUser(sekretariat);
						} else {*/
						sekretariat= pl.compan.docusafe.parametrization.paa.NormalLogic
								.getUserPelniocyRoleSekretariatu(dyrektor);
						if (sekretariat.equals(dyrektor)) {
							log.info("Znalaz�em dyrektora , Dyrektor ->" + dyrektor
									+ " Dekretuj� na dyrektora u�ytkownika " + odbiorca.getName());
							assignable.addCandidateUser(dyrektor);
						} else {
							log.info("Znalaz�em dyrektora i pracownika sekretariuatu sekretariat ->" + sekretariat
									+ "Dyrektor ->" + dyrektor + " Dekretuj� na dyrektora  i Sekretariat u�ytkownika "
									+ odbiorca.getName());
							assignable.addCandidateUser(dyrektor);
							assignable.addCandidateUser(sekretariat);
							
							
						}
					}
					
					
					AssignmentHistoryEntry entry = new AssignmentHistoryEntry();
					entry.setSourceUser(DSApi.context().getDSUser().getName());
					entry.setTargetUser(dyrektor);
					
					String dzialNad =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(DSApi.context().getDSUser().getName());
					entry.setSourceGuid(dzialNad);
					
					String dzialOdb =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(dyrektor);
					entry.setTargetGuid(dzialOdb);
					
					String statusDokumentu =  doc.getFieldsManager().getEnumItem("STATUSDOKUMENTU").getTitle();
					entry.setObjective(statusDokumentu);
					entry.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
					doc.addAssignmentHistoryEntry(entry);
					
					Date date = (Date) Calendar.getInstance().getTime();
					DateUtils data2 = new DateUtils();
			    	String dataa = data2.formatCommonDateTime(date);
					doc.addWorkHistoryEntry(Audit.create(DSApi.context().getPrincipalName(),DSApi.context().getPrincipalName(),
			                "Dokument zosta� zdekretowany na  : "+dyrektor+ " w dniu : " +dataa));
					/*
					 * log.info("Znalaz�em dyrektora " + dyrektor + " Pobieram Pracownika sekretariatu po dyrektorze");
					 * sekretariat= pl.compan.docusafe.parametrization.paa.NormalLogic
					 * .getUserPelniocyRoleSekretariatu(dyrektor);
					 * 
					 * if (sekretariat.equals(dyrektor)) {
					 * log.info("Nie znalaz�em  pracownika sekretariatu po dziale u�ytkownika " + odbiorca.getName() +
					 * " Dekretuje na u�ytkownika" + odbiorca.getName());
					 * assignable.addCandidateUser(odbiorca.getName()); } else { log.info(" dekretuj� na Sekretariat" +
					 * sekretariat + "oraz na u�ytkownika " + odbiorca.getName());
					 * assignable.addCandidateUser(sekretariat); assignable.addCandidateUser(odbiorca.getName()); } }
					 */

					// dekretowanie
					// assignable.addCandidateUser(odbiorca.getName());

					// dodac recznie do historii (w WSSK robilem to tak:)
					// Util_AHE.addToHistory(openExecution, doc, asygnowanyUzytkownik, null, null, log);
				}
			}
			
			AssignmentHistoryEntry entry = new AssignmentHistoryEntry();
			entry.setSourceUser(DSApi.context().getDSUser().getName());
			entry.setTargetUser(sekretariat);
			String dzialNad =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(DSApi.context().getDSUser().getName());
			entry.setSourceGuid(dzialNad);
			String dzialOdb =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(sekretariat);
			entry.setTargetGuid(dzialOdb);
			String statusDokumentu =  doc.getFieldsManager().getEnumItem("STATUSDOKUMENTU").getTitle();
			entry.setObjective(statusDokumentu);
			entry.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
			doc.addAssignmentHistoryEntry(entry);
			
			Date date = (Date) Calendar.getInstance().getTime();
			DateUtils data2 = new DateUtils();
	    	String dataa = data2.formatCommonDateTime(date);
			doc.addWorkHistoryEntry(Audit.create(DSApi.context().getPrincipalName(),DSApi.context().getPrincipalName(),
	                "Dokument zosta� zdekretowany na  : "+sekretariat+ " w dniu : " +dataa));
		}
	}


	

}
