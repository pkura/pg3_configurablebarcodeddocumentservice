package pl.compan.docusafe.parametrization.paa;

import java.util.Calendar;
import java.util.List;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class PlanPracy_AsgnHandler implements AssignmentHandler
{
	private static final Logger log = LoggerFactory.getLogger(PlanPracy_AsgnHandler.class);
	
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
		
		log.info("PlanPracy_AsgnHandler >");

		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		String data = Calendar.getInstance().getTime().toString();
		String rodzajDokumentu = doc.getDocumentKind().getCn();
		log.info("Dokument  id = "+docId+", Rodzaj = " +rodzajDokumentu+ ", czas zajscia procesu "+data);
		String asygnowanyUzytkownik = "admin";
		
		List<DSDivision> divisions= DSDivision.findByDescription("DYR");
		for( DSDivision div : divisions ){
			// dyrektor generalny to ten, ktorego nadrzednym jest PREZES
			if( div.getParent()!=null && div.getParent().getDescription()!=null &&
					div.getParent().getDescription().equals("PREZES"))
			{
				asygnowanyUzytkownik= div.getUsers()[0].getName();
				break;
			}
		}
		
		//dekretowanie
		assignable.addCandidateUser(asygnowanyUzytkownik);
		
		AssignmentHistoryEntry entry = new AssignmentHistoryEntry();
		
		entry.setSourceUser(DSApi.context().getDSUser().getName());
		entry.setTargetUser(asygnowanyUzytkownik);
		
		String dzialNad =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(DSApi.context().getDSUser().getName());
		entry.setSourceGuid(dzialNad);
		String dzialOdb =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(asygnowanyUzytkownik);
		entry.setTargetGuid(dzialOdb);
		
		String statusDokumentu =  doc.getFieldsManager().getEnumItem("STATUSDOKUMENTU").getTitle();
		entry.setObjective(statusDokumentu);
		entry.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
		doc.addAssignmentHistoryEntry(entry);
		
		log.info("< PlanPracy_AsgnHandler dekretuje na " +asygnowanyUzytkownik);
	}
}
