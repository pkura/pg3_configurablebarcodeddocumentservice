package pl.compan.docusafe.parametrization.paa;


import java.util.Calendar;
import java.util.Date;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.FieldSetterListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** Asygnuje dokument na przelozonego osoby wskazanej jako nadawca */
public class NadawcyPrzelozony_AsgnHandler  implements AssignmentHandler {
	private final static Logger log= LoggerFactory.getLogger(NadawcyPrzelozony_AsgnHandler.class);
	
	private String poleNadawcy; //to jest pole typu private ale mimo to pojawi si� tu warto�� z zewn�trz.
	
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
		
		log.info("NadawcyPrzelozony_AsgnHandler");
		
		
		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		
		String data = Calendar.getInstance().getTime().toString();
		String rodzajDokumentu = doc.getDocumentKind().getCn();
		log.info("Dokument  id = "+docId+"Rodzaj = " +rodzajDokumentu+ " czas zajscia procesu "+data);

		String useridstl=fm.getStringKey(poleNadawcy);
		log.info("pobra��m user id = " +useridstl);
		long userId = 0;
		try{
			userId = Long.parseLong(useridstl);
		} catch (NumberFormatException nfe){
			log.info("podane ID u�ytkownika (" +useridstl+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
			throw new IllegalStateException("podane ID u�ytkownika (" +useridstl+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
		}
		
		Person person =Person.find(userId);
		String personfirstname = person.getFirstname();
		String personLastname = person.getLastname();
		DSUser user = DSUser.findByFirstnameLastname(personfirstname, personLastname);
		
		DSUser przelozony = null; //todo przelozony
		// dyrektor jako prze�o�ony 
		String dyrektor= pl.compan.docusafe.parametrization.paa.NormalLogic.getDyrektorDepartamentu(user.getName(), doc);

		AssignmentHistoryEntry entry = new AssignmentHistoryEntry();
		
		entry.setSourceUser(DSApi.context().getDSUser().getName());
		entry.setTargetUser(user.getName());
		
		String dzialNad =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(DSApi.context().getDSUser().getName());
		entry.setSourceGuid(dzialNad);
		
		String dzialOdb =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(user.getName());
		entry.setTargetGuid(dzialOdb);
		
		String statusDokumentu =  doc.getFieldsManager().getEnumItem("STATUSDOKUMENTU").getTitle();
		entry.setObjective(statusDokumentu);
		entry.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
		doc.addAssignmentHistoryEntry(entry);
		//dekretowanie
		assignable.addCandidateUser(dyrektor);
		Date date = (Date) Calendar.getInstance().getTime();
		DateUtils data2 = new DateUtils();
    	String dataa = data2.formatCommonDateTime(date);
		doc.addWorkHistoryEntry(Audit.create(DSApi.context().getPrincipalName(),DSApi.context().getPrincipalName(),
                "Dokument zosta� zdekretowany na  : "+dyrektor+ " w dniu : " +dataa));
		
		log.info("NadawcyPrzelozony_AsgnHandler dekretuje na " +dyrektor);
		
		
		// dodac recznie do historii (w WSSK robilem to tak:) 
		// Util_AHE.addToHistory(openExecution, doc, asygnowanyUzytkownik, null, null, log);
	}
}
