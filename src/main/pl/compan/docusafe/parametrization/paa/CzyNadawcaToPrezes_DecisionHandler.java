package pl.compan.docusafe.parametrization.paa;


import java.util.Calendar;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** stwierdza, czy autor dokumentu jest nadawc� pisma */
@SuppressWarnings("serial")
public class CzyNadawcaToPrezes_DecisionHandler implements DecisionHandler {
	private static final Logger log = LoggerFactory.getLogger(CzyNadawcaToPrezes_DecisionHandler.class);
	//private String wskazanaInnaOsoba = "";
	//private String autor = "";
	public String decide(OpenExecution openExecution) {
		
		log.info("CzyNadawcaToPrezes_DecisionHandler");
		
		String decision = "autor"; //domyslne na wypadek wyjatku
		
		try {
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			
			String data = Calendar.getInstance().getTime().toString();
			String rodzajDokumentu = doc.getDocumentKind().getCn();
			log.info("Dokument  id = "+docId+" Rodzaj = " +rodzajDokumentu+ " czas zajscia procesu "+data);
			//decision = "autor";
			
			DSUser usertworz = DSApi.context().getDSUser();
			
			long useridstl=fm.getLongKey("SENDER_HERE");
			Person person =Person.find(useridstl);
			String personfirstname = person.getFirstname();
			String personLastname = person.getLastname();
			DSUser nadawca = DSUser.findByFirstnameLastname(personfirstname, personLastname);
		
			
			log.info("Sprawdzam czy Nadawca  to Prezes "+fm.getStringKey("SENDER_HERE") +"[person]---> "+nadawca.getName());
			if(nadawca.getName().equals("wlodarski"))
				 decision = "NadawcaToPrezes";
			else 
				 decision = "NadawcaToNiePrezes";
				
			//mozliwe decyzje:
			
			//autor					- nadawca to autor
			//wskazanaInnaOsoba		- nadawca zmieniony, trzeba to akceptowac

		} catch (Exception e) {
			//logowanie
			log.error("", e);
		}
		

		log.info("CzyNadawcaToPrezes_DecisionHandler zwracam decyzje decision = "+decision);
		
		return decision;
	}
}
