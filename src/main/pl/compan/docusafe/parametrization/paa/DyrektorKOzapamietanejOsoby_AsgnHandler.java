package pl.compan.docusafe.parametrization.paa;


import java.util.Calendar;
import java.util.Date;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** asygnuje na dyrektora komorki organizacyjnej,
 * w ktorej pracuje osoba zapamietana w zmiennej procesu */
@SuppressWarnings("serial")
public class DyrektorKOzapamietanejOsoby_AsgnHandler implements AssignmentHandler{
	private final static Logger log= LoggerFactory.getLogger(DyrektorKOzapamietanejOsoby_AsgnHandler.class);
	
	private String nazwaZmiennej;
	
	@Override
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception
	{
		
		log.info("DyrektorKOzapamietanejOsoby_AsgnHandler");
		
		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		
		OfficeDocument doc = OfficeDocument.find(docId);
		String data = Calendar.getInstance().getTime().toString();
		String rodzajDokumentu = doc.getDocumentKind().getCn();
		log.info("Dokument  id = "+docId+"Rodzaj = " +rodzajDokumentu+ " czas zajscia procesu "+data);
		
		
		// user, ktorego dyrektor ma byc znaleziony
		String zapamietanyUzytkownik= (String) openExecution.getVariable(nazwaZmiennej); // gotowe // username

	//	String asygnowanyUzytkownik= "";
		DSUser user= DSUser.findByUsername(zapamietanyUzytkownik);
		/**
		 * TODO dyrektor komorki pisma
		 */
		String dekretacja = "";
		//OfficeDocument doc = ;
		// dyrektor departamentu
		String dyrektor= pl.compan.docusafe.parametrization.paa.NormalLogic.getDyrektorDepartamentu(user.getName(), doc);
		String sekretariat= "";
/*		if (dyrektor.equals("WBDG")){
			dekretacja =pl.compan.docusafe.parametrization.paa.NormalLogic.getDyrektorGeneralny();
			assignable.addCandidateUser(dekretacja);
			
		}else*/
		if (!dyrektor.contains(user.getName())) {
			log.error("DyrektorKOzapamietanejOsoby_AsgnHandler - Dyrektor zosta� odnaleziony , dekretuje na dyrektora  : "
					+ dyrektor);
			// dekretowanie
			assignable.addCandidateUser(dyrektor);
		 dekretacja = dyrektor;
		} else {
			log.info("DyrektorKOzapamietanejOsoby_AsgnHandler - Dyrektor nie zosta� odnaleziony - wyszukuj� po dziale - pracownika sekretariatu ");
			sekretariat= pl.compan.docusafe.parametrization.paa.NormalLogic
					.getUserPelniocyRoleSekretariatu(user.getName());

			if (!sekretariat.contains(user.getName())) {
				log.error("DyrektorKOzapamietanejOsoby_AsgnHandler - Pracownik sekretariatu zosta� odnaleziony , dekretuje na pracownika : "
						+ sekretariat);
				// dekretowanie
				assignable.addCandidateUser(sekretariat);
				 dekretacja = sekretariat;
			} else {
				dekretacja = "admin";
				assignable.addCandidateUser(dekretacja);
				log.info("DyrektorKOzapamietanejOsoby_AsgnHandler - Pracownik sekretariatu nie zosta� odnaleziony - dokument trafia do administratora i sie zawiesza ID "
			+openExecution.getProcessDefinitionId()+" ; KEY "+openExecution.getKey()+";NAME "+openExecution.getName()+" STATE "+openExecution.getState());
				// dekretowanie
				//assignable.addCandidateUser(user.getName());
			}
		}
		// String asygnowanyUzytkownik = ".....okreslic......";
		// dekretowanie
		// assignable.addCandidateUser(asygnowanyUzytkownik);
		// dodac recznie do historii (w WSSK robilem to tak:)
		// Util_AHE.addToHistory(openExecution, doc, asygnowanyUzytkownik, null, null, log);
		
		
		
	
		AssignmentHistoryEntry entry = new AssignmentHistoryEntry();
		entry.setSourceUser(zapamietanyUzytkownik);
		entry.setTargetUser(dekretacja);
		String dzialNad =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(zapamietanyUzytkownik);
		entry.setSourceGuid(dzialNad);
		String dzialOdb =pl.compan.docusafe.parametrization.paa.NormalLogic.getGuidDzialUzytkownika(dekretacja);
		entry.setTargetGuid(dzialOdb);
		String statusDokumentu =  doc.getFieldsManager().getEnumItem("STATUSDOKUMENTU").getTitle();
		entry.setObjective(statusDokumentu);
		entry.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
		doc.addAssignmentHistoryEntry(entry);
		
		Date date = (Date) Calendar.getInstance().getTime();
		DateUtils data2 = new DateUtils();
    	String dataa = data2.formatCommonDateTime(date);
		doc.addWorkHistoryEntry(Audit.create(DSApi.context().getPrincipalName(),DSApi.context().getPrincipalName(),
                "Dokument zosta� zdekretowany na  : "+dekretacja+ " w dniu : " +dataa));
	}

}
