package pl.compan.docusafe.parametrization.ima;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.ValidationException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.ima.dictionary.Mediator;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.form.UpdateUser;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class MediatorLogic extends AbstractDocumentLogic {

	private static MediatorLogic instance;
	protected static Logger log = LoggerFactory.getLogger(MediatorLogic.class);

	// static StringManager sm =
	// GlobalPreferences.loadPropertiesFile(MediatorLogic.class.getPackage().getName(),null);
	private static final StringManager sm = StringManager
			.getManager(MediatorLogic.class.getPackage().getName());

	public static MediatorLogic getInstance() {
		if (instance == null)
			instance = new MediatorLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "MEDIATOR_READ",
				ObjectPermission.GROUP, "Mediator - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,
				"MEDIATOR_ATT_READ", ObjectPermission.GROUP,
				"Mediator za��cznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY,
				"MEDIATOR_READ_MODIFY", ObjectPermission.GROUP,
				"Mediator - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,
				"MEDIATOR_READ_ATT_MODIFY", ObjectPermission.GROUP,
				"Mediator za��cznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE,
				"MEDIATOR_READ_DELETE", ObjectPermission.GROUP,
				"Mediator - usuwanie"));

		for (PermissionBean perm : perms) {
			if (perm.isCreateGroup()
					&& perm.getSubjectType().equalsIgnoreCase(
							ObjectPermission.GROUP)) {
				String groupName = perm.getGroupName();
				if (groupName == null) {
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	public void setInitialValues(FieldsManager fm, int type)
			throws EdmException {
		DataBaseEnumField def = (DataBaseEnumField)fm.getField("DS_USER");
		def.reload();
	}

	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {
		if (document.getDocumentKind() == null)
			throw new NullPointerException("document.getDocumentKind()");

		// FieldsManager fm =
		// document.getDocumentKind().getFieldsManager(document.getId());
		/*
		 * Folder folder = Folder.getRootFolder(); document.setFolder(folder);
		 */
		// Zmieniamy typ dokumentu
		DocumentKind kind = DocumentKind.findByCn("mediator_show");
		
		document.setDocumentKind(kind);
	}

	/**
	 * Standardowa implementacja - nic nie robi
	 * @return 
	 */
	@Override
	public Field validateDwr(Map<String, FieldData> values,  FieldsManager fm) throws EdmException
	{
		log.info("validate contents zapotrzebowanie ifpan: \n {} ", values);
		
		// Sprawdzenie, czy ustawiono u�ytkownika systemu
		if (values.get("DWR_DS_USER") == null) {
			throw new EdmException(sm.getString("NieWybranoPracownika"));
		}
		 		
		// Pobranie identyfikatora u�ytkownika
		FieldData fd = values.get("DWR_DS_USER");
		EnumValues ev = fd.getEnumValuesData();
		List<String> selectedOptions = ev.getSelectedOptions();
		Long iddsuser = null;
		if(selectedOptions != null && selectedOptions.size() > 0) {
			try {
				iddsuser = Long.parseLong(selectedOptions.get(0)); // opcja --wybierz-- ma warto�� pusty string
			} catch(NumberFormatException e) {
				iddsuser = null;
			}
		}
		// Sprawdzenie, czy taki mediator nie istnieje ju� w bazie
		if(iddsuser != null) {
			DSApi.openAdmin();
			List<Mediator> mediators = Mediator.findByDSUserId(iddsuser);
			if (mediators != null && mediators.size() > 0) {
				throw new EdmException(sm
						.getString("MediatorIstniejeWSystemie"));
			}
			DSApi.close();
		}
		
		return null;
		
	}

	@Override
	public void validate(Map<String, Object> values, DocumentKind kind,
			Long documentId) throws EdmException {

	}

	@Override
	public void onSaveActions(DocumentKind kind, Long documentId,
			Map<String, ?> fieldValues) throws SQLException, EdmException {
		// Pobranie identyfikatora u�ytkownika
		Long iddsuser = Long.parseLong(fieldValues.get("DS_USER").toString());

		// Pobranie obiektu u�ytkownik
		DSUser user = (DSUser) UserImpl.find(iddsuser);

		// Pobranie grup, do ktorych nalezy uzytkownik
		String[] groups = user.getGroups();

		// Sprawdzenie, czy dokument nalezy do grupy MEDIATOR,
		boolean result = false;
		for (String group : groups) {
			if (group == "MEDIATOR") {
				result = true;
				break;
			}
		}

		// Jezeli nie jest w grupie MEDIATOR, to nalezy go dodac
		if (!result) {
			// Przypisanie u�ytkownika do grupy MEDIATOR

			String[] dstGroups = new String[groups.length + 1];

			System.arraycopy(groups, 0, dstGroups, 0, groups.length);

			dstGroups[dstGroups.length - 1] = "MEDIATOR";

			UpdateUser.updateGroups(dstGroups, user);
		}

		// Aktualizacja danych mediatora: imie, nazwisko i email (do popupu)

		Mediator mediator = Mediator.findByDocumentId(documentId);
		mediator.setFirstname(user.getFirstname());
		mediator.setLastname(user.getLastname());
		mediator.setEmail(user.getEmail());

		DSApi.context().session().save(mediator);

		// Wkladamy karte do folerdu Karty mediatorow

		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Mediators");
		Document doc = Document.find(documentId);
		doc.setFolder(folder);
		doc.setTitle("Mediator Card - " + mediator.getLastname() + " "
				+ mediator.getFirstname());
		
	}

	@Override
	public boolean isPersonalRightsAllowed() {
		return true;
	}

}
