                      
-- ds_doccase
IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_doccase') 
DROP TABLE [ds_doccase];

GO

CREATE TABLE ds_doccase(
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[casetype] [varchar](50) NULL,
	[casestatus] [varchar](50) NULL,
	[requiredfinishdate] [datetime] NULL,
	[idmediator] [int] NULL,
	[idmediator2] [int] NULL,
	[casemanager] [int] NULL,
	[casemanager2] [int] NULL,
	[title] [varchar](50) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[startdate] [datetime] NULL,
	[finishdate] [datetime] NULL,
	[idcustomers] [int] NULL,
	[idcustomers2] [int] NULL,
	[test] [int] NULL,
	[fee] [float] NULL,
	[number] [varchar](256) NULL,
	[mandatory] [smallint] NULL
) ON [PRIMARY]

GO

-- ds_ima_case_milestones
IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_ima_case_milestones') 
DROP TABLE [ds_ima_case_milestones];

GO

CREATE TABLE ds_ima_case_milestones(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](1000) NOT NULL,
	[milestone_date] [varchar](50) NULL,
	[place] [varchar](1000) NOT NULL,
	[place_address] [varchar](1000) NOT NULL,
	[participants] [varchar](1000) NULL
) ON [PRIMARY]

GO

-- ds_docincase

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_docincase') 
DROP TABLE [ds_docincase];

GO

CREATE TABLE [ds_docincase](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[deadline_time] [datetime] NULL,
	[doctype] [nchar](100) NULL,
	[docstatus] [nchar](100) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[dsuser] [nvarchar](256) NULL,
	[taskdescription] [nvarchar](1000) NULL,
	[iddoccase] [nvarchar](50) NULL
) ON [PRIMARY]

GO

-- ds_docincase_dictionary

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_docincase_dictionary') 
DROP TABLE [ds_docincase_dictionary];

GO

CREATE TABLE [ds_docincase_dictionary](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[FIELD_CN] [varchar](50) NOT NULL,
	[FIELD_VAL] [varchar](50) NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO

-- ds_ima_customers

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_ima_customers') 
DROP TABLE ds_ima_customers;

GO

CREATE TABLE ds_ima_customers(
	[firstname] [varchar](50) NULL,
	[lastname] [varchar](50) NULL,
	[company] [varchar](1000) NULL,
	[email] [varchar](256) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[address] [varchar](1000) NULL,
	[zip] [varchar](50) NULL,
	[town] [varchar](1000) NULL,
	[country] [varchar](1000) NULL,
	[languages] [varchar](1000) NULL,
	[fee] [float] NULL,
	[invoice_address] [varchar](1000) NULL,
	[vat] [varchar](50) NULL,
	[reference] [varchar](1000) NULL,
	[nationality] [varchar](1000) NULL,
	[customer_kind] [varchar](50) NULL,
	[full_name] [varchar](1000) NULL,
	[phone] [varchar](50) NULL
) ON [PRIMARY]

GO

--ds_docincase_status

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_docincase_status') 
DROP TABLE [ds_docincase_status];

GO

CREATE TABLE [ds_docincase_status](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NULL,
	[language] [varchar](255) NULL
) ON [PRIMARY]

GO

INSERT INTO ds_docincase_status (cn,title,centrum,available,language) VALUES ('filed','WNIESIONA',0,0,'pl')
GO
INSERT INTO ds_docincase_status (cn,title,centrum,available,language) VALUES ('filed','FILED',0,0,'en')
GO
INSERT INTO ds_docincase_status (cn,title,centrum,available,language) VALUES ('inprogress','W TOKU',0,0,'pl')
GO
INSERT INTO ds_docincase_status (cn,title,centrum,available,language) VALUES ('inprogress','IN PROGRESS',0,0,'en')
GO
INSERT INTO ds_docincase_status (cn,title,centrum,available,language) VALUES ('canceled','ANULOWANA',0,0,'pl')
GO
INSERT INTO ds_docincase_status (cn,title,centrum,available,language) VALUES ('canceled','CANCELED',0,0,'en')
GO
INSERT INTO ds_docincase_status (cn,title,centrum,available,language) VALUES ('settled','SETTLED',0,0,'pl')
GO
INSERT INTO ds_docincase_status (cn,title,centrum,available,language) VALUES ('settled','FILED',0,0,'en')
GO
INSERT INTO ds_docincase_status (cn,title,centrum,available,language) VALUES ('closed','ZAMKNIETA/ZARCHIWIZOWANA',0,0,'pl')
GO
INSERT INTO ds_docincase_status (cn,title,centrum,available,language) VALUES ('closed','CLOSED/ARCHIVED',0,0,'en')
GO
INSERT INTO ds_docincase_status (cn,title,centrum,available,language) VALUES ('archived','ZARCHIWIZOWANA',0,0,'pl')
GO
INSERT INTO ds_docincase_status (cn,title,centrum,available,language) VALUES ('archived','ARCHIVED',0,0,'en')
GO



-- ds_docincase_type


IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_docincase_type') 
DROP TABLE [ds_docincase_type];

GO

CREATE TABLE [ds_docincase_type](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NULL,
	[language] [varchar](255) NULL
) ON [PRIMARY]

GO


INSERT INTO ds_docincase_type (cn,title,centrum,available,language) VALUES ('mediation_contract','MEDIATION CONTRACT',0,0,'en')
GO
INSERT INTO ds_docincase_type (cn,title,centrum,available,language) VALUES ('mediation_egreement','MEDIATION AGREEMENT',0,0,'en')
GO
INSERT INTO ds_docincase_type (cn,title,centrum,available,language) VALUES ('emails','EMAILS',0,0,'en')
GO
INSERT INTO ds_docincase_type (cn,title,centrum,available,language) VALUES ('tasks','TASKS',0,0,'en')
GO
INSERT INTO ds_docincase_type (cn,title,centrum,available,language) VALUES ('dates','DATES',0,0,'en')
GO
INSERT INTO ds_docincase_type (cn,title,centrum,available,language) VALUES ('documents','DOCUMENTS',0,0,'en')
GO
INSERT INTO ds_docincase_type (cn,title,centrum,available,language) VALUES ('note','NOTES',0,0,'en')
GO
INSERT INTO ds_docincase_type (cn,title,centrum,available,language) VALUES ('evaluation_documents','EVALUATION DOCUMENTS',0,0,'en')
GO
INSERT INTO ds_docincase_type (cn,title,centrum,available,language) VALUES ('mediation_contract','UMOWA MEDIACJI',0,0,'pl')
GO
INSERT INTO ds_docincase_type (cn,title,centrum,available,language) VALUES ('mediation_egreement','POZWOLENIE NA MEDIACJ�',0,0,'pl')
GO
INSERT INTO ds_docincase_type (cn,title,centrum,available,language) VALUES ('emails','POCZTA',0,0,'pl')
GO
INSERT INTO ds_docincase_type (cn,title,centrum,available,language) VALUES ('tasks','ZADANIA',0,0,'pl')
GO
INSERT INTO ds_docincase_type (cn,title,centrum,available,language) VALUES ('dates','SPOTKANIA',0,0,'pl')
GO
INSERT INTO ds_docincase_type (cn,title,centrum,available,language) VALUES ('documents','DOKUMENTY',0,0,'pl')
GO
INSERT INTO ds_docincase_type (cn,title,centrum,available,language) VALUES ('note','NOTATKA',0,0,'pl')
GO
INSERT INTO ds_docincase_type (cn,title,centrum,available,language) VALUES ('evaluation_documents','DOKUMENTY ROZWOJOWE',0,0,'pl')
GO




--ds_ima_mediator

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_ima_mediator') 
DROP TABLE [ds_ima_mediator];

GO

CREATE TABLE ds_ima_mediator(
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[firstname] [varchar](50) NULL,
	[lastname] [varchar](50) NULL,
	[email] [varchar](256) NULL,
	[domain] [varchar](256) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[iddsuser] [decimal](18, 0) NULL,
	[rq1] [int] NULL,
	[rq2] [int] NULL,
	[rq3] [int] NULL,
	[rq4] [int] NULL,
	[rq5] [int] NULL,
	[rq6] [int] NULL,
	[languages] [varchar](50) NULL,
	[specializations] [varchar](50) NULL,
	[note] [int] NULL,
	[iban] [varchar](256) NULL,
	[bic] [varchar](256) NULL
) ON [PRIMARY]

GO


-- ds_ima_mediator_domains

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_ima_mediator_domains') 
DROP TABLE [ds_ima_mediator_domains];

GO

CREATE TABLE [ds_ima_mediator_domains](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[FIELD_CN] [varchar](50) NOT NULL,
	[FIELD_VAL] [varchar](50) NOT NULL
) ON [PRIMARY]

GO

-- ds_ima_questionnaire

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_ima_questionnaire') 
DROP TABLE [ds_ima_questionnaire];

GO

CREATE TABLE [ds_ima_questionnaire](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[iddoccase] [numeric](19, 0) NULL,
	[title] [nvarchar](100) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idquestions] [int] NULL,
	[idmediator] [int] NULL
) ON [PRIMARY]

GO

-- ds_ima_questionnaire_questions

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_ima_questionnaire_questions') 
DROP TABLE [ds_ima_questionnaire_questions];

GO

CREATE TABLE [ds_ima_questionnaire_questions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[DOCUMENT_ID] [numeric](19, 0) NULL,
	[q1] [int] NULL,
	[q2] [int] NULL,
	[q3] [int] NULL,
	[q4] [int] NULL,
	[q5] [int] NULL,
	[q6] [int] NULL
) ON [PRIMARY]

GO

-- ds_ima_case_status

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_ima_case_status') 
DROP TABLE [ds_ima_case_status];
GO

CREATE TABLE [ds_ima_case_status](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NULL,
	[language] [varchar](256) NULL
) ON [PRIMARY]

GO

INSERT INTO ds_ima_case_status  (cn,title,centrum,available,language) VALUES ('new','NOWA',0,0,'pl')
GO
INSERT INTO ds_ima_case_status  (cn,title,centrum,available,language) VALUES ('new','NEW',0,0,'en')
GO
INSERT INTO ds_ima_case_status  (cn,title,centrum,available,language) VALUES ('inprogress','W TOKU',0,0,'pl')

INSERT INTO ds_ima_case_status  (cn,title,centrum,available,language) VALUES ('inprogress','IN PROGRESS',0,0,'en')
GO
INSERT INTO ds_ima_case_status  (cn,title,centrum,available,language) VALUES ('closed','ZAMKNI�TA',0,0,'pl')
GO
INSERT INTO ds_ima_case_status  (cn,title,centrum,available,language) VALUES ('closed','CLOSED',0,0,'en')
GO
INSERT INTO ds_ima_case_status  (cn,title,centrum,available,language) VALUES ('canceled','ANULOWANA',0,0,'pl')
GO
INSERT INTO ds_ima_case_status  (cn,title,centrum,available,language) VALUES ('canceled','CANCELED',0,0,'en')
GO



-- ds_ima_case_type

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_ima_case_type') 
DROP TABLE [ds_ima_case_type];

GO

CREATE TABLE [ds_ima_case_type](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NULL,
	[language] [varchar](256) NULL
) ON [PRIMARY]

GO

INSERT INTO ds_ima_case_type (cn,title,centrum,available,language) VALUES ('mediacja_biznesowa','Mediacja Biznesowa',0,0,'pl')
GO
INSERT INTO ds_ima_case_type (cn,title,centrum,available,language) VALUES ('mediacja_biznesowa','Business mediation',0,0,'en')
GO
INSERT INTO ds_ima_case_type (cn,title,centrum,available,language) VALUES ('mediacja_prawna','Mediacja Prawna',0,0,'pl')
GO
INSERT INTO ds_ima_case_type (cn,title,centrum,available,language) VALUES ('mediacja_prawna','Law mediation',0,0,'en')
GO


-- ds_ima_doccase_dictionary

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_ima_doccase_dictionary') 
DROP TABLE [ds_ima_doccase_dictionary];

GO

CREATE TABLE [ds_ima_doccase_dictionary](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[FIELD_CN] [varchar](50) NOT NULL,
	[FIELD_VAL] [varchar](50) NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO

-- ds_ima_doctemplate

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_ima_doctemplate') 
DROP TABLE [ds_ima_doctemplate];

GO

CREATE TABLE ds_ima_doctemplate(
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[title] [nvarchar](256) NULL,
	[description] [nvarchar](1000) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[languages] [nvarchar](1000) NULL
) ON [PRIMARY]

GO

-- ds_ima_task

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_ima_task') 
DROP TABLE [ds_ima_task];

GO

CREATE TABLE [ds_ima_task](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](256) NULL,
	[task_description] [nvarchar](1000) NULL,
	[dsuser] [nvarchar](256) NULL,
	[deadline_time] [datetime] NULL,
	[connected_case] [nvarchar](50) NULL
) ON [PRIMARY]

GO


-- ds_ima_task_dictionary
IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_ima_task_dictionary') 
DROP TABLE [ds_ima_task_dictionary];

GO

CREATE TABLE ds_ima_task_dictionary(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	FIELD_CN varchar(50) NOT NULL,
	FIELD_VAL varchar(50) NOT NULL,
	ID int IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO

-- DS_IMA_COUNTRY_DICTIONARY

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_ima_country_dictionary') 
DROP TABLE [ds_ima_country_dictionary];

GO

CREATE TABLE [ds_ima_country_dictionary](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NULL,
	[language] [varchar](255) NULL
) ON [PRIMARY]

GO

INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Andorra','Andorra',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('United','United',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Afghanistan','Afghanistan',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Antigua','Antigua',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Anguilla','Anguilla',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Albania','Albania',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Armenia','Armenia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Netherlands','Netherlands',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Angola','Angola',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Antarctica','Antarctica',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Argentina','Argentina',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('American','American',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Austria','Austria',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Australia','Australia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Aruba','Aruba',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Azerbaijan','Azerbaijan',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Bosnia','Bosnia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Barbados','Barbados',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Bangladesh','Bangladesh',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Belgium','Belgium',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Burkina','Burkina',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Bulgaria','Bulgaria',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Bahrain','Bahrain',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Burundi','Burundi',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Benin','Benin',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Bermuda','Bermuda',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Brunei','Brunei',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Bolivia','Bolivia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Brazil','Brazil',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Bahamas','Bahamas',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Bhutan','Bhutan',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Bouvet','Bouvet',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Botswana','Botswana',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Belarus','Belarus',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Belize','Belize',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Canada','Canada',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Cocos','Cocos',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Central','Central',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Congo','Congo',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Switzerland','Switzerland',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Cote','Cote',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Cook','Cook',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Chile','Chile',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Cameroon','Cameroon',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('China','China',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Colombia','Colombia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Costa','Costa',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Czechoslovakia','Czechoslovakia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Cuba','Cuba',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Cape','Cape',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Christmas','Christmas',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Cyprus','Cyprus',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Czech','Czech',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Germany','Germany',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Djibouti','Djibouti',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Denmark','Denmark',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Dominica','Dominica',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Dominican','Dominican',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Algeria','Algeria',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Ecuador','Ecuador',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Estonia','Estonia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Egypt','Egypt',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Western','Western',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Eritrea','Eritrea',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Spain','Spain',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Ethiopia','Ethiopia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Finland','Finland',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Fiji','Fiji',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Falkland','Falkland',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Micronesia','Micronesia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Faroe','Faroe',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('France','France',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('France,','France,',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Gabon','Gabon',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Great','Great',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Grenada','Grenada',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Georgia','Georgia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('French','French',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Ghana','Ghana',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Gibraltar','Gibraltar',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Greenland','Greenland',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Gambia','Gambia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Guinea','Guinea',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Guadeloupe','Guadeloupe',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Equatorial','Equatorial',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Greece','Greece',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('S.','S.',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Guatemala','Guatemala',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Guam','Guam',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Guinea-Bissau','Guinea-Bissau',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Guyana','Guyana',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Hong','Hong',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Heard','Heard',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Honduras','Honduras',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Croatia','Croatia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Haiti','Haiti',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Hungary','Hungary',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Indonesia','Indonesia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Ireland','Ireland',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Israel','Israel',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('India','India',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('British','British',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Iraq','Iraq',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Iran','Iran',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Iceland','Iceland',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Italy','Italy',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Jamaica','Jamaica',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Jordan','Jordan',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Japan','Japan',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Kenya','Kenya',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Kyrgyzstan','Kyrgyzstan',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Cambodia','Cambodia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Kiribati','Kiribati',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Comoros','Comoros',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Saint','Saint',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Korea','Korea',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Korea','Korea',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Kuwait','Kuwait',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Cayman','Cayman',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Kazakhstan','Kazakhstan',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Laos','Laos',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Lebanon','Lebanon',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Saint','Saint',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Liechtenstein','Liechtenstein',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Sri','Sri',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Liberia','Liberia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Lesotho','Lesotho',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Lithuania','Lithuania',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Luxembourg','Luxembourg',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Latvia','Latvia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Libya','Libya',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Morocco','Morocco',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Monaco','Monaco',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Moldova','Moldova',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Madagascar','Madagascar',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Marshall','Marshall',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Macedonia','Macedonia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Mali','Mali',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Myanmar','Myanmar',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Mongolia','Mongolia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Macau','Macau',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Northern','Northern',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Martinique','Martinique',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Mauritania','Mauritania',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Montserrat','Montserrat',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Malta','Malta',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Mauritius','Mauritius',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Maldives','Maldives',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Malawi','Malawi',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Mexico','Mexico',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Malaysia','Malaysia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Mozambique','Mozambique',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Namibia','Namibia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('New','New',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Niger','Niger',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Norfolk','Norfolk',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Nigeria','Nigeria',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Nicaragua','Nicaragua',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Netherlands','Netherlands',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Norway','Norway',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Nepal','Nepal',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Nauru','Nauru',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Neutral','Neutral',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Niue','Niue',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('New','New',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Oman','Oman',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Panama','Panama',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Peru','Peru',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('French','French',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Papua','Papua',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Philippines','Philippines',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Pakistan','Pakistan',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Poland','Poland',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('St.','St.',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Pitcairn','Pitcairn',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Puerto','Puerto',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Portugal','Portugal',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Palau','Palau',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Paraguay','Paraguay',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Qatar','Qatar',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Reunion','Reunion',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Romania','Romania',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Russian','Russian',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Rwanda','Rwanda',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Saudi','Saudi',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Solomon','Solomon',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Seychelles','Seychelles',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Sudan','Sudan',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Sweden','Sweden',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Singapore','Singapore',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('St.','St.',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Slovenia','Slovenia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Svalbard','Svalbard',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Slovak','Slovak',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Sierra','Sierra',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('San','San',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Senegal','Senegal',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Somalia','Somalia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Suriname','Suriname',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Sao','Sao',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('USSR','USSR',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('El','El',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Syria','Syria',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Swaziland','Swaziland',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Turks','Turks',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Chad','Chad',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('French','French',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Togo','Togo',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Thailand','Thailand',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Tajikistan','Tajikistan',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Tokelau','Tokelau',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Turkmenistan','Turkmenistan',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Tunisia','Tunisia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Tonga','Tonga',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('East','East',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Turkey','Turkey',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Trinidad','Trinidad',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Tuvalu','Tuvalu',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Taiwan','Taiwan',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Tanzania','Tanzania',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Ukraine','Ukraine',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Uganda','Uganda',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('United','United',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('US','US',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('United','United',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Uruguay','Uruguay',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Uzbekistan','Uzbekistan',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Vatican','Vatican',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Saint','Saint',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Venezuela','Venezuela',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Virgin','Virgin',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Virgin','Virgin',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Viet','Viet',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Vanuatu','Vanuatu',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Wallis','Wallis',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Samoa','Samoa',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Yemen','Yemen',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Mayotte','Mayotte',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Yugoslavia','Yugoslavia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('South','South',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Zambia','Zambia',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Zaire','Zaire',0,0,'en') 
GO
INSERT INTO ds_ima_country_dictionary (cn,title,centrum,available,language) VALUES ('Zimbabwe','Zimbabwe',0,0,'en') 
GO


--v_ds_ima_not_mediators

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE  TABLE_NAME = 'v_ds_ima_not_mediators') 
DROP VIEW [v_ds_ima_not_mediators];

GO 

CREATE VIEW [v_ds_ima_not_mediators]
AS
SELECT     ID, NAME AS CN, LASTNAME + ' ' + FIRSTNAME AS TITLE, 0 AS CENTRUM, NULL AS REFVALUE, 0 AS AVAILABLE
FROM         dbo.DS_USER
WHERE     (ID NOT IN
                          (SELECT     iddsuser
                            FROM          dbo.ds_ima_mediator))

GO

--v_ds_ima_doccase

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE  TABLE_NAME = 'v_ds_ima_doccase') 
DROP VIEW [v_ds_ima_doccase];

GO

CREATE VIEW [v_ds_ima_doccase]
AS
SELECT     dbo.ds_doccase.DOCUMENT_ID, dbo.ds_doccase.title, dbo.ds_doccase.requiredfinishdate, dbo.ds_doccase.id AS iddoccase, dbo.ds_doccase.id, 
                      dbo.ds_doccase.startdate, dbo.ds_doccase.finishdate, dbo.ds_ima_mediator.firstname, dbo.ds_ima_mediator.lastname, dbo.ds_ima_mediator.email, 
                      dbo.ds_ima_case_status.title AS casestatusname, dbo.ds_ima_case_type.title AS casetypename, dbo.ds_ima_doccase_dictionary.FIELD_CN, 
                      dbo.ds_ima_doccase_dictionary.FIELD_VAL, dbo.ds_ima_mediator.id AS idmediator
FROM         dbo.ds_doccase LEFT OUTER JOIN
                      dbo.ds_ima_case_type ON dbo.ds_doccase.casetype = dbo.ds_ima_case_type.id LEFT OUTER JOIN
                      dbo.ds_ima_case_status ON dbo.ds_doccase.casestatus = dbo.ds_ima_case_status.id INNER JOIN
                      dbo.ds_ima_doccase_dictionary ON dbo.ds_doccase.DOCUMENT_ID = dbo.ds_ima_doccase_dictionary.DOCUMENT_ID INNER JOIN
                      dbo.ds_ima_mediator ON dbo.ds_ima_doccase_dictionary.FIELD_VAL = dbo.ds_ima_mediator.id
WHERE     (dbo.ds_ima_doccase_dictionary.FIELD_CN = 'DS_IMA_MEDIATOR')

GO

                      
                      
--v_ds_ima_mediator_notes 
IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE  TABLE_NAME = 'v_ds_ima_mediator_notes') 
DROP VIEW [v_ds_ima_mediator_notes];

GO

CREATE VIEW [dbo].[v_ds_ima_mediator_notes]
AS
SELECT     AVG(dbo.v_ds_ima_questionnaire_result.q1) AS rq1, AVG(dbo.v_ds_ima_questionnaire_result.q2) AS rq2, AVG(dbo.v_ds_ima_questionnaire_result.q3) 
                      AS rq3, AVG(dbo.v_ds_ima_questionnaire_result.q4) AS rq4, AVG(dbo.v_ds_ima_questionnaire_result.q5) AS rq5, 
                      AVG(dbo.v_ds_ima_questionnaire_result.q6) AS rq6, dbo.v_ds_ima_questionnaire_result.idmediator AS id, dbo.ds_ima_mediator.firstname, 
                      dbo.ds_ima_mediator.lastname, dbo.ds_ima_mediator.email, dbo.ds_ima_mediator.domain, dbo.ds_ima_mediator.id AS idmediator
FROM         dbo.v_ds_ima_questionnaire_result INNER JOIN
                      dbo.ds_ima_mediator ON dbo.v_ds_ima_questionnaire_result.idmediator = dbo.ds_ima_mediator.id
GROUP BY dbo.v_ds_ima_questionnaire_result.idmediator, dbo.ds_ima_mediator.firstname, dbo.ds_ima_mediator.lastname, dbo.ds_ima_mediator.email, 
                      dbo.ds_ima_mediator.domain, dbo.ds_ima_mediator.id

GO
                    

-- v_ds_ima_questionnaire_result   
IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE  TABLE_NAME = 'v_ds_ima_questionnaire_result') 
DROP VIEW [v_ds_ima_questionnaire_result];

GO

CREATE VIEW [dbo].[v_ds_ima_questionnaire_result]
AS
SELECT     dbo.ds_ima_questionnaire.DOCUMENT_ID, dbo.ds_ima_questionnaire.iddoccase, dbo.ds_ima_questionnaire.idmediator, 
                      dbo.ds_ima_questionnaire.title, dbo.ds_ima_questionnaire.id, dbo.ds_ima_questionnaire.idquestions, dbo.ds_ima_questionnaire_questions.q1, 
                      dbo.ds_ima_questionnaire_questions.q2, dbo.ds_ima_questionnaire_questions.q3, dbo.ds_ima_questionnaire_questions.q4, 
                      dbo.ds_ima_questionnaire_questions.q5, dbo.ds_ima_questionnaire_questions.q6
FROM         dbo.ds_ima_questionnaire LEFT OUTER JOIN
                      dbo.ds_ima_questionnaire_questions ON dbo.ds_ima_questionnaire.idquestions = dbo.ds_ima_questionnaire_questions.id

GO
  

--v_ds_ima_customer_full

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE  TABLE_NAME = 'v_ds_ima_customer_full') 
DROP VIEW [v_ds_ima_customer_full];

GO 

CREATE VIEW [v_ds_ima_customer_full]
AS
SELECT     dbo.ds_ima_customers.firstname, dbo.ds_ima_customers.lastname, dbo.ds_ima_customers.email, dbo.ds_ima_customers.address, 
                      dbo.ds_ima_customers.zip, dbo.ds_ima_customers.town, dbo.ds_ima_customers.country, dbo.ds_ima_customers.languages, 
                      dbo.ds_ima_customers.fee, dbo.ds_ima_customers.invoice_address, dbo.ds_ima_customers.vat, dbo.ds_ima_customers.reference, 
                      dbo.ds_ima_customers.nationality, dbo.ds_ima_customers.customer_kind, dbo.ds_ima_customers.full_name, dbo.ds_ima_customers.phone, 
                      dbo.ds_ima_customers_kind.role, dbo.ds_ima_customers_kind.id AS kindid, CEILING(((dbo.ds_ima_customers_kind.id + dbo.ds_ima_customers.id) 
                      * (dbo.ds_ima_customers_kind.id + dbo.ds_ima_customers.id + 1) + dbo.ds_ima_customers.id) / 2) AS id, ds_ima_customers.company
FROM         dbo.ds_ima_customers_kind INNER JOIN
                      dbo.ds_ima_customers ON dbo.ds_ima_customers_kind.customer_id = dbo.ds_ima_customers.id

GO


-- trigger

IF EXISTS(SELECT name FROM sys.triggers WHERE name = 't_ds_ima_customer_full_insert') 
DROP TRIGGER t_ds_ima_customer_full_insert;

GO

CREATE TRIGGER t_ds_ima_customer_full_insert 
   ON  v_ds_ima_customer_full 
   INSTEAD OF INSERT
AS 
		
	DECLARE @exists Int
	DECLARE @b Varchar(50)
	
	DECLARE @firstname Varchar(50)
	DECLARE @lastname Varchar(50)
	DECLARE @email Varchar(50)
	DECLARE @customerid Int
	DECLARE @fullName VARCHAR(100)

	SELECT @firstname = (SELECT firstname FROM inserted)
	SELECT @lastname = (SELECT lastname FROM inserted)
	SELECT @email = (SELECT email FROM inserted)
	
		
	-- Sprawdzenie, czy Customer juz jest w bazie
	SELECT @exists = (SELECT COUNT(*) FROM ds_ima_customers i WHERE
	 i.firstname = @firstname AND i.lastname = @lastname AND i.email = @email)
	 
	 if @exists=0
	 BEGIN
		-- Customer nie istnieje w bazie, wiec go zapisujemy
		SELECT @fullName = (SELECT firstname + ' ' + lastname + ' ' + email + ' ' + phone FROM Inserted)
		
		INSERT INTO ds_ima_customers 
		(firstname,lastname,email,address,zip,town,country,languages,fee,invoice_address,vat,reference,
		nationality,phone,full_name) SELECT 
		j.firstname,j.lastname,j.email,j.address,j.zip,j.town,j.country,j.languages,j.fee,
		j.invoice_address,j.vat,j.reference,j.nationality,j.phone,@fullName
		 FROM inserted j
		 
		
	 END
	 
	-- Pobranie id customera
	SELECT @customerid = (SELECT id FROM ds_ima_customers i WHERE
	i.firstname = @firstname AND i.lastname = @lastname AND i.email = @email)
	 
	-- Zapisanie w bazie rodzaju customera
	INSERT INTO ds_ima_customers_kind (customer_id,role)
	SELECT @customerid,role FROM inserted
	






IF EXISTS(SELECT name FROM sys.triggers WHERE name = 't_ds_ima_customer_full_update') 
DROP TRIGGER t_ds_ima_customer_full_update;

GO

CREATE TRIGGER t_ds_ima_customer_full_update 
   ON  v_ds_ima_customer_full 
   INSTEAD OF UPDATE
AS 
	DECLARE @customerid Int
	DECLARE @kindid Int
	
	-- Pobranie id aktualizowanego customera
	
	SELECT @customerid = (SELECT id FROM inserted)
	SELECT @kindid = (SELECT kindid FROM inserted)
	
	-- Aktualizacja tabeli ds_ima_customer
	
	UPDATE ds_ima_customers SET
		firstname = (SELECT firstname FROM inserted),
		lastname = (SELECT lastname FROM inserted),
		email = (SELECT email FROM inserted),
		address = (SELECT address FROM inserted),
		zip = (SELECT zip FROM inserted),
		town = (SELECT town FROM inserted),
		country = (SELECT country FROM inserted),
		languages = (SELECT languages FROM inserted),
		fee = (SELECT fee FROM inserted),
		invoice_address = (SELECT invoice_address FROM inserted),
		vat = (SELECT vat FROM inserted),
		reference = (SELECT reference FROM inserted),
		nationality = (SELECT nationality FROM inserted),
		phone = (SELECT phone FROM inserted),
		full_name = (SELECT firstname + ' ' + lastname + ' ' + email + ' ' + phone FROM Inserted)
	WHERE id = @customerid	
	
	-- Aktualizacja tabeli ds_ima_customer_kind
	
	UPDATE ds_ima_customers_kind SET
		role = (SELECT role FROM inserted)
	WHERE id = (SELECT kindid FROM deleted)


--IF EXISTS(SELECT name FROM sys.triggers WHERE name = 't_ds_doccase_number_update') 
--DROP TRIGGER t_ds_doccase_number_update;
--
Create TRIGGER [t_ds_doccase_number_update] 
    ON [ds_doccase]
    AFTER INSERT
AS
BEGIN
	
	DECLARE @numberSet as Int
	DECLARE @year as Int
	SET @year = datename(YYYY,getDate())
	
	SET @numberSet = (SElECT MAX(d.v)+1 FROM (
                                    select SUBSTRING(number,1,LEN(number)-5) as v 
                                    from ds_doccase 
                                    where number LIKE '%' + CAST(@year as Varchar)) 
                            as d) 
	
    UPDATE ds_doccase set ds_doccase.number = CAST(ISNULL(@numberSet, 100) as Varchar) +'/'+CAST(@year as Varchar)
    FROM INSERTED
    where INSERTED.id=ds_doccase.id

END