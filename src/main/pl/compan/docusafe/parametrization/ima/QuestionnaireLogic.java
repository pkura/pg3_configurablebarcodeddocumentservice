package pl.compan.docusafe.parametrization.ima;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.ima.dictionary.Doccase;
import pl.compan.docusafe.core.ima.dictionary.Mediator;
import pl.compan.docusafe.core.ima.dictionary.MediatorNotes;
import pl.compan.docusafe.core.ima.dictionary.Questionnaire;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/*
 * Automatyczna aktualizacja statystyk danego Mediatora na podstawie wszystkich statystyk czastkowych
 * 
 * Mediator jest oceniany w kontekscie ankiety, stad jego ocena jest w tabeli:
 * ds_ima_questionnaire - ankieta glowna
 * ds_ima_questionnaire_questions - pytania ankiety
 * 
 * Sprawa jest zapisana w tabeli 
 * ds_doccase
 * 
 * Mediator:
 * ds_ima_mediator
 * 
 * Wyniki mediatora w kotnekscie sprawy:
 * v_ds_ima_questionnaire_result
 * 
 * Aktualna ocena Mediatora
 * v_ds_ima_mediator_notes
 */
public class QuestionnaireLogic extends AbstractDocumentLogic {

	
	 private static QuestionnaireLogic instance;
	    protected static Logger log = LoggerFactory.getLogger(QuestionnaireLogic.class);

	    public static QuestionnaireLogic getInstance() {
	        if (instance == null)
	            instance = new QuestionnaireLogic();
	        return instance;
	    }
	
	    
	    public void documentPermissions(Document document) throws EdmException {
	        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
	        perms.add(new PermissionBean(ObjectPermission.READ, "QUESTIONNAIRE_READ", ObjectPermission.GROUP, "Ocena ankiety - odczyt"));
	        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "QUESTIONNAIRE_ATT_READ", ObjectPermission.GROUP, "Ocena ankiety za��cznik - odczyt"));
	        perms.add(new PermissionBean(ObjectPermission.MODIFY, "QUESTIONNAIRE_READ_MODIFY", ObjectPermission.GROUP, "Ocena ankiety - modyfikacja"));
	        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "QUESTIONNAIRE_READ_ATT_MODIFY", ObjectPermission.GROUP, "Ocena ankiety za��cznik - modyfikacja"));
	        perms.add(new PermissionBean(ObjectPermission.DELETE, "QUESTIONNAIRE_READ_DELETE", ObjectPermission.GROUP, "Ocena ankiety - usuwanie"));

	        for (PermissionBean perm : perms) {
	            if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP)) {
	                String groupName = perm.getGroupName();
	                if (groupName == null) {
	                    groupName = perm.getSubject();
	                }
	                createOrFind(document, groupName, perm.getSubject());
	            }
	            DSApi.context().grant(document, perm);
	            DSApi.context().session().flush();
	        }
	    }
	    
	
		public void setInitialValues(FieldsManager fm, int type) throws EdmException
	    {
			 log.info("type: {}", type);
			 Map<String,Object> values = new HashMap<String,Object>();
		     values.put("TYPE", type);
		     DSUser user = DSApi.context().getDSUser();

		     fm.reloadValues(values);
		}
		
		
        public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
	    	if (document.getDocumentKind() == null)
	            throw new NullPointerException("document.getDocumentKind()");
	    	
	    	Long iddocument = document.getId();
	    	
	    	// Pobranie idmediatora dla ankiety
	    	Questionnaire questionnaire = Questionnaire.findByDocumentId(iddocument);
	    	
	    	if (questionnaire.getIdmediator()==null)
	    	{
	    		throw new EdmException("You have to choose case.");
	    	}
	    	
	    	Integer idmediator = questionnaire.getIdmediator();
	    	
	    	// Pobranie obiektu mediatora
	    	MediatorNotes notes = MediatorNotes.findByMediatorId(idmediator);
									
	    	// Aktualizacja mediatora o wyniki ankiety
			Mediator mediator = Mediator.find(idmediator);
			
			//MediatorNotes notes = MediatorNotes.find(idmediator);
			mediator.setRq1(notes.getRq1());
			mediator.setRq2(notes.getRq2());
			mediator.setRq3(notes.getRq3());
			mediator.setRq4(notes.getRq4());
			mediator.setRq5(notes.getRq5());
			mediator.setRq6(notes.getRq6());
			
			Integer summary = (notes.getRq1()+notes.getRq2()+notes.getRq3()+notes.getRq4()+notes.getRq5()+notes.getRq6()) / 6;
			
			// Ustawienie wypadkowej z ocen Mediatora
			mediator.setNote(summary);
			
			
			DSApi.context().session().saveOrUpdate(mediator);
	    	
			// Wlozenie oceny ankiety do teczki sprawy powi�zanej

						
			Integer sprawaId = questionnaire.getIddoccase();
			
			Doccase doccase = null;
			
			// Pobieramy dla sprawy document_id
			if (sprawaId!=null && sprawaId>0)
			{
				doccase = Doccase.find(sprawaId);
			}
			else
			{
				throw new EdmException("You have to choose case.");
			}
			
			// Pobranie folderu sprawy
			Long docId = doccase.getDocument_id();
			Document doc = Document.find(docId);		
			Folder folder = doc.getFolder();

			folder = folder.createSubfolderIfNotPresent("Documents");
			
			// Nadanie ankiecie nazwy
			document.setTitle("Evaluation: " + questionnaire.getTitle());
			
			
			document.setFolder(folder);
					
	        
	    }
	    
	    @Override
	    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException	   
	    {
	    	/*// Sprawdzenie, czy ustawiono u�ytkownika systemu
	    	if (values.get("ds_user") == null)
	    	{
	    		throw new EdmException(sm.getString("NieWybranoPracownika"));
	    	}
	    	// Pobranie identyfikatora u�ytkownika	    	
	    	Long iddsuser = Long.parseLong(values.get("ds_user").toString());
	    	
	    	// Sprawdzenie, czy taki mediator nie istnieje ju� w bazie    	 
	    	 
	    	List<ImaMediator> mediators = ImaMediator.findByDSUserId(iddsuser);
	    	if (mediators != null && mediators.size()>0) {
	    		throw new EdmException(sm.getString("MediatorIstniejeWSystemie"));
	    	}*/
	    }
	    
	    @Override
	    public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
	    {	
	    	// Ustawienie idenytfikatora Mediatora
	    	
	    	// Jezeli zapisujemy slownik v_ds_ima_doccase
	    	if (fieldValues.containsKey("V_DS_IMA_DOCCASE"))
	    	{
	    		Integer iddoccase = Integer.parseInt(fieldValues.get("V_DS_IMA_DOCCASE").toString());
	    		
	    		// Pobranie szczegolow rozszerzonej sprawy
	    		StringBuilder query = new StringBuilder("select * from v_ds_ima_doccase where id=").append(iddoccase);
	    		
	    		PreparedStatement ps =  DSApi.context().prepareStatement(query);
	    		
	    		ResultSet rs = ps.executeQuery();
	    		
	    		while (rs.next())
	    		{
	    			try
	    			{
	    				Integer idmediator = rs.getInt("IDMEDIATOR");
	    				
	    				// Aktualizacja ankiety o id mediatora
	    				Questionnaire questionnaire = Questionnaire.findByDocumentId(documentId);
	    				questionnaire.setIdmediator(idmediator);
	    				DSApi.context().session().saveOrUpdate(questionnaire);
	    				    
	    				
	    			} catch (SQLException sExc)
	    			{
	    				throw sExc;
	    			}
	    		}
	    		
	    	}
	    
	    }
	    
	    @Override
		public boolean isPersonalRightsAllowed()
		{
			return true;
		}
	    
}
