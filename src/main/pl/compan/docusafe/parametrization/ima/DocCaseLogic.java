package pl.compan.docusafe.parametrization.ima;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.ValidationException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.ima.dictionary.CaseStatus;
import pl.compan.docusafe.core.ima.dictionary.Doccase;
import pl.compan.docusafe.core.ima.dictionary.Docincase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

public class DocCaseLogic extends AbstractDocumentLogic {

	public static final String CASE_TYPE_CN = "casetype";
	public static final String CASE_STATUS_CN = "casestatus";
        private static final String NEXT_NUMBER_SQL = 
                            "SElECT MAX(d.v)+1 FROM "
                                + "(select SUBSTRING(number,1,LEN(number)-5) as v "
                                + "from ds_doccase where number LIKE '%/' + CAST(datename(YYYY,getDate()) as Varchar) AND LEN(number) > 5) as d";
	private static DocCaseLogic instance;
	protected static Logger log = LoggerFactory.getLogger(DocCaseLogic.class);

	public static DocCaseLogic getInstance() {
		if (instance == null)
			instance = new DocCaseLogic();
		return instance;
	}
	
	private static StringManager sm = StringManager.getManager(DocCaseLogic.class.getPackage().getName());

	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		
		perms.add(new PermissionBean(ObjectPermission.READ, document.getAuthor() , ObjectPermission.USER));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getAuthor() , ObjectPermission.USER));
		perms.add(new PermissionBean(ObjectPermission.DELETE, document.getAuthor() , ObjectPermission.USER));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getAuthor() , ObjectPermission.USER));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getAuthor() , ObjectPermission.USER));
		
		// Nadanie uprawnien administratorowi
		perms.add(new PermissionBean(ObjectPermission.READ, "admin" , ObjectPermission.USER));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "admin" , ObjectPermission.USER));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "admin" , ObjectPermission.USER));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "admin" , ObjectPermission.USER));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "admin" , ObjectPermission.USER));
	
		
		
		for (PermissionBean perm : perms) {			
			DSApi.context().grant(document, perm);			
		}
	}
	
	/* (non-Javadoc)
	 * @see pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic#onEndProcess(pl.compan.docusafe.core.office.OfficeDocument, java.lang.Boolean)
	 */
	@Override
    public void onEndProcess(OfficeDocument document, boolean isManual)
			throws EdmException {
		// Zmieniamy status dokumentu na Zamkni�ty
		
		Doccase doccase = Doccase.findByDocumentId(document.getId());
		if (doccase!=null)
		{
			doccase.setCasestatus("5");
			DSApi.context().session().update(doccase);
			
		}
		
	}
	
	@Override
	public void validate(Map<String, Object> values, DocumentKind kind,
			Long documentId) throws EdmException {
	
			boolean result = true;

			if (values.get("REQUIREDFINISHDATE") != null) {
				Date deadline_time = (Date) values.get("REQUIREDFINISHDATE");

				Date data_rejestracji = new Date();

				if (documentId != null) {
					// Aktualizacja dokumentu
					Document doc = Document.find(documentId);
					data_rejestracji = doc.getCtime();
				}

				int compare_result = data_rejestracji.compareTo(deadline_time);

				if (compare_result > 0) {
					result = false;
					throw new ValidationException(
							sm.getString("WymaganaDataZakonczeniaNieMozeBycWczesniejszaNizDataRejestracjiDokumentu"));
				}
		}
	}
	
	
	  @Override
		public boolean canFinishProcess(OfficeDocument document) throws AccessDeniedException, EdmException
	    {
		  boolean hasTasks = false;
			
			// Pobranie wszystkich dokumentow powiazanych z dokumentem Sprawy
			List<Docincase> docs = Docincase.findByRelatedDocumentId(document.getId().toString());
			
			// Dla kazdedo pobranego dokumentu sprawdzamy, czy nie ma w tabeli taskow z nim zwiazanych
			for(Docincase docincase : docs)
			{
				Long docincase_id = docincase.getDocument_id();
				
				List<JBPMTaskSnapshot> tasks = JBPMTaskSnapshot.findByDocumentId(docincase_id);
				
				if (tasks!=null && tasks.size()>0) 
				{
					hasTasks = true;
					break;
				}
			}
			
			if (hasTasks)
			{
//				throw new EdmException(sm.getString("NieZamknieteDokumentyWSprawie"));
				return false;
			}
			else
				return true;
	    }
	

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) {
		log.info("--- DocCaseLogic : START PROCESS !!! ---- {}", event);
		try {
			// Pobranie pol dokumentu

			Map<String, Object> map = Maps.newHashMap();
			FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
                        
            //String numberCo = getNextNumber();
                        

			// map.put(ASSIGN_USER_PARAM,
			// event.getAttribute(ASSIGN_USER_PARAM));
            // map.put("NUMBER", numberCo);
			putIfNotNull(map, "DSUSER", fm);
			putIfNotNull(map, "DOCDESCRIPTION", fm);
				if (document.getAuthor() != null)
				map.put("DSAUTHOR", document.getAuthor());
			if (event.getAttribute(ASSIGN_USER_PARAM) != null)
				map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			if (event.getAttribute(ASSIGN_DIVISION_GUID_PARAM) != null)
				map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));

			// Deadline
			if (fm.getValue("REQUIREDFINISHDATE") != null) {
				map.put("DEADLINE_TIME", fm.getValue("REQUIREDFINISHDATE"));
			}			
			
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

			// Rejestrujemy zdarzenie przypominajace o terminie zamkniecia
			// zadania
			
			// Deadline
			if (fm.getValue("REQUIREDFINISHDATE") != null) {
				map.put(Jbpm4WorkflowFactory.DEADLINE_TIME, fm
						.getValue("REQUIREDFINISHDATE"));
			}
			
			// Pobieramy date zadana
			Date deadline_date = (Date) fm.getValue("REQUIREDFINISHDATE");
			
			// Pobieramy maile osoby przypisujacej
			String author = document.getAuthor();

			
			// Rejestrujemy zdarzenie przypominajace

			EventFactory.registerEvent("immediateTrigger", "TaskEventNotifier",
					author + "|" + deadline_date.toString(), null,
					deadline_date);
			

		} catch (Exception e) {
			log.error(e.getMessage(), e);

		}
	}
	
	private void putIfNotNull(Map<String, Object> map, String field,
			FieldsManager fm) throws EdmException {

		if (fm.getValue(field) != null) {
			map.put(field, fm.getValue(field).toString());
		}
	}

	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document)
			throws EdmException {
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	public void setInitialValues(FieldsManager fm, int type)
			throws EdmException {
		Map<String, Object> values = new HashMap<String, Object>();
		// Ustawienie daty sprawy na dzisiejsza
		values.put("STARTDATE", new Date());
		// Ustawienie automatycznie statusu sprawy na Nowy
		CaseStatus cs = CaseStatus.findByCnAndLanguage("new", "pl");
		
		if (cs==null)
		{
			throw new EdmException("W s�owniku brakuje pozycji o nazwie \"NEW\"");
		}
		
		values.put("CASESTATUS",cs.getId().toString());		
		
		fm.reloadValues(values);
	}

	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {
		if (document.getDocumentKind() == null)
			throw new NullPointerException("document.getDocumentKind()");

		
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		
		// Przy aktualizacji Sprawy aktualizujemy dane na li�cie zada�
		
		// Status - 1
		String caseStatus = "";
		if (fm.getValue("CASESTATUS")!=null) {
			 caseStatus = fm.getValue("CASESTATUS").toString();
		}	
		
		// Tytul - 2
		String title = "";
		if (fm.getValue("TITLE")!=null) {
			title = fm.getValue("TITLE").toString();
		}
		
		// Numer - 0
		String number = "";
		if (fm.getValue("NUMBER")!=null) {
			 number = fm.getValue("NUMBER").toString();
		}	
		
		// Wyszukanie taskow dla danego dokumentu
		List<JBPMTaskSnapshot> tasks = JBPMTaskSnapshot.findByAttribute5(document.getId().toString());
		
		for (JBPMTaskSnapshot task : tasks)
		{
			task.setDockindBusinessAtr1(number);
			task.setDockindBusinessAtr2(caseStatus);
			task.setDockindBusinessAtr3(title);		
			DSApi.context().session().update(task);
		}
	}

	@Override
	public void onSaveActions(DocumentKind kind, Long documentId,
			Map<String, ?> fieldValues) throws SQLException, EdmException {
		// Po utworzeniu dokument jest umieszczony w strukturze:
		// Sprawa -> Status sprawy -> Typ sprawy
		// Wkladamy karte do folerdu Karty mediatorow

		// Tworzymy strukture, jezel jeszcze nie istnieje
		Folder rootFolder = Folder.getRootFolder();
		Folder sprawyFolder = rootFolder.createSubfolderIfNotPresent("Cases");

		// Folder fWToku = folder.createSubfolderIfNotPresent("W toku");
		// Folder fZamkniete = folder.createSubfolderIfNotPresent("Zamkni�te");

		// Je�eli Sprawa przy utworzeniu ma od razu status zamknieta, to nie
		// uruchamiamy procesu
		FieldsManager fm = kind.getFieldsManager(documentId);
		String nazwaSprawy = fm.getValue("TITLE").toString();
		Folder tytulSprawyFolder = sprawyFolder.createSubfolderIfNotPresent(nazwaSprawy, true);
		// Folder tytulSprawyFolder =
		// sprawyFolder.createSubfolderIfNotPresent(nazwaSprawy);

		// Pierwsze zapisanie dokumentu, umieszczamy go w strukturze
		Document doc = Document.find(documentId);
		doc.setFolder(tytulSprawyFolder);
		doc.setTitle(fieldValues.get("TITLE").toString());
		//kind.logic().archiveActions(doc, DocumentLogic.TYPE_ARCHIVE);
				
	}
	
	@Override
	public boolean isPersonalRightsAllowed()
	{
		return true;
	}
	
	@Override
	public TaskListParams getTaskListParams(DocumentKind kind, long documentId) throws EdmException{
		TaskListParams params = new TaskListParams();
    	try
    	{
    		FieldsManager fm = kind.getFieldsManager(documentId);
    		
    		
    		//Numer dokumentu - numer sprawy
			if (fm.getValue("NUMBER")!=null) {
				params.setAttribute(fm.getValue("NUMBER").toString(), 0);
			}
			
			// Status dokumentu - status sprawy
			if (fm.getValue("CASESTATUS")!=null) {
				params.setAttribute(fm.getValue("CASESTATUS").toString(), 1);
			}				
			
			// Tytul sprawy
			if (fm.getValue("TITLE")!=null) {
				params.setAttribute(fm.getValue("TITLE").toString(),2 );
			}
			
			// Tytul sprawy
			params.setAttribute("-", 3);
									
			
			// ID Sprawy
			Long document_id = new Long(documentId);
			params.setAttribute(document_id.toString(), 4);
			
			    			
    	}
    	catch (Exception e) 
    	{
			e.printStackTrace();
		}
		return params;
	}
        
        /**
         * Zwraca nast�pny numer dla dokumentu w kolejno�ci do bazyw kolumnie number:
         * format tego jest "(aktualny_numer+1)/rok"
         * @return
         * @throws EdmException 
         */
        private String getNextNumber() throws EdmException
        {
            try
            {
                //pobieram nastepny numer z tego roku;
                PreparedStatement ps = DSApi.context().prepareStatement(NEXT_NUMBER_SQL); 
                ResultSet rs = ps.executeQuery();
                Integer numberNext = null;
                
                while(rs.next())
                {
                    numberNext = rs.getInt(1);
                }
                
                if(numberNext != null)
                    return numberNext+1 + "/" + DateUtils.getCurrentYear();
                else
                    return "100/" + DateUtils.getCurrentYear(); //domyslny jesli brak
                
            } catch (SQLException ex)
            {
                log.error("",ex);
                throw new EdmException("B��d ustalania nast�pnego numeru CO.");
            }
        }
}