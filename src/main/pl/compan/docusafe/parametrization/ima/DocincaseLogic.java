package pl.compan.docusafe.parametrization.ima;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.PermissionFactory;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.ValidationException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.calendar.Calendar;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.ima.dictionary.CaseStatus;
import pl.compan.docusafe.core.ima.dictionary.Doccase;
import pl.compan.docusafe.core.ima.dictionary.DoccaseDictionary;
import pl.compan.docusafe.core.ima.dictionary.Docincase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

public class DocincaseLogic extends AbstractDocumentLogic {

	/*
	 * private String userName; private String commandDescription;
	 */
	private static DocincaseLogic instance;
	protected static Logger log = LoggerFactory.getLogger(DocincaseLogic.class);

	private static final StringManager sm = StringManager
			.getManager(DocincaseLogic.class.getPackage().getName());

	public static DocincaseLogic getInstance() {
		if (instance == null)
			instance = new DocincaseLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

		for (PermissionBean perm : perms) {
			if (perm.isCreateGroup()
					&& perm.getSubjectType().equalsIgnoreCase(
							ObjectPermission.GROUP)) {
				String groupName = perm.getGroupName();
				if (groupName == null) {
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {
		if (document.getDocumentKind() == null)
			throw new NullPointerException("document.getDocumentKind()");

		// Wlozenie dokumentu do teczki sprawy powiązanej

		// Pobranie folderu dowiązanej sprawy

		FieldsManager fm = document.getDocumentKind().getFieldsManager(
				document.getId());

		String sprawaId = fm.getFieldValues().get("DS_DOCCASE").toString();

		// Numer jest podany w nawiasach kwadratowych []
		sprawaId = sprawaId.replace("[", "");
		sprawaId = sprawaId.replace("]", "");

		// Pobranie folderu sprawy
		Long docId = Long.parseLong(sprawaId);
		Document doc = Document.find(docId);
		Folder folder = doc.getFolder();

		folder = folder.createSubfolderIfNotPresent("Documents");

		document.setFolder(folder);
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) {
		try {
			// Pobranie pol dokumentu

			Map<String, Object> map = Maps.newHashMap();
			FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

			// Przekazanie parametrow do logiki procesu
			// Przypisany uzytkownik
			if (event.getAttribute(ASSIGN_USER_PARAM) != null)
				map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));

			// Organizacja
			if (event.getAttribute(ASSIGN_DIVISION_GUID_PARAM) != null)
				map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));

			// Deadline
			if (fm.getValue("DEADLINE_TIME") != null) {
				map.put("DEADLINE_TIME", fm.getValue("DEADLINE_TIME"));
			}

			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

			// Rejestrujemy zdarzenie przypominajace o terminie zamkniecia
			// zadania

			// Pobieramy date zadana
			Date deadline_time = (Date) fm.getValue("DEADLINE_TIME");

			// Opis
			if (fm.getValue("NAME")!=null){
				map.put("DOCDESCRIPTION", fm.getValue("NAME"));
			}
			
			// Pobieramy maile osoby przypisujacej
			String author = document.getAuthor();

			// Pobieramy mail osoby przypisanej
			String assigner = fm.getValue("DSUSER").toString();
			DSUser assigner_user = DSUser.findByFirstnameLastname(
					getFirstName(assigner), getLastName(assigner), false);
			String assigner_name = assigner_user.getName();

			// Rejestrujemy zdarzenie przypominajace

			String df = DateFormat.getDateTimeInstance().format(deadline_time);
			
			
			EventFactory.registerEvent("immediateTrigger", "TaskEventNotifier",
					assigner_name + "|" + df + "|" + document.getId().toString(), null,
					deadline_time, document.getId());

		} catch (Exception e) {
			log.error(e.getMessage(), e);

		}
	}

	@Override
	public void onEndProcess(OfficeDocument document, boolean isManual)
			throws EdmException {
		System.out.println("Wywołano: onEndProcess");
		{
			// Po zamknieciu procesu zmieniamy status dokumentu na archived id=4

			// Pobranie ID slownika status - ARCHIVED
			Docincase docincase = Docincase.findByDocumentId(document.getId());
			docincase.setDocstatus("16");
			DSApi.context().session().update(docincase);

			// Zamkniecie wszystkich zdarzen zwiazanych z dokumentem
			EventFactory.closeEventsByDocumentId(document.getId());
		}

	}

	private String getFirstName(String name) {
		String[] name_array = name.split(" ");
		return name_array[1];
	}

	private String getLastName(String name) {
		String[] name_array = name.split(" ");
		return name_array[0];
	}

	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document)
			throws EdmException {
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	@Override
	public void validate(Map<String, Object> values, DocumentKind kind,
			Long documentId) throws EdmException {

		// Marker - sprawdza, czy jest to stan, w ktorym mapa nie jest
		// wypelniana
		if (values.get("DEADLINE_TIME") != null) {

			boolean result = true;

			if (values.get("DEADLINE_TIME") != null) {
				Date deadline_time = (Date) values.get("DEADLINE_TIME");

				Date data_rejestracji = new Date();

				if (documentId != null) {
					// Aktualizacja dokumentu
					Document doc = Document.find(documentId);
					data_rejestracji = doc.getCtime();
				}

				int compare_result = data_rejestracji.compareTo(deadline_time);

				if (compare_result > 0) {
					result = false;
					throw new ValidationException(
							sm.getString("WymaganaDataZakonczeniaNieMozeBycWczesniejszaNizDataRejestracjiDokumentu"));
				}
			}

			// Data deadline_time nie moze byc wczesniejsza niz data rejestracji

			// Sprawdzenie, czy wybrano sprawe

			if (values.get("DS_DOCCASE") != null) {
				String doccase_string = values.get("DS_DOCCASE").toString();
				try {
					Long doccase_id = Long.parseLong(doccase_string);
				} catch (NumberFormatException nfe) {
					result = false;
				}
			} else {
				result = false;
			}
			if (!result) {
				throw new ValidationException(sm
						.getString("NalezyWskazacSpraweDoKtorejDokumentNalezy"));
			}
		}
	}

	

	@Override
	public void onSaveActions(DocumentKind kind, Long documentId,
			Map<String, ?> fieldValues) throws SQLException, EdmException {

		// Pobranie id sprawy
		Long doccase_id =Long.parseLong(fieldValues.get("DS_DOCCASE").toString());

		// Pobranie uprawnien sprawy
		Set<PermissionBean> permissions = PermissionFactory.getInstance()
				.GetDocumentPermission(doccase_id);

		// Ustawienie uprawnien na nowy dokument
		this.setUpPermission(Document.find(documentId), permissions);

		// Ustawienie tytulu dokumentu
		Document doc = Document.find(documentId);
		doc.setTitle(fieldValues.get("NAME").toString());
		kind.logic().archiveActions(doc, DocumentLogic.TYPE_ARCHIVE);

		// Dodanie do Sprawy powiazania z tworzonym dokumentem
		// Podczas tworzenia dokumentu wskazujemy Sprawe, do ktorej nalezy.
		DoccaseDictionary doccaseDictionary = DoccaseDictionary
				.findByCaseAndDocincase(doccase_id, documentId);

		if (doccaseDictionary == null) {
			// Nie ma jeszcze powiazania, wiec je robimy
			String dId = documentId.toString();
			doccaseDictionary = new DoccaseDictionary("WNIOSEK", doccase_id, dId);
			doccaseDictionary.create();
		}

	}

	@Override
	public boolean isPersonalRightsAllowed() {
		return true;
	}
	
	@Override
	public TaskListParams getTaskListParams(DocumentKind kind, long documentId) throws EdmException{
		TaskListParams params = new TaskListParams();
    	try
    	{
    		FieldsManager fm = kind.getFieldsManager(documentId);
    		
    		String ids = fm.getValue("DS_DOCCASE").toString();
			Long idDoccase = Long.parseLong(ids);
			Doccase doccase = Doccase.findByDocumentId(idDoccase);
    		
    		//Numer dokumentu - numer sprawy
			if (doccase.getNumber()!=null) {
				params.setAttribute(doccase.getNumber(), 0);
			}
			
			// Status dokumentu - status sprawy
			if (doccase.getCasestatus() != null) {
				Integer idcasestatus = Integer.parseInt(doccase.getCasestatus());
				CaseStatus cs = CaseStatus.find(idcasestatus);
				cs = CaseStatus.findByCnAndLanguage(cs.getCn(), Docusafe.getCurrentLanguageLocale().getLanguage());
				params.setAttribute(cs.getTitle(), 1);
			}
			
			// Tytul sprawy
			if (doccase.getTitle()!=null)
			{
				params.setAttribute(doccase.getTitle(), 2);
			}
			
			// Nazwa tasku
			if (fm.getValue("NAME") != null) {
					params.setAttribute(fm.getValue("NAME").toString(),3);
			}
			
			// ID Sprawy
			if (doccase.getId()!=null){
				params.setAttribute(ids, 4);
			}
			    			
    	}
    	catch (Exception e) 
    	{
			e.printStackTrace();
		}
		return params;
	}

}
