package pl.compan.docusafe.parametrization.ima;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.Maps;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class DoctemplateLogic extends AbstractDocumentLogic {

    private static DoctemplateLogic instance;
    protected static Logger log = LoggerFactory.getLogger(DoctemplateLogic.class);

    public static DoctemplateLogic getInstance() {
        if (instance == null)
            instance = new DoctemplateLogic();
        return instance;
    }

    public void documentPermissions(Document document) throws EdmException {
        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
        perms.add(new PermissionBean(ObjectPermission.READ, "DOCTEMPLATE_READ", ObjectPermission.GROUP, "Szablon dokumentu - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCTEMPLATE_READ_ATT", ObjectPermission.GROUP, "Za�acznik szablonu dokumentu - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCTEMPLATE_READ_MODIFY", ObjectPermission.GROUP, "Szablon dokumentu - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCTEMPLATE_READ_ATT_MODIFY", ObjectPermission.GROUP, "Za��cznik szablonu dokumentu - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCTEMPLATE_READ_DELETE", ObjectPermission.GROUP, "Szablon dokumentu - usuwanie"));

        for (PermissionBean perm : perms) {
            if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP)) {
                String groupName = perm.getGroupName();
                if (groupName == null) {
                    groupName = perm.getSubject();
                }
                createOrFind(document, groupName, perm.getSubject());
            }
            DSApi.context().grant(document, perm);
            DSApi.context().session().flush();
        }
    }


    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) {
        log.info("--- DoctemplateLogic : START PROCESS !!! ---- {}", event);
        try {
            Map<String, Object> map = Maps.newHashMap();
            //map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
            map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
            map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException {
        ProcessCoordinator ret = new ProcessCoordinator();
        ret.setUsername(document.getAuthor());
        return ret;
    }

    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        log.info("type: {}", type);
        Map<String, Object> values = new HashMap<String, Object>();
        log.error("Ustawienie warto�ci REQUIRED_FINISH_DATE_CN");
        values.put("REQUIRED_FINISH_DATE_CN", new Date());
        DSUser user = DSApi.context().getDSUser();

        fm.reloadValues(values);
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");

       
    }
    
    @Override
	public boolean isPersonalRightsAllowed()
	{
		return true;
	}
    
    @Override
    public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
    {
    	// Wkladamy karte do folerdu Karty mediatorow
		
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Templates");
		Document doc = Document.find(documentId);
		doc.setFolder(folder);
		doc.setTitle("Template - " + fieldValues.get("title").toString());
		kind.logic().archiveActions(doc, DocumentLogic.TYPE_ARCHIVE);
    }
   }