package pl.compan.docusafe.parametrization.ima;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.PermissionFactory;
import pl.compan.docusafe.core.ValidationException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.ima.dictionary.CaseStatus;
import pl.compan.docusafe.core.ima.dictionary.Doccase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

public class TaskLogic extends AbstractDocumentLogic {

	private static TaskLogic instance;
	protected static Logger log = LoggerFactory.getLogger(TaskLogic.class);

	private static final StringManager sm = StringManager
			.getManager(TaskLogic.class.getPackage().getName());

	public static TaskLogic getInstance() {
		if (instance == null)
			instance = new TaskLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

		for (PermissionBean perm : perms) {
			if (perm.isCreateGroup()
					&& perm.getSubjectType().equalsIgnoreCase(
							ObjectPermission.GROUP)) {
				String groupName = perm.getGroupName();
				if (groupName == null) {
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {
		/*if (document.getDocumentKind() == null)
			throw new NullPointerException("document.getDocumentKind()");*/

	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) {
		try {
			// Pobranie pol dokumentu

			Map<String, Object> map = Maps.newHashMap();
			FieldsManager fm = document.getDocumentKind().getFieldsManager(
					document.getId());

			// Przekazanie parametrow do logiki procesu
			// Przypisany uzytkownik
			if (event.getAttribute(ASSIGN_USER_PARAM) != null)
				map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));

			// Organizacja
			if (event.getAttribute(ASSIGN_DIVISION_GUID_PARAM) != null)
				map.put(ASSIGN_DIVISION_GUID_PARAM, event
						.getAttribute(ASSIGN_DIVISION_GUID_PARAM));

			// Deadline
			if (fm.getValue("DEADLINE_TIME") != null) {
				map.put("DEADLINE_TIME", fm.getValue("DEADLINE_TIME"));
			}			
		
			document.getDocumentKind().getDockindInfo()
					.getProcessesDeclarations().onStart(document, map);

			// Rejestrujemy zdarzenie przypominajace o terminie zamkniecia
			// zadania

			// Pobieramy date zadana
			Date deadline_time = (Date) fm.getValue("DEADLINE_TIME");
	
			// Pobieramy mail osoby przypisanej
			String assigner = fm.getValue("DSUSER").toString();
			DSUser assigner_user = DSUser.findByFirstnameLastname(
					getFirstName(assigner), getLastName(assigner), false);
			String assigner_name = assigner_user.getName();

			// Rejestrujemy zdarzenie przypominajace

			String df = DateFormat.getDateTimeInstance().format(deadline_time);

			EventFactory.registerEvent("immediateTrigger", "TaskEventNotifier",
					assigner_name + "|" + df + "|" + document.getId().toString(),
					null, deadline_time, document.getId());

		} catch (Exception e) {
			log.error(e.getMessage(), e);

		}
	}

	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		return null;
	}

	@Override
	public void onEndProcess(OfficeDocument document, boolean isManual)
			throws EdmException {
		{
			// Zamkniecie wszystkich zdarzen zwiazanych z dokumentem
			EventFactory.closeEventsByDocumentId(document.getId());
		}

	}

	private String getFirstName(String name) {
		String[] name_array = name.split(" ");
		return name_array[1];
	}

	private String getLastName(String name) {
		String[] name_array = name.split(" ");
		return name_array[0];
	}

	/*
	 * @Override public ProcessCoordinator getProcessCoordinator(OfficeDocument
	 * document) throws EdmException { ProcessCoordinator ret = new
	 * ProcessCoordinator(); ret.setUsername(document.getAuthor()); return ret;
	 * }
	 */

	@Override
	public void validate(Map<String, Object> values, DocumentKind kind,
			Long documentId) throws EdmException {

		// Marker - sprawdza, czy jest to stan, w ktorym mapa nie jest
		// wypelniana
		if (values.get("DEADLINE_TIME") != null) {

			boolean result = true;

			if (values.get("DEADLINE_TIME") != null) {
				Date deadline_time = (Date) values.get("DEADLINE_TIME");

				Date data_rejestracji = new Date();

				if (documentId != null) {
					// Aktualizacja dokumentu
					Document doc = Document.find(documentId);
					data_rejestracji = doc.getCtime();
				}

				int compare_result = data_rejestracji.compareTo(deadline_time);

				if (compare_result > 0) {
					result = false;
					throw new ValidationException(
							sm
									.getString("WymaganaDataZakonczeniaNieMozeBycWczesniejszaNizDataRejestracjiDokumentu"));
				}
			}

			// Data deadline_time nie moze byc wczesniejsza niz data rejestracji

			result = true;

			// Sprawdzenie, czy wybrano sprawe

			if (values.get("DS_DOCCASE") != null) {
				String doccase_string = values.get("DS_DOCCASE").toString();
				try {
					Long doccase_id = Long.parseLong(doccase_string);
				} catch (NumberFormatException nfe) {
					result = false;
				}
			} else {
				result = false;
			}
			if (!result) {
				throw new ValidationException(sm
						.getString("NalezyWskazacSpraweDoKtorejDokumentNalezy"));
			}
		}
	}

	@Override
	public void onSaveActions(DocumentKind kind, Long documentId,
			Map<String, ?> fieldValues) throws SQLException, EdmException {

		// Wlozenie dokumentu do teczki sprawy powiązanej

		// Pobranie folderu dowiazanej sprawy

			if (fieldValues.get("DS_DOCCASE")!=null){
			String docIdString = fieldValues.get("DS_DOCCASE").toString();
			Long docId = Long.parseLong(docIdString);
			Document doc = Document.find(docId);
			Folder folder = doc.getFolder();
	
			folder = folder.createSubfolderIfNotPresent("Documents");
	
			// Wczytanie aktualnego Tasku i przypisanie do folderu
			Document document = Document.find(documentId);
			document.setFolder(folder);
			
			// Ustawienie tytulu dokumentu
			if (fieldValues.get("TITLE")!=null){
				document.setTitle(fieldValues.get("TITLE").toString());
			}
		}	
	}

	@Override
	public boolean isPersonalRightsAllowed() {
		return true;
	}
	
	@Override
	public TaskListParams getTaskListParams(DocumentKind kind, long documentId) throws EdmException{
		TaskListParams params = new TaskListParams();
    	try
    	{
    		FieldsManager fm = kind.getFieldsManager(documentId);
    		
    		String ids = fm.getValue("DS_DOCCASE").toString();
			Long idDoccase = Long.parseLong(ids);
			Doccase doccase = Doccase.findByDocumentId(idDoccase);
    		
    		//Numer dokumentu - numer sprawy
			if (doccase.getNumber()!=null) {
				params.setAttribute(doccase.getNumber(), 0);
			}
			
			// Status dokumentu - status sprawy
			if (doccase.getCasestatus() != null) {
				Integer idcasestatus = Integer.parseInt(doccase.getCasestatus());
				CaseStatus cs = CaseStatus.find(idcasestatus);
				cs = CaseStatus.findByCnAndLanguage(cs.getCn(), Docusafe.getCurrentLanguageLocale().getLanguage());
				params.setAttribute(cs.getTitle(), 1);
			}
			
			// Tytul sprawy
			if (doccase.getTitle()!=null)
			{
				params.setAttribute(doccase.getTitle(), 2);
			}
			
			// Nazwa tasku
			if (fm.getValue("TITLE") != null) {
					params.setAttribute(fm.getValue("TITLE").toString(),3);
			}
			
			// ID Sprawy
			if (doccase.getId()!=null){
				params.setAttribute(ids, 4);
			}
			    			
    	}
    	catch (Exception e) 
    	{
			e.printStackTrace();
		}
		return params;
	}

}
