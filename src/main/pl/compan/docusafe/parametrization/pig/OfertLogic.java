package pl.compan.docusafe.parametrization.pig;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import com.google.common.collect.Maps;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.ocr.OcrSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class OfertLogic extends AbstractDocumentLogic
{

	private static OfertLogic instance;
	protected static Logger log = LoggerFactory.getLogger(OfertLogic.class);

	/**
	 * Akcpetacje
	 */
	public final static String COMMISION_DECISION = "commision-decision";
	public final static String PREPARATION = "PREPARATION";

	public static OfertLogic getInstance()
	{
		if (instance == null)
			instance = new OfertLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		Map<String, Object> values = new HashMap<String, Object>();

		if (fm.getEnumItem("STATUS") == null)
		{
			for (DSDivision division : DSApi.context().getDSUser().getDivisionsWithoutGroup())
			{
				if (division.getGuid().equals("dpizu-division"))
				{
					values.put("STATUS", 10);
					fm.getField("TYP").setHidden(false);
					break;
				}
				else
				{
					values.put("STATUS", 10);
					values.put("TYP", 1);
					fm.getField("TYP").setHidden(true);
				}
			}
		}

		if (fm.getEnumItem("AUTHOR") == null && DSApi.context().getDSUser().getDivisionsWithoutGroup().length > 0)
			values.put("AUTHOR", DSApi.context().getDSUser().getDivisionsWithoutGroup()[0].getId());

		if (fm.getEnumItem("STATUS") != null && fm.getEnumItem("STATUS").getId() == 30 && fm.getEnumItem("PREPARATION") == null)
			values.put("PREPARATION", fm.getEnumItem("AUTHOR").getId());

		for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
		{
			if (f.getDefaultValue() != null)
			{
				values.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
			}
		}

		fm.reloadValues(values);
	}

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

	@Override
	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			FieldsManager fm = dockind.getFieldsManager(id);
			params.setStatus((String) fm.getValue("STATUS"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return params;
	}

	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn)
	{
		boolean assigned = false;
		try
		{
			Map<String, Object> values = new HashMap<String, Object>();
			String userNAme = AcceptanceCondition.find("vice-director-decision", "7F0001010180808F130501EE1FF1E1BFD9C").get(0).getUsername();

			values.put("KOM_PRZEWODNICZACY", DSUser.findByUsername(userNAme).getId());
			doc.getDocumentKind().setOnly(doc.getId(), values);

			if (accpetionCn.equals(COMMISION_DECISION))
			{
				String przewodniczacy = doc.getFieldsManager().getEnumItem("KOM_PRZEWODNICZACY").getCn();
				assignable.addCandidateUser(przewodniczacy);

				List<String> commision = new ArrayList<String>();

				String sekretarz = doc.getFieldsManager().getEnumItem("KOM_SEKRETARZ").getCn();
				commision.add(sekretarz);

				String kom1 = doc.getFieldsManager().getEnumItem("KOM_1").getCn();
				commision.add(kom1);

				if (doc.getFieldsManager().getEnumItem("KOM_2") != null)
				{
					commision.add(doc.getFieldsManager().getEnumItem("KOM_2").getCn());
				}
				if (doc.getFieldsManager().getEnumItem("KOM_3") != null)
				{
					commision.add(doc.getFieldsManager().getEnumItem("KOM_3").getCn());
				}
				if (doc.getFieldsManager().getEnumItem("KOM_4") != null)
				{
					commision.add(doc.getFieldsManager().getEnumItem("KOM_4").getCn());
				}

				for (String cn : commision)
				{
					doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(doc, Collections.<String, Object> singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY, cn));
				}
				assigned = true;
			}
			else if (accpetionCn.equals(PREPARATION))
			{
				String preparation = doc.getFieldsManager().getEnumItem("PREPARATION") != null ? doc.getFieldsManager().getEnumItem("PREPARATION").getCn() : null;
				if (preparation == null)
				{
					assignable.addCandidateUser(doc.getAuthor());
					assigned = true;
				}
				else
				{
					assignable.addCandidateGroup(preparation);
					assigned = true;
					EnumItem ei = doc.getFieldsManager().getEnumItem("PREPARATION");
					doc.getDocumentKind().setOnly(doc.getId(), Collections.singletonMap("AUTHOR", ei.getCn()));
				}
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage());
		}
		return assigned;
	}

	public void validate(Map<String, Object> values, DocumentKind kind) throws EdmException
	{

		if (values.containsKey("WNIOSEK") && ((List) values.get("WNIOSEK")).size() > 0)
		{
			List<Long> ids = (List<Long>) values.get("WNIOSEK");
			try
			{
				PigParametrization.checkDocuments(ids);
				NextTask.finishTask(ids);
			}
			catch (EdmException e)
			{
				throw e;
			}
		}
	}

	@Override
	public OcrSupport getOcrSupport()
	{
		return PigOcrSupport.get();
	}
}
