package pl.compan.docusafe.parametrization.pig;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ApplicationWGLLogic {
	private static ApplicationWGLLogic instance;
	protected static Logger log = LoggerFactory.getLogger(OfertLogic.class);


	public static ApplicationWGLLogic getInstance()
	{
		if (instance == null)
			instance = new ApplicationWGLLogic();
		return instance;
	}

}