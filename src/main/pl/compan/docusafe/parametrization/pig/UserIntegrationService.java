package pl.compan.docusafe.parametrization.pig;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.DuplicateNameException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.Profile;
import pl.compan.docusafe.core.users.ProfileNotFoundException;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

public class UserIntegrationService extends ServiceDriver implements Service
{
	public static final Logger log = LoggerFactory.getLogger(UserIntegrationService.class);
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(UserIntegrationService.class.getPackage().getName(), null);
	private Property[] properties;
	private Timer timer;
	private Integer periodWww = 10;
	private static Profile profile; 

	class ImportTimesProperty extends Property
	{
		public ImportTimesProperty()
		{
			super(SIMPLE, PERSISTENT, UserIntegrationService.this, "periodWww", "Co ile godzin", Integer.class);
		}

		protected Object getValueSpi()
		{
			return periodWww;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (UserIntegrationService.this)
			{
				if (object != null)
					periodWww = (Integer) object;
				else
					periodWww = 10;
			}
		}
	}



	public UserIntegrationService()
	{
		properties = new Property[] { new ImportTimesProperty() };
	}

	@Override
	protected void start() throws ServiceException
	{
		log.info("Start uslugi UsersIntegrationService");
		timer = new Timer(true);
		timer.schedule(new Import(), 0, periodWww * DateUtils.HOUR);
		console(Console.INFO, sm.getString("System UsersIntegrationService wystartowal"));
	}

	@Override
	protected void stop() throws ServiceException
	{
		log.info("Stop uslugi UsersIntegrationService");
		console(Console.INFO, sm.getString("System UsersIntegrationService zako�czy� dzia�anie"));
		try
		{
			DSApi.close();
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

	class Import extends TimerTask
	{

        public static final String PIG_IMPORTED = "PIG IMPORTED";

        @Override
		public void run()
		{
			PreparedStatement ps = null;
			ResultSet rs = null;
			try
			{
				DSApi.openAdmin();
				profile = Profile.findByProfilename("Podstawowy");
				ps = DSApi.context().prepareStatement("select * from OBIEGDOK_STRUKTURA o1 order by o1.id");
//				ps = DSApi.context().prepareStatement("select * from OBIEGDOK_STRUKTURA o1 where (select count(1) from obiegdok_powiazanie_uzytkow p where p.id_dzialu = o1.id) > 0 order by o1.id");
				rs = ps.executeQuery();
				String externalId =null;
				String nazwaKomorki = null;
				String idDzialuNadrzednego = null;
				String code = null;
				//DODAWANIE DZIA��W
				while (rs.next())
				{
					
					DSDivision division = null;
					externalId = rs.getString("ID");
					nazwaKomorki = rs.getString("NAZWA");
					idDzialuNadrzednego = rs.getString("ID_DZIALU_NADRZEDNEGO");
					code = rs.getString("KOD_L").trim().replace(" ", "-");
					log.error("PR�BA: DODAJE DZIA� "+nazwaKomorki+" ID "+externalId);
					try
					{
						
						DSDivision div = DSDivision.findByExternalId(externalId);
						log.error("ZNALAZ�: USTAWIA JAKO AKTYWNY");
						DSApi.context().begin();
                        div.setDescription(PIG_IMPORTED);
						div.setHidden(false);
						div.setName(nazwaKomorki);
						if (!StringUtils.isEmpty(idDzialuNadrzednego))
						{
							division = DSDivision.findByExternalId(idDzialuNadrzednego);
						}
						else
						{
							division = DSDivision.find(DSDivision.ROOT_GUID);
						}
						div.setParent(division);
						DSApi.context().commit();
					}
					catch (DivisionNotFoundException de)
					{
						log.error("NIE ZNALAZ�: DODAJE");
						try
						{
							DSApi.context().begin();
							// pobieram bie��cy dzia� (o ile istnieje)
							if (!StringUtils.isEmpty(idDzialuNadrzednego))
							{
								division = DSDivision.findByExternalId(idDzialuNadrzednego);
							}
							else
							{
								division = DSDivision.find(DSDivision.ROOT_GUID);
							}
							
							DSDivision newDivision = division.createDivisionWithExternal(nazwaKomorki,code , externalId);
							newDivision.setDescription(PIG_IMPORTED);
							newDivision.update();
							DSApi.context().commit();
							log.error("DODA�");

							console(Console.INFO, sm.getString("System UsersIntegrationService zaimportowa� dzia� " + nazwaKomorki));
						}
						catch (DuplicateNameException dne)
						{
							log.error("DuplicateNameException");
							DSApi.context().rollback();
							DSApi.context().begin();
							try
							{
								DSDivision parentdivision = null;
								if (!StringUtils.isEmpty(idDzialuNadrzednego))
								{
									parentdivision = DSDivision.findByExternalId(idDzialuNadrzednego);
								}
								else
								{
									parentdivision = DSDivision.find(DSDivision.ROOT_GUID);
								}
								division = DSDivision.findByName(nazwaKomorki);
								division.setCode(code);
								division.setExternalId(externalId);
                                division.setDescription(PIG_IMPORTED);
								division.setParent(parentdivision);
								division.setHidden(false);
								division.update();
								DSApi.context().commit();
								console(Console.INFO, sm.getString("System UsersIntegrationService zmodyfikowa� dzia� " + nazwaKomorki));
							}
							catch (Exception exc)
							{
								log.error("B��d dodania externalId {},code {},nazwaKomorki {},idDzialuNadrzednego {}",externalId,code,nazwaKomorki,idDzialuNadrzednego);
								log.error(exc.getMessage(), exc);
								DSApi.context().rollback();
							}

						}
						catch (Exception exc)
						{
							log.error("B��d dodania externalId {},code {},nazwaKomorki {},idDzialuNadrzednego {}",externalId,code,nazwaKomorki,idDzialuNadrzednego);
							log.error(exc.getMessage(), exc);
							DSApi.context().rollback();
						}
					}
				}
				ps.close();
				ps = DSApi.context().prepareStatement("select * from OBIEGDOK_UZYTKOWNICY");
				rs = ps.executeQuery(); 
				String nrEwidencji = "";
				String imie;
				String nazwisko;
				String login;
				String email;
				DSUser user = null;
				//DODAWANIE UZYTKOWNIKOW
				while (rs.next())
				{
					login = rs.getString("LOGINAD").replace(".", "").trim();
					imie = rs.getString("IMIE");
					nazwisko = rs.getString("NAZWISKO");
					nrEwidencji = rs.getString("ID");
					email = rs.getString("EMAIL");
					if (nrEwidencji != null)
					{
						try
						{
							user = DSUser.findByExtension(nrEwidencji);
                            user.setFirstname(imie);
                            user.setLastname(nazwisko);
                            user.setRemarks(PIG_IMPORTED);
                            user.setEmail(email);
                            user.setExtension(nrEwidencji);
                            user.update();
						}
						catch (UserNotFoundException e)
						{
							try
							{
									DSApi.context().begin();
									user = UserFactory.getInstance().createUser(login,imie,nazwisko);
									user.setExternalName(login);
									user.setExtension(nrEwidencji);
									user.setRemarks(PIG_IMPORTED);
									user.setEmail(email);
									user.setCtime(new Timestamp(new Date().getTime()));
									user.setDomainPrefix("PGI");
									user.update();
									user.setAdUser(true);
									
								//	Role.findByName(PODSTAWOWE_UPRAWNIENIA).addUser(user.getName());
									DSApi.context().commit();
									console(Console.INFO, sm.getString("System UsersIntegrationService zaimportowa� uzytkownika " + user.getName()));
							}
							catch (DuplicateNameException dne)
							{
								try
								{
									user = DSUser.findByUsername(login);
									user.setExtension(nrEwidencji);
                                    user.setRemarks(PIG_IMPORTED);
////									boolean flag = false;
////									for (String rola : user.getOfficeRoles()) {
////										if (rola.equals(PODSTAWOWE_UPRAWNIENIA))
////											flag = true;
////									}
////									if (!flag)
////										Role.findByName(PODSTAWOWE_UPRAWNIENIA).addUser(user.getName());
////									if (!user.isAdUser())
////										user.setAdUser(true);
									user.update();
									DSApi.context().commit();
									
								}
								catch (Exception exc)
								{
									log.error("nrEwidencji {} ,imie {},nazwisko {},login {},email {}", nrEwidencji ,imie,nazwisko,login,email);
									log.error(exc.getMessage(), exc);
									DSApi.context().rollback();
								}

							}
							catch (DivisionNotFoundException de)
							{
								log.error("nrEwidencji {} ,imie {},nazwisko {},login {},email {}", nrEwidencji ,imie,nazwisko,login,email);
								DSApi.context().rollback();
								console(Console.INFO, sm.getString("System UsersIntegrationService nie mo�e zaimportowa� u�ytkownika " + rs.getString("Imie") + " " + rs.getString("Nazwisko") + " poniewa� dzia� " + rs.getString("Komorka") + " nie istnieje"));
							}
							catch (Exception exc)
							{
								log.error("nrEwidencji {} ,imie {},nazwisko {},login {},email {}", nrEwidencji ,imie,nazwisko,login,email);
								log.error(exc.getMessage(), exc);
								DSApi.context().rollback();
							}
							profile.addUser(user);
						}
						log.error("ZAIMPORTOWA� nrEwidencji {} ,imie {},nazwisko {},login {},email {}", nrEwidencji ,imie,nazwisko,login,email);
					}
				}
				ps.close();
				ps = DSApi.context().prepareStatement("select * from OBIEGDOK_POWIAZANIE_UZYTKOW");
				rs = ps.executeQuery();
				String idDzialu = null;
				String idUzytkownika = null;
				HashMap<String, String> synchronizedUsers = new HashMap<String, String>();
				//DODAWANIE POWIAZANIA DO JEDNOSTKI
				while (rs.next())
				{
					try
					{
						idDzialu = rs.getString("ID_DZIALU");
						idUzytkownika = rs.getString("ID_UZYTKOWNIKA").trim();
						if(!synchronizedUsers.containsKey(idUzytkownika)){
							DSDivision division = null;
							DSUser userpow = null;
							HashMap<String,String> divisionsToUserFromView = new HashMap<String, String>();
							try
							{
								userpow = DSUser.findAllByExtension(idUzytkownika);
							}
							catch(UserNotFoundException e)
							{
								PreparedStatement psuser = null;
								ResultSet rsuser = null;
								try
								{
									psuser = DSApi.context().prepareStatement("select * from OBIEGDOK_POWIAZANIE_UZYTKOW where ID_UZYTKOWNIKA = ?");
									psuser.setString(1, idUzytkownika);
									rsuser = psuser.executeQuery();
									while(rsuser.next())
									{
										divisionsToUserFromView.put(rsuser.getString("ID_DZIALU"), rsuser.getString("ID_UZYTKOWNIKA"));
									}
								}
								finally
								{
									DbUtils.closeQuietly(psuser);
								}
							}
							try
							{
								PreparedStatement psuser = null;
								ResultSet rsuser = null;
								try
								{
									psuser = DSApi.context().prepareStatement("select * from OBIEGDOK_POWIAZANIE_UZYTKOW where ID_UZYTKOWNIKA = ?");
									psuser.setString(1, idUzytkownika);
									rsuser = psuser.executeQuery();
									while(rsuser.next())
									{
										divisionsToUserFromView.put(rsuser.getString("ID_DZIALU"), rsuser.getString("ID_UZYTKOWNIKA"));
									}
								}
								finally
								{
									DbUtils.closeQuietly(psuser);
								}
								
								
								if(userpow != null){
									DSApi.context().begin();
									DSDivision[] userDivisionsFromSystem = userpow.getAllDivisions();
									//usuwam uzytkownika  z dzialu 
									for(DSDivision systemDiv : userDivisionsFromSystem){
										if(systemDiv.isDivision()){
											if(divisionsToUserFromView.containsKey(systemDiv.getExternalId())){
												;
											}else{
												log.error("Usuwam z dzia�u: '" + systemDiv.getName() + "' uzytkownika o username: " + userpow.getName());
												systemDiv.removeUser(userpow);
											}
										}
									}
									//dodaje uzytkownika do dzialu
									for(String dzial : divisionsToUserFromView.keySet()){
										division = DSDivision.findByExternalId(dzial);
										log.error("Dodaje do dzia�u: " + division.getName() + " uzytkownika o username: " + userpow.getName());
										division.addUser(userpow);
									}
									DSApi.context().commit();
								}
								
							}
							catch (Exception e)
							{
								log.error(" idDzialu {},idUzytkownika{}", idDzialu,idUzytkownika);
								log.error(e.getMessage(), e);
								//console(Console.INFO, sm.getString("Nie uda�o si� zaimportowa� dzi��u {} dla usera {}",idDzialu,idUzytkownika));
								DSApi.context().rollback();
							}
						}else{
							synchronizedUsers.put(idUzytkownika, idDzialu);
						}
					}
					catch (Exception e)
					{
						log.error(" idDzialu {},idUzytkownika{}", idDzialu,idUzytkownika);
						log.error(e.getMessage(), e);
						//console(Console.INFO, sm.getString("Nie uda�o si� zaimportowa� dzi��u {} dla usera {}",idDzialu,idUzytkownika));
						DSApi.context().rollback();
					}
				}
				DbUtils.closeQuietly(ps);
				
				List<DSUser> users = DSUser.list(DSUser.SORT_FIRSTNAME_LASTNAME);
				int i = 0;
				//USUWANIE UZYTKOWNIKOW
				for (DSUser dsUser : users) 
				{
                    if (dsUser.getRemarks().equals(PIG_IMPORTED))
                        continue;
					i++;
					try
					{
						DSApi.context().begin();
						ps = DSApi.context().prepareStatement("select * from OBIEGDOK_UZYTKOWNICY where LOWER(loginAD) = LOWER(?)");
						ps.setString(1, dsUser.getName());
						rs = ps.executeQuery();
						if(!rs.next())
						{
							log.error("USUWAM USERA {} idobce {}", dsUser.getName(),dsUser.getExtension());
							UserFactory.getInstance().deleteUser(dsUser.getName());
						}
						ps.close();
						
						if(i % 50 == 0 )
						{
							log.error("Wyczyszczenie sesji na idx: " + i);
							DSApi.context().session().flush();
							DSApi.context().session().clear();
							DSApi.context().commit();
							DSApi.close();
							DSApi.openAdmin();
						}
						else
						{
							DSApi.context().commit();
						}
					}
					catch (Exception se) {
						DbUtils.closeQuietly(ps);
						log.error(se.getMessage(), se);
						DSApi.context().rollback();
					}
					
				}
				
				//USUWANIE DZIALOW
				DSDivision[]  divisions = DSDivision.getAllDivisions();
				for (DSDivision dsDivision : divisions)
				{
					if(DSDivision.TYPE_DIVISION.equals(dsDivision.getDivisionType()))
					{
                        if (dsDivision.getDescription().equals(PIG_IMPORTED))
                            continue;
						try
						{
							
							DSApi.context().begin();
							if(StringUtils.isEmpty(dsDivision.getExternalId()) && !DSDivision.ROOT_GUID.equals(dsDivision.getGuid()))
							{
								log.error("USUWAM DZIA� dsdivision {}", dsDivision.getName()+" "+dsDivision.getGuid()+" "+dsDivision.getExternalId());
								UserFactory.getInstance().deleteDivision(dsDivision.getGuid(), DSDivision.ROOT_GUID);
							}
							else if(StringUtils.isNotEmpty(dsDivision.getExternalId()) && !DSDivision.ROOT_GUID.equals(dsDivision.getGuid()))
							{
								ps = DSApi.context().prepareStatement("select * from OBIEGDOK_STRUKTURA o1 where o1.id = ?");
								ps.setString(1, dsDivision.getExternalId());
								rs = ps.executeQuery();
								if(!rs.next())
								{
									log.error("USUWAM DZIA� dsdivision {}", dsDivision.getName()+" "+dsDivision.getGuid()+" "+dsDivision.getExternalId());
									UserFactory.getInstance().deleteDivision(dsDivision.getGuid(), DSDivision.ROOT_GUID);
								}
								else
								{
									log.error("NIE usuwa dzia�u "+dsDivision.getName()+" "+dsDivision.getGuid()+" "+dsDivision.getExternalId());
								}
								
								ps.close();
							}
							else
							{
								log.error("DZIA� G��WNY NIE USUWAM");
							}
							DSApi.context().commit();
						}
						catch (Exception se) {
							
							log.error(se.getMessage(), se);
							DbUtils.closeQuietly(ps);
							DSApi.context().rollback();
						}
					}
				}				
			}
			catch (ProfileNotFoundException pnfe){ // rzucane na samym poczatku przy szukaniu profili.
				//nic nie robic. Wystarczy zwykle zamkniecie w finally
				log.debug("Pomini�cie zb�dnej us�ugi " +pnfe.getMessage());
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
				console(Console.INFO, sm.getString(e.toString()));
				try
				{
					DSApi.context().rollback();
				}
				catch (EdmException e1)
				{
					log.error(e.getMessage(), e1);
				}
			}
			finally
			{
				try
				{
					DSApi.context().closeStatement(ps);
					DSApi.close();
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
				}
			}

		}
	}

	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}
}

