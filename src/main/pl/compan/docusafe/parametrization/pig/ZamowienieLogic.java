package pl.compan.docusafe.parametrization.pig;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import com.google.common.collect.Maps;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.ocr.OcrSupport;
import pl.compan.docusafe.core.ocr.OcrUtils;
import pl.compan.docusafe.core.office.CasePriority;
import pl.compan.docusafe.core.office.CaseStatus;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.TopicFoundingSource;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class ZamowienieLogic extends AbstractDocumentLogic
{

	private static ZamowienieLogic instance;
	protected static Logger log = LoggerFactory.getLogger(ZamowienieLogic.class);

	/**
	 * Akcpetacje
	 */
	private static String TOPIC_MANAGER = "topic-manager";
	private static String TOPIC_SUPERVISOR = "topic-supervisor";
	private static String SUPERVISOR_MANAGER = "supervisor-manager";

	private static String CORRECTION = "correction";
	private static String REALIZATION = "realization";
	private static String SSZP = "sszp";
	private static String AUTHOR = "author";

	private static BigDecimal proc_12 = new BigDecimal(12000);
	private static BigDecimal proc_14 = new BigDecimal(14000);
	private static BigDecimal proc_193 = new BigDecimal(193000);

	static
	{
		proc_12.setScale(2);
		proc_14.setScale(2);
		proc_193.setScale(2);
	}

	public static ZamowienieLogic getInstance()
	{
		if (instance == null)
			instance = new ZamowienieLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		Map<String, Object> toReload = Maps.newHashMap();
		for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
		{
			if (f.getDefaultValue() != null)
			{
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
			}
		}
		fm.reloadValues(toReload);
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		log.info("--- ZamowienieLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

	@Override
	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			FieldsManager fm = dockind.getFieldsManager(id);
			params.setStatus((String) fm.getValue("STATUS"));
			params.setAttribute((String) fm.getValue("NR_ZAMOWIENIA"), 0);
			params.setDocumentNumber((String) fm.getValue("NR_ZAMOWIENIA"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return params;
	}

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{

	}

	public void validate(Map<String, Object> values, DocumentKind kind) throws EdmException
	{

		if (values.containsKey("WNIOSEK") && ((List) values.get("WNIOSEK")).size() > 0)
		{
			List<Long> ids = (List<Long>) values.get("WNIOSEK");
			try
			{
				PigParametrization.checkDocuments(ids);
				NextTask.finishTask(ids);
			}
			catch (EdmException e)
			{
				throw e;
			}
		}
	}

	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		log.info("validate contents zapotrzebowanie ifpan: \n {} ", values);

		DocumentKind kind = fm.getDocumentKind();

		if (values.get("DWR_NETTO").getData() != null)
		{
			BigDecimal netto = values.get("DWR_NETTO").getMoneyData();
			String kurs = Docusafe.getAdditionProperty("kurs_euro") != null ? Docusafe.getAdditionProperty("kurs_euro") : "3.839";
			BigDecimal euro = new BigDecimal(kurs);
			euro = netto.divide(euro, 2, RoundingMode.DOWN);
			values.get("DWR_EURO_NETTO").setMoneyData(euro);

			// setRealizationProcedure(values);

			if (values.get("DWR_VAT").getData() != null)
			{
				BigDecimal kwotaVat = new BigDecimal(values.get("DWR_VAT").getIntegerData());
				kwotaVat = kwotaVat.divide(new BigDecimal(100), 2, RoundingMode.DOWN);
				kwotaVat = kwotaVat.multiply(netto);

				values.get("DWR_KWOTA_VAT").setMoneyData(kwotaVat);
				BigDecimal brutto = values.get("DWR_KWOTA_VAT").getMoneyData();
				brutto = brutto.add(netto);
				values.get("DWR_BRUTTO").setMoneyData(brutto);
			}
		}
		if (values.get("DWR_START_DATE").getData() != null && values.get("DWR_FINISH_DATE").getData() != null)
		{
			Date startDate = values.get("DWR_START_DATE").getDateData();
			Date finishDate = values.get("DWR_FINISH_DATE").getDateData();

			if (finishDate.before(startDate))
			{
				values.get("DWR_FINISH_DATE").setDateData(startDate);
			}
		}
		if (values.containsKey("DWR_ZABEZPIECZNIE") && values.get("DWR_ZABEZPIECZNIE").getData() != null)
		{

			BigDecimal brutto = values.get("DWR_BRUTTO").getMoneyData();
			BigDecimal zabez = values.get("DWR_ZABEZPIECZNIE").getMoneyData();
			if (zabez.compareTo(brutto) == -1)
			{
				values.get("DWR_ZABEZPIECZNIE").setMoneyData(brutto);
			}
		}

		return null;
	}

	private void setRealizationProcedure(Map<String, FieldData> values)
	{
		BigDecimal euro = values.get("DWR_EURO_NETTO").getMoneyData();
		log.debug("ERUO: {}", euro);
		if (values.get("DWR_PROC_REALIZACJI") != null)
		{
			if (values.get("DWR_PROC_REALIZACJI").getEnumValuesData().getSelectedOptions().get(0).equals("21"))
				return;
			else
				values.get("DWR_PROC_REALIZACJI").getEnumValuesData().getSelectedOptions().remove(0);

			if (euro.compareTo(proc_12) < 0)
			{
				values.get("DWR_PROC_REALIZACJI").getEnumValuesData().getSelectedOptions().add("9");
			}
			else if (euro.compareTo(proc_12) == 0 || euro.compareTo(proc_14) < 0)
			{
				values.get("DWR_PROC_REALIZACJI").getEnumValuesData().getSelectedOptions().add("12");
			}
			else if (euro.compareTo(proc_14) == 0 || euro.compareTo(proc_193) < 0)
			{
				values.get("DWR_PROC_REALIZACJI").getEnumValuesData().getSelectedOptions().add("15");
			}
			else if (euro.compareTo(proc_193) == 0 || euro.compareTo(proc_193) > 0)
			{
				values.get("DWR_PROC_REALIZACJI").getEnumValuesData().getSelectedOptions().add("18");
			}
		}
	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn)
	{
		boolean assigned = false;
		try
		{
			if (accpetionCn.equals(TOPIC_MANAGER))
			{
				Integer id = doc.getFieldsManager().getEnumItem("TOPIC_FUNDING_SOURCE").getId();
				String manager = TopicFoundingSource.find(id.longValue()).getKierownik_konto_oracle();

				assignable.addCandidateUser(manager);
				assigned = true;
			}
			else if (accpetionCn.equals(TOPIC_SUPERVISOR))
			{
				Integer id = doc.getFieldsManager().getEnumItem("TOPIC_FUNDING_SOURCE").getId();
				String supervisor = TopicFoundingSource.find(id.longValue()).getOpiekun_konto_oracle();

				assignable.addCandidateUser(supervisor);
				assigned = true;
			}
			else if (accpetionCn.equals(SUPERVISOR_MANAGER))
			{
				Integer id = doc.getFieldsManager().getEnumItem("TOPIC_FUNDING_SOURCE").getId();
				String supervisor = TopicFoundingSource.find(id.longValue()).getOpiekun_konto_oracle();

				if (DSUser.findByUsername(supervisor).getDivisionsGuid().length > 0)
				{
					String guid = DSUser.findByUsername(supervisor).getDivisionsGuid()[0];
					for (AcceptanceCondition accept : AcceptanceCondition.find("manager-decision", guid))
					{
						if (accept.getUsername() != null)
						{
							assignable.addCandidateUser(accept.getUsername());
							assigned = true;
						}
						if (accept.getDivisionGuid() != null)
						{
							assignable.addCandidateGroup(accept.getDivisionGuid());
							assigned = true;
						}
					}
				}
			}
			else if (accpetionCn.equals(CORRECTION) || accpetionCn.equals(REALIZATION))
			{
				String realizationGuid = doc.getFieldsManager().getEnumItem("REALIZATION").getCn();
				assignable.addCandidateGroup(realizationGuid);
				assigned = true;
			}
			else if (accpetionCn.equals(SSZP))
			{
				String realizationUser = doc.getFieldsManager().getEnumItem("TO_REALIZATION").getCn();
				assignable.addCandidateUser(realizationUser);
				assigned = true;
			}
			else if (accpetionCn.equals(AUTHOR))
			{
				String realizationUser = doc.getAuthor();
				assignable.addCandidateUser(realizationUser);
				assigned = true;
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage());
		}
		return assigned;
	}

	public void doDockindEvent(DockindButtonAction eventActionSupport, ActionEvent event, Document document, String activity, Map<String, Object> values) throws EdmException
	{
		log.debug("do Docking Event: " + eventActionSupport.getDockindEventValue());
		log.debug("DocId: {}", document.getId());
		log.debug("FieldsVales: {}", document.getFieldsManager().getFieldValues().toString());
		log.debug("Values: {}", values);
		if ("doZbiorczy".equals(eventActionSupport.getDockindEventValue()))
		{
			newDocument(eventActionSupport, event, document);
		}
	}

	private void newDocument(DockindButtonAction eventActionSupport, ActionEvent event, Document document) throws EdmException
	{

	}

	@Override
	public OcrSupport getOcrSupport()
	{
		return PigOcrSupport.get();
	}
}
