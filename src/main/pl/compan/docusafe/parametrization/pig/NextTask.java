package pl.compan.docusafe.parametrization.pig;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class NextTask implements ExternalActivityBehaviour  {

	private final static Logger log = LoggerFactory.getLogger(NextTask.class);
	private Long docuemntId;
	
	public void execute(ActivityExecution execution) throws Exception {
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");

		OfficeDocument doc = OfficeDocument.find(docId);
		List<Long> ids = new ArrayList<Long>();
		boolean zbiorczy = false;
		if (doc.getFieldsManager().getValue("WNIOSEK")!=null)
			for (String id : (List<String>) doc.getFieldsManager().getValue("WNIOSEK"))
				ids.add(Long.valueOf(id));
		
		if (ids.size()>0)
		{	
			PigParametrization.checkDocuments(ids);
			zbiorczy = finishTask(ids);
		}
		if (zbiorczy)
			execution.take("to-realization");
		else
			execution.take("to-manager");
	}

	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
	}
	
	public static boolean finishTask(List<Long> ids) throws EdmException
	{
		for (Long id : ids)
		{
			WorkflowFactory wf = WorkflowFactory.getInstance();
            if(wf instanceof InternalWorkflowFactory){
                String activityId = wf.findManualTask(id);
                wf.manualFinish(activityId, false);
            	toMessage(Long.valueOf(id));
            } else if(wf instanceof Jbpm4WorkflowFactory) {
            	try {
            		String firstActivityId = Jbpm4ProcessLocator.taskIds(id).iterator().next();
            		wf.manualFinish(firstActivityId, false);
            		toMessage(Long.valueOf(id));
            	}
            	catch (NoSuchElementException e) {
            		log.warn("Dokument {} jest juz zamkniety",id);
            	}
            } else {
                throw new IllegalStateException("nieznany WorkflowFactory");
            }
		}
		return true;
	}
	
	private static void toMessage(Long id) throws DocumentNotFoundException, EdmException
	{
		OfficeDocument document = OfficeDocument.find(Long.valueOf(id));
		document.getDocumentKind()
				.getDockindInfo()
				.getProcessesDeclarations()
				.getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME)
				.getLogic()
				.startProcess(document, Collections.<String, Object>singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY,document.getAuthor()));
	}
}
