package pl.compan.docusafe.parametrization.pig;

import com.google.common.collect.Maps;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.FolderResolver;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.ocr.OcrSupport;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.zebra.ZebraPrinterManager;
import pl.compan.docusafe.util.BarcodeUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.*;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class NormalLogic extends AbstractDocumentLogic {

    private static NormalLogic instance;
    protected static Logger log = LoggerFactory.getLogger(NormalLogic.class);
    
    private static final String PRZYCHODZACE = "normal";
	private static final String WYCHODZACE = "normal_out";
	private static final String WEWNETRZNE = "normal_int";

    public static NormalLogic getInstance() {
        if (instance == null)
            instance = new NormalLogic();
        return instance;
    }

    public void documentPermissions(Document document) throws EdmException {
        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
        perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk�y- odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk�y - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk�y - usuwanie"));

        for (PermissionBean perm : perms) {
            if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP)) {
                String groupName = perm.getGroupName();
                if (groupName == null) {
                    groupName = perm.getSubject();
                }
                createOrFind(document, groupName, perm.getSubject());
            }
            DSApi.context().grant(document, perm);
            DSApi.context().session().flush();
        }
    }

	public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
		  log.info("type: {}", type);
	      Map<String, Object> toReload = Maps.newHashMap();
	        for(Field f : fm.getFields()){
	            if(f.getDefaultValue() != null){
	                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
	            }
	        }
	        toReload.put("TYPE", type);
	     fm.reloadValues(toReload);
	}

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        FolderResolver fr = document.getDocumentKind().getDockindInfo().getFolderResolver();
        if(fr != null) {
            as.useFolderResolver(document);
        }
    }

    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) {
        log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
        try {
            Map<String, Object> map = Maps.newHashMap();
            map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
            map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
            
			DSApi.context().watch(URN.create(document));
			
			FieldsManager fm = document.getFieldsManager();
			if("normal_out".equals(fm.getDocumentKind().getCn())){
				if(fm.getKey("ZPO") != null){
					Boolean zpo = (Boolean)fm.getKey("ZPO");
					if(zpo){
						document.addRemark(new Remark("ZPO"));
					}
				}
				if(fm.getKey("DOC_DELIVERY") != null){
					String prio = (String)fm.getKey("DOC_DELIVERY");
					if(prio.equals("2") || prio.equals("4")){
						document.addRemark(new Remark("prio"));
					}
				}
			}
			
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException {
        ProcessCoordinator ret = new ProcessCoordinator();
        ret.setUsername(document.getAuthor());
        return ret;
    }


    @Override
    public OcrSupport getOcrSupport() {
        return PigOcrSupport.get();
    }
    
    @Override
    public void printLabel(Document document, int type, Map<String, Object> dockindKeys) throws EdmException
    {
    	if(!AvailabilityManager.isAvailable("print.barcode.create", document.getDocumentKind().getCn()))
    		return;
    	String barcode = null;
    	if(document instanceof InOfficeDocument)
    	{
    		barcode = ((OfficeDocument)document).getBarcode();
    		 if(StringUtils.isEmpty(barcode))
    		 {
    			 ((OfficeDocument)document).setBarcode(BarcodeUtils.getKoBarcode((OfficeDocument)document));
    			 barcode = ((OfficeDocument)document).getBarcode();
    		 }
    	}
    	else
    	{
    		return;
    	}
    	Map<String,String> fields = new LinkedHashMap<String,String>();
    	ZebraPrinterManager zm = new ZebraPrinterManager();
		zm.printLabel(barcode, fields);
    }
}
