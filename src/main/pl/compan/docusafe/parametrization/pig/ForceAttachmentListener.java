package pl.compan.docusafe.parametrization.pig;

import java.math.BigDecimal;
import java.util.Arrays;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ForceAttachmentListener extends AbstractEventListener{
    private final static Logger LOG = LoggerFactory.getLogger(ForceAttachmentListener.class);

    private String attachmentTitle;
    
    
    public void notify(EventListenerExecution eventListenerExecution) throws Exception {

    	String[] titles = attachmentTitle.split("#");
    	String[] less14 = titles[0].split(",");
    	String[] grater14 = titles[1].split(",");
    	
        Long docId = Long.valueOf(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);
        BigDecimal val = new BigDecimal(((String)doc.getFieldsManager().getValue("EURO_NETTO")).replaceAll(" " , ""));
        
        String exString = "";
        int i = 0;
        
        for (String att : less14)
        {
    		boolean ok = false;
    		for(Attachment at: doc.getAttachments()){
    			if(at.getTitle().equals(att)){
    				i++;
    				ok = true;
    			}
    		}
    		if (!ok)
    			if (exString.length()==0)
    				exString=att;
    			else
    				exString+=", "+att;
    		
    	}
    	
    	if (i == 2 && val.compareTo(new BigDecimal("14000.00")) < 0)
    		return;
    	
    	if (val.compareTo(new BigDecimal("14000.00")) >=0)
    	{
    		for (String att : grater14)
    		{
    			boolean ok = false;
        		for(Attachment at: doc.getAttachments()){
        			if(at.getTitle().equals(att)){
        				i++;
        				ok = true;
        			}
        		}
        		if (!ok)
        			if (exString.length()==0)
        				exString=att;
        			else
        				exString+=", "+att;
    		}
    		if (i == 4)
    			return;
    	}

        throw new IllegalStateException("Brak wymaganego załącznika - " + exString);
    }
}
