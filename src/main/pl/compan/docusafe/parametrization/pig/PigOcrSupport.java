package pl.compan.docusafe.parametrization.pig;

import com.google.common.collect.Sets;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.ocr.OcrSupport;
import pl.compan.docusafe.core.ocr.OcrUtils;
import java.io.File;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
class PigOcrSupport {
    private final static OcrSupport OCR_SUPPORT = OcrUtils.getFileOcrSupport(
            new File(Docusafe.getHome(), "ocr-put"),
            new File(Docusafe.getHome(), "ocr-get"),
            Sets.newHashSet("tiff","tif","gif")
    );

    public static OcrSupport get(){
        return OCR_SUPPORT;
    }
}
