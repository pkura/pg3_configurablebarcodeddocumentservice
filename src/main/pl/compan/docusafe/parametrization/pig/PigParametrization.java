package pl.compan.docusafe.parametrization.pig;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.AbstractParametrization;
import pl.compan.docusafe.web.office.tasklist.TaskListAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class PigParametrization extends AbstractParametrization
{

	public static String DO_ZBIORCZY = "doZbiorczy";

	private Long documentId;
	private String activity;
	public static Map<String, String> taskListEvent = new HashMap<String, String>();

	static
	{
		taskListEvent.put("doZbiorczy", "Wniosek zbiorczy");
	}

	public void doTaskListAction(TaskListAction taskListAction, String actionName, ActionEvent event, Long[] docIds) throws EdmException
	{
		if (DO_ZBIORCZY.equals(actionName))
			doZbiorczy(taskListAction, event, docIds);
		else
			throw new EdmException("Brak implementacj akcji: {}" + actionName);
	}

	public static boolean checkDocuments(List<Long> docIds) throws EdmException
	{
		List<Document> documents = Document.find(docIds);
		for (Document doc : documents)
		{
			if (!doc.getDocumentKind().getCn().equals(DocumentKind.ZAMOWIENIE_PIG))
				throw new EdmException("Wniosek zbiorczy mo�na tylko stwrzy� dla dokumentow typu Zamowienie Publiczne!\nDokument " + doc.getTitle() + " nie jest tego typu");

			DocumentAcceptance acceptance = null;

			if ((Integer) doc.getFieldsManager().getKey(("TOPIC_ONLY")) == 1)
			{
				acceptance = DocumentAcceptance.find(doc.getId(), "dpizu-manager", null);
				throw new EdmException("Zamowienie " + ((OfficeDocument) doc).getFieldsManager().getValue("NR_ZAMOWIENIA") + " nie posiada akcpetacji Kierownika DPiZU");
			}
			else
			{
				acceptance = DocumentAcceptance.find(doc.getId(), "manager-decision", null);
				if (acceptance == null)
					throw new EdmException("Zamowienie " + ((OfficeDocument) doc).getFieldsManager().getValue("NR_ZAMOWIENIA") + " nie posiada akcpetacji Kierownika komorki wnisokujacej");
			}

		}
		return true;
	}

	private void doZbiorczy(TaskListAction taskListAction, ActionEvent event, Long[] docIds) throws EdmException
	{
		try
		{
			checkDocuments(Arrays.asList(docIds));

			taskListAction.setDocumentKindCn(DocumentKind.ZAMOWIENIE_PIG);
			Map<String, Long[]> dependsDocs = new HashMap<String, Long[]>();

			dependsDocs.put("WNIOSEK", docIds);
			Gson gson = new Gson();
			String ids = gson.toJson(dependsDocs);
			String encodedString = null;
			ids = ids.replace("\"", "");
			try
			{
				encodedString = URLEncoder.encode(ids, "UTF-8");
			}
			catch (UnsupportedEncodingException e)
			{
				log.error(e.getMessage());
				e.printStackTrace();
			}
			taskListAction.setDependsDocsJson(encodedString);
			event.setResult("new-int-office-document-dwr");
		}
		catch (Exception e)
		{
			log.error(e.getMessage());
			event.addActionError(e.getMessage());
			throw new EdmException(e.getMessage());
		}
	}

	public Map<String, String> getTaskListEvent()
	{
		return taskListEvent;
	}

	public Long getDocumentId()
	{
		return documentId;
	}

	public void setDocumentId(Long documentId)
	{
		this.documentId = documentId;
	}

	public String getActivity()
	{
		return activity;
	}

	public void setActivity(String activity)
	{
		this.activity = activity;
	}

}
