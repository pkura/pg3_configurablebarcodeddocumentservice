package pl.compan.docusafe.parametrization.pig;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.jbpm4.EnumCnDecisionHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class AssigneeDecisionHandler implements DecisionHandler
{

	private final static Logger LOG = LoggerFactory.getLogger(EnumCnDecisionHandler.class);

	private String DIVISION_DIRECTOR = "division-director-decision";

	private String enumCn;

	public String decide(OpenExecution openExecution)
	{
		try
		{
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			LOG.info("documentid = " + docId + " field = " + enumCn);
			String guid = fm.getEnumItem(enumCn).getCn();

			if (AcceptanceCondition.find(DIVISION_DIRECTOR, guid).size() > 0)
				return "full";
			else
				return "short";

		}
		catch (EdmException ex)
		{
			LOG.error(ex.getMessage(), ex);
			return "error";
		}
	}

}
