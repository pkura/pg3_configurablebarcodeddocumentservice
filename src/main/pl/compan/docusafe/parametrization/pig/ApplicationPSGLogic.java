package pl.compan.docusafe.parametrization.pig;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.sql.SQLException;
import java.util.Map;
import java.util.Set;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import com.google.common.collect.Maps;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.PersonDictionary;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.ocr.OcrSupport;
import pl.compan.docusafe.core.ocr.OcrUtils;
import pl.compan.docusafe.core.office.CasePriority;
import pl.compan.docusafe.core.office.CaseStatus;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.TopicFoundingSource;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class ApplicationPSGLogic extends AbstractDocumentLogic
{
	static final Object ODBIOR_POCZTA_FIELD = "ODBIOR_POCZTA";
	static final Object ODBIOR_OSOBISTY_FIELD = "ODBIOR_OSOBISTY";
	static final Object ODBIOR_EMAIL_FIELD = "ODBIOR_EMAIL";
	private static ApplicationPSGLogic instance;
	protected static Logger log = LoggerFactory.getLogger(ApplicationPSGLogic.class);

	public static ApplicationPSGLogic getInstance()
	{
		if (instance == null)
			instance = new ApplicationPSGLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		
		String docKindName = document.getDocumentKind().getName();
		
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, docKindName +" - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, docKindName +" - za��cznik, odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, docKindName +" - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP,docKindName +" - za��cznik, modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, docKindName +" - usuwanie"));

		Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
		perms.addAll(documentPermissions);
		
		this.setUpPermission(document, perms);
	}

	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException {
		if ((fieldValues.get("TYP") != null && !fieldValues.get("TYP").equals(new Integer(210)) || fieldValues.get("TYP") == null) && (fieldValues.containsKey(ODBIOR_POCZTA_FIELD) || fieldValues.containsKey(ODBIOR_OSOBISTY_FIELD) || fieldValues.containsKey(ODBIOR_EMAIL_FIELD))) {
			int checkedBoxCount = 0;
			
			if (fieldValues.get(ODBIOR_POCZTA_FIELD) != null && (Boolean) fieldValues.get(ODBIOR_POCZTA_FIELD) == true) {
				checkedBoxCount++;
			}
			if (fieldValues.get(ODBIOR_OSOBISTY_FIELD) != null && (Boolean) fieldValues.get(ODBIOR_OSOBISTY_FIELD) == true) {
				checkedBoxCount++;
			}
			if (fieldValues.get(ODBIOR_EMAIL_FIELD) != null && (Boolean) fieldValues.get(ODBIOR_EMAIL_FIELD) == true) {
				checkedBoxCount++;
			}

			if (checkedBoxCount == 0) {
				throw new EdmException("Nie wybrano �adnego sposobu odbioru, prosz� zaznaczy� przynajmniej jeden");
			}
		}
	}

	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		Map<String, Object> toReload = Maps.newHashMap();
		for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
		{
			if (f.getDefaultValue() != null)
			{
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
			}
		}
		fm.reloadValues(toReload);
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		log.info("--- ApplicationLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			
			java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

			DSUser author = DSApi.context().getDSUser();
			String user = author.getName();
			String fullName = author.getLastname() + " " + author.getFirstname();

			perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

			Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
			perms.addAll(documentPermissions);
			this.setUpPermission(document, perms);

		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		
		return null;
	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn)
	{
		boolean assigned = false;
		return assigned;
	}

	@Override
	public OcrSupport getOcrSupport()
	{
		return PigOcrSupport.get();
		
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
		// TODO Auto-generated method stub
		
	}
}
