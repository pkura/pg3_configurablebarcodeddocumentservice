package pl.compan.docusafe.parametrization.pig;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class FillSubmitterDataListener extends AbstractEventListener
{
	private final static Logger LOG = LoggerFactory.getLogger(FillSubmitterDataListener.class);

	public void notify(EventListenerExecution eventListenerExecution) throws Exception
	{
		Long docId = Long.valueOf(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		LOG.info("OGIEN");
		String status = doc.getFieldsManager().getEnumItemCn("STATUS");
		String actype = doc.getFieldsManager().getEnumItemCn("ACCEPTING_TYPE");
		LOG.info("1.Status: {} - {} ", status, actype);

	}
}
