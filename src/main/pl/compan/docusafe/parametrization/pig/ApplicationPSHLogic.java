package pl.compan.docusafe.parametrization.pig;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import com.google.common.collect.Maps;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.ocr.OcrSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import static pl.compan.docusafe.parametrization.pig.ApplicationPSGLogic.ODBIOR_EMAIL_FIELD;
import static pl.compan.docusafe.parametrization.pig.ApplicationPSGLogic.ODBIOR_OSOBISTY_FIELD;
import static pl.compan.docusafe.parametrization.pig.ApplicationPSGLogic.ODBIOR_POCZTA_FIELD;

public class ApplicationPSHLogic extends AbstractDocumentLogic {
	private static ApplicationPSHLogic instance;
	protected static Logger log = LoggerFactory.getLogger(ApplicationPSHLogic.class);
	private static final String SUBSTANTIVE_ACCEPTANCE = "acceptance_substantive";
	private static final String ZRODLO_FIELD = "ZRODLO";
	private static final String INNE_BAZY_FIELD = "INNE_BAZY";
	private static final Object ZRODLO_MWP_FIELD = null;
	
	public static ApplicationPSHLogic getInstance() {
		if (instance == null)
			instance = new ApplicationPSHLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		
		String docKindName = document.getDocumentKind().getName();
		
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, docKindName +" - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, docKindName +" - za��cznik, odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, docKindName +" - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP,docKindName +" - za��cznik, modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, docKindName +" - usuwanie"));

		Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
		perms.addAll(documentPermissions);
		
		this.setUpPermission(document, perms);
	}
	
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
		Map<String, Object> toReload = Maps.newHashMap();
		for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields()) {
			if (f.getDefaultValue() != null) {
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
			}
		}
		fm.reloadValues(toReload);
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
		//if (fieldValues.containsKey("INNE EXCEL"))
		//	throw new EdmException("Suma kwot centr�w koszt�w musi by� r�wna kwocie brutto faktury");
		
		log.info("--- ApplicationLogic : START PROCESS !!! ---- {}", event);
		try {
			Map<String,Object> fieldValues = document.getFieldsManager().getFieldValues();
			
			int checkedBoxCount = 0;
			List<String> checkboxes = new ArrayList<String>();
			checkboxes.add("ZRODLO_MWP");
			checkboxes.add("ZRODLO_CBDH");
			checkboxes.add("ZRODLO_POBORY");
			checkboxes.add("ZRODLO_GZWP");
			checkboxes.add("ZRODLO_INNE");

			for (String field : checkboxes) {
				if (fieldValues.get(field) != null && (Boolean) fieldValues.get(field) == true) {
					checkedBoxCount++;
				}
			}

			if (checkedBoxCount == 0) {
				throw new EdmException("Nie wybrano �adnych informacji do udost�pnienia, prosz� zaznaczy� przynajmniej jeden rodzaj bazy");
			}
			
			
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			
			java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

			DSUser author = DSApi.context().getDSUser();
			String user = author.getName();
			String fullName = author.getLastname() + " " + author.getFirstname();

			perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

			Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
			perms.addAll(documentPermissions);
			this.setUpPermission(document, perms);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException {
		if (fieldValues.containsKey(ODBIOR_POCZTA_FIELD) || fieldValues.containsKey(ODBIOR_OSOBISTY_FIELD) || fieldValues.containsKey(ODBIOR_EMAIL_FIELD)) {
			int checkedBoxCount = 0;
			
			if (fieldValues.get(ODBIOR_POCZTA_FIELD) != null && (Boolean) fieldValues.get(ODBIOR_POCZTA_FIELD) == true) {
				checkedBoxCount++;
			}
			if (fieldValues.get(ODBIOR_OSOBISTY_FIELD) != null && (Boolean) fieldValues.get(ODBIOR_OSOBISTY_FIELD) == true) {
				checkedBoxCount++;
			}
			if (fieldValues.get(ODBIOR_EMAIL_FIELD) != null && (Boolean) fieldValues.get(ODBIOR_EMAIL_FIELD) == true) {
				checkedBoxCount++;
			}

			if (checkedBoxCount == 0) {
				throw new EdmException("Nie wybrano �adnego sposobu odbioru, prosz� zaznaczy� przynajmniej jeden");
			}
		}
	}

	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
		return null;
	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn) {
		boolean assigned = false;
		try {
			
			/*if (accpetionCn.equals(SUBSTANTIVE_ACCEPTANCE)) {
				assigned = substantiveAssignee(doc, assignable, openExecution);
			}*/
			
			if (accpetionCn.equals("INNE_accept")) {
				List<String> users = new ArrayList<String>();
				List<String> divisions = new ArrayList<String>();
				String cn = null;
				
				String otherSource = doc.getFieldsManager().getEnumItem(INNE_BAZY_FIELD).getCn();
				
				if (otherSource != null && !otherSource.equals("")) {
					cn = otherSource;
				}
				
				if (cn != null) {
					for (AcceptanceCondition accept : AcceptanceCondition.find(cn))
					{
						if (accept.getUsername() != null && !users.contains(accept.getUsername()))
							users.add(accept.getUsername());
						if (accept.getDivisionGuid() != null && !divisions.contains(accept.getDivisionGuid()))
							divisions.add(accept.getDivisionGuid());
					}
				}
				
				for (String user : users)
				{
					assignable.addCandidateUser(user);
					AssigneeHandler.addToHistory(openExecution, doc, user, null);
					assigned = true;
				}
				for (String division : divisions)
				{
					assignable.addCandidateGroup(division);
					AssigneeHandler.addToHistory(openExecution, doc, null, division);
					assigned = true;
				}
			}

		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}

		return assigned;
	}

	private boolean substantiveAssignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution) throws EdmException,
			UserNotFoundException {
		boolean assigned = false;
		
		List<String> users = new ArrayList<String>();
		List<String> divisions = new ArrayList<String>();
		String cn = null;
		
		String source = doc.getFieldsManager().getEnumItem(ZRODLO_FIELD).getCn();
		if (source != null && !source.equals("")) {
			if (source.equals("INNE")) {
				String otherSource = doc.getFieldsManager().getEnumItem(INNE_BAZY_FIELD).getCn();
				
				if (otherSource != null && !otherSource.equals("")) {
					cn = otherSource;
				}
			} else {
				cn = source;
			}
		}
		
		if (cn != null) {
			for (AcceptanceCondition accept : AcceptanceCondition.find(cn))
			{
				if (accept.getUsername() != null && !users.contains(accept.getUsername()))
					users.add(accept.getUsername());
				if (accept.getDivisionGuid() != null && !divisions.contains(accept.getDivisionGuid()))
					divisions.add(accept.getDivisionGuid());
			}
		}
		
		for (String user : users)
		{
			assignable.addCandidateUser(user);
			AssigneeHandler.addToHistory(openExecution, doc, user, null);
			assigned = true;
		}
		for (String division : divisions)
		{
			assignable.addCandidateGroup(division);
			AssigneeHandler.addToHistory(openExecution, doc, null, division);
			assigned = true;
		}
		return assigned;
	}

	@Override
	public OcrSupport getOcrSupport() {
		return PigOcrSupport.get();

	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
		// TODO Auto-generated method stub

	}
}
