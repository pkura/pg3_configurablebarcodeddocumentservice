package pl.compan.docusafe.parametrization.pig;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.service.mail.MailerDriver;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class SendMail implements ExternalActivityBehaviour
{

	private static final long serialVersionUID = 1L;
	private final static Logger log = LoggerFactory.getLogger(SendMail.class);

	private String template;

	public void execute(ActivityExecution execution) throws Exception
	{
		log.trace("Rozpoczecie wysylanie maili");
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		log.debug("docId: {}", docId);
		OfficeDocument doc = OfficeDocument.find(docId);
		MailerDriver mailer = (MailerDriver) ServiceManager.getService(Mailer.NAME);
		mailer.setFromEmail(DSApi.context().getDSUser().getEmail());
		String mail;
		for (int i = 1; i <= 3; i++)
		{
			mail = doc.getFieldsManager().getValue("MAIL" + i) != null ? doc.getFieldsManager().getValue("MAIL" + i).toString() : null;
			if (mail != null)
			{
				mailer.send(mail, mail, null, Configuration.getMail(template), prepareContect(doc.getFieldsManager()), false);
			}
		}
		execution.takeDefaultTransition();
	}

	private Map<String, Object> prepareContect(FieldsManager fm)
	{
		Map<String, Object> ctext = new HashMap<String, Object>();
		try
		{
			for (String cn : fm.getFieldValues().keySet())
			{
				ctext.put(cn, fm.getValue(cn));
			}
		}
		catch (EdmException e)
		{
			log.error(e.getMessage());
		}

		return ctext;
	}

	public void signal(ActivityExecution execution, String signalName, Map<String, ?> parameters) throws Exception
	{
	}

	public String getTemplate()
	{
		return template;
	}

	public void setTemplate(String template)
	{
		this.template = template;
	}

}
