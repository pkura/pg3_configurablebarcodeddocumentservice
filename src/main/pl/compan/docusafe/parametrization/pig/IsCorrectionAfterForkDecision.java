package pl.compan.docusafe.parametrization.pig;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

public class IsCorrectionAfterForkDecision implements DecisionHandler {

	public String decide(OpenExecution openExecution) {
		Integer cor = (Integer) openExecution.getVariable("correction");

		if (cor != null && cor == 1) {
			openExecution.removeVariable("correction");
			return "correction";
		} else if (cor != null && cor == 2) {
			openExecution.removeVariable("correction");
			return "cancel_app";
		} else {
			return "normal";
		}
	}
}
