package pl.compan.docusafe.parametrization.holiday;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.process.InstantiationParameters;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLogicSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import static pl.compan.docusafe.util.FolderInserter.root;

/**
 *
 */
public class HolidayRequestLogic extends AbstractDocumentLogic{
    private final static Logger LOG = LoggerFactory.getLogger(HolidayRequestLogic.class);

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        root()
			.subfolder("Dokumenty pracownicze")
			.subfolder("Wnioski o urlop")
            .insert(document);

        document.setTitle("Wniosek o urlop");
        ((OfficeDocument) document).setSummaryWithoutHistory("Wniosek urlopowy : " + document.getFieldsManager().getValue("STATUS") + "");
        document.setDescription(document.getFieldsManager().getValue("STATUS") + "");
    }

    @Override
    public void onStartProcess(OfficeDocument document) {
        LOG.info("START PROCESS !!!");
        try {
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }

    public void documentPermissions(Document document) throws EdmException {

    }
}
