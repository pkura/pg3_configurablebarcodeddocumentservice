package pl.compan.docusafe.parametrization.holiday;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class AssignmentAtEndListener extends AbstractEventListener {
    private final static Logger LOG = LoggerFactory.getLogger(AssignmentAtEndListener.class);

    private String dw;
    private String field;

    public void notify(EventListenerExecution eventListenerExecution) throws Exception {
        Long docId = Long.valueOf(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");

        String[] activityIds = new String[]{eventListenerExecution.getVariable(Jbpm4Constants.ACTIVITY_ID_PROPERTY).toString()};
        //String assignment = "null/"+dw+"/do_wiadomosci//Do wiadomo�ci";
        LOG.info("Button id = " + activityIds[0]);
        String assignment = null;

        if(field == null){
            assignment = dw+"/do_wiadomosci//Do wiadomo�ci";
        } else {
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            String username = fm.getEnumItemCn(field);
            if(username == null){
                throw new IllegalStateException("Pole " + fm.getField(field).getName() + " nie zosta�o wype�nione");
            }
            assignment = "null/" + username + "/do_wiadomosci//Do wiadomo�ci";
        }

        String[] plannedAssignments = new String[]{assignment};

        WorkflowFactory.multiAssignment(activityIds,DSApi.context().getPrincipalName(), plannedAssignments, null, null);
        LOG.info("dekretacja si� powiod�a");
    }
}
    //
