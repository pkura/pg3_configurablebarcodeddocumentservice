package pl.compan.docusafe.parametrization.cbk;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.core.record.projects.ProjectEntry;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class AddProjectEntryListener implements ExternalActivityBehaviour
{
	private static final Logger log = LoggerFactory.getLogger(AddProjectEntryListener.class);

	public void execute(ActivityExecution execution) throws Exception
	{
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Document document = Document.find(docId);
		
		Statement stat = null;
		ResultSet result = null;
		
		try
		{
			stat = DSApi.context().createStatement();
			String caseNumber = getCaseNumber(docId, stat, result);
			setBlocade(docId, caseNumber, document, stat, result);
			execution.takeDefaultTransition();
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			stat.close();
		}
	}

	private String getCaseNumber(Long docId, Statement stat, ResultSet result) throws SQLException, EdmException
	{
		String caseNumber = null;
		String year = DateUtils.formatYear(new java.util.Date());
		try
		{
			result = stat.executeQuery("SELECT caseNumber FROM dsg_cbk_order WHERE DOCUMENT_ID = " + docId);
			if (result.next())
				caseNumber = result.getString(1);
			if (caseNumber != null)
				return caseNumber;

			result = stat.executeQuery("SELECT MAX(CAST(SUBSTRING(caseNumber,1,LEN(caseNumber)-5) AS INT)) from dsg_cbk_order where caseNumber like '%/" + year + "'");
			result.next();
				
			Integer lastCaseNumber = result.getInt(1);
			if (lastCaseNumber == null)
				caseNumber = "1/" + year;
			else
				caseNumber =  new Integer(lastCaseNumber) + 1 + "/" + year;
			
			stat.executeUpdate("UPDATE dsg_cbk_order SET caseNumber = '" + caseNumber + "' WHERE DOCUMENT_ID = " + docId);
			return caseNumber;
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage(), e);
		}
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
	}
	
	private void setBlocade(Long docId, String caseNumber, Document document, Statement stat, ResultSet result) throws DocumentNotFoundException, DocumentLockedException, AccessDeniedException, EdmException, SQLException
	{

		String dicName = "DSG_CENTRUM_KOSZTOW_FAKTURY";
		ArrayList<Long> dictionaryIds = (ArrayList<Long>) Document.find(docId).getFieldsManager().getKey(dicName);
		FieldsManager fm = document.getFieldsManager();
		Integer entryNumber = 0;
		for (Long dicId : dictionaryIds)
		{
			String projectId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get(dicName).getValues(dicId.toString()).get(dicName + "_CENTRUMID")).getSelectedOptions().get(0);
			BigDecimal costPln = new BigDecimal(DwrDictionaryFacade.dictionarieObjects.get(dicName).getValues(dicId.toString()).get(dicName + "_AMOUNT").toString());
			Project project = Project.find(new Long(projectId));
			ProjectEntry projectEntry = ProjectEntry.findByCostId(dicId);
			if (projectEntry == null)
			{
				projectEntry = new ProjectEntry();
				projectEntry.setDateEntry(new java.util.Date());
				projectEntry.setCostId(dicId);
				projectEntry.setDocNr(docId.toString());
				projectEntry.setCurrency("PLN");
				projectEntry.setRate(new BigDecimal(1.0000));
				projectEntry.setDemandNr(caseNumber);
			}
			if (projectEntry.getDemandNr() != null)
				projectEntry.setDemandNr(caseNumber);
			
			projectEntry.setEntryType("BLOCADE");
			projectEntry.setGross(costPln);
			projectEntry.setMtime(new java.util.Date());
			
			if (!((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(dicId.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_ACCOUNTNUMBER")).getSelectedOptions().get(0).equals(""))
				projectEntry.setCostKindId(new Integer(((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(dicId.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_ACCOUNTNUMBER")).getSelectedOptions().get(0)));
			if (DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(dicId.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_AMOUNT") != null)
				projectEntry.setGross(new BigDecimal(DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(dicId.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_AMOUNT").toString().replace(" ", "")));
			if (fm.getValue("DESCRIPTION") != null && !fm.getValue("DESCRIPTION").equals(""))
				projectEntry.setDescription(fm.getValue("DESCRIPTION").toString());
			else
				projectEntry.setDescription(" - ");
			
			if (projectEntry.getEntryNumber() == null)
			{
				entryNumber = getEntryNumber(caseNumber, stat, result, entryNumber);
				projectEntry.setEntryNumber(caseNumber + "/"+ entryNumber);
			}
			
			projectEntry.setCostDate(new java.util.Date());
			project.addEntry(projectEntry);
		}
		
		List<ProjectEntry> projectEntries = ProjectEntry.findByDocNr(docId);

		for (ProjectEntry entry : projectEntries)
		{
			if (!dictionaryIds.contains(entry.getCostId()))
				entry.delete();
		}
	}

	private Integer getEntryNumber(String caseNumber, Statement stat, ResultSet result, Integer entryNumber) throws SQLException
	{
		result = stat.executeQuery("SELECT MAX(CAST(SUBSTRING(REVERSE(entryNumber),1,LEN(entryNumber)-LEN(demandNr)-1) AS INT)) FROM dsr_project_entry where demandNr = '" + caseNumber + "'");
		result.next();
		Integer lastEntryNumber = result.getInt(1);
		if (lastEntryNumber == null)
			return entryNumber;
		else if (lastEntryNumber > 0)
		{
			Integer newEntryNumber = lastEntryNumber + 1;
			return newEntryNumber;
		}
		else
		{
			return ++entryNumber;
		}
	}

}
