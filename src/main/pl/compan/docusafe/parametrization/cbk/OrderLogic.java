package pl.compan.docusafe.parametrization.cbk;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import com.google.common.collect.Maps;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class OrderLogic extends AbstractDocumentLogic
{
	public final static String DIVORPROJ = "div_or_proj_acc";
	public final static String CORRECTION = "correction";
	public final static String ACCEPTATION = "acceptation";
	
	private static Logger log = LoggerFactory.getLogger(OrderLogic.class);
	private static OrderLogic instance;
	
	public static OrderLogic getInstance()
	{
		if (instance == null)
			instance = new OrderLogic();
		return instance;
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException 
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Zapotrzebowania CBK");
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(document.getCtime()));
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(document.getCtime()));
		document.setFolder(folder);

		document.setTitle("Zapotrzeobwanie CBK - " + fm.getValue("NAZWA_PRZEDMIOTU"));
		document.setDescription("Zapotrzeobwanie CBK - " + fm.getValue("NAZWA_PRZEDMIOTU"));
	}
	
	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn() + "_DOCUMENT_READ", ObjectPermission.GROUP, "Zapotrzebowanie CBK - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Zapotrzebowanie CBK zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn() + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Zapotrzebowanie CBK - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Zapotrzebowanie CBK zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn() + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Zapotrzebowanie CBK - usuwanie"));
		this.setUpPermission(document, perms);
	}
	
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		Map<String, Object> toReload = Maps.newHashMap();
		for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
		{
			if (f.getDefaultValue() != null)
			{
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
			}
		}
		log.info("toReload = {}", toReload);
		fm.reloadValues(toReload);
	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn)
	{
		boolean assigned = false;
		try
		{
			log.info("DokumentKind: {}, DokumentId: {}', acceptationCn: {}, enumCn: {}", doc.getDocumentKind().getCn(), doc.getId(), accpetionCn, enumCn);

			if (accpetionCn.equals(DIVORPROJ))
				assigned = assigneToDivOrProj(assignable, openExecution);
			else if (accpetionCn.equals(CORRECTION))
			{
				assignable.addCandidateUser(doc.getAuthor());
				assigned = true;
			}
		}
		catch (Exception ee)
		{
			log.error(ee.getMessage(), ee);
			return false;
		}
		return assigned;
	}

	public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		log.info("DokumentKind: {}, DocumentId: {}, Values from JavaScript: {}", fm.getDocumentKind().getCn(), fm.getDocumentId(), values);

		if (values.get("DWR_KWOTA_BRUTTO").getData() != null)
		{
			BigDecimal bruttoZl = values.get("DWR_KWOTA_BRUTTO").getMoneyData().setScale(2);
			String kurs = Docusafe.getAdditionProperty("kurs_euro") != null ? Docusafe.getAdditionProperty("kurs_euro") : "3.839";
			BigDecimal euro = new BigDecimal(kurs).setScale(2, RoundingMode.DOWN);
			euro = bruttoZl.divide(euro, 2, RoundingMode.DOWN).setScale(2).divide(new BigDecimal("1.23"), RoundingMode.HALF_UP).setScale(2);
			values.get("DWR_CONTRACTEVALUE").setMoneyData(euro);
		}
		return null;
	}

	public void specialSetDictionaryValues(Map<String, ?> dockindFields)
	{
		log.info("Special dictionary values: {}", dockindFields);
		if (dockindFields.keySet().contains("SUG_CONTRACTOR_DICTIONARY"))
		{
			Map<String, FieldData> m = (Map<String, FieldData>) dockindFields.get("SUG_CONTRACTOR_DICTIONARY");
			m.put("SUG_CONTRACTOR_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			m.put("SUG_CONTRACTOR_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
		else if (dockindFields.keySet().contains("CONTRACTOR_DICTIONARY"))
		{
			Map<String, FieldData> m = (Map<String, FieldData>) dockindFields.get("CONTRACTOR_DICTIONARY");
			m.put("CONTRACTOR_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			m.put("CONTRACTOR_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}

	public void addSpecialInsertDictionaryValues(String dictionaryName, Map<String, FieldData> values)
	{
		log.info("Before Insert - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.contains("CONTRACTOR"))
		{
			values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}

	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		log.info("Before Search - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.contains("CONTRACTOR"))
		{
			values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}
	
	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- OrderLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		try
		{
			/*Document doc = Document.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			
			// ustawienie Konrahenta
			setContractor(fm, values);
			
			// ustawienie list porjektow
			setProjects(fm, values);
			
			// uzytkownik generujący
			values.put("USER", DSApi.context().getDSUser());
			
			// data generowani
			values.put("DATE", DateUtils.formatCommonDate(new java.util.Date()).toString());
			
			//akceptacje
			for (DocumentAcceptance docAcc : DocumentAcceptance.find(docId))
				values.put(docAcc.getAcceptanceCn(), DSUser.safeToFirstnameLastname(docAcc.getUsername()));*/
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			FieldsManager fm = dockind.getFieldsManager(id);
			
			// Ustawienie list projektow z ktorych pokyrwana jest faktura
			setProjectsNumberList(fm, params);
			
			// Ustawienie statusy dokumentu
			params.setStatus(fm.getValue("STATUS") != null ? (String) fm.getValue("STATUS") : null);
			
			// Ustawienie nuemru sprawy jako numeru dokumentu
			params.setDocumentNumber(fm.getValue("NRWNIOSKU") != null ? (String) fm.getValue("NRWNIOSKU") : null);
			
			// ustawienie Nazwy kontrahenta
			if (fm.getKey("CONTRACTOR") != null)
			{
				String organizator = (String) DwrDictionaryFacade.dictionarieObjects.get("CONTRACTOR").getValues(fm.getKey("CONTRACTOR").toString()).get("CONTRACTOR_ORGANIZATION");
				params.setAttribute(organizator != null ? organizator : "", 1);
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return params;
	}

	private boolean assigneToDivOrProj(Assignable assignable, OpenExecution openExecution) throws NumberFormatException, EdmException
	{
		String centrumId = openExecution.getVariable("costCenterId").toString();
		String userName = Project.find(Long.valueOf(centrumId)).getProjectManager();
		assignable.addCandidateUser(userName);
		return true;
	}

	private void setProjectsNumberList(FieldsManager fm, TaskListParams params)
	{
		try
		{
			String projectsNumerList = "";
			List<Long> ids = (List<Long>) fm.getKey("DSG_CENTRUM_KOSZTOW_FAKTURY");
			Iterator itr = ids.iterator();
			while (itr.hasNext())
			{
				String projectId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(itr.next().toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMID")).getSelectedOptions().get(0);
				if (projectId != null && !projectId.equals(""))
				{
					projectsNumerList = Project.find(Long.valueOf(projectId)).getNrIFPAN();
					if (itr.hasNext())
					{
						projectsNumerList += "...";
						break;
					}
				}
			}
			params.setAttribute(projectsNumerList, 0);
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
	}

	private void setProjects(FieldsManager fm, Map<String, Object> values)
	{
		try
		{
			String projectsNumerList = "";
			List<Long> ids = (List<Long>) fm.getKey("DSG_CENTRUM_KOSZTOW_FAKTURY");
			Iterator itr = ids.iterator();
			while (itr.hasNext())
			{
				String pojId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(itr.next().toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMID")).getSelectedOptions().get(0);
				String nrIfpan = Project.find(Long.valueOf(pojId)).getNrIFPAN();
				if (!projectsNumerList.contains(nrIfpan))
				{
					if (projectsNumerList.length() > 0)
						projectsNumerList += ", ";
					projectsNumerList += Project.find(Long.valueOf(pojId)).getNrIFPAN();
				}
			}
			values.put("PROJECTS", projectsNumerList);
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
	}

	private void setContractor(FieldsManager fm, Map<String, Object> values)
	{
		try
		{
			Long personId = (Long) fm.getKey("CONTRACTOR");
			Person oragnization = Person.find(personId.longValue());
			values.put("CONTRACTOR", oragnization);
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
	}
}
