package pl.compan.docusafe.parametrization.cbk;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import com.google.common.collect.Maps;

public class InvoiceOrderLogic extends AbstractDocumentLogic
{
	private static InvoiceOrderLogic instance;
	private static Logger log = LoggerFactory.getLogger(InvoiceOrderLogic.class);
	
	public static InvoiceOrderLogic getInstance()
	{
		if (instance == null)
			instance = new InvoiceOrderLogic();
		return instance;
	}

	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
	{
		// suma kwot centrow kosztow musi byc rowna kwocie brutto faktury
		Object o = Document.find(documentId).getFieldsManager().getKey("STATUS");
		if (fieldValues.get("KWOTA_BRUTTO") != null && fieldValues.get("DSG_CENTRUM_KOSZTOW_FAKTURY") != null && o != null && ((Integer) o).equals(5))
			checkSumCosts(fieldValues);
			
		// jezeli nie zostal wybrany rodzaj majatku trwalego system automatycznie uzupelnia pole nr inwentarzowy wartoscia "Nie dotyczy"
		if (fieldValues.get("DSG_CENTRUM_KOSZTOW_FAKTURY") != null)
			setNrInwentarz(fieldValues);
	}

	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		log.info("DokumentKind: {}, DocumentId: {}, Values from JavaScript: {}", fm.getDocumentKind().getCn(), fm.getDocumentId(), values);

		try
		{
			// automatycznie wyliczanie kwoty brutto jako sumy wartosci pozycji kosztowych - nie wiem po co to jest;/??
			if (values.get("DWR_DSG_CENTRUM_KOSZTOW_FAKTURY").getDictionaryData() != null)
				setBrutto(values);
			
			// automatycznie wylcizanie kwoty netto na podstawie wzor: KwotaWWalucie*Kurs
			if (values.get("DWR_KWOTA_W_WALUCIE").getMoneyData()!= null && values.get("DWR_KURS").getData()!= null)
				setNetto(values);

			// autoamtycznie wylcizenie kwoty brutto wg wzoru: netto+vat
			if (values.get("DWR_KWOTA_NETTO") != null && values.get("DWR_KWOTA_NETTO").getData() != null)
				setBruttoAsNettoPlusVat(values);
			
			// uzupelnienie centrom koszto faktury na podstawie podlaczonego zapotrzebowania
			if (values.get("DWR_ZAPOTRZEBOWANIE") != null)
				setCostCenters(values);
			
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return null;
	}

	
	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn() + "_DOCUMENT_READ", ObjectPermission.GROUP, "Faktura kosztowa/Zapotrzebowanie - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Faktura kosztowa/Zapotrzebowanie zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn() + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Faktura kosztowa/Zapotrzebowanie - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Faktura kosztowa/Zapotrzebowanie zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn() + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Faktura kosztowa/Zapotrzebowanie - usuwanie"));
		this.setUpPermission(document, perms);
	}

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Faktury kosztowe CBK powi�zane z Zapotrzebowaniem CBK");
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(fm.getValue("DATA_WYSTAWIENIA")));
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(fm.getValue("DATA_WYSTAWIENIA")));
		document.setFolder(folder);

		document.setTitle("Faktura kosztowa CBK nr " + String.valueOf(fm.getValue("NUMER_FAKTURY")) + " powi�zana z Zapotrzebowaniem CBK" );
		document.setDescription("Faktura kosztowa CBK nr " + String.valueOf(fm.getValue("NUMER_FAKTURY")) + " powi�zana z Zapotrzebowaniem CBK" );
	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn)
	{
		boolean assigned = false;
		try
		{
			log.info("DokumentKind: {}, DokumentId: {}', acceptationCn: {}, enumCn: {}", doc.getDocumentKind().getCn(), doc.getId(), accpetionCn, enumCn);
			if (accpetionCn.equals("ososba_zamawiajaca"))
			{
				ArrayList<Long> dictionaryIds = (ArrayList<Long>) doc.getFieldsManager().getKey("ZAPOTRZEBOWANIE");
				for (Long di : dictionaryIds)
					assignable.addCandidateUser(OfficeDocument.find(di).getAuthor());
				assigned = true;
			}
		}
		catch (Exception ee)
		{
			log.error(ee.getMessage(), ee);
		}
		return assigned;
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- InvoiceOrder : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}
	
	public void specialSetDictionaryValues(Map<String, ?> dockindFields)
	{
		log.info("Special dictionary values: {}", dockindFields);
		if (dockindFields.keySet().contains("CONTRACTOR_DICTIONARY"))
		{
			Map<String, FieldData> m = (Map<String, FieldData>) dockindFields.get("CONTRACTOR_DICTIONARY");
			m.put("DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			m.put("ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}

	public void addSpecialInsertDictionaryValues(String dictionaryName, Map<String, FieldData> values)
	{
		log.info("Before Insert - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.contains("CONTRACTOR") && !values.containsKey("DISCRIMINATOR"))
		{
			values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}

	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		log.info("Before Search - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.contains("CONTRACTOR"))
		{
			values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}

	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			FieldsManager fm = dockind.getFieldsManager(id);
			
			// Ustawienie statusy dokumentu
			params.setStatus(fm.getValue("STATUS") != null ? (String) fm.getValue("STATUS") : null);
			
			// Ustawienie list projektow z ktorych pokyrwana jest faktura
			setProjectsNumberList(fm, params);
			
			// Ustawienie numeru faktury jako numery dokumentu
			params.setDocumentNumber(fm.getValue("NUMER_FAKTURY") != null ? (String) fm.getValue("NUMER_FAKTURY") : null);
			
			// Ustawienie Nazwy kontraktora
			if (fm.getKey("CONTRACTOR") != null)
			{
				String organizator = (String) DwrDictionaryFacade.dictionarieObjects.get("CONTRACTOR").getValues(fm.getKey("CONTRACTOR").toString()).get("CONTRACTOR_ORGANIZATION");
				params.setAttribute(organizator != null ? organizator : "", 1);
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return params;
	}

	private void setContractor(FieldsManager fm, Map<String, Object> values)
	{
		try
		{
			Long personId = (Long) fm.getKey("CONTRACTOR");
			Person oragnization = Person.find(personId.longValue());
			values.put("CONTRACTOR", oragnization);
		}
		catch (EdmException e)
		{
			log.warn(e.getMessage(), e);
		}
	}

	private void setProjectsNumberList(FieldsManager fm, TaskListParams params)
	{
		try
		{
			String projectsNumerList = "";
			List<Long> ids = (List<Long>) fm.getKey("DSG_CENTRUM_KOSZTOW_FAKTURY");
			Iterator itr = ids.iterator();
			while (itr.hasNext())
			{
				String projectId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(itr.next().toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMID")).getSelectedOptions().get(0);
				if (projectId != null && !projectId.equals(""))
				{
					projectsNumerList = Project.find(Long.valueOf(projectId)).getNrIFPAN();
					if (itr.hasNext())
					{
						projectsNumerList += "...";
						break;
					}
				}
			}
			params.setAttribute(projectsNumerList, 0);
		}
		catch (EdmException e)
		{
			log.warn(e.getMessage(), e);
		}
	}
	
	private void setNrInwentarz(Long di, String nrInwentarzowy)
	{
		Statement stat = null;
		try
		{
			stat = DSApi.context().createStatement();
			int a = stat.executeUpdate("UPDATE DSG_CENTRUM_KOSZTOW_FAKTURY SET INWENTARZ = '" + nrInwentarzowy + "' where ID = " + di);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			DSApi.context().closeStatement(stat);
		}
	}
	
	private void setNrInwentarz(Map<String, ?> fieldValues)
	{
		List<Long> dictionaryIds = (ArrayList<Long>)fieldValues.get("DSG_CENTRUM_KOSZTOW_FAKTURY");
		// Je�eli dokument zawiera pozycje kosztowe
		if (dictionaryIds != null)
		{
			try 
			{
				String majatekId = null;
			
			
				for (Long di : dictionaryIds)
				{
					// pobranie id_majatku z ktorego pokrywany jest koszt
					majatekId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(di.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_ACCEPTINGCENTRUMID")).getSelectedOptions().get(0);
					if (majatekId.equals("0"))
					{
						setNrInwentarz(di,"Nie dotyczy");
					}
				}
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
			}
		}
	}

	private void checkSumCosts(Map<String, ?> fieldValues) throws EdmException
	{
		BigDecimal brutto = (BigDecimal) fieldValues.get("KWOTA_BRUTTO");
		BigDecimal sumCentrum = new BigDecimal(0);
		ArrayList<Long> dictionaryIds = (ArrayList<Long>) fieldValues.get("DSG_CENTRUM_KOSZTOW_FAKTURY");
		BigDecimal oneAmount = null;
		for (Long di : dictionaryIds)
		{
			oneAmount = new BigDecimal((Double) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(di.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_AMOUNT"));
			sumCentrum = sumCentrum.add(oneAmount);
		}
		if (!brutto.setScale(2, RoundingMode.HALF_UP).equals(sumCentrum.setScale(2, RoundingMode.HALF_UP)))
			throw new EdmException("Suma kwot centr�w koszt�w musi by� r�wna kwocie brutto faktury");
		
	}
	
	private void setCostCenters(Map<String, FieldData> values) throws EdmException
	{
		Map<String, FieldData> dvalues = values.get("DWR_ZAPOTRZEBOWANIE").getDictionaryData();
		String idsss = "";
		int i = 1;
		for (String o : dvalues.keySet())
		{
			if (o.contains("_ID_"))
			{
				Long idZap = new Long(dvalues.get(o).getData().toString());
				Statement stat = null;
				ResultSet result = null;
				try
				{
					DSApi.openAdmin();
					stat = DSApi.context().createStatement();
					result = stat.executeQuery("SELECT FIELD_VAL FROM dsg_cbk_order_multiple_value where (DOCUMENT_ID = " + idZap + ") AND (FIELD_CN ='DSG_CENTRUM_KOSZTOW_FAKTURY')");
					while (result.next())
					{
						Integer id = result.getInt(1);
						if (i == 1)
							idsss += id.toString();
						else
							idsss = idsss + "," + id.toString();
						++i;
					}
				}
				catch (Exception ie)
				{
					log.error(ie.getMessage(), ie);
				}
				finally
				{
					DSApi.context().closeStatement(stat);
					DSApi.close();
				}
			}
		}
		values.put("DWR_DSG_CENTRUM_KOSZTOW_FAKTURY", new FieldData(Field.Type.STRING, idsss));
	}

	private void setBruttoAsNettoPlusVat(Map<String, FieldData> values)
	{
		BigDecimal netto = values.get("DWR_KWOTA_NETTO").getMoneyData();
		if (values.get("DWR_VAT").getData() != null)
		{
			BigDecimal brutto = values.get("DWR_VAT").getMoneyData();
			brutto = brutto.add(netto).setScale(2, RoundingMode.HALF_UP);
			values.get("DWR_KWOTA_BRUTTO").setMoneyData(brutto.setScale(2, RoundingMode.HALF_UP));
		}
	}

	private void setNetto(Map<String, FieldData> values)
	{
		BigDecimal kwotaWWalucie = values.get("DWR_KWOTA_W_WALUCIE").getMoneyData();
		BigDecimal kurs = (BigDecimal)values.get("DWR_KURS").getData();
		BigDecimal netto = kwotaWWalucie.multiply(kurs).setScale(2, RoundingMode.HALF_UP);
		values.get("DWR_KWOTA_NETTO").setMoneyData(netto.setScale(2, RoundingMode.HALF_UP));
	}

	private void setBrutto(Map<String, FieldData> values)
	{
		BigDecimal sumCS = BigDecimal.ZERO;
		Map<String, FieldData> dvalues = values.get("DWR_DSG_CENTRUM_KOSZTOW_FAKTURY").getDictionaryData();
		for (String o : dvalues.keySet())
		{
			if (o.contains("AMOUNT") && dvalues.get(o).getMoneyData() != null)
				sumCS = sumCS.add(dvalues.get(o).getMoneyData());
		}
		if (!sumCS.equals(BigDecimal.ZERO))
			values.get("DWR_KWOTA_BRUTTO").setMoneyData(sumCS);
	}

}
