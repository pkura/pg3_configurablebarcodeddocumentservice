package pl.compan.docusafe.parametrization.ins;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** Asygnacja na uzytkownika wybranego na formularzu
 * (CN pola) lub w podanym polu.
 * @author Rafal Odoj
 * @date 2012-09-11 */
public class AsgnHandler_DSUserOnForm implements AssignmentHandler {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(AsgnHandler_DSUserOnForm.class);

    /** opcjonalnie podane pole typu DSUSER, kt�re nale�y sprawdzi� */ private String pole;

    public void assign(org.jbpm.api.task.Assignable assignable, OpenExecution openExecution) throws Exception {
        Long docId = Long.parseLong(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);

        String user = (pole==null) ? (doc.getFieldsManager().getStringKey("DSUSER")) : (doc.getFieldsManager().getStringKey(pole));
        if (user==null){
            log.info("nie okre�lono, kt�ry u�ytkownik ma wykona� nast�pne zadanie.");
            throw new IllegalStateException("nie okre�lono, kt�ry u�ytkownik ma wykona� nast�pne zadanie.");
        }

        Long userId = null;
        try{
            userId = Long.parseLong(user);
        } catch (NumberFormatException nfe){
            log.info("podane ID u�ytkownika (" +user+ ") nie da si� zamieni� na prawid�ow� liczb� ca�kowit�.");
            throw new IllegalStateException("podane ID u�ytkownika (" +user+ ") nie da si� zamieni� na prawid�ow� liczb� ca�kowit�.");
        }
        DSUser u = DSUser.findById(userId);

        log.info("Dekretacja na: " + u.getName());
        assignable.addCandidateUser(u.getName());
        Util_AHE.addToHistory(openExecution, doc, u.getName(), null, null, log);
    }

}
