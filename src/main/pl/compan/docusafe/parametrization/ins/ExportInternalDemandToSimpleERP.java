package pl.compan.docusafe.parametrization.ins;

import org.apache.commons.dbcp.BasicDataSource;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExportInternalDemandToSimpleERP
{
	private static final Logger log = LoggerFactory.getLogger(ExportInternalDemandToSimpleERP.class);

	static public void export(String xml, OfficeDocument document) throws EdmException
	{

		log.info("EXPORT internal demand xml: {}", xml);
		Connection con = null;
		CallableStatement call = null;
		try
		{
			String erpUser, erpPassword, erpUrl, erpDataBase, erpSchema, proc_export;
			erpUser = Docusafe.getAdditionProperty("erp.database.user");
			erpPassword = Docusafe.getAdditionProperty("erp.database.password");
			erpUrl = Docusafe.getAdditionProperty("erp.database.url");
			erpDataBase = Docusafe.getAdditionProperty("erp.database.dataBase");
			erpSchema = Docusafe.getAdditionProperty("erp.database.schema");
			// up_gsd_ksiegapodawcza
			proc_export = Docusafe.getAdditionProperty("erp.database.export.zapotrzebowanie");
			if (proc_export == null)
				proc_export = "api_zapdost_utworz";

			if (erpUser == null || erpPassword == null || erpUrl == null)
				throw new EdmException("B��d eksportu faktury, brak parametr�w po��czenia do bazy ");

			log.info("user: {}, password: {}, url: {}", erpUser, erpPassword, erpUrl);
			BasicDataSource ds = new BasicDataSource();
			ds.setDriverClassName(Docusafe.getDefaultDriver("sqlserver_2000"));
			ds.setUrl(erpUrl);
			ds.setUsername(erpUser);
			ds.setPassword(erpPassword);
			con = ds.getConnection();

			call = con.prepareCall("{call [" + erpDataBase + "].[" + erpSchema + "].[" + proc_export + "] (?,?,?,?,?)}");

			Integer return_value = null;
			BigDecimal zapdost_id = null, error_id = null;
            String zapdost_idm = null;
			String error_description = null;

            String xmlOut = null;
            Integer statusCode = null;
            String statusMessage = null;

            //xmlIn
            call.setObject(1, xml);
            //isResultset
            call.setInt(2, 0);
            //xmlOut
            call.registerOutParameter(3, Types.OTHER);
            //statusCode
            call.registerOutParameter(4, Types.OTHER);
            //statusMessage
            call.registerOutParameter(5, Types.OTHER);


			int resultNum = 0;

			while (true)
			{
				boolean queryResult = false;
				int rowsAffected;

				if (1 == ++resultNum)
				{
					try
					{
                        try{
						    queryResult = call.execute();
                        }catch (SQLException e){
                            log.error("B��d podczas wykonywania exportu wniosku: "+e);
                        }
                        xmlOut = call.getString(3);
                        statusCode = call.getInt(4);
                        String id = null;
                        String idm = null;
                        if (xmlOut.contains("<ID>"))
                            id = xmlOut.substring(xmlOut.indexOf("<ID>")+4,xmlOut.indexOf("</ID>"));
                        if (xmlOut.contains("<IDM>"))
                            idm = xmlOut.substring(xmlOut.indexOf("<IDM>")+5,xmlOut.indexOf("</IDM>"));
                        if(statusCode==0 && id != null && !id.equals("")){
                            zapdost_id = new BigDecimal(id);
                            zmapujIdentyfikatoryERP(zapdost_id.intValue(),document.getId());
                        }
                        if(statusCode==0 && idm != null && !idm.equals(""))
                            zapdost_idm = idm;

                        statusMessage = call.getString(5);

                        System.out.println("xmlOut : " + xmlOut);
                        System.out.println("statusCode : " + statusCode);
                        System.out.println("statusMessage : " + statusMessage);

//						return_value = call.getInt(1);
//						zapdost_id = call.getBigDecimal(3);
//						error_id = call.getBigDecimal(4);
//						error_description = call.getString(5);

						log.debug("EXPORT internal demand xml: return_value: {}, zapdost_id: {}, error_id: {}, error_description: {}", return_value, zapdost_id, error_id, error_description);



					}
					catch (Exception e)
					{
						// Process the error
						log.error("Result " + resultNum + " is an error: " + e.getMessage());
                        System.out.println(e);
						// When execute() throws an exception, it may just
						// be that the first statement produced an error.
						// Statements after the first one may have
						// succeeded. Continue processing results until
						// there
						// are no more.
						continue;
					}
				}
				else
				{
					try
					{
						queryResult = call.getMoreResults();
					}
					catch (Exception e)
					{
						// Process the error
						log.error("1 != ++resultNum: Result " + resultNum + " is an error: " + e.getMessage());

						// When getMoreResults() throws an exception, it
						// may just be that the current statement produced
						// an error.
						// Statements after that one may have succeeded.
						// Continue processing results until there
						// are no more.
						continue;
					}
				}

				if (queryResult)
				{
					ResultSet rs = call.getResultSet();

					// Process the ResultSet
					log.debug("Result " + resultNum + " is a ResultSet: " + rs);
					ResultSetMetaData md = rs.getMetaData();

					int count = md.getColumnCount();
					log.debug("<table border=1>");
					log.debug("<tr>");
					for (int i = 1; i <= count; i++)
					{
						log.debug("<th>");
						log.debug("GEtColumnLabeL: {}", md.getColumnLabel(i));
					}
					log.debug("</tr>");
					while (rs.next())
					{
						log.debug("<tr>");
						for (int i = 1; i <= count; i++)
						{
							log.debug("<td>");
							log.debug("Get.Object: {}", rs.getObject(i));
						}
						log.debug("</tr>");
					}
					log.debug("</table>");
					rs.close();
				}
				else
				{
					rowsAffected = call.getUpdateCount();

					// No more results
					if (-1 == rowsAffected)
					{
						--resultNum;
						break;
					}

					// Process the update count
					log.debug("Result " + resultNum + " is an update count: " + rowsAffected);
				}
			}

			log.debug("Done processing " + resultNum + " results");
			if (resultNum == 0)
			{

			}
			if (zapdost_id == null){
				throw new EdmException("Procedura importujaca wniosek zapotrzebowania do dostawcy (" + proc_export + ") do Simple.ERP nie zwrocila identyfikatora zapdost_id !!! " +
						"Zwr�cne wartosci z procedury: statusMessage: " + statusMessage);
            }else{
                //UZUPELNIENIE POLA Numer ERP
                document.getDocumentKind().setOnly(
                        document.getId(),
                        Collections.singletonMap("NRERP", zapdost_idm));

                //UZUPELNIENIE POLA ERP ID
                document.getDocumentKind().setOnly(
                        document.getId(),
                        Collections.singletonMap("ERPID", zapdost_id.intValue()));

                //DODANIE WNIOSKU DO OBSERWOWANYCH AUTORA
//                try{
//                    DSApi.context().watch(URN.create(document),document.getAuthor());
//                }catch (EdmException e){
//                    log.error("B��d podczas dodawania dokumentu do obserwowanych: "+e);
//                }

                Remark rem = new Remark("Export wniosku do SIMPLE.ERP pomy�lnie uko�czony. Identyfikator wniosku w SIMPLE.ERP to "+ zapdost_idm);
                document.addRemark(rem);
            }
			
			setUrl(con, erpDataBase, erpSchema, zapdost_id.intValue(), document);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException("B��d akcpetacji wniosku :" + e.getMessage());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					con.close();
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
					throw new EdmException("B��d akcpetacji wniosku :" + e.getMessage());
				}
			}
		}
	}

	private static void setUrl(Connection con, String erpDataBase, String erpSchema, Integer dokzak_id, OfficeDocument doc) throws DocumentNotFoundException, DocumentLockedException, AccessDeniedException, EdmException
	{
			
		try
		{
			String url = Docusafe.getBaseUrl() + createLink(doc);
			String nazwa = "Wniosek zapotrzebowania do dostawcy w Docusafe";
			//@bd_rezeracja_id numeric (10,0) ,
			//@nazwa varchar (256) ,
			//@url varchar (256)
			CallableStatement call = con.prepareCall("{call [" + erpDataBase + "].[" + erpSchema + "].[up_gsd_dodaj_zal_do_dokzak] (?,?,?)}");
			call.setInt(1, dokzak_id);
			call.setString(2, nazwa);
			call.setString(3, url);
			int resultNum = 0;
	
			while (true)
			{
				boolean queryResult;
				int rowsAffected;
	
				if (1 == ++resultNum)
				{
					try
					{
						queryResult = call.execute();
					
						log.debug("EXPORT Accepting dokzak_id (dokzak_id) : {}", dokzak_id);
						
					}
					catch (Exception e)
					{
						// Process the error
						log.error("Result " + resultNum + " is an error: " + e.getMessage());
	
						// When execute() throws an exception, it may just
						// be that the first statement produced an error.
						// Statements after the first one may have
						// succeeded. Continue processing results until
						// there
						// are no more.
						continue;
					}
				}
				else
				{
					try
					{
						queryResult = call.getMoreResults();
					}
					catch (Exception e)
					{
						// Process the error
						log.error("1 != ++resultNum: Result " + resultNum + " is an error: " + e.getMessage());
	
						// When getMoreResults() throws an exception, it
						// may just be that the current statement produced
						// an error.
						// Statements after that one may have succeeded.
						// Continue processing results until there
						// are no more.
						continue;
					}
				}
	
				if (queryResult)
				{
					ResultSet rs = call.getResultSet();
	
					// Process the ResultSet
					log.debug("Result " + resultNum + " is a ResultSet: " + rs);
					ResultSetMetaData md = rs.getMetaData();
	
					int count = md.getColumnCount();
					log.debug("<table border=1>");
					log.debug("<tr>");
					for (int i = 1; i <= count; i++)
					{
						log.debug("<th>");
						log.debug("GEtColumnLabeL: {}", md.getColumnLabel(i));
					}
					log.debug("</tr>");
					while (rs.next())
					{
						log.debug("<tr>");
						for (int i = 1; i <= count; i++)
						{
							log.debug("<td>");
							log.debug("Get.Object: {}", rs.getObject(i));
						}
						log.debug("</tr>");
					}
					log.debug("</table>");
					rs.close();
				}
				else
				{
					rowsAffected = call.getUpdateCount();
	
					// No more results
					if (-1 == rowsAffected)
					{
						--resultNum;
						break;
					}
	
					// Process the update count
					log.debug("Result " + resultNum + " is an update count: " + rowsAffected);
				}
			}
	
			log.debug("Done processing " + resultNum + " results");
			if (resultNum == 0)
			{
	
			}
			
	
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException("B��d wywloania procedury wniosku :" + e.getMessage());
		}		
	}

	private static String createLink(Document doc) throws DocumentNotFoundException, DocumentLockedException, AccessDeniedException, EdmException
	{
	
	    Long docId = doc.getId();
	    StringBuilder b = new StringBuilder();
	    log.info("PageContext: {}", Docusafe.getPageContext());
	    String pageContext = Docusafe.getPageContext();
	    if (doc instanceof InOfficeDocument)
	    {
	        if(pageContext.length() > 0)
	            b.append("/");
	        b.append(Docusafe.getPageContext() + "/office/incoming/document-archive.action?documentId=").append(docId);
	    }
	    else if (doc instanceof OutOfficeDocument)
	    {
	        if (((OutOfficeDocument) doc).isInternal())
	        {
	            if(pageContext.length() > 0)
	                b.append("/");
	            b.append(Docusafe.getPageContext() + "/office/internal/document-archive.action?documentId=").append(docId);
	        }
	        else
	        {
	            if(pageContext.length() > 0)
	                b.append("/");
	            b.append(Docusafe.getPageContext() + "/office/outgoing/document-archive.action?documentId=").append(docId);
	        }
	    }
	    else
	    {
	        if(pageContext.length() > 0)
	            b.append("/");
	        b.append(Docusafe.getPageContext() + "/repository/edit-dockind-document.action?id=").append(docId);
	    }
	    log.info("Link: {}", b.toString());
	    return b.toString();
	}

    private static void zmapujIdentyfikatoryERP(Integer erpId, Long documentId) {
        try {

            List lista = pobierzZWidokuPozycjeDlaDokumentu(erpId);
            if (lista != null){
                zmapujPobraneIdentyfikatory(documentId, lista);
            }

        } catch (Exception e) {
            log.error("B��d podczas mapowania: " + e);
        }
    }

    private static List pobierzZWidokuPozycjeDlaDokumentu(Integer erpId) throws EdmException, SQLException {
        PreparedStatement ps = DSApi.context().prepareStatement("SELECT id " +
                "  FROM zapdostpoz_v " +
                "  where zapdost_id = ? ");
        ps.setInt(1,erpId);

        List listaPozycji = new ArrayList();
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            listaPozycji.add(rs.getLong(1));
        }

        if (listaPozycji.size()>0)
            return listaPozycji;
        else
            return null;
    }

    private static void zmapujPobraneIdentyfikatory(Long docId, List<Long> lista) throws EdmException, SQLException {
        for (Long poz : lista){
            PreparedStatement ps = DSApi.context().prepareStatement("INSERT INTO INS_WniosZapDost_Dict " +
                    "( DOCUMENT_ID , FIELD_CN, FIELD_VAL) " +
                    "VALUES (?, ?, ?)");
            ps.setLong(1,docId);
            ps.setString(2, "REALIZACJAPOZYCJI");
            ps.setLong(3, poz);

            ps.executeUpdate();

            ps.close();
        }
    }
}
