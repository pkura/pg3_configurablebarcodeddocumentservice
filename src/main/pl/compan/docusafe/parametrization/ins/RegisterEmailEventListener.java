package pl.compan.docusafe.parametrization.ins;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Date;

public class RegisterEmailEventListener extends AbstractEventListener {
    private static final Logger log = LoggerFactory.getLogger(RegisterEmailEventListener.class);

    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        try{
            OfficeDocument document = Jbpm4Utils.getDocument(execution);
            FieldsManager fm = document.getFieldsManager();

            String username = DSUser.findById(fm.getLongKey("REALIZUJACY")).getName();
            String komorka = fm.getStringValue("KOMORKA");
            Date dataOczekiwana = (Date) fm.getValue("DATAZAPOTRZEBOWANIA");
            EventFactory.registerEvent("immediateTrigger", "InternalDemandNotifier",
                    username + "|" + fm.getStringValue("NRERP")+"|"+
                            komorka+"|"+ DateUtils.formatJsDate(dataOczekiwana), new Date(),
                    new Date());
        }catch(Exception e){
            log.error("B��d podczas wysy�ania maila do realizotora wniosku zapotrzebowania do dostawcy. "+e);
        }

    }
}
