package pl.compan.docusafe.parametrization.ins;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;

import java.util.Map;

public class MpkDictLogic extends DwrDictionaryBase {
    @Override
    public long add(Map<String, FieldData> values) throws EdmException {
        //sprawdzenie czy przy wyborze projektu wybrane s�
        //projekt, budzet, etap i zasob
        if (values.get("KONTRAKT").getData() != null && !values.get("KONTRAKT").getData().equals("")){
            if (values.get("BUDZET").getData() == null || values.get("BUDZET").getData().equals("")){
                throw new EdmException("Przy rozliczaniu projekt�w nale�y wype�ni� pola: Projekt, Bud�et projektu, Etap i Zas�b.");
            }
            if (values.get("FAZA").getData() == null || values.get("FAZA").getData().equals("")){
                throw new EdmException("Przy rozliczaniu projekt�w nale�y wype�ni� pola: Projekt, Bud�et projektu, Etap i Zas�b.");
            }
            if (values.get("ZASOB").getData() == null || values.get("ZASOB").getData().equals("")){
                throw new EdmException("Przy rozliczaniu projekt�w nale�y wype�ni� pola: Projekt, Bud�et projektu, Etap i Zas�b.");
            }
        }
        return super.add(values);
    }

    @Override
    public int update(Map<String, FieldData> values) throws EdmException {
        //sprawdzenie czy przy wyborze projektu wybrane s�
        //projekt, budzet, etap i zasob

        if (values.get("NETTO").getData() == null || values.get("NETTO").getData().equals(""))
            throw new EdmException("Przypisz do pozycji bud�etowej kwot� netto.");

        if (values.get("KONTRAKT").getData() != null && !values.get("KONTRAKT").getData().equals("")){
            if (values.get("BUDZET").getData() == null || values.get("BUDZET").getData().equals("")){
                throw new EdmException("Przy rozliczaniu projekt�w nale�y wype�ni� pola: Projekt, Bud�et projektu, Etap i Zas�b.");
            }
            if (values.get("FAZA").getData() == null || values.get("FAZA").getData().equals("")){
                throw new EdmException("Przy rozliczaniu projekt�w nale�y wype�ni� pola: Projekt, Bud�et projektu, Etap i Zas�b.");
            }
            if (values.get("ZASOB").getData() == null || values.get("ZASOB").getData().equals("")){
                throw new EdmException("Przy rozliczaniu projekt�w nale�y wype�ni� pola: Projekt, Bud�et projektu, Etap i Zas�b.");
            }
        }
        return super.update(values);
    }
}
