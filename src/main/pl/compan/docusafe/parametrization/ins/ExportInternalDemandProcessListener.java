package pl.compan.docusafe.parametrization.ins;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.builder.SAXOMBuilder;
import org.jbpm.api.listener.EventListenerExecution;
import pl.com.simple.simpleerp.*;
import pl.com.simple.simpleerp.ObjectTypeManager;
import pl.com.simple.simpleerp.zapdost.ObjectFactory;
import pl.com.simple.simpleerp.zapdost.ZapdostTYPE;
import pl.com.simple.simpleerp.zapdost.ZapdostpozTYPE;
import pl.com.simple.simpleerp.zapdost.ZapdostpozycjeTYPE;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ins.entities.PozycjaZapotrzebowania;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

public class ExportInternalDemandProcessListener extends AbstractEventListener
{
	private static final Logger log = LoggerFactory.getLogger(ExportInternalDemandProcessListener.class);

    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        OfficeDocument document = Jbpm4Utils.getDocument(execution);
        FieldsManager fm = document.getFieldsManager();
        try
        {
            JAXBContext jaxbContext = JAXBContext.newInstance("pl.com.simple.simpleerp.zapdost");
            Marshaller marshaller = jaxbContext.createMarshaller();
            ObjectFactory factory = new ObjectFactory();
            ZapdostTYPE zapdostTYPE = factory.createZapdostTYPE();


            //pracownik_id    NN
            Integer prac = fm.getIntegerKey("PRACOWNIK");
            if (prac == null)
                throw new EdmException("Brak wybranego pracownika.");
            BigDecimal idPracownika = new BigDecimal(prac);
            IDNIDTYPE pracownikId = ObjectTypeManager.createIDNIDTYPE(factory, IDENTITYNATURETYPE.N, idPracownika, null);
            zapdostTYPE.setPracownikId(pracownikId);

            //komzapotrz_id   NN
            Integer kom = fm.getIntegerKey("KOMORKA");
            if (kom == null)
                throw new EdmException("Brak wybranej kom�rki.");
            BigDecimal idKomZapotrzebowania = new BigDecimal(kom);
            IDNIDTYPE komzapotrzId = ObjectTypeManager.createIDNIDTYPE(factory, IDENTITYNATURETYPE.N, idKomZapotrzebowania,null);
            zapdostTYPE.setKomzapotrzId(komzapotrzId);

            //kategobr_id
            IDNIDTYPE kategobrId = ObjectTypeManager.createIDNIDTYPE(factory, IDENTITYNATURETYPE.N, new BigDecimal(1),null);
            zapdostTYPE.setKategobrId(kategobrId);

            //tabkurs_id
            IDNIDTYPE tabkursId = ObjectTypeManager.createIDNIDTYPE(factory, IDENTITYNATURETYPE.T, null,"UNI2013");
            zapdostTYPE.setTabkursId(tabkursId);

            //uzytk_id
            String idnUzytk = Docusafe.getAdditionProperty("erp.operator.user.idn");
            if (idnUzytk == null || idnUzytk.equals(""))
                throw new EdmException("Brak skonfigurowanego operatora ERP.");

            IDNIDTYPE uzytkId = ObjectTypeManager.createIDNIDTYPE(factory,IDENTITYNATURETYPE.T,null, idnUzytk);
            zapdostTYPE.setUzytkId(uzytkId);

            //typ_zapdost_id   NN
            Integer typ = fm.getIntegerKey("TYP");
            if (typ == null)
                throw new EdmException("Brak wybranego typu dokumentu.");
            BigDecimal idTypZapDost = new BigDecimal(typ);
            IDNIDTYPE typZapDostId = ObjectTypeManager.createIDNIDTYPE(factory, IDENTITYNATURETYPE.N, idTypZapDost, null);
            zapdostTYPE.setTypZapdostId(typZapDostId);

            //waluta_id
            SYMBOLWALUTYIDTYPE symbolWaluty = factory.createSYMBOLWALUTYIDTYPE();
            symbolWaluty.setSymbolwaluty(fm.getStringValue("WALUTA"));
            symbolWaluty.setType(IDENTITYNATURETYPE.T);
            zapdostTYPE.setWalutaId(symbolWaluty);

            //oddzial_id
            BigDecimal idOddzial = new BigDecimal(fm.getIntegerKey("ODZIAL"));
            IDNIDTYPE oddzialId = ObjectTypeManager.createIDNIDTYPE(factory,IDENTITYNATURETYPE.N, idOddzial,null);
            zapdostTYPE.setOddzialId(oddzialId);

            //datdok                     NN
            zapdostTYPE.setDatdok((Date) fm.getValue("DATADOKUMENTU"));

            //datoper                    NN
            zapdostTYPE.setDatoper((Date) fm.getValue("DATAZAPOTRZEBOWANIA"));

            //kurs
            BigDecimal kurs = new BigDecimal((Double) fm.getValue("KURS")).setScale(4,BigDecimal.ROUND_HALF_UP);
            zapdostTYPE.setKurs(kurs);

            //wartdok
//            BigDecimal wartDok= new BigDecimal((Double) fm.getValue("SUMA"));
//            zapdostTYPE.setWartdok(wartDok);
            ZapdostpozycjeTYPE zapdostpozycjeTYPE = factory.createZapdostpozycjeTYPE();
            List pozycjeZapotrzebowania = (List) fm.getValue("ZAMOWIENIAPOZ");
            if (pozycjeZapotrzebowania == null)
                throw new EdmException("Brak pozycji wniosku.");
            List pozycjeBudzetowe = (List) fm.getValue("BUDZET_POZ");
            int i = 0;
            for(Object pozZapdost : pozycjeZapotrzebowania){
                Map mapaPozZapdost = (Map) pozZapdost;
                Map mapaPozBudzetowej = null;
                i++;
                ////
                EnumValues pozycjaIdE = (EnumValues) mapaPozZapdost.get("ZAMOWIENIAPOZ_POZYCJA_BUDZETU");
                Integer pozycjaId = (Integer) mapaPozZapdost.get("ZAMOWIENIAPOZ_ID");
                PozycjaZapotrzebowania pozZapotrzebowania = PozycjaZapotrzebowania.findById(pozycjaId.longValue());
                //wyci�gni�cie odpowiedniej pozycji budzetowej
                if (pozZapotrzebowania.getPozycjaBudzetu() != null)
                    mapaPozBudzetowej = getBudgetPositionForDemandPosition(pozycjeBudzetowe, pozZapotrzebowania.getPozycjaBudzetu().intValue());
                else
                    mapaPozBudzetowej = null;

                //ustawienie obiektu dokzakPoz
                zapdostpozycjeTYPE.getZapdostpoz().add(getZapdostPozElementFromPosiotion(factory, mapaPozBudzetowej, mapaPozZapdost, i));
            }
            zapdostTYPE.setZapdostpozycje(zapdostpozycjeTYPE);
//sys_dok_zalaczniki

            //tworzenie XMLa
            SAXOMBuilder builder = new SAXOMBuilder();

            marshaller.marshal(factory.createZapdost(zapdostTYPE), builder);
            OMElement xmlElement= builder.getRootElement();
            if(xmlElement != null){
                log.error(xmlElement.toString());
                ExportInternalDemandToSimpleERP.export(xmlElement.toString(), document);
            }

        }catch (JAXBException e){
            log.error("B��d podczas tworzenia XML dla obiektu faktury kosztowej: "+e);
            throw new EdmException("B��d podczas tworzenia XML dla obiektu faktury kosztowej: "+e);
        }catch (NumberFormatException e){
            log.error("B��d formatu danych: "+e);
            throw new EdmException("B��d formatu danych: "+e);
        }catch (Exception e){
            log.error("B��d podczas exportu faktury kosztowej do SIMPLE.ERP: "+e);
            throw new EdmException("B��d podczas exportu faktury kosztowej do SIMPLE.ERP: "+e);
        }
    }

    private Map getBudgetPositionForDemandPosition(List pozycjeBudzetowe, Integer pozycjaBudzetu) {
        for (Object poz : pozycjeBudzetowe)
        {
            Map mapaPoz = (Map) poz;
            Integer idPozycji = (Integer) mapaPoz.get("BUDZET_POZ_ID");
            if(idPozycji.compareTo(pozycjaBudzetu) == 0)
                return mapaPoz;
        }
        return null;    }

    private ZapdostpozTYPE getZapdostPozElementFromPosiotion(ObjectFactory factory, Map mapaPozBudzetowej, Map mapaPozZapdost, int i) throws EdmException {
        ZapdostpozTYPE poz = factory.createZapdostpozTYPE();
        String pozycjeZapotrzebowaniaDictCnPrefix = "ZAMOWIENIAPOZ_";
        String pozycjeBudzetoweDictCnPrefix = "BUDZET_POZ_";

        //kategobr_id
        poz.setKategobrId(ObjectTypeManager.createIDNIDTYPE(factory,IDENTITYNATURETYPE.N,BigDecimal.ONE,null));

        //vatstaw_id
        EnumValues vatRateId = (EnumValues) mapaPozZapdost.get(pozycjeZapotrzebowaniaDictCnPrefix + "VAT");
        if (vatRateId.getSelectedOptions().get(0).equals(""))
            throw new EdmException("Brak wybranej stawki vat dla pozycji.");
        poz.setVatstawId(ObjectTypeManager.createIDSIDTYPE(factory, vatRateId));
        //jm_id
        EnumValues jmId = (EnumValues) mapaPozZapdost.get(pozycjeZapotrzebowaniaDictCnPrefix + "JM");
        if (jmId.getSelectedOptions().get(0).equals(""))
            throw new EdmException("Brak wybranej jednostki miary dla pozycji.");
        poz.setJmId(ObjectTypeManager.createIDNIDTYPE(factory, IDENTITYNATURETYPE.N, new BigDecimal(jmId.getSelectedOptions().get(0)), null));
        //wytwor_id
        EnumValues wytworId = (EnumValues) mapaPozZapdost.get(pozycjeZapotrzebowaniaDictCnPrefix + "PRODUKT");
        if (wytworId.getSelectedOptions().get(0).equals(""))
            throw new EdmException("Brak wybranego produktu dla pozycji.");
        poz.setWytworId(ObjectTypeManager.createIDMIDTYPE(factory, wytworId));
        //ilosc
        BigDecimal ilosc = (BigDecimal) mapaPozZapdost.get(pozycjeZapotrzebowaniaDictCnPrefix + "ILOSC");
        if (ilosc == null)
            throw new EdmException("Brak podanej ilo�ci dla pozycji wniosku");
        poz.setIlosc(ilosc);
        //cena
        BigDecimal cena = (BigDecimal) mapaPozZapdost.get(pozycjeZapotrzebowaniaDictCnPrefix + "CENA");
        if (cena == null)
            throw new EdmException("Brak podanej ceny jednostkowej");
        poz.setCena(cena);
        //cenwalbaz
        BigDecimal cenaWalbaz = (BigDecimal) mapaPozZapdost.get(pozycjeZapotrzebowaniaDictCnPrefix + "CENA_BAZ");
        if (cenaWalbaz == null)
            throw new EdmException("Brak podanej ceny jednostkowej w walucie");
        poz.setCenwalbaz(cenaWalbaz);
        //koszt
        BigDecimal koszt= (BigDecimal) mapaPozZapdost.get(pozycjeZapotrzebowaniaDictCnPrefix + "NETTO");
        if (koszt == null)
            throw new EdmException("Brak podanego kosztu pozycji");
        poz.setKoszt(koszt);
        //kosztwalbaz
        BigDecimal kosztWalbaz= (BigDecimal) mapaPozZapdost.get(pozycjeZapotrzebowaniaDictCnPrefix + "NETTO_BAZ");
        if (kosztWalbaz == null)
            throw new EdmException("Brak podanego kosztu pozycji w walucie bazowej");
        poz.setKosztwalbaz(kosztWalbaz);
        //nrpoz
        poz.setNrpoz(i);
        //uwagi
        String opis = (String) mapaPozZapdost.get(pozycjeZapotrzebowaniaDictCnPrefix + "OPIS");
        poz.setUwagi(opis);
        //datoper
        Date datOczek = (Date) mapaPozZapdost.get(pozycjeZapotrzebowaniaDictCnPrefix + "DATAOCZEKIWANA");
        if (datOczek== null)
            throw new EdmException("Brak podanej daty oczekiwanej na pozycji wniosku.");
        try {
            poz.setDatoper(ObjectTypeManager.convertToXMLGregorianCalendarDate(datOczek));
        } catch (DatatypeConfigurationException e) {
            log.error("B��d podczas konwertowania daty do odpowiedniego formatu: "+e);
        }

        if (mapaPozBudzetowej!=null){
            //zasob_id
            EnumValues zasobId = (EnumValues) mapaPozBudzetowej.get(pozycjeBudzetoweDictCnPrefix + "ZASOB");
            if (zasobId.getSelectedOptions().size()>0 && !zasobId.getSelectedOptions().get(0).equals("") && zasobId.getSelectedOptions().get(0) != null)
                poz.setZasobId(new BigDecimal(zasobId.getSelectedOptions().get(0)));
            //kontrakt_id
            EnumValues projektId = (EnumValues) mapaPozBudzetowej.get(pozycjeBudzetoweDictCnPrefix + "KONTRAKT");
            if (projektId.getSelectedOptions().size()>0 && !projektId.getSelectedOptions().get(0).equals("") && projektId.getSelectedOptions().get(0) != null)
                poz.setKontraktId(ObjectTypeManager.createIDMIDTYPE(factory, projektId));
            //projekt_id
            EnumValues budzetProjektuId = (EnumValues) mapaPozBudzetowej.get(pozycjeBudzetoweDictCnPrefix + "BUDZET");
            if (budzetProjektuId.getSelectedOptions().size()>0 && !budzetProjektuId.getSelectedOptions().get(0).equals("") && budzetProjektuId.getSelectedOptions().get(0) != null)
                poz.setProjektId(ObjectTypeManager.createIDMIDTYPE(factory, budzetProjektuId));
            //etap_id
            EnumValues etapId = (EnumValues) mapaPozBudzetowej.get(pozycjeBudzetoweDictCnPrefix + "FAZA");
            if (etapId.getSelectedOptions().size()>0 && !etapId.getSelectedOptions().get(0).equals("") && etapId.getSelectedOptions().get(0) != null)
                poz.setEtapId(new BigDecimal(etapId.getSelectedOptions().get(0)));

            //zlecprod_id
            EnumValues zlecprodId = (EnumValues) mapaPozBudzetowej.get(pozycjeBudzetoweDictCnPrefix + "ZLECENIA");
            if (zlecprodId.getSelectedOptions().size()>0 && !zlecprodId.getSelectedOptions().get(0).equals("") && zlecprodId.getSelectedOptions().get(0) != null)
                poz.setZlecprodId(ObjectTypeManager.createIDMIDTYPE(factory,zlecprodId));

            //komorkaId
            EnumValues mpkId = (EnumValues) mapaPozBudzetowej.get(pozycjeBudzetoweDictCnPrefix + "MPK");
            if (mpkId.getSelectedOptions().size()>0 && !mpkId.getSelectedOptions().get(0).equals("") && mpkId.getSelectedOptions().get(0) != null)
                poz.setKomorkaId(ObjectTypeManager.createIDNIDTYPE(factory,mpkId));
        }
        return poz;
    }

    private SysDokZalacznikiTYPE createSysDokZalacznikiTYPE(ObjectFactory factory, OfficeDocument document) throws EdmException {
        SysDokZalacznikiTYPE sysDokZalacznikiTYPE = factory.createSysDokZalacznikiTYPE();
        List<Attachment> zalaczniki = document.getAttachments();
        for(Attachment zal:zalaczniki){

            String sciezka = "";

            if (Docusafe.getAdditionProperty("erp.faktura.link_do_skanu")!=null){
                String urlSystemu = Docusafe.getAdditionProperty("erp.faktura.link_do_skanu");
                sciezka  = urlSystemu;
            }
            else
            {
                sciezka  = Docusafe.getBaseUrl();
            }

            sciezka += "/repository/view-attachment-revision.do?id="+zal.getMostRecentRevision().getId();

            SysDokZalacznikTYPE sysDokZalacznikTYPE = factory.createSysDokZalacznikTYPE();
            sysDokZalacznikTYPE.setNazwa(zal.getTitle());
            sysDokZalacznikTYPE.setSciezka(sciezka);
            sysDokZalacznikTYPE.setType("URL");

            //dodanie do listy zalacznik�w
            sysDokZalacznikiTYPE.getSysDokZalacznik().add(sysDokZalacznikTYPE);
        }
        return sysDokZalacznikiTYPE;
    }


    private Map getBudgetPositionForVatRate(List pozycjeBudzetoweFaktury, Integer pozycjaId) {
        for (Object poz : pozycjeBudzetoweFaktury)
        {
            Map mapaPoz = (Map) poz;
            Integer idPozycji = (Integer) mapaPoz.get(CostInvoiceLogic.MPK_DOCKIND_CN + "_" + CostInvoiceLogic.MPK_ID_DOCKIND_CN);
            if(idPozycji.compareTo(pozycjaId) == 0)
                return mapaPoz;
        }
        return null;
    }

    /*private DokzakpozTYPE getDokzakPozElementFromVatRatePosition(ObjectFactory factory, Map mapaPozycjiBudzetowej, Map mapaPozycjiStawkiVat, int nrPoz) {
        DokzakpozTYPE dokzakpozTYPE = factory.createDokzakpozTYPE();
        EnumValues komorkaId = (EnumValues) mapaPozycjiBudzetowej.get(CostInvoiceLogic.MPK_DOCKIND_CN + "_" + CostInvoiceLogic.MPK_KOMORKAID_DOCKIND_CN);
        dokzakpozTYPE.setKomorkaId(createIDNIDTYPE(factory,IDENTITYNATURETYPE.N,new BigDecimal(komorkaId.getSelectedOptions().get(0)),null));
        EnumValues budzetId = (EnumValues) mapaPozycjiBudzetowej.get(CostInvoiceLogic.MPK_DOCKIND_CN + "_" + CostInvoiceLogic.MPK_BUDZETID_DOCKIND_CN);
        dokzakpozTYPE.setBudzetId(createIDMIDTYPE(factory,budzetId));
        EnumValues pozycjaId = (EnumValues) mapaPozycjiBudzetowej.get(CostInvoiceLogic.MPK_DOCKIND_CN + "_" + CostInvoiceLogic.MPK_POZYCJAID_DOCKIND_CN);
//        TODO: doda� zapisywanie pozycji bud�etu
        EnumValues przeznaczenie = (EnumValues) mapaPozycjiBudzetowej.get(CostInvoiceLogic.MPK_DOCKIND_CN + "_" + CostInvoiceLogic.MPK_PRZEZNACZENIE_DOCKIND_CN);
        dokzakpozTYPE.setPrzeznzak(przeznaczenie.getSelectedOptions().get(0));

        EnumValues pozycja = (EnumValues) mapaPozycjiStawkiVat.get(CostInvoiceLogic.STAWKI_VAT_DOCKIND_CN + "_" + CostInvoiceLogic.STAWKI_VAT_POZYCJA_DOCKIND_CN);
        dokzakpozTYPE.setWytworId(createIDMIDTYPE(factory, pozycja));
        dokzakpozTYPE.setNazwa(pozycja.getSelectedValue());
        EnumValues stawkaVat = (EnumValues) mapaPozycjiStawkiVat.get(CostInvoiceLogic.STAWKI_VAT_DOCKIND_CN + "_" + CostInvoiceLogic.STAWKI_VAT_STAWKA_DOCKIND_CN);
        dokzakpozTYPE.setVatstawId(createIDSIDTYPE(factory, stawkaVat));
        BigDecimal kwotaNetto = (BigDecimal) mapaPozycjiStawkiVat.get(CostInvoiceLogic.STAWKI_VAT_DOCKIND_CN + "_" + CostInvoiceLogic.STAWKI_VAT_NETTO_DOCKIND_CN);
        dokzakpozTYPE.setCena(kwotaNetto);
        dokzakpozTYPE.setKwotnett(kwotaNetto);
        BigDecimal kwotaVat = (BigDecimal) mapaPozycjiStawkiVat.get(CostInvoiceLogic.STAWKI_VAT_DOCKIND_CN + "_" + CostInvoiceLogic.STAWKI_VAT_KWOTA_VAT_DOCKIND_CN);
        dokzakpozTYPE.setKwotvat(kwotaVat);
//        BigDecimal kwotaBrutto = (BigDecimal) mapaPozycjiStawkiVat.get(CostInvoiceLogic.STAWKI_VAT_DOCKIND_CN + "_" + CostInvoiceLogic.STAWKI_VAT_BRUTTO_DOCKIND_CN);

        dokzakpozTYPE.setJmId(createIDNIDTYPE(factory,IDENTITYNATURETYPE.T,null,"szt"));
        dokzakpozTYPE.setNrpoz(nrPoz);
        dokzakpozTYPE.setIlosc(BigDecimal.ONE);
        dokzakpozTYPE.setRodzwytw("0");
        dokzakpozTYPE.setRodztowaru("0");

        return dokzakpozTYPE;
    }*/

    private IDMIDTYPE createIDMIDTYPE(ObjectFactory factory, EnumValues enumValue) {
        IDMIDTYPE wytworId = factory.createIDMIDTYPE();
        wytworId.setType(IDENTITYNATURETYPE.N);
        wytworId.setId(new BigDecimal(enumValue.getSelectedOptions().get(0)));
        return wytworId;
    }

    private IDSIDTYPE createIDSIDTYPE(ObjectFactory factory, EnumValues enumValues) {
        IDSIDTYPE idsidtype = factory.createIDSIDTYPE();
        idsidtype.setType(IDENTITYNATURETYPE.N);
        idsidtype.setId(new BigDecimal(enumValues.getSelectedOptions().get(0)));
        return idsidtype;
    }

    private XMLGregorianCalendar convertToXMLGregorianCalendarDate(Date date) throws DatatypeConfigurationException {
        GregorianCalendar gregoryDate = new GregorianCalendar();
        DateUtils.exportDateFormat.format(date);
        gregoryDate.setTime(date);
        XMLGregorianCalendar xmlDate;
        xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregoryDate);
        return xmlDate;
    }

    private IDNIDTYPE createIDNIDTYPE(ObjectFactory fac, IDENTITYNATURETYPE type, BigDecimal id, String idn){
        IDNIDTYPE element = fac.createIDNIDTYPE();
        element.setType(type);
        element.setId(id);
        element.setIdn(idn);
        return element;
    }
}
