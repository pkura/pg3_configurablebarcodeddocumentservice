package pl.compan.docusafe.parametrization.ins.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.CallbackException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

@Entity
@Table(name = "DSG_COSTINVOICE_MPK")
public class PozycjaBudzetowaFaktury implements Lifecycle  {
    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "POZID", nullable = false)
    private Long pozid;

    @Column(name = "NETTO", nullable = false)
    private BigDecimal netto;

    @Column(name = "KOMORKAID", nullable = false)
    private Long komorkaid;

    @Column(name = "BUDZETID", nullable = false)
    private Long budzetid;

    @Column(name = "POZYCJAID", nullable = false)
    private Long pozycjaid;

    @Column(name = "KONTRAKT", nullable = false)
    private Long kontrakt;

    @Column(name = "BUDZET", nullable = false)
    private Long budzet;

    @Column(name = "FAZA", nullable = false)
    private Long faza;

    @Column(name = "ZASOB", nullable = false)
    private Long zasob;

    @Column(name = "PRZEZNACZENIE", nullable = false)
    private Long przeznaczenie;

    @Column(name = "ZLECENIA", nullable = false)
    private Long zlecenia;

    public PozycjaBudzetowaFaktury() {
    }

    /**
     * Wyszukanie pozycji budzetowej po id
     * @param id
     * @return
     * @throws pl.compan.docusafe.core.EdmException
     */
    public static PozycjaBudzetowaFaktury findById(Long id) throws EdmException {
        return (PozycjaBudzetowaFaktury)(DSApi.context().session().createCriteria(PozycjaBudzetowaFaktury.class)
                .add(Restrictions.eq("id", id)).list().get(0));
    }

    /**
     * Wyszukanie pozycji budzetowej po pozId
     * @param pozid
     * @return
     * @throws pl.compan.docusafe.core.EdmException
     */
    public static List<PozycjaBudzetowaFaktury> findByPozId(Long pozid) throws EdmException {
        return DSApi.context().session().createCriteria(PozycjaBudzetowaFaktury.class)
                .add(Restrictions.eq("pozid", pozid)).list();
    }

    public static BigDecimal sumNettoForPozId(Long pozid, List<Long> ids) throws EdmException {
        BigDecimal suma = BigDecimal.ZERO;
        List<PozycjaBudzetowaFaktury> listaPozycji = DSApi.context().session().createCriteria(PozycjaBudzetowaFaktury.class)
                .add(Restrictions.eq("pozid", pozid))
                .add(Restrictions.in("id",ids)).list();
        if (listaPozycji == null || listaPozycji.size()==0)
            return BigDecimal.ZERO;

        for (PozycjaBudzetowaFaktury poz : listaPozycji){
            suma = suma.add(poz.getNetto());
        }
        return suma;
    }

    public String toString() {
        return ""+id;
    }

    public final void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    @Override
    public boolean onDelete(Session arg0) throws CallbackException {
        return false;
    }

    @Override
    public void onLoad(Session arg0, Serializable arg1) {
    }

    @Override
    public boolean onSave(Session arg0) throws CallbackException {
        return false;
    }

    @Override
    public boolean onUpdate(Session arg0) throws CallbackException {
        return false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPozid() {
        return pozid;
    }

    public void setPozid(Long pozid) {
        this.pozid = pozid;
    }

    public Long getKomorkaid() {
        return komorkaid;
    }

    public void setKomorkaid(Long komorkaid) {
        this.komorkaid = komorkaid;
    }

    public Long getBudzetid() {
        return budzetid;
    }

    public void setBudzetid(Long budzetid) {
        this.budzetid = budzetid;
    }

    public Long getPozycjaid() {
        return pozycjaid;
    }

    public void setPozycjaid(Long pozycjaid) {
        this.pozycjaid = pozycjaid;
    }

    public Long getKontrakt() {
        return kontrakt;
    }

    public void setKontrakt(Long kontrakt) {
        this.kontrakt = kontrakt;
    }

    public Long getBudzet() {
        return budzet;
    }

    public void setBudzet(Long budzet) {
        this.budzet = budzet;
    }

    public Long getFaza() {
        return faza;
    }

    public void setFaza(Long faza) {
        this.faza = faza;
    }

    public Long getZasob() {
        return zasob;
    }

    public void setZasob(Long zasob) {
        this.zasob = zasob;
    }

    public Long getPrzeznaczenie() {
        return przeznaczenie;
    }

    public void setPrzeznaczenie(Long przeznaczenie) {
        this.przeznaczenie = przeznaczenie;
    }

    public Long getZlecenia() {
        return zlecenia;
    }

    public void setZlecenia(Long zlecenia) {
        this.zlecenia = zlecenia;
    }

    public BigDecimal getNetto() {
        return netto;
    }

    public void setNetto(BigDecimal netto) {
        this.netto = netto;
    }
}