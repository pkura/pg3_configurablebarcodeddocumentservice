package pl.compan.docusafe.parametrization.ins.entities;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.CallbackException;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 24.09.13
 * Time: 15:52
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "DSG_STAWKI_VAT_FAKTURY")
public class PozycjaFaktury implements Lifecycle  {
    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "pozycja", nullable = false)
    private Integer pozycja;

    @Column(name = "netto", nullable = false)
    private BigDecimal netto;

    @Column(name = "stawka", nullable = false)
    private Integer stawka;

    @Column(name = "kwota_vat", nullable = false)
    private BigDecimal kwotaVat;

    @Column(name = "brutto", nullable = false)
    private BigDecimal brutto;

    @Column(name = "pozycjaId", nullable = false)
    private Integer pozycjaId;

    public PozycjaFaktury() {

    }

    /**
     * Wyszukanie pozycji faktury po id
     * @param id
     * @return
     * @throws pl.compan.docusafe.core.EdmException
     */
    public static PozycjaFaktury findById(Long id) throws EdmException {
        return (PozycjaFaktury)(DSApi.context().session().createCriteria(PozycjaFaktury.class).add(Restrictions.eq("id", id)).list().get(0));
    }



    public String toString() {
        return ""+id;
    }

    public final void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    @Override
    public boolean onDelete(Session arg0) throws CallbackException {
        return false;
    }

    @Override
    public void onLoad(Session arg0, Serializable arg1) {
    }

    @Override
    public boolean onSave(Session arg0) throws CallbackException {
        return false;
    }

    @Override
    public boolean onUpdate(Session arg0) throws CallbackException {
        return false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPozycja() {
        return pozycja;
    }

    public void setPozycja(Integer pozycja) {
        this.pozycja = pozycja;
    }

    public BigDecimal getNetto() {
        return netto;
    }

    public void setNetto(BigDecimal netto) {
        this.netto = netto;
    }

    public Integer getStawka() {
        return stawka;
    }

    public void setStawka(Integer stawka) {
        this.stawka = stawka;
    }

    public BigDecimal getKwotaVat() {
        return kwotaVat;
    }

    public void setKwotaVat(BigDecimal kwotaVat) {
        this.kwotaVat = kwotaVat;
    }

    public BigDecimal getBrutto() {
        return brutto;
    }

    public void setBrutto(BigDecimal brutto) {
        this.brutto = brutto;
    }

    public Integer getPozycjaId() {
        return pozycjaId;
    }

    public void setPozycjaId(Integer pozycjaId) {
        this.pozycjaId = pozycjaId;
    }
}
