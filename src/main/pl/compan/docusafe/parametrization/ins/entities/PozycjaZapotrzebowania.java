package pl.compan.docusafe.parametrization.ins.entities;


import org.hibernate.CallbackException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

@Entity
@Table(name = "INS_WnioZapDoDost_Pozycje")
public class PozycjaZapotrzebowania implements Lifecycle  {
    @Id
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "LP", nullable = false)
    private String lp;

    @Column(name = "POZYCJA_BUDZETU", nullable = false)
    private Long pozycjaBudzetu;

    @Column(name = "PRODUKT", nullable = false)
    private Long produkt;

    @Column(name = "OPIS", nullable = false)
    private String opis;

    @Column(name = "ILOSC", nullable = false)
    private BigDecimal ilosc;

    @Column(name = "JM", nullable = false)
    private Long jm;

    @Column(name = "CENA", nullable = false)
    private BigDecimal cena;

    @Column(name = "CENA_BAZ", nullable = false)
    private BigDecimal cenaBaz;

    @Column(name = "NETTO", nullable = false)
    private BigDecimal netto;

    @Column(name = "NETTO_BAZ", nullable = false)
    private BigDecimal nettoBaz;

    @Column(name = "VAT", nullable = false)
    private Long vat;

    @Column(name = "DATAOCZEKIWANA", nullable = false)
    private Date dataOczekiwana;

    public PozycjaZapotrzebowania() {

    }

    /**
     * Wyszukanie pozycji zapotrzebowania po id
     * @param id
     * @return
     * @throws pl.compan.docusafe.core.EdmException
     */
    public static PozycjaZapotrzebowania findById(Long id) throws EdmException {
        return (PozycjaZapotrzebowania)(DSApi.context().session().createCriteria(PozycjaZapotrzebowania.class).add(Restrictions.eq("id", id)).list().get(0));
    }



    public String toString() {
        return ""+id;
    }

    public final void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    @Override
    public boolean onDelete(Session arg0) throws CallbackException {
        return false;
    }

    @Override
    public void onLoad(Session arg0, Serializable arg1) {
    }

    @Override
    public boolean onSave(Session arg0) throws CallbackException {
        return false;
    }

    @Override
    public boolean onUpdate(Session arg0) throws CallbackException {
        return false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLp() {
        return lp;
    }

    public void setLp(String lp) {
        this.lp = lp;
    }

    public Long getPozycjaBudzetu() {
        return pozycjaBudzetu;
    }

    public void setPozycjaBudzetu(Long pozycjaBudzetu) {
        this.pozycjaBudzetu = pozycjaBudzetu;
    }

    public Long getProdukt() {
        return produkt;
    }

    public void setProdukt(Long produkt) {
        this.produkt = produkt;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public BigDecimal getIlosc() {
        return ilosc;
    }

    public void setIlosc(BigDecimal ilosc) {
        this.ilosc = ilosc;
    }

    public Long getJm() {
        return jm;
    }

    public void setJm(Long jm) {
        this.jm = jm;
    }

    public BigDecimal getCena() {
        return cena;
    }

    public void setCena(BigDecimal cena) {
        this.cena = cena;
    }

    public BigDecimal getCenaBaz() {
        return cenaBaz;
    }

    public void setCenaBaz(BigDecimal cenaBaz) {
        this.cenaBaz = cenaBaz;
    }

    public BigDecimal getNetto() {
        return netto;
    }

    public void setNetto(BigDecimal netto) {
        this.netto = netto;
    }

    public BigDecimal getNettoBaz() {
        return nettoBaz;
    }

    public void setNettoBaz(BigDecimal nettoBaz) {
        this.nettoBaz = nettoBaz;
    }

    public Long getVat() {
        return vat;
    }

    public void setVat(Long vat) {
        this.vat = vat;
    }

    public Date getDataOczekiwana() {
        return dataOczekiwana;
    }

    public void setDataOczekiwana(Date dataOczekiwana) {
        this.dataOczekiwana = dataOczekiwana;
    }
}
