package pl.compan.docusafe.parametrization.ins;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.builder.SAXOMBuilder;
import org.apache.commons.dbcp.BasicDataSource;
import org.jbpm.api.listener.EventListenerExecution;

import pl.com.simple.simpleerp.*;
import pl.com.simple.simpleerp.dokzak.*;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ins.entities.PozycjaBudzetowaFaktury;
import pl.compan.docusafe.parametrization.ins.entities.PozycjaFaktury;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ExportInvoiceProcessListener extends AbstractEventListener
{
	private static final Logger log = LoggerFactory.getLogger(ExportInvoiceProcessListener.class);

    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        OfficeDocument document = Jbpm4Utils.getDocument(execution);
        FieldsManager fm = document.getFieldsManager();
        try
        {
            JAXBContext jaxbContext = JAXBContext.newInstance("pl.com.simple.simpleerp.dokzak");
            Marshaller marshaller = jaxbContext.createMarshaller();

            //tworzenie obiekt�w dla faktury kosztowej
            ObjectFactory factory = new ObjectFactory();
            DokzakTYPE dokzakTYPE = factory.createDokzakTYPE();

            //warplat_id
            String idnWarplat = fm.getStringValue(CostInvoiceLogic.SPOSOB_DOCKIND_CN);
            IDNIDTYPE warplatId = ObjectTypeManager.createIDNIDTYPE(factory,IDENTITYNATURETYPE.T, null, idnWarplat.trim());
            dokzakTYPE.getContent().add(factory.createDokzakTYPEWarplatId(warplatId));

            //stdost_id
            BigDecimal idStdost = new BigDecimal(fm.getIntegerKey(CostInvoiceLogic.STANOWISKO_DOSTAW_DOCKIND_CN));
            IDNIDTYPE stdostId = ObjectTypeManager.createIDNIDTYPE(factory, IDENTITYNATURETYPE.N, idStdost, null);
            dokzakTYPE.getContent().add(factory.createDokzakTYPEStdostId(stdostId));

            //tabkurs_id
            IDNIDTYPE tabkursId = ObjectTypeManager.createIDNIDTYPE(factory, IDENTITYNATURETYPE.T, null,"UNI2013");
            dokzakTYPE.getContent().add(factory.createDokzakTYPETabkursId(tabkursId));

            //zlecprod_id
            if(fm.getIntegerKey(CostInvoiceLogic.ZLECENIA_DOCKIND_CN) != null){
                BigDecimal idZlecenia = new BigDecimal(fm.getIntegerKey(CostInvoiceLogic.ZLECENIA_DOCKIND_CN));
                IDMIDTYPE zlecprodId = ObjectTypeManager.createIDMIDTYPE(factory,idZlecenia);
                dokzakTYPE.getContent().add(factory.createDokzakTYPEZlecprodId(zlecprodId));
            }

            //pracownik_id
            if(fm.getIntegerKey(CostInvoiceLogic.PRACOWNIK_DOCKIND_CN) != null){
                BigDecimal idPracownika = new BigDecimal(fm.getIntegerKey(CostInvoiceLogic.PRACOWNIK_DOCKIND_CN));
                IDNIDTYPE pracownikId = ObjectTypeManager.createIDNIDTYPE(factory,IDENTITYNATURETYPE.N,idPracownika,null);
                dokzakTYPE.getContent().add(factory.createDokzakTYPEPracownikId(pracownikId));
            }

            //uzytk_id
//            String idnUzytk = "mtomkowicz";
            String idnUzytk = Docusafe.getAdditionProperty("erp.operator.user.idn");
            if (idnUzytk == null || idnUzytk.equals(""))
                throw new EdmException("Brak skonfigurowanego operatora ERP.");

            IDNIDTYPE uzytkId = ObjectTypeManager.createIDNIDTYPE(factory,IDENTITYNATURETYPE.T,null, idnUzytk);
            dokzakTYPE.getContent().add(factory.createDokzakTYPEUzytkId(uzytkId));

            //waluta_id
            SYMBOLWALUTYIDTYPE symbolWaluty = factory.createSYMBOLWALUTYIDTYPE();
            symbolWaluty.setSymbolwaluty(fm.getStringValue(CostInvoiceLogic.WALUTA_DOCKIND_CN));
            symbolWaluty.setType(IDENTITYNATURETYPE.T);
            dokzakTYPE.getContent().add(factory.createDokzakTYPEWalutaId(symbolWaluty));

            //typdokzak_id
            BigDecimal idTypDokzakId = null;
            if(fm.getKey(CostInvoiceLogic.TYP_FAKTURY_WAL_DOCKIND_CN) == null && fm.getKey(CostInvoiceLogic.TYP_FAKTURY_PLN_DOCKIND_CN) != null){
                idTypDokzakId = new BigDecimal(fm.getIntegerKey(CostInvoiceLogic.TYP_FAKTURY_PLN_DOCKIND_CN));
            }else if(fm.getKey(CostInvoiceLogic.TYP_FAKTURY_WAL_DOCKIND_CN) != null && fm.getKey(CostInvoiceLogic.TYP_FAKTURY_PLN_DOCKIND_CN) == null){
                idTypDokzakId = new BigDecimal(fm.getIntegerKey(CostInvoiceLogic.TYP_FAKTURY_WAL_DOCKIND_CN));
            }else if(fm.getKey(CostInvoiceLogic.TYP_FAKTURY_WAL_DOCKIND_CN) != null && fm.getKey(CostInvoiceLogic.TYP_FAKTURY_PLN_DOCKIND_CN) != null){
                idTypDokzakId = new BigDecimal(fm.getIntegerKey(CostInvoiceLogic.TYP_FAKTURY_PLN_DOCKIND_CN));
            }else{
                throw new EdmException("B��d podczas exportu faktury kosztowej: BRAK WYBRANEGO TYPU DOKUMENTU ZAKUPU");
            }
            IDNIDTYPE typdokzakId = ObjectTypeManager.createIDNIDTYPE(factory,IDENTITYNATURETYPE.N, idTypDokzakId, null);
            dokzakTYPE.getContent().add(factory.createDokzakTYPETypdokzakId(typdokzakId));

            //dostfakt_id
            IDNIDTYPE dostawcaId = ObjectTypeManager.createIDNIDTYPE(factory, IDENTITYNATURETYPE.N, new BigDecimal(document.getSender().getExternalID()), null);
            dokzakTYPE.getContent().add(factory.createDokzakTYPEDostawcaId(dostawcaId));

            //datdok
//            dokzakTYPE.getContent().add(factory.createDokzakTYPEDatdok(convertToXMLGregorianCalendarDate((Date) fm.getValue(CostInvoiceLogic.DATA_WYSTAWIENIA_DOCKIND_CN))));
            dokzakTYPE.getContent().add(factory.createDokzakTYPEDatdok((Date) fm.getValue(CostInvoiceLogic.DATA_WYSTAWIENIA_DOCKIND_CN)));

            //datoper
//            dokzakTYPE.getContent().add(factory.createDokzakTYPEDatoper(convertToXMLGregorianCalendarDate((Date) fm.getValue(CostInvoiceLogic.TERMIN_PLATNOSCI_DOCKIND_CN))));
            dokzakTYPE.getContent().add(factory.createDokzakTYPEDatoper((Date) fm.getValue(CostInvoiceLogic.TERMIN_PLATNOSCI_DOCKIND_CN)));

            //kurs
            BigDecimal kurs = new BigDecimal((Double) fm.getValue(CostInvoiceLogic.KURS_DOCKIND_CN)).setScale(4,BigDecimal.ROUND_HALF_UP);
            dokzakTYPE.getContent().add(factory.createDokzakTYPEKurs(kurs));

            //wartdok
            BigDecimal kwotaNetto = new BigDecimal( fm.getStringValue(CostInvoiceLogic.KWOTA_NETTO_WALBAZ_DOCKIND_CN).replace(" ",""));
            dokzakTYPE.getContent().add(factory.createDokzakTYPEWartdok(kwotaNetto));

            //wartwalbaz
            dokzakTYPE.getContent().add(factory.createDokzakTYPEWartwalbaz(kwotaNetto));

            //dokobcy
            dokzakTYPE.getContent().add(factory.createDokzakTYPEDokobcy(fm.getStringValue(CostInvoiceLogic.NR_FAKTURY_DOCKIND_CN)));

            //datprzyj
//            dokzakTYPE.getContent().add(factory.createDokzakTYPEDatprzyj(convertToXMLGregorianCalendarDate((Date) fm.getValue(CostInvoiceLogic.DATA_WPLYWU_DOCKIND_CN))));
            dokzakTYPE.getContent().add(factory.createDokzakTYPEDatprzyj((Date) fm.getValue(CostInvoiceLogic.DATA_WPLYWU_DOCKIND_CN)));

            //kwvat
            BigDecimal kwotaVat = new BigDecimal(fm.getStringValue(CostInvoiceLogic.KWOTA_VAT_DOCKIND_CN).replace(" ",""));
            dokzakTYPE.getContent().add(factory.createDokzakTYPEKwvat(kwotaVat));

            //kwotvatwalbaz
            dokzakTYPE.getContent().add(factory.createDokzakTYPEKwotvatwalbaz(kwotaVat));

            //dokzakpozycje
            DokzakpozycjeTYPE dokzakpozycjeTYPE = factory.createDokzakpozycjeTYPE();
//            List pozycjeBudzetoweFaktury = (List) fm.getValue(CostInvoiceLogic.MPK_DOCKIND_CN);
            List pozycjeFaktury = (List) fm.getValue(CostInvoiceLogic.MPK_DOCKIND_CN);
            int i = 1;
            for(Object poz : pozycjeFaktury){
                Map mapaPoz = (Map) poz;
                /*List<Map> maps = null;
                BigDecimal pozycjaId = (BigDecimal) mapaPoz.get(CostInvoiceLogic.STAWKI_VAT_DOCKIND_CN + "_"
                        + CostInvoiceLogic.STAWKI_VAT_ID_DOCKIND_CN);
                //wyci�gni�cie odpowiedniej pozycji budzetowej
                maps = getBudgetPositionForVatRate(pozycjeBudzetoweFaktury, pozycjaId.longValue());
                //ustawienie obiektu dokzakPoz
                if (maps == null){
                    dokzakpozycjeTYPE.getDokzakpoz().add(getDokzakPozElementFromVatRatePosition(factory, null,
                            mapaPoz, i, kurs));
                    i++;
                }else{
                    for (Map mapaPozycjiBudzetowej: maps){
                        dokzakpozycjeTYPE.getDokzakpoz().add(getDokzakPozElementFromVatRatePosition(factory, mapaPozycjiBudzetowej,
                                mapaPoz, i, kurs));
                        i++;
                    }
                }*/

                //pobieranie pozycji faktury ze s�ownika MPK
                dokzakpozycjeTYPE.getDokzakpoz().add(getDokzakPozElementFromVatRatePosition(factory, mapaPoz, null, i, kurs));
                i++;
            }
            dokzakTYPE.getContent().add(factory.createDokzakTYPEDokzakpozycje(dokzakpozycjeTYPE));

            //warplatdokzaki
            WarplatdokzakTYPE warplatdokzak = factory.createWarplatdokzakTYPE();
            String sposobZaplaty = null;
            if(fm.getStringValue(CostInvoiceLogic.SPOSOB_DOCKIND_CN).trim().equals("Got�wka"))
                sposobZaplaty = "0";
            else
                sposobZaplaty = "1";
            warplatdokzak.getContent().add(factory.createWarplatdokzakTYPESposzapl(sposobZaplaty));
            if(kurs.compareTo(BigDecimal.ONE)==0)
                warplatdokzak.getContent().add(factory.createWarplatdokzakTYPEKurs(BigDecimal.ZERO));
            else
                warplatdokzak.getContent().add(factory.createWarplatdokzakTYPEKurs(kurs));
            warplatdokzak.getContent().add(factory.createWarplatdokzakTYPECzyproc("0"));
            warplatdokzak.getContent().add(factory.createWarplatdokzakTYPEProckwot(BigDecimal.ZERO));
            BigDecimal kwotaBrutto = new BigDecimal(fm.getStringValue(CostInvoiceLogic.KWOTA_BRUTTO_DOCKIND_CN).replace(" ",""));
            warplatdokzak.getContent().add(factory.createWarplatdokzakTYPEKwota(kwotaBrutto));
            warplatdokzak.getContent().add(factory.createWarplatdokzakTYPETypplat("0"));
            warplatdokzak.getContent().add(factory.createWarplatdokzakTYPETerplat((Date) fm.getValue(CostInvoiceLogic.TERMIN_PLATNOSCI_DOCKIND_CN)));
            WarplatdokzakiTYPE warplatdokzaki = factory.createWarplatdokzakiTYPE();
            warplatdokzaki.getWarplatdokzak().add(warplatdokzak);
            dokzakTYPE.getContent().add(factory.createDokzakTYPEWarplatdokzaki(warplatdokzaki));

            dokzakTYPE.getContent().add(factory.createDokzakTYPESysDokZalaczniki(createSysDokZalacznikiTYPE(factory, document)));

            //tworzenie XMLa
            SAXOMBuilder builder = new SAXOMBuilder();

            marshaller.marshal(factory.createDokzak(dokzakTYPE), builder);
            OMElement xmlElement= builder.getRootElement();
            if(xmlElement != null){
                log.error(xmlElement.toString());
                ExportInvoiceToSimpleERP.export(xmlElement.toString(), document);
            }

        }catch (JAXBException e){
            log.error("B��d podczas tworzenia XML dla obiektu faktury kosztowej: "+e);
            throw new EdmException("B��d podczas tworzenia XML dla obiektu faktury kosztowej: "+e);
        }catch (NumberFormatException e){
            log.error("B��d formatu danych: "+e);
            throw new EdmException("B��d formatu danych: "+e);
        }catch (Exception e){
            log.error("B��d podczas exportu faktury kosztowej do SIMPLE.ERP: "+e);
            throw new EdmException("B��d podczas exportu faktury kosztowej do SIMPLE.ERP: "+e);
        }
    }

    private SysDokZalacznikiTYPE createSysDokZalacznikiTYPE(ObjectFactory factory, OfficeDocument document) throws EdmException {
        SysDokZalacznikiTYPE sysDokZalacznikiTYPE = factory.createSysDokZalacznikiTYPE();
        List<Attachment> zalaczniki = document.getAttachments();
        for(Attachment zal:zalaczniki){

            String sciezka = "";

            if (Docusafe.getAdditionProperty("erp.faktura.link_do_skanu")!=null){
                String urlSystemu = Docusafe.getAdditionProperty("erp.faktura.link_do_skanu");
                sciezka  = urlSystemu;
            }
            else
            {
                sciezka  = Docusafe.getBaseUrl();
            }

            sciezka += "/repository/view-attachment-revision.do?id="+zal.getMostRecentRevision().getId();

            SysDokZalacznikTYPE sysDokZalacznikTYPE = factory.createSysDokZalacznikTYPE();
            sysDokZalacznikTYPE.setNazwa(zal.getTitle());
            sysDokZalacznikTYPE.setSciezka(sciezka);
            sysDokZalacznikTYPE.setType("URL");

            //dodanie do listy zalacznik�w
            sysDokZalacznikiTYPE.getSysDokZalacznik().add(sysDokZalacznikTYPE);
        }
        return sysDokZalacznikiTYPE;
    }


    private List<Map> getBudgetPositionForVatRate(List pozycjeBudzetoweFaktury, Long pozycjaId) throws EdmException {
        List lista = new ArrayList();
        if (pozycjeBudzetoweFaktury != null){
            for (Object poz : pozycjeBudzetoweFaktury)
            {
                Map mapaPoz = (Map) poz;
                PozycjaBudzetowaFaktury pozBud = PozycjaBudzetowaFaktury.findById(
                        ((Integer) mapaPoz.get("MPK_ID")).longValue());
                if(pozBud.getPozid().compareTo(pozycjaId) == 0)
                    lista.add(mapaPoz);
            }
            return lista;
        }
        return null;
    }

    private DokzakpozTYPE getDokzakPozElementFromVatRatePosition(ObjectFactory factory, Map mapaPozycjiBudzetowej, Map mapaPozycjiStawkiVat, int nrPoz, BigDecimal kurs) throws EdmException, SQLException {
        DokzakpozTYPE dokzakpozTYPE = factory.createDokzakpozTYPE();

        if (mapaPozycjiBudzetowej != null){
            //procent kwoty
            BigDecimal netto = (BigDecimal) mapaPozycjiBudzetowej.get(CostInvoiceLogic.MPK_DOCKIND_CN+"_NETTO");
            //budzetowanie podmiotu
            EnumValues komorkaId = (EnumValues) mapaPozycjiBudzetowej.get(CostInvoiceLogic.MPK_DOCKIND_CN + "_MPK");
            if (komorkaId.getSelectedOptions().size()>0 && !komorkaId.getSelectedOptions().get(0).equals("") && komorkaId.getSelectedOptions().get(0) != null)
                dokzakpozTYPE.setKomorkaId(ObjectTypeManager.createIDNIDTYPE(factory,IDENTITYNATURETYPE.N,new BigDecimal(komorkaId.getSelectedOptions().get(0)),null));
            EnumValues budzetId = (EnumValues) mapaPozycjiBudzetowej.get(CostInvoiceLogic.MPK_DOCKIND_CN + "_" + CostInvoiceLogic.MPK_BUDZETID_DOCKIND_CN);
            if (budzetId.getSelectedOptions().size()>0 && !budzetId.getSelectedOptions().get(0).equals("") && budzetId.getSelectedOptions().get(0) != null){
                dokzakpozTYPE.setBdBudzetKoId(ObjectTypeManager.createIDMIDTYPE(factory,budzetId));
                Long okrBudId = getOkresBudzetowyId(budzetId.getSelectedOptions().get(0));
                if (okrBudId != null)
                    dokzakpozTYPE.setBudzetId(ObjectTypeManager.createIDMIDTYPE(factory,new BigDecimal(okrBudId.longValue())));
            }
            EnumValues pozycjaId = (EnumValues) mapaPozycjiBudzetowej.get(CostInvoiceLogic.MPK_DOCKIND_CN + "_" + CostInvoiceLogic.MPK_POZYCJAID_DOCKIND_CN);
            if (pozycjaId.getSelectedOptions().size()>0 && !pozycjaId.getSelectedOptions().get(0).equals("") && pozycjaId.getSelectedOptions().get(0) != null)
                dokzakpozTYPE.setBdRodzajKoId(new BigDecimal(pozycjaId.getSelectedOptions().get(0)));

            //budzetowanie projektow
            EnumValues budzetProjektuId = (EnumValues) mapaPozycjiBudzetowej.get(CostInvoiceLogic.MPK_DOCKIND_CN +
                    "_"+CostInvoiceLogic.MPK_BUDZET_PROJEKTU_DOCKIND_CN);
            if (budzetProjektuId.getSelectedOptions().size()>0 && !budzetProjektuId.getSelectedOptions().get(0).equals("") && budzetProjektuId.getSelectedOptions().get(0) != null)
                dokzakpozTYPE.setProjektId(ObjectTypeManager.createIDMIDTYPE(factory, budzetProjektuId));
            EnumValues projektId = (EnumValues) mapaPozycjiBudzetowej.get(CostInvoiceLogic.MPK_DOCKIND_CN + "_"
                    +CostInvoiceLogic.MPK_PROJEKT_DOCKIND_CN);
            if (projektId.getSelectedOptions().size()>0 && !projektId.getSelectedOptions().get(0).equals("") && projektId.getSelectedOptions().get(0) != null)
                dokzakpozTYPE.setKontraktId(ObjectTypeManager.createIDMIDTYPE(factory, projektId));
            EnumValues etapId = (EnumValues) mapaPozycjiBudzetowej.get(CostInvoiceLogic.MPK_DOCKIND_CN + "_"
                    +CostInvoiceLogic.MPK_ETAP_PROJEKTU_DOCKIND_CN);
            if (etapId.getSelectedOptions().size()>0 && !etapId.getSelectedOptions().get(0).equals("") && etapId.getSelectedOptions().get(0) != null)
                dokzakpozTYPE.setEtapId(new BigDecimal(etapId.getSelectedOptions().get(0)));
            EnumValues zasobId = (EnumValues) mapaPozycjiBudzetowej.get(CostInvoiceLogic.MPK_DOCKIND_CN + "_"+
                    CostInvoiceLogic.MPK_ZASOB_PROJEKTU_DOCKIND_CN);
            if (zasobId.getSelectedOptions().size()>0 && !zasobId.getSelectedOptions().get(0).equals("") && zasobId.getSelectedOptions().get(0) != null)
                dokzakpozTYPE.setZasobId(new BigDecimal(zasobId.getSelectedOptions().get(0)));

            EnumValues przeznaczenie = (EnumValues) mapaPozycjiBudzetowej.get(CostInvoiceLogic.MPK_DOCKIND_CN
                    + "_" + CostInvoiceLogic.STAWKI_VAT_PRZEZNACZENIE_DOCKIND_CN);
            if (przeznaczenie.getSelectedOptions().size()>0 && !przeznaczenie.getSelectedOptions().get(0).equals("") && przeznaczenie.getSelectedOptions().get(0) != null)
                dokzakpozTYPE.setPrzeznzak(przeznaczenie.getSelectedOptions().get(0));

            EnumValues zlecprodId = (EnumValues) mapaPozycjiBudzetowej.get(CostInvoiceLogic.MPK_DOCKIND_CN+"_"
                    + CostInvoiceLogic.STAWKI_VAT_ZLECENIA_DOCKIND_CN);
            if (zlecprodId.getSelectedOptions().size()>0 && !zlecprodId.getSelectedOptions().get(0).equals("") && zlecprodId.getSelectedOptions().get(0) != null)
                dokzakpozTYPE.setZlecprodId(ObjectTypeManager.createIDMIDTYPE(factory,zlecprodId));

            EnumValues pozycja = (EnumValues) mapaPozycjiBudzetowej.get(CostInvoiceLogic.MPK_DOCKIND_CN + "_" + CostInvoiceLogic.STAWKI_VAT_POZYCJA_DOCKIND_CN);
            dokzakpozTYPE.setWytworId(ObjectTypeManager.createIDMIDTYPE(factory, pozycja));
            String nazwaTemp = pozycja.getSelectedValue().length() < 40 ? pozycja.getSelectedValue() :
                    pozycja.getSelectedValue().substring(0,40);
            dokzakpozTYPE.setNazwa(nazwaTemp);
            EnumValues stawkaVat = (EnumValues) mapaPozycjiBudzetowej.get(CostInvoiceLogic.MPK_DOCKIND_CN + "_" + CostInvoiceLogic.STAWKI_VAT_STAWKA_DOCKIND_CN);
            dokzakpozTYPE.setVatstawId(ObjectTypeManager.createIDSIDTYPE(factory, stawkaVat));
//            BigDecimal kwotaNetto = (BigDecimal) mapaPozycjiBudzetowej.get(CostInvoiceLogic.MPK_DOCKIND_CN
//                    + "_" + CostInvoiceLogic.STAWKI_VAT_NETTO_DOCKIND_CN);
            BigDecimal kwotaVat = (BigDecimal) mapaPozycjiBudzetowej.get(CostInvoiceLogic.MPK_DOCKIND_CN + "_VAT");
//        dokzakpozTYPE.setCena(kwotaNetto.divide(kurs,2,BigDecimal.ROUND_HALF_UP));
//        dokzakpozTYPE.setCenwalbaz(kwotaNetto);
            dokzakpozTYPE.setCena(netto);
//        dokzakpozTYPE.setKwotnett(kwotaNetto.divide(kurs,2,BigDecimal.ROUND_HALF_UP));
//        dokzakpozTYPE.setKwotnettwalbaz(kwotaNetto);
            dokzakpozTYPE.setKwotnett(netto);
//        dokzakpozTYPE.setKwotvat(kwotaVat.divide(kurs,2,BigDecimal.ROUND_HALF_UP));
//        dokzakpozTYPE.setKwotvatwalbaz(kwotaVat);
            dokzakpozTYPE.setKwotvat(kwotaVat);
//        BigDecimal kwotaBrutto = (BigDecimal) mapaPozycjiStawkiVat.get(CostInvoiceLogic.STAWKI_VAT_DOCKIND_CN + "_" + CostInvoiceLogic.STAWKI_VAT_BRUTTO_DOCKIND_CN);

        }

        dokzakpozTYPE.setJmId(ObjectTypeManager.createIDNIDTYPE(factory,IDENTITYNATURETYPE.T,null,"szt"));
        dokzakpozTYPE.setNrpoz(nrPoz);
        dokzakpozTYPE.setIlosc(BigDecimal.ONE);
        dokzakpozTYPE.setRodzwytw("0");
        dokzakpozTYPE.setRodztowaru("0");

        return dokzakpozTYPE;
    }

    private Long getOkresBudzetowyId(String s) throws EdmException, SQLException {
        if(s.equals("")|| s==null)
            return null;

        Connection con = connectToERPDatabase();
        String erpDataBase = Docusafe.getAdditionProperty("erp.database.dataBase");
        String erpSchema = Docusafe.getAdditionProperty("erp.database.schema");
        String sql = "SELECT DISTINCT okr_bud_id" +
                "  FROM " + erpDataBase + "." + erpSchema + ".[v_bd_budzetowanie]" +
                "  where bd_budzet_id = ?";
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setObject(1, s);
        ResultSet rs = ps.executeQuery();
        while(rs.next())
        {
            Long okrBudzetowyId = rs.getLong("okr_bud_id");
            if (con != null)
            {
                con.close();
            }
            return okrBudzetowyId;
        }
        if (con != null)
        {
            con.close();
        }
        return null;
    }

    private Connection connectToERPDatabase() throws EdmException, SQLException {
        Connection con = null;
        CallableStatement call = null;

        String erpUser, erpPassword, erpUrl;
        erpUser = Docusafe.getAdditionProperty("erp.database.user");
        erpPassword = Docusafe.getAdditionProperty("erp.database.password");
        erpUrl = Docusafe.getAdditionProperty("erp.database.url");

        if (erpUser == null || erpPassword == null || erpUrl == null)
            throw new EdmException("B��d podczas pod��czenia do bazy ERP.");

        log.info("user: {}, password: {}, url: {}", erpUser, erpPassword, erpUrl);
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName(Docusafe.getDefaultDriver("sqlserver_2000"));
        ds.setUrl(erpUrl);
        ds.setUsername(erpUser);
        ds.setPassword(erpPassword);
        con = ds.getConnection();

        return con;
    }
}
