package pl.compan.docusafe.parametrization.ins.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.archiwum.UdostepnienieLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class CzyDodatkowaAkceptMerytDecisionHandler implements DecisionHandler
{
    public static final String AKCEPTACJA_MERYTORYCZNA = "oczekujeZatwierdzenie";
    public static final String AKCEPTACJA_DYREKTORA = "oczekujeDoERP";

    private final static Logger log = LoggerFactory.getLogger(CzyDodatkowaAkceptMerytDecisionHandler.class);

    public String decide(OpenExecution execution)
    {
        try {
            OfficeDocument doc = null;
            doc = Jbpm4Utils.getDocument(execution);
            FieldsManager fm = doc.getFieldsManager();
            boolean czyDodatkowa = true;

            if (fm.getKey("AKCEPTUJACY").equals(fm.getKey("ZATWIERDZAJACY")))
                czyDodatkowa = false;

            if (czyDodatkowa)
                return AKCEPTACJA_MERYTORYCZNA;
            else
                return AKCEPTACJA_DYREKTORA;
        } catch (EdmException e) {
            log.error("Proces wniosku zapotrzevowania wewn�trznego: B��d podczas sprawdzania czy potrzebna jest dodatkowa akceptacja merytoryczna. "+e);
            return AKCEPTACJA_MERYTORYCZNA;
        }
    }

}
