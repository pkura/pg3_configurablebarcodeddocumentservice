package pl.compan.docusafe.parametrization.ins.jbpm;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ins.WniosekZapDoDostLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class PrepareZamawianyTowarTable extends AbstractEventListener {

	protected static Logger log = LoggerFactory.getLogger(PrepareZamawianyTowarTable.class);
	
	private static class ZamawianyTowar{
		private Long produktId;
		private BigDecimal ilosc;
		private BigDecimal brutto;
		private String dotyczy;
		private Long indeks;
        private String uwagi;
	}

    public static List<ZamawianyTowar> transformPozycjeZamowieniaToZamawianyTowar(Long documentId, FieldsManager fm) {
    	List<ZamawianyTowar> wynik = new ArrayList<ZamawianyTowar>();
    	
    	boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();
			String sql = "SELECT \n" +
                    " poz.PRODUKT AS PRODUKT, \n" +
                    " poz.ILOSC AS ILOSC, \n" +
                    " poz.POZYCJA_BUDZETU, \n" +
                    " poz.NETTO AS NETTO, \n" +
                    " poz.VAT AS VAT, \n" +
                    " poz.IND AS IND,\n" +
                    " poz.OPIS AS OPIS, \n"+
                    " b.KONTRAKT as PROJEKT,\n" +
                    " b.ZLECENIA,\n" +
                    " b.MPK\n" +
                    " FROM INS_WnioZapDoDost_Pozycje AS poz\n" +
                    " LEFT JOIN INS_WniosZapDost_Dict AS multi \n" +
                    " ON multi.FIELD_VAL=poz.ID AND multi.FIELD_CN='ZAMOWIENIAPOZ'\n" +
                    " LEFT JOIN INS_BUDZETPOZ AS b ON b.ID=poz.POZYCJA_BUDZETU \n" +
                    " WHERE multi.DOCUMENT_ID="+documentId;
			ResultSet result = DSApi.context().prepareStatement(sql).executeQuery();
			
			while(result.next()){
				ZamawianyTowar pozycjaZamowienia = new ZamawianyTowar();
				
				pozycjaZamowienia.produktId = result.getLong("PRODUKT");
				pozycjaZamowienia.ilosc = result.getBigDecimal("ILOSC");
				BigDecimal vat = new BigDecimal(DataBaseEnumField.getEnumItemForTable("simple_erp_per_docusafe_vatstaw_view"
                        , result.getInt("VAT")).getCn());
				if(!vat.equals(new BigDecimal(0)))
					pozycjaZamowienia.brutto = result.getBigDecimal("NETTO").setScale(2, RoundingMode.HALF_UP)
                            .multiply(vat.setScale(2, RoundingMode.HALF_UP).divide(new BigDecimal(100)).add(new BigDecimal(1)));
				else
					pozycjaZamowienia.brutto = result.getBigDecimal("NETTO").setScale(2, RoundingMode.HALF_UP);
				pozycjaZamowienia.indeks = result.getLong("IND");

                pozycjaZamowienia.uwagi = result.getString("OPIS");


				if(fm.getBoolean("KOSZTY_WYDZIAL"))
					pozycjaZamowienia.dotyczy = "Koszt wydziałowy";
                else{
                    String dot = "";
                    Integer projektId = result.getInt("PROJEKT");
                    Integer zleceniaId = result.getInt("ZLECENIA");
                    Integer mpkId = result.getInt("MPK");

                    if (projektId != null && projektId != 0){
                        dot+="Projekt: "+DataBaseEnumField.getEnumItemForTable(
                                WniosekZapDoDostLogic.WIDOK_POZYCJE_BUDZETOWE_KONTRAKT, projektId).getTitle();
                    } else
                    if (zleceniaId != null && zleceniaId != 0){
                        dot+="Zlecenie: "+DataBaseEnumField.getEnumItemForTable(
                                "simple_erp_per_docusafe_zlecprod_view", zleceniaId).getTitle();
                    } else
                    if (mpkId != null && mpkId != 0){
                        dot+="Koszt wydziału: "+DataBaseEnumField.getEnumItemForTable(
                                "simple_erp_per_docusafe_mpk", mpkId).getTitle();
                    } else{
                        dot = "Koszt wydziałowy";
                    }

                    pozycjaZamowienia.dotyczy=dot;
                }
				
				wynik.add(pozycjaZamowienia);
			}
		} catch (Exception e){
			log.error(e.getMessage(), e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
		
		return wynik;
	}
    
    

	public static void przygotujPozycje(Long documentId, List<ZamawianyTowar> lista) {
    	boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();
			
			for(ZamawianyTowar zamawianyTowar: lista){
				PreparedStatement preparedStatement = DSApi.context().prepareStatement(
                        "INSERT INTO INS_WnioZapDoDost_ZAMOWIONY_TOWAR(DOCUMENT_ID,PRODUKT,ILOSC,BRUTTO,DOTYCZY,IND,UWAGI) " +
                                " VALUES(?,?,?,?,?,?,?)");
				
				preparedStatement.setLong(1, documentId);
				preparedStatement.setObject(2, zamawianyTowar.produktId);
				preparedStatement.setBigDecimal(3, zamawianyTowar.ilosc);
				preparedStatement.setBigDecimal(4, zamawianyTowar.brutto);
				preparedStatement.setString(5, zamawianyTowar.dotyczy);
				preparedStatement.setObject(6, zamawianyTowar.indeks);
                preparedStatement.setString(7, zamawianyTowar.uwagi);
				
				preparedStatement.executeUpdate();
			}
		} catch (Exception e){
			log.error(e.getMessage(), e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
	}

	public static void usunPozycje(Long documentId){
    	boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();
			
			DSApi.context().prepareStatement("DELETE FROM INS_WnioZapDoDost_ZAMOWIONY_TOWAR " +
                    " WHERE DOCUMENT_ID="+documentId).executeUpdate();
		} catch (Exception e){
			log.error(e.getMessage(), e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
    }

	@Override
	public void notify(EventListenerExecution execution) throws Exception {
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);

        usunPozycje(docId);
        
        List<ZamawianyTowar> lista = transformPozycjeZamowieniaToZamawianyTowar(docId, doc.getFieldsManager());
        
        przygotujPozycje(docId, lista);
	}

}
