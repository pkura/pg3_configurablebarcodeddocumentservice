package pl.compan.docusafe.parametrization.ins.jbpm;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ins.Util_AHE;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class AssignToDivisionFromAddsHandler implements AssignmentHandler {
    protected String guid;
    protected String user;
    protected String status; //np. wewnatrz forka mozna wpisac odpowiedni status do historii

    private static final Logger log = LoggerFactory.getLogger(AssignToDivisionFromAddsHandler.class);

    @Override
    public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
        Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);

        try {
            if (guid != null) {
                for (String g : guid.split(",")) {
                    String cel = Docusafe.getAdditionProperty(g);
                    assignable.addCandidateGroup(cel);
                    System.out.println("Asygnacja z adds: "+ g +": "+ cel);
                    Util_AHE.addToHistory(openExecution, doc, null, cel, status, log);
                }
            } else if (user != null) {
                if (user.equals("author")) {
                    assignable.addCandidateUser(doc.getAuthor());
                    System.out.println("Asygnacja z adds: autor");
                    Util_AHE.addToHistory(openExecution, doc, doc.getAuthor(), null, status, log);
                } else {
                    String cel = Docusafe.getAdditionProperty(user);
                    assignable.addCandidateUser(cel);
                    System.out.println("Asygnacja z adds: "+ user +": "+ cel);
                    Util_AHE.addToHistory(openExecution, doc, cel, null, status, log);
                }
            }
        }
        catch (EdmException e) {
            log.error(e.getMessage());
            throw e;
        }
    }
}
