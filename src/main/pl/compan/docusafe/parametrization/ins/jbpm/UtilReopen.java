package pl.compan.docusafe.parametrization.ins.jbpm;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.dbutils.DbUtils;
import org.directwebremoting.dwrp.CallBatch;
import org.jbpm.api.Execution;
import org.jbpm.api.ExecutionService;
import org.jbpm.api.ProcessInstance;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.util.Logger;

/** klasa utylitarna, przechowuje wsp�lny kod dla logik implementuj�cych interfejs LogicExtension_Reopen.
 * Napisane dla WSSK, gdzie procesy musz� by� po zako�czeniu wznawiane w dziwaczny spos�b,
 * np. proces wniosku o utworzenie kont systemowych po realizacji mo�e by� ponownie otwarty,
 * jego pola zmodyfikowane i dokument puszczony w obieg jeszcze raz.
 *
 * Rozszerzona logika umo�liwia edycj� "in place" wznawianego dokumentu przez osob� wznawiaj�c�.
 * Technicznie: dokument trafia od razu na list� zada� 1. uczestnika procesu, ale wznawiaj�cy (ju� po tym fakcie!!!)
 * mo�e go przez chwil� modyfikowa� i zapisa� - nie najszcz�liwsze rozwi�zanie, ale nie widz� lepszego.
 * @author Rafal Odoj
 * @date 2012-10-08 */
public class UtilReopen {
    /** Szuka procesu o danej nazwie i w podanym stanie (activity) dla podanego id dokumentu.
     * Jesli jest ich wi�cej, rzuca wyj�tek.
     * Je�li nie ma �adnego, zwraca null. */
    public static String processInStateForDocumentId(String processName, String activity, long documentId, Logger log) throws EdmException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        String jbpmID=null;
        String activityDB;

        //  SELECT jbpm4_id, ACTIVITYNAME_ FROM ds_document_to_jbpm4, JBPM4_EXECUTION
        //  WHERE jbpm4_id=ID_ AND document_id=59 AND process_name='reopen'
        //samo ds_document_to_jbpm4 nie wystarczy, bo zawiera te� stare, zamkni�te procesy
        String sql = "SELECT jbpm4_id, ACTIVITYNAME_ FROM ds_document_to_jbpm4, JBPM4_EXECUTION "
                +"WHERE jbpm4_id=ID_ AND document_id = ? AND process_name = ?";
        try {
            ps = DSApi.context().prepareStatement(sql);
            ps.setLong(1, documentId);
            ps.setString(2, processName);
            rs = ps.executeQuery();

            while (rs.next()) {

                activityDB = rs.getString(2);

                if ( activityDB==null && activity!=null ) // puste activity, a nie takie bylo szukane
                    continue;

                if (jbpmID != null)
                    err("Wi�cej ni� jeden �ywy proces o nazwie " +processName +" dla dokumentu o ID=" +documentId, log);

                jbpmID = rs.getString(1);
            }
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            DSApi.context().closeStatement(ps);
            DbUtils.closeQuietly(rs);
        }
        return jbpmID;
    }

    /** error do logu i wyjatek Edm */
    private static void err (String s, Logger log) throws EdmException {
        log.error(s);
        throw new EdmException(s);
    }
}
