package pl.compan.docusafe.parametrization.ins.jbpm;

import org.jbpm.api.Execution;
import org.jbpm.api.ExecutionService;
import org.jbpm.api.ProcessInstance;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.util.Logger;

/** uogolnienie narz�dzi z Util_Reopen */
public class UtilTrigger {

    /** Wybija proces ze stanu czekania (STATE), przesy�a do procesu podany sygna� (albo domy�lny jesli NULL)
     * @param processName - nazwa procesu, po prostu CN
     * @param activity - nazwa elementu JBPM typu STATE okre�lona w JPDL - spodziewany aktualny stan procesu
     * @param documentId - id dokumentu, kt�rego proces jest szukany, w postaci Long
     * @param sygnal - nazwa transition, kt�rym nale�y opu�ci� stan czekania
     * @param log - logger z klasy wywo�uj�cej, �eby nie tworzy� nowego co chwil�
     * @throws EdmException */
    public static void resume(String processName, String activity, long documentId, String sygnal, Logger log) throws EdmException{

        String processInstanceID = UtilReopen.processInStateForDocumentId(processName, activity, documentId, log);

        ExecutionService executionService = Jbpm4Provider.getInstance().getExecutionService();

        ProcessInstance processInstance = executionService.findProcessInstanceById(processInstanceID);
        if (processInstance==null)
            err("Nie znaleziono procesu w stanie '" +activity+ "' o nazwie '" +processName +"' dla dokumentu o ID=" +documentId, log);
        Execution executionWaiting = processInstance.findActiveExecutionIn(activity);
        if (executionWaiting==null)
            return; // Nie znaleziono, wiec widocznie takiego juz nie ma. Przeciez metoda mogla byc wywolana kilka razy pod rzad przy przeladowaniu strony.
//			err("Nie znaleziono 'execution' dla procesu w stanie '" +activity+ "' o nazwie '" +processName +"' dla dokumentu o ID=" +documentId, log);

        //wybudzenie:
        if (sygnal==null)
            executionService.signalExecutionById(executionWaiting.getId());
        else
            executionService.signalExecutionById(executionWaiting.getId(), sygnal);
        TaskSnapshot.updateByDocument( OfficeDocument.find(documentId) );
    }

    /** error do logu i wyjatek Edm */
    private static void err (String s, Logger log) throws EdmException {
        log.error(s);
        throw new EdmException(s);
    }
}
