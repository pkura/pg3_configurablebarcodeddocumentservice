package pl.compan.docusafe.parametrization.ins;

import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;


public class CzekajNaERP implements ExternalActivityBehaviour{
	
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(CzekajNaERP.class);

	private String signal = null;
	private String alter  = null;
	
	@Override
	public void execute(ActivityExecution activityExecution) throws Exception {
		String obecneID = activityExecution.getId();
		activityExecution.waitForSignal();
		MyThread t=new MyThread(activityExecution);
		t.start();
	}

	@Override
	public void signal(ActivityExecution activityExecution, String s, Map<String, ?> arg2)
			throws Exception {
		if (s == null){
			log.debug("nieprawidlowy sygnal (NULL)");
			return;
		} else {
			Long documentId = Long.valueOf( activityExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			if (s.equals(signal)){
				//otrzymano oczekiwany sygnal, isc dalej
					
				activityExecution.takeDefaultTransition();
				activityExecution.take(signal);
				TaskSnapshot.updateByDocument(OfficeDocument.find(documentId));
			} else {
				//otrzymano inny niz oczekiwany sygnal, isc dalej, ale sciezka alternatywna
				activityExecution.take(alter);
				TaskSnapshot.updateByDocument(OfficeDocument.find(documentId));
			}
		}
		
	}
	
	class MyThread extends Thread {
		ActivityExecution activityExecution;
		public MyThread(ActivityExecution activityExecution) {
			this.activityExecution=activityExecution;
		}

		public void run() {
			try {
				sleep(5000);
				
				try {
					signal(activityExecution,"zrealizowane",null);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		}

}
