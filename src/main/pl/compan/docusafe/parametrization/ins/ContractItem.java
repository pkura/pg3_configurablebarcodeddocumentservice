package pl.compan.docusafe.parametrization.ins;

import pl.compan.docusafe.parametrization.ins.Contract;

import java.math.BigDecimal;

public class ContractItem {
	private Long id;
	private String name;
	private String description;
	private String unit;
	private Long quantity;
	private BigDecimal netAmount;
	private BigDecimal vatAmount;
	private Contract contract;
	
	public ContractItem() {
		
	}
	
	public ContractItem(Long id, String name, String description, String unit, Long quantity, BigDecimal netAmount, BigDecimal vatAmount) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.unit = unit;
		this.quantity = quantity;
		this.netAmount = netAmount;
		this.vatAmount = vatAmount;
	}
	
	public ContractItem(String name, String description, String unit, Long quantity, BigDecimal netAmount, BigDecimal vatAmount) {
		this.name = name;
		this.description = description;
		this.unit = unit;
		this.quantity = quantity;
		this.netAmount = netAmount;
		this.vatAmount = vatAmount;
	}

	public ContractItem(String name, String description, String unit, Long quantity, BigDecimal netAmount, BigDecimal vatAmount, Contract contract) {
		this.name = name;
		this.description = description;
		this.unit = unit;
		this.quantity = quantity;
		this.netAmount = netAmount;
		this.vatAmount = vatAmount;
		this.contract = contract;
	}

	public void setBaseContractItemFields (ContractItem item) {
		this.name = item.name;
		this.description = item.description;
		this.unit = item.unit;
		this.quantity = item.quantity;
		this.netAmount = item.netAmount;
		this.vatAmount = item.vatAmount;
		this.contract = item.contract;
	}
	
	@Override
	public String toString() {
		return "ContractItem [id=" + id + ", name=" + name + ", description=" + description + ", unit=" + unit + ", quantity=" + quantity + ", netAmount="
				+ netAmount + ", vatAmount=" + vatAmount + ", contract=" + contract + "]";
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getUnit() {
		return unit;
	}


	public void setUnit(String unit) {
		this.unit = unit;
	}


	public Long getQuantity() {
		return quantity;
	}


	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getNetAmount() {
		return netAmount;
	}


	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}


	public BigDecimal getVatAmount() {
		return vatAmount;
	}


	public void setVatAmount(BigDecimal vatAmount) {
		this.vatAmount = vatAmount;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
}
