package pl.compan.docusafe.parametrization.ins;


import java.util.Date;

import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.utp.OfficeUtils;
import pl.compan.docusafe.util.Logger;

public class Util_AHE {

    /** Dodaje historie dekretacji, na dzial lub na uzytkownika
     * @param openExecution proces, w ktorym sie dekretuje
     * @param doc dokument poddany dekretacji
     * @param user nazwa uzytkownika, na ktorego dekretowac lub "author"
     * @param guid dokladny guid grupy, moze byc kilka po przecinku
     * @param status status do zapamietania, moze byc null
     * @param log Logger oryginalnej klasy, ktora korzysta z tej metody, moze byc null
     * @throws EdmException
     * @throws UserNotFoundException
     */
    public static void addToHistory(OpenExecution openExecution, OfficeDocument doc, String user, String guid, String status, Logger log) throws EdmException, UserNotFoundException {
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setSourceUser(DSApi.context().getPrincipalName());
        ahe.setProcessName(openExecution.getProcessDefinitionId());
        if (user == null) {
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_DIVISION);
            ahe.setTargetGuid(guid);
        } else {
            ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_SINGLE_USER);
            ahe.setTargetUser(user);
        }
        DSDivision[] divs = DSApi.context().getDSUser().getOriginalDivisionsWithoutGroup();
        if (divs != null && divs.length > 0)
            ahe.setSourceGuid(divs[0].getName());
        ahe.setCtime(new Date());
        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);

        //zapisanie statusu. Podany z zewnatrz albo pobrany z formatki
        try {
            String statusHist = status;
            if ( ! OfficeUtils.isValidString(statusHist) )
                statusHist = doc.getFieldsManager().getEnumItem(CostInvoiceLogic.STATUS_DOCKIND_CN).getTitle();
            ahe.setStatus(status);
        } catch (Exception e) {
            if (log != null)
                log.warn(e.getMessage());
        }
        doc.addAssignmentHistoryEntry(ahe);
    }
}
