package pl.compan.docusafe.parametrization.ins;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Strings;

import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.Field.Type;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogicUtils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.opensymphony.webwork.ServletActionContext;

public class CostInvoiceLogic extends AbstractInsDocumentLogic
{
	// ladowanie kierownik�w oraz sekcji finansowej
	protected static final List<String> divShortcut = new ArrayList<String>();

    public static final String ERP_SPOSOB_TABLENAME = "simple_erp_per_docusafe_warpla_view";
    public static final String ERP_WALUTA_TABLENAME = "simple_erp_per_docusafe_waluta_view";
    public static final String ERP_STRUKTURA_BUDZETOWA_TABLENAME = "simple_erp_per_docusafe_bd_str_budzet_view";
    public static final String ERP_BUDZETY_TABLENAME = "simple_erp_per_docusafe_bd_budzety_view";
    public static final String ERP_POZYCJE_BUDZETOWE_TABLENAME = "simple_erp_per_docusafe_bd_pozycje_view";
    public static final String ERP_ZRODLA_BUDZETOWE_TABLENAME = "simple_erp_per_docusafe_bd_zrodla_view";
    public static final String ERP_RODZAJ_FAKTURY_WALUTOWEJ_TABLENAME = "simple_erp_per_docusafe_typdokzak_wal_view";
    public static final String ERP_RODZAJ_FAKTURY_PLN_TABLENAME = "simple_erp_per_docusafe_typdokzak_pln_view";
    public static final String ERP_WYTWORY_TABLENAME = "simple_erp_per_docusafe_wytwor_view";
    public static final String ERP_KOMORKI_TABLENAME = "simple_erp_per_docusafe_komorka_view";
    public static final String ERP_ZLECENIA_TABLENAME = "simple_erp_per_docusafe_zlecprod_view";
    public static final String ERP_STAWKI_VAT_TABLENAME = "simple_erp_per_docusafe_vatstaw_view";
    //tabele zwiazane z budzetowaniem projekt�w
    public static final String ERP_PROJEKTY_TABLENAME = "simple_erp_per_docusafe_projekty";
    public static final String ERP_BUDZETY_PROJEKTOW_TABLENAME = "simple_erp_per_docusafe_budzety_projektow";
    public static final String ERP_ETAPY_PROJEKTOW_TABLENAME = "simple_erp_per_docusafe_etapy_projektow";
    public static final String ERP_ZASOBY_ETAPOW_TABLENAME = "simple_erp_per_docusafe_zasoby_projektow";
    public static final String ERP_PRACOWNICY_TABLENAME = "simple_erp_per_docusafe_pracownicy";



    //POLA DOCKIND'owe DLA FAKTURY
    public static final String WALUTA_DOCKIND_CN = "WALUTA";
    public static final String TYP_FAKTURY_WAL_DOCKIND_CN = "TYP_FAKTURY_WAL";
    public static final String TYP_FAKTURY_PLN_DOCKIND_CN = "TYP_FAKTURY_PLN";
    public static final String KURS_DOCKIND_CN = "KURS";
    public static final String KWOTA_VAT_DOCKIND_CN = "VAT";
    public static final String KWOTA_NETTO_DOCKIND_CN = "KWOTA_NETTO";
    public static final String KWOTA_NETTO_WALBAZ_DOCKIND_CN = "KWOTA_W_WALUCIE";
    public static final String KWOTA_BRUTTO_DOCKIND_CN = "KWOTA_BRUTTO";
    public static final String DATA_WPLYWU_DOCKIND_CN = "F_DATA_WPLYWU";
    public static final String STANOWISKO_DOSTAW_DOCKIND_CN = "STANOWISKO_DOSTAW";
    public static final String PRACOWNIK_DOCKIND_CN = "PRACOWNIK_DELEGACJA";
    public static final String ZLECENIA_DOCKIND_CN = "ZLECENIA";

    public static final String MPK_ID_DOCKIND_CN = "ID";
    public static final String MPK_POZID_DOCKIND_CN = "POZID";
    public static final String MPK_KOMORKAID_DOCKIND_CN = "KOMORKAID";
    public static final String MPK_BUDZETID_DOCKIND_CN = "BUDZETID";
    public static final String MPK_POZYCJAID_DOCKIND_CN = "POZYCJAID";
    public static final String MPK_PROJEKT_DOCKIND_CN = "KONTRAKT";
    public static final String MPK_BUDZET_PROJEKTU_DOCKIND_CN = "BUDZET";
    public static final String MPK_ETAP_PROJEKTU_DOCKIND_CN = "FAZA";
    public static final String MPK_ZASOB_PROJEKTU_DOCKIND_CN = "ZASOB";

    public static final String STAWKI_VAT_PRZEZNACZENIE_DOCKIND_CN = "PRZEZNACZENIE";

    public static final String STAWKI_VAT_ID_DOCKIND_CN = "ID";
    public static final String STAWKI_VAT_DOCKIND_CN = "STAWKI_VAT";
    public static final String STAWKI_VAT_POZYCJAID_DOCKIND_CN = "POZYCJAID";
    public static final String STAWKI_VAT_POZYCJA_DOCKIND_CN = "POZYCJA";
    public static final String STAWKI_VAT_NETTO_DOCKIND_CN = "NETTO";
    public static final String STAWKI_VAT_STAWKA_DOCKIND_CN = "STAWKA";
    public static final String STAWKI_VAT_KWOTA_VAT_DOCKIND_CN = "KWOTA_VAT";
    public static final String STAWKI_VAT_BRUTTO_DOCKIND_CN = "BRUTTO";
    public static final String STAWKI_VAT_ZLECENIA_DOCKIND_CN = "ZLECENIA";
	
	private static final String DATA_PLATNOSCI_ERROR_MSG = "Data p�atno�ci nie mo�e by� wcze�niejsza ni� data wystawienia";
	private static final String DATA_WPLYWU_ERROR_MSG = "Data wp�ywu nie mo�e by� wcze�niejsza ni� data wystawienia";
	private static final String DATA_WPLYWU_TERAZ_ERROR_MSG = "Data wp�ywu nie mo�e by� p�niejsza od dzisiejszego dnia";

	protected static Logger log = LoggerFactory.getLogger(CostInvoiceLogic.class);
	private StringManager sm = GlobalPreferences.loadPropertiesFile(CostInvoiceLogic.class.getPackage().getName(), null);
	
	private static CostInvoiceLogic instance;
	
	public static CostInvoiceLogic getInstance()
	{
		if (instance == null)
			instance = new CostInvoiceLogic();
		return instance;
	}

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> values = new HashMap<String, Object>();
        values.put("F_DATA_WPLYWU", new Date(System.currentTimeMillis()));
        values.put("STATUS", 10);
        values.put("KWOTA_POZOSTALA", BigDecimal.ZERO);
        values.put(STANOWISKO_DOSTAW_DOCKIND_CN, 5);
        fm.reloadValues(values);
        super.setInitialValues(fm, type);
    }

    public void doDockindEvent(DockindButtonAction eventActionSupport, ActionEvent event, Document document, String activity, Map<String, Object> values, Map<String, Object> dockindKeys) throws EdmException
	{
		 ServletActionContext.getResponse().setContentType("text/rtf"); 
           Map<String, Object> params = new HashMap<String, Object>();
           DocFacade df= Documents.document(document.getId());
           params.put("fields", df.getFields());
           df.getDocument().getDocumentKind().logic().setAdditionalTemplateValues(df.getId(), params);
           String templateName = "FakturaKosztowaAkceptacje.rtf";
           ServletActionContext.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + templateName + "\"");
           try
           { 
          	 pl.compan.docusafe.core.templating.Templating.rtfTemplate(templateName, params, ServletActionContext.getResponse().getOutputStream(),"true");
           }
           catch (Exception e)
           {
          	 throw new EdmException(e.getMessage());
           }
	}

	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		log.info("Before Search - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.contains("CPVNR"))
		{
			values.put(dictionaryName + "BASE", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.INTEGER, 1));
		}
	}

    /** Wywolywane za ka�dym razem, po tym jak u�ytkownik zmieni
     * warto�� pola w s�owniku (wybierze pozycj� z enum, database
     * lub sko�czy wpisywa� warto�� z klawiatury).
     *
     * S�u�y do aktualizacji p�l tego s�ownika.
     *
     * Wywo�ywane wcze�niej ni� validateDwr.
     *
     * @param dictionaryName - nazwa tego s�ownika, bez przedrostka DWR_
     * @param values - tylko i wy��cznie wartosci jednego rekordu, w kt�rym nast�pi�a zmiana,
     * bez dost�pu do reszty p�l, innych s�ownik�w i innych rekord�w tego s�ownika */
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues, Long documentId) {
        Map<String, Object> results = new HashMap<String, Object>();

        if(dictionaryName.equals("MPK")){
            getAvailableItems(values,fieldsValues,results,dictionaryName+"_"+MPK_KOMORKAID_DOCKIND_CN,
                    dictionaryName+"_"+MPK_BUDZETID_DOCKIND_CN,ERP_BUDZETY_TABLENAME,MPK_KOMORKAID_DOCKIND_CN);

            getAvailableItems(values,fieldsValues,results,dictionaryName+"_"+MPK_BUDZETID_DOCKIND_CN,
                    dictionaryName+"_"+MPK_POZYCJAID_DOCKIND_CN,ERP_POZYCJE_BUDZETOWE_TABLENAME,MPK_BUDZETID_DOCKIND_CN);

            getAvailableItems(values,fieldsValues,results,dictionaryName+"_"+MPK_PROJEKT_DOCKIND_CN,
                    dictionaryName+"_"+MPK_BUDZET_PROJEKTU_DOCKIND_CN,ERP_BUDZETY_PROJEKTOW_TABLENAME,MPK_PROJEKT_DOCKIND_CN);

            getAvailableItems(values,fieldsValues,results,dictionaryName+"_"+MPK_BUDZET_PROJEKTU_DOCKIND_CN,
                    dictionaryName+"_"+MPK_ETAP_PROJEKTU_DOCKIND_CN,ERP_ETAPY_PROJEKTOW_TABLENAME,MPK_BUDZET_PROJEKTU_DOCKIND_CN);

            getAvailableItems(values,fieldsValues,results,dictionaryName+"_"+MPK_ETAP_PROJEKTU_DOCKIND_CN,
                    dictionaryName+"_"+MPK_ZASOB_PROJEKTU_DOCKIND_CN,ERP_ZASOBY_ETAPOW_TABLENAME,MPK_ETAP_PROJEKTU_DOCKIND_CN);
        }
        try{
	        if(dictionaryName.equals("ZAMOWIENIA_POZYCJE")){
				FieldData iloscZrealizowana=values.get("ZAMOWIENIA_POZYCJE_ILOSC_ZREALIZOWANA");
				FieldData iloscZamowiona=values.get("ZAMOWIENIA_POZYCJE_ILOSC_ZAMOWIONA");
				FieldData wartosc=values.get("ZAMOWIENIA_POZYCJE_WARTOSC");
				FieldData stawkaVat=values.get("ZAMOWIENIA_POZYCJE_STAWKA_VAT");
				
				if(iloscZrealizowana.getData()!=null && stawkaVat.getData()!=null && iloscZrealizowana.getIntegerData()!=0){
					EnumValues ev=stawkaVat.getEnumValuesData();
					if(ev.getSelectedId()!=null && !ev.getSelectedId().equals("")){
						DataBaseEnumField dbef=DataBaseEnumField.getEnumFiledForTable("simple_erp_per_docusafe_vatstaw_view");
						EnumItem e=dbef.getEnumItem(Integer.parseInt(ev.getSelectedId()));
	
						BigDecimal wartoscZrealizowana = wartosc.getMoneyData().multiply(new BigDecimal(iloscZrealizowana.getIntegerData()).setScale(2,RoundingMode.HALF_UP));
						
						results.put("ZAMOWIENIA_POZYCJE_WARTOSC_ZREALIZOWANA", wartoscZrealizowana);
						results.put("ZAMOWIENIA_POZYCJE_WARTOSC_VAT", wartoscZrealizowana.multiply(new BigDecimal(e.getCn())).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP));
					}
				}
	        }
        }
        catch(Exception e){}
        
        // wylczenie obciazenia dla danej stawki vat, w s�owniku Stawki VAT
        fillStawkiVat_Obciazenie(dictionaryName, values, results);

        fillPrzeznaczenieZakupu(dictionaryName, values, fieldsValues, results, documentId);

        return results;
    }

    private void fillPrzeznaczenieZakupu(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues, Map<String, Object> results, Long docId) {
        if (dictionaryName.equals("MPK"))
        {
            Object projekt = values.get("MPK_KONTRAKT").getData();
//            if (projekt == null)
//                return;

//            Object przeznzak = values.get("MPK_PRZEZNACZENIE").getData();
//            if (przeznzak != null)
//                return;
            Object komorkaMpk = values.get("MPK_MPK").getData();

            try {
                String projektName = projekt != null ?DataBaseEnumField.getEnumItemForTable(
                        ERP_PROJEKTY_TABLENAME,
                        Integer.valueOf((String) projekt)).getTitle() : null;
                String mpkName = komorkaMpk !=null ? DataBaseEnumField.getEnumItemForTable(
                        "simple_erp_per_docusafe_mpk",
                        Integer.valueOf((String) komorkaMpk)).getTitle() : null;

                Map<String, String> enumValues = new LinkedHashMap<String, String>();
                enumValues.put("0","Sprzeda� opodatkowana");
                enumValues.put("1","Sprzeda� nieopodatkowana");
                enumValues.put("2","Nieokre�lone");

                List<String> enumSelected = new ArrayList<String>();
                if(mpkName != null && mpkName.startsWith("B")){
                  enumSelected.add("2");
                    results.put("MPK_PRZEZNACZENIE", new EnumValues(enumValues, enumSelected));
                }else if(projektName != null && (projektName.startsWith("0.") || projektName.startsWith("6.")
                        || projektName.startsWith("9."))){
                    enumSelected.add("1");
                    results.put("MPK_PRZEZNACZENIE", new EnumValues(enumValues, enumSelected));
                }else{
                    enumSelected.add("0");
                    results.put("MPK_PRZEZNACZENIE", new EnumValues(enumValues, enumSelected));
                }
            } catch (Exception e) {
                log.error(e.getMessage(),e);
            }

//            FieldData mpkId = ((FieldData)((Map) values).get("MPK_ID"));
//            Object id = mpkId.getData();
//            Object pozid = values.get("MPK_POZID").getData();
//            if (pozid == null)
//                return;
//
//            Object netto = values.get("MPK_NETTO").getData();
//            if (netto != null)
//                return;

//            BigDecimal nettoSV = null;
//            FieldsManager fm = null;
//            try {
//                fm = OfficeDocument.find(docId).getFieldsManager();
//                if (fm!=null){
//                    List positions = (List) fm.getValue("STAWKI_VAT");
//                    for(Object o : positions) {
//                        if (((Map) o).get("id").equals(pozid))
//                            nettoSV = (BigDecimal) ((Map) o).get("STAWKI_VAT_NETTO");
//                    }
//
//                    List mpks = (List) fm.getValue("MPK");
//                    if (mpks!=null){
//                        for(Object o : mpks){
//                            if (!((Map) o).get("id").equals(id)){
//                                BigDecimal inneNetto= (BigDecimal) ((Map) o).get("MPK_NETTO");
//                                nettoSV = nettoSV.add(inneNetto.negate());
//                            }
//
//                        }
//                    }
//                }
//            } catch (EdmException e) {
//                log.error(e.getMessage(), e);
//            }

            try
            {
//                results.put("MPK_NETTO", nettoSV);
//                MpkDictLogic mpkDict = new MpkDictLogic();
//                if (id == null)
//                    mpkDict.add(values);
//                else
//                    mpkDict.update(values);
            }
            catch (Exception e)
            {
                log.error(e.getMessage(), e);
            }

        }
    }

    private void getAvailableItems(Map<String, FieldData> values,
                                   Map<String, Object> fieldsValues,
                                   Map<String, Object> results,
                                   String dictCn,
                                   String dependentDictCn,
                                   String dependentDictTablename,
                                   String fieldCn) {
        FieldData dictField = values.get(dictCn);
        if(dictField != null){
            EnumValues dictEnum = dictField.getEnumValuesData();
            String wybranyEnumItem = dictEnum.getSelectedOptions().get(0);

            if(!Strings.isNullOrEmpty(wybranyEnumItem))
            {
                /** zawezanie pozycji */
                String dependendId= values.get(dependentDictCn).getEnumValuesData().getSelectedOptions().get(0);
                fieldsValues.put(fieldCn+"_1", wybranyEnumItem);
                getAvailableEnumItemsForField(fieldsValues, results, dependentDictTablename, dependentDictCn, dependendId);
            }
        }
    }

    private void getAvailableEnumItemsForField(Map<String, Object> fieldsValues, Map<String, Object> results, String tabelaPodrzedna, String cnPolaPodrzednego, String selectedItem){
        String MPK_DICT	= "MPK";
        boolean containsPozycjaId = false;

        for (DataBaseEnumField d : DataBaseEnumField.tableToField.get(tabelaPodrzedna)) {
            EnumValues val = null;
            try {
                val = d.getEnumItemsForDwr(fieldsValues); // element aktualnie poprawne (po uwzgl�dnieniu refValue)
            } catch (EntityNotFoundException e) {
                e.printStackTrace();
            }

            results.put(cnPolaPodrzednego, val);

            for (Map<String,String> map : val.getAllOptions()) {
                if(map.containsKey(selectedItem)) {
                    containsPozycjaId = true;
                    break;
                }
            }
            break;
        }

        if ( ! Strings.isNullOrEmpty( selectedItem ) ) { // jesli cokolwiek bylo wybrane
            List<String> selectedPositionId = new ArrayList<String>(); // wybrany element
            if (containsPozycjaId)
                selectedPositionId.add(selectedItem);
            ((EnumValues) results.get(cnPolaPodrzednego)).setSelectedOptions(selectedPositionId);
        }
    }
    
    private void dodajPozycjeZamowienia(PozycjaZamowienia pozycjaZamowienia){
    	boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();
			
			PreparedStatement preparedStatement = DSApi.context().prepareStatement("INSERT INTO dsg_ins_costinvoice_zamowienia_pozycje(POZ_ID, ZAMDOST_ID, NAZWA, ILOSC_ZAMOWIONA, ILOSC_ZREALIZOWANA, WARTOSC, STAWKA_VAT, WARTOSC_VAT, PRODUKT, MPK_ID, PROJEKT_ID, BUDZET_PROJEKTU_ID, ETAP_ID, ZASOB_ID, ZLECENIA_ID, PRZEZNACZENIE_ID, WARTOSC_ZREALIZOWANA, DOCUMENT_ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			
			preparedStatement.setLong(1, pozycjaZamowienia.getIdPozycji());
			preparedStatement.setLong(2, pozycjaZamowienia.getIdZamowienia());
			preparedStatement.setString(3, pozycjaZamowienia.getNazwa());
			preparedStatement.setObject(4, pozycjaZamowienia.getIloscZamowiona());
			preparedStatement.setObject(5, pozycjaZamowienia.getIloscZrealizowana());
			preparedStatement.setBigDecimal(6, pozycjaZamowienia.getWartosc());
			preparedStatement.setObject(7, pozycjaZamowienia.getStawkaVat());
			preparedStatement.setBigDecimal(8, pozycjaZamowienia.getWartoscVat());
			preparedStatement.setString(9, pozycjaZamowienia.getProdukt());
			preparedStatement.setObject(10, pozycjaZamowienia.getMpkId());
			preparedStatement.setObject(11, pozycjaZamowienia.getProjektId());
			preparedStatement.setObject(12, pozycjaZamowienia.getBudzetProjektuId());
			preparedStatement.setObject(13, pozycjaZamowienia.getEtapId());
			preparedStatement.setObject(14, pozycjaZamowienia.getZasobId());
			preparedStatement.setObject(15, pozycjaZamowienia.getZleceniaId());
			preparedStatement.setObject(16, pozycjaZamowienia.getPrzeznaczenieId());
			preparedStatement.setBigDecimal(17, pozycjaZamowienia.getWartoscZrealizowana());
			preparedStatement.setLong(18, pozycjaZamowienia.getDocumentId());
			
			preparedStatement.executeUpdate();
		} catch (Exception e){
			log.error(e.getMessage(), e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
    }
    
    private List<PozycjaZamowienia> pobierzPozycjeDlaZamowienia(Long idZamowienia){
    	List<PozycjaZamowienia> wynik = new ArrayList<PozycjaZamowienia>();

		boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();
			
			ResultSet result = DSApi.context().prepareStatement("SELECT * FROM simple_erp_per_docusafe_zamdostpoz_view WHERE ZAMOWIENIE_ID="+idZamowienia).executeQuery();
			
			while(result.next()){
				PozycjaZamowienia pozycjaZamowienia = new PozycjaZamowienia();
				
				pozycjaZamowienia.setIdPozycji(result.getLong("ID"));
				pozycjaZamowienia.setIdZamowienia(idZamowienia);
				pozycjaZamowienia.setIloscZamowiona(result.getInt("ILOSC_ZAMOWIONA"));
				pozycjaZamowienia.setIloscZrealizowana(0);
				pozycjaZamowienia.setNazwa(result.getString("NAZWA"));
				pozycjaZamowienia.setStawkaVat(result.getInt("STAWKA_VAT"));
				pozycjaZamowienia.setWartosc(result.getBigDecimal("WARTOSC"));
				pozycjaZamowienia.setWartoscVat(new BigDecimal(0));
				pozycjaZamowienia.setProdukt(result.getString("PRODUKT"));
				pozycjaZamowienia.setMpkId(result.getLong("MPK_ID"));
				pozycjaZamowienia.setProjektId(result.getLong("PROJEKT_ID"));
				pozycjaZamowienia.setBudzetProjektuId(result.getLong("BUDZET_PROJEKTU_ID"));
				pozycjaZamowienia.setEtapId(result.getLong("ETAP_ID"));
				pozycjaZamowienia.setZasobId(result.getLong("ZASOB_ID"));
				pozycjaZamowienia.setZleceniaId(result.getLong("ZLECENIA_ID"));
				pozycjaZamowienia.setPrzeznaczenieId(result.getLong("PRZEZNACZENIE_ID"));
				pozycjaZamowienia.setWartoscZrealizowana(new BigDecimal(0)); 
				
				wynik.add(pozycjaZamowienia);
			}
			
		} catch (Exception e){
			log.error(e.getMessage(), e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
        
        return wynik;
    }
    
    private void usunPozycjeZagregowaneDlaZamowienia(Long idZamowienia, Long documentId){
    	boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();
			
			DSApi.context().prepareStatement("DELETE FROM dsg_ins_costinvoice_zamowienia_pozycje_rozliczenie WHERE ZAMDOST_ID="+idZamowienia+" AND DOCUMENT_ID="+documentId).executeUpdate();			
		} catch (Exception e){
			log.error(e.getMessage(), e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
    }
    
    private List<PozycjaZamowienia> pobierzPozycjeZamowienia(Long idZamowienia, Long documentId){
    	List<PozycjaZamowienia> wynik = new ArrayList<PozycjaZamowienia>();

		boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();
			
			ResultSet result = DSApi.context().prepareStatement("SELECT * FROM dsg_ins_costinvoice_zamowienia_pozycje WHERE ZAMDOST_ID="+idZamowienia+" AND DOCUMENT_ID="+documentId).executeQuery();
			
			while(result.next()){
				PozycjaZamowienia pozycjaZamowienia = new PozycjaZamowienia();
				
				pozycjaZamowienia.setId(result.getLong("ID"));
				pozycjaZamowienia.setIdPozycji(result.getLong("POZ_ID"));
				pozycjaZamowienia.setIdZamowienia(idZamowienia);
				pozycjaZamowienia.setIloscZamowiona(result.getInt("ILOSC_ZAMOWIONA"));
				pozycjaZamowienia.setIloscZrealizowana(0);
				pozycjaZamowienia.setNazwa(result.getString("NAZWA"));
				pozycjaZamowienia.setStawkaVat(result.getInt("STAWKA_VAT"));
				pozycjaZamowienia.setWartosc(result.getBigDecimal("WARTOSC"));
				pozycjaZamowienia.setWartoscVat(new BigDecimal(0));
				pozycjaZamowienia.setProdukt(result.getString("PRODUKT"));
				pozycjaZamowienia.setMpkId(result.getLong("MPK_ID"));
				pozycjaZamowienia.setProjektId(result.getLong("PROJEKT_ID"));
				pozycjaZamowienia.setBudzetProjektuId(result.getLong("BUDZET_PROJEKTU_ID"));
				pozycjaZamowienia.setEtapId(result.getLong("ETAP_ID"));
				pozycjaZamowienia.setZasobId(result.getLong("ZASOB_ID"));
				pozycjaZamowienia.setZleceniaId(result.getLong("ZLECENIA_ID"));
				pozycjaZamowienia.setPrzeznaczenieId(result.getLong("PRZEZNACZENIE_ID"));
				pozycjaZamowienia.setWartoscZrealizowana(new BigDecimal(0)); 
				
				wynik.add(pozycjaZamowienia);
			}
			
		} catch (Exception e){
			log.error(e.getMessage(), e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
        
        return wynik;
    }
    
    private List<PozycjaZagregowana> pobierzPozycjeZagregowaneDlaZamowienia(Long idZamowienia, Long documentId){
    	List<PozycjaZagregowana> wynik = new ArrayList<PozycjaZagregowana>();

		boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();
			
			ResultSet result = DSApi.context().prepareStatement("SELECT * FROM dsg_ins_costinvoice_zamowienia_pozycje_rozliczenie WHERE ZAMDOST_ID="+idZamowienia+" AND DOCUMENT_ID="+documentId).executeQuery();
			
			while(result.next()){
				PozycjaZagregowana pozycjaZagregowana = new PozycjaZagregowana();
				
				pozycjaZagregowana.setId(result.getLong("ID"));
				
				wynik.add(pozycjaZagregowana);
			}
			
		} catch (Exception e){
			log.error(e.getMessage(), e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
        
        return wynik;
    }

    private Long convertZeroToNull(Long liczba){
    	if(liczba==0L)
    		return null;
    	
    	return liczba;
    }
    
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
	{
		log.info("DocumentKind: {}, DocumentId: {}, Values from JavaScript: {}", fm.getDocumentKind().getCn(), fm.getDocumentId(), values);
 		StringBuilder msgBuilder = new StringBuilder();
		try
		{
			if(values.get("DWR_ZAMOWIENIE") != null && values.get("DWR_ZAMOWIENIE").isSender()){
				FieldData zamowieniaPozycje = values.get("DWR_ZAMOWIENIA_POZYCJE"); 
			
				Long idZamowienia = Long.valueOf(values.get("DWR_ZAMOWIENIE").getEnumValuesData().getSelectedId());
				
				List<Long> listaPozycji = new ArrayList<Long>();
				
				Map<String, FieldData> mapa = new HashMap<String, FieldData>();
				
				for(PozycjaZamowienia pozycjaZamowienia: pobierzPozycjeDlaZamowienia(idZamowienia)){
					pozycjaZamowienia.setDocumentId(fm.getDocumentId());
					dodajPozycjeZamowienia(pozycjaZamowienia); 
				}

				int i = 1;
				for(PozycjaZamowienia pozycjaZamowienia: pobierzPozycjeZamowienia(idZamowienia, fm.getDocumentId()))
					mapa.put("ZAMOWIENIA_POZYCJE_ID_"+(i++), new FieldData(Type.LONG, pozycjaZamowienia.getId())); 
				
				zamowieniaPozycje.setDictionaryData(mapa);
			}
			
			if(values.get("DWR_AKCEPTACJA_POZYCJI") != null && values.get("DWR_AKCEPTACJA_POZYCJI").isSender()){
				Long idZamowienia = Long.valueOf(values.get("DWR_ZAMOWIENIE").getEnumValuesData().getSelectedId());
				
				FieldData zamowieniaPozycjeZagregowane = values.get("DWR_MPK");
				
				if(zamowieniaPozycjeZagregowane.getDictionaryData().get("MPK_ID_1").getIntegerData()==null){
					FieldData zamowieniaPozycje = values.get("DWR_ZAMOWIENIA_POZYCJE");
					
					int i = 1;
					List<PozycjaZamowienia> list = new ArrayList<PozycjaZamowienia>();
					Map<String, FieldData> pozMap = zamowieniaPozycje.getDictionaryData();
					while(pozMap.containsKey("ZAMOWIENIA_POZYCJE_ID_"+i) && pozMap.get("ZAMOWIENIA_POZYCJE_ID_"+i)!=null){
						PozycjaZamowienia pozycjaZamowienia = new PozycjaZamowienia();
						
						pozycjaZamowienia.setId(pozMap.get("ZAMOWIENIA_POZYCJE_ID_"+i).getLongData());
						pozycjaZamowienia.setIdZamowienia(idZamowienia);
						pozycjaZamowienia.setIloscZamowiona(pozMap.get("ZAMOWIENIA_POZYCJE_ILOSC_ZAMOWIONA_"+i).getIntegerData());
						pozycjaZamowienia.setIloscZrealizowana(pozMap.get("ZAMOWIENIA_POZYCJE_ILOSC_ZREALIZOWANA_"+i).getIntegerData());
						pozycjaZamowienia.setStawkaVat(Integer.parseInt(pozMap.get("ZAMOWIENIA_POZYCJE_STAWKA_VAT_"+i).getEnumValuesData().getSelectedId()));
						pozycjaZamowienia.setWartosc(pozMap.get("ZAMOWIENIA_POZYCJE_WARTOSC_"+i).getMoneyData());
						pozycjaZamowienia.setWartoscVat(pozMap.get("ZAMOWIENIA_POZYCJE_WARTOSC_VAT_"+i).getMoneyData());
						pozycjaZamowienia.setMpkId(convertZeroToNull(pozMap.get("ZAMOWIENIA_POZYCJE_MPK_ID_"+i).getLongData()));
						pozycjaZamowienia.setProjektId(convertZeroToNull(pozMap.get("ZAMOWIENIA_POZYCJE_PROJEKT_ID_"+i).getLongData()));
						pozycjaZamowienia.setBudzetProjektuId(convertZeroToNull(pozMap.get("ZAMOWIENIA_POZYCJE_BUDZET_PROJEKTU_ID_"+i).getLongData()));
						pozycjaZamowienia.setEtapId(convertZeroToNull(pozMap.get("ZAMOWIENIA_POZYCJE_ETAP_ID_"+i).getLongData()));
						pozycjaZamowienia.setZasobId(convertZeroToNull(pozMap.get("ZAMOWIENIA_POZYCJE_ZASOB_ID_"+i).getLongData()));
						pozycjaZamowienia.setZleceniaId(convertZeroToNull(pozMap.get("ZAMOWIENIA_POZYCJE_ZLECENIA_ID_"+i).getLongData()));
						pozycjaZamowienia.setPrzeznaczenieId(convertZeroToNull(pozMap.get("ZAMOWIENIA_POZYCJE_PRZEZNACZENIE_ID_"+i).getLongData()));
						pozycjaZamowienia.setWartoscZrealizowana(pozMap.get("ZAMOWIENIA_POZYCJE_WARTOSC_ZREALIZOWANA_"+i).getMoneyData());
						pozycjaZamowienia.setIdPozycji(pozMap.get("ZAMOWIENIA_POZYCJE_POZ_ID_"+i).getLongData());
						pozycjaZamowienia.setDocumentId(fm.getDocumentId());
						
						if(pozMap.get("ZAMOWIENIA_POZYCJE_KONTO_"+i).getEnumValuesData().getSelectedId()!=null && !pozMap.get("ZAMOWIENIA_POZYCJE_KONTO_"+i).getEnumValuesData().getSelectedId().equals(""))
							pozycjaZamowienia.setKonto(Long.parseLong(pozMap.get("ZAMOWIENIA_POZYCJE_KONTO_"+i).getEnumValuesData().getSelectedId()));
						
						list.add(pozycjaZamowienia);
						i++;
					}
			
					usunPozycjeZagregowaneDlaZamowienia(idZamowienia, fm.getDocumentId());

					Map<Long, Boolean> nieagregowanePozycjeZamowienia = new HashMap<Long, Boolean>();
					
					for(PozycjaZamowienia pozycjaZamowienia: list){
						if(pozycjaZamowienia.getKonto() != null){
							String konto = DataBaseEnumField.getEnumItemForTable("simple_erp_per_docusafe_wytwor_view", pozycjaZamowienia.getKonto().intValue()).getCn();
							
							if(konto.startsWith("300-"))
								nieagregowanePozycjeZamowienia.put(pozycjaZamowienia.getKonto(), true);
						}
					}
						
					List<PozycjaZagregowana> pozycjeZagregowane = agregujPozycjeZamowienia(list, nieagregowanePozycjeZamowienia);
					
					for(PozycjaZagregowana pozycjaZagregowana: pozycjeZagregowane)
						dodajPozycjeZagregowane(pozycjaZagregowana);
					
					List<PozycjaZagregowana> pozycjeZagregowaneDlaZamowienia = pobierzPozycjeZagregowaneDlaZamowienia(idZamowienia, fm.getDocumentId());
					
					Map<String, FieldData> mapa = new HashMap<String, FieldData>();
					int y = 1;
					for(PozycjaZagregowana pozycjaZagregowana: pozycjeZagregowaneDlaZamowienia){
						mapa.put("MPK_ID_"+(y++), new FieldData(Type.LONG, pozycjaZagregowana.getId()));
					}
					
					zamowieniaPozycjeZagregowane.setDictionaryData(mapa);
				}
			}
			
				Set<String> budgetRealizationId = new HashSet<String>();
			if (values.get("DWR_MPK") != null && values.get("DWR_REALIZACJA_BUDZETU") != null) {
				loadBudgetRealizationItems(values, "MPK", budgetRealizationId);
				
				StringBuilder ids = new StringBuilder();
				
				for (String pozycja : budgetRealizationId) {
					if (ids.length() != 0) {
						ids.append(",");
					}
					ids.append(pozycja);
				}
				values.put("DWR_REALIZACJA_BUDZETU", new FieldData(Field.Type.STRING, ids.toString()));
			}
			if (fm.getEnumItemCn("STATUS") != null && fm.getEnumItemCn("STATUS").equals("opis_merytoryczny") && (DwrUtils.isNotNull(values, "DWR_RECIPIENT_HERE") || fm.getKey("RECIPIENT_HERE") != null))
			{
				if ( fm.getKey("RECIPIENT_HERE") != null && "".equals(values.get("DWR_RECIPIENT_HERE").getDictionaryData().get("id").getStringData()))
				{
					values.put("DWR_DZIAL", new FieldData(Field.Type.STRING,"-"));
					if (values.get("DWR_MPK") != null) 
					{
						List<Long> mpkIds = DwrUtils.extractIdFromDictonaryField(values, "MPK");
						if (mpkIds.size() == 0)
							values.put("DWR_MPK", new FieldData(Field.Type.STRING, ""));
					}
				}
				else
				{
					String recipientId = (String) fm.getKey("RECIPIENT_HERE");
					if (recipientId == null)
					{
						recipientId = values.get("DWR_RECIPIENT_HERE").getDictionaryData().get("id").getStringData();
						if (recipientId.equals(""))
							recipientId = "n:"+values.get("DWR_RECIPIENT_HERE").getDictionaryData().get("RECIPIENT_HERE_DIVISION").getStringData();
					}
					DSApi.openAdmin();
					try
					{
						String code = "-";
						DSDivision division = null;
						if (recipientId.contains("d:"))
						{
							int indexOf = recipientId.indexOf("d:");
							String guid = recipientId.substring(indexOf+2);
							division = DSDivision.find(guid);
							code = division.getCode();
						}
						else if (recipientId.contains("n:"))
						{
							int indexOf = recipientId.indexOf("n:");
							String name = recipientId.substring(indexOf+2);
							division = DSDivision.findByName(name);
							code = division.getCode();
						}
						else
						{
							Person adresatFaktury = Person.find(Long.parseLong(recipientId));
							division = DSDivision.findByName(adresatFaktury.getOrganizationDivision());
							code = division.getCode();
						}
						
						String name = division.getName();

						if (values.get("DWR_DZIAL") != null && (!values.get("DWR_DZIAL").getStringData().equals(code) || code.equals("-") || values.get("DWR_DZIAL").getStringData().equals("-")))
						{
							// dodanie psutek stringa jako wartosc slownika, wymusza pobranie dla niego ponownie wartosi -> Dictionary.getFieldsValues, tymczasowe obejscie.
							if (values.get("DWR_MPK") != null) 
							{
								List<Long> mpkIds = DwrUtils.extractIdFromDictonaryField(values, "MPK");
								if (mpkIds.size() == 0)
									values.put("DWR_MPK", new FieldData(Field.Type.STRING, ""));
							}
						}
						values.put("DWR_DZIAL", new FieldData(Field.Type.STRING, code));
					}
					catch (Exception e)
					{
						values.put("DWR_DZIAL", new FieldData(Field.Type.STRING,"-"));
						if (values.get("DWR_MPK") != null) 
						{
							List<Long> mpkIds = DwrUtils.extractIdFromDictonaryField(values, "MPK");
							if (mpkIds.size() == 0)
								values.put("DWR_MPK", new FieldData(Field.Type.STRING, ""));
						}
					}
					finally
					{
						DSApi.close();
					}
				}
			}
//			else
//			{
//				if (values.get("DWR_MPK") != null)
//				{
//					List<Long> mpkIds = DwrUtils.extractIdFromDictonaryField(values, "MPK");
//					if (mpkIds.size() == 0)
//						values.put("DWR_MPK", new FieldData(Field.Type.STRING, ""));
//				}
//				if (values.get("DWR_DZIAL") != null)
//					values.put("DWR_DZIAL", new FieldData(Field.Type.STRING, values.get("DWR_DZIAL").getStringData()));
//			}


            if (values.get("DWR_SPOSOB") != null && DwrUtils.isSenderField(values, "DWR_SPOSOB"))
                InvoiceLogicUtils.setTerminPlatnosci(values);

			if (values.get("DWR_KWOTA_W_WALUCIE") != null && values.get("DWR_KURS") != null && values.get("DWR_KWOTA_W_WALUCIE").getMoneyData() != null && values.get("DWR_KURS").getData() != null)
				InvoiceLogicUtils.setNetto(values);

			// autoamtycznie wyliczenie kwoty brutto wg wzoru: netto+vat
			if (values.get(InvoiceLogicUtils.DWR_KWOTA_W_WALUCIE) != null && values.get(InvoiceLogicUtils.DWR_VAT).getData() != null)
				InvoiceLogicUtils.setBruttoAsNettoPlusVat(values);

            if (values.get(InvoiceLogicUtils.DWR_KWOTA_BRUTTO) != null && values.get(InvoiceLogicUtils.DWR_KWOTA_BRUTTO).getData() != null)
                InvoiceLogicUtils.setKwotaPozostala(values);

			// data platnosci nie moze byc wczesniej niz data wystawienia
			msgBuilder.append(validateDate(values.get("DWR_DATA_WYSTAWIENIA"), values.get("DWR_TERMIN_PLATNOSCI"), DATA_PLATNOSCI_ERROR_MSG));
			
			// data wp�ywu nie mo�e by� wcze�niejsza ni� data wystawienia
			msgBuilder.append(validateDate(values.get("DWR_DATA_WYSTAWIENIA"), values.get("DWR_F_DATA_WPLYWU"), DATA_WPLYWU_ERROR_MSG));
			
			// data wp�ywu nie mo�e by� wcze�niejsza ni� data rejestracji
			msgBuilder.append(validateDate(values.get("DWR_F_DATA_WPLYWU"), new Date(), DATA_WPLYWU_TERAZ_ERROR_MSG));
			
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}

		if (msgBuilder.length()>0)
		{
			values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
			return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
		}
		return null;
	}
	
	private List<PozycjaZagregowana> agregujPozycjeZamowienia(List<PozycjaZamowienia> pozycjeZamowienia, Map<Long, Boolean> wyjatki){
		List<PozycjaZagregowana> pozycjeZagregowane = new ArrayList<PozycjaZagregowana>();
		Map<Long,Integer> pozycjeJuzZagregowane = new HashMap<Long,Integer>();

		for(PozycjaZamowienia pozycjaZamowienia: pozycjeZamowienia){
			if(pozycjeJuzZagregowane.containsKey(pozycjaZamowienia.getId()))
				continue;
			
			pozycjeJuzZagregowane.put(pozycjaZamowienia.getId(), null);
			
			PozycjaZagregowana pozycjaZagregowana = new PozycjaZagregowana();
			
			pozycjaZagregowana.setZleceniaId(pozycjaZamowienia.getZleceniaId());
			pozycjaZagregowana.setZasobId(pozycjaZamowienia.getZasobId());
			pozycjaZagregowana.setStawka(pozycjaZamowienia.getStawkaVat());
			pozycjaZagregowana.setPrzeznaczenieId(pozycjaZamowienia.getPrzeznaczenieId());
			pozycjaZagregowana.setProjektId(pozycjaZamowienia.getProjektId());
			pozycjaZagregowana.setPozycja(pozycjaZamowienia.getKonto());
			pozycjaZagregowana.setMpkId(pozycjaZagregowana.getMpkId());
			pozycjaZagregowana.setNetto(pozycjaZamowienia.getWartoscZrealizowana());
			pozycjaZagregowana.setIdZamowienia(pozycjaZamowienia.getIdZamowienia());
			pozycjaZagregowana.setEtapId(pozycjaZamowienia.getEtapId());
			pozycjaZagregowana.setBudzetProjektuId(pozycjaZamowienia.getBudzetProjektuId());
			pozycjaZagregowana.setBrutto(pozycjaZamowienia.getWartoscZrealizowana().add(pozycjaZamowienia.getWartoscVat()));
			pozycjaZagregowana.setVat(pozycjaZamowienia.getWartoscVat());
			pozycjaZagregowana.setDocumentId(pozycjaZamowienia.getDocumentId());

			if(wyjatki.containsKey(pozycjaZamowienia.getId()))
				continue;
			
			for(PozycjaZamowienia pozycjaZamowienia2: pozycjeZamowienia){
				if(wyjatki.containsKey(pozycjaZamowienia2.getId()))
					continue;
				
				if(pozycjeJuzZagregowane.containsKey(pozycjaZamowienia2.getId()))
					continue;
				
				if((pozycjaZamowienia.getKonto()!=null && pozycjaZamowienia.getStawkaVat()!=null &&
						pozycjaZamowienia.getKonto().equals(pozycjaZamowienia2.getKonto()) && pozycjaZamowienia.getStawkaVat().equals(pozycjaZamowienia2.getStawkaVat())) &&
						((pozycjaZamowienia.getMpkId()!=null && pozycjaZamowienia.getMpkId().equals(pozycjaZamowienia2.getMpkId())) || 
						(pozycjaZamowienia.getProjektId()!=null && pozycjaZamowienia.getBudzetProjektuId()!=null && pozycjaZamowienia.getEtapId()!=null && pozycjaZamowienia.getZasobId()!=null && 
						pozycjaZamowienia.getProjektId().equals(pozycjaZamowienia2.getProjektId()) && pozycjaZamowienia.getBudzetProjektuId().equals(pozycjaZamowienia2.getBudzetProjektuId()) && pozycjaZamowienia.getEtapId().equals(pozycjaZamowienia2.getEtapId())
						&& pozycjaZamowienia.getZasobId().equals(pozycjaZamowienia2.getZasobId())) || (pozycjaZamowienia.getZleceniaId()!=null && pozycjaZamowienia.getZleceniaId().equals(pozycjaZamowienia2.getZleceniaId())))){

					pozycjeJuzZagregowane.put(pozycjaZamowienia2.getId(), null);

					pozycjaZagregowana.setNetto(pozycjaZagregowana.getNetto().add(pozycjaZamowienia2.getWartoscZrealizowana()));
					pozycjaZagregowana.setBrutto(pozycjaZagregowana.getBrutto().add(pozycjaZamowienia2.getWartoscZrealizowana().add(pozycjaZamowienia2.getWartoscVat())));
					pozycjaZagregowana.setVat(pozycjaZagregowana.getVat().add(pozycjaZamowienia2.getWartoscVat()));
				}
			}
			
			pozycjeZagregowane.add(pozycjaZagregowana);
		}
		
		return pozycjeZagregowane;
    }
	
	private void dodajPozycjeZagregowane(PozycjaZagregowana pozycjaZagregowana){
    	boolean contextOpened = false;
		
		try {
			contextOpened = DSApi.openContextIfNeeded();
			
			PreparedStatement preparedStatement = DSApi.context().prepareStatement("INSERT INTO dsg_ins_costinvoice_zamowienia_pozycje_rozliczenie(ZAMDOST_ID,POZYCJA,NETTO,STAWKA,VAT,BRUTTO,MPK,KONTRAKT,BUDZET,FAZA,ZASOB,ZLECENIA,DOCUMENT_ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)");
			
			preparedStatement.setLong(1, pozycjaZagregowana.getIdZamowienia());
			preparedStatement.setObject(2, pozycjaZagregowana.getPozycja());
			preparedStatement.setBigDecimal(3, pozycjaZagregowana.getNetto());
			preparedStatement.setObject(4, pozycjaZagregowana.getStawka());
			preparedStatement.setBigDecimal(5, pozycjaZagregowana.getVat());
			preparedStatement.setBigDecimal(6, pozycjaZagregowana.getBrutto());
			preparedStatement.setObject(7, pozycjaZagregowana.getMpkId());
			preparedStatement.setObject(8, pozycjaZagregowana.getProjektId());
			preparedStatement.setObject(9, pozycjaZagregowana.getBudzetProjektuId());
			preparedStatement.setObject(10, pozycjaZagregowana.getEtapId());
			preparedStatement.setObject(11, pozycjaZagregowana.getZasobId());
			preparedStatement.setObject(12, pozycjaZagregowana.getZleceniaId());
			preparedStatement.setObject(13, pozycjaZagregowana.getDocumentId());

			preparedStatement.executeUpdate();
		} catch (Exception e){
			log.error(e.getMessage(), e);
		}
		finally{
			DSApi.closeContextIfNeeded(contextOpened);
		}
	}
	
	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException {
		StringBuilder msgBuilder = new StringBuilder();
		
//		BigDecimal totalCentrum = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
//		// sumowanie kwot poszczegolnych pozycji budzetowych
//		if (fieldValues.get("M_DICT_VALUES") != null)
//			totalCentrum = sumBudgetItems(fieldValues, "M_DICT_VALUES");
//
//		// suma kwot pozycji budzetowych musi byc rowna kwocie wniosku
//		if (fieldValues.get("KWOTA_BRUTTO") != null && fieldValues.get("MPK") != null)
//			checkTotalCentrum("KWOTA_BRUTTO", totalCentrum, fieldValues, msgBuilder);
		
		// data platnosci nie moze byc wczesniej niz data wystawienia
		msgBuilder.append(validateDate(fieldValues.get("DATA_WYSTAWIENIA"), fieldValues.get("TERMIN_PLATNOSCI"), DATA_PLATNOSCI_ERROR_MSG));
		
		// data wp�ywu nie mo�e by� wcze�niejsza ni� data wystawienia
		msgBuilder.append(validateDate(fieldValues.get("DATA_WYSTAWIENIA"), fieldValues.get("F_DATA_WPLYWU"), DATA_WPLYWU_ERROR_MSG));
		
		// data wp�ywu nie mo�e by� wcze�niejsza ni� data rejestracji
		msgBuilder.append(validateDate(fieldValues.get("F_DATA_WPLYWU"), new Date(), DATA_WPLYWU_TERAZ_ERROR_MSG));
		
		
		if (msgBuilder.length() > 0)
			throw new EdmException(msgBuilder.toString());
	}

	public TaskListParams getTaskListParams(DocumentKind dockind, long id, TaskSnapshot task) throws EdmException
	{
		TaskListParams params = super.getTaskListParams(dockind, id, task);
		SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
		FieldsManager fm = dockind.getFieldsManager(id);
		
		// Kategoria - rodzaj faktury
		//params.setCategory((String) fm.getValue("RODZAJ"));

		// Nr faktury jako nr dokumentu
		params.setDocumentNumber((String) fm.getValue("NR_FAKTURY"));
		//data dokumentu
		params.setAttribute(fmt.format(fm.getValue("DATA_WYSTAWIENIA")),3);
		// Termin platnosci
		try
		{
			params.setAttribute(DateUtils.formatCommonDate((Date)fm.getValue("TERMIN_PLATNOSCI")), 1);
		}
		catch (Exception e)
		{
			log.error("B��d przy ustawianiu kolumny Termin p�atno�ci, listy zada�");
			params.setAttribute("Nie dotyczy", 1);
		}
		
		// Spos�b p�atno�ci
		try
		{
			params.setAttribute(fm.getEnumItem("SPOSOB") != null ? fm.getEnumItem("SPOSOB").getTitle() : "Nie okre�lony", 4);
		}
		catch (Exception e)
		{
			log.error("B��d przy ustawianiu kolumny Spos�b p�atno�ci, listy zada�");
			params.setAttribute("Nie dotyczy", 4);
		}
		return params;
	}

	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		super.setAdditionalTemplateValues(docId, values);
		try
		{
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();

			// document_id
			values.put("DOCUMENT_ID", doc.getId());

			// nr KO
			values.put("DOCUMENT_KO", doc.getOfficeNumber());
						
			// wystawca faktury
			values.put("SENDER", doc.getSender());

			// odbiorca faktury
			values.put("RECIPIENT", doc.getRecipients().get(0));

			// osoba generujaca
			values.put("USER", DSApi.context().getDSUser());

			// data wystawienia
			values.put("DATA_WYSTAWIENIA", DateUtils.formatCommonDate((Date) fm.getValue("DATA_WYSTAWIENIA")));

			// termin platnosci
			values.put("TERMIN_PLATNOSCI", DateUtils.formatCommonDate((Date) fm.getValue("TERMIN_PLATNOSCI")));

		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	protected void checkTotalCentrum(String fieldName, BigDecimal totalCentrum, Map<String, ?> fieldValues, StringBuilder msgBuilder)
	{
		if (!fieldValues.get(fieldName).equals(totalCentrum))
		{
			if (msgBuilder.length() == 0)
				msgBuilder.append("B��d: Suma kwot pozycji bud�etowych musi by� rowna kwocie brutto faktury.");
			else
				msgBuilder.append(";  Suma kwot pozycji bud�etowych musi by� rowna kwocie brutto faktury.");
		}
	}
	
	private void fillStawkiVat_Obciazenie(String dictionaryName, Map<String, FieldData> values, Map<String, Object> results)
	{
		if (dictionaryName.equals("MPK"))
		{
			Object netto = values.get("MPK_NETTO").getData();
			if (netto == null)
				return;
			
			Object stawkaVat = values.get("MPK_STAWKA").getData();
			if (stawkaVat == null)
				return;
			
			try
			{
				EnumItem stawkaVatItem = DataBaseEnumField.getEnumItemForTable("simple_erp_per_docusafe_vatstaw_view",
                        Integer.parseInt(stawkaVat.toString()));
				BigDecimal stawkVatB = new BigDecimal (stawkaVatItem.getCn());
				BigDecimal nettoB = new BigDecimal (netto.toString());
				BigDecimal kwotaVatB;
				BigDecimal bruttoB = nettoB;
				if (stawkVatB.compareTo(BigDecimal.ZERO) == 1)
				{
					kwotaVatB = nettoB.multiply(stawkVatB.divide(new BigDecimal(100).setScale(2),
                            RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_UP)).setScale(2, RoundingMode.HALF_UP);
					bruttoB = nettoB.add(kwotaVatB);
				}
				else
					kwotaVatB = BigDecimal.ZERO;
				
				results.put("MPK_VAT", kwotaVatB);
				results.put("MPK_BRUTTO",  bruttoB);
            }
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
			}

		}
	}
	
	
	@Override
	public void validate(Map<String, Object> values, DocumentKind kind,
			Long documentId) throws EdmException {
		if (documentId != null) {
			OfficeDocument doc = OfficeDocument.find(documentId);
			FieldsManager fm = doc.getFieldsManager();
			if (fm.getEnumItem("STATUS").getId().equals(26) && values.get("AKCEPTANT_BRANZYSTY") != null) {
				String branzysta = String.valueOf(values.get("AKCEPTANT_BRANZYSTY"));
				values.put("PIERWSZY_BRANZYSTA", Long.valueOf(branzysta));
			}
		}
	}

    private class PozycjaZamowienia{
    	private Long id;
    	private Long idPozycji;
    	private Long idZamowienia;
    	private Long documentId;
    	private String nazwa;
    	private Integer iloscZamowiona;
    	private Integer iloscZrealizowana;
    	private BigDecimal wartosc;
    	private Integer stawkaVat;
    	private BigDecimal wartoscVat;
    	private Long konto;
    	private String produkt;
    	private Long mpkId;
    	private Long projektId;
    	private Long budzetProjektuId;
    	private Long etapId;
    	private Long zasobId;
    	private Long zleceniaId;
    	private Long przeznaczenieId;
    	private BigDecimal wartoscZrealizowana;
    	
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public Long getIdZamowienia() {
			return idZamowienia;
		}
		public void setIdZamowienia(Long idZamowienia) {
			this.idZamowienia = idZamowienia;
		}
		public String getNazwa() {
			return nazwa;
		}
		public void setNazwa(String nazwa) {
			this.nazwa = nazwa;
		}
		public Integer getIloscZamowiona() {
			return iloscZamowiona;
		}
		public void setIloscZamowiona(Integer iloscZamowiona) {
			this.iloscZamowiona = iloscZamowiona;
		}
		public Integer getIloscZrealizowana() {
			return iloscZrealizowana;
		}
		public void setIloscZrealizowana(Integer iloscZrealizowana) {
			this.iloscZrealizowana = iloscZrealizowana;
		}
		public BigDecimal getWartosc() {
			return wartosc;
		}
		public void setWartosc(BigDecimal wartosc) {
			this.wartosc = wartosc;
		}
		public Integer getStawkaVat() {
			return stawkaVat;
		}
		public void setStawkaVat(Integer stawkaVat) {
			this.stawkaVat = stawkaVat;
		}
		public BigDecimal getWartoscVat() {
			return wartoscVat;
		}
		public void setWartoscVat(BigDecimal wartoscVat) {
			this.wartoscVat = wartoscVat;
		}
		public Long getKonto() {
			return konto;
		}
		public void setKonto(Long konto) {
			this.konto = konto;
		}
		public String getProdukt() {
			return produkt;
		}
		public void setProdukt(String produkt) {
			this.produkt = produkt;
		}
		public Long getMpkId() {
			return mpkId;
		}
		public void setMpkId(Long mpkId) {
			this.mpkId = mpkId;
		}
		public Long getProjektId() {
			return projektId;
		}
		public void setProjektId(Long projektId) {
			this.projektId = projektId;
		}
		public Long getBudzetProjektuId() {
			return budzetProjektuId;
		}
		public void setBudzetProjektuId(Long budzetProjektuId) {
			this.budzetProjektuId = budzetProjektuId;
		}
		public Long getEtapId() {
			return etapId;
		}
		public void setEtapId(Long etapId) {
			this.etapId = etapId;
		}
		public Long getZasobId() {
			return zasobId;
		}
		public void setZasobId(Long zasobId) {
			this.zasobId = zasobId;
		}
		public Long getZleceniaId() {
			return zleceniaId;
		}
		public void setZleceniaId(Long zleceniaId) {
			this.zleceniaId = zleceniaId;
		}
		public Long getPrzeznaczenieId() {
			return przeznaczenieId;
		}
		public void setPrzeznaczenieId(Long przeznaczenieId) {
			this.przeznaczenieId = przeznaczenieId;
		}
		public BigDecimal getWartoscZrealizowana() {
			return wartoscZrealizowana;
		}
		public void setWartoscZrealizowana(BigDecimal wartoscZrealizowana) {
			this.wartoscZrealizowana = wartoscZrealizowana;
		}
		public Long getDocumentId() {
			return documentId;
		}
		public void setDocumentId(Long documentId) {
			this.documentId = documentId;
		}
		public Long getIdPozycji() {
			return idPozycji;
		}
		public void setIdPozycji(Long idPozycji) {
			this.idPozycji = idPozycji;
		}
    }
    
    private class PozycjaZagregowana{
    	private Long id;
    	private Long idZamowienia;
    	private Long documentId;
    	private Long pozycja;
    	private BigDecimal netto;
    	private Integer stawka;
    	private BigDecimal brutto;
    	private BigDecimal vat;
    	private Long mpkId;
    	private Long projektId;
    	private Long budzetProjektuId;
    	private Long etapId;
    	private Long zasobId;
    	private Long zleceniaId;
    	private Long przeznaczenieId;
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public Long getIdZamowienia() {
			return idZamowienia;
		}
		public void setIdZamowienia(Long idZamowienia) {
			this.idZamowienia = idZamowienia;
		}
		public Long getPozycja() {
			return pozycja;
		}
		public void setPozycja(Long pozycja) {
			this.pozycja = pozycja;
		}
		public BigDecimal getNetto() {
			return netto;
		}
		public void setNetto(BigDecimal netto) {
			this.netto = netto;
		}
		public Integer getStawka() {
			return stawka;
		}
		public void setStawka(Integer stawka) {
			this.stawka = stawka;
		}
		public BigDecimal getBrutto() {
			return brutto;
		}
		public void setBrutto(BigDecimal brutto) {
			this.brutto = brutto;
		}
		public Long getMpkId() {
			return mpkId;
		}
		public void setMpkId(Long mpkId) {
			this.mpkId = mpkId;
		}
		public Long getProjektId() {
			return projektId;
		}
		public void setProjektId(Long projektId) {
			this.projektId = projektId;
		}
		public Long getBudzetProjektuId() {
			return budzetProjektuId;
		}
		public void setBudzetProjektuId(Long budzetProjektuId) {
			this.budzetProjektuId = budzetProjektuId;
		}
		public Long getEtapId() {
			return etapId;
		}
		public void setEtapId(Long etapId) {
			this.etapId = etapId;
		}
		public Long getZasobId() {
			return zasobId;
		}
		public void setZasobId(Long zasobId) {
			this.zasobId = zasobId;
		}
		public Long getZleceniaId() {
			return zleceniaId;
		}
		public void setZleceniaId(Long zleceniaId) {
			this.zleceniaId = zleceniaId;
		}
		public Long getPrzeznaczenieId() {
			return przeznaczenieId;
		}
		public void setPrzeznaczenieId(Long przeznaczenieId) {
			this.przeznaczenieId = przeznaczenieId;
		}
		public BigDecimal getVat() {
			return vat;
		}
		public void setVat(BigDecimal vat) {
			this.vat = vat;
		}
		public Long getDocumentId() {
			return documentId;
		}
		public void setDocumentId(Long documentId) {
			this.documentId = documentId;
		}
    }
}
    
