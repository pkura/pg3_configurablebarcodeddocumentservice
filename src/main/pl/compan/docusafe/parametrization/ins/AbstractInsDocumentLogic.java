package pl.compan.docusafe.parametrization.ins;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.ins.MpkAccept.AcceptType;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;


public abstract class AbstractInsDocumentLogic extends AbstractDocumentLogic{
    // cn p�l dockind�w
    public static final String MPK_DOCKIND_CN = "MPK";
    public static final String NR_WNIOSEK_DOCKIND_CN = "NR_WNIOSEK";
    public static final String NR_FAKTURY_DOCKIND_CN = "NR_FAKTURY";
    public static final String STATUS_DOCKIND_CN = "STATUS";
    public static final String SPOSOB_DOCKIND_CN = "SPOSOB";
    public static final String WORKER_DIVISION_DOCKIND_CN = "WORKER_DIVISION";
    public static final String WNIOSKODAWCA_DOCKIND_CN = "WNIOSKODAWCA";
    public static final String DATA_WYSTAWIENIA_DOCKIND_CN = "DATA_WYSTAWIENIA";
    public static final String DATA_WPLYWU_DOCKIND_CN = "DATA_WPLYWU";
    public static final String TERMIN_PLATNOSCI_DOCKIND_CN = "TERMIN_PLATNOSCI";


    // cn pol z przedrostkiem dwr
    public static final String DWR_REALIZACJA_BUDZETU_CN = "DWR_REALIZACJA_BUDZETU";
    public static final String DWR_MPK_CN = "DWR_MPK";

    // task names
    public static final String FINANCIAL_SECTION_TASK_NAME = "financial_section";
    public static final String MANAGER_OF_DEPARTMENT_TASK_NAME = "manager_of_department";
    public static final String MANAGER_OF_UNIT_TASK_NAME = "manager_of_unit";


    // zmienne do szablonow
    public static final String DOC_DATE_TEMPLATE_VAR = "DOC_DATE";
    public static final String GENERATE_DATE_TEMPLATE_VAR = "GENERATE_DATE";

    public static final String MPK_VARIABLE_NAME = "costCenterId";

    //tabele mpk: budzet, pozycja, zrodlo
    public static final String CENTRUM_ID_TABLE_NAME = "simple_erp_per_docusafe_bd_str_budzet_view";
    public static final String SIMPLE_ERP_PER_DOCUSAFE_BD_POZYCJE_VIEW = "simple_erp_per_docusafe_bd_pozycje_view";
    public static final String SIMPLE_ERP_PER_DOCUSAFE_BD_ZRODLA_VIEW = "simple_erp_per_docusafe_bd_zrodla_view";

    protected Logger log = LoggerFactory.getLogger(AbstractInsDocumentLogic.class);
    final StringManager sm = GlobalPreferences.loadPropertiesFile(AbstractInsDocumentLogic.class.getPackage().getName(), null);

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
        String docKindTitle = document.getDocumentKind().getName();
        FieldsManager fm = document.getFieldsManager();

        Folder folder = Folder.getRootFolder();

        folder = folder.createSubfolderIfNotPresent(docKindTitle);

        folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(document.getCtime()));
        folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(document.getCtime()));

        String docNumber;
        if (fm.getValue(NR_WNIOSEK_DOCKIND_CN) != null)
        {
            docNumber = String.valueOf(fm.getValue(NR_WNIOSEK_DOCKIND_CN));
        }
        else if (fm.getValue(NR_FAKTURY_DOCKIND_CN) != null)
        {
            docNumber = String.valueOf(fm.getValue(NR_FAKTURY_DOCKIND_CN));
        }
        else
        {
            docNumber = "";
        }

        document.setTitle(docKindTitle + " " + docNumber);
        document.setDescription(docKindTitle + " " + docNumber);
        ((OfficeDocument) document).setSummaryWithoutHistory((docKindTitle + " " + docNumber));
        document.setFolder(folder);
    }

    @Override
    public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
    {
        try
        {
            Document doc = Document.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            fm.initialize();
            // --------------------------
            // DO USUNIECIA
            // w rtfie dajemy $fields.WNIOSKODAWCA.title
            // wnioskodawca
            if (fm.containsField(WNIOSKODAWCA_DOCKIND_CN))
            {
                values.put(WNIOSKODAWCA_DOCKIND_CN, fm.getEnumItem(WNIOSKODAWCA_DOCKIND_CN).getTitle());
            }
            // jednostka wnioskuj�cego
            if (fm.containsField(WORKER_DIVISION_DOCKIND_CN))
            {
                values.put(WORKER_DIVISION_DOCKIND_CN, fm.getEnumItem(WORKER_DIVISION_DOCKIND_CN).getTitle());
            }
            // ------------------------------

            // czas wygenerowania metryki
            values.put(GENERATE_DATE_TEMPLATE_VAR, DateUtils.formatCommonDate(new java.util.Date()).toString());

            // data dokumentu
            values.put(DOC_DATE_TEMPLATE_VAR, DateUtils.formatCommonDate(doc.getCtime()));

            // generowanie akceptacji
//            setAcceptances(docId, values);

        }
        catch (EdmException e)
        {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void documentPermissions(Document document) throws EdmException
    {
        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
        String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
        String documentKindName = document.getDocumentKind().getName();

        perms.add(new PermissionBean(ObjectPermission.READ, "INS_" + documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, "INS - " + documentKindName + " - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "INS_" + documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "INS - " + documentKindName + " zalacznik - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "INS_" + documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "INS - " + documentKindName + " - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "INS_" + documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "INS - " + documentKindName + " zalacznik - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, "INS_" + documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "INS - " + documentKindName + " - usuwanie"));
        Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
        perms.addAll(documentPermissions);
        this.setUpPermission(document, perms);
    }

    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue)
    {
        boolean assigned = false;

        return assigned;
    }

    // column.dockindBusinessAtr3 = Pozcyja bud\u017cetowa: bud\u017cet
    // column.dockindBusinessAtr4 = Pozcyja bud\u017cetowa: kwota
    @Override
    public TaskListParams getTaskListParams(DocumentKind kind, long documentId, TaskSnapshot task) throws EdmException
    {
        TaskListParams params = new TaskListParams();

        List<String> mpkAcceptationNames = new ArrayList<String>();
        mpkAcceptationNames.add(AssingUtils.MANAGER_OF_DEPARTMENT);
        mpkAcceptationNames.add(AssingUtils.MANAGER_OF_UNIT);
        mpkAcceptationNames.add(AssingUtils.FINANCIAL_SECTION);
        String taskId = task.getActivityKey();

        if (!mpkAcceptationNames.contains(Jbpm4Provider.getInstance().getTaskService().getTask(taskId).getName()))
        {
            params.setAttribute("Nie dotyczy", 2);
            params.setAttribute("Nie dotyczy", 3);

            FieldsManager fm = kind.getFieldsManager(documentId);

            if (fm != null && fm.getKey(MPK_DOCKIND_CN) != null)
            {
                Object mpkRaw = fm.getKey(MPK_DOCKIND_CN);
                if (mpkRaw != null && mpkRaw instanceof List<?> && ((List<?>) mpkRaw).size() > 0)
                {
                    try
                    {
                        String mpkRawId = ((List<?>) mpkRaw).get(0).toString();
                        Long firstMpkId = Long.valueOf(mpkRawId);

                        CentrumKosztowDlaFaktury mpkInstance = CentrumKosztowDlaFaktury.getInstance();
                        CentrumKosztowDlaFaktury mpk = mpkInstance.find(firstMpkId);
                        if (mpk != null && mpk.getCentrumId() != null)
                        {
                            EnumItem field = DataBaseEnumField.getEnumItemForTable(CENTRUM_ID_TABLE_NAME, mpk.getCentrumId());

                            String postfix = ((List<?>) mpkRaw).size() > 1 ? ", ..." : "";

                            params.setAttribute(field.getTitle() + postfix, 2);
                            params.setAttribute(mpk.getRealAmount().toString() + postfix, 3);
                        }
                    }
                    catch (NumberFormatException e)
                    {
                        log.error(e.toString());
                    }
                    catch (EdmException e1)
                    {
                        log.error(e1.toString());
                    }
                }
            }
        }
        else
        {
            String mpkRawId = Jbpm4Provider.getInstance().getTaskService().getVariable(taskId, MPK_VARIABLE_NAME) != null ? Jbpm4Provider.getInstance().getTaskService().getVariable(taskId, MPK_VARIABLE_NAME).toString() : "";
            if (!mpkRawId.equals(""))
            {
                try
                {
                    Long mpkId = Long.valueOf(mpkRawId);
                    CentrumKosztowDlaFaktury mpkInstance = CentrumKosztowDlaFaktury.getInstance();
                    CentrumKosztowDlaFaktury mpk = mpkInstance.find(mpkId);

                    EnumItem field = DataBaseEnumField.getEnumItemForTable(CENTRUM_ID_TABLE_NAME, mpk.getCentrumId());

                    params.setAttribute(field.getTitle(), 2);
                    params.setAttribute(mpk.getRealAmount().toString(), 3);

                }
                catch (EdmException e)
                {
                    log.error(e.toString());
                }
            }
        }
        FieldsManager fm = kind.getFieldsManager(documentId);

        // Status
        params.setStatus((String) fm.getValue(STATUS_DOCKIND_CN));

        // Spos�b p�atno�ci
        try
        {
            params.setAttribute(fm.getEnumItem(SPOSOB_DOCKIND_CN) != null ? fm.getEnumItem(SPOSOB_DOCKIND_CN).getTitle() : "Nie okre�lony", 4);
        }
        catch (Exception e)
        {
            log.error("B��d przy ustawianiu kolumny Spos�b p�atno�ci, listy zada�");
            params.setAttribute("Nie dotyczy", 1);
        }
        return params;
    }

    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues)
    {
        Map<String, Object> results = new HashMap<String, Object>();

        return results;
    }




    public Set<String> loadBudgetRealizationItems(Map<String, FieldData> values, String cn, Set<String> pozycjeId)
    {
        Map<String, FieldData> mpkValues = DwrUtils.getDictionaryValue(values, "DWR_" + cn);

        if (mpkValues != null && mpkValues.size() > 0)
        {

            for (String key : mpkValues.keySet())
            {
                if (key.contains(cn + "_POZYCJAID_"))
                {

                    EnumValues pozycjeDwr = mpkValues.get(key).getEnumValuesData();
                    if (pozycjeDwr.getSelectedId() != null)
                    {
                        pozycjeId.add(pozycjeDwr.getSelectedId());
                    }
                }
            }
        }

        // dobry kod, lecz nie dzia�a
        // zostawiam, bo by� mo�e trzbe b�dzie do tego wr�ci�
        // KK
        //
        // StringBuilder ids = new StringBuilder();
        //
        // if (mpkValues != null && mpkValues.size()>0) {
        // Set<String> pozycjeId = new HashSet<String>();
        //
        // Map<String,BigDecimal> pozycjeKwotaMap = new
        // HashMap<String,BigDecimal>();
        //
        // for (String key : mpkValues.keySet()) {
        // if (key.contains("MPK_ACCOUNTNUMBER_")) {
        // String index = key.replace("MPK_ACCOUNTNUMBER_", "");
        // BigDecimal kwota =
        // mpkValues.get("MPK_AMOUNT_"+index)!=null?mpkValues.get("MPK_AMOUNT_"+index).getMoneyData():null;
        //
        // EnumValues pozycjeDwr = mpkValues.get(key).getEnumValuesData();
        // if (pozycjeDwr.getSelectedId()!=null) {
        // BigDecimal sumaKwot =
        // pozycjeKwotaMap.get(pozycjeDwr.getSelectedId()) !=
        // null?pozycjeKwotaMap.get(pozycjeDwr.getSelectedId()).add(kwota):kwota;
        // pozycjeKwotaMap.put(pozycjeDwr.getSelectedId(), sumaKwot);
        // }
        // }
        // }
        //
        // for (String key : realizacjeValues.keySet()) {
        // if (key.contains("REALIZACJA_BUDZETU_ID_") &&
        // realizacjeValues.get(key) != null &&
        // realizacjeValues.get(key).getStringData() != null) {
        // String index = key.replace("REALIZACJA_BUDZETU_ID_", "");
        //
        // BigDecimal kwota =
        // realizacjeValues.get("REALIZACJA_BUDZETU_WSTEPNA_"+index)!=null?realizacjeValues.get("REALIZACJA_BUDZETU_WSTEPNA_"+index).getMoneyData():null;
        //
        // if (kwota != null) {
        // if (pozycjeKwotaMap.get(realizacjeValues.get(key).getStringData())
        // != null) {
        // realizacjeValues.get("REALIZACJA_BUDZETU_WSTEPNA_"+index).setMoneyData(
        // kwota.add(pozycjeKwotaMap.get(realizacjeValues.get(key).getStringData()))
        // );
        // }
        // }
        // }
        // }
        //
        // for (String pozycja : pozycjeId) {
        // if (ids.length()!=0) {
        // ids.append(",");
        // }
        // ids.append(pozycja);
        // }
        // }

        return pozycjeId;
    }

    protected StringBuilder validateDate(Object start, Object finish, String errorMsg)
    {
        StringBuilder msg = new StringBuilder("");
        try
        {
            if (start != null && finish != null)
            {
                Date startDate = null;
                Date finishDate = null;

                if (start instanceof Date)
                {
                    startDate = (Date) start;
                }
                else if (start instanceof FieldData && ((FieldData) start).getData() instanceof Date)
                {
                    startDate = ((FieldData) start).getDateData();
                }

                if (finish instanceof Date)
                {
                    finishDate = (Date) finish;
                }
                else if (finish instanceof FieldData && ((FieldData) finish).getData() instanceof Date)
                {
                    finishDate = ((FieldData) finish).getDateData();
                }

                if (startDate != null && finishDate != null && startDate.after(finishDate))
                {
                    msg.append(errorMsg + "; ");
                }

            }
        }
        catch (Exception e)
        {
            log.warn(e.getMessage(), e);
        }
        return msg;
    }
}
