package pl.compan.docusafe.parametrization.ins;

import java.math.BigDecimal;
import java.sql.*;
import java.util.Collections;

import org.apache.commons.dbcp.BasicDataSource;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ExportInvoiceToSimpleERP
{
	private static final Logger log = LoggerFactory.getLogger(ExportInvoiceToSimpleERP.class);

	static public void export(String xml, OfficeDocument document) throws EdmException
	{

		log.info("EXPORT invoice xml: {}", xml);
		Connection con = null;
		CallableStatement call = null;
		try
		{
			String erpUser, erpPassword, erpUrl, erpDataBase, erpSchema, proc_export;
			erpUser = Docusafe.getAdditionProperty("erp.database.user");
			erpPassword = Docusafe.getAdditionProperty("erp.database.password");
			erpUrl = Docusafe.getAdditionProperty("erp.database.url");
			erpDataBase = Docusafe.getAdditionProperty("erp.database.dataBase");
			erpSchema = Docusafe.getAdditionProperty("erp.database.schema");
			// up_gsd_ksiegapodawcza
			proc_export = Docusafe.getAdditionProperty("erp.database.export.faktura");
			if (proc_export == null)
				proc_export = "docusafe_imp_dokzak2";
			
			if (erpUser == null || erpPassword == null || erpUrl == null)
				throw new EdmException("B��d eksportu faktury, brak parametr�w po��czenia do bazy ");

			log.info("user: {}, password: {}, url: {}", erpUser, erpPassword, erpUrl);
			BasicDataSource ds = new BasicDataSource();
			ds.setDriverClassName(Docusafe.getDefaultDriver("sqlserver_2000"));
			ds.setUrl(erpUrl);
			ds.setUsername(erpUser);
			ds.setPassword(erpPassword);
			con = ds.getConnection();

			call = con.prepareCall("{call [" + erpDataBase + "].[" + erpSchema + "].[" + proc_export + "] (?,?,?,?,?)}");

			Integer return_value = null;
            String dokzak_idm = null;
			BigDecimal dokzak_id = null, error_id = null;
			String error_description = null;

            String xmlOut = null;
            Integer statusCode = null;
            String statusMessage = null;

            //xmlIn
            call.setObject(1, xml);
            //isResultset
            call.setInt(2, 0);
            //xmlOut
            call.registerOutParameter(3, java.sql.Types.VARCHAR);
            //statusCode
            call.registerOutParameter(4, java.sql.Types.INTEGER);
            //statusMessage
            call.registerOutParameter(5, java.sql.Types.VARCHAR);


			int resultNum = 0;

			while (true)
			{
				boolean queryResult;
				int rowsAffected;

				if (1 == ++resultNum)
				{
					try
					{
                        queryResult = call.execute();
                        xmlOut = call.getString(3);
                        String id = null;
                        String idm = null;
                        if (xmlOut.contains("<ID>"))
                            id = xmlOut.substring(xmlOut.indexOf("<ID>")+4,xmlOut.indexOf("</ID>"));
                        if (xmlOut.contains("<IDM>"))
                            idm = xmlOut.substring(xmlOut.indexOf("<IDM>")+5,xmlOut.indexOf("</IDM>"));
                        statusCode = call.getInt(4);
                        if(statusCode==0 && id != null && !id.equals(""))
                            dokzak_id = new BigDecimal(id);

                        if(statusCode==0 && idm != null && !idm.equals(""))
                            dokzak_idm = idm;

                        statusMessage = call.getString(5);

						log.debug("EXPORT invoice xml: return_value: {}, dokzak_id: {}, dokzak_idm: {}, error_id: {}, error_description: {}", return_value, dokzak_id, dokzak_idm, error_id, error_description);
					}
					catch (Exception e)
					{
						// Process the error
						log.error("Result " + resultNum + " is an error: " + e.getMessage());
                        System.out.println(e);
                        throw new EdmException(e);
					}
				}
				else
				{
					try
					{
						queryResult = call.getMoreResults();
					}
					catch (Exception e)
					{
						// Process the error
						log.error("1 != ++resultNum: Result " + resultNum + " is an error: " + e.getMessage());

						// When getMoreResults() throws an exception, it
						// may just be that the current statement produced
						// an error.
						// Statements after that one may have succeeded.
						// Continue processing results until there
						// are no more.
						continue;
					}
				}

				if (queryResult)
				{
					ResultSet rs = call.getResultSet();

					// Process the ResultSet
					log.debug("Result " + resultNum + " is a ResultSet: " + rs);
					ResultSetMetaData md = rs.getMetaData();

					int count = md.getColumnCount();
					log.debug("<table border=1>");
					log.debug("<tr>");
					for (int i = 1; i <= count; i++)
					{
						log.debug("<th>");
						log.debug("GEtColumnLabeL: {}", md.getColumnLabel(i));
					}
					log.debug("</tr>");
					while (rs.next())
					{
						log.debug("<tr>");
						for (int i = 1; i <= count; i++)
						{
							log.debug("<td>");
							log.debug("Get.Object: {}", rs.getObject(i));
						}
						log.debug("</tr>");
					}
					log.debug("</table>");
					rs.close();
				}
				else
				{
					rowsAffected = call.getUpdateCount();

					// No more results
					if (-1 == rowsAffected)
					{
						--resultNum;
						break;
					}

					// Process the update count
					log.debug("Result " + resultNum + " is an update count: " + rowsAffected);
				}
			}

			log.debug("Done processing " + resultNum + " results");
			if (resultNum == 0)
			{

			}
			if (dokzak_idm == null || dokzak_idm.equals(""))
				throw new EdmException("Procedura importujaca fatkure (" + proc_export + ") do Simple.ERP nie zwrocila identyfikatora dokzak_id !!! " +
						"Zwr�cne wartosci z procedury: statusMessage: " + statusMessage);
            else{
                document.getDocumentKind().setOnly(
								document.getId(),
								Collections.singletonMap("NR_WNIOSKU", dokzak_idm));
                Remark rem = new Remark("Export faktury kosztowej do SIMPLE.ERP pomy�lnie uko�czony. Identyfikator faktury w SIMPLE.ERP to "+ dokzak_idm);
                document.addRemark(rem);
            }
			
//			setUrl(con, erpDataBase, erpSchema, dokzak_id.intValue(), document);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException("B��d akcpetacji wniosku :" + e.getMessage());
		}
		finally
		{
			if (con != null)
			{
				try
				{
					con.close();
				}
				catch (Exception e)
				{
					log.error(e.getMessage(), e);
					throw new EdmException("B��d akcpetacji wniosku :" + e.getMessage());
				}
			}
		}
	}

	private static void setUrl(Connection con, String erpDataBase, String erpSchema, Integer dokzak_id, OfficeDocument doc) throws DocumentNotFoundException, DocumentLockedException, AccessDeniedException, EdmException
	{
			
		try
		{
			String url = Docusafe.getBaseUrl() + createLink(doc);
			String nazwa = "Faktura w Docusafe";
			//@bd_rezeracja_id numeric (10,0) ,
			//@nazwa varchar (256) ,
			//@url varchar (256)
			CallableStatement call = con.prepareCall("{call [" + erpDataBase + "].[" + erpSchema + "].[up_gsd_dodaj_zal_do_dokzak] (?,?,?)}");
			call.setInt(1, dokzak_id);
			call.setString(2, nazwa);
			call.setString(3, url);
			int resultNum = 0;
	
			while (true)
			{
				boolean queryResult;
				int rowsAffected;
	
				if (1 == ++resultNum)
				{
					try
					{
						queryResult = call.execute();
					
						log.debug("EXPORT Accepting dokzak_id (dokzak_id) : {}", dokzak_id);
						
					}
					catch (Exception e)
					{
						// Process the error
						log.error("Result " + resultNum + " is an error: " + e.getMessage());
	
						// When execute() throws an exception, it may just
						// be that the first statement produced an error.
						// Statements after the first one may have
						// succeeded. Continue processing results until
						// there
						// are no more.
						continue;
					}
				}
				else
				{
					try
					{
						queryResult = call.getMoreResults();
					}
					catch (Exception e)
					{
						// Process the error
						log.error("1 != ++resultNum: Result " + resultNum + " is an error: " + e.getMessage());
	
						// When getMoreResults() throws an exception, it
						// may just be that the current statement produced
						// an error.
						// Statements after that one may have succeeded.
						// Continue processing results until there
						// are no more.
						continue;
					}
				}
	
				if (queryResult)
				{
					ResultSet rs = call.getResultSet();
	
					// Process the ResultSet
					log.debug("Result " + resultNum + " is a ResultSet: " + rs);
					ResultSetMetaData md = rs.getMetaData();
	
					int count = md.getColumnCount();
					log.debug("<table border=1>");
					log.debug("<tr>");
					for (int i = 1; i <= count; i++)
					{
						log.debug("<th>");
						log.debug("GEtColumnLabeL: {}", md.getColumnLabel(i));
					}
					log.debug("</tr>");
					while (rs.next())
					{
						log.debug("<tr>");
						for (int i = 1; i <= count; i++)
						{
							log.debug("<td>");
							log.debug("Get.Object: {}", rs.getObject(i));
						}
						log.debug("</tr>");
					}
					log.debug("</table>");
					rs.close();
				}
				else
				{
					rowsAffected = call.getUpdateCount();
	
					// No more results
					if (-1 == rowsAffected)
					{
						--resultNum;
						break;
					}
	
					// Process the update count
					log.debug("Result " + resultNum + " is an update count: " + rowsAffected);
				}
			}
	
			log.debug("Done processing " + resultNum + " results");
			if (resultNum == 0)
			{
	
			}
			
	
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException("B��d wywloania procedury wniosku :" + e.getMessage());
		}		
	}

	private static String createLink(Document doc) throws DocumentNotFoundException, DocumentLockedException, AccessDeniedException, EdmException
	{
	
	    Long docId = doc.getId();
	    StringBuilder b = new StringBuilder();
	    log.info("PageContext: {}", Docusafe.getPageContext());
	    String pageContext = Docusafe.getPageContext();
	    if (doc instanceof InOfficeDocument)
	    {
	        if(pageContext.length() > 0)
	            b.append("/");
	        b.append(Docusafe.getPageContext() + "/office/incoming/document-archive.action?documentId=").append(docId);
	    }
	    else if (doc instanceof OutOfficeDocument)
	    {
	        if (((OutOfficeDocument) doc).isInternal())
	        {
	            if(pageContext.length() > 0)
	                b.append("/");
	            b.append(Docusafe.getPageContext() + "/office/internal/document-archive.action?documentId=").append(docId);
	        }
	        else
	        {
	            if(pageContext.length() > 0)
	                b.append("/");
	            b.append(Docusafe.getPageContext() + "/office/outgoing/document-archive.action?documentId=").append(docId);
	        }
	    }
	    else
	    {
	        if(pageContext.length() > 0)
	            b.append("/");
	        b.append(Docusafe.getPageContext() + "/repository/edit-dockind-document.action?id=").append(docId);
	    }
	    log.info("Link: {}", b.toString());
	    return b.toString();
	}

}
