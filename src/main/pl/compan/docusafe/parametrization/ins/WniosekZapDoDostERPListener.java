package pl.compan.docusafe.parametrization.ins;

import org.jbpm.api.Execution;
import org.jbpm.api.ExecutionService;
import org.jbpm.api.ProcessInstance;
import org.jbpm.api.activity.ActivityExecution;






import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.parametrization.wssk.CustomBase;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class WniosekZapDoDostERPListener extends CustomBase {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void executionInsideContext(ActivityExecution execution)
			throws Exception {
	
		super.executionInsideContext(execution);
		  Long documentId = Long.valueOf( execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		
		ExecutionService executionService = Jbpm4Provider.getInstance().getExecutionService();
		
		if (execution==null)
			return; // Nie znaleziono, wiec widocznie takiego juz nie ma. Przeciez metoda mogla byc wywolana kilka razy pod rzad przy przeladowaniu strony. 
//			err("Nie znaleziono 'execution' dla procesu w stanie '" +activity+ "' o nazwie '" +processName +"' dla dokumentu o ID=" +documentId, log);
		
/*		//wybudzenie:
		if (sygnal==null)
			executionService.signalExecutionById(executionWaiting.getId());
		else*/
		/*DSApi.context().commit();
		executionService.signalExecutionById(execution.getId(),"czekanieNaERP");
		TaskSnapshot.updateByDocument( OfficeDocument.find(documentId) );
		DSApi.context().commit();
		if (activityExecution.getActivity().getDefaultOutgoingTransition() != null)
			activityExecution.takeDefaultTransition();
		*/
		
	}
	
	
}
