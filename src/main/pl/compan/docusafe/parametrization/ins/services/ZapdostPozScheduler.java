package pl.compan.docusafe.parametrization.ins.services;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.parametrization.ins.jbpm.UtilTrigger;
import pl.compan.docusafe.service.*;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Timer;
import java.util.TimerTask;

public class ZapdostPozScheduler extends ServiceDriver implements Service {
    public static final Logger log = LoggerFactory.getLogger(ZapdostPozScheduler.class);
    private final static StringManager sm = GlobalPreferences.loadPropertiesFile(ZapdostPozScheduler.class.getPackage().getName(), null);
    private Property[] properties;
    private Timer timer;
    private Integer period = 10;
    private boolean syncing = false;

    class ImportTimesProperty extends Property
    {
        public ImportTimesProperty()
        {
            super(SIMPLE, PERSISTENT, ZapdostPozScheduler.this, "period", "Co ile minut", Integer.class);
        }

        protected Object getValueSpi()
        {
            return period;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            synchronized (ZapdostPozScheduler.this)
            {
                if (object != null){
                    period = (Integer) object;
                    stop();
                }
                else
                    period = 10;
            }
        }
    }


    public ZapdostPozScheduler()
    {
        properties = new Property[] { new ImportTimesProperty()};
    }

    @Override
    protected void start() throws ServiceException {
        log.info("Start uslugi ZapdostPozScheduler");
        timer = new Timer(true);
        timer.schedule(new PositionMapper(), 0, period * DateUtils.MINUTE);
        console(Console.INFO, sm.getString("System ZapdostPozScheduler wystartowal. Cz�stotliwo��: "+period+" m."));
    }

    @Override
    protected void stop() throws ServiceException {
        if(timer != null){
            log.info("Stop uslugi ZapdostPozScheduler");
            console(Console.INFO, sm.getString("System ZapdostPozScheduler zako�czy� dzia�anie"));
            timer.cancel();
            timer = new Timer();
            timer.schedule(new PositionMapper(), 0, period * DateUtils.MINUTE);
            console(Console.INFO, sm.getString("System ZapdostPozScheduler wystartowal. Cz�stotliwo��: "+period+" m."));
        }
    }

    @Override
    protected boolean canStop() {
        return !syncing;
    }

    class PositionMapper extends TimerTask
    {
        @Override
        public void run()
        {
            wybudzZrealizowane();
        }
    }

    private void wybudzZrealizowane() {
        boolean wasOpened = false;
        try{
            wasOpened = DSApi.openContextIfNeeded();
            if (!DSApi.context().inTransaction())
                DSApi.context().begin();

            try {
                syncing = true;
                PreparedStatement ps = DSApi.context().prepareStatement("SELECT a.document_id " +
                        " FROM INS_WniosekZapDoDost a " +
                        " INNER JOIN INS_WniosZapDost_Dict b ON a.document_id=b.DOCUMENT_ID " +
                        " INNER JOIN zapdostpoz_v c ON b.FIELD_VAL=c.ID " +
                        " where a.status=4 and b.FIELD_CN = 'REALIZACJAPOZYCJI' " +
                        " GROUP BY a.document_id " +
                        " HAVING SUM(ilosc)=SUM(ilzreal)");
                ResultSet rs = ps.executeQuery();
                while(rs.next()){
                    Long docId = rs.getLong(1);
                    // Przekazanie do ksi�gowo�ci

                    UtilTrigger.resume(
                            "wniosekzapdodost",
                            "czekanieNaERP",
                            docId,
                            "to zrealizowane",
                            log);
                    console(Console.INFO,"Wybudzony zosta� wniosek o ID " + docId);
                }
                ps.close();
            }catch (Exception e){
                log.error("B��d podczas mapowania: "+e);
                console(Console.ERROR,"B��d podczas mapowania: " +e, e);
            }finally {
                syncing = false;
            }
            DSApi.context().commit();
        }catch (Exception e){
            log.error("B��d podczas po��czenia z baz� danych: "+e);
            console(Console.ERROR,"B��d podczas po��czenia z baz� danych: " +e, e);
            try {
                DSApi.context().rollback();
            } catch (EdmException e1) {
                log.error("B��d podczas wycofywania transakcji: "+e);
                console(Console.ERROR, "B��d podczas wycofywania transakcji: " + e, e);
            }
        }finally {
            DSApi.closeContextIfNeeded(wasOpened);
        }
    }

    public Property[] getProperties()
    {
        return properties;
    }

    public void setProperties(Property[] properties)
    {
        this.properties = properties;
    }
}
