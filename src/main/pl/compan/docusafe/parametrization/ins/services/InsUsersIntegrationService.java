package pl.compan.docusafe.parametrization.ins.services;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.DuplicateNameException;
import pl.compan.docusafe.core.dockinds.field.DSUserEnumField;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.ReferenceToHimselfException;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.ad.ActiveDirectoryManager;
import pl.compan.docusafe.core.users.sql.DivisionImpl;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import javax.naming.Name;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

/**
 * Serwis odpowiadaj�cy za synchronizacj� struktury organizacyjnej w INS.
 * TODO:narazie jest tylko przekopiowany z POLITECHNIKI CZ�STOCHOWSKIEJ, nale�y go przerobi� na INS
 */
public class InsUsersIntegrationService extends ServiceDriver implements Service {
    public static final Logger log = LoggerFactory.getLogger(InsUsersIntegrationService.class);
    private final static StringManager sm = GlobalPreferences
            .loadPropertiesFile(InsUsersIntegrationService.class.getPackage().getName(), null);
//    public static final String PODSTAWOWE_UPRAWNIENIA = Docusafe.getAdditionProperty("pcz.rola.PodstawoweUprawnienia");
//    public static final String WSZYSCY_PRACOWNICY = Docusafe.getAdditionProperty("pcz.guid.WszyscyPracownicy");
//    public static final String STUDENCI = Docusafe.getAdditionProperty("pcz.guid.Studenci");
//    public static final String AD_DOMAIN_PREFIX = Docusafe.getAdditionProperty("active.directory.userDomainPrefix");
    private Property[] properties;
    private Timer timer;
    private Integer periodWww = 10;
    private String url = Docusafe.getAdditionProperty("erp.database.url");
    private String username = Docusafe.getAdditionProperty("erp.database.user");
    private String password = Docusafe.getAdditionProperty("erp.database.password");

    class ImportTimesProperty extends Property {
        public ImportTimesProperty() {
            super(SIMPLE, PERSISTENT, InsUsersIntegrationService.this, "periodWww", "Co ile minut", Integer.class);
        }

        protected Object getValueSpi() {
            return periodWww;
        }

        protected void setValueSpi(Object object) throws ServiceException {
            synchronized (InsUsersIntegrationService.this) {
                if (object != null)
                    periodWww = (Integer) object;
                else
                    periodWww = 10;
            }
        }
    }

    class URLProperty extends Property {
        public URLProperty() {
            super(SIMPLE, PERSISTENT, InsUsersIntegrationService.this, "url", "Sciezka do bazy", String.class);
        }

        protected Object getValueSpi() {
            return url;
        }

        protected void setValueSpi(Object object) throws ServiceException {
            synchronized (InsUsersIntegrationService.this) {
                if (object != null)
                    url = (String) object;
                else
                    url = Docusafe.getAdditionProperty("erp.database.url");
            }
        }
    }

    class UsernameProperty extends Property {
        public UsernameProperty() {
            super(SIMPLE, PERSISTENT, InsUsersIntegrationService.this, "username", "Nazwa u�ytkownika", String.class);
        }

        protected Object getValueSpi() {
            return username;
        }

        protected void setValueSpi(Object object) throws ServiceException {
            synchronized (InsUsersIntegrationService.this) {
                if (object != null)
                    username = object.toString();
                else
                    username = Docusafe.getAdditionProperty("erp.database.user");
            }
        }
    }

    class PasswordProperty extends Property {
        public PasswordProperty() {
            super(SIMPLE, PERSISTENT, InsUsersIntegrationService.this, "password", "Has�o", String.class);
        }

        protected Object getValueSpi() {
            return password;
        }

        protected void setValueSpi(Object object) throws ServiceException {
            synchronized (InsUsersIntegrationService.this) {
                if (object != null)
                    password = (String) object;
                else
                    password = Docusafe.getAdditionProperty("erp.database.password");
            }
        }
    }

    public InsUsersIntegrationService() {
        properties = new Property[] { new ImportTimesProperty(), new URLProperty(), new UsernameProperty(), new PasswordProperty() };
    }

    @Override
    protected void start() throws ServiceException {
        log.info("Start uslugi UsersIntegrationService");
        timer = new Timer(true);
        timer.schedule(new Import(), 0, periodWww * DateUtils.MINUTE);
        console(Console.INFO, sm.getString("System UsersIntegrationService wystartowal"));
    }

    @Override
    protected void stop() throws ServiceException {
        log.info("Stop uslugi UsersIntegrationService");
        console(Console.INFO, sm.getString("System UsersIntegrationService zako�czy� dzia�anie"));
        try {
            DSApi.close();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    protected boolean canStop() {
        return false;
    }

    class Import extends TimerTask {
        @Override
        public void run() {

            Connection conn = null;
            PreparedStatement ps = null;
            ResultSet rs = null;
            ActiveDirectoryManager ad = null;
            DirContext context = null;

            try {
                DSApi.openAdmin();
                conn = DriverManager.getConnection(url, username, password);

                //polaczenie z LDAP
                console(Console.INFO, "Start");
                console(Console.INFO, "Laczy z AD");
                ad = new ActiveDirectoryManager();
                context = ad.LdapConnectAsAdmin();
                console(Console.INFO, "Polaczyl sie z AD");

                synchronizeRootDivision(conn, ps);

                synchronizeOrganizationStructure(conn, ps, context);

//                synchronizeErpUsers(conn, ps);

                synchronizeUsersUsingErpAndLdap(conn, ps, context);

            } catch (SQLException se) {
                log.error(se.getMessage(), se);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                // console(Console.INFO, sm.getString(e.toString()));
                try {
                    DSApi.context().rollback();
                } catch (EdmException e1) {
                    log.error(e.getMessage(), e1);
                }
            } finally {
                try {
                    DSUserEnumField.reloadForAll();
                    DSApi.context().closeStatement(ps);
                    DSApi.close();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }

        private void synchronizeUsersUsingErpAndLdap(Connection conn, PreparedStatement ps, DirContext context) throws SQLException, EdmException {
            ResultSet rs;
            try{
                DSApi.context().begin();
                //wyszukanie wszystkich aktywnych pracownik�w ERP
                ps = conn.prepareStatement("select * from v_erp_users_to_divs");

                rs = ps.executeQuery();
                while (rs.next()) {
                    //pobranie nr ewid pracownika
                    Integer employeeNumber = rs.getInt("nrEwid");
                    //pobranie komorki organizacyjnej do ktorej jest przypisany uzytkownik
                    String komorkaId = rs.getString("komorka_id");
                    DSDivision division = null;
                    try{
                        division = DSDivision.findByExternalId(komorkaId);
                    }catch (DivisionNotFoundException e){
                        console(Console.ERROR, "Nie znaleziono dzia�u do kt�rego ma przynaleze� pracownik z nr ewid = "
                                +employeeNumber);
                        log.error("Nie znaleziono dzia�u do kt�rego ma przynaleze� pracownik z nr ewid = "
                                +employeeNumber);
                    }

                    if (employeeNumber != 0){
                        //sprawdzenie loginu z LDAP po nr ewid
                        SearchControls controls = new SearchControls();
                        controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
                        String tmpfilter = "(&(objectClass=user)(objectClass=person)(objectClass=organizationalPerson)" +
                                "(employeeNumber="+employeeNumber+"))";

                        console(Console.INFO, "Szuka uzytkownika z numerem ewidencyjnym = "+employeeNumber);
                        NamingEnumeration<SearchResult> renum = context.search("",tmpfilter, controls);
                        while (renum.hasMoreElements())
                        {
                            SearchResult element = (SearchResult) renum.nextElement();
                            String login = getStringAttr(element,"sAMAccountName");
                            String firstname = getStringAttr(element,"givenName");
                            String lastname = getStringAttr(element,"sn");
                            String email = getStringAttr(element,"mail");
                            //sprawdzenie czy istnieje uzytkownik
                            DSUser user;
                            try
                            {
                                user = DSUser.findByUsername(login);
                                console(Console.INFO, "Znalazl "+ user.asFirstnameLastname());
                                user.setFirstname(firstname);
                                user.setLastname(lastname);
                                user.setEmail(email);
                                user.update();
                            }
                            catch (UserNotFoundException e)
                            {
                                console(Console.INFO, "Nie znalazl dodaje");
                                user = UserFactory.getInstance().createUser(login, firstname, lastname);
                                user.setEmail(email);
                                user.setCtime(new Timestamp(new Date().getTime()));
                                user.setAdUser(true);
                                user.setDomainPrefix("ins");
                                user.setLoginDisabled(false);
                                console(Console.INFO, "Dodal "+user.asFirstnameLastname());
                            }
                            DSApi.context().session().save(user);

                            //przypisanie pracownika do komorki organizacyjnej
                            if (division != null)
                            {
                                if (!division.isHidden())
                                    division.addUser(user);
                                else{
                                    division = getFirstNotHiddenParentDiv(division);
                                    division.addUser(user);
                                }
                            }
                            break;
                        }
                    }
                }
                DSApi.context().commit();
            }catch(Exception e)
            {
                log.error("",e);
                console(Console.ERROR, "B��d [ "+e+" ]");
                DSApi.context().rollback();
            }
            finally
            {

                log.trace("Import  STOP");
                console(Console.INFO, "Koniec synchronizacji");
                if(context != null)
                    try {
                        context.close();
                    } catch (NamingException e) {
                        log.error(e.getMessage(),e);
                    }
            }
        }

        private String getStringAttr(SearchResult element,String name) throws NamingException
        {
            if(element.getAttributes().get(name) != null)
            {
                return (String) element.getAttributes().get(name) .get();
            }
            return null;
        }

        private void synchronizeOrganizationStructure(Connection conn, PreparedStatement ps, DirContext context) throws SQLException, EdmException {
            ResultSet rs;
            ps = conn.prepareStatement("select * from v_erp_org_structure");
            rs = ps.executeQuery();

            while (rs.next()) {
                DSDivision division = null;
                try {
                    division = DSDivision.findByExternalId(rs.getString("ID_KomPod"));
                    DSApi.context().begin();
                    updateDivision(rs, division);
                    DSApi.context().commit();
                } catch (DivisionNotFoundException de) {
                    String nazwaKomorki = null;
                    try {
                        DSApi.context().begin();
                        // pobieram bie��cy dzia� (o ile istnieje)
                        if (!StringUtils.isEmpty(rs.getString("ID_KomNad"))) {
                            division = DSDivision.findByExternalId(rs.getString("ID_KomNad"));
                        }
                        nazwaKomorki = rs.getString("Nazwa_KomPoda").trim();
                        DSDivision komorkaStworzona = division.createDivisionWithExternal(nazwaKomorki, null,
                                rs.getString("ID_KomPod"));
                        komorkaStworzona.setDescription("SIMPLE IMPORTED");
                        komorkaStworzona.setCode(rs.getString("IDK_KomPod").trim());

                        Integer kierownikId = rs.getInt("kierownik");
                        if (kierownikId!=0){
                            DSUser user = null;
                            try {
                                Integer employeeNumber = null;
                                PreparedStatement ps2 = conn.prepareStatement("select * from v_erp_users_to_divs " +
                                        " where pracownik_id = "+kierownikId);
                                ResultSet rs2 = ps2.executeQuery();
                                while(rs2.next()){
                                    //pobranie nr ewid pracownika
                                    employeeNumber = rs2.getInt("nrEwid");
                                    break;
                                }
                                if (employeeNumber != null){
                                    //sprawdzenie loginu z LDAP po nr ewid
                                    SearchControls controls = new SearchControls();
                                    controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
                                    String tmpfilter = "(&(objectClass=user)(objectClass=person)(objectClass=organizationalPerson)" +
                                            "(employeeNumber="+employeeNumber+"))";
                                    NamingEnumeration<SearchResult> renum = context.search("",tmpfilter, controls);
                                    while (renum.hasMoreElements())
                                    {
                                        SearchResult element = (SearchResult) renum.nextElement();
                                        String login = getStringAttr(element,"sAMAccountName");
                                        user = DSUser.findByUsername(login);
                                    }
                                }
                            }catch (Exception e){
                                log.error(e.getMessage(), e);
                            }
                            if (user != null)
                                createSupervisorsPosition(rs, komorkaStworzona, user);
                        }
                        komorkaStworzona.update();
                        DSApi.context().commit();

                        console(Console.INFO, sm.getString("System InsUsersIntegrationService zaimportowa� dzia� " + nazwaKomorki));
                    } catch (DuplicateNameException dne) {
                        try {
                            division = DSDivision.findByName(nazwaKomorki);
                            updateDivision(rs, division);
                            DSApi.context().commit();
                        } catch (Exception exc) {
                            log.error(exc.getMessage(), exc);
                            DSApi.context().rollback();
                        }

                    } catch (Exception exc) {
                        log.error(exc.getMessage(), exc);
                        DSApi.context().rollback();
                    }
                }
            }
            ps.close();
        }

        private void synchronizeRootDivision(Connection conn, PreparedStatement ps) throws SQLException, EdmException {
            ResultSet rs;
            ps = conn.prepareStatement("select * from [v_erp_org_structure] where IDK_KomNad = 'root' AND czy_aktywna = 1");
            rs = ps.executeQuery();

            if (rs.next()) {
                DSDivision division = null;
                try {
                    DSDivision.findByExternalId(rs.getString("ID_KomPod"));
                } catch (DivisionNotFoundException de) {
                    try {
                        division = DSDivision.find(DSDivision.ROOT_GUID);
                        DSApi.context().begin();
                        String nazwaKomorki = rs.getString("Nazwa_KomPoda");
                        division.setName(nazwaKomorki);
//                            division.setCode(rs.getString("ID_KomPod"));
                        division.setExternalId(rs.getString("ID_KomPod"));
                        division.setDescription("SIMPLE ROOT IMPORTED");
                        division.update();
                        DSApi.context().commit();
                    } catch (Exception exc) {
                        log.error(exc.getMessage(), exc);
                        DSApi.context().rollback();
                    }
                }
            }
            ps.close();
        }

        private void synchronizeErpUsers(Connection conn, PreparedStatement ps) throws Exception {
            ResultSet rs;
            ps = conn.prepareStatement("Select * FROM v_erp_users_to_divs");
            rs = ps.executeQuery();
//                String nrEwidencji = "";
            DSUser user = null;
            while (rs.next()) {
                DSDivision division = DSDivision.findByExternalId(rs.getString("ID_KomPod"));
                try{
                    user = DSUser.findByExternalName(rs.getString("uzytk_idn"));
                    updateUser(rs, user, division);
                }catch (UserNotFoundException e){
                    try {
                        division = DSDivision.findByExternalId(rs.getString("ID_KomPod"));
                        String erpLogin = rs.getString("uzytk_idn");
                        if (erpLogin != null && !erpLogin.equals("")) {
                            user = UserFactory.getInstance().createUser(erpLogin.replace(".", "").trim(),
                                    rs.getString("imie"), rs.getString("nazwisko"));
                            DSApi.context().begin();
                            user.setExternalName(erpLogin);
//                                user.setExtension(rs.getString("NrEwid"));
//                                String pracuje = rs.getString("Pracuje");
//                                if (pracuje.equals("N")) {
//                                    UserFactory.getInstance().deleteUser(rs.getString("Konto").replace(".", "").trim());
//                                }
//                                user.setLoginDisabled(pracuje.equals("T") ? false : true);
                            user.setRemarks("SIMPLE IMPORTED");
//                                user.setEmail(StringUtils.trimToNull(rs.getString("Email")));
                            user.setCtime(new Timestamp(new java.util.Date().getTime()));
//                                user.setAdUser(true);
//                                user.setDomainPrefix(AD_DOMAIN_PREFIX);
//                                Role.findByName(PODSTAWOWE_UPRAWNIENIA).addUser(user.getName());
                            division.addUser(user);
//                                try {
//                                    division = DSDivision.find(WSZYSCY_PRACOWNICY);
//                                    division.addUser(user);
//                                    division.update();
//                                } catch (DivisionNotFoundException e1) {
//                                    log.warn("System UsersIntegrationService nie doda� u�tkownika do grupy \"Wszyscy pracownicy\", poniewa� nie znalaz� takiego dzia�u");
//                                }
                            user.update();
                            division.update();
                            DSApi.context().commit();
//                                AbsenceFactory.createCard(user, "Politechnika Cz�stochowska", rs.getString("Funkcja"), null, null);
                            console(Console.INFO,
                                    sm.getString("System InsUsersIntegrationService zaimportowa� uzytkownika " + user.getName()));
                        }
                    } catch (DuplicateNameException dne) {
                        user = DSUser.findByUsername(rs.getString("Konto").replace(".", "").trim());
                        updateUser(rs, user, division);
                        DSApi.context().commit();
                    } catch (DivisionNotFoundException de) {
                        DSApi.context().rollback();
                        console(Console.INFO,
                                sm.getString("System UsersIntegrationService nie mo�e zaimportowa� u�ytkownika "
                                        + rs.getString("Imie") + " " + rs.getString("Nazwisko") + " poniewa� dzia�, do kt�rego ma by� przypisany ("
                                        + rs.getString("Komorka") + ") nie istnieje"));
                    } catch (Exception exc) {
                        log.error(exc.getMessage(), exc);
                        DSApi.context().rollback();
                    }
                }
            }
        }
    }

    private DSDivision getFirstNotHiddenParentDiv(DSDivision division) throws EdmException {
        if (division.getParent() == null)
            return null;

        if (!division.getParent().isHidden())
            return division.getParent();
        else
            return getFirstNotHiddenParentDiv(division.getParent());
    }

    public Property[] getProperties() {
        return properties;
    }

    public void setProperties(Property[] properties) {
        this.properties = properties;
    }

    private PreparedStatement removeUsersFromDivisions(Connection conn, PreparedStatement ps, ResultSet rs) throws EdmException,
            DivisionNotFoundException, SQLException {
        console(Console.INFO, sm.getString("UsersIntegrationService: Usuwanie u�ytkownik�w z dzia��w"));
        log.error("UsersIntegrationService: Usuwanie u�ytkownik�w z dzia��w");
        for (DSUser juser : DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME)) {
//            try {
//                if (juser.inDivision(DSDivision.find(STUDENCI)) || juser.isAdmin())
//                    continue;
//            } catch (Exception e) {
//                log.error(juser.getName());
//                log.error(e.getMessage(), e);
//            }
            for (DSDivision div : juser.getOriginalDivisionsWithoutGroup()) {
                if (div.isPosition()
                        || (Docusafe.getAdditionProperty("nieusuwalny.dzial") != null && Docusafe.getAdditionProperty(
                        "nieusuwalny.dzial").equals(div.getGuid())))
                    continue;
                ps.close();
                ps = conn.prepareStatement("select * from per_docusafe_uzytkownicy where Komorka = ? and NrEwid = ?");
                ps.setString(1, div.getExternalId());
                ps.setString(2, juser.getExtension());
                rs.close();
                rs = ps.executeQuery();
                if (!rs.next()) {
                    DSApi.context().begin();
                    try {
                        div.removeUser(juser);
                        div.update();
                        log.error("System UsersIntegrationService usun�� u�ytkownika " + juser.getName() + " nr."+ juser.getExtension() + " z dzia�u " + div.getName() +" nr. "+ div.getExternalId());
                        console(Console.INFO,
                                sm.getString("System UsersIntegrationService usun�� u�ytkownika " + juser.getName() + " z dzia�u "
                                        + div.getName()));
                        DSApi.context().commit();
                    } catch (EdmException edm) {
                        log.error(edm.getMessage(), edm);
                        DSApi.context().rollback();
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                        DSApi.context().rollback();
                    }
                }
                ps.close();
            }
        }
        return ps;
    }

    private PreparedStatement updateDivisions(Connection conn, PreparedStatement ps) throws EdmException, SQLException {
        ResultSet rs;
        console(Console.INFO, sm.getString("UsersIntegrationService: Aktualizacja/Usuwanie dzia��w"));
        log.error("UsersIntegrationService: Aktualizacja/Usuwanie dzia��w");
        for (DSDivision division : DSDivision.getOnlyDivisionsAndPositions(false)) {
            // Je�li dodajemy jakis dzia�, kt�ry nie istnieje w
            // strukturze ERP np. UPRAWNIENIA,
            // trzeba ustawi� dla takiego dzia�u description =
            // "docusafe",
            // inaczej zostanie usuni�ty przez serwis
            if (division.isGroup()
                    || "docusafe".equalsIgnoreCase(((DivisionImpl) division).getDescription())
                    || (Docusafe.getAdditionProperty("nieusuwalny.dzial") != null && Docusafe.getAdditionProperty(
                    "nieusuwalny.dzial").equals(division.getGuid())))
                continue;
            if (division.isPosition()) {
                String ending = division.getName().split("dzia�u", 2)[1].trim();
                ps = conn.prepareStatement("select * from per_docusafe_struktura where Nazwa_KomPoda = ?");
                ps.setString(1, ending);
                rs = ps.executeQuery();
                if (!rs.next()) {
                    UserFactory.getInstance().deleteDivision(division.getGuid(), DSDivision.ROOT_GUID);
                    log.error("System UsersIntegrationService usun�� dzia� " + division.getName());
                } else if (rs.getString("Kier_ImieNazwisko") == null) {
                    UserFactory.getInstance().deleteDivision(division.getGuid(), DSDivision.ROOT_GUID);
                    log.error("System UsersIntegrationService usun�� dzia� " + division.getName());
                }
                ps.close();
                rs.close();
            } else if (division.getUsers().length == 0) {
                String externalID;
                boolean canDelete = true;
                ps = conn.prepareStatement("select * from per_docusafe_struktura where ID_KomPod = ?");
                externalID = division.getExternalId();
                ps.setString(1, externalID);
                rs = ps.executeQuery();
                if (!rs.next()) {
                    ps.close();
                    for (DSDivision child : division.getChildren()) {
                        externalID = child.getExternalId();
                        ps = conn.prepareStatement("select * from per_docusafe_struktura where ID_KomPod = ?");
                        ps.setString(1, externalID);
                        rs = ps.executeQuery();
                        if (rs.next()) {
                            log.error("System UsersIntegrationService nie usun�� dzia�u " + division.getName()
                                    + ", w widoku per_docusafe_struktura nadal znajduj� si� dzia�y podleg�e temu dzia�owi");
                            console(Console.INFO,
                                    sm.getString("System UsersIntegrationService nie usun�� dzia�u " + division.getName()
                                            + ", w widoku per_docusafe_struktura nadal znajduj� si� dzia�y podleg�e temu dzia�owi"));
                            canDelete = false;
                            break;
                        }
                        ps.close();
                    }
                    if (canDelete) {
                        UserFactory.getInstance().deleteDivision(division.getGuid(), DSDivision.ROOT_GUID);
                        log.error("System UsersIntegrationService usun�� dzia� " + division.getName());
                        console(Console.INFO, sm.getString("System UsersIntegrationService usun�� dzia� " + division.getName()));
                    }
                }
            }
        }
        return ps;
    }

    protected static void updateUser(ResultSet rs, DSUser user, DSDivision division) throws EdmException, Exception, SQLException {
        DSApi.context().begin();
        boolean changed = false;
        try {
//            if (!user.getExtension().equals(rs.getString("NrEwid"))) {
//                user.setExtension(rs.getString("NrEwid"));
//                changed = true;
//            }
            if (!user.getFirstname().equals(rs.getString("Imie"))) {
                user.setFirstname(rs.getString("Imie"));
                changed = true;
            }
            if (!user.getLastname().equals(rs.getString("Nazwisko"))) {
                user.setLastname(rs.getString("Nazwisko"));
                changed = true;
            }
//            if (rs.getString("Email") != null && !StringUtils.trimToEmpty(rs.getString("Email")).equals(StringUtils.trimToEmpty(user.getEmail()))) {
//                user.setEmail(StringUtils.trimToNull(rs.getString("Email")));
//                changed = true;
//            }
            if (division != null && !user.inOriginalDivision(division)) {
                division.addUser(user);
                division.update();
                changed = true;
            }

//            String pracuje = rs.getString("Pracuje");
//            user.setLoginDisabled(pracuje.equals("T") ? false : true);
//            if (pracuje.equals("T") && user.isDeleted()) {
//                UserFactory.getInstance().revertUser(user.getName());
//                changed = true;
//            }
//            if (pracuje.equals("N") && !user.isDeleted()) {
//                UserFactory.getInstance().deleteUser(rs.getString("Konto").replace(".", "").trim());
//                changed = true;
//            }

            boolean flag = false;
//            for (String rola : user.getOfficeRoles()) {
//                if (rola.equals(PODSTAWOWE_UPRAWNIENIA))
//                    flag = true;
//            }
//            if (!flag)
//                Role.findByName(PODSTAWOWE_UPRAWNIENIA).addUser(user.getName());

//            try {
//                division = DSDivision.find(WSZYSCY_PRACOWNICY);
//                if (!user.inDivision(division)) {
//                    division.addUser(user);
//                    division.update();
//                }
//            } catch (UserNotFoundException e1) {
//                log.warn("System UsersIntegrationService nie doda� u�tkownika do grupy \"Wszyscy pracownicy\", poniewa� nie znalaz� takiego dzia�u");
//            }
//            user.setDomainPrefix(AD_DOMAIN_PREFIX);

            user.update();
            DSApi.context().commit();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            DSApi.context().rollback();
        }

//        if (AbsenceFactory.getActiveEmployeeCardByUserName(user.getName()) == null) {
//            AbsenceFactory.createCard(user, "Politechnika Cz�stochowska", rs.getString("Funkcja"), null, division.getName());
//            log.error("System tworzy kart� pracownika dla: " + user.getName());
//        } else {
//            EmployeeCard card = AbsenceFactory.getActiveEmployeeCardByUserName(user.getName());
//            if (card.getCompany() == null || !card.getCompany().equals("Politechnika Cz�stochowska")) {
//                card.setCompany("Politechnika Cz�stochowska");
////				log.error("System aktualizuje kart� pracownika " + user.getName() + "; card.company = " + card.getCompany());
//            }
//            if (card.getPosition() == null || !card.getPosition().equals(rs.getString("Funkcja"))) {
//                card.setPosition(rs.getString("Funkcja"));
////				log.error("System aktualizuje kart� pracownika " + user.getName() + "; card.position = " + card.getPosition());
//            }
//        }

        if (changed)
            log.error("System UsersIntegrationService zmodyfikowa� uzytkownika " + user.getName());
    }

    protected static void updateDivision(ResultSet rs, DSDivision division) throws EdmException, SQLException, DivisionNotFoundException,
            ReferenceToHimselfException {
        try {
//			DSApi.context().begin();
            boolean changed = false;
            if (division.isHidden())
                return;
            boolean czyAktywna = rs.getBigDecimal("czy_aktywna").compareTo(BigDecimal.ZERO)==0 ? false : true;
            if(division.isHidden() != !czyAktywna){
                division.setHidden(!czyAktywna);
                changed = true;
            }

            if(!division.getCode().equals(rs.getString("IDK_KomPod"))){
                division.setCode(rs.getString("IDK_KomPod").trim());
                changed = true;
            }

            if (!division.getName().equals(rs.getString("Nazwa_KomPoda").trim())) {
                division.setName(rs.getString("Nazwa_KomPoda").trim());
                changed = true;
            }
            if (!division.getExternalId().equals(rs.getString("ID_KomPod"))) {
                division.setExternalId(rs.getString("ID_KomPod"));
                changed = true;
            }
            if (!division.isRoot() && !division.getParent().getExternalId().equals(rs.getString("ID_KomNad"))) {
                division.setParent(DSDivision.findByExternalId(rs.getString("ID_KomNad")));
                changed = true;
            }

            Integer kierownikId = rs.getInt("kierownik");
            if (kierownikId!=0){
                DSUser user = null;
                try {
                    EnumItem pracownik = DataBaseEnumField.getEnumItemForTable(
                            "simple_erp_per_docusafe_pracownicy",kierownikId);
//                    pracownik.getTitle();
                    if (pracownik.getTitle().split(" ").length==2){
                        user = DSUser.findByFirstnameLastname(pracownik.getTitle().split(" ")[1],
                                pracownik.getTitle().split(" ")[0]);
                    }

                }catch (Exception e){
                    log.error(e.getMessage(), e);
                }
                if (user != null)
                    createSupervisorsPosition(rs, division, user);
            }
            division.update();
//			DSApi.context().commit();
            if (changed)
                log.error("System UsersIntegrationService zmodyfikowa� dzia� " + division.getName());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
//			DSApi.context().rollback();
        }
    }
    protected static void createSupervisorsPosition(ResultSet rs, DSDivision division, DSUser kierownik){
        try {
            DSDivision stanowiskoKierownika = null;
            for(DSDivision divs : division.getChildren(DSDivision.TYPE_POSITION)){
                if (divs.getName().equals("Kierownik")){
                    stanowiskoKierownika = divs;
                    break;
                }
            }

            if (stanowiskoKierownika == null)
                stanowiskoKierownika = division.createPosition("Kierownik");

            stanowiskoKierownika.addUser(kierownik);

            division.update();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}

