package pl.compan.docusafe.parametrization.ins.services;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.PersonNotFoundException;
import pl.compan.docusafe.service.*;
import pl.compan.docusafe.util.*;

import java.sql.*;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class InsContractorIntegrationsService extends ServiceDriver implements Service {
    public static final Logger log = LoggerFactory.getLogger(InsContractorIntegrationsService.class);
    private final static StringManager sm = GlobalPreferences
            .loadPropertiesFile(InsContractorIntegrationsService.class.getPackage().getName(), null);

    private Property[] properties;
    private Timer timer;
    private Integer periodWww = 10;
    private String url = Docusafe.getAdditionProperty("erp.database.url");
    private String username = Docusafe.getAdditionProperty("erp.database.user");
    private String password = Docusafe.getAdditionProperty("erp.database.password");

    class ImportTimesProperty extends Property {
        public ImportTimesProperty() {
            super(SIMPLE, PERSISTENT, InsContractorIntegrationsService.this, "periodWww", "Co ile minut", Integer.class);
        }

        protected Object getValueSpi() {
            return periodWww;
        }

        protected void setValueSpi(Object object) throws ServiceException {
            synchronized (InsContractorIntegrationsService.this) {
                if (object != null)
                    periodWww = (Integer) object;
                else
                    periodWww = 10;
            }
        }
    }

    class URLProperty extends Property {
        public URLProperty() {
            super(SIMPLE, PERSISTENT, InsContractorIntegrationsService.this, "url", "Sciezka do bazy", String.class);
        }

        protected Object getValueSpi() {
            return url;
        }

        protected void setValueSpi(Object object) throws ServiceException {
            synchronized (InsContractorIntegrationsService.this) {
                if (object != null)
                    url = (String) object;
                else
                    url = Docusafe.getAdditionProperty("erp.database.url");
            }
        }
    }

    class UsernameProperty extends Property {
        public UsernameProperty() {
            super(SIMPLE, PERSISTENT, InsContractorIntegrationsService.this, "username", "Nazwa u�ytkownika", String.class);
        }

        protected Object getValueSpi() {
            return username;
        }

        protected void setValueSpi(Object object) throws ServiceException {
            synchronized (InsContractorIntegrationsService.this) {
                if (object != null)
                    username = object.toString();
                else
                    username = Docusafe.getAdditionProperty("erp.database.user");
            }
        }
    }

    class PasswordProperty extends Property {
        public PasswordProperty() {
            super(SIMPLE, PERSISTENT, InsContractorIntegrationsService.this, "password", "Has�o", String.class);
        }

        protected Object getValueSpi() {
            return password;
        }

        protected void setValueSpi(Object object) throws ServiceException {
            synchronized (InsContractorIntegrationsService.this) {
                if (object != null)
                    password = (String) object;
                else
                    password = Docusafe.getAdditionProperty("erp.database.password");
            }
        }
    }

    public InsContractorIntegrationsService() {
        properties = new Property[] { new ImportTimesProperty(), new URLProperty(), new UsernameProperty(), new PasswordProperty() };
    }

    @Override
    protected void start() throws ServiceException {
        log.info("Start us�ugi ContractorIntegrationService");
        timer = new Timer(true);
        timer.schedule(new Import(), 0, periodWww * DateUtils.MINUTE);
        console(Console.INFO, sm.getString("System ContractorIntegrationService wystartowa�"));
    }

    @Override
    protected void stop() throws ServiceException {
        log.info("Stop us�ugi ContractorIntegrationService");
        console(Console.INFO, sm.getString("System ContractorIntegrationService zako�czy� dzia�anie"));
        try {
            DSApi.close();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    protected boolean canStop() {
        return false;
    }

    class Import extends TimerTask {
        @Override
        public void run() {

            Connection conn = null;
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                DSApi.openAdmin();
                conn = DriverManager.getConnection(url, username, password);

                ps = conn.prepareStatement("select * from [simple_erp_per_docusafe_kontrahenci_view]");
                rs = ps.executeQuery();

                while (rs.next()) {
                    DSApi.context().begin();
                    Person person = null;
                    String externalID = null;
                    try{
//                        String discriminator = rs.getString("DISCRIMINATOR");
                        String lparam = rs.getString("lparam");
                        externalID = rs.getString("externalID");
                        person = Person.findPersonByExternalIdAndLparam(externalID,lparam);

                        person.setOrganization(rs.getString("organization"));
                        person.setZip(rs.getString("zip"));
                        person.setLocation(rs.getString("location"));
                        person.setStreet(rs.getString("street"));
                        String kraj = rs.getString("country");
                        DSCountry country = null;
                        if (kraj != null && !"".equals(kraj)){
                            List<DSCountry> countryList = DSCountry.find(null, kraj, null);
                            if (countryList!= null && countryList.size()>0)
                                country = countryList.get(0);
                        }
                        if(country != null)
                            person.setCountry(country.getId().toString());
                        person.setNip(rs.getString("nip"));

                        person.update();
                        log.info("Zaktualizowano dane kontrahenta (externalId=" + externalID + ")");
                        console(Console.INFO, sm.getString("Zaktualizowano dane kontrahenta (externalId=" + externalID + ")"));
                    }catch (PersonNotFoundException e){
                        log.info("Us�uga InsContractorIntegrationService: nie znaleziono kontrahenta (externalId=" + externalID + "). Kontrahent zostaje dodany do systemu EOD.");
                        console(Console.INFO, sm.getString("Nie znaleziono kontrahenta (externalId=" + externalID + ")"));
                        person = new Person();
                        person.setDictionaryGuid("rootdivision");
                        person.setOrganization(rs.getString("organization"));
                        person.setZip(rs.getString("zip"));
                        person.setLocation(rs.getString("location"));
                        person.setStreet(rs.getString("street"));
                        String kraj = rs.getString("country");
                        DSCountry country = null;
                        if (kraj != null && !"".equals(kraj)){
                            List<DSCountry> countryList = DSCountry.find(null, kraj, null);
                            if (countryList!= null && countryList.size()>0)
                                country = countryList.get(0);
                        }
                        if(country != null)
                            person.setCountry(country.getId().toString());
                        person.setNip(rs.getString("nip"));
                        person.setLparam(rs.getString("lparam"));
                        person.setExternalID(rs.getString("externalID"));
                        person.create();
                        log.info("Dodano kontrahenta (externalId=" + externalID + ")");
                        console(Console.INFO, sm.getString("Dodano kontrahenta (externalId=" + externalID + ")"));
                    }finally {
                        DSApi.context().commit();
                    }
                }
                ps.close();
            } catch (SQLException se) {
                log.error(se.getMessage(), se);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                // console(Console.INFO, sm.getString(e.toString()));
                try {
                    DSApi.context().rollback();
                } catch (EdmException e1) {
                    log.error(e.getMessage(), e1);
                }
            } finally {
                try {
                    DSApi.context().closeStatement(ps);
                    DSApi.close();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    public Property[] getProperties() {
        return properties;
    }

    public void setProperties(Property[] properties) {
        this.properties = properties;
    }
}

