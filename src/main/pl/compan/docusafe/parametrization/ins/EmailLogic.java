package pl.compan.docusafe.parametrization.ins;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

import edu.emory.mathcs.backport.java.util.Arrays;

public class EmailLogic extends AbstractDocumentLogic {
	private final static Logger log = LoggerFactory.getLogger(EmailLogic.class);
	private static EmailLogic instance;
	private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
	public static EmailLogic getInstance() {
		if (instance == null)
			instance= new EmailLogic();
		return instance;
	}

	@Override
	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));
		
		for (PermissionBean perm : perms) {
			if (perm.isCreateGroup()
					&& perm.getSubjectType().equalsIgnoreCase(
							ObjectPermission.GROUP)) {
				String groupName = perm.getGroupName();
				if (groupName == null) {
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}

	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {
		
		as.useFolderResolver(document);
		as.useAvailableProviders(document);
		log.info("document title : '{}', description : '{}'",
				document.getTitle(), document.getDescription());

	}
	
	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
			throws EdmException {
		DSApi.openAdmin();
		List<OfficeCase> ca = OfficeCase.findByClerk(DSApi.context().getPrincipalName());
		
		DSApi._close();
		return null;
	}
	
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{		
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			if (document instanceof InOfficeDocument) {
				map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
			} else {
				map.put(ASSIGN_USER_PARAM,
						event.getAttribute(ASSIGN_USER_PARAM));
				map.put(ASSIGN_DIVISION_GUID_PARAM,
						event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			}

			//konczenie procesu jesli mail zostal wyslany
			FieldsManager fm = document.getFieldsManager();
			if(!fm.getValue("WYSLAC").equals("Tak")){
			document.getDocumentKind().getDockindInfo()
					.getProcessesDeclarations().onStart(document, map);
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

	@Override
	public void onSaveActions(DocumentKind kind, Long documentId,
			Map<String, ?> fieldValues) throws SQLException, EdmException {

		OfficeDocument doc = OfficeDocument.find(documentId);
		String[] toEmail = null;
		String[] emailCC = null;
		String[] emailBCC = null;
		String mail = "";
		String mailCC = "";
		String mailBCC = "";
		FieldsManager fm = doc.getFieldsManager();
		if(fm.getValue("WYSLAC").equals("Tak")){
			Map<String, FieldData> slowniki = (Map<String, FieldData>) fieldValues.get("M_DICT_VALUES");
			if(slowniki != null){
				for (Map.Entry<String,FieldData> entry : slowniki.entrySet()) {
					if(entry.getKey().startsWith("SENDER_")){
						Map<String, FieldData> sender = (Map<String, FieldData>) entry.getValue();
						for(Map.Entry<String, FieldData> ent : sender.entrySet()) {
							if(ent.getKey().equalsIgnoreCase("EMAIL") && ent.getValue() != null){
								mail += "," + ent.getValue().toString();
							}
						}
					} else if(entry.getKey().startsWith("SENDERKOPIA_")){
						Map<String, FieldData> sender_kopia = (Map<String, FieldData>) entry.getValue();
						for(Map.Entry<String, FieldData> ent : sender_kopia.entrySet()) {
							if(ent.getKey().equalsIgnoreCase("EMAIL") && ent.getValue() != null){
								mailCC += "," + ent.getValue().toString();
							}
						}
					} else if(entry.getKey().startsWith("SENDERUKRYTA")){
						Map<String, FieldData> sender_ukryta_kopia = (Map<String, FieldData>) entry.getValue();
						for(Map.Entry<String, FieldData> ent : sender_ukryta_kopia.entrySet()) {
							if(ent.getKey().equalsIgnoreCase("EMAIL") && ent.getValue() != null){
								mailBCC += "," +ent.getValue().toString();
							}
						}
					}
				}
			}
			String[] email = mail.split(",");
			List<String> list = new ArrayList<String>(Arrays.asList(email));
			if(!list.isEmpty()){
				list.remove(0);
				toEmail = list.toArray(new String[0]);
			}
			String[] emailC = mailCC.split(",");
			List<String> listCC = new ArrayList<String>(Arrays.asList(emailC));
			if(!listCC.isEmpty()){
				listCC.remove(0);
				emailCC = listCC.toArray(new String[0]);
			}
			String[] emailBC = mailBCC.split(",");
			List<String> listBCC = new ArrayList<String>(Arrays.asList(emailBC));
			if(!listBCC.isEmpty()){
				listBCC.remove(0);
				emailBCC = listBCC.toArray(new String[0]);
			}
			List<File> tempAttachments = new ArrayList<File>();
			Collection<Mailer.Attachment> attachments = null;
			Collection<Mailer.Attachment> attachmentsTmp = null;
			try {
				attachments = getMailerAttachment(doc, tempAttachments);
			} catch (IOException e) {
				log.error(" "+ e);
			}
			String message = (String) fm.getValue("CONTENTDATA");
			String subject = (String) fm.getValue("TEMAT");
			if(toEmail != null && fm.getValue("WYSLANO") == null){
				((Mailer) ServiceManager.getService(Mailer.NAME)).send(toEmail, emailCC, emailBCC, subject, message, attachments);

				if(doc.getOfficeNumber() == null){
					AssignOfficeNumber(doc);
				}

				Date data = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
				doc.setCzyCzystopis(true);
				doc.addWorkHistoryEntry(Audit.create("email",
						DSApi.context().getPrincipalName(), sm.getString("Wys豉no email", sdf.format(data))));
				try {
					PreparedStatement ps = DSApi.context().prepareStatement( "UPDATE DOCUMENT_IN_EMAIL set wyslano=1 where document_id=?");
					ps.setLong(1, documentId);
					ps.executeUpdate();
				} catch (SQLException e) {
					log.error(e.getMessage(), e);
				} catch (EdmException e) {
					log.error(e.getMessage(), e);
				}
			} else {
				throw new EdmException("Nie podano adresu email.");
			}
		}	
	}
	

	@Override
	public void validate(Map<String, Object> values, DocumentKind kind,
			Long documentId) throws EdmException {
	
	}
	
	private Collection<Mailer.Attachment> getMailerAttachment(Document doc, List<File> tempAttachments) throws EdmException, IOException
	{
		//Dodaje zalaczniki
		List<Attachment> docAtta = doc.getAttachments();
		Collection<Mailer.Attachment> attachments = null;
		
		if(docAtta != null && !docAtta.isEmpty())
		{
			attachments = new ArrayList<Mailer.Attachment>();
			
			for(Attachment att : docAtta)
			{
				if(att.isDeleted()) continue;
				AttachmentRevision ar = att.getMostRecentRevision();
				File tempAr = ar.saveToTempFile();
				if(ar.getSize() > 0)
				{
					attachments.add(new Mailer.Attachment(ar.getOriginalFilename(), tempAr.toURI().toURL()));
				}
				tempAttachments.add(tempAr);
			}
		}
		
		return attachments;
	}
	
	private void AssignOfficeNumber(Document doc){
		try
		{
			if (doc instanceof OutOfficeDocument)
			{
				Integer sequenceId = Journal.TX_newEntry2(Journal.getMainOutgoing().getId(), doc.getId(), new Date());
				((OutOfficeDocument) doc).bindToJournal(Journal.getMainOutgoing().getId(), sequenceId);
				((OutOfficeDocument) doc).setOfficeNumber(sequenceId);
			}
			DSApi.context().commit();
			DSApi.context().begin();
		} catch (Exception e)
		{	
			DSApi.context()._rollback();
			log.error("B??d wykonywania akcji "+ e);
		}
	}
	
	/**
	 * Zwraca list� u篡tkownik闚 kt鏎zy maja dost瘼 do dokumentu
	 * Na podstawie:
	 * 	ds_permission - Subjecttyp brany pod uwag� to user i group.
	 * 	tabela DSW_JBPM_TASKLIST - U kogo aktualnie znajduje si� pismo.
	 * @param documentId
	 * @return
	 */
	@Override
	public List<DSUser> getUserWithAccessToDocument(Long documentId){
		try {
			Set<PermissionBean> docperm = DSApi.context().getDocumentPermissions(Document.find(documentId));
			HashMap<String, PermissionBean> test = new HashMap<String, PermissionBean>();
			for (PermissionBean permissionBean : docperm) {
				if(!test.containsKey(permissionBean.getSubject())){
					test.put(permissionBean.getSubject(), permissionBean);					
				}
			}
			
			Set<String> keys = test.keySet();
			List<DSUser> dsUsers = new ArrayList<DSUser>();
			for (String string : keys) {
				if(test.get(string).getSubjectType().equalsIgnoreCase("user"))
					dsUsers.add(DSUser.findByUsername(string));
			}
			
			return dsUsers;
			
		} catch (EdmException e) {
			log.debug("b章d podczas szukania dokumentu.");
			return null;
		}
	}
}