package pl.compan.docusafe.parametrization.ins;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;


/**
 *
 */
public class NormalLogic extends AbstractDocumentLogic {

    private static NormalLogic instance;
    protected static Logger log = LoggerFactory.getLogger(NormalLogic.class);
    private StringManager sm = GlobalPreferences.loadPropertiesFile(
			NormalLogic.class.getPackage().getName(), null);
    public final static String BARCODE = "BARCODE";
    private boolean dodajDoHist = false;
    public static NormalLogic getInstance() {
        if (instance == null)
            instance = new NormalLogic();
        return instance;
    }

    public void documentPermissions(Document document) throws EdmException {
        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
        perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

        for (PermissionBean perm : perms) {
            if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP)) {
                String groupName = perm.getGroupName();
                if (groupName == null) {
                    groupName = perm.getSubject();
                }
                createOrFind(document, groupName, perm.getSubject());
            }
            DSApi.context().grant(document, perm);
            DSApi.context().session().flush();
        }
    }
	
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
        log.info("type: {}", type);
        Map<String,Object> values = new HashMap<String,Object>();
        values.put("TYPE", type);
        DSUser user = DSApi.context().getDSUser();
        if (user.getDivisionsWithoutGroup().length>0){
            String usrStr = "u:"+user.getName()+";";
            usrStr+= "d:"+user.getDivisionsWithoutGroup()[0].getGuid();
            values.put("SENDER_HERE", usrStr);
        }
		if(fm.getDocumentKind().getCn().equals("normal_out")){
			fm.getField("RODZAJ_PISMA_OUT").setReadOnly(false);
			fm.getField("DOC_DESCRIPTION").setReadOnly(false);
			fm.getField("SENDER_HERE").setReadOnly(false);
			fm.getField("DOC_DATE").setReadOnly(false);
			fm.getField("POSTAL_REG_NR").setReadOnly(false);
			fm.getField("DOC_DELIVERY").setReadOnly(false);
			fm.getField("DOC_NUMBER").setReadOnly(false);
			fm.getField("RECIPIENT").setReadOnly(false);
			fm.getField("JOURNAL").setReadOnly(false);
		}
        fm.reloadValues(values);
	}
	
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        as.useFolderResolver(document);
        as.useAvailableProviders(document);
        log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
    }

    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) {
        log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
        try {
            Map<String, Object> map = Maps.newHashMap();
            if (document instanceof InOfficeDocument)
            {
            	 map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
            }
            else{
            	map.put(ASSIGN_USER_PARAM,
						DSApi.context().getPrincipalName());
            	map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
            }
           
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
            
            String barcode = document.getFieldsManager().getStringValue(BARCODE);
           
			if(document instanceof InOfficeDocument){
				InOfficeDocument in = (InOfficeDocument) document;
			if (barcode != null) {
				in.setBarcode(barcode);
			} else {
				in.setBarcode("Brak");
			}
			} else if (document instanceof OutOfficeDocument) {
				OutOfficeDocument od = (OutOfficeDocument) document;
				if (barcode != null) {
					od.setBarcode(barcode);
				} else {
					od.setBarcode("Brak");
				}
			}
            if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
			

				
			
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    @Override
    public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException {
        ProcessCoordinator ret = new ProcessCoordinator();
        ret.setUsername(document.getAuthor());
        return ret;
    }
/*
    private final static OcrSupport OCR_SUPPORT = OcrUtils.getFileOcrSupport(
                new File(Docusafe.getHome(), "ocr-dest"),
                new File(Docusafe.getHome(), "ocr-get"),
                Collections.singleton("tiff")
    );

    @Override
    public OcrSupport getOcrSupport() {
        return OCR_SUPPORT;
    }
    */

	@Override
	public void onRejectedListener(Document doc) throws EdmException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException
	{
		// TODO Auto-generated method stub
		
	}
	
	public TaskListParams getTaskListParams(DocumentKind dockind, long documentId) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
			if(dockind.equals("normal")){
				InOfficeDocument doc = InOfficeDocument.findInOfficeDocument(documentId);
				if(doc.getDelivery()!=null)
					params.setCategory(doc.getDelivery().getName());
			}
			OfficeDocument doc = OfficeDocument.find(documentId);
			FieldsManager fm = doc.getFieldsManager();
				if(fm.getValue("DOC_NUMBER") != null)
					params.setDocumentNumber((String) fm.getValue("DOC_NUMBER"));
				if(fm.getValue("DOC_DATE") != null)
					params.setAttribute(fmt.format(fm.getValue("DOC_DATE")),3);
		}
		catch (Exception e)
		{
			log.error("",e);
		}
		return params;
	}
	
	@Override
	public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
	{
		//dodanie na formatke kolejnego numeru z dziennika wydzialowego
		if(values.get("JOURNAL") != null){
			String dziennik = String.valueOf(values.get("JOURNAL"));
			PreparedStatement ps = null;
			ResultSet rs = null;
			int numer = 0;
			if(documentId != null){
				List<JournalEntry> journal = JournalEntry.findByDocumentId(documentId);
				for(JournalEntry j:journal){
					if(j.getJournal().getId().equals(Long.valueOf(dziennik))){
						numer = j.getSequenceId();
						values.put("NRKOLEJNY", numer);
					}
				}
			}
			if(numer == 0){
				try {
					ps = DSApi.context().prepareStatement("SELECT MAX(sequenceid) as numer from dso_journal_entry where JOURNAL_ID=?");

					ps.setLong(1, Long.valueOf(dziennik));
					rs = ps.executeQuery();
					rs.next();
					numer = rs.getInt("numer");

					values.put("NRKOLEJNY", numer+1);
					dodajDoHist = true;
				} catch (SQLException e) {
					log.error("", e);
				}
			}
		}
	}
	
	@Override
	public void onSaveActions(DocumentKind kind, Long documentId,
			Map<String, ?> fieldValues) throws SQLException, EdmException {
		OfficeDocument document = OfficeDocument.find(documentId);
		String dziennik = "";

		if(fieldValues.get("JOURNAL") != null){
			dziennik = String.valueOf(fieldValues.get("JOURNAL"));

			Journal jName = Journal.find(Long.valueOf(dziennik));
			if(dodajDoHist)
				document.addWorkHistoryEntry(Audit.create("journal", DSApi.context().getPrincipalName(),
						sm.getString("DodanoDoDziennikaWydzialowego",jName.getDescription())));
		}
        if(fieldValues.containsKey("doc_number") && fieldValues.get("doc_number") != null && document instanceof InOfficeDocument)
            ((InOfficeDocument) document).setReferenceId((String)fieldValues.get("doc_number"));
	}
	
	/**
	 * Zwraca list� u篡tkownik闚 kt鏎zy maja dost瘼 do dokumentu
	 * Na podstawie:
	 * 	ds_permission - Subjecttyp brany pod uwag� to user i group.
	 * 	tabela DSW_JBPM_TASKLIST - U kogo aktualnie znajduje si� pismo.
	 * @param documentId
	 * @return
	 */
	@Override
	public List<DSUser> getUserWithAccessToDocument(Long documentId){
		try {
			Set<PermissionBean> docperm = DSApi.context().getDocumentPermissions(Document.find(documentId));
			HashMap<String, PermissionBean> test = new HashMap<String, PermissionBean>();
			for (PermissionBean permissionBean : docperm) {
				if(!test.containsKey(permissionBean.getSubject())){
					test.put(permissionBean.getSubject(), permissionBean);					
				}
			}
			
			Set<String> keys = test.keySet();
			List<DSUser> dsUsers = new ArrayList<DSUser>();
			for (String string : keys) {
				if(test.get(string).getSubjectType().equalsIgnoreCase("user"))
					dsUsers.add(DSUser.findByUsername(string));
			}
			
			return dsUsers;
			
		} catch (EdmException e) {
			log.debug("b章d podczas szukania dokumentu.");
			return null;
		}
	}

    public Map<String, String> setInitialValuesForReplies(Long inDocumentId) throws EdmException
    {
        Map<String, String> dependsDocs = new HashMap<String, String>();
        Long recipientChildId = OfficeDocument.find(inDocumentId).getSender().getId();
        Long recipientBaseId = OfficeDocument.find(inDocumentId).getSender().getBasePersonId();
        Long recipientId;

        recipientId = (recipientBaseId != null && Person.findIfExist(recipientBaseId) != null)?recipientBaseId:recipientChildId;

        dependsDocs.put("RECIPIENT", String.valueOf(recipientId));
        //czy user ma swoj dzial
        if (DSApi.context().getDSUser().getDivisionsWithoutGroup().length>0) {
            String sender = "u:"+DSApi.context().getDSUser().getName();
            sender+=(";d:"+DSApi.context().getDSUser().getDivisionsWithoutGroup()[0].getGuid());
            dependsDocs.put("SENDER_HERE", sender);
        }
        return dependsDocs;
    }
}
