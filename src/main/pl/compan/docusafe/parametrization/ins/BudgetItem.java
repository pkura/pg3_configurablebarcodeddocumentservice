package pl.compan.docusafe.parametrization.ins;

import java.math.BigDecimal;

public class BudgetItem
{
	String budzet, pozycja, zrodlo, status;
	BigDecimal kwota;
	int idBlokady;
	
	public BudgetItem(String budzet, String pozycja, String zrodlo,
			String status, BigDecimal kwota, int idBlokady) {
		super();
		this.budzet = budzet;
		this.pozycja = pozycja;
		this.zrodlo = zrodlo;
		this.status = status;
		this.kwota = kwota;
		this.idBlokady = idBlokady;
	}
	
	public BudgetItem(){
		
	}

	public String getBudzet() {
		return budzet;
	}

	public void setBudzet(String budzet) {
		this.budzet = budzet;
	}

	public String getPozycja() {
		return pozycja;
	}

	public void setPozycja(String pozycja) {
		this.pozycja = pozycja;
	}

	public String getZrodlo() {
		return zrodlo;
	}

	public void setZrodlo(String zrodlo) {
		this.zrodlo = zrodlo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getKwota() {
		return kwota;
	}

	public void setKwota(BigDecimal kwota) {
		this.kwota = kwota;
	}

	public int getIdBlokady() {
		return idBlokady;
	}

	public void setIdBlokady(int idBlokady) {
		this.idBlokady = idBlokady;
	}
	
	
	
	
	

}
