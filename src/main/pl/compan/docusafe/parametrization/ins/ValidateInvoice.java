package pl.compan.docusafe.parametrization.ins;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ins.entities.PozycjaBudzetowaFaktury;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.util.*;


public class ValidateInvoice extends AbstractEventListener {
    private final static Logger log = LoggerFactory.getLogger(ValidateInvoice.class);

    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY)+"");
        OfficeDocument doc = OfficeDocument.find(docId);
        FieldsManager fm = doc.getFieldsManager();
        Map<String, Object> fieldValues = fm.getFieldValues();

        Date dataWplywu = (Date) fm.getKey(CostInvoiceLogic.DATA_WPLYWU_DOCKIND_CN);
        Date terminPlatnosci = (Date) fm.getKey(CostInvoiceLogic.TERMIN_PLATNOSCI_DOCKIND_CN);
        Date dataWystawienia = (Date) fm.getKey(CostInvoiceLogic.DATA_WYSTAWIENIA_DOCKIND_CN);

        if(dataWystawienia.compareTo(new Date())>0){
            throw new EdmException("Data wystawienia dokumentu nie mo�e by� dat� z przysz�o�ci.");
        }
        if(dataWplywu.compareTo(dataWystawienia)<0){
            throw new EdmException("Data wp�ywu jest wcze�niejsza od daty wystawienia dokumentu.");
        }

        if(terminPlatnosci.compareTo(dataWystawienia)<0){
            throw new EdmException("Termin p�atno�ci jest wcze�niejszy od daty wystawienia dokumentu.");
        }
        Calendar wystawienie = Calendar.getInstance();
        wystawienie.setTime(dataWystawienia);
        Calendar rokTemu = Calendar.getInstance();
        rokTemu.setTime(new Date());
        rokTemu.add(Calendar.YEAR, -1); // odjac rok
        if(wystawienie.compareTo(rokTemu)<0){
            throw new EdmException("Data wystawienia jest zbyt odleg�a od tera�niejszo�ci.");
        }

        //sprawdzanie pozycji budzetowych
//        Object mpkDict = fm.getValue(CostInvoiceLogic.MPK_DOCKIND_CN);
//        List<Long> mpkIds = new ArrayList<Long>();
//        if (mpkDict != null){
//            for (Object o: (List) mpkDict){
//                Map mpkMap = (Map) o;
//                mpkIds.add(((Integer) mpkMap
//                        .get(CostInvoiceLogic.MPK_DOCKIND_CN+"_"+CostInvoiceLogic.MPK_ID_DOCKIND_CN)).longValue());
//            }
//        }
        //sprawdzanie pozycji faktury
        Object pozFakDict = fm.getValue(CostInvoiceLogic.MPK_DOCKIND_CN);
        BigDecimal brutto = (BigDecimal) fieldValues.get(CostInvoiceLogic.KWOTA_BRUTTO_DOCKIND_CN);
        BigDecimal netto = (BigDecimal) fieldValues.get(CostInvoiceLogic.KWOTA_NETTO_WALBAZ_DOCKIND_CN);
        BigDecimal vat = (BigDecimal) fieldValues.get(CostInvoiceLogic.KWOTA_VAT_DOCKIND_CN);
        BigDecimal sumaBrutto = BigDecimal.ZERO;
        BigDecimal sumaNetto = BigDecimal.ZERO;
        BigDecimal sumaVat= BigDecimal.ZERO;
        if (pozFakDict == null)
            throw new EdmException("Brak danych pozycji faktury.");
        for (Object o : (List) pozFakDict){
            Map poz = (Map) o;
//            BigDecimal pozId = (BigDecimal) poz.get(CostInvoiceLogic.STAWKI_VAT_DOCKIND_CN + "_"
//                    + CostInvoiceLogic.STAWKI_VAT_ID_DOCKIND_CN);
            sumaBrutto = sumaBrutto.add((BigDecimal) poz.get(CostInvoiceLogic.MPK_DOCKIND_CN + "_"
                    + "BRUTTO"));
            BigDecimal pozNetto = (BigDecimal) poz.get(CostInvoiceLogic.MPK_DOCKIND_CN + "_"
                    + "NETTO");
            sumaNetto= sumaNetto.add(pozNetto);
            sumaVat = sumaVat.add((BigDecimal) poz.get(CostInvoiceLogic.MPK_DOCKIND_CN + "_"
                    + "VAT"));
        }
        if (sumaBrutto.compareTo(brutto)!=0)
            throw new EdmException("Suma kwot brutto pozycji ("+sumaBrutto+") nie jest r�wna kwocie brutto na fakturze"+
                    " ("+ brutto +").");
        if (sumaNetto.compareTo(netto)!=0)
            throw new EdmException("Suma kwot netto pozycji ("+sumaNetto+") nie jest r�wna kwocie netto na fakturze"+
                    " ("+ netto +").");
        if (sumaVat.compareTo(vat)!=0)
            throw new EdmException("Suma kwot VAT pozycji ("+sumaVat+") nie jest r�wna kwocie VAT na fakturze"+
                    " ("+ vat +").");
    }
}
