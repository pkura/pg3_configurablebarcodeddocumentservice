package pl.compan.docusafe.parametrization.eservice;

import java.util.HashMap;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.service.imports.dsi.DSIImportHandler;
import pl.compan.docusafe.service.imports.dsi.ImportHelper;

public class EserviceDSIHandler extends DSIImportHandler
{

	public String getDockindCn() 
	{
		return "eservice";
	}
	protected static HashMap<String, Integer> klasaMap = new HashMap<String, Integer>();
	static
	{
		klasaMap.put("Umowy",10);
		klasaMap.put("Dokumenty formalne",40);
		klasaMap.put("Formularze",20);
		klasaMap.put("Aneksy",30);
		klasaMap.put("Protokoey",50);
		klasaMap.put("Protoko�y",50);
		klasaMap.put("Protokoly",50);
		klasaMap.put("Inne",60);
		
	}
	
	@Override
	protected boolean isStartProces()
	{
		return true;
	}
	
	protected void prepareImport() throws Exception
	{
		ImportHelper.renameField(values, "NR UMOWY", "NR_UMOWY");
		ImportHelper.renameField(values, "NR SPRAWY", "NR_SPRAWY");
		ImportHelper.renameField(values, "NR POS ID", "NR_POS_ID");
		ImportHelper.renameField(values, "Barkod dokumentu", "BARCODE");
		String klasa = (String) values.get("KLASA");
		values.put("KLASA", klasaMap.get(klasa));
		String nipKlienta = (String)values.get("NIP KLIENTA");
		String numerKlienta = (String)values.get("NR KLIENTA");
		String nazwaKlienta = (String)values.get("NAZWA KLIENTA");
		values.put("KLIENT", getKlient(nipKlienta, numerKlienta, nazwaKlienta));
	}
	
	private Long getKlient(String nipKlienta, String numerKlienta, String nazwaKlienta) throws EdmException
	{
		Criteria c = DSApi.context().session().createCriteria(DicInvoice.class);
		c.add(Restrictions.like("numerKontrahenta",  numerKlienta));
		c.add(Restrictions.like("nip",  nipKlienta));
		//c.add(Restrictions.like("numerKontrahenta",  numerKlienta));
		List<DicInvoice> result = c.list();
		if(result != null && result.size() > 0)
			return result.get(0).getId();
		DicInvoice dic = new DicInvoice();
		dic.setName(nazwaKlienta);
		dic.setNip(nipKlienta);
		dic.setNumerKontrahenta(numerKlienta);
		dic.create();
        DSApi.context().session().save(dic);
        DSApi.context().session().flush();
        DSApi.context().session().refresh(dic);
		return dic.getId();
	}
}
