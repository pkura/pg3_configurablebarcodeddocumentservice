package pl.compan.docusafe.parametrization.eservice;

import static java.lang.String.format;
import static pl.compan.docusafe.util.FolderInserter.root;
import static pl.compan.docusafe.util.FolderInserter.toQuarter;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class EserviceLogic extends AbstractDocumentLogic{
    private final static Logger LOG = LoggerFactory.getLogger(EserviceLogic.class);

    public final static String KLASA_FIELD_CN = "KLASA";
    public final static String TYP_FIELD_CN = "TYP";
    public final static String KLIENT_FIELD_CN = "KLIENT";

    public final static String UMOWA_KLASA_CN = "UMOWA";

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        
        String klasa = fm.getEnumItemCn(KLASA_FIELD_CN);
        if(klasa == null)
        	throw new EdmException("Brak pola KLASA");

        LOG.info("archiveAction eservice");

        if(fm.getValue(TYP_FIELD_CN) != null)
        	document.setTitle(fm.getValue(TYP_FIELD_CN)+getTitle(fm));
        else if(fm.getValue(KLASA_FIELD_CN) != null)
        	document.setTitle(fm.getValue(KLASA_FIELD_CN)+"" +getTitle(fm));

        FolderInserter inserter = null;
        if(fm.getValue(KLIENT_FIELD_CN) != null)
        {
        	DicInvoice dic = (DicInvoice) fm.getValue(KLIENT_FIELD_CN);
        	inserter = root().subfolder(toQuarter(dic.getName() + ""))
			.subfolder(format("%s (%s)",dic.getName(), dic.getNumerKontrahenta()));
        	if(fm.getValue("NR_UMOWY") != null)
        		inserter = inserter.subfolder(""+fm.getValue("NR_UMOWY"));
        	else
        		inserter = inserter.subfolder("Brak umowy");
        }
        else
        {
        	inserter = root().subfolder("Brak klienta");
        }
        if(!klasa.equals(UMOWA_KLASA_CN)){
            inserter.subfolder(fm.getValue(KLASA_FIELD_CN)+"")
                    .insert(document);
        } else {
            inserter.insert(document);
        }
    }
    
    private String getTitle(FieldsManager fm) throws EdmException
    {
    	return " - "+(fm.getValue("NR_UMOWY") != null ? fm.getValue("NR_UMOWY") : "");
    }
    @Override
    public void documentPermissions(Document document) throws EdmException 
    {
    	
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
	    perms.add(new PermissionBean(ObjectPermission.READ, "ESERVICE_READ", ObjectPermission.GROUP,"eService - odczyt"));
	    perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "ESERVICE_READ", ObjectPermission.GROUP,"eService - odczy"));
	    
	    perms.add(new PermissionBean(ObjectPermission.MODIFY, "ESERVICE_MODIFY", ObjectPermission.GROUP,"eService - zapis"));
	    perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "ESERVICE_MODIFY", ObjectPermission.GROUP,"eService - zapis"));
	    
	    perms.add(new PermissionBean(ObjectPermission.READ_ORGINAL_ATTACHMENTS, "ES_READ_ORG", ObjectPermission.GROUP,"eService - odczyt orgina�u"));
	    
		this.setUpPermission(document, perms);
	  }
    @Override
	public void doDockindEvent(DockindButtonAction eventActionSupport, ActionEvent event,Document document, String activity,Map<String, Object> values) throws EdmException {

		if(eventActionSupport.getDockindEventValue() == null)
			throw new EdmException("Nie wybrano trybu zam�wienia");
    	if (eventActionSupport.getDockindEventValue().startsWith("ORDER"))
		{
			orderDocument(eventActionSupport, eventActionSupport.getDockindEventValue(), event, values,document);
		}
		else
		{
			throw new EdmException("Nieznana operacja");
		}
	}

	private void orderDocument(DockindButtonAction eventActionSupport, String dockindEventValue, ActionEvent event, Map<String, Object> values, Document document) throws EdmException 
	{
		try
		{
			Map<String, Object> context = new HashMap<String, Object>();
	        context.put("documentTitle", document.getTitle());
	        context.put("box", document.getBox().getName());	        
	        context.put("orderer", DSApi.context().getDSUser().asFirstnameLastname());
	        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
	        fm.initialize();
	        for (String key : fm.getFieldsCns()) 
	        {
	        	context.put(key, fm.getValue(key));
			}
	        context.put("rodzajZam",  "ORDER_ST".equals(dockindEventValue)? "Standard" : "Ekspres");
	        String mailto = Docusafe.getAdditionProperty("eservice.order.email");
	        ((Mailer) ServiceManager.getService(Mailer.NAME)).
	            send(mailto, null, null, Configuration.getMail(Mail.ORDER_DOCUMENT), context);
	        Map<String, Object> fieldValues = new HashMap<String, Object>();
			fieldValues.put("ZAMAWIAJACY", DSApi.context().getDSUser().asFirstnameLastname());
			fieldValues.put("STATUS_ZAMOWIENIA", "ORDER_ST".equals(dockindEventValue)? 10 : 20);
			document.getDocumentKind().setOnly(document.getId(), fieldValues);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new EdmException("Nie uda�o si� wys�a� zam�wienia :"+e.getMessage());
		}
	}

//    @Override
//	public void doDockindEvent(DockindButtonAction eventActionSupport, ActionEvent event,Document document, String activity,Map<String, Object> values) throws EdmException {
//		System.out.println("JEDZIE :D");
//		System.out.println(eventActionSupport.getDockindEventValue());
//		if(eventActionSupport.getDockindEventValue() == null)
//			throw new EdmException("Nie wybrano trybu zam�wienia");
//    	if (eventActionSupport.getDockindEventValue().startsWith("ORDER"))
//		{
//			orderDocument(eventActionSupport, eventActionSupport.getDockindEventValue(), event, values,document);
//		}
//		else
//		{
//			throw new EdmException("Nieznana operacja");
//		}
//	}
//
//	private void orderDocument(DockindButtonAction eventActionSupport, String dockindEventValue, ActionEvent event, Map<String, Object> values, Document document) throws EdmException 
//	{
//		try
//		{
//			Map<String, Object> context = new HashMap<String, Object>();
//	        context.put("documentTitle", document.getTitle());
//	        context.put("box", document.getBox().getName());	        
//	        context.put("orderer", DSApi.context().getDSUser().asFirstnameLastname());
//	        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
//	        fm.initialize();
//	        for (String key : fm.getFieldsCns()) 
//	        {
//	        	System.out.println(key+" - "+fm.getValue(key));
//	        	context.put(key, fm.getValue(key));
//			}
//	        String mailto = Docusafe.getAdditionProperty("eservice.order.email");
//	        ((Mailer) ServiceManager.getService(Mailer.NAME)).
//	            send(mailto, null, "Zam�wienie dokumentu eService", Configuration.getMail(Mail.ORDER_DOCUMENT), context);
//		}
//		catch (Exception e) {
//			e.printStackTrace();
//			throw new EdmException("Nie uda�o si� wys�a� zam�wienia :"+e.getMessage());
//		}
//	}

}
