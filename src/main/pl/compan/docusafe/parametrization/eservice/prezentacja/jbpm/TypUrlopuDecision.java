package pl.compan.docusafe.parametrization.eservice.prezentacja.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class TypUrlopuDecision implements DecisionHandler {

    private static final long serialVersionUID = 1L;
    private Logger log = LoggerFactory.getLogger(TypUrlopuDecision.class);

    @Override
    public String decide(OpenExecution openExecution) {

        try {
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();

            if ((Integer) fm.getKey("URLOPRODZAJ") == 12) {
                return "UOK";
            }

            return "else";

        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            return "error";
        }


    }

}
