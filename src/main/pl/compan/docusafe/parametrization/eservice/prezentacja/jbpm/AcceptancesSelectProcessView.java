package pl.compan.docusafe.parametrization.eservice.prezentacja.jbpm;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.process.InstantiationParameters;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.jbpm4.ActivitiesExtensionProcessView;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessInstance;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.parametrization.eservice.prezentacja.FakturaVatLogic;
import pl.compan.docusafe.process.form.ActivityController;
import pl.compan.docusafe.process.form.SelectItem;
import pl.compan.docusafe.web.common.ExtendedRenderBean;

import java.util.ArrayList;
import java.util.List;

public class AcceptancesSelectProcessView extends ActivitiesExtensionProcessView {

    public AcceptancesSelectProcessView(InstantiationParameters ip) throws Exception {
        super(ip);
    }

    @Override
    public ExtendedRenderBean render(ProcessInstance pi, ProcessActionContext context) throws EdmException {
        return render((Jbpm4ProcessInstance) pi, context);
    }

    private ExtendedRenderBean render(Jbpm4ProcessInstance pi,
                                      ProcessActionContext context) throws EdmException {
        ExtendedRenderBean ret = super.render(pi, context);

        Long docId = Jbpm4ProcessLocator.documentIdForExtenedProcesssId(((Jbpm4ProcessInstance) pi).getProcessInstanceId());

        List<DocumentAcceptance> acceptances = DocumentAcceptance.find(docId);

        Field docProcesses = Document.find(docId).getDocumentKind().getFieldByCn("STATUS");

        String docStatus = Document.find(docId).getFieldsManager().getEnumItemCn("STATUS");

        List<SelectItem> selectItems = new ArrayList<SelectItem>();
        for (DocumentAcceptance itemAcceptance : acceptances) {
            itemAcceptance.initDescription();

            // wy�wietlenie na li�cie akceptacji tylko akceptacji etapu I Akceptacji merytorycznej 'manual_am' - na potrzeby konsultacji mi�dzy AM I i AM II
            if ("am2".equals(docStatus)) {
                if (itemAcceptance.getAcceptanceCn().equals(FakturaVatLogic.MANUAL_AM_ACCEPTANCE))
                    selectItems.add(ActivityController.createSelectItem(itemAcceptance.getId(), itemAcceptance.getAsFirstnameLastname(), itemAcceptance.getUsername(), docProcesses.getEnumItemByCn(itemAcceptance.getAcceptanceCn()).getTitle(), itemAcceptance.getAcceptanceCn()));
            } else {
                // nie pokazywanie na li�cie akceptacji dla takiego samego etapu, co aktualnie wykonywany
                if (!itemAcceptance.getAcceptanceCn().equals(docStatus))
                    selectItems.add(ActivityController.createSelectItem(itemAcceptance.getId(), itemAcceptance.getAsFirstnameLastname(), itemAcceptance.getUsername(), docProcesses.getEnumItemByCn(itemAcceptance.getAcceptanceCn()).getTitle(), itemAcceptance.getAcceptanceCn()));
            }
        }

        ret.putValue("selectItems", selectItems);

        return ret;

    }


}
