package pl.compan.docusafe.parametrization.eservice.prezentacja.jbpm;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PrepareForMpkSinglePath implements ExternalActivityBehaviour
{
	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		
		Long mpkId = (Long)execution.getVariable("mpkId");
		final List<Long> argSets = new LinkedList<Long>();
		if (mpkId != null) {
			argSets.add(mpkId);
		} else {
			argSets.add(new Long(0));
		}
		execution.setVariable("ids", argSets);
		execution.setVariable("count", argSets.size());

        //wykomentowane - po wykonaniu poprawy kontynuowana jest �cie�ka procesowania (nie musz� wszyscy ponownie akceptowa�)
        //execution.setVariable("singlePath", true);
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		
	}
}
