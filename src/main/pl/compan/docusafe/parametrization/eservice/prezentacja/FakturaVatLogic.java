package pl.compan.docusafe.parametrization.eservice.prezentacja;

import com.google.common.collect.Maps;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.*;

public class FakturaVatLogic extends AbstractDocumentLogic {

    private static final long serialVersionUID = 1L;
    protected Logger log = LoggerFactory.getLogger(FakturaVatLogic.class);

    public static final String MPK_VARIABLE_NAME = "costCenterId";
    public static final String MANUAL_AM_ACCEPTANCE = "manual_am";
    public static final String AM1_ACCEPTANCE = "am1";
    public static final String AM2_ACCEPTANCE = "am2";
    public static final String MPK_CN = "MPK";

    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {

        boolean assigned = false;
        try {
            assigned = false;
            FieldsManager fm = new FieldsManager(doc.getId(), doc.getDocumentKind());

            if (AM1_ACCEPTANCE.equals(acceptationCn)) {
                if (fm.getBoolean("NA_DZIAL_BOOL")) {
                    Integer divisionID = (Integer) fm.getKey("DZIAL");
                    if (divisionID != null) {
                        assignable.addCandidateGroup(DSDivision.findById(divisionID).getGuid());
                        assigned = true;
                        AssigneeHandler.addToHistory(openExecution, doc, null, DSDivision.findById(divisionID).getName());
                        return assigned;
                    } else {
                        assignable.addCandidateUser("admin");
                        assigned = true;
                        AssigneeHandler.addToHistory(openExecution, doc, "admin", null);
                        return assigned;
                    }
                } else {
                    Integer userID = (Integer) fm.getKey("OS_MERYTORYCZNA");
                    if (userID != null) {
                        assignable.addCandidateUser(DSUser.findById(Long.valueOf(userID)).getName());
                        assigned = true;
                        AssigneeHandler.addToHistory(openExecution, doc, DSUser.findById(Long.valueOf(userID)).getName(), null);
                        return assigned;
                    } else {
                        assignable.addCandidateUser("admin");
                        assigned = true;
                        AssigneeHandler.addToHistory(openExecution, doc, "admin", null);
                        return assigned;
                    }
                }
            }
            if (AM2_ACCEPTANCE.equals(acceptationCn)) {
                Long mpkId = (Long) openExecution.getVariable(MPK_VARIABLE_NAME);
                CentrumKosztowDlaFaktury mpkInstance = CentrumKosztowDlaFaktury.getInstance();
                CentrumKosztowDlaFaktury mpk = mpkInstance.find(mpkId);

                Long userID = Long.valueOf(mpk.getCentrumId());
                String supervisorUsername = DSUser.findById(userID).getSupervisor().getName();
                if (userID != null) {
                    assignable.addCandidateUser(supervisorUsername);
                    assigned = true;
                    AssigneeHandler.addToHistory(openExecution, doc, supervisorUsername, null);
                    return assigned;
                } else {
                    assignable.addCandidateUser("admin");
                    assigned = true;
                    AssigneeHandler.addToHistory(openExecution, doc, "admin", null);
                    return assigned;
                }
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }

        return assigned;
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {
        Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
        String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
        String documentKindName = document.getDocumentKind().getName();

        perms.add(new PermissionBean(ObjectPermission.READ, documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, documentKindName + " - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, documentKindName + " zalacznik - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, documentKindName + " - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, documentKindName + " zalacznik - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, documentKindName + " - usuwanie"));
        Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
        perms.addAll(documentPermissions);
        this.setUpPermission(document, perms);
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent("Faktura VAT");

        folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(document.getCtime()));

        document.setFolder(folder);
    }

//	@Override
//	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
//		Map<String, Object> toReload = Maps.newHashMap();
//		for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
//			if (f.getDefaultValue() != null)
//				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
//
//		toReload.put("DATA_WPLYWU_FAKTURY", new Date());
//		fm.reloadValues(toReload);
//	}

    @Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        try {

            StringBuilder msgBuilder = new StringBuilder();


            if (msgBuilder.length() > 0) {
                values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
                return new Field("messageField", msgBuilder.toString(), Field.Type.BOOLEAN);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues, Long documentId) {
        Map<String, Object> result = new HashMap<String, Object>();
        if (dictionaryName.equals(MPK_CN)) {
            try {
                if (DwrUtils.isNotNull(values, MPK_CN + "_CONNECTED_TYPE_ID")) {
                    Long loggedUserID = DSApi.context().getDSUser().getId();
                    if (loggedUserID != null) {
                        List<String> selected = new LinkedList<String>();
                        selected.add(loggedUserID.toString());
                        DataBaseEnumField d = DataBaseEnumField.getEnumFiledForTable("dsg_mpk_osoby");
                        EnumValues val = d.getEnumItemsForDwr(fieldsValues);
                        val.setSelectedOptions(selected);
                        result.put(MPK_CN + "_CENTRUMID", val);
                    }
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return result;
    }

    @Override
    public void setAdditionalValues(FieldsManager fm, Integer valueOf, String activity) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();
        for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields()) {
            if (fm.getKey(f.getCn()) == null && f.getDefaultValue() != null) {
                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
            }
        }
        if ((Integer) fm.getKey("STATUS") == 99) {
            toReload.put("OS_MERYTORYCZNA",DSApi.context().getDSUser().getId());
        }
        fm.reloadValues(toReload);
    }

}
