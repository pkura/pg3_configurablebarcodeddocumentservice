package pl.compan.docusafe.parametrization.zara;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;

import java.math.BigDecimal;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class MoneyDecisionHandler implements DecisionHandler{
    public String decide(OpenExecution openExecution) {
        Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        try {
            BigDecimal bd = (BigDecimal) Documents.document(docId).getFields().get("KWOTA_BRUTTO");
            if(bd.compareTo(new BigDecimal("5000")) > 0){
                return "to-chief";
            } else {
                return "pass";
            }
        } catch (Exception ex) {
            return "error";
        }
    }
}
