package pl.compan.docusafe.parametrization.zara;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ic.InvoiceICLogic;

import static pl.compan.docusafe.util.FolderInserter.root;
import static pl.compan.docusafe.util.FolderInserter.to3Letters;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ZaraInvoiceLogic  extends AbstractDocumentLogic {
    private final static Logger LOG = LoggerFactory.getLogger(ZaraInvoiceLogic.class);

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        DicInvoice dostawca = (DicInvoice) fm.getValue(InvoiceICLogic.DOSTAWCA_FIELD_CN);
        root().subfolder("�rodki trwa�e")
                .subfolder(to3Letters(dostawca.getName()))
                .subfolder(dostawca.getName())
                .insert(document);
//        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
    }

    @Override
    public void onStartProcess(OfficeDocument document) throws EdmException {
        try {
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }

    public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

		perms.add(new PermissionBean(ObjectPermission.READ, DocumentLogicLoader.ABSENCE_REQUEST+"_READ",ObjectPermission.GROUP,"Dokumenty - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,DocumentLogicLoader.ABSENCE_REQUEST+"_ATT_READ",ObjectPermission.GROUP,"Dokumenty zalacznik - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY,DocumentLogicLoader.ABSENCE_REQUEST+"_MODIFY",ObjectPermission.GROUP,"Dokumenty - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,DocumentLogicLoader.ABSENCE_REQUEST+"_ATT_MODIFY",ObjectPermission.GROUP,"Dokumenty zalacznik - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.DELETE,DocumentLogicLoader.ABSENCE_REQUEST+"_DELETE",ObjectPermission.GROUP,"Dokumenty - usuwanie"));
   	 	this.setUpPermission(document, perms);
	}

}
