package pl.compan.docusafe.parametrization.infosys;

import org.apache.commons.dbutils.DbUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * User: liptomek
 * Date: 20.05.13                                     `
 * Time: 09:33
 */
public class AddworkReport extends Report {

    private final static Logger LOG = LoggerFactory.getLogger(AddworkReport.class);
    UniversalTableDumper dumper;

    private final static String REPOR_SQL = "" +
            "select d.assignment_code, d.assignment_name, d.delivery_center_i, d.department_i, d.infosys_code, u.FIRSTNAME + ' ' + u.LASTNAME, 'INVTO_ORU', " +
            "d.requestors_country_i, dic.oru, d.cust_deptcode, " +
            "(select top 1 u.FIRSTNAME + ' ' + u.LASTNAME from dso_document_asgn_history h join ds_user u on u.NAME = h.sourceuser where h.document_id = doc.id and h.sourceguid = 'AKCEPTACJA1' and h.targetguid = 'AKCEPTACJA2'), " +
            "(select top 1 u.FIRSTNAME + ' ' + u.LASTNAME from dso_document_asgn_history h join ds_user u on u.NAME = h.sourceuser where h.document_id = doc.id and h.sourceguid = 'AKCEPTACJA2' and h.targetguid = 'x'), " +
            "d.service_month_i, d.billing_month_i, dic.labor_number, dic.resource_unit_code, d.billing_currency, " +
            "d.price_bilcur, d.price_eur, " +
            "ISNULL(dic.resource_unit_code,0) * ISNULL(d.price_bilcur,0), " +
            "ISNULL(dic.resource_unit_code,0) * ISNULL(d.price_eur,0), " +
            "d.billing_region_i, d.service_type_i, d.billing_start_month, d.service_start_month, d.service_end_month, " +
            "doc.lastRemark, d.finops_region_i " +
            "from dsg_infosys d " +
            "join dsg_infosys_multiple_value m on m.DOCUMENT_ID = d.document_id " +
            "join ds_document doc on doc.id = d.document_id " +
            "join ds_user u on u.NAME = doc.AUTHOR " +
            "join dsg_infosys_ibc dic on dic.id = m.FIELD_VAL where m.FIELD_CN = 'IN_BILLING_CURRENCY'";


    public void initParametersAvailableValues() throws ReportException {
        LoggerFactory.getLogger("tomekl").debug("AddworkReport -> initParametersAvailableValues()");
    }

    @Override
    public void doReport() throws Exception {
        LoggerFactory.getLogger("tomekl").debug("AddworkReport -> doReport()");

        dumper = new UniversalTableDumper();
        XlsPoiDumper poiDumper = new XlsPoiDumper();
        File xlsFile = new File(this.getDestination(), "raport.xls");
        poiDumper.openFile(xlsFile);
        dumper.addDumper(poiDumper);

        dumpHeader();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DSApi.context().prepareStatement(REPOR_SQL);
            rs = ps.executeQuery();
            while(rs.next()) {
                dumper.newLine();

                dumper.addText(rs.getString(1));
                dumper.addText(rs.getString(2));
                try { dumper.addText(DocumentKind.findByCn("infosys").getFieldByCn("DELIVERY_CENTER").getEnumItem(rs.getInt(3)).getTitle()); } catch (Exception e) { dumper.addText(""); }
                try { dumper.addText(DocumentKind.findByCn("infosys").getFieldByCn("DEPARTMENT").getEnumItem(rs.getInt(4)).getTitle()); } catch (Exception e) { dumper.addText(""); }
                dumper.addText(rs.getString(5));
                dumper.addText(rs.getString(6));
                dumper.addText(rs.getString(7));
                try { dumper.addText(DocumentKind.findByCn("infosys").getFieldByCn("REQUESTORS_COUNTRY").getEnumItem(rs.getInt(8)).getTitle()); } catch (Exception e) { dumper.addText(""); }
                dumper.addText(rs.getString(9));
                dumper.addText(rs.getString(10));
                dumper.addText(rs.getString(11));
                dumper.addText(rs.getString(12));
                try { dumper.addText(DocumentKind.findByCn("infosys").getFieldByCn("SERVICE_MONTH").getEnumItem(rs.getInt(13)).getTitle()); } catch (Exception e) { dumper.addText(""); }
                try { dumper.addText(DocumentKind.findByCn("infosys").getFieldByCn("BILLING_MONTH").getEnumItem(rs.getInt(14)).getTitle()); } catch (Exception e) { dumper.addText(""); }
                dumper.addText(rs.getString(15));
                dumper.addText(rs.getString(16));
                try { dumper.addText(DocumentKind.findByCn("infosys").getFieldByCn("BILLING_CURRENCY").getEnumItem(rs.getInt(17)).getTitle()); } catch (Exception e) { dumper.addText(""); }
                try { dumper.addInteger(rs.getInt(18)); } catch (Exception e) { dumper.addText(""); }
                try { dumper.addInteger(rs.getInt(19)); } catch (Exception e) { dumper.addText(""); }
                try { dumper.addInteger(rs.getInt(20)); } catch (Exception e) { dumper.addText(""); }
                try { dumper.addInteger(rs.getInt(21)); } catch (Exception e) { dumper.addText(""); }
                try { dumper.addText(DocumentKind.findByCn("infosys").getFieldByCn("BILLING_REGION").getEnumItem(rs.getInt(22)).getTitle()); } catch (Exception e) { dumper.addText(""); }
                try { dumper.addText(DocumentKind.findByCn("infosys").getFieldByCn("SERVICE_TYPE").getEnumItem(rs.getInt(23)).getTitle()); } catch (Exception e) { dumper.addText(""); }
                try { dumper.addText(DocumentKind.findByCn("infosys").getFieldByCn("BILLING_START_MONTH").getEnumItem(rs.getInt(24)).getTitle()); } catch (Exception e) { dumper.addText(""); }
                try { dumper.addText(DocumentKind.findByCn("infosys").getFieldByCn("SERVICE_START_MONTH").getEnumItem(rs.getInt(25)).getTitle()); } catch (Exception e) { dumper.addText(""); }
                try { dumper.addText(DocumentKind.findByCn("infosys").getFieldByCn("SERVICE_END_MONTH").getEnumItem(rs.getInt(26)).getTitle()); } catch (Exception e) { dumper.addText(""); }
                dumper.addText(rs.getString(27));
                try { dumper.addText(DocumentKind.findByCn("infosys").getFieldByCn("FINOPS_REGION").getEnumItem(rs.getInt(28)).getTitle()); } catch (Exception e) { dumper.addText(""); }

                dumper.dumpLine();
            }
        }
        catch (Exception e) {
            LOG.debug(e.getMessage(),e);
            e.printStackTrace();
        }
        finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(ps);
        }

        dumper.closeFileQuietly();
    }

    private void dumpHeader() throws IOException {
        dumper.newLine();
        dumper.addText("ASSIGNEMNT_CODE");    //
        dumper.addText("ASSIGNMENT_NAME");    //
        dumper.addText("INFOSYS_CENTER");     //
        dumper.addText("INFOSYS_DEPARTMENT"); //
        dumper.addText("INFOSYS_CODE");       //
        dumper.addText("INFOSYS_TOUCHPOINT"); //
        dumper.addText("INVTO_ORU");          //
        dumper.addText("CUSTOMER_COUNTRY");   //
        dumper.addText("CUST_ORU");           //
        dumper.addText("CUST_DEPTCODE");      //
        dumper.addText("SPONSOR");            //
        dumper.addText("SBSF_RESPONSIBLE");   //
        dumper.addText("SERVICE_MONTH");
        dumper.addText("BILLING_MONTH");
        dumper.addText("VOLUME ");
        dumper.addText("RU");
        dumper.addText("BILCUR");
        dumper.addText("PRICE_BILCUR");
        dumper.addText("PRICE_EUR");
        dumper.addText("VALUE_BILCUR");
        dumper.addText("VALUE_EUR");
        dumper.addText("BILLING_METHOD");
        dumper.addText("SERVICE_TYPE");
        dumper.addText("BILLING_START_MONTH");
        dumper.addText("SERVICE_START_MONTH");
        dumper.addText("SERVICE_END_MONTH");
        dumper.addText("COMMENT");
        dumper.addText("REGION");
        dumper.dumpLine();
    }
}
