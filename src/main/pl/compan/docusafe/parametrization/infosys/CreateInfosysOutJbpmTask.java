package pl.compan.docusafe.parametrization.infosys;

import com.google.common.collect.Maps;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Date;
import java.util.Map;

/**
 * User: liptomek
 * Date: 16.04.13
 * Time: 16:36
 */
public class CreateInfosysOutJbpmTask implements ExternalActivityBehaviour {
    private static final Logger LOG = LoggerFactory.getLogger(CreateInfosysOutJbpmTask.class);

    @Override
    public void execute(ActivityExecution execution) throws Exception {
        //TODO: create document +
        //TODO: fill new document from old +
        //TODO: fill new document from history - (noneed)

        Document orgDoc = Jbpm4Utils.getDocument(execution);
        FieldsManager orgFm = orgDoc.getDocumentKind().getFieldsManager(orgDoc.getId());

        DSUser acceptanceUser = DSApi.context().getDSUser();
        Date acceptanceDate = new Date();

        Map<String,Object> values = Maps.newHashMap();
        values.put(InfosysLogic.ASSIGNMENT_CODE_CN,orgFm.getKey(InfosysLogic.ASSIGNMENT_CODE_CN));
        values.put(InfosysLogic.ASSIGNMENT_NAME_CN,orgFm.getKey(InfosysLogic.ASSIGNMENT_NAME_CN));

        values.put(InfosysLogic.REQUEST_APPROVAL_CN,Boolean.TRUE);
        values.put(InfosysLogic.APPROVED_BY_CN,acceptanceUser.asLastnameFirstname());
        values.put(InfosysLogic.DATE_APPROVED_CN,acceptanceDate);

        Document newDoc = createOutDocument(acceptanceUser, values);
    }

    private static Document createOutDocument(DSUser acceptanceUser, Map<String,Object> values) throws EdmException {
        OutOfficeDocument crtDoc = new OutOfficeDocument();

        crtDoc.setDocumentKind(DocumentKind.findByCn(InfosysLogic.INFOSYS_OUT_DOCKIND_CN));

        crtDoc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
        crtDoc.setDivisionGuid(DSDivision.ROOT_GUID);
        crtDoc.setCurrentAssignmentAccepted(Boolean.FALSE);
        crtDoc.setSummary("");
        crtDoc.setCreatingUser(acceptanceUser.getName());

        crtDoc.create();

        crtDoc.getDocumentKind().set(crtDoc.getId(), values);

        crtDoc.setDocumentDate(new Date());
        crtDoc.getDocumentKind().logic().archiveActions(crtDoc, 0);
        crtDoc.getDocumentKind().logic().documentPermissions(crtDoc);
        DSApi.context().session().save(crtDoc);

        DSApi.context().session().flush();

        Map<String, Object> map = Maps.newHashMap();
        crtDoc.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(crtDoc, map);

        TaskSnapshot.updateByDocument(crtDoc);

        return crtDoc;
    }

    @Override
    public void signal(ActivityExecution execution, String s, Map<String, ?> stringMap) throws Exception {

    }
}
