package pl.compan.docusafe.parametrization.infosys;

import com.google.common.collect.Maps;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;

import java.util.Calendar;
import java.util.Map;

/**
 * User: liptomek
 * Date: 16.04.13
 * Time: 15:58
 */
public class InfosysLogic extends AbstractDocumentLogic {

    //public final static String INFOSYS_DOCKIND_CN = "infosys";
    public final static String INFOSYS_OUT_DOCKIND_CN = "infosys_out";
    public final static String ASSIGNMENT_CODE_CN = "ASSIGNMENT_CODE";
    public final static String ASSIGNMENT_NAME_CN = "ASSIGNMENT_NAME";
    public final static String BILLING_CURRENCY_CN = "BILLING_CURRENCY";
    public final static String REQUEST_APPROVAL_CN = "REQUEST_APPROVAL";
    public final static String APPROVED_BY_CN = "APPROVED_BY";
    public final static String DATE_APPROVED_CN = "DATE_APPROVED";
    public final static String BILLING_START_YEAR_CN = "BILLING_START_YEAR";
    public final static String SERVICE_START_YEAR_CN = "SERVICE_START_YEAR";
    public final static String SERVICE_END_YEAR_CN = "SERVICE_END_YEAR";

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
        document.setFolder(folder);

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        document.setTitle(String.valueOf(fm.getValue(fm.getMainFieldCn())));
        document.setDescription(String.valueOf(fm.getValue(fm.getMainFieldCn())));
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {

    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String,Object> values = Maps.newHashMap();

        int y = Calendar.getInstance().get(Calendar.YEAR);
        values.put(BILLING_START_YEAR_CN,y);
        values.put(SERVICE_START_YEAR_CN,y);
        values.put(SERVICE_END_YEAR_CN,y);
        values.put(BILLING_CURRENCY_CN,1);

        fm.reloadValues(values);
    }
}
