package pl.compan.docusafe.parametrization.bialystok;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

/**
 * logika dla dokumentu typu "protokol brakowania / oceny dokumentacji niearchiwalnej"
 * @author bkaliszewski
 *
 */
public class ProtokolDocLogic extends AbstractDocumentLogic
{

	private static ProtokolDocLogic instance;
	protected static Logger log= LoggerFactory.getLogger(ProtokolDocLogic.class);

	public static ProtokolDocLogic getInstance()
	{
		if (instance == null)
			instance= new ProtokolDocLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms= new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP,
				"Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ",
				ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		// perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP,
		// "Dokumenty zwyk造 - modyfikacja"));
		// perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY",
		// ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP,
				"Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms) {
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP)) {
				String groupName= perm.getGroupName();
				if (groupName == null) {
					groupName= perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		log.info("type: {}", type);
		Map<String, Object> values= new HashMap<String, Object>();
		values.put("TYPE", type);
		DSUser user= DSApi.context().getDSUser();

		fm.reloadValues(values);
	}


	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		as.useFolderResolver(document);
		as.useAvailableProviders(document);
		log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
	}

	@Override
	public void onStartProcess(OfficeDocument document)
	{
		try {
			Map<String, Object> map= Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, document.getAuthor());
			map.put(ASSIGN_DIVISION_GUID_PARAM, document.getDivisionGuid());
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret= new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	@Override
	public void onRejectedListener(Document doc) throws EdmException
	{
	}

	@Override
	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException
	{
	}
	
	@Override
	public void onSaveActions(DocumentKind kind, Long documentId,
			Map<String, ?> fieldValues) throws SQLException, EdmException {
	}
}
