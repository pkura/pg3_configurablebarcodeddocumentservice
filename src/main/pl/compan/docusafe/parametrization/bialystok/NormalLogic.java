package pl.compan.docusafe.parametrization.bialystok;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.util.HashMap;
import java.util.Map;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;


/**
 *
 */
public class NormalLogic extends AbstractDocumentLogic {

    private static NormalLogic instance;
    protected static Logger log = LoggerFactory.getLogger(NormalLogic.class);
    public final static String BARCODE = "BARCODE";
    public static NormalLogic getInstance() {
        if (instance == null)
            instance = new NormalLogic();
        return instance;
    }

    public void documentPermissions(Document document) throws EdmException {
        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
        perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

        for (PermissionBean perm : perms) {
            if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP)) {
                String groupName = perm.getGroupName();
                if (groupName == null) {
                    groupName = perm.getSubject();
                }
                createOrFind(document, groupName, perm.getSubject());
            }
            DSApi.context().grant(document, perm);
            DSApi.context().session().flush();
        }
    }
	
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
		 log.info("type: {}", type);
		 Map<String,Object> values = new HashMap<String,Object>();
	     values.put("TYPE", type);
	     DSUser user = DSApi.context().getDSUser();
	     
	     
	     
	   

	     fm.reloadValues(values);
	}
	
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        as.useFolderResolver(document);
        as.useAvailableProviders(document);
        log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
    }

    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) {
        log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
        try {
            Map<String, Object> map = Maps.newHashMap();
            if (document instanceof InOfficeDocument)
            {
            	 map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
            }
            else{
            	map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
            	map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
            }
           
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
            
            String barcode = document.getFieldsManager().getStringValue(BARCODE);
           
			if(document instanceof InOfficeDocument){
				InOfficeDocument in = (InOfficeDocument) document;
			if (barcode != null) {
				in.setBarcode(barcode);
			} else {
				in.setBarcode("Brak");
			}
			} else if (document instanceof OutOfficeDocument) {
				OutOfficeDocument od = (OutOfficeDocument) document;
				if (barcode != null) {
					od.setBarcode(barcode);
				} else {
					od.setBarcode("Brak");
				}
			}
            if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
			

				
			
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    @Override
    public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException {
        ProcessCoordinator ret = new ProcessCoordinator();
        ret.setUsername(document.getAuthor());
        return ret;
    }
/*
    private final static OcrSupport OCR_SUPPORT = OcrUtils.getFileOcrSupport(
                new File(Docusafe.getHome(), "ocr-dest"),
                new File(Docusafe.getHome(), "ocr-get"),
                Collections.singleton("tiff")
    );

    @Override
    public OcrSupport getOcrSupport() {
        return OCR_SUPPORT;
    }
    */

	@Override
	public void onRejectedListener(Document doc) throws EdmException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException
	{
		// TODO Auto-generated method stub
		
	}
	
	public TaskListParams getTaskListParams(DocumentKind dockind, long documentId) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			InOfficeDocument doc = InOfficeDocument.findInOfficeDocument(documentId);
			if(doc.getDelivery()!=null)
				params.setCategory(doc.getDelivery().getName());
		}
		catch (Exception e)
		{
			log.error("",e);
		}
		return params;
	}
}
