package pl.compan.docusafe.parametrization.bialystok;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.Field.Type;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.permissions.PermissionCache;
import pl.compan.docusafe.util.DateUtils;

/* Tworzony dokument (Wykaz zdawczo-odbiorczy) ma cn="wykaz"
 Protokol brakowania ma cn="protokol_br" 
 */

/**
 * raz dziennie sprawdza, czy sa sprawy do przeniesienia do archiwum. Taka sprawa jest przenoszona, gdy mina 2 lata od
 * momentu zamkniecia sprawy, ale liczone od stycznia roku nastepnego
 * 
 * @author bkaliszewski
 * 
 */
public class CaseArchiverScheduled extends ServiceDriver
{
	private static final String[] TABELE_DOKUMENTU= { "dso_in_document", "DSO_OUT_DOCUMENT", "dso_order" };

	private static final boolean __debug= false;
	
	private Timer archiveTimer;
	private Timer protokolTimer;

	
	
	public class WykazModel
	{
		int		wykaz_number;
		Date	archive_date;
		List<WykazCaseModel>	cases= new ArrayList();
		
		public WykazModel(Date archive_date)
		{
			this.archive_date=archive_date;
		}
	}
	
	public class ProtokolModel
	{
		int		protokol_number;
		List<ProtokolCaseModel>	cases= new ArrayList();
		
		private int casesOrdinal= 0;
		
		/**
		 * nastepna liczba porzadkowa (lp), zwiazana z liczba spraw
		 */
		public int getNextCasesLp()
		{
			++casesOrdinal;
			return casesOrdinal;
		}
	}
	
	/**
	 * 
	 * @param finishDate
	 * @throws EdmException
	 */
	private void generujWykazy(Date finishDate) throws EdmException
	{
		Date today= Calendar.getInstance().getTime();
		
		try {
			DSApi.openAdmin();
			DSApi.context().begin();

			Set<String> permittedUsers= getUsersForPermission(DSPermission.ADMINISTRACJA_WYKAZ_DOC.getName());

			List cases= getCasesByFinishDate(finishDate);

			// dla pustej listy nic nie robimy
			if (cases.isEmpty())
				return;
			
			Map<String,WykazModel> wykazyByDivisionGuid = new HashMap();
			
			if( __debug) System.out.println("[Wykaz] Do zarchiwizowania "+cases.size()+" spraw");
			
			for (Object o : cases) {
				OfficeCase casee= (OfficeCase) o;
				
				// pomijamy sprawy, ktore juz sa w jakims wykazie zdawczo-odbiorczym / maja protokol / sa wybrakowane
				if( casee.getArchiveStatus()!=null && !casee.getArchiveStatus().equals(OfficeCase.ArchiveStatus.None)  )
					continue;
				
				// stworz wykaz lub wez stworzony
				String divisionGuid= casee.getDivisionGuid();
				
				WykazModel wykaz= wykazyByDivisionGuid.get(divisionGuid);

				if (wykaz == null) {
					wykaz= new WykazModel(today);
					wykazyByDivisionGuid.put(divisionGuid, wykaz);
				}
				
				wykaz.cases.add(new WykazCaseModel(casee, wykaz.wykaz_number));
			}

			if( __debug) System.out.println("[Wykaz] Ukonczono przegladanie spraw");
			/*
			 * Do u�ytkownika odpowiedzialnego w danej kom�rce organizacyjnej za przekazywanie dokumentacji do archiwum
			 * zak�adowego na list� zada� trafia wiadomo�� zawieraj�ca informacj� o przekazaniu dokumentacji do archiwum
			 * zak�adowego z dat� ich przekazania oraz wykazem przekazanych dokument�w. wiadomo�� z tytu�em spis
			 * zdawczo-odbiorczy akt Nr( tutaj numer spisu zdawczo odbiorczego nadawany automatycznie przez system)
			 */

			Set<String> divisions = wykazyByDivisionGuid.keySet();
			if( __debug) System.out.println("[Wykaz] Do wygenerowania "+divisions.size()+" wykazow");
			
			int wykazNextNumber = getMaxWykazNumber()+1;
			
			for( String divisionGuid : divisions )
			{
				WykazModel wykaz= wykazyByDivisionGuid.get(divisionGuid);
				
				int ilosc_spraw = wykaz.cases.size();
				/**
				 * 
				 */
				if( __debug && ilosc_spraw>300){
					 System.out.println("[Wykaz] W trybie debug pomija wykaz z iloscia spraw: "+ilosc_spraw);
					continue;
				}
				/**
				 * 
				 */
				if( __debug) System.out.println("[Wykaz] Generuje wykaz zawierajacy "+ilosc_spraw+" spraw");
				
				DSUser odbiorca= findOsobaUprawniona(DSDivision.find(divisionGuid), permittedUsers );

				if( odbiorca==null )
				{
					if( __debug) System.out.println("[Wykaz] Brak odbiorcy; dla guid "+divisionGuid);
					/**///continue;
					odbiorca= DSUser.findByUsername("admin");
					/**/
				}
				
				int case_lp = 1;
				// archiwizuj sprawy
				for( WykazCaseModel wcase : wykaz.cases )
				{
					if( pobierzDatyWykazCase( wcase.tmp_case, wcase ) )
					{
						wcase.ignore= false;

						if( __debug) System.out.println("[Wykaz] " + case_lp + "\t / " + ilosc_spraw + "\t archiwizuje sprawe "	+ wcase.tmp_case.getOfficeId());

						archiveCase(wcase.tmp_case, today);

						wcase.lp= case_lp;

						++case_lp;
					}else
						if( __debug) System.out.println("[Wykaz] pobierzDatyWykazCase returned false");

					//wcase.tmp_case.setArchived(true);
					wcase.tmp_case.setArchivedDate(today);
					wcase.tmp_case.setArchiveStatus(OfficeCase.ArchiveStatus.ToArchive /* zarchiwizowano */);
					DSApi.context().session().update(wcase.tmp_case);
				}

				DSApi.context().commit();
				DSApi.close();
				DSApi.openAdmin();
				DSApi.context().begin();
				// stworz wiadomosc dla wykazu
				if( __debug) System.out.println("[Wykaz] wiadomosc bedzie wyslana do " + odbiorca.getName());

				wykaz.wykaz_number= wykazNextNumber;
				++wykazNextNumber;
				createWykazDoc(odbiorca.getName(), wykaz, divisionGuid);
			}
			DSApi.context().commit();

		} catch (HibernateException e) {
			DSApi.context().rollback();
			throw new EdmHibernateException(e);
		} catch (Exception e) {
			DSApi.context().rollback();
			throw new EdmException(e);
		} finally {
			DSApi.close();
			if( __debug) System.out.println("[Wykaz] koniec" );
		}
	}

	private void archiveCase(OfficeCase casee, Date today) throws SQLException, EdmException
	{
		// ustawia flage Archived

		Long caseId= casee.getId();
		String[] tabele= TABELE_DOKUMENTU;

		// doc.setArchived(true);
		for (String tabelaKanc : tabele) {
			String query_data= "update " + tabelaKanc + " set archived=1 where "
				+"id in (select M.DOCUMENT_ID from ds_wykaz_multiple M join DS_WYKAZ_CASES C on C.id=M.FIELD_VAL where C.CASE_ID=" + caseId+")";
			PreparedStatement ps_data= DSApi.context().prepareStatement(query_data);
			ps_data.executeUpdate();
			ps_data.close();
		}
	}

	/**
	 * tworzy wiadomosc na liscie zadan, poprzez utworzenie dokumentu wewnetrznego
	 * 
	 */
	private void createWykazDoc(String osoba, WykazModel wykaz, String divisionGuid ) throws Exception
	{
		String opis= "Sprawy zosta�y przeniesione. Wygenerowano spis zdawczo-odbiorczy nr "
				+ ((Integer) wykaz.wykaz_number).toString();
		
		Map<String, Map<String, FieldData>> casesMap= new HashMap();
		
		Integer case_index= 1;
		for( WykazCaseModel _case : wykaz.cases)
		{
			if( _case.ignore )
				continue;
			
			Map<String, FieldData> fields = new HashMap();
			casesMap.put("WYKAZ_CASES_"+case_index, fields);
			fields.put("ZNAK", new FieldData( Type.STRING, _case.znak));
			fields.put("TYTUL", new FieldData( Type.STRING, _case.tytul));
			fields.put("LP", new FieldData( Type.INTEGER, _case.lp));
			fields.put("DATA_OD", new FieldData( Type.STRING, _case.data_od));
			fields.put("DATA_DO", new FieldData( Type.STRING, _case.data_do));
			fields.put("KAT_AKT", new FieldData( Type.STRING, _case.kat_akt));
			fields.put("ID", new FieldData( Type.STRING, ""));
			fields.put("CASE_ID", new FieldData( Type.INTEGER, _case.case_id));
			fields.put("C_WYKAZ_NUMBER", new FieldData( Type.INTEGER, wykaz.wykaz_number));
			
			++case_index;
		}
		
		// nie zostala dodana zadna sprawa
		if( case_index== 1)
		{
			if( __debug )System.out.println("[Wykaz] Brak spraw, nie tworzy wykazu");
			return;
		}
		
		
		OutOfficeDocument doc= new OutOfficeDocument();

		doc.setCurrentAssignmentGuid(divisionGuid);
		doc.setDivisionGuid(divisionGuid);
		doc.setCurrentAssignmentAccepted(Boolean.FALSE);
		doc.setCreatingUser(osoba);
		doc.setInternal(true);
		doc.setSummary(opis);
		doc.setForceArchivePermissions(false);
		doc.setAssignedDivision(divisionGuid);
		doc.setClerk(osoba);
		doc.setSender(new Sender());
		doc.setSource("");
		
		doc.create();

		DocumentKind documentKind= DocumentKind.findByCn(DocumentLogicLoader.WYKAZ_ZDAWCZO_ODBIORCZY);
		Map<String, Object> dockindKeys= new HashMap();
		dockindKeys.put("DOC_DESCRIPTION", opis);
		dockindKeys.put("DOC_DATE", wykaz.archive_date);
		dockindKeys.put("WYKAZ_NUMBER", wykaz.wykaz_number);
		dockindKeys.put("M_DICT_VALUES", casesMap);

		doc.setDocumentKind(documentKind);

//		doc.create();

		Long newDocumentId= doc.getId();
		
		if( DwrDictionaryFacade.dictionarieObjects.get("WYKAZ_CASES")==null)
			DwrDictionaryFacade.dictionarieObjects.put("WYKAZ_CASES",new DwrDictionaryBase(DocumentKind.findByCn(DocumentLogicLoader.WYKAZ_ZDAWCZO_ODBIORCZY).getFieldByCn("WYKAZ_CASES"),null ) );
		
		documentKind.saveMultiDictionaryValues(doc, dockindKeys );
		
		documentKind.set(newDocumentId, dockindKeys);
		
		doc.getDocumentKind().logic().archiveActions(doc, 0);
		doc.getDocumentKind().logic().documentPermissions(doc);
		doc.setAuthor(osoba);
		DSApi.context().session().save(doc);

		//doc.getDocumentKind().logic().onStartProcess(doc);

		WorkflowFactory.createNewProcess(doc, false, "Wykaz zdawczo-odbiorczy nr "+((Integer) wykaz.wykaz_number).toString());
		TaskSnapshot.updateByDocument(doc);
		if( __debug) System.out.println(opis);
	}
	
	private void generujProtokolyBrakowania() throws EdmException
	{
		Date today= Calendar.getInstance().getTime();
		
		try {
			DSApi.openAdmin();
			DSApi.context().begin();
			
			Set<String> permittedUsers= getUsersForPermission(DSPermission.ADMINISTRACJA_WYKAZ_DOC.getName());
			
			List cases= getCasesDoBrakowania();
			if( __debug) System.out.println("[Protokol] cases= "+cases.isEmpty());

			// dla pustej listy nic nie robimy
			if (cases.isEmpty())
				return;
			
			Map<String,ProtokolModel> protokolByDivisionGuid = new HashMap();
			
			for (Object o : cases) {
				OfficeCase casee= (OfficeCase) o;
				
				// pomijamy sprawy, ktore sa wybrakowane
				if( casee.getArchiveStatus()!=null && !casee.getArchiveStatus().equals(OfficeCase.ArchiveStatus.Archived)  )
					continue;
				
				/*
				 * warunek przyjecia do protokolu:
				 * Kategoria archiwalna: B + liczba
				 */
				if( !casee.getRwa().getAcHome().matches("B[^A-Za-z0-9]?[0-9]+") )
					continue;
				
				
				// OK, dodajemy sprawe do protokolu

				// stworz protokol lub wez stworzony
				String divisionGuid= casee.getDivisionGuid();
				
				ProtokolModel protokol= protokolByDivisionGuid.get(divisionGuid);

				if (protokol == null) {
					protokol= new ProtokolModel();
					protokolByDivisionGuid.put(divisionGuid, protokol);
				}
				
				// dodanie sprawy do protokolu
				protokol.cases.add(new ProtokolCaseModel(casee /*, case_docs, protokol.getNextCasesLp(), number,lp*/));
			}
			
			
			if( __debug) System.out.println("[Protokol] Do wygenerowania "+protokolByDivisionGuid.keySet().size()+" protokolow");
			
			int protokolNextNumber = getMaxProtokolNumber()+1;

			for( String divisionGuid : protokolByDivisionGuid.keySet() )
			{
				ProtokolModel protokol= protokolByDivisionGuid.get(divisionGuid);
				
				// stworz wiadomosc dla wykazu
				//DSDivision dzial= DivisionBoss.findManager());
				DSUser odbiorca= findOsobaUprawniona(DSDivision.find(divisionGuid), permittedUsers);

				if( odbiorca==null )
				{
					if( __debug) System.out.println("[Protokol] Brak odbiorcy; dla guid "+divisionGuid);
					
					odbiorca= DSUser.findByUsername("admin");
				}
				int case_lp = 1;
				// archiwizuj sprawy
				for( ProtokolCaseModel pcase : protokol.cases )
				{
					pcase.ignore= false;
					
					if(! pobierzDatyWykazCase( pcase.tmp_case, pcase ) )
						pcase.ignore= true;
					
					if( !pobierzParametryWykazCase( pcase.tmp_case, pcase ) )
						pcase.ignore= true;
					
					pcase.lp= case_lp;
					if( __debug) System.out.println("[Protokol] "+case_lp+" ustawia date utworzenia protokolu, zmienia status na 2. " + pcase.tmp_case.getOfficeId()+",  ignore="+pcase.ignore  );
					
					// zmien status sprawy
					pcase.tmp_case.setArchiveStatus(OfficeCase.ArchiveStatus.ToShred /*wygenerowano protokol*/);
					pcase.tmp_case.setProtokolDate(today);
					DSApi.context().session().update(pcase.tmp_case);
					
					if( !pcase.ignore )
						++case_lp;
				}
				
				DSApi.context().commit();
				DSApi.close();
				DSApi.openAdmin();
				DSApi.context().begin();
				
				if( __debug) System.out.println("[Protokol] protokol bedzie wyslany do " + odbiorca.getName() );
				protokol.protokol_number= protokolNextNumber;
				++protokolNextNumber;
				createProtokolBrakowania(odbiorca.getName(), protokol, divisionGuid);
			}
			DSApi.context().commit();
			

		} catch (HibernateException e) {
			DSApi.context().rollback();
			throw new EdmHibernateException(e);
		} catch (Exception e) {
			DSApi.context().rollback();
			throw new EdmException(e);
		} finally {
			DSApi.close();
			if( __debug) System.out.println("[Protokol] koniec" );
		}
	}
	
	private DSUser findOsobaUprawniona(DSDivision dzial, Set<String> permittedUsers ) throws EdmException
	{
		DSUser[] users = dzial.getUsers(true);
		
		for( DSUser usr : users )
		{
			if( permittedUsers.contains(usr.getName()))
				return usr;
		}
		return null;
	}
	
	private void createProtokolBrakowania(String osoba, ProtokolModel protokol, String divisionGuid ) throws Exception
	{
		String opis= "Protok� oceny dokumentacji niearchiwalnej";

		Map<String, Map<String, FieldData>> casesMap= new HashMap();
		
		java.util.Collections.sort(protokol.cases, new Comparator<ProtokolCaseModel>()
				{
					@Override
					public int compare(ProtokolCaseModel o1, ProtokolCaseModel o2)
					{
						return (o1.lp < o2.lp ? 1 : (o1.lp == o2.lp ? 0 : -1));
					}
				});
		
		Integer case_index= 1;
		for( ProtokolCaseModel _case : protokol.cases)
		{
			if( _case.ignore )
				continue;
			
			Map<String, FieldData> fields = new HashMap();
			casesMap.put("PROTOKOL_CASES_"+case_index, fields);
			fields.put("LP", new FieldData( Type.INTEGER, _case.lp));
			fields.put("NR_SPISU", new FieldData( Type.INTEGER, _case.wykaz_number));
			fields.put("LP_W_SPISIE", new FieldData( Type.INTEGER, _case.lp_w_wykazie));
			fields.put("SYMBOL", new FieldData( Type.STRING, _case.symbol));
			fields.put("TYTUL", new FieldData( Type.STRING, _case.tytul));
			
			fields.put("DATA_OD", new FieldData( Type.STRING, _case.data_od));
			fields.put("DATA_DO", new FieldData( Type.STRING, _case.data_do));
			fields.put("CASE_ID", new FieldData( Type.INTEGER,  _case.case_id));
			
			fields.put("ID", new FieldData( Type.STRING, ""));
			
			++case_index;
		}
		
		// nie zostala dodana zadna sprawa
		if( case_index== 1)
		{
			if( __debug) System.out.println("[Protokol] Brak spraw, nie tworzy dokumentu");
			return;
		}
		
		OutOfficeDocument doc= new OutOfficeDocument();

		doc.setCurrentAssignmentGuid(divisionGuid);
		doc.setDivisionGuid(divisionGuid);
		doc.setCurrentAssignmentAccepted(Boolean.FALSE);
		doc.setCreatingUser(osoba);
		doc.setInternal(true);
		doc.setSummary(opis);
		doc.setForceArchivePermissions(false);
		doc.setAssignedDivision(divisionGuid);
		doc.setClerk(osoba);
		doc.setSender(new Sender());
		doc.setSource("");
		
		doc.create();

		DocumentKind documentKind= DocumentKind.findByCn(DocumentLogicLoader.PROTOKOL_BRAKOWANIA);

		Map<String, Object> dockindKeys= new HashMap();
		dockindKeys.put("DOC_DESCRIPTION", opis);
		//TODO ?
		//dockindKeys.put("DOC_DATE", wykaz.archive_date);
		dockindKeys.put("PROTOKOL_NUMBER", protokol.protokol_number);
		dockindKeys.put("M_DICT_VALUES", casesMap);
		
		doc.setDocumentKind(documentKind);
		
		Long newDocumentId= doc.getId();
	
		if( DwrDictionaryFacade.dictionarieObjects.get("PROTOKOL_CASES")==null)
			DwrDictionaryFacade.dictionarieObjects.put("PROTOKOL_CASES",new DwrDictionaryBase(DocumentKind.findByCn(DocumentLogicLoader.PROTOKOL_BRAKOWANIA).getFieldByCn("PROTOKOL_CASES"),null ) );
		
		documentKind.saveMultiDictionaryValues(doc, dockindKeys );
		
		documentKind.set(newDocumentId, dockindKeys);

		doc.getDocumentKind().logic().archiveActions(doc, 0);
		doc.getDocumentKind().logic().documentPermissions(doc);
		doc.setAuthor(osoba);
		DSApi.context().session().save(doc);

		doc.getDocumentKind().logic().onStartProcess(doc);

		WorkflowFactory.createNewProcess(doc, false, "Protok� oceny dokumentacji niearchiwalnej "+((Integer) protokol.protokol_number).toString());
	}
	

	/**
	 * pobiera sprawy, ktorych data zamkniecia jest mniejsza od podanej.
	 * dziala przy otwartym polaczeniu (sesji) bazy danych
	 */
	private List getCasesByFinishDate(Date finishDate) throws EdmException
	{
		Criteria criteria= DSApi.context().session().createCriteria(OfficeCase.class);
		criteria.add(Restrictions.eq("archived", false));
		//criteria.add(Restrictions.or(Restrictions.eq("archiveStatus", 0),Restrictions.isNull("archiveStatus")));
		criteria.add(Restrictions.lt("finishDate", finishDate));
		
		if( __debug) System.out.println("[Wykaz] Warunek: archiveStatus==0/null, finishDate<"+finishDate.toGMTString());
		return criteria.list();
	}

	private List getCasesDoBrakowania() throws EdmException
	{
		Criteria criteria= DSApi.context().session().createCriteria(OfficeCase.class);
		//criteria.add(Restrictions.eq("archived", true));
		criteria.add(Restrictions.eq("archiveStatus", 2));
		if( __debug) System.out.println("[getCasesDoBrakowania] Warunek: archiveStatus=2");

		return criteria.list();
	}

	private int getMaxWykazNumber() throws HibernateException, EdmException, SQLException
	{
		String query = "select max(WYKAZ_NUMBER)from DS_WYKAZ_DOC";
		
		PreparedStatement ps =  DSApi.context().prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		
		rs.next();
		Integer numer = rs.getInt(1);
		rs.close();
		ps.close();
		
		if( numer==null || numer< 0)
			return 0;
		
		return numer.intValue();
	}

	
	private boolean pobierzDatyWykazCase(OfficeCase tmp_case, SuperiorCaseModel wcase) throws SQLException, EdmException
	{
		Date minDate= null;
		Date maxDate= null;

		Long caseId= tmp_case.getId();
		String[] tabele= TABELE_DOKUMENTU;
		for (String tabelaKanc : tabele) {
			
			String query_data= "select min(doc.ctime), max(doc.ctime) from ds_document doc join " + tabelaKanc
			+ " dockanc on ( doc.id=dockanc.id and dockanc.CASE_ID=" + caseId + ")";
			PreparedStatement ps_data= DSApi.context().prepareStatement(query_data);
			ResultSet rs_data= ps_data.executeQuery();
			if (!rs_data.next() || rs_data.getDate(1)==null) {
				rs_data.close();
				ps_data.close();
				continue;
			}

			// Daty
			if (minDate == null)
				minDate= rs_data.getDate(1);
			else if (rs_data.getDate(1).before(minDate))
				minDate= rs_data.getDate(1);
			if (maxDate == null)
				maxDate= rs_data.getDate(2);
			else if (rs_data.getDate(2).after(maxDate))
				maxDate= rs_data.getDate(2);

			rs_data.close();
			ps_data.close();
			break;
		}

		if( minDate==null)
			return false;
		DateFormat df= new SimpleDateFormat(SuperiorCaseModel.DATA_FORMAT);
		
		wcase.data_od= df.format(minDate);
		wcase.data_do= df.format(maxDate);
		
		return true;
	}

	/** 
	 * @param pcase 
	 * @return zwraca true, jesli mozna archiwizowac sprawe, false jesli nie
	 */
	private boolean pobierzParametryWykazCase(OfficeCase tmp_case, ProtokolCaseModel pcase) throws SQLException,
			EdmException, Exception
	{
		// pobierz potrzebne parametry - numer Wykazu oraz pozycji w Wykazie, date utworzenia (najmniejsza)
		String query= "select C_wykaz_number, lp, data_do from DS_WYKAZ_CASES C join ds_wykaz_multiple M on C.id=M.FIELD_VAL where CASE_ID=" + tmp_case.getId();
		PreparedStatement ps= DSApi.context().prepareStatement(query);
		ResultSet rs= ps.executeQuery();
		if (!rs.next()) {
			// wykaz nie wystepuje, - sprawa nie zawierala dokumentow lub blad
			if( __debug) System.out.println("[Protokol] brak wykazu dla dla sprawy CASE_ID=" + tmp_case.getId());
			rs.close();
			ps.close();
			return false;
		}
		long wykaz_number= rs.getInt(1);
		long lp_w_wykazie= rs.getInt(2);
		String str_data_do= rs.getString(3);
		rs.close();
		ps.close();

		DateFormat formatter= new SimpleDateFormat(SuperiorCaseModel.DATA_FORMAT);
		Date data_od= formatter.parse(str_data_do);

		// wyznaczanie po jakim czasie sprawa moze zostac brakowana
		String str= tmp_case.getRwa().getAcHome().replaceAll("[^0-9]", "");
		int years= Integer.parseInt(str);
		Calendar calendar= Calendar.getInstance();
		calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - years);
		calendar.set(Calendar.MONTH, /* 0-based */0);
		calendar.set(Calendar.DAY_OF_MONTH, /* 1-based */1);
		calendar.set(Calendar.HOUR_OF_DAY, /* 0-based */0);
		calendar.set(Calendar.MINUTE, /* 0-based */0);
		Date finishDate= calendar.getTime();

		if (!data_od.before(finishDate))
			return false;

		pcase.wykaz_number= wykaz_number;
		pcase.lp_w_wykazie= lp_w_wykazie;
		
		return true;
	}

	private int getMaxProtokolNumber() throws EdmException, SQLException
	{
		String query = "select max(PROTOKOL_NUMBER)from DS_PROTOKOL_DOC";
		
		PreparedStatement ps =  DSApi.context().prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		
		rs.next();
		Integer numer = rs.getInt(1);			
		rs.close();
		ps.close();
		
		if( numer==null || numer< 0)
			return 0;
		
		return numer.intValue();
	}
	
	
	public Set<String> getUsersForPermission(String permissionStr) throws EdmException
	{
		Role[] roles = Role.list();
		for( Role r : roles )
			if( r.getPermissions().contains(permissionStr))
				return r.getUsernames();
		
		return new HashSet<String>();
	}
	

	/**
	 * Klasa podpinana do Timera, majaca wywolac akcje archiwizowania z odpowiednia data
	 */
	class CaseArchiverAction extends TimerTask
	{
		@Override
		public void run()
		{
			// ustaw date: rok-2, styczen 01
			Calendar calendar= Calendar.getInstance();
			calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 2);
			calendar.set(Calendar.MONTH, /* 0-based */0);
			calendar.set(Calendar.DAY_OF_MONTH, /* 1-based */1);
			calendar.set(Calendar.HOUR_OF_DAY, /* 0-based */0);
			calendar.set(Calendar.MINUTE, /* 0-based */0);
			Date finishDate= calendar.getTime();

			if( __debug) System.out.println("[Wykaz] Odpalam CaseArchiverAction::run, sprawdzam sprawy z data < " + finishDate.toString());

			try {
				generujWykazy(finishDate);
			} catch (EdmException e) {
				if( __debug) System.out.println("[Wykaz] CaseArchiverAction::run Niepowodzenie");
				e.printStackTrace(System.out);
			}
		}
	}
	
	
	/**
	 * Klasa podpinana do Timera, majaca wywolac akcje tworzenia protokolu brakowania
	 */
	class ProtokolBrakowaniaAction extends TimerTask
	{
		@Override
		public void run()
		{
			if( __debug) System.out.println("[Protokol] Odpalam ProtokolBrakowaniaAction::run");

			try {
				generujProtokolyBrakowania();
			} catch (EdmException e) {
				if( __debug) System.out.println("[Protokol] ProtokolBrakowaniaAction::run Niepowodzenie");
				e.printStackTrace(System.out);
			}
		}
	}

	/**
	 * ustaw cykliczne wywolywanie - raz dziennie. Pierwszy run - niech przeczeka z 5 sekund, na konczenia inicjalizacji
	 * Tomcata
	 */
	@Override
	protected void start() throws ServiceException
	{
		archiveTimer= new Timer(true);
		protokolTimer= new Timer(true);

		System.out.println("CaseArchiverScheduled - action scheduled");
		archiveTimer.schedule(new CaseArchiverAction(), 0, DateUtils.DAY);
		protokolTimer.schedule(new ProtokolBrakowaniaAction(), 0, DateUtils.DAY);
	}

	@Override
	protected void stop() throws ServiceException
	{
		System.out.println("CaseArchiverScheduled - stop uslugi");
		if (archiveTimer != null)
			archiveTimer.cancel();
		if (protokolTimer != null)
			protokolTimer.cancel();
	}

	/**
	 * uzytkownik nie moze zatrzymac
	 */
	@Override
	protected boolean canStop()
	{
		return false;
	}
}
