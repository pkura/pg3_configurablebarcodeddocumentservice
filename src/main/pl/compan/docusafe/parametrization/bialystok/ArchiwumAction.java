package pl.compan.docusafe.parametrization.bialystok;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

import com.lowagie.text.Cell;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.webwork.ServletActionContext;


/**
 * akcja do przyciskow na formatce dokumentu dla cn='wykaz' i cn='protokol_cn'
 * @author bkaliszewski
 *
 */
public class ArchiwumAction extends EventActionSupport //PortfoliosBaseAction
{
	protected String baseLink = "/office/archiwum.action";
    
    public String getBaseLink()
    {
        return baseLink;
    }
    
    
    // @IMPORT
    private Long documentId;
	private String komunikat= "";
    
    

    protected void setup()
    {
        /*registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new Tabs()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
*/
        
        registerListener("doArchivedCasesListPDF").
	       append(OpenHibernateSession.INSTANCE).
	       append(new CreateArchivedCasesPdfAction()).
	       appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doProtokolPDF").
	       append(OpenHibernateSession.INSTANCE).
	       append(new CreateProtokolPdfAction()).
	       appendFinally(CloseHibernateSession.INSTANCE);
        
        registerListener("doProtokolBrakuj").
	       append(OpenHibernateSession.INSTANCE).
	       append(new BrakujDokumentacjeAction()).
	       appendFinally(CloseHibernateSession.INSTANCE);
    }

    
    public void setDocumentId(Long documentId)
	{
		this.documentId= documentId;
	}



	public String getKomunikat()
	{
		return komunikat;
	}


	public void setKomunikat(String komunikat)
	{
		this.komunikat= komunikat;
	}



	/**
     * druk wykazu zdawczo odbiorczego do PDF
     */
	private class CreateArchivedCasesPdfAction implements ActionListener
	{

		public void actionPerformed(ActionEvent event)
		{
			try {
				ServletUtils.streamFile(ServletActionContext.getResponse(), pdfList(), "application/pdf",
						"Content-Disposition: attachment; filename=\"wykaz zdawczo-odbiorczy.pdf\"");
			} catch (EdmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		private File pdfList() throws EdmException
		{
			OutOfficeDocument doc = OutOfficeDocument.findOutOfficeDocument(documentId);
			FieldsManager fm = doc.getFieldsManager();
			fm.initialize();
			String[][] data= getData(fm);

			File pdfFile= null;
			try {
				// font
				File fontDir= new File(Docusafe.getHome(), "fonts");
				File arial= new File(fontDir, "arial.ttf");
				BaseFont baseFont= BaseFont.createFont(arial.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

				Font fontSuperSmall= new Font(baseFont, (float) 6);
				Font fontSmall= new Font(baseFont, (float) 8);
				Font fontNormal= new Font(baseFont, (float) 12);
				Font fontLarge= new Font(baseFont, (float) 16);

				// file
				pdfFile= File.createTempFile("docusafe_", "_tmp");
				pdfFile.deleteOnExit();
				com.lowagie.text.Document pdfDoc= new com.lowagie.text.Document(PageSize.A4);
				PdfWriter.getInstance(pdfDoc, new FileOutputStream(pdfFile));

				pdfDoc.open();

				// content

				/* ********************************** */

				Cell empty= new Cell();
				empty.setBorder(0);
				Table emptyTable= new Table(1);
				emptyTable.addCell(empty);
				emptyTable.setBorder(0);
				emptyTable.setPadding(10f);

				// skladanie naglowka
				Table topTable= new Table(1);
				topTable.setCellsFitPage(true);
				topTable.setWidth(100);
				topTable.setPadding(3);
				topTable.setBorder(0);

				// TODO wstawic numer
				Cell titleCell= new Cell(new Paragraph("Spis zdawczo-odbiorczy akt nr "+fm.getIntegerValue("WYKAZ_NUMBER"), fontLarge));
				// TODO wstawic nazwe
				Cell divisionCell= new Cell(new Paragraph(DSDivision.find(doc.getDivisionGuid()).getName(), fontNormal));
				Cell divisionUnderCell= new Cell(new Paragraph("(Nazwa jednostki/kom�rki organizacyjnej)", fontSmall));

				divisionCell.setBorder(0);
				divisionUnderCell.setBorder(0);
				titleCell.setBorder(0);
				titleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				topTable.addCell(divisionCell);
				topTable.addCell(divisionUnderCell);
				topTable.addCell(titleCell);
				topTable.addCell(empty);
				topTable.addCell(empty);
				pdfDoc.add(topTable);

				int[] widths= { 5, 10, 26, 16, 8, 8, 13, 14 };
				final int AMOUNT_COLUMNS = widths.length;
				
				// tabela wlasciwa
				Table table= new Table(AMOUNT_COLUMNS);
				table.setCellsFitPage(true);
				table.setWidth(100);
				table.setPadding(3);
				table.setBorder(1);

				table.setWidths(widths);

				Cell[] cells= new Cell[AMOUNT_COLUMNS];
				cells[0]= new Cell(new Phrase("Lp.", fontNormal));
				cells[1]= new Cell(new Phrase("Znak teczki", fontNormal));
				cells[2]= new Cell(new Phrase("Tytu� teczki lub tomu/sprawa", fontNormal));
				cells[3]= new Cell(new Phrase("Daty skrajne od - do", fontNormal));
				cells[4]= new Cell(new Phrase("Kat. arch.", fontNormal));
				cells[5]= new Cell(new Phrase("Liczba teczek", fontNormal));
				cells[6]= new Cell(new Phrase("Miejsce przechowywania akt w sk�adnicy", fontNormal));
				cells[7]= new Cell(new Phrase("Data zniszczenia lub przekazania", fontNormal));

				for (Cell c : cells) {
					c.setHorizontalAlignment(Element.ALIGN_CENTER);
					table.addCell(c);
				}

				// tabela wlasciwa - content
				////int i= 1;
				for (String[] dataLine : data) {
					int kolumna= 0;

					//// Lp.
					////table.addCell(new Cell(((Integer) i++).toString()));
					++kolumna;

					// kolejne kolumny
					for (String s : dataLine) {
						table.addCell(new Cell(new Phrase(s, fontNormal)));
						++kolumna;
						if (kolumna > AMOUNT_COLUMNS)
							break;
					}

					// uzupelnienie brakujacych kolumn do konca
					for (; kolumna <= AMOUNT_COLUMNS; ++kolumna)
						table.addCell(new Cell());
				}
				pdfDoc.add(table);

				// stopka
				Table stopka= new Table(2);
				stopka.setCellsFitPage(true);
				stopka.setWidth(100);
				stopka.setPadding(3);
				stopka.setBorder(0);

				Cell przekazujacy= new Cell( new Phrase("przekazuj�cy akta:", fontNormal));
				przekazujacy.setBorder(0);
				przekazujacy.setHorizontalAlignment(Element.ALIGN_LEFT);
				Cell przyjmujacy= new Cell( new Phrase("przyjmuj�cy akta:", fontNormal));
				przyjmujacy.setBorder(0);
				przyjmujacy.setHorizontalAlignment(Element.ALIGN_RIGHT);

				stopka.addCell(przekazujacy);
				stopka.addCell(przyjmujacy);

				Cell przekazujacy2= new Cell( new Phrase("(podpis)", fontNormal));
				przekazujacy2.setBorder(0);
				przekazujacy2.setHorizontalAlignment(Element.ALIGN_LEFT);
				Cell przyjmujacy2= new Cell( new Phrase("(podpis)", fontNormal));
				przyjmujacy2.setBorder(0);
				przyjmujacy2.setHorizontalAlignment(Element.ALIGN_RIGHT);

				stopka.addCell(przekazujacy2);
				stopka.addCell(przyjmujacy2);

				pdfDoc.add(emptyTable);
				pdfDoc.add(stopka);

				pdfDoc.close();
			} catch (Exception e) {
				throw new EdmException(e);
			}

			return pdfFile;
		}

		/**
		 * zwraca tablice, w ktorej kazdy wiersz odpowiada kolejnej sprawie i zawiera nastepujace dane: <br />
		 * - Lp. <br />
		 * - Znak teczki <br />
		 * - Tytul teczki lub tomu/sprawa <br />
		 * - Daty skrajne: od-do <br />
		 * - Kat. arch. <br />
		 * @return String[amount_of_cases][5_string_fields]
		 * @throws EdmException
		 */
		private String[][] getData(FieldsManager fm) throws EdmException
		{
			List<Map<String, Object>> casesFields = (List<Map<String, Object>>)fm.getValue("WYKAZ_CASES");
			
			String[][] data = new String[casesFields.size()][];
			
			for ( Map<String, Object> fields : casesFields) {
				int i = (int) ((Long.parseLong(fields.get("WYKAZ_CASES_LP").toString()))-1); 
				data[i]= new String[5];
				
				data[i][0]= fields.get("WYKAZ_CASES_LP").toString();
				data[i][1]= fields.get("WYKAZ_CASES_ZNAK").toString();
				data[i][2]= fields.get("WYKAZ_CASES_TYTUL").toString();
				data[i][3]= fields.get("WYKAZ_CASES_DATA_OD").toString()+", "+ fields.get("WYKAZ_CASES_DATA_DO").toString();
				data[i][4]= fields.get("WYKAZ_CASES_KAT_AKT").toString();
			}
			return data;
		}
	}
	
	
	/**
     * druk protokolu brakowania do PDF
     */
	private class CreateProtokolPdfAction implements ActionListener
	{

		public void actionPerformed(ActionEvent event)
		{
			try {
				ServletUtils.streamFile(ServletActionContext.getResponse(), pdfList(), "application/pdf",
						"Content-Disposition: attachment; filename=\"protokol oceny dokumentacji niearchiwalnej.pdf\"");
			} catch (EdmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		private File pdfList() throws EdmException
		{
			OutOfficeDocument doc = OutOfficeDocument.findOutOfficeDocument(documentId);
			FieldsManager fm = doc.getFieldsManager();
			fm.initialize();
			String[][] data= getData(fm);

			File pdfFile= null;
			try {
				// font
				File fontDir= new File(Docusafe.getHome(), "fonts");
				File arial= new File(fontDir, "arial.ttf");
				BaseFont baseFont= BaseFont.createFont(arial.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

				Font fontSuperSmall= new Font(baseFont, (float) 6);
				Font fontSmall= new Font(baseFont, (float) 8);
				Font fontNormal= new Font(baseFont, (float) 12);
				Font fontNormal_Large= new Font(baseFont, (float) 14);
				Font fontLarge= new Font(baseFont, (float) 16);

				// file
				pdfFile= File.createTempFile("docusafe_", "_tmp");
				pdfFile.deleteOnExit();
				com.lowagie.text.Document pdfDoc= new com.lowagie.text.Document(PageSize.A4);
				PdfWriter.getInstance(pdfDoc, new FileOutputStream(pdfFile));

				pdfDoc.open();

				// content

				/* ********************************** */

				Cell empty= new Cell();
				empty.setBorder(0);
				Table emptyTable= new Table(1);
				emptyTable.addCell(empty);
				emptyTable.setBorder(0);
				emptyTable.setPadding(10f);

				// skladanie naglowka
				Table topTable= new Table(1);
				topTable.setCellsFitPage(true);
				topTable.setWidth(100);
				topTable.setPadding(3);
				topTable.setBorder(0);

				Cell titleCell= new Cell(new Paragraph("Protok� oceny dokumentacji niearchiwalnej", fontLarge));
				
				Cell subTitleCell = new Cell(new Paragraph("Wykaz dokumentacji niearchiwalnej przeznaczonej na makulatur� lub zniszczenie", fontNormal_Large));
				
				// TODO adres jednostki organizacyjnej?
				Cell divisionCell= new Cell(new Paragraph(DSDivision.find(doc.getDivisionGuid()).getName(), fontNormal));
				Cell divisionUnderCell= new Cell(new Paragraph("(nazwa i adres jednostki organizacyjnej)", fontSmall));

				divisionCell.setBorder(0);
				divisionUnderCell.setBorder(0);
				titleCell.setBorder(0);
				titleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				subTitleCell.setBorder(0);
				subTitleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				topTable.addCell(divisionCell);
				topTable.addCell(divisionUnderCell);
				topTable.addCell(empty);
				topTable.addCell(empty);
				topTable.addCell(titleCell);
				topTable.addCell(empty);
				topTable.addCell(empty);
				topTable.addCell(empty);
				topTable.addCell(empty);
				pdfDoc.add(topTable);
				
				
				Table content = new Table(1);
				content.setCellsFitPage(true);
				content.setWidth(100);
				content.setPadding(3);
				content.setBorder(0);
				
				Cell content1Cell= new Cell(new Paragraph("Komisja w sk�adzie (imiona, nazwiska i stanowiska cz�onk�w komisji):", fontNormal));
				content1Cell.setBorder(0);
				content.addCell(content1Cell);
				Cell contentDotsCell= new Cell(new Paragraph("...................................................................................................................................................", fontNormal));
				contentDotsCell.setBorder(0);
				content.addCell(contentDotsCell);
				content.addCell(contentDotsCell);
				content.addCell(contentDotsCell);
				content.addCell(contentDotsCell);
				content.addCell(contentDotsCell);
				Cell content2Cell= new Cell(new Paragraph("dokona�a oceny i wydzielenia przeznaczonej do przekazania na makulatur� lub zniszczenie dokumentacji niearchiwalnej w ilo�ci ................. mb i stwierdzi�a, �e stanowi ona dokumentacj� niearchiwaln� nieprzydatn� dla cel�w praktycznych jednostki organizacyjnej, oraz �e up�yn�y terminy jej przechowywania, okre�lone w jednolitym rzeczowym wykazie akt lub kwalifikatorze dokumentacji technicznej.", fontNormal));
				content2Cell.setBorder(0);
				content.addCell(content2Cell);
				content.addCell(empty);
				pdfDoc.add(content);
				
				
				
				Table contentStopka = new Table(3);
				//contentStopka.setCellsFitPage(true);
				contentStopka.setWidth(100);
				contentStopka.setPadding(3);
				contentStopka.setBorder(0);
				
				Cell contentStopka1Cell= new Cell(new Paragraph("Przewodnicz�cy Komisji", fontNormal));
				contentStopka1Cell.setBorder(0);
				contentStopka1Cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				Cell contentStopka2Cell= new Cell(new Paragraph("Cz�onkowie Komisji", fontNormal));
				contentStopka2Cell.setBorder(0);
				contentStopka2Cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				Cell dotsCell= new Cell(new Paragraph(".....................................", fontNormal));
				dotsCell.setBorder(0);
				dotsCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				/*Cell emptyCell= new Cell(new Paragraph(" ", fontNormal));
				emptyCell.setBorder(0);
				emptyCell.setHorizontalAlignment(Element.ALIGN_RIGHT);*/
				Cell contentStopka3Cell= new Cell(new Paragraph("(podpisy)", fontNormal));
				contentStopka3Cell.setBorder(0);
				contentStopka3Cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				
				contentStopka.addCell(empty);
				contentStopka.addCell(contentStopka1Cell);
				contentStopka.addCell(dotsCell);
				
				contentStopka.addCell(empty);
				contentStopka.addCell(contentStopka2Cell);
				contentStopka.addCell(dotsCell);
				
				
				contentStopka.addCell(empty);
				contentStopka.addCell(empty);
				contentStopka.addCell(dotsCell);
				
				contentStopka.addCell(empty);
				contentStopka.addCell(empty);
				contentStopka.addCell(dotsCell);
				
				contentStopka.addCell(empty);
				contentStopka.addCell(empty);
				contentStopka.addCell(contentStopka3Cell);
				pdfDoc.add(contentStopka);
				
				
				Table preTable = new Table(1);
				//contentStopka.setCellsFitPage(true);
				preTable.setWidth(100);
				preTable.setPadding(3);
				preTable.setBorder(0);
				preTable.addCell(empty);
				preTable.addCell(empty);
				preTable.addCell(empty);
				preTable.addCell(subTitleCell);
				preTable.addCell(empty);
				preTable.addCell(empty);
				pdfDoc.add(preTable);
				
				

				
				int[] widths= { 5, 14, 17, 30, 14, 8, 12 };
				final int AMOUNT_COLUMNS = widths.length;
				
				// tabela wlasciwa
				Table table= new Table(AMOUNT_COLUMNS);
				table.setCellsFitPage(true);
				table.setWidth(100);
				table.setPadding(3);
				table.setBorder(1);

				table.setWidths(widths);

				Cell[] cells= new Cell[AMOUNT_COLUMNS];
				cells[0]= new Cell(new Phrase("Lp.", fontNormal));
				cells[1]= new Cell(new Phrase("Nr i lp. spisu zdawczo-odbiorczego", fontNormal));
				cells[2]= new Cell(new Phrase("Symbol z wykazu akt", fontNormal));
				cells[3]= new Cell(new Phrase("Tytu� teczki", fontNormal));
				cells[4]= new Cell(new Phrase("Daty skrajne", fontNormal));
				cells[5]= new Cell(new Phrase("Liczba tom�w", fontNormal));
				cells[6]= new Cell(new Phrase("Uwagi", fontNormal));

				for (Cell c : cells) {
					c.setHorizontalAlignment(Element.ALIGN_CENTER);
					table.addCell(c);
				}

				// tabela wlasciwa - content
				////int i= 1;
				for (String[] dataLine : data) {
					int kolumna= 1;

					// kolejne kolumny
					for (String s : dataLine) {
						table.addCell(new Cell(new Phrase(s, fontNormal)));
						++kolumna;
						if (kolumna > AMOUNT_COLUMNS)
							break;
					}

					// uzupelnienie brakujacych kolumn do konca
					for (; kolumna <= AMOUNT_COLUMNS; ++kolumna)
						table.addCell(new Cell());
				}
				pdfDoc.add(table);

				// stopka
				/*Table stopka= new Table(2);
				stopka.setCellsFitPage(true);
				stopka.setWidth(100);
				stopka.setPadding(3);
				stopka.setBorder(0);

				Cell przekazujacy= new Cell("Przekazuj�cy akta:");
				przekazujacy.setBorder(0);
				przekazujacy.setHorizontalAlignment(Element.ALIGN_LEFT);
				Cell przyjmujacy= new Cell("przyjmuj�cy akta:");
				przyjmujacy.setBorder(0);
				przyjmujacy.setHorizontalAlignment(Element.ALIGN_RIGHT);

				stopka.addCell(przekazujacy);
				stopka.addCell(przyjmujacy);

				Cell przekazujacy2= new Cell("(podpis)");
				przekazujacy2.setBorder(0);
				przekazujacy2.setHorizontalAlignment(Element.ALIGN_LEFT);
				Cell przyjmujacy2= new Cell("(podpis)");
				przyjmujacy2.setBorder(0);
				przyjmujacy2.setHorizontalAlignment(Element.ALIGN_RIGHT);

				stopka.addCell(przekazujacy2);
				stopka.addCell(przyjmujacy2);

				pdfDoc.add(emptyTable);
				pdfDoc.add(stopka);*/

				pdfDoc.close();
			} catch (Exception e) {
				throw new EdmException(e);
			}

			return pdfFile;
		}

		/**
		 * zwraca tablice, w ktorej kazdy wiersz odpowiada kolejnej sprawie i zawiera nastepujace dane: <br />
		 * - Lp. <br />
		 * - nr spisu / lp. w spisie <br />
		 * - symbol <br />
		 * - Tytul teczki <br />
		 * - Daty skrajne: od-do <br />
		 * @return String[cases.size()][5]
		 * @throws EdmException
		 */
		private String[][] getData(FieldsManager fm) throws EdmException
		{
			List<Map<String, Object>> casesFields = (List<Map<String, Object>>)fm.getValue("PROTOKOL_CASES");
			
			String[][] data = new String[casesFields.size()][];
			
			int i=0;
			for ( Map<String, Object> fields : casesFields) {
				//int i = ((Integer)fields.get("PROTOKOL_CASES_LP"))-1; 
				data[i]= new String[5];
				
				data[i][0]= fields.get("PROTOKOL_CASES_LP").toString();
				data[i][1]= fields.get("PROTOKOL_CASES_NR_SPISU").toString()+", "+fields.get("PROTOKOL_CASES_LP_W_SPISIE").toString();
				data[i][2]= fields.get("PROTOKOL_CASES_SYMBOL").toString();
				data[i][3]= fields.get("PROTOKOL_CASES_TYTUL").toString();
				data[i][4]= fields.get("PROTOKOL_CASES_DATA_OD").toString()+", "+ fields.get("PROTOKOL_CASES_DATA_DO").toString();
				++i;
			}
			return data;
		}
	}
	
	
	/**
     * powoduje wybrakowanie dokumentacji wylistowanej w danym protokole
     */
	private class BrakujDokumentacjeAction implements ActionListener
	{

		public void actionPerformed(ActionEvent event)
		{
			try {
				if (!DSApi.context().hasPermission(DSPermission.BRAKOWANIE_DOKUMENTOW)) {
					// throw new EdmException(sm.getString("NieMaszUprawnienDoZarzadzaniaKluczamiKryptograficznymi"));
					komunikat= "Nie masz wystarcz�jacych uprawnie�";
					return;
				}
				
				
				OutOfficeDocument doc = OutOfficeDocument.findOutOfficeDocument(documentId);
				FieldsManager fm = doc.getFieldsManager();
				fm.initialize();
				
				List<Map<String, Object>> casesFields = (List<Map<String, Object>>)fm.getValue("PROTOKOL_CASES");
				
				for ( Map<String, Object> fields : casesFields) {
					Long case_id = Long.parseLong(fields.get("PROTOKOL_CASES_CASE_ID").toString());
					OfficeCase casee = OfficeCase.find(case_id);
					
					if( casee.getArchiveStatus().equals(OfficeCase.ArchiveStatus.ToShred) /* protokol brakowania*/)
					{
						System.out.println("Brakuje sprawe: id="+casee.getOfficeId());
						casee.setArchiveStatus(OfficeCase.ArchiveStatus.Shredded /*usunieto/brakowano*/);
						DSApi.context().session().update(casee);
					/*List<OfficeDocument> dokumenty = OfficeCase.find(case_id).getDocuments();
					for( OfficeDocument d : dokumenty )
					{
						
					}*/
					}
					else
						System.out.println("Nie moge wybrakowac sprawy: id="+casee.getOfficeId());
				}

				komunikat= "Dokonano brakowania dokumentacji";
				// TODO BIALYSTOK
				//

			} catch (EdmException e) {
				e.printStackTrace();
			}
		}
	}
}
