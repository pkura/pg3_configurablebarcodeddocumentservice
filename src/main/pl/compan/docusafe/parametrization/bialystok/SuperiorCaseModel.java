package pl.compan.docusafe.parametrization.bialystok;

import java.io.Serializable;

import org.hibernate.CallbackException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;

public class SuperiorCaseModel implements Lifecycle
{
	public static final String DATA_FORMAT= "dd-MM-yyyy";
	
	long case_id;
	String data_od;
	String data_do;
	
	
	@Override
	public boolean onDelete(Session arg0) throws CallbackException
	{
		return false;
	}

	@Override
	public void onLoad(Session arg0, Serializable arg1)
	{
	}

	@Override
	public boolean onSave(Session arg0) throws CallbackException
	{
		return false;
	}

	@Override
	public boolean onUpdate(Session arg0) throws CallbackException
	{
		return false;
	}
}
