package pl.compan.docusafe.parametrization.bialystok;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.CallbackException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;

public class WykazCaseModel extends SuperiorCaseModel
{
	long c_wykaz_number;
	
	boolean ignore= true;		// czy z jakiegos powodu sprawa ma byc pominieta
	
	String znak;
	
	String tytul;
	
	String kat_akt;
	
	OfficeCase tmp_case;
	int lp;
	//List tmp_case_docs;

	//liczba teczek
	//miejsce przechowywania
	//data zniszczenia
	

	
	public WykazCaseModel()
	{
	}
	
	public WykazCaseModel(OfficeCase _case, int wykaz_number)
	{
		this.c_wykaz_number= wykaz_number;
		this.case_id= _case.getId();
		this.tmp_case = _case;
		znak= _case.getOfficeId();//DSDivision.find(divisionGuid).getCode();//+" "+lp;
		tytul= _case.getParent().getName();
		kat_akt= _case.getRwa().getAcHome();
	}
}
