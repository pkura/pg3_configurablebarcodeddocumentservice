package pl.compan.docusafe.parametrization.bialystok;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.CallbackException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;

public class ProtokolCaseModel extends SuperiorCaseModel
{
	long wykaz_number;
	
	boolean ignore= true;		// czy z jakiegos powodu sprawa ma byc pominieta
	
	long lp_w_wykazie;
	
	String symbol;
	
	String tytul;
	
	OfficeCase tmp_case;
	int lp;

	
	public ProtokolCaseModel()
	{
	}
	
	
	public ProtokolCaseModel(OfficeCase _case /*, List case_docs , int lp, long wykaz_number, long lp_w_wykazie*/) 
	{
		this.tmp_case = _case;
		String divisionGuid= _case.getDivisionGuid();
		// /this.wykaz_number= wykaz_number;
		this.case_id= _case.getId();
		// this.lp= lp;
		// this.wykaz_number=wykaz_number;
		// this.lp_w_wykazie=lp_w_wykazie;

		symbol= _case.getOfficeId();// TODO
		tytul= _case.getParent().getName();
	}
}
