package pl.compan.docusafe.parametrization.bialystok;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;

/**
 * klasa do "kierownikow dzialu"
 * 
 * @author bkaliszewski
 * 
 */
public class DivisionBoss
{
	private static String[] kierownikSynonimy= { "kierownik", "g��wna ksi�gowa", "ordynator", "koordynator", "naczelnik" };

	/**
	 * wyszukuje kierownika w dziale lub w dziale wyzej
	 * 
	 * @param division
	 * @return
	 * @throws EdmException
	 */
	public static DSDivision findManager(DSDivision division) throws EdmException
	{
		DSDivision managerDivision= null;
		out: if (division.getDivisionType().equals("division")) {
			// 1. dla dzialu: jesli jest stanowisko kierownik, to dla niego
			managerDivision= DivisionBoss.findManagerChilds(division);
			if (managerDivision != null)
				break out;

			// 2. dla dzialu: jesli dzia� wy�ej ma stanowisko kierownik, to
			// dla niego
			managerDivision= DivisionBoss.findManagerChilds(division.getParent());
			if (managerDivision != null)
				break out;

		}

		out: if (division.getDivisionType().equals("position")) {
			// 3. dla stanowiska: przechodzi do dzialu i szuka stanowiska
			// "kierownik"
			managerDivision= DivisionBoss.findManagerChilds(division.getParent());
			if (managerDivision != null)
				break out;

			// 4. je�eli nie znajdzie kierownika, to na u�ytkownika w tym dziale
			// wy�szym (o ile jest tam jakis uzytkownik)
			if( division.getParent()!=null && division.getParent().getOriginalUsers().length>0)
				managerDivision= division.getParent();
		}
		
		// jesli nie znalezlismy kierownika i mamy pusty dzial, to probujemy wyzej
		if( managerDivision==null && division.getOriginalUsers().length==0 && division.getParent()!=null )
			return findManager(division.getParent());

		return (managerDivision != null) ? managerDivision : division;
	}

	/**
	 * funkcja wyszukuje stanowisko kierownika
	 */
	private static DSDivision findManagerChilds(DSDivision division) throws EdmException
	{
		if( division==null)
			return null;
		
		DSDivision childs[]= division.getChildren("position");

		for (DSDivision ch : childs)
			for (String kierownik : kierownikSynonimy)
				if (ch.getName().equalsIgnoreCase(kierownik))
					return ch;

		return null;
	}
}
