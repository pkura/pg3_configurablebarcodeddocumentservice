package pl.compan.docusafe.parametrization.dnbnord;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Maps;

public class PromotionalMaterialsLogic extends AbstractDocumentLogic 
{
	private static final long serialVersionUID = 1L;

	private static PromotionalMaterialsLogic instance;
	
	public static final String AKCEPACJA_WYMAGANA_CN = "W_AKCEPTACJA";
	public static final String RODZAJ_CN = "RODZAJ";
	
	public static PromotionalMaterialsLogic getInstance() 
	{
		if (instance == null)
			instance = new PromotionalMaterialsLogic();
		return instance;
	}
	
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException 
	{
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
		document.setFolder(folder);

		document.setTitle(document.getDocumentKind().getName());
		document.setDescription(document.getDocumentKind().getName());
		
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		Map<String,Object> values = new HashMap<String,Object>();
		//values.put(AKCEPACJA_WYMAGANA_CN, (Boolean) fm.getKey(AKCEPACJA_WYMAGANA_CN) == null ? false : fm.getKey(AKCEPACJA_WYMAGANA_CN));
		values.put(AKCEPACJA_WYMAGANA_CN, Integer.valueOf(document.getDocumentKind().getFieldByCn(RODZAJ_CN).getEnumItem((Integer) fm.getKey(RODZAJ_CN)).getArg(1)) >= 50);
		document.getDocumentKind().setOnly(document.getId(), values);
	}
	
	public void documentPermissions(Document document) throws EdmException 
	{
		Set<PermissionBean> perms = new HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn() + "_DOCUMENT_READ", ObjectPermission.GROUP,document.getDocumentKind().getName()+" - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_READ",ObjectPermission.GROUP,document.getDocumentKind().getName()+" - zalacznik odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn() + "_DOCUMENT_READ_MODIFY",ObjectPermission.GROUP,document.getDocumentKind().getName()+" - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_MODIFY",ObjectPermission.GROUP,document.getDocumentKind().getName()+" - zalacznik modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn() + "_DOCUMENT_READ_DELETE",ObjectPermission.GROUP,document.getDocumentKind().getName()+" - usuwanie"));
		this.setUpPermission(document, perms);
	}
	
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();
        
        DSUser loggedUser = DSApi.context().getDSUser();
        toReload.put("SENDER_HERE", "u:"+loggedUser.getName()+";d:"+loggedUser.getDivisionsWithoutGroupPosition()[0].getGuid());
        
        fm.reloadValues(toReload);
    }

}
