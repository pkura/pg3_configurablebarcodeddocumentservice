package pl.compan.docusafe.parametrization.dnbnord;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class BailiffNoteLogic extends AbstractDocumentLogic 
{
	public static final String KLIENT_CN = "KLIENT";
	public static final String KOMORNIK_CN = "KOMORNIK";
	public static final String DANE_UZUPELNIONE_CN = "DANE_UZUPELNIONE";
	public static final String KLIENT_BANKU_CN = "KLIENT_BANKU";
	public static final String DIC_BANKOWY_CN = "KLIENT_BANKOWY";
	public static final String DIC_SYGNATURA_CN = "KLIENT_SYGNATURA";
	
	private static final long serialVersionUID = 1L;

	private static Logger LOG = LoggerFactory.getLogger(BailiffNoteLogic.class);

	private static BailiffNoteLogic instance;

	public static BailiffNoteLogic getInstance() 
	{
		if (instance == null)
			instance = new BailiffNoteLogic();
		return instance;
	}
	
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException 
	{
		LOG.info("BailiffNoteLogic archiveActions");
		
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Dokumenty Komornicze");
		
		String sygnatura = fm.getKey(KLIENT_CN) != null ? (String) ((Map<String, Object>) fm.getValue(KLIENT_CN)).get(DIC_SYGNATURA_CN) : null;		
		if(sygnatura != null && sygnatura.length() > 0)
		{
			folder = folder.createSubfolderIfNotPresent(sygnatura);
		}
		else
		{
			folder = folder.createSubfolderIfNotPresent("Brak Sygnatury");
		}
		document.setFolder(folder);

		document.setTitle(document.getDocumentKind().getName());
		document.setDescription(document.getDocumentKind().getName());
		
		Map<String,Object> values = new HashMap<String,Object>();
		
		values.put(DANE_UZUPELNIONE_CN, fm.getKey(KLIENT_CN) != null && fm.getKey(KOMORNIK_CN) != null);
		values.put(KLIENT_BANKU_CN, fm.getKey(KLIENT_CN) != null ? "1".equals(((EnumValues) ((Map<String, Object>) fm.getValue(KLIENT_CN)).get(DIC_BANKOWY_CN)).getSelectedId()) : false);
		
		document.getDocumentKind().setOnly(document.getId(), values);
	}
	
	public void documentPermissions(Document document) throws EdmException 
	{
		LOG.info("BailiffNoteLogic documentPermissions");
		Set<PermissionBean> perms = new HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn() + "_DOCUMENT_READ", ObjectPermission.GROUP,document.getDocumentKind().getName()+" - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_READ",ObjectPermission.GROUP,document.getDocumentKind().getName()+" - zalacznik odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn() + "_DOCUMENT_READ_MODIFY",ObjectPermission.GROUP,document.getDocumentKind().getName()+" - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_MODIFY",ObjectPermission.GROUP,document.getDocumentKind().getName()+" - zalacznik modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn() + "_DOCUMENT_READ_DELETE",ObjectPermission.GROUP,document.getDocumentKind().getName()+" - usuwanie"));
		this.setUpPermission(document, perms);
	}


}