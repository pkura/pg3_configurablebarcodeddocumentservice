package pl.compan.docusafe.parametrization.dnbnord;

import java.util.HashSet;
import java.util.Set;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class BailiffNoteOutLogic extends AbstractDocumentLogic 
{

	private static final long serialVersionUID = 1L;

	private static Logger LOG = LoggerFactory.getLogger(BailiffNoteOutLogic.class);

	private static BailiffNoteOutLogic instance;

	public static BailiffNoteOutLogic getInstance() {
		if (instance == null)
			instance = new BailiffNoteOutLogic();
		return instance;
	}
	
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException 
	{
		LOG.info("BailiffNoteOutLogic archiveActions");
		
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		
		document.setFolder(fm.getKey("SOURCE") != null ? Document.find((Long) fm.getKey("SOURCE")).getFolder() : Folder.getRootFolder());
		
		document.setTitle(document.getDocumentKind().getName());
		document.setDescription(document.getDocumentKind().getName());		
	}
	
	public void documentPermissions(Document document) throws EdmException 
	{
		LOG.info("BailiffNoteOutLogic documentPermissions");
		Set<PermissionBean> perms = new HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn() + "_DOCUMENT_READ", ObjectPermission.GROUP,document.getDocumentKind().getName()+" - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_READ",ObjectPermission.GROUP,document.getDocumentKind().getName()+" - zalacznik odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn() + "_DOCUMENT_READ_MODIFY",ObjectPermission.GROUP,document.getDocumentKind().getName()+" - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_MODIFY",ObjectPermission.GROUP,document.getDocumentKind().getName()+" - zalacznik modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn() + "_DOCUMENT_READ_DELETE",ObjectPermission.GROUP,document.getDocumentKind().getName()+" - usuwanie"));
		this.setUpPermission(document, perms);
	}

}
