package pl.compan.docusafe.parametrization.dnbnord;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.templating.Templating;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Maps;

public class CreateBnOutJbpmTask implements ExternalActivityBehaviour 
{
	private static final Logger log = LoggerFactory.getLogger(CreateBnOutJbpmTask.class);
	private static final String OUT_DK = "bn_out"; 
	private static final String INT_DK = "bn_int";
	
	public void execute(ActivityExecution execution) throws Exception 
	{
		Document orgDoc = Jbpm4Utils.getDocument(execution);
		Document komDoc = null;
		Document kliDoc = null;
		Document opInfDoc = null;
		FieldsManager orgFm = orgDoc.getDocumentKind().getFieldsManager(orgDoc.getId());
		DSUser loggedUser = DSApi.context().getDSUser();
		
		Map<String,Object> valuesKom = new HashMap<String,Object>();
		valuesKom.put("SENDER_HERE", "u:"+loggedUser.getName()+";d:WINDYKACJA");
		valuesKom.put("TYP", 101);
		valuesKom.put("KOMORNIK", orgFm.getKey("KOMORNIK"));
		valuesKom.put("SOURCE", orgDoc.getId());
		komDoc = this.createDoc(valuesKom,loggedUser,DocumentKind.findByCn(OUT_DK), "OUT");
		
		if((Boolean) orgFm.getKey("KLIENT_BANKU"))
		{
			Map<String,Object> valuesKli = new HashMap<String,Object>();
			valuesKli.put("SENDER_HERE", "u:"+loggedUser.getName()+";d:WINDYKACJA");
			valuesKli.put("TYP", 201);
			valuesKli.put("KLIENT", orgFm.getKey("KLIENT"));
			valuesKli.put("SOURCE", orgDoc.getId());
			kliDoc = this.createDoc(valuesKli,loggedUser,DocumentKind.findByCn(OUT_DK), "OUT");
			
			Map<String,Object> valuesOpInf = new HashMap<String,Object>();
			valuesOpInf.put("TYP", 1);
			valuesOpInf.put("KLIENT", orgFm.getKey("KLIENT"));
			valuesOpInf.put("SOURCE", orgDoc.getId());
			opInfDoc = this.createDoc(valuesOpInf,loggedUser,DocumentKind.findByCn(INT_DK), "INT");
		}
		
		Map<String,String> tempValues = new HashMap<String,String>();
		tempValues.put("KOM_SAD",(String) ((Map<String, Object>) orgFm.getValue("KOMORNIK")).get("KOMORNIK_SAD"));
		tempValues.put("KOM_IMIE",(String) ((Map<String, Object>) orgFm.getValue("KOMORNIK")).get("KOMORNIK_IMIE"));
		tempValues.put("KOM_NAZWISKO",(String) ((Map<String, Object>) orgFm.getValue("KOMORNIK")).get("KOMORNIK_NAZWISKO"));
		tempValues.put("KOM_ULICA",(String) ((Map<String, Object>) orgFm.getValue("KOMORNIK")).get("KOMORNIK_ULICA") +" "+ (String) ((Map<String, Object>) orgFm.getValue("KOMORNIK")).get("NR_DOMU"));
		tempValues.put("KOM_KOD",(String) ((Map<String, Object>) orgFm.getValue("KOMORNIK")).get("KOMORNIK_KOD")); 
		tempValues.put("KOM_MIASTO",(String) ((Map<String, Object>) orgFm.getValue("KOMORNIK")).get("KOMORNIK_MIEJSCOWOSC"));
		tempValues.put("DATA",DateUtils.formatJsDate(orgDoc.getCtime()));
		tempValues.put("SYG",(String) ((Map<String, Object>) orgFm.getValue("KLIENT")).get("KLIENT_SYGNATURA"));
		tempValues.put("KLIENT_NAZWA", (String) ((Map<String, Object>) orgFm.getValue("KLIENT")).get("KLIENT_NAZWA") + " " + (String) ((Map<String, Object>) orgFm.getValue("KLIENT")).get("KLIENT_IMIE") + " " + (String) ((Map<String, Object>) orgFm.getValue("KLIENT")).get("KLIENT_NAZWISKO"));
		
		if(komDoc != null && kliDoc == null)
		{
			this.createAttachment(komDoc, "bo_kom_brak.rtf", tempValues,"Pismo do Komornika");
		}
		else if(komDoc != null && kliDoc != null)
		{
			this.createAttachment(komDoc, "bo_kom_ist.rtf", tempValues,"Pismo do Komornika");
			this.createAttachment(kliDoc, "bo_kli_ist.rtf", tempValues,"Pismo do Klienta");
		}
  
	}
	
	private void createAttachment(Document doc, String tempName, Map<String, String> params, String title) throws Exception
	{
		File tmpfile = File.createTempFile("template", ".rtf");
		FileOutputStream fos = new FileOutputStream(tmpfile);
		Templating.rtfTemplate(tempName, params, fos, "false");
        IOUtils.closeQuietly(fos);
        
    	Attachment attachment = new Attachment(title);
    	doc.createAttachment(attachment);
    	AttachmentRevision rev = attachment.createRevision(tmpfile);
    	rev.setOriginalFilename(title.replace(" ", "_")+".rtf");
    	
    	TaskSnapshot.updateByDocument(doc);
	}
	
	private Document createDoc(Map<String,Object> values, DSUser loggedUser, DocumentKind kind, String type) throws Exception 
	{
		
		OutOfficeDocument crtDoc = new OutOfficeDocument();
		if("INT".equals(type))
		{
			crtDoc.setInternal(true);
			
		}
		
		
		
		crtDoc.setDocumentKind(kind);
		
		crtDoc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
		crtDoc.setDivisionGuid(DSDivision.ROOT_GUID);
		crtDoc.setCurrentAssignmentAccepted(Boolean.FALSE);
		crtDoc.setSummary("");
		crtDoc.setCreatingUser(loggedUser.getName());
		
		crtDoc.create();
		
		crtDoc.getDocumentKind().set(crtDoc.getId(), values);
		
		crtDoc.setDocumentDate(new Date());
		crtDoc.getDocumentKind().logic().archiveActions(crtDoc, 0);
		crtDoc.getDocumentKind().logic().documentPermissions(crtDoc);
		DSApi.context().session().save(crtDoc);
		
		DSApi.context().session().flush();
		
		Map<String, Object> map = Maps.newHashMap();
		crtDoc.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(crtDoc, map);
		
		TaskSnapshot.updateByDocument(crtDoc);
		return crtDoc;
	}

	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception 
	{
		
	}

}
