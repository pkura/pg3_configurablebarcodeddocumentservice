package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


/**
 *  wymusza dodanie za��cznika przy zmianie stanu na WYDANO
 *  @author Rafal Odoj
 */
public class Stamp_ForceAttachmentWhenWYDANOListener extends AbstractEventListener{

	private static final long serialVersionUID = 1L;
	private final static Logger log = LoggerFactory.getLogger(Stamp_ForceAttachmentWhenWYDANOListener.class);

	/** tytu� za��cznika, kt�ry b�dzie sprawdzany/szukany.
	 * Je�li NULL, to nie ma wp�ywu - b�dzie sprawdzenie, czy istnieje jakikolwiek za��cznik */
    private String attachmentTitle;

    public void notify(EventListenerExecution eventListenerExecution) throws Exception {
        Long docId = Long.valueOf(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument document = OfficeDocument.find(docId);
        
        String status = document.getFieldsManager().getStringKey(WSSK_CN.STATUSDOKUMENTU); //pobranie statusu
		if( ! status.equals(WSSK_CN.STMP_STAT_5))		// "wydano"
			return; //w pozostalych przypadkach nie ma co sprawdzac
        
		//wydano, sprawdzic zalacznik
        for(Attachment at: document.getAttachments())
            if((attachmentTitle== null && at != null) || (attachmentTitle != null && at.getTitle().equals(attachmentTitle)))
                return;
        
        String msg = "Brak wymaganego za��cznika";
       
        if(attachmentTitle != null)
        	msg += " - " + attachmentTitle;
        
        log.debug(msg);
        throw new IllegalStateException(msg);
    }
}
