package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** Desicion Handler, do wywo�ania koniecznie na samym pocz�tku procesu.
 * Sprawdza, czy status procesu wynosi NULL.
 * <ul> jesli tak, zwraca decyzj� "tak" (nowy)</ul>
 * <ul> je�li nie, zwraca decyzj� "nie" (wznowiony) i ustawia zmienn� procesu
 * o nazwie spod WSSK_CN.VAR_ProcReopened na WSSK_CN.dwrTrue.
 * Czyli obecnie ProcessIsReopened="tak" </ul>
 * <p>
 * Tak wi�c brak zmiennej, lub inna warto�� oznaczaj� p�niej, �e to proces nowy.
 * Dzia�anie opiera si� na za�o�eniu, �e w chwili tworzenia nowego procesu (i tylko wtedy)
 * STATUSDOKUMENTU jest null, bo jest ustawiany listenerem przy pierwszym ludzkim tasku.
 * <p>
 * Raczej nie mo�na do tego celu u�y� pola MARKERWZNOWIENIA,
 * bo jest on na razie (2012-10-12) ustawiany PO faktycznym wznowieniu
 * i doj�ciu do pierwszego ludzkiego tasku.
 * @author Rafal Odoj
 * @date 2012-10-12  */
public class Reopen_CheckProcessNew_Decision implements DecisionHandler {
	
	private static final long serialVersionUID = 1L;
	private final static Logger log = LoggerFactory.getLogger(Reopen_CheckProcessNew_Decision.class);
	
	public String decide(OpenExecution execution) {

		try {
			Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument document = OfficeDocument.find(docId);
			FieldsManager fm = document.getFieldsManager();
			
			Object status = fm.getKey(WSSK_CN.STATUSDOKUMENTU);
			if (status==null)
				return "tak";
			else {
				execution.setVariable(WSSK_CN.VAR_ProcReopened, WSSK_CN.dwrTrue);
				return "nie";
			}
			
		} catch (Exception e) {
			log.error("b��d podejmowania decyzji: "+e.getMessage(), e);
			return "tak"; //wyj�cie awaryjne
		}
	}
}
