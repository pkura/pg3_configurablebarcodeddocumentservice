package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** Pobiera login z formularza, wyszukuje uzytkownika i asygnuje mu zadanie.
 * Jesli nie znajdzie nikogo, wysyla do tworcy wniosku alarmujace info.
 * Pole status sluzy do zapisania sztywnego statusu w historii dekreacji (w forku)
 * @author Rafal Odoj
 * @date 2012-08-13 */
public class SysAcc_NewUser_AsgnHandler implements AssignmentHandler{
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(SysAcc_NewUser_AsgnHandler.class);
	
	protected String status;

	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
		
		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		String login = (String) fm.getValue(WSSK_CN.CRSYS_login);
		
		try {
			DSUser uzytkownik = DSUser.findByUsername(login);
			assignable.addCandidateUser(uzytkownik.getName());
			AHEUtils.addToHistory(openExecution, doc, uzytkownik.getName(), null, status, log);
			
		} catch (Exception e){
			doc.setDescription("B��d! Po zako�czeniu realizacji wniosku nie mo�na znale��� u�ytkownika");
			log.debug("B��d! Po zako�czeniu realizacji wniosku nie mo�na znale��� u�ytkownika " +login );
			assignable.addCandidateUser(doc.getAuthor());
			AHEUtils.addToHistory(openExecution, doc, doc.getAuthor(), null, "adresat nie zosta� znaleziony, zwrot do nadawcy", log);
			throw e;
		}
	}
}
