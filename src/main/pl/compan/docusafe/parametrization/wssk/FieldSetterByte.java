package pl.compan.docusafe.parametrization.wssk;

import java.util.HashMap;
import java.util.Map;

/** 
 * Zmienia pole w dokumencie - ustawia pole "field" na warto�� "value".
 * Wywoluje przy tym specjalizowana metode, ktora zapewnia typ byte
 * @author Rafal Odoj
 * @date 2012-08-30
 */
public class FieldSetterByte extends FieldSetterBase {
	private static final long serialVersionUID = 1L;

	@Override
	protected Map<String, ?> setter(){
		Map<String, Byte> map = new HashMap<String, Byte>();
		Byte bajt = Byte.parseByte(value);
		map.put(field, bajt);
		return map;
	}
}
