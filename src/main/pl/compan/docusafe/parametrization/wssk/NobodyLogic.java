package pl.compan.docusafe.parametrization.wssk;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.util.HashMap;
import java.util.Map;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

/** 
 * Logika, ktorej celem jest uniemozliwienie dostepu do mozliwosci wypelnienia tego pisma przez uzytkownikow.
 * Do calkowitego usuniecia
 * @author Rafal Odoj
 */
public class NobodyLogic extends AbstractDocumentLogic {

	private static final long serialVersionUID = 1L;
	private static NobodyLogic instance;
    protected static Logger log = LoggerFactory.getLogger(NobodyLogic.class);

    public static NobodyLogic getInstance() {
        if (instance == null)
            instance = new NobodyLogic();
        return instance;
    }

    /** nikt nie ma do tego uprawnien, tylko usluga moze wypelnic ten dokument */
    public void documentPermissions(Document document) throws EdmException {
    	
    }
	
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
		 log.info("type: {}", type);
		 Map<String,Object> values = new HashMap<String,Object>();
	     values.put("TYPE", type);
	     fm.reloadValues(values);
	}
	
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        as.useFolderResolver(document);
        as.useAvailableProviders(document);
        log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
    }
    

    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) {
        log.info("--- MinimalLogic : START PROCESS !!! ---- {}", event);
        try {
            Map<String, Object> map = Maps.newHashMap();
            if (document instanceof InOfficeDocument)
            {
            	 map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
            }
            else{
            	map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
            	map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
            }
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    @Override
    public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException {
        ProcessCoordinator ret = new ProcessCoordinator();
        ret.setUsername(document.getAuthor());
        return ret;
    }

	@Override
	public void onRejectedListener(Document doc) throws EdmException { }

	@Override
	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException { }
}
