package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *  Sprawdza, czy wszystkie checkboxy sa zaznaczone przy danym przejsciu.
 *  Jesli nie, to nie wolno przejsc dalej (wyjatek).
 *  W pliku formatki wszystkie pola checkboxow, ktore maja byc sprawdzane, musza miec identyczna nazwe (jak w klasie WSSK_CN stala DOKLS_check (obecnie 'CHECK')).
 *  Do tej nazwy doklejamy kolejne liczby zaczynajac od 1. W tej samej klasie stala DOKLS_checkNo wskazuje numer ostatniego uzywanego checkboxu
 *  (wlasciwie liczbe checkboxow).
 *  Aby proces mogl przejsc dalej, checkboxy CHECK1, CHECK2, ..., CHECK7 musza byc zaznaczone.
 *  Jezeli sie usunie checkbox z formatki, przejscie dalej stanie sie niemozliwe - 
 *  konieczne bedzie przenumerowanie checkboxow do ciaglego szeregu od 1 do n i ustawienie stalej DOKLS_checkNo na n (w kodzie).
 *  
 *  TODO : ilosc checkboxow powinna byc okreslana w XML a nie w kodzie, gdyz zmiana ilosci checkboxow wymaga obecnie rekompilacji aplikacji.
 *  
 *  @author Rafal Odoj
 */
public class AreAllCheckboxesSetListener extends AbstractEventListener {

	private static final long serialVersionUID = 1L;
	private final static Logger log = LoggerFactory.getLogger(AreAllCheckboxesSetListener.class);
	
	public void notify(EventListenerExecution eventListenerExecution) throws Exception {
		Long docId = Long.valueOf(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument document = OfficeDocument.find(docId);
		
		FieldsManager fm = document.getFieldsManager();
		
		String checkbo = null;
		
		for (int i=1; i<=WSSK_CN.DOKLS_checkNo; i++){
			checkbo = WSSK_CN.DOKLS_check + i;
			if ( ! WSSK_CN.dwrTrue.equals(fm.getValue(checkbo)) ){
				log.debug("Aby m�c przej�� dalej nale�y zapozna� si� ze wszystkimi dokumentami");
				throw new EdmException ("Aby m�c przej�� dalej nale�y zapozna� si� ze wszystkimi dokumentami");
			}
		}
    }
}
