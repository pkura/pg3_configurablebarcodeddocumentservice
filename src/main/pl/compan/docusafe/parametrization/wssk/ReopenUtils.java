package pl.compan.docusafe.parametrization.wssk;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.dbutils.DbUtils;
import org.directwebremoting.dwrp.CallBatch;
import org.jbpm.api.Execution;
import org.jbpm.api.ExecutionService;
import org.jbpm.api.ProcessInstance;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.util.Logger;

/** klasa utylitarna, przechowuje wsp�lny kod dla logik implementuj�cych interfejs LogicExtension_Reopen.
 * Napisane dla WSSK, gdzie procesy musz� by� po zako�czeniu wznawiane w dziwaczny spos�b,
 * np. proces wniosku o utworzenie kont systemowych po realizacji mo�e by� ponownie otwarty,
 * jego pola zmodyfikowane i dokument puszczony w obieg jeszcze raz.
 * 
 * Rozszerzona logika umo�liwia edycj� "in place" wznawianego dokumentu przez osob� wznawiaj�c�.
 * Technicznie: dokument trafia od razu na list� zada� 1. uczestnika procesu, ale wznawiaj�cy (ju� po tym fakcie!!!)
 * mo�e go przez chwil� modyfikowa� i zapisa� - nie najszcz�liwsze rozwi�zanie, ale nie widz� lepszego.
 * @author Rafal Odoj
 * @date 2012-10-08 */
public class ReopenUtils {
	
	/** wpisuje do DB marker oznaczaj�cy 'do wznowienia', po to by Drools mog�y pozwoli� na pe�n� edycj� dokumentu */
	public static void reopenMarker(Long docId, String tabela, String kolumna, String markerWznowienia) throws EdmException {
		DSApi.context().session().flush();
		
    	String sql= "update " +tabela +" set "+kolumna+"="+markerWznowienia+" where Document_Id=?";
    	PreparedStatement ps = null;
        try{
            ps = DSApi.context().prepareStatement(sql);
            ps.setLong(1, docId);
            ps.execute();
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            DSApi.context().closeStatement(ps);
        }
        
        DSApi.context().session().flush();
	}
	
	/** odczytuje z DB marker oznaczaj�cy 'do wznowienia' */
	public static Integer getReopenMarker(Long docId, String tabela, String kolumna) throws EdmException {
		DSApi.context().session().flush();
		
    	String sql= "select " +kolumna +" from "+tabela +" where Document_Id=?";
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	Integer wynik = null;
        try{
            ps = DSApi.context().prepareStatement(sql);
            ps.setLong(1, docId);
            rs = ps.executeQuery();
            if (rs.next())
                wynik = rs.getInt(1);
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            DSApi.context().closeStatement(ps);
        }
        
        DSApi.context().session().flush();
        return wynik;
	}
	
	/** zrzuca warto�ci z fieldsManagera do mapy */
	public static Map<String, Object> fieldsManagerValues(FieldsManager fieldsManager) throws EdmException {
		Map<String, Object> vals = new HashMap<String, Object>();
		for ( Map.Entry<String, Object> o: fieldsManager.getFieldValues().entrySet()){
			Object fvl = o.getValue();
			vals.put(o.getKey(), fvl);
		}
		return vals;
	}
	
	/** kod obs�ugi fasady dokumentu przy wznawianiu przez DWR.
	 * M.in. sprawdza, czy jest ci�g 'ReopenWf' w URL.
	 * @param req - request z DwrFacade
	 * @param dwrSessionValues - warto�ci przechowywane w sesji DWR, z DwrFacade
	 * @param dwrfacade - sam obiekt DwrFacade
	 * @param dockindCn - spodziewany cn tego dockindu - gdyby okaza� si� faktycznie inny to zg�aszany jest error. Z logiki
	 * @param log - logger z logiki */
	public static void manageReopenMarker(
			HttpServletRequest req,
			Map<String, Object> dwrSessionValues,
			FieldsManager fieldsManager,			// DEL
			DwrFacade dwrfacade,
			int[] statusyKoncaProcesu,				// DEL
			String tabela_DB,						// DEL
			String dockindCn,
			String processCn,
			String stanCzekania,
			Logger log
			) throws EdmException {
		
		if (dockindCn==null)
			err("brak cn dockindu, kt�rego dotyczy wznowienie", log);
		if ( ! dockindCn.equals(dwrSessionValues.get("dockindKind")))
			err("cn dockindu, kt�rego dotyczy wznowienie, jest niezgodny z oczekiwanym", log);
		
		HashMap<String, Object> wartosci = (HashMap<String, Object>) dwrSessionValues.get("FIELDS_VALUES");
		Integer status = (Integer) wartosci.get(WSSK_CN.STATUSDOKUMENTU);
		Integer marker = (Integer) wartosci.get(WSSK_CN.MARKERWZNOWIENIA);
		boolean resume=false;

		String page=null;
		CallBatch cb = (CallBatch) req.getAttribute("org.directwebremoting.dwrp.batch");
		if (cb==null){
			// jak to null?
			resume=true;
			log.debug("CallBatch ��dania DWR przy wznawianiu dokumentu == null");
		} else {
			page=cb.getPage();
			if (page == null){
				// jak to null?
				resume=true;
				log.debug("CallBatch.getPage() ��dania DWR przy wznawianiu dokumentu == null");
			} else {
				Integer marker_DB = ReopenUtils.getReopenMarker(
						(Long)dwrSessionValues.get("documentId"),
						tabela_DB,
						WSSK_CN.MARKERWZNOWIENIA);
				
				if (page.contains(WSSK_CN.REOPENWF_INURL)) {
					// edycja - wpisac markerwznowienia z bazy
					if (wartosci != null)
						wartosci.put(WSSK_CN.MARKERWZNOWIENIA, new Integer(1));
				}
				else if (marker_DB != null) {
					if (marker_DB.equals(WSSK_CN.WZNAWIANY))
						// w URL nie ma ReopenWf ale jest markerwznowienia=1, czyli:
						// wci�ni�to ZAPISZ lub co� takiego, koniec wznowienia, wybudzic proces.
						// NIESTETY z powodu przekierowania strony proces b�dzie wybudzony, ale reszta b�dzie wycofana przez rollback
						// w tym takze odswiezenie listy zadan. Na szczescie gdy markerwznowienia w DB jest juz 0, to w sesji jest jeszcze 1
						// stad ponizszy else
						resume=true;
					else if (marker==1)
						// odswiezenie listy zadan - czytaj opis powyzej
						TaskSnapshot.updateByDocument( OfficeDocument.find((Long)dwrSessionValues.get("documentId")) );
				}
			}
		}

		if (resume)
			resume ( //Proces powinien by� zosta� zablokowany na swym pocz�tku przez STATE o nazwie 'czekaj'. A teraz trzeba go odblokowa�
					processCn,
					stanCzekania,
					(Long)dwrSessionValues.get("documentId"),
					log);
	}
	
	/** KOPIA */
	public static void KOPIA__manageReopenMarker__KOPIA(
			HttpServletRequest req,
			Map<String, Object> dwrSessionValues,
			FieldsManager fieldsManager,
			DwrFacade dwrfacade,
			int[] statusyKoncaProcesu,
			String tabela_DB,
			String dockindCn,
			String processCn,
			String stanCzekania,
			Logger log
			) throws EdmException {
		
		if (dockindCn==null)
			err("brak cn dockindu, kt�rego dotyczy wznowienie", log);
		
		if ( ! dockindCn.equals(dwrSessionValues.get("dockindKind")))
			err("cn dockindu, kt�rego dotyczy wznowienie, jest niezgodny z oczekiwanym", log);
		else {
			HashMap<String, Object> wartosci = (HashMap<String, Object>) dwrSessionValues.get("FIELDS_VALUES");
			Integer status = (Integer) wartosci.get(WSSK_CN.STATUSDOKUMENTU);
			Integer marker = (Integer) wartosci.get(WSSK_CN.MARKERWZNOWIENIA);
			boolean ustawNormalnie=true; 
			boolean resetujMarkerRestartu=false;
			if ( status!= null) {
				if ( in (status.intValue(), statusyKoncaProcesu)  ){
					// czyli obecny status jest statusem ko�ca procesu (jednym z podanych mo�liwych),
					// wczytuj marker wznowienia z DB a reszt� p�l z DWR
					
					ustawNormalnie=false;
					
					Integer marker_DB = ReopenUtils.getReopenMarker(
							(Long)dwrSessionValues.get("documentId"),
							tabela_DB,
							WSSK_CN.MARKERWZNOWIENIA);
					Map<String, Object> mapaWartosci = new HashMap<String, Object>();
					mapaWartosci.put(WSSK_CN.MARKERWZNOWIENIA, marker_DB);
					fieldsManager.reloadValues(mapaWartosci);
					
					dwrfacade.setFieldValues(  ReopenUtils.fieldsManagerValues(fieldsManager)  );
					
				} else if (marker!=null)
					if (marker.intValue()==WSSK_CN.WZNAWIANY ){
						String page=null;
						CallBatch cb = (CallBatch) req.getAttribute("org.directwebremoting.dwrp.batch");
						if (cb==null){
							//dlaczego null? Na wszelki wypadek kasuj� marker restartu
							resetujMarkerRestartu=true;
							log.debug("CallBatch ��dania DWR przy wznawianiu dokumentu == null");
						} else {
							page=cb.getPage();
							if (page == null){
								//dlaczego null? Na wszelki wypadek kasuj� marker restartu
								resetujMarkerRestartu=true;
								log.debug("CallBatch.getPage() ��dania DWR przy wznawianiu dokumentu == null");
							} else {
								/* U�ytkownik klikn�� link "przywr��" w archiwum i modyfikuje dokument przed puszczeniem go dalej.
								 * Modyfikacja dokumentu przez wznawiaj�cego w tym stanie charakteryzuje si� tym, �e:
								 * request.attributes."batch".getPage() = np. /docusafe/office/internal/document-archive.action?documentId=71&doReopenWf=true
								 * dwrSessionValues."activity" = null albo "" (nie wiem dokladnie)
								 * 
								 * gdy zostanie wci�ni�ty przycisk ZAPISZ, dopisek ReopenWf w page zniknie i b�dzie:
								 * request.attributes."batch".getPage() = np. /docusafe/office/internal/document-archive.action
								 * dwrSessionValues."activity" = null albo "" (nie wiem dokladnie)
								 * 
								 * gdy kolejny u�ytkownik wejdzie do tasku b�dzie:
								 * request.attributes."batch".getPage() = np. /docusafe/office/internal/document-archive.action?documentId=71&activity=internal
								 * dwrSessionValues."activity" != null (jest podane aktualne)
								 * 
								 * Czyli edycja przy wznowieniu trwa tak d�ugo, jak w adresie mamy ReopenWf */
								
								if (page.contains(WSSK_CN.REOPENWF_INURL)){
									// edycja nadal trwa
								} else {
									// wci�ni�to ZAPISZ lub co� takiego, usun�� marker restartu
									resetujMarkerRestartu=true;
								}
								
							}
						}
					}
			}
			if (ustawNormalnie){
				dwrfacade.setFieldValues((Map<String, Object>) dwrSessionValues.get("FIELDS_VALUES"));
				
				if (resetujMarkerRestartu){
					dwrfacade.getFieldValues().put(WSSK_CN.MARKERWZNOWIENIA, new Integer(0));
					
					reopenMarker(
							(Long)dwrSessionValues.get("documentId"),
							tabela_DB,
							WSSK_CN.MARKERWZNOWIENIA,
							""+WSSK_CN.STAN_NORMALNY);
					
					resume (		//proces powinien by� zosta� zablokowany na samym pocz�tku przez STATE o nazwie czekaj. A teraz trzeba go pchn�� (odblokowa�).
							processCn,
							stanCzekania,
							(Long)dwrSessionValues.get("documentId"),
							log);
				}
				fieldsManager.reloadValues(dwrfacade.getFieldValues());
			}
		}
	}
	
	/** Namiastka operatora IN, sprawdza, czy podana warto�� wyst�puje na li�cie.
	 * <p> TODO : Gdyby status�w ko�cowych mia�o by� du�o, zastosowa� Set,
	 * bo obecnie przegl�da tablic� element po elemencie. */
	public static boolean in (int szukany, int[] tablicaSzukanych ){
		boolean wynik = false;
		for (int status : tablicaSzukanych)
			if ( status == szukany ){
				wynik=true;
				break;
			}
		return wynik;
	}
	
	/** Wybija proces ze stanu czekania (STATE o nazwie "czekaj"),
	 * w kt�ry wszed� przy wznowieniu, by wznawiaj�cy m�g� spokojnie edytowa� dokument.  
	 * @throws EdmException */
	public static void resume(String processName, String activity, long documentId, Logger log) throws EdmException{
		
		String processInstanceID = processInStateForDocumentId(processName, activity, documentId, log);
		
		ExecutionService executionService = Jbpm4Provider.getInstance().getExecutionService();
		
		ProcessInstance processInstance = executionService.findProcessInstanceById(processInstanceID);
		if (processInstance==null)
			err("Nie znaleziono procesu w stanie '" +activity+ "' o nazwie '" +processName +"' dla dokumentu o ID=" +documentId, log);
		Execution executionWaiting = processInstance.findActiveExecutionIn(WSSK_CN.reopenWaitState);
		if (executionWaiting==null)
			return; // Nie znaleziono, wiec widocznie takiego juz nie ma. Przeciez metoda mogla byc wywolana kilka razy pod rzad przy przeladowaniu strony. 
//			err("Nie znaleziono 'execution' dla procesu w stanie '" +activity+ "' o nazwie '" +processName +"' dla dokumentu o ID=" +documentId, log);
		
		//wybudzenie:
		executionService.signalExecutionById(executionWaiting.getId());
		TaskSnapshot.updateByDocument( OfficeDocument.find(documentId) );
	}
	
	/** Szuka procesu o danej nazwie i w podanym stanie (activity) dla podanego id dokumentu.
	 * Jesli jest ich wi�cej, rzuca wyj�tek.
	 * Je�li nie ma �adnego, zwraca null. */
	public static String processInStateForDocumentId(String processName, String activity, long documentId, Logger log) throws EdmException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        String jbpmID=null;
        String activityDB;
        
        //  SELECT jbpm4_id, ACTIVITYNAME_ FROM ds_document_to_jbpm4, JBPM4_EXECUTION
        //  WHERE jbpm4_id=ID_ AND document_id=59 AND process_name='reopen'
        //samo ds_document_to_jbpm4 nie wystarczy, bo zawiera te� stare, zamkni�te procesy
        String sql = "SELECT jbpm4_id, ACTIVITYNAME_ FROM ds_document_to_jbpm4, JBPM4_EXECUTION "
        			+"WHERE jbpm4_id=ID_ AND document_id = ? AND process_name = ?";
        try {
            ps = DSApi.context().prepareStatement(sql);
            ps.setLong(1, documentId);
            ps.setString(2, processName);
            rs = ps.executeQuery();
            
            while (rs.next()) {
            	
            	activityDB = rs.getString(2);
            	
            	if ( activityDB==null && activity!=null ) // puste activity, a nie takie bylo szukane
            		continue;
            	
            	if (jbpmID != null)
            		err("Wi�cej ni� jeden �ywy proces o nazwie " +processName +" dla dokumentu o ID=" +documentId, log);
            	
            	jbpmID = rs.getString(1);
            }
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            DSApi.context().closeStatement(ps);
            DbUtils.closeQuietly(rs);
        }
        return jbpmID;
    }
	
//	@SuppressWarnings("unused")
//	/** info do logu i wyjatek Edm */
//	private static void edm (String s, Logger log) throws EdmException {
//		log.debug(s);
//		throw new EdmException(s);
//	}
	
	/** error do logu i wyjatek Edm */
	private static void err (String s, Logger log) throws EdmException {
		log.error(s);
		throw new EdmException(s);
	}
}
