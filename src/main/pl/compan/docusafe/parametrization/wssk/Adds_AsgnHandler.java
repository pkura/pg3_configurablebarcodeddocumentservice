package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.OfficeDocument;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** AssigneeHandler w wersji uproszczonej, ale dziala w forku i pobiera dane z adds.properties.
 * Moze ustawiac podane sztywno statusy, ale nie musi (status moze zostac null).
 * Jako guid i user podajemy nazwy wpisow w adds a nie same guidy, czy nazwy uzytkownikow.
 * To w zmiennych adds powinny byc wpisane te guidy i loginy.
 * Ma to taki cel, zeby przy zmianie struktury organ. nie zmieniac XML-i i Javy.
 * @author Rafal Odoj
 * @date 2012-09-06 */
public class Adds_AsgnHandler  implements AssignmentHandler {

	private static final long serialVersionUID = 1L;
	
	protected String guid;
	protected String user;
	protected String status; //np. wewnatrz forka mozna wpisac odpowiedni status do historii
	
	private static final Logger log = LoggerFactory.getLogger(Adds_AsgnHandler.class);

	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
	    
		boolean isOpened = true;
		if (!DSApi.isContextOpen()) {
			isOpened = false;
			DSApi.openAdmin();
		}
		Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		
		try {
			if (guid != null) {
				for (String g : guid.split(",")) {
					String cel = Docusafe.getAdditionProperty(g);
					assignable.addCandidateGroup(cel);
					AHEUtils.addToHistory(openExecution, doc, null, cel, status, log);
				}
			} else if (user != null) {
				if (user.equals("author")) {
					assignable.addCandidateUser(doc.getAuthor());
					AHEUtils.addToHistory(openExecution, doc, doc.getAuthor(), null, status, log);
				} else {
					String cel = Docusafe.getAdditionProperty(user);
					assignable.addCandidateUser(cel);
					AHEUtils.addToHistory(openExecution, doc, cel, null, status, log);
				}
			}
		}
		catch (EdmException e) {
			log.error(e.getMessage());
			throw e;
		}
		finally {
			if (DSApi.isContextOpen() && !isOpened)
				DSApi.close();
		}
		
	}
}
