package pl.compan.docusafe.parametrization.wssk;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/** Custom activity, operacja utworzenia podprocesu w procesie. <br/>
 * Podproces ma poinformowac o swym zakonczeniu rodzica sygnalem.
 * Pierwszym asygnowanym nowego procesu bedzie administrator danych osobowych. <br/>
 * Parametr ado moze byc poprzedzony gwiazdka (*) - wtedy jest traktowany jak nazwa adds
 * (gwiazdka bedzie pominieta przy szukaniu adds-a) */
public class NewEmployee_ChildProcessStart implements ExternalActivityBehaviour {
	
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(NewEmployee_ChildProcessStart.class);

	private String ado; //administratordanychosobowych
	
	private static Format formatter = new SimpleDateFormat("yyyy-MM-dd");
	
	private static void edm (String s) throws EdmException {
		log.debug(s);
		throw new EdmException(s);
	}
	private static void err (String s) throws EdmException {
		log.error(s);
		throw new EdmException(s);
	}

	public void signal(ActivityExecution activityExecution, String s, Map<String, ?> stringMap) throws Exception {  }

	public void execute(ActivityExecution activityExecution) throws Exception {
		
		boolean wasOpened = true;
		try {
			wasOpened = DSApi.isContextOpen();
			if (!wasOpened) {
				DSApi.openAdmin();
				DSApi.context().begin();
			}
		} catch (Exception e) {
			err("B��d pod��czenia do bazy danych. " + e.getMessage());
			return;
		}
		
		String ado_str = null;
		
		// znalezienie ADO
		DSUser asygnowany = null;
		try {
			if (ado.startsWith("*"))
				ado_str = Docusafe.getAdditionProperty(ado.substring(1));
			else
				ado_str = ado;
			
			// odszukanie ado
			asygnowany = UserFactory.getInstance().findByUsername( ado_str );
		} catch (UserNotFoundException unfe) {
			edm("Brak uzytkownika ADO o nazwie okreslonej w zmiennych podprocesu");
		} catch (EdmException exc) {
			edm("B��d podczas wyszukiwania u�ytkownika ADO" + exc.getMessage());
		}
		if (asygnowany == null)
			edm("Brak uzytkownika ADO o nazwie okreslonej w zmiennych podprocesu");
		
		
		// Znalezienie dzialu, do ktorego nalezy ADO
		DSDivision division = null;
		try {
			division = GetterUtil.getReasonableDSDivisionOfUser(asygnowany.getDivisions());
		} catch (Exception e){
			log.debug("uzytkownik ADO nie ma przypisanego zadnego dzialu. Nie jest to blad krytyczny dla procesu. "+e.getMessage());
		}
		String dvsn = division.getName();

		
		// Znalezienie uzytkownika, ktorego sprawa dotyczy
		Object nowyo = activityExecution.getVariable(WSSK_CN.NEWEE_PVAR_employee);
		if (nowyo == null)
			edm("Brak okreslonej nazwy nowego uzytkownika w zmiennych podprocesu");
		String nowys = (String) nowyo;
		DSUser nowy = null;
		try {
			// odszukanie pracownika
			nowy = UserFactory.getInstance().findByUsername( nowys );
		} catch (UserNotFoundException unfe) {
			edm("Brak pracownika o nazwie okreslonej w zmiennych podprocesu");
		} catch (EdmException exc) {
			edm("B��d podczas wyszukiwania u�ytkownika " + exc.getMessage());
		}
		if (nowy == null)
			edm("Brak pracownika o nazwie okreslonej w zmiennych podprocesu");
		
		
		Object doco = activityExecution.getVariable(WSSK_CN.NEWEE_PVAR_parentDoc);
		if (doco == null)
			edm("Brak okreslonego id dokumentu Lista dokumentow w zmiennych procesu");
		Long docID = (Long) doco;
		
		
		
		// 5 pol z uslugi do wpisania do UDO/UKS: ------------------------------------------------------
		// - pesel,
		// - tytul,
		// - nr uprawnien zawodowych,
		// - stanowisko(POSITION),
		// - miejsce pracy (WORKPLACE).
		// komorka organizacyjna - nie, bo idzie tylko do DSUser. Tu nie jest potrzebna i nie jest przesylana
		Object peselo = activityExecution.getVariable(WSSK_CN.NEWEE_PVAR_pesel);
		if (peselo == null)
			edm("Brak okre�lonego numeru pesel w zmiennych procesu");
		String pesel = (String) peselo;
		
		Object tytulo = activityExecution.getVariable(WSSK_CN.NEWEE_PVAR_tytul);
		if (tytulo == null)
			edm("Brak okre�lonego tytu�u w zmiennych procesu");
		String tytul = (String) tytulo;
		
		Object nrUprZawo = activityExecution.getVariable(WSSK_CN.NEWEE_PVAR_nrUprZaw);
		if (nrUprZawo == null)
			edm("Brak okre�lonego numeru uprawnie� zawodowych w zmiennych procesu");
		String nrUprZaw = (String) nrUprZawo;
		
		Object stanowiskoo = activityExecution.getVariable(WSSK_CN.NEWEE_PVAR_stanowisko);
		if (stanowiskoo == null)
			edm("Brak okre�lonego stanowiska w zmiennych procesu");
		String stanowisko = (String) stanowiskoo;
		
		Object miejscePracyo = activityExecution.getVariable(WSSK_CN.NEWEE_PVAR_miejscePracy);
		if (miejscePracyo == null)
			edm("Brak okre�lonego miejsca pracy w zmiennych procesu");
		String miejscePracy = (String) miejscePracyo;
		//-------------------------------------------------------------------------------------------

		try {
			// Utworzy nowy dokument z Upowaznieniem do przetwazania danych osobowych (U.D.O.)
			// Przesle dokument na liste ado
			String summary = "Dokument wygenerowany automatycznie do zatwierdzenia przez Administratora Danych Osobowych";
			OutOfficeDocument doc = OfficeUtils.newOutOfficeDocument(dvsn, ado_str, dvsn, ado_str, null, summary, true);
						
			doc.create();

			DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.WSSK_UDO);
			Map<String, Object> dockindKeys = new HashMap<String, Object>();
			dockindKeys.put("DOC_DESCRIPTION", summary);
			dockindKeys.put("DOC_DATE", new Date());

			doc.setDocumentKind(documentKind);
			Long newDocumentId = doc.getId();
			documentKind.set(newDocumentId, dockindKeys);

			doc.getDocumentKind().logic().archiveActions(doc, 0);
			doc.getDocumentKind().logic().documentPermissions(doc);
			doc.setAuthor(ado_str);
			
			
			// numer UDO - generacja
			// TODO : dokumenty UDO tworzone recznie nie beda mialy wygenerowanego numeru. Doda� to do logiki
			String nrUDO = newDocumentId +"/" + new Integer(GlobalPreferences.getCurrentYear()); // document_id / rok
			
			// wypelnienie pol dokumentu
			FieldsManager fm = doc.getFieldsManager();
			Map<String,Object> values = new HashMap<String,Object>();
			values.put("FIRSTNAME",	nowy.getFirstname());
			values.put("LASTNAME",	nowy.getLastname());
			values.put("CITY",		WSSK_CN.miejscowosc);
			values.put("DATEE",		formatter.format(new Date()) );
			values.put("POSITION",	stanowisko);		// stanowisko jest ze zmiennych procesu
			values.put("WORKPLACE",	miejscePracy);		// workplace jest ze zmiennych procesu
			values.put("NUMBEER",	nrUDO);				// nr UDO generowany przed chwila. NUMBEER to nazwa pola w DB

			fm.reloadValues(values);
			doc.getDocumentKind().setOnly(newDocumentId, values);
			DSApi.context().session().save(doc);
			
			//dziennik
			OfficeUtils.bindToJournal(doc, (ActionEvent) null, Journal.INTERNAL);
			
			//parammetry podprocesu
			String rootExecutionID = activityExecution.getId();
			if (rootExecutionID==null)
				throw new EdmException("Nie uda�o si� uzyska� ExecutionId - nie ma sposobu wskazania punktu powrotu.");

			//ZMIENNE PROCESU
			Map<String, Object> map = Maps.newHashMap();

			map.put(ASSIGN_USER_PARAM, ado_str);
			map.put(ASSIGN_DIVISION_GUID_PARAM, "rootdivision");
			map.put(WSSK_CN.NEWEE_PVAR_employee, nowy.getName()); //zapamietanie, kto jest tym uzytkownikiem, ktorego sprawa dotyczy
			map.put(WSSK_CN.NEWEE_PVAR_parentExecutionId, rootExecutionID); //zapamietanie procesu, do ktorego nalezy powrocic
			map.put(WSSK_CN.NEWEE_PVAR_parentDoc, docID); //zapamietanie dokumentu, wg ktorego nalezy aktualizowac listy zadan (bez tego "Lista dokumentow" nie wraca do zadan)
			map.put(WSSK_CN.NEWEE_PVAR_nrUDO,		nrUDO);			// zapamietanie wygenerowanego numeru UDO do uzycia dalej
			map.put(WSSK_CN.NEWEE_PVAR_stanowisko,	stanowisko);	// position jest ze zmiennych procesu
			map.put(WSSK_CN.NEWEE_PVAR_miejscePracy,miejscePracy);	// workplace jest ze zmiennych procesu
			map.put(WSSK_CN.NEWEE_PVAR_pesel,		pesel);			// pesel jest ze zmiennych procesu
			map.put(WSSK_CN.NEWEE_PVAR_tytul,		tytul);			// tytul jest ze zmiennych procesu
			map.put(WSSK_CN.NEWEE_PVAR_nrUprZaw,	nrUprZaw);		// numer uprawnien zawodowych jest ze zmiennych procesu
			
			doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(doc, map);

			TaskSnapshot.updateByDocument(doc);
			
			if (DSApi.isContextOpen() && !wasOpened)
				DSApi.context().commit();

		} catch (Exception exc) {
			edm("B��d podczas tworzenia dokumentu dla podprocesu i uruchomienia podprocesu obiegu U.D.O.: " + exc.getMessage());
		}
		finally {
			if (DSApi.isContextOpen() && !wasOpened)
				DSApi.close();
		}

		if (activityExecution.getActivity().getDefaultOutgoingTransition() != null)
			activityExecution.takeDefaultTransition();
	}
}
