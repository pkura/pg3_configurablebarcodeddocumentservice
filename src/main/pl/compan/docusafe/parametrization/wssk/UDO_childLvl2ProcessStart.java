package pl.compan.docusafe.parametrization.wssk;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/** custom activity, operacja utworzenia podprocesu w podprocesie.
 * Proces o zaglebieniu 2 ma poinformowac o swym zakonczeniu nie rodzica, tylko roota (poziom 0) */
public class UDO_childLvl2ProcessStart implements ExternalActivityBehaviour {
	
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(UDO_childLvl2ProcessStart.class);
	
	private static void edm (String s) throws EdmException {
		log.debug(s);
		throw new EdmException(s);
	}
	private static void err (String s) throws EdmException {
		log.error(s);
		throw new EdmException(s);
	}

	public void signal(ActivityExecution activityExecution, String s, Map<String, ?> stringMap) throws Exception {  }

	public void execute(ActivityExecution activityExecution) throws Exception {
		
		boolean wasOpened = true;
		try {
			wasOpened = DSApi.isContextOpen();
			if (!wasOpened) {
				DSApi.openAdmin();
				DSApi.context().begin();
			}
		} catch (Exception e) {
			err("B��d pod��czenia do bazy danych. " + e.getMessage());
			return;
		}
		
		// Znalezienie uzytkownika, ktorego sprawa dotyczy
		Object nowyo = activityExecution.getVariable(WSSK_CN.NEWEE_PVAR_employee);
		if (nowyo == null)
			edm("Brak okre�lonej nazwy nowego u�ytkownika w zmiennych podprocesu");
		String nowys = (String) nowyo;
		DSUser nowy = null;
		try {
			// odszukanie pracownika
			nowy = UserFactory.getInstance().findByUsername( nowys );
		} catch (UserNotFoundException unfe) {
			edm("Brak pracownika o nazwie okre�lonej w zmiennych podprocesu");
		} catch (EdmException exc) {
			edm("B��d podczas wyszukiwania u�ytkownika " + exc.getMessage());
		}
		if (nowy == null)
			edm("Brak pracownika o nazwie okre�lonej w zmiennych podprocesu");
		
		
		DSUser supervisor = nowy.getSupervisor();
		if (supervisor == null )
			edm("Nie znaleziono prze�o�onego u�ytkownika: " + nowy.getName());
		
		String superwajzor = supervisor.getName();
		
		// Znalezienie dzialu, do ktorego nalezy supervisor
		DSDivision division = null;
		try {
			division = GetterUtil.getReasonableDSDivisionOfUser(supervisor.getDivisions());
		} catch (Exception e){
			log.debug("prze�o�ony nie ma przypisanego �adnego dzia�u. Nie jest to b��d krytyczny dla procesu. "+e.getMessage());
		}
		String dvsn = division.getName();
		
		
		Object doco = activityExecution.getVariable(WSSK_CN.NEWEE_PVAR_parentDoc);
		if (doco == null)
			edm("Brak okre�lonego id dokumentu Lista dokument�w w zmiennych procesu");
		Long docID = (Long) doco;
		
		Object nrUDOo = activityExecution.getVariable(WSSK_CN.NEWEE_PVAR_nrUDO);
		if (nrUDOo == null)
			edm("Brak okre�lonego numeru U.D.O. w zmiennych procesu");
		String nrUDO = (String) nrUDOo;
		
		
		// 5 pol z uslugi do wpisania do UKS: ------------------------------------------------------
		// - pesel,
		// - tytul,
		// - nr uprawnien zawodowych,
		// - stanowisko(POSITION),
		// - miejsce pracy (WORKPLACE).
		// komorka organizacyjna - nie, bo idzie tylko do DSUser. Tu nie jest potrzebna i nie jest przesylana
		Object peselo = activityExecution.getVariable(WSSK_CN.NEWEE_PVAR_pesel);
		if (peselo == null)
			edm("Brak okre�lonego numeru pesel w zmiennych procesu");
		String pesel = (String) peselo;
		
		Object tytulo = activityExecution.getVariable(WSSK_CN.NEWEE_PVAR_tytul);
		if (tytulo == null)
			edm("Brak okre�lonego tytu�u w zmiennych procesu");
		String tytul = (String) tytulo;
		
		Object nrUprZawo = activityExecution.getVariable(WSSK_CN.NEWEE_PVAR_nrUprZaw);
		if (nrUprZawo == null)
			edm("Brak okre�lonego numeru uprawnie� zawodowych w zmiennych procesu");
		String nrUprZaw = (String) nrUprZawo;
		
		Object stanowiskoo = activityExecution.getVariable(WSSK_CN.NEWEE_PVAR_stanowisko);
		if (stanowiskoo == null)
			edm("Brak okre�lonego stanowiska w zmiennych procesu");
		String stanowisko = (String) stanowiskoo;
		
		Object miejscePracyo = activityExecution.getVariable(WSSK_CN.NEWEE_PVAR_miejscePracy);
		if (miejscePracyo == null)
			edm("Brak okre�lonego miejsca pracy w zmiennych procesu");
		String miejscePracy = (String) miejscePracyo;
		//-------------------------------------------------------------------------------------------
		
		try {
			// Tworzy nowy dokument - wniosek o utworzenie kont systemowych (UKS)
			// Przesle dokument na liste przelozonego nowego pracownika
			String summary = "Dokument wygenerowany automatycznie do zatwierdzenia i opisania przez prze�o�onego nowego pracownika";
			OutOfficeDocument doc = OfficeUtils.newOutOfficeDocument(dvsn, superwajzor, dvsn, superwajzor, null, summary, true);
						
			doc.create();

			DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.WSSK_UTWORZENIE_KONT_SYSTEMOWYCH);
			Map<String, Object> dockindKeys = new HashMap<String, Object>();
			dockindKeys.put("DOC_DESCRIPTION", summary);
			dockindKeys.put("DOC_DATE", new Date());

			doc.setDocumentKind(documentKind);
			Long newDocumentId = doc.getId();
			documentKind.set(newDocumentId, dockindKeys);

			doc.getDocumentKind().logic().archiveActions(doc, 0);
			doc.getDocumentKind().logic().documentPermissions(doc);
			doc.setAuthor(superwajzor);
			
			
			// wypelnienie pol dokumentu
			FieldsManager fm = doc.getFieldsManager();
			Map<String,Object> values = new HashMap<String,Object>();
			
//			FIRSTNAME		Imi�
//			LASTNAME		Nazwisko
//			POSITION		Stanowisko
//			--TITLE			Tytu�
//			WORKPLACE		Miejsce pracy
//			--PESEL			Pesel
//			--NR1			Numer prawa wykonywania zawodu
//			NR2				Numer upowa�nienia do przetwarzania danych
//			nie uwzgledniamy tu komorki organizacyjnej
			
			values.put("FIRSTNAME",	nowy.getFirstname());
			values.put("LASTNAME",	nowy.getLastname());
			values.put("POSITION",	stanowisko);			// position jest ze zmiennych procesu
			values.put("WORKPLACE",	miejscePracy);			// workplace jest ze zmiennych procesu
			values.put("PESEL",		pesel);					// pesel jest ze zmiennych procesu
			values.put("TITLE",		tytul);					// tytul jest ze zmiennych procesu
			values.put("NR1",		nrUprZaw);				// numer uprawnien zawodowych jest ze zmiennych procesu
			values.put("NR2",		nrUDO);					// numer UDO jest ze zmiennych procesu, generowany przez NewEmployee_ChildProcessStart
			values.put("LOGIN",		nowy.getName());

			fm.reloadValues(values);
			doc.getDocumentKind().setOnly(newDocumentId, values);
			DSApi.context().session().save(doc);
			
			
			//dziennik, nr KO
			OfficeUtils.bindToJournal(doc, (ActionEvent) null, Journal.INTERNAL);
			
			
			//parammetry podprocesu
			Object rootExecutionIDo = activityExecution.getVariable(WSSK_CN.NEWEE_PVAR_parentExecutionId);
			if (rootExecutionIDo==null)
				throw new EdmException("Nie uda�o si� uzyska� ExecutionId procesu nadrzednego - nie ma sposobu wskazania punktu powrotu.");

			Map<String, Object> map = Maps.newHashMap();

			map.put(ASSIGN_USER_PARAM, superwajzor);
			map.put(ASSIGN_DIVISION_GUID_PARAM, "rootdivision");
			map.put(WSSK_CN.NEWEE_PVAR_employee, nowy.getName()); //zapamietanie, kto jest tym uzytkownikiem, ktorego sprawa dotyczy
			map.put(WSSK_CN.NEWEE_PVAR_parentExecutionId, (String) rootExecutionIDo); //zapamietanie procesu, do ktorego nalezy powrocic
			map.put(WSSK_CN.NEWEE_PVAR_parentDoc, docID);		//zapamietanie dokumentu, wg ktorego nalezy aktualizowac listy zadan (bez tego "Lista dokumentow" nie wraca do zadan)
			map.put(WSSK_CN.NEWEE_PVAR_UDOid, newDocumentId);	//zapamietanie ID dokumentu UDO
			
			doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(doc, map);

			TaskSnapshot.updateByDocument(doc);
			
			if (DSApi.isContextOpen() && !wasOpened)
				DSApi.context().commit();

		} catch (Exception exc) {
			edm("B��d podczas tworzenia dokumentu dla podprocesu i uruchomienia podprocesu obiegu U.D.O.: " + exc.getMessage());
		}
		finally {
			if (DSApi.isContextOpen() && !wasOpened)
				DSApi.close();
		}

		if (activityExecution.getActivity().getDefaultOutgoingTransition() != null)
			activityExecution.takeDefaultTransition();
	}
	
	
}
