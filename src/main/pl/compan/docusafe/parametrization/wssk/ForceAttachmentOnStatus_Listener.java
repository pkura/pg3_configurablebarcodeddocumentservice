package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** Wymusza dodanie za��cznika przy zmianie stanu na podany
 * lub po prostu wymuszenie, gdy nie jest okre�lony stan.
 *  @author Rafal Odoj */
public class ForceAttachmentOnStatus_Listener extends AbstractEventListener{

	private static final long serialVersionUID = 1L;
	private final static Logger log = LoggerFactory.getLogger(ForceAttachmentOnStatus_Listener.class);

	/** tytu� za��cznika, kt�ry b�dzie sprawdzany/szukany.
	 * Je�li NULL, to nie ma wp�ywu - b�dzie sprawdzenie, czy istnieje jakikolwiek za��cznik */
    private String attachmentTitle;
    
    /** status na jakim ma by� wymuszone dodanie za��cznika.
     * Je�li NULL, to wymuszenie dodania niezale�nie od statusu */
    private String forceOnStatus;

    public void notify(EventListenerExecution eventListenerExecution) throws Exception {
        Long docId = Long.valueOf(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument document = OfficeDocument.find(docId);
        
        if (forceOnStatus != null){
        	String status = document.getFieldsManager().getStringKey(WSSK_CN.STATUSDOKUMENTU); //pobranie statusu
    		if( ! status.equals(forceOnStatus))
    			return; // wymaganie za��cznika tylko dla podanego statusu i to akurat nie ten status
        }
        
		// sprawdzic czy dodano zalacznik
        for(Attachment at: document.getAttachments())
            if((attachmentTitle== null && at != null) || (attachmentTitle != null && at.getTitle().equals(attachmentTitle)))
                return;
        
        String msg = "Brak wymaganego za��cznika";
       
        if(attachmentTitle != null)
        	msg += " - " + attachmentTitle;
        
        log.error(msg);
        throw new IllegalStateException(msg);
    }
}
