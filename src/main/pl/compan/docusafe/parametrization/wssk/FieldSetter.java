package pl.compan.docusafe.parametrization.wssk;

import java.util.Collections;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;

/** 
 * Zmienia pole w dokumencie (zwlaszcza status).
 * Dokladniej ustawia pole "field" na warto�� "value"
 * @author Rafal Odoj (dostosowanie FieldSetterListener)
 * @date 2012-08-30
 */
public class FieldSetter implements ExternalActivityBehaviour {
	
	private static final long serialVersionUID = 1L;
	
	private String field;
	private String value;

	@Override
	public void execute(ActivityExecution execution) throws Exception {
		
		boolean hasOpen = true;
		if (!DSApi.isContextOpen())
		{
			hasOpen = false;
			DSApi.openAdmin();
		}
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		doc.getDocumentKind().setOnly(docId, Collections.singletonMap(field, value));
		if (DSApi.isContextOpen() && !hasOpen)
			DSApi.close();
	}

	@Override
	public void signal(ActivityExecution execution, String signalName, Map<String, ?> arg2) throws Exception {
		if (signalName != null)
			execution.take(signalName);
		else 
			execution.takeDefaultTransition();
	}
}
