package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.activity.ActivityExecution;
import org.slf4j.LoggerFactory;

/** custom activity, wywoluje usluge ERP - wysyla dane pacjenta.
 * @author Rafal Odoj
 * @date 2012-09-18 */
public class ERPsend_PacjentNieubezp extends CustomBase {
	private static final long serialVersionUID = 1L;
	static {   log = LoggerFactory.getLogger(ERPsend_PacjentNieubezp.class);   }

	@Override
	protected void executionInsideContext(ActivityExecution execution) throws Exception {
		
		exception="B��d podczas wywo�ania us�ugi integracyjnej ERP celem wys�ania danych pacjenta nieubezpieczonego";
		
		
	}
}
