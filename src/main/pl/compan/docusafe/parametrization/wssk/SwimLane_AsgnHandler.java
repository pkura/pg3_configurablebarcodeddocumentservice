package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.OfficeDocument;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** Tworzy namiastke swimlane, bo brakuje tego w DocuSafe.
 * W miejscu, gdzie swimlane jest zakladany TRZEBA DODATKOWO uzyc
 * listenera SwimLaneInitiatorListener ON-EVENT="END".
 * Mozna tego uzywac wewnatrz forka.
 * Asygnacja dziala poprzez adds, czyli w polach user albo guid wpisujemy NAZWE zmiennej w ADDS!!! */
public class SwimLane_AsgnHandler implements AssignmentHandler {
private static final long serialVersionUID = 1L;
	
	protected String guid;
	protected String user;
	
	/** nazwa zmiennej procesu, pod jaka bedzie/byl zapamietany user, ktory przyjal/przyjmie zadanie */
	protected String swimlaneUser_var;
	
	/** status, ktory bedzie wpisany na sztywno do historii. Moze byc null. Uzywane wewnatrz forka. */
	protected String status;
	
	private static final Logger log = LoggerFactory.getLogger(SwimLane_AsgnHandler.class);

	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
	    	
		boolean isOpened = true;
		if (!DSApi.isContextOpen()) {
			isOpened = false;
			DSApi.openAdmin();
		}
		Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		
		Object swimlaneUser = openExecution.getVariable(swimlaneUser_var); //zwraca null gdy swimlaneUser_var==null, gdy nie ma takiego usera lub zmiennej
		
		
		if (swimlaneUser != null){
			
			//jest swimlaneUser, czyli proces juz tu raz byl i swimlane juz byl zainicjowany. Asygnacja na tegoz usera
			try {
				assignable.addCandidateUser(user);
				AHEUtils.addToHistory(openExecution, doc, (String)swimlaneUser, null, status, log);
			} catch (EdmException e) {
				log.error(e.getMessage());
				throw e;
			}
			finally {
				if (DSApi.isContextOpen() && !isOpened)
					DSApi.close();
			}
		} else {
			
			
			//nie ma swimlaneUser, wiec to pierwsze wejscie, asygnowac ogolnie
			try {
				if (guid != null) {
					for (String g : guid.split(",")) {
						String cel = Docusafe.getAdditionProperty(g);
						assignable.addCandidateGroup(cel);
						AHEUtils.addToHistory(openExecution, doc, null, cel, status, log);
					}
				} else if (user != null) {
					if (user.equals("author")) {
						assignable.addCandidateUser(doc.getAuthor());
						AHEUtils.addToHistory(openExecution, doc, doc.getAuthor(), null, status, log);
					} else {
						String cel = Docusafe.getAdditionProperty(user);
						assignable.addCandidateUser(cel);
						AHEUtils.addToHistory(openExecution, doc, cel, null, status, log);
					}
				}
			}
			catch (EdmException e) {
				log.error(e.getMessage());
				throw e;
			} finally {
				if (DSApi.isContextOpen() && !isOpened)
					DSApi.close();
			}
		}
	}
}
