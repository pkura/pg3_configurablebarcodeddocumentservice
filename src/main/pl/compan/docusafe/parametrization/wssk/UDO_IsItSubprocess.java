package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa podejmuje decyzje czy proces UDO jest uruchomiony przez czlowieka,
 * czy moze jest procesem podrzednym utworzonym automatycznie.
 * <ul> standalone = uruchomil czlowiek; </ul>
 * <ul> sub = uruchomiono automatycznie jako podproces. </ul>
 * Pewne zmienne powinny byc zdefiniowane przez nadproces.
 * Jesli ich brakuje to zakladamy, ze proces uruchomiono recznie.
 * @author Rafa� Odoj
 */
public class UDO_IsItSubprocess implements DecisionHandler {
	private static final long serialVersionUID = 1L;
	private final static Logger log = LoggerFactory.getLogger(UDO_IsItSubprocess.class);
	
	public String decide(OpenExecution openExecution) {
		String decision = "standalone";
		try {
			// te zmienne powinny byc zdefiniowane przez nadproces.
			// Jesli ich brakuje to jest to proces uruchomiony recznie
			Object emp   = openExecution.getVariable(WSSK_CN.NEWEE_PVAR_parentExecutionId);
			Object pexid = openExecution.getVariable(WSSK_CN.NEWEE_PVAR_employee);
			if ( (emp==null) && (pexid==null) )
				decision = "standalone";
			else
				decision = "sub";
		} catch (Exception e) {
			log.error("B��d okre�lania, czy proces uruchomiono automat. jako podrz�dny, czy r�cznie przez cz�owieka: ",e);
		}
		return decision;
	}
}
