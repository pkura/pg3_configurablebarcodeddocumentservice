package pl.compan.docusafe.parametrization.wssk;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

/** Logika procesu zgloszenia zapotrzebowania wewnetrznego.
 *  Pozwala na zapis nowego takiego dokumentu tylko w sytuacji, gdy 
 *  @author Rafa� Odoj
 *  @date 2012-07-30 */
public class DemandLogic extends AbstractDocumentLogic {
 
	private static final long serialVersionUID = 1L;
	private static DemandLogic instance;
 	protected static Logger log = LoggerFactory.getLogger(DemandLogic.class);
 
 	public static DemandLogic getInstance() {
 		if (instance == null)
 			instance = new DemandLogic();
 		return instance;
 	}
 
 	@Override
 	public void documentPermissions(Document document) throws EdmException {
 		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
 		
 		String kierownicy = Docusafe.getAdditionProperty("wssk.guid.Kierownicy");
 
		perms.add(new PermissionBean(ObjectPermission.READ,   kierownicy , ObjectPermission.GROUP));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, kierownicy , ObjectPermission.GROUP));
		perms.add(new PermissionBean(ObjectPermission.DELETE, kierownicy , ObjectPermission.GROUP));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, kierownicy , ObjectPermission.GROUP));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,   kierownicy , ObjectPermission.GROUP));
		
		perms.add(new PermissionBean(ObjectPermission.READ,"DOCUMENT_READ",ObjectPermission.GROUP,"Dokumenty - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"DOCUMENT_READ_ATT_READ",ObjectPermission.GROUP,"Dokumenty zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY,"DOCUMENT_READ_MODIFY",ObjectPermission.GROUP,"Dokumenty - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"DOCUMENT_READ_ATT_MODIFY",ObjectPermission.GROUP,"Dokumenty zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE,"DOCUMENT_READ_DELETE",ObjectPermission.GROUP,"Dokumenty - usuwanie"));
 
 		for (PermissionBean perm : perms) {
 			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP)) {
 				String groupName = perm.getGroupName();
 				if (groupName == null) {
 					groupName = perm.getSubject();
 				}
 				createOrFind(document, groupName, perm.getSubject());
 			}
 			DSApi.context().grant(document, perm);
 			DSApi.context().session().flush();
 		}
 	}
 
 	@Override
 	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
 		log.info("type: {}", type);
 		Map<String,Object> values = new HashMap<String,Object>();
 		values.put("TYPE", type);
 
 		fm.reloadValues(values);
 	}
 
 	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
 		as.useFolderResolver(document);
 		as.useAvailableProviders(document);
 		log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
 	}
 	
 	/** utworzyc dokument moze tylko osoba z grupy kierownicy. Nikt inny nie zapisze nowego dokumentu */
 	@Override
 	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException {
 		
 		OfficeDocument document = OfficeDocument.find(documentId);
 		String officeStatus = document.getOfficeStatus();
 		
 		if ( "new".equals(officeStatus) ) {
 			DSUser user = null;
 			
 			try {
 				user = DSUser.findByUsername(document.getCreatingUser());
 				
 			} catch (UserNotFoundException unfe) {
 				throw new EdmException ("Tylko u�ytkownik z grupy 'Kierownicy' mo�e zapisa� dokument tego typu", unfe);
 			}
 
 			DSDivision[] dzialy = user.getDivisions();
 			boolean kierownik = false;
 			for ( DSDivision dsd : dzialy)
 				if (Docusafe.getAdditionProperty("wssk.guid.Kierownicy").equals( dsd.getGuid() )) {
 					kierownik = true;
 					break;
 				}
 			
 			if ( ! kierownik )
 				throw new EdmException ("Tylko u�ytkownik z grupy 'Kierownicy' mo�e zapisa� dokument tego typu");
 		}
     }
 
 
 	@Override
 	public void onStartProcess(OfficeDocument document, ActionEvent event) {
 		log.info("--- DemandLogic : START PROCESS !!! ---- {}", event);
 		try {
 			Map<String, Object> map = Maps.newHashMap();
 			if (document instanceof InOfficeDocument) {
 				map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
 			}
 			else {
 				map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
 				map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
 			}
 			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
 			if (AvailabilityManager.isAvailable("addToWatch"))
 				DSApi.context().watch(URN.create(document));
 		} catch (Exception e) {
 			log.error(e.getMessage(), e);
 		}
 	}
 
 
 	@Override
 	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException {
 		ProcessCoordinator ret = new ProcessCoordinator();
 		ret.setUsername(document.getAuthor());
 		return ret;
 	}
 
 	@Override
 	public void onRejectedListener(Document doc) throws EdmException {
 		
 	}
 
 	@Override
 	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException {
 		
 	}
 }
