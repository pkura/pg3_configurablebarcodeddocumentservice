package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.activity.ActivityExecution;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;

/** custom activity, obsluguje zawilosci dla procesu Udostepnienia Dokumentacji Medycznej (wniosek zewnetrzny).
 * Zaklada teczke (jesli trzeba); zaklada sprawe (jesli trzeba); dodaje sprawe do teczki
 * i do niej dodaje wniosek i odpowiedz na wniosek.
 * przed guidem mozna dodac gwiazdke - to spowoduje wziecie guidu z adds.
 * W zmiennych procesu ma byc obecny id dokumentu wniosku.
 * Zmienne te to:
 * dok_WNSK_ID = id Wniosku o Udostepnienie Dokumentacji Medycznej
 * @author Rafal Odoj
 * @date 2012-09-19 */
public class ShareDocOut_FolderEtCase extends CustomBase {
	
	private static final long serialVersionUID = 1L;
	static {   log = LoggerFactory.getLogger(ShareDocOut_FolderEtCase.class);   }
	
	protected String rwaId;
	protected String guid;
	
	protected String officeFolderName;	//nazwa teczki
	protected String days;				//ilosc dni na zalatwienie sprawy

	@Override
	protected void executionInsideContext(ActivityExecution execution) throws Exception {
		
		exception="Błąd podczas obsługi sprawy/teczki wniosku o udostępnienie dokumentacji medycznej ";
		
		String guid_str = null;
		if (guid.startsWith("*"))
			guid_str = Docusafe.getAdditionProperty(guid.substring(1));
		else
			guid_str = guid;
		
		//teczka
		OfficeFolder ofisfold = OfficeUtils.newOfficeFolderIfNotExist(rwaId, guid_str, officeFolderName, days);
		
		// sprawa i dokumenty
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();

		//dane sprawy
		String nazwiskoImie = OfficeUtils.nazwisko_Imie(execution, doc, fm, WSSK_CN.SHODOC_nazwisko, WSSK_CN.SHODOC_imie);
		String tytul = "Udostępnienie Dokumentacji Medycznej";
		String opis = nazwiskoImie;
		Integer dni = OfficeUtils.parseInt_noExc(days); // null nie szkodzi, wywolana pozniej metoda da wartosc domyslna
		
		//sprawa
		OfficeCase sprawa = OfficeUtils.newOfficeCase(null, ofisfold, dni, tytul, opis, "admin"); // TODO : kto ma byc clerkiem
		if (sprawa == null)
			edm("nie utworzono odpowiedniej sprawy dla Udostępnienia Dokumentacji Medycznej następującej osoby: " +nazwiskoImie);

		// dodanie wniosku
		OfficeUtils.addDocToCase(sprawa, doc);
		
		// dodanie odpowiedzi do wniosku (wg zmiennej procesu)
		Object odp_id  = execution.getVariable(WSSK_CN.SHODOC_PVAR_odpowiedzId);
		if (odp_id==null)
			edm("Nie znaleziono id odpowiedzi w zmiennych procesu ");
		OfficeUtils.addDocToCase(sprawa, (Long)odp_id );
	}
}
