package pl.compan.docusafe.parametrization.wssk;

/** Interfejs trzymaj�cy sta�e (m.in. pami�tane na sztywno nazwy kodowe).
 * U�ywane w klasach logiki, listenerach i handlerach WSSK.
 * @author Rafal Odoj
 * @date 2012-08-01 */
public interface WSSK_CN {
	
	//pola ogolne
	/** pole z wybranym uzytkownikiem */	String DSUSER  = "DSUSER";
	/** pole z wybranym dzialem */			String DSDIVISION = "DSDIVISION";
	/** pole ze statusem dokumentu */		String STATUSDOKUMENTU = "STATUSDOKUMENTU";
	
	//pola i wartosci zwi�zane ze wznawianiem
	/** pole robocze do obs�ugi wznowienia*/	String MARKERWZNOWIENIA = "MARKERWZNOWIENIA";
	/** ci�g w URL kt. �wiadczy o wznawianiu*/	String REOPENWF_INURL = "ReopenWf";
	
	/** marker: dok. jest w�a�nie wznawiany*/	int WZNAWIANY = 1;
	
	/** marker: dok. nie jest teraz wznawiany.
	 * Pr�cz tego marker mo�e by� NULL, co oznacza,
	 * �e dokument nie jest OBECNIE i NIGDY nie by� wznawiany */
												int STAN_NORMALNY = 0;
	
	/** nazwa zmiennej procesu wskazuj�cej warto�ci� WSSK_CN.dwrTrue
	 * �e proces jest ponownie uruchomiony */	String VAR_ProcReopened = "ProcessIsReopened";
	
	/** activity, gdy proces czeka na edycj� przy wznowieniu (STATE czekaj) */
	 											String reopenWaitState = "czekaj";
	
	//wartosci ogolne
	/** boolean - warto�� w DWR symbolizuj�ca TRUE */
											String dwrTrue = "tak";
	/** miejscowosc - Wroc�aw (dla UDO) */	String miejscowosc = "Wroc�aw"; //uzywane w UDO (Upowaznienie do przetwarzania Danych Osobowych)
	
	//STMP = Stamp, Piecz�tka
	String STMP_statusProc = "STATUSPROCESU";
	String STMP_typWniosku = "TYPWNIOSKU";
	String STMP_imie       = "IMIE";
	String STMP_nazwisko   = "NAZWISKO";
	String STMP_dataOdb    = "DATAODB";
	
	/** status dokumentu - zaakceptowana */	String STMP_STAT_2     = "2";
	/** status dokumentu - odrzucono*/		String STMP_STAT_3     = "3";
	/** status dokumentu - do wydania */	String STMP_STAT_4     = "4";
	/** status dokumentu - wydano */		String STMP_STAT_5     = "5";
	/** status dokumentu - wydano */		int    STMP_STAT_5int  =  5;
	
	//DMND = Demand, Zapotrzebowanie
	
	//CRSYS = Create System Account, Utworzenie Kont Systemowych, UKS
	//pola dokumentu
	String CRSYS_nazwisko = "LASTNAME";
	String CRSYS_imie     = "FIRSTNAME";
	String CRSYS_pesel    = "PESEL";
	String CRSYS_login    = "LOGIN";
	
	/** status dokumentu - zakonczono */	String CRSYS_STAT_6     = "6";
	/** status dokumentu - do wznowienia */	String CRSYS_STAT_7     = "7";
	
	//DOKLS = DOK_LISTA, Potwierdzenie zapoznania z dokumentami
	String DOKLS_check = "CHECK"; //nazwa checkboxow (po niej doklejana juz tylko liczba)
	int DOKLS_checkNo = 7; //ilosc checkboxow
	
	//NEWEE = NEW EMPLOYEE, Nowy Pracownik
	/** zmienna proc - nazwa nowego pracownika */	String NEWEE_PVAR_employee = "empl_name";
	/** zmienna proc - id nadprocesu */				String NEWEE_PVAR_parentExecutionId = "PARENT_EXECUTION_ID";
	/** zmienna proc - id dokumentu nadprocesu */	String NEWEE_PVAR_parentDoc = "PARENT_DOC_ID";
	/** zmienna proc - numer UDO */					String NEWEE_PVAR_nrUDO = "NR_UDO";
	/** zmienna proc - id dokumentu UDO */			String NEWEE_PVAR_UDOid = "UDOid";
	                 
	/** zmienna proc - stanowisko, do UDO i UKS */	String NEWEE_PVAR_stanowisko = "stanowisko";
	/** zmienna proc - miejsce pracy, do UDO i UKS */String NEWEE_PVAR_miejscePracy = "miejscePracy";
	/** zmienna proc - kom.org., do UDO i UKS */		String NEWEE_PVAR_komorkaOrganiz = "komorkaOrganizacyjna";
	/** zmienna proc - pesel do wpisania do UKS */	String NEWEE_PVAR_pesel = "pesel";
	/** zmienna proc - tytul do wpisania do UKS */	String NEWEE_PVAR_tytul = "tytul";
	/** zmienna proc - nr uprawnien do wykonywania zawodu, do wpisania do UKS */
					 								String NEWEE_PVAR_nrUprZaw = "nrUprZaw";
	
	//SHODOC = SHARE_DOC_OUT, Wydanie Dokumentacji (Zewnetrzne)
	/** zmienna proc - id nadprocesu */				String SHODOC_PVAR_parentExecutionId = "PARENT_EXECUTION_ID";
	/** zmienna proc - id dokumentu nadprocesu */	String SHODOC_PVAR_parentDoc = "PARENT_DOC_ID";
	/** zmienna proc - id dokumentu wniosku */		String SHODOC_PVAR_wniosekId = "WNIOSEK_ID";
	/** zmienna proc - id dokumentu odpowiedzi */	String SHODOC_PVAR_odpowiedzId = "ODPOWIEDZ_ID";
	//pola dokumentu
	String SHODOC_nazwisko = "LASTNAME";
	String SHODOC_imie     = "FIRSTNAME";
	String SHODOC_pesel    = "PESEL";
	String SHODOC_dataOd   = "DATEFROM";
	String SHODOC_dataDo   = "DATETO";
	
	
	//INVC = INVOICE, Faktura z opisem MPK-�w
	/** zmienna proc: kolejny akceptuj�cy user */					String INVC_PVAR_userAccept = "userAccept";
	/** zmienna proc: kolejny akceptuj�cy dzia�, nie u�ywane */		String INVC_PVAR_guidAccept = "guidAccept";
	/** zmienna proc: wynik akceptacji (akceptacja/odrzucenie) */	String INVC_PVAR_acceptStstus = "acceptStatus";
	/** zmienna proc: data akceptacji */ 							String INVC_PVAR_acceptDate = "acceptDate";
	
	
	
	
	
}


