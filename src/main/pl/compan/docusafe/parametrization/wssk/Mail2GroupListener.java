package pl.compan.docusafe.parametrization.wssk;

import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import org.jbpm.api.listener.EventListenerExecution;
import org.jbpm.api.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.service.mail.MailerDriver;

/**
 * Kopia <code>SendMailListener</code>
 * poddana okrojeniu i z poprawionym bledem zamykania kontekstu gdy nie byl otwierany w tej metodzie.
 * user i guid moga byc podane z gwiazdka na przodzie - beda potraktowane jako nazwa adds.
 * @author Rafal Odoj (tylko dostosowanie)
 */
public class Mail2GroupListener  extends AbstractEventListener{
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(Mail2GroupListener.class);

	private String user;
	private String guid;
	private String mail;
	private String template;
	private String realization_time_m;
	private String realization_time_h;
	private String realization_time_d;
	private String realization_time_M;
	private String realization_time_y;

	@Override
	public void notify(EventListenerExecution execution) throws Exception
	{
		boolean wasOpened = true;
		try {
			wasOpened = DSApi.isContextOpen();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return;
		}
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		try {
			if (!wasOpened)
				DSApi.openAdmin();

			if (template == null) {
				log.error("Nie podano szablonu maila!");
				throw new EdmException("Nie podano szablonu maila!");
			}
			OfficeDocument doc = OfficeDocument.find(docId);

			MailerDriver mailer = (MailerDriver) ServiceManager.getService(Mailer.NAME);
			mailer.setFromEmail("docusafe@nie.odpowiadaj.nic"); // TODO : czy to jest potrzebne gdy DocuSafe jest w pelni skonfigurowany???
												// mozna pobrac adres aktualnie zalogowanej osoby: DSApi.context().getDSUser().getEmail() 

			if (mail != null) {
				mailer.send(mail, mail, null, Configuration.getMail(template), prepareContext(doc.getFieldsManager(), null, execution), false);
			} else if (user != null) {
				String user_str=null;
				if (user.equals("author"))
					mail = DSUser.findByUsername(doc.getAuthor()).getEmail();
				else {
					if (user.startsWith("*"))
						user_str = Docusafe.getAdditionProperty(user.substring(1));
					else
						user_str = user;
					mail = DSUser.findByUsername(user_str).getEmail();
				}	

				mailer.send(mail, mail, null, Configuration.getMail(template), prepareContext(doc.getFieldsManager(), DSUser.findByUsername(user_str), execution), false);
			}
			else if (guid != null) {
				String guid_str=null;
				if (guid.startsWith("*"))
					guid_str = Docusafe.getAdditionProperty(guid.substring(1));
				else
					guid_str = guid;
				
				for (DSUser user : Arrays.asList(UserFactory.getInstance().findDivision(guid_str).getUsers()))
					mailer.send(user.getEmail(), user.getEmail(), null, Configuration.getMail(template), prepareContext(doc.getFieldsManager(), user, execution), false);
			}
		}
		catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		finally {
			if (DSApi.isContextOpen() && !wasOpened)
				DSApi.close();
		}
	}

	private Map<String, Object> prepareContext(FieldsManager fm, DSUser user, EventListenerExecution execution) throws UserNotFoundException, EdmException {
		Map<String, Object> ctext = new HashMap<String, Object>();
		if (user != null) {
			ctext.put("first-name", user.getFirstname());
			ctext.put("last-name", user.getLastname());
		}
		ctext.put("document-name", fm.getDocumentKind().getName());
		ctext.put("document-id", fm.getDocumentId());
		ctext.put("document-state", "stan nieokreslony");  // TODO : wczesniej bylo jeszcze gorzej, bo ustalano stan nie zwazajac na fork

		for (String taskId : Jbpm4ProcessLocator.taskIds(fm.getDocumentId())) {
			Task task = Jbpm4Provider.getInstance().getTaskService().getTask(taskId);
			if (execution.getId().equals(task.getExecutionId())) {
				GregorianCalendar gregorianCalendar = new GregorianCalendar();
				gregorianCalendar.setTime(task.getCreateTime());

				if (realization_time_y != null) {
					gregorianCalendar.add(Calendar.YEAR, Integer.valueOf(realization_time_y));
					ctext.put("document-state-realization-time", realization_time_y + " lat");
				}
				else if (realization_time_M != null) {
					gregorianCalendar.add(Calendar.MONTH, Integer.valueOf(realization_time_M));
					ctext.put("document-state-realization-time", realization_time_M + " miesiecy");	
				}
				else if (realization_time_d != null) {
					gregorianCalendar.add(Calendar.DAY_OF_MONTH, Integer.valueOf(realization_time_d));
					ctext.put("document-state-realization-time", realization_time_d + " dni");	
				}
				else if (realization_time_h != null) {
					gregorianCalendar.add(Calendar.HOUR, Integer.valueOf(realization_time_h));
					ctext.put("document-state-realization-time", realization_time_h + " godzin");	
				}
				else if (realization_time_m != null) {
					gregorianCalendar.add(Calendar.MINUTE, Integer.valueOf(realization_time_m));
					ctext.put("document-state-realization-time", realization_time_m + " minut");	
				}
//
//				ctext.put("document-deadline", gregorianCalendar.getTime().toLocaleString());
			}
		}
		Map<String, Object> values = fm.getFieldValues();

		for(String key : values.keySet())
			ctext.put(key, values.get(key));

		return ctext;
	}


}
