package pl.compan.docusafe.parametrization.wssk;

import java.util.HashMap;
import java.util.Map;

/** 
 * Zmienia pole w dokumencie - ustawia pole "field" na warto�� "value".
 * Wywoluje przy tym specjalizowana metode, ktora zapewnia typ boolean.
 * wartosci: true/1/tak/yes oraz false/0/nie/no.
 * Brak wartosci oraz wartosc nieznana daja wynik false.
 * @author Rafal Odoj
 * @date 2012-08-30
 */
public class FieldSetterBoolean extends FieldSetterBase {
	private static final long serialVersionUID = 1L;

	@Override
	protected Map<String, ?> setter(){
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		boolean bit = false;
		
		if (value != null){
			if ("true".equalsIgnoreCase(value) || "1".equals(value) || "tak".equalsIgnoreCase(value) || "yes".equalsIgnoreCase(value)  )
				bit=true;
			if ("false".equalsIgnoreCase(value) || "0".equals(value) || "nie".equalsIgnoreCase(value) || "no".equalsIgnoreCase(value)  )
				bit=false;
		}
		map.put(field, bit);
		return map;
	}
}
