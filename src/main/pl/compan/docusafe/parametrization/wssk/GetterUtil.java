package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.activity.ActivityExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.ReferenceToHimselfException;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;


import org.slf4j.Logger;

/** klasa utylitarna sluzaca do upraszczania zapisu w kodzie.
 * Zawiera rozne metody do pobierania typowych obiektow w procesach
 * @author Rafal Odoj
 * @date 2012-09-07 */
public class GetterUtil {

	/** wyszukuje uzytkownika po jego nazwie (loginie) i zwraca obiekt DSUser */
	public static DSUser dSUserByUserName(String username, Logger log) throws Exception{
		if ( ! OfficeUtils.isValidString(username) )
			edm(log, "Brak wymaganej nazwy uzytkownika");
		
		DSUser user = null;
		try {
			user = UserFactory.getInstance().findByUsername( username );
		} catch (UserNotFoundException unfe) {
			edm(log, "Brak uzytkownika " +username);
		} catch (EdmException exc) {
			edm(log, "B��d podczas wyszukiwania u�ytkownika: " + exc.getMessage());
		}
		if (user == null)
			edm(log, "Brak uzytkownika " +username);
		return user;
	}
	
	/** wyszukuje uzytkownika po jego nazwie (loginie) zawartym w zmiennej procesu i zwraca obiekt DSUser */
	public static DSUser dSUserByUserNameFromProcVariable(String varname, ActivityExecution execution, Logger log) throws Exception {
		if ( ! OfficeUtils.isValidString(varname) )
			edm(log, "Brak wymaganej nazwy zmiennej procesu");
		
		Object usero = execution.getVariable(WSSK_CN.NEWEE_PVAR_employee);
		if (usero == null)
			edm(log, "Brak nazwy u�ytkownika w zmiennej podprocesu o nazwie "+varname);
		String username = (String) usero;
		DSUser user = null;
		try {
			user = UserFactory.getInstance().findByUsername( username );
		} catch (UserNotFoundException unfe) {
			edm(log, "Brak uzytkownika " +username);
		} catch (EdmException exc) {
			edm(log, "B��d podczas wyszukiwania u�ytkownika: " + exc.getMessage());
		}
		if (user == null)
			edm(log, "Brak uzytkownika " +username);
		return user;
	}
	
	/** pobiera zmienna procesu i zamienia ja na Long */
	public static Long documentIdByProcVariable(String varname, ActivityExecution execution, Logger log) throws Exception {
		if ( ! OfficeUtils.isValidString(varname) )
			edm(log, "Brak wymaganej nazwy zmiennej procesu");
		
		Object docido = execution.getVariable(WSSK_CN.NEWEE_PVAR_parentDoc);
		if (docido == null)
			edm(log, "Brak okreslonego id dokumentu w zmienej procesu o nazwie "+varname);
		try {
			return (Long) docido;
		} catch (Exception e){
			try {
				return Long.parseLong( (String) docido );
			} catch (Exception ee){
				edm(log, "Zawarto�� zmiennej procesu o nazwie " +varname +" nie daje si� konwertowa� na id dokumentu (liczb�)");
				return -1L;
			}
		}
	}
	
	/** Wybiera i zwraca sensowny guid (reprezentatywny guid usera)
	 * z tablicy obiekt�w DSDivision.
	 * @throws EdmException 
	 * @throws ReferenceToHimselfException 
	 * @throws DivisionNotFoundException */
	public static String getReasonableGuidOfUser(DSDivision[] divisions)
	throws DivisionNotFoundException, ReferenceToHimselfException, EdmException {
		if (divisions == null)
			return DSDivision.ROOT_GUID;
		
		String userDivisionGuid = null;
		if (divisions.length > 0)
			for (int i = 0; i < divisions.length; i++)
				if (divisions[i].isPosition()) {
					userDivisionGuid = divisions[i].getParent().getGuid();
					break;
				} else if (divisions[i].isDivision()) {
					userDivisionGuid = divisions[i].getGuid();
					break;
				}
		
		if (userDivisionGuid == null)
			return DSDivision.ROOT_GUID;
		else
			return userDivisionGuid;
	}
	
	/** Wybiera i zwraca sensowny DSDivision (reprezentatywny dla usera)
	 * z tablicy obiekt�w DSDivision.
	 * @throws EdmException 
	 * @throws ReferenceToHimselfException 
	 * @throws DivisionNotFoundException */
	public static DSDivision getReasonableDSDivisionOfUser(DSDivision[] divisions)
	throws DivisionNotFoundException, ReferenceToHimselfException, EdmException {
		if (divisions == null)
			return DSDivision.findByName(DSDivision.ROOT_GUID);
		
		DSDivision userDivision = null;
		if (divisions.length > 0)
			for (int i = 0; i < divisions.length; i++)
				if (divisions[i].isPosition()) {
					userDivision = divisions[i].getParent();
					break;
				} else if (divisions[i].isDivision()) {
					userDivision = divisions[i];
					break;
				}
		
		if (userDivision == null)
			return DSDivision.findByName(DSDivision.ROOT_GUID);
		else
			return userDivision;
	}
	
	protected static void edm (Logger log, String s) throws EdmException {
		log.debug(s);
		throw new EdmException(s);
	}

	protected static void err (Logger log, String s) throws EdmException {
		log.error(s);
		throw new EdmException(s);
	}
}
