package pl.compan.docusafe.parametrization.wssk;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.office.OfficeDocument;

/** Implementuj� to logiki dokument�w, kt�re wymagaj�
 * ponownego wej�cia do procesu w innym celu, ni� pierwotny.
 * Napisane dla WSSK, gdzie procesy musz� by� po zako�czeniu wznawiane w dziwaczny spos�b.
 * <ul> np. najpierw zam�wienie piecz�tki, a potem zwrot na tym samym formularzu jako proces. </ul>
 * <ul> albo np. utworzenie kont systemowych: po realizacji proces mo�e by� ponownie otwarty,
 * zmodyfikowany i puszczony w obieg jeszcze raz. </ul>
 * 
 * <p> Rozszerzenie pozwala zr�nicowa� spos�b zachowania przy wznawianiu procesu.
 * Rozszerzona logika umo�liwia edycj� "in place" wznawianego dokumentu przez osob� wznawiaj�c�.
 * Technicznie: dokument trafia od razu na list� zada� 1. uczestnika procesu, ale wznawiaj�cy (ju� po tym fakcie!!!)
 * mo�e go przez chwil� modyfikowa� i zapisa� - nie najszcz�liwsze rozwi�zanie, ale nie widz� lepszego.
 * @author Rafal Odoj
 * @date 2012-10-08 */
public interface LogicExtension_Reopen {
	
	/** Odpowiada na pytanie, czy dokument mo�na teraz przepu�ci� ponownie przez proces. */
	public boolean canReopenNow();
	
	/** wykonuje czynno�ci dodatkowe przy wznowieniu procesu, np. modyfikuje warto�ci p�l.
	 * Na razie nie u�ywane. */
	public void reopenNow (OfficeDocument doc) throws EdmException;
	
	/** ustawia w DB marker DO WZNOWIENIA jako ustawiony. */
	public void setReopenMarkerOn (OfficeDocument doc) throws EdmException;
	
	/** Metoda z za�o�enia powoduje efekty uboczne, tj. modyfikuje swoje argumenty.
	 * Ma zapewni�, �eby w chwili wznowienia procesu DWR nie przesy�a� warto�ci z formatki, tylko pobiera� je z bazy.
	 * W tym czasie w bazie ma by� ju� wpisany marker DO WZNOWIENIA tak,
	 * by regu�y Drools mog�y uczyni� formatk� jako do zapisu.
	 * 
	 * <pre> Metoda mo�e korzysta� z nast�puj�cych wywo�a�:
	 * dwrFacade.setFieldValues(...)
	 * dwrFacade.getFieldValues()
	 * dwrSessionValues.get("activity")
	 * dwrSessionValues.get("dockindKind")
	 * dwrSessionValues.get("FIELDS_VALUES")
	 * fieldsManager.reloadValues(...)
	 * request.getAttribute("org.directwebremoting.dwrp.batch").getPage() </pre>
	 * 
	 * Podczas jednego ��dania DWR b�dzie wywo�ana 1, 2 lub 3 razy.
	 * Wymaga otwartego kontekstu, bo odwo�uje si� do DB. */
	public void modifyDwrFacade(
			HttpServletRequest req,
			Map<String, Object> dwrSessionValues,
			FieldsManager fieldsManager,
			DwrFacade dwrfacade)
			throws EdmException ;
	
}
