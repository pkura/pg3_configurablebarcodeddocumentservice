package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.activity.ActivityExecution;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;

/** Custom activity, do wywo�ania koniecznie na samym pocz�tku procesu.
 * Sprawdza, czy status procesu wynosi NULL.
 * 
 * <ul> jesli nie, ustawia zmienn� procesu
 * o nazwie spod WSSK_CN.VAR_ProcReopened na WSSK_CN.dwrTrue.
 * Czyli obecnie ProcessIsReopened="tak" </ul>
 * 
 * <ul>jesli tak, nic nie robi (w szczeg�lno�ci nie tworzy pustej zmiennej ani jej nie czy�ci).</ul>
 * 
 * Tak wi�c brak zmiennej, lub inna warto�� oznaczaj�, �e to proces nowy.
 * Dzia�anie opiera si� na za�o�eniu, �e w chwili tworzenia nowego procesu (i tylko wtedy)
 * STATUSDOKUMENTU jest null, bo jest ustawiany listenerem przy pierwszym ludzkim tasku.
 * <p>
 * Raczej nie mo�na do tego celu u�y� pola MARKERWZNOWIENIA,
 * bo jest on na razie (2012-10-12) ustawiany PO faktycznym wznowieniu
 * i doj�ciu do pierwszego ludzkiego tasku.
 * @author Rafal Odoj
 * @date 2012-10-12  */
public class Reopen_CheckProcessNew extends CustomBase {

	private static final long serialVersionUID = 1L;
	static {   log = LoggerFactory.getLogger(Reopen_CheckProcessNew.class);   }
	
	@Override
	protected void executionInsideContext(ActivityExecution execution) throws Exception {
		
		exception="B��d okre�lania procesu jako nowy/wznowiony";
		failureAsError = true;
		
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument document = OfficeDocument.find(docId);
		FieldsManager fm = document.getFieldsManager();
		
		Object status = fm.getKey(WSSK_CN.STATUSDOKUMENTU);
		if (status!=null)
			execution.setVariable(WSSK_CN.VAR_ProcReopened, WSSK_CN.dwrTrue);
		
	}
}
