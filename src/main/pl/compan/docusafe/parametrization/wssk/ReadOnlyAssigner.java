package pl.compan.docusafe.parametrization.wssk;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Map;

/** Tworzy proces do wiadomo�ci i wysy�a go do u�ytkownika.
 * Pola user i guid wskazuj� na nazw� sta�ej z pliku ADDS!
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>, przyci�cie i dostosowanie - Rafal Odoj */
public class ReadOnlyAssigner implements ExternalActivityBehaviour {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ReadOnlyAssigner.class);

	private String user;
	private String guid;

	public void signal(ActivityExecution activityExecution, String s, Map<String, ?> stringMap) throws Exception { }

	public void execute(ActivityExecution activityExecution) throws Exception {
		long documentId = (Long) activityExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY);
		OfficeDocument od = OfficeDocument.find(documentId);
		
		// guid z XML
		if (StringUtils.isNotBlank(guid)) {
			LinkedList<String> guidAsList = new LinkedList<String>();
			for (String g : guid.split(",")) {
				String cel = Docusafe.getAdditionProperty(g);
				guidAsList.add(cel);
			}
			od.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(od, Collections.<String, Object> singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNMENT_GUIDS, guidAsList));
		}
		// user z XML
		else if (user!=null) {
			if (user.equals("author"))
				od.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(od, Collections.<String, Object> singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY, od.getAuthor()));
			else {
				String cel = Docusafe.getAdditionProperty(user);
				od.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(od, Collections.<String, Object> singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY, cel));
			}
		}

		if (activityExecution.getActivity().getDefaultOutgoingTransition() != null)
			activityExecution.takeDefaultTransition();
	}
}
