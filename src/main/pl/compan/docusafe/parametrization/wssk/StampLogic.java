package pl.compan.docusafe.parametrization.wssk;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.util.HashMap;
import java.util.Map;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

/**
 *  Logika procesu wydawania pieczatki
 *  @author Rafa� Odoj
 */
public class StampLogic extends AbstractDocumentLogic {

	private static final long serialVersionUID = 1L;
	private static StampLogic instance;
	protected static Logger log = LoggerFactory.getLogger(StampLogic.class);

	public static StampLogic getInstance() {
		if (instance == null)
			instance = new StampLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk�y- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk�y - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk�y - usuwanie"));

		for (PermissionBean perm : perms) {
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP)) {
				String groupName = perm.getGroupName();
				if (groupName == null) {
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
		log.info("type: {}", type);
		Map<String,Object> values = new HashMap<String,Object>();
		values.put("TYPE", type);
		fm.reloadValues(values);
	}

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
		as.useFolderResolver(document);
		as.useAvailableProviders(document);
		log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
	}

	private String attachmentTitle;

	@Override
	public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException {
		if (documentId == null) return;

		OfficeDocument document = OfficeDocument.find(documentId);

		Integer status=null;
		if (values.containsKey(WSSK_CN.STATUSDOKUMENTU)){
			status = (Integer) values.get(WSSK_CN.STATUSDOKUMENTU);
		}
		if( status != null )
			if ( ! ( WSSK_CN.STMP_STAT_5int == status) )
				return; //w przypadkach innych niz "wydano" nie ma co sprawdzac


		//imie, nazwisko, data
		if (! values.containsKey(WSSK_CN.STMP_imie))
			throw new EdmException("brak podanego imienia");
		if ( (values.get(WSSK_CN.STMP_imie)==null) || ("".equals(values.get(WSSK_CN.STMP_imie))))
			throw new EdmException("brak podanego imienia");
		if (! values.containsKey(WSSK_CN.STMP_nazwisko))
			throw new EdmException("brak podanego nazwiska");
		if ( (values.get(WSSK_CN.STMP_nazwisko)==null) || ("".equals(values.get(WSSK_CN.STMP_nazwisko))))
			throw new EdmException("brak podanego nazwiska");
		if (! values.containsKey(WSSK_CN.STMP_dataOdb))
			throw new EdmException("brak podanej daty odbioru");
		if ( (values.get(WSSK_CN.STMP_dataOdb)==null) || ("".equals(values.get(WSSK_CN.STMP_dataOdb))))
			throw new EdmException("brak podanej daty odbioru");
		if (! values.containsKey(WSSK_CN.STMP_typWniosku))
			throw new EdmException("brak podanego rodzaju wniosku (piecz�tki)");
		if ( (values.get(WSSK_CN.STMP_typWniosku)==null) || ("".equals(values.get(WSSK_CN.STMP_typWniosku))))
			throw new EdmException("brak podanego rodzaju wniosku (piecz�tki)");

		//wydano pieczatke, sprawdzic zalacznik
		for(Attachment at: document.getAttachments()){
			if((attachmentTitle== null && at != null) || (attachmentTitle != null && at.getTitle().equals(attachmentTitle))){
				return;
			}
		}
		String msg = "Nie mo�na ustawi� statusu 'wydano' w przypadku kiedy nie zosta� dodany odpowiedni za��cznik";
		if(attachmentTitle != null)
			msg += "- " + attachmentTitle;

		throw new EdmException(msg);
	}


	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) {
		log.info("--- StampLogic : START PROCESS !!! ---- {}", event);
		try {
			Map<String, Object> map = Maps.newHashMap();
			if (document instanceof InOfficeDocument) {
				map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
			}
			else {
				map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
				map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			}
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}


	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException {
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	@Override
	public void onRejectedListener(Document doc) throws EdmException { }

	@Override
	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException { }
}
