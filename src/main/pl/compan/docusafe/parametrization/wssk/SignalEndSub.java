package pl.compan.docusafe.parametrization.wssk;

import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;

/** 
 * Wysyla okreslony sygnal do procesu nadrzednego.
 * @author Rafal Odoj
 * @date 2012-08-28
 */
public class SignalEndSub implements ExternalActivityBehaviour {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(SignalEndSub.class);
	
	private String signal;

	@Override
	public void execute(ActivityExecution execution) throws Exception {
		Object parentExecutionId = execution.getVariable(WSSK_CN.NEWEE_PVAR_parentExecutionId);

		try {
			if (parentExecutionId != null)
				Jbpm4Provider.getInstance().getExecutionService().signalExecutionById((String) parentExecutionId, signal);

			execution.takeDefaultTransition();
		} catch (Exception e) {
			err("B��d podczas przesy�ania sygna�u do innego procesu" + e.getMessage());
		}
	}

	@Override
	public void signal(ActivityExecution execution, String signalName, Map<String, ?> arg2) throws Exception {
		if (signalName != null)
			execution.take(signalName);
		else 
			execution.takeDefaultTransition();
	}
	
	/** error do logu i wyjatek Edm */
	private static void err (String s) throws EdmException {
		log.error(s);
		throw new EdmException(s);
	}
}
