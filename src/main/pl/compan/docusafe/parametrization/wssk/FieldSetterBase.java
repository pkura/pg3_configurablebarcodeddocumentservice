package pl.compan.docusafe.parametrization.wssk;

import java.util.Collections;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;

/** 
 * Zmienia pole w dokumencie (zwlaszcza status).
 * Dokladniej ustawia pole "field" na warto�� "value".
 * Wywoluje przy tym specjalizowana metode, ktora zapewnia odpowiedni typ.
 * W podklasach robic Override metody setter.
 * @author Rafal Odoj (dostosowanie FieldSetterListener)
 * @date 2012-08-30
 */
public class FieldSetterBase implements ExternalActivityBehaviour {
	
	private static final long serialVersionUID = 1L;
	
	protected String field;
	protected String value;

	@Override
	public void execute(ActivityExecution execution) throws Exception {
		
		boolean hasOpen = true;
		if (!DSApi.isContextOpen())
		{
			hasOpen = false;
			DSApi.openAdmin();
		}
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		
		Map<String, ?> values = setter();
		
		doc.getDocumentKind().setOnly(docId, values);
		if (DSApi.isContextOpen() && !hasOpen)
			DSApi.close();
	}

	protected Map<String, ?> setter(){
		return Collections.singletonMap(field, Byte.parseByte(value));
	}
	
	@Override
	public void signal(ActivityExecution execution, String signalName, Map<String, ?> arg2) throws Exception {
		if (signalName != null)
			execution.take(signalName);
		else 
			execution.takeDefaultTransition();
	}
}
