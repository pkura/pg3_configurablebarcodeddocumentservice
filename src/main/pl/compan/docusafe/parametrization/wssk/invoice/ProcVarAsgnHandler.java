package pl.compan.docusafe.parametrization.wssk.invoice;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.wssk.AHEUtils;
import pl.compan.docusafe.parametrization.wssk.WSSK_CN;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

@SuppressWarnings("serial")
public class ProcVarAsgnHandler implements AssignmentHandler {
	private static final Logger log = LoggerFactory.getLogger(ProcVarAsgnHandler.class);

	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
	    
		boolean isOpened = true;
		if (!DSApi.isContextOpen()) {
			isOpened = false;
			DSApi.openAdmin();
		}
		Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		
		try {
			if (openExecution.getVariable(WSSK_CN.INVC_PVAR_userAccept) != null) {
					assignable.addCandidateUser(openExecution.getVariable(WSSK_CN.INVC_PVAR_userAccept).toString());
					AHEUtils.addToHistory(openExecution, doc, openExecution.getVariable(WSSK_CN.INVC_PVAR_userAccept).toString(), null, null, log);
					return;
			}
			else if (openExecution.getVariable(WSSK_CN.INVC_PVAR_guidAccept) != null) {
				assignable.addCandidateUser(openExecution.getVariable(WSSK_CN.INVC_PVAR_guidAccept).toString());
				AHEUtils.addToHistory(openExecution, doc, null, openExecution.getVariable(WSSK_CN.INVC_PVAR_guidAccept).toString(), null, log);
				return;
			}
			else log.error("zmienne procesu nie zawiera�y wskazanego akceptuj�cego");
		}
		catch (EdmException e) {
			log.error(e.getMessage());
			throw e;
		}
		finally {
			if (DSApi.isContextOpen() && !isOpened)
				DSApi.close();
		}
		
	}
}