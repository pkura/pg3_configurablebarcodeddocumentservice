package pl.compan.docusafe.parametrization.wssk.invoice;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.wssk.WSSK_CN;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** Steruje p�tl� akceptacji: Tworzy zmienn� procesu wskazuj�c�, kto ma dokona� kolejnej akceptacji. <br/>
 * Je�eli ju� nikt nie zosta�, nie wywo�uje kolejnej iteracji p�tli */
@SuppressWarnings("serial")
public class MpkAsgnLoopDecisionHandler implements DecisionHandler {
	
	private final static Logger log = LoggerFactory.getLogger(MpkAsgnLoopDecisionHandler.class);
	
	@SuppressWarnings({ "rawtypes", "unused" })
	public String decide(OpenExecution execution) {
		try {
			Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument document = OfficeDocument.find(docId);
			FieldsManager fm = document.getFieldsManager();
			
			List val  = (List) fm.getValue("MPK");
			for (Object o : val){
				Map m = (Map) o;
				Set entries = m.entrySet();
				int x=0; x++; //slowniki sa typu EnumValues
			}
			
//			if (mpks==null)
//				return "break";
//			else {
//				execution.setVariable(WSSK_CN.VAR_ProcReopened, WSSK_CN.dwrTrue);
//				return "nie";
//			}
			return "break";
			
		} catch (Exception e) {
			log.error("b��d obs�ugi p�tli akceptacyjnej: "+e.getMessage(), e);
			return "break"; // TODO : wyj�cie awaryjne
		}
	}
}