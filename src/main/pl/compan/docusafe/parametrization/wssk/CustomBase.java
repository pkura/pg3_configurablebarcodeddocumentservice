package pl.compan.docusafe.parametrization.wssk;

import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.slf4j.Logger;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/** Klasa bazowa, custom activity, pozwala skupic sie na kodzie a nie na wyjatkach.
 * W podklasach ustawiac:
 * wlasciwy logger,
 * info (ogolna tresc wyjatku, moze byc tez podane poprzez fieldsetter),
 * definiowac executionInsideContext
 * @author Rafal Odoj
 * @date 2012-09-04 */
public abstract class CustomBase implements ExternalActivityBehaviour {

	private static final long serialVersionUID = 1L;
	
	/** logger, w podklasie robic log = LoggerFactory.getLogger(KLASA.class); */
	protected static Logger log; // = LoggerFactory.getLogger(CustomBase.class);
	
	/** komunikat ogolny o bledzie (do logu i do wyjatku */
	protected String exception;
	
	/** traktowanie bledu. true -> error, false -> debug */
	protected boolean failureAsError = false ;

	/** Glowna metoda wywolywana przy wykonaniu custom activity.
	 * W jej wnetrzu jest otwarty kontekst i nie nalezy go na koncu zamykac.
	 * Nie trzeba tez lapac wyjatkow i logowac, o ile nie ma potrzeby dawania informacji bardziej szczegolowej
	 * niz ogolne info zdefiniowane w CustomBase.
	 * Robic override w podklasie */
	protected void executionInsideContext(ActivityExecution execution) throws Exception {}
	
	@Override
	public void execute(ActivityExecution execution) throws Exception {
		
		boolean wasOpened = true;
		if (!DSApi.isContextOpen()) {
			wasOpened = false;
			DSApi.openAdmin();
		} 
		try {
			
			executionInsideContext(execution);
			
			if (DSApi.isContextOpen() && !wasOpened)
				DSApi.context().commit();

		} catch (Exception exc) {
			logexc( ((exception!=null)?exception:"") + exc.getMessage(), failureAsError);
		} finally {
			if (DSApi.isContextOpen() && !wasOpened)
				DSApi.close();
		}
	
		if (execution.getActivity().getDefaultOutgoingTransition() != null)
			execution.takeDefaultTransition();
	}
	
	@Override
	public void signal(ActivityExecution execution, String signalName, Map<String, ?> arg2) throws Exception {
		if (signalName != null)
			execution.take(signalName);
		else 
			execution.takeDefaultTransition();
	}
	
	
	/** wyjatek Edm oraz informacja do logu jako error (true) lub debug (false) */
	private static void logexc (String s, boolean treatFailAsError) throws EdmException {
		if (treatFailAsError)
			log.error(s);
		else
			log.debug(s);
		throw new EdmException(s);
	}
	
	/** info do logu i wyjatek Edm */
	protected static void edm (String s) throws EdmException {
		log.debug(s);
		throw new EdmException(s);
	}
	
	/** error do logu i wyjatek Edm */
	protected static void err (String s) throws EdmException {
		log.error(s);
		throw new EdmException(s);
	}
}




