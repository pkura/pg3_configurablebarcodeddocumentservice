package pl.compan.docusafe.parametrization.wssk;

import java.util.List;

import org.jbpm.api.activity.ActivityExecution;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.Rwa;


/** custom activity, dodaje glowna teczke do dzialu, o ile jeszcze nie jest dodana.
 * @author Rafal Odoj
 * @date 2012-09-04 */
public class CreateOfficeFolder extends CustomBase {
	
	private static final long serialVersionUID = 1L;
	static {   log = LoggerFactory.getLogger(CreateOfficeFolder.class);   }
	
	protected String rwaId;
	protected String guid;
	protected String officeFolderName;
	protected String days;

	@Override
	protected void executionInsideContext(ActivityExecution execution) throws Exception {
		
		exception="B��d podczas tworzenia teczki";
		
		Integer rok = new Integer(GlobalPreferences.getCurrentYear());
		Integer rwa_id = Integer.parseInt(rwaId);
	
		Rwa rwa = Rwa.find(rwa_id);
		String digits = OfficeUtils.rwaDigitsToString(rwa);
		
		List<OfficeFolder> ofs = OfficeFolder.findByDivisionGuidYear(guid, rok);
		OfficeFolder ofisfold = OfficeUtils.findOfficeFolderOnList(ofs, digits);
		
		if (ofisfold == null)
			ofisfold = OfficeUtils.newOfficeFolder ( rwa, guid, null, officeFolderName, days, null);
	}
	
	
}
