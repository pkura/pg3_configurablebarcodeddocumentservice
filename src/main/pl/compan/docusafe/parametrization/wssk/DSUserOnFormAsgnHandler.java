package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Asygnacja na uzytkownika wybranego na formularzu (CN pola zapisany w WSSK_CN)
 * @author Rafal Odoj
 * @date 2012-09-11 */
public class DSUserOnFormAsgnHandler implements AssignmentHandler {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(SupervisorAssignmentHandler.class);
	
	public void assign(org.jbpm.api.task.Assignable assignable, OpenExecution openExecution) throws Exception {
		Long docId = Long.parseLong(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		
		String user = doc.getFieldsManager().getStringKey(WSSK_CN.DSUSER);
		Long userId = null;
		try{
			userId = Long.parseLong(user);
		} catch (NumberFormatException nfe){
			log.info("podane ID użytkownika (" +user+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
			throw new IllegalStateException("podane ID użytkownika (" +user+ ") nie chce sie zamienic na prawidlowa liczbe calkowita.");
		}
		DSUser u = DSUser.findById(userId);
		
		log.info("Dekretacja na: " + u.getName());
		assignable.addCandidateUser(u.getName());
		AHEUtils.addToHistory(openExecution, doc, u.getName(), null, null, log);
	}

}
