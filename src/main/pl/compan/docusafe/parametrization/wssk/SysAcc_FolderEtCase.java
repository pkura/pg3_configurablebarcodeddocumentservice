package pl.compan.docusafe.parametrization.wssk;

import java.util.List;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.model.OpenExecution;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.CaseNotFoundException;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;

/** custom activity, obs�uguje zawi�o�ci dla procesu Utworzenia Kont Systemowych (UKS).
 * Zak�ada teczk� (je�li trzeba); zak�ada spraw� (je�li trzeba); dodaje spraw� do teczki
 * i do niej dodaje odpowiednie dokumenty, zale�nie od tego, czy proces jest uruchomiony r�cznie, czy automatycznie.
 * Zawsze dodaje dokument, z kt�rym proces jest sprz�zony (Utworzenie Kont Systemowych).
 * Przed wartosci� guid w XML mo�na doda� gwiazdk� - wtedy guid b�dzie pobrany z adds.
 * Je�eli w zmiennych procesu s� obecne zmienne z id innych dokument�w, one te� b�d� dodane.
 * Zmienne te to:
 * dok_UDO_ID = id Upowa�nienia do przetwarzania Danych Osobowych
 * dok_ListaDok_ID = id potwierdzenia zapoznania si� z dokumentami przez nowego pracownika
 * @author Rafal Odoj
 * @date 2012-09-04 */
public class SysAcc_FolderEtCase extends CustomBase {
	
	private static final long serialVersionUID = 1L;
	static {   log = LoggerFactory.getLogger(SysAcc_FolderEtCase.class);   }
	
	protected String rwaId;
	protected String guid;

	protected String dok_UDO_ID;		//UDOid,			XML ma trzymac synchronizacje z WSSK_CN. To nazwa zmiennej procesu
	protected String dok_ListaDok_ID;	//PARENT_DOC_ID,	XML ma trzymac synchronizacje z WSSK_CN. To nazwa zmiennej procesu
	
	protected String officeFolderName;	//nazwa teczki
	protected String officeCaseDescr;	//opis sprawy (do niego dodany bedzie na koncu login)
	protected String officeCaseTitle;	//tytul teczki, automatycznie = "nazwisko imie" a tu mozna dodac cos jeszcze ale nazwisko i imie zostanie
	protected String days;				//ilosc dni na zalatwienie sprawy

	@Override
	protected void executionInsideContext(ActivityExecution execution) throws Exception {
		
		exception="B��d podczas obs�ugi sprawy/teczki pracownika ";
		
		String guid_str = null;
		if (guid.startsWith("*"))
			guid_str = Docusafe.getAdditionProperty(guid.substring(1));
		else
			guid_str = guid;
		
		//teczka
		OfficeFolder ofisfold = OfficeUtils.newOfficeFolderIfNotExist(rwaId, guid_str, officeFolderName, days);
		
		// sprawy i dokumenty
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		
		List<OfficeCase> sprawy = null;
		boolean zero = false;
		try {
			sprawy = OfficeCase.findByOfficeIdLike(ofisfold.getOfficeId(), ofisfold);
		} catch (CaseNotFoundException cnfe){
			zero = true;
		}
		
		List<OfficeCase> cases = null;
		int ileSpraw = 0;
		String login = null;
		
		String nazwiskoImie = OfficeUtils.nazwisko_Imie(execution, doc, fm, WSSK_CN.CRSYS_nazwisko, WSSK_CN.CRSYS_imie);
		Object lgn = fm.getKey(WSSK_CN.CRSYS_login);
		if (lgn==null)
			edm("nie podano loginu nowego pracownika");
		login=lgn.toString();
		
		if ( ! zero ){
			cases = OfficeUtils.findOfficeCaseOnList (sprawy, nazwiskoImie, login);
			ileSpraw = cases.size();
			
			if ( ileSpraw > 1 )
				edm("Znaleziono wi�cej ni� jedn� teczk� pracownika");
		}
		
		// tu spraw jest 0 lub 1
		OfficeCase sprawa = null;
		if (zero){
			//tytuly i opisy teczek
			String tytul = nazwiskoImie + (OfficeUtils.isValidString(officeCaseTitle) ? (" "+officeCaseTitle) : "");
			String opis = (OfficeUtils.isValidString(officeCaseDescr) ? (officeCaseDescr) : "") +login;
			
			// liczba dni na zalatwienie
			Integer dni = OfficeUtils.parseInt_noExc(days); //jesli bedzie null to nie szkodzi, wywolana pozniej metoda da wartosc domyslna
			
			sprawa = OfficeUtils.newOfficeCase(null, ofisfold, dni, tytul, opis, "admin"); // TODO : kto ma byc clerkiem
		
			if (sprawa == null)
				edm("nie znaleziono ani nie utworzono odpowiedniej sprawy dla pracownika " +nazwiskoImie);
		} else {
			if (ileSpraw != 1)
				edm("problem przy zak�adaniu/wyszukiwaniu sprawy dla pracownika " +nazwiskoImie); // to nie powinno zajsc
			sprawa = sprawy.get(0);
		}
		
		// jedna sprawa.
		// Doda� UKS do sprawy tylko pod warunkiem, �e to nie jest proces wznowiony.
		if ( czyNowy(execution) )
			OfficeUtils.addDocToCase(sprawa, doc);
		
		
		Object udo  = execution.getVariable(dok_UDO_ID);
		Object ldok = execution.getVariable(dok_ListaDok_ID);
		
		if (udo==null && ldok==null) {
			//zaden nie wypelniony, czyli to chyba proces reczny
			
		} else if (udo!=null && ldok!=null){
			//oba wypelnione, wiec to podproces, dodac
			OfficeUtils.addDocToCase(sprawa, Long.parseLong( udo.toString()) );
			OfficeUtils.addDocToCase(sprawa, Long.parseLong(ldok.toString()) );
			
		} else {
			//w tym procesie si� zapami�tuje UKS (zawsze), UDO (opcjonalnie), LISTADOK (opcjonalnie). Albo 1 albo 3!!! Wype�niony tylko 1 z 2 opcjonalnych ID, to b��d
			edm("W procesie uwtorzenia kont systemowych podano do zapisu 2 dokumenty, ale standardem s� 3 albo 1");
		}	
	}
	
	/** Odpowiada, czy proces jest wznowiony,
	 * poprzez sprawdzenie, czy istnieje zmienna procesu
	 * o nazwie spod WSSK_CN.VAR_ProcReopened i o warto�ci spod WSSK_CN.dwrTrue,
	 * czyli czy openExecution.getVariable("ProcessIsReopened")=="tak"
	 * 
	 * <br/> - je�li tak, proces jest wznowiony -> "wznowiony"
	 * <br/> - je�li nie, proces jest nowy -> "nowy" */
	protected boolean czyNowy(ActivityExecution execution){
		try {
			Object zmienna = execution.getVariable(WSSK_CN.VAR_ProcReopened);
			if (zmienna == null)
				return true;
			else
				if ( ((String)zmienna).equals(WSSK_CN.dwrTrue) )
					return false;
				else 
					return true;
		} catch (Exception e) {
			log.error("blad podejmowania decyzji: "+e.getMessage(), e);
			return true; //wyjscie awaryjne
		}
	}
}
