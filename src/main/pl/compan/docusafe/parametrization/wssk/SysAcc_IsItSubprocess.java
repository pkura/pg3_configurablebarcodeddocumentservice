package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa podejmuje decyzje czy proces utworzenia kont systemowych jest uruchomiony przez czlowieka,
 * czy moze jest procesem podrzednym utworzonym automatycznie.
 * standalone = uruchomil czlowiek;
 * sub = uruchomiono automatycznie jako podproces.
 * Pewne zmienne powinny byc zdefiniowane przez nadproces.
 * Jesli ktorejs brakuje to pewnie jest blad nadprocesu,
 * ale jesli brakuje wszystkich, to na pewno proces uruchomiono recznie.
 * @author Rafa� Odoj
 */
public class SysAcc_IsItSubprocess implements DecisionHandler {

	private static final long serialVersionUID = 1L;
	private final static Logger log = LoggerFactory.getLogger(SysAcc_IsItSubprocess.class);
	
	public String decide(OpenExecution openExecution) {
		String decision = "standalone";
		try {
			// te zmienne powinny byc zdefiniowane przez nadproces.
			// Jesli ktorejs brakuje to pewnie jest blad nadprocesu,
			// ale jesli brakuje wszystkich, to na pewno proces uruchomiono recznie 
			Object emp   = openExecution.getVariable(WSSK_CN.NEWEE_PVAR_employee);
			Object pexid = openExecution.getVariable(WSSK_CN.NEWEE_PVAR_parentExecutionId);
			Object doc   = openExecution.getVariable(WSSK_CN.NEWEE_PVAR_parentDoc);
			if ( (emp==null) && (pexid==null) && (doc==null) )
				decision = "standalone";
			else
				decision = "sub";
			
		} catch (Exception e) {
			log.error("B��d okre�lania, czy proces uruchomiono automat. jako podrz�dny, czy r�cznie przez cz�owieka: ",e);
		}
		return decision;
	}
}
