package pl.compan.docusafe.parametrization.wssk;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.activity.ActivityExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.CasePriority;
import pl.compan.docusafe.core.office.CaseStatus;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Rwa;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.webwork.event.ActionEvent;

/** Klasa utylitarna dla teczek (folder), spraw (case), doumentow.
 * Zaklada nowe teczki, zaklada nowe sprawy, dodaje dokumenty do spraw itp. */
public class OfficeUtils {
	
	private static final Logger log = LoggerFactory.getLogger(CreateOfficeFolder.class);
	
	/** Tworzy nowa teczke i ja zwraca.
	 * @param rwaCategoryId id kategorii RWA, string
	 * @param divisionGuid w jakim dziale zalozyc teczke, konieczne
	 * @param barcode moze byc null i takim pozostac
	 * @param name nazwa dla teczki
	 * @param days liczba dni na za�atwienie dokumentu w teczce
	 * @param parent obiekt teczki nadrzednej, moze byc null, wtedy bedzie zalozona 1. teczka dla dzialu */
	public static OfficeFolder newOfficeFolder (
			Integer rwaCategoryId,
			String divisionGuid,
			String barcode,
			String name,
			String days,
			OfficeFolder parent
			) throws Exception {
		
		Rwa rwaCategory = Rwa.find(rwaCategoryId);

		return newOfficeFolder (rwaCategory, divisionGuid, barcode, name, days, parent );
	}
	
	/** Tworzy nowa teczke i ja zwraca.
	 * @param rwa zywy obiekt kategorii RWA, konieczne
	 * @param divisionGuid w jakim dziale zalozyc teczke, konieczne
	 * @param barcode moze byc null i takim pozostac
	 * @param name nazwa dla teczki
	 * @param days liczba dni na za�atwienie dokumentu w teczce, moze byc null
	 * @param parent obiekt teczki nadrzednej, moze byc null, wtedy bedzie zalozona 1. teczka dla dzialu */
	public static OfficeFolder newOfficeFolder (
			Rwa rwa,
			String divisionGuid,
			String barcode,
			String name,
			String days,
			OfficeFolder parent
			) throws Exception {
		if (rwa.getAcHome() == null)
			throw new EdmException("Nie mo�na za�o�y� teczki w RWA nie posiadaj�cym kategorii archiwalnej");

		if( ! rwa.getIsActive())
			throw new EdmException("Nie mo�na za�o�y� teczki w RWA, kt�re jest nieaktywne");
		
		// nowa teczka
		OfficeFolder officeFolder = new OfficeFolder(
				name,  DSDivision.find(divisionGuid),  rwa,  parent  );
		
		// clerk
		officeFolder.setClerk(DSApi.context().getPrincipalName());	// TODO : kto powinien byc clerkiem. Tak jest na pewno zle, bo to nie event z GUI

		// Rok teczki - bie��cy
		officeFolder.setYear(new Integer(GlobalPreferences.getCurrentYear()));                	

		// liczba dni ( na zalatwienie? )
		Integer dni = null;
		try  {
			dni = Integer.parseInt(days); 
		} catch (Exception e) {     }
		if ( dni != null )
			officeFolder.setDays(dni); 

		// barcode
		officeFolder.setBarcode(barcode);

		// ID
		String[] breakdown = officeFolder.suggestOfficeId();
		String suggestedOfficeId = StringUtils.join(breakdown, "");
    	String officeIdPrefix = breakdown[0];
    	String officeIdMiddle = breakdown[2];
    	String officeIdSuffix = breakdown[4];
		officeFolder.create(suggestedOfficeId, officeIdPrefix, officeIdMiddle, officeIdSuffix);

		return officeFolder;
	}
	
	
	
	
	
	
	
	/** Tworzy nowa teczke (o ile nie istnieje) i ja zwraca.
	 * Tylko teczki typu root (nie zagniezdzone).
	 * Tworzy w podanym dziale i w biezacym roku.
	 * 
	 * @param rwaId id kategorii RWA jako string, konieczne
	 * @param guid w jakim dziale zalozyc/szukac teczke, konieczne
	 * @param officeFolderName nazwa dla teczki, uzywane jesli musi byc zalozona nowa
	 * @param days liczba dni na za�atwienie dokumentu w teczce, moze byc null  */
	public static OfficeFolder newOfficeFolderIfNotExist (
			String rwaId,
			String guid,
			String officeFolderName,
			String days
			) throws Exception {
		Integer rok = new Integer(GlobalPreferences.getCurrentYear());
		Integer rwa_id = Integer.parseInt(rwaId);
	
		Rwa rwa = Rwa.find(rwa_id);
		String digits = OfficeUtils.rwaDigitsToString(rwa);
		
		List<OfficeFolder> ofs = OfficeFolder.findByDivisionGuidYear(guid, rok);
		OfficeFolder ofisfold = OfficeUtils.findOfficeFolderOnList(ofs, digits);
		
		if (ofisfold == null)
			ofisfold = OfficeUtils.newOfficeFolder ( rwa, guid, null, officeFolderName, days, null);
		if (ofisfold == null)
			edm("nie znaleziono ani nie utworzono odpowiedniej teczki o RWA:" +digits +", roku " +rok +", GUID dzia�u: " +guid);
		return ofisfold;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Tworzy nowa sprawe w teczce.
	 * @param rwa string z numerem RWA. Nalez podac albo RWA albo zywy obiekt teczki (XOR).
	 * @param teka obiekt teczki
	 * @param ileDni czas dla sprawy. Mozna go podac (ilosc dni), a jesli sie poda null to bedzie wziety z teczki.
	 * A jesli teczka nie podaje czasu, to bedzie ustalone domyslnie 30 dni.
	 * @param descr opcjonalny opis dla teczki. Jesli null, nalezy ustawic recznie po powrocie
	 * @param clerk opcjonalna string z nazwa clerka. Jesli null, nalezy ustawic recznie po powrocie */
	public static OfficeCase newOfficeCase (
			String rwa,			//   \___XOR (to albo to, drugie null)
			OfficeFolder teka,	//   /
			Integer ileDni,	// mozna nie podac, bedzie brane z teczki albo 30
			String tytul, //tytul sprawy, moze byc null, ale wtedy trzeba samemu po powrocie
			String descr, //opis sprawy, moze byc null, ale wtedy trzeba samemu po powrocie
			String clerk //ustawia clerka, moze byc null, ale wtedy trzeba samemu po powrocie
			) throws Exception {
		
		OfficeFolder teczka = null;
		
		if (teka != null)
			// bierze przekazana teczke
			teczka = teka;
		else {
			if (rwa == null)
				throw new EdmException("Brak nr RWA");
			
			// odnajduje teczke na podstawie kodu RWA
			teczka = OfficeFolder.findByOfficeIdMiddle(rwa);
		}
		
		// nowa sprawa
		OfficeCase officeCase = new OfficeCase(teczka);

		// status
		officeCase.setStatus(CaseStatus.find(CaseStatus.ID_OPEN));
		
		// Proiorytet sprawy - zwykla
		officeCase.setPriority(CasePriority.find(1));
		
		// data zakonczenia - albo podana   albo z teczki   albo 30 dni
		Calendar cal = Calendar.getInstance();
		int days = 30;
		if (ileDni != null)
			days = ileDni;
		else if (teczka.getDays() != null)
			days = teczka.getDays();

		cal.add(Calendar.DATE, days);
		officeCase.setFinishDate(cal.getTime());
		
		// opis
		if (descr != null)
			officeCase.setDescription(descr);
		
		// tytul
		if (tytul != null)
			officeCase.setTitle(tytul);
		
		// referent
		if (clerk != null)
			officeCase.setClerk(clerk);

//		String[] breakdown = officeCase.suggestOfficeId();
//		String suggestedOfficeId = StringUtils.join(breakdown, "");
//		String customOfficeIdPrefix = breakdown[0];
//		String customOfficeIdPrefixSeparator = breakdown[1];
//		String customOfficeIdMiddle = breakdown[2];
//		String customOfficeIdSuffixSeparator = breakdown[3];
//		String customOfficeIdSuffix = breakdown[4];
//
//		String oid = StringUtils.join(new String[] {
//				customOfficeIdPrefix != null ? customOfficeIdPrefix.trim() : "",
//				customOfficeIdPrefixSeparator != null ? customOfficeIdPrefixSeparator.trim() : "",
//				customOfficeIdMiddle != null ? customOfficeIdMiddle.trim() : "",
//				customOfficeIdSuffixSeparator != null ? customOfficeIdSuffixSeparator.trim() : "",
//				customOfficeIdSuffix != null ? customOfficeIdSuffix.trim() : ""
//				}, "");
//
//		officeCase.create(oid, customOfficeIdPrefix != null ? customOfficeIdPrefix.trim() : "", customOfficeIdMiddle != null ? customOfficeIdMiddle.trim() : "", customOfficeIdSuffix != null ? customOfficeIdSuffix.trim() : "");

		String[] breakdown = officeCase.suggestOfficeId();
		String suggestedOfficeId = StringUtils.join(breakdown, "");
		String offIdPref = breakdown[0];
		String offIdMidd = breakdown[2];
		String offIdSuff = breakdown[4];

		officeCase.create(suggestedOfficeId, offIdPref != null ? offIdPref.trim() : "", offIdMidd != null ? offIdMidd.trim() : "", offIdSuff != null ? offIdSuff.trim() : "");

		return officeCase;
	}
	
	/** dodaje dokument do sprawy, podaje sie sprawe i dokument */
	public static void addDocToCase(OfficeCase officeCase, OfficeDocument document ) throws Exception {
		document.setContainingCase(officeCase, false);
	}
	
	/** dodaje dokument do sprawy, podaje sie sprawe i id dokumentu */
	public static void addDocToCase(OfficeCase officeCase, Long docID ) throws Exception {
		OfficeDocument document = OfficeDocument.find(docID);
		document.setContainingCase(officeCase, false);
	}
	
	/** wyciagz z obiektu RWA cyfry jego oznaczenia i robi z nich jednorodny string */
	public static String rwaDigitsToString (Rwa rwa){
		if (rwa==null)
			return null;
		/*Character[] cyfry = arr(rwa.getDigit1(), rwa.getDigit2(), rwa.getDigit3(), rwa.getDigit4(), rwa.getDigit5()); 
		StringBuilder sb = new StringBuilder();
		for (Character cyf : cyfry)
			sb.append( (cyf!=null) ? cyf : "" );
		return sb.toString();*/
		StringBuilder sb = null;
		String[] cyfry = arr(rwa.getDigit1(), rwa.getDigit2(), rwa.getDigit3(), rwa.getDigit4(), rwa.getDigit5());
		for(String cyf : cyfry)
			sb.append((cyf!=null) ? cyf : "");
		return sb.toString();
	}
	
	/** parsuje Integer, moze zwrocic null, ale nie rzuca wyjatkow */
	public static Integer parseInt_noExc(String s){
		try {
			return Integer.parseInt(s);
		} catch (Exception e) {
			return null;
		}
	}
	
	/** pobiera z formatki nazwisko i imie i robi z tego jeden napis */
	public static String nazwisko_Imie(ActivityExecution execution, OfficeDocument doc, FieldsManager fm, String keyNazwisko, String keyImie) throws Exception {
		Object lname = fm.getKey(keyNazwisko);
		Object fname = fm.getKey(keyImie);
		if (lname==null || fname==null)
			edm("nie znaleziono imienia i nazwiska ");
		
		return lname.toString() +" " +fname.toString();
	}
	
	/** zamiana listy argumentow na tablice typu Character */
	public static Character[] arr (Character...chars ){
		return chars;
	}
	
	/** zamiana listy argumentow na tablice typu String */
	public static String[] arr (String...strings ){
		return strings;
	}
	
	/** zamienia podana tablice stringow na jeden napis, gdzie stringi sa oddzielone przecinkami.
	 * Nulle sa pomijane, a na koncu przecinek nie wystepuje.
	 * @param wartosci tablica stringow */
	public static String cssv(String[] wartosci){
		StringBuilder sb = new StringBuilder();
		boolean notFirst=false;
		for (String pole : wartosci){
			if (pole != null) {
				if ( notFirst )
					sb.append(",");

				notFirst = true;
				sb.append(pole);
			}
		}
		return sb.toString();
	}
	
	/** Szuka na liscie jednej teczki o podanym numerze RWA.
	 * @param listaTeczekISpraw - lista zwrocona przez metody szukajace dostepne w klasie OfficeFolder.
	 * Na tej liscie moze byc zarowno teczka, jak i sprawa. */
	public static OfficeFolder findOfficeFolderOnList (List<OfficeFolder> listaTeczekISpraw, String middleDigits){
		if (middleDigits==null)
			return null;
		OfficeFolder teczka = null;
		for ( OfficeFolder of : listaTeczekISpraw)
			if ( middleDigits.equals(of.getOfficeIdMiddle()) )
				if ( of instanceof OfficeFolder ){
					teczka = of;
					break;
				}
		return teczka;			
	}
	
	/** Szuka na liscie teczki podanego pracownika.
	 * Tytul teczki musi sie zaczynac od nazwiska, potem miec imie po spacji
	 * a jesli cos jest w tytule dalej, to koniecznie oddzielone spacja.
	 * @param listaSpraw	lista zwrocona przez metody szukajace dostepne w klasie OfficeFolder.
	 * @param nazwiskoImie	sklejone NAZWISKO spacja IMIE pracownika
	 * @param login			login pracownika (na wypadek, gdyby bylo wiecej osob o tych samych personaliach) */
	public static List<OfficeCase> findOfficeCaseOnList (List<OfficeCase> listaSpraw, String nazwiskoImie, String login){
		if (nazwiskoImie==null)
			return null;
		List<OfficeCase> sprawy = new ArrayList<OfficeCase>();
		for ( OfficeCase of : listaSpraw) {
			String nazwa = of.getTitle();
			if ( ! czyToTytulTeczkiOsobowejUsera(nazwiskoImie, nazwa) )
				continue;
			
			String opis = of.getDescription();
			if (opis==null)
				continue;
			if ( ! opis.contains(login) )
				continue;
			
			sprawy.add(of);
		}
		return sprawy;			
	}
	
	/** sprawdza, czy podany tytul teczki jest zgodny z ustalonym wzorcem i zawiera nazwisko i imie pracownika */
	public static boolean czyToTytulTeczkiOsobowejUsera(String nazwiskoImie, String tytul){
		if (tytul==null)
			return false;
		if ( ! tytul.startsWith(nazwiskoImie) ) // tytul musi sie zaczynac od nazwiska i imienia
			return false;
		else {
			//dobry poczatek
			if ( nazwiskoImie.length() < tytul.length() )
				if ( tytul.charAt(nazwiskoImie.length()) != ' '  ) // sprawdzam, czy po nazwisku i imieniu jest spacja, bo moze byc imie Jan i Janusz albo Magda i Magdalena
					return false;
			return true; //albo caly tytul teczki to nazwisko i imie, albo po imieniu jest spacja i cos dalej. Oba przypadki OK.
		}
	}
	
	/** sprawdza, czy podany string nie jest null i czy po przycieciu jest dluzszy niz 0 znakow */
	public static boolean isValidString(String str){
		if (str==null)
			return false;
		if ( str.trim().isEmpty() )
			return false;
		else
			return true;
	}
	
	/** Tworzy nowy OutOfficeDocument (dokument) i go zwraca.
	 * @param authDvsnGuid nazwa dzialu autora
	 * @param authName nazwa autora
	 * @param dvsnGuid dekretacja na guid, moze byc null
	 * @param asgnUser dekretacja na uzytkownika, moze byc null
	 * 
	 * @param title tytul dokumentu, moze byc null
	 * @param summary opis dokumentu, moze byc null
	 * 
	 * @param internal wskazuje, czy robic zen dokument wewnetrzny */
	public static OutOfficeDocument newOutOfficeDocument (
			String authDvsnGuid,
			String authName,
			String dvsnGuid,
			String asgnUser,
			String title,
			String summary,
			boolean internal
			) throws Exception {
		OutOfficeDocument doc = new OutOfficeDocument();
		
		doc.setCurrentAssignmentGuid(authDvsnGuid);
		doc.setDivisionGuid(authDvsnGuid);
		doc.setCurrentAssignmentAccepted(Boolean.FALSE);
		doc.setCreatingUser(authName);
		if (summary != null) doc.setSummary(summary);
		if (title != null) doc.setTitle(title);
		doc.setForceArchivePermissions(false);
		doc.setAssignedDivision(dvsnGuid);
		doc.setClerk(asgnUser);
		doc.setSender(new Sender());
		doc.setSource("");
		doc.setInternal(internal);
		return doc;
	}

    /** Tworzy nowy OutOfficeDocument (dokument) i go zwraca.
     * @param authDvsnGuid nazwa dzialu autora
     * @param authName nazwa autora
     * @param dvsnGuid dekretacja na guid, moze byc null
     * @param asgnUser dekretacja na uzytkownika, moze byc null
     *
     * @param title tytul dokumentu, moze byc null
     * @param summary opis dokumentu, moze byc null
     *
     * @param internal wskazuje, czy robic zen dokument wewnetrzny
     * @param attachment plik zalacznika, ktory zostanie dodany do dokumentu*/
    public static OutOfficeDocument newOutOfficeDocument (
            String authDvsnGuid,
            String authName,
            String dvsnGuid,
            String asgnUser,
            String title,
            String summary,
            boolean internal,
            File attachment
    ) throws Exception {
        OutOfficeDocument doc = new OutOfficeDocument();

        doc.setCurrentAssignmentGuid(authDvsnGuid);
        doc.setDivisionGuid(authDvsnGuid);
        doc.setCurrentAssignmentAccepted(Boolean.FALSE);
        doc.setCreatingUser(authName);
        if (summary != null) doc.setSummary(summary);
        if (title != null) doc.setTitle(title);
        doc.setForceArchivePermissions(false);
        doc.setAssignedDivision(dvsnGuid);
        doc.setClerk(asgnUser);
        doc.setInternal(internal);
        //dodawanie zalacznika
        try {
            if (attachment != null){
                Attachment att = new Attachment(attachment.getName());
                doc.createAttachment(att);
                att.createRevision(attachment).setOriginalFilename(attachment.getName());
            }
        }
        catch (Exception e){
            log.error("Blad podczas dodawania zalacznika do dokumentu: "+e);
        }
        return doc;
    }

    /** Tworzy nowy OutOfficeDocument (dokument) i go zwraca.
     * @param authDvsnGuid nazwa dzialu autora
     * @param authName nazwa autora
     * @param dvsnGuid dekretacja na guid, moze byc null
     * @param asgnUser dekretacja na uzytkownika, moze byc null
     * @param title tytul dokumentu, moze byc null
     * @param summary opis dokumentu, moze byc null
     *
     */
    public static InOfficeDocument newInOfficeDocument (
            String authDvsnGuid,
            String authName,
            String dvsnGuid,
            String asgnUser,
            String title,
            String summary,
            File attachment
    ) throws Exception {
        InOfficeDocument doc = new InOfficeDocument();

        doc.setCurrentAssignmentGuid(authDvsnGuid);
        doc.setDivisionGuid(authDvsnGuid);
        doc.setCurrentAssignmentAccepted(Boolean.FALSE);
        doc.setCreatingUser(authName);
        if (summary != null) doc.setSummary(summary);
        if (title != null) doc.setTitle(title);
        doc.setForceArchivePermissions(false);
        doc.setAssignedDivision(dvsnGuid);
        doc.setClerk(asgnUser);

        //dodawanie zalacznika
        try {
            if (attachment != null){
                Attachment att = new Attachment(attachment.getName());
                doc.createAttachment(att);
                att.createRevision(attachment).setOriginalFilename(attachment.getName());
            }
        }
        catch (Exception e){
            log.error("Blad podczas dodawania zalacznika do dokumentu: "+e);
        }

        return doc;
    }
	//------------------------------
	
	/** Tworzy link (URL) do dokumentu o podanym ID.
	 * Metoda skopiowana z klasy LinkField, gdzie byla
	 * ona - nie wiedziec czemu - zrobiona jako niestatyczna.
	 * Poddana niewielkim modyfikacjom. */
	public static String createLink(String id) throws DocumentNotFoundException, DocumentLockedException, AccessDeniedException, EdmException {

        Long docId = null;
        try {
            docId = Long.parseLong(id);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        Document doc = Document.find(docId,false);
        StringBuilder b = new StringBuilder();
        log.info("PageContext: {}", Docusafe.getPageContext());
        String pageContext = Docusafe.getPageContext();
        if (doc instanceof InOfficeDocument) {
            if(pageContext.length() > 0)
                b.append("/");
            b.append(Docusafe.getPageContext() + "/office/incoming/document-archive.action?documentId=").append(docId);
        }
        else if (doc instanceof OutOfficeDocument) {
            if (((OutOfficeDocument) doc).isInternal()) {
                if(pageContext.length() > 0)
                    b.append("/");
                b.append(Docusafe.getPageContext() + "/office/internal/document-archive.action?documentId=").append(docId);
            } else {
                if(pageContext.length() > 0)
                    b.append("/");
                b.append(Docusafe.getPageContext() + "office/outgoing/document-archive.action?documentId=").append(docId);
            }
        } else {
            if(pageContext.length() > 0)
                b.append("/");
            b.append(Docusafe.getPageContext() + "/repository/edit-dockind-document.action?id=").append(docId);
        }
        log.info("Link: {}", b.toString());
        return b.toString();
    }
	//------------------------------
	/** przypinanie dokumentu do dziennika dokumentow wewnetrznych - nadanie numeru KO */
	public static void bindToJournal(OfficeDocument document, ActionEvent event, String typDziennika) throws EdmException {
		OutOfficeDocument doc = (OutOfficeDocument) document;
		Journal journal = null;
		if (typDziennika==null)
			journal = Journal.getMainInternal();
		
		else if (typDziennika.equals(Journal.INTERNAL) )
			journal = Journal.getMainInternal();
		
		else if (typDziennika.equals(Journal.INCOMING) )
			journal = Journal.getMainIncoming();
		
		else if (typDziennika.equals(Journal.OUTGOING) )
			journal = Journal.getMainOutgoing();
		
		Long journalId = journal.getId();
		DSApi.context().session().flush();
		Date entryDate = new Date();
		Integer sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), entryDate);
		doc.bindToJournal(journalId, sequenceId, null, event);
		doc.setOfficeDate(entryDate);
	}
	//-------------------------------
	
	/** info do logu i wyjatek Edm */
	private static void edm (String s) throws EdmException {
		log.debug(s);
		throw new EdmException(s);
	}
	@SuppressWarnings("unused")
	/** error do logu i wyjatek Edm */
	private static void err (String s) throws EdmException {
		log.error(s);
		throw new EdmException(s);
	}
}
