package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** Klasa podejmuje decyzje gdzie przeslac dokument:
 * <ul> do dzialu posredniego, ktory moze dokonac edycji </ul>
 * <ul> czy prosto do realizacji </ul>
 * @author Rafa� Odoj */
public class Stamp_EdycjaCzyRealizacja_Decision implements DecisionHandler {

	private static final long serialVersionUID = 1L;
	private final static Logger log = LoggerFactory.getLogger(Stamp_EdycjaCzyRealizacja_Decision.class);
	
	public String decide(OpenExecution openExecution) {
		String decision = "realizacja";
		try {
			Document document = Jbpm4Utils.getDocument(openExecution);
			String type = document.getFieldsManager().getStringKey(WSSK_CN.STMP_typWniosku);
			log.info("typ = " + type);
			
			if( "4".equals(type) || "3".equals(type) || "2".equals(type) || "1".equals(type) ) // TODO : wpisane na sztywno
				decision = "edycja";
			else 
				decision = "realizacja";
			
		} catch (Exception e) {
			log.error("blad podejmowania decyzji",e);
		}
		
		return decision;
	}
}