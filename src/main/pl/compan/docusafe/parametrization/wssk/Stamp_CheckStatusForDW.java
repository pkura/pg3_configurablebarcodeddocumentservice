package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa podejmuje decyzje czy przeslac dokument DW (do wiadomosci), czy nie (end)
 * @author Rafa� Odoj
 */
public class Stamp_CheckStatusForDW implements DecisionHandler {

	private static final long serialVersionUID = 1L;
	private final static Logger log = LoggerFactory.getLogger(Stamp_CheckStatusForDW.class);
	
	public String decide(OpenExecution openExecution) {
		String decision = "end";
		try {
			Document document = Jbpm4Utils.getDocument(openExecution);
			String type = document.getFieldsManager().getStringKey(WSSK_CN.STATUSDOKUMENTU);
			
			log.info("status = " + type);
			
			if(type.equals(WSSK_CN.STMP_STAT_4))		// "do wydania"
				decision = "do_wiadomosci";
			else {
				if (type.equals(WSSK_CN.STMP_STAT_2))	// "zaakceptowana"
					decision = "realizacja";
				else
					decision = "end";
				}
			
		} catch (Exception e) {
			log.error("blad podejmowania decyzji",e);
		}
		
		return decision;
	}
}