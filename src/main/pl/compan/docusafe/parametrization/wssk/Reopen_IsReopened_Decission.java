package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** DecissionHandler, sprawdza, czy istnieje zmienna procesu
 * o nazwie spod WSSK_CN.VAR_ProcReopened i o warto�ci spod WSSK_CN.dwrTrue, <br/>
 * czyli czy openExecution.getVariable("ProcessIsReopened")=="tak"
 * 
 * <ul> - je�li tak, to proces jest wznowiony -> "wznowiony" </ul>
 * <ul> - je�li nie, to proces jest nowy -> "nowy" </ul>
 * @author Rafal Odoj  */
public class Reopen_IsReopened_Decission implements DecisionHandler {

	private static final long serialVersionUID = 1L;
	private final static Logger log = LoggerFactory.getLogger(Reopen_IsReopened_Decission.class);
	
	public String decide(OpenExecution openExecution) {

		try {
			Object zmienna = openExecution.getVariable(WSSK_CN.VAR_ProcReopened);
			
			if (zmienna == null)
				return "nowy";
			
			if ( ((String)zmienna).equals(WSSK_CN.dwrTrue) )
				return "wznowiony";
			else 
				return "nowy";
			
		} catch (Exception e) {
			log.error("blad podejmowania decyzji: "+e.getMessage(), e);
			return "nowy"; //wyj�cie awaryjne
		}
	}

}
