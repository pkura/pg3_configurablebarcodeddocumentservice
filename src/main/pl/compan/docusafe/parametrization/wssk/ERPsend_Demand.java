package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.activity.ActivityExecution;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;

/** custom activity, wywoluje usluge ERP - wysyla liste zapotrzebowania.
 * @author Rafal Odoj
 * @date 2012-10-02 */
public class ERPsend_Demand extends CustomBase {
	private static final long serialVersionUID = 1L;
	static {   log = LoggerFactory.getLogger(ERPsend_Demand.class);   }

	@Override
	protected void executionInsideContext(ActivityExecution execution) throws Exception {
		
		exception="B��d podczas wywo�ania us�ugi integracyjnej ERP celem wys�ania listy zapotrzebowania wewn�trznego";
		
		Long docId = Long.valueOf( execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		
		Object o = fm.getValue("ZAPOTRZEBOWANIE_DICT");
		
//		int x=0; x++;
	}
}
