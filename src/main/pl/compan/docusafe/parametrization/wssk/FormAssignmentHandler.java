package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** 
 * Pobiera z formularza pole DSUSER (obowiazkowe) i asygnuje zadanie dla wskazanego uzytkownika
 * @author Rafal Odoj
 */
public class FormAssignmentHandler implements AssignmentHandler {

	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(FormAssignmentHandler.class);

	public void assign(Assignable assignable, OpenExecution openExecution)
			throws Exception {
		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();

		String author;
		
		if (fm.getValue(WSSK_CN.DSUSER) != null) {
			DSUser user = (DSUser)UserImpl.find(
					new Long(  fm.getKey(WSSK_CN.DSUSER).toString()  )
					);
    		assignable.addCandidateUser(user.getName());
    		AHEUtils.addToHistory(openExecution, doc, user.getName(), null, null, log);
		} else {
			author = doc.getAuthor();
			assignable.addCandidateUser(author);
			AHEUtils.addToHistory(openExecution, doc, author, null, null, log);
		}
	}
}
