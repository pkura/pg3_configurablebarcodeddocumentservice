package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Asygnacja na przelozonego, pierwszego, jaki zostanie znaleziony
 * @author Rafal Odoj
 * @date 2012-08-16
 */
public class SupervisorAssignmentHandler implements AssignmentHandler {
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOG = LoggerFactory.getLogger(SupervisorAssignmentHandler.class);
	
	public void assign(org.jbpm.api.task.Assignable assignable, OpenExecution execution) throws Exception {
		Long docId = Long.parseLong(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Document document = Document.find(docId);
		
		UserImpl author = (UserImpl) DSUser.findByUsername(document.getAuthor());
		
		DSUser supervisor = author.getSupervisor();
		if (supervisor == null ) {
			LOG.info("Nie znaleziono przełożonego użytkownika: " + author.getName());
			throw new IllegalStateException("Brak zdefiniowanego przełożonego");
		}
		
		LOG.info("Dekretacja na: " + supervisor.getName());
		assignable.addCandidateUser(supervisor.getName());
	}
}
