package pl.compan.docusafe.parametrization.wssk;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/** custom activity, utworzenie podprocesu odpowiedzi na wniosek o udostepnienie dokumentacji */
public class ShareDocOut_ChildProcessStart implements ExternalActivityBehaviour {
	
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ShareDocOut_ChildProcessStart.class);
	
	private static Format formatter = new SimpleDateFormat("yyyy-MM-dd");
	
	private static void edm (String s) throws EdmException {
		log.debug(s);
		throw new EdmException(s);
	}
	private static void err (String s) throws EdmException {
		log.error(s);
		throw new EdmException(s);
	}

	public void signal(ActivityExecution activityExecution, String s, Map<String, ?> stringMap) throws Exception {  }

	public void execute(ActivityExecution activityExecution) throws Exception {
		
		boolean wasOpened = true;
		try {
			wasOpened = DSApi.isContextOpen();
			if (!wasOpened) {
				DSApi.openAdmin();
				DSApi.context().begin();
			}
		} catch (Exception e) {
			err("B��d pod��czenia do bazy danych. " + e.getMessage());
			return;
		}
		
		// znalezienie uzytkownika, ktory przyjal zadanie napisania odpowiedzi do wniosku
		String asyg = DSApi.context().getPrincipalName(); // kto przyjal to poprzednie zadanie
		DSUser asygnowany = DSUser.findByUsername(asyg);
		
		// Znalezienie dzialu, do ktorego nalezy asygnowany
		DSDivision division = null;
		try {
			division = GetterUtil.getReasonableDSDivisionOfUser(asygnowany.getDivisions());
		} catch (Exception e){
			log.debug("asygnowany do stworzenia odpowiedzi u�ytkownik nie ma przypisanego �adnego dzia�u. Nie jest to b��d krytyczny dla procesu. "+e.getMessage());
		}
		String dvsn = division.getName();
		

		try {
			Long rootDocID = (Long) activityExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY);
			String rootDocumentID = rootDocID.toString();
			OfficeDocument rootDoc = OfficeDocument.find(rootDocID);
			
			FieldsManager rootFM = rootDoc.getFieldsManager();
			
			Object imio = rootFM.getKey(WSSK_CN.SHODOC_imie); // TODO : tu i kolejne linie: nie bedzie null, tylko od razu wyjatek!
			Object nazo = rootFM.getKey(WSSK_CN.SHODOC_nazwisko);

			Object dodo = rootFM.getKey(WSSK_CN.SHODOC_dataOd);
			Object ddoo = rootFM.getKey(WSSK_CN.SHODOC_dataDo);
			
			StringBuilder sb = new StringBuilder();
			sb.append( (imio==null) ? "" : (String)imio );
			sb.append( " " );
			sb.append( (nazo==null) ? "" : (String)nazo );
			sb.append( " (" );
			sb.append( (dodo==null) ? "" : formatter.format((Date) dodo) );
			sb.append( " - " );
			sb.append( (ddoo==null) ? "" : formatter.format((Date) ddoo) );
			sb.append( ")" );
			
			String tytul = sb.toString();
			String summary = tytul +" - Odpowied� na wniosek o udost�pnienie dokumentacji medycznej";
			
			rootDoc.setTitle(tytul +" - wniosek o udost�pnienie dokumentacji medycznej");
			
			// Utworzy nowy dokument wychodzacy z Odpowiedzia na wniosek o udostepnienie dokumentacji medycznej
			// Przesle dokument na liste tego, kto sie podjal tego zadania wczesniej i przypnie go do niego.
			OutOfficeDocument doc = OfficeUtils.newOutOfficeDocument(dvsn, asyg, dvsn, asyg, summary, summary, false);
					
			doc.create();
			
			DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.WSSK_UDOSTEP_DOK_ODP);
			Map<String, Object> dockindKeys = new HashMap<String, Object>();
			dockindKeys.put("DOC_DESCRIPTION", summary);
			dockindKeys.put("DOC_DATE", new Date());

			doc.setDocumentKind(documentKind);
			Long newDocumentId = doc.getId();
			documentKind.set(newDocumentId, dockindKeys);

			doc.getDocumentKind().logic().archiveActions(doc, 0);
			doc.getDocumentKind().logic().documentPermissions(doc);
			doc.setAuthor(asyg);
			
			
			// wypelnienie pol nowego dokumentu
			Map<String,Object> values = new HashMap<String,Object>();
			values.put("ODPOWIEDZ_NA",		rootDocumentID);
			doc.getDocumentKind().setOnly(newDocumentId, values);
			
			activityExecution.setVariable(WSSK_CN.SHODOC_PVAR_odpowiedzId, newDocumentId);
			activityExecution.setVariable(WSSK_CN.SHODOC_PVAR_wniosekId, rootDocID);
			
			
			//dziennik
//			OfficeUtils.bindToJournal(doc, (ActionEvent) null, Journal.OUTGOING);
			
			//parammetry podprocesu
			String rootExecutionID = activityExecution.getId();
			if (rootExecutionID==null)
				throw new EdmException("Nie uda�o si� uzyska� ExecutionId - nie ma sposobu wskazania punktu powrotu.");

			Map<String, Object> map = Maps.newHashMap();

			map.put(ASSIGN_USER_PARAM, asyg);
			map.put(ASSIGN_DIVISION_GUID_PARAM, "rootdivision");
			map.put(WSSK_CN.SHODOC_PVAR_parentExecutionId, rootExecutionID);//zapamietanie procesu, do ktorego nalezy powrocic
			map.put(WSSK_CN.SHODOC_PVAR_parentDoc, rootDocID); 				//zapamietanie dokumentu, wg ktorego nalezy aktualizowac listy zadan (root process nie wraca do zadan)


			doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(doc, map);

			TaskSnapshot.updateByDocument(doc);
			
			if (DSApi.isContextOpen() && !wasOpened)
				DSApi.context().commit();

		} catch (Exception exc) {
			edm("B��d podczas tworzenia dokumentu dla podprocesu i uruchomienia podprocesu pisania odpowiedzi "
					+"na wniosek o udost�pnienie dokumentacji medycznej: " + exc.getMessage());
		}
		finally {
			if (DSApi.isContextOpen() && !wasOpened)
				DSApi.close();
		}

		if (activityExecution.getActivity().getDefaultOutgoingTransition() != null)
			activityExecution.takeDefaultTransition();
	}

}
