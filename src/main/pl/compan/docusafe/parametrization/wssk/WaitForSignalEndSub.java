package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;

import java.util.Map;

/**
 * Oczekiwanie na zewnetrzny sygnal (z subprocesu, a dokladniej z procesu wewnetrznego).
 * Oczekuje, ze nazwa sygnalu bedzie rowna temu, co jest w "signal".
 * W przeciwnym wypadku wybierze transition podane w "alter".
 * @author rodoj
 * @date 2012-08-27
 */
public class WaitForSignalEndSub implements ExternalActivityBehaviour {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(WaitForSignalEndSub.class);

	private String signal = null;
	private String alter  = null;

	@Override
	@SuppressWarnings("unused")
	public void execute(ActivityExecution activityExecution) throws Exception {
		String obecneID = activityExecution.getId();
		activityExecution.waitForSignal();
	}
	
	@Override
	public void signal(ActivityExecution activityExecution, String s, Map<String, ?> stringMap) throws Exception {
		if (s == null){
			edm("nieprawidlowy sygnal (NULL)");
			return;
		} else {
			if (s.equals(signal)){
				//otrzymano oczekiwany sygnal, isc dalej
				activityExecution.take(signal);
				updateTaskList(activityExecution);
			} else {
				//otrzymano inny niz oczekiwany sygnal, isc dalej, ale sciezka alternatywna
				activityExecution.take(alter);
				updateTaskList(activityExecution);
			}
		}
	}
	
	/** pobiera ID dokumentu nadprocesu, znajduje ten dokument i uaktualnia listy zadan wg tego dokumentu */
	private void updateTaskList(ActivityExecution activityExecution) throws Exception {
		boolean wasOpened = true;
		try {
			wasOpened = DSApi.isContextOpen();
			if (!wasOpened) {
				DSApi.openAdmin();
				DSApi.context().begin();
			}
		} catch (Exception e) {
			err("B��d pod��czenia do bazy danych. " + e.getMessage());
			return;
		}
		
		Object docIDo = activityExecution.getVariable(WSSK_CN.NEWEE_PVAR_parentDoc);
		if (docIDo == null)
			edm("Brak okreslonego id dokumentu Lista dokumentow w zmiennych procesu podrzednego");
		long docID = (Long) docIDo;
		
		try {
			OfficeDocument doc = OfficeDocument.find(docID);
			
			TaskSnapshot.updateByDocument(doc);
		
			if (DSApi.isContextOpen() && !wasOpened)
				DSApi.context().commit();

		} catch (Exception exc) {
			edm("B��d podczas aktualizacji listy zadan " + exc.getMessage());
		}
		finally {
			if (DSApi.isContextOpen() && !wasOpened)
				DSApi.close();
		}
	}
	/** info do logu i wyjatek Edm */
	private static void edm (String s) throws EdmException {
		log.debug(s);
		throw new EdmException(s);
	}
	/** error do logu i wyjatek Edm */
	private static void err (String s) throws EdmException {
		log.error(s);
		throw new EdmException(s);
	}
}
