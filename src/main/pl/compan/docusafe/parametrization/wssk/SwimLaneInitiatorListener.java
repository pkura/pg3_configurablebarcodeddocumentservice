package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** Musi byc wywolywany ON-EVENT="END" bo inczaej bedzie dawac zle wyniki.
 *  Sprawdza, kto przyjal zadanie (przez getPrincipalUser) i zapisuje jego login
 *  jako zmienna procesu o nazwie podanej w polu saveUserAs.
 *  Listaner tak naprawde moze dzialac bez swimlane, gdyz robi tylko to, o czym tu mowa.
 *  @author Rafal Odoj
 *  @date 2012-09-10
 */
public class SwimLaneInitiatorListener extends AbstractEventListener {
	private static final long serialVersionUID = 1L;
	private final static Logger log = LoggerFactory.getLogger(SwimLaneInitiatorListener.class);
	
	protected String saveUserAs;
	
	public void notify(EventListenerExecution eventListenerExecution) throws Exception {
		
		boolean isOpened = true;
		if (!DSApi.isContextOpen()) {
			isOpened = false;
			DSApi.openAdmin();
		}
		try {
			if (saveUserAs == null)
				err("B��d procesu - nie mo�na zapami�ta� u�ytkownika, do kt�rego dokument ma wr�ci� - nie podano nazwy pod jak� to zapisa�");
			String name = DSApi.context().getPrincipalName();
			eventListenerExecution.setVariable(saveUserAs, name);
		
		} catch (Exception e){
			log.error(e.getMessage());
			throw e;
		} finally {
			if (DSApi.isContextOpen() && !isOpened)
				DSApi.close();
		}
    }
	
	/** error do logu i wyjatek Edm */
	private static void err (String s) throws EdmException {
		log.error(s);
		throw new EdmException(s);
	}
}
