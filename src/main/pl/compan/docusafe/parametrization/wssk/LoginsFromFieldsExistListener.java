package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *  Sprawdza, czy wskazane pola formatki maja wpisany wazny login uzytkownika.
 *  Jesli nie, to nie wolno przejsc dalej (wyjatek).
 *  Nazwy pol podaje sie w XML, mozna podac do 5 nazw, ale nie trzeba wszystkich 5.
 *  Jesli sie juz jakas poda, to pole o tej nazwie bedzie sprawdzone.
 *  @author Rafal Odoj
 *  @date 2012-09-06
 */
public class LoginsFromFieldsExistListener  extends AbstractEventListener {
	private static final long serialVersionUID = 1L;
	private final static Logger log = LoggerFactory.getLogger(LoginsFromFieldsExistListener.class);
	
	protected String pole1;
	protected String pole2;
	protected String pole3;
	protected String pole4;
	protected String pole5;
	
	public void notify(EventListenerExecution eventListenerExecution) throws Exception {
		Long docId = Long.valueOf(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument document = OfficeDocument.find(docId);
		
		FieldsManager fm = document.getFieldsManager();
		
		String[] pola = OfficeUtils.arr (pole1, pole2, pole3, pole4, pole5);
		for (String s : pola)
			isLoginValid (fm,  s  , pola);
    }
	
	private void isLoginValid(FieldsManager fm, String pole, String[] pola) throws Exception {
		if (pole == null)
			return;
		Object valo = fm.getValue(pole);
		if (valo == null)
			testFailed(pola);
		if ( ((String)valo).trim().isEmpty() )
			testFailed(pola);
		else {
			//badanie czy user istnieje
			try {
				DSUser.findByUsername( (String)valo  );
			} catch (UserNotFoundException unfe){
				testFailed(pola);
			} catch (EdmException ee){
				testFailed(pola);
			}
			return;
		}
	}
	
	private void testFailed(String[] pola) throws EdmException{
		String wymagane = OfficeUtils.cssv(pola);		
		log.debug("Aby m�c przej�� dalej nale�y poda� prawid�owy login w polu: " +wymagane +".");
		throw new EdmException ("Aby m�c przej�� dalej nale�y poda� prawid�owy login w polu: " +wymagane +".");
	}
}
