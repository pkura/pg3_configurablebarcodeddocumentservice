package pl.compan.docusafe.parametrization.wssk;

import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

/** Podejmuje decyzje, ktory dzial dokona edycji i akceptacji.
 * GUID-y sa zapisane w adds.
 * @author Rafa� Odoj */
public class Stamp_KtoryDzial_AsgnHandler implements AssignmentHandler {

	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(Stamp_KtoryDzial_AsgnHandler.class);

	public void assign(Assignable assignable, OpenExecution openExecution)
			throws Exception {
		
		String decision = Docusafe.getAdditionProperty("wssk.guid.Dzial_Zatrudnienia_i_Wynagrodzenia");
		try {
			Document document = Jbpm4Utils.getDocument(openExecution);
			String type = document.getFieldsManager().getStringKey(WSSK_CN.STMP_typWniosku);
			
			if( "4".equals(type) || "3".equals(type) )				// TODO : wpisane na sztywno
				decision = Docusafe.getAdditionProperty("wssk.guid.Dzial_Zatrudnienia_i_Wynagrodzenia");
			else 
				decision = Docusafe.getAdditionProperty("wssk.guid.Dzial_OrganizacyjnoPrawny");
			
		} catch (Exception e) {
			log.error("blad podejmowania decyzji",e);
		}
		
		assignable.addCandidateGroup(decision);
		
		Long docId = Long.parseLong(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		AHEUtils.addToHistory(openExecution, doc, null, decision, null, log);
	}
}
