package pl.compan.docusafe.parametrization.nfos;

import java.io.Serializable;
import java.sql.SQLXML;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DS_XSL_WZORY")
//@SuppressWarnings("serial")
public class XSLWzory implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	/*@Column(name="CREATE_TIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTime;
	
	@Column(name="MODIFIED_TIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedTime;*/
	
	@Column(name="XSL_FILE")
	private String xslFile;
	
	@Column(name="XSL_URI")
	private String xslUri;
	
	@Column (name="unix_file")
	private String unixFile;
	

	public String getUnixFile() {
		return unixFile;
	}

	public void setUnixFile(String unixFile) {
		this.unixFile = unixFile;
	}

	public XSLWzory() {
//		super();
//		this.setCreateTime(Calendar.getInstance().getTime());
	}
	
	/*public void create() throws EdmException
    {
        try
        {
        	if(id != null)
        		this.setModifiedTime(Calendar.getInstance().getTime());
        	
            DSApi.context().session().saveOrUpdate(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }*/
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

/*	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getModifiedTime() {
		return modifiedTime;
	}

	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}*/

	public String getXslFile() {
		return xslFile;
	}

	public void setXslFile(String xslFile) {
		this.xslFile = xslFile;
	}
	public String getXslUri() {
		return xslUri;
	}
	
	public void setXslUri(String xslUri) {
		this.xslUri = xslUri;
	}
	
}
