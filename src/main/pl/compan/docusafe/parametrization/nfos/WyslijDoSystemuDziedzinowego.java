package pl.compan.docusafe.parametrization.nfos;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;

import java.util.Map;

/**
 * Zapisuje do kolejki dokument do wyslania do systemu dziedzinowego 
 * pobiera nazwe pola na dokumencie, w kt�rym jest id systemu dziedzinowego
 * */
@SuppressWarnings("serial")
public class WyslijDoSystemuDziedzinowego implements ExternalActivityBehaviour {
	private static final Logger log = LoggerFactory.getLogger(WyslijDoSystemuDziedzinowego.class);
	
	@Override
	public void execute(ActivityExecution activityExecution) throws Exception {
		Long docId = Jbpm4Utils.getDocumentId(activityExecution);
		
		NfosExportManager.send(docId); //dodanie do kolejki do wyslania do sys. dziedzinowego
		
		activityExecution.waitForSignal(); // TODO
	}
	
	@Override
	public void signal(ActivityExecution activityExecution, String signalName, Map<String, ?> stringMap) throws Exception {
		if (signalName != null)
			activityExecution.take(signalName);
		else 
			activityExecution.takeDefaultTransition();
	}
}
