package pl.compan.docusafe.parametrization.nfos;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Rwa;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.handlers.DocumentMailHandler;
import pl.compan.docusafe.service.epuap.EpuapExportDocument;
import pl.compan.docusafe.service.epuap.multi.EpuapSkrytka;
import pl.compan.docusafe.service.epuap.multi.EpuapSkrytkaManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.archive.repository.search.SearchDockindDocumentsAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.gov.epuap.ws.pull.PkPullServiceStub.OdpowiedzPullPobierzTyp;

import com.google.common.collect.Maps;

@SuppressWarnings("serial")
public class Minimal_INT_Logic extends AbstractDocumentLogic implements NfosLogicable {

	private static Minimal_INT_Logic instance;
    protected static Logger log = LoggerFactory.getLogger(Minimal_INT_Logic.class);

    private static final String DOC_RECIPIENT = "RECIPIENT";
    private static final String DOC_SENDER = "SENDER";
    private static final String DOC_ODBIORCY = "ODBIORCY_DICT";

    
    private static final List<String> availablePersonFieldsCn = new ArrayList<String>();
    static{
    	availablePersonFieldsCn.add(DOC_RECIPIENT);
    	availablePersonFieldsCn.add(DOC_SENDER);
    	availablePersonFieldsCn.add(DOC_ODBIORCY);    	
    }
    

    public static Minimal_INT_Logic getInstance() {
        if (instance == null)
            instance = new Minimal_INT_Logic();
        return instance;
    }

    public void documentPermissions(Document document) throws EdmException {
    	
    	FieldsManager fm = document.getFieldsManager();
    	EnumItem status = fm.getEnumItem(NfosLogicable.STATUS_CN);
    	if(status == null || status.getId() == NfosLogicable.BIEZACY){
	        documentDefaultPermission(document);
    	} else if(status.getId() == NfosLogicable.ARCHIWALNY || 
    			status.getId() == NfosLogicable.USUNIETY) {
    		documentArchivePermission(document);
    	}
//    	else if(status.getId() == NfosLogicable.USUNIETY) {
//    		documentDeletePermission(document);
//    	}
    }

	private void documentDeletePermission(Document document) throws EdmException {
		revokeAll(document);
		
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
//		perms.add(new PermissionBean(ObjectPermission.READ, "ADMIN_READ", ObjectPermission.GROUP, "Admin - odczyt"));
//		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "ADMIN_READ_ATT_READ", ObjectPermission.GROUP, "Admin - zalacznik odczyt"));
//		perms.add(new PermissionBean(ObjectPermission.MODIFY, "ADMIN_READ_MODIFY", ObjectPermission.GROUP, "Admin - modyfikacja"));
//		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "ADMIN_READ_ATT_MODIFY", ObjectPermission.GROUP, "Admin - zalacznik modyfikacja"));
//		perms.add(new PermissionBean(ObjectPermission.DELETE, "ADMIN_READ_DELETE", ObjectPermission.GROUP, "Admin - zwyk造 usuwanie"));
		
		for (PermissionBean perm : perms) {
		    if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP)) {
		        String groupName = perm.getGroupName();
		        if (groupName == null) {
		            groupName = perm.getSubject();
		        }
		        createOrFind(document, groupName, perm.getSubject());
		    }
		    DSApi.context().grant(document, perm);
		    DSApi.context().session().flush();
		}
	}

	private void documentDefaultPermission(Document document) throws EdmException {
		
		DSApi.context().clearPermissions(document);
		
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));//??
		
		for (PermissionBean perm : perms) {
		    if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP)) {
		        String groupName = perm.getGroupName();
		        if (groupName == null) {
		            groupName = perm.getSubject();
		        }
		        createOrFind(document, groupName, perm.getSubject());
		    }
		    DSApi.context().grant(document, perm);
		    DSApi.context().session().flush();
		}
	}
	
    private void revokeAll(Document document) throws EdmException{
    	
    	
    	
    	java.util.Set<PermissionBean> permsToRevoke = new java.util.HashSet<PermissionBean>();
		permsToRevoke.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		permsToRevoke.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		permsToRevoke.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		permsToRevoke.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		permsToRevoke.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));
		//Archiwum
		permsToRevoke.add(new PermissionBean(ObjectPermission.READ, "ARCHIWUM_READ", ObjectPermission.GROUP, "Dokumenty archiwum- odczyt"));
		permsToRevoke.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "ARCHIWUM_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty archiwum zalacznik - odczyt"));
		permsToRevoke.add(new PermissionBean(ObjectPermission.MODIFY, "ARCHIWUM_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty archiwum - modyfikacja"));
		permsToRevoke.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "ARCHIWUM_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty archiwum zalacznik - modyfikacja"));
		permsToRevoke.add(new PermissionBean(ObjectPermission.DELETE, "ARCHIWUM_READ_DELETE", ObjectPermission.GROUP, "Dokumenty archiwum - usuwanie"));
		//Admin
		permsToRevoke.add(new PermissionBean(ObjectPermission.READ, "ADMIN_READ", ObjectPermission.GROUP, "Admin - odczyt"));
		permsToRevoke.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "ADMIN_READ_ATT_READ", ObjectPermission.GROUP, "Admin - zalacznik odczyt"));
		permsToRevoke.add(new PermissionBean(ObjectPermission.MODIFY, "ADMIN_READ_MODIFY", ObjectPermission.GROUP, "Admin - modyfikacja"));
		permsToRevoke.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "ADMIN_READ_ATT_MODIFY", ObjectPermission.GROUP, "Admin - zalacznik modyfikacja"));
		permsToRevoke.add(new PermissionBean(ObjectPermission.DELETE, "ADMIN_READ_DELETE", ObjectPermission.GROUP, "Admin - zwyk造 usuwanie"));
		
		for (PermissionBean perm : permsToRevoke) {
		    DSApi.context().revoke(document, perm);
		    DSApi.context().session().flush();
		}
    }
    
    @Override
	public void onLoadData(FieldsManager fm) throws EdmException{
    	
		if (fm.getDocumentKind().getCn().equals(NORMAL_OUT)) {
			NfosSystem nfosSystem = NfosSystem.findByCn("ogolny");
			if (!hasEpuapSkrytka(nfosSystem.getId())) {
				throw new EdmException(
						"Przed wys豉niem dokumentu nale篡 skonfigurowa� skrytke og鏊n�.");
			}
		}
    	
		if (fm.getDocumentKind().getName().equals("pismo przychodz帷e")) {
			try {
				if (fm.getEnumItem(NfosLogicable.STATUS_CN).getId() == 12
						|| fm.getEnumItem(NfosLogicable.STATUS_CN).getId() == 13) {
					fm.getField("KATEGORIAARCHIWALNA").setReadOnly(true);
					fm.getField("DOC_DESCRIPTION").setReadOnly(true);
					fm.getField("DOC_DATE").setReadOnly(true);
					fm.getField("DZIAL").setReadOnly(true);
					for (Field field : fm.getField(NfosLogicable.SENDER_CN).getFields()) {
						field.setReadOnly(true);
					}
				}
				else {
					fm.getField("KATEGORIAARCHIWALNA").setReadOnly(false);
					fm.getField("DOC_DESCRIPTION").setReadOnly(false);
					fm.getField("DOC_DATE").setReadOnly(false);
					fm.getField("DZIAL").setReadOnly(false);
					for (Field field : fm.getField(NfosLogicable.SENDER_CN).getFields()) {
						field.setReadOnly(false);
					}
				}
								
			} catch (Exception e) {
				log.info("FieldsManager nie posiada jeszcze p鏊.");
			}
		}
    	super.onLoadData(fm);
    }

	public void setInitialValues(FieldsManager fm, int type)
			throws EdmException {
		super.setInitialValues(fm, type);
		
		log.info("type: {}", type);
		Map<String, Object> values = new HashMap<String, Object>();
		if ("normal_int".equals(fm.getDocumentKind().getCn())) {
			NfosSystem nfosSystem = NfosSystem.findByCn("ogolny");
			if (nfosSystem != null)
				values.put(NfosLogicable.SYSTEM_CN, nfosSystem.getId());
		}
		values.put("TYPE", type);
		values.put(NfosLogicable.STATUS_CN, NfosLogicable.BIEZACY);
		values.put(NfosLogicable.DOC_CZY_UPO_CN, NfosLogicable.DOKUMENT);
		values.put("DZIAL", DSDivision.find("rootdivision").getId());
		values.put("DOC_DATE", new Date());
		fm.reloadValues(values);
	}

	/** metoda wywolywana przez serwis importuj帷y z ePUAP.
	 * Pozwala na specyficzn� dla danego rodzaju dokumentu inicjalizacj� p鏊 tego dokumentu.
	 * {@link pl.compan.docusafe.service.epuap.multi.EpuapMultiImportService} */
	public void setDocValuesAfterImportFromEpuap (OfficeDocument document, EpuapSkrytka skrytka, boolean dokument1_upo0) throws EdmException {
		Map<String, Object> values = new HashMap<String, Object>();
		
		Integer systemId = Integer.parseInt(skrytka.getParam1());// w NFOS parametr param1 przechowuje id systemu, do ktorego trzeba przeslac dokument
		if (systemId==null)
			systemId=1;
		values.put(NfosLogicable.SYSTEM_CN, systemId);
		values.put(NfosLogicable.STATUS_CN, NfosLogicable.BIEZACY);
		
		if (dokument1_upo0)
			values.put(NfosLogicable.DOC_CZY_UPO_CN, NfosLogicable.DOKUMENT);
		else
			values.put(NfosLogicable.DOC_CZY_UPO_CN, NfosLogicable.UPO);
		values.put("DZIAL", DSDivision.find(DSDivision.ROOT_GUID).getId());
		document.getFieldsManager().reloadValues(values);
		document.getDocumentKind().setOnly(document.getId(), values);
	}
	
	/** metoda wywolywana przez serwis eksportuj帷y do ePUAP.
	 * Okre郵a ktora skrytka ma byc jako nadawca dokumentu epuap.
	 * Jesli zwroci null, bedzie uzyta domyslna skrytka
	 * (co jest uzyteczne zwlaszcza, gdy uzywana jest w ogole tylko jedna).
	 * 
	 * Tu wskazuje skrytke na podstawie systemu wskazanego na formatce */
	public EpuapSkrytka wybierzSkrytkeNadawcyEpuap (OfficeDocument document) throws EdmException {
		FieldsManager fm = document.getFieldsManager();
		
//		EnumItem system = fm.getEnumItem(NfosLogicable.SYSTEM_CN);
//		if (system==null)
//			return null;
//		String  systemStrId = ""+system.getId();
		Integer system = fm.getIntegerKey(NfosLogicable.SYSTEM_CN);
		if (system==null)
			return null;
		List<EpuapSkrytka> skrytki = EpuapSkrytka.findByParam1Equality(""+system);
		if (skrytki != null && ( ! skrytki.isEmpty() )  )
			return skrytki.get(0);
		else
			return null;
	}
	
	/** przekazuje do logiki id, jakie dokument dostal z epuapu oraz date wyslania.
	 * Uzywane dla dokumentow wychodzacych i przychodzacych. W przychodzacych wywolywac te metode z data==null
	 * Domyslnie (w innych logikach) nic nie robi. */
	@Override
	public void saveEpuapDetails(Document doc, Long epuapId, Date dataWyslania)
	{
		
		
		try 
		{
			if (epuapId==null) return;
			Map<String, Object> values = new HashMap<String, Object>();
			values.put(NfosLogicable.EPUAP_ID_CN, epuapId);
			if (dataWyslania != null) {
				values.put(NfosLogicable.POWODZENIE_WYSLANIA_CN, new Boolean(true));
				values.put(NfosLogicable.DATA_WYSLANIA_CN, dataWyslania);
			}
			String link = null;
			if(doc instanceof OutOfficeDocument){
				link = Docusafe.getBaseUrl() + "/office/outgoing/document-archive.action?documentId=" + doc.getId();
			} else if (doc instanceof InOfficeDocument){
				link = Docusafe.getBaseUrl() + "/office/incoming/document-archive.action?documentId=" + doc.getId();
			}
			if(link!=null) values.put(NfosLogicable.LINK_CN, link);
			
			doc.getFieldsManager().reloadValues(values);
			doc.getDocumentKind().setOnly(doc.getId(), values);
		} catch (Exception e){
			log.error("Nieudany zapis euapId do dokumentu", e);
		}
		
		try
		{
			KomunikatorRemoteFactory.wyslijStatus(doc, "OK", dataWyslania);
		}catch(Exception e){
			log.error("Nie uda這 si� wys豉c info o statusie do systemu dziedzinowego", e);
		}
	}
		
	@Override
	public void sendEpuapExportStatus(OutOfficeDocument doc, String komunikat, EpuapExportDocument epuapDocument)
	{
		try
		{
			KomunikatorRemoteFactory.wyslijStatus(doc, komunikat, epuapDocument.getDataNadania() == null ? new Date() : epuapDocument.getDataNadania());
		}catch(Exception e){
			log.error("Nie uda這 si� wys豉c info o statusie do systemu dziedzinowego", e);
		}
	}

	/** wpisuje do dokumentu info o doreczeniu (ePUAP Doreczyciel). Domyslnie nie robi zupelnie nic.
	 * 
	 * @param upoDoc	- dokument DS zrobiony na podstawie UPO, moze byc NULL, jesli tego nie robiono.
	 * 					Jesli nie jest null to tym UPO jest UPD doreczenia dokumenu doc.
	 * @param doc		- dokument, kt鏎y pomy郵nie doreczono
	 * @param upo		- obiekt wyslany do nas przez ePUAP
	 * @param dtDoreczenia - data doreczenia
	 * @param kod		- kod wyniku operacji zwracany przez ePUAP. (Dostawalem 1 jako sukces) */
	public void oznaczDokumentJakoDostarczony(Document upoDoc, Document doc,
			OdpowiedzPullPobierzTyp upo, Date dtDoreczenia, int kod){
		
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		
		// TODO : TO W OG粌E NIE BYΜ TESTOWANE ANI NAWET NIE URUCHAMIANE. Nie mam podpisu kwalifikowanego!
		
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		
		if (dtDoreczenia==null)
			return;
		
		Map<String, Object> values = new HashMap<String, Object>();
		values.put(NfosLogicable.DATA_ODBIORU_CN, dtDoreczenia);
		try {
			DSApi.context().session().flush();
			doc.getFieldsManager().reloadValues(values);
			doc.getDocumentKind().setOnly(doc.getId(), values);
		} catch (Exception e){
			log.error("Nieudany zapis w dokumencie informacji o doreczeniu dokumentu", e);
		}
	}
	
	/** dodaje do dokumentu link do dokumentu zawierajacego UPO. Czyli wi捫e dwa dokumenty. */
	public void dodajLinkDoUPO(Document upoDoc, Document doc, String nazwaUpo,	OdpowiedzPullPobierzTyp upo) throws EdmException
	{
		List<Long> upoLinksIdList = (List<Long>) doc.getFieldsManager().getKey(NfosLogicable.UPO_LINKS_CN);
		Map<String,Object> newLinksIdValues = new HashMap<String, Object>();
		
		if(upoLinksIdList == null)
			upoLinksIdList = new ArrayList<Long>();

		// zapis obiektu nowego linku do DB
		UpoLink upoLink = new UpoLink(upoDoc.getId(), nazwaUpo);
		
		DSApi.context().session().saveOrUpdate(upoLink);
		
//		Long newLinkId = upoLink.getId();
//		if (newLinkId==null)
//			return; //taki link juz byl, nie dodajemy go ponownie
		Long newLinkId = upoDoc.getId();
		
		// dodanie id nowego linku do slownika
		if(!upoLinksIdList.contains(newLinkId))
		{
			upoLinksIdList.add(newLinkId);
			newLinksIdValues.put(NfosLogicable.UPO_LINKS_CN, upoLinksIdList);
		
			doc.getDocumentKind().setOnly(doc.getId(), newLinksIdValues);
		}
	}
	
	
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        as.useFolderResolver(document);
        as.useAvailableProviders(document);
        log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
    }
    
    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) {
        log.info("--- Minimal_INT_Logic : START PROCESS !!! ---- {}", event);
		try {
			Map<String, Object> map = Maps.newHashMap();
			FieldsManager fm = document.getFieldsManager();
			if (document instanceof OutOfficeDocument) {
				Map<String, Object> fmvalues = Maps.newHashMap();
				fmvalues.put(NfosLogicable.STATUS_CN, NfosLogicable.BIEZACY);
                fmvalues.put(NfosLogicable.DOC_CZY_UPO_CN, NfosLogicable.DOKUMENT);
				if (fm.getLongKey(NfosLogicable.SYSTEM_CN) == null) {
					NfosSystem nfosSystem = NfosSystem.findByCn("ogolny");
					if (nfosSystem != null){
						fmvalues.put(NfosLogicable.SYSTEM_CN, nfosSystem.getId());
					}
				}
				String link = Docusafe.getBaseUrl() + "/office/internal/document-archive.action?documentId=" + document.getId();
				fmvalues.put(NfosLogicable.LINK_CN, link);
				document.getDocumentKind().setOnly(document.getId(), fmvalues);
			}

            map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
            map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));

			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
    }
    
    @Override
	public void onRejectedListener(Document doc) throws EdmException { }

	@Override
	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException { }

	/**
	 * 
	 * @see SearchDockindDocumentsAction#moveToArchive
	 */
	@Override
	public void globalActionDocument(Document document, int documentType, String actionName) throws EdmException
	{
		//TODO: Od鈍ie瘸� dokument TaskSnapshoty
		EnumItem status = null;
		try
		{
			status = document.getFieldsManager().getEnumItem(NfosLogicable.STATUS_CN);
		}catch(Exception e){
			log.error("", e);
		}	
		Map<String, Object> values = new HashMap<String, Object>();
		String newStatus = null;
		if(status == null || status.getId().intValue() == NfosLogicable.BIEZACY)
		{
			if("moveToArchive".equals(actionName)){
				values.put(NfosLogicable.STATUS_CN, NfosLogicable.ARCHIWALNY);
				values.put(NfosLogicable.ARCHIVE_DATE_CN, new Date());
				values.put(NfosLogicable.DESTROY_DATE_CN, calculateDestroyDate(document.getId()));
				newStatus = "archwialny";
			} else if("removeDocument".equals(actionName)){
				values.put(NfosLogicable.STATUS_CN, NfosLogicable.USUNIETY);
				values.put(NfosLogicable.ARCHIVE_DATE_CN, new Date());
				values.put(NfosLogicable.DESTROY_DATE_CN, new Date());
                newStatus = "usuniety";
            }
			document.getDocumentKind().setOnly(document.getId(), values);
            NfosLogicable.log.info("Status dokumentu id={} zmieniony z {} na {} ", document.getId(), status != null ? status.getCn() : "brak", newStatus);
        }
		else if(status.getId().intValue() == NfosLogicable.ARCHIWALNY)
		{
			if("removeDocument".equals(actionName)){
				values.put(NfosLogicable.STATUS_CN, NfosLogicable.USUNIETY);
				values.put(NfosLogicable.ARCHIVE_DATE_CN, new Date());
				values.put(NfosLogicable.DESTROY_DATE_CN, new Date());
                newStatus = "usuniety";
                document.getDocumentKind().setOnly(document.getId(), values);
                NfosLogicable.log.info("Status dokumentu id={} zmieniony z {} na {} ", document.getId(), status != null ? status.getCn() : "brak", newStatus);
			} else {
				throw new EdmException("Dokument ma ju� status Archiwalny!");
			}
		}
		else if(status.getId().intValue() == NfosLogicable.USUNIETY)
		{
			throw new EdmException("Dokument ma ju� status Usuni皻y!");
		}		
	}

	private void documentArchivePermission(Document document)
			throws EdmException {
		
		DSApi.context().clearPermissions(document);

		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "ARCHIWUM_READ", ObjectPermission.GROUP, "Dokumenty archiwum- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "ARCHIWUM_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty archiwum zalacznik - odczyt"));
//		perms.add(new PermissionBean(ObjectPermission.MODIFY, "ARCHIWUM_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty archiwum - modyfikacja"));
//		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "ARCHIWUM_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty archiwum zalacznik - modyfikacja"));
//		perms.add(new PermissionBean(ObjectPermission.DELETE, "ARCHIWUM_READ_DELETE", ObjectPermission.GROUP, "Dokumenty archiwum - usuwanie"));
		
		for (PermissionBean perm : perms) {
		    if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP)) {
		        String groupName = perm.getGroupName();
		        if (groupName == null) {
		            groupName = perm.getSubject();
		        }
		        createOrFind(document, groupName, perm.getSubject());
			    DSApi.context().documentPermission(document.getId(), perm.getPermission());
		    }
		    DSApi.context().grant(document, perm);
		    DSApi.context().session().flush();
		    }
	}
	
	@Override
    public String getMailForJbpmMailListener(Document document, String mailForLogicCn) {
        //@TODO na pozniejszym etapie b璠zie do konfiguracji
        String email = null;
        try
        {
              if(SYSTEM_CN.equalsIgnoreCase(mailForLogicCn))
              {
                  EpuapSkrytka skrytka = getEpuapSkrytkaBySystemCn(document);
                  if(skrytka != null)
                    email  = getEpuapSkrytkaBySystemCn(document).getEmail();
              }
        }catch(Exception e){
            log.error("", e);
        }
        return email;
    }
    
    /** 
     * @param document
     * @return true - W przypadku gdy istnieje skrytka, kt鏎a ma przypisany system dziedzinowy z dokumentu.
     * false - W przypadku gdy nie ma skrtyki o systemie dzidzinowym z dokumentu.
     * @throws EdmException 
     */
	public boolean isSetEpuapSkrytkaBySystemCn(Document document)
			throws EdmException {
		EpuapSkrytka epuapSkrytka = Minimal_INT_Logic.getEpuapSkrytkaBySystemCn(document);
		if (epuapSkrytka != null) return true;
		return false;
	}
    

    public static EpuapSkrytka getEpuapSkrytkaBySystemCn(Document document) throws EdmException {
        Long systemId = document.getFieldsManager().getLongKey(SYSTEM_CN);
        if(systemId == null)
        {
            return null;
        }
        List<EpuapSkrytka> skrytki = EpuapSkrytka.findByParam1Equality(systemId.toString());
        if(skrytki == null || skrytki.isEmpty())
        {
            return null;
        }

        return skrytki.get(0);
    }

    @Override
    public void prepareMailToSend(Document document, String[] eventParams, DocumentMailHandler.DocumentMailBean documentMailBean)
    {
    	Integer doc_upo = null;
    	try {
			doc_upo = document.getFieldsManager().getEnumItem(NfosLogicable.DOC_CZY_UPO_CN).getId();
		} catch (EdmException e1) {
			log.error("", e1);
		}
    	
		if(doc_upo == NfosLogicable.DOKUMENT){    	
	        try {
	        	Map<String, String> templateParameters = new HashMap<String, String>();
	        	templateParameters.put("tytul", document.getTitle());
	        	templateParameters.put("link", Docusafe.getBaseUrl() + "/office/incoming/document-archive.action?documentId=" + document.getId());
	            documentMailBean.setTemplateParameters(templateParameters);
	            documentMailBean.setTemplate("systemDziedzinowyPowiadomienie.txt");
	
	            EpuapSkrytka skrytka = getEpuapSkrytkaBySystemCn(document);
	            if(skrytka!= null)
	            {
	                documentMailBean.setEmail(skrytka.getEmail());
	            }
	        } catch (EdmException e) {
	            log.error("B陰d podczas wysy豉nia maila dla dokumentu", e);
	        } catch (Exception e){}
		} else {
			try {
				Long docId = UpoLink.getUpoDocumentId(document.getId());
				Document doc = null;
				DSUser user = null;
				
				Map<String, String> templateParameters = new HashMap<String, String>();
				templateParameters.put("tytul", document.getTitle());
	            templateParameters.put("link", Docusafe.getBaseUrl() + "/office/incoming/document-archive.action?documentId=" + document.getId());
	            documentMailBean.setTemplateParameters(templateParameters);
	            documentMailBean.setTemplate("powiadomienieUPO.txt");
					doc = Document.find(docId);
					user = DSUser.findByUsername(doc.getAuthor());
					if(user.getEmail() == null || user.getEmail().isEmpty())
						throw new EdmException("Brak maila");
					documentMailBean.setEmail(user.getEmail());
			} catch (EdmException e) {
				log.error("B陰d podczas wysy豉nia maila dla UPO", e);
			} catch (Exception ex){
				log.error("", ex);
			}
			
		}
    }
    
    @Override
	public boolean canChangeDockind(Document document) throws EdmException
	{
		return false;
	}
    
    public boolean canModifyAttachments(Long id){
    	try {
    		Document document = Document.find(id);
			EnumItem status = document.getFieldsManager().getEnumItem(NfosLogicable.STATUS_CN);
			if (status.getId()!=null && status.getId()!=NfosLogicable.BIEZACY) {
				return false;
			}
			return true;
		} catch (EdmException e) {
			log.error("Brak dokumentu.", e);
			return true;
		}
    }
    
	@Override
	public void onSaveActions(DocumentKind kind, Long documentId,
			Map<String, ?> fieldValues) throws SQLException, EdmException {
		// synchronizacja tabel DSO_PERSON i NFOS_DOC_EMAIL_LISTA
		FieldsManager fm = OfficeDocument.find(documentId).getFieldsManager();
		for (String fieldCn : availablePersonFieldsCn) {
			if (fieldCn.equals(DOC_RECIPIENT)) {
				Object o = fm.getKey(fieldCn);
				if (o != null) {
					// synchronizacja z odbiorcami maili
					Long personId = (Long) o;
					LotusRecipient rec = LotusRecipientManager.findByPersonId(Person.findIfExist(personId).getBasePersonId());
					if (rec == null) {
						Person p = Person.findIfExist(personId);
						if (p != null) {
							rec = new LotusRecipient();
							rec.setEmail(p.getEmail());
							rec.setFax(p.getFax());
							rec.setFirstname(p.getFirstname());
							rec.setLastname(p.getLastname());
							rec.setLocation(p.getLocation());
							rec.setNip(p.getNip());
							rec.setOrganization(p.getOrganization());
							rec.setOrganizationDivision(p.getOrganizationDivision());
							rec.setPesel(p.getPesel());
							rec.setRegon(p.getRegon());
							rec.setStreet(p.getStreet());
							rec.setTitle(p.getTitle());
							rec.setZip(p.getZip());
							if (p.getBasePersonId() != null) {
								rec.setPersonId(p.getBasePersonId());
							}
							rec.save();
						}
					}

				}
			}
		}
		super.onSaveActions(kind, documentId, fieldValues);
	}
    
    @Override
    public boolean canDeleteAttachments(Document document) throws EdmException {
    	return false;
    }
    
    private Date calculateDestroyDate(Long docId) throws DocumentNotFoundException, EdmException{
    	OfficeDocument doc = OfficeDocument.find(docId);
    	FieldsManager fm = doc.getFieldsManager();
    	Long rwaId = (Long) fm.getKey(NfosLogicable.RWA_CN);
    	if(rwaId==null)
    		throw new EdmException("Brak przypisanej kategorii RWA");
    	Rwa rwa = Rwa.find(rwaId.intValue());
    	
    	if(rwa.getAcHome()==null)
    		throw new EdmException("Brak przypisanej kategorii archiwalnej");
    	
    	Integer years = getYearsFromAcHome(rwa);
    	
    	Calendar destroyDate = Calendar.getInstance();
    	destroyDate.add(Calendar.YEAR, 1+years.intValue());
    	destroyDate.set(Calendar.DAY_OF_YEAR, 1);
    	destroyDate.set(Calendar.HOUR_OF_DAY, 1);
    	
		return destroyDate.getTime();
    }

	private Integer getYearsFromAcHome(Rwa rwa) {
		String acHome = rwa.getAcHome();
		String regex = "([a-z]|[A-Z])+\\d{1,2}";
		if (acHome.split("-").length >= 2) {
			return Integer.valueOf(acHome.split("-")[1]);
		} else if (acHome.matches(regex)){
			String years = acHome.replaceAll("[a-z|A-Z]{1,2}", "");
			return Integer.valueOf(years);
		}
		return null;
	}
    
	private boolean hasEpuapSkrytka(Long systemDziedzinowyId){
    	if (systemDziedzinowyId != null) {
    		try {
				NfosSystem nfosSystem = NfosSystem.find(systemDziedzinowyId);
				if(nfosSystem != null)
					for (EpuapSkrytka skrytka : EpuapSkrytkaManager.list()) {
						Long sysDziedzinowy_id = Long.parseLong(skrytka.getParam1());
						if(sysDziedzinowy_id == systemDziedzinowyId) return true;
					}
			} catch (EdmException e) {
				log.error("B陰d podczas wyszukiwania systemu dziedzinowego", e);
			}
		}
    	return false;
    }
}