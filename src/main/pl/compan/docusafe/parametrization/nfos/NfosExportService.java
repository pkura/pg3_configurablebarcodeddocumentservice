package pl.compan.docusafe.parametrization.nfos;

import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.parametrization.nfos.NfosExport.NfosStatus;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Service odpowiedzialny za wysylanie dokument�w do system�w dziedzinowych
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class NfosExportService extends ServiceDriver implements Service
{
	private static final Logger log = LoggerFactory.getLogger(NfosExportService.class);
	private Timer timer;
	
	@Override
	protected void start() throws ServiceException
	{
		log.info("start uslugi NfosExportService");
		timer = new Timer(true);
		timer.schedule(new Export(), (long) 2*1000 ,(long) 1*DateUtils.MINUTE);
		console(Console.INFO,"Start uslugi NfosExportService");
	}

	@Override
	protected void stop() throws ServiceException
	{
	}

	public class Export extends TimerTask
	{

		@Override
		public void run()
		{
			log.trace("Export START");
			console(Console.INFO, "Start wysylania");
			try
			{
				DSApi.openAdmin();
				//lista dokument�w do wyslania do system�w dziedzinwoych
				List<NfosExport> exports = NfosExportManager.list(NfosStatus.ACTIVE);
				
				for(NfosExport export : exports)
				{
					try
					{
						Long docId = export.getDocumentId();
						Document document = Document.find(export.getDocumentId());
						if(!(document.getDocumentKind().logic() instanceof NfosLogicable))
						{
							console(Console.INFO, "Dokument o ID=" + docId + " nie jest przeznaczony do wys�ania");
							return;
						}
						
						FieldsManager fm = document.getFieldsManager();
						Integer system = fm.getIntegerKey(NfosLogicable.SYSTEM_CN);
						NfosSystem nfosSystem = NfosSystem.find(Long.valueOf(system));
						String iDocNr = fm.getStringValue(NfosLogicable.IDOC_NR);
						if (system==null){
							console(Console.INFO, "Brak systemu dziedzinowego dla dokumentu o ID= " + docId);
						} else {
	
							Integer docCzyUpo = fm.getIntegerKey(NfosLogicable.DOC_CZY_UPO_CN);
							if(NfosLogicable.DOKUMENT == docCzyUpo.intValue())
							{
								if (iDocNr == null || iDocNr.isEmpty()){
									console(Console.INFO, "Dokument o ID=" + docId + " nie posiada jeszcze numeru kancelaryjnego systemu iDoc.");
								} else {
								console(Console.INFO, "Wysy�am dokument o ID= " + docId + " do systemu dziedzinowego = " + nfosSystem.getTitle());
								String result = KomunikatorRemoteFactory.wyslijDokumentPrzychodzacy(document);
								if(result.isEmpty())
									console(Console.INFO, "Wysy�anie poprawne.");
								console(Console.INFO, result);
								export.setStatus(NfosStatus.DONE);
								export.setSendDate(Calendar.getInstance().getTime());
								}
							}
							else
							{
								console(Console.INFO, "Wysy�am UPO o ID= " + docId + " do systemu dziedzinowego = " + nfosSystem.getTitle());
								String result = KomunikatorRemoteFactory.wyslijUPO(document);
								console(Console.INFO, result);
								export.setStatus(NfosStatus.DONE);
								export.setSendDate(Calendar.getInstance().getTime());
							}						
						}
						
					}catch(Exception e){
						log.error("", e);
						//do zastanowienia czy ustawiamy error czy probujemy wysla� dalej
						export.setStatus(NfosStatus.ERROR);
						export.setSendDate(Calendar.getInstance().getTime());
						console(Console.ERROR, "B��d [ "+e.getCause().getMessage()+" ]");
					}
				}
				
				try
				{
					DSApi.context().begin();
					for(NfosExport export : exports)
					{
						if(NfosStatus.DONE.equals(export.getStatus()))
							export.create();
					}
					DSApi.context().commit();
				}catch(Exception e){
					DSApi.context()._rollback();
					throw e;
				}
					
			}catch(Exception e)
			{
				log.error("",e);
				console(Console.ERROR, "B��d [ "+e+" ]");
			}
			finally
			{
				log.trace("Export Nfos STOP");
				console(Console.INFO, "Koniec wysylania");
				DSApi._close();
			}		
		}

	}
	
	@Override
	protected boolean canStop()
	{
		return false;
	}

}
