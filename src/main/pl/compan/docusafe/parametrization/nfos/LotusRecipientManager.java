package pl.compan.docusafe.parametrization.nfos;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Manager do Obiektu LotusEmail
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class LotusRecipientManager
{
	private static final Logger log = LoggerFactory.getLogger(LotusRecipientManager.class);
	/**
	 * Listuje wszystkich odbiorc�w w bazie
	 * 
	 * @return lista wszystkich odbiorc�w, przy b��dzie zwraca pust� list�
	 * @throws EdmException
	 */
	@SuppressWarnings("unchecked")
	public static synchronized List<LotusRecipient> list() throws EdmException
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(LotusRecipient.class);
			return (List<LotusRecipient>) c.list();
		}catch(Exception e){
			log.error("", e);
			return new ArrayList<LotusRecipient>();
		}
	}
	/**
	 * Zwraca odbiorce o danym id, je�li wyst�pi wyj�tek zwraca null.
	 * 
	 * @return Odbiorc� o id przekazanym w parametrze.
	 * 
	 */
	@SuppressWarnings("unchecked")
	public static synchronized LotusRecipient findById(Long id)
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(LotusRecipient.class);
			c.add(Restrictions.eq("id", id));
			return (LotusRecipient) c.uniqueResult();
		}catch(Exception e){
			log.error("B��d podczas wykonywania zapytania.", e);
			return null;
		}
	}
	
	/**
	 * Wyszukuje odbiorc�w maili po adresie email
	 * 
	 * @param email - zostan� wyszukani odbiorcy z takim adresem email
	 * @return lista odbiorc�w, przy b��dzie zwraca pust� list�
	 * @throws EdmException
	 */
	@SuppressWarnings("unchecked")
	public static synchronized List<LotusRecipient> findByEmail(String email) throws EdmException
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(LotusRecipient.class);
			c.add(Restrictions.eq("email", email));
			return (List<LotusRecipient>) c.list();
		}catch(Exception e){
			log.error("", e);
			return new ArrayList<LotusRecipient>();
		}
	}
	
	/**
	 * Wyszukuje odbiorc� maili po id z tabeli DSO_PERSON
	 * 
	 * @param personId - zostanie wyszukany odbiorca z takim personId
	 * @return LotusRecipient
	 * @throws EdmException
	 */
	@SuppressWarnings("unchecked")
	public static synchronized LotusRecipient findByPersonId(Long personId) throws EdmException
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(LotusRecipient.class);
			c.add(Restrictions.eq("personId", personId));
			return (LotusRecipient) c.list().get(0);
		}catch(Exception e){
			log.error("", e);
		}
		return null;
	}
	/**
	 * Na podstawie persona uzupe�nia dane obiektu LotusRecipient.
	 * @param lotusRecipient
	 * @param person
	 */
	public static void fillByPerson(LotusRecipient lotusRecipient, Person person) {
		lotusRecipient.setPersonId(person.getId());
		lotusRecipient.setFirstname(person.getFirstname());
		lotusRecipient.setLastname(person.getLastname());
		lotusRecipient.setOrganization(person.getOrganization());
		lotusRecipient.setLocation(person.getLocation());
		lotusRecipient.setOrganizationDivision(person.getOrganizationDivision());
		lotusRecipient.setStreet(person.getStreet());
		lotusRecipient.setZip(person.getZip());
		lotusRecipient.setEmail(person.getEmail());
		lotusRecipient.setFax(person.getFax());
		lotusRecipient.setNip(person.getNip());
		lotusRecipient.setPesel(person.getPesel());
		lotusRecipient.setRegon(person.getRegon());
		lotusRecipient.setTitle(person.getTitle());
	}
}
