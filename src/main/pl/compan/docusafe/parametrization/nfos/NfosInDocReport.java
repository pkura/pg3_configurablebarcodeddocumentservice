package pl.compan.docusafe.parametrization.nfos;

import java.security.cert.X509Certificate;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.hibernate.HibernateException;
import org.jfree.util.Log;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.certificates.ElectronicSignature;
import pl.compan.docusafe.core.certificates.ElectronicSignatureBean;
import pl.compan.docusafe.core.certificates.ElectronicSignatureStore;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;
import pl.compan.docusafe.web.admin.absences.AbstractXlsReport;

/**

 */
public class NfosInDocReport extends AbstractXlsReport
{
    private String system;
    private String kategoria;
    private Date incomingDateFrom;
    private Date incomingDateTo;
    private String officeNumberFrom;
    private String officeNumberTo;
    private Date certificateDateFrom;
    private Date certificateDateTo;
    
	public NfosInDocReport(String reportName)
    {
        super(reportName);

        columns = new String[]
        {
            "Lp.", 
            "ID", 
            "Numer KO",
            "Nr iDoc",
            "Opis", 
            "Nadawca",
            "Kategoria archiwalna", 
            "System dziedzinowy", 
            "Data otrzymania",
            "Certyfikat wa�ny od",
            "Certyfikat wa�ny do"
        };
    }

    @Override
    protected void createEntries()
    {

        try
        {
            int rowCount = 1;
            int lp = 1;
            try {
            	FromClause from = new FromClause();
                TableAlias nfosdocTable = from.createTable("NFOS_DOC_IN");
                TableAlias baseDocTable = from.createJoinedTable(DSApi.getTableName(InOfficeDocument.class), true, nfosdocTable, FromClause.LEFT_OUTER, "$l.document_id = $r.id");
                TableAlias systemyDziedzinoweTable = from.createJoinedTable(DSApi.getTableName(NfosSystem.class),true,nfosdocTable,FromClause.LEFT_OUTER,"$l.SYSTEM = $r.id");
                TableAlias kategoriaTable = from.createJoinedTable("v_dso_rwa",true,nfosdocTable,FromClause.LEFT_OUTER,"$l.KATEGORIAARCHIWALNA = $r.id");

                WhereClause where = new WhereClause();
                
                SelectClause selectId = new SelectClause(true);
                SelectColumn colId = selectId.add(nfosdocTable, "document_id");
                SelectColumn colKO = selectId.add(baseDocTable, "officenumber");
                SelectColumn colKOIDoc = selectId.add(nfosdocTable, "IDOC_NUMER");
                SelectColumn colIncomingDate = selectId.add(baseDocTable, "incomingdate");
                SelectColumn colOpis = selectId.add(baseDocTable, "summary");
                SelectColumn colNadawca = selectId.add(baseDocTable, "sender");
                SelectColumn colKategoriaArchCode = selectId.add(kategoriaTable, "code");
                SelectColumn colKategoriaArchDesc = selectId.add(kategoriaTable, "description");
                SelectColumn colSystemDziedzinowy = selectId.add(systemyDziedzinoweTable, "title");
                
                if(system != null && !system.equals(""))
                	where.add(Expression.eq(systemyDziedzinoweTable.attribute("title"), system));
                if(kategoria != null && !kategoria.equals(""))
                	where.add(Expression.eq(kategoriaTable.attribute("code"), kategoria));
                
                if (incomingDateFrom != null)
                    where.add(Expression.ge(baseDocTable.attribute("incomingdate"), DateUtils.midnight(incomingDateFrom, 0)));

                if (incomingDateTo != null)
                    where.add(Expression.le(baseDocTable.attribute("incomingdate"), DateUtils.midnight(incomingDateTo, 1)));
                
                if(officeNumberFrom != null && !officeNumberFrom.equals(""))
                	where.add(Expression.ge(baseDocTable.attribute("officenumber"), Integer.parseInt(officeNumberFrom)));
                
                if(officeNumberTo != null && !officeNumberTo.equals(""))
                	where.add(Expression.le(baseDocTable.attribute("officenumber"), Integer.parseInt(officeNumberTo)));

                SelectQuery selectQuery = new SelectQuery(selectId, from, where, null);
                
				ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
				
				while (rs.next())
	            {
					
                    String docId = rs.getString(colId.getPosition());
                    List<Date> datyWaznosciCert = getCertificationDateForDocument(Long.valueOf(docId));
                    if(certificateDateFrom == null && certificateDateTo == null){
                    	int i = -1;
                        HSSFRow row = sheet.createRow(rowCount++);
                        row.createCell(++i).setCellValue(new HSSFRichTextString(lp++ +""));
	                    if(docId!=null)
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(docId));
	                    else
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
	                    
	                    //numer KO
	                    String ko = rs.getString(colKO.getPosition());
	                    if(ko!=null)
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(ko));
	                    else
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
	                    
	                    //Numer kancelaryjny iDoc
	                    String iDocKO = rs.getString(colKOIDoc.getPosition());
	                    if(iDocKO!=null)
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(iDocKO));
	                    else
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
	                    
	                    String summary = rs.getString(colOpis.getPosition());
	                    if(summary!=null){
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(summary));
	                    }else
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
	                    
	                    long sender = rs.getLong(colNadawca.getPosition());
	                    if(sender!=0){
	                    	Person s = Person.find(sender);
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(s.getShortSummary()));
	                    }else
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
	                    
	                    String kategoriaArchiwalna = rs.getString(colKategoriaArchCode.getPosition()) + " " +rs.getString(colKategoriaArchDesc.getPosition()) ;
	                    if(!kategoriaArchiwalna.equals("null null"))
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(kategoriaArchiwalna));
	                    else
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
	                                        
	                    String systemDziedzinowy = rs.getString(colSystemDziedzinowy.getPosition());
	                    if(systemDziedzinowy!=null)
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(systemDziedzinowy));
	                    else
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
	                    
	                    Date dataPrzyjecia = rs.getDate(colIncomingDate.getPosition());
	                    if(dataPrzyjecia != null)
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(DateUtils.formatCommonDate(dataPrzyjecia)));
	                    else
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
	                    //Pierwsza data OD - druga data DO
	                    if(datyWaznosciCert.size()>0)
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(DateUtils.formatCommonDate(datyWaznosciCert.get(0))));
	                    else
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
	                    if(datyWaznosciCert.size()>0)
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(DateUtils.formatCommonDate(datyWaznosciCert.get(1))));
	                    else
	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
                    }else if(datyWaznosciCert.size()>0){
                    	if(	(certificateDateFrom != null && certificateDateTo != null 
                    			&& datyWaznosciCert.get(0).after(certificateDateFrom) 
                    			&& datyWaznosciCert.get(1).before(certificateDateTo))
                    			||
                    			(certificateDateFrom != null && certificateDateTo == null 
                            			&& datyWaznosciCert.get(0).after(certificateDateFrom))
                            	||
                    			(certificateDateFrom == null && certificateDateTo != null 
                    					&& datyWaznosciCert.get(1).before(certificateDateTo))
                    			){
                    		int i = -1;
                            HSSFRow row = sheet.createRow(rowCount++);
                            row.createCell(++i).setCellValue(new HSSFRichTextString(lp++ +""));
                    		if(docId!=null)
    	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(docId));
    	                    else
    	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
    	                    //Numer KO
    	                    String ko = rs.getString(colKO.getPosition());
    	                    if(ko!=null)
    	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(ko));
    	                    else
    	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
    	                    
    	                  	//Numer kancelaryjny iDoc
    	                    String iDocKO = rs.getString(colKOIDoc.getPosition());
    	                    if(iDocKO!=null)
    	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(iDocKO));
    	                    else
    	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
    	                    
    	                    String summary = rs.getString(colOpis.getPosition());
    	                    if(summary!=null){
    	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(summary));
    	                    }else
    	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
    	                    
    	                    long sender = rs.getLong(colNadawca.getPosition());
    	                    if(sender!=0){
    	                    	Person s = Person.find(sender);
    	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(s.getShortSummary()));
    	                    }else
    	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
    	                    
    	                    String kategoriaArchiwalna = rs.getString(colKategoriaArchCode.getPosition()) + " " +rs.getString(colKategoriaArchDesc.getPosition()) ;
    	                    if(!kategoriaArchiwalna.equals("null null"))
    	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(kategoriaArchiwalna));
    	                    else
    	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
    	                                        
    	                    String systemDziedzinowy = rs.getString(colSystemDziedzinowy.getPosition());
    	                    if(systemDziedzinowy!=null)
    	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(systemDziedzinowy));
    	                    else
    	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
    	                    
    	                    Date dataPrzyjecia = rs.getDate(colIncomingDate.getPosition());
    	                    if(dataPrzyjecia != null)
    	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(DateUtils.formatCommonDate(dataPrzyjecia)));
    	                    else
    	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
    	                    //Pierwsza data OD - druga data DO
    	                    if(datyWaznosciCert.size()>0)
    	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(DateUtils.formatCommonDate(datyWaznosciCert.get(0))));
    	                    else
    	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
    	                    if(datyWaznosciCert.size()>0)
    	                    	row.createCell(++i).setCellValue(new HSSFRichTextString(DateUtils.formatCommonDate(datyWaznosciCert.get(1))));
    	                    else
    	                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
                    	}
                    }
	            }
			} catch (HibernateException e) {
	            Logger.getLogger(NfosInDocReport.class.getName()).log(Level.SEVERE, null, e);
			} catch (SQLException e) {
	            Logger.getLogger(NfosInDocReport.class.getName()).log(Level.SEVERE, null, e);
			}
            
        }
        catch ( EdmException ex )
        {
            Logger.getLogger(NfosInDocReport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

	public void setSystem(String system) {
		this.system = system;
	}

	public void setKategoria(String kategoria) {
		this.kategoria = kategoria;
	}

	public void setIncomingDateFrom(Date incomingDateFrom) {
		this.incomingDateFrom = incomingDateFrom;
	}

	public void setIncomingDateTo(Date incomingDateTo) {
		this.incomingDateTo = incomingDateTo;
	}

	public void setOfficeNumberFrom(String officeNumberFrom) {
		this.officeNumberFrom = officeNumberFrom;
	}

	public void setOfficeNumberTo(String officeNumberTo) {
		this.officeNumberTo = officeNumberTo;
	}

	public void setCertificateDateFrom(Date certificateDateFrom) {
		this.certificateDateFrom = certificateDateFrom;
	}

	public Date getCertificateDateFrom() {
		return certificateDateFrom;
	}

	public void setCertificateDateTo(Date certificateDateTo) {
		this.certificateDateTo = certificateDateTo;
	}

	public Date getCertificateDateTo() {
		return certificateDateTo;
	}
	
	private List<Date> getCertificationDateForDocument(Long docId){		
		try {
			List<Date> list = new ArrayList<Date>();
			List<ElectronicSignatureBean> esB = ElectronicSignatureStore.getInstance().findSignaturesForDocument(docId);
			if(esB.size()>0){
				ElectronicSignature es = ElectronicSignatureStore.getInstance().get(esB.get(0).getId());
				X509Certificate cert = ElectronicSignatureStore.getInstance().extractX509Certificate(es);
				list.add(new Date(cert.getNotBefore().getTime()));
				list.add(new Date(cert.getNotAfter().getTime()));
			}	

			return list;
		} catch (EdmException e) {
			Log.error("B��d podczas wyci�gania podpisu dla dokumentu" + e);
		} catch (Exception e) {
			Log.error("B��d podczas wyci�gania podpisu dla dokumentu" + e);		}
        return null;
	}

}
