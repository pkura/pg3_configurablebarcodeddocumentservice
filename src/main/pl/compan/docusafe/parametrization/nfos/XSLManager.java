package pl.compan.docusafe.parametrization.nfos;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class XSLManager {

	private static final Logger log = LoggerFactory.getLogger(XSLManager.class);
	private static String defaultDirName = "nfosXSL";
	private String dirName = null;

	public static InputStream getInputStream(String xsl_file){
		InputStream is = new ByteArrayInputStream(xsl_file.getBytes());
		return is;
	}
		
	private static class SingletonHolder { 
        private final static XSLManager instance = new XSLManager();
    }
	
	public static XSLManager getInstance(){
		return SingletonHolder.instance;
	}
	
	protected XSLManager(){}
	
	public static boolean isExists(String xslURI) {
		try {
			Criteria c = DSApi.context().session().createCriteria(XSLWzory.class);
			c.add(Restrictions.like("xslUri", xslURI));
			XSLWzory result = (XSLWzory) c.uniqueResult();
			if (result != null)
				return true;
		} catch (HibernateException e) {
			log.info("W tabeli s� dublikaty, kt�re mo�na usun��");
			return true;
		} catch (Exception e) {
			log.error("", e);
		}
		return false;
	}
	
	public static XSLWzory getByUri(String xslURI){
		try
		{
			Criteria c = DSApi.context().session().createCriteria(XSLWzory.class);
			c.add(Restrictions.eq("xslUri", xslURI));
			if(c.list().size() > 0)
				return (XSLWzory)c.list().get(0);
		}catch(Exception e){
			log.error("", e);
			return null;
		}
		return null;
	}
	
	public String getDirName() {
		if(this.dirName==null) return defaultDirName;
		return dirName;
	}
	
	public void setDirName(String dirName) {
		this.dirName = dirName;
	}
	
	public String getFullPathName(){
		return Docusafe.getHome().getPath()+ "\\" + getDirName();
	}
	
	/**
	 * Tworzy katalog "nfosXSL" lub podany przez setDirName(String dirName)
	 * Funkcja wykorzystywana w SaveFile()
	 */
	private void makeDir() {
		String basePath = this.getDirName();
		File dsHome = new File(Docusafe.getHome(), basePath);
		if (!dsHome.exists()) {
			dsHome.mkdir();
		}
	}
	
	public void SaveFile(String fileName, InputStream inputStream) {
		
    	InputStream _inputStream = inputStream;
    	OutputStream outputStream = null;
    	
    	this.makeDir();
    	
    	try {
    		// read this file into InputStream
//    		_inputStream = new FileInputStream("/Users/mkyong/Downloads/holder.js");
     
    		// write the inputStream to a FileOutputStream
    		outputStream = new FileOutputStream(new File(fileName));
     
    		int read = 0;
    		byte[] bytes = new byte[1024];
     
    		while ((read = _inputStream.read(bytes)) != -1) {
    			outputStream.write(bytes, 0, read);
    		}
     
    		System.out.println("Done!");
     
    	} catch (IOException e) {
    		e.printStackTrace();
    	} finally {
    		if (_inputStream != null) {
    			try {
    				_inputStream.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    		if (outputStream != null) {
    			try {
    				// outputStream.flush();
    				outputStream.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}     
    		}
    	}
	}
}
