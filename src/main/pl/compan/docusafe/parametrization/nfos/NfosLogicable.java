package pl.compan.docusafe.parametrization.nfos;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Okre�la, czy logika nalezy do dokumentu NFOS
 * oraz przechowuje sta�e
 */
public interface NfosLogicable
{
    public static Logger log = LoggerFactory.getLogger(NfosLogicable.class);
	public static final String SYSTEM_CN = "SYSTEM";
	
	public static final String STATUS_CN = "STATUS";
	
	public static final String WYMAGANE_UPO_CN = "WYMAGANEUPO";
	public static final String DATA_ODBIORU_CN = "DATAODBIORU";
	public static final String EPUAP_ID_CN     = "EPUAP_ID";
	public static final String POWODZENIE_WYSLANIA_CN = "POWODZENIEWYSLANIA";
	public static final String DATA_WYSLANIA_CN = "DATAWYSLANIA";
	/** Typ zawarto�ci: czy to dokument czy UPO */
	public static final String DOC_CZY_UPO_CN = "DOCCZYUPO";
	/** cn slownika linkow do UPO */
	public static final String UPO_LINKS_CN = "UPOLINKS";
	public static final String SENDER_CN = "SENDER";
	public static final String LINK_CN = "LINK";	
    /** Numer kancelaryjny z systemu iDoc */
	public static final String IDOC_NR = "IDOCNUMER";
	/** Data zarejestrowania pisma w systemie iDoc */
	public static final String IDOC_DATA = "IDCODATE";
	public static final String WEWNETRZE = "WEWNETRZNE";
	
    public static final String ARCHIVE_DATE_CN = "ARCHIVE_DATE";
    public static final String DESTROY_DATE_CN = "DESTROY_DATE";
	
	public static final int BIEZACY = 11;
	public static final int ARCHIWALNY = 12;
	public static final int USUNIETY = 13;
	
	public static final int DOKUMENT = 151;
	public static final int UPO = 152;
	
	public static final String EMAIL_SUBJECT = "DOC_DESCRIPTION";
	public static final String EMAIL_CONTENT = "EMAILCONTENT";
	public static final String EMAIL_SENDER = "SENDER";
	public static final String EMAIL_RECIPIENT = "ODBIORCY_DICT";
	public static final String EMAIL_DATA = "EMAILDATA";
	public static final String EMAIL_RWA = "KATEGORIAARCHIWALNA";
	public static final String DSI_EMAIL_RECIPIENT = "EMAIL_RECIPIENT";
	public static final String EMAIL_INITIATOR = "INITIATOR";

	public static final String RWA_CN = "KATEGORIAARCHIWALNA";
	

	/** nazwa kodowa typu dokumentu  */
	public static final String NORMAL_OUT = "normal_out";
	/** nazwa kodowa typu dokumentu	 */
	public static final String NORMAL_IN = "normal_in";
	/** nazwa kodowa typu dokumentu	 */
	public static final String BLANK = "blank";
	
	public static final String ANULOWANY_CN = "ANULOWANY";
	
}
