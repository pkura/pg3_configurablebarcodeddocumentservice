package pl.compan.docusafe.parametrization.nfos;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;
import pl.compan.docusafe.web.admin.absences.AbstractXlsReport;

/**

 */
public class NfosOutDocReport extends AbstractXlsReport
{
    private String system;
    private String kategoria;
    private Date incomingDateFrom;
    private Date incomingDateTo;
    private String officeNumberFrom;
    private String officeNumberTo;
    
	public NfosOutDocReport(String reportName)
    {
        super(reportName);

        columns = new String[]
        {
            "Lp.", 
            "ID", 
            "Numer KO",
            "Nr iDoc",
            "Opis", 
            "Odbiorca",
            "Kategoria archiwalna", 
            "System dziedzinowy", 
            "Data wys�ania"
        };
    }

    @Override
    protected void createEntries()
    {

        try
        {
            int rowCount = 1;
            int lp = 1;
            try {
            	FromClause from = new FromClause();
                TableAlias nfosdocTable = from.createTable("NFOS_DOC_OUT");
                TableAlias baseDocTable = from.createJoinedTable(DSApi.getTableName(OutOfficeDocument.class), true, nfosdocTable, FromClause.LEFT_OUTER, "$l.document_id = $r.id");
                TableAlias personDocTable = from.createJoinedTable(DSApi.getTableName(Person.class), true, nfosdocTable, FromClause.LEFT_OUTER, "$l.document_id = $r.document_id");
                TableAlias systemyDziedzinoweTable = from.createJoinedTable(DSApi.getTableName(NfosSystem.class),true,nfosdocTable,FromClause.LEFT_OUTER,"$l.SYSTEM = $r.id");
                TableAlias kategoriaTable = from.createJoinedTable("v_dso_rwa",true,nfosdocTable,FromClause.LEFT_OUTER,"$l.KATEGORIAARCHIWALNA = $r.id");

                WhereClause where = new WhereClause();
                
                SelectClause selectId = new SelectClause(true);
                SelectColumn colId = selectId.add(nfosdocTable, "document_id");
                SelectColumn colKO = selectId.add(baseDocTable, "officenumber");
                SelectColumn colKOIDoc = selectId.add(nfosdocTable, "IDOC_NUMER");
                SelectColumn colSendDate = selectId.add(nfosdocTable, "datawyslania");
                SelectColumn colOpis = selectId.add(baseDocTable, "summary");
                SelectColumn colOdbiorca = selectId.add(personDocTable, "id");
                SelectColumn colKategoriaArchCode = selectId.add(kategoriaTable, "code");
                SelectColumn colKategoriaArchDesc = selectId.add(kategoriaTable, "description");
                SelectColumn colSystemDziedzinowy = selectId.add(systemyDziedzinoweTable, "title");
                
                if(system != null && !system.equals(""))
                	where.add(Expression.eq(systemyDziedzinoweTable.attribute("title"), system));
                if(kategoria != null && !kategoria.equals(""))
                	where.add(Expression.eq(kategoriaTable.attribute("code"), kategoria));
                
                if (incomingDateFrom != null)
                    where.add(Expression.ge(nfosdocTable.attribute("datawyslania"), DateUtils.midnight(incomingDateFrom, 0)));

                if (incomingDateTo != null)
                    where.add(Expression.le(nfosdocTable.attribute("datawyslania"), DateUtils.midnight(incomingDateTo, 1)));
                
                if(officeNumberFrom != null && !officeNumberFrom.equals(""))
                	where.add(Expression.ge(baseDocTable.attribute("officenumber"), Integer.parseInt(officeNumberFrom)));
                
                if(officeNumberTo != null && !officeNumberTo.equals(""))
                	where.add(Expression.le(baseDocTable.attribute("officenumber"), Integer.parseInt(officeNumberTo)));

                SelectQuery selectQuery = new SelectQuery(selectId, from, where, null);
                
				ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
				
				while (rs.next())
	            {
					int i = -1;
                    HSSFRow row = sheet.createRow(rowCount++);
                    row.createCell(++i).setCellValue(new HSSFRichTextString(lp++ +""));
                    //document ID
                    String docId = rs.getString(colId.getPosition());
                    if(docId!=null)
                    	row.createCell(++i).setCellValue(new HSSFRichTextString(docId));
                    else
                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
                    //KO
                    String ko = rs.getString(colKO.getPosition());
                    if(ko!=null)
                    	row.createCell(++i).setCellValue(new HSSFRichTextString(ko));
                    else
                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
                    
                    //Numer kancelaryjny iDoc
                    String iDocKO = rs.getString(colKOIDoc.getPosition());
                    if(iDocKO!=null)
                    	row.createCell(++i).setCellValue(new HSSFRichTextString(iDocKO));
                    else
                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
                    
                    String summary = rs.getString(colOpis.getPosition());
                    if(summary!=null){
                    	row.createCell(++i).setCellValue(new HSSFRichTextString(summary));
                    }else
                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
                    
                    long odbiorca = rs.getLong(colOdbiorca.getPosition());
                    if(odbiorca!=0){
                    	Person s = Person.find(odbiorca);
                    	row.createCell(++i).setCellValue(new HSSFRichTextString(s.getShortSummary()));
                    }else
                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
                    
                    String kategoriaArchiwalna = rs.getString(colKategoriaArchCode.getPosition()) + " " +rs.getString(colKategoriaArchDesc.getPosition()) ;
                    if(!kategoriaArchiwalna.equals("null null"))
                    	row.createCell(++i).setCellValue(new HSSFRichTextString(kategoriaArchiwalna));
                    else
                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
                                        
                    String systemDziedzinowy = rs.getString(colSystemDziedzinowy.getPosition());
                    if(systemDziedzinowy!=null)
                    	row.createCell(++i).setCellValue(new HSSFRichTextString(systemDziedzinowy));
                    else
                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
                    
                    Date dataWyslania = rs.getDate(colSendDate.getPosition());
                    if(dataWyslania != null)
                    	row.createCell(++i).setCellValue(new HSSFRichTextString(DateUtils.formatCommonDate(dataWyslania)));
                    else
                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
                    
                    
	            }
			} catch (HibernateException e) {
	            Logger.getLogger(NfosOutDocReport.class.getName()).log(Level.SEVERE, null, e);
			} catch (SQLException e) {
	            Logger.getLogger(NfosOutDocReport.class.getName()).log(Level.SEVERE, null, e);
			}
            
        }
        catch ( EdmException ex )
        {
            Logger.getLogger(NfosOutDocReport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

	public void setSystem(String system) {
		this.system = system;
	}

	public void setKategoria(String kategoria) {
		this.kategoria = kategoria;
	}

	public void setIncomingDateFrom(Date incomingDateFrom) {
		this.incomingDateFrom = incomingDateFrom;
	}

	public void setIncomingDateTo(Date incomingDateTo) {
		this.incomingDateTo = incomingDateTo;
	}

	public void setOfficeNumberFrom(String officeNumberFrom) {
		this.officeNumberFrom = officeNumberFrom;
	}

	public void setOfficeNumberTo(String officeNumberTo) {
		this.officeNumberTo = officeNumberTo;
	}

}
