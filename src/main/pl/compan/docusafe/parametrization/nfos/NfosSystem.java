package pl.compan.docusafe.parametrization.nfos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.service.epuap.multi.EpuapSkrytka;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** slownik nazw systemow dziedzinowych, z ktorymi wspolpracuje DocuSafe */
@Entity
@Table(name="NFOS_DICT_SYSTEM")
@SuppressWarnings("serial")
public class NfosSystem implements Serializable
{
	private static final Logger log = LoggerFactory.getLogger(NfosSystem.class);
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
    private String cn;
    
    private String title;
    
    private int centrum;
    
    private String refValue;
    
    private boolean available;
    
    private String language;
    
    
	public static NfosSystem find(Long id) throws EdmException {
		return Finder.find(NfosSystem.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public static List<NfosSystem> list() throws EdmException
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(NfosSystem.class);
			return (List<NfosSystem>) c.list();
		}catch(Exception e){
			log.error("", e);
			return null;
		}
	}
	
	/**
	 * 
	 * @param available
	 * @param notSet tylko te kt�re nie sa ustawione
	 * @return
	 * @throws EdmException
	 */
	@SuppressWarnings("unchecked")
	public static List<NfosSystem> list(boolean available) throws EdmException
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(NfosSystem.class);
			c.add(Restrictions.eq("available", available));
			return (List<NfosSystem>) c.list();
		} catch(Exception e){
			log.error("", e);
			return null;
		}
	}
	
	/** Wyszukuje pozycje slownika (typu database) na podstawie nazwy (title).
	 * Zwraca tylko pozycje dostepne (available = true) */
	@SuppressWarnings("unchecked")
	public static List<NfosSystem> findByName(String nazwa) throws EdmException
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(NfosSystem.class);
			c.add(Restrictions.eq("available", true));
			c.add(Restrictions.eq("title", nazwa));
			return (List<NfosSystem>) c.list();
		} catch(Exception e){
			log.error("", e);
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public static NfosSystem findByCn(String nazwa) throws EdmException
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(NfosSystem.class);
			c.add(Restrictions.eq("available", true));
			c.add(Restrictions.eq("cn", nazwa));
			return (NfosSystem) c.uniqueResult();
		} catch(Exception e){
			log.error("", e);
			return null;
		}
	}
	
	/** Wyszukuje pozycje slownika (typu database) na podstawie nazwy (title).
	 * Uwzglednia tylko pozycje dostepne (available = true).
	 * Zwraca jedn� pozycj�, ale tylko pod warunkiem, �e odnalaz� dok�adnie jedn�
	 * (czyli nie 0 i nie 2) */
	public static NfosSystem getExactlyOneByName(String nazwa) throws EdmException {
		List<NfosSystem> lista = NfosSystem.findByName(nazwa);
		if ( (lista==null) || (lista.size()!=1) )
			return null;
		else
			return lista.get(0);
	}
	
	/**
	 * Zwraca ju� id ustawionych system�w dziedzionwych
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static List<Integer> getChoosenSystemId()
	{
		List<Integer> systemsId = new ArrayList<Integer>();
		
		try
		{
			Criteria c = DSApi.context().session().createCriteria(EpuapSkrytka.class);
			List<EpuapSkrytka> skrytki = (List<EpuapSkrytka>) c.list();
			for(EpuapSkrytka s : skrytki)
			{
				if(s.getParam1() != null)
				{
					systemsId.add(Integer.parseInt(s.getParam1()));
				}
			}
		} catch(Exception e){
			log.error("", e);
			return systemsId;
		}
		
		return systemsId;
	}
	
	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getCn()
	{
		return cn;
	}

	public void setCn(String cn)
	{
		this.cn = cn;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public int getCentrum()
	{
		return centrum;
	}

	public void setCentrum(int centrum)
	{
		this.centrum = centrum;
	}

    /**
     * Zwraca wsdl systemu dziedzinowego
     * @return
     */
	public String getRefValue()
	{
		return refValue;
	}

	public void setRefValue(String refValue)
	{
		this.refValue = refValue;
	}

	public boolean isAvailable()
	{
		return available;
	}

	public void setAvailable(boolean available)
	{
		this.available = available;
	}

	public String getLanguage()
	{
		return language;
	}

	public void setLanguage(String language)
	{
		this.language = language;
	}
}
