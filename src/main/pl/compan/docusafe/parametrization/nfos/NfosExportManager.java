package pl.compan.docusafe.parametrization.nfos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.parametrization.nfos.NfosExport.NfosStatus;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Manager obiekt�w NfosExport
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 */
@SuppressWarnings("serial")
public class NfosExportManager implements Serializable
{
	private static final Logger log = LoggerFactory.getLogger(NfosExportService.class);
	
	/**
	 * Dodaje dokument do kolejki wyslania do systemu dizedzinowego
	 * @param documentId
	 * @throws Exception
	 */
	public static void send(Long documentId) throws Exception
	{
		NfosExport nfosExport = new NfosExport();
		nfosExport.setDocumentId(documentId);
		nfosExport.create();
	}
	
	/**
	 * Listuje Dokumenty do wyslania do systemu dziedzinowego.
	 * 
	 * @param status - zostan� wyszukane dokumenty o takim statusie
	 * @return lista dokument�w, przy b��dzie zwraca pust� list�
	 * @throws EdmException
	 */
	@SuppressWarnings("unchecked")
	public static synchronized List<NfosExport> list(NfosStatus status) throws EdmException
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(NfosExport.class);
			c.add(Restrictions.eq("status", status));
			return (List<NfosExport>) c.list();
		}catch(Exception e){
			log.error("", e);
			return new ArrayList<NfosExport>();
		}
	}
}
