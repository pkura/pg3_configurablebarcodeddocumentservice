package pl.compan.docusafe.parametrization.nfos;

import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Slownik nadawc�w : System�w dziedzinowych
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 */
public class SystemDictionary extends DwrDictionaryBase
{
	private final static Logger log = LoggerFactory.getLogger(SystemDictionary.class);
}
