package pl.compan.docusafe.parametrization.nfos;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;

/**
 * Decyduje, czy ten dokument trzeba wyslac do systemu dziedzinowego, czy zostawic w Docusafe 
 * pobiera nazwe pola na dokumencie, w kt�rym jest id systemu dziedzinowego.
 * wynik:
 * "doPrzeslania" / "wewnetrzny"
 * */
@SuppressWarnings("serial")
public class Decision_DoPrzeslaniaCzyWewnetrzny implements DecisionHandler {
	private static final Logger log = LoggerFactory.getLogger(Decision_DoPrzeslaniaCzyWewnetrzny.class);

	/** nazwa pola, kt�re zawiera id systemu, do kt�rego pismo ma i�� */
	private String systemFieldCn = null;

	@Override
	public String decide(OpenExecution openExecution) {
		
		try {
            OfficeDocument doc = Jbpm4Utils.getDocument(openExecution);
			FieldsManager fm = doc.getFieldsManager();
			
			Integer sysId = fm.getIntegerKey(systemFieldCn);
			if (sysId==null){
				return "wewnetrzny";
			}
			//TODO	Sprawdza� czy system dziedzinowy ma ustawiony refValue w NFOS_DICT_SYSTEM
			//		Zmiana ma by� wprowadzona poniewa� do GWD nie b�d� przesy�ane dokumenty.
			else if (sysId==1) {
				return "wewnetrzny";
			}
			else {
				openExecution.setVariable("systemDziedzinowyID", sysId);
				return "doPrzeslania";
			}
		} catch (Exception e) {
			log.error("b��d podejmowania decyzji: "+e.getMessage(), e);
			return "wewnetrzny"; //wyj�cie awaryjne
		}
	}
}
