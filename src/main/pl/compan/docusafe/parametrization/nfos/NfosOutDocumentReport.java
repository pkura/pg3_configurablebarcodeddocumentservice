package pl.compan.docusafe.parametrization.nfos;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.OrderClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

/**
 * @author Jerzy Pir�g <jerzy.pirog@docusafe.pl>
 */
public class NfosOutDocumentReport extends Report
{

    //data otrzymania
	//system dziedzinowy
	//kategoria archiwalna
	//numer ko
	
	private String system;
    private String kategoria;
    private Date incomingDateFrom;
    private Date incomingDateTo;
    private String officeNumberFrom;
    private String officeNumberTo;
    private UniversalTableDumper dumper;

    public void initParametersAvailableValues() throws ReportException
    {
        super.initParametersAvailableValues();
        try
        {

            for( ReportParameter rp : params )
            {
                if( rp.getFieldCn().equals("system") )
                {
                    rp.setAvailableValues(pobierzSystemyDziedzinowe());
                }
                if( rp.getFieldCn().equals("kategoria") )
                {
                    rp.setAvailableValues(pobierzKategorieArchiwalne());
                }
            }
        }
        catch ( Exception e )
        {
            throw new ReportException(e);
        }
    }

    @Override
    public void doReport() throws Exception
    {
        for( ReportParameter rp : params )
        {
            if( rp.getFieldCn().equals("system") )
            {
                system = rp.getValueAsString();
            }
            if( rp.getFieldCn().equals("kategoria") )
            {
                kategoria = rp.getValueAsString();
            }
            if( rp.getFieldCn().equals("incoming_date_from") )
            {
            	String[] tmp = rp.getValueAsString().split("-");
            	if(tmp.length!=1){
            		Date tmpDateFrom = new Date(Integer.parseInt(tmp[2])-1900, Integer.parseInt(tmp[1])-1, Integer.parseInt(tmp[0]));
                	incomingDateFrom = tmpDateFrom;
            	}
            }
            if( rp.getFieldCn().equals("incoming_date_to") )
            {
            	String[] tmp = rp.getValueAsString().split("-");
            	if(tmp.length!=1){
		        	Date tmpDateTo = new Date(Integer.parseInt(tmp[2])-1900, Integer.parseInt(tmp[1])-1, Integer.parseInt(tmp[0]));
		            incomingDateTo = tmpDateTo;
            	}
            }
            if( rp.getFieldCn().equals("officeNumberFrom") )
            {
                officeNumberFrom = rp.getValueAsString();
            }
            if( rp.getFieldCn().equals("officeNumberTo") )
            {
                officeNumberTo = rp.getValueAsString();
            }
        }

        dumper = new UniversalTableDumper();
        XlsPoiDumper poiDumper = new XlsPoiDumper();
        File xlsFile = new File(this.getDestination(), "raport.xls");
        poiDumper.openFile(xlsFile);
        FileOutputStream fis = new FileOutputStream(xlsFile);

        doReportForAll().getWorkbook().write(fis);
        fis.close();
        dumper.addDumper(poiDumper);

    }

    private NfosOutDocReport doReportForAll() throws Exception
    {
        NfosOutDocReport xlsReport = new NfosOutDocReport("Raport dokument�w wychodz�cych");
        if(system != null && !system.equals(""))
        	xlsReport.setSystem(system);
        if(kategoria != null && !kategoria.equals(""))
        	xlsReport.setKategoria(kategoria);
        xlsReport.setIncomingDateFrom(incomingDateFrom);
        xlsReport.setIncomingDateTo(incomingDateTo);
        if(officeNumberFrom != null && !officeNumberFrom.equals(""))
        	xlsReport.setOfficeNumberFrom(officeNumberFrom);
        if(officeNumberTo != null && !officeNumberTo.equals(""))
        	xlsReport.setOfficeNumberTo(officeNumberTo);
        
        
        xlsReport.generate();
        return xlsReport;
    }

    private Map<String, String> pobierzSystemyDziedzinowe()
    {
        Map<String, String> systemy = new HashMap<String, String>();
        try
        {
        	for(NfosSystem system : NfosSystem.list()){
        		systemy.put(system.getTitle(), system.getTitle());
        	}
        }
        catch ( Exception e )
        {
            log.debug(e.getMessage(), e);
        }
        return systemy;
    }
    

    private Map<String, String> pobierzKategorieArchiwalne()
    {
        Map<String, String> kategorieArch = new TreeMap<String, String>();
        try
        {
        	//////
        	FromClause from = new FromClause();
            TableAlias rwaTable = from.createTable("v_dso_rwa");
            WhereClause where = new WhereClause();
            OrderClause order = new OrderClause();
            
            SelectClause selectId = new SelectClause(true);
            SelectColumn colId = selectId.add(rwaTable, "id");
            SelectColumn colCode = selectId.add(rwaTable, "code");
            SelectColumn colDescription = selectId.add(rwaTable, "description");
            
            order.add(rwaTable.attribute("code"), OrderClause.ASC);
            
            SelectQuery selectQuery = new SelectQuery(selectId, from, where, order);
            
			ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
			
			while (rs.next())
            {
        		kategorieArch.put(rs.getString(colCode.getPosition()), rs.getString(colCode.getPosition()) + " - " + rs.getString(colDescription.getPosition()));
            
            }

        }
        catch ( Exception e )
        {
            log.debug(e.getMessage(), e);
        }
        return sortByValues(kategorieArch);
        

    }
    public static <K, V extends Comparable<V>> Map<K, V> sortByValues(final Map<K, V> map) {
	Comparator<K> valueComparator =  new Comparator<K>() {
	    public int compare(K k1, K k2) {
	        int compare = map.get(k2).compareTo(map.get(k1));
	        if (compare == 0) return 1;
	        else return -compare;
	    }
	};
	Map<K, V> sortedByValues = new TreeMap<K, V>(valueComparator);
	sortedByValues.putAll(map);
	return sortedByValues;
	}
}
