package pl.compan.docusafe.parametrization.nfos;

import java.util.HashMap;
import java.util.Map;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.events.handlers.DocumentMailHandler.DocumentMailBean;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.archive.repository.search.SearchDockindDocumentsAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

@SuppressWarnings("serial")
public class NfosEmailLogic extends AbstractDocumentLogic
{
	private static NfosEmailLogic instance;
    protected static Logger log = LoggerFactory.getLogger(NfosEmailLogic.class);

    public static NfosEmailLogic getInstance() {
        if (instance == null)
            instance = new NfosEmailLogic();
        return instance;
    }
    
	@Override
	public void documentPermissions(Document document) throws EdmException {

		FieldsManager fm = document.getFieldsManager();
		EnumItem status = fm.getEnumItem(NfosLogicable.STATUS_CN);
		if (status == null || status.getId() == NfosLogicable.BIEZACY) {
			documentDefaultPermission(document);
		} else if (status.getId() == NfosLogicable.ARCHIWALNY
				|| status.getId() == NfosLogicable.USUNIETY) {
			documentArchivePermission(document);
		}
	}
	
	private void documentDefaultPermission(Document document) throws EdmException {
		
		DSApi.context().clearPermissions(document);
		
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));//??
		
		for (PermissionBean perm : perms) {
		    if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP)) {
		        String groupName = perm.getGroupName();
		        if (groupName == null) {
		            groupName = perm.getSubject();
		        }
		        createOrFind(document, groupName, perm.getSubject());
		    }
		    DSApi.context().grant(document, perm);
		    DSApi.context().session().flush();
		}
	}
	
	private void documentArchivePermission(Document document)
			throws EdmException {

		DSApi.context().clearPermissions(document);

		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "ARCHIWUM_READ", ObjectPermission.GROUP, "Dokumenty archiwum- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "ARCHIWUM_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty archiwum zalacznik - odczyt"));
		// perms.add(new PermissionBean(ObjectPermission.MODIFY, "ARCHIWUM_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty archiwum - modyfikacja"));
		// perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "ARCHIWUM_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty archiwum zalacznik - modyfikacja"));
		// perms.add(new PermissionBean(ObjectPermission.DELETE, "ARCHIWUM_READ_DELETE", ObjectPermission.GROUP, "Dokumenty archiwum - usuwanie"));
				
		for (PermissionBean perm : perms) {
			if (perm.isCreateGroup()
					&& perm.getSubjectType().equalsIgnoreCase(
							ObjectPermission.GROUP)) {
				String groupName = perm.getGroupName();
				if (groupName == null) {
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
				DSApi.context().documentPermission(document.getId(),
						perm.getPermission());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		as.useFolderResolver(document);
        as.useAvailableProviders(document);
        log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
	}
	
	@Override
	public boolean canChangeDockind(Document document) throws EdmException
	{
		return false;
	}
	
	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event)
			throws EdmException {
		// TODO Auto-generated method stub
		super.onStartProcess(document, event);
	}
	/**
	 * 
	 * @see SearchDockindDocumentsAction#moveToArchive
	 */
	@Override
	public void globalActionDocument(Document document, int documentType, String actionName) throws EdmException
	{
		EnumItem status = null;
		try
		{
			status = document.getFieldsManager().getEnumItem(NfosLogicable.STATUS_CN);
		}catch(Exception e){
			log.error("", e);
		}	
		Map<String, Object> values = new HashMap<String, Object>();
		String newStatus = null;
		if(status == null || status.getId().intValue() == NfosLogicable.BIEZACY)
		{
			if("moveToArchive".equals(actionName)){
				values.put(NfosLogicable.STATUS_CN, NfosLogicable.ARCHIWALNY);
				newStatus = "archwialny";
			} else if("removeDocument".equals(actionName)){
				values.put(NfosLogicable.STATUS_CN, NfosLogicable.USUNIETY);
                newStatus = "usuniety";
            }
			document.getDocumentKind().setOnly(document.getId(), values);
            NfosLogicable.log.info("Status dokumentu id={} zmieniony z {} na {} ", document.getId(), status != null ? status.getCn() : "brak", newStatus);
        }
		else if(status.getId().intValue() == NfosLogicable.ARCHIWALNY)
		{
			if("removeDocument".equals(actionName)){
				values.put(NfosLogicable.STATUS_CN, NfosLogicable.USUNIETY);
                newStatus = "usuniety";
                document.getDocumentKind().setOnly(document.getId(), values);
                NfosLogicable.log.info("Status dokumentu id={} zmieniony z {} na {} ", document.getId(), status != null ? status.getCn() : "brak", newStatus);
			} else {
				throw new EdmException("Dokument ma ju� status Archiwalny!");
			}
		}
		else if(status.getId().intValue() == NfosLogicable.USUNIETY)
		{
			throw new EdmException("Dokument ma ju� status Usuni皻y!");
		}		
	}
	
	@Override
	public void prepareMailToSend(Document document, String[] eventParams,
			DocumentMailBean documentMailBean) {
		if (eventParams.length != 3) {
			documentMailBean.setEmail(null);
			return;
		}

		String email = eventParams[1];
		if (email == null || email.equals("")) {
			documentMailBean.setEmail(null);
			return;
		}

		documentMailBean.setEmail(email);

		Map<String, String> templateParameters = new HashMap<String, String>();
		if (eventParams[2].equals("")) {
			documentMailBean.setEmail(null);
			return;
		}
		// TODO zmiana paratetr闚 szablonu lub rozszerzenie.
		templateParameters.put("tytul", document.getTitle());
		templateParameters.put("link", Docusafe.getBaseUrl()
				+ "/office/incoming/document-archive.action?documentId="
				+ document.getId());
		documentMailBean.setTemplateParameters(templateParameters);
		documentMailBean.setTemplate(eventParams[2]);

		super.prepareMailToSend(document, eventParams, documentMailBean);
	}
}
