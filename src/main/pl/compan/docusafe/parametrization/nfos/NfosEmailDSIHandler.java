package pl.compan.docusafe.parametrization.nfos;

import java.io.File;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.commons.io.FileUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.events.handlers.DocumentMailHandler;
import pl.compan.docusafe.parametrization.nfos.IImportedUser.EmailUser;
import pl.compan.docusafe.service.imports.dsi.DSIBean;
import pl.compan.docusafe.service.imports.dsi.DSIImportHandler;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Handler do obs�ugi przychodz�cych maili z nfos.
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class NfosEmailDSIHandler extends DSIImportHandler
{
	private final static Logger log = LoggerFactory.getLogger(NfosEmailDSIHandler.class);
	
	@Override
	protected boolean isStartProces()
	{
		return true;
	}
	
	@Override
	protected void prepareImport()
	{
		//Data
		try {
			Date dataDoc = new SimpleDateFormat("yyyy-MM-dd").parse((String) values.get(NfosLogicable.EMAIL_DATA));
			values.put(NfosLogicable.EMAIL_DATA, dataDoc);
			// RWA
			Integer rwa = Integer.parseInt((String) values.get(NfosLogicable.EMAIL_RWA));
			values.put(NfosLogicable.EMAIL_RWA, rwa);

			values.put("EMAIL_SENDER", values.get(NfosLogicable.EMAIL_SENDER));
			values.remove(NfosLogicable.EMAIL_SENDER);
			values.put(NfosLogicable.STATUS_CN, NfosLogicable.BIEZACY);
			
			//Sender i Recipients b�d� importowani w metodzie actionAfterCreate
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}
	
	@Override
	public void actionAfterCreate(Long documentId, DSIBean dsiB)
			throws Exception {
//		Ustawienie nadawcy
		String senderEmail = (String) values.get("EMAIL_SENDER");
		try {
			DSApi.beginTransacionSafely();
			IImportedUser importedUser = checkEmail(senderEmail);
			List<Person> personByEmail = new ArrayList<Person>();
			OfficeDocument doc = OfficeDocument.find(documentId);
			
//			OfficeDocument.find(documentId).getSender().fromMap((iiu.getPerson().toMap()));

			personByEmail = importedUser.getPersons();
			Person person = null;
			if(personByEmail.size() > 1){
				log.info("Istniej� dublicaty kontrahent�w.");
				person = personByEmail.get(0);
			}
			else if(personByEmail == null || personByEmail.size() == 0){
				person = new Person();
				person = importedUser.getPerson();
				person.create();
			} else
				person = personByEmail.get(0);
			
			Sender sender = doc.getSender();
			sender.fromMap(person.toMap());
			sender.setLparam("lotus");
			sender.setBasePersonId(person.getId());
			sender.update();
			doc.setSender(sender);
			DSApi.context().commit();
			DSApi.beginTransacionSafely();
			
			values.put("SENDER", sender.getId());
		} catch (EdmException e) {
			String errorMsg = "B��d podczas dodawania nadawcy dokumentu.";
			log.error(errorMsg, e);
			DocumentMailHandler.createEvent(documentId, (String)values.get(NfosLogicable.EMAIL_INITIATOR), "lotusPowiadomienieNiepoprawny.txt");
		}
		
//		Odbiorcy
		String recipients = (String) values.get(NfosLogicable.DSI_EMAIL_RECIPIENT);
		List<Person> foundPersons = null;
		
		Person person = null;
		Recipient recipient = null;
		LotusRecipient lotusRecipient = null;
			
		String lotusRecipientEmail;
		
		for (String strRecipient : recipients.split(",")) {
						
			if(strRecipient.startsWith("<") || strRecipient.endsWith(">")){
				lotusRecipientEmail =  strRecipient.replace("<", "").replace(">", "");
			} else {
				lotusRecipientEmail = strRecipient;
			}
			
			try {
				List<Person> temp = Person.findByGivenFieldValue("email", lotusRecipientEmail);
				foundPersons = new ArrayList<Person>();
				for (Person person2 : temp){
					if(person2 instanceof Recipient || person2 instanceof Sender) continue;
					foundPersons.add(person2);
				}
			} catch (EdmException e) {
				log.error("B��d podczas pobierania rekord�w z DSO_PERSON", e);
				DocumentMailHandler.createEvent(documentId, (String)values.get(NfosLogicable.EMAIL_INITIATOR), "lotusPowiadomienieNiepoprawny.txt");
			}
			
			if (foundPersons == null || foundPersons.size() == 0){ // brak persona o mailu w lotusRecipientEmail 
				IImportedUser importedUserEmail = checkEmail(lotusRecipientEmail);
				//Utworzy� persona
				person = new Person();
//				person.setEmail(lotusRecipientEmail);
				person = importedUserEmail.getPerson();
				person.createIfNew();
			} else { //person istnieje
				person = foundPersons.get(0); // powinien by� jeden person.
			}
//				lotusRecipient = LotusRecipientManager.findById(person.getId());
//				if (lotusRecipient == null) {//nie ma odbiorcy LotusRecipient
			recipient = new Recipient();
			recipient.fromMap(person.toMap());
			recipient.setLparam("lotus");
			recipient.setDocumentId(documentId);
			recipient.setBasePersonId(person.getId());
			recipient.create();
			
			lotusRecipient = new LotusRecipient();
			LotusRecipientManager.fillByPerson(lotusRecipient, recipient);
			lotusRecipient.save();
				
			powiazZDokumentem(documentId, lotusRecipient.getId());
		}
//		dodawanie za��cznik�w
		try {
			List<LotusAttachment> listAtt = null;
			Long importId = dsiB.getImportId();
			List<LotusEmail> list = LotusEmailManager.findByImportId(importId);
			if (list != null || list.size() > 0) {
				LotusEmail lotusEmail = list.get(0);
				listAtt = lotusEmail.getAttachments();
				// listAtt = LotusAttachment.list(lotusEmail.getId());
				if (listAtt != null && listAtt.size() > 0) {
					for (LotusAttachment att : listAtt) {
//						byte[] bytes = Base64.decodeBase64(att.getContent());
//						pierwsze rozwiazanie z base64
						byte[] bytes = att.getContent();
						File file = new File(att.getFileName());
						FileUtils.writeByteArrayToFile(file, bytes);

						// DSApi.context().begin();

						Document document = Document.find(documentId);

						Attachment attachment = new Attachment(file.getName());
						document.createAttachment(attachment);						
						attachment.createRevision(file).setOriginalFilename(
								att.getFileName());

						DSApi.context().session().update(document);
					}
				}
			}
		} catch (Exception e) {
			log.error("B��d podczas dodawania za��cznik�w.", e.getMessage());
			DocumentMailHandler.createEvent(documentId, (String)values.get(NfosLogicable.EMAIL_INITIATOR), "lotusPowiadomienieNiepoprawny.txt");
		}

		Document document = Document.find(documentId);
		FieldsManager fm = document.getDocumentKind().getFieldsManager(documentId);
		
		//link do dokumentu.
		Map<String, Object> fieldValues = new HashMap<String, Object>();
		String link = Docusafe.getBaseUrl() + "office/incoming/document-archive.action?documentId=";
		fieldValues.put(NfosLogicable.LINK_CN, link + documentId);
		fieldValues.put(NfosLogicable.EMAIL_INITIATOR, values.get(NfosLogicable.EMAIL_INITIATOR));
		
		document.getDocumentKind().setOnly(documentId, fieldValues);
		
		document.setTitle(fm.getStringKey(NfosLogicable.EMAIL_SUBJECT));
		DSApi.context().session().update(document);
		
		DocumentMailHandler.createEvent(documentId, (String)values.get(NfosLogicable.EMAIL_INITIATOR), "lotusPowiadomieniePoprawny.txt");
		
		super.actionAfterCreate(documentId, dsiB);
	}
	
	private IImportedUser checkEmail(String lotusRecipientEmail){
		return new EmailUser(lotusRecipientEmail);
	}

	/**
	 * Uzupe�nia tabel� NFOS_DOC_EMAIL_RECIPIENTS_MULTI o DOCUMENT_ID i FIELD_VAL
	 * 
	 * @param documentId
	 * @param recipientId
	 */
	private void powiazZDokumentem(Long documentId, Long recipientId) {
		try {
			String _sql = " INSERT INTO NFOS_DOC_EMAIL_RECIPIENTS_MULTI "
					+ " (DOCUMENT_ID,FIELD_CN,FIELD_VAL) "
					+ " VALUES ( ? ,'ODBIORCY_DICT', ? )";
			PreparedStatement ps = DSApi.context().prepareStatement(_sql);
			ps.setLong(1, documentId);
			ps.setLong(2, recipientId);
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			log.error("B��d podczas dodawania odbiorcy o id: "+ recipientId + "do dokumentu: " + documentId, e);
		}
	}

	@Override
	public String getDockindCn()
	{
		return DocumentLogicLoader.NFOS_EMAIL;
	}

	@Override
	public boolean isRequiredAttachment()
	{
		return AvailabilityManager.isAvailable("nfosemail.isrequiredAttachment");
	}
	
	
}
