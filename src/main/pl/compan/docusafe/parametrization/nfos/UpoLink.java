package pl.compan.docusafe.parametrization.nfos;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;

/** Mapuje tabele linkow do dokumentow.
 * <pre>
 * CREATE TABLE NFOS_MULTILINK (
 *  ID			numeric(18,0) NOT NULL,
 *  DOCUMENT_ID	numeric(18,0) NOT NULL,
 *  TITLE		varchar(1000) NOT NULL
 * )
*/
@Entity
@Table(name="NFOS_MULTILINK")
public class UpoLink
{
    private final static Logger log = LoggerFactory.getLogger(UpoLink.class);
	@Id
//	@GeneratedValue(strategy=GenerationType.AUTO) // id jest jednoczesnie kluczem glownym w tabeli i kluczem obcym do dokumentu !!! Nie ja tak to wymyslilem
    //id upo
	private Long id;
	
	@Column (name="DOCUMENT_ID")
	private Long documentId;
	
	private String title;
	
	
	public UpoLink() {  };
	
	public UpoLink(Long obaId, String titlee)
	{
		this();
		documentId = obaId;
		id = obaId;
		title = titlee;
	}

    /**
     * Zwraca ID dokumentu do kt�rego nale�y upo, wyszukuje tylko dokumenty wychodz�ce
     * @param upoId
     * @return
     */
    public static Long getUpoDocumentId(Long upoId)
    {
        Long documentId = null;
        try
        {
//            PreparedStatement ps = DSApi.context().prepareStatement("SELECT id FROM DOC_OUT_MULTI where FIELD_VAL = ?");
            PreparedStatement ps = DSApi.context().prepareStatement("SELECT DOCUMENT_ID FROM NFOS_DOC_OUT_MULTI where FIELD_VAL = ?");
            ps.setString(1, upoId.toString());

            ResultSet rs = ps.executeQuery();

            while(rs.next())
            {
                documentId = rs.getLong("DOCUMENT_ID");
                break;
            }

            ps.close();
            rs.close();
        }catch(Exception e){
            log.error("", e);
        }

        return documentId;
    }

	// akcesory ----------------------------------------
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getDocumentId() {
		return documentId;
	}
	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
}
