package pl.compan.docusafe.parametrization.nfos;

import java.util.*;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.util.QueryForm;

public interface IImportedUser {
	public Person getPerson();
	public List<Person> getPersons() throws EdmException;

	public class ADUser implements IImportedUser {

		Map<String, String> adCN;
		String organization;
		String externalId;

		static String FIRSTNAME = "FIRSTNAME";
		static String LASTNAME = "LASTNAME";

		public ADUser(String adIteam) {
			externalId = adIteam;
			adCN = new HashMap<String, String>();
			String[] strings = adIteam.split("/");
			String[] nazwa = strings[0].split("=")[1].split(" ");
			adCN.put(FIRSTNAME, nazwa[0]);
			adCN.put(LASTNAME, nazwa[1]);
			organization = strings[1].split("=")[1];
		}

		@Override
		public Person getPerson() {
			Person person = new Person();
			person.setFirstname(adCN.get(FIRSTNAME));
			person.setLastname(adCN.get(LASTNAME));
			person.setOrganization(organization);
			return person;
		}

		@Override
		public List<Person> getPersons() throws EdmException {
			List<Person> result = new ArrayList<Person>();
			QueryForm form = new QueryForm(0,0);
			form.addProperty(FIRSTNAME, adCN.get(FIRSTNAME));
			form.addProperty(LASTNAME, adCN.get(FIRSTNAME));
			Person[] persons = Person.search(form).results();
			
			
			return null;
		}

	}

	public class EmailUser implements IImportedUser {

		String email;
		
		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public EmailUser(String email) {
			this.setEmail(email);
		}
		
		@Override
		public Person getPerson() {
			Person person = new Person();
			person.setEmail(this.getEmail());
			return person;
		}

		@Override
		public List<Person> getPersons() throws EdmException {
//			List<Person> result = new ArrayList<Person>();
//			QueryForm form = new QueryForm(0,0);
//			form.addProperty("EMAIL", email);
//			Person[] persons = Person.search(form).results();
//			result = Person.findByGivenFieldValue("email", getEmail());
			return Person.findByGivenFieldValue("email", getEmail());
		}

	}
}
