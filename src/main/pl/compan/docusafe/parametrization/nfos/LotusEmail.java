package pl.compan.docusafe.parametrization.nfos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Tabela przechowuje informacje dotycz�ce 
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
@Entity
@Table(name="NFOS_LOTUS_EMAIL")
@SuppressWarnings("serial")
public class LotusEmail implements Serializable
{	
	private static final Logger log = LoggerFactory.getLogger(LotusEmail.class);

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="import_id", nullable=false)
	private Long importId;
	
	@Enumerated
	private Status status;
	
	@Column(name="error_code")
	private String errorCode;
	
	@Column(name="create_date", nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	
	@Column(name="parse_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date parseDate;
	
	private String subject;
	
	private String content;
	
	@OneToMany(mappedBy="lotusEmail")
	@OrderBy("createDate")
	private List<LotusAttachment> attachments;
	public List<LotusAttachment> getAttachments() { return attachments; } 
	public void setAttachments(List<LotusAttachment> attachments) { this.attachments = attachments; }
	
	@Column(name="initiator")
	private String initiator;
		
	@Column(name="sender")
	private String sender;
	
	@Column(name="recipient")
	private String recipient;
	
	@Column(name="lotus_email_id", insertable = false, updatable = false)
	private Long lotusEmailId;

	@Column(name="id_rwa")
	private Long rwa_id;
	
	@Column(name="num_attachments")
	private Long numberOfAttachments;
	
	@Column(name="oryginal_id", insertable = false, updatable = false)
	private String oryginalId;
	
	public Long getId()
	{
		return id;
	}


	public void setId(Long id)
	{
		this.id = id;
	}


	public Long getImportId()
	{
		return importId;
	}


	public void setImportId(Long importId)
	{
		this.importId = importId;
	}
	
	public void create() throws EdmException
    {
        try
        {
        	if(id != null)
        		parseDate = Calendar.getInstance().getTime();
        	
            DSApi.context().session().saveOrUpdate(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

	public Status getStatus()
	{
		return status;
	}


	public void setStatus(Status status)
	{
		this.status = status;
	}
	
	public Date getCreateDate()
	{
		return createDate;
	}


	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}


	public Date getParseDate()
	{
		return parseDate;
	}


	public void setParseDate(Date parseDate)
	{
		this.parseDate = parseDate;
	}

	public String getErrorCode()
	{
		return errorCode;
	}


	public void setErrorCode(String errorCode)
	{
		this.errorCode = errorCode;
	}


	public String getSubject()
	{
		return subject;
	}


	public void setSubject(String subject)
	{
		this.subject = subject;
	}


	public String getContent()
	{
		return content;
	}


	public void setContent(String content)
	{
		this.content = content;
	}

	public void setInitiator(String initiator) {
		this.initiator = initiator;
	}
	public String getInitiator() {
		if(this.initiator.startsWith("<") || this.initiator.endsWith(">")){
			return this.initiator.replace("<", "").replace(">", "");
		} else {
			return initiator;			
		}
	}
	public String getSender() {
		if(this.sender.startsWith("<") || this.sender.endsWith(">")){
			return this.sender.replace("<", "").replace(">", "");
		} else {
			return sender;			
		}
	}


	public void setSender(String sender) {
		this.sender = sender;
	}


	public String getRecipient() {
		return recipient;
	}


	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public Long getRwa_id() {
		return rwa_id;
	}

	public void setRwa_id(Long rwa_id) {
		this.rwa_id = rwa_id;
	}
	
	public void setLotusEmailId(Long lotusEmailId) {
		this.lotusEmailId = lotusEmailId;
	}
	
	public Long getLotusEmailId() {
		return lotusEmailId;
	}

	public Long getNumberOfAttachments() {
		return numberOfAttachments;
	}

	public void setOryginalId(String oryginalId) {
		this.oryginalId = oryginalId;
	}
	public String getOryginalId() {
		return oryginalId;
	}

	/**
	 * Nie zmienia� kolejno�ci
	 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
	 *
	 */
	enum Status{
		IN_PROGRESS,		//0
		NEW,				//1
		DONE,				//2
		ERROR,				//3
		CANCEL_BY_USER,		//4
	}
}
