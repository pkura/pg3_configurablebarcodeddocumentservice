package pl.compan.docusafe.parametrization.nfos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.bouncycastle.crypto.DSA;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Rwa;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.web.admin.absences.AbstractXlsReport;

/**

 */
public class DocsToDestroy extends AbstractXlsReport
{
    private Date dateFrom;
    private Date dateTo;

    /**
     * sta�e dla widoku, z kt�rego s� wyci�gana informacje do raportu
     */
    private static final String DOCUMENTS_TO_DESTROY_VIEW = "v_documents_to_destroy";
    private static final String STATUS_DOKUMENTU_COLUMNNAME = "statusdokumentu";
    private static final String KATEGORIA_ARCHIWALNA_COLUMNNAME = "KATEGORIAARCHIWALNA";
    private static final String DOCUMENT_ID_COLUMNNAME = "DOCUMENT_ID";
    private static final String SYSTEM_DZIEDZINOWY_COLUMNNAME = "system";
    private static final String DATA_ARCHIWIZACJI_COLUMNNAME = "archive_date";
    private static final String SZACOWANA_DATA_BRAKOWANIA_COLUMNNAME = "destroy_date";
    private static final String RODZAJ_DOKUEMNTU_COLUMNNAME = "rodzajDokumentu";
    
 
	public DocsToDestroy(String reportName)
    {
        super(reportName);

        columns = new String[]
        {
            "Lp.", 
            "ID dokumentu", 
            "Opis",
            "Status", 
            "Kategoria archiwalna", 
            "Opis z RWA",
            "System dziedzinowy",
            "Data archiwizacji", 
            "Przewidywana data zniszczenia", 
            "Rodzaj dokumentu"
        };
    }

    @Override
    protected void createEntries()
    {

        try
        {
            int rowCount = 1;
            int lp = 1;
            try {
            	FromClause from = new FromClause();
                TableAlias docsToDestroyView = from.createTable(DOCUMENTS_TO_DESTROY_VIEW);
                TableAlias rwaTable = from.createJoinedTable(DSApi.getTableName(Rwa.class), true, docsToDestroyView, FromClause.LEFT_OUTER, "$l.KATEGORIAARCHIWALNA = $r.id");
                
                SelectClause selectId = new SelectClause(true);
                SelectColumn colId = selectId.add(docsToDestroyView, DOCUMENT_ID_COLUMNNAME);
                SelectColumn colStatus = selectId.add(docsToDestroyView, STATUS_DOKUMENTU_COLUMNNAME);
                SelectColumn colSystemDziedzinowy = selectId.add(docsToDestroyView, SYSTEM_DZIEDZINOWY_COLUMNNAME);
                SelectColumn colDataArch = selectId.add(docsToDestroyView, DATA_ARCHIWIZACJI_COLUMNNAME);
                SelectColumn colDataBrakowania = selectId.add(docsToDestroyView, SZACOWANA_DATA_BRAKOWANIA_COLUMNNAME);
                SelectColumn colRodzajDokumentu = selectId.add(docsToDestroyView, RODZAJ_DOKUEMNTU_COLUMNNAME);
                
                SelectColumn colKatArchDesc = selectId.add(rwaTable, DSApi.getColumnName(Rwa.class, "description"));
                SelectColumn colKatArchAcHome = selectId.add(rwaTable, DSApi.getColumnName(Rwa.class, "acHome"));

                SelectQuery selectQuery = new SelectQuery(selectId, from, null, null);
                
				ResultSet rs = selectQuery.resultSet(DSApi.context().session().connection());
				
				while (rs.next())
	            {
					int i = -1;
                    HSSFRow row = sheet.createRow(rowCount++);
                    row.createCell(++i).setCellValue(new HSSFRichTextString(lp++ +""));
                    String docId = rs.getString(colId.getPosition());
                    if(docId!=null)
                    	row.createCell(++i).setCellValue(new HSSFRichTextString(docId));
                    else
                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
                    
                    if(docId!=null){
                    	String desc = OfficeDocument.find(Long.valueOf(docId)).getDescription();
                    	if(desc!=null)
                    		row.createCell(++i).setCellValue(new HSSFRichTextString(desc));
                    	else
                    		row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
                    } else
                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
                    
                    String status = rs.getString(colStatus.getPosition());
                    if(status!=null){
                    	if(status.equals("11"))
                    		row.createCell(++i).setCellValue(new HSSFRichTextString("Bie��cy"));
                    	else if(status.equals("12"))
                    		row.createCell(++i).setCellValue(new HSSFRichTextString("Archiwalny"));
                    	else if(status.equals("13"))
                    		row.createCell(++i).setCellValue(new HSSFRichTextString("Usuni�ty"));
                    } else
                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
                    
                    String katArchAcHome = rs.getString(colKatArchAcHome.getPosition());
                    if(katArchAcHome!=null){
                    	row.createCell(++i).setCellValue(new HSSFRichTextString(katArchAcHome));
                    }else
                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
                    
                    String katArchDesc = rs.getString(colKatArchDesc.getPosition());
                    if(katArchDesc!=null){
                    	row.createCell(++i).setCellValue(new HSSFRichTextString(katArchDesc));
                    }else
                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
                    
                    String system = rs.getString(colSystemDziedzinowy.getPosition());
                    if(system!=null){
                    	row.createCell(++i).setCellValue(new HSSFRichTextString(system));
                    }else
                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
                    
                    String dataArchiwizacji = rs.getString(colDataArch.getPosition());
                    if(dataArchiwizacji!=null)
                    	row.createCell(++i).setCellValue(new HSSFRichTextString(dataArchiwizacji.substring(0, 10)));
                    else
                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
                                        
                    String szacowanaDataBrakowania = rs.getString(colDataBrakowania.getPosition());
                    if(szacowanaDataBrakowania!=null)
                    	row.createCell(++i).setCellValue(new HSSFRichTextString(szacowanaDataBrakowania.substring(0, 10)));
                    else
                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
                    
                    String rodzajDok = rs.getString(colRodzajDokumentu.getPosition());
                    if(rodzajDok != null)
                    	row.createCell(++i).setCellValue(new HSSFRichTextString(OfficeDocument.find(Long.valueOf(docId)).getDocumentKind().getName()));
                    else
                    	row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
                    
                    
	            }
			} catch (HibernateException e) {
	            Logger.getLogger(DocsToDestroy.class.getName()).log(Level.SEVERE, null, e);
			} catch (SQLException e) {
	            Logger.getLogger(DocsToDestroy.class.getName()).log(Level.SEVERE, null, e);
			}
            
        }
        catch ( EdmException ex )
        {
            Logger.getLogger(DocsToDestroy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public Date getDateTo() {
		return dateTo;
	}

}
