package pl.compan.docusafe.parametrization.nfos;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;

/**
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class CzyPotrzebnyPodpisHandler implements DecisionHandler
{
    public static final String WYMAGANY_PODPIS = "wymaganyPodpis";
    public static final String BEZ_PODPISU = "bezPodpisu";

    public String decide(OpenExecution openExecution) {
        try {
            OfficeDocument doc = Jbpm4Utils.getDocument(openExecution);
            FieldsManager fm = doc.getFieldsManager();
            Boolean wymaganyPodpis = fm.getBoolean(NfosLogicable.WYMAGANE_UPO_CN);

            if(wymaganyPodpis)
                return WYMAGANY_PODPIS;

        } catch (EdmException e) {
            e.printStackTrace();
        }

        return BEZ_PODPISU;
    }
}
