package pl.compan.docusafe.parametrization.nfos;

import java.util.HashMap;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class NfosEmailRecipientDictionaryLogic extends DwrDictionaryBase {
	
	private static final Logger log = LoggerFactory.getLogger(NfosEmailRecipientDictionaryLogic.class);

	
	@Override
	public long add(Map<String, FieldData> values) throws EdmException {
		
		Person person = new Person();
		try {
			personFromValues(person, values);
		} catch (Exception e) {
			log.error("B��d podczas tworzenia kontrahenta", e);
		}
		
		try {			
			DSApi.beginTransacionSafely();
			person.create();
			DSApi.context().commit();
		} catch (Exception e) {
			log.error("B�ad podczas zapisu do bazy", e);
		}
				
		DSApi.beginTransacionSafely();
		long addResult = super.add(values);
		
		LotusRecipient lotusRecipient = LotusRecipientManager.findById(addResult);
		lotusRecipient.setPersonId(person.getId());
		lotusRecipient.save();
		DSApi.context().commit();
		DSApi.beginTransacionSafely();
		
		//potrzebne jeszcze bedzie utworzenie recipienta
		
		return addResult;
	}

	private void personFromValues(Person person, Map<String, FieldData> values) throws EdmException {
		String dictionaryCN = this.getName()+"_"; //ODBIORCY_DICT
		Map<String, Object> fieldsCN = new HashMap<String, Object>();
		String strValue = null;
		
		if (!values.containsKey("id") && !values.containsKey("ID"))
			throw new NullPointerException("No id key found in values map");
		String sId = null;
		
		if (values.containsKey("id"))
			sId = "id";
		else
			sId = "ID";
	
		for (String field : values.keySet()) {
			if(field.equals(sId)) continue;
			if(values.get(field).getData() != null)
					fieldsCN.put(field.replace(dictionaryCN, "").toUpperCase(), values.get(field).getData());
		}
				
//		for (Object val : values.keySet().toArray()) {
//			strValue = (String)val;
//			if(strValue.equals(sId)) continue;
//			fieldsCN.put(strValue.replace(dictionaryCN, "").toUpperCase(), values.get(strValue).getData());
//		}
		person.setParam(fieldsCN);
//		person.fromMap(fieldsCN);
	}
	
	@Override
	public int update(Map<String, FieldData> values) throws EdmException {
		
		return super.update(values);
	}
}
