package pl.compan.docusafe.parametrization.nfos;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Fabryka dla NFOS
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class NfosFactory implements Serializable
{
	private static final long serialVersionUID = 1L;
	private final static Logger log = LoggerFactory.getLogger(NfosFactory.class);
	/**
	 * Zwraca Dokument po epupaId, Wyszukuje w obu tabelach in oraz out
	 * @param wpuapId
	 * @return
	 * @throws EdmException 
	 */
	public static Document getDocumentByEpuapId(Long epuapId) throws Exception
	{
		Document document = null;
		
		try
		{
			Long id = null;
			PreparedStatement ps = DSApi.context().prepareStatement("SELECT DOCUMENT_ID FROM NFOS_DOC_IN where EPUAP_ID = ?");
			ps.setLong(1, epuapId);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next())
			{
				id = rs.getLong(1);
			}
			
			ps.close();
			rs.close();
			
			if(id == null)
			{
				ps = DSApi.context().prepareStatement("SELECT DOCUMENT_ID FROM NFOS_DOC_OUT where EPUAP_ID = ?");
				ps.setLong(1, epuapId);
				rs = ps.executeQuery();
				
				while(rs.next())
				{
					id = rs.getLong(1);
				}
			}
			
			document = Document.find(id);
		}
		catch (SQLException e) {
			log.error("",e);
		} catch (Exception e){
			log.error("",e);
			throw e;
		}
		
		return document;
	}
	
	/** Zwraca liste typu Dokument na podstawie epupaId.
	 * Wyszukuje w obu tabelach IN oraz OUT, ale szuka tylko dokument�w (nie UPO)
	 * @throws EdmException */
	public static List<Document> getDocumentsByEpuapId_withExceptions(Long epuapId) throws Exception
	{
		List<Document> docs = new ArrayList<Document>();
		Long id = null;
		try {
			PreparedStatement ps = DSApi.context().prepareStatement(
					"SELECT DOCUMENT_ID FROM NFOS_DOC_IN where EPUAP_ID = ? AND DOCCZYUPO = ?");
			ps.setLong(1, epuapId);
			ps.setInt (2, NfosLogicable.DOKUMENT);
			ResultSet rs = ps.executeQuery();
			
			while( rs.next()) {
				id = rs.getLong(1);
				docs.add(Document.find(id));
			}
			ps.close();
			rs.close();
			//-----------
			
			ps = DSApi.context().prepareStatement(
					"SELECT DOCUMENT_ID FROM NFOS_DOC_OUT where EPUAP_ID = ?");
			ps.setLong(1, epuapId);
			rs = ps.executeQuery();
			
			while( rs.next()) {
				id = rs.getLong(1);
				docs.add(Document.find(id));
			}
			ps.close();
			rs.close();
			//-----------
		} catch (SQLException e) {
			log.error("blad przy wyszukiwaniu dokumentow powiazanych (po epuapId)",e);
		} catch (Exception e){
			log.error("blad przy wyszukiwaniu dokumentow powiazanych (po epuapId)",e);
			throw e;
		}
		return docs;
	}
	
	/** Zwraca liste typu Dokument na podstawie epupaId.
	 * Wyszukuje w obu tabelach IN oraz OUT, ale szuka tylko dokument�w (nie UPO)
	 * i nie rzuca wyjatkow (zwraca null w najgorszym razie)
	 * 
	 * @see NfosFactory.getDocumentsByEpuapId_withExceptions */
	public static List<Document> getDocumentsByEpuapId (Long epuapId){
		try {
			return getDocumentsByEpuapId_withExceptions(epuapId);
		} catch (Exception e) {
			return null; // blad jest juz zalogowany i nie trzeba go obslugiwac
		}
	}
	
	/**
	 * Zwraca liste dokument�w kt�rych data utworzenia przekracza okres przechowywania podany jako parametr 
	 * @return  
	 * @throws Exception 
	 */
	public static List<Document> getDocumentForArchive(String storageTime) throws Exception{
		List<Document> docs = new ArrayList<Document>();
		Long id = null;
		Document doc = null;
		Calendar currentDay = Calendar.getInstance();
		Date storageDate = new Date(currentDay.getTimeInMillis()-DateUtils.DAY*14);
		
		try {
			String query = "select DOCUMENT_ID from NFOS_DOC_IN " +
					"where statusdokumentu = 11";
			PreparedStatement ps = DSApi.context().prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			
//			currentDay.setTime();
			while( rs.next()) {
				id = rs.getLong(1);
				doc = Document.find(id);
				if(doc.getCtime().before(storageDate))
					docs.add(doc);
			}
			ps.close();
			rs.close();
			//-----------
			
			query = "select DOCUMENT_ID from NFOS_DOC_OUT "
					+ "where statusdokumentu = 11";
			ps = DSApi.context().prepareStatement(query);
			rs = ps.executeQuery();

			doc = null;
			while (rs.next()) {
				id = rs.getLong(1);
				doc = Document.find(id);
				if (doc.getCtime().before(storageDate))
					docs.add(doc);
			}
			ps.close();
			rs.close();
			//-----------
		} catch (SQLException e) {
			log.error("Blad przy wyszukiwaniu dokumentow",e);
		} catch (Exception e){
			log.error("Blad przy wyszukiwaniu dokumentow",e);
			throw e;
		}
		return docs;
	}
}
