package pl.compan.docusafe.parametrization.nfos;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.axis2.AxisFault;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.JournalEntry;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.nfos.KomunikatorStub;
import pl.compan.docusafe.ws.client.nfos.KomunikatorStub.Query;
import pl.compan.docusafe.ws.client.nfos.KomunikatorStub.QueryResponse;

/**
 * Klasa definiuje metody do komunikacji z systemami dziedzinowymi
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class KomunikatorRemoteFactory 
{
	KomunikatorRemoteFactory(){}
	
	private static final Logger log = LoggerFactory.getLogger(KomunikatorRemoteFactory.class);
	
	private static boolean isDebugTest()
	{
		boolean isEnable =AvailabilityManager.isAvailable("nfos.komunikator.enable");
		log.debug(" nfos.komunikator.enable {}" + isEnable);
		return isEnable;
	}
	/**
	 * Definuje wysylanie dokumentu przychodzacego, wysyla do NFOS
	 * Tresc binarna dokument
	 * 
	 * Atrybuty
	 * numerKancelaryjny, dataObioru, ePUAPSkrzynka

	 * @param document
	 * @throws EdmException 
	 */
	public static String wyslijDokumentPrzychodzacy(Document document) throws EdmException
	{
		try
		{
			if(!isDebugTest())
				return "OK";
			
			FieldsManager fm = document.getFieldsManager();
			KomunikatorStub stub = getStub(fm);
			
			if(stub != null){
				Query query = new Query();
				query.setRodzajKomunikatu("dokumentPrzychodzacy");
				ustawTrescBinarna(query, document);
				ustawKO(query, document);
				ustawEPUAPSkrzynka(query, fm);
				//Do ustalenia jaka data ma tu by� wstawiana
				ustawDate(query, "dataOdbioru", document.getCtime());
				
				QueryResponse response = stub.query(query);
				String result = response.get_return();
				return result;
			}
			return "";
		}catch(Exception e){
			log.error("", e);
			throw new EdmException(e);
		}
	}
	


	/**
	 * Wysy�a upo do systemu dziedzinowego
	 * Tre�ci� binarn� jest UPO
		Atrybuty:
		numerKancelaryjny(dokumentu),dataOtrzymania,ePUAPSkrzynka

	 * @param document
	 * @return
	 */
	public static String wyslijUPO(Document document) throws EdmException
	{
		try
		{
			if(!isDebugTest())
				return "OK";
			
			FieldsManager fm = document.getFieldsManager();
			KomunikatorStub stub = getStub(fm);
			
			if(stub != null){
				Query query = new Query();
				query.setRodzajKomunikatu("upo");
				ustawTrescBinarna(query, document);
				ustawUPOKO(query, document);
				//do sprawdzenia czy jest tak samo ustawione jak w dokumencie
				ustawEPUAPSkrzynka(query, fm);
				//do sprawdzenia czy to ta data ma byc wstawiana
				ustawDate(query, "dataOtrzymania", document.getCtime());
				
				QueryResponse response = stub.query(query);
				String result = response.get_return();
				return result;
			}
			return "";
		}catch(Exception e){
			log.error("", e);
			throw new EdmException(e);
		}
	}
	
	/**
	 * Brak tre�ci binarnej
		Atrybuty:
		status,dataWyslania, numerKancelaryjny
	 * @throws EdmException 

	 */
	public static String wyslijStatus(Document document, String status, Date data) throws EdmException
	{
		try
		{
			if(!isDebugTest())
				return "OK";
			
			FieldsManager fm = document.getFieldsManager();
			KomunikatorStub stub = getStub(fm);
			
			if(stub != null){
				Query query = new Query();
				query.setRodzajKomunikatu("statusWysylki");
				ustawKO(query, document);
				
				//ustawic date otrzymania
				ustawDate(query, "dataOtrzymania", data);
				
				QueryResponse response = stub.query(query);
				String result = response.get_return();
				
				return result;
			}
			return "";
		}catch(Exception e){
			log.error("", e);
			throw new EdmException(e);
		}
	}
	
	private static KomunikatorStub getStub(FieldsManager fm) throws EdmException, AxisFault
	{
		Long systemId = fm.getLongKey(NfosLogicable.SYSTEM_CN);
		// jedynka oznacza system ogolny
		if(systemId != null && systemId != 1){
			NfosSystem system = NfosSystem.find(systemId);
			return getStub(system);
		}
		return null;
	}
	/**
	 * Zwraca stuba do po��cze� webserwisowych
	 * @TODO do konfiguracji wsdl sa w systemach dziedzinowych
	 * @return
	 * @throws AxisFault 
	 */
	
	private static KomunikatorStub getStub(NfosSystem system) throws AxisFault
	{
		return new KomunikatorStub(system.getRefValue());
	}
	
	private static void ustawTrescBinarna(Query query, Document document) throws EdmException, IOException
	{
		List<Attachment> attachments = document.getAttachments();
		
		if(attachments == null || attachments.isEmpty())
			throw new NullPointerException("Brak za��cznika!");

        AttachmentRevision rev = attachments.get(0).getMostRecentRevision();
		//narazie biore pierwszy z brzegu
        for(Attachment att : attachments)
        {
            if(att.getTitle().equalsIgnoreCase("Formularz"))
                rev = att.getMostRecentRevision();
        }
		
		if(rev == null)
			throw new NullPointerException("Brak rewizji za��cznika!");
		
		query.setTrescBinarna(AttachmentRevision.asBase64(rev));
	}

    /**
     * Ustawiamy KO dokumentu do kt�rego nale�y UPO
     */
    private static void ustawUPOKO(Query query, Document upo) throws EdmException
    {
        Long documentId = UpoLink.getUpoDocumentId(upo.getId());
        if(documentId==null) throw new NullPointerException("Nie znaleziono dokumentu wychodz�cego o id: " + upo.getId());
        List<JournalEntry> entries = JournalEntry.findByDocumentId(documentId);
        if(entries == null || entries.isEmpty())
            throw new NullPointerException("Nie znalazlem numeru KO");

        String ko = entries.get(0).getSequenceId().toString();
        ustawAtrybut(query,"numerKancelaryjny", ko);
    }

    /**
     * Ustawia numer kancelaryjny nadany przez system iDoc
     * @param query
     * @param document
     * @throws EdmException
     */
	private static void ustawKO(Query query, Document document) throws EdmException
	{
//		List<JournalEntry> entries = JournalEntry.findByDocumentId(document.getId());
//		if(entries == null || entries.isEmpty())
//			throw new NullPointerException("Nie znalazlem numeru KO");
		
		//sprawdzanie czy nale�y do dziennika prawid�owego: pisma przychodzace
//		String ko = entries.get(0).getSequenceId().toString();
		
		FieldsManager fm = document.getFieldsManager();
		String ko = fm.getStringValue(NfosLogicable.IDOC_NR);
		if(ko == null || ko.isEmpty())
			throw new NullPointerException("Nie znaleziono numeru KO dla dokumentu o ID=" + document.getId());
		ustawAtrybut(query,"numerKancelaryjny", ko);
	}
	
	private static void ustawEPUAPSkrzynka(Query query, FieldsManager fm) throws EdmException
	{
		Long senderId = fm.getLongKey(NfosLogicable.SENDER_CN);
		Person person = Person.find(senderId.longValue());
		
		ustawAtrybut(query, "ePUAPSkrzynka", person.getAdresSkrytkiEpuap());
	}
	
	
	/**
	 * Ustawia dat� w formacie YYYY-MM-DD HH24:Mi z podan� nazw� jako atrybut
	 * @param query
	 * @param nazwa
	 * @param data
	 */
	private static void ustawDate(Query query, String nazwa, Date data)
	{
		ustawAtrybut(query, nazwa, DateUtils.sqlDateTimeFormatNoSeconds.format(data));
	}
	
	/**
	 * Ustawia atrybut w query
	 * @param query
	 * @param nazwa
	 * @param wartosc
	 */
	private static void ustawAtrybut(Query query, String nazwa, String wartosc)
	{
		KomunikatorStub.Atrybut atryb = new KomunikatorStub.Atrybut();
		atryb.setNazwa(nazwa);
		atryb.setWartosc(wartosc);
		
		query.addAtrybuty(atryb);
	}
}