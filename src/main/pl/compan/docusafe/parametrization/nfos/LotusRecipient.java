package pl.compan.docusafe.parametrization.nfos;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

/**
 * Tabela przechowuje informacje dotycz�ce odbiorc�w emaili 
 *
 */
@Entity
@Table(name="NFOS_DOC_EMAIL_LISTA")
@SuppressWarnings("serial")
public class LotusRecipient implements Serializable
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="PERSON_ID", nullable=true)
	private Long personId;
	
	@Column(name="FIRSTNAME", nullable=true)
	private String firstname;

	@Column(name="LASTNAME", nullable=true)
	private String lastname;

	@Column(name="ORGANIZATION", nullable=true)
	private String organization;
	
	@Column(name="LOCATION", nullable=true)
	private String location;
	
	@Column(name="ORGANIZATIONDIVISION", nullable=true)
	private String organizationDivision;
	
	@Column(name="STREET", nullable=true)
	private String street;
	
	@Column(name="ZIP", nullable=true)
	private String zip;
	
	@Column(name="EMAIL", nullable=true)
	private String email;
	
	@Column(name="FAX", nullable=true)
	private String fax;
	
	@Column(name="NIP", nullable=true)
	private String nip;
	
	@Column(name="PESEL", nullable=true)
	private String pesel;
	
	@Column(name="REGON", nullable=true)
	private String regon;
	
	@Column(name="TITLE", nullable=true)
	private String title;
		
	public void save() throws EdmException
    {
        try
        {	
            DSApi.context().session().saveOrUpdate(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
 
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getOrganizationDivision() {
		return organizationDivision;
	}

	public void setOrganizationDivision(String organizationDivision) {
		this.organizationDivision = organizationDivision;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public String getRegon() {
		return regon;
	}

	public void setRegon(String regon) {
		this.regon = regon;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public Long getPersonId() {
		return personId;
	}

}
