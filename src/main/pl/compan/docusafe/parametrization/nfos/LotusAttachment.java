package pl.compan.docusafe.parametrization.nfos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Tre�� za��cznik�w z systemu lotus
 */
@Entity
@Table(name="NFOS_LOTUS_ATTACHMENT")
@SuppressWarnings("serial")
public class LotusAttachment implements Serializable 
{
	
	private static final Logger log = LoggerFactory.getLogger(LotusAttachment.class);
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="create_date", nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	
//	@ManyToOne (fetch = FetchType.EAGER)
//	@JoinColumn(name="lotus_email_id", referencedColumnName="lotus_email_id", nullable=true)
//	private LotusEmail lotusEmail;
//	public LotusEmail getLotusEmail() { return lotusEmail; }
//	public void setLotusEmail(LotusEmail lotusEmail) { this.lotusEmail = lotusEmail; }
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn(name="oryginal_id", referencedColumnName="oryginal_id", nullable=true)
	private LotusEmail lotusEmail;
	public LotusEmail getLotusEmail() { return lotusEmail; }
	public void setLotusEmail(LotusEmail lotusEmail) { this.lotusEmail = lotusEmail; }
	
	@Column(name="lotus_email_id", updatable=false)
	private Long lotusEmailId;

	public void save() throws EdmException
    {
        try
        {	
            DSApi.context().session().saveOrUpdate(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
	
	
	@Column(name = "mime", nullable = false)
	protected String mime;
	
	@Lob
	@Basic(fetch=FetchType.LAZY)
	@Column(name = "content_file", nullable = false)
	private byte[] content;
	
//	@Column(name = "content_file", nullable =false)
//	protected String content;

	@Column(name = "file_name", nullable = false)
	private String fileName;
	
	@Column(name = "oryginal_id", insertable=false, updatable=false)
	private String oryginalId;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getMime() {
		return mime;
	}

	public void setMime(String mime) {
		this.mime = mime;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}
	
	
	@SuppressWarnings("unchecked")
	public static synchronized List<LotusEmail> list(Long emailId) throws EdmException
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(LotusEmail.class);
			c.add(Restrictions.eq("emailId", emailId));
			return (List<LotusEmail>) c.list();
		}catch(Exception e){
			log.error("", e);
			return new ArrayList<LotusEmail>();
		}
	}
	public void setLotusEmailId(Long lotusEmailId) {
		this.lotusEmailId = lotusEmailId;
	}
	public Long getLotusEmailId() {
		return lotusEmailId;
	}
	public void setOryginalId(String oryginalId) {
		this.oryginalId = oryginalId;
	}
	public String getOryginalId() {
		return oryginalId;
	}
}
