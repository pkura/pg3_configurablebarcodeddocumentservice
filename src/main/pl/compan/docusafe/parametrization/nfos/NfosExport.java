package pl.compan.docusafe.parametrization.nfos;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

/**
 * Tabela przechowuje dokumenty wymagane do exportu do system�w dziedzinowych,
 * wraz ze statusem wysy�ania, czasem dodania do kolejki i czasem wys�ania
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 */
@Entity
@Table(name="NFOS_EXPORT")
@SuppressWarnings("serial")
public class NfosExport implements Serializable
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="document_id", nullable=false)
	private Long documentId;
	
	/** status wysy�ania dokumentu */
	@Enumerated
	private NfosStatus status;
	
	/** data utworzenia tego wpisu (a nie np. dokumentu, kt�rego ten wpis dotyczy) */
	@Column(name="create_date", nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	
	@Column(name="send_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date sendDate;

	@Column(name="error_message")
	private String errorMessafe;
	
	public NfosExport()
	{
		super();
		status = NfosStatus.ACTIVE;
		createDate = Calendar.getInstance().getTime();
	}

	/**
	 * Nie zmienia� kolejno�ci
	 */
	enum NfosStatus{
		ACTIVE, //0
		DONE, //1
		ERROR, //2
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Long getDocumentId()
	{
		return documentId;
	}

	public void setDocumentId(Long documentId)
	{
		this.documentId = documentId;
	}

	public NfosStatus getStatus()
	{
		return status;
	}

	public void setStatus(NfosStatus status)
	{
		this.status = status;
	}

	public Date getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}

	public Date getSendDate()
	{
		return sendDate;
	}

	public void setSendDate(Date sendDate)
	{
		this.sendDate = sendDate;
	}
	
	public void setErrorMessafe(String errorMessafe) {
		this.errorMessafe = errorMessafe;
	}

	public String getErrorMessafe() {
		return errorMessafe;
	}

	public void create() throws EdmException
    {
        try
        {
        	if(id != null)
        		sendDate = Calendar.getInstance().getTime();
        	
            DSApi.context().session().saveOrUpdate(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
}
