package pl.compan.docusafe.parametrization.nfos;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

import javax.crypto.SealedObject;

import com.asprise.util.tiff.q;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeCase.Query;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Serwis wykonywany na prze�omie roku kalendarzowego (31 grudzie�/1 stycze�).
 * Sprawy (dokumenty do nich nale�ace) po up�ywie dw�ch lat, licz�c od pierwszego stycznia 
 * roku nast�puj�cego po roku, w kt�rym sprawa zosta�a zako�czona.
 * 
 * @author <a href="mailto:jerzy.pirog@docusafe.pl"> Jerzy Pir�g </a>
 *
 */
public class NfosArchiveService extends ServiceDriver implements Service
{
	private static final Logger log = LoggerFactory.getLogger(NfosArchiveService.class);
	private Timer timer;
	
	@Override
	protected void start() throws ServiceException
	{
		log.info("start uslugi NfosArchiveService");
		timer = new Timer(true);
		timer.schedule(new Archiver(), (long) 2*1000 ,(long) 1*DateUtils.HOUR);
		console(Console.INFO,"Start uslugi NfosArchiveService");
	}
	
	class Archiver extends TimerTask
	{
		boolean opened;
		@Override
		public void run()
		{
			if(shouldRunArchiver()){
				log.trace("START");
				try
				{
					opened = DSApi.openContextIfNeeded();
					OfficeCase[] sprawy = searchCases();
					for(OfficeCase sprawa : sprawy){
						for(Object o : sprawa.getDocuments()){
							try{
								OfficeDocument doc = (OfficeDocument) o;
								Minimal_Logic.getInstance().globalActionDocument(doc,0,"moveToArchive");	
							}catch (Exception e) {
								log.error("",e);
								console(Console.ERROR, "Blad [ "+e+" ]");
							}
						}
						sprawa.setArchived(true);
						sprawa.setArchivedDate(new Date());
					}
					if(!DSApi.context().inTransaction())
						DSApi.context().begin();
					setNextActivityDate();
					DSApi.context().commit();
				}catch(Exception e)
				{
					log.error("",e);
					console(Console.ERROR, "Blad [ "+e+" ]");
				}
				finally
				{
					log.trace("STOP");
					console(Console.INFO, "Koniec wysylania");
					DSApi.closeContextIfNeeded(opened);
				}
			}else{
				log.trace("POMIJANIE ARCHIWIZACJI");
				console(Console.INFO, "POMIJANIE ARCHIWIZACJI");
			}
		}		
	}
	
	@Override
	protected void stop() throws ServiceException
	{
		System.out.println("Stop us�ugi NfosArchiveService");
		if (timer != null)
			timer.cancel();
	}

	@Override
	protected boolean canStop()
	{
		return false;
	}
	private boolean shouldRunArchiver(){
		
		boolean opened = false;
		try {
			opened = DSApi.openContextIfNeeded();

			Date now = new Date();
			Long nextActivityTime = DSApi.context().systemPreferences().node("NfosArchiveService").getLong("nextActivityTime", 0);
			
			if(nextActivityTime.compareTo(Long.valueOf(0))==0){
				setNextActivityDate();
				return false;
			}
			
			Calendar nextActivityDate = new GregorianCalendar();
			nextActivityDate.setTime(new Date(nextActivityTime));
//			nextActivityDate.setTime(new Date(Long.valueOf("1373299089251")));
			
			if(nextActivityDate.getTime().before(now))
			{ 
				console(Console.INFO, "Uruchamiam serwis archiwizuj�cy");
				return true;
			}else{
				console(Console.INFO, "Nast�pne uruchomienie serwisu archiwizuj�cego: "+nextActivityDate.getTime());
			}
		} catch (EdmException e) {
			log.trace("B��d podczas metody shouldRunArchiver"+e);
			console(Console.ERROR, "B��d podczas metody shouldRunArchiver"+e);
		}finally{
			DSApi.closeContextIfNeeded(opened);
		}
		return false;
	}
	private Date setNextActivityDate(){
		Calendar nextTime = Calendar.getInstance();
		nextTime.add(Calendar.YEAR, 1);
		nextTime.set(Calendar.DAY_OF_YEAR, 1);
		nextTime.set(Calendar.HOUR_OF_DAY, 1);
		DSApi.context().systemPreferences().node("NfosArchiveService").putLong("nextActivityTime", nextTime.getTimeInMillis());
		return nextTime.getTime();
	}
	
	private OfficeCase[] searchCases(){
		Query query = new Query(0, 10);
		Calendar date = Calendar.getInstance();
		date.add(Calendar.YEAR, -3);
		date.set(Calendar.MONTH, Calendar.DECEMBER);
		date.set(Calendar.DAY_OF_MONTH, 31);
		date.set(Calendar.HOUR_OF_DAY, 23);
		date.set(Calendar.MINUTE, 59);
		query.setFinishDateTo(date.getTime());
		query.setArchived(false);
		try {
			SearchResults<OfficeCase> ret = OfficeCase.search(query);
			return ret.results();
		} catch (EdmException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
