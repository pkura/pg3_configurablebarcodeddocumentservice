package pl.compan.docusafe.parametrization.nfos;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.DateUtils;


/**
 * @author Jerzy Pir�g <jerzy.pirog@docusafe.pl>
 */
public class DocumentsToDestroyReport extends Report
{

	private Date dateFrom;
	private Date dateTo;
	private UniversalTableDumper dumper;

	public void initParametersAvailableValues() throws ReportException
	{
		super.initParametersAvailableValues();
	}

	@Override
	public void doReport() throws Exception
	{
		for( ReportParameter rp : params )
		{
			if( rp.getFieldCn().equals("dateFrom") )
			{
				if(!rp.getValueAsString().equals("") && rp.getValueAsString() != null)
					setDateFrom(DateUtils.commonDateFormat.parse(rp.getValueAsString()));
			}
			if( rp.getFieldCn().equals("dateTo") )
			{
				if(!rp.getValueAsString().equals("") && rp.getValueAsString() != null)
					setDateTo(DateUtils.commonDateFormat.parse(rp.getValueAsString()));
			}
		}

		dumper = new UniversalTableDumper();
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "raport.xls");
		poiDumper.openFile(xlsFile);
		FileOutputStream fis = new FileOutputStream(xlsFile);

		doReportForAll().getWorkbook().write(fis);
		fis.close();
		dumper.addDumper(poiDumper);

	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public Date getDateTo() {
		return dateTo;
	}

	private DocsToDestroy doReportForAll() throws Exception
	{
		DocsToDestroy xlsReport = new DocsToDestroy("Raport dokument�w do brakowania");
		xlsReport.setDateFrom(dateFrom);
		xlsReport.setDateTo(dateTo);
		
		xlsReport.generate();
		return xlsReport;
	}


}
