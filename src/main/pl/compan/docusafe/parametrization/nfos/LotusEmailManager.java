package pl.compan.docusafe.parametrization.nfos;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Manager do Obiektu LotusEmail
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class LotusEmailManager
{
	private static final Logger log = LoggerFactory.getLogger(LotusEmailManager.class);
	/**
	 * Listuje Dokumenty do wyslania do systemu dziedzinowego.
	 * 
	 * @param status - zostan� wyszukane dokumenty o takim statusie
	 * @return lista dokument�w, przy b��dzie zwraca pust� list�
	 * @throws EdmException
	 */
	@SuppressWarnings("unchecked")
	public static synchronized List<LotusEmail> list(LotusEmail.Status status) throws EdmException
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(LotusEmail.class);
			c.add(Restrictions.eq("status", status));
			return (List<LotusEmail>) c.list();
		}catch(Exception e){
			log.error("", e);
			return new ArrayList<LotusEmail>();
		}
	}
	
	/**
	 * Wyszukuje wszystkich dokument�w po importID
	 * @param importId 
	 * @return
	 * @throws EdmException
	 */
	@SuppressWarnings("unchecked")
	public static synchronized List<LotusEmail> findByImportId(Long importId) throws EdmException
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(LotusEmail.class);
			c.add(Restrictions.eq("importId", importId));
			return (List<LotusEmail>) c.list();
		}catch(Exception e){
			log.error("", e);
			return new ArrayList<LotusEmail>();
		}
	}

	/**
	 * Wyszukuje wszystkie rekordy o polu oryginal_id
	 * 
	 * @param importId
	 * @return null je�li wyst�pi Exception
	 * @throws EdmException
	 */
	@SuppressWarnings("unchecked")
	public static synchronized List<LotusEmail> findByOryginalId(String oryginalId) throws EdmException
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(LotusEmail.class);
			c.add(Restrictions.eq("oryginalId", oryginalId));
			return (List<LotusEmail>) c.list();
		}catch(Exception e){
			log.error("", e);
			return null;
		}
	}
}
