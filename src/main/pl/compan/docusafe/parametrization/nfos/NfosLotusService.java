package pl.compan.docusafe.parametrization.nfos;

import java.sql.Connection;
import java.util.*;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.imports.dsi.DSIAttribute;
import pl.compan.docusafe.service.imports.dsi.DSIBean;
import pl.compan.docusafe.service.imports.dsi.DSIManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.service.mail.MailerDriver;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa przegl�da tabele, do kt�rej wrzucane s� dokumenty z lotusa
 * i tworzy wpisy w dsi_import_tabel
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class NfosLotusService extends ServiceDriver implements Service
{
	private static final Logger log = LoggerFactory.getLogger(NfosLotusService.class);
	private DSIManager manager = new DSIManager();
	private Timer timer;
	
	@Override
	protected void start() throws ServiceException
	{
		log.info("start uslugi NfosLotusService");
		timer = new Timer(true);
		timer.schedule(new CreateDSI(), (long) 2*1000 ,(long) 1*DateUtils.MINUTE);
		console(Console.INFO,"Start uslugi NfosLotusService");
	}
	
	class CreateDSI extends TimerTask
	{

		@Override
		public void run()
		{
			log.trace("START");
			try
			{
				checkForNew();
				
				DSApi.openAdmin();
				@SuppressWarnings("deprecation")
				java.sql.Connection con = DSApi.context().session().connection();
				List<LotusEmail> lotusEmails = LotusEmailManager.list(LotusEmail.Status.NEW);
				console(Console.INFO, "Znalazlem: " + lotusEmails.size());
				
				for(LotusEmail email : lotusEmails)
				{
					try
					{
						DSIBean dsiBean = supplyImport(email);
						saveDsiBeans(dsiBean, con);
						email.setImportId(dsiBean.getImportId());
						email.setStatus(LotusEmail.Status.DONE);
						saveLotusEmail(email);
						
					}catch(Exception e){
						log.error("", e);
						console(Console.ERROR, "B��d przy id:" + email.getId());
						email.setStatus(LotusEmail.Status.ERROR);
						email.setParseDate(Calendar.getInstance().getTime());
					}
				}
			}catch(Exception e)
			{
				log.error("",e);
				console(Console.ERROR, "B��d [ "+e+" ]");
			}
			finally
			{
				log.trace("STOP");
				console(Console.INFO, "Koniec wysylania");
//				if(context != null)
//					try {
//						context.close();
//					} catch (NamingException e) {
//						log.error(e.getMessage(),e);
//					}
				DSApi._close();
			}
		}
		
		private void checkForNew() {			
			try {
				DSApi.openAdmin();
				java.sql.Connection con = DSApi.context().session().connection();
				List<LotusEmail> lotusEmails = LotusEmailManager.list(LotusEmail.Status.IN_PROGRESS);
				int numberOfAttachments;
				
				for (LotusEmail lotusEmail : lotusEmails) {
					List<LotusEmail> listaDublikatow = LotusEmailManager.findByOryginalId(lotusEmail.getOryginalId());
					if (listaDublikatow.size() > 1) {
						DSApi.context().begin();
						lotusEmail.setStatus(LotusEmail.Status.ERROR);
						lotusEmail.setErrorCode("B��d w kolumnie oryginal_id, pewne rekordy si� powtarzaj�.");
						lotusEmail.setParseDate(new Date());
						DSApi.context().session().saveOrUpdate(lotusEmail);	
						DSApi.context().commit();

						MailerDriver mailerDriver = (MailerDriver) ServiceManager.getService(Mailer.NAME);
						
						
						String subject = "Powiadomienie o b��dnym imporcie maila.";
						
						String msg = 
							"Podczas importu wyst�pi� nieoczekiwany b��d dokumentu \""+ lotusEmail.getSubject() +"\".\n" +
							"Prosimy o zg�oszenie tego b��du aby nasi specjali�my mogli sie nim zaj��.\n\n" +
							"--" +
							"Docusafe";
						try {
							mailerDriver.send(lotusEmail.getInitiator(), "DocuSafe", subject, msg);							
						} catch (Exception e2) {
							log.error("B��d podczs wysy�ania maila.", e2);
						}
					} else {
						numberOfAttachments = lotusEmail.getAttachments().size();
						if (lotusEmail.getNumberOfAttachments() == numberOfAttachments) {
							DSApi.context().begin();
							lotusEmail.setStatus(LotusEmail.Status.NEW);
							DSApi.context().session().saveOrUpdate(lotusEmail);
							DSApi.context().commit();
						}
					}
				}
			} catch (EdmException e){
				log.error("", e);
			} catch (Exception e) {
				log.error(e.getMessage());
				DSApi.context()._rollback();
			} finally {
				DSApi._close();
			}
		}

		private void saveLotusEmail(LotusEmail email) throws EdmException
		{
			try
			{
				DSApi.context().begin();
				
				email.create();
				
				DSApi.context().commit();
			}catch(Exception e ){
				log.error("Blad: ", e);
				DSApi.context()._rollback();
				throw new EdmException(e.getMessage());
			}
			
		}

		/**
		 * Zapisuje wpisy do tabeli dsi_import_table
		 * @param beans
		 * @param con
		 */
		private void saveDsiBeans(DSIBean bean, Connection con) throws EdmException
		{
			try
			{
				DSApi.context().begin();
				
				manager.saveDSIBean(bean, con);
				
				DSApi.context().commit();
			}catch(Exception e ){
				log.error("Blad: ", e);
				DSApi.context()._rollback();
				throw new EdmException(e.getMessage());
			}
		}
		
		private DSIBean supplyImport(LotusEmail email)
		{
			DSIBean dsiBean = new DSIBean();
			List<DSIAttribute> dsiAttributes = new ArrayList<DSIAttribute>();
			
			dsiAttributes.add(new DSIAttribute(NfosLogicable.EMAIL_SUBJECT, email.getSubject()));
			dsiAttributes.add(new DSIAttribute(NfosLogicable.EMAIL_CONTENT, email.getContent()));
			dsiAttributes.add(new DSIAttribute(NfosLogicable.EMAIL_SENDER, email.getSender()));
			dsiAttributes.add(new DSIAttribute(NfosLogicable.EMAIL_RWA, email.getRwa_id().toString()));
			dsiAttributes.add(new DSIAttribute(NfosLogicable.DSI_EMAIL_RECIPIENT, email.getRecipient()));
			dsiAttributes.add(new DSIAttribute(NfosLogicable.EMAIL_INITIATOR, email.getInitiator()));
//			dsiAttributes.addAll(parseRecipients(email.getRecipient()));
//			dsiAttributes.add(new DSIAttribute(NfosLogicable.EMAIL_DATA, email.getParseDate().toString()));
			dsiAttributes.add(new DSIAttribute(NfosLogicable.EMAIL_DATA, email.getCreateDate().toString()));
			
			dsiBean.setAttributes(dsiAttributes);
			
			dsiBean.setBoxCode("noneed");
			dsiBean.setDocumentKind("nfosEmail");
			dsiBean.setImageArray(null);
//			dsiBean.setFilePath(file.getName());
			dsiBean.setImportKind(1);
			dsiBean.setSubmitDate(Calendar.getInstance().getTime());
			dsiBean.setUserImportId(null);
			dsiBean.setImageName(null);
			dsiBean.setImportStatus(1);
			dsiBean.setImportId(null);
			return dsiBean;
		}
	}
	
	@Override
	protected void stop() throws ServiceException
	{

	}

	@Override
	protected boolean canStop()
	{
		return false;
	}

}
