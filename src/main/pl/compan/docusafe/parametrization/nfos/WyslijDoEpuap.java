package pl.compan.docusafe.parametrization.nfos;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.service.epuap.EpuapExportDocument;
import pl.compan.docusafe.service.epuap.EpuapExportManager;

import java.util.Map;

/**
 * Zapisuje do kolejki dokument do wyslania do ePUAP.
 * Odczytuje z dockindu, czy wymagane jest UPO (cn pola brany z interfeju NfosLogicable) */
@SuppressWarnings("serial")
public class WyslijDoEpuap implements ExternalActivityBehaviour {
	private static final Logger log = LoggerFactory.getLogger(WyslijDoEpuap.class);
	
	@Override
	public void execute(ActivityExecution activityExecution) throws Exception {
		Long docId = Jbpm4Utils.getDocumentId(activityExecution);
		OutOfficeDocument doc = (OutOfficeDocument) OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		
		Boolean wymaganeUPO = fm.getBoolean(NfosLogicable.WYMAGANE_UPO_CN);
		Integer usluga = EpuapExportDocument.USLUGA_SKRYTKA;
		if (wymaganeUPO != null && wymaganeUPO == true)
			usluga = EpuapExportDocument.USLUGA_DORECZYCIEL;
		
		// Trzeba ustawic delivery na OutOfficeDocumentDelivery.DELIVERY_EPUAP, bo inaczej nie pojdzie z powodu sprawdzenia:
		// OutOfficeDocumentDelivery.DELIVERY_EPUAP.equalsIgnoreCase(document.getDelivery().getName())
		
		doc.setDelivery( OutOfficeDocumentDelivery.findByName(OutOfficeDocumentDelivery.DELIVERY_EPUAP) );
		
		// TODO : tu istotny aspekt. Ponizej false oznacza, ze przed wyslaniem nie musi byc nadany numer KO.
		// true oznaczaloby, ze musi. Zabezpieczyc tworzenie numeru KO i zmienic na true.
		EpuapExportManager.sendToEpuap(docId, false, usluga);
		
		activityExecution.waitForSignal(); // TODO
	}
	
	@Override
	public void signal(ActivityExecution activityExecution, String signalName, Map<String, ?> stringMap) throws Exception {
		if (signalName != null)
			activityExecution.take(signalName);
		else 
			activityExecution.takeDefaultTransition();
	}
}
