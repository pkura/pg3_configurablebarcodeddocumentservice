/**
 * 
 */
package pl.compan.docusafe.parametrization.yetico;

import java.sql.PreparedStatement;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.parametrization.yetico.hb.PrewencjaItem;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class CanUpdatePrewencjaDictionary extends DwrDictionaryBase {
	
	private static final Logger logger = LoggerFactory.getLogger(CanUpdatePrewencjaDictionary.class);
	public static final String TABLE_NAME = "dsg_yetico_prewencja_item";
	
	@Override
	public long add(Map<String, FieldData> values) throws EdmException {
		try {
			if (values.containsKey(PrewencjaDictionary.MIRROR_ID) && values.get(PrewencjaDictionary.MIRROR_ID) != null && 
					values.containsKey(PrewencjaDictionary.IMIE_NAZWISKO) && values.get(PrewencjaDictionary.IMIE_NAZWISKO)!= null) {
				Long id = values.get(PrewencjaDictionary.MIRROR_ID).getLongData();
				Integer userId = values.get(PrewencjaDictionary.IMIE_NAZWISKO).getIntegerData();
				PrewencjaItem item = (PrewencjaItem)DSApi.context().session().get(PrewencjaItem.class, id);
				item.setImieNazwisko(userId);
				DSApi.context().session().update(item);
				DSApi.context().session().flush();
				return id;
			} else {
				return -1L;
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return -1L;
		}
	}
		/*try {
			if (values.containsKey(PrewencjaDictionary.ID_MIRROR) && values.get(PrewencjaDictionary.ID_MIRROR) != null && 
					values.containsKey(PrewencjaDictionary.IMIE_NAZWISKO) && values.get(PrewencjaDictionary.IMIE_NAZWISKO)!= null) {
				FieldData id = values.get(PrewencjaDictionary.ID_MIRROR);
				values.put("ID",id);
				return (long)super.update(values);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return -1L;
		}
		return -1L;
	}*/
		/*PreparedStatement ps = null;
		try {
			if (values.containsKey(PrewencjaDictionary.ID_MIRROR) && values.get(PrewencjaDictionary.ID_MIRROR) != null && 
					values.containsKey(PrewencjaDictionary.IMIE_NAZWISKO) && values.get(PrewencjaDictionary.IMIE_NAZWISKO)!= null) {
				Long id = values.get(PrewencjaDictionary.ID_MIRROR).getLongData();
				Integer name = Integer.valueOf(values.get(PrewencjaDictionary.IMIE_NAZWISKO).getStringData());
				StringBuilder query = new StringBuilder();
				query.append("UPDATE ").append(TABLE_NAME).append(" SET imie_nazwisko = ").append(name).append(" WHERE id = ").append(id);
				ps = DSApi.context().prepareStatement(query);
				int psi = 1;
				for (String cn : values.keySet())
				{
					if (values.get(cn).getData() == null)
					{
						i++;
						continue;
					}
					FieldData fd = values.get(cn);
					if (fd.getData() != null)
						fd.getData(ps, psi);
					psi++;
				}
				values.put(sId, fieldId);
				return (long)ps.executeUpdate();
			} else {
				return -1L;
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return -1L;
		} finally {
			DSApi.context().closeStatement(ps);
		}*/

    @Override
    public int update(Map<String, FieldData> values) throws EdmException {

        values.put("ID", values.get("MIRROR_ID"));
        return super.update(values);
    }

}
