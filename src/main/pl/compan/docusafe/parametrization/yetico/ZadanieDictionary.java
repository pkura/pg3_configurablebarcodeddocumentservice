package pl.compan.docusafe.parametrization.yetico;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.LinkValue;
import pl.compan.docusafe.core.dockinds.field.LinkField;
import pl.compan.docusafe.core.dockinds.field.NonColumnField;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class ZadanieDictionary extends DwrDictionaryBase {
    public static final String DODATKOWE_BADANIA = "DODATKOWE_BADANIA";
    public static final String DZIALANIA_KORYGUJACE = "DZIALANIA_KORYGUJACE";
    public static final String OST_DECYZJE_DO_REALIZACJI = "OST_DECYZJE_DO_REALIZACJI";
    public static final String OST_DECYZJE_DO_REALIZACJI_EDITABLE = "OST_DECYZJE_DO_REALIZACJI_EDITABLE";
    public static final String PROP_DECYZJE_DO_REALIZACJI = "PROP_DECYZJE_DO_REALIZACJI";
    public static final String ZAD_REALIZOWANE_W_WERYFIKACJI = "ZAD_REALIZOWANE_W_WERYFIKACJI";
    private static final String DODATKOWE_BADANIA_EDITABLE = "DODATKOWE_BADANIA_EDITABLE";
    private static final String DZIALANIA_KORYGUJACE_EDITABLE = "DZIALANIA_KORYGUJACE_EDITABLE";
    private static final String ZAD_REALIZOWANE_W_WERYFIKACJI_EDITABLE = "ZAD_REALIZOWANE_W_WERYFIKACJI_EDITABLE";
    private static final String PROP_DECYZJE_DO_REALIZACJI_EDITABLE = "PROP_DECYZJE_DO_REALIZACJI_EDITABLE";

    public static final String ZADANIE_CN = Docusafe.getAdditionProperty("zadanie.CN") != null ? Docusafe
	    .getAdditionProperty("zadanie.CN") : "zadanie";

    public long add(Map<String, FieldData> values) throws EdmException {
	boolean inTransatiocn = true;
	Map<String, Object> valuesForSet = new HashMap<String, Object>();

	long ret = -1;


	for (String cn : values.keySet()) {
	    String newCn = cn.replace(getName(), "");
	    // jesli pierwszy znak to _, np.: _STATUS_DOKUMENTU
	    if (newCn.charAt(0) == '_') {
		newCn = newCn.substring(1);
	    }

	    if (!newCn.equals("OPIS_REALIZACJI") && !newCn.equals("STATUS") && !newCn.equals("ID")
		    && (values.get(cn).getData() == null || values.get(cn).getData().equals(""))) {
		return ret;
	    }
	    valuesForSet.put(newCn, values.get(cn).getData());
	}

	// pracownik zlecajacy
	DSUser uzytkownikRejestrujacy = DSApi.context().getDSUser();
	Long uzytkownikRejestrujacyId = uzytkownikRejestrujacy.getId();
	valuesForSet.put("PRACOWNIK_ZLECAJACY", uzytkownikRejestrujacyId);

	// typ zadania
	String summary = "Zadanie";
	valuesForSet.put("TYP", getType());

	OutOfficeDocument document = new OutOfficeDocument();
	document.setInternal(true);
	document.setCreatingUser(uzytkownikRejestrujacy.getName());
	document.setCurrentAssignmentAccepted(Boolean.FALSE);
	DocumentKind documentKind = DocumentKind.findByCn("zadanie");
	document.setAuthor(uzytkownikRejestrujacy.getName());
	document.setSummary(summary != null ? summary : documentKind.getName());
	document.setAssignedDivision(DSDivision.ROOT_GUID);
	document.setDocumentKind(documentKind);
	document.setForceArchivePermissions(false);
	document.setFolder(Folder.getRootFolder());
	document.setSummary(summary);
	document.create();

	Long newDocumentId = document.getId();

	documentKind.setOnly(newDocumentId, valuesForSet, false);
	documentKind.logic().archiveActions(document, DocumentLogic.TYPE_INTERNAL_OFFICE);
	documentKind.logic().documentPermissions(document);

	documentKind.logic().archiveActions(document, 0);
	documentKind.logic().documentPermissions(document);
	DSApi.context().session().save(document);

	if (getType() != 20) {
	    documentKind.logic().onStartProcess(document, null);
	}

	TaskSnapshot.updateByDocument(document);

	return newDocumentId;
    }

    private Integer getType() {
        String s = getName();
	if (getName().equalsIgnoreCase(ZAD_REALIZOWANE_W_WERYFIKACJI_EDITABLE))
	    return 10;
	else if (getName().equalsIgnoreCase(PROP_DECYZJE_DO_REALIZACJI_EDITABLE))
	    return 15;
	else if (getName().equalsIgnoreCase(OST_DECYZJE_DO_REALIZACJI_EDITABLE))
	    return 20;
	else if (getName().equalsIgnoreCase(DZIALANIA_KORYGUJACE_EDITABLE))
	    return 25;
	else if (getName().equalsIgnoreCase(DODATKOWE_BADANIA_EDITABLE))
	    return 30;
	else
	    return 1;
    }

    
    
    public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset) throws EdmException {
	List<Map<String, Object>> ret = Lists.newArrayList();
	// LEASING_DOC_1,DSG_LEASINGG_1,TYP_DOKUMENTU_1,STATUS_DOKUMENTU_1,PRZYCZYNA_1,OPIS_1,ATTACH_1
	String documentId = "";
	if (!(values.containsKey("id") && !(values.get("id").getData().toString()).equals("")))
	    return ret;

	documentId = values.get("id").getData().toString();

	Statement stat = null;
	ResultSet result = null;
	String dictionaryName = getName();
	boolean contextOpened = true;
	try {
	    if (!DSApi.isContextOpen()) {
		DSApi.openAdmin();
		contextOpened = false;
	    }
	    stat = DSApi.context().createStatement();

	    StringBuilder select = new StringBuilder(
		    "select status, typ, pracownik_odpowiedzialny, pracownik_zlecajacy, decyzja, opis, opis_realizacji, od, do from DSG_YETICO_ZADANIE where DOCUMENT_ID = ")
		    .append(documentId);

	    result = stat.executeQuery(select.toString());
	    while (result.next()) {
		Map<String, Object> row = Maps.newLinkedHashMap();
		Long status = result.getLong("status");
		Long typ = result.getLong("typ");
		Long pracownikOdpowiedzialny = result.getLong("pracownik_odpowiedzialny");
		Long pracownikZlecajacy = result.getLong("pracownik_zlecajacy");
		Long decyzja = result.getLong("decyzja");
		String opis = result.getString("opis");
		String opisRealiZacji = result.getString("opis_realizacji");
		Date odDate = result.getDate(8);
		Date doDate = result.getDate("do");
		row.put("id", documentId);

		row.put(dictionaryName + "_OPIS", opis);
		if (!decyzja.equals(new Long(0))) {
			row.put(dictionaryName + "_DECYZJA", decyzja);
		}
		row.put(dictionaryName + "_OD", odDate);
		row.put(dictionaryName + "_DO", doDate);
		row.put(dictionaryName + "_OPIS_REALIZACJI", opisRealiZacji);
		row.put(dictionaryName + "_PRACOWNIK_ODPOWIEDZIALNY", pracownikOdpowiedzialny);
		row.put(dictionaryName + "_STATUS", status);

        for (pl.compan.docusafe.core.dockinds.field.Field f : getOldField().getFields())
        {
            String fCn = f.getCn();
            String column = f.getColumn();
            if (isMultiple())
            {
                fCn = fCn.substring(0, fCn.lastIndexOf("_"));
                if (!(f instanceof NonColumnField))
                    column = column.substring(0, column.lastIndexOf("_"));
            }
            if (f.getType().equals(pl.compan.docusafe.core.dockinds.field.Field.LINK))
            {
                Document doc = Document.find(Long.parseLong((String) row.get("id")), false);
                String label = doc.getTitle();
                row.put(dictionaryName+ "_LINK", new LinkValue(label, ((LinkField)f).getLogicField().createLink((String) row.get("id"))));
            }
        }

		ret.add(row);
	    }

	} catch (Exception ie) {
	    throw new EdmException(ie.getMessage());
	} finally {
	    DSApi.context().closeStatement(stat);
	    if (!contextOpened && DSApi.isContextOpen())
		DSApi.close();
	}
	return ret;
    }

    public Map<String, Object> getValues(String id) throws EdmException {
	return super.getValues(id);
    }
}
