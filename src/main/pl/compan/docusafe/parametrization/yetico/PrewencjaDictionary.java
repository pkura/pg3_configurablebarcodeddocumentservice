/**
 * 
 */
package pl.compan.docusafe.parametrization.yetico;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;

import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class PrewencjaDictionary extends DwrDictionaryBase {
	
	public static final String ID = "ID";
	public static final String NR_LISTY = "NR_LISTY";
	public static final String AKCJA_PREWENCYJNA = "AKCJA_PREWENCYJNA";
	public static final String OPIS = "OPIS";
	public static final String PLANOWANA_DATA_REALIZACJI = "PLANOWANA_DATA_REALIZACJI";
	public static final String PLANOWANA_DATA_ZAKONCZENIA = "PLANOWANA_DATA_ZAKONCZENIA";
	public static final String DATA_REALIZACJI = "DATA_REALIZACJI";
	public static final String CZAS_REALIZACJI = "CZAS_REALIZACJI";
	public static final String STATUS = "STATUS";
	public static final String IMIE_NAZWISKO = "IMIE_NAZWISKO";
	public static final String MIRROR_ID = "MIRROR_ID";
	public static final String PREWENCJA_OPIS = "PREWENCJA_OPIS";
	public static final String PREWENCJA_AKCJA_PREWENCYJNA = "PREWENCJA_AKCJA_PREWENCYJNA";
	public static final String PREWENCJA_PLANOWANA_DATA_REALIZACJI = "PREWENCJA_PLANOWANA_DATA_REALIZACJI";
	public static final String PREWENCJA_PLANOWANA_DATA_ZAKONCZENIA = "PREWENCJA_PLANOWANA_DATA_ZAKONCZENIA";
	
	@Override
	public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset) throws EdmException {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		Statement statement = null;
		boolean contextOpened = true;
		try {
			if (!DSApi.isContextOpen()) {
				DSApi.openAdmin();
				contextOpened = false;
			}
			statement = DSApi.context().createStatement();
			StringBuilder query = new StringBuilder();
			String selectedColumns = "id, nr_listy, akcja_prewencyjna, opis, planowana_data_realizacji, planowana_data_zakonczenia, " +
					"data_realizacji, czas_realizacji, pi.status, imie_nazwisko, mirror_id";
			if (values.containsKey(ID.toLowerCase()) && values.get(ID.toLowerCase()).getData() != null) {
				Long id = values.get(ID.toLowerCase()).getLongData();
				query.append("SELECT ").append(selectedColumns).append(" FROM dsg_yetico_prewencja_item pi").append(" WHERE id = ").append(id);
			} else {
				//nie sprawdzam czy jest taki klucz, gdyz zawsze bedzie on ustawiony, najwyzej wartosc jego bedzie nullem
				String description = values.get(PREWENCJA_OPIS).getStringData();
				query.append("SELECT ").append(selectedColumns.replace("pi.status", "status")).append(" FROM(SELECT ").append(selectedColumns)
					.append(", ROW_NUMBER() OVER (ORDER BY id) AS RowNum FROM dsg_yetico_prewencja_item pi join dsg_yetico_prewencja p on pi.akcja_prewencyjna = p.document_id where p.status = 2000 and opis LIKE '%")
					.append(description).append("%'");
				if (values.containsKey(PREWENCJA_AKCJA_PREWENCYJNA) && values.get(PREWENCJA_AKCJA_PREWENCYJNA).getData() != null) {
					Integer action = values.get(PREWENCJA_AKCJA_PREWENCYJNA).getIntegerData();
					query.append(" AND pi.akcja_prewencyjna LIKE '%").append(action).append("%'") ;
				}
				if (values.containsKey(PREWENCJA_PLANOWANA_DATA_REALIZACJI) && values.get(PREWENCJA_PLANOWANA_DATA_REALIZACJI).getData() != null) {
					Date realizationDate = values.get(PREWENCJA_PLANOWANA_DATA_REALIZACJI).getDateData();
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
					query.append(" AND pi.planowana_data_realizacji >='").append(formatter.format(realizationDate)).append("'");
				}
				if (values.containsKey(PREWENCJA_PLANOWANA_DATA_ZAKONCZENIA) && values.get(PREWENCJA_PLANOWANA_DATA_ZAKONCZENIA).getData() != null) {
					Date finishDate = values.get(PREWENCJA_PLANOWANA_DATA_ZAKONCZENIA).getDateData();
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
					query.append(" AND pi.planowana_data_zakonczenia <='").append(formatter.format(finishDate)).append("'");
				}
				query.append(" AND id NOT IN(SELECT FIELD_VAL FROM dsg_yetico_lista_prewencyjna_multiple_value WHERE FIELD_CN='PREWENCJA')) AS tempTable WHERE tempTable.RowNum BETWEEN ")
				.append(offset).append(" AND ").append(offset + limit);
			}	
			ResultSet resultQuery = statement.executeQuery(query.toString());
			while (resultQuery.next()) {
				Long id = resultQuery.getLong(ID);
				String nrListy = resultQuery.getString(NR_LISTY);
				Integer akcjaPrewencyjna = resultQuery.getInt(AKCJA_PREWENCYJNA);
				String opis = resultQuery.getString(OPIS);
				Date planowanaDataRealizacji = resultQuery.getTimestamp(PLANOWANA_DATA_REALIZACJI);
				Date planowanaDataZakonczenia = resultQuery.getTimestamp(PLANOWANA_DATA_ZAKONCZENIA);
				Date dataRealizacji = resultQuery.getTimestamp(DATA_REALIZACJI);
				Integer czasRealizacji = resultQuery.getInt(CZAS_REALIZACJI);
				Integer status = resultQuery.getInt(STATUS);
				Integer imieNazwisko = resultQuery.getInt(IMIE_NAZWISKO);
				Long idMirror = resultQuery.getLong(MIRROR_ID);
				
				
				Map<String, Object> row = new LinkedHashMap<String, Object>();
				row.put(ListaPrewencyjnaLogic.PREWENCJA + "_" + ID, id);
				row.put(ListaPrewencyjnaLogic.PREWENCJA + "_" + NR_LISTY, nrListy);
				row.put(ListaPrewencyjnaLogic.PREWENCJA + "_" + AKCJA_PREWENCYJNA, akcjaPrewencyjna);
				row.put(ListaPrewencyjnaLogic.PREWENCJA + "_" + OPIS, opis);
				row.put(ListaPrewencyjnaLogic.PREWENCJA + "_" + PLANOWANA_DATA_REALIZACJI, planowanaDataRealizacji);
				row.put(ListaPrewencyjnaLogic.PREWENCJA + "_" + PLANOWANA_DATA_ZAKONCZENIA, planowanaDataZakonczenia);
				row.put(ListaPrewencyjnaLogic.PREWENCJA + "_" + DATA_REALIZACJI, dataRealizacji);
				if (czasRealizacji != null) {
					row.put(ListaPrewencyjnaLogic.PREWENCJA + "_" + CZAS_REALIZACJI, czasRealizacji);
				}
				row.put(ListaPrewencyjnaLogic.PREWENCJA + "_" + STATUS, status);
				row.put(ListaPrewencyjnaLogic.PREWENCJA + "_" + IMIE_NAZWISKO, imieNazwisko);
				row.put(ListaPrewencyjnaLogic.PREWENCJA + "_" + MIRROR_ID, idMirror);
				result.add(row);
				
			}
		} catch (Exception e) {
			throw new EdmException(e.getMessage());
		} finally {
			DSApi.context().closeStatement(statement);
			if (!contextOpened && DSApi.isContextOpen())
				DSApi.close();
		}
		return result;
	}
	
	@Override
	public long add(Map<String, FieldData> values) throws EdmException {
		return -1L;
	}
	
	@Override
	public int remove(String id) throws EdmException {
		return -1;
	}
	
	@Override
	public int update(Map<String, FieldData> values) throws EdmException {

        return -1;
	}
}
