package pl.compan.docusafe.parametrization.yetico.jbpm;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.joda.time.DateTime;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.parametrization.polcz.jbpm.CreateApplicationInErpListener;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class GeneratePreventions implements ExternalActivityBehaviour{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(CreateApplicationInErpListener.class);
	
	@Override
	public void execute(ActivityExecution execution) throws Exception {
		
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		Document doc = Document.find(docId);
		Integer documentId = doc.getFieldsManager().getDocumentId().intValue();
		
		String sqlSelect = "select * from dsg_yetico_prewencja where document_id = " + docId + ";";
		String sqlSelectId = "select id from dsg_yetico_prewencja_item where akcja_prewencyjna =" + docId +";";
		String sqlInsert = "insert into dsg_yetico_prewencja_item(nr_listy, akcja_prewencyjna, opis, planowana_data_realizacji, "+
						   "planowana_data_zakonczenia) "+
						   "values(?,?,?,?,?);";
		Statement stmtSelect = null;
		Statement stmtSelectId = null;
		PreparedStatement psInsert = null;
		
		Date planowanaDataRealizacji; //data rozpocz�cia prewencji
		Timestamp dataPoczatkowa; // data + czas rozpoczecia prewencji
		
		long startTime, timeToAdd, subtract;
		int czasRealizacji, czestotliwosc;
		
		String opis;
		
		try{
			
			stmtSelect = DSApi.context().createStatement();
			ResultSet rs = stmtSelect.executeQuery(sqlSelect);
			
			if(rs.next()){
				czestotliwosc = rs.getInt("czestotliwosc");
				dataPoczatkowa = rs.getTimestamp("data_poczatkowa");
				planowanaDataRealizacji = rs.getDate("data_poczatkowa");
				opis = rs.getString("opis_czynnosci");
				czasRealizacji = rs.getInt("planowany_czas_realizacji");
			
				DateTime startDateYoda = new DateTime(planowanaDataRealizacji);
				Date sDate = planowanaDataRealizacji;
				
				timeToAdd = (long)czasRealizacji*60*60*1000; // liczba godzin potrzebna na realizacje pojedynczej prewencji 
				startTime = ((Date)dataPoczatkowa).getTime(); // dzien rozpoczecia prewencji
				subtract = dataPoczatkowa.getTime() - planowanaDataRealizacji.getTime(); // godzina rozpoczecia prewencji
				
				while(sDate.getYear() == dataPoczatkowa.getYear()){
					try{
						
						psInsert = DSApi.context().prepareStatement(sqlInsert);
						psInsert.setString(1, null);
						psInsert.setInt(2, documentId);
						psInsert.setString(3, opis);
						psInsert.setTimestamp(4, new Timestamp(sDate.getTime() + subtract));
						psInsert.setTimestamp(5, new Timestamp ((sDate.getTime() + subtract + timeToAdd)));
						psInsert.executeUpdate();
						startTime += (long)((long)czestotliwosc*1000L*60L*60L*24L*7L);
						
						startDateYoda = startDateYoda.plusDays(czestotliwosc*7);
						sDate = startDateYoda.toDate();
					}catch(Exception e){
						log.error(e.getMessage(), e);
						throw new EdmException("B��d generacji prewencji na rok - INSERT :" + e.getMessage());
					}
				}
			
			}
			
			stmtSelectId = DSApi.context().createStatement();
			ResultSet rsSelectId = stmtSelectId.executeQuery(sqlSelectId);
			Map<String, ArrayList<Long>> toSet = new HashMap<String, ArrayList<Long>>();
			ArrayList <Long> idList = new ArrayList<Long>();
			
			while(rsSelectId.next()){
				idList.add(rsSelectId.getLong("id"));
			}
			
			toSet.put("PREWENCJA", idList);
			doc.getDocumentKind().setOnly(docId, toSet);
			
		}catch (Exception e){
			log.error(e.getMessage(), e);
			throw new EdmException("B��d generacji prewencji na rok - SELECT:" + e.getMessage());
		}
		finally{
			DSApi.context().closeStatement(stmtSelect);
			DSApi.context().closeStatement(stmtSelectId);
			DSApi.context().closeStatement(psInsert);
		}
		
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
		// TODO Auto-generated method stub
		
	}
	

}
