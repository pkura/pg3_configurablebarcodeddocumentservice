/**
 * 
 */
package pl.compan.docusafe.parametrization.yetico.jbpm;

import java.util.List;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Maciej Starosz <maciej.starosz@docusafe.com>
 *
 */
public class CheckStatus implements DecisionHandler{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static Logger LOG = LoggerFactory.getLogger(CheckStatus.class);
	private String statusNaprawy;
	
	public String decide(OpenExecution openExecution) {
        try {
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            LOG.info("documentid = " + docId + " Status naprawy = " + fm.getKey(statusNaprawy).toString());
            
            if(fm.getKey(statusNaprawy).toString().equals("101")){// Naprawa wykonana
            	return "wykonana";
            } else if (fm.getKey(statusNaprawy).toString().equals("102")) {
            	return "niewykonana";
            } else {
            	return "w_trakcie";
            }
            
        } catch (EdmException ex) {
            LOG.error(ex.getMessage(), ex);
            return "error";
        }
	}

}
