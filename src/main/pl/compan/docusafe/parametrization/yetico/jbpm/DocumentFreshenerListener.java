/**
 * 
 */
package pl.compan.docusafe.parametrization.yetico.jbpm;

import java.util.Date;
import java.util.List;

import org.jbpm.api.ExecutionService;
import org.jbpm.api.TaskService;
import org.jbpm.api.listener.EventListenerExecution;
import org.jbpm.api.task.Participation;
import org.jbpm.api.task.Task;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Constants;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.parametrization.yetico.ZadaniePrewencyjneLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class DocumentFreshenerListener extends AbstractEventListener {
	
	private static final Logger logger = LoggerFactory.getLogger(DocumentFreshenerListener.class);
	private static final StringManager sm = StringManager.getManager(Constants.Package);
	public static final String REFRESH = "refresh";
	public static final String PREVIOUS_ASSIGNEE = "previousAssignee";
	public static final String CURRENT_ASSIGNEE = "currentAssignee";
	
	public void notify(EventListenerExecution eventListenerExecution) throws Exception {
		Boolean contextOpened = null;
		try {
			contextOpened = DSApi.openContextIfNeeded();
			Boolean refresh = Boolean.valueOf(eventListenerExecution.getVariable(REFRESH) + "");
			
			if (refresh != null && Boolean.TRUE.equals(refresh)) {
				OfficeDocument doc = Jbpm4Utils.getDocument(eventListenerExecution);
	        	String activityId = null;
	        	Iterable<String> taskIds = Jbpm4ProcessLocator.taskIds(doc.getId());
	        	for (String taskId : taskIds)
	        		activityId = taskId;
				activityId = cleanActivityId(activityId);
				TaskService ts = Jbpm4Provider.getInstance().getTaskService();
				List<Participation> taskParticipations = ts.getTaskParticipations(activityId);
				String userName = null;
				String term = eventListenerExecution.getVariable(PreventionTaskDateVerification.TERM) + "";
				
				//przypadek gdy proces jest na etapie: OCZEKIWANIE_NA_REALIZACJE
				if(term != null && PreventionTaskDateVerification.BEFORE_TERM.equals(term)) {
					Date reassignmentTimeMirror = (Date)eventListenerExecution.getVariable(PreventionTaskDateVerification.REASSIGNMENT_TIME_MIRROR);
					eventListenerExecution.setVariable(Jbpm4WorkflowFactory.REASSIGNMENT_TIME_VAR_NAME, reassignmentTimeMirror);
					Boolean accepted = (Boolean) eventListenerExecution.getVariable(ZadaniePrewencyjneLogic.ACCEPTED_VAR_NAME);
					if (Boolean.TRUE.equals(accepted)) {
						for (Participation participation : taskParticipations) {
							userName = participation.getUserId();
						}
						ts.assignTask(activityId, userName);
					}
				}
				
				//przypadek gdy proces jest na etapie: PONOWNE_ZLECENIE_ZADANIA
				else if(term != null && PreventionTaskDateVerification.AFTER_TERM.equals(term)) {
					ExecutionService executionService = Jbpm4Provider.getInstance().getExecutionService();
					userName = DSApi.context().getDSUser().getName();
					Task task = ts.getTask(activityId);
					executionService.setVariable(task.getExecutionId(), CURRENT_ASSIGNEE, userName);
				}
				
				DSApi.context().begin();
				TaskSnapshot.updateByDocument(doc);
				DSApi.context().commit();
				eventListenerExecution.setVariable(REFRESH, Boolean.FALSE);
				logger.info(sm.getString("DocumentFreshenerListenerRefresh", doc.getId()));
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			DSApi.closeContextIfNeeded(contextOpened);
		}
	}
	
    private String cleanActivityId(String activityId){
        return activityId.replaceAll("[^,]*,","");
    }
    
}
