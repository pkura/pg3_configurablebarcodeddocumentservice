package pl.compan.docusafe.parametrization.yetico.jbpm;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sdicons.json.validator.impl.predicates.True;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.LinkValue;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.yetico.hb.Niezgodnosc;
import pl.compan.docusafe.parametrization.yetico.hb.NiezgodnoscWew;
import pl.compan.docusafe.parametrization.yetico.hb.Wyrob;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Maps;

@SuppressWarnings("serial")
public class StworzReklamacjeWewnetrzna implements ExternalActivityBehaviour {
	private final static Logger LOG = LoggerFactory.getLogger(StworzReklamacjeWewnetrzna.class);

	@Override
	public void execute(ActivityExecution activityExecution) throws Exception {

		Long docId = Long.valueOf(activityExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		@SuppressWarnings("unchecked")
		List<Long> idsWyrobow = (List<Long>) fm.getKey("WYROB");

		for (Long id : idsWyrobow) {
			stworzNiezgodnosc(fm, id, doc);
		}

	}

	private void stworzNiezgodnosc(FieldsManager fm, Long id, OfficeDocument doc) throws Exception {
		OutOfficeDocument newDocument = null;

		try {
			newDocument = new OutOfficeDocument();
			DocumentKind rekWewDocumentKind = DocumentKind.findByCn(NiezgodnoscWew.NIEZGODNOSC_WEW_CN);

			newDocument.setCreatingUser(DSApi.context().getPrincipalName());
			newDocument.setSummary("Niezgodność stworzona z reklamacji zewnetrznej");
			newDocument.setDocumentKind(rekWewDocumentKind);

			newDocument.create();
			Long newDocumentID = newDocument.getId();

			setAfterCreate(newDocument);

			Map<String, Object> valuesToSet = Maps.newHashMap();

            //BLOKUJE POLA PRZENIESIONE Z REK ZEW
            valuesToSet.put("PRZENIESIONE_Z_REK_ZEW", true);
			Wyrob wyrob = (Wyrob) DSApi.getObject(Wyrob.class, id);
			if (wyrob != null && wyrob.getRodzaj() != null)
				valuesToSet.put("RODZAJ", wyrob.getRodzaj());
                valuesToSet.put("PRZENIESIONE_Z_REK_ZEW_RODZAJ", true);
			if (wyrob != null && wyrob.getTyp() != null) {
				valuesToSet.put("TYP", wyrob.getTyp());
                valuesToSet.put("PRZENIESIONE_Z_REK_ZEW_TYP", true);
			}
			
			valuesToSet.put("AUTOR", DSUser.findByUsername(doc.getAuthor()).getId());
			valuesToSet.put("DATA_WYKRYCIA", (Date) fm.getValue("DATA_ZGLOSZENIA"));
			valuesToSet.put("DOT_ZAKLADU", fm.getKey("DOT_ZAKLADU"));
			
			Map<String, Object> klient = DwrDictionaryFacade.dictionarieObjects.get("KLIENT").getValues(fm.getKey("KLIENT").toString());
			if (klient != null && klient.get("KLIENT_NAZWA") != null){
				valuesToSet.put("MIEJSCE_WYKRYCIA", klient.get("KLIENT_NAZWA"));
                valuesToSet.put("PRZENIESIONE_Z_REK_ZEW_MIEJSCE_WYKRYCIA", true);
            }
			StringBuilder sb = new StringBuilder();
			if (wyrob.getDataGodzinaProdukcji() != null)
				sb.append("Data produkcji wyrobu: " + DateUtils.formatCommonDate((Date) wyrob.getDataGodzinaProdukcji()) + " \n");
			if (wyrob.getGniazdo() != null)
				sb.append("Gniazdo: " + wyrob.getGniazdo() + " \n");
			if (wyrob.getZmiana() != null)
				sb.append("Zmiana: " + wyrob.getZmiana() + " \n");
			valuesToSet.put("INFORMACJE", sb.toString());
			valuesToSet.put("UWAGI_LINK", doc.getId());
			valuesToSet.put("DATA_REJESTRACJI", new Date());
			valuesToSet.put("WYROB", id);
            valuesToSet.put("STATUS_NIEZGODNOSCI", 10);

			rekWewDocumentKind.setOnly(newDocumentID, valuesToSet);

			DSApi.context().session().save(newDocument);
			newDocument.getDocumentKind().logic().onStartProcess(newDocument, null);
			TaskSnapshot.updateByDocument(newDocument);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw e;
		}
	}

	private void setAfterCreate(OfficeDocument document) throws EdmException {
		((OutOfficeDocument) document).setInternal(true);
		try {
			java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

			DSUser author = DSApi.context().getDSUser();
			String user = author.getName();
			String fullName = author.getLastname() + " " + author.getFirstname();

			perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

			Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
			perms.addAll(documentPermissions);
			((AbstractDocumentLogic) document.getDocumentKind().logic()).setUpPermission(document, perms);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
	}

}
