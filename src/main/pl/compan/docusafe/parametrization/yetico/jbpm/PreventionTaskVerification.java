/**
 * 
 */
package pl.compan.docusafe.parametrization.yetico.jbpm;

import java.util.List;
import java.util.Map;

import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.yetico.ListaPrewencyjnaLogic;
import pl.compan.docusafe.parametrization.yetico.hb.PrewencjaItem;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class PreventionTaskVerification extends AbstractEventListener {
	
	private static final Logger logger = LoggerFactory.getLogger(PreventionTaskVerification.class);
	public static final Integer WYKONANA = 101;
	private String field;
	
	public void notify(EventListenerExecution eventListenerExecution) throws Exception {
		Long documentId = (Long)eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY);
		OfficeDocument document = OfficeDocument.find(documentId);
		FieldsManager fm = document.getFieldsManager();
		List<Long> preventionTasks = (List<Long>)fm.getKey(field);
		if (preventionTasks != null && preventionTasks.size() != 0) {
			for (Long id : preventionTasks) {
				if (id != null) {
					PrewencjaItem item = (PrewencjaItem)DSApi.context().session().get(PrewencjaItem.class, id);
					if (item == null || !item.getStatus().equals(WYKONANA)) {
						throw new EdmException("Wszystkie zadania prewencyjne musz\u0105 by\u0107 zrealizowane");
					}
				}
			}
		}
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

}
