package pl.compan.docusafe.parametrization.yetico.jbpm;

import java.util.List;

import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class WeryfikacjaZakonczona extends AbstractEventListener
{
	private final static Logger LOG = LoggerFactory.getLogger(WeryfikacjaZakonczona.class);
	private String field;
	private String required;
    private String isEmptyList;

	public void notify(EventListenerExecution eventListenerExecution) throws Exception
	{

		Long docId = Long.valueOf(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		List<Long> zadIds = (List<Long>)fm.getKey(field);
		if (zadIds != null && !zadIds.isEmpty())
		{
			for (Long id : zadIds)
			{
				OfficeDocument zadanie = (OfficeDocument) OfficeDocument.find(id, false);
				EnumItem item = zadanie.getFieldsManager().getEnumItem("STATUS");
				if (item != null)
				{
					if (!item.getCn().equals("ZREALIZOWANE"))
					{
						if (field.equalsIgnoreCase("ZAD_REALIZOWANE_W_WERYFIKACJI"))
							throw new EdmException("Wszystkie zadania realizowane w ramach weryfikacji musz� zosta� zako�czone, aby mo�na by�o przekaza� reklamacj� dalej.");
						else if (field.equals("OST_DECYZJE_DO_REALIZACJI"))
							throw new EdmException("Aby m�c przekaza� reklamacj� do naliczenia jej koszt�w, wszystkie zlecone zadania musz� zosta� zrealizowane.");
						else if (field.equals("DODATKOWE_BADANIA"))
							throw new EdmException("Przes�anie niezgodno�ci dalej mo�liwe jest tylko po zleceniu oraz zrealizowaniu wszystkich dodatkowych bada�.");
						else if (field.equals("DZIALANIA_KORYGUJACE")&& fm.getKey(isEmptyList)==null) {
                            throw new EdmException("Przekazanie niezgodno�ci do naliczenia koszt�w mo�liwe jest tylko w przypadku zlecenia dzia�a� koryguj�cych oraz ich zrealizowania.");
                        }
                        else if (field.equals("DZIALANIA_KORYGUJACE")&& fm.getKey(isEmptyList)!=null) {
                            throw new EdmException("Nale�y zrealizowa� dzia�ania koryguj�ce, aby zako�czy� prac�.");
                        }
                    }
				}
			}

		}
        else {
        	if (required.equalsIgnoreCase("true")) {
        		if (field.equalsIgnoreCase("ZAD_REALIZOWANE_W_WERYFIKACJI"))
					throw new EdmException("Wszystkie zadania realizowane w ramach weryfikacji musz� zosta� zako�czone, aby mo�na by�o przekaza� reklamacj� dalej.");
				else if (field.equals("OST_DECYZJE_DO_REALIZACJI"))
					throw new EdmException("Aby m�c przekaza� reklamacj� do naliczenia jej koszt�w, wszystkie zlecone zadania musz� zosta� zrealizowane.");
			//	else if (field.equals("DODATKOWE_BADANIA"))
			//		throw new EdmException("Przes�anie niezgodno�ci dalej mo�liwe jest tylko po zleceniu oraz zrealizowaniu wszystkich dodatkowych bada�.");
				else if (field.equals("DZIALANIA_KORYGUJACE"))
					throw new EdmException("Przekazanie niezgodno�ci do dalszego procesowania mo�liwe jest tylko w przypadku zlecenia dzia�a� koryguj�cych oraz ich zrealizowania. Prosz� zleci� dzia�ania koryguj�ce.");
			//	else
			//		throw new EdmException("Przekazanie reklamacji do dalszego procesowania mo�liwe jest tylko w przypadku zlecenia dodatkowcyh bada� oraz ich zrealizowania. Prosz� zleci� badania dodatkowe.");
        	}
        }
	}

	public String getField()
	{
		return field;
	}

	public void setField(String field)
	{
		this.field = field;
	}

	public String getRequired() {
		return required;
	}

	public void setRequired(String required) {
		this.required = required;
	}
}
