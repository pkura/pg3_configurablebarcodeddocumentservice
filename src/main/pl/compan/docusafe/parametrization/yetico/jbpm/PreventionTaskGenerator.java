/**
 * 
 */
package pl.compan.docusafe.parametrization.yetico.jbpm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.yetico.ListaPrewencyjnaLogic;
import pl.compan.docusafe.parametrization.yetico.ZadaniePrewencyjneLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class PreventionTaskGenerator implements ExternalActivityBehaviour {
	
	private static final Logger logger = LoggerFactory.getLogger(PreventionTaskGenerator.class);
	/**
	 *  Nazwa kodowa(Cn) dokumentu: zadanie_prewencyjne
	 */
	public static final String ZADANIE_PREWENCYJNE = "zadanie_prewencyjne";
	
	public void execute(ActivityExecution activityExecution) throws Exception {
		Long documentId = (Long)activityExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY);
		OfficeDocument document = OfficeDocument.find(documentId);
		FieldsManager fm = document.getFieldsManager();
		List<Long> preventionIds = (List<Long>)fm.getKey(ListaPrewencyjnaLogic.PREWENCJA);
		for (Long id : preventionIds) {
			createPreventionTask(id);
		}
	}
	
	private void createPreventionTask(Long id) {
		try {
			OutOfficeDocument newDocument = new OutOfficeDocument();
			DocumentKind preventionTaskDocKind = DocumentKind.findByCn(ZADANIE_PREWENCYJNE);
			newDocument.setCreatingUser(DSApi.context().getDSUser().getName());
			newDocument.setSummary("Zadanie prewencyjne stworzone z dokumentu: Zlecenie wykonania prewencji");
			newDocument.setDocumentKind(preventionTaskDocKind);
			newDocument.create();
			newDocument.setInternal(true);
			
			Set<PermissionBean> perms = new HashSet<PermissionBean>();
			DSUser author = DSApi.context().getDSUser();
			String user = author.getName();
			String fullName = author.getLastname() + " " + author.getFirstname();
			perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(newDocument);
			perms.addAll(documentPermissions);
			((AbstractDocumentLogic) newDocument.getDocumentKind().logic()).setUpPermission(newDocument, perms);
			
			Map<String, Object> valuesToSet = new HashMap<String, Object>();
			valuesToSet.put(ZadaniePrewencyjneLogic.OSOBA_ZLECAJACA, author.getId());
			valuesToSet.put(ListaPrewencyjnaLogic.PREWENCJA, id);
			preventionTaskDocKind.setOnly(newDocument.getId(), valuesToSet);
			
			DSApi.context().session().save(newDocument);
			newDocument.getDocumentKind().logic().onStartProcess(newDocument, null);
			TaskSnapshot.updateByDocument(newDocument);
		} catch(Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {

	}

}
