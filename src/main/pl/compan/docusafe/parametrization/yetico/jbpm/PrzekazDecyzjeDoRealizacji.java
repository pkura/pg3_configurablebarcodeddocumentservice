package pl.compan.docusafe.parametrization.yetico.jbpm;

import java.util.List;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.parametrization.yetico.ZadanieDictionary;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

@SuppressWarnings("serial")
public class PrzekazDecyzjeDoRealizacji implements ExternalActivityBehaviour {
    private final static Logger LOG = LoggerFactory.getLogger(PrzekazDecyzjeDoRealizacji.class);

    @Override
    public void execute(ActivityExecution activityExecution) throws Exception {

	Long docId = Long.valueOf(activityExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
	OfficeDocument doc = OfficeDocument.find(docId);
	FieldsManager fm = doc.getFieldsManager();

	@SuppressWarnings("unchecked")
	List<Long> idsDecyzji = (List<Long>) fm.getKey(ZadanieDictionary.OST_DECYZJE_DO_REALIZACJI);

	for (Long id : idsDecyzji) {
	    OfficeDocument zadanie = OfficeDocument.find(id);
	    zadanie.getDocumentKind().logic().onStartProcess(zadanie, null);
	    TaskSnapshot.updateByDocument(zadanie);
	}

    }

    @Override
    public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
    }

}
