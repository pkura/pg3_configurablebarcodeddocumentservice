/**
 * 
 */
package pl.compan.docusafe.parametrization.yetico.jbpm;

import java.util.Date;
import java.util.Map;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Constants;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.parametrization.yetico.ListaPrewencyjnaLogic;
import pl.compan.docusafe.parametrization.yetico.PrewencjaDictionary;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class PreventionTaskDateVerification implements DecisionHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(PreventionTaskDateVerification.class);
	private static final StringManager sm = StringManager.getManager(Constants.Package);
	public static final String TERM = "term";
	public static final String AFTER_TERM = "afterTerm";
	public static final String BEFORE_TERM = "beforeTerm";
	public static final String  REASSIGNMENT_TIME_MIRROR = "reassignment-time-mirror";
	
	public String decide(OpenExecution openExecution) {
		Boolean contextOpened = null;
		OfficeDocument doc = null;
		try {
			contextOpened = DSApi.openContextIfNeeded();
			openExecution.setVariable(DocumentFreshenerListener.REFRESH, Boolean.TRUE);
			doc = Jbpm4Utils.getDocument(openExecution);
			FieldsManager fm = doc.getFieldsManager();
			Map<String, Object> prevention = (Map<String, Object>)fm.getValue(ListaPrewencyjnaLogic.PREWENCJA);
			if (!prevention.containsKey(PrewencjaDictionary.PREWENCJA_PLANOWANA_DATA_ZAKONCZENIA) && 
					prevention.get(PrewencjaDictionary.PREWENCJA_PLANOWANA_DATA_ZAKONCZENIA) == null) {
				throw new Exception(sm.getString("PreventionTaskDateVerificationNoFinishDate"));
			}
			Date finishDate = (Date)prevention.get(PrewencjaDictionary.PREWENCJA_PLANOWANA_DATA_ZAKONCZENIA);
			Date actualDate = new Date();
			if (actualDate.after(finishDate)) {
				openExecution.setVariable(TERM, AFTER_TERM);
				return AFTER_TERM;
			} else {
				openExecution.setVariable(TERM, BEFORE_TERM);
				Date reassignmentTime = (Date)openExecution.getVariable(Jbpm4WorkflowFactory.REASSIGNMENT_TIME_VAR_NAME);
				openExecution.setVariable(REASSIGNMENT_TIME_MIRROR, reassignmentTime);
				return BEFORE_TERM;
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		} finally {
			DSApi.closeContextIfNeeded(contextOpened);
		}
	}
	

}
