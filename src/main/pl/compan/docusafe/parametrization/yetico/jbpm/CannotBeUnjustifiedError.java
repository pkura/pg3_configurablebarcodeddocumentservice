/**
 * 
 */
package pl.compan.docusafe.parametrization.yetico.jbpm;

import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.EdmException;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class CannotBeUnjustifiedError implements ExternalActivityBehaviour {

	public void execute(ActivityExecution arg0) throws Exception {
		throw new EdmException("Niezgodność została stworzona z reklamacji zewnętrznej. Brak możliwości uznania za niezasadną!");
	}

	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
	}

}
