package pl.compan.docusafe.parametrization.yetico.jbpm;

import java.util.Date;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.jbpm.api.listener.EventListenerExecution;

import org.joda.time.DateTime;
import org.joda.time.Days;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;


public class DateUpdate extends AbstractEventListener
{
    private static final Logger log = LoggerFactory.getLogger(DateUpdate.class);



    @Override
    public void notify(EventListenerExecution eventListenerExecution) throws Exception {
        boolean isOpened = true;
        Long docId = Long.valueOf(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");

        try {
            if (!DSApi.isContextOpen()) {
                isOpened = false;
                DSApi.openAdmin();
            }
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();



            Date data=  (Date)fm.getValue("DATA_REJESTRACJI");
            long now = System.currentTimeMillis();
            Date data2 = new Date(now);
            int days = Days.daysBetween(new DateTime(data).toLocalDate(), new DateTime(data2).toLocalDate()).getDays();
            putendtime(docId, days+1);






        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            if (DSApi.isContextOpen() && !isOpened) {
                DSApi.close();
            }
        }

    }
    private void putendtime(Long id, Integer days) throws EdmException {

        PreparedStatement ps = null;
        try {
            String sql = "UPDATE  dsg_yetico_rek_zew set data_zakonczenia=? where document_id=?";
            ps = DSApi.context().prepareStatement(sql);
            ps.setInt(1, days);
            ps.setLong(2, id);
            ps.executeQuery();

        }  catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(ps);
        }

    }

}
