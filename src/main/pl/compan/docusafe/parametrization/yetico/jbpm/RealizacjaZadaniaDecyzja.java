package pl.compan.docusafe.parametrization.yetico.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;

public class RealizacjaZadaniaDecyzja  implements DecisionHandler 
{

	@Override
	public String decide(OpenExecution execution)
	{
		try
		{
			OfficeDocument doc = Jbpm4Utils.getDocument(execution);
			FieldsManager fm = doc.getFieldsManager();
			Integer typ = fm.getEnumItem("TYP").getId();
			
			if (typ == 10)
				return "ZAD_REALIZOWANE_W_WERYFIKACJI";
			else if (typ == 15)
				return "PROP_DECYZJE_DO_REALIZACJI";
			else if (typ == 20)
				return "OST_DECYZJE_DO_REALIZACJI";
			else if (typ == 25)
				return "DZIALANIA_KORYGUJACE";
			else if (typ == 30)
				return "DODATKOWE_BADANIA";
			else
				return "error";
		}
		catch (Exception e)
		{
			return "error";
		}
	}
}
