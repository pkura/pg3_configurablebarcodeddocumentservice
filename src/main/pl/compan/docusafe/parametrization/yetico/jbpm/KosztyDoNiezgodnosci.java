package pl.compan.docusafe.parametrization.yetico.jbpm;

import org.apache.commons.dbutils.DbUtils;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.parametrization.yetico.ZadanieDictionary;
import pl.compan.docusafe.parametrization.yetico.hb.ReclamationCosts;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class KosztyDoNiezgodnosci implements ExternalActivityBehaviour {
    private final static Logger LOG = LoggerFactory.getLogger(KosztyDoNiezgodnosci.class);

    @Override
    public void execute(ActivityExecution activityExecution) throws Exception {

        Long docId = Long.valueOf(activityExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);
        FieldsManager fm = doc.getFieldsManager();

        List<Long> idx = idNiezgodnosci(docId);
        List<Long> koszt = idRezMulti(docId);
        for(Long ids:idx){
            for(Long koszt2:koszt){

                putNiezgodMulti( ids, koszt2);
            }
        }
















    }
        @SuppressWarnings("unchecked")

        private List<Long> idNiezgodnosci(Long id) throws EdmException {
            List<Long> output= new ArrayList();
            PreparedStatement ps = null;
            try {
                String sql = "SELECT Document_id FROM dsg_yetico_niezgodnosc_wew WHERE uwagi_link=?";
                ps = DSApi.context().prepareStatement(sql);

                ps.setLong(1, id);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    output.add(rs.getLong(1));
                }

            }  catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DbUtils.closeQuietly(ps);
            }
            return   output;
        }


    private List<Long> idRezMulti(Long id) throws EdmException {
        List<Long> output= new ArrayList();
        PreparedStatement ps = null;
        try {
            String sql = "SELECT FIELD_VAL FROM dsg_yetico_rek_zew_multiple_value WHERE FIELD_CN='KOSZTY_REKLAMACJI'AND DOCUMENT_ID=?";
            ps = DSApi.context().prepareStatement(sql);

            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                output.add(rs.getLong(1));
            }

        }  catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(ps);
        }
        return   output;
    }

    private void putNiezgodMulti(Long id, Long koszt) throws EdmException {

        PreparedStatement ps = null;
        try {
            String sql = "INSERT INTO dsg_yetico_niezgodnosc_wew_multiple_value values (?,'KOSZTY_REKLAMACJI',?)";
            ps = DSApi.context().prepareStatement(sql);
            ps.setLong(1, id);
            ps.setLong(2, koszt);
            ps.executeQuery();

        }  catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(ps);
        }

    }


        @Override
        public void signal (ActivityExecution arg0, String arg1, Map < String,?>arg2)throws Exception {
        }


}