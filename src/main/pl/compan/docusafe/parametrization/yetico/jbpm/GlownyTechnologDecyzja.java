package pl.compan.docusafe.parametrization.yetico.jbpm;

import java.util.List;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.yetico.ReklamacjaZewLogic;
import pl.compan.docusafe.parametrization.yetico.hb.Niezgodnosc;
import pl.compan.docusafe.parametrization.yetico.hb.Wyrob;

public class GlownyTechnologDecyzja  implements DecisionHandler 
{

	@Override
	public String decide(OpenExecution execution)
	{
		try
		{
			OfficeDocument doc = Jbpm4Utils.getDocument(execution);
			FieldsManager fm = doc.getFieldsManager();
			List<Long> wyrobIds = (List<Long>)fm.getKey("WYROB");
			for (Long wyrobId : wyrobIds)
			{
				Wyrob wyrob = Wyrob.findById(wyrobId);
				Integer rodzajId = wyrob.getRodzaj();
				String rodzaj = DataBaseEnumField.getEnumItemForTable("dsg_yetico_rodzaje_niezgodnosci",rodzajId).getCn();
				if (rodzaj.equalsIgnoreCase(ReklamacjaZewLogic.NIEZGODNOSC_RODZAJ_JAKOSCIOWE_CN))
					return "true";
			}
			return "false";
		}
		catch (Exception e)
		{
			return "error";
		}
	}
}
