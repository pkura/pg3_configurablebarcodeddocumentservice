package pl.compan.docusafe.parametrization.yetico.reports;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.dbutils.DbUtils;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.parametrization.ic.reports.ReportUtilsIC;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.CsvDumper;
import pl.compan.docusafe.reports.tools.PDFDumper;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.DateUtils;


public class YeticoReport extends Report
{	
	UniversalTableDumper dumper = new UniversalTableDumper();
	private Date dateFrom;
    private Date dateTo;
    private Set<String> zaklad;

    
/*	@Override
	public void initParametersAvailableValues() throws ReportException
	{
		super.initParametersAvailableValues();
		try
		{
			for(ReportParameter rp:params)
			{
				if(rp.getFieldCn().equals("project"))
					rp.setAvailableValues(ReportUtilsIC.getProjectsMap(DSApi.context().getDSUser()));
			}
		}catch (Exception e) {
			throw new ReportException(e);
		}
	}*/
	
	private void initValues() throws Exception {
        for (ReportParameter param : getParams()) {
            if (param.getType().equals("break-line")) {
                continue;
            }
            
            if(param.getFieldCn().equals("ZAKLAD_REPORT") && param.getValue() != null){
            	zaklad = new TreeSet<String>();
            	zaklad.addAll(param.valuesAsList());
            }

            if (param.getFieldCn().equals("dateFrom") && param.getValue() != null) {
                dateFrom = DateUtils.jsDateFormat.parse((String) param.getValue());
            }

            if (param.getFieldCn().equals("dateTo") && param.getValue() != null) {
                dateTo = DateUtils.jsDateFormat.parse((String) param.getValue());
            }

        }
    }
	
	/**
	 * Obecnie mo�liwe warto�ci reklamacji do integera to 10 - wytworzenie, 15 - folia, 20 - transport PL, 
	 * 25 - transport DE, 30 - recykling, 35 - inne
	 */
	private HashMap<Integer, BigDecimal> typReklamacji(Integer typReklamacjiValue){
		HashMap<Integer, BigDecimal> typReklamacji = new HashMap<Integer, BigDecimal>();
		String sql = "select mv.DOCUMENT_ID, kr.koszt_calkowity, kr.typ_kosztu "+
					 "from dsg_yetico_koszty_reklamacji kr, dsg_yetico_niezgodnosc_wew_multiple_value mv "+
					 "where mv.FIELD_VAL = kr.id  "+
					 "and mv.FIELD_CN = 'KOSZTY_REKLAMACJI' order by mv.DOCUMENT_ID;";
		Statement stmt = null;
		
		try{
			stmt = DSApi.context().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()){
				if(typReklamacjiValue.equals((Integer)rs.getInt(3))){
					typReklamacji.put((Integer)rs.getInt(1), rs.getBigDecimal(2));
				}
			}
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		} 
		finally{
			DSApi.context().closeStatement(stmt);
		}
		
		return typReklamacji;
	}
	
	
	public void doReport() throws Exception {
		
		HashMap<Integer, BigDecimal> wytworzenie = typReklamacji(10);
		HashMap<Integer, BigDecimal> folia = typReklamacji(15);
		HashMap<Integer, BigDecimal> transportPL = typReklamacji(20);
		HashMap<Integer, BigDecimal> transportDE = typReklamacji(25);
		HashMap<Integer, BigDecimal> recykling = typReklamacji(30);
		HashMap<Integer, BigDecimal> inne = typReklamacji(35);
		
		
		LoggerFactory.getLogger("Maciek1").debug("Yetico Report");
		try {
            this.initValues();
        } catch (Exception e) {
            e.printStackTrace();
        }
		
		initializeDumpers();
		
		PreparedStatement ps = null;
		
		String sql = "select g.nr_reklamacji as 'NR REKLAMACJI', g.data_wykrycia as 'Data wykrycia', mv.FIELD_VAL as ZAK�AD, " +
					"t1.TITLE as RODZAJ, t2.TITLE as TYP, "+
					"g.laczny_koszt as 'KOSZTY RAZEM' ,g.document_id "+
					"from dsg_yetico_niezgodnosc_wew as g "+
					"inner join dsg_yetico_niezgodnosc_wew_multiple_value as mv on g.document_id = mv.DOCUMENT_ID  "+
					"inner join dsg_yetico_rodzaje_niezgodnosci as t1 on g.rodzaj = t1.ID  "+
					"inner join dsg_yetico_typ_niezgodnosci as t2 on g.typ = t2.ID  "+
					"where mv.FIELD_VAL = ?  "+
					"and (data_wykrycia between ? and ?);";
		try {			
			Iterator it = zaklad.iterator();
			BigDecimal sum = new BigDecimal(0);
			BigDecimal wytworzenieSum = new BigDecimal(0);
			BigDecimal foliaSum = new BigDecimal(0);
			BigDecimal transportPLSum = new BigDecimal(0);
			BigDecimal transportDESum = new BigDecimal(0);
			BigDecimal recyklingSum = new BigDecimal(0);
			BigDecimal inneSum = new BigDecimal(0);
			
			ps = DSApi.context().prepareStatement(sql);
			ps.setInt(1, Integer.valueOf((String)it.next()));
			ps.setDate(2, java.sql.Date.valueOf(DateUtils.formatSqlDate(dateFrom)));
			ps.setDate(3, java.sql.Date.valueOf(DateUtils.formatSqlDate(dateTo)));
			
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData rsmd = rs.getMetaData();
			
			dumper.newLine();
			dumper.addText(rsmd.getColumnName(1)); //numer reklamacji
			//dumper.addText(rsmd.getColumnName(2)); //data wykrycia
			dumper.addText(rsmd.getColumnName(3)); //zaklad
			dumper.addText(rsmd.getColumnName(4)); //rodzaj
			dumper.addText(rsmd.getColumnName(5)); //typ
			dumper.addText("Reklamacja: wytworzenie");
			dumper.addText("Reklamacja: folia");
			dumper.addText("Reklamacja: transport PL");
			dumper.addText("Reklamacja: transport DE");
			dumper.addText("Reklamacja: recykling");
			dumper.addText("Reklamacja: inne");
			dumper.addText(rsmd.getColumnName(6)); //laczny koszt
			dumper.dumpLine();
			
			while(rs.next()){
				dumper.newLine();
				dumper.addText(rs.getString(1));
				//dumper.addText(rs.getString(2));
				
				if(rs.getInt(3) == 10){
					dumper.addText("GALEWICE");
				}else if(rs.getInt(3) == 15){
					dumper.addText("GORZ�W");
				}else if(rs.getInt(3) == 20){
					dumper.addText("OLSZTYN");
				}else{
					dumper.addEmptyText();
				}
				
				dumper.addText(rs.getString(4));
				dumper.addText(rs.getString(5));

				if (wytworzenie.containsKey((Integer)rs.getInt(7))) {
					dumper.addText(wytworzenie.get((Integer)rs.getInt(7)).toString() + " z�");
					wytworzenieSum = wytworzenieSum.add(wytworzenie.get((Integer)rs.getInt(7)));
				}else{
					dumper.addEmptyText();
				}
				if(folia.containsKey((Integer)rs.getInt(7))) {
					dumper.addText(folia.get((Integer)rs.getInt(7)).toString() + " z�");
					foliaSum = foliaSum.add(folia.get((Integer)rs.getInt(7)));
				}else{
					dumper.addEmptyText();
				}
				if(transportPL.containsKey((Integer)rs.getInt(7))) {
					dumper.addText(transportPL.get((Integer)rs.getInt(7)).toString() + " z�");
					transportPLSum = transportPLSum.add(transportPL.get((Integer)rs.getInt(7)));
				}else{
					dumper.addEmptyText();
				}
				if(transportDE.containsKey((Integer)rs.getInt(7))) {
					dumper.addText(transportDE.get((Integer)rs.getInt(7)).toString() + " z�");
					transportDESum = transportDESum.add(transportDE.get((Integer)rs.getInt(7)));
				}else{
					dumper.addEmptyText();
				}
				if(recykling.containsKey((Integer)rs.getInt(7))) {
					dumper.addText(recykling.get((Integer)rs.getInt(7)).toString() + " z�");
					recyklingSum = recyklingSum.add(recykling.get((Integer)rs.getInt(7)));
				}else{
					dumper.addEmptyText();
				}
				if(inne.containsKey((Integer)rs.getInt(7))) {
					dumper.addText(inne.get((Integer)rs.getInt(7)).toString() + " z�");
					inneSum = inneSum.add(inne.get((Integer)rs.getInt(7)));
				}else{
					dumper.addEmptyText();
				}
				if(rs.getInt(6) != 0){
					dumper.addText(rs.getBigDecimal(6).toString() + " z�");
					sum = sum.add(rs.getBigDecimal(6));
				}else{
					dumper.addText("0 z�");
				}
				dumper.dumpLine();
			}
			
			dumper.newLine();
			dumper.addEmptyText();
			//dumper.addEmptyText();
			dumper.addEmptyText();
			dumper.addEmptyText();
			dumper.addText("SUMA");
			dumper.addText(wytworzenieSum.toString() + " z�");
			dumper.addText(foliaSum.toString() + " z�");
			dumper.addText(transportPLSum.toString() + " z�");
			dumper.addText(transportDESum.toString() + " z�");
			dumper.addText(recyklingSum.toString() + " z�");
			dumper.addText(inneSum.toString() + " z�");
			dumper.addText(sum.toString() + " z�");
			dumper.dumpLine();
			
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		} 
		finally{
		DSApi.context().closeStatement(ps);
		dumper.closeFileQuietly();
		}
	}
	
	
	
	
	public void initializeDumpers() throws IOException{
        /** DUMPER **/
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "raport_" + DateUtils.formatJsDate(dateFrom) + "_" + DateUtils.formatJsDate(dateTo) + ".xls");
		poiDumper.openFile(xlsFile);
		dumper.addDumper(poiDumper);
		
		PDFDumper pdfDumper = new PDFDumper();
		File pdfFile = new File(this.getDestination(), "raport_" + DateUtils.formatJsDate(dateFrom) + "_" + DateUtils.formatJsDate(dateTo) + ".pdf");
		pdfDumper.openFile(pdfFile);
		dumper.addDumper(pdfDumper);
    }

}
