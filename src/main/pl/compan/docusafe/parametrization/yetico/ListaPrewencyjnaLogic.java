/**
 * 
 */
package pl.compan.docusafe.parametrization.yetico;

import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.collections.iterators.EntrySetMapIterator;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import edu.emory.mathcs.backport.java.util.Collections;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.yetico.hb.PrewencjaItem;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class ListaPrewencyjnaLogic extends AbstractDocumentLogic {
	
	private static final Logger logger = LoggerFactory.getLogger(ListaPrewencyjnaLogic.class);
	
	/**
	 * Nazwa kodowa(Cn) dokumentu: lista_prewencyjna
	 */
	public static final String LISTA_PREWENCYJNA = "lista_prewencyjna";
	
	/**
	 * Nazwa kodowa(Cn) s�ownika znajduj�cego si� w dockind: lista_prewencyjna
	 */
	public static final String PREWENCJA = "PREWENCJA";
	
	/**
	 * Nazwa kodowa(Cn) pola znajduj�cego si� w dockind: lista_prewencyjna
	 */
	public static final String NR_LISTY = "NR_LISTY";
	
	/**
	 * Nazwa kodowa(Cn) pola znajduj�cego si� w dockind: lista_prewencyjna
	 */
	public static final String OPIS = "OPIS";
	
	/**
	 * Nazwa kodowa(Cn) pola slownika znajdujacego sie w dockind: lista_prewencyjna
	 */
	public static final String PLAN_PREWENCYJNY_DICT = "PLAN_PREWENCYJNY_DICT";
	
	/**
	 * Nazwa kodowa(Cn) pola znajduj�cego si� w dockind: lista_prewencyjna
	 */
	public static final String STATUS = "STATUS";
	public static final String ZAKLAD_FIELD_CN = "ZAKLAD";
	public static final String ID = "ID";
	public static final String PREWENCJA_PLANOWANA_DATA_REALIZACJI = "PREWENCJA_PLANOWANA_DATA_REALIZACJI";
	public static final String PREWENCJA_ID = "PREWENCJA_ID";
	public static final String PREWENCJA_STATUS = "PREWENCJA_STATUS";
	public static final String PREWENCJA_IMIE_NAZWISKO = "PREWENCJA_IMIE_NAZWISKO";
	public static final String DWR_ZAKLAD = "DWR_ZAKLAD";
	public static final String DWR_PLAN_PREWENCYJNY_DICT = "DWR_PLAN_PREWENCYJNY_DICT";
	public static final String PLAN_PREWENCYJNY_DICT_LINK = "PLAN_PREWENCYJNY_DICT_LINK";
	public static final String PLAN_PREWENCYJNY_DICT_ROK = "PLAN_PREWENCYJNY_DICT_ROK";
			
	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
		FieldsManager fm = document.getFieldsManager();
		Map<String, Object> valuesToSet = new HashMap<String, Object>();
		valuesToSet.put(STATUS, 15);
		Integer actualYear = Calendar.getInstance().get(Calendar.YEAR);
		StringBuilder listNumber = new StringBuilder();
		listNumber = listNumber.append(fm.getDocumentId()).append("/").append(actualYear);
		valuesToSet.put(NR_LISTY, listNumber.toString());
		
		List<Long> idList = (List<Long>)fm.getKey(PREWENCJA);
		Long[] idTable = idList.toArray(new Long[idList.size()]);
		List<PrewencjaItem> prewencjaItemList = Lists.newArrayList();
		prewencjaItemList = DSApi.context().session().createCriteria(PrewencjaItem.class)
																.add(Restrictions.in("id", idTable))
																.list();
		for (PrewencjaItem item : prewencjaItemList) {
			item.setNrListy(listNumber.toString());
			DSApi.context().session().update(item);
		}
		DSApi.context().session().flush();
		fm.getDocumentKind().setOnly(fm.getDocumentId(), valuesToSet);
		
		Long planId = findPreventionPlan(fm);
		
		if (planId == null) {
			createPlanPrewencji(fm,event);
		} else {
			addListToPlan(fm, planId);
		}
	}
	
	private Long findPreventionPlan(FieldsManager fm) throws EdmException {
		Integer worksIdFromList = (Integer) fm.getKey(ZAKLAD_FIELD_CN);
		Integer actualYear = Calendar.getInstance().get(Calendar.YEAR);
		List<Long> docIdList = Document.findByDocumentKind(PlanPrewencyjnyLogic.PLAN_PREWENCYJNY);
		for (Long id : docIdList) {
			if (id != null && id != 0L) {
				FieldsManager fmForPlan = Document.find(id).getFieldsManager();
				Integer yearFromPlan = (Integer)fmForPlan.getKey(PlanPrewencyjnyLogic.ROK);
				Integer worksIdFromPlan = (Integer) fmForPlan.getKey(ZAKLAD_FIELD_CN);
				if (yearFromPlan != null && worksIdFromPlan != null) {
					if (yearFromPlan.equals(actualYear) && worksIdFromPlan.equals(worksIdFromList)) {
						return fmForPlan.getDocumentId();
					} 
				}
			}
		}
		return null;
	}
	
	private void createPlanPrewencji(FieldsManager fm, ActionEvent event) throws EdmException {
		OutOfficeDocument newDocument = new OutOfficeDocument();
		try {	
			DSUser author = DSApi.context().getDSUser();
			String authorName = author.getName();
			newDocument.setCreatingUser(authorName);
			newDocument.setSummary("Plan prewencji stworzony z listy prewencyjnej");
			DocumentKind planPrewencjiDocKind = DocumentKind.findByCn(PlanPrewencyjnyLogic.PLAN_PREWENCYJNY);
			newDocument.setDocumentKind(planPrewencjiDocKind);
			newDocument.create();
			newDocument.setInternal(true);
			
			Set<PermissionBean> perms = new HashSet<PermissionBean>();
			String authorFullName = author.getLastname() + " " + author.getFirstname();
			perms.add(new PermissionBean(ObjectPermission.READ, authorName, ObjectPermission.USER, authorFullName + " (" + authorName + ")"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, authorName, ObjectPermission.USER, authorFullName + " (" + authorName + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, authorName, ObjectPermission.USER, authorFullName + " (" + authorName + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, authorName, ObjectPermission.USER, authorFullName + " (" + authorName + ")"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, authorName, ObjectPermission.USER, authorFullName + " (" + authorName + ")"));
			Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(newDocument);
			perms.addAll(documentPermissions);
			((AbstractDocumentLogic) newDocument.getDocumentKind().logic()).setUpPermission(newDocument, perms);
			
			Map<String, Object> valuesToSet = new HashMap<String, Object>();
			Integer actualYear = Calendar.getInstance().get(Calendar.YEAR);
			Integer worksId = (Integer) fm.getKey(ZAKLAD_FIELD_CN);
			valuesToSet.put(PlanPrewencyjnyLogic.ROK, actualYear);
			valuesToSet.put(ZAKLAD_FIELD_CN, worksId);
			List<Long> idList = new ArrayList<Long>();
			idList.add(fm.getDocumentId());
			valuesToSet.put(PlanPrewencyjnyLogic.LISTA_PREWENCYJNA_DICT, idList);
			planPrewencjiDocKind.setOnly(newDocument.getId(), valuesToSet);
			
			DSApi.context().session().save(newDocument);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new EdmException("Nie uda�o si� utworzy� dokumentu: Plan prewencyjny");
		}
		newDocument.getDocumentKind().logic().onStartProcess(newDocument, event);
		TaskSnapshot.updateByDocument(newDocument);
		
		Map<String, Object> valuesToSet = new HashMap<String, Object>();
		List<Long> idList = new ArrayList<Long>();
		idList.add(newDocument.getId());
		valuesToSet.put(ListaPrewencyjnaLogic.PLAN_PREWENCYJNY_DICT, idList);
		fm.getDocumentKind().setOnly(fm.getDocumentId(), valuesToSet);
	}
	
	private void addListToPlan(FieldsManager fm, Long planId) throws EdmException {
		try {
			FieldsManager fmFromPlan = Document.find(planId).getFieldsManager();
			LinkedList<Long> idList = (LinkedList<Long>)fmFromPlan.getKey(PlanPrewencyjnyLogic.LISTA_PREWENCYJNA_DICT);
			if (idList != null) {
				idList.add(fm.getDocumentId());
				Map<String, Object> values = new HashMap<String, Object>();
				values.put(PlanPrewencyjnyLogic.LISTA_PREWENCYJNA_DICT, idList);
				fmFromPlan.getDocumentKind().setOnly(planId, values);
			}
			fm.getDocumentKind().setOnly(fm.getDocumentId(), Collections.singletonMap(PLAN_PREWENCYJNY_DICT, planId));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new EdmException("Nie uda�o si� doda� listy prewencyjnej do planu prewencyjnego");
		}
	}
	
	public void documentPermissions(Document document) throws EdmException {

	}

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {

	}

}
