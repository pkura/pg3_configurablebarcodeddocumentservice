/**
 * 
 */
package pl.compan.docusafe.parametrization.yetico;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.AssignUtils;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.AssigneeLogic;
import pl.compan.docusafe.core.jbpm4.AssigningHandledException;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class NaprawaLogic extends AbstractDocumentLogic {

	private final static Logger logger = LoggerFactory.getLogger(NaprawaLogic.class);
	
	public static final String ZAKLAD_FIELD_CN = "ZAKLAD";
	
    public static final Map<String, AssigneeLogic> ASSIGNEE_LOGIC_MAP = ImmutableMap.<String, AssigneeLogic>builder()
            .put("leader", new LeaderAssignee())
            .build();
			
    public static final Map<Integer, String> ACCEPTANCE_CN_FOR_WORKS = ImmutableMap.<Integer, String>builder()
            .put(10, "leader_GAL")
            .put(15, "leader_GW")
            .put(20, "leader_OL")
            .build();
    
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

			java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

			DSUser author = DSApi.context().getDSUser();
			String user = author.getName();
			String fullName = author.getLastname() + " " + author.getFirstname();

			perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

			Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
			perms.addAll(documentPermissions);
			this.setUpPermission(document, perms);
			
			if (AvailabilityManager.isAvailable("addToWatch")) {
                DSApi.context().watch(URN.create(document));
            }
		}
		catch (Exception e)
		{
			logger.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}
	
	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
		String documentKindName = document.getDocumentKind().getName();

		perms.add(new PermissionBean(ObjectPermission.READ, "YETICO_" + documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, "YETICO_ - " + documentKindName + " - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "YETICO_" + documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "PCz - " + documentKindName + " zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "YETICO_" + documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "YETICO_ - " + documentKindName + " - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "YETICO_" + documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "YETICO_ - " + documentKindName + " zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "YETICO_" + documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "YETICO_ - " + documentKindName + " - usuwanie"));
		
		DSUser user = DSApi.context().getDSUser();
		String fullName = user.getLastname()+ " " +user.getFirstname();
		
		perms.add(new PermissionBean(ObjectPermission.READ, user.getName(), ObjectPermission.USER, fullName+" ("+user.getName()+")"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user.getName(), ObjectPermission.USER, fullName+" ("+user.getName()+")"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, user.getName(), ObjectPermission.USER, fullName+" ("+user.getName()+")"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user.getName(), ObjectPermission.USER, fullName+" ("+user.getName()+")"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, user.getName(), ObjectPermission.USER, fullName+" ("+user.getName()+")"));
		
		Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
		perms.addAll(documentPermissions);
		this.setUpPermission(document, perms);
	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) throws EdmException {
		AssigneeLogic assigneer = ASSIGNEE_LOGIC_MAP.get(acceptationCn);
		if (assigneer != null) {
            FieldsManager fm = doc.getFieldsManager();
            try {
                assigneer.assign(doc, fm, assignable, openExecution);
            } catch (AssigningHandledException e) {
                AssignUtils.addRemarkAndAssignToAdmin(doc, assignable, e.getMessage());
            } catch (Exception e) {
                AssignUtils.addRemarkAndAssignToAdmin(doc, assignable, "Nie znaleziono osoby do przypisania, przypisano do admin'a");
                logger.error(e.getMessage(), e);
            } finally {
                return true;
            }
        }
		return false;
	}
	
    static class LeaderAssignee implements AssigneeLogic {
    	
        public void assign(OfficeDocument doc, FieldsManager fm, Assignable assignable, OpenExecution openExecution) throws EdmException {
        	List<String> users = new ArrayList<String>();
        	List<String> divisions = new ArrayList<String>();
        	
        	Integer worksId = (Integer) fm.getKey(ZAKLAD_FIELD_CN);
            String acceptationCn = ACCEPTANCE_CN_FOR_WORKS.get(worksId);
            for (AcceptanceCondition accept : AcceptanceCondition.find(acceptationCn))
			{
				if (accept.getUsername() != null && !users.contains(accept.getUsername()))
					users.add(accept.getUsername());
				if (accept.getDivisionGuid() != null && !divisions.contains(accept.getDivisionGuid()))
					divisions.add(accept.getDivisionGuid());
			}
			for (String user : users)
			{
				assignable.addCandidateUser(user);
				AssigneeHandler.addToHistory(openExecution, doc, user, null);
			}
			for (String division : divisions)
			{
				assignable.addCandidateGroup(division);
				AssigneeHandler.addToHistory(openExecution, doc, null, division);
			}
        }
        
    }
    
	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {

	}

}
