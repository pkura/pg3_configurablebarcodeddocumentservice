package pl.compan.docusafe.parametrization.yetico;

import java.util.Map;
import java.util.Set;

import com.google.common.collect.Maps;
import org.jbpm.api.task.Participation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.users.DSUser;

public class ZadanieLogic  extends AbstractDocumentLogic
{
    private static final Logger log = LoggerFactory.getLogger(ZadanieLogic.class);
	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		 Map<String, Object> toReload = Maps.newHashMap();
	        for(Field f : fm.getFields())
	        {
	            if(f.getDefaultValue() != null)
	            {
	                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
	            }
	        }
	        
	        Long userId = DSApi.context().getDSUser().getId();
	        toReload.put("PRACOWNIK_ZLECAJACY", userId);
	        fm.reloadValues(toReload);
	}
	
	@Override
	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
		String documentKindName = document.getDocumentKind().getName();

		perms.add(new PermissionBean(ObjectPermission.READ, "YETICO_" + documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, "YETICO_ - " + documentKindName + " - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "YETICO_" + documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "PCz - " + documentKindName + " zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "YETICO_" + documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "YETICO_ - " + documentKindName + " - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "YETICO_" + documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "YETICO_ - " + documentKindName + " zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "YETICO_" + documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "YETICO_ - " + documentKindName + " - usuwanie"));
		
		DSUser user = DSApi.context().getDSUser();
		String fullName = user.getLastname()+ " " +user.getFirstname();
		
		perms.add(new PermissionBean(ObjectPermission.READ, user.getName(), ObjectPermission.USER, fullName+" ("+user.getName()+")"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user.getName(), ObjectPermission.USER, fullName+" ("+user.getName()+")"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, user.getName(), ObjectPermission.USER, fullName+" ("+user.getName()+")"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user.getName(), ObjectPermission.USER, fullName+" ("+user.getName()+")"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, user.getName(), ObjectPermission.USER, fullName+" ("+user.getName()+")"));
		
		Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
		perms.addAll(documentPermissions);
		this.setUpPermission(document, perms);
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		// TODO Auto-generated method stub
		
	}
	
	public void onStartProcess(pl.compan.docusafe.core.office.OfficeDocument document) throws EdmException {
		Map<String, Object> map = Maps.newHashMap();
		document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
	};

    @Override
    public String getMailForJbpmMailListener(Document document, String mailForLogicCn) {
        DSUser author = null;
        try {
            author = DSUser.findByUsername(document.getAuthor());
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
        if(mailForLogicCn.equals("author")){
                return author.getEmail();
        }
        else if(mailForLogicCn.equals("authorSupervisor")){
            try {
                if(author.getSupervisor()!=null){
                    return author.getSupervisor().getEmail();
                }
                else{
                    return author.getEmail();
                }
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
            }
        }
        return null;
    }
}
