package pl.compan.docusafe.parametrization.yetico.hb;

import java.util.Date;

public class PrewencjaItem {
	
	Date planowanaDataRealizacji, planowanaDataZakonczenia, dataRealizacji;
	Integer akcjaPrewencyjna, czasRealizacji, status;
	Long id;
	String nrListy, opis;
	Integer imieNazwisko;

	public PrewencjaItem()
	{
		
	}
	public PrewencjaItem(Date planowanaDataRealizacji,
			Date planowanaDataZakonczenia, Date dataRealizacji,
			Integer akcjaPrewencyjna, Integer czasRealizacji, Integer status,
			Long id, String nrListy, String opis, Integer imieNazwisko) {
		super();
		this.planowanaDataRealizacji = planowanaDataRealizacji;
		this.planowanaDataZakonczenia = planowanaDataZakonczenia;
		this.dataRealizacji = dataRealizacji;
		this.akcjaPrewencyjna = akcjaPrewencyjna;
		this.czasRealizacji = czasRealizacji;
		this.status = status;
		this.id = id;
		this.nrListy = nrListy;
		this.opis = opis;
		this.imieNazwisko = imieNazwisko;
	}
	public Date getPlanowanaDataRealizacji() {
		return planowanaDataRealizacji;
	}
	public void setPlanowanaDataRealizacji(Date planowanaDataRealizacji) {
		this.planowanaDataRealizacji = planowanaDataRealizacji;
	}
	public Date getDataRealizacji() {
		return dataRealizacji;
	}
	public void setDataRealizacji(Date dataRealizacji) {
		this.dataRealizacji = dataRealizacji;
	}
	public Integer getAkcjaPrewencyjna() {
		return akcjaPrewencyjna;
	}
	public void setAkcjaPrewencyjna(Integer akcjaPrewencyjna) {
		this.akcjaPrewencyjna = akcjaPrewencyjna;
	}
	public Integer getCzasRealizacji() {
		return czasRealizacji;
	}
	public void setCzasRealizacji(Integer czasRealizacji) {
		this.czasRealizacji = czasRealizacji;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNrListy() {
		return nrListy;
	}
	public void setNrListy(String nrListy) {
		this.nrListy = nrListy;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public Integer getImieNazwisko() {
		return imieNazwisko;
	}
	public void setImieNazwisko(Integer imieNazwisko) {
		this.imieNazwisko = imieNazwisko;
	}

	public Date getPlanowanaDataZakonczenia() {
		return planowanaDataZakonczenia;
	}

	public void setPlanowanaDataZakonczenia(Date planowanaDataZakonczenia) {
		this.planowanaDataZakonczenia = planowanaDataZakonczenia;
	}
	
	
	
	
	
	
	
}
