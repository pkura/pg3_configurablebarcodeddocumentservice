/**
 * 
 */
package pl.compan.docusafe.parametrization.yetico.hb;

import pl.compan.docusafe.web.office.ReclamationListAction;

import com.google.common.collect.ImmutableMap;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class ReclamationCosts implements java.io.Serializable {
	private Long id;
	private Long works;
	private Long costType;
	private String description;
	private Double unitCost;
	private Double number;
	private Double totalCost;
	
	public ReclamationCosts() {
		
	}
	
	public String getWorksString() {
		return NiezgodnoscWew.WORKS_MAP.get(works.intValue());
	}

	public String getCostTypeString() {
		return ReclamationListAction.typyKosztu.get(costType.intValue());
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getWorks() {
		return works;
	}

	public void setWorks(Long works) {
		this.works = works;
	}

	public Long getCostType() {
		return costType;
	}

	public void setCostType(Long costType) {
		this.costType = costType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getUnitCost() {
		return unitCost;
	}

	public void setUnitCost(Double unitCost) {
		this.unitCost = unitCost;
	}

	public Double getNumber() {
		return number;
	}

	public void setNumber(Double number) {
		this.number = number;
	}

	public Double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}
	
}
