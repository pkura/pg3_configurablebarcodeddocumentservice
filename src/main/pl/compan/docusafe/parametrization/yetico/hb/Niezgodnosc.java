package pl.compan.docusafe.parametrization.yetico.hb;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.parametrization.yetico.TypNiezgodnosci;

public class Niezgodnosc
{
	private Long id;
	private Integer rodzaj;
	private Integer typ;
	private String opis;
	
	public Niezgodnosc()
	{
	}
	
	public Niezgodnosc(Long id, Integer rodzaj, Integer typ, String opis)
	{
		this.id = id;
		this.rodzaj = rodzaj;
		this.typ = typ;
		this.opis = opis;
	}
	
	public Long getId()
	{
		return id;
	}
	public void setId(Long id)
	{
		this.id = id;
	}
	public Integer getRodzaj()
	{
		return rodzaj;
	}
	public void setRodzaj(Integer rodzaj)
	{
		this.rodzaj = rodzaj;
	}
	public Integer getTyp()
	{
		return typ;
	}
	public void setTyp(Integer typ)
	{
		this.typ = typ;
	}
	public String getOpis()
	{
		return opis;
	}
	public void setOpis(String opis)
	{
		this.opis = opis;
	}
	
	public static Niezgodnosc find(Long id) throws EdmException
	{
		Niezgodnosc niezgodnosc = (Niezgodnosc) DSApi.getObject(Niezgodnosc.class, id);

		if (niezgodnosc == null)
			throw new EntityNotFoundException(Niezgodnosc.class, id);

		return niezgodnosc;
	}
	
	public static List<Niezgodnosc> list() throws EdmException
	{
		return DSApi.context().session().createCriteria(Niezgodnosc.class).list();
	}
	
	public static Map<Integer, String> rodzajNiezgodnosciList() throws EdmException
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		Map<Integer, String> rodzajeNiezgodnosci = new HashMap<Integer, String>();
		boolean isOpened = true; 
		try
		{
			if (!DSApi.isContextOpen())
			{   
				isOpened = false; 
				DSApi.openAdmin();    
			}
			ps = DSApi.context().prepareStatement("select id, title from dsg_yetico_rodzaje_niezgodnosci");
			rs = ps.executeQuery();
			while (rs.next())
				rodzajeNiezgodnosci.put(rs.getInt(1), rs.getString(2));
		}
		catch (Exception e)
		{
			throw new EdmException(e);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
			if (DSApi.isContextOpen() && !isOpened) 
			{ 
				DSApi.close();    
			}
		}
		return rodzajeNiezgodnosci;
		/*
		DataBaseEnumField a = DataBaseEnumField.getEnumFiledForTable("dsg_yetico_rodzaje_niezgodnosci");
		Map<Integer, String> rodzajeNiezgodnosci = new HashMap<Integer, String>();
		for (EnumItem item : a.getEnumItems())
		{
			rodzajeNiezgodnosci.put(a.getId(), a.getName());
		}
		return rodzajeNiezgodnosci;*/
		
	}
	
	public static List<TypNiezgodnosci> typNiezgodnosciList() throws EdmException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<TypNiezgodnosci> typeList = new ArrayList<TypNiezgodnosci>();
		boolean isOpened = true; 
		try {
			if (!DSApi.isContextOpen()) {   
				isOpened = false; 
				DSApi.openAdmin();    
			}
			ps = DSApi.context().prepareStatement("select id, title, refValue from dsg_yetico_typ_niezgodnosci");
			rs = ps.executeQuery();
			while (rs.next()) {
				TypNiezgodnosci type = new TypNiezgodnosci(rs.getLong(1), rs.getString(2), rs.getString(3));
				typeList.add(type);
			}
			return typeList;
				
		} catch (Exception e) {
			throw new EdmException(e);
		}
		finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
			if (DSApi.isContextOpen() && !isOpened) { 
				DSApi.close();    
			}
		}
	}
	
	public static Map<Integer, String> typNiezgodnosciMap() throws EdmException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Map<Integer, String> typeMap = new HashMap<Integer, String>();
		boolean isOpened = true; 
		try {
			if (!DSApi.isContextOpen()) {   
				isOpened = false; 
				DSApi.openAdmin();    
			}
			ps = DSApi.context().prepareStatement("select id, title from dsg_yetico_typ_niezgodnosci");
			rs = ps.executeQuery();
			while (rs.next()) {
				typeMap.put(rs.getInt(1), rs.getString(2));
			}
			return typeMap;
				
		} catch (Exception e) {
			throw new EdmException(e);
		}
		finally {
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
			if (DSApi.isContextOpen() && !isOpened) { 
				DSApi.close();    
			}
		}
	}
	
}
