package pl.compan.docusafe.parametrization.yetico.hb;

import java.math.BigDecimal;

public class WymaganaMasaGestoscWyrobu {
	
	private Long 	id;
	private BigDecimal wymaganaMasa;
	private BigDecimal masaZmierzona;
	private BigDecimal odchylkaMasy;
	private BigDecimal wymaganaGestosc;
	private BigDecimal gestoscZmierzona;
	private BigDecimal odchylkaGestosci;
	private String 	opis;
	
	public WymaganaMasaGestoscWyrobu() {}
	
	public WymaganaMasaGestoscWyrobu(Long id, BigDecimal wymaganaMasa, BigDecimal masaZmierzona, BigDecimal odchylkaMasy, BigDecimal wymaganaGestosc,
									 BigDecimal gestoscZmierzona, BigDecimal odchylkaGestosci, String opis) {
		this.id = id;
		this.wymaganaMasa = wymaganaMasa;
		this.masaZmierzona = masaZmierzona;
		this.odchylkaMasy = odchylkaMasy;
		this.wymaganaGestosc = wymaganaGestosc;
		this.gestoscZmierzona = gestoscZmierzona;
		this.odchylkaGestosci = odchylkaGestosci;
		this.opis = opis;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getWymaganaMasa() {
		return wymaganaMasa;
	}

	public void setWymaganaMasa(BigDecimal wymaganaMasa) {
		this.wymaganaMasa = wymaganaMasa;
	}

	public BigDecimal getMasaZmierzona() {
		return masaZmierzona;
	}

	public void setMasaZmierzona(BigDecimal masaZmierzona) {
		this.masaZmierzona = masaZmierzona;
	}

	public BigDecimal getOdchylkaMasy() {
		return odchylkaMasy;
	}

	public void setOdchylkaMasy(BigDecimal odchylkaMasy) {
		this.odchylkaMasy = odchylkaMasy;
	}

	public BigDecimal getWymaganaGestosc() {
		return wymaganaGestosc;
	}

	public void setWymaganaGestosc(BigDecimal wymaganaGestosc) {
		this.wymaganaGestosc = wymaganaGestosc;
	}

	public BigDecimal getGestoscZmierzona() {
		return gestoscZmierzona;
	}

	public void setGestoscZmierzona(BigDecimal gestoscZmierzona) {
		this.gestoscZmierzona = gestoscZmierzona;
	}

	public BigDecimal getOdchylkaGestosci() {
		return odchylkaGestosci;
	}

	public void setOdchylkaGestosci(BigDecimal odchylkaGestosci) {
		this.odchylkaGestosci = odchylkaGestosci;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}
}
