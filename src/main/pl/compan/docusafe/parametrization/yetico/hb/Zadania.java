package pl.compan.docusafe.parametrization.yetico.hb;

import java.util.Date;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.jfree.util.Log;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;

public class Zadania {

	private Long id;
	private Integer status;
	private Integer typ;
	private Integer pracownikOdpowiedzialny;
	private Integer pracownikZlecajacy;
	private Integer decyzja;
	private String opis;
	private String opisRealizacji;
	private Date odData;
	private Date doData;
	private Integer reklamacja;
	private Integer niezgodnosc;
	private Set<NiezgodnoscWew> niezgodnosciWEW;
	
	public Zadania(){
		
	}
	
	public Zadania(Long id, Integer status, Integer pracownikOdpowiedzialny,
			Integer pracownikZlecajacy, Integer decyzja, String opis,
			String opisRealizacji, Date odData, Date doData,
			Integer reklamacja, Integer niezgodnosc) {
		this.id = id;
		this.status = status;
		this.pracownikOdpowiedzialny = pracownikOdpowiedzialny;
		this.pracownikZlecajacy = pracownikZlecajacy;
		this.decyzja = decyzja;
		this.opis = opis;
		this.opisRealizacji = opisRealizacji;
		this.odData = odData;
		this.doData = doData;
		this.reklamacja = reklamacja;
		this.niezgodnosc = niezgodnosc;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getTyp() {
		return typ;
	}

	public void setTyp(Integer typ) {
		this.typ = typ;
	}

	public Integer getPracownikOdpowiedzialny() {
		return pracownikOdpowiedzialny;
	}
	public void setPracownikOdpowiedzialny(Integer pracownikOdpowiedzialny) {
		this.pracownikOdpowiedzialny = pracownikOdpowiedzialny;
	}
	public Integer getPracownikZlecajacy() {
		return pracownikZlecajacy;
	}
	public void setPracownikZlecajacy(Integer pracownikZlecajacy) {
		this.pracownikZlecajacy = pracownikZlecajacy;
	}
	public Integer getDecyzja() {
		return decyzja;
	}
	public void setDecyzja(Integer decyzja) {
		this.decyzja = decyzja;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public String getOpisRealizacji() {
		return opisRealizacji;
	}
	public void setOpisRealizacji(String opisRealizacji) {
		this.opisRealizacji = opisRealizacji;
	}
	public Date getOdData() {
		return odData;
	}
	public void setOdData(Date odData) {
		this.odData = odData;
	}
	public Date getDoData() {
		return doData;
	}
	public void setDoData(Date doData) {
		this.doData = doData;
	}
	public Integer getReklamacja() {
		return reklamacja;
	}
	public void setReklamacja(Integer reklamacja) {
		this.reklamacja = reklamacja;
	}
	public Integer getNiezgodnosc() {
		return niezgodnosc;
	}
	public void setNiezgodnosc(Integer niezgodnosc) {
		this.niezgodnosc = niezgodnosc;
	}
	
	public Set<NiezgodnoscWew> getNiezgodnosciWEW() {
		return niezgodnosciWEW;
	}

	public void setNiezgodnosciWEW(Set<NiezgodnoscWew> niezgodnosciWEW) {
		this.niezgodnosciWEW = niezgodnosciWEW;
	}

	public static Zadania findById(Long id) throws EdmException
	{
		Log.info("Id zadania = " + id);
		Criteria crit = DSApi.context().session().createCriteria(Zadania.class);
		crit.add(Restrictions.eq("id", id));
		crit.setMaxResults(1);
		Zadania zadania = (Zadania) crit.list().get(0);

		if (zadania == null)
			throw new EntityNotFoundException(Zadania.class, id);

		return zadania;
	}
}
