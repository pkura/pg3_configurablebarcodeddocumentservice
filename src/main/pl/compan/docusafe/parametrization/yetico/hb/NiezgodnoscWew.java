package pl.compan.docusafe.parametrization.yetico.hb;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.yetico.ZadanieDictionary;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Joiner;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableMap;
import com.lowagie.text.Document;

public class NiezgodnoscWew {
	private static Logger log = LoggerFactory.getLogger(NiezgodnoscWew.class);

	/*public static final String NIEZGODNOSC_WEW_CN = Docusafe.getAdditionProperty("reklamacja.wewnetrzna.CN") != null ? Docusafe
			.getAdditionProperty("reklamacja.wewnetrzna.CN") : "niezgodnosc";*/
	public static final String NIEZGODNOSC_WEW_CN = "niezgodnosc";
	public static final String STATUS_NIEZGODNOSCI = "STATUS_NIEZGODNOSCI";
	public static final String STATUS = "STATUS";
	public static final String DOT_ZAKLADU = "DOT_ZAKLADU";
	public static final String COSTS_OF_COMPLAINT_DICTIONARY_CN = "KOSZTY_REKLAMACJI";
			
	private Long documentId;
	private Integer statusNiezgodnosci;
	private Integer statusRealizacji;
	private String nrReklamacji;
	private Integer rodzaj;
	private Integer typ;
	private Date dataRejestracji;
	private DSUser autor;
	private Date dataWykrycia;
	private Integer dotyczyZakladu;
	private String miejsceWykrycia;
	private String osobaPoinformowana;
	private String nrZdjecia;
	private String informacje;
	private Set<Zadania> zadania;
	private Set<NiezgodnoscWewMultiple> multipleValues = new HashSet<NiezgodnoscWewMultiple>();
	private Set<ReclamationCosts> reclamationCosts = new HashSet<ReclamationCosts>();
	private BigDecimal lacznyKoszt = new BigDecimal(0);
	
	public static final ImmutableMap<Integer, String> WORKS_MAP = new ImmutableMap.Builder<Integer, String>()
			.put(10, "GAL")
			.put(15, "GW")
			.put(20, "OL")
			.build();
	
	public NiezgodnoscWew() {
	}

	public static NiezgodnoscWew find(Long id) throws EdmException {
		NiezgodnoscWew niezgodnoscWew = (NiezgodnoscWew) DSApi.getObject(NiezgodnoscWew.class, id);

		if (niezgodnoscWew == null)
			throw new EntityNotFoundException(NiezgodnoscWew.class, id);

		return niezgodnoscWew;
	}

	@SuppressWarnings("unchecked")
	public static List<NiezgodnoscWew> list() throws EdmException {
		return DSApi.context().session().createCriteria(NiezgodnoscWew.class).list();
	}

	public static SearchResults<NiezgodnoscWew> otwarteList(int offset, int limit, String sortField, boolean ascending) throws EdmException, ParseException {
		return znajdzNiezgodnosci(10, null, null, null, null, null, null, null, null, null, null, null, offset, limit, sortField, ascending, false, null, null, null, null, null);
	}
	
	public static SearchResults<NiezgodnoscWew> allList(int offset, int limit, String sortField, boolean ascending) throws EdmException, ParseException {
		return znajdzNiezgodnosci(null, null, null, null, null, null, null, null, null, null, null, null, offset, limit, sortField, ascending, false, null, null, null, null, null);
	}
	
	@SuppressWarnings("unchecked")
	public static SearchResults<NiezgodnoscWew> znajdzNiezgodnosci(Integer statusNiezgodnosci, Integer statusRealizacji,
			String nrNiezgodnosci, Integer rodzaj, Integer typ, Long autor, String dataWykryciaOd, String dataWykryciaDo, Integer dotyczyZakladu,
			String miejsceWykrycia, String osobaPoinformowana, String nrZdjecia, int offset, int limit, String sortField, boolean ascending, boolean omitLimit,
			Integer zaklad, Integer typKosztu, Double kosztJednostkowy, Double ilosc, Double kosztCalkowity)
			throws EdmException, ParseException {

		try {
			Criteria crit = DSApi.context().session().createCriteria(NiezgodnoscWew.class);
			boolean wasReclamationCostsDictionary = false;
			
			if (statusNiezgodnosci != null) {
				crit.add(Restrictions.eq("statusNiezgodnosci", statusNiezgodnosci));
			}
			if (statusRealizacji != null) {
				crit.add(Restrictions.eq("statusRealizacji", statusRealizacji));
			}
			if (nrNiezgodnosci != null) {
				crit.add(Restrictions.eq("nrReklamacji", nrNiezgodnosci));
			}
			if (rodzaj != null) {
				crit.add(Restrictions.eq("rodzaj", rodzaj));
			}
			if (typ != null) {
				crit.add(Restrictions.eq("typ", typ));
			}
			if (autor != null) {
				crit.add(Restrictions.eq("autor", DSUser.findById(autor)));
			}
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");

			if (dataWykryciaOd != null) {
				Date dataOd = df.parse(dataWykryciaOd);
				crit.add(Restrictions.ge("dataWykrycia", dataOd));
			}
			if (dataWykryciaDo != null) {
				Date dataDo = df.parse(dataWykryciaDo);
				crit.add(Restrictions.le("dataWykrycia", dataDo));
			}
			if (miejsceWykrycia != null) {
				crit.add(Restrictions.like("miejsceWykrycia", miejsceWykrycia, MatchMode.ANYWHERE));
			}
			if (osobaPoinformowana != null) {
				crit.add(Restrictions.like("osobaPoinformowana", osobaPoinformowana, MatchMode.ANYWHERE));
			}
			if (nrZdjecia != null) {
				crit.add(Restrictions.eq("nrZdjecia", nrZdjecia));
			}
			if (zaklad != null || typKosztu != null || kosztJednostkowy != null || ilosc != null || kosztCalkowity != null) {
				wasReclamationCostsDictionary = true;
				Criteria tempCrit = DSApi.context().session().createCriteria(ReclamationCosts.class);
				if (zaklad != null) {
					tempCrit.add(Restrictions.eq("works", zaklad.longValue()));
				}
				if (typKosztu != null) {
					tempCrit.add(Restrictions.eq("costType", typKosztu.longValue()));
				}
				if (kosztJednostkowy != null) {
					tempCrit.add(Restrictions.eq("unitCost", kosztJednostkowy));
				}
				if (ilosc != null) {
					tempCrit.add(Restrictions.eq("number", ilosc));
				}
				if (kosztCalkowity != null) {
					tempCrit.add(Restrictions.eq("totalCost", kosztCalkowity));
				}
				List<ReclamationCosts> reclamationCosts = tempCrit.list();
				Function<ReclamationCosts, String> reclamationCostsIdFunction = new Function<ReclamationCosts, String>() {
					public String apply(ReclamationCosts cost) {
						return cost.getId().toString();
					}
				};
				Collection<String> reclamationCostsIds = Collections2.transform(reclamationCosts, reclamationCostsIdFunction);
				
				// w przypadku gdy nie znaleziono kosztow reklamacji (gdy reclamationCostsIds jest pusty)
				// jest zwracana pusta lista (pomijane sa kolejne kroki tworzenia criteria)
				if (reclamationCostsIds.isEmpty()) {
					return new SearchResultsAdapter<NiezgodnoscWew>(new ArrayList<NiezgodnoscWew>(), 0, 0, NiezgodnoscWew.class);
				}
				
				if (dotyczyZakladu == null) {
					crit.createCriteria("multipleValues")
						.add(Restrictions.and(Restrictions.eq("fieldCn", COSTS_OF_COMPLAINT_DICTIONARY_CN), Restrictions.in("fieldVal", reclamationCostsIds)));
				} else {
					crit.createCriteria("multipleValues")
							.add(Restrictions.or
									(Restrictions.and(Restrictions.eq("fieldCn", DOT_ZAKLADU), Restrictions.eq("fieldVal", dotyczyZakladu.toString())),
									Restrictions.and(Restrictions.eq("fieldCn", COSTS_OF_COMPLAINT_DICTIONARY_CN), Restrictions.in("fieldVal", reclamationCostsIds))));
				}
			}
			if (dotyczyZakladu != null && !wasReclamationCostsDictionary) {
				//crit.add(Restrictions.eq("dotyczyZakladu", dotyczyZakladu));
				crit.createCriteria("multipleValues")
					.add(Restrictions.and(Restrictions.eq("fieldCn", DOT_ZAKLADU), Restrictions.eq("fieldVal", dotyczyZakladu.toString())));
			}
			if (sortField == null) {
				sortField = "statusNiezgodnosci";
			}

			crit.addOrder(ascending ? Order.asc(sortField) : Order.desc(sortField));

			List<NiezgodnoscWew> results = crit.list();
			
			int totalCount = results.size();
			

			if (limit > 0 && !omitLimit) {
				crit.setFirstResult(offset);
				crit.setMaxResults(limit);
				results = crit.list();
			}
			
			for (NiezgodnoscWew item : results) {
				item.populateReclamationCosts();
			}
			
			return new SearchResultsAdapter<NiezgodnoscWew>(results, offset, totalCount, NiezgodnoscWew.class);

		} catch (EdmException e) {
			log.debug(e.getMessage(), e);
		}
		return null;
	}
	
//	private List<NiezgodnoscWew> filterByCostsOfComplaintDictionary(List<NiezgodnoscWew> results) {
//		List<NiezgodnoscWew> filterResults = new ArrayList<NiezgodnoscWew>();
//		try {
//			for(NiezgodnoscWew item : results) {
//				List<Long> costsDictionaryIds = (List<Long>) DSApi.context().session().createSQLQuery("SELECT FIELD_VAL FROM dsg_yetico_niezgodnosc_wew_multiple_value WHERE DOCUMENT_ID = :ids AND FIELD_CN = :dictionaryCn")
//						.setParameter("ids", item.getDocumentId())
//						.setParameter("dictionaryCn", COSTS_OF_COMPLAINT_DICTIONARY_CN)
//						.list();
//			}
//		} catch (EdmException e) {
//			log.error(e.getMessage(), e);
//		}
//		return filterResults;
//	}
	
	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public Integer getStatusNiezgodnosci() {
		return statusNiezgodnosci;
	}

	public String textStatus() {
		if (statusNiezgodnosci == null)
			return "-";
		try {
			return getEnumItemMap(STATUS_NIEZGODNOSCI).get(statusNiezgodnosci);
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
			return "-";
		}
	}

	public void setStatusNiezgodnosci(Integer status) {
		this.statusNiezgodnosci = status;
	}

	public Integer getStatusRealizacji() {
		return statusRealizacji;
	}

	public void setStatusRealizacji(Integer statusRealizacji) {
		this.statusRealizacji = statusRealizacji;
	}

	public String textStatusRealizacji() {
		if (statusRealizacji == null)
			return "-";
		try {
			return getEnumItemMap(STATUS).get(statusRealizacji);
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
			return "-";
		}
	}

	public Integer getRodzaj() {
		return rodzaj;
	}

	public void setRodzaj(Integer rodzaj) {
		this.rodzaj = rodzaj;
	}

	public String textRodzaj() {
		if (rodzaj == null)
			return "-";
		try {
			return Niezgodnosc.rodzajNiezgodnosciList().get(rodzaj);
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
			return "-";
		}
	}
	
	public Integer getTyp() {
		return typ;
	}
	
	public void setTyp(Integer typ) {
		this.typ = typ;
	}
	
	public String textTyp() {
		if (typ == null)
			return "-";
		try {
			return Niezgodnosc.typNiezgodnosciMap().get(typ);
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
			return "-";
		}
	}

	public Date getDataRejestracji() {
		try {
			return OfficeDocument.find(documentId).getCtime();
		} catch (DocumentNotFoundException e) {
			log.error(e.getMessage(), e);
			return null;
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
			return null;
		}
	}

	public DSUser getAutor() {
		return autor;
	}

	public void setAutor(DSUser autor) {
		this.autor = autor;
	}

	public String textAutor() {
		return autor != null ? autor.asFirstnameLastname() : "-";
	}

	public Date getDataWykrycia() {
		return dataWykrycia;
	}
	
	public String getDataWykryciaText() {
		return DateUtils.formatJsDate(dataWykrycia);
	}
	
	public void setDataWykrycia(Date dataWykrycia) {
		this.dataWykrycia = dataWykrycia;
	}

	public Integer getDotyczyZakladu() {
		return dotyczyZakladu;
	}

	public void setDotyczyZakladu(Integer dotyczyZakladu) {
		this.dotyczyZakladu = dotyczyZakladu;
	}

	public String textDotyczyZakladu() {
		if (dotyczyZakladu == null)
			return "-";
		try {
			return getEnumItemMap(DOT_ZAKLADU).get(dotyczyZakladu);
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
			return "-";
		}
	}
	
	public String getDotyczyZakladuString() {
		String result = "-";
		try {
			if (!DSApi.isContextOpen()) {
				DSApi.openAdmin();
			}
			FieldsManager fm = OfficeDocument.find(getDocumentId()).getFieldsManager();
			List<String> institutionsShorts = (List<String>)fm.getValue(DOT_ZAKLADU);
			if (!DSApi.isContextOpen()) {
				DSApi.close();
			}
			Joiner joiner = Joiner.on(", ").skipNulls();
			result = joiner.join(institutionsShorts);
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}
		return result;
	}

	public String getMiejsceWykrycia() {
		return miejsceWykrycia;
	}

	public void setMiejsceWykrycia(String miejsceWykrycia) {
		this.miejsceWykrycia = miejsceWykrycia;
	}

	public String getOsobaPoinformowana() {
		return osobaPoinformowana;
	}

	public void setOsobaPoinformowana(String osobaPoinformowana) {
		this.osobaPoinformowana = osobaPoinformowana;
	}

	public String getNrZdjecia() {
		return nrZdjecia;
	}

	public void setNrZdjecia(String nrZdjecia) {
		this.nrZdjecia = nrZdjecia;
	}

	public String getInformacje() {
		return informacje;
	}

	public void setInformacje(String informacje) {
		this.informacje = informacje;
	}

	public String getNrReklamacji() {
		return nrReklamacji;
	}

	public void setNrReklamacji(String nrReklamacji) {
		this.nrReklamacji = nrReklamacji;
	}

	public BigDecimal getLacznyKoszt() {
		return lacznyKoszt;
	}

	public void setLacznyKoszt(BigDecimal lacznyKoszt) {
		this.lacznyKoszt = lacznyKoszt;
	}

	public static Map<Integer, String> getEnumItemMap(String fieldCN) throws EdmException {
		List<EnumItem> enumItems;
		Map<Integer, String> results = new HashMap<Integer, String>();
		boolean isOpened = true;
		try {
			if (!DSApi.isContextOpen())
			{   
				isOpened = false; 
				DSApi.openAdmin();    
			}
			enumItems = DocumentKind.findByCn(NIEZGODNOSC_WEW_CN).getFieldByCn(fieldCN).getEnumItems();
			for (EnumItem ei : enumItems) {
				results.put(ei.getId(), ei.getTitle());
			}
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}finally
		{
			if (DSApi.isContextOpen() && !isOpened) 
			{ 
				DSApi.close();    
			}
		}
		return results;
	}
	
	public String getOpisyDzialanKorygujacych() {
		
//		OfficeDocument doc = OfficeDocument.find(documentId);
//		FieldsManager fm = doc.getFieldsManager();
//		fm.getKey(ZadanieDictionary.DZIALANIA_KORYGUJACE);
		try {
			StringBuilder sb = new StringBuilder("");
			for (Zadania zadanie : zadania) {
				if (zadanie.getTyp() == 25) {
					sb.append(zadanie.getOpis() + "; ");
				}
			}
			if (sb.length() == 0) {
				return "Brak dzia�a� koryguj�cych";
			}
			return sb.toString();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return "Nie uda�o si� pobra� dzia�a� koryguj�cych";
		}
	}

	public Set<Zadania> getZadania() {
		return zadania;
	}

	public void setZadania(Set<Zadania> zadania) {
		this.zadania = zadania;
	}

	/**
	 * 
	 * @return liste obiektow reprezentujacych poszczegolne wpisy w slowniku "Koszty reklamacji" dla danej niezgodnosci. 
	 */
	public void populateReclamationCosts() {
		try {
			List<String> dictionaryIds = (List<String>) DSApi.context().session().createSQLQuery("SELECT FIELD_VAL FROM dsg_yetico_niezgodnosc_wew_multiple_value WHERE DOCUMENT_ID = :documentId AND FIELD_CN = :dictionaryCn")
					.setParameter("documentId", documentId)
					.setParameter("dictionaryCn", COSTS_OF_COMPLAINT_DICTIONARY_CN)
					.list();
			Function<String, Long> idsFromStringToLongFunction = new Function<String, Long>() {
				public Long apply(String id) {
					return Long.valueOf(id);
				}
			};
			Collection<Long> reclamationCostsIds = Collections2.transform(dictionaryIds, idsFromStringToLongFunction);
			if (!dictionaryIds.isEmpty()) {
				List<ReclamationCosts> reclamationCostsList = (List<ReclamationCosts>) DSApi.context().session().createCriteria(ReclamationCosts.class)
						.add(Restrictions.in("id", reclamationCostsIds))
						.list();
				reclamationCosts = new HashSet<ReclamationCosts>(reclamationCostsList);
			}
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}
	}
	
	public Set<NiezgodnoscWewMultiple> getMultipleValues() {
		return multipleValues;
	}

	public void setMultipleValues(Set<NiezgodnoscWewMultiple> multipleValues) {
		this.multipleValues = multipleValues;
	}

	public Set<ReclamationCosts> getReclamationCosts() {
		return reclamationCosts;
	}

	public void setReclamationCosts(Set<ReclamationCosts> reclamationCosts) {
		this.reclamationCosts = reclamationCosts;
	}
}