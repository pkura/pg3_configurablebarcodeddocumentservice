package pl.compan.docusafe.parametrization.yetico.hb;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.record.projects.Project;

public class Klient
{
	private Long id;
	private String nazwa;
	private String miejscowosc;
	private Integer kraj;
	private String mail;
	
	public Klient()
	{
	}
	
	public Klient(Long id, String nazwa, String miejscowosc, Integer kraj, String mail)
	{
		this.id = id;
		this.nazwa = nazwa;
		this.miejscowosc = miejscowosc;
		this.kraj = kraj;
		this.mail = mail;
	}
	
	public String getNazwa()
	{
		return nazwa;
	}
	public void setNazwa(String nazwa)
	{
		this.nazwa = nazwa;
	}
	public String getMiejscowosc()
	{
		return miejscowosc;
	}
	public void setMiejscowosc(String miejscowosc)
	{
		this.miejscowosc = miejscowosc;
	}
	public Integer getKraj()
	{
		return kraj;
	}
	public void setKraj(Integer kraj)
	{
		this.kraj = kraj;
	}
	public String getMail()
	{
		return mail;
	}
	public void setMail(String mail)
	{
		this.mail = mail;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}
	
	public static Klient find(Long id) throws EdmException
	{
		Klient klient = (Klient) DSApi.getObject(Klient.class, id);

		if (klient == null)
			throw new EntityNotFoundException(Klient.class, id);

		return klient;
	}
	
	public static List<Klient> list() throws EdmException
	{
		return DSApi.context().session().createCriteria(Klient.class).list();
	}
}
