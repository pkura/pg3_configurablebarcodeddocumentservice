/**
 * 
 */
package pl.compan.docusafe.parametrization.yetico.hb;

import com.google.common.collect.ImmutableMap;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Map;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
@Entity
@Table(name = RodzajMaszynyhb.TABLE_NAME)
public class RodzajMaszynyhb implements java.io.Serializable {

	public static final String TABLE_NAME = "dsg_yetico_koszty_reklamacji";

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "zaklad")
	private Long works;
	@Column(name = "typ_kosztu")
	private Long typeOfCost;
	@Column(name = "opis", length = 2048)
	private String description;
	@Column(name = "koszt_jednostkowy")
	private BigDecimal unitCost;
	@Column(name = "ilosc")
	private BigDecimal number;
	@Column(name = "koszt_calkowity")
	private BigDecimal totalCost;

	public static final Map<Integer, String> TYPES_OF_COST = ImmutableMap.<Integer, String>builder()
			.put(10, "Wytworzenie")
			.put(15, "Folia")
			.put(20, "Transport PL")
			.put(25, "Transport DE")
			.put(30, "Recykling")
			.put(35, "Inne")
			.build();

	public RodzajMaszynyhb() {

	}

	public RodzajMaszynyhb(Long works, Long typeOfCost,
                           String description, BigDecimal unitCost, BigDecimal number,
                           BigDecimal totalCost) {
		super();
		this.works = works;
		this.typeOfCost = typeOfCost;
		this.description = description;
		this.unitCost = unitCost;
		this.number = number;
		this.totalCost = totalCost;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getWorks() {
		return works;
	}
	
	public void setWorks(Long works) {
		this.works = works;
	}
	
	public Long getTypeOfCost() {
		return typeOfCost;
	}
	
	public void setTypeOfCost(Long typeOfCost) {
		this.typeOfCost = typeOfCost;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public BigDecimal getUnitCost() {
		return unitCost;
	}
	
	public void setUnitCost(BigDecimal unitCost) {
		this.unitCost = unitCost;
	}
	
	public BigDecimal getNumber() {
		return number;
	}
	
	public void setNumber(BigDecimal number) {
		this.number = number;
	}
	
	public BigDecimal getTotalCost() {
		return totalCost;
	}
	
	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}
	
}
