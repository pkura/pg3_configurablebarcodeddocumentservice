/**
 * 
 */
package pl.compan.docusafe.parametrization.yetico.hb;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class NiezgodnoscWewMultiple implements java.io.Serializable {
	public Long documentId;
	public String fieldCn;
	public String fieldVal;
	
	public NiezgodnoscWewMultiple() {
		
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public String getFieldCn() {
		return fieldCn;
	}

	public void setFieldCn(String fieldCn) {
		this.fieldCn = fieldCn;
	}

	public String getFieldVal() {
		return fieldVal;
	}

	public void setFieldVal(String fieldVal) {
		this.fieldVal = fieldVal;
	}
	
	
}
