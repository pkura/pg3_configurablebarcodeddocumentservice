package pl.compan.docusafe.parametrization.yetico.hb;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.jfree.util.Log;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.absences.Absence;

public class Wyrob
{

	private Long id;
	private String nazwa;
	private String wymiar;
	private Integer rodzaj;
	private String rodzajStr;
	private Integer typ;
	private String typStr;
	private String opis;
	private String nrFaktury;
	private Integer iloscRekWyrobu;
	private Integer iloscWyrNaFakturze;
	private Integer jm;
	private Date dataGodzinaProdukcji;
	private Date dataZaladunku;
	private String gniazdo;
	private String zmiana;
	private Date dataDostawy;
	private String miejsceDostawy;
	private String nrZlecenia;


    private String klientId;
	
	public Wyrob(){
		
	}
	
	public Wyrob(Long id, String nazwa, String wymiar, Integer rodzaj, Integer typ, String opis, String nrFaktury,
				 Integer iloscRekWyrobu, Integer iloscWyrNaFakturze, Integer jm, Date dataGodzinaProdukcji, Date dataZaladunku,
				 String gniazdo, String zmiana, Date dataDostawy, String miejsceDostawy, String nrZlecenia,String klientId)
	{
		this.id = id;
		this.nazwa = nazwa;
		this.wymiar = wymiar;
		this.rodzaj = rodzaj;
		this.typ = typ;
		this.opis = opis;
		this.nrFaktury = nrFaktury;
		this.iloscRekWyrobu = iloscRekWyrobu;
		this.iloscWyrNaFakturze = iloscWyrNaFakturze;
		this.jm = jm;
		this.dataGodzinaProdukcji = dataGodzinaProdukcji;
		this.dataZaladunku = dataZaladunku;
		this.gniazdo = gniazdo;
		this.zmiana = zmiana;
		this.dataDostawy = dataDostawy;
		this.miejsceDostawy = miejsceDostawy;
		this.nrZlecenia = nrZlecenia;
        this.klientId= klientId;
	}
	
	public long getId(){
		return id;
	}
	public void setId(Long id){
		this.id = id;
	}

    public String getKlientId() {
        return klientId;
    }

    public void setKlientId(String klientId) {
        this.klientId = klientId;
    }
	public String getNazwa(){
		return nazwa;
	}	
	public void setNazwa(String nazwa){
		this.nazwa = nazwa;
	}	
	public String getWymiar(){
		return wymiar;
	}	
	public void setWymiar(String wymiar){
		this.wymiar = wymiar;
	}	
	public Integer getRodzaj(){
		return rodzaj;
	}	
	public void setRodzaj(Integer rodzaj){
		this.rodzaj = rodzaj;
	}	
	public Integer getTyp(){
		return typ;
	}	
	public void setTyp(Integer typ){
		this.typ = typ;
	}
	public String getOpis(){
		return opis;
	}	
	public void setOpis(String opis){
		this.opis = opis;
	}	
	public String getNrFaktury(){
		return nrFaktury;
	}	
	public void setNrFaktury(String nrFaktury){
		this.nrFaktury = nrFaktury;
	}	
	public Integer getIloscRekWyrobu(){
		return iloscRekWyrobu;
	}	
	public void setIloscRekWyrobu(Integer iloscRekWyrobu){
		this.iloscRekWyrobu = iloscRekWyrobu;
	}	
	public Integer getIloscWyrNaFakturze(){
		return iloscWyrNaFakturze;
	}	
	public void setIloscWyrNaFakturze(Integer iloscWyrNaFakturze){
		this.iloscWyrNaFakturze = iloscWyrNaFakturze;
	}	
	public Integer getJm() {
		return jm;
	}
	public void setJm(Integer jm) {
		this.jm = jm;
	}
	public Date getDataGodzinaProdukcji(){
		return dataGodzinaProdukcji;
	}	
	public void setDataGodzinaProdukcji(Date dataGodzinaProdukcji){
		this.dataGodzinaProdukcji = dataGodzinaProdukcji;
	}		
	public Date getDataZaladunku(){
		return dataZaladunku;
	}	
	public void setDataZaladunku(Date dataZaladunku){
		this.dataZaladunku = dataZaladunku;
	}	
	public String getGniazdo(){
		return gniazdo;
	}	
	public void setGniazdo(String gniazdo){
		this.gniazdo = gniazdo;
	}	
	public String getZmiana(){
		return zmiana;
	}	
	public void setZmiana(String zmiana){
		this.zmiana = zmiana;
	}	
	public Date getDataDostawy(){
		return dataDostawy;
	}	
	public void setDataDostawy(Date dataDostawy){
		this.dataDostawy = dataDostawy;
	}	
	public String getMiejsceDostawy(){
		return miejsceDostawy;
	}	
	public void setMiejsceDostawy(String miejsceDostawy){
		this.miejsceDostawy = miejsceDostawy;
	}	
	public String getNrZlecenia() {
		return nrZlecenia;
	}
	public void setNrZlecenia(String nrZlecenia) {
		this.nrZlecenia = nrZlecenia;
	}

	public String getRodzajStr() {
		return rodzajStr;
	}

	public void setRodzajStr(String rodzajStr) {
		this.rodzajStr = rodzajStr;
	}

	public String getTypStr() {
		return typStr;
	}

	public void setTypStr(String typStr) {
		this.typStr = typStr;
	}
	
	public static Wyrob findById(Long id) throws EdmException
	{
		Log.info("Id wyrobu = " + id);
		Criteria crit = DSApi.context().session().createCriteria(Wyrob.class);
		crit.add(Restrictions.eq("id", id));
		crit.setMaxResults(1);
		Wyrob wyrob = (Wyrob) crit.list().get(0);

		if (wyrob == null)
			throw new EntityNotFoundException(Wyrob.class, id);

		return wyrob;
	}

}
