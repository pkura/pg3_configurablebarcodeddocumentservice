package pl.compan.docusafe.parametrization.yetico;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

public class WyrobDictionary extends DwrDictionaryBase {
    protected static final String ERP_FLAG = " [erp]";
    private static Logger log = LoggerFactory.getLogger(WyrobDictionary.class);
    private boolean integrationWithSimpleERPIsOn = AvailabilityManager.isAvailable("integrationWithSimpleERPIsOn");
    @Override
    public int remove(String id) throws EdmException {
	return -1;
    }

    public int update(Map<String, FieldData> values) throws EdmException
    {
	    log.debug("update");
        if (integrationWithSimpleERPIsOn)
        {
            long ret = -1;
            try {
                boolean contextOpened = true;
                Statement stat = null;
                ResultSet result = null;
                if (values.get("ID") != null) {
                if (!DSApi.isContextOpen()) {
                    DSApi.openAdmin();
                    contextOpened = false;
                }
                stat = DSApi.context().createStatement();

                StringBuilder select = new StringBuilder("select base from dsg_yetico_wyrob where id = ").append(values.get("ID"));

                result = stat.executeQuery(select.toString());
                while (result.next()) {
                    Integer base = result.getInt("base");
                    if (base.equals(1))
                    {
                        long newId = super.add(values);
                        values.get("ID").setData(newId);
                        return Integer.parseInt(newId + "");
                    }
                    else if (base == null || base.equals(0))
                    {
                        return super.update(values);
                    }
                }
                }
            } catch (Exception e) {
                return -1;
            }
            return  -1;
        }
        else
            return super.update(values);

    }

    @Override
    public long add(Map<String, FieldData> values) throws EdmException {
        log.debug("add");
        if (integrationWithSimpleERPIsOn)
            return  -1;
        else
            return super.add(values);

    }

    @Override
    public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset) throws EdmException {
        return super.search(values, limit, offset);
    }
}
