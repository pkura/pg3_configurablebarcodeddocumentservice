package pl.compan.docusafe.parametrization.yetico;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

public class NiezgodnoscWewLogic extends AbstractDocumentLogic
{
	private final static Logger log = LoggerFactory.getLogger(NiezgodnoscWewLogic.class);
	public static final String DYREKTOR_DZIALU = "dyrektor_dzialu";
	
	public static final String DZIALANIA_KORYGUJACE_EDITABLE_FIELD_CN = "DZIALANIA_KORYGUJACE_EDITABLE";
	
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		Map<String, Object> toReload = Maps.newHashMap();
		for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
		{
			if (f.getDefaultValue() != null)
			{
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
			}
		}

		toReload.put("AUTOR", DSApi.context().getDSUser().getId());
		toReload.put("DATA_REJESTRACJI", new Date());
		fm.reloadValues(toReload);
	}
	
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			if (event != null) {
				map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
				map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			}

			Map<String, Object> fieldValues = new HashMap<String, Object>();
			fieldValues.put("NR_REKLAMACJI", generateNrReklamacji(document));
			document.getDocumentKind().setOnly(document.getId(), fieldValues);
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

			java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

			DSUser author = DSApi.context().getDSUser();
			String user = author.getName();
			String fullName = author.getLastname() + " " + author.getFirstname();

			perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

			Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
			perms.addAll(documentPermissions);
			this.setUpPermission(document, perms);
			
			DSApi.context().watch(URN.create(document));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

	/**
	 * Automatyczna numeracja rejestrowanych reklamacji wg wzoru: A/B/C: 
	 * 
	 * A – Kolejny numer niezgodności w danym roku 
	 * B – Data rejestracji(miesiąc.rok) 
	 * C – Kod zakładu (GAL, GW, OL)
	 * 
	 * @throws EdmException 
	 * 
	 */
	private String generateNrReklamacji(OfficeDocument document) throws EdmException {
		String nrNiezgodnosci = "-";
		try {
			FieldsManager fm = document.getFieldsManager();
			String a = "";
			try {
				PreparedStatement ps = DSApi.context().prepareStatement(
						"select count(*) from dsg_yetico_niezgodnosc_wew n join ds_document doc on n.document_id ="
								+ "doc.ID where YEAR(CTIME) = ?");
				Calendar cal = Calendar.getInstance();
				Integer calYear = cal.get(Calendar.YEAR);
				ps.setInt(1, calYear);
				ResultSet rs = ps.executeQuery();
				boolean hasNext = rs.next();
				a = hasNext ? String.valueOf(rs.getInt(1)) : "1";
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				throw new EdmException("Błąd przy nadaniu numeru niezgodności", e);
			}
			String b = new SimpleDateFormat("MM-yyyy").format(document.getCtime());
			String c = getZaklad(fm);
			nrNiezgodnosci = a + "/" + b + "/" + c;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new EdmException("Błąd przy nadaniu numeru niezgodności", e);
		}

		return nrNiezgodnosci;
	}
	
	private String getZaklad(FieldsManager fm) {
		String s = "";
		try {
			List<String> ids = (List<String>)fm.getValue("DOT_ZAKLADU");
			for (int i=0; i<ids.size(); i++) {
				if (i == 0) {
					s += ids.get(i);
				}
				else {
					s += "," + ids.get(i);
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return s;
	}

	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		Field msg = null;
		
		if (values.containsKey("DWR_KOSZTY_REKLAMACJI"))
		{
			Map<String, FieldData> slownikKS = values.get("DWR_KOSZTY_REKLAMACJI").getDictionaryData();

			String suffix = null;
			if (slownikKS != null) 
			{
				BigDecimal suma = new BigDecimal(0);
				String zaladCn = null;
				String typIdCn = null;
				String iloscCn = null;
				for (String cn : slownikKS.keySet()) 
				{
					if (cn.contains("ILOSC_") && slownikKS.get(cn).getData() != null) 
					{
						suffix = cn.substring(cn.lastIndexOf("_"));
						zaladCn = "KOSZTY_REKLAMACJI_ZAKLAD".concat(suffix);
						typIdCn = "KOSZTY_REKLAMACJI_TYP_KOSZTU".concat(suffix);
						iloscCn = "KOSZTY_REKLAMACJI_ILOSC".concat(suffix);
						if (slownikKS.get(zaladCn).getData() != null 
								&& slownikKS.get(typIdCn).getData() != null
									&& slownikKS.get(iloscCn).getData() != null) 
						{
							String zakladId = slownikKS.get(zaladCn).getEnumValuesData().getSelectedOptions().get(0);
							String typId = slownikKS.get(typIdCn).getEnumValuesData().getSelectedOptions().get(0);
							BigDecimal ilosc = slownikKS.get(iloscCn).getMoneyData();
							BigDecimal koszt = getKoszt(zakladId, typId);
							BigDecimal kosztCalkowity = koszt.multiply(ilosc).setScale(2, RoundingMode.HALF_UP);
							suma = suma.add(kosztCalkowity);
						}
					}
				}
				values.get("DWR_LACZNY_KOSZT").setMoneyData(suma);
			}
		}
		if (values.get("DWR_WYROB") != null) {
			Map<String, FieldData> dict = values.get("DWR_WYROB").getDictionaryData();
			String contentMsg = "\"Ilo�� reklamowanego wyrobu\" nie mo�e by� wi�ksza od \"Ilo�� wyrobu na fakturze\"!!!";
			msg = validateFieldsInDict(values, dict, "WYROB_ILOSC_REK_WYROBU", "WYROB_ILOSC_WYROBU_NA_FAKTURZE", "FAKTURZE", "BigDecimal", contentMsg);
			if (msg != null) {
				return msg;
			}
		}
		if (values.get("DWR_DODATKOWE_BADANIA_EDITABLE") != null) {
			Map<String, FieldData> dict = values.get("DWR_DODATKOWE_BADANIA_EDITABLE").getDictionaryData();
			String contentMsg = "\"Termin do\" nie mo�e by� wcze�niejszy od \"Terminu od\"!!!";
			msg = validateFieldsInDict(values, dict, "DODATKOWE_BADANIA_EDITABLE_OD", "DODATKOWE_BADANIA_EDITABLE_DO", "DO_", "Date", contentMsg);
			if (msg != null) {
				return msg;
			}
		}
		if (values.get("DWR_DZIALANIA_KORYGUJACE_EDITABLE") != null) {
			Map<String, FieldData> dict = values.get("DWR_DZIALANIA_KORYGUJACE_EDITABLE").getDictionaryData();
			String contentMsg = "\"Termin do\" nie mo�e by� wcze�niejszy od \"Terminu od\"!!!";
			msg = validateFieldsInDict(values, dict, "DZIALANIA_KORYGUJACE_EDITABLE_OD", "DZIALANIA_KORYGUJACE_EDITABLE_DO", "DO_", "Date", contentMsg);
			if (msg != null) {
				return msg;
			}
            contentMsg = "\"Termin do\" nie mo�e by� p�niej ni� 30 dni od \"Terminu od\"!!!";
            msg = validateFieldsInDict30Days(values, dict, "DZIALANIA_KORYGUJACE_EDITABLE_OD", "DZIALANIA_KORYGUJACE_EDITABLE_DO", "DO_", "Date", contentMsg);
            if (msg != null) {
                return msg;
            }
		}
		return msg;
	}

    private Field validateFieldsInDict(Map<String, FieldData> values, Map<String, FieldData> dict, String smallerFieldCn,
                                       String biggerFieldCn, String wordKey, String typeField, String contentMsg) {
        if (dict != null) {
            String suffix = null;
            for (String cn : dict.keySet()) {
                if (cn.contains(wordKey) && dict.get(cn).getData() != null) {
                    suffix = cn.substring(cn.lastIndexOf("_"));
                    smallerFieldCn = smallerFieldCn.concat(suffix);
                    biggerFieldCn = biggerFieldCn.concat(suffix);
                    if (dict.containsKey(smallerFieldCn) && dict.containsKey(biggerFieldCn) && dict.get(smallerFieldCn).getData() != null && dict.get(biggerFieldCn).getData() != null) {
                        if (typeField.equals("Date")) {
                            Date smallerField = dict.get(smallerFieldCn).getDateData();
                            Date biggerField = dict.get(biggerFieldCn).getDateData();
                            if (biggerField.before(smallerField)) {
                                Field msg = new Field("messageField", contentMsg, Field.Type.BOOLEAN);
                                values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
                                return msg;
                            }
                        } else if (typeField.equals("Integer")) {
                            Integer smallerField = dict.get(smallerFieldCn).getIntegerData();
                            Integer biggerField = dict.get(biggerFieldCn).getIntegerData();
                            if (biggerField.compareTo(smallerField) < 0) {
                                Field msg = new Field("messageField", contentMsg, Field.Type.BOOLEAN);
                                values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
                                return msg;
                            }
                        } else if (typeField.equals("BigDecimal")) {
							BigDecimal smallerField = dict.get(smallerFieldCn).getMoneyData();
							BigDecimal biggerField = dict.get(biggerFieldCn).getMoneyData();
							if (biggerField.compareTo(smallerField) < 0) {
								Field msg = new Field("messageField", contentMsg, Field.Type.BOOLEAN);
								values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
								return msg;
							}
						}
                    }
                }
            }
        }
        return null;
    }

	private Field validateFieldsInDict30Days(Map<String, FieldData> values, Map<String, FieldData> dict, String firstDateCn,
			String secondFieldCn, String wordKey, String typeField, String contentMsg) {
		if (dict != null) {
			String suffix = null;
			for (String cn : dict.keySet()) {
				if (cn.contains(wordKey) && dict.get(cn).getData() != null) {
					suffix = cn.substring(cn.lastIndexOf("_"));
                    firstDateCn = firstDateCn.concat(suffix);
                    secondFieldCn = secondFieldCn.concat(suffix);
					if (dict.get(firstDateCn).getData() != null && dict.get(secondFieldCn).getData() != null) {
						if (typeField.equals("Date")) {
							Date firstDate = dict.get(firstDateCn).getDateData();
							Date secondDate = dict.get(secondFieldCn).getDateData();

                            firstDate.setTime(firstDate.getTime() + 30L * 24 * 60 * 60 * 1000);

                            if (firstDate.before(secondDate)) {
								Field msg = new Field("messageField", contentMsg, Field.Type.BOOLEAN);
								values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
								return msg;
							}
                        }
                    }
                }
            }
        }
	    return null;
	}
	
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) {
		Map<String, Object> results = new HashMap<String, Object>();
		try {
			if (dictionaryName.equals("WYROB"))
			{
				// rodzaj
				Map<String, Object> fieldsValues = new HashMap<String, Object>();
				EnumValues rodzajIdEnum = values.get(dictionaryName + "_RODZAJ").getEnumValuesData();
				String rodzajId = rodzajIdEnum.getSelectedOptions().get(0);

				if (rodzajId != null && !rodzajId.equals(""))
				{
					fieldsValues.put("RODZAJ_1", rodzajId);
					for (DataBaseEnumField d : DataBaseEnumField.tableToField.get("dsg_yetico_typ_niezgodnosci"))
					{
						EnumValues val = d.getEnumItemsForDwr(fieldsValues);
						results.put(dictionaryName + "_TYP", val);
						break;
					}
					String typId = values.get(dictionaryName + "_TYP").getEnumValuesData().getSelectedOptions().get(0);
					if (typId != null && !typId.equals(""))
					{
						// wybrana pozycja budzetowa
						List<String> selectedPositionId = new ArrayList<String>();
						selectedPositionId.add(typId);
						((EnumValues) results.get(dictionaryName + "_TYP")).setSelectedOptions(selectedPositionId);
					}
				}
				else
				{
					EnumValues val = new EnumValues(Collections.EMPTY_MAP, Collections.EMPTY_LIST);
					results.put(dictionaryName + "_TYP", val);
				}
			} else if (dictionaryName.equals("KOSZTY_REKLAMACJI")) {
				String zakladId = values.get(dictionaryName + "_ZAKLAD").getEnumValuesData().getSelectedOptions().get(0);
				String typKosztuId = values.get(dictionaryName + "_TYP_KOSZTU").getEnumValuesData().getSelectedOptions().get(0);
				if (zakladId != null && !zakladId.equals("") && typKosztuId != null && !typKosztuId.equals("")) {
					BigDecimal koszt = getKoszt(zakladId, typKosztuId);
					results.put(dictionaryName + "_KOSZT_JEDNOSTKOWY", koszt);
					BigDecimal ilosc = values.get(dictionaryName + "_ILOSC").getMoneyData();
					if (ilosc != null) {
						BigDecimal kosztCalkowity = koszt.multiply(ilosc).setScale(2, RoundingMode.HALF_UP);
						results.put(dictionaryName + "_KOSZT_CALKOWITY", kosztCalkowity);
					}
				}
			} else if (dictionaryName.equals("DODATKOWE_BADANIA_EDITABLE") || dictionaryName.equals("DZIALANIA_KORYGUJACE_EDITABLE"))
			{
				Map<String, Object> fieldsValues = new HashMap<String, Object>();
				// status
				String statusId = values.get(dictionaryName + "_STATUS").getEnumValuesData().getSelectedOptions().get(0);

				if (statusId == null || statusId.equals(""))
				{
					for (DataBaseEnumField d : DataBaseEnumField.tableToField.get("dsg_yetico_zadanie_status"))
					{
						EnumValues val = d.getEnumItemsForDwr(fieldsValues);
						results.put(dictionaryName + "_STATUS", val);
						break;
					}
					// wybrana pozycja budzetowa
					List<String> selectedPositionId = new ArrayList<String>();
					statusId = "10";
					selectedPositionId.add(statusId);
					((EnumValues) results.get(dictionaryName + "_STATUS")).setSelectedOptions(selectedPositionId);
				}
			} else if (dictionaryName.equals("WYMAGANA_MASA_GESTOSC_WYROBU")) {
				BigDecimal requiredWeight = values.get(dictionaryName + "_WYMAGANA_MASA").getMoneyData();
				BigDecimal measuredWeight = values.get(dictionaryName + "_MASA_ZMIERZONA").getMoneyData();
				if (requiredWeight != null && measuredWeight != null) {
					BigDecimal res = requiredWeight.subtract(measuredWeight);
					results.put(dictionaryName + "_ODCHYLKA_MASY", res);
				}
				BigDecimal requiredDensity = values.get(dictionaryName + "_WYMAGANA_GESTOSC").getMoneyData();
				BigDecimal measuredDensity = values.get(dictionaryName + "_GESTOSC_ZMIERZONA").getMoneyData();
				if (requiredDensity != null && measuredDensity != null) {
					BigDecimal res = requiredDensity.subtract(measuredDensity);
					results.put(dictionaryName + "_ODCHYLKA_GESTOSCI", res);
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return results;
	}
	

	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException {
		TaskListParams params = new TaskListParams();
		try {
			FieldsManager fm = dockind.getFieldsManager(id);
			params.setAttribute(fm.getValue("RODZAJ").toString(), 0);
			params.setAttribute(fm.getValue("TYP").toString(), 1);
			params.setDocumentNumber((String)fm.getKey("NR_REKLAMACJI"));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return params;
	}

	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{

	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue)
	{
		boolean assigned = false;
		if (DYREKTOR_DZIALU.equalsIgnoreCase(acceptationCn)) {
			assigned = assignToDyrektorDzialu(doc, assignable, openExecution, acceptationCn, fieldCnValue, assigned);
		}
		return assigned;
	}

	private boolean assignToDyrektorDzialu(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn,
			String fieldCnValue, boolean assigned) {
		List<String> users = new ArrayList<String>();
		List<String> divisions = new ArrayList<String>();
		try {
			if (fieldCnValue == null)
				fieldCnValue = DSApi.context().getDSUser().getDivisionsWithoutGroupPosition()[0].getGuid();
			for (AcceptanceCondition accept : AcceptanceCondition.find(acceptationCn, fieldCnValue)) {
				if (accept.getUsername() != null && !users.contains(accept.getUsername()))
					users.add(accept.getUsername());
				if (accept.getDivisionGuid() != null && !divisions.contains(accept.getDivisionGuid()))
					divisions.add(accept.getDivisionGuid());
			}
			for (String user : users)
			{
				assignable.addCandidateUser(user);
					AssigneeHandler.addToHistory(openExecution, doc, user, null);
				assigned = true;
			}
			for (String division : divisions)
			{
				assignable.addCandidateGroup(division);
				AssigneeHandler.addToHistory(openExecution, doc, null, division);
				assigned = true;
			}
		} catch (UserNotFoundException e) {
			log.error(e.getMessage(), e);
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}
		return assigned;
	}

	public void documentPermissions(Document document) throws EdmException
	{

	}
	
	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		log.info("Before Search - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.contains("WYROB"))
		{
		    if (values.get("id") != null && !values.get("id").equals(""))
			return;
			values.put(dictionaryName + "_BASE", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.INTEGER, 1));
		}
	}

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		String docKindTitle = document.getDocumentKind().getName();
		FieldsManager fm = document.getFieldsManager();
		
		List<Long> ids = (List<Long>) fm.getKey(DZIALANIA_KORYGUJACE_EDITABLE_FIELD_CN);
		if (ids != null && !ids.isEmpty()) {
			List<Object[]> result = DSApi.context().session().createSQLQuery("SELECT od, do FROM dsg_yetico_zadanie WHERE document_id IN :ids ")
				.setParameterList("ids", ids)
				.list();
			for (Object[] row : result) {
				try {
					Date firstDate = DateUtils.parseDateAnyFormat(row[0].toString());
					Date secondDate = DateUtils.parseDateAnyFormat(row[1].toString());
					firstDate.setTime(firstDate.getTime() + 30L * 24 * 60 * 60 * 1000);
					if (firstDate.before(secondDate)) {
						throw new EdmException("\"Termin do\" nie mo�e by� p�niej ni� 30 dni od \"Terminu od\"!!!");
					}
				} catch (ParseException e) {
					log.error(e.getMessage(), e);
				}
			}
		}
		
		moveValuesToAnotherDictionary(document, "DODATKOWE_BADANIA_EDITABLE", "DODATKOWE_BADANIA");
		moveValuesToAnotherDictionary(document, "DZIALANIA_KORYGUJACE_EDITABLE", "DZIALANIA_KORYGUJACE");
		
		setReklamacjaId(document, fm, "DODATKOWE_BADANIA");
		setReklamacjaId(document, fm, "DZIALANIA_KORYGUJACE");
        setReklamacjaId(document, fm, "DZIALANIA_KORYGUJACE_EDITABLE");
        setReklamacjaId(document, fm, "DODATKOWE_BADANIA_EDITABLE");
	}
	
	private void setReklamacjaId(Document document, FieldsManager fm, String dictionaryCn) throws EdmException, DocumentNotFoundException, DocumentLockedException, AccessDeniedException
	{
		List<Long> zadania = (List<Long>)fm.getKey(dictionaryCn);
		if (zadania != null)
			for (Long zadanieId : zadania)
			{
				OfficeDocument zadanie = (OfficeDocument) Document.find(zadanieId, false);
				Long document_id = zadanie.getFieldsManager().getLongKey("NIEZGODNOSC");
				if (document_id == null)
				{
					Map<String, Object> fieldValues = new HashMap<String, Object>();
					fieldValues.put("NIEZGODNOSC", document.getId());
					zadanie.getDocumentKind().setOnly(zadanieId, fieldValues);
				}
			}
	}
	
	private BigDecimal getKoszt(String zakladId, String typKosztuId) throws NumberFormatException, EdmException
	{

		String idStawki = typKosztuId+zakladId;
		String stawkaString = DataBaseEnumField.getEnumItemForTable("dsg_yetico_stawki_reklamacji", Integer.parseInt(idStawki)).getRefValue();

		return new BigDecimal(stawkaString).setScale(2, RoundingMode.HALF_UP);
	}
	
	
	private void moveValuesToAnotherDictionary(Document document, String fromFieldCn, String toFieldCn) throws EdmException {
		FieldsManager fm = document.getFieldsManager();
		Map<String, Object> fieldValues = new HashMap<String, Object>();
		if ((fromFieldCn != null) && (toFieldCn != null) && (fm.getKey(fromFieldCn) != null)) {
			if (fm.getKey(toFieldCn) == null) {
				fieldValues.put(toFieldCn, fm.getKey(fromFieldCn));
			} else if (fm.getKey(toFieldCn) instanceof List) {
				List toFieldVals = (List)fm.getKey(toFieldCn);
				if (fm.getKey(fromFieldCn) instanceof List) {
					toFieldVals.addAll((List)fm.getKey(fromFieldCn));
				} else {
					toFieldVals.add(fm.getKey(fromFieldCn));
				}
				fieldValues.put(toFieldCn, toFieldVals);
			}
			fieldValues.put(fromFieldCn, null);
			document.getDocumentKind().setOnly(document.getId(), fieldValues);
		}
	}

}