/**
 * 
 */
package pl.compan.docusafe.parametrization.yetico;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class PlanPrewencyjnyLogic extends AbstractDocumentLogic {

	private static final Logger logger = LoggerFactory.getLogger(ListaPrewencyjnaLogic.class);
	
	/**
	 * Nazwa kodowa(Cn) dokumentu: plan_prewencyjny
	 */
	public static final String PLAN_PREWENCYJNY = "plan_prewencyjny";
	
	/**
	 * Nazwa kodowa(Cn) pola znajduj�cego si� w dockind: plan_prewencyjny
	 */
	public static final String ROK = "ROK";
	
	/**
	 * Nazwa kodowa(Cn) s�ownika znajduj�cego si� w dockind: plan_prewencyjny 
	 */
	public static final String LISTA_PREWENCYJNA_DICT = "LISTA_PREWENCYJNA_DICT";
	
	public void documentPermissions(Document document) throws EdmException {

	}

	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {

	}

}
