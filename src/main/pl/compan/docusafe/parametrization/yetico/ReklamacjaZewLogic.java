package pl.compan.docusafe.parametrization.yetico;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import org.joda.time.DateTime;
import org.joda.time.Days;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContextOpener;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.yetico.hb.Klient;
import pl.compan.docusafe.parametrization.yetico.hb.Niezgodnosc;
import pl.compan.docusafe.parametrization.yetico.hb.Wyrob;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

public class ReklamacjaZewLogic extends AbstractDocumentLogic
{

	private final static Logger log = LoggerFactory.getLogger(ReklamacjaZewLogic.class);
	public static final String NIEZGODNOSC_RODZAJ_JAKOSCIOWE_CN = "JAKOSCIOWE";
	/**
	 * Nazwa kodowa(Cn) jednej z mo�liwych warto�ci pola: STATUS w dockind: rek_zew
	 */
	public static final String DYR_BZ_DECYZJA_RODZAJ_REKOMPENSATY = "DYR_BZ_DECYZJA_RODZAJ_REKOMPENSATY";
	
	/**
	 * Nazwa kodowa(Cn) jednej z mo�liwych warto�ci pola: STATUS w dockind: rek_zew
	 */
	public static final String DYR_BZ_DECYZJA_RODZAJ_REKOMPENSATY_2 = "DYR_BZ_DECYZJA_RODZAJ_REKOMPENSATY_2";
	
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		Field msg = null;
		
		if (values.containsKey("DWR_KOSZTY_REKLAMACJI"))
		{
			Map<String, FieldData> slownikKR = values.get("DWR_KOSZTY_REKLAMACJI").getDictionaryData();

			String suffix = null;
			if (slownikKR != null) 
			{
				BigDecimal suma = new BigDecimal(0);
				String zaladCn = null;
				String typIdCn = null;
				String iloscCn = null;
				for (String cn : slownikKR.keySet()) 
				{
					if (cn.contains("ILOSC_") && slownikKR.get(cn).getData() != null) 
					{
						suffix = cn.substring(cn.lastIndexOf("_"));
						zaladCn = "KOSZTY_REKLAMACJI_ZAKLAD".concat(suffix);
						typIdCn = "KOSZTY_REKLAMACJI_TYP_KOSZTU".concat(suffix);
						iloscCn = "KOSZTY_REKLAMACJI_ILOSC".concat(suffix);
						if (slownikKR.get(zaladCn).getData() != null 
								&& slownikKR.get(typIdCn).getData() != null
									&& slownikKR.get(iloscCn).getData() != null) 
						{
							String zakladId = slownikKR.get(zaladCn).getEnumValuesData().getSelectedOptions().get(0);
							String typId = slownikKR.get(typIdCn).getEnumValuesData().getSelectedOptions().get(0);
							BigDecimal ilosc = slownikKR.get(iloscCn).getMoneyData();
							BigDecimal koszt = getKoszt(zakladId, typId);
							BigDecimal kosztCalkowity = koszt.multiply(ilosc).setScale(2, RoundingMode.HALF_UP);
							suma = suma.add(kosztCalkowity);
						}
					}
				}
				values.get("DWR_LACZNY_KOSZT").setMoneyData(suma);
			}
		}

		if (values.get("DWR_WYROB") != null) {
			Map<String, FieldData> dict = values.get("DWR_WYROB").getDictionaryData();
			String contentMsg = "\"Ilo�� reklamowanego wyrobu\" nie mo�e by� wi�ksza od \"Ilo�� wyrobu na fakturze\"!!!";
			msg = validateFieldsInDict(values, dict, "WYROB_ILOSC_REK_WYROBU", "WYROB_ILOSC_WYROBU_NA_FAKTURZE", "WYROB_ILOSC_WYROBU_NA_FAKTURZE", "BigDecimal", contentMsg);
			if (msg != null) {
				return msg;
			}


		}

       if(DwrUtils.isSenderField(values, "DWR_WYROB")){

      //  List<Object> lista =DwrUtils.getValueListFromDictionary(values,"WYROB",true,"KLIENT_ID");
           List<Object> lista =DwrUtils.getValueListFromDictionary(values,"WYROB",true,"ID");
           if(lista.size()>0){
        String l = lista.get(0).toString();
        values.get("DWR_KLIENT").getDictionaryData().put("id",new FieldData(Field.Type.STRING,putClient(l)));
        values.get("DWR_OSOBA_ODPOWIEDZIALNA").setStringData(putResponsiblePerson(putClient(l)));}
       }

        if(DwrUtils.isSenderField(values, "DWR_KLIENT")){
            if(!values.get("DWR_KLIENT").getDictionaryData().get("id").toString().equals("")){
                Long klient= Long.valueOf(String.valueOf(values.get("DWR_KLIENT").getDictionaryData().get("id")));
                values.get("DWR_OSOBA_ODPOWIEDZIALNA").setStringData(putResponsiblePerson(klient.toString()));

        }}

		if (values.get("DWR_ZAD_REALIZOWANE_W_WERYFIKACJI_EDITABLE") != null) {
			Map<String, FieldData> dict = values.get("DWR_ZAD_REALIZOWANE_W_WERYFIKACJI_EDITABLE").getDictionaryData();
			String contentMsg = "\"Termin do\" nie mo�e by� wcze�niejszy od \"Terminu od\"!!!";
			msg = validateFieldsInDict(values, dict, "ZAD_REALIZOWANE_W_WERYFIKACJI_EDITABLE_OD", "ZAD_REALIZOWANE_W_WERYFIKACJI_EDITABLE_DO", "DO_", "Date", contentMsg);
			if (msg != null) {
				return msg;
			}
		}
		if (values.get("DWR_OST_DECYZJE_DO_REALIZACJI_EDITABLE") != null) {
			Map<String, FieldData> dict = values.get("DWR_OST_DECYZJE_DO_REALIZACJI_EDITABLE").getDictionaryData();
			String contentMsg = "\"Termin do\" nie mo�e by� wcze�niejszy od \"Terminu od\"!!!";
			msg = validateFieldsInDict(values, dict, "OST_DECYZJE_DO_REALIZACJI_EDITABLE_OD", "OST_DECYZJE_DO_REALIZACJI_EDITABLE_DO", "DO_", "Date", contentMsg);
			if (msg != null) {
				return msg;
			}
		}
		return msg;
	}
	
	private Field validateFieldsInDict(Map<String, FieldData> values, Map<String, FieldData> dict, String smallerFieldCn,
										String biggerFieldCn, String wordKey, String typeField, String contentMsg) {
		if (dict != null) {
			for (String cn : dict.keySet()) {
				if (cn.contains(wordKey) && dict.get(cn).getData() != null) {
					String suffix = null;
					suffix = cn.substring(cn.lastIndexOf("_"));
					smallerFieldCn = smallerFieldCn.concat(suffix);
					biggerFieldCn = biggerFieldCn.concat(suffix);
					if (dict.containsKey(smallerFieldCn) && dict.containsKey(biggerFieldCn) && dict.get(smallerFieldCn).getData() != null && dict.get(biggerFieldCn).getData() != null) {
						if (typeField.equals("Date")) {
							Date smallerField = dict.get(smallerFieldCn).getDateData();
							Date biggerField = dict.get(biggerFieldCn).getDateData();
							if (biggerField.before(smallerField)) {
								Field msg = new Field("messageField", contentMsg, Field.Type.BOOLEAN);
								values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
								return msg;
							}
						} else if (typeField.equals("Integer")) {
							Integer smallerField = dict.get(smallerFieldCn).getIntegerData();
							Integer biggerField = dict.get(biggerFieldCn).getIntegerData();
							if (biggerField.compareTo(smallerField) < 0) {
								Field msg = new Field("messageField", contentMsg, Field.Type.BOOLEAN);
								values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
								return msg;
							}
						} else if (typeField.equals("BigDecimal")) {
							BigDecimal smallerField = dict.get(smallerFieldCn).getMoneyData();
							BigDecimal biggerField = dict.get(biggerFieldCn).getMoneyData();
							if (biggerField.compareTo(smallerField) < 0) {
								Field msg = new Field("messageField", contentMsg, Field.Type.BOOLEAN);
								values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
								return msg;
							}
						}
					}
				}
			}
		}
		return null;
	}
	
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values)
	{
		Map<String, Object> results = new HashMap<String, Object>();

		try
		{
			if (dictionaryName.equals("WYROB"))
			{
                DwrUtils.setRefValueEnumOptions(values, results, "RODZAJ", dictionaryName+"_", "TYP", "dsg_yetico_typ_niezgodnosci");
				// rodzaj
				/*Map<String, Object> fieldsValues = new HashMap<String, Object>();
				EnumValues rodzajIdEnum = values.get(dictionaryName + "_RODZAJ").getEnumValuesData();
				String rodzajId = rodzajIdEnum.getSelectedOptions().get(0);

				if (rodzajId != null && !rodzajId.equals(""))
				{
					fieldsValues.put("RODZAJ_1", rodzajId);
					for (DataBaseEnumField d : DataBaseEnumField.tableToField.get("dsg_yetico_typ_niezgodnosci"))
					{
						EnumValues val = d.getEnumItemsForDwr(fieldsValues);
						results.put(dictionaryName + "_TYP", val);
						break;
					}
					String typId = values.get(dictionaryName + "_TYP").getEnumValuesData().getSelectedOptions().get(0);
					if (typId != null && !typId.equals(""))
					{
						// wybrana pozycja budzetowa
						List<String> selectedPositionId = new ArrayList<String>();
						selectedPositionId.add(typId);
						((EnumValues) results.get(dictionaryName + "_TYP")).setSelectedOptions(selectedPositionId);
					}
				}
				else
				{
					EnumValues val = new EnumValues(Collections.EMPTY_MAP, Collections.EMPTY_LIST);
					results.put(dictionaryName + "_TYP", val);
				}*/
			}
			else if (dictionaryName.equals("ZAD_REALIZOWANE_W_WERYFIKACJI_EDITABLE") || dictionaryName.equals("OST_DECYZJE_DO_REALIZACJI_EDITABLE"))
			{
				Map<String, Object> fieldsValues = new HashMap<String, Object>();
				// status
				String statusId = values.get(dictionaryName + "_STATUS").getEnumValuesData().getSelectedOptions().get(0);

				if (statusId == null || statusId.equals(""))
				{
					for (DataBaseEnumField d : DataBaseEnumField.tableToField.get("dsg_yetico_zadanie_status"))
					{
						EnumValues val = d.getEnumItemsForDwr(fieldsValues);
						results.put(dictionaryName + "_STATUS", val);
						break;
					}
					// wybrana pozycja budzetowa
					List<String> selectedPositionId = new ArrayList<String>();
					statusId = "10";
					selectedPositionId.add(statusId);
					((EnumValues) results.get(dictionaryName + "_STATUS")).setSelectedOptions(selectedPositionId);
				}
			}
			else if (dictionaryName.equals("KOSZTY_REKLAMACJI"))
			{
				String zakladId = values.get(dictionaryName + "_ZAKLAD").getEnumValuesData().getSelectedOptions().get(0);
				String typKosztuId = values.get(dictionaryName + "_TYP_KOSZTU").getEnumValuesData().getSelectedOptions().get(0);
				if (zakladId != null && !zakladId.equals("") && typKosztuId != null && !typKosztuId.equals(""))
				{
					BigDecimal koszt = getKoszt(zakladId, typKosztuId);
					results.put(dictionaryName + "_KOSZT_JEDNOSTKOWY", koszt);
					BigDecimal ilosc = values.get(dictionaryName + "_ILOSC").getMoneyData();
					if (ilosc != null)
					{
						BigDecimal kosztCalkowity = koszt.multiply(ilosc).setScale(2, RoundingMode.HALF_UP);
						results.put(dictionaryName + "_KOSZT_CALKOWITY", kosztCalkowity);
					}
				}
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}

		return results;
	}

	private BigDecimal getKoszt(String zakladId, String typKosztuId) throws NumberFormatException, EdmException
	{

		String idStawki = typKosztuId+zakladId;
		String stawkaString = DataBaseEnumField.getEnumItemForTable("dsg_yetico_stawki_reklamacji", Integer.parseInt(idStawki)).getRefValue();

		return new BigDecimal(stawkaString).setScale(2, RoundingMode.HALF_UP);
	}

	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		try
		{
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			fm.initialize();
            List<Map<String,Object>> nr_faktury = FieldsManagerUtils.getDictionaryItems(fm, "WYROB", "WYROB_NR_FAKTURY");
            Map<String,Object> ik = nr_faktury.get(0);

            // czas wygenerowania metryki
            values.put("GENERATE_DATE", DateUtils.formatCommonDate(new java.util.Date()).toString());

            values.put("KLIENT", getKlientItems(fm));
            values.put("WYROB", getWyrobItems(fm));
            values.put("ALL_WYROB", getAllWyrobList(fm));
            values.put("ZAKLAD", getZaklad(fm));
            values.put("FAKTURA", (ik.get("WYROB_NR_FAKTURY")));
			// czas wygenerowania metryki

		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
	}
	
	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException {
		TaskListParams params = new TaskListParams();
		try {
			FieldsManager fm = dockind.getFieldsManager(id);
			String rodzaj = "";
			String typ = "";
			List<Wyrob> wyrobList = getAllWyrobList(fm);
			Map<Integer, String> rodzajMap = Niezgodnosc.rodzajNiezgodnosciList();
			Map<Integer, String> typMap = Niezgodnosc.typNiezgodnosciMap();
			for(int i=0; i<wyrobList.size(); i++) {
				if(i == 0) {
					rodzaj += rodzajMap.get(wyrobList.get(i).getRodzaj());
					typ += typMap.get(wyrobList.get(i).getTyp());
				}
				else {
					rodzaj += "," + rodzajMap.get(wyrobList.get(i).getRodzaj());
					typ += "," + typMap.get(wyrobList.get(i).getTyp());
				}
			}
			params.setAttribute(rodzaj, 0);
			params.setAttribute(typ, 1);
			params.setDocumentNumber((String)fm.getKey("NR_REKLAMACJI"));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return params;
	}
	
	private List<Wyrob> getAllWyrobList(FieldsManager fm) {
		List<Wyrob> wyrobList = new ArrayList<Wyrob>();
		try {
			LinkedList<Long> keys = (LinkedList<Long>) fm.getKey("WYROB");
			if(keys != null) {
				for(Long id : keys) {
					Wyrob wyrob = Wyrob.findById(id);
					String rodzaj = null;
					String typ = null;
					rodzaj = (String) DSApi.context().session().createSQLQuery("SELECT TITLE FROM dsg_yetico_rodzaje_niezgodnosci WHERE ID = :id")
						.setParameter("id", wyrob.getRodzaj())
						.uniqueResult();
					typ = (String) DSApi.context().session().createSQLQuery("SELECT TITLE FROM dsg_yetico_typ_niezgodnosci WHERE ID = :id")
							.setParameter("id", wyrob.getTyp())
							.uniqueResult();
					if (StringUtils.isNotBlank(rodzaj)) {
						wyrob.setRodzajStr(rodzaj);
					}
					if (StringUtils.isNotBlank(typ)) {
						wyrob.setTypStr(typ);
					}
					wyrob.setRodzajStr(rodzaj);
					wyrobList.add(wyrob);
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return wyrobList;
	}
	
	private ArrayList<WyrobItem> getWyrobItems(FieldsManager fm) {
		ArrayList<WyrobItem> ret = new ArrayList<WyrobItem>();
		try {
			int i = 0;
			@SuppressWarnings("unchecked")
			
			LinkedList<Long> wyrobList = (LinkedList<Long>) fm.getKey("WYROB");
			for (Long wyrobId : wyrobList)
			{
				Wyrob wyrob = Wyrob.findById(wyrobId);
				WyrobItem wi = new WyrobItem();
				wi.setLp(String.valueOf(++i));
				wi.setNazwa(wyrob.getNazwa());
				wi.setWymiary(wyrob.getWymiar());
				wi.setNrFaktury(wyrob.getNrFaktury());
				ret.add(wi);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return ret;
	}

	private ArrayList<KlientItem> getKlientItems(FieldsManager fm) {
		ArrayList<KlientItem> ret = new ArrayList<KlientItem>();
		try {
			@SuppressWarnings("unchecked")
			HashMap<String, Object> klient = (HashMap<String, Object>) fm.getValue("KLIENT");
				if (klient != null) {
					KlientItem ki = new KlientItem();
					if (klient.get("KLIENT_NAZWA") != null)
						ki.setNazwa((String) klient.get("KLIENT_NAZWA"));
					if (klient.get("KLIENT_MIEJSCOWOSC") != null)
						ki.setMiejscowosc((String) klient.get("KLIENT_MIEJSCOWOSC"));
					if (klient.get("KLIENT_KRAJ") != null)
						ki.setKraj(((EnumValues) klient.get("KLIENT_KRAJ")).getSelectedValue());
					if (klient.get("KLIENT_MAIL") != null)
						ki.setEmail((String) klient.get("KLIENT_MAIL"));
					ret.add(ki);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return ret;
	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue)
	{
		return false;
	}

	public void documentPermissions(Document document) throws EdmException 
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
		String documentKindName = document.getDocumentKind().getName();

		perms.add(new PermissionBean(ObjectPermission.READ, "YETICO_" + documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, "YETICO_ - " + documentKindName + " - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "YETICO_" + documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "PCz - " + documentKindName + " zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "YETICO_" + documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "YETICO_ - " + documentKindName + " - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "YETICO_" + documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "YETICO_ - " + documentKindName + " zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "YETICO_" + documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "YETICO_ - " + documentKindName + " - usuwanie"));
		
		DSUser user = DSApi.context().getDSUser();
		String fullName = user.getLastname()+ " " +user.getFirstname();
		
		perms.add(new PermissionBean(ObjectPermission.READ, user.getName(), ObjectPermission.USER, fullName+" ("+user.getName()+")"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user.getName(), ObjectPermission.USER, fullName+" ("+user.getName()+")"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, user.getName(), ObjectPermission.USER, fullName+" ("+user.getName()+")"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user.getName(), ObjectPermission.USER, fullName+" ("+user.getName()+")"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, user.getName(), ObjectPermission.USER, fullName+" ("+user.getName()+")"));
		
		Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
		perms.addAll(documentPermissions);
		this.setUpPermission(document, perms);
	}

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		String docKindTitle = document.getDocumentKind().getName();
		FieldsManager fm = document.getFieldsManager();
		EnumItem status = fm.getEnumItem("STATUS");
		if (status != null)
		{
			Folder folder = Folder.getRootFolder();

			folder = folder.createSubfolderIfNotPresent(docKindTitle);
			String rodzaj = null;
			if (status.getId().equals(40) || status.getId().equals(35))
			{
				rodzaj = "Niezasadne";
			}
			else
				rodzaj = "Zasadne";

			folder = folder.createSubfolderIfNotPresent(rodzaj);
			folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(document.getCtime()));
			folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(document.getCtime()));

			String docNumber;
			if (fm.getValue("NR_REKLAMACJI") != null)
			{
				docNumber = fm.getStringKey("NR_REKLAMACJI");
			}
			else
			{
				docNumber = "";
			}

			document.setTitle(docKindTitle + ": " + docNumber);
			document.setDescription(docKindTitle + ": " + docNumber);
			((OfficeDocument) document).setSummaryWithoutHistory((docKindTitle + ": " + docNumber));
			document.setFolder(folder);
			
			moveValuesToAnotherDictionary(document, "ZAD_REALIZOWANE_W_WERYFIKACJI_EDITABLE", "ZAD_REALIZOWANE_W_WERYFIKACJI");
			if (status.getCn().equals(DYR_BZ_DECYZJA_RODZAJ_REKOMPENSATY) || status.getCn().equals(DYR_BZ_DECYZJA_RODZAJ_REKOMPENSATY_2)) {
				moveValuesToAnotherDictionary(document, "OST_DECYZJE_DO_REALIZACJI_EDITABLE", "OST_DECYZJE_DO_REALIZACJI");

                List<Map<String, Object>> decyzje = (List<Map<String, Object>>) document.getFieldsManager().getValue("OST_DECYZJE_DO_REALIZACJI");
                if (decyzje != null)
                {
                for (Map<String, Object> decyzja : decyzje)
                {
                    String id = (String) decyzja.get("id");
                    String statusId = ((EnumValues)decyzja.get("OST_DECYZJE_DO_REALIZACJI_STATUS")).getSelectedId();
                    if ("10".equals(statusId))
                    {
                        DocumentKind documentKind = DocumentKind.findByCn("zadanie");
                        OfficeDocument zadanie = OfficeDocument.find(Long.parseLong(id));
                        documentKind.logic().onStartProcess(zadanie, null);
                        TaskSnapshot.updateByDocument(document);
                    }




                }
                }
                //documentKind.logic().onStartProcess(document, null);

                //DocumentKind documentKind = DocumentKind.findByCn("zadanie");

                //TaskSnapshot.updateByDocument(document);

            }

            if (status.getId().equals(35) ){

                List<Long> idx = idNiezgodnosci(document.getId());
                List<Long> koszt = idRezMulti(document.getId());
                   for(Long koszt2:koszt){
                   putNiezgodMulti( idx.get(0), koszt2);
                    }
            }


       /*      Date data=  (Date)fm.getValue("DATA_REJESTRACJI");
                long now = System.currentTimeMillis();
                Date data2 = new Date(now);
             int days = Days.daysBetween(new DateTime(data).toLocalDate(), new DateTime(data2).toLocalDate()).getDays();
                putendtime(document.getId(), days+1);


			*/
			// powi�zanie zadania z reklamacj�
			setReklamacjaId(document, fm, ZadanieDictionary.ZAD_REALIZOWANE_W_WERYFIKACJI);
            setReklamacjaId(document, fm, "ZAD_REALIZOWANE_W_WERYFIKACJI_EDITABLE");
            setReklamacjaId(document, fm, "OST_DECYZJE_DO_REALIZACJI_EDITABLE");
			setReklamacjaId(document, fm, ZadanieDictionary.OST_DECYZJE_DO_REALIZACJI);
		}
	}

	private void setReklamacjaId(Document document, FieldsManager fm, String dictionaryCn) throws EdmException, DocumentNotFoundException, DocumentLockedException, AccessDeniedException
	{
		List<Long> zadania = (List<Long>)fm.getKey(dictionaryCn);
		if (zadania != null)
			for (Long zadanieId : zadania)
			{
				OfficeDocument zadanie = (OfficeDocument) Document.find(zadanieId, false);
				Long document_id = zadanie.getFieldsManager().getLongKey("REKLAMACJA");
				if (document_id == null)
				{
					Map<String, Object> fieldValues = new HashMap<String, Object>();
					fieldValues.put("REKLAMACJA", document.getId());
					zadanie.getDocumentKind().setOnly(zadanieId, fieldValues);
				}
			}
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));

			Map<String, Object> fieldValues = new HashMap<String, Object>();
			fieldValues.put("NR_REKLAMACJI", generateNrReklamacji(document));
			document.getDocumentKind().setOnly(document.getId(), fieldValues);
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

			java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

			DSUser author = DSApi.context().getDSUser();
			String user = author.getName();
			String fullName = author.getLastname() + " " + author.getFirstname();

			perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

			Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
			perms.addAll(documentPermissions);
			this.setUpPermission(document, perms);
			
			DSApi.context().watch(URN.create(document));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

	/*
	 * Automatyczna numeracja rejestrowanych reklamacji wg wzoru: A/B/C/D/E/F A
	 * - Kod kraju (wynikaj�cy z kraju Klienta) B - Symbol rodzaju niezgodno�ci
	 * (J-jako�ciowe, M-magazynowe, L-logistyka, I-ilo�ciowe, H-handlowe, IN -
	 * inne) C - Kolejny numer (licznik) reklamacji w danym miesi�cu w ramach
	 * ca�ej firmy. D - data rejestracji (dzie�-miesi�c-rok) E - Kod zak�adu
	 * (GAL, GW, OL)
	 */
	private Object generateNrReklamacji(OfficeDocument document) throws EdmException
	{
		String nrReklamacji = "-";
		try
		{
			FieldsManager fm = document.getFieldsManager();

			Long klientId = (Long) fm.getKey("KLIENT");
			Klient klient = Klient.find(klientId);
			String a = DataBaseEnumField.getEnumItemForTable("dsr_country", klient.getKraj()).getCn();
			List<Long> wyrobIds = (List<Long>) fm.getKey("WYROB");
			String b = "";
			for (Long wyrobId : wyrobIds)
			{
				if (b.length() > 0)
					b += ",";
				Wyrob wyrob = Wyrob.findById(wyrobId);
				Integer rodzajId = wyrob.getRodzaj();
                		if (rodzajId != null) {
                		    String rodzaj = DataBaseEnumField.getEnumItemForTable("dsg_yetico_rodzaje_niezgodnosci", rodzajId).getCn().substring(0, 1);
                		    b = b + rodzaj;
                		}
			}
			String c = "";
			try
			{
				PreparedStatement ps = DSApi.context().prepareStatement("select count(*) from dsg_yetico_rek_zew rek_zew join ds_document doc on rek_zew.document_id =" + "doc.ID where MONTH(CTIME) = ?");
				Calendar cal = Calendar.getInstance();
				Integer calMonth = cal.get(Calendar.MONTH) + 1;
				ps.setInt(1, calMonth);
				ResultSet rs = ps.executeQuery();
				boolean hasNext = rs.next();
				c = hasNext ? String.valueOf(rs.getInt(1)) : "1";
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
				throw new EdmException(e.getMessage());
			}
			String d = DateUtils.commonDateFormat.format(new Date());
			String e = getZaklad(fm);
			nrReklamacji = a + "/" + b + "/" + c + "/" + d + "/" + e;
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
		return nrReklamacji;
	}
	
	private String getZaklad(FieldsManager fm) {
		String s = "";
		try {
			List<String> ids = (List<String>)fm.getValue("DOT_ZAKLADU");
			if (ids != null) {
				for (int i=0; i<ids.size(); i++) {
					if (i == 0) {
						s += ids.get(i);
					}
					else {
						s += "," + ids.get(i);
					}
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return s;
	}
	
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		Map<String, Object> toReload = Maps.newHashMap();
		for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
		{
			if (f.getDefaultValue() != null)
			{
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
			}
		}
		log.info("toReload = {}", toReload);

		toReload.put("DATA_ZGLOSZENIA", new Date());
		toReload.put("DATA_REJESTRACJI", new Date());
		fm.reloadValues(toReload);
	}
	
	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		log.info("Before Search - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.contains("WYROB"))
		{
		    if (values.get("id") != null && !values.get("id").equals(""))
			return;


            if(dockindFields.get("KLIENT")!=null){
              Number klient= Long.valueOf(String.valueOf(dockindFields.get("KLIENT")));
             values.get("WYROB_KLIENT_ID").setStringData(String.valueOf(klient));

            }


			values.put(dictionaryName + "_BASE", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.INTEGER, 1));
		}
	}

	private void moveValuesToAnotherDictionary(Document document, String fromFieldCn, String toFieldCn) throws EdmException {
		FieldsManager fm = document.getFieldsManager();
		Map<String, Object> fieldValues = new HashMap<String, Object>();
		if ((fromFieldCn != null) && (toFieldCn != null) && (fm.getKey(fromFieldCn) != null)) {
			if (fm.getKey(toFieldCn) == null) {
				fieldValues.put(toFieldCn, fm.getKey(fromFieldCn));
			} else if (fm.getKey(toFieldCn) instanceof List) {
				List toFieldVals = (List)fm.getKey(toFieldCn);
				if (fm.getKey(fromFieldCn) instanceof List) {
					toFieldVals.addAll((List)fm.getKey(fromFieldCn));
				} else {
					toFieldVals.add(fm.getKey(fromFieldCn));
				}
				fieldValues.put(toFieldCn, toFieldVals);
			}
			fieldValues.put(fromFieldCn, null);
			document.getDocumentKind().setOnly(document.getId(), fieldValues);
		}
	}

    private List<Long> idNiezgodnosci(Long id) throws EdmException {
        List<Long> output= new ArrayList();
        PreparedStatement ps = null;
        try {
            String sql = "SELECT Document_id FROM dsg_yetico_niezgodnosc_wew WHERE uwagi_link=?";
            ps = DSApi.context().prepareStatement(sql);

            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                output.add(rs.getLong(1));
            }

        }  catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(ps);
        }
        return   output;
    }


    private List<Long> idRezMulti(Long id) throws EdmException {
        List<Long> output= new ArrayList();
        PreparedStatement ps = null;
        try {
            String sql = "SELECT FIELD_VAL FROM dsg_yetico_rek_zew_multiple_value WHERE FIELD_CN='KOSZTY_REKLAMACJI'AND DOCUMENT_ID=?";
            ps = DSApi.context().prepareStatement(sql);

            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                output.add(rs.getLong(1));
            }

        }  catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(ps);
        }
        return   output;
    }

    private void putNiezgodMulti(Long id, Long koszt) throws EdmException {

        PreparedStatement ps = null;
        try {
            String sql = "INSERT INTO dsg_yetico_niezgodnosc_wew_multiple_value values (?,'KOSZTY_REKLAMACJI',?)";
            ps = DSApi.context().prepareStatement(sql);
            ps.setLong(1, id);
            ps.setLong(2, koszt);
            ps.executeQuery();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(ps);
        }
    }
    private String putResponsiblePerson(String id) throws EdmException {
        String code="";
        PreparedStatement ps = null;
        DSContextOpener opener = null;
        try {
            opener = DSContextOpener.openAsAdminIfNeed();
            String sql = "SELECT cn from dsg_yetico_opiekunowie where refValue=?";
            ps = DSApi.context().prepareStatement(sql);
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            if (!rs.next()) {
                return "";
            }
            code = rs.getString(1);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(ps);
            DSContextOpener.closeIfNeed(opener);
        }

        return code;
    }
    private String putClient(String id) throws EdmException {
        String code="";
        PreparedStatement ps = null;
        DSContextOpener opener = null;
        try {
            opener = DSContextOpener.openAsAdminIfNeed();
            String sql = "SELECT Klient_id from dsg_yetico_wyrob where ID=?";
            ps = DSApi.context().prepareStatement(sql);
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            if (!rs.next()) {
                return "";
            }
            code = rs.getString(1);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(ps);
            DSContextOpener.closeIfNeed(opener);
        }

        return code;
    }
    private void putendtime(Long id, Integer days) throws EdmException {

        PreparedStatement ps = null;
        try {
            String sql = "UPDATE  dsg_yetico_rek_zew set data_zakonczenia=? where document_id=?";
            ps = DSApi.context().prepareStatement(sql);
            ps.setInt(1, days);
            ps.setLong(2, id);
            ps.executeQuery();

        }  catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(ps);
        }

    }

}