/**
 * 
 */
package pl.compan.docusafe.parametrization.yetico;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.criterion.Restrictions;
import org.jbpm.api.TaskService;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import edu.emory.mathcs.backport.java.util.Collections;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.yetico.hb.PrewencjaItem;
import pl.compan.docusafe.parametrization.yetico.jbpm.PreventionTaskDateVerification;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class ZadaniePrewencyjneLogic extends AbstractDocumentLogic {
	
	private static final Logger logger = LoggerFactory.getLogger(ZadaniePrewencyjneLogic.class);
	public static final String LIDER = "LIDER";
	public static final String KIEROWNIK_UTRZYMANIA = "kierownik_utrzymania";
	public static final String END = "END";
	public static final String OSOBA_ZLECAJACA = "OSOBA_ZLECAJACA";
	public static final String STATUS = "STATUS";
	public static final Integer OCZEKIWANIE_NA_REALIZACJE = 5;
	public static final String ACCEPTED_VAR_NAME = "accepted";
	
	@Override
	public void acceptTask(Document document, ActionEvent event, String activity) {
		try {	
			FieldsManager fm = document.getFieldsManager();
			Integer status = (Integer)fm.getKey(STATUS);
			if (status.equals(OCZEKIWANIE_NA_REALIZACJE)) {
				String activityId = getActivityId(document);
				if (activityId != null) {
					TaskService ts = Jbpm4Provider.getInstance().getTaskService();
					ts.setVariables(activityId, Collections.singletonMap(ACCEPTED_VAR_NAME, Boolean.TRUE));
				}
			}
		} catch (EdmException e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	public void documentPermissions(Document document) throws EdmException {

	}

	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {

	}
	
	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			if (event != null) {
				map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
				map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			}

			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

			Set<PermissionBean> perms = new HashSet<PermissionBean>();

			DSUser author = DSApi.context().getDSUser();
			String user = author.getName();
			String fullName = author.getLastname() + " " + author.getFirstname();

			perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

			Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
			perms.addAll(documentPermissions);
			this.setUpPermission(document, perms);
			
			DSApi.context().watch(URN.create(document));
		}
		catch (Exception e)
		{
			logger.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}
	
	@Override
	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
		try {
			if ("LIDER".equalsIgnoreCase(acceptationCn)) {
				FieldsManager fm = doc.getFieldsManager();
				Long id = (Long)fm.getKey(ListaPrewencyjnaLogic.PREWENCJA);
				PrewencjaItem item = (PrewencjaItem)DSApi.context().session().get(PrewencjaItem.class, id);
				if (item != null) {
					item.setStatus(103);
					DSApi.context().session().update(item);
					DSApi.context().session().flush();
					Long userId = Long.valueOf(item.getImieNazwisko());
					DSUser user = DSUser.findById(userId);
					assignable.addCandidateUser(user.getName());
					AssigneeHandler.addToHistory(openExecution, doc, user.getName(), null);
					return true;
				}
				return false;
				
			} else if (KIEROWNIK_UTRZYMANIA.equalsIgnoreCase(acceptationCn)) {
				boolean setStatus = false;
				FieldsManager fm = doc.getFieldsManager();
				Long id = (Long)fm.getKey(ListaPrewencyjnaLogic.PREWENCJA);
				PrewencjaItem item = (PrewencjaItem)DSApi.context().session().get(PrewencjaItem.class, id);
				if (item != null) {
					item.setStatus(102);
					DSApi.context().session().update(item);
					DSApi.context().session().flush();
					setStatus = true;
				}
				
				String[] assigness = acceptationCn.split(",");
				logger.debug("assignee operations for acceptationCn: {}, fieldCnValue: {}", acceptationCn, fieldCnValue);
				boolean assigned = false;
				String fieldValue = null;
				for (String ass : Arrays.asList(assigness)) {
					List<String> users = new ArrayList<String>();
					List<String> divisions = new ArrayList<String>();
					if (fieldCnValue != null) {
						fieldValue = doc.getFieldsManager().getEnumItem(fieldCnValue).getCn();
					}

					for (AcceptanceCondition accept : AcceptanceCondition.find(ass, fieldValue)) {
						if (accept.getUsername() != null && !users.contains(accept.getUsername()))
							users.add(accept.getUsername());
						if (accept.getDivisionGuid() != null && !divisions.contains(accept.getDivisionGuid()))
							divisions.add(accept.getDivisionGuid());
					}
					for (String user : users) {
						assignable.addCandidateUser(user);
						logger.info("For accept " + ass + " and fieldValue " + fieldValue + " - candidateUser: " + user);
						AssigneeHandler.addToHistory(openExecution, doc, user, null);
						assigned = true;
					}
					for (String division : divisions) {
						assignable.addCandidateGroup(division);
						logger.info("For accept " + ass + " and fieldValue " + fieldValue + " - candidateGroup: " + division);
						AssigneeHandler.addToHistory(openExecution, doc, null, division);
						assigned = true;
					}
				}
				if (assigned && setStatus) {
					openExecution.setVariable(ACCEPTED_VAR_NAME, Boolean.FALSE);
					String term = openExecution.getVariable(PreventionTaskDateVerification.TERM) + "";
					if (PreventionTaskDateVerification.BEFORE_TERM.equals(term)) {
						openExecution.setVariable(PreventionTaskDateVerification.TERM, null);
					}
					return true;
				} else {
					throw new Exception("Error: Not Assigned for " + acceptationCn);
				}
				
			} else if (END.equalsIgnoreCase(acceptationCn)) {
				FieldsManager fm = doc.getFieldsManager();
				Long id = (Long)fm.getKey(ListaPrewencyjnaLogic.PREWENCJA);
				PrewencjaItem item = (PrewencjaItem)DSApi.context().session().get(PrewencjaItem.class, id);
				if (item != null) {
					item.setStatus(101);
					DSApi.context().session().update(item);
					DSApi.context().session().flush();
					return true;
				}
				return false;
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return false;
	}
	
	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values) {
		values.put("GENERATE_DATE", DateUtils.formatCommonDate(new java.util.Date()).toString());
		
		List<PrewencjaItem> preventions = getPreventionTasks(docId);
		if (preventions != null && preventions.size() > 0 && !preventions.contains(null)) {
				values.put("PREVENTIONS", preventions);
		}
	}
	
	private String getActivityId(Document document) throws EdmException {
    	String activityId = null;
    	Iterable<String> taskIds = Jbpm4ProcessLocator.taskIds(document.getId());
    	for (String taskId : taskIds)
    		activityId = taskId;
		activityId = cleanActivityId(activityId);
		return activityId;
	}
	
    private String cleanActivityId(String activityId){
        return activityId.replaceAll("[^,]*,","");
    }
    
	private List<PrewencjaItem> getPreventionTasks(long docId) {
		List<PrewencjaItem> preventions = null;
		try {
			DocumentKind docKind = OfficeDocument.find(docId).getDocumentKind();
			List<BigDecimal> docIds = DSApi.context().session().createSQLQuery("SELECT document_id FROM DSW_JBPM_TASKLIST " +
					"WHERE assigned_resource = (SELECT assigned_resource FROM DSW_JBPM_TASKLIST WHERE document_id = :docId) " +
					"AND dockind_name = :dockind")
					.setParameter("docId", docId)
					.setParameter("dockind", docKind.getName())
					.list();
			if (docIds != null && docIds.size() > 0) {
				LinkedHashSet<Long> preventionIdsList = Sets.newLinkedHashSet();
				for (BigDecimal id : docIds) {
					if (id != null) {
						FieldsManager fm = OfficeDocument.find(id.longValue()).getFieldsManager();
						Long preventionId = (Long)fm.getKey(ListaPrewencyjnaLogic.PREWENCJA);
						if (preventionId != null) {
							preventionIdsList.add(preventionId);
						}
					}
				}
				preventions = DSApi.context().session().createCriteria(PrewencjaItem.class)
						.add(Restrictions.in("id", preventionIdsList))
						.list();
			}
		} catch (EdmException e) {
			logger.error(e.getMessage(), e);
		}
		return preventions;
	}
	
}
