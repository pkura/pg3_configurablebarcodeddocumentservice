package pl.compan.docusafe.parametrization.yetico;

public class TypNiezgodnosci {
	
	private Long id;
	private String title;
	private String refValue;
	
	public TypNiezgodnosci() {}
	
	public TypNiezgodnosci(Long id, String title, String refValue) {
		this.id = id;
		this.title = title;
		this.refValue = refValue;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRefValue() {
		return refValue;
	}

	public void setRefValue(String refValue) {
		this.refValue = refValue;
	}
}
