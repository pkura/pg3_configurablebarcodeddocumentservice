/**
 * 
 */
package pl.compan.docusafe.parametrization.yetico;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Constants;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.yetico.hb.PrewencjaItem;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class WykonaniePrewencjiPrzegladowService extends ServiceDriver
		implements Service {
	
	private static final StringManager sm = StringManager.getManager(Constants.Package);
	private static final Logger logger = LoggerFactory.getLogger(WykonaniePrewencjiPrzegladowService.class);
	
	public static final String ZLECENIE_WYK_PREW = "zlecenie_wyk_prew";
	public static final Integer WEEK = 7;
	public static final Integer TWO_WEEKS = 14;
	public static final Integer THREE_WEEKS = 21;
	public static final Integer FOUR_WEEKS = 28;
	private Property[] properties;
	private Timer timer;
	private Date startDate;
	private Integer cycle;
	private Boolean equalsDate;
	
	public WykonaniePrewencjiPrzegladowService() {
		properties = new Property[] {new StartDateProperty(), new CycleProperty()};
		Calendar calendar = Calendar.getInstance();
		calendar = setMidnightInCalendar(calendar);
		calendar.add(Calendar.DATE, 1);
		startDate = calendar.getTime();
		cycle = 5;
	}
	
	protected void start() throws ServiceException {
		timer = new Timer(true);
		long period = getPeriod();
		//timer.schedule(new DocumentsGenerator(), startDate, period);
		timer.schedule(new DocumentsGenerator(), 1000, 1 * DateUtils.MINUTE);
		console(Console.INFO, sm.getString("WykonaniePrewencjiPrzegladowServiceStart"));
	}

	protected void stop() throws ServiceException {
		if (timer != null) {
			timer.cancel();
		}
		console(Console.INFO, sm.getString("WykonaniePrewencjiPrzegladowServiceStop"));
	}
	
	protected boolean canStop() {
		return true;
	}
	
	@Override
	public Property[] getProperties() 
	{
		return properties;
	}
	
	private Calendar setMidnightInCalendar(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.clear(Calendar.MINUTE);
		calendar.clear(Calendar.SECOND);
		calendar.clear(Calendar.MILLISECOND);
		return calendar;
	}
	
	private long getPeriod() {
		long period;
		if (cycle.equals(5)) {
			period = DateUtils.WEEK; 
		} else if (cycle.equals(10)) {
			period = 2 * DateUtils.WEEK;
		} else if (cycle.equals(15)) {
			period = 3 * DateUtils.WEEK;
		} else {
			period = 4 * DateUtils.WEEK;
		}
		return period;
	}
	
	private Integer getDaysInCycle() {
		Integer days;
		if (cycle.equals(5)) {
			days = WEEK;
		} else if (cycle.equals(10)) {
			days = TWO_WEEKS;
		} else if (cycle.equals(15)) {
			days = THREE_WEEKS;
		} else {
			days = FOUR_WEEKS;
		}
		return days;
	}
	
	private Integer getCycleNumber() {
		return DSApi.context().systemPreferences().node("wykonaniePrewencjiPrzegladowService").getInt("cycleNumber", 0);
	}
	
	private Date getCorrectDate(Integer cycleNumber) {
		Integer days = getDaysInCycle();
		return DateUtils.plusDays(startDate, days * cycleNumber);
	}
	
	class DocumentsGenerator extends TimerTask {

		public void run() {
			boolean contextOpened = false;
			try {
				contextOpened = DSApi.openContextIfNeeded();
				List<FieldsManager> fmList = findPlan();
				if (fmList != null && !fmList.isEmpty()) {
					List<Long> preventionLists = getPreventionLists(fmList);
					if (preventionLists != null && preventionLists.size() != 0) {
						List<Long> currentPreventions = findCurrentPreventions(preventionLists);
						Calendar calendar = Calendar.getInstance();
						calendar = setMidnightInCalendar(calendar);
						String actualDate = DateUtils.formatJsDate(calendar.getTime());
						calendar.add(Calendar.DATE, getDaysInCycle());
						String finishCycleDate = DateUtils.formatJsDate(calendar.getTime());
						if (currentPreventions != null && currentPreventions.size() != 0) {
							Boolean wasGenerated = generateNewDockind(currentPreventions);
							if (wasGenerated) {
								console(Console.INFO, sm.getString("WykonaniePrewencjiPrzegladowServiceGenerateDocument", actualDate, finishCycleDate));
							} else {
								console(Console.INFO, sm.getString("WykonaniePrewencjiPrzegladowServiceGenerateDocumentError"));
							}
						} else {
							console(Console.INFO, sm.getString("WykonaniePrewencjiPrzegladowServiceNoPreventions", actualDate, finishCycleDate));
						}
					} else {
						console(Console.INFO, sm.getString("WykonaniePrewencjiPrzegladowServiceNoPreventionLists"));
					}
				} else {
					console(Console.INFO, sm.getString("WykonaniePrewencjiPrzegladowServiceNoPreventionPlan"));
				}
				Integer cycleNumber = getCycleNumber();
				cycleNumber++;
				DSApi.beginTransacionSafely();
				DSApi.context().systemPreferences().node("wykonaniePrewencjiPrzegladowService").putInt("cycleNumber", cycleNumber);
				DSApi.context().commit();
				console(Console.INFO, sm.getString("WykonaniePrewencjiPrzegladowServiceNewTerm") + DateUtils.formatJsDate(getCorrectDate(cycleNumber)));
			} catch (EdmException e) {
				logger.error(e.getMessage(), e);
			} finally {
				DSApi.closeContextIfNeeded(contextOpened);
			}
		}
		
		private List<FieldsManager> findPlan() throws EdmException {
			List<FieldsManager> results = new ArrayList<FieldsManager>();
			List<Long> docIdList = Document.findByDocumentKind(PlanPrewencyjnyLogic.PLAN_PREWENCYJNY);
			if (docIdList != null && docIdList.size() != 0) {
				for (Long id : docIdList) {
					if (id != null && id != 0) {
						FieldsManager fm = Document.find(id).getFieldsManager();
						Integer yearFromPlan = (Integer)fm.getKey(PlanPrewencyjnyLogic.ROK);
						if (yearFromPlan != null) {
							Integer actualYear = Calendar.getInstance().get(Calendar.YEAR);
							if (yearFromPlan.equals(actualYear)) {				//&& fm.getKey("STATUS") != null && fm.getKey("STATUS") == 30
								results.add(fm);
							}
						}
					}
				}
			}
			return results;
		}
		
		private List<Long> getPreventionLists(List<FieldsManager> fmList) throws EdmException {
			List<Long> ids = new ArrayList<Long>();
			for (FieldsManager fm : fmList) {
				ids.addAll((List<Long>)fm.getKey(PlanPrewencyjnyLogic.LISTA_PREWENCYJNA_DICT));
			}
			return ids;
		}
		
		private List<Long> findCurrentPreventions(List<Long> preventionLists) throws EdmException {
			List<Long> currentPreventions = new ArrayList<Long>();
			for (Long id : preventionLists) {
				if (id != null && id != 0) {
					FieldsManager fm = Document.find(id).getFieldsManager();
					List<Map<String, Object>> preventionsValues = (List<Map<String, Object>>)fm.getValue(ListaPrewencyjnaLogic.PREWENCJA);
					for (Map<String, Object> prevention : preventionsValues) {
						if (prevention != null && prevention.containsKey(ListaPrewencyjnaLogic.PREWENCJA_PLANOWANA_DATA_REALIZACJI) &&
								prevention.get(ListaPrewencyjnaLogic.PREWENCJA_PLANOWANA_DATA_REALIZACJI) != null) {
							Date plannedDate = (Date)prevention.get(ListaPrewencyjnaLogic.PREWENCJA_PLANOWANA_DATA_REALIZACJI);
							if (isDateInCycle(plannedDate) && prevention.containsKey(ListaPrewencyjnaLogic.PREWENCJA_ID) &&
									prevention.get(ListaPrewencyjnaLogic.PREWENCJA_ID) != null &&
                                    ((Integer)prevention.get(ListaPrewencyjnaLogic.PREWENCJA_STATUS)) == 0) {
								currentPreventions.add((Long)prevention.get(ListaPrewencyjnaLogic.PREWENCJA_ID));
							}
						}
					}
				}
			}
			return currentPreventions;
		}
		
		private Boolean isDateInCycle(Date plannedDate) {
			Calendar calendar = Calendar.getInstance();
			calendar = setMidnightInCalendar(calendar);
			Date actualDate = calendar.getTime();
			calendar.add(Calendar.DATE, getDaysInCycle());
			Date finishCycleDate = calendar.getTime();
            boolean result = (!plannedDate.before(actualDate) && plannedDate.before(finishCycleDate));
			return result;
		}
		
		private Boolean generateNewDockind(List<Long> currentPreventions) throws EdmException {
			Boolean wasGenerated = Boolean.FALSE;
			OutOfficeDocument newDocument = new OutOfficeDocument();
			try {
				DSUser author = DSApi.context().getDSUser();
				String authorName = author.getName();
				newDocument.setCreatingUser(authorName);
				newDocument.setSummary("Zlecenie wykonania prewencji wygenerowane przez serwis");
				DocumentKind docKind = DocumentKind.findByCn(ZLECENIE_WYK_PREW);
				newDocument.setDocumentKind(docKind);
				newDocument.create();
				newDocument.setInternal(true);
				
				Set<PermissionBean> perms = new HashSet<PermissionBean>();
				String authorFullName = author.getLastname() + " " + author.getFirstname();
				perms.add(new PermissionBean(ObjectPermission.READ, authorName, ObjectPermission.USER, authorFullName + " (" + authorName + ")"));
				perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, authorName, ObjectPermission.USER, authorFullName + " (" + authorName + ")"));
				perms.add(new PermissionBean(ObjectPermission.MODIFY, authorName, ObjectPermission.USER, authorFullName + " (" + authorName + ")"));
				perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, authorName, ObjectPermission.USER, authorFullName + " (" + authorName + ")"));
				perms.add(new PermissionBean(ObjectPermission.DELETE, authorName, ObjectPermission.USER, authorFullName + " (" + authorName + ")"));
				Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(newDocument);
				perms.addAll(documentPermissions);
				((AbstractDocumentLogic) newDocument.getDocumentKind().logic()).setUpPermission(newDocument, perms);
                for (Long prevId : currentPreventions)
                {
                    PrewencjaItem prevItem = Finder.find(PrewencjaItem.class, prevId);
                    prevItem.setStatus(100);
                    Persister.update(prevItem);
                }
                Map<String, Object> valuesToSet = new HashMap<String, Object>();
				valuesToSet.put(ListaPrewencyjnaLogic.PREWENCJA, currentPreventions);
				docKind.setOnly(newDocument.getId(), valuesToSet);
				
				DSApi.context().session().save(newDocument);
				newDocument.getDocumentKind().logic().onStartProcess(newDocument, null);
				DSApi.context().begin();
				TaskSnapshot.updateByDocument(newDocument);
				DSApi.context().commit();
				wasGenerated = Boolean.TRUE;
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			return wasGenerated;
		}
		
	}
	
	private class StartDateProperty extends Property {
		
		public StartDateProperty() {
			super(SIMPLE, PERSISTENT, WykonaniePrewencjiPrzegladowService.this, "startDate", "Data rozpocz�cia", String.class, DATE);
		}
		
		@Override
		protected Object getValueSpi() {
			if (startDate != null) {
				return DateUtils.formatJsDate(startDate);
			} else {
				return null;
			}
		}
		
		@Override
		protected void setValueSpi(Object object) throws ServiceException {
			synchronized (WykonaniePrewencjiPrzegladowService.this) {
				try {
					if (object != null) {
						equalsDate = Boolean.FALSE;
						Integer cycleNumber = getCycleNumber();
						Date dateFromUser = DateUtils.parseDateAnyFormat((String) object);
						if (checkCanChangeDate(dateFromUser, cycleNumber)) {
							String dateTemp = DSApi.context().systemPreferences().node("services").node("WykonaniePrewencjiPrzegladowService").node("default").get("startDate", "");
							Date dateFromDatabase = DateUtils.parseDateAnyFormat(dateTemp);
							if (dateFromUser.equals(dateFromDatabase)) {
								equalsDate = Boolean.TRUE;
							}
							startDate = dateFromUser;
							console(Console.INFO, sm.getString("WykonaniePrewencjiPrzegladowServiceSetDate") +  DateUtils.formatJsDate(startDate));
						} else {
							StringBuilder msg = new StringBuilder();
							msg.append(sm.getString("WykonaniePrewencjiPrzegladowServiceUncorrectDate"));
							if (cycleNumber.equals(0)) {
								Date correctDate = DateUtils.plusDays(new Date(), 1);
								msg.append(DateUtils.formatJsDate(correctDate));
							} else {
								msg.append(DateUtils.formatJsDate(getCorrectDate(cycleNumber)));
							}
							console(Console.WARN, msg.toString());
							throw new ServiceException(msg.toString());
						}	
					} else {
						console(Console.WARN, sm.getString("WykonaniePrewencjiPrzegladowServiceNoDate"));
					}
				} catch (ParseException e) {
					logger.error(e.getMessage(), e);
					console(Console.WARN, sm.getString("WykonaniePrewencjiPrzegladowServiceValidationDateError"));
				}
			}
		}
		
		private Boolean checkCanChangeDate(Date dateFromUser, Integer cycleNumber) {
			if (cycleNumber.equals(0)) {
				Calendar calendar = Calendar.getInstance();
				Date actualDate = setMidnightInCalendar(calendar).getTime();
				return (actualDate.before(startDate) && dateFromUser.after(actualDate));
			} else {
				Date correctDate = getCorrectDate(cycleNumber);
				return !dateFromUser.before(correctDate);
			}
		}
		
	}
	
	private class CycleProperty extends Property {
		
		private Map<Integer, String> valueConstraints = new LinkedHashMap<Integer, String>();
		
		public CycleProperty() {
			super(ENUMERATED, PERSISTENT, WykonaniePrewencjiPrzegladowService.this, "cycle", "Cykl", Integer.class);
			valueConstraints.put(5, "1 tydzie�");
			valueConstraints.put(10, "2 tygodnie");
			valueConstraints.put(15, "3 tygodnie");
			valueConstraints.put(20, "4 tygodnie");
		}
		
		@Override
		public Map getValueConstraints() throws ServiceException {
			return valueConstraints;
		}
		
		@Override
		protected Object getValueSpi() {
			return cycle;
		}
		
		@Override
		protected void setValueSpi(Object object) throws ServiceException {
			synchronized (WykonaniePrewencjiPrzegladowService.this) {
				try {
					if (object != null) {
						Integer cycleFromDatabase = DSApi.context().systemPreferences().node("services").node("WykonaniePrewencjiPrzegladowService").node("default").getInt("cycle", 0);
						if (!cycleFromDatabase.equals((Integer)object) && !equalsDate) {
							DSApi.beginTransacionSafely();
							DSApi.context().systemPreferences().node("wykonaniePrewencjiPrzegladowService").putInt("cycleNumber", 0);
							DSApi.context().session().flush();
							if (timer != null) {
								timer.cancel();
							}
							//long period = getPeriod();
							//timer.schedule(new DocumentsGenerator(), startDate, period);
						} 
						cycle = (Integer) object;
						console(Console.INFO, sm.getString("WykonaniePrewencjiPrzegladowServiceSetCycle") + valueConstraints.get(cycle));
						Integer cycleNumber = getCycleNumber();
						console(Console.INFO, sm.getString("WykonaniePrewencjiPrzegladowServiceNewTerm") + DateUtils.formatJsDate(getCorrectDate(cycleNumber)));
					} else {
						console(Console.WARN, sm.getString("WykonaniePrewencjiPrzegladowServiceNoCycle"));
					}
				} catch (EdmException e) {
					logger.error(e.getMessage(), e);
					console(Console.WARN, sm.getString("WykonaniePrewencjiPrzegladowServiceSetCycleError"));
				}
			}
		}
		
	}

}
