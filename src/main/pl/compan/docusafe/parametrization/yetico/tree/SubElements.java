package pl.compan.docusafe.parametrization.yetico.tree;

import java.util.List;

public class SubElements {
	
	private List subElements;
	private String lab; 
	private MachineTreeElement parentElement;

	
	
	
	
	public SubElements(){
	
	}
	
//	public SubElements(List subElements, String lab) {
//		super();
//		this.subElements = subElements;
//		this.lab = lab;
//	}
	
	

	public SubElements(List subElements, String lab,
			MachineTreeElement parentElement) {
		super();
		this.subElements = subElements;
		this.lab = lab;
		this.parentElement = parentElement;
	}

	public List getSubElements() {
		return subElements;
	}

	public void setSubElements(List subElements) {
		this.subElements = subElements;
	}

	public String getLab() {
		return lab;
	}

	public void setLab(String lab) {
		this.lab = lab;
	}

	public MachineTreeElement getParentElement() {
		return parentElement;
	}

	public void setParentElement(MachineTreeElement parentElement) {
		this.parentElement = parentElement;
	}

	
}
