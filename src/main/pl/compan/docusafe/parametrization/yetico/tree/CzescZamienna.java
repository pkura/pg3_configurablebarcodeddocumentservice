package pl.compan.docusafe.parametrization.yetico.tree;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.emory.mathcs.backport.java.util.LinkedList;

public class CzescZamienna extends MachineElement implements MachineTreeEditable{

	private String nazwa;
	private String producent;
	private String nrSeryjny;
	private boolean strategiczna;
	private Lokalizacja lokalizacja;
	private String zdjecie;

	private Czesc czesci=null;
	//private MachineTreeElement parentElement;
	
	
	public CzescZamienna(){
	
	}
	
	public CzescZamienna(String nazwa, String producent, String nrSeryjny,
			boolean strategicznal, Lokalizacja lokalizacja, String imageUrl, Czesc czesci) {
		super();
		this.nazwa = nazwa;
		this.producent = producent;
		this.nrSeryjny = nrSeryjny;
		this.strategiczna = strategicznal;
		this.lokalizacja = lokalizacja;
		this.zdjecie = imageUrl;
		this.czesci=czesci;
		//this.parentElement=par;
	}
	
	/*public MachineTreeElement getParentElement() {
		return parentElement;
	}

	public void setParentElement(MachineTreeElement parentElement) {
		this.parentElement = parentElement;
	}*/

	public String getZdjecie() {
		return zdjecie;
	}

	public void setZdjecie(String zdjecie) {
		this.zdjecie = zdjecie;

	}
	public boolean isStrategiczna() {
		return strategiczna;
	}

	public void setStrategiczna(boolean strategiczna) {
		this.strategiczna = strategiczna;
	}

	public Czesc getCzesci() {
		return czesci;
	}

	public void setCzesci(Czesc czesci) {
		this.czesci = czesci;
	}

	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public String getProducent() {
		return producent;
	}
	public void setProducent(String producent) {
		this.producent = producent;
	}
	public String getNrSeryjny() {
		return nrSeryjny;
	}
	public void setNrSeryjny(String nrSeryjny) {
		this.nrSeryjny = nrSeryjny;
	}
	public boolean isStrategicznal() {
		return strategiczna;
	}
	public void setStrategicznal(boolean strategicznal) {
		this.strategiczna = strategicznal;
	}
	public Lokalizacja getLokalizacja() {
		return lokalizacja;
	}
	public void setLokalizacja(Lokalizacja lokalizacja) {
		this.lokalizacja = lokalizacja;
	}
	public String getImageUrl() {
		return zdjecie;
	}
	public void setImageUrl(String imageUrl) {
		this.zdjecie = imageUrl;
	}

	@Override
	public Integer getElementType() {
		return MachineTreeModel.MACHINE_TREE_SPARE_PART;
	}

	@Override
	public String getLabel() {
		return "Cz�� zamienna: " + getNazwa() + " ( nr seryjny: " + getNrSeryjny() + " )";
	}




	@Override
	public List getChilds() {
		return new LinkedList();
	}



	public Long getElementId() {
		return getId();
	}


	@Override
	public List getProperChildren() {
		return null;
	}



	@Override
	public Integer getLevel() {
		return 15;
	}

	public static String[] getFieldNames() {
		
		return new String[] {"spareId", "nazwa", "producent", "nrSeryjny", "strategiczna", "lokalizacja", "zdjecie", "parentId"};
	}
	
	public static  String[] getFieldIds()
	{
		return new String[] {"elementId", "nazwa", "producent","nrSeryjny", "strategiczna", "lokalizacja", "zdjecie", "parentId"};
	}

	public static String[] getFieldTypes() {
		return new String[] {"hidden", "text", "text", "text", "checkbox", "select:Lokalizacja", "text", "hidden"};
	}

	@Override
	public String[] getFieldValues() {
		return new String[] {getId().toString(), getNazwa(), getProducent(), getNrSeryjny(), isStrategiczna() ? "tak" : "nie", MachineDictionary.getInstance().getSelect(Lokalizacja.class), getZdjecie(), "xxx"};
	}

	public static String[] getFieldLabels() {
		return new String[] {"a", "Nazwa", "Producent", "Nr seryjny", "Strategiczna", "Lokalizacja", "Zdj�cie",  "a"};
	}

	@Override
	public Integer getChildType() {
		return null;
	}

	@Override
	public Integer getParentId() {
		return null;
	}

	public String getTyp() {
		return "SPARE";
	}

	@Override
	public MachineTreeElement getParent() {
		// TODO Auto-generated method stub
        return getCzesci();
	}
    public String toString() {
        return "Cz�� zamienna: "+getNazwa()+", producent:  " + getProducent()+", nr seryjny : "+ getNrSeryjny() + ", lokalizacja: "+getLokalizacja().toString()+'\n';
    }
}

