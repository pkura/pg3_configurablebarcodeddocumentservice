package pl.compan.docusafe.parametrization.yetico.tree;

import java.util.HashSet;

import pl.compan.docusafe.web.tree.HtmlTree;

public class MachineTree {
	
	public static HtmlTree newTree(){
		HtmlTree tree = new HtmlTree(new MachineTreeModel(), new MachineTreeView(), "/docusafe");
		return tree;
	}
	
	public static HtmlTree newExpandedTree(Integer elementType, Long elementId, Integer parentType, Integer level){
		HtmlTree tree = new HtmlTree(new MachineTreeModel(elementType, elementId, parentType, level), new MachineTreeView(), "/docusafe"); 
		return tree;
	}
	
	public static HtmlTree newExpandedTree(Long elementId)
	{
		HtmlTree tree = new HtmlTree(new MachineTreeModel(elementId), new MachineTreeView(), "/docusafe");
		return tree;
	}

}
