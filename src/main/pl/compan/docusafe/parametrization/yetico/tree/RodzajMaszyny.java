package pl.compan.docusafe.parametrization.yetico.tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class RodzajMaszyny extends MachineElement implements MachineTreeEditable{

	private String kod;
	private String nazwa;
	private String label;
  //  private RodzajMaszynyhb rodzajMaszynyhb;


    private Set<Maszyna> maszyny;
    private Zaklad zaklad;
	
	public RodzajMaszyny(){
		
	}
	
	public RodzajMaszyny(String kod, String nazwa, Set<Maszyna> maszyny,Zaklad zaklad, Long id) {
		super();
		this.kod = kod;
		this.nazwa = nazwa;
		this.maszyny = maszyny;
        this.zaklad=zaklad;
      //  this.rodzajMaszynyhb=rodzajMaszynyhb;
	}


	public Set<Maszyna> getMaszyny() {
		return maszyny;
	}

	public void setMaszyny(Set<Maszyna> maszyny) {
		this.maszyny = maszyny;
	}
    public Zaklad getZaklad() {
        return zaklad;
    }

    public void setZaklad(Zaklad zaklad) {

        this.zaklad = zaklad;
    }
	public String getKod() 
	{
		return kod;
	}
	public void setKod(String kod) {
		this.kod = kod;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

  /*  public RodzajMaszynyhb getRodzajMaszynyhb() {
        return rodzajMaszynyhb;
    }

    public void setRodzajMaszynyhb(RodzajMaszynyhb rodzajMaszynyhb) {
        this.rodzajMaszynyhb = rodzajMaszynyhb;
    }*/


    @Override
	public Integer getElementType() {
		return MachineTreeModel.MACHINE_TREE_KIND;
	}

	@Override
	public String getLabel() {
		return "Rodzaj maszyny: " + nazwa + " ( kod: " + kod + " )";
	}

	@Override
	public List getChilds() {
		return new LinkedList(getMaszyny());
	}



	@Override
	public Long getElementId() {
		return getId();
	}

	@Override
	public MachineTreeElement getParent() {
        return getZaklad();
	}
	
	/*@Override
	public boolean equals(Object rm){
		if(rm instanceof RodzajMaszyny){
			RodzajMaszyny rodzaj = (RodzajMaszyny) rm; 
		if(rodzaj.getElementId()==this.getElementId()) return true;
		}
		return false;
	
	}*/

	@Override
	public List getProperChildren() {
		return new LinkedList(getMaszyny());
	}



	@Override
	public Integer getLevel() {
		return MachineTreeModel.MACHINE_TREE_LEVEL_KIND;
	}

	public static String[] getFieldNames() 
	{
		return new String[] {"kindId", "kod","nazwa", "parentId"};
	}
	
	public static String[] getFieldIds() 
	{
		return new String[] {"elementId", "kod","nazwa", "parentId"};
	}

	public static String[] getFieldTypes() {
		return new String[] {"hidden", "text", "text","hidden"};
	}

	@Override
	public String[] getFieldValues() {
		return new String[] {getId().toString(), getKod(), getNazwa(), getZaklad().getId().toString()};
	}

	public static String[] getFieldLabels() {
		return new String[] {"a", "Kod", "Nazwa","a"};
	}

	@Override
	public Integer getChildType() {
		return 3;
	}

	@Override
	public Integer getParentId() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getTyp() {
		return "KIND";
	}


    public String toString() {
        return "Rodzaj maszyny: "+getNazwa()+", Kod:  " + getKod()+", zak�ad : "+ getZaklad().getLokalizacja();
    }

}
