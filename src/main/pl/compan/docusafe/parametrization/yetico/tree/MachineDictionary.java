package pl.compan.docusafe.parametrization.yetico.tree;

import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.admin.dockind.acceptance.AcceptanceConditionBean;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class MachineDictionary extends AbstractMachineTreeElement implements MachineTreeElement {
	
	private final static Logger log = LoggerFactory.getLogger(MachineDictionary.class);
	
	private static final String LABEL="S�OWNIK MASZYN I CZʶCI ZAMIENNYCH";
	
	private static MachineDictionary instance=null;
	private Map<String, Object> dictionary=null;
	private List<RodzajMaszyny> rodzajeMaszyn=null;
    private List<Zaklad> zaklad=null;

    public Set<Long> expandedLocs;
	public Set<Long> expandedKinds;
	public Set<Long> expandedMachines;
	public Set<Long> expandedComponents;
	public Set<Long> expandedParts;
	
	
	public static MachineDictionary getInstance(){
		if(instance==null){
			try {
				DSApi.context().session().flush();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				log.error(e.getMessage(), e);
			} 
			instance = new MachineDictionary();
		}
		else
		{
			instance.reaload();
		}
		return instance;
	}
	
	
	private void reaload() {
		expanded = true;
		try{
			zaklad = DSApi.context().session().createCriteria(Zaklad.class).add(Restrictions.eq("typ", "LOC")).list();
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
		
	}


	public MachineTreeEditable find(Class klasa, Long id){
		try{
			DSApi.context().session().flush();
			return (MachineTreeEditable) DSApi.context().session().createQuery("from " + klasa.getName() + " as xxx where xxx.id=" + id).list().get(0);
		}catch(Exception e){
			
		}

		return null;
	}
	
	public String getAddNewForm(Integer type)
	{
		if(type==null)
		{
			String ret  = "<form action=\"?doAddNew=doAddNew\">"
						+ "<select name=kind>"

						+ "<option value=2>Rodzaj maszyny</option>"
						+ "<option value=3>Maszyna</option>"
						+ "<option value=4>Podzesp�</option>"
						+ "<option value=5>Cz��</option>"
						+ "<option value=6>Cz�� zamienna</option>"
						+ "</select>"
						+ "<input type=\"hidden\" name=\"doAddNew\" value=\"doAddNew\"/>"
						+ "<input type=\"submit\" value=\"Wybierz\"/>"
						+ "</form>";
			return ret;
		}
		else
		{
			String ret  = "<form action=\"?doAddNew=doAddNew\">"
					+ "<select name=kind>"

                    + "<option>Rodzaj maszyny</option>"
					+ "<option>Maszyna</option>"
					+ "<option>Podzesp�</option>"
					+ "<option>Cz��</option>"
					+ "<option>Cz�� zamienna</option>"
					+ "</select>"
					+ "<input type=\"hidden\" name=\"doAddNew\" value=\"doAddNew\"/>"
					+ "<input type=\"submit\" value=\"Wybierz\"/>"
					+ "</form>";
			
//			ret += getForm(MachineTreeModel., id, level, parentType, parentId) 
			return ret;
		}
	}
	
	public String getInfo(Integer type, Long id, Integer level, Integer parentType, Long parentId) throws HibernateException, EdmException{
		String[] labels=null;
		String[] types=null;
		String[] names=null;
		String[] values=null;
		String[] ids=null;
		
		
//		MachineElement ele = (MachineElement) DSApi.context().session().createCriteria(MachineElement.class).add(Restrictions.eq("id", id)).list().get(0);
	   //ele.getTyp();
		switch(type){
            case MachineTreeModel.MACHINE_TREE_LOC:
                labels=Zaklad.getFieldLabels();
                types=Zaklad.getFieldTypes();
                names=Zaklad.getFieldNames();
                ids=Zaklad.getFieldIds();
//				criteria.add(Restrictions.eq("code", code));
                try{
                    if(id != null)
                        values = ((Zaklad) DSApi.context().session().createCriteria(Zaklad.class).add(Restrictions.eq("id", id)).list().get(0)).getFieldValues();
                }
                catch(Exception e){
                    log.error(e.getMessage(), e);
                }
                break;
			case MachineTreeModel.MACHINE_TREE_KIND: 
				labels=RodzajMaszyny.getFieldLabels();
				types=RodzajMaszyny.getFieldTypes();
				names=RodzajMaszyny.getFieldNames();
				ids=RodzajMaszyny.getFieldIds();
//				criteria.add(Restrictions.eq("code", code));
				try{
					if(id != null)
				values = ((RodzajMaszyny) DSApi.context().session().createCriteria(RodzajMaszyny.class).add(Restrictions.eq("id", id)).list().get(0)).getFieldValues();
				}
				catch(Exception e){
					log.error(e.getMessage(), e);
				}
				break;
			case MachineTreeModel.MACHINE_TREE_MACHINE: 
				labels=Maszyna.getFieldLabels();
				types=Maszyna.getFieldTypes();
				names=Maszyna.getFieldNames();
				ids=Maszyna.getFieldIds();
				try{
					if(id != null)
					values = ((Maszyna) DSApi.context().session().createCriteria(Maszyna.class).add(Restrictions.eq("id", id)).list().get(0)).getFieldValues();
					}
					catch(Exception e){
						log.error(e.getMessage(), e);
					}
				break;
			case MachineTreeModel.MACHINE_TREE_COMPONENT: 
				labels=Podzespol.getFieldLabels();
				types=Podzespol.getFieldTypes();
				names=Podzespol.getFieldNames();
				ids=Podzespol.getFieldIds();
				try{
					if(id != null)
					values = ((Podzespol) DSApi.context().session().createCriteria(Podzespol.class).add(Restrictions.eq("id", id)).list().get(0)).getFieldValues();
					}
					catch(Exception e){
						log.error(e.getMessage(), e);
					}
				break;
			case MachineTreeModel.MACHINE_TREE_PART:  
				labels=Czesc.getFieldLabels();
				types=Czesc.getFieldTypes();
				names=Czesc.getFieldNames();
				ids=Czesc.getFieldIds();
				try{
					if(id != null)
					values = ((Czesc) DSApi.context().session().createCriteria(Czesc.class).add(Restrictions.eq("id", id)).list().get(0)).getFieldValues();
					}
					catch(Exception e){
						log.error(e.getMessage(), e);
					}
				break;
			case MachineTreeModel.MACHINE_TREE_SPARE_PART:  
				labels=CzescZamienna.getFieldLabels();
				types=CzescZamienna.getFieldTypes();
				names=CzescZamienna.getFieldNames();
				ids=CzescZamienna.getFieldIds();
				try{
					if(id != null)
					values = ((CzescZamienna) DSApi.context().session().createCriteria(CzescZamienna.class).add(Restrictions.eq("id", id)).list().get(0)).getFieldValues();
					}
					catch(Exception e){
						log.error(e.getMessage(), e);
					}
				break;
		}
		if(labels !=null && names!=null && types!=null && labels.length==names.length && labels.length==types.length)
		{
			String form="<table>";
			
			for(int i=0; i < names.length; i++){
				form += "<tr>";
				if(!types[i].equals("hidden")){
					if(types[i].contains("select:")){
						form += "<td>"+labels[i]+": </td><td><select disabled name=\""+names[i]+"\"  id=\"" + ids[i] + "\" >";
						if(names[i].equals("lokalizacja"))
						{
							for(Lokalizacja opcja:Lokalizacja.values())
							{
								form += "<option disabled value=\"" + opcja.toString() + "\"";
								if(values!=null && opcja.toString().equals(values[i]))
								{
									form += " selected ";
								}
								form += ">" + opcja.toString() + "</option>";
							}
						}
						else if(names[i].equals("rodzaj"))
						{
							for(RodzajCzesci opcja: RodzajCzesci.values())
							{
								form += "<option disabled value=\"" + opcja.toString() + "\"";
								if(values!=null && opcja.toString().equals(values[i]))
								{
									form += " selected ";
								}
								form += ">" + opcja.toString() + "</option>";
							}
						}
					
						form += "</select>"; 
					}
					else
					form += "<td>"+labels[i]+": </td><td><input readonly type=\""+types[i]+"\" name=\""+names[i]+"\"  id=\"" + ids[i] + "\" ";
					
					if(!types[i].contains("select:") && values != null){ 
						form += "value=\"" + values[i] + "\" /></td>"; 
					}
				}
				
				form += "</tr>";
			}
			if(id != null && id > 0) form += "<tr><td><input type=\"hidden\" name=\"id\" id=\"elementId\" value=\"" + id +"\" /></td></tr>";
			form = "Parametry wybranego elementu:</br>" + form + "</table>";
			return form;
		}
		return "BRAK";
	}
	
	public String getForm(Integer type, Long id, Integer level, Integer parentType, Long parentId){
		String[] labels=null;
		String[] types=null;
		String[] names=null;
		String[] values=null;
		String[] ids=null;
		List<MachineTreeElement> parents=null;
		String typString = "";
		try{
		switch(type){
            case MachineTreeModel.MACHINE_TREE_LOC:
                typString=" (Zak�ad) ";
                labels=Zaklad.getFieldLabels();
                types=Zaklad.getFieldTypes();
                names=Zaklad.getFieldNames();
                ids=Zaklad.getFieldIds();

//				criteria.add(Restrictions.eq("code", code));
                try{
                    if(id != null)
                        values = ((Zaklad) DSApi.context().session().createCriteria(Zaklad.class).add(Restrictions.eq("id", id)).list().get(0)).getFieldValues();
                }
                catch(Exception e){
                    log.error(e.getMessage(), e);
                }
                break;
			case MachineTreeModel.MACHINE_TREE_KIND: 
				typString=" (Rodzaj maszyny) ";
				labels=RodzajMaszyny.getFieldLabels();
				types=RodzajMaszyny.getFieldTypes();
				names=RodzajMaszyny.getFieldNames();
				ids=RodzajMaszyny.getFieldIds();
				
//				criteria.add(Restrictions.eq("code", code));
               if(parentId==null)parents = DSApi.context().session().createCriteria(Zaklad.class).add(Restrictions.eq("typ", "LOC")).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
				try{
					if(id != null)
				values = ((RodzajMaszyny) DSApi.context().session().createCriteria(RodzajMaszyny.class).add(Restrictions.eq("id", id)).list().get(0)).getFieldValues();
				}
				catch(Exception e){
					log.error(e.getMessage(), e);
				}
				break;
			case MachineTreeModel.MACHINE_TREE_MACHINE: 
				typString=" (Maszyna) ";
				labels=Maszyna.getFieldLabels();
				types=Maszyna.getFieldTypes();
				names=Maszyna.getFieldNames();
				ids=Maszyna.getFieldIds();
				if(parentId==null)parents = DSApi.context().session().createCriteria(RodzajMaszyny.class).add(Restrictions.eq("typ", "KIND")).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
				try{
					if(id != null)
					values = ((Maszyna) DSApi.context().session().createCriteria(Maszyna.class).add(Restrictions.eq("id", id)).list().get(0)).getFieldValues();
					}
					catch(Exception e){
						log.error(e.getMessage(), e);
					}
				break;
			case MachineTreeModel.MACHINE_TREE_COMPONENT: 
				typString=" (Podzesp�) ";
				labels=Podzespol.getFieldLabels();
				types=Podzespol.getFieldTypes();
				names=Podzespol.getFieldNames();
				ids=Podzespol.getFieldIds(); 
				if(parentId==null)parents = DSApi.context().session().createCriteria(Maszyna.class).add(Restrictions.eq("typ", "MACHINE")).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
				try{
					if(id != null)
					values = ((Podzespol) DSApi.context().session().createCriteria(Podzespol.class).add(Restrictions.eq("id", id)).list().get(0)).getFieldValues();
					}
					catch(Exception e){
						log.error(e.getMessage(), e);
					}
				break;
			case MachineTreeModel.MACHINE_TREE_PART:  
				typString=" (Cz��) ";
				labels=Czesc.getFieldLabels();
				types=Czesc.getFieldTypes();
				names=Czesc.getFieldNames();
				ids=Czesc.getFieldIds();
				if(parentId==null)parents = DSApi.context().session().createCriteria(Podzespol.class).add(Restrictions.eq("typ", "COMPONENT")).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
				try{
					if(id != null)
					values = ((Czesc) DSApi.context().session().createCriteria(Czesc.class).add(Restrictions.eq("id", id)).list().get(0)).getFieldValues();
					}
					catch(Exception e){
						log.error(e.getMessage(), e);
					}
				break;
			case MachineTreeModel.MACHINE_TREE_SPARE_PART:
				typString=" (Cz�� zamienna) ";
				labels=CzescZamienna.getFieldLabels();
				types=CzescZamienna.getFieldTypes();
				names=CzescZamienna.getFieldNames();
				ids=CzescZamienna.getFieldIds();
				if(parentId==null)parents = DSApi.context().session().createCriteria(Czesc.class).add(Restrictions.eq("typ", "PART")).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
				try{
					if(id != null)
					values = ((CzescZamienna) DSApi.context().session().createCriteria(CzescZamienna.class).add(Restrictions.eq("id", id)).list().get(0)).getFieldValues();
					}
					catch(Exception e){
						log.error(e.getMessage(), e);
					}
				break;
		}
		if(labels !=null && names!=null && types!=null && ids!= null && labels.length==names.length && labels.length==types.length && labels.length==ids.length)
		{
			String form="";
			
			for(int i=0; i < names.length; i++){
				form += "<tr>";
				if(types[i].equals("hidden")){ 
					if(!names[i].equals("parentId"))
					{ 
						form += "<td></td><td><input type=\""+types[i]+"\" name=\""+names[i]+"\" id=\"" + ids[i] + "\" ";
						if(values != null) form += " value=\"" + values[i] + "\"";
						form += " /></td>";
					}
				}
				else{
					if(types[i].contains("select:")){
						form += "<td>"+labels[i]+": </td><td><select name=\""+names[i]+"\"  id=\"" + ids[i] + "\" >";
						if(names[i].equals("lokalizacja"))
						{
							for(Lokalizacja opcja:Lokalizacja.values())
							{
								form += "<option value=\"" + opcja.toString() + "\"";
								if(values!=null && opcja.toString().equals(values[i]))
								{
									form += " selected ";
								}
								form += ">" + opcja.toString() + "</option>";
							}
						}
						else if(names[i].equals("rodzaj"))
						{
							for(RodzajCzesci opcja: RodzajCzesci.values())
							{
								form += "<option value=\"" + opcja.toString() + "\"";
								if(values!=null && opcja.toString().equals(values[i]))
								{
									form += " selected ";
								}
								form += ">" + opcja.toString() + "</option>";
							}
						}
						form += "</select>"; 
					}
					else
					form += "<td>"+labels[i]+": </td><td><input type=\""+types[i]+"\" name=\""+names[i]+"\"  id=\"" + ids[i] + "\" ";
					
					if(values != null){ 
						form += "value=\"" + values[i] + "\" /></td>"; 
					}
				}
				form += "</tr>";
			}
			
			if(level != null && level > 0) form += "<td><input type=\"hidden\" name=\"level\" value=\"" + level +"\" /></td>";
			if(type != null && type > 0) form += "<td><input type=\"hidden\" name=\"type\" value=\"" + type +"\" /></td>";
			if(parentId != null && parentId > 0) form += "<td><input type=\"hidden\" name=\"parentId\" value=\"" + parentId +"\" /></td>";
			if(parentType != null && parentType > 0) form += "<td><input type=\"hidden\" name=\"parentType\" value=\"" + parentType +"\" /></td>";
			if(id != null && id > 0) form += "<td><input type=\"hidden\" name=\"id\" value=\"" + id +"\" /></td>";
			if(values!=null)
				form += "<td><input type=\"hidden\" name=\"doEdit\" value=\"doEdit\" /></td>";
			else
				form += "<td><input type=\"hidden\" name=\"doAdd\" value=\"doAdd\" /></td>";
			
			form+="</table><input type=\"submit\" value=\"Zapisz\"/></form>";   
			
			if(parentId==null && parents != null && parents.size()>0)
			{
				String selectParent = "<select name=\"parentId\" id=\"parentId\">";
				
				for(MachineTreeElement eleTemp: parents) selectParent += "<option value=\""+ eleTemp.getElementId() +"\">" + eleTemp.getLabel() + "</option>";
				selectParent += "</select>";
				
				form = selectParent + form;
			}
			
			form = "<form action=\"\" method=\"get\"><table>" + form;
			
			if(id!=null) form = "Edycja elementu<br/>" + form;
			else form = "Dodawanie elementu" + typString + "<br/>" + form;
			return form;
			
			
		}
		}
		catch(Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return "BRAK";
	}
	
	public String generate(MachineTreeEditable item)
	{
		String ret = "<form method=\"post\" action=\"\"";
		return null;
	}
	
	public MachineTreeElement get(Long id)
	{
		try
		{
			ResultSet rs = DSApi.context().createStatement().executeQuery("select typ from dsg_machine_tree_element where id=" + id);
			while(rs.next()){
				String typ = rs.getString("typ");
                if(typ.equals("LOC"))
                {

                    return (Zaklad) DSApi.context().session().createCriteria(Zaklad.class).add(Restrictions.eq("id", id)).list().get(0);
                }
				else if(typ.equals("KIND"))
				{
					
					return (RodzajMaszyny) DSApi.context().session().createCriteria(RodzajMaszyny.class).add(Restrictions.eq("id", id)).list().get(0);
				}
				else if(typ.equals("MACHINE"))
				{
					return (Maszyna) DSApi.context().session().createCriteria(Maszyna.class).add(Restrictions.eq("id", id)).list().get(0);
				}
				else if(typ.equals("COMPONENT"))
				{
					return (Podzespol) DSApi.context().session().createCriteria(Podzespol.class).add(Restrictions.eq("id", id)).list().get(0);
				}
				else if(typ.equals("PART"))
				{
					return (Czesc) DSApi.context().session().createCriteria(Czesc.class).add(Restrictions.eq("id", id)).list().get(0);
				}
				else if(typ.equals("SPARE"))
				{
					return (CzescZamienna) DSApi.context().session().createCriteria(CzescZamienna.class).add(Restrictions.eq("id", id)).list().get(0);
				}
			}
		}
		catch(Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return null;
	}
	
	public MachineTreeElement get(Integer type, Long id, Integer parentType){
		try{
		String from=null;
		String idcol=null;
		
//		if(type==null) return false;
		switch(type){
            case MachineTreeModel.MACHINE_TREE_LOC: from=Zaklad.class.getName(); break;
			case MachineTreeModel.MACHINE_TREE_KIND: from=RodzajMaszyny.class.getName(); break;
			case MachineTreeModel.MACHINE_TREE_MACHINE: from=Maszyna.class.getName(); break;
			case MachineTreeModel.MACHINE_TREE_COMPONENT: from=Podzespol.class.getName(); break;
			case MachineTreeModel.MACHINE_TREE_PART: from=Czesc.class.getName(); break;
			case MachineTreeModel.MACHINE_TREE_SPARE_PART: from=CzescZamienna.class.getName(); break;
			
			case MachineTreeModel.MACHINE_TREE_PARAMS: from="params"; break;
			case MachineTreeModel.MACHINE_TREE_PARAM: from="param"; break;
			case MachineTreeModel.MACHINE_TREE_SUB_ELEMENT: from="sub"; break;
		}
		if(from != null){
			if(from.equals("params"))
			{
				MachineTreeElement xxx = get(parentType, id, null);
				return xxx;
			}
			else if (from.equals("param"))
			{
				
			}
			else if (from.equals("sub"))
			{
				MachineTreeElement xxx = get(parentType, id, null);
				return xxx;
			}
			String query = "from " + from + " as element where element.id="+id;
			MachineTreeElement mte = (MachineTreeElement) DSApi.context().session().load(from, id);
			
			
			
			return mte;
		}
		
		}catch(Exception e){
			e.getMessage();
		}
		return null;
	}
	
	
	public void expand(Integer kind, Long id){
		switch(kind){
            case MachineTreeModel.MACHINE_TREE_LOC: toggle(expandedLocs, id); break;
			case MachineTreeModel.MACHINE_TREE_KIND: toggle(expandedKinds, id); break;
			case MachineTreeModel.MACHINE_TREE_MACHINE: toggle(expandedMachines, id); break;
			case MachineTreeModel.MACHINE_TREE_COMPONENT: toggle(expandedComponents, id); break;
			case MachineTreeModel.MACHINE_TREE_PART: toggle(expandedParts, id); break;
		}
	}
	
	private void toggle(Set<Long> set, Long id){
		if(set.contains(id)) set.remove(id);
		else set.add(id);
	}
	
	public boolean isExpanded(Integer kind, Integer id){
		switch(kind){
            case MachineTreeModel.MACHINE_TREE_LOC: return expandedLocs.contains(id);
			case MachineTreeModel.MACHINE_TREE_KIND: return expandedKinds.contains(id);
			case MachineTreeModel.MACHINE_TREE_MACHINE: return expandedMachines.contains(id);
			case MachineTreeModel.MACHINE_TREE_COMPONENT: return expandedComponents.contains(id);
			case MachineTreeModel.MACHINE_TREE_PART: return expandedParts.contains(id);
		}
		
		return false;
	}
	
	private MachineDictionary(){
		super();
		expanded = true;
		try{
			zaklad = DSApi.context().session().createCriteria(Zaklad.class).add(Restrictions.eq("typ", "LOC")).list();
			
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
		
	}
	
	public MachineTreeElement getElement(){
		return null;
	}

	public Map<String, Object> getDictionary() {
		return dictionary;
	}

	public void setDictionary(Map<String, Object> dictionary) {
		this.dictionary = dictionary;
	}

	public List<RodzajMaszyny> getRodzajeMaszyn() {
		return rodzajeMaszyn;
	}

	public void setRodzajeMaszyn(List<RodzajMaszyny> rodzajeMaszyn) {
		this.rodzajeMaszyn = rodzajeMaszyn;
	}
    public List<Zaklad> getZaklad() {
        return zaklad;
    }

    public void setZaklad(List<Zaklad> zaklad) {
        this.zaklad = zaklad;
    }

	@Override
	public Integer getElementType() {
		return 0;
	}

	@Override
	public String getLabel() {
		return MachineDictionary.LABEL;
	}

	@Override
	public List getChilds() {		
		try {
			return DSApi.context().session().createCriteria(Zaklad.class).add(Restrictions.eq("typ", "LOC")).list();
		} catch (Exception e) {
			return null;
		} 
	}



	@Override
	public Long getElementId() {
		return -1L;
	}

	@Override
	public MachineTreeElement getParent() {
		// TODO Auto-generated method stub
		
		return null;
	}

	@Override
	public List getProperChildren() {
		return getZaklad();
	}


	@Override
	public Integer getLevel() {
		return MachineTreeModel.MACHINE_TREE_LEVEL_DICTIONARY;
	}


	public static String getSelect(Class class1) {
		
		return null;
	}


	@Override
	public Integer getChildType() {
		return 1;
	}


	@Override
	public Integer getParentId() {
		return null;
	}


	@Override
	public String getTyp() {
		return "ROOT";
	}
	
	

}
