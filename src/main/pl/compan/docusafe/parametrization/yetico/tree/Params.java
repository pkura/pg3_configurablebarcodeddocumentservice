package pl.compan.docusafe.parametrization.yetico.tree;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.emory.mathcs.backport.java.util.LinkedList;

public class Params {
	
	private List<Param> parameters;
	private MachineTreeElement parentElement;
	
	
	
	public Params(){
		
	}

//	public Params(List<Param> parmeters) {
//		super();
//		this.parameters = parmeters;
//	}
	
	

	public Params(List<Param> parameters, MachineTreeElement parentElement) {
		super();
		this.parameters = parameters;
		this.parentElement = parentElement;
	}

	public List<Param> getParmeters() 
	{
		return parameters;
	}

	public void setParmeters(List<Param> parmeters) 
	{
		this.parameters = parmeters;
	}

	public List<Param> getParameters() {
		return parameters;
	}

	public void setParameters(List<Param> parameters) {
		this.parameters = parameters;
	}

	public MachineTreeElement getParentElement() {
		return parentElement;
	}

	public void setParentElement(MachineTreeElement parentElement) {
		this.parentElement = parentElement;
	}




}
