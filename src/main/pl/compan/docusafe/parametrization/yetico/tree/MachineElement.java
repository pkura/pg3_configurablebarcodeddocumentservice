package pl.compan.docusafe.parametrization.yetico.tree;

public class MachineElement {
	
	private Long id;
	private String typ;
	
	public MachineElement(){
		
	}
	
	public MachineElement(Long id, String typ) {
		super();
		this.id = id;
		this.typ = typ;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	
	

}
