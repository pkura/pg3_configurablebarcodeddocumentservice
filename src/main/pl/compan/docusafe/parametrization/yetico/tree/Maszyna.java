package pl.compan.docusafe.parametrization.yetico.tree;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import edu.emory.mathcs.backport.java.util.Collections;

public class Maszyna extends MachineElement implements MachineTreeEditable{
	
	private String kod;
	private String nazwa;
	private String nrSeryjny;
	private String producent;
	private String model;
	private Lokalizacja lokalizacja;
	
	private RodzajMaszyny rodzaj;
	
	private Set<Podzespol> podzespoly;
	
	public Maszyna()
	{
		
	}

	public Maszyna(String kod, String nazwa, String nrSeryjny,
			String producent, String model, Lokalizacja lokalizacja,
			Set<Podzespol> podzespoly, RodzajMaszyny rodzaj, Long machineId) {
		super();
		this.kod = kod;
		this.nazwa = nazwa;
		this.nrSeryjny = nrSeryjny;
		this.producent = producent;
		this.model = model;
		this.lokalizacja = lokalizacja;
		this.podzespoly = podzespoly;
		this.rodzaj=rodzaj;
	}
	

	public RodzajMaszyny getRodzaj() {
		return rodzaj;
	}

	public void setRodzaj(RodzajMaszyny rodzaj) {
		this.rodzaj = rodzaj;
	}




	public Set<Podzespol> getPodzespoly() {
		return podzespoly;
	}

	public void setPodzespoly(Set<Podzespol> podzespoly) {
		this.podzespoly = podzespoly;
	}

	public String getKod() {
		return kod;
	}
	public void setKod(String kod) {
		this.kod = kod;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public String getNrSeryjny() {
		return nrSeryjny;
	}
	public void setNrSeryjny(String nrSeryjny) {
		this.nrSeryjny = nrSeryjny;
	}
	public String getProducent() {
		return producent;
	}
	public void setProducent(String producent) {
		this.producent = producent;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public Lokalizacja getLokalizacja() {
		return lokalizacja;
	}
	public void setLokalizacja(Lokalizacja lokalizacja) {
		this.lokalizacja = lokalizacja;
	}

	@Override
	public Integer getElementType() {
		return MachineTreeModel.MACHINE_TREE_MACHINE;
	}

	@Override
	public String getLabel() {
//		String prefix = MachineTreeView.vline + MachineTreeView.blank + MachineTreeView.childlessDown;
		return "Maszyna: " + nazwa;
	}

	@Override
	public List getChilds() {
		return new LinkedList(getPodzespoly());
	}


	@Override
	public Long getElementId() {
		return getId();
	}

	@Override
	public MachineTreeElement getParent() {
		return getRodzaj();
	}

	@Override
	public List getProperChildren() {
		return new LinkedList(getPodzespoly());
	}



	public static String[] getFieldNames() {
		
		return new String[] {"machineId", "kod", "nazwa", "nrSeryjny", "producent", "model", "lokalizacja", "parentId"};
	}

	public static  String[] getFieldIds()
	{
		return new String[] {"elementId", "kod", "nazwa", "nr_seryjny", "producent", "model", "lokalizacja", "parentId"};
	}
	
	public static String[] getFieldTypes() {
		return new String[] {"hidden", "text", "text", "text", "text", "text", "select:Lokalizacja", "hidden"};
	}

	@Override
	public String[] getFieldValues() {
		return new String[] {getId().toString(), getKod(), getNazwa(), getNrSeryjny(), getProducent(), getModel(), lokalizacja.toString(), getRodzaj().getId().toString()};
	}

	public static String[] getFieldLabels() {
		return new String[] {"a", "Kod", "Nazwa", "Nr seryjny", "Producent", "Model", "Lokalizacja", "kod"};
	}

	@Override
	public Integer getLevel() {
		return MachineTreeModel.MACHINE_TREE_LEVEL_MACHINE;
	}

	@Override
	public Integer getChildType() {
		return 4;
	}

	@Override
	public Integer getParentId() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getTyp() {
		return "MACHINE";
	}

    public String toString() {
        return "Maszyna: "+getNazwa()+", kod:  " + getKod()+", nrSeryjny : "+ getNrSeryjny()+ ", producent: "+getProducent()+", model: "+getModel()+
                ", lokalizacja: "+ getLokalizacja().toString()+", "+getRodzaj().toString()+'\n';
    }
}

