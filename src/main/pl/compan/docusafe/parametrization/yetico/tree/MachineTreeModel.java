package pl.compan.docusafe.parametrization.yetico.tree;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.jfree.util.Log;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.machinetree.ShowTreeAction;
import pl.compan.docusafe.web.tree.HtmlTreeModel;

public class MachineTreeModel implements HtmlTreeModel {
	
	public static final int MACHINE_TREE_ROOT = 0;
    public static final int MACHINE_TREE_LOC = 1;
	public static final int MACHINE_TREE_KIND = 2;
	public static final int MACHINE_TREE_MACHINE = 3;
	public static final int MACHINE_TREE_COMPONENT = 4;
	public static final int MACHINE_TREE_PART = 5;
	public static final int MACHINE_TREE_SPARE_PART = 6;
	public static final int MACHINE_TREE_PARAMS = 7;
	public static final int MACHINE_TREE_PARAM = 8;
	public static final int MACHINE_TREE_SUB_ELEMENT = 9;
	
	public static final int MACHINE_TREE_LEVEL_DICTIONARY = 0;
    public static final int MACHINE_TREE_LEVEL_LOC = 3;
	public static final int MACHINE_TREE_LEVEL_KIND = 6;
	public static final int MACHINE_TREE_LEVEL_MACHINE = 9;
	public static final int MACHINE_TREE_LEVEL_MACHINE_PARAMS = 10;
	public static final int MACHINE_TREE_LEVEL_MACHINE_PARAM = 11;
	public static final int MACHINE_TREE_LEVEL_COMPONENTS = 12;
	public static final int MACHINE_TREE_LEVEL_COMPONENT = 13;
	
	private Integer type, parentType, level;
	private Long id;
	private HashSet<MachineTreeElement> exp;
	public static Long partId;
	
	public static Logger log = LoggerFactory.getLogger(MachineTreeModel.class);
	
	public MachineTreeModel(){
	}

	public MachineTreeModel(Integer type, Long id, Integer par, Integer level) {
		this.id = id;
		this.type = type;
		this.parentType=par;
		this.level=level;
	}

	public MachineTreeModel(HashSet<MachineTreeElement> exp) {
		this.exp=exp;
	}

	public MachineTreeModel(Long elementId) {
		this.id=elementId;
	}

	@Override
	public Object getRootElement() {
		return MachineDictionary.getInstance();
	}

	@Override
	public List getChildElements(Object element) {
		try{
		if(element instanceof MachineTreeElement){
			MachineTreeElement mte = (MachineTreeElement) element;
			if(mte.getTyp().equals("ROOT"))
			{
				return DSApi.context().session().createCriteria(Zaklad.class).add(Restrictions.eq("typ", "LOC")).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
			}
            else if(mte.getTyp().equals("LOC"))
            {
                Zaklad temp = (Zaklad) mte;
                return DSApi.context().session().createCriteria(RodzajMaszyny.class).add(Restrictions.eq("typ", "KIND")).add(Restrictions.eq("zaklad.id", temp.getId())).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            }
			else if(mte.getTyp().equals("KIND"))
			{
				RodzajMaszyny temp = (RodzajMaszyny) mte;
				return DSApi.context().session().createCriteria(Maszyna.class).add(Restrictions.eq("typ", "MACHINE")).add(Restrictions.eq("rodzaj.id", temp.getId())).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
			}
			else if(mte.getTyp().equals("MACHINE"))
			{
				Maszyna temp = (Maszyna) mte;
				return DSApi.context().session().createCriteria(Podzespol.class).add(Restrictions.eq("typ", "COMPONENT")).add(Restrictions.eq("maszyna.id", temp.getId())).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
			}
			else if(mte.getTyp().equals("COMPONENT"))
			{
				Podzespol temp = (Podzespol) mte;
				return DSApi.context().session().createCriteria(Czesc.class).add(Restrictions.eq("typ", "PART")).add(Restrictions.eq("podzespol.id", temp.getId())).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
			}
			else if(mte.getTyp().equals("PART"))
			{
				Czesc temp = (Czesc) mte;
				DSApi.context().session().refresh(temp);
				LinkedList ret = new LinkedList();
				for(CzescZamienna cz :temp.getCzesciZamienne()) if(!ret.contains(cz)) ret.add(cz);
				return ret;
			}
			else if(mte.getTyp().equals("SPARE"))
			{
				return null;
			}
		}
		}
		catch(Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return new ArrayList(0);
	}
	
	public boolean isPa(MachineTreeElement doSprawdzenia, MachineTreeElement rozwijany){
		if(doSprawdzenia.getParent()==null) return false; 
		else{
			Log.error("1XYZCX: " + doSprawdzenia.getElementId());
			Log.error("2XYZCX: " + rozwijany.getElementId());
			if(doSprawdzenia.getElementType()==rozwijany.getElementType() && doSprawdzenia.getElementId() != rozwijany.getElementId()) return false;
			if(!(rozwijany instanceof CzescZamienna) && rozwijany.getParent()==null) return false;
			MachineTreeElement parent = null;
			if(rozwijany instanceof CzescZamienna) parent = MachineDictionary.getInstance().get(MACHINE_TREE_PART, MachineTreeModel.partId, null);
			else parent = rozwijany.getParent();
			if(parent.getElementId()==doSprawdzenia.getElementId() && parent.getElementType()==doSprawdzenia.getElementType()) return true;
			else return isPa(doSprawdzenia, parent);		
		}
	}

	public boolean isParentRecurrent(MachineTreeElement czyOtwarty, MachineTreeElement temp)
	{
		if(czyOtwarty==null || temp==null) return false;
		else
		{
			if(czyOtwarty.getElementId().equals(temp.getElementId())) return true;
			else return isParentRecurrent(czyOtwarty, temp.getParent());
		}
	}
	

	@Override
	public boolean isExpanded(Object element) {
//		return true;
//		element.getClass();
//		if(type==null || id==null) return false;
//		if(element instanceof MachineTreeElement )
//		{
//			MachineTreeElement el = (MachineTreeElement) element;
//			if(el.getElementType()==MachineTreeModel.MACHINE_TREE_ROOT) return true;
//			if(el.getElementId()==id && el.getElementType()==type) return true;
//			if(el.getLevel()>level) return false;
//			
//			
//			MachineTreeElement rozwijany = MachineDictionary.getInstance().get(type, id, parentType);
//			
//			return isPa(el, rozwijany);
//		}
		if(element instanceof MachineTreeElement )
		{
			MachineTreeElement el = (MachineTreeElement) element;
			if(el.getElementType()==MachineTreeModel.MACHINE_TREE_ROOT) return true;
			if(id!=null && el.getElementId().equals(id)) return true;
			
			if(id!=null){
				MachineTreeElement temp = MachineDictionary.getInstance().get(id);
				return isParentRecurrent(el, temp);
			}
		}
		return false;
	}

	@Override
	public String getUrl(Object element) {
		MachineTreeElement mte = (MachineTreeElement) element;
		String url = "show-tree.action?doExpand=doExpand&level=" + mte.getLevel() + "&type=" + mte.getElementType() + "&id=" + mte.getElementId();
		if(mte instanceof Params) url+="&parentType=" + mte.getParent().getElementType();
		if(mte instanceof SubElements) url += "&parentType=" + mte.getParent().getElementType() + "&parentId=" + mte.getParent().getElementId();
		if(mte instanceof Podzespol) url += "&parentType=8";
		if(mte instanceof CzescZamienna){
			url += "&parentType=8";
			if(partId!=null) url += "&parentId="+partId;
		}
		if(element instanceof MachineTreeEditable){
			
		}
		return url;
	}

}
