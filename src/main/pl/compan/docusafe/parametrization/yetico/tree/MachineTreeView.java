package pl.compan.docusafe.parametrization.yetico.tree;

import pl.compan.docusafe.web.tree.HtmlTreeView;

public class MachineTreeView implements HtmlTreeView {
	
	public static final String blank = "<img src='/docusafe/img/blank.gif' width='17' height='17' border='0' class='tree'>";
	public static final String childless = "<img src='/docusafe/img/childless-up.gif' width='17' height='17' border='0' class='tree'>";
	public static final String vline = "<img src='/docusafe/img/vline.gif' width='17' height='17' border='0' class='tree'>";
	public static final String childlessDown = "<img src='/docusafe/img/childless-updown.gif' width='17' height='17' border='0' class='tree'>";

    public static final String removeImg = "<img src='/docusafe/img/minus.gif' width='17' height='10' border='0' >";
	public static final String editImg = "<img src='/docusafe/img/plus.gif' width='17' height='10' border='0' >";
	public static final String addImg = "<img src='/docusafe/img/pencil.gif' width='17' height='10' border='0' >";
	
	public MachineTreeView(){
		
	}

	@Override
	public String getHtml(Object element) {
		if(element instanceof MachineTreeElement)
		{
			MachineTreeElement mte = (MachineTreeElement) element;
			
			if(mte instanceof Param || mte instanceof Params || mte instanceof SubElements || mte instanceof MachineDictionary) return mte.getLabel(); 
			else{
				Integer typ = 1;
				switch(mte.getElementType())
				{
                case 0: typ=MachineTreeModel.MACHINE_TREE_LOC; break;
				case 1: typ=MachineTreeModel.MACHINE_TREE_KIND; break;
				case 2: typ=MachineTreeModel.MACHINE_TREE_MACHINE; break;
				case 3: typ=MachineTreeModel.MACHINE_TREE_COMPONENT; break;
				case 4: typ=MachineTreeModel.MACHINE_TREE_PART; break;
				case 5: typ=MachineTreeModel.MACHINE_TREE_SPARE_PART; break;
				case 6: break;
				}
				
				String url =
				"<a href=\"?doInfo=doInfo&level=" + mte.getLevel() 
				+ "&type=" + mte.getElementType() 
				+ "&id=" + mte.getElementId() 
				+ "&prentType=" + mte.getParent().getElementType() 
				+ "&parentId=" + mte.getParent().getElementId() 
				+  "\">" + mte.getLabel() + "</a>";
				
				
				String ret = url  
						+ "<a href=\"?doAddForm=doAddForm&level=" + mte.getLevel() 
						+ "&type=" + mte.getElementType()
						+ "&addType=" + typ
						+ "&id=" + mte.getElementId() 
						+ "&prentType=" + mte.getParent().getElementType() 
						+ "&parentId=" + mte.getElementId() 
						+  "\">" + MachineTreeView.editImg 
						+ "</a>"
						+ "<a href=\"?doEditForm=doEditForm&level=" + mte.getLevel() 
						+ "&type=" + mte.getElementType() 
						+ "&id=" + mte.getElementId() 
						+ "&prentType=" + mte.getParent().getElementType() 
						+ "&parentId=" + mte.getParent().getElementId() 
						+  "\">" + MachineTreeView.addImg
						+ "</a>"
                        + "<a href=\"?doRemove=doRemove&level=" + mte.getLevel()
                        + "&type=" + mte.getElementType()
                        + "&id=" + mte.getElementId()
                        + "&prentType=" + mte.getParent().getElementType()
                        + "&parentId=" + mte.getParent().getElementId()
                        +  "\">" + MachineTreeView.removeImg
                        + "</a>";

                return ret;
			}
		}
		return null;
	}

}
