package pl.compan.docusafe.parametrization.yetico.tree;

import edu.emory.mathcs.backport.java.util.LinkedList;

import java.util.List;
import java.util.Set;

public class Zaklad extends MachineElement implements MachineTreeEditable{
    private String label;

    private Lokalizacja lokalizacja;
    private Set<RodzajMaszyny> rodzajMaszyny;
	public Zaklad(){

	}
	public Zaklad(Set<RodzajMaszyny> rodzajMaszyny,Lokalizacja lokalizacja) {
		super();

        this.lokalizacja = lokalizacja;
		this.rodzajMaszyny = rodzajMaszyny;
	}

	public Set<RodzajMaszyny> getRodzajMaszyny() {

		return rodzajMaszyny;
	}
    public void setRodzajMaszyny(Set<RodzajMaszyny>rodzajMaszyny) {
        this.rodzajMaszyny = rodzajMaszyny;
    }
    public Lokalizacja getLokalizacja() {
        return lokalizacja;
    }
    public void setLokalizacja(Lokalizacja lokalizacja) {
        this.lokalizacja = lokalizacja;
    }

	@Override
	public Integer getElementType() {
		return MachineTreeModel.MACHINE_TREE_LOC;
	}

	@Override
	public String getLabel() {
		return "Zak�ad: " + lokalizacja.toString() ;
	}

	@Override
	public List getChilds() {

		return new LinkedList(getRodzajMaszyny());
	}

	@Override
	public Long getElementId() {
		return getId();
	}
	@Override
	public MachineTreeElement getParent() {

        return MachineDictionary.getInstance();
	}
	@Override
	public List getProperChildren() {
		return new LinkedList( getRodzajMaszyny());
	}
	public Integer getLevel() {
        return MachineTreeModel.MACHINE_TREE_LEVEL_LOC;
	}
	public static  String[] getFieldNames()
	{
		return new String[] {"zakladId","lokalizacja"};
	}
	
	public static  String[] getFieldIds()
	{
		return new String[] {"elementId","lokalizacja"};
	}

	public static String[] getFieldTypes() 
	{
		return new String[] {"hidden", "select:Lokalizacja"};
	}

	@Override
	public String[] getFieldValues() 
	{
		return new String[] {getId().toString(), lokalizacja.toString()};
	}

	public static String[] getFieldLabels() 
	{
		return new String[] {"a", "Lokalizacja"};
	}
	@Override
	public Integer getChildType() {
		return 2;
	}
	@Override
	public Integer getParentId() {
		return null;
	}
	public String getTyp() {
		return "LOC";
	}
	
	

}
