package pl.compan.docusafe.parametrization.yetico.tree;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Podzespol extends MachineElement implements MachineTreeEditable{
	
	private String kod;
	private String nazwa;
	private Set<Czesc> czesci=null;
	
	private Maszyna maszyna;
	
	public Podzespol(){
		
	}
	
	public Podzespol(String kod, String nazwa) {
		super();
		this.kod = kod;
		this.nazwa = nazwa;
	}
	
	public Podzespol(String kod, String nazwa, HashSet<Czesc> czesci, Maszyna maszyna, Long componentId) {
		super();
		this.kod = kod;
		this.nazwa = nazwa;
		this.czesci = czesci;
		this.maszyna=maszyna;
	}


	public Maszyna getMaszyna() {
		return maszyna;
	}

	public void setMaszyna(Maszyna maszyna) {
		this.maszyna = maszyna;
	}

	public String getKod() {
		return kod;
	}
	public void setKod(String kod) {
		this.kod = kod;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Set<Czesc> getCzesci() {
		return czesci;
	}

	public void setCzesci(Set<Czesc> czesci) {
		this.czesci = czesci;
	}

	@Override
	public Integer getElementType() {
		return MachineTreeModel.MACHINE_TREE_COMPONENT;
	}

	@Override
	public String getLabel() {
		return "Podzesp�: " + nazwa + " ( kod: " + kod + " )";
	}

	@Override
	public List getChilds() {
		
		return new LinkedList(getCzesci());
	}



	@Override
	public Long getElementId() {
		return getId();
	}

	@Override
	public MachineTreeElement getParent() {
		return getMaszyna();
	}

	@Override
	public List getProperChildren() {
		return new LinkedList(getCzesci());
	}



	@Override
	public Integer getLevel() {
		return MachineTreeModel.MACHINE_TREE_LEVEL_COMPONENT;
	}

	public static String[] getFieldNames() {
		
		return new String[] {"componentId", "kod", "nazwa", "parentId"};
	}
	
	public static String[] getFieldIds() {
		
		return new String[] {"elementId", "kod", "nazwa", "parentId"};
	}

	public static String[] getFieldTypes() {
		return new String[] {"hidden", "text", "text", "hidden"};
	}

	@Override
	public String[] getFieldValues() {
		return new String[] {getId().toString(), getKod(), getNazwa(), getMaszyna().getId().toString()};
	}

	public static String[] getFieldLabels() {
		return new String[] {"a", "Kod", "Nazwa", "a"};
	}

	@Override
	public Integer getChildType() {
		return 5;
	}

	@Override
	public Integer getParentId() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getTyp() {
		return "COMPONENT";
	}

    public String toString() {
        return "Podzesp�: "+getNazwa()+", kod:  " + getKod()+", "+ getMaszyna().toString()+"\n";
    }


}
