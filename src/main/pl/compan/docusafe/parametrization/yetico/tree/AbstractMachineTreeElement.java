package pl.compan.docusafe.parametrization.yetico.tree;


public abstract class AbstractMachineTreeElement{
	
	protected boolean expanded;
	
	
	public AbstractMachineTreeElement(){
		expanded = false;
	}

	public boolean isExpanded() {
		return expanded;
	}

	public void setExpanded(boolean expanded) {
		
		this.expanded = expanded;
	}

	
	
}
