
package pl.compan.docusafe.parametrization.yetico.tree;

import org.apache.xpath.operations.Bool;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.*;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.parametrization.uek.hbm.ErpPositionEntity;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.*;
/**
 * Created by Raf on 2014-07-16.
 */

@Entity(name = "RodzajMaszyny")
@Table(name = "dsg_yetico_rodzaj_maszyny")
public class RodzajMaszynyhb  {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "NAZWA", length = 250)
    private String nazwa;
    @Column(name = "Kod")
    private String kod;



    public static RodzajMaszynyhb findById(Long entryId) throws EdmException {

        Criteria criteria = DSApi.context().session().createCriteria(RodzajMaszynyhb.class);
        criteria.add(Restrictions.idEq(entryId));

        return (RodzajMaszynyhb)criteria.uniqueResult();
    }

    public static Map<String,String> getMapToDocFields(){
        Map<String, String> mapping = new LinkedHashMap<String, String>();
        mapping.put("nazwa","NAZWA");
        mapping.put("kod","KOD");

        return mapping;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getKod() {
        return kod;
    }

    public void setKod(String kod) {
        this.kod = kod;
    }



}


