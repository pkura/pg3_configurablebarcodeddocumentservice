package pl.compan.docusafe.parametrization.yetico.tree;

import java.util.List;

public interface MachineTreeElement {
	
	public Integer getElementType();    //typ elementu
	public String getLabel();	//nazwa wy�wietlona w drzewie
	public List getChilds(); //zwraca dzieci wyswietlane w drzewie np. dla Maszyny b�d� to 2 szt. - Params oraz SubElements 
	public String getTyp();
	

	
	public Long getElementId(); // id tego elementu (obiekty Params oraz SubElements maja id rodzic�w)
	public MachineTreeElement getParent(); // zwraca rodzica wyswietlanego w drzewie
	public List getProperChildren(); // zwraca dzieci wynikajace z diagramu np. dla Maszyna.getProperChildren() zwr�ci lista Podzesp��w
	
	public Integer getLevel(); // poziom drzewa
	
	public Integer getChildType();
	public Integer getParentId();
	
}
