package pl.compan.docusafe.parametrization.yetico.tree;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.auth.AuthUtil;

import com.opensymphony.webwork.ServletActionContext;

import edu.emory.mathcs.backport.java.util.LinkedList;

public class Czesc extends MachineElement implements MachineTreeEditable{
	
	private String kod;
	private String nazwa;
	private String opis;
	private RodzajCzesci rodzajczesci;
	
	private Podzespol podzespol;
	
	private Set<CzescZamienna> czesciZamienne=null;
	

	public Czesc(){
		
	}
	public Czesc(String kod, String nazwa, String opis,
			RodzajCzesci rodzajczesci, Set<CzescZamienna> czesciZamienne) {
		super();
		this.kod = kod;
		this.nazwa = nazwa;
		this.opis = opis;
		this.rodzajczesci = rodzajczesci;
		this.czesciZamienne = czesciZamienne;
	}

	public Set<CzescZamienna> getCzesciZamienne() {
		//for(CzescZamienna cz: czesciZamienne) cz.setParentElement(this);
		return czesciZamienne;
	}

	public void setCzesciZamienne(Set<CzescZamienna> czesciZamienne) {
		this.czesciZamienne = czesciZamienne;
	}


	public Podzespol getPodzespol() {
		return podzespol;
	}

	public void setPodzespol(Podzespol podzespol) {
		this.podzespol = podzespol;
	}

	public String getKod() {
		return kod;
	}
	public void setKod(String kod) {
		this.kod = kod;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public RodzajCzesci getRodzajczesci() {
		return rodzajczesci;
	}
	public void setRodzajczesci(RodzajCzesci rodzajczesci) {
		this.rodzajczesci = rodzajczesci;
	}

	@Override
	public Integer getElementType() {
		return MachineTreeModel.MACHINE_TREE_PART;
	}

	@Override
	public String getLabel() {
		return "Cz��: " + nazwa + " ( kod: " + kod + " )";
	}

	@Override
	public List getChilds() {

		return new LinkedList(getCzesciZamienne());
	}

	@Override
	public Long getElementId() {
		return getId();
	}
	@Override
	public MachineTreeElement getParent() {
		
		return getPodzespol();
	}
	@Override
	public List getProperChildren() {
		return new LinkedList( getCzesciZamienne());
	}
	public Integer getLevel() {
		return 14;
	}
	public static  String[] getFieldNames()
	{
		return new String[] {"partId", "kod", "nazwa", "opis", "rodzaj", "parentId"};
	}
	
	public static  String[] getFieldIds()
	{
		return new String[] {"elementId", "kod", "nazwa", "opis", "rodzaj", "parentId"};
	}

	public static String[] getFieldTypes() 
	{
		return new String[] {"hidden", "text", "text", "text", "select:RodzajCzesci", "hidden"};
	}

	@Override
	public String[] getFieldValues() 
	{
		return new String[] {getId().toString(), getKod(), getNazwa(), getOpis(), rodzajczesci.toString(), getPodzespol().getId().toString()};
	}

	public static String[] getFieldLabels() 
	{
		return new String[] {"a", "Kod", "Nazwa", "Opis", "Rodzaj cz�ci", "a"};
	}
	@Override
	public Integer getChildType() {
		return 6;
	}
	@Override
	public Integer getParentId() {
		return null;
	}
	public String getTyp() {
		return "PART";
	}

    public String toString() {
        return "Cz��: "+getNazwa()+", Kod:  " + getKod()+", opis : "+ getOpis() + ", rodzaj cz�ci: "+getRodzajczesci().toString()+", "+getPodzespol().toString()+'\n';
    }

}
