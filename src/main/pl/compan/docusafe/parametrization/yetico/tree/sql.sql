create table dsg_machine_tree_element(
	id numeric(19, 0) identity (1,1) primary key,
	parent_id numeric(19, 0),
	rodzajczesci varchar(249),
	kod varchar(249) ,
	nazwa varchar(249),
	nr_seryjny varchar(249),
	producent varchar(249),
	model varchar(249),
	lokalizacja varchar(249),
	strategiczna bit,
	zdjecie varchar(249),
	typ varchar(249),
	opis varchar(249)
);

create table dsg_machine_tree_element_spare_element (
   part_id numeric(19, 0) NOT NULL,
   spare_id numeric(19, 0) NOT NULL,
   PRIMARY KEY (part_id,spare_id)
);


alter table dsg_machine_tree_element add discriminator varchar(100)
update dsg_machine_tree_element set discriminator=typ;


INSERT INTO [dbo].[dsg_machine_tree_element]
           ([lokalizacja] ,[typ] ,[discriminator]) VALUES ('GORZ�W_WLKP','LOC','LOC' )
INSERT INTO [dbo].[dsg_machine_tree_element]
           ([lokalizacja] ,[typ] ,[discriminator]) VALUES ('OLSZTYN','LOC','LOC' )
INSERT INTO [dbo].[dsg_machine_tree_element]
           ([lokalizacja] ,[typ] ,[discriminator]) VALUES ('GALEWICE','LOC','LOC' )