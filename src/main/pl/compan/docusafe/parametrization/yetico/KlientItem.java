package pl.compan.docusafe.parametrization.yetico;

public class KlientItem {

	String nazwa, miejscowosc, kraj, email;

	public KlientItem() {

	}

	public KlientItem(String nazwa, String miejscowosc, String kraj, String email) {
		super();
		this.nazwa = nazwa;
		this.miejscowosc = miejscowosc;
		this.kraj = kraj;
		this.email = email;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getMiejscowosc() {
		return miejscowosc;
	}

	public void setMiejscowosc(String miejscowosc) {
		this.miejscowosc = miejscowosc;
	}

	public String getKraj() {
		return kraj;
	}

	public void setKraj(String kraj) {
		this.kraj = kraj;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}



}
