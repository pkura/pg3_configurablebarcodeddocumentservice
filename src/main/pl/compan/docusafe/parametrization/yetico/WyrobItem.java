package pl.compan.docusafe.parametrization.yetico;

public class WyrobItem {

	String nazwa, typ, wymiary, nrFaktury;
	String lp;

	public WyrobItem() {

	}

	public WyrobItem(String nazwa, String typ, String wymiary, String nrFaktury) {
		this.nazwa = nazwa;
		this.typ = typ;
		this.wymiary = wymiary;
		this.nrFaktury = nrFaktury;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getWymiary() {
		return wymiary;
	}

	public void setWymiary(String wymiary) {
		this.wymiary = wymiary;
	}

	public String getNrFaktury() {
		return nrFaktury;
	}

	public void setNrFaktury(String nrFaktury) {
		this.nrFaktury = nrFaktury;
	}

	public String getLp() {
		return lp;
	}

	public void setLp(String lp) {
		this.lp = lp;
	}

}
