package pl.compan.docusafe.parametrization.pg;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.net.ssl.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.*;
import java.security.cert.CertificateException;
import java.sql.*;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: slim
 * Date: 22.09.13
 * Time: 17:26
 * To change this template use File | Settings | File Templates.
 */
public class CASApi {

    private static final Logger log = LoggerFactory.getLogger(CASApi.class);
    private static final String PG_ROOT_CA_KEYSOTRE= Docusafe.getAdditionProperty("cas.pg_root_ca_keystore");
    private static final String TRUSTED_CERT_KEYSTORE=Docusafe.getAdditionProperty("cas.trusted_cert_keystore");;
    private static final String CAS_API_PASSWORD=Docusafe.getAdditionProperty("cas.api_password");
    private static final String CAS_TRUSTED_KEYSTORE_PASSWORD=Docusafe.getAdditionProperty("cas.trusted.keystore_password");
    private static final String CAS_API_URL=Docusafe.getAdditionProperty("cas.api_url");
    private static final String NEW_USER_ROLE = "Nowy użytkownik";



    public static DSUser getDSUserByPersonalNumber(String callerPersonalNumber, String personalNumber)
    {

        try{
        XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
//            https://ip2-api.test.pg.gda.pl:8443/bp/xmlrpc
        config.setServerURL(
                new URL(CAS_API_URL));
        config.setEnabledForExtensions(true);
        XmlRpcClient client = new XmlRpcClient();
        client.setConfig(config);
//            keystore.jks
        File file = new File(PG_ROOT_CA_KEYSOTRE);
//            trusted.jks
        File trustFile = new File(TRUSTED_CERT_KEYSTORE);
//            changeit
        SSLContext sslc = initSSLContextFactory(file, trustFile, CAS_TRUSTED_KEYSTORE_PASSWORD);
        HttpsURLConnection.setDefaultSSLSocketFactory(sslc.getSocketFactory());
        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });
        Object[] params;


//            PersonAPI.getPersonData 528162 eod-app1.test.pg.gda.pl 291495
            params = new Object[] { callerPersonalNumber, CAS_API_PASSWORD, personalNumber};


        Object result = null;
        try {
            result = client.execute("PersonAPI.getPersonData", params);
        } catch (XmlRpcException e) {
            log.error(e.getMessage(), e);
        }
        System.out.println(result);
        if(result instanceof Object[])
        {
            Object[] res = (Object[]) result;

            String userName = null;
            String firstName = null;
            String lastName = null;
            String mail = null;

            for(int i=0; i<res.length; i++)
            {
//                System.out.println(i + ":" + res[i]);
                log.error("{FROM CAS}" + i + ":" + res[i]);
                switch(i)
                {
                    case 2:
                        {

                            if(res[2]!=null)
                                firstName=(String) res[2];
                            else
                                firstName="";
                            break;
                        }
                    case 3:
                        {
                            if(res[3]!=null)
                                lastName=(String) res[3];
                            else
                                lastName="";
                            break;
                        }
                    case 4:
                        {   /*
                            if(res[4]!=null)
                            {

                                mail=(String) res[4];
                                userName=((String)res[4]).split("@")[0];
                                log.error("CAS******************** userName=" + userName);
                            }
                            else
                            {  */
                                log.error("CAS******************** userName=" + userName);
                                Object result2 = null;
                                try {
                                    result2 = client.execute("PersonAPI.getAccountNamesByPersonNumber", params);
                                } catch (XmlRpcException e) {
                                    log.error(e.getMessage(), e);
                                }
                                if(result2!=null)
                                {
                                    if(result2 instanceof Object[])
                                    {
                                        Object[] res2 = (Object[]) result2;
//                                        Object[] loginInfo = (Object[]) res2[1];
//                                        String login =(String) loginInfo[0];
                                        log.error("OD CASA res2[]="+res2);
                                        if(res2 instanceof Object[])
                                        {
                                            for(Object ob: res2)log.error("res2[i]="+ob);
                                        }
                                        Object[] loginy = (Object[]) res2[1];
                                        Object[] loginArr = (Object[]) loginy[0];
                                        String login =(String) loginArr[0];
                                        log.error("**********LOGIN z CAS:" + login);
                                        if(login!=null)
                                        {
                                            userName=login;
                                            log.error("LOGIN Z CAS="+login);
                                            UserImpl newUser = (UserImpl) UserFactory.getInstance().createUser(login, firstName, lastName);
                                            log.error("LOGIN Z DOCUSAFE="+newUser.getName());
                                            newUser.setEmail(mail);
                                            newUser.setExternalName(callerPersonalNumber);
                                            newUser.setCtime(new Timestamp(new java.util.Date().getTime()));

                                            Role r = Role.findByName(NEW_USER_ROLE);

                                            newUser.addRole(NEW_USER_ROLE);

                                            return newUser;
                                        }
                                    }
                                }
                            }
                            break;
//                        }
                }
            }

        }
        }catch(Exception e)
        {
               log.error(e.getMessage(), e);
        }
        finally
        {
//            DSApi._close();
        }
        return null;
    }

    private static SSLContext initSSLContextFactory(File pKeyFile,
                                                    File trustFile,
                                                    String pKeyPassword)
            throws NoSuchAlgorithmException, KeyStoreException,
            CertificateException, IOException,
            UnrecoverableKeyException, KeyManagementException {
        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
//===============================
        KeyStore keyStore = KeyStore.getInstance("JKS");
        InputStream keyInput = new FileInputStream(pKeyFile);
        keyStore.load(keyInput, pKeyPassword.toCharArray());
        keyInput.close();
//        Key key = keyStore.getKey("1", "changeit".toCharArray());
        kmf.init(keyStore, pKeyPassword.toCharArray());
//==============================
        KeyStore trustStore = KeyStore.getInstance("JKS");
        InputStream trustInput = new FileInputStream(trustFile);
        trustStore.load(trustInput, "changeit".toCharArray());
        trustInput.close();
        tmf.init(trustStore);
//=====================================
        SSLContext context = SSLContext.getInstance("TLS");
        context.init(kmf.getKeyManagers(),
                tmf.getTrustManagers(), new SecureRandom());
        return context;
    }
}
