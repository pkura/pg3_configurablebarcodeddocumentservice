package pl.compan.docusafe.parametrization.pg.integration;

import com.google.common.collect.Lists;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import net.sourceforge.rtf.util.StringUtils;
import org.springframework.util.CollectionUtils;
import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.api.user.DivisionDto;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.UserService;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.NewAvailabilityManager;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.docusafe.pg.model.Field;
import pl.docusafe.pg.model.Person;
import pl.docusafe.pg.model.Unit;
import pl.docusafe.pg.service.RPCDivisionService;
import pl.docusafe.pg.service.RPCRequestService;
import pl.docusafe.pg.service.RPCResponseService;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Klasa s�u��ca do opakowania konfiguracji po��czenia z Moja PG
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class IntegrationFactory {

    public static final Logger log = LoggerFactory.getLogger(IntegrationFactory.class);
    public List<Person> searchPersons() throws Exception {
        List<String>[] order = getOrder();

        return searchPersons(order);
    }

    protected ArrayList[] getOrder() {
        return new ArrayList[] {
                Lists.newArrayList("person.lastName", "true"),
                Lists.newArrayList("person.firstName", "true")
        };
    }

    public Person searchPersonByNumber(String number) throws Exception{

        RPCRequestService request = createRPCRequestService();

        request.setColumns( getPersonColumnSearch());
        request.addOrder(getOrder());
        request.getFields().addField(new Field("person.personNumber", "=", number));
        request.setWhereOperator(getPersonFieldWhereOperatorSearch());

        JSONRPC2Response personList = request.searchPersonMethod();
        JSONRPC2Response countMethodResult = request.countPersonMethod();
        System.out.println(countMethodResult.getResult().toString());
        List<Person> result = RPCResponseService.parsePersonList(personList);
        if(!CollectionUtils.isEmpty(result))
            return result.iterator().next();
        else
            return null;
    }
    /**
     * request.setColumns("person.student.albumNumber", "person.firstName", "person.lastName");
     request.addOrder(Lists.newArrayList("person.lastName", "true"),
     Lists.newArrayList("person.firstName", "true"));
     request.addField("lower(person.lastName)", "like", "'%kowal%'");
     * @param columns
     * @param field
     * @param whereOperator AND/OR
     * @param order
     * @return
     * @throws Exception
     */
    public List<Person> searchPersons(List<String>... order ) throws Exception {
        RPCRequestService request = createRPCRequestService();

        request.setColumns( getPersonColumnSearch());
        request.addOrder(order);
        request.getFields().setFields( getPersonFieldSearch());
        request.setWhereOperator(getPersonFieldWhereOperatorSearch());

        JSONRPC2Response personList = request.searchPersonMethod();
        JSONRPC2Response countMethodResult = request.countPersonMethod();
        System.out.println(countMethodResult.getResult().toString());
        List<Person> result = RPCResponseService.parsePersonList(personList);
        return result;
    }

    public List<Person> getDivisionManager(String unitSymbol) throws Exception {
        RPCRequestService request = createRPCRequestService();
        JSONRPC2Response response = request.searchPersonAsManager(unitSymbol);
        List<Person> persons = RPCResponseService.parsePersonList(response);
        return persons;
    }

    /**
     * Tworzy obiekt requestu
     * @return
     * @throws MalformedURLException
     */
    private RPCRequestService createRPCRequestService() throws MalformedURLException {
        return new RPCRequestService(
                    getCallerPN(),
                    getPassword(),
                    getUrl(),
                    getFile(),
                    getTrustedFile(),
                getKeystoreClientName(),
                getKeystoreClientPass()
            );
    }

    private String getCallerPN() {
        String callerPN = AdditionManager.getProperty("pg.integration.callerPN");
        return callerPN.equals("") ? null : callerPN;
    }

    private String getPassword() {
        return AdditionManager.getProperty("pg.integration.password");
    }

    private URL getUrl() throws MalformedURLException {
        return new URL(AdditionManager.getProperty("pg.integration.url"));
    }

    private String getKeystoreClientName(){
        return AdditionManager.getProperty("pg.integration.keystoreClientName");
    }

    private String getKeystoreClientPass(){
        return AdditionManager.getProperty("pg.integration.keysoreClientPass");
    }

    public UserDto convertToUserDto(Person p) {
        UserDto userDto = new UserDto();
        userDto.setEmail(p.getEmail());
        userDto.setFirstname(p.getFirstName());
        userDto.setLastname(p.getLastName());
        userDto.setPersonNumber(p.getPersonNumber() == null ? null: String.valueOf(p.getPersonNumber()));
        userDto.setUsername(p.getPersonNumber() == null ? null: String.valueOf(p.getPersonNumber()));
        return userDto;
    }

    public DSUser createDSUser(String username) throws Exception{

        Person person = searchPersonByNumber(username);
        UserDto userDto = convertToUserDto(person);
        UserService bean = Docusafe.getBean(UserService.class);
        long l = bean.create(userDto);
        return DSUser.findById(l);
    }

    private File getFile() {
        return new File(Docusafe.getHome() + "/pg_integration/client_keycert.jks");
    }

    private File getTrustedFile() {
        return new File(Docusafe.getHome() + "/pg_integration/PGRootCA.jks");
    }

    public String[] getPersonColumnSearch()
    {
        List<String> pps = AdditionManager.getPropertyAsList("pg.integration.person.column.search");
        return pps.toArray(new String[pps.size()]);
    }

    public List<Field> getPersonFieldSearch()
    {
        List<Field> fields = Lists.newArrayList();
        List<String> pps = AdditionManager.getPropertyAsList("pg.integration.person.field.search");

        for(String p : pps ){
            String[] splited = StringUtils.split(p.trim(), " ");
            if(splited.length == 3 ){
                fields.add(new Field(splited[0],splited[1],splited[2]));
            } else if(splited.length == 4 ) {
                fields.add(new Field(splited[0],splited[1],splited[2] + " " +splited[3]));
            }
        }
        return fields;
    }

    private String getPersonFieldWhereOperatorSearch() throws MalformedURLException {
        return AdditionManager.getProperty("pg.integration.person.fieldWhereOperator.search");
    }

    /**
     * Buduje struktur� drzewa
     * @return
     */
    public Unit[] buildDivisionsTree() throws Exception {
            RPCDivisionService divService = new RPCDivisionService(
                    getCallerPN(),
                    getPassword(),
                    getUrl(),
                    getFile(),
                    getTrustedFile(),
                    getKeystoreClientName(),
                    getKeystoreClientPass()
            );
            JSONRPC2Response response = divService.searchAdministrativeUnits();
            return RPCResponseService.parseUnitList(response);
    }
}
