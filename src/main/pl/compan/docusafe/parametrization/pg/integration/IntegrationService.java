package pl.compan.docusafe.parametrization.pg.integration;

import com.beust.jcommander.internal.Maps;
import com.beust.jcommander.internal.Sets;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import org.springframework.util.CollectionUtils;
import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.api.user.*;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DuplicateNameException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSDivisionFilter;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.rest.ResourceNotFoundException;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.docusafe.pg.factory.PersonFactory;
import pl.docusafe.pg.model.*;

import javax.annotation.Nullable;
import java.util.*;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class IntegrationService extends ServiceDriver implements Service {

    public static final Logger log = LoggerFactory.getLogger(IntegrationService.class);
    private Timer timer;
    private int period = AdditionManager.getIntPropertyOrDefault("pg.integration.period", 1);
    IntegrationFactory factory = new IntegrationFactory();

    public static final String KIEROWNIK_GUID = "Kierownik";

    @Override
    protected void start() throws ServiceException {
        log.info("start uslugi IntegrationService");
        timer = new Timer(true);
        timer.schedule(new IntegrationUserImport(), (long) 2 * 1000, (long) period * DateUtils.MINUTE);
        console(Console.INFO, "Start us�ugi integracji z moje-pg");
    }

    @Override
    protected void stop() throws ServiceException {
        log.info("stop uslugi");
        console(Console.INFO, "Stop Us�ugi");
        if(timer != null) timer.cancel();
    }

    @Override
    protected boolean canStop() {
        return false;
    }

    private class IntegrationUserImport extends TimerTask {

        @Override
        public void run() {

            try {
                DSApi.openAdmin();

                createDivisionTree();
                List<Person> persons = factory.searchPersons();
                List<UserDto> userDtos = convertPersonToUserDto(persons);
                saveUsers(userDtos);
                saveUsersToDivisions(persons);

            } catch(Exception e){
                log.error("", e);
                console(Console.ERROR, "B��d [ " + e + " ]");
            } finally {
                log.trace("IntegrationUserImport  STOP");
                console(Console.INFO, "Koniec synchronizacji IntegrationUserImport");
                DSApi._close();
            }
        }

        private void saveUsers(List<UserDto> userDtos) {
            try {
                int pack = 50;


                DSApi.context().begin();
                for(int i = 0 ; i < userDtos.size(); i++){

                    UserDto userDto = userDtos.get(i);
                    UserService bean = Docusafe.getBean(UserService.class);
                    DSUser user = null;
                    try {
                        user = DSUser.findByExternalName(userDto.getPersonNumber());
                    }catch(UserNotFoundException e){
                        log.trace("->", e.getMessage());
                    }

                    if(user != null){
                        userDto.setId(user.getId());
                        bean.update(userDto);
                        log.debug("update {}", i);
                    } else {
                        bean.create(userDto);
                        log.debug("save {}", i);
                    }

                    if(i % pack == 0 && i != 0){
                        DSApi.context().session().flush();
                        log.debug("robi� flusha");
                        DSApi.context().commit();
                        DSApi.context().begin();
                    }
                }

                DSApi.context().commit();


            } catch(Exception e){
                log.error("", e);
                console(Console.ERROR, "B��d [ " + e + " ]");
                DSApi.context()._rollback();
            }

        }

        protected List<UserDto> convertPersonToUserDto(List<Person> persons) {
            log.debug("person. lists size {}", persons.size());
            List<UserDto> usersDtos = Lists.newArrayList();

            for(Person p : persons) {
                UserDto userDto = factory.convertToUserDto(p);
                usersDtos.add(userDto);
            }

            log.debug("userDtos list size {}", usersDtos.size());
            return usersDtos;
        }

        protected void createDivisionTree() throws Exception {
            Unit[] units = factory.buildDivisionsTree();
            log.debug("integrationDivisions size {}", units.length);
            Long parentId = DSDivision.find(DSDivision.ROOT_GUID).getId();
            for(Unit unit : units) {
                try {
                    DSApi.context().begin();
                    saveUnitWithManager(unit, parentId);
                    DSApi.context().commit();
                }catch(Exception e){
                    log.error("", e);
                    DSApi.context()._rollback();
                }
            }
        }

        private long saveUnitWithManager(Unit unit, Long parentId) throws Exception{
            log.debug("iteracja {} {} {}", unit.getId(), unit.getName(), unit.getSymbol());
            DivisionDto divisionDto = convertToDivisionDto(unit, parentId);
            DivisionService divisionService = Docusafe.getBean(DivisionService.class);
            Optional<DivisionDto> division = divisionService.getByGuid(DSDivision.GUID_PREFIX + unit.getId());
            Long divId = null;

            if(division.isPresent()){
                divId = division.get().getId();
                divisionDto.setId(divId);
                divisionService.update(divisionDto);
                log.debug("update unit");
            } else {
                divId = divisionService.create(divisionDto);
                log.debug("save unit");
            }

            saveManager(unit, divId);

            if(!CollectionUtils.isEmpty(unit.getChilds())) {
                for(Unit child : unit.getChilds()){
                    saveUnitWithManager(child, divId);
                }
            }


            return divId;
        }

        private void saveManager(Unit unit, long divId) throws Exception {
            //@TODO pobra� kierownika dla danego dzia�u i utworzy� poddzia� Kierownik
            DSDivision division = DSDivision.findById(divId);

            DSDivision kierownikDivision = null;

            if(division != null) {
                try {
                    kierownikDivision = division.createDivision(KIEROWNIK_GUID, null);
                }catch (DuplicateNameException e){
                    log.debug("dzial Kierownik istnieje dla {}", divId);
                    kierownikDivision = division.getChildren(new DSDivisionFilter() {
                        @Override
                        public boolean accept(DSDivision division) {
                            return division.getName().equals(KIEROWNIK_GUID);
                        }
                    })[0];
                }
            }

            if(kierownikDivision != null){
                log.debug("aktualizacja kierownika dzia�u");
                List<Person> manager = factory.getDivisionManager(unit.getSymbol());
                DSUser[] users = kierownikDivision.getUsers();
                Set<String> actualUser = Sets.newHashSet();

                if(users != null)
                    for(DSUser u : users) {
                        actualUser.add(u.getExternalName());
                    }

                if(manager != null)
                    for(Person p : manager) {
                        //zawiera u�ytkownika
                        if(actualUser.contains(p.getPersonNumber().toString())){
                            //nic nie robi� ju� jest, usuwam go z listy
                            actualUser.remove(p.getPersonNumber().toString());
                            log.debug("remove kierownik juz jest");
                        } else {
                            //dodaje u�ytkownika do dzia�u
                            DSUser byExternalName = null;
                            try {
                                 byExternalName = DSUser.findByExternalName(p.getPersonNumber().toString());
                            }catch(UserNotFoundException e){
                                log.info("pobieram usera {} bo nie ma ", p.getPersonNumber());
                                UserService bean = Docusafe.getBean(UserService.class);
                                long userId = bean.create(factory.convertToUserDto(p));
                                byExternalName = DSUser.findById(userId);
                            }

                            kierownikDivision.addUser(byExternalName);
                        }
                    }

                //pozosta�ych z setu usuwam z dzia�u
                if(actualUser != null)
                    for(String name : actualUser) {
                        kierownikDivision.removeUser(DSUser.findByExternalName(name));
                        log.debug("usuwam usera z dzia�u {} {},", name, kierownikDivision.getId());
                    }
            }

        }

        /**
         *
         * @param unit slownik zewn�trzny
         * @param localParentId id z naszej bazy
         * @return
         */
        private DivisionDto convertToDivisionDto(Unit unit, Long localParentId) {
            DivisionDto divDto = new DivisionDto();
            divDto.setName(unit.getName());
            divDto.setCode(unit.getSymbol());
            divDto.setGuid(DSDivision.GUID_PREFIX + unit.getId());
            //ewentualnie robi� tutaj przeszukanie.
            divDto.setParentId(localParentId);
            divDto.setType(DivisionType.DIVISION);
            return divDto;

        }

        /**
         * Zapisuje u�ytkownik�w do danego dzia�u
         */
        private void saveUsersToDivisions(List<Person> persons) throws Exception {
            Map<Long, Set<String>> persontoDivision = mapPersonToDivision(persons);


            saveUserDivisions(persontoDivision);

        }

        private void saveUserDivisions(Map<Long, Set<String>> persontoDivision) throws EdmException {
            for(Map.Entry<Long, Set<String>> entry : persontoDivision.entrySet()){
                try {
                    DSApi.context().begin();
                    saveUserDiv(entry);
                    DSApi.context().commit();
                }catch(Exception e){
                    log.error("", e);
                    DSApi.context()._rollback();
                }

            }
        }

        private void saveUserDiv(Map.Entry<Long, Set<String>> entry) throws EdmException {
            DivisionService divisionService = Docusafe.getBean(DivisionService.class);
            DSUser user = DSUser.findByExternalName(entry.getKey().toString());
            Set<String> userActualDivisions = new HashSet<String>(Lists.newArrayList(user.getDivisionsGuid()));
            Set<String> userNewDivisions = entry.getValue();

            log.debug("{} userActualDivision {}", user.getFirstname() + " " + user.getLastname(), userActualDivisions );
            log.debug("{} userNewDivision {}", user.getFirstname() + " " + user.getLastname(), userNewDivisions );

            for(String newDivision : userNewDivisions){
                // jesli dzial jest w aktualnych usun z listy
                if(userActualDivisions.contains(DSDivision.GUID_PREFIX + newDivision)) {
                    continue;
                } else { //jesli nie ma w aktualnych dodaj do nowych
                    divisionService.addUser(DSDivision.GUID_PREFIX + newDivision, user.getExternalName());
                }
            }

            for(String oldDivision : userActualDivisions) {
                //jesli stary dzia� jest w nowych
                String[] split = oldDivision.split("_");
                if(userNewDivisions.contains(split.length > 1 ? split[1]: oldDivision)) {
                    continue;
                } else { //jesli nie ma w nowych usun
                    if(oldDivision.startsWith("GUID_"))
                        divisionService.removeUser(oldDivision, user.getExternalName());
                }

            }
        }

        /**
         * Mapuje na mape externalName -> id dzia��w (jest to przysz�y guid )
         * @param persons
         * @return
         */
        private Map<Long, Set<String>> mapPersonToDivision(List<Person> persons) {
            Map<Long, Set<String>> personToDivision = Maps.newLinkedHashMap();
            for(Person p : persons) {
                log.debug("dodawanie do grup personNumber {} ", p.getPersonNumber());
                Set<Unit> divisions = PersonFactory.getDivisions(p);
                Collection<String> divisionsIds = Collections2.transform(divisions, new Function<Unit, String>() {
                    @Nullable
                    @Override
                    public String apply(Unit input) {
                        return "" + input.getId();
                    }
                });
                 personToDivision.put(p.getPersonNumber(), new HashSet<String>(divisionsIds));
            }
            return personToDivision;
        }

    }
}
