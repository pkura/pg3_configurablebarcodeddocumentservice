package pl.compan.docusafe.parametrization.pg;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: slim
 * Date: 27.08.13
 * Time: 14:15
 * To change this template use File | Settings | File Templates.
 */
public class BarcodeOrderLogic extends AbstractDocumentLogic {
    private static Logger log = LoggerFactory.getLogger(BarcodeOrderLogic.class);


    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        super.setInitialValues(fm, type);
        Map<String, Object> toReload = new HashMap<String, Object>();
        if ("barcode_order".equals(fm.getDocumentKind().getCn())) {
            toReload.put("STATUS", 10);
            toReload.put("DOC_DATE", new Date());
            DSUser user = DSApi.context().getDSUser();
            toReload.put("USER_AUTHOR", user.getId());
        }
        fm.reloadValues(toReload);
    }


    public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException
    {
        if(acceptationCn.equals("author_confirm"))
        {
            FieldsManager fm = doc.getFieldsManager();
//            String firstGenerated = (String) fieldValues.get("START_BARCODE");
//            String firstPrinted = (String) fieldValues.get("START_BARCODE_CHECK");
            String firstGenerated = (String) fm.getFieldValues().get("START_BARCODE");
            String firstPrinted = (String) fm.getFieldValues().get("START_BARCODE_CHECK");
            if(firstGenerated!=null && firstPrinted!=null)
            {
                if(!firstGenerated.equals(firstPrinted))
                {
                    throw new EdmException("Nie mo�na potwierdzi�! Wprowadzony kod jest r�ny od wygenerowanego.");
                }
            }
            else
            {
//                throw new EdmException("Brak pierwszego i/lub drugiego barkodu!");

            }
        }
    }

    public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException {
        boolean isOpened = true;
        String message = null;
        try{
/*
            Statement st2 = DSApi.context().createStatement();
            String query = "select * from dsg_pg_barcodes_check where document_id="+documentId;
            ResultSet rs2 = st2.executeQuery(query);
            int check = 0;
            while(rs2.next())
            {
                Object test =rs2.getObject(1);
                check =rs2.getInt(2);
                String stop="sdf";
            }
            rs2.close();
            st2.close();*/
            Object stat = fieldValues.get("STATUS");
            Integer status = null;
            if(stat instanceof String){
                status = Integer.parseInt((String) stat);
            }
            else if(stat instanceof Integer)
            {
                status = (Integer) stat;
            }

            if(status.intValue()==10)
            {
                Integer userId = (Integer) fieldValues.get("USER_AUTHOR");
                OfficeDocument.find(documentId).setAuthor(DSUser.findById(userId.longValue()).getName());
                Integer ilosc = (Integer) fieldValues.get("ILOSC");

                int limit = Integer.parseInt(Docusafe.getAdditionProperty("barcode.limit_value"));
                int pointer=0;
                Statement st3 = DSApi.context().createStatement();
                ResultSet rs3 = st3.executeQuery("select * from dsg_pg_barcode_generator");
                while(rs3.next())
                {
                    pointer = rs3.getInt("pointer_value");
                }
                rs3.close();
                st3.close();

                if(limit-pointer<ilosc) message="Pula numer�w na kody zosta�a wyczerpana. Prosimy o kontakt z Administratorem.";
                else{
                Date date = (Date) fieldValues.get("DOC_DATE");

                BarcodePool pula = BarcodeGenerator.getInstance().generateBarcodePool(ilosc, date, userId.longValue(), documentId);

                DSApi.context().session().save(pula);
                Statement st = DSApi.context().createStatement();
                int endval = pula.getEnd();
                String sql = "update dsg_pg_barcode_generator set pointer_value=" + (endval + 1);
                int testSt = st.executeUpdate(sql);
                }
            }
            if(status.intValue()==1000 || status.intValue()==2000)
            {
             }
            if(status.intValue()==40)
            {
                String firstGenerated = (String) fieldValues.get("START_BARCODE");
                String firstPrinted = (String) fieldValues.get("START_BARCODE_CHECK");
              if(firstGenerated!=null && firstPrinted!=null)
              {
                  if(!firstGenerated.equals(firstPrinted))
                    {
                        message="Nie mo�na potwierdzi�! Wprowadzony kod jest r�ny od wygenerowanego.";
                    }
                }
            }
        }
        catch(Exception e)
        {
            log.error(e.getMessage(), e);

        }
        if(message!=null) throw new EdmException(message);
    }

    public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) {

        StringBuilder msgBuilder = new StringBuilder();

        try {
//            DSApi.openContextIfNeeded();

            List status = values.get("DWR_STATUS").getEnumValuesData().getSelectedOptions();
            Integer stat = Integer.parseInt((String) status.get(0));

            if (values.containsKey("DWR_ILOSC") && values.get("DWR_ILOSC").getEnumValuesData().getSelectedValue() != null && stat.intValue()==10){

                Object obj = values.get("DWR_ILOSC");

                FieldData fd = (FieldData) obj;
                EnumValues ev = fd.getEnumValuesData();
                List<String> bbb = ev.getSelectedOptions();
                int ilosc = Integer.parseInt(bbb.get(0));
                Long userId = new Long((Integer) fm.getKey("USER_AUTHOR"));
                BarcodePool pula = BarcodeGenerator.getInstance().generateBarcodePool(ilosc, new Date(), userId, fm.getDocumentId());
                if(pula!=null)
                {
                    List<String> lista = pula.getBarcodes();
                    values.put("DWR_START_BARCODE", new FieldData(Field.Type.STRING, lista.get(0)));
                    values.put("DWR_END_BARCODE", new FieldData(Field.Type.STRING, lista.get(lista.size() - 1)));
                }
                else msgBuilder.append("Pula numer�w na kody zosta�a wyczerpana. Prosimy o kontakt z Administratorem.");
            }
            if (values.containsKey("DWR_CPP") && values.get("DWR_CPP").getEnumValuesData().getSelectedValue() != null )
            {

                String value = values.get("DWR_CPP").getEnumValuesData().getSelectedValue();
                List lista = values.get("DWR_CPP").getEnumValuesData().getSelectedOptions();
                String value2 = values.get("DWR_CPP").getEnumValuesData().getSelectedOptions().get(0);
                Map<String, Object> fieldValues = new HashMap<String, Object>();
                fieldValues.put("CPP", value2);
                DSApi.openAdmin();
                fm.getDocumentKind().setOnly(fm.getDocumentId(), fieldValues);
                DSApi.close();
            }
            if(stat.intValue()==40)
            {
//                String firstGenerated = values.get("DWR_START_BARCODE").getStringData();
//                String firstPrinted = values.get("DWR_START_BARCODE_CHECK").getStringData();
//                if(firstGenerated!=null && firstPrinted!=null)
//                {
//                    if(!firstGenerated.equals(firstPrinted))
//                    {
//                        Map<String, Object> fieldValues = new HashMap<String, Object>();
//                        fieldValues.put("BARCODES_CHECK", new Boolean(false));
//                        DSApi.openAdmin();
//                        fm.getDocumentKind().setOnly(fm.getDocumentId(), fieldValues);
//                        DSApi.close();
//                        throw new EdmException("Nie mo�na potwierdzi�! Wprowadzony kod jest r�ny od wygenerowanego.");
//
//                    }
//                }
//                else
//                {
//                }
            }
        } catch (Exception e) {
            msgBuilder.append(e.getMessage());
        } finally {
            try {
//                DSApi.close();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }

        if (msgBuilder.length() > 0) {
            values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
            return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
        }

        return null;
    }

    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable,
                            OpenExecution openExecution, String acceptationCn,
                            String fieldCnValue) {
        try {
            if (acceptationCn.equals("cpp_accept") || acceptationCn.equals("cpp_print")) {
                FieldsManager fm = doc.getFieldsManager();
                Object obj = fm.getKey("CPP_DIVISION");
                Integer divId = (Integer) obj;
                String guid = DSDivision.findById(divId).getGuid();
                assignable.addCandidateGroup(guid);
                return true;
            }
            if (acceptationCn.equals("author"))
            {
                FieldsManager fm = doc.getFieldsManager();
                Long userId = new Long((Integer) fm.getKey("USER_AUTHOR"));
                assignable.addCandidateUser(DSUser.findById(userId).getName());
//                Map<String, Object> valuesToReload = new HashMap<String, Object>();
//                valuesToReload.put("BARCODES_CHECK", new Boolean(true));
//                fm.reloadValues(valuesToReload);


//                Map<String, Object> fieldValues = doc.getFieldsManager().getFieldValues();
//                fieldValues.put("BARCODES_CHECK", new Boolean(true));
//                DSApi.openAdmin();
//                fm.getDocumentKind().setOnly(fm.getDocumentId(), fieldValues);
//                DSApi.close();

                PreparedStatement sttt = DSApi.context().prepareStatement("insert into dsg_pg_barcodes_check values(?,?)");
//                String query = "insert into dsg_pg_barcodes_check values(" + doc.getId() + ", " + "1);";
                sttt.setLong(1, doc.getId().longValue());
                sttt.setLong(2, 1L);
                int result = sttt.executeUpdate();
                sttt.close();
                return true;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    @Override
    public boolean canFinishProcess(OfficeDocument document) throws AccessDeniedException, EdmException
    {
        String x = "tu jestem";
        String xx = "tu jestem2";
        return false;
    }
    @Override
    public void onEndProcess(OfficeDocument document, boolean isManual) throws EdmException
    {
        String t="tu jestem";
        throw new EdmException("NIE!");
    }
}

