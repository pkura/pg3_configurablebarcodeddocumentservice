package pl.compan.docusafe.parametrization.pg;


import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.alfresco.AlfrescoApi;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.office.common.DwrDocumentHelper;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.Maps;

import edu.emory.mathcs.backport.java.util.Arrays;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.*;

/**
 * <h1>PgDocOutLogic</h1>
 *
 * <p>
 *     Klasa s逝篡 do obs逝gi zachowania/implementaji funkcjonalno軼i pism wychodz帷ych z systemu.
 * </p>
 */

@SuppressWarnings("serial")
public class PgDocOutLogic extends AbstractPgLogic
{
	private static PgDocOutLogic instance;
	protected static Logger log = LoggerFactory.getLogger(PgDocOutLogic.class);
	protected static DwrDocumentHelper dwrDocumentHelper = new DwrDocumentHelper();
	private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
	 
	public static PgDocOutLogic getInstance()
	{
		if (instance == null)
			instance = new PgDocOutLogic();
		return instance;
	}


	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	//nowa kolumna na liscie zadan 
	public TaskListParams getTaskListParams(DocumentKind dockind, long id)	throws EdmException {
		TaskListParams params = new TaskListParams();

		try {
			
			} catch (Exception e) {
			log.error("", e);
		}
		return params;
	}
	
	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		Field msg = null;
		if (values.get(_dataDokumentuDwr) != null && values.get(_dataDokumentuDwr).getData() != null)
		{
			Date startDate = new Date();
			Date finishDate = values.get(_dataDokumentuDwr).getDateData();
			
			if (finishDate.after(startDate))
			{
				msg = new Field("messageField", "Data pisma nie mo瞠 by� dat� z przysz這�ci.", Field.Type.BOOLEAN);
				values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
			}
		
		}
		if (values.get(czystopisDwr)!=null
				&& (values.get(czystopisDwr).getEnumValuesData().getSelectedId().contains("1")))
				msg = new Field("messageField", "W pismie oznaczonym jako czystpis nie mo積a dodawa� odbiorc闚", Field.Type.BOOLEAN);

		if(AvailabilityManager.isAvailable("PgValidateBarcode")){
			msg = validateBarcode(values, msg);
		}
		
		return msg;
	}

	@Override
	public  void onLoadData(FieldsManager fm) throws EdmException {

		if(!DSApi.context().hasPermission(DSPermission.PISMO_WYCH_DOSTEP)) {
			fm.getField("DOSTEP").setHidden(true);
		}else {
			fm.getField("DOSTEP").setHidden(false);
		}

	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{

		log.info("type: {}", type);
		fm.getField("JOURNAL").setHidden(true);
		fm.getField("DOC_REFERENT").setHidden(true);

		Map<String, Object> values= new HashMap<String, Object>();
		values.put("TYPE", type);
		values.put(_dataDokumentu, new Date());
		values.put("DOC_BARCODE", "12345");
        try {
            generateBarcode(fm, values);
        } catch (Exception e) {
            throw new EdmException("B陰d przy generowaniu nowego identyfikatora dokumentu", e);
        }

		//values.put(czystopis,2);
	//	values.put(_nadawca, Long.parseLong(Docusafe.getAdditionProperty("DomyslnyOdbiorcaInNadawcaOut")));
	//	fillCreatingUser(values);
		fm.reloadValues(values);
			
	}
	public boolean searchCheckPermissions(Map<String,Object> values)
    {
        return true;
    }
	
	/**
	 * Metoda jest wykorzystywana przy generowaniu dokumentu z szablonu (rtf).
	 * Ustawia dodatkowe pola.
	 * 
	 * @param docId - id dokumentu
	 * @param values - mapa warto軼i
	 */
	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		
	}
	
	
	/**
	 * Metoda ustawia warto軼i domy郵ne na dokumencie wychodz帷ym, kt鏎e jest pismem odpowiedzi� na inne pismo
	 * 
	 * @param inDocumentId - id dokumentu
	 */
	public Map<String, String> setInitialValuesForReplies(Long inDocumentId) throws EdmException
	{
		Map<String, String> dependsDocs = new HashMap<String, String>();
		OfficeDocument doc =  OfficeDocument.find(inDocumentId);
		if(doc instanceof InOfficeDocument && InOfficeDocumentDelivery.DELIVERY_EPUAP.contains(((InOfficeDocument)doc).getDelivery().getName())){
			dependsDocs.put("RECIPIENT", doc.getSender().getBasePersonId().toString());
			dependsDocs.put(DOC_DESCRIPTION_CN,  "Odpowiedz na pismo przychodzace z ePUAP o numerze KO :"+doc.getOfficeNumber().toString());
			dependsDocs.put(DOC_DELIVERY_CN,OutOfficeDocumentDelivery.findByName("ePUAP").getId().toString());
		}
		
		
		return dependsDocs;
	}

    /**
     * Mechanizm wykonywany po wcisnieciu przycisku wyslij do zewn皻rzengo repozytorium.
     * @param fieldsManager
     * @param property
     * @return
     * @throws EdmException
     */
    @Override
    public boolean sendToExternaRepostiory(FieldsManager fieldsManager, String property) throws EdmException {
        boolean ok = false;
        AlfrescoApi alfrescoApi = new AlfrescoApi();
        alfrescoApi.connect();
        try{
            Document document = Document.find(fieldsManager.getDocumentId());
            log.info("zapisuje do folderu {}", property);
            if(document.getAttachments() == null || document.getAttachments().isEmpty()){
                //wysylamy tylko metryke
                alfrescoApi.createDocument(document, null, property);
                ok=true;
            } else {
                for(Attachment att : document.getAttachments() ){
                    alfrescoApi.createDocument(document, att, property);
                    //zapisa� uuid na metryk�
                    ok = true;
                }
            }
        }catch(Exception e){
            log.error("",e);
            throw new EdmException("Nie uda這 si� zapisac dokumentu w systemie zewn皻rznym!");
        }finally{
            alfrescoApi.disconnect();
        }
        return ok;
    }

	/**
	 * Metoda wywo造wana w trakcie wykonania akcji zwi您anych z archiwizacj� dokumentu.
	 * Wi捫e dokument z akcjami archiwalnymi
	 * 
	 * @param document - obj. dokumentu
	 * @param type - typ dokumentu (archiwalny,przychodz帷y,wychodz帷y,wewn皻rzny)
	 * @param as - obj. wykonuj帷y akcje archiwalne
	 * 
	 */
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		as.useFolderResolver(document);
		as.useAvailableProviders(document);
		log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
	}

	@Override
	public String onStartManualAssignment(OfficeDocument doc, String fromUsername, String toUsername)
	{
		log.info("--- PGDocOutLogic : onStartManualAssignment  dekretacja reczna dokument闚 :: dokument ID = "+doc.getId()+" : pomiedzy nadawc� -->"+fromUsername +" a odbiorc� ->"+toUsername );
		return toUsername;
	
	}

    /**
     * Metoda wywo造wana przy starcie procesu
     * 
     * @param document
     * @param event
     */
	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- PGDocOutLogic : START PROCESS !!! ---- {}", event);
		try
		{

			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, DSApi.context().getDSUser().getName());
			//map.put(ASSIGN_DIVISION_GUID_PARAM,event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			DSDivision[] divs = DSApi.context().getDSUser().getOriginalDivisionsWithoutGroup();
			if (divs.length > 0)
			{
				for (DSDivision d : divs)
				{
				map.put(ASSIGN_DIVISION_GUID_PARAM, d.getGuid() != null ? d.getGuid() : DSDivision.ROOT_GUID);
				}
			} else
				map.put(ASSIGN_DIVISION_GUID_PARAM, DSDivision.ROOT_GUID);

			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));

		} catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}


	}
	 @Override
	public void updateDocumentBarcodeVersion(Document document) throws Exception
	{
			updateBarcodeVersion(document);
	}
	@Override
	public void validate(Map<String, Object> values, DocumentKind kind,Long documentId) throws EdmException {
	
		System.out.println(kind.getCn());
		System.out.println(values);
		//if (documentId!=null)
		//	validateRecipientInDocument(values, documentId);
	
	}
	/**
	 * 
	 * Metoda ustawia dzia� w kt鏎ym jest u篡tkownik
	 * 
	 * @param values
	 * @throws UserNotFoundException
	 * @throws EdmException
	 */
	private void fillCreatingUser(Map<String, Object> values) throws UserNotFoundException, EdmException
	{
		Long userDivision = (long) 1;
		DSUser user = DSApi.context().getDSUser();

		DSDivision[] divisions = user.getOriginalDivisions();

		for (int i = 0; i < divisions.length; i++)
		{
			if (divisions[i].isPosition())
			{
				
				DSDivision parent = divisions[i].getParent();
				if (parent != null)
				{
					userDivision = parent.getId();
					break;
				} else
				{
					userDivision = divisions[i].getId();
				}
			} else if (divisions[i].isDivision())
			{
				userDivision = divisions[i].getId();
				break;
			}
		}
	//	values.put(_USER, DSApi.context().getDSUser().getId());
	//	values.put(_DIVISION, userDivision);
		
	}
	private Recipient utworzRecipient(Person person)
	{
	Recipient recipient = new Recipient();
	
	try
	{
		recipient.fromMap(person.toMap());
		recipient.setBasePersonId(person.getId());
		recipient.create();
		
	} catch (EdmException e)
	{
		log.error("", e);
	}
	
	return recipient;
		
	}

	/**
	 * 
	 * Metoda usuwa redundancje odbiorc闚
	 * 
	 * @param recipientsToRemove
	 */
	private static void removeDuplicates(List<Long> recipientsToRemove) {
        HashSet set = new HashSet(recipientsToRemove);
        recipientsToRemove.clear();
        recipientsToRemove.addAll(set);
}

	/**
	 * 
	 * Metoda sprawdza validuje odbiorc闚
	 * 
	 * @param values
	 * @param documentId
	 * @throws DocumentNotFoundException
	 * @throws EdmException
	 */
	private void validateRecipientInDocument(Map<String, Object> values,
			Long documentId) throws DocumentNotFoundException, EdmException {

		OutOfficeDocument doc = null;
		doc = (OutOfficeDocument) OutOfficeDocument.find(documentId);
		List<Long> selectetPersonIds = new ArrayList();
		Map<String, FieldData> odbiorcy = (Map<String, FieldData>) values.get(multiOdbiorca);
		if (odbiorcy != null) {
			for (String key : odbiorcy.keySet()) {
				if (key.contains("RECIPIENT")) {
					{
						Map<String, FieldData> wpisMulti = (Map<String, FieldData>) odbiorcy.get(key);
						selectetPersonIds.add(wpisMulti.get(recipientID).getLongData());
					}
					if (isAnyInvalidRecipients(selectetPersonIds, doc))
						createValidRecipient(doc, selectetPersonIds);
				}
			}
		}
	}

	/**
	 * 
	 * Metoda sprawdza czy istnieje jaki� niepoprawny odbiorca
	 * 
	 * @param selectetPersonIds
	 * @param doc
	 * @return
	 * @throws EdmException
	 */
	private boolean isAnyInvalidRecipients( List<Long> selectetPersonIds, OutOfficeDocument doc) throws EdmException
	{
		List<Long> wybrane = new ArrayList();
		wybrane.addAll(selectetPersonIds);
		List<Long> basePersonIdsRecipients = getListBasePersonIdRecipiets(doc);
		basePersonIdsRecipients.removeAll(wybrane);
				if(!basePersonIdsRecipients.isEmpty())
					return true;
				else {
					wybrane.removeAll(getListBasePersonIdRecipiets(doc));
					if (!wybrane.isEmpty())
						return true;
				}
			
		return false;
		
		
	}
		

	/** Nadanie numeru KO i przypinanie dokumentu do dziennika  wychodz鉍ych 
	* @param document - dokument, kt鏎ego operacja dotyczy
	
	*/
	
/*	public void bindToJournal(OfficeDocument oficeDocument ,ActionEvent event) throws EdmException
	{
		if ((Boolean) event.getAttribute("epuap")){
		OutOfficeDocument doc = (OutOfficeDocument) oficeDocument;
		Date entryDate = new Date();
		Journal journal = Journal.getMainOutgoing();
		Integer sequenceId = Journal.TX_newEntry2(journal.getId(), doc.getId(), entryDate);
		doc.setOfficeDate(entryDate);
		}
	}*/

	/**
	 * 
	 * Metoda tworzy oraz ustawia odbiorc闚
	 * 
	 * @param doc
	 * @param selectetPersonIds
	 * @throws EdmException
	 */
	private void createValidRecipient(OutOfficeDocument doc, List<Long> selectetPersonIds) throws EdmException
	{

		//selectetPersonIds - id perso闚 z dokinda w s這wniku multi
		
		List<Recipient> newListRecipient = new ArrayList();
		//lista person闚 z kt鏎ych utworzeni zostali odbiorcy danego dokumentu
		List<Long> basePersonIdsRecipients = getListBasePersonIdRecipiets(doc);
		//lista id person z kt鏎eg otrzeba utworzy� odbiorce i doda� do dokumentu 
		List<Long> personToCreate = new  ArrayList();
		//lista odbiorc闚 dokumentu do usuni璚ia 
		List<Long> recipientsToRemove = getListBasePersonIdRecipiets(doc);
		removeDuplicates(recipientsToRemove);
		//wyciagniecie recipienta kt鏎ego trezeaba usun寞 bo nie jest zgodny z dokumentem
		recipientsToRemove.removeAll(selectetPersonIds);
		
		//usuniecie wszytkich id wybranych person闚 z kt鏎ych utworzeni s� juz odbiorcy kt鏎zy s� zgodni z tymi co na dokumencie 
		selectetPersonIds.removeAll(basePersonIdsRecipients);
		
		for (Long l :selectetPersonIds)
		personToCreate.add(l);
		
		Iterator<Recipient> iter = doc.getRecipients().listIterator();
		while (iter.hasNext())
		{
			Recipient rec = iter.next();
			for (Long l : recipientsToRemove)
			{
				if (rec!=null && rec.getBasePersonId()!=null && rec.getBasePersonId().compareTo(l) == 0){
					iter.remove();
					rec.delete();
				}
			}
		}
		
		for (Long l : personToCreate){	
			doc.addRecipient(utworzRecipient(Person.findIfExist(l)));
		}
	}

	/**
	 * Metoda pobiera odbiorc闚 dla dokumentu
	 * 
	 * @param doc
	 * @return
	 * @throws EdmException
	 */
	private List<Long> getListBasePersonIdRecipiets(OutOfficeDocument doc) throws EdmException
	{
		List<Long> list = new ArrayList();
		for (Recipient rec : doc.getRecipients()){
			list.add(rec.getBasePersonId());
			}
		return list;
	}

	public void specialSetDictionaryValues(Map<String, ?> dockindFields)
	{
		log.info("specialSetDictionaryValues: \n {} ", dockindFields);

		if (dockindFields.keySet().contains("RECIPIENT_DICTIONARY"))
		{
			Map<String, FieldData> m = new HashMap<String, FieldData>();
			m.put("DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			m.put("ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}

	}

	/**
	 * Dodawanie warto��ci do s這wnika dokumentu
	 * 
	 * @param dictionaryName - nazwa s這wnika
	 * @param values - mapa warto軼i dodawanych do s這wnika
	 */
	public void addSpecialInsertDictionaryValues(String dictionaryName, Map<String, FieldData> values)
	{
		if (dictionaryName.contains("RECIPIENT"))
		{
			values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}

	}

	/**
	 * Metoda dodaje warto��ci do s這wnika dokumentu
	 * 
	 * @param dictionaryName - nazwa s這wnika
	 * @param values - wpis do s這wnika
	 * @param dockindFields - pole dokindu
	 */
	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		if (dictionaryName.contains("RECIPIENT"))
		{
			values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}


	@Override
	public void onStartProcess(OfficeDocument document)
	{
	
		try {
			Map<String, Object> map= Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, document.getAuthor());
			map.put(ASSIGN_DIVISION_GUID_PARAM, document.getDivisionGuid());
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	
	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret= new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	@Override
	public void onRejectedListener(Document doc) throws EdmException
	{
	}

	@Override
	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException
	{
	}
	
	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException,
			EdmException
	{ 
		super.onSaveActions(kind, documentId, fieldValues);
		if(fieldValues.get("WYSLACEMAIL")!=null &&  (Integer)fieldValues.get("WYSLACEMAIL")==1 )
		sendDocumentAsEmail(kind , documentId , fieldValues);
		
	}

	@Override
	public void revaluateFastAssignmentSelectetValuesOnDocument(OfficeDocument doc, String fastAssignmentSelectUser, String fastAssignmentSelectDivision)
			throws UserNotFoundException, EdmException
	{

		fastAssignmentSelectUser = DSApi.context().getDSUser().getName();

		DSDivision[] divs = DSApi.context().getDSUser().getOriginalDivisionsWithoutGroup();
		if (divs.length > 0)
		{
			for (DSDivision d : divs)
			{
				fastAssignmentSelectDivision = d.getGuid() != null ? d.getGuid() : DSDivision.ROOT_GUID;

			}
		} else
		{
			fastAssignmentSelectDivision = DSDivision.ROOT_GUID;
		}

		doc.setAssignedDivision(fastAssignmentSelectDivision);
		doc.setCurrentAssignmentGuid(fastAssignmentSelectDivision);
		doc.setCurrentAssignmentUsername(fastAssignmentSelectUser);
	}

	/**
	 * 
	 * Metoda wysy豉 wiadomo嗆 email z danymi z dokumentu
	 * 
	 * @param kind - typ dokumentu
	 * @param documentId - id dokumentu
	 * @param fieldValues - mapa s這wnika dokumentu
	 * @throws DocumentNotFoundException
	 * @throws EdmException
	 */
	private void sendDocumentAsEmail(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws DocumentNotFoundException, EdmException
	{

		String mailto = "";
		OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId, false);
		FieldsManager fm = doc.getFieldsManager();
		if (fm.getValue("WYSLACEMAIL").equals("Tak"))
		{
			Map<String, Object> slowniki = (Map<String, Object>) fieldValues.get("RECIPIENT_PARAM");
			if (slowniki != null)
			{
				
				for (Entry<String, Object> entry : slowniki.entrySet())
				{
					if (entry.getKey().equalsIgnoreCase("EMAIL") && entry.getValue() != null)
					{
						mailto = (String)entry.getValue();
						
					}

				}
			}


			List<File> tempAttachments = new ArrayList<File>();
			Collection<Mailer.Attachment> attachments = null;
			Collection<Mailer.Attachment> attachmentsTmp = null;
			try
			{
				attachments = getMailerAttachment(doc, tempAttachments);
			} catch (IOException e)
			{
				log.error(" " + e);
			}

			String message = (String) fm.getValue("TRESCEMAIL");
			String subject = (String) fm.getValue("TYTULEMAIL");

			((Mailer) ServiceManager.getService(Mailer.NAME)).send(mailto, subject, message, attachments);

			if (doc.getOfficeNumber() == null)
			{
				assignOfficeNumber(doc);
			}

			Date data = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			doc.setCzyCzystopis(true);
			doc.addWorkHistoryEntry(Audit.create("email", DSApi.context().getPrincipalName(), sm.getString("Wys豉no email", sdf.format(data))));
			try
			{
				PreparedStatement ps = DSApi.context().prepareStatement("UPDATE DS_PG_OUT_DOC set ZABLOKUJ=? where document_id=?");
				ps.setBoolean(1, true);
				ps.setLong(2, documentId);
				ps.executeUpdate();
			} catch (SQLException e)
			{
				log.error(e.getMessage(), e);
			} catch (EdmException e)
			{
				log.error(e.getMessage(), e);
			}
		}


	}

	/**
	 * Metoda pobiera kolekcje za陰cznik闚 z dokumentu i przetwarza je na za陰czniki do mailera
	 * 
	 * @param doc
	 * @param tempAttachments
	 * @return
	 * @throws EdmException
	 * @throws IOException
	 */
		private Collection<Mailer.Attachment> getMailerAttachment(Document doc, List<File> tempAttachments) throws EdmException, IOException
		{
			//Dodaje zalaczniki
			List<Attachment> docAtta = doc.getAttachments();
			Collection<Mailer.Attachment> attachments = null;
			
			if(docAtta != null && !docAtta.isEmpty())
			{
				attachments = new ArrayList<Mailer.Attachment>();
				
				for(Attachment att : docAtta)
				{
					if(att.isDeleted()) continue;
					AttachmentRevision ar = att.getMostRecentRevision();
					File tempAr = ar.saveToTempFile();
					if(ar.getSize() > 0)
					{
						attachments.add(new Mailer.Attachment(ar.getOriginalFilename(), tempAr.toURI().toURL()));
					}
					tempAttachments.add(tempAr);
				}
			}
			
			return attachments;
		}
		
		/**
		 * Metoda ustawia numer kancelaryjny na dokumencie, oraz powi您uje dokument z dziennikiem pism wychodz帷ych
		 * 
		 * @param doc
		 */
		private void assignOfficeNumber(Document doc){
			try
			{
				if (doc instanceof OutOfficeDocument)
				{
					Integer sequenceId = Journal.TX_newEntry2(Journal.getMainOutgoing().getId(), doc.getId(), new Date());
					((OutOfficeDocument) doc).bindToJournal(Journal.getMainOutgoing().getId(), sequenceId);
					((OutOfficeDocument) doc).setOfficeNumber(sequenceId);
				}
				DSApi.context().commit();
				DSApi.context().begin();
			} catch (Exception e)
			{	
				DSApi.context()._rollback();
				log.error("B??d wykonywania akcji "+ e);
			}
		}
	}
	
	