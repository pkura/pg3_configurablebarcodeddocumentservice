package pl.compan.docusafe.parametrization.pg;



import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.task.IdentityLink;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.util.*;

/**
 * User: Tomasz
 * Date: 21.05.14
 * Time: 12:47
 */
public class PGProcessBean implements TaskListener {
	
	/**
	 * Metoda powaidamia mailowo u�ytkownika o tym �e pismo znalaz�o si� na jego li�cie zada�
	 */
    @Override
    public void notify(DelegateTask delegateTask) {
        Long docId = (Long) delegateTask.getVariable("docId");
        for(IdentityLink il : delegateTask.getCandidates()) {
            try {
                String email = DSUser.findByUsername(il.getUserId()).getEmail();
                String msg = "Dokument : "+docId+" pojawil sie na liscie zadan w celu: "+delegateTask.getName();
                ((Mailer) ServiceManager.getService(Mailer.NAME)).send(email, email, "nowe zadanie na liscie", msg);

            } catch (Exception e) {
                LoggerFactory.getLogger("tomekl").debug(e.getMessage(),e);
            }
            LoggerFactory.getLogger("tomekl").debug("{} {} {}",docId, il.getUserId(),delegateTask.getName());
        }
    }

    private static final Logger LOG = LoggerFactory.getLogger(PGProcessBean.class);

	/**
	 * Metoda pobiera list� u�ytkownik�w dla dzia�u o <code>guid</code>
	 * 
	 * @param guid - identyfikator dzia�u
	 * @return
	 */
    public List<String> getDivisionUsers(String guid) {
        try {
            List<String> r = new ArrayList<String>();
            DSUser[] users = DSDivision.find(guid).getUsers();
            for(DSUser u : users) {
                r.add(u.getName());
            }
            return r;
        } catch (Exception e) {
            LoggerFactory.getLogger("tomekl").debug(e.getMessage(),e);
            return Collections.EMPTY_LIST;
        }

    }
    /**
     * Metoda pobiera autora dokumentu
     * 
     * @param documentId
     * @return
     * @throws DocumentNotFoundException
     * @throws EdmException
     */
    public String  getAutor(Long documentId) throws DocumentNotFoundException, EdmException {
       return OfficeDocument.find(documentId).getAuthor();

    }
    /**
     * Metoda sprawdza czy dokument jest u autora albo osoby odpowiedzialnej
     * 
     * @param documentId
     * @return
     * @throws DocumentNotFoundException
     * @throws EdmException
     */
    public boolean isAutorOrClerk(Long documentId) throws DocumentNotFoundException, EdmException{
    	OfficeDocument doc =   OfficeDocument.findOfficeDocument(documentId,false);
    	OfficeCase c = getCaseById(doc.getFieldsManager().getStringKey("CASES"));
    	String contextName = DSApi.context().getDSUser().getName();
    	if(contextName.equals(c.getClerk()) || contextName.equals(c.getAuthor()))
    		return true;
    	else 
    		
		return false;
    	
    }

    /**
     * Metoda sprawdza czy na�o�one s� dodatkowe akceptacje
     * 
     * @param documentId
     * @param fieldCn
     * @return
     * @throws DocumentNotFoundException
     * @throws EdmException
     */
	public boolean isAdditionalAceptanceAvaialble(Long documentId, String fieldCn) throws DocumentNotFoundException, EdmException {
    	OfficeDocument doc =   OfficeDocument.findOfficeDocument(documentId,false);
    	//String cnField ="";
    	if(fieldCn!=null && !fieldCn.isEmpty()){
    		if(doc.getFieldsManager().getIntegerKey(fieldCn)!=null ){
    			return true;
    		}
    		else
    		return false ;
    	}
    	return false;
    }
	
	/**
	 * Metoda sprawdza czy jest ten sam autor sprawy co osoba odpowiedzialna
	 * 
	 * @param documentId
	 * @return true/false operacji
	 * @throws DocumentNotFoundException
	 * @throws EdmException
	 */
    public boolean  isSameCaseAutorAndClerk(Long documentId) throws DocumentNotFoundException, EdmException {
    	OfficeDocument doc =   OfficeDocument.findOfficeDocument(documentId,false);
    	OfficeCase c = null;
    	if(doc.getFieldsManager().getStringKey("CASES")!=null && !doc.getFieldsManager().getStringKey("CASES").isEmpty())
    	 c = getCaseById(doc.getFieldsManager().getStringKey("CASES"));
		
    	if(c==null)
    		return false;
    	else
    	return (c.getAuthor().equals(c.getClerk()));
    	
     }
    /**
     * Metoda sprawdza czy jest ten sam autor sprawy co osoba odpowiedzialna
     * 
     * @param documentId
     * @param fieldCn
     * @return
     * @throws DocumentNotFoundException
     * @throws EdmException
     */
    public boolean  isSameCaseAutorAndClerk(Long documentId,String fieldCn) throws DocumentNotFoundException, EdmException {
    	OfficeDocument doc =   OfficeDocument.findOfficeDocument(documentId,false);
    	OfficeCase c = null;
    	String cnField ="CASE";
    	if(fieldCn!=null && !fieldCn.isEmpty())
    		cnField = fieldCn;
    	if(doc.getFieldsManager().getStringKey(cnField)!=null && !doc.getFieldsManager().getStringKey(cnField).isEmpty())
    	 c = getCaseById(doc.getFieldsManager().getStringKey(cnField));
		
    	if(c==null)
    		return false;
    	else
    	return (c.getAuthor().equals(c.getClerk()));
    	
     }
    
    /**
     * Metoda pobiera autora sprawy.
     * 
     * @param documentId
     * @return
     * @throws DocumentNotFoundException
     * @throws EdmException
     */
    public String  getCaseAutor(Long documentId) throws DocumentNotFoundException, EdmException {
    	OfficeDocument doc =   OfficeDocument.findOfficeDocument(documentId,false);
    	OfficeCase c = null;
    	if(doc.getFieldsManager().getStringKey("CASES")!=null && !doc.getFieldsManager().getStringKey("CASES").isEmpty())
    	 c = getCaseById(doc.getFieldsManager().getStringKey("CASES"));
    	
    	if(c==null)
    		return "admin";
    	else
		return c.getAuthor();

     }
    
    /**
     * Metoda pobiera osob� odpowiedzialn� w sprawie
     * 
     * @param documentId
     * @return
     * @throws DocumentNotFoundException
     * @throws EdmException
     */
    public String  getCaseReferent(Long documentId) throws DocumentNotFoundException, EdmException {
    	OfficeDocument doc =   OfficeDocument.findOfficeDocument(documentId,false);
    	OfficeCase c = null;
    	if(doc.getFieldsManager().getStringKey("CASES")!=null && !doc.getFieldsManager().getStringKey("CASES").isEmpty())
    	 c = getCaseById(doc.getFieldsManager().getStringKey("CASES"));
    	
    	if(c==null)
    		return "admin";
    	else
		return c.getClerk();


     }
    
    /**
     * 
     *  Metoda pobiera autora sprawy
     * 
     * @param documentId
     * @param fieldCn
     * @return
     * @throws DocumentNotFoundException
     * @throws EdmException
     */
    public String  getCaseAutor(Long documentId,String fieldCn) throws DocumentNotFoundException, EdmException {
    	OfficeDocument doc =  OfficeDocument.findOfficeDocument(documentId,false);
    	String cnField ="CASE";
    	if(fieldCn!=null && !fieldCn.isEmpty())
    		cnField = fieldCn;
    		
    	OfficeCase c = null;
    	if(doc.getFieldsManager().getStringKey(cnField)!=null && !doc.getFieldsManager().getStringKey(cnField).isEmpty())
    	 c = getCaseById(doc.getFieldsManager().getStringKey(cnField));
    	if(c==null)
    		return "admin";
    	else
		return c.getAuthor();
     }
    /**
     * Metoda pobiera osob� odpowiedzialn� w sprawie
     * 
     * @param documentId
     * @param fieldCn
     * @return
     * @throws DocumentNotFoundException
     * @throws EdmException
     */
    public String  getCaseReferent(Long documentId,String fieldCn) throws DocumentNotFoundException, EdmException {
    	OfficeDocument doc =  OfficeDocument.findOfficeDocument(documentId,false);
    	String cnField ="CASE";
    	if(fieldCn!=null && !fieldCn.isEmpty())
    		cnField = fieldCn;
    		
    	OfficeCase c = null;
    	if(doc.getFieldsManager().getStringKey(cnField)!=null && !doc.getFieldsManager().getStringKey(cnField).isEmpty())
    	 c = getCaseById(doc.getFieldsManager().getStringKey(cnField));
    	if(c==null)
    		return "admin";
    	else
		return c.getClerk();
     }
    /**
     * Metoda pobiera spraw� po id
     * 
     * @param caseId
     * @return OfficeCase
     * @throws NumberFormatException
     * @throws EdmException
     */
   private OfficeCase getCaseById(String caseId) throws NumberFormatException, EdmException{
	   return OfficeCase.find(Long.valueOf(caseId.trim()));
   }
   /**
    * Metoda pobiera nazw� obecnego u�ytkownika
    * 
    * @return String u�ytkownia
    */
    public String currentUser() {
        return DSApi.context().getPrincipalName();
    }

    /**
     * Metoda ustawia status dokumentu
     * 
     * @param docId
     * @param statusKey
     */
    public void setStatus(Long docId, String statusKey) {
        try {
            Document doc = Document.find(docId);
            int statusId = doc.getDocumentKind().getFieldByCn("STATUS").getEnumItemByCn(statusKey).getId();
            Map<String,Object> m = new HashMap<String, Object>();
            m.put("STATUS",statusId);
            doc.getDocumentKind().setOnly(docId,m,false);
        } catch (Exception e) {
            LOG.debug(e.getMessage(),e);
        }
    }

    /**
     * Metoda pobiera guid dzia�u.
     * 
     * @param docId - id dokumentu
     * @param fieldCn - nazwa kodowa dzia�u
     * @return String - guid
     */
	public String getGuidFromDockindDivisionField(Long docId, String fieldCn)
	{
		String guid = "rootdivision";
		try
		{
			OfficeDocument doc = OfficeDocument.findOfficeDocument(docId, false);
			String cnField = "DSDIVISION";

			if (fieldCn != null && !fieldCn.isEmpty())
				cnField = fieldCn;

			if (doc.getFieldsManager().getIntegerKey(cnField) != null)
			{
				Integer divId = doc.getFieldsManager().getIntegerKey(cnField);
				DSDivision div = DSDivision.findById(divId);
				guid = div.getGuid();
			}
		} catch (Exception e)
		{
			return "rootdivision";
		}

		return guid;
	}
	/**
	 * Metoda pobiera guid dzia�u.
	 * 
	 * @param divisionName - nazwa dzia�u
	 * @return String - guid
	 */
    public String getGuidFromDivision(String divisionName) {

        String guid = null;

        try {
            guid = DSDivision.findByName(divisionName).getGuid();
        } catch(Exception e) {
            guid = "Niepoprawna nazwa dzia�u";
        }

        return guid;
    }

    /**
     * Metoda modyfikuje metadane dokumentu
     * 
     * @param id
     * @param openDate
     * @param author
     * @param rwa
     * @param clerk
     * @param finishDate
     * @param priority
     * @param status
     * @param title
     * @param description
     * @throws Exception
     */
    public void modifyCaseMetadataWhereIsDocument(Long id, Date openDate, String author, Rwa rwa, String clerk, Date finishDate, CasePriority priority, CaseStatus status, String title, String description) throws Exception {

        OfficeDocument doc = (OfficeDocument) Document.find(id);
        OfficeCase c = OfficeCase.find(Long.parseLong(doc.getCaseDocumentId()));
        StringBuilder sb = new StringBuilder("Zmieniono :");
        if(openDate != null) {
            c.setOpenDate(openDate);
        }
        if(author != null) {
            c.setAuthor(author);
            DSUser u = DSUser.findByUsername(author);
            sb.append("Autora sprawy na: "+ u.asLastnameFirstname()+" ; ") ;
           
        }
        if(rwa != null) {
            c.setRwa(rwa);
        }
        if(clerk != null) {
            c.setClerk(clerk);
            DSUser u = DSUser.findByUsername(clerk);
            sb.append("Referenta sprawy na: "+ u.asLastnameFirstname()+" ; ") ;
        }
        if(finishDate != null) {
            c.setFinishDate(finishDate);
            sb.append("Dat� zako�czenia sprawy na: "+ DateUtils.formatJsDate(finishDate)+" ; ") ;
        }
        if(priority != null) {
            c.setPriority(priority);
            sb.append("Priorytet sprawy na: "+ priority.getName()+" ; ") ;
        }
        if(status != null) {
            c.setStatus(status);
      	  sb.append("Status sprawy na: "+ status.getName()+" ; ") ;
        }
        if(title != null) {
            c.setTitle(title);
            sb.append("Tytu� sprawy na: "+title+" ; ") ;
        }
        if(description != null) {
            c.setDescription(description);
            sb.append("Opis sprawy na: "+ description+" ; ") ;
        }
        
        Audit a =  Audit.create(null, DSApi.context().getDSUser().getName(), sb.toString());
        c.getAudit().add(a);
        DSApi.context().session().save(c);
    }
    /**
     * Metoda modyfikuje metadane sprawy z dokindu
     * 
     * @param docId
     * @throws DocumentNotFoundException
     * @throws EdmException
     */
    public void modifyCaseMetadataFromDockind(Long docId) throws DocumentNotFoundException, EdmException {
    	  OfficeDocument doc = OfficeDocument.findOfficeDocument(docId, false);
          FieldsManager fm = doc.getFieldsManager();
          StringBuilder sb = new StringBuilder("Zmieniono :");
          OfficeCase c = getCaseById(doc.getFieldsManager().getStringKey("CASES"));
         Map<String, Object> values = fm.getFieldValues();
         if(values.get("CASETITLE")!=null && !((String)values.get("CASETITLE")).isEmpty()){
        	 c.setTitle((String)values.get("CASETITLE"));
         sb.append("Tytu� sprawy na: "+ c.getTitle()+" ; ") ;
         
         }
         if(values.get("CASEDESCRIPTION")!=null && !((String)values.get("CASEDESCRIPTION")).isEmpty()){
             c.setDescription((String)values.get("CASEDESCRIPTION"));
         sb.append("Opis sprawy na: "+ c.getDescription()+" ; ") ;
         }
         if(values.get("AUTOR")!=null ){
        	 DSUser u = DSUser.findById(fm.getLongKey("AUTOR"));
             c.setAuthor(u.getName());
         sb.append("Autora sprawy na: "+ u.asLastnameFirstname()+" ; ") ;
         }
         if(values.get("CLERK")!=null ){
        	 DSUser u = DSUser.findById(fm.getLongKey("CLERK"));
        	    c.setClerk(u.getName());
         sb.append("Referenta sprawy na: "+ u.asLastnameFirstname()+" ; ") ;
         }
         if(values.get("PLANINGENDTIME")!=null){
             Date fDate = (Date)values.get("PLANINGENDTIME");
             c.setFinishDate(fDate);
             sb.append("Dat� zako�czenia sprawy na: "+ DateUtils.formatJsDate(c.getFinishDate())+" ; ") ;
         }
         if(values.get("CASESPRIORITY")!=null){
             c.setPriority(CasePriority.find(fm.getIntegerKey("CASESPRIORITY")));
         sb.append("Priorytet sprawy na: "+ c.getPriority().getName()+" ; ") ;
         }
         if(values.get("CASESTATUS")!=null){
        	  c.setStatus(CaseStatus.find(fm.getIntegerKey("CASESTATUS")));
        	  sb.append("Status sprawy na: "+ c.getStatus().getName()+"  ") ;
         }
         
       
         Audit a =  Audit.create(null, doc.getAuthor(), sb.toString());
         c.getAudit().add(a);
        
         DSApi.context().session().save(c);

    }
    /**
     * Metoda sprawdza czy kwota znajduje si� w zakresie
     * 
     * @param docId
     * @param from
     * @param to
     * @return
     */
    public boolean kwotaWZakresie(Long docId, String from, String to) {
        try {
            Document doc = Document.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            BigDecimal v = (BigDecimal) fm.getKey("KWOTA");
            BigDecimal f = BigDecimal.valueOf(Double.valueOf(from));
            BigDecimal t = BigDecimal.valueOf(Double.valueOf(to));
            if(v.compareTo(f) > v.compareTo(t)){
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            LOG.debug(e.getMessage(),e);
        }
        return false;
    }
}
