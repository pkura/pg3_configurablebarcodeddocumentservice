package pl.compan.docusafe.parametrization.pg;

import java.util.Calendar;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** asygnuje na odbiorce */
@SuppressWarnings("serial")
public class PackageAssigneHandler implements AssignmentHandler {
	private static final Logger log = LoggerFactory.getLogger(PackageAssigneHandler.class);
	private String poleOdbiorcy;
	@Override
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
		log.info("PackageAssigneHandler");
		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		
		DSUser odb = null;
		 openExecution.setVariable(Jbpm4WorkflowFactory.OBJECTIVE,  doc.getFieldsManager().getEnumItem("STATUS").getTitle());
		
		if(fm.getFieldValues()!=null && fm.getFieldValues().get(PgPackageLogic.CN_PACZKA_NA_DZIAL)!=null 
				&& ((Boolean) fm.getFieldValues().get(PgPackageLogic.CN_PACZKA_NA_DZIAL)==true)){
			odb = DSApi.context().getDSUser();
		}
		
		
		
		DSUser nadawca = null;
		DSUser odbiorca = null;
		
		if(doc!=null){
			for(Recipient rec : doc.getRecipients()){
				odbiorca = DSUser.findByFirstnameLastname(rec.getFirstname(), rec.getLastname());
			}
			//nadawca = DSUser.findByFirstnameLastname(doc.getSender().getFirstname(), doc.getSender().getLastname());
			nadawca = DSUser.findByUsername(doc.getAuthor());
		}
		
		String useridstl=fm.getStringKey(poleOdbiorcy);
		assignable.addCandidateUser(odbiorca.getName()!=null ?odbiorca.getName(): DSApi.context().getDSUser().getName() );
		
		AssignmentHistoryEntry entry = new AssignmentHistoryEntry();
		entry.setSourceUser(nadawca.getName());
		entry.setTargetUser(odbiorca.getName());
		String dzialNad =PackageEmailLogic.getGuidDzialUzytkownika(nadawca.getName());
		entry.setSourceGuid(dzialNad);
		String dzialOdb =PackageEmailLogic.getGuidDzialUzytkownika(odbiorca.getName());
		entry.setTargetGuid(dzialOdb);
		String statusDokumentu =  doc.getFieldsManager().getEnumItem("STATUS").getTitle();
		entry.setObjective(statusDokumentu);
		entry.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
		doc.addAssignmentHistoryEntry(entry);
	}
}
