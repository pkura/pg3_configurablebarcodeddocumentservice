package pl.compan.docusafe.parametrization.pg;

import com.beust.jcommander.internal.Lists;
import com.google.common.collect.Maps;
import com.google.common.primitives.Longs;

import org.springframework.util.CollectionUtils;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.AbstractDictionaryField;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.office.common.DwrDocumentHelper;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.*;

/**
 * <h1>ZadanieLogic</h1>
 *
 * <p>
 *     Klasa s�u�y do obs�ugi zachowania/implementaji funkcjonalno�ci dokumentu zadania.
 * </p>
 */
@SuppressWarnings("serial")
public class ZadanieLogic extends AbstractPgLogic
{

	public static final String multiOdbiorca = "M_DICT_VALUES";
	public static final String recipientID = "ID";
	public static final String _dataDokumentu = "DOC_DATE";
	public static final String _dataDokumentuDwr = "DWR_DOC_DATE";
	public final static String _USER = "DOC_AUTOR";
	public final static String _DIVISION = "DOC_AUTORDIVISION";
    public static final String DOC_DATE_REMINDER_CN = "DOC_DATE_REMINDER";
    public static final String DOC_DATE_REALIZATION_CN = "DOC_DATE_REALIZATION";

	private static final String _status = "STATUS";
	private String _RECIPIENT = "RECIPIENT";
	private StringManager sm = GlobalPreferences.loadPropertiesFile("", null);

	protected static DwrDocumentHelper dwrDocumentHelper = new DwrDocumentHelper();

	public static Map<Integer, String> userStatusItem;
	{
	    Map<Integer,String> temp = new HashMap<Integer, String>();
	    temp.put(0,"Oczekiwanie na akceptacje zadania");
	    temp.put(1,"Realizacja zadania");
	    temp.put(2,"Zrealizowano zadanie");
	    temp.put(3,"Odrzucono zmian� daty realizacji");
	    temp.put(4,"Zaakceptowano zmian� daty realizacji");
	    temp.put(5,"Delegowany zaakceptowa� zadanie - W realizacji");
	    temp.put(6,"Zaakceptowano oddelegowane zadanie - W realizacji");
	    temp.put(7,"Odrzucono Delegowane zadanie - W realizacji");
	    temp.put(8,"Akceptacja zmiany termin�w realizacji");
	    temp.put(9,"Akceptacja oddelegowanego zadania");
	    temp.put(10,"Odrzucono wykonanie zadania");
	    userStatusItem = Collections.unmodifiableMap(temp);
	}
	
	
	private static ZadanieLogic instance;
	protected static Logger log = LoggerFactory.getLogger(ZadanieLogic.class);


	public static ZadanieLogic getInstance()
	{
		if (instance == null)
			instance = new ZadanieLogic();
		return instance;
	}


	@Override
	public void acceptTask(Document document, ActionEvent event, String activity)
	{
		// Proces acctiviti narazie nie ma na nim barkodu  jak na jakims bedzie to sie bedzie aktualizowalo barkod
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk�y- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk�y - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP,
				"Dokumenty zwyk�y zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk�y - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	//nowa kolumna na liscie zadan 
	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();

		try
		{

		} catch (Exception e)
		{
			log.error("", e);
		}
		return params;
	}

	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
	{
		Field msg = null;
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		Date startDate = cal.getTime();
		System.out.println("Yesterday's date = "+ cal.getTime());
        FieldData reminderField = containsField(values, DOC_DATE_REMINDER_CN);
        FieldData realizationField = containsField(values, DOC_DATE_REALIZATION_CN);
		if (reminderField != null && reminderField.getDateData() != null && reminderField.getDateData().before(startDate))
		{
			msg = new Field("messageField", sm.getString("dataXXXNieMozeBycDataZPrzeszlosci", ": przypomnienia o zadaniu"), Field.Type.BOOLEAN);
			values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
			return msg;
		} else if (realizationField != null && realizationField.getDateData() != null && realizationField.getDateData().before(startDate))
		{
			msg = new Field("messageField", sm.getString("dataXXXNieMozeBycDataZPrzeszlosci", ": termin realizacji zadania"), Field.Type.BOOLEAN);
			values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
			return msg;
		}

        FieldData recipient = containsField(values, _RECIPIENT);
        Map<String, FieldData> mapa = recipient.getDictionaryData();
		for (String key : mapa.keySet())
		{

			if (key.contains("RECIPIENT_USER_REALIZATION_DATE_CHANGED_"))
			{
				if (mapa.get(key).getDateData() != null)
				{
					Date tempDate = (mapa.get(key).getDateData());
					if (tempDate.before(startDate))
					{

						msg = new Field("messageField", sm.getString("dataXXXNieMozeBycDataZPrzeszlosci", ": zmiana terminu realizacji zadania"), Field.Type.BOOLEAN);
						values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
						return msg;
					}
					else if (realizationField.getDateData()!=null && tempDate.after(realizationField.getDateData())){
						msg = new Field("messageField", sm.getString("dataXXXNieMozeBycPuzniejszaNizDataXXX", ": zmiana terminu realizacji zadania" , "realizacji zadania "), Field.Type.BOOLEAN);
						values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
						return msg;
					}
							
				}
			} else if (key.contains("RECIPIENT_USER_REALIZATION_DATE_"))
				if (mapa.get(key).getDateData() != null)
				{
					Date tempDate = (mapa.get(key).getDateData());
					if (tempDate.before(startDate))
					{

						msg = new Field("messageField", sm.getString("dataXXXNieMozeBycDataZPrzeszlosci", ": termin realizacji zadania"), Field.Type.BOOLEAN);
						values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
						return msg;
					}
				}
		}

		return msg;
	}

	@Override
	public void onLoadData(FieldsManager fm) throws EdmException
	{

		super.onLoadData(fm);
		Long documentID = fm.getDocumentId();
		DSUser currentUSer = DSApi.context().getDSUser();
		if (documentID != null && fm.getEnumItem(_status) != null && fm.getEnumItem(_status).getId() < 21)
			fm.getField("RECIPIENT").setCantUpdate(false);
		else if (documentID != null && fm.getEnumItem(_status) != null && fm.getEnumItem(_status).getId() > 21)
			fm.getField("RECIPIENT").setCantUpdate(false);
		fm.getFieldsByAspect("");


	}

	@Override
	public void revaluateMultipleDictionaryValues(Long documentId, AbstractDictionaryField abstractDictionaryField, Collection<Long> dictionaryIds)
			throws DocumentNotFoundException, AccessDeniedException, EdmException
	{


		DSUser currentUSer = DSApi.context().getDSUser();
		if (documentId != null)
		{
			Document doc = OfficeDocument.find(documentId, false);

			//FieldsManager fm;
			//List<String> popupButtons = Arrays.asList("doSelect", "doClear");
		//	abstractDictionaryField.setPopUpButtons(popupButtons);
			if (!doc.getAuthor().equals(currentUSer.getName()) && (Integer.parseInt(doc.getLparam()) > 20))
			{
			//	List<String> buttons = Arrays.asList("");
			//	abstractDictionaryField.setPopUpButtons(buttons);
				//fm = doc.getFieldsManager()
				try
				{

					boolean saved = (Boolean) (DSApi.context().getAttribute("saved") != null ? DSApi.context().getAttribute("saved") : false);
					if (saved && DSApi.context().getAttribute("dictionaryIds") != null)
					{
						dictionaryIds.addAll((List<Long>) DSApi.context().getAttribute("dictionaryIds"));
					} else
					{
						Map<Long, String> resultsMap = new HashMap<Long, String>();
						List<Long> listIdsToShow = new ArrayList<Long>();
						List<Long> lsd = new ArrayList<Long>();

						for (Long l : dictionaryIds)
						{
							lsd.add(l);
						}
						DSApi.context().setAttribute("dictionaryIds", lsd);

						resultsMap = getShowResultMap(null, lsd);
						DSApi.context().setAttribute("map", resultsMap);

						if (resultsMap.get(currentUSer.getId()) != null)
						{

							listIdsToShow.add(Long.parseLong(resultsMap.get(currentUSer.getId())));
							for (ZadanieOdbiorca odb : getlistaOdbiorcow(lsd))
							{
								//odb.setUserRealizationDate((Date)fm.getFieldValues().get("DOC_DATE_REALIZATION"));
								if (odb.getDelegateUserId() != null && odb.getDelegateUserId().equals(currentUSer.getId()))
								{
									String a ="";//listIdsToShow.add(odb.getId());
								}

							}
							dictionaryIds.clear();
							dictionaryIds.addAll(listIdsToShow);
							DSApi.context().setAttribute("listIdsToShow", listIdsToShow);
						} else
						{
							for (ZadanieOdbiorca odb : getlistaOdbiorcow(lsd))
							{
								if (odb.getDelegateUserId() != null && odb.getDelegateUserId().equals(currentUSer.getId()))
								{
									listIdsToShow.add(odb.getId());
								}

							}

							/*if (listIdsToShow.isEmpty())
							{
								throw new EdmException("Brak uprawnien do tego dokumentu");
							} else
							{*/
							if (!listIdsToShow.isEmpty())
							{
								dictionaryIds.clear();
								dictionaryIds.addAll(listIdsToShow);
								DSApi.context().setAttribute("listIdsToShow", listIdsToShow);
							}
						}

					}

				} catch (SQLException e)
				{
					log.error("", e);
				}
			}
		}

	}
	/**
	 * 
	 * Metoda pobiera list� odbiorc�w zadania
	 * 
	 * @param lsd
	 * @return
	 * @throws EdmException
	 */
	private List<ZadanieOdbiorca>  getlistaOdbiorcow(List<Long> lsd) throws EdmException
	{
		return ZadanieOdbiorca.getOdbiorcyFromIds(lsd);
	}



	/**
	 * Metoda ustawia warto�ci multis�ownika dokumentu
	 * @param dictionaryName - nazwa s�ownika
	 * @param values - wpis do s�ownika
	 * @param fieldsValues - pole dokindu
	 */
	@Override
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues,
			Long documentId)
	{
		// TODO Auto-generated method stub
		return super.setMultipledictionaryValue(dictionaryName, values, fieldsValues, documentId);
	}



	/**
	 * Metoda zwaraca mape u�ytkownik�w i odbiorc�w
	 * 
	 * @param fm
	 * @param IdsFromDictionary
	 * @return
	 * @throws EdmException
	 * @throws SQLException
	 */
	public Map<Long, String> getShowResultMap(FieldsManager fm , List<Long> IdsFromDictionary) throws EdmException, SQLException
	{
		Map<Long, String> resultsMap = new HashMap<Long, String>();
		List<Long> listRec = new ArrayList<Long>();
		try
		{
			if(fm!=null && fm.getFieldValues().get("RECIPIENT") !=null )
			listRec = (List<Long>) fm.getFieldValues().get("RECIPIENT");
			else 
				listRec.addAll(IdsFromDictionary);
			
			
			StringBuilder sql = new StringBuilder("Select  USER_ID,ID from DS_PG_ZADANIE_ODBIORCA where id in( ");
			for (Long ql : listRec)
			{
				sql.append(ql + ",");
			}
			String select = sql.substring(0, sql.length() - 1)+")";
			PreparedStatement ps = DSApi.context().prepareStatement(select);
			ResultSet rs = ps.executeQuery();
			while (rs.next())
			{
				resultsMap.put(rs.getLong("USER_ID"),rs.getString("ID") );

			}
			return resultsMap;
		}
		catch (SQLException e)
		{
			log.error("", e);
		}
		return resultsMap;
	}
	
	/**
	 * 
	 * Metoda zwaraca liste id-k�w u�ytkownik�w
	 * 
	 * @param idsFromDictionary
	 * @return List ids
	 * @throws EdmException
	 * @throws SQLException
	 */
	private List<Long> getUsersIdsList (String[] idsFromDictionary) throws EdmException, SQLException
	{
		List<Long> listRec = new ArrayList<Long>();
		try
		{
			

			StringBuilder sql = new StringBuilder("Select  USER_ID,ID from DS_PG_ZADANIE_ODBIORCA where id in( ");
			for (String ql : idsFromDictionary)
			{
				sql.append(ql + ",");
			}
			String select = sql.substring(0, sql.length() - 1)+")";
			PreparedStatement ps = DSApi.context().prepareStatement(select);
			ResultSet rs = ps.executeQuery();
			while (rs.next())
			{
				listRec.add(rs.getLong("USER_ID"));

			}
			return listRec;
		}
		catch (SQLException e)
		{
			log.error("", e);
		}
		return listRec;
	}

	/**
	 * Metoda dodaje warto�ci do s�ownika dokumentu
	 * @param dictionaryName - nazwa s�ownika
	 * @param values - wpis do s�ownika
	 * @param dockindFields - pole dokindu
	 */
	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		if (dictionaryName.contains("RECIPIENT"))
		{
			if(values.get("id")!=null && values.get("id").getData()!=null){
				String a = "";
			}
			else{
			    values.put(dictionaryName + "_SHOW_ON_SELECT", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN,true));
			}
			
		}
	}
	/**
	 * Dodawanie warto�ci do s�ownika dokumentu
	 * 
	 * @param dictionaryName - nazwa s�ownika
	 * @param values - mapa warto�ci dodawanych do s�ownika
	 */
	@Override
	public void addSpecialInsertDictionaryValues(String dictionaryName, Map<String, FieldData> values)
	{
		if (dictionaryName.contains("RECIPIENT"))
		{
			values.put("SHOW_ON_SELECT", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN,false));
		}
	}
	/**
	 * 
	 * Metoda ustawia pola multis�ownika
	 * 
	 * @param dictionaryName - nazwa s�ownika
	 * @param values - mapa s�ownika
	 */
	@Override
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values)
	{
		
		return super.setMultipledictionaryValue(dictionaryName, values);
	}
	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
	{
		// TODO Auto-generated method stub
		super.onSaveActions(kind, documentId, fieldValues);
		if(fieldValues.get(_status)!=null)
		OfficeDocument.find(documentId,false).setLparam( fieldValues.get(_status).toString());
	}

	@Override
	public void setAfterCreateBeforeSaveDokindValues(DocumentKind kind, Long id, Map<String, Object> dockindKeys) throws EdmException
	{

		super.setAfterCreateBeforeSaveDokindValues(kind, id, dockindKeys);
		Integer statusID = (Integer) dockindKeys.get(_status);
		boolean inPRogres = statusID > 11;//boolean inPRogres = statusID > 20;	//
		String countOdbZad = "0";
		if(statusID<21){
		Map<String, FieldData> slowniki = (Map<String, FieldData>) dockindKeys.get("M_DICT_VALUES");
		if (slowniki != null)
		{
			for (Map.Entry<String,FieldData> entry : slowniki.entrySet()) {
				if(entry.getKey().startsWith("RECIPIENT_")){
					
					Map<String, FieldData> tr = (Map<String, FieldData>) entry.getValue();
					for (Map.Entry<String,FieldData> ent : tr.entrySet()) {
						if(ent.getKey().equalsIgnoreCase("ID") && ent.getValue() != null){
							String a  = "," +ent.getValue().toString();
								if(!inPRogres){
									ent.getValue().setData(null);
								}
								}
						if (ent.getKey().equalsIgnoreCase("USER_REALIZATION_DATE") && ent.getValue() != null)
						{
							if(ent.getValue().getData()==null)
								ent.getValue().setDateData((Date) dockindKeys.get("DOC_DATE_REALIZATION"));
						}

					}
				}

			}
		}
		}
	}
	

	@Override
	public void addSpecialValueToSendingActionEmail(OfficeDocument doc, StringBuilder emailTresc, DSUser odbiorca) throws EdmException
	{
		FieldsManager fm = doc.getFieldsManager() ;
		fm.getFieldValues();
		fm.getDictionaryFields(_RECIPIENT );
		String odbiorcy = fm.getStringKey(_RECIPIENT );
		
		for(ZadanieOdbiorca odb : ZadanieOdbiorca.getOdbiorcyFromIds(odbiorcy)){
			if(odbiorca.getId().equals(odb.getUserId())){
				emailTresc.append ("Dokladny opis: ");
				emailTresc.append(odb.getDescription());
			}
		}
					
	}

	public List<String> getUsersToProcesActionFromEmail(Long documentId) throws DocumentNotFoundException, EdmException
	{
		List<String> users = new ArrayList<String>();
		OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId, false);
		FieldsManager fm = doc.getFieldsManager() ;
		String odbiorcy = fm.getStringKey(_RECIPIENT );
		String[] ids;
	
		if (odbiorcy.startsWith("[")) {
			odbiorcy= odbiorcy.substring(1, odbiorcy.length() - 1);
			ids= odbiorcy.split(",");
		} else {
			ids= new String[1];
			ids[0]= odbiorcy;
		}
		
		try
		{
			for (Long id : getUsersIdsList(ids)) {
				DSUser u = 	DSUser.findById(id);
				users.add(u.getName());
				}
		} catch (NumberFormatException e)
		{
			log.error("", e);
		} catch (SQLException e)
		{
			log.error("", e);
		}
		return users;
	}
	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{

		log.info("type: {}", type);
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("TYPE", type);
		values.put(_dataDokumentu, new Date());
		values.put(_status, 10);
		//fm.getField("DOC_REFERENT").setHidden(true);
		//fillCreatingUser(values);

		fm.reloadValues(values);

	}

    /**
     * Metoda wywo�ywana przy starcie procesu
     * 
     * @param document
     * @param event
     */
	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		
		super.onStartProcess(document, event);
			DSApi.context().watch(URN.create(document));
	}
	/**
	 * Metoda wywo�ywana w trakcie wykonania akcji zwi�zanych z archiwizacj� dokumentu.
	 * Wi��e dokument z akcjami archiwalnymi
	 * 
	 * @param document - obj. dokumentu
	 * @param type - typ dokumentu (archiwalny,przychodz�cy,wychodz�cy,wewn�trzny)
	 * @param as - obj. wykonuj�cy akcje archiwalne
	 * 
	 */
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		as.useFolderResolver(document);
		as.useAvailableProviders(document);
		
		updateMultipeRecipient(document);
		
		log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
	}


	/**
	 * Metoda aktualizuje wielu odbiorc�w dla dokumentu
	 * 
	 * @param document
	 */
	private void updateMultipeRecipient(Document document)
	{
		if (DSApi.context().getAttribute("listIdsToShow") != null 
				&& !((List<Long>) DSApi.context().getAttribute("listIdsToShow")).isEmpty()
				&& DSApi.context().getAttribute("dictionaryIds") != null 
				&& !((List<Long>) DSApi.context().getAttribute("dictionaryIds")).isEmpty())
		{
			List<Long> currentIds = (List<Long>) DSApi.context().getAttribute("listIdsToShow");
			List<Long> allIds = (List<Long>) DSApi.context().getAttribute("dictionaryIds");
			allIds.removeAll(currentIds);
			PreparedStatement ps ;
			for (Long l : allIds)
			{

				String sql = "insert into " + document.getDocumentKind().getMultipleTableName() + " (DOCUMENT_ID,FIELD_CN,FIELD_VAL) values (?,?,?)";
				try
				{
					 ps = DSApi.context().prepareStatement(sql);

					ps.setLong(1, document.getId());

					ps.setString(2, "RECIPIENT");

					ps.setString(3, l.toString());
					ps.execute();
					ps.close();

				} catch (SQLException e)
				
				{
					log.error("", e);
				} catch (EdmException e)
				{
					log.error("", e);
					
				}
			}
		}

	}

    @Override
    public void globalValidate(Map<String, Object> values, Document document) throws EdmException {
        super.globalValidate(values, document);
        log.error("vv {}", values);
        List<String> recipientsIds = (List<String>) values.get(_RECIPIENT);

        List<Long> recipientsIdLong = Lists.newArrayList();
        for(String recId : recipientsIds)
            recipientsIdLong.add(Long.valueOf(recId));

        List<ZadanieOdbiorca> recipients = ZadanieOdbiorca.getOdbiorcyFromIds(recipientsIdLong);

//        if(!CollectionUtils.isEmpty(recipientsIdLong) && CollectionUtils.isEmpty(recipients))
//            throw new EdmException("Taki id s�ownika odbiorca nie istnieje!");

        //doda� walidacj� czy element s�ownika nie jest ju� ustawiony na dokumencie
        Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		Date startDate = cal.getTime();

        for(ZadanieOdbiorca recipient : recipients){
            if(recipient.getUserRealizationDateChanged() != null &&
               recipient.getUserRealizationDateChanged().before(startDate)){
                throw new EdmException(sm.getString("dataXXXNieMozeBycDataZPrzeszlosci", ": zmiana terminu realizacji zadania"));
            }
            else if (recipient.getUserRealizationDate() != null &&
                     recipient.getUserRealizationDate().before(startDate)){
                throw new EdmException(sm.getString("dataXXXNieMozeBycDataZPrzeszlosci", ": termin realizacji zadania"));
            }
        }


    }

    @Override
	public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
	{
    	
    	Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		Date startDate = cal.getTime();
		System.out.println(kind.getCn());
		System.out.println(values);
		DSApi.context().setAttribute("saved", true);

        if (values.get("DOC_DATE_REMINDER") != null && (Date) values.get("DOC_DATE_REMINDER") != null
                && ((Date) values.get("DOC_DATE_REMINDER")).before(startDate))
        {
            throw new EdmException(sm.getString("dataXXXNieMozeBycDataZPrzeszlosci", ": przypomnienia o zadaniu"));

        } else if (values.get("DOC_DATE_REALIZATION") != null && (Date) values.get("DOC_DATE_REALIZATION") != null
                && ((Date) values.get("DOC_DATE_REALIZATION")).before(startDate))
        {
            throw new EdmException(sm.getString("dataXXXNieMozeBycDataZPrzeszlosci", ": termin realizacji zadania"));

        }


        Map<String, FieldData> slowniki = (Map<String, FieldData>) values.get("M_DICT_VALUES");
		if (slowniki != null)
		{
			for (Map.Entry<String, FieldData> entry : slowniki.entrySet())
			{
				if (entry.getKey().startsWith("RECIPIENT_"))
				{
					Map<String, FieldData> tr = (Map<String, FieldData>) entry.getValue();


					String action = (String) values.get("processAction");
					verifyCorrectValues(values);
					verifyExecutedAction(action, values, tr,documentId);
					

				}
			}
		}
	}

    /**
     * 
     * Metoda sprawdza poprawno�� danych
     * 
     * @param values
     * @throws EdmException
     */
	private void verifyCorrectValues(Map<String, Object> values) throws EdmException
	{
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		Date startDate = cal.getTime();

		if (values.get("DOC_DATE_REMINDER") != null && (Date) values.get("DOC_DATE_REMINDER") != null
				&& ((Date) values.get("DOC_DATE_REMINDER")).before(startDate))
		{
			throw new EdmException(sm.getString("dataXXXNieMozeBycDataZPrzeszlosci", ": przypomnienia o zadaniu"));

		} else if (values.get("DOC_DATE_REALIZATION") != null && (Date) values.get("DOC_DATE_REALIZATION") != null
				&& ((Date) values.get("DOC_DATE_REALIZATION")).before(startDate))
		{
			throw new EdmException(sm.getString("dataXXXNieMozeBycDataZPrzeszlosci", ": termin realizacji zadania"));

		}


		Map<String, FieldData> slowniki = (Map<String, FieldData>) values.get("M_DICT_VALUES");
		if (slowniki != null)
		{
			for (Map.Entry<String, FieldData> entry : slowniki.entrySet())
			{
				if (entry.getKey().startsWith("RECIPIENT_"))
				{
					Map<String, FieldData> recipientDictionaryValues = (Map<String, FieldData>) entry.getValue();

					if (recipientDictionaryValues.get("USER_REALIZATION_DATE_CHANGED") != null
							&& recipientDictionaryValues.get("USER_REALIZATION_DATE_CHANGED").getData() != null
							&& recipientDictionaryValues.get("USER_REALIZATION_DATE_CHANGED").getDateData().before(startDate))
					{

						throw new EdmException(sm.getString("dataXXXNieMozeBycDataZPrzeszlosci", ": zmiana terminu realizacji zadania"));

					} else if (recipientDictionaryValues.get("USER_REALIZATION_DATE") != null
							&& recipientDictionaryValues.get("USER_REALIZATION_DATE").getData() != null
							&& recipientDictionaryValues.get("USER_REALIZATION_DATE").getDateData().before(startDate))
					{

						throw new EdmException(sm.getString("dataXXXNieMozeBycDataZPrzeszlosci", ": termin realizacji zadania"));
					}
					else if (recipientDictionaryValues.get("USER_REALIZATION_DATE_CHANGED") != null
							&& recipientDictionaryValues.get("USER_REALIZATION_DATE_CHANGED").getData() != null){
						
								if(values.get("DOC_DATE_REALIZATION") != null && (Date) values.get("DOC_DATE_REALIZATION") != null && 
										( (Date) values.get("DOC_DATE_REALIZATION")).after(recipientDictionaryValues.get("USER_REALIZATION_DATE_CHANGED").getDateData())){
									
								}
								throw new EdmException(sm.getString("dataXXXNieMozeBycPuzniejszaNizDataXXX", ": zmiana terminu realizacji zadania" , "realizacji zadania "));
					}
				}
			}
		}
	}

	/**
	 * Metoda weryfikuje poprawno�� danych w momencie wykonywania akcji
	 * 
	 * @param action
	 * @param values
	 * @param recipientDictionaryValues
	 * @param documentId
	 * @throws EdmException
	 */
	private void verifyExecutedAction(String action, Map<String, Object> values, Map<String, FieldData> recipientDictionaryValues, Long documentId) throws EdmException
	{
		Long delegetUserId = null;
		Date changedDate = null;
		for (Map.Entry<String, FieldData> ent : recipientDictionaryValues.entrySet())
		{
			if (ent.getKey().equalsIgnoreCase("USER_ID") && ent.getValue() != null && ent.getValue().getLongData().equals(DSApi.context().getDSUser().getId()))
			{

				if (recipientDictionaryValues.get("DELEGATE_USER_ID") != null 
						&& recipientDictionaryValues.get("DELEGATE_USER_ID").getData()!=null && !recipientDictionaryValues.get("DELEGATE_USER_ID").getStringData().isEmpty())
					delegetUserId = recipientDictionaryValues.get("DELEGATE_USER_ID").getLongData();
				if (recipientDictionaryValues.get("USER_REALIZATION_DATE_CHANGED") != null && recipientDictionaryValues.get("USER_REALIZATION_DATE_CHANGED").getData()!=null)
					changedDate = recipientDictionaryValues.get("USER_REALIZATION_DATE_CHANGED").getDateData();
		

		}
		/*if("przekazane".equals(action)){}
		if("wykonalem".equals(action)){}
		if("niewykonam".equals(action)){}
		if("zmiana".equals(action)){}*/

		}
		if ("przekazane".equals(action) && delegetUserId == null)
		{
			throw new EdmException("Nie uzupelniono osoby do oddelegowania zadania ");
		} else if ("przekazane".equals(action) && delegetUserId != null)
		{
			DSApi.context().setAttribute("delegowany", delegetUserId);
		} else if ("zmiana".equals(action) && changedDate == null)
		{
			throw new EdmException("Nie uzupelniono daty zmiany realizacji zadania ");
		}
		
		if ("zmiana".equals(action) && changedDate != null){

			DSApi.context().setAttribute("terminPrzelozony", changedDate);
			DSApi.context().setAttribute("processAction", action);
		}
		else if(documentId!=null && action==null){
			throw new EdmException(sm.getString("akcjaKtoraWykonalesJestNiemozliwaNaTymDokumencieWykonajAkcjeProcesowa"));
		}
		
	}
}

