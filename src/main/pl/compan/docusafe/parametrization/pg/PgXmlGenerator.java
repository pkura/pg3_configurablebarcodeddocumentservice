package pl.compan.docusafe.parametrization.pg;



import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.DocumentFactory;
import org.dom4j.ProcessingInstruction;
import org.dom4j.io.SAXReader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.XmlGenerator;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.general.mapers.utils.strings.PolishStringUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;



public class PgXmlGenerator extends XmlGenerator
{
	public PgXmlGenerator(OfficeDocument document)
	{
		super(document);
		// TODO Auto-generated constructor stub
		this.document = document;
		
	}
	private final static Logger log = LoggerFactory.getLogger(XmlGenerator.class);
     private byte[] xml;
    private String sha1;
    private Document xmlDoc;
    private OfficeDocument document;
	@Override
    public void addXls(Document xmlDoc, Element rootElement)
   	{
		//AddXSL(xmlDoc,rootElement);
   	}

	@Override
	public void addData(Document xmlDoc, Element rootElement)
	{
		Element headAssignment = addHeadElementAssignmentHistory(xmlDoc, rootElement);
		Element headWork = addHeadElementWorkHistory(xmlDoc, rootElement);
		try
		{
			for (AssignmentHistoryEntry ahe : document.getAssignmentHistory())
			{

				addAssignmentHistoryElement(ahe, xmlDoc, headAssignment);
			}
			for (Audit audit : document.getWorkHistory())
			{
				addWorkHistoryElement(audit, xmlDoc, headWork);
			}

		} catch (UserNotFoundException e)
		{
			log.error("", e);
		} catch (EdmException e)
		{
			log.error("", e);;
		}
	}
	@Override
	public void createElementWithTextValue(Element rootElement, String elementName, String elementValue) {
        Element element = getXmlDoc().createElement(elementName);
        element.setTextContent(replaceAllPoolishSymbols(elementValue));
        rootElement.appendChild(element);
    }
	@Override
	public Map<String, String> personToMap(Person person) {
        Map<String, String> map = new HashMap<String, String>();

        map.put("title", replaceAllPoolishSymbols(person.getTitle()));
        map.put("firstname", replaceAllPoolishSymbols(person.getFirstname()));
        map.put("lastname", replaceAllPoolishSymbols(person.getLastname()));
        map.put("organization", replaceAllPoolishSymbols(person.getOrganization()));
        map.put("street", replaceAllPoolishSymbols(person.getStreet()));
        map.put("zip", person.getZip());
        map.put("location", replaceAllPoolishSymbols(person.getLocation()));
        map.put("country", replaceAllPoolishSymbols(person.getCountry()));
        map.put("email", person.getEmail());
        map.put("phoneNumber", person.getPhoneNumber());
        map.put("fax", person.getFax());
        map.put("pesel", person.getPesel());
        map.put("nip", person.getRegon());
        map.put("remarks", replaceAllPoolishSymbols(person.getRemarks()));

        return map;
    }

	private void AddXSL(Document xmlDoc, Element rootElement)
	{

		try
		{
			//String stylePath = "file:///" + Docusafe.getHome().getPath() + "\\XSL\\" +"dane_style.xsl // Docusafe.getAdditionPropertyOrDefault("SigningXmlDocumentStyleFile", " ");
			String stylePath = "file:///" + Docusafe.getHome().getPath() + "\\XSL\\" + "dane_style.xsl";
			Node pi = xmlDoc.createProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"" + stylePath + "\"");
			xmlDoc.insertBefore(pi, rootElement);
		} catch (Exception e)
		{
			log.error("blad parsowania ", e);
		}
	}
	private Element addHeadElementWorkHistory(Document xmlDoc, Element rootElement)
	{
		Element el = xmlDoc.createElement("document-work-history");
		  rootElement.appendChild(el);
		   return el;
	}

	private Element addHeadElementAssignmentHistory(Document xmlDoc, Element rootElement)
	{

		 Element el = xmlDoc.createElement("document-decretation-history");
		   rootElement.appendChild(el);
		   return el;
	}

	private void addAssignmentHistoryElement(AssignmentHistoryEntry ahe, Document xmlDoc, Element headAssignment) throws UserNotFoundException, EdmException
	{
		//DateUtils.formatSqlDateTime(ahe.getCtime())
		Element aheELement = xmlDoc.createElement("decretation");
		aheELement.setAttribute("ID", String.valueOf(ahe.getId()));
		aheELement.setTextContent("time :" + ahe.getCtime().toLocaleString());
		headAssignment.appendChild(aheELement);
		DSUser odbiorca = null;
		DSUser dekretujacy = DSUser.findByUsername(ahe.getSourceUser());
		boolean targetUserExist = !ahe.getTargetUser().equals("x");
		boolean targetdivisionExist = !ahe.getTargetGuid().equals("x");
		if (targetUserExist)
			odbiorca = DSUser.findByUsername(ahe.getTargetUser());
		Element dekretElement = addDekretujacyElement(xmlDoc, aheELement);
		Element odbiorcaElement = addOdbiorcaElement(xmlDoc, aheELement);

		addHistoryData(xmlDoc, dekretElement, "type", replaceAllPoolishSymbols((ahe.getType() != null ? ahe.getType().toString() : "")));
		addHistoryData(xmlDoc, dekretElement, "objective", replaceAllPoolishSymbols((ahe.getObjective() != null ? ahe.getObjective() : "")));
		addHistoryData(xmlDoc, dekretElement, "user", replaceAllPoolishSymbols(dekretujacy.asFirstnameLastname()));
		addHistoryData(xmlDoc, dekretElement, "user-external-name", replaceAllPoolishSymbols((dekretujacy.getExternalName() != null ? dekretujacy.getExternalName() : "")));
		addHistoryData(xmlDoc, dekretElement, "user-division-name", replaceAllPoolishSymbols((ahe.getSourceGuid() != null ? ahe.getSourceGuid() : "")));
		if (targetUserExist)
		{
			addHistoryData(xmlDoc, odbiorcaElement, "user", replaceAllPoolishSymbols(odbiorca.asFirstnameLastname()));
			addHistoryData(xmlDoc, odbiorcaElement, "user-external-name", replaceAllPoolishSymbols((odbiorca.getExternalName() != null ? odbiorca.getExternalName() : "")));
			addHistoryData(xmlDoc, dekretElement, "user-division-name", replaceAllPoolishSymbols((ahe.getTargetGuid() != null ? ahe.getTargetGuid() : "")));
		} else if (targetdivisionExist)
		{
			addHistoryData(xmlDoc, odbiorcaElement, "division-name", replaceAllPoolishSymbols(DSDivision.find(ahe.getTargetGuid()).getName()));
		}
	}
	private String  replaceAllPoolishSymbols(String name){
		if(name!=null){
		PolishStringUtils psu = new PolishStringUtils();
		return	psu.zamienPolskieZnaki(name);
		}
		return "";
	}					   
	private Element addOdbiorcaElement(Document xmlDoc, Element aheELement)
	{
		 Element el = xmlDoc.createElement("decretation-recipient");
		 aheELement.appendChild(el);
		   return el;
	}

	private Element addDekretujacyElement(Document xmlDoc, Element aheELement)
	{
		 Element el = xmlDoc.createElement("decretation-sender");
		 aheELement.appendChild(el);
		   return el;
	}

	

	private void addHistoryData(Document xmlDoc, Element element, String data, String opis)
	{
		 Element el = xmlDoc.createElement(data);
		 el.setTextContent(opis);
		 element.appendChild(el);
	}

	private void addWorkHistoryElement(Audit audit, Document xmlDoc, Element headWork) throws UserNotFoundException, EdmException
	{
		 Element entry = xmlDoc.createElement("history-entry");
		 entry.setTextContent("time :" +audit.getCtime().toLocaleString());
		headWork.appendChild(entry);
		DSUser userEntry = DSUser.findByUsername(audit.getUsername());
		addHistoryData(xmlDoc, entry, "user", replaceAllPoolishSymbols(userEntry.asFirstnameLastname()));
		addHistoryData(xmlDoc ,entry , "user-external-name" ,replaceAllPoolishSymbols((userEntry.getExternalName()!=null ?userEntry.getExternalName() :""))); 
		addHistoryData(xmlDoc, entry, "entry-description", replaceAllPoolishSymbols((audit.getDescription()!=null ?audit.getDescription() :"")));
		
	}
   	}