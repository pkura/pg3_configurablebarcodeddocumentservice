package pl.compan.docusafe.parametrization.pg;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.task.IdentityLink;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.EmailConstants;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.jfree.xml.ElementDefinitionException;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.EnumItemImpl;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.events.handlers.PgZadanieReminder;
import pl.compan.docusafe.service.ServiceDriverNotSelectedException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.ServiceNotFoundException;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

/**
 * <h1>ZadanieProcessBean</h1>
 *
 * <p>
 *     Klasa jest obserwatorem procesu
 * </p>
 * @author Grzegorz Filip
 */
public class ZadanieProcessBean implements TaskListener
{

	private Logger logger = LoggerFactory.getLogger(ZadanieProcessBean.class);
	private String documentUrl = Docusafe.getBaseUrl() + "/office/internal/document-archive.action?documentId=";
	private StringManager sm = GlobalPreferences.loadPropertiesFile("", null);
	/**
	 * Metoda powaidamia mailowo u�ytkownika o tym �e pismo znalaz�o si� na jego li�cie zada�
	 */
	@Override
	public void notify(DelegateTask delegateTask)
	{
		Long docId = (Long) delegateTask.getVariable("docId");
		for (IdentityLink il : delegateTask.getCandidates())
		{
			try
			{
				String email = DSUser.findByUsername(il.getUserId()).getEmail();
				String msg = "Dokument : " + docId + " pojawil sie na liscie zadan w celu: " + delegateTask.getName();
				((Mailer) ServiceManager.getService(Mailer.NAME)).send(email, email, "nowe zadanie na liscie", msg);

			} catch (Exception e)
			{
				LoggerFactory.getLogger("tomekl").debug(e.getMessage(), e);
			}
			LoggerFactory.getLogger("tomekl").debug("{} {} {}", docId, il.getUserId(), delegateTask.getName());
		}
	}

	private static final Logger LOG = LoggerFactory.getLogger(ZadanieProcessBean.class);

	/**
	 * Metoda pobiera list� u�ytkownik�w dla dzia�u o <code>guid</code>
	 * 
	 * @param guid - identyfikator dzia�u
	 * @return
	 */
	public List<String> getDivisionUsers(String guid)
	{
		try
		{
			List<String> r = new ArrayList<String>();
			DSUser[] users = DSDivision.find(guid).getUsers();
			for (DSUser u : users)
			{
				r.add(u.getName());
			}
			return r;
		} catch (Exception e)
		{
			LoggerFactory.getLogger("tomekl").debug(e.getMessage(), e);
			return Collections.EMPTY_LIST;
		}

	}

	/**
	 * Metoda pobiera autora pisma
	 * 
	 * @param documentId
	 * @return
	 * @throws DocumentNotFoundException
	 * @throws EdmException
	 */
	public String getAutor(Long documentId) throws DocumentNotFoundException, EdmException
	{
		return OfficeDocument.findOfficeDocument(documentId, false).getAuthor();

	}
	
	/**
	 * Metoda zwraca u�ytkownika delegowanego do zadania.
	 * 
	 * @param docId - id dokumentu
	 * @param delegujacy - nazwa delegujacego
	 * @return
	 * @throws DocumentNotFoundException
	 * @throws EdmException
	 */
	public String getDelegowany(Long docId, String delegujacy) throws DocumentNotFoundException, EdmException
	{
		OfficeDocument doc = (OfficeDocument) Document.find(docId, false);
		FieldsManager fm = doc.getFieldsManager();

		String ids = fm.getStringKey("RECIPIENT");
		List<ZadanieOdbiorca> odbiorcy = ZadanieOdbiorca.getOdbiorcyFromIds(ids);


		if (!odbiorcy.isEmpty())
		{
			for (ZadanieOdbiorca odb : odbiorcy)
			{
				if (getOdbiorcatName(odb.getUserId()).equals(delegujacy))
				{
					if (odb.getDelegateUserId() != null)
					{
						return DSUser.findById(odb.getDelegateUserId()).getName();
					} else
					{
						Long userDelegowanyId = (Long) DSApi.context().getAttribute("delegowany");
						odb.setDelegateUserId(userDelegowanyId);
						DSApi.context().session().save(odb);
						return DSUser.findById(userDelegowanyId).getName();
					}
				}
			}
		}
		return delegujacy;

	}

	/**
	 * Metoda tworzy delegowanego odbiorce do zadania
	 * 
	 * @param docId - id dokumentu
	 * @param dekretujacy - nazwa dekretuj�cego
	 */
	public void createDelegeteRecipient(Long docId, String dekretujacy)
	{
		try
		{
			OfficeDocument doc = OfficeDocument.findOfficeDocument(docId, false);
			FieldsManager fm = doc.getFieldsManager();
			fm.initializeValues();
			String ids = fm.getStringKey("RECIPIENT");
			ZadanieOdbiorca newOdbm = null;
			List<ZadanieOdbiorca> odbiorcy = ZadanieOdbiorca.getOdbiorcyFromIds(ids);
			newOdbm = new ZadanieOdbiorca();
			if (!odbiorcy.isEmpty())
			{

				Criteria criteria = DSApi.context().session().createCriteria(ZadanieOdbiorca.class);

				criteria.add(Restrictions.eq("userId", DSApi.context().getDSUser().getId()));
				criteria.add(Restrictions.eq("showOnSelect", true));
				criteria.addOrder(Order.desc("id"));
				List<ZadanieOdbiorca> currentOdb = criteria.list();
				if (!currentOdb.isEmpty())
				{
					przygotujOdbiorce(dekretujacy, newOdbm, odbiorcy, currentOdb);
				}
			}
			if (newOdbm != null && newOdbm.getId() != null)
			{

				String sql = "insert into " + doc.getDocumentKind().getMultipleTableName() + " (DOCUMENT_ID,FIELD_CN,FIELD_VAL) values (?,?,?)";

				PreparedStatement ps = DSApi.context().prepareStatement(sql);

				ps.setLong(1, docId);
				ps.setString(2, "RECIPIENT");
				ps.setLong(3, newOdbm.getId());
				ps.execute();
				ps.close();
			}


		} catch (SQLException e)

		{
			logger.error("", e);
		} catch (EdmException e)
		{
			logger.error("", e);

		}
	}

	/**
	 * Metoda 
	 * 
	 * @param docId
	 * @param deleguj�cy
	 * @throws UserNotFoundException
	 * @throws EdmException
	 */
	public void rejectedDelegationByRecipient(Long docId, String deleguj�cy) throws UserNotFoundException, EdmException
	{
		OfficeDocument doc = OfficeDocument.findOfficeDocument(docId, false);
		FieldsManager fm = doc.getFieldsManager();
		fm.initializeValues();
		String ids = fm.getStringKey("RECIPIENT");

		List<ZadanieOdbiorca> odbiorcy = ZadanieOdbiorca.getOdbiorcyFromIds(ids);

		if (!odbiorcy.isEmpty())
		{
			for (ZadanieOdbiorca old : odbiorcy)
			{
				String odbiorcaName = getOdbiorcatName(old.getUserId());
				if (odbiorcaName.equals(deleguj�cy))
				{
					/*	<enum-item id="7" cn="rejectedDelegationSender" title="Odrzucono Delegowane zadanie - W realizacji"/>
						<enum-item id="8" cn="rejectedDelegationRecipient" title="Odrzucono Delegowane zadanie - W realizacji"/>*/
					old.setUserStatus(7);
					StringBuilder delegateMessageSender = new StringBuilder("Zadanie oddelegowane do ");
					delegateMessageSender.append(DSUser.findById(DSApi.context().getDSUser().getId()).getLastnameFirstnameWithOptionalIdentifier());
					delegateMessageSender.append(" , zosta�o niezakceptowane w dniu : " +DateUtils.formatJsDateTime(new Date()));
					old.setDelegateMessage(delegateMessageSender.toString());
					DSApi.context().session().saveOrUpdate(old);
				}
			}
		}
	}

	/**
	 * Metoda przygowowuje odbiorce dla procesu zadanie
	 * 
	 * @param dekretujacy
	 * @param newOdbm
	 * @param odbiorcy
	 * @param currentOdb
	 * @throws EdmException
	 */
	private void przygotujOdbiorce(String dekretujacy, ZadanieOdbiorca newOdbm, List<ZadanieOdbiorca> odbiorcy, List<ZadanieOdbiorca> currentOdb)
			throws EdmException
	{
		StringBuilder delegateMessageRecipient = new StringBuilder("Zadanie to  zosta�o oddelegowane przez ");
		StringBuilder delegateMessageSender = new StringBuilder("Zadanie to zosta�o oddelegowane do ");

		delegateMessageRecipient.append(DSUser.findByUsername(dekretujacy).getLastnameFirstnameWithOptionalIdentifier());
		delegateMessageRecipient.append(" ,oraz zosta�o  zaakceptowane przez Ciebie, w dniu :" + DateUtils.formatJsDateTime(new Date()));
		ZadanieOdbiorca newDefault = currentOdb.get(0);
		for (ZadanieOdbiorca old : odbiorcy)
		{
			String odbiorcaName = getOdbiorcatName(old.getUserId());
			if (odbiorcaName.equals(dekretujacy))
			{
				delegateMessageSender.append(DSUser.findByUsername(newDefault.getName()).getLastnameFirstnameWithOptionalIdentifier());
				delegateMessageSender.append(" ,i zosta�o zaakceptowane, w dniu :" + DateUtils.formatJsDateTime(new Date()));
				old.setDelegateMessage(delegateMessageSender.toString());
				newOdbm.setDelegateMessage(delegateMessageRecipient.toString());
				newOdbm.setDivision(newDefault.getDivision());
				newOdbm.setUserId(newDefault.getUserId());
				newOdbm.setFirstname(newDefault.getFirstname());
				newOdbm.setLastname(newDefault.getLastname());
				newOdbm.setName((newDefault.getName()!=null?newDefault.getName() : ""));
				newOdbm.setShowOnSelect(false);
				if (old.getDescription() != null)
					newOdbm.setDescription(old.getDescription());
				if (old.getUserRealizationDate() != null)
					newOdbm.setUserRealizationDate(old.getUserRealizationDate());


				/*<enum-item id="5" cn="acceptetDelegationSender" title="Delegowany zaakceptowa� zadanie -W realizacji"/>
				<enum-item id="6" cn="acceptetDelegationRecipient" title="Zaakceptowano delegowane zadanie - W realizacji"/>
				*/
				old.setUserStatus(5);
				newOdbm.setUserStatus(6);

				DSApi.context().session().save(newOdbm);
				DSApi.context().session().saveOrUpdate(old);
			}
		}
	}


	public String currentUser()
	{
		return DSApi.context().getPrincipalName();

	}
	
	/**
	 * Metoda wysy�a maila do autora informuj�cego o statusie zadania.
	 * 
	 * @param docId
	 * @param odbiorca
	 * @param tresc
	 * @param addRecipientInfo
	 * @throws ServiceNotFoundException
	 * @throws ServiceDriverNotSelectedException
	 * @throws EdmException
	 */
	public void sendEmail(Long docId, String odbiorca, String tresc, Boolean addRecipientInfo) throws ServiceNotFoundException,
			ServiceDriverNotSelectedException, EdmException
	{


		OfficeDocument doc = OfficeDocument.findOfficeDocument(docId, false);
		FieldsManager fm = doc.getFieldsManager();
		fm.initializeValues();
		String ids = fm.getStringKey("RECIPIENT");
		DSUser autorDokumentu = DSUser.findByUsername(doc.getAuthor());
		DSUser currentUser = DSApi.context().getDSUser();
		DSUser userOdbiorca = null;
		List<ZadanieOdbiorca> odbiorcy = ZadanieOdbiorca.getOdbiorcyFromIds(ids);
		if (!odbiorcy.isEmpty())
		{
			for (ZadanieOdbiorca odb : odbiorcy)
			{
				if (getOdbiorcatName(odb.getUserId()).equals(odbiorca))
				{
					if (odb.getDelegateUserId() != null)
						userOdbiorca = DSUser.findById(odb.getDelegateUserId());

					String status = ZadanieLogic.userStatusItem.get(odb.getUserStatus());

					StringBuilder sb = new StringBuilder("Zlecone przez Ciebie zadnaie zosta�o zmodyfikowane: ");
					addHtmlBreakeLineTag(sb, 1);
					sb.append("U�ytkownik " + currentUser.getLastnameFirstnameWithOptionalIdentifier() + " wykona� akcj�: ");
					sb.append(tresc);
					sb.append(" " + status);
					if (addRecipientInfo && userOdbiorca != null)
					{
						sb.append(" do u�ytkownika " + userOdbiorca.getLastnameFirstnameWithOptionalIdentifier());
					}
					addHtmlBreakeLineTag(sb, 1);
					sb.append("Data utworzenia zadania :");
					sb.append(DateUtils.formatJsDateTime(doc.getCtime()));
					addHtmlBreakeLineTag(sb, 1);

					if (odb.getUserRealizationDate() != null)
					{
						sb.append("Data realizacji zadania : ");
						sb.append(odb.getUserRealizationDate());
						addHtmlBreakeLineTag(sb, 1);
					}
					sb.append("Odbiorca zadania : ");
					sb.append(DSUser.findByUsername(getOdbiorcatName(odb.getUserId())).getLastnameFirstnameWithOptionalIdentifier());
					addHtmlBreakeLineTag(sb, 1);

					sb.append("Tytu� zadania : ");
					sb.append(doc.getDescription());
					addHtmlBreakeLineTag(sb, 1);

					if (doc.getAbstractDescription() != null)
					{
						sb.append("Opis zadania : ");
						sb.append(doc.getAbstractDescription());
						addHtmlBreakeLineTag(sb, 1);
					}
					if (odb.getDescription() != null)
					{
						sb.append("Opis szczego�owy zadania : ");
						sb.append(odb.getDescription());
						addHtmlBreakeLineTag(sb, 1);
					}
					/*sb.append("U�ytkownik " + currentUser.getLastnameFirstnameWithOptionalIdentifier() + " wykona� akcj�: ");
					sb.append(tresc);
					sb.append(" " + status);
					if (addRecipientInfo &&userOdbiorca!=null )
					{
						sb.append(" do u�ytkownika " + userOdbiorca.getLastnameFirstnameWithOptionalIdentifier());
					}
					//addHtmlBreakeLineTag(sb, 1);

					addHtmlBreakeLineTag(sb, 2);*/
					sb.append("Link do zadania : ");
					//addHtmlBreakeLineTag(sb, 1);
					sb.append(getActionLinkToEmail(doc.getId(), "Przejdz"));



					((Mailer) ServiceManager.getService(Mailer.NAME)).send(autorDokumentu.getEmail(), autorDokumentu.getEmail(),
							"Zmiana statusu twojego zadania", sb.toString(), EmailConstants.TEXT_HTML);
				}

			}


		}
	}
	
	/**
	 * Metoda zwraca link do dokumentu
	 * 
	 * @param id - id dokumentu
	 * @param name - nazwa linku
	 * @return
	 */
	private String getActionLinkToEmail(Long id, String name)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<a href='" + documentUrl + id + "'>" + name + "</a>");
		addHtmlBreakeLineTag(sb, 1);

		return sb.toString();
	}

	/**
	 * Metoda dodaje nowe linie do wiadomi�ci html
	 * 
	 * @param sb
	 * @param countElements
	 */
	public void addHtmlBreakeLineTag(StringBuilder sb, Integer countElements)
	{
		if (countElements != null)
		{
			for (int i = 0; i <= countElements; i++)
			{
				sb.append("<br></br>");
			}
		} else
			sb.append("<br></br>");

	}
	
	/**
	 * Metoda ustawia status odbiorc�w w procesie.
	 * 
	 * @param docId - id dokumentu
	 * @param odbiorca - identyfikator odbiorcy
	 * @param status - status odbiorcy
	 * @throws DocumentNotFoundException
	 * @throws EdmException
	 */
	public void setupStatus(Long docId, String odbiorca, Integer status) throws DocumentNotFoundException, EdmException
	{

		OfficeDocument doc = OfficeDocument.findOfficeDocument(docId, false);
		FieldsManager fm = doc.getFieldsManager();
		fm.initializeValues();
		String ids = fm.getStringKey("RECIPIENT");

		List<ZadanieOdbiorca> odbiorcy = ZadanieOdbiorca.getOdbiorcyFromIds(ids);

		if (!odbiorcy.isEmpty())
		{
			for (ZadanieOdbiorca odb : odbiorcy)
			{
				if (getOdbiorcatName(odb.getUserId()).equals(odbiorca))
				{
					odb.setUserStatus(status);
					Date d = (DSApi.context().getAttribute("terminPrzelozony")!=null? (Date) (DSApi.context().getAttribute("terminPrzelozony")): null) ;
					String action =  (DSApi.context().getAttribute("processAction")!=null? (String) (DSApi.context().getAttribute("processAction")): null) ;
					
					if(d!=null && action!=null && action.equals("zmiana")){
						odb.setUserRealizationDateChanged(d);
					}
					DSApi.context().session().saveOrUpdate(odb);
				}
			}
		}
	}

	/**
	 * Metoda usuwa date zmienion� date realizacji zadania
	 * 
	 * @param docId - id dokumentu
	 * @param odbiorca - identyfikator odbiorcy
	 * @throws DocumentNotFoundException
	 * @throws EdmException
	 */
	public void removeRealizationDateChanged(Long docId, String odbiorca) throws DocumentNotFoundException, EdmException
	{

		OfficeDocument doc = OfficeDocument.findOfficeDocument(docId, false);
		FieldsManager fm = doc.getFieldsManager();
		fm.initializeValues();
		String ids = fm.getStringKey("RECIPIENT");

		List<ZadanieOdbiorca> odbiorcy = ZadanieOdbiorca.getOdbiorcyFromIds(ids);

		if (!odbiorcy.isEmpty())
		{
			for (ZadanieOdbiorca odb : odbiorcy)
			{
				if (getOdbiorcatName(odb.getUserId()).equals(odbiorca))
				{
					odb.setUserRealizationDateChanged(null);
					DSApi.context().session().saveOrUpdate(odb);
				}
			}
		}
	}
	/**
	 * Metoda ustawia status odbiorcy
	 * 
	 * @param docId - id dokumentu
	 * @param task - 
	 * @param action - nazwa akcji
	 * @param odbiorca
	 * @param delegowany
	 * @param status
	 * @throws DocumentNotFoundException
	 * @throws EdmException
	 */
	public void setupStatusAccepted(Long docId, DelegateTask task, String action, String odbiorca, String delegowany, Integer status)
			throws DocumentNotFoundException, EdmException
	{

		OfficeDocument doc = OfficeDocument.findOfficeDocument(docId, false);
		FieldsManager fm = doc.getFieldsManager();
		fm.initializeValues();
		String ids = fm.getStringKey("RECIPIENT");

		List<ZadanieOdbiorca> odbiorcy = ZadanieOdbiorca.getOdbiorcyFromIds(ids);

		if (!odbiorcy.isEmpty())
		{
			for (ZadanieOdbiorca old : odbiorcy)
			{
				if (old.getName().equals(odbiorca))
				{

					DSApi.context().session().saveOrUpdate(old);
				}
			}
		}
	}
	
	/**
	 * Metoda ustawia status dokumentu
	 * 
	 * @param docId - id dokumentu
	 * @param statusKey
	 */
	public void setStatus(Long docId, String statusKey)
	{
		try
		{
			Document doc = OfficeDocument.findOfficeDocument(docId, false);
			int statusId = doc.getDocumentKind().getFieldByCn("STATUS").getEnumItemByCn(statusKey).getId();
			Map<String, Object> m = new HashMap<String, Object>();
			m.put("STATUS", statusId);
			doc.getDocumentKind().setOnly(docId, m, false);
		} catch (Exception e)
		{
			LOG.debug(e.getMessage(), e);
		}
	}
	/**
	 * Metoda tworzy przypomnienie dla dokumentu w procesie zadanie.
	 * 
	 * @param docId - id dokumentu
	 */
	 public void createRemindEvent(Long docId) {
	        try {
	            OfficeDocument doc = OfficeDocument.findOfficeDocument(docId,false);
	            doc.getFieldsManager();
	         Date date = (Date) doc.getFieldsManager().getValue("DOC_DATE_REMINDER");
	         
	         if(date!=null)
	           PgZadanieReminder.createRemindEvent(docId, date, docId.toString(),"");
	            
	           
	        } catch (Exception e) {
	            LOG.debug(e.getMessage(),e);
	        }
	    }

	/**
	 * Metoda do wykorzystania jako activiti/spring bean <br/>
	 * w tasku w listenerach wybra� event: <code>create</code> oraz typ <code>Expression</code> i zapisa� w postaci <br/>
	 * <code>${historyHandler.addCandidatesToHistory(docId,task,description)}</code>
	 * @param docId
	 * @param task
	 * @throws pl.compan.docusafe.core.EdmException
	 */
	public void addCandidatesToHistory(Long docId, DelegateTask task, String description) throws EdmException
	{
		OfficeDocument doc = OfficeDocument.findOfficeDocument(docId, false);
		List<ZadanieOdbiorca> listaOdbiiorcow = ZadanieOdbiorca.getOdbiorcyFromIds(doc.getFieldsManager().getStringKey("RECIPIENT"));
		String processName = task.getProcessDefinitionId();
		for (IdentityLink candidate : task.getCandidates())
		{
			String candidateUser = candidate.getUserId();
			String guid = candidate.getGroupId();
			if (candidateUser != null && StringUtils.isNotBlank(candidateUser) && StringUtils.isBlank(guid))
			{
				DSUser user = DSUser.findByUsername(candidateUser);
				if (user != null)
				{
					DSDivision[] div = user.getOriginalDivisionsWithoutGroup();
					if (div.length > 0)
					{
						guid = div[0].getGuid();
					}
				}
			}
			String od = "Od : " + DSApi.context().getDSUser().getLastnameFirstnameWithOptionalIdentifier() + " - ";
			
				if(doc.getFieldsManager().getIntegerKey("STATUS") == 18){
					description = sm.getString("zadanieProcesBean.cykliczne");
				}
						
			for (ZadanieOdbiorca odb : listaOdbiiorcow)
			{
				
				String odbiorcaName = getOdbiorcatName(odb.getUserId());
				if (description != null && !description.isEmpty() && (odbiorcaName.equals(candidateUser))
						|| (doc.getAuthor().equals(candidateUser) && odbiorcaName.equals(DSApi.context().getDSUser().getName())))
				{
					task.setDescription(od + description);
					if (odb.getUserStatus() > 7 || odb.getUserStatus() < 5)
						odb.setDelegateMessage(od + description);
				} else if (odbiorcaName.equals(candidateUser)
						|| (doc.getAuthor().equals(candidateUser) && odbiorcaName.equals(DSApi.context().getDSUser().getName())))
				{
					description = ZadanieLogic.userStatusItem.get(odb.getUserStatus());
					task.setDescription(od + description);
					if (odb.getUserStatus() > 7 || odb.getUserStatus() < 5)
						odb.setDelegateMessage(od + description);

				} else if (description != null && !description.isEmpty() && odbiorcaName.equals(DSApi.context().getDSUser().getName())
						&& odb.getDelegateUserId() != null && (DSUser.findById(odb.getDelegateUserId()).getName().equals(candidateUser)))
				{

					task.setDescription(od + description);
					odb.setDelegateMessage(od + description);

				}
				else {
					if(description != null)
					task.setDescription(od + description);
					else 
						task.setDescription(task.getName());
				}

			}

			addCandidatesToHistory(doc, processName, description, candidateUser, guid);
		}
	}
	
	private String getOdbiorcatName(Long id) throws UserNotFoundException, EdmException
	{
		return DSUser.findById(id).getName();
	}

	/**
	 * Dodaje wpis do Historii dekretacji typu <br/>
	 * "Przekaza� zadanie do u�ytkownika .... (lub dzia�u .....) do etapu ....."
	 * @param doc
	 * @param processName
	 * @param status <u>warto��</u> pola STATUS
	 * @param userName
	 * @param guid
	 * @throws pl.compan.docusafe.core.EdmException
	 */
	protected void addCandidatesToHistory(OfficeDocument doc, String processName, String description, String userName, String guid) throws EdmException
	{
		AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
		ahe.setSourceUser(DSApi.context().getPrincipalName());
		ahe.setProcessName(processName);
		if (userName == null)
		{
			ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_DIVISION);
			ahe.setTargetGuid(guid);
		} else
		{
			ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_SINGLE_USER);
			ahe.setTargetUser(userName);
		}
		DSDivision[] divs = DSApi.context().getDSUser().getOriginalDivisionsWithoutGroup();
		if (divs != null && divs.length > 0)
		{
			ahe.setSourceGuid(divs[0].getName());
		}

		ahe.setCtime(new Date());
		ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);

		if (description != null && !description.isEmpty())
		{
			ahe.setStatus(description);
			ahe.setObjective(description);
		} else
		{
			ahe.setStatus("x");
			ahe.setObjective("x");
		}


		doc.addAssignmentHistoryEntry(ahe);
	}
}
