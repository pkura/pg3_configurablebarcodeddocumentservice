package pl.compan.docusafe.parametrization.pg;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.util.Map;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

public class PgDokumentPublicznyLogic extends AbstractPgLogic {

	public static final String RECIPIENT_HERE = "RECIPIENT_HERE";
	public static final String ADD_RECIPIENT_HERE = "ADD_RECIPIENT_HERE";
	private static PgDokumentPublicznyLogic instance;

	public static PgDokumentPublicznyLogic getInstance() {
		if (instance == null)
			instance = new PgDokumentPublicznyLogic();
		return instance;
	}

	@Override
	public void onLoadData(FieldsManager fm) throws EdmException {
		// TODO Auto-generated method stub
		if (!DSApi.context().hasPermission(DSPermission.PISMO_WYCH_DOSTEP)) {
			fm.getField("DOSTEP").setHidden(true);
		} else {
			fm.getField("DOSTEP").setHidden(false);
		}

	}

	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ",
				ObjectPermission.GROUP, "Dokumenty - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,
				"DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP,
				"Dokumenty zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY,
				"DOCUMENT_READ_MODIFY", ObjectPermission.GROUP,
				"Dokumenty - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,
				"DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP,
				"Dokumenty zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE,
				"DOCUMENT_READ_DELETE", ObjectPermission.GROUP,
				"Dokumenty - usuwanie"));

		for (PermissionBean perm : perms) {
			if (perm.isCreateGroup()&& perm.getSubjectType().equalsIgnoreCase(
							ObjectPermission.GROUP)) {
				String groupName = perm.getGroupName();
				if (groupName == null) {
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}

	}

	public void validate(Map<String, Object> values, DocumentKind kind,
			Long documentId) throws EdmException {

	}

	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {
		if (DocumentLogic.TYPE_ARCHIVE == type)
			document.setFolder(Folder.getRootFolder());
		else
			document.setFolder(Folder.findSystemFolder(Folder.SN_OFFICE));
		document.setDoctypeOnly(null);
	}

	/**
	 * Metoda wywoływana przy starcie procesu
	 * 
	 * @param document
	 * @param event
	 */
	public void onStartProcess(OfficeDocument document, ActionEvent event) {
		log.info("--- PGDocInLogic : START PROCESS !!! ---- {}", event);
		try {

			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, DSApi.context().getDSUser().getName());
			event.setAttribute(ASSIGN_USER_PARAM, map.get(ASSIGN_USER_PARAM));
			// map.put(ASSIGN_DIVISION_GUID_PARAM,event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			DSDivision[] divs = DSApi.context().getDSUser()
					.getOriginalDivisionsWithoutGroup();
			if (divs.length > 0) {
				for (DSDivision d : divs) {
					map.put(ASSIGN_DIVISION_GUID_PARAM,d.getGuid() != null ? d.getGuid(): DSDivision.ROOT_GUID);
					event.setAttribute(ASSIGN_DIVISION_GUID_PARAM,map.get(ASSIGN_DIVISION_GUID_PARAM));
				}
			} else {
				map.put(ASSIGN_DIVISION_GUID_PARAM, DSDivision.ROOT_GUID);
				event.setAttribute(ASSIGN_DIVISION_GUID_PARAM,map.get(ASSIGN_DIVISION_GUID_PARAM));
			}

			document.getDocumentKind().getDockindInfo()
					.getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

	}

}
