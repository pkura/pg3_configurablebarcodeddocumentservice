
package pl.compan.docusafe.parametrization.pg;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.jbpm.api.model.OpenExecution;

import com.google.common.collect.Maps;

import edu.emory.mathcs.backport.java.util.Arrays;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.paa.Wpisanie_urlopu;
import pl.compan.docusafe.service.ServiceDriverNotSelectedException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.ServiceNotFoundException;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


public class PackageEmailLogic implements ExternalActivityBehaviour
{
	/**
	 * Logger
	 */
	private static final Logger log = LoggerFactory.getLogger(PackageEmailLogic.class);

	@SuppressWarnings("deprecation")
	public void notify(OpenExecution openExecution) throws Exception
	{
		Long docId = Long.parseLong(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		log.info("Email Logic Package  - doc id = " + docId);
		// pobranie obiektu dokumentu
		Document document = Document.find(docId, false);

		FieldsManager fm = document.getFieldsManager();
		String data = Calendar.getInstance().getTime().toString();
		String rodzajDokumentu = document.getDocumentKind().getCn();
		log.info("Dokument  id = " + docId.toString() + "Rodzaj =" + rodzajDokumentu + " czas zajscia procesu " + data);

		if (openExecution.hasVariable("dekretuj") && ((String) openExecution.getVariable("dekretuj")).contains("true"))
		{
			reaasignDocumentsFromPackage(fm, docId);
		}
		
		if (openExecution.hasVariable("createDWProcess") && ((String) openExecution.getVariable("createDWProcess")).contains("true"))
		{
			createDWProcessFromPackage(fm, docId);
			openExecution.setVariable("createDWProcess", "false");
		}
		
		//wysylanie maila o statusie paczki
		sendMail(fm, docId);


	}
/**
 * Pdzedekretowanie pism  b�docych w paczce na liste zadan odbiorcy
 * @param fm
 * @param packageDocumentId
 * @throws DocumentNotFoundException
 * @throws EdmException
 */
	private void reaasignDocumentsFromPackage(FieldsManager fm, Long packageDocumentId) throws DocumentNotFoundException, EdmException
	{
		boolean succesed = false;
		List<Long> documentsIds = new ArrayList();
		documentsIds = (List<Long>) (fm.getFieldValues().get(PgPackageLogic.multipleDOCUMENTS));

		OfficeDocument packageDocument = OfficeDocument.findOfficeDocument(packageDocumentId, false);
		StringBuilder documentsBarcodesList = new StringBuilder("Identyfikatory : ");
		Map<String, Object> params = Maps.newHashMap();
		//params.put(Jbpm4WorkflowFactory.OBJECTIVE, "Do realizacji z paczki dokument�w");
		params.put(Jbpm4WorkflowFactory.OBJECTIVE, "Do realizacji z przesy�ki ");
		if(documentsIds!=null){
		for (Long id : documentsIds)
		{
			succesed = false;
			OfficeDocument doc = OfficeDocument.findOfficeDocument(id,false);
			Map<String,Object> values = new HashMap<String, Object>();
			//dodajemy identyfikator paczki do dokumentu aby go powiazac ze soba
			values.put(PgPackageLogic.CN_DOC_FROM_PACKAGE, true);
			values.put(PgPackageLogic.CN_IDENTYFIKATOR_PACZKI, packageDocument.getBarcode());
			doc.getDocumentKind().setOnly(doc.getId(),values);
			addToHistory(doc, fm);
			
			for (String aid : Jbpm4ProcessLocator.taskIds(doc.getId()))
			{ TaskSnapshot taskSnapshot = TaskSnapshot.getInstance().findByActivityKey(aid.replaceAll("[^,]*,",""));
			  if (taskSnapshot!=null && taskSnapshot.getProcess().equals("jbpm4"))
				Jbpm4WorkflowFactory.reassign(aid, doc.getId(), doc.getAuthor(), DSApi.context().getDSUser().getName(), params);
				
			  succesed = true;
			  doc.setInPackage(false);
			  documentsBarcodesList.append( (doc.getBarcode()!=null ? doc.getBarcode(): "" )+",");
			}
			
		}
		if (succesed)
		{
			insertRemovedDocumentsToAdditionMEtadata(documentsBarcodesList ,packageDocumentId);
			wpisUsunieciaDokumentowZPaczki(documentsIds, fm, packageDocumentId);
			deleteFromDokindTable(fm, packageDocumentId);
		}
		}
		
			for (String aid : Jbpm4ProcessLocator.taskIds(packageDocumentId)){
				 TaskSnapshot taskSnapshot = TaskSnapshot.getInstance().findByActivityKey(aid.replaceAll("[^,]*,",""));
				  if (taskSnapshot!=null && !taskSnapshot.getProcess().equals("jbpm4")
						  && taskSnapshot.getActivity_lastActivityUser()!=null 
						  && taskSnapshot.getActivity_lastActivityUser().contains(Document.find(packageDocumentId,false).getAuthor()))
					  
					  WorkflowFactory.getInstance().manualFinish(aid, false);
			}
		}

	/**
	 * Pdzedekretowanie pism  bedacych w paczce na liste zadan odbiorcy
	 * @param fm
	 * @param packageDocumentId
	 * @throws DocumentNotFoundException
	 * @throws EdmException
	 */
	private void createDWProcessFromPackage(FieldsManager fm, Long packageDocumentId) throws DocumentNotFoundException, EdmException
	{

		DSUser[] users = null;
		DSUser odbiorca = null;
		OfficeDocument doc = OfficeDocument.find(packageDocumentId);
		
		setLokalizacjaPaczki(doc);
		Map<String, Object> params = Maps.newHashMap();
		params.put(Jbpm4WorkflowFactory.OBJECTIVE, "Do Wiadomosci - Zosta�a wys�ana do Ciebie Paczka");
		params.put(Jbpm4Constants.KEY_PROCESS_NAME, Jbpm4Constants.READ_ONLY_PROCESS_NAME);
		params.put(Jbpm4WorkflowFactory.DOCDESCRIPTION, "DW");

		if (fm.getFieldValues() != null && fm.getFieldValues().get(PgPackageLogic.CN_PACZKA_NA_DZIAL) != null
				&& ((Boolean) fm.getFieldValues().get(PgPackageLogic.CN_PACZKA_NA_DZIAL) == true))
		{

			DSDivision div = DSDivision.findById(fm.getEnumItem(PgPackageLogic.CN_DZIAL).getId());
			users = div.getUsers(false);
		
			if (users != null && AvailabilityManager.isAvailable("packageDocumentDwToDivision"))
			{
				params.put(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM, div.getGuid());
				doc.getDocumentKind().getDockindInfo().getProcessesDeclarations()
				.getProcess((String) params.get(Jbpm4Constants.KEY_PROCESS_NAME))
				.getLogic().startProcess(doc, params);
			}
				
				/*
				DSUser autor = DSUser.findByUsername(doc.getAuthor());
				if(!DSUser.findByUsername(doc.getAuthor()).inDivision(div)){
				//	users[users.length+1] = autor;
				}
				for (DSUser user : users)
				{ 
					if (!(user.getId() == DSApi.context().getDSUser().getId()))
					{
						params.put(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM, user.getName());
						doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess((String) params.get(Jbpm4Constants.KEY_PROCESS_NAME))
								.getLogic().startProcess(doc, params);
					}
				}*/
			
		} else
		{
			if(doc!=null){
				for(Recipient rec : doc.getRecipients()){
					odbiorca = DSUser.findByFirstnameLastname(rec.getFirstname(), rec.getLastname());
				}
			}
		//	odbiorca = DSUser.findByEmail((String) fm.getValue(PgPackageLogic.CN_EMAILODBIORCA));
			params.put(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM, odbiorca.getName());

			doc.getDocumentKind().getDockindInfo().getProcessesDeclarations()
			.getProcess((String) params.get(Jbpm4Constants.KEY_PROCESS_NAME))
			.getLogic().startProcess(doc, params);

		}
		
		/*if (fm.getFieldValues() != null && fm.getFieldValues().get(PgPackageLogic.CN_PACZKA_NA_DZIAL) != null
				&& ((Boolean) fm.getFieldValues().get(PgPackageLogic.CN_PACZKA_NA_DZIAL) == true))
		{

			DSDivision div = DSDivision.find(fm.getEnumItem(PgPackageLogic.CN_DZIAL).getId());
			users = div.getUsers(false);

			if (users != null && AvailabilityManager.isAvailable("packageDocumentDwToDivision"))
			{
				
				for (DSUser user : users)
				{
					if (!(user.getId() == DSApi.context().getDSUser().getId()))
					{
						params.put(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM, user.getName());
						doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess((String) params.get(Jbpm4Constants.KEY_PROCESS_NAME))
								.getLogic().startProcess(doc, params);
					}
				}
			} else
				{
				params.put(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM, div.getGuid());
				doc.getDocumentKind().getDockindInfo().getProcessesDeclarations()
				.getProcess((String) params.get(Jbpm4Constants.KEY_PROCESS_NAME))
				.getLogic().startProcess(doc, params);
				}


		} else
		{
			odbiorca = DSUser.findByEmail((String) fm.getValue(PgPackageLogic.CN_EMAILODBIORCA));
			params.put(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM, odbiorca.getName());

			doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess((String) params.get(Jbpm4Constants.KEY_PROCESS_NAME)).getLogic()
					.startProcess(doc, params);

		}*/

	}
	
	/**
	 * Metoda ustawia lokalizacje paczki
	 * 
	 * @param doc
	 * @throws UserNotFoundException
	 * @throws EdmException
	 */
	private void setLokalizacjaPaczki(OfficeDocument doc) throws UserNotFoundException, EdmException
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Paczka znajduje sie u :");
		sb.append(DSApi.context().getDSUser().asFirstnameLastname());
		sb.append( " i oczekuje na odbi�r ");
		
		Map<String,Object> values = Maps.newHashMap();
		values.put(PgPackageLogic.CN_LOKALIZACJA_PACZKI, sb.toString());
		doc.getDocumentKind().setOnly(doc.getId(),values);
		DSApi.context().session().update(doc);
		
	}
	/**
	 * usuniecie idk�w pism z paczki z tabeli dokindowej bo zostaly przedekretowane do odbiorcy
	 * @param fm
	 * @param packageDocumentId
	 */
	private void deleteFromDokindTable(FieldsManager fm, Long packageDocumentId)
	{
		try
		{
			String sql = "Delete from " + fm.getDocumentKind().getMultipleTableName() + " where DOCUMENT_ID = ? and FIELD_CN = ?";
			PreparedStatement ps = DSApi.context().prepareStatement(sql);

			ps.setLong(1, packageDocumentId);
			ps.setString(2, PgPackageLogic.multipleDOCUMENTS);
			ps.execute();
			ps.close();
			DSApi.context().session().flush();
		} catch (Exception e)
		{
			log.error("", e);
		}


	}
/**
 * Dodanie do histori pisam wpis o usunieciu dokumentu z paczki
 * @param doc
 * @param fm
 * @throws EdmException
 */
	private void addToHistory(OfficeDocument doc, FieldsManager fm) throws EdmException
	{
		String description = "Dokument usuni�to z paczki o kodzie kreskowym = " + fm.getFieldValues().get(PgPackageLogic.CN_KOD_KRESKOWY) + ".";

		Audit wpis = new Audit();
		wpis.setDescription(description);
		wpis.setCtime(new Date());
		wpis.setUsername(DSApi.context().getDSUser().getName());
		wpis.setProperty("giveAccept");
		wpis.setLparam("");
		wpis.setPriority(1);
		DataMartManager.addHistoryEntry(doc.getId(), wpis);


	}
/**
 * Dodanie do historii dokuemntu( paczki )wpisu o usunieciu  z niej dokument�w
 * @param documentsIds
 * @param fm
 * @param packageDocumentId
 * @throws DocumentNotFoundException
 * @throws EdmException
 */
	private void wpisUsunieciaDokumentowZPaczki(List<Long> documentsIds, FieldsManager fm, Long packageDocumentId) throws DocumentNotFoundException, EdmException
	{
		StringBuilder sbRemove = new StringBuilder();
		sbRemove.append("Z paczki usuni�to dokumenty  o ID : ");
		OfficeDocument doc = OfficeDocument.find(packageDocumentId);
		for (Long l : documentsIds)
			sbRemove.append("," + l);
		Audit wpis = new Audit();
		
		wpis.setDescription(sbRemove.toString());
		wpis.setCtime(new Date());
		wpis.setUsername(DSApi.context().getDSUser().getName());
		wpis.setProperty("giveAccept");
		wpis.setLparam("");
		wpis.setPriority(1);
		DataMartManager.addHistoryEntry(doc.getId(), wpis);
		
		
	}
/**
 * metoda robi inserta z barkodami pism kture by�y w tej paczce czyli dodaje wpis do tabeli dodatkowych metadanych  i do tabeli wi���cej z dokindem 
 * @param sbRemove
 * @param packageDocumentId
 */
	private void insertRemovedDocumentsToAdditionMEtadata(StringBuilder barcodesList, Long packageDocumentId)
	{
		try
		{
			OfficeDocument packageDocument = OfficeDocument.findOfficeDocument(packageDocumentId, false);
			String sqlMetadane = "insert into ds_ar_dodatkowe_metadane"
					+ " ( id,nazwapola, textdata) values(?,?,?)";
			String sqlPackageDocument = "insert into " + packageDocument.getDocumentKind().getMultipleTableName() 
					+ "(document_id, field_cn, field_val) values(?,?,?)";  
			PreparedStatement ps;
			Integer id_wpisu = null;
			ps = DSApi.context().prepareStatement("select nextval('ds_ar_dodatkowe_metadane_id_seq');");
			ResultSet rs = ps.executeQuery();
			if (rs.next()){
				 id_wpisu = rs.getInt(1);
			}			
			if(id_wpisu!=null){
			ps = DSApi.context().prepareStatement(sqlMetadane);
			ps.setInt(1, id_wpisu);
			ps.setString(2, "Dokumenty z paczki");
			ps.setString(3, "Identyfikatory pism :" + barcodesList.toString());
			ps.execute();
			ps = DSApi.context().prepareStatement(sqlPackageDocument);
			ps.setLong(1, packageDocument.getId());
			ps.setString(2, PgPackageLogic.CN_DODATKOWE_METADANE);
			ps.setString(3, id_wpisu.toString());
			ps.execute();
			}
			ps.close();
		} catch (SQLException e)
		{
			log.error("", e);
			DSApi.context()._rollback();
		} catch (EdmException e)
		{
			log.error("", e);
			DSApi.context()._rollback();
		}
	}
	private void sendMail(FieldsManager fm, Long docId) throws ServiceNotFoundException, ServiceDriverNotSelectedException, EdmException
	{
		OfficeDocument doc = OfficeDocument.find(docId);

		String emailNadawcy = "";
		String emailOdbiorca = "";
		String[] toEmail = null;
		String[] emailCC = null;

		if(fm.getFieldValues()!=null && fm.getFieldValues().get(PgPackageLogic.CN_PACZKA_NA_DZIAL)!=null 
				&& ((Boolean) fm.getFieldValues().get(PgPackageLogic.CN_PACZKA_NA_DZIAL)==true)){
			DSDivision div = DSDivision.findById(fm.getEnumItem(PgPackageLogic.CN_DZIAL).getId());
			 DSUser[] users = div.getUsers(false);
			 for (DSUser dsUser : users)
			{
				if (dsUser.getEmail()!=null )
					emailOdbiorca +=  ","+dsUser.getEmail();
			}
		}
		
		DSUser nadawca = null;
		DSUser odbiorca =null;
		
		
		if(doc!=null){
			for(Recipient rec : doc.getRecipients()){
				odbiorca = DSUser.findByFirstnameLastname(rec.getFirstname(), rec.getLastname());
			}
			//nadawca = DSUser.findByFirstnameLastname(doc.getSender().getFirstname(), doc.getSender().getLastname());
			nadawca = DSUser.findByUsername(doc.getAuthor());
		}
		if(nadawca!=null && nadawca.getEmail()!=null )
		emailNadawcy += ","+ nadawca.getEmail();
		if(emailOdbiorca.isEmpty() && odbiorca!=null && odbiorca.getEmail()!=null)
		emailOdbiorca += ","+odbiorca.getEmail();
		
		
		log.error("email odbiorca ="+emailOdbiorca);
		log.error("email nadawca ="+emailNadawcy);
		String  message = (String) fm.getValue(PgPackageLogic.CN_DOC_DESCRIPTION);
		StringBuilder emailDescription = new StringBuilder();
		StringBuilder emailTitle = new StringBuilder();
		emailTitle.append("Potwierdzenie dotyczace przesy�ki");
		emailTitle.append(" o kodzie kreskowym : " + fm.getValue(PgPackageLogic.CN_KOD_KRESKOWY));
		
		emailDescription.append("Status Przesy�ki:\n");
		emailDescription.append(fm.getEnumItem(PgPackageLogic.CN_STATUS_DOKUMENTU).getTitle() + "\n");
		emailDescription.append("Nadawca Przesy�ki:\n");
		emailDescription.append(nadawca.getFirstnameLastnameName() + "\n");
		emailDescription.append("Odbiorca Przesy�ki:\n");
		emailDescription.append(odbiorca!=null ? odbiorca.getFirstnameLastnameName() : "Dzia� : " + DSDivision.findById(fm.getEnumItem(PgPackageLogic.CN_DZIAL).getId()).getName()+ "\n");
		emailDescription.append("Opis Przesy�ki:\n");
		emailDescription.append(fm.getValue(PgPackageLogic.CN_DOC_DESCRIPTION));



		String[] emailNad = emailNadawcy.split(",");
		String[] emailOdb = emailOdbiorca.split(",");

		
		List<String> list = new ArrayList<String>(Arrays.asList(emailNad));
		if (!list.isEmpty())
		{
			list.remove(0);
			toEmail = list.toArray(new String[0]);
		}
		List<String> listCC = new ArrayList<String>(Arrays.asList(emailOdb));
		if (!listCC.isEmpty())
		{
			listCC.remove(0);
			emailCC = listCC.toArray(new String[0]);
		}
		
		log.error("toemail  ="+toEmail);
		log.error("emailCC ="+emailCC);
		if(!(emailCC.length>1))
			emailCC=null;
		if(toEmail.length>0)
		((Mailer) ServiceManager.getService(Mailer.NAME)).send(toEmail, emailCC, null, emailTitle.toString(), emailDescription.toString(), null);

	}

	/**
	 * Wykonuje akcje
	 */
	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		notify(execution);

		if (execution.getActivity().getDefaultOutgoingTransition() != null)
			execution.takeDefaultTransition();


	}

	@Override
	public void signal(ActivityExecution execution, String signalName, Map<String, ?> arg2) throws Exception
	{
		if (signalName != null)
			execution.take(signalName);
		else
			execution.takeDefaultTransition();
	}

	public static String getGuidDzialUzytkownika(String username) throws UserNotFoundException, EdmException
	{
		String dzial = "";
		DSUser user = DSUser.findByUsername(username);
		DSDivision[] division = user.getOriginalDivisions();
		for (DSDivision d : division)
		{
			if (d.isDivision())
			{
				dzial = d.getGuid();
				return dzial;
			} else
			{
				if (d.isPosition())
				{
					dzial = d.getGuid();
				}
			}
		}
		return dzial;
	}




}
