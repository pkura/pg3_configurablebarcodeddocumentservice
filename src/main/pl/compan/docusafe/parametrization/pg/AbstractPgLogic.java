package pl.compan.docusafe.parametrization.pg;

import org.apache.commons.net.util.Base64;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.Field.Type;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.office.AccessToDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.utils.ChecksumAlgorithm;
import pl.compan.docusafe.parametrization.archiwum.PublicDocument;
import pl.compan.docusafe.parametrization.archiwum.services.AktaPubliczneHelper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <h1>AbstractPgLogic</h1>
 *
 * <p>
 *     Klasa abstrakcyjna dostarczaj�ca interfejs dla wszystkich class implementuj�cych logik� dowolnego dokumentu w PG.
 * </p>
 * @see PgDocInLogic, PgDocOutLogic, PgModifyCaseLogic, ZadanieLogic
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public abstract class AbstractPgLogic extends AbstractDocumentLogic {

    protected static final String multiOdbiorca = "M_DICT_VALUES";
    protected static final String recipientID = "ID";
    protected static final String czystopisDwr = "DWR_DOC_OUT_OFFICEPREPARESTATE";
    protected static final String czystopis = "DOC_OUT_OFFICEPREPARESTATE";
    protected static final String _dataDokumentu = "DOC_DATE";
    protected static final String _dataDokumentuDwr = "DWR_DOC_DATE";

    protected static final String BARCODE_CN = "DOC_BARCODE";
    protected static final String AUTOR_DOKUMENTU_CN ="DOC_AUTOR";
    protected static final String DZIAL_AUTORA_DOKUMENTU_CN ="DOC_AUTORDIVISION";
    protected static final String TYP_DOKUMENTU_CN = "TYP_DOKUMENTU";
    protected static final String ABSTRAKT_DOKUMENTU_CN = "ABSTRAKT";
    protected static final String MIESCEUTWOORZENIA_DOKUMENTU_CN = "MIESCEUTWOORZENIA";
    protected static final String DOC_DESCRIPTION_CN = "DOC_DESCRIPTION";
    protected static final String ALFRESCO_IMPORT_CN = "ALFRESCO_IMPORT";
    protected static final String ALFRESCO_UUID_CN = "ALFRESCO_UUID";
    protected static final String SHOW_EMAIL_CN = "SHOW_EMAIL";
    protected static final String SUBJECT_EMAIL_CN = "SUBJECT_EMAIL";
    protected static final String EMAIL_SENDER_CN = "EMAIL_SENDER";
    protected static final String SENDER_CN = "SENDER";
    protected static final String DOC_DELIVERY_CN = "DOC_DELIVERY";
    protected static final String SENDER_HERE_CN = "SENDER_HERE";
    protected static final String RECIPIENT_HERE_CN = "RECIPIENT_HERE";
    protected static final String DATA_EPUAP_CN = "DATA_EPUAP";
    protected static final String DOCUMENT_IN_OFFICE_KIND_CN = "DOCUMENT_IN_OFFICE_KIND";
    protected static final String KEYWORDS = "SLOWAKLUCZOWE";
    protected static final String AUTOR_DZIELA ="AUTORDZIELA";
    protected static final String MIESCEUTWORZENIA = "MIESCEUTWORZENIA";
    protected static final String DATADZIELA = "DATA_UTWORZENIA_DZIELA";
    protected static final Long PUBLIC_DOCUMENT_ID = Long.valueOf(1);

	protected static Logger log = LoggerFactory.getLogger(AbstractPgLogic.class);
    /**
     * Walidacja barkodu
     * @param values
     * @param msg
     * @return
     */
    protected Field validateBarcode(Map<String, FieldData> values, Field msg) {
        FieldData barcodeField = containsField(values, BARCODE_CN);
        if(barcodeField != null && barcodeField.getData() != null && !ChecksumAlgorithm.PG.isValid(barcodeField.getStringData())){
            msg = new Field("messageField", "Niepoprawny barkod!", Field.Type.BOOLEAN);
            values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
        }
        return msg;
    }
    /**
     * 
     * Metoda zwraca list� parametr�w dla dokumentu z barkodem(o ile istniej)
     * 
     * @param DocumentKind - typ dokumentu
     * @param id - id dokumentu
     * @param TaskSnapshot - instancja procesu powi�zana z dokumentem
     * @return TaskListParams - parametry dokumentu
     * @exception EdmException
     */
    public TaskListParams getTaskListParams(DocumentKind dockind, long id, TaskSnapshot task) throws EdmException {
        TaskListParams params = super.getTaskListParams(dockind, id, task);
        try{
        	 FieldsManager fm = dockind.getFieldsManager(id);
             if (fm.getValue(BARCODE_CN) != null) {
                 params.setAttribute(fm.getStringValue(BARCODE_CN), 0);
             }
           
        } catch (Exception e){
        	log.error(e.getMessage(), e);
        }
        return params;
    }

    /**
     * <p>
     *     Je�li {@code PgIntegration.isAvailable()} to pobiera nowy barkod z {@code PgIntegration}
     *     i zapisuje w polu o cn {@code DOC_BARCODE}.
     * </p>
     * @param fm
     * @param values
     * @throws Exception
     * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
     */
    protected void generateBarcode(FieldsManager fm, Map<String, Object> values) throws Exception {
        String barcode = (String)fm.getFieldValues().get(PgDocInLogic.BARCODE_CN);
        if(StringUtils.isEmpty(barcode)) {
            PgIntegration pgIntegration = new PgIntegration(new RestTemplate());

            if(pgIntegration.isAvailable()) {
                String externalName = DSApi.context().getDSUser().getExternalName();
                if(externalName!=null){
                	 while(externalName.length()<5 )
                  	   externalName = "0"+externalName;
                }
                String temp = String.valueOf(Systemy.convertTo(Integer.valueOf(externalName), 60));
                while(temp.length()<5 )
                	temp = "0"+temp;
              //  barcode = pgIntegration.getRestNewExtendedBarcode( (externalName!=null ? externalName : "" ), "01");
                //	barcode = pgIntegration.getRestNewExtendedBarcode( (temp!=null ? temp : "" ), "01"); 
                barcode = pgIntegration.getRestNewExtendedBarcode64((temp!=null ? temp : "" ), "01" , false);
                values.put(PgDocInLogic.BARCODE_CN, barcode);
            }
        }
    }
    /**
     * <p>
     *     Je�li {@code PgIntegration.isAvailable()} to pobiera nowy barkod z {@code PgIntegration}
     *     i zapisuje w polu o cn {@code DOC_BARCODE}.
     * </p>
     * @param fm
     * @param values
     * @throws Exception
     * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
     */
   
	protected void generateBarcodeValidateDWR(FieldsManager fm, Map<String, FieldData> values) throws Exception
	{

		String barcode = values.get(DwrUtils.dwr(PgDocInLogic.BARCODE_CN)).getStringData();
		if (StringUtils.isEmpty(barcode))
		{
			PgIntegration pgIntegration = new PgIntegration(new RestTemplate());

			String authorExtName = "";
			String externalName = "";
			if (values.get("DWR_DOCAUTOREXTERNALNAME") != null)
				authorExtName = values.get("DWR_DOCAUTOREXTERNALNAME").getStringData();

			if (pgIntegration.isAvailable())
			{
				if (DSApi.isContextOpen())
				{
					externalName = DSApi.context().getDSUser().getExternalName();
				} else
				{
					externalName = authorExtName;
				}
				if (externalName != null)
				{
					while (externalName.length() < 5)
						externalName = "0" + externalName;
				}
				String temp = String.valueOf(Systemy.convertTo(Integer.valueOf(externalName), 60));
				while (temp.length() < 5)
					temp = "0" + temp;
				//  barcode = pgIntegration.getRestNewExtendedBarcode( (externalName!=null ? externalName : "" ), "01");
				barcode = pgIntegration.getRestNewExtendedBarcode64((temp != null ? temp : ""), "01",false);
				values.put(DwrUtils.dwr(PgDocInLogic.BARCODE_CN), new FieldData(Type.STRING, barcode));

			}
		}
	}
	/**
	 *  
     * Aktualizuje wersje barcodu
     * 
     * @param fm - dostarcza warto�ci p�l dokumentu
     * @throws Exception
	 */
    protected void updateBarcodeVersion(FieldsManager fm) throws Exception {
        String barcode = (String)fm.getFieldValues().get(PgDocInLogic.BARCODE_CN);
        if(!StringUtils.isEmpty(barcode)) {
            PgIntegration pgIntegration = new PgIntegration(new RestTemplate());
            if(pgIntegration.isAvailable()) {
                String[] parts = barcode.split(":");
                String version = parts[1];
                version = version.substring(version.length()-2);
                int ver = Integer.parseInt(version);
                if(ver < 99) {
                    ver++;
                    if (ver < 10) {
                        version = "0" + String.valueOf(ver);
                    } else if (ver >= 10) {
                        version = String.valueOf(ver);
                    }
                }
                String barcodeBase64 = Base64.encodeBase64String(barcode.getBytes());
 
                barcode = pgIntegration.getUpdatedExtendedBarcode(barcodeBase64, null, version); // Je�eli null to nie zmieni si� externalname
                Map<String,Object> values = new HashMap<String, Object>();
                values.put(PgDocInLogic.BARCODE_CN, barcode);
                fm.getDocumentKind().setOnly(fm.getDocumentId(), values);
            }
        }
    }
    /**
     * 
     * Metoda wykonywana w momencie klikni�cia przycisku "Zapisz"
     * 
     * @param kind - rodzaj dokumentu
     * @param documentId - id dokumentu
     * @param fieldValues - warto�ci p�l dokumentu
     * @throws SQLException, EdmException
     * 
     */
	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
	{
		 OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId, false);
		 if( doc.getOfficeStatus()!=null && doc.getOfficeStatus().equals("new")){
		    	List<OfficeDocument> ls = OfficeDocument.findAllByBarcode((String)fieldValues.get(PgDocInLogic.BARCODE_CN));
		    	if( !ls.isEmpty()  && ls.size()>1){
		    		throw new EdmException("Istnieje juz dokument z identycznym barkodem : "+(String)fieldValues.get(PgDocInLogic.BARCODE_CN));
		    	}
		    	if(!ls.isEmpty() && !ls.get(0).getId().equals(doc.getId())){
		    		throw new EdmException("Istnieje juz dokument z identycznym barkodem : "+(String)fieldValues.get(PgDocInLogic.BARCODE_CN));
		    	} 
		    }
	     PublicDocument publicDocument = PublicDocument.findById(documentId);
	
	     if (doc.getAccessToDocument() != null && doc.getAccessToDocument().compareTo(PUBLIC_DOCUMENT_ID)==0) {
	         if (publicDocument == null) {
	             publicDocument = this.getPublicDocument(doc);
	             setFromDockind(doc,publicDocument , fieldValues);
	            
	             publicDocument.create();
	         }else{
	             publicDocument.setTitle(doc.getTitle());
	             publicDocument.setAbstrakt(doc.getAbstractDescription());
	             setFromDockind(doc, publicDocument , fieldValues);
	             DSApi.context().session().update(publicDocument);
	
	         }
	     }else if (publicDocument != null)
	         DSApi.context().session().delete(publicDocument);
	    
	}
    private void setFromDockind(OfficeDocument doc, PublicDocument publicDocument,
			Map<String, ?> fieldValues) throws DivisionNotFoundException, EdmException {
    	 if(fieldValues.containsKey(MIESCEUTWORZENIA) && fieldValues.get(MIESCEUTWORZENIA)!=null ){
       	  publicDocument.setLocation((String)fieldValues.get(MIESCEUTWORZENIA)); 
        }
    	
      //  publicDocument.setLocation(DSDivision.find(doc.getDivisionGuid()).getName());
        publicDocument.setOrganizationalUnit(DSDivision.find(doc.getDivisionGuid()).getName());
        if(fieldValues.containsKey(KEYWORDS) && fieldValues.get(KEYWORDS)!=null )
        {
       	 publicDocument.setKeyWords((String)fieldValues.get(KEYWORDS));
        }
        if(fieldValues.containsKey(AUTOR_DZIELA) && fieldValues.get(AUTOR_DZIELA)!=null )
        {
       	 publicDocument.setAuthor((String)fieldValues.get(AUTOR_DZIELA));
        }
        publicDocument.setAttachmentCount(doc.getAttachments().size());
        if(fieldValues.containsKey(DATADZIELA) && fieldValues.get(DATADZIELA)!=null )
        {
       	 publicDocument.setCtime((Date)fieldValues.get(DATADZIELA));
        }
        publicDocument.setAttachmentCount(doc.getAttachments().size());
        publicDocument.setBase64Miniatura(AktaPubliczneHelper.getBase64Miniatura(doc));
		
	}
	protected void updateBarcodeVersion(Document document) throws Exception {
        FieldsManager fm = document.getFieldsManager();
        updateBarcodeVersion(fm);
    }

    /**
     * 
     * Aktualizuje barcode
     * 
     * @param fm - dostarcza warto�ci p�l dokumentu
     * @param values - mapa warto�ci p�l dokumentu
     * @throws Exception
     */
    protected void updateBarcodeExternalname(FieldsManager fm, Map<String, Object> values) throws Exception {
        String barcode = (String)fm.getFieldValues().get(PgDocInLogic.BARCODE_CN);
        if(!StringUtils.isEmpty(barcode)) {
            PgIntegration pgIntegration = new PgIntegration(new RestTemplate());
            if(pgIntegration.isAvailable()) {
                String[] parts = barcode.split(":");
                String version = parts[1];
                version = version.substring(version.length()-2); // Bez podbijania wersji po zmianie exernalname
                String barcodeBase64 = Base64.encodeBase64String(barcode.getBytes());
                String externalname = DSApi.context().getDSUser().getExternalName();
                if(externalname.equals(null)) {
                    externalname = "null";
                }
                if (externalname != null)
				{
					while (externalname.length() < 5)
						externalname = "0" + externalname;
				}
				String temp = String.valueOf(Systemy.convertTo(Integer.valueOf(externalname), 60));
				while (temp.length() < 5)
					temp = "0" + temp;
               // barcode = pgIntegration.getUpdatedExtendedBarcode(barcodeBase64, temp, version);
                barcode = pgIntegration.getUpdatedExtendedBarcode(barcodeBase64, null, version);
                Map<String,Object> val = new HashMap<String, Object>();
                values.put(PgDocInLogic.BARCODE_CN, barcode);
                val.put(PgDocInLogic.BARCODE_CN, barcode);
                fm.getDocumentKind().setOnly( fm.getDocumentId(), val);
            }
        }
    }


    /*   @Override
    public void correctValues(Map<String, Object> values, DocumentKind kind, long documentId) throws EdmException {
       OfficeDocument doc = OfficeDocument.find(documentId);
        PublicDocument publicDocument = PublicDocument.findById(documentId);

        if (doc.getAccessToDocument() != null && doc.getAccessToDocument().compareTo(PUBLIC_DOCUMENT_ID)==0) {
            if (publicDocument == null) {
                publicDocument = this.getPublicDocument(doc);
                publicDocument.create();
            }else{
                publicDocument.setTitle(doc.getTitle());
                publicDocument.setAbstrakt(doc.getAbstractDescription());
                publicDocument.setLocation(DSDivision.find(doc.getDivisionGuid()).getName());
                publicDocument.setOrganizationalUnit(DSDivision.find(doc.getDivisionGuid()).getName());
                DSApi.context().session().update(publicDocument);

            }
        }else if (publicDocument != null)
            DSApi.context().session().delete(publicDocument);

    }
    }*/
    
    /**
     * Metoda zwraca dokument archiwalny(publiczny)
     * 
     * @param OfficeDocument - pismo kancelaryjne
     * @return PublicDocument - reprezentacja dokumentu jako dokumentu publicznego z archiwum
     * @exception EdmException gdy nie istnieje pobierany zas�b {user,division,etc...}
     */
    @Override
    public PublicDocument getPublicDocument(OfficeDocument doc) throws EdmException {
        PublicDocument publicDocument = super.getPublicDocument(doc);
        publicDocument.setAbstrakt(doc.getAbstractDescription());
        publicDocument.setOrganizationalUnit(DSDivision.find(doc.getDivisionGuid()).getName());
        publicDocument.setLocation(DSDivision.find(doc.getDivisionGuid()).getName());
        publicDocument.setAttachmentCount(doc.getAttachments().size());
        publicDocument.setBase64Miniatura(AktaPubliczneHelper.getBase64Miniatura(doc));
        
        return publicDocument;
    }
}
