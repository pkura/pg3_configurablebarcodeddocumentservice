package pl.compan.docusafe.parametrization.pg;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: slim
 * Date: 26.08.13
 * Time: 16:12
 * To change this template use File | Settings | File Templates.
 */
public class BarcodeGenerator {

    private static Logger log = LoggerFactory.getLogger(BarcodeGenerator.class);

    private static BarcodeGenerator instance=null;
    private int start, limit, pointer;
    private String prefix;

    public static String getPrefix()
    {
        boolean isOpened = true;
        try{
            if (!DSApi.isContextOpen()) {
                isOpened = false;
                DSApi.openAdmin();
            }
            String prefix=null;
            Statement st = DSApi.context().createStatement();
            ResultSet rs = st.executeQuery("select * from dsg_pg_barcode_generator");
            while(rs.next())
            {
                prefix = rs.getString("prefix");
            }
            rs.close();
            st.close();
            return prefix;
        }
        catch(Exception e)
        {
            log.error(e.getMessage(), e);
        }
        finally
        {
            if (DSApi.isContextOpen() && !isOpened) {
                try {
                    DSApi.close();
                } catch (EdmException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
        return null;
    }

    private BarcodeGenerator()
    {
        boolean isOpened = true;
        try{
            if (!DSApi.isContextOpen()) {
                isOpened = false;
                DSApi.openAdmin();
            }

            Statement st = DSApi.context().createStatement();
            ResultSet rs = st.executeQuery("select * from dsg_pg_barcode_generator");
            while(rs.next())
            {
                pointer = rs.getInt("pointer_value");
                prefix = rs.getString("prefix");
            }
            start = Integer.parseInt(Docusafe.getAdditionProperty("barcode.start_value"));
            limit = Integer.parseInt(Docusafe.getAdditionProperty("barcode.limit_value"));
//            prefix = Docusafe.getAdditionProperty("barcode.prefix_value");
            rs.close();
            st.close();
        }
        catch(Exception e)
        {
            log.error(e.getMessage(), e);
        }
        finally
        {
            if (DSApi.isContextOpen() && !isOpened) {
                try {
                    DSApi.close();
                } catch (EdmException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    public static BarcodeGenerator getInstance()
    {
        if(instance==null) return new BarcodeGenerator();
        else return instance;
    }

    public BarcodePool generateBarcodePool(int amount, Date date, Long userId, Long documentId) throws EdmException
    {

        if(amount + pointer > limit) return null;
        else
        {
            BarcodePool pool = new  BarcodePool(pointer, pointer+amount-1, date, userId, prefix, documentId);
            return pool;
        }
    }

    public static String getBarcode(int index, String prefix)
    {
        try{
            String test = Systemy.convertTo(60, index);
            String temp = String.valueOf(Systemy.convertTo(index, 60));
            String summaryCode = BarcodeHelper.getSummaryCode(prefix, temp);
            String xxx = BarcodeHelper.addZeros(BarcodeHelper.getCode(summaryCode));

            return BarcodeHelper.getSummaryCode(prefix, temp);
        }
        catch(Exception e)
        {
            log.error(e.getMessage(), e);
        }
        return null;
    }
}
