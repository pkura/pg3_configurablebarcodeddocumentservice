package pl.compan.docusafe.parametrization.pg;

//import java.sql.PreparedStatement;


import java.util.Calendar;
import java.util.List;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Stwierdza, czy odbiorca pisma jest prawid�owy
 */
@SuppressWarnings("serial")
public class PackageDecisionHandler implements DecisionHandler {
	private static final Logger log = LoggerFactory.getLogger(PackageDecisionHandler.class);
	//private String wskazanaInnaOsoba = "";
	//private String autor = "";
	public String decide(OpenExecution openExecution) {
		
		log.info("PackageDecisionHandler");
		
		String decision = "autor"; //domyslne na wypadek wyjatku
		
		try {
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			
			
			String data = Calendar.getInstance().getTime().toString();
			String rodzajDokumentu = doc.getDocumentKind().getCn();
			log.info("Dokument  id = "+docId+"Rodzaj = " +rodzajDokumentu+ " czas zajscia procesu "+data);
			//decision = "autor";
			
			DSUser usertworz = DSApi.context().getDSUser();
		
			long useridstl=fm.getLongKey("RECIPIENT_HERE");
			Person person =Person.find(useridstl);
			String personfirstname = person.getFirstname();
			String personLastname = person.getLastname();
			DSUser odbiorca = DSUser.findByFirstnameLastname(personfirstname, personLastname);
			// jak odbiorca to prezes lub wiceprezes
			if(odbiorca.getName().equals("wlodarski") || odbiorca.getName().equals("jurkowski")){
				 decision = "odbiorcaToDyrDept"; //odbiorcy to prezesi vice prezesi dorobic sciezke dla nich
				 openExecution.setVariable("odbiorcaToDyrDept", true);
			}else if( czyUserToDyr(odbiorca)){
				 decision = "odbiorcaToDyrDept";
			openExecution.setVariable("odbiorcaToDyrDept", true);
			}
				else {
				decision = "odbiorcaToNIEDyrDept";
				openExecution.setVariable("odbiorcaToDyrDept", false);
			}
			
			
			
			
//			log.info("Sprawdzam czy user tworz to autor "+fm.getStringKey("SENDER_HERE") +"[person]---> "+nadawca.getId()+"[user] to  Id autora "+usertworz.getId());
//			if(!nadawca.getId().equals(usertworz.getId())){
//				
//				decision = "odbiorcaToDyrDept";
//			} else {
//				
//				decision = "odbiorcaToNIEDyrDept";
//			}
			
			
			
			
			// napisac. Uwaga, bo ktos wpisal SENDER_HERE na sztywno, a tu ma byc odbiorca a nie nadawca
			
			//String decision = "...";
			//return decision;
				
			//mozliwe decyzje:
			
			//odbiorcaToDyrDept			- odbiorca to dyrektor departamentu
			//odbiorcaToNIEDyrDept		- odbiorca to nie dyrektor departamentu (jaki� inny u�ytkownik)

		} catch (Exception e) {
			//logowanie
			log.error("", e);
		}
		

		log.info("CzyOdbiorcaToDyrDepartamentu_DecisionHandler zwracam decyzje decision = "+decision);
		
		return decision;
	}
	public static boolean czyUserToDyr(DSUser odbiorca) throws Exception
	{
		//PreparedStatement ps = null;
		//String query = "Select (ID) from DS_USER  where   DS_DIVISION.divisiontype = 'position' and  DS_DIVISION.description ='DYR'
		
		boolean  b = false;
		List<DSDivision> divisionsDyrs = DSDivision.findByDescription("DYR");
		DSDivision[] divisionsOdb = odbiorca.getOriginalDivisions();
		for(DSDivision  divOdb :divisionsOdb) {
				for(DSDivision  divDyr :divisionsDyrs) {
				if (divOdb.getId()==(divDyr.getId())){
					b = true;
					break;
				}
			}
		}
			 
		return b;	
		}
		
		//long dyrs[] = {21,91,35,48,56,77};
		// 21- PIotr korzecki dyr DP  ; 91 -Robert Czarnecki dyr BDG  ; 35 -  Edward Raban dyr DOR
		//  48 - Bo�ena �opacka dyr DEB ; 56  Marcin Zagrajek dyr DBJ ; 63 Krzysztof D�browski vc dyr CEZAR
		// 15 Agata Gajda naczelnik Wydzia�u prezydentialnego , 16 Beata Lewandowska naczelnik Wydzia�u prezydentialnego
		//for( long dyrektor : dyrs){
		//	try{
		//		if (odbiorca.getName().contains(DSUser.findById(dyrektor).getName())){
		//			return true;
		//		}
		//	}catch(Exception e)
		//	{}
		//}
	
	
}



