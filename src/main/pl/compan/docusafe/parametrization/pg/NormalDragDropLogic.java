package pl.compan.docusafe.parametrization.pg;

import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.logic.NormalLogic;

public class NormalDragDropLogic extends NormalLogic {
	
	public static final String FOLDER_CN = "FOLDER";
	public static final String DESCRIPTION_CN = "DOC_DESCRIPTION";
	public static final String DATE_CN = "DOC_DATE";
	
	
	private static final long serialVersionUID = 1L;

	@Override
	public void correctImportValues(Document document, Map<String, Object> values, StringBuilder builder) throws EdmException {
		super.correctImportValues(document, values, builder);
		/*if(values.containsKey(FOLDER_CN) && values.get(FOLDER_CN) != null){
			Folder f = Folder.find(Long.parseLong(values.get(FOLDER_CN).toString()));
			document.setFolder(f);
		}*/
	}
 
}
