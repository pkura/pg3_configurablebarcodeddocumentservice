package pl.compan.docusafe.parametrization.pg;

import pl.compan.docusafe.core.users.DSUser;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: slim
 * Date: 26.08.13
 * Time: 17:06
 * To change this template use File | Settings | File Templates.
 */
public class BarcodePool {

    private Long id, documentId;
    private int start, end;
    private Date creationDate;
    private Long userId;
    private String prefix;


    public BarcodePool()
    {
    	
    }
    
    public BarcodePool(Long id, int start, int end, Date date, Long user, Long documentId) {
        this.id = id;
        this.start = start;
        this.end = end;
        this.creationDate = date;
        this.userId = user;
        this.documentId = documentId;
    }

    public BarcodePool(int start, int end, Date date, Long user, String prefix, Long documentId) {
        this.start = start;
        this.end = end;
        this.creationDate = date;
        this.userId = user;
        this.prefix=prefix;
        this.documentId = documentId;
    }

    public List<String> getBarcodes()
    {
        String pref = BarcodeGenerator.getPrefix();
        LinkedList<String> barcodes = new LinkedList<String>();
        for(int i=start; i<=end; i++)
        {
           String iString = String.valueOf(i);
           barcodes.add(getBarcode(i, pref));
        }
        return barcodes;
    }

    public String getBarcode(int index, String prefix)
    {
        return BarcodeGenerator.getBarcode(index, prefix);
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public Date getDate() {
        return creationDate;
    }

    public void setDate(Date date) {
        this.creationDate = date;
    }

    public Long getUser() {
        return userId;
    }

    public void setUser(Long user) {
        this.userId = user;
    }

    public  String getPrefix() {
        return prefix;
    }

    public  void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }
}
