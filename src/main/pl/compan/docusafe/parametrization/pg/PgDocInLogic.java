package pl.compan.docusafe.parametrization.pg;

import com.opensymphony.webwork.ServletActionContext;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Property;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.jfree.util.Log;
import org.springframework.web.client.RestTemplate;

import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.alfresco.AlfrescoApi;
import pl.compan.docusafe.core.alfresco.AlfrescoUtils;
import pl.compan.docusafe.core.alfresco.DockindMappedField;
import pl.compan.docusafe.core.alfresco.DockindMappedFieldStore;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.OfficePoint;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.field.DocumentInOfficeKindsField;
import pl.compan.docusafe.core.dockinds.field.DocumentPersonField;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.ImportLogic;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.mail.MailMessage;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.utils.ChecksumAlgorithm;
import pl.compan.docusafe.service.epuap.multi.EpuapSkrytka;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.DwrDocumentHelper;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.gov.epuap.ws.pull.PkPullServiceStub.OdpowiedzPullPobierzTyp;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.security.auth.Subject;

import com.asprise.util.tiff.D;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.*;

/**
 * <h1>PgDocInLogic</h1>
 *
 * <p>
 *     Klasa s�u�y do obs�ugi zachowania/implementaji funkcjonalno�ci pism przychodz�cych do systemu.
 * </p>
 */

@SuppressWarnings("serial")
public class PgDocInLogic extends AbstractPgLogic implements ImportLogic
{
	private static PgDocInLogic instance;
	protected static Logger log = LoggerFactory.getLogger(PgDocInLogic.class);
    protected static DwrDocumentHelper dwrDocumentHelper = new DwrDocumentHelper();

	public static PgDocInLogic getInstance()
	{
		if (instance == null)
			instance = new PgDocInLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk�y- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk�y - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk�y - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}
	@Override
	public void acceptTask(Document document, ActionEvent event, String activity)
	{
		FieldsManager fm = document.getFieldsManager();
		try
		{
			updateBarcodeExternalname(fm, fm.getFieldValues());
		} catch (EdmException e)
		{
			log.error("", e);
		} catch (Exception e)
		{
			log.error("", e);
		}

	}

/*	@Override
	public void updateDocumentBarcodeVersion(Document document) throws Exception
	{
		for (InOfficeDocumentDelivery kind : InOfficeDocumentDelivery.list())
		{
			if ((kind.getName().equals("ePUAP") && document.getFieldsManager().getFieldValues().get(DOC_DELIVERY_CN)!=null &&  (document.getFieldsManager().getFieldValues().get(DOC_DELIVERY_CN)).equals(kind.getId().toString()))
					|| (kind.getName().equals("eMail") && (document.getFieldsManager().getFieldValues().get(DOC_DELIVERY_CN)).equals(kind.getId().toString())))
			{
				try
				{
					updateBarcodeVersion(document);
				} catch (Exception e)
				{
					throw new EdmException("B��d przy generowaniu nowego identyfikatora dokumentu", e);
				}
				break;
			}
		}

	}*/
	@Override
	public void updateDocumentBarcodeVersion(Document document) throws Exception
	{
		for (InOfficeDocumentDelivery kind : InOfficeDocumentDelivery.list())
		{
			String delivery = document.getFieldsManager().getStringKey(DOC_DELIVERY_CN);
			boolean areEquals = kind.getId().toString().equals(delivery);
			if ((kind.getName().equals("ePUAP") && areEquals) || (kind.getName().equals("eMail") && areEquals))
			{
				try
				{
					updateBarcodeVersion(document);
				} catch (Exception e)
				{
					throw new EdmException("B��d przy generowaniu nowego identyfikatora dokumentu", e);
				}
				break;
			}
		}

	}



	//nowa kolumna na liscie zadan 
	public TaskListParams getTaskListParams(DocumentKind dockind, long id)	throws EdmException {
		TaskListParams params = new TaskListParams();

		try {
			
			} catch (Exception e) {
			log.error("", e);
		}
		return params;
	}
	
	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		Field msg = null;

		if (values.get(_dataDokumentuDwr) != null && values.get(_dataDokumentuDwr).getData() != null)
		{
			Date startDate = new Date();
			Date finishDate = values.get(_dataDokumentuDwr).getDateData();

			if (finishDate.after(startDate))
			{
				msg = new Field("messageField", "Data pisma nie mo�e by� dat� z przysz�o�ci.", Field.Type.BOOLEAN);
				values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
			}
		}
	
			boolean adminOpened = DSApi.openContextIfNeeded();

			for (InOfficeDocumentDelivery kind : InOfficeDocumentDelivery.list())
			{
				if (kind.getName().equals("ePUAP") && values.get(DwrUtils.dwr(DOC_DELIVERY_CN)) != null
						&& (values.get(DwrUtils.dwr(DOC_DELIVERY_CN)).getEnumValuesData().getSelectedId()).equals(kind.getId().toString()))
				{
					try
					{
						DSApi.closeContextIfNeeded(adminOpened);
						generateBarcodeValidateDWR(fm, values);
					} catch (Exception e)
					{
						throw new EdmException("B��d przy generowaniu nowego identyfikatora dokumentu", e);
					}
					break;
				}
			}
			DSApi.closeContextIfNeeded(adminOpened);
		

		if (AvailabilityManager.isAvailable("PgValidateBarcode"))
			msg = validateBarcode(values, msg);

		return msg;
	}

	@Override
	public  void onLoadData(FieldsManager fm) throws EdmException {

		if(!DSApi.context().hasPermission(DSPermission.PISMO_WYCH_DOSTEP)) {
			fm.getField("DOSTEP").setHidden(true);
		}else {
			fm.getField("DOSTEP").setHidden(false);
		}

	}

    @Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		log.info("type: {}", type);
		fm.getField("JOURNAL").setHidden(true);
		fm.getField("DOC_REFERENT").setHidden(true);

		Map<String, Object> values= new HashMap<String, Object>();
		values.put("TYPE", type);
		values.put(_dataDokumentu, new Date());
		values.put("DOCAUTOREXTERNALNAME", DSApi.context().getDSUser().getExternalName());
		
		setFromEmailValues(fm,values);
        setAlfrescoProperties(fm, values);
        	
        fm.reloadValues(values);
	}


    /**
     * Ustawia parametry z alfresco z przekazanego uuid przez request
     */
	
    private void setAlfrescoProperties(FieldsManager fm, Map<String, Object> values) {
        // uuid dokumentu z alfresco do zaimportowania
        String alfrescoUuidCn = "property_" + DSApi.context().getPrincipalName();
        String uuid = System.getProperty(alfrescoUuidCn);
        if(StringUtils.isNotEmpty(uuid))
        {
            AlfrescoApi alfrescoApi = new AlfrescoApi();
            log.debug("Pobralem uuid z requesta {}", uuid);
            System.clearProperty(alfrescoUuidCn);
            try
            {
                CmisObject cmisObj = alfrescoApi.getObject(uuid);
                if(!(cmisObj instanceof org.apache.chemistry.opencmis.client.api.Document)){
                    throw new Exception("Dokument o podanym uuid nie jest dokumentem Alfresco lub nie istnieje!");
                }

                DockindMappedFieldStore.setDockindMappedFields(fm.getDocumentKind().getCn(), cmisObj, values);
                values.put(ALFRESCO_IMPORT_CN, true);
                values.put(ALFRESCO_UUID_CN, uuid);
            }catch(Exception e){
                log.error("", e);
            }finally{
                alfrescoApi.disconnect();
            }
        }
    }
    
	/*public void onLoadData(FieldsManager fm) throws EdmException {
	      
		if(!DSApi.context().hasPermission(DSPermission.SLOWNIK_OSOB_DODAWANIE)){
			
			  DocumentPersonField f =   (DocumentPersonField) fm.getField(SENDER_CN);
			    String[] buttons =  new String[]{"doClear","showPopup", "doRemove"}; //{"[showPopup, doAdd, doRemove, doClear]};
			    ((DocumentPersonField)fm.getField("SENDER")).setPopUpButtons(Lists.newArrayList(buttons));
		} else{
			  super.onLoadData(fm);
		}
		
	}*/
    
    /**
     * 
     * Aktualizuje historie dokumentu
     * 
     * @param description
     * @param doc
     * @throws UserNotFoundException
     * @throws EdmException
     */
    private void addWpisHistDoDokuemntu(String description, OfficeDocument doc) throws UserNotFoundException, EdmException
	{
		Audit wpis = new Audit();
		wpis.setDescription(description);
		wpis.setCtime(new Date());
		wpis.setUsername(DSApi.context().getDSUser().getName());
		wpis.setProperty("giveAccept");
		wpis.setLparam("");
		wpis.setPriority(1);
		DataMartManager.addHistoryEntry(doc.getId(), wpis);
		
	}
    @Override
    public void setAdditionalValuesFromEpuapDocument(OdpowiedzPullPobierzTyp odpowiedz, InOfficeDocument doc,Map<String, Object> values) throws EdmException, DocumentException, IOException
    {
    FieldsManager fm  = doc.getFieldsManager();
   
    List<org.dom4j.Element> historiaKomunikacji  = null;
    SAXReader reader = new SAXReader();
   String dataNadania =  odpowiedz.getDataNadania().getTime().toLocaleString();
   values.put(DATA_EPUAP_CN, dataNadania);
   
   addWpisHistDoDokuemntu(prepaareDescriptionDoHistori(odpowiedz ,doc), doc);
   
   Calendar cal = odpowiedz.getDataNadania();
	Date data  = cal.getTime();
    boolean doreczenie = false;
	org.dom4j.Document xmlDocument = reader.read(odpowiedz.getDokument().getZawartosc().getInputStream());
	org.dom4j.Document xmlDocumentDaneDotatkowe = reader.read( odpowiedz.getDaneDodatkowe().getInputStream());
	historiaKomunikacji = xmlDocumentDaneDotatkowe.getRootElement().elements("HistoriaKomunikacji");
	try {
	for (Element wpis : historiaKomunikacji){
		org.dom4j.Element elementSkrytka = wpis.element("Skrytka");
		org.dom4j.Element elementDoreczenie= wpis.element("Doreczenie");
		org.dom4j.Element elementSkrytka2 = wpis.element("Skrytka");
		 if (elementSkrytka!=null){ 
			String Nadawca = elementSkrytka.element("Nadawca").getData().toString();
			String Odbiorca =  elementSkrytka.element("Odbiorca").getData().toString();
			String AdresNadania =  elementSkrytka.element("AdresNadania").getData().toString();
			String AdresOdpowiedzi =  elementSkrytka.element("AdresOdpowiedzi").getData().toString();
			String RodzajDokumentu =  elementSkrytka.element("RodzajDokumentu").getData().toString();
			String IdentyfikatorDokumentu =  elementSkrytka.element("IdentyfikatorDokumentu").getData().toString();
			String IdentyfikatorPoswiadczenia = elementSkrytka.element("IdentyfikatorPoswiadczenia").getData().toString();
			String DataPrzekazania =  elementSkrytka.element("DataPrzekazania").getData().toString();
			String IdentyfikatorZleceniaDoreczenia =  elementSkrytka.element("IdentyfikatorZleceniaDoreczenia").getData().toString();
			
			
			
			 
			// Date DataEpuap = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").parse(elementSkrytka.element("DataPrzekazania").getData().toString());
			
		 }
		 if (elementDoreczenie!=null){
			 
			 prepaareDescriptionDoreczenieDoHistori(elementDoreczenie, doc);
			 doreczenie = true;
		 }
		 if (doreczenie && elementSkrytka2!=null){
			 
		 przeprePotwierdzeniedoreczeniaUPD(doc,elementSkrytka2);
			
		 }
	}
	fm.reloadValues(values);
	}
	catch (Exception e) {
		log.error("blad przy padsowaniu dodatkowych metadanych", e);
	}
	
	
    }
    
    /**
     * Metoda parsuje date przekazania pisma z epuap
     * 
     * @param doc
     * @param elementSkrytka2
     */
    private void przeprePotwierdzeniedoreczeniaUPD(InOfficeDocument doc, Element elementSkrytka2)
	{

    	// String Nadawca = elementSkrytka2.element("Nadawca").getData().toString();
			//String Odbiorca =  elementSkrytka2.element("Odbiorca").getData().toString();
		///	String AdresNadania =  elementSkrytka2.element("AdresNadania").getData().toString();
		////	String AdresOdpowiedzi =  elementSkrytka2.element("AdresOdpowiedzi").getData().toString();
		//	String RodzajDokumentu =  elementSkrytka2.element("RodzajDokumentu").getData().toString();
		//	String IdentyfikatorDokumentu =  elementSkrytka2.element("IdentyfikatorDokumentu").getData().toString();
		///	String IdentyfikatorPoswiadczenia = elementSkrytka2.element("IdentyfikatorPoswiadczenia").getData().toString();
			String DataPrzekazania =  elementSkrytka2.element("DataPrzekazania").getData().toString();
	    	
			StringBuilder sb = new StringBuilder();
					
					sb.append("DOstarczono UPD do odbiorcy w dniu  ");
					
					Date dataEpuap = null;
					try {
						dataEpuap = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").parse(DataPrzekazania);
					}catch (Exception e) {
					log.error("", e);
					}
		
	}

    /**
     * 
     * Metoda dodaje wpis - sk�adaj�cy si� z info. z epuap - do historii dokumentu.
     * 
     * @param elementDoreczenie - objekt pisma z epuap
     * @param doc
     * @throws UserNotFoundException
     * @throws EdmException
     */
	private void prepaareDescriptionDoreczenieDoHistori( Element elementDoreczenie  ,InOfficeDocument doc) throws UserNotFoundException, EdmException
	{
    	
    	// String Nadawca = elementSkrytka.element("Nadawca").getData().toString();
		String OdbiorcaDoreczenia =  elementDoreczenie.element("Odbiorca").getData().toString();
		//String AdresNadania =  elementDoreczenie.element("AdresNadania").getData().toString();
		//String AdresOdpowiedzi =  elementDoreczenie.element("AdresOdpowiedzi").getData().toString();
		//String RodzajDokumentu =  elementDoreczenie.element("RodzajDokumentu").getData().toString();
		//String IdentyfikatorDokumentuDoreczenia =  elementDoreczenie.element("IdentyfikatorDokumentu").getData().toString();
	//	String IdentyfikatorPoswiadczeniaDoreczenia = elementDoreczenie.element("IdentyfikatorPoswiadczenia").getData().toString();
		String DataPrzekazaniaDoreczenia =  elementDoreczenie.element("DataPrzekazania").getData().toString();
		String IdentyfikatorZleceniaDoreczenia =  elementDoreczenie.element("IdentyfikatorZleceniaDoreczenia").getData().toString();
		//String IdentyfikatorPoswiadczenia =  elementDoreczenie.element("IdentyfikatorZleceniaDoreczenia").getData().toString();
		Date dataEpuap = null;
		try {
			dataEpuap = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").parse(DataPrzekazaniaDoreczenia);
		}catch (Exception e) {
		log.error("", e);
		}
		StringBuilder sb = new StringBuilder();
		
		sb.append("Do odbiorcy : ");
		sb.append(OdbiorcaDoreczenia+" ");
		sb.append("wyslano do wypelnienia UPD w dniu  ");
		if (dataEpuap!=null){
			sb.append(dataEpuap);
		}else{
			sb.append(" ");
		}
		sb.append(" o identyfikatorze : "+IdentyfikatorZleceniaDoreczenia);
			
			sb.append("."); 
		
			addWpisHistDoDokuemntu(sb.toString(), doc);
		
	}
    
    /**
     * 
     * Metoda przygotowuje wpis - sk�adaj�cy si� z info. z epuap - do historii dokumentu.
     * 
     * @param odpowiedz - odpowied� z epuap
     * @param doc - dokument przychodz�cy
     * @return
     */
    private String prepaareDescriptionDoHistori(OdpowiedzPullPobierzTyp odpowiedz, InOfficeDocument doc)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Pismo z ePUAP od: ");
		if ("F".equals(odpowiedz.getDanePodmiotu().getTypOsoby())){
			sb.append(odpowiedz.getDanePodmiotu().getImieSkrot()+" ");
			sb.append(odpowiedz.getDanePodmiotu().getNazwiskoNazwa()+" ");
			sb.append("z dnia : "+odpowiedz.getDataNadania().getTime().toLocaleString());
			if(odpowiedz.getDanePodmiotu().getZgoda()){
				sb.append(" ,zgoda na elektroniczna odpowiedz : TAK "); 
			//	sb.append(", Odpowiedz na adres : "+odpowiedz.getAdresOdpowiedzi());
			}else{
				sb.append("zgoda na elektroniczna odpowiedz : NIE "); 
			}
			sb.append("."); 
			
		}else if ("P".equals(odpowiedz.getDanePodmiotu().getTypOsoby())){
			sb.append(odpowiedz.getDanePodmiotu().getImieSkrot()+" ");
			sb.append("z dnia : "+odpowiedz.getDataNadania().getTime().toLocaleString());
			sb.append("."); 
		}
		
		
		
		
		return sb.toString();
	}
    /**
     * 
     * Metoda ustawia warto�ci mapy(values) z email
     * 
     * @param fm
     * @param values
     * @throws EdmException
     */
	private void setFromEmailValues(FieldsManager fm, Map<String, Object> values) throws EdmException
	{
		if (fm.getValue(SUBJECT_EMAIL_CN)!=null){
		String emailTitle=	(String) fm.getValue(SUBJECT_EMAIL_CN);
		values.put(DOC_DESCRIPTION_CN, "Pismo z eMail :" +emailTitle);
		if (fm.getValue(EMAIL_SENDER_CN)!=null){
			String emailNadawca = (String)fm.getValue(EMAIL_SENDER_CN);
			int x = emailNadawca.lastIndexOf("<");
			int y = emailNadawca.lastIndexOf(">");
			if(x > 0 && y > 0 )
			emailNadawca = emailNadawca.substring(x+1, y);
			
			Map<String,String> personData = new HashMap<String, String>();
			personData.put("EMAIL", emailNadawca);
			Person p = Person.findPerson(personData);
			if( p!=null)
			values.put(SENDER_CN,p.getId().toString());
		}
		for (InOfficeDocumentKind kind :InOfficeDocumentKind.list()){
			if( kind.getName().equals("List")){
			values.put(DOCUMENT_IN_OFFICE_KIND_CN,kind.getId());
			break;
			}
		}
		
		values.put(DOC_DELIVERY_CN,(InOfficeDocumentDelivery.findByName("eMail").getId()!=null?InOfficeDocumentDelivery.findByName("eMail").getId() : 1) );
		values.put(SHOW_EMAIL_CN , true);
		 try {
	            generateBarcode(fm, values);
	        } catch (Exception e) {
	            throw new EdmException("B��d przy generowaniu nowego identyfikatora dokumentu", e);
	        }
			
		}
		
	}

	
	/*private void setInitialValuesFromEmail(FieldsManager fm ) throws EdmException
	{

		Map<String, Object> values= new HashMap<String, Object>();
		values.put(DOC_DESCRIPTION_CN, " Tytu� z eMAIL :"+mailMessage.getSubject());
		values.put(SHOW_EMAIL_CN , true);
		
		
	}*/


    @Override
    public void setAfterCreateBeforeSaveDokindValues(DocumentKind kind, Long id, Map<String, Object> dockindKeys) throws EdmException {
        log.error("{}", dockindKeys.get(ALFRESCO_IMPORT_CN));
        boolean importAlfresco = false;
        if(dockindKeys.get(ALFRESCO_IMPORT_CN)!=null){
         importAlfresco = (Boolean) dockindKeys.get(ALFRESCO_IMPORT_CN);
        }
        //jesli parametr jest zaznaczony, tzn ze mamy pobra� za��cznik z Alfresco
        if(Boolean.TRUE.equals(importAlfresco)){
            String uuid = (String) dockindKeys.get(ALFRESCO_UUID_CN);
            AlfrescoApi alfrescoApi = new AlfrescoApi();
			InputStream stream = null;
            try
            {
                alfrescoApi.connect();
                CmisObject doc = alfrescoApi.getObject(uuid);
                if(!(doc instanceof org.apache.chemistry.opencmis.client.api.Document)){
                    throw new Exception("Dokument o podanym uuid nie jest dokumentem Alfresco lub nie istnieje!");
                }
                Document document = Document.find(id);

                ContentStream contentStream = ((org.apache.chemistry.opencmis.client.api.Document) doc).getContentStream();
                if(contentStream == null)
                {
                    return;
                }
				byte[] bytes = IOUtils.toByteArray(contentStream.getStream());
				stream = new ByteArrayInputStream(bytes);
				Attachment attachment = new Attachment(doc.getName());
                document.createAttachment(attachment);
                attachment.createRevision(stream, bytes.length, contentStream.getFileName());


            }catch(Exception e){
                log.error("", e);
                throw new EdmException(e);
            }finally {
				IOUtils.closeQuietly(stream);
                alfrescoApi.disconnect();
            }


        }
    }

    public boolean searchCheckPermissions(Map<String,Object> values)
    {
        return true;
    }

	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		
	}
	

	/**
	 * Metoda wywo�ywana w trakcie wykonania akcji zwi�zanych z archiwizacj� dokumentu.
	 * Wi��e dokument z akcjami archiwalnymi
	 * 
	 * @param document - obj. dokumentu
	 * @param type - typ dokumentu (archiwalny,przychodz�cy,wychodz�cy,wewn�trzny)
	 * @param as - obj. wykonuj�cy akcje archiwalne
	 * 
	 */
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
        FieldsManager fm = document.getFieldsManager();

        if(AvailabilityManager.isAvailable("app.parametrization.pg.enabled")) {
            if (StringUtils.isEmpty(fm.getStringValue(PgDocInLogic.BARCODE_CN))) {
                try {
                    Map<String, Object> values = new HashMap<String, Object>();
                    generateBarcode(fm, values);
                    fm.getDocumentKind().setOnly(document.getId(), values);
                } catch (Exception e) {
                    log.error("Barcode creation error");
                }
            }
        }

        as.useFolderResolver(document);
		as.useAvailableProviders(document);
		log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
	}

    @Override
	public String onStartManualAssignment(OfficeDocument doc, String fromUsername, String toUsername)
	{
		log.info("--- PGDocInLogic : onStartManualAssignment  dekretacja reczna dokument�w :: dokument ID = "+doc.getId()+" : pomiedzy nadawc� -->"+fromUsername +" a odbiorc� ->"+toUsername );
		return toUsername;
	
	}
    
    /**
     * Metoda wywo�ywana przy starcie procesu
     * 
     * @param document
     * @param event
     */
	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- PGDocInLogic : START PROCESS !!! ---- {}", event);
		try
		{

			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, DSApi.context().getDSUser().getName());
			event.setAttribute(ASSIGN_USER_PARAM, map.get(ASSIGN_USER_PARAM));
			//map.put(ASSIGN_DIVISION_GUID_PARAM,event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			DSDivision[] divs = DSApi.context().getDSUser().getOriginalDivisionsWithoutGroup();
			if (divs.length > 0)
			{
				for (DSDivision d : divs)
				{
				map.put(ASSIGN_DIVISION_GUID_PARAM, d.getGuid() != null ? d.getGuid() : DSDivision.ROOT_GUID);
				event.setAttribute(ASSIGN_DIVISION_GUID_PARAM,  map.get(ASSIGN_DIVISION_GUID_PARAM));
				}
			} else{
				map.put(ASSIGN_DIVISION_GUID_PARAM, DSDivision.ROOT_GUID);
				event.setAttribute(ASSIGN_DIVISION_GUID_PARAM,  map.get(ASSIGN_DIVISION_GUID_PARAM));
			}

			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));

		} catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}


	}
	 
	@Override
	public void validate(Map<String, Object> values, DocumentKind kind,Long documentId) throws EdmException {
	
		System.out.println(kind.getCn());
		System.out.println(values);
		try
		{
            if (values.get("ZEWNETRZNY")!=null && (Boolean)values.get("ZEWNETRZNY") && values.get("RECIPIENT")!=null){

                long RecipientID =  (Long) values.get("RECIPIENT");

                Map<String,String> map =  (Map<String, String>) values.get("RECIPIENT_PARAM");
                if( map.get("PESEL")!=null &&  !map.get("PESEL").isEmpty()){
                    PreparedStatement ps = DSApi.context().prepareStatement("update dso_person set PESEL = ? where id = ?");
                    ps.setString(1, map.get("PESEL"));
                    ps.setLong(2, RecipientID);
                    ps.execute();
                    ps.close();
                }
            }
		} catch (SQLException e)
		{
			log.error("", e);
		}

        Object barcodeField = containsField(values, BARCODE_CN);
        if(AvailabilityManager.isAvailable("PgValidateBarcode") && barcodeField != null && !ChecksumAlgorithm.PG.isValid(barcodeField.toString())){
            throw new EdmException("Niepoprawny barkod!");
        }
	}


	

	/** Nadanie numeru KO i przypinanie dokumentu do dziennika  wychodz�cych 
	* @param oficeDocument - dokument, kt�rego operacja dotyczy
	*/
	public void bindToJournal(OfficeDocument oficeDocument ,ActionEvent event) throws EdmException
	{/*
		OutOfficeDocument doc = (OutOfficeDocument) oficeDocument;
		Date entryDate = new Date();
		Journal journal = Journal.getMainOutgoing();
		Integer sequenceId = Journal.TX_newEntry2(journal.getId(), doc.getId(), entryDate);
		doc.setOfficeDate(entryDate);*/
	}

	
	

	public void specialSetDictionaryValues(Map<String, ?> dockindFields)
	{
		
	}

	/**
	 * Dodawanie warto�ci do s�ownika dokumentu
	 * 
	 * @param dictionaryName - nazwa s�ownika
	 * @param values - mapa warto�ci dodawanych do s�ownika
	 */
	public void addSpecialInsertDictionaryValues(String dictionaryName, Map<String, FieldData> values)
	{
		
	}

	/**
	 * Metoda dodaje warto�ci do s�ownika dokumentu
	 * @param dictionaryName - nazwa s�ownika
	 * @param values - wpis do s�ownika
	 * @param dockindFields - pole dokindu
	 */
	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		
	}
	@Override
	public void setDocValuesAfterImportFromEpuap(OfficeDocument document, EpuapSkrytka skrytka, boolean dokument1_upo0) throws EdmException
	{
		
	}

	@Override
	public void onStartProcess(OfficeDocument document )
	{
	
		try {
			Map<String, Object> map= Maps.newHashMap();
			  String guid = AdditionManager.getPropertyOrDefault("logic.PgDocInLogic.importDivisionGuid", DSDivision.ROOT_GUID);
	            log.debug("[onImportAction] setting divisionGuid = {}", guid);
	            document.setDivisionGuid(guid);
	            document.setCurrentAssignmentGuid(guid);
	            document.setAssignedDivision(guid);
			//map.put(ASSIGN_USER_PARAM, document.getAuthor());
			map.put(ASSIGN_DIVISION_GUID_PARAM, document.getDivisionGuid());
            map.put(Jbpm4WorkflowFactory.OBJECTIVE, "Do Realizacji zeskanowanego dokumentu");
            map.put(Jbpm4Constants.KEY_PROCESS_NAME, Jbpm4Constants.MANUAL_PROCESS_NAME);
           
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	
	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret= new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	@Override
	public void onRejectedListener(Document doc) throws EdmException
	{
	}

	@Override
	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException
	{
	}
	
	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException,
			EdmException
	{ 
		super.onSaveActions(kind, documentId, fieldValues);
		//if(fieldValues.get(PgPackageLogic.CN_IDENTYFIKATOR_PACZKI)!=null)
		//insertBarcodeToPackageDocument(documentId,(String)fieldValues.get(PgPackageLogic.CN_IDENTYFIKATOR_PACZKI),kind);
		String a = "";
		
	}
	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event ,String filePrefix) throws EdmException
	{
		try {
			String guid = "";
			Map<String,String> punktyKancelryjne = AdditionManager.getPropertyAsMap("logic.PgDocInLogic.importedDocument.MultiDivisionMapPrefixToGuid");
			Map<String, Object> map= Maps.newHashMap();
			if(filePrefix!=null && !filePrefix.isEmpty()){
				OfficePoint op = OfficePoint.findByPrefix(filePrefix+"_");
				if(op!=null){
					guid = DSDivision.findById(op.getDivisionId()).getGuid();
				}else {
					 guid =punktyKancelryjne.get(filePrefix);
				}
			}else{
				 guid = AdditionManager.getPropertyOrDefault("logic.PgDocInLogic.importDivisionGuid", DSDivision.ROOT_GUID);
				  log.debug(" [onImportAction] setting divisionGuid ->  Nie znaleziono dzialu dla prefixu = "+ filePrefix +"dekretuje na :" +guid );
			}
	            log.debug("[onImportAction] setting divisionGuid = {}", guid);
	            document.setDivisionGuid(guid);
	            document.setCurrentAssignmentGuid(guid);
	            document.setAssignedDivision(guid);
			//map.put(ASSIGN_USER_PARAM, document.getAuthor());
			map.put(ASSIGN_DIVISION_GUID_PARAM, document.getDivisionGuid());
            map.put(Jbpm4WorkflowFactory.OBJECTIVE, "Do Realizacji zeskanowanego dokumentu");
            map.put(Jbpm4Constants.KEY_PROCESS_NAME, Jbpm4Constants.MANUAL_PROCESS_NAME);
           
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	
	}
	/**
	 * 
	 * Metoda ustawia na paczce dokument�w barcode
	 * 
	 * @param documentId
	 * @param barcode
	 * @param kind
	 */
	private void insertBarcodeToPackageDocument(Long documentId, String barcode, DocumentKind kind)
	{

		try
		{
			String sqlMetadane = "insert into ds_ar_dodatkowe_metadane" + " ( id,nazwapola, textdata) values(?,?,?)";
			OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId);
			List<OfficeDocument> listPackageDocuments = OfficeDocument.findAllByBarcode(barcode);
			for (OfficeDocument packageDocument : listPackageDocuments)
			{

				PreparedStatement ps;

				ps = DSApi.context().prepareStatement(
						" select textdata from ds_ar_dodatkowe_metadane  dm"
								+ "  left join ds_package_documents_multiple mult on (dm.id = mult.field_val ::INTEGER) where mult.document_id = ?");
				ps.setLong(1, packageDocument.getId());

				ResultSet rs = ps.executeQuery();
				boolean hasbarcode = false;
				if (!rs.next())
				{
					hasbarcode = false;//nieistnieje paczka o takim barkodzie
				} else
				{
					while (rs.next())
					{
						String desc = rs.getString(1);
						if (desc.contains(doc.getBarcode()))
						{
							hasbarcode = true;
							break;
							//zawiewra juz tak iwpis w bazie danych dana paczka dokument�w 
						}
					}
					Integer id_wpisu = null;
					//jesli nie posiada barkodu w takiej formie trzeba dodac do tabeli
					if (!hasbarcode)
						id_wpisu = null;
					rs = null;
					ps = DSApi.context().prepareStatement("select nextval('ds_ar_dodatkowe_metadane_id_seq');");
					rs = ps.executeQuery();
					if (rs.next())
					{
						id_wpisu = rs.getInt(1);
					}
					if (id_wpisu != null)
					{
						String sqlPackageDocument = "insert into " + packageDocument.getDocumentKind().getMultipleTableName()
								+ "(document_id, field_cn, field_val) values(?,?,?)";
						ps = DSApi.context().prepareStatement(sqlMetadane);
						ps.setInt(1, id_wpisu);
						ps.setString(2, "Dokumenty z paczki");
						ps.setString(3, "Identyfikator pisma :" + doc.getBarcode());
						ps.execute();
						ps = DSApi.context().prepareStatement(sqlPackageDocument);
						ps.setLong(1, packageDocument.getId());
						ps.setString(2, PgPackageLogic.CN_DODATKOWE_METADANE);
						ps.setString(3, id_wpisu.toString());
						ps.execute();
					}
					ps.close();
					rs.close();
				}
			}


		} catch (SQLException e)
		{
			log.error("", e);
			DSApi.context()._rollback();
		} catch (EdmException e)
		{
			log.error("", e);
			DSApi.context()._rollback();
		}


	}

	@Override
	public void revaluateFastAssignmentSelectetValuesOnDocument(OfficeDocument doc, String fastAssignmentSelectUser, String fastAssignmentSelectDivision)
			throws UserNotFoundException, EdmException
	{

		fastAssignmentSelectUser = DSApi.context().getDSUser().getName();

		DSDivision[] divs = DSApi.context().getDSUser().getOriginalDivisionsWithoutGroup();
		if (divs.length > 0)
		{
			for (DSDivision d : divs)
			{
				fastAssignmentSelectDivision = (d.getGuid() != null ? d.getGuid() : DSDivision.ROOT_GUID);

			}
		} else{
			fastAssignmentSelectDivision = DSDivision.ROOT_GUID;
		}
		doc.setAssignedDivision(fastAssignmentSelectDivision);
		doc.setCurrentAssignmentGuid(fastAssignmentSelectDivision);
		doc.setCurrentAssignmentUsername(fastAssignmentSelectUser);
	}

    @Override
    public void onImportAction(Document document) {
        if(document instanceof OfficeDocument) {
            OfficeDocument officeDoc = (OfficeDocument) document;
            String guid = AdditionManager.getPropertyOrDefault("logic.PgDocInLogic.importDivisionGuid", DSDivision.ROOT_GUID);
            log.debug("[onImportAction] setting divisionGuid = {}", guid);
            officeDoc.setDivisionGuid(guid);
            officeDoc.setCurrentAssignmentGuid(guid);
        }
    }
}
	
