package pl.compan.docusafe.parametrization.pg;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.permission.rule.PermissionRule;


public class IsDocumentInPackage implements PermissionRule
{

	@Override
	public boolean validateRead(Document document) throws EdmException
	{
		return document.getInPackage();
	}

	@Override
	public boolean validateModify(Document document) throws EdmException
	{
		
		return false;
	}

	@Override
	public boolean validateReadAttachments(Document document) throws EdmException
	{
		
		return document.getInPackage();
	}

	@Override
	public boolean validateModifyAttachments(Document document) throws EdmException
	{
		
		return false;
	}

}
