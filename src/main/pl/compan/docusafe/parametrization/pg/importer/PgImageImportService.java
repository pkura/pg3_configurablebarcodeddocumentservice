package pl.compan.docusafe.parametrization.pg.importer;

import pl.compan.docusafe.service.IterateTimerServiceDriver;

import static pl.compan.docusafe.service.barcodes.recognizer.ZxingUtils.decodeBarcode;

/**
 * <p>Serwis importuj�cy dokumenty (pisma) z plik�w pdf z zasobu.</p>
 *
 * <b>Zasada dzia�ania</b>
 *
 * <p>
 *     Co okre�lony czas serwis skanuje katalog <code>HOME/import/before_import</code>.
 *     Ka�dy plik z tego katalogu serwis zaci�ga na kolejk�, przerzuca do katalogu <code>HOME/import/do_import</code>
 *     i wrzuca do docusafowego DSI Importera z t� �cie�k�. Nast�pnie DSI Importer zaci�ga plik z <code>do_import</code>
 *     jako pismo, po czym po udanej operacji plik jest przenoszony do katalogu <code>HOME/import/after_import</code>.
 * </p>
 *
 * <p>
 *     Serwis ma mo�liwo�� odczytywania barkod�w z obrazk�w (za��cznik�w). Nale�y skonfigurowa� addition property
 *     o nazwie <code>service.property.ImageImportService.barcodeFormats</code> w formie listy oddzielonej przecinkami
 *     zawieraj�cej nazwy z enuma {@link com.google.zxing.BarcodeFormat}. <br>
 *     Na przyk�ad: <code>service.property.ImageImportService.barcodeFormats = CODE_128,QR_CODE</code>
 * </p>
 *
 * <p>
 *     Ze wzgl�du na konstrukcj� DSI Importera nie da�o si� zrobi�, �eby po b��dzie DSI Importera plik by� przenoszony
 *     do katalogu <code>failed_import</code>. Kwestia refactoringu, na kt�ry nie starczy�o czasu. W tej chwili plik zostaje w
 *     <code>do_import</code> (ale nie jest zaci�gany ponownie na kolejk� DSI Importera).
 * </p>
 *
 * <b>Konfiguracja</b>
 * <p>
 *     W <code>adds.properties</code>:
 *     <ul>
 *         <li>
 *             <code>service.period.ImageImportService</code> - okres w sekundach, czyli co ile sekund b�dzie skanowany katalog before_import
 *         </li>
 *         <li>
 *             <code>service.delay.ImageImportService</code> - op�nienie w sekundach, czyli po uruchomieniu Docusafe ile sekund czekamy do pierwszego uruchomienia serwisu
 *         </li>
 *     </ul>
 * </p>
 *
 * </p>
 *
 * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
public class PgImageImportService extends IterateTimerServiceDriver {
    public PgImageImportService() {
        setTask(new PgImageImporter());
    }
}
