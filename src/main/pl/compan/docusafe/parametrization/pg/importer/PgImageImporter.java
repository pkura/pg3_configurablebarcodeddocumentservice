package pl.compan.docusafe.parametrization.pg.importer;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Collections2;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.NotFoundException;
import com.itextpdf.text.DocumentException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jackrabbit.JackrabbitAttachmentRevision;
import pl.compan.docusafe.parametrization.pg.BarcodeHelper;
import pl.compan.docusafe.service.FileGroupTimerTask;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.barcodes.ConfigurableBarcodedDocumentService;
import pl.compan.docusafe.service.barcodes.PdfBarcodedDocument;
import pl.compan.docusafe.service.barcodes.recognizer.WireBarcodeFinder;
import pl.compan.docusafe.service.barcodes.recognizer.xing.BarcodeFinder;
import pl.compan.docusafe.service.imports.dsi.DSIAttribute;
import pl.compan.docusafe.service.imports.dsi.DSIBean;
import pl.compan.docusafe.service.imports.dsi.DSIManager;
import pl.compan.docusafe.service.imports.dsi.ImportHelper;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

public class PgImageImporter extends FileGroupTimerTask {

    private static final Logger log = LoggerFactory.getLogger(PgImageImporter.class);

    public static final String PARENT_SERVICE_NAME = "PgImageImportService";
    public static final String DOC_BARCODE_CN = "DOC_BARCODE";
    public static final String DOC_REMARKS_CN = "DOC_REMARKS";

    /**
     * Regexp for file extension
     */
    public static final String FILE_EXT_REGEXP = "(\\.[A-Za-z0-9]+)$";
    public static final String FILE_TIMESTAMP_REGEXP = "_[0-9]+"+FILE_EXT_REGEXP;

    private String docInDeliveryKind = "DOC_DELIVERY";
    private String filePrefix = "FILEPREFIX";
    private Map<DecodeHintType, Object> hints = new HashMap<DecodeHintType, Object>();

    private String docBarcodeCn;

    private String beforeImportPath;
    private String importPath;
    private String inXing;
    private String outXing;
    private String workXing;

    private static final String CLUSTER_FILENAME_PREFIX = "NODE_";

    public void updateConfiguration() {
        this.beforeImportPath = evalualtePath(ServiceDriver.getServicePropertyOrDefault(PARENT_SERVICE_NAME, "beforeImportPath", Docusafe.getHome() + "/import/before_import"));
        this.importPath = evalualtePath(ServiceDriver.getServicePropertyOrDefault(PARENT_SERVICE_NAME, "importPath", Docusafe.getHome() + "/import/do_import"));
        this.inXing = evalualtePath(ServiceDriver.getServicePropertyOrDefault(PARENT_SERVICE_NAME, "inXing", Docusafe.getHome() + "/import/xing_in"));
        this.outXing = evalualtePath(ServiceDriver.getServicePropertyOrDefault(PARENT_SERVICE_NAME, "outXing", Docusafe.getHome() + "/import/xing_out"));
        this.workXing = evalualtePath(ServiceDriver.getServicePropertyOrDefault(PARENT_SERVICE_NAME, "workXing", Docusafe.getHome() + "/import/xing_work"));

        this.setFolderPath(this.beforeImportPath);

        this.docBarcodeCn = ServiceDriver.getServicePropertyOrDefault(PgImageImportService.class.toString(), "barcodeFieldCn", DOC_BARCODE_CN);
    }

    protected String evalualtePath(String path) {
        return path == null ? null : path.replaceFirst("\\$HOME", Docusafe.getHome().getPath().replaceAll("\\\\", "/"));
    }

    public PgImageImporter() {
        super();
        setIterateInContext(true);
    }

    @Override
    public void beforeRun() throws Exception {
        updateConfiguration();

        super.beforeRun();

        // initialize barcode formats
        initBarcodeFormats();
    }

    private void initBarcodeFormats() {
        Collection<String> stringBarcodeFormats = ServiceDriver.getServicePropertyAsList(PARENT_SERVICE_NAME, "barcodeFormats");

        Collection<BarcodeFormat> barcodeFormats = Collections2.transform(stringBarcodeFormats, new Function<String, BarcodeFormat>() {
            @Override
            public BarcodeFormat apply(String name) {
                return BarcodeFormat.valueOf(name);
            }
        });

        hints = new HashMap<DecodeHintType, Object>();
//            hints.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);
        hints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
        hints.put(DecodeHintType.POSSIBLE_FORMATS, barcodeFormats);
    }


    @Override
    protected void forEach(List<File> files, int index) throws EdmException, IOException, SQLException {
        log.debug("[forEach] i = {}", index);

        if(files.size() == 0) {
            log.debug("[forEach] size == 0, continue");
        } else {

            String filename = files.get(0).getName().replaceFirst("\\.[A-Za-z0-9]+$", "");

            String clusterName = AdditionManager.getProperty("docusafe.cluster.name");
            if(clusterName != null) {
                if(! deleteFilesIfProcessed(clusterName, files)) {
                    return;
                }
            }

            final List<File> destFiles = getDestFiles(files);
            final Collection<String> filePaths = Collections2.transform(destFiles, new Function<File, String>() {
                @Override
                public String apply(File file) {
                    return file.getAbsolutePath();
                }
            });

            log.debug("[forEach] filePaths = {}", filePaths);

            File firstDestFile = destFiles.get(0);

            String firstFilePath = firstDestFile.getAbsolutePath();
            String userImportId = ((Long)new Date().getTime()).toString();
            String importDockind = "normal.image_import";

            log.info("[forEach] creating bean: {}", firstFilePath);
            DSIBean bean = ImportHelper.getSimpleDSIBean(userImportId, filename, importDockind, filePaths);

            // odczytujemy barcode z obrazk�w i dodajemy do pola "DOC_BARCODE"
            Optional<String> barcode = decodeBarcode(files, hints, bean, userImportId);
            String remarksFieldCn = ServiceDriver.getServicePropertyOrDefault(PARENT_SERVICE_NAME, "remarksFieldCn", DOC_REMARKS_CN);
			if (barcode.isPresent()) {
				log.debug("[forEach] znaleziono barkod: " + barcode.get());
				if (barcode.get() != null && barcode.get().length() != 12) {
					bean.addAttribute(new DSIAttribute(remarksFieldCn,"Rozpoznany barkod: " + barcode.get()
									+ "Nie jest poprawnym barcodem PG "));

				} else if (!BarcodeHelper.validateCheckCharacter(barcode.get())) {
					bean.addAttribute(new DSIAttribute(remarksFieldCn,
							"W Rozpoznanym barkodzie: " + barcode.get()
									+ " nie zgadza sie suma kontrolna "));
				} else {
					bean.addAttribute(new DSIAttribute(docBarcodeCn, barcode
							.get()));
				}

			} else {
				log.debug("[forEach] nie znaleziono barkodu");
			}
            bean.addAttribute(new DSIAttribute(docInDeliveryKind, ServiceDriver.getServicePropertyOrDefault("PgImageImportService", "DefaultinOfficeDocumentDeliveryId", "1")));
            if(filename.contains("_")){
            	String prefix = "";
            	  String fPrefix=""; 
            	  String fPrefix2 ="";
           	    if(filename.indexOf("_") != -1) {
           	    	try{
           	    		fPrefix = filename.split("_")[0];
          		    if(!fPrefix.contains("K")){
          		    	fPrefix2 = filename.split("_")[1];
          		    	prefix = fPrefix2;
          		    }
          		    else{
          		    	prefix =fPrefix;
          		    }
           	    	}
          		    catch (IndexOutOfBoundsException e) {
						log.info("Problem z pobraniem prefixu dla pliku" + filename);
					}
           	    	
          		    
          		    String a = "" ;
         	 	    bean.addAttribute(new DSIAttribute(filePrefix,prefix));
                }
            }
         
            DSIManager manager = new DSIManager();
            manager.saveDSIBean(bean, DSApi.context().session().connection());

            moveFiles(files, destFiles);
        }

    }

    /**
     * <p>
     *     Ignorujemy plik je�li wcze�niej by� ju� wgrany i procesujemy w klastrze.
     *     Usuwa pliki je�li by�y przeprocesowane.
     *
     *     Je�li zwr�ci false wtedy powinni�my dalej procesowa� plik.
     * </p>
     * @param files
     * @return
     */
    private boolean deleteFilesIfProcessed(String clusterName, List<File> files) throws EdmException {
        String filename = files.get(0).getName();

        log.debug("[deleteFilesIfProcessed] clusterName = {}, filename = {}", clusterName);

        // if filename is not with node prefix then ignore
        if(! filename.matches("^"+ CLUSTER_FILENAME_PREFIX+".*")) {
            log.debug("[deleteFilesIfProcessed] no node prefix, return false, prefix = {}", CLUSTER_FILENAME_PREFIX);
            return false;
        }

        // do nothing if file is not from this cluster
        if(! filename.matches("^"+ CLUSTER_FILENAME_PREFIX + clusterName+"_.*")) {
            log.debug("[deleteFilesIfProcessed] not current cluster name in filename, return false", CLUSTER_FILENAME_PREFIX);
            return false;
        }
        // check if file was processed
        //   if it was -> return false
        //   otherwise -> return true
        else {
            if(wasProcessed(filename)) {
                log.debug("[deleteFilesIfProcessed] file was processed already, delete files and return false, filename = {}", filename);
                deleteFiles(files);
                return false;
            } else {
                log.debug("[deleteFilesIfProcessed] file was not processed, return true, filename = {}", filename);
                return true;
            }
        }
    }

    private void deleteFiles(List<File> files) {
        for(File f: files) {
            f.delete();
        }
    }

    /**
     * <p>
     *     Pseudo-code: {@code return (SELECT * FROM ds_attachment_revision WHERE originalfilename = filename).length > 0}
     * </p>
     * @param filename
     * @return
     * @throws EdmException
     */
    private boolean wasProcessed(String filename) throws EdmException {
        Criteria criteria = DSApi.context().session().createCriteria(JackrabbitAttachmentRevision.class);
        criteria.add(Restrictions.eq("originalFilename", filename));
        return criteria.list().size() > 0;
    }

    private void moveFiles(List<File> files, List<File> destFiles) throws IOException {
        for(int i = 0; i < files.size(); i++) {
            File file = files.get(i);
            File destFile = destFiles.get(i);

            log.info("[forEach] moving file {} --> {}", file.getAbsolutePath(), destFile.getAbsolutePath());
            FileUtils.moveFile(file, destFile);
        }
    }

    private List<File> getDestFiles(List<File> files) {
        List<File> destFiles = new ArrayList<File>();

        for(File file: files) {
            String destFilename = putTimestamp(file.getName());
            File destFile = new File(importPath, destFilename);
            destFiles.add(destFile);
        }

        return destFiles;
    }

    private String getFilePath(File file) {
        String destFilename = putTimestamp(file.getName());
        File destFile = new File(importPath, destFilename);

        String filePath = destFile.getAbsolutePath();

        return filePath;
    }

    public Optional<String> decodeBarcode(Collection<File> files, Map<DecodeHintType, Object> hints, DSIBean bean, String userImportId) {
        Optional<String> barcode = Optional.absent();

        if(files.size() == 0) {
            log.debug("[decodeBarcode] No files to process, userImportId = {}", userImportId);
            return Optional.absent();
        }

        String remarksFieldCn = ServiceDriver.getServicePropertyOrDefault(PARENT_SERVICE_NAME, "remarksFieldCn", DOC_REMARKS_CN);

        try {
            barcode = recognizeBarcode(files, bean);
        } catch (IOException e) {
            log.error("[decodeBarcode] error", e);
        } catch (DocumentException e) {
            log.error("[decodeBarcode] error", e);
        } catch (NotFoundException e) {
            log.error("[decodeBarcode] error", e);
        }

        if(! barcode.isPresent()) {
            log.debug("[decodeBarcode] no barcode found, userImportId = {}", userImportId);
            bean.addAttribute(new DSIAttribute(remarksFieldCn, "Nie rozpoznano barkodu w �adnym z za��cznik�w"));
        }

        return barcode;
    }

	public Optional<String> recognizeBarcode(Collection<File> files,
			DSIBean bean) throws IOException, NotFoundException,
			DocumentException {

		NotFoundException notFoundException = null;
		String barcode = null;

		WireBarcodeFinder configurableBarcodedDocumentService = new ConfigurableBarcodedDocumentService();

		for (File file : files) {
			log.debug("[recognizeBarcode] file.name = {}", file.getName());

			String ext = FilenameUtils.getExtension(file.getName());
			String name = FilenameUtils.getName(file.getName());
			String baseName = FilenameUtils.getBaseName(file.getName());
			String pdfFilename = baseName + ".pdf";
			File[] pdfFiles;

			if (ext.equals("tif") || ext.equals("tiff")) {

				File tifFile = null;
				File pdfFile = null;

				try {

					ImageSeparator imageSeparator = new ImageSeparator();

					tifFile = new File(inXing + "/" + file.getName());
					pdfFile = new File(workXing + "/" + baseName + ".pdf");
					tifFile.createNewFile();

					FileUtils.copyFile(file, tifFile);

					imageSeparator.setInFileDir(inXing);
					imageSeparator.setInTifFileName(file.getName());
					imageSeparator.setOutFileDir(outXing);
					imageSeparator.setWorkFileDir(workXing);
					imageSeparator.setWorkPdfFileName(pdfFilename);

					imageSeparator.convertTiffToPdf();

					pdfFiles = new File[] { pdfFile };
					barcode = getBarcodeFromRvision(pdfFiles);

					if (barcode == null) {
						return Optional.absent();
						/*barcode = configurableBarcodedDocumentService
								.find(pdfFiles);*/
					}
					// barcode =
					// configurableBarcodedDocumentService.find(pdfFiles);

				} finally {
					try {
						if (pdfFile != null && pdfFile.exists()) {
							FileUtils.forceDelete(pdfFile);
							
						}
						if (tifFile != null && tifFile.exists()) {
							FileUtils.forceDelete(tifFile);
							
						}
					} catch (Exception ex) {
						log.error(ex.toString());
					}
				}
			} else if (ext.equals("pdf")) {
				pdfFiles = new File[] { file };
				barcode = getBarcodeFromRvision(pdfFiles);
				if (barcode == null) {
					return Optional.absent();
				/*	barcode = configurableBarcodedDocumentService.find(pdfFiles);*/
				}
			}

		}
		if (barcode != null) {
			return Optional.of(barcode);
		}
		return Optional.absent();
	}
    private String getBarcodeFromRvision(File[] pdfFiles) throws IOException {
    	BarcodeFinder finder = new BarcodeFinder();
        for (int i = 0; i < pdfFiles.length; i++) {
			File file2 = pdfFiles[i];
			return 	finder.findBarcode(file2);
	}
		return null;
        

	}

    /**
     * <p>Adds timestamp to filename</p>
     *
     * <p>
     *     Example:<br>
     *     <code>putTimestamp("myFile.txt")</code> returns <code>"myFile_123123123.txt"</code>
     * </p>
     *
     * @param filename
     * @return
     */
    public static String putTimestamp(String filename) {
        Long timestamp = new Date().getTime();

        String replace = "_" + timestamp.toString() + "$1";

        return filename.replaceFirst(FILE_EXT_REGEXP, replace);
    }

    /**
     * <p>Removes timestamp from filename added by <code>putTimestamp()</code></p>
     *
     * <p>
     *     Example:<br>
     *     <code>removeTimestamp("myFile_123123312.txt")</code> returns <code>"myFile.txt"</code>
     * </p>
     *
     * @param filename
     * @return
     */
    public static String removeTimestamp(String filename) {
        return filename.replaceFirst(FILE_TIMESTAMP_REGEXP, "$1");
    }

}
