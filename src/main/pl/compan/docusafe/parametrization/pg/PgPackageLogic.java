package pl.compan.docusafe.parametrization.pg;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;

import edu.emory.mathcs.backport.java.util.Arrays;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.service.ServiceDriverNotSelectedException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.ServiceNotFoundException;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

/**
 * <h1>PgPackageLogic</h1>
 *
 * <p>
 *     Klasa s逝篡 do obs逝gi zachowania/implementaji funkcjonalno軼i paczki/przesy趾i.
 * </p>
 */
public class PgPackageLogic extends AbstractDocumentLogic
{
	private static PgPackageLogic instance;
	protected static Logger log = LoggerFactory.getLogger(PgPackageLogic.class);
	public static final String CN_DOC_DESCRIPTION = "DOC_DESCRIPTION";
	public static final String CN_AUTOR_DOKUMENTU ="DOC_AUTOR";
	public static final String CN_DZIAL_AUTORA_DOKUMENTU ="DOC_AUTORDIVISION";
	public static final String CN_DATA_DOKUMENTU = "DOC_DATE";
	public static final String multipleValues = "M_DICT_VALUES";
	public static final String multipleDOCUMENTS = "DOCUMENTS";
	public static final String CN_DODATKOWE_METADANE = "DODATKOWE_METADANE";
	public static final String CN_ODBIORCA = "RECIPIENT_HERE";
	public static final String CN_NADAWCA = "SENDER_HERE";
	public static final String CN_DWR_ODBIORCA = "DWR_RECIPIENT_HERE";
	public static final String CN_DWR_NADAWCA = "DWR_SENDER_HERE";
	public static final String CN_DWR_EMAILNADAWCA  = "DWR_EMAILNADAWCA";
	public static final String CN_DWR_EMAILODBIORCA = "DWR_EMAILODBIORCA";	 
	public static final String CN_DWR_EMAIL_DICTIONARY = "DWR_EMAIL";
	public static final String CN_EMAILNADAWCA  = "EMAILNADAWCA";
	public static final String CN_EMAILODBIORCA = "EMAILODBIORCA";	 
	public static final String CN_SENDER_EMAIL_DICTIONARY = "SENDER_HERE_EMAIL";	
	public static final String CN_RECIPIENT_EMAIL_DICTIONARY = "RECIPIENT_HERE_EMAIL";	
	public static final String CN_STATUS_DOKUMENTU = "STATUS";
	public static final String CN_USER_ODBIORCA = "USER_ODBIORCA";
	public static final String CN_USER_NADAWCA = "USER_NADAWCA";
	public static final String CN_KOD_KRESKOWY = "DOC_BARCODE";
	public static final String CN_DWR_PACZKA_NA_DZIAL = "DWR_PACZKA_NA_DZIAL";
	public static final String CN_DWR_DZIAL = "DWR_DZIAL"; 
	public static final String CN_PACZKA_NA_DZIAL = "PACZKA_NA_DZIAL";
	public static final String CN_DZIAL = "DZIAL";
	public static final String CN_LOKALIZACJA_PACZKI = "LOKALIZACJA_PACZKI";
	public static final String CN_DOC_FROM_PACKAGE = "DOC_FROM_PACKAGE"; 
	public static final String CN_IDENTYFIKATOR_PACZKI = "IDENTYFIKATOR_PACZKI"; 
	
	public static PgPackageLogic getInstance()
	{
		if (instance == null)
			instance = new PgPackageLogic();
		return instance;
	}
	@Override
	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
		
	}
	
	@Override
	public boolean searchCheckPermissions(Map<String, Object> values)
	{
		return false;
	}
	@Override
	public boolean canChangeDockind(Document document) throws EdmException
	{
		return false;
	}
	
	/**
	 * Metoda ustawia dodatkowe dane dla szblon闚 dokumentu
	 * 
	 */
	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
			try
			{
				String[] ids;
				Document doc = Document.find(docId);
				FieldsManager fm = doc.getFieldsManager();
				fm.initialize();
				 
				String odbiorcy = fm.getStringKey(CN_ODBIORCA)!=null?fm.getStringKey(CN_ODBIORCA):"" ;
	    		if (odbiorcy.startsWith("[")) {
	    			odbiorcy= odbiorcy.substring(1, odbiorcy.length() - 1);
	    			if (odbiorcy.contains(","))
	    				ids= odbiorcy.split(",");
	    			else ids= new String[1];
	    			ids[0]= odbiorcy;
	    			
	    		} else {
	    			ids= new String[1];
	    			ids[0]= odbiorcy;
	    		}
	    		for (String id : ids) {
				Map<String,Object> odbiorca = DwrDictionaryFacade.dictionarieObjects.get(CN_ODBIORCA).getValues(id.trim());
				
			
				values.put("FIRSTNAME", odbiorca.get("RECIPIENT_HERE_FIRSTNAME")!=null ? odbiorca.get("RECIPIENT_HERE_FIRSTNAME") : "" );
				values.put("LASTNAME", odbiorca.get("RECIPIENT_HERE_LASTNAME")!=null ? odbiorca.get("RECIPIENT_HERE_LASTNAME") : "" );
				values.put("DIVISION", odbiorca.get("RECIPIENT_HERE_DIVISION")!=null ? odbiorca.get("RECIPIENT_HERE_DIVISION") : "" );
				
	    		}
	    		
	    		if (odbiorcy.isEmpty() && fm.getFieldValues()!=null && fm.getFieldValues().get(PgPackageLogic.CN_PACZKA_NA_DZIAL)!=null 
	    					&& ((Boolean) fm.getFieldValues().get(PgPackageLogic.CN_PACZKA_NA_DZIAL)==true)){
	    			
	    				DSDivision div = DSDivision.findById(fm.getEnumItem(PgPackageLogic.CN_DZIAL).getId());
	    			values.put("DIVISION", div.getName());
	    			}
	    		
	    	//	values.put("BARCODE",fm.getStringValue(CN_KOD_KRESKOWY)!=null ? fm.getStringValue(CN_KOD_KRESKOWY):"");
			
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
	}
	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		log.info("type: {}", type);
		Map<String, Object> values= new HashMap<String, Object>();
		
		DSUser autor =  DSApi.context().getDSUser();
		if (autor.getEmail()!=null){
			values.put(CN_EMAILNADAWCA, autor.getEmail());
		}
		values.put(CN_DATA_DOKUMENTU, new Date());
	//	values.put(CN_NADAWCA, autor.getName()); 
		//values.put(CN_NADAWCA,"u:eodi;d:99133E2557187E76140ED63E3775E96915F");
		values.put(CN_STATUS_DOKUMENTU, 10);
		values.put(CN_NADAWCA, getCurrentUserAsSenderParams());
		fm.reloadValues(values);
		
	}
	/**
	 * Metoda wywo造wana w trakcie wykonania akcji zwi您anych z archiwizacj� dokumentu.
	 * Wi捫e dokument z akcjami archiwalnymi
	 * 
	 * @param document - obj. dokumentu
	 * @param type - typ dokumentu (archiwalny,przychodz帷y,wychodz帷y,wewn皻rzny)
	 * @param as - obj. wykonuj帷y akcje archiwalne
	 * 
	 */
	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		as.useFolderResolver(document);
		as.useAvailableProviders(document);
		log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
		
	}
	/**
	 * Metoda tworzy string informacji o u篡tkowniku, sk豉daj帷y si� z username i guid dzia逝 do kt鏎ego nale篡 powy窺zy u篡tkownik
	 * 
	 * @return String informacji o u篡tkowniku
	 * @throws UserNotFoundException
	 * @throws EdmException
	 */
	private String getCurrentUserAsSenderParams() throws UserNotFoundException, EdmException
	{		// przyk像dowy string dla recipientHERE u:eodi;d:99133E2557187E76140ED63E3775E96915F
		StringBuilder sb = new StringBuilder();
		sb.append("u:");
	
		String userDivision = "";
		DSUser user = DSApi.context().getDSUser();
		sb.append(user.getName()+";");
		
		DSDivision[] divisions = user.getOriginalDivisions();
		sb.append("d:");
		for (int i = 0; i < divisions.length; i++)
		{
			if (divisions[i].isPosition())
			{
				
				DSDivision parent = divisions[i].getParent();
				if (parent != null)
				{
					userDivision = parent.getGuid();
					break;
				} else
				{
					userDivision = divisions[i].getGuid();
				}
			} else if (divisions[i].isDivision())
			{
				userDivision = divisions[i].getGuid();
				break;
			}
		}
		sb.append(userDivision);
		return sb.toString();
	}
	/**
	 * Metoda dodaje warto�ci do s這wnika dokumentu
	 * @param dictionaryName - nazwa s這wnika
	 * @param values - wpis do s這wnika
	 * @param dockindFields - pole dokindu
	 */
	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		try {
		if (dictionaryName.contains(multipleDOCUMENTS)&& !values.containsKey("id"))
		{  
			values.put(dictionaryName + "_USER_TASK", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, DSApi.context().getDSUser().getName()));
			values.put(dictionaryName + "_DOC_IN_PACKAGE", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING,"0"));
		}
		}catch (Exception e) {
			log.error("B豉d podczas wyszukiwania zadan uzytkownika po nazwie", e);
		}
	}
	@Override
	public void validate(Map<String, Object> values, DocumentKind kind) throws EdmException
	{
		Map<String, Object> mapDocuments =  (Map<String, Object>) values.get(multipleDOCUMENTS);
		super.validate(values, kind);
		if(values.get(CN_DWR_EMAILNADAWCA)==null){
			values.put(CN_DWR_EMAILNADAWCA, " ");
		}
	}
	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		Field msg = null;
		try{
		
		//Map<String, FieldData> mapNadawca =  (Map<String, FieldData>) values.get(CN_DWR_NADAWCA).getDictionaryData();
		Map<String, FieldData> mapOdbiorca =  (Map<String, FieldData>) values.get(CN_DWR_ODBIORCA).getDictionaryData();
		
	//	if ( mapNadawca !=null &&  mapNadawca.get(CN_SENDER_EMAIL_DICTIONARY)!=null)
		//	values.put(CN_DWR_EMAILNADAWCA, mapNadawca.get(CN_SENDER_EMAIL_DICTIONARY));
		
		if( mapOdbiorca !=null &&  mapOdbiorca.get(CN_RECIPIENT_EMAIL_DICTIONARY)!=null)
			values.put(CN_DWR_EMAILODBIORCA, mapOdbiorca.get(CN_RECIPIENT_EMAIL_DICTIONARY));
		
/*		if (values.get(CN_DWR_EMAILNADAWCA)!=null  && mapNadawca.containsKey(CN_SENDER_EMAIL_DICTIONARY)
				&& mapNadawca.get(CN_SENDER_EMAIL_DICTIONARY).getStringData().isEmpty())
				mapOdbiorca.put(CN_SENDER_EMAIL_DICTIONARY,values.get(CN_DWR_EMAILNADAWCA));
		
		if (values.get(CN_DWR_EMAILODBIORCA)!=null  && mapOdbiorca.containsKey(CN_RECIPIENT_EMAIL_DICTIONARY)
				&& mapOdbiorca.get(CN_RECIPIENT_EMAIL_DICTIONARY).getStringData().isEmpty())
				mapOdbiorca.put(CN_RECIPIENT_EMAIL_DICTIONARY,values.get(CN_DWR_EMAILNADAWCA));*/
		/*if (values.get(CN_GENERATE_PRZYJMIJ) != null){
			
		}*/
		
		
		}catch (Exception e) {
			log.error("", e);
		}
	
		return msg ;
		
	}

	@Override
	public void correctValues(Map<String, Object> values, DocumentKind kind, long documentId) throws EdmException
	{
		DSUser user = DSApi.context().getDSUser();
		FieldsManager fm = kind.getFieldsManager(documentId);
		OfficeDocument doc = OfficeDocument.find(documentId);
		doc.getRecipients();
		boolean czyOdbiorcaPaczki = false;

		if (fm.getValue(CN_STATUS_DOKUMENTU) != null &&  fm.getEnumItem(CN_STATUS_DOKUMENTU).getId() != 40)
		{
			throw new EdmException("Paczka nie jest gotowa do wysy趾i bad� zosta豉 ju� odebrana");
		}
		if (!isCorrectRecipientOrSender(fm,doc))
		{
			setPackageLocalization(doc, czyOdbiorcaPaczki);
			String description = "Paczka zosta豉 odebrana przez u篡tkownika  " + user.asFirstnameLastname() + " w celu dostarczenia , w dniu :"
					+ DateUtils.formatCommonDateTime(new Date());
			addWpisHistDoDokuemntu(description, doc);
			sendDostarczycielMail(fm, documentId, description);
			createDWProcessFromPackage(fm, documentId, description, false);
		} else
		{
			czyOdbiorcaPaczki = true;
			String description = "Przesy趾a zosta豉 odebrana przez odbiorce : " + user.asFirstnameLastname() + DateUtils.formatCommonDateTime(new Date());
			addWpisHistDoDokuemntu(description, doc);
			setPackageLocalization(doc, czyOdbiorcaPaczki);
			Map<String, Object> params = Maps.newHashMap();
			params.put(Jbpm4WorkflowFactory.OBJECTIVE, " Potwierdzenie odbioru Przesy趾i");
			params.put(Jbpm4WorkflowFactory.DOCDESCRIPTION, "Do realizacji");
			//DSApi.context().commit();DSApi.context().begin();
			for (String aid : Jbpm4ProcessLocator.taskIds(doc.getId()))
			{
				TaskSnapshot taskSnapshot = TaskSnapshot.getInstance().findByActivityKey(aid.replaceAll("[^,]*,", ""));
				if (taskSnapshot != null && taskSnapshot.getProcess().equals("jbpm4") && taskSnapshot.getDescription().isEmpty())
					Jbpm4WorkflowFactory.reassign(aid, doc.getId(), doc.getAuthor(), DSApi.context().getDSUser().getName(), params);
				// TaskSnapshot.updateByDocumentId(doc.getId(), "anyType");
			}
			DSApi.context().commit();
			DSApi.context().begin();

			createDWProcessFromPackage(fm, documentId, description, true);
		}
	}
	
	/**
	 * Metoda aktualizuje lokalizacje paczki/przesy趾i
	 * 
	 * @param doc - dokument
	 * @param czyOdbiorcaPaczki - warto嗆 boolean wskazuj帷a czy paczka znajduje si� u odbiorcy
	 * @throws EdmException
	 */
	public void setPackageLocalization(OfficeDocument doc , boolean czyOdbiorcaPaczki) throws EdmException{
		StringBuilder sb = new StringBuilder();
		sb.append("Paczka znajduje sie u:");
		sb.append(DSApi.context().getDSUser().asFirstnameLastname());
		sb.append(" w celu ");
		if(!czyOdbiorcaPaczki) 
			sb.append("dostarczenia paczki");
			else  
			//sb.append("potwierdzenia odbioru paczki dokument闚"); 
			sb.append("potwierdzenia odbioru paczki"); 
				sb.append(" w dniu :" + DateUtils.formatCommonDateTime(new Date()));
		
		 Map<String,Object> values = new HashMap<String, Object>();
		 values.put(CN_LOKALIZACJA_PACZKI, sb.toString());
		doc.getDocumentKind().setOnly(doc.getId(),values);
		DSApi.context().session().update(doc);
	}
	
	/**
	 * Metoda usuwa duplikaty z listy
	 * 
	 * @param listLong
	 */
	private static void removeDuplicates(List<Long> listLong){
		HashSet set = new HashSet(listLong);
		listLong.clear();
		listLong.addAll(set);
	}	
	
	/**
	 * Metoda sprawdza czy istniej� nieprawid這we dokumenty
	 * 
	 * @param selectetIds
	 * @param baseId
	 * @return
	 * @throws EdmException
	 */
	private boolean isAnyInvalidDocument(List<Long> selectetIds, List<Long> baseId) throws EdmException
	{
		List<Long> wybrane = new ArrayList();
		List<Long> baseDocumentIds = new ArrayList();
		for (Long l : selectetIds)
		{
			wybrane.add(l);
		}
		for (Long l : baseId)
		{
			baseDocumentIds.add(l);
		}

		baseDocumentIds.removeAll(wybrane);
		if (!baseDocumentIds.isEmpty())
			return true;
		else
		{
			wybrane.removeAll(baseId);
			if (!wybrane.isEmpty())
				return true;
		}

		return false;


	}

	@Override
	public void setAfterCreateBeforeSaveDokindValues(DocumentKind kind, Long documentId, Map<String, Object> dockindKeys) throws EdmException
	{
		int  statusID = (Integer) dockindKeys.get(CN_STATUS_DOKUMENTU);
		if (statusID == 40 && (DSApi.context().getDSUser().getName().contains(OfficeDocument.find(documentId).getAuthor()))){
			if(dockindKeys.get("processAction")!=null &&dockindKeys.get("processAction").equals("finish-task") ){
				
			}
			throw new EdmException("Paczka zosta豉 wys豉na - edycja zablokowana ");
			
		} else if (statusID == 40)
			return;
		if(statusID < 21){
		Map <String,Object> multipe = ((Map<String, Object>) dockindKeys.get(multipleValues));
		Map <String,Object> documents =  (Map<String, Object>) multipe.get(multipleDOCUMENTS+"_1");
		//documents
		/*if (documents==null ||documents.isEmpty())
			throw new EdmException("Paczka powinna posiadac przynajmniej jeden dokument");
		}*/
		StringBuilder sbInsert = new StringBuilder();
		StringBuilder sbRemove = new StringBuilder();
		List<Long> listIdDocumentInBase = new ArrayList();
		List<Long> curentDocumentsInDokind = new ArrayList();
		List<Long> doUsuniecia = new ArrayList();
		List<Long> doDodania = new ArrayList();
		String barcode = (String)dockindKeys.get(CN_KOD_KRESKOWY);
		// dokumenty z bazy 
		listIdDocumentInBase.addAll(getDokumentyWbazie(kind, documentId));
		curentDocumentsInDokind.addAll(getIdsDocumentFromDictionary(dockindKeys));

		if (listIdDocumentInBase.isEmpty())
		{
			doDodania.addAll(curentDocumentsInDokind);
		} else if (curentDocumentsInDokind.isEmpty())
		{
			doUsuniecia.addAll(listIdDocumentInBase);
		} else if (isAnyInvalidDocument(curentDocumentsInDokind, listIdDocumentInBase))
		{

			doUsuniecia.addAll(listIdDocumentInBase);
			doUsuniecia.removeAll(curentDocumentsInDokind);
			//usuniecie wszytkich id wybranych dokument闚 z kt鏎ych istniej� ju� wpisy w bazie
			curentDocumentsInDokind.removeAll(listIdDocumentInBase);
			doDodania.addAll(curentDocumentsInDokind);
			removeDuplicates(doUsuniecia);
			removeDuplicates(doDodania);
			
			//		w zaleznosci od tego czy usunieto dokument czy dodano do paczki wpisy do histori oraz flagi 

		}
		Collections.sort(doUsuniecia);
		Collections.sort(doDodania);
		dodajDokumenty(doDodania, sbInsert ,barcode ,documentId);
		usunDokumenty(doUsuniecia, sbRemove, barcode,documentId);

		if (!doDodania.isEmpty())
			addWpisHistDoDokuemntu(sbInsert.toString(), OfficeDocument.find(documentId));

		if (!doUsuniecia.isEmpty())
			addWpisHistDoDokuemntu(sbRemove.toString(), OfficeDocument.find(documentId));
	}
	}

	/**
	 * Metoda dodaje dokumenty do paczki/przesy趾i.
	 * 
	 * @param doDodania - lista id-k闚 dokument闚
	 * @param sbInsert - obj. buduj帷y odpowied� dla u篡tkownika
	 * @param barcode - kod kreskowy
	 * @param paczkaId - id paczki
	 * @throws UserNotFoundException
	 * @throws EdmException
	 */
	private void dodajDokumenty(List<Long> doDodania ,StringBuilder sbInsert,String barcode, Long paczkaId) throws UserNotFoundException, EdmException
	{
		String descriptionInsert = "Dokument dodano do paczki o kodzie kreskowym = " + barcode+".";
		
		sbInsert.append("Do paczki dodano dokumenty o ID : ");

		for (Long l : doDodania)
		{
			OfficeDocument doc = OfficeDocument.findOfficeDocument(l,false);
			doc.setInPackage(true);
			//doc.setSource("w paczce o id :"+paczkaId);
			addWpisHistDoDokuemntu(descriptionInsert, doc);
			sbInsert.append("," + l);
		}
		updateTaskSnapshots(doDodania, true);
	
	}

	/**
	 * Metoda dodaje zadanie do paczki i aktualizuje owe zadanie.
	 * 
	 * @param doDodania
	 * @param b
	 */
	private void updateTaskSnapshots(List<Long> doDodania, boolean b)
	{
		try
		{
			for (Long l : doDodania)
			{
				List<JBPMTaskSnapshot> tasks = JBPMTaskSnapshot.findByDocumentId(l);
				for (JBPMTaskSnapshot t : tasks)
				{
					t.setTaskInPackage(b);
					DSApi.context().session().save(t);
				}
			}

		} catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}


	}
	/**
	 * Metoda usuwa dokumenty z paczki/przesy趾i.
	 * 
	 * @param doUsuniecia - lista id-k闚 dokument闚
	 * @param sbRemove - obj. buduj帷y odpowied� dla u篡tkownika
	 * @param barcode - kod kreskowy
	 * @param paczkaId - id przesy趾i
	 * @throws UserNotFoundException
	 * @throws EdmException
	 */
	private void usunDokumenty(List<Long> doUsuniecia ,StringBuilder sbRemove,String barcode, Long paczkaId) throws UserNotFoundException, EdmException
	{
		String description = "Dokument usuni皻o z paczki o kodzie kreskowym = " + barcode +".";
		sbRemove.append("Z paczki usuni皻o dokumenty  o ID : ");

		for (Long l : doUsuniecia)
		{
			OfficeDocument doc = OfficeDocument.findOfficeDocument(l, false);
			doc.setInPackage(false);
			//doc.setSource("");
			addWpisHistDoDokuemntu(description, doc);
			sbRemove.append("," + l);
			DSApi.context().session().update(doc);
			
		}
		updateTaskSnapshots(doUsuniecia, false);
		
	}
	/**
	 * Metoda pobiera list� id-k闚 dokument闚 ze s這wnika
	 * 
	 * @param fieldValues
	 * @return
	 */
	private List<Long> getIdsDocumentFromDictionary(Map<String, ?> fieldValues)
	{
		List<Long> l = new ArrayList();
		HashMap<String, Object> slowniki = (HashMap<String, Object>) fieldValues.get(multipleValues);
		if (slowniki!=null){
		for(Map.Entry<String, Object> entry:slowniki.entrySet()){
			if (entry.getKey().contains(multipleDOCUMENTS)){
				Map<String, FieldData> selectedDocuments = (Map<String, FieldData>) slowniki.get(entry.getKey());
				if (selectedDocuments!=null && !selectedDocuments.get("ID").getStringData().isEmpty() )	
				l.add(selectedDocuments.get("ID").getLongData());	
			}
				
			}
		return l ;
		}
		return l ;
	}

	/**
	 * Metoda pobiera dokumenty z bazy.
	 * 
	 * @param kind
	 * @param documentId
	 * @return
	 */
	private List<Long> getDokumentyWbazie(DocumentKind kind, Long documentId)
	{
		List<Long> l = new ArrayList();

		String sql = "Select FIELD_VAL from " + kind.getMultipleTableName() + " where DOCUMENT_ID = ? and FIELD_CN = ? ";

		if (documentId != null)
		{
			PreparedStatement ps;
			try
			{
				ps = DSApi.context().prepareStatement(sql);

				ps.setLong(1, documentId);
				ps.setString(2, multipleDOCUMENTS);
				ResultSet rs = ps.executeQuery();
				while (rs.next())
				{
					l.add(rs.getLong(1));
				}
				ps.close();



			} catch (SQLException e)
			{
				log.error("", e);
			} catch (EdmException e)
			{
				log.error("", e);
			}

		}
		return l;
	}
	
	/**
	 * Metoda dodaje wpis do historii dokumentu
	 * 
	 * @param description
	 * @param doc
	 * @throws UserNotFoundException
	 * @throws EdmException
	 */
	private void addWpisHistDoDokuemntu(String description, OfficeDocument doc) throws UserNotFoundException, EdmException
	{
		Audit wpis = new Audit();
		wpis.setDescription(description);
		wpis.setCtime(new Date());
		wpis.setUsername(DSApi.context().getDSUser().getName());
		wpis.setProperty("giveAccept");
		wpis.setLparam("");
		wpis.setPriority(1);
		DataMartManager.addHistoryEntry(doc.getId(), wpis);
		
	}
	@Override
	public void onStartProcess(OfficeDocument document) throws EdmException
	{
		
		super.onStartProcess(document);
	}
	@Override
	public void onLoadData(FieldsManager fm) throws EdmException
	{
		boolean  correctRecipientOrSender = false;
		if (fm.getDocumentId()!=null && fm.getFieldValues()!=null 
				&& fm.getFieldValues().get(CN_EMAILODBIORCA)!=null)
			correctRecipientOrSender = isCorrectRecipientOrSender(fm,null);
			
		
		if (fm.getDocumentId()!=null && !isCorrectRecipientOrSender(fm,null)){
			fm.getField(multipleDOCUMENTS).setHidden(true);
			fm.getField(CN_DODATKOWE_METADANE).setHidden(true);
		}else {
			fm.getField(multipleDOCUMENTS).setHidden(false);
			fm.getField(CN_DODATKOWE_METADANE).setHidden(false);
		}	
	}

	/**
	 * Metoda sprawdza poprawno嗆 nadawcy/odbiorcy dokumentu
	 * 
	 * @param fm - menager p鏊 dokumentu
	 * @param doc - obj. dokumentu
	 * @return
	 * @throws EdmException
	 */
	private boolean isCorrectRecipientOrSender(FieldsManager fm, OfficeDocument doc) throws EdmException
	{
		if(doc==null && fm.getDocumentId()!=null)
			doc = OfficeDocument.findOfficeDocument(fm.getDocumentId(),false);
		
		if (DSApi.context().getDSUser().getName().contains(doc.getAuthor()))
			return true;

		if (fm.getFieldValues() != null && fm.getFieldValues().get(CN_PACZKA_NA_DZIAL) != null
				&& ((Boolean) fm.getFieldValues().get(CN_PACZKA_NA_DZIAL) == true))
		{
			DSDivision div = DSDivision.findById(fm.getEnumItem(CN_DZIAL).getId());
			if (DSApi.context().getDSUser().inDivisionByGuid(div.getGuid()))
				return true;
			
		//dla user jako odbiorca 
		} else
		{
			if(doc!=null){
				for(Recipient rec : doc.getRecipients()){
					DSUser odb = DSUser.findByFirstnameLastname(rec.getFirstname(), rec.getLastname());
					if (odb!=null &&  DSApi.context().getDSUser().getId().equals(odb.getId()))
						return true;
				}
			}
			
			/*if (fm.getFieldValues() != null && fm.getFieldValues().get(CN_EMAILODBIORCA) != null)
			{
				DSUser userOdb = DSUser.findByEmail((String) fm.getFieldValues().get(CN_EMAILODBIORCA));
				if (DSApi.context().getDSUser().getId().equals(userOdb.getId()))
				{
					return true;
				}
			}*/
			/*if (fm.getFieldValues() != null && fm.getFieldValues().get(CN_EMAILNADAWCA) != null)
			{
				DSUser useNadawca = DSUser.findByEmail((String) fm.getFieldValues().get(CN_EMAILNADAWCA));
				if (DSApi.context().getDSUser().getId().equals(useNadawca.getId()))
					return true;
			}*/
		}
		return false;
	}

	/**
	 * Metoda tworzy proces <code>Do Wiadomo軼i</code> dla paczki dokument闚.
	 * 
	 * @param fm
	 * @param packageDocumentId
	 * @param objectiveDescription
	 * @param odbiorcaODebral
	 * @throws DocumentNotFoundException
	 * @throws EdmException
	 */
	private void createDWProcessFromPackage(FieldsManager fm, Long packageDocumentId, String objectiveDescription, boolean odbiorcaODebral)
			throws DocumentNotFoundException, EdmException
	{

		DSUser[] users = null;
		DSUser odbiorca = null;
		OfficeDocument doc = OfficeDocument.find(packageDocumentId);

		Map<String, Object> params = Maps.newHashMap();
		params.put(Jbpm4WorkflowFactory.OBJECTIVE, "Do Wiadomo軼i -" + objectiveDescription);
		params.put(Jbpm4WorkflowFactory.DOCDESCRIPTION, "DW");
		params.put(Jbpm4Constants.KEY_PROCESS_NAME, Jbpm4Constants.READ_ONLY_PROCESS_NAME);

		if (fm.getFieldValues() != null && fm.getFieldValues().get(PgPackageLogic.CN_PACZKA_NA_DZIAL) != null
				&& ((Boolean) fm.getFieldValues().get(PgPackageLogic.CN_PACZKA_NA_DZIAL) == true))
		{

			DSDivision div = DSDivision.find(fm.getEnumItem(PgPackageLogic.CN_DZIAL).getId());
			users = div.getUsers(false);

			if (users != null && AvailabilityManager.isAvailable("packageDocumentDwToDivision"))
			{
				params.put(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM, div.getGuid());
				//params.put(Jbpm4Constants.READ_ONLY_ASSIGNMENT_GUIDS, div.getGuid());
				doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess((String) params.get(Jbpm4Constants.KEY_PROCESS_NAME)).getLogic()
						.startProcess(doc, params);

			}

			if (!DSUser.findByUsername(doc.getAuthor()).inDivision(div))
			{
				params.remove(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM);
				params.put(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY, doc.getAuthor());
				doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess((String) params.get(Jbpm4Constants.KEY_PROCESS_NAME)).getLogic()
						.startProcess(doc, params);
			}

		} else
		{
			List<String> userNames = new ArrayList();
			for(Recipient rec : doc.getRecipients()){
				odbiorca = DSUser.findByFirstnameLastname(rec.getFirstname(), rec.getLastname());
			}
			if (!odbiorcaODebral)
				userNames.add(odbiorca.getName());
			
				userNames.add(doc.getAuthor());

			for (String name : userNames)
			{
				params.put(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY, name);
				doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess((String) params.get(Jbpm4Constants.KEY_PROCESS_NAME)).getLogic()
						.startProcess(doc, params);
			}


		}
		TaskSnapshot.updateByDocumentId(packageDocumentId, "anyType");
	}
	/**
	 * Metoda wysy豉 maila.
	 * 
	 * @param fm
	 * @param docId
	 * @param description
	 * @throws ServiceNotFoundException
	 * @throws ServiceDriverNotSelectedException
	 * @throws EdmException
	 */
	private void sendDostarczycielMail(FieldsManager fm, Long docId, String description) throws ServiceNotFoundException, ServiceDriverNotSelectedException,
			EdmException
	{
		OfficeDocument doc = OfficeDocument.find(docId);

		String emailNadawcy = "";
		String emailOdbiorca = "";
		String[] toEmail = null;
		String[] emailCC = null;

		if (fm.getFieldValues() != null && fm.getFieldValues().get(PgPackageLogic.CN_PACZKA_NA_DZIAL) != null
				&& ((Boolean) fm.getFieldValues().get(PgPackageLogic.CN_PACZKA_NA_DZIAL) == true))
		{
			DSDivision div = DSDivision.findById(fm.getEnumItem(PgPackageLogic.CN_DZIAL).getId());
			DSUser[] users = div.getUsers(false);
			for (DSUser dsUser : users)
			{
				if (dsUser.getEmail() != null)
					emailOdbiorca += "," + dsUser.getEmail();
			}
		}
		DSUser nadawca = null;
		DSUser odbiorca = null;
		
		if(doc!=null){
			for(Recipient rec : doc.getRecipients()){
				odbiorca = DSUser.findByFirstnameLastname(rec.getFirstname(), rec.getLastname());
			}
			//nadawca = DSUser.findByFirstnameLastname(doc.getSender().getFirstname(), doc.getSender().getLastname());
			nadawca = DSUser.findByUsername(doc.getAuthor());	
		}
		
		if(nadawca.getEmail()!=null)
		emailNadawcy += "," + nadawca.getEmail();
		if (emailOdbiorca.isEmpty() && odbiorca!=null && odbiorca.getEmail()!=null && !odbiorca.getEmail().isEmpty())
			emailOdbiorca += "," + (String) fm.getValue(PgPackageLogic.CN_EMAILODBIORCA);

		StringBuilder emailDescription = new StringBuilder();
		StringBuilder emailTitle = new StringBuilder();
		emailTitle.append("Potwierdzenie dotycz帷e paczki");
		emailTitle.append(" o kodzie kreskowym : " + fm.getValue(PgPackageLogic.CN_KOD_KRESKOWY));
	
		emailDescription.append("Kurier:\n");
		emailDescription.append(description + "\n");
		emailDescription.append("Status Paczki:\n");
		emailDescription.append(fm.getEnumItem(PgPackageLogic.CN_STATUS_DOKUMENTU).getTitle() + "\n");
		emailDescription.append("Nadawca paczki:\n");
		emailDescription.append(nadawca.getFirstnameLastnameName() + "\n");
		emailDescription.append("Odbiorca paczki:\n");
		emailDescription.append(odbiorca != null ? odbiorca.getFirstnameLastnameName() : "Dzia� : "
				+ DSDivision.findById(fm.getEnumItem(PgPackageLogic.CN_DZIAL).getId()).getName() + "\n");
		emailDescription.append("Opis Paczki:\n");
		emailDescription.append(fm.getValue(PgPackageLogic.CN_DOC_DESCRIPTION));



		String[] emailNad = emailNadawcy.split(",");
		String[] emailOdb = emailOdbiorca.split(",");
		log.error("toemail  ="+toEmail);
		log.error("emailCC ="+emailCC);
		
		List<String> list = new ArrayList<String>(Arrays.asList(emailNad));
		if (!list.isEmpty())
		{
			list.remove(0);
			toEmail = list.toArray(new String[0]);
		}
		List<String> listCC = new ArrayList<String>(Arrays.asList(emailOdb));
		if (!listCC.isEmpty())
		{
			listCC.remove(0);
			emailCC = listCC.toArray(new String[0]);
		}
		if(!(emailCC.length>1))
			emailCC=null;
		if(toEmail.length>0)
		((Mailer) ServiceManager.getService(Mailer.NAME)).send(toEmail, emailCC, null, emailTitle.toString(), emailDescription.toString(), null);

	}
    /**
     * Metoda wywo造wana przy starcie procesu
     * 
     * @param document
     * @param event
     */
	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		
		log.info("--- PaaDocInLogic : START PROCESS !!! ---- {}", event);
		try {
			
			Map<String, Object> map = Maps.newHashMap();
			
			setLokalizacjaPaczki(document);
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM,event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			map.put(Jbpm4WorkflowFactory.OBJECTIVE, "W celu sprawdzenia poprawnych danych");
			

			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));


		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

	}
	/**
	 * Metoda ustawia lokalizacj� paczki
	 * 
	 * @param doc
	 * @throws UserNotFoundException
	 * @throws EdmException
	 */
	private void  setLokalizacjaPaczki(OfficeDocument doc) throws UserNotFoundException, EdmException
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Paczka znajduje si� u :");
		sb.append(DSApi.context().getDSUser().asFirstnameLastname());
		sb.append( " i jest przygotowana do wysy趾i");
		
		Map<String,Object> values = new HashMap<String, Object>();
		values.put(PgPackageLogic.CN_LOKALIZACJA_PACZKI, sb.toString());
		doc.getDocumentKind().setOnly(doc.getId(),values);
		DSApi.context().session().update(doc);
		
	}
	/**
	 * Metoda pobiera typ dokumentu.
	 * 
	 * @return DocumentKind - obj. typu dokumentu
	 * @throws EdmException
	 */
	 public static DocumentKind setKindFromRedirectAction() throws EdmException
		{
		 DocumentKind kind = null;
	    		
	    		if (Docusafe.getAdditionProperty("kind-package")!=null &&!Docusafe.getAdditionProperty("kind-package").isEmpty()){
	    			kind = DocumentKind.findByCn(Docusafe.getAdditionProperty("kind-package"));
	    		}
	    		return kind;
	    		
		}

	 /**
	  * Metoda sprawdza czy jest mo磧iwe przekierowanie paczki/przesy赧
	  * 
	  * @return true/false operacji
	  * @throws EdmException
	  */
	public static boolean isPackageRedirectAvailable() throws EdmException
	{
		if (AvailabilityManager.isAvailable("menu.left.repository.paczki.dokumentow") && ServletActionContext.getRequest().getParameter("package") != null
				&& !ServletActionContext.getRequest().getParameter("package").isEmpty()
				&& ServletActionContext.getRequest().getParameter("package").contains("true"))
		{
			return true;
		} else if (ServletActionContext.getRequest().getAttribute("package") != null && (Boolean) ServletActionContext.getRequest().getAttribute("package"))
		{
			//ServletActionContext.getRequest().removeAttribute("package");
			return true;
		} else
			return false;

	}

}
