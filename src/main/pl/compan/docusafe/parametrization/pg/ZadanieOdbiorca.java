package pl.compan.docusafe.parametrization.pg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.axis2.databinding.types.soapencoding.Array;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.primitives.Longs;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.xes.XesLog;

/**
 * <h1>ZadanieOdbiorca</h1>
 *
 * <p>
 *     Klasa modeluje odbiorce zadania.
 * </p>
 */
public class ZadanieOdbiorca
{

	public ZadanieOdbiorca()
	{
		
	}

	private Long id;
	private Long userId;
	private Long documentId;
	private String name;
	private String firstname;
	private String lastname;
	private String division;
	private String description;
	private Boolean showOnSelect;
	private Integer userStatus;
	private Date userRealizationDate;
	private Date userRealizationDateChanged;
	private Long delegateUserId;
	private String delegateMessage;
	
	
	
	
	
	
	public Long getId()
	{
		return id;
	}
	
	public void setId(Long id)
	{
		this.id = id;
	}
	
	public Long getUserId()
	{
		return userId;
	}
	
	public void setUserId(Long userId)
	{
		this.userId = userId;
	}
	
	public Long getDocumentId()
	{
		return documentId;
	}
	
	public void setDocumentId(Long documentId)
	{
		this.documentId = documentId;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getFirstname()
	{
		return firstname;
	}
	
	public void setFirstname(String firstname)
	{
		this.firstname = firstname;
	}
	
	public String getLastname()
	{
		return lastname;
	}
	
	public void setLastname(String lastname)
	{
		this.lastname = lastname;
	}
	
	public String getDivision()
	{
		return division;
	}
	
	public void setDivision(String division)
	{
		this.division = division;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public void setDescription(String description)
	{
		this.description = description;
	}
	
	public Boolean getShowOnSelect()
	{
		return showOnSelect;
	}
	
	public void setShowOnSelect(Boolean showOnSelect)
	{
		this.showOnSelect = showOnSelect;
	}
	
	public Integer getUserStatus()
	{
		return userStatus;
	}
	
	public void setUserStatus(Integer userStatus)
	{
		this.userStatus = userStatus;
	}
	
	public Date getUserRealizationDate()
	{
		return userRealizationDate;
	}
	
	public void setUserRealizationDate(Date userRealizationDate)
	{
		this.userRealizationDate = userRealizationDate;
	}
	
	/**
	 *  zwraca liste obiekt�w OdbiorcyZadania z przekazanej listy long�w  
	 * @param odbiorcy
	 * @return
	 * @throws EdmException
	 */
	public static List<ZadanieOdbiorca> getOdbiorcyFromIds(Long[] longs ) throws EdmException{
		Criteria criteria = DSApi.context().session().createCriteria(ZadanieOdbiorca.class);
		criteria.add(Restrictions.in("id", longs));
		criteria.addOrder(Order.desc("id"));
		return criteria.list();

	}

	/**
	 *  zwraca liste obiekt�w OdbiorcyZadania z przekazanego stringa 
	 * @param odbiorcy
	 * @return
	 * @throws EdmException
	 */
	public static List<ZadanieOdbiorca> getOdbiorcyFromIds(String odbiorcy) throws EdmException
	{
		if(odbiorcy==null || odbiorcy.isEmpty()){
			List<ZadanieOdbiorca> empty = new ArrayList<ZadanieOdbiorca>();
			return empty;
		}
		String[] ids = null;
		if(odbiorcy!=null && !odbiorcy.isEmpty())
		if (odbiorcy.startsWith("[")) {
			odbiorcy= odbiorcy.substring(1, odbiorcy.length() - 1);
			ids= odbiorcy.split(",");
		} else {
			ids= new String[1];
			ids[0]= odbiorcy;
		}
		 List<Long> longList = new ArrayList<Long>();
		for(String  s : ids){
			longList.add(Long.parseLong((s.trim())));
		}
		
		Criteria criteria = DSApi.context().session().createCriteria(ZadanieOdbiorca.class);
		criteria.add(Restrictions.in("id", longList));
		criteria.addOrder(Order.desc("id"));
		return criteria.list();
	}
	/**
	 * Metoda pobiera odbiorc�w <code>ZadanieOdbiorca</code> dla id-k�w
	 * 
	 * @param lsd
	 * @return Lista <code>ZadanieOdbiorca</code>
	 * @throws EdmException
	 */
	public static  List<ZadanieOdbiorca> getOdbiorcyFromIds(List<Long> lsd) throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(ZadanieOdbiorca.class);
		criteria.add(Restrictions.in("id", lsd));
		criteria.addOrder(Order.desc("id"));
		return criteria.list();
	}
	
	public Long getDelegateUserId()
	{
		return delegateUserId;
	}

	
	public void setDelegateUserId(Long delegateUserId)
	{
		this.delegateUserId = delegateUserId;
	}

	
	public Date getUserRealizationDateChanged()
	{
		return userRealizationDateChanged;
	}

	
	public void setUserRealizationDateChanged(Date userRealizationDateChanged)
	{
		this.userRealizationDateChanged = userRealizationDateChanged;
	}

	
	public String getDelegateMessage()
	{
		return delegateMessage;
	}

	
	public void setDelegateMessage(String delegateMessage)
	{
		this.delegateMessage = delegateMessage;
	}


	
	
}
