package pl.compan.docusafe.parametrization.pg.reports;


import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.parametrization.pg.BarcodeGenerator;
import pl.compan.docusafe.parametrization.pg.BarcodePool;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.CsvDumper;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.DateUtils;

import java.io.File;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: slim
 * Date: 03.10.13
 * Time: 13:48
 * To change this template use File | Settings | File Templates.
 */
public class BarcodeRangeReport extends Report{

    @Override
    public void doReport() throws Exception {

        try
        {
        log.error("REPORT PARAMS=" + params);
        log.error("ROBIE RAPORT!!!!!");

        Date dateFrom = null;
            Date dateTo = null;
            ArrayList<String> users=null;

            String dateFromStr = null;
            String dateToStr = null;

        for(ReportParameter p: params)
        {
            if(p.getFieldCn().equals("dateFrom")&&p.getValueAsString()!=null&&p.getValueAsString().trim().length()>9)
            {
               dateFromStr = p.getValueAsString();
               dateFrom= DateUtils.parseDateAnyFormat(p.getValueAsString());
            }
            else if(p.getFieldCn().equals("dateTo")&&p.getValueAsString()!=null&&p.getValueAsString().trim().length()>9)
            {
                dateToStr = p.getValueAsString();
                dateTo= DateUtils.parseDateAnyFormat(p.getValueAsString());
            }
            else if(p.getFieldCn().equals("user")&&p.getValue()!=null)
            {
                Object obj = p.getValue();
                if(p.getValue() instanceof String)
                {
                   users = new ArrayList<String>();
                   users.add(p.getValueAsString());
                }
                else
                {
                   users = new ArrayList<String>();
                   String temp[] = (String[]) p.getValue();
                   for(String s: temp) users.add(s);
                }
            }
        }


//        Criteria criteria = DSApi.context().session().createCriteria(BarcodePool.class);
//        if(dateFrom!=null) criteria.add(Restrictions.ge("creationDate", dateFrom));
//        if(dateTo!=null) criteria.add(Restrictions.le("creationDate", dateTo));
//        if(users!=null){
//            ArrayList<Criterion> kryteria = new ArrayList<Criterion>();
//            for(String userLogin: users)
//            {
//                UserImpl usImpl = (UserImpl) UserImpl.findByUsername(userLogin);
//                criteria.add(Restrictions.disjunction().add(Restrictions.eq("userId", usImpl.getId())));
//                criteria.add(Restrictions.or(criteria, Restrictions.eq("userId", usImpl.getId())));
//            }
//
//        }

//        List test = criteria.list();
        Statement st = DSApi.context().createStatement();
        String query = "select * from dsg_pg_barcode_pool ";
        if(dateFrom!=null)
        {

            if(query.contains("where"))
                query += " and creationdate>='"+DateUtils.formatSqlDate(dateFrom)+"'";
            else
                query += " where creationdate>='"+DateUtils.formatSqlDate(dateFrom)+"'";
        }
        if(dateTo!=null)
        {
            if(query.contains("where"))
                query += " and creationdate<='"+DateUtils.formatSqlDate(dateTo)+"'";
            else
                query += " where creationdate<='"+DateUtils.formatSqlDate(dateTo)+"'";
        }
        if(users!=null)
        {
            if(query.contains("where"))
            {
                query += " and (";
                for(int i=0; i<users.size(); i++)
                {
                    query+="user_id="+UserImpl.findByUsername(users.get(i)).getId();
                    if(i<users.size()-1) query+=" or ";
                }
                query += " ) ";
            }
            else
            {
                query += " where ";
                for(int i=0; i<users.size(); i++)
                {
                    query+="user_id="+users.get(i);
                    if(i<users.size()-1) query+=" or ";
                }
            }

        }

        UniversalTableDumper dumper = new UniversalTableDumper();

        CsvDumper csvDumper = new CsvDumper();
        File csvFile = new File(this.getDestination(), "report.csv");
        csvDumper.openFile(csvFile);
        dumper.addDumper(csvDumper);

        XlsPoiDumper poiDumper = new XlsPoiDumper();
        File xlsFile = new File(this.getDestination(), "report.xls");
        poiDumper.openFile(xlsFile);
        dumper.addDumper(poiDumper);

        ResultSet rs = st.executeQuery(query);

        dumper.newLine();

        dumper.addText("Kod pocz�tkowy");
        dumper.addText("Kod ko�cowy");
        dumper.addText("Data zam�wienia");
        dumper.addText("Login zamawiaj�cego");
        dumper.addText("Ilo�� zam�wionych kod�w");
        dumper.addText("Ilo�� wykorzystanych kod�w");
        dumper.dumpLine();

        while(rs.next())
        {
            String barcode1 = BarcodeGenerator.getBarcode(rs.getInt(3), rs.getString(7));
            String barcode2 = BarcodeGenerator.getBarcode(rs.getInt(4), rs.getString(7));
            UserImpl userTemp = (UserImpl) UserImpl.findById(rs.getLong(6));
            int amount = rs.getInt(4) - rs.getInt(3) + 1;

            dumper.newLine();
            dumper.addText(barcode1);
            dumper.addText(barcode2);
            dumper.addText(rs.getString(5));
            dumper.addText(userTemp.getName());
            dumper.addText(String.valueOf(amount));
            dumper.addText("0");
            dumper.dumpLine();
        }

        dumper.closeFileQuietly();
        }
        catch(Exception e)
        {
            log.error(e.getMessage(), e);
        }
    }


}
