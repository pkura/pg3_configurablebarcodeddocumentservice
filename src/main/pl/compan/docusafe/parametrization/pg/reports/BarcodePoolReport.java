package pl.compan.docusafe.parametrization.pg.reports;


import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.CsvDumper;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.File;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created with IntelliJ IDEA.
 * User: slim
 * Date: 30.09.13
 * Time: 14:39
 * To change this template use File | Settings | File Templates.
 */
public class BarcodePoolReport extends Report

{

    private static Logger log = LoggerFactory.getLogger(BarcodePoolReport.class);
    @Override
    public void doReport() throws Exception
    {
        log.error("REPORT PARAMS=" + params);
        log.error("ROBIE RAPORT!!!!!");

        Statement st = DSApi.context().createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM dsg_pg_barcode_generator");
        String used = "";
        String left = "";

        int limit = Integer.parseInt(Docusafe.getAdditionProperty("barcode.limit_value"));
        int usedInt = 0;
        while(rs.next())
        {
            usedInt = rs.getInt(1);
        }
        rs.close();
        st.close();
        int leftInt = limit-usedInt;

        UniversalTableDumper dumper = new UniversalTableDumper();

        CsvDumper csvDumper = new CsvDumper();
        File csvFile = new File(this.getDestination(), "report.csv");
        csvDumper.openFile(csvFile);
        dumper.addDumper(csvDumper);

        XlsPoiDumper poiDumper = new XlsPoiDumper();
        File xlsFile = new File(this.getDestination(), "report.xls");
        poiDumper.openFile(xlsFile);
        dumper.addDumper(poiDumper);

        dumper.newLine();
        dumper.addText("Wykorzystano");
        dumper.addText("Pozosta�o");
        dumper.dumpLine();

        dumper.newLine();

        dumper.addInteger(usedInt);
        dumper.addInteger(leftInt);
        dumper.dumpLine();

        dumper.closeFileQuietly();
    }
}
