package pl.compan.docusafe.parametrization.pg;


import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.OBJECTIVE;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.dbutils.DbUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.google.common.collect.Maps;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *    
 * @author Grzegorz Filip
 * serwis odpowiedzialny za   wzsyukanie oray utworzenie czklicznego zadania
 */
public class ZadaniaCykliczneService extends ServiceDriver
{
	
	public static final Logger log = LoggerFactory.getLogger(ZadaniaCykliczneService.class);
//	private static Queue<XesLog> xesEntries = new LinkedList<XesLog>();
	private Map<Long, Integer> mapDocIdTCyclicPeriod = new HashMap<Long, Integer>();
	private List<OfficeDocument> docsToCreate = new ArrayList<OfficeDocument>();
	private Map<String, Object> valuesFromDoc = new HashMap<String, Object>();
	private Map<String, Object> newValues = new HashMap<String, Object>();
	private final String  sql = "select DOCUMENT_ID,CYKLICZNE,CYKLICZNE_PERIOD from DS_PG_INT_ZADANIE_DOC  where CYKLICZNE is not null and CYKLICZNE = ?";
	
	
	/**
	 * 
	 * Klasa uruchomiaj�ca us�ug�
	 *
	 */
	public class ZadaniaCykliczneTimer extends TimerTask
	{

		@Override
		public void run()
		{

			try
			{


				DSApi.openAdmin();
				DSApi.context().begin();


				prepareResulsts();
				Date currentDate = new Date();
				docsToCreate = new ArrayList<OfficeDocument>();
				prepareDocumentToCreate(currentDate);

				for (OfficeDocument doc : docsToCreate)
				{
					try
					{
						valuesFromDoc = new HashMap<String, Object>();
						newValues = new HashMap<String, Object>();
						OutOfficeDocument newDoc = new OutOfficeDocument();
						valuesFromDoc = doc.getFieldsManager().getFieldValues();
						prepareNewValues(doc, valuesFromDoc, newValues);
						createCopyDocument(doc, newDoc, newValues);
						Map <String,Object> updatevalues = new HashMap<String, Object>();
						updatevalues.put("CYKLICZNE", false);
						updatevalues.put("CYKLICZNE_PERIOD", null);
						doc.getDocumentKind().setOnly(doc.getId(), updatevalues);
						DSApi.context().session().save(doc);
					} catch (Exception e)
					{
						log.error("", e);
						continue;

					}
				}
				DSApi.context().commit();
				DSApi.close();
			} catch (EdmException e)
			{
				log.error("", e);
			} catch (Exception e)
			{
				log.error("", e);


			}
		}

	}

	/**
	 * Metoda ustawia mape id's dokument�w z cyklicznymi okresami.
	 * 
	 * @code mapDocIdTCyclicPeriod
	 * @throws Exception
	 */
	private void prepareResulsts() throws Exception 
	{
		mapDocIdTCyclicPeriod = new HashMap<Long, Integer>();
		PreparedStatement pst = null;
		ResultSet rs = null;
		try
		{
			pst = DSApi.context().prepareStatement(sql);
		
		pst.setBoolean(1, true);
		
		 rs = pst.executeQuery();
		
		while (rs.next()){
			mapDocIdTCyclicPeriod.put(rs.getLong(1), rs.getInt(2));
		}
		rs.close();
		pst.close();
		} catch (SQLException e)
		{
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(pst);
			throw new Exception(e);
		} catch (EdmException e)
		{
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(pst);
			throw new Exception(e);
		}
	}
	/**
	 * Metoda ustawia list� <code>docsToCreate</code> dokument�w do stworzenia po pewnym okresie czasu.
	 * 
	 * @code docsToCreate
	 * @param currentDate - aktualna data
	 * @throws Exception
	 */
	private void prepareDocumentToCreate(Date currentDate) throws Exception {
		try
		{
		for(Long keLong :mapDocIdTCyclicPeriod.keySet()){
			OfficeDocument doc;
			
				doc = OfficeDocument.findOfficeDocument(keLong, false);
				Date dateToCreate = org.apache.commons.lang.time.DateUtils.addDays(doc.getCtime(), mapDocIdTCyclicPeriod.get(keLong));
				
			
			if(org.apache.commons.lang.time.DateUtils.isSameDay(dateToCreate, currentDate))
				docsToCreate.add(doc);
			else if (AvailabilityManager.isAvailable("testCyklicznych"))
				docsToCreate.add(doc);
		}
		} catch (DocumentNotFoundException e)
		{
			log.error("", e);
			throw new Exception(e);
		} catch (EdmException e)
		{
			log.error("", e);
			throw new Exception(e);
		}
	}

	/**
	 * Metoda ustawia nowe warto�ci(o ile s�) na zadaniu.
	 * 
	 * @param oldDoc - dokument
	 * @param oldDocValues - warto�ci dokumentu
	 * @param newDocValues - nowe warto�ci dokumentu
	 * @throws EdmException
	 */
	public void prepareNewValues(OfficeDocument oldDoc, Map<String, Object> oldDocValues, Map<String, Object> newDocValues) throws EdmException
	{
		List<ZadanieOdbiorca> listaOdbiiorcow = ZadanieOdbiorca.getOdbiorcyFromIds(oldDoc.getFieldsManager().getStringKey("RECIPIENT"));
		List<Long> odbortyids =new ArrayList<Long>() ;
		List<String> newIdsodborcy =new ArrayList<String>();
		if(!listaOdbiiorcow.isEmpty()){
		for (ZadanieOdbiorca odb : listaOdbiiorcow)
		{
			odbortyids.add(odb.getUserId());
	
		}	
		Criteria crit = DSApi.context().session().createCriteria(ZadanieOdbiorca.class);
		crit.add(Restrictions.in("userId", odbortyids));
		crit.add(Restrictions.eq("showOnSelect", true));
		crit.addOrder(Order.desc("id"));
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<ZadanieOdbiorca>  odbiorcysToAdd = crit.list();
		
		for(ZadanieOdbiorca odb :odbiorcysToAdd){
			newIdsodborcy.add(createOdbiorca(odb));
		}
		removeDuplicates(newIdsodborcy);
		}
		newDocValues.put("RECIPIENT", newIdsodborcy.toArray(new String[newIdsodborcy.size()]));
		newDocValues.put("CYKLICZNE_PERIOD", oldDocValues.get("CYKLICZNE_PERIOD"));
		newDocValues.put("CYKLICZNE", oldDocValues.get("CYKLICZNE"));
		newDocValues.put("STATUS", 18);
		newDocValues.put("CYKLICZNE_PERIOD", oldDocValues.get("CYKLICZNE_PERIOD"));
	}
	
	/**
	 * Metoda tworzy odbiorce zadania
	 * 
	 * @param odb - obiekt odbiorcy zadania
	 * @return string id odbiorcy
	 * @throws HibernateException
	 * @throws EdmException
	 */
	private String createOdbiorca(ZadanieOdbiorca odb) throws HibernateException, EdmException
	{
		ZadanieOdbiorca newOdbm = new ZadanieOdbiorca();
		
		newOdbm.setDivision(odb.getDivision());
		newOdbm.setUserId(odb.getUserId());
		newOdbm.setFirstname(odb.getFirstname());
		newOdbm.setLastname(odb.getLastname());
		newOdbm.setUserStatus(0);
		newOdbm.setName((odb.getName()!=null?odb.getName() : ""));
		newOdbm.setShowOnSelect(false);
		
		DSApi.context().session().save(newOdbm);
		return newOdbm.getId().toString();
		
	}
	/**
	 * Metoda usuwa duplikaty z listy odbiorc�w.
	 * 
	 * @param recipientsToRemove
	 */
	private static void removeDuplicates(List<String> recipientsToRemove) {
        HashSet set = new HashSet(recipientsToRemove);
        recipientsToRemove.clear();
        recipientsToRemove.addAll(set);
}
	/**
	 * Tworzy wpis w s�owniku pism wychod��cy nadaj�c numer Ko  pismu 
	 * 
	 * @param newOutDocument 
	 * @throws EdmException 
	 */
	private static void assignOfficeDocumentId(OutOfficeDocument newDoc ) throws EdmException
	{
		
		Date entryDate = new Date();
		Integer sequenceId = Journal.TX_newEntry2(Journal.getMainInternal().getId(), newDoc.getId(), entryDate);
		newDoc.bindToJournal(Journal.getMainInternal().getId(), sequenceId);
		
	}
	/**
	 * Metoda tworzy kopi� dokumentu
	 * 
	 * @param doc
	 * @param newDoc
	 * @param valuesToAdd
	 * @throws AccessDeniedException
	 * @throws EdmException
	 */
	public void createCopyDocument(OfficeDocument doc, OutOfficeDocument newDoc, Map<String, Object> valuesToAdd) throws AccessDeniedException, EdmException
	{
		Date cdtate = new Date();
		newDoc.setDescription(doc.getDescription());
		newDoc.setAbstractDescription(doc.getAbstractDescription());
		newDoc.setAuthor(doc.getAuthor());
		newDoc.setTitle(doc.getTitle());
		newDoc.setDocumentKind(doc.getDocumentKind());
		newDoc.setAutorDivision(doc.getAutorDivision());
		newDoc.setBarcode(doc.getBarcode());
		newDoc.setCreatingUser(doc.getCreatingUser());
		newDoc.setCountryLetter(doc.getCountryLetter());
		newDoc.setCzyAktualny(doc.getCzyAktualny());
		newDoc.setCzyCzystopis(doc.getCzyCzystopis());
		newDoc.setCurrentAssignmentUsername(doc.getAuthor());
		newDoc.setCurrentAssignmentAccepted(false);
		newDoc.setDocumentDate(cdtate);
		newDoc.setFolder(doc.getFolder());
		newDoc.setInPackage(false);
		newDoc.setInternal(true);
		newDoc.setIsArchived(false);
	
		newDoc.setPermissionsOn(doc.isPermissionsOn());
		newDoc.setForceArchivePermissions(doc.isForceArchivePermissions());
		 DSDivision[] div = DSUser.findByUsername(doc.getAuthor()).getOriginalDivisions();
		 String guid = "rootdivision";
		 for (DSDivision dsDivision : div)
		{
			 if(dsDivision.isDivision()){
				 guid = dsDivision.getGuid();
				newDoc.setAssignedDivision(guid);
				 break;
				 
			 }
		}
	
		newDoc.setDivisionGuid(guid);
		newDoc.setSummary(doc.getSummary());
		newDoc.create();
		
		newDoc.getDocumentKind().saveDictionaryValues(newDoc, valuesToAdd);
		newDoc.getDocumentKind().saveMultiDictionaryValues(newDoc, valuesToAdd);
		newDoc.getDocumentKind().set(newDoc.getId(), valuesToAdd);
		
		
		newDoc.getDocumentKind().logic().documentPermissions(newDoc);
		
		
		// tworzenie i uruchamianie wewn�trznego procesu workflow
	
		assignOfficeDocumentId(newDoc);
		createProces(newDoc);
		TaskSnapshot.updateAllTasksByDocumentId(newDoc.getId(), ((OfficeDocument) newDoc).getStringType());
		DSApi.context().session().saveOrUpdate(newDoc);
		
		
		
	
		
		
	}
	/**
	 * Metoda tworzy proces dla dokumentu
	 * 
	 * @param newDoc - nowy dokument
	 * @throws EdmException
	 */
	private void createProces(OutOfficeDocument newDoc) throws EdmException
	{
		 Map<String, Object> map = Maps.newHashMap();
         
             map.put(ASSIGN_USER_PARAM, newDoc.getAuthor());
             newDoc.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(newDoc, map);
		
	}
	private Timer timer;
	

	/**
	 * Metoda startuje service
	 */
	@Override
	protected void start() throws ServiceException
	{
		timer = new Timer(true);
		int delay = 30;
		int period = 2;
		period = Integer.parseInt(Docusafe.getAdditionPropertyOrDefault("zadanieCykliczneCoIleMinut", "2"));
		timer.schedule(new ZadaniaCykliczneTimer(), delay * DateUtils.SECOND, period  * DateUtils.MINUTE );
		log.info("Xes lOG :" + period+ " s.");
	}

	/**
	 * Metoda zatrzymuje service
	 */
	@Override
	protected void stop() throws ServiceException
	{
		if (timer != null) {
			timer.cancel();
		}
		
	}

	@Override
	protected boolean canStop()
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	}
	
