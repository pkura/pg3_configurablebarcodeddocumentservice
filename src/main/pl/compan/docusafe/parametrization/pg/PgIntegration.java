package pl.compan.docusafe.parametrization.pg;

import org.apache.commons.net.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
@Service
public class PgIntegration {


    private final static Logger log = LoggerFactory.getLogger(PgIntegration.class);

    protected final RestTemplate restTemplate;

    private static final String REST_NEW_SIMPLE_BARCODE_PATH = "barcode/simple/new";
    private static final String REST_NEW_EXTENDED_BARCODE_PATH = "/barcode/extended";
    private static final String UPDATE_EXTENDED_BARCODE_PATH = "/barcode/extended/update";

    @Autowired
    public PgIntegration(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String getNewSimpleBarcode() throws Exception {

        String applicationUrl = AdditionManager.getProperty("app.parametrization.pg.url");

        if(applicationUrl == null) {
            throw new Exception("[getNewSimpleBarcode] applicationUrl not set");
        }

        String url = applicationUrl + REST_NEW_SIMPLE_BARCODE_PATH;
        BarcodeResponse barcodeResponse = restTemplate.postForObject(url, null, BarcodeResponse.class);

        return barcodeResponse.barcode;
    }

    public String getRestNewExtendedBarcode(String externalName, String version) throws Exception {
        String applicationUrl = AdditionManager.getProperty("app.parametrization.pg.url");

        if(applicationUrl == null) {
            throw new Exception("[getRestNewExtendedBarcode] applicationUrl not set");
        }

        String url = applicationUrl + REST_NEW_EXTENDED_BARCODE_PATH + "/" + externalName + "/" + version;
        BarcodeResponse barcodeResponse = restTemplate.postForObject(url, null, BarcodeResponse.class);

        return barcodeResponse.barcode;

    }
    public String getRestNewExtendedBarcode64(String externalName, String version,boolean encoded) throws Exception {
        String applicationUrl = AdditionManager.getProperty("app.parametrization.pg.url");
        String externalName64 ="";
        if(!encoded)
        	externalName64 = Base64.encodeBase64String(externalName.getBytes());
        else 
        	externalName64 = externalName;
        if(applicationUrl == null) {
            throw new Exception("[getRestNewExtendedBarcode] applicationUrl not set");
        }
      //  /barcode/extended/{externalName64}/{version}/{encoded}
        String url = applicationUrl + REST_NEW_EXTENDED_BARCODE_PATH + "/" + externalName64 + "/" + version+"/"+"true";
        BarcodeResponse barcodeResponse = restTemplate.postForObject(url, null, BarcodeResponse.class);

        return barcodeResponse.barcode;

    }

    public String getUpdatedExtendedBarcode(String barcodeBase64, String externalName, String version) throws Exception {
        String applicationUrl = AdditionManager.getProperty("app.parametrization.pg.url");

        if(applicationUrl == null) {
            throw new Exception("[getUpdatedExtendedBarcode] applicationUrl not set");
        }

        String url = applicationUrl + UPDATE_EXTENDED_BARCODE_PATH + "/" + barcodeBase64 + "/" + externalName + "/" + version;
        BarcodeResponse barcodeResponse = restTemplate.postForObject(url, null, BarcodeResponse.class);

        return barcodeResponse.barcode;

    }

    public boolean isAvailable() {
        return AvailabilityManager.isAvailable("app.parametrization.pg.enabled");
    }

    public static class BarcodeResponse {
        public String barcode;
    }

	public String getRestNewExtendedBarcode(String string, String string2,
			boolean b) {
		// TODO Auto-generated method stub
		return null;
	}

}
