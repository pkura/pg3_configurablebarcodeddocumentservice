package pl.compan.docusafe.parametrization.pg;

import java.util.Map;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentPermission;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.permission.SearchPermmisionFactory;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;


public class PgSearchPermissionFactory extends DocumentKindsManager implements SearchPermmisionFactory
{ 
	  private final static Logger log = LoggerFactory.getLogger(PgSearchPermissionFactory.class);

	  /**
	   * Metoda tworzy zaptytanie do DB z upraenienia dla dokument�w in/out
	   * 
	   */
	@Override
	public void getPerrmissionsQuery(Map<String, TableAlias> tables, FromClause from, WhereClause where, DockindQuery query, DocumentKind documentKind,TableAlias perm)
	{
		TableAlias docTable = null;
		String table = "";
		try
		{
			if (!DSApi.context().isAdmin()
					&& !(DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_PODGLAD)
							|| DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_MODYFIKACJE) || DSApi.context().hasPermission(
							DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD)))
			{

				if (query.getDocumentKind().getDockindInfo().getDocumentTypes().contains(DocumentType.INCOMING))
				{
					table = "dso_in_document";
					docTable = tables.get(table);
					
					permissionInDocuments(where, docTable,documentKind,from,tables, perm);
				} else
				{
					table = "dso_out_document";
					docTable = tables.get(table);
					permissionOutDocuments(where, docTable,documentKind,from,tables,perm);
				}
			}
		} catch (Exception e)
		{
			log.error("", e);
		}
	}

	private static void permissionOutDocuments(WhereClause where, TableAlias docTable, DocumentKind documentKind, FromClause from, Map<String, TableAlias> tables,TableAlias perm) throws DivisionNotFoundException, EdmException
	{
		DSUser user = DSApi.context().getDSUser();
		Junction OR = Expression.disjunction().
                add(Expression.eq(perm.attribute("subjecttype"), ObjectPermission.ANY));
		fromPermissionTable(OR,tables,documentKind,perm);

		if (DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_PODGLAD) || DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_MODYFIKACJE))
		{
			DSDivision[] divisions = user.getDivisions();
			if (divisions.length > 0)
			{
				for (int i = 0; i < divisions.length; i++)
				{
					OR.add(Expression.eq(docTable.attribute("divisionguid"), divisions[i].getGuid()));
					if (divisions[i].isPosition())
					{
						DSDivision parent = divisions[i].getParent();
						if (parent != null)
							OR.add(Expression.eq(docTable.attribute("divisionguid"), parent.getGuid()));
					}
					if (DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_PODGLAD_W_PODWYDZIALACH)){
						
						DSDivision[] children_POSITION = divisions[i].getChildren(DSDivision.TYPE_POSITION);
						if (children_POSITION.length > 0)
						{
							for (int j = 0; j < children_POSITION.length; j++)
							{
								OR.add(Expression.eq(docTable.attribute("divisionguid"), children_POSITION[j].getGuid()));
							}
						}
						DSDivision[] children_POSITION2 = divisions[i].getChildren(DSDivision.TYPE_DIVISION);
						if (children_POSITION2.length > 0)
						{
							for (int j = 0; j < children_POSITION2.length; j++)
							{
								OR.add(Expression.eq(docTable.attribute("divisionguid"), children_POSITION2[j].getGuid()));
							}
						}
					}
					}
				}
				OR.add(Expression.eq(docTable.attribute("clerk"), user.getName()));
				
				
				DocumentLogic documentLogic = documentKind.logic();
				
				where.add(OR);
		}
		if (DSApi.context().hasPermission(DSPermission.PISMO_KO_PODGLAD))
		{
			OR.add(Expression.eq(docTable.attribute("divisionguid"), DSDivision.ROOT_GUID));
		}

		OR.add(Expression.eq(docTable.attribute("clerk"), user.getName()));
		where.add(OR);


	}




	private static void permissionInDocuments(WhereClause where, TableAlias docTable, DocumentKind documentKind, FromClause from, Map<String, TableAlias> tables,TableAlias perm) throws  DivisionNotFoundException, EdmException
	{

		DSUser user = DSApi.context().getDSUser();

		Junction OR = Expression.disjunction().
                add(Expression.eq(perm.attribute("subjecttype"), ObjectPermission.ANY));

		fromPermissionTable(OR,tables,documentKind,perm);
		
		if (DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_PODGLAD) || DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_MODYFIKACJE))
		{
			DSDivision[] divisions = user.getDivisions();
			if (divisions.length > 0)
			{

				for (int i = 0; i < divisions.length; i++)
				{
					if (!divisions[i].isGroup())
					{

						OR.add(Expression.eq(docTable.attribute("divisionguid"), divisions[i].getGuid()));
						if (divisions[i].isPosition())
						{
							DSDivision parent = divisions[i].getParent();
							if (parent != null)
								OR.add(Expression.eq(docTable.attribute("divisionguid"), parent.getGuid()));
						}
					if (DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_PODGLAD_W_PODWYDZIALACH)){
						DSDivision[] children_POSITION = divisions[i].getChildren(DSDivision.TYPE_POSITION);
						if (children_POSITION.length > 0)
						{
							for (int j = 0; j < children_POSITION.length; j++)
							{
								OR.add(Expression.eq(docTable.attribute("divisionguid"), children_POSITION[j].getGuid()));
							}
						}
						DSDivision[] children_POSITION2 = divisions[i].getChildren(DSDivision.TYPE_DIVISION);
						if (children_POSITION2.length > 0)
						{
							for (int j = 0; j < children_POSITION2.length; j++)
							{
								OR.add(Expression.eq(docTable.attribute("divisionguid"), children_POSITION2[j].getGuid()));
							}
						}
					}
				}
				
				//  OR.add(Expression.eq(docTable.attribute("clerk"), user.getName()));
			}


		}
		}
		if (DSApi.context().hasPermission(DSPermission.PISMO_KO_PODGLAD))
		{
			OR.add(Expression.eq(docTable.attribute("divisionguid"), DSDivision.ROOT_GUID));
		}

		OR.add(Expression.eq(docTable.attribute("clerk"), user.getName()));

						
		where.add(OR);
		
	}


	private static void fromPermissionTable( Junction oR, Map<String, TableAlias> tables, DocumentKind documentKind, TableAlias perm)
	{
		DocumentLogic documentLogic = documentKind.logic();

		if (documentLogic.isPersonalRightsAllowed())
		{
			oR.add(Expression.conjunction().add(Expression.eq(perm.attribute("subjecttype"), ObjectPermission.USER))
					.add(Expression.eq(perm.attribute("subject"), DSApi.context().getPrincipalName())));
		}

	}
	@Override
	public boolean canReadXXX(Document document) throws EdmException
	{
		return true;
		/*if (document.getId() == null)
			return true;

		if (DSApi.context().isAdmin())
			return true;

		if (AvailabilityManager.isAvailable("search.withoutPermissionTable"))
			return true;

		return DSApi.context().documentPermission(document.getId(), ObjectPermission.READ);*/
		//return DSApi.context().canRead(this);

	}

	/**
	 * Metoda sprawdza czy u�ytkownik ma wgl�d do dokumentu.
	 * 
	 * @param officeDoc
	 * @return
	 * @throws EdmException
	 */
	public static boolean canReadDocument(OfficeDocument officeDoc) throws EdmException
	{

		String guidDocuments = officeDoc.getAssignedDivision();

		if (!DSApi.context().isAdmin()
				&& !(DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_PODGLAD)
						|| DSApi.context().hasPermission(DSPermission.PISMO_WSZYSTKIE_MODYFIKACJE) || DSApi.context().hasPermission(
						DSPermission.DZIENNIK_WSZYSTKIE_PODGLAD)))
		{

			DSUser user = DSApi.context().getDSUser();

			if (DSApi.context().hasPermission(DSPermission.PISMO_KO_PODGLAD))
			{
				if (DSDivision.ROOT_GUID.contains(guidDocuments))
					return true;

			}
			if (officeDoc.getClerk()!=null && officeDoc.getClerk().contains(DSApi.context().getDSUser().getName()))
				return true;


			if (DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_PODGLAD) || DSApi.context().hasPermission(DSPermission.PISMO_KOMORKA_MODYFIKACJE))
			{
				DSDivision[] divisions = user.getDivisions();
				if (divisions.length > 0)
				{

					for (int i = 0; i < divisions.length; i++)
					{


						if (!divisions[i].isGroup() && divisions[i].getGuid().contains(guidDocuments))
							return true;

						if (divisions[i].isDivision() || divisions[i].isPosition())
						{
							DSDivision parent = divisions[i].getParent();
							if (parent.getGuid().contains(guidDocuments))
								return true;

						}

						DSDivision[] children_POSITION = divisions[i].getChildren(DSDivision.TYPE_POSITION);
						if (children_POSITION.length > 0)
						{
							for (int j = 0; j < children_POSITION.length; j++)
							{
								if (children_POSITION[j].getGuid().contains(guidDocuments))
									return true;
							}
						}
						DSDivision[] children_POSITION2 = divisions[i].getChildren(DSDivision.TYPE_DIVISION);
						if (children_POSITION2.length > 0)
						{
							for (int j = 0; j < children_POSITION2.length; j++)
							{

								if (children_POSITION2[j].getGuid().contains(guidDocuments))
									return true;
							}
						}
					}
				}
			}


			return false;

		}
		return true;
	}
}
