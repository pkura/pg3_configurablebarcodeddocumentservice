package pl.compan.docusafe.parametrization.pg;

import com.google.common.collect.Lists;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.permission.NewPermissionManager;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.base.permission.rule.*;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.List;

/**
 * Manager specjalnie przystosowany do uprawnie� zale�nych od danego dokumentu.
 * Definuje jakie uprawnienia maj� byc sprawdzane podczas wchodzenia na dokumentu.
 *
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public class PgPermissionManager extends NewPermissionManager {

    private final static Logger log = LoggerFactory.getLogger(PgPermissionManager.class);


    /**
     * Definiuje kolejno�� sprawdzanych uprawnie�.
     * @return
     * @throws EdmException
     */
    @Override
    public List<PermissionRule> getPermissionRules() throws EdmException {
        List<PermissionRule> permissionRules = Lists.newLinkedList();


        permissionRules.add(new IsAdminRule());
        permissionRules.add(new IsAuthorRule());
        permissionRules.add(new IsClerkRule());
        permissionRules.add(new OnTasklistRule());
        permissionRules.add(new OnWatchRule());
        permissionRules.add(new OnDivisionRule());
        permissionRules.add(new HasDocumentPermissionRule());
        permissionRules.add(new OfficePermissionRule());
        permissionRules.add(new IsDockindPackage());
        permissionRules.add(new IsDocumentInPackage());

        log.trace("PgPermission {}", permissionRules.size());
        return permissionRules;
    }
    @Override
    public  PgSearchPermissionFactory getSearchPermisionFactory()
	{
    	//PgSearchPermissionFactory spf  = new PgSearchPermissionFactory();
    	return new PgSearchPermissionFactory();
    	
	}

}
