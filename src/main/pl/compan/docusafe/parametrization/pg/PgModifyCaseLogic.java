package pl.compan.docusafe.parametrization.pg;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.Maps;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.office.common.DwrDocumentHelper;
import pl.compan.docusafe.webwork.event.ActionEvent;

/**
 * <h1>PgModifyCaseLogic</h1>
 *
 * <p>
 *     Klasa s�u�y do obs�ugi zachowania/implementaji funkcjonalno�ci dokumentu modyfikuj�cego spraw�.
 * </p>
 */
public class PgModifyCaseLogic extends AbstractPgLogic
{

	private static PgModifyCaseLogic instance;
	protected static Logger log = LoggerFactory.getLogger(PgModifyCaseLogic.class);
	protected static DwrDocumentHelper dwrDocumentHelper = new DwrDocumentHelper();
	private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);

	public static PgModifyCaseLogic getInstance()
	{
		if (instance == null)
			instance = new PgModifyCaseLogic();
		return instance;
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		log.info("type: {}", type);
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("TYPE", type);
		values.put(_dataDokumentu, new Date());
		values.put("STATUS", 1);


		fm.reloadValues(values);
	}


	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk�y- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk�y - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP,
				"Dokumenty zwyk�y zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk�y - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}


	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
	{
		Field msg = null;
		Date startDate = new Date();
		startDate.setMinutes(1);
		startDate.setHours(0);
		Date planingDate = null;
		FieldData planingDateField = containsField(values, "PLANINGENDTIME");
		FieldData CaseStatusField = containsField(values, "PLANINGENDTIME");
		if (planingDateField != null && planingDateField.getDateData() != null)
		{
			planingDate = planingDateField.getDateData();
			if (planingDateField.getDateData().before(startDate))
			{

				msg = new Field("messageField", sm.getString("dataXXXNieMozeBycDataZPrzeszlosci", ": Planowana data zako�czenia sprawy "), Field.Type.BOOLEAN);
				values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
				return msg;
			}
		}
		
		return msg;
	}

	@Override
	public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
	{
		Date startDate = new Date();
		startDate.setMinutes(1);
		startDate.setHours(0);

		System.out.println(kind.getCn());
		System.out.println(values);

		if (values.get("DSDIVISION") != null)
		{
			Integer divId = (Integer) values.get("DSDIVISION");
			DSDivision div = DSDivision.find(divId);
			DSUser[] users = div.getUsers(false);
			if (users.length < 1)
				throw new EdmException("W dziale " + div.getName() + " niema pracownika");

		}
		if (values.get("PLANINGENDTIME") != null && (Date) values.get("PLANINGENDTIME") != null && ((Date) values.get("PLANINGENDTIME")).before(startDate))
		{
			throw new EdmException(sm.getString("dataXXXNieMozeBycDataZPrzeszlosci", ": Planowana data zako�czenia sprawy"));

		}
		//if (values.get("CASES") == null)
		//	throw new EdmException("Nie wybrano Sprawy ");
	}

	/**
	 * Metoda wywo�ywana w trakcie wykonania akcji zwi�zanych z archiwizacj� dokumentu.
	 * Wi��e dokument z akcjami archiwalnymi
	 * 
	 * @param document - obj. dokumentu
	 * @param type - typ dokumentu (archiwalny,przychodz�cy,wychodz�cy,wewn�trzny)
	 * @param as - obj. wykonuj�cy akcje archiwalne
	 * 
	 */
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		as.useFolderResolver(document);
		as.useAvailableProviders(document);
		log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
	}

    /**
     * Metoda wywo�ywana przy starcie procesu
     * 
     * @param document
     * @param event
     */
	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{

		super.onStartProcess(document, event);
		DSApi.context().watch(URN.create(document));
	}

	@Override
	public void onStartProcess(OfficeDocument document) throws EdmException
	{
		try
		{
			Map<String, Object> map = Maps.newHashMap();

			map.put(ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
			map.put(ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM, DSDivision.ROOT_GUID);
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			DSApi.context().watch(URN.create(document));

		} catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

}
