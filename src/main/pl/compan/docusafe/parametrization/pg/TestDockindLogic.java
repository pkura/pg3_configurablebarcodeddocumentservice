package pl.compan.docusafe.parametrization.pg;

import com.google.common.collect.Maps;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.permission.PermissionPolicy;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

/**
 * User: Tomasz
 * Date: 16.04.14
 * Time: 12:33
 */
public class TestDockindLogic extends AbstractDocumentLogic {

    private static TestDockindLogic instance;
    protected static Logger LOG = LoggerFactory.getLogger(TestDockindLogic.class);

    public static synchronized TestDockindLogic getInstance() {
        if (instance == null)
            instance = new TestDockindLogic();
        return instance;
    }

    public void onStartProcess(OfficeDocument document,ActionEvent event)
    {
        try {
            Map<String, Object> map = Maps.newHashMap();
            map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {}

    @Override
    public void documentPermissions(Document document) throws EdmException {

        FieldsManager fm = document.getFieldsManager();
        if(fm.getField("TAK_NIE").getEnumItems().size() > 2) {
            int i = (Integer) fm.getKey("TAK_NIE");
            String username = fm.getField("TAK_NIE").getEnumItem(i).getArg(1);
            Set<PermissionBean> params = new HashSet<PermissionBean>();
            if(username != null && username.length() > 0) {
                params.add(new PermissionBean(ObjectPermission.READ,username,ObjectPermission.USER,username));
                params.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,username,ObjectPermission.USER,username));
                params.add(new PermissionBean(ObjectPermission.MODIFY,username,ObjectPermission.USER,username));
                params.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,username,ObjectPermission.USER,username));
                params.add(new PermissionBean(ObjectPermission.DELETE,username,ObjectPermission.USER,username));
            }
            this.setUpPermission(document,params);
        }
    }

    @Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        if (DwrUtils.isNotNull(values, "DWR_WALUTA", "DWR_KWOTA_PREV") && values.get("DWR_WALUTA").isSender()) {
            String s = values.get("DWR_WALUTA").getEnumValuesData().getSelectedId();
            String arg = fm.getField("WALUTA").getEnumItem(Integer.valueOf(s)).getArg(1);
            int m = Integer.valueOf(arg);
            BigDecimal kwotaPrev = values.get("DWR_KWOTA_PREV").getMoneyData();
            BigDecimal kwotaAfter = kwotaPrev.multiply(BigDecimal.valueOf(m));
            values.get("DWR_KWOTA_AFTER").setMoneyData(kwotaAfter);
        }

        if (DwrUtils.isNotNull(values, "DWR_POLE_KOD") && values.get("DWR_POLE_KOD").isSender()) {
            String zip = values.get("DWR_POLE_KOD").getStringData();
            if(zip.length() > 4) {
                if(zip.contains("-")) {
                    zip = zip.replaceAll("-","");
                }
                LoggerFactory.getLogger("tomekl").debug("zip {}",zip);
                values.get("DWR_POLE_KOD").setStringData(zip.substring(0, 2)+"-"+zip.substring(2, 5));
            }
        }

        return null;
    }

    @Override
    public PermissionPolicy getPermissionPolicy() {
        return super.getPermissionPolicy();
    }

    @Override
    public int getPermissionPolicyMode() {
        return super.getPermissionPolicyMode();
    }
}
