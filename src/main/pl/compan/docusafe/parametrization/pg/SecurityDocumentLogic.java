package pl.compan.docusafe.parametrization.pg;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.*;
import pl.compan.docusafe.core.base.permission.PermissionPolicy;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jackrabbit.JackrabbitAttachmentPrivileges;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.SQLException;
import java.util.*;

/**
 * User: Tomasz
 * Date: 06.02.14
 * Time: 10:13
 */
public class SecurityDocumentLogic extends AbstractDocumentLogic {

    public final static String USER_FIELD_CN = "USER_FIELD";
    private final static Logger log = LoggerFactory.getLogger(SecurityDocumentLogic.class);

    private static SecurityDocumentLogic instance;
    public static SecurityDocumentLogic getInstance()
    {
        if (instance == null)
            instance = new SecurityDocumentLogic();
        return instance;
    }

	/**
	 * Metoda wywoływana w trakcie wykonania akcji związanych z archiwizacją dokumentu.
	 * Wiąże dokument z akcjami archiwalnymi
	 * 
	 * @param document - obj. dokumentu
	 * @param type - typ dokumentu (archiwalny,przychodzący,wychodzący,wewnętrzny)
	 * @param as - obj. wykonujący akcje archiwalne
	 * 
	 */
    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
        document.setFolder(folder);
        document.setTitle(document.getDocumentKind().getName() + " " + document.getId());
        document.setDescription(document.getDocumentKind().getName() + " " + document.getId());
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {
        FieldsManager fm = document.getFieldsManager();
        String username = (String) fm.getValue(USER_FIELD_CN);
        Set<PermissionBean> params = new HashSet<PermissionBean>();
        params.add(new PermissionBean(ObjectPermission.READ,username,ObjectPermission.USER,username));
        params.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,username,ObjectPermission.USER,username));
        params.add(new PermissionBean(ObjectPermission.MODIFY,username,ObjectPermission.USER,username));
        params.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,username,ObjectPermission.USER,username));
        params.add(new PermissionBean(ObjectPermission.DELETE,username,ObjectPermission.USER,username));
        this.setUpPermission(document,params);
        this.setJcrPermissions(document, fm, username);
    }

    /**
     * Metoda ustawia uprawnienia z jackrabitta
     * 
     * @param document
     * @param fm
     * @param username
     * @throws EdmException
     */
    public void setJcrPermissions(Document document, FieldsManager fm, String username) throws EdmException {
        log.debug("[setJcrPermissions] username = {}", username);

        Field field = fm.getField(USER_FIELD_CN);

        for(EnumItem enumItem: field.getEnumItems()) {
            String usernameToDelete = enumItem.getCn();

            if(username.equals(enumItem.getTitle())) {
                continue;
            }

            log.debug("[setJcrPermissions] usernameToDelete = {}", usernameToDelete);
            for(Attachment attachment: document.getAttachments()) {
                for(AttachmentRevision revision: attachment.getRevisions()) {
                    try {
                        JackrabbitAttachmentPrivileges.instance().revokeReadPrivilege(revision.getId(), usernameToDelete);
                    } catch(Exception e) {
                        log.error("[setJcrPermissions] error revId = {}, usernameToDelete = {}", revision.getId(), usernameToDelete);
                    }
                }
            }
        }

        // create usernames

        final DSUser user = UserImpl.findByUsername(username);

        final String externalName = user.getExternalName();

        if(externalName == null) {
            throw new EdmException("Użytkownik "+username+" nie ma przypisanego użytkownika JackRabbit");
        }

        log.debug("[setJcrPermissions] username = {}, externalName = {}", username, externalName);

        for(Attachment attachment: document.getAttachments()) {
            for(AttachmentRevision revision: attachment.getRevisions()) {
                Long revId = revision.getId();
                log.debug("[setJcrPermissions] clear and add privileges revId = {}", revId);
                try {
                    JackrabbitAttachmentPrivileges.instance().addReadPrivilege(revId, externalName);
                } catch(Exception e) {
                    log.error("[setJcrPermissions] error, revId = {}", revId, e);
                }
            }
        }
    }

    public List<String> getExternalNames(List<String> usernames) throws EdmException, SQLException {
        final Criteria criteria = DSApi.context().session().createCriteria(UserImpl.class);
        criteria.add(Restrictions.in("name", usernames));
        return criteria.list();
    }

    @Override
    public PermissionPolicy getPermissionPolicy() {
        return super.getPermissionPolicy();
    }

    @Override
    public int getPermissionPolicyMode() {
        return super.getPermissionPolicyMode();
    }
}
