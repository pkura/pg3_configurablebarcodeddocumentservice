package pl.compan.docusafe.parametrization.ams;

import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.enums.EnumDataSourceLogic;
import pl.compan.docusafe.core.dockinds.field.enums.EnumSourceException;
import pl.compan.docusafe.core.dockinds.field.enums.ExternalSourceEnumItem;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.util.axis.AxisClientUtils;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;
import pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Created by kk on 27.02.14.
 */
public class UnitBudgetKoKindEnumSource implements EnumDataSourceLogic {

    private static final Logger log = LoggerFactory.getLogger(UnitBudgetKoKindEnumSource.class);


    @Override
    public List<EnumItem> getEnumItemsFromSource(String refFieldId) throws EnumSourceException {
        final List<EnumItem> enumItems = Lists.newLinkedList();
        if (Strings.isNullOrEmpty(refFieldId)) {
            return enumItems;
        }

        final Long parsedRefFieldId;
        try {
            parsedRefFieldId = Long.parseLong(refFieldId);
        } catch (NumberFormatException e) {
            log.error(e.getMessage(), e);
            return enumItems;
        }

        try {
            long start = System.currentTimeMillis();
            AxisClientConfigurator axisClientConfigurator = new AxisClientConfigurator();
            DictionaryServiceStub stub = new DictionaryServiceStub(axisClientConfigurator.getAxisConfigurationContext());
            axisClientConfigurator.setUpHttpParameters(stub, "/services/DictionaryService");
            axisClientConfigurator.setHttpClient(AxisClientUtils.createHttpClient(30, 30));
            DictionaryServiceStub.GetUnitBudgetKoKindByParentID params = new DictionaryServiceStub.GetUnitBudgetKoKindByParentID();
            params.setId(parsedRefFieldId);
            params.setPage(1);
            params.setPageSize(1000);
            DictionaryServiceStub.UnitBudgetKoKind[] wsItems = stub.getUnitBudgetKoKindByParentID(params).get_return();
            if (wsItems != null) {
                for (DictionaryServiceStub.UnitBudgetKoKind wsItem : wsItems) {
                    if (wsItem.getP_koszt() - wsItem.getR_koszt() - wsItem.getRez_koszt() > 0) {
                        enumItems.add(ExternalSourceEnumItem.makeEnumItem("" + (long) wsItem.getId() + ExternalSourceEnumItem.SPLIT_CHAR + (long) wsItem.getId() + ExternalSourceEnumItem.SPLIT_CHAR + makeEnumTitle(wsItem)));
                    }
                }
            }
            log.error("UnitBudgetKoKindEnumSource for {} total {}" + parsedRefFieldId, (System.currentTimeMillis()-start));
        } catch (Exception e) {
            throw new EnumSourceException(e.getMessage(), e);
        }

        return enumItems;
    }

    private String makeEnumTitle(DictionaryServiceStub.UnitBudgetKoKind wsItem) {
        StringBuilder sb = new StringBuilder();
        return sb.append(StringUtils.left(wsItem.getWytwor_idm().trim()+": "+wsItem.getNazwa().trim().replaceAll("\"",""), 200))
                .append(" (plan-wykonanie: ")
                .append(BigDecimal.valueOf(wsItem.getP_koszt()-wsItem.getR_koszt()).setScale(2, RoundingMode.HALF_UP))
                .append("; rezerwacje: ")
                .append(BigDecimal.valueOf(wsItem.getRez_koszt()).setScale(2, RoundingMode.HALF_UP))
                .append(")")
                .toString();
    }

    @Override
    public List<EnumItem> getEnumItemsFromSource() throws EnumSourceException {
        return null;
    }
}
