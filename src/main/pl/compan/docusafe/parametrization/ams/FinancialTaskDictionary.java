package pl.compan.docusafe.parametrization.ams;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.commons.lang.ObjectUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.CacheableSourceEnumField;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.ExternalSourceEnumField;
import pl.compan.docusafe.core.dockinds.field.enums.ExternalSourceEnumItem;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: kk
 * Date: 09.02.14
 * Time: 19:31
 */
public class FinancialTaskDictionary extends DwrDictionaryBase {

    protected static final Logger log = LoggerFactory.getLogger(FinancialTaskDictionary.class);


    @Override
    public Map<String, Object> getValues(String id, Map<String, Object> fieldValues) throws EdmException {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

        Map<String, Object> fieldsValues = new HashMap<String, Object>();
        if (fieldValues != null)
            fieldsValues.putAll(fieldValues);
        try
        {
            Map<String, FieldData> values = new HashMap<String, FieldData>();
            if (id != null && !"".equals(id))
            {
                FieldData idData = new FieldData();
                if (id.contains(";u:") || id.contains(";d:")){
                    idData.setType(Field.Type.STRING);
                    idData.setStringData(id);
                } else {
                    idData.setType(Field.Type.INTEGER);
                    idData.setData(Integer.parseInt(id));
                }
                values.put("id", idData);

                result = search(values, 1, 2, false, null); // szukanie w DB
            }

            for (pl.compan.docusafe.core.dockinds.field.Field field : getOldField().getFields())
            {
                String fCn = field.getCn();
                // dla multiple ucina _(id wpisu) z CN
                if (isMultiple())
                {
                    fCn = fCn.substring(0, fCn.lastIndexOf("_"));
                }


                if (fCn.startsWith("ZADANIE_FINANSOWE_")) {
                    if (result.isEmpty()) {
                        result.add(Maps.<String,Object>newHashMap());
                    }

                    String preparedFieldCn = getOldField().getCn() + "_" + fCn;
                    EnumValues enumValues;
                    String rawResult = ObjectUtils.toString(result.get(0).get(preparedFieldCn));
                    if (!rawResult.isEmpty()) {
                        enumValues = new EnumValues(ImmutableMap.of("", "-wybierz-", rawResult, ExternalSourceEnumItem.makeEnumItem(rawResult).getTitle()), ImmutableList.of(rawResult));
                    } else {
                        enumValues = ZamowienieLogic.EMPTY_ENUM_VALUE;
                    }

                    result.get(0).put(preparedFieldCn, enumValues);
                }

               // result.get(0).put(fCn, ((DataBaseEnumField)f).getEnumItemsForDwr(fieldsValues,id));

                /*
                if (field.getType().equals(Field.Type.ENUM_AUTOCOMPLETE) || field.getType().equals(Field.Type.ENUM) || field.getType().equals(Field.Type.ENUM_MULTIPLE))
                {
                    for (pl.compan.docusafe.core.dockinds.field.Field f : (getOldField().getFields()))
                    {

                        if (fCn.toUpperCase().replaceFirst(getName().toUpperCase() + "_", "").toUpperCase().equals(f.getColumn().contains("_1") ? f.getColumn().replaceFirst("_1", "").toUpperCase() : f.getColumn().toUpperCase()))
                        {
                            Map<String, Object> enumVal = new HashMap<String, Object>();
                            if (result.size() > 0 && result.get(0).containsKey(fCn))
                            {
                                fieldsValues.put(f.getCn(), result.get(0).get(fCn));
                            }
                            if (result.size() == 0)
                            {
                                if (f instanceof DataBaseEnumField) {
                                    enumVal.put(fCn, ((DataBaseEnumField)f).getEnumItemsForDwr(fieldsValues,id));
                                } else {
                                    enumVal.put(fCn, f.getEnumItemsForDwr(fieldsValues));
                                }
                                result.add(enumVal);
                            }
                            else
                            {
                                if (f instanceof DataBaseEnumField) {
                                    result.get(0).put(fCn, ((DataBaseEnumField)f).getEnumItemsForDwr(fieldsValues,id));
                                } else {
                                    result.get(0).put(fCn, f.getEnumItemsForDwr(fieldsValues));
                                }
                            }
                        }
                    }
                    */

            }
            if (result.size() == 0)
                return new HashMap<String, Object>();
            return result.get(0);
        }
        catch (EdmException e)
        {
            log.error(e.getMessage(), e);
            return Collections.EMPTY_MAP;
        }
    }
}
