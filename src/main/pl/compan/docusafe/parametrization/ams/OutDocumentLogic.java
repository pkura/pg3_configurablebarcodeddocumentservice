package pl.compan.docusafe.parametrization.ams;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.EmailConstants;
import org.directwebremoting.WebContextFactory;
import org.jbpm.api.activity.ActivityExecution;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.Field.Kind;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.HtmlEditorValue;
import pl.compan.docusafe.core.dockinds.dwr.attachments.DwrAttachmentStorage;
import pl.compan.docusafe.core.dockinds.dwr.attachments.DwrFileItem;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.FieldLogic;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.mail.DSEmailChannel;
import pl.compan.docusafe.core.mail.MailMessage;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.workflow.TaskListUtils;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.sql.UserAdditionalData;
import pl.compan.docusafe.parametrization.ilpoldwr.SentResponseHtmlEditorFieldLogic;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.service.upload.UploadDriver;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

public class OutDocumentLogic extends AbstractDocumentLogic {
	
	private final static Logger LOG = LoggerFactory.getLogger(OutDocumentLogic.class);
	private final static StringManager sm = StringManager.getManager(OutDocumentLogic.class.getPackage().getName());
	
	
	private static final long serialVersionUID = 1L;
	private static final String PRZYCHODZACE = "normal";
	private static final String WYCHODZACE = "normal_out";
	private static final String WEWNETRZNE = "normal_int";
	
	private final static String DWR_SEND_BUTTON = "DWR_SEND_BUTTON";
	private final static String DWR_SEND_FORWARD_BUTTON = "DWR_SEND_FORWARD_BUTTON";
	private final static String DWR_GENERATE_KO = "DWR_GENERATE_KO";
	private final static String DWR_RESPONSE_BUTTON = "DWR_RESPONSE_BUTTON";
	private final static String DWR_FORWARD_RESPONSE_BUTTON = "DWR_FORWARD_RESPONSE_BUTTON";
	private final static String SEND_BUTTON = "SEND_BUTTON";
	private final static String RESPONSE_BUTTON = "RESPONSE_BUTTON";
	private final static String RESPONSE_CONTENT = "RESPONSE_CONTENT";
	
	private final static String VALIDATE_RECIPIENT = "Pole Odbiorca jest wymagane";
	
	
	private static final String ORG_EMAIL_ATTACHMENT = "ORG_EMAIL_ATTACHMENT";
    public static final String DWR_ORG_EMAIL_ATTACHMENT = "DWR_ORG_EMAIL_ATTACHMENT";
    private String ORIGINAL_MSG = "<br /><br />- - - Oryginalna wiadomo�� - - -";
	private String forwardTo;
	private String channelUserName;
	
	private static NormalLogic instance;
	protected static Logger log = LoggerFactory.getLogger(NormalLogic.class);

	public static NormalLogic getInstance() {
		if (instance == null)
			instance = new NormalLogic();
		return instance;
	}
	
	public static final String DWR_ZPO = "DWR_ZPO";
    public static final String DWR_DOC_DELIVERY = "DWR_DOC_DELIVERY";
    public static final String DWR_DOC_MAIL_ZONE = "DWR_DOC_MAIL_ZONE";
    public static final String DWR_DOC_MAIL_SIZE = "DWR_DOC_MAIL_SIZE";
    public static final String DWR_DOC_MAIL_WEIGHT = "DWR_DOC_MAIL_WEIGHT";
    public static final String DWR_KOSZT_PRZESYLKI = "DWR_KOSZT_PRZESYLKI";
    public static final String DOC_MAIL_ZONE = "DOC_MAIL_ZONE";
	
	public final static int DELIVERY_TYPE_EMAIL = 700;
    public final static int DELIVERY_TYPE_MANUAL = 710;
    public final static int DELIVERY_TYPE_FAX = 720;
    public final static int DELIVERY_TYPE_IMPORT = 730;
    public final static int DELIVERY_TYPE_DOCUMENT_IN = 740;
	
	static enum DeliveryType {
        EMAIL(DELIVERY_TYPE_EMAIL), MANUAL(DELIVERY_TYPE_MANUAL), FAX(DELIVERY_TYPE_FAX), IMPORT(DELIVERY_TYPE_IMPORT), DOCUMENT_IN(DELIVERY_TYPE_DOCUMENT_IN);

        private int value;

        private DeliveryType(int value) {
            this.value = value;
        }

        int getValue() {
            return value;
        }

        static DeliveryType fromValue(int value) {
            for(DeliveryType type : DeliveryType.values()) {
                if(type.getValue() == value) {
                    return type;
                }
            }

            return null;
        }
    };
    private DeliveryType deliveryType;

	public void onStartProcess(OfficeDocument document, ActionEvent event) {
		log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
		try{
			Map<String, Object> map = Maps.newHashMap();
			if(WYCHODZACE.equals(document.getDocumentKind().getCn())){
				/*map.put(ASSIGN_USER_PARAM, null);
				FieldsManager fm = document.getFieldsManager();
				String userName = fm.getEnumItemCn("KANCELARIA");
				map.put(ASSIGN_USER_PARAM, userName);*/
				map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
	            map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			}
			else{
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
            map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			}
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
            log.error(AvailabilityManager.isAvailable("addToWatch").toString());
            if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
	}

	private void bindToJournalOutDocument(OutOfficeDocument doc, Date entryDate) throws EdmException{
		    Journal journal = Journal.getMainOutgoing();
	        Long journalId = journal.getId();
	        DSApi.context().session().flush();
	        Integer sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), entryDate);
	        doc.bindToJournal(journalId, sequenceId);
	        doc.setOfficeDate(entryDate);
	}

	
	@Override
	public void documentPermissions(Document document) throws EdmException {
	}


    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        as.useFolderResolver(document);
        as.useAvailableProviders(document);
        log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());

        if (document != null && document.getDocumentKind().getCn().equals("normal_out"))
        {
            OfficeCase officeCase = ((OfficeDocument) document).getContainingCase();
            if (officeCase != null)
            {
                PreparedStatement ps = null;
                try {
                    ps = DSApi.context().prepareStatement("update dsg_normal_dockind set NR_DOKUMENTU = ? where document_id = ?");
                    ps.setString(1, officeCase.getOfficeId());
                    ps.setLong(2, document.getId());
                    ps.executeUpdate();
                    ps.close();

                    DSApi.context().closeStatement(ps);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                } finally {
                    DSApi.context().closeStatement(ps);
                }
            }
        }
    }

    private List<DwrFileItem> getStoredDWRAttachments(){
        return DwrAttachmentStorage.getInstance().getFiles(WebContextFactory.get().getSession(false), "EMAIL_ATTACHMENT", ORG_EMAIL_ATTACHMENT);
    }
    
    private List<File> prepareAttachments(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
		List<DwrFileItem> mailAttachments = getStoredDWRAttachments();
		File tempFile;
		List<File> attachedFiles = new ArrayList<File>();
		try {
			for(FileItem fileItem : mailAttachments){
				tempFile = new File(UploadDriver.tempDir + "" + fileItem.getSize() + "_ORGNAME_" + fileItem.getName());
				if(tempFile.exists()){
					fileItem.write(tempFile);
					attachedFiles.add(tempFile);
				}else if(tempFile.createNewFile()){
					fileItem.write(tempFile);
					attachedFiles.add(tempFile);
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return null;
		}

		return attachedFiles;
	}
    
    /**
	 * Metoda weryfikuje poprawno�� ustawie� kana�u e-mail na podstawie powi�zanej z nim wiadomo�ci
	 * i odpowiednio modyfikuje komunikat wy�wietlany u�ytkownikowi w przypadku wykrycia b��du.
	 * @param msg wiadomo�� wy�wietlana u�ytkownikowi
	 * @param messageId id wiadomo�c email s�u��ce do identyfikacji kana�u email
	 * @throws EdmException
	 */
	private boolean verifyEmailChannel(Field msg, Long messageId) throws EdmException{
		MailMessage mail = MailMessage.find(messageId);
		DSEmailChannel channel = mail.getEmailChannel();

		boolean isValid = false;
		if(channel == null){
			msg.setLabel("Nie znaleziono w systemie kana�u email " + " ( " + mail.getFrom() + " ) " + ", z kt�rego pobrano wiadomo��");
		//	jesli kanal email, z ktorego zostal pobrany mail, jest aktualnie aktywny
		}else if(channel.isEnabled()){
			//	jesli kanal email, z ktorego zostal pobrany mail, ma aktywn� poczt� wychodzac�
			if(channel.isSmtpEnabled()){
				return true;
			}else{
				msg.setLabel("Kana� email " + channel.getKolejka() + " ( " + channel.getUser() +" ) " + "nie ma aktywnej konfiguracji SMTP");
			}
		}else{
			msg.setLabel("Kana� email " + channel.getKolejka() + " ( " + channel.getUser() +" ) " + "nie jest aktywny");
		}
		return isValid;
	}
    
	private DSEmailChannel getValidEmailChannel() throws EdmException{
		List<DSEmailChannel> emailChannelList = DSEmailChannel.list();

		for(DSEmailChannel channel : emailChannelList){
			if(channel.isEnabled() && channel.isSmtpEnabled()){
				return channel;
			}
		}
		return null;
	}
	
    private Field sendMail(String actionButtonType, Map<String, FieldData> values, FieldsManager fm) throws EntityNotFoundException, EdmException{

		StringBuilder sb = new StringBuilder("Wys�ano wiadomo�� do ");
		String subject;
		String toEmail;
		Field msg = new Field("cn", sb.toString(), Field.Type.STRING);
		msg.setKind(Kind.MESSAGE);
		boolean contextOpened = false;
		String messageReceipt = null;
		Long messageId;
        DSEmailChannel emailChannel;
		// tworzymy tymczasowe pliki z zwartoscia zalacznikow dodanych z poziomu formatki 
		List<File> attachedFiles = prepareAttachments(values, fm);
		if(actionButtonType.equals(DWR_SEND_FORWARD_BUTTON)){

			FieldData receiverData = values.get("DWR_RECIVER");
			Map<String, FieldData> receiverMap = (Map<String, FieldData>)receiverData.getData();
			//sb.append(receiverMap.get("RECEIVER_LASTNAME") + " ").append(receiverMap.get("RECEIVER_FIRSTNAME") + " ");
			boolean atLeastOneEmail = false;
			for(String key : receiverMap.keySet()){
				if(key.contains("RECIVER_EMAIL")){
					messageReceipt = DwrUtils.getStringValue(values, "DWR_RESPONSE_CONTENT");
					toEmail = receiverMap.get(key).getStringData();
					if(!toEmail.isEmpty()){
						atLeastOneEmail = true;
						subject =  values.get("DWR_SUBJECT_EMAIL").getStringData();
						try {
							InternetAddress addr = new InternetAddress(toEmail);
							sb.append(toEmail + " ");
							toEmail = addr.getAddress();
						} catch(AddressException e) {
							LOG.error(e.getMessage(), e);
						}
						try {
							contextOpened = DSApi.openContextIfNeeded();
							// jak zapisany dokument
							if(fm.getDocumentId() != null){
								if(deliveryType == DeliveryType.EMAIL) {
                                    messageId = ((InOfficeDocument)InOfficeDocument.find(fm.getDocumentId())).getMessageId();
                                    emailChannel = MailMessage.find(messageId).getEmailChannel();
									if(verifyEmailChannel(msg, messageId)){
										//((Mailer) ServiceManager.getService(Mailer.NAME)).send(toEmail, toEmail, subject, messageReceipt, att);
										//(Mailer) ServiceManager.getService(Mailer.NAME)).send(toEmail, toEmail, subject, messageReceipt);
	                                    //send(toEmail, toEmail, subject, messageReceipt, true, EmailConstants.TEXT_HTML, emailChannel, attachedFiles);  
										((Mailer) ServiceManager.getService(Mailer.NAME)).send(toEmail, toEmail, subject, messageReceipt, true, EmailConstants.TEXT_HTML, emailChannel, attachedFiles);
									}
								} else {
                                    emailChannel = getValidEmailChannel();
                                    //((Mailer) ServiceManager.getService(Mailer.NAME)).send(toEmail, toEmail, subject, messageReceipt, att);
                                    //((Mailer) ServiceManager.getService(Mailer.NAME)).send(toEmail, toEmail, subject, messageReceipt);
                                    //send(toEmail, toEmail, subject, messageReceipt, true, EmailConstants.TEXT_HTML, emailChannel, attachedFiles);  
                                    ((Mailer) ServiceManager.getService(Mailer.NAME)).send(toEmail, toEmail, subject, messageReceipt, true, EmailConstants.TEXT_HTML, emailChannel, attachedFiles);
								}
							}else{ // nie ma dokumentu
                                emailChannel = getValidEmailChannel();
								if(emailChannel != null){
									//((Mailer) ServiceManager.getService(Mailer.NAME)).send(toEmail, toEmail, subject, messageReceipt, att);
                                    ((Mailer) ServiceManager.getService(Mailer.NAME)).send(toEmail, toEmail, subject, messageReceipt, true, EmailConstants.TEXT_HTML, emailChannel, attachedFiles);
                                    //send(toEmail, toEmail, subject, messageReceipt, true, EmailConstants.TEXT_HTML, emailChannel, attachedFiles);  
								}else{
									msg.setLabel("Nie znaleziono w systemie �adnego aktywnego kana�u email");
								}
							}
							DSApi.closeContextIfNeeded(contextOpened);
						} catch (EdmException e) {
							msg.setLabel(e.getMessage());
							LOG.error(e.getMessage(), e);
							DSApi.closeContextIfNeeded(contextOpened);
						}
					}
				}
			}
			
		}
		removeTemporaryFiles(attachedFiles);
		msg = new Field("cn", sb.toString(), Field.Type.STRING);
		msg.setKind(Kind.MESSAGE);
		return msg;
	}
    
    private void setResponseContent(Map<String, FieldData> values, String responseContent){
		FieldData resp = values.get("DWR_RESPONSE_CONTENT");
		HtmlEditorValue val = new HtmlEditorValue(responseContent);
		resp.setHtmlEditorData(val);
	}
    
    private void removeTemporaryFiles(List<File> files){
		for(File file : files){
			file.delete();
		}
        DwrAttachmentStorage.getInstance().removeAll(WebContextFactory.get().getSession(), null);
	}
    
    private String getLastQuotedMessageDate(Map<String, FieldData> values){
//		String DATE_PATTERN = "\n'Dnia' dd - MM - yyyy 'o godzinie' HH:mm:ss 'napisano:'\n";
		String DATE_PATTERN = "\nEEEE, d MMMM yyyy, HH:mm:ss\n";
		SimpleDateFormat format = new SimpleDateFormat(DATE_PATTERN);
		String date = "";
		if(StringUtils.isEmpty(DwrUtils.getStringValue(values, "DWR_SENT_RESP"))) {
			date = format.format(values.get("DWR_SENTDATE").getDateData());
		}else{
			date = format.format(Calendar.getInstance().getTime());
		}
		return date;
	}
    
    private String prepareQuotedMessage(Map<String, FieldData> values, FieldsManager fm, String clickedButtonType) throws DocumentNotFoundException, EdmException{

		String date = "";
		String header = "";
		String content = "";
		String quotedMessage = "";

		if(deliveryType == DeliveryType.EMAIL && clickedButtonType.equals(DWR_RESPONSE_BUTTON)){
			date = getLastQuotedMessageDate(values);
			header = prepareQuoteHeader(values, fm, date, clickedButtonType);
		}else if(clickedButtonType.equals(DWR_FORWARD_RESPONSE_BUTTON)){
//			content = "<br /><br />- - - Wiadomo�� przes�ana dalej - - -";
			content = ORIGINAL_MSG;
		}

		content += getLastQuotedMessageContent(values, fm);
		quotedMessage = header + content;

		return getUserMailFooter() + quotedMessage;
	}
    
    private String getChannelUserName(FieldsManager fm) throws EdmException{
		if(channelUserName == null || channelUserName.equals("")){
			boolean contextOpened = DSApi.openContextIfNeeded();
			Long messageId = ((InOfficeDocument)InOfficeDocument.find(fm.getDocumentId())).getMessageId();
            if(messageId != null){
                MailMessage mail = MailMessage.find(messageId);
                DSEmailChannel channel = mail.getEmailChannel();
                DSApi.closeContextIfNeeded(contextOpened);
                return channelUserName = channel.getUser();
            }else{
                channelUserName = "";
            }
		}
		return channelUserName;
	}
    
    private String prepareQuoteHeader(Map<String, FieldData> values, FieldsManager fm, String quotedMessageDate, String clickedButtonType) throws DocumentNotFoundException, EdmException{
		/*
		From: cok@impuls-leasing.pl [mailto:cok@impuls-leasing.pl]
		Sent: Thursday, April 25, 2013 11:19 PM
		To: darls@op.pl
		Subject: RE: Test z nocki
		 */
		String header = "";
		String from = "Od: ";
		String to = "Do: ";
		String subject = "Temat: ";
		try {
			if(clickedButtonType.equals(DWR_RESPONSE_BUTTON) || clickedButtonType.equals(DWR_FORWARD_RESPONSE_BUTTON)){
				InternetAddress addr;
				addr = new InternetAddress(values.get("DWR_SENDER").getStringData());
				from += addr.getAddress();
				to += getChannelUserName(fm);
				subject += values.get("DWR_SUBJECT_EMAIL").getStringData();
			}else if(clickedButtonType.equals(DWR_SEND_BUTTON)){
				from += getChannelUserName(fm);
				InternetAddress addr = new InternetAddress(values.get("DWR_SENDER").getStringData());
				to += addr.getAddress();
				subject += "RE: " + values.get("DWR_SUBJECT_EMAIL").getStringData();
			}else if(clickedButtonType.equals(DWR_SEND_FORWARD_BUTTON)){
				from += getChannelUserName(fm);
				to += forwardTo;
				subject += "FW: " + values.get("DWR_SUBJECT_EMAIL").getStringData();
			}
		} catch (AddressException e) {
			LOG.error(e.getMessage(), e);
		}
		header = "<br /><br />" + from + "<br />" + "Wys�ano: " + quotedMessageDate + "<br />" + to + "<br />" + subject + "<br />";

		return header;
	}
    
    private String getUserMailFooter(){
        boolean contextOpened = DSApi.isContextOpen();
        try {
            if(!contextOpened){
                DSApi.open((javax.security.auth.Subject)WebContextFactory.get().getSession(false).getAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY));
                contextOpened = true;
            }
            String footer = null;

            if(UserAdditionalData.findByUserId(DSApi.context().getDSUser().getId()) != null)
                footer = UserAdditionalData.findByUserId(DSApi.context().getDSUser().getId()).getMailFooter();
            if(footer != null){
                footer = "<br /><br />" + UserAdditionalData.findByUserId(DSApi.context().getDSUser().getId()).getMailFooter().replace("\n", "<br/>").replace("\r", "");
            }else{
                footer = "";
            }
            return footer;
        } catch (EdmException e) {
            LOG.error(e.getMessage(), e);
            return "";
        } finally{
            if(contextOpened)
                DSApi._close();
        }
    }
    
    private String getLastQuotedMessageContent(Map<String, FieldData> values, FieldsManager fm) throws EdmException {

		StringBuilder sb = new StringBuilder();
		String quotedMessage;


		//	sprawdzamy czy uprzednio powstala inna odpowiedz na maila z zadania
        if(deliveryType == DeliveryType.EMAIL)
		    quotedMessage = DwrUtils.getStringValue(values, "DWR_SENT_RESP");
        else
            quotedMessage = DwrUtils.getStringValue(values, "DWR_CONVERSATION_DESCRIPTION");

        if(StringUtils.isEmpty(quotedMessage)) {
			if(deliveryType == DeliveryType.EMAIL){
                if(fm.getValue("CONTENTDATA") != null){
                    quotedMessage = fm.getValue("CONTENTDATA").toString();
                }else{
                    quotedMessage = "";
                    LOG.error("Nieudane pobranie wiadomo�ci e-mail dla dokumentu o id {}", fm.getDocumentId());
                }
			}else{
                if(fm.getValue("CONVERSATION_DESCRIPTION") != null){
                    quotedMessage = fm.getValue("CONVERSATION_DESCRIPTION").toString();
                }else{
                    quotedMessage = "";
                    LOG.error("Nieudane pobranie opisu rozmowy dla dokumentu o id {}", fm.getDocumentId());
                }
			}
		}

		if(quotedMessage == null)
			quotedMessage = "";

		sb.append(quotedMessage);
		return sb.toString();

	}
    
    private void setSentResponseContent(Map<String, FieldData> values, String responseContent){
		FieldData resp = values.get("DWR_SENT_RESP");
		HtmlEditorValue val = new HtmlEditorValue(responseContent);
        if(resp == null){
            resp = new FieldData();
        }
		resp.setHtmlEditorData(val);
	}
    
    private void calculateStampFee(Map<String, FieldData> values, StringBuilder msgBuilder) throws EdmException {
        boolean isOpened = false;
        PreparedStatement ps = null;
        ResultSet rs;
        try {
            if (!DSApi.isContextOpen()) {
                DSApi.openAdmin();
                isOpened = true;
            }
            Integer zpo = values.get(DWR_ZPO).getBooleanData() ? 1 : 0;
            Integer sendKind = Integer.valueOf(values.get(DWR_DOC_DELIVERY).getEnumValuesData().getSelectedId());
            Integer poziomListu = OutOfficeDocumentDelivery.find(sendKind).getPosn();
            Integer gabaryt = 0;
            if (poziomListu == 4 || poziomListu == 5 || poziomListu == 6 || poziomListu == 7) {
                if (DwrUtils.isNotNull(values, DWR_DOC_MAIL_SIZE)) {
                    gabaryt = Integer.valueOf(values.get(DWR_DOC_MAIL_SIZE).getEnumValuesData().getSelectedId());
                } else {
                    msgBuilder.append("Nie wybrano gabarytu");
                }
            }
            Integer waga = Integer.valueOf(values.get(DWR_DOC_MAIL_WEIGHT).getEnumValuesData().getSelectedId());

            String query = "SELECT kwota " +
                    "FROM ds_stawki_oplat_pocztowych " +
                    "WHERE zpo = ? " +
                    "AND poziomGabarytu = ? " +
                    "AND poziomListu = ? " +
                    "AND poziomWagi = ?";

            ps = DSApi.context().prepareStatement(query);
            ps.setInt(1, zpo);
            ps.setInt(2, gabaryt);
            ps.setInt(3, poziomListu);
            ps.setInt(4, waga);
            rs = ps.executeQuery();
            double kwota = 0.00;
            while (rs.next()) {
                kwota = rs.getDouble("kwota");
                break;
            }
            values.get(DWR_KOSZT_PRZESYLKI).setMoneyData(BigDecimal.valueOf(kwota));
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        } finally {
            if (DSApi.isContextOpen()) {
                DSApi.context().closeStatement(ps);
            }
            if (isOpened) {
                DSApi.close();
            }
        }
    }
    
    @Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
    	 super.validateDwr(values, fm);
    	 
         StringBuilder msgBuilder = new StringBuilder();

         if (DwrUtils.isNotNull(values, DWR_DOC_MAIL_WEIGHT, DWR_DOC_DELIVERY, DWR_DOC_MAIL_SIZE) && (values.get(DWR_DOC_MAIL_WEIGHT).isSender() || values.get(DWR_DOC_DELIVERY).isSender()
        		 || values.get(DWR_DOC_MAIL_SIZE).isSender())) {
             calculateStampFee(values, msgBuilder);
         }

         if (msgBuilder.length() > 0) {
             values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
             return new Field("messageField", msgBuilder.toString(), Field.Type.BOOLEAN);
         }

      // klikni�to Wy�lij (Wyslij)
         if(values.containsKey(DWR_SEND_FORWARD_BUTTON)) {
             FieldData fd = values.get(DWR_SEND_FORWARD_BUTTON);
             if(fd.isSender()) {
//                 setCurrentUserAsDocumentAuthor(fm);
                 //	ustawiamy zawartosc odpowiedzi aby byla widoczna na formatce
                 return sendMail(DWR_SEND_FORWARD_BUTTON, values, fm);
             }
         } 
         
         if(values.containsKey(DWR_GENERATE_KO)) {
        	 boolean  context = false;
        	 try {
        		 context = DSApi.openContextIfNeeded();
        		 DSApi.context().begin();
        		 FieldData fd = values.get(DWR_GENERATE_KO);
                 if(fd.isSender()) {
                	
                	 Document document = Document.find(fm.getDocumentId());
                	 if(document instanceof OutOfficeDocument){
         				OutOfficeDocument outDoc = (OutOfficeDocument)document;
         				if(outDoc.getDelivery() != null){
         					OutOfficeDocumentDelivery delivery =  outDoc.getDelivery();			
         					if(delivery.getName().equalsIgnoreCase("Osobi�cie") || delivery.getName().equalsIgnoreCase("Skrzynka email")){
         				            Date entryDate = GlobalPreferences.getCurrentDay();
         				            if (outDoc.getOfficeNumber() == null)
         				            {
         				                 bindToJournalOutDocument((OutOfficeDocument) document, entryDate);
         				                TaskSnapshot.updateAllTasksByDocumentId(document.getId(), document.getStringType());
         				                 //TaskSnapshot.updateByDocument(document);
         				                 return new Field("messageField", "Nadano numer KO " +  outDoc.getOfficeNumber(), Field.Type.BOOLEAN);
         				            }
         					}
         				}
         			}
                 }
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			} finally {
				DSApi.context().commit();
				DSApi.context().session().flush();
				DSApi.closeContextIfNeeded(context);
			}
             
         }


         return null;
 
    	
    }
    
    public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException{
    	FieldsManager fm = Document.find(documentId).getFieldsManager();
    	StringBuilder msgBuilder = new StringBuilder();
    	if ((WYCHODZACE.equals(fm.getDocumentKind().getCn())) && fm.getValue("RECIPIENT") == null){
		     msgBuilder.append(VALIDATE_RECIPIENT).append(System.getProperty("line.separator"));

		     if(msgBuilder.length() > 0) {
		         throw new EdmException(msgBuilder.toString());
		     }
    	}
	}
    
    
	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
		Map<String, Object> toReload = Maps.newHashMap();
		if ((WEWNETRZNE.equals(fm.getDocumentKind().getCn()) || WYCHODZACE.equals(fm.getDocumentKind().getCn())) && DSApi.context().getDSUser().getDivisionsWithoutGroup().length>0) {
			String sender = "u:"+DSApi.context().getDSUser().getName();
			sender+=(";d:"+DSApi.context().getDSUser().getDivisionsWithoutGroup()[0].getGuid());
			toReload.put("SENDER_HERE", sender);	
		}
		
		if(WYCHODZACE.equals(fm.getDocumentKind().getCn())){
			toReload.put("DOC_DELIVERY", 5);
		}
        //toReload.put("DATA_GODZINA_WPLYWU", Calendar.getInstance().getTime());
        toReload.put("DOC_DATE", Calendar.getInstance().getTime());
		fm.reloadValues(toReload);
	}


}
