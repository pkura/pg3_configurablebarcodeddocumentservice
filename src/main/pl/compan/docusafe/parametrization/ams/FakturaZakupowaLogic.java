package pl.compan.docusafe.parametrization.ams;

import com.google.common.collect.Sets;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.primitives.Ints;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Constants;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.enums.ExternalSourceEnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.exports.*;
import pl.compan.docusafe.core.exports.FinancialTask;
import pl.compan.docusafe.core.imports.simple.repository.CacheHandler;
import pl.compan.docusafe.core.imports.simple.repository.Dictionary;
import pl.compan.docusafe.core.imports.simple.repository.DictionaryMap;
import pl.compan.docusafe.core.imports.simple.repository.SimpleRepositoryAttribute;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.ams.hb.AMSProduct;
import pl.compan.docusafe.parametrization.ams.hb.AMSSupplier;
import pl.compan.docusafe.parametrization.ams.hb.MPKDictionaryItem;
import pl.compan.docusafe.parametrization.ams.hb.PatternsOfCostSharing;
import pl.compan.docusafe.parametrization.ams.hb.PurchaseDocumentType;
import pl.compan.docusafe.parametrization.ams.hb.RequestForReservation;
import pl.compan.docusafe.parametrization.ams.hb.ReservationPositionDictionaryItem;
import pl.compan.docusafe.parametrization.ams.hb.SupplierOrderPosition;
import pl.compan.docusafe.parametrization.ams.hb.SupplierOrderPositionItem;
import pl.compan.docusafe.parametrization.ams.hb.VatRatio;
import pl.compan.docusafe.parametrization.ams.hb.Warehouse;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static pl.compan.docusafe.core.dockinds.field.DataBaseEnumField.REPO_REF_FIELD_PREFIX;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

public class FakturaZakupowaLogic extends AbstractDocumentLogic
{

    public static final String DOC_KIND_CN = "faktura";

	public static final String STATUS_FIELD = "STATUS";
	public static final String KWOTA_BRUTTO_FIELD = "KWOTA_BRUTTO";
	public static final String KWOTA_NETTO_FIELD = "KWOTA_NETTO";
	public static final String KWOTA_VAT_FIELD = "KWOTA_VAT";
	public static final String KWOTA_UZNANIA_FIELD = "KWOTA_UZNANIA";
    public static final String RATY_FIELD = "RATY";
	public static final String SUMA_RAT_FIELD = "SUMA_RAT";
	public static final String ZALICZKA_FIELD = "ZALICZKA";
	public static final String MPK_FIELD = "MPK";
	public static final String DEKRETY_FIELD = "DEKRETY";
    public static final String DEKRETY_WITH_SUFFIX_FIELD = DEKRETY_FIELD + "_";
    public static final String MAGAZYN_FIELD = "MAGAZYN";
	public static final String SENDER_FIELD = "SENDER";
	public static final String DATA_WYSTAWIENIA_FIELD = "DATA_WYSTAWIENIA";
	public static final String TERMIN_PLATNOSCI_FIELD = "TERMIN_PLATNOSCI";
	public static final String TYP_FAKTURY_FIELD = "TYP_FAKTURY";
	public static final String SUPPLIER_FIELD = "SUPPLIER";
	public static final String RECIPIENT_HERE_FIELD = "RECIPIENT_HERE";
	public static final String MPK_SUM_AMOUNT_FIELD = "MPK_SUM_AMOUNT";
	public static final String DEKRETY_SUM_AMOUNT_FIELD = "DEKRETY_SUM_AMOUNT";
	public static final String NR_ZAMOWIENIA_FROM_ERP_FIELD = "NR_ZAMOWIENIA_FROM_ERP";
	public static final String POZYCJE_ZAMOWIENIA_FIELD = "POZYCJE_ZAMOWIENIA";
	public static final String NR_POZ_FIELD = "NR_POZ";
	public static final String WYTWOR_FIELD = "WYTWOR";
	public static final String ILOSC_FIELD = "ILOSC";
	public static final String CENA_FIELD = "CENA";
	public static final String CENA_BRUTTO_FIELD = "CENA_BRUTTO";
	public static final String KOSZT_FIELD = "KOSZT";
	public static final String NR_FAKTURY_FIELD = "NR_FAKTURY";
	public static final String WALUTA_FIELD = "WALUTA";
	public static final String NR_KONTA_NOWY_FIELD = "NR_KONTA_NOWY";
	public static final String DOSTAWCA_NR_KONTA_ID_FIELD = "DOSTAWCA_NR_KONTA_ID";
	public static final String MPK_KWOTA_DO_ROZPISANIA_FIELD = "MPK_KWOTA_DO_ROZPISANIA";
	public static final String VAT_RATIO_FIELD = "VAT_RATIO";
	public static final String KWOTA_BUDZETU_FIELD = "KWOTA_BUDZETU";
	public static final String KWOTA_VAT_DO_ODLICZENIA_FIELD = "KWOTA_VAT_DO_ODLICZENIA";
	public static final String VAT_DO_ODLICZENIA_SUM_FIELD = "VAT_DO_ODLICZENIA_SUM";
	public static final String VAT_KOSZTY_SUM_FIELD = "VAT_KOSZTY_SUM";
	public static final String KWOTA_VAT_KOSZTY_FIELD = "KWOTA_VAT_KOSZTY";
	public static final String POZYCJE_REZERWACJI_FIELD = "POZYCJE_REZERWACJI";
	public static final String DEKRETY_SPRZEDAZ_OPODATKOWANA = "DEKRETY_SPRZEDAZ_OPODATKOWANA";

	public static final String ZALICZKA_DICT_FIELD = "ZALICZKA_DICT";

	
	public static final String REALIZATOR_ACCEPTATION_CN = "REALIZATOR";
	public static final String DYSPONENT_ACCEPTATION_CN = "dysponent";
	public static final String CONFIRMATION_JW_ACCEPTATION_CN = "confirmation_jw";
	public static final String MAGAZYNIER_ACCEPTATION_CN = "magazynier";
	public static final String COMPATIBILITY_VERIFICATION_ACCEPTATION_CN = "compatibility_verification";
	public static final String COMPATIBILITY_ACCEPTATION_ACCEPTATION_CN = "compatibility_acceptation";
	public static final String IMPLEMENTING_CELL_SUPERVISOR_ACCEPTATION_CN = "implementing_cell_supervisor";
	
	public static final String DWR_STATUS = "DWR_STATUS";
	public static final String DWR_KWOTA_BRUTTO = "DWR_KWOTA_BRUTTO";
	public static final String DWR_KWOTA_UZNANIA = "DWR_KWOTA_UZNANIA";
	public static final String DWR_HIDE_MPK_DO_WYJ = "DWR_HIDE_MPK_DO_WYJ";
	public static final String DWR_RATY = "DWR_RATY";
    public static final String DATA_WPLYWU_FAKTURY_FIELD = "DATA_WPLYWU_FAKTURY";
    public static final String DWR_KWOTA_VAT = "DWR_KWOTA_VAT";
    public static final String DWR_KWOTA_NETTO = "DWR_KWOTA_NETTO";
    public static final String DWR_MPK = "DWR_MPK";
    public static final String DWR_MPK_DO_WYJ = "DWR_MPK_DO_WYJ";
    public static final String MPK_AMOUNT = "MPK_AMOUNT";
    public static final String MPK_DO_WYJ_AMOUNT = "MPK_DO_WYJ_AMOUNT";
    public static final String DWR_MPK_SUM_AMOUNT = "DWR_MPK_SUM_AMOUNT";
    public static final String DWR_MPK_DO_WYJ_SUM_AMOUNT = "DWR_MPK_DO_WYJ_SUM_AMOUNT";
    public static final String DWR_SUMA_RAT = "DWR_SUMA_RAT";
    public static final String RATY_KWOTA = "RATY_KWOTA";
    public static final String MPK_STAWKA = "MPK_STAWKA";
    public static final String MPK_NETTO = "MPK_NETTO";
    public static final String MPK_BRUTTO = "MPK_BRUTTO";
    public static final String MPK_KWOTA_VAT = "MPK_KWOTA_VAT";
    public static final String MPK_DO_WYJ_BRUTTO = "MPK_DO_WYJ_BRUTTO";
    public static final String GENERATE_DATE = "GENERATE_DATE";
    public static final String SUPPLIER_ORGANIZATION = "SUPPLIER_ORGANIZATION";
    public static final String SUPPLIER_NIP = "SUPPLIER_NIP";
    public static final String SUPPLIER_POST = "SUPPLIER_POST";
    public static final String SUPPLIER_STREET = "SUPPLIER_STREET";
    public static final String SUPPLIER_CITY = "SUPPLIER_CITY";
    public static final String FIELDS = "fields";
    public static final String DEKRETY_STAWKA = "DEKRETY_STAWKA";
    public static final String DEKRETY_NETTO = "DEKRETY_NETTO";
    public static final String DEKRETY_BRUTTO = "DEKRETY_BRUTTO";
    public static final String DEKRETY_KWOTA_VAT = "DEKRETY_KWOTA_VAT";
    public static final String DWR_TYP_FAKTURY = "DWR_TYP_FAKTURY";
    public static final String DWR_SHOW_KURS = "DWR_SHOW_KURS";
    public static final String MPK_KWOTA_BUDZETU = "MPK_KWOTA_BUDZETU";
    public static final String DWR_DEKRETY = "DWR_DEKRETY";
    public static final String DWR_DEKRETY_SUM_AMOUNT = "DWR_DEKRETY_SUM_AMOUNT";
    public static final String DWR_NR_ZAMOWIENIA_FROM_ERP = "DWR_NR_ZAMOWIENIA_FROM_ERP";
    public static final String DWR_POZYCJE_ZAMOWIENIA = "DWR_POZYCJE_ZAMOWIENIA";
    public static final String DWR_MPK_KWOTA_DO_ROZPISANIA = "DWR_MPK_KWOTA_DO_ROZPISANIA";
    public static final String DEKRETY_KWOTA_VAT_DO_ODLICZENIA = "DEKRETY_KWOTA_VAT_DO_ODLICZENIA";
    public static final String DEKRETY_KWOTA_VAT_KOSZTY = "DEKRETY_KWOTA_VAT_KOSZTY";
    public static final String DWR_VAT_KOSZTY_SUM = "DWR_VAT_KOSZTY_SUM";
    public static final String DWR_VAT_DO_ODLICZENIA_SUM = "DWR_VAT_DO_ODLICZENIA_SUM";
    public static final String DWR_WNIOSKI_O_REZERWACJE = "DWR_WNIOSKI_O_REZERWACJE";
    public static final String DWR_POZYCJE_REZERWACJI = "DWR_POZYCJE_REZERWACJI";
    public static final String DWR_SET_FIELDS_IN_MPK_AND_DEKRETY = "DWR_SET_FIELDS_IN_MPK_AND_DEKRETY";
    public static final String WNIOSKI_O_REZERWACJE_REZERWACJA_IDM = "WNIOSKI_O_REZERWACJE_REZERWACJA_IDM";
    public static final String ID = "ID";
    public static final String DWR_PREFIX = "DWR_";
    
    public static final String TABLENAME_WITH_VAT_RATES = "dsg_ams_view_vat_rate";
    public static final String TABLENAME_WITH_BUDGET_CELL = "dsg_ams_view_budget_cell";
    public static final String TABLENAME_WITH_BUDGET_POSITION = "dsg_ams_view_budget_position";
    public static final String TABLENAME_WITH_BUDGET_IDENTIFIER = "dsg_ams_view_budget";
    public static final String TABLENAME_WITH_BUDGET_TASK = "dsg_ams_view_budget_task";
    
    public static final Integer PROCESS_CREATION_STATUS = 10;
    public static final Integer ESSENTIAL_DESCRIPTION_STATUS = 50;
    public static final Integer ACCOUNT_CONTROL_STATUS = 500;
    
    public static final String SPRZEDAZ_OPODATKOWANA = "0";
    public static final String SPRZEDAZ_NIE_OPODATKOWANA = "1";
    public static final String NIE_OKRESLONE = "2";
    
    public static final String USER_ID_JBPM4_VARIABLE = "userId";

    private static final long serialVersionUID = 1L;
    private static final int MONEY_SCALE = 2;
    protected static final EnumValues EMPTY_ENUM_VALUE = new EnumValues(new HashMap<String, String>(), new ArrayList<String>());
    public static final EnumValues EMPTY_NOT_HIDDEN_ENUM_VALUE = new EnumValues(new HashMap<String,String>(){{put("","-- wybierz --");}}, new ArrayList<String>(){{add("");}});
    public static final long REPOSITORY_POSITION_CONTEXT_ID = 6l;
    public static final String DEKRETY_TABLE = "dsg_ams_dekrety_dictionary";
    public static final String DSG_AMS_VIEW_BUDGET_KIND = "dsg_ams_view_budget_kind";
    public static final String RATY_FIELD_CN = "RATY";
    public static final String SPOSOB_ZAPLATY_FIELD = "SPOSOB";
    public static final String PRODUCT_VIEW = "dsg_ams_view_product";
    
    public static final String UNPLANNED_BUDGET = "1";
	public static final String PLANNED_BUDGET = "2";

    private static final Logger log = LoggerFactory.getLogger(FakturaZakupowaLogic.class);
    private static final StringManager sm = StringManager.getManager(Constants.Package);

    
    @Override
    public boolean canChangeDockind(Document document) throws EdmException {
    	return false;
    }
    
    @Override
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException {
		/* sprawdzenie czy istnieje w bazie faktura o takim samym numerze, kontrahencie i roku, jezeli tak to rzucany jest wyjatek */
    	Integer status = (Integer) values.get(STATUS_FIELD);
    	if (status == null || status.equals(PROCESS_CREATION_STATUS)) {
			if (!DSApi.isContextOpen()) {
				DSApi.openAdmin();
			}
	        List<Long> ids = getListOfIdenticalFactures(values, kind.getTablename());
			if (!ids.isEmpty()) {
				throw new EdmException(sm.getString("IdenticalFactureError", ids));
			}
    	}
    }
    
    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
    {
        log.info("--- Faktura Zakupowa : START PROCESS !!! ---- {}", event);

        try {
            Map<String, Object> map = Maps.newHashMap();
            map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
            map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
            java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

            DSUser author = DSApi.context().getDSUser();
            String user = author.getName();
            String fullName = author.asFirstnameLastname();

            perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));
    		
            Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
            perms.addAll(documentPermissions);
            this.setUpPermission(document, perms);

            if (AvailabilityManager.isAvailable("addToWatch")) {
                DSApi.context().watch(URN.create(document));
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new EdmException(e.getMessage());
        }
    }
    
    @Override
    public void setAdditionalTemplateValues(long docId, Map<String, Object> values) {
    	try {
    		Document doc = Document.find(docId);
    		FieldsManager fm = doc.getFieldsManager();
    		
    		// umieszczenie daty generacji dokumentu
    		values.put(GENERATE_DATE, DateUtils.formatCommonDate(new Date()));
    		
    		// umieszczenie w values podstawowych informacji z dokumentu
    		putDataFromDocumentToValues(fm, values);
    		
    		// umieszczenie typu faktury
    		if (fm.getEnumItem(TYP_FAKTURY_FIELD) != null) {
    			values.put(TYP_FAKTURY_FIELD, fm.getEnumItem(TYP_FAKTURY_FIELD).getTitle());
    		}
    		
    		// umieszczenie waluty
    		values.put(WALUTA_FIELD, fm.getEnumItem(WALUTA_FIELD).getCn());
    		
    		// umieszczenie numeru konta (poprawionego albo normalnego)
    		if (fm.getKey(NR_KONTA_NOWY_FIELD) != null) {
    			values.put("NR_KONTA", fm.getKey(NR_KONTA_NOWY_FIELD));
    		} else if (fm.getKey(DOSTAWCA_NR_KONTA_ID_FIELD) != null) {
    			values.put("NR_KONTA", fm.getEnumItem(DOSTAWCA_NR_KONTA_ID_FIELD).getTitle());
    		}
    		
    		// umieszczenie danych o dostawcy
    		AMSSupplier supplier = getSupplierFromDocument(fm);
    		if (supplier != null) {
    			values.put(SUPPLIER_ORGANIZATION, supplier.getNazwa());
    			values.put(SUPPLIER_NIP, supplier.getNip());
    			values.put(SUPPLIER_POST, supplier.getKodPocztowy());
    			values.put(SUPPLIER_CITY, supplier.getMiasto());
    			values.put(SUPPLIER_STREET, supplier.getUlica());
    		}
    		
    		// ustawienie akceptacji
    		setAcceptances(docId, values, CONFIRMATION_JW_ACCEPTATION_CN, COMPATIBILITY_ACCEPTATION_ACCEPTATION_CN, COMPATIBILITY_VERIFICATION_ACCEPTATION_CN);
    		
//    		for (DocumentAcceptance dacc : DocumentAcceptance.find(docId)) {
//    			if (!CONFIRMATION_JW_ACCEPTATION_CN.equals(dacc.getAcceptanceCn())) {
//    				DSUser acceptedUser = DSUser.findByUsername(dacc.getUsername());
//    				values.put(dacc.getAcceptanceCn(), acceptedUser.getLastname() + " " + acceptedUser.getFirstname());
//    				values.put(dacc.getAcceptanceCn() + "_DATE", DateUtils.formatCommonDate(dacc.getAcceptanceTime()));
//    			}
//    		}
    		
    	} catch (EdmException e) {
    		log.error(e.getMessage(), e);
    	}
    }

    private void putDataFromDocumentToValues(FieldsManager fm, Map<String, Object> values) throws EdmException {
    	Map<String, Object> fieldValues = (Map<String, Object>) values.get(FIELDS);
    	if (fieldValues.containsKey(DATA_WPLYWU_FAKTURY_FIELD) && fieldValues.get(DATA_WPLYWU_FAKTURY_FIELD) != null) {
    		String date = DateUtils.formatCommonDate((Date) fieldValues.get(DATA_WPLYWU_FAKTURY_FIELD));
    		values.put(DATA_WPLYWU_FAKTURY_FIELD, date);
    	}
    	if (fieldValues.containsKey(DATA_WYSTAWIENIA_FIELD) && fieldValues.get(DATA_WYSTAWIENIA_FIELD) != null) {
    		String date = DateUtils.formatCommonDate((Date) fieldValues.get(DATA_WYSTAWIENIA_FIELD));
    		values.put(DATA_WYSTAWIENIA_FIELD, date);
    	}
    	if (fieldValues.containsKey(TERMIN_PLATNOSCI_FIELD) && fieldValues.get(TERMIN_PLATNOSCI_FIELD) != null) {
    		String date = DateUtils.formatCommonDate((Date) fieldValues.get(TERMIN_PLATNOSCI_FIELD));
    		values.put(TERMIN_PLATNOSCI_FIELD, date);
    	}
    	if (fieldValues.containsKey(KWOTA_BRUTTO_FIELD) && fieldValues.get(KWOTA_BRUTTO_FIELD) != null) {
    		BigDecimal grossAmount = (BigDecimal) fieldValues.get(KWOTA_BRUTTO_FIELD);
    		grossAmount = grossAmount.setScale(MONEY_SCALE);
    		values.put(KWOTA_BRUTTO_FIELD, grossAmount);
    	}
    	if (fieldValues.containsKey(KWOTA_NETTO_FIELD) && fieldValues.get(KWOTA_NETTO_FIELD) != null) {
    		BigDecimal grossAmount = (BigDecimal) fieldValues.get(KWOTA_NETTO_FIELD);
    		grossAmount = grossAmount.setScale(MONEY_SCALE);
    		values.put(KWOTA_NETTO_FIELD, grossAmount);
    	}
    	if (fieldValues.containsKey(KWOTA_VAT_FIELD) && fieldValues.get(KWOTA_VAT_FIELD) != null) {
    		BigDecimal grossAmount = (BigDecimal) fieldValues.get(KWOTA_VAT_FIELD);
    		grossAmount = grossAmount.setScale(MONEY_SCALE);
    		values.put(KWOTA_VAT_FIELD, grossAmount);
    	}
    }
    
    private AMSSupplier getSupplierFromDocument(FieldsManager fm) throws EdmException {
		Long supplierId = (Long) fm.getKey(SUPPLIER_FIELD);
		AMSSupplier supplier = (AMSSupplier) DSApi.context().session().createCriteria(AMSSupplier.class)
			.add(Restrictions.idEq(supplierId))
			.uniqueResult();
    	return supplier;
    }
    
    @Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
    	Map<String, Object> toReload = Maps.newHashMap();
        toReload.put(DATA_WPLYWU_FAKTURY_FIELD, Calendar.getInstance().getTime());
		fm.reloadValues(toReload);
	}

    @Override
    public void documentPermissions(Document document) throws EdmException
    {
        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
        String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
        String documentKindName = document.getDocumentKind().getName();
        
        perms.add(new PermissionBean(ObjectPermission.READ, documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, documentKindName + " - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP,
                documentKindName + " zalacznik - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, documentKindName
                + " - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP,
                documentKindName + " zalacznik - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, documentKindName
                + " - usuwanie"));
        Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
        perms.addAll(documentPermissions);
        this.setUpPermission(document, perms);
        
//        removePermissionsToAtachments(document);
    }
    
    public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException {
    	TaskListParams params = new TaskListParams();
    	FieldsManager fm = dockind.getFieldsManager(id);
    	
    	AMSSupplier supplier = getSupplierFromDocument(fm);
    	params.setAttribute(supplier.getNazwa(), 0);
    	
    	String numerFaktury = fm.getStringValue(NR_FAKTURY_FIELD);
    	if(numerFaktury != null && numerFaktury.length() > 0) {
    		params.setAttribute(numerFaktury, 2);
    	}
    	
    	if (fm.getKey(KWOTA_BRUTTO_FIELD) != null) {
    		BigDecimal kwotaBruttoFaktury = (BigDecimal) fm.getKey(KWOTA_BRUTTO_FIELD);
    		params.setAttribute(kwotaBruttoFaktury.toString(), 3);
    	}
    	
    	
    	return params;
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
    	FieldsManager fm = document.getFieldsManager();
    	Integer status = (Integer) fm.getKey(STATUS_FIELD);
    	if (status == null || status.equals(PROCESS_CREATION_STATUS)) {
    		/* przypisanie wartosci do pola: KWOTA_UZNANIA */
    		BigDecimal amountGross = (BigDecimal) fm.getKey(KWOTA_BRUTTO_FIELD);
    		if (amountGross != null) {
    			Map<String, Object> fieldValues = new HashMap<String, Object>();
				fieldValues.put(KWOTA_UZNANIA_FIELD, amountGross);
    			document.getDocumentKind().setOnly(document.getId(), fieldValues);
    		}
    	}	
    }
    
    private List<Long> getListOfIdenticalFactures(Map<String, Object> values, String tableName) throws EdmException {
		String factureNumber = (String) values.get(NR_FAKTURY_FIELD);
		Date date = (Date) values.get(DATA_WYSTAWIENIA_FIELD);
		Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
		Long supplierId = (Long) values.get(SUPPLIER_FIELD);
		StringBuilder queryStr = new StringBuilder();
		queryStr.append("SELECT document_id FROM ")
			.append(tableName)
			.append(" WHERE nr_faktury = :factureNumber AND year(data_wystawienia) = :year AND supplier = :supplierId");
		SQLQuery query = DSApi.context().session().createSQLQuery(queryStr.toString());
		query.setParameter("factureNumber", factureNumber);
		query.setParameter("year", cal.get(Calendar.YEAR));
		query.setParameter("supplierId", supplierId);
		return query.list();
    }
    
    /*
     * 
     */
    private BigDecimal sumBigDecimalValuesInDictionary(Map<String, FieldData> values, String DictionaryCn, String fieldCn) {
    	BigDecimal sum = new BigDecimal(0);
    	sum = sum.setScale(MONEY_SCALE);
    	Map<String, FieldData> dictionaryValues = (Map<String, FieldData>) values.get(DWR_PREFIX + DictionaryCn).getDictionaryData();
    	for (String key : dictionaryValues.keySet()) {
    		if (key.contains(DictionaryCn + "_" + fieldCn) && dictionaryValues.get(key).getData() != null) {
    			BigDecimal valueFromField = dictionaryValues.get(key).getMoneyData().setScale(MONEY_SCALE);
    			sum = sum.add(valueFromField);
    		}
    	}
    	return sum;
    }
    
    @Override
    public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
    {
        /* Wyliczanie kwoty VAT (PLN)*/
        if (DwrUtils.isNotNull(values, DWR_KWOTA_NETTO, DWR_KWOTA_BRUTTO) && (values.get(DWR_KWOTA_NETTO).isSender() || values.get(DWR_KWOTA_BRUTTO).isSender())) {
    	    BigDecimal netto = values.get(DWR_KWOTA_NETTO).getMoneyData().setScale(2, RoundingMode.HALF_UP);
    	    BigDecimal brutto = values.get(DWR_KWOTA_BRUTTO).getMoneyData().setScale(2, RoundingMode.HALF_UP);
    	    BigDecimal vat = brutto.subtract(netto);
    	    values.get(DWR_KWOTA_VAT).setMoneyData(vat);
    	}

        /* pokazywanie slownika z mpk'ami do wyjasnienia w przypadku gdy kwota umowna jest mniejsza od kwot na fakturze */
//        if (values.containsKey(DWR_STATUS)) {
//        	String statusId = values.get(DWR_STATUS).getEnumValuesData().getSelectedId();
//        	if (ESSENTIAL_DESCRIPTION_STATUS.toString().equals(statusId)) {
//        		if (isAmountOfRecognitionLessThanAmountGross(values)) {
//        			values.get(DWR_HIDE_MPK_DO_WYJ).setBooleanData(true);
//        		} else {
//        			values.get(DWR_HIDE_MPK_DO_WYJ).setBooleanData(false);
//        		}
//        	}
//        }
        
        /* zliczanie sumy kwot z mpk'ow ze slownika: "MPK" i zliczanie proponowanej kwoty brutto do rozpisania w mpk */
        if (values.containsKey(DWR_MPK) && values.get(DWR_MPK) != null) {
        	BigDecimal sum = sumBigDecimalValuesInDictionary(values, MPK_FIELD, KWOTA_BUDZETU_FIELD);
        	values.get(DWR_MPK_SUM_AMOUNT).setMoneyData(sum);
        	// sprawdzenie czy suma kwot brutto mpkow jest wieksza od kwoty brutto na fakturze,
        	//jezeli tak to jest rzucany wyjatek
        	BigDecimal grossAmount = values.get(DWR_KWOTA_BRUTTO).getMoneyData().setScale(2, RoundingMode.HALF_UP);
        	if (sum.compareTo(grossAmount) > 0) {
				values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
				return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", sm.getString("MpkAmountsAreBiggerThanGrossAmount"), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
        	}
        	// wyliczenie i ustawienie pola z proponowana kwota brutto do rozpisania w mpk
        	FieldData proposedAmount = new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.MONEY, grossAmount.subtract(sum).setScale(MONEY_SCALE, RoundingMode.HALF_UP));
        	values.put(DWR_MPK_KWOTA_DO_ROZPISANIA, proposedAmount);
        	
        }
        
        /* zliczanie sumy kwot brutto ze slownika "DEKRETY" */
        if (values.containsKey(DWR_DEKRETY) && values.get(DWR_DEKRETY) != null) {
        	BigDecimal sum = new BigDecimal(0);
        	sum = sum.setScale(MONEY_SCALE);
        	Map<String, FieldData> dekretyValues = (Map<String, FieldData>) values.get(DWR_DEKRETY).getDictionaryData();
        	BigDecimal oldGrossAmount = null;
        	BigDecimal newGrossAmount = null;
//        	HttpSession session = WebContextFactory.get().getSession();
//            if (session != null) {
//            	oldGrossAmount = (BigDecimal) session.getAttribute(DEKRETY_BRUTTO + "_OLD");
//                newGrossAmount = (BigDecimal) session.getAttribute(DEKRETY_BRUTTO + "_NEW");
//            }
        	for (String key : dekretyValues.keySet()) {
        		if (key.contains(DEKRETY_BRUTTO) && dekretyValues.get(key).getData() != null) {
        			BigDecimal amount = dekretyValues.get(key).getMoneyData().setScale(MONEY_SCALE);
//        			if (amount.equals(oldGrossAmount)) {
//        				amount = newGrossAmount;
//        			}
        			sum = sum.add(amount);
//        		} else if(key.contains(DEKRETY_BRUTTO) && dekretyValues.get(key).getData() == null && oldGrossAmount == null && newGrossAmount != null) {
//        			sum = sum.add(newGrossAmount);
        		}
        	}
        	values.get(DWR_DEKRETY_SUM_AMOUNT).setMoneyData(sum);
        	BigDecimal vatToDeduct = sumBigDecimalValuesInDictionary(values, DEKRETY_FIELD, KWOTA_VAT_DO_ODLICZENIA_FIELD);
        	values.get(DWR_VAT_DO_ODLICZENIA_SUM).setMoneyData(vatToDeduct);
        	BigDecimal vatCosts = sumBigDecimalValuesInDictionary(values, DEKRETY_FIELD, KWOTA_VAT_KOSZTY_FIELD);
        	values.get(DWR_VAT_KOSZTY_SUM).setMoneyData(vatCosts);
        	// sprawdzenie czy suma kwot brutto dekretow jest wieksza od kwoty brutto na fakturze,
        	//jezeli tak to jest rzucany wyjatek
        	BigDecimal grossAmount = values.get(DWR_KWOTA_BRUTTO).getMoneyData().setScale(MONEY_SCALE, RoundingMode.HALF_UP);
        	if (sum.compareTo(grossAmount) > 0) {
				values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
				return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", sm.getString("DekretyAmountsAreBiggerThanGrossAmount"), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
        	}
        	
        }
        
//        
//        /* zliczanie sumy kwot z mpk'ow ze slownika: "MPK_DO_WYJ"*/
//        if (values.containsKey(DWR_MPK_DO_WYJ) && values.get(DWR_MPK_DO_WYJ) != null) {
//        	BigDecimal sum = new BigDecimal(0);
//        	sum = sum.setScale(MONEY_SCALE);
//        	Map<String, FieldData> mpkValues = (Map<String, FieldData>) values.get(DWR_MPK_DO_WYJ).getDictionaryData();
//        	for (String key : mpkValues.keySet()) {
//        		if (key.contains(MPK_DO_WYJ_BRUTTO) && mpkValues.get(key).getData() != null) {
//        			BigDecimal amount = mpkValues.get(key).getMoneyData().setScale(MONEY_SCALE);
//        			sum = sum.add(amount);
//        		}
//        	}
//        	values.get(DWR_MPK_DO_WYJ_SUM_AMOUNT).setMoneyData(sum);
//        }
//        
        /* zliczenie sumy kwot rat ze slownika: "RATY" */
        if (values.containsKey(DWR_RATY) && values.get(DWR_RATY) != null) {
        	BigDecimal sum = new BigDecimal(0);
        	sum = sum.setScale(MONEY_SCALE);
        	Map<String, FieldData> installmentsValues = (Map<String, FieldData>) values.get(DWR_RATY).getDictionaryData();
        	for (String key : installmentsValues.keySet()) {
        		if (key.contains(RATY_KWOTA) && installmentsValues.get(key).getData() != null) {
        			BigDecimal amount = installmentsValues.get(key).getMoneyData().setScale(MONEY_SCALE);
        			sum = sum.add(amount);
        		}
        	}
        	values.get(DWR_SUMA_RAT).setMoneyData(sum);
        	if (values.containsKey(DWR_KWOTA_UZNANIA) && values.get(DWR_KWOTA_UZNANIA) != null) {
        		BigDecimal amount = values.get(DWR_KWOTA_UZNANIA).getMoneyData();
        		if (sum.compareTo(amount) > 0) {
        			values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
    				return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", sm.getString("InstallmentsAreBiggerThanGrossAmount"), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
        		}
        	}
        }
        
//        /* sprawdzenie czy jest wymagany kurs dla danego typu faktury, jezeli tak to jest ustawiane */
//        if (values.containsKey(DWR_TYP_FAKTURY) && values.get(DWR_TYP_FAKTURY) != null) {
//        	String id = values.get(DWR_TYP_FAKTURY).getEnumValuesData().getSelectedId();
//        	DSApi.openAdmin();
//        	try {
//        		if (StringUtils.isNotBlank(id)) {
//		        	PurchaseDocumentType purchaseDocumentType = (PurchaseDocumentType) DSApi.context().session().createCriteria(PurchaseDocumentType.class)
//		        		.add(Restrictions.eq("id", Long.valueOf(id)))
//		        		.uniqueResult();
//		        	if (new Double(1).equals(purchaseDocumentType.getCzywal()) ? true : false) {
//		        		values.get(DWR_SHOW_KURS).setBooleanData(true);
//		        	} else {
//		        		values.get(DWR_SHOW_KURS).setBooleanData(null);
//		        	}
//        		}
//        	} catch (Exception e) {
//        		log.error(e.getMessage(), e);
//        	} finally {
//        		DSApi.close();
//        	}
//        }
        
        if (values.containsKey(DWR_NR_ZAMOWIENIA_FROM_ERP) && values.get(DWR_NR_ZAMOWIENIA_FROM_ERP) != null && values.get(DWR_NR_ZAMOWIENIA_FROM_ERP).isSender()) {
        	Map<String, FieldData> positionValues = new LinkedHashMap<String, FieldData>();
//        	Map<String, FieldData> mpkValues = new LinkedHashMap<String, FieldData>();
//        	Map<String, FieldData> dekretyValues = new LinkedHashMap<String, FieldData>();
        	try {
	        	String selectedOrder = ((FieldData) values.get(DWR_NR_ZAMOWIENIA_FROM_ERP)).getEnumValuesData().getSelectedId();
	        	if (StringUtils.isNotBlank(selectedOrder)) {
	        		DSApi.openAdmin();
	        		List<SupplierOrderPosition> positionList = (List<SupplierOrderPosition>) DSApi.context().session().createCriteria(SupplierOrderPosition.class)
	        				.add(Restrictions.eq("supplier_order_id", Long.valueOf(selectedOrder)))
	        				.list();
	        		int index = 1;
	        		for (SupplierOrderPosition position : positionList) {
	        			DSApi.context().begin();
	        			AMSProduct product = (AMSProduct) DSApi.context().session().createCriteria(AMSProduct.class)
	        				.add(Restrictions.eq("idm", position.getWytwor_idm()))
	        				.uniqueResult();
	        			
	        			SupplierOrderPositionItem positionItem = new SupplierOrderPositionItem(position.getNrpoz(), product.getIdm(), product.getNazwa(), position.getIlosc(), position.getCena(),
	        					position.getCenabrutto(), position.getKoszt(), position.getKwotnett(), position.getKwotvat());
	        			Persister.create(positionItem);
	        			
	//        			/* uzupelnianie wpisow w MPK */
	//        			Long bdBudzetRodzajId = null;
	//        			if (position.getBdRodzajKoId() != null) {
	//        				bdBudzetRodzajId = position.getBdRodzajKoId();
	//        			} else if (position.getBdRodzajPlanZamId() != null) {
	//        				bdBudzetRodzajId = position.getBdRodzajPlanZamId();
	//        			}
	//        			List<BudgetView> budgetsList = (List<BudgetView>) DSApi.context().session().createCriteria(BudgetView.class)
	//        				.add(Restrictions.eq("bdBudzetRodzajId", bdBudzetRodzajId.doubleValue()))
	//        				.list();
	//        			BudgetView firstBudgetFromList = budgetsList.get(0);
	//        			Integer budgetCellId = (Integer) DSApi.context().session().createSQLQuery("SELECT ID FROM dsg_ams_view_budget_cell WHERE cn = :cnFromBudget")
	//        				.setParameter("cnFromBudget", firstBudgetFromList.getDokBdStrBudzetIdn())
	//        				.uniqueResult();
	//        			BigDecimal budgetTypeId = (BigDecimal) DSApi.context().session().createSQLQuery("SELECT ID FROM dsg_ams_view_budget_kind WHERE refValue = :budgetCellId AND title = :bdRodzaj")
	//        				.setParameter("budgetCellId", budgetCellId)
	//        				.setParameter("bdRodzaj", firstBudgetFromList.getBdRodzaj())
	//        				.uniqueResult();
	//        				
	//        			MPKDictionaryItem mpkItem = new MPKDictionaryItem(position.getNrpoz(), firstBudgetFromList.getBudzetId().longValue(), budgetCellId.longValue(), budgetTypeId.longValue(), firstBudgetFromList.getBdBudzetId().longValue(), bdBudzetRodzajId, null, null, null, null, null, null);
	//        			Persister.create(mpkItem);
	//        			
	//        			DekretyDictionaryItem dekretyItem = new DekretyDictionaryItem();
	        			DSApi.context().commit();
	        			positionValues.put(POZYCJE_ZAMOWIENIA_FIELD + "_" + ID + "_" + index, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, positionItem.getId()));
	//        			mpkValues.put(MPK_FIELD + "_" + ID + "_" + index, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, mpkItem.getId()));
	        			index++;
	        		}
	        		values.get(DWR_POZYCJE_ZAMOWIENIA).setDictionaryData(positionValues);
	        		
	        		setFieldsInMPKDictionaryAndDekretyDictionaryForOrders(fm, values, positionList);

	        		DSApi.close();
	        	}
        	} catch (EdmException e) {
        		log.error(e.getMessage(), e);
        	}
        }
        
        /* uzupelnianie wpisow w MPK */
//        if (IsMPKDictionaryEmpty(values) && values.containsKey(DWR_NR_ZAMOWIENIA_FROM_ERP) && values.get(DWR_NR_ZAMOWIENIA_FROM_ERP) != null) {
//        	Map<String, FieldData> mpkValues = new LinkedHashMap<String, FieldData>();
//        	String selectedOrder = ((FieldData) values.get(DWR_NR_ZAMOWIENIA_FROM_ERP)).getEnumValuesData().getSelectedId();
//        	if (StringUtils.isNotBlank(selectedOrder)) {
//        		DSApi.openAdmin();
//        		List<SupplierOrderPosition> positionList = (List<SupplierOrderPosition>) DSApi.context().session().createCriteria(SupplierOrderPosition.class)
//        				.add(Restrictions.eq("supplier_order_id", Long.valueOf(selectedOrder)))
//        				.list();
//        		int index = 1;
//        		for (SupplierOrderPosition position : positionList) {
//        			DSApi.context().begin();
//					Long bdBudzetRodzajId = null;
//					if (position.getBdRodzajKoId() != null) {
//						bdBudzetRodzajId = position.getBdRodzajKoId();
//					} else if (position.getBdRodzajPlanZamId() != null) {
//						bdBudzetRodzajId = position.getBdRodzajPlanZamId();
//					}
//        			List<BudgetView> budgetsList = (List<BudgetView>) DSApi.context().session().createCriteria(BudgetView.class)
//            				.add(Restrictions.eq("bdBudzetRodzajId", bdBudzetRodzajId.doubleValue()))
//            				.list();
//        			BudgetView firstBudgetFromList = budgetsList.get(0);
//        			Integer budgetCellId = (Integer) DSApi.context().session().createSQLQuery("SELECT ID FROM dsg_ams_view_budget_cell WHERE cn = :cnFromBudget")
//        				.setParameter("cnFromBudget", firstBudgetFromList.getDokBdStrBudzetIdn())
//        				.uniqueResult();
//        			
//        			/*Integer budgetCellId = (Integer) DSApi.context()
//        			 * .session()
//        			 * .createSQLQuery("SELECT ID FROM dsg_ams_view_budget_cell WHERE cn = :cnFromBudget and refValue = :budgetType")
//            				.setParameter("budgetType", id typu budzetu)
//            				.setParameter("cnFromBudget", firstBudgetFromList.getDokBdStrBudzetIdn())
//            				.uniqueResult();
//*/
//
//        			//BigDecimal budgetTypeId =BK -> 1, PZP -> 2
///*        			BigDecimal budgetTypeId = (BigDecimal) DSApi.context().session().createSQLQuery("SELECT ID FROM dsg_ams_view_budget_kind WHERE refValue = :budgetCellId AND title = :bdRodzaj")
//        				.setParameter("budgetCellId", budgetCellId)
//        				.setParameter("bdRodzaj", firstBudgetFromList.getBdRodzaj())
//        				.uniqueResult();*/
//        			
//        			Long budgetTypeId = null;
//        			if (firstBudgetFromList.getBdRodzaj().equals("BK")) {
//        				budgetTypeId = 1L;
//        			} else if (firstBudgetFromList.getBdRodzaj().equals("PZB")) {
//        				budgetTypeId = 2L;
//        			}
//        			
//        			MPKDictionaryItem mpkItem = new MPKDictionaryItem(position.getNrpoz(), firstBudgetFromList.getBudzetId().longValue(), budgetCellId.longValue(), budgetTypeId, firstBudgetFromList.getBdBudzetId().longValue(), bdBudzetRodzajId, null, null, null, null, null, null);
//        			Persister.create(mpkItem);
//        			
//        			DSApi.context().commit();
//        			mpkValues.put(MPK_FIELD + "_" + ID + "_" + index, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, mpkItem.getId()));
//        			index++;
//        		}
//        		values.get(DWR_MPK).setDictionaryData(mpkValues);
//        		DSApi.close();
//        	}
//        }
//        
//        /* uzupelnienie wpisow w DEKRETY */
//        if (IsDekretyDictionaryEmpty(values) && values.containsKey(DWR_NR_ZAMOWIENIA_FROM_ERP) && values.get(DWR_NR_ZAMOWIENIA_FROM_ERP) != null) {
//        	Map<String, FieldData> dekretyValues = new LinkedHashMap<String, FieldData>();
//        	String selectedOrder = ((FieldData) values.get(DWR_NR_ZAMOWIENIA_FROM_ERP)).getEnumValuesData().getSelectedId();
//        	if (StringUtils.isNotBlank(selectedOrder)) {
//        		DSApi.openAdmin();
//        		List<SupplierOrderPosition> positionList = (List<SupplierOrderPosition>) DSApi.context().session().createCriteria(SupplierOrderPosition.class)
//        				.add(Restrictions.eq("supplier_order_id", Long.valueOf(selectedOrder)))
//        				.list();
//        		int index = 1;
//        		for (SupplierOrderPosition position : positionList) {
//        			DSApi.context().begin();
//        			BigDecimal productId = (BigDecimal) DSApi.context().session().createSQLQuery("SELECT id FROM dsg_ams_view_product WHERE cn = :wytworIdm")
//        				.setParameter("wytworIdm", position.getWytwor_idm())
//        				.uniqueResult();
//        			DekretyDictionaryItem dekretyItem = new DekretyDictionaryItem(position.getNrpoz(), null, productId.longValue(), null, null, null, null, null, null, null, null, null, null, null, null);
//        			Persister.create(dekretyItem);
//        			
//        			DSApi.context().commit();
//        			dekretyValues.put(DEKRETY_FIELD + "_" + ID + "_" + index, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, dekretyItem.getId()));
//        			index++;
//        		}
//        		values.get(DWR_DEKRETY).setDictionaryData(dekretyValues);
//        		DSApi.close();
//        	}
//        }
        
        if (values.containsKey(DWR_WNIOSKI_O_REZERWACJE) && values.get(DWR_WNIOSKI_O_REZERWACJE).isSender()) {
        	List<Long>reservationIds = getRequestForReservationIds(values);
        	if (!reservationIds.isEmpty()) {
        		setReservationPositions(fm, values, reservationIds);
        	}
        }
        
        if (values.containsKey(DWR_SET_FIELDS_IN_MPK_AND_DEKRETY) && values.get(DWR_SET_FIELDS_IN_MPK_AND_DEKRETY).isSender()) {
        	List<Long>reservationIds = getRequestForReservationIds(values);
        	if (!reservationIds.isEmpty()) {
        		setFieldsInMPKDictionaryAndDekretyDictionaryForReservations(fm, reservationIds, values);
        	} else {
    			values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
				return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", sm.getString("NotSelectedRequstForReservation"), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
        	}
        }
        
        return null;
    }
    
    private void setFieldsInMPKDictionaryAndDekretyDictionaryForOrders(FieldsManager fm, Map<String, FieldData> values, List<SupplierOrderPosition> positionList) throws EdmException {
    	Map<String, FieldData> mpkValues = new LinkedHashMap<String, FieldData>();
    	List<Long> dekretyIds = new ArrayList<Long>();
    	
    	int index = 1;
		for (SupplierOrderPosition position : positionList) {
			DSApi.context().begin();
			Long reservationId = position.getBdRezerwacjaId();
			String budgetIdm = position.getBudzetIdm();
			
			/* pobranie id okresu budzetowego */
            Number budgetPhaseId = (Number) DSApi.context().session().createSQLQuery("SELECT top 1 id FROM dsg_ams_view_budget_phase_no_ref WHERE cn = :budgetIdm")
				.setParameter("budgetIdm", budgetIdm)
				.uniqueResult();
			
			/* pobranie rezerwacji*/
			RequestForReservation reservation = (RequestForReservation) DSApi.context().session().createCriteria(RequestForReservation.class)
				.add(Restrictions.eq("id", reservationId))
				.uniqueResult();
			
			if (reservation != null) {
				Object[] result = null;
				Long budgetTypeId = null;
				/* planowane */
				if (StringUtils.isNotBlank(reservation.getBdPlanZamIdm()) && StringUtils.isNotBlank(reservation.getPzpNazwa())) {
					String bdPlanZamIdm = reservation.getBdPlanZamIdm();
					String pzpNazwa = reservation.getPzpNazwa();
					
					result = (Object[]) DSApi.context().session().createSQLQuery("SELECT top 1 id, refValue FROM dsg_ams_view_purchase_plan WHERE cn = :bdPlanZamIdm")
						.setParameter("bdPlanZamIdm", bdPlanZamIdm)
						.uniqueResult();
					
					budgetTypeId = 2L;
				/* nieplanowane */
				} else if (StringUtils.isNotBlank(reservation.getBdBudzetKoIdm()) && StringUtils.isNotBlank(reservation.getBkNazwa())) {
					String bdBudzetKoIdm = reservation.getBdBudzetKoIdm();
					String bkNazwa = reservation.getBkNazwa();
					
					result = (Object[]) DSApi.context().session().createSQLQuery("SELECT top 1 id, refValue FROM dsg_ams_view_budget_unit_ko WHERE cn = :bdBudzetKoIdm")
							.setParameter("bdBudzetKoIdm", bdBudzetKoIdm)
							.uniqueResult();
					
					budgetTypeId = 1L;
				}
				
				String financialTaskId = "" + reservation.getZadanieId().longValue() + ExternalSourceEnumItem.SPLIT_CHAR + reservation.getZadanieIdn() + ExternalSourceEnumItem.SPLIT_CHAR + "TITLE";
				
				if (result != null) {
					MPKDictionaryItem mpkItem = null;
					if (budgetTypeId.equals(new Long(2))) {
						String budgetPositionId = "" + reservation.getBdRodzajPlanZamId().longValue() + ExternalSourceEnumItem.SPLIT_CHAR + reservation.getBdRodzajPlanZamId().longValue() + ExternalSourceEnumItem.SPLIT_CHAR + "TITLE";
						mpkItem = new MPKDictionaryItem(index, budgetPhaseId.longValue(), budgetTypeId, ((Number)result[1]).longValue(), ((Number)result[0]).longValue(), null, budgetPositionId, null, financialTaskId, null);
						//mpkItem = new MPKDictionaryItem(index, budgetPhaseId.longValue(), budgetTypeId, ((BigDecimal)result[1]).longValue(), ((BigDecimal)result[0]).longValue(), null, null, null, null, null);
					} else {
						String budgetPositionId = "" + reservation.getBdRodzajKoId().longValue() + ExternalSourceEnumItem.SPLIT_CHAR + reservation.getBdRodzajKoId().longValue() + ExternalSourceEnumItem.SPLIT_CHAR + "TITLE";
						mpkItem = new MPKDictionaryItem(index, budgetPhaseId.longValue(), budgetTypeId, ((Number)result[1]).longValue(), null, ((Number)result[0]).longValue(), null, budgetPositionId, null, financialTaskId);
						//mpkItem = new MPKDictionaryItem(index, budgetPhaseId.longValue(), budgetTypeId, ((BigDecimal)result[1]).longValue(), null, ((BigDecimal)result[0]).longValue(), null, null, null, null);
					}
					Persister.create(mpkItem);
					DSApi.context().commit();
					
					mpkValues.put(MPK_FIELD + "_" + ID + "_" + index, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, mpkItem.getId()));
				}
			}
			
//			Long bdBudzetRodzajId = null;
//			if (position.getBdRodzajKoId() != null) {
//				bdBudzetRodzajId = position.getBdRodzajKoId();
//			} else if (position.getBdRodzajPlanZamId() != null) {
//				bdBudzetRodzajId = position.getBdRodzajPlanZamId();
//			}
//			List<BudgetView> budgetsList = (List<BudgetView>) DSApi.context().session().createCriteria(BudgetView.class)
//    				.add(Restrictions.eq("bdBudzetRodzajId", bdBudzetRodzajId.doubleValue()))
//    				.list();
//			BudgetView firstBudgetFromList = budgetsList.get(0);
//			Integer budgetCellId = (Integer) DSApi.context().session().createSQLQuery("SELECT ID FROM dsg_ams_view_budget_cell WHERE cn = :cnFromBudget")
//				.setParameter("cnFromBudget", firstBudgetFromList.getDokBdStrBudzetIdn())
//				.uniqueResult();
//			
//			/*Integer budgetCellId = (Integer) DSApi.context()
//			 * .session()
//			 * .createSQLQuery("SELECT ID FROM dsg_ams_view_budget_cell WHERE cn = :cnFromBudget and refValue = :budgetType")
//    				.setParameter("budgetType", id typu budzetu)
//    				.setParameter("cnFromBudget", firstBudgetFromList.getDokBdStrBudzetIdn())
//    				.uniqueResult();
//*/
//
//			//BigDecimal budgetTypeId =BK -> 1, PZP -> 2
///*        			BigDecimal budgetTypeId = (BigDecimal) DSApi.context().session().createSQLQuery("SELECT ID FROM dsg_ams_view_budget_kind WHERE refValue = :budgetCellId AND title = :bdRodzaj")
//				.setParameter("budgetCellId", budgetCellId)
//				.setParameter("bdRodzaj", firstBudgetFromList.getBdRodzaj())
//				.uniqueResult();*/
//			
//			Long budgetTypeId = null;
//			if (firstBudgetFromList.getBdRodzaj().equals("BK")) {
//				budgetTypeId = 1L;
//			} else if (firstBudgetFromList.getBdRodzaj().equals("PZB")) {
//				budgetTypeId = 2L;
//			}
//			
//			MPKDictionaryItem mpkItem = new MPKDictionaryItem(position.getNrpoz(), firstBudgetFromList.getBudzetId().longValue(), budgetCellId.longValue(), budgetTypeId, firstBudgetFromList.getBdBudzetId().longValue(), bdBudzetRodzajId, null, null, null, null, null, null);
//			Persister.create(mpkItem);
//			
//			DSApi.context().commit();
//			mpkValues.put(MPK_FIELD + "_" + ID + "_" + index, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, mpkItem.getId()));
//			
//			/* uzupelnienie wpisow w dekretach */
//			DSApi.context().begin();
//			BigDecimal productId = (BigDecimal) DSApi.context().session().createSQLQuery("SELECT id FROM dsg_ams_view_product WHERE cn = :wytworIdm")
//				.setParameter("wytworIdm", position.getWytwor_idm())
//				.uniqueResult();
//			DekretyDictionaryItem dekretyItem = new DekretyDictionaryItem(position.getNrpoz(), null, productId.longValue(), null, null, null, null, null, null, null, null, null, null, null, null);
//			Persister.create(dekretyItem);
//			
//			DSApi.context().commit();
//			dekretyIds.add(dekretyItem.getId());
			
			index++;
		}
		values.get(DWR_MPK).setDictionaryData(mpkValues);
		//fm.getDocumentKind().setOnly(fm.getDocumentId(), Collections.singletonMap(DEKRETY_FIELD, dekretyIds));
    }
    
    private List<Long> getRequestForReservationIds(Map<String, FieldData> values) {
    	List<Long> reservationIds = new ArrayList<Long>();
    	if (values.containsKey(DWR_WNIOSKI_O_REZERWACJE) && values.get(DWR_WNIOSKI_O_REZERWACJE) != null) {
    		Map<String, FieldData> reservationDictionary = ((FieldData) values.get(DWR_WNIOSKI_O_REZERWACJE)).getDictionaryData();
    		for (String key : reservationDictionary.keySet()) {
    			if (key.contains(WNIOSKI_O_REZERWACJE_REZERWACJA_IDM)) {
    				String selectedId = reservationDictionary.get(key).getEnumValuesData().getSelectedId();
    				if (StringUtils.isNotBlank(selectedId)) {
    					reservationIds.add(Long.valueOf(selectedId));
    				}
    			}
    		}
    	}
    	return reservationIds;
    }
    
    private void setReservationPositions(FieldsManager fm, Map<String, FieldData> values, List<Long> reservationIds) {
    	Map<String, FieldData> positionValues = new LinkedHashMap<String, FieldData>();
    	try {
	    	DSApi.openAdmin();
	    	List<RequestForReservation> requestsForReservationList = (List<RequestForReservation>) DSApi.context().session().createCriteria(RequestForReservation.class)
	    		.add(Restrictions.in("id", reservationIds))
	    		.list();
	    	if (!requestsForReservationList.isEmpty()) {
	    		int index = 1;
	    		List<Long> ids = new ArrayList<Long>();
	    		for (RequestForReservation reservationItem : requestsForReservationList) {
	    			DSApi.context().begin();
	    			AMSProduct product = (AMSProduct) DSApi.context().session().createCriteria(AMSProduct.class)
	        				.add(Restrictions.eq("erpId", reservationItem.getWytworId()))
	        				.uniqueResult();
	    			
	    			String productIdm = null;
	    			String productName = null;
	    			if (product != null) {
	    				productIdm = product.getIdm();
	    				productName = product.getNazwa();
	    			} 
	    			ReservationPositionDictionaryItem reservationPosition = new ReservationPositionDictionaryItem(reservationItem.getBdRezerwacjaIdm(), reservationItem.getNrpoz(), productIdm, productName, reservationItem.getIlosc(), reservationItem.getCena(), null, reservationItem.getKoszt(), null, null);
	    			Persister.create(reservationPosition);
	    			DSApi.context().commit();
	    			positionValues.put(POZYCJE_REZERWACJI_FIELD + "_" + ID + "_" + index, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, reservationPosition.getId()));
	    			ids.add(reservationPosition.getId());
	    			index++;
	    		}
	    		values.get(DWR_POZYCJE_REZERWACJI).setDictionaryData(positionValues);
	    		//fm.getDocumentKind().setOnly(fm.getDocumentId(), Collections.singletonMap(POZYCJE_REZERWACJI_FIELD, ids));
	    		DSApi.close();
	    	}
    	} catch (EdmException e) {
    		log.error(e.getMessage(), e);
    	} catch (Exception e) {
    		log.error(e.getMessage(), e);
    	}
    }
    
    private void setFieldsInMPKDictionaryAndDekretyDictionaryForReservations(FieldsManager fm, List<Long> reservationIds, Map<String, FieldData> values) {
    	Map<String, FieldData> mpkValues = new LinkedHashMap<String, FieldData>();
    	List<Long> dekretyIds = new ArrayList<Long>();
    	try {
	    	DSApi.openAdmin();
			
	    	/* pobranie listy wnioskow o rezerwacje */
	    	List<RequestForReservation> reservationPositions = (List<RequestForReservation>) DSApi.context().session().createCriteria(RequestForReservation.class)
					.add(Restrictions.in("id", reservationIds))
					.list();
			
			int index = 1;
			for (RequestForReservation reservation : reservationPositions) {
				Object[] result = null;
				Long budgetTypeId = null;
				String budgetIdm = reservation.getBudzetIdm();
				
				/* pobranie id okresu budzetowego */
				Number budgetPhaseId = (Number) DSApi.context().session().createSQLQuery("SELECT top 1 id FROM dsg_ams_view_budget_phase_no_ref WHERE cn = :budgetIdm")
					.setParameter("budgetIdm", budgetIdm)
					.uniqueResult();
				
				/* planowane */
				if (StringUtils.isNotBlank(reservation.getBdPlanZamIdm()) && StringUtils.isNotBlank(reservation.getPzpNazwa())) {
					String bdPlanZamIdm = reservation.getBdPlanZamIdm();
					String pzpNazwa = reservation.getPzpNazwa();
					
					result = (Object[]) DSApi.context().session().createSQLQuery("SELECT top 1 id, refValue FROM dsg_ams_view_purchase_plan WHERE cn = :bdPlanZamIdm")
						.setParameter("bdPlanZamIdm", bdPlanZamIdm)
						.uniqueResult();
					
					budgetTypeId = 2L;
				/* nieplanowane */
				} else if (StringUtils.isNotBlank(reservation.getBdBudzetKoIdm()) && StringUtils.isNotBlank(reservation.getBkNazwa())) {
					String bdBudzetKoIdm = reservation.getBdBudzetKoIdm();
					String bkNazwa = reservation.getBkNazwa();
					
					result = (Object[]) DSApi.context().session().createSQLQuery("SELECT top 1 id, refValue FROM dsg_ams_view_budget_unit_ko WHERE cn = :bdBudzetKoIdm")
							.setParameter("bdBudzetKoIdm", bdBudzetKoIdm)
							.uniqueResult();
					
					budgetTypeId = 1L;
				}
				
				String financialTaskId = "" + reservation.getZadanieId().longValue() + ExternalSourceEnumItem.SPLIT_CHAR + reservation.getZadanieIdn() + ExternalSourceEnumItem.SPLIT_CHAR + "TITLE";
				
				if (result != null) {
					DSApi.context().begin();
					MPKDictionaryItem mpkItem = null;
					if (budgetTypeId.equals(new Long(2))) {
						String budgetPositionId = "" + reservation.getBdRodzajPlanZamId().longValue() + ExternalSourceEnumItem.SPLIT_CHAR + reservation.getBdRodzajPlanZamId().longValue() + ExternalSourceEnumItem.SPLIT_CHAR + "TITLE";
						mpkItem = new MPKDictionaryItem(index, budgetPhaseId.longValue(), budgetTypeId, ((Number)result[1]).longValue(), ((Number)result[0]).longValue(), null, budgetPositionId, null, financialTaskId, null);
						//mpkItem = new MPKDictionaryItem(index, budgetPhaseId.longValue(), budgetTypeId, ((BigDecimal)result[1]).longValue(), ((BigDecimal)result[0]).longValue(), null, null, null, null, null);
					} else {
						String budgetPositionId = "" + reservation.getBdRodzajKoId().longValue() + ExternalSourceEnumItem.SPLIT_CHAR + reservation.getBdRodzajKoId().longValue() + ExternalSourceEnumItem.SPLIT_CHAR + "TITLE";
						mpkItem = new MPKDictionaryItem(index, budgetPhaseId.longValue(), budgetTypeId, ((Number)result[1]).longValue(), null, ((Number)result[0]).longValue(), null, budgetPositionId, null, financialTaskId);
						//mpkItem = new MPKDictionaryItem(index, budgetPhaseId.longValue(), budgetTypeId, ((BigDecimal)result[1]).longValue(), null, ((BigDecimal)result[0]).longValue(), null, null, null, null);
					}
					Persister.create(mpkItem);
					DSApi.context().commit();
					
					mpkValues.put(MPK_FIELD + "_" + ID + "_" + index, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, mpkItem.getId()));
				}
				
				
//				Double bdBudzetRodzajId = null;
//				if (positionItem.getBdRodzajKoId() != null && !positionItem.getBdRodzajKoId().equals(0.00)) {
//					bdBudzetRodzajId = positionItem.getBdRodzajKoId();
//				} else if (positionItem.getBdRodzajPlanZamId() != null && !positionItem.getBdRodzajPlanZamId().equals(0.00)) {
//					bdBudzetRodzajId = positionItem.getBdRodzajPlanZamId();
//				}
//				List<BudgetView> budgetsList = (List<BudgetView>) DSApi.context().session().createCriteria(BudgetView.class)
//						.add(Restrictions.eq("bdBudzetRodzajId", bdBudzetRodzajId))
//						.list();
//				Criteria crit = DSApi.context().session().createCriteria(BudgetView.class);
//				crit.add(Restrictions.eq("bdBudzetRodzajId", bdBudzetRodzajId));
//				if (StringUtils.isNotBlank(positionItem.getZadanieIdn())) {
//					crit.add(Restrictions.eq("bdZadanieIdn", positionItem.getZadanieIdn()));
//				}
//				List<BudgetView> budgetsList = crit.list();
//				if (!budgetsList.isEmpty()) {
//					BudgetView firstBudgetFromList = budgetsList.get(0);
//					Integer budgetCellId = (Integer) DSApi.context().session().createSQLQuery("SELECT ID FROM dsg_ams_view_budget_cell WHERE cn = :cnFromBudget")
//						.setParameter("cnFromBudget", firstBudgetFromList.getDokBdStrBudzetIdn())
//						.uniqueResult();
//					
					/*Integer budgetCellId = (Integer) DSApi.context()
					 * .session()
					 * .createSQLQuery("SELECT ID FROM dsg_ams_view_budget_cell WHERE cn = :cnFromBudget and refValue = :budgetType")
							.setParameter("budgetType", id typu budzetu)
							.setParameter("cnFromBudget", firstBudgetFromList.getDokBdStrBudzetIdn())
							.uniqueResult();
				*/
				
					//BigDecimal budgetTypeId =BK -> 1, PZP -> 2
				/*        			BigDecimal budgetTypeId = (BigDecimal) DSApi.context().session().createSQLQuery("SELECT ID FROM dsg_ams_view_budget_kind WHERE refValue = :budgetCellId AND title = :bdRodzaj")
						.setParameter("budgetCellId", budgetCellId)
						.setParameter("bdRodzaj", firstBudgetFromList.getBdRodzaj())
						.uniqueResult();*/
					
//					Long budgetTypeId = null;
//					if (firstBudgetFromList.getBdRodzaj().equals("BK")) {
//						budgetTypeId = 1L;
//					} else if (firstBudgetFromList.getBdRodzaj().equals("PZB")) {
//						budgetTypeId = 2L;
//					}
					
//					DSApi.context().begin();
//					MPKDictionaryItem mpkItem = new MPKDictionaryItem(index, firstBudgetFromList.getBudzetId().longValue(), budgetCellId.longValue(), budgetTypeId, firstBudgetFromList.getBdBudzetId().longValue(), bdBudzetRodzajId.longValue(), firstBudgetFromList.getId(), null, null, null, null, null, null);
//					Persister.create(mpkItem);
//					
//					DSApi.context().commit();
//					mpkValues.put(MPK_FIELD + "_" + ID + "_" + index, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, mpkItem.getId()));
//				}
//				/* uzupelnianie wpisow w slowniku z dekretami */
//				DSApi.context().begin();
//				BigDecimal productIdTemp = (BigDecimal) DSApi.context().session().createSQLQuery("SELECT id FROM dsg_ams_product WHERE erpId = :wytworId")
//						.setParameter("wytworId", positionItem.getWytworId())
//						.uniqueResult();
//				Long productId = null;
//				if (productIdTemp != null) {
//					productId = productIdTemp.longValue();
//				}
//				DekretyDictionaryItem dekretyItem = new DekretyDictionaryItem(index, null, productId, null, null, null, null, null, null, null, null, null, null, null, null);
//				Persister.create(dekretyItem);
//				DSApi.context().commit();
//				dekretyIds.add(dekretyItem.getId());
				
				index++;
			}
			values.get(DWR_MPK).setDictionaryData(mpkValues);
			//fm.getDocumentKind().setOnly(fm.getDocumentId(), Collections.singletonMap(DEKRETY_FIELD, dekretyIds));
			DSApi.close();
    	} catch (EdmException e) {
    		log.error(e.getMessage(), e);
    	}
	}
	

    private boolean IsMPKDictionaryEmpty(Map<String, FieldData> values) {
		boolean isEmpty = true;
		if (values.containsKey(DWR_MPK) && values.get(DWR_MPK) != null) {
			Map<String, FieldData> mpkDictionary = ((FieldData) values.get(DWR_MPK)).getDictionaryData();
			if (mpkDictionary.containsKey("MPK_ID_1") && ((FieldData) mpkDictionary.get("MPK_ID_1")).getData() != null) {
				isEmpty = false;
			}
		} else {
			isEmpty = false;
		}
		return isEmpty;
    }
    
    private boolean IsDekretyDictionaryEmpty(Map<String, FieldData> values) {
    	boolean isEmpty = true;
    	if (values.containsKey(DWR_DEKRETY) && values.get(DWR_DEKRETY) != null) {
    		Map<String, FieldData> mpkDictionary = ((FieldData) values.get(DWR_DEKRETY)).getDictionaryData();
    		if (mpkDictionary.containsKey("DEKRETY_ID_1") && ((FieldData) mpkDictionary.get("DEKRETY_ID_1")).getData() != null) {
    			isEmpty = false;
    		}
    	} else {
    		isEmpty = false;
    	}
    	return isEmpty;
    }
    
    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues, Long documentId) {
    	Map<String, Object> result = Maps.newHashMap();
    	try {
	    	if (dictionaryName.equals(DEKRETY_FIELD)) {
                calculateTaxGrossAmounts(values, result);
                calculateVatToDeduct(values, fieldsValues, result);
	    		calculateVatCosts(values, result);
                fillRepositoryDictionaries(values, result);
	    	}
	    	if(dictionaryName.equals(MPK_FIELD))
	    	{
	    		String budgetTypeId = null;
	    		if (values.containsKey("MPK_RODZAJ_BUDZETU") && values.get("MPK_RODZAJ_BUDZETU") != null) {
	    			budgetTypeId = values.get("MPK_RODZAJ_BUDZETU").getEnumValuesData().getSelectedId();
	    		}
	    		
	    		/* przechodzenie na nowe slowniki */
	    		DwrUtils.setRefValueEnumOptions(values, result, "OKRES_BUDZETOWY", dictionaryName + "_", "KOMORKA_BUDZETOWA", "dsg_ams_view_budget_unit_no_ref", null, EMPTY_NOT_HIDDEN_ENUM_VALUE, "_1", "_1", null, DOC_KIND_CN);
                if (PLANNED_BUDGET.equals(budgetTypeId)) {
                	DwrUtils.setRefValueEnumOptions(values, result, "KOMORKA_BUDZETOWA", dictionaryName + "_", "IDENTYFIKATOR_PLANU", "dsg_ams_view_purchase_plan", null, EMPTY_NOT_HIDDEN_ENUM_VALUE, "_1", "_1", null, DOC_KIND_CN);
                	reloadRefEnumItems(values, result, dictionaryName, "POZYCJA_PLANU_ZAKUPOW_RAW", "IDENTYFIKATOR_PLANU", DOC_KIND_CN, EMPTY_NOT_HIDDEN_ENUM_VALUE, null);
                	reloadRefEnumItems(values, result, dictionaryName, "ZADANIE_FINANSOWE_PLANU_RAW", "POZYCJA_PLANU_ZAKUPOW_RAW", DOC_KIND_CN, EMPTY_NOT_HIDDEN_ENUM_VALUE, null);
	                result.put("MPK_BUDZET", EMPTY_ENUM_VALUE);
	                result.put("MPK_POZYCJA_BUDZETU_RAW", EMPTY_ENUM_VALUE);
	                result.put("MPK_ZADANIE_FINANSOWE_BUDZETU_RAW", EMPTY_ENUM_VALUE);
                }
                if (UNPLANNED_BUDGET.equals(budgetTypeId)) {
                	DwrUtils.setRefValueEnumOptions(values, result, "KOMORKA_BUDZETOWA", dictionaryName + "_", "BUDZET", "dsg_ams_view_budget_unit_ko", null, EMPTY_NOT_HIDDEN_ENUM_VALUE, "_1", "_1", null, DOC_KIND_CN);
	                reloadRefEnumItems(values, result, dictionaryName, "POZYCJA_BUDZETU_RAW", "BUDZET", DOC_KIND_CN, EMPTY_NOT_HIDDEN_ENUM_VALUE, null);
	                reloadRefEnumItems(values, result, dictionaryName, "ZADANIE_FINANSOWE_BUDZETU_RAW", "POZYCJA_BUDZETU_RAW", DOC_KIND_CN, EMPTY_NOT_HIDDEN_ENUM_VALUE, null);
	                result.put("MPK_IDENTYFIKATOR_PLANU", EMPTY_ENUM_VALUE);
	                result.put("MPK_POZYCJA_PLANU_ZAKUPOW_RAW", EMPTY_ENUM_VALUE);
	                result.put("MPK_ZADANIE_FINANSOWE_PLANU_RAW", EMPTY_ENUM_VALUE);
                }
                
	    		/* w przypadku testu */
//	    		DwrUtils.setRefValueEnumOptions(values, result, "RODZAJ_BUDZETU", dictionaryName + "_", "KOMORKA_BUDZETOWA", TABLENAME_WITH_BUDGET_CELL, null, EMPTY_ENUM_VALUE, "_1","_1");
//	    		DwrUtils.setRefValueEnumOptions(values, result, "KOMORKA_BUDZETOWA", dictionaryName + "_", "IDENTYFIKATOR_PLANU", TABLENAME_WITH_BUDGET_IDENTIFIER, null, EMPTY_ENUM_VALUE, "_1", "_1");
//	    		DwrUtils.setRefValueEnumOptions(values, result, "IDENTYFIKATOR_PLANU", dictionaryName + "_", "POZYCJA_PLANU_ZAKUPOW", TABLENAME_WITH_BUDGET_POSITION, null, EMPTY_ENUM_VALUE, "_1", "_1");
//	    		DwrUtils.setRefValueEnumOptions(values, result, "POZYCJA_PLANU_ZAKUPOW", dictionaryName + "_", "ZADANIE_FINANSOWE", "dsg_ams_view_budget_task", null, EMPTY_ENUM_VALUE, "_1","_1");
	    		
	    		/* w przypadku produkcji */
//	    		DwrUtils.setRefValueEnumOptions(values, result, "KOMORKA_BUDZETOWA", dictionaryName + "_", "RODZAJ_BUDZETU", "dsg_ams_view_budget_kind", null, EMPTY_ENUM_VALUE, "_1","_1");
//				DwrUtils.setRefValueEnumOptions(values, result, "RODZAJ_BUDZETU", dictionaryName + "_", "IDENTYFIKATOR_PLANU", "dsg_ams_view_budget", null, EMPTY_ENUM_VALUE, "_1","_1");
//				DwrUtils.setRefValueEnumOptions(values, result, "IDENTYFIKATOR_PLANU", dictionaryName + "_", "POZYCJA_PLANU_ZAKUPOW", "dsg_ams_view_budget_position", null, EMPTY_ENUM_VALUE, "_1","_1");
//				DwrUtils.setRefValueEnumOptions(values, result, "POZYCJA_PLANU_ZAKUPOW", dictionaryName + "_", "ZADANIE_FINANSOWE", "dsg_ams_view_budget_task", null, EMPTY_ENUM_VALUE, "_1","_1");
				
				calculateProposedAmount(values, fieldsValues, result);
            }
	    	if (dictionaryName.equals(ZALICZKA_DICT_FIELD)) {
	    		DwrUtils.setRefValueEnumOptions(values, result, "IMIE_NAZWISKO", dictionaryName + "_", "NR_ZALICZKI", "dsg_ams_view_financial_document", null, EMPTY_ENUM_VALUE, "_1","_1");
	    	}
    	} catch (EdmException e) {
    		log.error(e.getMessage(), e);
    	}
    	return result;
    }
    
    void calculateTaxGrossAmounts(Map<String, FieldData> values, Map<String, Object> result) throws EdmException {
        if (values.containsKey(DEKRETY_STAWKA) && values.get(DEKRETY_STAWKA).getData() != null && values.containsKey(DEKRETY_NETTO) && values.get(DEKRETY_NETTO).getData() != null && values.get(DEKRETY_BRUTTO).getData() == null) {
            int rateId = Integer.parseInt(values.get(DEKRETY_STAWKA).getEnumValuesData().getSelectedId());
            BigDecimal amount = values.get(DEKRETY_NETTO).getMoneyData();
            BigDecimal vatAmount = calculateVatAmount(rateId, amount);
            BigDecimal grossAmount = amount.add(vatAmount);
            result.put(DEKRETY_KWOTA_VAT, vatAmount);
            result.put(DEKRETY_BRUTTO, grossAmount);
            //values.put(DEKRETY_BRUTTO, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.MONEY, grossAmount));
        }
    }

    protected Set<String> getRepositoryDictsIds(Integer docTypeObjectId, Integer contextId) {
        List<SimpleRepositoryAttribute> repositoryDicts = CacheHandler.getRepositoryDicts(docTypeObjectId, contextId);
        Set<String> repositoryDictsIds = Sets.newHashSet();
        for (SimpleRepositoryAttribute repositoryDict : repositoryDicts) {
            repositoryDictsIds.add(repositoryDict.getRepKlasaId().toString());
        }
        return repositoryDictsIds;
    }

    private Integer getDocTypeId() {
        WebContext dwrFactory = WebContextFactory.get();
        if (dwrFactory.getSession().getAttribute("dwrSession") instanceof Map<?, ?>) {
            Map<String, Object> dwrSession = (Map<String, Object>) dwrFactory.getSession().getAttribute("dwrSession");
            if (dwrSession.get("FIELDS_VALUES") instanceof Map<?, ?>) {
                Map<String,Object> fieldsValues = (Map<String, Object>) dwrSession.get("FIELDS_VALUES");

                Object docType = fieldsValues.get(TYP_FAKTURY_FIELD);
                if (docType != null) {
                    try {
                        return Integer.valueOf(docType.toString());
                    } catch (NumberFormatException e) {
                    }
                }
            }
        }
        return null;
    }

    private void fillRepositoryDictionaries(Map<String, FieldData> values, Map<String, Object> results) throws EdmException {
        Integer docTypeId = getDocTypeId();

        if (docTypeId != null) {
            EnumItem docTypeEnum = DataBaseEnumField.getEnumItemForTable(PurchaseDocumentType.ENUM_VIEW_TABLE, docTypeId);

            clearAllDictionaresFields(DOC_KIND_CN, results);

            if (docTypeEnum != null && docTypeEnum.getRefValue() != null) {
                Integer docTypeObjectId = Ints.tryParse(docTypeEnum.getRefValue());

                Set<String> repositoryDictsIds = getRepositoryDictsIds(docTypeObjectId, (int) REPOSITORY_POSITION_CONTEXT_ID);

                for (Dictionary dict : CacheHandler.getAvailableDicts(DOC_KIND_CN)) {
                    if (!dict.getDeleted() && dict.getDictSource() == Dictionary.SourceType.REPOSITORY.value) {
                        setRepositoryDict(values, results, DEKRETY_WITH_SUFFIX_FIELD, docTypeObjectId, (int) REPOSITORY_POSITION_CONTEXT_ID, repositoryDictsIds,
                                null, EMPTY_ENUM_VALUE, dict, log);
                    }
                }
            }
        }
    }

    private void clearAllDictionaresFields(String docKindCn, Map<String, Object> results) {
        for (Dictionary dictionaryToClear : CacheHandler.getAvailableDicts(docKindCn)) {
            results.put(DEKRETY_WITH_SUFFIX_FIELD + dictionaryToClear.getCn(), EMPTY_ENUM_VALUE);
        }
    }

    protected void setRepositoryDict(Map<String, FieldData> values, Map<String, Object> results, String dicNameWithSuffix,
                                     Integer docTypeObjectId, Integer contextId, Set<String> repositoryDictsIds, DictionaryMap dictMap, EnumValues defaultEnum, Dictionary dict, Logger log) {
        if (dictMap != null && dictMap.isShowAllReferenced()) {
            DwrUtils.setRefValueEnumOptions(values, results, DataBaseEnumField.REPO_REF_FIELD_PREFIX + dict.getCn(), dicNameWithSuffix, dict.getCn(),
                    dict.getTablename(), DataBaseEnumField.REF_ID_FOR_ALL_FIELD, defaultEnum);
        } else {
            if (repositoryDictsIds.contains(dict.getCode())) {
                try {
                    Long dictObjectId = Long.valueOf(dict.getCode());
                    SimpleRepositoryAttribute attr = CacheHandler.getRepostioryDict(dictObjectId, docTypeObjectId, contextId);
                    if (attr != null) {
                        DwrUtils.setRefValueEnumOptions(values, results, REPO_REF_FIELD_PREFIX + dict.getCn(), dicNameWithSuffix, dict.getCn(),
                                dict.getTablename(), attr.getRepAtrybutId().toString(), defaultEnum);
                    }
                } catch (NumberFormatException e) {
                    log.error("CAUGHT: "+e.getMessage(),e);
                }
            }
        }
    }
    
    @Override
	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException
	{
    	FieldsManager fm = doc.getFieldsManager();
    	Integer status = (Integer) fm.getKey(STATUS_FIELD);
    	
    	if (status != null && status.equals(ESSENTIAL_DESCRIPTION_STATUS)) {
    		if (isInstallmentsSumBiggerThanGrossAmount(fm)) {
    			throw new EdmException(sm.getString("InstallmentsAreBiggerThanGrossAmount"));
    		}
    		if (isMPKAmountSumEqualsGrossAmount(fm) != null && !isMPKAmountSumEqualsGrossAmount(fm)) {
    			throw new EdmException(sm.getString("MpkAmountsAreNotEqualsGrossAmount"));
    		}
    	}
    	if (status != null && status.equals(ACCOUNT_CONTROL_STATUS)) {
    		if (isDekretyAmountSumEqualsGrossAmount(fm) != null && !isDekretyAmountSumEqualsGrossAmount(fm)) {
    			throw new EdmException(sm.getString("DekretyAmountsAreNotEqualsGrossAmount"));
    		}
    	}
	}

    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) throws EdmException
    {
        boolean assigned = false;
        try {
            Long documentId = doc.getId();
            FieldsManager fm = Document.find(documentId).getFieldsManager();
            
            if (REALIZATOR_ACCEPTATION_CN.equalsIgnoreCase(acceptationCn)){
            	String realizator = (String) fm.getValue(RECIPIENT_HERE_FIELD);
            	String[] realizatorArray = realizator.split(",");
            	if(realizatorArray.length > 1){
            		String fullName = realizatorArray[1];
            		String[] firstLastName = fullName.split(" "); 
            		if(DSUser.findByFirstnameLastname(firstLastName[1], firstLastName[2], false) != null){
            			assignable.addCandidateUser(DSUser.findByFirstnameLastname(firstLastName[1], firstLastName[2], false).getName());
        				AssigneeHandler.addToHistory(openExecution, doc, DSUser.findByFirstnameLastname(firstLastName[1], firstLastName[2], false).getName(), null);
        				assigned = true;
            		}
            	} else {
            		if(DSDivision.findByName(realizatorArray[0]) != null){
            			assignable.addCandidateGroup(DSDivision.findByName(realizatorArray[0]).getGuid());
        				AssigneeHandler.addToHistory(openExecution, doc, null, DSDivision.findByName(realizatorArray[0]).getGuid());
        				assigned = true;
            		}
            	}
            } else if (DYSPONENT_ACCEPTATION_CN.equalsIgnoreCase(acceptationCn)) {
            	String trustee = (String) fm.getValue("DYSPONENT");
            	String[] firstLastName = trustee.split(" ");
        		if(DSUser.findByFirstnameLastname(firstLastName[1], firstLastName[0], false) != null) {
        			assignable.addCandidateUser(DSUser.findByFirstnameLastname(firstLastName[1], firstLastName[0], false).getName());
    				AssigneeHandler.addToHistory(openExecution, doc, DSUser.findByFirstnameLastname(firstLastName[1], firstLastName[0], false).getName(), null);
    				assigned = true;
        		}
            } else if(CONFIRMATION_JW_ACCEPTATION_CN.equalsIgnoreCase(acceptationCn) || COMPATIBILITY_VERIFICATION_ACCEPTATION_CN.equalsIgnoreCase(acceptationCn) || COMPATIBILITY_ACCEPTATION_ACCEPTATION_CN.equalsIgnoreCase(acceptationCn)) {
            	Long userId = (Long) openExecution.getVariable(USER_ID_JBPM4_VARIABLE);
            	assigned = assignUser(doc, assignable, openExecution, userId);
            } else if (MAGAZYNIER_ACCEPTATION_CN.equalsIgnoreCase(acceptationCn)) {
            	long warehouseId = ((Integer) fm.getKey(MAGAZYN_FIELD)).longValue();
            	DSUser warehouseman = findWarehouseman(warehouseId);
            	
            	assignable.addCandidateUser(warehouseman.getName());
				AssigneeHandler.addToHistory(openExecution, doc, warehouseman.getName(), null);
				assigned = true;
            } else if (IMPLEMENTING_CELL_SUPERVISOR_ACCEPTATION_CN.equalsIgnoreCase(acceptationCn)) {
        		String implementingCell = (String) fm.getValue(RECIPIENT_HERE_FIELD);
        		String[] implementingCellArray = implementingCell.split(",");
            	if (DSDivision.findByName(implementingCellArray[0]) != null ) {
            			String guid = DSDivision.findByName(implementingCellArray[0]).getGuid();
            			String profileId = Docusafe.getAdditionProperty("SUPERVISOR_PROFILE_ID");
            			List<String> userList = (List<String>) DSApi.context().session().createSQLQuery("select DISTINCT u.NAME from DS_DIVISION d join DS_USER_TO_DIVISION ud on d.ID = ud.DIVISION_ID join DS_USER u on ud.USER_ID = u.ID join ds_profile_to_user pu on u.ID = pu.user_id where pu.profile_id = :profileId and d.GUID = :guid and u.DELETED = 0")
            					.setParameter("guid", guid)
            					.setParameter("profileId", profileId)
            					.list();
            			if (!userList.isEmpty()) {
            				log.error(userList.toString());
	            			for (String username : userList) {
	            				assignable.addCandidateUser(username);
	            				AssigneeHandler.addToHistory(openExecution, doc, username, null);
	            				assigned = true;
	            			}
            			} else {
            				//assigned = false;
            				log.error("Nie znaleziono osoby z profilem kierownik dla dzia�u: " + implementingCellArray[0]);
                            throw new EdmException("Nie znaleziono osoby z profilem kierownik dla dzia�u: " + implementingCellArray[0]);
                            //return false;
                            //assignable.addCandidateUser("admin");
            			}
        		}
            }
//            // kasowanie uprawnien do dodawania i usuwania zalacznikow
//            if (assigned == true) {
//            	removePermissionsToAtachments(doc);
//            }
            
            return assigned;
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            throw e;
            //return assigned;
        }
    }
    
    private boolean isInstallmentsSumBiggerThanGrossAmount(FieldsManager fm) {
    	boolean isBigger = false;
    	try {
			BigDecimal installmentsSum = (BigDecimal) fm.getKey(SUMA_RAT_FIELD);
			if (installmentsSum != null) {
				installmentsSum = installmentsSum.setScale(MONEY_SCALE);
				BigDecimal grossAmount = (BigDecimal) fm.getKey(KWOTA_BRUTTO_FIELD);
				grossAmount = grossAmount.setScale(MONEY_SCALE);
				
				BigDecimal advance = (BigDecimal) fm.getKey(ZALICZKA_FIELD);
				if (advance != null) {
					advance = advance.setScale(MONEY_SCALE);
					installmentsSum = installmentsSum.subtract(advance);
				}
				if (installmentsSum.compareTo(grossAmount) == 1) {
					isBigger = true;
				}
			}
    	} catch (EdmException e) {
    		log.error(e.getMessage(), e);
    	}
    	return isBigger;
    }
    
    private boolean isAmountOfRecognitionLessThanAmountGross(Map<String, FieldData> values) {
    	boolean isAmount = false;
    	if (values.containsKey(DWR_KWOTA_UZNANIA) && values.get(DWR_KWOTA_UZNANIA).getData() != null && values.containsKey(DWR_KWOTA_BRUTTO) && values.get(DWR_KWOTA_BRUTTO).getData() != null) {
    		BigDecimal amountOfRecognition = values.get(DWR_KWOTA_UZNANIA).getMoneyData().setScale(2);
    		BigDecimal amountGross = values.get(DWR_KWOTA_BRUTTO).getMoneyData().setScale(2);
    		if (amountOfRecognition.compareTo(amountGross) == -1) {
    			isAmount = true;
    		}
    	}
    	return isAmount;
    }

    private BigDecimal calculateVatAmount(int rateId, BigDecimal amount) throws EdmException {
    	StringBuilder query = new StringBuilder();
    	query.append("SELECT procent FROM ")
    		.append(TABLENAME_WITH_VAT_RATES)
    		.append(" WHERE id = ")
    		.append(rateId);
    	BigDecimal vatRate = ((BigDecimal)DSApi.context().session().createSQLQuery(query.toString()).uniqueResult()).setScale(MONEY_SCALE);
    	vatRate = vatRate.divide(new BigDecimal(100), MONEY_SCALE, RoundingMode.HALF_UP);
    	BigDecimal vatAmount = amount.multiply(vatRate).setScale(MONEY_SCALE, RoundingMode.HALF_UP);
    	return vatAmount;
    }
    
    private DSUser findWarehouseman(long warehouseId) throws EdmException {
    	Warehouse warehouse = (Warehouse) DSApi.context().session().createCriteria(Warehouse.class)
        		.add(Restrictions.eq("id", warehouseId)).uniqueResult();
        	
        	double employeeId = warehouse.getPracownikId();
        	
        	return DSUser.findAllByExtension(String.valueOf(employeeId));
    }
    
    private boolean assignUser(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, Long userId)
            throws EdmException, UserNotFoundException
    {
        boolean assigned = false;
        String userName = DSUser.findById(userId).getName();
        assignable.addCandidateUser(userName);
        AssigneeHandler.addToHistory(openExecution, doc, userName, null);
        assigned = true;
        return assigned;
    }
    
    private Boolean isMPKAmountSumEqualsGrossAmount(FieldsManager fm) {
    	Boolean isEquals = null;
    	try {
			BigDecimal mpkAmountSum = (BigDecimal) fm.getKey(MPK_SUM_AMOUNT_FIELD);
			if (mpkAmountSum != null) {
				mpkAmountSum = mpkAmountSum.setScale(MONEY_SCALE);
				BigDecimal grossAmount = (BigDecimal) fm.getKey(KWOTA_BRUTTO_FIELD);
				grossAmount = grossAmount.setScale(MONEY_SCALE);
				
				if (mpkAmountSum.compareTo(grossAmount) == 0) {
					isEquals = true;
				} else {
					isEquals = false;
				}
			}
    	} catch (EdmException e) {
    		log.error(e.getMessage(), e);
    	}
    	return isEquals;
    }
    
    private Boolean isDekretyAmountSumEqualsGrossAmount(FieldsManager fm) {
    	Boolean isEquals = null;
    	try {
			BigDecimal dekretyAmountSum = (BigDecimal) fm.getKey(DEKRETY_SUM_AMOUNT_FIELD);
			if (dekretyAmountSum != null) {
				dekretyAmountSum = dekretyAmountSum.setScale(MONEY_SCALE);
				BigDecimal grossAmount = (BigDecimal) fm.getKey(KWOTA_BRUTTO_FIELD);
				grossAmount = grossAmount.setScale(MONEY_SCALE);
				
				if (dekretyAmountSum.compareTo(grossAmount) == 0) {
					isEquals = true;
				} else {
					isEquals = false;
				}
			}
    	} catch (EdmException e) {
    		log.error(e.getMessage(), e);
    	}
    	return isEquals;
    }

    public Long getRepositoryAttributeValue(Long postionId, String positionTable, SimpleRepositoryAttribute dict, Logger log) {
        PreparedStatement ps = null;
        Long result = null;
        try {
            ps = DSApi.context().prepareStatement("select centrum from "+ positionTable +" bp join "+dict.getTableName()+" r on bp."+ dict.getTableName() +"=r.id where bp.id = ?");
            ps.setLong(1, postionId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                result = rs.getLong(1) != 0l ? rs.getLong(1) : null;
                if (rs.next()) {
                    log.error("Found more than one repository item to budget position, bp id: "+postionId+", attribute: "+dict.getTableName());
                }
            }
            rs.close();
            ps.close();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        DbUtils.closeQuietly(ps);
        return result;
    }
    
    @Override
    public ExportedDocument buildExportDocument(OfficeDocument doc) throws EdmException {
        boolean processNotApplicableValue = false;
        Integer notApplicableValue = -1;

        PurchasingDocument exported = new PurchasingDocument();
        FieldsManager fm = doc.getFieldsManager();
        //stdost_id
        exported.setDelivery("SD");
        //typdokzak_id
        EnumItem documentTypeItem = fm.getEnumItem(TYP_FAKTURY_FIELD);
        exported.setPurchaseType(documentTypeItem.getCn());
        //dostawca_id
        AMSSupplier contractor = AMSSupplier.find(Long.valueOf(fm.getIntegerKey(SUPPLIER_FIELD)));
        exported.setExternalContractor(contractor.getErpId().longValue());
        //kontobankowe_id
        if(fm.getEnumItem("DOSTAWCA_NR_KONTA_ID") != null)
        {
        	String kontrahent_nr_konta_id = fm.getEnumItem("DOSTAWCA_NR_KONTA_ID").getCn();
        	exported.setContractorAccountId(Integer.valueOf(kontrahent_nr_konta_id.substring(0, kontrahent_nr_konta_id.indexOf("."))));
        } else if (fm.getKey("NR_KONTA_NOWY") != null) {
    		String accountNumber = (String) fm.getKey("NR_KONTA_NOWY");
    		if (StringUtils.isNumeric(accountNumber)) {
    			exported.setContractorAccount(accountNumber);
    		}
        }
        //datdok
        exported.setIssued((Date) fm.getKey(DATA_WYSTAWIENIA_FIELD));
        //dok_uwagi
//        exported.setDescription(getAdditionFieldsDescription(doc, fm));
        //dokobcy
        exported.setCode(fm.getStringValue(NR_FAKTURY_FIELD));
        //datprzyj
        exported.setRecieved(doc.getCtime());

        //dokzakpozycje
        List<Long> pozycjeFaktury = (List) fm.getKey(MPK_FIELD);
        StringBuilder sb = new StringBuilder();
        for (Long id : pozycjeFaktury) {
            sb.append(id);
            sb.append(",");
        }
        String mpkIDs = sb.toString().endsWith(",") ? sb.toString().substring(0, sb.toString().length() - 1) : sb.toString();
        log.error(mpkIDs);

//        LinkedList<LinkedHashMap> pozycjeFaktury = (LinkedList)fm.getValue(MPK_FIELD);
//        System.out.println(obj.getClass());
//        Object ob2 = ((LinkedList)obj).get(0);
//        System.out.println(ob2.getClass());

        List<SimpleRepositoryAttribute> repositoryDicts = CacheHandler.getRepositoryDicts(documentTypeItem.getRefValue() != null ? Ints.tryParse(documentTypeItem.getRefValue()) : null, (int)REPOSITORY_POSITION_CONTEXT_ID);

        LinkedList<LinkedHashMap> dekretyFaktury = (LinkedList) fm.getValue(DEKRETY_FIELD);
        if (dekretyFaktury != null) {
            int i = 1;
            List<BudgetItem> budgetItems = Lists.newArrayList();
            for (LinkedHashMap dekret : dekretyFaktury) {
                BudgetItem bItem = new BudgetItem();
                for (SimpleRepositoryAttribute dict : repositoryDicts) {
                    Long value = null;
                    try {
                        value = getRepositoryAttributeValue(Long.valueOf(dekret.get("id").toString()), DEKRETY_TABLE, dict, log);
                    } catch (RuntimeException e) {
                        log.error(e.getMessage(), e);
                    }
                    if (value != null) {
                        bItem.addRepositoryItem(new RepositoryItem(dict.getRepAtrybutId(), value));
                    }
                }
            	PreparedStatement ps = null;

                BigDecimal numerPozycji = (BigDecimal) dekret.get("DEKRETY_NUMER_POZYCJI");

                //vatstaw_id
                if (dekret.get(DEKRETY_STAWKA) != null) {
                    bItem.setVatRateCode(getCnValue("dsg_ams_view_vat_rate", dekret.get(DEKRETY_STAWKA)));
                }
                //jm_id
                bItem.setUnit("szt");
                //ilosc
                bItem.setQuantity(BigDecimal.ONE);
                //nrpoz
                bItem.setItemNo(i++);
                //rodztowaru
                //bItem.setFixedAsset(false);
                //kwotnett
                bItem.setNetAmount((BigDecimal) dekret.get(DEKRETY_NETTO));
                // projekt_id
//                EnumValues projektEnum = (EnumValues) dekret.get("DEKRETY_PROJEKT");
//                bItem.setBudgetCode(getCnValue("dsg_ams_view_contract", dekret.get("DEKRETY_PROJEKT")));

                //komorka_id
                EnumValues mpkEnum = (EnumValues) dekret.get("DEKRETY_MPK");
                DSDivision selectedDiv = DSDivision.findById(Integer.valueOf(mpkEnum.getSelectedId()));
                bItem.setMpkCode(selectedDiv.getCode());

                //wytwor_id
                bItem.setCostKindCode(getCnValue(PRODUCT_VIEW, dekret.get("DEKRETY_PRODUKT")));
                //przeznzak
                bItem.setSprzedazOpodatkowana(Integer.valueOf(((EnumValues) dekret.get("DEKRETY_SPRZEDAZ_OPODATKOWANA")).getSelectedId()));
                //szablon_id
                if (dekret.get("DEKRETY_PODZIAL_KOSZTOW") != null && StringUtils.isNotBlank(((EnumValues)dekret.get("DEKRETY_PODZIAL_KOSZTOW")).getSelectedId())) {
	                PatternsOfCostSharing pattern = (PatternsOfCostSharing) DSApi.context().session().get(PatternsOfCostSharing.class, Long.valueOf(((EnumValues) dekret.get("DEKRETY_PODZIAL_KOSZTOW")).getSelectedId()));
	                bItem.setTemplateId(new BigDecimal(pattern.getErpId()));
                }
                //rodztowaru
                if (Boolean.TRUE.equals((Boolean) dekret.get("DEKRETY_SRODEK_TRWALY"))) {
                	bItem.setFixedAsset(true); 
                } else {
                	bItem.setFixedAsset(false);
                }
//                //czy_magazynowana
//                if (dekret.get("DEKRETY_MAGAZYNOWANY") != null) {
//                	bItem.setStored((Boolean) dekret.get("DEKRETY_MAGAZYNOWANY"));
//                }

                try {
//                    SQLQuery sqlQuery = DSApi.context().session().createSQLQuery("select * from dsg_ams_mpk_dictionary where pozycjaid = :p and id in (:l)");
//                    sqlQuery.setParameter("p", numerPozycji);
//                    sqlQuery.setParameterList("l", pozycjeFaktury);
//                    sqlQuery.list();

                    ps = DSApi.context().prepareStatement("select * from dsg_ams_mpk_dictionary where pozycjaid = ? and id in (" + mpkIDs + ")");
                    ps.setInt(1, numerPozycji.intValue());
                    ResultSet rs = ps.executeQuery();
                    if (!rs.next())
                        throw new EdmException("Brak linni faktury");

                    if (rs.getInt("rodzaj_budzetu") == 2) {
                        bItem.setPlanZamId(rs.getLong("identyfikator_planu"));
                        String raw = rs.getString("pozycja_planu_zakupow_raw");
                        if (!Strings.isNullOrEmpty(raw)) {

                            bItem.setRodzajPlanZamId(Long.valueOf(raw.split("" + ExternalSourceEnumItem.SPLIT_CHAR)[0]));
                            //bItem.setBudgetTaskCode(rs.getString("zadanie_finansowe_planu_raw").split(""+ExternalSourceEnumItem.SPLIT_CHAR)[1]);
                            raw = rs.getString("zadanie_finansowe_planu_raw");
                            if (!Strings.isNullOrEmpty(raw)) {
                                bItem.addFinanacialTask(
                                        new FinancialTask()
                                                .setTaskCode(raw.split("" + ExternalSourceEnumItem.SPLIT_CHAR)[1])
                                                .setPercent(BigDecimal.ONE.movePointRight(2))
                                                .setAmount(bItem.getNetAmount()));
                            }
                        }
                    }

                    if (rs.getInt("rodzaj_budzetu") == 1) {
                        bItem.setBudgetKoId(rs.getLong("budzet"));
                        String raw = rs.getString("pozycja_budzetu_raw");
                        if (!Strings.isNullOrEmpty(raw)) {
                            bItem.setBudgetKindId(Long.valueOf(raw.split("" + ExternalSourceEnumItem.SPLIT_CHAR)[0]));

                            raw = rs.getString("zadanie_finansowe_budzetu_raw");
                            if (!Strings.isNullOrEmpty(raw)) {
                                bItem.addFinanacialTask(
                                        new FinancialTask()
                                                .setTaskCode(raw.split("" + ExternalSourceEnumItem.SPLIT_CHAR)[1])
                                                .setPercent(BigDecimal.ONE.movePointRight(2))
                                                .setAmount(bItem.getNetAmount()));
                            }
                        }
                    }

                    //FIXME
                    //wtf, OKRES_BUDZETOWY jest puste
                    //bItem.setBudgetDirectCode(getCnValue(VIEW_BUDGET, position.get(MPK_FIELD_WITH_SUFFIX + OKRES_BUDZETOWY)));
                    bItem.setBudgetDirectCode(Docusafe.getAdditionPropertyOrDefault("export.reservation.default_budget_idn", "B/2014/0001"));




                } catch (SQLException e) {
                    log.error(e.getMessage(), e);
                }    finally
                {
                    DbUtils.closeQuietly(ps);
                }

                if (dekret.get("DEKRETY_ZRODLO_FINANSOWANIA") != null) {
                    FundSource fItem = new FundSource();
                    fItem.setFundPercent(new BigDecimal(100));
                    // zrodlo finansowania
                    // zrodlo_id
                    fItem.setCode(getCnValue("dsg_ams_view_funding_source", dekret.get("DEKRETY_ZRODLO_FINANSOWANIA")));

                    bItem.setFundSources(Lists.newArrayList(fItem));
                }


                budgetItems.add(bItem);
            }
            exported.setBudgetItems(budgetItems);
        }

        LinkedList<LinkedHashMap> platnosciRatalne = (LinkedList) fm.getValue(RATY_FIELD_CN);
        if (platnosciRatalne != null && !platnosciRatalne.isEmpty()) {
            List<InstallmentItem> installmentItems = Lists.newArrayList();
            Integer sposobId = (Integer) fm.getKey(SPOSOB_ZAPLATY_FIELD);
            EnumItem enumItem = DataBaseEnumField.getEnumItemForTable("dsg_ams_view_payment_terms", sposobId);
//            String sposobTitle = enumItem.getRefValue();
            Integer sposobZaplaty;
            if (enumItem != null && enumItem.getRefValue() != null) {
                sposobZaplaty = Integer.valueOf(enumItem.getRefValue());
            } else {
                sposobZaplaty = 1;
            }
//            if (sposobTitle != null && (sposobTitle.contains("Przelew") || sposobTitle.contains("przelew"))) {
//                sposobZaplaty = 1;
//            } else {
//                sposobZaplaty = 0;
//            }
            for (LinkedHashMap rata : platnosciRatalne) {
                InstallmentItem iItem = new InstallmentItem();
                iItem.setSposobZaplaty(sposobZaplaty);
                iItem.setKwota((BigDecimal) rata.get(RATY_FIELD + "_KWOTA"));
                iItem.setTerminPlatnosci((Date) rata.get(RATY_FIELD + "_DATA"));

                installmentItems.add(iItem);
            }
            exported.setInstallmentItems(installmentItems);
        }

        String systemPath = null;
        if (Docusafe.getAdditionProperty("erp.faktura.link_do_skanu") != null) {
            systemPath = Docusafe.getAdditionProperty("erp.faktura.link_do_skanu");
        } else {
            systemPath = Docusafe.getBaseUrl();
        }

        for (Attachment zal : doc.getAttachments()) {
            AttachmentInfo info = new AttachmentInfo();
            String path = systemPath + "/repository/view-attachment-revision.do?id=" + zal.getMostRecentRevision().getId();
            info.setName(zal.getTitle());
            info.setUrl(path);
            exported.addAttachementInfo(info);
        }

        return exported;
    }


    public String getCnValue(String tableName, Object enumValues) throws EdmException {
    	if(enumValues == null)
    		return null;
    	String enumId = ((EnumValues)enumValues).getSelectedId();
        if (enumId != null) {
            return DataBaseEnumField.getEnumItemForTable(tableName, Integer.valueOf(enumId)).getCn();
        }
        return null;
    }
    
    private void calculateProposedAmount(Map<String, FieldData> values, Map<String, Object> fieldsValues, Map<String, Object> result) {
    	BigDecimal amount = null;
    	BigDecimal oldGrossAmount = values.get(MPK_KWOTA_BUDZETU).getMoneyData();
    	BigDecimal proposedAmount = (BigDecimal) fieldsValues.get(MPK_KWOTA_DO_ROZPISANIA_FIELD);
    	if (proposedAmount != null) {
    		if (values.get(MPK_KWOTA_BUDZETU).getData() == null) {
    			amount = proposedAmount;
    		} else {
    			amount = oldGrossAmount;
    		}
    	} else {
    		amount = (BigDecimal) fieldsValues.get(KWOTA_BRUTTO_FIELD);
    	}
    	result.put(MPK_KWOTA_BUDZETU, amount);
		// wrzucenie do sesji starej i nowej kwoty brutto aby w metodzie validateDwr je wykorzystac, bo teraz zmiana tej kwoty nie bedzie widoczna w values w validateDwr
//        HttpSession session = WebContextFactory.get().getSession();
//        if (session != null) {
//            session.setAttribute(DEKRETY_BRUTTO + "_OLD", oldGrossAmount);
//            session.setAttribute(DEKRETY_BRUTTO + "_NEW", amount);
//        }
    }
    
    private void calculateVatToDeduct(Map<String, FieldData> values, Map<String, Object> fieldsValues, Map<String, Object> result) {
    	try {
	    	if (result.containsKey(DEKRETY_KWOTA_VAT) && result.get(DEKRETY_KWOTA_VAT) != null) {
	    		BigDecimal vatAmount = (BigDecimal) result.get(DEKRETY_KWOTA_VAT);
	    		
				if (values.get(DEKRETY_SPRZEDAZ_OPODATKOWANA) != null) {
					String selectedId = values.get(DEKRETY_SPRZEDAZ_OPODATKOWANA).getEnumValuesData().getSelectedId();
					if (selectedId.equals(SPRZEDAZ_OPODATKOWANA)) {
						result.put(DEKRETY_KWOTA_VAT_DO_ODLICZENIA, vatAmount);
					} else if (selectedId.equals(SPRZEDAZ_NIE_OPODATKOWANA)) {
						result.put(DEKRETY_KWOTA_VAT_DO_ODLICZENIA, new BigDecimal(0));
					} else if (selectedId.equals(NIE_OKRESLONE)) {
						if (fieldsValues.get(VAT_RATIO_FIELD) != null) {
							Long id = ((Integer) fieldsValues.get(VAT_RATIO_FIELD)).longValue();
							VatRatio vatRatio = (VatRatio) DSApi.context().session().get(VatRatio.class, id);
							BigDecimal vatToDeduct = vatAmount.multiply(new BigDecimal(vatRatio.getWartosc()).divide(new BigDecimal(100), MONEY_SCALE, RoundingMode.HALF_UP)).setScale(MONEY_SCALE, RoundingMode.HALF_UP);
							result.put(DEKRETY_KWOTA_VAT_DO_ODLICZENIA, vatToDeduct);
						}
					}
				}
	    	}
    	} catch (Exception e) {
    		log.error(e.getMessage(), e);
    	}
    }
    
    private void calculateVatCosts(Map<String, FieldData> values, Map<String, Object> result) {
    	if (result.containsKey(DEKRETY_KWOTA_VAT) && result.get(DEKRETY_KWOTA_VAT) != null && result.containsKey(DEKRETY_KWOTA_VAT_DO_ODLICZENIA) && result.get(DEKRETY_KWOTA_VAT_DO_ODLICZENIA) != null) {
    		BigDecimal vatAmount = (BigDecimal) result.get(DEKRETY_KWOTA_VAT);
    		BigDecimal vatToDeduct = (BigDecimal) result.get(DEKRETY_KWOTA_VAT_DO_ODLICZENIA);
    		BigDecimal vatCosts = vatAmount.subtract(vatToDeduct).setScale(MONEY_SCALE, RoundingMode.HALF_UP);
    		result.put(DEKRETY_KWOTA_VAT_KOSZTY, vatCosts);
    	}
    }
    
    private void removePermissionsToAtachments(Document document) {
    	try {
	        FieldsManager fm = document.getFieldsManager();
	        Integer status = (Integer) fm.getKey(STATUS_FIELD);
	        if (status != null && status.compareTo(new Integer(50)) >= 0) {
	        	DSApi.context().session().createSQLQuery("DELETE FROM ds_document_permission WHERE document_id = :documentId AND SUBJECTTYPE LIKE :subjectType AND NAME LIKE :permissionName")
	        		.setParameter("documentId", document.getId())
	        		.setParameter("subjectType", ObjectPermission.USER)
	        		.setParameter("permissionName", ObjectPermission.MODIFY_ATTACHMENTS);
	        	DSApi.context().session().flush();
	        }
    	} catch (EdmException e) {
    		log.error(e.getMessage(), e);
    	}
    }
    
    private void setAcceptances(long docId, Map<String, Object> values, String cellSupervisorTaskName, String projectSupervisorTaskName, String projectAssistantTaskName) throws EdmException, UserNotFoundException {
        List<Acceptance> cellSupervisorAcceptances = new ArrayList<Acceptance>();
        List<Acceptance> projectSupervisorAcceptances = new ArrayList<Acceptance>();
        List<Acceptance> projectAssistantAcceptances = new ArrayList<Acceptance>();

        for (DocumentAcceptance dacc : DocumentAcceptance.find(docId)) {
            DSUser acceptedUser = DSUser.findByUsername(dacc.getUsername());
            if (dacc.getAcceptanceCn().equals(cellSupervisorTaskName)) {
            	Acceptance acc = new Acceptance(acceptedUser.asFirstnameLastname(), DateUtils.formatJsDateTime((dacc.getAcceptanceTime())));
            	cellSupervisorAcceptances.add(acc);
            } else if (dacc.getAcceptanceCn().equals(projectSupervisorTaskName)) {
            	Acceptance acc = new Acceptance(acceptedUser.asFirstnameLastname(), DateUtils.formatJsDateTime((dacc.getAcceptanceTime())));
            	projectSupervisorAcceptances.add(acc);
            } else if (dacc.getAcceptanceCn().equals(projectAssistantTaskName)) {
            	Acceptance acc = new Acceptance(acceptedUser.asFirstnameLastname(), DateUtils.formatJsDateTime((dacc.getAcceptanceTime())));
            	projectAssistantAcceptances.add(acc);
            } else {
				values.put(dacc.getAcceptanceCn(), acceptedUser.getLastname() + " " + acceptedUser.getFirstname());
				values.put(dacc.getAcceptanceCn() + "_DATE", DateUtils.formatCommonDate(dacc.getAcceptanceTime()));
            }
        }
        
        if (!cellSupervisorAcceptances.isEmpty()) {
        	values.put(cellSupervisorTaskName, cellSupervisorAcceptances);
        }
        if (!projectSupervisorAcceptances.isEmpty()) {
        	values.put(projectSupervisorTaskName, projectSupervisorAcceptances);
        }
        if (!projectAssistantAcceptances.isEmpty()) {
        	values.put(projectAssistantTaskName, projectAssistantAcceptances);
        }
    }
    
    void reloadRefEnumItems(Map<String, ?> values, Map<String, Object> results, String dictionaryCn, final String fieldCn, final String refFieldCn, String dockindCn, EnumValues emptyHiddenValue, String refFieldSelectedId) {
        try {
            Optional<pl.compan.docusafe.core.dockinds.field.Field> resourceField = findField(dockindCn, dictionaryCn, fieldCn + "_1");

            if (resourceField.isPresent()) {
                String dicWithSuffix = dictionaryCn + "_";
                if (refFieldSelectedId == null) {
                    refFieldSelectedId = getFieldSelectedId(values, results, dicWithSuffix, refFieldCn);
                }

                String fieldSelectedId = "";
                EnumValues enumValue = getEnumValues(values, dicWithSuffix + fieldCn);
                if (enumValue != null) {
                    if (!enumValue.getSelectedOptions().isEmpty()) {
                        fieldSelectedId = enumValue.getSelectedOptions().get(0);
                    }
                }

                EnumValues enumValues = resourceField.get().getEnumItemsForDwr(ImmutableMap.<String, Object>of(refFieldCn + "_1", ObjectUtils.toString(refFieldSelectedId), fieldCn + "_1", ObjectUtils.toString(fieldSelectedId)));

                if (isEmptyEnumValues(enumValues)) {
                    results.put(dicWithSuffix + fieldCn, emptyHiddenValue);
                } else {
                    results.put(dicWithSuffix + fieldCn, enumValues);
                }
            } else {
                throw new EdmException("cant find " + fieldCn + " field");
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }

    static boolean isEmptyEnumValues(EnumValues enumValues) {
        return enumValues.getAllOptions().isEmpty() || (enumValues.getAllOptions().size() == 1 && enumValues.getAllOptions().get(0).containsKey(""));
    }

    static String getFieldSelectedId(Map<String, ?> values, Map<String, Object> results, String dic, String fieldCn) {
        String fieldSelectedId = "";
        if (results.containsKey(dic + fieldCn)) {
            try {
                fieldSelectedId = ((EnumValues)results.get(dic + fieldCn)).getSelectedId();
            } catch (Exception e) {
                log.error("CAUGHT "+e.getMessage(),e);
            }
        } else {
            try {
                EnumValues enumValue = getEnumValues(values, dic + fieldCn);
                if (enumValue != null) {
                    fieldSelectedId = enumValue.getSelectedOptions().get(0);
                }
            } catch (Exception e) {
                log.error("CAUGHT " + e.getMessage(), e);
            }
        }

        return fieldSelectedId;
    }

    public static EnumValues getEnumValues(Map<String, ?> values, String fieldName) {
        Object value = values.get(fieldName);
        if (value instanceof FieldData) {
            return ((FieldData) value).getEnumValuesData();
        } else if (value instanceof EnumValues) {
            return (EnumValues) value;
        }
        return null;
    }

    static Optional<pl.compan.docusafe.core.dockinds.field.Field> findField(String dockindCn, String dictionaryCn, final String dictionaryFieldCn) throws EdmException {
        return Iterables.tryFind(DocumentKind.findByCn(dockindCn).getFieldsMap().get(dictionaryCn).getFields(), new Predicate<pl.compan.docusafe.core.dockinds.field.Field>() {
            @Override
            public boolean apply(pl.compan.docusafe.core.dockinds.field.Field input) {
                return dictionaryFieldCn.equals(input.getCn());
            }
        });
    }
}
