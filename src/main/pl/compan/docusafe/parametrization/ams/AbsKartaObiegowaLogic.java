package pl.compan.docusafe.parametrization.ams;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContextFactory;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.*;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.jbpm4.AssigneeHandlerFromLogic;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.Profile;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.util.*;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public abstract class AbsKartaObiegowaLogic
        extends
        AbstractDocumentLogic {

    protected static final int ID_EFEKTY_PROJEKTU_PRZEZNACZENIE_EPP1 = 1; //Potrzeby w�asne uczelni
    protected static final int ID_EFEKTY_PROJEKTU_PRZEZNACZENIE_EPP2 = 2; //Na sprzeda�
    protected static final int ID_EFEKTY_PROJEKTU_PRZEZNACZENIE_EPP3 = 3; //Wdro�enie
    protected static final int ID_EFEKTY_PROJEKTU_PRZEZNACZENIE_EPP4 = 4; //Przekazanie
    protected static final int ID_EFEKTY_PROJEKTU_PRZEZNACZENIE_EPP5 = 5; //Inne
    public Integer getEfektyProjektuPrzeznaczenieIdByCn(FieldsManager fm, String cn) throws EdmException {
        return ((DataBaseEnumField)FieldsManagerUtils.getFieldFromMultiple(fm,FIELD_EFEKTY_PROJEKTU_DICT,"PRZEZNACZENIE",false)).getEnumItemByCn(cn).getId();
    }
    protected static final int ID_EFEKTY_PROJEKTU_RODZAJ_EPR1 = 1; //Rozw�j dyscypliny naukowej
    protected static final int ID_EFEKTY_PROJEKTU_RODZAJ_EPR2 = 2; //Publikacje
    protected static final int ID_EFEKTY_PROJEKTU_RODZAJ_EPR3 = 3; //Prototyp
    protected static final int ID_EFEKTY_PROJEKTU_RODZAJ_EPR4 = 4; //Oprogramowanie
    protected static final int ID_EFEKTY_PROJEKTU_RODZAJ_EPR5 = 5; //�rodek trwa�y
    protected static final int ID_EFEKTY_PROJEKTU_RODZAJ_EPR6 = 6; //Inne
    public Integer getEfektyProjektuRodzajIdByCn(FieldsManager fm, String cn) throws EdmException {
        return ((DataBaseEnumField)FieldsManagerUtils.getFieldFromMultiple(fm,FIELD_EFEKTY_PROJEKTU_DICT,"RODZAJ",false)).getEnumItemByCn(cn).getId();
    }

    protected static final String STATUS = "STATUS";
    protected static final String STATUS_PROCESS_CREATION = "process_creation";
    protected static final String STATUS_CORRECTION_BY_AUTHOR = "correction_by_author";

    protected static final String FIELD_WKLAD_WLASNY_WARTOSC = "WKLAD_WLASNY_WARTOSC";
    protected static final String FIELD_PREFINANSOWANIE = "PREFINANSOWANIE";
    protected static final String FIELD_TYP_PROJEKTU = "TYP_PROJEKTU";
    protected static final String FIELD_EFEKTY_PROJEKTU_DICT = "EFEKTY_PROJEKTU";
    protected static final String DICTFIELD_EFEKTY_PROJEKTU_PRZEZNACZENIE = "PRZEZNACZENIE";
    protected static final String DICTFIELD_EFEKTY_PROJEKTU_RODZAJ = "RODZAJ";

    protected static final String ENUM_CN_PROJEKTY_BUDOWLANE = "PROJEKTY_BUDOWLANE";

    protected static final String ACCEPTATION_CN_userRole = "userRole";
    protected static final String ACCEPTATION_CN_taskStatus = "taskStatus";

    protected static Logger log = LoggerFactory.getLogger(AbsKartaObiegowaLogic.class);

    private static Map<String, String> roleToAcceptationCn = new HashMap<String, String>();

    static {
//        roleToAcceptationCn.put("dyrektor-CTTM", "DYR_CTTM");
//        roleToAcceptationCn.put("kier-NM", "KIER_NM");
    }

    private static Set<DSUser> getUsers(Collection<String> allUsernames) {
        Set<DSUser> allUsers = new HashSet<DSUser>();

        if (allUsernames != null) {
            for (String username : allUsernames) {
                try {
                    DSUser user = DSUser.findByUsername(username);
                    if (user != null)
                        allUsers.add(user);
                } catch (EdmException e) {
                    log.error("BRAK U�YTKOWNIKA: " + username, e);
                }
            }
        }

        return allUsers;
    }

    protected static boolean isUserDysponentSrodkow(DSUser user) {
        String name = user.getName();
        return name.equalsIgnoreCase("admin") || name.equalsIgnoreCase("kierownikprojektu");
    }

    protected static boolean compareStatus(EnumItem status, String cn) {
        return status != null && compareStatus(status.getCn(), cn);
    }

    protected static boolean compareStatus(String statusCn, String cn) {
        return StringUtils.isNotEmpty(statusCn) && StringUtils.isNotEmpty(cn) && statusCn.equalsIgnoreCase(cn);
    }

    protected static boolean compareStatus(EnumItem status, int id) {
        return status != null && compareStatus(status.getId(), id);
    }

    protected static boolean compareStatus(Integer statusId, Integer id) {
        return statusId != null && id != null && statusId.equals(id);
    }

    protected static void appendMsg(StringBuilder msgBuilder, StringBuilder msgString) {
        appendMsg(msgBuilder, msgString.toString());
    }

    protected static void appendMsg(StringBuilder msgBuilder, String msgString) {
        if (msgString == null)
            return;
        if (msgBuilder.length() > 0)
            msgBuilder.append(". ").append(msgString);
        else
            msgBuilder.append(msgString);
    }

    /**
     * @return null when there's no value for cn
     */
    protected static boolean validateDwrPercentField(Map<String, FieldData> values, String fieldCn, Boolean returnValIfNoField) {
        FieldData percentField = values.get(fieldCn);
        if (percentField == null) return returnValIfNoField;
        Double percentValue = DwrUtils.getDoubleValue(percentField);
        return percentValue != null ? validatePercentValue(percentValue) : returnValIfNoField;
    }

    /**
     * @return null when there's no value for cn
     */
    protected static boolean validateDwrPercentField(Map<String, FieldData> values, String fieldCn, Boolean returnValIfNoField, boolean addDwrPrefix) {
        return validateDwrPercentField(values, (addDwrPrefix ? "DWR_" : "") + fieldCn, returnValIfNoField);
    }

    protected static boolean validatePercentValue(double percentValue) {
        return validateValueRange(percentValue, 0, 100);
    }

    protected static boolean validateValueRange(double value, double min, double max) {
        return value >= min && value <= max;
    }

    public static String joinDictionaryWithField(String dictionaryCn, String fieldCn) {
        return dictionaryCn + '_' + fieldCn;
    }

    @Override
    public boolean canChangeDockind(Document document) throws EdmException {
        return false;
    }

    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
        boolean assigned = false;

        String userRole = null;
        if (ACCEPTATION_CN_userRole.equalsIgnoreCase(acceptationCn)) {
            userRole = fieldCnValue;
        }

        if (StringUtils.isNotEmpty(userRole)) {
//            if (assignToAllUsersByRole(doc, assignable, openExecution, userRole))
//                assigned = true;
            try {
                assigned = AssigneeHandlerFromLogic.assign(doc, assignable, openExecution, userRole, null, true);
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
                throw new IllegalArgumentException(e.getMessage(), e);
            }
        }

        return assigned;
    }

    public boolean isPrefinansowanieRequired(FieldsManager fm) throws EdmException {
//        EnumItem prefinansowanie = FieldsManagerUtils.getEnumItem(fm, "PREFINANSOWANIE");
//        if (prefinansowanie != null && "TAK".equalsIgnoreCase(prefinansowanie.getCn()))
//            return "prefinansowanie";

        EnumItem prefinansowanie = FieldsManagerUtils.getEnumItem(fm, FIELD_PREFINANSOWANIE);
        return prefinansowanie != null && prefinansowanie.getId().equals(1);
    }

    public boolean isWkladWlasnyRequired(FieldsManager fm) throws EdmException {
//        Integer wkladWlasny = (Integer) fm.getValue("WKLAD_WLASNY_AM_PROC_WP");
//        if (wkladWlasny != null && wkladWlasny > 0)
//            return "wkladWlasny";

        Object wkladWlasny = fm.getValue(FIELD_WKLAD_WLASNY_WARTOSC);
        return wkladWlasny != null && wkladWlasny instanceof BigDecimal && ((BigDecimal) wkladWlasny).doubleValue() > 0d;
    }

    protected static DocumentKind getDocumentKindFromDwrSession() {
        HttpSession session = WebContextFactory.get().getSession();
        if (session == null) return null;

        Map<String, Object> sessionVals = ((Map<String, Object>) session.getAttribute("dwrSession"));
        if (sessionVals == null) return null;

        String kindCn = (String) (sessionVals.get("dockindKind"));
        if (StringUtils.isEmpty(kindCn)) return null;

        DocumentKind kind;
        try {
            return DocumentKind.findByCn(kindCn);
        } catch (EdmException e) {
            log.error("Nie mo�na odnale�� dockinda na podstawie cn = " + kindCn + " pobranego z sesji dwrSession", e);
            return null;
        }
    }


    public static Set<DSUser> getSupervisors(DSUser user) throws EdmException {
        Profile profile = getSupervisorProfile();
        DSDivision[] divisions = user.getDivisions();
        Set<DSUser> supervisors = new HashSet<DSUser>();
        putSupervisorsFromDivisions(divisions, profile.getName(), supervisors);
        if (supervisors.isEmpty())
            throw new EdmException("Nie znalezeiono kierownika pionu dla u�ytkownika: "+user.getWholeName());
        return supervisors;
    }
    public static Set<DSUser> getSupervisors(DSDivision division) throws EdmException {
        Profile profile = getSupervisorProfile();
        Set<DSUser> supervisors = new HashSet<DSUser>();
        putSupervisorsFromDivisions(new DSDivision[]{division}, profile.getName(), supervisors);
        if (supervisors.isEmpty())
            throw new EdmException("Nie znalezeiono kierownika pionu dla dzia�u: "+division.getName());
        return supervisors;
    }
    public static Profile getSupervisorProfile() throws EdmException {
        String adds = Docusafe.getAdditionProperty("profile.id.kierownik");
        if (StringUtils.isBlank(adds))
            throw new EdmException("No adds: profile.id.kierownik");
        Long profileId = Long.parseLong(adds);
        Profile profile = Profile.findById(profileId);
        return profile;
    }
    public static void putSupervisorsFromDivisions(DSDivision[] divisions, String profileName, Set<DSUser> supervisors) throws EdmException {
//        if (!supervisors.isEmpty())
//            return;

        List<DSUser> users = DSDivision.getUsersFromDivisionAsList(divisions);
        for (DSUser u : users){
            if (Arrays.asList(u.getProfileNames()).contains(profileName));
            supervisors.add(u);
        }

        if (supervisors.isEmpty()){
            Collection<DSDivision> parentDivisions = new HashSet<DSDivision>();
            for (DSDivision d : divisions){
                if (!d.isRoot())
                    parentDivisions.add(d.getParent());
            }

            putSupervisorsFromDivisions(parentDivisions.toArray(new DSDivision[parentDivisions.size()]), profileName, supervisors);
        }
    }


    protected static boolean assignToAllUsersByRole(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String... userRole) throws EdmException {
        if (userRole.length == 0) return false;

        Set<String> acceptancesCns = new HashSet<String>();
        for (String role : userRole){
            String acceptanceCn = roleToAcceptationCn.get(role);
            acceptancesCns.add(acceptanceCn == null ? role : acceptanceCn);
        }

        String acceptacnesCnsLine = StringUtils.join(acceptancesCns.toArray(new String[acceptancesCns.size()]), ",");

        return AssigneeHandlerFromLogic.assign(doc,assignable, openExecution, acceptacnesCnsLine, null, true);
    }
    protected static boolean assignToAllUsersByRole(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, Collection<String> userRole) throws EdmException {
        return assignToAllUsersByRole(doc, assignable, openExecution, userRole.toArray(new String [userRole.size()]));

//        Set<DSUser> allUsers = getAllUsersByRole(userRole);
//        boolean anyOk = false;
//        for (DSUser user : allUsers)
//            if (AssigneeHandlerUtils.assignToUser(doc, assignable, openExecution, user))
//                anyOk = true;
//        return anyOk;
    }



    @Override
    public void setAdditionalTemplateValues(long docId, Map<String, Object> values) {
        try {
            Map<Class, Format> defaultFormatters = Maps.newLinkedHashMap();
            setAdditionalTemplateValuesUnsafe(docId, values, defaultFormatters);
        } catch (EdmException e) {
            log.error(e.getMessage(),e);
        }
    }

    public void setAdditionalTemplateValuesUnsafe(long docId, Map<String, Object> values, Map<Class, Format> defaultFormatters) throws EdmException {
        OfficeDocument doc = OfficeDocument.find(docId);
        FieldsManager fm = doc.getFieldsManager();
        fm.initialize();

        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(',');
        defaultFormatters.put(Double.class, new DecimalFormat("### ### ##0.0000;-### ### ##0.0000", symbols));
        defaultFormatters.put(BigDecimal.class, new DecimalFormat("### ### ##0.0000;-### ### ##0.0000", symbols));

        putExtractedValues(values, fm);
    }
    public static String getEnumTitle(FieldsManager fm, String enumFieldCn) throws EdmException {
        EnumItem enumField = fm.getEnumItem(enumFieldCn);
        if (enumField!=null) return enumField.getTitle();

//        Integer kierProjId = (Integer)fm.getKey(enumFieldCn);
//        pl.compan.docusafe.core.dockinds.field.Field kierProjField = fm.getField(enumFieldCn);
//        if (kierProjField!=null && kierProjField!=null)
//            return kierProjField.getEnumItem(kierProjId).getTitle();
        return null;
    }
    public static String getUserNameFromAcceptance(FieldsManager fm, String taskCn) throws EdmException {
        Long docId = fm.getDocumentId();
        List<DocumentAcceptance> accList = DocumentAcceptance.find(docId);
        for (int i = 0; i < accList.size(); ++i){
            DocumentAcceptance acc = accList.get(i);


            if (taskCn.equalsIgnoreCase(acc.getAcceptanceCn())){
                String username = acc.getUsername();
                try{
                    DSUser user = DSUser.findByUsername(username);
                    return user.getWholeName();
                } catch (Exception e){
                    log.error(e.getMessage(),e);
                }
            }
        }
        return null;
    }

    @Override
    public void setAdditionalTemplateValues(long docId, Map<String, Object> values, Map<Class, Format> defaultFormatters) {
        try {
            setAdditionalTemplateValuesUnsafe(docId, values, defaultFormatters);
        } catch (EdmException e) {
            log.error(e.getMessage(),e);
        }
    }

    public void putExtractedValues(Map<String, Object> values, FieldsManager fm) {
        FieldsManagerUtils.FieldValuesGetter getter = new FieldsManagerUtils.DateEnumValuesGetter(false);
        FieldsManagerUtils.FieldValuesGetter getterNullable = new FieldsManagerUtils.DateEnumValuesGetter(true);
        try {
            for (pl.compan.docusafe.core.dockinds.field.Field field : fm.getFields()) {
                try {
                    String fieldCn = field.getCn();
                    if (field instanceof DictionaryField) {
                        List<Map<String, Object>> dictTemplateValues = FieldsManagerUtils.getMultiDictionaryExtractedValues(fm, fieldCn, getter);
                        values.put(fieldCn, dictTemplateValues);
                    } else if (field instanceof DocumentPersonField) {
                        Long personId = (Long) fm.getKey(field.getCn());
                        Person person = Person.find(personId.longValue());
                        Map<String, Object> templatePerson = toTemplate(person);
                        values.put(fieldCn, templatePerson);
                    } else if (field instanceof DataBaseEnumField) {
                        Object fieldValue = fm.getValue(fieldCn);
                        values.put(fieldCn, fieldValue);
                    } else if (field instanceof AttachmentField) {
                        Attachment fieldValue = (Attachment) fm.getValue(fieldCn);
                        if (fieldValue!=null)
                            values.put(fieldCn, fieldValue.getTitle());
                    } else {
                        Object fieldValue = fm.getValue(fieldCn);
                        Object extractedValue = getter.getValue(fieldValue);
                        if (extractedValue != null)
                            values.put(fieldCn, extractedValue);
                        if (fieldValue instanceof List)
                            values.put(fieldCn, fieldValue);
                    }
                } catch (EdmException e) {
                    log.error(e.getMessage(), e);
                }
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }

    private Map<String, Object> toTemplate(Person person) {
        Map<String, Object> template = new HashMap<String, Object>();
        template.put("ORGANIZATION", person.getOrganization());
        template.put("STREET", person.getStreet());
        template.put("ZIP", person.getZip());
        template.put("LOCATION", person.getLocation());
        try
        {
            String country = DataBaseEnumField.getEnumItemForTable("dsr_country", Integer.valueOf(person.getCountry())).getTitle();
            template.put("COUNTRY", country);
        }
        catch (Exception e)
        {
            log.error(e.getMessage());
            template.put("COUNTRY","---");
        }
        template.put("NIP", person.getNip());
        template.put("REGON", person.getRegon());
        return template;
    }

}
