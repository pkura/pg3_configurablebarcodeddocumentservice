package pl.compan.docusafe.parametrization.ams;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.api.ActivityInfo;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.LogicUtils;
import pl.compan.docusafe.core.jbpm4.Jbpm4ActivityInfo;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.ams.jbpm.ProjKrajDecisionHandler;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ProjKrajLogic
        extends
        AbsKartaObiegowaLogic
        implements
        ProjKrajDecisionHandler.FormaPrzekazaniaEfektow,
        ProjKrajDecisionHandler.FinansowanieInwestBudowlanych,
        ProjKrajDecisionHandler.Prefinansowanie,
        ProjKrajDecisionHandler.WkladWlasny {


    public static final String FIELD_OPIEKUN_PROJEKTU = "OPIEKUN_PROJEKTU";
    public static final String FIELD_KOSZTY_POSREDNIE_PROC_KB = "KOSZTY_POSREDNIE_PROC_KB";
    public static final String FIELD_WKLAD_WLASNY_AM_PROC_WP = "WKLAD_WLASNY_AM_PROC_WP";
    public static final String FIELD_KOSZTY_POSREDNIE = "KOSZTY_POSREDNIE";
    public static final String FIELD_KOSZTY_POSREDNIE_PROC = "KOSZTY_POSREDNIE_PROC";
    public static final String FIELD_DYSPONENT_SRODKOW = "DYSPONENT_SRODKOW";
    public static final String FIELD_KOSZTY_BEZPOSREDNIE = "KOSZTY_BEZPOSREDNIE";
    public static final String FIELD_KOSZTY_BEZPOSREDNIE_PROC = "KOSZTY_BEZPOSREDNIE_PROC";
    public static final String FIELD_LACZNA_WARTOSC_PROJEKTU = "LACZNA_WARTOSC_PROJEKTU";
    public static final String FIELD_KIEROWNIK_PROJEKTU = "KIEROWNIK_PROJEKTU";
    public static final String FIELD_FINANSOWANIE_ZEWNETRZNE_PROC_WP = "FIELD_FINANSOWANIE_ZEWNETRZNE_PROC_WP";
    public static final String DICTFIELD_DYSPONENCI_DYSPONENT = "DYSPONENT";
    public static final String DICTFIELD_DYSPONENCI_ZADANIE_FINANSOWE = "ZADANIE_FINANSOWE";
    public static final String FIELD_DYSPONENCI_SRODKOW_DICT = "DYSPONENCI_SRODKOW";
    public static final String FIELD_DYSPONENCI_SRODKOW_DISABLED_DICT = "DYSPONENCI_SRODKOW_DISABLED";

    public static final String STATUS_OPINIA_DZIALU_NAUKI = "opinia_dzialu_nauki";
    public static final String STATUS_WKLAD_WLASNY = "wklad_wlasny";
    public static final String STATUS_WKLAD_WLASNY_DISABLED = "wklad_wlasny_disabled";


    protected static Logger log = LoggerFactory.getLogger(ProjKrajLogic.class);


    @Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {

        String statusCn = fm.getEnumItemCn(STATUS);
        StringBuilder msgBuilder = new StringBuilder();

        if (compareStatus(statusCn, STATUS_PROCESS_CREATION) || compareStatus(statusCn,STATUS_CORRECTION_BY_AUTHOR)) {
//                tu mo�e by� > 100
//                if (!validateDwrPercentField(values,"KOSZTY_POSREDNIE_PROC_KB",true,true))
//                    appendMsg(msgBuilder,"Niepoprawna warto�� procentowa pola: "+FieldsManagerUtils.getFieldName(fm,"KOSZTY_POSREDNIE_PROC_KB",true));
            if (!validateDwrPercentField(values, FIELD_KOSZTY_BEZPOSREDNIE_PROC, true, true)) {
                appendMsg(msgBuilder, "Niepoprawna warto�� procentowa pola: " + FieldsManagerUtils.getFieldName(fm, FIELD_KOSZTY_BEZPOSREDNIE_PROC, true));
            }
            if (!validateDwrPercentField(values, FIELD_FINANSOWANIE_ZEWNETRZNE_PROC_WP, true, true)) {
                appendMsg(msgBuilder, "Niepoprawna warto�� procentowa pola: " + FieldsManagerUtils.getFieldName(fm, FIELD_FINANSOWANIE_ZEWNETRZNE_PROC_WP, true));
                DwrUtils.resetField(values, FIELD_FINANSOWANIE_ZEWNETRZNE_PROC_WP, true);
            }
            if (!validateDwrPercentField(values, FIELD_WKLAD_WLASNY_AM_PROC_WP, true, true)) {
                appendMsg(msgBuilder, "Niepoprawna warto�� procentowa pola: " + FieldsManagerUtils.getFieldName(fm, FIELD_WKLAD_WLASNY_AM_PROC_WP, true));
                DwrUtils.resetField(values, FIELD_WKLAD_WLASNY_AM_PROC_WP, true);
            }


            FieldData kosztyPosrednie = values.get(DwrUtils.dwr(FIELD_KOSZTY_POSREDNIE));
            FieldData kosztyBezposrednie = values.get(DwrUtils.dwr(FIELD_KOSZTY_BEZPOSREDNIE));
            FieldData lacznaWartoscProjektu = values.get(DwrUtils.dwr(FIELD_LACZNA_WARTOSC_PROJEKTU));
            DwrUtils.calculateSum(lacznaWartoscProjektu, kosztyPosrednie, kosztyBezposrednie);


            FieldData wkladWlasnyProcWartosciProj = values.get(DwrUtils.dwr(FIELD_WKLAD_WLASNY_AM_PROC_WP));
            FieldData wkladWlasnyWartosc = values.get(DwrUtils.dwr(FIELD_WKLAD_WLASNY_WARTOSC));
            DwrUtils.calculateRelatedField(wkladWlasnyWartosc, true, wkladWlasnyProcWartosciProj, true, lacznaWartoscProjektu, false);


            //FieldData kosztyBezposrednieProc = values.get(DwrUtils.dwr(FIELD_KOSZTY_BEZPOSREDNIE_PROC));
            //FieldData kosztyPosrednieProcKb = values.get(dwr(FIELD_KOSZTY_POSREDNIE_PROC_KB));
            //DwrUtils.calculateRelatedField(kosztyBezposrednie, kosztyBezposrednieProc, lacznaWartoscProjektu);


            //FieldData kosztyPosrednieProc = values.get(DwrUtils.dwr(FIELD_KOSZTY_POSREDNIE_PROC));
            //DwrUtils.calculateRelatedField(kosztyPosrednie, true, kosztyPosrednieProc, false, lacznaWartoscProjektu, false);


//            FieldData typProjektu = values.get(DwrUtils.dwr(FIELD_TYP_PROJEKTU));
//            if (typProjektu != null && typProjektu.isSender()){
//                EnumValues typProjektuEV = typProjektu.getEnumValuesData();
//                String typProjektuSelectedId = typProjektuEV.getSelectedId();
//                String typProjektuSelectedValue = typProjektuEV.getSelectedValue();
//                System.out.println(typProjektuEV);
//            }




            //        LACZNA_WARTOSC_PROJEKTU > KOSZTY_BEZPOSREDNIE
            //        WKLAD_WLASNY_WARTOSC ??
            //        PREFINANSOWANIE_WARTOSC ??
        }
        if (compareStatus(statusCn, STATUS_WKLAD_WLASNY)) {
            FieldData lacznaWartoscProjektu = values.get(DwrUtils.dwr(FIELD_LACZNA_WARTOSC_PROJEKTU));
            FieldData wkladWlasnyWartosc = values.get(DwrUtils.dwr(FIELD_WKLAD_WLASNY_WARTOSC));
            FieldData wkladWlasnyProcWartosciProj = values.get(DwrUtils.dwr(FIELD_WKLAD_WLASNY_AM_PROC_WP));
            DwrUtils.calculateRelatedField(wkladWlasnyWartosc, true, wkladWlasnyProcWartosciProj, true, lacznaWartoscProjektu, false);

        }

        if (msgBuilder.length() > 0) {
            values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
            return new Field("messageField", msgBuilder.toString(), Field.Type.BOOLEAN);
        }

        return null;
    }


    private enum TypProjektu{
        PROJEKTY_INWESTYCYJNE(1),
        PROJEKTY_BUDOWLANE(2),
        PROJEKTY_REMONTOWE(3),
        PROJEKTY_MI�DZYNARODOWE(4),
        PROJEKTY_KRAJOWE(5),
        PROJEKTY_CELOWE_DOTACJE(6),
        PRACE_ZLECONE(7),
        PROJEKTY_ORGANIZACYJNE(8);

        public final int id;

        private TypProjektu(int id){
            this.id = id;
        }
    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = new HashMap<String, Object>();
        for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
            if (f.getDefaultValue() != null)
                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));

        DSUser user = DSApi.context().getDSUser();
        toReload.put(FIELD_KIEROWNIK_PROJEKTU, user.getId());

//        if (isUserDysponentSrodkow(user))
//            toReload.put(FIELD_DYSPONENT_SRODKOW, user.getId());


        fm.reloadValues(toReload);
    }


    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
        boolean assigned = super.assignee(doc, assignable, openExecution, acceptationCn, fieldCnValue);
        if (assigned)
            return true;

//        if ("taskStatus".equalsIgnoreCase(acceptationCn)) {
//            String userRole = null;
//            if (StringUtils.isNotEmpty(userRole) && assignToAllUsersByRole(doc, assignable, openExecution, userRole))
//                return true;
//        }

        return assigned;
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        //To change body of implemented methods use File | Settings | File Templates.
    }



    private pl.compan.docusafe.core.dockinds.field.Field getFieldFromDictionary(DocumentKind dockind, String dictionaryName, String dictionaryFieldName) {
        FieldsManager fm = dockind.getFieldsManager(null);
        if (fm == null) return null;

        DictionaryField dictionary;
        try {
            dictionary = (DictionaryField) fm.getField(dictionaryName);
        } catch (EdmException e) {
            log.error("Nie mo�na s�ownika " + dictionaryName + " w podanym dockindzie", e);
            return null;
        }

        Pattern pattern = Pattern.compile('('+dictionaryFieldName+"$)|("+dictionaryFieldName+"_\\d+$)");
        for (pl.compan.docusafe.core.dockinds.field.Field field : dictionary.getFields()) {
            if (pattern.matcher(field.getCn()).find()) {
                return field;
            }
        }

        return null;
    }


    @Override
    public void setAdditionalValues(FieldsManager fieldsManager, Integer valueOf, String activity) throws EdmException {
        try {
            Jbpm4ActivityInfo activityInfo = new Jbpm4ActivityInfo(activity);
            String taskName = (String) activityInfo.get("task_name");
            EnumItem status = fieldsManager.getEnumItem(STATUS);

            if (fieldsManager != null && status != null) {
                String statusCn = status.getCn();


                if (statusCn.equals(STATUS_OPINIA_DZIALU_NAUKI)) {
                    DSDivision division = DSDivision.findByName("Dzia� Nauki");
                    DSUser[] users = division.getUsers(false);
                    List<Long> usersIds = new ArrayList<Long>();
                    for (DSUser user : users)
                        usersIds.add(user.getId());

                    Object dic = fieldsManager.getField(FIELD_OPIEKUN_PROJEKTU);
                    if (dic != null && dic instanceof List) {
                        ((List) dic).clear();
                        ((List) dic).addAll(usersIds);
                    }
                }

                if (statusCn.equals(STATUS_WKLAD_WLASNY_DISABLED) || statusCn.equals(STATUS_WKLAD_WLASNY)) {
                    DSUser user = DSUser.getLoggedInUserSafely();
                    List<Object> dysponenci = new ArrayList<Object>();

                    List<Map<String, Object>> opinieAll = FieldsManagerUtils.getDictionaryItems(fieldsManager, FIELD_DYSPONENCI_SRODKOW_DICT,
                            new FieldsManagerUtils.FieldProperty(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_DYSPONENT), EnumValues.class),
                            new FieldsManagerUtils.FieldProperty(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, "ID")));

                    for (Map o : opinieAll) {
                        if (o.containsKey(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_DYSPONENT))) {

                            Long selectedUserId = Long.parseLong(((EnumValues) o.get(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_DYSPONENT))).getSelectedId());
                            if (selectedUserId.equals(user.getId())) {
                                Object id = o.get(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, "ID"));
                                dysponenci.add(id);
                            }
                        }
                    }

                    Object dic = fieldsManager.getFieldValues().get(FIELD_DYSPONENCI_SRODKOW_DICT);
                    if (dic != null && dic instanceof List) {
                        ((List) dic).clear();
                        ((List) dic).addAll(dysponenci);
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void beforeSetWithHistory(Document document, Map<String, Object> values, String activity) throws EdmException {
        Long docId = document.getId();
        PreparedStatement ps = null;
        try {
            ActivityInfo activityInfo = new Jbpm4ActivityInfo(activity);
            String taskName = (String) activityInfo.get("task_name");
            if (document.getFieldsManager().getEnumItemCn(STATUS).equals(STATUS_WKLAD_WLASNY_DISABLED)
                    || document.getFieldsManager().getEnumItemCn(STATUS).equals(STATUS_WKLAD_WLASNY)) {
                FieldsManager oldFm = document.getFieldsManager();
                oldFm.initialize();
                Map<String, Object> mDict = (Map) values.get("M_DICT_VALUES");
                LogicUtils.putOldMultipleDictionaryValues(mDict, oldFm, FIELD_DYSPONENCI_SRODKOW_DICT);
                values.put("M_DICT_VALUES", mDict);
                document.getFieldsManager().reloadValues(values);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {
        //To change body of implemented methods use File | Settings | File Templates.
    }


    @Override
    public boolean isFormaPrzkazaniaEfektow(FieldsManager fm) throws EdmException {
        List<EnumValues> przeznaczenia = FieldsManagerUtils.getDictionaryItems(fm, FIELD_EFEKTY_PROJEKTU_DICT, DICTFIELD_EFEKTY_PROJEKTU_PRZEZNACZENIE, true, EnumValues.class);
        for (EnumValues przeznaczenie : przeznaczenia) {
            if (przeznaczenie.getSelectedId().equals(ID_EFEKTY_PROJEKTU_PRZEZNACZENIE_EPP2) || przeznaczenie.getSelectedId().equals(ID_EFEKTY_PROJEKTU_PRZEZNACZENIE_EPP4))
                return true;
        }
        return false;
    }

    @Override
    public boolean isFinansowanieInwestBudowlanych(FieldsManager fm) throws EdmException {
        EnumItem projectType = FieldsManagerUtils.getEnumItem(fm, FIELD_TYP_PROJEKTU);
        return projectType != null && projectType.getCn().equalsIgnoreCase(ENUM_CN_PROJEKTY_BUDOWLANE);
    }

}
