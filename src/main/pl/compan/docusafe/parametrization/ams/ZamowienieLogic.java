package pl.compan.docusafe.parametrization.ams;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.*;
import com.google.common.base.Objects;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.ObjectUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.SessionUtils;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.enums.ExternalSourceEnumItem;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.exports.*;
import pl.compan.docusafe.core.jbpm4.AssignUtils;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.AssigneeLogic;
import pl.compan.docusafe.core.jbpm4.AssigningHandledException;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.ams.hb.BudgetUnitOrganizationUnit;
import pl.compan.docusafe.parametrization.ams.hb.PurchasePlan;
import pl.compan.docusafe.parametrization.ams.hb.UnitBudget;
import pl.compan.docusafe.parametrization.ams.hb.UnitBudgetKo;
import pl.compan.docusafe.parametrization.ams.jbpm.IsCostFactureDecision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionType;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.primitives.Ints;

/**
 * User: kk
 * Date: 15.01.14
 * Time: 13:36
 */
public class ZamowienieLogic extends FakturaZakupowaLogic {
    public static final String DYSPONENT_VIEW = "dsg_ams_dysponent_pion";
    protected static final Logger log = LoggerFactory.getLogger(ZamowienieLogic.class);


    public static final String CASE_NUMBER_FIELD_CN = "NR_SPRAWY";
    public static final String DOCKIND_TABLE = "dsg_ams_zamowienie";

	public static final int MONEY_SCALE = 2;

    public static final Map<String, AssigneeLogic> ASSIGNEE_LOGIC_MAP = ImmutableMap.<String, AssigneeLogic>builder()
            .put("confirmation_jw", new ApplicationDivisionManager())
            .put("project_attendant", new ApplicationProjectAttendant())
            .put("dysponent_jednostki_wnioskujacej", new Dysponent())
            .put("confirmation_jr", new RealizationDivision())
            .put("implementing_cell_supervisor", new AssigneeLogic() {
                @Override
                public void assign(OfficeDocument doc, FieldsManager fm, Assignable assignable, OpenExecution openExecution) throws EdmException {
                    String divGuid = getRealizationDivisionGuid(fm);
                    AssignUtils.checkState(divGuid != null, "Nie wybrano jednostki realizuj�cej");

                    List<String> userList = (List<String>) DSApi.context().session().createSQLQuery("select DISTINCT u.NAME from DS_DIVISION d join DS_USER_TO_DIVISION ud on d.ID = ud.DIVISION_ID join DS_USER u on ud.USER_ID = u.ID join ds_profile_to_user pu on u.ID = pu.user_id where pu.profile_id = (SELECT id FROM ds_profile WHERE name = 'kierownik') and d.GUID = :guid and u.DELETED = 0")
                            .setParameter("guid", divGuid)
                            .list();

                    AssignUtils.checkState(userList != null && !userList.isEmpty(), "Nie znaleziono kierownika jednostki realizuj�cej: " + divGuid);

                    log.error(userList.toString());
                    for (String username : userList) {
                        assignable.addCandidateUser(username);
                        AssigneeHandler.addToHistory(openExecution, doc, username, null);
                    }
                }
            })
            .put("REALIZATOR", new RealizationDivision())
            .build();

    private static final String UPDATE_CASE_NO = "declare @n int;\n" +
            "select @n=ISNULL(max(cast(substring(nr_sprawy, 6, 4) as int)),0)+1\n" +
            "from " + DOCKIND_TABLE + "\n" +
            "where SUBSTRING(nr_sprawy, 1, 4)=?;\n" +
            "update " + DOCKIND_TABLE + " set nr_sprawy=?+replace(str(@n,4),' ','0') where document_id=?;";


    private static final Map<String, String> MAIN_PERMISSION = ImmutableMap.<String, String>builder().
            put(ObjectPermission.READ, "odczyt").
            put(ObjectPermission.READ_ATTACHMENTS, "odczyt zalacznika").
            put(ObjectPermission.MODIFY, "edycja").
            put(ObjectPermission.MODIFY_ATTACHMENTS, "edycja zalacznika").
            put(ObjectPermission.DELETE, "usuwanie").
            build();
    private static final String WNIOSKODAWCA = "WNIOSKODAWCA";
    private static final String WNIOSKODAWCA_JEDNOSTKA = "WNIOSKODAWCA_JEDNOSTKA";
    private static final String OPIS_FIELD = "OPIS";
    public static final String NIE_DOTYCZY_PRODUCT_LABEL = "nie dotyczy";
    private static final String JM_FIELD = "JM";
    private static final String PRODUKT_FIELD = "PRODUKT";
    public static final String DEKRETY_ZLECENIE = "DEKRETY_ZLECENIE";
    public static final String ERP_DOCUMENT_IDM = "ERP_DOCUMENT_IDM";
    public static final String DYSPONENT_FIELD = "DYSPONENT";
    public static final BigDecimal BIG_DECIMAL_100 = BigDecimal.ONE.movePointRight(2);
    public static final String SELECTED_BUDGET_POSITION = "SELECTED_BUDGET_POSITION";
    public static final String ZADANIA_FIELD = "ZADANIA";
    public static final String ZADANIE_PROCENT = ZADANIA_FIELD + "_" + "PROCENT";
    public static final String ZADANIE_BRUTTO = ZADANIA_FIELD + "_" + "BRUTTO";
    public static final String ZADANIE_FINANSOWE_FIELD = "ZADANIE_FINANSOWE";
    public static final String POSITION_WITH_AMOUNT = "POSITION_WITH_AMOUNT";
    private static final String MPK_FIELD_WITH_SUFFIX = MPK_FIELD + "_";
    public static final String DOCKIND_CN = "zamowienie_ams";
    public static final String TYP_REZERWACJI = "TYP_REZERWACJI";
    public static final String VIEW_PURCHASE_PLAN = "dsg_ams_view_purchase_plan";
    public static final String IDENTYFIKATOR_PLANU = "IDENTYFIKATOR_PLANU";
    public static final String OKRES_BUDZETOWY = "OKRES_BUDZETOWY";
    public static final String POZYCJA_PLANU_ZAKUPOW_RAW = "POZYCJA_PLANU_ZAKUPOW_RAW";
    public static final String BUDZET = "BUDZET";
    public static final String POZYCJA_BUDZETU_RAW = "POZYCJA_BUDZETU_RAW";
    public static final String PROJECT_ATTENDANT_ACCEPTATION_CN = "project_attendant";
    public static final String WOZ_RESERVATION_TYPE = "WOZ";
    public static final String WB_RESERVATION_TYPE = "WB";
    private static final String OKRES_BUDZETOWY_BK = OKRES_BUDZETOWY + "_BK";
    public static final String MPK_FINAL_FIELD = MPK_FIELD_WITH_SUFFIX + "FINAL";
    public static final String ZADANIA_FINAL_FIELD = "ZADANIA_FINAL";
    public static final String TYP_REZERWACJI_FINAL = TYP_REZERWACJI + "_FINAL";
    public static final String CZY_EDYCJA_DZP = "CZY_EDYCJA_DZP";
    public static final String ZADANIE_FINANSOWE_BUDZETU_RAW = "ZADANIE_FINANSOWE_BUDZETU_RAW";
    public static final String ZADANIE_FINANSOWE_PLANU_RAW = "ZADANIE_FINANSOWE_PLANU_RAW";
    public static final String BUDGET_RESERVATION_TYPE_ID = "2";
    public static final String PURCHASE_PLAN_RESERVATION_TYPE_ID = "1";
    public static final String UNIT_OF_MEASURE_TABLE = "dsg_ams_unit_of_measure";
    public static final String JEDNOSTKA_REALIZUJACA = "JEDNOSTKA_REALIZUJACA";
    public static final String REALIZATION_DIVISION_CODE_SESSION_KEY = "REALIZATION_DIVISION_CODE";
    public static final String ZRODLA_FIELD = "ZRODLA";
    public static final String ZRODLA_FINAL_FIELD = "ZRODLA_FINAL";
    public static final String ZRODLO_BUDZETU_RAW = "ZRODLO_BUDZETU_RAW";
    private static final String JEDNOSTKA_REALIZUJACA_AUTO = "JEDNOSTKA_REALIZUJACA_AUTO";


    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        SessionUtils.clearSession(fm.getDocumentId(), REALIZATION_DIVISION_CODE_SESSION_KEY);
        Map<String, Object> toReload = Maps.newHashMap();
        for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
            if (f.getDefaultValue() != null)
                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));

        DSUser user = DSApi.context().getDSUser();
        toReload.put(WNIOSKODAWCA, user.getId());
        toReload.put(WNIOSKODAWCA_JEDNOSTKA, user.getDivisionsWithoutGroup().length > 0 ? user.getDivisionsWithoutGroup()[0].getId() : null);
        fm.reloadValues(toReload);
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {
    }


    private void checkFilledAmountByAvailableLimits(Map<String, FieldData> values, String dictionaryCn, String firstFieldCn) {
        FieldData positionInfo = values.get("DWR_DOSTEPNE_SRODKI_INFO");
        FieldData dictionaryFieldData = values.get("DWR_"+dictionaryCn);

        if (positionInfo != null && dictionaryFieldData != null) {
            Map<String, FieldData> dictionaryData = dictionaryFieldData.getDictionaryData();
            Pattern pattern = Pattern.compile(".*(plan-wykonanie: )([0-9]+.[0-9]+)(; )(rezerwacje: )([0-9]+.[0-9]+)");

            for(int i = 1;;i++) {
                FieldData resourceField = dictionaryData.get(dictionaryCn + "_" + firstFieldCn + "_" + i);
                FieldData amount = dictionaryData.get(dictionaryCn + "_" + "BRUTTO" + "_" + i);

                if (resourceField == null || amount == null) {
                    break;
                }

                BigDecimal filledAmount = amount.getMoneyData();

                if (filledAmount == null) {
                    continue;
                }

                BigDecimal positionRemainAmount = null;

                String selectedValue = Iterables.getFirst(resourceField.getEnumValuesData().getSelectedOptions(), null);

                if (!Strings.isNullOrEmpty(selectedValue)) {
                    Matcher matcher = pattern.matcher(selectedValue);
                    if (matcher.find()) {
                        positionRemainAmount = new BigDecimal(matcher.group(2)).subtract(new BigDecimal(matcher.group(5)));
                    }

                    if (positionRemainAmount != null) {
                        if (filledAmount.compareTo(positionRemainAmount) >= 1) {
                            positionInfo.setStringData("Zosta�y przekroczone dost�pne �rodki.");
                            return;
                        }
                    }
                }

            }
        }
    }

	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {

		try {
            validateAmountAvailability(values);

            fillRealizationDivisionFromPositions(values, fm.getDocumentId());

			FieldData dwrMpk = values.get("DWR_MPK");
			FieldData dwrMpkFinal = values.get("DWR_MPK_FINAL");

			if (dwrMpk != null || dwrMpkFinal != null) {
				boolean found = false;

				if (dwrMpk != null) {
					found = searchForSelectedProject(dwrMpk, "MPK_PROJEKT_");
				}
				if (dwrMpkFinal != null && found == false) {
					found = searchForSelectedProject(dwrMpkFinal, "MPK_FINAL_PROJEKT_");
				}

				if (found) {
					values.get("DWR_WYDRUK_HELPER").setStringData("M");
				} else {
					values.get("DWR_WYDRUK_HELPER").setStringData("K");
				}
			}

			boolean czySumaNettoManualna = values.get("DWR_CZY_SUMA_NETTO_MANUALNA") != null && Boolean.TRUE.equals(values.get("DWR_CZY_SUMA_NETTO_MANUALNA").getBooleanData());
            if (values.get("DWR_SUMA_NETTO") != null && !czySumaNettoManualna) {
                boolean wasOpened = true;
                try {
                    wasOpened = DSApi.openContextIfNeeded();
                    values.get("DWR_SUMA_NETTO").setMoneyData(getNettoSum(values));
                } catch (EdmException e) {
                    log.error(e.getMessage());
                } finally {
                    if (!wasOpened) {
                        DSApi._close();
                    }
                }
            }
        } catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		return null;
	}

    private void fillRealizationDivisionFromPositions(Map<String, FieldData> values, Long documentId) {
        if (values.get(DWR_PREFIX + JEDNOSTKA_REALIZUJACA_AUTO) != null) {
            EnumValues enumValuesData = values.get(DWR_PREFIX + JEDNOSTKA_REALIZUJACA_AUTO).getEnumValuesData();
            String realizationDivisionCode = SessionUtils.getFromSession(documentId, REALIZATION_DIVISION_CODE_SESSION_KEY, null);
            String id = "";
            if (realizationDivisionCode != null) {
                if (!"".equals(realizationDivisionCode)) {
                    boolean wasOpened = true;
                    try {
                        wasOpened = DSApi.openContextIfNeeded();
                        DSDivision dsDivision = DSDivision.findByCode(realizationDivisionCode);
                        id = dsDivision.getId().toString();

                    } catch (EdmException e) {
                        log.error(e.getMessage(), e);
                    } finally {
                        if (!wasOpened) {
                            DSApi._close();
                        }
                    }
                }

                enumValuesData.setSelectedId(id);
            }
        }
    }

    private void validateAmountAvailability(Map<String, FieldData> values) {
        FieldData positionInfo = values.get("DWR_DOSTEPNE_SRODKI_INFO");

        if (positionInfo != null) {
            positionInfo.setStringData("Kwoty s� dost�pne");

            boolean takeEditedPositions = "1".equals(DwrUtils.getStringValue(values, DWR_PREFIX + CZY_EDYCJA_DZP));

            String reservationType = DwrUtils.getStringValue(values, DWR_PREFIX + (takeEditedPositions ? TYP_REZERWACJI_FINAL : TYP_REZERWACJI));

            checkFilledAmountByAvailableLimits(values, takeEditedPositions ? MPK_FINAL_FIELD : MPK_FIELD, BUDGET_RESERVATION_TYPE_ID.equals(reservationType) ? POZYCJA_BUDZETU_RAW : POZYCJA_PLANU_ZAKUPOW_RAW);
            checkFilledAmountByAvailableLimits(values, takeEditedPositions ? ZADANIA_FINAL_FIELD : ZADANIA_FIELD, BUDGET_RESERVATION_TYPE_ID.equals(reservationType) ? ZADANIE_FINANSOWE_BUDZETU_RAW : ZADANIE_FINANSOWE_PLANU_RAW);
        }
    }

    private boolean searchForSelectedProject(FieldData dwrMpk, String keyTemplate) {

		Map<String, Object> map = (Map) dwrMpk.getData();
		for (String key : map.keySet()) {
			if (key.matches(keyTemplate + "(\\d)")) {
				FieldData entry = (FieldData) map.get(key);
				EnumValues enumValues = entry.getEnumValuesData();
				if (enumValues.getSelectedId() != null && !enumValues.getSelectedId().equals("")) {
					return true;
				}
			}
		}

		return false;
	}

    private void assigneDecreeNumber(Long id, String year) throws Exception {
        int updated;
        PreparedStatement ps = null;
        try {
            ps = DSApi.context().prepareStatement(UPDATE_CASE_NO);
            ps.setString(1, year);
            ps.setString(2, String.format("%.4s/", year));
            ps.setLong(3, id);

            updated = ps.executeUpdate();

            Preconditions.checkState(updated == 1);
        } catch (Exception e) {
            if (ps != null) {
                DSApi.context().closeStatement(ps);
            }
            throw e;
        }
    }

    @Override
    public boolean canChangeDockind(Document document) throws EdmException {
        return false;
    }

    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
        super.onStartProcess(document, event);

        createPermissions(document);

        createCaseNumber(document);
    }


    private void createCaseNumber(OfficeDocument document) throws EdmException {
        try {
            FieldsManager fm = document.getFieldsManager();
            if (fm.getKey(CASE_NUMBER_FIELD_CN) == null) {
                Date currentDay = GlobalPreferences.getCurrentDay();
                Calendar calInstance = Calendar.getInstance();
                calInstance.setTime(currentDay);

                String year = String.valueOf(calInstance.get(Calendar.YEAR));

                assigneDecreeNumber(document.getId(), year);
            } else {
                log.error("Pr�bowano ponownie nada� numer sprawy");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new EdmException("B��d w generowaniu numeru sprawy: " + e.getMessage());
        }
    }

    private void createPermissions(OfficeDocument document) throws EdmException {
        Set<PermissionBean> perms = new HashSet<PermissionBean>();
        String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
        String documentKindName = document.getDocumentKind().getName();

        for (Map.Entry<String, String> entry : MAIN_PERMISSION.entrySet()) {
            perms.add(new PermissionBean(entry.getKey(), String.format("%s_%s", documentKindCn, entry.getKey().toUpperCase()), ObjectPermission.GROUP, String.format("%s - %s", documentKindName, entry.getValue())));
        }

        Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
        perms.addAll(documentPermissions);
        this.setUpPermission(document, perms);
    }

    @Override
    public List<String> getActionMessage(ActionType source, Document doc) {
        List<String> messages = super.getActionMessage(source, doc);

        try {
            switch (source) {
                case OFFICE_CREATE_DOCUMENT:
                    messages.add("Zosta� nadany numer sprawy: " + doc.getFieldsManager().getStringValue(CASE_NUMBER_FIELD_CN));
                    break;
                default:
                    break;
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }

        return messages;
    }

    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues, Long documentId) {
        Map<String, Object> result = Maps.newHashMap();
        try {
            String dictionaryNameWithPrefix = dictionaryName + "_";
            String reservationTypeId = dictionaryName.equals(MPK_FINAL_FIELD) || dictionaryName.equals(ZADANIA_FINAL_FIELD) ? ObjectUtils.toString(fieldsValues.get(TYP_REZERWACJI_FINAL)) : ObjectUtils.toString(fieldsValues.get(TYP_REZERWACJI));
            if (dictionaryName.equals(MPK_FIELD) || dictionaryName.equals(MPK_FINAL_FIELD)) {
                SessionUtils.putToSession(documentId, REALIZATION_DIVISION_CODE_SESSION_KEY, "");
                DwrUtils.setRefValueEnumOptions(values, result, "WNIOSKODAWCA_JEDNOSTKA", dictionaryNameWithPrefix, OKRES_BUDZETOWY, UnitBudget.UNIT_BUDGET_VIEW, ObjectUtils.toString(fieldsValues.get("WNIOSKODAWCA_JEDNOSTKA"), ""), EMPTY_ENUM_VALUE, "_1", "", null, DOCKIND_CN);
                DwrUtils.setRefValueEnumOptions(values, result, OKRES_BUDZETOWY, dictionaryNameWithPrefix, "KOMORKA_BUDZETOWA", BudgetUnitOrganizationUnit.BUDGET_UNIT_VIEW, null, EMPTY_ENUM_VALUE, "_1", "_1", null, DOCKIND_CN);
                DwrUtils.setRefValueEnumOptions(values, result, OKRES_BUDZETOWY_BK, dictionaryNameWithPrefix, "KOMORKA_BUDZETOWA_BK", BudgetUnitOrganizationUnit.BUDGET_UNIT_FOR_APPLICATION_VIEW, null, EMPTY_ENUM_VALUE, "_1", "_1", null, DOCKIND_CN);
                DwrUtils.setRefValueEnumOptions(values, result, "KOMORKA_BUDZETOWA", dictionaryNameWithPrefix, "IDENTYFIKATOR_PLANU", PurchasePlan.PURCHASE_PLAN_VIEW, null, EMPTY_ENUM_VALUE, "_1", "_1", null, DOCKIND_CN);
                DwrUtils.setRefValueEnumOptions(values, result, "KOMORKA_BUDZETOWA_BK", dictionaryNameWithPrefix, BUDZET, UnitBudgetKo.VIEW_NAME, null, EMPTY_ENUM_VALUE, "_1", "_1", null, DOCKIND_CN);

                String positionId = null;
                if (PURCHASE_PLAN_RESERVATION_TYPE_ID.equals(reservationTypeId)) {
                    reloadRefEnumItems(values, result, dictionaryName, POZYCJA_PLANU_ZAKUPOW_RAW, "IDENTYFIKATOR_PLANU", DOCKIND_CN, EMPTY_ENUM_VALUE, null);
                    EnumValues positionEnum = (EnumValues) result.get(dictionaryNameWithPrefix + POZYCJA_PLANU_ZAKUPOW_RAW);
                    if (positionEnum != null && !Strings.isNullOrEmpty(positionEnum.getSelectedId())) {
                        String realizationDivisionCode = ExternalSourceEnumItem.makeEnumItem(positionEnum.getSelectedId()).getArg(1);
                        SessionUtils.putToSession(documentId, REALIZATION_DIVISION_CODE_SESSION_KEY, realizationDivisionCode);
                    }
                    positionId = result.get(dictionaryName + "_" + POZYCJA_PLANU_ZAKUPOW_RAW) != null ? ((EnumValues) result.get(dictionaryName + "_" + POZYCJA_PLANU_ZAKUPOW_RAW)).getSelectedId() : null;
                } else if (BUDGET_RESERVATION_TYPE_ID.equals(reservationTypeId)) {
                    reloadRefEnumItems(values, result, dictionaryName, POZYCJA_BUDZETU_RAW, BUDZET, DOCKIND_CN, EMPTY_ENUM_VALUE, null);
                    positionId = result.get(dictionaryName + "_" + POZYCJA_BUDZETU_RAW) != null ? ((EnumValues) result.get(dictionaryName + "_" + POZYCJA_BUDZETU_RAW)).getSelectedId() : null;
                }

                Integer itemNo = values.get(dictionaryName + "_" + "POZYCJAID") != null ? values.get(dictionaryName + "_" + "POZYCJAID").getIntegerData() : null;
                Map<Integer, BigDecimal> position = SessionUtils.getFromSession(documentId, POSITION_WITH_AMOUNT, new HashMap<Integer, BigDecimal>());

                BigDecimal positionAmount = values.get(dictionaryNameWithPrefix + "BRUTTO") != null && values.get(dictionaryNameWithPrefix + "BRUTTO").getMoneyData() != null ?
                        values.get(dictionaryNameWithPrefix + "BRUTTO").getMoneyData()
                        : null;

                if (itemNo != null) {
                    Map<Integer, String> selectedBudgetPostions = SessionUtils.getFromSession(documentId, SELECTED_BUDGET_POSITION, new HashMap<Integer, String>());
                    selectedBudgetPostions.put(itemNo, positionId);
                    SessionUtils.putToSession(documentId, SELECTED_BUDGET_POSITION, selectedBudgetPostions);
                    position.put(itemNo, positionAmount);
                    SessionUtils.putToSession(documentId, POSITION_WITH_AMOUNT, position);
                }

                String productRawId = Objects.firstNonNull(DwrUtils.getStringValue(values, dictionaryNameWithPrefix + PRODUKT_FIELD), "");
                Integer productId = Ints.tryParse(productRawId);
                if (productId != null) {
                    EnumItem productEnumItem = DataBaseEnumField.getEnumItemForTable(PRODUCT_VIEW, productId);
                    result.put(dictionaryNameWithPrefix + JM_FIELD, productEnumItem.getRefValue());
                    result.put(dictionaryNameWithPrefix + OPIS_FIELD, NIE_DOTYCZY_PRODUCT_LABEL);
                }
            } else {
                SessionUtils.clearSession(documentId, REALIZATION_DIVISION_CODE_SESSION_KEY);
            }

            if (dictionaryName.equals(ZADANIA_FIELD) || dictionaryName.equals(ZADANIA_FINAL_FIELD) || dictionaryName.equals(ZRODLA_FIELD) || dictionaryName.equals(ZRODLA_FINAL_FIELD) ) {
                boolean fsDict = dictionaryName.equals(ZRODLA_FIELD) || dictionaryName.equals(ZRODLA_FINAL_FIELD);

                Map<Integer, String> selectedBudgetPositions = SessionUtils.getFromSession(documentId, SELECTED_BUDGET_POSITION, new HashMap<Integer, String>());
                Map<Integer, BigDecimal> positionWithAmount = SessionUtils.getFromSession(documentId, POSITION_WITH_AMOUNT, new HashMap<Integer, BigDecimal>());

                Integer itemNo = values.get(dictionaryName + "_" + "POZYCJAID") != null ? values.get(dictionaryName + "_" + "POZYCJAID").getIntegerData() : null;

                if (itemNo != null) {
                    String budgetPositionId = selectedBudgetPositions.get(itemNo);

                    if (PURCHASE_PLAN_RESERVATION_TYPE_ID.equals(reservationTypeId) && !fsDict) {
                        reloadRefEnumItems(values, result, dictionaryName, ZADANIE_FINANSOWE_PLANU_RAW, "POZYCJA_PLANU_ZAKUPOW", DOCKIND_CN, EMPTY_ENUM_VALUE, budgetPositionId);
                    } else if(BUDGET_RESERVATION_TYPE_ID.equals(reservationTypeId) && !fsDict) {
                        reloadRefEnumItems(values, result, dictionaryName, ZADANIE_FINANSOWE_BUDZETU_RAW, POZYCJA_BUDZETU_RAW, DOCKIND_CN, EMPTY_ENUM_VALUE, budgetPositionId);
                    } else if(BUDGET_RESERVATION_TYPE_ID.equals(reservationTypeId) && fsDict) {
                        reloadRefEnumItems(values, result, dictionaryName, ZRODLO_BUDZETU_RAW, POZYCJA_BUDZETU_RAW, DOCKIND_CN, EMPTY_ENUM_VALUE, budgetPositionId);
                    }

                    if (positionWithAmount.get(itemNo) != null) {
                        if (values.get(dictionaryNameWithPrefix + "BRUTTO") != null && values.get(dictionaryNameWithPrefix + "BRUTTO").isSender()) {
                            BigDecimal grossAmount = values.get(dictionaryNameWithPrefix + "BRUTTO").getMoneyData();

                            if (grossAmount != null) {
                                result.put(dictionaryNameWithPrefix + "PROCENT", grossAmount.divide(positionWithAmount.get(itemNo), 10, RoundingMode.HALF_UP).movePointRight(2).setScale(2, RoundingMode.HALF_UP));
                            }
                        } else if (values.get(dictionaryNameWithPrefix + "PROCENT") != null) {
                            BigDecimal percentAmount = values.get(dictionaryNameWithPrefix + "PROCENT").getMoneyData();

                            if (percentAmount != null) {
                                result.put(dictionaryNameWithPrefix + "BRUTTO", percentAmount.movePointLeft(2).multiply(positionWithAmount.get(itemNo).setScale(2, RoundingMode.HALF_UP)));
                            }
                        }
                    } else {
                        result.put(dictionaryNameWithPrefix + "BRUTTO", BigDecimal.ZERO);
                    }
                }

            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    void reloadRefEnumItems(Map<String, ?> values, Map<String, Object> results, String dictionaryCn, final String fieldCn, final String refFieldCn, String dockindCn, EnumValues emptyHiddenValue, String refFieldSelectedId) {
        try {
            Optional<pl.compan.docusafe.core.dockinds.field.Field> resourceField = findField(dockindCn, dictionaryCn, fieldCn + "_1");

            if (resourceField.isPresent()) {
                String dicWithSuffix = dictionaryCn + "_";
                if (refFieldSelectedId == null) {
                    refFieldSelectedId = getFieldSelectedId(values, results, dicWithSuffix, refFieldCn);
                }

                String fieldSelectedId = "";
                EnumValues enumValue = getEnumValues(values, dicWithSuffix + fieldCn);
                if (enumValue != null) {
                    if (!enumValue.getSelectedOptions().isEmpty()) {
                        fieldSelectedId = enumValue.getSelectedOptions().get(0);
                    }
                }

                EnumValues enumValues = resourceField.get().getEnumItemsForDwr(ImmutableMap.<String, Object>of(refFieldCn + "_1", ObjectUtils.toString(refFieldSelectedId), fieldCn + "_1", ObjectUtils.toString(fieldSelectedId)));

                if (isEmptyEnumValues(enumValues)) {
                    results.put(dicWithSuffix + fieldCn, emptyHiddenValue);
                } else {
                    results.put(dicWithSuffix + fieldCn, enumValues);
                }
            } else {
                throw new EdmException("cant find " + fieldCn + " field");
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }

    static boolean isEmptyEnumValues(EnumValues enumValues) {
        return enumValues.getAllOptions().isEmpty() || (enumValues.getAllOptions().size() == 1 && enumValues.getAllOptions().get(0).containsKey(""));
    }

    static String getFieldSelectedId(Map<String, ?> values, Map<String, Object> results, String dic, String fieldCn) {
        String fieldSelectedId = "";
        if (results.containsKey(dic + fieldCn)) {
            try {
                fieldSelectedId = ((EnumValues)results.get(dic + fieldCn)).getSelectedId();
            } catch (Exception e) {
                log.error("CAUGHT "+e.getMessage(),e);
            }
        } else {
            try {
                EnumValues enumValue = getEnumValues(values, dic + fieldCn);
                if (enumValue != null) {
                    fieldSelectedId = enumValue.getSelectedOptions().get(0);
                }
            } catch (Exception e) {
                log.error("CAUGHT " + e.getMessage(), e);
            }
        }

        return fieldSelectedId;
    }

    public static EnumValues getEnumValues(Map<String, ?> values, String fieldName) {
        Object value = values.get(fieldName);
        if (value instanceof FieldData) {
            return ((FieldData) value).getEnumValuesData();
        } else if (value instanceof EnumValues) {
            return (EnumValues) value;
        }
        return null;
    }

    static Optional<pl.compan.docusafe.core.dockinds.field.Field> findField(String dockindCn, String dictionaryCn, final String dictionaryFieldCn) throws EdmException {
        return Iterables.tryFind(DocumentKind.findByCn(dockindCn).getFieldsMap().get(dictionaryCn).getFields(), new Predicate<pl.compan.docusafe.core.dockinds.field.Field>() {
            @Override
            public boolean apply(pl.compan.docusafe.core.dockinds.field.Field input) {
                return dictionaryFieldCn.equals(input.getCn());
            }
        });
    }


    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) throws EdmException {
        AssigneeLogic assigneer = ASSIGNEE_LOGIC_MAP.get(acceptationCn);
        if (assigneer != null) {
            FieldsManager fm = doc.getFieldsManager();
            try {
                assigneer.assign(doc, fm, assignable, openExecution);
            } catch (AssigningHandledException e) {
                AssignUtils.addRemarkAndAssignToAdmin(doc, assignable, e.getMessage());
            } catch (Exception e) {
                AssignUtils.addRemarkAndAssignToAdmin(doc, assignable, "Nie znaleziono osoby do przypisania, przypisano do admin'a");
                log.error(e.getMessage(), e);
            } finally {
                return true;
            }
        } else {
            return super.assignee(doc, assignable, openExecution, acceptationCn, fieldCnValue);
        }
    }

    static class ApplicationDivisionManager implements AssigneeLogic {

        @Override
        public void assign(OfficeDocument doc, FieldsManager fm, Assignable assignable, OpenExecution openExecution) throws EdmException {
            List<Long> userIds = IsCostFactureDecision.findManagerUserIdFromDivisions(Lists.newArrayList(fm.getEnumItemCn(WNIOSKODAWCA_JEDNOSTKA)));

            AssignUtils.checkState(userIds != null && !userIds.isEmpty(), "Nie znaleziono kierownika dla danej jednostki");

            for (Long userId : userIds) {
                String username = DSUser.findById(userId).getName();
                assignable.addCandidateUser(username);
                AssigneeHandler.addToHistory(openExecution, doc, username, null);
            }
        }
    }
    
    static class ApplicationProjectAttendant implements AssigneeLogic {
    	@Override
        public void assign(OfficeDocument doc, FieldsManager fm, Assignable assignable, OpenExecution openExecution) throws EdmException {
	    	String attendant = (String) fm.getValue("OPIEKUN_PROJEKTU");
	    	String[] firstLastName = attendant.split(" ");
			if(DSUser.findByFirstnameLastname(firstLastName[1], firstLastName[0], false) != null) {
				assignable.addCandidateUser(DSUser.findByFirstnameLastname(firstLastName[1], firstLastName[0], false).getName());
				AssigneeHandler.addToHistory(openExecution, doc, DSUser.findByFirstnameLastname(firstLastName[1], firstLastName[0], false).getName(), null);
			}
    	}
    }

    static class Dysponent implements AssigneeLogic {

        @Override
        public void assign(OfficeDocument doc, FieldsManager fm, Assignable assignable, OpenExecution openExecution) throws EdmException {
            EnumItem enumItem = fm.getEnumItem(WNIOSKODAWCA_JEDNOSTKA);
            AssignUtils.checkState(enumItem != null, "Pole Jednostka wnioskuj�ca jest puste");
            String divisionCode = enumItem.getArg(1);
            List<String> userNames = DSApi.context().session().createSQLQuery("select cn from " + DYSPONENT_VIEW + " where available = 1 and refValue is not null and :divCode like refValue")
                    .setParameter("divCode", divisionCode).list();

            AssignUtils.checkState(!userNames.isEmpty(), "Nie mo�na odnale�� kierownika pionu dla dzia�u: "+divisionCode);

            for (String user : userNames) {
                AssignUtils.checkState(DSUser.findByUsername(user) != null, "Nie mo�na odnale�� kierownika pionu: " + user);
                assignable.addCandidateUser(user);
                AssigneeHandler.addToHistory(openExecution, doc, user, null);
            }
        }
    }

    private String getAdditionFieldsDescription(OfficeDocument doc, FieldsManager fm) throws EdmException {
        Map<String, String> additionFields = Maps.newLinkedHashMap();
        additionFields.put("Nr KO", doc.getOfficeNumber() != null ? doc.getOfficeNumber().toString() : "brak");
        additionFields.put("Numer sprawy", fm.getStringValue("NR_SPRAWY"));
        additionFields.put("Skr�cony opis", fm.getStringValue("NAZWA_ZAMOWIENIA"));
        additionFields.put("Uzasadnienie zakupu", fm.getStringValue("UZASADNIENIE"));
        additionFields.put("Rodzaj zam�wienia", fm.getStringValue("RODZAJ_ZAMOWIENIA"));
        additionFields.put("Tryb zam�wienia", fm.getStringValue("TRYB_ZAMOWIENIA"));
        additionFields.put("Opinia w sprawie VAT", fm.getStringValue("OPINIA_KWESTURY"));
        additionFields.put("Opinia w sprawie zadania finansowego", fm.getStringValue("OPINIA_ZADANIA_FINANSOWEGO"));
        additionFields.put("Uzasadnienie edycji wniosku", fm.getStringValue("UZASADNIENIE_EDYCJI_DZP"));

        StringBuilder description = new StringBuilder();
        for (String label : additionFields.keySet()) {
            if (!Strings.isNullOrEmpty(additionFields.get(label))) {
                description.append(label)
                        .append(": ")
                        .append(additionFields.get(label))
                        .append("; ");
            }
        }
        return description.toString();
    }

    @Override
    public ExportedDocument buildExportDocument(OfficeDocument doc) throws EdmException {
        FieldsManager fm = doc.getFieldsManager();

        ReservationDocument exported = new ReservationDocument();

        exported.setReservationStateFromId(Objects.firstNonNull(fm.getIntegerKey("ERP_STATUS"), 1));

        exported.setReservationNumber(fm.getStringKey(ERP_DOCUMENT_IDM));

        String mpkCode = fm.getEnumItem("WNIOSKODAWCA_JEDNOSTKA") != null ? fm.getEnumItem("WNIOSKODAWCA_JEDNOSTKA").getArg(0) : null;

        boolean editedByDZP = ((Integer) 1).equals(fm.getIntegerKey(CZY_EDYCJA_DZP));
        exported.setBudgetReservationTypeCode(editedByDZP ? fm.getEnumItemCn(TYP_REZERWACJI_FINAL) : fm.getEnumItemCn(TYP_REZERWACJI));
        exported.setReservationMethod(ReservationDocument.ReservationMethod.AUTO);

        String mpkDictionaryCn = editedByDZP ? MPK_FINAL_FIELD : MPK_FIELD;
        String taskDictionaryCn = editedByDZP ? ZADANIA_FINAL_FIELD : ZADANIA_FIELD;
        String fsDictionaryCn = editedByDZP ? ZRODLA_FINAL_FIELD : ZRODLA_FIELD;

        List<Map<String, Object>> positions = (List) fm.getValue(mpkDictionaryCn);
        String mpkDictionaryWithSuffix = mpkDictionaryCn + "_";
        List<? extends Number> finanancialTasksDictionaryIds = (List<? extends Number>) fm.getKey(taskDictionaryCn);
        List<? extends Number> fsDictionaryIds = (List<? extends Number>) fm.getKey(fsDictionaryCn);

        if (positions != null) {
            int i = 1;
            List<BudgetItem> budgetItems = Lists.newArrayList();
            BudgetItem bItem;
            for (Map<String, Object> position : positions) {
                budgetItems.add(bItem = new BudgetItem());

                bItem.setItemNo(i++);
                bItem.setName(ObjectUtils.toString(position.get(mpkDictionaryWithSuffix + "UWAGI"), null));

/*                if (position.get(mpkDictionaryWithSuffix + JM_FIELD) != null && !Strings.isNullOrEmpty(((EnumValues) position.get(mpkDictionaryWithSuffix + JM_FIELD)).getSelectedId())) {
                    EnumItem costKindEnum = DataBaseEnumField.getEnumItemForTable(UNIT_OF_MEASURE_TABLE, Ints.tryParse(((EnumValues) position.get(mpkDictionaryWithSuffix + JM_FIELD)).getSelectedId()));
                    bItem.setUnit(costKindEnum.getCn());
                }
                if (bItem.getQuantity() == null) {
                    bItem.setUnit("szt");
                }*/
                bItem.setUnit(ObjectUtils.toString(position.get(mpkDictionaryWithSuffix + JM_FIELD), "szt"));
                bItem.setQuantity((BigDecimal) position.get(mpkDictionaryWithSuffix + ILOSC_FIELD));
                bItem.setNetAmount(((BigDecimal) position.get(mpkDictionaryWithSuffix + "BRUTTO")).divide(((BigDecimal) position.get(mpkDictionaryWithSuffix + ILOSC_FIELD)), 2, RoundingMode.HALF_UP));

                Number itemNo = (Number) position.get(mpkDictionaryWithSuffix + "POZYCJAID");

                if (position.get(mpkDictionaryWithSuffix + "STAWKA") != null && !Strings.isNullOrEmpty(((EnumValues) position.get(mpkDictionaryWithSuffix + "STAWKA")).getSelectedId())) {
                    bItem.setVatRateCode(getCnValue(TABLENAME_WITH_VAT_RATES, position.get(mpkDictionaryWithSuffix + "STAWKA")));
                }

                if (position.get(mpkDictionaryWithSuffix + PRODUKT_FIELD) != null && !Strings.isNullOrEmpty(((EnumValues) position.get(mpkDictionaryWithSuffix + PRODUKT_FIELD)).getSelectedId())) {
                    EnumItem costKindEnum = DataBaseEnumField.getEnumItemForTable(PRODUCT_VIEW, Ints.tryParse(((EnumValues) position.get(mpkDictionaryWithSuffix + PRODUKT_FIELD)).getSelectedId()));
                    bItem.setCostKindCode(costKindEnum.getCn());
                    exported.setCostKindCodeAllowed(true);
                } else {
                    bItem.setCostKindName(ObjectUtils.toString(position.get(mpkDictionaryWithSuffix + OPIS_FIELD), ""));
                }

                bItem.setMpkCode(mpkCode);


                if (WOZ_RESERVATION_TYPE.equals(exported.getBudgetReservationTypeCode())) {
                    bItem.setPlanZamId(getEnumId(position.get(mpkDictionaryWithSuffix + IDENTYFIKATOR_PLANU)));
                    bItem.setRodzajPlanZamId(getExternalEnumId(position.get(mpkDictionaryWithSuffix + POZYCJA_PLANU_ZAKUPOW_RAW)));

                    if (exported.getHeaderBudget().getPlanZamId() == null) {
                        exported.getHeaderBudget().setPlanZamId(bItem.getPlanZamId());
                    }
                }

                if (WB_RESERVATION_TYPE.equals(exported.getBudgetReservationTypeCode())) {
                    bItem.setBudgetKoId(getEnumId(position.get(mpkDictionaryWithSuffix + BUDZET)));
                    bItem.setBudgetKindId(getExternalEnumId(position.get(mpkDictionaryWithSuffix + POZYCJA_BUDZETU_RAW)));

                    if (exported.getHeaderBudget().getBudgetKoId() == null) {
                        exported.getHeaderBudget().setBudgetKoId(bItem.getBudgetKoId());
                    }

/*                    FundSource fundSource = new FundSource();
                    fundSource.setCode(Docusafe.getAdditionPropertyOrDefault("export.reservation.default_fs", "_Brak"));
                    bItem.setFundSources(Lists.newArrayList(fundSource));*/
                }

/*                Number budgetId = (Number) DSApi.context().session()
                        .createSQLQuery("select " + (WB_RESERVATION_TYPE.equals(exported.getBudgetReservationTypeCode()) ? OKRES_BUDZETOWY_BK : OKRES_BUDZETOWY) + " from dsg_ams_mpk_dictionary where id=:id")
                        .setParameter("id", position.get("id"))
                        .uniqueResult();
                bItem.setBudgetDirectId(budgetId.longValue());

                if (exported.getHeaderBudget().getBudgetDirectId() == null) {
                    exported.getHeaderBudget().setBudgetDirectId(bItem.getBudgetDirectId());
                }*/
                //bItem.setBudgetDirectCode(getCnValue(WB_RESERVATION_TYPE.equals(exported.getBudgetReservationTypeCode()) ? "dsg_ams_view_budget_phase_no_ref" : "dsg_ams_view_budget_phase", position.get(MPK_FIELD_WITH_SUFFIX +
                                                    //(WB_RESERVATION_TYPE.equals(exported.getBudgetReservationTypeCode()) ? OKRES_BUDZETOWY_BK : OKRES_BUDZETOWY))));
                bItem.setBudgetDirectCode(Docusafe.getAdditionPropertyOrDefault("export.reservation.default_budget_idn", "B/2014/0001"));
                if (exported.getHeaderBudget().getBudgetDirectCode() == null) {
                    exported.getHeaderBudget().setBudgetDirectCode(bItem.getBudgetDirectCode());
                }

                PreparedStatement ps = null;
                ResultSet rs = null;
                try {
                    if (finanancialTasksDictionaryIds != null && !finanancialTasksDictionaryIds.isEmpty()) {
                        ps = DSApi.context().prepareStatement("select * from dsg_ams_mpk_dictionary where pozycjaid = ? and id in (" + Joiner.on(',').skipNulls().join(finanancialTasksDictionaryIds) + ")");
                        ps.setInt(1, itemNo.intValue());
                        rs = ps.executeQuery();

                        while (rs.next()) {
                            bItem.addFinanacialTask(
                                    new FinancialTask()
                                            .setTaskCode(getTaskFinancialCode(rs, exported.getBudgetReservationTypeCode()))
                                            .setPercent(rs.getBigDecimal("procent"))
                                            .setAmount(rs.getBigDecimal("brutto")));

                        }
                    }

                    if (fsDictionaryIds != null && !fsDictionaryIds.isEmpty()) {
                        ps = DSApi.context().prepareStatement("select * from dsg_ams_mpk_dictionary where pozycjaid = ? and id in (" + Joiner.on(',').skipNulls().join(fsDictionaryIds) + ")");
                        ps.setInt(1, itemNo.intValue());
                        rs = ps.executeQuery();

                        while (rs.next()) {
                            bItem.addFundSource(
                                    new FundSource()
                                            .setCode(getFundSourcesCode(rs))
                                            .setAmount(rs.getBigDecimal("brutto")));

                        }
                    }

                    validateFinancialTasksAndFundSources(bItem);


                } catch (EdmException e) {
                    log.error(e.getMessage(), e);
                } catch (SQLException e) {
                    log.error(e.getMessage(), e);
                } finally {
                    DbUtils.closeQuietly(ps);
                }

            }
            exported.setPostionItems(budgetItems);
        }

//wywalone na chiwle �eby testy przesz�y, �le uzupe�nia w xml-u pracownika wstawia idm a shema tego nie akceptuje 
//        DSUser cUser = DSUser.findByUsername(doc.getCreatingUser());
//        if (cUser.getExtension() != null) {
//            exported.setCreatingUser(cUser.getExtension());
//        }

/*        if (fm.getKey("SUPPLIER") != null) {
            exported.setExternalContractor(AMSSupplier.find(((Number) fm.getKey("SUPPLIER")).longValue()).getErpId().longValue());
        }*/

        exported.setCreated(doc.getCtime());

/*        exported.setOrderKind(fm.getEnumItem("RODZAJ_ZAMOWIENIA") != null ? fm.getEnumItem("RODZAJ_ZAMOWIENIA").getTitle() : null);
        exported.setOrderKind(fm.getEnumItem("TRYB_ZAMOWIENIA") != null ? fm.getEnumItem("TRYB_ZAMOWIENIA").getTitle() : null);
        exported.setOrderName(fm.getStringValue("NAZWA_ZAMOWIENIA"));
        exported.setOrderSubjectDescription(fm.getStringValue("SZCZEGOLOWY_OPIS"));*/
        exported.setTitle(fm.getStringValue("NR_SPRAWY"));

        exported.setDescription(getAdditionFieldsDescription(doc, fm));

        String systemPath = Docusafe.getBaseUrl();

        for (Attachment zal : doc.getAttachments()) {
            AttachmentInfo info = new AttachmentInfo();

            String path = systemPath + "/repository/view-attachment-revision.do?id=" + zal.getMostRecentRevision().getId();

            info.setName(zal.getTitle());
            info.setUrl(path);

            exported.addAttachementInfo(info);
        }

        return exported;
    }

    private String getFundSourcesCode(ResultSet rs) throws SQLException {
        String fsRawId = rs.getString("zrodlo_budzetu_raw");

        if (!Strings.isNullOrEmpty(fsRawId)) {
            return ExternalSourceEnumItem.makeEnumItem(fsRawId).getCn();
        }

        return null;
    }

    private void validateFinancialTasksAndFundSources(BudgetItem bItem) throws EdmException {
        //TODO
        return;
    }

    private String getTaskFinancialCode(ResultSet rs, String budgetReservationTypeCode) throws SQLException {
        String financialTaskRawId = null;
        if (WOZ_RESERVATION_TYPE.equals(budgetReservationTypeCode)) {
            financialTaskRawId = rs.getString("zadanie_finansowe_planu_raw");
        }

        if (WB_RESERVATION_TYPE.equals(budgetReservationTypeCode)) {
            financialTaskRawId = rs.getString("zadanie_finansowe_budzetu_raw");
        }

        if (!Strings.isNullOrEmpty(financialTaskRawId)) {
            return ExternalSourceEnumItem.makeEnumItem(financialTaskRawId).getCn();
        }

        return null;
    }

    private Long getExternalEnumId(Object field) {
        if (field != null && !Strings.isNullOrEmpty(((EnumValues) field).getSelectedId())) {
            return ExternalSourceEnumItem.makeEnumItem(((EnumValues) field).getSelectedId()).getId().longValue();
        }

        return null;
    }

    private Long getEnumId(Object field) {
        return field != null && !Strings.isNullOrEmpty(((EnumValues) field).getSelectedId()) ? Long.valueOf(((EnumValues) field).getSelectedId()) : null;
    }

    @Override
    public void setAdditionalTemplateValues(long docId, Map<String, Object> values, Map<Class, Format> defaultFormatters) {

	    try {
		    Document doc = Document.find(docId);
		    FieldsManager fm = doc.getFieldsManager();
		    fm.initialize();

		    Pattern pattern;
		    Matcher matcher;

		    String recipient = (String) fm.getValue("RECIPIENT_HERE");
		    if (recipient != null) {
			    pattern = Pattern.compile("(.+),(.+)");
			    matcher = pattern.matcher(recipient);
			    if (matcher.find()) {
				    values.put("RECIPIENT", matcher.group(1));
				    values.put("OSOBA_REALIZUJACA_ZAM", matcher.group(2));
			    } else {
				    values.put("RECIPIENT", recipient);
			    }
		    }

		    Map suplier = (HashMap<String, String>) fm.getValue("SUPPLIER");
            values.put("SPRZEDAWCA", suplier.get("SUPPLIER_NAZWA"));

		    values.put("PODPIS_WNIOSKODAWCY", getAcceptanceByCn(doc.getId(), "compatibility_acceptation"));
		    values.put("AKCEPTACJA_DYSPONENTU_SRODKOW", getAcceptanceByCn(doc.getId(), "dysponent"));
		    values.put("PODPIS_KIEROWNIKA_PIONU", getAcceptanceByCn(doc.getId(), "manager_division_opinion"));
			values.put("PODPIS_DZIALU_ZAMOWIEN_PUBLICZNYCH", getAcceptanceByCn(doc.getId(), "dzp_accept"));
		    values.put("PODPIS_JEDNOSTKI_REALIZUJACEJ_ZAMOWIENIE", getAcceptanceByCn(doc.getId(), "confirmation_jr"));

		    values.put("FINANCIAL_TASKS", fm.getValue("ZADANIA"));

		    Integer applicantId = (Integer) fm.getKey("WNIOSKODAWCA");
		    if (applicantId != null && applicantId > 0) {
			    DSUser applicant = DSUser.findById(applicantId.longValue());
                values.put("WNIOSKODAWCA_NAZWA", applicant.asLastnameFirstname());
                values.put("WNIOSKODAWCA_POKOJ", applicant.getRoomNum());
                values.put("WNIOSKODAWCA_EMAIL", applicant.getEmail());
		    }

//		    $OSOBA_REALIZUJACA_ZAM
//		    $OSOBA_REALIZUJACA_ZAM_POK
//		    $OSOBA_REALIZUJACA_ZAM_TEL


		    values.put("mpkPozycjaPlanuZakupowSet", getMpkPozycjaPlanuZakupow(fm));
		    values.put("bruttoSum", getBruttoSum(fm));
		    values.put("nettoSum", fm.getValue("SUMA_NETTO"));

		    DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		    symbols.setDecimalSeparator(',');
		    defaultFormatters.put(BigDecimal.class, new DecimalFormat("### ### ##0.00", symbols));
		    defaultFormatters.put(Date.class, new SimpleDateFormat("dd-MM-yyyy"));
		    defaultFormatters.put(Timestamp.class, new SimpleDateFormat("dd-MM-yyyy"));

		    String uzasadnienieEdycjiDzp = (String) fm.getValue("UZASADNIENIE_EDYCJI_DZP");
		    values.put("UZASADNIENIE_EDYCJI_DZP", uzasadnienieEdycjiDzp);

		    values.put("DOC_DATE", doc.getCtime());
		    values.put("L_Z", getNumberOfAttachment(docId));
	    } catch (EdmException e) {
		    log.error(e.getMessage(), e);
	    }
    }

	/**
	 * @param documentId
	 * @param acceptanceCn
	 * @return
	 */
	private String getAcceptanceByCn(Long documentId, String acceptanceCn) {

		String output = "";

		PreparedStatement ps = null;

		try {
			String sql = "SELECT username FROM ds_document_acceptance WHERE document_id=? AND acceptanceCn=?";

			ps = DSApi.context().prepareStatement(sql);
			ps.setLong(1, documentId);
			ps.setString(2, acceptanceCn);
			ResultSet rs = ps.executeQuery();
			if (!rs.next()) {
				return "";
			}

			String userName = rs.getString(1);

			DSUser user = DSUser.findByUsername(userName);
			output = user.asFirstnameLastname();
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		} finally {
			DbUtils.closeQuietly(ps);
		}

		return output;
	}

	/**
	 * Oblicza sume brutto zamowienia
	 *
	 * @param fm
	 * @return
	 * @throws EdmException
	 */
	private BigDecimal getBruttoSum(FieldsManager fm) throws EdmException {

		BigDecimal bruttoSum = new BigDecimal(0).setScale(MONEY_SCALE);

		List mpkList = (LinkedList) fm.getValue("MPK");
		for (Object element : mpkList) {
			Map map = (Map) element;
			BigDecimal mpkBrutto = (BigDecimal) map.get("MPK_BRUTTO");
			if (mpkBrutto != null) {
				mpkBrutto = mpkBrutto.setScale(MONEY_SCALE);
				bruttoSum = bruttoSum.add(mpkBrutto);
			}
		}

		return bruttoSum;
	}

	/**
	 * Oblicza sume netto z podanych kwot brutto i stawek zamowienia
	 *
	 * @param values
	 * @return
	 * @throws EdmException
	 */
	private BigDecimal getNettoSum(Map values) throws EdmException {

		BigDecimal nettoSum = new BigDecimal(0).setScale(MONEY_SCALE);
		BigDecimal oneHundred = new BigDecimal(100).setScale(MONEY_SCALE);

		Pattern pattern;
		Matcher matcher;

		FieldData fieldData = (FieldData) values.get("DWR_MPK");
		Map<String, Object> dwrMpk = (HashMap<String, Object>) fieldData.getData();
		for (String key : dwrMpk.keySet()) {
			pattern = Pattern.compile("MPK_BRUTTO_(\\d+)");
			matcher = pattern.matcher(key);
			if (matcher.find()) {
				Integer index = Integer.valueOf(matcher.group(1));
				FieldData fieldDataBrutto = (FieldData) dwrMpk.get(key);
				BigDecimal mpkBrutto = (BigDecimal) fieldDataBrutto.getData();
				if (mpkBrutto != null) {
					mpkBrutto = mpkBrutto.setScale(MONEY_SCALE);
					String rateKeyName = "MPK_STAWKA_" + index;
					FieldData rateKeyNameFieldValue = (FieldData) dwrMpk.get(rateKeyName);
					String mpkStawka = (String) rateKeyNameFieldValue.getData();
					if (mpkStawka != null && !mpkStawka.equals("")) {
						StringBuilder query = new StringBuilder();
						query.append("SELECT procent FROM ")
								.append(TABLENAME_WITH_VAT_RATES)
								.append(" WHERE id = ")
								.append(mpkStawka);
						BigDecimal vatRate = ((BigDecimal) DSApi.context().session().createSQLQuery(query.toString()).uniqueResult()).setScale(MONEY_SCALE);
						vatRate = vatRate.add(oneHundred).divide(oneHundred, MONEY_SCALE);
						BigDecimal mpkNetto = mpkBrutto.divide(vatRate, MONEY_SCALE);
						nettoSum = nettoSum.add(mpkNetto);
					} else {
						nettoSum = nettoSum.add(mpkBrutto);
					}
				}
			}
		}

		return nettoSum;
	}

	/**
	 * Zwraca liste pozycji planu zakupowego
	 *
	 * @param fm
	 * @return
	 * @throws EdmException
	 */
	private Set<String> getMpkPozycjaPlanuZakupow(FieldsManager fm) throws EdmException {

		Pattern pattern;
		Matcher matcher;

		List mpkList = (LinkedList) fm.getValue("MPK");
		Set<String> mpkPozycjaPlanuZakupowSet = new HashSet<String>();

		for (Object element : mpkList) {
			Map map = (Map) element;
			EnumValues mpkPozycjaPlanuZakupowEnum = (EnumValues) map.get("MPK_POZYCJA_PLANU_ZAKUPOW_RAW");
			if (mpkPozycjaPlanuZakupowEnum != null) {
				String mpkPozycjaPlanuZakupow = mpkPozycjaPlanuZakupowEnum.getSelectedValue();
				if (mpkPozycjaPlanuZakupow != null) {
					pattern = Pattern.compile("([.A-Z0-9]+):(.+)");
					matcher = pattern.matcher(mpkPozycjaPlanuZakupow);
					if (matcher.find()) {
						mpkPozycjaPlanuZakupowSet.add(matcher.group(1));
					}
				}
			}
		}

		return mpkPozycjaPlanuZakupowSet;
	}

    @Override
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException {
    }

    private String getNumberOfAttachment(long documentId) {
        try {
            Document doc = Document.find(documentId, false);
            return String.valueOf(doc.getAttachments().size());
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            return "";
        }
    }

    private static class RealizationDivision implements AssigneeLogic {
        @Override
        public void assign(OfficeDocument doc, FieldsManager fm, Assignable assignable, OpenExecution openExecution) throws EdmException {

            String divGuid = getRealizationDivisionGuid(fm);
            AssignUtils.checkState(divGuid != null, "Nie wybrano jednostki realizuj�cej");

            assignable.addCandidateGroup(divGuid);
            AssigneeHandler.addToHistory(openExecution, doc, null, divGuid);
        }
    }

    private static String getRealizationDivisionGuid(FieldsManager fm) throws EdmException {
        boolean editedByDZP = ((Integer) 1).equals(fm.getIntegerKey(CZY_EDYCJA_DZP));
        String reservationCode = editedByDZP ? fm.getEnumItemCn(TYP_REZERWACJI_FINAL) : fm.getEnumItemCn(TYP_REZERWACJI);

        return fm.getEnumItemCn(editedByDZP || ZamowienieLogic.WOZ_RESERVATION_TYPE.equals(reservationCode) ? ZamowienieLogic.JEDNOSTKA_REALIZUJACA_AUTO : ZamowienieLogic.JEDNOSTKA_REALIZUJACA);
    }

    @Override
    public TaskListParams getTaskListParams(DocumentKind kind, long documentId, TaskSnapshot task) throws EdmException {
        TaskListParams taskListParams = TaskListParams.fromDockindProperties(kind, documentId);
        FieldsManager fm = kind.getFieldsManager(documentId);

        task.setDecretation_to_div(fm.getStringValue(WNIOSKODAWCA_JEDNOSTKA));

        boolean editedByDZP = ((Integer) 1).equals(fm.getIntegerKey(CZY_EDYCJA_DZP));

        String description = generateFinancialTaskShortDescription(fm, editedByDZP);
        taskListParams.setAttribute(description, 4);

        BigDecimal amount = sumApplicationPositionsAmount(fm, editedByDZP);
        taskListParams.setAmount(amount);

        return taskListParams;
    }

    private String generateFinancialTaskShortDescription(FieldsManager fm, boolean editedByDZP) throws EdmException {
        final String taskDictionaryCn = editedByDZP ? ZADANIA_FINAL_FIELD : ZADANIA_FIELD;
        final String budgetReservationTypeCode = editedByDZP ? fm.getEnumItemCn(TYP_REZERWACJI_FINAL) : fm.getEnumItemCn(TYP_REZERWACJI);
        final String financialTaskFieldCn = WOZ_RESERVATION_TYPE.equals(budgetReservationTypeCode) ? ZADANIE_FINANSOWE_PLANU_RAW : ZADANIE_FINANSOWE_BUDZETU_RAW;

        List<Map<String,Object>> fsDictionary = (List<Map<String, Object>>) fm.getValue(taskDictionaryCn);

        final AtomicInteger fsSize = new AtomicInteger(0);
        Iterable<Object> ft = Iterables.transform(fsDictionary, new Function<Map<String, Object>, Object>() {
            @Override
            public Object apply(Map<String, Object> input) {
                EnumValues enumValues = (EnumValues) input.get(taskDictionaryCn + "_" + financialTaskFieldCn);
                String result = enumValues != null ? enumValues.getSelectedValue() : null;
                if (result != null) {
                    fsSize.incrementAndGet();
                }
                return result;
            }
        });

        return fsSize.get() > 0 ? "wiele" : Joiner.on(", ").skipNulls().join(ft);
    }

    private BigDecimal sumApplicationPositionsAmount(FieldsManager fm, boolean editedByDZP) throws EdmException {
        String mpkDictionaryCn = editedByDZP ? MPK_FINAL_FIELD : MPK_FIELD;

        List<Map<String,Object>> mpkDictionary = (List<Map<String, Object>>) fm.getValue(mpkDictionaryCn);

        BigDecimal amount = BigDecimal.ZERO;
        for (Map<String, Object> item : mpkDictionary) {
            amount = amount.add(item.get(mpkDictionaryCn + "_" + "BRUTTO") != null ? (BigDecimal) item.get(mpkDictionaryCn + "_" + "BRUTTO") : BigDecimal.ZERO);
        }
        return amount;
    }
}
