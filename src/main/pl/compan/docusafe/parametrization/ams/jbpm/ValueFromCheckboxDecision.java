/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.BooleanDecisionHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class ValueFromCheckboxDecision implements DecisionHandler {
	private static final Logger logger = LoggerFactory.getLogger(ValueFromCheckboxDecision.class);

	private String field;

	public String decide(OpenExecution openExecution) 
	{
		try
		{
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			Boolean cn = fm.getBoolean(field);
			if(cn == null) {
				return "false";
			}
			return Boolean.toString(cn);
		} 
		catch (EdmException ex) 
		{
			logger.error(ex.getMessage(), ex);
			return "false";
		}
	}
}
