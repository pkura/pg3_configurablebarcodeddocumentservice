/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ams.hb.AmsAssetRecordSystemTypeForOperationType;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 08-04-2014
 * ds_trunk_2014
 * LikwidacjaRsmTypeDecision.java
 */
public class LikwidacjaRsmTypeDecision implements DecisionHandler {

	private final static Logger LOG = LoggerFactory.getLogger(LikwidacjaRsmTypeDecision.class);
	
	private final static String TYP_OPERACJI = "TYP_OPERACJI";
	private final static String POZABILANSOWY = "POZABILANSOWY";
	
	@Override
	public String decide(OpenExecution openExecution) {
		boolean hasOpen = true;
		try
		{
            if (!DSApi.isContextOpen())
            {
                hasOpen = false;
                DSApi.openAdmin();
            }
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			String typOperacjiCn = fm.getEnumItemCn(TYP_OPERACJI);
			if (typOperacjiCn != null && typOperacjiCn.length() > 0) {
				AmsAssetRecordSystemTypeForOperationType operationRecSysType = AmsAssetRecordSystemTypeForOperationType.findBytypOpeIdn(typOperacjiCn);
				if (operationRecSysType.getTypSysewiIdn().equals(POZABILANSOWY)) {
					return "POZABILANSOWY";
				} else {
					return "OTHER";
				}
			}
			return "ERROR";
		}
		catch (EdmException ex)
		{
			LOG.error(ex.getMessage(), ex);
			return "ERROR";
		} finally {
            if (DSApi.isContextOpen() && !hasOpen)
                DSApi._close();
        }
	}

}
