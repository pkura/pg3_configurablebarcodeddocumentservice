package pl.compan.docusafe.parametrization.ams.jbpm;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ams.ProjMnarLogic;

import javax.validation.UnexpectedTypeException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ZbiorOpiniiDecision implements DecisionHandler {
    private String opinionRequired;

    @Override
    public String decide(OpenExecution execution) {
        if (StringUtils.isBlank(opinionRequired)) throw new IllegalArgumentException("Brak nazwy wymaganej opinii");
        Set<String> requiredOpinions = checkOpinionRequired(execution);
        return requiredOpinions.contains(opinionRequired) ? "required" : "normal";
    }

    protected Set<String> checkOpinionRequired(OpenExecution execution) {
        String processVar = (String) execution.getVariable(ProjMnarLogic.ACCEPTATION_CN_FORKHEADER_OPINIA);
        if (processVar.startsWith(ProjMnarLogic.PROCESS_VAR_OPINIA_JEDN_DODATKOWEJ)) {
            return Collections.singleton(ProjMnarLogic.PROCESS_VAR_OPINIA_JEDN_DODATKOWEJ);
        } else if (processVar.startsWith(ProjMnarLogic.PROCESS_VAR_OPINIA_CTTM_LUB_NM)) {
            try {
                OfficeDocument doc = getOfficeDocument(execution);
                FieldsManager fm = doc.getFieldsManager();

                Set<String> requiredOpinions = new HashSet<String>();
                Set<ProjMnarLogic.DzialKierownikaDlaZrodlaFinansowania> dzialy = ProjMnarLogic.getDzialyKierownikowDlaZrodelFinansowania(fm);
                for (ProjMnarLogic.DzialKierownikaDlaZrodlaFinansowania d : dzialy) {
                    switch (d) {
                        case CTTM:
                            requiredOpinions.add(ProjMnarLogic.OPINIA_CTTM);
                            break;
                        case NM:
                            requiredOpinions.add(ProjMnarLogic.OPINIA_NM);
                            break;
                        default:
                            throw new UnexpectedTypeException("Nieoczekiwany rodzaj kierownika odpowiadaj�cego �r�dle finansowania: " + d.name());
                    }
                }
                return requiredOpinions;

            } catch (Exception e) {
                throw new UnexpectedTypeException("Nie mo�na okre�li� kierownika odpowiadaj�cego �r�dle finansowania: " + e.getMessage());
            }
        } else {
            throw new UnexpectedTypeException("Nie mo�na okre�li�, do kogo ma by� skierowane zadanie: " + processVar);
        }
    }

    public static OfficeDocument getOfficeDocument(OpenExecution execution) throws EdmException {
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);
        return doc;
    }
}
