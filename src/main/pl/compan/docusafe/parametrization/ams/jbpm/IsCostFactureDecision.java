/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.jbpm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class IsCostFactureDecision implements DecisionHandler {

	private static final Logger logger = LoggerFactory.getLogger(IsCostFactureDecision.class);
	
	public static final String MPK_FIELD = "MPK";
	
	public static final Long SUPERVISOR_PROFILE_ID = 1L;
	
	private String field;
	
	public String decide(OpenExecution openExecution) {
		try
		{
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			
			List<Long> idList = (List<Long>) fm.getKey(MPK_FIELD);
			if (idList != null && !idList.isEmpty()) {
				List<String> guidList = (List<String>) DSApi.context().session().createSQLQuery("SELECT guid FROM DS_DIVISION WHERE id IN (SELECT jednostka_wnioskujaca FROM dsg_ams_mpk_dictionary WHERE id IN (:idList))")
					.setParameterList("idList", idList)
					.list();
				if (!guidList.isEmpty()) {
                    List<Long> ids = findManagerUserIdFromDivisions(guidList);
					if (!ids.isEmpty()) {
						openExecution.setVariable("ids", ids);
						openExecution.setVariable("count", ids.size());
						return "cost";
					} else {
						return "error";
					}
				} else {
					return "other";
				}
			} else {
				return "other";
			}
		} 
		catch (EdmException ex) 
		{
			logger.error(ex.getMessage(), ex);
			return "error";
		}
	}

    public static List<Long> findManagerUserIdFromDivisions(List<String> guidsList) throws EdmException {
    	String profileId = Docusafe.getAdditionProperty("SUPERVISOR_PROFILE_ID");
        List<BigDecimal> userIds = (List<BigDecimal>) DSApi.context().session().createSQLQuery("select DISTINCT u.ID from DS_DIVISION d join DS_USER_TO_DIVISION ud on d.ID = ud.DIVISION_ID join DS_USER u on ud.USER_ID = u.ID join ds_profile_to_user pu on u.ID = pu.user_id where pu.profile_id = :profileId and d.GUID IN (:guidList) and u.DELETED = 0")
            .setParameterList("guidList", guidsList)
            .setParameter("profileId", profileId)
            .list();

        // przekonwertowanie listy id userow o typie BigDecimal na typ Long
        List<Long> ids = new ArrayList<Long>();
        for (BigDecimal id : userIds) {
            ids.add(id.longValue());
        }
        return ids;
    }

}
