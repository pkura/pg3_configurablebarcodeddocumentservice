/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class OneOrTwoTrusteeDecision implements DecisionHandler {

	private static final Logger logger = LoggerFactory.getLogger(OneOrTwoTrusteeDecision.class);
	
	public static final String DYSPONENT_ZRODLOWY_FIELD_CN = "DYSPONENT_ZRODLOWY";
	public static final String DYSPONENT_DOCELOWY_FIELD_CN = "DYSPONENT_DOCELOWY";
	
	public String decide(OpenExecution openExecution) {
		try {
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			
			Integer sourceTrusteeId = (Integer) fm.getKey(DYSPONENT_ZRODLOWY_FIELD_CN);
			Integer finalTrusteeId = (Integer) fm.getKey(DYSPONENT_DOCELOWY_FIELD_CN);
			
			if (sourceTrusteeId.equals(finalTrusteeId)) {
				return "one";
			} else {
				return "two";
			}
			
		} catch (EdmException e) {
			return "error";
		}
	}

}
