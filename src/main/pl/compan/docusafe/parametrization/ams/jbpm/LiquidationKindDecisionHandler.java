/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.jbpm;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import com.google.common.collect.ImmutableList;

import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.DataBaseEnumFieldAssigneHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.ams.hb.AmsAssetRecordSystemTypeForOperationType;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 09-04-2014
 * ds_trunk_2014
 * LiquidationKindDecisionHandler.java
 */
public class LiquidationKindDecisionHandler implements DecisionHandler{

	/**
	 * CN pola dataBaseUnumField
	 */
	private String dataBaseEnumField;
	
	/**
	 * Mozliwe wartosci: cn, title, refValue
	 */
	private String rsmLiquidationReasonControlValue;
	
	private final String CN = "cn";
	private final String TITLE = "title";
	private final String REFVALUE = "refValue";
	
	private final String SZMATY = "SZMATY";
	private final String WYSYPISKO = "WYSYPISKO";
	private final String ZLOM = "ZLOM";
	private final String UTYLIZACJA = "UTYLIZACJA";
	
	private final static String ERROR = "ERROR";
	
	private final List<String> CONTROL_VALUE = ImmutableList.<String>builder().
			add(CN).
			add(TITLE).
			add(REFVALUE).
			build();
	
	private final List<String> LIQUIDATION_REASON = ImmutableList.<String>builder().
			add(SZMATY).
			add(WYSYPISKO).
			add(ZLOM).
			add(UTYLIZACJA).
			build();
	
	private static final Logger log = LoggerFactory.getLogger(DataBaseEnumFieldAssigneHandler.class);
	private final static StringManager SM = GlobalPreferences.loadPropertiesFile(DataBaseEnumFieldAssigneHandler.class.getPackage().getName(), null);
	
	@Override
	public String decide(OpenExecution openExecution) {
		boolean isOpened = false;
		try {
			String checkBy = Docusafe.getAdditionProperty(rsmLiquidationReasonControlValue);
			
			isOpened = DSApi.openContextIfNeeded();
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId,false);
			
			if(StringUtils.isNotBlank(dataBaseEnumField) && StringUtils.isNotBlank(checkBy)){
				
				FieldsManager fm = doc.getFieldsManager();
				EnumItem dataBaseField = fm.getEnumItem(dataBaseEnumField);

				if (CONTROL_VALUE.contains(rsmLiquidationReasonControlValue)) {
					try {
						if(LIQUIDATION_REASON.contains(dataBaseField.getCn())) {
							return dataBaseField.getCn();
							//TO DO
						} else if (LIQUIDATION_REASON.contains(dataBaseField.getTitle())) {
							return dataBaseField.getTitle();
							//TO DO
						} else if (LIQUIDATION_REASON.contains(dataBaseField.getRefValue())) {
							return dataBaseField.getRefValue();
							//TO DO
						} else {
							return ERROR;
						}
					} catch (Exception e) {
						log.error(e.getMessage(), e);
						return ERROR;
					}
				} else {
					return ERROR;
				}
			} else {
				return ERROR;
			}
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return ERROR;
		} 
	}

}
