/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class CheckFactureTypeDecision implements DecisionHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(CheckFactureTypeDecision.class);
	
	public static final String COST_FACTURE_TYPE = "KOSZTOWA";
	public static final String WAREHOUE_FACTURE_TYPE = "MAGAZYNOWA";
	public static final String PROJECT_FACTURE_TYPE = "PROJEKTOWA";
	
	private String field;
	
	public String decide(OpenExecution openExecution) {
		try
		{
			String decisionResult = null;
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			String cn = fm.getEnumItemCn(field);
			
			if (COST_FACTURE_TYPE.equals(cn)) {
				decisionResult = "kosztowa";
			} else if (WAREHOUE_FACTURE_TYPE.equals(cn)) {
				decisionResult = "magazynowa";
			} else if (PROJECT_FACTURE_TYPE.equals(cn)) {
				decisionResult = "projektowa";
			} else {
				decisionResult = "other";
			}
			
			return decisionResult;
		} 
		catch (EdmException ex) 
		{
			logger.error(ex.getMessage(), ex);
			return "error";
		}
	}

}
