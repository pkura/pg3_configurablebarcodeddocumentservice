package pl.compan.docusafe.parametrization.ams.jbpm;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContextOpener;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Date;
import java.util.List;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class DataBaseToUserAssigneeHandler implements AssignmentHandler {

    private static final Logger log = LoggerFactory.getLogger(DataBaseToUserAssigneeHandler.class);
    private String field;

    public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
        DSContextOpener opener = DSContextOpener.openAsAdminIfNeed();

        Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId, false);

        try {
            openExecution.setVariable(Jbpm4WorkflowFactory.REASSIGNMENT_TIME_VAR_NAME, new Date());

            FieldsManager fm = doc.getFieldsManager();
            Field user = fm.getField(field);

            String userName = null;
            if (user.isMultiple()) {
                List<Long> userIds = (List<Long>) fm.getKey(field);
                for (Long userId : userIds) {
                    userName = DSUser.findById(userId).getName();
                    assignable.addCandidateUser(userName);
                    AssigneeHandler.addToHistory(openExecution, doc, userName, null);
                }
            } else {
                userName = fm.getEnumItemCn(field);
                //jesli jest null to niech lepiej idzie do autora niz w kosmos
                if (StringUtils.isEmpty(userName))
                    userName = DSApi.context().getPrincipalName();
                assignable.addCandidateUser(userName);
                AssigneeHandler.addToHistory(openExecution, doc, userName, null);
            }
        } catch (EdmException e) {
            log.error(e.getMessage());
            throw e;
        } finally {
            opener.closeIfNeed();
        }
    }
}
