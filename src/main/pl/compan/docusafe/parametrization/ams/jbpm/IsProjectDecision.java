/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.jbpm;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class IsProjectDecision implements DecisionHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(IsProjectDecision.class);
	
	public static final String DEKRETY_FIELD = "DEKRETY";
	public static final String MPK_FIELD = "MPK";
	
	public static final double ASSISTANT_ROLE_ID = 8.00;
	
	public String decide(OpenExecution openExecution) {
		Set<Long> ids = new HashSet<Long>();
		try {
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			
    		List<Long> idList = (List<Long>) fm.getKey(MPK_FIELD);
    		List<BigDecimal> usersIds = null;
    		if (idList != null && !idList.isEmpty()) {
	    		usersIds = (List<BigDecimal>) DSApi.context().session().createSQLQuery("SELECT DISTINCT pracownik_id FROM dsg_ams_role_in_project WHERE kontrakt_id IN (SELECT erpId FROM dsg_ams_contract WHERE id IN (SELECT projekt FROM dsg_ams_mpk_dictionary WHERE id IN (:idList))) AND bp_rola_id = :assistant")
	    				.setParameterList("idList", idList)
	    				.setParameter("assistant", ASSISTANT_ROLE_ID)
	    				.list();
    		}
    		if (usersIds != null && !usersIds.isEmpty()) {
	    		for (BigDecimal id : usersIds) {
	    			String idStr = id.toString().replaceAll(".00", ".0");
	    			try {
		    			DSUser user = DSUser.findAllByExtension(idStr);
		    			ids.add(user.getId());
	    			} catch (EdmException e) {
	    				logger.error(e.getMessage(), e);
	    			}
	    		}
	    		
	    		openExecution.setVariable("ids", ids);
	    		openExecution.setVariable("count", ids.size());
	    		return "project";
    		
    		} else {
    			return "other";
    		}
		} catch (EdmException ex) {
			logger.error(ex.getMessage(), ex);
			return "error";
		}
	}
}
