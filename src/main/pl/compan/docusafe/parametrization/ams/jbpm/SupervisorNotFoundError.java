/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.jbpm;

import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.EdmException;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class SupervisorNotFoundError implements ExternalActivityBehaviour {

	public void execute(ActivityExecution arg0) throws Exception {
		throw new EdmException("W danych jednostkach wnioskujących nie znaleziono osoby z profilem kierownik");

	}

	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
		// TODO Auto-generated method stub

	}

}
