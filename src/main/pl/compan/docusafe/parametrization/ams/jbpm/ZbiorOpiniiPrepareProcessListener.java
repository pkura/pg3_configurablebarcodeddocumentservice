package pl.compan.docusafe.parametrization.ams.jbpm;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.ams.ProjMnarLogic;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ZbiorOpiniiPrepareProcessListener implements ExternalActivityBehaviour {

    private String dictionaryField;
    private String enumIdField;
    public static final List<String> guids = new LinkedList<String>();

    @Override
    public void execute(ActivityExecution execution) throws Exception {
        final List<String> argSets = new LinkedList<String>();

        OfficeDocument doc = getOfficeDocument(execution);
        FieldsManager fm = doc.getFieldsManager();
        List<EnumValues> divisions = FieldsManagerUtils.getDictionaryItems(fm, ProjMnarLogic.FIELD_OPINIE_DODATKOWE_DICT, ProjMnarLogic.FIELD_OPINIE_DODATKOWE_DICT + "_" + ProjMnarLogic.DICTFIELD_OPINIE_DODATKOWE_JEDNOSTKA, EnumValues.class);

        for (EnumValues div : divisions) {
            Long selectedId = Long.parseLong(div.getSelectedId());
            if (!argSets.contains(selectedId))
                argSets.add(ProjMnarLogic.PROCESS_VAR_OPINIA_JEDN_DODATKOWEJ + selectedId);
        }

        Set<DSUser> allKiers = ProjMnarLogic.getKierownikDlaZrodlaFinansowania(fm);
        for(DSUser kier : allKiers)
            argSets.add(ProjMnarLogic.PROCESS_VAR_OPINIA_CTTM_LUB_NM + kier.getId());


        execution.setVariable("ids", argSets);
        execution.setVariable("count", argSets.size());
        execution.setVariable("count2", 2);
    }

    @Override
    public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
        // TODO Auto-generated method stub

    }

    public static OfficeDocument getOfficeDocument(ActivityExecution execution) throws EdmException {
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);
        return doc;
    }


}
