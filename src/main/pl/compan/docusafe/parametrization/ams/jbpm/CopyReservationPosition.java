package pl.compan.docusafe.parametrization.ams.jbpm;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.parametrization.ams.ZamowienieLogic;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by kk on 07.03.14.
 */
public class CopyReservationPosition extends AbstractEventListener {

    public static final String SQL = "DECLARE @inserted_ids TABLE ([id] INT);\n" +
            "insert into dsg_ams_mpk_dictionary ([okres_budzetowy],[komorka_budzetowa],[netto],[stawka],[kwota_vat],[brutto],[identyfikator_planu],[pozycjaid],[projekt],[procent],[produkt],[ilosc],[jm],[opis],[uwagi],[budzet],[pozycja_budzetu_raw],[pozycja_planu_zakupow_raw],[zadanie_finansowe_planu_raw],[zadanie_finansowe_budzetu_raw],[komorka_budzetowa_bk],[okres_budzetowy_bk])\n" +
            "OUTPUT INSERTED.[id] INTO @inserted_ids \n" +
            "select [okres_budzetowy],[komorka_budzetowa],[netto],[stawka],[kwota_vat],[brutto],[identyfikator_planu],[pozycjaid],[projekt],[procent],[produkt],[ilosc],[jm],[opis],[uwagi],[budzet],[pozycja_budzetu_raw],[pozycja_planu_zakupow_raw],[zadanie_finansowe_planu_raw],[zadanie_finansowe_budzetu_raw],[komorka_budzetowa_bk],[okres_budzetowy_bk]\n" +
            "from dsg_ams_mpk_dictionary where id in (?);\n" +
            "select id from @inserted_ids;";

    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        boolean hasOpen = true;
        if (!DSApi.isContextOpen()) {
            hasOpen = false;
            DSApi.openAdmin();
        }
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        Document doc = Document.find(docId, false);

        FieldsManager fm = doc.getFieldsManager();

        List<Long> duplicatedMpkIds = Lists.newArrayList();
        List<Long> duplicatedTaskIds = Lists.newArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Iterable<? extends Number> mpkIds = (Iterable<? extends Number>) fm.getKey(ZamowienieLogic.MPK_FIELD);
            if (mpkIds != null && Iterables.size(mpkIds) > 0) {
                String mpkIdsRaw = Joiner.on(',').skipNulls().join(mpkIds);
                ps = DSApi.context().prepareStatement(Docusafe.getAdditionPropertyOrDefault("sql.zamowienie.copy_positions", SQL).replace("?", mpkIdsRaw));

                rs = ps.executeQuery();

                while (rs.next()) {
                    duplicatedMpkIds.add(rs.getLong(1));
                }

                DSApi.context().closeStatement(ps);

            }

            Iterable<? extends Number> taskIds = (Iterable<? extends Number>) fm.getKey(ZamowienieLogic.ZADANIA_FIELD);
            if (taskIds != null && Iterables.size(taskIds) > 0) {
                String taskIdsRaw = Joiner.on(',').skipNulls().join(taskIds);
                ps = DSApi.context().prepareStatement(Docusafe.getAdditionPropertyOrDefault("sql.zamowienie.copy_positions", SQL).replace("?", taskIdsRaw));

                rs = ps.executeQuery();

                while (rs.next()) {
                    duplicatedTaskIds.add(rs.getLong(1));
                }
            }

        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            if (ps != null) {
                DSApi.context().closeStatement(ps);
            }
        }

        doc.getDocumentKind().setOnly(docId, ImmutableMap.of(ZamowienieLogic.MPK_FINAL_FIELD, duplicatedMpkIds, ZamowienieLogic.ZADANIA_FINAL_FIELD, duplicatedTaskIds, ZamowienieLogic.TYP_REZERWACJI_FINAL, fm.getKey(ZamowienieLogic.TYP_REZERWACJI)), false);

        if (DSApi.isContextOpen() && !hasOpen)
            DSApi.close();
    }
}
