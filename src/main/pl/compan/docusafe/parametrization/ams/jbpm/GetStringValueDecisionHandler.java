package pl.compan.docusafe.parametrization.ams.jbpm;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class GetStringValueDecisionHandler implements DecisionHandler {

    private final static Logger LOG = LoggerFactory.getLogger(GetStringValueDecisionHandler.class);
    private String name;


    public String decide(OpenExecution openExecution) {
        if (StringUtils.isEmpty(name))
            return "noDecision";

        Object value = openExecution.getVariable(name);
        if (value == null || (value instanceof String && StringUtils.isEmpty((String) value)))
            return "noDecision";

        return (String) value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
