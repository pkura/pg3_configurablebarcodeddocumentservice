/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.jbpm;

import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class CleanCorrectionVariableInFork implements ExternalActivityBehaviour {

	public void execute(ActivityExecution activityExecution) throws Exception {
		Integer cor = (Integer) activityExecution.getVariable("correction");
		if (cor != null && cor.equals(11)) {
			if (activityExecution.getParent() != null) {
				activityExecution.getParent().removeVariable("correction");
			}
			activityExecution.removeVariable("correction");
		}
	}

	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {

	}

}
