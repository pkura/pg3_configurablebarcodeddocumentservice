package pl.compan.docusafe.parametrization.ams.jbpm;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;

import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class GetPrevAcceptanceTask implements ExternalActivityBehaviour {

//    @Override
//    protected String getDecision(String dec, OfficeDocument doc, FieldsManager fm) throws EdmException {
//        String state = null;
//        DocumentLogic logic = doc.getDocumentKind().logic();
//
//        List<DocumentAcceptance> documentAcceptance = DocumentAcceptance.find(doc.getId());
//
//
//        return "noAcceptance";
//    }

    @Override
    public void signal(ActivityExecution activityExecution, String s, Map<String, ?> stringMap) throws Exception {}

    @Override
    public void execute(ActivityExecution activityExecution) throws Exception {
        Long docId = Long.valueOf(activityExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        String state = "correction_by_author";

        List<DocumentAcceptance> documentAcceptance = DocumentAcceptance.find(docId);


        activityExecution.take(state);
    }
}
