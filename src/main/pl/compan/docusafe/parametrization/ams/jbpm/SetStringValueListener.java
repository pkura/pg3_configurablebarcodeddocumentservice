package pl.compan.docusafe.parametrization.ams.jbpm;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class SetStringValueListener extends AbstractEventListener {

    private String name;
    private String value;

    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        execution.setVariable(name, value);
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
