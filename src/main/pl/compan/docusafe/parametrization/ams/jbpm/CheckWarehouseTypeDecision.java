/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class CheckWarehouseTypeDecision implements DecisionHandler {

	private static final Logger logger = LoggerFactory.getLogger(CheckWarehouseTypeDecision.class);
	
	public static final String TABLE_NAME_WITH_WAREHOUSE = "dsg_ams_warehouse";
	public static final String COLUMN_NAME_WITH_WAREHOUSE_TYPE = "type";
	
	public static final Byte PHYSICAL_WAREHOUSE_TYPE = 0;
	public static final Byte VIRTUAL_WAREHOUSE_TYPE = 1;
	
	private String field;
	
	public String decide(OpenExecution openExecution) {
		String decisionResult = null;
		try {
//			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
//			OfficeDocument doc = OfficeDocument.find(docId);
//			FieldsManager fm = doc.getFieldsManager();
//			Integer warehouseId = (Integer) fm.getKey(field);
//			
//			StringBuilder queryString = new StringBuilder();
//			queryString.append("SELECT ")
//				.append(COLUMN_NAME_WITH_WAREHOUSE_TYPE)
//				.append(" FROM ")
//				.append(TABLE_NAME_WITH_WAREHOUSE)
//				.append(" WHERE id = :warehouseId");
//			
//			Byte warehouseType = (Byte) DSApi.context().session().createSQLQuery(queryString.toString())
//				.setParameter("warehouseId", warehouseId).uniqueResult();
//			
//			if (PHYSICAL_WAREHOUSE_TYPE.equals(warehouseType)) {
//				decisionResult = "fizyczny";
//			} else if (VIRTUAL_WAREHOUSE_TYPE.equals(warehouseType)) {
//				decisionResult = "wirtualny";
//			} else {
//				decisionResult = "error";
//			}
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			Integer warehouseId = (Integer) fm.getKey(field);
			if (warehouseId != null) {
				return "fizyczny";
			} else {
				return "brak_magazynu";
			}
		} catch (Exception e) {//EdmException
			logger.error(e.getMessage(), e);
			decisionResult = "error";
		}
		return decisionResult;
	}

}
