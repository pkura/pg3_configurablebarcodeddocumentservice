package pl.compan.docusafe.parametrization.ams.jbpm;

import pl.compan.docusafe.core.jbpm4.MultipleDictPrepareProcessListener;
import pl.compan.docusafe.parametrization.ams.ProjKrajLogic;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ProjKrajDysponenciPrepareProcessListener extends MultipleDictPrepareProcessListener {
    @Override
    public String getDictCn() {
        return ProjKrajLogic.FIELD_DYSPONENCI_SRODKOW_DICT;
    }

    @Override
    public String getUserFieldCn() {
        return ProjKrajLogic.DICTFIELD_DYSPONENCI_DYSPONENT;
    }
}
