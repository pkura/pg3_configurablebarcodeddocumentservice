package pl.compan.docusafe.parametrization.ams.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.BooleanDecisionHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public abstract class AbsDecisionHandler implements DecisionHandler {

    private final static Logger LOG = LoggerFactory.getLogger(BooleanDecisionHandler.class);
    public abstract String getDecisionName ();


    public String decide(OpenExecution openExecution) {
        try {
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();

            if (getDecisionName () != null) {
                String[] decisions = getDecisionName ().split(",");

                for (String dec : decisions) {
                    String decision = getDecision(dec, doc, fm);
                    if (decision != null)
                        return decision;
                }
            }

            return "noDecision";
        } catch (EdmException ex) {
            LOG.error(ex.getMessage(), ex);
            return "error";
        }
    }

    protected abstract String getDecision(String dec, OfficeDocument doc, FieldsManager fm) throws EdmException;
}