package pl.compan.docusafe.parametrization.ams.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class FieldValueDecision implements DecisionHandler {

    private static final Logger logger = LoggerFactory.getLogger(CheckFactureTypeDecision.class);
    private static final String RETURN_TRUE = "true";
    private static final String RETURN_FALSE = "false";

    private String field;
    private Boolean canNull;
    private Double greater;
    private Double equal;
    private Double notEqual;
    private Double less;

    public String decide(OpenExecution openExecution) {
        try
        {
            String decisionResult = null;
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();

            Object fieldValue = fm.getKey(field);
            if (fieldValue==null){
                if (canNull != null){
                    return canNull == true ? RETURN_TRUE : RETURN_FALSE;
                }
                else
                    throw new IllegalArgumentException("Field is null. No decision.");
            }

            String stringValue = fieldValue.toString();
            double value = Double.parseDouble(stringValue);


            if (equal != null && !(value == equal)){
                return RETURN_FALSE;
            }
            if (greater != null && !(value > greater)){
                return RETURN_FALSE;
            }
            if (less != null && !(value < less)){
                return RETURN_FALSE;
            }


            return RETURN_TRUE;
        }
        catch (EdmException ex){
            logger.error(ex.getMessage(), ex);
            return "error";
        }
    }

}
