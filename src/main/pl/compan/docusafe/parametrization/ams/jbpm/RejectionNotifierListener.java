package pl.compan.docusafe.parametrization.ams.jbpm;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.SimpleAcceptanceListener;
import pl.compan.docusafe.core.office.OfficeDocument;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class RejectionNotifierListener extends AbstractEventListener {


    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        OfficeDocument document = SimpleAcceptanceListener.getOfficeDocument(execution);
        DocumentLogic logic = document.getDocumentKind().logic();
        if (logic instanceof OnRejectionListener)
            ((OnRejectionListener)logic).notifyRejectionListener(execution, document);
    }

    public interface OnRejectionListener{
        void notifyRejectionListener(EventListenerExecution execution, OfficeDocument document);
    }
}
