package pl.compan.docusafe.parametrization.ams.jbpm;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class OpinieDodatkowePrepareProcessListener implements ExternalActivityBehaviour {

    private String dictionaryField;
    private String enumIdField;
    public static final List<String> guids = new LinkedList<String>();

    @Override
    public void execute(ActivityExecution execution) throws Exception {
        final List<Long> argSets = new LinkedList<Long>();

        OfficeDocument doc = getOfficeDocument(execution);
        FieldsManager fm = doc.getFieldsManager();
        List<EnumValues> divisions = FieldsManagerUtils.getDictionaryItems(fm, "OPINIE_DODATKOWE", "OPINIE_DODATKOWE_JEDNOSTKA", EnumValues.class);

        for (EnumValues div : divisions){
            Long selectedId = Long.parseLong(div.getSelectedId());
            if (!argSets.contains(selectedId))
                argSets.add(selectedId);
        }

        execution.setVariable("ids", argSets);
        execution.setVariable("count", argSets.size());
        execution.setVariable("count2", 2);
    }

    @Override
    public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
        // TODO Auto-generated method stub

    }

    public static OfficeDocument getOfficeDocument(ActivityExecution execution) throws EdmException {
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);
        return doc;
    }

}
