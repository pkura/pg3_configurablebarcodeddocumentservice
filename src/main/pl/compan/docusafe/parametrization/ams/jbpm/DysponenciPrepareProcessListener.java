package pl.compan.docusafe.parametrization.ams.jbpm;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ams.ProjMnarLogic;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class DysponenciPrepareProcessListener implements ExternalActivityBehaviour {

    private String dictionaryField;
    private String enumIdField;
    public static final List<String> guids = new LinkedList<String>();

    @Override
    public void execute(ActivityExecution execution) throws Exception {
        final List<Long> argSets = new LinkedList<Long>();

        OfficeDocument doc = getOfficeDocument(execution);
        FieldsManager fm = doc.getFieldsManager();
        List<Map<String, Object>> dysponenci = FieldsManagerUtils.getDictionaryItems(
                fm,
                ProjMnarLogic.FIELD_DYSPONENCI_SRODKOW_DICT,
                new FieldsManagerUtils.FieldProperty[]{
                        new FieldsManagerUtils.FieldProperty(ProjMnarLogic.FIELD_DYSPONENCI_SRODKOW_DICT,ProjMnarLogic.DICTFIELD_DYSPONENCI_DYSPONENT, EnumValues.class),
                        new FieldsManagerUtils.FieldProperty(ProjMnarLogic.FIELD_DYSPONENCI_SRODKOW_DICT,ProjMnarLogic.DICTFIELD_DYSPONENCI_POTWIERDZENIE, EnumValues.class)}
        );

        for (Map<String, Object> dysponent : dysponenci){
            Long dysponentSelectedId = Long.parseLong(((EnumValues)dysponent.get(ProjMnarLogic.joinDictionaryWithField(ProjMnarLogic.FIELD_DYSPONENCI_SRODKOW_DICT,ProjMnarLogic.DICTFIELD_DYSPONENCI_DYSPONENT))).getSelectedId());
            if (!argSets.contains(dysponentSelectedId)){
                EnumValues potwierdzenie = ((EnumValues)dysponent.get(ProjMnarLogic.joinDictionaryWithField(ProjMnarLogic.FIELD_DYSPONENCI_SRODKOW_DICT,ProjMnarLogic.DICTFIELD_DYSPONENCI_POTWIERDZENIE)));
                if (potwierdzenie == null){
                    argSets.add(dysponentSelectedId);
                } else {
                    String potwierdzenieSelectedId = potwierdzenie.getSelectedId();
                    if (StringUtils.isEmpty(potwierdzenieSelectedId))
                        argSets.add(dysponentSelectedId);
                }
            }
        }

        if (argSets.size() == 0) throw new Exception("Nie znaleziono dysponent�w, kt�rzy jeszcze nie potwierdzili wniosku");

        execution.setVariable("ids", argSets);
        execution.setVariable("count", argSets.size());
        execution.setVariable("count2", 2);
    }

    @Override
    public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
        // TODO Auto-generated method stub

    }

    public static OfficeDocument getOfficeDocument(ActivityExecution execution) throws EdmException {
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);
        return doc;
    }
}
