package pl.compan.docusafe.parametrization.ams.jbpm;

import com.google.common.base.Strings;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.exports.ExportHandler;
import pl.compan.docusafe.core.exports.ExportedDocument;
import pl.compan.docusafe.core.exports.RequestResult;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.parametrization.ams.ZamowienieLogic;
import pl.compan.docusafe.parametrization.ams.export.SimpleErpExportFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Collections;

import static pl.compan.docusafe.parametrization.ul.jbpm.export.ExportDocument.*;

@SuppressWarnings("serial")
public class ExportDocumentDecision implements DecisionHandler {

    private static final String FAST_REPEAT = "fast_repeat";
    private static final String REPEAT = "repeat";
    private static final String SUCCESS_TRANSITION = "success_transition";
    private static final String FAIL_TRANSITION = "fail_transition";
    private static final String DEFAULT_TRANSITION = FAIL_TRANSITION;
    public static final String TO_CONFIRM = "to-confirm";

    //ZGL_BLEDU = ZGLOSZENIE_BLEDU, Formularz do przesyłania info o błędzie do admina (usługą lub przez człowieka)  ----------
				/** zgłoszenie błędu: ID statusu błędu nowego */				String ZGL_BLEDU_nowy = "11";
				/** zgłoszenie błędu: ID błędu będącego w obsłudze */ 			String ZGL_BLEDU_obslugiwany = "12";
				/** zgłoszenie błędu: ID błędu rozwiązanego */ 					String ZGL_BLEDU_rozwiazany = "13";
				/** zgłoszenie błędu: ID błędu odrzuconego */ 					String ZGL_BLEDU_odrzucony = "14";
				/** zgłoszenie błędu: ID błędu odsuniętego w czasie */ 			String ZGL_BLEDU_odsuniety= "15";
			
				/** Komunikat gotowy do pobrania przez szynę. */				String ERP_PRZESLANO_NA_SZYNE = "NEW"; 
				/** Komunikat został pobrany przez task */						String ESB_POBRANY_PRZEZ_SZYNE = "UPLOADED"; 
				/** Komunikat jest przetwarzany przez szynę danych */			String ESB_PRZETWARZANY_PRZEZ_SZYNE = "PROCESSING"; 
				/** Komunikat został poprawnie dodany do Simple.ERP */			String ESB_DODANO_DO_ERP = "FINISHED"; 
				/** Komunikat nie przeszedł wstępnej walidacji XSD */			String ESB_BLAD_WALIDACJI_PLIKU_XML = "INVALID"; 
				/** Komunikat nie został dodany do Simple.ERP system 
				 * zgłosił kod błędu */ 										String ESB_BLAD_WEWNETRZNY_ERP = "FAULT"; 
	
	private static final Logger log = LoggerFactory.getLogger(ExportDocumentDecision.class);

	private Integer fastRepeats;
	private String transitionToTake;
	

	protected void setResults(String result, Long docId) throws EdmException {
		try {
			DSApi.context().begin();
	        OfficeDocument doc = OfficeDocument.findOfficeDocument(docId);
	        doc.addRemark(new Remark(result, "admin"));
			DSApi.context().commit();
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
			DSApi.context().setRollbackOnly();
		}
	}
	
	protected void setStatus(Long docId, int status) throws EdmException {
		try {
			DSApi.context().begin();
			OfficeDocument.find(docId).getDocumentKind().setOnly(docId, Collections.singletonMap("STATUS", status), false);
			DSApi.context().commit();
		} catch (EdmException e) {
			log.error(e.getMessage(),e);
			DSApi.context().setRollbackOnly();
		}
	}

	@Override
	public String decide(OpenExecution exec) {
		boolean isOpened = true;
		try {
			if (!DSApi.isContextOpen()) {
				isOpened = false;
				DSApi.openAdmin();
			}
            exec.setVariable(IS_EXPORT, true);
			Long docId = Long.valueOf(exec.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			
			Integer counter = 1;
			if (exec.hasVariable(EXPORT_REPEAT_COUNTER)) {
				counter = (Integer) exec.getVariable(EXPORT_REPEAT_COUNTER);
			}
			if (counter + 1 > fastRepeats) {
				takeTransition(REPEAT);
			} else {
				takeTransition(FAST_REPEAT);
			}
			exec.setVariable(EXPORT_REPEAT_COUNTER, ++counter);

			String guid;

			if (exec.hasVariable(DOC_GUID)) {
				guid = exec.getVariable(DOC_GUID).toString();

				ExportHandler handler = new ExportHandler(new SimpleErpExportFactory());
				RequestResult result = handler.checkExport(guid);

				int state = result.getStateId();
				exec.setVariable(EXPORT_RESULT_STATE, state);

				String stateName = result.getStateName();
				exec.setVariable(EXPORT_RESULT_STATE_NAME, stateName);

				String requestResult = result.getResult();

				if (ESB_BLAD_WEWNETRZNY_ERP.equals(stateName) || ESB_BLAD_WALIDACJI_PLIKU_XML.equals(stateName)) {
					log.error("B��d eksportu, counter: " + counter);
					log.error("B��d eksportu, guid: " + guid);
					log.error("B��d eksportu, docId: " + exec.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY));
					log.error("B��d eksportu, state: " + state);
					log.error("B��d eksportu, stateName: " + stateName);
					log.error("B��d eksportu, request result: " + requestResult);
					exec.removeVariable(EXPORT_REPEAT_COUNTER);
                    exec.removeVariable(DOC_GUID);

                    takeTransition(FAIL_TRANSITION);

					if (docId != null) {
						setResults("B��d eksportu!\n"+ buildExportRemarks(guid, stateName, requestResult), docId);
					}
				} else if (ESB_DODANO_DO_ERP.equals(stateName)) {
                    exec.removeVariable(DOC_GUID);
					exec.removeVariable(EXPORT_REPEAT_COUNTER);
                    exec.removeVariable(IS_EXPORT);
                    exec.setVariable(TO_CONFIRM, true);
					takeTransition(SUCCESS_TRANSITION);
					
					setStatus(docId, 1600);
                    setResults("Eksport zako�czony pomy�lnie\n"+ buildExportRemarks(guid, stateName, requestResult), docId);

                    exec.setVariable(ZamowienieLogic.ERP_DOCUMENT_IDM, getDocumentErpIdm(requestResult));
				}

			} else {
                OfficeDocument doc = OfficeDocument.find(docId);
                ExportedDocument exported = null;
                setStatus(docId, 1500);
                try {
                    exported = doc.getDocumentKind().logic().buildExportDocument(doc);
                    if (exported == null) {
                        throw new NullPointerException("Export document not built!");
                    }
                } catch (Exception e) {
                    setResults("B��d eksportu, guid: brak, id dokumentu: "+docId, docId);
                    exec.removeVariable(EXPORT_REPEAT_COUNTER);
                    takeTransition(FAIL_TRANSITION);

                    throw new EdmException(e);
                }

                try {
                    ExportHandler handler = new ExportHandler(new SimpleErpExportFactory());
                    guid = handler.xmlExport(exported);
                    if (Strings.isNullOrEmpty(guid)) {
                        throw new EdmException("B�ad w eksporcie dokumentu, mo�liwy b��d w komunikacji, serwis nie zwr�ci� kodu guid dla dokumentu.");
                    }
                    exec.setVariable(DOC_GUID, guid);
                } catch (Exception e) {
                    if (counter > 3) {
                        setResults("B��d eksportu, przekroczono ilo�� 3 pr�b wys�ania dokumentu do szyny przetwarzaj�cej. Zg�o� ten b��d do administratora.", docId);
                        exec.removeVariable(EXPORT_REPEAT_COUNTER);
                        exec.removeVariable(DOC_GUID);
                        takeTransition(FAIL_TRANSITION);
                    }

                    throw new EdmException(e);
                }
            }
        } catch (Exception e) {
			log.error(e.getMessage(),e);
        } finally {
			if (DSApi.isContextOpen() && !isOpened)
				DSApi._close();
		}
		
		return getTakenTransition();
	}

    private String getDocumentErpIdm(String requestResult) {
        try {
            Document requestResultXml = DocumentHelper.parseText(requestResult);
            Element root = requestResultXml.getRootElement();
            if (root != null) {
                if (root.element("IDM") != null && !Strings.isNullOrEmpty(root.element("IDM").getText())) {
                    return root.element("IDM").getText();
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    private String buildExportRemarks(String guid, String stateName, String requestResult) {
        StringBuilder sb = new StringBuilder();

        try {
            Document requestResultXml = DocumentHelper.parseText(requestResult);
            Element root = requestResultXml.getRootElement();
            if (root != null) {
                if (root.element("ID") != null && !Strings.isNullOrEmpty(root.element("ID").getText())) {
                   sb.append("ID dokumentu ERP: ").append(root.element("ID").getText()).append("\n");
                }
                if (root.element("IDM") != null && !Strings.isNullOrEmpty(root.element("IDM").getText())) {
                    sb.append("IDM dokumentu ERP: ").append(root.element("IDM").getText()).append("\n");
                }
                if (root.element("StatusMessage") != null && !Strings.isNullOrEmpty(root.element("StatusMessage").getText())) {
                    sb.append("Status eksportu: ").append(root.element("StatusMessage").getText()).append("\n");
                }
            }
        } catch (Exception e) {
            log.error("CAUGHT: "+e.getMessage(),e);
        }

        sb.append("\n").append("Guid dokumentu: ").append(guid).append("\n");
        sb.append("Nazwa stanu: ").append(stateName).append("\n");
        return sb.toString();
    }

    private void takeTransition(String transition) {
		transitionToTake = transition;
	}
	
	private String getTakenTransition() {
		return transitionToTake != null ? transitionToTake : DEFAULT_TRANSITION;
	}
}
