/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class IsCorrectionAfterForkDecision implements DecisionHandler {

    @Override
    public String decide(OpenExecution openExecution) {
        Integer cor = (Integer) openExecution.getVariable("correction");

        if (cor != null && cor == 11) {
            openExecution.removeVariable("correction");
            return "correction";
        } else if (cor != null && cor == 12) {
            openExecution.removeVariable("correction");
            return "rejection";
        } else {
            return "normal";
        }
    }

}
