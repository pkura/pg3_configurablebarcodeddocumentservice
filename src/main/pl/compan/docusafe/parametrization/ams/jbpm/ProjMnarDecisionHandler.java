package pl.compan.docusafe.parametrization.ams.jbpm;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.OfficeDocument;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ProjMnarDecisionHandler extends AbsDecisionHandler {

    public String decisionName;

    @Override
    public String getDecisionName() {
        return decisionName;
    }

    @Override
    protected String getDecision(String dec, OfficeDocument doc, FieldsManager fm) throws EdmException {

        DocumentLogic logic = doc.getDocumentKind().logic();

        if (dec.equalsIgnoreCase("opinieDodatkowe")) {
            if (logic instanceof OpinieDodatkowe && ((OpinieDodatkowe) logic).isOpinieDodatkoweRequired(fm))
                return "wkladWlasny";

        }
        if (dec.equalsIgnoreCase("osobaZatwierdzajaca")) {
            if (logic instanceof OsobaZatwierdzajaca )
                return ((OsobaZatwierdzajaca) logic).getOsobaZatwierdzajaca(fm);

        }
        if (dec.equalsIgnoreCase("opiniaKanclerza")) {
            //todo w przypadku finansowania inwestycji budowlanych s�u��cych potrzebom bada� naukowych lub prac rozwojowych - art. 22 Ustawy o finansowaniu nauki
            //todo ???
            if (logic instanceof OpiniaKanclerza && ((OpiniaKanclerza) logic).isOpiniaKanclerzaRequired(fm))
                return "true";
        }

        return null;
    }

    public interface OpinieDodatkowe {
        boolean isOpinieDodatkoweRequired(FieldsManager fm) throws EdmException;
    }
    public interface OsobaZatwierdzajaca {
        String getOsobaZatwierdzajaca(FieldsManager fm) throws EdmException;
    }
    public interface OpiniaKanclerza {
        boolean isOpiniaKanclerzaRequired(FieldsManager fm) throws EdmException;
    }
}