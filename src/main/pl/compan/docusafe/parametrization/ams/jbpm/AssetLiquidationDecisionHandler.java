/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.jbpm4.DataBaseEnumFieldAssigneHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 09-04-2014
 * ds_trunk_2014
 * AssetLiquidationDecisionHandler.java
 */
public class AssetLiquidationDecisionHandler implements DecisionHandler {

	private String enumFieldCn;
	
	private final static String SPRZEDAZ_DAROWIZNA = "SPRZEDAZ/DAROWIZNA";
	private final static String FIZYCZNA_LIKWIDACJA = "FIZYCZNA_LIKWIDACJA";
	
	
	private static final Logger log = LoggerFactory.getLogger(DataBaseEnumFieldAssigneHandler.class);
	private final static StringManager SM = GlobalPreferences.loadPropertiesFile(AssetLiquidationDecisionHandler.class.getPackage().getName(), null);
	
	@Override
	public String decide(OpenExecution openExecution) {
		try
		{
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			
			EnumItem enumItem = fm.getEnumItem(enumFieldCn);
			String enumItemRefValue = enumItem.getRefValue();
			
			if (enumItemRefValue != null && enumItemRefValue.length() > 0){
				if (enumItemRefValue.equals(SPRZEDAZ_DAROWIZNA)) {
					return SPRZEDAZ_DAROWIZNA;
				} else if (enumItemRefValue.equals(FIZYCZNA_LIKWIDACJA)) {
					return FIZYCZNA_LIKWIDACJA;
				} else {
					return "error";
				}
			} else {
				return "error";
			}
		} 
		catch (EdmException ex) 
		{
			log.error(ex.getMessage(), ex);
			return "error";
		}
	}

}
