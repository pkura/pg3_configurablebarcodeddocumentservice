package pl.compan.docusafe.parametrization.ams.jbpm;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.OfficeDocument;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ProjKrajDecisionHandler extends AbsDecisionHandler {

    public String decisionName;

    @Override
    public String getDecisionName() {
        return decisionName;
    }

    @Override
    protected String getDecision(String dec, OfficeDocument doc, FieldsManager fm) throws EdmException {

        DocumentLogic logic = doc.getDocumentKind().logic();

        if (dec.equalsIgnoreCase("rodzajEfektuProjektu")) {
            if (logic instanceof FormaPrzekazaniaEfektow && ((FormaPrzekazaniaEfektow) logic).isFormaPrzkazaniaEfektow(fm))
                return "formaPrzekazania";

        } else if (dec.equalsIgnoreCase("rodzajFinansowaniaInwestycji")) {
            if (logic instanceof FinansowanieInwestBudowlanych && ((FinansowanieInwestBudowlanych) logic).isFinansowanieInwestBudowlanych(fm))
                return "inwestycjeBudowlane";

        } else if (dec.equalsIgnoreCase("prefinansowanie")) {
            if (logic instanceof Prefinansowanie && ((Prefinansowanie) logic).isPrefinansowanieRequired(fm))
                return "prefinansowanie";

        } else if (dec.equalsIgnoreCase("wkladWlasny")) {
            if (logic instanceof WkladWlasny && ((WkladWlasny) logic).isWkladWlasnyRequired(fm))
                return "wkladWlasny";

        }

        return null;
    }

    public interface FormaPrzekazaniaEfektow {
        boolean isFormaPrzkazaniaEfektow(FieldsManager fm) throws EdmException;
    }

    public interface FinansowanieInwestBudowlanych {
        boolean isFinansowanieInwestBudowlanych(FieldsManager fm) throws EdmException;
    }

    public interface Prefinansowanie {
        boolean isPrefinansowanieRequired(FieldsManager fm) throws EdmException;
    }

    public interface WkladWlasny {
        boolean isWkladWlasnyRequired(FieldsManager fm) throws EdmException;
    }
}