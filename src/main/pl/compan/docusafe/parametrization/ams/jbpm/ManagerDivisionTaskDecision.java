package pl.compan.docusafe.parametrization.ams.jbpm;

import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.ams.ZamowienieLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * User: kk
 * Date: 04.02.14
 * Time: 13:35
 */
public class ManagerDivisionTaskDecision implements DecisionHandler {

    private static final Logger log = LoggerFactory.getLogger(ManagerDivisionTaskDecision.class);
    
    public static final String MPK_FIELD = "MPK";

    @Override
    public String decide(OpenExecution execution) {
        Set<String> usernames = Sets.newHashSet();

        boolean opened = false;
        try {
            Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");

            if (!DSApi.isContextOpen()) {
                DSApi.openAdmin();
                opened = true;
            }

            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();

            List<Map<String, Object>> values = (List<Map<String, Object>>) fm.getValue(ZamowienieLogic.DYSPONENT_FIELD);

            if (values != null && !values.isEmpty()) {

                for (Map<String, Object> item : values) {
                    EnumValues enumValues = (EnumValues) item.get(ZamowienieLogic.DYSPONENT_FIELD + "_" + ZamowienieLogic.DYSPONENT_FIELD);

                    if (enumValues != null && !Strings.isNullOrEmpty(enumValues.getSelectedId())) {
                        usernames.add(DSUser.findById(Long.valueOf(enumValues.getSelectedId())).getName());
                    }
                }
            }

            if (usernames.isEmpty()) {
                return "omit";
            } else if (checkIsProject(fm)) {
            	return "omit";
            }/* info od Waldka, z 09-04-14 15-14
                else if (isAmountLessThan500PLN(fm)) {
            	return "omit";
            }*/
            else {
                execution.createVariable("manager_usernames", usernames);
                execution.createVariable("count", usernames.size());
                return "take";
            }


        } catch (EdmException ex) {
            log.error(ex.getMessage(), ex);
        } finally {
            if (opened && DSApi.isContextOpen()) {
                DSApi._close();
            }
        }

        return "error";
    }
    
    private boolean checkIsProject(FieldsManager fm) throws EdmException {
		List<Long> idList = (List<Long>) fm.getKey(MPK_FIELD);
		if (idList != null && !idList.isEmpty()) {
			List<Long> projectIds = (List<Long>) DSApi.context().session().createSQLQuery("SELECT projekt FROM dsg_ams_mpk_dictionary WHERE id IN (:idList) AND projekt IS NOT NULL")
    				.setParameterList("idList", idList)
    				.list();
			if (projectIds != null && !projectIds.isEmpty()) {
				return true;
			}
		}
		return false;
    }
    
    private boolean isAmountLessThan500PLN(FieldsManager fm) throws EdmException {
   		List<Long> idList = (List<Long>) fm.getKey(MPK_FIELD);
		if (idList != null && !idList.isEmpty()) {
			List<BigDecimal> grossList = (List<BigDecimal>) DSApi.context().session().createSQLQuery("SELECT brutto FROM dsg_ams_mpk_dictionary WHERE id IN (:idList) AND brutto IS NOT NULL")
				.setParameterList("idList", idList)
				.list();
			if (grossList != null && !grossList.isEmpty()) {
				BigDecimal sum = new BigDecimal(0);
				for (BigDecimal item : grossList) {
					if (item != null) {
						sum = sum.add(item);
					}
				}
				if (sum.compareTo(new BigDecimal(500)) < 0) {
					return true;
				}
			}
		}
		return false;
    }
}
