package pl.compan.docusafe.parametrization.ams.export;

import org.apache.axiom.om.OMElement;
import org.apache.axis2.client.Stub;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.exports.ExportedDocument;
import pl.compan.docusafe.core.exports.Exporter;
import pl.compan.docusafe.core.exports.FixedAssetLiquidationDocument;
import pl.compan.docusafe.core.exports.PurchasingDocument;
import pl.compan.docusafe.core.exports.ReservationDocument;
import pl.compan.docusafe.core.exports.XmlCreator;
import pl.compan.docusafe.core.exports.ServiceHandledDocument;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.util.axis.AxisClientUtils;
import pl.compan.docusafe.ws.ams.ApplicationForReservationProxyStub;
import pl.compan.docusafe.ws.ams.CostInvoiceCreateProxyStub;

public class SimpleWSExporter implements Exporter {

	private Stub stub;
	private String result;

	public Stub getStub() throws EdmException {
		return stub;
	}

	public void setStub(Stub stub) {
		this.stub = stub;
	}

	@Override
	public void initExport(ExportedDocument doc) throws EdmException {
        try {
            if (doc instanceof ServiceHandledDocument) {
                AxisClientConfigurator conf = new AxisClientConfigurator();
                ServiceHandledDocument jaxbMapped = (ServiceHandledDocument) doc;
                stub = jaxbMapped.getSenderStub(conf.getAxisConfigurationContext());
                conf.setUpHttpParameters(stub, jaxbMapped.getSenderStubServicesSuffix());
                conf.setHttpClient(AxisClientUtils.createHttpClient(18, 18));
            } else {
                if (doc instanceof PurchasingDocument) {
                    AxisClientConfigurator conf = new AxisClientConfigurator();
                    stub = new CostInvoiceCreateProxyStub(conf.getAxisConfigurationContext());
                    conf.setUpHttpParameters(stub, "/services/CostInvoiceCreateProxy");
                    conf.setHttpClient(AxisClientUtils.createHttpClient(18, 18));
                }
                if (doc instanceof ReservationDocument) {
                    AxisClientConfigurator conf = new AxisClientConfigurator();
                    stub = new ApplicationForReservationProxyStub(conf.getAxisConfigurationContext());
                    conf.setUpHttpParameters(stub, "/services/ApplicationForReservationProxy");
                    conf.setHttpClient(AxisClientUtils.createHttpClient(18, 18));
                }
                if (doc instanceof FixedAssetLiquidationDocument) {
                    AxisClientConfigurator conf = new AxisClientConfigurator();
                    stub = new ApplicationForReservationProxyStub(conf.getAxisConfigurationContext());
                    conf.setUpHttpParameters(stub, "/services/AssetOperationsProxy");
                    conf.setHttpClient(AxisClientUtils.createHttpClient(18, 18));
                }
            }
        } catch (Exception e) {
            throw new EdmException(e.getMessage(),e);
        }
	}

	@Override
	public void exportXml(XmlCreator creator) throws EdmException {
		try {
			OMElement result = creator.getResult();
			OMElement response = getStub()._getServiceClient().sendReceive(result);
			this.result = response.getFirstElement().getText();
		} catch (Exception e) {
			throw new EdmException(e.getMessage(), e);
		}
	}

	@Override
	public String getResult() {
		return this.result;
	}

	@Override
	public void finalizeExport() {
		// TODO Auto-generated method stub
		
	}

}
