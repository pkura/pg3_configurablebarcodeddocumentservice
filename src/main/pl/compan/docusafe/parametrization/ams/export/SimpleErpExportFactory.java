package pl.compan.docusafe.parametrization.ams.export;


import pl.compan.docusafe.core.exports.*;

public class SimpleErpExportFactory implements ExportFactory {

	@Override
	public XmlCreator getXmlCreator() {
		return new SimpleWSXmlCreator();
	}

	@Override
	public Exporter getExporter() {
		return new SimpleWSExporter();
	}

	@Override
	public Checker getChecker() {
		return new SimpleWSChecker();
	}
}
