/**
 * 
 */
package pl.compan.docusafe.parametrization.ams;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.exports.ExportedDocument;
import pl.compan.docusafe.core.exports.FixedAssetLiquidationDocument;
import pl.compan.docusafe.core.exports.FixedAssetPosition;
import pl.compan.docusafe.core.exports.FixedAssetPositionDetail;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.DivisionImpl;
import pl.compan.docusafe.parametrization.ams.hb.AmsAssetCardInfo;
import pl.compan.docusafe.parametrization.imgwwaw.ImgwDocumentLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 11-03-2014
 * ds_trunk_2014
 * WniosekStLogic.java
 */
public class WniosekStLogic extends AbstractDocumentLogic{

	protected static Logger log = LoggerFactory.getLogger(WniosekStLogic.class);
    private static WniosekStLogic instance;
    
    private static final String AUTOR = "AUTOR";
    private static final String NUMER_WNIOSKU = "NUMER_WNIOSKU";
    private static final String DATA_REJESTRACJI = "DATA_REJESTRACJI";
    private static final String DATA_OPERACJI = "DATA_OPERACJI";
    private static final String EXPORT_STATUS = "EXPORT_STATUS";
    private static final String TYP_OPERACJI = "TYP_OPERACJI";
    private static final String ODDZIAL = "ODDZIAL";
    private static final String DATA_OBOWIAZYWANIA = "DATA_OBOWIAZYWANIA";
    private static final String IDENTYFIKATOR_LIKWIDACJI = "IDENTYFIKATOR_LIKWIDACJI";
    private static final String SRODEK_TRWALY_NUMER_INWENTARZOWY = "SRODEK_TRWALY_NUMER_INWENTARZOWY";
    
    
    private static final String STATUS_DOC_EXPORTED_1 = "0";
    private static final String STATUS_DOC_EXPORTED_2 = "1";
    
    private static final String SUFFIX = "WZ";
	
	public static synchronized WniosekStLogic getInstance() {
        if (instance == null)
            instance = new WniosekStLogic();
        return instance;
    }

    @Override
    public boolean canChangeDockind(Document document) throws EdmException {
        return false;
    }

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
		log.info("--- Wniosek o przeniesienie ST : START PROCESS !!! ---- {}", event);
		try
		{   
			 Map<String, Object> map = Maps.newHashMap();
	         map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
	         map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
	         document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
	         
	         java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

	         DSUser author = DSApi.context().getDSUser();
	         String user = author.getName();
	         String fullName = author.asFirstnameLastname();

	         perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
	         perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
	         perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
	         perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
	         perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

	         Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
	         perms.addAll(documentPermissions);
	         this.setUpPermission(document, perms);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}
	
	@Override
	public void documentPermissions(Document document) throws EdmException {
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {
		 	Folder folder = Folder.getRootFolder();
	        folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
	        document.setFolder(folder);

	        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
	        document.setTitle("" + (String) fm.getValue(fm.getMainFieldCn()));
	        document.setDescription("" + (String) fm.getValue(fm.getMainFieldCn()));
		
	}
	
	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
		Map<String, Object> toReload = Maps.newHashMap();
		DSUser currentUser = DSUser.getLoggedInUserSafely();
		toReload.put(AUTOR, currentUser.getId());
		toReload.put(DATA_OPERACJI, new Date());
		fm.reloadValues(toReload);
	}
	
    public ExportedDocument buildExportDocument(OfficeDocument doc) throws EdmException {
        FixedAssetLiquidationDocument exported = new FixedAssetLiquidationDocument();

        FieldsManager fm = doc.getFieldsManager();
        
        Integer exportStatus = fm.getIntegerValue(EXPORT_STATUS);
        
        //likwidacja srodka
        if (exportStatus.equals(STATUS_DOC_EXPORTED_1)) {
        	 AmsAssetCardInfo amsAssetCardInfo = AmsAssetCardInfo.findByInventoryNumber((String)fm.getKey(SRODEK_TRWALY_NUMER_INWENTARZOWY));
             
        	 exported.setOperationTypeCode(fm.getEnumItemCn(TYP_OPERACJI));
             exported.setDepartmentCode(fm.getEnumItemCn(ODDZIAL));
             exported.setOperation((Date)fm.getKey(DATA_OPERACJI));
             exported.setValidity((Date) fm.getKey(DATA_OBOWIAZYWANIA));
             
             FixedAssetPosition position;
             exported.addPosition(position = new FixedAssetPosition());
             
             position.setCardIdentifier(Long.valueOf(amsAssetCardInfo.getErpId().toString()));
             position.setLiquidationCode(fm.getEnumItemCn(IDENTYFIKATOR_LIKWIDACJI));
             position.addPosition(new FixedAssetPositionDetail().setRatio(BigDecimal.ONE).setRegistrationSystemCode(Docusafe.getAdditionPropertyOrDefault("export.fixed_asset.registration_amortization_code1", "BILANSOWY")));
             position.addPosition(new FixedAssetPositionDetail().setRatio(BigDecimal.ONE).setRegistrationSystemCode(Docusafe.getAdditionPropertyOrDefault("export.fixed_asset.registration_amortization_code2", "PODATKOWY")));

             //exported.setDescription(buildAdditionDescription(getAdditionFieldsDescription(doc, fm)));
             
        //przyjecie srodka
        } /*else if (exportStatus.equals(STATUS_DOC_EXPORTED_2)) {
        	 exported.setOperation(new Date());
             exported.setValidity((Date) fm.getKey("DATA_OBOWIAZYWANIA"));
             
             Integer oddzialId = Integer.valueOf(fm.getEnumItemCn("ODDZIAL"));
             DSDivision division = DivisionImpl.find(oddzialId);
             exported.setDepartmentCode(division.getCode());
             
             exported.setDepartmentCode(fm.getEnumItemCn("ODDZIAL"));
             exported.setOperationTypeCode(Docusafe.getAdditionPropertyOrDefault("export.fixed_asset.operation_type", "W"));
             //exported.setOperationTypeCode(fm.getEnumItemCn("TYP_OPERACJI"));
             FixedAssetPosition position;
             exported.addPosition(position = new FixedAssetPosition());
             position.setLiquidationCode(fm.getEnumItemCn("POWOD"));
             position.setCardIdentifier(fm.getEnumItem("NR_INWENTARZOWY") != null ? Long.valueOf(fm.getEnumItem("NR_INWENTARZOWY").getCentrum()) : 1);

             //position.addPosition(new FixedAssetPositionDetail().setRatio(BigDecimal.ONE).setRegistrationSystemCode(Docusafe.getAdditionPropertyOrDefault("export.fixed_asset.registration_amortization_code1", "BILANSOWY")));
             //position.addPosition(new FixedAssetPositionDetail().setRatio(BigDecimal.ONE).setRegistrationSystemCode(Docusafe.getAdditionPropertyOrDefault("export.fixed_asset.registration_amortization_code2", "PODATKOWY")));

             //exported.setDescription(ImgwDocumentLogic.buildAdditionDescription(getAdditionFieldsDescription(doc, fm)));
        }*/
        
        return exported;
    }
    
    private Map<String,String> getAdditionFieldsDescription(OfficeDocument doc, FieldsManager fm) throws EdmException {
        Map<String,String> additionFields = Maps.newLinkedHashMap();
        additionFields.put("Nr KO",doc.getOfficeNumber() != null ? doc.getOfficeNumber().toString() : "brak");
        additionFields.put("Opis przyczyny", fm.getStringValue("OPIS_PRZYCZYNY"));
        additionFields.put("Numer �rodka", fm.getStringValue("NUMER_SRODKA"));

        return additionFields;
    }
    
    static String buildAdditionDescription(Map<String, String> additionField) throws EdmException {
        StringBuilder description = new StringBuilder();
        Map<String, String> additionFields = additionField;
        for (Map.Entry<String, String> item : additionFields.entrySet()) {
            if (!Strings.isNullOrEmpty(item.getValue())) {
                description.append(item.getKey())
                        .append(": ")
                        .append(item.getValue())
                        .append("; ");
            }
        }
        return description.toString();
    }

}
