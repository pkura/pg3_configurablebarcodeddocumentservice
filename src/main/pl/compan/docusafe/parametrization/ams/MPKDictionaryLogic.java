/**
 * 
 */
package pl.compan.docusafe.parametrization.ams;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;

import com.google.common.collect.Lists;
import com.google.common.collect.ImmutableList;
/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class MPKDictionaryLogic extends DwrDictionaryBase {
	
	public static final String MPK_FIELD = "MPK";
	public static final String BUDGET_DICTIONARY_CN = "BUDGET";
	public static final String BUDGET_FINAL_DICTIONARY_CN = "BUDGET_FINAL";
	
	public static final String STATUS_FIELD = "STATUS";
	public static final String OKRES_BUDZETOWY_FIELD = "OKRES_BUDZETOWY";
	public static final String RODZAJ_BUDZETU_FIELD = "RODZAJ_BUDZETU";
	public static final String IDENTYFIKATOR_PLANU_FIELD = "IDENTYFIKATOR_PLANU";
	public static final String BUDZET_FIELD = "BUDZET";
	public static final String POZYCJA_PLANU_ZAKUPOW_RAW_FIELD = "POZYCJA_PLANU_ZAKUPOW_RAW";
	public static final String POZYCJA_BUDZETU_RAW_FIELD = "POZYCJA_BUDZETU_RAW";
	public static final String ZADANIE_FINANSOWE_PLANU_RAW_FIELD = "ZADANIE_FINANSOWE_PLANU_RAW";
	public static final String ZADANIE_FINANSOWE_BUDZETU_RAW_FIELD = "ZADANIE_FINANSOWE_BUDZETU_RAW";
	
	public static final String UNPLANNED_BUDGET = "1";
	public static final String PLANNED_BUDGET = "2";
	
	public static final Integer PROCESS_CREATION_STATUS_ID = 100;
	public static final Integer CORRECTION_BY_AUTHOR_STATUS_ID = 150;
	public static final Integer RECEIVE_OPINIONS_STATUS_ID = 200;
	public static final Integer CONFIRMATION_SHIFT_APPLICANT_STATUS_ID = 250;
	
	protected static final EnumValues EMPTY_ENUM_VALUE = new EnumValues(new HashMap<String, String>(), new ArrayList<String>());
	
	//public static final List<String> TYPES_OF_COST = ImmutableList.<String>builder().add()
	
	@Override
	public void filterAfterGetValues(Map<String, Object> dictionaryFilteredFieldsValues, String dicitonaryEntryId, Map<String, Object> fieldsValues, Map<String, Object> documentValues) {
		super.filterAfterGetValues(dictionaryFilteredFieldsValues, dicitonaryEntryId, fieldsValues, documentValues);
		String dictionaryCn = null;
		if (dictionaryFilteredFieldsValues.containsKey(MPK_FIELD + "_" + RODZAJ_BUDZETU_FIELD)) {
			dictionaryCn = MPK_FIELD;
		} else if(dictionaryFilteredFieldsValues.containsKey(BUDGET_DICTIONARY_CN + "_" + OKRES_BUDZETOWY_FIELD)) {
			dictionaryCn = BUDGET_DICTIONARY_CN;
		} else if(dictionaryFilteredFieldsValues.containsKey(BUDGET_FINAL_DICTIONARY_CN + "_" + OKRES_BUDZETOWY_FIELD)) {
			dictionaryCn = BUDGET_FINAL_DICTIONARY_CN;
		}
		
		if (MPK_FIELD.equals(dictionaryCn)) {
			String selectedId = ((EnumValues) dictionaryFilteredFieldsValues.get(dictionaryCn + "_" + RODZAJ_BUDZETU_FIELD)).getSelectedId();
			if (StringUtils.isNotBlank(selectedId)) {
				if (selectedId.equals(UNPLANNED_BUDGET)) {
					List<String> fieldsToHide = Lists.newArrayList(dictionaryCn + "_" + IDENTYFIKATOR_PLANU_FIELD, dictionaryCn + "_" + POZYCJA_PLANU_ZAKUPOW_RAW_FIELD, dictionaryCn + "_" + ZADANIE_FINANSOWE_PLANU_RAW_FIELD);
					hideFieldsInDictionary(dictionaryFilteredFieldsValues, fieldsToHide);
				} else if (selectedId.equals(PLANNED_BUDGET)) {
					List<String> fieldsToHide = Lists.newArrayList(dictionaryCn + "_" + BUDZET_FIELD, dictionaryCn + "_" + POZYCJA_BUDZETU_RAW_FIELD, dictionaryCn + "_" + ZADANIE_FINANSOWE_BUDZETU_RAW_FIELD);
					hideFieldsInDictionary(dictionaryFilteredFieldsValues, fieldsToHide);
				}
			} else {
				List<String> fieldsToHide = Lists.newArrayList(dictionaryCn + "_" + IDENTYFIKATOR_PLANU_FIELD, dictionaryCn + "_" + POZYCJA_PLANU_ZAKUPOW_RAW_FIELD, dictionaryCn + "_" + ZADANIE_FINANSOWE_PLANU_RAW_FIELD, dictionaryCn + "_" + BUDZET_FIELD, dictionaryCn + "_" + POZYCJA_BUDZETU_RAW_FIELD, dictionaryCn + "_" + ZADANIE_FINANSOWE_BUDZETU_RAW_FIELD);
				hideFieldsInDictionary(dictionaryFilteredFieldsValues, fieldsToHide);
			}
		} else if(BUDGET_DICTIONARY_CN.equals(dictionaryCn) || BUDGET_FINAL_DICTIONARY_CN.equals(dictionaryCn)) {
			Integer status = (Integer) fieldsValues.get(STATUS_FIELD);
			if (status == null || status.equals(PROCESS_CREATION_STATUS_ID) || status.equals(CORRECTION_BY_AUTHOR_STATUS_ID) || status.equals(RECEIVE_OPINIONS_STATUS_ID) || status.equals(CONFIRMATION_SHIFT_APPLICANT_STATUS_ID)) {
				List<String> fieldsToHide = Lists.newArrayList(dictionaryCn + "_" + BUDZET_FIELD, dictionaryCn + "_" + POZYCJA_BUDZETU_RAW_FIELD, dictionaryCn + "_" + ZADANIE_FINANSOWE_BUDZETU_RAW_FIELD);
				hideFieldsInDictionary(dictionaryFilteredFieldsValues, fieldsToHide);
			}
		}
	}

	private void hideFieldsInDictionary(Map<String, Object> dictionaryFilteredFieldsValues, List<String> fieldsToHide) {
		for (String fieldCn : fieldsToHide) {
			((EnumValues)dictionaryFilteredFieldsValues.get(fieldCn)).setSelectedOptions(new ArrayList<String>());
			((EnumValues)dictionaryFilteredFieldsValues.get(fieldCn)).setAllOptions(new ArrayList<Map<String, String>>());
		}
	}
}
