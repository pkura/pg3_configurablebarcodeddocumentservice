package pl.compan.docusafe.parametrization.ams;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DSUserEnumField;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.AssignUtils;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ProjPraceZleconeLogic
        extends
        AbsKartaObiegowaLogic {

    private static final String FIELD_DYSPONENT_SRODKOW = "DYSPONENT_SRODKOW";

    public static final String FIELD_KOSZTY_BEZPOSREDNIE = "KOSZTY_BEZPOSREDNIE";
    public static final String FIELD_KOSZTY_BEZPOSREDNIE_PROC = "KOSZTY_BEZPOSREDNIE_PROC";
    public static final String FIELD_KOSZTY_POSREDNIE_PROC_KB = "KOSZTY_POSREDNIE_PROC_KB";
    public static final String FIELD_LACZNA_WARTOSC_PROJEKTU = "LACZNA_WARTOSC_PROJEKTU";
    protected static final String FIELD_JEDN_REALIZUJACA = "JEDN_REALIZUJACA";
    protected static final String FIELD_KIEROWNIK_PROJEKTU = "KIEROWNIK_PROJEKTU";
    protected static final String FIELD_KIEROWNIK_PIONU = "KIEROWNIK_PIONU";
    protected static final String FIELD_DICTIONARY_OPINIE_DODATKOWE = "OPINIE_DODATKOWE";
    protected static final String FIELD_DICTIONARY_PRACOWNICY_WYKONUJACY = "PRACOWNICY_WYKONYWUJACY"; // todo zmienic na WYKONUJACY
    protected static final String DICTIONARY_FIELD_OPINIA = "OPINIA";
    protected static final String DICTIONARY_FIELD_JEDNOSTKA = "JEDNOSTKA";
    protected static final String DICTIONARY_FIELD_WYKONUJACY = "WYKONAWCA";
    protected static final String DICTIONARY_FIELD_WYKONUJACY_TEXT = "WYKONAWCA_TEXT";

    private static final String JPDL_ACCEPTATION_CN_OPINIE_DODATKOWE_DIVISION_ID = "opinieDodatkowe-divisionId";
    private static final String JPDL_ACCEPTATION_CN_FIELD_WYKONAWCA = "field-wykonawca";
    private static final String JPDL_ACCEPTATION_CN_USER_WYKONAWCA = "user-wykonawca";

    private static final String STATUS_OPINIA_DODATKOWA = "opinia_dodatkowa";

    private static final String TABLENAME_MULTIPLE_VALUE = "dsg_ams_karta_obiegowa_multiple_value";
    private static final String KIEROWNICY_PROFILE_NAME = Docusafe.getAdditionProperty("profile.name.kierownik");
    private static final String STATUS_ZGODA_DZIEKANA_WYDZIALU_CN = "zgoda_dziekana_wydzialu";


    protected static Logger log = LoggerFactory.getLogger(ProjPraceZleconeLogic.class);


        @Override
        public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {

        String statusCn = fm.getEnumItemCn(STATUS);
        StringBuilder msgBuilder = new StringBuilder();

//        tryUpdateKierownikEnumField(values, fm);

        if (msgBuilder.length() > 0) {
            values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
            return new Field("messageField", msgBuilder.toString(), Field.Type.BOOLEAN);
        }

        return null;
    }

//    private void tryUpdateKierownikEnumField(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
//
//        DataBaseEnumField komorkaField = (DataBaseEnumField)fm.getField("KOMORKA_ORG_AMS_NADZOR");
//        if (komorkaField==null || !komorkaField.isSubmit()) return;
//
//        FieldData komorkaFieldData = values.get("DWR_KOMORKA_ORG_AMS_NADZOR");
//        if (komorkaFieldData==null) return;
//        String komorkaId = komorkaFieldData.getEnumValuesData().getSelectedId();
//        if (StringUtils.isBlank(komorkaId)) return;
//        String divisionGuid = komorkaField.getEnumItem(komorkaId).getCn();
//        DSDivision division = DSDivision.find(divisionGuid);
//
//        DSUser author = null;
//        String statusCn = fm.getEnumItemCn(STATUS);
//        if (statusCn == null || STATUS_PROCESS_CREATION.equalsIgnoreCase(statusCn)){
//            author = DSApi.context().getDSUser();
//        } else {
//            Long docId = fm.getDocumentId();
//            if (docId == null) return;
//            Document doc = Document.find(docId);
//            String authorName = doc.getAuthor();
//            author = DSUser.findByUsername(authorName);
//        }
//
//        DSUser[] divisionUsers = division.getUsers(false);
//        Map<String,DSUser> kierownicy = new HashMap<String, DSUser>();
//        for (DSUser user : divisionUsers){{
//            for (String pn : user.getProfileNames()){
//                if (KIEROWNICY_PROFILE_NAME.equalsIgnoreCase(pn)){
//                    kierownicy.put(user.getName(),user);
//                    break;
//                }
//            }
//        }
//
//        kierownicy.put(author.getName(),author);
//        EnumValues authorEV = FieldsManagerUtils.getUserEnumValues(kierownicy.values());
//        authorEV.setSelectedId(Long.toString(author.getId()));
//        values.put("DWR_KIEROWNIK_PROJEKTU",authorEV);
//    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = new HashMap<String, Object>();
        for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
            if (f.getDefaultValue() != null)
                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));

        DSUser user = DSApi.context().getDSUser();
        toReload.put(FIELD_KIEROWNIK_PROJEKTU, user.getId());

//        Map<String, Object> fmValues = fm.getFieldValues();
//        EnumValues kierownikProjektuEV = FieldsManagerUtils.getSimpleEnumValues(user);
//        fmValues.put(FIELD_KIEROWNIK_PROJEKTU, kierownikProjektuEV);
//        fm.reloadValues(fmValues);

        if (isUserDysponentSrodkow(user))
            toReload.put(FIELD_DYSPONENT_SRODKOW, user.getId());


        fm.reloadValues(toReload);
    }


    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
        try{
            boolean assigned = super.assignee(doc, assignable, openExecution, acceptationCn, fieldCnValue);
            if (assigned)
                return true;

            if (JPDL_ACCEPTATION_CN_OPINIE_DODATKOWE_DIVISION_ID.equals(acceptationCn)) {
                Long dicId = (Long) openExecution.getVariable(JPDL_ACCEPTATION_CN_OPINIE_DODATKOWE_DIVISION_ID);
                if (dicId != null) {
                    try {
                        DSDivision division = DSDivision.findById(dicId);
                        assignable.addCandidateGroup(division.getGuid());
                        AssigneeHandler.addToHistory(openExecution, doc, null, division.getGuid());
                        assigned = true;

                    } catch (EdmException e) {
                        log.error(e.getMessage(), e);
                    }
                } else {
                    log.error("A found value by id-name [" + JPDL_ACCEPTATION_CN_OPINIE_DODATKOWE_DIVISION_ID + "] is null.");
                }
            }

            else if (ACCEPTATION_CN_taskStatus.equalsIgnoreCase(acceptationCn)) {

                if (STATUS_ZGODA_DZIEKANA_WYDZIALU_CN.equals(fieldCnValue)){
                    assignToDysponent(doc, assignable, openExecution);
//                    Set<DSUser> kierownicyPionu = getSupervisors(author);
//                    for (DSUser dsUser : kierownicyPionu)
//                        AssigneeHandlerUtils.assignToUser(doc,assignable, openExecution, dsUser);

                } else
                    throw new Exception("Nie mo�na przypisa� osoby: "+ acceptationCn + ", " + fieldCnValue);
            }

            return assigned;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new IllegalArgumentException(e.getMessage(), e); // Wywal przypisanie jbpm-owe! Albo wszystko ok, albo nie r�b
        }
    }

    private void assignToDysponent(OfficeDocument doc, Assignable assignable, OpenExecution openExecution) throws EdmException {

        String authorname = doc.getAuthor();
        DSUser author = DSUser.findByUsername(authorname);
        DSDivision[] divisions = author.getDivisions();

        boolean assigned = false;
        for (DSDivision d : divisions){
            String divisionCode = d.getCode();

            List<String> userNames = DSApi.context().session().createSQLQuery("select cn from " + ZamowienieLogic.DYSPONENT_VIEW + " where available = 1 and refValue is not null and :divCode like refValue")
                    .setParameter("divCode", divisionCode).list();

            assigned = assigned || !userNames.isEmpty();

            for (String user : userNames) {
                AssignUtils.checkState(DSUser.findByUsername(user) != null, "Nie mo�na odnale�� kierownika pionu: " + user);
                assignable.addCandidateUser(user);
                AssigneeHandler.addToHistory(openExecution, doc, user, null);
            }
        }

        AssignUtils.checkState(assigned, "Nie mo�na odnale�� kierownika pionu dla dzia��w: "+StringUtils.join(divisions,","));
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

//    @Override
//    public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException {
//        super.onAcceptancesListener(doc, acceptationCn);    //To change body of overridden methods use File | Settings | File Templates.
//    }

    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) {
        Map<String, Object> result = new HashMap<String, Object>();
        if (dictionaryName.equals(FIELD_DICTIONARY_PRACOWNICY_WYKONUJACY)) {

            EnumValues wykonawcaEV = DwrUtils.getFieldData(values, joinDictionaryWithField(dictionaryName, DICTIONARY_FIELD_WYKONUJACY), EnumValues.class);
            String wykonawcaToText = null;
            try {
                if (wykonawcaEV != null) {
                    String wykonawcaIdString = wykonawcaEV.getSelectedId();
                    if (StringUtils.isNotEmpty(wykonawcaIdString)) {
                        Long wykonawcaId = Long.parseLong(wykonawcaIdString);
                        DSUser wykonawca = DSUser.findById(wykonawcaId);
                        wykonawcaToText = wykonawca.getWholeName();
                    }
                }
            } catch (Exception e) {
                log.error("Nieudane odnalezienie wykonawcy na podstawie wybranej warto�ci z select-a (EnumField)", e);
            }
            //reset enuma
            if (StringUtils.isNotEmpty(wykonawcaToText)) {
                result.put(joinDictionaryWithField(dictionaryName, DICTIONARY_FIELD_WYKONUJACY_TEXT), wykonawcaToText);

//                DwrUtils.setEnumNoSelected(wykonawcaEV);
//                result.put(joinDictionaryWithField(dictionaryName,DICTIONARY_FIELD_WYKONUJACY), wykonawcaEV);

                DocumentKind dockind = getDocumentKindFromDwrSession();
                if (dockind != null) {
                    DSUserEnumField field = (DSUserEnumField)getFieldFromDictionary(dockind, dictionaryName, DICTIONARY_FIELD_WYKONUJACY);
                    if (field != null) {
                        try {
                            EnumValues enumValues = field.getEnumItemsForDwr(null);
                            result.put(joinDictionaryWithField(dictionaryName, DICTIONARY_FIELD_WYKONUJACY), enumValues);
                        } catch (EntityNotFoundException e) {
                            log.error("Nie udane pobranie u�ytkownik�w do enum field", e);
                            Collection<EnumItem> items = field.getSortedEnumItems();
                            if (items != null) {
                                EnumValues enumValues = FieldsManagerUtils.getEnumValues(items);
                                result.put(joinDictionaryWithField(dictionaryName, DICTIONARY_FIELD_WYKONUJACY), enumValues);
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    private pl.compan.docusafe.core.dockinds.field.Field getFieldFromDictionary(DocumentKind dockind, String dictionaryName, String dictionaryFieldName) {
        FieldsManager fm = dockind.getFieldsManager(null);
        if (fm == null) return null;

        DictionaryField dictionary;
        try {
            dictionary = (DictionaryField) fm.getField(dictionaryName);
        } catch (EdmException e) {
            log.error("Nie mo�na s�ownika " + dictionaryName + " w podanym dockindzie", e);
            return null;
        }

        Pattern pattern = Pattern.compile('('+dictionaryFieldName+"$)|("+dictionaryFieldName+"_\\d+$)");
        for (pl.compan.docusafe.core.dockinds.field.Field field : dictionary.getFields()) {
            if (pattern.matcher(field.getCn()).find()) {
                return field;
            }
        }

        return null;
    }


//    @Override
//    public void setAdditionalValues(FieldsManager fieldsManager, Integer valueOf, String activity) throws EdmException {
//        try {
//            Jbpm4ActivityInfo activityInfo = new Jbpm4ActivityInfo(activity);
//            String taskName = (String) activityInfo.get("task_name");
//            EnumItem status = fieldsManager.getEnumItem(STATUS);
//
//        } catch (Exception e) {
//            log.error(e.getMessage(), e);
//        }
//    }
//
//    private List<Long> toIdsList(Object obj) {
//        List<Long> ids = new ArrayList<Long>();
//        if (obj != null) {
//            if (obj instanceof List<?>)
//                ids = (List<Long>) obj;
//            else if (obj instanceof Long)
//                ids.add((Long) obj);
//        }
//        return ids;
//    }

    @Override
    public void documentPermissions(Document document) throws EdmException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

//    public enum KartaObiegowa {
//        PROJEKT_MIEDZYNARODOWY("projekt-miedzynarodowy"),
//        PROJEKT_KRAJOWY("projekt-krajowy"),
//        WNIOSEK_OPZ("wniosek-opz");
//
//        private final String PROPERTY_NAME;
//
//        private KartaObiegowa(String propertyName) {
//            this.PROPERTY_NAME = propertyName;
//        }
//
//        public static KartaObiegowa getType(DocumentKind documentKind) {
//            return getType(documentKind.getProperties());
//        }
//
//        public static KartaObiegowa getType(Document doc) {
//            return getType(doc.getDocumentKind());
//        }
//
//        public static KartaObiegowa getType(Map<String, String> properties) {
//            if (properties == null || properties.isEmpty())
//                throw new IllegalArgumentException("Podane properties dockind-a s� puste");
//            String propertyVal = properties.get("karta_obiegowa-typ_projektu");
//            if (StringUtils.isEmpty(propertyVal))
//                throw new IllegalArgumentException("W dockindzie w properties nie okre�lono rodzaju karty obiegowej");
//            return getType(propertyVal);
//        }
//
//        public static KartaObiegowa getType(String propertyVal) {
//            try {
//
//                for (KartaObiegowa ko : KartaObiegowa.values()) {
//                    if (ko.PROPERTY_NAME.equalsIgnoreCase(propertyVal))
//                        return ko;
//                }
//                throw new IllegalArgumentException("W dockindzie podano nieznany rodzaj karty obiegowej: " + propertyVal);
//            } catch (IllegalArgumentException e) {
//                log.error(e.getMessage(), e);
//                throw e;
//            }
//        }
//
//        public String getKey() {
//            return this.PROPERTY_NAME;
//        }
//    }

}
