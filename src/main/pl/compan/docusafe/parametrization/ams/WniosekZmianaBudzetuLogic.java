/**
 * 
 */
package pl.compan.docusafe.parametrization.ams;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.ObjectUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.AssignUtils;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.AssigneeLogic;
import pl.compan.docusafe.core.jbpm4.AssigningHandledException;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.ams.ZamowienieLogic.ApplicationDivisionManager;
import pl.compan.docusafe.parametrization.ams.ZamowienieLogic.ApplicationProjectAttendant;
import pl.compan.docusafe.parametrization.ams.ZamowienieLogic.Dysponent;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Sets;
import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.primitives.Ints;

import edu.emory.mathcs.backport.java.util.Collections;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class WniosekZmianaBudzetuLogic extends AbstractDocumentLogic {

	private static final Logger logger = LoggerFactory.getLogger(WniosekZmianaBudzetuLogic.class);
	
	public static final String BUDGET_DICTIONARY_CN = "BUDGET";
	public static final String BUDGET_FINAL_DICTIONARY_CN = "BUDGET_FINAL";
	
	public static final String DYSPONENT_ZRODLOWY_FIELD_CN = "DYSPONENT_ZRODLOWY";
	public static final String DYSPONENT_DOCELOWY_FIELD_CN = "DYSPONENT_DOCELOWY";
	public static final String JEDNOSTKA_OPINIE_FIELD_CN = "JEDNOSTKA_OPINIE";
	public static final String JEDNOSTKA_DODATKOWE_OPINIE_FIELD_CN = "JEDNOSTKA_DODATKOWE_OPINIE";
	public static final String STATUS_FIELD_CN = "STATUS";
	public static final String CZY_OPINIE_FIELD_CN = "CZY_OPINIE";
	
	public static final Integer PROCESS_CREATION_STATUS_ID = 100;
	public static final Integer CORRECTION_BY_AUTHOR_STATUS_ID = 150;
	public static final Integer CONFIRMATION_SHIFT_APPLICANT_STATUS_ID = 200;
	
	protected static final EnumValues EMPTY_ENUM_VALUE = new EnumValues(new HashMap<String, String>(), new ArrayList<String>());
	
	public static final String DOCKIND_CN = "zmiana_budzetu";
	
	public static final String UNPLANNED_BUDGET = "1";
	public static final String PLANNED_BUDGET = "2";
	
    public static final Map<String, AssigneeLogic> ASSIGNEE_LOGIC_MAP = ImmutableMap.<String, AssigneeLogic>builder()
            .put("receive_opinions", new RecipientAssignee())
            .put("first_trustee", new FirstTrusteeAssignee())
            .put("additional_opinions", new RecipientAssignee())
            .put("second_trustee", new SecondTrusteeAssignee())
            .build();

	
    @Override
    public boolean canChangeDockind(Document document) throws EdmException {
    	return false;
    }
    
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
		logger.info("--- Wniosek o zmiane budzetu : START PROCESS !!! ---- {}", event);
		try
		{   
			 Map<String, Object> map = Maps.newHashMap();
	         map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
	         map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
	         document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
	         
	         java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

	         DSUser author = DSApi.context().getDSUser();
	         String user = author.getName();
	         String fullName = author.asFirstnameLastname();

	         perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
	         perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
	         perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
	         perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
	         perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

	         Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
	         perms.addAll(documentPermissions);
	         this.setUpPermission(document, perms);
		}
		catch (Exception e)
		{
			logger.error(e.getMessage(), e);
		}
	}
	
	public void documentPermissions(Document document) throws EdmException {

	}

	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {

	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) throws EdmException {
		AssigneeLogic assigneer = ASSIGNEE_LOGIC_MAP.get(acceptationCn);
		if (assigneer != null) {
            FieldsManager fm = doc.getFieldsManager();
            try {
                assigneer.assign(doc, fm, assignable, openExecution);
            } catch (AssigningHandledException e) {
                AssignUtils.addRemarkAndAssignToAdmin(doc, assignable, e.getMessage());
            } catch (Exception e) {
                AssignUtils.addRemarkAndAssignToAdmin(doc, assignable, "Nie znaleziono osoby do przypisania, przypisano do admin'a");
                logger.error(e.getMessage(), e);
            } finally {
                return true;
            }
        }
		return false;
	}
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues,Long documentId) {
		Map<String, Object> result = Maps.newHashMap();
		
		if(dictionaryName.equals(BUDGET_DICTIONARY_CN) || dictionaryName.equals(BUDGET_FINAL_DICTIONARY_CN)) {
			DwrUtils.setRefValueEnumOptions(values, result, "OKRES_BUDZETOWY", dictionaryName + "_", "KOMORKA_BUDZETOWA", "dsg_ams_view_budget_unit_no_ref", null, EMPTY_ENUM_VALUE, "", "_1");
			Integer status = (Integer) fieldsValues.get(STATUS_FIELD_CN);
			//if (status != null && !status.equals(PROCESS_CREATION_STATUS_ID) && !status.equals(CORRECTION_BY_AUTHOR_STATUS_ID) && !status.equals(CONFIRMATION_SHIFT_APPLICANT_STATUS_ID)) {
		        DwrUtils.setRefValueEnumOptions(values, result, "KOMORKA_BUDZETOWA", dictionaryName + "_", "BUDZET", "dsg_ams_view_budget_unit_ko", null, EMPTY_ENUM_VALUE, "", "_1");
		        reloadRefEnumItems(values, result, dictionaryName, "POZYCJA_BUDZETU_RAW", "BUDZET", DOCKIND_CN, EMPTY_ENUM_VALUE, null);
		        reloadRefEnumItems(values, result, dictionaryName, "ZADANIE_FINANSOWE_BUDZETU_RAW", "POZYCJA_BUDZETU_RAW", DOCKIND_CN, EMPTY_ENUM_VALUE, null);
			//}
		}
		return result;
	}
	
    void reloadRefEnumItems(Map<String, ?> values, Map<String, Object> results, String dictionaryCn, final String fieldCn, final String refFieldCn, String dockindCn, EnumValues emptyHiddenValue, String refFieldSelectedId) {
        try {
            Optional<pl.compan.docusafe.core.dockinds.field.Field> resourceField = findField(dockindCn, dictionaryCn, fieldCn);

            if (resourceField.isPresent()) {
                String dicWithSuffix = dictionaryCn + "_";
                if (refFieldSelectedId == null) {
                    refFieldSelectedId = getFieldSelectedId(values, results, dicWithSuffix, refFieldCn);
                }

                String fieldSelectedId = "";
                EnumValues enumValue = getEnumValues(values, dicWithSuffix + fieldCn);
                if (enumValue != null) {
                    if (!enumValue.getSelectedOptions().isEmpty()) {
                        fieldSelectedId = enumValue.getSelectedOptions().get(0);
                    }
                }

                EnumValues enumValues = resourceField.get().getEnumItemsForDwr(ImmutableMap.<String, Object>of(refFieldCn, ObjectUtils.toString(refFieldSelectedId), fieldCn, ObjectUtils.toString(fieldSelectedId)));

                if (isEmptyEnumValues(enumValues)) {
                    results.put(dicWithSuffix + fieldCn, emptyHiddenValue);
                } else {
                    results.put(dicWithSuffix + fieldCn, enumValues);
                }
            } else {
                throw new EdmException("cant find " + fieldCn + " field");
            }
        } catch (EdmException e) {
            logger.error(e.getMessage(), e);
        }
    }

    static boolean isEmptyEnumValues(EnumValues enumValues) {
        return enumValues.getAllOptions().isEmpty() || (enumValues.getAllOptions().size() == 1 && enumValues.getAllOptions().get(0).containsKey(""));
    }

    static String getFieldSelectedId(Map<String, ?> values, Map<String, Object> results, String dic, String fieldCn) {
        String fieldSelectedId = "";
        if (results.containsKey(dic + fieldCn)) {
            try {
                fieldSelectedId = ((EnumValues)results.get(dic + fieldCn)).getSelectedId();
            } catch (Exception e) {
                logger.error("CAUGHT "+e.getMessage(),e);
            }
        } else {
            try {
                EnumValues enumValue = getEnumValues(values, dic + fieldCn);
                if (enumValue != null) {
                    fieldSelectedId = enumValue.getSelectedOptions().get(0);
                }
            } catch (Exception e) {
                logger.error("CAUGHT " + e.getMessage(), e);
            }
        }

        return fieldSelectedId;
    }

    public static EnumValues getEnumValues(Map<String, ?> values, String fieldName) {
        Object value = values.get(fieldName);
        if (value instanceof FieldData) {
            return ((FieldData) value).getEnumValuesData();
        } else if (value instanceof EnumValues) {
            return (EnumValues) value;
        }
        return null;
    }

    static Optional<pl.compan.docusafe.core.dockinds.field.Field> findField(String dockindCn, String dictionaryCn, final String dictionaryFieldCn) throws EdmException {
        return Iterables.tryFind(DocumentKind.findByCn(dockindCn).getFieldsMap().get(dictionaryCn).getFields(), new Predicate<pl.compan.docusafe.core.dockinds.field.Field>() {
            @Override
            public boolean apply(pl.compan.docusafe.core.dockinds.field.Field input) {
                return dictionaryFieldCn.equals(input.getCn());
            }
        });
    }
    
    static class RecipientAssignee implements AssigneeLogic {

        public void assign(OfficeDocument doc, FieldsManager fm, Assignable assignable, OpenExecution openExecution) throws EdmException {
        	boolean assigne = false;
        	String realizator = (String) fm.getValue(JEDNOSTKA_OPINIE_FIELD_CN);
        	String[] realizatorArray = realizator.split(",");
        	if(realizatorArray.length > 1){
        		String fullName = realizatorArray[1];
        		String[] firstLastName = fullName.split(" "); 
        		if(DSUser.findByFirstnameLastname(firstLastName[1], firstLastName[2], false) != null){
        			assignable.addCandidateUser(DSUser.findByFirstnameLastname(firstLastName[1], firstLastName[2], false).getName());
    				AssigneeHandler.addToHistory(openExecution, doc, DSUser.findByFirstnameLastname(firstLastName[1], firstLastName[2], false).getName(), null);
    				assigne = true;
        		}
        	} else {
        		if(DSDivision.findByName(realizatorArray[0]) != null){
        			assignable.addCandidateGroup(DSDivision.findByName(realizatorArray[0]).getGuid());
    				AssigneeHandler.addToHistory(openExecution, doc, null, DSDivision.findByName(realizatorArray[0]).getGuid());
    				assigne = true;
        		}
        	}
        	if (assigne) {
        		cleanRecipientValue(doc, fm);
        		cleanIsOpinionCheckBox(doc, fm);
        	}
        }
        
        private void cleanRecipientValue(OfficeDocument document, FieldsManager fm) {
        	try {
	    		String sql = "delete from dso_person where discriminator ='recipient' and document_id="
	    				+ document.getId().toString();
	    		PreparedStatement ps = null;
	    		try {
	    			ps = DSApi.context().prepareStatement(sql);
	    			ps.executeUpdate();
	    		} catch (SQLException e) {
	    			DSApi.context().rollback();
	    			logger.error(e.getMessage(), e);
	    		}
        	} catch (EdmException e) {
        		logger.error(e.getMessage(), e);
        	}
        }
        
        private void cleanIsOpinionCheckBox(OfficeDocument document, FieldsManager fm) {
        	try {
        		fm.getDocumentKind().setOnly(document.getId(), Collections.singletonMap(CZY_OPINIE_FIELD_CN, false));
        	} catch (EdmException e) {
        		logger.error(e.getMessage(), e);
        	}
        }
    }
    
    static class FirstTrusteeAssignee implements AssigneeLogic {
    	
        public void assign(OfficeDocument doc, FieldsManager fm, Assignable assignable, OpenExecution openExecution) throws EdmException {
        	Integer trusteeId = (Integer) fm.getKey(DYSPONENT_ZRODLOWY_FIELD_CN);
            String username = DSUser.findById(trusteeId.longValue()).getName();
            assignable.addCandidateUser(username);
            AssigneeHandler.addToHistory(openExecution, doc, username, null);
        }
    }
    
    static class SecondTrusteeAssignee implements AssigneeLogic {
    	
        public void assign(OfficeDocument doc, FieldsManager fm, Assignable assignable, OpenExecution openExecution) throws EdmException {
        	Integer trusteeId = (Integer) fm.getKey(DYSPONENT_DOCELOWY_FIELD_CN);
            String username = DSUser.findById(trusteeId.longValue()).getName();
            assignable.addCandidateUser(username);
            AssigneeHandler.addToHistory(openExecution, doc, username, null);
        }
    }
}
