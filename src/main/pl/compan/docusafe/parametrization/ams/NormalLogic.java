package pl.compan.docusafe.parametrization.ams;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.crypto.DSA;
import org.directwebremoting.extend.Sleeper;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;

import pl.compan.docusafe.core.office.OutOfficeDocument;

import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;

import pl.compan.docusafe.core.users.DSUser;

import pl.compan.docusafe.service.barcodes.DocumentBarcodable;
import pl.compan.docusafe.service.barcodes.PdfBarcodedDocument;
import pl.compan.docusafe.service.barcodes.recognizer.Recognizer;
import pl.compan.docusafe.service.barcodes.recognizer.ZxingRecognizer;
import pl.compan.docusafe.service.barcodes.slicer.ImageSlicer;
import pl.compan.docusafe.service.barcodes.slicer.ImageSlicerImpl;

import pl.compan.docusafe.util.DateUtils;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.MultiFormatReader;

public class NormalLogic extends AbstractDocumentLogic {

	private static final long serialVersionUID = 1L;
	private static final String PRZYCHODZACE = "normal";
	private static final String WYCHODZACE = "normal_out";
	private static final String WEWNETRZNE = "normal_int";
	
	private static final String STATUS_FIELD_NAME = "RODZAJ_PISMA_IN";
	private static final String DOC_DATE = "DOC_DATE";
	private static final String VALIDATE_DATE = "Zapisanie pisma z datą z przyszłości zablokowane.";

	private static NormalLogic instance;
	protected static Logger log = LoggerFactory.getLogger(NormalLogic.class);

	public static NormalLogic getInstance() {
		if (instance == null)
			instance = new NormalLogic();
		return instance;
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event) {
		log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
		try{
			Map<String, Object> map = Maps.newHashMap();
			if(WYCHODZACE.equals(document.getDocumentKind().getCn())){
				/*map.put(ASSIGN_USER_PARAM, null);
				FieldsManager fm = document.getFieldsManager();
				String userName = fm.getEnumItemCn("KANCELARIA");
				map.put(ASSIGN_USER_PARAM, userName);*/
				map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
	            map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			}
			else{
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
            map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			}
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
            
            java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

			DSUser author = DSApi.context().getDSUser();
			String user = author.getName();
			String fullName = author.asFirstnameLastname();

			perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

			Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
			perms.addAll(documentPermissions);
			this.setUpPermission(document, perms);
            
            
            log.error(AvailabilityManager.isAvailable("addToWatch").toString());
            if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
            
            
            
            Thread thread = new Thread(new GetBC(document.getId()));
            thread.start();
            
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
	}
	

	private class GetBC implements Runnable
	{
		private Long documentId;
		
		public GetBC(Long documentId)
        {
			this.documentId = documentId;
        }
		
		@Override
		public void run() {
			try
			{
//				System.out.println("Spi");
//				Thread.currentThread().sleep(5000);
//				System.out.println("Wstalk");
				DSApi.openAdmin();
				Document document = Document.find(documentId);
				ImageSlicer slicerService = new ImageSlicerImpl(8, 33, 15, 10);
	            List<Recognizer> recognizers = Lists.newArrayList(getRecognizer(),getAlternativeRecognizer());
	
	            for(pl.compan.docusafe.core.base.Attachment atta: document.getAttachments())
	            {	            	
	            	File file = File.createTempFile(atta.getMostRecentRevision().getOriginalFilename(),".pdf");
	            	OutputStream outputStream = new FileOutputStream(file);
	            	IOUtils.copy(atta.getMostRecentRevision().getBinaryStream(), outputStream);
	            	outputStream.close();
	            
	
	                String fileName = file.getName();
	
	                DocumentBarcodable documentBC;
	
	                if (fileName.toLowerCase().endsWith(".pdf")) {
	                	documentBC = new PdfBarcodedDocument();
	                } else {
	                    log.error("Nie obs�ugiwany plik: "+fileName);
	                    continue;
	                }
	
	                documentBC.setFile(file);
	            
	            
		            boolean dockindFiltering = false;
		            boolean debugMode = false;
		            
		            String bcString = findCorrectBarcode(slicerService, recognizers, documentBC, dockindFiltering, debugMode);
		            if(bcString != null && bcString.length() > 0)
		            {
		            	DSApi.context().begin();
		            	 if (document instanceof InOfficeDocument)
		         	        ((InOfficeDocument) document).setBarcode(bcString);
		         	    else if (document instanceof OutOfficeDocument)
		         		    ((OutOfficeDocument) document).setBarcode(bcString);
		            	 DSApi.context().commit();
		            }
	            }								
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
			}
			finally
			{
				DSApi._close();
			}
		}
		
	}
	
  
    protected Recognizer getRecognizer() {
        Hashtable<DecodeHintType, Object> hints = new Hashtable<DecodeHintType, Object>();
        hints.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);
        hints.put(DecodeHintType.POSSIBLE_FORMATS, Lists.newArrayList(BarcodeFormat.CODE_128));
        return new ZxingRecognizer(new MultiFormatReader(), hints);
    }

   
    protected Recognizer getAlternativeRecognizer() {
        Hashtable<DecodeHintType, Object> hints = new Hashtable<DecodeHintType, Object>();
        hints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
        hints.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);
        hints.put(DecodeHintType.POSSIBLE_FORMATS, Lists.newArrayList(BarcodeFormat.CODE_128));
        return new ZxingRecognizer(new MultiFormatReader(), hints);
    }
	
	 /**
    *
    * @param slicerService
    * @param recognizers
    * @param document
    * @param dockindFiltering
    * @param debugMode
    * @return barkod dokumentu, je�li zosta� on potwierdzony poprzez pomy�lne wyszukania dokumentu w systemie
    */
   private String findCorrectBarcode(ImageSlicer slicerService, List<Recognizer> recognizers, DocumentBarcodable document, boolean dockindFiltering, boolean debugMode) {
       String fileName = document.getFile().getName();
       for (Recognizer currentRecognizer : recognizers) {
           try {
               List<BufferedImage> pages = document.getPages();
               int pageNo = 0;
               for (BufferedImage page : pages) {
                   pageNo++;
                   if (debugMode) {
                       savePageImage(Docusafe.getAdditionProperty("ams.ocrs.debug"), fileName, pageNo, page);
                   }

                   slicerService.setPage(page);
                   int sliceNo = 0;
                   for (BufferedImage slice : slicerService.getSlicedImages()) {
                       sliceNo++;
                       if (debugMode) {
                           saveSlicedImage(Docusafe.getAdditionProperty("ams.ocrs.debug"), fileName, pageNo, sliceNo, slice);
                       }

                       try {
                           String barcode = currentRecognizer.recognize(slice);
                           if (barcode != null) {

                               /* WALIDACJA U�
                              String[] a = barcode.split("-");
                               if (a.length > 1) {
                                   String[] b = a[1].split("/");
                                   if (b.length > 1 && b[0].length() == 4) {
                                       log.debug("ZNALEZIONO " + fileName + " barkod:" + barcode + " " + " " + currentRecognizer.recognizerDescription());
                                       if (debugMode) {
                                           saveSlicedImage(getDebugModeFolder(), document.getFile().getName(), pageNo, sliceNo, slice);
                                       }
                                       document.addBarcode(barcode);
                                       return barcode;
                                   }
                               }
                               log.debug("ZNALEZIONO b�edny " + fileName + " barkod:" + barcode + " " + " " + currentRecognizer.recognizerDescription());
                               */

                               log.debug("ZNALEZIONO " + document.getFile().getName() + " barkod:" + barcode);

                               return barcode;


                           }
                       } catch (EdmException e) {
                           log.error(e.getMessage(), e);
                       }

                   }
               }


           } catch (IOException e) {
               log.error(e.getMessage(), e);
           }
       }
       return null;
   }
   
   private void savePageImage(String debugFolder, String fileName, int pageNo, BufferedImage page) throws IOException {
       File pageToSave = new File(debugFolder + "//" + fileName + "_p" + pageNo + ".jpg");
       ImageIO.write(page, "jpg", pageToSave);
   }
   
   private void saveSlicedImage(String debugFolder, String fileName, int pageNo, int sliceNo, BufferedImage slice) throws IOException {
       File sliceToSave = new File(debugFolder + "//" + fileName + "_p" + pageNo + "_s" + sliceNo + ".jpg");
       ImageIO.write(slice, "jpg", sliceToSave);
   }


	
	private boolean validateDwrDates(FieldsManager fm) {
      //  if (isStatus(fm, 5000, true)) {
            try {
                    Date currentDate = new Date(); 
                    Date docDate = (Date)fm.getValue(DOC_DATE); 

                    if (currentDate != null && docDate != null && docDate.after(currentDate)){
                        return false;
                    } else {
                    	return true;
                    }
                    	
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
       // }
        return true;
    }
	
	private boolean isStatus (FieldsManager fm, int statusId, boolean nullStatusOk) {
        try {
            EnumItem statusEnum = fm.getEnumItem(STATUS_FIELD_NAME);
            if (statusEnum == null)
                return nullStatusOk;
            Integer curStatusId = statusEnum.getId();
            if (curStatusId.equals(statusId))
                return true;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return false;
    }
	
	
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException{
		if(AvailabilityManager.isAvailable("validateDocDate.AMS")){
			 StringBuilder msgBuilder = new StringBuilder();
		     FieldsManager fm = Document.find(documentId).getFieldsManager();
		
		     if(!validateDwrDates(fm)){
		     	msgBuilder.append(VALIDATE_DATE).append(System.getProperty("line.separator"));
		     }

		     if(msgBuilder.length() > 0) {
		         throw new EdmException(msgBuilder.toString());
		     }
		}
	}
	
	
	@Override
	public void documentPermissions(Document document) throws EdmException {
	}
	
	public TaskListParams getTaskListParams(DocumentKind dockind, long id, TaskSnapshot task) throws EdmException {
        TaskListParams params = super.getTaskListParams(dockind, id, task);
        try{
        	 FieldsManager fm = dockind.getFieldsManager(id);
             if (PRZYCHODZACE.equals(fm.getDocumentKind().getCn()) && fm.getValue("EMAIL_SENDER") != null) {
                 String source = "e-mail";
                 params.setAttribute(source, 1);
             }
             else {
             	String source = "Kancelaria";
                 params.setAttribute(source, 1);
             }
        } catch (Exception e){
        	log.error(e.getMessage(), e);
        }
        return params;
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        as.useFolderResolver(document);
        as.useAvailableProviders(document);
        log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());

        if (document != null && document.getDocumentKind().getCn().equals("normal_out"))
        {
            OfficeCase officeCase = ((OfficeDocument) document).getContainingCase();
            if (officeCase != null)
            {
                PreparedStatement ps = null;
                try {
                    ps = DSApi.context().prepareStatement("update dsg_normal_dockind set NR_DOKUMENTU = ? where document_id = ?");
                    ps.setString(1, officeCase.getOfficeId());
                    ps.setLong(2, document.getId());
                    ps.executeUpdate();
                    ps.close();

                    DSApi.context().closeStatement(ps);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                } finally {
                    DSApi.context().closeStatement(ps);
                }
            }
        }
    }
    
	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
		Map<String, Object> toReload = Maps.newHashMap();
		if ((WEWNETRZNE.equals(fm.getDocumentKind().getCn()) || WYCHODZACE.equals(fm.getDocumentKind().getCn())) && DSApi.context().getDSUser().getDivisionsWithoutGroup().length>0) {
			String sender = "u:"+DSApi.context().getDSUser().getName();
			sender+=(";d:"+DSApi.context().getDSUser().getDivisionsWithoutGroup()[0].getGuid());
			toReload.put("SENDER_HERE", sender);	
		}
		if(PRZYCHODZACE.equals(fm.getDocumentKind().getCn()) && fm.getValue("EMAIL_SENDER") != null){
			toReload.put("CZY_MAIL", "true");
		}
		
        //toReload.put("DATA_GODZINA_WPLYWU", Calendar.getInstance().getTime());
        toReload.put("DOC_DATE", Calendar.getInstance().getTime());
		fm.reloadValues(toReload);
	}


}
