/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.services;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.ContractBogdet;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.GetContractBogdet;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.GetContractBogdetResponse;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class ContractBogdetImport extends AbstractDictionaryImport {

	private static Logger logger = LoggerFactory.getLogger(ContractBogdetImport.class);
	
	private static final String SERVICE_PATH = "/services/DictionaryService";
	private static final String SERVICE_TABLE_NAME_SOURCES = "dsg_ams_contract_bogdet";
	
	private DictionaryServiceStub stub;
	private int importPageSize;
	private int importPageNumber;
	private Set<Double> itemIds;
	
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);
	}

	public boolean doImport() throws RemoteException, EdmException {
		if (stub != null) {
			GetContractBogdet params = new GetContractBogdet();
			params.setPage(importPageNumber);
			params.setPageSize(importPageSize);
			GetContractBogdetResponse response = stub.getContractBogdet(params);
			if (response != null) {
				ContractBogdet[] items = response.get_return();
				if (items != null && items.length > 0) {
					for (ContractBogdet item : items) {
						if (item != null) {
							Map<String, Object> fieldValues = new HashMap<String, Object>(1);
							fieldValues.put("idm", item.getIdm());
							List<pl.compan.docusafe.parametrization.ams.hb.ContractBogdet> founds = DictionaryUtils.findByGivenFieldValues(pl.compan.docusafe.parametrization.ams.hb.ContractBogdet.class, fieldValues);
							
							if (!founds.isEmpty()) {
								for (pl.compan.docusafe.parametrization.ams.hb.ContractBogdet found : founds) {
									found.setAllFieldsFromServiceObject(item);
									found.save();
								}
							} else {
								pl.compan.docusafe.parametrization.ams.hb.ContractBogdet contractBogdet = new pl.compan.docusafe.parametrization.ams.hb.ContractBogdet();
								contractBogdet.setAllFieldsFromServiceObject(item);
								contractBogdet.save();
							}
							
						}
					}
				} else {
					return true;
				}
			} else {
				return true;
			}
		}
		return true;
	}
	
	@Override
	public void initImport() {
		importPageSize = 1000;
		importPageNumber = 1;
		itemIds = new HashSet<Double>();
	}
	
    @Override
    public void finalizeImport() throws EdmException {
        //int deleted = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME_SOURCES, true);
    }

}
