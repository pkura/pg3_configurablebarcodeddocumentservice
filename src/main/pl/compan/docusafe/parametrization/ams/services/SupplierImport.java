/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.services;

import java.rmi.RemoteException;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class SupplierImport extends AbstractDictionaryImport {

    private static final String SERVICE_PATH = "/services/DictionaryService";
    DictionaryServiceStub stub;
    DictionaryServiceStub.Supplier[] items;
    int itemPointer;
    private StringBuilder message;

    private int importPageSize;
    private int importPageNumber;

    protected static Logger logger = LoggerFactory.getLogger(SupplierImport.class);

    @Override
    public void initConfiguration(AxisClientConfigurator conf) throws Exception {
        stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
        conf.setUpHttpParameters(stub, SERVICE_PATH);
    }

    @Override
    public boolean doImport() throws RemoteException, EdmException {
        message = new StringBuilder();
        if (stub != null) {
            DictionaryServiceStub.GetSupplier params = new DictionaryServiceStub.GetSupplier();
            params.setPageSize(importPageSize);
            params.setPage(importPageNumber++);
            DictionaryServiceStub.GetSupplierResponse response;
//            int pageIndex = 1;
//            boolean itemsWereEmpty = false;
//            while (!itemsWereEmpty) {
//                params.setPage(pageIndex);
//                params.setPageSize(PAGE_SIZE);
                if (params != null && (response = stub.getSupplier(params)) != null) {
                    items = response.get_return();
                    if (items != null) {
                        message.append("Pobrano kontrahent�w do przetworzenia: " + items.length);
                        for (DictionaryServiceStub.Supplier item : items) {
                            updateOrCreateNewPerson(item);
                        }
                        return false;
                    } else {
//                        itemsWereEmpty = true;
                        message.append("Nie pobrano, serwis zwraca pust� tablic�");
                        return true;
                    }
//                    pageIndex++;
                }
//            }
        }
        return true;
    }

    private void updateOrCreateNewPerson(DictionaryServiceStub.Supplier item) throws EdmException {
        String streetBuilder = null;
        if (StringUtils.isNotEmpty(item.getDostawca_ulica())) {
            streetBuilder = item.getDostawca_ulica().trim() + (
                    StringUtils.isNotEmpty(item.getDostawca_nrdomu()) ?
                            " " + item.getDostawca_nrdomu().trim() + (
                                    StringUtils.isNotEmpty(item.getDostawca_nrmieszk()) ?
                                            "/" + item.getDostawca_nrmieszk().trim() : "")
                            : "");
        }

        List<Person> found = Person.findByGivenFieldValue("wparam", (long) item.getId());
        boolean createBasePerson = true;

        if (!found.isEmpty()) {
            for (Person foundPerson : found) {

                if (!(foundPerson instanceof Sender || foundPerson instanceof Recipient)) {
                    createBasePerson = false;
                }

                foundPerson.setZip(item.getDostawca_kodpoczt() != null ? StringUtils.left(item.getDostawca_kodpoczt().trim(), 14) : null);
                foundPerson.setLocation(item.getDostawca_miasto() != null ? StringUtils.left(item.getDostawca_miasto().trim(), 50) : null);
                foundPerson.setOrganization(item.getDostawca_nazwa() != null ? StringUtils.left(item.getDostawca_nazwa().trim(), 128) : null);
                foundPerson.setNip(item.getDostawca_nip() != null ? StringUtils.left(item.getDostawca_nip().trim(), 15) : null);
                foundPerson.setStreet(streetBuilder);
                foundPerson.update();
            }
        }

        if (createBasePerson) {
            Person newPerson = new Person();
            newPerson.setWparam((long) item.getId());
            newPerson.setZip(item.getDostawca_kodpoczt() != null ? StringUtils.left(item.getDostawca_kodpoczt().trim(), 14) : null);
            newPerson.setLocation(item.getDostawca_miasto() != null ? StringUtils.left(item.getDostawca_miasto().trim(), 50) : null);
            newPerson.setOrganization(item.getDostawca_nazwa() != null ? StringUtils.left(item.getDostawca_nazwa().trim(), 128) : null);
            newPerson.setNip(item.getDostawca_nip() != null ? StringUtils.left(item.getDostawca_nip().trim(), 15) : null);
            newPerson.setStreet(streetBuilder);
            logger.debug(newPerson.toString());
            newPerson.create();
            message.append(" Utworzono nowego kontrahenta, id: " + newPerson.getId());
        }

    }

    @Override
    public boolean isSleepAfterEachDoImport() {
        return false;
    }

    @Override
    public void initImport() {
        items = null;
        importPageNumber = 1;
        importPageSize = 1000;
        itemPointer = 0;
    }

    @Override
    public String getMessage() {
        return message.length() > 0 ? message.toString() : null;
    }
}
