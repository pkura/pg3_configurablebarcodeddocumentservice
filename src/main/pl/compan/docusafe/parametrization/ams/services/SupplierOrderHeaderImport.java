/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.services;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.axis2.context.ConfigurationContextFactory;
import org.apache.commons.io.IOUtils;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import pl.com.simple.simpleerp.zamdost.ZamdostTYPE;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.parametrization.ams.hb.SupplierOrderPosition;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.GetSupplierOrderHeader;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.GetSupplierOrderHeaderResponse;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.SupplierOrderHeader;
import pl.compan.docusafe.ws.ams.GenericDocumentServiceStub;
import pl.compan.docusafe.ws.ams.GenericDocumentServiceStub.GetDocument;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class SupplierOrderHeaderImport extends AbstractDictionaryImport {

	private static Logger logger = LoggerFactory.getLogger(WarehouseImport.class);
	
	private static final String SERVICE_PATH = "/services/DictionaryService";
	private static final String SERVICE_GD_PATH = "/services/GenericDocumentService";
	private static final String SERVICE_TABLE_NAME_SOURCES = "dsg_ams_warehouse";
	
	private DictionaryServiceStub stub;
	private GenericDocumentServiceStub gdStub;
	private int importPageSize;
	private int importPageNumber;
	private Set<Long> itemIds;
	public static final String GD_DOCUMENT_TYPE = "zamdost";
	
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);

		AxisClientConfigurator axisContext = new AxisClientConfigurator();
		
		gdStub = new GenericDocumentServiceStub(axisContext.getAxisConfigurationContext());
		conf.setUpHttpParameters(gdStub, SERVICE_GD_PATH);
		System.out.println(SERVICE_GD_PATH);
	}

	public boolean doImport() throws RemoteException, EdmException {
		System.out.println("START SERWIS");
		if (stub != null) {
			GetSupplierOrderHeader params = new GetSupplierOrderHeader();
			params.setPage(importPageNumber);
			params.setPageSize(importPageSize);
			GetSupplierOrderHeaderResponse response = stub.getSupplierOrderHeader(params);
			if (response != null) {
				SupplierOrderHeader[] items = response.get_return();
				if (items != null && items.length > 0) {
					for (SupplierOrderHeader item : items) {
						System.out.println(item.getIdm());
						if (item != null) {
							Map<String, Object> fieldValues = new HashMap<String, Object>(1);
							fieldValues.put("idm", item.getIdm());
							List<pl.compan.docusafe.parametrization.ams.hb.SupplierOrderHeader> founds = DictionaryUtils.findByGivenFieldValues(pl.compan.docusafe.parametrization.ams.hb.SupplierOrderHeader.class, fieldValues);
							
							if (!founds.isEmpty()) {
								for (pl.compan.docusafe.parametrization.ams.hb.SupplierOrderHeader found : founds) {
									found.setAllFieldsFromServiceObject(item);
									found.save();
									itemIds.add(found.getId());
									importPosition(found.getIdm(),found.getId());
								}
							} else {
								pl.compan.docusafe.parametrization.ams.hb.SupplierOrderHeader supplierOrderHeader = new pl.compan.docusafe.parametrization.ams.hb.SupplierOrderHeader();
								supplierOrderHeader.setAllFieldsFromServiceObject(item);
								supplierOrderHeader.save();
								itemIds.add(supplierOrderHeader.getId());
								importPosition(supplierOrderHeader.getIdm(),supplierOrderHeader.getId());
							}
							
						}
					}
				} else {
					return true;
				}
			} 
			
		}
//		importPosition("ZDD /2013/00001");
		return true;
	}
	
	private void importPosition(String idm,Long id) throws EdmException
	{
		try
		{
			GetDocument params = new GetDocument();
			params.setDocumentType(GD_DOCUMENT_TYPE);
			params.setIdm(idm);
			String response = new String(gdStub.getDocument(params).get_return().getBytes(), "UTF-8");
			SAXReader reader = new SAXReader();
			org.dom4j.Document doc = reader.read(new ByteArrayInputStream(response.getBytes()));
			List pozycjeF = doc.getRootElement().element("zamdostpozycje").elements("zamdostpoz");
			 for (Iterator<Element> iterator = pozycjeF.iterator(); iterator.hasNext();) 
			 {
				 Element el = iterator.next();
				 
				 Map<String, Object> fieldValues = new HashMap<String, Object>(1);
				 fieldValues.put("zamdospoz_id",  Long.parseLong(el.element("zamdospoz_id").getTextTrim()));
				 List<pl.compan.docusafe.parametrization.ams.hb.SupplierOrderPosition> founds = DictionaryUtils.findByGivenFieldValues(pl.compan.docusafe.parametrization.ams.hb.SupplierOrderPosition.class, fieldValues);
				 if(!founds.isEmpty())
				 {
					 for (pl.compan.docusafe.parametrization.ams.hb.SupplierOrderPosition found : founds) {
							found.setValueFromElement(el,idm,id);
							found.save();
						}
				 }
				 else
				 {
					 SupplierOrderPosition position = new SupplierOrderPosition(el,idm,id);
					 position.save();
				 }
				 

			}
//			JAXBContext jaxbContext = JAXBContext.newInstance(ZamdostTYPE.class);
//			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
////		ZamdostTYPE zamowienie = (ZamdostTYPE) unmarshaller.unmarshal(new ByteArrayInputStream(response.getBytes()));				
//			System.out.println(zamowienie.getStan());
//			System.out.println(zamowienie.getZamdostId());
		}
		catch(Exception e)
		{
			throw new EdmException(e);
		}
		
 
	}
	
	
	@Override
	public void initImport() {
		importPageSize = 1000;
		importPageNumber = 1;
		itemIds = new HashSet<Long>();
	}
	
    @Override
    public void finalizeImport() throws EdmException {
        //int deleted = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME_SOURCES, true);
    }

}
