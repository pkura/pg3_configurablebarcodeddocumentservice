/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.services;

import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImportService;
import pl.compan.docusafe.service.dictionaries.DictionaryImport;
import pl.compan.docusafe.util.DateUtils;

import com.google.common.collect.ImmutableList;
/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class AMSFinancialTasksImport extends AbstractDictionaryImportService {
    
	private static final ImmutableList<DictionaryImport> AVAILABLE_IMPORTS = ImmutableList.<DictionaryImport>builder()
            .add(new FinancialTasksImport())
            .build();

    @Override
    protected ImmutableList<DictionaryImport> getAvailableImports() {
        return AVAILABLE_IMPORTS;
    }

    @Override
    protected long getTimerDefaultPeriod() {
        return 600l;
    }

    @Override
    protected long getTimerStartDelay() {
        return 25 * DateUtils.SECOND;
    }
}
