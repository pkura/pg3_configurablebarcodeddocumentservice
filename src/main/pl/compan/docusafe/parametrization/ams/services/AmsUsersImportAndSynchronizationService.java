/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.services;

import java.sql.Timestamp;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Constants;
import pl.compan.docusafe.core.dockinds.field.DSUserEnumField;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.Profile;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.Person;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.*;

import com.google.common.collect.Lists;

/**
 * serwis do synchronizacji userow w AMS
 * 
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class AmsUsersImportAndSynchronizationService extends ServiceDriver implements Service {
	
	private static final Logger logger = LoggerFactory.getLogger(AmsUsersImportAndSynchronizationService.class);
	private static final StringManager sm = StringManager.getManager(Constants.Package);
	
	public static final String AMS_IMPORTED = "AMS_IMPORTED";
	public static final String DEFAULT_PASSWORD = "qaz";
	
	public static final Integer PAGE_SIZE = 100;
	
	private static Profile kieroniwkProfile;
	private static Profile podstawowyProfile;
	
	private Timer timer;
	
	public AmsUsersImportAndSynchronizationService() {
		
	}
	
	protected void start() throws ServiceException {
		logger.info("AmsUsersImportAndSynchronizationService zostal uruchomiony");
		if (timer != null) {
			timer.cancel();
		}
		timer = new Timer(true);
		timer.schedule(new Import(), 0, DateUtils.DAY);
		console(Console.INFO, sm.getString("UsersImportAndSynchronizationServiceServiceStart"));
	}

	protected void stop() throws ServiceException {
		if (timer != null) {
			timer.cancel();
		}
		console(Console.INFO, sm.getString("UsersImportAndSynchronizationServiceServiceStop"));
	}

	protected boolean canStop() {
		return true;
	}
	
	class Import extends TimerTask {
		
		private static final String SERVICE_PATH = "/services/DictionaryService";
		
		DictionaryServiceStub stub;
		Person[] items;
		
		public void run() {
			console(Console.INFO, sm.getString("UsersImportAndSynchronizationServiceServiceStartSynchronization"));
			List<String> usersRegistrationNumbersFromResponse = Lists.newArrayListWithExpectedSize(1200);
			try {
				DSApi.openAdmin(); 
				initConfiguration();
				GetPerson params = new GetPerson();
				boolean itemsWereEmpty = false;
				int pageIndex = 1;
				while (!itemsWereEmpty) {
					log.error("Strona "+ (pageIndex*PAGE_SIZE));
					log.debug("1 -"+System.currentTimeMillis());
					params.setPage(pageIndex);
					params.setPageSize(PAGE_SIZE);
					GetPersonResponse response = stub.getPerson(params);
					log.debug("2 -"+System.currentTimeMillis());
					if (params != null && response != null) {
						items = response.get_return();
						if (items != null) {
							for (Person item : items) {
								log.error("PERSON "+item.getNazwisko()+" "+item.getImie()+" "+ item.getGuid()+" "+ item.getCzy_aktywny_osoba());
								log.debug("3 -"+System.currentTimeMillis());
								if(item.getCzy_aktywny_osoba() < 1)
								{
									console(Console.INFO, "Uzytkownik nieaktywny "+ item.getGuid() + " "+item.getCzy_aktywny_osoba());
									log.error("Uzytkownik nieaktywny "+ item.getGuid() + " "+item.getCzy_aktywny_osoba());
									continue;
								}
								DSUser user = updateOrCreateNewUser(item);
								if(user != null)
								{
									log.error("Dodaje usera do nieusuwanych "+ user.getName());
									usersRegistrationNumbersFromResponse.add(user.getName());
								}
								log.debug("4 -"+System.currentTimeMillis());
								addProfileToKierownik(item);
//								addProfilePodstawoey(item);
							}
						} else {
							itemsWereEmpty = true;
						}
						pageIndex++;
					}
				}
				
				deleteNotExistingInResponseUsers(usersRegistrationNumbersFromResponse);
				console(Console.INFO, sm.getString("UsersImportAndSynchronizationServiceServiceStopSynchronization"));
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			
			try {
				DSApi.close();
			} catch (EdmException e) {
				logger.error(e.getMessage(), e);
			}
		}
		/**
		 * Dodaje profil kierownikowi
		 * @param item
		 */
		private void addProfileToKierownik(Person item) {
			if(kieroniwkProfile == null)
				return;
			if(StringUtils.isNotEmpty(item.getKierownik_pracownika_guid()))
			{
				try
				{
					DSApi.context().begin();
					DSUser kieroniwk = DSUser.findByIdentifier(item.getKierownik_pracownika_guid());
					kieroniwkProfile.addUser(kieroniwk);
					DSApi.context().commit();
				}
				catch(Exception e)
				{
					log.error("Dodam profil nastÍpnym razem, na razie nie ma jeszcze zaiumpotowanego kieroniwka");
					try {
						DSApi.context().rollback();
					} catch (EdmException edm) {
						logger.error(edm.getMessage(), edm);
					}
				}
			}
			
			
		}

		private void initConfiguration() throws Exception {
			AxisClientConfigurator conf = new AxisClientConfigurator();
			stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
			conf.setUpHttpParameters(stub, SERVICE_PATH);
			if(Docusafe.getAdditionProperty("profile.kieroniwk.id") != null)
				kieroniwkProfile = Profile.findById(Long.parseLong(Docusafe.getAdditionProperty("profile.kieroniwk.id")));
			if(Docusafe.getAdditionProperty("profile.podstawowy.id") != null)
				podstawowyProfile = Profile.findById(Long.parseLong(Docusafe.getAdditionProperty("profile.podstawowy.id")));
		}
		
		private DSUser updateOrCreateNewUser(Person item) {
			DSUser user = null;
			log.error("DODAJE LUN AKTUALIZUJE "+item.getGuid());
			String username = createUserName2(item);
			try {
				log.error("5 -"+System.currentTimeMillis());
				user = DSUser.findByIdentifier(item.getGuid());
				log.error("6 -"+System.currentTimeMillis());
				updateUser(item, user);
				log.debug("7 -"+System.currentTimeMillis());
				return user;
			} catch (UserNotFoundException e) {
				log.error("11-"+System.currentTimeMillis());
				return createNewUser(item);
			} catch (Exception e) {
				logger.error("0000001111222: "+e.getMessage(), e);
			}
			return null;
			
			
//			try {
//				log.debug("5 -"+System.currentTimeMillis());
//				user = DSUser.findByIdentifier(item.getGuid());
//				log.debug("6 -"+System.currentTimeMillis());
//				updateUser(item, user);
//				log.debug("7 -"+System.currentTimeMillis());
//			} catch (UserNotFoundException e) {
//				try {
//					log.debug("8 -"+System.currentTimeMillis());
//					user = DSUser.findAllByExtension(String.valueOf(item.getPracownik_nrewid()));
//					log.debug("9 -"+System.currentTimeMillis());
//					updateUser(item, user);
//					log.debug("10-"+System.currentTimeMillis());
//					return;
//				} catch (Exception e1) {
//					log.debug("11-"+System.currentTimeMillis());
//					createNewUser(item);
//					log.debug("12-"+System.currentTimeMillis());
//					return;
//				}
//			} catch (Exception e) {
//				logger.error("0000001111222: "+e.getMessage(), e);
//			}
		}
		
		/**
		 * Metoda tworzy nowego uzytkownika na podstawie uzytkownika przeslanego przez webservice - parametr item.
		 * @param item - uzytkownik przeslany przez webservice
		 */
		private DSUser createNewUser(Person item) {
			try {
				String userName = createUserName(item);
//				DSDivision division = null;
//				if(item.getKomorka_idn() != null)
//					division= isExistsDivision(item.getKomorka_idn().trim());
				if (item.getCzy_aktywny_osoba() > 0) {
					if (userName != null) {
						DSApi.context().begin();
						DSUser user = UserFactory.getInstance().createUser(userName, item.getImie().trim(), item.getNazwisko().trim());
						user.setPassword(userName);
						user.setExtension(String.valueOf(item.getPracownik_nrewid()));
						user.setRemarks(AMS_IMPORTED);
						user.setIdentifier(item.getGuid());
						user.setCtime(new Timestamp(new Date().getTime()));
						if (StringUtils.isNotBlank(item.getPesel())) {
							user.setPesel(item.getPesel().trim());
						}
//						addUserToDivision(user, division);
						if(podstawowyProfile != null)
							podstawowyProfile.addUser(user);
						DSApi.context().commit();
						console(Console.INFO, sm.getString("UsersImportAndSynchronizationServiceServiceCreatedNewUser", user.getFirstname(), user.getLastname(), user.getName()));
						return user;
					} else {
						console(Console.ERROR, sm.getString("UsersImportAndSynchronizationServiceServiceErrorCreatedUserName", item.getImie(), item.getNazwisko()));
					}
				} else {
					console(Console.ERROR, sm.getString("UsersImportAndSynchronizationServiceServiceErrorDivisionNotFound", item.getImie(), item.getNazwisko(), item.getKomorka_idn()));
				}
			} catch (Exception e) {
				console(Console.ERROR, sm.getString("UsersImportAndSynchronizationServiceServiceErrorCreatedNewUser", item.getImie(), item.getNazwisko()));
				logger.error(e.getMessage(), e);
				try {
					DSApi.context().rollback();
				} catch (EdmException edm) {
					logger.error("ERR: 99999122332 "+edm.getMessage(), edm);
				}
			}
			return null;
		}
		
		/**
		 * Metoda sprawdza czy potrzebna jest aktualizacja uzytkownika user, jezeli tak to aktualizuje go.
		 * @param item - uzytkownik przeslany przez webservice (aktualny)
		 * @param user - uzytkownik znajdujacy sie w bazie danych
		 */
		private void updateUser(Person item, DSUser user) {
			boolean userHasChanged = false;
			try {
				if (item.getCzy_aktywny_osoba() >= 1 && user.isDeleted()) {
					DSApi.context().begin();
					UserFactory.getInstance().revertUser(user.getName());
//					DSUserEnumField.reloadForAll();
					DSApi.context().commit();
					userHasChanged = true;
				} 
				if (!user.getFirstname().equals(item.getImie().trim())) {
					user.setFirstname(item.getImie().trim());
					userHasChanged = true;
				}
				if (!user.getLastname().equals(item.getNazwisko().trim())) {
					user.setLastname(item.getNazwisko().trim());
					userHasChanged = true;
				}

				if (!item.getGuid().equals(user.getIdentifier())){//StringUtils.isBlank(user.getIdentifier()) || !user.getIdentifier().equals()) {
					user.setIdentifier(item.getGuid());
					userHasChanged = true;
				}
				
				if ((StringUtils.isNotBlank(user.getPesel()) && !user.getPesel().equals(item.getPesel().trim())) || (StringUtils.isNotBlank(user.getPesel()) && StringUtils.isNotBlank(item.getPesel()))) {
					user.setPesel(item.getPesel().trim());
					userHasChanged = true;
				}
//				DSDivision division = DSDivision.findByCode(item.getKomorka_idn().trim());
//				if (division != null && !user.inDivision(division)) {
//					DSApi.context().begin();
//					addUserToDivision(user, division);
//					DSApi.context().commit();
//					userHasChanged = true;
//				}
			} catch (Exception e) {
				console(Console.ERROR, sm.getString("UsersImportAndSynchronizationServiceServiceErrorUpdatedUser", item.getImie(), item.getNazwisko()));
				logger.error(e.getMessage(), e);
				userHasChanged = false;
			}
			
			if (userHasChanged) {
				try {
					DSApi.context().begin();
					user.update();
					DSApi.context().commit();
					console(Console.INFO, sm.getString("UsersImportAndSynchronizationServiceServiceUpdatedUser", user.getFirstname(), user.getLastname(), user.getName()));
				} catch (Exception e) {
					console(Console.ERROR, sm.getString("UsersImportAndSynchronizationServiceServiceErrorUpdatedUser", item.getImie(), item.getNazwisko()));
					logger.error("ERR:000981234 "+e.getMessage(), e);
					try {
						DSApi.context().rollback();
					} catch (EdmException edm) {
						logger.error(edm.getMessage(), edm);
					}
				}
			}
		}
		
		/**
		 * Metoda tworzy username dla uzytkownika. Username jest tworzony na podstawie imienia i nazwiska. Jest brana pierwsza litera z imienia i pelne
		 * nazwisko. Username jest bez polskich znakow. Jezeli nazwisko jest dwuczlonowe to pomijany jest myslnik. W przypadku gdy istnieje juz taki username
		 * tylko innego uzytkownika to pobierane sa kolejne litery z imienia i pelne nazwisko. Jezeli nie udalo sie stworzyc username to zwracany jest null
		 * @param item - dane uzytkownika przeslanego przez webservice
		 * @return zwraca utworzony username uzytkownika. W przypadku gdy sie nie udalo stworzyc username jest zwracany null
		 */
		private String createUserName(Person item) {
			// pozbywanie sie polskich znakow w imieniu i nazwisku
			String firstName = Normalizer.normalize(item.getImie().trim(), Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "").toLowerCase();
			// Normalizer.Form.NFD nie normalizuje polskiej litery ≥ dlatego taki triczek jest zastosowany
			firstName = firstName.replace('≥', 'l');
			String lastName = Normalizer.normalize(item.getNazwisko().trim(), Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "").toLowerCase();
			lastName = lastName.replace('≥', 'l');
			// w przypadku nazwisk dwuczlonowych usuwamy myslnik
			lastName = lastName.replace("-", "");
			
			for (int lastIndex=1; lastIndex<firstName.length(); lastIndex++) {
				String userName = firstName.substring(0, lastIndex) + "." + lastName;
				try {
					DSUser.findByUsername(userName);
				} catch (UserNotFoundException e) {
					return userName;
				} catch (EdmException e) {
					logger.error(e.getMessage(), e);
					return null;
				}
			}
			return null;
		}
		
		/**
		 * Metoda tworzy username dla uzytkownika. Username jest tworzony na podstawie imienia i nazwiska. Jest brana pierwsza litera z imienia i pelne
		 * nazwisko. Username jest bez polskich znakow. Jezeli nazwisko jest dwuczlonowe to pomijany jest myslnik. W przypadku gdy istnieje juz taki username
		 * tylko innego uzytkownika to pobierane sa kolejne litery z imienia i pelne nazwisko. Jezeli nie udalo sie stworzyc username to zwracany jest null
		 * @param item - dane uzytkownika przeslanego przez webservice
		 * @return zwraca utworzony username uzytkownika. W przypadku gdy sie nie udalo stworzyc username jest zwracany null
		 */
		private String createUserName2(Person item) {
			// pozbywanie sie polskich znakow w imieniu i nazwisku
			String firstName = Normalizer.normalize(item.getImie().trim(), Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "").toLowerCase();
			// Normalizer.Form.NFD nie normalizuje polskiej litery ≥ dlatego taki triczek jest zastosowany
			firstName = firstName.replace('≥', 'l');
			String lastName = Normalizer.normalize(item.getNazwisko().trim(), Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "").toLowerCase();
			lastName = lastName.replace('≥', 'l');
			// w przypadku nazwisk dwuczlonowych usuwamy myslnik
			lastName = lastName.replace("-", "");
			
			for (int lastIndex=1; lastIndex<firstName.length(); lastIndex++) {
				String userName = firstName.substring(0, lastIndex) + "." + lastName;
					return userName;
				
			}
			return null;
		}
		
		/**
		 * Metoda usuwa uzytkownikow ktorzy nie sa aktualni. Czyli tych ktorzy nie znajduja sie w danych przesylanych przez webservice i ktorzy posiadaja extension.
		 * @param usersRegistrationNumbersFromResponse - lista external ids'ow uzytkownikow ktore zostaly przeslane przez webservice
		 */
		private void deleteNotExistingInResponseUsers(List<String> usersRegistrationNumbersFromResponse) {
			try {
				log.error("ROZPOCZYNAM USUWANIE");
				List<DSUser> users = DSUser.list(DSUser.SORT_FIRSTNAME_LASTNAME);
				for (DSUser user : users) {
					log.error("Iteruje po "+user.getName());
					if (!user.isAdmin() && !usersRegistrationNumbersFromResponse.contains(user.getName())) {
						log.error("Usuwa "+user.getName());
						DSApi.context().begin();
						UserFactory.getInstance().deleteUser(user.getName());
						DSApi.context().commit();
						console(Console.INFO, sm.getString("UsersImportAndSynchronizationServiceServiceDeletedUser", user.getFirstname(), user.getLastname(), user.getName()));
					}
				}
			} catch (EdmException e) {
				logger.error(e.getMessage(), e);
				console(Console.ERROR,  sm.getString("UsersImportAndSynchronizationServiceServiceErrorDeletedUser"));
				try {
					DSApi.context().rollback();
				} catch (EdmException edm) {
					logger.error(edm.getMessage(), edm);
				}
			}
		}
		
//		/**
//		 * Metoda przypisuje uzytkownika do dzialu.
//		 * @param user - uzytkownik, ktory ma byc przypisany
//		 * @param division - dzial do ktorego ma byc przypisany uzytkowmnik
//		 * @throws EdmException
//		 */
//		private void addUserToDivision(DSUser user, DSDivision division) throws EdmException {
//			division.addUser(user);
//			division.update();
//		}
//		
//		/**
//		 * Metoda sprawdza czy istnieje dzial o danym code. Jezeli istnieje to zwraca ten dzial, jezeli nie istnieje to zwraca null
//		 * @param divisionCode - code dzialu
//		 * @return Jezeli istnieje dzial to zwraca ten dzial, jezeli nie istnieje to zwraca null
//		 */
//		private DSDivision isExistsDivision(String divisionCode) {
//			DSDivision division = null;
//			try {
//				division = DSDivision.findByCode(divisionCode);
//			} catch (EdmException e) {
//				logger.error(e.getMessage(), e);
//				division = null;
//			}
//			return division;
//		}
		
	}
	
}

