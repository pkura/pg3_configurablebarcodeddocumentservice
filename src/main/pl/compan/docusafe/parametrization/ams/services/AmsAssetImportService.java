/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.services;

import pl.compan.docusafe.parametrization.imgwwaw.services.AssetCardInfoImport;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImportService;
import pl.compan.docusafe.service.dictionaries.DictionaryImport;
import pl.compan.docusafe.util.DateUtils;

import com.google.common.collect.ImmutableList;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 11-03-2014
 * ds_trunk_2014
 * AmsAssetImportService.java
 */
public class AmsAssetImportService extends AbstractDictionaryImportService {

    private static final ImmutableList<DictionaryImport> AVAILABLE_IMPORTS = ImmutableList.<DictionaryImport>builder()
            .add(new AssetCardInfoImport())
            .build();

    @Override
    protected ImmutableList<DictionaryImport> getAvailableImports() {
        return AVAILABLE_IMPORTS;
    }

    @Override
    protected long getTimerDefaultPeriod() {
        return 600l;
    }

    @Override
    protected long getTimerStartDelay() {
        return 25 * DateUtils.SECOND;
    }
}

