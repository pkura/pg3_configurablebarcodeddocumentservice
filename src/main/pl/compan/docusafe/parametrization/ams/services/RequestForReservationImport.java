/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.services;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.GetRequestForReservation;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.GetRequestForReservationResponse;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.RequestForReservation;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class RequestForReservationImport extends AbstractDictionaryImport {
	
	private static Logger logger = LoggerFactory.getLogger(RequestForReservationImport.class);

	private static final String SERVICE_PATH = "/services/DictionaryService";
	private static final String SERVICE_TABLE_NAME_SOURCES = "dsg_ams_request_for_reservation";
	
	private DictionaryServiceStub stub;
    private StringBuilder message;
	private int importPageSize;
	private int importPageNumber;
	private Set<Long> itemIds;
	
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);
	}

	public boolean doImport() throws RemoteException, EdmException {
        message = new StringBuilder();
		if (stub != null) {
			pl.compan.docusafe.ws.ams.DictionaryServiceStub.GetRequestForReservation params = new GetRequestForReservation();
			message.append("rozmiar strony: " + importPageSize);
			message.append(", nr strony: " + importPageNumber);
			params.setPage(importPageNumber++);
			params.setPageSize(importPageSize);
			GetRequestForReservationResponse response = stub.getRequestForReservation(params);
			if (response != null) {
				RequestForReservation[] items = response.get_return();
				if (items != null && items.length > 0) {
					for (RequestForReservation item : items) {
						if (item != null) {
							Map<String, Object> fieldValues = new HashMap<String, Object>(1);
							fieldValues.put("bdRezerwacjaId", item.getBd_rezerwacja_id());
							List<pl.compan.docusafe.parametrization.ams.hb.RequestForReservation> founds = DictionaryUtils.findByGivenFieldValues(pl.compan.docusafe.parametrization.ams.hb.RequestForReservation.class, fieldValues);
							
							if (!founds.isEmpty()) {
								for (pl.compan.docusafe.parametrization.ams.hb.RequestForReservation found : founds) {
									found.setAllFieldsFromServiceObject(item);
									found.save();
									itemIds.add(found.getId());
								}
							} else {
								pl.compan.docusafe.parametrization.ams.hb.RequestForReservation reservation = new pl.compan.docusafe.parametrization.ams.hb.RequestForReservation();
								reservation.setAllFieldsFromServiceObject(item);
								reservation.save();
								itemIds.add(reservation.getId());
							}
						}	
					}
					return false;
				}
			}
		}
		return true;
	}
	
	@Override
	public void initImport() {
		importPageSize = 1000;
		importPageNumber = 1;
		itemIds = new HashSet<Long>();
	}
	
    @Override
    public void finalizeImport() throws EdmException {
        int deleted = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME_SOURCES, true);
        message.append("Deleted: " + deleted);
    }

    @Override
    public String getMessage() {
        return message!=null?message.toString():null;
    }
    
}
