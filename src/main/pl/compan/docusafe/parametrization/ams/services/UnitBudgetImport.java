package pl.compan.docusafe.parametrization.ams.services;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.ams.hb.BudgetUnitOrganizationUnit;
import pl.compan.docusafe.parametrization.ams.hb.UnitBudget;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;

/**
 * Created by kk on 24.03.14.
 *
 * serwis DictionaryService metoda getUnitBudget
 * tabela dsd_ams_services_budget_unit
 */
public class UnitBudgetImport extends AbstractDictionaryImport {
    protected static Logger log = LoggerFactory.getLogger(UnitBudgetImport.class);

    private static final String SERVICE_PATH = "/services/DictionaryService";

    DictionaryServiceStub stub;
    private StringBuilder message;

    DictionaryServiceStub.UnitBudget[] items;
    private int importPageSize;
    private int importPageNumber;
    private Set<Long> itemIds;

    @Override
    public void initConfiguration(AxisClientConfigurator conf) throws Exception {
        stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
        conf.setUpHttpParameters(stub, SERVICE_PATH);

    }

    @Override
    public void initImport() {
        items = null;
        importPageNumber = 1;
        importPageSize = 1000;
        itemIds = Sets.newHashSet();
    }

    @Override
    public boolean doImport() throws RemoteException, EdmException {
        message = new StringBuilder();
        if (stub != null) {
            DictionaryServiceStub.GetUnitBudget params = new DictionaryServiceStub.GetUnitBudget();
            message.append("rozmiar strony: " + importPageSize);
            message.append(", nr strony: " + importPageNumber);
            params.setPageSize(importPageSize);
            params.setPage(importPageNumber++);
            DictionaryServiceStub.GetUnitBudgetResponse response;
            if ((response = stub.getUnitBudget (params)) != null) {
                items = response.get_return();
                if (items != null) {
                    for (DictionaryServiceStub.UnitBudget item : items) {
                        if (item != null) {
                            if (item.getId() >= 1000) {
                                throw new EdmException("Nieobs�u�one, z powodu: dsg_ams_view_budget_phase");
                            }
                            List<UnitBudget> found = pl.compan.docusafe.parametrization.invoice.DictionaryUtils.findByGivenFieldValue(UnitBudget.class, "erpId", (long)item.getId());
                            if (!found.isEmpty()) {
                                if (found.size() > 1) {
                                    message.append("Znaleziono wi�cej ni� 1 wpis o idm: " + item.getIdm());
                                }
                                for (UnitBudget resource : found) {
                                    resource.setAllFieldsFromServiceObject(item);
                                    resource.save();

                                    itemIds.add(resource.getId());
                                }
                            } else {
                            	UnitBudget resource = new UnitBudget();
                                resource.setAllFieldsFromServiceObject(item);
                                resource.save();

                                message.append("Utworzono nowy obiekt: idm: " + item.getIdm());
                                itemIds.add(resource.getId());
                            }
                        }
                    }
                    return false; // import niezako?czony
                }
            }
        }
        return true;
    }

    @Override
    public void finalizeImport() throws EdmException {
        int deleted = ServicesUtils.disableDeprecatedItem(itemIds, UnitBudget.TABLE_NAME);
        DataBaseEnumField.reloadForTable(UnitBudget.UNIT_BUDGET_NO_REF_VIEW);
        DataBaseEnumField.reloadForTable(UnitBudget.UNIT_BUDGET_VIEW);
        message.append("Usuni�to: " + deleted);
    }

    @Override
    public String getMessage() {
        return message!=null?message.toString():null;
    }

    @Override
    public boolean isSleepAfterEachDoImport() {
        return false;
    }
}
