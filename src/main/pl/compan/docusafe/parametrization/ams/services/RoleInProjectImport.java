/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.services;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.GetRolesInProjects;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.GetRolesInProjectsResponse;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.RolesInProjects;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class RoleInProjectImport extends AbstractDictionaryImport {
	
	private static Logger logger = LoggerFactory.getLogger(RoleInProjectImport.class);
	
	private static final String SERVICE_PATH = "/services/DictionaryService";
	private static final String SERVICE_TABLE_NAME_SOURCES = "dsg_ams_role_in_project";
	
	private DictionaryServiceStub stub;
	private int importPageSize;
	private int importPageNumber;
	private Set<Long> itemIds;
	
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);
	}

	public boolean doImport() throws RemoteException, EdmException {
		if (stub != null) {
			GetRolesInProjects params = new GetRolesInProjects();
			params.setPage(importPageNumber);
			params.setPageSize(importPageSize);
			GetRolesInProjectsResponse response = stub.getRolesInProjects(params);
			if (response != null) {
				RolesInProjects[] items = response.get_return();
				if (items != null && items.length > 0) {
					for (RolesInProjects item : items) {
						if (item != null) {
							Map<String, Object> fieldValues = new HashMap<String, Object>(1);
							fieldValues.put("bpRolaKontraktId", item.getBp_rola_kontrakt_id());
							fieldValues.put("kontraktId", item.getKontrakt_id());
							List<pl.compan.docusafe.parametrization.ams.hb.RoleInProject> founds = DictionaryUtils.findByGivenFieldValues(pl.compan.docusafe.parametrization.ams.hb.RoleInProject.class, fieldValues);
							
							if (!founds.isEmpty()) {
								for (pl.compan.docusafe.parametrization.ams.hb.RoleInProject found : founds) {
									found.setAllFieldsFromServiceObject(item);
									found.save();
									itemIds.add(found.getId());
								}
							} else {
								pl.compan.docusafe.parametrization.ams.hb.RoleInProject roleInProject = new pl.compan.docusafe.parametrization.ams.hb.RoleInProject();
								roleInProject.setAllFieldsFromServiceObject(item);
								roleInProject.save();
								itemIds.add(roleInProject.getId());
							}
							
						}
					}
				} else {
					return true;
				}
			} else {
				return true;
			}
		}
		return true;
	}
	
	@Override
	public void initImport() {
		importPageSize = 1000;
		importPageNumber = 1;
		itemIds = new HashSet<Long>();
	}
	
    @Override
    public void finalizeImport() throws EdmException {
        //int deleted = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME_SOURCES, true);
    }
    
}
