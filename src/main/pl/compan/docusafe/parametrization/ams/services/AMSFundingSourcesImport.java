/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.services;

import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImportService;
import pl.compan.docusafe.service.dictionaries.DictionaryImport;
import pl.compan.docusafe.util.DateUtils;

import com.google.common.collect.ImmutableList;
/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class AMSFundingSourcesImport extends AbstractDictionaryImportService {

    private static final ImmutableList<DictionaryImport> AVAILABLE_IMPORTS = ImmutableList.<DictionaryImport>builder()
            .add(new FundingSourcesImport())
            .add(new FinancialTasksImport())
            .add(new WarehouseImport())
            .add(new PurchaseDocumentTypeImport())
            .add(new ProductionOrderImport())
            .add(new BudgetViewImport())
            .add(new ContractTypeImport())
            .add(new ContractImport())
            .add(new SupplierImport())
            .add(new SupplierBankAccountImport())
            .add(new ContractBogdetImport())
            .add(new VatRateImport())
            .add(new RoleInProjectImport())
            .add(new SupplierOrderHeaderImport())
            .add(new SupplierImportToDictionary())
            .add(new ProductImport())
            .add(new FinancialDocumentImport())
            .add(new AMSEmployeeImport())
            .add(new PaymentTermsImport())
            .add(new PatternsOfCostSharingImport())
            .add(new VatRatioImport())
            .add(new RequestForReservationImport())
            .add(new AmsAssetCardInfoImport())
            .add(new AMSDepartmentImport())
            .add(new AmsAssetOperationTypeImport())
            .add(new AmsAssetLiquidationReasonImport())
            .add(new AmsAssetRecordSystemTypeForOperationTypeImport())
            .add(new BudgetUnitOrganizationUnitImport())
            .add(new PurchasePlanImport())
            .add(new UnitBudgetImport())
            .add(new UnitBudgetKoImport())
            .build();

    @Override
    protected ImmutableList<DictionaryImport> getAvailableImports() {
        return AVAILABLE_IMPORTS;
    }

    @Override
    protected long getTimerDefaultPeriod() {
        return 600l;
    }

    @Override
    protected long getTimerStartDelay() {
        return 25 * DateUtils.SECOND;
    }

}
