/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.services;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.ams.hb.AmsAssetLiquidationReason;
import pl.compan.docusafe.parametrization.ams.hb.AmsAssetRecordSystemTypeForOperationType;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;

import com.google.common.collect.Sets;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 25-03-2014
 * ds_trunk_2014
 * AmsAssetRecordSystemTypeForOperationImport.java
 */
public class AmsAssetRecordSystemTypeForOperationTypeImport extends AbstractDictionaryImport{
	protected static Logger log = LoggerFactory.getLogger(AmsAssetRecordSystemTypeForOperationTypeImport.class);

    private static final String SERVICE_PATH = "/services/DictionaryService";
    private static final String SERVICE_TABLE_NAME_ASSET = "dsg_ams_asset_rec_sys_op_type";

    DictionaryServiceStub stub;
    private StringBuilder message;

    DictionaryServiceStub.AssetRecordSystemTypesForOperationType[] items;
    private int importPageSize;
    private int importPageNumber;
    private Set<Long> itemIds;

    @Override
    public void initConfiguration(AxisClientConfigurator conf) throws Exception {
        stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
        conf.setUpHttpParameters(stub, SERVICE_PATH);

    }

    @Override
    public void initImport() {
        items = null;
        importPageNumber = 1;
        importPageSize = 1000;
        itemIds = Sets.newHashSet();
    }

    @Override
    public boolean doImport() throws RemoteException, EdmException {
        message = new StringBuilder();
        if (stub != null) {
            DictionaryServiceStub.GetAssetRecordSystemTypesForOperationType params = new DictionaryServiceStub.GetAssetRecordSystemTypesForOperationType();
            message.append("rozmiar strony: " + importPageSize);
            message.append(", nr strony: " + importPageNumber);
            params.setPageSize(importPageSize);
            params.setPage(importPageNumber++);
            DictionaryServiceStub.GetAssetRecordSystemTypesForOperationTypeResponse response;
            if ((response = stub.getAssetRecordSystemTypesForOperationType (params)) != null) {
                items = response.get_return();
                if (items != null) {
                    message.append("Pobrano karty srodkow do przetworzenia: " + items.length);
                    for (DictionaryServiceStub.AssetRecordSystemTypesForOperationType item : items) {
                        if (item != null) {
                            List<AmsAssetRecordSystemTypeForOperationType> found = pl.compan.docusafe.parametrization.invoice.DictionaryUtils.findByGivenFieldValue(AmsAssetRecordSystemTypeForOperationType.class, "erpId", item.getId());
                            if (!found.isEmpty()) {
                                if (found.size() > 1) {
                                    message.append("Znaleziono wi�cej ni� 1 wpis o idm: " + item.getId());
                                }
                                for (AmsAssetRecordSystemTypeForOperationType resource : found) {
                                    resource.setAllFieldsFromServiceObject(item);
                                    resource.save();

                                    itemIds.add(resource.getId());
                                }
                            } else {
                            	AmsAssetRecordSystemTypeForOperationType resource = new AmsAssetRecordSystemTypeForOperationType();
                                resource.setAllFieldsFromServiceObject(item);
                                resource.save();

                                message.append("Utworzono nowy obiekt: id: " + item.getId());
                                itemIds.add(resource.getId());
                            }
                        }
                    }
                    return false; // import niezako?czony
                }
            }
        }
        return true;
    }

    @Override
    public void finalizeImport() throws EdmException {
        //int deleted = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME_ASSET, false);
        //DataBaseEnumField.reloadForTable(AmsAssetRecordSystemTypeForOperationType.VIEW_NAME);
        //message.append("Usuni�to: " + deleted);
    }

    @Override
    public String getMessage() {
        return message!=null?message.toString():null;
    }

    @Override
    public boolean isSleepAfterEachDoImport() {
        return false;
    }
}

