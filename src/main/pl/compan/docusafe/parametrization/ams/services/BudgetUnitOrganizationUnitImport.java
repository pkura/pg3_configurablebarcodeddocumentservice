package pl.compan.docusafe.parametrization.ams.services;

import com.google.common.collect.ImmutableMap;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.ams.hb.AmsAssetCardInfo;
import pl.compan.docusafe.parametrization.ams.hb.BudgetUnitOrganizationUnit;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;

/**
 * Created by kk on 24.03.14.
 *
 * serwis DictionaryService metoda getBudgetUnitOrganizationUnit
 * tabela dsd_ams_services_budget_unit_organizational
 */
public class BudgetUnitOrganizationUnitImport extends AbstractDictionaryImport {
	protected static Logger log = LoggerFactory.getLogger(BudgetUnitOrganizationUnitImport.class);

    private static final String SERVICE_PATH = "/services/DictionaryService";

    DictionaryServiceStub stub;
    private StringBuilder message;

    DictionaryServiceStub.BudgetUnitOrganizationUnit[] items;
    private int importPageSize;
    private int importPageNumber;
    private Set<Long> itemIds;

    @Override
    public void initConfiguration(AxisClientConfigurator conf) throws Exception {
        stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
        conf.setUpHttpParameters(stub, SERVICE_PATH);

    }

    @Override
    public void initImport() {
        items = null;
        importPageNumber = 1;
        importPageSize = 1000;
        itemIds = Sets.newHashSet();
    }

    @Override
    public boolean doImport() throws RemoteException, EdmException {
        message = new StringBuilder();
        if (stub != null) {
            DictionaryServiceStub.GetBudgetUnitOrganizationUnit params = new DictionaryServiceStub.GetBudgetUnitOrganizationUnit();
            message.append("rozmiar strony: " + importPageSize);
            message.append(", nr strony: " + importPageNumber);
            params.setPageSize(importPageSize);
            params.setPage(importPageNumber++);
            DictionaryServiceStub.GetBudgetUnitOrganizationUnitResponse response;
            if ((response = stub.getBudgetUnitOrganizationUnit (params)) != null) {
                items = response.get_return();
                if (items != null) {
                    for (DictionaryServiceStub.BudgetUnitOrganizationUnit item : items) {
                        if (item != null) {
                            List<BudgetUnitOrganizationUnit> found = DictionaryUtils.findByGivenFieldValues(BudgetUnitOrganizationUnit.class,
                                    ImmutableMap.of("bdStrBudzetId", item.getBd_str_budzet_id(),
                                                    "budzetId", item.getBudzet_id(),
                                                    "komorkaId", item.getKomorka_id()));
                            if (!found.isEmpty()) {
                                if (found.size() > 1) {
                                    message.append("Znaleziono wi�cej ni� 1 wpis o idm: " + item.getBd_str_budzet_id());
                                }
                                for (BudgetUnitOrganizationUnit resource : found) {
                                    resource.setAllFieldsFromServiceObject(item);
                                    resource.save();

                                    itemIds.add(resource.getId());
                                }
                            } else {
                            	BudgetUnitOrganizationUnit resource = new BudgetUnitOrganizationUnit();
                                resource.setAllFieldsFromServiceObject(item);
                                resource.save();

                                message.append("Utworzono nowy obiekt: idm: " + item.getBd_str_budzet_id());
                                itemIds.add(resource.getId());
                            }
                        }
                    }
                    return false; // import niezako?czony
                }
            }
        }
        return true;
    }

    @Override
    public void finalizeImport() throws EdmException {
        int deleted = ServicesUtils.disableDeprecatedItem(itemIds, BudgetUnitOrganizationUnit.TABLE_NAME, false);
        DataBaseEnumField.reloadForTable(BudgetUnitOrganizationUnit.BUDGET_UNIT_FOR_APPLICATION_VIEW);
        DataBaseEnumField.reloadForTable(BudgetUnitOrganizationUnit.BUDGET_UNIT_NO_REF_VIEW);
        DataBaseEnumField.reloadForTable(BudgetUnitOrganizationUnit.BUDGET_UNIT_VIEW);
        message.append("Usuni�to: " + deleted);
    }

    @Override
    public String getMessage() {
        return message!=null?message.toString():null;
    }

    @Override
    public boolean isSleepAfterEachDoImport() {
        return false;
    }
}
