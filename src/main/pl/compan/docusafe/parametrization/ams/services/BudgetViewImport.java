/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.services;

import java.rmi.RemoteException;
import java.util.*;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.BudgetView2;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.GetBudgetView2;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.GetBudgetView2Response;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class BudgetViewImport extends AbstractDictionaryImport {
	
private static Logger logger = LoggerFactory.getLogger(BudgetViewImport.class);
	
	private static final String SERVICE_PATH = "/services/DictionaryService";
	private static final String SERVICE_TABLE_NAME_SOURCES = "dsg_ams_budget_copy";
	
	private DictionaryServiceStub stub;
    private StringBuilder message;
	private int importPageSize;
	private int importPageNumber;
	private Set<Long> itemIds;
	
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);
	}

	public boolean doImport() throws RemoteException, EdmException {
        message = new StringBuilder();
		if (stub != null) {
			pl.compan.docusafe.ws.ams.DictionaryServiceStub.GetBudgetView2 params = new GetBudgetView2();
			message.append("rozmiar strony: " + importPageSize);
			message.append(", nr strony: " + importPageNumber);
			params.setPage(importPageNumber++);
			params.setPageSize(importPageSize);
//          logger.error("-- " + DateUtils.formatAxisDateTime(new Date(System.currentTimeMillis())) + " -- BudgetViewImport: Wczytuj� stron� " + importPageNumber);
//          logger.error("-- " + DateUtils.formatAxisDateTime(new Date(System.currentTimeMillis())) + " -- BudgetViewImport: Pobieranie response (getBudgetView2)");
			GetBudgetView2Response response = stub.getBudgetView2(params);
			if (response != null) {
//              logger.error("-- " + DateUtils.formatAxisDateTime(new Date(System.currentTimeMillis())) + " -- BudgetViewImport: Wczytywanie {} wpis�w z {} strony", importPageSize, importPageNumber);
				BudgetView2[] items = response.get_return();
				if (items != null && items.length > 0) {
//                  logger.error("-- " + DateUtils.formatAxisDateTime(new Date(System.currentTimeMillis())) + " -- BudgetViewImport: Przetwarzanie pobranych wpis�w");
					for (BudgetView2 item : items) {
						if (item != null) {
							Map<String, Object> fieldValues = new HashMap<String, Object>(1);
							fieldValues.put("bdBudzetId", item.getBd_budzet_id());
							fieldValues.put("bdBudzetRodzajId", item.getBd_budzet_rodzaj_id());
							fieldValues.put("bdGrupaRodzajId", item.getBd_grupa_rodzaj_id());
							fieldValues.put("bdSzablonPozId", item.getBd_szablon_poz_id());
//                          logger.error("-- " + DateUtils.formatAxisDateTime(new Date(System.currentTimeMillis())) + " -- BudgetViewImport: Wyszukiwanie pojedynczego wpisu w DocuSafe");
							List<pl.compan.docusafe.parametrization.ams.hb.BudgetView> founds = DictionaryUtils.findByGivenFieldValues(pl.compan.docusafe.parametrization.ams.hb.BudgetView.class, fieldValues);
							
							if (!founds.isEmpty()) {
								for (pl.compan.docusafe.parametrization.ams.hb.BudgetView found : founds) {
//                                  logger.error("-- " + DateUtils.formatAxisDateTime(new Date(System.currentTimeMillis())) + " -- BudgetViewImport: Znaleziono - aktualizacja");
									found.setAllFieldsFromServiceObject(item);
									found.save();
									itemIds.add(found.getId());
								}
							} else {
//                                   logger.error("-- " + DateUtils.formatAxisDateTime(new Date(System.currentTimeMillis())) + " -- BudgetViewImport: Nie znaleziono - tworzenie");
								pl.compan.docusafe.parametrization.ams.hb.BudgetView budgetView = new pl.compan.docusafe.parametrization.ams.hb.BudgetView();
								budgetView.setAllFieldsFromServiceObject(item);
								budgetView.save();
								itemIds.add(budgetView.getId());
							}
						}	
					}
//                        logger.error("-- " + DateUtils.formatAxisDateTime(new Date(System.currentTimeMillis())) + " -- BudgetViewImport: Zako�czono przetwarzanie wpis�w ze strony {}", importPageNumber);
					return false;
				}
			}
		}
		return true;
	}
	
	@Override
	public void initImport() {
		importPageSize = 1000;
		String valueFromAdds = Docusafe.getAdditionProperty("pageNumberInBudgetViewImport");
		if (valueFromAdds != null) {
			importPageNumber = Integer.valueOf(valueFromAdds);
		} else {
			importPageNumber = 1;
		}
		itemIds = new HashSet<Long>();
	}
	
    @Override
    public void finalizeImport() throws EdmException {
        //int deleted = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME_SOURCES, false);
        //message.append("Deleted: " + deleted);
    }

    @Override
    public String getMessage() {
        return message!=null?message.toString():null;
    }

}