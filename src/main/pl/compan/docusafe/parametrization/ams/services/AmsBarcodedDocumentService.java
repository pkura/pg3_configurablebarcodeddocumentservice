/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.services;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 31-01-2014
 * ds_trunk_2014
 * AmsBarcodedDocumentService.java
 */

import com.google.common.collect.Lists;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.MultiFormatReader;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.service.barcodes.BarcodedDocumentService;
import pl.compan.docusafe.service.barcodes.recognizer.Recognizer;
import pl.compan.docusafe.service.barcodes.recognizer.ZxingRecognizer;
import pl.compan.docusafe.service.barcodes.slicer.ImageSlicer;
import pl.compan.docusafe.service.barcodes.slicer.ImageSlicerImpl;

import java.util.Hashtable;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 01.07.13
 * Time: 09:50
 * To change this template use File | Settings | File Templates.
 */
public class AmsBarcodedDocumentService extends BarcodedDocumentService {

    @Override
    protected String getScansFolder() {
        return "C://ocrs";
    }

    @Override
    protected String getAttachmentColumnCn() {
        return null;
    }

    @Override
    protected String getAttachmentNameTemplate() {
        return "Skan dokumentu nr %file_no%";
    }

    @Override
    protected String getDockindCn() {
        return null;
    }

    @Override
    protected String getFinishedScansFolder() {
        return "usuwaj".equals(Docusafe.getAdditionPropertyOrDefault("ocr.finished-folder", "C://ocrs//finished")) ? null : Docusafe.getAdditionPropertyOrDefault("ocr.finished-folder", "C://ocrs//finished");
    }

    @Override
    protected Recognizer getRecognizer() {
        Hashtable<DecodeHintType, Object> hints = new Hashtable<DecodeHintType, Object>();
        hints.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);
        hints.put(DecodeHintType.POSSIBLE_FORMATS, Lists.newArrayList(BarcodeFormat.CODE_128));
        return new ZxingRecognizer(new MultiFormatReader(), hints);
    }

    @Override
    protected Recognizer getAlternativeRecognizer() {
        Hashtable<DecodeHintType, Object> hints = new Hashtable<DecodeHintType, Object>();
        hints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
        hints.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);
        hints.put(DecodeHintType.POSSIBLE_FORMATS, Lists.newArrayList(BarcodeFormat.CODE_128));
        return new ZxingRecognizer(new MultiFormatReader(), hints);
    }

    @Override
    protected ImageSlicer getImageSlicer() {
        return new ImageSlicerImpl(10, 25, 20, 14);

    }

    @Override
    protected String getDebugModeFolder() {
        return Docusafe.getAdditionProperty("ocr.debug-mode-folder");
    }
}