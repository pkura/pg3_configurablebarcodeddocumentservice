package pl.compan.docusafe.parametrization.ams.services;

import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.imports.simple.repository.SimpleRepositoryImportService;
import pl.compan.docusafe.parametrization.ams.FakturaZakupowaLogic;
import pl.compan.docusafe.parametrization.ams.hb.PurchaseDocumentType;

/**
 * User: kk
 * Date: 03.01.14
 * Time: 14:58
 */
public class AmsSimpleRepositoryImportService extends SimpleRepositoryImportService {

    @Override
    public String getDockindCn() throws EdmException {
        return FakturaZakupowaLogic.DOC_KIND_CN;
    }

    @Override
    public String getDynamicDictionaryCn() throws EdmException {
        return FakturaZakupowaLogic.DEKRETY_FIELD;
    }

    @Override
    public Long getContextId() throws EdmException {
        return FakturaZakupowaLogic.REPOSITORY_POSITION_CONTEXT_ID;
    }

    @Override
    public Long getDictIdToOmitDictionariesUpdate() throws EdmException {
        return null;
    }

    @Override
    public List<Long> getObjectIds() throws EdmException {
        return PurchaseDocumentType.getDocumentTypesId();
    }
}
