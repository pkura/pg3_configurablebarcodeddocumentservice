/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.services;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Constants;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.imgw.DictionaryServiceStub;
import pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPersonAgreement;
import pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPersonAgreementResponse;
import pl.compan.docusafe.ws.imgw.DictionaryServiceStub.PersonAgreement;

/**
 * serwis do synchronizacji userow w IMGW
 * 
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class AMSUsersToDivisionSynchronizationService extends ServiceDriver implements Service {
	
	private static final Logger logger = LoggerFactory.getLogger(AMSUsersToDivisionSynchronizationService.class);
	private static final StringManager sm = StringManager.getManager(Constants.Package);
	
	public static final String IMGW_IMPORTED = "IMGW_IMPORTED";
	
	public static final Integer PAGE_SIZE = 100;
	public List<String> excludedGroup;
	private Timer timer;
	
	public AMSUsersToDivisionSynchronizationService() {
		
	}
	
	protected void start() throws ServiceException {
		logger.info("UsersToDivisionImportAndSynchronizationService zostal uruchomiony");
		if (timer != null) {
			timer.cancel();
		}
		timer = new Timer(true);
		timer.schedule(new Import(), 0, DateUtils.DAY);
		console(Console.INFO, sm.getString("UsersToDivisionImportAndSynchronizationService"));
	}

	protected void stop() throws ServiceException {
		if (timer != null) {
			timer.cancel();
		}
		console(Console.INFO, sm.getString("UsersToDivisionImportAndSynchronizationService"));
	}

	protected boolean canStop() {
		return true;
	}
	
	class Import extends TimerTask {
		
		private static final String SERVICE_PATH = "/services/DictionaryService";
		
		DictionaryServiceStub stub;
		PersonAgreement[] items;
		
		public void run() {
			console(Console.INFO, sm.getString("UsersToDivisionImportAndSynchronizationService"));
			HashMap<String,Set<String>> usersToDiv = new HashMap<String, Set<String>>();//;Lists.newArrayListWithExpectedSize(2000);
			try {
				DSApi.openAdmin();
				initConfiguration();
				GetPersonAgreement params = new GetPersonAgreement();
				boolean itemsWereEmpty = false;
				int pageIndex = 1;
				while (!itemsWereEmpty) {
					params.setPage(pageIndex);
					params.setPageSize(PAGE_SIZE);
					console(Console.INFO, "Pobiera strone");
					GetPersonAgreementResponse response = stub.getPersonAgreement(params);
					if (params != null && response != null) {
						items = response.get_return();
						if (items != null) {
							for (PersonAgreement item : items) 
							{
								console(Console.INFO, "Pobra� nowego "+ item.getGuid() + " |"+ item.getGrupa_zawodowa()+"|");
								if(item.getGrupa_zawodowa() != null && excludedGroup.contains(item.getGrupa_zawodowa().trim()))
								{
									console(Console.INFO, "Przypisanie do grupy zawodowej wykluczonej "+item.getGrupa_zawodowa() );
									continue;
								}
								if(item.getCzy_aktywny_umowa() < 1)
								{
									console(Console.INFO, "Umowa nieaktywna "+item.getGrupa_zawodowa() );
									continue;
								}
								addToMap(item, usersToDiv);
								usersToDiv.get(item.getGuid_osoba());
								createUserToDivision(item);
							}
						} else {
							itemsWereEmpty = true;
						}
						pageIndex++;
					}
					console(Console.INFO, "Zako�czy� strone");
				}
				console(Console.INFO, sm.getString("Zoko�czy� dodawanie"));
				deleteNotExisting(usersToDiv);
				console(Console.INFO, sm.getString("UsersToDivisionImportAndSynchronizationService"));
				
			} catch (Exception e) {
				console(Console.ERROR, "B��d "+e.getMessage());
				logger.error(e.getMessage(), e);
			}
			
			try {
				DSApi.close();
			} catch (EdmException e) {
				logger.error(e.getMessage(), e);
			}
		}
		
		private void addToMap(PersonAgreement item,
				HashMap<String, Set<String>> usersToDiv) {
			Set<String> userDivs = usersToDiv.get(item.getGuid_osoba());
			if(userDivs == null)
				userDivs = new HashSet<String>();
			userDivs.add(item.getKomorka_idn());
			usersToDiv.put(item.getGuid_osoba(), userDivs);
			
		}

		private void createUserToDivision(PersonAgreement item) 
		{
			
			log.debug("OSOAB "+ item.getGuid_osoba()+ " "+ item.getKomorka_idn());
			try {
				DSApi.context().begin();
				DSUser user = DSUser.findByIdentifier(item.getGuid_osoba().trim());
				DSDivision division = DSDivision.findByCode(item.getKomorka_idn().trim());
				division.addUser(user);
				DSApi.context().commit();
			} catch (Exception e) {
				log.error(e.getMessage(),e);
				console(Console.ERROR, "B��d "+ e.getMessage());
				try {
					DSApi.context().rollback();
				} catch (EdmException edm) {
					logger.error(edm.getMessage(), edm);
				}
			}
			
		}

		private void initConfiguration() throws Exception {
			AxisClientConfigurator conf = new AxisClientConfigurator();
			stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
			conf.setUpHttpParameters(stub, SERVICE_PATH);
			log.error("Wczytuje wykluczone grupy zawodowe ="+ Docusafe.getAdditionProperty("userToDivision.grupa_zawodowa.excluded"));
			excludedGroup = Arrays.asList(Docusafe.getAdditionProperty("userToDivision.grupa_zawodowa.excluded").split(","));
		
		}
		
		
		
		/**
		 * Metoda usuwa uzytkownikow ktorzy nie sa aktualni. Czyli tych ktorzy nie znajduja sie w danych przesylanych przez webservice i ktorzy posiadaja extension.
		 * @param usersRegistrationNumbersFromResponse - lista external ids'ow uzytkownikow ktore zostaly przeslane przez webservice
		 */
		private void deleteNotExisting(HashMap<String, Set<String>> usersToDiv) {
			try {
				console(Console.INFO, sm.getString("Rozpoczyna usuwanie powiazan u�ytkownik�w do dzialow kt�rych nie zwr�ci� WS"));
				List<DSUser> users = DSUser.list(DSUser.SORT_FIRSTNAME_LASTNAME);
				for (DSUser user : users)
				{
					DSApi.context().begin();
					
					console(Console.INFO, "Sprawdza dzia� u�ytkownika "+user.asFirstnameLastnameName());
					Set<String> userDivs = usersToDiv.get(user.getIdentifier());
					if(userDivs == null)
						userDivs = new HashSet<String>();
					for(DSDivision div :user.getDivisions())
					{
						log.debug(user.asFirstnameLastnameName() + " "+ div.getCode());
						if( !div.isGroup()  && !userDivs.contains(div.getCode()))
						{
							log.debug( "Usuwa u�ytkownika"+user.asLastnameFirstnameName()+" z dzia�u "+div.getCode() + " " );
							console(Console.INFO, "Usuwa u�ytkownika"+user.asLastnameFirstnameName()+" z dzia�u "+div.getCode() + " " );
							div.removeUser(user);
						}
					}
					DSApi.context().commit();					
				}
				
			} catch (EdmException e) {
				logger.error(e.getMessage(), e);
				console(Console.ERROR,  sm.getString("UsersToDivisionImportAndSynchronizationService"));
				try {
					DSApi.context().rollback();
				} catch (EdmException edm) {
					logger.error(edm.getMessage(), edm);
				}
			}
		}
		
		
		
		/**
		 * Metoda sprawdza czy istnieje dzial o danym code. Jezeli istnieje to zwraca ten dzial, jezeli nie istnieje to zwraca null
		 * @param divisionCode - code dzialu
		 * @return Jezeli istnieje dzial to zwraca ten dzial, jezeli nie istnieje to zwraca null
		 */
		private DSDivision isExistsDivision(String divisionCode) {
			DSDivision division = null;
			try {
				division = DSDivision.findByCode(divisionCode);
			} catch (Exception e) {
				logger.debug(e.getMessage(), e);
				division = null;
			}
			return division;
		}
		
	}
	
}

