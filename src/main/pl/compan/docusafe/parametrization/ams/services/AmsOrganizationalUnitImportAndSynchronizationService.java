/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.services;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Constants;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.GetOrganizationalUnit;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.GetOrganizationalUnitResponse;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.OrganizationalUnit;

import com.google.common.collect.Lists;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class AmsOrganizationalUnitImportAndSynchronizationService extends
		ServiceDriver implements Service {

	private static final Logger logger = LoggerFactory.getLogger(AmsOrganizationalUnitImportAndSynchronizationService.class);
	private static final StringManager sm = StringManager.getManager(Constants.Package);
	
	public static final Integer PAGE_SIZE = 90;
	
	private Timer timer;
	
	protected void start() throws ServiceException {
		logger.info("AmsOrganizationalUnitImportAndSynchronizationService");
		if (timer != null) {
			timer.cancel();
		}
		timer = new Timer(true);
		timer.schedule(new Import(), 0, DateUtils.DAY);
		console(Console.INFO, sm.getString("OrganizationalUnitImportAndSynchronizationServiceStart"));
	}

	protected void stop() throws ServiceException {
		if (timer != null) {
			timer.cancel();
		}
		console(Console.INFO, sm.getString("OrganizationalUnitImportAndSynchronizationServiceStop"));
	}

	protected boolean canStop() {
		return true;
	}
	
class Import extends TimerTask {
		
		private static final String SERVICE_PATH = "/services/DictionaryService";
		
		DictionaryServiceStub stub;
		OrganizationalUnit[] items;
		
		public void run() {
			console(Console.INFO, sm.getString("OrganizationalUnitImportAndSynchronizationServiceStartSynchronization"));
			List<String> divisionIdsFromResponse = Lists.newArrayList();
			try {
				DSApi.openAdmin();
				initConfiguration();
				if (stub != null) {
					GetOrganizationalUnit params = new GetOrganizationalUnit();
					boolean itemsWereEmpty = false;
					int pageIndex = 1;
					while (!itemsWereEmpty) {
						params.setPage(pageIndex);
						params.setPageSize(PAGE_SIZE);
						GetOrganizationalUnitResponse response = stub.getOrganizationalUnit(params);
						if (params != null && response != null) {
							items = response.get_return();
							if (items != null) {
								for (OrganizationalUnit item : items) {
									divisionIdsFromResponse.add(String.valueOf(item.getId()));
									updateOrCreateNewDivision(item);
								}
							} else {
								itemsWereEmpty = true;
							}
							pageIndex++;
						}
					}
					
					deleteNotExistingInResponseDivisions(divisionIdsFromResponse);
					console(Console.INFO, sm.getString("OrganizationalUnitImportAndSynchronizationServiceStopSynchronization"));
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			try {
				DSApi.close();
			} catch (EdmException e) {
				logger.error(e.getMessage(), e);
			}
		}
		
		private void initConfiguration() throws Exception {
            AxisClientConfigurator conf = new AxisClientConfigurator();
            stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
            conf.setUpHttpParameters(stub, SERVICE_PATH);
        }
		
		private void updateOrCreateNewDivision(OrganizationalUnit item) {
			DSDivision division = null;
			try {
				division = DSDivision.findByCode(String.valueOf(item.getIdm()));
				updateDivision(item, division);
			} catch (DivisionNotFoundException e) {
				createNewDivision(item);
			} catch (EdmException e) {
				logger.error(e.getMessage(), e);
			}
		}
		
		/**
		 * Metoda tworzy nowy dzial na podstawie dzialu przeslanego przez webservice - parametr item.
		 * @param item - dzial przeslany przez webservice
		 */
		private void createNewDivision(OrganizationalUnit item) {
			try {
				DSApi.context().begin();
				DSDivision rootDivision = DSDivision.find(DSDivision.ROOT_GUID);
				DSDivision newDivision = rootDivision.createDivisionWithExternal(item.getNazwa(), item.getIdm(), String.valueOf(item.getId()));
				DSDivision parentDivision;
				if (item.getKomorka_nad_idn() != null) {
					parentDivision = DSDivision.findByCode(item.getKomorka_nad_idn());
				} else {
					parentDivision = DSDivision.find(DSDivision.ROOT_GUID);
				}
				newDivision.setParent(parentDivision);
				newDivision.update();
				DSApi.context().commit();
				console(Console.INFO, sm.getString("OrganizationalUnitImportAndSynchronizationServiceCreatedNewDivision", newDivision.getName()));
			} catch (EdmException e) {
				console(Console.ERROR, sm.getString("OrganizationalUnitImportAndSynchronizationServiceErrorCreatedNewDivision", item.getNazwa()));
				logger.error(e.getMessage(), e);
				try {
					DSApi.context().rollback();
				} catch (EdmException exp) {
					logger.error(exp.getMessage(), exp);
				}
			}
		}
		
		/**
		 * Metoda sprawdza czy potrzebna jest aktualizacja dzialu division, jezeli tak to aktualizuje go.
		 * @param item - dzial przeslany przez webservice (aktualny)
		 * @param division - dzial znajdujacy sie w bazie danych
		 */
		private void updateDivision(OrganizationalUnit item, DSDivision division) {
			DSDivision parentDivision;
			boolean divisionHasChanged = false;
			try {
				//jezeli dzial byl wczesniej usuniety to go przywraca
				if (division.isHidden()) {
					division.setHidden(false);
					divisionHasChanged = true;
				}
				if (!division.getCode().equals(item.getIdm())) {
					division.setCode(item.getIdm());
					divisionHasChanged = true;
				}
				if (!division.getName().equals(item.getNazwa())) {
					division.setName(item.getNazwa());
					divisionHasChanged = true;
				}
				if (StringUtils.isNotBlank(item.getKomorka_nad_idn())) {
					parentDivision = DSDivision.findByCode(item.getKomorka_nad_idn());
				} else {
					parentDivision = DSDivision.find(DSDivision.ROOT_GUID);
				}
				if (division.getParent() != null && parentDivision != null) {
					if (parentDivision.isRoot()) {
						if (!division.getParent().isRoot()) {
							division.setParent(parentDivision);
							divisionHasChanged = true;
						}
					} else {
						if (!parentDivision.getCode().equals(division.getParent().getCode())) {
							division.setParent(parentDivision);
							divisionHasChanged = true;
						}
					}
				}
			} catch (EdmException e) {
				divisionHasChanged = false;
				logger.error(e.getMessage(), e);
				console(Console.ERROR, sm.getString("OrganizationalUnitImportAndSynchronizationServiceErrorUpdatedDivision", item.getNazwa()));
			}
			
			if (divisionHasChanged) {
				try {
					DSApi.context().begin();
					division.update();
					DSApi.context().commit();
					console(Console.INFO, sm.getString("OrganizationalUnitImportAndSynchronizationServiceUpdatedDivision", division.getName()));
				} catch (EdmException e) {
					logger.error(e.getMessage(), e);
					console(Console.ERROR, sm.getString("OrganizationalUnitImportAndSynchronizationServiceErrorUpdatedDivision", item.getNazwa()));
					try {
						DSApi.context().rollback();
					} catch (EdmException exp) {
						logger.error(exp.getMessage(), exp);
					}
				}
			}
		}
		
		/**
		 * Metoda usuwa dzialy ktore nie sa aktualne. Czyli te ktore nie znajduja sie w danych przesylanych przez webservice i ktore posiadaja externalId.
		 * @param divisionIdsFromResponse - lista external ids'ow dzialow ktore zostaly przeslane przez webservice
		 */
		private void deleteNotExistingInResponseDivisions(List<String> divisionIdsFromResponse) {
			try {
				DSDivision[] divisions = DSDivision.getAllDivisions();
				for (DSDivision division : divisions) {
					if (StringUtils.isNotBlank(division.getExternalId()) && !divisionIdsFromResponse.contains(division.getExternalId()) && !division.isHidden()) {
						try {
							DSApi.context().begin();
							UserFactory.getInstance().deleteDivision(division.getGuid(), DSDivision.ROOT_GUID);
							DSApi.context().commit();
							console(Console.INFO, sm.getString("OrganizationalUnitImportAndSynchronizationServiceDeletedDivision", division.getName()));
						} catch (EdmException e) {
							logger.error(e.getMessage(), e);
							console(Console.ERROR,  sm.getString("OrganizationalUnitImportAndSynchronizationServiceErrorDeletedDivision", division.getName()));
							DSApi.context().rollback();
						}
					}
				}
			} catch (EdmException e) {
				logger.error(e.getMessage(), e);
			}
		}
		
	}
}
