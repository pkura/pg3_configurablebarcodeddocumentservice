/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.services;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.GetPatternsOfCostSharing;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.GetPatternsOfCostSharingResponse;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.PatternsOfCostSharing;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class PatternsOfCostSharingImport extends AbstractDictionaryImport {
	
	private static Logger logger = LoggerFactory.getLogger(PatternsOfCostSharingImport.class);

	private static final String SERVICE_PATH = "/services/DictionaryService";
	private static final String SERVICE_TABLE_NAME_SOURCES = "dsg_ams_patterns_of_cost_sharing";
	
	private DictionaryServiceStub stub;
    private StringBuilder message;
	private int importPageSize;
	private int importPageNumber;
	private Set<Long> itemIds;
	
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);
	}

	public boolean doImport() throws RemoteException, EdmException {
        message = new StringBuilder();
		if (stub != null) {
			pl.compan.docusafe.ws.ams.DictionaryServiceStub.GetPatternsOfCostSharing params = new GetPatternsOfCostSharing();
			message.append("rozmiar strony: " + importPageSize);
			message.append(", nr strony: " + importPageNumber);
			params.setPage(importPageNumber++);
			params.setPageSize(importPageSize);
			GetPatternsOfCostSharingResponse response = stub.getPatternsOfCostSharing(params);
			if (response != null) {
				PatternsOfCostSharing[] items = response.get_return();
				if (items != null && items.length > 0) {
					for (PatternsOfCostSharing item : items) {
						if (item != null) {
							Map<String, Object> fieldValues = new HashMap<String, Object>(1);
							fieldValues.put("erpId", item.getId());
							List<pl.compan.docusafe.parametrization.ams.hb.PatternsOfCostSharing> founds = DictionaryUtils.findByGivenFieldValues(pl.compan.docusafe.parametrization.ams.hb.PatternsOfCostSharing.class, fieldValues);
							
							if (!founds.isEmpty()) {
								for (pl.compan.docusafe.parametrization.ams.hb.PatternsOfCostSharing found : founds) {
									found.setAllFieldsFromServiceObject(item);
									found.save();
									itemIds.add(found.getId());
								}
							} else {
								pl.compan.docusafe.parametrization.ams.hb.PatternsOfCostSharing patterns = new pl.compan.docusafe.parametrization.ams.hb.PatternsOfCostSharing();
								patterns.setAllFieldsFromServiceObject(item);
								patterns.save();
								itemIds.add(patterns.getId());
							}
						}	
					}
					return false;
				}
			}
		}
		return true;
	}
	
	@Override
	public void initImport() {
		importPageSize = 1000;
		importPageNumber = 1;
		itemIds = new HashSet<Long>();
	}
	
    @Override
    public void finalizeImport() throws EdmException {
        int deleted = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME_SOURCES, false);
        message.append("Deleted: " + deleted);
    }

    @Override
    public String getMessage() {
        return message!=null?message.toString():null;
    }
}
