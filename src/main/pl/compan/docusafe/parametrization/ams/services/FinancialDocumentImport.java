/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.services;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.FinancialDocument;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.GetFinancialDocument;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.GetFinancialDocumentResponse;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class FinancialDocumentImport extends AbstractDictionaryImport {
	
	private static Logger logger = LoggerFactory.getLogger(BudgetViewImport.class);
	
	private static final String SERVICE_PATH = "/services/DictionaryService";
	private static final String SERVICE_TABLE_NAME_SOURCES = "dsg_ams_financial_document";
	
	private DictionaryServiceStub stub;
	private int importPageSize;
	private int importPageNumber;
	private Set<Long> itemIds;
	
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);
	}

	public boolean doImport() throws RemoteException, EdmException {
		if (stub != null) {
			GetFinancialDocument params = new GetFinancialDocument();
			boolean itemsWereEmpty = false;
			while (!itemsWereEmpty) {
				params.setPage(importPageNumber);
				params.setPageSize(importPageSize);
				GetFinancialDocumentResponse response = stub.getFinancialDocument(params);
				if (response != null) {
					FinancialDocument[] items = response.get_return();
					if (items != null && items.length > 0) {
						for (FinancialDocument item : items) {
							if (item != null) {
								Map<String, Object> fieldValues = new HashMap<String, Object>(1);
								fieldValues.put("erpId", item.getId());
								List<pl.compan.docusafe.parametrization.ams.hb.FinancialDocument> founds = DictionaryUtils.findByGivenFieldValues(pl.compan.docusafe.parametrization.ams.hb.FinancialDocument.class, fieldValues);
								
								if (!founds.isEmpty()) {
									for (pl.compan.docusafe.parametrization.ams.hb.FinancialDocument found : founds) {
										found.setAllFieldsFromServiceObject(item);
										found.save();
										itemIds.add(found.getId());
									}
								} else {
									pl.compan.docusafe.parametrization.ams.hb.FinancialDocument financialDocument = new pl.compan.docusafe.parametrization.ams.hb.FinancialDocument();
									financialDocument.setAllFieldsFromServiceObject(item);
									financialDocument.save();
									itemIds.add(financialDocument.getId());
								}
								
							}
						}
					} else {
						itemsWereEmpty = true;
					}
					importPageNumber++;
				}
			}
		}
		return true;
	}
	
	@Override
	public void initImport() {
		importPageSize = 1000;
		importPageNumber = 1;
		itemIds = new HashSet<Long>();
	}
	
    @Override
    public void finalizeImport() throws EdmException {
        //int deleted = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME_SOURCES, true);
    }

}
