/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.services;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.parametrization.ul.hib.DictionaryUtils;
import pl.compan.docusafe.service.dictionaries.AbstractDictionaryImport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.GetWarehouse;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.GetWarehouseResponse;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub.Warehouse;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class WarehouseImport extends AbstractDictionaryImport {
	
private static Logger logger = LoggerFactory.getLogger(WarehouseImport.class);
	
	private static final String SERVICE_PATH = "/services/DictionaryService";
	private static final String SERVICE_TABLE_NAME_SOURCES = "dsg_ams_warehouse";
	
	private DictionaryServiceStub stub;
	private int importPageSize;
	private int importPageNumber;
	private Set<Long> itemIds;
	
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
		stub = new DictionaryServiceStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(stub, SERVICE_PATH);
	}

	public boolean doImport() throws RemoteException, EdmException {
		if (stub != null) {
			GetWarehouse params = new GetWarehouse();
			params.setPage(importPageNumber);
			params.setPageSize(importPageSize);
			GetWarehouseResponse response = stub.getWarehouse(params);
			if (response != null) {
				Warehouse[] items = response.get_return();
				if (items != null && items.length > 0) {
					for (Warehouse item : items) {
						if (item != null) {
							Map<String, Object> fieldValues = new HashMap<String, Object>(1);
							fieldValues.put("idm", item.getIdm());
							List<pl.compan.docusafe.parametrization.ams.hb.Warehouse> founds = DictionaryUtils.findByGivenFieldValues(pl.compan.docusafe.parametrization.ams.hb.Warehouse.class, fieldValues);
							
							if (!founds.isEmpty()) {
								for (pl.compan.docusafe.parametrization.ams.hb.Warehouse found : founds) {
									found.setAllFieldsFromServiceObject(item);
									found.save();
									itemIds.add(found.getId());
								}
							} else {
								pl.compan.docusafe.parametrization.ams.hb.Warehouse warehouse = new pl.compan.docusafe.parametrization.ams.hb.Warehouse();
								warehouse.setAllFieldsFromServiceObject(item);
								warehouse.save();
								itemIds.add(warehouse.getId());
							}
							
						}
					}
				} else {
					return true;
				}
			} else {
				return true;
			}
		}
		return true;
	}
	
	@Override
	public void initImport() {
		importPageSize = 1000;
		importPageNumber = 1;
		itemIds = new HashSet<Long>();
	}
	
    @Override
    public void finalizeImport() throws EdmException {
        //int deleted = ServicesUtils.disableDeprecatedItem(itemIds, SERVICE_TABLE_NAME_SOURCES, true);
    }

}
