/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.news.Notification;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 25-03-2014
 * ds_trunk_2014
 * AmsAssetRecordSystemForOperationType.java
 */
@Entity
@Table(name = AmsAssetRecordSystemTypeForOperationType.TABLE_NAME)
public class AmsAssetRecordSystemTypeForOperationType implements java.io.Serializable {
    public static final String TABLE_NAME = "dsg_ams_asset_rec_sys_op_type";
    public static final String VIEW_NAME = "dsg_ams_asset_rec_sys_op_type_view";
    private static AmsAssetRecordSystemTypeForOperationType instance;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "dsg_ams_asset_rec_sys_op_type_seq")
    @SequenceGenerator(name = "dsg_ams_asset_rec_sys_op_type_seq", sequenceName = "dsg_ams_asset_rec_sys_op_type_seq")
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name="ERP_ID")
    private Double erpId;
    @Column(name="TYP_OPE_ID")
    private Double typOpeId;
    @Column(name="TYP_OPE_IDN", length = 50)
    private String typOpeIdn;
    @Column(name="TYP_SYSEWI_ID")
    private Double typSysewiId;
    @Column(name="TYP_SYSEWI_IDN", length = 50)
    private String typSysewiIdn;

    public static AmsAssetRecordSystemTypeForOperationType find(Integer id) throws EdmException {
        return Finder.find(AmsAssetRecordSystemTypeForOperationType.class, id);
    }
    
    public static AmsAssetRecordSystemTypeForOperationType findBytypOpeIdn(String typOpeIdn) throws EdmException {
    	Criteria crit = DSApi.context().session().createCriteria(AmsAssetRecordSystemTypeForOperationType.class);
    	crit.add(Restrictions.eq("typOpeIdn", typOpeIdn));
    	crit.uniqueResult();
    	return (AmsAssetRecordSystemTypeForOperationType)crit.uniqueResult();
    }

    public AmsAssetRecordSystemTypeForOperationType() {
    }

    public void setAllFieldsFromServiceObject(DictionaryServiceStub.AssetRecordSystemTypesForOperationType item) {

        erpId = item.getId();
        typOpeId = item.getTyp_ope_id();
        typOpeIdn = item.getTyp_ope_idn();
        typSysewiId = item.getTyp_sysewi_id();
        typSysewiIdn = item.getTyp_sysewi_idn();
    }

    public void save() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public Double getErpId() {
        return erpId;
    }

    public void setErpId(Double erpId) {
        this.erpId = erpId;
    }

    public Long getId() {
        return id;
    }
    
    public Double getTypOpeId() {
		return typOpeId;
	}

	public void setTypOpeId(Double typOpeId) {
		this.typOpeId = typOpeId;
	}

	public String getTypOpeIdn() {
		return typOpeIdn;
	}

	public void setTypOpeIdn(String typOpeIdn) {
		this.typOpeIdn = typOpeIdn;
	}

	public Double getTypSysewiId() {
		return typSysewiId;
	}

	public void setTypSysewiId(Double typSysewiId) {
		this.typSysewiId = typSysewiId;
	}

	public String getTypSysewiIdn() {
		return typSysewiIdn;
	}

	public void setTypSysewiIdn(String typSysewiIdn) {
		this.typSysewiIdn = typSysewiIdn;
	}

    
}



