/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 25-03-2014
 * ds_trunk_2014
 * AmsAssetLiquidationReason.java
 */
@Entity
@Table(name = AmsAssetLiquidationReason.TABLE_NAME)
public class AmsAssetLiquidationReason implements java.io.Serializable {
    public static final String TABLE_NAME = "dsg_ams_asset_liquidation";
    public static final String VIEW_NAME = "dsg_ams_asset_liquidation_view";
    private static AmsAssetLiquidationReason instance;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "dsg_ams_asset_liquidation_seq")
    @SequenceGenerator(name = "dsg_ams_asset_liquidation_seq", sequenceName = "dsg_ams_asset_liquidation_seq")
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name="ERP_ID")
    private Double erpId;
    @Column(name="IDM", length = 50)
    private String idm;
    @Column(name="CZY_SPRZEDAZ")
    private Double czySprzedaz;
	@Column(name="LIKWIDACJA_NAZWA", length = 100)
    private String likwidacjaNazwa;

    public static AmsAssetLiquidationReason find(Integer id) throws EdmException {
        return Finder.find(AmsAssetLiquidationReason.class, id);
    }

    public AmsAssetLiquidationReason() {
    }

    public void setAllFieldsFromServiceObject(DictionaryServiceStub.AssetLiquidationReason item) {

        erpId = item.getId();
        idm = item.getIdm();
        czySprzedaz = item.getCzy_sprzedaz();
        likwidacjaNazwa = item.getLikwidacja_nazwa();
    }

    public void save() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public Double getErpId() {
        return erpId;
    }

    public String getIdm() {
        return idm;
    }

    public void setErpId(Double erpId) {
        this.erpId = erpId;
    }

    public void setIdm(String idm) {
        this.idm = idm;
    }

    public Long getId() {
        return id;
    }
    
    public Double getCzySprzedaz() {
		return czySprzedaz;
	}

	public void setCzySprzedaz(Double czySprzedaz) {
		this.czySprzedaz = czySprzedaz;
	}

	public String getLikwidacjaNazwa() {
		return likwidacjaNazwa;
	}

	public void setLikwidacjaNazwa(String likwidacjaNazwa) {
		this.likwidacjaNazwa = likwidacjaNazwa;
	}

}



