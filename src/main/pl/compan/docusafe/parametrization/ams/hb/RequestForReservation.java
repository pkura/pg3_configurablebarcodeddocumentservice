/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
@Entity
@Table(name = RequestForReservation.TABLE_NAME)
public class RequestForReservation implements java.io.Serializable {

	public static final String TABLE_NAME = "dsg_ams_request_for_reservation";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "bd_budzet_ko_id")
	private Double bdBudzetKoId;
	
	@Column(name = "bd_budzet_ko_idm", length = 100)
	private String bdBudzetKoIdm;
	
	@Column(name = "bd_plan_zam_id")
	private Double bdPlanZamId;
	
	@Column(name = "bd_plam_zam_idm", length = 100)
	private String bdPlanZamIdm;
	
	@Column(name = "bd_rezerwacja_id")
	private Double bdRezerwacjaId;
	
	@Column(name = "bd_rezerwacja_idm", length = 100)
	private String bdRezerwacjaIdm;
	
	@Column(name = "bd_rezerwacja_poz_id")
	private Double bdRezerwacjaPozId;
	
	@Column(name = "bd_rodzaj_ko_id")
	private Double bdRodzajKoId;
	
	@Column(name = "bd_rodzaj_plan_zam_id")
	private Double bdRodzajPlanZamId;
	
	@Column(name = "bd_str_budzet_id")
	private Double bdStrBudzetId;
	
	@Column(name = "bd_typ_rezerwacja_idn", length = 100)
	private String bdTypRezerwacjaIdn;
	
	@Column(name = "bk_bd_okr_szablon_rel_id")
	private Double bkBdOkrSzablonRelId;
	
	@Column(name = "bk_bd_szablon_poz_id")
	private Double bkBdSzablonPozId;
	
	/* na tescie tego nie ma
	@Column(name = "bk_czy_archiwalny")
	private Double bkCzyArchiwalny;*/
	
	@Column(name = "bk_nazwa", length = 150)
	private String bkNazwa;
	
	@Column(name = "bk_poz_nazwa", length = 150)
	private String bkPozNazwa;
	
	@Column(name = "bk_poz_nrpoz")
	private Integer bkPozNrpoz;
	
	@Column(name = "bk_poz_p_koszt")
	private Double bkPozPKoszt;
	
	@Column(name = "bk_poz_p_koszt_zrodla")
	private Double bkPozPKosztZrodla;
	
	@Column(name = "bk_poz_r_koszt")
	private Double bkPozRKoszt;
	
	@Column(name = "bk_poz_r_koszt_zrodla")
	private Double bkPozRKosztZrodla;
	
	@Column(name = "bk_poz_rez_koszt")
	private Double bkPozRezKoszt;
	
	@Column(name = "bk_poz_rez_koszt_zrodla")
	private Double bkPozRezKosztZrodla;
	
	/* na tescie tego nie ma
	@Column(name = "bk_status")
	private Integer bkStatus;*/
	
	@Column(name = "bk_wytwor_id")
	private Double bkWytworId;
	
	@Column(name = "budzet_id")
	private Double budzetId;
	
	@Column(name = "budzet_idm", length = 100)
	private String budzetIdm;
	
	@Column(name = "cena")
	private Double cena;
	
	@Column(name = "czy_obsluga_pzp")
	private Double czyObslugaPzp;
	
	@Column(name = "ilosc")
	private Double ilosc;
	
	@Column(name = "koszt")
	private Double koszt;
	
	@Column(name = "magazyn_id")
	private Double magazynId;
	
	@Column(name = "nazwa", length = 150)
	private String nazwa;
	
	@Column(name = "nrpoz")
	private Integer nrpoz;
	
	@Column(name = "okrrozl_id")
	private Double okrrozlId;
	
	@Column(name = "pzp_bd_okr_szablon_rel_id")
	private Double pzpBdOkrSzablonRelId;
	
	@Column(name = "pzp_bd_szablon_poz_id")
	private Double pzpBdSzablonPozId;
	
	/* na tescie tego nie ma
	@Column(name = "pzp_czy_archiwalny")
	private Double pzpCzyArchiwalny;*/
	
	@Column(name = "pzp_jm_id")
	private Double pzpJmId;
	
	@Column(name = "pzp_jm_idn", length = 100)
	private String pzpJmIdn;
	
	@Column(name = "pzp_nazwa", length = 150)
	private String pzpNazwa;
	
	@Column(name = "pzp_poz_nazwa", length = 150)
	private String pzpPozNazwa;
	
	@Column(name = "pzp_poz_nrpoz")
	private Integer pzpPozNrpoz;
	
	@Column(name = "pzp_poz_p_ilosc")
	private Double pzpPozPIlosc;
	
	@Column(name = "pzp_poz_p_koszt")
	private Double pzpPozPKoszt;
	
	@Column(name = "pzp_poz_r_ilosc")
	private Double pzpPozRIlosc;
	
	@Column(name = "pzp_poz_r_koszt")
	private Double pzpPozRKoszt;
	
	@Column(name = "pzp_poz_rez_ilosc")
	private Double pzpPozRezIlosc;
	
	@Column(name = "pzp_poz_rez_koszt")
	private Double pzpPozRezKoszt;
	
	/* na tescie tego nie ma
	@Column(name = "pzp_status")
	private Integer pzpStatus;*/
	
	@Column(name = "pzp_wytwor_id")
	private Double pzpWytworId;
	
	/* nie ma tego na produkcji */
	@Column(name = "pzp_wytwor_idm", length = 100)
	private String pzpWytworIdm;
	
	@Column(name = "rez_jm_id")
	private Double rezJmId;
	
	@Column(name = "rez_jm_idn", length = 100)
	private String rezJmIdn;
	
	@Column(name = "rezerwacja_czy_archiwalny")
	private Double rezerwacjaCzyArchiwalny;
	
	@Column(name = "rezerwacja_status")
	private Integer rezerwacjaStatus;
	
	@Column(name = "vatstaw_id")
	private Double vatstawId;
	
	@Column(name = "wytwor_edit_idm", length = 100)
	private String wytworEditIdm;
	
	@Column(name = "wytwor_edit_nazwa", length = 150)
	private String wytworEditNazwa;
	
	@Column(name = "wytwor_id")
	private Double wytworId;
	
	/* nie ma tego na produkcji */
	@Column(name = "zadanie_id")
	private Double zadanieId;
	
	/* nie ma tego na produkcji */
	@Column(name = "zadanie_idn", length = 100)
	private String zadanieIdn;
	
	/* nie ma tego na produkcji */
	@Column(name = "zadanie_nazwa", length = 150)
	private String zadanieNazwa;
	
	@Column(name = "zrodlo_id")
	private Double zrodloId;
	
	/* nie ma tego na produkcji */
	@Column(name = "zrodlo_idn", length = 100)
	private String zrodloIdn;
	
	/* nie ma tego na produkcji */
	@Column(name = "zrodlo_nazwa", length = 150)
	private String zrodloNazwa;
	
	public RequestForReservation() {
		
	}
	
	public RequestForReservation(Double bdBudzetKoId, String bdBudzetKoIdm,
			Double bdPlanZamId, String bdPlanZamIdm, Double bdRezerwacjaId,
			String bdRezerwacjaIdm, Double bdRezerwacjaPozId,
			Double bdRodzajKoId, Double bdRodzajPlanZamId,
			Double bdStrBudzetId, String bdTypRezerwacjaIdn,
			Double bkBdOkrSzablonRelId, Double bkBdSzablonPozId,
			String bkNazwa, String bkPozNazwa, Integer bkPozNrpoz,
			Double bkPozPKoszt, Double bkPozPKosztZrodla, Double bkPozRKoszt,
			Double bkPozRKosztZrodla, Double bkPozRezKoszt,
			Double bkPozRezKosztZrodla, Double bkWytworId, Double budzetId,
			String budzetIdm, Double cena, Double czyObslugaPzp, Double ilosc,
			Double koszt, Double magazynId, String nazwa, Integer nrpoz,
			Double okrrozlId, Double pzpBdOkrSzablonRelId,
			Double pzpBdSzablonPozId, Double pzpJmId, String pzpJmIdn,
			String pzpNazwa, String pzpPozNazwa, Integer pzpPozNrpoz,
			Double pzpPozPIlosc, Double pzpPozPKoszt, Double pzpPozRIlosc,
			Double pzpPozRKoszt, Double pzpPozRezIlosc, Double pzpPozRezKoszt,
			Double pzpWytworId, String pzpWytworIdm, Double rezJmId,
			String rezJmIdn, Double rezerwacjaCzyArchiwalny,
			Integer rezerwacjaStatus, Double vatstawId, String wytworEditIdm,
			String wytworEditNazwa, Double wytworId, Double zadanieId,
			String zadanieIdn, String zadanieNazwa, Double zrodloId,
			String zrodloIdn, String zrodloNazwa) {
		super();
		this.bdBudzetKoId = bdBudzetKoId;
		this.bdBudzetKoIdm = bdBudzetKoIdm;
		this.bdPlanZamId = bdPlanZamId;
		this.bdPlanZamIdm = bdPlanZamIdm;
		this.bdRezerwacjaId = bdRezerwacjaId;
		this.bdRezerwacjaIdm = bdRezerwacjaIdm;
		this.bdRezerwacjaPozId = bdRezerwacjaPozId;
		this.bdRodzajKoId = bdRodzajKoId;
		this.bdRodzajPlanZamId = bdRodzajPlanZamId;
		this.bdStrBudzetId = bdStrBudzetId;
		this.bdTypRezerwacjaIdn = bdTypRezerwacjaIdn;
		this.bkBdOkrSzablonRelId = bkBdOkrSzablonRelId;
		this.bkBdSzablonPozId = bkBdSzablonPozId;
		this.bkNazwa = bkNazwa;
		this.bkPozNazwa = bkPozNazwa;
		this.bkPozNrpoz = bkPozNrpoz;
		this.bkPozPKoszt = bkPozPKoszt;
		this.bkPozPKosztZrodla = bkPozPKosztZrodla;
		this.bkPozRKoszt = bkPozRKoszt;
		this.bkPozRKosztZrodla = bkPozRKosztZrodla;
		this.bkPozRezKoszt = bkPozRezKoszt;
		this.bkPozRezKosztZrodla = bkPozRezKosztZrodla;
		this.bkWytworId = bkWytworId;
		this.budzetId = budzetId;
		this.budzetIdm = budzetIdm;
		this.cena = cena;
		this.czyObslugaPzp = czyObslugaPzp;
		this.ilosc = ilosc;
		this.koszt = koszt;
		this.magazynId = magazynId;
		this.nazwa = nazwa;
		this.nrpoz = nrpoz;
		this.okrrozlId = okrrozlId;
		this.pzpBdOkrSzablonRelId = pzpBdOkrSzablonRelId;
		this.pzpBdSzablonPozId = pzpBdSzablonPozId;
		this.pzpJmId = pzpJmId;
		this.pzpJmIdn = pzpJmIdn;
		this.pzpNazwa = pzpNazwa;
		this.pzpPozNazwa = pzpPozNazwa;
		this.pzpPozNrpoz = pzpPozNrpoz;
		this.pzpPozPIlosc = pzpPozPIlosc;
		this.pzpPozPKoszt = pzpPozPKoszt;
		this.pzpPozRIlosc = pzpPozRIlosc;
		this.pzpPozRKoszt = pzpPozRKoszt;
		this.pzpPozRezIlosc = pzpPozRezIlosc;
		this.pzpPozRezKoszt = pzpPozRezKoszt;
		this.pzpWytworId = pzpWytworId;
		this.pzpWytworIdm = pzpWytworIdm;
		this.rezJmId = rezJmId;
		this.rezJmIdn = rezJmIdn;
		this.rezerwacjaCzyArchiwalny = rezerwacjaCzyArchiwalny;
		this.rezerwacjaStatus = rezerwacjaStatus;
		this.vatstawId = vatstawId;
		this.wytworEditIdm = wytworEditIdm;
		this.wytworEditNazwa = wytworEditNazwa;
		this.wytworId = wytworId;
		this.zadanieId = zadanieId;
		this.zadanieIdn = zadanieIdn;
		this.zadanieNazwa = zadanieNazwa;
		this.zrodloId = zrodloId;
		this.zrodloIdn = zrodloIdn;
		this.zrodloNazwa = zrodloNazwa;
	}

	public void setAllFieldsFromServiceObject(pl.compan.docusafe.ws.ams.DictionaryServiceStub.RequestForReservation item) {
    	setBdBudzetKoId(item.getBd_budzet_ko_id());
    	setBdBudzetKoIdm(item.getBd_budzet_ko_idm());
    	setBdPlanZamId(item.getBd_plan_zam_id());
    	setBdPlanZamIdm(item.getBd_plan_zam_idm());
    	setBdRezerwacjaId(item.getBd_rezerwacja_id());
    	setBdRezerwacjaIdm(item.getBd_rezerwacja_idm());
    	setBdRezerwacjaPozId(item.getBd_rezerwacja_poz_id());
    	setBdRodzajKoId(item.getBd_rodzaj_ko_id());
    	setBdRodzajPlanZamId(item.getBd_rodzaj_plan_zam_id());
    	setBdStrBudzetId(item.getBd_str_budzet_id());
    	setBdTypRezerwacjaIdn(item.getBd_typ_rezerwacja_idn());
    	setBkBdOkrSzablonRelId(item.getBk_bd_okr_szablon_rel_id());
    	setBkBdSzablonPozId(item.getBk_bd_szablon_poz_id());
    	/* nie ma tego na tescie
    	setBkCzyArchiwalny(item.getBk_czy_archiwalny());*/
    	setBkNazwa(item.getBk_nazwa());
    	setBkPozNazwa(item.getBk_poz_nazwa());
    	setBkPozNrpoz(item.getBk_poz_nrpoz());
    	setBkPozPKoszt(item.getBk_poz_p_koszt());
    	setBkPozPKosztZrodla(item.getBk_poz_p_koszt_zrodla());
    	setBkPozRKoszt(item.getBk_poz_r_koszt());
    	setBkPozRKosztZrodla(item.getBk_poz_r_koszt_zrodla());
    	setBkPozRezKoszt(item.getBk_poz_rez_koszt());
    	setBkPozRezKosztZrodla(item.getBk_poz_rez_koszt_zrodla());
    	/* nie ma tego na tescie
    	setBkStatus(item.getBk_status());*/
    	setBkWytworId(item.getBk_wytwor_id());
    	setBudzetId(item.getBudzet_id());
    	setBudzetIdm(item.getBudzet_idm());
    	setCena(item.getCena());
    	setCzyObslugaPzp(item.getCzy_obsluga_pzp());
    	setIlosc(item.getIlosc());
    	setKoszt(item.getKoszt());
    	setMagazynId(item.getMagazyn_id());
    	setNazwa(item.getNazwa());
    	setNrpoz(item.getNrpoz());
    	setOkrrozlId(item.getOkrrozl_id());
    	setPzpBdOkrSzablonRelId(item.getPzp_bd_okr_szablon_rel_id());
    	setPzpBdSzablonPozId(item.getPzp_bd_szablon_poz_id());
    	/* nie ma tego na tescie
    	setPzpCzyArchiwalny(item.getPzp_czy_archiwalny());*/
    	setPzpJmId(item.getPzp_jm_id());
    	setPzpJmIdn(item.getPzp_jm_idn());
    	setPzpNazwa(item.getPzp_nazwa());
    	setPzpPozNazwa(item.getPzp_poz_nazwa());
    	setPzpPozNrpoz(item.getPzp_poz_nrpoz());
    	setPzpPozPIlosc(item.getPzp_poz_p_ilosc());
    	setPzpPozPKoszt(item.getPzp_poz_p_koszt());
    	setPzpPozRIlosc(item.getPzp_poz_r_ilosc());
    	setPzpPozRKoszt(item.getPzp_poz_r_koszt());
    	setPzpPozRezIlosc(item.getPzp_poz_rez_ilosc());
    	setPzpPozRezKoszt(item.getPzp_poz_rez_koszt());
    	/* nie ma tego na tescie
    	setPzpStatus(item.getPzp_status());*/
    	setPzpWytworId(item.getPzp_wytwor_id());
    	setPzpWytworIdm(item.getPzp_wytwor_idm());		//nie ma tego na produkcji
    	setRezJmId(item.getRez_jm_id());
    	setRezJmIdn(item.getRez_jm_idn());
    	setRezerwacjaCzyArchiwalny(item.getRezerwacja_czy_archiwalny());
    	setRezerwacjaStatus(item.getRezerwacja_status());
    	setVatstawId(item.getVatstaw_id());
    	setWytworEditIdm(item.getWytwor_edit_idm());
    	setWytworEditNazwa(item.getWytwor_edit_nazwa());
    	setWytworId(item.getWytwor_id());
    	setZadanieId(item.getZadanie_id());			//nie ma tego na produkcji
    	setZadanieIdn(item.getZadanie_idn());		//nie ma tego na produkcji
    	setZadanieNazwa(item.getZadanie_nazwa());	//nie ma tego na produkcji
    	setZrodloId(item.getZrodlo_id());
    	setZrodloIdn(item.getZrodlo_idn());			//nie ma tego na produkcji
    	setZrodloNazwa(item.getZrodlo_nazwa());		//nie ma tego na produkcji
    }
    
    public void save() throws EdmException {
    	try {
    		DSApi.context().session().save(this);
    	} catch (HibernateException e) {
    		new EdmException(e);
    	}
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getBdBudzetKoId() {
		return bdBudzetKoId;
	}

	public void setBdBudzetKoId(Double bdBudzetKoId) {
		this.bdBudzetKoId = bdBudzetKoId;
	}

	public String getBdBudzetKoIdm() {
		return bdBudzetKoIdm;
	}

	public void setBdBudzetKoIdm(String bdBudzetKoIdm) {
		this.bdBudzetKoIdm = bdBudzetKoIdm;
	}

	public Double getBdPlanZamId() {
		return bdPlanZamId;
	}

	public void setBdPlanZamId(Double bdPlanZamId) {
		this.bdPlanZamId = bdPlanZamId;
	}

	public String getBdPlanZamIdm() {
		return bdPlanZamIdm;
	}

	public void setBdPlanZamIdm(String bdPlanZamIdm) {
		this.bdPlanZamIdm = bdPlanZamIdm;
	}

	public Double getBdRezerwacjaId() {
		return bdRezerwacjaId;
	}

	public void setBdRezerwacjaId(Double bdRezerwacjaId) {
		this.bdRezerwacjaId = bdRezerwacjaId;
	}

	public String getBdRezerwacjaIdm() {
		return bdRezerwacjaIdm;
	}

	public void setBdRezerwacjaIdm(String bdRezerwacjaIdm) {
		this.bdRezerwacjaIdm = bdRezerwacjaIdm;
	}

	public Double getBdRezerwacjaPozId() {
		return bdRezerwacjaPozId;
	}

	public void setBdRezerwacjaPozId(Double bdRezerwacjaPozId) {
		this.bdRezerwacjaPozId = bdRezerwacjaPozId;
	}

	public Double getBdRodzajKoId() {
		return bdRodzajKoId;
	}

	public void setBdRodzajKoId(Double bdRodzajKoId) {
		this.bdRodzajKoId = bdRodzajKoId;
	}

	public Double getBdRodzajPlanZamId() {
		return bdRodzajPlanZamId;
	}

	public void setBdRodzajPlanZamId(Double bdRodzajPlanZamId) {
		this.bdRodzajPlanZamId = bdRodzajPlanZamId;
	}

	public Double getBdStrBudzetId() {
		return bdStrBudzetId;
	}

	public void setBdStrBudzetId(Double bdStrBudzetId) {
		this.bdStrBudzetId = bdStrBudzetId;
	}

	public String getBdTypRezerwacjaIdn() {
		return bdTypRezerwacjaIdn;
	}

	public void setBdTypRezerwacjaIdn(String bdTypRezerwacjaIdn) {
		this.bdTypRezerwacjaIdn = bdTypRezerwacjaIdn;
	}

	public Double getBkBdOkrSzablonRelId() {
		return bkBdOkrSzablonRelId;
	}

	public void setBkBdOkrSzablonRelId(Double bkBdOkrSzablonRelId) {
		this.bkBdOkrSzablonRelId = bkBdOkrSzablonRelId;
	}

	public Double getBkBdSzablonPozId() {
		return bkBdSzablonPozId;
	}

	public void setBkBdSzablonPozId(Double bkBdSzablonPozId) {
		this.bkBdSzablonPozId = bkBdSzablonPozId;
	}
	
	/* nie ma tego na tescie
	public Double getBkCzyArchiwalny() {
		return bkCzyArchiwalny;
	}

	public void setBkCzyArchiwalny(Double bkCzyArchiwalny) {
		this.bkCzyArchiwalny = bkCzyArchiwalny;
	}*/

	public String getBkNazwa() {
		return bkNazwa;
	}

	public void setBkNazwa(String bkNazwa) {
		this.bkNazwa = bkNazwa;
	}

	public String getBkPozNazwa() {
		return bkPozNazwa;
	}

	public void setBkPozNazwa(String bkPozNazwa) {
		this.bkPozNazwa = bkPozNazwa;
	}

	public Integer getBkPozNrpoz() {
		return bkPozNrpoz;
	}

	public void setBkPozNrpoz(Integer bkPozNrpoz) {
		this.bkPozNrpoz = bkPozNrpoz;
	}

	public Double getBkPozPKoszt() {
		return bkPozPKoszt;
	}

	public void setBkPozPKoszt(Double bkPozPKoszt) {
		this.bkPozPKoszt = bkPozPKoszt;
	}

	public Double getBkPozPKosztZrodla() {
		return bkPozPKosztZrodla;
	}

	public void setBkPozPKosztZrodla(Double bkPozPKosztZrodla) {
		this.bkPozPKosztZrodla = bkPozPKosztZrodla;
	}

	public Double getBkPozRKoszt() {
		return bkPozRKoszt;
	}

	public void setBkPozRKoszt(Double bkPozRKoszt) {
		this.bkPozRKoszt = bkPozRKoszt;
	}

	public Double getBkPozRKosztZrodla() {
		return bkPozRKosztZrodla;
	}

	public void setBkPozRKosztZrodla(Double bkPozRKosztZrodla) {
		this.bkPozRKosztZrodla = bkPozRKosztZrodla;
	}

	public Double getBkPozRezKoszt() {
		return bkPozRezKoszt;
	}

	public void setBkPozRezKoszt(Double bkPozRezKoszt) {
		this.bkPozRezKoszt = bkPozRezKoszt;
	}

	public Double getBkPozRezKosztZrodla() {
		return bkPozRezKosztZrodla;
	}

	public void setBkPozRezKosztZrodla(Double bkPozRezKosztZrodla) {
		this.bkPozRezKosztZrodla = bkPozRezKosztZrodla;
	}
	
	/* nie ma tego na tescie
	public Integer getBkStatus() {
		return bkStatus;
	}

	public void setBkStatus(Integer bkStatus) {
		this.bkStatus = bkStatus;
	}*/

	public Double getBkWytworId() {
		return bkWytworId;
	}

	public void setBkWytworId(Double bkWytworId) {
		this.bkWytworId = bkWytworId;
	}

	public Double getBudzetId() {
		return budzetId;
	}

	public void setBudzetId(Double budzetId) {
		this.budzetId = budzetId;
	}

	public String getBudzetIdm() {
		return budzetIdm;
	}

	public void setBudzetIdm(String budzetIdm) {
		this.budzetIdm = budzetIdm;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public Double getCzyObslugaPzp() {
		return czyObslugaPzp;
	}

	public void setCzyObslugaPzp(Double czyObslugaPzp) {
		this.czyObslugaPzp = czyObslugaPzp;
	}

	public Double getIlosc() {
		return ilosc;
	}

	public void setIlosc(Double ilosc) {
		this.ilosc = ilosc;
	}

	public Double getKoszt() {
		return koszt;
	}

	public void setKoszt(Double koszt) {
		this.koszt = koszt;
	}

	public Double getMagazynId() {
		return magazynId;
	}

	public void setMagazynId(Double magazynId) {
		this.magazynId = magazynId;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Integer getNrpoz() {
		return nrpoz;
	}

	public void setNrpoz(Integer nrpoz) {
		this.nrpoz = nrpoz;
	}

	public Double getOkrrozlId() {
		return okrrozlId;
	}

	public void setOkrrozlId(Double okrrozlId) {
		this.okrrozlId = okrrozlId;
	}

	public Double getPzpBdOkrSzablonRelId() {
		return pzpBdOkrSzablonRelId;
	}

	public void setPzpBdOkrSzablonRelId(Double pzpBdOkrSzablonRelId) {
		this.pzpBdOkrSzablonRelId = pzpBdOkrSzablonRelId;
	}

	public Double getPzpBdSzablonPozId() {
		return pzpBdSzablonPozId;
	}

	public void setPzpBdSzablonPozId(Double pzpBdSzablonPozId) {
		this.pzpBdSzablonPozId = pzpBdSzablonPozId;
	}
	
	/* nie ma tego na tescie
	public Double getPzpCzyArchiwalny() {
		return pzpCzyArchiwalny;
	}

	public void setPzpCzyArchiwalny(Double pzpCzyArchiwalny) {
		this.pzpCzyArchiwalny = pzpCzyArchiwalny;
	}*/

	public Double getPzpJmId() {
		return pzpJmId;
	}

	public void setPzpJmId(Double pzpJmId) {
		this.pzpJmId = pzpJmId;
	}

	public String getPzpJmIdn() {
		return pzpJmIdn;
	}

	public void setPzpJmIdn(String pzpJmIdn) {
		this.pzpJmIdn = pzpJmIdn;
	}

	public String getPzpNazwa() {
		return pzpNazwa;
	}

	public void setPzpNazwa(String pzpNazwa) {
		this.pzpNazwa = pzpNazwa;
	}

	public String getPzpPozNazwa() {
		return pzpPozNazwa;
	}

	public void setPzpPozNazwa(String pzpPozNazwa) {
		this.pzpPozNazwa = pzpPozNazwa;
	}

	public Integer getPzpPozNrpoz() {
		return pzpPozNrpoz;
	}

	public void setPzpPozNrpoz(Integer pzpPozNrpoz) {
		this.pzpPozNrpoz = pzpPozNrpoz;
	}

	public Double getPzpPozPIlosc() {
		return pzpPozPIlosc;
	}

	public void setPzpPozPIlosc(Double pzpPozPIlosc) {
		this.pzpPozPIlosc = pzpPozPIlosc;
	}

	public Double getPzpPozPKoszt() {
		return pzpPozPKoszt;
	}

	public void setPzpPozPKoszt(Double pzpPozPKoszt) {
		this.pzpPozPKoszt = pzpPozPKoszt;
	}

	public Double getPzpPozRIlosc() {
		return pzpPozRIlosc;
	}

	public void setPzpPozRIlosc(Double pzpPozRIlosc) {
		this.pzpPozRIlosc = pzpPozRIlosc;
	}

	public Double getPzpPozRKoszt() {
		return pzpPozRKoszt;
	}

	public void setPzpPozRKoszt(Double pzpPozRKoszt) {
		this.pzpPozRKoszt = pzpPozRKoszt;
	}

	public Double getPzpPozRezIlosc() {
		return pzpPozRezIlosc;
	}

	public void setPzpPozRezIlosc(Double pzpPozRezIlosc) {
		this.pzpPozRezIlosc = pzpPozRezIlosc;
	}

	public Double getPzpPozRezKoszt() {
		return pzpPozRezKoszt;
	}

	public void setPzpPozRezKoszt(Double pzpPozRezKoszt) {
		this.pzpPozRezKoszt = pzpPozRezKoszt;
	}
	/* nie ma tego na tescie
	public Integer getPzpStatus() {
		return pzpStatus;
	}

	public void setPzpStatus(Integer pzpStatus) {
		this.pzpStatus = pzpStatus;
	}*/

	public Double getPzpWytworId() {
		return pzpWytworId;
	}

	public void setPzpWytworId(Double pzpWytworId) {
		this.pzpWytworId = pzpWytworId;
	}
	
	public String getPzpWytworIdm() {
		return pzpWytworIdm;
	}

	public void setPzpWytworIdm(String pzpWytworIdm) {
		this.pzpWytworIdm = pzpWytworIdm;
	}
	
	public Double getRezJmId() {
		return rezJmId;
	}

	public void setRezJmId(Double rezJmId) {
		this.rezJmId = rezJmId;
	}

	public String getRezJmIdn() {
		return rezJmIdn;
	}

	public void setRezJmIdn(String rezJmIdn) {
		this.rezJmIdn = rezJmIdn;
	}

	public Double getRezerwacjaCzyArchiwalny() {
		return rezerwacjaCzyArchiwalny;
	}

	public void setRezerwacjaCzyArchiwalny(Double rezerwacjaCzyArchiwalny) {
		this.rezerwacjaCzyArchiwalny = rezerwacjaCzyArchiwalny;
	}

	public Integer getRezerwacjaStatus() {
		return rezerwacjaStatus;
	}

	public void setRezerwacjaStatus(Integer rezerwacjaStatus) {
		this.rezerwacjaStatus = rezerwacjaStatus;
	}

	public Double getVatstawId() {
		return vatstawId;
	}

	public void setVatstawId(Double vatstawId) {
		this.vatstawId = vatstawId;
	}

	public String getWytworEditIdm() {
		return wytworEditIdm;
	}

	public void setWytworEditIdm(String wytworEditIdm) {
		this.wytworEditIdm = wytworEditIdm;
	}

	public String getWytworEditNazwa() {
		return wytworEditNazwa;
	}

	public void setWytworEditNazwa(String wytworEditNazwa) {
		this.wytworEditNazwa = wytworEditNazwa;
	}

	public Double getWytworId() {
		return wytworId;
	}

	public void setWytworId(Double wytworId) {
		this.wytworId = wytworId;
	}

	public Double getZadanieId() {
		return zadanieId;
	}

	public void setZadanieId(Double zadanieId) {
		this.zadanieId = zadanieId;
	}

	public String getZadanieIdn() {
		return zadanieIdn;
	}

	public void setZadanieIdn(String zadanieIdn) {
		this.zadanieIdn = zadanieIdn;
	}

	public String getZadanieNazwa() {
		return zadanieNazwa;
	}

	public void setZadanieNazwa(String zadanieNazwa) {
		this.zadanieNazwa = zadanieNazwa;
	}
	
	public Double getZrodloId() {
		return zrodloId;
	}

	public void setZrodloId(Double zrodloId) {
		this.zrodloId = zrodloId;
	}

	public String getZrodloIdn() {
		return zrodloIdn;
	}

	public void setZrodloIdn(String zrodloIdn) {
		this.zrodloIdn = zrodloIdn;
	}

	public String getZrodloNazwa() {
		return zrodloNazwa;
	}

	public void setZrodloNazwa(String zrodloNazwa) {
		this.zrodloNazwa = zrodloNazwa;
	}

}
