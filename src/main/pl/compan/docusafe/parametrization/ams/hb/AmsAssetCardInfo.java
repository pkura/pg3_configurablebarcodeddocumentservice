/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.news.Notification;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 11-03-2014
 * ds_trunk_2014
 * AmsAssetCardInfo.java
 */
@Entity
@Table(name = AmsAssetCardInfo.TABLE_NAME)
public class AmsAssetCardInfo implements java.io.Serializable {
    public static final String TABLE_NAME = "dsg_ams_asset_card_info";
    public static final String VIEW_NAME = "dsg_ams_view_asset_card_info";
    private static AmsAssetCardInfo instance;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "dsg_ams_asset_card_info_seq")
    @SequenceGenerator(name = "dsg_ams_asset_card_info_seq", sequenceName = "dsg_ams_asset_card_info_seq")
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name="AMO_PROCENT")
    private Double amoProcent;
    @Column(name="DATA_EWIDENCJI")
    private Date dataEwidencji;
    @Column(name="DATA_LIKWIDACJI")
    private Date dataLikwidacji;
    @Column(name="DATA_OBOWIAZYWANIA")
    private Date dataObowiazywania;
    @Column(name="DATA_PRZEKAZANIA")
    private Date dataPrzekazania;
    @Column(name="DATA_WPROWADZENIA")
    private Date dataWprowadzenia;
    @Column(name="DATA_ZAKUPU")
    private Date dataZakupu;
    @Column(name="DATA_ZMIANY")
    private Date dataZmiany;
    @Column(name="GUS", length = 50)
    private String gus;
    @Column(name="ERP_ID")
    private Double erpId;
    @Column(name="IDM", length = 50)
    private String idm;
    @Column(name="ILOSC")
    private Integer ilosc;
    @Column(name="LICZNY")
    private Double liczny;
    @Column(name="NAZWA", length = 50)
    private String nazwa;
    @Column(name="NAZWA_RODZAJOWA", length = 50)
    private String nazwaRodzajowa;
    @Column(name="NR_INWENTARZOWY", length = 50)
    private String nrInwentarzowy;
    @Column(name="OPIS", length = 255)
    private String opis;
    @Column(name="RODZAJ_MAJATKU", length = 50)
    private String rodzajMajatku;
    @Column(name="ROK_PROD")
    private Integer rokProd;
    @Column(name="STAN_MAJATKU", length = 100)
    private String stanMajatku;
    @Column(name="STATUS", length = 50)
    private String status;
    @Column(name="STAWKA_AMO")
    private Double stawkaAmo;
    @Column(name="UMORZENIE_POCZATKOWE")
    private Double umorzeniePoczatkowe;
    @Column(name="USUNIETY")
    private Double usuniety;
    @Column(name="WARTOSC_POCZATKOWA")
    private Double wartoscPoczatkowa;
    @Column(name="WERSJA")
    private Integer wersja;
    @Column(name="ZUZYCIE_POCZATKOWE")
    private Double zuzyciePoczatkowe;
    @Column(name = "DANE_KARTY_ID")
    private Double daneKartyId;

    public static AmsAssetCardInfo find(Long id) throws EdmException {
        return Finder.find(AmsAssetCardInfo.class, id);
    }

    public static AmsAssetCardInfo findByInventoryNumber(String nrInwentarzowy) throws EdmException 
	{
    	Criteria crit = DSApi.context().session().createCriteria(AmsAssetCardInfo.class);
    	crit.add(Restrictions.eq("nrInwentarzowy", nrInwentarzowy));
    	return (AmsAssetCardInfo) crit.uniqueResult();
	}
    
    public AmsAssetCardInfo() {
    }

    public void setAllFieldsFromServiceObject(DictionaryServiceStub.AssetCardInfo item) {

        amoProcent = item.getCzy_amo_procent();
        dataEwidencji = item.getData_ewidencji();
        dataLikwidacji = item.getData_likwidacji();
        dataObowiazywania = item.getData_obowiazywania();
        dataPrzekazania = item.getData_przekazania();
        dataWprowadzenia = item.getData_wprowadzenia();
        dataZakupu = item.getData_zakupu();
        dataZmiany = item.getData_zmiany();
        gus = item.getGus();
        erpId = item.getId();
        idm = item.getIdm();
        ilosc = item.getIlosc();
        liczny = item.getLiczny();
        nazwa = StringUtils.left(item.getNazwa(), 100);
        nazwaRodzajowa = item.getNazwa_rodzajowa();
        nrInwentarzowy = item.getNr_inwentarzowy();
        opis = item.getOpis();
        rodzajMajatku = item.getRodzaj_majatku();
        rokProd = item.getRok_prod();
        stanMajatku = item.getStan_majatku();
        status = item.getStatus();
        stawkaAmo = item.getStawka_amo();
        umorzeniePoczatkowe = item.getUmorzenie_poczatkowe();
        usuniety = item.getUsuniety();
        wartoscPoczatkowa = item.getWartosc_poczatkowa();
        wersja = item.getWersja();
        zuzyciePoczatkowe = item.getZuzycie_poczatkowe();
        daneKartyId = item.getDane_karty_id();

    }

    public void save() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public Double getAmoProcent() {
        return amoProcent;
    }

    public Date getDataEwidencji() {
        return dataEwidencji;
    }

    public Date getDataLikwidacji() {
        return dataLikwidacji;
    }

    public Date getDataObowiazywania() {
        return dataObowiazywania;
    }

    public Date getDataPrzekazania() {
        return dataPrzekazania;
    }

    public Date getDataWprowadzenia() {
        return dataWprowadzenia;
    }

    public Date getDataZakupu() {
        return dataZakupu;
    }

    public Date getDataZmiany() {
        return dataZmiany;
    }

    public String getGus() {
        return gus;
    }

    public Double getErpId() {
        return erpId;
    }

    public String getIdm() {
        return idm;
    }

    public Integer getIlosc() {
        return ilosc;
    }

    public Double getLiczny() {
        return liczny;
    }

    public String getNazwa() {
        return nazwa;
    }

    public String getNazwaRodzajowa() {
        return nazwaRodzajowa;
    }

    public String getNrInwentarzowy() {
        return nrInwentarzowy;
    }

    public String getOpis() {
        return opis;
    }

    public String getRodzajMajatku() {
        return rodzajMajatku;
    }

    public Integer getRokProd() {
        return rokProd;
    }

    public String getStanMajatku() {
        return stanMajatku;
    }

    public String getStatus() {
        return status;
    }

    public Double getStawkaAmo() {
        return stawkaAmo;
    }

    public Double getUmorzeniePoczatkowe() {
        return umorzeniePoczatkowe;
    }

    public Double getUsuniety() {
        return usuniety;
    }

    public Double getWartoscPoczatkowa() {
        return wartoscPoczatkowa;
    }

    public Integer getWersja() {
        return wersja;
    }

    public Double getZuzyciePoczatkowe() {
        return zuzyciePoczatkowe;
    }

    public void setAmoProcent(Double amoProcent) {
        this.amoProcent = amoProcent;
    }

    public void setDataEwidencji(Date dataEwidencji) {
        this.dataEwidencji = dataEwidencji;
    }

    public void setDataLikwidacji(Date dataLikwidacji) {
        this.dataLikwidacji = dataLikwidacji;
    }

    public void setDataObowiazywania(Date dataObowiazywania) {
        this.dataObowiazywania = dataObowiazywania;
    }

    public void setDataPrzekazania(Date dataPrzekazania) {
        this.dataPrzekazania = dataPrzekazania;
    }

    public void setDataWprowadzenia(Date dataWprowadzenia) {
        this.dataWprowadzenia = dataWprowadzenia;
    }

    public void setDataZakupu(Date dataZakupu) {
        this.dataZakupu = dataZakupu;
    }

    public void setDataZmiany(Date dataZmiany) {
        this.dataZmiany = dataZmiany;
    }

    public void setGus(String gus) {
        this.gus = gus;
    }

    public void setErpId(Double erpId) {
        this.erpId = erpId;
    }

    public void setIdm(String idm) {
        this.idm = idm;
    }

    public void setIlosc(Integer ilosc) {
        this.ilosc = ilosc;
    }

    public void setLiczny(Double liczny) {
        this.liczny = liczny;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public void setNazwaRodzajowa(String nazwaRodzajowa) {
        this.nazwaRodzajowa = nazwaRodzajowa;
    }

    public void setNrInwentarzowy(String nrInwentarzowy) {
        this.nrInwentarzowy = nrInwentarzowy;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public void setRodzajMajatku(String rodzajMajatku) {
        this.rodzajMajatku = rodzajMajatku;
    }

    public void setRokProd(Integer rokProd) {
        this.rokProd = rokProd;
    }

    public void setStanMajatku(String stanMajatku) {
        this.stanMajatku = stanMajatku;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setStawkaAmo(Double stawkaAmo) {
        this.stawkaAmo = stawkaAmo;
    }

    public void setUmorzeniePoczatkowe(Double umorzeniePoczatkowe) {
        this.umorzeniePoczatkowe = umorzeniePoczatkowe;
    }

    public void setUsuniety(Double usuniety) {
        this.usuniety = usuniety;
    }

    public void setWartoscPoczatkowa(Double wartoscPoczatkowa) {
        this.wartoscPoczatkowa = wartoscPoczatkowa;
    }

    public void setWersja(Integer wersja) {
        this.wersja = wersja;
    }

    public void setZuzyciePoczatkowe(Double zuzyciePoczatkowe) {
        this.zuzyciePoczatkowe = zuzyciePoczatkowe;
    }

    public Double getDaneKartyId() {
        return daneKartyId;
    }

    public void setDaneKartyId(Double daneKartyId) {
        this.daneKartyId = daneKartyId;
    }

    public Long getId() {
        return id;
    }
}

