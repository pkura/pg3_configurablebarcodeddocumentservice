/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class ContractType implements java.io.Serializable {
	private long id;
	private String idm;
	private String name;
	private double czyAktywny;
	
	public ContractType() {
		
	}
	
	public ContractType(String idm, String name, double czyAktywny) {
		super();
		this.idm = idm;
		this.name = name;
		this.czyAktywny = czyAktywny;
	}
	
    public void setAllFieldsFromServiceObject(pl.compan.docusafe.ws.ams.DictionaryServiceStub.ContractType item) {
    	setIdm(item.getIdm());
    	setName(item.getNazwa());
    	setCzyAktywny(item.getCzy_aktywny());
    }
    
    public void save() throws EdmException {
    	try {
    		DSApi.context().session().save(this);
    	} catch (HibernateException e) {
    		new EdmException(e);
    	}
    }
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idm == null) ? 0 : idm.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ContractType)) {
			return false;
		}
		ContractType other = (ContractType) obj;
		if (idm == null) {
			if (other.idm != null) {
				return false;
			}
		} else if (!idm.equals(other.idm)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getIdm() {
		return idm;
	}
	public void setIdm(String idm) {
		this.idm = idm;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getCzyAktywny() {
		return czyAktywny;
	}
	public void setCzyAktywny(double czyAktywny) {
		this.czyAktywny = czyAktywny;
	}

}
