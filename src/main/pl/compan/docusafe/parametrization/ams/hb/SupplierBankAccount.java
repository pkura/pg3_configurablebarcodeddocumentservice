/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class SupplierBankAccount implements java.io.Serializable {
	private long id;
	private double idFromWebservice;
	private double czyAktywne;
	private double identyfikatorNum;
	private String idm;
	private int kolejnoscUzycia;
	private String numerKonta;
	private String symbolWaluty;
	private boolean available;
	
	public SupplierBankAccount() {
		
	}
	
	public SupplierBankAccount(double idFromWebservice, double czyAktywne,
			double identyfikatorNum, String idm, int kolejnoscUzycia,
			String numerKonta, String symbolWaluty, boolean available) {
		super();
		this.idFromWebservice = idFromWebservice;
		this.czyAktywne = czyAktywne;
		this.identyfikatorNum = identyfikatorNum;
		this.idm = idm;
		this.kolejnoscUzycia = kolejnoscUzycia;
		this.numerKonta = numerKonta;
		this.symbolWaluty = symbolWaluty;
		this.available = available;
	}
	
    public void setAllFieldsFromServiceObject(pl.compan.docusafe.ws.ams.DictionaryServiceStub.SupplierBankAccount item) {
    	setIdFromWebservice(item.getId());
//    	setCzyAktywne(item.getCzy_aktywne());
//    	setIdentyfikatorNum(item.getIdentyfikator_num());
    	setIdm(item.getIdm());
    	setKolejnoscUzycia(item.getKolejnosc_uzycia());
    	setNumerKonta(item.getNumer_konta());
    	setSymbolWaluty(item.getSymbolwaluty());
//    	setAvailable(new Double(1).equals(item.getCzy_aktywne()) ? true : false);
    }
    
    public void save() throws EdmException {
    	try {
    		DSApi.context().session().save(this);
    	} catch (HibernateException e) {
    		new EdmException(e);
    	}
    }
    

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(czyAktywne);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(idFromWebservice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(identyfikatorNum);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((idm == null) ? 0 : idm.hashCode());
		result = prime * result + kolejnoscUzycia;
		result = prime * result
				+ ((numerKonta == null) ? 0 : numerKonta.hashCode());
		result = prime * result
				+ ((symbolWaluty == null) ? 0 : symbolWaluty.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof SupplierBankAccount)) {
			return false;
		}
		SupplierBankAccount other = (SupplierBankAccount) obj;
		if (Double.doubleToLongBits(czyAktywne) != Double
				.doubleToLongBits(other.czyAktywne)) {
			return false;
		}
		if (Double.doubleToLongBits(idFromWebservice) != Double
				.doubleToLongBits(other.idFromWebservice)) {
			return false;
		}
		if (Double.doubleToLongBits(identyfikatorNum) != Double
				.doubleToLongBits(other.identyfikatorNum)) {
			return false;
		}
		if (idm == null) {
			if (other.idm != null) {
				return false;
			}
		} else if (!idm.equals(other.idm)) {
			return false;
		}
		if (kolejnoscUzycia != other.kolejnoscUzycia) {
			return false;
		}
		if (numerKonta == null) {
			if (other.numerKonta != null) {
				return false;
			}
		} else if (!numerKonta.equals(other.numerKonta)) {
			return false;
		}
		if (symbolWaluty == null) {
			if (other.symbolWaluty != null) {
				return false;
			}
		} else if (!symbolWaluty.equals(other.symbolWaluty)) {
			return false;
		}
		return true;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public double getIdFromWebservice() {
		return idFromWebservice;
	}
	public void setIdFromWebservice(double idFromWebservice) {
		this.idFromWebservice = idFromWebservice;
	}
	public double getCzyAktywne() {
		return czyAktywne;
	}
	public void setCzyAktywne(double czyAktywne) {
		this.czyAktywne = czyAktywne;
	}
	public double getIdentyfikatorNum() {
		return identyfikatorNum;
	}
	public void setIdentyfikatorNum(double identyfikatorNum) {
		this.identyfikatorNum = identyfikatorNum;
	}
	public String getIdm() {
		return idm;
	}
	public void setIdm(String idm) {
		this.idm = idm;
	}
	public int getKolejnoscUzycia() {
		return kolejnoscUzycia;
	}
	public void setKolejnoscUzycia(int kolejnoscUzycia) {
		this.kolejnoscUzycia = kolejnoscUzycia;
	}
	public String getNumerKonta() {
		return numerKonta;
	}
	public void setNumerKonta(String numerKonta) {
		this.numerKonta = numerKonta;
	}
	public String getSymbolWaluty() {
		return symbolWaluty;
	}
	public void setSymbolWaluty(String symbolWaluty) {
		this.symbolWaluty = symbolWaluty;
	}
	public boolean isAvailable() {
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}
	
}
