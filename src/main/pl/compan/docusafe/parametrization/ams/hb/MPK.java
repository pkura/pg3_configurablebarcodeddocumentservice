/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import java.math.BigDecimal;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

import com.google.common.collect.Lists;



public class MPK {
	
	private Integer id;
	private Integer centrumId;
	private BigDecimal grossAmount;
	private String centrumName;
	private BigDecimal netAmount;
	
	public MPK() {}
	
	public MPK(Integer id, Integer centrumId, BigDecimal grossAmount, String centrumName, BigDecimal netAmount) {
		this.id = id;
		this.centrumId = centrumId;
		this.grossAmount = grossAmount;
		this.centrumName = centrumName;
		this.netAmount = netAmount;
	}
	
	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (other == this) {
			return true;
		}
		if (!(other instanceof MPK)) {
			return false;
		}
		MPK otherMPK = (MPK)other;
		if (otherMPK.centrumId.equals(this.centrumId) && otherMPK.grossAmount.equals(this.grossAmount)) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		int hashCentrum = 0;
		int hashAmount = 0;
		if (centrumId != null) {
			hashCentrum = centrumId;
		}
		if (grossAmount != null) {
			hashAmount = grossAmount.intValue();
		}
		return hashCentrum + hashAmount;
	}
	
	public static List<Integer> findMPK(Long documentId) throws EdmException {
		List<Integer> ids = Lists.newArrayList();
		List<String> idsString = Lists.newArrayList();
		idsString = DSApi.context().session().createSQLQuery("SELECT field_val FROM dsg_ams_fakt_zakup_multiple WHERE document_id = :docId AND field_cn = 'MPK'")
			.setParameter("docId", documentId)
			.list();
		for (String id : idsString) {
			ids.add(Integer.valueOf(id));
		}
		return ids;
	}
	
	public static String findCentrumName(Integer centrumId) throws EdmException {
		String centrumName = (String) DSApi.context().session().createSQLQuery("SELECT title FROM dsg_ams_centra_kosztow WHERE id = :centrumId")
			.setParameter("centrumId", centrumId)
			.uniqueResult();
		return centrumName;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCentrumId() {
		return centrumId;
	}
	public void setCentrumId(Integer centrumId) {
		this.centrumId = centrumId;
	}
	public BigDecimal getGrossAmount() {
		return grossAmount;
	}
	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}

	public String getCentrumName() {
		return centrumName;
	}

	public void setCentrumName(String centrumName) {
		this.centrumName = centrumName;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}
	
}
