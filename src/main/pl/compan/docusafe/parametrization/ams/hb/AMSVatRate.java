/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import java.util.Date;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class AMSVatRate implements java.io.Serializable {
	private long id;
	private double czyAktywna;
	private double czyZwolniona;
	private Date dataObowDo;
	private double idFromWebservice;
	private String idm;
	private String nazwa;
	private double niePodlega;
	private double procent;
	private boolean available;
	
	public AMSVatRate() {
		
	}

	public AMSVatRate(double czyAktywna, double czyZwolniona, Date dataObowDo,
			double idFromWebservice, String idm, String nazwa,
			double niePodlega, double procent, boolean available) {
		super();
		this.czyAktywna = czyAktywna;
		this.czyZwolniona = czyZwolniona;
		this.dataObowDo = dataObowDo;
		this.idFromWebservice = idFromWebservice;
		this.idm = idm;
		this.nazwa = nazwa;
		this.niePodlega = niePodlega;
		this.procent = procent;
		this.available = available;
	}

    public void setAllFieldsFromServiceObject(pl.compan.docusafe.ws.ams.DictionaryServiceStub.VatRate item) {
    	setCzyAktywna(item.getCzy_aktywna());
    	setCzyZwolniona(item.getCzy_zwolniona());
    	setDataObowDo(item.getData_obow_do());
    	setIdFromWebservice(item.getId());
    	setIdm(item.getIdm());
    	setNazwa(item.getNazwa());
    	setNiePodlega(item.getNie_podlega());
    	setProcent(item.getProcent());
    	setAvailable(new Double(1).equals(item.getCzy_aktywna()) ? true : false);
    }
    
    public void save() throws EdmException {
    	try {
    		DSApi.context().session().save(this);
    	} catch (HibernateException e) {
    		new EdmException(e);
    	}
    }
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (available ? 1231 : 1237);
		long temp;
		temp = Double.doubleToLongBits(czyAktywna);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(czyZwolniona);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result
				+ ((dataObowDo == null) ? 0 : dataObowDo.hashCode());
		temp = Double.doubleToLongBits(idFromWebservice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((idm == null) ? 0 : idm.hashCode());
		result = prime * result + ((nazwa == null) ? 0 : nazwa.hashCode());
		temp = Double.doubleToLongBits(niePodlega);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(procent);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AMSVatRate)) {
			return false;
		}
		AMSVatRate other = (AMSVatRate) obj;
		if (available != other.available) {
			return false;
		}
		if (Double.doubleToLongBits(czyAktywna) != Double
				.doubleToLongBits(other.czyAktywna)) {
			return false;
		}
		if (Double.doubleToLongBits(czyZwolniona) != Double
				.doubleToLongBits(other.czyZwolniona)) {
			return false;
		}
		if (dataObowDo == null) {
			if (other.dataObowDo != null) {
				return false;
			}
		} else if (!dataObowDo.equals(other.dataObowDo)) {
			return false;
		}
		if (Double.doubleToLongBits(idFromWebservice) != Double
				.doubleToLongBits(other.idFromWebservice)) {
			return false;
		}
		if (idm == null) {
			if (other.idm != null) {
				return false;
			}
		} else if (!idm.equals(other.idm)) {
			return false;
		}
		if (nazwa == null) {
			if (other.nazwa != null) {
				return false;
			}
		} else if (!nazwa.equals(other.nazwa)) {
			return false;
		}
		if (Double.doubleToLongBits(niePodlega) != Double
				.doubleToLongBits(other.niePodlega)) {
			return false;
		}
		if (Double.doubleToLongBits(procent) != Double
				.doubleToLongBits(other.procent)) {
			return false;
		}
		return true;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getCzyAktywna() {
		return czyAktywna;
	}

	public void setCzyAktywna(double czyAktywna) {
		this.czyAktywna = czyAktywna;
	}

	public double getCzyZwolniona() {
		return czyZwolniona;
	}

	public void setCzyZwolniona(double czyZwolniona) {
		this.czyZwolniona = czyZwolniona;
	}

	public Date getDataObowDo() {
		return dataObowDo;
	}

	public void setDataObowDo(Date dataObowDo) {
		this.dataObowDo = dataObowDo;
	}

	public double getIdFromWebservice() {
		return idFromWebservice;
	}

	public void setIdFromWebservice(double idFromWebservice) {
		this.idFromWebservice = idFromWebservice;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public double getNiePodlega() {
		return niePodlega;
	}

	public void setNiePodlega(double niePodlega) {
		this.niePodlega = niePodlega;
	}

	public double getProcent() {
		return procent;
	}

	public void setProcent(double procent) {
		this.procent = procent;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

}
