/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 25-03-2014
 * ds_trunk_2014
 * AmsAssetOperationType.java
 */
@Entity
@Table(name = AmsAssetOperationType.TABLE_NAME)
public class AmsAssetOperationType implements java.io.Serializable {
    public static final String TABLE_NAME = "dsg_ams_asset_operation_type";
    public static final String VIEW_NAME = "dsg_ams_asset_operation_type_view";
    private static AmsAssetOperationType instance;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "dsg_ams_asset_op_type_seq")
    @SequenceGenerator(name = "dsg_ams_asset_op_type_seq", sequenceName = "dsg_ams_asset_op_type_seq")
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name="ERP_ID")
    private Double erpId;
    @Column(name="IDM", length = 50)
    private String idm;
    @Column(name="NAZWA", length = 50)
    private String nazwa;
    @Column(name="AKTYWNY")
	private Double aktywny;
    @Column(name="ILOSC_DO_POZYCJI")
    private Double iloscDoPozycji;
    @Column(name="WALUTOWY")
    private Double walutowy;
    @Column(name="WIELOPOZ")
    private Double wielopoz;
    @Column(name="DOKUMENT")
    private Integer dokument;
    @Column(name="RODZAJ_OPE", length = 50)
    private String rodzajOpe;
    @Column(name="RODZAJ_OPEPOZ", length = 50)
    private String rodzajOpepoz;
    @Column(name="WALUTA_ID")
    private Double walutaId;
    @Column(name="WPLYW_OPERACJI")
    private Integer wplywOperacji;

    public static AmsAssetOperationType find(Integer id) throws EdmException {
        return Finder.find(AmsAssetOperationType.class, id);
    }

    public AmsAssetOperationType() {
    }

    public void setAllFieldsFromServiceObject(DictionaryServiceStub.AssetOperationType item) {

        erpId = item.getId();
        idm = item.getIdm();
        nazwa = StringUtils.left(item.getNazwa(), 100);
        aktywny = item.getCzy_aktywny();
        iloscDoPozycji = item.getCzy_ilosc_do_pozycji();
        walutowy = item.getCzy_walutowy();
        wielopoz = item.getCzy_wielepoz();
        dokument = item.getDokument();
        rodzajOpe = item.getRodzaj_ope();
        rodzajOpepoz = item.getRodzaj_opepoz();
        walutaId = item.getWaluta_id();
        wplywOperacji = item.getWplywoperacji();
    }

    public void save() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public Double getAktywny() {
		return aktywny;
	}

	public void setAktywny(Double aktywny) {
		this.aktywny = aktywny;
	}

	public Double getIloscDoPozycji() {
		return iloscDoPozycji;
	}

	public void setIloscDoPozycji(Double iloscDoPozycji) {
		this.iloscDoPozycji = iloscDoPozycji;
	}

	public Double getWalutowy() {
		return walutowy;
	}

	public void setWalutowy(Double walutowy) {
		this.walutowy = walutowy;
	}

	public Double getWielopoz() {
		return wielopoz;
	}

	public void setWielopoz(Double wielopoz) {
		this.wielopoz = wielopoz;
	}

	public Integer getDokument() {
		return dokument;
	}

	public void setDokument(Integer dokument) {
		this.dokument = dokument;
	}

	public String getRodzajOpe() {
		return rodzajOpe;
	}

	public void setRodzajOpe(String rodzajOpe) {
		this.rodzajOpe = rodzajOpe;
	}

	public String getRodzajOpepoz() {
		return rodzajOpepoz;
	}

	public void setRodzajOpepoz(String rodzajOpepoz) {
		this.rodzajOpepoz = rodzajOpepoz;
	}

	public Double getWalutaId() {
		return walutaId;
	}

	public void setWalutaId(Double walutaId) {
		this.walutaId = walutaId;
	}

	public Integer getWplywOperacji() {
		return wplywOperacji;
	}

	public void setWplywOperacji(Integer wplywOperacji) {
		this.wplywOperacji = wplywOperacji;
	}


    public Double getErpId() {
        return erpId;
    }

    public String getIdm() {
        return idm;
    }

    public void setErpId(Double erpId) {
        this.erpId = erpId;
    }

    public void setIdm(String idm) {
        this.idm = idm;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public Long getId() {
        return id;
    }
}


