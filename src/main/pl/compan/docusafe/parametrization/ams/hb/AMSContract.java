/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import java.util.Date;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class AMSContract implements java.io.Serializable {
	private long id;
	private double erpId;
	private double identyfikatorNum;
	private String idm;
	private String nazwa;
	private Date pDatako;
	private Date pDatapo;
	private double typKontraktId;
	private String typKontraktIdn;
	
	public AMSContract() {
		
	}

	public AMSContract(double erpId, double identyfikatorNum, String idm, String nazwa,
			Date pDatako, Date pDatapo, double typKontraktId,
			String typKontraktIdn) {
		super();
		this.erpId = erpId;
		this.identyfikatorNum = identyfikatorNum;
		this.idm = idm;
		this.nazwa = nazwa;
		this.pDatako = pDatako;
		this.pDatapo = pDatapo;
		this.typKontraktId = typKontraktId;
		this.typKontraktIdn = typKontraktIdn;
	}

    public void setAllFieldsFromServiceObject(pl.compan.docusafe.ws.ams.DictionaryServiceStub.Contract item) {
    	setErpId(item.getId());
    	setIdentyfikatorNum(item.getIdentyfikator_num());
    	setIdm(item.getIdm());
    	setNazwa(item.getNazwa());
    	setpDatako(item.getP_datako());
    	setpDatapo(item.getP_datapo());
    	setTypKontraktId(item.getTyp_kontrakt_id());
    	setTypKontraktIdn(item.getTyp_kontrakt_idn());
    }
    
    public void save() throws EdmException {
    	try {
    		DSApi.context().session().save(this);
    	} catch (HibernateException e) {
    		new EdmException(e);
    	}
    }
    
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public double getErpId() {
		return erpId;
	}
	
	public void setErpId(double erpId) {
		this.erpId = erpId;
	}

	public double getIdentyfikatorNum() {
		return identyfikatorNum;
	}

	public void setIdentyfikatorNum(double identyfikatorNum) {
		this.identyfikatorNum = identyfikatorNum;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Date getpDatako() {
		return pDatako;
	}

	public void setpDatako(Date pDatako) {
		this.pDatako = pDatako;
	}

	public Date getpDatapo() {
		return pDatapo;
	}

	public void setpDatapo(Date pDatapo) {
		this.pDatapo = pDatapo;
	}

	public double getTypKontraktId() {
		return typKontraktId;
	}

	public void setTypKontraktId(double typKontraktId) {
		this.typKontraktId = typKontraktId;
	}

	public String getTypKontraktIdn() {
		return typKontraktIdn;
	}

	public void setTypKontraktIdn(String typKontraktIdn) {
		this.typKontraktIdn = typKontraktIdn;
	}

}
