/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
@Entity
@Table(name = VatRatio.TABLE_NAME)
public class VatRatio implements java.io.Serializable {
	
	public static final String TABLE_NAME = "dsg_ams_vat_ratio";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "erpId")
	private Double erpId;
	@Column(name = "komorka_id")
	private Double komorkaId;
	@Column(name = "okrrozl_id")
	private Double okrrozlId;
	@Column(name = "rok")
	private Integer rok;
	@Column(name = "wartosc")
	private Double wartosc;
	
	public VatRatio() {
		
	}
	
    public VatRatio(Double erpId, Double komorkaId, Double okrrozlId,
			Integer rok, Double wartosc) {
		super();
		this.erpId = erpId;
		this.komorkaId = komorkaId;
		this.okrrozlId = okrrozlId;
		this.rok = rok;
		this.wartosc = wartosc;
	}

	public void setAllFieldsFromServiceObject(pl.compan.docusafe.ws.ams.DictionaryServiceStub.VatRatios item) {
    	setErpId(item.getId());
    	setKomorkaId(item.getKomorka_id());
    	setOkrrozlId(item.getOkrrozl_id());
    	setRok(item.getRok());
    	setWartosc(item.getWartosc());
    }

    public void save() throws EdmException {
    	try {
    		DSApi.context().session().save(this);
    	} catch (HibernateException e) {
    		new EdmException(e);
    	}
    }
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getErpId() {
		return erpId;
	}

	public void setErpId(Double erpId) {
		this.erpId = erpId;
	}

	public Double getKomorkaId() {
		return komorkaId;
	}

	public void setKomorkaId(Double komorkaId) {
		this.komorkaId = komorkaId;
	}

	public Double getOkrrozlId() {
		return okrrozlId;
	}

	public void setOkrrozlId(Double okrrozlId) {
		this.okrrozlId = okrrozlId;
	}

	public Integer getRok() {
		return rok;
	}

	public void setRok(Integer rok) {
		this.rok = rok;
	}

	public Double getWartosc() {
		return wartosc;
	}

	public void setWartosc(Double wartosc) {
		this.wartosc = wartosc;
	}
	
}
