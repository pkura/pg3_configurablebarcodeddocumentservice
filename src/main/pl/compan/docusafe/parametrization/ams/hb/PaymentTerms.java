/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
@Entity
@Table(name = PaymentTerms.TABLE_NAME)
public class PaymentTerms implements java.io.Serializable {
	
	public static final String TABLE_NAME = "dsg_ams_payment_terms";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "erpId")
	private Double erpId;
	@Column(name = "idm", length = 100)
	private String idm;
	@Column(name = "przeznaczenie", length = 100)
	private String przeznaczenie;
	
	public PaymentTerms() {
		
	}
	
	public PaymentTerms(Double erpId, String idm, String przeznaczenie) {
		super();
		this.erpId = erpId;
		this.idm = idm;
		this.przeznaczenie = przeznaczenie;
	}
	
	public void setAllFieldsFromServiceObject(pl.compan.docusafe.ws.ams.DictionaryServiceStub.PaymentTerms item) {
    	setErpId(item.getId());
    	setIdm(item.getIdm());
    	setPrzeznaczenie(item.getPrzeznaczenie());
    }
    
    public void save() throws EdmException {
    	try {
    		DSApi.context().session().save(this);
    	} catch (HibernateException e) {
    		new EdmException(e);
    	}
    }

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Double getErpId() {
		return erpId;
	}
	public void setErpId(Double erpId) {
		this.erpId = erpId;
	}
	public String getIdm() {
		return idm;
	}
	public void setIdm(String idm) {
		this.idm = idm;
	}
	public String getPrzeznaczenie() {
		return przeznaczenie;
	}
	public void setPrzeznaczenie(String przeznaczenie) {
		this.przeznaczenie = przeznaczenie;
	}
}
