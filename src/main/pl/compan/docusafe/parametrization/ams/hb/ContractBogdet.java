/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import java.util.Date;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class ContractBogdet implements java.io.Serializable {
	private long id;
	private double idFromWebservice;
	private String idm;
	private double kontraktId;
	private String nazwa;
	private Date pDatako;
	private Date pDatapo;
	private int stan;
	private int status;
	
	public ContractBogdet() {
		
	}
	
	public ContractBogdet(double idFromWebservice, String idm,
			double kontraktId, String nazwa, Date pDatako, Date pDatapo,
			int stan, int status) {
		super();
		this.idFromWebservice = idFromWebservice;
		this.idm = idm;
		this.kontraktId = kontraktId;
		this.nazwa = nazwa;
		this.pDatako = pDatako;
		this.pDatapo = pDatapo;
		this.stan = stan;
		this.status = status;
	}
	
    public void setAllFieldsFromServiceObject(pl.compan.docusafe.ws.ams.DictionaryServiceStub.ContractBogdet item) {
    	setIdFromWebservice(item.getId());
    	setIdm(item.getIdm());
    	setKontraktId(item.getKontrakt_id());
    	setNazwa(item.getNazwa());
    	setpDatako(item.getP_datako());
    	setpDatapo(item.getP_datapo());
    	setStan(item.getStan());
    	setStatus(item.getStatus());
    }
    
    public void save() throws EdmException {
    	try {
    		DSApi.context().session().save(this);
    	} catch (HibernateException e) {
    		new EdmException(e);
    	}
    }
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(idFromWebservice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((idm == null) ? 0 : idm.hashCode());
		temp = Double.doubleToLongBits(kontraktId);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((nazwa == null) ? 0 : nazwa.hashCode());
		result = prime * result + ((pDatako == null) ? 0 : pDatako.hashCode());
		result = prime * result + ((pDatapo == null) ? 0 : pDatapo.hashCode());
		result = prime * result + stan;
		result = prime * result + status;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ContractBogdet)) {
			return false;
		}
		ContractBogdet other = (ContractBogdet) obj;
		if (Double.doubleToLongBits(idFromWebservice) != Double
				.doubleToLongBits(other.idFromWebservice)) {
			return false;
		}
		if (idm == null) {
			if (other.idm != null) {
				return false;
			}
		} else if (!idm.equals(other.idm)) {
			return false;
		}
		if (Double.doubleToLongBits(kontraktId) != Double
				.doubleToLongBits(other.kontraktId)) {
			return false;
		}
		if (nazwa == null) {
			if (other.nazwa != null) {
				return false;
			}
		} else if (!nazwa.equals(other.nazwa)) {
			return false;
		}
		if (pDatako == null) {
			if (other.pDatako != null) {
				return false;
			}
		} else if (!pDatako.equals(other.pDatako)) {
			return false;
		}
		if (pDatapo == null) {
			if (other.pDatapo != null) {
				return false;
			}
		} else if (!pDatapo.equals(other.pDatapo)) {
			return false;
		}
		if (stan != other.stan) {
			return false;
		}
		if (status != other.status) {
			return false;
		}
		return true;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getIdFromWebservice() {
		return idFromWebservice;
	}

	public void setIdFromWebservice(double idFromWebservice) {
		this.idFromWebservice = idFromWebservice;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public double getKontraktId() {
		return kontraktId;
	}

	public void setKontraktId(double kontraktId) {
		this.kontraktId = kontraktId;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Date getpDatako() {
		return pDatako;
	}

	public void setpDatako(Date pDatako) {
		this.pDatako = pDatako;
	}

	public Date getpDatapo() {
		return pDatapo;
	}

	public void setpDatapo(Date pDatapo) {
		this.pDatapo = pDatapo;
	}

	public int getStan() {
		return stan;
	}

	public void setStan(int stan) {
		this.stan = stan;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
