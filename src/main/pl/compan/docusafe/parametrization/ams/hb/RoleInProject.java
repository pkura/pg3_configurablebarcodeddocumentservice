/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class RoleInProject implements java.io.Serializable {
	private long id;
	private double bpRolaId;
	private String bpRolaIdn;
	private double bpRolaKontraktId;
	private double czyBlokadaPrac;
	private double czyBlokadaRoli;
	private double kontraktId;
	private String kontraktIdm;
	private double pracownikId;
	private String pracownikIdn;
	private int pracownikNrewid;
	private String osobaGuid;
	
	public RoleInProject() {
		
	}

	public RoleInProject(double bpRolaId, String bpRolaIdn,
			double bpRolaKontraktId, double czyBlokadaPrac,
			double czyBlokadaRoli, double kontraktId, String kontraktIdm,
			double pracownikId, String pracownikIdn, int pracownikNrewid) {
		super();
		this.bpRolaId = bpRolaId;
		this.bpRolaIdn = bpRolaIdn;
		this.bpRolaKontraktId = bpRolaKontraktId;
		this.czyBlokadaPrac = czyBlokadaPrac;
		this.czyBlokadaRoli = czyBlokadaRoli;
		this.kontraktId = kontraktId;
		this.kontraktIdm = kontraktIdm;
		this.pracownikId = pracownikId;
		this.pracownikIdn = pracownikIdn;
		this.pracownikNrewid = pracownikNrewid;
	}
	
    public void setAllFieldsFromServiceObject(pl.compan.docusafe.ws.ams.DictionaryServiceStub.RolesInProjects item) {
    	setBpRolaId(item.getBp_rola_id());
    	setBpRolaIdn(item.getBp_rola_idn());
    	setBpRolaKontraktId(item.getBp_rola_kontrakt_id());
    	setCzyBlokadaPrac(item.getCzy_blokada_prac());
    	setCzyBlokadaRoli(item.getCzy_blokada_roli());
    	setKontraktId(item.getKontrakt_id());
    	setKontraktIdm(item.getKontrakt_idm());
    	setPracownikId(item.getPracownik_id());
    	setPracownikIdn(item.getPracownik_idn());
    	setPracownikNrewid(item.getPracownik_nrewid());
//    	setOsobaGuid(item.get)
    }
    
    public void save() throws EdmException {
    	try {
    		DSApi.context().session().save(this);
    	} catch (HibernateException e) {
    		new EdmException(e);
    	}
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(bpRolaId);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result
				+ ((bpRolaIdn == null) ? 0 : bpRolaIdn.hashCode());
		temp = Double.doubleToLongBits(bpRolaKontraktId);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(czyBlokadaPrac);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(czyBlokadaRoli);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(kontraktId);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result
				+ ((kontraktIdm == null) ? 0 : kontraktIdm.hashCode());
		temp = Double.doubleToLongBits(pracownikId);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result
				+ ((pracownikIdn == null) ? 0 : pracownikIdn.hashCode());
		result = prime * result + pracownikNrewid;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof RoleInProject)) {
			return false;
		}
		RoleInProject other = (RoleInProject) obj;
		if (Double.doubleToLongBits(bpRolaId) != Double
				.doubleToLongBits(other.bpRolaId)) {
			return false;
		}
		if (bpRolaIdn == null) {
			if (other.bpRolaIdn != null) {
				return false;
			}
		} else if (!bpRolaIdn.equals(other.bpRolaIdn)) {
			return false;
		}
		if (Double.doubleToLongBits(bpRolaKontraktId) != Double
				.doubleToLongBits(other.bpRolaKontraktId)) {
			return false;
		}
		if (Double.doubleToLongBits(czyBlokadaPrac) != Double
				.doubleToLongBits(other.czyBlokadaPrac)) {
			return false;
		}
		if (Double.doubleToLongBits(czyBlokadaRoli) != Double
				.doubleToLongBits(other.czyBlokadaRoli)) {
			return false;
		}
		if (Double.doubleToLongBits(kontraktId) != Double
				.doubleToLongBits(other.kontraktId)) {
			return false;
		}
		if (kontraktIdm == null) {
			if (other.kontraktIdm != null) {
				return false;
			}
		} else if (!kontraktIdm.equals(other.kontraktIdm)) {
			return false;
		}
		if (Double.doubleToLongBits(pracownikId) != Double
				.doubleToLongBits(other.pracownikId)) {
			return false;
		}
		if (pracownikIdn == null) {
			if (other.pracownikIdn != null) {
				return false;
			}
		} else if (!pracownikIdn.equals(other.pracownikIdn)) {
			return false;
		}
		if (pracownikNrewid != other.pracownikNrewid) {
			return false;
		}
		return true;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getBpRolaId() {
		return bpRolaId;
	}

	public void setBpRolaId(double bpRolaId) {
		this.bpRolaId = bpRolaId;
	}

	public String getBpRolaIdn() {
		return bpRolaIdn;
	}

	public void setBpRolaIdn(String bpRolaIdn) {
		this.bpRolaIdn = bpRolaIdn;
	}

	public double getBpRolaKontraktId() {
		return bpRolaKontraktId;
	}

	public void setBpRolaKontraktId(double bpRolaKontraktId) {
		this.bpRolaKontraktId = bpRolaKontraktId;
	}

	public double getCzyBlokadaPrac() {
		return czyBlokadaPrac;
	}

	public void setCzyBlokadaPrac(double czyBlokadaPrac) {
		this.czyBlokadaPrac = czyBlokadaPrac;
	}

	public double getCzyBlokadaRoli() {
		return czyBlokadaRoli;
	}

	public void setCzyBlokadaRoli(double czyBlokadaRoli) {
		this.czyBlokadaRoli = czyBlokadaRoli;
	}

	public double getKontraktId() {
		return kontraktId;
	}

	public void setKontraktId(double kontraktId) {
		this.kontraktId = kontraktId;
	}

	public String getKontraktIdm() {
		return kontraktIdm;
	}

	public void setKontraktIdm(String kontraktIdm) {
		this.kontraktIdm = kontraktIdm;
	}

	public double getPracownikId() {
		return pracownikId;
	}

	public void setPracownikId(double pracownikId) {
		this.pracownikId = pracownikId;
	}

	public String getPracownikIdn() {
		return pracownikIdn;
	}

	public void setPracownikIdn(String pracownikIdn) {
		this.pracownikIdn = pracownikIdn;
	}

	public int getPracownikNrewid() {
		return pracownikNrewid;
	}

	public void setPracownikNrewid(int pracownikNrewid) {
		this.pracownikNrewid = pracownikNrewid;
	}

	public String getOsobaGuid() {
		return osobaGuid;
	}

	public void setOsobaGuid(String osobaGuid) {
		this.osobaGuid = osobaGuid;
	}
	
}
