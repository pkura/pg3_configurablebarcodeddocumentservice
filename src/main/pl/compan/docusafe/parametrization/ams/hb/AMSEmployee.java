/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
@Entity
@Table(name = AMSEmployee.TABLE_NAME)
public class AMSEmployee implements java.io.Serializable {
	
	public static final String TABLE_NAME = "dsg_ams_employee";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "erpId")
	private Double erpId;
	@Column(name = "identyfikator_num")
	private Double identyfikatorNum;
	@Column(name = "idm")
	private Integer idm;
	@Column(name = "imie", length=50)
	private String imie;
	@Column(name = "nazwisko", length=50)
	private String nazwisko;
	@Column(name = "komorka_org", length=50)
	private String komorkaOrg;
	@Column(name = "nip", length=50)
	private String nip;
	@Column(name = "pesel", length=50)
	private String pesel;
	
	public AMSEmployee() {
		
	}

	public AMSEmployee(Double erpId, Double identyfikatorNum, Integer idm,
			String imie, String nazwisko, String komorkaOrg, String nip,
			String pesel) {
		super();
		this.erpId = erpId;
		this.identyfikatorNum = identyfikatorNum;
		this.idm = idm;
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.komorkaOrg = komorkaOrg;
		this.nip = nip;
		this.pesel = pesel;
	}

    public void setAllFieldsFromServiceObject(pl.compan.docusafe.ws.ams.DictionaryServiceStub.Employee item) {
    	setErpId(item.getId());
    	setIdentyfikatorNum(item.getIdentyfikator_num());
    	setIdm(item.getIdm());
    	setImie(item.getImie());
    	setNazwisko(item.getNazwisko());
    	setKomorkaOrg(item.getKomorka_org());
    	setNip(item.getNip());
    	setPesel(item.getPesel());
    }
    
    public void save() throws EdmException {
    	try {
    		DSApi.context().session().save(this);
    	} catch (HibernateException e) {
    		new EdmException(e);
    	}
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getErpId() {
		return erpId;
	}

	public void setErpId(Double erpId) {
		this.erpId = erpId;
	}

	public Double getIdentyfikatorNum() {
		return identyfikatorNum;
	}

	public void setIdentyfikatorNum(Double identyfikatorNum) {
		this.identyfikatorNum = identyfikatorNum;
	}

	public Integer getIdm() {
		return idm;
	}

	public void setIdm(Integer idm) {
		this.idm = idm;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getKomorkaOrg() {
		return komorkaOrg;
	}

	public void setKomorkaOrg(String komorkaOrg) {
		this.komorkaOrg = komorkaOrg;
	}
	
	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
	
}
