/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class SupplierOrderPosition implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private long supplier_order_id;
	private String supplier_order_idm;
	private String wytwor_idm;
	private Float ilosc;
	private Integer nrpoz;
	private Float cena;
	private Float cenabrutto;
	private Float koszt;
	private Float kwotnett;
	private Float kwotvat;
	private long zamdospoz_id;
	private Long budzetId;
	private String budzetIdm;
	private Long bdBudzetKoId;
	private String bdBudzetKoIdm;
	private Long bdRodzajKoId;
	private Long bdPlanZamId;
	private String bdPlanZamIdm;
	private Long bdRodzajPlanZamId;
	private Long bdRezerwacjaId;
	private String bdRezerwacjaIdm;
	private Long bdRezerwacjaPozId;
	private Long kontraktId;
	private Long projektId;
	private Long etapId;
	private Long zasobId;
	
	
	public SupplierOrderPosition() {
		super();
	}

	
    
    public SupplierOrderPosition(long id, Long supplier_order_id,
			String supplier_order_idm, String wytwor_idm, Float ilosc,
			Integer nrpoz, Float cena, Float cenabrutto, Float koszt,
			Float kwotnett, Float kwotvat) {
		super();
		this.id = id;
		this.supplier_order_id = supplier_order_id;
		this.supplier_order_idm = supplier_order_idm;
		this.wytwor_idm = wytwor_idm;
		this.ilosc = ilosc;
		this.nrpoz = nrpoz;
		this.cena = cena;
		this.cenabrutto = cenabrutto;
		this.koszt = koszt;
		this.kwotnett = kwotnett;
		this.kwotvat = kwotvat;
	}



	public SupplierOrderPosition(Element el,String supplier_order_idm, Long supplier_order_id) {
		this.supplier_order_id = supplier_order_id;
		this.supplier_order_idm = supplier_order_idm;
		this.wytwor_idm = el.element("wytwor_id").element("idm").getTextTrim();
		this.ilosc = Float.parseFloat(el.element("ilosc").getTextTrim());
		this.nrpoz = Integer.parseInt(el.element("nrpoz").getTextTrim());
		this.cena = Float.parseFloat(el.element("cena").getTextTrim());;
		this.cenabrutto = Float.parseFloat(el.element("cenabrutto").getTextTrim());
		if (el.element("koszt") != null) {
			this.koszt = Float.parseFloat(el.element("koszt").getTextTrim());
		}
		this.kwotnett = Float.parseFloat(el.element("kwotnett").getTextTrim());
		this.kwotvat = Float.parseFloat(el.element("kwotvat").getTextTrim());
		this.zamdospoz_id = Long.parseLong(el.element("zamdospoz_id").getTextTrim());
		if (el.element("budzet_id") != null && el.element("budzet_id").element("id") != null) {
			this.budzetId = Long.parseLong(el.element("budzet_id").element("id").getTextTrim());
		}
		if (el.element("budzet_id") != null && el.element("budzet_id").element("idm") != null) {
			this.budzetIdm = el.element("budzet_id").element("idm").getTextTrim();
		}
		if (el.element("bd_budzet_ko_id") != null && el.element("bd_budzet_ko_id").element("id") != null) {
			this.bdBudzetKoId = Long.parseLong(el.element("bd_budzet_ko_id").element("id").getTextTrim());
		}
		if (el.element("bd_budzet_ko_id") != null && el.element("bd_budzet_ko_id").element("idm") != null) {
			this.bdBudzetKoIdm = el.element("bd_budzet_ko_id").element("idm").getTextTrim();
		}
		if (el.element("bd_rodzaj_ko_id") != null) {
			this.bdRodzajKoId = Long.parseLong(el.element("bd_rodzaj_ko_id").getTextTrim());
		}
		if (el.element("bd_plan_zam_id") != null && el.element("bd_plan_zam_id").element("id") != null) {
			this.bdPlanZamId = Long.parseLong(el.element("bd_plan_zam_id").element("id").getTextTrim());
		}
		if (el.element("bd_plan_zam_id") != null && el.element("bd_plan_zam_id").element("idm") != null) {
			this.bdPlanZamIdm = el.element("bd_plan_zam_id").element("idm").getTextTrim();
		}
		if (el.element("bd_rodzaj_plan_zam_id") != null) {
			this.bdRodzajPlanZamId = Long.parseLong(el.element("bd_rodzaj_plan_zam_id").getTextTrim());
		}
		if (el.element("bd_rezerwacja_id") != null && el.element("bd_rezerwacja_id").element("id") != null) {
			this.bdRezerwacjaId = Long.parseLong(el.element("bd_rezerwacja_id").element("id").getTextTrim());
		}
		if (el.element("bd_rezerwacja_id") != null && el.element("bd_rezerwacja_id").element("idm") != null) {
			this.bdRezerwacjaIdm = el.element("bd_rezerwacja_id").element("idm").getTextTrim();
		}
		if (el.element("bd_rezerwacja_poz_id") != null) {
			this.bdRezerwacjaPozId = Long.parseLong(el.element("bd_rezerwacja_poz_id").getTextTrim());
		}
		if (el.element("kontrakt_id") != null && StringUtils.isNotBlank(el.element("kontrakt_id").getTextTrim())) {
			this.kontraktId = Long.parseLong(el.element("kontrakt_id").getTextTrim());
		}
		if (el.element("projekt_id") != null) {
			this.projektId = Long.parseLong(el.element("projekt_id").getTextTrim());
		}
		if (el.element("etap_id") != null) {
			this.etapId = Long.parseLong(el.element("etap_id").getTextTrim());
		}
		if (el.element("zasob_id") != null) {
			this.zasobId = Long.parseLong(el.element("zasob_id").getTextTrim());
		}
	}
	
	public void setValueFromElement(Element el,String supplier_order_idm, Long supplier_order_id) 
	{
		this.supplier_order_id = supplier_order_id;
		this.supplier_order_idm = supplier_order_idm;
		this.wytwor_idm = el.element("wytwor_id").element("idm").getTextTrim();
		this.ilosc = Float.parseFloat(el.element("ilosc").getTextTrim());
		this.nrpoz = Integer.parseInt(el.element("nrpoz").getTextTrim());
		this.cena = Float.parseFloat(el.element("cena").getTextTrim());;
		this.cenabrutto = Float.parseFloat(el.element("cenabrutto").getTextTrim());
		this.koszt = Float.parseFloat(el.element("koszt").getTextTrim());
		this.kwotnett = Float.parseFloat(el.element("kwotnett").getTextTrim());
		this.kwotvat = Float.parseFloat(el.element("kwotvat").getTextTrim());
		this.zamdospoz_id = Long.parseLong(el.element("zamdospoz_id").getTextTrim());
		if (el.element("budzet_id") != null && el.element("budzet_id").element("id") != null) {
			this.budzetId = Long.parseLong(el.element("budzet_id").element("id").getTextTrim());
		}
		if (el.element("budzet_id") != null && el.element("budzet_id").element("idm") != null) {
			this.budzetIdm = el.element("budzet_id").element("idm").getTextTrim();
		}
		if (el.element("bd_budzet_ko_id") != null && el.element("bd_budzet_ko_id").element("id") != null) {
			this.bdBudzetKoId = Long.parseLong(el.element("bd_budzet_ko_id").element("id").getTextTrim());
		}
		if (el.element("bd_budzet_ko_id") != null && el.element("bd_budzet_ko_id").element("idm") != null) {
			this.bdBudzetKoIdm = el.element("bd_budzet_ko_id").element("idm").getTextTrim();
		}
		if (el.element("bd_rodzaj_ko_id") != null) {
			this.bdRodzajKoId = Long.parseLong(el.element("bd_rodzaj_ko_id").getTextTrim());
		}
		if (el.element("bd_plan_zam_id") != null && el.element("bd_plan_zam_id").element("id") != null) {
			this.bdPlanZamId = Long.parseLong(el.element("bd_plan_zam_id").element("id").getTextTrim());
		}
		if (el.element("bd_plan_zam_id") != null && el.element("bd_plan_zam_id").element("idm") != null) {
			this.bdPlanZamIdm = el.element("bd_plan_zam_id").element("idm").getTextTrim();
		}
		if (el.element("bd_rodzaj_plan_zam_id") != null) {
			this.bdRodzajPlanZamId = Long.parseLong(el.element("bd_rodzaj_plan_zam_id").getTextTrim());
		}
		if (el.element("bd_rezerwacja_id") != null && el.element("bd_rezerwacja_id").element("id") != null) {
			this.bdRezerwacjaId = Long.parseLong(el.element("bd_rezerwacja_id").element("id").getTextTrim());
		}
		if (el.element("bd_rezerwacja_id") != null && el.element("bd_rezerwacja_id").element("idm") != null) {
			this.bdRezerwacjaIdm = el.element("bd_rezerwacja_id").element("idm").getTextTrim();
		}
		if (el.element("bd_rezerwacja_poz_id") != null) {
			this.bdRezerwacjaPozId = Long.parseLong(el.element("bd_rezerwacja_poz_id").getTextTrim());
		}
		if (el.element("kontrakt_id") != null  && StringUtils.isNotBlank(el.element("kontrakt_id").getTextTrim())) {
			this.kontraktId = Long.parseLong(el.element("kontrakt_id").getTextTrim());
		}
		if (el.element("projekt_id") != null) {
			this.projektId = Long.parseLong(el.element("projekt_id").getTextTrim());
		}
		if (el.element("etap_id") != null) {
			this.etapId = Long.parseLong(el.element("etap_id").getTextTrim());
		}
		if (el.element("zasob_id") != null) {
			this.zasobId = Long.parseLong(el.element("zasob_id").getTextTrim());
		}
	}



	public void save() throws EdmException {
    	try {
    		DSApi.context().session().save(this);
    	} catch (HibernateException e) {
    		new EdmException(e);
    	}
    }


	public String getSupplier_order_idm() {
		return supplier_order_idm;
	}

	public void setSupplier_order_idm(String supplier_order_idm) {
		this.supplier_order_idm = supplier_order_idm;
	}

	public String getWytwor_idm() {
		return wytwor_idm;
	}

	public void setWytwor_idm(String wytwor_idm) {
		this.wytwor_idm = wytwor_idm;
	}

	public Float getIlosc() {
		return ilosc;
	}

	public void setIlosc(Float ilosc) {
		this.ilosc = ilosc;
	}

	public Integer getNrpoz() {
		return nrpoz;
	}

	public void setNrpoz(Integer nrpoz) {
		this.nrpoz = nrpoz;
	}

	public Float getCena() {
		return cena;
	}

	public void setCena(Float cena) {
		this.cena = cena;
	}

	public Float getCenabrutto() {
		return cenabrutto;
	}

	public void setCenabrutto(Float cenabrutto) {
		this.cenabrutto = cenabrutto;
	}

	public Float getKoszt() {
		return koszt;
	}

	public void setKoszt(Float koszt) {
		this.koszt = koszt;
	}

	public Float getKwotnett() {
		return kwotnett;
	}

	public void setKwotnett(Float kwotnett) {
		this.kwotnett = kwotnett;
	}

	public Float getKwotvat() {
		return kwotvat;
	}

	public void setKwotvat(Float kwotvat) {
		this.kwotvat = kwotvat;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getSupplier_order_id() {
		return supplier_order_id;
	}

	public void setSupplier_order_id(long supplier_order_id) {
		this.supplier_order_id = supplier_order_id;
	}

	public long getZamdospoz_id() {
		return zamdospoz_id;
	}

	public void setZamdospoz_id(long zamdospoz_id) {
		this.zamdospoz_id = zamdospoz_id;
	}

	public Long getBudzetId() {
		return budzetId;
	}

	public void setBudzetId(Long budzetId) {
		this.budzetId = budzetId;
	}

	public String getBudzetIdm() {
		return budzetIdm;
	}

	public void setBudzetIdm(String budzetIdm) {
		this.budzetIdm = budzetIdm;
	}

	public Long getBdBudzetKoId() {
		return bdBudzetKoId;
	}

	public void setBdBudzetKoId(Long bdBudzetKoId) {
		this.bdBudzetKoId = bdBudzetKoId;
	}

	public String getBdBudzetKoIdm() {
		return bdBudzetKoIdm;
	}

	public void setBdBudzetKoIdm(String bdBudzetKoIdm) {
		this.bdBudzetKoIdm = bdBudzetKoIdm;
	}

	public Long getBdRodzajKoId() {
		return bdRodzajKoId;
	}

	public void setBdRodzajKoId(Long bdRodzajKoId) {
		this.bdRodzajKoId = bdRodzajKoId;
	}

	public Long getBdPlanZamId() {
		return bdPlanZamId;
	}

	public void setBdPlanZamId(Long bdPlanZamId) {
		this.bdPlanZamId = bdPlanZamId;
	}

	public String getBdPlanZamIdm() {
		return bdPlanZamIdm;
	}

	public void setBdPlanZamIdm(String bdPlanZamIdm) {
		this.bdPlanZamIdm = bdPlanZamIdm;
	}

	public Long getBdRodzajPlanZamId() {
		return bdRodzajPlanZamId;
	}

	public void setBdRodzajPlanZamId(Long bdRodzajPlanZamId) {
		this.bdRodzajPlanZamId = bdRodzajPlanZamId;
	}

	public Long getBdRezerwacjaId() {
		return bdRezerwacjaId;
	}

	public void setBdRezerwacjaId(Long bdRezerwacjaId) {
		this.bdRezerwacjaId = bdRezerwacjaId;
	}

	public String getBdRezerwacjaIdm() {
		return bdRezerwacjaIdm;
	}

	public void setBdRezerwacjaIdm(String bdRezerwacjaIdm) {
		this.bdRezerwacjaIdm = bdRezerwacjaIdm;
	}

	public Long getBdRezerwacjaPozId() {
		return bdRezerwacjaPozId;
	}

	public void setBdRezerwacjaPozId(Long bdRezerwacjaPozId) {
		this.bdRezerwacjaPozId = bdRezerwacjaPozId;
	}

	public Long getKontraktId() {
		return kontraktId;
	}

	public void setKontraktId(Long kontraktId) {
		this.kontraktId = kontraktId;
	}

	public Long getProjektId() {
		return projektId;
	}

	public void setProjektId(Long projektId) {
		this.projektId = projektId;
	}

	public Long getEtapId() {
		return etapId;
	}

	public void setEtapId(Long etapId) {
		this.etapId = etapId;
	}

	public Long getZasobId() {
		return zasobId;
	}

	public void setZasobId(Long zasobId) {
		this.zasobId = zasobId;
	}

}
