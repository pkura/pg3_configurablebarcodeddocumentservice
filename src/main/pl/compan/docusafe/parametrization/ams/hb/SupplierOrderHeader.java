/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import java.util.Date;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class SupplierOrderHeader implements java.io.Serializable {
	private long id;
	private Date datDok;
	private Date datOdb;
	private double dostawcaId;
	private String dostawcaIdn;
	private double idFromWebservice;
	private String idm;
	private String opisWewn;
	private String uwagi;
	private double wartdok;
	
	public SupplierOrderHeader() {
		
	}

	public SupplierOrderHeader(Date datDok, Date datOdb, double dostawcaId,
			String dostawcaIdn, double idFromWebservice, String idm,
			String opisWewn, String uwagi, double wartdok) {
		super();
		this.datDok = datDok;
		this.datOdb = datOdb;
		this.dostawcaId = dostawcaId;
		this.dostawcaIdn = dostawcaIdn;
		this.idFromWebservice = idFromWebservice;
		this.idm = idm;
		this.opisWewn = opisWewn;
		this.uwagi = uwagi;
		this.wartdok = wartdok;
	}

    public void setAllFieldsFromServiceObject(pl.compan.docusafe.ws.ams.DictionaryServiceStub.SupplierOrderHeader item) {
    	setDatDok(item.getDatdok());
    	setDatOdb(item.getDatodb());
    	setDostawcaId(item.getDostawca_id());
    	setDostawcaIdn(item.getDostawca_idn());
    	setIdFromWebservice(item.getId());
    	setIdm(item.getIdm());
    	setOpisWewn(item.getOpis_wewn());
    	setUwagi(item.getUwagi());
    	setWartdok(item.getWartdok());
    }
    
    public void save() throws EdmException {
    	try {
    		DSApi.context().session().save(this);
    	} catch (HibernateException e) {
    		new EdmException(e);
    	}
    }
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((datDok == null) ? 0 : datDok.hashCode());
		result = prime * result + ((datOdb == null) ? 0 : datOdb.hashCode());
		long temp;
		temp = Double.doubleToLongBits(dostawcaId);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result
				+ ((dostawcaIdn == null) ? 0 : dostawcaIdn.hashCode());
		temp = Double.doubleToLongBits(idFromWebservice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((idm == null) ? 0 : idm.hashCode());
		result = prime * result
				+ ((opisWewn == null) ? 0 : opisWewn.hashCode());
		result = prime * result + ((uwagi == null) ? 0 : uwagi.hashCode());
		temp = Double.doubleToLongBits(wartdok);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof SupplierOrderHeader)) {
			return false;
		}
		SupplierOrderHeader other = (SupplierOrderHeader) obj;
		if (datDok == null) {
			if (other.datDok != null) {
				return false;
			}
		} else if (!datDok.equals(other.datDok)) {
			return false;
		}
		if (datOdb == null) {
			if (other.datOdb != null) {
				return false;
			}
		} else if (!datOdb.equals(other.datOdb)) {
			return false;
		}
		if (Double.doubleToLongBits(dostawcaId) != Double
				.doubleToLongBits(other.dostawcaId)) {
			return false;
		}
		if (dostawcaIdn == null) {
			if (other.dostawcaIdn != null) {
				return false;
			}
		} else if (!dostawcaIdn.equals(other.dostawcaIdn)) {
			return false;
		}
		if (Double.doubleToLongBits(idFromWebservice) != Double
				.doubleToLongBits(other.idFromWebservice)) {
			return false;
		}
		if (idm == null) {
			if (other.idm != null) {
				return false;
			}
		} else if (!idm.equals(other.idm)) {
			return false;
		}
		if (opisWewn == null) {
			if (other.opisWewn != null) {
				return false;
			}
		} else if (!opisWewn.equals(other.opisWewn)) {
			return false;
		}
		if (uwagi == null) {
			if (other.uwagi != null) {
				return false;
			}
		} else if (!uwagi.equals(other.uwagi)) {
			return false;
		}
		if (Double.doubleToLongBits(wartdok) != Double
				.doubleToLongBits(other.wartdok)) {
			return false;
		}
		return true;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDatDok() {
		return datDok;
	}

	public void setDatDok(Date datDok) {
		this.datDok = datDok;
	}

	public Date getDatOdb() {
		return datOdb;
	}

	public void setDatOdb(Date datOdb) {
		this.datOdb = datOdb;
	}

	public double getDostawcaId() {
		return dostawcaId;
	}

	public void setDostawcaId(double dostawcaId) {
		this.dostawcaId = dostawcaId;
	}

	public String getDostawcaIdn() {
		return dostawcaIdn;
	}

	public void setDostawcaIdn(String dostawcaIdn) {
		this.dostawcaIdn = dostawcaIdn;
	}

	public double getIdFromWebservice() {
		return idFromWebservice;
	}

	public void setIdFromWebservice(double idFromWebservice) {
		this.idFromWebservice = idFromWebservice;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public String getOpisWewn() {
		return opisWewn;
	}

	public void setOpisWewn(String opisWewn) {
		this.opisWewn = opisWewn;
	}

	public String getUwagi() {
		return uwagi;
	}

	public void setUwagi(String uwagi) {
		this.uwagi = uwagi;
	}

	public double getWartdok() {
		return wartdok;
	}

	public void setWartdok(double wartdok) {
		this.wartdok = wartdok;
	}
	
}
