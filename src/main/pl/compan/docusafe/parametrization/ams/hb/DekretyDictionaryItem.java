/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
@Entity
@Table(name = DekretyDictionaryItem.TABLE_NAME)
public class DekretyDictionaryItem implements java.io.Serializable {
	
	public static final String TABLE_NAME = "dsg_ams_dekrety_dictionary";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "numer_pozycji")
	private Integer numerPozycji;
	@Column(name = "zlecenie")
	private Long zlecenie;
	@Column(name = "produkt")
	private Long produkt;
	@Column(name = "podzial_kosztow")
	private Long podzialKosztow;
	@Column(name = "sprzedaz_opodatkowana")
	private Long sprzedazOpodatkowana;
	@Column(name = "mpk")
	private Long mpk;
	@Column(name = "netto")
	private BigDecimal netto;
	@Column(name = "stawka")
	private Long stawka;
	@Column(name = "kwota_vat")
	private BigDecimal kwotaVat;
	@Column(name = "kwota_vat_do_odliczenia")
	private BigDecimal kwotaVatDoOdliczenia;
	@Column(name = "kwota_vat_koszty")
	private BigDecimal kwotaVatKoszty;
	@Column(name = "brutto")
	private BigDecimal brutto;
	@Column(name = "magazynowany")
	private Boolean magazynowany;
	@Column(name = "srodek_trwaly")
	private Boolean srodekTrwaly;
	@Column(name = "dsd_repo116_mtf_zrodlo_finansowania")
	private Long zrodloFinansowania;
	
	public DekretyDictionaryItem() {
		
	}

	public DekretyDictionaryItem(Integer numerPozycji, Long zlecenie,
			Long produkt, Long podzialKosztow, Long sprzedazOpodatkowana,
			Long mpk, BigDecimal netto, Long stawka, BigDecimal kwotaVat,
			BigDecimal kwotaVatDoOdliczenia, BigDecimal kwotaVatKoszty,
			BigDecimal brutto, Boolean magazynowany, Boolean srodekTrwaly,
			Long zrodloFinansowania) {
		super();
		this.numerPozycji = numerPozycji;
		this.zlecenie = zlecenie;
		this.produkt = produkt;
		this.podzialKosztow = podzialKosztow;
		this.sprzedazOpodatkowana = sprzedazOpodatkowana;
		this.mpk = mpk;
		this.netto = netto;
		this.stawka = stawka;
		this.kwotaVat = kwotaVat;
		this.kwotaVatDoOdliczenia = kwotaVatDoOdliczenia;
		this.kwotaVatKoszty = kwotaVatKoszty;
		this.brutto = brutto;
		this.magazynowany = magazynowany;
		this.srodekTrwaly = srodekTrwaly;
		this.zrodloFinansowania = zrodloFinansowania;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNumerPozycji() {
		return numerPozycji;
	}

	public void setNumerPozycji(Integer numerPozycji) {
		this.numerPozycji = numerPozycji;
	}

	public Long getZlecenie() {
		return zlecenie;
	}

	public void setZlecenie(Long zlecenie) {
		this.zlecenie = zlecenie;
	}

	public Long getProdukt() {
		return produkt;
	}

	public void setProdukt(Long produkt) {
		this.produkt = produkt;
	}

	public Long getPodzialKosztow() {
		return podzialKosztow;
	}

	public void setPodzialKosztow(Long podzialKosztow) {
		this.podzialKosztow = podzialKosztow;
	}

	public Long getSprzedazOpodatkowana() {
		return sprzedazOpodatkowana;
	}

	public void setSprzedazOpodatkowana(Long sprzedazOpodatkowana) {
		this.sprzedazOpodatkowana = sprzedazOpodatkowana;
	}

	public Long getMpk() {
		return mpk;
	}

	public void setMpk(Long mpk) {
		this.mpk = mpk;
	}

	public BigDecimal getNetto() {
		return netto;
	}

	public void setNetto(BigDecimal netto) {
		this.netto = netto;
	}

	public Long getStawka() {
		return stawka;
	}

	public void setStawka(Long stawka) {
		this.stawka = stawka;
	}

	public BigDecimal getKwotaVat() {
		return kwotaVat;
	}

	public void setKwotaVat(BigDecimal kwotaVat) {
		this.kwotaVat = kwotaVat;
	}

	public BigDecimal getKwotaVatDoOdliczenia() {
		return kwotaVatDoOdliczenia;
	}

	public void setKwotaVatDoOdliczenia(BigDecimal kwotaVatDoOdliczenia) {
		this.kwotaVatDoOdliczenia = kwotaVatDoOdliczenia;
	}

	public BigDecimal getKwotaVatKoszty() {
		return kwotaVatKoszty;
	}

	public void setKwotaVatKoszty(BigDecimal kwotaVatKoszty) {
		this.kwotaVatKoszty = kwotaVatKoszty;
	}

	public BigDecimal getBrutto() {
		return brutto;
	}

	public void setBrutto(BigDecimal brutto) {
		this.brutto = brutto;
	}

	public Boolean getMagazynowany() {
		return magazynowany;
	}

	public void setMagazynowany(Boolean magazynowany) {
		this.magazynowany = magazynowany;
	}

	public Boolean getSrodekTrwaly() {
		return srodekTrwaly;
	}

	public void setSrodekTrwaly(Boolean srodekTrwaly) {
		this.srodekTrwaly = srodekTrwaly;
	}

	public Long getZrodloFinansowania() {
		return zrodloFinansowania;
	}

	public void setZrodloFinansowania(Long zrodloFinansowania) {
		this.zrodloFinansowania = zrodloFinansowania;
	}
	
}
