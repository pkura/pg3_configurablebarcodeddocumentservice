/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
@Entity
@Table(name = ReservationPositionDictionaryItem.TABLE_NAME)
public class ReservationPositionDictionaryItem implements java.io.Serializable {

	public static final String TABLE_NAME = "dsg_ams_reservation_position_dict";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "rezerwacja_idm", length=100)
	private String rezerwacjaIdm;
	@Column(name = "nr_poz_rez")
	private Integer nrPozRez;
	@Column(name = "wytwor_rez", length=100)
	private String wytworRez;
	@Column(name = "wytwor_nazwa_rez", length=150)
	private String wytworNazwaRez;
	@Column(name = "ilosc_rez")
	private Double iloscRez;
	@Column(name = "cena_rez")
	private Double cenaRez;
	@Column(name = "cena_brutto_rez")
	private Double cenaBruttoRez;
	@Column(name = "koszt_rez")
	private Double kosztRez;
	@Column(name = "kwota_netto_rez")
	private Double kwotaNettoRez;
	@Column(name = "kwota_vat_rez")
	private Double kwotaVatRez;

	public ReservationPositionDictionaryItem() {
		
	}

	public ReservationPositionDictionaryItem(String rezerwacjaIdm,
			Integer nrPozRez, String wytworRez, String wytworNazwaRez,
			Double iloscRez, Double cenaRez, Double cenaBruttoRez, Double kosztRez,
			Double kwotaNettoRez, Double kwotaVatRez) {
		super();
		this.rezerwacjaIdm = rezerwacjaIdm;
		this.nrPozRez = nrPozRez;
		this.wytworRez = wytworRez;
		this.wytworNazwaRez = wytworNazwaRez;
		this.iloscRez = iloscRez;
		this.cenaRez = cenaRez;
		this.cenaBruttoRez = cenaBruttoRez;
		this.kosztRez = kosztRez;
		this.kwotaNettoRez = kwotaNettoRez;
		this.kwotaVatRez = kwotaVatRez;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRezerwacjaIdm() {
		return rezerwacjaIdm;
	}

	public void setRezerwacjaIdm(String rezerwacjaIdm) {
		this.rezerwacjaIdm = rezerwacjaIdm;
	}

	public Integer getNrPozRez() {
		return nrPozRez;
	}

	public void setNrPozRez(Integer nrPozRez) {
		this.nrPozRez = nrPozRez;
	}

	public String getWytworRez() {
		return wytworRez;
	}

	public void setWytworRez(String wytworRez) {
		this.wytworRez = wytworRez;
	}

	public String getWytworNazwaRez() {
		return wytworNazwaRez;
	}

	public void setWytworNazwaRez(String wytworNazwaRez) {
		this.wytworNazwaRez = wytworNazwaRez;
	}

	public Double getIloscRez() {
		return iloscRez;
	}

	public void setIloscRez(Double iloscRez) {
		this.iloscRez = iloscRez;
	}

	public Double getCenaRez() {
		return cenaRez;
	}

	public void setCenaRez(Double cenaRez) {
		this.cenaRez = cenaRez;
	}

	public Double getCenaBruttoRez() {
		return cenaBruttoRez;
	}

	public void setCenaBruttoRez(Double cenaBruttoRez) {
		this.cenaBruttoRez = cenaBruttoRez;
	}

	public Double getKosztRez() {
		return kosztRez;
	}

	public void setKosztRez(Double kosztRez) {
		this.kosztRez = kosztRez;
	}

	public Double getKwotaNettoRez() {
		return kwotaNettoRez;
	}

	public void setKwotaNettoRez(Double kwotaNettoRez) {
		this.kwotaNettoRez = kwotaNettoRez;
	}

	public Double getKwotaVatRez() {
		return kwotaVatRez;
	}

	public void setKwotaVatRez(Double kwotaVatRez) {
		this.kwotaVatRez = kwotaVatRez;
	}
	
}
