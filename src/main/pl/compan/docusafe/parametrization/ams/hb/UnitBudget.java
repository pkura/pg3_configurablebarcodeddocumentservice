/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 27-03-2014
 * ds_trunk_2014
 * UnitBudget.java
 */
@Entity
@Table(name = UnitBudget.TABLE_NAME)
public class UnitBudget implements java.io.Serializable {
    public static final String TABLE_NAME = "dsg_ams_unit_budget";
    public static final String UNIT_BUDGET_VIEW = "dsg_ams_view_budget_phase";
    public static final String UNIT_BUDGET_NO_REF_VIEW = "dsg_ams_view_budget_phase_no_ref";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "dsg_ams_unit_budget_seq")
    @SequenceGenerator(name = "dsg_ams_unit_budget_seq", sequenceName = "dsg_ams_unit_budget_seq")
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "ERP_ID", nullable = false)
    private Long erpId;
    @Column(name="IDM", length = 50)
    private String idm;
    @Column(name="NAZWA", length = 50)
    private String nazwa;
    @Column(name="CZY_ARCHIWALNY")
    private Double czyArchiwalny;
    @Column(name="DATA_OB_DO")
    private Date dataObowDo;
    @Column(name="DATA_OB_OD")
    private Date dataObOd;
    @Column(name="WERSJA")
    private Integer wersja;

    
    public static UnitBudget find(Integer id) throws EdmException {
        return Finder.find(UnitBudget.class, id);
    }

    public UnitBudget() {
    }

    public void setAllFieldsFromServiceObject(DictionaryServiceStub.UnitBudget item) {
    	erpId = (long)item.getId();
    	idm = item.getIdm();
    	nazwa = item.getNazwa();
    	czyArchiwalny = item.getCzy_archiwalny();
    	dataObowDo = item.getData_obow_do();
    	dataObOd = item.getData_obow_od();
    	wersja = item.getWersja();
    }

    public void save() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public Long getId() {
        return id;
    }

	public String getIdm() {
		return idm;
	}

    public Long getErpId() {
        return erpId;
    }

    public void setErpId(Long erpId) {
        this.erpId = erpId;
    }

    public void setIdm(String idm) {
		this.idm = idm;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Double getCzyArchiwalny() {
		return czyArchiwalny;
	}

	public void setCzyArchiwalny(Double czyArchiwalny) {
		this.czyArchiwalny = czyArchiwalny;
	}

	public Date getDataObowDo() {
		return dataObowDo;
	}

	public void setDataObowDo(Date dataObowDo) {
		this.dataObowDo = dataObowDo;
	}

	public Date getDataObOd() {
		return dataObOd;
	}

	public void setDataObOd(Date dataObOd) {
		this.dataObOd = dataObOd;
	}

	public Integer getWersja() {
		return wersja;
	}

	public void setWersja(Integer wersja) {
		this.wersja = wersja;
	}

}



