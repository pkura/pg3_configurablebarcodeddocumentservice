/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import org.hibernate.HibernateException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;

import javax.persistence.*;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 27-03-2014
 * ds_trunk_2014
 * PurchasePlan.java
 */
@Entity
@Table(name = PurchasePlan.TABLE_NAME)
public class PurchasePlan implements java.io.Serializable {

    public static final String TABLE_NAME = "dsg_ams_purchase_plan";
    public static final String PURCHASE_PLAN_VIEW = "dsg_ams_view_purchase_plan";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "dsg_ams_purchase_plan_seq")
    @SequenceGenerator(name = "dsg_ams_purchase_plan_seq", sequenceName = "dsg_ams_purchase_plan_seq")
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "ERP_ID", nullable = false)
    private Long erpId;
    @Column(name="IDM", length = 50)
    private String idm;
    @Column(name="NAZWA", length = 50)
    private String nazwa;
    @Column(name="BD_STR_BUDZET_ID")
    private Double bdStrBudzetId;
    @Column(name="BD_STR_BUDZET_IDN", length = 50)
    private String bdStrBudzetIdn;
    @Column(name="BUDZET_ID")
    private Double budzetId;
    @Column(name="BUDZET_IDM", length = 50)
    private String budzetIdm;
    @Column(name="CZY_AGREGAT")
    private Integer czyAgregat;
    

    
    public static PurchasePlan find(Integer id) throws EdmException {
        return Finder.find(PurchasePlan.class, id);
    }

    public PurchasePlan() {
    }

    public void setAllFieldsFromServiceObject(DictionaryServiceStub.PurchasePlan item) {
        erpId = (long)item.getId();
    	bdStrBudzetId = item.getBd_str_budzet_id();
    	bdStrBudzetId = item.getBd_str_budzet_id();
    	bdStrBudzetIdn = item.getBd_str_budzet_idn();
    	budzetId = item.getBudzet_id();
    	budzetIdm = item.getBudzet_idm();
    	czyAgregat = item.getCzy_agregat();
    	idm = item.getIdm();
    	nazwa = item.getNazwa();
    }

    public void save() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public Long getId() {
        return id;
    }

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Double getBdStrBudzetId() {
		return bdStrBudzetId;
	}

	public void setBdStrBudzetId(Double bdStrBudzetId) {
		this.bdStrBudzetId = bdStrBudzetId;
	}

	public String getBdStrBudzetIdn() {
		return bdStrBudzetIdn;
	}

	public void setBdStrBudzetIdn(String bdStrBudzetIdn) {
		this.bdStrBudzetIdn = bdStrBudzetIdn;
	}

	public Double getBudzetId() {
		return budzetId;
	}

	public void setBudzetId(Double budzetId) {
		this.budzetId = budzetId;
	}

	public String getBudzetIdm() {
		return budzetIdm;
	}

	public void setBudzetIdm(String budzetIdm) {
		this.budzetIdm = budzetIdm;
	}

	public Integer getCzyAgregat() {
		return czyAgregat;
	}

	public void setCzyAgregat(Integer czyAgregat) {
		this.czyAgregat = czyAgregat;
	}

    public Long getErpId() {
        return erpId;
    }

    public void setErpId(Long erpId) {
        this.erpId = erpId;
    }
}



