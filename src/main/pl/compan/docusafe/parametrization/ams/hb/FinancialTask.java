/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class FinancialTask implements java.io.Serializable {
	private long id;
	private double erpId;
	private double numericId;
	private String idm;
	private String sourceName;
	
	public FinancialTask() {
		
	}
	
	public FinancialTask(double erpId, double numericId, String idm, String sourceName) {
		this.erpId = erpId;
		this.numericId = numericId;
		this.idm = idm;
		this.sourceName = sourceName;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public double getErpId() {
		return erpId;
	}
	public void setErpId(double erpId) {
		this.erpId = erpId;
	}
	public double getNumericId() {
		return numericId;
	}
	public void setNumericId(double numericId) {
		this.numericId = numericId;
	}
	public String getIdm() {
		return idm;
	}
	public void setIdm(String idm) {
		this.idm = idm;
	}
	public String getSourceName() {
		return sourceName;
	}
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

    public void setAllFieldsFromServiceObject(pl.compan.docusafe.ws.ams.DictionaryServiceStub.FinancialTask item) {
    	setErpId(item.getId());
    	setNumericId(Double.parseDouble(item.getIdentyfikator_num()));
    	setIdm(item.getIdm());
    	setSourceName(item.getZrodlo_nazwa());
    }
    
    public void save() throws EdmException {
    	try {
    		DSApi.context().session().save(this);
    	} catch (HibernateException e) {
    		new EdmException(e);
    	}
    }

}
