/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class Warehouse implements java.io.Serializable {
	private long id;
	private double czyAktywna;
	private double czyBilansowy;
	private double idFromWebservice;
	private double identyfikatorNum;
	private String idm;
	private String magazynIds;
	private String nazwa;
	private double pracownikId;
	private boolean available;
	
	public Warehouse() {
		
	}

	public Warehouse(double czyAktywna, double czyBilansowy,
			double idFromWebservice, double identyfikatorNum, String idm,
			String magazynIds, String nazwa, double pracownikId,
			boolean available) {
		super();
		this.czyAktywna = czyAktywna;
		this.czyBilansowy = czyBilansowy;
		this.idFromWebservice = idFromWebservice;
		this.identyfikatorNum = identyfikatorNum;
		this.idm = idm;
		this.magazynIds = magazynIds;
		this.nazwa = nazwa;
		this.pracownikId = pracownikId;
		this.available = available;
	}

    public void setAllFieldsFromServiceObject(pl.compan.docusafe.ws.ams.DictionaryServiceStub.Warehouse item) {
    	setCzyAktywna(item.getCzy_aktywna());
    	setCzyBilansowy(item.getCzy_bilansowy());
    	setIdFromWebservice(item.getId());
    	setIdentyfikatorNum(item.getIdentyfikator_num());
    	setIdm(item.getIdm());
    	setMagazynIds(item.getMagazyn_ids());
    	setNazwa(item.getNazwa());
    	setPracownikId(item.getPracownik_id());
    	setAvailable(new Double(1).equals(item.getCzy_aktywna()) ? true : false);
    }
    
    public void save() throws EdmException {
    	try {
    		DSApi.context().session().save(this);
    	} catch (HibernateException e) {
    		new EdmException(e);
    	}
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (available ? 1231 : 1237);
		long temp;
		temp = Double.doubleToLongBits(czyAktywna);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(czyBilansowy);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(idFromWebservice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(identyfikatorNum);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((idm == null) ? 0 : idm.hashCode());
		result = prime * result
				+ ((magazynIds == null) ? 0 : magazynIds.hashCode());
		result = prime * result + ((nazwa == null) ? 0 : nazwa.hashCode());
		temp = Double.doubleToLongBits(pracownikId);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Warehouse)) {
			return false;
		}
		Warehouse other = (Warehouse) obj;
		if (available != other.available) {
			return false;
		}
		if (Double.doubleToLongBits(czyAktywna) != Double
				.doubleToLongBits(other.czyAktywna)) {
			return false;
		}
		if (Double.doubleToLongBits(czyBilansowy) != Double
				.doubleToLongBits(other.czyBilansowy)) {
			return false;
		}
		if (Double.doubleToLongBits(idFromWebservice) != Double
				.doubleToLongBits(other.idFromWebservice)) {
			return false;
		}
		if (Double.doubleToLongBits(identyfikatorNum) != Double
				.doubleToLongBits(other.identyfikatorNum)) {
			return false;
		}
		if (idm == null) {
			if (other.idm != null) {
				return false;
			}
		} else if (!idm.equals(other.idm)) {
			return false;
		}
		if (magazynIds == null) {
			if (other.magazynIds != null) {
				return false;
			}
		} else if (!magazynIds.equals(other.magazynIds)) {
			return false;
		}
		if (nazwa == null) {
			if (other.nazwa != null) {
				return false;
			}
		} else if (!nazwa.equals(other.nazwa)) {
			return false;
		}
		if (Double.doubleToLongBits(pracownikId) != Double
				.doubleToLongBits(other.pracownikId)) {
			return false;
		}
		return true;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getCzyAktywna() {
		return czyAktywna;
	}

	public void setCzyAktywna(double czyAktywna) {
		this.czyAktywna = czyAktywna;
	}

	public double getCzyBilansowy() {
		return czyBilansowy;
	}

	public void setCzyBilansowy(double czyBilansowy) {
		this.czyBilansowy = czyBilansowy;
	}

	public double getIdFromWebservice() {
		return idFromWebservice;
	}

	public void setIdFromWebservice(double idFromWebservice) {
		this.idFromWebservice = idFromWebservice;
	}

	public double getIdentyfikatorNum() {
		return identyfikatorNum;
	}

	public void setIdentyfikatorNum(double identyfikatorNum) {
		this.identyfikatorNum = identyfikatorNum;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public String getMagazynIds() {
		return magazynIds;
	}

	public void setMagazynIds(String magazynIds) {
		this.magazynIds = magazynIds;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public double getPracownikId() {
		return pracownikId;
	}

	public void setPracownikId(double pracownikId) {
		this.pracownikId = pracownikId;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}
    
}
