/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 27-03-2014
 * ds_trunk_2014
 * BudgetUnitOrganizationUnit.java
 */
@Entity
@Table(name = BudgetUnitOrganizationUnit.TABLE_NAME)
public class BudgetUnitOrganizationUnit implements java.io.Serializable {
    public static final String TABLE_NAME = "dsg_ams_budget_unit_org_unit";
    public static final String BUDGET_UNIT_VIEW = "dsg_ams_view_budget_unit";
    public static final String BUDGET_UNIT_FOR_APPLICATION_VIEW = "dsg_ams_view_budget_unit_no_ref_application";
    public static final String BUDGET_UNIT_NO_REF_VIEW = "dsg_ams_view_budget_unit_no_ref";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "dsg_ams_budget_unit_org_unit_seq")
    @SequenceGenerator(name = "dsg_ams_budget_unit_org_unit_seq", sequenceName = "dsg_ams_budget_unit_org_unit_seq")
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name="BD_STR_BUDZET_ID")
    private Double bdStrBudzetId;
    @Column(name="BD_STR_BUDZET_IDN", length = 50)
    private String bdStrBudzetIdn;
    @Column(name="BD_STR_BUDZET_NAD_ID")
    private Double bdStrBudzetNadId;
    @Column(name="BD_STR_BUDZET_NAD_IDN", length = 50)
    private String bdStrBudzetNadIdn;
    @Column(name="BUDZET_ID")
    private Double budzetId;
    @Column(name="CZY_WIODACA")
    private Double czyWiodaca;
    @Column(name="KOMORKA_ID")
    private Double komorkaId;
    @Column(name="KOMORKA_IDN", length = 50)
    private String komorkaIdn;
    @Column(name="NAZWA_KOMORKI_BUDZETOWEJ", length = 100)
    private String nazwaKomowkiBudzetowej;
    @Column(name="NAZWA_KOMORKI_ORGANIZACYJNEJ", length = 100)
    private String nazwaKomowkiOrganizacyjnej;

    
    public static BudgetUnitOrganizationUnit find(Integer id) throws EdmException {
        return Finder.find(BudgetUnitOrganizationUnit.class, id);
    }

    public BudgetUnitOrganizationUnit() {
    }

    public void setAllFieldsFromServiceObject(DictionaryServiceStub.BudgetUnitOrganizationUnit item) {

    	bdStrBudzetId = item.getBd_str_budzet_id();
    	bdStrBudzetIdn = item.getBd_str_budzet_idn();
    	bdStrBudzetNadId = item.getBd_str_budzet_nad_id();
    	bdStrBudzetNadIdn  = item.getBd_str_budzet_nad_idn();
    	budzetId = item.getBudzet_id();
    	czyWiodaca = item.getCzy_wiodaca();
    	komorkaId = item.getKomorka_id();
    	komorkaIdn = item.getKomorka_idn();
    	nazwaKomowkiBudzetowej = item.getNazwa_komorki_budzetowej();
    	nazwaKomowkiOrganizacyjnej = item.getNazwa_komorki_organizacyjnej();
    }

    public void save() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public Long getId() {
        return id;
    }

	public Double getBdStrBudzetId() {
		return bdStrBudzetId;
	}

	public void setBdStrBudzetId(Double bdStrBudzetId) {
		this.bdStrBudzetId = bdStrBudzetId;
	}

	public String getBdStrBudzetIdn() {
		return bdStrBudzetIdn;
	}

	public void setBdStrBudzetIdn(String bdStrBudzetIdn) {
		this.bdStrBudzetIdn = bdStrBudzetIdn;
	}

	public Double getBdStrBudzetNadId() {
		return bdStrBudzetNadId;
	}

	public void setBdStrBudzetNadId(Double bdStrBudzetNadId) {
		this.bdStrBudzetNadId = bdStrBudzetNadId;
	}

	public String getBdStrBudzetNadIdn() {
		return bdStrBudzetNadIdn;
	}

	public void setBdStrBudzetNadIdn(String bdStrBudzetNadIdn) {
		this.bdStrBudzetNadIdn = bdStrBudzetNadIdn;
	}

	public Double getBudzetId() {
		return budzetId;
	}

	public void setBudzetId(Double budzetId) {
		this.budzetId = budzetId;
	}

	public Double getCzyWiodaca() {
		return czyWiodaca;
	}

	public void setCzyWiodaca(Double czyWiodaca) {
		this.czyWiodaca = czyWiodaca;
	}

	public Double getKomorkaId() {
		return komorkaId;
	}

	public void setKomorkaId(Double komorkaId) {
		this.komorkaId = komorkaId;
	}

	public String getKomorkaIdn() {
		return komorkaIdn;
	}

	public void setKomorkaIdn(String komorkaIdn) {
		this.komorkaIdn = komorkaIdn;
	}

	public String getNazwaKomowkiBudzetowej() {
		return nazwaKomowkiBudzetowej;
	}

	public void setNazwaKomowkiBudzetowej(String nazwaKomowkiBudzetowej) {
		this.nazwaKomowkiBudzetowej = nazwaKomowkiBudzetowej;
	}

	public String getNazwaKomowkiOrganizacyjnej() {
		return nazwaKomowkiOrganizacyjnej;
	}

	public void setNazwaKomowkiOrganizacyjnej(String nazwaKomowkiOrganizacyjnej) {
		this.nazwaKomowkiOrganizacyjnej = nazwaKomowkiOrganizacyjnej;
	}
}


