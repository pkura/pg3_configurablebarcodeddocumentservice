/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
@Entity
@Table(name = SupplierOrderPositionItem.TABLE_NAME)
public class SupplierOrderPositionItem implements java.io.Serializable {
	
	public static final String TABLE_NAME = "dsg_ams_order_position_dict";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "nr_poz")
	private Integer nrPoz;
	@Column(name = "wytwor", length=100)
	private String wytwor;
	@Column(name = "wytwor_nazwa", length=150)
	private String wytworNazwa;
	@Column(name = "ilosc")
	private Float ilosc;
	@Column(name = "cena")
	private Float cena;
	@Column(name = "cena_brutto")
	private Float cenaBrutto;
	@Column(name = "koszt")
	private Float koszt;
	@Column(name = "kwota_netto")
	private Float kwotaNetto;
	@Column(name = "kwota_vat")
	private Float kwotaVat;
	
	public SupplierOrderPositionItem() {
		
	}

	public SupplierOrderPositionItem(Integer nrPoz, String wytwor, String wytworNazwa, Float ilosc,
			Float cena, Float cenaBrutto, Float koszt, Float kwotaNetto,
			Float kwotaVat) {
		super();
		this.nrPoz = nrPoz;
		this.wytwor = wytwor;
		this.wytworNazwa = wytworNazwa;
		this.ilosc = ilosc;
		this.cena = cena;
		this.cenaBrutto = cenaBrutto;
		this.koszt = koszt;
		this.kwotaNetto = kwotaNetto;
		this.kwotaVat = kwotaVat;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNrPoz() {
		return nrPoz;
	}

	public void setNrPoz(Integer nrPoz) {
		this.nrPoz = nrPoz;
	}

	public String getWytwor() {
		return wytwor;
	}

	public void setWytwor(String wytwor) {
		this.wytwor = wytwor;
	}

	public String getWytworNazwa() {
		return wytworNazwa;
	}

	public void setWytworNazwa(String wytworNazwa) {
		this.wytworNazwa = wytworNazwa;
	}
	
	public Float getIlosc() {
		return ilosc;
	}

	public void setIlosc(Float ilosc) {
		this.ilosc = ilosc;
	}

	public Float getCena() {
		return cena;
	}

	public void setCena(Float cena) {
		this.cena = cena;
	}

	public Float getCenaBrutto() {
		return cenaBrutto;
	}

	public void setCenaBrutto(Float cenaBrutto) {
		this.cenaBrutto = cenaBrutto;
	}

	public Float getKoszt() {
		return koszt;
	}

	public void setKoszt(Float koszt) {
		this.koszt = koszt;
	}

	public Float getKwotaNetto() {
		return kwotaNetto;
	}

	public void setKwotaNetto(Float kwotaNetto) {
		this.kwotaNetto = kwotaNetto;
	}

	public Float getKwotaVat() {
		return kwotaVat;
	}

	public void setKwotaVat(Float kwotaVat) {
		this.kwotaVat = kwotaVat;
	}

}
