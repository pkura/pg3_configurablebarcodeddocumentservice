/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
@Entity
@Table(name = AMSProduct.TABLE_NAME)
public class AMSProduct implements java.io.Serializable {
	
	public static final String TABLE_NAME = "dsg_ams_product";
	
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
	private long id;
    @Column(name = "erpId")
	private double erpId;
    @Column(name = "idm", length = 50)
	private String idm;
    @Column(name = "jm_idn", length = 50)
	private String jm_idn;
    @Column(name = "klaswytw_idn", length = 50)
	private String klaswytw_idn;
    @Column(name = "nazwa", length = 150)
	private String nazwa;
    @Column(name = "rodztowaru")
	private int rodztowaru;
    @Column(name = "vatstaw_ids")
	private String vatstaw_ids;
	
    public void setAllFieldsFromServiceObject(pl.compan.docusafe.ws.ams.DictionaryServiceStub.CostInvoiceProduct item) {
    	setErpId(item.getId());
    	setIdm(item.getIdm());
    	setJm_idn(item.getJm_idn());
    	setKlaswytw_idn(item.getKlaswytw_idn());
    	setNazwa(item.getNazwa());
    	setRodztowaru(item.getRodztowaru());
    	setVatstaw_ids(item.getVatstaw_ids());
    }
    
    public void save() throws EdmException {
    	try {
    		DSApi.context().session().save(this);
    	} catch (HibernateException e) {
    		new EdmException(e);
    	}
    }

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getErpId() {
		return erpId;
	}

	public void setErpId(double erpId) {
		this.erpId = erpId;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public String getJm_idn() {
		return jm_idn;
	}

	public void setJm_idn(String jm_idn) {
		this.jm_idn = jm_idn;
	}

	public String getKlaswytw_idn() {
		return klaswytw_idn;
	}

	public void setKlaswytw_idn(String klaswytw_idn) {
		this.klaswytw_idn = klaswytw_idn;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public int getRodztowaru() {
		return rodztowaru;
	}

	public void setRodztowaru(int rodztowaru) {
		this.rodztowaru = rodztowaru;
	}

	public String getVatstaw_ids() {
		return vatstaw_ids;
	}

	public void setVatstaw_ids(String vatstaw_ids) {
		this.vatstaw_ids = vatstaw_ids;
	}
    
}
