/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
@Entity
@Table(name = MPKDictionaryItem.TABLE_NAME)
public class MPKDictionaryItem implements java.io.Serializable {
	
	public static final String TABLE_NAME = "dsg_ams_mpk_dictionary";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "pozycjaid")
	private Integer pozycjaId;
	@Column(name = "okres_budzetowy")
	private Long okresBudzetowy;
	@Column (name = "rodzaj_budzetu")
	private Long rodzajBudzetu;
	@Column(name = "komorka_budzetowa")
	private Long komorkaBudzetowa;
	@Column (name = "identyfikator_planu")
	private Long identyfikatorPlanu;
	@Column (name = "budzet")
	private Long budzet;
	@Column (name = "pozycja_planu_zakupow_raw")
	private String pozycjaPlanuZakupowRaw;
	@Column(name = "pozycja_budzetu_raw")
	private String pozycjaBudzetuRaw;
	@Column (name = "zadanie_finansowe_planu_raw")
	private String zadanieFinansowePlanuRaw;
	@Column (name = "zadanie_finansowe_budzetu_raw")
	private String zadanieFinansoweBudzetuRaw;
	@Column (name = "jednostka_wnioskujaca")
	private Long jednostkaWnioskujaca;
	@Column (name = "projekt")
	private Long projekt;
	@Column (name = "jednostka_miary")
	private Long jednostkaMiary;
	@Column (name = "ilosc_sztuk")
	private Integer iloscSztuk;
	@Column (name = "kwota_budzetu")
	private BigDecimal kwotaBudzetu;
	
	public MPKDictionaryItem() {
		
	}

	public MPKDictionaryItem(Integer pozycjaId, Long okresBudzetowy,
			Long rodzajBudzetu, Long komorkaBudzetowa, Long identyfikatorPlanu,
			Long budzet, String pozycjaPlanuZakupowRaw,
			String pozycjaBudzetuRaw, String zadanieFinansowePlanuRaw,
			String zadanieFinansoweBudzetuRaw, Long jednostkaWnioskujaca,
			Long projekt, Long jednostkaMiary, Integer iloscSztuk,
			BigDecimal kwotaBudzetu) {
		super();
		this.pozycjaId = pozycjaId;
		this.okresBudzetowy = okresBudzetowy;
		this.rodzajBudzetu = rodzajBudzetu;
		this.komorkaBudzetowa = komorkaBudzetowa;
		this.identyfikatorPlanu = identyfikatorPlanu;
		this.budzet = budzet;
		this.pozycjaPlanuZakupowRaw = pozycjaPlanuZakupowRaw;
		this.pozycjaBudzetuRaw = pozycjaBudzetuRaw;
		this.zadanieFinansowePlanuRaw = zadanieFinansowePlanuRaw;
		this.zadanieFinansoweBudzetuRaw = zadanieFinansoweBudzetuRaw;
		this.jednostkaWnioskujaca = jednostkaWnioskujaca;
		this.projekt = projekt;
		this.jednostkaMiary = jednostkaMiary;
		this.iloscSztuk = iloscSztuk;
		this.kwotaBudzetu = kwotaBudzetu;
	}

	public MPKDictionaryItem(Integer pozycjaId, Long okresBudzetowy, Long rodzajBudzetu, Long komorkaBudzetowa, Long identyfikatorPlanu, Long budzet,
			String pozycjaPlanuZakupowRaw, String pozycjaBudzetuRaw, String zadanieFinansowePlanuRaw, String zadanieFinansoweBudzetuRaw) {
		this(pozycjaId, okresBudzetowy, rodzajBudzetu, komorkaBudzetowa, identyfikatorPlanu, budzet, pozycjaPlanuZakupowRaw, pozycjaBudzetuRaw, zadanieFinansowePlanuRaw, zadanieFinansoweBudzetuRaw, null, null, null, null, null);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPozycjaId() {
		return pozycjaId;
	}

	public void setPozycjaId(Integer pozycjaId) {
		this.pozycjaId = pozycjaId;
	}

	public Long getOkresBudzetowy() {
		return okresBudzetowy;
	}

	public void setOkresBudzetowy(Long okresBudzetowy) {
		this.okresBudzetowy = okresBudzetowy;
	}

	public Long getRodzajBudzetu() {
		return rodzajBudzetu;
	}
	
	public void setRodzajBudzetu(Long rodzajBudzetu) {
		this.rodzajBudzetu = rodzajBudzetu;
	}
	
	public Long getKomorkaBudzetowa() {
		return komorkaBudzetowa;
	}

	public void setKomorkaBudzetowa(Long komorkaBudzetowa) {
		this.komorkaBudzetowa = komorkaBudzetowa;
	}

	public Long getIdentyfikatorPlanu() {
		return identyfikatorPlanu;
	}

	public void setIdentyfikatorPlanu(Long identyfikatorPlanu) {
		this.identyfikatorPlanu = identyfikatorPlanu;
	}

	public Long getBudzet() {
		return budzet;
	}

	public void setBudzet(Long budzet) {
		this.budzet = budzet;
	}

	public String getPozycjaPlanuZakupowRaw() {
		return pozycjaPlanuZakupowRaw;
	}

	public void setPozycjaPlanuZakupowRaw(String pozycjaPlanuZakupowRaw) {
		this.pozycjaPlanuZakupowRaw = pozycjaPlanuZakupowRaw;
	}

	public String getPozycjaBudzetuRaw() {
		return pozycjaBudzetuRaw;
	}

	public void setPozycjaBudzetuRaw(String pozycjaBudzetuRaw) {
		this.pozycjaBudzetuRaw = pozycjaBudzetuRaw;
	}

	public String getZadanieFinansowePlanuRaw() {
		return zadanieFinansowePlanuRaw;
	}

	public void setZadanieFinansowePlanuRaw(String zadanieFinansowePlanuRaw) {
		this.zadanieFinansowePlanuRaw = zadanieFinansowePlanuRaw;
	}

	public String getZadanieFinansoweBudzetuRaw() {
		return zadanieFinansoweBudzetuRaw;
	}

	public void setZadanieFinansoweBudzetuRaw(String zadanieFinansoweBudzetuRaw) {
		this.zadanieFinansoweBudzetuRaw = zadanieFinansoweBudzetuRaw;
	}

	public Long getJednostkaWnioskujaca() {
		return jednostkaWnioskujaca;
	}

	public void setJednostkaWnioskujaca(Long jednostkaWnioskujaca) {
		this.jednostkaWnioskujaca = jednostkaWnioskujaca;
	}

	public Long getProjekt() {
		return projekt;
	}

	public void setProjekt(Long projekt) {
		this.projekt = projekt;
	}

	public Long getJednostkaMiary() {
		return jednostkaMiary;
	}

	public void setJednostkaMiary(Long jednostkaMiary) {
		this.jednostkaMiary = jednostkaMiary;
	}

	public Integer getIloscSztuk() {
		return iloscSztuk;
	}

	public void setIloscSztuk(Integer iloscSztuk) {
		this.iloscSztuk = iloscSztuk;
	}

	public BigDecimal getKwotaBudzetu() {
		return kwotaBudzetu;
	}

	public void setKwotaBudzetu(BigDecimal kwotaBudzetu) {
		this.kwotaBudzetu = kwotaBudzetu;
	}

}
