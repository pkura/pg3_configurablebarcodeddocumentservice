/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
@Entity
@Table(name = AMSDepartment.TABLE_NAME)
public class AMSDepartment implements java.io.Serializable {
	
	public static final String TABLE_NAME = "dsg_ams_department";
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "czy_aktywna")
	private Double czyAktywna;
	@Column(name = "erpId")
	private Double erpId;
	@Column(name = "identyfikator_num")
	private Double identyfikatorNum;
	@Column(name = "idm", length = 50)
	private String idm;
	@Column(name = "nazwa", length = 100)
	private String nazwa;
	@Column(name = "nazwa_rozw", length = 100)
	private String nazwaRozw;
	@Column(name = "available")
	private Boolean available;
	
	public AMSDepartment() {
		
	}

	public AMSDepartment(Double czyAktywna, Double erpId,
			Double identyfikatorNum, String idm, String nazwa, String nazwaRozw, Boolean available) {
		super();
		this.czyAktywna = czyAktywna;
		this.erpId = erpId;
		this.identyfikatorNum = identyfikatorNum;
		this.idm = idm;
		this.nazwa = nazwa;
		this.nazwaRozw = nazwaRozw;
		this.available = available;
	}

	public void setAllFieldsFromServiceObject(pl.compan.docusafe.ws.ams.DictionaryServiceStub.Department item) {
		setCzyAktywna(item.getCzy_aktywna());
		setErpId(item.getId());
		setIdentyfikatorNum(item.getIdentyfikator_num());
		setIdm(item.getIdm());
		setNazwa(item.getNazwa());
		setNazwaRozw(item.getNazwa_rozw());
    	setAvailable(new Double(1).equals(item.getCzy_aktywna()) ? true : false);
    }
    
    public void save() throws EdmException {
    	try {
    		DSApi.context().session().save(this);
    	} catch (HibernateException e) {
    		new EdmException(e);
    	}
    }
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getCzyAktywna() {
		return czyAktywna;
	}

	public void setCzyAktywna(Double czyAktywna) {
		this.czyAktywna = czyAktywna;
	}

	public Double getErpId() {
		return erpId;
	}

	public void setErpId(Double erpId) {
		this.erpId = erpId;
	}

	public Double getIdentyfikatorNum() {
		return identyfikatorNum;
	}

	public void setIdentyfikatorNum(Double identyfikatorNum) {
		this.identyfikatorNum = identyfikatorNum;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getNazwaRozw() {
		return nazwaRozw;
	}

	public void setNazwaRozw(String nazwaRozw) {
		this.nazwaRozw = nazwaRozw;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}
	
}
