/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
@Entity
@Table(name = FinancialDocument.TABLE_NAME)
public class FinancialDocument implements java.io.Serializable {
	
	public static final String TABLE_NAME = "dsg_ams_financial_document";
	
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
	private long id;
    @Column(name = "data_platnosci")
    @Temporal(TemporalType.DATE)
	private Date dataPlatnosci;
    @Column(name = "datdok")
    @Temporal(TemporalType.DATE)
	private Date datdok;
    @Column(name = "datoper")
    @Temporal(TemporalType.DATE)
	private Date datoper;
	@Column(name = "erpId")
	private double erpId;
	@Column(name="idm", length=50)
	private String idm;
	@Column(name="imie", length=50)
	private String imie;
	@Column(name="nazwisko", length=50)
	private String nazwisko;
	@Column(name="pesel", length=50)
	private String pesel;
	@Column(name="pracownik_nrewid")
	private int pracownikNrewid;
	@Column(name="kurs")
	private double kurs;
	@Column(name="kwota")
	private double kwota;
	@Column(name="kwotawalbaz")
	private double kwotawalbaz;
	@Column(name="status")
	private int status;
	
	public FinancialDocument() {
		
	}
	
    public FinancialDocument(Date dataPlatnosci, Date datdok, Date datoper,
			double erpId, String idm, String imie, String nazwisko,
			String pesel, int pracownikNrewid, double kurs, double kwota,
			double kwotawalbaz, int status) {
		super();
		this.dataPlatnosci = dataPlatnosci;
		this.datdok = datdok;
		this.datoper = datoper;
		this.erpId = erpId;
		this.idm = idm;
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.pesel = pesel;
		this.pracownikNrewid = pracownikNrewid;
		this.kurs = kurs;
		this.kwota = kwota;
		this.kwotawalbaz = kwotawalbaz;
		this.status = status;
	}

	public void setAllFieldsFromServiceObject(pl.compan.docusafe.ws.ams.DictionaryServiceStub.FinancialDocument item) {
    	setDataPlatnosci(item.getData_platnosci());
    	setDatdok(item.getDatdok());
    	setDatoper(item.getDatoper());
    	setErpId(item.getId());
    	setIdm(item.getIdm());
    	setImie(item.getImie());
    	setNazwisko(item.getNazwisko());
    	setPesel(item.getPesel());
    	setPracownikNrewid(item.getPracownik_nrewid());
    	setKurs(item.getKurs());
    	setKwota(item.getKwota());
    	setKwotawalbaz(item.getKwotawalbaz());
    	setStatus(item.getStatus());
    }
    
    public void save() throws EdmException {
    	try {
    		DSApi.context().session().save(this);
    	} catch (HibernateException e) {
    		new EdmException(e);
    	}
    }

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDataPlatnosci() {
		return dataPlatnosci;
	}

	public void setDataPlatnosci(Date dataPlatnosci) {
		this.dataPlatnosci = dataPlatnosci;
	}

	public Date getDatdok() {
		return datdok;
	}

	public void setDatdok(Date datdok) {
		this.datdok = datdok;
	}

	public Date getDatoper() {
		return datoper;
	}

	public void setDatoper(Date datoper) {
		this.datoper = datoper;
	}

	public double getErpId() {
		return erpId;
	}

	public void setErpId(double erpId) {
		this.erpId = erpId;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public int getPracownikNrewid() {
		return pracownikNrewid;
	}

	public void setPracownikNrewid(int pracownikNrewid) {
		this.pracownikNrewid = pracownikNrewid;
	}

	public double getKurs() {
		return kurs;
	}

	public void setKurs(double kurs) {
		this.kurs = kurs;
	}

	public double getKwota() {
		return kwota;
	}

	public void setKwota(double kwota) {
		this.kwota = kwota;
	}

	public double getKwotawalbaz() {
		return kwotawalbaz;
	}

	public void setKwotawalbaz(double kwotawalbaz) {
		this.kwotawalbaz = kwotawalbaz;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
