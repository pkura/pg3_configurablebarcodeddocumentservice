/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
@Entity
@Table(name = PatternsOfCostSharing.TABLE_NAME)
public class PatternsOfCostSharing implements java.io.Serializable {
	
	public static final String TABLE_NAME = "dsg_ams_patterns_of_cost_sharing";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "erpId")
	private Double erpId;
	@Column(name = "idm", length = 100)
	private String idm;
	@Column(name = "nazwa", length = 100)
	private String nazwa;
	
	public PatternsOfCostSharing() {
		
	}

	public PatternsOfCostSharing(Double erpId, String idm, String nazwa) {
		super();
		this.erpId = erpId;
		this.idm = idm;
		this.nazwa = nazwa;
	}

    public void setAllFieldsFromServiceObject(pl.compan.docusafe.ws.ams.DictionaryServiceStub.PatternsOfCostSharing item) {
    	setErpId(item.getId());
    	setIdm(item.getIdm());
    	setNazwa(item.getNazwa());
    }
    
    public void save() throws EdmException {
    	try {
    		DSApi.context().session().save(this);
    	} catch (HibernateException e) {
    		new EdmException(e);
    	}
    }
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getErpId() {
		return erpId;
	}

	public void setErpId(Double erpId) {
		this.erpId = erpId;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	
}
