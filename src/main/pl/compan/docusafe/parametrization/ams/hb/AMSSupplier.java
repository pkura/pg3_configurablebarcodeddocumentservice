/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.ws.ams.DictionaryServiceStub;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
@Entity
@Table(name = AMSSupplier.TABLE_NAME)
public class AMSSupplier implements java.io.Serializable {

	    public static final String TABLE_NAME = "dsg_ams_supplier";
	    private static AMSSupplier instance;

	    @Id
	    @GeneratedValue(strategy=GenerationType.AUTO)
	    @Column(name = "id")
	    private long id;
	    @Column(name = "available", nullable = false)
	    private boolean available = true;
	    @Column(name = "erpId")
	    private Double erpId;
	    @Column(name = "idm", length = 50)
	    private String idm;
	    @Column(name = "identyfikatorNum")
	    private Double identyfikatorNum;
	    @Column(name = "klasdostId")
	    private Double klasdostId;
	    @Column(name = "klasdostIdn", length = 50)
	    private String klasdostIdn;
	    @Column(name = "nazwa", length = 300)
	    private String nazwa;
	    @Column(name = "nip", length = 50)
	    private String nip;
	    @Column(name = "miasto", length = 50)
	    private String miasto;
	    @Column(name = "kod_pocztowy", length = 50)
	    private String kodPocztowy;
	    @Column(name = "ulica", length = 50)
	    private String ulica;
	    @Column(name = "nr_domu", length = 50)
	    private String nrDomu;
	    @Column(name = "nr_mieszkania", length = 50)
	    private String nrMieszkania;

	    public AMSSupplier() {
	    }
	    
	    
	    public static AMSSupplier find(Long id) throws EdmException {
	        return Finder.find(AMSSupplier.class, id);
	    }
	    

	    public long getId() {
	        return id;
	    }

	    public void setId(long id) {
	        this.id = id;
	    }

	    public boolean isAvailable() {
	        return available;
	    }

	    public void setAvailable(boolean available) {
	        this.available = available;
	    }

	    public Double getErpId() {
	        return erpId;
	    }

	    public void setErpId(Double erpId) {
	        this.erpId = erpId;
	    }

	    public String getIdm() {
	        return idm;
	    }

	    public void setIdm(String idm) {
	        this.idm = idm;
	    }

	    public Double getIdentyfikatorNum() {
	        return identyfikatorNum;
	    }

	    public void setIdentyfikatorNum(Double identyfikatorNum) {
	        this.identyfikatorNum = identyfikatorNum;
	    }

	    public Double getKlasdostId() {
	        return klasdostId;
	    }

	    public void setKlasdostId(Double klasdostId) {
	        this.klasdostId = klasdostId;
	    }

	    public String getKlasdostIdn() {
	        return klasdostIdn;
	    }

	    public void setKlasdostIdn(String klasdostIdn) {
	        this.klasdostIdn = klasdostIdn;
	    }

	    public String getNazwa() {
	        return nazwa;
	    }

	    public void setNazwa(String nazwa) {
	        this.nazwa = nazwa;
	    }

	    public String getNip() {
	        return nip;
	    }

	    public void setNip(String nip) {
	        this.nip = nip;
	    }

	    public String getMiasto() {
	        return miasto;
	    }

	    public void setMiasto(String miasto) {
	        this.miasto = miasto;
	    }

	    public String getKodPocztowy() {
	        return kodPocztowy;
	    }

	    public void setKodPocztowy(String kodPocztowy) {
	        this.kodPocztowy = kodPocztowy;
	    }

	    public String getUlica() {
	        return ulica;
	    }

	    public void setUlica(String ulica) {
	        this.ulica = ulica;
	    }

	    public String getNrDomu() {
	        return nrDomu;
	    }

	    public void setNrDomu(String nrDomu) {
	        this.nrDomu = nrDomu;
	    }

	    public String getNrMieszkania() {
	        return nrMieszkania;
	    }

	    public void setNrMieszkania(String nrMieszkania) {
	        this.nrMieszkania = nrMieszkania;
	    }

	    public void setAllFieldsFromServiceObject(DictionaryServiceStub.Supplier fromService) {
	        this.setErpId(fromService.getId());
	        this.setIdentyfikatorNum(fromService.getIdentyfikator_num());
	        this.setIdm(fromService.getIdm());
	        this.setNazwa(fromService.getDostawca_nazwa());
	        this.setKodPocztowy(fromService.getDostawca_kodpoczt());
	        this.setMiasto(fromService.getDostawca_miasto());
	        this.setNip(fromService.getDostawca_nip());
	        this.setNrDomu(fromService.getDostawca_nrdomu());
	        this.setNrMieszkania(fromService.getDostawca_nrmieszk());
	        this.setUlica(fromService.getDostawca_ulica());
	        this.setKlasdostId(fromService.getKlasdost_id());
	        this.setKlasdostIdn(fromService.getKlasdost_idn());
	    }

	    public void save() throws EdmException {
	        try {
	            DSApi.context().session().save(this);
	        } catch (HibernateException e) {
	            throw new EdmHibernateException(e);
	        }
	    }
}
