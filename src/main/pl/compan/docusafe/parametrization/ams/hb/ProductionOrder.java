/**
 * 
 */
package pl.compan.docusafe.parametrization.ams.hb;

import java.io.Serializable;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class ProductionOrder implements Serializable {
	private long id;
	private double idFromWebservice;
	private String idm;
	private String nazwa;
	
	public ProductionOrder() {
		
	}
	
    public ProductionOrder(double idFromWebservice, String idm, String nazwa) {
		super();
		this.idFromWebservice = idFromWebservice;
		this.idm = idm;
		this.nazwa = nazwa;
	}

    
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(idFromWebservice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((idm == null) ? 0 : idm.hashCode());
		result = prime * result + ((nazwa == null) ? 0 : nazwa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ProductionOrder)) {
			return false;
		}
		ProductionOrder other = (ProductionOrder) obj;
		if (Double.doubleToLongBits(idFromWebservice) != Double
				.doubleToLongBits(other.idFromWebservice)) {
			return false;
		}
		if (idm == null) {
			if (other.idm != null) {
				return false;
			}
		} else if (!idm.equals(other.idm)) {
			return false;
		}
		if (nazwa == null) {
			if (other.nazwa != null) {
				return false;
			}
		} else if (!nazwa.equals(other.nazwa)) {
			return false;
		}
		return true;
	}

	public void setAllFieldsFromServiceObject(pl.compan.docusafe.ws.ams.DictionaryServiceStub.ProductionOrder item) {
    	setIdFromWebservice(item.getId());
    	setIdm(item.getIdm());
    	setNazwa(item.getNazwa());
    }
    
    public void save() throws EdmException {
    	try {
    		DSApi.context().session().save(this);
    	} catch (HibernateException e) {
    		new EdmException(e);
    	}
    }

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getIdFromWebservice() {
		return idFromWebservice;
	}

	public void setIdFromWebservice(double idFromWebservice) {
		this.idFromWebservice = idFromWebservice;
	}

	public String getIdm() {
		return idm;
	}

	public void setIdm(String idm) {
		this.idm = idm;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
    
}
