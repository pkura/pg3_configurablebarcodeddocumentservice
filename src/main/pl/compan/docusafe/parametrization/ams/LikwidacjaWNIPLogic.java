/**
 * 
 */
package pl.compan.docusafe.parametrization.ams;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.exports.ExportedDocument;
import pl.compan.docusafe.core.exports.FixedAssetLiquidationDocument;
import pl.compan.docusafe.core.exports.FixedAssetPosition;
import pl.compan.docusafe.core.exports.FixedAssetPositionDetail;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.ams.hb.AmsAssetCardInfo;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class LikwidacjaWNIPLogic extends AbstractDocumentLogic {
	
	protected static Logger logger = LoggerFactory.getLogger(LikwidacjaWNIPLogic.class);
	
	public static final String RECIPIENT_HERE_FIELD_CN = "RECIPIENT_HERE";
	
	public static final String ORGANIZATIONAL_UNIT_ACCEPTATION_CN = "ORGANIZATIONAL_UNIT";
	
    @Override
    public boolean canChangeDockind(Document document) throws EdmException {
    	return false;
    }
    
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
		logger.info("--- Likwidacja WNIP : START PROCESS !!! ---- {}", event);
		try
		{   
			 Map<String, Object> map = Maps.newHashMap();
	         map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
	         map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
	         document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
	         
	         java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

	         DSUser author = DSApi.context().getDSUser();
	         String user = author.getName();
	         String fullName = author.asFirstnameLastname();

	         perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
	         perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
	         perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
	         perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
	         perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

	         Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
	         perms.addAll(documentPermissions);
	         this.setUpPermission(document, perms);
		}
		catch (Exception e)
		{
			logger.error(e.getMessage(), e);
		}
	}
	
	@Override
	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) throws EdmException {
        boolean assigned = false;
        try {
        	
            Long documentId = doc.getId();
            FieldsManager fm = Document.find(documentId).getFieldsManager();
            
            if (ORGANIZATIONAL_UNIT_ACCEPTATION_CN.equalsIgnoreCase(acceptationCn)) {
            	String realizator = (String) fm.getValue(RECIPIENT_HERE_FIELD_CN);
            	String[] realizatorArray = realizator.split(",");
            	if(realizatorArray.length > 1){
            		String fullName = realizatorArray[1];
            		String[] firstLastName = fullName.split(" "); 
            		if(DSUser.findByFirstnameLastname(firstLastName[1], firstLastName[2], false) != null){
            			assignable.addCandidateUser(DSUser.findByFirstnameLastname(firstLastName[1], firstLastName[2], false).getName());
        				AssigneeHandler.addToHistory(openExecution, doc, DSUser.findByFirstnameLastname(firstLastName[1], firstLastName[2], false).getName(), null);
        				assigned = true;
            		}
            	} else {
            		if(DSDivision.findByName(realizatorArray[0]) != null){
            			assignable.addCandidateGroup(DSDivision.findByName(realizatorArray[0]).getGuid());
        				AssigneeHandler.addToHistory(openExecution, doc, null, DSDivision.findByName(realizatorArray[0]).getGuid());
        				assigned = true;
            		}
            	}
            }
            
            return assigned;
            
        } catch (EdmException e) {
                logger.error(e.getMessage(), e);
                throw e;
        }
	}
	
	public void documentPermissions(Document document) throws EdmException {

	}

	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {

	}

    @Override
    public ExportedDocument buildExportDocument(OfficeDocument doc) throws EdmException {
        FixedAssetLiquidationDocument exported = new FixedAssetLiquidationDocument();

        FieldsManager fm = doc.getFieldsManager();
        exported.setOperation(new Date());
        exported.setValidity((Date) fm.getKey("DATA_OBOWIAZYWANIA"));
        exported.setDepartmentCode(fm.getEnumItemCn("ODDZIAL"));
        exported.setOperationTypeCode(Docusafe.getAdditionPropertyOrDefault("export.fixed_asset.operation_type", "LTT"));
        FixedAssetPosition position;

        List<Map<String, Object>> positions = (List)  fm.getValue("WNIP");

        for (Map<String, Object> fixedAsset : positions) {
            exported.addPosition(position = new FixedAssetPosition());

            if (fixedAsset.get("WNIP_NUMBER") != null && !Strings.isNullOrEmpty(((EnumValues) fixedAsset.get("WNIP_NUMBER")).getSelectedId())) {
                String fixedAssetId = ((EnumValues) fixedAsset.get("WNIP_NUMBER")).getSelectedId();
                AmsAssetCardInfo amsAssetCardInfo = AmsAssetCardInfo.find(Long.valueOf(fixedAssetId));

                position.setLiquidationCode(fm.getEnumItemCn("IDENTYFIKATOR_LIKWIDACJI"));
                position.setCardIdentifier(amsAssetCardInfo.getErpId().longValue());

                position.addPosition(new FixedAssetPositionDetail().setRatio(BigDecimal.ONE).setRegistrationSystemCode(Docusafe.getAdditionPropertyOrDefault("export.fixed_asset.registration_amortization_code1", "BILANSOWY")));
                position.addPosition(new FixedAssetPositionDetail().setRatio(BigDecimal.ONE).setRegistrationSystemCode(Docusafe.getAdditionPropertyOrDefault("export.fixed_asset.registration_amortization_code2", "PODATKOWY")));

            }
        }

        exported.setDescription(WniosekStLogic.buildAdditionDescription(getAdditionFieldsDescription(doc, fm)));

        return exported;
    }

    private Map<String,String> getAdditionFieldsDescription(OfficeDocument doc, FieldsManager fm) throws EdmException {
        Map<String,String> additionFields = Maps.newLinkedHashMap();
        additionFields.put("Nr KO",doc.getOfficeNumber() != null ? doc.getOfficeNumber().toString() : "brak");
        additionFields.put("Osoba/Jednostka organizacyjna", fm.getStringValue("RECIPIENT_HERE"));

        return additionFields;
    }
}
