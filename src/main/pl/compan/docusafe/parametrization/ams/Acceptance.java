/**
 * 
 */
package pl.compan.docusafe.parametrization.ams;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class Acceptance {
	private String name;
	private String date;
	
	public Acceptance() {
		
	}
	
	public Acceptance(String name, String date) {
		this.name = name;
		this.date = date;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
}
