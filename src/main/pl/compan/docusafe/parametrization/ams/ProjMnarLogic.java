package pl.compan.docusafe.parametrization.ams;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.jbpm.api.listener.EventListenerExecution;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.api.ActivityInfo;
import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentUpdater;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dwr.*;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.LogicUtils;
import pl.compan.docusafe.core.jbpm4.AssigneeHandlerUtils;
import pl.compan.docusafe.core.jbpm4.Jbpm4ActivityInfo;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.templating.Templating;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.ams.jbpm.AcceptanceNotifierListener;
import pl.compan.docusafe.parametrization.ams.jbpm.ProjMnarDecisionHandler;
import pl.compan.docusafe.parametrization.ams.jbpm.RejectionNotifierListener;
import pl.compan.docusafe.parametrization.ilpol.DlLogic;
import pl.compan.docusafe.util.DocumentReplicator;
import pl.compan.docusafe.util.MultipleDictionaryReplicator;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.web.admin.dictionaries.DockindDictionaries;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

import javax.validation.UnexpectedTypeException;
import java.io.*;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.Format;
import java.util.*;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class ProjMnarLogic
        extends
        AbsKartaObiegowaLogic
        implements RejectionNotifierListener.OnRejectionListener,
        AcceptanceNotifierListener.OnAcceptanceListener,
        ProjMnarDecisionHandler.OpinieDodatkowe,
        ProjMnarDecisionHandler.OsobaZatwierdzajaca,
        ProjMnarDecisionHandler.OpiniaKanclerza {


    public static final String ENUM_TITLE_TYP_KARTY_A = "Karta obiegowa typu A";
    public static final String ENUM_TITLE_TYP_KARTY_B = "Karta obiegowa typu B";

    public static final String FIELD_GENERATE_CARD_B = "GENERATE_CARD_B";
    public static final String FIELD_FINANSOWANIE_ZEWNETRZNE_PROC_WP = "FINANSOWANIE_ZEWNETRZNE_PROC_WP";
    public static final String FIELD_WKLAD_WLASNY_AM_PROC_WP = "WKLAD_WLASNY_AM_PROC_WP";
    public static final String FIELD_OSOBA_ZATWIERDZAJACA = "OSOBA_ZATWIERDZAJACA";
    public static final String FIELD_OPIEKUN_PROJEKTU = "OPIEKUN_PROJEKTU";
    public static final String FIELD_DYSPONENCI_SUMA_WKLADU_WLASNEGO = "DYSPONENCI_SUMA_WKLADU_WLASNEGO";
    public static final String FIELD_DYSPONENCI_SUMA_PREFINANSOWANIA = "DYSPONENCI_SUMA_PREFINANSOWANIA";
    public static final String FIELD_ZRODLA_FINANSOWANIA_DICT = "ZRODLA_FINANSOWANIA";
    public static final String FIELD_KOSZTY_BEZPOSREDNIE = "KOSZTY_BEZPOSREDNIE";
    public static final String FIELD_KOSZTY_BEZPOSREDNIE_PROC = "KOSZTY_BEZPOSREDNIE_PROC";
    public static final String FIELD_KOSZTY_POSREDNIE_PROC_KB = "KOSZTY_POSREDNIE_PROC_KB";
    public static final String FIELD_KOSZTY_POSREDNIE = "KOSZTY_POSREDNIE";
    public static final String FIELD_LACZNA_WARTOSC_PROJEKTU = "LACZNA_WARTOSC_PROJEKTU";
    public static final String FIELD_KIEROWNIK_PROJEKTU = "KIEROWNIK_PROJEKTU";
    public static final String FIELD_KOMORKA_ORG_AMS_NADZOR = "KOMORKA_ORG_AMS_NADZOR";
    public static final String FIELD_OPINIE_DODATKOWE_DICT = "OPINIE_DODATKOWE";
    public static final String FIELD_DYSPONENCI_SRODKOW_DICT = "DYSPONENCI_SRODKOW";
    public static final String FIELD_DYSPONENCI_SRODKOW_DISABLED_DICT = "DYSPONENCI_SRODKOW_DISABLED";
    public static final String DICTFIELD_KWOTA_WKLADU_WLASNEGO = "KWOTA_WKLADU_WLASNEGO";
    public static final String DICTFIELD_KWOTA_PREFINANSOWANIA = "KWOTA_PREFINANSOWANIA";
    //public static final String DICTFIELD_DYSPONENCI_USERNAME = "USERNAME";
    public static final String DICTFIELD_DYSPONENCI_DYSPONENT = "DYSPONENT";
    public static final String DICTFIELD_DYSPONENCI_POTWIERDZENIE = "POTWIERDZENIE";
    public static final String DICTFIELD_OPINIE_DODATKOWE_JEDNOSTKA = "JEDNOSTKA";
    public static final String DICTFIELD_ZRODLO_FINANSOWANIA = "ZRODLO_FINANSOWANIA";

    public static final String ACCEPTATION_CN_FORKHEADER_OPINIA = "opiniaForkHeader";
    public static final String ACCEPTATION_CN_FORKHEADER_DYSPONENT = "dysponentForkHeader";
    public static final String ACCEPTATION_FIELD_CNVALUE_zatwierdzenie_projektu = "zatwierdzenie_projektu";
    public static final String ACCEPTATION_FIELD_CNVALUE_zp_rektor = "zp_rektor";
    public static final String ACCEPTATION_FIELD_CNVALUE_zp_prorektor_ds_nauki = "zp_prorektor_ds_nauki";
    public static final String ACCEPTATION_FIELD_CNVALUE_zp_prorektor_ds_nauczania = "zp_prorektor_ds_nauczania";

    public static final String ACCEPTATIONTREE_KIER_NM = "KIER_NM";
    public static final String ACCEPTATIONTREE_DYR_CTTM = "DYR_CTTM";
    public static final String ACCEPTATIONTREE_REKTOR = "rektor";
    public static final String ACCEPTATIONTREE_PR_DS_NAUKI = "prorektor-ds-nauki";
    public static final String ACCEPTATIONTREE_PR_DS_NAUCZ = "prorektor-ds-naucz";

    public static final String TABLDENAME_ZRODLA_FINANSOWANIA = "dsg_ams_zrodla_finansowania";

    public static final String STATUS_WYBOR_OPIEKUNA = "wybor_opiekuna";
    public static final String STATUS_ZBIOR_OPINII = "zbior_opinii";
    public static final String STATUS_DYSPONENCI = "dysponenci";
    public static final String STATUS_ZMIANA_DYSPONENTOW = "zmiana_dysponentow";
    public static final String STATUS_WYBOR_OSOB_DO_OPINII = "wybor_osob_do_opinii";
    public static final String STATUS_OPINIA_KWESTORA = "opinia_kwestora";

    public static final String ZBIOR_OPINII = "zbior-opinii";
    public static final String PROCESS_VAR_OPINIA_JEDN_DODATKOWEJ = "opinia-jedn-dodatkowej";
    public static final String OPINIA_CTTM = "opinia-cttm";
    public static final String OPINIA_NM = "opinia-nm";
    public static final String PROCESS_VAR_OPINIA_CTTM_LUB_NM = "opinia-cttm-nm";
    public static final String ENUMVALUE_DYSPONENCI_SRODKOW_POTWIERDZENIE_NIE = "NIE";
    public static final String FIELD_TYP_KARTY = "TYP_KARTY";
    public static final String FIELD_JEDN_REALIZUJACA = "JEDN_REALIZUJACA";
    private static final String FIELD_KIEROWNIK_PIONU = "KIEROWNIK_PIONU";
    private static final String FIELD_OPINIA_JM_REKTORA = "OPINIA_JM_REKTORA";
    private static final String FIELD_OPINIA_PROREKTORA_DS_NAUKI = "OPINIA_PROREKTORA_DS_NAUKI";
    private static final String FIELD_OPINIA_PROREKTORA_DS_NAUCZANIA = "OPINIA_PROREKTORA_DS_NAUCZANIA";
    private static final String FIELD_RODZAJ_PROJEKTU = "RODZAJ_PROJEKTU";

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = new HashMap<String, Object>();
        for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
            if (f.getDefaultValue() != null)
                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));

        DSUser user = DSApi.context().getDSUser();
        toReload.put(FIELD_KIEROWNIK_PROJEKTU, user.getId());

        fm.reloadValues(toReload);
    }

    @Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {

        String statusCn = fm.getEnumItemCn(STATUS);
        StringBuilder msgBuilder = new StringBuilder();
//
//        if (compareStatus(statusCn, "zatwierdzenie_projektu")){
//            Map<String, Object> toReload = new HashMap<String, Object>();
//            toReload.put(FIELD_OSOBA_ZATWIERDZAJACA, 1);
//            fm.reloadValues(toReload);
//
//
//            get
//
//            String sender = fm.getStringValue(SENDER);
//            String nrKonta = fm.getStringValue("KONTRAHENT_NR_KONTA");
//            if (StringUtils.isEmpty(sender) || StringUtils.isEmpty(nrKonta)){
//
//                ((InOfficeDocument)doc).setSenderId(null);
//                DSApi.context().session().flush();
//            }
//        }


        if (compareStatus(statusCn, STATUS_PROCESS_CREATION)
                || compareStatus(statusCn, STATUS_CORRECTION_BY_AUTHOR)) {
//            if (!validateDwrPercentField(values, FIELD_KOSZTY_POSREDNIE_PROC_KB,true,true))
//                appendMsg(msgBuilder,"Niepoprawna warto�� procentowa pola: "+FieldsManagerUtils.getFieldName(fm,FIELD_KOSZTY_POSREDNIE_PROC_KB,true));
            if (!validateDwrPercentField(values, FIELD_KOSZTY_BEZPOSREDNIE_PROC, true, true)) {
                appendMsg(msgBuilder, "Niepoprawna warto�� procentowa pola: " + FieldsManagerUtils.getFieldName(fm, FIELD_KOSZTY_BEZPOSREDNIE_PROC, true));
                DwrUtils.resetField(values, FIELD_KOSZTY_BEZPOSREDNIE_PROC, true);
            }
            if (!validateDwrPercentField(values, FIELD_FINANSOWANIE_ZEWNETRZNE_PROC_WP, true, true)) {
                appendMsg(msgBuilder, "Niepoprawna warto�� procentowa pola: " + FieldsManagerUtils.getFieldName(fm, FIELD_FINANSOWANIE_ZEWNETRZNE_PROC_WP, true));
                DwrUtils.resetField(values, FIELD_FINANSOWANIE_ZEWNETRZNE_PROC_WP, true);
            }
            if (!validateDwrPercentField(values, FIELD_WKLAD_WLASNY_AM_PROC_WP, true, true)) {
                appendMsg(msgBuilder, "Niepoprawna warto�� procentowa pola: " + FieldsManagerUtils.getFieldName(fm, FIELD_WKLAD_WLASNY_AM_PROC_WP, true));
                DwrUtils.resetField(values, FIELD_WKLAD_WLASNY_AM_PROC_WP, true);
            }

            //przeliczenia p�l
            FieldData lacznaWartoscProjektu = values.get(DwrUtils.dwr(FIELD_LACZNA_WARTOSC_PROJEKTU));
            FieldData wkladWlasnyProcWP = values.get(DwrUtils.dwr(FIELD_WKLAD_WLASNY_AM_PROC_WP));
            FieldData wkladWlasny = values.get(DwrUtils.dwr(FIELD_WKLAD_WLASNY_WARTOSC));
            DwrUtils.calculateRelatedField(wkladWlasny, true, wkladWlasnyProcWP, true, lacznaWartoscProjektu, false);

            //przeliczenia p�l
            FieldData kosztyBezposrednie = values.get(DwrUtils.dwr(FIELD_KOSZTY_BEZPOSREDNIE));
            FieldData kosztyPosrednieProcKb = values.get(DwrUtils.dwr(FIELD_KOSZTY_POSREDNIE_PROC_KB));
            FieldData kosztyPosrednie = values.get(DwrUtils.dwr(FIELD_KOSZTY_POSREDNIE));
            DwrUtils.calculateRelatedField(kosztyPosrednie, true, kosztyPosrednieProcKb, true, kosztyBezposrednie, false);

//            FieldData kosztyBezposrednieProc = values.get(DwrUtils.dwr(FIELD_KOSZTY_BEZPOSREDNIE_PROC));
//            DwrUtils.calculateRelatedField(kosztyBezposrednie, kosztyBezposrednieProc, lacznaWartoscProjektu);

            DwrUtils.calculateSum(lacznaWartoscProjektu, kosztyPosrednie, kosztyBezposrednie);


            //sumowanie p�l ze s�ownika wielowarto�ciowego
            FieldData sumaWkladuWlasnego = values.get(DwrUtils.dwr(FIELD_DYSPONENCI_SUMA_WKLADU_WLASNEGO));
            if (sumaWkladuWlasnego != null) {
                BigDecimal sumaWkladuWlasnegoValue = DwrUtils.sumDictionaryMoneyFields(values, FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_KWOTA_WKLADU_WLASNEGO);
                sumaWkladuWlasnego.setMoneyData(sumaWkladuWlasnegoValue);
            }
            FieldData sumaPrefinansowania = values.get(DwrUtils.dwr(FIELD_DYSPONENCI_SUMA_PREFINANSOWANIA));
            if (sumaPrefinansowania != null) {
                BigDecimal sumaPrefinansowaniaValue = DwrUtils.sumDictionaryMoneyFields(values, FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_KWOTA_PREFINANSOWANIA);
                sumaPrefinansowania.setMoneyData(sumaPrefinansowaniaValue);
            }


            makeDysponenciUnique(values);


            // przeliczanie koszt�w
//            KOSZTY_BEZPOSREDNIE
//            KOSZTY_BEZPOSREDNIE_PROC
//            LACZNA_WARTOSC_PROJEKTU
            // sumowanie kwot wkladu wlasnego i prefinansowania


            //        LACZNA_WARTOSC_PROJEKTU > KOSZTY_BEZPOSREDNIE
        }

        if (compareStatus(statusCn, STATUS_ZMIANA_DYSPONENTOW)) {

            FieldData sumaWkladuWlasnego = values.get(DwrUtils.dwr(FIELD_DYSPONENCI_SUMA_WKLADU_WLASNEGO));
            if (sumaWkladuWlasnego != null) {
                DwrUtils.SumDictionaryNumbersEnumCondition sumBuilder = new DwrUtils.SumDictionaryNumbersEnumCondition(DICTFIELD_DYSPONENCI_POTWIERDZENIE, new int[]{1}) {
                    @Override
                    public boolean checkCondition(String dictionaryCn, Map<String, Object> conditionValues) {
                        return dictionaryCn.equalsIgnoreCase(FIELD_DYSPONENCI_SRODKOW_DICT) || super.checkCondition(dictionaryCn, conditionValues);
                    }
                };
                double sum = sumBuilder.sumDictionaryNumbers(values, new String[]{FIELD_DYSPONENCI_SRODKOW_DICT, FIELD_DYSPONENCI_SRODKOW_DISABLED_DICT}, DICTFIELD_KWOTA_WKLADU_WLASNEGO);
                DwrUtils.setFieldData(sumaWkladuWlasnego, sum);
            }
            FieldData sumaPrefinansowania = values.get(DwrUtils.dwr(FIELD_DYSPONENCI_SUMA_PREFINANSOWANIA));
            if (sumaPrefinansowania != null) {
                DwrUtils.SumDictionaryNumbersEnumCondition sumBuilder = new DwrUtils.SumDictionaryNumbersEnumCondition(DICTFIELD_DYSPONENCI_POTWIERDZENIE, new int[]{1}) {
                    @Override
                    public boolean checkCondition(String dictionaryCn, Map<String, Object> conditionValues) {
                        return dictionaryCn.equalsIgnoreCase(FIELD_DYSPONENCI_SRODKOW_DICT) || super.checkCondition(dictionaryCn, conditionValues);
                    }
                };
                double sum = sumBuilder.sumDictionaryNumbers(values, new String[]{FIELD_DYSPONENCI_SRODKOW_DICT, FIELD_DYSPONENCI_SRODKOW_DISABLED_DICT}, DICTFIELD_KWOTA_PREFINANSOWANIA);
                DwrUtils.setFieldData(sumaPrefinansowania, sum);
            }
        }

        //        LACZNA_WARTOSC_PROJEKTU > KOSZTY_BEZPOSREDNIE
        //        WKLAD_WLASNY_WARTOSC ??
        //        PREFINANSOWANIE_WARTOSC ??

        if (DwrUtils.isSenderField(values, DwrUtils.dwr(FIELD_GENERATE_CARD_B))){
            try {
                File printCardA = generatePrintFileOfCardA(fm);
                DocumentReplicator.ReplicationResult rr = generateCardB(values, fm);

                for (String err : rr.getErrors())
                    appendMsg(msgBuilder, err);
                Document newDoc = rr.getDocument();

                addAttachmentsToDocument(newDoc, printCardA);

                if (newDoc == null) {
                    appendMsg(msgBuilder, "Dokument karty obiegowej typu B nie zost�� utworzony");
                } else {
                    appendMsg(msgBuilder, "Utworzono dokument karty obiegowej typu B. Id dokumentu: " + newDoc.getId());
                }
            } catch (Exception e) {
                appendMsg(msgBuilder, "Nie udalo sie utworzyc karty obiegowej typu A: " + e.getMessage());
                log.error(e.getMessage(), e);
            }
        }


        if (msgBuilder.length() > 0) {
            values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
            return new Field("messageField", msgBuilder.toString(), Field.Type.BOOLEAN);
        }

        return null;
    }

    private void addAttachmentsToDocument(Document newDoc, File printCardA) throws EdmException {
        DSContextOpener opener = DSContextOpener.openAsUserIfNeed(DSContextOpener.SubjectSource.HttpServlet);
        try {
            DSApi.context().begin();
            DSUser user = DSUser.getLoggedInUser();

            Attachment attachment;
            attachment = new Attachment("Karta obiegowa typu A");
            attachment.setDocument(newDoc);
            attachment.setAuthor(user.getName());
            newDoc.createAttachment(attachment);
            DSApi.context().session().save(newDoc);

//            String attachmentProperty = DataMartDefs.DOCUMENT_ATTACHMENT_ADD;
//            String attachmentDescription = "Dodano za��cznik " + attachment.getTitle();
//            Audit attachmentAudit = Audit.create(attachmentProperty, user.getName(), attachmentDescription);
//            DataMartManager.addHistoryEntry(newDoc.getId(), attachmentAudit);
//            Map<String, Object> attachmentValues = new HashMap<String, Object>();
//            attachmentValues.put(DlLogic.STATUS_DOKUMENTU_CN, 20);
//            newDoc.getDocumentKind().setWithHistory(newDoc.getId(), attachmentValues, false, Label.SYSTEM_LABEL_OWNER);
//            TaskSnapshot.updateByDocument(newDoc);

//            DSApi.context().commit();
//            DSApi.context().begin();

            attachment.createRevision(printCardA);
            String revisionProperty = DataMartDefs.DOCUMENT_ATTACHMENT_REVISION_ADD;
            String revisionDescription = "Utworzono now� wersj� za��cznika " + attachment.getTitle();
            Audit revisionAudit = Audit.create(revisionProperty, user.getName(), revisionDescription);
            DataMartManager.addHistoryEntry(attachment.getDocument().getId(), revisionAudit);
            Map<String, Object> revisionValues = new HashMap<String, Object>();
            revisionValues.put(DlLogic.STATUS_DOKUMENTU_CN, newDoc.getFieldsManager().getKey("STATUS"));
            attachment.getDocument().getDocumentKind().setWithHistory(attachment.getDocument().getId(), revisionValues, false, Label.SYSTEM_LABEL_OWNER);

            DSApi.context().session().save(newDoc);
            TaskSnapshot.updateByDocument(newDoc);
            DSApi.context().commit();
//        } catch(Exception e){
//            //
        } finally {
            opener.closeIfNeed();
        }
    }

    protected void makeDysponenciUnique(Map<String, FieldData> values) {
        Map<String, FieldData> fields = DwrUtils.getDictionaryFields(values, FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_DYSPONENT);

        Set<String> ids = new HashSet<String>();
        Set<String> keysOfDuplicatedUsers = new HashSet<String>();
//        List<String> dictIdsToDelete = new ArrayList<String>();

        for (Map.Entry<String, FieldData> fieldUser : fields.entrySet()) {
            FieldData fieldData = fieldUser.getValue();
            if (fieldData != null && !fieldData.isEmpty()) {
                String userId = fieldData.getEnumValuesData().getSelectedId();
                if (!ids.contains(userId)) {
                    ids.add(userId);
                } else {
//                    String dictId = usernameFiled.getKey().substring(usernameFiled.getKey().lastIndexOf('_')+1);
//                    dictIdsToDelete.add(dictId);
                    keysOfDuplicatedUsers.add(fieldUser.getKey());
                }
            }
        }

        if (keysOfDuplicatedUsers.size() > 0) {
            FieldData dict = values.get(DwrUtils.dwr(FIELD_DYSPONENCI_SRODKOW_DICT));
            Map<String, FieldData> dictData = dict.getDictionaryData();

            for (String key : keysOfDuplicatedUsers) {
                EnumValues enumValues = dictData.get(key).getEnumValuesData();
                enumValues.setSelectedId("");

                dictData.get(key).setEnumValuesData(enumValues);
            }

            dict.setDictionaryData(dictData);
            values.put(DwrUtils.dwr(FIELD_DYSPONENCI_SRODKOW_DICT), dict);
        }

    }

//    @Override
//    public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException {
//
//        FieldsManager fm = doc.getFieldsManager();
//        EnumItem status = fm.getEnumItem(STATUS);
//
//        if ("zatwierdzenie_projektu".equalsIgnoreCase(status.getCn()) || STATUS_WYBOR_OPIEKUNA.equalsIgnoreCase(status.getCn())) {
//        }
//
//        super.onAcceptancesListener(doc, acceptationCn);
//    }

    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
        try {
            boolean assigned = super.assignee(doc, assignable, openExecution, acceptationCn, fieldCnValue);
            if (assigned)
                return true;

            if (ACCEPTATION_CN_taskStatus.equalsIgnoreCase(acceptationCn)) {
                Set<String> userRole = new HashSet<String>();

                if (ACCEPTATION_FIELD_CNVALUE_zatwierdzenie_projektu.equalsIgnoreCase(fieldCnValue)
                        || ACCEPTATION_FIELD_CNVALUE_zp_rektor.equalsIgnoreCase(fieldCnValue)
                        || ACCEPTATION_FIELD_CNVALUE_zp_prorektor_ds_nauki.equalsIgnoreCase(fieldCnValue)
                        || ACCEPTATION_FIELD_CNVALUE_zp_prorektor_ds_nauczania.equalsIgnoreCase(fieldCnValue)) {
                    FieldsManager fm = doc.getFieldsManager();

                    OsobaZatwierdzajaca osobaZatwierdzajaca = checkOsobaZatwierdzajaca(fm);
                    switch (osobaZatwierdzajaca) {
                        case JM_REKTOR:
                            userRole.add(ACCEPTATIONTREE_REKTOR);
                            break;
                        case PROREKTOR_DS_NAUKI:
                            userRole.add(ACCEPTATIONTREE_PR_DS_NAUKI);
                            break;
                        case PRORETKOR_DS_NAUCZANIA:
                            userRole.add(ACCEPTATIONTREE_PR_DS_NAUCZ);
                            break;
                        default:
                            throw new UnexpectedTypeException("Nieoczekiwana osoba zatwierdzaj�ca: " + osobaZatwierdzajaca.name());
                    }
                }

                if (userRole.isEmpty())
                    throw new EdmException("Nie mo�na znale�� osoby do przypisania dla dokumentu");
                if (assignToAllUsersByRole(doc, assignable, openExecution, userRole))
                    return true;
            } else if (ACCEPTATION_CN_FORKHEADER_OPINIA.equalsIgnoreCase(acceptationCn)) {
                String processVar = (String) openExecution.getVariable(ACCEPTATION_CN_FORKHEADER_OPINIA);
                if (processVar.startsWith(PROCESS_VAR_OPINIA_JEDN_DODATKOWEJ)) {
                    Long divId = Long.parseLong(processVar.substring(PROCESS_VAR_OPINIA_JEDN_DODATKOWEJ.length()));

                    //*
                    DSDivision division = DSDivision.find(divId.intValue());
                    Set<DSUser> kierownicyPionu = getSupervisors(division);
                    for (DSUser dsUser : kierownicyPionu)
                        AssigneeHandlerUtils.assignToUser(doc,assignable, openExecution, dsUser);
                    /*/
                    assigned = AssigneeHandlerUtils.assignToDivisionWhileFork(doc, assignable, openExecution, divId);
                    //*/
                } else if (processVar.startsWith(PROCESS_VAR_OPINIA_CTTM_LUB_NM)) {
                    Long userId = Long.parseLong(processVar.substring(PROCESS_VAR_OPINIA_CTTM_LUB_NM.length()));
                    assigned = AssigneeHandlerUtils.assignToUserWhileFork(doc, assignable, openExecution, userId);
                }
            } else if (ACCEPTATION_CN_FORKHEADER_DYSPONENT.equalsIgnoreCase(acceptationCn)) {
                Long userId = (Long) openExecution.getVariable(ACCEPTATION_CN_FORKHEADER_DYSPONENT);
                assigned = AssigneeHandlerUtils.assignToUserWhileFork(doc, assignable, openExecution, userId);
            }

            return assigned;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new IllegalArgumentException(e.getMessage(), e); // Wywal przypisanie jbpm-owe! Albo wszystko ok, albo nie r�b
        }
    }

    @Override
    public void notifyRejectionListener(EventListenerExecution execution, OfficeDocument document) {
        setDysponentPotwierdzenie(execution, document, false);
    }

    @Override
    public void notifyAcceptanceListener(EventListenerExecution execution, OfficeDocument document, String value) {
        setDysponentPotwierdzenie(execution, document, true);
    }

    private void setDysponentPotwierdzenie(EventListenerExecution execution, OfficeDocument document, boolean zgoda) {
        try {
            DSUser dysponent = DSApi.context().getDSUser();
            DwrDictionaryBase dysponenciSrodkowDict = DwrDictionaryFacade.getDwrDictionary(document.getDocumentKind().getCn(), FIELD_DYSPONENCI_SRODKOW_DICT);

            List<Long> allIds = new ArrayList<Long>();
            FieldsManager fieldsManager = document.getFieldsManager();
            List<Map<String, Object>> dysponenciAll = FieldsManagerUtils.getDictionaryItems(fieldsManager, FIELD_DYSPONENCI_SRODKOW_DICT,
                    new FieldsManagerUtils.FieldProperty(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_DYSPONENT), EnumValues.class),
                    new FieldsManagerUtils.FieldProperty(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_POTWIERDZENIE), EnumValues.class),
                    new FieldsManagerUtils.FieldProperty(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, "ID")));

            for (Map o : dysponenciAll) {
                if (o.containsKey(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_DYSPONENT))) {

                    Long selectedUserId = Long.parseLong(((EnumValues) o.get(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_DYSPONENT))).getSelectedId());
                    if (selectedUserId.equals(dysponent.getId())) {

                        EnumValues potwierdzenie = (EnumValues) o.get(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_POTWIERDZENIE));
                        String potwierdzenieSelectedId;
                        if (potwierdzenie == null || StringUtils.isEmpty(potwierdzenieSelectedId = potwierdzenie.getSelectedId())) {

                            Object dictIdOb = o.get(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, "ID"));
                            Integer dictId = LogicUtils.getId(dictIdOb);
                            allIds.add((long) dictId.intValue());
                        }
                    }
                }
            }

            Long id;
            if (allIds.size() != 1)
                throw new IllegalArgumentException("Nie mo�na jednoznacznie okre�li�, kt�rego wpisu dotyczy potwierdzenie/odrzucenie. Dysponent: "
                        + dysponent.getWholeName() + ", " + allIds.size());
            else
                id = allIds.get(0);


            Map<String, FieldData> values = new HashMap<String, FieldData>();
            values.put("id", new FieldData(Field.Type.LONG, id));
            String potwierdzenieSelectedId = zgoda ? "1" : "2";
            String potwierdzenieSelectedValue = zgoda ? "ZAAKCEPTOWANE" : "ODRZUCONE";
            values.put(DICTFIELD_DYSPONENCI_POTWIERDZENIE, new FieldData(Field.Type.ENUM, FieldsManagerUtils.getSimpleEnumValues(potwierdzenieSelectedId, potwierdzenieSelectedValue)));
//            values.put(DICTFIELD_DYSPONENCI_POTWIERDZENIE, new FieldData(Field.Type.ENUM, FieldsManagerUtils.getSimpleEnumValues("2", "2")));
            dysponenciSrodkowDict.update(values);

        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public boolean isOpinieDodatkoweRequired(FieldsManager fm) throws EdmException {
        List<Object> divisions = FieldsManagerUtils.getDictionaryItems(fm, "OPINIE_DODATKOWE", "OPINIE_DODATKOWE_ID", Object.class);
        return divisions.size() > 0;
    }

    @Override
    public String getOsobaZatwierdzajaca(FieldsManager fm) throws EdmException {
        OsobaZatwierdzajaca osobaZatwierdzajaca = checkOsobaZatwierdzajaca(fm);
        return osobaZatwierdzajaca.name();
//        switch (osobaZatwierdzajaca) {
//            case JM_REKTOR:
//                return "rektor";
//                break;
//            case PROREKTOR_DS_NAUKI:
//                return "prorektor-ds-nauki";
//                break;
//            case PRORETKOR_DS_NAUCZANIA:
//                return "prorektor-ds-nauczania";
//                break;
//        }
    }

    @Override
    public boolean isOpiniaKanclerzaRequired(FieldsManager fm) throws EdmException {
        EnumItem enumField = fm.getEnumItem(FIELD_RODZAJ_PROJEKTU);
        if (enumField!=null){
            return "INWESTYCYJNY".equalsIgnoreCase(enumField.getCn());
        }
        return false;
    }

    public enum OsobaZatwierdzajaca {
        JM_REKTOR,
        PROREKTOR_DS_NAUKI,
        PRORETKOR_DS_NAUCZANIA
    }

    public boolean canBeNewCardB(FieldsManager fm ) throws EdmException {
        EnumItem status = fm.getEnumItem(STATUS);
        if (!(status != null && STATUS_WYBOR_OPIEKUNA.equalsIgnoreCase(status.getCn())))
            return false;
        Integer typKarty = (Integer) fm.getKey(FIELD_TYP_KARTY);
        return typKarty != null && typKarty == 2;

    }

    /**
     * @param fm
     * @return
     * @throws EdmException obiekt lub null gdy moze to byc swiezy dokument obiegu karty typu B i nie ma ustawionego pola
     */
    public OsobaZatwierdzajaca checkOsobaZatwierdzajaca(FieldsManager fm) throws EdmException {
        EnumItem opiekun = fm.getEnumItem(FIELD_OPIEKUN_PROJEKTU);
        if (opiekun==null && canBeNewCardB(fm)) return null;
        DSUser user = DSUser.findByUsername(opiekun.getCn());


        if (isPrefinansowanieRequired(fm) || isWkladWlasnyRequired(fm)) {
            return OsobaZatwierdzajaca.JM_REKTOR;
        } else {
            // todo sprawdza� czy opiekun nale�y do odp dzia�u

            for (DSDivision division : user.getDivisions()) {
                String divName = division.getName();
                if ("NM".equalsIgnoreCase(divName)) {
                    return OsobaZatwierdzajaca.PRORETKOR_DS_NAUCZANIA;
                } else if ("BC".equalsIgnoreCase(divName) || "CTTM".equalsIgnoreCase(divName)) {
                    return OsobaZatwierdzajaca.PROREKTOR_DS_NAUKI;
                }
            }

            log.error("NIE ZNALEZIONO ODPOWIEDNIEGO DZIA�U ([BC/CTTM] / [NM], DO KT�REGO NALE�Y OPIEKUN");
            return OsobaZatwierdzajaca.PROREKTOR_DS_NAUKI;
        }
    }

    public enum DzialKierownikaDlaZrodlaFinansowania {
        CTTM, NM
    }

    public static Set<DzialKierownikaDlaZrodlaFinansowania> getDzialyKierownikowDlaZrodelFinansowania(FieldsManager fm) throws EdmException, SQLException {
        Set<DzialKierownikaDlaZrodlaFinansowania> zrodla = new HashSet<DzialKierownikaDlaZrodlaFinansowania>();

        List<EnumValues> zrodlaEV = FieldsManagerUtils.getSimpleDictionaryItems(fm, FIELD_ZRODLA_FINANSOWANIA_DICT, DICTFIELD_ZRODLO_FINANSOWANIA, EnumValues.class, true);
        for (EnumValues zEV : zrodlaEV){
            DockindDictionaries dockindDictionaries = DockindDictionaries.find(Integer.parseInt(zEV.getSelectedId()), TABLDENAME_ZRODLA_FINANSOWANIA);
            zrodla.add(convertToKierownikDlaZrodlaFinansowania(dockindDictionaries, true));
        }

        return zrodla;
    }
    public static DzialKierownikaDlaZrodlaFinansowania convertToKierownikDlaZrodlaFinansowania(DockindDictionaries dockindDictionaries, boolean exceptionIfNotExist){

        String dzial = dockindDictionaries.getRefValue();

        if ("NM".equalsIgnoreCase(dzial)) {
            return DzialKierownikaDlaZrodlaFinansowania.NM; // je�eli �r�d�o finansowania = NM
        } else if ("BC".equalsIgnoreCase(dzial) || "CTTM".equalsIgnoreCase(dzial)) {
            return DzialKierownikaDlaZrodlaFinansowania.CTTM; // je�eli �r�d�o finansowania = BC
        }

        log.error("NIE ZNALEZIONO ODPOWIEDNIEGO DZIA�U ([BC/CTTM] / [NM] W ZALE�NOSCI OD ZRODLA FINANSOWANIA");
        if (exceptionIfNotExist)
            throw new IllegalArgumentException("NIE ZNALEZIONO ODPOWIEDNIEGO DZIA�U ([BC/CTTM] / [NM] W ZALE�NOSCI OD ZRODLA FINANSOWANIA");
        return DzialKierownikaDlaZrodlaFinansowania.NM;
    }

    public static String getRole(DzialKierownikaDlaZrodlaFinansowania dzialKierownikaDlaZrodlaFinansowania) {
        switch (dzialKierownikaDlaZrodlaFinansowania) {
            case CTTM:
                return ACCEPTATIONTREE_DYR_CTTM; // je�eli �r�d�o finansowania = BC
            case NM:
                return ACCEPTATIONTREE_KIER_NM; // je�eli �r�d�o finansowania = NM
        }
        throw new UnexpectedTypeException();
    }

    public static Set<DSUser> getKierownikDlaZrodlaFinansowania(FieldsManager fm) throws EdmException, SQLException {
        Set<DzialKierownikaDlaZrodlaFinansowania> dzialyKierownikowDlaZrodelFinansowania = getDzialyKierownikowDlaZrodelFinansowania(fm);
        return getKierownikDlaZrodlaFinansowania(dzialyKierownikowDlaZrodelFinansowania);
    }

    public static Set<DSUser> getKierownikDlaZrodlaFinansowania(Set<DzialKierownikaDlaZrodlaFinansowania> dzialyKierownikowDlaZrodelFinansowania) {
        try{
            Set<DSUser> kierownicy = new HashSet<DSUser>();
            for(DzialKierownikaDlaZrodlaFinansowania dzial : dzialyKierownikowDlaZrodelFinansowania){

                String role = getRole(dzial);

                List<String> users = new ArrayList<String>();
                List<String> divisionsGuids = new ArrayList<String>();

                for (AcceptanceCondition accept : AcceptanceCondition.find(role, null))
                {
                    if (accept.getUsername() != null && !users.contains(accept.getUsername()))
                        users.add(accept.getUsername());
                    if (accept.getDivisionGuid() != null && !divisionsGuids.contains(accept.getDivisionGuid()))
                        divisionsGuids.add(accept.getDivisionGuid());
                }

                for (String username : users)
                {
                    DSUser kierownik = DSUser.findByUsername(username);
                    kierownicy.add(kierownik);
                }
                for (String dGuid : divisionsGuids)
                {
                    DSDivision division = DSDivision.find(dGuid);
                    DSUser[] userss = division.getUsers(false);
                    kierownicy.addAll(Arrays.asList(userss));
                }

//            Set<DSUser> users = getAllUsersByRole(role);
//            kierownicy.addAll(users);
            }
            return kierownicy;
//         (DivisionNotFoundException e)
//         (UserNotFoundException e)
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            throw new IllegalArgumentException(e.getMessage(), e); // Wywal przypisanie jbpm-owe! Albo wszystko ok, albo nie r�b
        }
    }

    @Override
    public void setAdditionalValues(FieldsManager fieldsManager, Integer valueOf, String activity) throws EdmException {
        try {
            Jbpm4ActivityInfo activityInfo = new Jbpm4ActivityInfo(activity);
            String taskName = (String) activityInfo.get("task_name");
            EnumItem status = fieldsManager.getEnumItem(STATUS);

            if (fieldsManager != null && status != null) {
                String statusCn = status.getCn();

                if (statusCn.equalsIgnoreCase(STATUS_ZBIOR_OPINII)) {

                    String processId = activityInfo.getVariableValue(ACCEPTATION_CN_FORKHEADER_OPINIA, String.class);
                    if (processId.startsWith(PROCESS_VAR_OPINIA_JEDN_DODATKOWEJ)) {
                        Long taskDivId = Long.parseLong(processId.substring(PROCESS_VAR_OPINIA_JEDN_DODATKOWEJ.length()));

                        List<Object> opinie = new ArrayList<Object>();
                        DSUser user = DSUser.getLoggedInUserSafely();
                        if (user != null) {

                            List<Map<String, Object>> opinieAll = FieldsManagerUtils.getDictionaryItems(fieldsManager, FIELD_OPINIE_DODATKOWE_DICT,
                                    new FieldsManagerUtils.FieldProperty(joinDictionaryWithField(FIELD_OPINIE_DODATKOWE_DICT, DICTFIELD_OPINIE_DODATKOWE_JEDNOSTKA), EnumValues.class),
                                    new FieldsManagerUtils.FieldProperty(joinDictionaryWithField(FIELD_OPINIE_DODATKOWE_DICT, "ID")));
                            ArrayList<DSDivision> userDivisions = user.getDivisionsAsList();

                            for (Map o : opinieAll) {
                                if (o.containsKey(joinDictionaryWithField(FIELD_OPINIE_DODATKOWE_DICT, DICTFIELD_OPINIE_DODATKOWE_JEDNOSTKA))) {
                                    Long selectedDivId = Long.parseLong(((EnumValues) o.get(joinDictionaryWithField(FIELD_OPINIE_DODATKOWE_DICT, DICTFIELD_OPINIE_DODATKOWE_JEDNOSTKA))).getSelectedId());
                                    if (taskDivId == null || selectedDivId.equals(taskDivId)) {
                                        DSDivision div = DSDivision.findById(selectedDivId);
                                        if (userDivisions.contains(div)) {
                                            opinie.add(o.get(joinDictionaryWithField(FIELD_OPINIE_DODATKOWE_DICT, "ID")));
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        Object dic = fieldsManager.getFieldValues().get(FIELD_OPINIE_DODATKOWE_DICT);
                        if (dic != null && dic instanceof List) {
                            ((List) dic).clear();

                            if (opinie.size() != 1)
                                throw new IllegalAccessError("Nieprawid�owa liczba wpis�w do potwierdzenia na jednego dysponenta: " + user.getWholeName() + ", " + opinie.size());

                            ((List) dic).addAll(opinie);
                        }
                    } else if (processId.startsWith(PROCESS_VAR_OPINIA_CTTM_LUB_NM)) {
                    }
                }

                if (statusCn.equals(STATUS_DYSPONENCI)) {
                    DSUser user = DSUser.getLoggedInUserSafely();
                    List<Object> dysponenci = new ArrayList<Object>();

                    List<Map<String, Object>> opinieAll = FieldsManagerUtils.getDictionaryItems(fieldsManager, FIELD_DYSPONENCI_SRODKOW_DICT,
                            new FieldsManagerUtils.FieldProperty(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_DYSPONENT), EnumValues.class),
                            new FieldsManagerUtils.FieldProperty(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_POTWIERDZENIE), EnumValues.class),
                            new FieldsManagerUtils.FieldProperty(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, "ID")));

                    for (Map o : opinieAll) {
                        if (o.containsKey(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_DYSPONENT))) {

                            Long selectedUserId = Long.parseLong(((EnumValues) o.get(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_DYSPONENT))).getSelectedId());
                            if (selectedUserId.equals(user.getId())) {

                                EnumValues potwierdzenie = (EnumValues) o.get(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_POTWIERDZENIE));
                                String potwierdzenieSelectedId;
                                if (potwierdzenie == null || StringUtils.isEmpty(potwierdzenieSelectedId = potwierdzenie.getSelectedId())) {

                                    Object id = o.get(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, "ID"));
                                    dysponenci.add(id);
                                }
                            }
                        }
                    }

                    Object dic = fieldsManager.getFieldValues().get(FIELD_DYSPONENCI_SRODKOW_DICT);
                    if (dic != null && dic instanceof List) {
                        ((List) dic).clear();
                        ((List) dic).addAll(dysponenci);
                    }
                }

                if (statusCn.equals(STATUS_ZMIANA_DYSPONENTOW)) {
                    List<Object> dysponenci = new ArrayList<Object>();
                    List<Object> dysponenciDisabled = new ArrayList<Object>();

                    List<Map<String, Object>> opinieAll = FieldsManagerUtils.getDictionaryItems(fieldsManager, FIELD_DYSPONENCI_SRODKOW_DICT,
                            new FieldsManagerUtils.FieldProperty(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_POTWIERDZENIE), EnumValues.class),
                            new FieldsManagerUtils.FieldProperty(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, "ID")));

                    for (Map o : opinieAll) {
                        if (o.containsKey(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_POTWIERDZENIE))) {
                            Object id = o.get(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, "ID"));

                            EnumValues potwierdzenie = (EnumValues) o.get(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_POTWIERDZENIE));
                            String potwierdzenieSelectedId = potwierdzenie.getSelectedId();
                            if (StringUtils.isEmpty(potwierdzenieSelectedId))
                                dysponenci.add(id);
                            else
                                dysponenciDisabled.add(id);
                        }
                    }

                    Object dsDict = fieldsManager.getFieldValues().get(FIELD_DYSPONENCI_SRODKOW_DICT);
                    if (dsDict != null && dsDict instanceof List) {
                        ((List) dsDict).clear();
                        ((List) dsDict).addAll(dysponenci);
                    }
                    Object dsdDict = fieldsManager.getFieldValues().get(FIELD_DYSPONENCI_SRODKOW_DISABLED_DICT);
                    if (dsdDict != null) {
                        if (dsdDict instanceof List) {
                            ((List) dsdDict).clear();
                            ((List) dsdDict).addAll(dysponenciDisabled);
                        }
                    } else {
                        Map<String, Object> values = fieldsManager.getFieldValues();
                        values.put(FIELD_DYSPONENCI_SRODKOW_DISABLED_DICT, dysponenciDisabled);
                        fieldsManager.reloadValues(values);
                    }
                }

                if (STATUS_OPINIA_KWESTORA.equalsIgnoreCase(status.getCn()) || STATUS_ZMIANA_DYSPONENTOW.equalsIgnoreCase(status.getCn())) {
                    if (isPrefinansowanieRequired(fieldsManager)) {

                        List<Map<String, Object>> prefinansowanie = FieldsManagerUtils.getDictionaryItems(fieldsManager, FIELD_DYSPONENCI_SRODKOW_DICT,
                                new FieldsManagerUtils.FieldProperty(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_POTWIERDZENIE), EnumValues.class),
                                new FieldsManagerUtils.FieldProperty(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_KWOTA_PREFINANSOWANIA), BigDecimal.class));

                        Double sum = null;

                        for (Map o : prefinansowanie) {
                            EnumValues potwierdzenie = (EnumValues) o.get(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_POTWIERDZENIE));
                            String potwierdzenieSelectedId = potwierdzenie.getSelectedId();
                            if (StringUtils.isEmpty(potwierdzenieSelectedId)) {
                                if (o.containsKey(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_POTWIERDZENIE))) {
                                    BigDecimal value = (BigDecimal) o.get(joinDictionaryWithField(FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_POTWIERDZENIE));
                                    if (value != null) {
                                        if (sum == null)
                                            sum = 0d;
                                        sum += value.doubleValue();
                                    }
                                }
                            }
                        }

                        if (sum != null) {
                            Map<String, Object> fmValues = fieldsManager.getFieldValues();
                            //FIELD_DYSPONENCI_SUMA_PREFINANSOWANIA
                            fmValues.put(FIELD_DYSPONENCI_SUMA_PREFINANSOWANIA, new BigDecimal(sum));
                            fieldsManager.reloadValues(fmValues);
                        }
                    }
                }
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }


    @Override
    public void beforeSetWithHistory(Document document, Map<String, Object> values, String activity) throws EdmException {
        Long docId = document.getId();
        PreparedStatement ps = null;
        try {
            ActivityInfo activityInfo = new Jbpm4ActivityInfo(activity);
            String taskName = (String) activityInfo.get("task_name");

            if (document.getFieldsManager().getEnumItemCn(STATUS).equals(STATUS_ZBIOR_OPINII)) {
                FieldsManager oldFm = document.getFieldsManager();
                oldFm.initialize();
                Map<String, Object> mDict = (Map) values.get("M_DICT_VALUES");
                LogicUtils.putOldMultipleDictionaryValues(mDict, oldFm, FIELD_OPINIE_DODATKOWE_DICT);
                values.put("M_DICT_VALUES", mDict);
                document.getFieldsManager().reloadValues(values);
            }

            if (document.getFieldsManager().getEnumItemCn(STATUS).equals(STATUS_DYSPONENCI)) {
                FieldsManager oldFm = document.getFieldsManager();
                oldFm.initialize();
                Map<String, Object> mDict = (Map) values.get("M_DICT_VALUES");
                LogicUtils.putOldMultipleDictionaryValues(mDict, oldFm, FIELD_DYSPONENCI_SRODKOW_DICT);
                values.put("M_DICT_VALUES", mDict);
                document.getFieldsManager().reloadValues(values);
            }

            if (document.getFieldsManager().getEnumItemCn(STATUS).equals(STATUS_ZMIANA_DYSPONENTOW)) {
                FieldsManager oldFm = document.getFieldsManager();
                oldFm.initialize();
                Map<String, Object> mDict = (Map) values.get("M_DICT_VALUES");
                LogicUtils.moveAllValuesToOtherMultipleDict(mDict, oldFm, FIELD_DYSPONENCI_SRODKOW_DICT, FIELD_DYSPONENCI_SRODKOW_DISABLED_DICT);
                values.put("M_DICT_VALUES", mDict);
                document.getFieldsManager().reloadValues(values);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException {
        super.onAcceptancesListener(doc, acceptationCn);
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {

        FieldsManager fm = document.getFieldsManager();
        //if (canBeNewCardB(fm)) return;

        EnumItem status = fm.getEnumItem(STATUS);

        StringBuilder builder = new StringBuilder();

        if (status == null
                || STATUS_PROCESS_CREATION.equalsIgnoreCase(status.getCn())) {
            String titleDesc = (String) fm.getValue(fm.getMainFieldCn());
            if (StringUtils.isBlank(titleDesc))
                titleDesc = document.getDocumentKind().getName();
            String typ = (String) fm.getValue(FIELD_TYP_KARTY);
            titleDesc += " - " + ((StringUtils.isNotBlank(typ)) ? typ : fm.getField(FIELD_TYP_KARTY).getEnumItemByCn("A").getTitle());

            document.setTitle(titleDesc);
            document.setDescription(titleDesc);
        }

        if (status == null
                || STATUS_PROCESS_CREATION.equalsIgnoreCase(status.getCn())
                || STATUS_CORRECTION_BY_AUTHOR.equalsIgnoreCase(status.getCn())) {

            Object wkladWlasny = fm.getKey(FIELD_WKLAD_WLASNY_AM_PROC_WP);
            Object finansZewn = fm.getKey(FIELD_FINANSOWANIE_ZEWNETRZNE_PROC_WP);
            if ((Double)wkladWlasny + (Double)finansZewn != 100)
                builder.append("Suma procentowego wk�adu w�asnego i finansowania zewn�trznego musi by� r�wna 100%.");

            Object sumWkladWlasny = fm.getKey(FIELD_DYSPONENCI_SUMA_WKLADU_WLASNEGO);
            Object szacSzacWkladWlasny = fm.getKey(FIELD_WKLAD_WLASNY_WARTOSC);
            if (((BigDecimal)szacSzacWkladWlasny).doubleValue() != ((BigDecimal)sumWkladWlasny).doubleValue())
                builder.append("Suma wk�adu w�asnego musi by� r�wna szacunkowej ��cznej warto�ci projektu.");
        }

        if (status == null
                || STATUS_PROCESS_CREATION.equalsIgnoreCase(status.getCn())
                || STATUS_ZMIANA_DYSPONENTOW.equalsIgnoreCase(status.getCn())
                || STATUS_CORRECTION_BY_AUTHOR.equalsIgnoreCase(status.getCn())) {

            Set<String> duplicatedDysponenciSelectedIds = getDuplicatedDysponenci(fm);

            if (duplicatedDysponenciSelectedIds.size() > 0) {
                List<String> duplicatedDysponenci = new ArrayList<String>();
                pl.compan.docusafe.core.dockinds.field.Field usernameField = FieldsManagerUtils.getDictionaryField(fm, FIELD_DYSPONENCI_SRODKOW_DICT, DICTFIELD_DYSPONENCI_DYSPONENT, pl.compan.docusafe.core.dockinds.field.Field.class);
                if (usernameField != null) {
                    for (String id : duplicatedDysponenciSelectedIds) {
                        String username = FieldsManagerUtils.getEnumTitle(usernameField, id);
                        if (username == null)
                            username = FieldsManagerUtils.getEnumLabel(usernameField, id);
                        duplicatedDysponenci.add(username);
                    }
                }
                builder.append("Wybrani dysponenci powtarzaj� si�" )
                        .append(duplicatedDysponenci.size() == 0 ? "" : ": " + StringUtils.join(duplicatedDysponenci.toArray(new String[duplicatedDysponenci.size()]), ", "))
                        .append(".");
            }
        }

        if (status != null) {
            if (STATUS_WYBOR_OSOB_DO_OPINII.equalsIgnoreCase(status.getCn())) {

                Set<String> duplicatedOpinieDodatkoweSelectedIds = getDuplicatedOpinieDodatkowe(fm);

                if (duplicatedOpinieDodatkoweSelectedIds.size() > 0) {
                    List<String> duplicatedOpinieDodatkowe = new ArrayList<String>();
                    pl.compan.docusafe.core.dockinds.field.Field jednostkaField = FieldsManagerUtils.getDictionaryField(fm, FIELD_OPINIE_DODATKOWE_DICT, DICTFIELD_OPINIE_DODATKOWE_JEDNOSTKA, pl.compan.docusafe.core.dockinds.field.Field.class);
                    if (jednostkaField != null) {
                        for (String id : duplicatedOpinieDodatkoweSelectedIds) {
                            String jednostkaName = FieldsManagerUtils.getEnumTitle(jednostkaField, id);
                            if (jednostkaName == null)
                                jednostkaName = FieldsManagerUtils.getEnumLabel(jednostkaField, id);
                            duplicatedOpinieDodatkowe.add(jednostkaName);
                        }
                    }
                    builder.append("Wybrane jednostki do opinii dodatkowych powtarzaj� si�")
                            .append(duplicatedOpinieDodatkowe.size() == 0 ? "" : ": " + StringUtils.join(duplicatedOpinieDodatkowe.toArray(new String[duplicatedOpinieDodatkowe.size()]), ", "))
                            .append(".");
                }
            }

            if (STATUS_WYBOR_OPIEKUNA.equalsIgnoreCase(status.getCn())) {
                try {
                    OsobaZatwierdzajaca osobaZatwierdzajaca = checkOsobaZatwierdzajaca(fm);
                    if (osobaZatwierdzajaca!=null){
                        int selectedId = 0;
                        switch (osobaZatwierdzajaca) {
                            case JM_REKTOR:
                                selectedId = 1;
                                break;
                            case PROREKTOR_DS_NAUKI:
                                selectedId = 2;
                                break;
                            case PRORETKOR_DS_NAUCZANIA:
                                selectedId = 3;
                                break;
                        }
                        if (selectedId > 0) {
                            DocumentUpdater.updateValue(document, FIELD_OSOBA_ZATWIERDZAJACA, selectedId);
                            //                    Map<String, Object> values = fm.getFieldValues();
                            //                    EnumValues osoba = FieldsManagerUtils.getSimpleEnumValues(Integer.toString(selectedId), Integer.toString(selectedId));
                            //                    values.put(FIELD_OSOBA_ZATWIERDZAJACA, osoba);
                            //                    fm.reloadValues(values);
                        }
                    }
                } catch (EdmException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }

        if (builder.length()>0)
            throw new EdmException(builder.toString());
    }


    public static Set<String> getDuplicatedDysponenci(FieldsManager fieldsManager) throws EdmException {

        List<String> dysponenci = getActiveDysponenciUsername(fieldsManager, FIELD_DYSPONENCI_SRODKOW_DICT);
        List<String> dysponenciDisabled = getActiveDysponenciUsername(fieldsManager, FIELD_DYSPONENCI_SRODKOW_DISABLED_DICT);

        Set<String> uniqueUsers = new HashSet<String>();
        Set<String> duplicatedUsers = new HashSet<String>();

        matchDuplicatedAndUnique(uniqueUsers, duplicatedUsers, dysponenci);
        matchDuplicatedAndUnique(uniqueUsers, duplicatedUsers, dysponenciDisabled);

        return duplicatedUsers;
    }


    public static Set<String> getDuplicatedOpinieDodatkowe(FieldsManager fieldsManager) throws EdmException {

        List<Map<String, Object>> jednostkiEV = FieldsManagerUtils.getDictionaryItems(fieldsManager, FIELD_OPINIE_DODATKOWE_DICT,
                new FieldsManagerUtils.FieldProperty(joinDictionaryWithField(FIELD_OPINIE_DODATKOWE_DICT, DICTFIELD_OPINIE_DODATKOWE_JEDNOSTKA), EnumValues.class));

        Set<String> unique = new HashSet<String>();
        Set<String> duplicated = new HashSet<String>();

        for (Map o : jednostkiEV) {
            if (o.containsKey(joinDictionaryWithField(FIELD_OPINIE_DODATKOWE_DICT, DICTFIELD_OPINIE_DODATKOWE_JEDNOSTKA))) {
                EnumValues jednostka = (EnumValues) o.get(joinDictionaryWithField(FIELD_OPINIE_DODATKOWE_DICT, DICTFIELD_OPINIE_DODATKOWE_JEDNOSTKA));

                if (jednostka != null) {
                    String jednostkaId = jednostka.getSelectedId();
                    if (unique.contains(jednostkaId))
                        duplicated.add(jednostkaId);
                    else
                        unique.add(jednostkaId);
                }
            }
        }

        return duplicated;
    }


    public static void matchDuplicatedAndUnique(Set<String> uniqueUsers, Set<String> duplicatedUsers, List<String> valuesToMatch) {
        for (String d : valuesToMatch)
            if (uniqueUsers.contains(d))
                duplicatedUsers.add(d);
            else
                uniqueUsers.add(d);
    }

    public static List<String> getActiveDysponenciUsername(FieldsManager fieldsManager, String dictCn) throws EdmException {

        List<Map<String, Object>> dysponenci = FieldsManagerUtils.getDictionaryItems(fieldsManager, dictCn,
                new FieldsManagerUtils.FieldProperty(joinDictionaryWithField(dictCn, DICTFIELD_DYSPONENCI_DYSPONENT), EnumValues.class),
                new FieldsManagerUtils.FieldProperty(joinDictionaryWithField(dictCn, DICTFIELD_DYSPONENCI_POTWIERDZENIE), EnumValues.class));

        List<String> usernames = new ArrayList<String>();

        for (Map o : dysponenci) {
            if (o.containsKey(joinDictionaryWithField(dictCn, DICTFIELD_DYSPONENCI_POTWIERDZENIE))) {
                EnumValues potwierdzenie = (EnumValues) o.get(joinDictionaryWithField(dictCn, DICTFIELD_DYSPONENCI_POTWIERDZENIE));
                String potwierdzenieSelectedId = potwierdzenie.getSelectedId();
                if (StringUtils.isEmpty(potwierdzenieSelectedId) || !"1".equalsIgnoreCase(potwierdzenie.getSelectedId())) {
                    EnumValues user = (EnumValues) o.get(joinDictionaryWithField(dictCn, DICTFIELD_DYSPONENCI_DYSPONENT));
                    if (user != null)
                        usernames.add(user.getSelectedId());
                }
            }
        }

        return usernames;
    }

    //    List<Map<String,Object>> items = new ArrayList<Map<String, Object>>();
//
//    Object efekty = fm.getValue(dictionaryCn);
//    if (efekty != null && efekty instanceof List) {
//    for (Object efekt : (List) efekty) {
//        if (efekt instanceof Map) {
//            Map<String,Object> item = new HashMap<String, Object>();
//            for (FieldProperty fp : fieldsProperties){
//                Object i = ((HashMap) efekt).get(cn);
////                        if (i!=null && fp.clazz.equals(i.getClass()))
////                            item.put(fp.cn,fp.clazz.cast(i));
//                if (i!=null && fp.clazz.isAssignableFrom(i.getClass()))
//                    item.put(cn,fp.clazz.cast(i));
//            }
//            if (!item.isEmpty())
//                items.add(item);
//        }
//    }
//}
    @Override
    public void documentPermissions(Document document) throws EdmException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void doDockindEvent(DockindButtonAction eventActionSupport, ActionEvent event, Document document, String activity, Map<String, Object> values, Map<String, Object> dockindKeys) throws EdmException {
        super.doDockindEvent(eventActionSupport, event, document, activity, values, dockindKeys);
    }

    private DocumentReplicator.ReplicationResult generateCardB(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        System.out.println("generate card b...");
        KartaObiegowaBCreator documentReplicator = KartaObiegowaBCreator.prepare(fm);
        DocumentReplicator.ReplicationResult rr = documentReplicator.copyDocument();

        return rr;
    }

    public static class KartaObiegowaBCreator extends DocumentReplicator {

        public static KartaObiegowaBCreator prepare(FieldsManager oldFm) throws EdmException {
            DSContextOpener opener = DSContextOpener.openAsUserIfNeed(DSContextOpener.SubjectSource.HttpServlet);
            KartaObiegowaBCreator documentReplicator = new KartaObiegowaBCreator(oldFm);
            opener.closeIfNeed();
            return documentReplicator;
        }

        protected KartaObiegowaBCreator(FieldsManager oldFm) throws EdmException {
            super(oldFm);
        }

        @Override
        protected void updateDocumentBeforeCreate(Document newDoc) throws EdmException {
            newDoc.setDescription(getValueForCardB(newDoc.getDescription()));
            newDoc.setTitle(getValueForCardB(newDoc.getTitle()));

        } // todo zmiana p�l z tekstem "karta obiegowa typu A" na B, np. title, description

        public String getValueForCardB (String valueForCardA){
            if (StringUtils.isBlank(valueForCardA)) return valueForCardA;
            return valueForCardA.replace(ENUM_TITLE_TYP_KARTY_A, ENUM_TITLE_TYP_KARTY_B);
        }

        @Override
        protected void putNewDocumentValues(Document newDoc, Map<String, Object> newValues) throws EdmException, SQLException {
            super.putNewDocumentValues(newDoc, newValues);

            newValues.put(ProjMnarLogic.STATUS, 10);
            newValues.put(ProjMnarLogic.FIELD_TYP_KARTY, 2);

            // todo zmiana p�l z tekstem "karta obiegowa typu A" na B
        }

        @Override
        protected Object getCopyOfValue(Document newDoc, pl.compan.docusafe.core.dockinds.field.Field field, Object oldValue) throws EdmException, SQLException {
            String cn = field.getCn();
            if (cn.equalsIgnoreCase("OS_W_PROJEKCIE"))
                oldValue = MultipleDictionaryReplicator.replicateDictionary(newDoc, cn, new DsgZrodlaFinansowania(oldDoc, "DSG_AMS_OS_W_PROJEKCIE", cn));
            if (cn.equalsIgnoreCase("ZRODLA_FINANSOWANIA"))
                oldValue = MultipleDictionaryReplicator.replicateDictionary(newDoc, cn, new DsgZrodlaFinansowania(oldDoc, "DSG_AMS_DOCKIND_ZRODLO_FINANSOWANIA", cn));
            if (cn.equalsIgnoreCase("DYSPONENCI_SRODKOW"))
                oldValue = MultipleDictionaryReplicator.replicateDictionary(newDoc, cn, new DsgDysponentSrodkow(oldDoc,"DSG_AMS_DYSPONENCI_SRODKOW",cn));
            if (cn.equalsIgnoreCase("DYSPONENCI_SRODKOW_DISABLED"))
                oldValue = MultipleDictionaryReplicator.replicateDictionary(newDoc, cn, new DsgDysponentSrodkow(oldDoc, "DSG_AMS_DYSPONENCI_SRODKOW", cn));
            if (cn.equalsIgnoreCase("EFEKTY_PROJEKTU"))
                oldValue = MultipleDictionaryReplicator.replicateDictionary(newDoc, cn, new DsgEfektyProjektu(oldDoc, "DSG_AMS_EFEKTY_PROJEKTU", cn));
            if (cn.equalsIgnoreCase("OPINIE_DODATKOWE"))
                oldValue = MultipleDictionaryReplicator.replicateDictionary(newDoc, cn, new DsgOpinieDodatkowe(oldDoc, "DSG_AMS_OPINIE_DODATKOWE", cn));
            return oldValue;
        }


        @Override
        protected Sender getCopyOfSender() throws EdmException {
            return createSender();
        }
    }


    public File generatePrintFileOfCardA(FieldsManager fm) throws Exception {
        long documentId = fm.getDocumentId();
        DSContextOpener dsContextOpener = DSContextOpener.openAsUserIfNeed(DSContextOpener.SubjectSource.HttpServlet);
        try {
//            ServletActionContext.getResponse().setContentType("text/rtf");

            Map<String, Object> params = Maps.newHashMap();
            DocFacade df= Documents.document(documentId);
            params.put("fields", df.getFields());
            Map<Class, Format> defaultFormatters = Maps.newLinkedHashMap();
            setAdditionalTemplateValuesUnsafe(df.getId(), params, defaultFormatters);

            OutputStream templateOutput = new ByteArrayOutputStream();
            Templating.rtfTemplate("KartaObiegowaA.rtf", params, templateOutput, defaultFormatters, "true");

//            InputStream decodedInputFromTemplateOutput = new ByteArrayInputStream(((ByteArrayOutputStream) templateOutput).toByteArray());
//            ServletActionContext.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + templateName.replace(".rtf", ".pdf") + "\"");
//            PdfUtils.convertRtfToPdf(decodedInputFromTemplateOutput, ServletActionContext.getResponse().getOutputStream(), true);

            try{

                File printFilePdf = File.createTempFile("printCardA",".pdf");
                OutputStream fopPdf = new FileOutputStream(printFilePdf);
                InputStream decodedInputFromTemplateOutput = new ByteArrayInputStream(((ByteArrayOutputStream) templateOutput).toByteArray());
                PdfUtils.convertRtfToPdf(decodedInputFromTemplateOutput, fopPdf, true);

                return printFilePdf;
            } catch(Exception e){
                log.error(e.getMessage(),e);
                File printFileRtf = File.createTempFile("printCardA",".rtf");
                FileOutputStream fopRtf = new FileOutputStream(printFileRtf);
                ((ByteArrayOutputStream) templateOutput).writeTo(fopRtf);

                return printFileRtf;
            }

        } finally {
            dsContextOpener.closeIfNeed();
        }
    }


    public void setAdditionalTemplateValuesUnsafe(long docId, Map<String, Object> values, Map<Class, Format> defaultFormatters) throws EdmException {
        super.setAdditionalTemplateValuesUnsafe(docId, values, defaultFormatters);

        OfficeDocument doc = OfficeDocument.find(docId);
        FieldsManager fm = doc.getFieldsManager();

        Object efektyProjektu = values.get(FIELD_EFEKTY_PROJEKTU_DICT);
        if (efektyProjektu != null && efektyProjektu instanceof List){
            List<Map<String, Object>> efektyPotrzebyUczelni = new ArrayList<Map<String, Object>>();
            List<Map<String, Object>> efektyPotrzebyInne = new ArrayList<Map<String, Object>>();
            boolean czySrodkiTrwale = false;

            for (Map efekt : (List<Map>)efektyProjektu){
                String przeznaczenieTitle = (String) efekt.get(FIELD_EFEKTY_PROJEKTU_DICT+"_"+DICTFIELD_EFEKTY_PROJEKTU_PRZEZNACZENIE);
                pl.compan.docusafe.core.dockinds.field.Field przeznaczenieEnum = FieldsManagerUtils.getFieldFromMultiple(fm, FIELD_EFEKTY_PROJEKTU_DICT, DICTFIELD_EFEKTY_PROJEKTU_PRZEZNACZENIE, true);
                EnumItem efektPrzeznaczenie = przeznaczenieEnum.getEnumItemByTitle(przeznaczenieTitle);
                Integer przeznaczenieId = efektPrzeznaczenie.getId();
                //((DataBaseEnumField)FieldsManagerUtils.getFieldFromMultiple(fm,FIELD_EFEKTY_PROJEKTU_DICT,"PRZEZNACZENIE",true).getEnumItemByTitle()
                if (przeznaczenieId != null && przeznaczenieId == ID_EFEKTY_PROJEKTU_PRZEZNACZENIE_EPP1)
                    efektyPotrzebyUczelni.add(efekt);
                else
                    efektyPotrzebyInne.add(efekt);

                String rodzajTitle = (String) efekt.get(FIELD_EFEKTY_PROJEKTU_DICT+"_"+DICTFIELD_EFEKTY_PROJEKTU_RODZAJ);
                pl.compan.docusafe.core.dockinds.field.Field rodzajEnum = FieldsManagerUtils.getFieldFromMultiple(fm, FIELD_EFEKTY_PROJEKTU_DICT, DICTFIELD_EFEKTY_PROJEKTU_RODZAJ, true);
                EnumItem efektRodzaj = rodzajEnum.getEnumItemByTitle(rodzajTitle);
                Integer rodzajId = efektRodzaj.getId();
                if (rodzajId != null && rodzajId == ID_EFEKTY_PROJEKTU_RODZAJ_EPR5)
                    czySrodkiTrwale = true;
            }

            values.put("EFEKTY_POTRZEBY_UCZELNI",efektyPotrzebyUczelni);
            values.put("EFEKTY_POTRZEBY_INNE",efektyPotrzebyInne);
            values.put("CZY_SRODKI_TRWALE",czySrodkiTrwale);
        }


        //fm.getKey("OSWIAD_O_RZETELNOSCI")
        //Object kierProj = fm.getKey(FIELD_KIEROWNIK_PROJEKTU);



        //OS_OSWIADCZAJACA
        String kierProjName = getEnumTitle(fm, FIELD_KIEROWNIK_PROJEKTU);
        values.put("OS_OSWIADCZAJACA",kierProjName);

        //OS_KIER_JR
        String jrName = getEnumTitle(fm, FIELD_JEDN_REALIZUJACA);
        values.put("OS_KIER_JR",jrName);

        //OS_KIER_BZ
        String kierBz = getUserNameFromAcceptance(fm,"zbior_opinii_nm");
        values.put("OS_KIER_BZ",kierBz);

        //OS_KIER_PIONU
        String kierPionuName = getEnumTitle(fm, FIELD_KIEROWNIK_PIONU);
        values.put("OS_KIER_PIONU",kierPionuName);

        //OS_KWESTOR
        String kwestor = getUserNameFromAcceptance(fm,"opinia_kwestora");
        values.put("OS_KWESTOR",kwestor);

        //OS_KANCLERZ
        String kanclerz = getUserNameFromAcceptance(fm,"opinia_kanclerza");
        values.put("OS_KANCLERZ",kanclerz);

        //OS_ZATWIERDZAJACA
        String rektor = getUserNameFromAcceptance(fm,"zp_rektor");
        String prorektorDsNauki = getUserNameFromAcceptance(fm,"zp_prorektor_ds_nauki");
        String prorektorDsNauczania = getUserNameFromAcceptance(fm,"zp_prorektor_ds_nauczania");
        if (StringUtils.isNotBlank(rektor))
            values.put("OS_ZATWIERDZAJACA",rektor);
        else if (StringUtils.isNotBlank(prorektorDsNauki))
            values.put("OS_ZATWIERDZAJACA",prorektorDsNauki);
        else if (StringUtils.isNotBlank(prorektorDsNauczania))
            values.put("OS_ZATWIERDZAJACA",prorektorDsNauczania);

        //OPINIA_OS_ZATWIERDZAJACEJ
        String opiniaRektora = (String)fm.getKey(FIELD_OPINIA_JM_REKTORA);
        String opiniaProrektoraDsNauki = (String)fm.getKey(FIELD_OPINIA_PROREKTORA_DS_NAUKI);
        String opiniaProrektoraDsNauczania = (String)fm.getKey(FIELD_OPINIA_PROREKTORA_DS_NAUCZANIA);
        if (StringUtils.isNotBlank(opiniaRektora))
            values.put("OPINIA_OS_ZATWIERDZAJACEJ",opiniaRektora);
        else if (StringUtils.isNotBlank(opiniaProrektoraDsNauki))
            values.put("OPINIA_OS_ZATWIERDZAJACEJ",opiniaProrektoraDsNauki);
        else if (StringUtils.isNotBlank(opiniaProrektoraDsNauczania))
            values.put("OPINIA_OS_ZATWIERDZAJACEJ",opiniaProrektoraDsNauczania);


    }
}
class DsgDysponentSrodkow extends MultipleDictionaryReplicator{
    private static final Map<String, Field.Type> columnsWithType;
    static {//DYSPONENT_1,KWOTA_PREFINANSOWANIA_1,KWOTA_WKLADU_WLASNEGO_1,ZADANIE_FINANSOWE_1,OPINIA_1,POTWIERDZENIE_1
        columnsWithType = new HashMap<String, Field.Type>();
        columnsWithType.put("DYSPONENT", Field.Type.LONG);
        columnsWithType.put("KWOTA_PREFINANSOWANIA", Field.Type.MONEY);
        columnsWithType.put("KWOTA_WKLADU_WLASNEGO", Field.Type.MONEY);
        columnsWithType.put("ZADANIE_FINANSOWE", Field.Type.STRING);
        columnsWithType.put("OPINIA", Field.Type.STRING);
        columnsWithType.put("POTWIERDZENIE", Field.Type.INTEGER);
    }
    protected DsgDysponentSrodkow(Document document, Object multipleDictTable, String dictCn) {
        super(document, multipleDictTable, dictCn);
    }
    @Override
    public Map<String, Field.Type> getColumnsWithType() {
        return columnsWithType;
    }
}
class DsgEfektyProjektu extends MultipleDictionaryReplicator{
    private static final Map<String, Field.Type> columnsWithType;
    static {//RODZAJ_1,PRZEZNACZENIE_1,WARTOSC_1,UZASADNIENIE_1
        columnsWithType = new HashMap<String, Field.Type>();
        columnsWithType.put("RODZAJ", Field.Type.LONG);
        columnsWithType.put("PRZEZNACZENIE", Field.Type.LONG);
        columnsWithType.put("WARTOSC", Field.Type.MONEY);
        columnsWithType.put("UZASADNIENIE", Field.Type.STRING);
    }
    protected DsgEfektyProjektu(Document document, Object multipleDictTable, String dictCn) {
        super(document, multipleDictTable, dictCn);
    }
    @Override
    public Map<String, Field.Type> getColumnsWithType() {
        return columnsWithType;
    }
}
class DsgOpinieDodatkowe extends MultipleDictionaryReplicator{
    private static final Map<String, Field.Type> columnsWithType;
    static {//JEDNOSTKA_1,OPINIA_1
        columnsWithType = new HashMap<String, Field.Type>();
        columnsWithType.put("JEDNOSTKA", Field.Type.LONG);
        columnsWithType.put("OPINIA", Field.Type.STRING);
    }
    protected DsgOpinieDodatkowe(Document document, Object multipleDictTable, String dictCn) {
        super(document, multipleDictTable, dictCn);
    }
    @Override
    public Map<String, Field.Type> getColumnsWithType() {
        return columnsWithType;
    }
}
class DsgZrodlaFinansowania extends MultipleDictionaryReplicator{
    private static final Map<String, Field.Type> columnsWithType;
    static {//JEDNOSTKA_1,OPINIA_1
        columnsWithType = new HashMap<String, Field.Type>();
        columnsWithType.put("ZRODLO_FINANSOWANIA", Field.Type.LONG);
    }
    protected DsgZrodlaFinansowania(Document document, Object multipleDictTable, String dictCn) {
        super(document, multipleDictTable, dictCn);
    }
    @Override
    public Map<String, Field.Type> getColumnsWithType() {
        return columnsWithType;
    }
}
class DsgOsWProjekcie extends MultipleDictionaryReplicator{
    private static final Map<String, Field.Type> columnsWithType;
    static {//USERNAME_1
        columnsWithType = new HashMap<String, Field.Type>();
        columnsWithType.put("USERNAME", Field.Type.LONG);
    }
    protected DsgOsWProjekcie(Document document, Object multipleDictTable, String dictCn) {
        super(document, multipleDictTable, dictCn);
    }
    @Override
    public Map<String, Field.Type> getColumnsWithType() {
        return columnsWithType;
    }
}



//TEMP - z tk

//            values.put("ID_SPRAWY", docId);
//
//            //czas wygenerowania metryki
//            values.put("GENERATE_DATE", DateUtils.formatCommonDate(new Date()));
//
//            DSUser osGenRaport = DSUser.getLoggedInUserSafely();
//            values.put("OSOBA_GENERUJACA_RAPORT", osGenRaport.getWholeName());
//
//            List<DocumentAcceptance> bookKeepers = findDocumentAcceptance(acceptances, STATUS_AKCEPTACJA_KSIEGOWOSCI);
//            if (!bookKeepers.isEmpty()) {
//                List<String> uniqueWholeNames = new ArrayList<String>();
//                List<Map<String, Object>> allTemplateBKs = new ArrayList<Map<String, Object>>();
//                for (DocumentAcceptance bk : bookKeepers) {
//                    try {
//                        String userWholeName = DSUser.findByUsername(bk.getUsername()).getWholeName();
//                        if (!uniqueWholeNames.contains(userWholeName)) {
//                            uniqueWholeNames.add(userWholeName);
//
//                            Map<String, Object> templateBK = new HashMap<String, Object>();
//                            templateBK.put("DATA", DateUtils.formatCommonDate(bk.getAcceptanceTime()));
//                            templateBK.put("DEKRETUJACY", userWholeName);
//                            allTemplateBKs.add(templateBK);
//                        }
//                    } catch (Exception e) {
//                        log.error("NIEZNALEZIONO U�YTKOWNIKA " + bk.getUsername() + " DLA AKCEPTACJI " + bk.getAcceptanceCn(), e);
//                    }
//                }
//                values.put("BOOK_KEEPERS", allTemplateBKs);
//            }
//
//            List<DocumentAcceptance> kierKsieg = findDocumentAcceptance(acceptances, STATUS_KIER_KSIEG);
//            if (!kierKsieg.isEmpty()) {
//                List<String> uniqueWholeNames = new ArrayList<String>();
//                List<Map<String, Object>> allTemplateKKs = new ArrayList<Map<String, Object>>();
//                for (DocumentAcceptance kk : kierKsieg) {
//                    try {
//                        String userWholeName = DSUser.findByUsername(kk.getUsername()).getWholeName();
//                        if (!uniqueWholeNames.contains(userWholeName)) {
//                            uniqueWholeNames.add(userWholeName);
//
//                            Map<String, Object> templateKK = new HashMap<String, Object>();
//                            templateKK.put("DATA", DateUtils.formatCommonDate(kk.getAcceptanceTime()));
//                            templateKK.put("DEKRETUJACY", userWholeName);
//                            allTemplateKKs.add(templateKK);
//                        }
//                    } catch (Exception e) {
//                        log.error("NIEZNALEZIONO U�YTKOWNIKA " + kk.getUsername() + " DLA AKCEPTACJI " + kk.getAcceptanceCn(), e);
//                    }
//                }
//                values.put("KIER_KSIEG", allTemplateKKs);
//            }