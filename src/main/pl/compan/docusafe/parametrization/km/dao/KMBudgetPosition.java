package pl.compan.docusafe.parametrization.km.dao;

import org.hibernate.HibernateException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;

import java.math.BigDecimal;

public class KMBudgetPosition implements java.io.Serializable {

	private Long id;
	private String accountView;
	private Integer cost;
	private Integer mpk;
	private Integer subjectKind;
	private Integer analitycs;
	private Integer project;
	private Integer tax;
	private Integer bp;
	private Integer vehicle;
	private Integer actionKind;
	private Integer costKind;
	private Integer completed;
	private BigDecimal amount;

    private static KMBudgetPosition instance;

    public static synchronized KMBudgetPosition getInstance() {
        if (instance == null)
            instance = new KMBudgetPosition();
        return instance;
    }

    public KMBudgetPosition() {
	}

	public KMBudgetPosition(Long id) {
		this.id = id;
	}

    public KMBudgetPosition(KMBudgetPosition bp) {
        this.accountView = bp.getAccountView();
        this.cost = bp.getCost();
        this.mpk = bp.getMpk();
        this.subjectKind = bp.getSubjectKind();
        this.analitycs = bp.getAnalitycs();
        this.project = bp.getProject();
        this.tax = bp.getTax();
        this.bp = bp.getBp();
        this.vehicle = bp.getVehicle();
        this.actionKind = bp.getActionKind();
        this.costKind = bp.getCostKind();
        this.completed = bp.getCompleted();
        this.amount = bp.getAmount();
    }

	public KMBudgetPosition(Long id, String accountView, Integer cost, Integer mpk, Integer subjectKind, Integer analitycs,
			Integer project, Integer tax, Integer bp, Integer vehicle, Integer actionKind, Integer costKind, Integer completed,
			BigDecimal amount) {
		this.id = id;
		this.accountView = accountView;
		this.cost = cost;
		this.mpk = mpk;
		this.subjectKind = subjectKind;
		this.analitycs = analitycs;
		this.project = project;
		this.tax = tax;
		this.bp = bp;
		this.vehicle = vehicle;
		this.actionKind = actionKind;
		this.costKind = costKind;
		this.completed = completed;
		this.amount = amount;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccountView() {
		return this.accountView;
	}

	public void setAccountView(String accountView) {
		this.accountView = accountView;
	}

	public Integer getCost() {
		return this.cost;
	}

	public void setCost(Integer cost) {
		this.cost = cost;
	}

	public Integer getMpk() {
		return this.mpk;
	}

	public void setMpk(Integer mpk) {
		this.mpk = mpk;
	}

	public Integer getSubjectKind() {
		return this.subjectKind;
	}

	public void setSubjectKind(Integer subjectKind) {
		this.subjectKind = subjectKind;
	}

	public Integer getAnalitycs() {
		return this.analitycs;
	}

	public void setAnalitycs(Integer analitycs) {
		this.analitycs = analitycs;
	}

	public Integer getProject() {
		return this.project;
	}

	public void setProject(Integer project) {
		this.project = project;
	}

	public Integer getTax() {
		return this.tax;
	}

	public void setTax(Integer tax) {
		this.tax = tax;
	}

	public Integer getBp() {
		return this.bp;
	}

	public void setBp(Integer bp) {
		this.bp = bp;
	}

	public Integer getVehicle() {
		return this.vehicle;
	}

	public void setVehicle(Integer vehicle) {
		this.vehicle = vehicle;
	}

	public Integer getActionKind() {
		return this.actionKind;
	}

	public void setActionKind(Integer actionKind) {
		this.actionKind = actionKind;
	}

	public Integer getCostKind() {
		return this.costKind;
	}

	public void setCostKind(Integer costKind) {
		this.costKind = costKind;
	}

	public Integer getCompleted() {
		return this.completed;
	}

	public void setCompleted(Integer completed) {
		this.completed = completed;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof KMBudgetPosition))
			return false;
		KMBudgetPosition castOther = (KMBudgetPosition) other;

		return (this.getId() == castOther.getId())
				&& ((this.getAccountView() == castOther.getAccountView()) || (this.getAccountView() != null
						&& castOther.getAccountView() != null && this.getAccountView().equals(castOther.getAccountView())))
				&& ((this.getCost() == castOther.getCost()) || (this.getCost() != null && castOther.getCost() != null && this.getCost()
						.equals(castOther.getCost())))
				&& ((this.getMpk() == castOther.getMpk()) || (this.getMpk() != null && castOther.getMpk() != null && this.getMpk().equals(
						castOther.getMpk())))
				&& ((this.getSubjectKind() == castOther.getSubjectKind()) || (this.getSubjectKind() != null
						&& castOther.getSubjectKind() != null && this.getSubjectKind().equals(castOther.getSubjectKind())))
				&& ((this.getAnalitycs() == castOther.getAnalitycs()) || (this.getAnalitycs() != null && castOther.getAnalitycs() != null && this
						.getAnalitycs().equals(castOther.getAnalitycs())))
				&& ((this.getProject() == castOther.getProject()) || (this.getProject() != null && castOther.getProject() != null && this
						.getProject().equals(castOther.getProject())))
				&& ((this.getTax() == castOther.getTax()) || (this.getTax() != null && castOther.getTax() != null && this.getTax().equals(
						castOther.getTax())))
				&& ((this.getBp() == castOther.getBp()) || (this.getBp() != null && castOther.getBp() != null && this.getBp().equals(
						castOther.getBp())))
				&& ((this.getVehicle() == castOther.getVehicle()) || (this.getVehicle() != null && castOther.getVehicle() != null && this
						.getVehicle().equals(castOther.getVehicle())))
				&& ((this.getActionKind() == castOther.getActionKind()) || (this.getActionKind() != null
						&& castOther.getActionKind() != null && this.getActionKind().equals(castOther.getActionKind())))
				&& ((this.getCostKind() == castOther.getCostKind()) || (this.getCostKind() != null && castOther.getCostKind() != null && this
						.getCostKind().equals(castOther.getCostKind())))
				&& ((this.getCompleted() == castOther.getCompleted()) || (this.getCompleted() != null && castOther.getCompleted() != null && this
						.getCompleted().equals(castOther.getCompleted())))
				&& ((this.getAmount() == castOther.getAmount()) || (this.getAmount() != null && castOther.getAmount() != null && this
						.getAmount().equals(castOther.getAmount())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getId() == null ? 0 : this.getId().hashCode());
		result = 37 * result + (getAccountView() == null ? 0 : this.getAccountView().hashCode());
		result = 37 * result + (getCost() == null ? 0 : this.getCost().hashCode());
		result = 37 * result + (getMpk() == null ? 0 : this.getMpk().hashCode());
		result = 37 * result + (getSubjectKind() == null ? 0 : this.getSubjectKind().hashCode());
		result = 37 * result + (getAnalitycs() == null ? 0 : this.getAnalitycs().hashCode());
		result = 37 * result + (getProject() == null ? 0 : this.getProject().hashCode());
		result = 37 * result + (getTax() == null ? 0 : this.getTax().hashCode());
		result = 37 * result + (getBp() == null ? 0 : this.getBp().hashCode());
		result = 37 * result + (getVehicle() == null ? 0 : this.getVehicle().hashCode());
		result = 37 * result + (getActionKind() == null ? 0 : this.getActionKind().hashCode());
		result = 37 * result + (getCostKind() == null ? 0 : this.getCostKind().hashCode());
		result = 37 * result + (getCompleted() == null ? 0 : this.getCompleted().hashCode());
		result = 37 * result + (getAmount() == null ? 0 : this.getAmount().hashCode());
		return result;
	}

    public KMBudgetPosition find(Long id) throws EdmException {
        return Finder.find(KMBudgetPosition.class, id);
    }

    public void save() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }
}
