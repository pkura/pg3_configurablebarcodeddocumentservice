package pl.compan.docusafe.parametrization.km.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


public class ObceJednDecision implements DecisionHandler {
    private final static Logger LOG = LoggerFactory.getLogger(ObceJednDecision.class);

    public String decide(OpenExecution openExecution) {
        try {
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();

            Integer wyjasnienieID = (Integer) fm.getKey("WYJASNIENIE");
            Integer uzupelnienieID = (Integer) fm.getKey("UZUP_OBCE_JEDN");

            if (wyjasnienieID != null && wyjasnienieID == 2) {
                return "stan-meryt";
            } else if (wyjasnienieID != null && wyjasnienieID == 1) {
                if (uzupelnienieID != null && uzupelnienieID == 2) {
                    return "kom-meryt";
                } else {
                    return "mzs1-obce-jedn";
                }
            }
            return "kom-meryt";
        } catch (EdmException ex) {
            LOG.error(ex.getMessage(), ex);
            return "kom-meryt";
        }
    }
}

