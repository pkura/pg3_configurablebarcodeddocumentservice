package pl.compan.docusafe.parametrization.km.jbpm;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 29.07.13
 * Time: 10:58
 * To change this template use File | Settings | File Templates.
 */
public class AddUsernameToVariables extends AbstractEventListener {

    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        //TODO
        //brak obs�ugi zast�pstw
        execution.setVariable("substantive_username", DSApi.context().getPrincipalName());
    }
}
