package pl.compan.docusafe.parametrization.km.jbpm;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;

public class PrepareProcessListener implements ExternalActivityBehaviour {

    private static final long serialVersionUID = 1L;
    private String dictionaryField;

    // private String enumIdField;
    // public static final List<String> guids = new LinkedList<String>();

    @Override
    public void execute(ActivityExecution execution) throws Exception {
	Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
	OfficeDocument doc = OfficeDocument.find(docId);
	final List<Long> argSets = new LinkedList<Long>();

	@SuppressWarnings("unchecked")
	List<Long> dictionaryIds = (List<Long>) doc.getFieldsManager().getKey(dictionaryField.toUpperCase());

	argSets.addAll(dictionaryIds);

	execution.setVariable("ids", argSets);
	execution.setVariable("count", argSets.size());
	execution.setVariable("count2", 2);
    }

    @Override
    public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
	// TODO Auto-generated method stub

    }

}
