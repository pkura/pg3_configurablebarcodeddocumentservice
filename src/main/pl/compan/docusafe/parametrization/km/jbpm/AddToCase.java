package pl.compan.docusafe.parametrization.km.jbpm;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.util.DateUtils;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

public class AddToCase implements ExternalActivityBehaviour {
    private static final Logger log = LoggerFactory.getLogger(AddToCase.class);
    private Integer customSequenceId;
    private String suggestedOfficeId;
    private String customOfficeIdPrefix;
    private String customOfficeIdPrefixSeparator;
    private String customOfficeIdMiddle;
    private String customOfficeIdSuffixSeparator;
    private String customOfficeIdSuffix;

    @Override
    public void execute(ActivityExecution execution) throws Exception {

        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        Long portfolioId = Long.valueOf(Docusafe.getAdditionProperty("ID_TECZKI_SPRAWY"));
        OfficeDocument document = OfficeDocument.findOfficeDocument(docId);
        FieldsManager fm = document.getFieldsManager();
        try {
//            DSApi.openAdmin();
            OfficeFolder portfolio = OfficeFolder.find(portfolioId);

            OfficeCase officeCase = new OfficeCase(portfolio);

            officeCase.setStatus(CaseStatus.find(CaseStatus.ID_OPEN));
            officeCase.setPriority(CasePriority.find(1));
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTime(((Date) fm.getKey("DATA_WPLYWU")));
            gc.add(Calendar.DAY_OF_MONTH, 14);
            Date finishDate = gc.getTime();
            officeCase.setFinishDate(finishDate);
            officeCase.setDescription("Skarga");
            officeCase.setTitle("Skarga z " + DateUtils.formatCommonDate((Date) fm.getKey("DATA_WPLYWU")));

            officeCase.setClerk(DSApi.context().getPrincipalName());

            customSequenceId = officeCase.suggestSequenceId();
            String[] breakdown = officeCase.suggestOfficeId();
            suggestedOfficeId = StringUtils.join(breakdown, "");
            customOfficeIdPrefix = breakdown[0];
            customOfficeIdPrefixSeparator = breakdown[1];
            customOfficeIdMiddle = breakdown[2];
            customOfficeIdSuffixSeparator = breakdown[3];
            customOfficeIdSuffix = breakdown[4];

            officeCase.setSequenceId(customSequenceId);

            String oid = StringUtils.join(new String[]{
                    customOfficeIdPrefix != null ? customOfficeIdPrefix.trim() : "",
                    customOfficeIdPrefixSeparator != null ? customOfficeIdPrefixSeparator.trim() : "",
                    customOfficeIdMiddle != null ? customOfficeIdMiddle.trim() : "",
                    customOfficeIdSuffixSeparator != null ? customOfficeIdSuffixSeparator.trim() : "",
                    customOfficeIdSuffix != null ? customOfficeIdSuffix.trim() : ""
            }, "");

//            DSApi.context().begin();
            officeCase.create(oid,
                    customOfficeIdPrefix != null ? customOfficeIdPrefix.trim() : "",
                    customOfficeIdMiddle != null ? customOfficeIdMiddle.trim() : "",
                    customOfficeIdSuffix != null ? customOfficeIdSuffix.trim() : "");


            document.setContainingCase(officeCase, false);


            PreparedStatement ps = DSApi.context().prepareStatement("UPDATE dsg_skarga SET NR_SKARGI = ? WHERE document_id =?");
            ps.setString(1, document.getCaseDocumentId());
            ps.setLong(2, document.getId());
            ps.executeUpdate();

            TaskSnapshot.updateByDocument(document);
//            TaskSnapshot.updateAllTasksByDocumentId(docId, document.getStringType());
//            DSApi.context().commit();

            log.error("Utworzono sprawe " + officeCase.getOfficeId() + " i umieszczono " +
                    "w niej biezacy dokument");

        } catch (EdmException e) {
            log.error(e.getMessage(), e);
//            DSApi.context().setRollbackOnly();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
//            DSApi.context().setRollbackOnly();
        } finally {
            try {
//                DSApi.close();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }


        execution.takeDefaultTransition();
    }

    @Override
    public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {

    }

}
