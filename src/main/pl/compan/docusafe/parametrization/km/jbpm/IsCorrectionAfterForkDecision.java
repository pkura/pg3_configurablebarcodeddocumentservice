package pl.compan.docusafe.parametrization.km.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

public class IsCorrectionAfterForkDecision implements DecisionHandler {

    private static final long serialVersionUID = 1L;

    @Override
    public String decide(OpenExecution openExecution) {
	Integer cor = (Integer) openExecution.getVariable("correction");

	if (cor != null && cor == 11) {
	    openExecution.removeVariable("correction");
	    openExecution.setVariable("correction", 2);
	    openExecution.setVariable("count", 2);
	    return "correction";
	} else if (cor != null && cor == 12) {
	    openExecution.removeVariable("correction");
	    openExecution.removeVariable("count");
	    return "rejection";
	} else {
	    return "normal";
	}
    }

}
