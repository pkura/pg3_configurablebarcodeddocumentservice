package pl.compan.docusafe.parametrization.km.jbpm;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CreateAnswer implements ExternalActivityBehaviour {
    private static final Logger log = LoggerFactory.getLogger(AddToCase.class);

    @Override
    public void execute(ActivityExecution execution) throws Exception {
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument document = OfficeDocument.findOfficeDocument(docId);
        FieldsManager fm = document.getFieldsManager();
        DSUser autor = DSApi.context().getDSUser();
        String summary = "Odpowied� do skargi";

        Map<String, Object> valuesToSet = new HashMap<String, Object>();
        valuesToSet.put("SENDER", fm.getKey("SENDER"));
        valuesToSet.put("STAN_MERYT", fm.getKey("STAN_MERYT"));
        valuesToSet.put("SKARGA", document.getId());
        valuesToSet.put("TRESC_SKARGI", fm.getKey("TRESC_SKARGI"));
        valuesToSet.put("NR_SKARGI", fm.getKey("NR_SKARGI"));

        OutOfficeDocument doc = new OutOfficeDocument();
//        document.setInternal(true);
        doc.setCreatingUser(autor.getName());
        doc.setCurrentAssignmentAccepted(Boolean.FALSE);
        DocumentKind documentKind = DocumentKind.findByCn("odpowiedz");
        doc.setAuthor(autor.getName());
        doc.setSummary(summary != null ? summary : documentKind.getName());
        doc.setAssignedDivision(DSDivision.ROOT_GUID);
        doc.setDocumentKind(documentKind);
        doc.setForceArchivePermissions(false);
        doc.setFolder(Folder.getRootFolder());
        doc.setSummary(summary);
        doc.create();
        setAfterCreate(doc);
        Long newDocumentId = doc.getId();

        documentKind.setOnly(newDocumentId, valuesToSet, false);
        documentKind.logic().archiveActions(doc, DocumentLogic.TYPE_OUT_OFFICE);

        documentKind.logic().documentPermissions(doc);
        DSApi.context().session().save(doc);

        documentKind.logic().onStartProcess(doc, null);

        doc.setContainingCase(document.getContainingCase(), false);

        TaskSnapshot.updateByDocument(doc);

        execution.takeDefaultTransition();
    }

    @Override
    public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {

    }

    private void setAfterCreate(OfficeDocument document) throws EdmException {
        try {
            java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

            DSUser author = DSApi.context().getDSUser();
            String user = author.getName();
            String fullName = author.getLastname() + " " + author.getFirstname();

            perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

            Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
            perms.addAll(documentPermissions);
            ((AbstractDocumentLogic) document.getDocumentKind().logic()).setUpPermission(document, perms);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
