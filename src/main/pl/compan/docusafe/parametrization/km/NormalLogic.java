package pl.compan.docusafe.parametrization.km;

import com.google.common.collect.Maps;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class NormalLogic extends AbstractDocumentLogic {

    private static NormalLogic instance;
    protected static Logger log = LoggerFactory.getLogger(NormalLogic.class);
    public static NormalLogic getInstance() {
        if (instance == null)
            instance = new NormalLogic();
        return instance;
    }
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();
        for(Field f : fm.getFields()){
            if(f.getDefaultValue() != null){
                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
            }
        }
        toReload.put("DATA_GODZINA_WPLYWU", Calendar.getInstance().getTime());
        toReload.put("DOC_DATE", Calendar.getInstance().getTime());
        toReload.put("WNIOSKODAWCA", DSApi.context().getDSUser().getId());
        
        log.info("toReload = {}", toReload);
        fm.reloadValues(toReload);
    }
    public void documentPermissions(Document document) throws EdmException {
        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
        perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

        for (PermissionBean perm : perms) {
            if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP)) {
                String groupName = perm.getGroupName();
                if (groupName == null) {
                    groupName = perm.getSubject();
                }
                createOrFind(document, groupName, perm.getSubject());
            }
            DSApi.context().grant(document, perm);
            DSApi.context().session().flush();
        }
    }
	
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        as.useFolderResolver(document);
        as.useAvailableProviders(document);
        log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
    }

    public void onAddRevision(Document doc)
    {
        if (AvailabilityManager.isAvailable("reopenWfInOnAddRevision", doc.getDocumentKind().getCn()))
        {
            try
            {
                doc.addRemark(new Remark("Proces wznowiono: Utworzono now� wersj� za陰cznika"));
                //DSApi.context().begin();

                WorkflowFactory.reopenWf(doc.getId());

                OfficeDocument document = OfficeDocument.findOfficeDocument(doc.getId());
                document.getDocumentKind().logic().canReopenDocument(document);

                TaskSnapshot.updateByDocument(document);
                //DSApi.context().commit();

                String[] activityIds = WorkflowFactory.findDocumentTasks(document.getId());
                if (activityIds.length > 1 || activityIds.length <= 0) {
                    log.debug("dla dokumentu przywroconego na liste zadan znaleziono "
                            + activityIds.length + " zadan");
                }
                else
                {

                }
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
                DSApi.context().setRollbackOnly();
            }
        }
    }

    public void onDeletedRevision(Document doc)
    {
        try
        {
            doc.addRemark(new Remark("Proces wznowiono: Usuni皻o za陰cznik"));
            //DSApi.context().begin();

            WorkflowFactory.reopenWf(doc.getId());

            OfficeDocument document = OfficeDocument.findOfficeDocument(doc.getId());
            document.getDocumentKind().logic().canReopenDocument(document);

            TaskSnapshot.updateByDocument(document);
            //DSApi.context().commit();

            String[] activityIds = WorkflowFactory.findDocumentTasks(document.getId());
            if (activityIds.length > 1 || activityIds.length <= 0) {
                log.debug("dla dokumentu przywroconego na liste zadan znaleziono "
                        + activityIds.length + " zadan");
            }
            else
            {

            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            DSApi.context().setRollbackOnly();
        }
    }

    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues, Long documentId)
    {
        Map<String, Object> result = new HashMap<String, Object>();
        if(dictionaryName.equals("MIEJSCE_NOSNIK_DATA"))
        {

                result.put("MIEJSCE_NOSNIK_DATA_DATA_ZDARZENIA", new Date());

        }

        return result;
    }
}
