package pl.compan.docusafe.parametrization.km;

import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.parametrization.ifpan.FakturaKosztowaLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * KmInvoiceLogic
 *
 * @author Micha� Sankowski michal.sankowski@docusafe.pl
 */
public class KmInvoiceLogic extends FakturaKosztowaLogic {
    private final static Logger LOG = LoggerFactory.getLogger(KmInvoiceLogic.class);

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        as.useFolderResolver(document);
        FieldsManager fm = Documents.document(document.getId()).getFieldsManager();
        document.setTitle("Faktura kosztowa nr " + String.valueOf(fm.getValue("NUMER_FAKTURY")));
        document.setDescription("Faktura kosztowa nr " + String.valueOf(fm.getValue("NUMER_FAKTURY")));
    }

    @Override
    public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException {
        TaskListParams ret = new TaskListParams();

        FieldsManager fm = dockind.getFieldsManager(id);
        // Ustawienie statusy dokumentu
        ret.setStatus(fm.getValue("STATUS") != null ? (String) fm.getValue("STATUS") : null);
        ret.setDocumentNumber(fm.getValue("NUMER_FAKTURY") != null ? (String) fm.getValue("NUMER_FAKTURY") : null);
        if (fm.getKey("CONTRACTOR") != null){
            String organizator = (String) DwrDictionaryFacade.dictionarieObjects.get("CONTRACTOR").getValues(fm.getKey("CONTRACTOR").toString()).get("CONTRACTOR_ORGANIZATION");
            ret.setAttribute(organizator != null ? organizator : "", 1);
        }
        return ret;
    }
}
