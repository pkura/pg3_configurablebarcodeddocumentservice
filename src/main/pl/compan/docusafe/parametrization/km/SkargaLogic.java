package pl.compan.docusafe.parametrization.km;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.PersonDictionary;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.zebra.ZebraPrinterManager;
import pl.compan.docusafe.util.*;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.sql.PreparedStatement;
import java.util.*;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

/**
 *
 */
public class SkargaLogic extends AbstractDocumentLogic {

    private static SkargaLogic instance;
    protected static Logger log = LoggerFactory.getLogger(SkargaLogic.class);

    public static SkargaLogic getInstance() {
        if (instance == null)
            instance = new SkargaLogic();
        return instance;
    }

    public void specialSetDictionaryValues(Map<String, ?> dockindFields)
    {
        log.info("Special dictionary values: {}", dockindFields);
        if (dockindFields.keySet().contains("ADD_SENDER"))
        {
            Map<String, FieldData> m = (Map<String, FieldData>) dockindFields.get("ADD_SENDER");
            m.put("DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
            m.put("ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
        }
    }

    public void addSpecialInsertDictionaryValues(String dictionaryName,Map<String, FieldData> values) {
        log.info("Before Insert - Special dictionary: '{}' values: {}",
                dictionaryName, values);
        if (dictionaryName.contains("ADD_SENDER")) {
            Person p = new Person();
            p.createBasePerson(dictionaryName, values);
            try {
                p.create();
            } catch (EdmException e) {
                log.error("", e);
            }
            PersonDictionary.prepareFieldNamesForSenderPerson(p.getId(), dictionaryName, values);
        }
    }

    @Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
    {
        log.info("Before Search - Special dictionary: '{}' values: {}", dictionaryName, values);
        if (dictionaryName.contains("ADD_SENDER"))
        {
            values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
            values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
        }
    }

    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues, Long documentId)
    {
        Map<String, Object> result = new HashMap<String, Object>();
        if(dictionaryName.equals("MIEJSCE_NOSNIK_DATA"))
        {
            result.put("MIEJSCE_NOSNIK_DATA_DATA_ZDARZENIA", new Date());
        }

        return result;
    }
    @Override
    public void setAdditionalValues(FieldsManager fm, Integer valueOf, String activity) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();
        for (Field f : fm.getFields()) {
            if (fm.getKey(f.getCn()) == null && f.getDefaultValue() != null) {
                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
            }
        }
        if ((Integer) fm.getKey("STATUS") == 100) {
            toReload.put("KOM_MERYTORYCZNE", DSDivision.findByCode("MZS").getId());
        }
        fm.reloadValues(toReload);
    }

    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();
        for (Field f : fm.getFields())
            if (f.getDefaultValue() != null)
                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
        toReload.put("DATA_WPLYWU", new Date());
        toReload.put("DATA_ZLOZENIA", new Date());

        fm.reloadValues(toReload);
    }

    public void documentPermissions(Document document) throws EdmException {
        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
        String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
        String documentKindName = document.getDocumentKind().getName();

        perms.add(new PermissionBean(ObjectPermission.READ, documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, documentKindName + " - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP,
                documentKindName + " zalacznik - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, documentKindName
                + " - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP,
                documentKindName + " zalacznik - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, documentKindName
                + " - usuwanie"));
        Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
        perms.addAll(documentPermissions);
        this.setUpPermission(document, perms);
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent("Skarga");
        document.setFolder(folder);
    }


    public void printLabel(Document document, int type, Map<String, Object> dockindKeys) throws EdmException {
        if (!AvailabilityManager.isAvailable("print.barcode.create", document.getDocumentKind().getCn()))
            return;
        String barcode = null;
//    	System.out.println(GlobalPreferences.getUseBarcodePrinter());
//    	;
        if (document instanceof OfficeDocument) {
            barcode = ((OfficeDocument) document).getBarcode();
            if (StringUtils.isEmpty(barcode)) {
                ((OfficeDocument) document).setBarcode(BarcodeUtils.getKoBarcode((OfficeDocument) document));
                barcode = ((OfficeDocument) document).getBarcode();
            }
        }
        Map<String, String> fields = new LinkedHashMap<String, String>();
        fields.put("Numer KO", TextUtils.nullSafeObject(((OfficeDocument) document).getOfficeNumber()));
        fields.put("Przyjmujacy", DSUser.findByUsername(document.getAuthor()).asFirstnameLastname());
        if (document instanceof InOfficeDocument) {
            fields.put("Data przyjecia", DateUtils.formatCommonDate(((InOfficeDocument) document).getCtime()));
        }
        ZebraPrinterManager zm = new ZebraPrinterManager();
        zm.printLabel(barcode, fields);

        PreparedStatement ps = null;
        try {
            ps = DSApi.context().prepareStatement("update dsg_skarga set barcode = ? where document_id = ?");
            ps.setString(1, barcode);
            ps.setLong(2, document.getId());
            ps.executeUpdate();
            ps.close();

            DSApi.context().closeStatement(ps);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            DSApi.context().closeStatement(ps);
        }
    }

    public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException {
        if (doc != null)
        {
           if (acceptationCn.equals("mzs1"))
           {
               setDateColumn(doc, "DATA_PRZEKAZANIA");
               setDateColumn(doc, "TERMIN_NA_ROZPATRZENIE");
           }
           else if  (acceptationCn.equals("mzs-manager"))
           {
               setDateColumn(doc, "DATA_ZAKONCZENIA");
           }
        }
    }

    public static void setDateColumn(Document doc, String dataColumn)
    {
        String updateDsgSkarga = "update dsg_skarga set " + dataColumn + " = ? where document_id = ?";

        PreparedStatement ps = null;
        try {
            ps = DSApi.context().prepareStatement(updateDsgSkarga);
            //ps.setString(1, dataColumn);
            if (dataColumn.equals("TERMIN_NA_ROZPATRZENIE"))
            {
                Calendar c = Calendar.getInstance();
                c.setTime((Date)doc.getFieldsManager().getKey("DATA_WPLYWU"));
                c.add(Calendar.DATE, 14);
                ps.setDate(1, new java.sql.Date(c.getTime().getTime()));
            }
            else
                ps.setDate(1, new java.sql.Date(new Date().getTime()));
            ps.setLong(2, doc.getId());
            ps.executeUpdate();
            ps.close();

            DSApi.context().closeStatement(ps);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            DSApi.context().closeStatement(ps);
        }
    }

    @Override
    public void setAdditionalTemplateValues(long docId, Map<String, Object> values) {
        try
        {
            Document doc = Document.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            fm.initialize();
            Sender sender = ((OfficeDocument)doc).getSender();
            values.put("SENDER", sender);
            values.put("DATA_ZLOZENIA", DateUtils.formatCommonDate((Date)fm.getKey("DATA_ZLOZENIA")));
            values.put("DATA_ZDARZENIA", DateUtils.formatCommonDate((Date)fm.getKey("DATA_ZDARZENIA")));
            //------------------------------

            // czas wygenerowania metryki
            values.put("GENERATE_DATE", DateUtils.formatCommonDate(new java.util.Date()).toString());

            // data dokumentu
            values.put("DOC_DATE", DateUtils.formatCommonDate(doc.getCtime()));

        }
        catch (EdmException e)
        {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
        try
        {
            Map<String, Object> map = Maps.newHashMap();
            map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
            map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
            java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();


            for (DSUser author : DSUser.list(0)) {
                String user = author.getName();
                String fullName = author.asFirstnameLastname();
                perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
                perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
                perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
                perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
                perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));


            }
            Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
            perms.addAll(documentPermissions);
            this.setUpPermission(document, perms);


//            if(document.getAttachments().size()!=0)
//            {
//                for (Attachment att : document.getAttachments())
//                {
//                    DSApi.context().begin();
//                    document.getDocumentKind().logic().getOcrSupport().putToOcr(att.getMostRecentRevision());
//                    DSApi.context().commit();
//                }
//            }

            if (AvailabilityManager.isAvailable("addToWatch")) {
                DSApi.context().watch(URN.create(document));
            }
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
            throw new EdmException(e.getMessage());
        }
    }
}
