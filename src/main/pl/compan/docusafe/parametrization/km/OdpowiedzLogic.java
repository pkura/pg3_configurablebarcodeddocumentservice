package pl.compan.docusafe.parametrization.km;

import com.google.common.collect.Maps;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.util.Date;
import java.util.Map;
import java.util.Set;

public class OdpowiedzLogic extends AbstractDocumentLogic {

    private static OdpowiedzLogic instance;
    protected static Logger log = LoggerFactory.getLogger(OdpowiedzLogic.class);

    public static OdpowiedzLogic getInstance() {
        if (instance == null)
            instance = new OdpowiedzLogic();
        return instance;
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent("Odpowied� do skargi");
        document.setFolder(folder);
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {
        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
        String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
        String documentKindName = document.getDocumentKind().getName();

        perms.add(new PermissionBean(ObjectPermission.READ, documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, documentKindName + " - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP,
                documentKindName + " zalacznik - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, documentKindName
                + " - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP,
                documentKindName + " zalacznik - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, documentKindName
                + " - usuwanie"));
        Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
        perms.addAll(documentPermissions);
        this.setUpPermission(document, perms);
    }

    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
        try {
            Map<String, Object> map = Maps.newHashMap();
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
            java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

//            DSUser author = DSApi.context().getDSUser();

            for (DSUser author : DSUser.list(0)) {
                String user = author.getName();
                String fullName = author.asFirstnameLastname();
                perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
                perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
                perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
                perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
                perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));


            }
            Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
            perms.addAll(documentPermissions);
            this.setUpPermission(document, perms);


            if (AvailabilityManager.isAvailable("addToWatch")) {
                DSApi.context().watch(URN.create(document));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new EdmException(e.getMessage());
        }
    }

    @Override
    public void setAdditionalTemplateValues(long docId, Map<String, Object> values) {
        try {
            Document doc = Document.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            fm.initialize();

            FieldsManager skargaFM = Document.find((Long) fm.getKey("SKARGA"), false).getFieldsManager();

            Sender sender = ((OfficeDocument) doc).getSender();
            values.put("SENDER", sender);
            values.put("DATA_ZLOZENIA", DateUtils.formatCommonDate((Date) (skargaFM.getKey("DATA_ZLOZENIA"))));
            values.put("DATA_ZDARZENIA", DateUtils.formatCommonDate((Date) (skargaFM.getKey("DATA_ZDARZENIA"))));
            //------------------------------

            // czas wygenerowania metryki
            values.put("GENERATE_DATE", DateUtils.formatCommonDate(new java.util.Date()).toString());

            // data dokumentu
            values.put("DOC_DATE", DateUtils.formatCommonDate(doc.getCtime()));
            Object nrOdpowiedzi = ((OfficeDocument) doc).getCaseDocumentId();
            if (nrOdpowiedzi != null)
                values.put("NR_ODPOWIEDZI", nrOdpowiedzi);

        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }

    public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException {
        if (doc != null) {
            if (acceptationCn.equals("mbz")) {
                SkargaLogic.setDateColumn(Document.find((Long) doc.getFieldsManager().getKey("SKARGA")), "DATA_WYSLANIA_ODP");
            }
        }
    }
}
