package pl.compan.docusafe.parametrization.km;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.primitives.Ints;
import com.opensymphony.webwork.ServletActionContext;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.km.dao.KMBudgetPosition;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: kk
 * Date: 22.07.13
 * Time: 17:25
 * To change this template use File | Settings | File Templates.
 */
public class CostInvoiceLogic extends AbstractDocumentLogic {

    protected static final Logger log = LoggerFactory.getLogger(CostInvoiceLogic.class);

    public static final String DOCKIND_CN = "km_costinvoice";
    public static final String TEMPLATE_DOCKIND_CN = "km_costinvoice_tp";

    private static final String DWR_PREFIX = "DWR_";

    public static final String BP_FIELD = "BP";
    static final String BP_FIELD_WITH_LINK = BP_FIELD+"_";
    private static final String KWOTA_NETTO_FIELD = "KWOTA_NETTO";
    private static final String KWOTA_BRUTTO_FIELD = "KWOTA_BRUTTO";
    private static final String RODZAJ_DZIALALNOSCI_FIELD = "RODZAJ_DZIALALNOSCI";
    private static final String KWOTA_BRUTTO_SLOWNIE = "KWOTA_BRUTTO_SLOWNIE";
    private static final String TERMIN_PLATNOSCI_FIELD = "TERMIN_PLATNOSCI";
    private static final String DATA_WYSTAWIENIA_FIELD = "DATA_WYSTAWIENIA";
    private static final String DATA_WPLYWU_FIELD = "DATA_WPLYWU_FAKTURY";
    static final String BP_DWR_FIELD = DWR_PREFIX+BP_FIELD;
    private static final String TERMIN_PLATNOSCI_DWR_FIELD = DWR_PREFIX+TERMIN_PLATNOSCI_FIELD;
    private static final String DATA_WPLYWU_DWR_FIELD = DWR_PREFIX+DATA_WPLYWU_FIELD;
    private static final String DATA_WYSTAWIENIA_DWR_FIELD = DWR_PREFIX+DATA_WYSTAWIENIA_FIELD;
    private static final String KWOTA_NETTO_DWR_FIELD = DWR_PREFIX+"KWOTA_NETTO";
    private static final String KWOTA_WALUTOWA_DWR_FIELD = DWR_PREFIX+"KWOTA_WALUTOWA";
    private static final String KWOTA_KURS_DWR_FIELD = DWR_PREFIX+"KWOTA_KURS";
    static final String KWOTA_BRUTTO_DWR_FIELD = DWR_PREFIX+KWOTA_BRUTTO_FIELD;
    private static final String KWOTA_VAT_DWR_FIELD = DWR_PREFIX+"KWOTA_VAT";
    public static final String KWOTA_POZOSTALA_MPK_DWR_FIELD = DWR_PREFIX+"KWOTA_POZOSTALA_MPK";

    private static final String DATA_PLATNOSCI_ERROR_MSG = "Data p�atno�ci nie mo�e by� wcze�niejsza ni� data wystawienia. ";
    private static final String DATA_WPLYWU_ERROR_MSG = "Data wp�ywu nie mo�e by� wcze�niejsza ni� data wystawienia. ";

    static final EnumValues EMPTY_NOT_HIDDEN_ENUM_VALUE = new EnumValues(new HashMap<String,String>(){{put("","-- wybierz --");}}, new ArrayList<String>(){{add("");}});
    static final EnumValues NOT_APPLICABLE_ENUM_VALUE = new EnumValues(new HashMap<String,String>(), new ArrayList<String>());
    static final EnumValues EMPTY_ENUM_VALUE = new EnumValues(new HashMap<String,String>(), new ArrayList<String>());
    public static final String BP_DIC_PREFIX = BP_FIELD+"_";
    public static final String SENDER_FIELD = "SENDER";
    public static final String TEMPLATE_VALUES_SESSION_MAP = "TEMPLATE_VALUES";

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        DocumentKind kind = document.getDocumentKind();
        Object templateValues = null;
        HttpServletRequest request = ServletActionContext.getRequest();
        HttpSession session = request.getSession();
        if (session != null) {
            templateValues = session.getAttribute(TEMPLATE_VALUES_SESSION_MAP);
        } else {
            log.error("session");
        }
        if (templateValues != null) {
            Map<String, Object> templateMapValues = (Map<String, Object>) templateValues;
            Map<String, Object> templateMapProcessedValues = Maps.newLinkedHashMap();
            for (String fieldCn : templateMapValues.keySet()) {
                pl.compan.docusafe.core.dockinds.field.Field field = kind.getFieldByCn(fieldCn);

                if (pl.compan.docusafe.core.dockinds.field.Field.DICTIONARY.equals(field.getType())) {
                    if (BP_FIELD.equals(fieldCn)) {
                        try {
                            List<Long> bpIdsCreated = createBudgetPosition((List<Long>) templateMapValues.get(fieldCn));
                            templateMapProcessedValues.put(fieldCn, bpIdsCreated);
                        } catch (EdmException e) {
                            log.error(e.getMessage(), e);
                        }
                    } else {
                        //TODO
                        log.error("Unsupported");
                    }
                } else {
                    templateMapProcessedValues.put(fieldCn, templateMapValues.get(fieldCn));

                }
            }
            kind.setOnly(document.getId(), templateMapProcessedValues);
            session.removeAttribute(TEMPLATE_VALUES_SESSION_MAP);
        }
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {
        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
        String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
        String documentKindName = document.getDocumentKind().getName();

        perms.add(new PermissionBean(ObjectPermission.READ, "KM_" + documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, "KM_ - " + documentKindName + " - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "KM_" + documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "KM_ - " + documentKindName + " zalacznik - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "KM_" + documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "KM_ - " + documentKindName + " - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "KM_" + documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "KM_ - " + documentKindName + " zalacznik - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, "KM_" + documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "KM_ - " + documentKindName + " - usuwanie"));
        Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
        perms.addAll(documentPermissions);
        this.setUpPermission(document, perms);
    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        super.setInitialValues(fm, type);

        Map<String, Object> toReload = Maps.newHashMap();
        toReload.put("DATA_REJESTRACJI", new Date());
        toReload.put("OKRES_SPRAWOZDAWCZY", DateUtils.formatYear_Month(new Date()));

        fm.reloadValues(toReload);
    }

    @Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        StringBuilder msgBuilder = new StringBuilder();

        if (values.get(KWOTA_NETTO_DWR_FIELD) != null) {
            BigDecimal plnNetValue = DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.MULTIPLY, 2, true, values, KWOTA_WALUTOWA_DWR_FIELD, KWOTA_KURS_DWR_FIELD);
            values.get(KWOTA_NETTO_DWR_FIELD).setMoneyData(plnNetValue);

            if (values.get(KWOTA_POZOSTALA_MPK_DWR_FIELD) != null) {
                BigDecimal amount = plnNetValue.subtract(DwrUtils.sumDictionaryMoneyFields(values, BP_FIELD, "AMOUNT"));
                values.get(KWOTA_POZOSTALA_MPK_DWR_FIELD).setMoneyData(amount);

                if (amount.compareTo(BigDecimal.ZERO) == -1) {
                    msgBuilder.append("Suma rozpisanych kwot nie mo�e by� wi�ksza od kwoty netto faktury. ");
                }
            }
        }

        if (values.get(KWOTA_BRUTTO_DWR_FIELD) != null) {
            values.get(KWOTA_BRUTTO_DWR_FIELD).setMoneyData(DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.ADD, 2, false, values, KWOTA_NETTO_DWR_FIELD, KWOTA_VAT_DWR_FIELD));
        }

        // data platnosci nie moze byc wczesniej niz data wystawienia
        msgBuilder.append(validateDate(values.get(DATA_WYSTAWIENIA_DWR_FIELD), values.get(TERMIN_PLATNOSCI_DWR_FIELD), DATA_PLATNOSCI_ERROR_MSG));

        // data wp�ywu nie mo�e by� wcze�niejsza ni� data wystawienia
        msgBuilder.append(validateDate(values.get(DATA_WYSTAWIENIA_DWR_FIELD), values.get(DATA_WPLYWU_DWR_FIELD), DATA_WPLYWU_ERROR_MSG));

        Map.Entry<String, FieldData> sender = DwrUtils.getSenderField(values);
        String senderFieldCn = sender != null ? sender.getKey() : "";
        if(senderFieldCn.equals("DWR_SZABLON_CREATE")) {
            createTemplate(values, fm, msgBuilder);
        }
        if(senderFieldCn.equals("DWR_SZABLON_LOAD")) {
            loadTemplate(values, msgBuilder);
        }


        if (msgBuilder.length()>0)
        {
            values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
            return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
        }

        return null;
    }

    protected StringBuilder validateDate(Object start, Object finish, String errorMsg) {
        StringBuilder msg = new StringBuilder();
        try {
            if (start != null && finish != null) {
                Date startDate = null;
                Date finishDate = null;

                if (start instanceof Date) {
                    startDate = (Date) start;
                } else if (start instanceof FieldData && ((FieldData) start).getData() instanceof Date) {
                    startDate = ((FieldData) start).getDateData();
                }

                if (finish instanceof Date) {
                    finishDate = (Date) finish;
                } else if (finish instanceof FieldData && ((FieldData) finish).getData() instanceof Date) {
                    finishDate = ((FieldData) finish).getDateData();
                }

                if (startDate != null && finishDate != null && startDate.after(finishDate)) {
                    msg.append(errorMsg);
                }

            }
        } catch (Exception e) {
            log.error("CAUGHT: "+e.getMessage(),e);
        }
        return msg;
    }

    private void loadTemplate(Map<String, FieldData> values, StringBuilder msgBuilder) throws EdmException {
        Long templateId = DwrUtils.extractIdFromDictonaryField(values, "SZABLON").isEmpty() ? null : DwrUtils.extractIdFromDictonaryField(values, "SZABLON").get(0);
        if (templateId != null) {
            try {
                DSApi.openAdmin();

                FieldsManager fm = Document.find(templateId, false).getFieldsManager();
                Map<String, Object> templateFieldsValues = fm.getFieldValues();

                Map<String, Object> hiddenFields = Maps.newLinkedHashMap();


                DocumentKind docKind = DocumentKind.findByCn(TEMPLATE_DOCKIND_CN);
                for (pl.compan.docusafe.core.dockinds.field.Field field : docKind.getFields()) {
                    String fieldCn = field.getCn();
                    Object templateData = templateFieldsValues.get(fieldCn);
                    String templateStringData = templateData != null ? templateData.toString() : "";

                    FieldData fieldData = values.get(DWR_PREFIX + fieldCn);
                    if (fieldData != null) {
                        if (SENDER_FIELD.equals(fieldCn)) {
                            Integer personId = Ints.tryParse(templateStringData);
                            if (personId != null) {
                                Map<String, FieldData> contractorFieldsMap = fieldData.getDictionaryData();
                                Person contractor = Person.find(personId);
                                Map<String, FieldData> contractorMod = new HashMap<String, FieldData>();
                                FieldData temp;
                                for (String contField : contractorFieldsMap.keySet()) {
                                    temp = contractorFieldsMap.get(contField);
                                    temp.setData(contractor.getParam(contField, SENDER_FIELD + "_"));
                                    contractorMod.put(contField, temp);
                                }
                                fieldData.setDictionaryData(contractorMod);
                            }
                        } else if (BP_FIELD.equals(fieldCn)) {
                            try {
                                DSApi.context().begin();

                                List<Long> bpIdsCreated = createBudgetPosition((List<Long>) templateData);
                                values.get(DWR_PREFIX+BP_FIELD).setStringData(Joiner.on(",").join(bpIdsCreated));
                                DSApi.context().commit();
                            } catch (EdmException e) {
                                DSApi.context().rollback();
                                log.error(e.getMessage(), e);
                            }
                        } else {
                            switch (fieldData.getType()) {
                                case STRING:
                                case TEXTAREA:
                                    fieldData.setData(templateStringData);
                                    break;
                                case ENUM:
                                    fieldData.getEnumValuesData().setSelectedId(templateStringData);
                                    break;
                                case DICTIONARY:
                                    break;
                                default:
                                    fieldData.setData(templateData);
                                    break;
                            }
                        }

                    } else {
                        hiddenFields.put(fieldCn, templateData);
                    }
                }
                if (!hiddenFields.isEmpty()) {
                    HttpSession session = WebContextFactory.get().getSession();
                    session.setAttribute(TEMPLATE_VALUES_SESSION_MAP, hiddenFields);
                }
                msgBuilder.append("Pomyslnie za�adowano szablon. ");

            } catch (EdmException e) {
                log.error(e.getMessage(), e);
                msgBuilder.append("Wyst�pi� b��d podczas �adowania szablonu. ");
            } finally {
                DSApi.close();
            }


        } else {
            log.error("Wrong button!");
        }
    }

    private void createTemplate(Map<String, FieldData> values, FieldsManager fm, StringBuilder msgBuilder) throws EdmException {
        String templateName = DwrUtils.getStringValue(values, "DWR_TEMPLATE_NAME");
        if (!Strings.isNullOrEmpty(templateName)) {
            Map<String,Object> createdTemplateValues = Maps.newLinkedHashMap();

            try {
                DSApi.openAdmin();
                DSApi.context().begin();

                InOfficeDocument doc = new InOfficeDocument();
                DocumentKind docKind = DocumentKind.findByCn(TEMPLATE_DOCKIND_CN);

                for (pl.compan.docusafe.core.dockinds.field.Field field : docKind.getFields()) {
                    String fieldCn = field.getCn();
                    FieldData fieldData = values.get(DWR_PREFIX+fieldCn);
                    if (fieldData != null) {
                        switch (fieldData.getType()) {
                            case DICTIONARY:
                                List<Long> ids = DwrUtils.extractIdFromDictonaryField(fieldData, fieldCn);
                                if (BP_FIELD.equals(fieldCn)) {
                                    List<Long> bpIdsCreated = createBudgetPosition(ids);
                                    createdTemplateValues.put(fieldCn, bpIdsCreated);

                                } else  if (SENDER_FIELD.equals(fieldCn)) {
                                    if (!ids.isEmpty()) {
                                        doc.setSender(createSender(ids.get(0)));
                                    } else {
                                        //TODO
                                        //kontrahent w trakcie dodawania
                                        log.error("unsupported");
                                    }
                                } else {
                                    //TODO
                                    //inny s�ownik
                                    log.error("unsupported");
                                }

                                break;
                            case BOOLEAN:
                                createdTemplateValues.put(fieldCn, fieldData.getBooleanData());
                                break;
                            case DATE:
                                createdTemplateValues.put(fieldCn, fieldData.getDateData());
                                break;
                            default:
                                createdTemplateValues.put(fieldCn, fieldData.getData());
                        }
                    } else {
                        createdTemplateValues.put(fieldCn, fm.getKey(fieldCn));
                    }
                }

                doc.setCreatingUser(DSApi.context().getPrincipalName());
                doc.setIncomingDate(GlobalPreferences.getCurrentDay());
                doc.setSummary(docKind.getName());
                doc.setDocumentKind(docKind);
                doc.create();
                Long newDocumentId = doc.getId();
                docKind.setOnly(newDocumentId, createdTemplateValues);
                DSApi.context().session().save(doc);

                //TODO
                //niedziala!
                if (values.get(DWR_PREFIX + "TEMPLATE_NAME") != null) {
                    values.get(DWR_PREFIX + "TEMPLATE_NAME").setStringData("");
                }

                DSApi.context().commit();
                msgBuilder.append("Pomy�lnie utworzono nowy szablon. ");
            } catch (Exception e) {
                msgBuilder.append("Wyst�pi� b��d podczas tworzenia nowego szablonu. ");
                log.error(e.getMessage(), e);
                DSApi.context().rollback();
            } finally {
                DSApi.close();
            }


        } else {
            msgBuilder.append("Prosz� poda� nazw� szablonu przed dodaniem. ");
        }
    }

    private List<Long> createBudgetPosition(List<Long> ids) throws EdmException {
        List<Long> bpIdsCreated = Lists.newArrayList();
        KMBudgetPosition instance = KMBudgetPosition.getInstance();
        for (Long bpId : ids) {
            if (bpId != null) {
                KMBudgetPosition bpFromTemplate = instance.find(bpId);
                KMBudgetPosition newBp = new KMBudgetPosition(bpFromTemplate);
                newBp.setAmount(null);
                newBp.save();
                bpIdsCreated.add(newBp.getId());
            }
        }
        return bpIdsCreated;
    }

    private Sender createSender(Long id) throws EdmException {
        Person person = Person.find(id.longValue());
        Sender sender = new Sender();
        sender.fromMap(person.toMap());
        sender.setDictionaryGuid("rootdivision");
        sender.setDictionaryType(Person.DICTIONARY_SENDER);
        sender.setBasePersonId(person.getId());
        sender.create();
        return sender;
    }

    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) {
        Map<String, Object> results = new HashMap<String, Object>();

        try {
            if (dictionaryName.equals(BP_FIELD)) {
                setBudgetPositionDictionary(values, results);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return results;
    }

    private void setBudgetPositionDictionary(Map<String, FieldData> values, Map<String, Object> results) throws EdmException {
        List<String> accountField = null;
        List<String> accountTemplateList = Lists.newArrayList();
        Map<String, String> templateData = Maps.newHashMap();

        String costCn = DwrUtils.getSelectedEnumCn(values, results, BP_DIC_PREFIX + "COST", "dsd_km_cost");


        if (costCn != null) {
            //TODO
            //zewnetrzne zrodlo danych
            if (costCn.startsWith("4")) {
                accountField = Lists.newArrayList("COST", "MPK", "SUBJECT_KIND", "ANALITYCS", "PROJECT", "TAX", "BP");
            }
            if (costCn.startsWith("5")) {
                accountField = Lists.newArrayList("COST", "ACTION_KIND", "COST_KIND", "PROJECT", "ANALITYCS", "VEHICLE");
            }
            if (costCn.startsWith("76")) {
                accountField = Lists.newArrayList("COST", "MPK", "SUBJECT_KIND", "ANALITYCS", "TAX", "BP");
            }
            if (costCn.startsWith("75")) {
                accountField = Lists.newArrayList("COST", "MPK", "SUBJECT_KIND", "ANALITYCS", "COMPLETED", "TAX", "BP");
            }

            putEnumItemsIntoResults(values, results, accountField);

            templateData.put("COST", DwrUtils.getSelectedEnumCn(values, results, BP_DIC_PREFIX + "COST", "dsd_km_cost"));
            templateData.put("MPK", DwrUtils.getSelectedEnumCn(values, results, BP_DIC_PREFIX + "MPK", "dsd_km_mpk"));
            templateData.put("SUBJECT_KIND", DwrUtils.getSelectedEnumCn(values, results, BP_DIC_PREFIX + "SUBJECT_KIND", "dsd_km_subject_kind"));
            templateData.put("ANALITYCS", DwrUtils.getSelectedEnumCn(values, results, BP_DIC_PREFIX + "ANALITYCS", "dsd_km_analitycs"));
            templateData.put("PROJECT", DwrUtils.getSelectedEnumCn(values, results, BP_DIC_PREFIX + "PROJECT", "dsd_km_project"));
            templateData.put("BP", DwrUtils.getSelectedEnumCn(values, results, BP_DIC_PREFIX + "BP", "dsd_km_bp"));
            templateData.put("VEHICLE", DwrUtils.getSelectedEnumCn(values, results, BP_DIC_PREFIX + "VEHICLE", "dsd_km_vehicle"));
            templateData.put("ACTION_KIND", DwrUtils.getSelectedEnumCn(values, results, BP_DIC_PREFIX + "ACTION_KIND", "dsd_km_action_kind"));
            templateData.put("COST_KIND", DwrUtils.getSelectedEnumCn(values, results, BP_DIC_PREFIX + "COST_KIND", "dsd_km_cost_kind"));
            templateData.put("COMPLETED", DwrUtils.getSelectedEnumCn(values, results, BP_DIC_PREFIX + "COMPLETED", "dsd_km_completed"));
            templateData.put("TAX", DwrUtils.getSelectedEnumCn(values, results, BP_DIC_PREFIX + "TAX", "dsd_km_tax"));

            for (String item : accountField) {
                accountTemplateList.add(templateData.get(item) != null ? templateData.get(item) : "...");
            }
        }


        results.put("BP_ACCOUNT_VIEW", Joiner.on(" - ").join(accountTemplateList));
    }

    private String getValueFromSession(String fieldCn) {
        WebContext dwrFactory = WebContextFactory.get();
        if (dwrFactory.getSession().getAttribute("dwrSession") instanceof Map<?, ?>) {
            Map<String, Object> dwrSession = (Map<String, Object>) dwrFactory.getSession().getAttribute("dwrSession");
            if (dwrSession.get("FIELDS_VALUES") instanceof Map<?, ?>) {
                Map<String,Object> fieldsValues = (Map<String, Object>) dwrSession.get("FIELDS_VALUES");

                Object field = fieldsValues.get(fieldCn);
                if (field != null) {
                    return field.toString();
                }
            }
        }
        return null;
    }

    private void putEnumItemsIntoResults(Map<String, FieldData> values, Map<String, Object> results, List<String> accountField) {
        if (accountField.contains("ANALITYCS")) {
            DwrUtils.setRefValueEnumOptions(values, results, "COST", BP_FIELD_WITH_LINK, "ANALITYCS", "dsd_km_analitycs", null, EMPTY_NOT_HIDDEN_ENUM_VALUE);
        } else {
            results.put(BP_DIC_PREFIX + "ANALYTICS", EMPTY_ENUM_VALUE);
        }
/*        if (accountField.contains("MPK")) {
            DwrUtils.setRefValueEnumOptions(values, results, "PRACOWNIK_MERYTORYCZNY", BP_FIELD_WITH_LINK, "MPK", "dsd_km_mpk_mapped_to_user", getValueFromSession("PRACOWNIK_MERYTORYCZNY"), EMPTY_NOT_HIDDEN_ENUM_VALUE, "_1", "");
        } else {
            results.put(BP_DIC_PREFIX + "MPK", EMPTY_ENUM_VALUE);
        }*/

        putCorrectEnumFieldToResults(values, results, accountField, "MPK", "dsd_km_mpk");
        putCorrectEnumFieldToResults(values, results, accountField, "SUBJECT_KIND", "dsd_km_subject_kind");
        putCorrectEnumFieldToResults(values, results, accountField, "PROJECT", "dsd_km_project");
        putCorrectEnumFieldToResults(values, results, accountField, "BP", "dsd_km_bp");
        putCorrectEnumFieldToResults(values, results, accountField, "VEHICLE", "dsd_km_vehicle");
        putCorrectEnumFieldToResults(values, results, accountField, "ACTION_KIND", "dsd_km_action_kind");
        putCorrectEnumFieldToResults(values, results, accountField, "COST_KIND", "dsd_km_cost_kind");
        putCorrectEnumFieldToResults(values, results, accountField, "ACTION_KIND", "dsd_km_action_kind");
        putCorrectEnumFieldToResults(values, results, accountField, "COMPLETED", "dsd_km_completed");
        putCorrectEnumFieldToResults(values, results, accountField, "TAX", "dsd_km_tax");
    }

    private void putCorrectEnumFieldToResults(Map<String, FieldData> values, Map<String, Object> results, List<String> accountField, String fieldCn, String tableName) {
        if (accountField.contains(fieldCn)) {
            DataBaseEnumField base = DataBaseEnumField.tableToField.get(tableName).iterator().next();
            results.put(BP_DIC_PREFIX + fieldCn, base.getEnumValuesForForcedPush(values, BP_DIC_PREFIX));
        } else {
            results.put(BP_DIC_PREFIX + fieldCn, EMPTY_ENUM_VALUE);
        }
    }

    @Override
    public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException {
/*        if (fieldValues.get("BP_IDS") != null) {
            Iterable<Long> templateBpIds = Lists.transform(Lists.newArrayList(Splitter.on(",").split(fieldValues.get("BP_IDS").toString())), new Function<String, Long>() {
                @Override
                public Long apply(String input) {
                    try {
                        return Long.valueOf(input);
                    } catch (NumberFormatException e) {
                        log.error(e.getMessage(), e);
                        return null;
                    }
                }
            });

            List<Object> bpIdsCreated = Lists.newArrayList();
            for (Long bpId : (List<Long>)templateBpIds) {
                if (bpId != null) {
                    KMBudgetPosition instance = KMBudgetPosition.getInstance();
                    KMBudgetPosition bpFromTemplate = instance.find(bpId);

                    KMBudgetPosition newBp = new KMBudgetPosition(bpFromTemplate);
                    newBp.save();
                    bpIdsCreated.add(newBp.getId());
                }
            }
            DocumentKind.saveMultipleData(documentId, BP_FIELD, bpIdsCreated, kind.getMultipleTableName());
        }*/
/*        Object templateValues = null;
        HttpServletRequest request = ServletActionContext.getRequest();
        HttpSession session = request.getSession();
        if (session != null) {
            templateValues = session.getAttribute(TEMPLATE_VALUES_SESSION_MAP);
        } else {
            log.error("session");
        }
        if (templateValues != null) {
            Map<String, Object> templateMapValues = (Map<String, Object>) templateValues;
            Map<String, Object> templateMapProcessedValues = Maps.newLinkedHashMap();
            for (String fieldCn : templateMapValues.keySet()) {
                pl.compan.docusafe.core.dockinds.field.Field field = kind.getFieldByCn(fieldCn);

                if (pl.compan.docusafe.core.dockinds.field.Field.DICTIONARY.equals(field.getType())) {
                    if (BP_FIELD.equals(fieldCn)) {
                        try {
                            List<Long> bpIdsCreated = Lists.newArrayList();
                            for (Long bpId : (List<Long>) templateMapValues.get(fieldCn)) {
                                if (bpId != null) {
                                    KMBudgetPosition instance = KMBudgetPosition.getInstance();
                                    KMBudgetPosition bpFromTemplate = instance.find(bpId);

                                    KMBudgetPosition newBp = new KMBudgetPosition(bpFromTemplate);
                                    newBp.save();
                                    bpIdsCreated.add(newBp.getId());
                                }
                            }
                            templateMapProcessedValues.put(fieldCn, bpIdsCreated);
                        } catch (EdmException e) {
                            log.error(e.getMessage(), e);
                        }
                    } else {
                        log.error("Unsupported");
                    }
                } else {
                    templateMapProcessedValues.put(fieldCn, templateMapValues.get(fieldCn));

                }
            }
            kind.setOnly(documentId, templateMapProcessedValues);
        }*/

    }

    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
        if ("supervisor_accept".equals(acceptationCn)) {
            try {
                if (openExecution.getVariable("substantive_username") != null) {
                    String username = openExecution.getVariable("substantive_username").toString();
                    DSUser user = DSUser.findByUsername(username);
                    if (user.getSupervisor() != null) {
                        String supervisorName = user.getSupervisor().getName();
                        assignable.addCandidateUser(supervisorName);
                        return true;
                    } else {
                        throw new AssigningHandledException("Osoba opisu merytorycznego nie posiada zdefiniowanego bezpo�redniego prze�o�onego.");
                    }
                } else {
                    throw new EdmException("brak username w zmiennych");
                }
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
                log.error("Nie znaleziono osoby do przypisania, przypisano do admin'a");
                if (e instanceof AssigningHandledException) {
                    addRemarkAndAssignToAdmin(doc, assignable, e.getMessage(), log);
                } else {
                    assignable.addCandidateUser("admin");
                }
                return true;
            }

        }

        if ("dept_manager_accept".equals(acceptationCn)) {
            try {
                if (openExecution.getVariable("substantive_username") != null) {
                    String username = openExecution.getVariable("substantive_username").toString();
                    DSUser user = DSUser.findByUsername(username);
                    if (user.getExtension() != null) {
                        assignable.addCandidateUser(user.getExtension());
                        return true;
                    } else {
                        throw new AssigningHandledException("Osoba opisu merytorycznego nie posiada zdefiniowanego Dyrektora Biura.");
                    }
                } else {
                    throw new EdmException("brak username w zmiennych");
                }
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
                log.error("Nie znaleziono osoby do przypisania, przypisano do admin'a");
                if (e instanceof AssigningHandledException) {
                    addRemarkAndAssignToAdmin(doc, assignable, e.getMessage(), log);
                } else {
                    assignable.addCandidateUser("admin");
                }
                return true;
            }
        }

        return false;
    }


    /**
     * Add message to document remarks and assigne to admin
     * @param doc
     * @param assignable
     * @param message
     */
    protected void addRemarkAndAssignToAdmin(OfficeDocument doc, Assignable assignable, String message, Logger log) {
        String adminName = "admin";
        try {
            adminName = GlobalPreferences.getAdminUsername();
            doc.addRemark(new Remark(message,"admin"));
        } catch (Exception e1) {
            log.error(e1.getMessage(), e1);
        }
        assignable.addCandidateUser(adminName);
    }
}
