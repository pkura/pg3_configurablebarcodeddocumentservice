package pl.compan.docusafe.parametrization.km;

import pl.compan.docusafe.core.EdmException;

/**
 * Used to throws exception, for which, a message are added to the document remarks
 */
class AssigningHandledException extends EdmException {
    public AssigningHandledException(String message) {
        super(message);
    }
}