package pl.compan.docusafe.parametrization.km;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.zebra.ZebraPrinterManager;
import pl.compan.docusafe.util.*;

import java.sql.PreparedStatement;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 */
public class WniosekOTelLogic extends AbstractDocumentLogic {

    private static WniosekOTelLogic instance;
    protected static Logger log = LoggerFactory.getLogger(WniosekOTelLogic.class);
    public static WniosekOTelLogic getInstance() {
        if (instance == null)
            instance = new WniosekOTelLogic();
        return instance;
    }
    public void printLabel(Document document, int type, Map<String, Object> dockindKeys) throws EdmException {
        if (!AvailabilityManager.isAvailable("print.barcode.create", document.getDocumentKind().getCn()))
            return;
        String barcode = null;

        if (document instanceof OfficeDocument) {
            barcode = ((OfficeDocument) document).getBarcode();
            if (StringUtils.isEmpty(barcode)) {
                ((OfficeDocument) document).setBarcode(BarcodeUtils.getKoBarcode((OfficeDocument) document));
                barcode = ((OfficeDocument) document).getBarcode();
            }
        }
        Map<String, String> fields = new LinkedHashMap<String, String>();
        fields.put("Numer KO", TextUtils.nullSafeObject(((OfficeDocument) document).getOfficeNumber()));
        fields.put("Rejestrujacy", DSUser.findByUsername(document.getAuthor()).asFirstnameLastname());
        if (document instanceof InOfficeDocument) {
            fields.put("Data rejestracji", DateUtils.formatCommonDate(((InOfficeDocument) document).getCtime()));
        }
        ZebraPrinterManager zm = new ZebraPrinterManager();
        zm.printLabel(barcode, fields);
        PreparedStatement ps = null;
        try {
            ps = DSApi.context().prepareStatement("update dsg_wiosek set barcode = ? where document_id = ?");
            ps.setString(1, barcode);
            ps.setLong(2, document.getId());
            ps.executeUpdate();
            ps.close();

            DSApi.context().closeStatement(ps);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            DSApi.context().closeStatement(ps);
        }
    }

    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();
        for(Field f : fm.getFields()){
            if(f.getDefaultValue() != null){
                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
            }
        }
        toReload.put("DATA_GODZINA_WPLYWU", Calendar.getInstance().getTime());
        toReload.put("DOC_DATE", Calendar.getInstance().getTime());
        toReload.put("WNIOSKODAWCA", DSApi.context().getDSUser().getId());
        
        log.info("toReload = {}", toReload);
        fm.reloadValues(toReload);
    }

    public void documentPermissions(Document document) throws EdmException {
        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
        perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

        for (PermissionBean perm : perms) {
            if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP)) {
                String groupName = perm.getGroupName();
                if (groupName == null) {
                    groupName = perm.getSubject();
                }
                createOrFind(document, groupName, perm.getSubject());
            }
            DSApi.context().grant(document, perm);
            DSApi.context().session().flush();
        }
    }
	
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        as.useFolderResolver(document);
        as.useAvailableProviders(document);
        log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
    }
}
