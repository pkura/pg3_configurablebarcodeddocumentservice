package pl.compan.docusafe.parametrization.complaint;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.measure.ProcessParameterBean;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ComplaintLogic extends AbstractDocumentLogic {
    private final static Logger LOG = LoggerFactory.getLogger(ComplaintLogic.class);


    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        as.getFolderInserter()
           .subfolder("Reklamacje")
           .insert(document);

        document.setTitle("Reklamacja");
    }

    @Override
    public void onStartProcess(OfficeDocument document) {
        LOG.info("START PROCESS !!!");
        try {
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }

    @Override
    public TaskListParams getTaskListParams(DocumentKind kind, long documentId) throws EdmException {
        TaskListParams ret = new TaskListParams();
        ret.setCategory("Reklamacja");
        ret.setAttribute(kind.getFieldsManager(documentId).getValue("DAYS_LEFT_TEXT")+"", 1);
        return ret;
    }

   public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

		perms.add(new PermissionBean(ObjectPermission.READ,"UMOWA_PRESENTATION_READ",ObjectPermission.GROUP,"Dokument "+document.getDocumentKind().getName()+" - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"UMOWA_PRESENTATION_ATT_READ",ObjectPermission.GROUP,"Dokument "+document.getDocumentKind().getName()+" zalacznik - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY,"UMOWA_PRESENTATION_MODIFY",ObjectPermission.GROUP,"Dokument "+document.getDocumentKind().getName()+" - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"UMOWA_PRESENTATION_ATT_MODIFY",ObjectPermission.GROUP,"Dokument "+document.getDocumentKind().getName()+" zalacznik - modyfikacja"));

   	 	this.setUpPermission(document, perms);

	}

}