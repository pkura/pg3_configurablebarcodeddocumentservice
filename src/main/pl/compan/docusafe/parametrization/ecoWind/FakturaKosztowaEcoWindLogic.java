package pl.compan.docusafe.parametrization.ecoWind;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import com.google.common.collect.Maps;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.DwrFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.beck.ZamowienieLogic;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class FakturaKosztowaEcoWindLogic extends AbstractDocumentLogic
{
	private static FakturaKosztowaEcoWindLogic instance;
	protected static Logger log = LoggerFactory.getLogger(FakturaKosztowaEcoWindLogic.class);

	public static FakturaKosztowaEcoWindLogic getInstance()
	{
		if (instance == null)
			instance = new FakturaKosztowaEcoWindLogic();
		return instance;
	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn)
	{
		boolean assigned = false;
		try
		{
			if (accpetionCn.equals("akcept_meteoryczna"))
			{
				String toAccept = doc.getFieldsManager().getEnumItemCn("ACCEPTANCE");
				assignable.addCandidateUser(toAccept);
				assigned = true;
			}
			else if (accpetionCn.equals("correction"))
			{
				assignable.addCandidateUser(doc.getAuthor());
				assigned = true;
			}
			else if (accpetionCn.equals("akcept_finanse"))
			{
				log.info("Start acceptation level:  " + accpetionCn);

				String centrumId = openExecution.getVariable("costCenterId").toString();
				String userName = Project.find(Long.valueOf(centrumId)).getProjectManager();

				log.debug("Dla ID centrum: " + centrumId + " akceptaja na: " + userName);
				assignable.addCandidateUser(userName);
				assigned = true;
			}
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}

		return assigned;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			String user = document.getFieldsManager().getEnumItemCn("ACCEPTANCE");
			map.put(ASSIGN_USER_PARAM, user);
			// map.put(ASSIGN_DIVISION_GUID_PARAM,
			// event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			FieldsManager fm = dockind.getFieldsManager(id);
			params.setStatus((String) fm.getValue("STATUS"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return params;
	}

	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, java.util.Map<String, ?> fieldValues)
	{
		Long id = null;
		FieldsManager fm = null;
		try
		{
			fm = Document.find(documentId).getFieldsManager();

			if (fieldValues.get("STATUS") == null && fm.getValue("STATUS") != null)
			{
				if (fm.getEnumItem("STATUS").getId().equals(2001) || fm.getEnumItem("STATUS").getId().equals(2003) || fm.getEnumItem("STATUS").getId().equals(2004))
				{
					try
					{
						Map<String, FieldData> valuesdic = new HashMap<String, FieldData>();
						valuesdic.put("NAZWA", new FieldData(Field.Type.STRING, fm.getEnumItem("STATUS").getTitle()));
						valuesdic.put("ACC_DATE", new FieldData(Field.Type.DATE, new Date()));
						valuesdic.put("NAZWISKO", new FieldData(Field.Type.STRING, DSApi.context().getDSUser().getLastname()));

						id = DwrDictionaryFacade.dictionarieObjects.get("AKCEPTACJE").add(valuesdic);

						PreparedStatement ps = null;
						String ins = "INSERT INTO " + kind.getMultipleTableName() + " (document_id,field_cn,field_val) values (" + documentId + " , 'AKCEPTACJE' , " + id + " )";
						ps = DSApi.context().prepareStatement(ins);
						ps.execute();
						DSApi.context().closeStatement(ps);
					}
					catch (EdmException e1)
					{
						log.error(e1.getMessage(), e1);
					}
					catch (SQLException e1)
					{
						log.error(e1.getMessage(), e1);
					}
				}
				else if (fm.getEnumItem("STATUS").getId().equals(2002))
				{
					try
					{
						for (Long idD : (List<Long>)fm.getValue("DSG_CENTRUM_KOSZTOW_FAKTURY"))
						{
							Long centrumId = Long.valueOf((((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").getValues(idD.toString()).get("DSG_CENTRUM_KOSZTOW_FAKTURY_CENTRUMID")).getSelectedOptions().get(0)));
						
							String lastName = Project.find(centrumId).getProjectManager();
							if (lastName.equals(DSApi.context().getDSUser().getLastname()))
							{
								Map<String, FieldData> valuesdic = new HashMap<String, FieldData>();
								valuesdic.put("AKCEPT", new FieldData(Field.Type.STRING, DSApi.context().getDSUser().getLastname()));
								valuesdic.put("id", new FieldData(Field.Type.STRING, idD.toString()));
								valuesdic.put("A_ACC_DATE", new FieldData(Field.Type.DATE, new Date()));
								DwrDictionaryFacade.dictionarieObjects.get("DSG_CENTRUM_KOSZTOW_FAKTURY").update(valuesdic);
							}
						}	
					}
					catch (EdmException e1)
					{
						log.error(e1.getMessage(), e1);
					}
				}
			}
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
	}

	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("SPOSB_DOSTARCZENIA", 201);
		fm.reloadValues(values);
	}
}
