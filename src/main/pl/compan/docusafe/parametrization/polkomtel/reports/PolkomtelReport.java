package pl.compan.docusafe.parametrization.polkomtel.reports;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.tools.CsvDumper;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.File;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: slim
 * Date: 11.10.13
 * Time: 12:06
 * To change this template use File | Settings | File Templates.
 */
public class PolkomtelReport extends Report
{

    private static String[] mpkLabels=
            {
                    "Numer stacji" ,
                    "Nazwa stacji" ,
                    "Adres stacji",
                    "Kod pocztowy" ,
                    "Miejscowo��" ,
                    "Nr licznika" ,
                    "Nr ewidencyjny" ,
                    "Taryfa OSD" ,
                    "Moc umowna [kW]" ,
                    "Zabezpieczenie przedlicznikowe [A]"
            };

    private static String[] labels=
            {
                    "Numer Zak�adu",//

                    "Numer lokalizacji"

                    ,"Numer PPE od Zak�adu Energetycznego" //
                    ,"Stacja" //

                    ,"Numer zam�wienia dla Dystrybucji"
                    ,"Numer pozycji z zam�wienia dla Dystrybucji"
                    ,"Numer zam�wienia dla Obrotu"
                    ,"Numer pozycji z zam�wienia dla Obrotu"

                    ,"Numer umowy KBZ" //

                    ,"Uwagi"

                    ,"Numer fiszki"    //
                    ,"Numer licznika"    //

                    ,"Numer PPE z SAP"

                    ,"Miesi�czne zu�ycie kWh"//
                    ,"Koszt kWh"
                    ,"Energia bierna pojemno�ciowa kVAh"
                    ,"Koszty kVarh"
                    ,"Koszty dodatkowe nazwa"
                    ,"Koszty dodatkowe warto��"
                    ,"Data pocz�tku okresu"
                    ,"Data ko�ca okresu wprowadzania"
                    ,"Miesi�czne zu�ycie kWh"
                    ,"Koszt kWh"
                    ,"Energia bierna pojemno�ciowa kVAh"
                    ,"Koszty kVarh"
                    ,"Koszty dodatkowe nazwa"
                    ,"Koszty dodatkowe warto��"
                    ,"Data pocz�tku okresu"
                    ,"Data ko�ca okresu wprowadzania"
                    ,"Numer kontraktu wg KBZ"//

                    ,"Rodzaj kontraktu"

                    ,"Numer dostawcy"//

                    ,"Waluta kontraktu"

                    ,"Pocz�tek obowi�zywania kontraktu"//
                    ,"Koniec obowi�zywania kontraktu"
                    ,"Warto�� kontraktu"//

                    ,"Klauzula kontraktu"
                    ,"Rodzaj zakupu - us�uga / materia�"

                    ,"Numer pozycji kontraktu"//

                    ,"Indeks materia�u"

                    ,"Opis materia�u us�ugi"//

                    ,"CAPEX czy OPEX"
                    ,"Nazwa konta"
                    ,"Nr konta"
                    ,"Kr�tki opis pozycji dla us�ugi"
                    ,"Indeks us�ugi"
                    ,"Opis us�ugi"
                    ,"Konto"
                    ,"Ilo�� "
                    ,"Cena jednostkowa netto"
                    ,"Jednostka miary"
                    ,"MPK"
                    ,"Kontrakt aneksowany"
                    ,"Warto�� kontraktu"

                    ,"Okres fakturowania"//
                    ,"TYP Umowy"
                    ,"Umowa"  //

                    ,"Departament odpowiedzialny "
                    ,"Dyrektor Odpowiedzialny "

                    ,"Nr aneksu"//

                    ,"Forma p�atno�ci"
                    ,"Departament/Biuro odpowiedzialny(e) za realizacj�"
                    ,"Finansuj�ce MPK / Projekt / Element PSP"

                    ,"Nadzoruje realizacj�"//
                    ,"Numer umowy kontrahenta"
                    ,"Dostawca"
                    ,"Data podpisania / zawarcia umowy"
                    ,"Data wej�cia w �ycie"
                    ,"Tekst - Przedmiot umowy"
                    ,"Kategoria Umowy"
                    ,"Rodzaj umowy"
                    ,"Parafowane przez"
                    ,"Podpis po stronie Polkomtel"
                    ,"Podpis po stronie Dostawcy"
                    ,"Ca�kowita warto��"//

                    ,"Waluta"
                    ,"Spos�b zap�aty"

                    ,"Termin p�atno�ci"//
                    ,"Harmonogram p�atno�ci"
                    ,"Okres obowi�zywania"
                    ,"Okres wypowiedzenia"//

                    ,"Jadnostka Gospodarcza"
                    ,"Typ pozycji"
                    ,"Typ dekretacji"
                    ,"Kontrakt"
                    ,"Pozycja kontraktu"
                    ,"Zak�ad"
                    ,"Ilo�� zam�wienia"
                    ,"Data dostawy"
                    ,"Cena netto"
                    ,"Okre�lenie linii harmonogramu/okres�w fakturowania"
                    ,"Dekratacja"

            };

    private static String[] mpkColumns=
            {
        "NR_STACJI",
        "NAZWA_STACJI" ,
        "ADRES_STACJI",
        "KOD_POCZTOWY" ,
        "MIEJSCOWOSC" ,
        "NR_LICZNIKA" ,
        "NR_EWIDENCYJNY" ,
        "TARYFA_OSD" ,
        "MOC_UMOWNA" ,
        "ZABEZPIECZENIE_PRZEDLICZNIKOWE"
            };





    private static String[] columns=
                                    {
         "text2"
        ,""
        ,"text4"
        ,"MPK"

        ,""
        ,""
        ,""
        ,""

        ,"text9"

        ,""

        ,"text11"
        ,"text12"

        ,""

        ,"text14"
        ,"text15"
        ,"text16"
        ,"text17"
        ,"text18"
        ,"text19"
        ,"text20"
        ,"text21"
        ,"text22"
        ,"text23"
        ,"text24"
        ,"text25"
        ,"text26"
        ,"text27"
        ,"text28"
        ,"text29"
        ,"text31"

        ,""

        ,"text32"

        ,""

        ,"text34"
        ,"text35"
        ,"text36"

        ,""
        ,""

        ,"text39"

        ,""

        ,"text41"

        ,""
        ,""
        ,""
        ,""
        ,""
        ,""
        ,""
        ,""
        ,""
        ,""
        ,""
        ,""
        ,""


        ,"text55"
        ,"text56"
        ,"text57"

        ,""
        ,""

        ,"text59a"

        ,""
        ,""
        ,""

        ,"text63a"
        ,"text64a"
        ,"text65a"
        ,"text66a"
        ,"text67a"
        ,"text68a"
        ,"text69a"
        ,"text70a"
        ,"text71a"
        ,"text72a"
        ,"text73a"
        ,"text74a"

        ,""
        ,""

        ,"text74b"
        ,"text75b"
        ,"text74d"
        ,"text75d"


        ,""
        ,""
        ,""
        ,""
        ,""
        ,""
        ,""
        ,""
        ,""
        ,""
        ,""
                                    };



    private static final Logger log = LoggerFactory.getLogger(PolkomtelReport.class);
    @Override
    public void doReport() throws Exception
    {
        UniversalTableDumper dumper = new UniversalTableDumper();

        CsvDumper csvDumper = new CsvDumper();
        File csvFile = new File(this.getDestination(), "report.csv");
        csvDumper.openFile(csvFile);
        dumper.addDumper(csvDumper);

        XlsPoiDumper poiDumper = new XlsPoiDumper();
        File xlsFile = new File(this.getDestination(), "report.xls");
        poiDumper.openFile(xlsFile);
        dumper.addDumper(poiDumper);

        dumper.newLine();
        for(int xxx=0; xxx<labels.length; xxx++)
        {

            if(labels[xxx].equals("Stacja"))
            {
               for(int yyy=0; yyy<mpkLabels.length; yyy++)
               {
                   dumper.addText(mpkLabels[yyy]);
               }
            }
            else
            {
                dumper.addText(labels[xxx]);
            }
        }
        dumper.dumpLine();


        Statement st = DSApi.context().createStatement();
        String query = "SELECT ";
        for(int i = 0; i < columns.length; i++)
        {
            query += columns[i];
            if(i!=columns.length-1 && columns[i].trim().length()>0)
            {
                query+=",";
            }
        }
        if(query.endsWith(",")) query = query.substring(0, query.length()-1);
        query += " FROM dsg_plktel_invoice";
        ResultSet rs = st.executeQuery(query);


        String query2 = "SELECT * FROM DSG_STACJE where id=";
        long mpkId = -1L;
//        int index=0;

        while(rs.next())
        {
            dumper.newLine();
            for(int index=0; index < columns.length; index++)
            {
                String col = columns[index];
                String lab = labels[index];

                if(col.equals("MPK"))
                {
                    query2+=rs.getLong("MPK");
                    Statement st2 = DSApi.context().createStatement();
                    ResultSet rs2 = st2.executeQuery(query2);
                    while(rs2.next())
                    {
                        for(int index2=0; index2<mpkColumns.length; index2++)
                        {
                            Object obj = rs2.getObject(mpkColumns[index2]);
                            if (obj==null) dumper.addText("");
                            if(obj instanceof String) dumper.addText((String) obj);
                            else if(obj instanceof Timestamp)dumper.addDate(new Date(((Timestamp) obj).getTime()));
                        }
                    }
                }
                else if(col.trim().length()>0)
                {
                    Object obj = rs.getObject(columns[index]);
                    if(columns[index].equals("text41"))
                    {
                        if(obj instanceof Integer)
                        {
                            Integer id = (Integer) obj;
                            switch(id)
                            {
                                case 10: dumper.addText("Dystrybucja");
                                case 20: dumper.addText("Energia");
                                case 30: dumper.addText("Kompleksowa");
                                case 40: dumper.addText("Podlicznik-umowa");
                            }
                        }
                    }
                    else
                    {
                        if (obj==null) dumper.addText("");
                        if(obj instanceof String) dumper.addText((String) obj);
                        else if(obj instanceof Timestamp)dumper.addDate(new Date(((Timestamp) obj).getTime()));
                        else if(obj instanceof Integer)dumper.addInteger((Integer) obj);
                        else if(obj instanceof Double)dumper.addObject(obj);
                        else if(obj instanceof Object)dumper.addObject(obj);
                    }
                }
                else
                {
                    dumper.addText("");
                }
            }
            dumper.dumpLine();
        }

        dumper.closeFileQuietly();
    }
}
