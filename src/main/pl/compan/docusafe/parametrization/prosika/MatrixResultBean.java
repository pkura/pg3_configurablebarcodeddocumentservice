package pl.compan.docusafe.parametrization.prosika;

/**
 * Klasa w ktorej zapisywane sa wyniki wyszukania przez macierz decyzyjna
 * @author wkutyla
 *
 */
public class MatrixResultBean 
{
	/**
	 * Identyfikator reguly
	 */
	private Integer ruleId;
	/**
	 * Docelowy guid
	 */
	private String targetGuid;
	/**
	 * Uzytkownik
	 */
	private String targetUser;
	/**
	 * Czy ma zostac wyzanczony nowy rekord
	 */
	private Boolean autoAssing;
	public String getTargetGuid() {
		return targetGuid;
	}
	public void setTargetGuid(String targetGuid) {
		this.targetGuid = targetGuid;
	}
	public String getTargetUser() {
		return targetUser;
	}
	public void setTargetUser(String targetUser) {
		this.targetUser = targetUser;
	}
	public Boolean getAutoAssing() {
		return autoAssing;
	}
	public void setAutoAssing(Boolean autoAssing) {
		this.autoAssing = autoAssing;
	}
	public Integer getRuleId() {
		return ruleId;
	}
	public void setRuleId(Integer ruleId) {
		this.ruleId = ruleId;
	}
}
