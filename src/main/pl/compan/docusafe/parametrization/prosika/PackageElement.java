package pl.compan.docusafe.parametrization.prosika;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;

/**
 * Element paczki 
 * @author wkutyla
 *
 */
public class PackageElement 
{
	private Long id;
	private String kod;
	private String barcode;
	private Long documentId;
	private Integer status;
	private Date ctime;
	private Date mtime;
	private Long packageId;
	//data dodania barcodu
	private Date cbtime;
	
	public PackageElement(boolean asd)
	{
		this.mtime = new Date();
		this.ctime = new Date();
	}
	
	public PackageElement()
	{
	}
	
	public void create() throws EdmException 
	{
		 try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();	       
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
		
	}
	
	public static PackageElement find(Long id) throws EdmException
	{
		  return (PackageElement) Finder.find(PackageElement.class, id);
	}
	
	public static boolean isBarcodeExist(String barcode) throws SQLException, EdmException
	{
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	boolean isExist = true;
        try
        {
        	ps = DSApi.context().prepareStatement(
        			"select "+
        				"id "+
        			"from "+ 
        				"ds_package_element pe "+
        			"where "+ 
						" pe.barcode = ?");
        	ps.setString(1, barcode);
            rs = ps.executeQuery();
            isExist = rs.next();
            if(rs != null){rs.close();}
    		DSApi.context().closeStatement(ps);
    		
            if(!isExist)
            {
            	ps = DSApi.context().prepareStatement(
            			"select "+
            				"DOCUMENT_ID "+
            			"from "+ 
            				"DSG_PROSIKA pe "+
            			"where "+ 
    						" pe.barcode = ?");
            	ps.setString(1, barcode);
                rs = ps.executeQuery();
                isExist = rs.next();
            }
    	}
    	finally
    	{
    		if(rs != null){rs.close();}
    		if(ps != null){DSApi.context().closeStatement(ps);}
		}
    	return isExist;
	}
	
	public void save()
	{
		this.mtime = new Date();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getKod() {
		return kod;
	}
	public void setKod(String kod) {
		this.mtime = new Date();
		this.kod = kod;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.cbtime = new Date();
		this.mtime = new Date();
		this.barcode = barcode;
	}
	public Long getDocumentId() {
		return documentId;
	}
	public void setDocumentId(Long documentId) {
		this.mtime = new Date();
		this.documentId = documentId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.mtime = new Date();
		this.status = status;
	}
	public Date getCtime() {
		return ctime;
	}
	public void setCtime(Date ctime) {
		this.mtime = new Date();
		this.ctime = ctime;
	}
	public Date getMtime() {
		return mtime;
	}

	public void setPackageId(Long packageId) {
		this.mtime = new Date();
		this.packageId = packageId;
	}

	public Long getPackageId() {
		return packageId;
	}

	public Date getCbtime() {
		return cbtime;
	}
}
