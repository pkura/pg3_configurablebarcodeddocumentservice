package pl.compan.docusafe.parametrization.prosika.barcodes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.List;

import org.hibernate.LockMode;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * Klasa obsluguje zarzadzanie barkodami. Konwencja jest taka ze pointery wskazuja
 *  na pierszyw wolny barkod w dabnym zakresie/puli
 * @author Kuba
 *
 */
public class BarcodeManager {
	
	private static Logger log = LoggerFactory.getLogger(BarcodeManager.class);
	private static StringManager sm = StringManager.getManager(BarcodeManager.class.getPackage().getName());
	private static BarcodePool activePool;
	
	public static Integer RANGE_STATUS_NEW = 1;
	public static Integer RANGE_STATUS_ACCEPTED = 2;
	public static Integer RANGE_STATUS_PRINTED = 3;
	public static Integer RANGE_STATUS_SENT = 4;
	public static Integer RANGE_STATUS_INUSE = 5;
	public static Integer RANGE_STATUS_USED = 6;
	public static Integer RANGE_STATUS_REJECTED = 7;
	public static Integer RANGE_STATUS_CANCELED = 8;
	public static Integer RANGE_STATUS_ADVERTISED = 9;
	
	public static Integer CLIENT_TYPE_USER = 1;
	public static Integer CLIENT_TYPE_MAILER = 2;
	
	private static String barcodeValidTipMessage;
	private static final String TIP_MESSAGE_FIRST_CHAR = "Barkod powinien zaczyna� si� od znaku 8";
	private static final String TIP_MESSAGE_INVALID_CHECK_SUM_BARCODE = "Nieprawid�owa suma kontrolna barkodu. Prosz� sprawdzi� w dokumentacji jak wygl�da prawid�owy format barkodu.";
	
	/**
	 * Metoda zwraza aktywn� dla systemu pul� barkod�w
	 * @return
	 */
	public static BarcodePool getActivePool()
	{
		if(activePool==null) findActivePool();
		return activePool;
	}
	
	
	public static BarcodeRange assignBarcodeRange(String clientName, Integer clientType, Integer numberOfBarcodes) throws EdmException
	{
		return assignBarcodeRange(clientName, clientType, numberOfBarcodes, null, null, null);
	}
	
	public static BarcodeRange assignBarcodeRange(String clientName, Integer clientType, Integer numberOfBarcodes, Long locationid, String remark, String transportServiceName) throws EdmException
	{
		BarcodeRange br = new BarcodeRange();
		br.setClientName(clientName);
		br.setClientType(clientType);
		br.setCtime(new Date());
		br.setStatus(RANGE_STATUS_NEW);
		br.setNumberOfBarcodes(numberOfBarcodes);
		br.setLocationId(locationid);
		br.setRemark(remark);
		br.setTransportServiceName(transportServiceName);
		
		DSContext ctx = DSApi.context();
		try
		{
			ctx.begin();
			ctx.session().save(br);
			ctx.session().refresh(br);
			ctx.commit();
		}
		catch(EdmException e)
		{
			ctx.setRollbackOnly();
		}
		return br;	
	}
	
	
	
	public static List<BarcodeRange> getRangeForClient(String clientName, Integer clientType, Integer status) throws EdmException
	{
		String sql = "select d from d in class "+ BarcodeRange.class.getName() + " where d.clientName =? and d.clientType=? and d.status=?";
		DSContext ctx = DSApi.context();
		Query query = ctx.session().createQuery(sql);
		query.setString(0, clientName);
		query.setInteger(1, clientType);
		query.setInteger(2, status);
		List<BarcodeRange> ranges = query.list();
		return ranges;
		
	}
	
	public static List<BarcodeRange> getRangeForClient(String clientName, Integer clientType) throws EdmException
	{
		String sql = "select d from d in class "+ BarcodeRange.class.getName() + " where d.clientName =? and d.clientType=?";
		DSContext ctx = DSApi.context();
		Query query = ctx.session().createQuery(sql);
		query.setString(0, clientName);
		query.setInteger(1, clientType);
		List<BarcodeRange> ranges = query.list();
		return ranges;
		
	}

	public static List<BarcodeRange> getRangeByStatus(Integer status) throws EdmException
	{
		
		String sql = "select d from d in class "+ BarcodeRange.class.getName() + " where d.status=?";
		DSContext ctx = DSApi.context();
		Query query = ctx.session().createQuery(sql);
		query.setInteger(0, status);
		List<BarcodeRange> ranges = query.list();
		return ranges;
		
	}
	public static List<BarcodeRange> getAllRanges() throws EdmException
	{
		
		String sql = "select d from d in class "+ BarcodeRange.class.getName();
		DSContext ctx = DSApi.context();
		Query query = ctx.session().createQuery(sql);
		List<BarcodeRange> ranges = query.list();
		return ranges;
		
	}
	public static synchronized BarcodeRange acceptBarcodeRange(Long brId, Integer status) throws EdmException
	{
		BarcodeRange br = null;
		DSContext ctx = DSApi.context();
		if(status==null) status = RANGE_STATUS_ACCEPTED;
		try
		{
			ctx.begin();
			ctx.session().lock(getActivePool(), LockMode.UPGRADE);
			br = findRangeById(brId);
			if(br.getEnd()!=null||br.getStart()!=null) throw new EdmException(sm.getString("ToZamowieniePosiadaJuzPrzydzielonyZakres"));
			Long start = getActivePool().getCurrentPointer();
			Long end = getActivePool().getCurrentPointer() + br.getNumberOfBarcodes() - 1;
			if(end > getActivePool().getEndBarcode())
			{
				throw new EdmException("Nie mo�na przydzielic takiej liczby barkodow ("+br.getNumberOfBarcodes()+") poniewaz w puli pozostalo za malo barkodow");
			}
			
			br.setStart(start);
			br.setEnd(end);
			br.setCurrentPointer(start);
			br.setAcceptTime(new Date());
			br.setStatus(status);
			br.setPoolId(getActivePool().getId());
			br.setPrefix(getActivePool().getPrefix());
			getActivePool().movePointer(end + 1);
			ctx.session().update(br);
			ctx.commit();
		}
		catch(EdmException e)
		{
			ctx.setRollbackOnly();
			throw new EdmException(e);
		}
		

		try
		{
			sendNotifiactionEmail(br.getId(), br.getStatusDesc());
		}
		catch (Exception e) 
		{
			log.error("Nie wys�ano powiadomienia",e);
		}  
		return br;
		
	}
	
	public static synchronized BarcodeRange acceptBarcodeRangeFromRange(Long brId, Integer status, Long masterId, Long start, Long end) throws EdmException
	{
		BarcodeRange br = null;
		DSContext ctx = DSApi.context();
		if(status==null) status = RANGE_STATUS_ACCEPTED;
		try
		{
			
			ctx.begin();
			
			br = findRangeById(brId);
			BarcodeRange master = findRangeById(masterId);
			BarcodePool bp = BarcodeManager.findPoolById(master.getPoolId());
			ctx.session().lock(bp, LockMode.UPGRADE);
			
			if(master.getEnd() - master.getCurrentPointer()< br.getNumberOfBarcodes())
				throw new EdmException(sm.getString("ZaMaloBarkodowWZakresie"));
			
			if(br.getEnd()!=null||br.getStart()!=null) 
				throw new EdmException(sm.getString("ToZamowieniePosiadaJuzPrzydzielonyZakres"));
		
			
			if(end > master.getEnd())
			{
				throw new EdmException("Nie mo�na przydzielic takiej liczby barkodow ("+br.getNumberOfBarcodes()+") poniewaz w puli pozostalo za malo barkodow");
			}
			
			br.setStart(start);
			br.setEnd(end);
			br.setCurrentPointer(start);
			br.setAcceptTime(new Date());
			br.setStatus(status);
			br.setPoolId(bp.getId());
			br.setPrefix(bp.getPrefix());
			br.setMasterRangeId(master.getId());
			br.setAssignedNumberOfBarcodes(new Long(end-start).intValue());
			master.setCurrentPointer(end + 1);
			ctx.session().update(br);
			ctx.session().update(master);
			ctx.commit();
		}
		catch(EdmException e)
		{
			ctx.setRollbackOnly();
			throw new EdmException(e);
		}
		

		try
		{
			sendNotifiactionEmail(br.getId(), br.getStatusDesc());
		}
		catch (Exception e) 
		{
			log.error("Nie wys�ano powiadomienia",e);
		}  
		return br;
	}
	
	public static synchronized BarcodeRange setStatusBarcodeRange(Long brId, Integer stat) throws EdmException
	{
		BarcodeRange br = null;
		DSContext ctx = DSApi.context();
		try
		{
			ctx.begin();
			br = findRangeById(brId);
			if((!br.getStatus().equals(RANGE_STATUS_NEW)) && stat.equals(RANGE_STATUS_REJECTED)) throw new EdmException(sm.getString("NieMoznaOdrzucicZaakceptowanegoZakresuBarkodow"));
			br.setStatus(stat);
			ctx.session().update(br);
			ctx.commit();
			
		}
		catch(EdmException e)
		{
			ctx.setRollbackOnly();
			throw new EdmException(e);
		}
		
		try
		{
			sendNotifiactionEmail(br.getId(), br.getStatusDesc());
		}
		catch (Exception e) 
		{
			log.error("Nie wys�ano powiadomienia",e);
		}  
		return br;
		
	}
	public static BarcodeRange findRangeById(Long id) throws EdmException
	{
		DSContext ctx = DSApi.context();
		String sql = "select d from d in class " + BarcodeRange.class.getName() + " where id=?";
		Query q = ctx.session().createQuery(sql);
		q.setLong(0, id);
		List<BarcodeRange> results = q.list();
		if(results.size()>0) return results.get(0);
		throw new EdmException(sm.getString("NieZnalezionoZakresuOPodanymId",id));
		
	}
	private static void findActivePool()
	{
		try
		{
			String sql = "select d from d in class "+ BarcodePool.class.getName() + " where d.active =?";
			DSContext ctx = DSApi.context();
			Query query = ctx.session().createQuery(sql);
			query.setBoolean(0, true);
			List<BarcodePool> pools = query.list();
			if(pools.size()>0)
				activePool = pools.get(0);
			else
				log.debug("Nie ma aktywnej puli barkod�w");
		}
		catch(EdmException e)
		{
			
			log.debug("",e);
		}
	}
	
	private static BarcodePool findPoolById(Long id) throws EdmException
	{
		
			String sql = "select d from d in class "+ BarcodePool.class.getName() + " where d.id =?";
			DSContext ctx = DSApi.context();
			Query query = ctx.session().createQuery(sql);
			query.setLong(0, id);
			List<BarcodePool> pools = query.list();
			if(pools.size()>0)
				return pools.get(0);
			else
				throw new EdmException("Nie ma puli barkod�w o takim id");
		
	}
	
	public static Integer calculateControlDigit(String s)
	{
		if(s.length()%2>0) s = "0"+s;
		int currentpos = 0, sum1 = 0, sum2 = 0;
		int ret = 0;
		try
		{
			while(currentpos < s.length())
			{
				sum1+=Integer.parseInt(""+s.charAt(currentpos));
				currentpos+=2;
			}
			currentpos = 1;
			while(currentpos < s.length())
			{
				sum2+=Integer.parseInt(""+s.charAt(currentpos));
				currentpos+=2;
			}
			sum2 = sum2*3;
			ret = (sum1+sum2)%10;
			ret = (10 - ret)%10;
			
		}
		catch(NumberFormatException e)
		{
			
		}
		return ret;
	}
	
	public static String getBarcodeAsString(Long b, Long poolId)
	{
		if(b==null) return sm.getString("NieOkreslono");
		return b.toString() + calculateControlDigit(b.toString());
	}
	
	public static String getStatusDesc(Integer status)
	{
		if(status.equals(RANGE_STATUS_ACCEPTED)) return sm.getString("Zaakceptowany");
		else if(status.equals(RANGE_STATUS_INUSE)) return sm.getString("Aktywny");
		else if(status.equals(RANGE_STATUS_NEW)) return sm.getString("Nowy");
		else if(status.equals(RANGE_STATUS_PRINTED)) return sm.getString("Drukowany");
		else if(status.equals(RANGE_STATUS_REJECTED)) return sm.getString("Odrzucony");
		else if(status.equals(RANGE_STATUS_SENT)) return sm.getString("Wyslany");
		else if(status.equals(RANGE_STATUS_USED)) return sm.getString("Zuzyty");
		else if(status.equals(RANGE_STATUS_CANCELED)) return sm.getString("Anulowany");
		else if(status.equals(RANGE_STATUS_ADVERTISED)) return sm.getString("Zareklamowany");
		else return sm.getString("Nieznany");
	}
	public static String getClientTypeDesc(Integer type)
	{
		if(type.equals(CLIENT_TYPE_USER)) return sm.getString("Uzytkownik");
		else return sm.getString("Nieznany");
	}
	
	public static Boolean isValid(String barcode)
	{
		if("false".equals(Docusafe.getAdditionProperty("isValidBarcod")))
			return true;
		try
		{
			if(Integer.parseInt(barcode.substring(0,1))!=8) {
				barcodeValidTipMessage = TIP_MESSAGE_FIRST_CHAR;
				return false;
			}
			Integer lastDigit = Integer.parseInt(barcode.substring(barcode.length()-1));
			if(lastDigit!=calculateControlDigit(barcode.substring(0,barcode.length()-1))) {
				barcodeValidTipMessage = TIP_MESSAGE_INVALID_CHECK_SUM_BARCODE;
				return false;
			}
			
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}
	
	public static Integer isValidReturnInteger(String barcode)
	{
		try
		{
			if(Integer.parseInt(barcode.substring(0,1))!=8) {
				barcodeValidTipMessage = TIP_MESSAGE_FIRST_CHAR;
				return -1;
			}
			Integer lastDigit = Integer.parseInt(barcode.substring(barcode.length()-1));
			if(lastDigit!=calculateControlDigit(barcode.substring(0,barcode.length()-1))) {
				barcodeValidTipMessage = TIP_MESSAGE_INVALID_CHECK_SUM_BARCODE;
				return -1;
			}
			
			PreparedStatement ps = null;
			try
			{
				ps = DSApi.context().prepareStatement("select barcode from dsg_prosika where barcode=?");
				ps.setString(1, barcode);
				ResultSet rs = ps.executeQuery();
				if(!rs.next()) return 1;
			}
			finally
			{
				DSApi.context().closeStatement(ps);
			}
			
			return -2;
		}
		catch(Exception e)
		{
			return -1;
		}
	}
	
	public static Boolean existsInSystem(String barcode)
	{
		return false;
	}
	
	private static void sendNotifiactionEmail(Long id, String status) throws Exception
	{
		EventFactory.registerEvent("forOneDay", "BarcodesNotifier",
				status+"|"+id+"|"+DSApi.context().getPrincipalName(),null, new Date());
	}


	/**
	 * Komunikat z przyczyna odrzucenia wprowadzonego barkodu.	 
	 */
	public static String getBarcodeValidTipMessage() {
		return barcodeValidTipMessage;
	}

}
