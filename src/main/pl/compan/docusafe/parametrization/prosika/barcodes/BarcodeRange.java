package pl.compan.docusafe.parametrization.prosika.barcodes;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.prosika.DicLocation;

public class BarcodeRange 
{
	
	private static final Logger log = LoggerFactory.getLogger(BarcodeRange.class);
	private Long id;
	private String prefix;
	private Long poolId;
	private Long start;
	private Long end;
	private Integer clientType;
	private String clientName;
	private Date ctime;
	private Integer status;
	private Long currentPointer;
	private Integer numberOfBarcodes;
	private Date acceptTime;
	private Long locationId;
	private Integer assignedNumberOfBarcodes;
	private Long masterRangeId;
	private String remark;
	private String transportServiceName;
	
	
	public String getNextBarcodeAsStringOrNullInsideTransaction() throws EdmException
	{
		boolean finished = false;
		//System.out.println(status);
		if(!status.equals(BarcodeManager.RANGE_STATUS_INUSE)) throw new EdmException("Zakres nie jest aktywny");
		if(currentPointer==end) finished = true;
		
		if(!finished)
		{
			String ret = prefix + currentPointer.toString() + BarcodeManager.calculateControlDigit(prefix + currentPointer.toString());
			movePointer();
			DSApi.context().session().update(this);
			return ret;
		}
		else
		{
			status = BarcodeManager.RANGE_STATUS_USED;
			DSApi.context().session().update(this);
			return null;
		}
		
	}
	
	public String getNextBarcodeAsStringOrNullOwnTransaction() throws EdmException
	{
		try
		{
			if(!DSApi.isContextOpen())
				DSApi.openAdmin();
			boolean finished = false;
			if(!status.equals(BarcodeManager.RANGE_STATUS_INUSE)) throw new EdmException("Zakres nie jest aktywny");
			if(currentPointer==end) finished = true;
			String ret;
			DSApi.context().begin();
			if(!finished)
			{
				log.trace("Prefix:"+prefix);
				log.trace("CurrentPointer:" + currentPointer.toString());
				LoggerFactory.getLogger("kubaw").debug("Prefix:"+prefix);
				LoggerFactory.getLogger("kubaw").debug("CurrentPointer:" + currentPointer.toString());
				ret = currentPointer.toString() + BarcodeManager.calculateControlDigit(currentPointer.toString());
				movePointer();
			}
			else
			{
				status = BarcodeManager.RANGE_STATUS_USED;
				ret = null;
			}
			DSApi.context().session().update(this);
			DSApi.context().commit();
			return ret;
			
			
		}
		catch(EdmException e)
		{
			DSApi.context().setRollbackOnly();
			throw e;
		}
	}
	
	public void movePointer() throws EdmException
	{
		this.currentPointer++;
		//DSContext ctx = DSApi.openAdmin();
		DSApi.context().session().update(this);
	}
	
	public Long getPoolId() {
		return poolId;
	}
	public void setPoolId(Long poolId) {
		this.poolId = poolId;
	}
	public Long getStart() {
		return start;
	}
	public void setStart(Long start) {
		this.start = start;
	}
	public Long getEnd() {
		return end;
	}
	public void setEnd(Long end) {
		this.end = end;
	}
	public Integer getClientType() {
		return clientType;
	}
	public void setClientType(Integer clientType) {
		this.clientType = clientType;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public Date getCtime() {
		return ctime;
	}
	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getId() {
		return id;
	}
	public Long getCurrentPointer() {
		return currentPointer;
	}
	public void setCurrentPointer(Long currentPointer) {
		this.currentPointer = currentPointer;
	}
	public Integer getNumberOfBarcodes() {
		return numberOfBarcodes;
	}
	public void setNumberOfBarcodes(Integer numberOfBarcodes) {
		this.numberOfBarcodes = numberOfBarcodes;
	}
	public Date getAcceptTime() {
		return acceptTime;
	}
	public void setAcceptTime(Date acceptTime) {
		this.acceptTime = acceptTime;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getStatusDesc()
	{
		return BarcodeManager.getStatusDesc(status);
	}
	public String getFullUsername()
	{
		if(clientType.equals(BarcodeManager.CLIENT_TYPE_USER))
		{
			try
			{
				DSUser us = DSUser.findByUsername(this.clientName);
				return us.asFirstnameLastnameName();
			}
			catch(Exception e)
			{
				return this.clientName;
			}
		}
		else
		{
			return BarcodeManager.getClientTypeDesc(this.clientType) + "/" +this.clientName;
		}
	}
	
	public String getStartString()
	{
		return BarcodeManager.getBarcodeAsString(this.start, this.poolId);
	}
	
	public String getEndString()
	{
		return BarcodeManager.getBarcodeAsString(this.end, this.poolId);
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}
	
	public String getLocationName()
	{
		String name = "";
		if(this.locationId != null)
		{
			try
			{
				DSApi.openAdmin();
				name = new DicLocation().find(this.locationId).getDictionaryDescription();
			}
			catch (Exception e) {}
			finally {DSApi._close();}
		}
		return name;
	}
	public Integer getAssignedNumberOfBarcodes() {
		return assignedNumberOfBarcodes;
	}
	public void setAssignedNumberOfBarcodes(Integer assignedNumberOfBarcodes) {
		this.assignedNumberOfBarcodes = assignedNumberOfBarcodes;
	}
	public Long getMasterRangeId() {
		return masterRangeId;
	}
	public void setMasterRangeId(Long masterRangeId) {
		this.masterRangeId = masterRangeId;
	}
	
	public Long getBarcodesLeft()
	{
		return end - currentPointer;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getTransportServiceName() {
		return transportServiceName;
	}
	public void setTransportServiceName(String transportServiceName) {
		this.transportServiceName = transportServiceName;
	}

}
