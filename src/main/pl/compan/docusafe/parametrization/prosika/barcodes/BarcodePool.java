package pl.compan.docusafe.parametrization.prosika.barcodes;

import java.util.Date;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;

public class BarcodePool {

	private Long id;
	private String prefix;
	private Long startBarcode;
	private Long endBarcode;
	private Date ctime;
	private Boolean active;
	private Date closeTime;
	private Long currentPointer;
	
	/**
	 * uzywac tylko wewnatrz otwrtej transakcji
	 * @param position
	 * @throws EdmException
	 */
	public void movePointer(Long position) throws EdmException
	{
		this.currentPointer = position;
		DSContext ctx = DSApi.context();
		ctx.session().update(this);		
	}
	
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public Long getStartBarcode() {
		return startBarcode;
	}
	public void setStartBarcode(Long startBarcode) {
		this.startBarcode = startBarcode;
	}
	public Long getEndBarcode() {
		return endBarcode;
	}
	public void setEndBarcode(Long endBarcode) {
		this.endBarcode = endBarcode;
	}
	public Date getCtime() {
		return ctime;
	}
	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}
	public Long getId() {
		return id;
	}
	
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Date getCloseTime() {
		return closeTime;
	}
	public void setCloseTime(Date closeTime) {
		this.closeTime = closeTime;
	}
	public Long getCurrentPointer() {
		return currentPointer;
	}
	public void setCurrentPointer(Long currentPointer) {
		this.currentPointer = currentPointer;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	
}
