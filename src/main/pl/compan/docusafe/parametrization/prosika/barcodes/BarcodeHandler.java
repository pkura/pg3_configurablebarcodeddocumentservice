package pl.compan.docusafe.parametrization.prosika.barcodes;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeFactory;
import org.slf4j.LoggerFactory;
import com.asprise.util.tiff.TIFFReader;
import com.asprise.util.tiff.TIFFWriter;
import com.java4less.vision.Barcode1DReader;
import com.java4less.vision.BarcodeData;
import com.java4less.vision.RImage;

public class BarcodeHandler 
{
	public static String DEFAULT_BARCODE = "00000000000";
	private String fileName;
	private String filePath = ".";

	private String policyBarcode = DEFAULT_BARCODE;
	private int x = 100;
	private int y = 3200;
	private int width = 800;
	private int height = 200;
	
	public static final int DEFAULT_BARCODE_TYPE = Barcode1DReader.INTERLEAVED25;
	
	
	public String scanImage() throws Exception
	{
		return scanImage(DEFAULT_BARCODE_TYPE);
	}
	
	public static File enrichTiffWithBarcodeReturnsFile(File tiff, String barcode) throws Exception
	{
		return enrichTiffWithBarcodeReturnsFile(tiff, barcode, null);
	}
	
	public static File enrichTiffWithBarcodeReturnsFile(File tiff, String barcode, String text) throws Exception
	{
		TIFFReader tr = new TIFFReader(tiff);
		Barcode bc = BarcodeFactory.createInt2of5(barcode);
		
		BufferedImage img = new BufferedImage(tr.getPage(0).getWidth(), (tr.getPage(0).getHeight() + bc.getHeight())+10, BufferedImage.TYPE_INT_RGB);
		
		Graphics2D g = img.createGraphics();
		g.setBackground(Color.WHITE);
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, img.getWidth(), img.getHeight());
		
		g.setPaint(Color.BLACK);
		g.setColor(Color.BLACK);
		bc.draw(g, 10, 10);
		
		if(text != null)
		{
			g.setPaint(Color.BLACK);
			Font font = new Font(g.getFont().getName(), g.getFont().getStyle(), g.getFont().getSize()*3);
			g.setFont(font);
			g.drawString(text, bc.getWidth()+20, bc.getHeight());
		}
		
		g.drawImage((BufferedImage)tr.getPage(0), null, 0, bc.getHeight()+10);
		g.dispose();
		
		BufferedImage[] images = new BufferedImage[tr.countPages()];
		for(int i=0;i<images.length;i++)
		{
			if(i == 0)
			{
				images[i] = img;
			}
			else
			{
				images[i] = (BufferedImage) tr.getPage(i);
			}
		}
		        		
		File wynik = File.createTempFile("tiff", ".tif");
		TIFFWriter.createTIFFFromImages(images, wynik);
		return wynik;
	}
	
	public static List<Rectangle> devideRectangle(Rectangle rectangle, int vertical, int linear)
	{
		List<Rectangle> rectangles = new ArrayList<Rectangle>();
		
		for(Rectangle rec : devideRectangleVertical(rectangle, vertical))
    	{
    		rectangles.addAll(BarcodeHandler.devideRectangle(rec, linear));
    	}
		return rectangles;
	}
	
	private static List<Rectangle> devideRectangleVertical(Rectangle rectangle, int parts)
	{
		List<Rectangle> rectangles = new ArrayList<Rectangle>();
		double oneWidth = rectangle.getWidth() / parts;
		
		for(int i = 0; i < parts; i++)
		{
			Rectangle tmp = new Rectangle();
			if(rectangles.isEmpty())
			{
				tmp.setRect(rectangle.getX(), rectangle.getY(), oneWidth, rectangle.getHeight());	
			}
			else
			{
				tmp.setRect(rectangles.get(rectangles.size()-1).getX()+oneWidth, rectangle.getY(), oneWidth, rectangle.getHeight());
			}
			rectangles.add(tmp);
		}
		
		return rectangles;
	}
	
	private static List<Rectangle> devideRectangle(Rectangle rectangle, int parts)
	{
		List<Rectangle> rectangles = new ArrayList<Rectangle>();
		
		double oneHeight = rectangle.getHeight() / parts;
		
		for(int i = 0; i < parts; i++)
		{
			Rectangle tmp = new Rectangle();
			if(rectangles.isEmpty())
			{
				tmp.setRect(rectangle.getX(), rectangle.getY(), rectangle.getWidth(), oneHeight);				
			}
			else
			{
				tmp.setRect(rectangle.getX(), rectangles.get(rectangles.size()-1).getY()+oneHeight, rectangle.getWidth(), oneHeight);
				
			}	
			rectangles.add(tmp);
		}
		return rectangles;
	}
	
	public static String scanAreas(BufferedImage image, int type, List<Rectangle> rectangles)
	{
		
		for(Rectangle rectangle : rectangles)
		{
			String barcode = scanArea(image, type, rectangle);
			if(!barcode.equalsIgnoreCase(DEFAULT_BARCODE))
			{
				return barcode;
			}
				
		}
		return DEFAULT_BARCODE;
	}
	
	public static String scanArea(BufferedImage image, int type, Rectangle rectangle)
	{
		try
		{
			Barcode1DReader reader = new Barcode1DReader();
			reader.setProgressListener(null);
			reader.setSymbologies(type);
			BarcodeData[] barcodes = reader.scan(new RImage(image, rectangle));
			if(barcodes.length > 0)
			{
				return barcodes[0].getValue();
			}
			
		}
		catch (Exception e)
		{
			LoggerFactory.getLogger("tomekl").debug("wtf",e);
			return DEFAULT_BARCODE;
		}
		
		return DEFAULT_BARCODE;
	}
	
    public String scanImage(int type) throws Exception 
    {	
    	File tif = new File(filePath+"/"+fileName);
    	TIFFReader tiffReader = new TIFFReader(tif);
		try
		{
			RenderedImage imPage = tiffReader.getPage(0);
			
			Barcode1DReader reader=new Barcode1DReader();
			reader.setSymbologies(type);			
			reader.setProgressListener(null);    		
			RImage rim=new RImage((BufferedImage) imPage,new Rectangle(x,y,imPage.getWidth(),imPage.getHeight()));		
			BarcodeData[] barcodes = reader.scan(rim);		
			rim=null;
			imPage=null;	
			if(barcodes.length>0)
			{
				policyBarcode = barcodes[0].getValue();
			}
		}
		catch (Exception e)
		{
			System.out.println("Przeczwycilem wyjatek "+e);    			
		}
    	
		return policyBarcode;						
	}

    
	Image loadImage(String f) throws Exception 
	{
		Image im2=null;
		java.awt.MediaTracker mt2=null;
		java.io.FileInputStream in=null;
		byte[] b=null;
		int size=0;
		in=new java.io.FileInputStream(f);
		if (in!=null) {
			size=in.available();
			b=new byte[size];
			in.read(b);
			im2=java.awt.Toolkit.getDefaultToolkit().createImage(b);
			in.close();
		}
		mt2 = new java.awt.MediaTracker(new Canvas());		
		if (im2!=null) 
		{
			if (mt2!=null) 
			{
				mt2.addImage(im2,0); mt2.waitForID(0);
			}
			int WidthIm = im2.getWidth(null);
		}
		BufferedImage input=new BufferedImage(im2.getWidth(null),im2.getHeight(null),BufferedImage.TYPE_INT_ARGB);	
		Graphics g=input.createGraphics();
		g.setColor(Color.white);
		g.fillRect(0,0,im2.getWidth(null),im2.getHeight(null));
		g.drawImage(im2,0,0,null);
		g.dispose();
		g=null;	
		return input;
	}	
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) 
	{
		this.filePath = filePath;
	}


	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getPolicyBarcode() {
		return policyBarcode;
	}

	public void setPolicyBarcode(String policyBarcode) {
		this.policyBarcode = policyBarcode;
	}


	
}
