package pl.compan.docusafe.parametrization.prosika;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.ValidationException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.parametrization.prosika.barcodes.BarcodeManager;
import pl.compan.docusafe.parametrization.prosika.ws.DroznikPersistManager;
import pl.compan.docusafe.util.StringManager;

public class Prosikalogic extends AbstractDocumentLogic 
{

	static final Logger log = LoggerFactory.getLogger(Prosikalogic.class);
	static final StringManager sm = GlobalPreferences.loadPropertiesFile(Prosikalogic.class.getPackage().getName(),null);
	
	private static Prosikalogic instance;
	
	//POLA
	public static final String BARCODE_FIELD_CN = "BARCODE";
	public static final String KOLEJKA_FIELD_CN = "KOLEJKA";
	public static final String SEGMENT_FIELD_CN = "SEGMENT";
	public static final String TYP_DOKUMENTU_FIELD_CN = "TYP_DOKUMENTU";
	public static final String PODTYP_DOKUMENTU_FIELD_CN = "PODTYP_DOKUMENTU";
	public static final String NR_PACZKI_FIELD_CN = "NR_PACZKI";
	public static final String KLIENT_FIELD_CN = "KLIENT";
	public static final String TAJNY_FIELD_CN = "TAJNY";
	public static final String STATUS_FIELD_CN = "STATUS";
	public static final String DATA_PRZYJECIA_IM_FIELD_CN = "DATA_PRZYJECIA_IM";
	public static final String DATA_PRZYJECIA_TP_FIELD_CN = "DATA_PRZYJECIA_TP";
	public static final String NR_ZGLOSZENIA_TC_FIELD_CN = "NR_ZGLOSZENIA_TC";
	public static final String ZNAK_FIELD_CN = "ZNAK";
	public static final String ID_PLATNIKA_FIELD_CN = "ID_PLATNIKA";
	public static final String NADAWCA_FIELD_CN = "NADAWCA";
	public static final String TYP_FIELD_CN = "TYP";
	public static final String PROBLEMY_FIELD_CN = "PROBLEMY";
	public static final String OPIS_PROBLEMU_FIELD_CN = "OPIS_PROBLEMU";
	public static final String NR_SPRAWY_REKLAMACJI_FIELD_CN = "NR_SPRAWY_REKLAMACJI";
	public static final String ZRODLO_PISMA_NIET_FIELD_CN = "ZRODLO_PISMA_NIET";
	public static final String ZRODLO_PISMA_FIELD_CN = "ZRODLO_PISMA";
	public static final String UWAGI_FIELD_CN = "UWAGI";
	public static final String WPROWADZIL_FIELD_CN = "WPROWADZIL";
	public static final String EMPTY_1_FIELD_CN = "EMPTY_1";
	public static final String EMPTY_2_FIELD_CN = "EMPTY_2";
	public static final String EMPTY_3_FIELD_CN = "EMPTY_3";
	public static final String EMPTY_4_FIELD_CN = "EMPTY_4";
	public static final String EMPTY_5_FIELD_CN = "EMPTY_5";
	public static final String EMPTY_6_FIELD_CN = "EMPTY_6";
	public static final String OSTATNIO_MODYFIKOWAL_FIELD_CN = "OSTATNIO_MODYFIKOWAL";
	public static final String NR_ZLEC_PS_FIELD_CN = "NR_ZLEC_PS";
	public static final String BRAK_W_PS_CN ="BRAK_W_PS";
	public static final String FAX_FIELD_CN = "FAX";
	public static final String EMAIL_FIELD_CN = "EMAIL";
	public static final String SPOSOB_PRZYJECIA_CN = "SPOSOB_PRZYJECIA";
	public static final String PNA_CN = "PNA";
	public static final String WIELOWATKOWY_CN = "WIELOWATKOWY";
	public static final String KLASA = "KLASA";
	public static final String DATA_SKANOWANIA_CN = "DATA_SKANOWANIA";
	public static final String KOD_PNA_CN = "KOD_PNA";
	public static final String DATA_DO_TP_CN = "DATA_DO_TP";
	public static final String DATA_ARCHIWIZACJI = "DATA_ZARCHIWIZOWANIA";

	private static final String[] MONTHS = new String[] {"Stycze�", "Luty", "Marzec",
        "Kwiecie�","Maj","Czerwiec","Lipiec","Sierpie�",
        "Wrzesie�","Pa�dziernik","Listopad","Grudzie�"};
	
	public static synchronized Prosikalogic getInstance()
    {
        if (instance == null)
            instance = new Prosikalogic();
        return instance;
    }
	
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {

		if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");
        
		Map<String, Object> values = new HashMap<String, Object>();
		
		Calendar cal = Calendar.getInstance();
        
        cal.setTime(document.getCtime());
        
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        
        
        /**
         * @TODO - tego typu funkcje stanowia na pewno slaby punkt wydajnosci, 
         * za kazdym razem pobierany jest folder i wyciagane dzieci, 
         * potem kopiowane uprawnienie
         * cos z tym trzeba zrobic
         * Proponuje zrobic metode, do ktorej przekazywana jest kolekckja (lancuszek folderow)
         * oraz uprawnienia do nich
         * Dodatkowym problemem jest to ze to sie wykonuje za kazdym razem, przy kazdym zapisie dokumentu (n.p. zmianie statusu
         * Dobrze by bylo wykrywac, czy obecny zestaw folderow rozni sie od porzedniego i dopiero jesli sie rozni 
         * probowac modyfikacji
         * Wedlug statystyk z AEGONA te operacje dosyc mocno obciazaja baze
         */
        
	    
        Folder folder = getProperFolderForDocument(fm, document);
		document.setFolder(folder);
        
		
		
		if(fm.getValue(KLIENT_FIELD_CN) != null)
		{
			String klientPNA =  ((DicProSIKA) fm.getValue(KLIENT_FIELD_CN)).getAs_kod();
			if (klientPNA != null)
			{
				String kodPNA = fm.getStringValue(KOD_PNA_CN);
				if (kodPNA == null || !klientPNA.equalsIgnoreCase(kodPNA))
				{					
					log.trace("wyliczamy kod grupy PNA");
					String pna = getPNAforZip(((DicProSIKA)fm.getValue(KLIENT_FIELD_CN)).getAs_kod()).toString();
					values.put(PNA_CN, pna);
					values.put(KOD_PNA_CN, klientPNA);
				}
				else
				{
					if (log.isTraceEnabled())
					{
						log.trace("Nie ma potrzeby wyliczania kodu grupy PNA poprzedni wyliczony w oparciu o {}",kodPNA);
					}
				}
			}
			else
			{
				log.trace("brak kodu PNA w slowniku nadawcy PNA=0");
				values.put(PNA_CN, 0);
			}
						
		}
		else
		{
			log.trace("brak dostawcy PNA=0");
			values.put(PNA_CN, 0);
		}
		//Trzeba obczaic jakis sposob zeby wywolywac ta funkcje tylko gdy zmieni sie segment lub typ
		Integer klasa = findClassForDocument(document);
    	values.put(KLASA, klasa);
		
        if(!BarcodeManager.isValid(fm.getValue(BARCODE_FIELD_CN).toString())) throw new EdmException(BarcodeManager.getBarcodeValidTipMessage());
        
        String title = fm.getValue(TYP_DOKUMENTU_FIELD_CN)+" "+fm.getValue(BARCODE_FIELD_CN);
        log.trace("Nadano tytul={}",title);      
        
        if(fm.getValue(DATA_ARCHIWIZACJI)==null && document.getAttachments().size()>0)
        	values.put(DATA_ARCHIWIZACJI, new Date());
        
        
        String description = "";
        if(fm.getValue(SEGMENT_FIELD_CN)!=null)
        {
	        description = fm.getValue(SEGMENT_FIELD_CN) + " - " + fm.getValue(TYP_DOKUMENTU_FIELD_CN);
	        DicProSIKA d = ((DicProSIKA)fm.getValue(KLIENT_FIELD_CN));
		    if(d!=null)
		    	description += "/"+d.getNrEwidencyjny();
        }
        else
        {
        	description = "Niezaindeksowany";
        }
	    log.trace("Nadano description = {}",description);
        if(document instanceof OfficeDocument)
        {        	
        	((OfficeDocument) document).setSummaryWithoutHistory(description);
        }
        
        document.setTitle(title);
        document.setDescription(description);
        
        if (values.size()>0)
        {
        	document.getDocumentKind().setOnly(document.getId(), values);
        }
        
        if(!document.getAuthor().equals("administratorim"))
        {
        	document.setAuthor("administratorim");
        }
        
        DroznikPersistManager dpm = new DroznikPersistManager();
        dpm.updateDocumentWithDroznikValues(document);
	}

	
	/**
	 * Probuje ustalic aspekt dokumentu na podstawie segmentu
	 * @param fm
	 * @return
	 */
	protected String getAspectForDocument(FieldsManager fm) throws EdmException
	{
		log.trace("getAspectForDocument");	
		if (fm.getValue(SEGMENT_FIELD_CN)==null || fm.getValue(TYP_DOKUMENTU_FIELD_CN)==null)
				return "XXX";
		    String segment_type = fm.getField(SEGMENT_FIELD_CN).getEnumItem((Integer) fm.getKey(SEGMENT_FIELD_CN)).getArg(1);	    
		    if (segment_type != null && segment_type.trim().length()>0)
		    {
		    	log.trace(segment_type);
		    	return segment_type.toUpperCase();
		    }
		    
		return "XXX";
	}
	/**
	 * Metoda okresla folder dla dokumentu
	 * @param fm
	 * @param document
	 * @return
	 * @throws EdmException
	 */
	public Folder getProperFolderForDocument(FieldsManager fm, Document document) throws EdmException
	{
		log.trace("getProperFolder id={}",document.getId());
		String stream = getAspectForDocument(fm);
		List<String> hierarchy = new ArrayList<String>();
		
		Calendar cal = Calendar.getInstance();
        cal.setTime(document.getCtime());
		Folder folder = Folder.getRootFolder();
		Folder old = document.getFolder();
		
		
		hierarchy.add(folder.getTitle());
		hierarchy.add("Archiwum dokument�w ProSIKA");
		if ("KK".equalsIgnoreCase(stream))
		{
			log.trace("sciezka dla KK");
			//KK typ dokumentu
			hierarchy.add("Pion kluczowych klient�w");			
			hierarchy.add((String)fm.getValue(TYP_DOKUMENTU_FIELD_CN));
		}
		else
		{
			//BM i COK normalnie
			hierarchy.add((String)fm.getValue(SEGMENT_FIELD_CN));			
		}
		hierarchy.add(String.valueOf(cal.get(Calendar.YEAR)));
		hierarchy.add(String.valueOf(cal.get(Calendar.MONTH)));
		if (log.isTraceEnabled())
		{
			for (String seg : hierarchy)
			{
				log.trace(seg);
			}
		}
		List<String> path = old.getPath();
		if(fm.getValue(SEGMENT_FIELD_CN)!=null)
		{
			boolean requireChange = false;
		
			if(old!=null)
			{
				
				requireChange = checkFolderPaths(path, hierarchy);
			}
			else
				requireChange = true;
			
			if(requireChange)
			{
				boolean notFirst = false;
				for (String folderName : hierarchy)
				{
					if (notFirst) //pierwsza nazwe pomijamy
					{
						folder = folder.createSubfolderIfNotPresent(folderName);
					}
					else
					{
						notFirst = true;
					}
				}				
			}
			else
			{
				folder = old;
			}
			
		}
		else
		{
			if(old==null||(old!=null && !old.getTitle().equals("Niezaindeksowane")))
				folder = folder.createSubfolderIfNotPresent("Niezaindeksowane");
			else
				folder = old;
		}
		
		return folder;
		
	}

	/**
	 * Na zakonczenie procesu wymuszamy zmiane statusu na zakonczony
	 */
	@Override
    public void onEndProcess(OfficeDocument document, boolean isManual)  throws EdmException
	{
		log.trace("endProcess {}, {}",document.getId(),isManual);
		
		if(isManual)
		{
			
			EnumItem actualStatus = document.getFieldsManager().getEnumItem(STATUS_FIELD_CN);
			if (actualStatus == null || actualStatus.getId().intValue()!=30);
			{
				log.trace("zmiana status z wartosci {} na 30",actualStatus);
				Map<String, Object> values = new HashMap<String, Object>();
				values.put(STATUS_FIELD_CN, 30);
				
				document.getDocumentKind().setWithHistory(document.getId(), values, false);				
			}
					
		}
		
	}
	
	/**
	 * Ustawia uprawnienia dla dokumentu prosika
	 */
	public void documentPermissions(Document document) throws EdmException 
	{
		 if (document.getDocumentKind() == null)
	            throw new NullPointerException("document.getDocumentKind()");
		 //DSApi.context().clearPermissions(document);
	        
	     FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
	     java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
	     if(fm.getValue(SEGMENT_FIELD_CN) != null)
	     {	    	 
	    	 String enum_cn = fm.getField(SEGMENT_FIELD_CN).getEnumItem((Integer )fm.getKey(SEGMENT_FIELD_CN)).getCn();		     
		     String segment_type = fm.getField(SEGMENT_FIELD_CN).getEnumItem((Integer) fm.getKey(SEGMENT_FIELD_CN)).getArg(1);		     
		     
		     String enumReadGroupName = "Dokument ProSIKA "+enum_cn+" - odczyt";
		     String enumReadGUID = "PROSIKA_"+enum_cn+"_READ";
		     
		     perms.add(new PermissionBean(ObjectPermission.READ,enumReadGUID,ObjectPermission.GROUP,enumReadGroupName));
		     perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,enumReadGUID,ObjectPermission.GROUP,enumReadGroupName));
		     
		     String enumWriteGroupName = "Dokument ProSIKA "+enum_cn+" - modyfikacja";
		     String enumWriteGUID = "PROSIKA_"+enum_cn+"_MODIFY";
		     
			 
		     perms.add(new PermissionBean(ObjectPermission.MODIFY,enumWriteGUID,ObjectPermission.GROUP,enumWriteGroupName));
		     perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,enumWriteGUID,ObjectPermission.GROUP,enumWriteGroupName));
		     
		     String segmentReadGroupName = "Dokument ProSIKA "+segment_type+" - odczyt";
		     String segmentReadGUID = "PROSIKA_"+segment_type+"_READ";
		     
		     perms.add(new PermissionBean(ObjectPermission.READ,segmentReadGUID,ObjectPermission.GROUP,segmentReadGroupName));
		     perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,segmentReadGUID,ObjectPermission.GROUP,segmentReadGroupName));
		     
		     String segmentWriteGroupName = "Dokument ProSIKA "+segment_type+" - modyfikacja";
		     String segmentWriteGUID = "PROSIKA_"+segment_type+"_MODIFY";
		     
			 
		     perms.add(new PermissionBean(ObjectPermission.MODIFY,segmentWriteGUID,ObjectPermission.GROUP,segmentWriteGroupName));
		     perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,segmentWriteGUID,ObjectPermission.GROUP,segmentWriteGroupName));
		     		     
	     }
	     else
	     {
	    	 perms.add(new PermissionBean(ObjectPermission.READ,"PROSIKA_EMPTY_READ",ObjectPermission.GROUP,"Dokument ProSIKA _EMPTY_ - odczyt"));
	    	 perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"PROSIKA_EMPTY_READ",ObjectPermission.GROUP,"Dokument ProSIKA _EMPTY_ - odczyt"));
	    	 perms.add(new PermissionBean(ObjectPermission.MODIFY,"PROSIKA_EMPTY_MODIFY",ObjectPermission.GROUP,"Dokument ProSIKA _EMPTY_ - modyfikacja"));
	    	 perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"PROSIKA_EMPTY_MODIFY",ObjectPermission.GROUP,"Dokument ProSIKA _EMPTY_ - modyfikacja"));
	    	 
	    	 //createPermission(document, "Dokument ProSIKA _EMPTY_ - odczyt", "PROSIKA_EMPTY_READ", new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS});
	    	 //createPermission(document, "Dokument ProSIKA _EMPTY_ - modyfikacja", "PROSIKA_EMPTY_MODIFY", new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS});
	    	 //DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "PROSIKA_EMPTY_READ", ObjectPermission.GROUP);
		     //DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "PROSIKA_EMPTY_MODIFY", ObjectPermission.GROUP);
	     }
	     this.setUpPermission(document, perms);
	}

	/**
	 * W prosice przyjmujemy zalozenie ze jesli segment != null to mozemy wyznaczyc koordynatora
	 */
	@Override
	public boolean isProcessCoordinator(OfficeDocument document) throws EdmException
    {
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());	
		if (fm.getValue(SEGMENT_FIELD_CN)==null || fm.getValue(TYP_DOKUMENTU_FIELD_CN)==null)
			return false;
    	return true;
    }
	/**
	 * Wyznaczenie koordynatora pocesu
	 */
	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
    {
		ProcessCoordinator coordinator = findCoordinatorByMatrix(document);
		if (coordinator!=null)
			return coordinator;
		coordinator = getIndexingQueueCoordinator(document);
		if (coordinator!=null)
			return coordinator;
    	String channel = document.getDocumentKind().getChannel(document.getId());
    	if(channel != null)
    	{
    		List<ProcessCoordinator> pcl = ProcessCoordinator.find(DocumentLogicLoader.PROSIKA,channel);
	    	 if( pcl != null && pcl.size() > 0)
	    	        return ProcessCoordinator.find(DocumentLogicLoader.PROSIKA,channel).get(0);
	    	 else
	    		 return null;
    	}
    	return null;
    }

	/**
	 * Metoda okresla koordynatora, ktorego zadaniem w przypadku, gdy dokument nie jest w pelni poindeksowany
	 * @param document
	 * @return
	 * @throws EdmException
	 */
	private ProcessCoordinator getIndexingQueueCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator resp = null;
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());	
		if (fm.getValue(TYP_DOKUMENTU_FIELD_CN)==null)			
		{			
			String queue = Docusafe.getAdditionProperty("mala.kancelaria.kolejka");
			log.trace("dokument zostanie zadekretowany celem poindeksowanie do dzialu {}",queue);
			resp = new ProcessCoordinator();
			resp.setGuid(queue);
			resp.setUsername(null);
			resp.setId(0l);
			resp.setChannelCn("INDEXING");
			resp.setDocumentKindCn("prosika");
		}
		return resp;
	}
	/**
	 * Metoda poszukuje koordynatora na podstawie magicznej macierzy
	 * zapisane w tabeli DSG_BM_CHANNEL
	 * @param document
	 * @return
	 * @throws EdmException
	 */
	protected ProcessCoordinator findCoordinatorByMatrix(OfficeDocument document) throws EdmException
	{
		log.trace("findCoordinatorByMatrix id={}",document.getId());
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());	
		if (fm.getValue(SEGMENT_FIELD_CN)==null || fm.getValue(TYP_DOKUMENTU_FIELD_CN)==null)
			return null;
	    String segment_type = fm.getField(SEGMENT_FIELD_CN).getEnumItem((Integer) fm.getKey(SEGMENT_FIELD_CN)).getArg(1);	    
	    if (!segment_type.equals("BM") && !segment_type.equals("KK"))
	    	return null;
		
	    DecisionMatrixProcessor decisionProcessor = new DecisionMatrixProcessor(segment_type);	    
	    return decisionProcessor.findCoordinator(document);				
	}
	public int getPermissionPolicyMode()
    {
        //return PermissionManager.NORMAL_POLICY;
		return PermissionManager.BUSINESS_LIGHT_POLICY;
    }
	
	private Object[] getTaskListParam(DocumentKind dockind,Long id) throws EdmException {
		Object[] result = new Object[10];
		FieldsManager fm = dockind.getFieldsManager(id);
		result[0] = fm.getValue(STATUS_FIELD_CN);
		result[3] = fm.getValue(TYP_DOKUMENTU_FIELD_CN);
		result[1] = fm.getValue(BARCODE_FIELD_CN);
		DicProSIKA dic = (DicProSIKA) fm.getValue(KLIENT_FIELD_CN);
		if(dic != null)
			result[4] = dic.getDictionaryDescription() ;
		result[5] = fm.getValue(WIELOWATKOWY_CN) != null ? fm.getValue(WIELOWATKOWY_CN) : "Nie";
		result[6] = "";
		
		if (fm.getValue(NR_ZLEC_PS_FIELD_CN)!=null)
		{
			result[6] = (String) fm.getValue(NR_ZLEC_PS_FIELD_CN);
		}
		if ("Tak".equalsIgnoreCase((String) fm.getValue(BRAK_W_PS_CN)))
		{
			result[7] = "Tak";
		}
		else
		{
			result[7] = "Nie";
		}
		return result;		
    }

    @Override
    public TaskListParams getTaskListParams(DocumentKind kind, long documentId) throws EdmException {
        return TaskListParams.fromMagicArray(getTaskListParam(kind, documentId));
    }

    @Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException 
	{
		Map<String,Object> values = new HashMap<String,Object>();
    	values.put(SPOSOB_PRZYJECIA_CN, 30);
    	values.put(SEGMENT_FIELD_CN, 140);
        fm.reloadValues(values);
	}

	
	/**
	 * Mapa w ktore cachujemy klasy
	 */
	private static Map<String,Integer> klasyBM = new HashMap<String,Integer>();
	private static Map<String,Integer> klasyKK = new HashMap<String,Integer>();
	/**
	 * Metoda poszukuje koordynatora na podstawie tabeli 
	 * dsg_prosika_tabela_klas
	 * @param document
	 * @return
	 * @throws EdmException	
	 */
	
	protected Integer findClassForDocument(Document document) throws EdmException
	{
		log.trace("findClassForDocument id={}",document.getId());
		
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		
		if (fm.getValue(SEGMENT_FIELD_CN)==null)
		{
			log.trace("segment==null");
			return null;
		}
		if (fm.getValue(TYP_DOKUMENTU_FIELD_CN)==null)
		{	
			log.trace("typ dokumentu==null");
			return null;
		}
	    String segment_type = fm.getField(SEGMENT_FIELD_CN).getEnumItem((Integer) fm.getKey(SEGMENT_FIELD_CN)).getArg(1);
	    log.trace("segmentType={}",segment_type);
	    if (segment_type.equals("BM") || segment_type.equals("KK"))
	    	return findClassForAspect(document,fm,segment_type);
	    
	    return null;
	}
	/**
	 * Pobiera klase dla znanego juz sobie aspektu
	 * @param document
	 * @param fm
	 * @param aspect
	 * @return
	 * @throws EdmException
	 */
	protected Integer findClassForAspect(Document document,FieldsManager fm,String aspect) throws EdmException
	{
		log.trace("findClassForBm id={}",document.getId());
		log.trace("ascpect={}",aspect);
		Map<String,Integer> cache = klasyBM;
		if (aspect.equals("KK"))
		{
			cache = klasyKK;
			log.trace("KK");
		}
		
			    
		EnumItem typ = fm.getEnumItem(TYP_DOKUMENTU_FIELD_CN);
		log.trace("typ={}",typ.getCn());
	    Integer resp = cache.get(typ.getCn());
	    if (resp != null)
	    {
	    	log.trace("wartosc {} znaleziona w cache",resp);
	    	return resp;
	    }
		PreparedStatement ps = null;
		try
		{									
			log.trace("typ dokumentu={}",typ.getCn());
			ps = DSApi.context().prepareStatement("select klasa_id from dsg_prosika_tabela_klas where aspekt = ? and typ_dokumentu_cn = ?");
			ps.setString(1, aspect);
			ps.setString(2, typ.getCn());						
			ResultSet rs = ps.executeQuery();
			if (rs.next())
			{
				resp = rs.getInt("klasa_id");				
				log.trace("wartosc {} znaleziona w tabeli",resp);
			}			
			rs.close();
			if (resp!=null)
			{
				cache.put(typ.getCn(), resp);
				return resp;
			}
			log.warn("nie wyznaczono klasy dla dokumentu: {} ,typ={}",document.getId(),typ.getFieldCn());
		}
		catch (SQLException e)
		{
			throw new EdmException(e.getMessage(),e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		return null;
		
	}
	
	/**
	 * Pobierany jest numer GRUPY_PNA dla okreslonego kodu PNA
	 * @param zip - PNA
	 * @return Grupa PNA
	 * @throws EdmException
	 */
	public Integer getPNAforZip(String zip) throws EdmException
	{
		log.trace("Pobieramy numer grupy PNA dla zip={}",zip);
		Integer ret = 0;
		PreparedStatement ps = null;
		if(zip==null || zip.length()!=6) 
			return ret;
		try
		{			
			ps = DSApi.context().prepareStatement("select pna from dsg_prosika_zip_to_pna where zip=?");
			ps.setString(1, zip);
			ResultSet rs = ps.executeQuery();
			if(rs.next())
			{
				ret = rs.getInt(1);
				log.trace("znaleziono numer grupy={}",ret);
			}
			rs.close();
		}
		catch(Exception e)
		{
			throw new EdmException(e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		return ret;
		
	}
	
	public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
	{
		Date fromField = getValue(kind, values, DATA_DO_TP_CN) != null ? (Date) getValue(kind, values, DATA_DO_TP_CN) : null;
		if(fromField != null && fromField.after(new Date()))
		{
			throw new ValidationException(sm.getString("DataDoTpNiePozniejsza"));
		}
	}
	//Oveerridy dopiero jak zejdziemy z taga prosika_473
	
	//@Override
	public String getExtendedPermissionPolicy()
	{
		return "1.1771.1212.prosika.bm.extended";
	}
	
	//@Override
	public boolean isReadPermissionCode(String permCode)
	{
		return permCode.startsWith("PROSIKA_") && permCode.contains("_READ");
	}
}
