package pl.compan.docusafe.parametrization.prosika;

import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;

/**
 * Klasa nieuzywanan na skutek zmiany koncepcji projektu
 * @author wkutyla
 *
 */
@Deprecated
public class ProsikaBlanklogic extends AbstractDocumentLogic {

	private static ProsikaBlanklogic instance;
	
	public static synchronized ProsikaBlanklogic getInstance()
    {
        if (instance == null)
            instance = new ProsikaBlanklogic();
        return instance;
    }
	
	
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");
		
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Archiwum dokumentów ProSIKA");
		folder = folder.createSubfolderIfNotPresent("Zamówienia");
        
		document.setFolder(folder);
		
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		
		String title = "Zamówienie na dokumenty ProSIKA";
		//document.setTitle(title);
        
		title +=  " ( segregatory od "+fm.getValue("NR_SEGREGATORA_OD")+" do "+fm.getValue("NR_SEGREGATORA_DO")+" )";
		//document.setDescription(title);
		
	}

	
	public void checkCanFinishProcess(OfficeDocument document)
			throws AccessDeniedException, EdmException {
		// TODO Auto-generated method stub

	}

	public void documentPermissions(Document document) throws EdmException 
	{
		 createPermission(document, "Dokument ProSIKA blank - odczyt", "PROSIKA_BLANK_READ", new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS});
		 createPermission(document, "Dokument ProSIKA  blank - modyfikacja", "PROSIKA_BLANK_MODIFY", new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS});
		 DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "PROSIKA_BLANK_READ", ObjectPermission.GROUP);
	     DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "PROSIKA_BLANK_MODIFY", ObjectPermission.GROUP);
	}

	private void createPermission(Document document, String name, String guid, String[] permissions) throws EdmException
    {        
        try
        {
            DSDivision.find(guid);
        }
        catch (DivisionNotFoundException e)
        {
            DSDivision parent = DSDivision.find(DSDivision.ROOT_GUID);
            parent.createGroup(name, guid); 
        }

        DSApi.context().grant(document, permissions, guid, ObjectPermission.GROUP);
    }


	@Override
	public boolean searchCheckPermissions(Map<String, Object> values) {
		// TODO Auto-generated method stub
		return false;
	}

	public void setInitialValues(FieldsManager fm, int type)
			throws EdmException {
		// TODO Auto-generated method stub

	}

}
