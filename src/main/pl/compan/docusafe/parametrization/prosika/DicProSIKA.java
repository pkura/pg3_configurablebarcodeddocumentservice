package pl.compan.docusafe.parametrization.prosika;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.crm.ContractorWorker;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.dictionary.AbstractDocumentKindDictionary;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.util.QueryForm;

public class DicProSIKA extends AbstractDocumentKindDictionary
{

	private static final Logger log = LoggerFactory.getLogger(DicProSIKA.class);
	
	private Long id;
	private String nrEwidencyjny;
	private String nazwaKlienta;

	private String nip;
	private String regon;
	private String nrTelefonuGlowny;
	
	//adres siedziby
	private String as_ulica;
	private String as_kod;
	private String as_miejscowosc;
	private String as_nrdomu;
	private String as_nrlokalu;
	
	//adres instalacyjny
	private String ai_ulica;
	private String ai_kod;
	private String ai_miejscowosc;
	private String ai_nrdomu;
	private String ai_nrlokalu;
	
	//adres korespondencyjny;
	private String ak_ulica;
	private String ak_kod;
	private String ak_miejscowosc;
	private String ak_nrdomu;
	private String ak_nrlokalu;
	
	private static DicProSIKA instance;
	public static synchronized DicProSIKA getInstance()
	{
		if (instance == null)
			instance = new DicProSIKA();
		return instance;
	}
	public DicProSIKA() {}
	
	public String dictionaryAction()
    {
        return "/office/common/dicprosika.action"; // /office/common/dicinvoice.action 
    }
	
    public String dictionaryAccept()
    {
        return "";              
    }
	
    public Map<String,String> dictionaryAttributes()
    {
        Map<String,String> map = new LinkedHashMap<String,String>();
        	map.put("nrEwidencyjny","Numer Ewidencyjny");
        	map.put("nazwaKlienta","Nazwa Klienta");
        	map.put("nip","Nr NIP");
        return map;
    }
	
    public String getDictionaryDescription()
    {
        return nrEwidencyjny + " " + nazwaKlienta + " " + nip;
    }
    
    public String dictionarySortColumn()
    {
        return "nrEwidencyjny";
    }
    
    public void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            log.error("Blad dodania wpisu do s�ownika. "+e.getMessage());
            throw new EdmException("Blad dodania wpisu do slownika. "+e.getMessage());
        }
    }
    
    public void delete() throws EdmException 
    {
        try 
        {
        	DockindQuery dockindQuery = new DockindQuery(0, 10);
        	dockindQuery.setDocumentKind(DocumentKind.findByCn(DocumentLogicLoader.PROSIKA));
        	dockindQuery.field(DocumentKind.findByCn(DocumentLogicLoader.PROSIKA).getFieldByCn(Prosikalogic.KLIENT_FIELD_CN), id);
        	SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
        	if(searchResults.count() > 0 )
        		throw new EdmException("Nie mo�na usun�� klienta zwi�zanego z dokumentem.");
        	
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
        } 
        catch (HibernateException e) 
        {
            log.error("Blad usuniecia wpisu ze slownika");
            throw new EdmException("Blad usuniecia wpisu ze slownika. "+e.getMessage());
        }
    }
    
    public DicProSIKA find(Long id) throws EdmException
    {
        DicProSIKA inst = DSApi.getObject(DicProSIKA.class, id);
        if (inst == null) 
            throw new EdmException("Nie znaleziono wpisu o nr " + id);
        else 
            return inst;
    }

    /**
     * Metoda pobiera wartosci ze slownika
     * @param parametry
     * @return
     * @throws EdmException
     */
    public static List<DicProSIKA> find(Map<String, Object> parametry) throws EdmException 
    {
        Criteria c = DSApi.context().session().createCriteria(DicProSIKA.class);
        if(c == null) 
            throw new EdmException("Nie mozna okreslic kryterium wyszukiwania.");
        if(parametry != null) 
        {
        	if(parametry.get("nrEwidencyjny") != null) 
                c.add(Expression.like("nrEwidencyjny",  "%"+parametry.get("nrEwidencyjny")+"%"));
        	if(parametry.get("nazwaKlienta") != null) 
                c.add(Expression.like("nazwaKlienta",  "%"+parametry.get("nazwaKlienta")+"%"));
            if(parametry.get("nip") != null) 
                c.add(Expression.like("nip", "%" + parametry.get("nip") + "%"));            
            if(parametry.get("regon") != null) 
                c.add(Expression.like("regon", "%" + parametry.get("regon") + "%"));
            if(parametry.get("nrTelefonuGlowny") != null) 
                c.add(Expression.like("nrTelefonuGlowny", "%" + parametry.get("nrTelefonuGlowny") + "%"));
            if(parametry.get("as_ulica") != null) 
                c.add(Expression.like("as_ulica", "%" + parametry.get("as_ulica") + "%"));
            if(parametry.get("as_kod") != null) 
                c.add(Expression.like("as_kod", "%" + parametry.get("as_kod") + "%"));
            if(parametry.get("as_miejscowosc") != null) 
                c.add(Expression.like("as_miejscowosc", "%" + parametry.get("as_miejscowosc") + "%"));
            if(parametry.get("as_nrdomu") != null) 
                c.add(Expression.like("as_nrdomu", "%" + parametry.get("as_nrdomu") + "%"));
            if(parametry.get("as_nrlokalu") != null) 
                c.add(Expression.like("as_nrlokalu", "%" + parametry.get("as_nrlokalu") + "%"));
            if(parametry.get("ai_ulica") != null) 
                c.add(Expression.like("ai_ulica", "%" + parametry.get("ai_ulica") + "%"));
            if(parametry.get("ai_kod") != null) 
                c.add(Expression.like("ai_kod", "%" + parametry.get("ai_kod") + "%"));
            if(parametry.get("ai_miejscowosc") != null) 
                c.add(Expression.like("ai_miejscowosc", "%" + parametry.get("ai_miejscowosc") + "%"));
            if(parametry.get("ai_nrdomu") != null) 
                c.add(Expression.like("ai_nrdomu", "%" + parametry.get("ai_nrdomu") + "%"));
            if(parametry.get("ai_nrlokalu") != null) 
                c.add(Expression.like("ai_nrlokalu", "%" + parametry.get("ai_nrlokalu") + "%"));
            if(parametry.get("ak_ulica") != null) 
                c.add(Expression.like("ak_ulica", "%" + parametry.get("ak_ulica") + "%"));
            if(parametry.get("ak_kod") != null) 
                c.add(Expression.like("ak_kod", "%" + parametry.get("ak_kod") + "%"));
            if(parametry.get("ak_miejscowosc") != null) 
                c.add(Expression.like("ak_miejscowosc", "%" + parametry.get("ak_miejscowosc") + "%"));
            if(parametry.get("ak_nrdomu") != null) 
                c.add(Expression.like("ak_nrdomu", "%" + parametry.get("ak_nrdomu") + "%"));
            if(parametry.get("ak_nrlokalu") != null) 
                c.add(Expression.like("ak_nrlokalu", "%" + parametry.get("ak_nrlokalu") + "%"));
            c.setMaxResults(200);
        }
        try 
        {
            log.info("pobranie listy (DicproSika)");
            return (List<DicProSIKA>)c.list();
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static SearchResults<? extends DicProSIKA> search(QueryForm form) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(DicProSIKA.class);

            if(form.hasProperty("nrEwidencyjny")) 
                c.add(Expression.like("nrEwidencyjny",  "%"+form.getProperty("nrEwidencyjny")+"%"));
        	if(form.hasProperty("nazwaKlienta")) 
                c.add(Expression.like("nazwaKlienta",  "%"+form.getProperty("nazwaKlienta")+"%"));
            if (form.hasProperty("nip"))
                c.add(Expression.like("nip", "%"+form.getProperty("nip")+"%"));            
            if (form.hasProperty("regon"))
                c.add(Expression.like("regon", "%"+form.getProperty("regon")+"%"));            
            if (form.hasProperty("nrTelefonuGlowny"))
                c.add(Expression.like("nrTelefonuGlowny", "%"+form.getProperty("nrTelefonuGlowny")+"%"));            
            
            if (form.hasProperty("as_ulica"))
                c.add(Expression.like("as_ulica", "%"+form.getProperty("as_ulica")+"%"));            
            if (form.hasProperty("as_kod"))
                c.add(Expression.like("as_kod", "%"+form.getProperty("as_kod")+"%"));            
            if (form.hasProperty("as_miejscowosc"))
                c.add(Expression.like("as_miejscowosc", "%"+form.getProperty("as_miejscowosc")+"%"));            
            if (form.hasProperty("as_nrdomu"))
                c.add(Expression.like("as_nrdomu", "%"+form.getProperty("as_nrdomu")+"%"));            
            if (form.hasProperty("as_nrlokalu"))
                c.add(Expression.like("as_nrlokalu", "%"+form.getProperty("as_nrlokalu")+"%"));            
            if (form.hasProperty("ai_ulica"))
                c.add(Expression.like("ai_ulica", "%"+form.getProperty("ai_ulica")+"%"));            
            if (form.hasProperty("ai_kod"))
                c.add(Expression.like("ai_kod", "%"+form.getProperty("ai_kod")+"%"));            
            if (form.hasProperty("ai_miejscowosc"))
                c.add(Expression.like("ai_miejscowosc", "%"+form.getProperty("ai_miejscowosc")+"%"));            
            if (form.hasProperty("ai_nrdomu"))
                c.add(Expression.like("ai_nrdomu", "%"+form.getProperty("ai_nrdomu")+"%"));            
            if (form.hasProperty("ai_nrlokalu"))
                c.add(Expression.like("ai_nrlokalu", "%"+form.getProperty("ai_nrlokalu")+"%"));            
            if (form.hasProperty("ak_ulica"))
                c.add(Expression.like("ak_ulica", "%"+form.getProperty("ak_ulica")+"%"));            
            if (form.hasProperty("ak_kod"))
                c.add(Expression.like("ak_kod", "%"+form.getProperty("ak_kod")+"%"));            
            if (form.hasProperty("ak_miejscowosc"))
                c.add(Expression.like("ak_miejscowosc", "%"+form.getProperty("ak_miejscowosc")+"%"));            
            if (form.hasProperty("ak_nrdomu"))
                c.add(Expression.like("ak_nrdomu", "%"+form.getProperty("ak_nrdomu")+"%"));            
            if (form.hasProperty("ak_nrlokalu"))
                c.add(Expression.like("ak_nrlokalu", "%"+form.getProperty("ak_nrlokalu")+"%"));            
            
            for (Iterator iter=form.getSortFields().iterator(); iter.hasNext(); )
            {
                QueryForm.SortField field = (QueryForm.SortField) iter.next();
                if (field.isAscending())
                    c.addOrder(Order.asc(field.getName()));
                else
                    c.addOrder(Order.desc(field.getName()));
            }
            c.setMaxResults(100);
            List<DicProSIKA> list = (List<DicProSIKA>) c.list();

            if (list.size() == 0)
                return SearchResultsAdapter.emptyResults(DicProSIKA.class);
            else
            {
                int fromIndex = form.getOffset() < list.size() ? form.getOffset() : list.size()-1;
                int toIndex = (form.getOffset()+form.getLimit() <= list.size()) ? form.getOffset()+form.getLimit() : list.size();
                 
                return new SearchResultsAdapter<DicProSIKA>(list.subList(fromIndex, toIndex), form.getOffset(), list.size(), DicProSIKA.class);
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNrEwidencyjny() {
		return nrEwidencyjny;
	}

	public void setNrEwidencyjny(String nrEwidencyjny) {
		this.nrEwidencyjny = nrEwidencyjny;
	}

	public String getNazwaKlienta() {
		return nazwaKlienta;
	}

	public void setNazwaKlienta(String nazwaKlienta) {
		this.nazwaKlienta = nazwaKlienta;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getRegon() {
		return regon;
	}

	public void setRegon(String regon) {
		this.regon = regon;
	}

	public String getNrTelefonuGlowny() {
		return nrTelefonuGlowny;
	}

	public void setNrTelefonuGlowny(String nrTelefonuGlowny) {
		this.nrTelefonuGlowny = nrTelefonuGlowny;
	}

	public String getAs_ulica() {
		return as_ulica;
	}

	public void setAs_ulica(String as_ulica) {
		this.as_ulica = as_ulica;
	}

	public String getAs_kod() {
		return as_kod;
	}

	public void setAs_kod(String as_kod) {
		this.as_kod = as_kod;
	}

	public String getAs_miejscowosc() {
		return as_miejscowosc;
	}

	public void setAs_miejscowosc(String as_miejscowosc) {
		this.as_miejscowosc = as_miejscowosc;
	}

	public String getAs_nrdomu() {
		return as_nrdomu;
	}

	public void setAs_nrdomu(String as_nrdomu) {
		this.as_nrdomu = as_nrdomu;
	}

	public String getAs_nrlokalu() {
		return as_nrlokalu;
	}

	public void setAs_nrlokalu(String as_nrlokalu) {
		this.as_nrlokalu = as_nrlokalu;
	}

	public String getAi_ulica() {
		return ai_ulica;
	}

	public void setAi_ulica(String ai_ulica) {
		this.ai_ulica = ai_ulica;
	}

	public String getAi_kod() {
		return ai_kod;
	}

	public void setAi_kod(String ai_kod) {
		this.ai_kod = ai_kod;
	}

	public String getAi_miejscowosc() {
		return ai_miejscowosc;
	}

	public void setAi_miejscowosc(String ai_miejscowosc) {
		this.ai_miejscowosc = ai_miejscowosc;
	}

	public String getAi_nrdomu() {
		return ai_nrdomu;
	}

	public void setAi_nrdomu(String ai_nrdomu) {
		this.ai_nrdomu = ai_nrdomu;
	}

	public String getAi_nrlokalu() {
		return ai_nrlokalu;
	}

	public void setAi_nrlokalu(String ai_nrlokalu) {
		this.ai_nrlokalu = ai_nrlokalu;
	}

	public String getAk_ulica() {
		return ak_ulica;
	}

	public void setAk_ulica(String ak_ulica) {
		this.ak_ulica = ak_ulica;
	}

	public String getAk_kod() {
		return ak_kod;
	}

	public void setAk_kod(String ak_kod) {
		this.ak_kod = ak_kod;
	}

	public String getAk_miejscowosc() {
		return ak_miejscowosc;
	}

	public void setAk_miejscowosc(String ak_miejscowosc) {
		this.ak_miejscowosc = ak_miejscowosc;
	}

	public String getAk_nrdomu() {
		return ak_nrdomu;
	}

	public void setAk_nrdomu(String ak_nrdomu) {
		this.ak_nrdomu = ak_nrdomu;
	}

	public String getAk_nrlokalu() {
		return ak_nrlokalu;
	}

	public void setAk_nrlokalu(String ak_nrlokalu) {
		this.ak_nrlokalu = ak_nrlokalu;
	}
	@Override
	public Map<String, String> popUpDictionaryAttributes() {
		// TODO Auto-generated method stub
		return null;
	}

	
	    
    
}
