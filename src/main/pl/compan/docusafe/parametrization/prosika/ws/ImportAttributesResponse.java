package pl.compan.docusafe.parametrization.prosika.ws;

public class ImportAttributesResponse 
{
	
	private Integer statusCode;
	private String statusDescription;
	
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusDescription() {
		return statusDescription;
	}
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
	
}
