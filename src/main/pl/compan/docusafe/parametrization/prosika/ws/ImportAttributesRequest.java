package pl.compan.docusafe.parametrization.prosika.ws;

import java.util.Calendar;
import java.util.Date;

public class ImportAttributesRequest 
{
	private String authCode;
	
	private Integer documentID;

	private String contractNumber;

	private String companyName;

	private Long clientID;

	private Long registryID;

	private String service;

	private Calendar tpContractDate;

	private Calendar clientContractDate;

	private Calendar operateDate;

	private Calendar sentToClientDate;

	private Calendar sentToProsikaDate;

	private String contractType;

	private Integer boxNumber;

	public String getAuthCode() {
		return authCode;
	}


	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}


	public Integer getDocumentID() {
		return documentID;
	}


	public void setDocumentID(Integer documentID) {
		this.documentID = documentID;
	}


	public String getContractNumber() {
		return contractNumber;
	}


	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}


	public String getCompanyName() {
		return companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public Long getClientID() {
		return clientID;
	}


	public void setClientID(Long clientID) {
		this.clientID = clientID;
	}


	public String getService() {
		return service;
	}


	public void setService(String service) {
		this.service = service;
	}


	public Calendar getTpContractDate() {
		return tpContractDate;
	}


	public void setTpContractDate(Calendar tpContractDate) {
		this.tpContractDate = tpContractDate;
	}


	public Calendar getClientContractDate() {
		return clientContractDate;
	}


	public void setClientContractDate(Calendar clientContractDate) {
		this.clientContractDate = clientContractDate;
	}


	public Calendar getOperateDate() {
		return operateDate;
	}


	public void setOperateDate(Calendar operateDate) {
		this.operateDate = operateDate;
	}


	public Calendar getSentToClientDate() {
		return sentToClientDate;
	}


	public void setSentToClientDate(Calendar sentToClientDate) {
		this.sentToClientDate = sentToClientDate;
	}


	public Calendar getSentToProsikaDate() {
		return sentToProsikaDate;
	}


	public void setSentToProsikaDate(Calendar sentToProsikaDate) {
		this.sentToProsikaDate = sentToProsikaDate;
	}


	public Long getRegistryID() {
		return registryID;
	}


	public void setRegistryID(Long registryID) {
		this.registryID = registryID;
	}


	public String getContractType() {
		return contractType;
	}


	public void setContractType(String contractType) {
		this.contractType = contractType;
	}


	public Integer getBoxNumber() {
		return boxNumber;
	}


	public void setBoxNumber(Integer boxNumber) {
		this.boxNumber = boxNumber;
	}
	
	


}
