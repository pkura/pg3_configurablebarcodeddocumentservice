package pl.compan.docusafe.parametrization.prosika.ws;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.parametrization.prosika.DicProSIKA;
import edu.emory.mathcs.backport.java.util.Arrays;



public class AttributesUpdaterService 
{

	public static Integer Success = 0;
	public static Integer AuthError = 1;
	public static Integer InvalidDocumentID = 2;
	public static Integer InvalidContractNumber = 3;
	public static Integer InvalidCompanyName = 4;
	public static Integer InvalidClientID = 5;
	public static Integer InvalidRegistryID = 6;
	public static Integer InvalidService = 7;
	public static Integer InvalidTpContractDate = 8;
	public static Integer InvalidClientContractDate = 9;
	public static Integer InvalidOperateDate = 10;
	public static Integer InvalidSentToClientDate = 11;
	public static Integer InvalidSentToProsikaDate = 12;
	public static Integer InvalidContractType = 13;
	public static Integer InvalidBoxNumber = 14;
	public static Integer ERROR = 666;
	
	private Logger log = LoggerFactory.getLogger(AttributesUpdaterService.class);
	
	
	public ImportAttributesResponse ImportAttributes(ImportAttributesRequest request)
	{	
		ImportAttributesResponse response = new ImportAttributesResponse();
		try
		{
		
			Integer result = DroznikUtils.validateRequest(request);
			if(!result.equals(0))
			{
				response.setStatusCode(result);
				return response;
			}
			
			DSApi.openAdmin();
			
			String secret = Docusafe.getAdditionProperty("WSSecret");
			byte[] counted = DroznikUtils.countHash(request.getDocumentID(), request.getContractNumber(), request.getClientContractDate().getTime(), secret);
			byte[] received = request.getAuthCode().getBytes("UTF-8");
			
			if (log.isDebugEnabled())
				log.debug("policzonyHash={}",IOUtils.toString(counted));
			if(!Arrays.equals(counted, received))
			{
				response.setStatusCode(AuthError);
				response.setStatusDescription("Niepoprawny kod weryfikacyjny:"+IOUtils.toString(counted));
				return response;
			}
			
			
			DroznikPersistManager manager = new DroznikPersistManager();
			
			manager.dumpRequest(request);
			
			manager.updateDocumentWithDroznikValuesByDocId(request.getDocumentID().toString());
			/*Map<String, Object> values = new TreeMap<String, Object>();
			values.put("NR_PACZKI", request.getBoxNumber().toString());
			values.put("DATA_ZAWARCIA_KLIENT", request.getClientContractDate());
			values.put("ID_KLIENTA", request.getClientID().toString());
			
			values.put("NR_UMOWY", request.getContractNumber().toString());
			
			Integer contract = 10;
			if(request.getContractType().equals("Standard"))
				contract = 10;
			else if(request.getContractType().equals("NonStandard"))
				contract =20;
			else
			{
				response.setStatusCode(InvalidContractType);
				return response;
			}
			values.put("TYP_UMOWY", contract);
			values.put("DATA_OBOWIAZYWANIA", request.getOperateDate());
			
			values.put("DATA_WYSLANIA_UMOWY_DO_KLIENTA", request.getSentToClientDate());
			values.put("DATA_WYSLANIA_UMOWY_DO_PROSIKA", request.getSentToProsikaDate());
			values.put("USLUGA", request.getService());
			values.put("DATA_ZAWARCIA_TP", request.getTpContractDate());
			
			Map<String, Object> klienciParams = new TreeMap<String, Object>();
			klienciParams.put("nrEwidencyjny", request.getRegistryID().toString());
			
			List<DicProSIKA> klienci = DicProSIKA.find(klienciParams);
			DicProSIKA klient;
			if(klienci.size()>0)
			{
				klient = klienci.get(0);
			}
			else
			{
				klient = new DicProSIKA();
				klient.setNrEwidencyjny(request.getRegistryID().toString());
				klient.setNazwaKlienta(request.getCompanyName());
				DSApi.context().session().save(klient);
				DSApi.context().session().flush();
				DSApi.context().session().refresh(klient);
			}
			values.put("KLIENT", klient.getId());
			
			while(results.hasNext())
			{
				Document doc = results.next();
				
				DSApi.context().begin();
				
				kind.setWithHistory(doc.getId(), values, false);
				DSApi.context().commit();
			}*/
			response.setStatusCode(Success);
		
		}
		catch(Exception e)
		{
			DSApi.context().setRollbackOnly();
			log.error(e.getMessage(),e);
			response.setStatusCode(ERROR);
			response.setStatusDescription(e.getMessage());
		}
		return response;
	}
	
	
}
