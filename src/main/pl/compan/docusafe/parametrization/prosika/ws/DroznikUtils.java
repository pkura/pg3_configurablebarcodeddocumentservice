package pl.compan.docusafe.parametrization.prosika.ws;

import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.EdmException;

public class DroznikUtils 
{
	protected static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	public static byte[] countHash(Integer documentId, Integer contractNumber, Date clientContractDate, String secret) throws EdmException
	{
		return countHash(documentId, contractNumber.toString(), clientContractDate,secret);
	}
	private static Logger log = LoggerFactory.getLogger(DroznikUtils.class);
	
	/**
	 * Liczy hash dla uslugi updatera danych
	 * @param documentId
	 * @param contractNumber
	 * @param clientContractDate
	 * @param secret
	 * @return
	 * @throws EdmException
	 */
	public static byte[] countHash(Integer documentId, String contractNumber, Date clientContractDate, String secret) throws EdmException
	{
		try
		{
			if (log.isTraceEnabled())
			{
				log.trace("countHash");
				log.trace("documentId={}",documentId);
				log.trace("contractNumber={}",contractNumber);
				log.trace("clientContractDate={}",clientContractDate);
			}
			byte[] result = null;
			if(secret==null) 
			{
				log.trace("secret==null");
				secret ="";
			}
			else
			{
				log.trace("secret!=null");
			}
			String salt = "sdf@#$mp";
			
			String toCount = secret + documentId + salt + contractNumber + sdf.format(clientContractDate);
			log.debug("hashKey={}",toCount);
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			md.update(toCount.getBytes("UTF-8"));
			
			result = Base64.encodeBase64(md.digest());

			return result;
		}
		catch (Exception e) 
		{
			throw new EdmException(e);
		}
	}
	
	public static byte[] countHashForTpFeedback(Date archiveDate, String barcode, String documentUrl) throws EdmException 
	{
		try
		{
			log.trace("countHashForTpFeedback");
			
			String authCodeSalt = "WEcv@sa*^$";
			String toCount = sdf.format(archiveDate) + authCodeSalt + barcode + documentUrl;
			log.debug("hashKey={}",toCount);
			 
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			md.update(toCount.getBytes("UTF-8"));
			
			byte[] result = Base64.encodeBase64(md.digest());
	
			return result;
		}
		catch (Exception e) 
		{
			throw new EdmException(e);
		}
	}
	public static Integer validateRequest(ImportAttributesRequest req)
	{
		if(req.getAuthCode()==null || req.getAuthCode().length()<10)
		{
			return AttributesUpdaterService.AuthError;
		}
		if(req.getDocumentID() == null) // || req.getDocumentID()  .equals(""))
		{
			return AttributesUpdaterService.InvalidDocumentID;
		}
		return AttributesUpdaterService.Success;
	}
	
}
