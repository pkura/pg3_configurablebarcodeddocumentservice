package pl.compan.docusafe.parametrization.prosika.ws;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.io.IOUtils;
import org.hibernate.criterion.Expression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.parametrization.prosika.DicProSIKA;
import pl.compan.docusafe.parametrization.prosika.DroznikBean;

public class DroznikPersistManager
{

	private static Logger log = LoggerFactory.getLogger(DroznikPersistManager.class);
	public void dumpRequest(ImportAttributesRequest req) throws EdmException
	{
		try
		{
			DSApi.context().begin();
			DroznikBean bean;
			List<DroznikBean> beans = DSApi.context().session().createCriteria(DroznikBean.class).add(Expression.eq("contractNumber", req.getContractNumber())).list();
			if(beans.size()>0)
				bean = beans.get(0);
			else
				bean = new DroznikBean();
			
			Integer contract = 10;
			if(req.getContractType().equals("Standard"))
				contract = 10;
			else if(req.getContractType().equals("NonStandard"))
				contract = 20;
			
			bean.setDocumentID(toStringOrNull(req.getDocumentID()));
			bean.setContractNumber(toStringOrNull(req.getContractNumber()));
			bean.setCompanyName(req.getCompanyName());
			bean.setClientId(toStringOrNull(req.getClientID()));
			bean.setRegistryId(toStringOrNull(req.getRegistryID()));
			bean.setService(toStringOrNull(req.getService()));
			bean.setBoxNumber(toStringOrNull(req.getBoxNumber()));
			bean.setContractType(contract);
			bean.setTpContractDate(req.getTpContractDate().getTime());
			bean.setClientContractDate(req.getClientContractDate().getTime());
			bean.setOperateDate(req.getOperateDate().getTime());
			bean.setSentToClientDate(req.getSentToClientDate().getTime());
			bean.setSentToProsikaDate(req.getSentToProsikaDate().getTime());
			DSApi.context().session().saveOrUpdate(bean);
			DSApi.context().commit();
			
		}
		catch(Exception e)
		{
			DSApi.context().setRollbackOnly();
			throw new EdmException(e);
		}
	}
	
	/**
	 * Updateuje danymi z droznika jeden okreslony dokument. Do napisania implementacja
	 * @param documentId
	 * @throws EdmException
	 */
	public Integer updateDocumentWithDroznikValues(Document document) throws EdmException
	{
	
		try
		{
			DocumentKind kind = document.getDocumentKind();
			FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
			
			//Boolean updated = fm.getBoolean("DROZNIK_UPDATED");
			//if(updated) return 0;
			
			//String iddok = (String)fm.getValue("ID_DOK");
			String contractNumber = (String)fm.getValue("NR_UMOWY");
			List<DroznikBean> beans = DSApi.context().session().createCriteria(DroznikBean.class).add(Expression.eq("contractNumber", contractNumber)).list();
			if(beans.size()<1)
				return 0;
			DroznikBean bean = beans.get(0);
			
			
			Map<String, Object> values = new TreeMap<String, Object>();
			values.put("NR_PACZKI", bean.getBoxNumber());
			values.put("DATA_ZAWARCIA_KLIENT", bean.getClientContractDate());
			values.put("ID_KLIENTA", bean.getClientId());
			values.put("ID_DOK", bean.getDocumentID());
			//values.put("NR_UMOWY", bean.getContractNumber());
			
			values.put("TYP_UMOWY", bean.getContractType());
			values.put("DATA_OBOWIAZYWANIA", bean.getOperateDate());
			
			values.put("DATA_WYSLANIA_UMOWY_DO_KLIENTA", bean.getSentToClientDate());
			values.put("DATA_WYSLANIA_UMOWY_DO_PROSIKA", bean.getSentToProsikaDate());
			values.put("USLUGA", bean.getService());
			values.put("DATA_ZAWARCIA_TP", bean.getTpContractDate());
			values.put("DROZNIK_UPDATED", true);
			
			Map<String, Object> klienciParams = new TreeMap<String, Object>();
			klienciParams.put("nrEwidencyjny", bean.getRegistryId());
			
			List<DicProSIKA> klienci = DicProSIKA.find(klienciParams);
			DicProSIKA klient;
			if(klienci.size()>0)
			{
				klient = klienci.get(0);
			}
			else
			{
				klient = new DicProSIKA();
				klient.setNrEwidencyjny(bean.getRegistryId());
				klient.setNazwaKlienta(bean.getCompanyName());
				DSApi.context().session().save(klient);
				DSApi.context().session().flush();
				DSApi.context().session().refresh(klient);
			}
			values.put("KLIENT", klient.getId());
			
			kind.setWithHistory(document.getId(), values, false);
			sendFeedbackToDroznik(document);
			return 1;
		}
		catch (Exception e) 
		{
			throw new EdmException(e);
		}
	}
	
	/**
	 * Updatuje danymi pobranymi z droznika wszystkie dokumenty kt�re maj� okre�lone docId
	 * @param contractNumber
	 * @return
	 * @throws EdmException
	 */
	public Integer updateDocumentWithDroznikValuesByDocId(String contractNumber) throws EdmException
	{

		try
		{
			
			DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.PROSIKA);
			DockindQuery query = new DockindQuery(0, 1000);
			query.setDocumentKind(kind);
			query.field(kind.getFieldByCn("NR_UMOWY"), contractNumber);
			
			SearchResults<Document> results = DocumentKindsManager.search(query);
			if(results.count()<0)
			{
				return 0;
			}
			if(!results.hasNext())
			{
				return 0;
			}
			
			//List<DroznikBean> beans = DSApi.context().session().createCriteria(DroznikBean.class).add(Expression.eq("documentID", docId)).list();
			List<DroznikBean> beans = DSApi.context().session().createCriteria(DroznikBean.class).add(Expression.eq("contractNumber", contractNumber)).list();
			if(beans.size()<1)
				return 0;
			DroznikBean bean = beans.get(0);
			
			Map<String, Object> values = new TreeMap<String, Object>();
			values.put("NR_PACZKI", bean.getBoxNumber());
			values.put("DATA_ZAWARCIA_KLIENT", bean.getClientContractDate());
			values.put("ID_KLIENTA", bean.getClientId());
			
			values.put("NR_UMOWY", bean.getContractNumber());
			
			values.put("TYP_UMOWY", bean.getContractType());
			values.put("DATA_OBOWIAZYWANIA", bean.getOperateDate());
			
			values.put("DATA_WYSLANIA_UMOWY_DO_KLIENTA", bean.getSentToClientDate());
			values.put("DATA_WYSLANIA_UMOWY_DO_PROSIKA", bean.getSentToProsikaDate());
			values.put("USLUGA", bean.getService());
			values.put("DATA_ZAWARCIA_TP", bean.getTpContractDate());
			values.put("DROZNIK_UPDATED", true);
			
			Map<String, Object> klienciParams = new TreeMap<String, Object>();
			klienciParams.put("nrEwidencyjny", bean.getRegistryId());
			
			List<DicProSIKA> klienci = DicProSIKA.find(klienciParams);
			DicProSIKA klient;
			if(klienci.size()>0)
			{
				klient = klienci.get(0);
			}
			else
			{
				klient = new DicProSIKA();
				klient.setNrEwidencyjny(bean.getRegistryId());
				klient.setNazwaKlienta(bean.getCompanyName());
				DSApi.context().session().save(klient);
				DSApi.context().session().flush();
				DSApi.context().session().refresh(klient);
			}
			values.put("KLIENT", klient.getId());
			
			Integer counter = 0;
			while(results.hasNext())
			{
				try
				{
					Document doc = results.next();
					
					DSApi.context().begin();
					
					kind.setWithHistory(doc.getId(), values, false);
					DSApi.context().commit();
					sendFeedbackToDroznik(doc);
					counter++;
				}
				catch (Exception e) 
				{
					DSApi.context().setRollbackOnly();
				}
			}
			log.debug("{}",counter);
			return counter;
			
		}
		catch (Exception e) 
		{
			DSApi.context().setRollbackOnly();
			throw new EdmException(e);
		}
		
	}
	
	/**
	 * Metoda rejestruje event, odsylajocy potwierdzenie dla dla droznika
	 * @param document
	 * @param fm
	 * @throws Exception
	 */
	protected void sendFeedbackToDroznik(Document document) throws Exception
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		log.trace("sendFeedbackToDroznik");
		log.trace("idDokumentu={}",document.getId());
		String idDok =  toStringOrEmptyString(fm.getFieldValues().get("ID_DOK"));
		log.trace("id_dok={}",idDok);
		String barcode = toStringOrEmptyString(fm.getFieldValues().get("BARCODE"));
		log.trace("barcode={}",barcode);
		Date dataArchiwizacji = (Date)fm.getValue("DATA_ZARCHIWIZOWANIA");
		if (dataArchiwizacji == null)
		{
			log.warn("data archiwizacji == null w dokumencie {}",document.getId());
			dataArchiwizacji = new Date();
		}
		String link = "nolink";
		String key = "nokey";
		byte[] countedHash = DroznikUtils.countHashForTpFeedback(dataArchiwizacji, barcode, link);
		String authCode = IOUtils.toString(countedHash);
		
		//"authCode|idDokumentu|documentBarcode|documentArchiveDate|documentLink|key"
		StringBuilder eventParams = new StringBuilder();
		eventParams.append(authCode);
		eventParams.append("|");
		eventParams.append(idDok);
		eventParams.append("|");
		eventParams.append(barcode);
		eventParams.append("|");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");		
		eventParams.append(sdf.format(dataArchiwizacji));
		eventParams.append("|");
		eventParams.append(link);
		eventParams.append("|");
		eventParams.append(key);
		
		EventFactory.registerEvent("immediateTrigger",
				"AttributesExport",eventParams.toString(),null,
				new Date());
	}
	/**
	 * 
	 * @param o
	 * @return
	 */
	private String toStringOrNull(Object o)
	{
		if(o==null) return null;
		return o.toString();
	}
	
	private String toStringOrEmptyString(Object o)
	{
		if(o==null) return "";
		return o.toString();
	}
	
	
}
