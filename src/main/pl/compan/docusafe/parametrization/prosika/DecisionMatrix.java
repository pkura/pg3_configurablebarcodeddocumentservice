package pl.compan.docusafe.parametrization.prosika;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;

/**
 * Klasa implementujaca macierz decyzjna Macierz jest przypisana do danego GUIDu i
 * podejmuje decyzje zwiazane dekretacja zadan prosiki do dalszych miejsc. 
 * @author wkutyla
 *
 */
public class DecisionMatrix 
{
	static final Logger log = LoggerFactory.getLogger(DecisionMatrix.class);
	/**
	 * GUID z ktorym zwiazana jest macierz
	 */
	private String matrixGuid;
	
	/**
	 * Konstruktor domyslny
	 */
	public DecisionMatrix()
	{
		super();
	}
	
	/**
	 * Konstruktor ze wskazaniem Guidu.
	 * @param guid
	 */
	public DecisionMatrix(String guid)
	{
		this();
		matrixGuid = guid;
	}

	/**
	 * Lista dostepnych regul dla macierzy
	 * @return
	 */
	
	public Collection<ProsikaDecisionRule> getMatrixRules() throws EdmException
	{
		String q = "select r from r in class " + ProsikaDecisionRule.class.getName() + " where r.matrixGuid = ?";
		Query query = DSApi.context().session().createQuery(q);
		query.setString(0, matrixGuid);		
		return query.list();
	}
	
	/**
	 * Lista regul kierujacych dany dokument do tej macierzy z macierzy innych
	 * @return
	 * @throws EdmException
	 */
	public Collection<ProsikaDecisionRule> getTargetingRules() throws EdmException
	{
		String q = "select r from r in class " + ProsikaDecisionRule.class.getName() + " where r.targetGuid = ?";
		Query query = DSApi.context().session().createQuery(q);
		query.setString(0, matrixGuid);
		return query.list();
	}
	/**
	 * Metoda zwraca wynik wyszukania dla macierzy 
	 * @param document
	 * @return
	 * @throws EdmException
	 */
	public MatrixResultBean findCoordinator(OfficeDocument document) throws Exception
	{
		int ruleId = -1;
		log.trace("findCoordinator documentId={}, matrixGuid={}",document.getId(),matrixGuid);
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		MatrixResultBean resp = null;
		PreparedStatement ps = null;
		try
		{
			ps = DSApi.context().prepareStatement(query);
			EnumItem segment  = fm.getEnumItem(Prosikalogic.SEGMENT_FIELD_CN);
			
			EnumItem typ = fm.getEnumItem(Prosikalogic.TYP_DOKUMENTU_FIELD_CN);
			EnumItem podTyp = fm.getEnumItem("PODTYP_DOKUMENTU");
			EnumItem kanalWplywu = fm.getEnumItem(Prosikalogic.SPOSOB_PRZYJECIA_CN);
			if (log.isTraceEnabled())
			{
				log.trace("segment={}",safeEnumCnValue(segment));
				log.trace("typ={}",safeEnumCnValue(typ));
				log.trace("podtyp={}",safeEnumCnValue(podTyp));
				log.trace("kanalWplywu={}",safeEnumCnValue(kanalWplywu));
				log.trace("grupaPNA={}",fm.getValue(Prosikalogic.PNA_CN));
			}
			ps.setString(1, safeEnumCnValue(segment));
			ps.setString(2, safeEnumCnValue(typ));
			ps.setString(3, safeEnumCnValue(podTyp));
			ps.setString(4, safeEnumCnValue(kanalWplywu));
			Integer grupaPna = null;
			try
			{
				if (fm.getValue(Prosikalogic.PNA_CN)!=null)
					grupaPna = new Integer(fm.getValue(Prosikalogic.PNA_CN).toString());
			}
			catch (Exception ex)
			{
				grupaPna = null;
			}
			if (grupaPna!=null)
			{
				ps.setInt(5, grupaPna);
				ps.setInt(6, grupaPna);
			}
			else
			{
				ps.setObject(5, null);
				ps.setObject(6, null);
			}
			ps.setString(7, matrixGuid);
			ResultSet rs = ps.executeQuery();
			if (rs.next())
			{
				resp = new MatrixResultBean();
				resp.setTargetGuid(rs.getString("target_guid"));
				resp.setTargetUser(rs.getString("target_user"));
				if (rs.getObject("auto_assign_flag")!=null && rs.getInt("auto_assign_flag")==1)
				{
					resp.setAutoAssing(true);
				}
				else
				{
					resp.setAutoAssing(false);
				}		
				resp.setRuleId(rs.getInt("id"));
			}
			rs.close();
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		if (resp !=null)
		{
			log.debug("wynik operacji dla dokumentu {}, regula numer {}",document.getId(),resp.getRuleId());
		}
		else
		{
			log.info("wynik operacji dla dokumentu {}, brak reguly",document.getId());
		}
		return resp;
	}		
	
	/**
	 * Bezpeczna wartosc CN z pola Enum
	 * @param enumItem
	 * @return
	 */
	private String safeEnumCnValue(EnumItem enumItem)
	{
		if (enumItem!=null)
			return enumItem.getCn();
		return null;
	}
	static final StringBuilder query = new StringBuilder()
	.append("select * from dsg_prosika_decision_matrix where ")
	.append("(segment_cn is null or segment_cn = ?) and ")
	.append("(typ_dokumentu_cn is null or typ_dokumentu_cn = ?) and ")
	.append("(podtyp_dokumentu_cn is null or podtyp_dokumentu_cn = ?) and ")
	.append("(kanal_wplywu_cn is null or kanal_wplywu_cn = ?) and ")
	.append("(grupa_pn_from is null or grupa_pn_from <= ?) and ")
	.append("(grupa_pn_to is null or grupa_pn_to >= ?) and ")
	.append("matrix_guid = ? order by priority");
			 
			
			
			
					
				
				 
	public String getMatrixGuid() 
	{
		return matrixGuid;
	}

	public void setMatrixGuid(String matrixGuid) 
	{
		this.matrixGuid = matrixGuid;
	}
	
	//public 
}
