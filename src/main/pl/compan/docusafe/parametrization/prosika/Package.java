package pl.compan.docusafe.parametrization.prosika;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.webwork.ServletActionContext;

/**
 * Klasa zwiazana z paczkami dla barcodow
 * @author wkutyla
 *
 */
public class Package 
{
	private Long id;
	private String numer;
	private Integer segment;
	private Integer typDokumentu;
	private Integer rodzajPrzesylki;
	private Integer lokalizacja;
	/**
	 * Lokalizacja odbiorcy wedlug slownika lokalizacji
	 */
	private Integer odbiorcaLokalizacja;
	private String opis;
	private String user;
	private Integer status;
	private Date ctime;
	private Date mtime;
	private Set<PackageElement> packageElements;
	private String transportServiceName;
	
	
	public static Map<Integer,String> statusMap;
	public static Map<Integer,String> rodzajeMap;
	public static Map<Integer,String> statusElementMap;
	
	public static final int STATUS_PACKAGE_NOWA = 1;
	public static final int STATUS_PACKAGE_WYSLANA = 2;
	public static final int STATUS_PACKAGE_PRZETWORZONA = 3;
	public static final int STATUS_PACKAGE_BLEDNA = 4;
	
	public static final int STATUS_ZGODNA = 5;
	public static final int STATUS_Z_BRAKAMI = 6;
	public static final int STATUS_NADMIAROWA = 7;
	public static final int STATUS_NADMIAROWA_Z_BRAKAMI = 8;
	public static final int STATUS_NIEZWERYFIKOWANA = 9;
	
	public static final int STATUS_ELEMENT_ZGODNA = 1;
	public static final int STATUS_ELEMENT_BRAK = 2;
	public static final int STATUS_ELEMENT_NADMIAROWA = 3;
	public static final int STATUS_ELEMENT_NIEZWERYFIKOWANA = 4;
	
	static
	{
		statusMap = new HashMap<Integer, String>();
		statusMap.put(STATUS_PACKAGE_NOWA, "Nowa");
		statusMap.put(STATUS_PACKAGE_WYSLANA, "Wys�ana");
		statusMap.put(STATUS_PACKAGE_PRZETWORZONA, "Przetworzona");
		statusMap.put(STATUS_PACKAGE_BLEDNA, "B��dna");
		statusMap.put(STATUS_ZGODNA, "Zgodna");
		statusMap.put(STATUS_Z_BRAKAMI, "Z brakami");
		statusMap.put(STATUS_NADMIAROWA, "Nadmiarowa");
		statusMap.put(STATUS_NADMIAROWA_Z_BRAKAMI, "Nadmiarowa z brakami");
		statusMap.put(STATUS_NIEZWERYFIKOWANA, "Niezweryfikowana");
		
		rodzajeMap = new HashMap<Integer, String>();
		rodzajeMap.put(1, "do opracowania");
		rodzajeMap.put(2, "do archiwizacji");
		
		statusElementMap = new HashMap<Integer, String>();
		statusElementMap.put(STATUS_ELEMENT_ZGODNA, "zgodna");
		statusElementMap.put(STATUS_ELEMENT_BRAK, "brak");
		statusElementMap.put(STATUS_ELEMENT_NADMIAROWA, "nadmiarowa");
		statusElementMap.put(STATUS_ELEMENT_NIEZWERYFIKOWANA, "niezweryfikowana");

	}
	
	private static final Log log = LogFactory.getLog(Package.class);
	
	
	public Package()
	{
	}
	
	public static String convertStatusToString(int status) {
		return statusMap.get(status);
	}
	
	public static String convertStatusElementToString(int status) {
		return statusElementMap.get(status);
	}
	
	public static List<Package> find(String numer, String user, String dateFrom, String dateTo) throws EdmException 
	{
		Criteria criteria = DSApi.context().session().createCriteria(Package.class);
		if (numer != null)
			criteria.add(Restrictions.eq("numer", numer));
    	if (user!= null)
    		criteria.add(Restrictions.eq("user", user));
    	if (dateFrom != null && dateTo != null)
    		criteria.add(Restrictions.between("ctime", DateUtils.parseJsDate(dateFrom), DateUtils.parseJsDate(dateTo)));
    	else if (dateFrom != null)
    		criteria.add(Restrictions.ge("ctime", DateUtils.parseJsDate(dateFrom)));
    	else if (dateTo != null)
    		criteria.add(Restrictions.le("ctime", DateUtils.parseJsDate(dateTo)));
    	return criteria.list();
	}
	
	public static List<Package> findByNumer(String numer) throws EdmException 
	{
    	Criteria crit = DSApi.context().session().createCriteria(Package.class);
    	crit.add(Restrictions.eq("numer", numer));
    	
    	return crit.list();
	}
	
	public static Package find(Long id) throws EdmException
	{
		  return (Package) Finder.find(Package.class, id);
	}
	
	public Set<PackageElement> getPackageElements()
	{
    	return packageElements;
	}
	
	public void setPackageElements(Set<PackageElement> packageElements) 
	{
		this.packageElements = packageElements;
	}

	public static List<Package> findByUser(String user) throws EdmException 
	{
    	Criteria crit = DSApi.context().session().createCriteria(Package.class);
    	crit.add(Restrictions.eq("user", user));
    	
    	return crit.list();
	}
	
	public static List<Package> findByState(Integer state) throws EdmException 
	{
    	Criteria crit = DSApi.context().session().createCriteria(Package.class);
    	crit.add(Restrictions.eq("status", state));
    	//crit.add(Expre.eq("status", state));
    	
    	return crit.list();
	}
	
	public void setStatusByElements()
	{
		if(this.getStatus().intValue() == Package.STATUS_PACKAGE_WYSLANA)
			return;
		
		int countZgodna = 0;
		int countBrak = 0;
		int countNadmiarowa = 0;
		int countNiezweryfikowana = 0;
		
		for(PackageElement element : this.packageElements)
		{
			switch (element.getStatus()) 
			{
			
			case STATUS_ELEMENT_ZGODNA:
				LoggerFactory.getLogger("tomekl").debug("STATUS_ELEMENT_ZGODNA");
				countZgodna++;
					break;
				
			case STATUS_ELEMENT_BRAK:
				LoggerFactory.getLogger("tomekl").debug("STATUS_ELEMENT_BRAK");
				countBrak++;
					break; 
			
			case STATUS_ELEMENT_NADMIAROWA:
				LoggerFactory.getLogger("tomekl").debug("STATUS_ELEMENT_NADMIAROWA");
				countNadmiarowa++;
					break;
			
			case STATUS_ELEMENT_NIEZWERYFIKOWANA:
				LoggerFactory.getLogger("tomekl").debug("STATUS_ELEMENT_NIEZWERYFIKOWANA");
				countNiezweryfikowana++;
					break;
					
			//default:
			//	countNiezweryfikowana++;
            //   break;
		
			}
		}
		
		if(countNiezweryfikowana > 0)
		{
			this.setStatus(STATUS_NIEZWERYFIKOWANA);
		}
		else if(countNadmiarowa > 0 && countBrak > 0)
		{
			this.setStatus(STATUS_NADMIAROWA_Z_BRAKAMI);
		}
		else if(countNadmiarowa > 0 && countBrak == 0)
		{
			this.setStatus(STATUS_NADMIAROWA);
		}
		else if(countNadmiarowa == 0 && countBrak > 0)
		{
			this.setStatus(STATUS_Z_BRAKAMI);
		}
		else if(this.packageElements.size() == countZgodna)
		{
			this.setStatus(STATUS_ZGODNA);
		}
	}
	
	/**	
	 * @param packageId id paczki
	 * @param email <br/>true -<i>tworzy raport do zalacznika</i> email <br/> false - <i>tworzy raport do pobrania przez przegladarke</i>
	 * @return sciezka do pliku z raportem
	 */
	public static String pdfReportForElements(Long packageId, boolean email, ActionEvent event) {
		File pdfFile = null;
    	try
    	{
    		Package pack = Package.find(packageId);
    		
            File fontDir = new File(Docusafe.getHome(), "fonts");
            File arial = new File(fontDir, "arial.ttf");
            BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            int fontsize = 10;
            Font font = new Font(baseFont, (float)fontsize);

            pdfFile = File.createTempFile("docusafe_", "_tmp");
            Document pdfDoc =
                new Document(PageSize.A4.rotate(),30,60,30,30);
            PdfWriter.getInstance(pdfDoc, new FileOutputStream(pdfFile));
            
            HeaderFooter footer =
           	 new HeaderFooter(new Phrase("Sporz�dzone przez "+DSApi.context().getDSUser().asFirstnameLastname()+ " " +
                        DateUtils.formatCommonDateTime(new Date()) + " Strona nr ", font), new Phrase("."));
            footer.setAlignment(Element.ALIGN_CENTER);
            pdfDoc.setFooter(footer);
            
            pdfDoc.open();

            PdfUtils.addHeaderImageToPdf(pdfDoc, "Szczeg�y paczki numer ".concat(pack.getNumer())+" Status "+Package.convertStatusToString(pack.getStatus()), baseFont);

            int[] widths = new int[] { 3, 3 , 2};

            Table table = new Table(widths.length);
            table.setWidths(widths);
            table.setWidth(100);
            table.setCellsFitPage(true);
            table.setPadding(3);

            Cell cell_nb = new Cell(new Phrase("Numer pozycji", font));
            cell_nb.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(cell_nb);

            Cell cell_status = new Cell(new Phrase("Barkod", font));
            cell_status.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(cell_status);
            table.endHeaders();
            
            Cell cell_document_count = new Cell(new Phrase("Status", font));
            cell_document_count.setHorizontalAlignment(Cell.ALIGN_CENTER);
            table.addCell(cell_document_count);
            table.endHeaders();
            
            for (PackageElement packElem : pack.getPackageElements()) {
            	table.addCell(new Phrase(packElem.getKod(), font));
            	table.addCell(new Phrase(packElem.getBarcode(), font));
            	table.addCell(new Phrase(Package.convertStatusElementToString(packElem.getStatus()), font));                	
            }
            
            pdfDoc.add(table);
            pdfDoc.close();
    	}
    	catch (Exception ex) 
    	{
    		if (event != null)
    			event.addActionError(ex.getMessage());
    		log.error("", ex);
		}
    	
        if (pdfFile != null && pdfFile.exists()) {
        	if (!email) {
	            try {
	                ServletUtils.streamResponse(ServletActionContext.getResponse(), new FileInputStream(pdfFile),
	                    "application/pdf", (int) pdfFile.length(),
	                    "Content-Disposition: attachment; filename=\"packages.pdf\"");
	                ServletActionContext.getResponse().getOutputStream().flush();
	                ServletActionContext.getResponse().getOutputStream().close();
	            } catch (IOException ex) {
	            	if (event != null)
	            		event.addActionError(ex.getMessage());
	                log.error("", ex);
	            } finally {
	            	pdfFile.delete();
	            }
        	} else {
        		return pdfFile.getAbsolutePath();
        	}
        }
        return null;
	}

	public String getNumer() {
		return numer;
	}

	public void setNumer(String numer) {
		this.numer = numer;
	}

	public Integer getSegment() {
		return segment;
	}

	public void setSegment(Integer segment) {
		this.segment = segment;
	}

	public Integer getLokalizacja() {
		return lokalizacja;
	}

	public void setLokalizacja(Integer lokalizacja) {
		this.lokalizacja = lokalizacja;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setTypDokumentu(Integer typDokumentu) {
		this.typDokumentu = typDokumentu;
	}

	public Integer getTypDokumentu() {
		return typDokumentu;
	}

	public void setRodzajPrzesylki(Integer rodzajPrzesylki) {
		this.rodzajPrzesylki = rodzajPrzesylki;
	}

	public Integer getRodzajPrzesylki() {
		return rodzajPrzesylki;
	}

	public void create() throws EdmException 
	{
		 try 
        {
			 this.ctime = new Date();
            DSApi.context().session().save(this);
            DSApi.context().session().flush();	       
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
		
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getUser() {
		return user;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

	public Date getCtime() {
		return ctime;
	}

	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}

	public Date getMtime() {
		return mtime;
	}

	public void setMtime(Date mtime) {
		this.mtime = mtime;
	}

	public String getTransportServiceName() {
		return transportServiceName;
	}

	public void setTransportServiceName(String transportServiceName) {
		this.transportServiceName = transportServiceName;
	}

	public void setOdbiorcaLokalizacja(Integer odbiorcaLokalizacja) {
		this.odbiorcaLokalizacja = odbiorcaLokalizacja;
	}

	public Integer getOdbiorcaLokalizacja() {
		return odbiorcaLokalizacja;
	}
}
