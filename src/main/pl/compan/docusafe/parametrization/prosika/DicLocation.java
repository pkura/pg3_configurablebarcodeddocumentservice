package pl.compan.docusafe.parametrization.prosika;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.dockinds.dictionary.AbstractDocumentKindDictionary;
import pl.compan.docusafe.parametrization.ilpol.DLBinder;
import pl.compan.docusafe.util.QueryForm;

public class DicLocation extends AbstractDocumentKindDictionary{
	
	private static final Logger log = Logger.getLogger(DicLocation.class);
	
	private Long id;
	private String name;
	private String street;
	private String zip;
	private String city;
	private Boolean official;
	
	private static DicLocation instance;
	public static synchronized DicLocation getInstance()
	{
		if (instance == null)
			instance = new DicLocation();
		return instance;
	}
	public DicLocation() {}
	
	public String dictionaryAction()
    {
        return "/office/common/diclocation.action";  
    }
	
    public String dictionaryAccept()
    {
        return "";              
    }
    
    public String getShortDescription()
    {
    	return getName()+" "+getZip()+" "+getCity();
    }
	
    public Map<String,String> dictionaryAttributes()
    {
        Map<String,String> map = new LinkedHashMap<String,String>();
        	map.put("name","Nazwa Lokalizacji");
        	map.put("street","Ulica");
        	map.put("zip","Kod pocztowy");
        	map.put("city","Miasto");
        return map;
    }
	
    public String getDictionaryDescription()
    {
        return name + ", " + street + ", " + zip + " " + city;
    }
    
    public String dictionarySortColumn()
    {
        return "name";
    }
    
    public void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            log.error("Blad dodania wpisu do s�ownika. "+e.getMessage());
            throw new EdmException("Blad dodania wpisu do slownika. "+e.getMessage());
        }
    }
    
	public static List<DicLocation> list() throws EdmException 
	{
        Criteria c = DSApi.context().session().createCriteria(DicLocation.class);
        return (List<DicLocation>) c.list();		
	}
    
    public void delete() throws EdmException 
    {
        try 
        {
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
        } 
        catch (HibernateException e) 
        {
            log.error("Blad usuniecia wpisu ze slownika");
            throw new EdmException("Blad usuniecia wpisu ze slownika. "+e.getMessage());
        }
    }
    
    public DicLocation find(Long id) throws EdmException
    {
        DicLocation inst = DSApi.getObject(DicLocation.class, id);
        if (inst == null) 
            throw new EdmException("Nie znaleziono wpisu o nr " + id);
        else 
            return inst;
    }
    
    public static List<DicLocation> find(Map<String, Object> parametry) throws EdmException 
    {
        Criteria c = DSApi.context().session().createCriteria(DicLocation.class);
        if(c == null) 
            throw new EdmException("Nie mozna okreslic kryterium wyszukiwania.");
        if(parametry != null) 
        {
        	if(parametry.get("name") != null) 
                c.add(Expression.like("name",  "%"+parametry.get("name")+"%"));
        	if(parametry.get("street") != null) 
                c.add(Expression.like("street",  "%"+parametry.get("street")+"%"));
            if(parametry.get("zip") != null) 
                c.add(Expression.like("zip",  "%"+parametry.get("zip")+"%"));
            if(parametry.get("city") != null) 
                c.add(Expression.like("city", "%" + parametry.get("city") + "%"));
            c.add(Expression.eq("official", 1));
        }
        try 
        {
            log.info("pobranie listy (DicLocation)");
            return (List<DicLocation>)c.list();
        } 
        catch (HibernateException e) 
        {
            throw new EdmHibernateException(e);
        }
    }
    
    public static SearchResults<? extends DicLocation> search(QueryForm form) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(DicLocation.class);

            if(form.hasProperty("nrEwidencyjny")) 
                c.add(Expression.like("nrEwidencyjny",  "%"+form.getProperty("nrEwidencyjny")+"%"));
        	if(form.hasProperty("street")) 
                c.add(Expression.like("street",  "%"+form.getProperty("street")+"%"));
            if (form.hasProperty("zip"))
                c.add(Expression.like("zip", "%"+form.getProperty("zip")+"%"));
            if (form.hasProperty("city"))
                c.add(Expression.like("city", "%"+form.getProperty("city")+"%"));            
            
            for (Iterator iter=form.getSortFields().iterator(); iter.hasNext(); )
            {
                QueryForm.SortField field = (QueryForm.SortField) iter.next();
                if (field.isAscending())
                    c.addOrder(Order.asc(field.getName()));
                else
                    c.addOrder(Order.desc(field.getName()));
            }

            List<DicLocation> list = (List<DicLocation>) c.list();

            if (list.size() == 0)
                return SearchResultsAdapter.emptyResults(DicLocation.class);
            else
            {
                int fromIndex = form.getOffset() < list.size() ? form.getOffset() : list.size()-1;
                int toIndex = (form.getOffset()+form.getLimit() <= list.size()) ? form.getOffset()+form.getLimit() : list.size();
                 
                return new SearchResultsAdapter<DicLocation>(list.subList(fromIndex, toIndex), form.getOffset(), list.size(), DicLocation.class);
            }
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}

	public Boolean getOfficial() {
		return official;
	}

	public void setOfficial(Boolean official) {
		this.official = official;
	}
	@Override
	public Map<String, String> popUpDictionaryAttributes() {
		// TODO Auto-generated method stub
		return null;
	}

	

	
}
