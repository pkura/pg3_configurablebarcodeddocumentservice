package pl.compan.docusafe.parametrization.prosika;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;


/**
 * Bean zawierajacy regule decyzyjna dla dokumentu prosika
 * @author wkutyla
 *
 */

public class ProsikaDecisionRule 
{
	/**
	 * Identyfikator reguly
	 */
	private Long id;
	/**
	 * Identyfikator macierzy
	 */
	private String matrixGuid;
	
	/**
	 * Priorytet reguly
	 */
	private Integer priority;
	/**
	 * Segment CN
	 */
	private String segment;
	/**
	 * TYP CN
	 */
	private String typ;
	/**
	 * Podtyp CN
	 */
	private String podTyp;
	/**
	 * Kanal wplywu (sposob dostarczenia
	 */
	private String kanalWplywu;
	/**
	 * Kod PNA od
	 */
	private Integer pnaGroupFrom;
	/**
	 * Kod PN to
	 */
	private Integer pnaGroupTo;
	
	/**
	 * Docelowy GUID
	 */
	private String targetGuid;
	/**
	 * Docelowy user
	 */
	private String targetUser;
	/**
	 * Flaga auto assignu
	 */
	private Boolean autoAssign;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMatrixGuid() {
		return matrixGuid;
	}
	public void setMatrixGuid(String matrixGuid) {
		this.matrixGuid = matrixGuid;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public String getSegment() {
		return segment;
	}
	public void setSegment(String segment) {
		this.segment = segment;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public String getPodTyp() {
		return podTyp;
	}
	public void setPodTyp(String podTyp) {
		this.podTyp = podTyp;
	}
	public String getKanalWplywu() {
		return kanalWplywu;
	}
	public void setKanalWplywu(String kanalWplywu) {
		this.kanalWplywu = kanalWplywu;
	}
	public Integer getPnaGroupFrom() {
		return pnaGroupFrom;
	}
	public void setPnaGroupFrom(Integer pnaGroupFrom) {
		this.pnaGroupFrom = pnaGroupFrom;
	}
	public Integer getPnaGroupTo() {
		return pnaGroupTo;
	}
	public void setPnaGroupTo(Integer pnaGroupTo) {
		this.pnaGroupTo = pnaGroupTo;
	}
	public String getTargetGuid() {
		return targetGuid;
	}
	public void setTargetGuid(String targetGuid) {
		this.targetGuid = targetGuid;
	}
	public String getTargetUser() {
		return targetUser;
	}
	public void setTargetUser(String targetUser) {
		this.targetUser = targetUser;
	}
	public Boolean getAutoAssign() {
		return autoAssign;
	}
	public void setAutoAssign(Boolean autoAssign) {
		this.autoAssign = autoAssign;
	}
	
	public static ProsikaDecisionRule find(Long id) throws EdmException
	{
		return DSApi.getObject(ProsikaDecisionRule.class, id);
	}
	
	public void create() throws EdmException
	{
		try
		{
			DSApi.context().session().save(this);
			DSApi.context().session().flush();
		}
		catch (HibernateException e)
		{
			throw new EdmException("B��d dodania regu�y",e);
		}
	}
	
	public void delete() throws EdmException
	{
		try
		{
			DSApi.context().session().delete(this);
			DSApi.context().session().flush();
		}
		catch (HibernateException e)
		{
			throw new EdmException("B��d usuni�cia regu�y",e);
		}

	}	
}
