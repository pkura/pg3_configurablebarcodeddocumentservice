package pl.compan.docusafe.parametrization.prosika;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;

/**
 * Klasa odpowiedzialna za procesowanie macierzy decyzyjnej 
 * @author wkutyla
 *
 */
public class DecisionMatrixProcessor 
{
	
	static final Logger log = LoggerFactory.getLogger(DecisionMatrixProcessor.class);
	
	/**
	 * Maksymalna liczba poziomow przeszukiwania
	 */
	static final int MAX_FIND_DEPTH = 7;
		
	/**
	 * Nazwa Streamu dla ktorego rozpatrujemy dane
	 */
	private String streamCode = "BM";
	
	/**
	 * Domyslny konstruktor
	 */
	public DecisionMatrixProcessor()
	{
		super();
	}
	
	/**
	 * Konstruktor z mozliwoscia wskazania kodu streamu. 
	 * @param streamCode
	 */
	public DecisionMatrixProcessor(String streamCode)
	{
		super();
		this.streamCode = streamCode;
	}
	/**
	 * Metoda wyznacza koordynatora procesu na podstawie macierzy decyzyjnej 
	 * @param document
	 * @return
	 * @throws EdmException
	 */
	public ProcessCoordinator findCoordinator(OfficeDocument document) throws EdmException
	{		
		if (document==null)
			return null;
		log.trace("wyznaczam koordynatora dla dokumentu {}",document.getId());
		try
		{
			ProcessCoordinator resp = null;
			int level = 0;
			DecisionMatrix decisionMatrix = findMasterMatrix(document);			
			while (level<MAX_FIND_DEPTH)
			{
				level++;
				if (level==MAX_FIND_DEPTH)
				{
					log.warn("przerwano zapetlenie regul dla dokumentu {}",document.getId());
				}
				log.trace("poszukujemy reguly w macierzy {}",decisionMatrix.getMatrixGuid());
				MatrixResultBean result = decisionMatrix.findCoordinator(document);
				if (result == null)
				{
					log.trace("nie znaleziono reguly dekretuje na dzial");
					resp = new ProcessCoordinator();
					resp.setGuid(decisionMatrix.getMatrixGuid());
					resp.setUsername(null);										
					break;
				} // jesli nie ma uzytkownika, jestli istnieje macierz i nie ma funkcji autodekretacji
				else if (result.getTargetUser() == null && matrixExists(result.getTargetGuid()) && (result.getAutoAssing() == null || !result.getAutoAssing().booleanValue()))
				{
					decisionMatrix = new DecisionMatrix(result.getTargetGuid());
					log.trace("wynikiem jest kolejna macierz o indeksie {}",decisionMatrix.getMatrixGuid());
				}
				else
				{
					log.trace("Znaleziono regule koncowa id={}",result.getRuleId());	
					String targetGuid = result.getTargetGuid();
					resp = new ProcessCoordinator();
					resp.setGuid(targetGuid);
					log.trace("dekretacja na dzial {}", targetGuid);
					if (result.getTargetUser()!=null && userExists(result.getTargetUser(), targetGuid))
					{
						resp.setUsername(result.getTargetUser());
					}
					else
					{
						resp.setUsername(null);
						log.warn("uzytkownik nie znaleziony - dekretujemy na dzial");
					}
					if (result.getAutoAssing() != null && result.getAutoAssing().booleanValue())
					{
						resp.setUsername(autoAssign(targetGuid));
					}
					log.trace("dekretacja na osobe {}", resp.getUsername());
					break;
				}
			}
			if (resp != null)
			{
				resp.setId(0l);
				resp.setChannelCn("TP_MATRIX");
				resp.setDocumentKindCn("prosika");
			}
			return resp;
		}
		catch (Exception e)
		{
			throw new EdmException("Blad w trakcie wyznaczania koordynatora ",e); 
		}
	}
	
	/**
	 * Metoda wyszukuje glowna macierz decyzyjna dla dokumentu
	 * @param document
	 * @return
	 * @throws EdmException
	 */
	protected DecisionMatrix findMasterMatrix(OfficeDocument document) throws EdmException
	{
		//jest to troche krzywe, ale te guidy raczej nie beda sie zmieniac
		if ("KK".equalsIgnoreCase(streamCode))
			return new DecisionMatrix("09C7DC1A00B9FCB011F080ADB348DB11732");
		
		return new DecisionMatrix("09C7DC1A014022AA11F1DB9E9E99536B890");
	}
	
	/**
	 * Metoda sprawdza czy istnieje w systemie macierz o takim GUIDzie
	 * @param guid
	 * @return
	 * @throws Exception
	 */
	protected boolean matrixExists(String guid) throws Exception
	{
		PreparedStatement ps = null;
		try
		{
			ps = DSApi.context().prepareStatement("select id from dsg_prosika_decision_matrix where matrix_guid = ?");
			ps.setString(1, guid);
			ResultSet rs = ps.executeQuery();
			boolean resp = false;
			if (rs.next())
			{
				resp = true;
			}
			rs.close();
			return resp;
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
	}
	
	/**
	 * Zwraca liste macierzy do ktorych uzytkownik ma prawo do edycji
	 * Admin moze edytowac wszystkie macierze, zwykly uzytkownik tylko te w ktorych sie znajduje
	 * @param user
	 * @return
	 * @throws EdmException
	 */
	public Collection<DecisionMatrix> getMatrixList(DSUser user) throws EdmException
	{
		if (user.isAdmin())
			return getMatrixList();
		Collection<DecisionMatrix> resp = new Vector<DecisionMatrix>();
		for (DSDivision div : user.getDivisionsWithoutGroup())
		{
			if (div.isDivision() || div.isPosition())
			{
				DecisionMatrix matrix = new DecisionMatrix(div.getGuid());
				resp.add(matrix);
			}
		}
		return resp;
	}
	/**
	 * Funkcja zwraca liste wszystkich macierzy decyzyjnych 
	 * @return
	 * @throws EdmException
	 */
	public Collection<DecisionMatrix> getMatrixList() throws EdmException
	{
		PreparedStatement ps = null;
		try
		{
			Collection<DecisionMatrix> resp = new Vector<DecisionMatrix>();
			ps = DSApi.context().prepareStatement("select distinct (matrix_guid) as guid from dsg_prosika_decision_matrix");
			ResultSet rs = ps.executeQuery();
			while (rs.next())
			{
				DecisionMatrix matrix = new DecisionMatrix(rs.getString("guid"));
				resp.add(matrix);
			}
			rs.close();
			return resp;
		}
		catch (Exception e)
		{
			throw new EdmException("Blad w czasie wyciagania listy macierzy", e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
	}
	/**
	 * Funkcja dekretujaca zadania na najmniej obciazonego uzytkownika w dziale
	 * @param guid
	 * @return
	 * @throws Exception
	 */
	protected String autoAssign(String guid) throws Exception
	{
		String winner = "admin";
		int record = 9999;
		DSDivision div = DSDivision.find(guid);
		for (DSUser user : div.getUsers(false)) 
		{
			String uname = user.getName();			
			int ile = countTask(uname,guid);
			if (ile<record)
			{
				record = ile;
				winner = uname;
			}
		}
		return winner;
	}
	
	/**
	 * Metoda weryfikuje czy uzytkownik istnieje w dziale guid
	 * Matrix nie jest synchronizowany z lista uzytkownikow
	 * @param userName
	 * @param guid
	 * @return
	 * @throws Exception
	 */
	protected boolean userExists(String userName, String guid) throws Exception
	{
		DSDivision div = DSDivision.find(guid);
		for (DSUser user : div.getUsers(false)) 
		{
			String uname = user.getName();
			if (uname.equals(userName))
			{
				return true;
			}
		}
		log.warn("Uzytkownik {} nie jest przypiety do dzialu {}",userName,guid);
		return false;
	}
	/**
	 * Zlicza zadania uzytkownika w danej guidze
	 * Dziala tylko dla JBPMa
	 * @param username
	 * @param guid - jesli null 
	 * @return
	 * @throws Excpetion
	 */
	protected int countTask(String username, String guid) throws Exception
	{		
		int resp = 0;
		PreparedStatement ps = null; 
		try
		{
			if (guid!=null)
			{
				ps = DSApi.context().prepareStatement("select count (*) as ile from dsw_jbpm_tasklist where assigned_resource = ? and division_guid = ?");
				ps.setString(1, username);
				ps.setString(2, guid);
			}
			else
			{
				ps = DSApi.context().prepareStatement("select count (*) as ile from dsw_jbpm_tasklist where assigned_resource = ?");
				ps.setString(1, username);
			}
			ResultSet rs = ps.executeQuery();
			rs.next();
			resp = rs.getInt("ile");
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		if (log.isTraceEnabled())
		{
			log.trace("" + resp + " zadan dla uzytkownika {}, guid={}",username,guid);
		}
		return resp;
	}
 

	public String getStreamCode() {
		return streamCode;
	}

	public void setStreamCode(String streamCode) {
		this.streamCode = streamCode;
	}

}
