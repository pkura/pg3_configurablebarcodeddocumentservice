package pl.compan.docusafe.parametrization.prosika;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

/**
 * @TODO - opis WTF is
 * @author wkutyla
 *
 */
public class ProsikaArchiveDocument
{
	
	static final Logger log = LoggerFactory.getLogger(ProsikaArchiveDocument.class);
	private Long id;
	/**
	 * Departament
	 */
	private String department;
		/**
	 * Wydzial
	 */
	private String wydzial;
	/**
	 * Numer Oracla
	 */
	private String oracleNo;
	/**
	 * Numer FKX (Region, kod kreskowy
	 */
	private String fkxNo;
	/**
	 * Nazwa dokumentu
	 */
	private String documentName;
	/**
	 * Znak akt
	 */	
	private String znakAkt;
	/**
	 * Tytul
	 */
	private String title;
	
	/**
	 * Rok
	 */
	private String rok;
	/**
	 * Numer segregatora, archiboxu
	 */
	private String numerSegregatora;
	
	/**
	 * Numer pudla firmy archiwistycznej
	 */
	private String boxNumber;
	
	/**
	 * Nazwa dokumentu
	 */
	private String name;
	
	/**
	 * Opis
	 */
	private String description;
	/** Departamenty od misiaczkow */
	public static String[] DEP = {"DEP. EWIDENCJI ZOBOWIAZAN WYDZIAL KANCELARIA",
		"DEP. KSIEGOWOSCI OGOLNEJ WYDZIAL GOSP MAGAZYNOWEJ"};
	
	public ProsikaArchiveDocument(){}
	
	
	
    public static ProsikaArchiveDocument find(Long id) throws EdmException
    {
       return DSApi.getObject(ProsikaArchiveDocument.class, id);
    }

    
    public static ProsikaArchiveDocument findByOrderId(Long orderId) throws Exception
    {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	ProsikaArchiveDocument result = null;
    	try
    	{
    		 ps = DSApi.context().prepareStatement("select archive_document_id from dsg_arch_doc_to_order where order_id = ? ");
    		 ps.setLong(1, orderId);
    		 rs = ps.executeQuery();    		 
    		 if(rs.next())
    		 {
    			 result = ProsikaArchiveDocument.find(rs.getLong(1));
    		 }    
    		 rs.close();
    	}
    	catch (Exception e) 
    	{
    		log.error("",e);
			throw new EdmException(e);
		}
    	finally
    	{    	
    		DSApi.context().closeStatement(ps);
    	}
    	return result;
    }

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getWydzial() {
		return wydzial;
	}

	public void setWydzial(String wydzial) {
		this.wydzial = wydzial;
	}

	public String getOracleNo() {
		return oracleNo;
	}

	public void setOracleNo(String oracleNo) {
		this.oracleNo = oracleNo;
	}

	public String getFkxNo() {
		return fkxNo;
	}

	public void setFkxNo(String fkxNo) {
		this.fkxNo = fkxNo;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getZnakAkt() {
		return znakAkt;
	}

	public void setZnakAkt(String znakAkt) {
		this.znakAkt = znakAkt;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRok() {
		return rok;
	}

	public void setRok(String rok) {
		this.rok = rok;
	}

	public String getNumerSegregatora() {
		return numerSegregatora;
	}

	public void setNumerSegregatora(String numerSegregatora) {
		this.numerSegregatora = numerSegregatora;
	}

	public String getBoxNumber() {
		return boxNumber;
	}

	public void setBoxNumber(String boxNumber) {
		this.boxNumber = boxNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
