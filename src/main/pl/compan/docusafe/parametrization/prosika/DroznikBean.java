package pl.compan.docusafe.parametrization.prosika;

import java.util.Date;

public class DroznikBean
{

	private Long id;
	
	private String documentID;

	private String contractNumber;

	private String companyName;

	private String clientId;

	private String registryId;

	private String service;

	private Date tpContractDate;

	private Date clientContractDate;

	private Date operateDate;

	private Date sentToClientDate;

	private Date sentToProsikaDate;

	private Integer contractType;

	private String boxNumber;

	
	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getDocumentID()
	{
		return documentID;
	}

	public void setDocumentID(String documentID)
	{
		this.documentID = documentID;
	}

	public String getContractNumber()
	{
		return contractNumber;
	}

	public void setContractNumber(String contractNumber)
	{
		this.contractNumber = contractNumber;
	}

	public String getCompanyName()
	{
		return companyName;
	}

	public void setCompanyName(String companyName)
	{
		this.companyName = companyName;
	}

	public String getClientId()
	{
		return clientId;
	}

	public void setClientId(String clientId)
	{
		this.clientId = clientId;
	}

	public String getRegistryId()
	{
		return registryId;
	}

	public void setRegistryId(String registryId)
	{
		this.registryId = registryId;
	}

	public String getService()
	{
		return service;
	}

	public void setService(String service)
	{
		this.service = service;
	}

	public Date getTpContractDate()
	{
		return tpContractDate;
	}

	public void setTpContractDate(Date tpContractDate)
	{
		this.tpContractDate = tpContractDate;
	}

	public Date getClientContractDate()
	{
		return clientContractDate;
	}

	public void setClientContractDate(Date clientContractDate)
	{
		this.clientContractDate = clientContractDate;
	}

	public Date getOperateDate()
	{
		return operateDate;
	}

	public void setOperateDate(Date operateDate)
	{
		this.operateDate = operateDate;
	}

	public Date getSentToClientDate()
	{
		return sentToClientDate;
	}

	public void setSentToClientDate(Date sentToClientDate)
	{
		this.sentToClientDate = sentToClientDate;
	}

	public Date getSentToProsikaDate()
	{
		return sentToProsikaDate;
	}

	public void setSentToProsikaDate(Date sentToProsikaDate)
	{
		this.sentToProsikaDate = sentToProsikaDate;
	}

	public Integer getContractType()
	{
		return contractType;
	}

	public void setContractType(Integer contractType)
	{
		this.contractType = contractType;
	}

	public String getBoxNumber()
	{
		return boxNumber;
	}

	public void setBoxNumber(String boxNumber)
	{
		this.boxNumber = boxNumber;
	}

	
	
}
