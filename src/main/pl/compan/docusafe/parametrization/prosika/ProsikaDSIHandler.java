package pl.compan.docusafe.parametrization.prosika;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.service.imports.dsi.DSIBean;
import pl.compan.docusafe.service.imports.dsi.DSIImportHandler;
import pl.compan.docusafe.service.imports.dsi.ImportHelper;
import pl.compan.docusafe.service.imports.dsi.ImportValidationException;

public class ProsikaDSIHandler extends DSIImportHandler 
{
	private final static String NR_EWIDENCYJNY = "NUMER_EWIDENCYJNY";
	private final static String NIP = "NIP";
	private final static String NAZWA  = "KLIENT";
	private final static String REGON  = "REGON";
	private final static String ADRES_SIEDZIBY_ULICA = "ADRES_SIEDZIBY_ULICA";
	private final static String ADRES_SIEDZIBY_MIASTO = "ADRES_SIEDZIBY_MIASTO";
	private final static String ADRES_SIEDZIBY_KOD_POCZTOWY = "ADRES_SIEDZIBY_KOD_POCZTOWY";
	private final static String ADRES_SIEDZIBY_NR_DOMU = "ADRES_SIEDZIBY_NR_DOMU";
	private final static String ADRES_SIEDZIBY_NR_LOKALU = "ADRES_SIEDZIBY_NR_LOKALU";
	//private final static String ADRES_SIEDZIBY_KRAJ = "ADRES_SIEDZIBY_KRAJ";
	
	@Override
	protected boolean isStartProces()
	{
		return true;
	}
	public String getDockindCn()
	{
		return "prosika";
	}
	protected void prepareImport() throws Exception 
	{
		String barcode = (String) values.get(Prosikalogic.BARCODE_FIELD_CN);
		if(barcode == null || values.get(Prosikalogic.TAJNY_FIELD_CN)== null || values.get(Prosikalogic.SEGMENT_FIELD_CN) == null 
				|| values.get(Prosikalogic.TYP_DOKUMENTU_FIELD_CN) == null)
		{
			throw new ImportValidationException("Brak atrybutu");
		}
		ImportHelper.setDefaultValue(values, Prosikalogic.WIELOWATKOWY_CN, "NIE");
		// kolejna funkcja im-friendly - oni zawsze przekazuja cn dokumentu nawet jak nie jest wielowatkowy
		ImportHelper.removeEmptyValue(values,"PODTYP_DOKUMENTU");
		ImportHelper.removeEmptyValue(values,"TYP_DOKUMENTU_WIELOWATKOWY");
		ImportHelper.removeEmptyValue(values,"PODTYP_DOKUMENTU_WIELOWATKOWY");
		
		
		
		ImportHelper.correctDateFormat(values, "DATA_DO_TP");
		ImportHelper.correctDateFormat(values, "DATA_DO_REGIONU");
		ImportHelper.correctDateFormat(values, "DATA_PRZYJECIA_IM");
		ImportHelper.correctDateFormat(values, "DATA_PRZYJECIA_TP");
		ImportHelper.correctDateFormat(values, "DATA_ZAWARCIA_KLIENT");
		ImportHelper.correctDateFormat(values, "DATA_ZAWARCIA_TP");
		ImportHelper.correctDateFormat(values, "DATA_OBOWIAZYWANIA");
		ImportHelper.correctDateFormat(values, "DATA_WYSLANIA_UMOWY_DO_KLIENTA");
		ImportHelper.correctDateFormat(values, "DATA_WYSLANIA_UMOWY_DO_PROSIKA");
		ImportHelper.correctDateFormat(values, "DATA_ZARCHIWIZOWANIA");
		
		ImportHelper.setDefaultValue(values, Prosikalogic.DATA_PRZYJECIA_IM_FIELD_CN, new Date());				
		ImportHelper.setDefaultValue(values, Prosikalogic.STATUS_FIELD_CN, "NOWY_DOKUMENT");
		ImportHelper.setDefaultValue(values, "SPOSOB_PRZYJECIA", "POCZTA");
		
		createDic(values);		
		ImportHelper.changeCnToID(values, getDockindCn());
		ImportHelper.removeInvalidFields(values, getDockindCn());
		importDocument = getDocumentByBarcode(barcode);
	}
	
	@Override
	public void actionAfterCreate(Long documentId, DSIBean dsiB) throws Exception
	{
		dsiB.setDocumentURL("link do DOC");
	}
   
   private InOfficeDocument getDocumentByBarcode(String barcode) throws Exception
   {
	   PreparedStatement ps = null;
	   InOfficeDocument document;
	   try
	   {
		   Connection con = DSApi.context().session().connection();
		   ps = con.prepareStatement("select DOCUMENT_ID from "+documentKind.getTablename()+" where "
				   +documentKind.getFieldByCn(Prosikalogic.BARCODE_FIELD_CN).getColumn()+" = ? ");
		   ps.setString(1, barcode);
		   ResultSet rs = ps.executeQuery();
		   if(rs.next())
		   {
			   Long documentId = rs.getLong(1);
			   document = InOfficeDocument.findInOfficeDocument(documentId);			   
		   }
		   else
		   {
			   throw new ImportValidationException("Nie znalaz� dockument o barcode = "+barcode);
		   }
	   }
	   finally
	   {
		   try { if (ps!= null) { ps.close(); } } catch (Exception e) { }
	   }
	   return document;
   }
   
   private void createDic(Map<String,Object> values) throws Exception
   {
	   if(values.get(NR_EWIDENCYJNY) != null && values.get(NR_EWIDENCYJNY).toString().length()>1)
		{
			Map<String,Object> parametry = new TreeMap<String, Object>();
			parametry.put("nrEwidencyjny",values.get(NR_EWIDENCYJNY));
			List<DicProSIKA> list = DicProSIKA.find(parametry);
			getDic(list,values);			
		}
		else if(values.get(NIP)!= null && values.get(NIP).toString().length()>1 && values.get(NAZWA) != null)
		{
			Map<String,Object> parametry = new TreeMap<String, Object>();
			parametry.put("nazwaKlienta",values.get(NAZWA));
			parametry.put("nip",values.get(NIP));
			List<DicProSIKA> list = DicProSIKA.find(parametry);
			getDic(list,values);
		}
		else if(values.get(NIP)!= null && values.get(NIP).toString().length()>1 && 
				values.get(ADRES_SIEDZIBY_ULICA)!= null && 
				values.get(ADRES_SIEDZIBY_MIASTO) != null && 
				values.get(ADRES_SIEDZIBY_KOD_POCZTOWY) != null) // && values.get(ADRES_SIEDZIBY_KRAJ) != null -- nie ma w dicu
		{
			Map<String,Object> parametry = new TreeMap<String, Object>();
			parametry.put("nip",values.get(NIP));
			parametry.put("as_ulica",values.get(ADRES_SIEDZIBY_ULICA));
			parametry.put("as_miejscowosc",values.get(ADRES_SIEDZIBY_MIASTO));
			parametry.put("as_kod",values.get(ADRES_SIEDZIBY_KOD_POCZTOWY));
			
			List<DicProSIKA> list = DicProSIKA.find(parametry);
			getDic(list,values);
		}
		else
		{
			throw new ImportValidationException("Brak atrybut�w klienta");
		}
   }
   
   private void getDic(List<DicProSIKA> list,Map<String,Object> values) throws Exception
   {
	   	if(list == null || list.size() < 1)
		{
		   	DicProSIKA dic = new DicProSIKA();
		   	dic.setNrEwidencyjny((String) values.get(NR_EWIDENCYJNY));
		   	dic.setNip((String)values.get(NIP));
		   	dic.setNazwaKlienta((String)values.get(NAZWA));
		   	dic.setAs_kod((String)values.get(ADRES_SIEDZIBY_KOD_POCZTOWY));
		   	dic.setAs_miejscowosc((String)values.get(ADRES_SIEDZIBY_MIASTO));
		   	dic.setAs_ulica((String)values.get(ADRES_SIEDZIBY_ULICA));
		   	dic.setAs_nrdomu((String)values.get(ADRES_SIEDZIBY_NR_DOMU));
		   	dic.setAs_nrlokalu((String)values.get(ADRES_SIEDZIBY_NR_LOKALU));
		   	dic.setRegon((String)values.get(REGON));
		   	dic.create();
			values.put(Prosikalogic.KLIENT_FIELD_CN, dic.getId());
		}
		else if(list.size() == 1)
		{
			DicProSIKA dic = list.get(0);
			dic.setNrEwidencyjny((String) values.get(NR_EWIDENCYJNY));
		   	dic.setNip((String)values.get(NIP));
		   	dic.setNazwaKlienta((String)values.get(NAZWA));
		   	dic.setAs_kod((String)values.get(ADRES_SIEDZIBY_KOD_POCZTOWY));
		   	dic.setAs_miejscowosc((String)values.get(ADRES_SIEDZIBY_MIASTO));
		   	dic.setAs_ulica((String)values.get(ADRES_SIEDZIBY_ULICA));
		   	dic.setAs_nrdomu((String)values.get(ADRES_SIEDZIBY_NR_DOMU));
		   	dic.setAs_nrlokalu((String)values.get(ADRES_SIEDZIBY_NR_LOKALU));
		   	dic.setRegon((String)values.get(REGON));
		   	dic.create();
			values.put(Prosikalogic.KLIENT_FIELD_CN, dic.getId());
		}
		else if(list.size() > 1)
		{
			throw new ImportValidationException("Istnieje wi�cej ni� jeden klienta o podanych parametrach");
		}
   }
}
