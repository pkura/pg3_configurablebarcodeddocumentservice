package pl.compan.docusafe.parametrization.dpd;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.util.FolderInserter;

import java.util.HashSet;
import java.util.Set;

public class DPDLogic extends AbstractDocumentLogic {

	
	private static final DPDLogic instance = new DPDLogic();
	
	public static DPDLogic getInstance() {
		return instance;
	}

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(fm.getValue("DATA")));
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(fm.getValue("DATA")));
		folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getValue("KOD_WYDRUKU")));
		document.setFolder(folder);
		
		document.setTitle(String.valueOf(fm.getValue(fm.getMainFieldCn())));
		document.setDescription(String.valueOf(fm.getValue(fm.getMainFieldCn())));
	}
	
	public void documentPermissions(Document document) throws EdmException {
        Set<PermissionBean> perms = new HashSet<PermissionBean>();
        perms.add(new PermissionBean(ObjectPermission.READ,"ADMIN",ObjectPermission.GROUP,"ADMIN"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"ADMIN",ObjectPermission.GROUP,"ADMIN"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY,"ADMIN",ObjectPermission.GROUP,"ADMIN"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"ADMIN",ObjectPermission.GROUP,"ADMIN"));
        setUpPermission(document, perms);
	}
}
