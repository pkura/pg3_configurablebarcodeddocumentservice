package pl.compan.docusafe.parametrization.zbp;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 23.06.14.
 */
public class CzasopismoListener implements ExternalActivityBehaviour {
    private static final Logger LOG = LoggerFactory.getLogger(CzasopismoListener.class);

    @Override
    public void execute(ActivityExecution activityExecution) throws Exception {
        Long docId = Long.valueOf(activityExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);
        FieldsManager fm = doc.getFieldsManager();
        List<Integer> usersIds = (List<Integer>) fm.getFieldValues().get("ATTACHMENTS_USERS");
        List<String> users = new ArrayList<String>();
        for(Integer user : usersIds){
            users.add(DSUser.findById(user.longValue()).getName());
        }
        activityExecution.setVariable("users", users);
        activityExecution.setVariable("count", users.size());

    }

    @Override
    public void signal(ActivityExecution activityExecution, String s, Map<String, ?> stringMap) throws Exception {

    }
}
