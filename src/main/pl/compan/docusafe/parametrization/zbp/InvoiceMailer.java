package pl.compan.docusafe.parametrization.zbp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.jbpm.api.task.Participation;
import org.jbpm.api.task.Task;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.joda.time.Days;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.zbp.hb.TerminPlatnosciMapping;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import edu.emory.mathcs.backport.java.util.Arrays;

public class InvoiceMailer extends ServiceDriver implements Service {

    public static final Logger log = LoggerFactory.getLogger(InvoiceMailer.class);
    private final static StringManager sm = GlobalPreferences.loadPropertiesFile(InvoiceMailer.class.getPackage().getName(), null);
    private Timer timer;
    // private String url = Docusafe.getAdditionProperty("database.url");
    // private String username = Docusafe.getAdditionProperty("database.user");
    // private String password =
    // Docusafe.getAdditionProperty("database.password");

    private final static String CHIEF_ACCOUNTANT_GUID = Docusafe.getAdditionProperty("chief.accountant.guid");

    private final static long FREQUENCY_ONE_DAY = 1000 * 60 * 60 * 24;
    private final static int ONE_DAY = 1;
    private final static int HOUR_AM = 9;
    private final static int HOUR_PM = 13;
    private final static int MINUTES = 0;

    class SendMail extends TimerTask {

	@Override
	public void run() {
	    try {
		DSApi.openAdmin();
		for (Long docId : getDocumentIdsToSend()) {
		    try {
			DSApi.context().begin();
			if (shoulSend(docId)) {
			    sendMail(docId);
			}
			DSApi.context().commit();
		    } catch (Throwable e) {
			log.error("", e);
			console(Console.ERROR, "B��d [ " + e + " ]");
			DSApi.context()._rollback();
		    }
		}
	    } catch (EdmException e) {
		log.error("", e);
		console(Console.ERROR, "B��d [ " + e + " ]");
	    } catch (Throwable e) {
		log.error("", e);
		console(Console.ERROR, "B��d [ " + e + " ]");
	    } finally {
		DSApi._close();
	    }
	}

	/**
	 * Sprawdza, czy s� dokumenty do wys�ania z terminem p�atno�ci w
	 * przysz�o�ci. Zwraca identyfikatory tych dokument�w, kt�re termin
	 * p�atno�ci maj� za dwa dni lub mniej. W przypadku czwartku i piatku
	 * pomijamy sobot� i niedziel�.
	 * 
	 * @return identyfikatory dokument�w, kt�re termin p�atno�ci maj� za dwa
	 *         dni pomijaj�c soboty i niedziele
	 */
	private List<Long> getDocumentIdsToSend() {
	    List<Long> ret = new ArrayList<Long>();
	    try {
		for (TerminPlatnosciMapping tr : TerminPlatnosciMapping.list()) {
		    Calendar termin = new GregorianCalendar();
		    int today = termin.get(Calendar.DAY_OF_WEEK);
		    termin.setTime(tr.getTerminPlatnosci());
		    int days = Days.daysBetween(new DateMidnight(new GregorianCalendar()), new DateMidnight(termin)).getDays();

		    switch (today) {
		    case Calendar.THURSDAY:
			if (days == 4) {
			    ret.add(tr.getDocumentId());
			}
			break;
		    case Calendar.FRIDAY:
			if (days == 4) {
			    ret.add(tr.getDocumentId());
			}
			break;
		    default:
			if (days <= 2) {
			    ret.add(tr.getDocumentId());
			}
		    }

		}
	    } catch (EdmException e) {
		log.error(e.getMessage(), e);
		console(Console.ERROR, "B��d [ " + e + " ]");
	    }
	    return ret;
	}

	/**
	 * Wysy�a mail'a z powiadomieniem o zbli�aj�cym si� terminie p��tno�ci
	 * dla ka�dego dokumentu o ID przekazanym w parametrze. Po wysy�ce
	 * usuwamy dokument z listy dokument�w do wys�ania z bazy danych
	 * 
	 * @param docId
	 */
	private void sendMail(Long docId) {
	    try {
		DSDivision chiefAccountantDivision = DSDivision.find(CHIEF_ACCOUNTANT_GUID);
		@SuppressWarnings("unchecked")
		List<DSUser> chiefAccountants = Arrays.asList(chiefAccountantDivision.getUsers());
		TerminPlatnosciMapping trm = TerminPlatnosciMapping.findByDocumentId(docId);
		for (DSUser user : chiefAccountants) {
		    Map<String, Object> ctext = prepareContext(user, docId, trm);
		    try {
			((Mailer) ServiceManager.getService(Mailer.NAME)).send(user.getEmail(), user.getEmail(), null,
				Configuration.getMail(Mail.INVOICE_MAILER), ctext);
			console(Console.INFO, "Wysylam mail do " + user.getName() + " na email " + user.getEmail() + " czas " + new Date());
			trm.delete();
		    } catch (Exception e) {
			log.error("B��d wys�ania maila o terminie p�atno�ci faktury do " + user.getName(), e);
			console(Console.ERROR, "B��d [ " + e + " ]");
		    }

		}
	    } catch (EdmException e) {
		log.error(e.getMessage(), e);
	    }

	}
    }

    /**
     * Sprawdza czy powiadomienie powinno zosta� wys�ane dla dokumentu o podanym
     * w parametrze ID. Wysy�ane jest tylko przed etapem ksi�gowo�ci i tylko w
     * dni powszednie.
     * 
     * @param docId
     * @return 'true' je�li etap procesu faktury jest przed ksi�gowo�ci� oraz
     *         dzie� dzisiejszy nie jest sobot� ani niedziel�
     */
    public boolean shoulSend(Long docId) {
	Calendar cal = new GregorianCalendar();
	int day = cal.get(Calendar.DAY_OF_WEEK);
	if (day == Calendar.SATURDAY || day == Calendar.SUNDAY) {
	    return false;
	}
	OfficeDocument doc;
	try {
	    doc = OfficeDocument.find(docId);
	    FieldsManager fm = doc.getFieldsManager();
	    int status = fm.getEnumItem("STATUS") != null ? fm.getEnumItem("STATUS").getId() : 0;
	    if (status > 0 && status < 120) {
		return true;
	    } else {
		return false;
	    }
	} catch (DocumentNotFoundException e) {
	    log.error("Invoice Mailer: Nie znaleziono dokumentu", e);
	    return false;
	} catch (EdmException e) {
	    log.error(e.getMessage(), e);
	    return false;
	}
    }

    /**
     * Zwraca termin uruchomienia serwisu o danej godzinie na podstawiesta�ej
     * HOUR_AM. Je�li sysytem zosta� uruchomiony po tej godzienie, termin
     * ustawiany jest na nast�pny dzie�.
     * 
     * @return
     */
    private static Date getFirstRunTime() {
	Calendar runTime = new GregorianCalendar();
	if (runTime.get(Calendar.HOUR_OF_DAY) >= HOUR_AM) {
	    runTime.add(Calendar.DATE, ONE_DAY);
	}
	Calendar firstRunTime = new GregorianCalendar(runTime.get(Calendar.YEAR), runTime.get(Calendar.MONTH), runTime.get(Calendar.DATE), HOUR_AM,
		MINUTES);
	log.error("First run time: " + DateUtils.formatCommonDateTime(firstRunTime.getTime()));
	return firstRunTime.getTime();
    }

    /**
     * Zwraca termin uruchomienia serwisu o danej godzinie na podstawiesta�ej
     * HOUR_PM. Je�li sysytem zosta� uruchomiony po tej godzienie, termin
     * ustawiany jest na nast�pny dzie�.
     * 
     * @return
     */
    private Date getSecondRunTime() {
	Calendar runTime = new GregorianCalendar();
	if (runTime.get(Calendar.HOUR_OF_DAY) >= HOUR_PM) {
	    runTime.add(Calendar.DATE, ONE_DAY);
	}
	Calendar secondRunTime = new GregorianCalendar(runTime.get(Calendar.YEAR), runTime.get(Calendar.MONTH), runTime.get(Calendar.DATE), HOUR_PM,
		MINUTES);
	log.error("Second run time: " + DateUtils.formatCommonDateTime(secondRunTime.getTime()));
	return secondRunTime.getTime();
    }

    @Override
    protected void start() throws ServiceException {
	log.info("Start uslugi InvoiceMailer");
	timer = new Timer(true);
	timer.scheduleAtFixedRate(new SendMail(), getFirstRunTime(), FREQUENCY_ONE_DAY);
	timer.scheduleAtFixedRate(new SendMail(), getSecondRunTime(), FREQUENCY_ONE_DAY);
	console(Console.INFO, sm.getString("Us�uga InvoiceMailer wystartowa�a"));
    }

    @Override
    protected void stop() throws ServiceException {
	log.info("stop uslugi");
	if (timer != null)
	    timer.cancel();
    }

    @Override
    protected boolean canStop() {
	return false;
    }

    /**
     * Zwraca map� kluczy, kt�rymi pos�ugujemy si� w szablonie tekstowym tre�ci maila z powiadomieniem.
     * @param user
     * @param docId
     * @param trm
     * @return
     * @throws UserNotFoundException
     * @throws EdmException
     */
    private Map<String, Object> prepareContext(DSUser user, Long docId, TerminPlatnosciMapping trm) throws UserNotFoundException, EdmException {
	Map<String, Object> ctext = new HashMap<String, Object>();

	OfficeDocument doc = OfficeDocument.find(docId);
	FieldsManager fm = doc.getFieldsManager();

	if (user != null) {
	    ctext.put("first-name", user.getFirstname());
	    ctext.put("last-name", user.getLastname());
	}
	ctext.put("document-name", fm.getDocumentKind().getName());
	ctext.put("document-id", fm.getDocumentId());
	ctext.put("document-state", fm.getEnumItem("STATUS") != null ? fm.getEnumItem("STATUS").getTitle() : "");
	ctext.put("link", Docusafe.getBaseUrl());

	if (doc instanceof OfficeDocument) {
	    ctext.put("document-ko", ((OfficeDocument) doc).getOfficeNumber());
	}

	fm.getDocumentKind().logic().addSpecialValueForProcessMailNotifier(fm, ctext);

	for (String taskId : Jbpm4ProcessLocator.taskIds(docId)) {
	    Task task = Jbpm4Provider.getInstance().getTaskService().getTask(taskId);
	    if (task.getAssignee() != null)
		ctext.put("document-assignees", DSUser.findByUsername(task.getAssignee()).getWholeName());
	    else {
		for (Participation part : Jbpm4Provider.getInstance().getTaskService().getTaskParticipations(taskId)) {
		    StringBuilder zadekretowani = new StringBuilder();
		    if (part.getUserId() != null) {
			ctext.put("document-assignees", DSUser.findByUsername(part.getUserId()).getWholeName());
		    } else {
			for (DSUser juser : DSDivision.find(part.getGroupId()).getUsers()) {
			    zadekretowani.append(juser.getWholeName() + ", ");
			}
			ctext.put("document-assignees", zadekretowani);
		    }
		}
	    }
	}
	/**
	 * Dodanie do szablonu wszystkich warto�ci pol na dokumencie
	 */
	for (String cn : fm.getFieldsCns()) {
	    Object value = fm.getValue(cn);
	    if (value instanceof Date) {
		String date = DateUtils.formatCommonDate((Date) value);
		ctext.put(cn, date);
	    } else
		ctext.put(cn, value);
	}

	return ctext;
    }

}
