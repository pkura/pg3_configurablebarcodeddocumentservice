package pl.compan.docusafe.parametrization.zbp;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.util.Collections;
import java.util.Map;

import com.google.common.collect.Maps;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class NormalLogic extends AbstractDocumentLogic {

	private static final long serialVersionUID = 1L;
	private static final String PRZYCHODZACE = "normal";
	private static final String WYCHODZACE = "normal_out";
	private static final String WEWNETRZNE = "normal_int";
//	private static final String KANCELARIA_GUID = Docusafe.getAdditionProperty("kancelaria.guid");
	
	private static NormalLogic instance;
	protected static Logger log = LoggerFactory.getLogger(NormalLogic.class);

	public static NormalLogic getInstance() {
		if (instance == null)
			instance = new NormalLogic();
		return instance;
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event) {
		log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
		try{
			Map<String, Object> map = Maps.newHashMap();
			if(WYCHODZACE.equals(document.getDocumentKind().getCn())){
			map.put(ASSIGN_USER_PARAM, null);
			FieldsManager fm = document.getFieldsManager();
			String userName = fm.getEnumItemCn("KANCELARIA");
			map.put(ASSIGN_USER_PARAM, userName);
			
			OfficeDocument od = document;
			od.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(od, Collections.<String, Object> singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY, od.getAuthor()));
			}
			else{
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
            map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			}
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
            log.error(AvailabilityManager.isAvailable("addToWatch").toString());
            if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public void documentPermissions(Document document) throws EdmException {
	}


	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
		Map<String, Object> toReload = Maps.newHashMap();
		if (WEWNETRZNE.equals(fm.getDocumentKind().getCn()) && DSApi.context().getDSUser().getDivisionsWithoutGroup().length>0) {
			String sender = "u:"+DSApi.context().getDSUser().getName();
			sender+=(";d:"+DSApi.context().getDSUser().getDivisionsWithoutGroup()[0].getGuid());
			toReload.put("SENDER_HERE", sender);	
		}
		fm.reloadValues(toReload);
	}


}
