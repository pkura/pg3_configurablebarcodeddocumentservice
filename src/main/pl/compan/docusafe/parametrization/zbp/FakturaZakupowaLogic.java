package pl.compan.docusafe.parametrization.zbp;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.jbpm.api.TaskService;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.Constants;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.FieldValue;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.polcz.CostAcceptance;
import pl.compan.docusafe.parametrization.zbp.hb.MPK;
import pl.compan.docusafe.parametrization.zbp.hb.TerminPlatnosciMapping;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multiset;

import edu.emory.mathcs.backport.java.util.Collections;

public class FakturaZakupowaLogic extends AbstractDocumentLogic
{
	public static final String STATUS_FIELD_NAME = "STATUS";
    public static final String HIDE_MPK_FIELD = "HIDE_MPK";
    public static final String MPK_CORRECTION_FIELD = "MPK_CORRECTION";
    public static final String TERMIN_PLATNOSCI_FIELD = "TERMIN_PLATNOSCI";
    public static final String DATA_WPLYWU_FAKTURY_FIELD = "DATA_WPLYWU_FAKTURY";
    public static final String DATA_WYSTAWIENIA_FIELD = "DATA_WYSTAWIENIA";
    public static final String WALUTA_FIELD = "WALUTA";
    public static final String SPOSOB_FIELD = "SPOSOB";
    public static final String SENDER_FIELD = "SENDER";
    public static final String TYP_FAKTURY_FIELD = "TYP_FAKTURY";
    public static final String KWOTA_NETTO_FIELD = "KWOTA_NETTO";
    public static final String KWOTA_VAT_FIELD = "KWOTA_VAT";
    public static final String ID_FIELD = "ID";
    public static final String MPK_FIELD = "MPK";
    public static final String ZARZAD_FIELD = "ZARZAD";
    public static final String ZARZAD_ADD_FIELD = "ZARZAD_ADD";
    public static final String ZARZAD_ADD_MEMBER_FIELD_NAME = "ZARZAD_ADD_MEMBER";
    public static final String OKRES_FAKTURY_OD_FIELD = "OKRES_FAKTURY_OD";
    public static final String OKRES_FAKTURY_DO_FIELD = "OKRES_FAKTURY_DO";
    public static final String CENTRUM_ID = "CENTRUMID";
    public static final String BOARD_TASK_NAME = "board";
    public static final String MPK_TASK_NAME = "board-from-mpk";
    public static final String BOARD_MEMBERS_ACCEPTATION_CN = "board-members";
    public static final String CENTRA_KOSZTOW_TABLENAME = "dsg_zbp_centra_kosztow";
    public static final String MPK_VARIABLE_NAME = "costCenterId";
    public static final String DWR_KWOTA_VAT = "DWR_KWOTA_VAT";
    public static final String DWR_KWOTA_BRUTTO = "DWR_KWOTA_BRUTTO";
    public static final String DWR_KWOTA_NETTO = "DWR_KWOTA_NETTO";
    public static final String DWR_GENERATE_MPK = "DWR_GENERATE_MPK";
    public static final String KWOTA_BRUTTO = "KWOTA_BRUTTO";
    public static final String M_DICT_VALUES = "M_DICT_VALUES";
    public static final String AMOUNT = "AMOUNT";
    public static final String MEMBER = "MEMBER";
    public static final String MPK_WAS_CHANGED_JBPM4_VARIABLE = "MPK_WAS_CHANGED";
    public static final String ADDS_MANAGEMENT_JBPM4_VARIABLE = "ADDS_MANAGEMENT";
    public static final String MANAGEMENT_MEMBERS = "MANAGEMENT_MEMBERS";
    public static final String F_PROFORMA = "F_PROFORMA";
    public static final String F_FAKTURA = "F_FAKTURA";
    public static final String KOREKTA = "KOREKTA";
    public static final String SHOW_KOREKTA_INFO = "SHOW_KOREKTA_INFO";
    public static final String MEMBER_OF_MANAGEMENT = "memberOfManagement";
    public static final String CORRECTION_BY_MEMBER = "CORRECTION_BY_MEMBER";
    public static final String GENERATE_DATE = "GENERATE_DATE";
    public static final String ID = "ID";
    public static final String FIELDS = "fields";
    public static final String C_USER_JBPM4_VARIABLE_NAME = "cUser";
    public List<String> setValue = new ArrayList<String>();
    private static final long serialVersionUID = 1L;
    private static final int MONEY_SCALE = 2;
    private final Logger log = LoggerFactory.getLogger(FakturaZakupowaLogic.class);
    private static final StringManager sm = StringManager.getManager(Constants.Package);

    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
    {
        log.info("--- Faktura Zakupowa : START PROCESS !!! ---- {}", event);

        try {
            Map<String, Object> map = Maps.newHashMap();
            map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
            map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
            java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

            DSUser author = DSApi.context().getDSUser();
            String user = author.getName();
            String fullName = author.asFirstnameLastname();

            perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));
    		
            Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
            perms.addAll(documentPermissions);
            this.setUpPermission(document, perms);

            if (AvailabilityManager.isAvailable("addToWatch")) {
                DSApi.context().watch(URN.create(document));
            }

            if (document.getFieldsManager().getKey(TERMIN_PLATNOSCI_FIELD) != null) {
                TerminPlatnosciMapping trm = new TerminPlatnosciMapping(document.getId(), (Date) document.getFieldsManager().getKey(
                        TERMIN_PLATNOSCI_FIELD));
                trm.save();
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new EdmException(e.getMessage());
        }
    }
    
    @Override
    public void setAdditionalTemplateValues(long docId, Map<String, Object> values) {
    	try {
    		Document doc = Document.find(docId);
    		FieldsManager fm = doc.getFieldsManager();
    		
    		// umieszczenie daty generacji dokumentu
    		values.put(ID, docId);
    		
    		// umieszczenie w values podstawowych informacji z dokumentu
    		
    		putDataFromDocumentToValues(fm, values);
    		
    		Person sender = getSenderFromDocument(fm);
    		if (sender != null) {
    			values.put(SENDER_FIELD, sender);
    		}
    		
    		// umieszczenie MPK w values
    		List<MPK> mpkList = getMpk(docId, fm);
    		if (mpkList != null && !mpkList.isEmpty()) {
    			values.put(MPK_FIELD, mpkList);
    		}
    		
    		for (DocumentAcceptance dacc : DocumentAcceptance.find(docId)) {
    			DSUser acceptedUser = DSUser.findByUsername(dacc.getUsername());
				values.put(dacc.getAcceptanceCn(), acceptedUser.getLastname() + " " + acceptedUser.getFirstname());
				values.put(dacc.getAcceptanceCn() + "_DATE", DateUtils.formatCommonDate(dacc.getAcceptanceTime()));
    		}
    	} catch (EdmException e) {
    		log.error(e.getMessage(), e);
    	}
    }

    private void putDataFromDocumentToValues(FieldsManager fm, Map<String, Object> values) throws EdmException {
    	Map<String, Object> fieldValues = (Map<String, Object>) values.get(FIELDS);
    	if (fieldValues.containsKey(DATA_WPLYWU_FAKTURY_FIELD) && fieldValues.get(DATA_WPLYWU_FAKTURY_FIELD) != null) {
    		String date = DateUtils.formatCommonDate((Date) fieldValues.get(DATA_WPLYWU_FAKTURY_FIELD));
    		values.put(DATA_WPLYWU_FAKTURY_FIELD, date);
    	}
    	if (fieldValues.containsKey(KWOTA_BRUTTO) && fieldValues.get(KWOTA_BRUTTO) != null) {
    		BigDecimal grossAmount = (BigDecimal) fieldValues.get(KWOTA_BRUTTO);
    		grossAmount = grossAmount.setScale(MONEY_SCALE);
    		values.put(KWOTA_BRUTTO, grossAmount);
    	}
    	if (fieldValues.containsKey(OKRES_FAKTURY_OD_FIELD) && fieldValues.get(OKRES_FAKTURY_OD_FIELD) != null) {
    		String date = DateUtils.formatCommonDate((Date) fieldValues.get(OKRES_FAKTURY_OD_FIELD));
    		values.put(OKRES_FAKTURY_OD_FIELD, date);
    	}
    	if (fieldValues.containsKey(OKRES_FAKTURY_DO_FIELD) && fieldValues.get(OKRES_FAKTURY_DO_FIELD) != null) {
    		String date = DateUtils.formatCommonDate((Date) fieldValues.get(OKRES_FAKTURY_DO_FIELD));
    		values.put(OKRES_FAKTURY_DO_FIELD, date);
    	}
    }
    
    private List<MPK> getMpk(Long documentId, FieldsManager fm) throws EdmException {
		BigDecimal tax = calculateTax(fm);
		tax.setScale(MONEY_SCALE + 2);
		
    	List<MPK> mpkListFromDataBase = null;
		List<Integer> mpkIds = MPK.findMPK(documentId);
		if (mpkIds != null && !mpkIds.isEmpty()) {
    		mpkListFromDataBase = DSApi.context().session().createCriteria(MPK.class)
    			.add(Restrictions.in("id", mpkIds))
    			.list();
    		
    		for (MPK mpkItem : mpkListFromDataBase) {
    			BigDecimal amountWithCorrectScale = mpkItem.getGrossAmount().setScale(MONEY_SCALE);
    			mpkItem.setGrossAmount(amountWithCorrectScale);
    			String centrumName = MPK.findCentrumName(mpkItem.getCentrumId());
    			mpkItem.setCentrumName(centrumName);
    			BigDecimal netAmount = amountWithCorrectScale.multiply(tax).setScale(MONEY_SCALE, RoundingMode.HALF_UP);
    			mpkItem.setNetAmount(netAmount);
    		}
		}
		return mpkListFromDataBase;
    }
    
    private Person getSenderFromDocument(FieldsManager fm) throws EdmException {
    	Long id = (Long) fm.getKey(SENDER_FIELD);
    	Person sender = Person.find(id.longValue());
    	return sender;
    }
    
    private BigDecimal calculateTax(FieldsManager fm) throws EdmException {
		BigDecimal netAmount = (BigDecimal) fm.getKey(KWOTA_NETTO_FIELD);
		netAmount = netAmount.setScale(MONEY_SCALE); 
		BigDecimal grossAmount = (BigDecimal) fm.getKey(KWOTA_BRUTTO);
		grossAmount = grossAmount.setScale(MONEY_SCALE);
		BigDecimal tax = netAmount.divide(grossAmount, MONEY_SCALE + 2, RoundingMode.HALF_UP);
		return tax;
    }
    
    @Override
    public void documentPermissions(Document document) throws EdmException
    {
        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
        String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
        String documentKindName = document.getDocumentKind().getName();

        perms.add(new PermissionBean(ObjectPermission.READ, documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, documentKindName + " - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP,
                documentKindName + " zalacznik - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, documentKindName
                + " - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP,
                documentKindName + " zalacznik - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, documentKindName
                + " - usuwanie"));
        Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
        perms.addAll(documentPermissions);
        this.setUpPermission(document, perms);

    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        

        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent("Faktura zakupowa");

        // folder =
        // folder.createSubfolderIfNotPresent(FolderInserter.toMonth(document.getCtime()));

        document.setFolder(folder);
        // sprawdzenie czy faktura zosta�a po��czona z inn� faktur�, je�eli, to w powi�znej ustawiamy PROFORMA na false
        checkAndSetProForma(fm);
        // sprawdzenie czy korekta zosta�a polaczona z originalna faktura, jezeli tak to jest wyswietlana informacja na fakturze oryginalnej
        // ze zostala powiazana z korekta
        checkAndSetKorekta(fm);

        /**
         * Je�li faktura jest na etapie ksi�gowo�ci, sprawdzamy czy zosta�y
         * dokonane zmiany w kosztach poszczeg�lnych pozycji bud�etowych. Je�li,
         * w kt�rym� przypadku zmiana jest wi�ksza ni� 10 ustawiamy pole
         * MPK_CORRECTION na true, aby p�niej w procesie wybra� odpowiedni�
         * �cie�k�.
         *
         * Po ka�dym zapisie dokumentu, dla ka�dej pozycji bud�etowej, ustawiamy
         * kwot� przed korekt�, na tak� sam� jak kwota brutto danej pozycji, aby
         * p�niej m�c sprawdzi� czy Dzia� Ksi�gowo�ci dokona� korekty
         */
        boolean toCorrection = false;
        Long docId = document.getId();
        @SuppressWarnings("unchecked")
        List<Long> list = (List<Long>) fm.getKey(MPK_FIELD);
        if (list != null && list.size() > 0) {
            int status = fm.getEnumItem("STATUS") != null ? fm.getEnumItem("STATUS").getId() : 0;
            for (Long id : list) {
                CentrumKosztowDlaFaktury ckf = CentrumKosztowDlaFaktury.getInstance().find(id);
                if (status == 120 || status == 140) {
                    if (ckf.getAmountUsed() != null) {
                        BigDecimal diff = ckf.getAmountUsed().setScale(2, BigDecimal.ROUND_HALF_DOWN)
                                .subtract(ckf.getRealAmount().setScale(2, BigDecimal.ROUND_HALF_DOWN)).abs();
                        if (diff.compareTo(BigDecimal.TEN) > 0) {
                            toCorrection = true;
                            break;
                        }
                    }
                } else {
                    ckf.setAmountUsed(ckf.getRealAmount());
                }
            }
            Map<String, Object> valuesToChange = new HashMap<String, Object>();
            valuesToChange.put(MPK_CORRECTION_FIELD, Boolean.valueOf(toCorrection));
            document.getDocumentKind().setOnly(docId, valuesToChange);
        }
        
        Map<String, Object> map = fm.getFieldValues();
        int i = 0;
    }

    private void checkAndSetProForma(FieldsManager fm) throws EdmException
    {
        if (fm != null && fm.getKey(F_PROFORMA) != null)
        {
            Long proformaId =  (Long) fm.getKey(F_PROFORMA);
            String tableName = fm.getDocumentKind().getTablename();
            String fProformaColumnName = fm.getField("PROFORMA").getColumn();
            PreparedStatement ps = null;
            ResultSet rs = null;
            try
            {
                StringBuilder sql = new StringBuilder("update ")
                        .append(tableName)
                        .append(" set ")
                        .append(fProformaColumnName)
                        .append(" = ")
                        .append("NULL")
                        .append(" where document_id = ").append(proformaId);
                ps = DSApi.context().prepareStatement(sql.toString());
                ps.executeUpdate();
                ps.close();

                DSApi.context().closeStatement(ps);
            }
            catch (Exception e)
            {
                log.error(e.getMessage(), e);
            }
            finally
            {
                DSApi.context().closeStatement(ps);
            }
        }
    }
    
    /**
     * Metoda sprawdza czy korekta jest powi�zana z oryginaln� faktur�. Je�eli tak to
     * po stronie oryginalnej faktury ustawia pole SHOW_KOREKTA_INFO na true (pole to jest flag�)
     * co powoduje wy�wietlenie informacji na oryginalnej fakturze o powi�zaniu z korekt�
     * @param fm - FieldsManager danego dokumentu
     * @throws EdmException
     */
    private void checkAndSetKorekta(FieldsManager fm) throws EdmException {
        if (fm != null && fm.getKey(F_FAKTURA) != null)
        {
            Long fakturaId =  (Long) fm.getKey(F_FAKTURA);
            String tableName = fm.getDocumentKind().getTablename();
            String korektaColumnName = fm.getField(SHOW_KOREKTA_INFO).getColumn();
            PreparedStatement ps = null;
            ResultSet rs = null;
            try
            {
                StringBuilder sql = new StringBuilder("update ")
                        .append(tableName)
                        .append(" set ")
                        .append(korektaColumnName)
                        .append(" = ")
                        .append("1")
                        .append(" where document_id = ").append(fakturaId);
                ps = DSApi.context().prepareStatement(sql.toString());
                ps.executeUpdate();
                ps.close();

                DSApi.context().closeStatement(ps);
            }
            catch (Exception e)
            {
                log.error(e.getMessage(), e);
            }
            finally
            {
                DSApi.context().closeStatement(ps);
            }
        }
    }

    @Override
    public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
    {
        /**
         * Wyliczanie kwoty VAT (PLN)
         */
        if (DwrUtils.isNotNull(values, DWR_KWOTA_NETTO, DWR_KWOTA_BRUTTO) && (values.get(DWR_KWOTA_NETTO).isSender() || values.get(DWR_KWOTA_BRUTTO).isSender())) {
    	    BigDecimal netto = values.get(DWR_KWOTA_NETTO).getMoneyData().setScale(2, RoundingMode.HALF_UP);
    	    BigDecimal brutto = values.get(DWR_KWOTA_BRUTTO).getMoneyData().setScale(2, RoundingMode.HALF_UP);
    	    BigDecimal vat = brutto.subtract(netto);
    	    values.get(DWR_KWOTA_VAT).setMoneyData(vat);
    	}
        
        /**
         * Automatyczne rozpisanie faktury na MPK
         */
        if(values.get(DWR_GENERATE_MPK) != null && values.get(DWR_GENERATE_MPK).isSender()){
    			Long mpkId = null;
    			BigDecimal koszt = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_DOWN);
    			BigDecimal kwotaBrutto = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_DOWN);
    			kwotaBrutto = values.get(DWR_KWOTA_BRUTTO).getMoneyData().setScale(2, RoundingMode.HALF_DOWN);
            	
				EnumItem dbEnumField;
				FieldData mpkFields = values.get("DWR_MPK");
//    			Map<String, FieldData> mpkValues = (HashMap<String, FieldData>) mpkFields.getData();
				Map<String, FieldData> mpkValues = new HashMap<String, FieldData>();
    			CentrumKosztowDlaFaktury mpk = new CentrumKosztowDlaFaktury();
				for(int i = 1; i<=10; i++){ // 8 pozycji ze s�ownika dsg_zbp_centra_kosztow
					
					dbEnumField = DataBaseEnumField.getEnumItemForTable(CENTRA_KOSZTOW_TABLENAME, i);
					koszt = BigDecimal.valueOf(Float.valueOf(dbEnumField.getRefValue()));
					koszt = (koszt.multiply(kwotaBrutto.divide(new BigDecimal(100))));
					
					DSApi.openAdmin();
					DSApi.context().begin();
					mpk.setCentrumId(i);
					mpk.setAmount(koszt.setScale(2, RoundingMode.HALF_DOWN));
					Persister.create(mpk);
					DSApi.context().commit();
					DSApi.close();
					mpkId = mpk.getId();
					mpkValues.put("MPK_ID_" + i, new FieldData(Field.Type.STRING, mpkId));
					
				}
				mpkFields.setDictionaryData(mpkValues);
        }
        
        /**
         * Ukrywanie pola ZARZAD, je�li uzupe�nimy kwot� w pozycji bud�etowej
         */
    	EnumItem statusEnum = fm.getEnumItem(STATUS_FIELD_NAME);
    	Integer status = null;
    	if (statusEnum != null) {
    		status = statusEnum.getId();
    	}
        boolean hasMpkCost = false;
        if (status != null && status.equals(100) && values.get("DWR_" + MPK_FIELD) != null) {
            Map<String, FieldData> mpk = values.get("DWR_" + MPK_FIELD).getDictionaryData();
            if (mpk != null) {
                for (String cn : mpk.keySet()) {
                    if ((cn.contains("MPK_ID_") && mpk.get(cn).getData() != null) || (cn.contains("MPK_AMOUNT") && mpk.get(cn).getData() != null)){
                        	hasMpkCost = true;
                    }
                }
                if ((values.get(DWR_GENERATE_MPK) != null && values.get(DWR_GENERATE_MPK).isSender()) || values.get("DWR_" + HIDE_MPK_FIELD) != null) {
                    values.get("DWR_" + HIDE_MPK_FIELD).setBooleanData(Boolean.valueOf(hasMpkCost));
                } else {
                    values.put("DWR_" + HIDE_MPK_FIELD, new FieldData(Field.Type.BOOLEAN, Boolean.valueOf(hasMpkCost)));
                }
            }
        }
        
        return null;
    }

//    @Override
//    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues,
//	    Long documentId) {
//	Map<String, Object> results = new HashMap<String, Object>();
//	if (dictionaryName.equals(MPK_FIELD)) {
//	    if(values.get(MPK_FIELD + "_AMOUNT").getData() != null && values.get(MPK_FIELD + "_CENTRUMID").getData() != null) {
//		results.put("HIDE_MPK", Boolean.TRUE);
//	    }
//	}
//	return results;
//    }

    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue)
    {
        boolean assigned = false;
        try {
            Long userId = (Long) openExecution.getVariable(FakturaZakupowaLogic.MPK_VARIABLE_NAME);
            if (BOARD_MEMBERS_ACCEPTATION_CN.equals(acceptationCn)) {
                if (MPK_FIELD.equals(fieldCnValue)) {
                	
						assignable.addCandidateUser(DSUser.findById(userId).getName());
						AssigneeHandler.addToHistory(openExecution, doc, DSUser.findById(userId).getName(), null);
						assigned = true;
					
                } else if (ZARZAD_FIELD.equals(fieldCnValue)) {

                    assigned = assignUser(doc, assignable, openExecution, fieldCnValue, userId);
                	
                } else if (MANAGEMENT_MEMBERS.equals(fieldCnValue)) {
                	
                    assigned = assignUser(doc, assignable, openExecution, fieldCnValue, userId);
                    
                } else if (MEMBER_OF_MANAGEMENT.equals(fieldCnValue)) {
                	Long documentId = doc.getId();
                	FieldsManager fm = doc.getDocumentKind().getFieldsManager(documentId);
                	List<Long> idList = (List<Long>) fm.getKey(ZARZAD_ADD_FIELD);
                	// slownik "ZARZAD_ADD" jest ustawiony jako wielowartosciowy ale ma zablokowana na tym etapie
                	// mozliwosc dodawania kolejnych elementow dlatego w liscie bedzie tylko jedna wartosc
                	Long id = idList.get(0);
                	Long memberId = getUserIdFromDatabase(id);
                	assigned = assignUser(doc, assignable, openExecution, fieldCnValue, memberId);
                } else if (CORRECTION_BY_MEMBER.equals(fieldCnValue)) {
                	String member = (String) openExecution.getVariable(C_USER_JBPM4_VARIABLE_NAME);
                    assignable.addCandidateUser(member);
                    AssigneeHandler.addToHistory(openExecution, doc, member, null);
                    assigned = true;
                }
            }
            return assigned;
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            return assigned;
        }
    }
    
    /**
     * Metoda na podstawie id wpisu do s�ownika "ZARZAD_ADD" zwraca id usera.
     * @param id wartosc ze s�ownika "ZARZAD_ADD" (id wpisu do tego s�ownika)
     * @return zwraca id usera lub w przypadku gdy nie znajdzie usera zwraca null
     */
    private Long getUserIdFromDatabase(Long id) {
    	Long userId = null;
    	try {
	    	BigDecimal userIdTemp = (BigDecimal) DSApi.context().session().createSQLQuery("SELECT MEMBER FROM DSG_ZBP_BOARD WHERE ID = :id")
	    		.setParameter("id", id).uniqueResult();
	    	userId = userIdTemp.longValue();
    	} catch (EdmException e) {
    		log.error(e.getMessage(), e);
    	}
    	return userId;
    }
    
    /**
     * Przypisuje u�ytkownika do zadania w procesie Faktury Zakupowej, zale�nie
     * od parametru fieldCnValue, kt�ry wskazuje s�ownik z cz�onkami zarz�du.
     *
     * @param doc
     * @param assignable
     * @param openExecution
     * @param fieldCnValue  nazwa s�ownika, z kt�rego wybieramy u�ytkownika
     * @param dicId         identyfikator wpisu w s�owniku, kt�rego nazw� okre�la
     *                      fieldCnValue
     * @return 'true' je�li znaleziono u�ytkownika
     * @throws EdmException
     * @throws UserNotFoundException
     */
    private boolean assignUser(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String fieldCnValue, Long userId)
            throws EdmException, UserNotFoundException
    {
        boolean assigned = false;
        String userName = DSUser.findById(userId).getName();
        assignable.addCandidateUser(userName);
        assigned = true;
        AssigneeHandler.addToHistory(openExecution, doc, userName, null);
        return assigned;
    }

    public void beforeSetWithHistory(Document document, Map<String, Object> values, String activity) throws EdmException {
    	Long documentId = document.getId();
    	FieldsManager fm = document.getDocumentKind().getFieldsManager(documentId);
    	
    	//sprawdzenie czy jest kwota brutto w MPK, jezeli jest to sprawdza czy zgadza sie z kwota brutto z faktury
    	BigDecimal amount = new BigDecimal(0.0);
    	boolean isAmountFromMPK = false;
    	BigDecimal amountFromMPK = new BigDecimal(0.0);
    	if (values.containsKey(M_DICT_VALUES) && values.get(M_DICT_VALUES) != null) {
    		Map<String, Map<String, FieldData>> mpkValues = (Map<String, Map<String, FieldData>>)values.get(M_DICT_VALUES);
    		for (Map.Entry<String,Map<String,FieldData>> entry : mpkValues.entrySet()) {
    			if (entry.getValue().containsKey(AMOUNT) && entry.getValue().get(AMOUNT).getMoneyData() != null) {
    				amountFromMPK = amountFromMPK.add(entry.getValue().get(AMOUNT).getMoneyData());
    				amountFromMPK = amountFromMPK.setScale(MONEY_SCALE);
    				isAmountFromMPK = true;
    			}
    		}
    		//pobranie kwoty brutto z faktury
    		amount = (BigDecimal)fm.getKey(KWOTA_BRUTTO);
    		amount = amount.setScale(MONEY_SCALE);
    		//sprawdzenie czy sa rowne
        	if (isAmountFromMPK && !amount.equals(amountFromMPK)) {
        		BigDecimal difference = amountFromMPK.subtract(amount);
        		//czy suma MPK jest mniejsza od kwoty brutto z faktury
        		if (difference.signum() < 0) {
        			throw new EdmException(sm.getString("FakturaZakupowaSmallerAcount", difference.abs().toString()));
        		} else {
        			throw new EdmException(sm.getString("FakturaZakupowaGreaterAcount", difference.abs().toString()));
        		}
        	}
    	}
    	
    	//sprawdzenie czy slownik MPK byl modyfikowany
    	EnumItem statusEnum = fm.getEnumItem(STATUS_FIELD_NAME);
    	Integer status = null;
    	if (statusEnum != null) {
    		status = statusEnum.getId();
    	}
    	if (status != null && (status.equals(110) || status.equals(111) || status.equals(113))) {
    		List<Integer> mpkIds = MPK.findMPK(documentId);
    		if (mpkIds != null && !mpkIds.isEmpty()) {
	    		List<MPK> mpkListFromDataBase = DSApi.context().session().createCriteria(MPK.class)
	    			.add(Restrictions.in("id", mpkIds))
	    			.list();
	    		
	    		List<MPK> mpkFromFields = getMPKFromFieldsManager(values);
	    		if (!areListsEquals(mpkListFromDataBase, mpkFromFields)) {
	    			//ustawienie flagi ze MPK byl modyfikowany
	    			String activityId = getActivityId(document);
	    			if (activityId != null) {
	    				TaskService ts = Jbpm4Provider.getInstance().getTaskService();
	    				ts.setVariables(activityId, Collections.singletonMap(MPK_WAS_CHANGED_JBPM4_VARIABLE, true));
	    			}
	    		}
	    		
    		}// else {
//    			List<MPK> mpkFromFields = getMPKFromFieldsManager(values);
//    			if (!mpkFromFields.isEmpty()) {
//    				//ustawienie flagi i zapisanie id wybranych dodatkowych czlonkow zarzadu
//    				List<Long> addsManagement = getUserIdsFromDwr((OfficeDocument)document, ZARZAD_ADD_MEMBER_FIELD_NAME, ZARZAD_ADD_FIELD);
//	    			String activityId = getActivityId(document);
//	    			if (activityId != null) {
//	    				TaskService ts = Jbpm4Provider.getInstance().getTaskService();
//	    				//ts.setVariables(activityId, Collections.singletonMap(MPK_WAS_CHANGED_JBPM4_VARIABLE, true));
//	    				ts.setVariables(activityId, Collections.singletonMap(ADDS_MANAGEMENT_JBPM4_VARIABLE, addsManagement));
//	    			}
//    			}
    		List<Long> addsManagement = getUserIdsFromDwr(values);
    		String activityId = getActivityId(document);
			if (activityId != null) {
				TaskService ts = Jbpm4Provider.getInstance().getTaskService();
				ts.setVariables(activityId, Collections.singletonMap(ADDS_MANAGEMENT_JBPM4_VARIABLE, addsManagement));
			}
    	}
    }

    @Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields) {
		log.info("Before Search - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.contains(F_PROFORMA)) {
			values.put(dictionaryName + "_PROFORMA", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
		} else if(dictionaryName.contains(F_FAKTURA)) {
			values.put(dictionaryName + "_PROFORMA", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, false));
			values.put(dictionaryName + "_KOREKTA", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, false));
		}
	}
    
    private List<MPK> getMPKFromFieldsManager(Map<String, ?> fieldValues) {
    	List<MPK> mpkList = Lists.newArrayList();
    	if (fieldValues.containsKey(M_DICT_VALUES) && fieldValues.get(M_DICT_VALUES) != null) {
			Map<String, FieldData> mpkValues = (Map<String, FieldData>) fieldValues.get(M_DICT_VALUES);
			for (Map.Entry<String, FieldData> mpkRow : mpkValues.entrySet()) {
				if (mpkRow.getKey().contains(MPK_FIELD) && mpkRow.getValue() != null) {
					Map<String, FieldData> rowValues = (Map<String, FieldData>) mpkRow.getValue();
					MPK mpk = new MPK();
					for (Map.Entry<String, FieldData> value : rowValues.entrySet()) {
							if(value.getKey().contains(AMOUNT)) {
								FieldData amountField = (FieldData) value.getValue();
								mpk.setGrossAmount(amountField.getMoneyData());
							}
							if(value.getKey().contains(CENTRUM_ID) && !StringUtils.isBlank(value.getValue().getStringData())) {
								String centrumIdString = ((FieldData) value.getValue()).getStringData();
								mpk.setCentrumId(Integer.valueOf(centrumIdString));
							}
					}
					if (mpk.getGrossAmount() != null && mpk.getCentrumId() != null) {
						mpkList.add(mpk);
					}
				}
			}
		}
    	return mpkList;
    }
    
    private boolean areListsEquals(List<MPK> listOne, List<MPK> listTwo) {
    	Multiset<MPK> setOne = HashMultiset.create();
    	setOne.addAll(listOne);
    	
    	Multiset<MPK> setTwo = HashMultiset.create();
    	setTwo.addAll(listTwo);
    	
    	return setOne.equals(setTwo);
    }
    
	private String getActivityId(Document document) throws EdmException {
    	String activityId = null;
    	Iterable<String> taskIds = Jbpm4ProcessLocator.taskIds(document.getId());
    	for (String taskId : taskIds)
    		activityId = taskId;
		activityId = cleanActivityId(activityId);
		return activityId;
	}
	
    private String cleanActivityId(String activityId){
        return activityId.replaceAll("[^,]*,","");
    }
    
    /**
     * Metoda szuka w values id user�w ze s�ownika z dodatkowymi cz�onkami zarz�du, a nast�pnie dodaje ich do listy
     * i zwraca t� list�. 
     * @param values mapa z warto�ciami z p�l dokumentu, gdzie kluczem jest cn pola, natomiast warto�ci� jest
     * warto�� danego pola
     * @return zwraca list� z id user�w ze s�ownika dodatkowych cz�onk�w zarz�du, lub pust� list� gdy nie
     * znaleziono w s�owniku �adnych user�w.
     * @throws EdmException
     */
    private List<Long> getUserIdsFromDwr(Map<String, Object> values) throws EdmException {
    	List<Long> ids = new LinkedList<Long>();
    	if (values.containsKey(M_DICT_VALUES) && values.get(M_DICT_VALUES) != null) {
			Map<String, Object> mDictValues = (Map<String, Object>) values.get(M_DICT_VALUES);
	    	for (Map.Entry<String, Object> field : mDictValues.entrySet()) {
	    		if (field.getKey().contains(ZARZAD_ADD_FIELD) && field.getValue() != null) {
	    			Map<String, Object> row = (Map<String, Object>) field.getValue();
	    			FieldData additionalMember = (FieldData)row.get(MEMBER);
	    			if (!StringUtils.isBlank(additionalMember.getStringData())) {
	    				ids.add(additionalMember.getLongData());
	    			}
	    		}
	    	}
    	}
    	return ids;
    }
}