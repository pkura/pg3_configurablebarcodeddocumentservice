package pl.compan.docusafe.parametrization.zbp;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;


public class CzasopismoLogic extends AbstractDocumentLogic {

	private static final long serialVersionUID = 1L;
	private static CzasopismoLogic instance;
    protected static Logger log = LoggerFactory.getLogger(CzasopismoLogic.class);

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {

    }

    @Override
    public void documentPermissions(Document document) throws EdmException {
    }

    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
        if (acceptationCn != null && acceptationCn.equals("TO_ACCEPTANCE")) {
            try{
                DSUser user = DSUser.findById(((Integer)doc.getFieldsManager().getFieldValues().get("USER_ACCEPTING")).longValue());
                assignable.addCandidateUser(user.getName());
                //addToHistory(openExecution, doc, user.getName());
            } catch (Exception e) {
                log.warn(e.getMessage());
                return false;
            }
            return true;
        }
        return false;
    }

    public static void addToHistory(OpenExecution openExecution, OfficeDocument doc, String user) throws EdmException {
        AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
        ahe.setSourceUser(DSApi.context().getPrincipalName());
        ahe.setProcessName(openExecution.getProcessDefinitionId());
        ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_SINGLE_USER);
        ahe.setTargetUser(user);

      /*  DSDivision[] divs = DSApi.context().getDSUser()
                .getOriginalDivisionsWithoutGroup();
        if (divs != null && divs.length > 0)
            ahe.setSourceGuid(divs[0].getName());*/
      /*  ahe.setCtime(new Date());
        ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
        try {
            String status = doc.getFieldsManager().getEnumItem("STATUS").getTitle();
            ahe.setStatus(status);
        } catch (Exception e) {
            log.warn(e.getMessage());
        }*/
        doc.addAssignmentHistoryEntry(ahe);
    }
}