/**
 * 
 */
package pl.compan.docusafe.parametrization.zbp.jbpm;

import java.util.Date;
import java.util.Set;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.SubstituteHistory;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 24-09-2013
 * docusafe
 * CheckReplacement.java
 */
public class CheckReplacement implements DecisionHandler{

	private final Logger log = LoggerFactory.getLogger(CheckReplacement.class);
	public static final String WNIOSKODAWCA_CN = "WNIOSKODAWCA";
	public static final String DATE_FROM_CN = "DATE_FROM";
	public static final String DATE_TO_CN = "DATE_TO";
	
	@Override
	public String decide(OpenExecution execution) {
		try {
			Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			FieldsManager fm = OfficeDocument.find(docId).getFieldsManager();
			Integer wnioskodawcaId = (Integer)fm.getKey(WNIOSKODAWCA_CN);
			Date fromDate = (Date)fm.getKey(DATE_FROM_CN);
			Date toDate = (Date)fm.getKey(DATE_TO_CN);
			DSUser user = DSUser.findById(wnioskodawcaId.longValue());
			Boolean isSubstitiute = SubstituteHistory.checkSubstitiute(user.getName(), fromDate, toDate);
			if(isSubstitiute){
				return "toDivisionOwner";
			}else{
				return "toSupervisor";
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return "toSupervisor";
	}

}
