/**
 * 
 */
package pl.compan.docusafe.parametrization.zbp.jbpm;

import java.util.List;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.jbpm4.CloseOtherExecutions;
import pl.compan.docusafe.parametrization.zbp.FakturaZakupowaLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class CheckingMPKChanges implements ExternalActivityBehaviour {
	
	public static final String MPK_WAS_CHANGED_JBPM4_VARIABLE = FakturaZakupowaLogic.MPK_WAS_CHANGED_JBPM4_VARIABLE;
	public static final String ADDS_MANAGEMENT_JBPM4_VARIABLE = "ADDS_MANAGEMENT";
	public static final Logger logger = LoggerFactory.getLogger(CheckingMPKChanges.class);
	
	public void execute(ActivityExecution activityExecution) throws Exception {
		Boolean mpkWasChanged = (Boolean)activityExecution.getVariable(MPK_WAS_CHANGED_JBPM4_VARIABLE);
		List<Long> additionalMembers = (List<Long>)activityExecution.getVariable(ADDS_MANAGEMENT_JBPM4_VARIABLE);
		if (mpkWasChanged != null && mpkWasChanged.equals(Boolean.TRUE)) {
			activityExecution.setVariable("correction", 13);
			closeOtherExecutions(activityExecution);
		}
	}

	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {

	}
	
	private void closeOtherExecutions(ActivityExecution activityExecution) throws Exception {
		CloseOtherExecutions closeOtherExecutions = new CloseOtherExecutions();
		closeOtherExecutions.execute(activityExecution);
		logger.info("Zamykamy wszystkie subprocesy utworzone w fork'u i ustawiamy warto�� 'correction' na 13");
	}
}

