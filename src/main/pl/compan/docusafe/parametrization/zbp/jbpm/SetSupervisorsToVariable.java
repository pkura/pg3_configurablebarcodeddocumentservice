/**
 * 
 */
package pl.compan.docusafe.parametrization.zbp.jbpm;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.base.Joiner;
import com.google.common.collect.Sets;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class SetSupervisorsToVariable implements ExternalActivityBehaviour {
	
	private final Logger logger = LoggerFactory.getLogger(SetSupervisorsToVariable.class);
	public static final String WNIOSKODAWCA_FIELD = "WNIOSKODAWCA";
	
	public void execute(ActivityExecution activityExecution) throws Exception {
		HashSet<Long> argSets = new HashSet<Long>();
		Set<String> userNames = Sets.newLinkedHashSet();
        Long docId = Long.valueOf(activityExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        boolean wasOpened = false;
        try {
            if (!DSApi.isContextOpen()) {
                wasOpened = true;
                DSApi.openAdmin();
            }

            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            
		    DSUser user = DSUser.findById(((Integer) doc.getFieldsManager().getKey(WNIOSKODAWCA_FIELD)).longValue());
		    if (user.getSupervisor() != null)
                argSets.add(user.getSupervisor().getId());
            else
            {
                for (DSUser prezes : DSUser.findByLastname("Furga"))
                {
                    argSets.add(prezes.getId());
                }
            }
		    
            for (Long userId : argSets) {
                userNames.add(DSUser.findById(userId).getName());
            }

            activityExecution.setVariable("supervisors", Joiner.on(",").join(userNames));

        } catch (EdmException ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (DSApi.isContextOpen() && wasOpened)
                try {
                    DSApi.close();
                } catch (EdmException e) {
                    logger.error(e.getMessage(), e);
                }
        }
        activityExecution.takeDefaultTransition();
	}

	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
	}

}
