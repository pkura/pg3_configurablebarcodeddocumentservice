package pl.compan.docusafe.parametrization.zbp.jbpm;

import java.util.Date;

import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.calendar.sql.SqlCalendarFactory;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.zbp.DelegacjaLogic;
import pl.compan.docusafe.parametrization.zbp.WniosekUrlopowyLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class CreateEventListener extends AbstractEventListener 
{
	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CN = "DATE_FROM";
	private static final String TO_CN = "DATE_TO";	
	private static final Logger LOG = LoggerFactory.getLogger(CreateEventListener.class);
	
	public void notify(EventListenerExecution eventListenerExecution) throws Exception 
	{
		Long docId = Long.parseLong(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		LOG.info("doc id = " + docId);
		Document document = Document.find(docId);		
		Date from = (Date) document.getFieldsManager().getValue(FROM_CN);
		Date to = (Date) document.getFieldsManager().getValue(TO_CN);
		if (document.getDocumentKind().getCn().equals(DelegacjaLogic.DELEGACJA_CN)) {
		    DSUser dsUser = DSUser.findByUsername(document.getFieldsManager().getEnumItemCn(DelegacjaLogic.DELEGOWANY_FIELD));
		    SqlCalendarFactory.getInstance().createEvents(dsUser, from, to,"[ Delegacja ]");
		} else if (document.getDocumentKind().getCn().equals(WniosekUrlopowyLogic.WNIOSEK_URLOPOWY_CN)) {
		    DSUser dsUser = DSUser.findByUsername(document.getFieldsManager().getEnumItemCn(WniosekUrlopowyLogic.WNIOSKODAWCA_CN));
		    SqlCalendarFactory.getInstance().createEvents(dsUser, from, to,"[ Urlop ]");
		}
	}
}
