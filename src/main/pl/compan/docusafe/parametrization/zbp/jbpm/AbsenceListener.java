package pl.compan.docusafe.parametrization.zbp.jbpm;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.absences.Absence;
import pl.compan.docusafe.core.base.absences.AbsenceType;
import pl.compan.docusafe.core.calendar.CalendarFactory;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.SubstituteHistory;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class AbsenceListener extends AbstractEventListener 
{
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CN = "DATE_FROM";
	private static final String TO_CN = "DATE_TO";
	private static final String WORKING_DAYS_CN = "WORKING_DAYS_CN";
	private static final String KIND_CN = "KIND";
	private static final String DESCRIPTION_CN = "DOC_DESCRIPTION";
	
//	private static final Map<Integer, String> absenceTypes = new HashMap<Integer, String>();
//
//	static {
////
////		absenceTypes.put(10, "UW");
////		absenceTypes.put(20, "UZ");
////		absenceTypes.put(30, "188");
////		absenceTypes.put(40, "UOK");
////		absenceTypes.put(50, "UB");
////		absenceTypes.put(60, "UI");
////		absenceTypes.put(70, "UJ");
////		absenceTypes.put(80, "UM");
////		absenceTypes.put(90, "UY");
////		absenceTypes.put(100, "OG");
////
////warto�ci ze zmiennej mCodes z metody debugAbsenceTypes
//        absenceTypes.put(40, "UW");
//        absenceTypes.put(45, "U�");
//        absenceTypes.put(50, "UBZ");
//        absenceTypes.put(55, "188");
//        absenceTypes.put(60, "OG");
//        absenceTypes.put(65, "UOK");
//        absenceTypes.put(70, "UB");
//        absenceTypes.put(75, "INW");
//        absenceTypes.put(80, "UM");
//        absenceTypes.put(85, "UMD");
//        absenceTypes.put(90, "UJ");
//        absenceTypes.put(95, "NN");
//        absenceTypes.put(100, "NUN");
//        absenceTypes.put(105, "NUP");
//        absenceTypes.put(110, "US");
//	}
//
//    @SuppressWarnings("unused")
//    private void debugAbsenceTypes(Document document) {
//        try {
//            FieldsManager fm = document.getFieldsManager();
//            Field f = fm.getField(KIND_CN);
//            String v = (String) fm.getValue(KIND_CN);
//            StringBuilder sbTitle = new StringBuilder();
//            StringBuilder sbCode = new StringBuilder();
//            for (EnumItem i : f.getEnumItems()) {
//                String type = i.getTitle();
//                sbTitle.append("absenceTypes.put(").append(i.getId()).append(", \"").append(type).append("\");");
//
//                int l = type.lastIndexOf('(');
//                int r = type.lastIndexOf(')');
//                if (l + 1 <= r)
//                    type = type.substring(l + 1, r);
//                sbCode.append("absenceTypes.put(").append(i.getId()).append(", \"").append(type).append("\");");
//            }
//            String mTitles = sbTitle.toString();
//            String mCodes = sbCode.toString();
//            int stop = 0;
//        } catch (Exception e) {
//            //do nothing
//        }
//    }

    /**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AbsenceListener.class);
	
	public void notify(EventListenerExecution eventListenerExecution) throws Exception 
	{
		Long docId = Long.parseLong(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		LOG.info("doc id = " + docId);
		
		// pobranie obiektu dokumentu
		Document document = Document.find(docId);
		
		// pobranie wnioskodawcy
		UserImpl userImpl = (UserImpl) DSUser.findByUsername(document.getAuthor());
		
		EmployeeCard empCard = getEmployeeCard(userImpl);
		if (empCard == null)
			throw new IllegalStateException("Brak karty pracownika! Nie mo�na przypisa� urlopu!");
		
		int year = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
		if (!hasChargedAbsencesInCurrentYear(year, empCard))
			throw new IllegalStateException("Brak przypisanego nale�nego wymiaru urlopowego na bierz�cy rok!");
		
		// utworzenie wpisu urlopu
		Absence absence = new Absence();
		
		absence.setYear(year);
		absence.setEmployeeCard(empCard);
		
		// ustawienie dat
		Date from = (Date) document.getFieldsManager().getValue(FROM_CN);
		Date to = (Date) document.getFieldsManager().getValue(TO_CN);
		absence.setStartDate(from);
		absence.setEndDate(to);
		
		// ustawienie typu urlopu
        //debugAbsenceTypes(document);
        //		int typeId = document.getFieldsManager().getField(KIND_CN).getEnumItemByTitle(
        //				(String) document.getFieldsManager().getValue(KIND_CN)).getId();
		String typeCode = document.getFieldsManager().getField(KIND_CN).getEnumItemByTitle(
				(String) document.getFieldsManager().getValue(KIND_CN)).getCn();
		AbsenceType absenceType = Finder.find(AbsenceType.class, typeCode);
		absence.setAbsenceType(absenceType);
		
		// ustawienie liczby dni
		int numDays = Integer.parseInt(document.getFieldsManager().getValue(WORKING_DAYS_CN) + "");
		absence.setDaysNum(numDays);
		
		// ustawienie opisu
		absence.setInfo((String) document.getFieldsManager().getValue(DESCRIPTION_CN));
		
		// data utworzenia wpisu
		absence.setCtime(new Date());
		absence.setDocumentId(document.getId());
		empCard.getAbsences().add(absence);
		absence.setEmployeeCard(empCard);
		// zapisanie do bazy
		DSApi.context().session().update(empCard);
		DSApi.context().session().save(absence);
        if(AvailabilityManager.isAvailable("absence.createEventOnCreate"))
        {
        	CalendarFactory.getInstance().createEvents(userImpl,from, to,"[ Na urlopie ]",absence.getId());
        }
        //Zastepstwa
        Calendar cal = Calendar.getInstance();
    	cal.setTime(to);
    	cal.set(Calendar.HOUR_OF_DAY, 23);
    	cal.set(Calendar.MINUTE, 59);
        //userImpl.setSubstitution(DSUser.findByUsername(document.getFieldsManager().getEnumItemCn("SUBSTITUTION")), from, cal.getTime());
    	SubstituteHistory history = new SubstituteHistory(userImpl.getName(), from, cal.getTime(), document.getFieldsManager().getEnumItemCn("OSOBA_ZASTEPUJ"));
        //jezeli zastepstwo jest na dzisiaj to od razu wpisujemy do ds_user 
        Date now = new Date();
        if(now.after(from)) userImpl.setSubstitutionFromHistory(history);
        DSApi.context().session().save(history);  
	}
	
	/**
	 * Sprawdza czy w danym roku jest przypisany nale�ny urlop dla karty pracownika
	 * 
	 * @param year
	 * @param empCard
	 * @return
	 * @throws Exception
	 */
	private boolean hasChargedAbsencesInCurrentYear(int year, EmployeeCard empCard) throws Exception
	{	
		String hql = "select 1 from " + Absence.class.getName() + " a where a.year = ? and a.absenceType.code like ? and a.employeeCard = ?";
		Integer has = (Integer) DSApi.context().session().createQuery(hql)
								.setInteger(0, year)
								.setString(1, "WUN")
								.setParameter(2, empCard)
								.setMaxResults(1)
								.uniqueResult();
		if (has == null)
			return false;
		
		return true;
	}
	
	/**
	 * Zwraca kart� pracownika
	 * 
	 * @param userImpl
	 * @return
	 * @throws Exception
	 */
	private EmployeeCard getEmployeeCard(UserImpl userImpl) throws Exception
	{
		return (EmployeeCard) DSApi.context().session().createQuery(
				"from " + EmployeeCard.class.getName() + " e where e.user = ?")
				.setParameter(0, userImpl)
				.setMaxResults(1)
				.uniqueResult();
	}
}
