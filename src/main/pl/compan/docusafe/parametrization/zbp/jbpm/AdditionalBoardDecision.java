package pl.compan.docusafe.parametrization.zbp.jbpm;

import java.util.List;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.zbp.FakturaZakupowaLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class AdditionalBoardDecision implements DecisionHandler {

    private static final long serialVersionUID = 1L;
    private final static Logger LOG = LoggerFactory.getLogger(AdditionalBoardDecision.class);

    public String decide(OpenExecution openExecution) {
	Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
	try {
	    OfficeDocument doc = OfficeDocument.find(docId);
	    FieldsManager fm = doc.getFieldsManager();
	    @SuppressWarnings("unchecked")
	    List<Long> ids = (List<Long>) fm.getKey(FakturaZakupowaLogic.ZARZAD_ADD_FIELD);
	    if (ids != null && ids.size() > 0) {
		return "true";
	    } else {
		return "false";
	    }
	} catch (EdmException e) {
	    LOG.error(e.getMessage(), e);
	    return "false";
	}

    }
}