package pl.compan.docusafe.parametrization.zbp.jbpm;

import java.util.List;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

public class IsCorrectionAfterForkDecision implements DecisionHandler {

    private static final long serialVersionUID = 1L;
    public static final String ADDS_MANAGEMENT_JBPM4_VARIABLE = "ADDS_MANAGEMENT";
    public static final String IDS_JBPM4_VARIABLE = "ids";
    public static final String COUNT_JBPM4_VARIABLE = "count";
    public static final String MODIFICATION_MPK_OR_ADDITIONAL_MEMBERS_DECISION_NAME = "modificationMpkOrAdditionalMembers"; 

    @Override
    public String decide(OpenExecution openExecution) {
	Integer cor = (Integer) openExecution.getVariable("correction");

	if (cor != null && cor == 11) {
	    openExecution.removeVariable("correction");
	    openExecution.setVariable("correction", 2);
	    openExecution.setVariable("count", 2);
	    return "correction";
	} else if (cor != null && cor == 12) {
	    openExecution.removeVariable("correction");
	    openExecution.removeVariable("count");
	    return "rejection";
	} else if (cor != null && cor == 13) {
		openExecution.removeVariable("correction");
		return MODIFICATION_MPK_OR_ADDITIONAL_MEMBERS_DECISION_NAME;
	} else {
		List<Long> additionalMembers = (List<Long>)openExecution.getVariable(ADDS_MANAGEMENT_JBPM4_VARIABLE);
		//przypadek gdy zostali dodani dodatkowi czlonkowie zarzadu
		if (additionalMembers != null && !additionalMembers.isEmpty()) {
			return MODIFICATION_MPK_OR_ADDITIONAL_MEMBERS_DECISION_NAME;
		}
	    return "normal";
	}
    }

}
