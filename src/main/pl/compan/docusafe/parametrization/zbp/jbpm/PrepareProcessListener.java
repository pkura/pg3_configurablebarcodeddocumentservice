package pl.compan.docusafe.parametrization.zbp.jbpm;

import com.google.common.collect.Maps;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.zbp.DelegacjaLogic;
import pl.compan.docusafe.parametrization.zbp.FakturaZakupowaLogic;
import pl.compan.docusafe.parametrization.zbp.WniosekUrlopowyLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PrepareProcessListener implements ExternalActivityBehaviour {

    private final Logger log = LoggerFactory.getLogger(PrepareProcessListener.class);
    private static final long serialVersionUID = 1L;
    private String dictionaryField;
    public static final String CENTRA_KOSZTOW_TABLENAME = "dsg_zbp_centra_kosztow";
    public static final String MPK_DWR_NAME = "MPK";
    public static final String ZARZAD_DWR_NAME = "ZARZAD";
    public static final String ZARZAD_MEMBER_FIELD_NAME = "ZARZAD_MEMBER";
    public static final String ZARZAD_ADD_DWR_NAME = "ZARZAD_ADD";
    public static final String ZARZAD_ADD_MEMBER_FIELD_NAME = "ZARZAD_ADD_MEMBER";
    public static final String MANAGEMENT_MEMBERS = "MANAGEMENT_MEMBERS";
    public static final String MEMBER_CORRECTION = "MEMBER_CORRECTION";
    public static final String MPK_WAS_CHANGED_JBPM4_VARIABLE = FakturaZakupowaLogic.MPK_WAS_CHANGED_JBPM4_VARIABLE;
    public List<String> setValue = new ArrayList<String>();
    //mapa zawierajaca id usera i informacje czy zostal juz do niego przypisany dokument. Wykorzystywane w forku
    //private Map<Long, Boolean> assignmentsMap = Maps.newHashMap();
    final HashSet<Long> argSets = new HashSet<Long>();

    // private String enumIdField;
    // public static final List<String> guids = new LinkedList<String>();

    @Override
    public void execute(ActivityExecution execution) throws Exception {
    	Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
    	OfficeDocument doc = OfficeDocument.find(docId);

		if (WniosekUrlopowyLogic.WNIOSKODAWCA_CN.equalsIgnoreCase(dictionaryField) || DelegacjaLogic.DELEGOWANY.equalsIgnoreCase(dictionaryField)) {
		    DSUser user = DSUser.findById(((Integer) doc.getFieldsManager().getKey(dictionaryField)).longValue());
		    if (user.getSupervisor() != null)
                argSets.add(user.getSupervisor().getId());
            else
            {
                for (DSUser prezes : DSUser.findByLastname("Furga"))
                {
                    argSets.add(prezes.getId());
                }
            }

//            for (DSUser supervisor : user.getImmediateSupervisors()) {
//		    	if (!argSets.contains(supervisor.getId())) {
//		    		argSets.add(supervisor.getId());
//		    	}
//		    }
		} else if (MANAGEMENT_MEMBERS.equalsIgnoreCase(dictionaryField)) {
			//wyczyszczenie listy z id
			argSets.clear();
			Boolean mpkWasChanged = (Boolean)execution.getVariable(MPK_WAS_CHANGED_JBPM4_VARIABLE);
			if (mpkWasChanged == null || mpkWasChanged.equals(Boolean.TRUE)) {
				// pobranie id glownych czlonkow zarzadu ze slownika
				getUserIdsFromDwr(doc, ZARZAD_DWR_NAME, ZARZAD_MEMBER_FIELD_NAME);
				// pobranie id dodatkowych czlonkow zarzadu
				getUserIdsFromDwr(doc, ZARZAD_ADD_DWR_NAME, ZARZAD_ADD_MEMBER_FIELD_NAME);
				getUserIdsFromMPK(doc, MPK_DWR_NAME);
			} else {
				// pobranie id userow: dodatkowych czlonkow zarzadu
				getUserIdsFromDwr(doc, ZARZAD_ADD_DWR_NAME, ZARZAD_ADD_MEMBER_FIELD_NAME);
			}
			//zresetowanie flagi zmiany MPK: poprzez ustawienie wartosci na false
			execution.setVariable(MPK_WAS_CHANGED_JBPM4_VARIABLE, false);
			moveAdditionalMembersToMain(docId);
			
		} else if (MEMBER_CORRECTION.equalsIgnoreCase(dictionaryField)) {
			argSets.clear();
			getUserIdsFromDwr(doc, ZARZAD_DWR_NAME, ZARZAD_MEMBER_FIELD_NAME);
			getUserIdsFromDwr(doc, ZARZAD_ADD_DWR_NAME, ZARZAD_ADD_MEMBER_FIELD_NAME);
			getUserIdsFromMPK(doc, MPK_DWR_NAME);
			execution.setVariable(MPK_WAS_CHANGED_JBPM4_VARIABLE, false);
			moveAdditionalMembersToMain(docId);
		}

    	execution.setVariable("ids", argSets);
    	execution.setVariable("count", argSets.size());
    }

    @Override
    public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception {
	// TODO Auto-generated method stub

    }
    
    /**
     * Metoda pobiera ze s�ownika z cz�onkami zarz�du (dodatkowych lub g��wnych w zale�no�ci od nazwy s�ownika) id user�w
     * i dodaje je do listy argSets (zmiennej instancyjnej)  
     * @param doc
     * @param dictionaryName - nazwa s�ownika z kt�rego pobieramy id user�w
     * @param fieldName - nazwa pola w s�owniku. Na podstawie tego pola pobieramy id user�w
     * @throws EdmException
     */
    private void getUserIdsFromDwr(OfficeDocument doc, String dictionaryName, String fieldName) throws EdmException {
		List<Map<String, Object>> valuesFromManagementDwr = (List<Map<String, Object>>) doc.getFieldsManager().getValue(dictionaryName);
		// pobranie id userow wybranych w danym slowniku
		if (valuesFromManagementDwr != null && !valuesFromManagementDwr.isEmpty()) {
			for (Map<String, Object> rowFromDwr : valuesFromManagementDwr) {
				for (Map.Entry<String, Object> entry : rowFromDwr.entrySet()) {
					if (entry.getKey().contains(fieldName) && entry.getValue() != null) {
						EnumValues val = (EnumValues)entry.getValue();
						if (val.getSelectedId() != null) {
							Long userId = Long.valueOf(val.getSelectedId());
							if (!argSets.contains(userId)) {
								argSets.add(userId);
							}
						}
					}
				}
			}
    	}
    }
    
    /**
     * Metoda pobiera ze s�ownika MPK o podanej nazwie (parametr dictionaryName) id user�w odpowiedzialnych za dany MPK i
     * dodaje je do listy argSets (zmiennej instancyjnej)
     * @param doc
     * @param dictionaryName - cn s�ownika z MPKami
     * @throws EdmException
     */
    private void getUserIdsFromMPK(OfficeDocument doc, String dictionaryName) throws EdmException {
    	setValue.clear();
	    List<Long> dictionaryIds = (List<Long>) doc.getFieldsManager().getKey(dictionaryName.toUpperCase());
	    if (dictionaryIds != null && !dictionaryIds.isEmpty()) {
		    for(Long ids : dictionaryIds){
		    	CentrumKosztowDlaFaktury mpk = CentrumKosztowDlaFaktury.getInstance().find(ids);
		        String centrumCn = DataBaseEnumField.getEnumItemForTable(CENTRA_KOSZTOW_TABLENAME, mpk.getCentrumId()).getCn();
		        List<String> users = new ArrayList<String>();
		        
		        for (AcceptanceCondition accept : AcceptanceCondition.find(centrumCn, null))
				{
					if (accept.getUsername() != null && !users.contains(accept.getUsername()))
						if(!setValue.contains(accept.getUsername())){
							users.add(accept.getUsername());
							if (!argSets.contains(DSUser.findByUsername(accept.getUsername()).getId())) {
								argSets.add(DSUser.findByUsername(accept.getUsername()).getId());
							}
							setValue.add(accept.getUsername());
						}
				}
		    }
	    }
    }
    
    /**
     * Metoda przepisuje cz�onk�w zarz�du ze s�ownika z dodatkowymi cz�onkami zarz�du do g��wnego s�ownika z cz�onkami.
     * S�ownik z dodatkowymi cz�onkami zarz�du zostaje zresetowany.
     * @param documentId - id dokumentu
     */
    private void moveAdditionalMembersToMain(Long documentId) {
    	try {
    		OfficeDocument doc = OfficeDocument.find(documentId);
    		FieldsManager fm = doc.getDocumentKind().getFieldsManager(documentId);
    		List<Long> mainMembers = (List<Long>) fm.getKey(ZARZAD_DWR_NAME);
    		if (mainMembers == null) {
    			mainMembers = new ArrayList<Long>();
    		}
    		List<Long> additionalMembers = (List<Long>) fm.getKey(ZARZAD_ADD_DWR_NAME);
    		if (additionalMembers != null && !additionalMembers.isEmpty()) {
    			mainMembers.addAll(additionalMembers);
				Map<String, Object> valuesToSet = Maps.newHashMap();
				valuesToSet.put(ZARZAD_DWR_NAME, mainMembers);
				valuesToSet.put(ZARZAD_ADD_DWR_NAME, null);
				fm.getDocumentKind().setOnly(documentId, valuesToSet);
    		}
    	} catch (EdmException e) {
    		log.error(e.getMessage(), e);
    	}
    	
    }

}
