package pl.compan.docusafe.parametrization.zbp.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.parametrization.zbp.FakturaZakupowaLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class AcceptanceTypeDecision implements DecisionHandler {

    private static final long serialVersionUID = 1L;
    private final static Logger LOG = LoggerFactory.getLogger(AcceptanceTypeDecision.class);

    public String decide(OpenExecution openExecution) {
	String acceptanceType = (String) openExecution.getVariable("accType");

	if (acceptanceType != null && !acceptanceType.equals("") && acceptanceType.equals("mpk")) {
	    return FakturaZakupowaLogic.MPK_TASK_NAME;
	} else
	    return FakturaZakupowaLogic.BOARD_TASK_NAME;
    }
}