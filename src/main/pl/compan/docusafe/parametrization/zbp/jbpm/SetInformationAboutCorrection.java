/**
 * 
 */
package pl.compan.docusafe.parametrization.zbp.jbpm;

import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 *
 */
public class SetInformationAboutCorrection implements ExternalActivityBehaviour {
	
	public static final String CORRECTION_FROM_HR_JBMP4_VARIABLE_NAME = "correctionFromHr";

	public void execute(ActivityExecution activityExecution) throws Exception {
		activityExecution.setVariable(CORRECTION_FROM_HR_JBMP4_VARIABLE_NAME, true);
	}

	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
		
	}

}
