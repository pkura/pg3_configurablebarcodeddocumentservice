package pl.compan.docusafe.parametrization.zbp.jbpm;

import java.util.List;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.zbp.FakturaZakupowaLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

@SuppressWarnings("serial")
public class MpkOrZarzad implements DecisionHandler {
    private final static Logger LOG = LoggerFactory.getLogger(MpkOrZarzad.class);

    public String decide(OpenExecution execution) {
	Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");

	try {
	    // boolean isOpened = true;
	    // if (!DSApi.isContextOpen()) {
	    // isOpened = false;
	    // DSApi.openAdmin();
	    // }
	    try {
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();

		@SuppressWarnings("unchecked")
		List<Long> mpk = (List<Long>) fm.getKey(FakturaZakupowaLogic.MPK_FIELD);
		@SuppressWarnings("unchecked")
		List<Long> board = (List<Long>) fm.getKey(FakturaZakupowaLogic.ZARZAD_FIELD);
		if (mpk != null && mpk.size() > 0) {
//		    execution.setVariable("accType", "mpk");
		    return "mpk";
		} else if (board != null && board.size() > 0) {
//		    execution.setVariable("accType", "board");
		    return "board";
		}

	    } finally {
		// if (DSApi.isContextOpen() && !isOpened) {
		// DSApi.close();
		// }
	    }
	} catch (EdmException e) {
	    LOG.error(e.getMessage(), e);
//	    execution.setVariable("accType", "board");
	    return "error";
	}
//	execution.setVariable("accType", "board");
	return "error";
    }
}
