/**
 * @author Damian Zych <damian.zych@docusafe.pl>
 *
 */
package pl.compan.docusafe.parametrization.zbp.hb;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.jfree.util.Log;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

import com.google.common.collect.Lists;

public class DelegationCosts {
	
	public DelegationCosts() {}
	
	private String Id;
	private String fromPlace;
	private String toPlace;
	private Date toDate;
	private Date fromDate;
	private String transport;
	private String opis;
	private BigDecimal koszt;
	private String listaZalacznikow;

	public static Integer[] findFieldVal(Long documentId) throws EdmException {
		List<String> fieldValsString = Lists.newArrayList();
		fieldValsString = DSApi.context().session().createSQLQuery("select field_val from dsg_zbp_delegacja_multiple where document_id = :docId AND field_cn = 'KOSZTY_PODROZY'")
			.setParameter("docId", documentId)
			.list();
		Integer[] fieldVals = new Integer[fieldValsString.size()];
		for (int i = 0; i < fieldVals.length; i++) {
			fieldVals[i] = Integer.valueOf(fieldValsString.get(i));
		}
		return fieldVals;
	}
	
	public static String findId(Integer id) throws EdmException {
	
		String fieldVals =(String) DSApi.context().session().createSQLQuery("select ID from DSG_ZBP_DEL_KOSZTY_PODROZY where ID = :id")
				.setParameter("id", id).uniqueResult();
		return fieldVals;
	}

	public static String findFromPlace(Integer id) throws EdmException{

		String fromPlace = (String) DSApi.context().session().createSQLQuery("select FROM_PLACE from DSG_ZBP_DEL_KOSZTY_PODROZY where ID = :id")
				.setParameter("id", id).uniqueResult();
		return fromPlace;
	}

	public static String findToPlace(Integer id) throws EdmException{
		
		String toPlace = (String) DSApi.context().session().createSQLQuery("select TO_PLACE from DSG_ZBP_DEL_KOSZTY_PODROZY where ID = :id")
				.setParameter("id", id).uniqueResult();
		return toPlace;
	}

	public static Date findFromDate(Integer id) throws EdmException{

		Date fromDate = (Date) DSApi.context().session().createSQLQuery("select FROM_DATE from DSG_ZBP_DEL_KOSZTY_PODROZY where ID = :id")
				.setParameter("id", id).uniqueResult();
		return fromDate;
	}

	public static Date findToDate(Integer id) throws EdmException{

		Date toDate = (Date) DSApi.context().session().createSQLQuery("select TO_DATE from DSG_ZBP_DEL_KOSZTY_PODROZY where ID = :id")
				.setParameter("id", id).uniqueResult();
		return toDate;
	}
	
	public static String findTransport(Integer id) throws EdmException{

		BigDecimal vehicleNr = (BigDecimal) DSApi.context().session().createSQLQuery("select TRANSPORT from DSG_ZBP_DEL_KOSZTY_PODROZY where ID = :id")
				.setParameter("id", id).uniqueResult();
		String transport = (String) DSApi.context().session().createSQLQuery("select TITLE from DSG_ZBP_DEL_SROD_LOK where ID = :id")
				.setParameter("id", vehicleNr.intValue()).uniqueResult();
		return transport;
	}

	public static String findOpis(Integer id) throws EdmException{

		String opis = (String) DSApi.context().session().createSQLQuery("select OPIS from DSG_ZBP_DEL_KOSZTY_PODROZY where ID = :id")
				.setParameter("id", id).uniqueResult();
		return opis;
	}

	public static BigDecimal findKoszt(Integer id) throws EdmException{

		BigDecimal koszt = (BigDecimal) DSApi.context().session().createSQLQuery("select KOSZT from DSG_ZBP_DEL_KOSZTY_PODROZY where ID = :id")
				.setParameter("id", id).uniqueResult();
		return koszt;
	}
	
	public static String findAttachmentNames(Long documentId)
	{
		List<BigDecimal> idsBigDecimal = Lists.newArrayList();
		String names = "";
		try{
			idsBigDecimal = DSApi.context().session().createSQLQuery("select ID from DS_ATTACHMENT where DOCUMENT_ID = :docId")
			.setParameter("docId", documentId)
			.list();
		}catch(Exception e){
			Log.error(e.getMessage());
		}

		if(idsBigDecimal.isEmpty())
			return names;
	
		Integer[] ids = new Integer[idsBigDecimal.size()];
		for (int i = 0; i < ids.length; i++) {
			ids[i] = idsBigDecimal.get(i).intValue();
		}

		List<String> nameList = Lists.newArrayList();  
		try{
		nameList = DSApi.context().session().createSQLQuery("select ORIGINALFILENAME from DS_ATTACHMENT_REVISION where ID in ( :ids )")
				.setParameterList("ids", ids).list();
		}catch(Exception e){
			Log.error(e.getMessage());
		}

		for(String name : nameList)
			names += name + ", ";
		return names.length() < 2 ? names : names.substring(0, names.length() - 2);		
	}

	public int compareTo(DelegationCosts o) {
		if (getFromDate() == null || o.getFromDate() == null)
		      return 0;
		    return getFromDate().compareTo(o.getFromDate());
	}	
	
	public Date getFromDate() {
		return fromDate;
	}
	public String getFromPlace() {
		return fromPlace;
	}
	public BigDecimal getKoszt() {
		return koszt;
	}
	public String getId() {
		return Id;
	}
	public String getOpis() {
		return opis;
	}
	public String getToPlace() {
		return toPlace;
	}
	public Date getToDate() {
		return toDate;
	}
	public String getTransport() {
		return transport;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public void setFromPlace(String fromPlace) {
		this.fromPlace = fromPlace;
	}
	public void setId(String id) {
		Id = id;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public void setToPlace(String toPlace) {
		this.toPlace = toPlace;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public void setKoszt(BigDecimal koszt) {
		this.koszt = koszt;
	}
	public void setTransport(String transport) {
		this.transport = transport;
	}	
	public String getListaZalacznikow() {
		return listaZalacznikow;
	}
	public void setListaZalacznikow(String listaZalacznikow) {
		this.listaZalacznikow = listaZalacznikow;
	}
}
