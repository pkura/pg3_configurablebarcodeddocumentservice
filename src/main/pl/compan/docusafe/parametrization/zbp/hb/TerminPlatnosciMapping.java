package pl.compan.docusafe.parametrization.zbp.hb;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class TerminPlatnosciMapping {

    private static Logger LOG = LoggerFactory.getLogger(TerminPlatnosciMapping.class);

    private long documentId;
    private Date terminPlatnosci;

    public TerminPlatnosciMapping() {
    }

    public TerminPlatnosciMapping(long documentId, Date terminPlatnosci) {
	super();
	this.documentId = documentId;
	this.terminPlatnosci = terminPlatnosci;
    }

    @SuppressWarnings("unchecked")
    public static List<TerminPlatnosciMapping> list() throws EdmException {
	return DSApi.context().session().createCriteria(TerminPlatnosciMapping.class).list();
    }

    public static TerminPlatnosciMapping findByDocumentId(Long id) throws EdmException {
	Criteria criteria = DSApi.context().session().createCriteria(TerminPlatnosciMapping.class);

	criteria.add(Restrictions.eq("documentId", id));
	criteria.setMaxResults(1);

	return (TerminPlatnosciMapping) criteria.list().get(0);
    }

    public void save() throws EdmException {
	try {
	    DSApi.context().session().save(this);
	} catch (HibernateException e) {
	    throw new EdmHibernateException(e);
	}
    }

    public void delete() throws EdmException {
	DSApi.context().session().delete(this);
    }

    public long getDocumentId() {
	return documentId;
    }

    public void setDocumentId(long documentId) {
	this.documentId = documentId;
    }

    public Date getTerminPlatnosci() {
	return terminPlatnosci;
    }

    public void setTerminPlatnosci(Date terminPlatnosci) {
	this.terminPlatnosci = terminPlatnosci;
    }

}
