package pl.compan.docusafe.parametrization.zbp;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContextFactory;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.FolderResolver;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.EnumItemImpl;
import pl.compan.docusafe.core.dockinds.field.FieldValue;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.parametrization.zbp.hb.DelegationCosts;
import pl.compan.docusafe.parametrization.zbp.hb.MPK;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;


public class DelegacjaLogic extends AbstractDocumentLogic {

    private static final long serialVersionUID = 1L;
    private static final int MONEY_SCALE = 2;
    public static final String SUPERVISOR = "immediate_supervisor";
    public static final String SUPERVISOR_ACCEPT = "supervisor_accept";
    public static final String DSG_ZBP_DEL_SROD_LOK = "dsg_zbp_del_srod_lok";

    public static final String DELEGOWANY = "DELEGOWANY";
    public static final String DELEGACJA_CN = "delegacja";
    public static final String DELEGOWANY_FIELD = "DELEGOWANY";

    public static final String ZESPOL_ACCEPTATION_CN = "zespol";
    public static final String ZESPOL_FIELD = "ZESPOL";
    public static final String ZESPOL_TABLENAME = "dsg_zbp_delegacja_zespol";

    public static final String DATE_FROM_FIELD_NAME = "DATE_FROM";
    public static final String DATE_TO_FIELD_NAME = "DATE_TO";
    public static final String DWR_PREFIX = "DWR_";
    public static final String CZLONEK_ZARZADU_FIELD_NAME = "CZLONEK_ZARZADU";


    public static final String STATUS_FIELD_NAME = "STATUS";
    public static final int PROCESS_CREATION_ID = 10;
    public static final int RE_OWNER_BUDGET_ACCEPT_STATUS_ID = 210;

    public static final String NIEPOPRAWNE_DATY= "Termin powrotu nie mo�e by� wcze�niejszy od terminu wyjazdu";
    public static final String WYBRANYM_CZLONKIEM_ZARZADU_JEST_ZALOGOWANY_UZYTKOWNIK = "Wybranym cz�onkiem zarz�du jest zalogowany u�ytkownik";
    
    public static final String PLANE_ID = "9";
    public static final String TRAIN_ID = "10";

    public static final String ID = "ID";
    public static final String FIELDS = "fields";
    public static final String DATE_FROM = "DATE_FROM";
    public static final String DATE_TO = "DATE_TO";
    public static final String SRODEK_LOKOMOCJI = "SRODEK_LOKOMOCJI";
    public static final String ZALICZKA = "ZALICZKA";
    public static final String PRZEW_KOSZT_DELEGACJI = "PRZEW_KOSZT_DELEGACJI";
    public static final String ZESPOL = "ZESPOL";
    public static final String RYCZALT_COST_TO_SUM = "RYCZALT_COST_TO_SUM";
    public static final String NOCLEGI_RYCZALT = "NOCLEGI_RYCZ_COST_TO_SUM";
    public static final String NOCLEGI_WG_RACHUNKOW =  "NOCLEGI_RACH_COST_TO_SUM";
    public static final String DIETY =  "DIETY_COST_TO_SUM";
    public static final String INNE_WYDATKI_WG_ZALACZNIKOW =  "INNE_WYD_COST_TO_SUM";
    public static final String KWOTA_OGOLEM_WYDANA =  "SUMA";    
    public static final String DO_ZWROTU =  "DO_ZWROTU";
    public static final String DO_WYPLATY =  "DO_WYPLATY";
    public static final String CZLONEK_ZARZADU =  "CZLONEK_ZARZADU";
    public static final String LISTA_ZALACZNIKOW =  "LISTA_ZALACZNIKOW";
    public static final String DELEGATION_COSTS =  "DELEGATION_COSTS";
    
    
    private static DelegacjaLogic instance;
    private Logger log = LoggerFactory.getLogger(DelegacjaLogic.class);

    public static DelegacjaLogic getInstance() {
	if (instance == null)
			instance = new DelegacjaLogic();
		return instance;
	}

    @Override
    public void documentPermissions(Document document) throws EdmException {
	java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
	String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
	String documentKindName = document.getDocumentKind().getName();

	perms.add(new PermissionBean(ObjectPermission.READ, documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, documentKindName + " - odczyt"));
	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, documentKindName + " zalacznik - odczyt"));
	perms.add(new PermissionBean(ObjectPermission.MODIFY, documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, documentKindName + " - modyfikacja"));
	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, documentKindName + " zalacznik - modyfikacja"));
	perms.add(new PermissionBean(ObjectPermission.DELETE, documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, documentKindName + " - usuwanie"));
	Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
	perms.addAll(documentPermissions);
	this.setUpPermission(document, perms);
    }


    @Override
    public void setAdditionalTemplateValues(long docId, Map<String, Object> values) {
    	try {
    		Document doc = Document.find(docId);
    		FieldsManager fm = doc.getFieldsManager();
    		
    		// umieszczenie daty generacji dokumentu
    		values.put(ID, docId);
    		
    		// umieszczenie w values podstawowych informacji z dokumentu
    		
    		putDataFromDocumentToValues(fm, values);
    		    		
    		List<DelegationCosts> costList = getDelegationCosts(docId);
    		if (costList != null && !costList.isEmpty()) {
    			java.util.Collections.sort(costList, new java.util.Comparator<DelegationCosts>(){
    				 public int compare(DelegationCosts o1, DelegationCosts o2) {
    					 if(o1.getFromDate() == null || o2.getFromDate() == null)
    						 return 0;
    		                return o1.compareTo(o2);
    		            }
    			}    					
    					);
    			values.put(DELEGATION_COSTS, costList);
    		}
    		
    		for (DocumentAcceptance dacc : DocumentAcceptance.find(docId)) {
    			DSUser acceptedUser = DSUser.findByUsername(dacc.getUsername());
				values.put(dacc.getAcceptanceCn(), acceptedUser.getLastname() + " " + acceptedUser.getFirstname());
				values.put(dacc.getAcceptanceCn() + "_DATE", DateUtils.formatCommonDate(dacc.getAcceptanceTime()));
    		}        	
    		values.put(LISTA_ZALACZNIKOW , DelegationCosts.findAttachmentNames(docId));
    	} catch (EdmException e) {
    		log.error(e.getMessage(), e);
    	}
    }

    
    private List<DelegationCosts> getDelegationCosts(Long documentId) throws EdmException {    	

    	List<DelegationCosts> costsList = new ArrayList<DelegationCosts>();
    	Integer[] fieldVals = DelegationCosts.findFieldVal(documentId);

    	for(int i = 0; i < fieldVals.length; i++)
    	{
    		Integer id = fieldVals[i];
    		DelegationCosts dc = new DelegationCosts();
    		dc.setFromDate(DelegationCosts.findFromDate(id));
    		dc.setToDate(DelegationCosts.findToDate(id));    		
    		dc.setFromPlace(DelegationCosts.findFromPlace(id));
    		dc.setToPlace(DelegationCosts.findToPlace(id));   
    		dc.setTransport(DelegationCosts.findTransport(id));
    		dc.setOpis(DelegationCosts.findOpis(id));
    		dc.setKoszt(DelegationCosts.findKoszt(id));
    		costsList.add(dc);
    	}    	
		return costsList;
    }
    
    private void putDataFromDocumentToValues(FieldsManager fm, Map<String, Object> values) throws EdmException {
    	Map<String, Object> fieldValues = (Map<String, Object>) values.get(FIELDS);
    	if (fieldValues.containsKey(DATE_FROM) && fieldValues.get(DATE_FROM) != null) {
    		String date = DateUtils.formatCommonDate((Date) fieldValues.get(DATE_FROM));
    		values.put(DATE_FROM, date);
    	}
    	if (fieldValues.containsKey(DATE_TO) && fieldValues.get(DATE_TO) != null) {
    		String date = DateUtils.formatCommonDate((Date) fieldValues.get(DATE_TO));
    		values.put(DATE_TO, date);
    	}
    	if (fieldValues.containsKey(DELEGOWANY) && fieldValues.get(DELEGOWANY) != null) {
    		EnumItemImpl eii = (EnumItemImpl) fieldValues.get(DELEGOWANY);
    		String delegowany = eii.getTitle();
    		values.put(DELEGOWANY, delegowany);
    	}
    	if (fieldValues.containsKey(SRODEK_LOKOMOCJI) && fieldValues.get(SRODEK_LOKOMOCJI) != null) {
    		EnumItemImpl eii = (EnumItemImpl) fieldValues.get(SRODEK_LOKOMOCJI);
    		String zespol = eii.getTitle();
    		values.put(SRODEK_LOKOMOCJI, zespol);
    	}
    	if (fieldValues.containsKey(ZESPOL) && fieldValues.get(ZESPOL) != null) {
    		EnumItemImpl eii = (EnumItemImpl) fieldValues.get(ZESPOL);
    		String zespol = eii.getTitle();
    		values.put(ZESPOL, zespol);
    	}
    	try{
    	if (fieldValues.containsKey(CZLONEK_ZARZADU) &&  fieldValues.get(CZLONEK_ZARZADU) != null) {

    		EnumItemImpl eii = (EnumItemImpl) fieldValues.get(CZLONEK_ZARZADU);
    		String czlonek = eii.getTitle();
    		values.put(CZLONEK_ZARZADU, czlonek);
    	}
    	}catch(Exception e)
    	{
    		log.error(e.getMessage(), e);
    		values.put(CZLONEK_ZARZADU, "");
    	}
    	if (fieldValues.containsKey(ZALICZKA)) {
    		if(fieldValues.get(ZALICZKA) != null){
    		BigDecimal grossAmount = (BigDecimal) fieldValues.get(ZALICZKA);
    		grossAmount = grossAmount.setScale(MONEY_SCALE);
    		values.put(ZALICZKA, grossAmount);
    		}else{
        		values.put(ZALICZKA, new BigDecimal(0).setScale(MONEY_SCALE));
    		}
    	}
    	if (fieldValues.containsKey(PRZEW_KOSZT_DELEGACJI)) {
    		if(fieldValues.get(PRZEW_KOSZT_DELEGACJI) != null){
    		BigDecimal grossAmount = (BigDecimal) fieldValues.get(PRZEW_KOSZT_DELEGACJI);
    		grossAmount = grossAmount.setScale(MONEY_SCALE);
    		values.put(PRZEW_KOSZT_DELEGACJI, grossAmount);
    		}else{
        		values.put(PRZEW_KOSZT_DELEGACJI, new BigDecimal(0).setScale(MONEY_SCALE));
    		}
    	}
    	if (fieldValues.containsKey(RYCZALT_COST_TO_SUM)) {
    		if (fieldValues.get(RYCZALT_COST_TO_SUM) != null){
    		BigDecimal ryczaltZaDojazd = (BigDecimal) fieldValues.get(RYCZALT_COST_TO_SUM);
    		values.put(RYCZALT_COST_TO_SUM, ryczaltZaDojazd.setScale(MONEY_SCALE));
    		}else{
        		values.put(RYCZALT_COST_TO_SUM, new BigDecimal(0).setScale(MONEY_SCALE));
    		}
    	}
    	if (fieldValues.containsKey(NOCLEGI_RYCZALT)) {
    		if(fieldValues.get(NOCLEGI_RYCZALT) != null){
    		BigDecimal noclegiRyczalt = (BigDecimal) fieldValues.get(NOCLEGI_RYCZALT);
    		values.put(NOCLEGI_RYCZALT, noclegiRyczalt.setScale(MONEY_SCALE));
    		}else{
    			values.put(NOCLEGI_RYCZALT, new BigDecimal(0).setScale(MONEY_SCALE));
    		}
    	}
    	if (fieldValues.containsKey(NOCLEGI_WG_RACHUNKOW)) {
    		if(fieldValues.get(NOCLEGI_WG_RACHUNKOW) != null){
    		BigDecimal noclegi = (BigDecimal) fieldValues.get(NOCLEGI_WG_RACHUNKOW);
    		values.put(NOCLEGI_WG_RACHUNKOW, noclegi.setScale(MONEY_SCALE));
    		}else{
    			values.put(NOCLEGI_WG_RACHUNKOW, new BigDecimal(0).setScale(MONEY_SCALE));
    		}
    	}
    	if (fieldValues.containsKey(DO_WYPLATY)) {
    		if( fieldValues.get(DO_WYPLATY) != null){
    		BigDecimal doWyplaty = (BigDecimal) fieldValues.get(DO_WYPLATY);
    		values.put(DO_WYPLATY, doWyplaty.setScale(MONEY_SCALE));
    		}else{
    			values.put(DO_WYPLATY, new BigDecimal(0).setScale(MONEY_SCALE));
    		}
    	}
    	if (fieldValues.containsKey(DO_ZWROTU)) {
    		if(fieldValues.get(DO_ZWROTU) != null){
    		BigDecimal doZwrotu = (BigDecimal) fieldValues.get(DO_ZWROTU);
    		values.put(DO_ZWROTU, doZwrotu.setScale(MONEY_SCALE));
    		}else{
    			values.put(DO_ZWROTU, new BigDecimal(0).setScale(MONEY_SCALE));
    		}
    	}
    	if (fieldValues.containsKey(KWOTA_OGOLEM_WYDANA)) {
    		if(fieldValues.get(KWOTA_OGOLEM_WYDANA) != null){
    		BigDecimal suma = (BigDecimal) fieldValues.get(KWOTA_OGOLEM_WYDANA);
    		values.put(KWOTA_OGOLEM_WYDANA, suma.setScale(MONEY_SCALE));
    		}else{
    			values.put(KWOTA_OGOLEM_WYDANA, new BigDecimal(0).setScale(MONEY_SCALE));
    		}
    	}
    	if (fieldValues.containsKey(INNE_WYDATKI_WG_ZALACZNIKOW)) {
    		if(fieldValues.get(INNE_WYDATKI_WG_ZALACZNIKOW) != null){
    		BigDecimal inne = (BigDecimal) fieldValues.get(INNE_WYDATKI_WG_ZALACZNIKOW);
    		values.put(INNE_WYDATKI_WG_ZALACZNIKOW, inne.setScale(MONEY_SCALE));
    		}else{
    			values.put(INNE_WYDATKI_WG_ZALACZNIKOW, new BigDecimal(0).setScale(MONEY_SCALE));
    		}
    	}
    	if (fieldValues.containsKey(DIETY)) {
    		if(fieldValues.get(DIETY) != null){
    		BigDecimal diety = (BigDecimal) fieldValues.get(DIETY);
    		values.put(DIETY, diety.setScale(MONEY_SCALE));
    		}else{
    			values.put(DIETY, new BigDecimal(0).setScale(MONEY_SCALE));
    		}
    	}
    }

    
    @Override
    public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException {
        if (doc != null) {

            FieldsManager fm = doc.getFieldsManager();

            if (isStatus(fm, PROCESS_CREATION_ID, false)) {
                Date startDate = (Date) fm.getKey(DATE_FROM_FIELD_NAME) ;
                Date finishDate = (Date) fm.getKey(DATE_TO_FIELD_NAME);

                if (startDate != null && finishDate != null && finishDate.before(startDate))
                    throw new EdmException(NIEPOPRAWNE_DATY);
            }

            if (isStatus(fm, RE_OWNER_BUDGET_ACCEPT_STATUS_ID, false)) {
                Integer chosenMemberId = null;
                Long loggedUserId = null;

                try {
                    chosenMemberId = (Integer) fm.getKey(CZLONEK_ZARZADU_FIELD_NAME); // todo ? warto�� pola dla u�ytkownika jest typu Integer, nie Long...
                    loggedUserId = DSApi.context().getDSUser().getId();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }

                if (loggedUserId != null && chosenMemberId != null && loggedUserId.equals((long)chosenMemberId))
                    throw new EdmException(WYBRANYM_CZLONKIEM_ZARZADU_JEST_ZALOGOWANY_UZYTKOWNIK);
            }
        }
    }

	public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
	{
        if(!validateDwrDates(values, fm))
            return new Field("messageField", NIEPOPRAWNE_DATY, Field.Type.BOOLEAN);

        if(!validateDwrChosenMemberOfBoard(values, fm))
            return new Field("messageField", WYBRANYM_CZLONKIEM_ZARZADU_JEST_ZALOGOWANY_UZYTKOWNIK, Field.Type.BOOLEAN);

        try
		{
			BigDecimal suma = new BigDecimal(0);
			if (values.get("DWR_KOSZTY_PODROZY") != null)
			{
				Map<String, FieldData> kosztyPodrozy = values.get("DWR_KOSZTY_PODROZY").getDictionaryData();
				if (kosztyPodrozy != null) {
					String suffix = null;
					String transportCN = null;
					String kosztCN = null;
 					for (String cn : kosztyPodrozy.keySet()) {
						if (cn.contains("OPIS") && kosztyPodrozy.get(cn).getData() != null) {
							suffix = cn.substring(cn.lastIndexOf("_"));
							transportCN = "KOSZTY_PODROZY" + "_TRANSPORT".concat(suffix);
							kosztCN = "KOSZTY_PODROZY" + "_KOSZT".concat(suffix);
							if (kosztyPodrozy.get(transportCN) != null && kosztyPodrozy.get(transportCN).getData() != null) {
								String selectedVatId = kosztyPodrozy.get(transportCN).getEnumValuesData().getSelectedId();
								if (!selectedVatId.equals(PLANE_ID) && !selectedVatId.equals(TRAIN_ID)) {
									BigDecimal stawka = new BigDecimal(Docusafe.getAdditionProperty(DataBaseEnumField.getEnumItemForTable("dsg_zbp_del_srod_lok", Integer.valueOf(selectedVatId)).getCn()));
									BigDecimal km = new BigDecimal(kosztyPodrozy.get(cn).toString());
									BigDecimal koszt = km.multiply(stawka).setScale(2, RoundingMode.HALF_DOWN);
									suma = suma.add(koszt);
									values.get("DWR_SUMA").setMoneyData(suma);
								}
							}
						}
						if (cn.contains("TRANSPORT") && kosztyPodrozy.get(cn).getData() != null) {
							suffix = cn.substring(cn.lastIndexOf("_"));
							kosztCN = "KOSZTY_PODROZY" + "_KOSZT".concat(suffix);
							String selectedVatId = kosztyPodrozy.get(cn).getEnumValuesData().getSelectedId();
							if ((selectedVatId.equals(PLANE_ID) || selectedVatId.equals(TRAIN_ID)) && (kosztyPodrozy.get(kosztCN) != null && kosztyPodrozy.get(kosztCN).getData() != null)) {
								suma = suma.add(new BigDecimal(kosztyPodrozy.get(kosztCN).toString()));
								values.get("DWR_SUMA").setMoneyData(suma);
							}
						}
					}
				}

				for (String cn : values.keySet())
				{
					if (cn.contains("COST_TO_SUM") && values.get(cn).getData() != null)
						suma = suma.add(values.get(cn).getMoneyData());
				}

				values.get("DWR_SUMA").setMoneyData(suma);
			}

			if (values.get("DWR_ZALICZKA") != null) {
				if (values.get("DWR_SUMA") != null && values.get("DWR_SUMA").getData() != null) {
					suma = values.get("DWR_ZALICZKA").getMoneyData().subtract(values.get("DWR_SUMA").getMoneyData());
					if (suma.compareTo(BigDecimal.ZERO) == 1) {
						values.get("DWR_DO_ZWROTU").setMoneyData(suma.abs().setScale(2, RoundingMode.HALF_UP));
						values.get("DWR_DO_WYPLATY").setMoneyData(BigDecimal.ZERO);
					} else {
						values.get("DWR_DO_WYPLATY").setMoneyData(suma.abs().setScale(2, RoundingMode.HALF_UP));
						values.get("DWR_DO_ZWROTU").setMoneyData(BigDecimal.ZERO);
					}
				}
			}

		} catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    private boolean validateDwrDates(Map<String, FieldData> values, FieldsManager fm) {
        if (isStatus(fm, PROCESS_CREATION_ID, false)) {
            try {
                if (isEditable(fm, DATE_FROM_FIELD_NAME) && isEditable(fm, DATE_TO_FIELD_NAME)) {
                    Date startDate = values.get(DWR_PREFIX + DATE_FROM_FIELD_NAME).getDateData();
                    Date finishDate = values.get(DWR_PREFIX + DATE_TO_FIELD_NAME).getDateData();

                    if (startDate != null && finishDate != null && finishDate.before(startDate))
                        return false;
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return true;
    }

    private boolean isEditable(FieldsManager fm, String fieldCn) {
        try {
            pl.compan.docusafe.core.dockinds.field.Field field = fm.getField(fieldCn);
            return isEditable(field);
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isEditable(pl.compan.docusafe.core.dockinds.field.Field field) {
        return field != null && !field.isReadOnly() && !field.isDisabled();
    }

    private boolean validateDwrChosenMemberOfBoard(Map<String, FieldData> values, FieldsManager fm) {
        try {
            if (isStatus(fm, RE_OWNER_BUDGET_ACCEPT_STATUS_ID, false)) {
                String chosenMemberIdString = values.get(DWR_PREFIX + CZLONEK_ZARZADU_FIELD_NAME).getEnumValuesData().getSelectedId();
                if (StringUtils.isNotEmpty(chosenMemberIdString)) {
                    Long chosenMemberId = Long.parseLong(chosenMemberIdString);

                    return !isLoggedUser(chosenMemberId);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return true;
    }

    private boolean isStatus (FieldsManager fm, int statusId, boolean nullStatusOk) {
        try {
            EnumItem statusEnum = fm.getEnumItem(STATUS_FIELD_NAME);
            if (statusEnum == null)
                return nullStatusOk;
            Integer curStatusId = statusEnum.getId();
            if (curStatusId.equals(statusId))
                return true;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return false;
    }

    /** Open context as logged user */
    private boolean isLoggedUser(Long chosenMemberId) {
        Long loggedUserId = null;

        try {
            HttpServletRequest req = WebContextFactory.get().getHttpServletRequest();
            DSApi.open(AuthUtil.getSubject(req));
            DSUser loggedUser = DSApi.context().getDSUser();
            loggedUserId = loggedUser.getId();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            DSApi._close();
        }

        return chosenMemberId != null && loggedUserId != null && chosenMemberId.equals(loggedUserId);
    }

	public void onStartProcess(OfficeDocument document, ActionEvent event) {
			log.info("--- DelegacjaLogic : START PROCESS !!! ---- {}", event);
			Statement stmt = null;
			ResultSet rs = null;
			String year = now("yyyy").toString();
			String sql = "select COUNT(1) from dsg_zbp_delegacja del  join ds_document doc on del.document_id = doc.id " +
						 "where doc.ctime between '" + year + "-01-01' and '" + year + "-12-31'";
			Integer i = 0;

			try{
				stmt = DSApi.context().createStatement();
				rs = stmt.executeQuery(sql);

				if(rs.next()){
					i =rs.getInt(1);
				}

				StringBuilder sb = new StringBuilder("");
				sb.append(i).append("/").append(now("MM/yyyy"));
				
				Map<String, Object> map = Maps.newHashMap();
			    map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			    map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			    document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

			    Map<String, Object> fieldValues = new HashMap<String, Object>();
				fieldValues.put("NUMER_DELEGACJI", sb.toString());
				document.getDocumentKind().setOnly(document.getId(), fieldValues);

				DSApi.context().watch(URN.create(document));

				if (AvailabilityManager.isAvailable("addToWatch")) {
    				DSApi.context().watch(URN.create(document));
    			}

			}catch(Exception e){
				log.error(e.getMessage(), e);
			}
			finally
	    	{
	    		DbUtils.closeQuietly(rs);
	            DSApi.context().closeStatement(stmt);
	    	}
		}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
		Map<String, Object> toReload = Maps.newHashMap();
		for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields()) {
			if (f.getDefaultValue() != null) {
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
			}
		}
		log.info("toReload = {}", toReload);

		toReload.put(DELEGOWANY_FIELD, DSApi.context().getDSUser().getId());
		fm.reloadValues(toReload);
	}

    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
	boolean assigned = false;
	Long docId = doc.getId();

		/**
	     * Akceptacja delegacji przez bezposredniego przelozonego
	     */
		/*if(SUPERVISOR.equals(acceptationCn.toLowerCase())){
	        	List<String> users = new ArrayList<String>();

				try {
					Long docId = doc.getId();
					FieldsManager fm = doc.getDocumentKind().getFieldsManager(docId);
				    String delegowany = fm.getKey(DELEGOWANY_FIELD).toString();//getValue(DELEGOWANY_FIELD).toString().split(" ");
				    DSUser dsUser = DSUser.findById(Long.valueOf(delegowany));
				    String divGuid = dsUser.getDivisionsGuid()[0];
					DSUser[] usersFromDiv = DSDivision.find(divGuid).getUsers();

					for(int i = 0; i < usersFromDiv.length; i++){
						if(usersFromDiv[i].isSupervisor())
							users.add(usersFromDiv[i].getName());
					}

					for (String user : users)
					{
						assignable.addCandidateUser(user);
						AssigneeHandler.addToHistory(openExecution, doc, user, null);
						assigned = true;
					}
				} catch (UserNotFoundException e) {
					log.error(e.getMessage(), e);
				} catch (EdmException e) {
					log.error(e.getMessage(), e);
				}
		}*/
        if (ZESPOL_ACCEPTATION_CN.equals(acceptationCn.toLowerCase())) {
            try {
                Integer zespolId = (Integer) doc.getFieldsManager().getKey(ZESPOL_FIELD);
                EnumItem e = DataBaseEnumField.getEnumItemForTable(ZESPOL_TABLENAME, zespolId);
                if(e.getRefValue() != null){
                    if(e.getRefValue().contains(",")){
                        String[] users = e.getRefValue().split(",");
                        Long[] usersId = new Long[users.length];
                        String[] userName = new String[users.length];
                        for(int i = 0; i< users.length; i++){
                            usersId[i] = Long.valueOf(users[i]);
                            userName[i] = DSUser.findById(Long.valueOf(users[i])).getName();

                        }
                        for(int i = 0; i< userName.length; i++){
                            assignable.addCandidateUser(userName[i]);
                            assigned = true;
                            AssigneeHandler.addToHistory(openExecution, doc, userName[i], null);
                        }
                    }else{
                        Long userId = Long.valueOf(e.getRefValue());
                        String username = DSUser.findById(userId).getName();
                        assignable.addCandidateUser(username);
                        assigned = true;
                        AssigneeHandler.addToHistory(openExecution, doc, username, null);
                    }
                }
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
            }
        }
        else if (SUPERVISOR_ACCEPT.equals(acceptationCn.toLowerCase())) {
            try {
                FieldsManager fm = Document.find(docId).getFieldsManager();
    			Integer delegowanyId = (Integer)fm.getKey(DELEGOWANY_FIELD);
    			if(delegowanyId.equals(2)){//jfurga
    				assignable.addCandidateUser(DSUser.findById(delegowanyId.longValue()).getName());
                    assigned = true;
                    AssigneeHandler.addToHistory(openExecution, doc, DSUser.findById(delegowanyId.longValue()).getName(), null);
    			}else{
    				pl.compan.docusafe.core.users.sql.UserImpl userImpl = pl.compan.docusafe.core.users.sql.UserImpl.find(delegowanyId.longValue());
                	Set<UserImpl> supervisorSet = userImpl.getSupervisors();
                	for(UserImpl supervisor : supervisorSet){
                		 assignable.addCandidateUser(supervisor.getName());
                         assigned = true;
                         AssigneeHandler.addToHistory(openExecution, doc, supervisor.getName(), null);
                	}
    			}
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
            }
        }
	return assigned;
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
	FolderResolver fr = document.getDocumentKind().getDockindInfo().getFolderResolver();
	if (fr != null)
	{
		as.useFolderResolver(document);
	}
    }

	@Override
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues, Long documentId) {
		Map<String, Object> result = new HashMap<String, Object>();

		try {
			String selectedVatId = values.get("KOSZTY_PODROZY_TRANSPORT").getEnumValuesData().getSelectedId();
			if (values.get("KOSZTY_PODROZY_OPIS") != null && (selectedVatId != null && !selectedVatId.equals(""))) {
				String cn = DataBaseEnumField.getEnumItemForTable(DSG_ZBP_DEL_SROD_LOK, Integer.valueOf(selectedVatId)).getCn();
				BigDecimal stawka = new BigDecimal(Docusafe.getAdditionProperty(cn));
				BigDecimal km = new BigDecimal(values.get("KOSZTY_PODROZY_OPIS").toString());
				BigDecimal koszt = km.multiply(stawka).setScale(2, RoundingMode.HALF_DOWN);
				result.put("KOSZTY_PODROZY_KOSZT", koszt);
			}

		} catch (NumberFormatException e) {
			log.error(e.getMessage(), e);
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}
		return result;
	}

    public String now(String dateFormat) {
    	Calendar cal = Calendar.getInstance();
    	SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
    	return sdf.format(cal.getTime());
    	}

}
