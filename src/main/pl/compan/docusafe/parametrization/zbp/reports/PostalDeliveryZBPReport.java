package pl.compan.docusafe.parametrization.zbp.reports;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeMap;

import com.asprise.util.tiff.W;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.parametrization.ic.reports.ReportUtilsIC;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.PDFDumper;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.DateUtils;


public class PostalDeliveryZBPReport extends Report
{	
	private static final String KG_KOSZT = "kg_koszt";
	private static final String KW_KOSZT = "kw_koszt";
	private static final String STAMPFEE = "stampfee";
	private static final String WYDAWN = "WYDAWN";
	private static final String CIG = "CIG";
	private static final String AMRON = "AMRON";
	private static final String CEM = "CEM";
	private static final String ZARZAD = "ZARZAD";
	private static final String PRAWN = "PRAWN";
	private static final String SWD = "SWD";
	private static final String SUMA = "SUMA";
	private static final String KANCELARIA = "kancelaria";
	
	UniversalTableDumper dumper = new UniversalTableDumper();
	private Date dateFrom;
    private Date dateTo;
    private Statement stmtGlowna;
    private Statement stmtWydawnictwo;

    
	@Override
	public void initParametersAvailableValues() throws ReportException
	{
		super.initParametersAvailableValues();
		try
		{
			for(ReportParameter rp:params)
			{
				if(rp.getFieldCn().equals("project"))
					rp.setAvailableValues(ReportUtilsIC.getProjectsMap(DSApi.context().getDSUser()));
			}
		}catch (Exception e) {
			throw new ReportException(e);
		}
	}
	
	private void initValues() throws Exception {
        for (ReportParameter param : getParams()) {
            if (param.getType().equals("break-line")) {
                continue;
            }

            if (param.getFieldCn().equals("dateFrom") && param.getValue() != null) {
                dateFrom = DateUtils.jsDateFormat.parse((String) param.getValue());
            }

            if (param.getFieldCn().equals("dateTo") && param.getValue() != null) {
                dateTo = DateUtils.jsDateFormat.parse((String) param.getValue());
            }

        }
    }

	private ResultSet returnResultSet(Statement stmt, String koszt, Date dateFrom, Date dateTo) throws Exception, SQLException{
		java.sql.Date sqlDateFrom = new java.sql.Date(dateFrom.getTime());
		java.sql.Date sqlDateTo = new java.sql.Date(dateTo.getTime());
		String sql = "select distinct dso_out_document.id, dsg_normal_dockind." + koszt + ", dso_out_document.stampfee, dso_out_document.documentdate, dsg_normal_dockind.kancelaria "
					+ "from dsg_normal_dockind , dso_out_document dso_out_document inner join dso_journal_entry de on dso_out_document.id = de.documentid" +
					"  where (dsg_normal_dockind.document_id = dso_out_document.id) "
					+ "and (de.entrydate between '" + sqlDateFrom.toString() + "' and '" + sqlDateTo.toString() + "')  and dsg_normal_dockind.rodzaj_out > 0";
	    stmt = DSApi.context().createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		return rs;
	}
	
	private String getCostCn(Integer costId){
		switch (costId) {
	        case 1:  return WYDAWN;
	        case 2:  return CIG;
	        case 3:  return AMRON;
	        case 4:  return CEM;
	        case 5:  return ZARZAD;
	        case 6:  return PRAWN;
	        case 7:  return SWD;
	        case 100:  return "INNE";
	        default: return "INNE";
		}
	}
	
	private void dumpRow(TreeMap<String, BigDecimal> mapa) throws IOException{
		dumper.newLine();
		dumper.addText("KG");
		if(!mapa.containsKey(WYDAWN)){
			dumper.addMoney(BigDecimal.ZERO);
		}else{
			dumper.addMoney(mapa.get(WYDAWN), true, 2);
		}
		if(!mapa.containsKey(CIG)){
			dumper.addMoney(BigDecimal.ZERO, true, 2);
		}else{
			dumper.addMoney(mapa.get(CIG), true, 2);
		}
		if(!mapa.containsKey(AMRON)){
			dumper.addMoney(BigDecimal.ZERO, true, 2);
		}else{
			dumper.addMoney(mapa.get(AMRON), true, 2);
		}
		if(!mapa.containsKey(CEM)){
			dumper.addMoney(BigDecimal.ZERO, true, 2);
		}else{
			dumper.addMoney(mapa.get(CEM), true, 2);
		}
		if(!mapa.containsKey(ZARZAD)){
			dumper.addMoney(BigDecimal.ZERO, true, 2);
		}else{
			dumper.addMoney(mapa.get(ZARZAD), true, 2);
		}
		if(!mapa.containsKey(PRAWN)){
			dumper.addMoney(BigDecimal.ZERO, true, 2);
		}else{
			dumper.addMoney(mapa.get(PRAWN), true, 2);
		}
		if(!mapa.containsKey(SWD)){
			dumper.addMoney(BigDecimal.ZERO, true, 2);
		}else{
			dumper.addMoney(mapa.get(SWD), true, 2);
		}
		dumper.addMoney(mapa.get(SUMA), true, 2);
		dumper.dumpLine();
	}
	
	public void doReport() throws Exception {
		
		try {
            this.initValues();
        } catch (Exception e) {
            e.printStackTrace();
        }
		
		initializeDumpers();
		setTitle("Tytu� raportu");
		setDescription("opis raportu");
		
		
		try {
			ResultSet rsGlowna = returnResultSet(stmtGlowna, KG_KOSZT, dateFrom, dateTo);
			TreeMap<String, BigDecimal> mapaGlowna = new TreeMap<String, BigDecimal>();
			while(rsGlowna.next()){
				Integer key =  rsGlowna.getInt(KG_KOSZT);
				BigDecimal value = rsGlowna.getBigDecimal(STAMPFEE);
				Integer kancelariaId = rsGlowna.getInt(KANCELARIA);
				if(key != null && key != 0 && value != null && kancelariaId.equals(11)){
					if(mapaGlowna.containsKey(getCostCn(key)) && !mapaGlowna.containsKey(getCostCn(100))){
						BigDecimal stampFeeValue = mapaGlowna.get(getCostCn(key));
						stampFeeValue = stampFeeValue.add(value.setScale(2));
						mapaGlowna.put(getCostCn(key), value.setScale(2));
					}
					else{
						mapaGlowna.put(getCostCn(key), value.setScale(2));
					}
				}
			}
			
			ResultSet rsWydawnictwo = returnResultSet(stmtWydawnictwo, KW_KOSZT, dateFrom, dateTo);
			TreeMap<String, BigDecimal> mapaWydawnictwo = new TreeMap<String, BigDecimal>();
			while(rsWydawnictwo.next()){
				Integer key =  rsWydawnictwo.getInt(KW_KOSZT);
				BigDecimal value = rsWydawnictwo.getBigDecimal(STAMPFEE);
				Integer kancelariaId = rsWydawnictwo.getInt(KANCELARIA);
				if(key != null && key != 0 && value != null && kancelariaId.equals(6)){
					if(mapaWydawnictwo.containsKey(getCostCn(key)) && !mapaWydawnictwo.containsKey(getCostCn(100))){
						BigDecimal stampFeeValue = mapaWydawnictwo.get(getCostCn(key));
						stampFeeValue = stampFeeValue.add(value.setScale(2));
						mapaWydawnictwo.put(getCostCn(key), value.setScale(2));
					}
					else{
						mapaWydawnictwo.put(getCostCn(key), value.setScale(2));
					}
				}
			}
			
			BigDecimal toSumGlowna = new BigDecimal(0).setScale(2);
			for(BigDecimal value : mapaGlowna.values()){
				toSumGlowna = toSumGlowna.add(value);
			}
			mapaGlowna.put(SUMA, toSumGlowna);
			
			BigDecimal toSumWydawnictwo = new BigDecimal(0).setScale(2);
			for(BigDecimal value : mapaWydawnictwo.values()){
				toSumWydawnictwo = toSumWydawnictwo.add(value);
			}
			mapaWydawnictwo.put(SUMA, toSumWydawnictwo);
			
			dumper.newLine();
			dumper.addEmptyText();
			dumper.addText(WYDAWN);
			dumper.addText(CIG);
			dumper.addText(AMRON);
			dumper.addText(CEM);
			dumper.addText(ZARZAD);
			dumper.addText(PRAWN);
			dumper.addText(SWD);
			dumper.addText(SUMA);
			dumper.dumpLine();
			
			dumpRow(mapaGlowna);
			
			dumper.newLine();
			dumper.addText("KW");
			if(!mapaWydawnictwo.containsKey(WYDAWN)){
				dumper.addMoney(BigDecimal.ZERO, true, 2);
			}else{
				dumper.addMoney(mapaWydawnictwo.get(WYDAWN), true, 2);
			}
			dumper.addEmptyText();
			dumper.addEmptyText();
			if(!mapaWydawnictwo.containsKey(CEM)){
				dumper.addMoney(BigDecimal.ZERO, true, 2);
			}else{
				dumper.addMoney(mapaWydawnictwo.get(CEM), true, 2);
			}
			dumper.addEmptyText();
			dumper.addEmptyText();
			dumper.addEmptyText();
			dumper.addMoney(mapaWydawnictwo.get(SUMA), true, 2);
			dumper.dumpLine();
			
			dumper.newLine();
			dumper.addEmptyText();
			dumper.addMoney(mapaGlowna.get(WYDAWN).add(mapaWydawnictwo.get(WYDAWN)), true, 2);
			dumper.addEmptyText();
			dumper.addEmptyText();
			dumper.addMoney(mapaGlowna.get(CEM).add(mapaWydawnictwo.get(CEM)), true, 2);
			dumper.addEmptyText();
			dumper.addEmptyText();
			dumper.addEmptyText();
			dumper.addMoney(mapaGlowna.get(SUMA).add(mapaWydawnictwo.get(SUMA)), true, 2);
			dumper.dumpLine();
			
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		}
		finally{
			DSApi.context().closeStatement(stmtGlowna);
			DSApi.context().closeStatement(stmtWydawnictwo);
			dumper.closeFileQuietly();
		}

	}
	
	
	public void initializeDumpers() throws IOException{
		java.sql.Date sqlDateFrom = new java.sql.Date(dateFrom.getTime());
		java.sql.Date sqlDateTo = new java.sql.Date(dateTo.getTime());
		
        /** DUMPER **/
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "raport_" + DateUtils.formatJsDate(new java.util.Date()) + ".xls");
		poiDumper.openFile(xlsFile);
		poiDumper.addParagraph("ROZLICZENIE WYSYLEK POCZTOWYCH OD " + sqlDateFrom.toString() + " DO " + sqlDateTo.toString());
		dumper.addDumper(poiDumper);
		
		PDFDumper pdfDumper = new PDFDumper();
		File pdfFile = new File(this.getDestination(), "raport_" + DateUtils.formatJsDate(new java.util.Date()) + ".pdf");
		pdfDumper.openFile(pdfFile);
		pdfDumper.addParagraph("ROZLICZENIE WYSYLEK POCZTOWYCH OD " + sqlDateFrom.toString() + " DO " + sqlDateTo.toString());
		dumper.addDumper(pdfDumper);
    }

}
