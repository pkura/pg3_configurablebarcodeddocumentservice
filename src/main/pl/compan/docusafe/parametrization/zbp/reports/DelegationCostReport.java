/**
 * 
 */
package pl.compan.docusafe.parametrization.zbp.reports;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.ic.reports.ReportUtilsIC;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.PDFDumper;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.util.DateUtils;

/**
 * @author Maciej Starosz
 * email: maciej.starosz@docusafe.pl
 * Data utworzenia: 03-10-2013
 * docusafe
 * DelegationCostReport.java
 */
public class DelegationCostReport extends Report{
	
	private static final String DZIAL_FINANSUJACY = "Dzia� finansuj�cy";
	private static final String PRACOWNIK = "Pracownik";
	private static final String MIEJSCE = "Miejsce";
	private static final String KOSZT = "Koszt";
	
	
	UniversalTableDumper dumper = new UniversalTableDumper();
	
	private Date dateFrom;
    private Date dateTo;
    private Set<String> zespol = new TreeSet<String>();
    private Set<String> users  = new TreeSet<String>();
    private String miejsca;
    
    @Override
	public void initParametersAvailableValues() throws ReportException
	{
		super.initParametersAvailableValues();
		try
		{
			for(ReportParameter rp:params)
			{
				if(rp.getFieldCn().equals("project"))
					rp.setAvailableValues(ReportUtilsIC.getProjectsMap(DSApi.context().getDSUser()));
			}
		}catch (Exception e) {
			throw new ReportException(e);
		}
	}
    
    private void initValues() throws Exception {
        for (ReportParameter param : getParams()) {
            if (param.getType().equals("break-line")) {
                continue;
            }

            if (param.getFieldCn().equals("dateFrom") && param.getValue() != null) {
                dateFrom = DateUtils.jsDateFormat.parse((String) param.getValue());
            }

            if (param.getFieldCn().equals("dateTo") && param.getValue() != null) {
                dateTo = DateUtils.jsDateFormat.parse((String) param.getValue());
            }

            if("user".equalsIgnoreCase(param.getFieldCn()))
			{
            	users.addAll(param.valuesAsList());
			}
            
            if("zespol".equalsIgnoreCase(param.getFieldCn()))
			{
				zespol.addAll(param.valuesAsList());
			}
            
            if("miejsce".equalsIgnoreCase(param.getFieldCn()))
			{
            	miejsca = (String)param.getValue();
			}
        }
    }
	
	@Override
	public void doReport() throws Exception {
		try {
            this.initValues();
        } catch (Exception e) {
            e.printStackTrace();
        }
		
		this.initializeDumpers();
		
		try {
			dumper.newLine();
			dumper.addText("Lp.");
			dumper.addText(DZIAL_FINANSUJACY);
			dumper.addText(PRACOWNIK);
			dumper.addText(MIEJSCE);
			dumper.addText(KOSZT);
			dumper.dumpLine();
			int i = 1;
			
			Set<Long> usersId  = new TreeSet<Long>();
			String[] miejscaArray = miejsca.split(",");
			for(String user : users){
				usersId.add(DSUser.findByUsername(user).getId());
			}
			
			for(String dzial : zespol){
				for(Long id : usersId){
					for(String place : miejscaArray){
						BigDecimal koszt = new BigDecimal(0);
						String sql = "SELECT suma FROM dsg_zbp_delegacja WHERE (zespol = " + dzial + ") " +
								"and (delegowany = " + id.longValue() + ") and (miejsce_delegacji like '" + place +"') " +
								"and ((termin_wyjazdu between '" + java.sql.Date.valueOf(DateUtils.formatSqlDate(dateFrom)) + "' and '" + java.sql.Date.valueOf(DateUtils.formatSqlDate(dateTo)) + "') " +
								"or (termin_powrotu between '" + java.sql.Date.valueOf(DateUtils.formatSqlDate(dateFrom)) + "' and '" + java.sql.Date.valueOf(DateUtils.formatSqlDate(dateTo)) + "')) " +
								"";
					
						Statement stmt = DSApi.context().createStatement();
						ResultSet rs = stmt.executeQuery(sql);
						while(rs.next()){
							if(rs.getBigDecimal(1) != null) koszt = koszt.add(rs.getBigDecimal(1));
						}
						if(koszt.compareTo(BigDecimal.ZERO) == 0 || koszt.compareTo(BigDecimal.ZERO) == -1){
							;
						}else{
							dumper.newLine();
							dumper.addInteger(i++);
							dumper.addText(getDivName(dzial));
							dumper.addText(DSUser.findById(id).getFirstname() + " " + DSUser.findById(id).getLastname());
							dumper.addText(place);
							dumper.addMoney(koszt, true, 2);
							dumper.dumpLine();
						}
						stmt.close();
					}
				}
			}
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		}
		finally{
			dumper.closeFileQuietly();
			}
	}
	
	private String getDivName(String value){
		switch (Integer.valueOf(value)) {
	        case 1:  return "System Analiz i Monitorowania Rynku Obrotu Nieruchomo�ciami";
	        case 2:  return "Centrum Edukacyjno-Marketingowe";
	        case 3:  return "Centrum Informacji Gospodarczej";
	        case 4:  return "Zesp� prawny";
	        case 5:  return "System W�z�a Dost�powego";
	        case 6:  return "Wydawnictwo";
	        case 7:  return "Zarz�d ";
	        case 8:  return "Zesp� organizacji ";
	        case 9:  return "Zesp� kadr i p�ac ";
	        case 10:  return "Zesp� ksi�gowo�ci ";
	        default: return "nie znaleziono";
		}
	}
	
	
	public void initializeDumpers() throws IOException{
		java.sql.Date sqlDateFrom = new java.sql.Date(dateFrom.getTime());
		java.sql.Date sqlDateTo = new java.sql.Date(dateTo.getTime());
		
        /** DUMPER **/
		PDFDumper pdfDumper = new PDFDumper();
		File pdfFile = new File(this.getDestination(), "raport_" + DateUtils.formatJsDate(new java.util.Date()) + ".pdf");
		pdfDumper.openFile(pdfFile);
		pdfDumper.addParagraph("ROZLICZENIE KOSZT�W DELEGACJI OD " + sqlDateFrom.toString() + " DO " + sqlDateTo.toString());
		dumper.addDumper(pdfDumper);
    }

}
