package pl.compan.docusafe.parametrization.zbp;

import pl.compan.docusafe.service.imports.dsi.DSIImportHandler;

/**
 * User: Tomasz
 * Date: 13.12.13
 * Time: 14:52
 */
public class CrmDSIHandler extends DSIImportHandler {

    @Override
    protected void prepareImport() throws Exception {

    }

    @Override
    protected boolean isStartProces() {
        return false;
    }

    @Override
    public String getDockindCn() {
        return "ZBP_CRM";
    }
}
