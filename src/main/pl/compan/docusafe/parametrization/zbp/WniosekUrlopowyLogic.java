package pl.compan.docusafe.parametrization.zbp;

import com.google.common.collect.Maps;
import org.apache.commons.dbutils.DbUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.joda.time.DateTime;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.base.absences.EmployeeAbsenceEntry;
import pl.compan.docusafe.core.base.absences.FreeDay;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbsenceLogic;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

public class WniosekUrlopowyLogic extends AbstractDocumentLogic implements AbsenceLogic {

    public static final String WNIOSEK_URLOPOWY_CN = "wniosek_urlopowy";
    public static final String WNIOSKODAWCA_CN = "WNIOSKODAWCA";
    public static final String SUPERVISOR_ACCEPT = "supervisor_accept";
    public static final String DIVISION_ACCEPT = "division_accept";

    private static final long serialVersionUID = 1L;

	private static WniosekUrlopowyLogic instance;
	protected static Logger log = LoggerFactory.getLogger(WniosekUrlopowyLogic.class);

	private static Boolean isSenderDaysCN = false;
	
	public static WniosekUrlopowyLogic getInstance() {
		if (instance == null)
			instance = new WniosekUrlopowyLogic();
		return instance;
	}
	
	/**
	 * Lista urlop�w pracownika
	 */
	protected List<EmployeeAbsenceEntry> emplAbsences;
	
	protected EmployeeCard employeeCard;
	
	/**
	 * Lista dni wolnych
	 */
	protected List<FreeDay> freeDays;

    @Override
    public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
		String documentKindName = document.getDocumentKind().getName();
	
		perms.add(new PermissionBean(ObjectPermission.READ, documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, documentKindName + " - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP,
			documentKindName + " zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, documentKindName
			+ " - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP,
			documentKindName + " zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, documentKindName
			+ " - usuwanie"));
		Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
		perms.addAll(documentPermissions);
		this.setUpPermission(document, perms);
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
	FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
	Folder folder = Folder.getRootFolder();
	Date date = (Date) fm.getValue("stampDate");
	folder = folder.createSubfolderIfNotPresent("Wnioski urlopowe");
	folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(date));
	folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(date));
	document.setFolder(folder);

	document.setTitle("Wniosek urlopowy - " + fm.getEnumItem("WNIOSKODAWCA").getTitle());
	//document.setDescription("Wniosek urlopowy - " + fm.getEnumItem("WNIOSKODAWCA").getTitle());

    }

    
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
    	
	Map<String, Object> toReload = Maps.newHashMap();
	for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields()) {
	    if (f.getDefaultValue() != null) {
		toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
	    }
	}

	DSUser user = DSApi.context().getDSUser();
	toReload.put("WNIOSKODAWCA", user.getId());
	toReload.put("stampDate", new Date());

	fm.reloadValues(toReload);

    }

    
    /**
	 * Sprawdzenie wnioskuj�cey b�d� wyznaczona osoba do zast�pstwac nie 
	 * ma ju� zaakceptowanego urlopu z podanego przedzia�u, mozna sprawdzic takze
	 * czy osoba wyznaczona do zastepstwa nie ma juz zaakceptowanego innego zastepstwa 
	 * w podanym terminie
	 */
    private Boolean checkVacation(Map<String, FieldData> values, String personSQL, String personCN, Date start, Date end){
    	PreparedStatement ps = null;
		ResultSet rs = null;
		String sql =   "select period_from, period_to from dsg_zbp_wniosek_urlopowy where "+personSQL+" = ? " +
				"and ((period_from between ? and ?) or (period_to between ? and ?) or " +
				"((period_from <= ?) and (period_to >= ?))) " +
				"and (status = 200 or status = 210 or status = 2000)";
		
		try{
			ps = DSApi.context().prepareStatement(sql);	
			ps.setInt(1, Integer.valueOf((String) values.get("DWR_"+personCN).getData()));
			ps.setDate(2, java.sql.Date.valueOf(DateUtils.formatSqlDate(start)));
			ps.setDate(3, java.sql.Date.valueOf(DateUtils.formatSqlDate(end)));
			ps.setDate(4, java.sql.Date.valueOf(DateUtils.formatSqlDate(start)));
			ps.setDate(5, java.sql.Date.valueOf(DateUtils.formatSqlDate(end)));
			ps.setDate(6, java.sql.Date.valueOf(DateUtils.formatSqlDate(start)));
			ps.setDate(7, java.sql.Date.valueOf(DateUtils.formatSqlDate(end)));
			rs = ps.executeQuery();
			
			if(rs.next()){
				if(personCN.equals("WNIOSKODAWCA"))
					return true;
				if(personCN.equals("OSOBA_ZASTEPUJ"))
					return true;
			}
			
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
		finally{
    		DbUtils.closeQuietly(rs);
            DSApi.context().closeStatement(ps);
    	}
		return false;
    }
    
    /**
	 * Sprawdzenie czy osoba wyznaczona do zastepstwa nie
	 *  ma wyznaczonego w tym terminie wyjazdu s�u�bowego
	 *  
	 */
    private Boolean checkDelegation(Map<String, FieldData> values, String personSQL, String personCN, Date start, Date end){
    	PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "select termin_wyjazdu, termin_powrotu from dsg_zbp_delegacja " +
				"where ("+personSQL+" = ?) and ((termin_wyjazdu between ? and ?) " +
				"or (termin_powrotu between ? and ?) or " +
				"((termin_wyjazdu <= ?) and (termin_powrotu >= ?))) " +
				"and ((status = 2000) or (status between 120 and 230))";
		
		try{
			ps = DSApi.context().prepareStatement(sql);	
			ps.setInt(1, Integer.valueOf((String) values.get("DWR_"+personCN).getData()));
			ps.setDate(2, java.sql.Date.valueOf(DateUtils.formatSqlDate(start)));
			ps.setDate(3, java.sql.Date.valueOf(DateUtils.formatSqlDate(end)));
			ps.setDate(4, java.sql.Date.valueOf(DateUtils.formatSqlDate(start)));
			ps.setDate(5, java.sql.Date.valueOf(DateUtils.formatSqlDate(end)));
			ps.setDate(6, java.sql.Date.valueOf(DateUtils.formatSqlDate(start)));
			ps.setDate(7, java.sql.Date.valueOf(DateUtils.formatSqlDate(end)));
			rs = ps.executeQuery();
			
			if(rs.next()){
				if(personSQL.equals("delegowany"))
					return true;
				if(personSQL.equals("DSUSER"))
					return true;
			}
			
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
		finally{
    		DbUtils.closeQuietly(rs);
            DSApi.context().closeStatement(ps);
    	}
		return false;
    }
    
    
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {

		log.info("validate contents wniosek urlopowy: \n {} ", values);
		Field msg = null;

		if (!DSApi.isContextOpen()) {
			DSApi.openAdmin();
		}
		try {
			if (fm.getDocumentId() == null || areSomeFieldsNotReadOnly(fm)) {

				if (values.get("DWR_DATE_FROM") != null && values.get("DWR_DATE_TO").getData() != null) {

					Date startDate = values.get("DWR_DATE_FROM").getDateData();
					Date finishDate = values.get("DWR_DATE_TO").getDateData();

					/**
					 * Ustawienie autouzupe�niania warto�ci b�d� mo�liwo��
					 * edycji przez u�ytkownika
					 */
					if (values.get("DWR_WORKING_DAYS_CN").isSender()) {
						isSenderDaysCN = true;
					}
					if (values.get("DWR_DATE_FROM").isSender() || values.get("DWR_DATE_TO").isSender()) {
						isSenderDaysCN = false;
					}

					if (finishDate != null && finishDate.before(startDate)) {
						return new Field("messageField", "Data rozpocz�cia urlopu jest p�niejsza ni� data zako�czenia urlopu", Field.Type.BOOLEAN);
					} else {
						if (!isSenderDaysCN) {
							if ((startDate != null) && (finishDate != null)) {
								Integer vacationDays = 0;
								Integer vacationDaysFromCalendar = 0;
								DateTime startDateYoda = new DateTime(startDate);
								Date sDate = startDate;

								while (sDate.getTime() < finishDate.getTime()) {
									sDate = startDateYoda.toDate();
									if (sDate.getDay() == 6 || sDate.getDay() == 0) {
										;
									} else {
										vacationDays++;
									}
									for (int i = 0; i < freeDays.size(); i++) {
										if (sDate.getTime() == freeDays.get(i).getDate().getTime()) {
											vacationDaysFromCalendar++;
										}
									}
									startDateYoda = startDateYoda.plusDays(1);
								}
								if (vacationDaysFromCalendar != 0) {
									for (int i = 0; i < vacationDaysFromCalendar; i++) {
										vacationDays--;
									}
								}

								if (startDate.getTime() == finishDate.getTime() && vacationDaysFromCalendar == 0) {
									vacationDays = 1;
								}

								if (vacationDays > 0) {
									values.get("DWR_WORKING_DAYS_CN").setIntegerData(vacationDays);
								} else {
									values.get("DWR_WORKING_DAYS_CN").setIntegerData(0);
								}
							}
						}
					}

					/**
					 * Sprawdzenie czy nie ma ju� zaakceptowanego urlopu z
					 * podanego przedzia�u
					 */
					if (checkVacation(values, "DSUSER", "WNIOSKODAWCA", startDate, finishDate)) {
						return new Field("messageField", "Wybrana data urlopu pokrywa si� z zatwierdzonym", Field.Type.BOOLEAN);
					}

				}

				if (values.get("DWR_WNIOSKODAWCA") != null && values.get("DWR_WNIOSKODAWCA").getData() != null) {
					/**
					 * sprawdzenie czy osoba zast�puj�ca nie jest ustawiona na
					 * osob� wnioskuj�c�
					 */
					if (values.get("DWR_OSOBA_ZASTEPUJ") != null && values.get("DWR_OSOBA_ZASTEPUJ").getData() != null) {
						int wnioskodawcaID = Long.valueOf(values.get("DWR_WNIOSKODAWCA").getEnumValuesData().getSelectedOptions().get(0)).intValue();
						int zastepstwoID = Long.valueOf(values.get("DWR_OSOBA_ZASTEPUJ").getEnumValuesData().getSelectedOptions().get(0)).intValue();

						if (wnioskodawcaID == zastepstwoID) {
							List<String> selected = new LinkedList<String>();
							selected.add("");
							values.get("DWR_OSOBA_ZASTEPUJ").getEnumValuesData().setSelectedOptions(selected);
							return new Field("messageField", "Osob� zast�puj�c� nie mo�e by� osoba wnioskuj�ca", Field.Type.BOOLEAN);
						}

						if (values.get("DWR_DATE_FROM") != null && values.get("DWR_DATE_TO").getData() != null) {
							Date startDate = values.get("DWR_DATE_FROM").getDateData();
							Date finishDate = values.get("DWR_DATE_TO").getDateData();
							/**
							 * Sprawdzenie czy osoba wyznaczona do zastepstwa
							 * nie ma juz wyznaczonego zastepstwa w tym okresie
							 */
							if (checkVacation(values, "OSOBA_ZASTEPUJ", "OSOBA_ZASTEPUJ", startDate, finishDate)) {
								return new Field("messageField", "Wyznaczona osoba do zast�pstwa ma ju� wyznaczone inne zast�pstwo w tym terminie", Field.Type.BOOLEAN);
							}

							/**
							 * Sprawdzenie czy osoba wyznaczona do zastepstwa
							 * nie ma wyznaczonego w tym terminie wyjazdu
							 * s�u�bowego
							 */
							if (checkDelegation(values, "delegowany", "OSOBA_ZASTEPUJ", startDate, finishDate)) {
								return new Field("messageField", "Wyznaczona osoba do zast�pstwa ma ju� zatwierdzony wyjazd s�u�bowy w podanym terminie", Field.Type.BOOLEAN);
							}

							/**
							 * Sprawdzenie czy osoba wyznaczona do zastepstwa
							 * nie ma zaakceptowanego w tym terminie urlopu
							 */
							if (checkVacation(values, "DSUSER", "OSOBA_ZASTEPUJ", startDate, finishDate)) {
								return new Field("messageField", "Wyznaczona osoba do zast�pstwa ma ju� zatwierdzony orlop w tym terminie", Field.Type.BOOLEAN);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		} finally {
			DSApi.close();
		}

		return msg;
	}

    private boolean areSomeFieldsNotReadOnly(FieldsManager fm) {
        return isNotReadOnly(fm, "KIND") ||
                isNotReadOnly(fm, "RECIPIENT_HERE") ||
                isNotReadOnly(fm, "WNIOSKODAWCA") ||
                isNotReadOnly(fm, "OSOBA_ZASTEPUJ") ||
                isNotReadOnly(fm, "DATE_FROM") ||
                isNotReadOnly(fm, "DATE_TO") ||
                isNotReadOnly(fm, "DOC_DESCRIPTION") ||
                isNotReadOnly(fm, "POZOSTALO_DNI") ||
                isNotReadOnly(fm, "stampDate") ||
                isNotReadOnly(fm, "WORKING_DAYS_CN");
    }

    private boolean isNotReadOnly(FieldsManager fm, String fieldCn) {
        try {
            pl.compan.docusafe.core.dockinds.field.Field field = fm.getField(fieldCn);
            return field != null && !field.isReadOnly();
        } catch (Exception e) {
            return false;
        }
    }
    
    public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
	log.info("--- WniosekUrlopowyLogic : START PROCESS !!! ---- {}", event);

	try {
	    Map<String, Object> map = Maps.newHashMap();
	    map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
	    map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
	    document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

	    if (AvailabilityManager.isAvailable("addToWatch"))
		DSApi.context().watch(URN.create(document));
	    java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
	    DSUser author = DSApi.context().getDSUser();
	    String user = author.getName();
	    String fullName = author.asFirstnameLastname();

	    perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
	    perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
	    perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
	    perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
	    perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

	    Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
	    perms.addAll(documentPermissions);
	    this.setUpPermission(document, perms);

	} catch (Exception e) {
	    log.error(e.getMessage(), e);
	    throw new EdmException(e.getMessage());
	}

    }

	@Override
	public List<FreeDay> getFreeDays() {
		
		return freeDays;
	}

	public void onLoadData(FieldsManager fm) throws EdmException 
	{
		freeDays = AbsenceFactory.getFreeDays();
		employeeCard = getEmployeeCard(fm);
		int currentYear = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
		emplAbsences = AbsenceFactory.getEmployeeAbsences(employeeCard, currentYear);
	}
	
	/**
	 * �aduje kart� pracownika
	 * 
	 * @param userImpl
	 * @return
	 * @throws Exception
	 */
	private void loadEmployeeCard(UserImpl userImpl) throws EdmException
	{
		employeeCard = AbsenceFactory.getActiveEmployeeCardByUser(userImpl);
		employeeCard.getExternalUser();
		
		if (employeeCard == null)
			throw new EdmException("Brak aktualnej karty pracownika!");
	}
	
	/**
	 * Zwraca list� urlop�w pracownika w bierzacym roku, dla widoku w dockind-specific-additions.jsp
	 * 
	 * @return
	 */
	public List<EmployeeAbsenceEntry> getEmplAbsences() 
	{
		
		return emplAbsences;
	}
	
	private EmployeeCard getEmployeeCard(FieldsManager fm) throws EdmException
	{
		UserImpl user;
			user = (UserImpl) DSApi.context().getDSUser();
		
		EmployeeCard employeeCard = (EmployeeCard) DSApi.context().session().createQuery(
				"from " + EmployeeCard.class.getName() + " em where em.user = ?")
				.setParameter(0, user)
				.setMaxResults(1)
				.uniqueResult();
		
		return employeeCard;
	}

	@Override
	public EmployeeCard getEmployeeCard() {
		// TODO Auto-generated method stub
		
		return employeeCard;
	}
	
	@Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
	boolean assigned = false;
	Long docId = doc.getId();
		if (SUPERVISOR_ACCEPT.equals(acceptationCn.toLowerCase())) {
            try {
            	FieldsManager fm = Document.find(docId).getFieldsManager();
    			Integer wnioskodawcaId = (Integer)fm.getKey(WNIOSKODAWCA_CN);
    			if(wnioskodawcaId.equals(2)){//jfurga
    				assignable.addCandidateUser(DSUser.findById(wnioskodawcaId.longValue()).getName());
                    assigned = true;
                    AssigneeHandler.addToHistory(openExecution, doc, DSUser.findById(wnioskodawcaId.longValue()).getName(), null);
    			}else{
    				pl.compan.docusafe.core.users.sql.UserImpl userImpl = pl.compan.docusafe.core.users.sql.UserImpl.find(wnioskodawcaId.longValue());
                	Set<UserImpl> supervisorSet = userImpl.getSupervisors();
                	for(UserImpl supervisor : supervisorSet){
                		 assignable.addCandidateUser(supervisor.getName());
                         assigned = true;
                         AssigneeHandler.addToHistory(openExecution, doc, supervisor.getName(), null);
                	}
    			}
            } catch (EdmException e) {
                log.error(e.getMessage(), e);
            }
      }
	  else if(DIVISION_ACCEPT.equals(acceptationCn.toLowerCase())){
			try {
				FieldsManager fm = Document.find(docId).getFieldsManager();
    			Integer wnioskodawcaId = (Integer)fm.getKey(WNIOSKODAWCA_CN);
    			if(wnioskodawcaId.equals(2)){//jfurga
    				assignable.addCandidateUser(DSUser.findById(wnioskodawcaId.longValue()).getName());
                    assigned = true;
                    AssigneeHandler.addToHistory(openExecution, doc, DSUser.findById(wnioskodawcaId.longValue()).getName(), null);
    			}else{
    				DSUser divOwner = DSUser.findById(getDivisionOwnerId(wnioskodawcaId.longValue()));
    				assignable.addCandidateUser(divOwner.getName());
                    assigned = true;
    			}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	return assigned;
    }

	private Long getDivisionOwnerId(Long id) {
    	Long userId = null;
    	try {
	    	BigDecimal userIdTemp = (BigDecimal) DSApi.context().session().createSQLQuery("SELECT OWNER_ID FROM dsg_user_to_owner WHERE USER_ID = :id")
	    		.setParameter("id", id).uniqueResult();
	    	userId = userIdTemp.longValue();
    	} catch (EdmException e) {
    		log.error(e.getMessage(), e);
    	}
    	return userId;
    }
	
}