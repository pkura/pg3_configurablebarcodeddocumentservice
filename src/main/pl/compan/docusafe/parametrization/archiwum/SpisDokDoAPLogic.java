package pl.compan.docusafe.parametrization.archiwum;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.Container.ArchiveStatus;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.parametrization.archiwum.db.provider.ArchiveQueryExecutor;
import pl.compan.docusafe.parametrization.archiwum.db.provider.ArchiveQueryProvider;
import pl.compan.docusafe.parametrization.utp.NormalLogic;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class SpisDokDoAPLogic extends AbstractDocumentLogic
{

	private ArchiveQueryExecutor archiveQueryExecutor = ArchiveQueryProvider.getInstance();
	
	public static final String SPIS_DOK_DO_AP = "SpisDokDoAP";
	public static final String STATUS_PRZEKAZANY_DO_AP = "Przekazane Do AP";
	public static final String STATUS_NOWY_SPIS = "Nowy spis";
	public static final String STATUS_ODRZUCONY = "Odrzucone";
	public static final String STATUS_PRZYJETY = "Przyjety";
	public static final String SYMBOL_DLA_POZYCJI_FIZYCZNEJ = "Pozycja fizyczna";
	public static final String RWA_DLA_POZYCJI_FIZYCZNEJ = "Pozycja fizyczna";
	public static final String CN_DATA_DOKUMENTU = "DOC_DATE";
	public static final String CN_AUTOR_DOKUMENTU = "DOC_AUTOR";
	public static final String CN_DZIAL_AUTORA_DOKUMENTU = "DOC_AUTORDIVISION";
	public static final String CN_TYP_DOKUMENTU = "TYP_DOKUMENTU";
	public static final String CN_ABSTRAKT_DOKUMENTU = "ABSTRAKT";
	public static final String CN_MIESCEUTWOORZENIA_DOKUMENTU = "MIESCEUTWOORZENIA";
	public static final String CN_DOC_DESCRIPTION = "DOC_DESCRIPTION";


	private static NormalLogic instance;
	public final static String BARCODE = "BARCODE";
	protected static Logger log = LoggerFactory.getLogger(SpisDokDoAPLogic.class);

	public static NormalLogic getInstance()
	{
		if (instance == null)
			instance = new NormalLogic();
		return instance;
	}

	private static final long serialVersionUID = 1L;

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP,
				"Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}


	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		//log.info("type: {}", type);
		Map<String, Object> values = new HashMap<String, Object>();

		values.put(CN_DATA_DOKUMENTU, new Date());
		values.put(CN_TYP_DOKUMENTU, 2);
		values.put(CN_DOC_DESCRIPTION, "Spis zdawczo-odbiorczy do Archiwum Pa雟twowego");

		fm.reloadValues(values);
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			if (document instanceof InOfficeDocument)
			{
				map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
			} else
			{
				map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
				map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			}


			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

			String barcode = document.getFieldsManager().getStringValue(BARCODE);

			if (document instanceof InOfficeDocument)
			{
				InOfficeDocument in = (InOfficeDocument) document;
				if (barcode != null)
				{
					in.setBarcode(barcode);
				} else
				{
					in.setBarcode("Brak");
				}
			} else if (document instanceof OutOfficeDocument)
			{
				OutOfficeDocument od = (OutOfficeDocument) document;
				if (barcode != null)
				{
					od.setBarcode(barcode);
				} else
				{
					od.setBarcode("Brak");
				}
			}



			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));


			TaskSnapshot.updateByDocument(document);
		} catch (Exception e)
		{

		}
	}

	@Override
	public void onStartProcess(OfficeDocument document) throws EdmException
	{
		Map<String, Object> map = Maps.newHashMap();
		if (document instanceof InOfficeDocument)
		{
			map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
		} else
		{
			map.put(ASSIGN_USER_PARAM, DSApi.context().getDSUser().getName());
			map.put(ASSIGN_DIVISION_GUID_PARAM, DSApi.context().getDSUser().getDivisions());
		}


		document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

		String barcode = document.getFieldsManager().getStringValue(BARCODE);

		if (document instanceof InOfficeDocument)
		{
			InOfficeDocument in = (InOfficeDocument) document;
			if (barcode != null)
			{
				in.setBarcode(barcode);
			} else
			{
				in.setBarcode("Brak");
			}
		} else if (document instanceof OutOfficeDocument)
		{
			OutOfficeDocument od = (OutOfficeDocument) document;
			if (barcode != null)
			{
				od.setBarcode(barcode);
			} else
			{
				od.setBarcode("Brak");
			}
		}



		if (AvailabilityManager.isAvailable("addToWatch"))
			DSApi.context().watch(URN.create(document));


		TaskSnapshot.updateByDocument(document);
	}

	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values)
	{
		return setMultipledictionaryValue(dictionaryName, values, null);
	}

	/** Wywolywane za ka盥ym razem, po tym jak u篡tkownik zmieni
	 * warto嗆 pola w s這wniku (wybierze pozycj� z enum, database
	 * lub sko鎍zy wpisywa� warto嗆 z klawiatury).
	 *
	 * S逝篡 do aktualizacji p鏊 tego s這wnika.
	 *
	 * Wywo造wane wcze郾iej ni� validateDwr.
	 *
	 * @param dictionaryName - nazwa tego s這wnika, bez przedrostka DWR_
	 * @param values - tylko i wy陰cznie wartosci jednego rekordu, w kt鏎ym nast徙i豉 zmiana,
	 * bez dost瘼u do reszty p鏊, innych s這wnik闚 i innych rekord闚 tego s這wnika */
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fValues)
	{
		Map<String, Object> results = new HashMap<String, Object>();
		Map<String, Object> fieldsValues = new HashMap<String, Object>();
		try
		{


			if (dictionaryName.equals("SPIS_AP_TECZKI"))
			{

				Object id_teczki = values.get("SPIS_AP_TECZKI_IDTECZKI").getData();
				Integer id_teczkAsString = Integer.parseInt((String) id_teczki);
				
				if (id_teczki == null)
					return results;

				else
				{
					FromClause from = new FromClause();
					TableAlias divisionTable = from.createTable("ds_ar_teczki_do_ap_view");
					WhereClause where = new WhereClause();
					if(AvailabilityManager.isAvailable("PG.parametrization"))
					where.add(Expression.eq(divisionTable.attribute("id"), id_teczkAsString));
					else
					where.add(Expression.eq(divisionTable.attribute("id"), id_teczki));
					SelectClause select = new SelectClause(true);
					SelectColumn idTitle = select.add(divisionTable, "RWATYTUL");
					SelectColumn idSymbol = select.add(divisionTable, "SYMBOL");
					SelectColumn idfdate = select.add(divisionTable, "DATADO");
					SelectColumn idodate = select.add(divisionTable, "DATAOD");
					SelectColumn folderId = select.add(divisionTable, "FOLDERID");
					SelectQuery query;
					SelectClause selectId = new SelectClause(true);
					ResultSet rs = null;
					query = new SelectQuery(select, from, where, null);

					rs = query.resultSet();
					if (rs.next())
					{

						if (rs.getString(folderId.getPosition()) != null)
						{
							if (rs.getString(idTitle.getPosition()) != null)
								results.put("SPIS_AP_TECZKI_RWATYTUL", rs.getString(idTitle.getPosition()));
							else
								results.put("SPIS_AP_TECZKI_RWATYTUL", "");
							if (rs.getString(idSymbol.getPosition()) != null)
								results.put("SPIS_AP_TECZKI_SYMBOL", rs.getString(idSymbol.getPosition()));
							else
								results.put("SPIS_AP_TECZKI_SYMBOL", "");
						} else
						{
							results.put("SPIS_AP_TECZKI_RWATYTUL", RWA_DLA_POZYCJI_FIZYCZNEJ);
							results.put("SPIS_AP_TECZKI_SYMBOL", SYMBOL_DLA_POZYCJI_FIZYCZNEJ);
						}
						if (rs.getDate(idodate.getPosition()) != null)
							results.put("SPIS_AP_TECZKI_DATAOD", rs.getDate(idodate.getPosition()));
						else
							results.put("SPIS_AP_TECZKI_DATAOD", "");
						if (rs.getDate(idfdate.getPosition()) != null)
							results.put("SPIS_AP_TECZKI_DATADO", rs.getDate(idfdate.getPosition()));
						else
							results.put("SPIS_AP_TECZKI_DATADO", "");
					}


				}

			}
		} catch (SQLException e)
		{
			log.error(e.getMessage(), e);
			e.printStackTrace();
		} catch (EdmException e)
		{
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}

		return results;
	}



	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{


		return null;


	}

	@Override
	public void doDockindEvent(DockindButtonAction eventActionSupport, ActionEvent event, Document document, String activity, Map<String, Object> values,
			Map<String, Object> dockindKeys) throws EdmException
	{
		// TODO Auto-generated method stub
		super.doDockindEvent(eventActionSupport, event, document, activity, values, dockindKeys);
	}

	public void doDockindEvent(DockindButtonAction eventActionSupport, ActionEvent event, Document doc, String activity, Map<String, Object> values)
			throws EdmException
	{
		try
		{

			boolean ctx = DSApi.openContextIfNeeded();
			Long documentId = doc.getId();
			//	Document doc=Document.find(documentId);

			OfficeCase oc = null;

			ArchivePackageManager man = new ArchivePackageManager();
			List<OfficeCase> listaSpraw = new ArrayList<OfficeCase>();

			PreparedStatement ps;

			ResultSet rs = archiveQueryExecutor.getPortfoliosWithMultipleInventory(documentId);
			while (rs.next())
			{
				if (rs.getString("id") != null && !rs.getString("id").equals(""))
				{
					OfficeFolder of = OfficeFolder.findByOfficeId(rs.getString("id"));
					for (Iterator<? extends Container> iter = of.getChildren().iterator(); iter.hasNext();)
					{
						Container c = (Container) iter.next();
						if (c instanceof OfficeCase)
							listaSpraw.add((OfficeCase) c);
					}
				}
			}


			if (listaSpraw != null)
			{
				File archivePackage = man.preparePackage(listaSpraw);
				if (archivePackage != null)
				{
					String now = DateUtils.formatFolderDate(new Date());
					ServletUtils.streamFile(ServletActionContext.getResponse(), archivePackage, "application/zip",
							"Content-Disposition: attachment; filename=\"paczka_archiwalna_" + now + ".zip\"");
				}
			}
			DSApi.closeContextIfNeeded(ctx);

			//return sendMail(DWR_SEND_FORWARD_BUTTON, values, fm);



		} catch (SQLException e)
		{
			log.debug("", e);
			e.printStackTrace();
		} catch (IOException e)
		{
			log.debug("", e);
			e.printStackTrace();
		}




	}

	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		try
		{

			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			fm.initialize();

			// czas wygenerowania metryki
			values.put("DATA_BIEZACA", DateUtils.formatCommonDate(new java.util.Date()).toString());
			values.put("DRUKUJACY", DSApi.context().getDSUser().asFirstnameLastname());
			values.put("NUMER", fm.getValue("NUMER").toString());
			values.put("KOM_ORG", fm.getValue("KOM_ORG").toString());
			values.put("MIEJSCE_AP", fm.getValue("MIEJSCE_AP").toString());
			String uwagi = "";

			if (!Remark.documentRemarks(doc.getId()).isEmpty())
			{
				for (Remark r : Remark.documentRemarks(doc.getId()))
				{
					uwagi += r.getContent() + "; ";
				}
			}
			values.put("UWAGI", uwagi);
			List<Long> slownik = (List<Long>) fm.getKey("SPIS_AP_TECZKI"); //ma byc lista kluczy
			if (slownik == null || slownik.isEmpty())
				return;

			StringBuilder sql = new StringBuilder(" select * from ds_ar_sprawy_do_ap_view sprawy_view  "
					+ "left join dso_container con on con.id=sprawy_view.parent_id "
					+ "left join ds_ar_teczki_do_ap teczkiAP on teczkiAP.idteczki2=con.officeid " + "where (");
			for (Long klucz : slownik)
			{
				sql.append("  teczkiAP.id=" + klucz + " OR");
			}
			sql.delete(sql.length() - 2, sql.length());
			sql.append(")");
			ResultSet rs;
			List<Map<String, String>> pozycje = new ArrayList();
			try
			{
				PreparedStatement ps = DSApi.context().prepareStatement(sql);
				rs = ps.executeQuery();
				int i = 0;


				while (rs.next())
				{
					i++;
					HashMap<String, String> pozycja = new HashMap<String, String>(7);

					pozycja.put("lp", Integer.toString(i));

					if (rs.getString("TYTUL") != null && !rs.getString("TYTUL").equals(""))
					{
						pozycja.put("tytul", rs.getString("TYTUL"));
					} else
					{
						pozycja.put("tytul", "-----");
					}

					if (rs.getString("ZNAK") != null && !rs.getString("ZNAK").equals(""))
					{
						pozycja.put("znak", rs.getString("ZNAK"));
					} else
					{
						pozycja.put("znak", "-----");
					}

					if (rs.getString("DATA_ROZP") != null && !rs.getString("DATA_ROZP").equals(""))
					{
						pozycja.put("data_rozp", rs.getString("DATA_ROZP"));
					} else
					{
						pozycja.put("data_rozp", "-----");
					}

					if (rs.getString("DATA_ZAK") != null && !rs.getString("DATA_ZAK").equals(""))
					{
						pozycja.put("data_zak", rs.getString("DATA_ZAK"));
					} else
					{
						pozycja.put("data_zak", "-----");
					}

					if (rs.getString("RWA_DESC") != null && !rs.getString("RWA_DESC").equals(""))
					{
						pozycja.put("rwa", rs.getString("RWA_DESC"));
					} else
					{
						pozycja.put("rwa", "-----");
					}

					if (rs.getString("doc_count") != null && !rs.getString("doc_count").equals(""))
					{
						pozycja.put("ilosc_dok", rs.getString("doc_count"));
					} else
					{
						pozycja.put("ilosc_dok", "-----");
					}
					pozycje.add(pozycja);

				}
			} catch (SQLException e)
			{
				log.error(e.getMessage(), e);
				e.printStackTrace();
			}
			values.put("SPRAWY", pozycje);


			sql = new StringBuilder(
					"   select distinct v.OFFICEID,tecz.symbol,tecz.rwatytul,tecz.dataod,tecz.datado,tecz.uwagi,tecz.usun as usuniecie from ds_ar_teczki_do_ap tecz "
							+ "left join DSO_CONTAINER v on tecz.IDTECZKI2=v.OFFICEID  where  ");
			for (Long klucz : slownik)
			{
				sql.append(" tecz.id=" + klucz + " OR");
			}
			sql.delete(sql.length() - 2, sql.length());
			pozycje = new ArrayList();
			try
			{
				PreparedStatement ps = DSApi.context().prepareStatement(sql);
				rs = ps.executeQuery();
				int i = 0;


				while (rs.next())
				{
					i++;
					HashMap<String, String> pozycja = new HashMap<String, String>(7);

					pozycja.put("lp", Integer.toString(i));

					if (rs.getString("OFFICEID") != null && !rs.getString("OFFICEID").equals(""))
					{
						pozycja.put("znak", rs.getString("OFFICEID"));
					} else
					{
						pozycja.put("znak", "-----");
					}

					if (rs.getString("SYMBOL") != null && !rs.getString("SYMBOL").equals(""))
					{
						pozycja.put("symbol", rs.getString("SYMBOL"));
					} else
					{
						pozycja.put("symbol", "-----");
					}

					if (rs.getString("RWATYTUL") != null && !rs.getString("RWATYTUL").equals(""))
					{
						pozycja.put("rwatytul", rs.getString("RWATYTUL"));
					} else
					{
						pozycja.put("rwatytul", "-----");
					}

					if (rs.getString("DATAOD") != null && !rs.getString("DATAOD").equals(""))
					{
						pozycja.put("dataod", rs.getString("DATAOD"));
					} else
					{
						pozycja.put("dataod", "-----");
					}

					if (rs.getString("DATADO") != null && !rs.getString("DATADO").equals(""))
					{
						pozycja.put("datado", rs.getString("DATADO"));
					} else
					{
						pozycja.put("datado", "-----");
					}

					if (rs.getString("uwagi") != null && !rs.getString("uwagi").equals(""))
					{
						pozycja.put("uwagi", rs.getString("uwagi"));
					} else
					{
						pozycja.put("uwagi", "-----");
					}
					if (rs.getString("usuniecie") != null && !rs.getString("usuniecie").equals(""))
					{
						if (rs.getString("usuniecie").equals("1"))
							pozycja.put("usuniecie", "TAK");
						pozycja.put("usuniecie", "TAK");
					} else
					{
						pozycja.put("usuniecie", "NIE");
					}
					pozycje.add(pozycja);

				}
			} catch (SQLException e)
			{
				log.error(e.getMessage(), e);
				e.printStackTrace();
			}
			values.put("TECZKI", pozycje);




		} catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public void validate(Map<String, Object> values, DocumentKind kind, Long documentId)
	{
		try
		{
			PreparedStatement ps;
			if (!values.containsKey("NUMER") || values.get("NUMER") == null)
			{
				boolean ctx = DSApi.openContextIfNeeded();

				ps = DSApi.context().prepareStatement("select max(numer)as maxnumer from ds_ar_spis_dok_do_ap");
				ResultSet rs = ps.executeQuery();
				if (rs.next())
				{
					values.put("NUMER", rs.getInt("maxnumer") + 1);

				} else
				{
					values.put("NUMER", 1);
				}
				DSApi.closeContextIfNeeded(ctx);
			}

			if (values.get("STATUS")!=null && values.get("STATUS").toString().equals("2"))
			{
				//przekazane do AP, ale nadal istnieje mozliwosc usuniecia
				// TODO poprawic usuwanie po przekazaniu do AP 
				if (values.containsKey("M_DICT_VALUES"))
				{
					HashMap tmp = (HashMap) values.get("M_DICT_VALUES");
					boolean koncz = false;
					for (int i = 1; koncz == false; i++)
					{
						if (tmp.containsKey("SPIS_AP_TECZKI_" + i))
						{
							HashMap<String, Object> dict = (HashMap<String, Object>) tmp.get("SPIS_AP_TECZKI_1");
							if (dict.containsKey("ID"))
							{
								ps = DSApi.context().prepareStatement(
										"select idteczki2, items.folderid as folderid from ds_ar_teczki_do_ap  "
												+ " left join ds_ar_inventory_items items on items.folder_officeid=idteczki2 where id=" + dict.get("ID"));
								ResultSet rs = ps.executeQuery();

								if (rs.next())
								{
									try
									{
										if (rs.getString("folderid") != null)
											ustawieniePolPrzyPrzekazywaniu(documentId, rs.getString("idteczki2"),
													((FieldData) dict.get("USUN")).getBooleanData());
									} catch (Exception e)
									{
										log.debug("blad przy usuwaniu pozycji na przekazywaniu do AP ", e);
										e.printStackTrace();

									}
								}
							}
						} else
							koncz = true;
					}
				}
			}
		} catch (SQLException e)
		{
			log.error(e.getMessage(), e);
			e.printStackTrace();
		} catch (EdmException e1)
		{
			log.error(e1.getMessage(), e1);
			e1.printStackTrace();
		}

	}


	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		// TODO Auto-generated method stub

	}


	public void ustawieniePolPrzyPrzekazywaniu(Long documentId, String officeId, boolean czyZatrzec)
	{
		try
		{
			OfficeFolder of = OfficeFolder.findByOfficeId(officeId);
			of.setArchiveStatus(ArchiveStatus.TransferedNA);
			if (czyZatrzec)
				of.setName("pozycja przekazana do AP");

			for (Container con : of.getChildren())
			{
				if (con instanceof OfficeCase)
				{
					OfficeCase oc = (OfficeCase) con;
					oc.setArchiveStatus(ArchiveStatus.TransferedNA);
					if (czyZatrzec)
					{
						oc.setDescription("pozycja przekazana do AP");
						oc.setTitle("pozycja przekazana do AP");
						List<OfficeDocument> dokumenty = oc.getDocuments();
						for (OfficeDocument dok : dokumenty)
						{
							dok.setTitle("pozycja przekazana do AP");
							dok.setDescription("pozycja przekazana do AP");
							//dok.setArchiveStatus(ArchiveStatus.Shredded.ordinal());
							for (Attachment a : dok.getAttachments())
								a.delete();

						}
					}
				}
			}
			if (czyZatrzec)
			{
				//usuniecie pozycji ze spisu, jesli ma zaznaczone usun
				PreparedStatement ps = DSApi.context().prepareStatement(
						" delete from ds_ar_spis_dok_do_ap_multi  where id in ( select multi.id from ds_ar_spis_dok_do_ap_multi multi "
								+ "left join ds_ar_teczki_do_ap teczki on multi.FIELD_VAL=teczki.ID where teczki.USUN='1' and DOCUMENT_ID="
								+ documentId.toString() + " )");
				ps.executeUpdate();

				ps = DSApi.context().prepareStatement("delete from ds_ar_teczki_do_ap where id not in (select field_val from ds_ar_spis_dok_do_ap_multi) ");
				ps.executeUpdate();
				DSApi.context().commit();
			}
		} catch (SQLException e)
		{
			log.error(e.getMessage(), e);
			e.printStackTrace();
		} catch (EdmException e1)
		{
			log.error(e1.getMessage(), e1);
			e1.printStackTrace();
		}
	}

	@Override
	public void onLoadData(FieldsManager fm) throws EdmException
	{
		if (fm.getEnumItem("STATUS") != null)
		{
			Integer statusid = fm.getEnumItem("STATUS").getId();
			pl.compan.docusafe.core.dockinds.field.Field field = fm.getField("SPIS_AP_TECZKI");
			String[] buttons = {};
			switch (statusid) {
			case 0:
				buttons = new String[] { "doAdd", "doRemove" };
			break;
			case 1:
				buttons = new String[] { "doRemove" };
			break;
			case 2:
				buttons = new String[] {};
			break;
			case 5:
				buttons = new String[] {};
			break;

			}
			((DictionaryField) field).setDicButtons(Lists.newArrayList(buttons));


			super.onLoadData(fm);
		}
	}



}
