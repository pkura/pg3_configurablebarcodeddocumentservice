package pl.compan.docusafe.parametrization.archiwum;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.Field.Type;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.office.Container.ArchiveStatus;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Role;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;

/**
 * raz dziennie sprawdza, czy sa sprawy do przeniesienia do archiwum. Taka sprawa jest przenoszona, gdy mina 2 lata od
 * momentu zamkniecia sprawy, ale liczone od stycznia roku nastepnego
 * 
 * @author bkaliszewski
 * 
 */
public class NewCaseArchiverScheduled extends ServiceDriver
{
	private static final String[] TABELE_DOKUMENTU= { "dso_in_document", "DSO_OUT_DOCUMENT", "dso_order" };

	public static final String wykazTableName= "ds_ar_inventory_transf_and_rec";
	public static final String wykazMultipleTableName= "DS_AR_INVENTORY_ITEMS_MULTI";
	public static final String wykazItemsTableName= "DS_AR_INVENTORY_ITEMS";
	
	private static final boolean __debug= true;

	private static final String UstawStatusTeczkomSQL= "UPDATE dso_container SET "+
		" archiveStatus= :status WHERE id in (:ids)";
	
	private Timer archiveTimer;
	private Timer protokolTimer;
	private static String kategoria;
	
	public static class Wykaz
	{
		private int		wykazNumber;
		private Date	archiveDate;
		private String	divisionGuid;
		private List<WykazFolder>	folders = new ArrayList();
		
		public Wykaz(Date archive_date, String divisionGuid){
			this.archiveDate=archive_date;
			this.divisionGuid= divisionGuid;
		}

		public void addFolder(OfficeFolder folder, Date dateFrom, Date dateTo){
			folders.add(new WykazFolder(folder, dateFrom, dateTo));
		}
		
		public List<WykazFolder> getFolders(){
			return folders;
		}
		
		public int getWykazNumber(){
			return wykazNumber;
		}
		
		public void setWykazNumber(int wykazNumber){
			this.wykazNumber= wykazNumber;
		}
		
		public Date getArchiveDate(){
			return archiveDate;
		}

		public String getDivisionGuid()
		{
			return divisionGuid;
		}
	}
	
	public static class WykazFolder 
	{
		public final static String DATA_FORMAT= null;
		public long id;
		//public int lp;
		public String officeId;
		public String title;
		public Date dateFrom;
		public Date dateTo;
		
		public String category;
		public WykazFolder(OfficeFolder folder, Date dateFrom, Date dateTo)
		{
			officeId= folder.getOfficeId();
			title= folder.getName();
			category= folder.getRwa().getAcHome();
			this.dateFrom= dateFrom;
			this.dateTo= dateTo;
			this.id= folder.getId();
		}
	}
	
	static class FolderCache{
		Date 			dateFrom;
		Date 			dateTo;
		OfficeFolder	folder;
		OfficeFolder	parent;
	}

	/**
	 * 
	 * @param finishDate
	 * @throws EdmException
	 */
	public static void generujWykazy(Date finishDate, String division) throws EdmException
	{
		Date archiveDate= Calendar.getInstance().getTime();
		
		try {
//			DSApi.openContextIfNeeded();
			DSApi.context().begin();

			Set<String> permittedUsers= getUsersForPermission(DSPermission.ADMINISTRACJA_WYKAZ_DOC.getName());

			List<Number> folders= getFoldersByFinishDate(finishDate, division);

			// dla pustej listy nic nie robimy
			if (folders.isEmpty())
				return;
			
			noteLog("[Wykaz] Do zarchiwizowania "+folders.size()+" teczek");
			
			Map<String,Wykaz> wykazyByDivisionGuid = new HashMap();
			
			// przechowujemy tu dla kazdej teczki dwie daty (min/max daty utworzenia dla dokumentow z tej teczki)
			// wyciagajac date teczki nadrzednej uzywamy teczek podrzednych, stad mapa
		////	Map<Number, Date[]> folderToDates = new HashMap<Number, Date[]>();

			for (Number f_id : folders) {
				OfficeFolder folder = (OfficeFolder) DSApi.context().session().get(OfficeFolder.class, f_id.longValue());
				
				//// sprawdzamy
				
				
		/*		if(folder.getChildren())
				// get children
				Criteria c = DSApi.context().session().createCriteria(OfficeFolder.class);
	            c.add(Restrictions.eq("parentId", folder.getId()));
	            List<OfficeFolder> children = new HashSet<OfficeFolder>(c.list());
	      */
				
				
				//OfficeFolder folder = (OfficeFolder) DSApi.context().session().createSQLQuery("select TO_DATE from DSG_ZBP_DEL_KOSZTY_PODROZY where ID = :id")
				//.setParameter("id", id).uniqueResult();
				//OfficeCase casee= (OfficeCase) o;
				
				// pomijamy teczki, ktore byly przeprocesowane (juz sa w jakims wykazie 
				// zdawczo-odbiorczym / maja protokol / sa wybrakowane)
				if( folder.getArchiveStatus()!=null && !folder.getArchiveStatus().equals(Container.ArchiveStatus.None))
					continue;
	
				//Date[]
				Object[] dates= getDatesFromTo(f_id.longValue());
				
	
				// stworz wykaz lub dodaj do stworzonego
				
				Wykaz wykaz= wykazyByDivisionGuid.get(folder.getDivisionGuid());

				if (wykaz == null) {
					wykaz= new Wykaz(archiveDate, folder.getDivisionGuid());
					wykazyByDivisionGuid.put(folder.getDivisionGuid(), wykaz);
					noteLog("[Wykaz] Dodaje wykaz: divisionGuid="+folder.getDivisionGuid()+", folderid "+f_id);
				}
				
				// dodaj teczke do wykazu
				wykaz.addFolder(folder, (Date)dates[0], (Date)dates[1]);
			}

			Set<String> divisions = wykazyByDivisionGuid.keySet();
			noteLog("[Wykaz] Ukonczono przegladanie teczek. Do wygenerowania "+divisions.size()+" wykazow");
			int wykazNextNumber = getMaxWykazNumber()+1;

			for( String divisionGuid : divisions )
			{
				Wykaz wykaz= wykazyByDivisionGuid.get(divisionGuid);
				
				int ilosc_teczek = wykaz.getFolders().size();

				if( __debug && ilosc_teczek>100){
					 System.out.println("[Wykaz] W trybie __debug pomija wykaz z iloscia teczek: "+ilosc_teczek);
					continue;
				}
				noteLog("[Wykaz] Generuje wykaz zawierajacy "+ilosc_teczek+" teczek");
				
				
				DSUser odbiorca= findOsobaUprawniona(DSDivision.find(divisionGuid), permittedUsers );

				if( odbiorca==null )
				{
					noteLog("[Wykaz] Brak odbiorcy; dla guid "+divisionGuid+", uprawnionych: "+permittedUsers.size());
					odbiorca= DSUser.findByUsername("admin");
				}
				
				// stworz wiadomosc dla wykazu
				noteLog("[Wykaz] wiadomosc bedzie wyslana do " + odbiorca.getName());

				wykaz.setWykazNumber(wykazNextNumber);
				
				if( createWykazDoc(odbiorca.getName(), wykaz) )
				{
					// jesli udalo sie dodac nowy wykaz, zanotuj i zapisz
					
					ustawStatusTeczkom(wykaz);
					
					++wykazNextNumber;
					DSApi.context().commit();
					DSApi.context().begin();
				}
			}
			
			DSApi.context().commit();


		} catch (Exception e) {
			e.printStackTrace();
			DSApi.context().rollback();
			throw new EdmException(e);
		} finally {
//			DSApi.close();
			noteLog("[Wykaz] koniec" );
		}
	}
	
	public static void ustawStatusTeczkom(Wykaz wykaz) throws HibernateException, EdmException
	{
		List<Long> idks = new ArrayList<Long>();

		for (int i = 0; i < wykaz.folders.size(); ++i)
		{
			//niezbedne jest wyselekcjonowanie tych teczek kt�re s� tej samej kategori archiwalnej  kategoria jest uzupelniania na wczesniejszym etapie
			// bo bez tego tworzy sie spis zdawczo odbiorczy z ruznymi kategoriami i nieda sie go procesowac poniewaz na procesie zachod� zalezno �ci po kategori 
			if (wykaz.folders.get(i).category.contains(kategoria))
			{
				idks.add(wykaz.folders.get(i).id);

			}

		}//throw new EdmException("dsds");
		if (idks.size() > 0)
		{
			Query query = DSApi.context().session().createSQLQuery(UstawStatusTeczkomSQL);
			query.setInteger("status", Container.ArchiveStatus.ToArchive.ordinal());
			query.setParameterList("ids", idks);

			query.executeUpdate();
		}
	}

	/**
	 * dla danej teczki musimy znac dwie daty: najmniejsza i najwieksza date z utworzenia dokumentu
	 * @param folderToDates 
	 * @param longValue
	 * @return list - dwie wartosci, najmniejsza i najwieksza data. Jest to Date[], ale java nie pozwala na rzutowanie
	 */
	public static Object[] getDatesFromTo(long folder_id) throws HibernateException, EdmException
	{
		String query_data= "" +
				"SELECT MIN(SPRAWA.OPENDATE) OPENDATE, "+
				"  MAX(SPRAWA.FINISHDATE) FINISHDATE" +
				" FROM DSO_CONTAINER SPRAWA" +
				" WHERE SPRAWA.DISCRIMINATOR = 'CASE'" +
				"  AND SPRAWA.PARENT_ID = :parentId";
		
		Query query = DSApi.context().session().createSQLQuery(query_data);
		query.setLong("parentId", folder_id);
		
		List<Date[]> result =query.list(); 
		return result.get(0);
	}

	/*public static void archiwizujTeczki(Wykaz wykaz) throws SQLException, EdmException
	{
		int ilosc_teczek = wykaz.getFolders().size();
		int case_lp = 1;
		// archiwizuj teczki
		for( WykazFolder w_folder : wykaz.getFolders() )
		{
			if( pobierzDatyWykazCase( w_folder ) )
			{
				w_folder.ignore= false;

				noteLog("[Wykaz] " + case_lp + "\t / " + ilosc_teczek + "\t archiwizuje sprawe "	+ w_folder.tmp_folder.getOfficeId());

				archiveFolder(w_folder.tmp_folder);

				w_folder.lp= case_lp;

				++case_lp;
			}else
				noteLog("[Wykaz] pobierzDatyWykazCase returned false");

			//wcase.tmp_case.setArchived(true);
			w_folder.tmp_folder.setArchivedDate(wykaz.getArchiveDate());
			w_folder.tmp_folder.setArchiveStatus( ArchiveStatus.Archived);
			DSApi.context().session().update(w_folder.tmp_folder);
		}
	}*/

	/**
	 * pobiera teczki (id), ktore zawieraja sprawy nadajace sie do archiwizacji (ktorych data zamkniecia 
	 * jest mniejsza od podanej)
	 */
	private static List<Number> getFoldersByFinishDate(Date finishDate, String division) throws EdmException
	{
		if(division != null){
			String query_data= 
					"SELECT TECZKA.ID" +
							" FROM DSO_CONTAINER  teczka  " +
							" WHERE teczka.discriminator = 'FOLDER'" +
							// zawiera sprawy bez wykazu
							"  AND EXISTS(  SELECT 1 FROM DSO_CONTAINER sprawa "+
							"   WHERE sprawa.parent_id= teczka.id AND (sprawa.archiveStatus is null OR sprawa.archiveStatus= 0 ) )"+
							// wszystkie sprawy sa zamkniete
							"  AND NOT EXISTS(  SELECT 1 FROM DSO_CONTAINER sprawa "+
							"   WHERE sprawa.parent_id= teczka.id AND sprawa.finishDate > :finishDate)"+
							"  AND EXISTS(  SELECT 1 FROM DSO_CONTAINER sprawa "+
							"   WHERE sprawa.parent_id= teczka.id AND teczka.divisionguid = :division)";
			Query query = DSApi.context().session().createSQLQuery(query_data);
			query.setDate("finishDate", finishDate);
			query.setString("division", division);
			return query.list();
		} else {
			String query_data= 
					"SELECT TECZKA.ID" +
							" FROM DSO_CONTAINER  teczka  " +
							" WHERE teczka.discriminator = 'FOLDER'" +
							// zawiera sprawy bez wykazu
							"  AND EXISTS(  SELECT 1 FROM DSO_CONTAINER sprawa "+
							"   WHERE sprawa.parent_id= teczka.id AND (sprawa.archiveStatus is null OR sprawa.archiveStatus= 0 ) )"+
							// wszystkie sprawy sa zamkniete
							"  AND NOT EXISTS(  SELECT 1 FROM DSO_CONTAINER sprawa "+
							"   WHERE sprawa.parent_id= teczka.id AND sprawa.finishDate > :finishDate)";
			Query query = DSApi.context().session().createSQLQuery(query_data);
			query.setDate("finishDate", finishDate);
			return query.list();
		}
		/*String s=	"SPRAWA.PARENT_ID, MIN(SPRAWA.OPENDATE) OPENDATE, "+
			"  MAX(SPRAWA.FINISHDATE) FINISHDATE, SPRAWA.DIVISIONGUID, TECZKA.OFFICEID" +
			" FROM DSO_CONTAINER  TECZKA  WITH(NOLOCK)" +
			"  JOIN dso_rwa RWA ON SPRAWA.RWA_ID=RWA.ID" +
			"  JOIN DSO_CONTAINER TECZKA ON SPRAWA.PARENT_ID=TECZKA.ID" +
			" WHERE SPRAWA.DISCRIMINATOR = 'FOLDER'" +
			"  AND RWA.achome LIKE 'A'" +
			"  AND SPRAWA.ARCHIVED = 0" + //zastanowic sie czy to musi tu wogole byc
			"  AND SPRAWA.PARENT_ID NOT IN" +
			"   (SELECT DISTINCT PARENT_ID FROM DSO_CONTAINER WHERE CLOSED = 0 AND DISCRIMINATOR = 'CASE')";*/

		
		/*Criteria criteria= DSApi.context().session().createCriteria(Container.class);
		criteria.add(Restrictions.eq("archived", false));
		//criteria.add(Restrictions.or(Restrictions.eq("archiveStatus", 0),Restrictions.isNull("archiveStatus")));
		criteria.add(Restrictions.lt("finishDate", finishDate));*/
	}

	/*private static void archiveFolder(OfficeFolder tmp_folder) throws SQLException, EdmException
	{
		// ustawia flage Archived

		Long folderId= tmp_folder.getId();
		String[] tabele= TABELE_DOKUMENTU;

		// doc.setArchived(true);
		for (String tabelaKanc : tabele) {
		 --zmienic tabele..
			String query_data= "update " + tabelaKanc + " set archived=1 where "
				+" id in (select M.DOCUMENT_ID from ds_wykaz_multiple M join DS_WYKAZ_CASES C on C.id=M.FIELD_VAL "+
				" where C.CASE_ID=" + folderId+")";
			PreparedStatement ps_data= DSApi.context().prepareStatement(query_data);
			ps_data.executeUpdate();
			ps_data.close();
		}
	}*/

	/**
	 * tworzy wiadomosc na liscie zadan, poprzez utworzenie dokumentu wewnetrznego
	 * @return 
	 * 
	 */
	public static boolean createWykazDoc(String osoba, Wykaz wykaz ) throws Exception
	{
		String divisionGuid= wykaz.getDivisionGuid();
		String opis= "Wygenerowano spis zdawczo-odbiorczy nr " + ((Integer) wykaz.getWykazNumber());
		
		Map<String, Map<String, FieldData>> foldersMap= new HashMap();
		
		// ustawiamy wartosci multislownika spisu, czyli liste folderow
		Integer folder_index= 1; 
		 kategoria="";
		for(WykazFolder _folder : wykaz.getFolders())
		{
			
			//niezbedne jest wyselekcjonowanie tych teczek kt�re s� tej samej kategori archiwalnej  kategoria jest uzupelniania na wczesniejszym etapie
			// bo bez tego tworzy sie spis zdawczo odbiorczy z ruznymi kategoriami i nieda sie go procesowac poniewaz na procesie zachod� zalezno �ci po kategori 
			 kategoria = _folder.category;
			 if(kategoria.length()>0)
				 kategoria =  kategoria.substring(0,1);
			break;
		}
		for( WykazFolder _folder : wykaz.getFolders())
		{
			if (!_folder.category.contains(kategoria)){
				continue;
			}
			else {
				
			Map<String, FieldData> fields = new HashMap();
			foldersMap.put("DS_AR_INVENTORY_ITEM_"+folder_index, fields);
			fields.put("FOLDER_OFFICEID", new FieldData( Type.STRING, _folder.officeId));
			fields.put("FOLDERID", new FieldData( Type.INTEGER, _folder.id));
			fields.put("FOLDER_TITLE", new FieldData( Type.STRING, _folder.title));
			// Liczba teczek elektronicznych jest zawsze r�wna 1 dla danej teczki
			fields.put("FOLDER_COUNT", new FieldData( Type.INTEGER, 1));
			
			//fields.put("COMMENTS", new FieldData( Type.STRING, _folder.znak));
			fields.put("DATE_FROM", new FieldData( Type.DATE, _folder.dateFrom));
			fields.put("DATE_TO", new FieldData( Type.DATE, _folder.dateTo));
			fields.put("CATEGORY_ARCH", new FieldData( Type.STRING, _folder.category));
			//fields.put("FOLDER_COUNT", new FieldData( Type.INTEGER, _folder.znak));
			//fields.put("FOLDER_LENGTH_METER", new FieldData( Type.STRING, _folder.znak));
			
			
			fields.put("ID", new FieldData( Type.STRING, ""));
////			fields.put("CASE_ID", new FieldData( Type.INTEGER, _folder.case_id));
			////fields.put("C_WYKAZ_NUMBER", new FieldData( Type.INTEGER, wykaz.getWykazNumber()));
			
			++folder_index;
		}
		}
		// nie zostal dodany zaden element
		if( folder_index== 1)
		{
			if( __debug )System.out.println("[Wykaz] Brak folderow, nie tworzy wykazu");
			return false;
		}
		
		OutOfficeDocument doc= new OutOfficeDocument();
		doc.setVersion(1);
		doc.setCzyAktualny(true);
		doc.setCzyCzystopis(false);
		doc.setCurrentAssignmentGuid(divisionGuid);
		doc.setDivisionGuid(divisionGuid);
		doc.setCurrentAssignmentAccepted(Boolean.FALSE);
		doc.setCreatingUser(osoba);
		if(AvailabilityManager.isAvailable("PG.parametrization"))
		doc.setInternal(false);
		else
		doc.setInternal(true);
		doc.setSummary(opis);
		doc.setForceArchivePermissions(false);
		doc.setAssignedDivision(divisionGuid);
		doc.setClerk(osoba);
		doc.setSender(new Sender());
		doc.setSource("");
		
		doc.create();

		DocumentKind documentKind= DocumentKind.findByCn(DocumentLogicLoader.ARCHIVES);
		Map<String, Object> dockindKeys= new HashMap();
		// STATUSDOKUMENTU
		dockindKeys.put("DSDIVISION", DSDivision.find(divisionGuid).getId());
		//dockindKeys.put("DSUSER", 1L);
		dockindKeys.put("IS_CATEGORY_A", wykaz.getFolders().get(0).category.equals("A")? 1 : 0 );
		
		dockindKeys.put("number_itr", wykaz.getWykazNumber());
		
		
	//	dockindKeys.put("DOC_DESCRIPTION", opis);
	//	dockindKeys.put("DOC_DATE", wykaz.getArchiveDate());
	//	dockindKeys.put("WYKAZ_NUMBER", wykaz.getWykazNumber());
		dockindKeys.put(DocumentKind.MULTIPLE_DICTIONARY_VALUES, foldersMap);

		doc.setDocumentKind(documentKind);

		Long newDocumentId = doc.getId();
		doc.setVersionGuid(newDocumentId.intValue());
		
		if( DwrDictionaryFacade.dictionarieObjects.get("DS_AR_INVENTORY_ITEM")==null)
			DwrDictionaryFacade.dictionarieObjects.put("DS_AR_INVENTORY_ITEM", 
					new DwrDictionaryBase(DocumentKind.findByCn(DocumentLogicLoader.ARCHIVES).getFieldByCn("DS_AR_INVENTORY_ITEM"),null ) );
		
		documentKind.saveMultiDictionaryValues(doc, dockindKeys );
		
		documentKind.set(newDocumentId, dockindKeys);
		doc.setAuthor(osoba);
		
		doc.getDocumentKind().logic().archiveActions(doc, 0);// DocumentLogic.TYPE_INTERNAL_OFFICE);
		doc.getDocumentKind().logic().documentPermissions(doc);
		
		
		//bindToJournal(doc, null);
		//poniewaz w pg cale arhiwum jest na pismach wychodzacych  to nei dodajemy do dziennika i nei nadajemy KO
		if(!AvailabilityManager.isAvailable("PG.parametrization")){
		Journal journal = Journal.getMainInternal();
		Long journalId = journal.getId();
		DSApi.context().session().flush();
		Integer sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), GlobalPreferences.getCurrentDay());
		ActionEvent event= null;
		doc.bindToJournal(journalId, sequenceId, null, event);
		doc.setOfficeDate(GlobalPreferences.getCurrentDay());
		}
		DSApi.context().session().save(doc);
		
		doc.getDocumentKind().logic().onStartProcess(doc);

		//WorkflowFactory.createNewProcess(doc, false, "Wykaz zdawczo-odbiorczy nr "+((Integer) wykaz.getWykazNumber()));
		TaskSnapshot.updateByDocument(doc);
		noteLog("[Wykaz] done creating "+opis);
		
		return true;
	}
	/*{
		String divisionGuid= wykaz.getDivisionGuid();
		String opis= "Wygenerowano spis zdawczo-odbiorczy nr "
				+ ((Integer) wykaz.getWykazNumber());
		
		Map<String, Map<String, FieldData>> foldersMap= new HashMap();
		
		Integer folder_index= 1;
		for( WykazFolder _folder : wykaz.getFolders())
		{
			if( _folder.ignore )
				continue;
			
			Map<String, FieldData> fields = new HashMap();
			foldersMap.put("WYKAZ_CASES_"+folder_index, fields);
			fields.put("ZNAK", new FieldData( Type.STRING, _folder.znak));
			fields.put("TYTUL", new FieldData( Type.STRING, _folder.tytul));
			fields.put("LP", new FieldData( Type.INTEGER, _folder.lp));
			fields.put("DATA_OD", new FieldData( Type.STRING, _folder.data_od));
			fields.put("DATA_DO", new FieldData( Type.STRING, _folder.data_do));
			fields.put("KAT_AKT", new FieldData( Type.STRING, _folder.kat_akt));
			fields.put("ID", new FieldData( Type.STRING, ""));
////			fields.put("CASE_ID", new FieldData( Type.INTEGER, _folder.case_id));
			fields.put("C_WYKAZ_NUMBER", new FieldData( Type.INTEGER, wykaz.getWykazNumber()));
			
			++folder_index;
		}
		
		// nie zostal dodany zaden element
		if( folder_index== 1)
		{
			if( __debug )System.out.println("[Wykaz] Brak folderow, nie tworzy wykazu");
			return;
		}
		
		
		OutOfficeDocument doc= new OutOfficeDocument();

		doc.setCurrentAssignmentGuid(divisionGuid);
		doc.setDivisionGuid(divisionGuid);
		doc.setCurrentAssignmentAccepted(Boolean.FALSE);
		doc.setCreatingUser(osoba);
		doc.setInternal(true);
		doc.setSummary(opis);
		doc.setForceArchivePermissions(false);
		doc.setAssignedDivision(divisionGuid);
		doc.setClerk(osoba);
		doc.setSender(new Sender());
		doc.setSource("");
		
		doc.create();

		DocumentKind documentKind= DocumentKind.findByCn(DocumentLogicLoader.WYKAZ_ZDAWCZO_ODBIORCZY);
		Map<String, Object> dockindKeys= new HashMap();
		dockindKeys.put("DOC_DESCRIPTION", opis);
		dockindKeys.put("DOC_DATE", wykaz.getArchiveDate());
		dockindKeys.put("WYKAZ_NUMBER", wykaz.getWykazNumber());
		dockindKeys.put("M_DICT_VALUES", foldersMap);

		doc.setDocumentKind(documentKind);

//		doc.create();

		Long newDocumentId= doc.getId();
		
		if( DwrDictionaryFacade.dictionarieObjects.get("WYKAZ_CASES")==null)
			DwrDictionaryFacade.dictionarieObjects.put("WYKAZ_CASES",new DwrDictionaryBase(DocumentKind.findByCn(DocumentLogicLoader.WYKAZ_ZDAWCZO_ODBIORCZY).getFieldByCn("WYKAZ_CASES"),null ) );
		
		documentKind.saveMultiDictionaryValues(doc, dockindKeys );
		
		documentKind.set(newDocumentId, dockindKeys);
		
		doc.getDocumentKind().logic().archiveActions(doc, 0);
		doc.getDocumentKind().logic().documentPermissions(doc);
		doc.setAuthor(osoba);
		DSApi.context().session().save(doc);

		//doc.getDocumentKind().logic().onStartProcess(doc);

		WorkflowFactory.createNewProcess(doc, false, "Wykaz zdawczo-odbiorczy nr "+((Integer) wykaz.getWykazNumber()).toString());
		TaskSnapshot.updateByDocument(doc);
		noteLog("[Wykaz] "+opis);
	}*/
	
	private static DSUser findOsobaUprawniona(DSDivision dzial, Set<String> permittedUsers ) throws EdmException
	{
		DSUser[] users = dzial.getUsers(true);
		
		for( DSUser usr : users )
		{
			if( permittedUsers.contains(usr.getName()))
				return usr;
		}
		return null;
	}

	private static List getCasesDoBrakowania() throws EdmException
	{
		Criteria criteria= DSApi.context().session().createCriteria(OfficeCase.class);
		//criteria.add(Restrictions.eq("archived", true));
		criteria.add(Restrictions.eq("archiveStatus", ArchiveStatus.Archived));
		noteLog("[getCasesDoBrakowania] Warunek: archiveStatus=2");

		return criteria.list();
	}

	public static int getMaxWykazNumber() throws HibernateException, EdmException, SQLException
	{
		String query = "select max(number_itr)from "+wykazTableName;
		
		PreparedStatement ps =  DSApi.context().prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		
		rs.next();
		Integer numer = rs.getInt(1);
		rs.close();
		ps.close();
		
		if( numer==null || numer< 0)
			return 0;
		
		return numer.intValue();
	}

	/*
	private static boolean pobierzDatyWykazCase(WykazFolder w_folder) throws SQLException, EdmException
	{
		Date minDate= null;
		Date maxDate= null;

		Long caseId= w_folder.tmp_folder.getId();
		String[] tabele= TABELE_DOKUMENTU;
		for (String tabelaKanc : tabele) {
			
			String query_data= "select min(doc.ctime), max(doc.ctime) from ds_document doc join " + tabelaKanc
			+ " dockanc on ( doc.id=dockanc.id and dockanc.CASE_ID=" + caseId + ")";
			PreparedStatement ps_data= DSApi.context().prepareStatement(query_data);
			ResultSet rs_data= ps_data.executeQuery();
			if (!rs_data.next() || rs_data.getDate(1)==null) {
				rs_data.close();
				ps_data.close();
				continue;
			}

			// Daty
			if (minDate == null)
				minDate= rs_data.getDate(1);
			else if (rs_data.getDate(1).before(minDate))
				minDate= rs_data.getDate(1);
			if (maxDate == null)
				maxDate= rs_data.getDate(2);
			else if (rs_data.getDate(2).after(maxDate))
				maxDate= rs_data.getDate(2);

			rs_data.close();
			ps_data.close();
			break;
		}

		if( minDate==null)
			return false;
		DateFormat df= new SimpleDateFormat(SuperiorFolder.DATA_FORMAT);
		
		w_folder.data_od= df.format(minDate);
		w_folder.data_do= df.format(maxDate);
		
		return true;
	}*/

	
	public static Set<String> getUsersForPermission(String permissionStr) throws EdmException
	{
		Role[] roles = Role.list();
		for( Role r : roles )
			if( r.getPermissions().contains(permissionStr))
				return r.getUsernames();
		
		return new HashSet<String>();
	}
	

	/**
	 * Klasa podpinana do Timera, majaca wywolac akcje archiwizowania z odpowiednia data
	 */
	class NewCaseArchiverAction extends TimerTask
	{
		@Override
		public void run()
		{


			Calendar cal= Calendar.getInstance();
		//	if(cal.get(Calendar.DAY_OF_MONTH)==1&&cal.get(Calendar.MONTH)==0){
				/*QueryForm query=new QueryForm(0,1);
				DocumentHelper dh = DSApi.context().getDocumentHelper();
				query.addProperty("DOCKIND_ID",DocumentKind.getDockindInfo(DocumentLogicLoader.ARCHIVES).getDockindId());
				query.("", value);*/
				boolean ctx = false;
				try {
					ctx = DSApi.openContextIfNeeded();
					PreparedStatement ps;
					ResultSet rs = null;
					//TODO nie sprawdzone dla Postgresa i firebirda
					if(DSApi.isOracleServer() || DSApi.isPostgresServer() || DSApi.isFirebirdServer()){
						ps=DSApi.context().prepareStatement("select id, (EXTRACT(YEAR from ctime)) from ds_document "
								+ " where dockind_id="+DocumentKind.getDockindInfo(DocumentLogicLoader.ARCHIVES).getDockindId()
								+ " and (EXTRACT(YEAR from ctime))='"+cal.get(Calendar.YEAR)+"' ");
						
						rs=ps.executeQuery();
					}
					else if(DSApi.isSqlServer()){
						ps=DSApi.context().prepareStatement("select id, (Datepart(YEAR,ctime)) from ds_document "
								+ " where dockind_id="+DocumentKind.getDockindInfo(DocumentLogicLoader.ARCHIVES).getDockindId()
								+ " and (Datepart(YEAR,ctime))='"+cal.get(Calendar.YEAR)+"'");
						rs=ps.executeQuery();
					}
					if(rs.next()){ 
						//jest juz wygenerowany spis
						return;
					}
					else{
						// ustaw date: rok-2, styczen 01
						Calendar calendar= Calendar.getInstance();
						calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 2);
						calendar.set(Calendar.MONTH, /* 0-based */0);
						calendar.set(Calendar.DAY_OF_MONTH, /* 1-based */1);
						calendar.set(Calendar.HOUR_OF_DAY, /* 0-based */0);
						calendar.set(Calendar.MINUTE, /* 0-based */0);
						Date finishDate= calendar.getTime();

						noteLog("[Wykaz] Odpalam NewCaseArchiverAction::run, sprawdzam sprawy z data < " + finishDate.toString());



						generujWykazy(finishDate, null);
					}
				} catch (EdmException e) {
					noteLog("[Wykaz] NewCaseArchiverAction::run Niepowodzenie");
					e.printStackTrace(System.out);

				} catch (SQLException e1) {
					noteLog("[Wykaz] NewCaseArchiverAction::run Niepowodzenie");
					e1.printStackTrace(System.out);
				} 
				DSApi.closeContextIfNeeded(ctx);
		//	}
		}
	}

	/**
	 * ustaw cykliczne wywolywanie - raz dziennie. Pierwszy run - niech przeczeka z 5 sekund, na konczenia inicjalizacji
	 * Tomcata
	 */
	@Override
	protected void start() throws ServiceException
	{	
		
		archiveTimer= new Timer(true);
		archiveTimer.schedule(new NewCaseArchiverAction(), 0, 5*DateUtils.MINUTE);
		/*protokolTimer= new Timer(true);
		protokolTimer.schedule(new ProtokolBrakowaniaAction(), 0, DateUtils.DAY);*/
		System.out.println("CaseArchiverScheduled - action scheduled");
	}

	public static void noteLog(String string)
	{
		if(__debug) System.out.println(string);
	}

	@Override
	protected void stop() throws ServiceException
	{
		System.out.println("CaseArchiverScheduled - stop uslugi");
		if (archiveTimer != null)
			archiveTimer.cancel();
		if (protokolTimer != null)
			protokolTimer.cancel();
	}

	/**
	 * uzytkownik nie moze zatrzymac
	 */
	@Override
	protected boolean canStop()
	{
		return false;
	}
	public String getKategoria()
	{
		return kategoria;
	}

	
	public void setKategoria(String kategoria)
	{
		this.kategoria = kategoria;
	}
}
