package pl.compan.docusafe.parametrization.archiwum;

import java.sql.Date;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.parametrization.utp.AbstractXlsReports;
import pl.compan.docusafe.util.DateUtils;


public class DokPublicUdostXlsRaport extends AbstractXlsReports{
	private String szukany;
	private Map<String, String> uzytkownicy;
	private Date ctimeOd;// data utworzenia od
	private Date ctimeDo;// data utworzenia do
	
	
	public DokPublicUdostXlsRaport(String reportName) {
		super(reportName);
		title = new String[] { "Raport pism publicznych." };

		columns = new String[] { "Lp.", "Numer ID", 
				"Opis","Data utworzenia","Autor"};
	}

	@Override
	protected void createEntries() {
		boolean ctx=false;
		try {
			int rowCount = 3;
			int lp = 1;
			ctx=DSApi.openContextIfNeeded();
			
			Criteria criteria= DSApi.context().session().createCriteria(Document.class);
			if (szukany != null && !szukany.equals("")) {
				criteria.add(Restrictions.eq("author", szukany));
			} else {
				criteria.add(Restrictions.in("author",uzytkownicy.keySet()));
			}
			
			 
		/*	if (ctimeOd != null) {
				criteria.add(Restrictions.ge("CTIME",DateUtils.formatSqlDateTime(DateUtils.parseDateAnyFormat((ctimeOd.toString())))));
			}
			if (ctimeDo != null) {
				criteria.add(Restrictions.le("CTIME",DateUtils.formatSqlDateTime(DateUtils.parseDateAnyFormat((ctimeDo.toString())))));
			}*/
			criteria.add(Restrictions.eq("accessToDocument", Long.parseLong("1")));
			
			List dokumenty=criteria.list();
			
			for(Object obj:dokumenty){
				Document doc=(Document)obj;
				if(ctimeOd==null || doc.getCtime().after(DateUtils.parseDateAnyFormat((ctimeOd.toString())))){

					if( ctimeDo==null ||doc.getCtime().before(DateUtils.parseDateAnyFormat((ctimeDo.toString())))){

						int i = 0;
						HSSFRow row = sheet.createRow(rowCount++);
						row.createCell(i++).setCellValue(
								new HSSFRichTextString(lp++ + ""));
						row.createCell(i++).setCellValue(
								new HSSFRichTextString(doc.getId().toString()));
						row.createCell(i++).setCellValue(
								new HSSFRichTextString(doc.getDescription()));
						row.createCell(i++).setCellValue(
								new HSSFRichTextString(doc.getCtime().toString()));
						row.createCell(i++).setCellValue(
								new HSSFRichTextString(doc.getAuthor()));
					}}}

			
			DSApi.closeContextIfNeeded(ctx);
		} catch (HibernateException e) {
			Logger.getLogger(DokPublicUdostXlsRaport.class.getName())
			.log(Level.SEVERE, null, e);
			DSApi.closeContextIfNeeded(ctx);
		} catch (EdmException ex) {
			Logger.getLogger(DokPublicUdostXlsRaport.class.getName())
			.log(Level.SEVERE, null, ex);
			DSApi.closeContextIfNeeded(ctx);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void setSzukany(String szukany) {
		this.szukany=szukany;
		
	}

	public void setUzytkownicy(Map<String, String> uzytkownicyPodlegli) {
		this.uzytkownicy=uzytkownicyPodlegli;
		
	}

	public Date getCtimeOd() {
		return ctimeOd;
	}

	public void setCtimeOd(Date ctimeOd) {
		this.ctimeOd = ctimeOd;
	}

	public Date getCtimeDo() {
		return ctimeDo;
	}

	public void setCtimeDo(Date ctimeDo) {
		this.ctimeDo = ctimeDo;
	}

}
