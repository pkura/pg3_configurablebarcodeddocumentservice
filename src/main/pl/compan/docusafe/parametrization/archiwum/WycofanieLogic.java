package pl.compan.docusafe.parametrization.archiwum;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.Container.ArchiveStatus;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class WycofanieLogic  extends AbstractDocumentLogic{

	protected static Logger log = LoggerFactory.getLogger(WycofanieLogic.class);
	private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
	private static WycofanieLogic instance;
	public final static String _USER = "DSUSER";
	public static String CN_DATA_DOKUMENTU ="DOC_DATE";
	public static String CN_AUTOR_DOKUMENTU ="DOC_AUTOR";
	public static String CN_DZIAL_AUTORA_DOKUMENTU ="DOC_AUTORDIVISION";
	public static String CN_TYP_DOKUMENTU = "TYP_DOKUMENTU";
	public static String CN_ABSTRAKT_DOKUMENTU = "ABSTRAKT";
	public static String CN_MIESCEUTWOORZENIA_DOKUMENTU = "MIESCEUTWOORZENIA";
	public static String CN_DOC_DESCRIPTION = "DOC_DESCRIPTION";  
	   public static final String DIVISION_CN = "DEPARTUSER";
	private static final String _Stanowisko_ID = 
			"select ID, GUID from DS_DIVISION where PARENT_ID = ? and name = ? and hidden = ?";
	private static final String _Kierownik_ID = 
			"select USER_ID from DS_USER_TO_DIVISION where DIVISION_ID = ?";
	public static WycofanieLogic getInstance()
	{
		if (instance == null)
			instance= new WycofanieLogic();
		return instance;
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {


	}
	public void setInitialValues(FieldsManager fm, int type)
			throws EdmException {
		//log.info("type: {}", type);
		Map<String, Object> values = new HashMap<String, Object>();


		values.put(CN_DATA_DOKUMENTU, new Date());
		values.put(CN_TYP_DOKUMENTU, 2);
		values.put(CN_DOC_DESCRIPTION, "Wniosek o wycofanie dokumentacji");
		values.put("data_zlozenia", new Date());
		values.put("data_wycofania", new Date());
		long user = DSApi.context().getDSUser().getId();
		DSUser login = DSUser.findById(user);
		DSDivision[] dc = login.getDivisionsWithoutGroupPosition();
		Long userDivision = (long) 1;

		if(dc.length>1){
			fm.getField(("DEPARTUSER")).setReadOnly(false);
		} else {
			fm.getField(("DEPARTUSER")).setReadOnly(true);
			for(int i=0;i<dc.length;i++){
				userDivision = dc[i].getId();
			}
		}
		values.put("DEPARTUSER", userDivision);
		values.put(_USER, DSApi.context().getDSUser().getId());
		fm.reloadValues(values);
	}


	@Override
	public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException{
		
		
		if (values.get(CN_DZIAL_AUTORA_DOKUMENTU) != null && values.get(DIVISION_CN) != null)
		{
			long al = (Long) values.get(CN_DZIAL_AUTORA_DOKUMENTU);
			int ul = (Integer) values.get(DIVISION_CN);
			if (al != ul)
				values.put(CN_DZIAL_AUTORA_DOKUMENTU, Long.parseLong(String.valueOf(ul)));
		}
		
		if(documentId != null){
			Document doc = Document.find(documentId);
			FieldsManager fm = doc.getFieldsManager();
			/*if(values.get("wybor_spraw") != null && fm.getEnumItem("STATUSDOKUMENTU").getId() == 1){
				Long id = (long)(Integer) values.get("wybor_spraw");
				try {
					OfficeCase offCase = OfficeCase.find(id);
					offCase.getDivisionGuid();
					DSDivision div = DSDivision.find(offCase.getDivisionGuid());
					DSDivision[] divParent = DSDivision.getAllDivisionsDesc();
					String stanowisko = Docusafe.getAdditionProperty("archiwum.jbpm.spiszdawczoodbiorczy.kierownicy");
					long divId = 0;
					if(div.getParent().getId() != 0){
						divId = div.getParent().getId(); 
					} else {
						divId = div.getId();
					}
					long guid = 0;
					for(int i=0;i<divParent.length;i++){
						if(divParent[i].getId().equals(divId)){
							PreparedStatement ps;
							try {
								ps = DSApi.context().prepareStatement(_Stanowisko_ID);

								ps.setLong(1,divId);
								ps.setString(2, stanowisko);
								ps.setBoolean(3, false);

								ResultSet rs = ps.executeQuery();
								rs.next();
								if(rs.getInt(1) != 0){
									guid = rs.getInt(1);
								}
								ps.close();
								rs.close();
							} catch (SQLException e) {

							}
							if(guid != 0){
								guid = divParent[i].getId();
								break;
							} else if(divParent[i].getParent() != null){
								divId = divParent[i].getParent().getId();	

							}

						}

					}

					values.put("sprawa", offCase.getTitle());
					values.put("DSDIVISION", guid);
					values.put("sprawaId", id);
					
					offCase.setArchiveStatus(ArchiveStatus.Withdraw);
					fm.reloadValues(values);

				} catch (EdmException e) {
					log.error(e.getMessage(), e);
				}
			}*/
			if(fm.getValue("sprawaId") != null && fm.getEnumItem("STATUSDOKUMENTU").getId() == 4){
				Long id = Long.valueOf(fm.getValue("sprawaId").toString());
				OfficeCase offCase = OfficeCase.find(id);
				offCase.getDivisionGuid();
				offCase.setArchiveStatus(ArchiveStatus.Archived);
			}
		}
		if(values.get("data_od") != null && values.get("data_do") != null){
			Date data_od = (Date) values.get("data_od");
			Date data_do = (Date) values.get("data_do");
			if(data_do.before(data_od))
				throw new EdmException(sm.getString("DataOdDo"));
		}
	
		}
	@Override
	public void setAfterCreateBeforeSaveDokindValues(DocumentKind kind, Long id, Map<String, Object> dockindKeys) throws EdmException
	{
		if (dockindKeys.get(CN_DZIAL_AUTORA_DOKUMENTU) != null && dockindKeys.get(DIVISION_CN) != null)
		{
			long al = (Long) dockindKeys.get(CN_DZIAL_AUTORA_DOKUMENTU);
			int ul = (Integer) dockindKeys.get(DIVISION_CN);
			if (al != ul)
				dockindKeys.put(CN_DZIAL_AUTORA_DOKUMENTU, Long.parseLong(String.valueOf(ul)));
		}
		
		if(id != null){
			Document doc = Document.find(id);
			FieldsManager fm = doc.getFieldsManager();
			if(dockindKeys.get("wybor_spraw") != null && fm.getEnumItem("STATUSDOKUMENTU").getId() == 1){
				Long sprawaId = (long)(Integer) dockindKeys.get("wybor_spraw");
				try {
					OfficeCase offCase = OfficeCase.find(sprawaId);
					offCase.getDivisionGuid();
					DSDivision div = DSDivision.find(offCase.getDivisionGuid());
					DSDivision[] divParent = DSDivision.getAllDivisionsDesc();
					String stanowisko = Docusafe.getAdditionProperty("archiwum.jbpm.spiszdawczoodbiorczy.kierownicy");
					long divId = 0;
					if(div.getParent().getId() != 0){
						divId = div.getParent().getId(); 
					} else {
						divId = div.getId();
					}
					long guid = 0;
					for(int i=0;i<divParent.length;i++){
						if(divParent[i].getId().equals(divId)){
							PreparedStatement ps;
							try {
								ps = DSApi.context().prepareStatement(_Stanowisko_ID);

								ps.setLong(1,divId);
								ps.setString(2, stanowisko);
								ps.setBoolean(3, false);

								ResultSet rs = ps.executeQuery();
								rs.next();
								if(rs.getInt(1) != 0){
									guid = rs.getInt(1);
								}
								ps.close();
								rs.close();
							} catch (SQLException e) {

							}
							if(guid != 0){
								guid = divParent[i].getId();
								break;
							} else if(divParent[i].getParent() != null){
								divId = divParent[i].getParent().getId();	

							}

						}

					}

					dockindKeys.put("sprawa", offCase.getTitle());
					dockindKeys.put("DSDIVISION", guid);
					dockindKeys.put("sprawaId", sprawaId);
					dockindKeys.remove("wybor_spraw");
					offCase.setArchiveStatus(ArchiveStatus.Withdraw);
					fm.reloadValues(dockindKeys);

				} catch (EdmException e) {
					log.error(e.getMessage(), e);
				}
			}
			if(fm.getValue("sprawaId") != null && fm.getEnumItem("STATUSDOKUMENTU").getId() == 4){
				Long sprawaId = Long.valueOf(fm.getValue("sprawaId").toString());
				OfficeCase offCase = OfficeCase.find(sprawaId);
				offCase.getDivisionGuid();
				offCase.setArchiveStatus(ArchiveStatus.Archived);
			}
			if(fm.getValue("sprawaId") != null && fm.getEnumItem("STATUSDOKUMENTU").getId() == 5){
				dockindKeys.remove("wybor_spraw");
			}
		}
		if(dockindKeys.get("data_od") != null && dockindKeys.get("data_do") != null){
			Date data_od = (Date) dockindKeys.get("data_od");
			Date data_do = (Date) dockindKeys.get("data_do");
			if(data_do.before(data_od))
				throw new EdmException(sm.getString("DataOdDo"));
		}
	}
	@Override
	public void onSaveActions(DocumentKind kind, Long documentId,
			Map<String, ?> fieldValues) throws SQLException, EdmException {

		Document document = Document.find(documentId);
		FieldsManager fm = document.getFieldsManager();
		long numerProtokolu = 0;
		if(fieldValues.get("STATUSDOKUMENTU") != null){
			if(fieldValues.get("STATUSDOKUMENTU").equals("3")){
				if(documentId != null){
					PreparedStatement ps = null;
					ps = DSApi.context().prepareStatement("SELECT MAX(numer_protokolu) as numer from DS_AR_WYCOFANIE ");
					ResultSet rs = ps.executeQuery();
					rs.next();
					numerProtokolu = rs.getLong("numer");
					if(fm.getValue("numer_protokolu") == null){
						ps = DSApi
								.context()
								.prepareStatement(
										"UPDATE DS_AR_WYCOFANIE set numer_protokolu=? where DOCUMENT_ID=?");
						ps.setLong(2, documentId);
						ps.setLong(1, numerProtokolu + 1);
						ps.executeUpdate();
					}
				}
			}
			if(fm.getValue("sprawaId") != null && fm.getEnumItem("STATUSDOKUMENTU").getId() == 5){
				fieldValues.remove("wybor_spraw");
			}
		}
	}

	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms) {
			if (perm.isCreateGroup()
					&& perm.getSubjectType().equalsIgnoreCase(
							ObjectPermission.GROUP)) {
				String groupName = perm.getGroupName();
				if (groupName == null) {
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) {
		log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
		try {
			Map<String, Object> map = Maps.newHashMap();
			if (document instanceof InOfficeDocument) {
				map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
			} else {
				map.put(ASSIGN_USER_PARAM,
						event.getAttribute(ASSIGN_USER_PARAM));
				map.put(ASSIGN_DIVISION_GUID_PARAM,
						event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			}

			document.getDocumentKind().getDockindInfo()
			.getProcessesDeclarations().onStart(document, map);

			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	public void setAdditionalTemplateValues(long docId,
			Map<String, Object> values) {

		try {
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			fm.initialize();
			if(fm.getValue("STATUSDOKUMENTU") != null){	
				values.put("STATUSDOKUMENTU", fm.getValue("STATUSDOKUMENTU"));
			} else {
				values.put("STATUSDOKUMENTU", "");
			}
			if(fm.getValue("data_zlozenia") != null){
				values.put("data_zlozenia", fm.getValue("data_zlozenia"));
			} else {
				values.put("data_zlozenia", "");
			}
			if(fm.getValue("DSUSER") != null){
				values.put("DSUSER", fm.getValue("DSUSER"));
			} else {
				values.put("DSUSER", "");
			}
			if(fm.getValue("DSDIVISION") != null){
				values.put("DSDIVISION", fm.getValue("DSDIVISION"));
			} else {
				values.put("DSDIVISION", "");
			}
			if(fm.getValue("sygnatura_archiwalna") != null){
				values.put("sygnatura_archiwalna", fm.getValue("sygnatura_archiwalna"));
			} else {
				values.put("sygnatura_archiwalna", "");
			}
			if(fm.getValue("data_od") != null){
				values.put("data_od", fm.getValue("data_od"));
			} else{
				values.put("data_od", "");
			}
			if(fm.getValue("") != null){
				values.put("data_do", fm.getValue("data_do"));
			}else {
				values.put("data_do", "");
			}
			if(fm.getValue("tytul_sprawy") != null){
				values.put("tytul_sprawy", fm.getValue("tytul_sprawy"));
			} else {
				values.put("tytul_sprawy", "");
			}
			if(fm.getValue("uzasadnienie_wycofania") != null){
				values.put("uzasadnienie_wycofania", fm.getValue("uzasadnienie_wycofania"));
			} else {
				values.put("uzasadnienie_wycofania", "");
			}
			if(fm.getValue("powod_odrzucenia") != null){
				values.put("powod_odrzucenia", fm.getValue("powod_odrzucenia"));
			} else {
				values.put("powod_odrzucenia", "");
			}
			if(fm.getValue("numer_protokolu") != null){
				values.put("numer_protokolu", fm.getValue("numer_protokolu"));
			}else {
				values.put("numer_protokolu", "");

			}
			if(fm.getValue("data_wycofania") != null){
				values.put("data_wycofania", fm.getValue("data_wycofania"));
			} else {
				values.put("data_wycofania", fm.getValue("data_wycofania"));

			}
			if (fm.getValue("wybor_spraw") != null)
			{
				values.put("wybor_spraw", fm.getValue("wybor_spraw"));
			} else
			{
				if (fm.getValue("wybor_spraw") == null && (fm.getValue("sprawaId") != null))
					values.put("wybor_spraw", fm.getValue("sprawaId"));
				else
					values.put("wybor_spraw", "");
			}
		} catch (DocumentNotFoundException e) {
			log.error(e.getMessage(), e);
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}

	}
	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) {

		
		return null;
	}

	public static String stanowiskoUser(int userName, Long departament, String stanowisko){
		try
		{
			//DSApi.openAdmin();
			Long idStanowiska = 0L;
			List<String> kierownicy = new ArrayList<String>();
			Long idKierownika = 0L;
			PreparedStatement ps = DSApi.context().prepareStatement(_Stanowisko_ID);
			ps.setLong(1,departament);
			ps.setString(2, stanowisko);
			ps.setBoolean(3, false);

			ResultSet rs = ps.executeQuery();
			rs.next();
			String guid = rs.getString(2);
			ps.close();
			rs.close();

			//DSApi._close();
			return guid;
		}catch(Exception e){
			//log.error("", e);
			return null;
		}

	}

}
