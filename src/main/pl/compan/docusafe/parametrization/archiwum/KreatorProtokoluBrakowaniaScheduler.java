package pl.compan.docusafe.parametrization.archiwum;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.Field.Type;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.JBPMTaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.sql.DivisionImpl;
import pl.compan.docusafe.parametrization.archiwum.ProtokolBrakowaniaLogic.ProtokolBrakowaniaStale;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class KreatorProtokoluBrakowaniaScheduler extends ServiceDriver {
	private static final Log log = LogFactory.getLog(KreatorProtokoluBrakowaniaScheduler.class);
	private static final boolean __debug = false;
	private Timer protokolTimer;
	
	public class PozycjaBrakowaniaImpl extends PozycjaBrakowania {

		public PozycjaBrakowaniaImpl() {
		}
		
		public PozycjaBrakowaniaImpl(long document_id, long ds_division_id,
				String category_arch) {
			super(document_id, ds_division_id, category_arch);
		}
	}
	
	public abstract class PozycjaBrakowania {
		
		long document_id;
		long ds_division_id;
		String category_arch;
		
		Integer folderId;
		String numerWykazu;
		String symbolWykazu;
		String tytulWykazu;
		Date dataOd;
		Date dataDo;
		Integer liczbaTomow;
		Double metryBiezace;
		//
		String uwagi;
		
		public PozycjaBrakowania() {
			
		}
		
		public PozycjaBrakowania(long document_id, long ds_division_id, String category_arch) {
			this.document_id = document_id;
			this.ds_division_id = ds_division_id;
			this.category_arch = category_arch;
		}
		
		public long getDocument_id() {
			return document_id;
		}

		public void setDocument_id(long document_id) {
			this.document_id = document_id;
		}

		public long getDs_division_id() {
			return ds_division_id;
		}

		public void setDs_division_id(long ds_division_id) {
			this.ds_division_id = ds_division_id;
		}

		public String getCategory_arch() {
			return category_arch;
		}

		public void setCategory_arch(String category_arch) {
			this.category_arch = category_arch;
		}

		
		public String getNumerWykazu() {
			return numerWykazu;
		}

		public void setNumerWykazu(String numerWykazu) {
			this.numerWykazu = numerWykazu;
		}

		public String getSymbolWykazu() {
			return symbolWykazu;
		}

		public void setSymbolWykazu(String symbolWykazu) {
			this.symbolWykazu = symbolWykazu;
		}

		public String getTytulWykazu() {
			return tytulWykazu;
		}

		public void setTytulWykazu(String tytulWykazu) {
			this.tytulWykazu = tytulWykazu;
		}

		public Date getDataOd() {
			return dataOd;
		}

		public void setDataOd(Date dataOd) {
			this.dataOd = dataOd;
		}

		public Date getDataDo() {
			return dataDo;
		}

		public void setDataDo(Date dataDo) {
			this.dataDo = dataDo;
		}

		public Integer getLiczbaTomow() {
			return liczbaTomow;
		}

		public void setLiczbaTomow(Integer liczbaTomow) {
			this.liczbaTomow = liczbaTomow;
		}

		public Double getMetryBiezace() {
			return metryBiezace;
		}

		public void setMetryBiezace(Double metryBiezace) {
			this.metryBiezace = metryBiezace;
		}

		public Integer getFolderId() {
			return folderId;
		}

		public void setFolderId(Integer folderId) {
			this.folderId = folderId;
		}
	}

	/**
	 * Klasa tworz�ca protoko�y brakowania dla 
	 */
	class KreatorProtokoluBrakowaniaAction extends TimerTask {
		@Override
		public void run() {
			
			Calendar cal = Calendar.getInstance();
			try {
				if(cal.get(Calendar.MONTH) == 0 && cal.get(Calendar.DAY_OF_MONTH) == 1){
					if (__debug) System.out.println("[Brakowanie] KreatorProtokoluBrakowania Start");
					boolean contextOpened = false;
					try{
						contextOpened= DSApi.openContextIfNeeded();
						generujProtokolBrakowania();
					} finally {
						DSApi.closeContextIfNeeded(contextOpened);
					}

				}
			} catch (Exception e) {
				if (__debug) System.out.println("[Brakowanie] KreatorProtokoluBrakowaniaAction::run Niepowodzenie");
				log.error(e);
			}
		}
	}
	
	/**
	 * ustaw cykliczne wywolywanie - raz dziennie.
	 */
	@Override
	protected void start() throws ServiceException {
		protokolTimer = new Timer(true);
		protokolTimer.schedule(new KreatorProtokoluBrakowaniaAction(), 0,
				DateUtils.DAY);
	}

	@Override
	protected void stop() throws ServiceException {
		System.out.println("KreatorProtokoluBrakowania - stop uslugi");
		if (protokolTimer != null)
			protokolTimer.cancel();
	}

	/**
	 * uzytkownik nie moze zatrzymac
	 */
	@Override
	protected boolean canStop() {
		return false;
	}
	
	public void generujProtokolBrakowania() {
		List<DSDivision> dzialy = pobierzDzia�y();
		for (DSDivision dsDivision : dzialy) {
			if(utworzProtokolDlaDzialu(dsDivision))
				System.out.println("Utworzony protokol brakowania dla dzialu: " + dsDivision.getName());
			else
				System.out.println("Problem z utworzeniem protokolu dla dzia�u: " + dsDivision.getName());
		}
	}
	
	private boolean utworzProtokolDlaDzialu(DSDivision dsDivision) {
		try{
		List<PozycjaBrakowania> pozycjeBrakowania = pobierzDoBrakowania(dsDivision);
		
		String osoba = "system"; // TODO przypisac archiwist�
		String divisionGuid= dsDivision.getGuid();
		String opis= "Wygenerowano protokol brakowania dla dzialu " + dsDivision.getName();
		
		Map<String, Map<String, FieldData>> foldersMap= new HashMap();
		
//		Ustawiamy wartosci multislownika spisu, czyli liste folderow
		Integer folder_index= 1;
		
//		Ustawianie pol dla slownika BRAKOWANIE_POZYCJA
		for( PozycjaBrakowania pb : pozycjeBrakowania)
		{
			Map<String, FieldData> fields = new HashMap();
			foldersMap.put("BRAKOWANIE_POZYCJA_"+folder_index, fields);
			fields.put("numer_wykazu", new FieldData(Type.STRING, pb.getNumerWykazu()));
			Long id = OfficeFolder.findByOfficeId(pb.getSymbolWykazu()).getId();
			fields.put("symbol_wykazu", new FieldData(Type.STRING, String.valueOf(id)));
			fields.put("tytul_wykazu", new FieldData(Type.STRING, pb.getTytulWykazu()));
			fields.put("data_od", new FieldData(Type.DATE, pb.getDataOd()));
			fields.put("data_do", new FieldData(Type.DATE, pb.getDataDo()));
			fields.put("liczba_tomow", new FieldData(Type.INTEGER, pb.getLiczbaTomow()));
			fields.put("metry_biezace", new FieldData(Type.STRING, pb.getMetryBiezace()));
			
			fields.put("wybrakuj", new FieldData(Type.BOOLEAN, true));
			fields.put("ID", new FieldData(Type.STRING, ""));
			
			++folder_index;
		}
		
//		nie zostal dodany zaden element
		if(folder_index == 1)
		{
			if(__debug) System.out.println("[Brakowanie] Brak pozycji brakowania, nie tworzy wykazu");
			return false;
		}
//		Utworzenie dokumentu dla nowego protokolu brakowania
		OutOfficeDocument doc = new OutOfficeDocument();
		
		doc.setVersion(1);
		doc.setCzyAktualny(true);
		doc.setCzyCzystopis(false);
		doc.setCurrentAssignmentGuid(divisionGuid);
		doc.setDivisionGuid(divisionGuid);
		doc.setCurrentAssignmentAccepted(Boolean.FALSE);
		doc.setCurrentAssignmentUsername(osoba);
		doc.setCreatingUser(osoba);// DSUser.findByUsername("system")
		doc.setInternal(true);
		doc.setSummary(opis);
		doc.setForceArchivePermissions(false);
		doc.setAssignedDivision(divisionGuid);
		doc.setClerk(osoba);
		doc.setPriority(false);
		doc.setSource("");
		doc.create();

		
//		Tworzenie dockind-u
		DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.AR_PROT_BRAK);
		
//		Ustawianie pol dockind-u
		Map<String, Object> dockindFields= new HashMap();
//		status - ustawienie na "Utworzenie dokumentu"
		dockindFields.put(ProtokolBrakowaniaStale.CN_STATUS, 11);
//		rok
		dockindFields.put(ProtokolBrakowaniaStale.CN_ROK, Calendar.getInstance().get(Calendar.YEAR));
//		ds_division_id
		dockindFields.put(ProtokolBrakowaniaStale.CN_DZIAL, dsDivision.getId());
		

		dockindFields.put(DocumentKind.MULTIPLE_DICTIONARY_VALUES, foldersMap);

//		Powiazanie dockind-u z dokumentem
		doc.setDocumentKind(documentKind);

		Long newDocumentId= doc.getId();
		doc.setVersionGuid(newDocumentId.intValue());
		
		if (DwrDictionaryFacade.dictionarieObjects.get("BRAKOWANIE_POZYCJA") == null)
			DwrDictionaryFacade.dictionarieObjects.put(
					"BRAKOWANIE_POZYCJA",
					new DwrDictionaryBase(DocumentKind.findByCn(
							DocumentLogicLoader.AR_PROT_BRAK).getFieldByCn(
							"BRAKOWANIE_POZYCJA"), null));
							
		documentKind.saveDictionaryValues(doc, dockindFields);
		documentKind.saveMultiDictionaryValues(doc, dockindFields );
		
		documentKind.set(newDocumentId, dockindFields);
		doc.setAuthor(osoba);
		
//		Ustawienie numeru kancelaryjnego
		Journal journal = Journal.getMainInternal();
		Long journalId = journal.getId();
		DSApi.context().session().flush();
		Integer sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), GlobalPreferences.getCurrentDay());
		ActionEvent event= null;
		doc.bindToJournal(journalId, sequenceId, null, event);
		doc.setOfficeDate(GlobalPreferences.getCurrentDay());
		
		doc.getDocumentKind().logic().archiveActions(doc, 0);
		doc.getDocumentKind().logic().documentPermissions(doc);
		
		DSApi.context().session().save(doc);
		
		if(!DSApi.context().inTransaction())
			DSApi.context().begin();
		doc.getDocumentKind().logic().onStartProcess(doc);

		DSApi.context().session().saveOrUpdate(doc);
		
		DSApi.context().commit();
//		TaskSnapshot.updateByDocument(doc);
		DSApi.context().begin();
		JBPMTaskSnapshot.updateByDocument(doc);
		DSApi.context().commit();
		if( __debug) System.out.println("[Brakowanie] done creating "+opis);
		
		return true;
		
		} catch(Exception e){
			log.error(e);
		}
		return false;
	}

	/**
	 * @return lista wszystkich pozycji (teczek) do brakowania
	 */
	private List<PozycjaBrakowania> pobierzDoBrakowania() {
		return pobierzDoBrakowania(null);
	}
	
	private List<PozycjaBrakowania> pobierzDoBrakowania(DSDivision dsDivision) {
		String query = "SELECT * FROM ds_ar_do_brakowania_view"
				+ (dsDivision==null ? "" : " where ds_division_id = " + dsDivision.getId());	
		PreparedStatement ps;
		List<PozycjaBrakowania> list = null;
		try {			
			ps = DSApi.context().prepareStatement(query);
			ps.execute();
			ResultSet rs = ps.getResultSet();
			
			list = new ArrayList<KreatorProtokoluBrakowaniaScheduler.PozycjaBrakowania>();
			while (rs.next()) {
				//pozycja
				//sprawdzanie czy odle�a�o swoje w Archiwum zakladowym
				
				if(!czyWybrakowac(rs.getDate("ARCHIVEDDATE"), rs.getString("CATEGORY_ARCH")))
					continue;
				
				PozycjaBrakowania pb = new PozycjaBrakowaniaImpl();
				pb.setNumerWykazu(rs.getString("NUMBER_ITR"));
				//symbol
				pb.setSymbolWykazu(rs.getString("FOLDER_OFFICEID"));
//				pb.setSymbolWykazu(rs.getString("FOLDER_ID"));
				//klasa jrwa
				pb.setTytulWykazu(rs.getString("FOLDER_TITLE"));
				//data od
				pb.setDataOd(rs.getDate("DATE_FROM"));
				//data do
				pb.setDataDo(rs.getDate("DATE_TO"));
				//liczba tomow
				pb.setLiczbaTomow(rs.getInt("FOLDER_COUNT"));
				//metrybierzace				
				pb.setMetryBiezace(rs.getDouble("FOLDER_LENGTH_METER"));
				//id
				pb.setFolderId(rs.getInt("FOLDER_ID"));
				
				list.add(pb);
			}
			ps.close();
			return list;
		} catch (Exception e) {
			log.error("B��d pobierania pozycji do brakowania");
		}
		return list;
	}
		
	private boolean czyWybrakowac(java.sql.Date date, String archCategory) {
		Calendar cal = Calendar.getInstance();
		String regex = "([a-z]|[A-Z])+\\d{1,2}";
		int czasPrzechowywania;

		cal.setTime(date);
		cal.add(Calendar.YEAR, 1);
		if(archCategory.equalsIgnoreCase("bc") || archCategory.equalsIgnoreCase("be")) czasPrzechowywania = 2;
		else if(archCategory.equalsIgnoreCase("b")) czasPrzechowywania = 25;
		else if (archCategory.split("-").length >= 2) {
			czasPrzechowywania = Integer.valueOf(archCategory.split("-")[1]);
		} else if (archCategory.matches(regex)){
			String years = archCategory.replaceAll("[a-z|A-Z]{1,2}", "");
			czasPrzechowywania = Integer.valueOf(years);
		} else {
			log.error("B��d podczas okre�lania daty przechowywania");
			return false;
		}
		
		Calendar dzis = Calendar.getInstance();
		cal.add(Calendar.YEAR, czasPrzechowywania);
		if(cal.get(Calendar.YEAR) <= dzis.get(Calendar.YEAR))
			return true;
		return false;
	}

	/**
	 * Pobiera dzia�y w ktorych s� dokumenty do brakowania.
	 * @return
	 */
	private List<DSDivision> pobierzDzia�y() {
		List<DSDivision> returnList = null;
		String query = "SELECT * FROM ds_ar_do_brakowania_view";
		PreparedStatement ps;
		try {
			ps = DSApi.context().prepareStatement(query);
			ps.execute();
			ResultSet rs = ps.getResultSet();
			DSDivision division;
			returnList = new ArrayList<DSDivision>();
			while (rs.next()) {
				Integer id = rs.getInt("ds_division_id");
				division = DivisionImpl.findById(id);
				if (!returnList.contains(division))
					returnList.add(division);
				division = null;
			}
			ps.close();
			return returnList;
		} catch (Exception e) {
			log.error("B��d pobierania pozycji do brakowania", e);
		}
		return returnList;
	}
}
