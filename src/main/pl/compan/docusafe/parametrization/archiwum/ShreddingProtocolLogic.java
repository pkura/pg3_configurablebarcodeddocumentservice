package pl.compan.docusafe.parametrization.archiwum;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

public class ShreddingProtocolLogic extends AbstractDocumentLogic {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static ShreddingProtocolLogic instance;
	protected static Logger log = LoggerFactory.getLogger(ShreddingProtocolLogic.class);
	public final static String BARCODE = "BARCODE";
	public final static String INCOMINGDATE = "INCOMINGDATE";
	public static final String LINKS_CN = "LINKS";
	public static ShreddingProtocolLogic getInstance() {
		if (instance == null)
			instance = new ShreddingProtocolLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));
		
		for (PermissionBean perm : perms) {
			if (perm.isCreateGroup()
					&& perm.getSubjectType().equalsIgnoreCase(
							ObjectPermission.GROUP)) {
				String groupName = perm.getGroupName();
				if (groupName == null) {
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	public void setInitialValues(FieldsManager fm, int type)
			throws EdmException {
		log.info("type: {}", type);
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("TYPE", type);
		DSUser user = DSApi.context().getDSUser();
		
		if (values.get("data") == null) {
			values.put("data", new Date());
		} else {
			values.put("data", null);
		}

		fm.reloadValues(values);
	}

	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) {
		/*FieldData fd = values.get("DWR_LINKS");
		Field msg = null;
//		values.put("DWR_LINKS_LINK", new FieldData(Field.Type.HTML, "http://google.pl"));
	
		Map<String, FieldData> map = values.get("DWR_LINKS").getDictionaryData();
		Iterator iterator = map.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry mapEntry = (Map.Entry) iterator.next();
		}
		for(String m:map.keySet()){
			FieldData as = values.get("LINKS_ID");
			String asd="tset";
		}
		String link = fd.getStringData();
		
		
		if(fd == null)*/
			return null; 
			

		/*if(link != null && !link.isEmpty())
		{
			msg = new Field("messageField", "B喚dny link ", Field.Type.BOOLEAN);
			values.put("DWR_ID", new FieldData(Field.Type.STRING, "http://" + link.replace(" ", "")));
		}
		
		//ustawianie wartosci dla link podczas przeladowywania dokumentu,
		values.put("DWR_LINKS_LINK", new FieldData(Field.Type.STRING, "http://onet.pl"));
		
		return msg;*/
	}

	@Override
	public void validate(Map<String, Object> values, DocumentKind kind,
			Long documentId) throws EdmException {
/*
		Date data = (Date) values.get("DOC_DATE");
		if (data == null) {
			data = new Date();

			values.put("DOC_DATE", " dokument bez daty");
		} else {
			values.put("DOC_DATE", data);
		}
*/
	}

	
	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {
		as.useFolderResolver(document);
		as.useAvailableProviders(document);
		log.info("document title : '{}', description : '{}'",
				document.getTitle(), document.getDescription());
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) {
		log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
		try {
			Map<String, Object> map = Maps.newHashMap();
			if (document instanceof InOfficeDocument) {
				map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
			} else {
				map.put(ASSIGN_USER_PARAM,
						event.getAttribute(ASSIGN_USER_PARAM));
				map.put(ASSIGN_DIVISION_GUID_PARAM,
						event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			}

			
			document.getDocumentKind().getDockindInfo()
					.getProcessesDeclarations().onStart(document, map);

			String barcode = document.getFieldsManager()
					.getStringValue(BARCODE);

			if (document instanceof InOfficeDocument) {
				InOfficeDocument in = (InOfficeDocument) document;
				if (barcode != null) {
					in.setBarcode(barcode);
				} else {
					in.setBarcode("Brak");
				}
			} else if (document instanceof OutOfficeDocument) {
				OutOfficeDocument od = (OutOfficeDocument) document;
				if (barcode != null) {
					od.setBarcode(barcode);
				} else {
					od.setBarcode("Brak");
				}
			}
			


			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		 
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public void onSaveActions(DocumentKind kind, Long documentId,
			Map<String, ?> fieldValues) throws SQLException, EdmException {

		OfficeDocument document = OfficeDocument.find(documentId);
		FieldsManager fm = document.getFieldsManager();

		if (document.getDocumentKind().getCn().equals("normal_out")) {
			if (fieldValues.containsKey("NUMER_KO")
					&& fieldValues.get("NUMER_KO") != null) {
				try {
					Integer nko = Integer.parseInt(((String) fieldValues
							.get("NUMER_KO")));
					document.setOfficeNumber(nko);

				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}
		} else if (document.getDocumentKind().getCn().equals("normal")) {
			if (fieldValues.containsKey("STATUS_REALIZACJI")) {
				try {

				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}
		}
	}

	public void setAdditionalTemplateValues(long docId,
			Map<String, Object> values) {

		try {
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			fm.initialize();
			String number;
			String allnumber = " ";
			List<Map> lista = (List<Map>) fm.getValue("NUMBER_DICT");
			try {
				for (Map m : lista) {
					number = (String) m.get("NUMBER_DICT_CN");
					allnumber += number + ",  ";

				}
				values.put("DOC_NUMBER", allnumber);
			} catch (Exception e) {

				values.put("DOC_NUMBER", "Brak numeru dokumentu");
			}

			try {
				if (fm.getValue("DOC_DATE") != null) {
					values.put("DATA", fm.getValue("DOC_DATE"));
				} else {
					values.put("DATA", " dokument bez daty");
				}
			} catch (Exception e) {

			}

			// numer KO
			try {
				values.put("KO", doc.getOfficeNumber().toString());
			} catch (Exception e) {
				values.put("KO", "-");

			}
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}

	}

	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document)
			throws EdmException {
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	@Override
	public void onRejectedListener(Document doc) throws EdmException {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAcceptancesListener(Document doc, String acceptationCn)
			throws EdmException {
		// TODO Auto-generated method stub

	}

	public TaskListParams getTaskListParams(DocumentKind dockind,
			long documentId) throws EdmException {
		TaskListParams params = new TaskListParams();
		try {
			InOfficeDocument doc = InOfficeDocument
					.findInOfficeDocument(documentId);
			if (doc.getDelivery() != null)
				params.setCategory(doc.getDelivery().getName());
		} catch (Exception e) {
			log.error("", e);
		}
		return params;
	}
}

