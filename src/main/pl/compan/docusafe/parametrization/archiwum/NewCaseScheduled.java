package pl.compan.docusafe.parametrization.archiwum;

import java.math.BigDecimal;
import java.security.acl.Permission;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.DocumentLockMode;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.Field.Type;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.reports.OutDocumentsFineReport;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.hql.ast.tree.FromClause;

import java.util.Iterator;

//TODO zmodyfikowac zapytanie selecta do bazy, ujac w nim sytuacje w ktorej teczki maja swoje podteczki,a podteczki 
//moga miec rowniez swoje podteczki
//TODO generowac oddzielny spis dla gurpy A I B
//ZALOZENIA: z automatycznego spisu teczek nie mozna usuwac teczki

public class NewCaseScheduled// extends ServiceDriver{
{
	/*
	private final static String selectQuery = "";
	private final static Logger log = LoggerFactory.getLogger(NewCaseScheduled.class);
	
	private Timer createTimer;
	
	class NewCaseAction extends TimerTask{

		@Override
		public void run() {
			try {
				DSApi.openAdmin();
				checkExecution();
			} catch (Exception e1) {
				log.error(e1.getMessage());
				e1.printStackTrace();
				DSApi.context()._rollback();
			}
			finally{
				DSApi._close();
			}
		}
		
	}
	
	class SpisZdawczoOdbiorczy{
		String divisionId;
		Map<String, Object> slownik;
		int iTeczka = 1;
		
		public SpisZdawczoOdbiorczy(String divisionId){
			this.divisionId = divisionId;
			
			slownik = new HashMap<String, Object>();
			slownik.put("IS_CATEGORY_A", new Integer(1));
			slownik.put("DSDIVISION", 120L);
			//slownik.put("DSUSER", 78L); 
			slownik.put("M_DICT_VALUES", new HashMap<String, Map<String, FieldData>>());
		}
		
		public void dodajTeczkeDoSpisu(TeczkaRecord teczka){
			Map<String, Map<String, FieldData>> m_dict_values = ((Map<String, Map<String, FieldData>>) slownik.get("M_DICT_VALUES"));
			Map<String, FieldData> teczkaFieldsMap = new HashMap<String, FieldData>();
			
			m_dict_values.put("DS_AR_INVENTORY_ITEM_"+(iTeczka++), teczkaFieldsMap);
			
			teczkaFieldsMap.put("ID", new FieldData( Type.STRING , "" ));
			teczkaFieldsMap.put("FOLDER_OFFICEID", new FieldData( Type.STRING , teczka.teczkaOfficeId ));
			teczkaFieldsMap.put("DATE_FROM", new FieldData( Type.STRING , teczka.caseOpenDateFrom ));
			teczkaFieldsMap.put("DATE_TO", new FieldData( Type.STRING , teczka.caseOpenDateTo ));
		}
	}
	
	private void checkExecution() throws Exception
	{
		int rok = Calendar.getInstance().get(Calendar.YEAR);
		
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date fromDate = df.parse("1-1-" + rok + " 00:00:00");
		Date toDate = df.parse("31-12-" + rok + " 23:59:59");
		Date finishDate = df.parse("1-1-" + (rok-2) + " 00:00:00");
		
		//Sprawdzamy czy w danym roku zostal wygenerowany spis zdawczo odbiorczy automatycznie
		Criteria criteria = DSApi.context().session().createCriteria(GeneracjaSpisu.class);
		criteria.add(Restrictions.isNull("divisionId"));
		criteria.add(Restrictions.eq("isExecuted", true));
		criteria.add(Restrictions.between("createdTime", fromDate, toDate));
		
		//Jesli spis nie zostal w danym roku wygenerowany to go generujemy
		if(criteria.list().isEmpty())
			stworzSpisZdawczoOdbiorczy(finishDate, null);
		
		Criteria criteria2 = DSApi.context().session().createCriteria(GeneracjaSpisu.class);
		
		criteria.add(Restrictions.eq("isExecuted", false));
		criteria.setProjection(Projections.projectionList().add(Projections.groupProperty("divisionId")));
		
		//lista komorek dla ktorych nastapilo polecenie wygenerowania spisu //TO NIE BEDZIE POTRZEBNE JESLI NIE BEDZIE MOZLIWOSCI USUNIECIA TECZKI ZE SPISU
		/*List<GeneracjaSpisu> listaKomorekOrganizacyjnych = criteria.list(); 
		
		for(GeneracjaSpisu a: listaKomorekOrganizacyjnych){
			stworzSpisZdawczoOdbiorczy(finishDate, a.getDivisionId());
		}* /
		
		//TODO
		//update'owac tabele *generation ze spis zostal dla danej komorki wygenerowany lub dodac wpis ze automatyczny zostal wygenerowany
	}
	
	private void stworzSpisZdawczoOdbiorczy(Date finishDate, String divisionGuid) throws Exception
	{
		try{
			DSApi.context().begin();
			
			Map<String, SpisZdawczoOdbiorczy> komorkaToSpis = new HashMap<String, SpisZdawczoOdbiorczy>();
			
			List<TeczkaRecord> listaTeczek = listaTeczek(finishDate, divisionGuid);
			
			for(TeczkaRecord teczka : listaTeczek){
				
				if(!komorkaToSpis.containsKey(teczka.caseDivisiondGuid))
					komorkaToSpis.put(teczka.caseDivisiondGuid, new SpisZdawczoOdbiorczy(teczka.caseDivisiondGuid));
			
				komorkaToSpis.get(teczka.caseDivisiondGuid).dodajTeczkeDoSpisu(teczka);
				
				odbierzUprawnieniaUzytkownikom(teczka.idTeczki);
				//log.error(teczka.caseDivisiondGuid);
			}		
			
			if( DwrDictionaryFacade.dictionarieObjects.get("DS_AR_INVENTORY_ITEM")==null)
				DwrDictionaryFacade.dictionarieObjects.put("DS_AR_INVENTORY_ITEM", 
						new DwrDictionaryBase(DocumentKind.findByCn(DocumentLogicLoader.ARCHIVES).getFieldByCn("DS_AR_INVENTORY_ITEM"),null ) );
			
			for(SpisZdawczoOdbiorczy spisZdawczoOdbiorczy: komorkaToSpis.values()){
				dodajSpisDoBazy(spisZdawczoOdbiorczy);
			}
			
			DSApi.context().commit();
		}catch (Exception e) {
			DSApi.context()._rollback();
			throw e;
		}
	}
	
	
	static class TeczkaRecord
	{
		public long	idTeczki;
		public String	caseOpenDateFrom;
		public String	caseOpenDateTo;
		public String	caseDivisiondGuid;
		public String	teczkaOfficeId;
		public TeczkaRecord(Object[] a){
			idTeczki= ((Number)a[0]).longValue();
			caseOpenDateFrom= a[1].toString();
			caseOpenDateTo= a[2].toString();
			caseDivisiondGuid= a[3].toString();
			teczkaOfficeId= a[4].toString();
		}
	}
	
	private List<TeczkaRecord> listaTeczek(Date finishDate, String divisionGuid) throws HibernateException, EdmException{
		/*
		 * Wszystkie sprawy w teczce musza byc zamkniete
		 * Dla kazdej organizacji oddzielnie
		 * Musi minac 2 lata
		 * /
		String query_data= "" +
				"SELECT SPRAWA.PARENT_ID, MIN(SPRAWA.OPENDATE) OPENDATE, "+
				"  MAX(SPRAWA.FINISHDATE) FINISHDATE, SPRAWA.DIVISIONGUID, TECZKA.OFFICEID" +
				" FROM DSO_CONTAINER SPRAWA" +
				"  JOIN dso_rwa RWA ON SPRAWA.RWA_ID=RWA.ID" +
				"  JOIN DSO_CONTAINER TECZKA ON SPRAWA.PARENT_ID=TECZKA.ID" +
				" WHERE SPRAWA.DISCRIMINATOR = 'CASE'" +
				"  AND RWA.achome LIKE 'A'" +
				"  AND SPRAWA.ARCHIVED = 0" + //zastanowic sie czy to musi tu wogole byc
				"  AND SPRAWA.PARENT_ID NOT IN" +
				"   (SELECT DISTINCT PARENT_ID FROM DSO_CONTAINER WHERE CLOSED = 0 AND DISCRIMINATOR = 'CASE')";
		
		if(divisionGuid!=null)
			query_data += " AND TECZKA.DIVISIONGUID=" + divisionGuid;
		
		query_data +=
				" GROUP BY SPRAWA.PARENT_ID, SPRAWA.DIVISIONGUID, TECZKA.OFFICEID" +
				" HAVING MAX(SPRAWA.FINISHDATE) < :finishDate";
		
		Query query = DSApi.context().session().createSQLQuery(query_data);
		query.setTimestamp("finishDate", finishDate);
		
		List<Object[]> qResult =query.list(); 
		List<TeczkaRecord> result = new ArrayList<TeczkaRecord>(qResult.size());
		for( Object[] record : qResult )
			result.add( new TeczkaRecord(record) );
		return result;
	}
	
	private void dodajSpisDoBazy(SpisZdawczoOdbiorczy spisZdawczoOdbiorczy) throws EdmException{
		OutOfficeDocument doc = new OutOfficeDocument();
		
		doc.setVersion(1);
		doc.setCzyAktualny(true);
		doc.setCzyCzystopis(false);
		doc.setSummary("");  //tu zmienic
		doc.setInternal(true);
		//doc.set
		doc.setCreatingUser("admin"); //tu zmienic
		
		//TODO poustawiac odpowiednie pola w obiekcie doc
		
		/*doc.setCurrentAssignmentGuid(divisionGuid);
		doc.setCurrentAssignmentAccepted(Boolean.FALSE);
		doc.setAssignedDivision(divisionGuid);

		doc.setDivisionGuid(divisionGuid);
		doc.setForceArchivePermissions(false); //nie wiem do czego to
		 * /
		
		doc.create();
		
		//TODO dodac wpis w tabeli NewCaseArchiverScheduled.wykazTableName
		DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.ARCHIVES);
		
		doc.setVersionGuid(doc.getId().intValue());
		doc.setDocumentKind(documentKind);

		documentKind.saveMultiDictionaryValues(doc, spisZdawczoOdbiorczy.slownik );
		documentKind.set(doc.getId(), spisZdawczoOdbiorczy.slownik);
		
		DSApi.context().session().save(doc);
		
		doc.getDocumentKind().logic().onStartProcess(doc);
		WorkflowFactory.createNewProcess(doc, false, "Wykaz zdawczo-odbiorczy nr "+"Numer");
	}
	
	//Zostaw uzytkownikom uprawnienia jedynie do odczytu
	private void odbierzUprawnieniaUzytkownikom(long idTeczki) throws ParseException, EdmException, HibernateException, SQLException{
		List<OfficeCase> officeCases = OfficeCase.findByParentId(idTeczki);

		for(OfficeCase officeCase: officeCases){
			List<OfficeDocument> listaDokumentow = officeCase.getDocuments();
		
			for(OfficeDocument doc: listaDokumentow){
				Set<PermissionBean> permissionBeans = DSApi.context().getDocumentPermissions(doc);
				
				for(PermissionBean permissionBean: permissionBeans){
					String permission = permissionBean.getPermission();
					
					if(!(permission.equals(ObjectPermission.READ) || permission.equals(ObjectPermission.READ_ATTACHMENTS) || permission.equals(ObjectPermission.READ_ORGINAL_ATTACHMENTS))){
						DSApi.context().revoke(doc, permissionBean);
					}
				}
			}		
		}
	}
	
	@Override
	protected void start() throws ServiceException {
		createTimer = new Timer(true);
		createTimer.schedule(new NewCaseAction(), 0, DateUtils.MINUTE);
		System.out.println("NewCaseScheduled - action scheduled");
	}

	@Override
	protected void stop() throws ServiceException {
		if(createTimer != null)
			createTimer.cancel();
	}

	@Override
	protected boolean canStop() {
		// TODO Auto-generated method stub
		return false;
	}
	*/

}
