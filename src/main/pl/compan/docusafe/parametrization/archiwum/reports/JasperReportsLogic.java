package pl.compan.docusafe.parametrization.archiwum.reports;

import com.jaspersoft.ireport.jasperserver.JServer;
import com.jaspersoft.ireport.jasperserver.ws.FileContent;
import com.jaspersoft.ireport.jasperserver.ws.WSClient;
import com.jaspersoft.jasperserver.api.metadata.xml.domain.impl.*;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.apache.commons.io.IOUtils;

import pl.compan.docusafe.api.user.office.JasperReportDto;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.*;

public class JasperReportsLogic {
    private static final Logger log = LoggerFactory.getLogger(JasperReportsLogic.class.getPackage().getName());

    private static final String JASPER_HOST = "jasperreports.host";
    private static final String JASPER_USERNAME = "jasperreports.username";
    private static final String JASPER_PASSWORD = "jasperreports.password";

    private File destination = null;
    private String fileName = "tempFile_";

    public JasperReportsLogic() {
    }

    public JasperReportsLogic(File destination, String fileName) {
        this.destination = destination;
        this.fileName = fileName;
    }

    public File getReport(JasperReportDto report) throws Exception {
        return getReport(report.getReportType(), report.getReport(),
                report.getStartTime().getTime(), report.getEndTime().getTime(), Long.valueOf(report.getUserId()));
    }

    public File getReport(String rodzajRaportu, String report, Date startDate, Date koniecDate, Long userId) throws Exception {
        JServer jServer = new JServer();
        jServer.setUsername(Docusafe.getAdditionProperty(JASPER_USERNAME));
        jServer.setPassword(Docusafe.getAdditionProperty(JASPER_PASSWORD));
        jServer.setUrl(Docusafe.getAdditionProperty(JASPER_HOST)+"/services/repository");

        WSClient client = new WSClient(jServer);
        ResourceDescriptor rd = new ResourceDescriptor();
        rd.setPassword(Docusafe.getAdditionProperty(JASPER_PASSWORD));
        rd.setUsername(Docusafe.getAdditionProperty(JASPER_USERNAME));
        rd.setUriString(report);
        rd.setIsNew(false);
        rd.setWsType(ResourceDescriptor.TYPE_CONTENT_RESOURCE);
        rd.setName("test");
        
        Map params = new HashMap();
        if (startDate != null){
            Long startDateLong = startDate.getTime();
//          rd.getParameters().add(new ListItem("start", startDateLong));
            params.put("start",startDateLong);
        }
        if (koniecDate != null){
            Long koniecDateLong = koniecDate.getTime();
//          rd.getParameters().add(new ListItem("koniec", koniecDateLong));
            params.put("koniec",koniecDateLong);
        }
        if (userId != null){
            params.put("zalogowany",userId);
        }
        List lista = new LinkedList();
        Argument arg = new Argument(Argument.RUN_OUTPUT_FORMAT,rodzajRaportu);
        lista.add(arg);

        Map response = client.runReport(rd,params,lista);
        FileContent fileContent = (FileContent) response.get("report");
        InputStream is = new ByteArrayInputStream(fileContent.getData());
        File file = null;

        if(destination == null)
            file = File.createTempFile(fileName, "."+rodzajRaportu.toLowerCase());
        else
            file = new File(destination, fileName + "."+rodzajRaportu.toLowerCase());


        FileOutputStream out = new FileOutputStream(file);
        IOUtils.copy(is, out);

        log.info("Test");
        return file;
    }
}
