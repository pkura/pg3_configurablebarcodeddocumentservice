--MSSQL
CREATE TABLE [dbo].[DS_JASPER_REPORTS_KIND](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PATH] [varchar](255) NOT NULL,
	[TITLE] [varchar](255) NOT NULL,
 CONSTRAINT [PK_DS_JASPER_REPORTS_KIND] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

--ORACLE
CREATE TABLE DS_JASPER_REPORTS_KIND(
	ID NUMBER(18, 0)NOT NULL,
	PATH varchar2(255) NOT NULL,
	TITLE varchar2(255) NOT NULL
);


CREATE SEQUENCE DS_JASPER_REPORTS_KIND_ID START WITH 1000 INCREMENT BY 1 NOCACHE;