package pl.compan.docusafe.parametrization.archiwum.reports;

import org.hibernate.CallbackException;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "DS_JASPER_REPORTS_KIND")
public class JasperReportsKind implements Lifecycle{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "PATH", nullable = false)
    private String path;

    @Column(name = "TITLE", nullable = false)
    private String title;

    public JasperReportsKind(){}

    public JasperReportsKind(String path,
                          String title) {

        this.setPath(path);
        this.setTitle(title);
    }


    /**
     * Wyszukanie rodzaju raportu po path
     * @param id
     * @return
     * @throws pl.compan.docusafe.core.EdmException
     */
    public static JasperReportsKind findByPath(String path) throws EdmException {
        JasperReportsKind result = (JasperReportsKind) (getCriteria()
                .add(Restrictions.eq("path", path))
                .uniqueResult());

        return result;
    }

    /**
     * Wyszukanie rodzaju raportu po identyfikatorze
     * @param id
     * @return
     * @throws pl.compan.docusafe.core.EdmException
     */
    public static JasperReportsKind findById(Long id) throws EdmException {
        List<JasperReportsKind> list = (List<JasperReportsKind>) (getCriteria()
                .add(Restrictions.eq("id", id)).list());

        if (list.size()==0)
            return null;
        else
            return list.get(0);
    }

    private static Criteria getCriteria() throws EdmException {
        return DSApi.context().session().createCriteria(JasperReportsKind.class);
    }

    /**
     * Wyszukanie pozycji faktury po id
     * @return lista dokumentow publicznie dostepnych
     * @throws pl.compan.docusafe.core.EdmException
     */
    public static List<JasperReportsKind> list() throws EdmException {
        Criteria criteria = getCriteria();
        return criteria.list();
    }

    public String toString() {
        return ""+id;
    }

    public final void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    @Override
    public boolean onDelete(Session arg0) throws CallbackException {
        return false;
    }

    @Override
    public void onLoad(Session arg0, Serializable arg1) {
    }

    @Override
    public boolean onSave(Session arg0) throws CallbackException {
        return false;
    }

    @Override
    public boolean onUpdate(Session arg0) throws CallbackException {
        return false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
