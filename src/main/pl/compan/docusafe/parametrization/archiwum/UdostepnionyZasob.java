package pl.compan.docusafe.parametrization.archiwum;

import org.hibernate.CallbackException;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.classic.Lifecycle;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

import javax.persistence.*;

import java.io.Serializable;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

@Entity
@Table(name = "DS_AR_UDOSTEPNIONE_ZASOBY")
public class UdostepnionyZasob implements Lifecycle{
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "USERNAME", nullable = false)
    private String username;

    @Column(name = "DATA_UDOSTEPNIENIA", nullable = false)
    private Date dataUdostepnienia;

    @Column(name = "DATA_ZWROTU", nullable = false)
    private Date dataZwrotu;

    @Column(name = "RODZAJ", nullable = false)
    private String rodzaj;

    @Column(name = "ZASOB_ID", nullable = false)
    private Long zasobId;

    public static final String DOKUMENT = "Document";
    public static final String SPRAWA = "OfficeCase";
    public static final String TECZKA= "OfficeFolder";

    public UdostepnionyZasob() {

    }

    public UdostepnionyZasob(String username, String rodzajZasobu, Long idZasobu, Date dataZwrotu) {
        this.setRodzaj(rodzajZasobu);
        Calendar cal = Calendar.getInstance();
        this.setDataUdostepnienia(new Date(cal.getTime().getTime()));
        this.setDataZwrotu(dataZwrotu);
        this.setZasobId(idZasobu);
        this.setUsername(username);
    }
    /**
     * Wyszukanie pozycji zasobu po id
     * @param id
     * @return
     * @throws pl.compan.docusafe.core.EdmException
     */
    public static UdostepnionyZasob findById(Long id) throws EdmException {
        return (UdostepnionyZasob)(DSApi.context().session().createCriteria(UdostepnionyZasob.class).add(Restrictions.eq("id", id)).list().get(0));
    }
    /**
     * Zwrocenie wszystkich zasobow
     * @return
     * @throws pl.compan.docusafe.core.EdmException
     */
    public static List<UdostepnionyZasob> findAll() throws EdmException{
    	return (List<UdostepnionyZasob>)(DSApi.context().session().createCriteria(UdostepnionyZasob.class).list());
    }



    public String toString() {
        return ""+id;
    }

    public final void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
        }
        catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    @Override
    public boolean onDelete(Session arg0) throws CallbackException {
        return false;
    }

    @Override
    public void onLoad(Session arg0, Serializable arg1) {
    }

    @Override
    public boolean onSave(Session arg0) throws CallbackException {
        return false;
    }

    @Override
    public boolean onUpdate(Session arg0) throws CallbackException {
        return false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getDataUdostepnienia() {
        return dataUdostepnienia;
    }

    public void setDataUdostepnienia(Date dataUdostepnienia) {
        this.dataUdostepnienia = dataUdostepnienia;
    }

    public Date getDataZwrotu() {
        return dataZwrotu;
    }

    public void setDataZwrotu(Date dataZwrotu) {
        this.dataZwrotu = dataZwrotu;
    }

    public String getRodzaj() {
        return rodzaj;
    }

    public void setRodzaj(String rodzaj) {
        this.rodzaj = rodzaj;
    }

    public Long getZasobId() {
        return zasobId;
    }

    public void setZasobId(Long zasobId) {
        this.zasobId = zasobId;
    }

    public static boolean isBorrowed(String user, String rodzajZasobu, Long zasobId) throws EdmException {
        Criteria c = DSApi.context().session().createCriteria(UdostepnionyZasob.class)
                .add(Restrictions.eq("username", user));
        c.add(Restrictions.eq("rodzaj", rodzajZasobu));
        c.add(Restrictions.eq("zasobId", zasobId));
        Date today = new Date(new java.util.Date().getTime());
        c.add(Restrictions.le("dataUdostepnienia",today));
        c.add(Restrictions.ge("dataZwrotu",today));

        if (c.list().size()>0)
            return true;
        else
            return false;
    }
}
