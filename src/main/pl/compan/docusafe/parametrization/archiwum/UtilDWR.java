package pl.compan.docusafe.parametrization.archiwum;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;

public class UtilDWR {

    /** @return TRUE jesli pole jest NULL albo zawiera NULL */
    public static boolean isEmptyField(FieldData pole){
        if (pole == null)
            return true;
        if ((pole.getData())==null)
            return true;
        return false;
    }

    /** @return TRUE jesli pole nie zawiera combo-boxu z wybranym jednym elementem */
    public static boolean isEnumNotSelected(FieldData pole){
        if (pole == null)
            return true;

        EnumValues combo = pole.getEnumValuesData();
        if (combo==null)
            return true;

        if ( "".equals(combo.getSelectedId()) )
            return true;
        return false;
    }

    /** @return Integer z wybranym indeksem z pola enum, lub null przy ka�dym mo�liwym niepowodzeniu */
    public static Integer getSelectedEnumId(FieldData pole) {
        try {
            if (pole == null)
                return null;

            EnumValues combo = pole.getEnumValuesData();
            if (combo==null)
                return null;

            String indeks = combo.getSelectedId();
            Integer liczba = Integer.parseInt(indeks);
            return liczba;
        }
        catch (Exception e){
            return null;
        }
    }

    /** @return TRUE jesli uda sie wybrac wskazany element w comboboxie */
    public static boolean selectEnum(FieldData pole, int selectId){
        if (pole == null)
            return false;

        EnumValues combo = pole.getEnumValuesData();
        if (combo==null)
            return false;

        combo.setSelectedOptions(monoLista(selectId));
        if (  (""+selectId).equals(combo.getSelectedId())  )
            return true;
        else
            return false;
    }

    /** Tworzy list� zawieraj�c� jeden String zrobiony ze zmiennej int.
     * Pomocne przy wybieraniu zaznaczonych pozycji w combobox */
    public static List<String> monoLista(int liczba){
        List<String> lista = new ArrayList<String>();
        lista.add(""+liczba);
        return lista;
    }

    /** Tworzy list� zawieraj�c� jeden String.
     * Pomocne przy wybieraniu zaznaczonych pozycji w combobox */
    public static List<String> monoLista(String liczba){
        List<String> lista = new ArrayList<String>();
        lista.add(liczba);
        return lista;
    }

    /** wpisuje pole pieni�ne do mapy Map<String, FieldData> jak w metodzie validateDwr */
    public static void valuesPutFieldData_Money(Map<String, FieldData> values, String key, BigDecimal newVal){
        if (values.containsKey(key)){
            FieldData fd = values.get(key);
            if (fd==null){
                values.put(key, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.MONEY, newVal));
            } else {
                fd.setMoneyData(newVal); //cos bylo w mapie, podmieniamy tylko dane
            }
        } else {
            values.put(key, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.MONEY, newVal));
        }
    }
    /** wpisuje pole daty do mapy jak w metodzie validateDwr **/
    public static void valuesPutFieldData_Date(Map<String, FieldData> values, String key, Date newVal){
        if (values.containsKey(key)){
            FieldData fd = values.get(key);
            if (fd==null){
                values.put(key, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.DATE, newVal));
            } else {
                fd.setDateData(newVal); //cos bylo w mapie, podmieniamy tylko dane
            }
        } else {
            values.put(key, new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.DATE, newVal));
        }
    }

    /** @return BigDecimal z pola albo NULL przy ka�dym mo�liwym niepowodzeniu */
    public static BigDecimal getBigDecimalSafely(FieldData pole) {
        try {
            if (pole == null) return null;
            return pole.getMoneyData();
        } catch (Exception e){
            return null;
        }
    }
    /** @return Long z pola albo NULL przy ka�dym mo�liwym niepowodzeniu */
    public static Long getLongSafely(FieldData pole) {
        try {
            if (pole == null) return null;
            return pole.getLongData();
        } catch (Exception e){
            return null;
        }
    }
}
