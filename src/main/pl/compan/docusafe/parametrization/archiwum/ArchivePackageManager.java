package pl.compan.docusafe.parametrization.archiwum;

import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.jackrabbit.JackrabbitAttachmentRevision;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.gov.mswia.standardy.ndap.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


/**
 * @author Jerzy Pir�g
 * @since 28-08-2013
 *
 * ArchivePackageManager - klasa, kt�rej zadaniem jest tworzenie paczki archiwalnej dla spraw
 */
public class ArchivePackageManager {
    private static final Logger log = LoggerFactory.getLogger(ArchivePackageManager.class.getPackage().getName());
    private static Marshaller marshaller;
    public ArchivePackageManager(){

    }

    public static File preparePackage(OfficeDocument doc) throws EdmException, IOException {
        JAXBContext jaxbContext;
        File file;
        File tempDir = Files.createTempDir();

        try{
            jaxbContext = JAXBContext.newInstance("pl.gov.mswia.standardy.ndap");
            marshaller = jaxbContext.createMarshaller();
        }catch (JAXBException e){
            log.error("B��d podczas tworzenia XML dla paczki archiwalnej: "+e);
        }

        List<Attachment> attachmentList = doc.getAttachments();
        String attFileName = "";
        if(attachmentList.size()==1){
            Attachment attachment = attachmentList.get(0);
            InputStream is = attachment.getMostRecentRevision().getBinaryStream();
            //tworzenie nazwy pliku z tytulu za��cznika i oryginalnego rozszerzenia
            String[] splitedOriginalFilename = attachment.getMostRecentRevision().getOriginalFilename().split("\\.");
            attFileName = attachment.getTitle()+"."+splitedOriginalFilename[splitedOriginalFilename.length-1];

            OutputStream os = new FileOutputStream(tempDir + File.separator + attFileName);
            IOUtils.copy(is, os);
            IOUtils.closeQuietly(is);
            IOUtils.closeQuietly(os);
        } else if(attachmentList.size()>1){
            attFileName = "doc_"+doc.getId();
            String subfolderPath = tempDir + File.separator + attFileName;
            new File(subfolderPath).mkdir();

            for(Attachment att : attachmentList){
                InputStream is = att.getMostRecentRevision().getBinaryStream();
                String[] splitedOriginalFilename = att.getMostRecentRevision().getOriginalFilename().split("\\.");
                String attachmentFilename = att.getTitle()+"."+splitedOriginalFilename[splitedOriginalFilename.length-1];

                OutputStream os = new FileOutputStream(subfolderPath + File.separator + attachmentFilename);
                IOUtils.copy(is, os);
                IOUtils.closeQuietly(is);
                IOUtils.closeQuietly(os);
            }
        }
        Dokument metaDok = createDocumentMetaFile(doc, doc.getContainingCase());
        try {
            createFile(metaDok, new File(tempDir+File.separator+attFileName+".xml"));
        }catch (JAXBException e){
            log.error("B��d podczas zapisu metadanych dokumentu: " + e);
        }

        String zippedFilePath = zipDirectory(tempDir);
        File zippedFile = new File(zippedFilePath);
        return zippedFile;
    }

    public static File getAttachmetnFileForDocument(Long docId) throws EdmException, IOException {
        OfficeDocument doc = OfficeDocument.find(docId);
        List<Attachment> attachmentList= doc.getAttachments();
        if (attachmentList.size()>1){
            File tempDir = Files.createTempDir();

            for (Attachment attachment : attachmentList){
                AttachmentRevision revision = attachment.getMostRecentRevision();
                InputStream is = null;
                try{
                    if (revision instanceof JackrabbitAttachmentRevision) {
                        is = ((JackrabbitAttachmentRevision) revision).getBinaryStream();
                    } else {
                        is = revision.getAttachmentStream();
                    }
                }catch (Exception e){
                    log.error("Error: "+ e.getStackTrace());
                }

                //tworzenie nazwy pliku z tytulu za��cznika i oryginalnego rozszerzenia
                String[] splitedOriginalFilename = attachment.getMostRecentRevision().getOriginalFilename().split("\\.");
                String attFileName = attachment.getTitle()+"."+splitedOriginalFilename[splitedOriginalFilename.length-1];

                OutputStream os = new FileOutputStream(tempDir + File.separator + attFileName);
                IOUtils.copy(is, os);
                IOUtils.closeQuietly(is);
                IOUtils.closeQuietly(os);
            }
            String zippedFilePath = zipDirectory(tempDir);
            File zippedFile = new File(zippedFilePath);
            return zippedFile;
        }else if(attachmentList.size()==1){
            Attachment attachment = attachmentList.get(0);
            AttachmentRevision revision = attachment.getMostRecentRevision();
            InputStream is = null;
            try{
                if (revision instanceof JackrabbitAttachmentRevision) {
                    is = ((JackrabbitAttachmentRevision) revision).getBinaryStream();
                } else {
                    is = revision.getAttachmentStream();
                }
            }catch (Exception e){
                log.error("Error: "+ e.getStackTrace());
            }
            String originalFilename = revision.getOriginalFilename();

            File file = new File(revision.getOriginalFilename());
            OutputStream os = new FileOutputStream(file);
            IOUtils.copy(is, os);
            IOUtils.closeQuietly(is);
            IOUtils.closeQuietly(os);

            return file;
        }else
            return null;
    }

    public File preparePackage(List<OfficeCase> listaSpraw) throws EdmException, IOException {
        JAXBContext jaxbContext;
        File file;
        File tempDir = Files.createTempDir();
        String dokumentyPath = tempDir.getAbsolutePath()+File.separator+"dokumenty";
        String metadanePath = tempDir.getAbsolutePath()+File.separator+"metadane";
        String sprawyPath = tempDir.getAbsolutePath()+File.separator+"sprawy";
        new File(dokumentyPath).mkdir();
        new File(metadanePath).mkdir();
        new File(sprawyPath).mkdir();

        try{
            jaxbContext = JAXBContext.newInstance("pl.gov.mswia.standardy.ndap");
            marshaller = jaxbContext.createMarshaller();
        }catch (JAXBException e){
            log.error("B��d podczas tworzenia XML dla paczki archiwalnej: "+e);
        }

        for(OfficeCase sprawa : listaSpraw)
        {
            Dokument metaDokSprawy = createCaseMetaFile(sprawa);
            try {
                createFile(metaDokSprawy, new File(sprawyPath+File.separator+"spr"+sprawa.getId().toString()+".xml"));
            } catch (JAXBException e) {
                log.error("B��d podczas zapisu metadanych sprawy: "+e);
            }
            List<OfficeDocument> documentListInOfficeCase = sprawa.getDocuments();
            for(OfficeDocument docs : documentListInOfficeCase){
                List<Attachment> attachmentList = docs.getAttachments();
                String attFileName = "";
                if(attachmentList.size()==1){
                    Attachment attachment = attachmentList.get(0);
                    InputStream is = attachment.getMostRecentRevision().getBinaryStream();
                    //tworzenie nazwy pliku z tytulu za��cznika i oryginalnego rozszerzenia
                    String[] splitedOriginalFilename = attachment.getMostRecentRevision().getOriginalFilename().split("\\.");
                    attFileName = attachment.getTitle()+"."+splitedOriginalFilename[splitedOriginalFilename.length-1];

                    OutputStream os = new FileOutputStream(dokumentyPath + File.separator + attFileName);
                    IOUtils.copy(is, os);
                    IOUtils.closeQuietly(is);
                    IOUtils.closeQuietly(os);
                } else if(attachmentList.size()>1){
                    attFileName = "doc_"+docs.getId();
                    String subfolderPath = dokumentyPath + File.separator + attFileName;
                    new File(subfolderPath).mkdir();

                    for(Attachment att : attachmentList){
                        InputStream is = att.getMostRecentRevision().getBinaryStream();
                        String[] splitedOriginalFilename = att.getMostRecentRevision().getOriginalFilename().split("\\.");
                        String attachmentFilename = att.getTitle()+"."+splitedOriginalFilename[splitedOriginalFilename.length-1];

                        OutputStream os = new FileOutputStream(subfolderPath + File.separator + attachmentFilename);
                        IOUtils.copy(is, os);
                        IOUtils.closeQuietly(is);
                        IOUtils.closeQuietly(os);
                    }
                }
                Dokument metaDok = createDocumentMetaFile(docs, sprawa);
                try {
                    createFile(metaDok, new File(metadanePath+File.separator+attFileName+".xml"));
                }catch (JAXBException e){
                    log.error("B��d podczas zapisu metadanych dokumentu: " + e);
                }
            }
        }
        String zippedFilePath = zipDirectory(tempDir);
        File zippedFile = new File(zippedFilePath);
        return zippedFile;
    }

    /**
     *
     * @param doc dokument kancelaryjny
    //     * @param officeCaseId znak sprawy, do kt�rej przynale�y dany dokument
     * @param sprawa obiekt sprawy, do kt�rej przynale�y dokument
     * @return obiekt dokumentu metadanych opisuj�cych ten za��cznik
     * @throws pl.compan.docusafe.core.EdmException
     */
    private static Dokument createDocumentMetaFile(OfficeDocument doc, OfficeCase sprawa) throws EdmException {
        if (doc == null)
            throw new EdmException("Brak wskazanego dokumentu, dla kt�rego ma by� utworzona paczka.");

        if (sprawa == null)
            throw new EdmException("Brak wskazanej sprawy, dla kt�rej ma by� utworzona paczka.");

        ObjectFactory fac = new ObjectFactory();
        Dokument dok = fac.createDokument();

        //Dodanie daty
        Data data = fac.createData();
        data.setTyp(TypDaty.STWORZONY);
        data.setCzas(DateUtils.formatAxisDateTime(doc.getCtime()));
        dok.getData().add(data);

        //Dost�p
        Dostep dostep = fac.createDostep();
        dostep.setDostepnosc(TypDostepnosci.WSZYSTKO.value());
        dok.setDostep(dostep);

        //Format
        if(doc.getAttachments().size()==1){
            Format format = fac.createFormat();
            format.setTyp(doc.getAttachments().get(0).getMostRecentRevision().getMime());
            Wielkosc wielkosc = fac.createWielkosc();
            wielkosc.setJednostka("bajt");
            wielkosc.setValue(doc.getAttachments().get(0).getMostRecentRevision().getSize().toString());
            format.setWielkosc(wielkosc);
            dok.setFormat(format);
        }

        //Grupowanie
        Grupowanie grupowanie = fac.createGrupowanie();
        grupowanie.setTyp("znak sprawy");
        grupowanie.setKod(sprawa.getOfficeId());
        grupowanie.setOpis(sprawa.getParent().getName());
        dok.getGrupowanie().add(grupowanie);

        //Identyfikator
        Identyfikator docId = fac.createIdentyfikator();
        docId.setTyp("docId");
        docId.setWartosc(doc.getId().toString());
        dok.getIdentyfikator().add(docId);
        for(Attachment att : doc.getAttachments()){
            Identyfikator identyfikator = fac.createIdentyfikator();
            identyfikator.setTyp("fileNameId");
            identyfikator.setWartosc(att.getTitle());
            dok.getIdentyfikator().add(identyfikator);
        }

        //Jezyk
        Jezyk jezyk = fac.createJezyk();
        jezyk.setKod(Jezyk.JEZYK_KOD_POL);
        dok.getJezyk().add(jezyk);

        //Tworca
        Tworca tworca = fac.createTworca();
        tworca.setFunkcja(Tworca.STWORZYL);
        Podmiot podmiotTworcy = fac.createPodmiot();
        if(doc.getSender() != null){
            Sender sender = doc.getSender();
            if (sender.getOrganization() == null && "".equals(sender.getOrganization())){
                Osoba osobaTworcy = fac.createOsoba();
                osobaTworcy.setImie(sender.getFirstname());
                osobaTworcy.setNazwisko(sender.getLastname());
                Adres adresNadawcy = fac.createAdres();
                adresNadawcy.setMiejscowosc(sender.getLocation());
                adresNadawcy.setUlica(sender.getStreet());
                adresNadawcy.setKraj(sender.getCountry());
                adresNadawcy.setKod(sender.getZip());
                osobaTworcy.setAdres(adresNadawcy);
                podmiotTworcy.setOsoba(osobaTworcy);
            }else{
                Instytucja instytucja = fac.createInstytucja();
                instytucja.setNazwa(sender.getOrganization());
                Jednostka jednostka = fac.createJednostka();
                jednostka.setNazwa(sender.getOrganizationDivision());
                Adres adresJednostki = fac.createAdres();
                adresJednostki.setMiejscowosc(sender.getLocation());
                adresJednostki.setKraj(sender.getCountry());
                adresJednostki.setKod(sender.getZip());
                adresJednostki.setUlica(sender.getStreet());
                jednostka.setAdres(adresJednostki);
                if (sender.getPhoneNumber() != null){
                    Kontakt kontakt = fac.createKontakt();
                    kontakt.setTyp("tel.");
                    kontakt.setValue(sender.getPhoneNumber());
                    jednostka.setKontakt(kontakt);
                }
                Pracownik pracownik = fac.createPracownik();
                pracownik.setImie(sender.getFirstname());
                pracownik.setNazwisko(sender.getLastname());
                jednostka.setPracownik(pracownik);

                instytucja.setJednostka(jednostka);
                podmiotTworcy.setInstytucja(instytucja);
            }
        }else if(doc instanceof OutOfficeDocument){
            Instytucja instytucja = fac.createInstytucja();
//            Uniwersytet Technologiczno-Przyrodniczy
//            im. Jana i J�drzeja �niadeckich
//            ul. Ks. Kordeckiego 20, 85-225 Bydgoszcz
//            tel. 52 373-14-50, fax 052 374-93-27
            instytucja.setNazwa(Docusafe.getAdditionProperty("archive.package.institution.name"));
            Adres adres = fac.createAdres();
            adres.setKraj(Docusafe.getAdditionProperty("archive.package.institution.address.country"));
            adres.setMiejscowosc(Docusafe.getAdditionProperty("archive.package.institution.location"));
            adres.setBudynek(Docusafe.getAdditionProperty("archive.package.institution.address.building"));
            adres.setKod(Docusafe.getAdditionProperty("archive.package.institution.address.code"));
            adres.setUlica(Docusafe.getAdditionProperty("archive.package.institution.address.street"));
            instytucja.setAdres(adres);
            Kontakt kontakt = fac.createKontakt();
            kontakt.setTyp("tel.");
            kontakt.setValue(Docusafe.getAdditionProperty("archive.package.institution.phone"));
            instytucja.setKontakt(kontakt);
            podmiotTworcy.setInstytucja(instytucja);
        }
        tworca.setPodmiot(podmiotTworcy);
        dok.getTworca().add(tworca);

        //odbiorca
        Podmiot podmiotOdbiorcy = fac.createPodmiot();
        if(doc.getRecipients() != null && doc.getRecipients().size()>0){
            for(Recipient recipient : doc.getRecipients()){
                Odbiorca odbiorca = fac.createOdbiorca();
                odbiorca.setRodzaj(Odbiorca.RODZAJ_ODBIORCY_GLOWNY);
                if (recipient.getOrganization() == null && "".equals(recipient.getOrganization())){
                    Osoba osobaOdbiorcy = fac.createOsoba();
                    osobaOdbiorcy.setImie(recipient.getFirstname());
                    osobaOdbiorcy.setNazwisko(recipient.getLastname());
                    Adres adresOdbiorcy = fac.createAdres();
                    adresOdbiorcy.setMiejscowosc(recipient.getLocation());
                    adresOdbiorcy.setUlica(recipient.getStreet());
                    adresOdbiorcy.setKraj(recipient.getCountry());
                    adresOdbiorcy.setKod(recipient.getZip());
                    osobaOdbiorcy.setAdres(adresOdbiorcy);
                    podmiotOdbiorcy.setOsoba(osobaOdbiorcy);
                }else{
                    Instytucja instytucja = fac.createInstytucja();
                    instytucja.setNazwa(recipient.getOrganization());
                    Jednostka jednostka = fac.createJednostka();
                    jednostka.setNazwa(recipient.getOrganizationDivision());
                    Adres adresJednostki = fac.createAdres();
                    adresJednostki.setMiejscowosc(recipient.getLocation());
                    adresJednostki.setKraj(recipient.getCountry());
                    adresJednostki.setKod(recipient.getZip());
                    adresJednostki.setUlica(recipient.getStreet());
                    jednostka.setAdres(adresJednostki);
                    if (recipient.getPhoneNumber() != null){
                        Kontakt kontakt = fac.createKontakt();
                        kontakt.setTyp("tel.");
                        kontakt.setValue(recipient.getPhoneNumber());
                        jednostka.setKontakt(kontakt);
                    }
                    Pracownik pracownik = fac.createPracownik();
                    pracownik.setImie(recipient.getFirstname());
                    pracownik.setNazwisko(recipient.getLastname());
                    jednostka.setPracownik(pracownik);

                    instytucja.setJednostka(jednostka);
                    podmiotOdbiorcy.setInstytucja(instytucja);
                }
                odbiorca.setPodmiot(podmiotOdbiorcy);
                dok.getOdbiorca().add(odbiorca);
            }
        } else if(doc instanceof InOfficeDocument){
            Instytucja instytucja = fac.createInstytucja();
            instytucja.setNazwa(Docusafe.getAdditionProperty("archive.package.institution.name"));
            Adres adres = fac.createAdres();
            adres.setKraj(Docusafe.getAdditionProperty("archive.package.institution.address.country"));
            adres.setMiejscowosc(Docusafe.getAdditionProperty("archive.package.institution.location"));
            adres.setBudynek(Docusafe.getAdditionProperty("archive.package.institution.address.building"));
            adres.setKod(Docusafe.getAdditionProperty("archive.package.institution.address.code"));
            adres.setUlica(Docusafe.getAdditionProperty("archive.package.institution.address.street"));
            instytucja.setAdres(adres);
            Kontakt kontakt = fac.createKontakt();
            kontakt.setTyp("tel.");
            kontakt.setValue(Docusafe.getAdditionProperty("archive.package.institution.phone"));
            instytucja.setKontakt(kontakt);
            podmiotOdbiorcy.setInstytucja(instytucja);
            Odbiorca odbiorca = fac.createOdbiorca();
            odbiorca.setRodzaj(Odbiorca.RODZAJ_ODBIORCY_GLOWNY);
            odbiorca.setPodmiot(podmiotOdbiorcy);
            dok.getOdbiorca().add(odbiorca);
        }

        //typ
        Typ typ = fac.createTyp();
        typ.setKategoria(Typ.KATEGORIA_TEXT);
        typ.setRodzaj("Plik");
        dok.setTyp(typ);

        //tytul
        Tytul tytul = fac.createTytul();
        TytulElement tytElement = fac.createTytulElement();
        tytElement.setJezyk(Jezyk.JEZYK_KOD_POL);
        tytElement.setValue(doc.getTitle());
        tytul.setOryginalny(tytElement);
        dok.setTytul(tytul);

        //opis
        dok.setOpis(doc.getSummary());
        return dok;
    }

    private Dokument createCaseMetaFile(OfficeCase sprawa) throws EdmException {
        ObjectFactory fac = new ObjectFactory();
        Dokument dok = fac.createDokument();

        //Dodanie daty
        Data data = fac.createData();
        if(sprawa.getFinishDate()!=null){
            data.setTyp(TypDaty.DATY_SKRAJNE);
            data.setOd(DateUtils.formatAxisDateTime(sprawa.getOpenDate()));
            data.setDo(DateUtils.formatAxisDateTime(sprawa.getFinishDate()));
        }else{
            data.setTyp(TypDaty.STWORZONY);
            data.setCzas(DateUtils.formatAxisDateTime(sprawa.getOpenDate()));
        }
        dok.getData().add(data);

        //Dost�p
        Dostep dostep = fac.createDostep();
        dostep.setDostepnosc(TypDostepnosci.METADANE.value());
        dok.setDostep(dostep);

        //Format
        Format format = fac.createFormat();
        format.setTyp("text/xml");
        dok.setFormat(format);

        //identyfikator
        Identyfikator identyfikator = fac.createIdentyfikator();
        identyfikator.setTyp("znak sprawy");
        identyfikator.setWartosc(sprawa.getOfficeId());
        dok.getIdentyfikator().add(identyfikator);

        //jezyk
        Jezyk jezyk = fac.createJezyk();
        jezyk.setKod(Jezyk.JEZYK_KOD_POL);
        dok.getJezyk().add(jezyk);

//        //kwalifikacja
//        Kwalifikacja kwalifikacja = fac.createKwalifikacja();
//        kwalifikacja.se

        //Tworca
        Tworca tworca = fac.createTworca();
        tworca.setFunkcja(Tworca.STWORZYL);
        Podmiot podmiotTworcy = fac.createPodmiot();
        Osoba osobaTworcy = fac.createOsoba();
        if(!sprawa.getAuthor().equals("admin")){
            DSUser caseCreator = DSUser.findByUsername(sprawa.getAuthor());
            osobaTworcy.setImie(caseCreator.getFirstname());
            osobaTworcy.setNazwisko(caseCreator.getLastname());
            podmiotTworcy.setOsoba(osobaTworcy);
        }else{
            Instytucja instytucja = fac.createInstytucja();
            instytucja.setNazwa(GlobalPreferences.getCustomer());
            podmiotTworcy.setInstytucja(instytucja);
        }
        tworca.setPodmiot(podmiotTworcy);
        dok.getTworca().add(tworca);

        //typ
        Typ typ = fac.createTyp();
        typ.setKategoria(Typ.KATEGORIA_COLLECTION);
        typ.setRodzaj("Sprawa");
        dok.setTyp(typ);

        //tytu�
        Tytul tytul = fac.createTytul();
        TytulElement tytulElement = fac.createTytulElement();
        tytulElement.setJezyk("pol");
        tytulElement.setValue(sprawa.getTitle());
        tytul.setOryginalny(tytulElement);
        dok.setTytul(tytul);

        return dok;
    }

    public static void createFile(Dokument dok, File file) throws JAXBException {
        JAXBElement<Dokument> jaxbElement = new JAXBElement<Dokument>(ObjectFactory._DokumentType_QNAME, Dokument.class,null, dok);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(jaxbElement, file);
    }

    private static String zipDirectory(File directoryFile){
        List<File> fileList = new ArrayList<File>();
        getAllFiles(directoryFile, fileList);
        String zippedFilePath = writeZipFile(directoryFile, fileList);
        return zippedFilePath;
    }

    public static void getAllFiles(File dir, List<File> fileList) {
        try {
            File[] files = dir.listFiles();
            for (File file : files) {
                fileList.add(file);
                if (file.isDirectory()) {
                    System.out.println("directory:" + file.getCanonicalPath());
                    getAllFiles(file, fileList);
                } else {
                    System.out.println("     file:" + file.getCanonicalPath());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String writeZipFile(File directoryToZip, List<File> fileList) {
        String path;
        try {
            path = directoryToZip.getPath()+".zip";
            FileOutputStream fos = new FileOutputStream(path);
            ZipOutputStream zos = new ZipOutputStream(fos);

            for (File file : fileList) {
                if (!file.isDirectory()) { // we only zip files, not directories
                    addToZip(directoryToZip, file, zos);
                }
            }

            zos.close();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return path;
    }

    public static void addToZip(File directoryToZip, File file, ZipOutputStream zos) throws FileNotFoundException,
            IOException {

        FileInputStream fis = new FileInputStream(file);

        // we want the zipEntry's path to be a relative path that is relative
        // to the directory being zipped, so chop off the rest of the path
        String zipFilePath = file.getCanonicalPath().substring(directoryToZip.getCanonicalPath().length() + 1,
                file.getCanonicalPath().length());
        System.out.println("Writing '" + zipFilePath + "' to zip file");
        ZipEntry zipEntry = new ZipEntry(zipFilePath);
        zos.putNextEntry(zipEntry);

        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zos.write(bytes, 0, length);
        }

        zos.closeEntry();
        fis.close();
    }
}
