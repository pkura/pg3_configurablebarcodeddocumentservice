package pl.compan.docusafe.parametrization.archiwum;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.BooleanUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.Field.Type;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.Container.ArchiveStatus;
import pl.compan.docusafe.parametrization.archiwum.db.provider.ArchiveQueryExecutor;
import pl.compan.docusafe.parametrization.archiwum.db.provider.ArchiveQueryProvider;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

public class ListaTeczekSpisAP extends DwrDictionaryBase{
	protected static Logger log = LoggerFactory.getLogger(ListaTeczekSpisAP.class);
	private ArchiveQueryExecutor archiveQueryExecutor = ArchiveQueryProvider.getInstance();
/*	@Override
	public boolean filterBeforeGetValues(
			Map<String, Object> dictionaryFilteredFieldsValues,
			String dictionaryEntryId, Map<String, Object> documentValues) {
		
		return super.filterBeforeGetValues(dictionaryFilteredFieldsValues,
				dictionaryEntryId, documentValues);
	}
	@Override
	public void filterAfterGetValues(
			Map<String, Object> dictionaryFilteredFieldsValues,
			String dicitonaryEntryId, Map<String, Object> fieldsValues,
			Map<String, Object> documentValues) {
		niezdziela
 * try {	


			DataBaseEnumField slown= (DataBaseEnumField) dictionaryFilteredFieldsValues.get("SPIS_AP_TECZKI_IDTECZKI");
			List<EnumItem> val;
			Set<EnumItem> val2=new HashSet<EnumItem>();
			val=slown.getEnumItems();
			
			HashMap<String, String> tylkoStat2=new HashMap<String,String>();
			
			for(int i=0;i<val.size();i++){
				String id=val.get(i).getId().toString();
					if(id!=null&&!id.equals("")){
						OfficeFolder folder;
						folder = OfficeFolder.find(Long.parseLong(id));

						if (folder.getArchiveStatus() == ArchiveStatus.Archived||folder.getArchiveStatus() == ArchiveStatus.TransferedNA||folder.getArchiveStatus() == ArchiveStatus.ToTransferToNA) 
						//	tylkoStat2.put(id, val.get(i).get(id));
							val2.add(val.get(i));
					}
					else{
						val2.add(val.get(i));
						//tylkoStat2.put(id, val.get(i).get(id));
					}
					
				

			}
			List<String> tmp=new ArrayList<String>();
			tmp.add("1");
			slown=new EnumValues(tylkoStat2,tmp);
			//slown.setAllItems();
			slown.setAllItems(val2);
			dictionaryFilteredFieldsValues.put("SPIS_AP_TECZKI_IDTECZKI",slown);
		} catch (NumberFormatException e) {
			log.error("[SpisDoAP]", e);
		} catch (EdmException e) {
			log.error("[SpisDoAP]", e);
		}
		super.filterAfterGetValues(dictionaryFilteredFieldsValues, dicitonaryEntryId,
				fieldsValues, documentValues);
	}*/
/*	@Override
	public void filterBeforeUpdateValues(
			Map<String, FieldData> dictionaryFilteredFieldData) {
		// TODO Auto-generated method stub
		super.filterBeforeUpdateValues(dictionaryFilteredFieldData);
	}*/
	@Override
	public long add(Map<String, FieldData> values) throws EdmException {

		ustawZPowrotemStatusyDlaUsunietych();

		boolean ctx=DSApi.openContextIfNeeded();
		if(values.containsKey("IDTECZKI")){

			Long id;
			try{
				id = values.get("IDTECZKI").getLongData(); 
			}catch(Exception e){
				//nie moze pobrac IDTECZKI, badz jest puste
				return super.add(values);
			}
			if(id!=null){
				FromClause from = new FromClause();
				TableAlias divisionTable = from.createTable("ds_ar_teczki_do_ap_view");
				WhereClause where = new WhereClause();
				where.add(Expression.eq(divisionTable.attribute("id"), id));
				SelectClause select = new SelectClause(true);
				SelectColumn folderId = select.add(divisionTable, "FOLDERID");
				SelectColumn cn = select.add(divisionTable, "CN");
				ResultSet rs = null;
				SelectQuery query = new SelectQuery(select, from, where, null);

				try {
					rs = query.resultSet();
				} catch (SQLException e) {
					log.debug("BLAD SQL",e);
				}
				try {
					if(rs.next()){
						if(rs.getString(folderId.getPosition())!=null){
							OfficeFolder folder = OfficeFolder.find(rs.getLong(folderId.getPosition()));
							// ArchiveStatus.None
							// Taki przypadek nie powinien wystapic, oznaczaloby ze na spisie zdawczo odbiorczym
							// nie jest ustawiany status ArchiveStatus.Archived
							if (folder.getArchiveStatus() == ArchiveStatus.None){
								DSApi.closeContextIfNeeded(ctx);
								return -1;	
							}
							if (folder.getArchiveStatus() == ArchiveStatus.Archived){
								folder.setArchiveStatus(ArchiveStatus.ToTransferToNA);
								for(Container c:folder.getChildren()){
									if(c instanceof OfficeCase){
										c.setArchiveStatus(ArchiveStatus.ToTransferToNA);
									}
								}
							}
							else{
								DSApi.closeContextIfNeeded(ctx);
								throw new EdmException("Teczka " + folder.getOfficeId()
										+ " jest zablokowana przez inny proces lub wybrano 2 te same teczki.");
							}
							values.put("IDTECZKI2", new FieldData(Type.STRING, folder.getOfficeId()));
						}
						else{
							archiveQueryExecutor.updateInventoryById(id);
							
							values.put("IDTECZKI2", new FieldData(Type.STRING, rs.getString(cn.getPosition())));
						}
					}
					
				} catch (SQLException e) {
					log.debug("BLAD SQL2",e);
				}

			}
		}

		DSApi.closeContextIfNeeded(ctx);
		return super.add(values);

	}
	@Override
	public int update(Map<String, FieldData> values) throws EdmException {
		int superReturn = super.update(values);
		
		return superReturn;
	}
	
	@Override
	public List<Map<String, Object>> search(Map<String, FieldData> values,
			int limit, int offset, boolean fromDwrSearch,
			Map<String, FieldData> dockindFields) throws EdmException {
		// TODO Auto-generated method stub
		return super.search(values, limit, offset, fromDwrSearch, dockindFields);
	}
	@Override
	public int remove(String id) throws EdmException {
		// TODO Auto-generated method stub
		return super.remove(id);
	}
	@Override
	public List<Map<String, Object>> search(Map<String, FieldData> values,
			int limit, int offset) throws EdmException {
		// TODO Auto-generated method stub
		return super.search(values, limit, offset);
	}
	
	
	
	public void ustawZPowrotemStatusyDlaUsunietych(){

		try {

			archiveQueryExecutor.updateCaseBelongsToDeletedPortfolios();
			
			archiveQueryExecutor.updateCaseBelongsToPortfoliosWhichBe();

			//powrot statusow dla fizycznych
//			ps=DSApi.context().prepareStatement(" update "+NewCaseArchiverScheduled.wykazItemsTableName+" set archive_status="+ArchiveStatus.Archived.ordinal()+" where id not in("
//					+ " select id from  ds_ar_teczki_do_ap where  id in ("
//					+ "select field_val from "+NewCaseArchiverScheduled.wykazMultipleTableName+")) ");

	} catch (SQLException e) {
		log.error(e.getMessage(), e);
		e.printStackTrace();
	} catch (EdmException e1) {
		log.error(e1.getMessage(), e1);
		e1.printStackTrace();

	}
	}


}
