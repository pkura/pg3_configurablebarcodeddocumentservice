package pl.compan.docusafe.parametrization.archiwum;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.Format;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field.Type;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

public class SpiszdawczoodbiorczyLogic  extends AbstractDocumentLogic{

	private final static Logger log = LoggerFactory.getLogger(SpiszdawczoodbiorczyLogic.class);
	
	StringManager sm = GlobalPreferences.loadPropertiesFile(SpiszdawczoodbiorczyLogic.class.getPackage().getName(), null);
	private static SpiszdawczoodbiorczyLogic instance;
	
	private static final String Stanowisko_SQL = 
			"select GUID from DS_DIVISION where PARENT_ID = ? and name = ?";
	private static final String SLOWNIK_DS_AR_INVENTORY_ITEM="DS_AR_INVENTORY_ITEM";
	private static final String SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_OD="DATE_FROM";
	private static final String SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_DO="DATE_TO";
	public static String CN_DATA_DOKUMENTU ="DOC_DATE";
	public static String CN_AUTOR_DOKUMENTU ="DOC_AUTOR";
	public static String CN_DZIAL_AUTORA_DOKUMENTU ="DOC_AUTORDIVISION";
	public static String CN_TYP_DOKUMENTU = "TYP_DOKUMENTU";
	public static String CN_ABSTRAKT_DOKUMENTU = "ABSTRAKT";
	public static String CN_MIESCEUTWOORZENIA_DOKUMENTU = "MIESCEUTWOORZENIA";
	public static String CN_DOC_DESCRIPTION = "DOC_DESCRIPTION";
	/*
	private static final String _Kierownik_ID = 
			"select USER_ID from DS_USER_TO_DIVISION where DIVISION_ID = ?";
	*/
	
	public static SpiszdawczoodbiorczyLogic getInstance()
	{
		if (instance == null)
			instance= new SpiszdawczoodbiorczyLogic();
		return instance;
	}
	@Override
	public void documentPermissions(Document document) throws EdmException {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * funkcja ta wykonuje sie przy tworzeniu dokumentu z automatu (newCaseArchiverScheduled)
	 */
	@Override
 	public void onStartProcess(OfficeDocument document) {
 		log.info("--- SpiszdawczoodbiorczyLogic : START PROCESS !!! ----");
 		try {
 			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, document.getAuthor());
			map.put(ASSIGN_DIVISION_GUID_PARAM, document.getDivisionGuid());
 			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
 			if (AvailabilityManager.isAvailable("addToWatch"))
 				DSApi.context().watch(URN.create(document));
 		} catch (Exception e) {
 			log.error(e.getMessage(), e);
 		}
 	}
	
	/*@Override
 	public void onStartProcess(OfficeDocument document, ActionEvent event) {
 		log.info("--- SpiszdawczoodbiorczyLogic : START PROCESS !!! ---- {}", event);
 		try {
 			Map<String, Object> map = Maps.newHashMap();
 			if (document instanceof InOfficeDocument) {
 				map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
 			}
 			else {
 				map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
 				map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
 			}
 			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
 			if (AvailabilityManager.isAvailable("addToWatch"))
 				DSApi.context().watch(URN.create(document));
 		} catch (Exception e) {
 			log.error(e.getMessage(), e);
 		}
 	}*/

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * uzupelnia spisZdawczoOdbiorczy.rtf zdefiniowany w spiszdawczoodbiorczy.activities.xml
	 */
	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values, Map<Class, Format> defaultFormatters)
	{
		PreparedStatement ps1= null;
		PreparedStatement ps2= null;
		try {
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			fm.initialize();
			List<Map<String, String>> pozycje = Lists.newArrayList();
			Double metry_bierzace = 0D;
			
			
			// $DIVISION i $NUMBER_ITR
			String query1= "select ds_division_id, number_itr from "+NewCaseArchiverScheduled.wykazTableName+" wykaz "+
				" where wykaz.DOCUMENT_ID = "+ docId;
	
			ps1 = DSApi.context().prepareStatement(query1);
			ResultSet queryResult1 = ps1.executeQuery();
			queryResult1.next();
			Long divisionId= queryResult1.getLong("ds_division_id");
			Integer number= queryResult1.getInt("number_itr");
			
			String division=DSDivision.findById(divisionId.intValue()).getName();
			
			values.put("DIVISION", division!=null ?
					division : ".........");
			values.put("NUMBER_ITR", (number!=null && number!=0)?
					number : ".........");
	
			
			// zawartosc tabelki
			
			String query2= "select * from "+NewCaseArchiverScheduled.wykazMultipleTableName+" multi left join "+
					NewCaseArchiverScheduled.wykazItemsTableName+" item on multi.FIELD_VAL = item.id  ";
			//postgres jawne rzutowanie typow na stringa bo nie mozna wrzucac integera ja kjest klolumna string
					if(DSApi.isPostgresServer()){
						query2	+="::text ";
					}
					
					query2+=	" where multi.DOCUMENT_ID = "+ docId;
			
		
			ps2 = DSApi.context().prepareStatement(query2);
			ResultSet queryResult2 = ps2.executeQuery();
			int i = 0;
			
			while (queryResult2.next()) {
				i++;
				int column= 0;
				HashMap<String, String> pozycja = new HashMap<String, String>(9);
				DateFormat dateFormat = DateFormat.getDateInstance();

				pozycja.put("lp", Integer.toString(i));
				
				// znak teczki
				String znak = queryResult2.getString("FOLDER_OFFICEID");
				pozycja.put("znak", StringUtils.isNotEmpty(znak)?
							znak : "-----");
				
				// tytul teczki
				String tytul = queryResult2.getString("FOLDER_TITLE");
				pozycja.put("rwatytul", StringUtils.isNotEmpty(tytul)?
						tytul : "-----");
				
				// uwagi
				String uwagi = queryResult2.getString("COMMENTS");
				pozycja.put("uwagi", StringUtils.isNotEmpty(uwagi)?
						uwagi : "-----");
				
				// daty od, do
				String dataOd = queryResult2.getString("DATE_FROM");
				//lokalnie na windowsiew paruje dobrze a na linuxie wywala error unparsable moze wina 2 jav zainstalowanych 6 i 8  ?
			/*	pozycja.put("dataOd", StringUtils.isNotEmpty(dataOd)?
						DateUtils.formatJsDate(dateFormat.parse(dataOd)) : "-----");*/
				pozycja.put("dataOd", StringUtils.isNotEmpty(dataOd)?
						dataOd : "-----");
				String dataDo = queryResult2.getString("DATE_TO");
				//lokalnie na windowsiew paruje dobrze a na linuxie wywala error unparsable moze wina 2 jav zainstalowanych 6 i 8  ?
				/*pozycja.put("dataDo", StringUtils.isNotEmpty(dataDo)?
						DateUtils.formatJsDate(dateFormat.parse(dataDo)) : "-----");*/
				pozycja.put("dataDo", StringUtils.isNotEmpty(dataDo)?
						dataDo : "-----");
				// kategoria
				String kategoria = queryResult2.getString("CATEGORY_ARCH");
				pozycja.put("kat", StringUtils.isNotEmpty(kategoria)?
						kategoria : "-----");
				
				// liczba teczek
				String liczba = queryResult2.getString("FOLDER_COUNT");
				pozycja.put("liczbaTeczek", StringUtils.isNotEmpty(liczba)?
						liczba : "-----");
						
				// miejsce przechowywania
				String miejsce = queryResult2.getString("ARCH_PLACE");
				pozycja.put("miejsce", StringUtils.isNotEmpty(miejsce)?
						miejsce : "-----");
				
				// data zniszczenia
				String dataZniszcz = queryResult2.getString("DATE_SHRED");
				//lokalnie na windowsiew paruje dobrze a na linuxie wywala error unparsable moze wina 2 jav zainstalowanych 6 i 8  ?
				/*pozycja.put("dataZnisz", StringUtils.isNotEmpty(dataZniszcz)?
						DateUtils.formatJsDate(dateFormat.parse(dataZniszcz)) : "-----");*/
				pozycja.put("dataZnisz", StringUtils.isNotEmpty(dataZniszcz)?
						dataZniszcz : "-----");
				pozycje.add(pozycja);
				
				metry_bierzace += queryResult2.getDouble("FOLDER_LENGTH_METER");
			}
			//values.put("metry", metry_bierzace);
			values.put("TECZKI", pozycje);

			/*values.put("member0", fm.getValue(ProtokolBrakowaniaStale.getCnUczestnik(0)));
			values.put("member1", fm.getValue(ProtokolBrakowaniaStale.getCnUczestnik(1)));
			values.put("member2", fm.getValue(ProtokolBrakowaniaStale.getCnUczestnik(2)));
			values.put("member3", fm.getValue(ProtokolBrakowaniaStale.getCnUczestnik(3)));*/
				
		} catch (Exception e) {
			log.error("Blad przy generowaniu rtf spisu zd-od nr "+docId+" "+e.getMessage(), e);
		}finally{
			if(ps1!=null)
				DSApi.context().closeStatement(ps1);
			if(ps2!=null)
				DSApi.context().closeStatement(ps2);
		}
	}
	
	/**
	 * @param user - moze byc nullem
	 */
	public static String stanowiskoUser(int departament, String stanowisko, Integer user) throws SQLException, EdmException{
		//DSApi.openAdmin();
		Long idStanowiska = 0L;
		List<String> kieronicy = new ArrayList<String>();
		Long idKierownika = 0L;
		PreparedStatement ps = DSApi.context().prepareStatement(Stanowisko_SQL);
		ps.setInt(1,departament);
		ps.setString(2, stanowisko);
		
		ResultSet rs = ps.executeQuery();
		String guid ;
		if (rs.next()){
		 guid = rs.getString(1);
		} else {
			return null;
		}
		ps.close();
		rs.close();
		
		//DSApi._close();
		return guid;
	}
	
	public void setInitialValues(FieldsManager fm, int type)
			throws EdmException {
		//log.info("type: {}", type);
		Map<String, Object> values = new HashMap<String, Object>();
		
		values.put(CN_DATA_DOKUMENTU, new Date());
		values.put(CN_TYP_DOKUMENTU, 2);
		values.put(CN_DOC_DESCRIPTION, "Spis zdawczo-odbiorczy");
		
		fm.reloadValues(values);
	}
	
	/*@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
    {
		values.isEmpty();
		if ((values.get("DS_AR_INVENTORY_ITEM_CATEGORY_ARCH") == null 
				|| StringUtils.isEmpty(values.get("DS_AR_INVENTORY_ITEM_CATEGORY_ARCH").toString()))
			&& dockindFields!=null && dockindFields.get("IS_CATEGORY_A") != null) 
		{
			// dodajemy filtr wg pola "kategoria" z dockindu, przy czym to pole jest... Integerem?
			Object category = dockindFields.get("IS_CATEGORY_A");
			Boolean isA= null;
			if( category instanceof FieldData && ((FieldData)category).getBooleanData()!=null)
				isA= ((FieldData)category).getBooleanData().booleanValue();
			if( category instanceof Integer )
				isA= ((Integer)category).equals(1);
			
			if( isA!=null )
				values.put("DS_AR_INVENTORY_ITEM_CATEGORY_ARCH", 
						isA ?
						new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "A")
						: new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "B") );
		}			
    }*/

	/**
	 * pobranie warto�ci z ROOM, RACK, SHELF, WAREHOUSE, STREET,BUILDING
	 *  a nastepnie sklejenie i dopisanie do pola ARCH_PLACE
	 */
	@Override
	public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException{
		HashMap<String, Object> slowniki = (HashMap<String, Object>) values.get("M_DICT_VALUES");
		if(documentId != null){
			Document document = Document.find(documentId);
			FieldsManager fm = document.getFieldsManager();

			for(Map.Entry<String, Object> entry:slowniki.entrySet()){
				if (entry.getKey().contains(SLOWNIK_DS_AR_INVENTORY_ITEM)){
				HashMap<String,Object> dict = (HashMap<String, Object>) slowniki.get(entry.getKey());
				String pomieszczenie = String.valueOf(dict.get("ROOM"));
				String regal = String.valueOf(dict.get("RACK"));
				String polka = String.valueOf(dict.get("SHELF"));
				String ulica = String.valueOf(dict.get("STREET"));
				String magazyn = String.valueOf(dict.get("WAREHOUSE"));
				String budynek = String.valueOf(dict.get("BUILDING"));
				String cat_arch = String.valueOf(dict.get("CATEGORY_ARCH"));
				String arch_place =  "Ulica: "+ ulica + " / " + "budynek: "+budynek+" / "+"magazyn: "+ magazyn+" / "+"pomieszczenie: " + pomieszczenie + " / " +"rega�/szafa: " + regal +" / " + "p�ka: " + polka ;
				if(!regal.isEmpty() && !polka.isEmpty() && !pomieszczenie.isEmpty()){
					dict.put("ARCH_PLACE",new FieldData(Type.STRING, arch_place));
				}
				if(!fm.getEnumItem("STATUSDOKUMENTU").getId().equals(6)){
					if(values.get("IS_CATEGORY_A").equals(1)){
						if(!cat_arch.toLowerCase().startsWith("a")){
							throw new EdmException(sm.getString("kategoriaArchiwalna"));
						}
					} else {
						if(!cat_arch.toLowerCase().startsWith("b")){
							throw new EdmException(sm.getString("kategoriaArchiwalna"));
						}
					}
				}
				
				//validacja daty
				if(dict.containsKey(SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_DO)){
					if(dict.containsKey(SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_OD)){
						if(dict.get(SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_OD)!=null&&dict.get(SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_DO)!=null){
							Date dataOd=((FieldData)dict.get(SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_OD)).getDateData();
							Date dataDo=((FieldData)dict.get(SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_DO)).getDateData();

							if(dataOd!=null&&dataDo!=null){
								if(dataOd.after(dataDo)){
									throw new EdmException(sm.getString("zlaData"));
								}
							}
						}
					}
				}		


					
				}
			}
		}
		
		if (values.get("M_DICT_VALUES") != null)
		{
			Map mapa = (Map) values.get("M_DICT_VALUES");

			for (Object obj : mapa.values())
			{
				if (((Map) obj).containsKey(SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_OD) || ((Map) obj).containsKey(SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_DO))
				{
					Date dataOd = ((FieldData) ((Map) obj).get(SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_OD)).getDateData();
					Date dataDo = ((FieldData) ((Map) obj).get(SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_DO)).getDateData();

					if (dataOd != null && dataDo != null)
					{
						if (dataOd.after(dataDo))
						{
							throw new EdmException(sm.getString("zlaData"));
						}
					}
				}
			}
		}
	}

	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
			throws EdmException {

		Field msg = null;
		
		boolean room = false;
		boolean rack = false;
		boolean shelf = false;
		if(fm.getValue("STATUSDOKUMENTU") != null){
			if(fm.getEnumItem("STATUSDOKUMENTU").getId().equals(6)){
				Map<String, FieldData> dict = values.get("DWR_DS_AR_INVENTORY_ITEM").getDictionaryData();
				values.put("DWR_ARCHIVE_PLACE", new FieldData(Type.STRING, "Tak"));

				for(Map.Entry<String, FieldData> entry:dict.entrySet()){

					if(entry.getKey().startsWith("DS_AR_INVENTORY_ITEM_ROOM")){
						if(entry.getValue().getStringData().isEmpty()){
							room = true;
							break;
						} else {
							room = false;
						}
					} else if(entry.getKey().startsWith("DS_AR_INVENTORY_ITEM_RACK")){
						if(entry.getValue().getStringData().isEmpty()){
							rack = true;
							break;
						} else {
							rack = false;
						}
					} else if(entry.getKey().startsWith("DS_AR_INVENTORY_ITEM_SHELF")){
						if(entry.getValue().getStringData().isEmpty()){
							shelf = true;
							break;
						} else {
							shelf = false;
						}
					}
				}
				if(room || rack || shelf){
					values.put("DWR_ARCHIVE_PLACE", new FieldData(Type.STRING, "Nie"));
				}
			


			}
		}

		if(values.get("DWR_"+SLOWNIK_DS_AR_INVENTORY_ITEM) != null){
			Map<String,FieldData> mapa = values.get("DWR_"+SLOWNIK_DS_AR_INVENTORY_ITEM).getDictionaryData();
			
			int  i = 1;
			
			while(true){
				if(!mapa.containsKey(SLOWNIK_DS_AR_INVENTORY_ITEM+"_ID_"+i))
					break;
				
				if(mapa.get(SLOWNIK_DS_AR_INVENTORY_ITEM+"_"+SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_DO+"_"+i) != null && mapa.get(SLOWNIK_DS_AR_INVENTORY_ITEM+"_"+SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_OD+"_"+i) != null){
					if(mapa.get(SLOWNIK_DS_AR_INVENTORY_ITEM+"_"+SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_OD+"_"+i).getDateData()!=null && mapa.get(SLOWNIK_DS_AR_INVENTORY_ITEM+"_"+SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_DO+"_"+i).getDateData()!=null){
						if(mapa.get(SLOWNIK_DS_AR_INVENTORY_ITEM+"_"+SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_OD+"_"+i).getDateData().after(mapa.get(SLOWNIK_DS_AR_INVENTORY_ITEM+"_"+SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_DO+"_"+i).getDateData())){
							msg = new Field("messageField", sm.getString("zlaData"), Field.Type.BOOLEAN);
							values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
						}
					}
				}
				
				i++;
			}
		}
		
	/*	if(values.containsKey(SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_OD)){
			if(values.containsKey(SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_DO)){
				if(values.get(SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_DO)!=null&&values.get(SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_OD)!=null){
					if(values.get(SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_DO).getDateData().before(values.get(SLOWNIK_DS_AR_INVENTORY_ITEM_DATA_OD).getDateData())){
						throw new EdmException(sm.getString("kategoriaArchiwalna"));
					}
				}
				
			}*/
		
		
		return msg;
	}
	
	
	

	
	
}
	
