package pl.compan.docusafe.parametrization.archiwum;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.FSDirectory;
import org.hibernate.*;
import org.hibernate.Query;
import org.hibernate.classic.Lifecycle;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.lucene.IndexHelper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.admin.FullTextSearchSettingsAction;

@Entity
@Table(name = "DS_AR_DOKUMENTY_PUBLICZNE")
public class PublicDocument implements Lifecycle {

    private static final Logger log = LoggerFactory.getLogger(PublicDocument.class);

    @Id
    @Column(name = "DOCUMENT_ID", nullable = false)
    private Long documentId;

    @Column(name = "AUTHOR", nullable = false)
    private String author;

    @Column(name = "TITLE", nullable = false)
    private String title;

    @Column(name = "CTIME", nullable = false)
    private Date ctime;

    @Column(name = "ABSTRACT", nullable = false)
    private String abstrakt;

    @Column(name = "LOCATIONS", nullable = false)
    private String location;

    @Column(name = "TYP_DOKUMENTU", nullable = false)
    private String type;

    @Column(name = "ORGANIZATIONAL_UNIT", nullable = false)
    private String organizationalUnit;
    
    @Column(name = "KEYWORDS", nullable = false)
    private String keyWords;
    
    @Column(name = "ATTACHMENTS_COUNT", nullable = false)
    private Integer attachmentCount;
    
    @Column(name = "MINIATURA", nullable = false)
    private String base64Miniatura;
    

    public PublicDocument() {
    }

    public PublicDocument(Long documentId,
                          String author,
                          String title,
                          String abstrakt,
                          Date ctime,
                          String organizationalUnit,
                          String location,
                          String type,
                          String keyWords,
                          Integer attachmentCount,
                          String base64Miniatura) {
    	

        this.setDocumentId(documentId);
        this.setAuthor(author);
        this.setTitle(title);
        this.setAbstrakt(abstrakt);
        this.setOrganizationalUnit(organizationalUnit);
        this.setLocation(location);
        this.setCtime(ctime);
        this.setType(type);
        this.setKeyWords(keyWords);
        this.setAttachmentCount(attachmentCount);
        this.setBase64Miniatura(base64Miniatura);
       
        
    }

    public String toString() {
        return "" + documentId;
    }

    public String toStringLong() {
        return "PublicDocument{" +
                "documentId=" + documentId +
                ", author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", ctime=" + ctime +
                ", abstrakt='" + abstrakt + '\'' +
                ", location='" + location + '\'' +
                ", type='" + type + '\'' +
                ", organizationalUnit='" + organizationalUnit + '\'' +
                ", keyWords='" + keyWords + '\'' +
                 ", attachmentCount='" + attachmentCount + '\'' +
                    ", base64Miniatura='" + base64Miniatura + '\'' +
                '}';
    }

    public final void create() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch(HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    @Override
    public boolean onDelete(Session arg0) throws CallbackException {
        return false;
    }

    @Override
    public void onLoad(Session arg0, Serializable arg1) {
    }

    @Override
    public boolean onSave(Session arg0) throws CallbackException {
        return false;
    }

    @Override
    public boolean onUpdate(Session arg0) throws CallbackException {
        return false;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAbstrakt() {
        return abstrakt;
    }

    public void setAbstrakt(String abstrakt) {
        this.abstrakt = abstrakt;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOrganizationalUnit() {
        return organizationalUnit;
    }

    public void setOrganizationalUnit(String organizationalUnit) {
        this.organizationalUnit = organizationalUnit;
    }

    /**
     * Wyszukanie dokumentu publicznego po id
     *
     * @param documentId
     * @return
     * @throws pl.compan.docusafe.core.EdmException
     *
     */
    public static PublicDocument findById(Long documentId) throws EdmException {
        List<PublicDocument> list = (List<PublicDocument>) (DSApi.context().session().createCriteria(PublicDocument.class).add(
                Restrictions.eq("documentId", documentId)).list());

        if(list.size() == 0)
            return null;
        else
            return list.get(0);
    }
    /**
     * Wyszukanie dokument�w publicznych po s�owach kluczowych
     *
     * @param documentId
     * @return
     * @throws pl.compan.docusafe.core.EdmException
     *
     */
    public static List<PublicDocument> findByKeyWords(String keyWords) throws EdmException {
        List<PublicDocument> list = (List<PublicDocument>) (DSApi.context().session().createCriteria(PublicDocument.class).add(
                Restrictions.like("keyWords", "%" + keyWords + "%")).list());

        if(list.size() == 0)
            return null;
        else
            return list;
    }

    /**
     * Wyszukanie pozycji faktury po id
     *
     * @return lista dokumentow publicznie dostepnych
     * @throws pl.compan.docusafe.core.EdmException
     *
     */
    public static List<PublicDocument> list() throws EdmException {
        Criteria criteria = DSApi.context().session().createCriteria(PublicDocument.class);
        return criteria.list();
    }

    public static Iterator<PublicDocument> iterate() throws EdmException {
        final Query query = DSApi.context().session().createQuery("from " + PublicDocument.class.getSimpleName() + " pd");

        return query.iterate();
    }

    /**
     * Wyszukanie pozycji faktury po id
     *
     * @return lista dokumentow publicznie dostepnych
     * @throws pl.compan.docusafe.core.EdmException
     *
     */
    public static List<PublicDocument> list(int indexFrom, int indexTo) throws EdmException {
        Criteria criteria = DSApi.context().session().createCriteria(PublicDocument.class);
        List<PublicDocument> lista = criteria.list();
        if(list().size() == 0)
            return null;

        List<PublicDocument> listaZwracana = new ArrayList<PublicDocument>();
        for(int i = indexFrom; i < indexTo; i++) {
            listaZwracana.add(lista.get(i));
        }
        return listaZwracana;
    }

    /**
     * Wyszukanie pozycji faktury po id
     *
     * @return lista dokumentow publicznie dostepnych
     * @throws pl.compan.docusafe.core.EdmException
     *
     */
    public static List<PublicDocument> findByParametersDetailed(int indexFrom, int indexTo,
                                                                Long documentId,
                                                                String author,
                                                                String title,
                                                                String abstrakt,
                                                                String type,
                                                                String organizationalUnit,
                                                                String location,
                                                                Date ctimeFrom,
                                                                Date ctimeTo,
                                                                String keyWords
                                                                
                                                                
    ) throws EdmException {
        Criteria criteria = DSApi.context().session().createCriteria(PublicDocument.class);
        if(documentId != null)
            criteria.add(Restrictions.eq("documentId", documentId));
        if(author != null && !"".equalsIgnoreCase(author))
            criteria.add(Restrictions.like("author", "%" + author + "%"));
        if(title != null && !"".equalsIgnoreCase(title))
            criteria.add(Restrictions.like("title", "%" + title + "%"));
        if(abstrakt != null && !"".equalsIgnoreCase(abstrakt))
            criteria.add(Restrictions.like("abstrakt", "%" + abstrakt + "%"));
        if(type != null && !"".equalsIgnoreCase(type))
            criteria.add(Restrictions.like("type", "%" + type + "%"));
        if(organizationalUnit != null && !"".equalsIgnoreCase(organizationalUnit))
            criteria.add(Restrictions.like("organizationalUnit", "%" + organizationalUnit + "%"));
        if(location != null && !"".equalsIgnoreCase(location))
            criteria.add(Restrictions.like("location", "%" + location + "%"));
        if(keyWords != null && !"".equalsIgnoreCase(keyWords))
            criteria.add(Restrictions.like("keyWords", "%" + keyWords + "%"));
        
        
        if(ctimeFrom != null)
            criteria.add(Restrictions.ge("ctime", ctimeFrom));
        if(ctimeTo != null)
            criteria.add(Restrictions.le("ctime", ctimeTo));

        List<PublicDocument> lista = criteria.list();
        int listaSize = lista.size();
        if(listaSize == 0)
            return null;

        if(indexFrom > listaSize)
            return null;

        if(indexTo > listaSize)
            indexTo = listaSize;

        List<PublicDocument> listaZwracana = new ArrayList<PublicDocument>();

        for(int i = indexFrom; i < indexTo; i++) {
            listaZwracana.add(lista.get(i));
        }
        return listaZwracana;
    }

    public static List<PublicDocument> findByFullText(String fullText, int maxResults) throws Exception {
        log.debug("[findByFullText] begin");

        FSDirectory index = null;
        IndexSearcher searcher = null;

        try {
            String query = fullText;

            // get full text results
            String indexPath = Docusafe.getAdditionPropertyOrDefault("fulltext.documentsIndexFolder", "indexes_akta_publiczne");

            final File fileIndexes = new File(Docusafe.getHome(), indexPath);
            index = FSDirectory.open(fileIndexes);
            StandardAnalyzer analyzer = new StandardAnalyzer(org.apache.lucene.util.Version.LUCENE_46);

            org.apache.lucene.search.Query q = new QueryParser(org.apache.lucene.util.Version.LUCENE_46, IndexHelper.CONTENT_FIELD, analyzer).parse(query);

            log.debug("[findByFullText] query = {}", q);

            IndexReader reader = DirectoryReader.open(index);
            searcher = new IndexSearcher(reader);
            TopScoreDocCollector collector = TopScoreDocCollector.create(maxResults, true);
            searcher.search(q, collector);
            ScoreDoc[] hitsDoc = collector.topDocs().scoreDocs;

            log.debug("[findByFullText] hitsDoc.length = {}", hitsDoc.length);

            // get PublicDocument ids from full text search result

            List<Long> documentIds = new ArrayList<Long>();
            for(ScoreDoc aHitsDoc : hitsDoc) {
                int docId = aHitsDoc.doc;
                org.apache.lucene.document.Document d = searcher.doc(docId);
                documentIds.add(Long.valueOf(d.get("documentId")));

                log.debug("d.id = {}, content = {}", d.get("documentId"), d.get("content"));
            }

            log.debug("[findByFullText] ids = {}", documentIds);

            // get PublicDocuments from db

            if(! documentIds.isEmpty()) {
                Criteria crit = DSApi.context().session().createCriteria(PublicDocument.class);
                crit.add(Restrictions.in("id", documentIds));
                List ret = crit.list();

                log.debug("[findByFullText] ret.size = {}", ret.size());

                return ret;
            } else {
                log.debug("[findByFullText] ids empty, return empty list");
                return new ArrayList();
            }
        } catch(Exception ex) {
            throw new Exception(ex);
        } finally {
            if(index != null) {
                index.close();
            }
        }
    }

    public static List<PublicDocument> findByParametersAndFullText(int indexFrom, int indexTo,
                                                                   Long documentId,
                                                                   String author,
                                                                   String title,
                                                                   String abstrakt,
                                                                   String type,
                                                                   String organizationalUnit,
                                                                   String location,
                                                                   Date ctimeFrom,
                                                                   Date ctimeTo,
                                                                   String fullText,
                                                                   String keyWords
    ) throws Exception {
        log.debug("[findByParametersAndFullText] begin");
        List<PublicDocument> normalSearch = findByParameters(documentId, author, title, abstrakt, type, organizationalUnit, location, ctimeFrom, ctimeTo,keyWords);
        List<PublicDocument> fullTextSearch = findByFullText(fullText, indexTo);

        List<PublicDocument> result = intersection(normalSearch, fullTextSearch);

        return subList(result, indexFrom, indexTo);
    }

    public static List<PublicDocument> findByParameters(int indexFrom, int indexTo, Long documentId,
                                                        String author,
                                                        String title,
                                                        String abstrakt,
                                                        String type,
                                                        String organizationalUnit,
                                                        String location,
                                                        Date ctimeFrom,
                                                        Date ctimeTo,
                                                        String keyWords
                                                        ) throws EdmException {
        log.debug("[findByParameters] begin, with range");

        List<PublicDocument> list = findByParameters(documentId, author, title, abstrakt, type, organizationalUnit, location, ctimeFrom, ctimeTo, keyWords);

        log.debug("[findByParameters] list = {}", list);

        return subList(list, indexFrom, indexTo);
    }

    public static List<PublicDocument> findByParameters(
            Long documentId,
            String author,
            String title,
            String abstrakt,
            String type,
            String organizationalUnit,
            String location,
            Date ctimeFrom,
            Date ctimeTo, 
            String keyWords
    ) throws EdmException {
        log.debug("[findByParameters] begin");

        Criteria criteria = DSApi.context().session().createCriteria(PublicDocument.class);

        if(documentId != null)
            criteria.add(Restrictions.eq("documentId", documentId));
        else {
            List criteriaList = new ArrayList();
            if(author != null && !"".equalsIgnoreCase(author))
                criteriaList.add(Restrictions.like("author", "%" + author + "%").ignoreCase());
            if(title != null && !"".equalsIgnoreCase(title))
                criteriaList.add(Restrictions.like("title", "%" + title + "%").ignoreCase());
            if(abstrakt != null && !"".equalsIgnoreCase(abstrakt))
                criteriaList.add(Restrictions.like("abstrakt", "%" + abstrakt + "%").ignoreCase());
            if(type != null && !"".equalsIgnoreCase(type))
                criteriaList.add(Restrictions.like("type", "%" + type + "%").ignoreCase());
            if(organizationalUnit != null && !"".equalsIgnoreCase(organizationalUnit))
                criteriaList.add(Restrictions.like("organizationalUnit", "%" + organizationalUnit + "%").ignoreCase());
            if(location != null && !"".equalsIgnoreCase(location))
                criteriaList.add(Restrictions.like("location", "%" + location + "%").ignoreCase());
            if(ctimeFrom != null)
                criteriaList.add(Restrictions.ge("ctime", ctimeFrom));
            if(ctimeTo != null)
                criteriaList.add(Restrictions.le("ctime", ctimeTo));
            if(keyWords != null && !"".equalsIgnoreCase(keyWords))
                criteriaList.add(Restrictions.like("keyWords", "%" + keyWords + "%").ignoreCase());
            Criterion criterion = null;
            for(Object restrictions : criteriaList) {
                if(criterion != null)
                    criterion = Restrictions.or(criterion, (Criterion) restrictions);
                else
                    criterion = (Criterion) restrictions;
            }
            if(criterion != null)
                criteria.add(criterion);
        }
        List<PublicDocument> lista = criteria.list();

        return lista;
    }

    /**
     * <p>Odpowiednik <code>List.subList</code> z pewnymi modyfikacjami:
     * <p/>
     * Je�eli <code>List.subList</code> rzuci wyj�tkiem LUB zwr�ci pust� list� wtedy metoda zwraca <code>null</code>
     * </p>
     *
     * @param list
     * @param indexFrom
     * @param indexTo
     * @return
     */
    private static List subList(List list, int indexFrom, int indexTo) {
        List ret;
        try {
            ret = list.subList(indexFrom, indexTo >= list.size() ? list.size() : indexTo);
        } catch(IndexOutOfBoundsException ex) {
            log.error("[subList] index out of bound, from = {}, to = {}, list.size = {}", indexFrom, indexTo, list.size());
            return null;
        }

        if(ret.size() == 0) {
            return null;
        }

        return ret;
    }

    /**
     * Zwraca przeci�cie list na podstawie pola <code>id</code>.
     *
     * @param list1
     * @param list2
     * @return
     */
    private static List<PublicDocument> intersection(List<PublicDocument> list1, List<PublicDocument> list2) {

        ArrayList<PublicDocument> ret = new ArrayList<PublicDocument>();

        List<Long> ids1 = listOfIds(list1);
        List<Long> ids2 = listOfIds(list2);

        log.debug("[intersection] ids1 = {}, ids2 = {}", ids1, ids2);

        ids1.retainAll(ids2);

        log.debug("[intersection] intersection = {}", ids1);

        final Map<Long, PublicDocument> map = listToMap(list1);

        return new ArrayList<PublicDocument>(Collections2.transform(ids1, new Function<Long, PublicDocument>() {
            @Override
            public PublicDocument apply(Long id) {
                return map.get(id);
            }
        }));
    }

    private static List<Long> listOfIds(List<PublicDocument> list) {
        return new ArrayList<Long>(Collections2.transform(list, new Function<PublicDocument, Long>() {
            @Override
            public Long apply(PublicDocument doc) {
                return doc.getDocumentId();
            }
        }));
    }

    private static Map<Long, PublicDocument> listToMap(List<PublicDocument> list) {
        Map<Long, PublicDocument> map = new HashMap<Long, PublicDocument>();

        for(PublicDocument doc : list) {
            map.put(doc.getDocumentId(), doc);
        }

        return map;
    }

	public String getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}

	public Integer getAttachmentCount() {
		return attachmentCount;
	}

	public void setAttachmentCount(Integer attachmentCount) {
		this.attachmentCount = attachmentCount;
	}

	public String getBase64Miniatura() {
		return base64Miniatura;
	}

	public void setBase64Miniatura(String base64Miniatura) {
		this.base64Miniatura = base64Miniatura;
	}
}
