package pl.compan.docusafe.parametrization.archiwum;
		
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.Field.Type;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.office.Container.ArchiveStatus;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class BrakowanieDictionary extends DwrDictionaryBase{
	
	protected static Logger log = LoggerFactory.getLogger(BrakowanieDictionary.class);
	boolean __debug = true;
	private static String _USUN = "USUN";
	private static String _WYBRAKUJ = "WYBRAKUJ";
	
	@Override
	public long add(Map<String, FieldData> values) throws EdmException {
		Long id;
		if(values.get("symbol_wykazu")!=null)
			id = values.get("symbol_wykazu").getLongData();
		else
			id = values.get("SYMBOL_WYKAZU").getLongData();
		if (id != null) {
			OfficeFolder folder = OfficeFolder.find(id);
			if (folder != null) {
				// ArchiveStatus.None
				// Taki przypadek nie powinien wystapic, oznaczaloby ze na
				// spisie
				// zdawczo odbiorczym
				// nie jest ustawiany status ArchiveStatus.Archived
				if (folder.getArchiveStatus() == ArchiveStatus.None)
					return -1;

				if (folder.getArchiveStatus() == ArchiveStatus.Archived)
					folder.setArchiveStatus(ArchiveStatus.ToShred);
				else
					throw new EdmException("Teczka " + folder.getOfficeId()
							+ " jest zablokowana przez inny proces.");
				values.put("FOLDER_OFFICEID", new FieldData(Type.STRING, folder.getOfficeId()));
				
				if( values.containsKey(_USUN) && values.get(_USUN).getData()==null){
					
					values.put(_WYBRAKUJ, new FieldData(Type.BOOLEAN, "true"));
					values.put(_USUN, new FieldData(Type.BOOLEAN, "false"));
					
				}else if ( values.containsKey(_USUN) && values.get(_USUN).getBooleanData()) {
					values.put(_WYBRAKUJ, new FieldData(Type.BOOLEAN, "false"));
				}
				/*}else if {
					values.put(_WYBRAKUJ, new FieldData(Type.BOOLEAN, "false"));
				}*/
				/*if(!values.containsKey(_WYBRAKUJ))
					values.put(_WYBRAKUJ, new FieldData(Type.BOOLEAN, "true"));*/
				setupBoolean(values);					
			}
		}
		Long idPozycji = super.add(values);
		if(idPozycji>0)
		setOfficeForderId(id,idPozycji);
		return idPozycji;
	}

	private void setOfficeForderId(Long id, Long idPozycji)
	{
		
		//zmiana folderid na officeid folder_officeid 11-02-2015
		String sql = "Update DS_AR_BRAKOWANIE_POZYCJA set folder_officeid = ? where id =?";
		PreparedStatement ps;
		try
		{
			ps = DSApi.context().prepareStatement(sql);

			ps.setLong(1, id);
			ps.setLong(2, idPozycji);
			ps.execute();
			ps.close();
		} catch (SQLException e)
		{
			log.error("b�ad przy dodawnaiu id folder do ds_ar_brakowanie_pozycja", e);
		} catch (EdmException e)
		{
			// TODO Auto-generated catch block
			log.error("b�ad przy dodawnaiu id folder do ds_ar_brakowanie_pozycja", e);
		}
	}

	@Override
	public int update(Map<String, FieldData> values) throws EdmException {
		int superReturn = super.update(values);
		setupBoolean(values);
		return superReturn;
	}

	private void setupBoolean(Map<String, FieldData> values){
		
		if( values.containsKey(_USUN) && values.get(_USUN).getData()==null){
			
			values.put(_WYBRAKUJ, new FieldData(Type.BOOLEAN, "true"));
			values.put(_USUN, new FieldData(Type.BOOLEAN, "false"));
			
		}else if ( values.containsKey(_USUN) && values.get(_USUN).getBooleanData()) {
			values.put(_WYBRAKUJ, new FieldData(Type.BOOLEAN, "false"));
		}

		String table = "ds_ar_brakowanie_pozycja";
		Boolean value;
		FieldData fd = values.get("WYBRAKUJ");
		String column = "wybrakuj";
		if(fd == null) value = false;
		// ustawia flage wybrakuj na false  czyli jest do usuniecia 
		else if(fd.getData() == null) value = false;
		
		else value = fd.getBooleanData();
		if (!values.get("ID").getStringData().isEmpty()) {
			setBooleanValue(table, column, values.get("ID").getStringData(), value);
		}		
		
		fd = values.get("USUN");
		column = "usun";
		if(fd == null) value = false;
		else if(fd.getData() == null) value = false;
		else value = fd.getBooleanData();
		if (!values.get("ID").getStringData().isEmpty()) {
			setBooleanValue(table, column, values.get("ID").getStringData(), value);
		}
	}
	
	/**
	 * @param table
	 * @param column
	 * @param id
	 * @param value
	 */
	private void setBooleanValue(String table, String column, String id, Boolean value) {
		try {
			//String sql = "update " + table + " set " + column + " = "+ BooleanUtils.toInteger(value) +" where id = " + id;
			String sql = "update " + table + " set " + column + " = ? where id = ?";
			PreparedStatement ps = DSApi.context().prepareStatement(sql);
			ps.setBoolean(1, value);
			ps.setLong(2, Long.parseLong(id));
			ps.execute();
			ps.close();
		} catch (Exception e) {
			log.error("[Brakowanie]", e);
		}
	}
}
