package pl.compan.docusafe.parametrization.archiwum.services;

import java.util.Date;

public class PublicDocumentsParameters {
    private Long documentId;
    private String author;
    private String title;
    private String abstrakt;
    private String type;
    private String organizationalUnit;
    private String location;
    private Date ctimeFrom;
    private Date ctimeTo;
    private String keyWords;
    private String fullText;
    private boolean fullTextOnly = false;

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAbstrakt() {
        return abstrakt;
    }

    public void setAbstrakt(String abstrakt) {
        this.abstrakt = abstrakt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOrganizationalUnit() {
        return organizationalUnit;
    }

    public void setOrganizationalUnit(String organizationalUnit) {
        this.organizationalUnit = organizationalUnit;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getCtimeFrom() {
        return ctimeFrom;
    }

    public void setCtimeFrom(Date ctimeFrom) {
        this.ctimeFrom = ctimeFrom;
    }

    public Date getCtimeTo() {
        return ctimeTo;
    }

    public void setCtimeTo(Date ctimeTo) {
        this.ctimeTo = ctimeTo;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public boolean isFullTextOnly() {
        return fullTextOnly;
    }

    public void setFullTextOnly(boolean fullTextOnly) {
        this.fullTextOnly = fullTextOnly;
    }

	public String getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}
}
