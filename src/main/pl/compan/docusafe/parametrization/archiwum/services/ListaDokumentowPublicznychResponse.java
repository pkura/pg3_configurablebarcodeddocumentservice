package pl.compan.docusafe.parametrization.archiwum.services;

import pl.compan.docusafe.parametrization.archiwum.PublicDocument;

public class ListaDokumentowPublicznychResponse {

    private boolean error;
    private String statusMessage;
    private PublicDocument[] publicDocuments;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        error = error;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public PublicDocument[] getPublicDocuments() {
        return publicDocuments;
    }

    public void setPublicDocuments(PublicDocument[] publicDocuments) {
        this.publicDocuments = publicDocuments;
    }
}
