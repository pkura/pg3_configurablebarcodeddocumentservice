
/**
 * AktaPubliczneServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.6  Built on : Aug 30, 2011 (10:00:16 CEST)
 */

    package pl.compan.docusafe.parametrization.archiwum.services.client;

    /**
     *  AktaPubliczneServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class AktaPubliczneServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public AktaPubliczneServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public AktaPubliczneServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for zalacznik method
            * override this method for handling normal response from zalacznik operation
            */
           public void receiveResultzalacznik(
                    pl.compan.docusafe.parametrization.archiwum.services.client.AktaPubliczneServiceStub.ZalacznikResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from zalacznik operation
           */
            public void receiveErrorzalacznik(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wyszukiwanieOgolneDokumentow method
            * override this method for handling normal response from wyszukiwanieOgolneDokumentow operation
            */
           public void receiveResultwyszukiwanieOgolneDokumentow(
                    pl.compan.docusafe.parametrization.archiwum.services.client.AktaPubliczneServiceStub.WyszukiwanieOgolneDokumentowResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wyszukiwanieOgolneDokumentow operation
           */
            public void receiveErrorwyszukiwanieOgolneDokumentow(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for listaDokumentow method
            * override this method for handling normal response from listaDokumentow operation
            */
           public void receiveResultlistaDokumentow(
                    pl.compan.docusafe.parametrization.archiwum.services.client.AktaPubliczneServiceStub.ListaDokumentowResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from listaDokumentow operation
           */
            public void receiveErrorlistaDokumentow(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for iloscDokumentowPublicznych method
            * override this method for handling normal response from iloscDokumentowPublicznych operation
            */
           public void receiveResultiloscDokumentowPublicznych(
                    pl.compan.docusafe.parametrization.archiwum.services.client.AktaPubliczneServiceStub.IloscDokumentowPublicznychResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from iloscDokumentowPublicznych operation
           */
            public void receiveErroriloscDokumentowPublicznych(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for wyszukiwanieSzczegoloweDokumentow method
            * override this method for handling normal response from wyszukiwanieSzczegoloweDokumentow operation
            */
           public void receiveResultwyszukiwanieSzczegoloweDokumentow(
                    pl.compan.docusafe.parametrization.archiwum.services.client.AktaPubliczneServiceStub.WyszukiwanieSzczegoloweDokumentowResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from wyszukiwanieSzczegoloweDokumentow operation
           */
            public void receiveErrorwyszukiwanieSzczegoloweDokumentow(java.lang.Exception e) {
            }
                


    }
    