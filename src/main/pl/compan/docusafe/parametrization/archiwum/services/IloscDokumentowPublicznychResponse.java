package pl.compan.docusafe.parametrization.archiwum.services;

public class IloscDokumentowPublicznychResponse {

    private boolean error;
    private String statusMessage;
    private int count;

    public boolean getError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
