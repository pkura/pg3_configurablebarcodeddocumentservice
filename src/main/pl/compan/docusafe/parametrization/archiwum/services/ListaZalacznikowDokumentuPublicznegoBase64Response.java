package pl.compan.docusafe.parametrization.archiwum.services;

import pl.compan.docusafe.parametrization.archiwum.PublicDocument;

/**
 * Created by GFilip on 2015-09-15.
 */
public class ListaZalacznikowDokumentuPublicznegoBase64Response {
	  private boolean error;
	    private String statusMessage;
    private ZalacznikDokumentuPublicznegoResponse[] ListaZalacznikowDokumentuPublicznegoBase64Response;

	

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public ZalacznikDokumentuPublicznegoResponse[] getListaZalacznikowDokumentuPublicznegoBase64Response() {
		return ListaZalacznikowDokumentuPublicznegoBase64Response;
	}

	public void setListaZalacznikowDokumentuPublicznegoBase64Response(
			ZalacznikDokumentuPublicznegoResponse[] listaZalacznikowDokumentuPublicznegoBase64Response) {
		ListaZalacznikowDokumentuPublicznegoBase64Response = listaZalacznikowDokumentuPublicznegoBase64Response;
	}

}
