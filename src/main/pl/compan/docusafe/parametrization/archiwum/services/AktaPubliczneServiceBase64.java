package pl.compan.docusafe.parametrization.archiwum.services;

import org.apache.axis2.context.MessageContext;
import org.apache.axis2.context.OperationContext;
import org.apache.axis2.wsdl.WSDLConstants;
import org.apache.commons.codec.binary.Base64;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.parametrization.archiwum.ArchivePackageManager;
import pl.compan.docusafe.parametrization.archiwum.PublicDocument;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;

import java.io.File;
import java.io.Serializable;
import java.util.List;

public class AktaPubliczneServiceBase64 implements Serializable{

    private final static Logger log = LoggerFactory.getLogger(AktaPubliczneServiceBase64.class);

    public IloscDokumentowPublicznychResponse iloscDokumentowPublicznych(){
        IloscDokumentowPublicznychResponse response = new
                IloscDokumentowPublicznychResponse();
        boolean contextOpened = false;
        try {
            contextOpened = DSApi.openContextIfNeeded();
            if(!DSApi.context().inTransaction())
                DSApi.context().begin();

            response.setCount(PublicDocument.list().size());
            response.setStatusMessage("Pomy?lnie pobrano ilo?? dokument?w publicznych");
        }catch (Exception e){
            log.error("B??d: "+e);
            response.setStatusMessage("B??d: "+e);
            response.setError(true);
        } finally {
            try {
                DSApi.context().commit();
            } catch (Exception e) {
                log.error("Blad podczas zatwierdzania transakcji bazy danych: " + e);
                response.setError(true);
                response.setStatusMessage("Blad podczas otwierania kontekstu bazy danych: " + e);
                try {
                    DSApi.context().rollback();
                } catch (Exception ex) {
                    log.error("Blad podczas wycofywania transakcji bazy danych: " + ex);
                    response.setError(true);
                    response.setStatusMessage("Blad podczas wycofywania transakcji bazy danych: " + ex);
                }
            }
            DSApi.closeContextIfNeeded(contextOpened);
        }
        return response;
    }

   /* public ZalacznikDokumentuPublicznegoBase64Response zalacznik(Long documentId){

        ZalacznikDokumentuPublicznegoBase64Response response = new ZalacznikDokumentuPublicznegoBase64Response();
        response.setError(false);
        response.setStatusMessage("Rozpocz?to pobieranie za??cznik?w dokumentu o ID "+documentId);

        boolean contextOpened = false;
        try {
            contextOpened = DSApi.openContextIfNeeded();
            if(!DSApi.context().inTransaction())
                DSApi.context().begin();

            if (PublicDocument.findById(documentId)==null){
                response.setError(true);
                response.setStatusMessage("Brak dokumentu publicznego o podanym ID: " + documentId);
                return response;
            }


            File attachment = ArchivePackageManager.getAttachmetnFileForDocument(documentId);
            if (attachment==null){
                response.setError(true);
                response.setStatusMessage("Brak za??cznika dla dokumentu publicznego o podanym ID: " + documentId);
                return response;
            }
            String base64 = new String(Base64.encodeBase64(FileUtils.getBytesFromFile(attachment)));
            response.setBase64(base64);
            response.setFileName(attachment.getName());
            response.setStatusMessage("Pobieranie za??cznika zako?czone sukcesem.");
        }catch (Exception e){
            log.error("B??d: ",e);
            response.setStatusMessage("B??d: "+e);
            response.setError(true);
        } finally {
            try {
                DSApi.context().commit();
            } catch (Exception e) {
                log.error("Blad podczas zatwierdzania transakcji bazy danych: " + e);
                response.setError(true);
                response.setStatusMessage("Blad podczas otwierania kontekstu bazy danych: " + e);
                try {
                    DSApi.context().rollback();
                } catch (Exception ex) {
                    log.error("Blad podczas wycofywania transakcji bazy danych: " + ex);
                    response.setError(true);
                    response.setStatusMessage("Blad podczas wycofywania transakcji bazy danych: " + ex);
                }
            }
            DSApi.closeContextIfNeeded(contextOpened);
        }
        return response;
    }
  */
    
    public ListaZalacznikowDokumentuPublicznegoBase64Response zalaczniki(Long documentId){

    	ListaZalacznikowDokumentuPublicznegoBase64Response response = new ListaZalacznikowDokumentuPublicznegoBase64Response();
        response.setError(false);
        response.setStatusMessage("Rozpocz?to pobieranie za??cznik?w dokumentu o ID "+documentId);

        boolean contextOpened = false;
        try {
            contextOpened = DSApi.openContextIfNeeded();
            if(!DSApi.context().inTransaction())
                DSApi.context().begin();

            if (PublicDocument.findById(documentId)==null){
                response.setError(true);
                response.setStatusMessage("Brak dokumentu publicznego o podanym ID: " + documentId);
                return response;
            }
            List<ZalacznikDokumentuPublicznegoResponse> lista = null;
            lista = AktaPubliczneHelper.PrepareAttachmentPublicDocumentBase64(documentId);
            
            /*PublicDocument[] tabLista = new PublicDocument[lista.size()];
            response.setPublicDocuments(lista.toArray(tabLista));*/

            
            ZalacznikDokumentuPublicznegoResponse[] tabLista = new ZalacznikDokumentuPublicznegoResponse[lista.size()];
         response.setListaZalacznikowDokumentuPublicznegoBase64Response(lista.toArray(tabLista));
         response.setStatusMessage("Pobieranie za??cznikow zako?czone sukcesem.");
        }catch (Exception e){
            log.error("B??d: ",e);
            response.setStatusMessage("B??d: "+e);
            response.setError(true);
        } finally {
            try {
                DSApi.context().commit();
            } catch (Exception e) {
                log.error("Blad podczas zatwierdzania transakcji bazy danych: " + e);
                response.setError(true);
                response.setStatusMessage("Blad podczas otwierania kontekstu bazy danych: " + e);
                try {
                    DSApi.context().rollback();
                } catch (Exception ex) {
                    log.error("Blad podczas wycofywania transakcji bazy danych: " + ex);
                    response.setError(true);
                    response.setStatusMessage("Blad podczas wycofywania transakcji bazy danych: " + ex);
                }
            }
            DSApi.closeContextIfNeeded(contextOpened);
        }
        return response;
    }
    /**
     * 
     * @param keyWords
     * @return
     */
    public ListaDokumentowPublicznychResponse listaDokumentowBykeyWords(String keyWords){
	
        ListaDokumentowPublicznychResponse response = new ListaDokumentowPublicznychResponse();
        response.setError(false);
        response.setStatusMessage("Rozpocz?to pobieranie listy akt publicznie dost?pnych.");
        boolean contextOpened = false;
        try {
            contextOpened = DSApi.openContextIfNeeded();
            if(!DSApi.context().inTransaction())
                DSApi.context().begin();

            if (keyWords==null || keyWords.isEmpty()){
                response.setError(true);
                response.setStatusMessage("Parametr keyWords jest pusty albo  null ");
                return response;
            }
          
            List<PublicDocument> lista = null;

            lista = PublicDocument.findByKeyWords(keyWords);


            if (lista != null)
            {
                PublicDocument[] tabLista = new PublicDocument[lista.size()];
                response.setPublicDocuments(lista.toArray(tabLista));
                response.setStatusMessage("Pobieranie listy dokument?w publicznie dost?pnych zako?czone sukcesem.");
            } else
            {
                response.setStatusMessage("Nie znaleziono dokument?w publicznych spe?niaj?cych odpowiednie kryterium.");
            }
        }catch (Exception e){
            log.error("B??d: "+e);
            response.setStatusMessage("B??d: "+e);
            response.setError(true);
        } finally {
            try {
                DSApi.context().commit();
            } catch (Exception e) {
                log.error("Blad podczas zatwierdzania transakcji bazy danych: " + e);
                response.setError(true);
                response.setStatusMessage("Blad podczas otwierania kontekstu bazy danych: " + e);
                try {
                    DSApi.context().rollback();
                } catch (Exception ex) {
                    log.error("Blad podczas wycofywania transakcji bazy danych: " + ex);
                    response.setError(true);
                    response.setStatusMessage("Blad podczas wycofywania transakcji bazy danych: " + ex);
                }
            }
            DSApi.closeContextIfNeeded(contextOpened);
        }
        return response;
    }

    public DokumentPublicznyResponse dokument(Long dokumentId){
        DokumentPublicznyResponse response = new DokumentPublicznyResponse();
        response.setError(false);
        response.setStatusMessage("Rozpocz?to pobieranie dokumentu publicznego");


        try {
            DSApi.openAdmin();
            PublicDocument document = PublicDocument.findById(dokumentId);
            if(document == null) {
                throw new EdmException("brak dokumentu");
            }
            response.setPublicDocument(document);
            response.setStatusMessage("Dokument pobrany z sukcesem!");
        } catch (EdmException e) {
            log.error("", e);
            response.setError(true);
            response.setStatusMessage("B??d pobierania dokumentu!");
        }finally{
            DSApi._close();
        }
        return response;
    }

    public ListaDokumentowPublicznychResponse wyszukiwanieSzczegoloweDokumentow(int indeksOd,
                                                                                int indeksDo,
                                                                                PublicDocumentsParameters parameters
    ){
        ListaDokumentowPublicznychResponse response = new ListaDokumentowPublicznychResponse();
        response.setError(false);
        response.setStatusMessage("Rozpocz?to pobieranie listy akt publicznie dost?pnych.");
        boolean contextOpened = false;
        try {
            contextOpened = DSApi.openContextIfNeeded();
            if(!DSApi.context().inTransaction())
                DSApi.context().begin();

            if (indeksDo<indeksOd){
                response.setError(true);
                response.setStatusMessage("Parametr indeksOd jest wi?kszy od parametru indeksDo!");
                return response;
            }
            int iloscDokumentowPublicznych = PublicDocument.list().size();

            if (indeksDo>iloscDokumentowPublicznych){
                response.setError(true);
                response.setStatusMessage("Parametr indeksDo jest wi?kszy od ilo?ci dokument?w publicznych");
                return response;
            }

            if (indeksOd>iloscDokumentowPublicznych){
                response.setError(true);
                response.setStatusMessage("Parametr indeksOd jest wi?kszy od ilo?ci dokument?w publicznych");
                return response;
            }

            List<PublicDocument> lista = null;
            if (parameters != null){
                lista = PublicDocument.findByParametersDetailed(indeksOd, indeksDo,
                        parameters.getDocumentId(),
                        parameters.getAuthor(),
                        parameters.getTitle(),
                        parameters.getAbstrakt(),
                        parameters.getType(),
                        parameters.getOrganizationalUnit(),
                        parameters.getLocation(),
                        parameters.getCtimeFrom(),
                        parameters.getCtimeTo(),
                        parameters.getKeyWords());
            }else{
                lista = PublicDocument.findByParametersDetailed(indeksOd, indeksDo,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null);
            }

            if (lista != null)
            {
                PublicDocument[] tabLista = new PublicDocument[lista.size()];
                response.setPublicDocuments(lista.toArray(tabLista));
                response.setStatusMessage("Pobieranie listy dokument?w publicznie dost?pnych zako?czone sukcesem.");
            } else
            {
                response.setStatusMessage("Nie znaleziono dokument?w publicznych spe?niaj?cych odpowiednie kryterium.");
            }
        }catch (Exception e){
            log.error("B??d: "+e);
            response.setStatusMessage("B??d: "+e);
            response.setError(true);
        } finally {
            try {
                DSApi.context().commit();
            } catch (Exception e) {
                log.error("Blad podczas zatwierdzania transakcji bazy danych: " + e);
                response.setError(true);
                response.setStatusMessage("Blad podczas otwierania kontekstu bazy danych: " + e);
                try {
                    DSApi.context().rollback();
                } catch (Exception ex) {
                    log.error("Blad podczas wycofywania transakcji bazy danych: " + ex);
                    response.setError(true);
                    response.setStatusMessage("Blad podczas wycofywania transakcji bazy danych: " + ex);
                }
            }
            DSApi.closeContextIfNeeded(contextOpened);
        }
        return response;
    }

    public ListaDokumentowPublicznychResponse wyszukiwanieOgolneDokumentow(int indeksOd,
                                                                           int indeksDo,
                                                                           PublicDocumentsParameters parameters
    ){
        ListaDokumentowPublicznychResponse response = new ListaDokumentowPublicznychResponse();
        response.setError(false);
        response.setStatusMessage("Rozpocz?to pobieranie listy akt publicznie dost?pnych.");
        boolean contextOpened = false;
        try {
            contextOpened = DSApi.openContextIfNeeded();
            if(!DSApi.context().inTransaction())
                DSApi.context().begin();

            if (indeksDo<indeksOd){
                response.setError(true);
                response.setStatusMessage("Parametr indeksOd jest wi?kszy od parametru indeksDo!");
                return response;
            }
            int iloscDokumentowPublicznych = PublicDocument.list().size();

            if (indeksDo>iloscDokumentowPublicznych){
                response.setError(true);
                response.setStatusMessage("Parametr indeksDo jest wi?kszy od ilo?ci dokument?w publicznych");
                return response;
            }

            if (indeksOd>iloscDokumentowPublicznych){
                response.setError(true);
                response.setStatusMessage("Parametr indeksOd jest wi?kszy od ilo?ci dokument?w publicznych");
                return response;
            }

            List<PublicDocument> lista = null;
            if (parameters != null){

                if(parameters.getFullText() != null) {

                    if(parameters.isFullTextOnly()) {
                        lista = PublicDocument.findByFullText(parameters.getFullText(), indeksDo);
                    } else {
                        lista = PublicDocument.findByParametersAndFullText(indeksOd, indeksDo,
                                parameters.getDocumentId(),
                                parameters.getAuthor(),
                                parameters.getTitle(),
                                parameters.getAbstrakt(),
                                parameters.getType(),
                                parameters.getOrganizationalUnit(),
                                parameters.getLocation(),
                                parameters.getCtimeFrom(),
                                parameters.getCtimeTo(),
                                parameters.getFullText(),
                                parameters.getKeyWords());
                    }
                } else {
                    lista = PublicDocument.findByParameters(indeksOd, indeksDo,
                            parameters.getDocumentId(),
                            parameters.getAuthor(),
                            parameters.getTitle(),
                            parameters.getAbstrakt(),
                            parameters.getType(),
                            parameters.getOrganizationalUnit(),
                            parameters.getLocation(),
                            parameters.getCtimeFrom(),
                            parameters.getCtimeTo(),
                    	parameters.getKeyWords());
                }

            } else {
                lista = PublicDocument.findByParameters(indeksOd, indeksDo,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null);
            }

            if (lista != null)
            {
                PublicDocument[] tabLista = new PublicDocument[lista.size()];
                response.setPublicDocuments(lista.toArray(tabLista));
                response.setStatusMessage("Pobieranie listy dokument?w publicznie dost?pnych zako?czone sukcesem.");
            } else
            {
                response.setStatusMessage("Nie znaleziono dokument?w publicznych spe?niaj?cych odpowiednie kryterium.");
            }
        }catch (Exception e){
            log.error("B??d: "+e);
            response.setStatusMessage("B??d: "+e);
            response.setError(true);
        } finally {
            try {
                DSApi.context().commit();
            } catch (Exception e) {
                log.error("Blad podczas zatwierdzania transakcji bazy danych: " + e);
                response.setError(true);
                response.setStatusMessage("Blad podczas otwierania kontekstu bazy danych: " + e);
                try {
                    DSApi.context().rollback();
                } catch (Exception ex) {
                    log.error("Blad podczas wycofywania transakcji bazy danych: " + ex, ex);
                    response.setError(true);
                    response.setStatusMessage("Blad podczas wycofywania transakcji bazy danych: " + ex);
                }
            }
            DSApi.closeContextIfNeeded(contextOpened);
        }
        return response;
    }
}
