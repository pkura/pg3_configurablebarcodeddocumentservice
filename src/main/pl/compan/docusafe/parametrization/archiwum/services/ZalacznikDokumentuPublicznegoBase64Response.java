package pl.compan.docusafe.parametrization.archiwum.services;

/**
 * Created by lwozniak on 2015-09-15.
 */
public class ZalacznikDokumentuPublicznegoBase64Response extends ZalacznikDokumentuPublicznegoResponse{
    private String base64;
    private String mime;

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }

	public String getMime() {
		return mime;
	}

	public void setMime(String mime) {
		this.mime = mime;
	}
	
	public String getFileName() {
		// TODO Auto-generated method stub
		return super.getFileName();
	}
}
