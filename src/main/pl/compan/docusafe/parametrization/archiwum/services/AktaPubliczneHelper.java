package pl.compan.docusafe.parametrization.archiwum.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.jackrabbit.JackrabbitAttachmentRevision;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.archiwum.ArchivePackageManager;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.io.Files;

public class AktaPubliczneHelper {
	private static final Logger log = LoggerFactory
			.getLogger(AktaPubliczneHelper.class.getPackage().getName());

	/**
	 * szuka zalacznika o nazwie miniatura w dokumencie jesli znajdcie to zwraca jego base64 as string albo pusty string
	 * @param doc
	 * @return
	 */
	public static String getBase64Miniatura(OfficeDocument doc) {

		String base64Miniatura = "";
		try {

			List<Attachment> attachmentList = doc.getAttachments();

			for (Attachment attachment : attachmentList) {
				AttachmentRevision revision = attachment
						.getMostRecentRevision();
				if (revision.getOriginalFilename().toLowerCase()
						.contains("miniatura")) {

					InputStream is = null;

					if (revision instanceof JackrabbitAttachmentRevision) {
						is = ((JackrabbitAttachmentRevision) revision)
								.getBinaryStream();
					} else {
						is = revision.getAttachmentStream();
					}

					File file = new File(revision.getOriginalFilename());
					OutputStream os = new FileOutputStream(file);
					IOUtils.copy(is, os);
					IOUtils.closeQuietly(is);
					IOUtils.closeQuietly(os);
					base64Miniatura = new String(Base64.encodeBase64(FileUtils
							.getBytesFromFile(file)));
				}
			}
		} catch (Exception e) {
			log.error("Error: " + e.getStackTrace());
		}
		return base64Miniatura;

	}
	
	
	
	
	
	static List<ZalacznikDokumentuPublicznegoResponse> PrepareAttachmentPublicDocumentBase64(
			
			
			Long documentId) {
		List<ZalacznikDokumentuPublicznegoResponse> list = new ArrayList<ZalacznikDokumentuPublicznegoResponse>();
		OfficeDocument doc;
		try {
			doc = OfficeDocument.find(documentId);

			List<Attachment> attachmentList = doc.getAttachments();
				for (Attachment attachment : attachmentList) {
					ZalacznikDokumentuPublicznegoResponse sigleAtt = new ZalacznikDokumentuPublicznegoResponse();
					AttachmentRevision revision = attachment
							.getMostRecentRevision();
				if(revision.getOriginalFilename().toLowerCase().contains("miniatura")){
					continue;
				}
					InputStream is = null;
					try {
						if (revision instanceof JackrabbitAttachmentRevision) {
							is = ((JackrabbitAttachmentRevision) revision)
									.getBinaryStream();
						} else {
							is = revision.getAttachmentStream();
						}
					} catch (Exception e) {
						log.error("Error: " + e.getStackTrace());
					}
					File file = new File(revision.getOriginalFilename());
					OutputStream os = new FileOutputStream(file);
					IOUtils.copy(is, os);
					IOUtils.closeQuietly(is);
					IOUtils.closeQuietly(os);
					sigleAtt.setFileName(revision.getOriginalFilename());
					sigleAtt.setMime(revision.getMime());
					 String base64 = new String(Base64.encodeBase64(FileUtils.getBytesFromFile(file)));
					 sigleAtt.setBase64(base64);
					 list.add(sigleAtt);
					 sigleAtt.setAttachmentId(attachment.getId());
				
				// String zippedFilePath = zipDirectory(tempDir);
				// File zippedFile = new File(zippedFilePath);
				// return zippedFile;
				}
				return list;
		} catch (DocumentNotFoundException e) {
			log.error("", e);
		} catch (EdmException e) {
			log.error("", e);
		} catch (FileNotFoundException e) {
			log.error("", e);
		} catch (IOException e) {
			log.error("", e);
		}
		
		return list;
	}

}
