package pl.compan.docusafe.parametrization.archiwum.services;

import pl.compan.docusafe.parametrization.archiwum.PublicDocument;

/**
 * Created by lwozniak on 2015-09-18.
 */
public class DokumentPublicznyResponse  {
    private boolean error;
    private String statusMessage;
    private PublicDocument publicDocument;

    public boolean getError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public PublicDocument getPublicDocument() {
        return publicDocument;
    }

    public void setPublicDocument(PublicDocument publicDocument) {
        this.publicDocument = publicDocument;
    }
}
