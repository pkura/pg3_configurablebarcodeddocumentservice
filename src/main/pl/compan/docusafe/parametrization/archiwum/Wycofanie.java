package pl.compan.docusafe.parametrization.archiwum;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;
import java.util.Set;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.Container.ArchiveStatus;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.utp.OfficeUtils;
import pl.compan.docusafe.util.StringManager;

/*Wycofanie sprawy z archiwum ustawia dla wycofywanej sprawy archivestatus na null, tworzy nowa teczke o ile nie istnieje i sprawe. 
 * Do sprawy sa przenoszone dokumenty z wycofywanej sprawy i trafiaja do uzytkownika ktory wnioskowal o wycofanie.
 * 
 */
public class Wycofanie implements ExternalActivityBehaviour{

	public void execute(ActivityExecution activity) throws Exception {
		StringManager sm = GlobalPreferences.loadPropertiesFile(
				Wycofanie.class.getPackage().getName(), null);
		Long documentId = Jbpm4Utils.getDocumentId(activity);
		Document doc = Document.find(documentId);
		FieldsManager fm = doc.getFieldsManager();
		fm.initialize();
		long userName= (long) (Integer) fm.getEnumItem("DSUSER").getId();
		DSUser user = DSUser.findById(userName);
		String caseId = String.valueOf(fm.getValue("sprawaId"));
		OfficeCase offCase = OfficeCase.find(Long.valueOf(caseId));
		OfficeFolder oldFolder = offCase.getParent();
		offCase.setArchiveStatus(ArchiveStatus.None);
		int userDivision = fm.getEnumItem("DEPARTUSER").getId();
		DSDivision div = DSDivision.findById(userDivision);
		//Tworzenie nowej teczki o ile istnieje
		OfficeFolder folder = OfficeUtils.newOfficeFolderIfNotExist(
				String.valueOf(offCase.getRwa().getId()),
				div.getGuid(),
				oldFolder.getName(),
				null);
		
		String title = "Wycofana z Archiwum :"+(offCase.getTitle()!=null ? offCase.getTitle(): "");
		if(title.length()>199 )
			title = title.substring(0, 198);
		String description =  "Wycofana z Archiwum :"+(offCase.getDescription()!=null ? offCase.getDescription(): "");
		if(description.length()>199 )
			description = description.substring(0, 198);
		//Tworzenie nowej sprawy
		OfficeCase sprawa = OfficeUtils.newOfficeCase(
				null,
				folder,
				null,
				title,
				description,
				user.getName(),
				null
				);
		folder.setAuthor(user.getName());
		folder.setClerk(user.getName());
		sprawa.setAuthor(user.getName());
		Remark remark = new Remark(sm.getString("uwaga") + offCase.getOfficeId() , user.getName());
		Remark remarkNew = new Remark(sm.getString("uwagaNew") + sprawa.getOfficeId() , user.getName());
		offCase.addRemark(remark);
		sprawa.addRemark(remarkNew);
		//przypisanie dokumentow do nowej sprawy
		ResultSet rs = null;
		PreparedStatement ps = null;
		OfficeDocument document;
//		ps = DSApi.context().prepareStatement(" select id from DS_DOCUMENT");
		ps = DSApi.context().prepareStatement(" select id from dso_in_document indoc where indoc.CASE_ID = ? "+
		" union "+
		" select id from dso_out_document outdoc where outdoc.CASE_ID = ? ");
		ps.setLong(1, offCase.getId());
		ps.setLong(2, offCase.getId());
		rs = ps.executeQuery();
		while(rs.next())
		{
			document = OfficeDocument.findOfficeDocument(rs.getLong(1) , false); 
			if(document instanceof InOfficeDocument && ((InOfficeDocument) document).getContainingCaseId() != null){
				InOfficeDocument docIn = InOfficeDocument.findInOfficeDocument(document.getId());
				if(offCase.getId().equals(docIn.getContainingCaseId())){
					docIn.setContainingCaseId(sprawa.getId());
					docIn.setCaseDocumentId(sprawa.getOfficeId());
					//nadanie uprawnien dla uzytkownika do odczytu i zapisu dokumentu
					java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
					String login = user.getName();
					String fullName = user.getLastname() + " "
							+ user.getFirstname();

					perms.add(new PermissionBean(ObjectPermission.READ,
							login, ObjectPermission.USER, fullName + " ("
									+ login + ")"));
					perms.add(new PermissionBean(ObjectPermission.MODIFY,
							login, ObjectPermission.USER, fullName + " ("
									+ login + ")"));

					Set<PermissionBean> documentPermissions = DSApi
							.context().getDocumentPermissions(document);
					perms.addAll(documentPermissions);
					((AbstractDocumentLogic) document.getDocumentKind()
							.logic()).setUpPermission(document, perms);
				}
			} else if(document instanceof OutOfficeDocument && ((OutOfficeDocument) document).getCaseDocumentId() != null){
				OutOfficeDocument docOut = OutOfficeDocument.findOutOfficeDocument(document.getId());
				if(offCase.getId().equals(docOut.getContainingCaseId())){
					docOut.setContainingCaseId(sprawa.getId());
					docOut.setCaseDocumentId(sprawa.getOfficeId());
					//nadanie uprawnien dla uzytkownika do odczytu i zapisu dokumentu
					java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
					String login = user.getName();
					String fullName = user.getLastname() + " "
							+ user.getFirstname();

					perms.add(new PermissionBean(ObjectPermission.READ,
							login, ObjectPermission.USER, fullName + " ("
									+ login + ")"));
					perms.add(new PermissionBean(ObjectPermission.MODIFY,
							login, ObjectPermission.USER, fullName + " ("
									+ login + ")"));

					Set<PermissionBean> documentPermissions = DSApi
							.context().getDocumentPermissions(document);
					perms.addAll(documentPermissions);
					((AbstractDocumentLogic) document.getDocumentKind()
							.logic()).setUpPermission(document, perms);
				}
			}
		}
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {

	}
}
