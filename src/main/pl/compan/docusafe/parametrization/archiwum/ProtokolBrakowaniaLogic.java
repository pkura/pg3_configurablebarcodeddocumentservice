package pl.compan.docusafe.parametrization.archiwum;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.Format;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class ProtokolBrakowaniaLogic extends AbstractDocumentLogic {
	
	private static final long serialVersionUID = 1L;
	private static ProtokolBrakowaniaLogic instance;
	protected static Logger log = LoggerFactory.getLogger(ProtokolBrakowaniaLogic.class);
	public static ProtokolBrakowaniaLogic getInstance() {
		if (instance == null)
			instance = new ProtokolBrakowaniaLogic();
		return instance;
	}
	
	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));
		
		for (PermissionBean perm : perms) {
			if (perm.isCreateGroup()
					&& perm.getSubjectType().equalsIgnoreCase(
							ObjectPermission.GROUP)) {
				String groupName = perm.getGroupName();
				if (groupName == null) {
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	public void setInitialValues(FieldsManager fm, int type)
			throws EdmException {
		log.info("type: {}", type);
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("TYPE", type);
		values.put(ProtokolBrakowaniaStale.CN_DATA_DOKUMENTU,  new Date());
		Calendar cal = Calendar.getInstance();
		values.put(ProtokolBrakowaniaStale.CN_ROK, Integer.toString(Calendar.YEAR));
		values.put(ProtokolBrakowaniaStale.CN_DOC_DESCRIPTION , "Protok馧 brakowania");
		values.put(ProtokolBrakowaniaStale.CN_TYP_DOKUMENTU, 2);
		values.put(ProtokolBrakowaniaStale.CN_STATUS, 10);
	//	values.put("DOC_AUTOR", DSApi.context().getDSUser().getId());
		//fillCreatingUser(values);
		
		fm.reloadValues(values);
	}

	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {
		as.useFolderResolver(document);
		as.useAvailableProviders(document);
		log.info("document title : '{}', description : '{}'",
				document.getTitle(), document.getDescription());
	}

	@Override
	public void onStartProcess(OfficeDocument document) throws EdmException {
		log.info("--- " + this.getClass().getName() + " : START PROCESS !!! ---- ");
		try {
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, document.getAuthor());
			map.put(ASSIGN_DIVISION_GUID_PARAM, document.getDivisionGuid());

			document.getDocumentKind().getDockindInfo()
					.getProcessesDeclarations().onStart(document, map);

			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
	
	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) {
		log.info("--- " + this.getClass().getName() + " : START PROCESS !!! ---- {}", event);
		try {
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));

			document.getDocumentKind().getDockindInfo() .getProcessesDeclarations().onStart(document, map);

			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
	
	@Override
	public void setAdditionalTemplateValues(long docId,
			Map<String, Object> values, Map<Class, Format> defaultFormatters) {
		try {
			int i = 0;
			int tomy = 0;
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			fm.initialize();
			
			
			//Dodanie cast as numeric , Artur Gdula 13-02-2015
			StringBuilder query = new StringBuilder(
					"select * from ds_ar_brakowanie_multiple bm left join ds_ar_brakowanie_pozycja bp on cast(bm.FIELD_VAL as numeric) = bp.id where bm.DOCUMENT_ID = "
							+ docId);
			ResultSet rs;
			List<Map<String, String>> pozycje = Lists.newArrayList();
			try {
				PreparedStatement ps = DSApi.context().prepareStatement(query);
				rs = ps.executeQuery();
				

				String[] dbCol = { 
						/* 0 */ "numer_wykazu",
						/* 1 */ "tytul_wykazu",
						/* 2 */ "data_od",
						/* 3 */ "data_do",
						/* 4 */ "liczba_tomow",
						/* 5 */ "uwagi",
						/* 6 */ "symbol_wykazu" };

				Double metry_biezace = 0D;
				while (rs.next()) {
					i++;
					HashMap<String, String> pozycja = new HashMap<String, String>(7);

					pozycja.put("lp", Integer.toString(i));
					metry_biezace += rs.getDouble("metry_biezace");
					
					// numer wykazu
					if (rs.getString(dbCol[0]) != null && !rs.getString(dbCol[0]).equals(""))
						pozycja.put("znak", rs.getString(dbCol[0]));
					else
						pozycja.put("znak", "-----");
					
					// tytul_wykazu
					if (rs.getString(dbCol[1]) != null
							&& !rs.getString(dbCol[1]).equals(""))
						pozycja.put("rwatytul", rs.getString(dbCol[1]));
					else
						pozycja.put("rwatytul", "-----");
					
					// daty od, do
					//jakies dziwne parsowanie lokalnie na windowsiew paruje dobrze a na linuxie wywala error unparsable moze wina 2 jav zainstalowanych 6 i 8  ?
					/*DateFormat dateFormat = DateFormat.getDateInstance();
					if (rs.getString(dbCol[2]) != null && !rs.getString(dbCol[2]).equals(""))
						pozycja.put("data_od", DateUtils.formatJsDate(dateFormat.parse(rs.getString(dbCol[2]))));*/
					DateFormat dateFormat = DateFormat.getDateInstance();
					if (rs.getString(dbCol[2]) != null && !rs.getString(dbCol[2]).equals(""))
						pozycja.put("data_od", rs.getString(dbCol[2]));
					else
						pozycja.put("data_od", "---");
					//jakies dziwne parsowanie lokalnie na windowsiew paruje dobrze a na linuxie wywala error unparsable moze wina 2 jav zainstalowanych 6 i 8  ?
					/*if (rs.getString(dbCol[3]) != null && !rs.getString(dbCol[3]).equals(""))
						pozycja.put("data_do", DateUtils.formatJsDate(dateFormat.parse(rs.getString(dbCol[3]))));*/
					if (rs.getString(dbCol[3]) != null && !rs.getString(dbCol[3]).equals(""))
						pozycja.put("data_do", rs.getString(dbCol[3]));
					else
						pozycja.put("data_do", "---");
					
					// ilosc tomow
					if (rs.getString(dbCol[4]) != null && !rs.getString(dbCol[4]).equals("")){
						pozycja.put("ilosc_tomow", rs.getString(dbCol[4]));
						tomy += Integer.parseInt(rs.getString(dbCol[4]));
					}
					else
						pozycja.put("ilosc_tomow", "---");
					
					// uwagi
					if (rs.getString(dbCol[5]) != null && !rs.getString(dbCol[5]).equals(""))
						pozycja.put("uwagi", rs.getString(dbCol[5]));
					else
						pozycja.put("uwagi", "-----");
					
					// symbol wykazu
					if (rs.getString(dbCol[6]) != null && !rs.getString(dbCol[6]).equals("")){
						OfficeFolder folder = OfficeFolder.find(Long.valueOf(rs.getString(dbCol[6])));
						pozycja.put("symbol", folder.getOfficeId());
					}
					else
						pozycja.put("symbol", "-----");
					pozycje.add(pozycja);
				}
				values.put("metry", metry_biezace);
				ps.close();
			} catch (SQLException e) {
				log.error(e.getMessage(), e);
				e.printStackTrace();
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
			values.put("pozycje", pozycje);
			
			values.put("member0", getUczestnikString(fm , ProtokolBrakowaniaStale.getCnUczestnik(0)));
			values.put("member1", getUczestnikString(fm , ProtokolBrakowaniaStale.getCnUczestnik(1)));
			values.put("member2", getUczestnikString(fm , ProtokolBrakowaniaStale.getCnUczestnik(2)));
			values.put("member3", getUczestnikString(fm , ProtokolBrakowaniaStale.getCnUczestnik(3)));
			values.put("tomow", tomy);
			values.put("pozycji", i);
			values.put("DATA", DateUtils.formatBipDate(new Date()));
			values.put("CASEID", doc.getCaseDocumentId());
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}
	}

	private String getUczestnikString(FieldsManager fm, String cnUczestnik) throws EdmException
	{
		String ucz = "";

		if (fm.getValue(cnUczestnik) != null)
		{
			ucz = ((String) fm.getValue(ProtokolBrakowaniaStale.getCnUczestnik(0))).substring(0,
					((String) fm.getValue(ProtokolBrakowaniaStale.getCnUczestnik(0))).lastIndexOf("("));
		}

		return ucz;
	}
	
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) {
		return setMultipledictionaryValue(dictionaryName, values, null);
	}
	
	 /** Wywolywane za ka盥ym razem, po tym jak u篡tkownik zmieni
     * warto嗆 pola w s這wniku (wybierze pozycj� z enum, database
     * lub sko鎍zy wpisywa� warto嗆 z klawiatury).
     *
     * S逝篡 do aktualizacji p鏊 tego s這wnika.
     *
     * Wywo造wane wcze郾iej ni� validateDwr.
     *
     * @param dictionaryName - nazwa tego s這wnika, bez przedrostka DWR_
     * @param values - tylko i wy陰cznie wartosci jednego rekordu, w kt鏎ym nast徙i豉 zmiana,
     * bez dost瘼u do reszty p鏊, innych s這wnik闚 i innych rekord闚 tego s這wnika */
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fValues) {
		Map<String, Object> results = new HashMap<String, Object>();
		if(dictionaryName.equalsIgnoreCase("BRAKOWANIE_POZYCJA")){

			Object id_teczki = values.get("BRAKOWANIE_POZYCJA_SYMBOL_WYKAZU").getData();
			
			String tableCN = ("brakowanie_pozycja".toUpperCase());
			String[] polaSlownika = {
					("tytul_wykazu".toUpperCase())
					,("numer_wykazu".toUpperCase())
					,("data_od".toUpperCase())
					,("data_do".toUpperCase())
					,("metry_biezace".toUpperCase())
					,("liczba_tomow".toUpperCase())
					,("folder_officeid".toUpperCase())
			};
			
			if (id_teczki == null)
				return results;
			else{
				FromClause from = new FromClause();
				TableAlias divisionTable = from.createTable("ds_ar_brakowanie_pozycja_view");
				WhereClause where = new WhereClause();
				//zmiana parsowanie na integer Artur Gdula 02-13-2015
				Integer id_teczki2 = Integer.parseInt((String) id_teczki);
				where.add(Expression.eq(divisionTable.attribute("id"), id_teczki2));
				SelectClause select = new SelectClause(true);
				SelectColumn idTitle = select.add(divisionTable, "RWATYTUL");
				SelectColumn number = select.add(divisionTable, "NUMBER_ITR");
				SelectColumn idfdate = select.add(divisionTable, "DATADO");
				SelectColumn idodate = select.add(divisionTable, "DATAOD");
				SelectColumn metry_biezace = select.add(divisionTable, "metryBiezace");
				SelectColumn liczba_tomow = select.add(divisionTable, "liczbaTomow");
				SelectColumn folder_officeid = select.add(divisionTable, "TITLE");
				SelectQuery query;
				SelectClause selectId = new SelectClause(true);
				ResultSet rs = null;
				query = new SelectQuery(select, from, where, null);
				try {
					rs = query.resultSet();
					if(rs.next()){
						//tytul
						if(rs.getString(idTitle.getPosition())!=null)results.put(tableCN+"_"+polaSlownika[0],  rs.getString(idTitle.getPosition()));
						else results.put(tableCN+"_"+polaSlownika[0], "");
						//numer pozycji spisu
						if(rs.getString(number.getPosition())!=null)results.put(tableCN+"_"+polaSlownika[1],  rs.getString(number.getPosition()));
						else results.put(tableCN+"_"+polaSlownika[1], "");
						
						//daty od i do
						if(rs.getString(idfdate.getPosition())!=null)results.put(tableCN+"_"+polaSlownika[2],  rs.getDate(idfdate.getPosition()));
						else results.put(tableCN+"_"+polaSlownika[2], "");
						if(rs.getString(idodate.getPosition())!=null)results.put(tableCN+"_"+polaSlownika[3],  rs.getDate(idodate.getPosition()));
						else results.put(tableCN+"_"+polaSlownika[3], "");
						//metry biezace
						if (rs.getString(metry_biezace.getPosition()) != null) results.put(tableCN + "_" + polaSlownika[4], rs.getDouble(metry_biezace.getPosition()));
						else results.put(tableCN + "_" + polaSlownika[4], "");
						//liczba tomow
						if(rs.getString(liczba_tomow.getPosition())!=null)results.put(tableCN+"_"+polaSlownika[5],  rs.getInt(liczba_tomow.getPosition()));
						else results.put(tableCN+"_"+polaSlownika[5], "");
						//folder office id
						if(rs.getString(folder_officeid.getPosition())!=null)results.put(tableCN+"_"+polaSlownika[6],  rs.getString(folder_officeid.getPosition()));
						else results.put(tableCN+"_"+polaSlownika[6], "");
					}
					values.remove("BRAKOWANIE_POZYCJA_SYMBOL_WYKAZU");
				} catch (SQLException e) {
					 log.error(e.getMessage(), e);
					e.printStackTrace();
				} catch (EdmException e) {
					 log.error(e.getMessage(), e);
					e.printStackTrace();
				}
			}
			
			FieldData fdWybrakuj = values.get(tableCN+"_WYBRAKUJ");
			FieldData fdUsun = values.get(tableCN+"_USUN");

			if(fdUsun.getBooleanData()) {
				results.put(tableCN+"_WYBRAKUJ", 0);
			} else {
				results.put(tableCN+"_WYBRAKUJ", 1);
			}
		}
		
		return results;
	}
	
	
	@Override
	public void doDockindEvent(DockindButtonAction eventActionSupport,
			ActionEvent event, Document document, String activity,
			Map<String, Object> values, Map<String, Object> dockindKeys)
			throws EdmException {
		super.doDockindEvent(eventActionSupport, event, document, activity, values,
				dockindKeys);
	}

	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
			throws EdmException {
		return super.validateDwr(values, fm);
	}
	
	public static class ProtokolBrakowaniaStale {
		public static String CN_STATUS = "STATUS";
		public static String CN_ROK = "rok";
		public static String CN_DZIAL = "ds_division_id";
		public static String CN_AP_NR_DECYZJI = "ap_numer_decyzji";
		public static String CN_AP_DATA_DECYZJI = "ap_data_decyzji"; 
		public static String CN_DICT_BRAKOWANIE_POZYCJA = "BRAKOWANIE_POZYCJA";
		public static String CN_WYBRAKUJ = "wybrakuj";
		public static String CN_USUN = "usun";
		public static String CN_DATA_DOKUMENTU ="DOC_DATE";
		public static String CN_AUTOR_DOKUMENTU ="DOC_AUTOR";
		public static String CN_DZIAL_AUTORA_DOKUMENTU ="DOC_AUTORDIVISION";
		public static String CN_TYP_DOKUMENTU = "TYP_DOKUMENTU";
		public static String CN_ABSTRAKT_DOKUMENTU = "ABSTRAKT";
		public static String CN_MIESCEUTWOORZENIA_DOKUMENTU = "MIESCEUTWOORZENIA";
		public static String CN_DOC_DESCRIPTION = "DOC_DESCRIPTION";

		
		//Komisja
		/** 
		 * @param i 0-przewodniczacy
		 * @return 
		 */
		public static String getCnUczestnik(int i){
			if(i==0) return "przewodniczacy";
			else return "uczestnik_"+i;
		}
		/** 
		 * @param i 0-przewodniczacy
		 * @return 
		 */
		public static String getCnUczestnikData(int i){
			if(i==0) return "przewodniczacy";
			else return "uczestnik_"+i+"_data";
		}
		/** 
		 * @param i 0-przewodniczacy
		 * @return 
		 */
		public static String getCnUczestnikUwaga(int i){
			if(i==0) return "przewodniczacy";
			else return "uczestnik_"+i+"_uwaga";
		}
		//end Komisja
	}
	static void sysout(Class<?> c){
		System.out.println("["+DateUtils.formatJsDateTimeWithSeconds(Calendar.getInstance().getTime())+"] "+
				c.getSimpleName()+"::"+Thread.currentThread().getStackTrace()[2].getMethodName());
	}
	
	@Override
	public void onLoadData(FieldsManager fm) throws EdmException {
		if (fm.getEnumItem(ProtokolBrakowaniaStale.CN_STATUS) != null) {
			Integer statusid = fm.getEnumItem(ProtokolBrakowaniaStale.CN_STATUS).getId();
			pl.compan.docusafe.core.dockinds.field.Field field = fm.getField(ProtokolBrakowaniaStale.CN_DICT_BRAKOWANIE_POZYCJA);
			String[] buttons = {};
			switch (statusid) {
			case 10:
				buttons = new String[]{"doAdd", "doRemove"};
				break;
			case 11:
			case 21:
			case 22:
			case 23:
				buttons = new String[]{};
				break;
			case 30:
				buttons = new String[]{"doRemove"};
				break;
			case 91:
			case 90:
				buttons = new String[]{};
				break;
			case 100:
				buttons = new String[]{};
				break;
			default:
				break;
			}
			((DictionaryField) field).setDicButtons(Lists.newArrayList(buttons));
		}
		super.onLoadData(fm);
	}
	private void fillUserDivisions(Map<String, Object> values) throws UserNotFoundException, EdmException
	{
		Long userDivision =  1l;
		DSUser user = DSApi.context().getDSUser();
		
	DSDivision[] divs = DSApi.context().getDSUser().getOriginalDivisionsWithoutGroup();

		for (int i = 0; i < divs.length; i++)
		{
			if (divs[i].isPosition())
			{
				
				DSDivision parent = divs[i].getParent();
				if (parent != null)
				{
					userDivision = parent.getId();
					break;
				} else
				{
					userDivision = divs[i].getId();
				}
			} else if (divs[i].isDivision())
			{
				userDivision = divs[i].getId();
				break;
			}
		}
		//values.put("a", divs);
	//	values.put("DOC_AUTORDIVISION", userDivision);
	}
	@Override
	public boolean canChangeDockind(Document document) throws EdmException {
		return false;
	}
}