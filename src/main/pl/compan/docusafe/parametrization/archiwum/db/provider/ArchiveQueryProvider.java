package pl.compan.docusafe.parametrization.archiwum.db.provider;

import pl.compan.docusafe.core.DSApi;

public class ArchiveQueryProvider {

	public static ArchiveQueryExecutor getInstance() {
		if (DSApi.isPostgresServer()) {
			return PostgresArchiwQueryExecutor.getInstance();
		} else {
			return ArchiveQueryExecutor.getInstance();
		}
	}

}
