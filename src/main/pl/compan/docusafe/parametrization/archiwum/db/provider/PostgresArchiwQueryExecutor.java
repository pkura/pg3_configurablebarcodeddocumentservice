package pl.compan.docusafe.parametrization.archiwum.db.provider;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.Container.ArchiveStatus;
import pl.compan.docusafe.parametrization.archiwum.NewCaseArchiverScheduled;

public class PostgresArchiwQueryExecutor extends ArchiveQueryExecutor {
	
	private static PostgresArchiwQueryExecutor postgresArchiwQueryExecutor;

	private PostgresArchiwQueryExecutor() {
	}
	
	public static ArchiveQueryExecutor getInstance(){
		if (postgresArchiwQueryExecutor == null) {
			postgresArchiwQueryExecutor = new PostgresArchiwQueryExecutor();
		}
		return postgresArchiwQueryExecutor;
	
	}
	
	@Override
	public void updateCaseBelongsToDeletedPortfolios() throws SQLException,
			EdmException {
		String sql = "update dso_container set ARCHIVESTATUS=2 where parent_id in(select id from dso_container where officeid not in("
			    + "select idteczki2 from  ds_ar_teczki_do_ap where  " 
			    + "CAST(id AS text) " 
			    + "in (select field_val from ds_ar_spis_dok_do_ap_multi)) and discriminator='FOLDER'  and (ARCHIVESTATUS=6 or ARCHIVESTATUS=7))"
	    		+ "and discriminator='CASE' and  (ARCHIVESTATUS=6 or ARCHIVESTATUS=7) ";
		PreparedStatement ps=DSApi.context().prepareStatement(sql);
		ps.executeUpdate();
	}
	
	@Override
	public void updateCaseBelongsToPortfoliosWhichBe() throws SQLException,
			EdmException {
		String sql =  "update dso_container set ARCHIVESTATUS=2 where officeid not in( "
					+ "select idteczki2 from  ds_ar_teczki_do_ap where "
					+ "CAST(id AS text) "
					+ "in (select field_val from ds_ar_spis_dok_do_ap_multi)) and discriminator='FOLDER'  and (ARCHIVESTATUS=6 or ARCHIVESTATUS=7) ";
		PreparedStatement ps = DSApi.context().prepareStatement(sql);
		ps.executeUpdate();
	}
	
	
	@Override
	public void updateResignationCaseBelongsToDeletedPortfolios()
			throws SQLException, EdmException {
		String sql = "update dso_container set ARCHIVESTATUS="+ArchiveStatus.Archived.ordinal()+" where parent_id in(select id from dso_container where officeid not in("
				+"select idteczki2 from  ds_ar_teczki_do_ap where "
				+"CAST(id AS text) "
				+"in (select field_val from ds_ar_spis_dok_do_ap_multi)) and discriminator='FOLDER'  and (ARCHIVESTATUS="+ArchiveStatus.ToTransferToNA.ordinal()+"  or ARCHIVESTATUS="+ArchiveStatus.TransferedNA.ordinal()+" )) "
				+"and discriminator='CASE' and  (ARCHIVESTATUS="+ArchiveStatus.ToTransferToNA.ordinal()+"  or ARCHIVESTATUS="+ArchiveStatus.TransferedNA.ordinal()+" ) ";

		PreparedStatement ps = DSApi.context().prepareStatement(sql);
		ps.executeUpdate();
	}
	
	@Override
	public void updateForCaseBelongsToPortfoliosWhichBe() throws SQLException, EdmException {
		String sql = "update dso_container set ARCHIVESTATUS="+ArchiveStatus.Archived.ordinal()+" where officeid not in( "
				+"select idteczki2 from  ds_ar_teczki_do_ap where "
				+"CAST(id AS text) "
				+"in (select field_val from ds_ar_spis_dok_do_ap_multi)) and discriminator='FOLDER'  and (ARCHIVESTATUS="+ArchiveStatus.ToTransferToNA.ordinal()+"  or ARCHIVESTATUS="+ArchiveStatus.TransferedNA.ordinal()+" ) ";

		PreparedStatement ps = DSApi.context().prepareStatement(sql);
		ps.executeUpdate();
	}
	
	@Override
	public void updateForComeBackPortfoliosStatus(Long documentId) throws SQLException, EdmException {
		String sql = "update dso_container set ARCHIVESTATUS="+ArchiveStatus.Archived.ordinal()+"  where discriminator='FOLDER' and OFFICEID in("
				+"select idteczki2 from  ds_ar_teczki_do_ap  where "
				+"CAST(id AS text) "
				+"in (select field_val from ds_ar_spis_dok_do_ap_multi where document_id="+documentId.toString() +"))";

		PreparedStatement ps = DSApi.context().prepareStatement(sql);
		ps.executeUpdate();
	}
	
	@Override
	public void updateForComeBackCaseStatus(Long documentId) throws SQLException, EdmException {
		String sql = "update dso_container set ARCHIVESTATUS="+ArchiveStatus.Archived.ordinal()+" where discriminator='CASE' and parent_id in( "
				+"select id from dso_container where officeid in("
				+"select idteczki2 from  ds_ar_teczki_do_ap where discriminator='FOLDER' and "
				+"CAST(id AS text) "
				+"in (select field_val from ds_ar_spis_dok_do_ap_multi where document_id="+documentId.toString() +")))";

		PreparedStatement ps = DSApi.context().prepareStatement(sql);
		ps.executeUpdate();
	}
	
	@Override
	public void deleteFromInventoryIfCheckIsDelete(Long documentId)throws SQLException, EdmException {
		PreparedStatement ps = DSApi.context().prepareStatement(" delete from ds_ar_spis_dok_do_ap_multi  where id in ( select multi.id from ds_ar_spis_dok_do_ap_multi multi "
				+ "left join ds_ar_teczki_do_ap teczki on multi.FIELD_VAL=CAST(teczki.ID AS text) where teczki.USUN='1' and DOCUMENT_ID="+documentId.toString()+" )");
		ps.executeUpdate();

		ps=DSApi.context().prepareStatement("delete from ds_ar_teczki_do_ap where CAST(id AS text) not in (select field_val from ds_ar_spis_dok_do_ap_multi) ");
		ps.executeUpdate();
	}
	
	@Override
	public void updateCaseFolder(Long documentId) throws SQLException, EdmException {
		PreparedStatement ps=DSApi.context().prepareStatement("update dso_container set archiveStatus="+ArchiveStatus.None.ordinal()+" where id in ( "
				+ "select folderid from ds_ar_inventory_items items "
				+ "left join ds_ar_inventory_items_multi multi on multi.field_val=CAST(items.id AS text) where multi.document_id=?)");
		ps.setLong(1, documentId);
		ps.executeUpdate();
	}
	
	@Override
	public void updateComeBackStatusCaseForPhysical() throws SQLException, EdmException {
		PreparedStatement ps = DSApi.context().prepareStatement(" update "+NewCaseArchiverScheduled.wykazItemsTableName+" set archive_Status="+ArchiveStatus.Archived.ordinal()
				+ " where  (archive_status=6 or archive_status=7) and (folder_officeid not in("
				+ "select ap.idteczki2 from ds_ar_spis_dok_do_ap_multi multi  "
				+ "left join ds_ar_teczki_do_ap ap on multi.field_val=CAST(ap.id AS text) ))");
		ps.executeUpdate();
	}
	
	@Override
	public ResultSet queryForDeletePortfolios(Document doc) throws SQLException, EdmException {
		 PreparedStatement ps = DSApi.context().prepareStatement("  select idteczki2,USUN,RWATYTUL from ds_ar_teczki_do_ap teczki "
					+ "left join ds_ar_spis_dok_do_ap_multi multi on multi.FIELD_VAL=CAST(teczki.ID AS text) "
					+ "left join ds_ar_spis_dok_do_ap spis on spis.document_id=multi.DOCUMENT_ID "
					+ "where spis.document_id="+doc.getId().toString());
		 
		 return ps.executeQuery();
	}
	
	
	@Override
	public ResultSet getPortfoliosWithMultipleInventory(Long documentId) throws SQLException, EdmException {
		PreparedStatement ps=DSApi.context().prepareStatement("select teczki.IDTECZKI2 as id from ds_ar_teczki_do_ap teczki "
				+ "left join ds_ar_spis_dok_do_ap_multi multi on multi.field_val=CAST(teczki.ID AS text) "
				+ "where multi.FIELD_CN='SPIS_AP_TECZKI' and multi.DOCUMENT_ID=?");
		ps.setLong(1, documentId);
		return ps.executeQuery();
	}
	
	@Override
	public void updateComeBackStatusCaseForPhysicalByArchiveStatus() throws SQLException, EdmException {
		PreparedStatement ps = DSApi.context().prepareStatement(" update "+NewCaseArchiverScheduled.wykazItemsTableName+" set archive_Status="+ArchiveStatus.Archived.ordinal()
				+ " where  (ARCHIVE_STATUS="+ArchiveStatus.ToTransferToNA.ordinal()+"  or ARCHIVE_STATUS="+ArchiveStatus.TransferedNA.ordinal()+") and (folder_officeid not in( "
				+ "select ap.idteczki2 from ds_ar_spis_dok_do_ap_multi multi  "
				+ "left join ds_ar_teczki_do_ap ap on multi.field_val=CAST(ap.id AS text )))");
		ps.executeUpdate();	
	}
	
	@Override
	public void updateComeBackStatusCaseForPhysicalBeingOnDictionary(Long documentId) throws SQLException, EdmException {
		PreparedStatement ps = DSApi.context().prepareStatement(" update "+NewCaseArchiverScheduled.wykazItemsTableName+" set archive_Status="+ArchiveStatus.Archived.ordinal()
				+ " where  (ARCHIVE_STATUS="+ArchiveStatus.ToTransferToNA.ordinal()+"  or ARCHIVE_STATUS="+ArchiveStatus.TransferedNA.ordinal()+")and folderid is null and (folder_officeid in( "
				+ "select ap.idteczki2 from ds_ar_spis_dok_do_ap_multi multi  "
				+ "left join ds_ar_teczki_do_ap ap on multi.field_val=CAST(ap.id AS text ) where multi.document_id="+documentId.toString()+"))");
		ps.executeUpdate();
	}
}
