package pl.compan.docusafe.parametrization.archiwum.db.provider;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.office.Container.ArchiveStatus;
import pl.compan.docusafe.parametrization.archiwum.NewCaseArchiverScheduled;
import pl.compan.docusafe.util.DateUtils;

public class ArchiveQueryExecutor {

	private static ArchiveQueryExecutor archiveQueryExecutor;

	protected ArchiveQueryExecutor() {
	}
	
	public static ArchiveQueryExecutor getInstance(){
		if (archiveQueryExecutor == null) {
			archiveQueryExecutor = new ArchiveQueryExecutor();
		}
		return archiveQueryExecutor;	
	}
	
	//powrot statusow dla spraw znajdujacych sie w teczkach usunietych
	public void updateCaseBelongsToDeletedPortfolios() throws SQLException, EdmException {
		String sql = "update dso_container set ARCHIVESTATUS=2 where parent_id in(select id from dso_container where officeid not in("
				    + "select idteczki2 from  ds_ar_teczki_do_ap where  " 
				    + "id " 
				    + "in (select field_val from ds_ar_spis_dok_do_ap_multi)) and discriminator='FOLDER'  and (ARCHIVESTATUS=6 or ARCHIVESTATUS=7))"
		    		+ "and discriminator='CASE' and  (ARCHIVESTATUS=6 or ARCHIVESTATUS=7) ";
		PreparedStatement ps=DSApi.context().prepareStatement(sql);
		ps.executeUpdate();
	}

	//powrot statusow dla teczek ktore byly dodane do spisu ale zostaly usuniete
	public void updateCaseBelongsToPortfoliosWhichBe() throws SQLException, EdmException {
		String sql =  "update dso_container set ARCHIVESTATUS=2 where officeid not in( "
					+ "select idteczki2 from  ds_ar_teczki_do_ap where "
					+ "id "
					+ "in (select field_val from ds_ar_spis_dok_do_ap_multi)) and discriminator='FOLDER'  and (ARCHIVESTATUS=6 or ARCHIVESTATUS=7) ";
		PreparedStatement ps = DSApi.context().prepareStatement(sql);
		ps.executeUpdate();
	}

	//powrot statusow dla fizycznych
	public void updateComeBackStatusCaseForPhysical() throws SQLException, EdmException {
		PreparedStatement ps = DSApi.context().prepareStatement(" update "+NewCaseArchiverScheduled.wykazItemsTableName+" set archive_Status="+ArchiveStatus.Archived.ordinal()
				+ " where  (archive_status=6 or archive_status=7) and (folder_officeid not in( "
				+ "select ap.idteczki2 from ds_ar_spis_dok_do_ap_multi multi  "
				+ "left join ds_ar_teczki_do_ap ap on multi.field_val=ap.id))");
		ps.executeUpdate();
	}

	public ResultSet queryForDeletePortfolios(Document doc) throws SQLException, EdmException {
		 PreparedStatement ps = DSApi.context().prepareStatement("  select idteczki2,USUN,RWATYTUL from ds_ar_teczki_do_ap teczki "
					+ "left join ds_ar_spis_dok_do_ap_multi multi on multi.FIELD_VAL=teczki.ID "
					+ "left join ds_ar_spis_dok_do_ap spis on spis.document_id=multi.DOCUMENT_ID "
					+ "where spis.document_id="+doc.getId().toString());
		 
		 return ps.executeQuery();
	}

	public void updateStatusCaseForSpis(ResultSet rs) throws SQLException, EdmException {
		PreparedStatement ps = DSApi.context().prepareStatement(" update ds_ar_inventory_items  set archive_status="+ArchiveStatus.TransferedNA.ordinal()+" where folder_officeid='"+rs.getString("idteczki2")+"'");
		ps.executeUpdate();
	}

	public void updateResignationCaseBelongsToDeletedPortfolios() throws SQLException, EdmException {
		String sql = "update dso_container set ARCHIVESTATUS="+ArchiveStatus.Archived.ordinal()+" where parent_id in(select id from dso_container where officeid not in("
					+"select idteczki2 from  ds_ar_teczki_do_ap where "
					+"id "
					+"in (select field_val from ds_ar_spis_dok_do_ap_multi)) and discriminator='FOLDER'  and (ARCHIVESTATUS="+ArchiveStatus.ToTransferToNA.ordinal()+"  or ARCHIVESTATUS="+ArchiveStatus.TransferedNA.ordinal()+" )) "
					+"and discriminator='CASE' and  (ARCHIVESTATUS="+ArchiveStatus.ToTransferToNA.ordinal()+"  or ARCHIVESTATUS="+ArchiveStatus.TransferedNA.ordinal()+" ) ";
		
		PreparedStatement ps = DSApi.context().prepareStatement(sql);
		ps.executeUpdate();
	}

	public void updateForCaseBelongsToPortfoliosWhichBe() throws SQLException, EdmException {
		String sql = "update dso_container set ARCHIVESTATUS="+ArchiveStatus.Archived.ordinal()+" where officeid not in( "
					+"select idteczki2 from  ds_ar_teczki_do_ap where "
					+"id "
					+"in (select field_val from ds_ar_spis_dok_do_ap_multi)) and discriminator='FOLDER'  and (ARCHIVESTATUS="+ArchiveStatus.ToTransferToNA.ordinal()+"  or ARCHIVESTATUS="+ArchiveStatus.TransferedNA.ordinal()+" ) ";

		PreparedStatement ps = DSApi.context().prepareStatement(sql);
		ps.executeUpdate();
		
	}

	public void updateForComeBackPortfoliosStatus(Long documentId) throws SQLException, EdmException {
		String sql = "update dso_container set ARCHIVESTATUS="+ArchiveStatus.Archived.ordinal()+"  where discriminator='FOLDER' and OFFICEID in("
					+"select idteczki2 from  ds_ar_teczki_do_ap  where "
					+"id "
					+"in (select field_val from ds_ar_spis_dok_do_ap_multi where document_id="+documentId.toString() +"))";

		PreparedStatement ps = DSApi.context().prepareStatement(sql);
		ps.executeUpdate();
	}

	public void updateForComeBackCaseStatus(Long documentId) throws SQLException, EdmException {
		String sql = "update dso_container set ARCHIVESTATUS="+ArchiveStatus.Archived.ordinal()+" where discriminator='CASE' and parent_id in( "
					+"select id from dso_container where officeid in("
					+"select idteczki2 from  ds_ar_teczki_do_ap where discriminator='FOLDER' and "
					+"id "
					+"in (select field_val from ds_ar_spis_dok_do_ap_multi where document_id="+documentId.toString() +")))";

		PreparedStatement ps = DSApi.context().prepareStatement(sql);
		ps.executeUpdate();
		
	}

	public void updateInventoryById(Long id) throws SQLException, EdmException {
		String updateFizyczne="update ds_ar_inventory_items set archive_status="+ArchiveStatus.ToTransferToNA.ordinal()+" where id="+id;
		PreparedStatement ps=DSApi.context().prepareStatement(updateFizyczne);
		ps.executeUpdate();
	}

	//potem usuniecie pozycji ze spisu, jesli ma zaznaczone usun
	public void deleteFromInventoryIfCheckIsDelete(Long documentId) throws SQLException, EdmException {
		PreparedStatement ps = DSApi.context().prepareStatement(" delete from ds_ar_spis_dok_do_ap_multi  where id in ( select multi.id from ds_ar_spis_dok_do_ap_multi multi "
				+ "left join ds_ar_teczki_do_ap teczki on multi.FIELD_VAL=teczki.ID where teczki.USUN='1' and DOCUMENT_ID="+documentId.toString()+" )");
		ps.executeUpdate();

		ps=DSApi.context().prepareStatement("delete from ds_ar_teczki_do_ap where id not in (select field_val from ds_ar_spis_dok_do_ap_multi) ");
		ps.executeUpdate();
	}

	public void updateCaseFolder(Long documentId) throws SQLException, EdmException {
		PreparedStatement ps=DSApi.context().prepareStatement("update dso_container set archiveStatus="+ArchiveStatus.None.ordinal()+" where id in ( "
				+ "select folderid from ds_ar_inventory_items items "
				+ "left join ds_ar_inventory_items_multi multi on multi.field_val=items.id where multi.document_id=?)");
		ps.setLong(1, documentId);
		ps.executeUpdate();
	}

	public void updateArchiveInventory(Long documentId) throws SQLException, EdmException {
		SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");
		PreparedStatement ps = DSApi.context().prepareStatement(" update ds_ar_spis_dok_do_ap set data_przek=?  where DOCUMENT_ID=? ");
		ps.setDate(1,java.sql.Date.valueOf(df.format(DateUtils.getCurrentTime())));
		ps.setLong(2, documentId);
		ps.executeUpdate();
	}

	public ResultSet getPortfoliosWithMultipleInventory(Long documentId) throws SQLException, EdmException {
		PreparedStatement ps=DSApi.context().prepareStatement("select teczki.IDTECZKI2 as id from ds_ar_teczki_do_ap teczki "
				+ "left join ds_ar_spis_dok_do_ap_multi multi on multi.field_val=teczki.ID "
				+ "where multi.FIELD_CN='SPIS_AP_TECZKI' and multi.DOCUMENT_ID=?");
		ps.setLong(1, documentId);
		return ps.executeQuery();
	}

	public void updateComeBackStatusCaseForPhysicalByArchiveStatus() throws SQLException, EdmException{
		PreparedStatement ps = DSApi.context().prepareStatement(" update "+NewCaseArchiverScheduled.wykazItemsTableName+" set archive_Status="+ArchiveStatus.Archived.ordinal()
				+ " where  (ARCHIVE_STATUS="+ArchiveStatus.ToTransferToNA.ordinal()+"  or ARCHIVE_STATUS="+ArchiveStatus.TransferedNA.ordinal()+") and (folder_officeid not in( "
				+ "select ap.idteczki2 from ds_ar_spis_dok_do_ap_multi multi  "
				+ "left join ds_ar_teczki_do_ap ap on multi.field_val=ap.id))");
		ps.executeUpdate();	
	}

	public void updateComeBackStatusCaseForPhysicalBeingOnDictionary(Long documentId) throws SQLException, EdmException {
		PreparedStatement ps = DSApi.context().prepareStatement(" update "+NewCaseArchiverScheduled.wykazItemsTableName+" set archive_Status="+ArchiveStatus.Archived.ordinal()
				+ " where  (ARCHIVE_STATUS="+ArchiveStatus.ToTransferToNA.ordinal()+"  or ARCHIVE_STATUS="+ArchiveStatus.TransferedNA.ordinal()+")and folderid is null and (folder_officeid in( "
				+ "select ap.idteczki2 from ds_ar_spis_dok_do_ap_multi multi  "
				+ "left join ds_ar_teczki_do_ap ap on multi.field_val=ap.id where multi.document_id="+documentId.toString()+"))");
		ps.executeUpdate();
	}



}
