package pl.compan.docusafe.parametrization.archiwum.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class FieldNotNullDecisionHandler implements DecisionHandler {
    private final static Logger LOG = LoggerFactory.getLogger(FieldNotNullDecisionHandler.class);

    private String field;

    public String decide(OpenExecution openExecution) {
        try
        {
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            LOG.info("documentid = " + docId + " field = " + field);
            Object val = fm.getKey(field);
            if (val == null)
          	  return "null";
            else 
          	  return "notnull";
            
        } catch (EdmException ex) {
            LOG.error(ex.getMessage(), ex);
            return "error";
        }
    }
}

