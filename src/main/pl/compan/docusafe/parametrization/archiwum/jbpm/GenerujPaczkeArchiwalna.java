package pl.compan.docusafe.parametrization.archiwum.jbpm;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.parametrization.archiwum.ArchivePackageManager;
import pl.compan.docusafe.parametrization.archiwum.db.provider.ArchiveQueryExecutor;
import pl.compan.docusafe.parametrization.archiwum.db.provider.ArchiveQueryProvider;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.ServletUtils;

import com.opensymphony.webwork.ServletActionContext;

public class GenerujPaczkeArchiwalna implements ExternalActivityBehaviour{

	private ArchiveQueryExecutor archiveQueryExecutor = ArchiveQueryProvider.getInstance();
	
	@Override
	public void execute(ActivityExecution ex) throws Exception {


		boolean contextOpened = false;
		Long documentId = Jbpm4Utils.getDocumentId(ex);
		Document doc=Document.find(documentId);
		
		OfficeCase oc = null;
		contextOpened = DSApi.openContextIfNeeded();
		ArchivePackageManager man = new ArchivePackageManager();
		List<OfficeCase> listaSpraw = new ArrayList<OfficeCase>();
		
		ResultSet rs = archiveQueryExecutor.getPortfoliosWithMultipleInventory(documentId);
		while(rs.next()){
			if(rs.getString("id")!=null&&!rs.getString("id").equals("")){
				OfficeFolder of=OfficeFolder.findByOfficeId(rs.getString("id"));
				for (Iterator<? extends Container> iter = of.getChildren().iterator(); iter.hasNext();)
				{
					Container c = (Container) iter.next();
					if (c instanceof OfficeCase)
						listaSpraw.add((OfficeCase) c);
				}
			}
		}
		
		
		if(listaSpraw!=null){
			File archivePackage = man.preparePackage(listaSpraw);
			if(archivePackage!=null){
				String now = DateUtils.formatFolderDate(new Date());
				ServletUtils.streamFile(ServletActionContext.getResponse(), archivePackage , "application/zip",
						"Content-Disposition: attachment; filename=\"paczka_archiwalna_" + now +".zip\"");
			}
		}
		DSApi.closeContextIfNeeded(contextOpened);

	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
		// TODO Auto-generated method stub

	}

}
