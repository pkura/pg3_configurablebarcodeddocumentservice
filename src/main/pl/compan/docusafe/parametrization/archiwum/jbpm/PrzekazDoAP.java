package pl.compan.docusafe.parametrization.archiwum.jbpm;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import oracle.sql.DATE;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.Container.ArchiveStatus;
import pl.compan.docusafe.parametrization.archiwum.NewCaseArchiverScheduled;
import pl.compan.docusafe.parametrization.archiwum.SpisDokDoAPLogic;
import pl.compan.docusafe.parametrization.archiwum.db.provider.ArchiveQueryExecutor;
import pl.compan.docusafe.parametrization.archiwum.db.provider.ArchiveQueryProvider;
import pl.compan.docusafe.parametrization.archiwum.db.provider.PostgresArchiwQueryExecutor;
import pl.compan.docusafe.util.DateUtils;

//klasa odpowiedzialna za usuwanie dokumentow z systemu, jesli zosta� zaznaczony odpowiedni checkbox na spisie
//oraz odnotowanie dat na spisach zdawczo odbiorczych
public class PrzekazDoAP implements ExternalActivityBehaviour{

	private ArchiveQueryExecutor archiveQueryExecutor = ArchiveQueryProvider.getInstance();
	
	@Override
	public void execute(ActivityExecution activity) throws Exception {
		boolean ctx=DSApi.openContextIfNeeded(); 
		Long documentId = Jbpm4Utils.getDocumentId(activity);
		Document doc=Document.find(documentId);
		
		//powrot statusow dla spraw znajdujacych sie w teczkach usunietych
		archiveQueryExecutor.updateCaseBelongsToDeletedPortfolios();
		
		//powrot statusow dla teczek ktore byly dodane do spisu ale zostaly usuniete
		archiveQueryExecutor.updateCaseBelongsToPortfoliosWhichBe();

		//powrot statusow dla fizycznych
		archiveQueryExecutor.updateComeBackStatusCaseForPhysical();

		//usuniecie z bazy wpisow narazie dotyczacych tylko pozycji ze slownika
		
		//usunac teczki wybrane najpierw
 		ResultSet rs = archiveQueryExecutor.queryForDeletePortfolios(doc);

		while(rs.next()){
			if(!rs.getString("RWATYTUL").equals(SpisDokDoAPLogic.RWA_DLA_POZYCJI_FIZYCZNEJ)){
				ustawieniePolPrzyPrzekazywaniu(rs.getString("idteczki2"), rs.getBoolean("USUN"));
			}
			else{
				archiveQueryExecutor.updateStatusCaseForSpis(rs);
			}
		}
		
		//potem usuniecie pozycji ze spisu, jesli ma zaznaczone usun
		archiveQueryExecutor.deleteFromInventoryIfCheckIsDelete(documentId);
	
		archiveQueryExecutor.updateArchiveInventory(documentId);

		//odebranie uprawnien do edycji

		DSApi.context().revoke(doc, new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk�y - modyfikacja"));
		DSApi.context().revoke(doc, new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk�y - usuwanie"));
		DSApi.context().revoke(doc, new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - modyfikacja"));


		DSApi.closeContextIfNeeded(ctx);

	}


	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
		// TODO Auto-generated method stub

	}
	
	
	public void ustawieniePolPrzyPrzekazywaniu(String officeId,boolean czyZatrzec) throws Exception{
		//mozliwe zacieranie gdy flaga dla danej pozycji na spisie bedzie ustawiona na true
		
		
		OfficeFolder of = OfficeFolder.findByOfficeId(officeId);
		of.setArchiveStatus(ArchiveStatus.TransferedNA);
		if(czyZatrzec)of.setName("pozycja przekazana do AP");

		for(Container con:of.getChildren()){
			if(con instanceof OfficeCase){
				OfficeCase oc =(OfficeCase)con;
				oc.setArchiveStatus(ArchiveStatus.TransferedNA);
				if(czyZatrzec){
					oc.setDescription("pozycja przekazana do AP");
					oc.setTitle("pozycja przekazana do AP");
					List<OfficeDocument> dokumenty=oc.getDocuments();
					for(OfficeDocument dok:dokumenty){
						dok.setTitle("pozycja przekazana do AP");
						dok.setDescription("pozycja przekazana do AP");
						//dok.setArchiveStatus(ArchiveStatus.Shredded.ordinal());
						for(Attachment a:dok.getAttachments())
							a.delete();

					}
				}
			}
		}
	}

}
