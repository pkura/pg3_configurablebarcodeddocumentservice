package pl.compan.docusafe.parametrization.archiwum.jbpm;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.DSUserEnumField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 25.10.13
 * Time: 09:17
 * To change this template use File | Settings | File Templates.
 */
public class ReadOnlyAssigner implements ExternalActivityBehaviour  {
    private String user;
    private String guid;
    private String field;
    private String fieldCnSourceType;
    private Boolean mustContainsUser;
    private String guidFromAdditionProperty;
    private Boolean everyPreviousUserWithoutAuthor;

    private static final Logger log = LoggerFactory.getLogger(ReadOnlyAssigner.class);

    @Override
    public void execute(ActivityExecution execution) throws Exception {
        long documentId = (Long) execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY);
        OfficeDocument doc = OfficeDocument.find(documentId);

        if (StringUtils.isNotBlank(guid))
        {
            LinkedList<String> guidAsList = new LinkedList<String>();
            guidAsList.add(guid);
            doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(
                    Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(
                    doc, Collections.<String, Object> singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNMENT_GUIDS, guidAsList));
        }
        else if (user!=null)
        {
            if (user.equals("author"))
            {
                doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(
                        Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(
                        doc, Collections.<String, Object> singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY,
                        doc.getAuthor()));
            }
            else
            {
                doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(
                        Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(
                        doc, Collections.<String, Object> singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY, user));
            }
        } else if (guidFromAdditionProperty != null) {
			String guids;
			if ((guids = Docusafe.getAdditionProperty(guidFromAdditionProperty)) != null) {
				LinkedList<String> guidAsList = new LinkedList<String>();
				for (String g : guids.split(","))
				{
					guidAsList.add(g);
                    doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(
                            Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(
                            doc, Collections.<String, Object> singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNMENT_GUIDS, guidAsList));
				}
			}
		} else if (field != null) {
            FieldsManager fm = doc.getFieldsManager();
            Field userOrDivisionField = fm.getField(field);
            if (userOrDivisionField instanceof DSUserEnumField)
            {
                String userName = null;
                if (userOrDivisionField.isMultiple())
                {
                    List<Long> userIds = (List<Long>)fm.getKey(field);
                    for (Long userId : userIds)
                    {
                        userName = DSUser.findById(userId).getName();
                        doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(
                                Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(
                                doc, Collections.<String, Object>singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY,
                                userName));
                    }
                }
                else
                {
                    userName = fm.getEnumItemCn(field);
                    //jesli jest null to niech lepiej idzie do autora niz w kosmos
                    if(!StringUtils.isEmpty(userName))
                        doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(
                                Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(
                                doc, Collections.<String, Object>singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY,
                                userName));
                }
            }
            else
            {
                if (fieldCnSourceType != null && "username".equals(fieldCnSourceType)) {
                    try {
                        String username = fm.getEnumItemCn(field);
                        doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(
                                Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(
                                doc, Collections.<String, Object>singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY,
                                username));
                    } catch (EdmException e) {
                        log.error("cant find any user to assignee for user field: " + field + ", go to admin");
                    }
                } else { //fieldCnSourceType="guid"
                    if (mustContainsUser != null && mustContainsUser) {
                        String guid = fm.getEnumItemCn(field);
                        DSDivision div = DSDivision.find(guid);
                        try {
                            while (div.getOriginalUsers().length == 0) {
                                div = div.getParent();
                            }

                            LinkedList<String> guidAsList = new LinkedList<String>();
                            guidAsList.add(div.getGuid());
                            doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(
                                    Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(
                                    doc, Collections.<String, Object> singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNMENT_GUIDS, guidAsList));

                        } catch (EdmException e) {
                            log.error("cant find any user to assignee for division field: " + field + ", go to admin");
                        }
                    } else {
                        String guid = fm.getEnumItemCn(field);
                        LinkedList<String> guidAsList = new LinkedList<String>();
                        guidAsList.add(guid);
                        doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(
                                Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(
                                doc, Collections.<String, Object> singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNMENT_GUIDS, guidAsList));

                    }
                }
            }
        } else if (everyPreviousUserWithoutAuthor != null && everyPreviousUserWithoutAuthor){
            String principalName = DSApi.context().getPrincipalName();
            String author = doc.getAuthor();
            Object[] tab = doc.getAssignmentHistory().toArray();
            Set<String> setOfUsername = new LinkedHashSet<String>();
            for (Object o : tab){
                AssignmentHistoryEntry entry = (AssignmentHistoryEntry) o;
                if (!entry.getSourceUser().equalsIgnoreCase(author)
                    && !entry.getSourceUser().equalsIgnoreCase(principalName)
                    && !entry.getSourceUser().equalsIgnoreCase("admin")
                    && !setOfUsername.contains(entry.getSourceUser())){

                    setOfUsername.add(entry.getSourceUser());
                }
            }
            for (String username : setOfUsername){
                try {
                    doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess(
                            Jbpm4Constants.READ_ONLY_PROCESS_NAME).getLogic().startProcess(
                            doc, Collections.<String, Object> singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY,
                            username));
                }catch (EdmException e){
                    log.error("B��d podczas wysy�ania dokumentu READ-ONLY: "+e);
                }
            }
        }
    }



	 public void signal(ActivityExecution activityExecution, String s, Map<String, ?> stringMap) throws Exception {
	    }
}
