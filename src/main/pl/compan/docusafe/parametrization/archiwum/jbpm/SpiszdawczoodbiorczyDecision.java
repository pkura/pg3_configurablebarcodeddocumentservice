package pl.compan.docusafe.parametrization.archiwum.jbpm;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SpiszdawczoodbiorczyDecision implements DecisionHandler
{
	private static final String TO_USER = "to user";
	private static final String TO_GUID = "to guid";
	private static final String ERROR_REPAIR= "error-repair";
	
	private static final String GET_GRUPA_SQL = 
		"select GUID from DS_DIVISION where PARENT_ID = ? and name = ?";
	
	private static final String GET_GRUPA_PROFILE_SQL =  "DECLARE @role_id int = ?, @div_id int = ?; select u.NAME from DS_DIVISION d join DS_USER_TO_DIVISION ud on d.ID=ud.DIVISION_ID join DS_USER u on ud.USER_ID=u.ID join dso_role_usernames rd on u.NAME=rd.username join ds_profile_to_role pr on pr.role_id=rd.role_id join ds_profile_to_user pu on pr.profile_id=pu.profile_id and pu.user_id=u.id and (pu.USERKEY is null or pu.USERKEY like '' or pu.USERKEY like '#' or pu.USERKEY like d.GUID) where rd.role_id=@role_id and d.ID=@div_id union select u.NAME from DS_DIVISION d join DS_USER_TO_DIVISION ud on d.ID=ud.DIVISION_ID join DS_USER u on ud.USER_ID=u.ID join dso_role_usernames rd on u.NAME=rd.username and rd.source ='own' where rd.role_id=@role_id and d.ID=@div_id;";
	
	private static final boolean __debug = true;
	
	private final static Logger log = LoggerFactory.getLogger(SpiszdawczoodbiorczyDecision.class);
	
	public String decide(OpenExecution execution)
	{
		Document document= null;
		String dzialGuid= "";
		String osobaUprawniona= null;
		String nazwaGrupy= null;
		String uprawnieni= null;
		Long idProfilu = null;
		try
		{
			document = Jbpm4Utils.getDocument(execution);
			FieldsManager fm = document.getFieldsManager();
			osobaUprawniona = fm.getEnumItemCn("DSUSER");
			
			
			if( !StringUtils.isEmpty(osobaUprawniona))
			{
				if(__debug) System.out.println("SpiszdawczoodbiorczyDecision -> to_user");
				// przypadek utworzenia przez kierownika, wybrany jest user na ktorego idzie dekretacja
				return TO_USER;
			}else
			{
				if(__debug) System.out.println("SpiszdawczoodbiorczyDecision -> to_guid ");
				// przypadek automatu, dekretujemy na grupe
				nazwaGrupy= Docusafe.getAdditionProperty("archiwum.jbpm.spiszdawczoodbiorczy.grupa_uprawnionych");
				idProfilu = (Docusafe.getAdditionProperty("archiwum.jbpm.profil.spiszdawczoodbiorczy.grupa_uprawnionych") != null ? Long.parseLong(Docusafe.getAdditionProperty("archiwum.jbpm.profil.spiszdawczoodbiorczy.grupa_uprawnionych")) : null);
				dzialGuid = fm.getEnumItemCn("DSDIVISION");
				Long dzialId= DSDivision.find(dzialGuid).getId();
				
				if(idProfilu == null)
				{
					uprawnieni= getGrupaUprawnionych(dzialId.intValue(), nazwaGrupy);
					execution.setVariable("DIVISION_GUID", uprawnieni);
					if(__debug) System.out.println("SpiszdawczoodbiorczyDecision grupa_uprawnionych= "+nazwaGrupy+", dzialGuid= "+dzialGuid+", result: uprawnieni= "+uprawnieni);
					
					return TO_GUID;
				}
				else
				{
					osobaUprawniona= getOsobaUprawnionychFromProfile(dzialId.intValue(), idProfilu);
					return TO_USER;
				}
				
			}
			
		}
		catch(Exception e){
			String info= "SpiszdawczoodbiorczyDecision error: "+e.getMessage();
			info+= " docid= "+document.getId();
			info+= " osobaUprawniona= "+osobaUprawniona;
			if( StringUtils.isEmpty(osobaUprawniona)){
				info+= " grupa_uprawnionych= "+nazwaGrupy;
				info+= " dzialGuid= "+dzialGuid;
				info+= " uprawnieni= "+uprawnieni;
			}
				
			if(__debug) System.out.println(info);
			log.error(info, e);
			execution.setVariable("ERROR_DESCRIPTION", info );
			
			return ERROR_REPAIR;
		}
	}
	
	
	private String getOsobaUprawnionychFromProfile(int intValue, Long idProfilu)throws Exception {
		
		PreparedStatement ps = DSApi.context().prepareStatement(GET_GRUPA_PROFILE_SQL);
		ps.setLong(1, idProfilu);
		ps.setInt(2, intValue);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			return rs.getString(1);
		}
		throw new EdmException("Nie odnaleziono użytkownika w dziale");
	}


	public static String getGrupaUprawnionych(int dzial, String nazwaGrupy) throws Exception
	{
		PreparedStatement ps = DSApi.context().prepareStatement(GET_GRUPA_SQL);
		ps.setInt(1, dzial);
		ps.setString(2, nazwaGrupy);
		
		ResultSet rs = ps.executeQuery();
		rs.next();
		String guid = rs.getString(1);
		ps.close();
		rs.close();
		return guid;
	}

}
