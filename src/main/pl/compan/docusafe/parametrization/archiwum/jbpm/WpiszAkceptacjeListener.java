package pl.compan.docusafe.parametrization.archiwum.jbpm;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class WpiszAkceptacjeListener extends AbstractEventListener{
    private final static Logger log = LoggerFactory.getLogger(WpiszAkceptacjeListener.class);

    /** pole w kt�re nale�y wpisa� aktualn� dat� */		protected String poleDaty;
    /** pole w kt�re nale�y wpisa� pozycj� z enum */	protected String poleEnum;
    /** warto�� enum do wpisania */						protected String wartoscEnum;

    public void notify(EventListenerExecution eventListenerExecution) throws Exception {
        if (poleDaty==null || poleEnum==null || wartoscEnum==null)
            throw new EdmException("definicja procesu nie zawiera kompletnego wskazania, kt�r� akceptacj� uzupe�ni�");

        boolean hasOpen = true;
        if (!DSApi.isContextOpen()) {
            hasOpen = false;
            DSApi.openAdmin();
        }
        Long docId = Long.valueOf(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = OfficeDocument.find(docId);

        Date data = new Date();
        Integer enumI = null;
        try{
            enumI = Integer.parseInt(wartoscEnum);
        }catch(Exception e){
            throw new EdmException("definicja procesu zawiera niew�a�ciwe wskazanie, jak ma si� zmieni� status akceptacji");
        }
        if (enumI==null)
            throw new EdmException("definicja procesu zawiera niew�a�ciwe wskazanie, jak ma si� zmieni� status akceptacji");

        Map<String, Object> mapa = new HashMap<String, Object>();
        mapa.put(poleDaty, data);
        mapa.put(poleEnum, enumI);
        doc.getDocumentKind().setOnly(docId, mapa);

        if (DSApi.isContextOpen() && !hasOpen)
            DSApi.close();
    }
}
