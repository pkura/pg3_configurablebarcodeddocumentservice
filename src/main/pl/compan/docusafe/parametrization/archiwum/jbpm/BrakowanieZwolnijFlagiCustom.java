package pl.compan.docusafe.parametrization.archiwum.jbpm;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.Container.ArchiveStatus;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.parametrization.archiwum.NewCaseArchiverScheduled;
import pl.compan.docusafe.parametrization.archiwum.ProtokolBrakowaniaLogic.ProtokolBrakowaniaStale;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class BrakowanieZwolnijFlagiCustom implements ExternalActivityBehaviour {
	private static final long serialVersionUID = 1L;
	protected static Logger log = LoggerFactory.getLogger(BrakowanieZwolnijFlagiCustom.class);
	
	@Override
	public void execute(ActivityExecution activityExecution) throws Exception {
		Long docid = Long.valueOf(activityExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY)+"");
//		OfficeDocument doc = OfficeDocument.find(docid);
		
		String sql = "select * from ds_ar_brakowanie_multiple bm ";
				 if(DSApi.isPostgresServer())
					 sql+= "left join ds_ar_brakowanie_pozycja bp on bm.FIELD_VAL ::numeric = bp.id "
								+ "where bm.DOCUMENT_ID = " + docid;
				else
					sql+= "left join ds_ar_brakowanie_pozycja bp on bm.FIELD_VAL  = bp.id "
							+ "where bm.DOCUMENT_ID = " + docid;
			
		PreparedStatement ps = DSApi.context().prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		
		while (rs.next()) {
			OfficeFolder folder = OfficeFolder.find(rs.getLong("folder_officeid"));
			folder.setArchiveStatus(ArchiveStatus.Archived);
		}
		
		ps.close();
	}

	/**
	 * !!! TODO !!!
	 * Funkcja oznacza w archiwum pozycj� jako usuniet�. 
	 * Przechodzi przez teczk�, sprawy w teczce, dokumenty w sprawach i trwale wymazuje informacje o nich.
	 * 
	 * @param id_pozycji_protokolu_brakwoania
	 */
	private void usun(int id_pozycji_protokolu_brakwoania) {
		// TODO Auto-generated method stub
	}
	
	/**
	 * Brakujemy teczki.
	 * W teczce ustawiany jest archivestatus na 4 (wybrakowany).
	 * Data wybrakowania na pozycji spisu zdawczo obiorczego
	 * Flaga czy_wybrakowany na pozycji protoko�u.
	 * 
	 * @param id_pozycji_protokolu_brakwoania
	 * @param officeFolerId
	 */
	private boolean wybrakuj(int id_pozycji_protokolu_brakwoania, String officeId) {
		try {
//			ustawienie dla pozycji spisu zdawczo odbiorczego daty wybrakowania
			String sql = "update "+NewCaseArchiverScheduled.wykazItemsTableName+" set DATE_SHRED = GETDATE() where FOLDER_ID = " + officeId;
			PreparedStatement ps = DSApi.context().prepareStatement(sql);
			ps.execute();
			ps.close();
			
//			ustawienie dla pozycji protokolu brakowania flagi wybrakowany
			sql = "update ds_ar_brakowanie_pozycja set czy_wybrakowany = 1 where id = " + id_pozycji_protokolu_brakwoania;
			ps = DSApi.context().prepareStatement(sql);
			ps.execute();
			ps.close();
			
//			ustawienie dla teczki pola ArchiveStatus na 4 (Shredded/Wybrakowany)
			OfficeFolder of = OfficeFolder.find(Long.valueOf(officeId));
			of.setArchiveStatus(ArchiveStatus.Shredded);
		} catch (Exception e) {
			log.error("[Brakowanie]", e);
			return false;
		}
		return true;
	}

	@Override
	public void signal(ActivityExecution activityExecution, String string, Map<String, ?> map)
			throws Exception {
		// TODO Auto-generated method stub
	}
}
