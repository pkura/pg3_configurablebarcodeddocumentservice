package pl.compan.docusafe.parametrization.archiwum.jbpm;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.Field.Type;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.Container.ArchiveStatus;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class NadanieNumeruSpisZdawOdb implements ExternalActivityBehaviour {
	
	 protected static Logger log = LoggerFactory.getLogger(NadanieNumeruSpisZdawOdb.class);
	@Override
	public void execute(ActivityExecution activity) throws Exception {
		boolean ctx=DSApi.openContextIfNeeded(); 
		Long documentId = Jbpm4Utils.getDocumentId(activity);

		try {
			PreparedStatement ps;
				ps=DSApi.context().prepareStatement("select max(number_itr)as maxnumer from DS_AR_INVENTORY_TRANSF_AND_REC");

				ResultSet rs=ps.executeQuery();
				if(rs.next()){
					ps=DSApi.context().prepareStatement("update DS_AR_INVENTORY_TRANSF_AND_REC set number_itr="+(rs.getInt("maxnumer")/*+1*/)+" where document_id="+documentId);

				}
				else{
					ps=DSApi.context().prepareStatement("update DS_AR_INVENTORY_TRANSF_AND_REC set number_itr=1 where document_id="+documentId);
				}
				ps.executeUpdate();

				DSApi.closeContextIfNeeded(ctx);
			

		} catch (SQLException e) {
			log.debug("",e);
			e.printStackTrace();
		}

		
		DSApi.closeContextIfNeeded(ctx);
		
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
		// TODO Auto-generated method stub
		
	}
	

}
