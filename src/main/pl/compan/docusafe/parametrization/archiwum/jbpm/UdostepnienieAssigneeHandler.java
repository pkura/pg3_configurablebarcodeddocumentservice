package pl.compan.docusafe.parametrization.archiwum.jbpm;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.parametrization.archiwum.SpiszdawczoodbiorczyLogic;
import pl.compan.docusafe.parametrization.archiwum.UdostepnienieLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/** Asygnuje dokument na Sekretariat Departamentu osoby wskazanej jako odbiorca */
@SuppressWarnings("serial")
public class UdostepnienieAssigneeHandler extends AssigneeHandler {
	private final static Logger log= LoggerFactory.getLogger(UdostepnienieAssigneeHandler.class);
    private static final boolean __debug = true;

    public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {
		log.info("Wniosek o udost�pnienie akt - Dekretacja do akceptacji kierownika wybranego przez Archiwist�.");
		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		int departament = fm.getIntegerKey(UdostepnienieLogic.KOMORKA_AKCEPTUJACA_WNIOSEK_CN);
		
		DSDivision div = DSDivision.findById(departament);

        Long idProfilu = (Docusafe.getAdditionProperty("archiwum.jbpm.spiszdawczoodbiorczy.profile.kierownicy") != null ? Long.parseLong(Docusafe.getAdditionProperty("archiwum.jbpm.spiszdawczoodbiorczy.profile.kierownicy")) : null);
		if(idProfilu == null)
		{
	        String stanowisko = "";
	        stanowisko= Docusafe.getAdditionProperty("archiwum.jbpm.spiszdawczoodbiorczy.kierownicy");

	        String kierownicy = SpiszdawczoodbiorczyLogic.stanowiskoUser(departament,stanowisko, null);
	        if (kierownicy==null){
	        
	    	throw new Exception("Nie znaleziono w systemie dzia�u nadrz�dnego dla :" 
	    			+div.getName()+ " ze stanowiskiem" +stanowisko 
	    			+" !!!  Wskaz�wka :  Sprawdz czy Departament osoby odpowiedzialnej " +
	    					"za akceptacje przekazania dokumentacji jest Prawid�owo Okreslony");
	        }
	    	if(__debug) System.out.println("UdostepnienieAssigneeHandler stanowisko= "+stanowisko+", departament= "+departament+", result: kierownicy= "+kierownicy);

	        assignable.addCandidateGroup(kierownicy);
		}
		else
		{
			assignable.addCandidateUser(getOsobaUprawnionychFromProfile(departament,idProfilu));
		}
	}
    
	private String getOsobaUprawnionychFromProfile(int intValue, Long idProfilu)throws Exception {
		
		PreparedStatement ps = DSApi.context().prepareStatement(GET_PROFILE_SQL);
		ps.setLong(1, idProfilu);
		ps.setInt(2, intValue);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			return rs.getString(1);
		}
		throw new EdmException("Nie odnaleziono kierownika w dziale");
	}
}
