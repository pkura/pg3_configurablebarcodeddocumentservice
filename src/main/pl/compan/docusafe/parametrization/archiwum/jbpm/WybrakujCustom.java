package pl.compan.docusafe.parametrization.archiwum.jbpm;

import java.io.File;

import java.io.PrintWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.office.Container.ArchiveStatus;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.parametrization.archiwum.NewCaseArchiverScheduled;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class WybrakujCustom implements ExternalActivityBehaviour {
	private static final long serialVersionUID = 1L;

	protected static Logger log = LoggerFactory.getLogger(WybrakujCustom.class);

	@Override
	public void execute(ActivityExecution activityExecution) throws Exception {
		Long docid = Long.valueOf(activityExecution
				.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		// OfficeDocument doc = OfficeDocument.find(docid);
		// dodanie cast as numeric Artur Gdula 13-02-2015
		String sql = "select * from ds_ar_brakowanie_multiple bm "
				+ "left join ds_ar_brakowanie_pozycja bp on cast(bm.FIELD_VAL as numeric) = bp.id "
				+ "where bm.DOCUMENT_ID = " + docid;
		PreparedStatement ps = DSApi.context().prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		boolean wybrakuj, usun;

		PrintWriter pw = new PrintWriter(prepareFileToWybrakowaniaLog());
		pw.append("Wybrakowane Log:\n");
		while (rs.next()) {
			long folderId = (rs.getLong("folder_officeid"));

			wybrakuj = rs.getBoolean("wybrakuj");
			usun = rs.getBoolean("usun");
			if ((!wybrakuj && !usun) || (wybrakuj && usun)) {
				return;
			} else if (wybrakuj) {
				wybrakuj(rs.getInt(5), folderId); // 5 = id pozycji brakowania
				// new Date().getTime();
				pw.append("Wybrakowano Teczke o ID = ");
				pw.append(String.valueOf(folderId));
				pw.append(" o symbolu : "
						+ OfficeFolder.find(folderId).getOfficeId() + "\n");
			} else if (usun) {
				pw.append("Usunieto Teczke o o ID = ");
				pw.append(String.valueOf(folderId));
				pw.append("o symbolu : "
						+ OfficeFolder.find(folderId).getOfficeId() + "\n");
				usun(rs.getInt(5), folderId);
			}
		}
		pw.close();
	}

	private String prepareFileToWybrakowaniaLog()

	{
		StringBuilder path = new StringBuilder();
		path.append(Docusafe.getHome().getAbsolutePath() + "/brakowanie/");
		new File(path.toString()).mkdirs();
		path.append("wybrakowanieLog"
				+ new DateUtils()
						.formatCommonDateTimeWithoutWhiteSpaces(new Date()));
		return path.toString();

	}

	/**
	 * Funkcja oznacza w archiwum pozycj� jako usuniet�. Przechodzi przez
	 * teczk�, sprawy w teczce, dokumenty w sprawach i trwale wymazuje
	 * informacje o nich.
	 * 
	 * @param id_pozycji_protokolu_brakwoania
	 */
	private void usun(int id_pozycji_protokolu_brakowania, long officeId) {
		try {
			String sql = "";
			if (DSApi.isOracleServer())
				sql = "update "
						+ NewCaseArchiverScheduled.wykazItemsTableName
						+ " set DATE_SHRED = (SELECT SYSDATE from dual)  where FOLDERID = ?";
			else if (DSApi.isPostgresServer())
				sql = "update " + NewCaseArchiverScheduled.wykazItemsTableName
						+ " set DATE_SHRED = current_date where FOLDERID = ?";
			else
				sql = "update " + NewCaseArchiverScheduled.wykazItemsTableName
						+ " set DATE_SHRED = GETDATE()  where FOLDERID = ?";
			// String sql =
			// "update "+NewCaseArchiverScheduled.wykazItemsTableName+" set DATE_SHRED = GETDATE() where FOLDER_ID = "
			// + officeId;
			PreparedStatement ps = DSApi.context().prepareStatement(sql);
			ps.setLong(1, officeId);
			ps.execute();
			ps.close();

			// sql =
			// "update DSO_CONTAINER set NAME= 'pozycja usunieta',description='pozycja usunieta',title='pozycja usunieta' where ID = "
			// + officeId;
			OfficeFolder of = OfficeFolder.find(officeId);
			of.setArchiveStatus(ArchiveStatus.Shredded);
			of.setName("pozycja usunieta");

			// sql =
			// "update DSO_CONTAINER set NAME= 'pozycja usunieta',description='pozycja usunieta',title='pozycja usunieta' where PARENT_ID = "
			// + officeId+" and discriminator='case'";

			for (Container con : of.getChildren()) {
				if (con instanceof OfficeCase) {
					OfficeCase oc = (OfficeCase) con;
					oc.setArchiveStatus(ArchiveStatus.Shredded);
					oc.setDescription("pozycja usunieta");
					oc.setTitle("pozycja usunieta");
					List<OfficeDocument> dokumenty = oc.getDocuments();
					for (OfficeDocument dok : dokumenty) {
						dok.setTitle("pozycja usunieta");
						dok.setDescription("pozycja usunieta");
						// dok.setArchiveStatus(ArchiveStatus.Shredded.ordinal());
						for (Attachment a : dok.getAttachments())
							a.delete();

					}
				}
			}

		} catch (Exception e) {
			log.error("[Brakowanie-usun]", e);

		}

	}

	/**
	 * Brakujemy teczki. W teczce ustawiany jest archivestatus na 4
	 * (wybrakowany). Data wybrakowania na pozycji spisu zdawczo obiorczego
	 * Flaga czy_wybrakowany na pozycji protoko�u.
	 * 
	 * @param id_pozycji_protokolu_brakwoania
	 * @param officeFolerId
	 */
	private boolean wybrakuj(int id_pozycji_protokolu_brakwoania, long officeId) {
		try {
			// ustawienie dla pozycji spisu zdawczo odbiorczego daty
			// wybrakowania
			String sql = "";
			// String sql =
			// "update "+NewCaseArchiverScheduled.wykazItemsTableName+" set DATE_SHRED = DATE_SHRED = GETDATE()  where FOLDERID = ?";
			if (DSApi.isOracleServer())
				sql = "update "
						+ NewCaseArchiverScheduled.wykazItemsTableName
						+ " set DATE_SHRED = (SELECT SYSDATE from dual)  where FOLDERID = ?";
			// zmiana dla postgresql 13-02-2015 AG
			if (DSApi.isPostgresServer())
				sql = "update " + NewCaseArchiverScheduled.wykazItemsTableName
						+ " set DATE_SHRED = current_date  where FOLDERID = ?";
			else
				sql = "update " + NewCaseArchiverScheduled.wykazItemsTableName
						+ " set DATE_SHRED = GETDATE()  where FOLDER_ID = ?";

			PreparedStatement ps = DSApi.context().prepareStatement(sql);
			ps.setLong(1, officeId);
			ps.execute();
			ps.close();

			// ustawienie dla pozycji protokolu brakowania flagi wybrakowany

			sql = "update ds_ar_brakowanie_pozycja set czy_wybrakowany = ? where id = ?";
			ps = DSApi.context().prepareStatement(sql);
			ps.setBoolean(1, true);
			ps.setInt(2, id_pozycji_protokolu_brakwoania);

			ps.execute();
			ps.close();

			// ustawienie dla teczki pola ArchiveStatus na 4
			// (Shredded/Wybrakowany)
			OfficeFolder of = OfficeFolder.find(Long.valueOf(officeId));
			of.setArchiveStatus(ArchiveStatus.Shredded);
		} catch (Exception e) {
			log.error("[Brakowanie]", e);
			return false;
		}
		return true;
	}

	@Override
	public void signal(ActivityExecution activityExecution, String string,
			Map<String, ?> map) throws Exception {
		// TODO Auto-generated method stub
	}
}
