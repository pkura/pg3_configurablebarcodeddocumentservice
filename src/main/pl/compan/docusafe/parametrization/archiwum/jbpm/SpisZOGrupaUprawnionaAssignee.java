package pl.compan.docusafe.parametrization.archiwum.jbpm;

import java.util.Date;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SpisZOGrupaUprawnionaAssignee implements AssignmentHandler
{
	private static final Logger log = LoggerFactory.getLogger(SpisZOGrupaUprawnionaAssignee.class);

	private static final String ErrorSignal= "error-repair";
	
	boolean __debug=true;

	@Override
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception
	{
		Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		String division= null;
		
		try
		{
			OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId,false);
			
			openExecution.setVariable(Jbpm4WorkflowFactory.REASSIGNMENT_TIME_VAR_NAME, new Date());
		
			division= (String) openExecution.getVariable("DIVISION_GUID");
			
			assignable.addCandidateGroup(division);
		}catch (Exception e)
		{
			String info= "SpisZOGrupaUprawnionaAssignee error: doc "+docId+", division= "+division+", "+e.getMessage();
			log.error(info, e);
			if(__debug) System.out.println(info);
			assignable.addCandidateUser(GlobalPreferences.getAdminUsername());
			openExecution.setVariable("ERROR_DESCRIPTION", info);
			Jbpm4Provider.getInstance().getExecutionService().signalExecutionById(
					openExecution.getProcessInstance().getId(),
					ErrorSignal);
		}
	}
//
}
