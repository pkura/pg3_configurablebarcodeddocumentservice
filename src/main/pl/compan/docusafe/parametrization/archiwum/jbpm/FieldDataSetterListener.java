package pl.compan.docusafe.parametrization.archiwum.jbpm;

import java.util.Calendar;
import java.util.Collections;

import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class FieldDataSetterListener extends AbstractEventListener {

	private final static Logger LOG = LoggerFactory.getLogger(FieldDataSetterListener.class);

	private String field;

	
	@Override
	public void notify(EventListenerExecution eventListenerExecution) throws Exception {
		boolean hasOpen = true;
		if (!DSApi.isContextOpen())
		{
			hasOpen = false;
			DSApi.openAdmin();
		}
		Long docId = Long.valueOf(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId, false);
		doc.getDocumentKind().setOnly(docId, Collections.singletonMap(field, Calendar.getInstance().getTime()), false);
		if (DSApi.isContextOpen() && !hasOpen)
			DSApi.close();
		}

}