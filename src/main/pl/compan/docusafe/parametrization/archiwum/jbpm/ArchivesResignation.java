package pl.compan.docusafe.parametrization.archiwum.jbpm;

import java.sql.PreparedStatement;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.Container.ArchiveStatus;
import pl.compan.docusafe.parametrization.archiwum.db.provider.ArchiveQueryExecutor;
import pl.compan.docusafe.parametrization.archiwum.db.provider.ArchiveQueryProvider;

public class ArchivesResignation implements ExternalActivityBehaviour{

	private ArchiveQueryExecutor archiveQueryExecutor = ArchiveQueryProvider.getInstance();

	@Override
	public void execute(ActivityExecution activity) throws Exception {
		boolean ctx=DSApi.openContextIfNeeded(); 
		Long documentId = Jbpm4Utils.getDocumentId(activity);

		
		ArchivesFinish.UstawStatusyUsunietymZeSlownika();
		archiveQueryExecutor.updateCaseFolder(documentId);
		
		DSApi.closeContextIfNeeded(ctx);
		
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

}
