package pl.compan.docusafe.parametrization.archiwum.jbpm;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;

import java.util.Collections;
import java.util.Date;

public class DateSetterListener extends AbstractEventListener {
    private String dateFieldCn;
    private String value;
    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
        OfficeDocument doc = (OfficeDocument) OfficeDocument.find(docId, false);
        if (value != null && value.equals("currentDate"))
            doc.getDocumentKind().setOnly(docId, Collections.singletonMap(dateFieldCn, new Date()), false);
    }
}
