package pl.compan.docusafe.parametrization.archiwum.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.archiwum.UdostepnienieLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class DodatkowaAkceptacjaRektoraDecisionHandler implements DecisionHandler
{
	public static final String BEZ_AKCEPTACJI_REKTORA = "udostepnianie_akt_wnioskodawcy";
	public static final String DODATKOWA_AKCEPTACJA_REKTORA = "akceptacja_rektora";

	private final static Logger log = LoggerFactory.getLogger(DodatkowaAkceptacjaRektoraDecisionHandler.class);
	
	public String decide(OpenExecution execution)
	{
        try {
            OfficeDocument doc = null;
            doc = Jbpm4Utils.getDocument(execution);
            FieldsManager fm = doc.getFieldsManager();
            Boolean czyWnioskodawcaZewnetrzny =(Boolean) fm.getKey(UdostepnienieLogic.CZY_WNIOSKODAWCA_ZEWNETRZNY_CN);
            if (czyWnioskodawcaZewnetrzny==null)
                return BEZ_AKCEPTACJI_REKTORA;
            else if (czyWnioskodawcaZewnetrzny==true)
                return DODATKOWA_AKCEPTACJA_REKTORA;
            else
                return BEZ_AKCEPTACJI_REKTORA;
        } catch (EdmException e) {
            log.error("Proces udost�pniania akt: B��d podczas sprawdzania czy potrzebna jest dodatkowa akceptacja rektora. "+e);
            return BEZ_AKCEPTACJI_REKTORA;
        }
	}

}
