package pl.compan.docusafe.parametrization.archiwum.jbpm;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.Container;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.Container.ArchiveStatus;
import pl.compan.docusafe.parametrization.archiwum.NewCaseArchiverScheduled;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ArchivesFinish implements ExternalActivityBehaviour{
	private final static Logger log= LoggerFactory.getLogger(ArchivesFinish.class);
	
	private static boolean __debug= true;
	private static final String ErrorSignal= "repair";
	//private static final String EndSignal= "ustawStatusPrzyjety"; 
	private static final String EndSignal= "end"; 
	
	
	// zapytania dla wszystkich tworzonych spis�w
	//private static final String _spis= "update "+NewCaseArchiverScheduled.wykazTableName+" set status = 1 where DOCUMENT_ID = ?";
	//private static final String _spis_pozycja= "update "+NewCaseArchiverScheduled.wykazItemsTableName+" set STATUS_ID = 1 where id IN (SELECT FIELD_VAL  FROM "+NewCaseArchiverScheduled.wykazMultipleTableName+" where DOCUMENT_ID = ?) ";
	// zapytania dla dokument�w isteniejcych w systemie
	//private static final String _document= "update DS_DOCUMENT set archived = 1, archivedDate = GETDATE() where id = ?";

	
	// private static final String _container =
	// "update DSO_CONTAINER set ARCHIVESTATUS = 2, archivedDate = GETDATE() where id= ?";

	private static final String UstawStatusPozycjomFizZeSpisuRecznegoSQL="UPDATE "+NewCaseArchiverScheduled.wykazItemsTableName+" set archive_status= :status,archive_Date= :archiveDate  "
			+ "where id in(select items.id from "+NewCaseArchiverScheduled.wykazItemsTableName+" items "
			+ "left join "+NewCaseArchiverScheduled.wykazMultipleTableName+" multi on items.id=cast(multi.field_val as numeric) "
			+ "where multi.document_id= :id and folderid is null)";
	
	private static final String ZwrocIDUsunietychTeczekSQL="select id from dso_container con where con.discriminator='FOLDER' and con.archivestatus=1 and con.id not in ( "
			+ "select distinct items.folderid from "+NewCaseArchiverScheduled.wykazMultipleTableName+" multi "
			+ "left join "+NewCaseArchiverScheduled.wykazItemsTableName+" items on items.id=cast(multi.field_val as numeric) where items.folderid is not null) ";
	
	
	private static final String UstawStatusTeczkomSQL= "UPDATE dso_container  set archiveStatus= :status,archivedDate= :archivedDate where id in ( select teczka.id from dso_container teczka "
			+ "JOIN "+NewCaseArchiverScheduled.wykazItemsTableName+" items ON (teczka.officeid= items.folder_officeid) "
			+ "JOIN "+NewCaseArchiverScheduled.wykazMultipleTableName+" mult ON (items.id= cast(mult.field_val as numeric)) WHERE mult.document_id = :id  AND teczka.discriminator='FOLDER' )";
	
	private static final String UstawStatusSprawomSQL= "UPDATE dso_container  SET archiveStatus= :status, archivedDate= :archivedDate where id in(select sprawa.id FROM dso_container sprawa JOIN dso_container teczka ON (sprawa.parent_id= teczka.id) "
			+ "JOIN "+NewCaseArchiverScheduled.wykazItemsTableName+" items ON (teczka.officeid= items.folder_officeid)  "
			+ "JOIN "+NewCaseArchiverScheduled.wykazMultipleTableName+" mult ON (items.id= cast(mult.field_val as numeric)) WHERE mult.document_id = :id  AND sprawa.discriminator='CASE')";

	private static final String UstawStatusDokumentomINSQL= "UPDATE ds_document SET isArchived= :isArchived, archivedDate= :archivedDate where id in( "
			+ "select doc.id  FROM ds_document doc  "
			+ "JOIN dso_in_document in_doc ON (in_doc.id=doc.id) "
			+ "JOIN dso_container sprawa ON (in_doc.case_id= sprawa.id) JOIN dso_container teczka ON (sprawa.parent_id= teczka.id)  "
			+ "JOIN "+NewCaseArchiverScheduled.wykazItemsTableName+" items ON (teczka.officeid= items.folder_officeid)  "
			+ "JOIN "+NewCaseArchiverScheduled.wykazMultipleTableName+" mult ON (items.id= cast(mult.field_val as numeric)) WHERE mult.document_id = :id)";
	private static final String UstawStatusDokumentomOUTSQL="UPDATE ds_document SET isArchived= :isArchived, archivedDate= :archivedDate where id in( "
			+ "select doc.id  FROM ds_document doc  "
			+ "JOIN DSO_OUT_DOCUMENT out_doc ON ( out_doc.id=doc.ID) "
			+ "JOIN dso_container sprawa ON (out_doc.CASE_ID=sprawa.ID) JOIN dso_container teczka ON (sprawa.parent_id= teczka.id)  "
			+ "JOIN "+NewCaseArchiverScheduled.wykazItemsTableName+" items ON (teczka.officeid= items.folder_officeid)  "
			+ "JOIN "+NewCaseArchiverScheduled.wykazMultipleTableName+" mult ON (items.id= cast(mult.field_val as numeric)) WHERE mult.document_id = :id)";
	private static final String UstawStatusSpisuPrzyjetoIZaakceptowanoWArchiwum="update "+NewCaseArchiverScheduled.wykazTableName+" set STATUSDOKUMENTU=8 where document_id= :id";
	private static final String usunRoleModify="delete from ds_document_permission where document_id=cast(:id as numeric) and SUBJECTTYPE='user'";
	
	public static int UstawStatusPrzyjetoArchiwumDlaSpisu(long wykazId) throws HibernateException, EdmException{
		Query query = DSApi.context().session().createSQLQuery(UstawStatusSpisuPrzyjetoIZaakceptowanoWArchiwum);
		query.setLong("id",wykazId);
		return query.executeUpdate();
	}
	public static int UstawStatusPozycjomFizZeSpisuRecznego(long wykazId, Date archivedDate) throws HibernateException, EdmException
	{
		Query query = DSApi.context().session().createSQLQuery(UstawStatusPozycjomFizZeSpisuRecznegoSQL);
		query.setInteger("status", Container.ArchiveStatus.Archived.ordinal());
		query.setLong("id", wykazId);
		query.setDate("archiveDate", archivedDate);
		
		return query.executeUpdate();
	}
	
	
	public static int UstawStatusyUsunietymZeSlownika()throws HibernateException, EdmException
	{
		int przywroconych=0;

		try {
			PreparedStatement ps= DSApi.context().prepareStatement(ZwrocIDUsunietychTeczekSQL);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				OfficeFolder of=OfficeFolder.find(rs.getLong("id"));
				of.setArchiveStatus(ArchiveStatus.None);

				for(Container oc:of.getChildren()){
					if(oc instanceof OfficeCase){
						oc.setArchiveStatus(ArchiveStatus.None);
						ArrayList al=new ArrayList(oc.getDocuments());
						for(int i=0;i<al.size();i++){
							OfficeDocument od=(OfficeDocument) al.get(i);
							od.setIsArchived(false);
						}
					}
				}
			}


		} catch (SQLException e) {
			log.debug("",e);
			e.printStackTrace();
		}
		return przywroconych;


	}
	
	
	
	public static int ustawStatusTeczkom(long wykazId, Date archivedDate) throws HibernateException, EdmException
	{
		Query query = DSApi.context().session().createSQLQuery(UstawStatusTeczkomSQL);
		query.setInteger("status", Container.ArchiveStatus.Archived.ordinal());
		query.setDate("archivedDate", archivedDate);
		query.setLong("id", wykazId);
		
		return query.executeUpdate();
	}
	
	public static int ustawStatusSprawom(long wykazId, Date archivedDate) throws HibernateException, EdmException
	{
		Query query = DSApi.context().session().createSQLQuery(UstawStatusSprawomSQL);
		query.setInteger("status", Container.ArchiveStatus.Archived.ordinal());
		query.setDate("archivedDate", archivedDate);
		query.setLong("id", wykazId);
		
		return query.executeUpdate();
	}
	
	public static int ustawStatusDokumentomIN(long wykazId, Date archivedDate) throws HibernateException, EdmException
	{
		Query query = DSApi.context().session().createSQLQuery(UstawStatusDokumentomINSQL);
		query.setBoolean("isArchived", true);
		query.setDate("archivedDate", archivedDate);
		query.setLong("id", wykazId);
		
		return query.executeUpdate();
	}
	public static int ustawStatusDokumentomOUT(long wykazId, Date archivedDate) throws HibernateException, EdmException
	{
		Query query = DSApi.context().session().createSQLQuery(UstawStatusDokumentomOUTSQL);
		query.setBoolean("isArchived", true);
		query.setDate("archivedDate", archivedDate);
		query.setLong("id", wykazId);
		
		return query.executeUpdate();
	}
	
	public static int usunPrawoModyfikacji(long docId) throws EdmException
	{
		Query query = DSApi.context().session().createSQLQuery(usunRoleModify);
		query.setLong("id", docId);

		return query.executeUpdate();
	}

	public static String nadajUprawnienia(long docId) throws EdmException
	{
		String guid = Docusafe.getAdditionProperty("archiwum.guid.archiwista");
		Document document = Document.find(docId);
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

		perms.add(new PermissionBean(ObjectPermission.READ,
				guid, ObjectPermission.GROUP));
		perms.add(new PermissionBean(
				ObjectPermission.READ_ATTACHMENTS, guid,
				ObjectPermission.GROUP));
		perms.add(new PermissionBean(ObjectPermission.MODIFY,
				guid, ObjectPermission.GROUP));
		perms.add(new PermissionBean(
				ObjectPermission.MODIFY_ATTACHMENTS, guid,
				ObjectPermission.GROUP));
		perms.add(new PermissionBean(ObjectPermission.DELETE,
				guid, ObjectPermission.GROUP));

		Set<PermissionBean> documentPermissions = DSApi
				.context().getDocumentPermissions(document);
		perms.addAll(documentPermissions);
		((AbstractDocumentLogic) document.getDocumentKind()
				.logic()).setUpPermission(document, perms);

		return guid;
	}
	
	@Override
	public void execute(ActivityExecution execution) throws Exception {
		Long docId= null;
		try{
			OfficeDocument doc= Jbpm4Utils.getDocument(execution);
			FieldsManager fm= doc.getFieldsManager();
			docId= doc.getId();
			Date archiveDate= new Date();
			
			if(__debug) System.out.println("ArchivesFinishListener dla wykazu "+docId);
			
			int teczek= ustawStatusTeczkom(docId, archiveDate);
			int spraw= ustawStatusSprawom(docId, archiveDate);
			int docs= ustawStatusDokumentomIN(docId, archiveDate);
			docs+= ustawStatusDokumentomOUT(docId, archiveDate);
			int fizyczne=UstawStatusPozycjomFizZeSpisuRecznego(docId,archiveDate);
			int przywrocone= UstawStatusyUsunietymZeSlownika();
			UstawStatusPrzyjetoArchiwumDlaSpisu(docId);
			int iloscUsunietychRol = usunPrawoModyfikacji(docId);
			String guidArchiwista = nadajUprawnienia(docId);
			if(__debug) System.out.println("Zarchiwizowano "+teczek+" teczek, "+spraw+" spraw, "+docs+" dokumentow, " +fizyczne+" fizycznych, "+przywrocone+" przywroconych");
			
			doc.addAssignmentHistoryEntry(new AssignmentHistoryEntry(DSApi.context().getPrincipalName(), execution
					.getProcessInstance().getName(), AssignmentHistoryEntry.JBPM_FINISH.value));
			log.debug("ilosc usunietych rol: " + iloscUsunietychRol + " dodanie modyfikacji na grupe: " + guidArchiwista);
			 
			// pomyslnie, koniec
			/*Jbpm4Provider.getInstance().getExecutionService().signalExecutionById(
					execution.getProcessInstance().getId(),
					EndSignal);*/
			execution.take(EndSignal);
		}catch(Exception e)
		{
			String info= "Wystapil blad w ArchivesFinishListener dla wykazu o docId "+docId+" : "+e.getMessage();
			if(__debug) System.out.println(info);
			log.error(info,e);
			
			// przeslanie dokumentu do admina
		//	execution.
		//	assignable.addCandidateUser(GlobalPreferences.getAdminUsername());
			execution.setVariable("ERROR_DESCRIPTION",
					info );
			
			/*Jbpm4Provider.getInstance().getExecutionService().signalExecutionById(
					execution.getProcessInstance().getId(),
					ErrorSignal);*/
			execution.take(ErrorSignal);
		}
	}

		
	

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

}
