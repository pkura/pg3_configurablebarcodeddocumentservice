package pl.compan.docusafe.parametrization.archiwum.jbpm;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.parametrization.archiwum.WycofanieLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ArchiveAssigneeHandler extends AssigneeHandler {
	private final static Logger log= LoggerFactory.getLogger(ArchivesAssigneeHandler.class);
	
	private String poleOdbiorcy; //to jest pole typu private ale mimo to pojawi si� tu warto�� z zewn�trz.
	private String stanowisko; //
	private Map<String, Object> values = new HashMap<String, Object>();
	

	
	
	
	public void assign(Assignable assignable, OpenExecution openExecution) throws Exception {

		log.info("SekretariatDepartamentuOdbiorcy_AsgnHandler");
		Long docId = Long.valueOf( openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		int user = fm.getIntegerKey("DSUSER");
		Long departament = fm.getLongKey("DEPARTUSER");
		stanowisko = Docusafe.getAdditionProperty("archiwum.jbpm.spiszdawczoodbiorczy.kierownicy");
		Long idProfilu = (Docusafe.getAdditionProperty("archiwum.jbpm.spiszdawczoodbiorczy.profile.kierownicy") != null ? Long.parseLong(Docusafe.getAdditionProperty("archiwum.jbpm.spiszdawczoodbiorczy.profile.kierownicy")) : null);
		if(idProfilu == null)
		{
			String kierownicy = WycofanieLogic.stanowiskoUser(user, departament, stanowisko);
			if(!StringUtils.isBlank(kierownicy)){
				assignable.addCandidateGroup(kierownicy);				
			} else {
				Remark info = new Remark("System nie znalaz� odpowiedniej grupy w dziale: " + DSDivision.findById(departament).getName() + 
						". Dokument skierowany do Adminsitratora Systemu." ,"System");				
				doc.addRemark(info);
				assignable.addCandidateUser(GlobalPreferences.getAdminUsername());
			}
		}
		else
		{
			assignable.addCandidateUser(getOsobaUprawnionychFromProfile(departament,idProfilu));
		}
	}
	
	private String getOsobaUprawnionychFromProfile(Long intValue, Long idProfilu)throws Exception {
		
		System.out.println(GET_PROFILE_SQL);
		PreparedStatement ps = DSApi.context().prepareStatement(GET_PROFILE_SQL);
		ps.setLong(1, idProfilu);
		ps.setLong(2, intValue);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			return rs.getString(1);
		}
		throw new EdmException("Nie odnaleziono kierownika w dziale");
	}
}
