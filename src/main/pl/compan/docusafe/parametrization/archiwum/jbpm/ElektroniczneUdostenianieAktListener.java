package pl.compan.docusafe.parametrization.archiwum.jbpm;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.WfActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.archiwum.UdostepnienieLogic;
import pl.compan.docusafe.parametrization.archiwum.UdostepnionyZasob;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class ElektroniczneUdostenianieAktListener extends AbstractEventListener {

    private final static Logger log = LoggerFactory.getLogger(ElektroniczneUdostenianieAktListener.class);
    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        OfficeDocument doc = Jbpm4Utils.getDocument(execution);
        FieldsManager fm = doc.getFieldsManager();
        Boolean czyNadanieUprawnienElektronicznych = (Boolean) fm.getKey(UdostepnienieLogic.CZY_NADANIE_UPRAWNIEN_ELEKTRONICZNYCH_CN);
        DSUser user = null;
        Date terminZwrotu = (Date) fm.getKey(UdostepnienieLogic.TERMIN_ZWROTU_CN);
        if (terminZwrotu != null){
            execution.setVariable(Jbpm4WorkflowFactory.DEADLINE_TIME,terminZwrotu);
            execution.setVariable(Jbpm4WorkflowFactory.REMINDER_DATE,getDataPrzypomnienia(terminZwrotu));
        }
        try{
            DSApi.context().watch(URN.create(doc),doc.getAuthor());
        }catch (EdmException e){
            log.error("Błąd podczas dodawania dokumentu do obserwowanych: "+e);
        }

        if (czyNadanieUprawnienElektronicznych != null && czyNadanieUprawnienElektronicznych==Boolean.TRUE){
            if (fm.getKey(UdostepnienieLogic.USER_CN)!=null){
                Integer userId = (Integer) fm.getKey(UdostepnienieLogic.USER_CN);
                user = DSUser.findById(userId.longValue());
            }

            if (user == null)
                throw new EdmException("Ustawiono właściwość \"Nadanie uprawnień elektronicznych\" ale nie wybrano użytkownika w systemie.");

            List<Long> listaTeczek = (List<Long>) fm.getKey(UdostepnienieLogic.LISTA_TECZEK_CN);
            if (listaTeczek != null){
                for (Long idTeczki : listaTeczek){
                    udostepnijTeczke(user, idTeczki, terminZwrotu);
                }
            }
            List<Long> listaSpraw = (List<Long>) fm.getKey(UdostepnienieLogic.LISTA_SPRAW_CN);
            if (listaSpraw != null){
                for (Long idSprawy : listaSpraw){
                    udostepnijSprawe(user, idSprawy, terminZwrotu);
                }
            }
            List<Long> listaDokumentow = (List<Long>) fm.getKey(UdostepnienieLogic.LISTA_DOKUMENTOW_CN);
            if (listaDokumentow != null){
                for (Long idDok : listaDokumentow){
                    udostepnijDokument(user, idDok, terminZwrotu);
                }
            }
        }
    }

    private boolean udostepnijDokument(DSUser user, Long idDok, Date terminZwrotu) throws EdmException {
        log.info("proces udostepniania akt: Udostepnienie dokumentu (ID=" + idDok +
                ") dla użytkownika " + user.getName());
        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
        perms.add(new PermissionBean(ObjectPermission.READ, user.getName(), ObjectPermission.USER, null));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user.getName(), ObjectPermission.USER, null));

        for (PermissionBean perm : perms)
        {
            DSApi.context().grant(Document.find(idDok), perm);
            DSApi.context().session().flush();
        }
        UdostepnionyZasob zasob = new UdostepnionyZasob(user.getName(),
                UdostepnionyZasob.DOKUMENT,
                idDok,
                new java.sql.Date(getDataZwrotu(terminZwrotu).getTime()));
        zasob.create();
        return true;
    }

    private boolean udostepnijSprawe(DSUser user, Long idSprawy, Date terminZwrotu) throws EdmException {
        log.info("proces udostepniania akt: Udostepnienie sprawy (ID=" + idSprawy +
                ") dla użytkownika " + user.getName());
        OfficeCase sprawa = OfficeCase.find(idSprawy);
        List<Document> listaDokumentow = (List<Document>) sprawa.getDocuments();
        for (Document doc : listaDokumentow){
            udostepnijDokument(user, doc.getId(),terminZwrotu);
        }
        UdostepnionyZasob zasob = new UdostepnionyZasob(user.getName(),
                UdostepnionyZasob.SPRAWA,
                idSprawy,
                new java.sql.Date(getDataZwrotu(terminZwrotu).getTime()));
        zasob.create();
        return true;
    }

    private boolean udostepnijTeczke(DSUser user, Long idTeczki, Date terminZwrotu) throws EdmException {
        log.info("proces udostepniania akt: Udostepnienie teczki (ID=" + idTeczki +
                ") dla użytkownika " + user.getName());
        OfficeFolder teczka = OfficeFolder.find(idTeczki);
        Set zbiorPodteczekISpraw= teczka.getChildren();
        for (Object container : zbiorPodteczekISpraw){
            if (container instanceof OfficeFolder){
                OfficeFolder podteczka = (OfficeFolder) container;
                udostepnijTeczke(user, podteczka.getId(), terminZwrotu);
            }else if (container instanceof OfficeCase){
                OfficeCase sprawa = (OfficeCase) container;
                udostepnijSprawe(user, sprawa.getId(), terminZwrotu);
            }
        }
        UdostepnionyZasob zasob = new UdostepnionyZasob(
                user.getName(),
                UdostepnionyZasob.TECZKA,
                idTeczki,
                new java.sql.Date(getDataZwrotu(terminZwrotu).getTime()));
        zasob.create();
        return true;
    }

    private Date getDataZwrotu(Date dataZwrotu) {
        Calendar cal = Calendar.getInstance();
        if (dataZwrotu != null){
            cal.setTime(dataZwrotu);
        }else{
            cal.add(Calendar.DATE, 14);
        }
        return cal.getTime();
    }

    private Date getDataPrzypomnienia(Date dataZwrotu) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dataZwrotu);
        cal.add(Calendar.DATE, -7);
        return cal.getTime();
    }
}
