package pl.compan.docusafe.parametrization.archiwum.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.archiwum.UdostepnienieLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Date;

public class CzyWnioskodawcaZewnetrznyDecisionHandler implements DecisionHandler {
    public static final String WEWNETRZNY_JEST_TERMIN_ZWROTU = "powiadomienie_o_udostepnieniu";
    public static final String WEWNETRZNY_KONIEC = "powiadomienie_o_udostepnieniu2";

    public static final String ZEWNETRZNY_JEST_TERMIN_ZWROTU = "potwierdzenie-zwrotu";
    public static final String ZEWNETRZYNY_KONIEC = "end";

    private final static Logger log = LoggerFactory.getLogger(DodatkowaAkceptacjaRektoraDecisionHandler.class);

    public String decide(OpenExecution execution)
    {
        try {
            OfficeDocument doc = null;
            doc = Jbpm4Utils.getDocument(execution);
            FieldsManager fm = doc.getFieldsManager();
            Boolean czyWnioskodawcaZewnetrzny =(Boolean) fm.getKey(UdostepnienieLogic.CZY_WNIOSKODAWCA_ZEWNETRZNY_CN);
            if (czyWnioskodawcaZewnetrzny == null)
                czyWnioskodawcaZewnetrzny = Boolean.FALSE;
            Date terminZwrotu = (Date) fm.getKey(UdostepnienieLogic.TERMIN_ZWROTU_CN);

            if (czyWnioskodawcaZewnetrzny==false && terminZwrotu != null)
                return WEWNETRZNY_JEST_TERMIN_ZWROTU;
            else if (czyWnioskodawcaZewnetrzny==false && terminZwrotu == null)
                return WEWNETRZNY_KONIEC;
            else if (czyWnioskodawcaZewnetrzny==true && terminZwrotu != null)
                return WEWNETRZNY_JEST_TERMIN_ZWROTU;
            else if (czyWnioskodawcaZewnetrzny==true && terminZwrotu == null)
                return WEWNETRZNY_KONIEC;
            else
                return WEWNETRZNY_KONIEC;
        } catch (EdmException e) {
            log.error("Proces udost�pniania akt: B��d podczas sprawdzania czy potrzebna jest dodatkowa akceptacja rektora. "+e);
            return WEWNETRZNY_KONIEC;
        }
    }
}
