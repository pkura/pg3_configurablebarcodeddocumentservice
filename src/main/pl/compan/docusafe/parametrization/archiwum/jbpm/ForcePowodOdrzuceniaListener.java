package pl.compan.docusafe.parametrization.archiwum.jbpm;

import org.jbpm.api.listener.EventListenerExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.archiwum.UdostepnienieLogic;

public class ForcePowodOdrzuceniaListener extends AbstractEventListener {

    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        OfficeDocument doc = Jbpm4Utils.getDocument(execution);
        FieldsManager fm = doc.getFieldsManager();
        if(fm.getValue(UdostepnienieLogic.POWOD_ODRZUCENIA_CN) == null)
            throw new EdmException("Wype�nij pole POW�D ODRZUCENIA.");
    }
}
