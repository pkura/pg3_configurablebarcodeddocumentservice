package pl.compan.docusafe.parametrization.archiwum.jbpm;

import java.sql.PreparedStatement;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.Container.ArchiveStatus;
import pl.compan.docusafe.parametrization.archiwum.NewCaseArchiverScheduled;
import pl.compan.docusafe.parametrization.archiwum.db.provider.ArchiveQueryExecutor;
import pl.compan.docusafe.parametrization.archiwum.db.provider.ArchiveQueryProvider;

public class RezygnacjaSpisAP  implements ExternalActivityBehaviour{

	ArchiveQueryExecutor archiveQueryExecutor = ArchiveQueryProvider.getInstance();
	
	@Override
	public void execute(ActivityExecution activity) throws Exception {
		boolean ctx=DSApi.openContextIfNeeded(); 
		Long documentId = Jbpm4Utils.getDocumentId(activity);
		Document doc=Document.find(documentId);
		
		
		PreparedStatement ps;
		//powrot statusow dla spraw znajdujacych sie w  teczkach usunietych
		archiveQueryExecutor.updateResignationCaseBelongsToDeletedPortfolios();
		
		//powrot statusow dla teczek ktore byly dodane do spisu ale zostaly usuniete
		archiveQueryExecutor.updateForCaseBelongsToPortfoliosWhichBe();
		
		//powrot statusow dla teczek ktore byly dodane do spisu ale zrezygnowano
		archiveQueryExecutor.updateForComeBackPortfoliosStatus(documentId);

		//powrot statusow dla spraw znajdujacych sie w tamtych teczkach
		archiveQueryExecutor.updateForComeBackCaseStatus(documentId);
		
		//powrot statusow dla fizycznych usunietych
		archiveQueryExecutor.updateComeBackStatusCaseForPhysicalByArchiveStatus();

		//powrot statusow dla fizycznych nadal na slowniku
		archiveQueryExecutor.updateComeBackStatusCaseForPhysicalBeingOnDictionary(documentId);

		
		DSApi.closeContextIfNeeded(ctx);

		
	}


	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

}
