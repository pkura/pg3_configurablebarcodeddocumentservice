package pl.compan.docusafe.parametrization.archiwum;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class UdostepnienieLogic  extends AbstractDocumentLogic{
    protected static Logger log = LoggerFactory.getLogger(UdostepnienieLogic.class);

    private static UdostepnienieLogic instance;
	public final static String USER_CN = "DSUSER";
    public static final String DIVISION_CN = "DSDIVISION";
    public static final String STATUS_DOKUMENTU_CN = "STATUSDOKUMENTU";
    public static final String DATA_ZLOZENIA_WNIOSKU_CN = "data_zlozenia_wniosku";
    public static final String CZY_WNIOSKODAWCA_ZEWNETRZNY_CN = "is_wnioskodawca_zewnetrzny";
    public static final String CZY_WNIOSKODAWCA_ZEWNETRZNY_OSOBA_PRAWNA = "WNIOSK_ZEW_OS_PRAW";
    public static final String DANE_WNIOSKODAWCY_ZEWNETRZNEGO_CN = "SENDER";
    public static final String KOMORKA_WLASCICIELI_AKT_CN = "DSDIVISION_WLASCICIELE";
    public static final String SYGNATURA_ARCHIWALNA_CN = "sygnatura_archiwalna";
    public static final String DATA_DOK_OD_CN = "data_dokumenty_od";
    public static final String DATA_DOK_DO_CN = "data_dokumenty_do";
    public static final String TYTUL_CN = "tytul";
    public static final String TERMIN_ZWROTU_CN = "TERMIN_ZWROTU";
    public static final String CZY_WYPOZYCZENIE_FIZYCZNE_CN = "is_wypozyczenie_fizyczne";
    public static final String CZY_UDOSTEPNIENIE_AKT_NA_MIEJSCU_CN = "is_udostepnienie_akt_na_miejscu";
    public static final String CZY_KSEROKOPIA_CN = "is_kserokopia";
    public static final String CZY_NADANIE_UPRAWNIEN_ELEKTRONICZNYCH_CN = "is_nadanie_uprawnien_elektronicznych";
    public static final String CZY_INNE_UDOSTEPNIENIE_CN = "inne_ch";
    public static final String INNE_UDOSTEPNIENIE_OPIS_CN = "INNE";
    public static final String UZASADNIENIE_WNIOSKU_CN = "uzasadnienie";
    public static final String KOMORKA_AKCEPTUJACA_WNIOSEK_CN = "DSDIVISION_KIEROWNIK";
    public static final String DATA_ODBIORU_DOK_CN = "data_odbioru_dokumentow";
    public static final String POWOD_ODRZUCENIA_CN = "powod_odrzucenia";
    public static final String DATA_ZWROTU_DOK_CN = "data_zwrotu_dokumentow";
    public static final String CZY_ZWROT_POTWIERDZONY_CN = "is_potwierdzony_zwrot";
    public static final String LISTA_TECZEK_CN = "LISTA_TECZEK";
    public static final String LISTA_SPRAW_CN = "LISTA_SPRAW";
    public static final String LISTA_DOKUMENTOW_CN = "LISTA_DOKUMENTOW";
    public static final String LISTA_NIEZDIGITALIZOWANE_CN = "LISTA_DOKUMENTOW";

    public static final int STATUS_ID_REJESTRACJA = 10;
    public static final int STATUS_ID_AKCEPTACJA_ARCHIWISTY = 20;
    public static final int STATUS_ID_WYBOR_DOKUMENTACJI = 30;
    public static final int STATUS_ID_AKCEPTACJA_KIEROWNIKA = 40;
    public static final int STATUS_ID_AKCEPTACJA_REKTORA = 45;
    public static final int STATUS_ID_UDOSTEPNIANIE_AKT = 50;
    public static final int STATUS_ID_POWIADOMIENIE_O_UDOSTEPNIENIU = 60;
    public static final int STATUS_ID_POTWIERDZENIE_ODBIORU = 65;
    public static final int STATUS_ID_AKTA_ZWROCONE = 70;
    public static final int STATUS_ID_WNIOSEK_ODRZUCONY = 80;

    public static final String AKCEPT_ARCHIWISTA_CN = "AKCEPT_ARCHIWISTA";
    public static final String DATA_AKCEPT_ARCHIWISTY_CN = "DATA_AKCEPT_ARCHIWISTY";
    public static final String STATUS_AKCEPT_ARCHIWISTY_CN = "STATUS_AKCEPT_ARCHIWISTY";
    public static final String AKCEPT_KIEROWNIK_CN = "AKCEPT_KIEROWNIK";
    public static final String DATA_AKCEPT_KIEROWNIK_CN = "DATA_AKCEPT_KIEROWNIK";
    public static final String STATUS_AKCEPT_KIEROWNIK_CN = "STATUS_AKCEPT_KIEROWNIK";
    public static final String AKCEPT_REKTORA_CN = "AKCEPT_REKTORA";
    public static final String DATA_AKCEPT_REKTORA_CN = "DATA_AKCEPT_REKTORA";
    public static final String STATUS_AKCEPT_REKTORA_CN = "STATUS_AKCEPT_REKTORA";
    public static final String DWRPESEL = "DWR_PESEL";
    public static final String PESEL = "PESEL";
    public static final String DWRNIP = "DWR_NIP";
    public static final String NIP = "NIP";
    public static final String DWRREGON = "DWR_REGON";
    public static final String REGON = "REGON";
	public static String CN_DATA_DOKUMENTU ="DOC_DATE";
	public static String CN_AUTOR_DOKUMENTU ="DOC_AUTOR";
	public static String CN_DZIAL_AUTORA_DOKUMENTU ="DOC_AUTORDIVISION";
	public static String CN_TYP_DOKUMENTU = "TYP_DOKUMENTU";
	public static String CN_ABSTRAKT_DOKUMENTU = "ABSTRAKT";
	public static String CN_MIESCEUTWOORZENIA_DOKUMENTU = "MIESCEUTWOORZENIA";
	public static String CN_DOC_DESCRIPTION = "DOC_DESCRIPTION";
	public static UdostepnienieLogic getInstance()
	{
		if (instance == null)
			instance= new UdostepnienieLogic();
		return instance;
	}
	@Override
	public void documentPermissions(Document document) throws EdmException {

    }

    public TaskListParams getTaskListParams(DocumentKind dockind, long documentId) throws EdmException
    {
        TaskListParams params = new TaskListParams();
        try
        {
            Document doc = Document.find(documentId);
            FieldsManager fm = doc.getFieldsManager();

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date terminPlatnosci = null;

            if (fm.getValue(TERMIN_ZWROTU_CN)!=null){
                terminPlatnosci = (Date) fm.getValue(TERMIN_ZWROTU_CN);
            }
//            setReminderDate(terminPlatnosci, documentId);
            params.setRealizationDate(terminPlatnosci);
            params.setMarkerClass("tasklistMarkerReminder");
        }
        catch (Exception e)
        {
            log.error("",e);
        }
        return params;
    }

    public void setInitialValues(FieldsManager fm, int type)
			throws EdmException {
		//log.info("type: {}", type);
		Map<String, Object> values = new HashMap<String, Object>();
		values.put(STATUS_DOKUMENTU_CN, STATUS_ID_REJESTRACJA);
		values.put(DATA_ZLOZENIA_WNIOSKU_CN, new Date());
		values.put(CN_DATA_DOKUMENTU, new Date());
		values.put(USER_CN, DSApi.context().getDSUser().getId());
		values.put(CN_TYP_DOKUMENTU, 2);
		values.put(CN_DOC_DESCRIPTION, "Wniosek o udost�pnienie dokumentacji");
        DSDivision[] divs =  DSApi.context().getDSUser().getDivisionsWithoutGroupPosition();
        if (divs.length>0)
            values.put(DIVISION_CN, divs[0].getId());

		fm.reloadValues(values);
	}

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        FieldsManager fm = document.getFieldsManager();
        Integer statusId = (Integer) fm.getKey(STATUS_DOKUMENTU_CN);
        Long userId = DSApi.context().getDSUser().getId();
        if (statusId == STATUS_ID_AKCEPTACJA_ARCHIWISTY){
            document.getDocumentKind().setOnly(document.getId(), Collections.singletonMap(AKCEPT_ARCHIWISTA_CN, userId));
        }else if (statusId == STATUS_ID_AKCEPTACJA_KIEROWNIKA){
            document.getDocumentKind().setOnly(document.getId(), Collections.singletonMap(AKCEPT_KIEROWNIK_CN, userId));
        }else if (statusId == STATUS_ID_AKCEPTACJA_REKTORA){
            document.getDocumentKind().setOnly(document.getId(), Collections.singletonMap(AKCEPT_REKTORA_CN, userId));
        }
    }


    @Override
    public void setAdditionalTemplateValues(long docId,
    		Map<String, Object> values) {
    	try {

    		OfficeDocument doc;

    		doc = OfficeDocument.find(docId);
    		FieldsManager fm = doc.getFieldsManager();
    		fm.initialize();

    		if(fm.getValue(DATA_ZLOZENIA_WNIOSKU_CN)!=null)
				try {
					DateFormat df=new SimpleDateFormat("dd-MM-yyyy");
					values.put("DATAWYP",df.format(DateUtils.parseDateTimeAnyFormat(((Date)fm.getValue(DATA_ZLOZENIA_WNIOSKU_CN)).toLocaleString())));
				} catch (ParseException e) {
					log.debug("nie udalo sie sformatowac daty przy tworzeniu template'a");
				}
			if(fm.getValue(DIVISION_CN)!=null)values.put("WYDZIAL",fm.getValue(DIVISION_CN));
			/*if(fm.getValue(TYTUL_CN)!=null)values.put("TYTUL", fm.getValue(TYTUL_CN));*/
			if(fm.getValue(CN_DOC_DESCRIPTION)!=null)values.put("TYTUL", fm.getValue(CN_DOC_DESCRIPTION));
			if(fm.getValue(SYGNATURA_ARCHIWALNA_CN)!=null)values.put("SYGNATURA",fm.getValue(SYGNATURA_ARCHIWALNA_CN));
			if(fm.getValue(TERMIN_ZWROTU_CN)!=null)values.put("DATAZW", fm.getValue(TERMIN_ZWROTU_CN));
			String uwagi="";

			if(!Remark.documentRemarks(doc.getId()).isEmpty()){
				for(Remark r:Remark.documentRemarks(doc.getId())){
					uwagi+=r.getContent()+"; ";
				}
			}
			values.put("UWAGI", uwagi);
    		
    		

    	} catch (DocumentNotFoundException e) {
    		log.error("",e);
    		e.printStackTrace();
    	} catch (EdmException e) {
    		log.error("",e);
    		e.printStackTrace();
    	}

    }

    @Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        StringBuilder msgBuilder = new StringBuilder();
        FieldData dataOdField = values.get("DWR_"+DATA_DOK_OD_CN);
        FieldData dataDoField = values.get("DWR_"+DATA_DOK_DO_CN);
        if (!UtilDWR.isEmptyField(dataOdField) && !UtilDWR.isEmptyField(dataDoField)){
            Date dataOd = dataOdField.getDateData();
            Date dataDo = dataDoField.getDateData();
            if (dataOd.compareTo(dataDo)>0)
                msgBuilder.append("Pole DATA DOKUMENT�W OD nie mo�e by� p�niejsze od pola DATA DOKUMENT�W DO.");
        }

        if (msgBuilder.length()>0) {
            values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
            return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
        } else
            return null;
    }
    
    
	@Override
	public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
	{

		if (values.get(CN_DZIAL_AUTORA_DOKUMENTU) != null && values.get(DIVISION_CN) != null)
		{
			long al = (Long) values.get(CN_DZIAL_AUTORA_DOKUMENTU);
			int ul = (Integer) values.get(DIVISION_CN);
			if (al != ul)
				values.put(CN_DZIAL_AUTORA_DOKUMENTU, Long.parseLong(String.valueOf(ul)));
		}
	}

	private void fillUserDivisions(Map<String, Object> values) throws UserNotFoundException, EdmException
	{
		Long userDivision = 1l;
		DSUser user = DSApi.context().getDSUser();

		DSDivision[] divs = DSApi.context().getDSUser().getOriginalDivisionsWithoutGroup();

		for (int i = 0; i < divs.length; i++)
		{
			if (divs[i].isPosition())
			{

				DSDivision parent = divs[i].getParent();
				if (parent != null)
				{
					userDivision = parent.getId();
					break;
				} else
				{
					userDivision = divs[i].getId();
				}
			} else if (divs[i].isDivision())
			{
				userDivision = divs[i].getId();
				break;
			}
		}
		values.put(DIVISION_CN, divs);
	}
}
