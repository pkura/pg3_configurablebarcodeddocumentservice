package pl.compan.docusafe.parametrization.archiwum;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.field.HTMLFieldLogic;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeFolder;

public class HTMLOfficeFolderLinkLogic implements HTMLFieldLogic {
    @Override
    public String getValue(String officeFolderId) throws EdmException {
        Long attachmentId = null;
        StringBuilder sb = new StringBuilder();
        String pageContext = Docusafe.pageContext;
        if (pageContext != null && !"".equalsIgnoreCase(pageContext))
            pageContext = "/"+pageContext;
        OfficeFolder officeCase = OfficeFolder.find(Long.valueOf(officeFolderId));
        sb.append("<a href=\"" + pageContext + "/office/edit-portfolio.action?id=");
        sb.append(officeFolderId);
        sb.append("\">");
        sb.append(officeCase.getOfficeId());
        sb.append("</a>");
        return sb.toString();
    }

    @Override
    public String getValue() {
        return null;
    }
}
