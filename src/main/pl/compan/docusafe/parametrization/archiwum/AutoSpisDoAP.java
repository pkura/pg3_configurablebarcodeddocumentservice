package pl.compan.docusafe.parametrization.archiwum;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;

public class AutoSpisDoAP extends ServiceDriver{



	private Timer timer;
	@Override
	protected void start() throws ServiceException
	{		
		timer = new Timer(true);
		timer.schedule(new SpisDoAP(), 10000,  DateUtils.MINUTE);


	}

	@Override
	protected void stop() throws ServiceException {
		if (timer != null) timer.cancel();
	}

	@Override
	protected boolean canStop() {
		return false;
	}

	public class SpisDoAP extends TimerTask {

		@Override
		public void run() {
			boolean ctx;
			try {
				ctx = DSApi.openContextIfNeeded();

				console(Console.INFO, "Tworzenie nowego Spisu Do AP");

				OutOfficeDocument doc = new OutOfficeDocument();
				DSApi.context().begin();
				doc.setTitle("Spis zdawczo-odbiorczy materiałów archiwalnych nr");
				doc.setDescription("Spis zdawczo-odbiorczy materiałów archiwalnych");
				doc.setVersion(1);
				doc.setCzyAktualny(true);
				doc.setCzyCzystopis(false);
				doc.setSummary(""); 
				doc.setInternal(true);
				doc.setCreatingUser("admin");
				doc.create();

				//TODO dodac wpis w tabeli NewCaseArchiverScheduled.wykazTableName
				DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.ARCHIVES_PRZEKAZANIE_AP);

				doc.setVersionGuid(doc.getId().intValue());
				doc.setDocumentKind(documentKind);
				Map<String, Object> fieldsValues = new HashMap<String, Object>();
				//documentKind.saveMultiDictionaryValues(doc, spisZdawczoOdbiorczy.slownik );
				documentKind.set(doc.getId(), fieldsValues);

				DSApi.context().session().save(doc);
				
				doc.getDocumentKind().logic().onStartProcess(doc);
				WorkflowFactory.createNewProcess(doc, false, "Wykaz zdawczo-odbiorczy nr "+"Numer");
				AssignOfficeNumber(doc);
				DSApi.context().commit();
				DSApi.closeContextIfNeeded(ctx); 
				
			} catch (EdmException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}

	}
	private void AssignOfficeNumber(Document doc){
		try
		{
			if (doc instanceof OutOfficeDocument)
			{
				Integer sequenceId = Journal.TX_newEntry2(Journal.getMainOutgoing().getId(), doc.getId(), new Date());
				((OutOfficeDocument) doc).bindToJournal(Journal.getMainOutgoing().getId(), sequenceId);
				((OutOfficeDocument) doc).setOfficeNumber(sequenceId);
			}
			

		} catch (Exception e)
		{	
			DSApi.context()._rollback();
			log.error("B??d wykonywania akcji "+ e);
		}
	}

}
