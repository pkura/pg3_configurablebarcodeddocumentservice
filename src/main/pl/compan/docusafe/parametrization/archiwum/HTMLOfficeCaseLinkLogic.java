package pl.compan.docusafe.parametrization.archiwum;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.dockinds.field.HTMLFieldLogic;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.web.viewserver.ViewServer;

public class HTMLOfficeCaseLinkLogic implements HTMLFieldLogic {
    @Override
    public String getValue(String officeCaseId) throws EdmException {
        Long attachmentId = null;
        StringBuilder sb = new StringBuilder();
        String pageContext = Docusafe.pageContext;
        if (pageContext != null && !"".equalsIgnoreCase(pageContext))
            pageContext = "/"+pageContext;
        OfficeCase officeCase = OfficeCase.find(Long.valueOf(officeCaseId));
        sb.append("<a href=\"" + pageContext + "/office/edit-case.do?id=");
        sb.append(officeCaseId);
        sb.append("&tabId=summary\">");
        sb.append(officeCase.getOfficeId());
        sb.append("</a>");
        return sb.toString();
    }

    @Override
    public String getValue() {
        return null;
    }
}
