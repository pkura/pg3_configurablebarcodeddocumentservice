package pl.compan.docusafe.parametrization.aegon;

import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.FileUtils;


public class FileImportDriver extends ServiceDriver implements Service
{
	private static final Log log = LogFactory.getLog(FileImportDriver.class);
	
	private final static long TIME_THRESHOLD = 60 * 1000;
	
	//Errorcodes
	private final static Long BROKEN_ID = -1L;
	private final static Long BROKEN_FILE_NAME = -2L;
	private final static Long BROKEN_DOCUMENT = -3L;
	
	private final static String RAPORT_FILE_NAME = "raport.csv";
	
	private final static int BYTE = 1024;
	
	private Timer timer;
	
	private File dir;
	private Boolean powerOn = false;
	private Long boxNumber;
	private Date date;
	private ArrayList<String> fileExtensions = new ArrayList<String>();
	private Boolean console = false;
	
	
	private Property[] properties;
	
	public Property[] getProperties() 
	{
		return properties;
	}

	public boolean hasConsole()
    {
        return console;
    }

	public FileImportDriver()
	{
		properties = new Property[] {
	            new DirProperty(),
	            new FileExtensionsProperty(),
	            new BoxNumberProperty(),
	            //new DateProperty(),
	            new ConsoleProperty(),
	            new PowerOnProperty(),
	            new ReportsProperty()
	        };
	}
	
	protected boolean canStop() {
		return false;
	}

	protected void start() throws ServiceException 
	{

		timer = new Timer(true);
		timer.schedule(new Import(), (long) 2*1000 ,(long) 1*DateUtils.MINUTE);				
	}

	protected void stop() throws ServiceException 
	{
		if(timer != null)
			timer.cancel();
	}

	private Map<String, String> getReports()
	{
		Map<String, String> reports = new HashMap<String, String>();
		if(dir == null)
			return reports;
		
		for(File x : FileUtils.findAllFilesInDirByName(dir, RAPORT_FILE_NAME))
		{
			reports.put(x.getParentFile().getName(), x.getParentFile().getName());		
		}
		
		return reports;
	}
	
	public File getReportFileByName(String name)
	{
		return FileUtils.findFilesInDirByName(new File(dir+File.separator+name), RAPORT_FILE_NAME).get(0);
	}
	
	private class Import extends TimerTask
	{
		private Boolean isRunning = false;
		
		private Long importSingleOne(File file)
		{
			Long documentId = BROKEN_ID;
			console(Console.INFO, "Importuje plik "+file.getAbsolutePath());
			
			Integer rodzaj = 0;
			String nrPolisy = "";
			String nrSprawy = null;
			try
			{
				String[] parts = file.getName().split("\\.")[0].split("_+");
				rodzaj = Integer.parseInt(parts[0]);
				nrPolisy = parts[1];
				if(parts.length == 4)
				{
					nrSprawy = parts[2];
				}
				//DocumentKind.findByCn(DocumentKind.NATIONWIDE_KIND).getFieldByCn("RODZAJ").getEnumItem(rodzaj);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				return BROKEN_FILE_NAME;			
			}
			
			try
			{
				DSApi.openAdmin();
				DSApi.context().begin();
				
				//dokument
				DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.NATIONWIDE_KIND);
				Document document = new Document("","");
				document.setDocumentKind(documentKind);
				document.setFolder(Folder.getRootFolder());
				document.create();
				
				DSApi.context().session().flush();
				
				//dockind
				Map<String,Object> values = new HashMap<String,Object>();

				documentKind.set(document.getId(), values);
				
				documentKind.logic().archiveActions(document, DocumentLogic.TYPE_ARCHIVE);
				documentKind.logic().documentPermissions(document);

				//zalacznik
				Attachment attachment = new Attachment("Skan");
                document.createAttachment(attachment);
                attachment.createRevision(file).setOriginalFilename(file.getName());
												
				DSApi.context().session().flush();
				
				//box
				document.setBox(Box.find(boxNumber));
				
				DSApi.context().commit();
				
				//jesli wszystko poszlo ok mozemy zwrocic id dokumentu
				documentId = document.getId();
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				DSApi.context()._rollback();
				log.error(new EdmException(e));
				return BROKEN_DOCUMENT;
			}
			finally
			{				
				DSApi._close();
			}
			
			return documentId;
		}
		
		public void run() {
			
			synchronized(this)
			{
				if(isRunning)
				{
					console(Console.INFO, "Import w trakcie - pomijanie uruchomienia");
					return;
				}
				else if(!powerOn)
				{
					//console(Console.INFO, "Import wylaczony - pomijanie uruchomienia");
					return;
				}
				else if(boxNumber == null /*|| date == null*/)
				{
					console(Console.INFO, "Brak wymaganych danych - pomijanie uruchomienia");
					return;
				}
				isRunning = true;
			}
			
			try
			{
				File[] files = dir.listFiles(new FileFilter()
				{
					public boolean accept(File file)
					{
						boolean acc = false;
						for(String str : fileExtensions)
						{
							if(file.getName().toLowerCase().endsWith(str.toLowerCase()))
							{
								acc = true;
								break;
							}
						}
						return ((System.currentTimeMillis() - file.lastModified()) > TIME_THRESHOLD  && acc && file.length() > BYTE*10);
						//return ((System.currentTimeMillis() - file.lastModified()) > TIME_THRESHOLD  
						//		&& fileExtensions.contains(file.getName().split("\\.")[file.getName().split("\\.").length-1]) 
						//		&& file.length() > BYTE*10);
					}
				});
				
				if(files.length == 0)
				{
					console(Console.INFO, "Brak plikow do importu");
					return;
				}
				
				File directory = new File(dir.getAbsolutePath()+File.separator+DateUtils.formatCommonDateTimeWithoutWhiteSpaces(new Date()));
				directory.mkdir();
				console(Console.INFO, "Tworze katalog "+directory.getAbsolutePath());
				
				StringBuffer sb = new StringBuffer();
				Integer done = new Integer(0);
				Integer broken = new Integer(0);
				
				
				sb.append("Plik;status;Id dokumentu\n");
				for(File file : files)
				{
					String endExtension = "";
					Long id = importSingleOne(file);
					
					if(id < 0)
					{
						endExtension = "broken";
						console(Console.INFO, "Nie udalo sie zaimportowac pliku "+file.getName());
						broken++;
						sb.append(file.getName()+";nie dodany;");
						sb.append("");
						if(id.equals(BROKEN_DOCUMENT))
							sb.append("nie udalo sie stwozyc dokumentu;\n");
						if(id.equals(BROKEN_FILE_NAME))
							sb.append("bledna nazwa pliku;\n");
					}
					else
					{
						endExtension = "done";
						console(Console.INFO, "Udalo sie zaimportowac plik "+file.getName()+" jako dokument od id "+id);
						done++;
						sb.append(file.getName()+";dodany;"+id+"\n");
					}
					
					FileUtils.copyFileToFile(file, new File(directory+File.separator+file.getName()+"."+endExtension));
					file.delete();
				}
				sb.append("\n");
				sb.append("Zaimportowano :;"+done+"\n");
				sb.append("Nie udalo sie zaimportowac :;"+broken+"\n");
				sb.append("Plikow w imporcie :;"+files.length+"\n");
				
				OutputStream os = new FileOutputStream(new File(directory+File.separator+RAPORT_FILE_NAME));
				os.write(sb.toString().getBytes());
				os.close();
			}
			catch (Exception e) {
				log.error(new EdmException(e));
				console(Console.ERROR, "Blad w czasie pracy importu "+e.getMessage());
			}
			finally
			{
				synchronized(this)
				{
					isRunning = false;
				}
			}
		}
		
	}
	
	class DirProperty extends Property
	{
		public DirProperty()
        {
            super(SIMPLE, PERSISTENT, FileImportDriver.this, "dir", "Katalog z plikami", String.class);
        }

        protected Object getValueSpi()
        {
            return dir != null ? dir.getAbsolutePath() : null;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
        	if (object == null)
                return;
            File tmp = new File((String) object);
            if (!tmp.isDirectory())
                throw new ServiceException(tmp+" nie istnieje lub nie jest katalogiem");
            if (!tmp.canWrite())
                throw new ServiceException("Brak praw zapisu w katalogu "+tmp);
            dir = tmp;
        }
	}
	
	class PowerOnProperty extends Property
	{
		
		public PowerOnProperty()
		{
			super(SIMPLE, PERSISTENT, FileImportDriver.this, "powerOn", "Uruchomic", Boolean.class);
		}
		
		protected Boolean getValueSpi()
		{
			return powerOn;
		}
		
		protected void setValueSpi(Object object)
		{
			powerOn = Boolean.valueOf(object.toString());
		}
	}
	
	class ConsoleProperty extends Property
	{
		
		public ConsoleProperty()
		{
			super(SIMPLE, PERSISTENT, FileImportDriver.this, "console", "Konsola", Boolean.class);
		}
		
		protected Boolean getValueSpi()
		{
			return console;
		}
		
		protected void setValueSpi(Object object)
		{
			console = Boolean.valueOf(object.toString());
		}
	}
	
	//TODO: uodpornic na wile przecinkow
	class FileExtensionsProperty extends Property
	{
		public FileExtensionsProperty()
        {
            super(SIMPLE, PERSISTENT, FileImportDriver.this, "extensions", "Rodzaje plikow (rodzielone przecinkami)", String.class);
        }
		
		protected String getValueSpi()
		{
			String retString = "";
			for(String str : fileExtensions)
			{
				retString += str+",";
			}			
			return retString.length() > 0 ? retString.substring(0, retString.length()-1) : retString;
		}
		
		protected void setValueSpi(Object object)
		{
			fileExtensions.clear();
			if(object != null)
				for(String str : object.toString().split(","))
				{
					fileExtensions.add(str.trim());
				}
		}
		
	}
	
	class BoxNumberProperty extends Property
	{
		public BoxNumberProperty()
        {
            super(ENUMERATED, PERSISTENT, FileImportDriver.this, "boxNumber", "Nr. pudla", Long.class);
        }

        protected Object getValueSpi()
        {
            return boxNumber;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
        	//System.out.println("hej "+object.toString());
            boxNumber = object != null ? Long.parseLong(object.toString()) : null;
        }

        public Map getValueConstraints() throws ServiceException
        {
        	Map<Long, String> tmp = new HashMap<Long, String>();
        	try
        	{
	        	for(Box b : Box.list())
	        	{
	        		tmp.put(b.getId(), b.getName());
	        	}
        	}
        	catch (Exception e) {
        		throw new ServiceException("Bledny numer pudla");
			}
            return tmp;
        }
	}
	
	class ReportsProperty extends Property
	{
		public ReportsProperty()
        {
            super(ENUMERATED, PERSISTENT, FileImportDriver.this, "reports", "Zestawienie", String.class, REPORT);
        }

        protected Object getValueSpi()
        {
            return "";
        }

        protected void setValueSpi(Object object) throws ServiceException
        {}

        public Map getValueConstraints() throws ServiceException
        {
        	return getReports();
        }
	}
	
	class DateProperty extends Property
	{

		protected DateProperty() 
		{
			super(SIMPLE, PERSISTENT, FileImportDriver.this, "date", "Data dodania pisma", String.class, DATE);
		}
		
		protected Object getValueSpi()
        {
            return date != null ? DateUtils.formatJsDate(date) : date;
        }
		
		protected void setValueSpi(Object object) throws ServiceException
        {
			if(object != null && DateUtils.nullSafeParseJsDate(object.toString()) == null)
				throw new ServiceException("Zly format daty");
			date = object != null ? DateUtils.nullSafeParseJsDate(object.toString()) : null;
        }
	}
}
