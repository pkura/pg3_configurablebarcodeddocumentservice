package pl.compan.docusafe.parametrization.aegon;

import java.util.List;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.agent.Agent;

/** 
 * Klasa uwspolnia pewne elementy agenta dla AEGONa
 * @author wkutyla
 *
 */
public abstract class AegonAgent extends Agent
{
	  public boolean agentAllowed()
	  {
	    	return Docusafe.hasExtra("nationwide");
	  }
	  
	  protected void removeAssign(TaskSnapshot task) throws EdmException
	    {
	        // w pierwszej kolejno�ci dekretacja na stanowisko, jak nie ma �adnego to na dzia�, a jak
	        // nie ma �adnego dzia�u, to na ROOT_DIVISION
	        DSUser user = DSUser.findByUsername(task.getActivity_lastActivityUser());
	        DSDivision[] divisions = user.getDivisions();
	        String guid = null;
	        String divisionGuid = null;
	        for (DSDivision division : divisions)
	        {
	            if (division.isPosition())
	            {
	                guid = division.getGuid();
	                break;
	            }   
	            else if (divisionGuid == null && division.isDivision())
	                divisionGuid = division.getGuid();                        
	        }
	        if (guid == null)
	        {
	            if (divisionGuid != null)
	                guid = divisionGuid;
	            else
	                guid = DSDivision.ROOT_GUID;
	        }
	        String plannedAssignment = guid+"/"+user.getName()+"/"+WorkflowFactory.wfManualName()+"//"+"realizacja";
	        
	        String []activityIds = {WorkflowFactory.activityIdOfWfNameAndKey(task.getWorkflowName(), task.getActivityKey())};
	        
	        WorkflowFactory.multiAssignment(activityIds,agentName, new String[] {plannedAssignment}, null, null);
	    }
	  
	  protected boolean canFinish(OfficeDocument doc, WorkflowActivity activity) throws EdmException
	    {

            if (activity.getDeadlineTime() != null)
            {
                List<Remark> remarks = ((OfficeDocument) doc).getRemarks();
                boolean found = false;
                for (Remark remark : remarks)
                {
                    if (remark.getCtime().getTime() > activity.getReceiveDate().getTime()
                        && remark.getAuthor().equals(DSApi.context().getPrincipalName()))
                    {
                        found = true;
                    }
                }

                if (!found)
                {
                    return false;
                }
            }
	        if (!"fax".equals(doc.getSource()))
	            if (!doc.hasWparamBits(Document.WPARAMBIT_NW_NEEDS_NOT_BOX)
	                && doc.getType() == DocumentType.INCOMING && !"fax".equals(doc.getSource())
	                && ((InOfficeDocument) doc).getKind() != null
	                && ((InOfficeDocument) doc).getKind().getName().indexOf("iznesowy") > 0
	                && doc.getBox() == null)
	            {
	                return false;
	            }
	        return true;
	    }
	  
	  protected boolean canStop()
	  {
	        return false;
	  }
}
