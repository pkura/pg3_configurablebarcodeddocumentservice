package pl.compan.docusafe.parametrization.aegon;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Timer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.texen.util.FileUtil;
import org.hibernate.HibernateException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.StringManager;

/**
 * @author Mariusz Kilja�czyk
 */
public class ICRDriver extends AegonAgent
{
	public StringManager sm = 
        GlobalPreferences.loadPropertiesFile("",null);
    private Timer timer;
    private static final Log log = LogFactory.getLog("agent_log");
        
    private String directory = null;
    private String tempDirectory = null;
    private static int FAILURE = 1;
    
    protected void setupAgent()
    {
    	directory = configuration.getProperty("icr.directory");
    	tempDirectory = configuration.getProperty("icr.directory");
    }

    public String getAgentCode()
    {
    	return "ICR";
    }
    
    public String getPropertyPrefix()
    {
    	return "icr.";
    }
    
    public boolean agentAllowed()
    {
    	return Docusafe.hasExtra("nationwide");
    }
    
    private ArrayList<File> generateFiles(Document doc) throws EdmException, IOException
    {
    	ArrayList<File> retList = new ArrayList<File>();
    	
    	FileUtil.mkdir(tempDirectory);
    	StringBuffer path = new StringBuffer(tempDirectory);
    	path.append(File.separator);
        path.append("Wnioski");
    	FileUtil.mkdir(path.toString());   
    	path.append(File.separator);
    	path.append(doc.getId());
    	path.append(".tif"); //Prawidlowa nazwa rozszerzenia to tif przez jedno f
    	retList.add(new File(path.toString()));
    	
    	return retList;
    }
    
    
    
    private Long findStatus(Long docId) throws SQLException, HibernateException, EdmException 
    {
    	
    	PreparedStatement ps = null;
    	try {
    		
    		ps = DSApi.context().prepareStatement("select status from ds_icr_document_status where id = ?");
    		ps.setLong(1, docId);
    		ResultSet rs = ps.executeQuery();
    		if (!rs.next())
    		{
    			return null;
    		} else {
    			return rs.getLong(1);
    		}
    	} finally {
    		DSApi.context().closeStatement(ps);    		
    	}
    }
    
    private void setStatus(Long docId, int status) throws HibernateException, SQLException, EdmException {
    	PreparedStatement ps = null;
    	try {
    		ps = DSApi.context().prepareStatement("insert into ds_icr_document_status (ID,STATUS) values(?,?)");
    		ps.setLong(1, docId);
    		ps.setInt(2, status);
    		ps.execute();
    	} finally {
    		DSApi.context().closeStatement(ps);    		
    	}
    }
    
    
    /** 
     * Metoda wykonuje jeden przebieg dla jednego zadania
     * @param task
     */
    private void doOne(TaskSnapshot task)
    {
    	//jesli commit = true to commit - jesli false to rollback
    	boolean commit = true;
    	OfficeDocument doc=null;
    	File file = null;            
    	
        
        
    	try
    	{
    		WorkflowFactory wff = WorkflowFactory.getInstance();
        	String activityId = wff.keyToId(task.getActivityKey());
        	WorkflowActivity activity = wff.getWfActivity(activityId);
        	
    		log.info("Przetwarzam dokument "+task.getDocumentId());
    		DSApi.context().begin();
    		
    		boolean doit = true;
    		try {
            	Long status = findStatus(task.getDocumentId());
            	if (status != null) {
            		log.error("ICR Proba ponownego przetwarzania tego samego dokumentu. Pozostawiam.");
            		doit = false;
            	} else 
            	{
            		setStatus(task.getDocumentId(), 1);
            	}
            } 
            catch (Exception e) 
            {
            	log.error("Blad w agencie ICR",e);                    	        
            
			}
            doc = (OfficeDocument) OfficeDocument.find(task.getDocumentId());
            
      
            if(doit && !task.isExternalWorkflow())
            { 
                log.trace("ICR Zadanie"+task.getActivityKey()+" Zwiazane z documentem "+doc.getId()+" nie znajduje sie w workflow");
                //if(doc.getDocumentKind()!=null && doc.getDocumentKind().getCn().equals(DocumentKind.NATIONWIDE_KIND))

                if (wff.isManual(activityId))
                {
                    //Jesli dokument nie jest documentem biznesowym oraz jest do realizacji
                    //zwracamy dekretacje
                    log.trace("ICR Zadanie nie dotyczy dokumentu nationwide i jest w obiegu recznym. Zwracam dekretacje");
                    removeAssign(task);
                }
                else
                {
                    doc.addAssignmentHistoryEntry(new AssignmentHistoryEntry(
                        "AGENT ICR", sm.getString("doWiadomosci"), AssignmentHistoryEntry.PROCESS_TYPE_CC));
                    wff.manualFinish(activityId, false);
                    log.trace("ICR Zadanie nie dotyczy dokumentu nationwide i jest do wiadomosci. Koncze prace");
                }
                TaskSnapshot.updateAllTasksByDocumentId(task.getDocumentId(),task.getDocumentType());
                log.trace("ICR Aktualizuje liste zadan");
            }
            
    		
    	}
    	catch(java.lang.IllegalArgumentException e)
    	{
            try
            {	
               		log.error("ICR blad pliku:  "+file.getPath()+" "+e.getMessage(), e);
            }
            catch (Exception e11)
            {
               		log.error("ICR blad pliku "+e.getMessage(),e);
            }           
            try 
            {
   					doc.addRemark(new Remark("Blad pliku: "+e.getMessage(),agentName));
   					removeAssign(task);
   		    } 
            catch (EdmException e1) 
   		    {
   					log.error("Blad icr",e1);
   		    }
   			
               	
        } catch (Exception e)
        {           	                         
            log.error("ICR"+e);
        }
    	finally
    	{
    		try
    		{
    			DSApi.context().commit();
    		}
    		catch (Exception e1)
    		{
    			log.error("Blad w w czasie zatwierdzania transakcji",e1);
    			DSApi._close();
    			try { DSApi.openAdmin(); } catch (Exception ex) { log.error("Blad",ex); }     			
    		}
    	}
    }
    public void run()
    {
         log.info("ICR Agent sprawdza liste zadan");
         TaskSnapshot.Query query;
         query = new TaskSnapshot.Query();
         TaskSnapshot task =null;
         
         try
         {
                DSApi.openAdmin();
                
                query.setUser(DSUser.findByUsername(agentName));
                SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
                
                log.trace("ICR Na liscie Agenta znajduje sie "+ results.count()+ " zadan");
                setupAgent();
                while (results.hasNext())
                {
                    task = results.next();
                    task.toFullTask(true);                    
                    doOne(task);                                                                               
                } 
                
         }          
         catch (Exception e)
         {
        	 log.error("ICR "+e.getStackTrace());
         }
         finally
         {
            	DSApi._close();
         }
      }   
    

}
