package pl.compan.docusafe.parametrization.aegon.overtime;

import java.util.Date;
import java.util.List;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import java.util.HashMap;
import java.util.Map;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import static pl.compan.docusafe.parametrization.aegon.overtime.OvertimeCn.*;

/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class BaseOvertimeLogic extends AbstractDocumentLogic
{
    private static final Logger log = LoggerFactory.getLogger(BaseOvertimeLogic.class);
    /**
     * Ustawia finalna akceptacje
     * @param fm
     * @param document
     */
    public void setFinalAcceptance(FieldsManager fm,Document document) throws EdmException
    {
        setFinalAcceptance(fm, document, true);
    }
    
    /**
     * Ustawia finalna akceptacje
     * @param fm
     * @param document
     */
    public void setFinalAcceptance(FieldsManager fm,Document document, boolean finalAcceptance) throws EdmException
    {
        Map<String,Object> values = new HashMap<String, Object>();
        values.put(FINAL_ACCEPTANCE_FIELD_CN, finalAcceptance);
        
        fm.reloadValues(values);
        fm.getDocumentKind().setOnly(document.getId(), values);
        
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
    }

    @Override
    public void documentPermissions(Document document) throws EdmException
    {
    }
    
    /**
     * Dodaje uzytkownikow jako szefow dzialu do mapy akceptacji
     * 
     * @param acceptancesMap
     * @throws EdmException
     */
    protected void addDivisionBosses(Map<String, String> acceptancesMap, String documentAuthor) throws EdmException
    {
        // wybranie szefow dzialu dla uzytkownika
        List<AcceptanceCondition> conditions = AcceptanceCondition.find(SZEF_DZIALU, documentAuthor);
        for (AcceptanceCondition cond : conditions)
        {
                try
                {
                        acceptancesMap.put(cond.getUsername(), DSUser.findByUsername(cond.getUsername()).asLastnameFirstname());
                }
                catch (UserNotFoundException ex)
                {
                        if (cond.getUsername().startsWith("ext:"))
                                acceptancesMap.put(cond.getUsername(), "U�ytk. zewn.: " + cond.getUsername().substring(4));
                        log.warn(ex.getMessage(), ex);
                }
        }	
    }
    
    /**
     * Metoda zmienia status wniosku w zaleznosci od obecnego statusu
     * @param document
     * @throws EdmException 
     */
    @Override
    public void onDecretation(Document document) throws EdmException
    {
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        Map<String, Object> values = new HashMap<String, Object>();

        //Przelozony kieruje wniosek do realizacji
        if(STATUS_ZAREJESTROWANY.equals(fm.getKey(STATUS_FIELD_CN)) || STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN)))
            values.put(STATUS_FIELD_CN,STATUS_REALIZOWANY);
        
        //Pracownik odzuca z�oszenie nadgodzin
        if(STATUS_REALIZOWANY.equals(fm.getKey(STATUS_FIELD_CN)))
            values.put(STATUS_FIELD_CN, STATUS_ODRZUCONY);
        
        fm.reloadValues(values);
        fm.getDocumentKind().setOnly(fm.getDocumentId(), values);
    }
    
    public static boolean isZAREJESTROWANY(Integer status)
    {
        if(STATUS_ZAREJESTROWANY.equals(status))
            return true;
        else
            return false;
    }
    
    public static boolean isZAAKCEPTOWANY(Integer status)
    {
        if(STATUS_ZAAKCEPTOWANY.equals(status))
            return true;
        else
            return false;
    }
    
    public static boolean isODRZUCONY(Integer status)
    {
        if(STATUS_ODRZUCONY.equals(status))
            return true;
        else
            return false;
    }
    
    public static boolean isREALIZOWANY(Integer status)
    {
        if(STATUS_REALIZOWANY.equals(status))
            return true;
        else
            return false;
    }
    
    public static boolean isANULOWANY(Integer status)
    {
        if(STATUS_ANULOWANY.equals(status))
            return true;
        else
            return false;
    }
    
     public Date checkDocDate(Object docDate)
    {                
        if(docDate == null)
            return new Date();
        else
            return (Date) docDate;       
    }
}
