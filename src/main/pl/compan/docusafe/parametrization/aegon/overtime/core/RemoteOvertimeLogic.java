package pl.compan.docusafe.parametrization.aegon.overtime.core;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import org.apache.axis2.addressing.EndpointReference;
import java.util.Date;
import java.util.List;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.absences.CardServiceStub;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.CardRequest;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.CardResponse;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.GetCard;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.GetCardResponse;
import pl.compan.docusafe.ws.client.overtimes.OvertimeServiceStub;
import pl.compan.docusafe.ws.client.overtimes.OvertimeServiceStub.DayIsFreeRequest;
import pl.compan.docusafe.ws.client.overtimes.OvertimeServiceStub.DayIsFreeResponse;
import pl.compan.docusafe.ws.client.overtimes.OvertimeServiceStub.GetIsFreeDay;
import pl.compan.docusafe.ws.client.overtimes.OvertimeServiceStub.GetIsFreeDayResponse;

/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
class RemoteOvertimeLogic extends AbstractOvertimeLogic
{

    private static final Logger log = LoggerFactory.getLogger(RemoteOvertimeLogic.class.getPackage().getName());

    public RemoteOvertimeLogic()
    {
    }
    
    @Override
    public EmployeeCard getActiveEmployeeCardByUserName(String userName) throws EdmException
    {
        try
        {
            // pobranie karty pracownika z webservicu
            CardServiceStub cardServiceStub = new CardServiceStub();
            cardServiceStub._getServiceClient().getOptions().setTo(
                    new EndpointReference(Docusafe.getAdditionProperty("cardService.endPointReference")));

            CardRequest cardRequest = new CardRequest();
            cardRequest.setUserName(userName);

            GetCard getCard = new GetCard();
            getCard.setRequest(cardRequest);

            GetCardResponse cardResponse = cardServiceStub.getCard(getCard);
            CardResponse response = cardResponse.get_return();

            if (response.getStatus() == -1)
            {
                throw new EdmException(response.getError());
            }

            EmployeeCard card = new EmployeeCard();
            card.setId(response.getId());
            card.setKPX(response.getKPX());
            card.setCompany(response.getCompany());
            card.setDepartment(response.getDepartment());
            card.setDivision(response.getDivision());

            if (response.getEmploymentStartDate() != null)
            {
                card.setEmploymentStartDate(response.getEmploymentStartDate().getTime());
            }

            if (response.getEmploymentEndDate() != null)
            {
                card.setEmploymentEndDate(response.getEmploymentEndDate().getTime());
            }

            card.setExternalUser(response.getExternalUser());
            card.setInfo(response.getInfo());
            card.setInvalidAbsenceDaysNum(response.getInvalidAbsenceDaysNum());
            card.setPosition(response.getPosition());
            card.setT188KP(response.getT188KP());
            card.setT188kpDaysNum(response.getT188KpDaysNum());
            card.setTINW(response.getTINW());
            card.setTUJ(response.getTUJ());
            card.setTujDaysNum(response.getTujDaysNum());
            //card.setUser(userImpl);

            return card;
        } catch (Exception ex)
        {
            log.error(ex.getMessage(), ex);
            throw new EdmException(ex.getMessage());
        }
    }

    @Override
    public boolean isFreeDay(Date date) throws EdmException
    {
        try
        {
            OvertimeServiceStub overtimeServiceStub = new OvertimeServiceStub();
            overtimeServiceStub._getServiceClient().getOptions().setTo(
                    new EndpointReference(Docusafe.getAdditionProperty("overtimeService.endPointReference")));

            DayIsFreeRequest freeDayRequest = new DayIsFreeRequest();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            
            freeDayRequest.setDate(calendar);

            GetIsFreeDay isFreeDay = new OvertimeServiceStub.GetIsFreeDay();
            isFreeDay.setRequest(freeDayRequest);

            GetIsFreeDayResponse freeDayResponse = overtimeServiceStub.getIsFreeDay(isFreeDay);
            DayIsFreeResponse response = freeDayResponse.get_return();

            if(!response.getIsFree())
                throw new EdmException("Ten dzie� znajduje si� w kalendarzu dni wolnych!");
            
            return true;
        } catch (Exception ex)
        {
            log.error(ex.getMessage(), ex);
            throw new EdmException(ex.getMessage());
        }
    }

    @Override
    public List<EmployeeOvertimeEntity> getEmployeeOvertimes(EmployeeCard empCard, int year) throws EdmException
    {
        try
        {
            OvertimeServiceStub overtimeServiceStub = new OvertimeServiceStub();
            overtimeServiceStub._getServiceClient().getOptions().setTo(
                    new EndpointReference(Docusafe.getAdditionProperty("overtimeService.endPointReference")));
            
            //request z danymi
            OvertimeServiceStub.YearOvertimeRequest request = new OvertimeServiceStub.YearOvertimeRequest();
            request.setEmpCardId(empCard.getId());
            request.setYear(year);
            
            //ustawiam do odpowiedz daneg do funkcji
            OvertimeServiceStub.GetEmployeeOvertimes getEmployeeOvertimes = new OvertimeServiceStub.GetEmployeeOvertimes();
            getEmployeeOvertimes.setRequest(request);
            
            //wywolanie i odpowiedz
            OvertimeServiceStub.GetEmployeeOvertimesResponse getEmployeeOveritmes = overtimeServiceStub.getEmployeeOvertimes(getEmployeeOvertimes);
            OvertimeServiceStub.YearOvertimeResponse response = getEmployeeOveritmes.get_return();
            
            return copyFromServiceList(response.getEmpOvertimes());
            
        } catch (Exception ex)
        {
            log.error(ex.getMessage(), ex);
            throw new EdmException(ex.getMessage());
        }
    }

    @Override
    public List<EmployeeOvertimeMonthlyEntity> getMonthEmployeeOvertimes(EmployeeCard empCard, int year, int month) throws EdmException
    {
         try
        {
            OvertimeServiceStub overtimeServiceStub = new OvertimeServiceStub();
            overtimeServiceStub._getServiceClient().getOptions().setTo(
                    new EndpointReference(Docusafe.getAdditionProperty("overtimeService.endPointReference")));
            
            //request z danymi
            OvertimeServiceStub.MonthOvertimeRequest request = new OvertimeServiceStub.MonthOvertimeRequest();
            request.setEmpCardId(empCard.getId());
            request.setYear(year);
            request.setMonth(month);
            
            //ustawiam do odpowiedz dane do funkcji
            OvertimeServiceStub.GetMonthEmployeeOvertimes getEmployeeOvertimes = new OvertimeServiceStub.GetMonthEmployeeOvertimes();
            getEmployeeOvertimes.setRequest(request);
            
            //wywolanie i odpowiedz
            OvertimeServiceStub.GetMonthEmployeeOvertimesResponse getEmployeeOveritmes = overtimeServiceStub.getMonthEmployeeOvertimes(getEmployeeOvertimes);
            OvertimeServiceStub.MonthOvertimeResponse response = getEmployeeOveritmes.get_return();
            
            return copyFromServiceList(response.getMonthOvertimes());
            
        } catch (Exception ex)
        {
            log.error(ex.getMessage(), ex);
            throw new EdmException(ex.getMessage());
        }
    }
        
    @Override
    public void chargeOvertimes(EmployeeCard empCard, Long documentId, String description, Date date, String from, String to, String count, Boolean receive) throws EdmException
    {
        try
        {
            OvertimeServiceStub overtimeServiceStub = new OvertimeServiceStub();
            overtimeServiceStub._getServiceClient().getOptions().setTo(
                    new EndpointReference(Docusafe.getAdditionProperty("overtimeService.endPointReference")));

            OvertimeServiceStub.ChargeOvertimeRequest overtimeRequest = new OvertimeServiceStub.ChargeOvertimeRequest();
            overtimeRequest.setEmpCardId(empCard.getId());
            overtimeRequest.setDocumentId(documentId);
            overtimeRequest.setDescription(description);
            
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            overtimeRequest.setDate(calendar);
            
            overtimeRequest.setFrom(from);
            overtimeRequest.setTo(to);
            overtimeRequest.setCount(count);
            overtimeRequest.setOvertimeReceive(receive);

            OvertimeServiceStub.ChargeOvertimes chargeOvertimes = new OvertimeServiceStub.ChargeOvertimes();
            chargeOvertimes.setRequest(overtimeRequest);

            OvertimeServiceStub.ChargeOvertimesResponse stubCharges = overtimeServiceStub.chargeOvertimes(chargeOvertimes);
            OvertimeServiceStub.ChargeOvertimeResponse response = stubCharges.get_return();

            if (response.getStatus() == -1)
            {
                throw new EdmException(response.getError());
            }


        } catch (Exception ex)
        {
            log.error(ex.getMessage(), ex);
            throw new EdmException(ex.getMessage());
        }
    }

    @Override
    public void unchargeOvertimes(EmployeeCard empCard, Long documentId) throws EdmException
    {
        try
        {
            OvertimeServiceStub overtimeServiceStub = new OvertimeServiceStub();
            overtimeServiceStub._getServiceClient().getOptions().setTo(
                    new EndpointReference(Docusafe.getAdditionProperty("overtimeService.endPointReference")));

            OvertimeServiceStub.UnchargeOvertimeRequest overtimeRequest = new OvertimeServiceStub.UnchargeOvertimeRequest();
            overtimeRequest.setEmpCardId(empCard.getId());
            overtimeRequest.setDocumentId(documentId);

            OvertimeServiceStub.UnchargeOvertimes chargeOvertimes = new OvertimeServiceStub.UnchargeOvertimes();
            chargeOvertimes.setRequest(overtimeRequest);

            OvertimeServiceStub.UnchargeOvertimesResponse stubCharges = overtimeServiceStub.unchargeOvertimes(chargeOvertimes);
            OvertimeServiceStub.UnchargeOvertimeResponse response = stubCharges.get_return();

            if (response.getStatus() == -1)
            {
                throw new EdmException(response.getError());
            }

        } catch (Exception ex)
        {
            log.error(ex.getMessage(), ex);
            throw new EdmException(ex.getMessage());
        }
    }

    @Override
    public File pdfCard(String username, int year) throws EdmException, Exception
    {
        //month jest nie potrzebne karta jest roczna
        return getReport(username, year, 0, 1);
    }

    @Override
    public File xlsCard(String username, int year) throws EdmException, Exception
    {
        //month jest nie potrzebne karta jest roczna
        return getReport(username, year, 0, 2);
    }

    @Override
    public File pdfMonthlyCard(String username, int year, int month) throws EdmException, Exception
    {
        return getReport(username, year, month, 3);
    }

    @Override
    public File xlsMonthlyCard(String username, int year, int month) throws EdmException, Exception
    {
        return getReport(username, year, month, 4);
    }

    
    public File getReport(String username, int year, int month, int type) throws EdmException, Exception
    {
        try
        {
            OvertimeServiceStub overtimeServiceStub = new OvertimeServiceStub();
            overtimeServiceStub._getServiceClient().getOptions().setTo(
                    new EndpointReference(Docusafe.getAdditionProperty("overtimeService.endPointReference")));
            
            OvertimeServiceStub.ReportRequest reportRequest = new OvertimeServiceStub.ReportRequest();
            reportRequest.setUsername(username);
            reportRequest.setType(type);
            reportRequest.setYear(year);
            reportRequest.setMonth(month);
            
            OvertimeServiceStub.GetReport getReport = new OvertimeServiceStub.GetReport();
            getReport.setRequest(reportRequest);
            
            //wywolanie i odpowiedz
            OvertimeServiceStub.GetReportResponse getReportResponse = overtimeServiceStub.getReport(getReport);
            OvertimeServiceStub.ReportResponse response = getReportResponse.get_return();
            
            File file = FileUtils.decodeByBase64ToFile(response.getCardReport());
            
            return file;
        } catch (Exception ex)
        {
            log.error(ex.getMessage(), ex);
            throw new EdmException(ex.getMessage());
        }
    }
    
        private List<EmployeeOvertimeEntity> copyFromServiceList(OvertimeServiceStub.EmployeeOvertimeEntity[] empOvertimes)
    {
        List<EmployeeOvertimeEntity> overtimes = new ArrayList<EmployeeOvertimeEntity>();
        
        for(OvertimeServiceStub.EmployeeOvertimeEntity entity: empOvertimes)
        {
            EmployeeOvertimeEntity tmp = new EmployeeOvertimeEntity();
            tmp.setLeftHour(entity.getLeftHour());
            tmp.setLeftMinute(entity.getLeftMinute());
            tmp.setMonth(entity.getMonth());
            tmp.setOvertimeMonth(entity.getOvertimeMonth());
            tmp.setSumHour(entity.getSumHour());
            tmp.setSumHourReceive(entity.getSumHourReceive());
            tmp.setSumMinute(entity.getSumMinute());
            tmp.setSumMinuteReceive(entity.getSumMinuteReceive());
            tmp.init();
            
            overtimes.add(tmp);
        }
        
        return overtimes;
    }

    private List<EmployeeOvertimeMonthlyEntity> copyFromServiceList(OvertimeServiceStub.OvertimeMonthlyWSEntity[] monthOvertimes)
    {
        List<EmployeeOvertimeMonthlyEntity> overtimes = new ArrayList<EmployeeOvertimeMonthlyEntity>();
        
        for(OvertimeServiceStub.OvertimeMonthlyWSEntity entity: monthOvertimes)
        {
                
            EmployeeOvertimeMonthlyEntity tmp = new EmployeeOvertimeMonthlyEntity(entity.getDate().getTime(), entity.getOvertimes(), entity.getDescription(), entity.getDocumentId(), entity.getMonth());
            if(entity.getEmpOvertimeReceive() != null)
            {
                tmp.setEmpOvertimeReceive(copyFromSubList(entity.getEmpOvertimeReceive()));
            }
            tmp.setFirstDateReceive(entity.getFirstDateReceive());
            tmp.setFirstOvertimesReceive(entity.getFirstOvertimesReceive());
            tmp.setFirstElements();
            
            overtimes.add(tmp);
           
        }
        
        return overtimes;
    }

    /**
     * Funkcja konerwertuje dane z tablicy z ws do listy
     */
    private List<EmployeeOvertimeReceiveMonthlyEntity> copyFromSubList(OvertimeServiceStub.EmployeeOvertimeReceiveMonthlyEntity[] empOvertimeReceive)
    {
        List<EmployeeOvertimeReceiveMonthlyEntity> overtimes = new ArrayList<EmployeeOvertimeReceiveMonthlyEntity>();
        
        for( OvertimeServiceStub.EmployeeOvertimeReceiveMonthlyEntity entity : empOvertimeReceive)
        {
            EmployeeOvertimeReceiveMonthlyEntity tmp = new EmployeeOvertimeReceiveMonthlyEntity(entity.getDate(), entity.getOvertimesReceive(), entity.getMonth());
            String[] overtime = tmp.getOvertimesReceive().split(":");
            
            if(overtime.length >1)
            {
                tmp.hours = Integer.parseInt(overtime[0]);
                tmp.minutes = Integer.parseInt(overtime[1]);
            }
            overtimes.add(tmp); 
        }
        
        return overtimes;
        
    }
}
