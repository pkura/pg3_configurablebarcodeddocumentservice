package pl.compan.docusafe.parametrization.aegon.overtime.core;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TimeUtils;

/**
 * Przechowuje miesieczn� list� nadgodzin u�ytkownika z wybranego roku
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class EmployeeOvertimesMonthlyBean
{
    private static final Logger log = LoggerFactory.getLogger(EmployeeOvertimesMonthlyBean.class);
    
    private int month;
    private int year;
    private EmployeeCard empCard;
    /** Przechowuje wszystkie miesiace z wybranego roku*/
    private List<EmployeeOvertimeMonthlyEntity> monthlyOvertimes;
    /** zgloszone nadgodziny*/
    private String overtimes;
    /** odebrane nadgodziny*/
    private String overtimesReceives;

    public EmployeeOvertimesMonthlyBean()
    {
    }

    /**
     * Wywoluje @see getEmployeeMonthlyOvertimes z tej klasy;
     * @param empCard
     * @param year
     * @param month 
     */
    public EmployeeOvertimesMonthlyBean( EmployeeCard empCard, int year, int month) throws EdmException
    {
        this.empCard = empCard;
        this.year = year;
        this.month = month;
       // this.getEmployeeMonthlyOvertimes(this.empCard, this.year, this.month);
    }
    
    /** Pobiera z bazy wszystkie potrzebne miesi�ce*/
    public void getEmployeeMonthlyOvertimes() throws EdmException
    {
        try{
            this.monthlyOvertimes = AbstractOvertimeLogic.getInstance().getMonthEmployeeOvertimes(empCard, year, month);
        } catch (Exception ex)
        {
            throw new EdmException("Nie uda�o si� wykona� zestawienia miesi�cznego");
        }
    }
    /**Oblicza sumy calego miesiace, zgloszone i odebrane nadgodziny  */
    public void init()
    {
        int hour = 0;
        int minute = 0;
        int rHour = 0;
        int rMinute = 0;
                
        Iterator it = monthlyOvertimes.iterator();
        EmployeeOvertimeMonthlyEntity ot;
        EmployeeOvertimeReceiveMonthlyEntity overtimeReceive;
        while (it.hasNext())
        {
            ot = (EmployeeOvertimeMonthlyEntity) it.next();
            if (ot.getMonth() != month)
            {
                it.remove();
                continue;
            } else
            {
                hour += ot.hours;
                minute += ot.minutes;
                String firstOvertime = ot.getFirstOvertimesReceive();
                if (firstOvertime != null)
                {
                    String[] arrayOvertimes = firstOvertime.split("\\:", 0);
                    if (arrayOvertimes[0] != null)
                    {
                        rHour += Integer.parseInt(arrayOvertimes[0]);
                    }
                    if (arrayOvertimes[1] != null)
                    {
                        rMinute += Integer.parseInt(arrayOvertimes[1]);
                    }
                }
                Iterator rt = ot.getEmpOvertimeReceive().iterator();

                while (rt.hasNext())
                {
                    overtimeReceive = (EmployeeOvertimeReceiveMonthlyEntity) rt.next();
                    rHour += overtimeReceive.hours;
                    rMinute += overtimeReceive.minutes;
                }
            }
        }
        overtimes = TimeUtils.parseMinuteToHours(hour, minute);
        overtimesReceives = TimeUtils.parseMinuteToHours(rHour, rMinute);
    }

    public EmployeeCard getEmpCard()
    {
        return empCard;
    }

    public void setEmpCard(EmployeeCard empCard)
    {
        this.empCard = empCard;
    }

    public int getMonth()
    {
        return month;
    }

    public void setMonth(int month)
    {
        this.month = month;
    }

    public List<EmployeeOvertimeMonthlyEntity> getMonthlyOvertimes()
    {
        return monthlyOvertimes;
    }

    public void setMonthlyOvertimes(List<EmployeeOvertimeMonthlyEntity> monthlyOvertimes)
    {
        this.monthlyOvertimes = monthlyOvertimes;
    }

    public String getOvertimes()
    {
        return overtimes;
    }

    public void setOvertimes(String overtimes)
    {
        this.overtimes = overtimes;
    }

    public String getOvertimesReceives()
    {
        return overtimesReceives;
    }

    public void setOvertimesReceives(String overtimesReceives)
    {
        this.overtimesReceives = overtimesReceives;
    }

    public int getYear()
    {
        return year;
    }

    public void setYear(int year)
    {
        this.year = year;
    }
    
    
}
