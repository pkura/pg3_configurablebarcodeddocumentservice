package pl.compan.docusafe.parametrization.aegon.overtime.core;

import java.io.File;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class LocalOvertimeLogic extends AbstractOvertimeLogic
{
    private static final Logger log = LoggerFactory.getLogger(AbstractOvertimeLogic.class);
    @Override
    public EmployeeCard getActiveEmployeeCardByUserName(String userName) throws EdmException
    {
        /** @XXX U�ywam istniej�cego zapytania, ewentualnie zrobi� DAO dla EmployeeCard*/
        return AbsenceFactory.getActiveEmployeeCardByUserName(userName);
    }

    /**
     * 
     * @param empCard
     * @param year
     * @return List<EmployeeOvertimeEntity> 
     */
    @Override
    public List<EmployeeOvertimeEntity> getEmployeeOvertimes(EmployeeCard empCard, int year) throws EdmException
    {
        return OvertimeFactory.getEmployeeOvertimes(empCard, year);
    }

    @Override
    public List<EmployeeOvertimeMonthlyEntity> getMonthEmployeeOvertimes(EmployeeCard empCard, int year, int month) throws EdmException
    {
        try
        {
            return OvertimeFactory.getEmployeeMonthlyOvertimes(empCard, year, month);
            
        } catch (SQLException ex)
        {
            throw new EdmException(ex);
        }
    }
   
    @Override
    public void chargeOvertimes(EmployeeCard empCard, Long documentId, String description, Date date, String from, String to, String count, Boolean receive) throws EdmException
    {
        OvertimeFactory.chargeOvertimes(empCard, documentId, date, from, to, count, description, receive);
    }

    @Override
    public void unchargeOvertimes(EmployeeCard empCard, Long documentId) throws EdmException
    {
       OvertimeFactory.unchargeOvertimes(empCard, documentId);
    }

    @Override
    public boolean isFreeDay(Date date) throws EdmException
    {
        try{
            OvertimeFactory.isFreeDay(date);
        } catch (EdmException ex)
        {
            return false;
        }
        
        return true;
    }

    @Override
    public File pdfCard(String username, int year) throws EdmException, Exception
    {
        return OvertimeRaportFactory.pdfCard(username, year);
    }

    @Override
    public File xlsCard(String username, int year) throws EdmException, Exception
    {
        return OvertimeRaportFactory.xlsCard(username, year);
    }

    @Override
    public File pdfMonthlyCard(String username, int year, int month) throws EdmException, Exception
    {
        return OvertimeRaportFactory.pdfMonthlyCard(username, year, month);
    }

    @Override
    public File xlsMonthlyCard(String username, int year, int month) throws EdmException, Exception
    {
        return OvertimeRaportFactory.xlsMonthlyCard(username, year, month);
    }
    
    
    
}
