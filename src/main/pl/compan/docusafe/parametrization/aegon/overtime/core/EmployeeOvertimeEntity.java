package pl.compan.docusafe.parametrization.aegon.overtime.core;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TimeUtils;

/**
 * Obiekt jest wykorzystywany do generowania listy nadgodzin
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class EmployeeOvertimeEntity
{
    private static final Logger log = LoggerFactory.getLogger(EmployeeOvertimeEntity.class);
    private int month;
    
    private String overtimeMonth;
    private String sumHour;
    private String sumMinute;
    private String sumHourReceive;
    private String sumMinuteReceive;
    private String leftHour;
    private String leftMinute;
    
    public static final int[] daysInMonths = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}; 

    public EmployeeOvertimeEntity()
    {
    }

    
    public EmployeeOvertimeEntity(int month, String overtimeMonth, String sumHour, String sumMinute)
    {
        this.month = month;
        setOvertimeMonthFromCalendar(overtimeMonth);
        this.sumHour = sumHour;
        this.sumMinute = sumMinute;
        this.sumMinuteReceive = "00";
        this.sumHourReceive = "00";
    }
    
    public int getMonth()
    {
        return month;
    }

    public void setMonth(int month)
    {
        this.month = month;
    }
    
    /** Zwraca nazwe miesiaca*/
    public String getOvertimeMonth()
    {
        
        return overtimeMonth;
    }
    
    /**
     * sprawdzanie poprawno�ci obiektu
     */
    public void init()
    {
        
        this.sumHour = TimeUtils.parseSmallValue(Integer.parseInt(this.sumHour));
        this.sumMinute = TimeUtils.parseSmallValue(Integer.parseInt(this.sumMinute));
        this.sumHourReceive = TimeUtils.parseSmallValue(Integer.parseInt(this.sumHourReceive));
        this.sumMinuteReceive = TimeUtils.parseSmallValue(Integer.parseInt(this.sumMinuteReceive));
        this.countLeftHour();
        
        this.leftHour = TimeUtils.parseSmallValue(Integer.parseInt(this.leftHour));
        this.leftMinute = TimeUtils.parseSmallValue(Integer.parseInt(this.leftMinute));
    }
    
    /**
     * Oblicza godziny jakie pozostaly
     */
    private void countLeftHour()
    {
        String hours = this.sumHour + ":" + this.sumMinute;
        String hoursReceive = this.sumHourReceive + ":" + this.sumMinuteReceive;
        String hourLeft = TimeUtils.substractTimes(hours, hoursReceive);
        String[] hourLeftArray = hourLeft.split(":");
        
        this.leftHour = hourLeftArray[0];
        this.leftMinute = hourLeftArray[1];
    }

    /**
     * 
     */
//    private void countLeftMinute()
//    {
//       int localMinute = Integer.parseInt(this.sumMinute);
//       int localMinuteReceive = Integer.parseInt(this.sumMinuteReceive);
//       
//       int localLeftMinute = localMinute - localMinuteReceive;
//       
//       if(localLeftMinute < 0) //oblicza minuty i ewentualnie inkrementuje godziny jesli przekroczy 60
//       {
//           int localHour = Integer.parseInt(this.leftHour);
//           localLeftMinute = 60 + localLeftMinute;
//           this.leftHour = new Integer((localHour-1)).toString();
//       }
//       
//       this.leftMinute = new Integer(localLeftMinute).toString();
//       
//    }
    
    public void setOvertimeMonth(String overtimeMonth)
    {
        this.overtimeMonth = overtimeMonth;
    }
    
    /**
     * Ustawia nazwe miesiaca
     * @param overtimeMonth 
     */
    private void setOvertimeMonthFromCalendar(String overtimeMonth)
    {
        this.overtimeMonth = DateUtils.getMonthName(this.month - 1);
    }

    /** Zwraca godziny zg�oszone*/
    public String getSumHour()
    {
        return sumHour;
    }

    public void setSumHour(String sumHour)
    {
        this.sumHour = sumHour;
    }

    /** Zwrca godziny odebrane*/
    public String getSumHourReceive()
    {
        return sumHourReceive;
    }

    public void setSumHourReceive(String sumHourReceive)
    {
        this.sumHourReceive = sumHourReceive;
    }

    /** Zwraca minuty odebrane*/
    public String getSumMinuteReceive()
    {
        return sumMinuteReceive;
    }

    public void setSumMinuteReceive(String sumMinutReceive)
    {
        this.sumMinuteReceive = sumMinutReceive;
    }
    
    /** zwraca wypracowane nadgodziny  w miesi�cu*/
    public String getWorked()
    {
        return sumHour + ":" + sumMinute;
    }
    
    /**
     * Zwraca odebrane nadgodziny w miesiacu
     */
    public String getReceivedWorked()
    {
        return sumHourReceive + ":" + sumMinuteReceive;
    }
    
    /** zwraca pozosta�e godziny */
    public String getLeftWorked()
    {
        return leftHour + ":" + leftMinute;
    }
    
    /** Zwrca minuty zgloszone*/
    public String getSumMinute()
    {
        return sumMinute;
    }

    public void setSumMinute(String sumMinute)
    {
        this.sumMinute = sumMinute;
    }

    /** zwraca godziny pozosta�e*/
    public String getLeftHour()
    {
        return leftHour;
    }

    
    public void setLeftHour(String countHour)
    {
        this.leftHour = countHour;
    }

    /** zwraca minuty pozosta�e*/
    public String getLeftMinute()
    {
        return leftMinute;
    }

    public void setLeftMinute(String countMinute)
    {
        this.leftMinute = countMinute;
    }
    
    public String getLastDayOfMonth()
    {
        GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("Europe/Warsaw"));
        int year = calendar.get(GregorianCalendar.YEAR);
        daysInMonths[1] += calendar.isLeapYear(year) ? 1 : 0; 
        int  day = new Integer(daysInMonths[this.month-1]);
        calendar.set(year, this.month-1, day);
        
        return DateUtils.commonDateFormat.format(calendar.getTime());
    }
    
    public String getFirstDayOfMonth()
    {
        GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("Europe/Warsaw"));
        int year = calendar.get(GregorianCalendar.YEAR);
        int  day = 1;
        calendar.set(year, this.month-1, day);
        
        return DateUtils.commonDateFormat.format(calendar.getTime());
    }
    
    
}
