package pl.compan.docusafe.parametrization.aegon.overtime;

import java.util.Calendar;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.base.absences.FreeDay;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.logic.Acceptable;
import com.opensymphony.webwork.ServletActionContext;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.RelatedDocumentBean;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WfActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.internal.WfActivityImpl;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.sql.DivisionImpl;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.parametrization.aegon.overtime.core.AbstractOvertimeLogic;
import pl.compan.docusafe.parametrization.aegon.overtime.core.EmployeeOvertimeEntity;
import pl.compan.docusafe.parametrization.aegon.overtime.core.EmployeeOvertimesSummary;
import pl.compan.docusafe.parametrization.aegon.overtime.core.OvertimeCard;
import pl.compan.docusafe.parametrization.aegon.overtime.core.OvertimeFactory;
import pl.compan.docusafe.parametrization.aegon.overtime.core.OvertimeRaportFactory;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TimeUtils;
import pl.compan.docusafe.web.common.form.UpdateUser;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import static pl.compan.docusafe.parametrization.aegon.overtime.OvertimeCn.*;
/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class OvertimeReceiveLogic extends BaseOvertimeLogic implements Acceptable
{
    private static final StringManager sm = StringManager.getManager(OvertimeReceiveLogic.class.getPackage().getName());
    private static final Logger log = LoggerFactory.getLogger(OvertimeReceiveLogic.class);
    private static AbstractOvertimeLogic instance;
    /**
     * Karta pracownika
     */
    private EmployeeCard empCard;
    private String userName;
    /**
     * Ca�kowita liczba nadgodzin
     */
    private String allHour;
    private String allHourReceive;
    private String allHourLeft;
    
    private List<EmployeeOvertimeEntity> empOvertimes;

    public OvertimeReceiveLogic()
    {
        //ustalenie typu logiki
        instance = AbstractOvertimeLogic.getInstance();
    }

    
    public static synchronized  AbstractOvertimeLogic getInstance()
    {
        if( instance == null )
            instance = AbstractOvertimeLogic.getInstance();
        return instance;
    }

    @Override
    public void onGiveAcceptance(Document document, String acceptanceCn) throws EdmException 
    {
        FieldsManager fm = document.getFieldsManager();

        if (STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN)) || STATUS_ANULOWANY.equals(fm.getKey(STATUS_FIELD_CN)))
        {
            return;
        }
        
        if(document.getAuthor().equals(DSApi.context().getPrincipalName()))
            return;

        //validateData(document);

        Map<String, Object> values = new HashMap<String, Object>();

        if (acceptanceCn.equals(SZEF_DZIALU))
        {
            updateEmployeeOvertimeCard(fm, document);
        }

        values.put(STATUS_FIELD_CN, STATUS_ZAAKCEPTOWANY);
        fm.reloadValues(values);
        fm.getDocumentKind().setOnly(fm.getDocumentId(), values);
        
        setDocumentDescription(document);
    }

    @Override
    public boolean canAcceptDocument(FieldsManager fm, String acceptanceCn)   throws EdmException 
    {
            Long documentAuthorId = (Long) fm.getValue(USER_CN);
            String userAuthor = UserImpl.find(documentAuthorId).getName();
            String principalName = DSApi.context().getPrincipalName();
            
            if (acceptanceCn.equals(SZEF_DZIALU))
            {
                    // sprawdza czy zalogowany uzytkownik jest przelozonym dla wnioskodowacy
                    List<AcceptanceCondition> conditions = null;
                    if (( AvailabilityManager.isAvailable("aegon.docusafe.remote")) || (!AvailabilityManager.isAvailable("aegon.docusafe.remote")))
                    {
                            conditions = AcceptanceCondition.find(SZEF_DZIALU, userAuthor);
                    }
                    else
                    {
                            conditions = AcceptanceCondition.find(SZEF_DZIALU, "ext:"+userAuthor);
                    }
                    
                    for (AcceptanceCondition condition : conditions)
                    {
                            if (condition.getUsername().equals(principalName))
                                    return true;
                    }
            }

            return false;
    }

    @Override
    public void onWithdrawAcceptance(Document document, String acceptanceCn) throws EdmException 
    {
            FieldsManager fm = document.getFieldsManager();
            Map<String, Object> values = new HashMap<String, Object>();

            // je�li cofniecie akceptacji wniosku AEGON przez szefa_dzialu
            if (acceptanceCn.equals(SZEF_DZIALU) && !STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN))
                            && !STATUS_ANULOWANY.equals(fm.getKey(STATUS_FIELD_CN)))
            {
                   deleteEmployeeOvertimeCard(document);
                    values.put(STATUS_FIELD_CN, STATUS_REALIZOWANY);
            }
            fm.reloadValues(values);
            fm.getDocumentKind().setOnly(fm.getDocumentId(), values);
            
            setDocumentDescription(document);
    }
    
    @Override
    public void onLoadData(FieldsManager fm) throws EdmException
    {               
    }
    
    @Override
    public void correctValues(Map<String, Object> values, DocumentKind kind) throws EdmException
    {            
        Date date = DateUtils.nullSafeParseJsDate((String)values.get(OVERTIME_DATE_CN));
        Date todayBack5 = DateUtils.plusDays(Calendar.getInstance().getTime(), OVERTIME_MAX_DATE);
        
        //rzuca wyj�tkiem
        instance.isFreeDay(date);
        
        if(!DateUtils.isGreaterDate(date, todayBack5 ))
            throw new EdmException("Mo�esz wype�nia� wniosek na 5 dni wstecz!");
        
        if(date == null)
            throw new EdmException("Pole Data nadgodzin jest obowi�zkowe!");
//        if(!DateUtils.isGreaterFromToday(date))
//            throw new EdmException("Nie mo�esz dodawa� z dat� wstecz!");
        values.remove(USER_CN);
        String timeFrom = (String)values.get(TIME_FROM_CN);
        String timeTo = (String)values.get(TIME_TO_CN);
        
        String timeCount = (String)values.get(TIME_COUNT_CN);
        
        if(!TimeUtils.isGreater(timeCount, "00:05"))
           throw new EdmException("Liczba nadogdzin jest zbyt ma�a");
        //sprawdzam walidacje godzin
        try{
            TimeUtils.checkTimeRange(timeFrom, timeTo);
        } catch (EdmException ex)
        {
            throw new EdmException(sm.getString(ex.getMessage()));
        }
        
        super.correctValues(values, kind);
    }
    
   /**
     * Metoda zmienia status wniosku w zaleznosci od obecnego statusu
     * @param document
     * @throws EdmException 
     */
    @Override
    public void onDecretation(Document document) throws EdmException
    {
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        Map<String, Object> values = new HashMap<String, Object>();
        
        //Pracownik kieruje wniosek do realizacji
        if(STATUS_ZAREJESTROWANY.equals(fm.getKey(STATUS_FIELD_CN)) || (STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN)) && isDocumentUserLogedIn(document.getAuthor())))
            values.put(STATUS_FIELD_CN,STATUS_REALIZOWANY);
        
//        
        fm.reloadValues(values);
        fm.getDocumentKind().setOnly(fm.getDocumentId(), values);
        
        setDocumentDescription(document);
    }
    
    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        DSUser author = DSUser.findByUsername(document.getAuthor()); 
        //laduje karte pracownika mo�e rzuci� bledem
        loadEmployeeCard(document.getAuthor());

        //sprawdzam czy zalogowany uzytkownik jest wlascicielem dokumentu jesli tak to sprawdza czy ma wystarczajaca ilosc nadgodzin
        if(DSApi.context().getPrincipalName().equals(document.getAuthor()) )
            hasOvertimesReceive(document, fm);
       
       if( document.getDocumentKind() == null )
            throw new NullPointerException("document.getDocumentKind()");
        
        Calendar cal =  DateUtils._GREGORIAN_CALENDAR;        
        Folder rootFolder = Folder.getRootFolder();
        Object docDate = fm.getValue(OVERTIME_DATE_CN);
        
        cal.setTime(checkDocDate(docDate));
        int year = cal.get(Calendar.YEAR);

        rootFolder = rootFolder.createSubfolderIfNotPresent(FOLDER_MAIN_OVERTIME).
                     createSubfolderIfNotPresent(FOLDER_OVERTIME_APP).
                     createSubfolderIfNotPresent(new Integer(year).toString()).
                     createSubfolderByFirstLetter(author.getLastname());

        setDocumentDescription(document);
        
        document.setFolder(rootFolder);
        
    }
    
    public void setDocumentDescription(Document document) throws EdmException
    {
        FieldsManager fm = document.getFieldsManager();
        DSUser author = DSUser.findByUsername(document.getAuthor()); 
        String date2 = new SimpleDateFormat("yyyy.MM.dd").format((Date)fm.getValue(OVERTIME_DATE_CN)); 
        String title = "Wniosek o odbi�r nadgodzin,"+ author.asFirstnameLastname() + ", " +date2  +", " + (String)fm.getValue(STATUS_FIELD_CN);
        
        if (document instanceof OfficeDocument)
        	((OfficeDocument)document).setSummary(title);
        
        document.setTitle(title);
        document.setDescription(title);
    }
    
    
    @Override
    public void documentPermissions(Document document) throws EdmException
    {
        LoggerFactory.getLogger("lukaszw").debug("documentPermissions");
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        String author = (String) document.getAuthor();
        DSUser dsUser = null;
        String name = null;
        try
        {
                dsUser = DSUser.findByUsername(author);
                name = dsUser.asLastnameFirstname();
        }
        catch (UserNotFoundException e) 
        {
            log.error("blad ",e);
            
             if (AvailabilityManager.isAvailable("aegon.docusafe.remote"))
                name = AbstractOvertimeLogic.getRemoteUserInfo(name).getName();
        }

        if(name == null || name.length() < 1)
        {
                //returnujemy bo nie wiemy na kogo nadac uprawnienia
                return;
        }

        String readName = "eNadgodziny "+name;
        String readGuid = "eNadgodziny_"+author;

        Set<PermissionBean> perms = new HashSet<PermissionBean>();
        perms.add(new PermissionBean(ObjectPermission.READ,readGuid,ObjectPermission.GROUP,readName));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,readGuid,ObjectPermission.GROUP,readName));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, readGuid,ObjectPermission.GROUP,readName));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, readGuid,ObjectPermission.GROUP,readName));

        perms.add(new PermissionBean(ObjectPermission.READ,document.getAuthor(),ObjectPermission.USER,document.getAuthor()));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,document.getAuthor(),ObjectPermission.USER,document.getAuthor()));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getAuthor(),ObjectPermission.USER,document.getAuthor()));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getAuthor(),ObjectPermission.USER,document.getAuthor()));

        setUpPermission(document, perms);
        
        //dodaje u�ytkownikowi grup�
        try{
            UpdateUser.addUserGroup(readGuid, dsUser);
        }catch(Exception e){log.error("",e);}
    }
    
    @Override
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
    {
    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
        DSUser author = null;
//        if (fm.getDocumentId() != null)
//                author =  UserImpl.find(new Long((String)fm.getValue(AUTHOR_CN)));
        // pobranie obiektu u�ytkownika zglaszaj�cego wniosek
        if (author == null)
                author = DSApi.context().getDSUser();
        
        // pobranie karty pracownika, moze rzucic wyjatek o braku aktualnej karty
        loadEmployeeCard(author); 
        /** laduje karte roczna nadgodzin*/
        loadEmployeeOvertimes();
        /** tworzy podsumowanie*/
        loadEmloyeeAllHour();

        Map<String,Object> values = new HashMap<String,Object>();
        values.put(TIME_FROM_CN, "00:00");
        values.put(TIME_TO_CN, "00:00");
        values.put(TIME_COUNT_CN, "00:00");
        values.put(STATUS_FIELD_CN, 5);
        fm.reloadValues(values);
    }
    /**
     * �aduje kart� pracownika
     * 
     * @param userImpl
     * @return
     * @throws Exception
     */
    private void loadEmployeeCard(String userName) throws EdmException
    {
        DSUser user = DSUser.findByUsername(userName);
        loadEmployeeCard(user);
    }
    /**
     * �aduje kart� pracownika
     * 
     * @param userImpl
     * @return
     * @throws Exception
     */
    private void loadEmployeeCard(DSUser userImpl) throws EdmException
    {
        LoggerFactory.getLogger("lukaszw").debug("loadEmployeeCard:userName=" + userImpl);
        empCard = instance.getActiveEmployeeCardByUserName(userImpl.getName());
        if (empCard == null)
                throw new EdmException(sm.getString("BrakAktualnejKartyPracownika"));
    }

    /**
     * Pobiera wszystkie dost�pne nadgodziny pracownika
     */
    private void loadEmployeeOvertimes() throws EdmException
    {
        LoggerFactory.getLogger("lukaszw").debug("loadEmployeeOvertimes:empCard=" + empCard.getExternalUser());
        int currentYear = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
        empOvertimes = instance.getEmployeeOvertimes(empCard, currentYear);
    }
    /**
     * sumuje nadgodziny pracownika w danych roku
     */
    private void loadEmloyeeAllHour()
    {
        if(this.empOvertimes.size() > 0)
        {
           EmployeeOvertimesSummary empOtSummary = EmployeeOvertimesSummary.getInstance(this.empOvertimes);
           this.allHour = empOtSummary.getAllHour();
           this.allHourReceive = empOtSummary.getAllHourReceive();
           this.allHourLeft = TimeUtils.substractTimes(allHour, allHourReceive);
        }
    }
       
    public EmployeeCard getEmpCard()
    {
        return empCard;
    }

    public void setEmpCard(EmployeeCard empCard)
    {
        this.empCard = empCard;
    }

    public List<EmployeeOvertimeEntity> getEmpOvertimes()
    {
        return empOvertimes;
    }

    public void setEmpOvertimes(List<EmployeeOvertimeEntity> emplOvertime)
    {
        this.empOvertimes = emplOvertime;
    }

    public String getAllHour()
    {
        return allHour;
    }

    public void setAllHour(String allHour)
    {
        this.allHour = allHour;
    }

    public String getAllHourReceive()
    {
        return allHourReceive;
    }

    public void setAllHourReceive(String allHourReceive)
    {
        this.allHourReceive = allHourReceive;
    }

    public String getAllHourLeft()
    {
        return allHourLeft;
    }

    public void setAllHourLeft(String allLeftHour)
    {
        this.allHourLeft = allLeftHour;
    }
    
    @Override
    public List<RelatedDocumentBean> getRelatedDocuments(FieldsManager fm) throws EdmException
    {
        DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
        
        OvertimeFactory of = new OvertimeFactory();
    	// wyszukanie dokument�w
    	List<RelatedDocumentBean> documentBeans = of.getReleatedDocuments(fm.getDocumentId());
    	
    	DSApi.close();
    	
    	return documentBeans;
    }
        
    /**
     * Updatuje wpis w tabeli DS_OVERTIME_CARD na podstawie id dokumentu
     * @TODO zrobic tak jak AbsenceRequest w klasie LocalOvertimeLogic
     * @param fm
     * @param document
     * @throws EdmException 
     */
    public void updateEmployeeOvertimeCard(FieldsManager fm, Document document) throws EdmException
    {
        String desc = (String)fm.getValue(DESCRIPTION_CN);
        String from = (String)fm.getValue(TIME_FROM_CN);
        String to = (String)fm.getValue(TIME_TO_CN);
        String count = (String)fm.getValue(TIME_COUNT_CN);
        Date date = (Date)fm.getValue(OVERTIME_DATE_CN);
        
        instance.chargeOvertimes(empCard, document.getId(), desc, date, from, to, count, Boolean.TRUE);
    }
    
    /**
     * @TODO zrobic tak jak AbsenceRequest w klasie LocalOvertimeLogic
     * @param document
     * @throws EdmException 
     */
    public void deleteEmployeeOvertimeCard(Document document) throws EdmException
    {
        instance.unchargeOvertimes(empCard, document.getId());
    }
     /**
     * Metoda pobiera list� do akceptacji
     * 
     * @param document
     * @return
     * @throws EdmException 
     */
    public Map<String, String> getAcceptancesMap(OfficeDocument document) throws EdmException
    {
       FieldsManager fm = document.getFieldsManager();
       Map<String, String> acceptancesMap = new LinkedHashMap<String, String>();
       try
        {
            if(STATUS_ZAAKCEPTOWANY.equals(fm.equals(fm.getKey(STATUS_FIELD_CN)) || STATUS_ANULOWANY.equals(fm.getKey(STATUS_FIELD_CN))))
                return acceptancesMap;
            
           //wniosek jest w realizacji zwracani s� szefowie dzia�u
           //wniosek jest odrzucony i jest u w�asciciela
           if(STATUS_ZAREJESTROWANY.equals(fm.getKey(STATUS_FIELD_CN)) || (STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN)) && isDocumentUserLogedIn(document.getAuthor())))
           {
               addDivisionBosses(acceptancesMap, DSApi.context().getPrincipalName());
           }
           
           if((STATUS_REALIZOWANY.equals(fm.getKey(STATUS_FIELD_CN)) && isDocumentUserLogedIn(document.getAuthor())))
           {
                addDivisionBosses(acceptancesMap, DSApi.context().getPrincipalName());
           }
           
           // wniosek u szefa dzia�u zwracany jest pracownik(autor dokumentu)
           // wniosek jest odrzucony ale jest u przelozonego
//           if(STATUS_REALIZOWANY.equals(fm.getKey(STATUS_FIELD_CN)) || (STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN)) && isDocumentUserLogedIn(document.getAuthor())))
//           {
//               userName = (DSUser.findByUsername(document.getAuthor())).asFirstnameLastname();
//               acceptancesMap.put(document.getAuthor(), userName);
//           }
           
           
        }catch (UserNotFoundException e) 
        {
        }
        
       return acceptancesMap;
    }
    
    @Override
    public void checkCanFinishProcess(OfficeDocument document) throws AccessDeniedException, EdmException
    {
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        String error = "Nie mo�na zako�czy� pracy z dokumentem: Wniosek musi by� odrzucony lub zaakceptowany finalnie.";

        fm.initializeAcceptances();
        if (STATUS_ZAAKCEPTOWANY.equals(fm.getKey(STATUS_FIELD_CN)) || STATUS_ANULOWANY.equals(fm.getKey(STATUS_FIELD_CN)) || STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN)))
        {
            return;
        }
        throw new AccessDeniedException(error);
    }
    
    @Override
    public AcceptanceCondition nextAcceptanceCondition(Document document) throws EdmException
    {
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        fm.initialize();
        fm.initializeAcceptances();
        boolean ok = false;
        String documentAuthor = document.getAuthor();
        String principalName = DSApi.context().getPrincipalName();
            
        
        // sprawdza czy zalogowany uzytkownik jest przelozonym dla wnioskodowacy
        List<AcceptanceCondition> conditions = null;
        if (( AvailabilityManager.isAvailable("aegon.docusafe.remote")) || (!AvailabilityManager.isAvailable("aegon.docusafe.remote")))
        {
                conditions = AcceptanceCondition.find(SZEF_DZIALU, documentAuthor.toString());
        }
        else
        {
                conditions = AcceptanceCondition.find(SZEF_DZIALU, "ext:"+documentAuthor.toString());
        }

        for (AcceptanceCondition condition : conditions)
        {
                if (condition.getUsername().equals(principalName))
                        ok = true;
        }
            
        if(!ok && STATUS_ZAAKCEPTOWANY.equals(fm.getKey(STATUS_FIELD_CN)) )
            throw new EdmException("Dokument zosta� ju� zaakceptowany");
        
        if(documentAuthor.equals(principalName) && STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN)))
        {
            throw new EdmException("Wniosek jest ju� odrzucony");
        }
        
        return getEmployeeAcceptanceCondition(fm, document);
    }
    
    	/**
	 * Wyszukuje akceptacj� szefa dzia�u
	 * 
	 * @param document
	 * @return
	 * @throws EdmException
	 */
	private AcceptanceCondition getBossAcceptanceCondition(FieldsManager fm,Document document) throws EdmException
	{
		// akceptacja szefa dzialu
		List<AcceptanceCondition> conditions = AcceptanceCondition.find(SZEF_DZIALU);
		
		if (conditions == null || conditions.isEmpty())
			throw new EdmException("Nie okre�lono szefa dzia�u!");
		
		AcceptanceCondition retValue = null;
		for (AcceptanceCondition ac : conditions)
		{
                    if ((AvailabilityManager.isAvailable("aegon.docusafe.remote")) || (!AvailabilityManager.isAvailable("aegon.docusafe.remote")))
                     {
                         if (ac.getFieldValue().equals(document.getAuthor()))
                         {
                             retValue = ac;
                             break;
                         }
                     }
                     else
                     {
                         if (ac.getFieldValue().contains(document.getAuthor()))
                         {
                             retValue = ac;
                             break;
                         }
                     }	
		}
		 
		if (retValue == null)
			throw new EdmException("Nie okre�lono szefa dzia�u!");
		
		return retValue;
	}
   /**
     * Zwraca akceptacje pracownika
     * 
     * @param document
     * @return
     * @throws EdmException
     */
    private AcceptanceCondition getEmployeeAcceptanceCondition(FieldsManager fm, Document document) throws EdmException
    {
            AcceptanceCondition retCondition = null;

            retCondition = new AcceptanceCondition();
            retCondition.setCn(PRACOWNIK);
            retCondition.setUsername(document.getAuthor());

            return retCondition;
    }
         

    @Override
    public void onSendDecretation(Document document, AcceptanceCondition condition) throws EdmException
    {
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

        if(STATUS_ZAAKCEPTOWANY.equals(fm.getKey(STATUS_FIELD_CN)))
        {
                sendDocument(document);    
        }
    }

    @Override
    public void onAccept(String acceptanceCn, Long documentId) throws EdmException
    {
    }

    @Override
    public void onWithdraw(String acceptanceCn, Long documentId) throws EdmException
    {
    }

    @Override
    public boolean canSendDecretation(FieldsManager fm) throws EdmException
    {
        return true;
    }
    
    /**
     * Wysyla dokument do relalizacji do wlasciciela i do waidomosci do grupy hr
     * @param document
     * @throws EdmException 
     */
    public void sendDocument(Document document) throws EdmException
    {
        String[] activities = WorkflowFactory.findDocumentTasks(document.getId(), document.getAuthor());
        String activity = null;
        
        
        if(activities.length < 1)
            return;
        else
            for(String a: activities)
                activity = a;
        
        String guid = Docusafe.getAdditionProperty("enadgodziny.grupa_hr");
        
        DSDivision ds = DivisionImpl.find(guid);
        DSUser[] users = ds.getUsers();
        
        log.error("sendDocument : users.length " + users.length);

        TaskSnapshot.updateTaskList(document.getId(), document.getType().getName(), 1, document.getAuthor());
        
        for(DSUser user : users)
        {
//        Wykonywane ju� w akcji po wywolaniu nextAcceptance
//        WorkflowFactory.simpleAssignment(activity, guid, document.getAuthor(), null, WorkflowFactory.wfManualName(), 
//                            WorkflowFactory.SCOPE_ONEUSER, null, null);
            WorkflowFactory.simpleAssignment(activity, guid, user.getName(), null, WorkflowFactory.wfCcName(), 
                            WorkflowFactory.SCOPE_ONEUSER, null, null);
        }
    }
    
    
   /**
     * @param eventActionSupport
     * @param event
     * @param document
     * @param activity
     * @param values
     * @throws EdmException 
     */
    @Override
    public void doDockindEvent(DockindButtonAction eventActionSupport, 
                ActionEvent event, Document document, String activity,Map<String, Object> values)
    {
        try
        {
            FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
            File file = OvertimeRaportFactory.pdfDocument(document, values, fm);
            DSUser author = DSUser.findByUsername(document.getAuthor()); 
            String date2 = new SimpleDateFormat("yyyy.MM.dd").format((Date)fm.getValue(OVERTIME_DATE_CN)); 
            String title = "Wniosek o odbi�r nadgodzin,"+ author.asFirstnameLastname() + ", " +date2;

            ServletUtils.streamFile(ServletActionContext.getResponse(), 
                                        file,
                                        "application/pdf", 
                                        "Content-Disposition: attachment; filename=\""+ title+".pdf\"");
        } catch (Exception ex)
        {
            event.addActionError("B��d podczas generowania dokumentu");
            log.error("blad",ex);
        }
    }
    
     /**
     * czy zalogowany jest zainteresowanym kogo dotyczy dokument
     * @param userId
     * @return
     * @throws EdmException 
     */
    public boolean isDocumentUserLogedIn(String username) throws EdmException
    {
        if(DSApi.context().getPrincipalName().equals(username))
            return true;
        else
            return false;
    }
    
    /**
     * Dodaje uzytkownikow jako szefow dzialu do mapy akceptacji
     * 
     * @param acceptancesMap
     * @throws EdmException
     */
    @Override
    protected void addDivisionBosses(Map<String, String> acceptancesMap, String documentAuthor) throws EdmException
    {
        // wybranie szefow dzialu dla uzytkownika
        List<AcceptanceCondition> conditions = AcceptanceCondition.find(SZEF_DZIALU, documentAuthor);
    	for (AcceptanceCondition cond : conditions)
    	{
    		try
    		{
    			acceptancesMap.put(cond.getUsername(), DSUser.findByUsername(cond.getUsername()).asLastnameFirstname());
    		}
    		catch (UserNotFoundException ex)
    		{
    			if (cond.getUsername().startsWith("ext:"))
    				acceptancesMap.put(cond.getUsername(), "U�ytk. zewn.: " + cond.getUsername().substring(4));
    			log.warn(ex.getMessage(), ex);
    		}
    	}	
    }

    @Override
    public void onStartProcess(OfficeDocument document) throws EdmException
    {
        FieldsManager fm = document.getFieldsManager();
        //laduje karte pracownika mo�e rzuci� bledem
        loadEmployeeCard(document.getAuthor());
        
        if(!STATUS_ZAREJESTROWANY.equals(fm.getKey(STATUS_FIELD_CN)))
            throw new EdmException("Nie mozna rejestrowa� dokumentu z innym statusem ni� zarejestrowany!");
        
        hasOvertimesReceive(document,fm);
        needAttachments(document.getId(), (Date)fm.getValue(OVERTIME_DATE_CN));
        
        Map<String, Object> values = new HashMap<String, Object>();
        values.put(USER_CN, DSApi.context().getDSUser().getId().toString());
        
        fm.reloadValues(values);
        fm.getDocumentKind().setOnly(fm.getDocumentId(), values);
        
        super.onStartProcess(document);
    }
    
    public void hasOvertimesReceive(Document document, FieldsManager fm) throws EdmException
    {
        loadEmployeeCard(document.getAuthor());
        String timeCount = (String)fm.getValue(TIME_COUNT_CN);
         /** Lista nadgodzin w roku*/
        List<EmployeeOvertimeEntity> empOtEntites = instance.getEmployeeOvertimes(empCard, DateUtils.getCurrentYear());
        /** podsumowania dla pracownika w danym roku */
        EmployeeOvertimesSummary empOtSummary = EmployeeOvertimesSummary.getInstance(empOtEntites);

        String currentApplications = OvertimeFactory.getEmployeeReceiveApplications(document.getAuthor(), document.getId());
        String overtimesLeft = TimeUtils.substractTimes(empOtSummary.getAllHourLeft(), currentApplications);

        if(TimeUtils.isNegative(overtimesLeft) || overtimesLeft.equalsIgnoreCase("00:00"))
            throw new EdmException("Nie posiadasz wypracowanych tylu nadgodzin!");

       
        if(TimeUtils.isGreater(timeCount, overtimesLeft) && isDocumentUserLogedIn(document.getAuthor()))
           throw new EdmException("Nie posiadasz wypracowanych tylu nadgodzin!");
    }
    
    /**
     * Walidacja czy ma zalacznik gdy data jest mniejsza niz 5 dni
     */
    public static void needAttachments(Long id, Date overtimeDate) throws EdmException
    {
        //5 dni wstecz data
        Date todayBack5 = DateUtils.plusDays(Calendar.getInstance().getTime(), OVERTIME_MAX_DATE);
        
        if(!DateUtils.isGreaterDate(overtimeDate, todayBack5 ))
        {
            throw new EdmException("Mo�esz wype�nia� wniosek tylko na 5 dni wstecz! W celu wype�nienia wniosku skontaktuj si� z dzia�em HR");
        }     
    }
}
