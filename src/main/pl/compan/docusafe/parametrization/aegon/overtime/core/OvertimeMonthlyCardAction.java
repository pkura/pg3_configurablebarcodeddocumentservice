package pl.compan.docusafe.parametrization.aegon.overtime.core;

import com.opensymphony.webwork.ServletActionContext;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.parametrization.aegon.overtime.core.OvertimeFactory;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TimeUtils;
import pl.compan.docusafe.webwork.event.*;

/**
 *
 * @author �ukasz Wo�niak <l.g.wozniak@gmail.com>
 */
public class OvertimeMonthlyCardAction extends EventActionSupport
{
    private static final Logger log = LoggerFactory.getLogger(OvertimeMonthlyCardAction.class);
    private static final String _POPUP = "popup";
    private static final String _SUCCESS = "success";
    
    private boolean popup;
    private int month;
    private int currentYear;
    private String overtimes;
    private String overtimesReceives;
    
    private List<EmployeeOvertimeMonthlyEntity> monthlyOvertimes;
    
    @Override
    protected void setup()
    {
        class SetResult implements ActionListener
        {
            public void actionPerformed(ActionEvent event)
            {
                event.setResult(popup ? _POPUP : _SUCCESS);
            }
        }

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(new SetResult()).
            append(new FillForm()).
            appendFinally(CloseHibernateSession.INSTANCE);
    }
    
    private class FillForm implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event)
        {
            try{
                /** Karta pracownika*/
                EmployeeCard emCard = AbstractOvertimeLogic.getInstance().getActiveEmployeeCardByUserName(DSApi.context().getPrincipalName());
                /** Tworzy okiekt przechowuj�cy potrzebne dane*/
                EmployeeOvertimesMonthlyBean empMonthlyBean = new EmployeeOvertimesMonthlyBean(emCard, currentYear, month);
                
                empMonthlyBean.getEmployeeMonthlyOvertimes();
                empMonthlyBean.init(); // oblicza potrzebne sumy u usuwa nie potrzebne miesi�ce
                
                monthlyOvertimes = empMonthlyBean.getMonthlyOvertimes();
                
                overtimes = empMonthlyBean.getOvertimes();
                overtimesReceives = empMonthlyBean.getOvertimesReceives();
            }
            catch (EdmException e) {
                addActionError(e.getMessage());
                //DSApi.context().setRollbackOnly();

            } finally {
                try {
                    DSApi.close();
                } catch (EdmException e) {
                }
            }
        }
    }
    
    public void setPopup(boolean popup)
    {
        this.popup = popup;
    }    
    
    public void setMonth(int month)
    {
        this.month = month;
    }
    
    public int getMonth()
    {
        return this.month;
    }

    public List<EmployeeOvertimeMonthlyEntity> getMonthlyOvertimes()
    {
        return monthlyOvertimes;
    }

    public void setMonthlyOvertimes(List<EmployeeOvertimeMonthlyEntity> monthlyOvertimes)
    {
        this.monthlyOvertimes = monthlyOvertimes;
    }

    public String getOvertimes()
    {
        return overtimes;
    }

    public void setOvertimes(String overtimes)
    {
        this.overtimes = overtimes;
    }

    public String getOvertimesReceives()
    {
        return overtimesReceives;
    }

    public void setOvertimesReceives(String overtimesReceives)
    {
        this.overtimesReceives = overtimesReceives;
    }

    public int getCurrentYear()
    {
        return this.currentYear;
    }

    public void setCurrentYear(int currentYear)
    {
        this.currentYear = currentYear;
    }
    
    
    
    
}
