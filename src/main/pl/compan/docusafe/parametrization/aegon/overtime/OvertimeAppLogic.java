package pl.compan.docusafe.parametrization.aegon.overtime;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.logic.Acceptable;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.aegon.overtime.core.AbstractOvertimeLogic;
import pl.compan.docusafe.parametrization.aegon.overtime.core.EmployeeOvertimeEntity;
import pl.compan.docusafe.parametrization.aegon.overtime.core.EmployeeOvertimesSummary;

import static pl.compan.docusafe.parametrization.aegon.overtime.OvertimeCn.*;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.absences.FreeDay;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.DSUserEnumField;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.parametrization.aegon.overtime.core.OvertimeFactory;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TimeUtils;
import pl.compan.docusafe.web.common.form.UpdateUser;


/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class OvertimeAppLogic extends BaseOvertimeLogic implements Acceptable
{
    private static final StringManager sm = StringManager.getManager(OvertimeAppLogic.class.getPackage().getName());
    private static final Logger log = LoggerFactory.getLogger(OvertimeAppLogic.class);
    private static AbstractOvertimeLogic instance;
    
    private String userName = null;
    
    
    /**
     * Karta pracownika wnioskodawcy
     */
    protected EmployeeCard empCard;
    

    public OvertimeAppLogic()
    {
        //ustalenie typu logiki
        instance = AbstractOvertimeLogic.getInstance();
    }

    
    public static synchronized  AbstractOvertimeLogic getInstance()
    {
        if( instance == null )
            instance = AbstractOvertimeLogic.getInstance();
        return instance;
    }

    @Override
    public void onGiveAcceptance(Document document, String acceptanceCn) throws EdmException
    {
        FieldsManager fm = document.getFieldsManager();

        if (STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN)) || STATUS_ANULOWANY.equals(fm.getKey(STATUS_FIELD_CN)))
        {
            return;
        }

        //validateData(document);

        Map<String, Object> values = new HashMap<String, Object>();

        if (acceptanceCn.equals(PRACOWNIK))
        {
            updateEmployeeOvertimeCard(fm, document);
        }

        values.put(STATUS_FIELD_CN, STATUS_ZAAKCEPTOWANY);
        fm.reloadValues(values);
        fm.getDocumentKind().setOnly(fm.getDocumentId(), values);
        
        setDocumentDescription(document);
    }
    
    @Override
    public void onWithdrawAcceptance(Document document, String acceptanceCn) throws EdmException 
    {
            FieldsManager fm = document.getFieldsManager();
            Map<String, Object> values = new HashMap<String, Object>();

            // je�li cofniecie akceptacji wniosku AEGON przez pracownika
            if (acceptanceCn.equals(PRACOWNIK) && !STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN))
                            && !STATUS_ANULOWANY.equals(fm.getKey(STATUS_FIELD_CN)))
            {
                
                   deleteEmployeeOvertimeCard(document);
                   values.put(STATUS_FIELD_CN, STATUS_REALIZOWANY);
            }
            
            fm.reloadValues(values);
            fm.getDocumentKind().setOnly(fm.getDocumentId(), values);
            
            setDocumentDescription(document);
    }

    /**
     * Akceptowa� mo�e tylko podw�adny, kt�rego dotyczy dokument
     */
    @Override
    public boolean canAcceptDocument(FieldsManager fm, String acceptanceCn) throws EdmException
    {
        Integer userId = (Integer)fm.getKey(USER_CN);

        if((DSApi.context().getDSUser().getId().equals(userId.longValue())))
            return true;
        
        return false;
    }
    
    @Override
    public void correctValues(Map<String, Object> values, DocumentKind kind) throws EdmException
    {
        String userId = (String)values.get(USER_CN);
        log.error("userIds" + userId);
        List<DSUser> usersSubordinates = OvertimeFactory.getListAcceptanceSubordinates(DSApi.context().getPrincipalName());
        DSUser user = getSubordinate(usersSubordinates, new Integer(userId));
        
        if(userId == null)
            throw new EdmException("Nie wybra�es podw�adnego lub te� nie masz uprawnie�!");

        if((!DSApi.context().getDSUser().getId().equals(Long.parseLong(userId)) && usersSubordinates.isEmpty()))
            throw new EdmException("Nie wybra�es podw�adnego lub te� nie masz uprawnie�!");
        
        if(user == null )
            throw new EdmException("To nie jest tw�j podw�adny!");

        String objDate = (String) values.get(OVERTIME_DATE_CN);
        
        //rzuca wyj�tkiem
        instance.isFreeDay(DateUtils.parseJsDate(objDate));
        
        String timeCount = (String)values.get(TIME_COUNT_CN);
        
        if(!TimeUtils.isGreater(timeCount, "00:05"))
           throw new EdmException("Liczba nadgodzin jest zbyt ma�a");
        
        
        //laduje karte pracownika mo�e rzuci� bledem
        loadEmployeeCard(user.getName());
        
        Date date = DateUtils.nullSafeParseJsDate((String)values.get(OVERTIME_DATE_CN));
       
        if(date == null)
            throw new EdmException("Pole Data nadgodzin jest obowi�zkowe!");
//        if(DateUtils.isGreaterFromToday(date))
//            throw new EdmException("Nie mo�esz dodawa� z dat� w prz�d!");
        
//        // sprawdza czy zalogowany uzytkownik jest przelozonym dla wnioskodowacy
//        List<AcceptanceCondition> conditions = null;
//        if ((AvailabilityManager.isAvailable("aegon.docusafe.remote")) || 
//           ( !AvailabilityManager.isAvailable("aegon.docusafe.remote")))
//        {
//            conditions = AcceptanceCondition.find(SZEF_DZIALU, user.getName());
//        }
//        else
//        {
//            conditions = AcceptanceCondition.find(SZEF_DZIALU, "ext:"+name);
//        }
//        for (AcceptanceCondition condition : conditions)
//        {
//                if (condition.getUsername().equals(DSApi.context().getPrincipalName()))
//                        ok = true;
//        }
//
//
//        if(!(ok || (DSApi.context().getDSUser().getId() == Long.parseLong(userId))))
//            throw new EdmException("To nie jest tw�j podw�adny!");
            
        String timeFrom = (String)values.get(TIME_FROM_CN);
        String timeTo = (String)values.get(TIME_TO_CN);
        
        //sprawdzam walidacje godzin
        try{
            TimeUtils.checkTimeRange(timeFrom, timeTo);
        } catch (EdmException ex)
        {
            throw new EdmException(sm.getString(ex.getMessage()));
        }
        
        super.correctValues(values, kind);
    }

    
    @Override
    public void onLoadData(FieldsManager fm) throws EdmException
    {
    }
      
    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
       if( document.getDocumentKind() == null )
            throw new NullPointerException("document.getDocumentKind()");
     
        FieldsManager fm = document.getFieldsManager();
        DSUser user = getSubordinate((Integer)fm.getKey(USER_CN));
        Calendar cal =  DateUtils._GREGORIAN_CALENDAR;        
        Folder rootFolder = Folder.getRootFolder();
        Object docDate = fm.getValue(OVERTIME_DATE_CN);
        
        //sprawdzam czy zalogowany uzytkownik jest wlascicielem dokumentu jesli tak to sprawdza czy ma wystarczajaca ilosc nadgodzin
        if(DSApi.context().getPrincipalName().equals(document.getAuthor()) )
            hasOvertimesBorder(document, fm);
        
        cal.setTime(checkDocDate(docDate));
        int year = cal.get(Calendar.YEAR);

        rootFolder = rootFolder.createSubfolderIfNotPresent(FOLDER_MAIN_OVERTIME).
                     createSubfolderIfNotPresent(FOLDER_OVERTIME_APP).
                     createSubfolderIfNotPresent(new Integer(year).toString()).
                     createSubfolderByFirstLetter(user.getLastname());
        
        document.setFolder(rootFolder);
        setDocumentDescription(document);
    }
    
    public void setDocumentDescription(Document document) throws EdmException
    {
        FieldsManager fm = document.getFieldsManager();
        String date2 = new SimpleDateFormat("yyyy.MM.dd").format((Date) fm.getValue(OVERTIME_DATE_CN)); 
        String title = "Zlecenie nadgodzin," +(String)fm.getValue(USER_CN) + ", " +date2 +", " + (String)fm.getValue(STATUS_FIELD_CN);
        if (document instanceof OfficeDocument)
        	((OfficeDocument)document).setSummary(title);
        document.setTitle(title);
        document.setDescription(title);
    }
    
    @Override
    public void documentPermissions(Document document) throws EdmException
    {
        LoggerFactory.getLogger("lukaszw").debug("documentPermissions");
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        String name = (String)fm.getValue(USER_CN);
        try
        {
            userName = getUserNameByCn(fm);
        }
        catch (UserNotFoundException e) 
        {
            log.error("Bl�d",e);
            
            if (AvailabilityManager.isAvailable("aegon.docusafe.remote"))
                userName = AbstractOvertimeLogic.getRemoteUserInfo(name).getName();
            
        }catch(Exception e){
                    log.error("Blad",e);
        }

        if(name == null || name.length() < 1)
        {
            log.error("Nie nadano uprawnien dla dokument:" +document.getId());
                //returnujemy bo nie wiemy na kogo nadac uprawnienia
                return;
        }

        
        String readName = "eNadgodziny "+name;
        String readGuid = "eNadgodziny_"+userName;

        Set<PermissionBean> perms = new HashSet<PermissionBean>();
        perms.add(new PermissionBean(ObjectPermission.READ,readGuid,ObjectPermission.GROUP,readName));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,readGuid,ObjectPermission.GROUP,readName));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, readGuid,ObjectPermission.GROUP,readName));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, readGuid,ObjectPermission.GROUP,readName));
        
        perms.add(new PermissionBean(ObjectPermission.READ,document.getAuthor(),ObjectPermission.USER,document.getAuthor()));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,document.getAuthor(),ObjectPermission.USER,document.getAuthor()));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getAuthor(),ObjectPermission.USER,document.getAuthor()));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getAuthor(),ObjectPermission.USER,document.getAuthor()));

        perms.add(new PermissionBean(ObjectPermission.READ,userName,ObjectPermission.USER,userName));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,userName,ObjectPermission.USER,userName));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, userName,ObjectPermission.USER,userName));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, userName,ObjectPermission.USER,userName));
        
        setUpPermission(document, perms);
        
        //Dodaje u�ytkownika do grupy
        try{
            UpdateUser.addUserGroup(readGuid, UserImpl.find(new Integer((Integer)fm.getKey(USER_CN)).longValue()));
        }catch(Exception e){ log.error("",e);
        }
    }
    
    @Override
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
    {
        FieldsManager fm =  kind.getFieldsManager(documentId);
        Date overtimeDate = (Date)fm.getValue(OVERTIME_DATE_CN);
        
        if(STATUS_ZAREJESTROWANY.equals(fm.getKey(STATUS_FIELD_CN)))
            needAttachments(documentId, overtimeDate);
    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
        ((DSUserEnumField)fm.getField(USER_CN)).loadEnumItem( DSUser.listAcceptanceSubordinates());
        fm.initialize();
        Map<String,Object> values = new HashMap<String,Object>();
        values.put(TIME_FROM_CN, "00:00");
        values.put(TIME_TO_CN, "00:00");
        values.put(TIME_COUNT_CN, "00:00");
        values.put(STATUS_FIELD_CN, 5);
        fm.reloadValues(values);   
    }
    
    public DSUser getSubordinate(Integer userId) throws EdmException
    {
        return getSubordinate(OvertimeFactory.getListAcceptanceSubordinates(DSApi.context().getPrincipalName()), userId);
    }
    
    public DSUser getSubordinate(List<DSUser> allSub, Integer userId) throws EdmException
    {
        DSUser user = null;
        
        if(userId != null)
        {
            
            for (DSUser subordinate : allSub)
            {
                if(subordinate.getId().equals(userId.longValue()))
                   user = (UserImpl)subordinate;
            }
        } 
        
        return user;
    }
    
    /**
     * �aduje kart� pracownika na podstawie id uzytkownika z FieldsManagera
     * @param UserId Integer
     * @throws EdmException 
     */
    private void loadEmployeeCard(Integer userId) throws EdmException
    {
        if(userId != null)
        {
            UserImpl user = null;
            List<DSUser> subordinates = DSUser.listAcceptanceSubordinates(DSApi.context().getPrincipalName());
            
            for (DSUser subordinate : subordinates)
            {
                if(subordinate.getId().equals(userId.longValue()))
                {
                    user = (UserImpl)subordinate;
                }
            }
            
//            UserImpl user = UserImpl.find(userId.longValue());
            //laduje karte pracownika
            loadEmployeeCard(user);
            
            
        }  
    }
    
      /**
     * �aduje kart� pracownika
     * 
     * @param userImpl
     * @return
     * @throws Exception
     */
    private void loadEmployeeCard(String userName) throws EdmException
    {
        log.debug("loadEmployeeCard:userName=" + userName);
        empCard = instance.getActiveEmployeeCardByUserName(userName);
        if (empCard == null)
                throw new EdmException(sm.getString("BrakAktualnejKartyPracownika"));
    }
    /**
     * �aduje kart� pracownika
     * 
     * @param userImpl
     * @return
     * @throws Exception
     */
    private void loadEmployeeCard(UserImpl userImpl) throws EdmException
    {
        log.debug("loadEmployeeCard:userName=" + userImpl);
        empCard = instance.getActiveEmployeeCardByUserName(userImpl.getName());
        if (empCard == null)
                throw new EdmException(sm.getString("BrakAktualnejKartyPracownika"));
    }
    
    /**
     * Walidacja czy ma zalacznik gdy data jest mniejsza niz 5 dni
     */
    public static void needAttachments(Long id, Date overtimeDate) throws EdmException
    {
        //5 dni wstecz data
        Date todayBack5 = DateUtils.plusDays(Calendar.getInstance().getTime(), OVERTIME_MAX_DATE);
        
        if(!DateUtils.isGreaterDate(overtimeDate, todayBack5 ))
            if(!DSApi.context().isAdmin())
            {
                throw new EdmException(sm.getString("ZaPoznoTylkoAdminToMoze"));
            }
            else
            {
                try{
                    Attachment attachment = Attachment.findFirstAttachment(id); 
                    if(attachment == null)
                        throw new EdmException(sm.getString("DodajZalacznikAbyDodawacPozniejNiz5dniwstecz"));
                }catch(SQLException e){
                    throw new EdmException(e.getMessage());
                }catch(NullPointerException e){
        //          throw new EdmException(sm.getString("DodajZalacznikAbyDodawacPozniejNiz5dniwstecz"));
                }
            }           
    }

    /**
     * Metoda pobiera list� do akceptacji
     * 
     * @param document
     * @return
     * @throws EdmException 
     */
    public Map<String, String> getAcceptancesMap(OfficeDocument document) throws EdmException
    {
       Map<String, String> acceptancesMap = new LinkedHashMap<String, String>();
       FieldsManager fm = document.getFieldsManager();
       Integer userId  = (Integer)fm.getKey(USER_CN);
       
       try
        {
            if(STATUS_ZAAKCEPTOWANY.equals(fm.equals(fm.getKey(STATUS_FIELD_CN)) || STATUS_ANULOWANY.equals(fm.getKey(STATUS_FIELD_CN))))
                return acceptancesMap;
            
           //wniosek jest w realizacji zwracany jest tylko u�ytkownik
           //pracownik odrzucil i jest u szefa dzialu
//           if(STATUS_ZAREJESTROWANY.equals(fm.getKey(STATUS_FIELD_CN))  || (STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN)) && !isDocumentUserLogedIn(userId.longValue())))
//           {
//               userName = getUserNameByCn(fm);
//               acceptancesMap.put(userName, (String)fm.getValue(USER_CN));
//           }
           
           // wniosek u pracownika zwracania sa szefownie dzialu
           //pracownik odrzucil dokument i jest u pracownika
//           if(STATUS_REALIZOWANY.equals(fm.getKey(STATUS_FIELD_CN)) || (STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN)) && isDocumentUserLogedIn(userId.longValue())))
//           {
//               addDivisionBosses(acceptancesMap, userName);
//           }
           
        }catch (UserNotFoundException e) 
        {
        }
        
       return acceptancesMap;
    }

        /**
     * Metoda zmienia status wniosku w zaleznosci od obecnego statusu
     * @param document
     * @throws EdmException 
     */
    @Override
    public void onDecretation(Document document) throws EdmException
    {
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        Map<String, Object> values = new HashMap<String, Object>();
        Integer userId  = (Integer)fm.getKey(USER_CN);
        
        //Przelozony kieruje wniosek do realizacji
        if(STATUS_ZAREJESTROWANY.equals(fm.getKey(STATUS_FIELD_CN)) || (STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN)) && !isDocumentUserLogedIn(userId.longValue())))
            values.put(STATUS_FIELD_CN,STATUS_REALIZOWANY);
        
        fm.reloadValues(values);
        
        fm.getDocumentKind().setOnly(fm.getDocumentId(), values);
        
        setDocumentDescription(document);
    }
    
     @Override
    public void checkCanFinishProcess(OfficeDocument document) throws AccessDeniedException, EdmException
    {
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        String error = "Nie mo�na zako�czy� pracy z dokumentem: Wniosek musi by� odrzucony lub zaakceptowany finalnie.";

        fm.initializeAcceptances();
        if (STATUS_ZAAKCEPTOWANY.equals(fm.getKey(STATUS_FIELD_CN)) || STATUS_ANULOWANY.equals(fm.getKey(STATUS_FIELD_CN))  || STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN)))
        {
            return;
        }
        
        throw new AccessDeniedException(error);
    }
    
    

    /**
     * Pobiera name uzytkownika z formularza
     * @param fm
     * @return
     * @throws UserNotFoundException
     * @throws EdmException 
     */
    private String getUserNameByCn(FieldsManager fm) throws UserNotFoundException, EdmException
    {
        if(userName == null)
        {
            String name = (String)fm.getValue(USER_CN);
            String[] aa = name.split("\\ ");
            DSUser user = DSUser.findByFirstnameLastname(aa[1], aa[0]);
            return user.getName();
        }
        else
            return userName;
    
    }
    
    @Override
    public AcceptanceCondition nextAcceptanceCondition(Document document) throws EdmException
    {
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        fm.initialize();
        fm.initializeAcceptances();
        Integer userId  = (Integer)fm.getKey(USER_CN);
                
        Date overtimeDate = (Date)fm.getValue(OVERTIME_DATE_CN);
        
        if(STATUS_ZAREJESTROWANY.equals(fm.getKey(STATUS_FIELD_CN)))
            needAttachments(document.getId(), overtimeDate);
        
        
        // pobranie wykonanych akceptacji og�lnych
//        List<DocumentAcceptance> docAccs = fm.getAcceptancesState().getGeneralAcceptances();
//        
//        boolean hasWorkerAcceptance = false;
//        
//        if(docAccs != null && docAccs.size() > 0)
//        {
//            for (DocumentAcceptance acceptance : docAccs)
//            {
//                    if (acceptance.getAcceptanceCn().equals(PRACOWNIK))
//                            hasWorkerAcceptance = true;
//            }
//        }
        
//        if(hasWorkerAcceptance)
//        {
//            return getEmployeeAcceptanceCondition(fm);
//        }
//        else
//        {
        if(STATUS_REALIZOWANY.equals(fm.getKey(STATUS_FIELD_CN)))
                throw new EdmException("Musisz podja� decyzje");
            //wniosek jest w realizacji zwracany jest tylko u�ytkownik
            //pracownik odrzucil i jest u szefa dzialu
            if(STATUS_ZAREJESTROWANY.equals(fm.getKey(STATUS_FIELD_CN)))
                return getEmployeeAcceptanceCondition(fm);

            if(STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN)) && !isDocumentUserLogedIn(userId.longValue()))
                throw new EdmException("Wniosek jest ju� odrzucony");
            //wniosek u pracownika zwracania sa szefownie dzialu
            //pracownik odrzucil dokument i jest u pracownika
            if((STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN)) && isDocumentUserLogedIn(userId.longValue())))
                return getAuthorAcceptanceCondition(fm, document.getAuthor());
//        }      
        return getEmployeeAcceptanceCondition(fm);

    }

    @Override
    public void onSendDecretation(Document document, AcceptanceCondition condition) throws EdmException
    {
        
    }
    
    @Override
    public void onAccept(String acceptanceCn, Long documentId) throws EdmException
    {
    }

    @Override
    public void onWithdraw(String acceptanceCn, Long documentId) throws EdmException
    {
    }

    @Override
    public boolean canSendDecretation(FieldsManager fm) throws EdmException
    {
        return true;
    }
    
    /**
     * Zwraca akceptacje pracownika
     * 
     * @param document
     * @return
     * @throws EdmException
     */
    private AcceptanceCondition getEmployeeAcceptanceCondition(FieldsManager fm) throws EdmException
    {
            AcceptanceCondition retCondition = null;

            retCondition = new AcceptanceCondition();
            retCondition.setCn(PRACOWNIK);
            retCondition.setUsername(getUserNameByCn(fm));

            return retCondition;
    }
    
    /**
     * Zwraca Akceptacj� do autora
     * @param fm
     * @param author
     * @return
     * @throws EdmException 
     */
    private AcceptanceCondition getAuthorAcceptanceCondition(FieldsManager fm, String author) throws EdmException
    {
        AcceptanceCondition retCondition = null;

        retCondition = new AcceptanceCondition();
        retCondition.setCn(SZEF_DZIALU);
        retCondition.setUsername(author);

        return retCondition;
    }
    
    public void updateEmployeeOvertimeCard(FieldsManager fm, Document document) throws EdmException
    {
        String desc = (String)fm.getValue(DESCRIPTION_CN);
        String from = (String)fm.getValue(TIME_FROM_CN);
        String to = (String)fm.getValue(TIME_TO_CN);
        String count = (String)fm.getValue(TIME_COUNT_CN);
        Date date = (Date)fm.getValue(OVERTIME_DATE_CN);
        
        instance.chargeOvertimes(empCard, document.getId(), desc, date, from, to, count, Boolean.FALSE);
    }
    
    /**
     * @TODO zrobic tak jak AbsenceRequest w klasie LocalOvertimeLogic
     * @param document
     * @throws EdmException 
     */
    public void deleteEmployeeOvertimeCard(Document document) throws EdmException
    {
       instance.unchargeOvertimes(empCard, document.getId());
    }
    
    /**
     * czy zalogowany jest zainteresowanym kogo dotyczy dokument
     * @param userId
     * @return
     * @throws EdmException 
     */
    public boolean isDocumentUserLogedIn(Long userId) throws EdmException
    {
        if(DSApi.context().getDSUser().getId().equals(userId))
            return true;
        else
            return false;
    }

   /**
     * czy zalogowany jest zainteresowanym kogo dotyczy dokument
     * @param userId
     * @return
     * @throws EdmException 
     */
    public boolean isDocumentUserLogedIn(String username) throws EdmException
    {
        if(DSApi.context().getPrincipalName().equals(username))
            return true;
        else
            return false;
    }
    
    @Override
    public void onStartProcess(OfficeDocument document) throws EdmException
    {
        FieldsManager fm = document.getFieldsManager();
       
        if(!STATUS_ZAREJESTROWANY.equals(fm.getKey(STATUS_FIELD_CN)))
            throw new EdmException("Nie mozna rejestrowa� dokumentu z innym statusem ni� zarejestrowany!");
    }

    @Override
    public void onEndCreateProcess(Document document) throws EdmException
    {
        String[] activities = WorkflowFactory.findDocumentTasks(document.getId(), document.getAuthor());
        FieldsManager fm = document.getFieldsManager();
        Integer userId = (Integer)fm.getKey(USER_CN);
        DSUser user = getSubordinate(userId);
       
        String activity = null;
       
        if(activities.length < 1)
            return;
        else
            for(String a: activities)
                activity = a;
        
        Map<String, Object> values = new HashMap<String, Object>();

        values.put(STATUS_FIELD_CN, STATUS_REALIZOWANY);
        fm.reloadValues(values);
        fm.getDocumentKind().setOnly(fm.getDocumentId(), values);
        
        WorkflowFactory.simpleAssignment(activity, "rootdivision", user.getName() , null, WorkflowFactory.wfManualName(), 
                            WorkflowFactory.SCOPE_ONEUSER, null, null);
        WorkflowFactory.createNewProcess((OfficeDocument) document, false, sm.getString("przekazano"), document.getAuthor(), "rootdivision", user.getName());
    }
    
    public void hasOvertimesBorder(Document document, FieldsManager fm) throws EdmException
    {
        loadEmployeeCard((Integer)fm.getKey(USER_CN));
         /** Lista nadgodzin w roku*/
        List<EmployeeOvertimeEntity> empOtEntites = instance.getEmployeeOvertimes(empCard, DateUtils.getCurrentYear());
        /** podsumowania dla pracownika w danym roku */
        EmployeeOvertimesSummary empOtSummary = EmployeeOvertimesSummary.getInstance(empOtEntites);
        
        String currentApplications = OvertimeFactory.getEmployeeAppApplications(document.getAuthor(), document.getId());
        
        String timeAll = TimeUtils.summaryTimes(empOtSummary.getAllHour(), currentApplications);
        
        if(TimeUtils.isGreater(timeAll, "150:00"))
            throw new EdmException("Przekroczono maksymaln� liczb� godzin nadliczbowych!");
        
       
    }
    
    
}
