package pl.compan.docusafe.parametrization.aegon.overtime.core;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TimeUtils;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import static pl.compan.docusafe.parametrization.aegon.overtime.OvertimeCn.*;

/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class OvertimeRaportFactory extends OvertimeFactory
{
    private static StringManager sm = StringManager.getManager(OvertimeRaportFactory.class.getPackage().getName());
    
    
    /**
     * Generuje dokument
     * @param doc
     * @param values
     * @return
     * @throws EdmException 
     */
    public static File pdfDocument(pl.compan.docusafe.core.base.Document doc, Map<String, Object> values, FieldsManager fm) throws EdmException
    {
        File pdfFile = null;
        EmployeeCard card = null;
        
        try
        {
            if (AvailabilityManager.isAvailable("aegon.docusafe.remote"))
            {
                // return loadPdfCardFromWS(userName, year);
            }
            card = getActiveEmployeeCardByUserName(doc.getAuthor());
         
            if(card == null)
                throw new EdmException("Brak aktualnej karty pracownika!");
            
            //AbsenceReportEntry entry = generateReportFromYear(year, card).get(0);

            File fontDir = new File(Docusafe.getHome(), "fonts");
            File arial = new File(fontDir, "arial.ttf");
            BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            Font fontSuperSmall = new Font(baseFont, (float) 6);
            Font fontSmall = new Font(baseFont, (float) 8);
            Font fontNormal = new Font(baseFont, (float) 12);
            Font fontLarge = new Font(baseFont, (float) 16);

            pdfFile = File.createTempFile("docusafe_", "_tmp");
            pdfFile.deleteOnExit();
            Document pdfDoc = new Document(PageSize.A4);
            PdfWriter.getInstance(pdfDoc, new FileOutputStream(pdfFile));

            pdfDoc.open();

            Table topTable = new Table(2);
            topTable.setCellsFitPage(true);
            topTable.setWidth(100);
            topTable.setPadding(3);
            topTable.setBorder(0);

            Paragraph title = new Paragraph();
            title.add(new Phrase("Karta Nadgodzin", fontLarge));
            title.add(new Phrase(" / Overtime card", fontSmall));

            Cell titleCell = new Cell(title);
            titleCell.setBorder(0);
            topTable.addCell(titleCell);
            // logo
            Image logo = Image.getInstance(Docusafe.getHome().getAbsolutePath() + "/img/" + Docusafe.getAdditionProperty("pdfCardGif"));
            Cell logoCell = new Cell(logo);
            logoCell.setBorder(0);
            topTable.addCell(logoCell);

            pdfDoc.add(topTable);
            pdfDoc.add(new Paragraph("Nazwisko i imi� : " + card.getLastName() + " " + card.getFirstName(), fontNormal));

            StringBuilder kpxs = new StringBuilder(card.getKPX());
            StringBuilder nazwas = new StringBuilder(card.getCompany());
            log.error("card"+card.getId());
            
            for (EmployeeCard othercard : OvertimeFactory.getActiveEmployeeCards(card))
            {
                kpxs.append(" , ");
                kpxs.append(othercard.getKPX());
                nazwas.append(" , ");
                nazwas.append(othercard.getCompany());
            }
            pdfDoc.add(new Paragraph("Numer pracownika : " + kpxs.toString(), fontNormal));
            pdfDoc.add(new Paragraph("Nazwa sp�ki : " + nazwas.toString(), fontNormal));
            pdfDoc.add(new Paragraph("Departament : " + card.getDepartment(), fontNormal));
            pdfDoc.add(new Paragraph("Dzia� : " + card.getDivision(), fontNormal));

            Table table = new Table(2);
            table.setCellsFitPage(true);
            table.setWidth(100);
            table.setPadding(3);
            table.setBorder(0);
            
            String timeCount = ((String)values.get(TIME_COUNT_CN));
            if(timeCount.length() > 5)
                timeCount = timeCount.substring(0, 4);
                
            List<String> texts = new ArrayList<String>();
            texts.add("Dokument id: " + doc.getId());
            texts.add("Suma nadgodzin: " + timeCount);
            for (String text : texts)
            {
                Cell cell = new Cell(new Phrase(text, fontNormal));
                cell.setBorder(0);
                table.addCell(cell);
            }

            pdfDoc.add(table);

            int[] widths = new int[]{1, 1};
            table = new Table(2);
            table.setCellsFitPage(true);
            table.setWidth(100);
            table.setWidths(widths);
            table.setPadding(3);
            
            for(String header : getDocumentColumnList())
            {
                table.addCell(OvertimeRaportFactory.getDefaultCell(sm.getString(header), fontSmall));
                if(header.equals(STATUS_FIELD_CN) || header.equals(FINAL_ACCEPTANCE_FIELD_CN) || header.equals(FLAG_CN))
                  if(fm.getValue(header) != null)  
                    table.addCell(OvertimeRaportFactory.getDefaultCell(fm.getValue(header).toString(), fontSmall));
                  else
                    table.addCell(OvertimeRaportFactory.getDefaultCell("", fontSmall));
                else
                {
                        if(values.get(header) != null)
                        {
                            String tmp = values.get(header).toString();
                            if(header.equals(TIME_FROM_CN) || header.equals(TIME_TO_CN))
                            {
                                //tymczasowa zmienna trzeba czasy obciac do 5 znak�w
                                if(tmp.length() > 5)
                                    tmp = tmp.substring(0,5);
                            }
                            table.addCell(OvertimeRaportFactory.getDefaultCell(tmp, fontSmall));
                        }
                        else
                            table.addCell(OvertimeRaportFactory.getDefaultCell("", fontSmall));
                    }
            }

            pdfDoc.add(table);
            pdfDoc.close();
        } catch (Exception e)
        {
            LoggerFactory.getLogger("lukaszw").error(e.getMessage(), e);
            throw new EdmException(e);
        }

        return pdfFile;
        
    }
    
    /**
     * Swraca list� nag��wk�w dla dokumentu
     * @return 
     */
    public static String[] getDocumentColumnList()
    {
        return new String[]{
            OVERTIME_DATE_CN,
            TIME_FROM_CN,
            TIME_TO_CN,
//            FLAG_CN,
            DESCRIPTION_CN,
            STATUS_FIELD_CN,
//            FINAL_ACCEPTANCE_FIELD_CN
        };
    }
    
    public static File pdfCard(String userName, int year) throws EdmException
    {
        File pdfFile = null;
        EmployeeCard card = null;
        try
        {
            if (AvailabilityManager.isAvailable("aegon.docusafe.remote"))
            {
                // return loadPdfCardFromWS(userName, year);
            }

            card = getActiveEmployeeCardByUserName(userName);
            //AbsenceReportEntry entry = generateReportFromYear(year, card).get(0);

            File fontDir = new File(Docusafe.getHome(), "fonts");
            File arial = new File(fontDir, "arial.ttf");
            BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            /** Lista nadgodzin w roku*/
            List<EmployeeOvertimeEntity> empOtEntites = OvertimeFactory.getEmployeeOvertimes(card, year);
            /** podsumowania dla pracownika w danym roku */
            EmployeeOvertimesSummary empOtSummary = EmployeeOvertimesSummary.getInstance(empOtEntites);

            String overtimesLastYear = OvertimeRaportFactory.getOvertimesYearaBack(card, year);

            Font fontSuperSmall = new Font(baseFont, (float) 6);
            Font fontSmall = new Font(baseFont, (float) 8);
            Font fontNormal = new Font(baseFont, (float) 12);
            Font fontLarge = new Font(baseFont, (float) 16);

            pdfFile = File.createTempFile("docusafe_", "_tmp");
            pdfFile.deleteOnExit();
            Document pdfDoc = new Document(PageSize.A4);
            PdfWriter.getInstance(pdfDoc, new FileOutputStream(pdfFile));

            pdfDoc.open();

            Table topTable = new Table(2);
            topTable.setCellsFitPage(true);
            topTable.setWidth(100);
            topTable.setPadding(3);
            topTable.setBorder(0);

            Paragraph title = new Paragraph();
            title.add(new Phrase("Karta Nadgodzin", fontLarge));
            title.add(new Phrase(" / Overtime card", fontSmall));

            Cell titleCell = new Cell(title);
            titleCell.setBorder(0);
            topTable.addCell(titleCell);
            // logo
            Image logo = Image.getInstance(Docusafe.getHome().getAbsolutePath() + "/img/" + Docusafe.getAdditionProperty("pdfCardGif"));
            Cell logoCell = new Cell(logo);
            logoCell.setBorder(0);
            topTable.addCell(logoCell);

            pdfDoc.add(topTable);
            pdfDoc.add(new Paragraph("Nazwisko i imi� : " + card.getLastName() + " " + card.getFirstName(), fontNormal));

            StringBuilder kpxs = new StringBuilder(card.getKPX());
            StringBuilder nazwas = new StringBuilder(card.getCompany());
            
            for (EmployeeCard othercard : OvertimeFactory.getActiveEmployeeCards(card))
            {
                kpxs.append(" , ");
                kpxs.append(othercard.getKPX());
                nazwas.append(" , ");
                nazwas.append(othercard.getCompany());
            }
            pdfDoc.add(new Paragraph("Numer pracownika : " + kpxs.toString(), fontNormal));
            pdfDoc.add(new Paragraph("Nazwa sp�ki : " + nazwas.toString(), fontNormal));
            pdfDoc.add(new Paragraph("Departament : " + card.getDepartment(), fontNormal));
            pdfDoc.add(new Paragraph("Dzia� : " + card.getDivision(), fontNormal));

            Table table = new Table(2);
            table.setCellsFitPage(true);
            table.setWidth(100);
            table.setPadding(3);
            table.setBorder(0);

            List<String> texts = new ArrayList<String>();
            texts.add("Nale�ne nadgodziny : " + empOtSummary.getAllHourLeft());
            texts.add("Pozosta�o z poprzedniego roku : " + overtimesLastYear);
            texts.add("��cznie : " + TimeUtils.summaryTimes(empOtSummary.getAllHourLeft(), overtimesLastYear));
            texts.add("Rok : " + year);

            for (String text : texts)
            {
                Cell cell = new Cell(new Phrase(text, fontNormal));
                cell.setBorder(0);
                table.addCell(cell);
            }

            pdfDoc.add(table);

            int[] widths = new int[]{1, 1, 1, 1, 1, 1};
            table = new Table(6);
            table.setCellsFitPage(true);
            table.setWidth(100);
            table.setWidths(widths);
            table.setPadding(3);

            for (String text : getYearHeaders())
            {
                Font fontHeader = fontSmall;
                fontHeader.setStyle(Font.BOLDITALIC);
                Cell cell = new Cell(new Phrase(text, fontHeader));
                cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                cell.setVerticalAlignment(Cell.ALIGN_CENTER);
                cell.setBorderWidthBottom(2);
                cell.setBorderWidthLeft(1);
                cell.setBorderWidthRight(1);

                table.addCell(cell);
            }

            for (EmployeeOvertimeEntity otEntity : empOtEntites)
            {
                otEntity.init();
                table.addCell(OvertimeRaportFactory.getDefaultCell(otEntity.getFirstDayOfMonth(), fontSmall));
                table.addCell(OvertimeRaportFactory.getDefaultCell(otEntity.getLastDayOfMonth(), fontSmall));
                table.addCell(OvertimeRaportFactory.getDefaultCell(otEntity.getSumHour() + ":" + otEntity.getSumMinute(), fontSmall));
                table.addCell(OvertimeRaportFactory.getDefaultCell(otEntity.getSumHourReceive() + ":" + otEntity.getSumMinuteReceive(), fontSmall));
                table.addCell(OvertimeRaportFactory.getDefaultCell(otEntity.getLeftHour() + ":" + otEntity.getLeftMinute(), fontSmall));
                table.addCell(OvertimeRaportFactory.getDefaultCell(otEntity.getOvertimeMonth(), fontSuperSmall));
//                table.addCell(OvertimeRaportFactory.getDefaultCell(" ", fontSmall));
            }
            /** Dodaje podsumowanie do tabeli */
            Cell summaryTextCell = OvertimeRaportFactory.getSummaryCell("PODSUMOWANIE", fontSmall);
            summaryTextCell.setColspan(2);
            summaryTextCell.setBorderWidthTop(2);
            summaryTextCell.setBorderWidthLeft(1);
            summaryTextCell.setBorderWidthRight(1);

            table.addCell(summaryTextCell);
            table.addCell(OvertimeRaportFactory.getSummaryCell(empOtSummary.getAllHour(), fontSmall));
            table.addCell(OvertimeRaportFactory.getSummaryCell(empOtSummary.getAllHourReceive(), fontSmall));
            table.addCell(OvertimeRaportFactory.getSummaryCell(empOtSummary.getAllHourLeft(), fontSmall));
//            table.addCell(OvertimeRaportFactory.getSummaryCell(" ", fontSmall));

            pdfDoc.add(table);
            pdfDoc.close();
        } catch (Exception e)
        {
            LoggerFactory.getLogger("lukaszw").debug(e.getMessage(), e);
            throw new EdmException(e);
        }

        return pdfFile;
    }

    public static File xlsCard(String userName, int year) throws EdmException
    {
        EmployeeCard card = null;
        try
        {
            card = getActiveEmployeeCardByUserName(userName);
            
            List<EmployeeOvertimeEntity> empOtEntites = OvertimeFactory.getEmployeeOvertimes(card, year);
            /** podsumowania dla pracownika w danym roku */
            EmployeeOvertimesSummary empOtSummary = EmployeeOvertimesSummary.getInstance(empOtEntites);

            String overtimesLastYear = OvertimeRaportFactory.getOvertimesYearaBack(card, year);
            
            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet sheet = wb.createSheet("raport");

            HSSFCellStyle cellStyle = wb.createCellStyle();
            cellStyle.setDataFormat(wb.createDataFormat().getFormat("dd/MM/yyyy"));

            DecimalFormat myFormatter = new DecimalFormat("000000");
            //myFormatter.format(12);

            short line = 0;
            HSSFRow head = sheet.createRow(line);
            
            int i = 0;
            for(String header: getYearHeaders())
            {
                head.createCell(i).setCellValue(header);
                i++;
            }
            
            for (EmployeeOvertimeEntity otEntity : empOtEntites)
            {
                line++;
                otEntity.init();
                HSSFRow row = sheet.createRow(line);
                
                row.createCell(0).setCellValue(otEntity.getFirstDayOfMonth());
                row.createCell(1).setCellValue(otEntity.getLastDayOfMonth());
                row.createCell(2).setCellValue(otEntity.getSumHour() + ":" + otEntity.getSumMinute());
                row.createCell(3).setCellValue(otEntity.getSumHourReceive() + ":" + otEntity.getSumMinuteReceive());
                row.createCell(4).setCellValue(otEntity.getLeftHour() + ":" + otEntity.getLeftMinute());
                row.createCell(5).setCellValue(otEntity.getOvertimeMonth());
//                row.createCell(6).setCellValue(" ");
            }
            
            line++;
            HSSFRow row = sheet.createRow(line);
             /** Dodaje podsumowanie do tabeli */
            row.createCell(0).setCellValue("PODSUMOWANIE");
            row.createCell(1).setCellValue(" ");
            row.createCell(2).setCellValue(empOtSummary.getAllHour());
            row.createCell(3).setCellValue(empOtSummary.getAllHourReceive());
            row.createCell(4).setCellValue(empOtSummary.getAllHourLeft());
//            row.createCell(5).setCellValue(" ");
            
            sheet.addMergedRegion(new CellRangeAddress(line,line,0,1));
              
            File file = File.createTempFile("docusafe", "overtimes");
            file.deleteOnExit();
            OutputStream os = new FileOutputStream(file);
            wb.write(os);
            os.close();
            
            return file;
            
        } catch (Exception ex)
        {
           LoggerFactory.getLogger("lukaszw").debug(ex.getMessage(), ex);
            throw new EdmException(ex); 
        }
    }
    
    
    /**
     * Tworzy dokument miesi�cznego zestawienia
     * @param userName
     * @param year
     * @return
     * @throws EdmException 
     */
    public static File pdfMonthlyCard(String userName, int year, int month) throws EdmException, Exception
    {
        File pdfFile = null;
        EmployeeCard card = null;
        
//        try{
            if (AvailabilityManager.isAvailable("aegon.docusafe.remote"))
            {
                // return loadPdfCardFromWS(userName, year);
            }

            card = getActiveEmployeeCardByUserName(userName);
            //AbsenceReportEntry entry = generateReportFromYear(year, card).get(0);

            File fontDir = new File(Docusafe.getHome(), "fonts");
            File arial = new File(fontDir, "arial.ttf");
            BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            /** Tworzy okiekt przechowuj�cy potrzebne dane*/
            EmployeeOvertimesMonthlyBean empMonthlyBean = new EmployeeOvertimesMonthlyBean(card, year, month);

            empMonthlyBean.getEmployeeMonthlyOvertimes();
            empMonthlyBean.init(); // oblicza potrzebne sumy u usuwa nie potrzebne miesi�ce

            List<EmployeeOvertimeMonthlyEntity> monthlyOvertimes = empMonthlyBean.getMonthlyOvertimes();

            String overtimes = empMonthlyBean.getOvertimes();
            String overtimesReceives = empMonthlyBean.getOvertimesReceives();

            Font fontSuperSmall = new Font(baseFont, (float) 6);
            Font fontSmall = new Font(baseFont, (float) 8);
            Font fontNormal = new Font(baseFont, (float) 12);
            Font fontLarge = new Font(baseFont, (float) 16);

            pdfFile = File.createTempFile("docusafe_", "_tmp");
            pdfFile.deleteOnExit();
            Document pdfDoc = new Document(PageSize.A4);
            PdfWriter.getInstance(pdfDoc, new FileOutputStream(pdfFile));

            pdfDoc.open();

            Table topTable = new Table(2);
            topTable.setCellsFitPage(true);
            topTable.setWidth(100);
            topTable.setPadding(3);
            topTable.setBorder(0);

            Paragraph title = new Paragraph();
            title.add(new Phrase("Karta Nadgodzin", fontLarge));
            title.add(new Phrase(" / Overtime card", fontSmall));

            Cell titleCell = new Cell(title);
            titleCell.setBorder(0);
            topTable.addCell(titleCell);
            // logo
            Image logo = Image.getInstance(Docusafe.getHome().getAbsolutePath() + "/img/" + Docusafe.getAdditionProperty("pdfCardGif"));
            Cell logoCell = new Cell(logo);
            logoCell.setBorder(0);
            topTable.addCell(logoCell);

            pdfDoc.add(topTable);
            pdfDoc.add(new Paragraph("Nazwisko i imi� : " + card.getLastName() + " " + card.getFirstName(), fontNormal));

            StringBuilder kpxs = new StringBuilder(card.getKPX());
            StringBuilder nazwas = new StringBuilder(card.getCompany());
            log.error("card"+card.getId());
            
            for (EmployeeCard othercard : OvertimeFactory.getActiveEmployeeCards(card))
            {
                kpxs.append(" , ");
                kpxs.append(othercard.getKPX());
                nazwas.append(" , ");
                nazwas.append(othercard.getCompany());
            }
            pdfDoc.add(new Paragraph("Numer pracownika : " + kpxs.toString(), fontNormal));
            pdfDoc.add(new Paragraph("Nazwa sp�ki : " + nazwas.toString(), fontNormal));
            pdfDoc.add(new Paragraph("Departament : " + card.getDepartment(), fontNormal));
            pdfDoc.add(new Paragraph("Dzia� : " + card.getDivision(), fontNormal));

            Table table = new Table(2);
            table.setCellsFitPage(true);
            table.setWidth(100);
            table.setPadding(3);
            table.setBorder(0);
            
            List<String> texts = new ArrayList<String>();
            texts.add("Miesi�c : " + DateUtils.getMonthName(month-1));
            texts.add("Rok : " + year);
            
            for (String text : texts)
            {
                Cell cell = new Cell(new Phrase(text, fontNormal));
                cell.setBorder(0);
                table.addCell(cell);
            }
            
            pdfDoc.add(table);

            int[] widths = new int[]{1, 1, 1, 1, 1};
            table = new Table(5);
            table.setCellsFitPage(true);
            table.setWidth(100);
            table.setWidths(widths);
            table.setPadding(3);

            for (String text : getMonthHeaders())
            {
                Font fontHeader = fontSmall;
                fontHeader.setStyle(Font.BOLDITALIC);
                Cell cell = new Cell(new Phrase(text, fontHeader));
                cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                cell.setVerticalAlignment(Cell.ALIGN_CENTER);
                cell.setBorderWidthBottom(2);
                cell.setBorderWidthLeft(1);
                cell.setBorderWidthRight(1);

                table.addCell(cell);
            }
            
            for (EmployeeOvertimeMonthlyEntity otEntity : monthlyOvertimes)
            {
                table.addCell(OvertimeRaportFactory.getDefaultCell(otEntity.getDate().toString(), fontSmall));
                table.addCell(OvertimeRaportFactory.getDefaultCell(otEntity.getOvertimes(), fontSmall));
                table.addCell(OvertimeRaportFactory.getDefaultCell(otEntity.getDescription(), fontSmall));
                table.addCell(OvertimeRaportFactory.getDefaultCell(otEntity.getFirstDateReceive(), fontSmall));
                table.addCell(OvertimeRaportFactory.getDefaultCell(otEntity.getFirstOvertimesReceive(), fontSmall));
                for(EmployeeOvertimeReceiveMonthlyEntity otReceiveEntity : otEntity.getEmpOvertimeReceive())
                {
                    table.addCell(OvertimeRaportFactory.getDefaultCell(" ", fontSmall));
                    table.addCell(OvertimeRaportFactory.getDefaultCell(" ", fontSmall));
                    table.addCell(OvertimeRaportFactory.getDefaultCell(" ", fontSmall));
                    table.addCell(OvertimeRaportFactory.getDefaultCell(otReceiveEntity.getDate(), fontSmall));
                    table.addCell(OvertimeRaportFactory.getDefaultCell(otReceiveEntity.getOvertimesReceive(), fontSmall));
                }
            }
            
            /** Dodaje podsumowanie do tabeli */
            Cell summaryTextCell = OvertimeRaportFactory.getSummaryCell("PODSUMOWANIE", fontSmall);
            summaryTextCell.setBorderWidthTop(2);
            summaryTextCell.setBorderWidthLeft(1);
            summaryTextCell.setBorderWidthRight(1);

            table.addCell(summaryTextCell);
            table.addCell(OvertimeRaportFactory.getSummaryCell(overtimes, fontSmall));
            table.addCell(OvertimeRaportFactory.getSummaryCell(" ", fontSmall));
            table.addCell(summaryTextCell);
            table.addCell(OvertimeRaportFactory.getSummaryCell(overtimesReceives, fontSmall));
            
            pdfDoc.add(table);
            pdfDoc.close();
            
//        }catch (Exception e)
//        {
//             LoggerFactory.getLogger("lukaszw").debug(e.getMessage(), e);
//            throw new EdmException(e);
//        }
        
        return pdfFile;
    }
    
    
    /**
     * 
     */
    public static File xlsMonthlyCard(String userName, int year, int month) throws EdmException, Exception
    {
        EmployeeCard card = null;
        try
        {
            card = getActiveEmployeeCardByUserName(userName);
             /** Tworzy okiekt przechowuj�cy potrzebne dane*/
            EmployeeOvertimesMonthlyBean empMonthlyBean = new EmployeeOvertimesMonthlyBean(card, year, month);

            empMonthlyBean.getEmployeeMonthlyOvertimes();
            empMonthlyBean.init(); // oblicza potrzebne sumy u usuwa nie potrzebne miesi�ce

            List<EmployeeOvertimeMonthlyEntity> monthlyOvertimes = empMonthlyBean.getMonthlyOvertimes();

            String overtimes = empMonthlyBean.getOvertimes();
            String overtimesReceives = empMonthlyBean.getOvertimesReceives();
            
            HSSFWorkbook wb = new HSSFWorkbook();
            HSSFSheet sheet = wb.createSheet("raport");

            HSSFCellStyle cellStyle = wb.createCellStyle();
            cellStyle.setDataFormat(wb.createDataFormat().getFormat("dd/MM/yyyy"));

            DecimalFormat myFormatter = new DecimalFormat("000000");
            //myFormatter.format(12);

            short line = 0;
            HSSFRow head = sheet.createRow(line);
            
            int i = 0;
            for(String header: getMonthHeaders())
            {
                head.createCell(i).setCellValue(header);
                i++;
            }
            
            for (EmployeeOvertimeMonthlyEntity otEntity : monthlyOvertimes)
            {
                line++;
                HSSFRow row = sheet.createRow(line);
                
                row.createCell(0).setCellValue(otEntity.getDate().toString());
                row.createCell(1).setCellValue(otEntity.getOvertimes());
                row.createCell(2).setCellValue(otEntity.getDescription());
                row.createCell(3).setCellValue(otEntity.getFirstDateReceive());
                row.createCell(4).setCellValue(otEntity.getFirstOvertimesReceive());
                
              for(EmployeeOvertimeReceiveMonthlyEntity otReceiveEntity : otEntity.getEmpOvertimeReceive())
                {
                    line++;
                    HSSFRow row2 = sheet.createRow(line);
                    row2.createCell(0).setCellValue(" ");
                    row2.createCell(1).setCellValue(" ");
                    row2.createCell(2).setCellValue(" ");
                    row2.createCell(3).setCellValue(otReceiveEntity.getDate());
                    row2.createCell(4).setCellValue(otReceiveEntity.getOvertimesReceive());
                }
            }
//            
            line++;
            HSSFRow row = sheet.createRow(line);
             /** Dodaje podsumowanie do tabeli */
            row.createCell(0).setCellValue("PODSUMOWANIE");
            row.createCell(1).setCellValue(overtimes);
            row.createCell(2).setCellValue(" ");
            row.createCell(3).setCellValue("PODSUMOWANIE");
            row.createCell(4).setCellValue(overtimesReceives);
            
            File file = File.createTempFile("docusafe", "overtimes");
            file.deleteOnExit();
            OutputStream os = new FileOutputStream(file);
            wb.write(os);
            os.close();
            
            return file;
            
        } catch (Exception ex)
        {
           LoggerFactory.getLogger("lukaszw").debug(ex.getMessage(), ex);
            throw new EdmException(ex); 
        }
        
    }
    
    /**
     * pobiera naglowki do rocznego podsumowania
     * @return 
     */
    public static String[] getYearHeaders()
    {
        return new String[]{
             "Data od", "Data do", "Liczba wypracowanych nadgodzin", "Liczba odebranych nadgodzin", "Pozosta�e nadgodziny", "Miesi�c"
        };
    }
    
    public static String[] getMonthHeaders()
    {
        return new String[]{
             "Data", "Liczba wypracowanych nadgodzin", "Uzasadnienie Nadgodzin", "Data odebrania", "Liczba odebranych nadgodzin"
        };
    }
    /**
     * Zwrca g��wne ustawienia dla kom�rki w pdf
     */
    private static Cell getDefaultCell(String text, Font font) throws BadElementException
    {
        Cell cell = new Cell(new Phrase(text, font));
        cell.setVerticalAlignment(Cell.ALIGN_CENTER);
        cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
        return cell;
    }

    private static Cell getSummaryCell(String text, Font font) throws BadElementException
    {
        Cell cell = new Cell(new Phrase(text, font));
        cell.setVerticalAlignment(Cell.ALIGN_CENTER);
        cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
        cell.setBorderWidthTop(2);
        cell.setBorderWidthLeft(1);
        cell.setBorderWidthRight(1);
        return cell;
    }

    public static String getOvertimesYearaBack(EmployeeCard card, int year) throws EdmException
    {
        List<EmployeeOvertimeEntity> tmpEmpOt = OvertimeFactory.getEmployeeOvertimes(card, year - 1);
        EmployeeOvertimesSummary tmpEmpSummary = EmployeeOvertimesSummary.getInstance(tmpEmpOt);
        return tmpEmpSummary.getAllHourLeft();
    }
}
