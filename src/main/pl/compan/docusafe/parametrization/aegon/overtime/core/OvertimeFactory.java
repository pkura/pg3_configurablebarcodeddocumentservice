package pl.compan.docusafe.parametrization.aegon.overtime.core;

import java.util.Collection;
import org.hibernate.Query;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.users.DSUser;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.dockinds.logic.RelatedDocumentBean;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import java.util.List;
import pl.compan.docusafe.util.TimeUtils;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.base.absences.FreeDay;
import pl.compan.docusafe.util.DateUtils;
import static pl.compan.docusafe.parametrization.aegon.overtime.OvertimeCn.*;

/**
 * Fabryka karty nadgodzin oraz generowania PDF
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class OvertimeFactory
{
    protected static final Logger log = LoggerFactory.getLogger(OvertimeFactory.class);
    private static final String _EMPLOYEE_COL_NAME = "employeeId";
    private static final String _OVERTIME_MONTH_COL_NAME = "overtimeMonth";
    private static final String _SUM_HOUR_COL_NAME = "sumohour";
    private static final String _SUM_MINUTE_COL_NAME = "sumominut";
    private static final String _OVERTIME_DATE_COL_NAME = "OVERTIME_DATE";
    private static final String _TIME_HOUR_COL_NAME = "ohour";
    private static final String _TIME_MINUTE_COL_NAME = "ominute";
    private static final String _DESCRIPTION_COL_NAME = "DESCRIPTION";
    private static final String _DOCUMENT_ID_COL_NAME = "DOCUMENT_ID";
    private static final String _OVERTIME_RECEIVE_COL_NAME = "OVERTIME_RECEIVE";
    private static final String _OVER_MONTH_COL_NAME = "OVER_MONTH";
    /**
     * Zapytanie zwracaj�ce wszystkie wpisy urlopowe danego pracownika w podanym roku
     */
    private static final String EMPLOYEE_OVERTIME_FROM_YEAR =
            "SELECT aa.EMPLOYEE_ID as employeeId, aa.overtime_month as overtimeMonth, FLOOR(SUM(aa.ohour)+SUM(aa.ominute)/60) as sumohour, (SUM(aa.ominute)%60) as sumominut "
            + " from (SELECT EMPLOYEE_ID, datepart(MONTH, OVERTIME_DATE) as overtime_month, SUM(cast(datename(\"HH\", TIME_COUNT) as INT)) as ohour,"
            + " SUM(cast(datename(\"MINUTE\", TIME_COUNT) as INT)) as ominute"
            + " FROM [DS_OVERTIME_CARD] as op"
            + " WHERE op.EMPLOYEE_ID = ? AND datename(YEAR, op.OVERTIME_DATE) = ? AND "
            + "(op.OVERTIME_RECEIVE <> 1 OR op.OVERTIME_RECEIVE IS NULL) "
            + " GROUP BY op.EMPLOYEE_ID, op.OVERTIME_DATE) as aa "
            + " GROUP BY aa.EMPLOYEE_ID, aa.overtime_month"
            + " ORDER BY aa.overtime_month;";
    /**
     * Zapytanie zwracaj�ce wszystkie odebrane wpisy urlopowe danego pracownika w podanym roku
     */
    private static final String EMPLOYEE_OVERTIME_RECEIVE_FROM_YEAR =
            "SELECT aa.EMPLOYEE_ID as employeeId, aa.overtime_month as overtimeMonth, FLOOR(SUM(aa.ohour)+SUM(aa.ominute)/60) as sumohour, (SUM(aa.ominute)%60) as sumominut "
            + " from (SELECT EMPLOYEE_ID, datepart(MONTH, OVERTIME_DATE) as overtime_month, SUM(cast(datename(\"HH\", TIME_COUNT) as INT)) as ohour,"
            + " SUM(cast(datename(\"MINUTE\", TIME_COUNT) as INT)) as ominute"
            + " FROM [DS_OVERTIME_CARD] as op"
            + " WHERE op.EMPLOYEE_ID = ? AND datename(YEAR, op.OVERTIME_DATE) = ? AND "
            + "(op.OVERTIME_RECEIVE  = 1) "
            + " GROUP BY op.EMPLOYEE_ID, op.OVERTIME_DATE) as aa "
            + " GROUP BY aa.EMPLOYEE_ID, aa.overtime_month"
            + " ORDER BY aa.overtime_month;";
    /**
     * Miesieczna lista nadgodzin pracownika
     */
    private static final String EMPLOYEE_OVERTIME_ALL_FROM_MONTH =
            "SELECT EMPLOYEE_ID, OVERTIME_DATE, OVERTIME_RECEIVE, DOCUMENT_ID, DESCRIPTION, datepart(\"MM\", OVERTIME_DATE) as OVER_MONTH, "
            + " SUM(cast(datename(\"HH\", TIME_COUNT) as INT))+SUM(cast(datename(\"MINUTE\", TIME_COUNT) as INT))/60 as ohour,"
            + " SUM(cast(datename(\"MINUTE\", TIME_COUNT) as INT))%60 as ominute "
            + " FROM [DS_OVERTIME_CARD] as op "
            + "WHERE op.EMPLOYEE_ID = ? AND datename(YEAR, op.OVERTIME_DATE) = ? "
            //       + " AND datepart(MONTH, op.OVERTIME_DATE) <= ? "
            + "GROUP BY op.EMPLOYEE_ID, op.OVERTIME_DATE,OVERTIME_RECEIVE, DOCUMENT_ID, DESCRIPTION "
            + "ORDER BY op.OVERTIME_RECEIVE,op.OVERTIME_DATE";
    
    /**
     * Obecne wnioski wniosek o odbi�r nadgodzin
     */    
    private static final String EMPLOYEE_OVERTIME_RECEIVE =  
            "SELECT FLOOR(SUM(aa.ohour)+SUM(aa.ominute)/60) as sumohour, (SUM(aa.ominute)%60) as sumominute "+
             "from (SELECT SUM(cast(datename(HH, TIME_COUNT) as INT)) as ohour, "+
             "SUM(cast(datename(MINUTE, TIME_COUNT) as INT)) as ominute "+
             "FROM [DSG_OVERTIME_RECEIVE] as op LEFT JOIN DS_DOCUMENT as dc ON dc.id = op.DOCUMENT_ID "+
             "WHERE dc.author = ? and op.status in (5,10) AND op.DOCUMENT_ID != ? "+
            "GROUP BY op.OVERTIME_DATE) as aa";

    /**
     * Obecne wnioski wniosek zlecenie nadgodzin
     */
    private static final String EMPLOYEE_OVERTIME =  
            "SELECT FLOOR(SUM(aa.ohour)+SUM(aa.ominute)/60) as sumohour, (SUM(aa.ominute)%60) as sumominute "+
             "from (SELECT SUM(cast(datename(HH, TIME_COUNT) as INT)) as ohour, "+
             "SUM(cast(datename(MINUTE, TIME_COUNT) as INT)) as ominute "+
             "FROM [DSG_OVERTIME_APP] as op LEFT JOIN DS_DOCUMENT as dc ON dc.id = op.DOCUMENT_ID "+
             "WHERE dc.author = ? and op.status in (5,10) AND op.DOCUMENT_ID != ? "+
            "GROUP BY op.OVERTIME_DATE) as aa";
    /**
     * Zapytanie HQL, kt�re wykorzystywane jest do wygenerowania raportu
     * nadgodzin wszystkich lub wybranych pracownik�w
    */
    private static final String REPORT_FROM_YEAR_HQL = 
                //"select emc "
                "from " + EmployeeCard.class.getName()
              + " emc left join fetch emc.overtimes op " //outer   or op.overtimeDate is NULL
              + "where (datename(YEAR, op.overtimeDate) = :YEAR) "
              + ":SEARCH_QUERY "
              + "order by emc.lastName asc, emc.firstName asc";
	
    
    
    /**
     * Zapisuje kart� pracownika
     * @param empCardId
     * @param documentId
     * @param timeFrom
     * @param timeTo
     * @param timeCount
     * @param Description
     * @throws EdmException 
     */
    public static void chargeOvertimes(EmployeeCard empCard, Long documentId, Date date, String timeFrom, String timeTo, String timeCount, String Description, Boolean receive) throws EdmException
    {
        OvertimeCard oc = null;
        
        try{
            oc = OvertimeCard.findByDocumentId(documentId);
        } catch (NullPointerException ex){
        }
        
        if(oc == null)
        {
            oc = new OvertimeCard();
            oc.setEmployeeId(empCard.getId());
            oc.setDocumentId(documentId);
        }
        
        oc.setEmployeeId(empCard.getId());
        oc.setDocumentId(documentId);
        oc.setOvertimeDate(date);
        oc.setTimeFrom(timeFrom);
        oc.setTimeTo(timeTo);
        oc.setTimeCount(timeCount);
        oc.setDescription(Description);
        oc.setOvertimeReceive(receive);

        if(oc.getId() == null)
            oc.saveWithPS();
        else
            oc.updateWithPS();
    }

    /**
     * Usuwa dany wpis nadgodzin
     * @throws EdmException 
     */
    public static void unchargeOvertimes(EmployeeCard empCard, Long documentId) throws EdmException
    {
       OvertimeCard oc = null;
        
        try{
            if(documentId == null)
                return;
            
            oc = OvertimeCard.find(empCard.getId(), documentId);
        } catch (NullPointerException ex){
            
        }
        
       if(oc == null)
        {
            oc = new OvertimeCard();
            oc.setEmployeeId(empCard.getId());
            oc.setDocumentId(documentId);
        }

       oc.delete();
    }
    
    /** Sprawdza czy dzie� mo�e by� dniem wolnym*/
    public static void isFreeDay(Date date) throws EdmException
    {
         FreeDay.isFreeDay(date);
    }
    
    public static List<EmployeeOvertimeEntity> getEmployeeOvertimes(EmployeeCard empCard, int year) throws EdmException
    {
        return getEmployeeOvertimes(empCard,year, true);
    }
    /**
     * Pobiera liste nadgodzin pracownika w danym roku kalendarzowym
     * @param empCard
     * @param year
     * @return List<EmployeeOvertimeEntity>
     * @throws EdmException 
     */
    public static List<EmployeeOvertimeEntity> getEmployeeOvertimes(EmployeeCard empCard, int year, boolean withEmptyMonths) throws EdmException
    {
        List<EmployeeOvertimeEntity> overtimesList = new ArrayList<EmployeeOvertimeEntity>();
        try
        {
            EmployeeOvertimeEntity empOvertimeEntity;
            Map<Integer, EmployeeOvertimeEntity> overtimeEntities;
            //mapa z pustymi miesiacami
            if(withEmptyMonths)
                overtimeEntities = OvertimeFactory.getOvertimesMap();
            else
                overtimeEntities = new HashMap<Integer, EmployeeOvertimeEntity>();
                
            PreparedStatement ps = DSApi.context().prepareStatement(EMPLOYEE_OVERTIME_FROM_YEAR);
            ps.setLong(1, empCard.getId());
            ps.setInt(2, year);
            //zgloszone nadgodziny
            ResultSet rsOvertime = ps.executeQuery();

            while (rsOvertime.next())
            {
                empOvertimeEntity = null;
                int month = rsOvertime.getInt(_OVERTIME_MONTH_COL_NAME);
                empOvertimeEntity = new EmployeeOvertimeEntity(
                        month,
                        rsOvertime.getString(_OVERTIME_MONTH_COL_NAME),
                        rsOvertime.getString(_SUM_HOUR_COL_NAME),
                        rsOvertime.getString(_SUM_MINUTE_COL_NAME));

                overtimeEntities.put(new Integer(month), empOvertimeEntity);
            }

            ps = DSApi.context().prepareStatement(EMPLOYEE_OVERTIME_RECEIVE_FROM_YEAR);
            ps.setLong(1, empCard.getId());
            ps.setInt(2, year);

            //odebrane nadgodziny
            ResultSet rsOvertimeReceive = ps.executeQuery();

            while (rsOvertimeReceive.next())
            {
                int month = rsOvertimeReceive.getInt(_OVERTIME_MONTH_COL_NAME);

                empOvertimeEntity = overtimeEntities.get(new Integer(month));
                if(empOvertimeEntity == null)
                {
                    empOvertimeEntity = new EmployeeOvertimeEntity( month, "","00", "00");
                }
                
                empOvertimeEntity.setSumHourReceive(rsOvertimeReceive.getString(_SUM_HOUR_COL_NAME));
                empOvertimeEntity.setSumMinuteReceive(rsOvertimeReceive.getString(_SUM_MINUTE_COL_NAME));

                overtimeEntities.put(new Integer(month), empOvertimeEntity);
            }

            Set<Entry<Integer, EmployeeOvertimeEntity>> mapSet = overtimeEntities.entrySet();

            Iterator it = mapSet.iterator();

            while (it.hasNext())
            {
                empOvertimeEntity = null;
                Entry me = (Entry) it.next();
                empOvertimeEntity = (EmployeeOvertimeEntity) me.getValue();
                empOvertimeEntity.init(); // sprawdzenie poprawno��
                overtimesList.add(empOvertimeEntity);
            }

        } catch (SQLException e)
        {
            throw new EdmException(e);
        }
        catch (Exception ex)
        {
            log.error("nieoczekiwany blad", ex);
        }
        return overtimesList;
    }

    /**
     * Podaje miesieczna liste nadgodzin pracownika
     * @param empCard
     * @param year
     * @param month
     * @return
     * @throws SQLException
     * @throws EdmException 
     */
    public static List<EmployeeOvertimeMonthlyEntity> getEmployeeMonthlyOvertimes(EmployeeCard empCard, int year, int month) throws SQLException, EdmException
    {
        PreparedStatement ps = DSApi.context().prepareStatement(EMPLOYEE_OVERTIME_ALL_FROM_MONTH);
        /** Lista wypracowanych nadgodzin*/
        List<EmployeeOvertimeMonthlyEntity> monthlyOvertimes = new ArrayList<EmployeeOvertimeMonthlyEntity>();
        List<EmployeeOvertimeMonthlyEntity> newOvertimes = new ArrayList<EmployeeOvertimeMonthlyEntity>();
        /** Lista nadgodzin do odbioru */
        List<EmployeeOvertimeReceiveMonthlyEntity> monthlyReceive = new ArrayList<EmployeeOvertimeReceiveMonthlyEntity>();
        /** Dodaje odebrane nadgodziny */
        EmployeeOvertimeReceiveMonthlyEntity emMonthlyReceive;
        /** Dodaje wyrobione nadgodziny */
        EmployeeOvertimeMonthlyEntity emMonthly;
        EmployeeOvertimeMonthlyEntity emMonthlyLast = new EmployeeOvertimeMonthlyEntity();
        
        ps.setLong(1, empCard.getId());
        ps.setInt(2, year);
        // ps.setInt(3, month);

        ResultSet rs = ps.executeQuery();
        while (rs.next())
        {
            int receive = rs.getInt(_OVERTIME_RECEIVE_COL_NAME);
            int tmpMonth = rs.getInt(_OVER_MONTH_COL_NAME);
            String overtimes = TimeUtils.parseMinuteToHours(rs.getInt(_TIME_HOUR_COL_NAME), rs.getInt(_TIME_MINUTE_COL_NAME));
            if (receive == 1)
            {
                emMonthlyReceive = new EmployeeOvertimeReceiveMonthlyEntity(
                        rs.getString(_OVERTIME_DATE_COL_NAME),
                        overtimes,
                        tmpMonth);
                emMonthlyReceive.hours = rs.getInt(_TIME_HOUR_COL_NAME);
                emMonthlyReceive.minutes = rs.getInt(_TIME_MINUTE_COL_NAME);
                
                monthlyReceive.add(emMonthlyReceive);
            } else
            {
                emMonthly = new EmployeeOvertimeMonthlyEntity(
                        rs.getDate(_OVERTIME_DATE_COL_NAME),
                        overtimes,
                        rs.getString(_DESCRIPTION_COL_NAME),
                        rs.getLong(_DOCUMENT_ID_COL_NAME),
                        tmpMonth);

                emMonthly.hours = rs.getInt(_TIME_HOUR_COL_NAME);
                emMonthly.minutes = rs.getInt(_TIME_MINUTE_COL_NAME);
                monthlyOvertimes.add(emMonthly);
            }
        }
        
        /** iterujemy po wyrobionych nadgodzinach*/
        Iterator itOvertimes = monthlyOvertimes.iterator();
        /** iterator po odebranych*/
        Iterator itReceives;
        /** Dla tymczasowych element�w do dodawania nastepna iteracje*/
//        EmployeeOvertimeReceiveMonthlyEntity tmpEmOverReceives = null;
        Calendar calendarO = DateUtils.getGregorianCalendar();
        Calendar calendarR = DateUtils.getGregorianCalendar();
        Calendar calstatic = DateUtils.getGregorianCalendar();
        calstatic.set(1970, 0, 1, 0, 0, 0);
        Date DateStatic = calstatic.getTime();
        
        while (itOvertimes.hasNext())
        {
            emMonthly = (EmployeeOvertimeMonthlyEntity) itOvertimes.next();
            itReceives = monthlyReceive.iterator();
            int overtimeHour = emMonthly.hours;
            int overtimeMinute = emMonthly.minutes;

            while (itReceives.hasNext())
            {
                calendarO.set(1970, 0, 1, overtimeHour, overtimeMinute, 0);
                emMonthlyReceive = (EmployeeOvertimeReceiveMonthlyEntity) itReceives.next();
                calendarR.set(1970, 0, 1, emMonthlyReceive.hours, emMonthlyReceive.minutes, 0);
                
                Date date = new Date((calendarO.getTimeInMillis() - calendarR.getTimeInMillis() - 60 * 60 * 1000));

                if (!DateUtils.isGreaterDate(DateStatic, date))
                {
                    emMonthly.addEmpOvertimeReceiveEntity(emMonthlyReceive);
                    itReceives.remove();
                    
                    String otHours = TimeUtils.parseSmallValue(overtimeHour) + ":" + TimeUtils.parseSmallValue(overtimeMinute);
                    String otHoursReceive = TimeUtils.parseSmallValue(emMonthlyReceive.hours) + ":" + TimeUtils.parseSmallValue(emMonthlyReceive.minutes);
                    //odejmuje godziny od siebie
                    String otResult = TimeUtils.substractTimes(otHours, otHoursReceive); 
                    String[] otArray = otResult.split(":");
                    
                    overtimeHour = Integer.parseInt(otArray[0]);
                    overtimeMinute = Integer.parseInt(otArray[1]);
                    
                    continue;
                } else
                {
                    //Prawdziwa r�nica
                    EmployeeOvertimeReceiveMonthlyEntity copyEmMonthly = 
                            new EmployeeOvertimeReceiveMonthlyEntity(emMonthlyReceive.getDate(), 
                                            emMonthlyReceive.getOvertimesReceive(), 
                                            emMonthlyReceive.getMonth());
                    
                    String otHours = TimeUtils.parseSmallValue(overtimeHour) + ":" + TimeUtils.parseSmallValue(overtimeMinute);
                    String otHoursReceive = TimeUtils.parseSmallValue(emMonthlyReceive.hours) + ":" + TimeUtils.parseSmallValue(emMonthlyReceive.minutes);
                    //odejmuje godziny od siebie
                    String otResult = TimeUtils.substractTimes(otHoursReceive, otHours); 
                    String[] otArray = otResult.split(":");
                    
                    int tmpHour = Integer.parseInt(otArray[0]);
                    int tmpMinute = Integer.parseInt(otArray[1]);
                    
                    emMonthlyReceive.setOvertimesReceive(TimeUtils.parseMinuteToHours(overtimeHour, overtimeMinute));
                    emMonthlyReceive.hours = overtimeHour;
                    emMonthlyReceive.minutes = overtimeMinute;         
                    
                    emMonthly.addEmpOvertimeReceiveEntity(emMonthlyReceive);
                    itReceives.remove();
                   // odkladac na stos element z data i godzina kt�ra pozostala aby dodac do 
                    copyEmMonthly.setOvertimesReceive(TimeUtils.parseMinuteToHours(tmpHour, tmpMinute));

                    copyEmMonthly.hours = tmpHour;
                    copyEmMonthly.minutes = tmpMinute;
                    
                    if(itOvertimes.hasNext())
                        monthlyReceive.add(0, copyEmMonthly);
                    else
                        emMonthly.addEmpOvertimeReceiveEntity(copyEmMonthly);
                    break;
                }
            }

            emMonthly.setFirstElements();
            newOvertimes.add(emMonthly);
        }
        
        try
        {
            //petla dodaje pozostale elementy do ostatniego wpisu
            itReceives = monthlyReceive.iterator();
            emMonthlyLast = (EmployeeOvertimeMonthlyEntity) newOvertimes.get(newOvertimes.size()-1);
            newOvertimes.remove(newOvertimes.size()-1);

            while(itReceives.hasNext())
            {
                emMonthlyReceive = (EmployeeOvertimeReceiveMonthlyEntity) itReceives.next();
                emMonthlyLast.addEmpOvertimeReceiveEntity(emMonthlyReceive);
            }

            if(emMonthlyLast.getEmpOvertimeReceive().size() > 0 || emMonthlyLast != null)
                newOvertimes.add(emMonthlyLast);
        } catch (ArrayIndexOutOfBoundsException ex)
        {
        }
        
        return newOvertimes;
    }
            
    /**
     * Zwraca aktywn� kart� pracownika na podstawie nazwy usera.
     * 
     * @param userName
     * @return
     * @throws EdmException
     */
    public static EmployeeCard getActiveEmployeeCardByUserName(String userName) throws EdmException
    {
        return (EmployeeCard) DSApi.context().session().createQuery(
                "from " + EmployeeCard.class.getName() + " e where e.externalUser = ? and e.employmentEndDate is null order by e.employmentStartDate desc").setParameter(0, userName).setMaxResults(1).uniqueResult();
    }

    /**
     * Zwr�cenie reszty aktywnych kart pracowniczych na podstawie przekazanej karty
     * 
     * @param card
     * @return
     * @throws EdmException
     */
    @SuppressWarnings("unchecked")
    public static List<EmployeeCard> getActiveEmployeeCards(EmployeeCard card) throws EdmException
    {
        return DSApi.context().session().createQuery(
                "from " + EmployeeCard.class.getName() + " e where e.employmentEndDate is null and e.id != ? and e.user = ?") //.setString(0, empCard.getKPX())
                .setLong(0, card.getId()).setParameter(1, card.getUser()).list();
    }

    public List<RelatedDocumentBean> getReleatedDocuments(Long id) throws EdmException
    {
        GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("Europe/Warsaw"));
        int year = calendar.get(GregorianCalendar.YEAR);
        List<Object[]> res = DSApi.context().session().createSQLQuery(
                "SELECT d.ID, d.TITLE, d.AUTHOR FROM DS_DOCUMENT d JOIN DSG_OVERTIME_RECEIVE as o ON o.DOCUMENT_ID = d.ID AND datename(YEAR, o.OVERTIME_DATE) = ?  AND o.STATUS = "+ STATUS_ZAAKCEPTOWANY +" WHERE d.author = ( SELECT author from DS_DOCUMENT WHERE id = ?) ORDER BY d.MTIME asc;").setInteger(0, year).setLong(1, id).list();

        List<RelatedDocumentBean> beans = new ArrayList<RelatedDocumentBean>();
        int i = 0;
        for (Object[] row : res)
        {
            beans.add(createReleatedDocumentBean(row, ++i, year));
        }

        return beans;
    }

    /**
     * Tworzy bean powi�zanego dokumentu
     * 
     * @param row
     * @return
     * @throws EdmException
     */
    private RelatedDocumentBean createReleatedDocumentBean(Object[] row, int i, int year) throws EdmException
    {
        Long docId = ((BigDecimal) row[0]).longValue();
        String title = "Wniosek Nr " + i + "/" + year;
        String activityKey = null;
        String docType = null;
        Long attachmentId = null;
        String attachmentMime = null;

//        Object r = DSApi.context().session().createSQLQuery("SELECT t.dsw_activity_activity_key, t.document_type FROM DSW_TASKLIST t "
//                + "WHERE t.dso_document_id = ? AND t.assigned_user = ?").setLong(0, docId).setString(1, DSApi.context().getPrincipalName()).uniqueResult();
//        if (r != null)
//        {
//            Object[] task = (Object[]) r;
//            activityKey = (String) task[0];
//            docType = (String) task[1];
//        }

        // pobranie ID za��cznika
        Object[] res = (Object[]) DSApi.context().session().createSQLQuery("SELECT TOP 1 ar.ID, ar.MIME FROM DS_ATTACHMENT a join DS_ATTACHMENT_REVISION ar on a.ID = ar.ATTACHMENT_ID "
                + "WHERE a.DOCUMENT_ID = :docId AND a.DELETED = 0 AND (a.cn IS NULL OR a.cn <> 'nw_signature') ORDER BY ar.REVISION DESC").setParameter("docId", docId).uniqueResult();

        if (res != null)
        {
            attachmentId = ((BigDecimal) res[0]).longValue();
            attachmentMime = (String) res[1];
        }

        RelatedDocumentBean rdb = new RelatedDocumentBean(docId, title, activityKey, docType, attachmentId, attachmentMime);
        return rdb;
    }

    /**
     * zwraca poczatkowa mape pustych miesiecy nadgodzin potem poszczegolne sa nadpisywane
     * @return 
     */
    public static Map<Integer, EmployeeOvertimeEntity> getOvertimesMap()
    {
        Map<Integer, EmployeeOvertimeEntity> overtimeEntities = new HashMap<Integer, EmployeeOvertimeEntity>();
        overtimeEntities.put(new Integer(1), OvertimeFactory.getEmptyMonthRecord(1));
        overtimeEntities.put(new Integer(2), OvertimeFactory.getEmptyMonthRecord(2));
        overtimeEntities.put(new Integer(3), OvertimeFactory.getEmptyMonthRecord(3));
        overtimeEntities.put(new Integer(4), OvertimeFactory.getEmptyMonthRecord(4));
        overtimeEntities.put(new Integer(5), OvertimeFactory.getEmptyMonthRecord(5));
        overtimeEntities.put(new Integer(6), OvertimeFactory.getEmptyMonthRecord(6));
        overtimeEntities.put(new Integer(7), OvertimeFactory.getEmptyMonthRecord(7));
        overtimeEntities.put(new Integer(8), OvertimeFactory.getEmptyMonthRecord(8));
        overtimeEntities.put(new Integer(9), OvertimeFactory.getEmptyMonthRecord(9));
        overtimeEntities.put(new Integer(10), OvertimeFactory.getEmptyMonthRecord(10));
        overtimeEntities.put(new Integer(11), OvertimeFactory.getEmptyMonthRecord(11));
        overtimeEntities.put(new Integer(12), OvertimeFactory.getEmptyMonthRecord(12));

        return overtimeEntities;
    }

    /**
     * Zwraca pusty miesiac nadgodzin
     * @return 
     */
    public static EmployeeOvertimeEntity getEmptyMonthRecord(Integer month)
    {
        EmployeeOvertimeEntity empOvertimeEntity = new EmployeeOvertimeEntity(
                month,
                month.toString(),
                "00",
                "00");
        empOvertimeEntity.setSumHourReceive("00");
        empOvertimeEntity.setSumMinuteReceive("00");

        return empOvertimeEntity;
    }
    
    public static List<DSUser> getListAcceptanceSubordinates(String username)
    {
        List<DSUser> users = new ArrayList<DSUser>();
        try
        {
//            usersByDocumentToUpdate = ArrayList<DSUser>();
//                DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
                
                users = DSUser.listAcceptanceSubordinates(username);
            
        } catch (EdmException e){
            log.error("",e);

            //DSApi.context().setRollbackOnly();
        } finally{
//            try{
//                DSApi.close();
//            } catch (EdmException e){}
        }

        return users;
    }
    
    
    public static List<String> getBossesFromAbsence(String username) throws EdmException
    {
        
       String grupaUrlopow = Docusafe.getAdditionProperty("enadgodziny.kodGrupyAkceptacyjnej");
        try{
            List<String> users = new ArrayList<String>();
            String sql = "SELECT distinct ac.username "+
                         "FROM ds_acceptance_condition as ac LEFT JOIN ds_acceptance_division as ad ON ad.CODE = ac.username "+
//                        "LEFT JOIN ds_acceptance_division as ad2 ON ad.PARENT_ID = ad2.ID "+
//                        "LEFT JOIN ds_acceptance_division as ad3 ON ad2.PARENT_ID = ad3.ID "+
                        "WHERE fieldValue = ? AND cn = ?";
            // AND ad3.CODE = ? AND ad2.CODE = ?;

           PreparedStatement ps = DSApi.context().prepareStatement(sql);
           ps.setString(1, username);
           ps.setString(2, SZEF_DZIALU);
//           ps.setString(3, grupaUrlopow);
//           ps.setString(4, SZEF_DZIALU);

           ResultSet rs = ps.executeQuery();

           while(rs.next()){
               users.add(rs.getString(1));
           }

           return users;
        } catch (SQLException ex)
        {
            log.error("", ex);
            throw new EdmException("Nie pobra�em prze�o�onych z drzewa akceptacji z " + grupaUrlopow);
        }
    }
    
    /**
     * Obecne zgloszone dokumenty o odbi�r nadgodzin
     * @param username
     * @param documentId
     * @return
     * @throws EdmException 
     */
    public static String getEmployeeReceiveApplications(String username, Long documentId) throws EdmException
    {
        try{
        PreparedStatement ps = DSApi.context().prepareStatement(EMPLOYEE_OVERTIME_RECEIVE);
        String time = "00:00";
        ps.setString(1,username);
        ps.setLong(2, documentId);

        ResultSet rs = ps.executeQuery();
        
        while(rs.next())
        {
            int hour = rs.getInt(1);
            int minute = rs.getInt(2);
            time = TimeUtils.parseSmallValue(hour) + ":" + TimeUtils.parseSmallValue(minute);
        }

        return time;
        } catch (SQLException ex)
        {
            log.error("",ex);
            throw new EdmException("Wyst�pi� nieoczekiwany b��d!");
        }
    }
    
    /**
     * Obecne dokumnety zlecenie nadgodzin
     */
    public static String getEmployeeAppApplications(String username, Long documentId) throws EdmException
    {
        try{
        PreparedStatement ps = DSApi.context().prepareStatement(EMPLOYEE_OVERTIME);
        String time = "00:00";
        ps.setString(1,username);
        ps.setLong(2, documentId);

        ResultSet rs = ps.executeQuery();
        
        while(rs.next())
        {
            int hour = rs.getInt(1);
            int minute = rs.getInt(2);
            time = TimeUtils.parseSmallValue(hour) + ":" + TimeUtils.parseSmallValue(minute);
        }

        return time;
        
        } catch (SQLException ex)
        {
            log.error("",ex);
            throw new EdmException("Wyst�pi� nieoczekiwany b��d!");
        }
    }
    
        /**
	 * Generuje raport nadgodzin dla wybranych pracownik�w w
	 * podanym roku. Obiekt employeeCard ustala pola po jakich ma by� 
	 * przeprowadzone wyszukiwanie. 
	 * 
	 * @param year
	 * @param employeeCard
	 * @param pageNumber Numer strony
	 * @param maxResults Liczba rezultat�w
	 * @return
	 * @throws EdmException
	 */
	public static List<EmployeeCard> generateReportFromYear(int year, EmployeeCard employeeCard, int pageNumber, int maxResults)
		throws EdmException
	{
		// utworzenie mapy z parametrami wyszukiwania
		Map<String, Object[]> queryParams = buildSearchQueryMapByEmployeeCard(employeeCard);
	
		@SuppressWarnings("unchecked")
		// utworzenie raportu wynikowego
		List<EmployeeCard> rows = createQueryObject(queryParams, year).setFirstResult(pageNumber).setMaxResults(maxResults).list();
		return rows;
	}
        
        /**
	 * Generuje raport nadgodzin dla wybranych pracownik�w w
	 * podanym roku. Obiekt employeeCard ustala pola po jakich ma by� 
	 * przeprowadzone wyszukiwanie. 
	 * 
	 * @param year
	 * @param employeeCard
	 * @return
	 * @throws EdmException
	 */
        public static Query generateReportFromYear(int year, EmployeeCard employeeCard) throws EdmException
        {
            // utworzenie mapy z parametrami wyszukiwania
		Map<String, Object[]> queryParams = buildSearchQueryMapByEmployeeCard(employeeCard);
	
		// utworzenie raportu wynikowego
		return createQueryObject(queryParams, year);
        }
        
        /**
	 * Tworzy obiekt query zapytania
	 * 
	 * @param queryParams
	 * @param year
	 * @return
	 * @throws EdmException
	 */
	@SuppressWarnings("unchecked")
	private static Query createQueryObject(Map<String, Object[]> queryParams, int year)
		throws EdmException
	{
		Query query = DSApi.context().session().createQuery(
				REPORT_FROM_YEAR_HQL.replaceFirst(":SEARCH_QUERY", createWhereClause(queryParams)))
				.setInteger("YEAR", year);
		
		// ustawienie dodatkowych parametr�w zapytania z warto�ci mapy
		if (queryParams != null)
			for (Object[] param : queryParams.values())
				if ((param[1] instanceof Collection))
					query.setParameterList((String) param[0], (Collection) param[1]);
				else if (param[1].getClass().isArray())
					query.setParameterList((String) param[0], (Object[]) param[1]);
				else
					query.setParameter((String) param[0], param[1]);
		
		return query;
	}
        
        /**
	 * Buduje klauzul� where z podanych parametr�w zapytania
	 * 
	 * @param queryParams
	 * @return
	 */
	private static String createWhereClause(Map<String, Object[]> queryParams)
	{
		if (queryParams == null)
			return "";
			
		StringBuilder hql = new StringBuilder();
		Object[] keys = queryParams.keySet().toArray();
		if (keys.length > 0)
			hql.append(" and ");
		// p�tla buduj�ca ci�g zapytania
		for (int i = 0; i < keys.length; i++)
		{
			hql.append(keys[i]);
			if (i + 1 < keys.length)
				hql.append(" and ");
		}
		
		return hql.toString();
	}
        
        /**
	 * Metoda buduj�ca map� z dodatkowymi parametrami wyszukiwania na podstawie obiektu employeeCard
	 * 
	 * @param employeeCard
	 * @return
	 */
	public static Map<String, Object[]> buildSearchQueryMapByEmployeeCard(EmployeeCard employeeCard)
	{
		if (employeeCard == null)
			return null;
		
		// Posta� mapy jest nast�puj�ca: 
		// String - cz�� zapytania HQL
		// Object[] - tablica z dwoma elementami: nazwa parametru i warto�� parametru
		Map<String, Object[]> queryParams = new HashMap<String, Object[]>();
		
		// Identyfikator
		if (employeeCard.getId() != null)
			queryParams.put("emc.id = :ID", 
					new Object[] {"ID", employeeCard.getId()});
		// Numer ewidencyjny
		if (!StringUtils.isEmpty(employeeCard.getKPX()))
			queryParams.put("emc.KPX like :KPX", 
					new Object[] {"KPX", employeeCard.getKPX() + "%"});
		
		if (employeeCard.getUser() != null)
		{
			// Nazwisko
			if (!StringUtils.isEmpty(employeeCard.getUser().getLastname()))
				queryParams.put("(lower(emc.lastName) like :LASTNAME or emc.externalUser like :LASTNAME)", 
						new Object[] {"LASTNAME", employeeCard.getUser().getLastname().toLowerCase() + "%"});
			// Imi�
			if (!StringUtils.isEmpty(employeeCard.getUser().getFirstname()))
				queryParams.put("lower(emc.firstName) like :FIRSTNAME", 
						new Object[] {"FIRSTNAME", employeeCard.getUser().getFirstname().toLowerCase() + "%"});
		}
		
		// Stanowisko
		if (!StringUtils.isEmpty(employeeCard.getPosition()))
			queryParams.put("lower(emc.position) like :POSITION", 
					new Object[] {"POSITION", "%" + employeeCard.getPosition().toLowerCase() + "%"});
		// Departament
		if (!StringUtils.isEmpty(employeeCard.getDepartment()))
			queryParams.put("lower(emc.department) like :DEPARTMENT", 
					new Object[] {"DEPARTMENT", "%" + employeeCard.getDepartment().toLowerCase() + "%"});
		// Sp�ka
		if (!StringUtils.isEmpty(employeeCard.getCompany()))
			queryParams.put("lower(emc.company) like :COMPANY", 
					new Object[] {"COMPANY", "%" + employeeCard.getCompany().toLowerCase() + "%"});
		
		return queryParams;
	}
        
        
    /**
     * Nalicza dla wybranych u�ytkownik�w nadgodziny na przysz�y rok
     */
    public static void chargeOvertimesForNewYear(List<EmployeeCard> employees, int year)
    {
        try
        {
            for (EmployeeCard empCard : employees)
            {
                //nadgodziny pracownika w poprzednim roku
                List<EmployeeOvertimeEntity> empOT = OvertimeFactory.getEmployeeOvertimes(empCard, year-1);
                /** podsumowania dla pracownika w poprzednim roku */
                EmployeeOvertimesSummary empOtSummary = EmployeeOvertimesSummary.getInstance(empOT);

                String toCharged = empOtSummary.getAllHourLeft();

                String[] overtimeToCharge = TimeUtils.createChargedOvertimes(toCharged);

                int i = 1;
                for (String overtime : overtimeToCharge)
                {
                    if("00:00".equals(overtime))
                        continue;
                    
                    GregorianCalendar calendar = DateUtils.getFirstDayOfMonth(0, year);
                    calendar.set(GregorianCalendar.DAY_OF_MONTH, i++);
                    OvertimeCard ot = new OvertimeCard();
                    ot.setDescription("Nadgodziny naliczone za rok " + (year-1));
                    ot.setTimeFrom("00:00");
                    ot.setTimeTo(overtime);
                    ot.setTimeCount(overtime);
                    ot.setEmployeeId(empCard.getId());
                    ot.setOvertimeDate(calendar.getTime());
                    ot.saveWithPS();
                }

            }

        } catch (Exception ex)
        {
            log.error(" ", ex);
        }
        }
}
