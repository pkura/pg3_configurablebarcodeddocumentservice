package pl.compan.docusafe.parametrization.aegon.overtime;

/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public interface OvertimeCn
{
    public static final String DOCKIND_NAME = "overtime_app";
    public static final String USER_CN = "DS_USER";
    public static final String OVERTIME_DATE_CN = "OVERTIME_DATE";
    public static final String TIME_FROM_CN = "TIME_FROM";
    public static final String TIME_TO_CN = "TIME_TO";
    public static final String DESCRIPTION_CN = "DESCRIPTION";   
    public static final String TIME_COUNT_CN = "TIME_COUNT";
    public static final String STATUS_FIELD_CN = "STATUS";
    public static final String FLAG_CN = "FLAG";
    public static final String FINAL_ACCEPTANCE_FIELD_CN = "FINAL_ACCEPTANCE";
    public static final int OVERTIME_MAX_DATE = -5;
    
    public static final String FOLDER_MAIN_OVERTIME = "Nadgodziny";
    public static final String FOLDER_OVERTIME_APP = "Zlecenia nadgodzin";
    public static final String FOLDER_OVERTIME_RECEIVE = "Wnioski o odbi�r nadgodzin";
    
    public static final String AUTHOR_CN = "AUTHOR";
    public static final String SZEF_DZIALU = "szef_dzialu";
    public static final String PRACOWNIK = "pracownik";

    public static final Integer STATUS_ZAREJESTROWANY = 5;
    public static final Integer STATUS_REALIZOWANY = 10;
    public static final Integer STATUS_ODRZUCONY = 20;
    public static final Integer STATUS_ZAAKCEPTOWANY = 30;
    public static final Integer STATUS_ANULOWANY = 40;
    
        
}
