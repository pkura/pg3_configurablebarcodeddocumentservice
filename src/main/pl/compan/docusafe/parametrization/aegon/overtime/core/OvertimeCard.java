package pl.compan.docusafe.parametrization.aegon.overtime.core;

import java.io.Serializable;
import java.lang.Boolean;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Obiekt odwzorowujacy karte nadgodzin
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
@Entity
@Table(name="DS_OVERTIME_CARD")
public class OvertimeCard implements Serializable
{
    private static final Logger log = LoggerFactory.getLogger(OvertimeCard.class.getPackage().getName());
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="ID",nullable=false)
    private Long id;
    
    @Temporal(TemporalType.DATE)
    @Column(name="OVERTIME_DATE", nullable= false)
    private Date overtimeDate;
    
    @Column(name="TIME_FROM", nullable= false)
    private String timeFrom;
    @Column(name="TIME_TO", nullable= false)
    private String timeTo;
    @Column(name="TIME_COUNT", nullable= false)
    private String timeCount;
    
    @Column(name="EMPLOYEE_ID", insertable=false, updatable=false)
    private Long employeeId;
    
    @Column(name="DESCRIPTION", nullable = true)
    private String description;
    @Column(name="OVERTIME_RECEIVE", nullable = true)
    private Boolean overtimeReceive;
    
    @Column(name="DOCUMENT_ID")
    private Long documentId;
    
    
    public static OvertimeCard find(Long empId, Long documentId) throws EdmException
    {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(OvertimeCard.class);
            
            if(documentId != null)
                criteria.add(org.hibernate.criterion.Expression.eq("documentId", documentId));
            else
                return null;
            
            if(empId != null)
                criteria.add(org.hibernate.criterion.Expression.eq("employeeId", empId));

            List<OvertimeCard> otList = criteria.list();

            if (otList.size() > 0) {
                return (OvertimeCard)otList.get(0);
            }
            return null;

        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        } 
    }
    
    public static OvertimeCard findByDocumentId(Long id) throws EdmException
    {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(OvertimeCard.class);
            criteria.add(org.hibernate.criterion.Expression.eq("documentId", id));

            List<OvertimeCard> otList = criteria.list();

            if (otList.size() > 0) {
                return (OvertimeCard)otList.get(0);
            }
            return null;

        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        } 
    }
    
    public static List<OvertimeCard> findByEmployeeId(Long emplId, int year) throws EdmException
    {
        try {
            Criteria criteria = DSApi.context().session().createCriteria(OvertimeCard.class);
            criteria.add(Restrictions.eq("employeeId", emplId));
            criteria.add(Restrictions.sqlRestriction("datename(YEAR, OVERTIME_DATE) = "+year));
            criteria.addOrder(Order.asc("overtimeDate"));
            List<OvertimeCard> otList = criteria.list();

            if (otList.size() > 0) {
                return otList;
            }
            
            return null;

        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        } 
        
    }
    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public Long getEmployeeId()
    {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId)
    {
        this.employeeId = employeeId;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Date getOvertimeDate()
    {
        return overtimeDate;
    }

    public void setOvertimeDate(Date overtimeDate)
    {
        this.overtimeDate = overtimeDate;
    }

    public Boolean getOvertimeReceive()
    {
        return overtimeReceive;
    }

    public void setOvertimeReceive(Boolean overtimeReceive)
    {
        this.overtimeReceive = overtimeReceive;
    }

    public String getTimeCount()
    {
        return timeCount;
    }
    
    public String getTimeCountRead()
    {
        return timeCount.substring(0,5);
    }

    public void setTimeCount(String timeCount)
    {
        this.timeCount = timeCount;
    }

    public String getTimeFrom()
    {
        return timeFrom;
    }
    
    public String getTimeFromRead()
    {
        return timeFrom.substring(0,5);
    }
    
    public void setTimeFrom(String timeFrom)
    {
        this.timeFrom = timeFrom;
    }

    public String getTimeTo()
    {
        return timeTo;
    }
    
    public String getTimeToRead()
    {
        return timeTo.substring(0, 5);
    }

    public void setTimeTo(String timeTo)
    {
        this.timeTo = timeTo;
    }
    
    public boolean save() throws EdmException {

        try {
            DSApi.context().session().saveOrUpdate(this);
          return true;
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }
    
    public void delete() throws EdmException
    {
        try
        {
            DSApi.context().session().delete(this);
        }
        catch ( HibernateException ex )
        {
            log.error("blad", ex);
            throw new EdmHibernateException(ex);
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if( obj == null )
            return false;
        if( getClass() != obj.getClass() )
            return false;
        final OvertimeCard other = (OvertimeCard) obj;
        if( this.overtimeDate != other.overtimeDate && (this.overtimeDate == null || !this.overtimeDate.equals(other.overtimeDate)) )
            return false;
        if( (this.timeFrom == null) ? (other.timeFrom != null) : !this.timeFrom.equals(other.timeFrom) )
            return false;
        if( (this.timeTo == null) ? (other.timeTo != null) : !this.timeTo.equals(other.timeTo) )
            return false;
        if( (this.timeCount == null) ? (other.timeCount != null) : !this.timeCount.equals(other.timeCount) )
            return false;
        if( this.employeeId != other.employeeId && (this.employeeId == null || !this.employeeId.equals(other.employeeId)) )
            return false;
        if( (this.description == null) ? (other.description != null) : !this.description.equals(other.description) )
            return false;
        if( this.overtimeReceive != other.overtimeReceive && (this.overtimeReceive == null || !this.overtimeReceive.equals(other.overtimeReceive)) )
            return false;
        if( this.documentId != other.documentId && (this.documentId == null || !this.documentId.equals(other.documentId)) )
            return false;
        return true;
    }

    @Override
    public int hashCode()
    {
        int hash = 5;
        hash = 89 * hash + (this.overtimeDate != null ? this.overtimeDate.hashCode() : 0);
        hash = 89 * hash + (this.timeFrom != null ? this.timeFrom.hashCode() : 0);
        hash = 89 * hash + (this.timeTo != null ? this.timeTo.hashCode() : 0);
        hash = 89 * hash + (this.timeCount != null ? this.timeCount.hashCode() : 0);
      //  hash = 89 * hash + (this.employeeId != null ? this.employeeId.hashCode() : 0);
        hash = 89 * hash + (this.description != null ? this.description.hashCode() : 0);
        hash = 89 * hash + (this.overtimeReceive != null ? this.overtimeReceive.hashCode() : 0);
        hash = 89 * hash + (this.documentId != null ? this.documentId.hashCode() : 0);
        return hash;
    }
    
    
    public String getOvertimeReceiveRead()
    {
        try{
            if(overtimeReceive.equals(true))
                return "Odbi�r nadgodzin";
        } catch (NullPointerException ex)
        {
        }
        
        return "Zlecenie nadgodzin";
    }
    
    public void saveWithPS() throws EdmException
    {
        try{
            PreparedStatement ps = DSApi.context().prepareStatement("INSERT INTO [DS_OVERTIME_CARD]"
                    + "([OVERTIME_DATE] ,[TIME_FROM],[TIME_TO],[TIME_COUNT],[EMPLOYEE_ID],[DESCRIPTION],[OVERTIME_RECEIVE],[DOCUMENT_ID])"+
                "VALUES(?,?,?,?,?,?,?,?)");
            
            Calendar cal = DateUtils._GREGORIAN_CALENDAR;
            cal.setTime(this.overtimeDate);
            ps.setDate(1, new java.sql.Date(cal.getTimeInMillis()));
            ps.setString(2, timeFrom);
            ps.setString(3, timeTo);
            ps.setString(4, timeCount);
            ps.setLong(5, employeeId);
            ps.setString(6, description);
            try{
                ps.setBoolean(7, overtimeReceive);
            } catch (NullPointerException ex)
            {
                ps.setNull(7, java.sql.Types.BOOLEAN);
                log.info(" ",ex);
            }
            
            if(documentId == null)
                ps.setNull(8, java.sql.Types.NUMERIC);
            else
                ps.setLong(8, documentId);
            
            ps.executeUpdate();
            
        }catch (SQLException ex) {
            log.error("",ex);
        }  
    }
    
    public void updateWithPS() throws EdmException
    {
        try
        {
            PreparedStatement ps = DSApi.context().prepareStatement("UPDATE [DS_OVERTIME_CARD]"
                           +"SET [OVERTIME_DATE] = ?"
                            +" ,[TIME_FROM] = ?"
                             +",[TIME_TO] = ?"
                              +",[TIME_COUNT] = ?"
                              +",[EMPLOYEE_ID] = ?"
                              +",[DESCRIPTION] = ?"
                              +",[OVERTIME_RECEIVE] = ?"
                              +",[DOCUMENT_ID] =?"
                         +" WHERE [ID] = ?");
             Calendar cal = DateUtils._GREGORIAN_CALENDAR;
            cal.setTime(this.overtimeDate);
            ps.setDate(1, new java.sql.Date(cal.getTimeInMillis()));
            ps.setString(2, timeFrom);
            ps.setString(3, timeTo);
            ps.setString(4, timeCount);
            ps.setLong(5, employeeId);
            ps.setString(6, description);
            try{
                ps.setInt(7, overtimeReceive == true ? 1 : 0);
            } catch (NullPointerException ex)
            {
                ps.setNull(7, java.sql.Types.BOOLEAN);
                log.error("update",ex);
            }
            
            try
            {
                ps.setLong(8, documentId);
            } catch (NullPointerException ex)
            {
                ps.setNull(8, java.sql.Types.NUMERIC);
                log.error("update",ex);
            }
            ps.setLong(9, id);
            ps.executeUpdate();
        }
        catch ( SQLException ex )
        {
            log.error("", ex);
        }
    }
}
