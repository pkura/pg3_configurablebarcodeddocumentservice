package pl.compan.docusafe.parametrization.aegon.overtime.core;

import com.opensymphony.webwork.ServletActionContext;
import java.io.File;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.util.DateUtils;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;
/**
 *
 * @author �ukasz Wo�niak <l.g.wozniak@gmail.com>
 */
public class PdfOvertimeCardAction extends EventActionSupport
{
    /**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(PdfOvertimeCardAction.class);
	
	private String empCardName;
	private Integer currentYear;
        private int month;
        private AbstractOvertimeLogic instance;
	
	@Override
        
	protected void setup() 
	{
            instance = AbstractOvertimeLogic.getInstance();
            
            registerListener("doPdf")
                    .append(OpenHibernateSession.INSTANCE)
                    .append(new PdfReport())
                    .appendFinally(CloseHibernateSession.INSTANCE);

            registerListener("doXls")
                    .append(OpenHibernateSession.INSTANCE)
                    .append(new XlsReport())
                    .appendFinally(CloseHibernateSession.INSTANCE);

            registerListener("doPdfMonthly")
                    .append(OpenHibernateSession.INSTANCE)
                    .append(new PdfMonthlyReport())
                    .appendFinally(CloseHibernateSession.INSTANCE);

            registerListener("doXlsMonthly")
                    .append(OpenHibernateSession.INSTANCE)
                    .append(new XlsMonthlyReport())
                    .appendFinally(CloseHibernateSession.INSTANCE);
                
                
	}
	
	private class PdfReport implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			try
			{
                            File file = instance.pdfCard(getEmpCardName(), currentYear); 

                            ServletUtils.streamFile(
                                    ServletActionContext.getResponse(), 
                                    file,
                                    "application/pdf", 
                                    "Content-Disposition: attachment; filename=\"Karta Roczna Nadgodzin" + currentYear +".pdf\"");
			}
			catch (Exception e) 
			{
				addActionError(e.getMessage());
                                LOG.error(e.getMessage(), e);
			}
		}
	}

        public class XlsReport implements ActionListener
        {
            public void actionPerformed(ActionEvent event)
            {
                try{
                     File file = instance.xlsCard(getEmpCardName(), currentYear); 

                           ServletUtils.streamFile(
                                   ServletActionContext.getResponse(),
                                   file,
                                   "application/vnd.ms-excel", 
                                   "Accept-Charset: iso-8859-2",
                                   "Content-Disposition: attachment; filename=\"Karta Roczna Nadgodzin-" + currentYear +".xls\"");

                } catch (Exception e)
                {
                  addActionError(e.getMessage());
                  LOG.error(e.getMessage(), e);
                }
            }
        }
        
        public class PdfMonthlyReport implements ActionListener
        {
            public void actionPerformed(ActionEvent event)
            {
                 try{
                     File file = instance.pdfMonthlyCard(getEmpCardName(), 
                                        DateUtils.getCurrentYear() , month); 

                   ServletUtils.streamFile(
                            ServletActionContext.getResponse(), 
                            file,
                            "application/pdf", 
                            "Content-Disposition: attachment; filename=\"Karta Miesieczna Nadgodzin-" + DateUtils.getMonthName(month-1) +".pdf\"");
                } catch (Exception e)
                {
                  addActionError(e.getMessage());
                  LOG.error(e.getMessage(), e);
                }
            }
        }
        
        public class XlsMonthlyReport implements ActionListener
    {

        public void actionPerformed(ActionEvent event)
        {
            try{
                   File file = instance.xlsMonthlyCard(getEmpCardName(), 
                                            DateUtils.getCurrentYear(),
                                            month); 

                   ServletUtils.streamFile(
                           ServletActionContext.getResponse(),
                           file,
                           "application/vnd.ms-excel", 
                           "Accept-Charset: iso-8859-2",
                           "Content-Disposition: attachment; filename=\"Karta Miesieczna Nadgodzin-" + DateUtils.getMonthName(month-1) +".xls\"");

                } catch (Exception e)
                {
                  addActionError(e.getMessage());
                  LOG.error(e.getMessage(), e);
                }
        }
    }
        
        
	public String getEmpCardName() 
	{
            if(empCardName == null)
                return DSApi.context().getPrincipalName();
            else
		return empCardName;
	}
	
	public void setEmpCardName(String empCardName) 
	{
		this.empCardName = empCardName;
	}

	public Integer getCurrentYear() 
	{
		return currentYear;
	}

	public void setCurrentYear(Integer currentYear) 
	{
		this.currentYear = currentYear;
	}
        
        public int getMonth()
        {
            return this.month;
        }
        
        public void setMonth(int month)
        {
            this.month = month;
        }
}
