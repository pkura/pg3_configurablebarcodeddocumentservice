package pl.compan.docusafe.parametrization.aegon.overtime.core;

/**
 * Klasa zawierajaca date i godziny do odebrania
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class EmployeeOvertimeReceiveMonthlyEntity
{
    private int month;
    private String date;
    private String overtimesReceive;
    public int hours;
    public int minutes;

    public EmployeeOvertimeReceiveMonthlyEntity(String date, String overtimesReceive, int month)
    {
        this.date = date;
        this.overtimesReceive = overtimesReceive;
        this.month = month;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public String getOvertimesReceive()
    {
        return overtimesReceive;
    }

    public void setOvertimesReceive(String overtimesReceive)
    {
        this.overtimesReceive = overtimesReceive;
    }

    public int getMonth()
    {
        return month;
    }

    public void setMonth(int month)
    {
        this.month = month;
    }
}
