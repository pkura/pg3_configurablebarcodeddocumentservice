package pl.compan.docusafe.parametrization.aegon.overtime.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import pl.compan.docusafe.parametrization.aegon.overtime.core.EmployeeOvertimeReceiveMonthlyEntity;

/**
 * Klasa przechowuje dane dotycz�ce dnia miesiaca nadgodzin
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class EmployeeOvertimeMonthlyEntity
{
    private int month;
    private Date date;
    /** Wypracowane nadgodziny*/
    private String overtimes;
    private String description;
    private Long documentId;
    
    private String firstDateReceive;
    private String firstOvertimesReceive;
    public int hours;
    public int minutes;
    
    private List<EmployeeOvertimeReceiveMonthlyEntity> empOvertimeReceive = new ArrayList<EmployeeOvertimeReceiveMonthlyEntity>();

    public EmployeeOvertimeMonthlyEntity()
    {
    }
    
    public EmployeeOvertimeMonthlyEntity(Date date, String overtimes, String description, Long documentId, int month)
    {
        this.date = date;
        this.overtimes = overtimes;
        this.description = description;
        this.documentId = documentId;
        this.month = month;
        
    }
    
    public void setFirstElements()
    {
        if(this.empOvertimeReceive.isEmpty())
            return;
        EmployeeOvertimeReceiveMonthlyEntity emA = this.empOvertimeReceive.get(0);
        if(emA != null)
        {
            this.firstDateReceive = emA.getDate();
            this.firstOvertimesReceive = emA.getOvertimesReceive();
            this.empOvertimeReceive.remove(0);
        }
    }
    
    public Date getDate()
    {
        return date;
    }

    public void setDate( Date date)
    {
        this.date = date;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getOvertimes()
    {
        return overtimes;
    }

    public void setOvertimes(String overtimes)
    {
        this.overtimes = overtimes;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public List<EmployeeOvertimeReceiveMonthlyEntity> getEmpOvertimeReceive()
    {
        return empOvertimeReceive;
    }

    public void setEmpOvertimeReceive(List<EmployeeOvertimeReceiveMonthlyEntity> empOvertimeReceive)
    {
        this.empOvertimeReceive = empOvertimeReceive;
    }
    
    /**
     * Dodaje element do tablicy odebranych nadgodzin
     */
    public void addEmpOvertimeReceiveEntity(EmployeeOvertimeReceiveMonthlyEntity empReceive)
    {
        this.empOvertimeReceive.add(empReceive);
    }
    
    public int getMonth()
    {
        return month;
    }

    public void setMonth(int month)
    {
        this.month = month;
    }

    public String getFirstDateReceive()
    {
        return firstDateReceive;
    }

    public void setFirstDateReceive(String firstDateReceive)
    {
        this.firstDateReceive = firstDateReceive;
    }

    public String getFirstOvertimesReceive()
    {
        return firstOvertimesReceive;
    }

    public void setFirstOvertimesReceive(String firstOvertimesReceive)
    {
        this.firstOvertimesReceive = firstOvertimesReceive;
    }
    
    
}
