package pl.compan.docusafe.parametrization.aegon.overtime.core;

import java.io.File;
import java.util.Date;
import java.util.List;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.ws.client.overtimes.OvertimeServiceStub;
import org.apache.axis2.addressing.EndpointReference;


/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public abstract class AbstractOvertimeLogic
{
    public static AbstractOvertimeLogic getInstance()
    {
        if (AvailabilityManager.isAvailable("aegon.docusafe.remote"))
        {
            return new RemoteOvertimeLogic();
        }

        return new LocalOvertimeLogic();
    }
    
    public static DSUser getRemoteUserInfo(String username) throws EdmException
    {
        UserImpl user = new UserImpl();
        try
        {
                OvertimeServiceStub overtimeServiceStub = new OvertimeServiceStub();
                overtimeServiceStub._getServiceClient().getOptions().setTo(
                        new EndpointReference(Docusafe.getAdditionProperty("overtimeService.endPointReference")));

                OvertimeServiceStub.UserRequest request = new OvertimeServiceStub.UserRequest();
                request.setLogin(username);

                OvertimeServiceStub.GetUserInfo info = new OvertimeServiceStub.GetUserInfo();
                info.setRequest(request);

                OvertimeServiceStub.GetUserInfoResponse response = overtimeServiceStub.getUserInfo(info);
                OvertimeServiceStub.UserResponse loginResponse = response.get_return();
                
                user.setFirstname(loginResponse.getFirstName());
                user.setLastname(loginResponse.getLastName());
                user.setName(loginResponse.getLogin());
                user.setId(loginResponse.getId());
        }
        catch (Exception e1) 
        {
                LoggerFactory.getLogger("kamilj").debug(e1.getMessage(),e1);
        }
        
        return user;
    }
     
    public abstract EmployeeCard getActiveEmployeeCardByUserName(String userName) throws EdmException;
    
    /** sprawdza czy data jest w kalendarzu dni wolnych */
    public abstract boolean isFreeDay(Date date) throws EdmException;
    
    /**
     * Pobiera list� nadgodzin pracownika z danego roku
     * @return List<EmployeeOvertimeEntity>
     */
    public abstract List<EmployeeOvertimeEntity> getEmployeeOvertimes(EmployeeCard empCard,int year) throws EdmException;
    
    /**
     * Pobiera liste nadgodzin w danym miesiacu z danego roku
     * @param empCard
     * @param year
     * @param month
     * @return
     * @throws EdmException 
     */
    public abstract List<EmployeeOvertimeMonthlyEntity> getMonthEmployeeOvertimes(EmployeeCard empCard, int year, int month) throws EdmException;
    /** Nalicza nadgodziny */
    public abstract void chargeOvertimes(EmployeeCard empCard, Long documentId, String description, Date date,String from, String to, String count, Boolean receive) throws EdmException;
    /** Usuwa wpis nadgodzin */
    public abstract void unchargeOvertimes(EmployeeCard empCard, Long documentId) throws EdmException;
    
    /** pdf z roczn� karta nadgodzin pracownika */
    public abstract File pdfCard(String username, int year) throws EdmException, Exception;

    /** xls z roczn� karta nadgodzi pracownika*/
    public abstract File xlsCard(String username, int year) throws EdmException, Exception;
    
    public abstract File pdfMonthlyCard(String username, int year, int month) throws EdmException, Exception;
    
    public abstract File xlsMonthlyCard(String username, int year, int month) throws EdmException, Exception;
    
    
}
