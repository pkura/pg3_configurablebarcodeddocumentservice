package pl.compan.docusafe.parametrization.aegon.overtime.core;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.EmployeeCardDao;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.web.admin.absences.AbstractXlsReport;
import pl.compan.docusafe.web.admin.overtimes.SuperiorDailyReport;

/**
 *
 * @author �ukasz Wo�niak <l.g.wozniak@gmail.com>
 */
public class ManagerOvertimeDailyReport extends Report
{
    private String name;
    private String year;
    private UniversalTableDumper dumper;

    public void initParametersAvailableValues() throws ReportException
    {
        super.initParametersAvailableValues();
        try
        {
            for( ReportParameter rp : params )
            {
                if( rp.getFieldCn().equals("year") )
                {
                    rp.setAvailableValues(getYears());
                }
                if( rp.getFieldCn().equals("user") )
                {
                    rp.setAvailableValues(getUsers());
                }
            }
        }
        catch ( Exception e )
        {
            throw new ReportException(e);
        }
    }

    @Override
    public void doReport() throws Exception
    {
        for( ReportParameter rp : params )
        {
            if( rp.getFieldCn().equals("year") )
            {
                year = rp.getValueAsString();
            }
            if( rp.getFieldCn().equals("user") )
            {
                name = rp.getValueAsString();
            }
        }
        
        dumper = new UniversalTableDumper();
        XlsPoiDumper poiDumper = new XlsPoiDumper();
        File xlsFile = new File(this.getDestination(), "raport.xls");
        poiDumper.openFile(xlsFile);
        FileOutputStream fis = new FileOutputStream(xlsFile);

        doReportForAll().getWorkbook().write(fis);
        
        fis.close();
        dumper.addDumper(poiDumper);
    }

    private AbstractXlsReport doReportForAll() throws Exception
    {
        List<EmployeeCard> overtimesToExcell = EmployeeCardDao.getSubordinatesCard(DSApi.context().getPrincipalName());
        SuperiorDailyReport xlsReport = new SuperiorDailyReport("Raport wymiar�w urlopowych", overtimesToExcell, Integer.parseInt(year));
        xlsReport.generate();
        return xlsReport;
    }
        
    private Map<String, String> getUsers()
    {
        Map<String, String> users = new LinkedHashMap<String, String>();
        users.put("all", "Wszyscy");
        String user = getUsername() != null ? getUsername() : DSApi.context().getPrincipalName();
        try
        {
            PreparedStatement ps = DSApi.context().prepareStatement("select fieldValue from ds_acceptance_condition where cn = 'szef_dzialu' and username = ?");
            ps.setString(1, user);
            ResultSet rs = ps.executeQuery();
            while( rs.next() )
            {
                user = rs.getString(1).replace("ext:", "");
                try
                {
                    users.put(user, DSUser.findByUsername(user).asLastnameFirstname());
                }
                catch ( Exception e )
                {
                    users.put(user, user);
                }
            }
            rs.close();
            ps.close();

        }
        catch ( Exception e )
        {
            log.debug(e.getMessage(), e);
        }

        return users;
    }

    private Map<String, String> getYears()
    {
        Map<String, String> years = new HashMap<String, String>();
        try
        {
            for( int i = 2010; i <= GregorianCalendar.getInstance().get(GregorianCalendar.YEAR); i++ )
                years.put(i + "", i + "");
        }
        catch ( Exception e )
        {
            log.debug(e.getMessage(), e);
        }
        return years;
    }
}
