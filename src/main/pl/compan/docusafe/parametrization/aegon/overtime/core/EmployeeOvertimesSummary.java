package pl.compan.docusafe.parametrization.aegon.overtime.core;

import java.util.Iterator;
import java.util.List;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TimeUtils;

/**
 * podlicza i przechowuje roczne sumy, uzywane do podsumowan na karcie
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class EmployeeOvertimesSummary
{
    private static final Logger log = LoggerFactory.getLogger(EmployeeOvertimesSummary.class);
    private EmployeeCard emplCard;
    private String allHour = "00:00";
    private String allHourReceive = "00:00";
    private String allHourLeft = "00:00";

    
    /**
     * tworzy wpis do podsumowania na podstawie zapytania  OvertimesFactory.getEmployeeOvertimes
     * @see EmployeeOvertimesSummary.summary(List<EmployeeOvertimeEntity> empOvertimes)
     * @param empOvertimes
     * @return 
     */
    public static EmployeeOvertimesSummary getInstance(List<EmployeeOvertimeEntity> empOvertimes)
    {
        EmployeeOvertimesSummary empOtSummary = new EmployeeOvertimesSummary();
        empOtSummary.summary(empOvertimes);
        return empOtSummary;
    }
    
    /**
     * sumuje wszystkie godziny
     * @param empOvertimes wynik zapytania z OvertimesFactory.getEmployeeOvertimes
     */
    public void summary(List<EmployeeOvertimeEntity> empOvertimes)
    {
        if (empOvertimes.size() > 0)
        {
            int hour = 0;
            int minute = 0;
            int hourReceive = 0;
            int minuteReceive = 0;
            int hourLeft = 0;
            int minuteLeft = 0;

            Iterator it = empOvertimes.iterator();

            while (it.hasNext())
            {
                EmployeeOvertimeEntity empOvertime = (EmployeeOvertimeEntity) it.next();
                int tmpHour = 0;
                int tmpMinute = 0;

                tmpHour = Integer.parseInt(empOvertime.getSumHour());
                hour += tmpHour;
                tmpMinute = Integer.parseInt(empOvertime.getSumMinute());
                minute += tmpMinute;


                tmpHour = Integer.parseInt(empOvertime.getSumHourReceive());
                hourReceive += tmpHour;
                tmpMinute = Integer.parseInt(empOvertime.getSumMinuteReceive());
                minuteReceive += tmpMinute;


                tmpHour = Integer.parseInt(empOvertime.getLeftHour());
                hourLeft += tmpHour;

                tmpMinute = Integer.parseInt(empOvertime.getLeftMinute());
                minuteLeft += tmpMinute;
            }

            this.allHour = TimeUtils.parseMinuteToHours(hour, minute);
            this.allHourReceive = TimeUtils.parseMinuteToHours(hourReceive, minuteReceive);
            this.allHourLeft = TimeUtils.substractTimes(allHour, allHourReceive);
        }
    }

    public String getAllHour()
    {
        return allHour;
    }

    public String getAllHourLeft()
    {
        return allHourLeft;
    }

    public String getAllHourReceive()
    {
        return allHourReceive;
    }

    public EmployeeCard getEmplCard()
    {
        return emplCard;
    }

    public void setEmplCard(EmployeeCard emplCard)
    {
        this.emplCard = emplCard;
    }
    
    
}
