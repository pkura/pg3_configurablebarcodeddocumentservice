package pl.compan.docusafe.parametrization.aegon.wf;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class WfProcedureReport extends AbstractWfReport implements WfReport
{
   private static final Logger log = LoggerFactory.getLogger(WfProjectReport.class);
    
    private void initValues() throws Exception
    {
        for (ReportParameter param : getParams())
        {
            if (param.getType().equals("break-line"))
            {
                continue;
            }
        }
    }

    @Override
    public void doReport() throws Exception
    {
        LoggerFactory.getLogger("lukaszl").debug("WfProjectReport");

        try
        {
            this.initValues();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        initializeDumpers();
        //Naglowek
        setHeaders();
        
        PreparedStatement ps = null;
        
        try {
            ps = DSApi.context().prepareStatement(getSQL());
            ps.setLong(1, new Long(PROCEDURE_CLASS_ID));
            ResultSet rs = ps.executeQuery();
            
            ps = DSApi.context().prepareStatement(getAcceptedUsers());
            
            int counter = 0;
            while (rs.next()) {
                Long documentId = rs.getLong("id");
                ps.setLong(1, documentId);
                ResultSet rsAccepted = ps.executeQuery();
                
                while(rsAccepted.next())
                {
                    addRecordLine(rs, ++counter, rsAccepted.getString(1), rsAccepted.getString(2));
                }
            }

            rs.close();

        } catch (Exception e) {
            throw new EdmException(e);
            //e.printStackTrace();
        } finally {
            DSApi.context().closeStatement(ps);
        }

        dumper.closeFileQuietly();
    } 
    
    @Override
    public void setHeaders() throws IOException, EdmException 
    {
        this.addTitle();
        this.addCreatedBy();
        this.addCreatedDate();
        
        this.dumper.newLine();
        this.dumper.addText("Lp.");
        this.dumper.addText(sm.getString("DokumentId"));
        this.dumper.addText(sm.getString("Autor"));
        this.dumper.addText(sm.getString("Nazwa"));
        this.dumper.addText(sm.getString("NumerProceduryRegulaminu"));
        this.dumper.addText(sm.getString("Deadline"));
        this.dumper.addText(sm.getString("WykonaneAkceptacje"));
        this.dumper.addText(sm.getString("AkceptacjeDoWykonania"));
        this.dumper.dumpLine();
    }
    /**
     * Dodaje rekord do pliku
     */
    @Override
     public void addRecordLine(ResultSet rs, int counter, String acceptedUsers, String acceptedToDo) throws IOException, SQLException {
        
        dumper.newLine();
        dumper.addInteger(counter);
        dumper.addLong(rs.getLong("id"));
        dumper.addText(rs.getString("autor"));
        dumper.addText(rs.getString("nazwa"));
        dumper.addText(rs.getString("numerprocedury"));
        dumper.addDate(rs.getDate("deadline"), "dd-MM-yyyy");
        
        try{
            dumper.addText(acceptedUsers.trim().substring(1));
        } catch (Exception ex) {
             log.info("Blad przy dokumencie acc: "+ rs.getLong("id") + "dla " + acceptedUsers, ex);
             dumper.addEmptyText();
         }
        
        try{
            dumper.addText(acceptedToDo.trim().substring(1));
        } catch (Exception ex){
             log.info("Blad przy dokumencie accTodo: "+ rs.getLong("id")+ "dla " + acceptedToDo, ex);
             dumper.addEmptyText();
         }
        dumper.dumpLine();
    }
    
}
