package pl.compan.docusafe.parametrization.aegon.wf;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import pl.compan.docusafe.core.dockinds.logic.RelatedDocumentBean;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.office.OfficeDocument;
import java.sql.SQLException;
import pl.compan.docusafe.core.EdmException;
import java.util.*;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.webwork.event.ActionEvent;
import static pl.compan.docusafe.parametrization.aegon.wf.WorkflowCn.*;

/**
 * klasa pomocnicza dla dokumnetu WorkflowLogic
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class DocWorkflowFactory
{
    private static final Logger log = LoggerFactory.getLogger(DocWorkflowFactory.class);
    private static final StringManager sm = StringManager.getManager(DocWorkflowFactory.class.getPackage().getName());
    
    /**
     * Funkcja dekretuje dokument w zale�no�ci od trybu przekazanego w parametrze
     * @param eventActionSupport
     * @param event
     * @param document
     * @param activity
     * @param values 
     */
    public static void goToDocument(Integer approveType, OfficeDocument document) throws EdmException, SQLException
    {
        FieldsManager fm = document.getFieldsManager();

        if(!BaseWorkflowLogic.isZAREJESTROWANY((Integer)fm.getKey(STATUS_FIELD_CN)))
            throw new EdmException("Dokument jest ju� w realizacji!");

        List<Integer> usersId = (List<Integer>)fm.getKey(DS_USER_APPROVER_CN); 
        DSUser user = UserImpl.find(usersId.get(0).longValue());

        if(BaseWorkflowLogic.isChainAcceptance(approveType))// �a�cuchowa
        {
            updateApproveType(document.getId(), WF_CHAIN_VALUE, STATUS_REALIZOWANY);
            document.addAssignmentHistoryEntry(getAsgnHistory("Rozpoczyna Dekretacja �a�cuchowa", DSApi.context().getPrincipalName(), WF_ACCEPTANCE_GROUP,WF_ACCEPTANCE_GROUP, true));
        }

        if(BaseWorkflowLogic.isParallelAcceptance(approveType)) //rownolegla
        {
            updateApproveType(document.getId(), WF_PARALLEL_VALUE, STATUS_REALIZOWANY);
            document.addAssignmentHistoryEntry(getAsgnHistory("Rozpoczyna Dekretacja R�wnoleg�a", DSApi.context().getPrincipalName(), WF_ACCEPTANCE_GROUP, WF_ACCEPTANCE_GROUP, true));
        }

        DSApi.context().session().saveOrUpdate(document);
    }
    
    /**
     * Funkcja wywolywana w momencie gdy u�ytkownik akceptuje lub odrzuca dokument
     * @param actionName
     * @param event
     * @param document
     * @param activity
     * @param values
     * @throws EdmException 
     */
    public static void decreateDocument(String actionName, ActionEvent event, OfficeDocument document, String activity,Map<String, Object> values) throws EdmException
    {
            FieldsManager fm = document.getFieldsManager();
                        
            if(!BaseWorkflowLogic.isREALIZOWANY((Integer)fm.getKey(STATUS_FIELD_CN)))
                throw new EdmException("Dokument musi by� w realizacji!");

            List<Integer> usersId = (List<Integer>)fm.getKey(DS_USER_APPROVER_CN); 
            DSUser author = DSUser.findByUsername(document.getAuthor());
            
            //powod odrzucenia
            String reason = (String)values.get(WF_REJECT_REASON_CN);
            //zwraca nastepnego uzytkownika
            Integer nextUser = getNextUser(usersId, new Integer(author.getId().intValue()));
            
            log.error("ddasda " + nextUser);
            if(actionName.equals(WF_DECRET_AUTHOR_CN))// dekretacja do autora
            {
                
            }
            
            if(actionName.equals(WF_DECRET_FURTHER_CN)) //dekretacja dalej
            {
            }
    }
    
    /**
     * Tworzy wpis w historii dekretacji dokumentu
     * @param approveType
     * @param targetUser
     * @param sourceGuid
     * @param targetGuid
     * @param accepted
     * @return 
     */
    public static AssignmentHistoryEntry getAsgnHistory(String approveType, String targetUser, String sourceGuid, String targetGuid, boolean accepted)
    {
        AssignmentHistoryEntry asgnHistory = new AssignmentHistoryEntry();
        
        asgnHistory.setCtimeAndCdate(new Date());
        asgnHistory.setSourceUser(DSApi.context().getPrincipalName());
        asgnHistory.setSourceGuid(sourceGuid);
        asgnHistory.setTargetUser(targetUser);
        asgnHistory.setTargetGuid(targetGuid);
        asgnHistory.setObjective(approveType);
        asgnHistory.setAccepted(accepted);
        asgnHistory.setFinished(false);
        asgnHistory.setProcessName("do realizacji");
        asgnHistory.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
        
        return asgnHistory;
    }
    
    /**
     * Wysyla u�ytkownikowi dokument, uzywane przy lancuchowej
     */
    public static void saveManualAssignment(String activity, String guid, String userName)  throws EdmException
    {
         WorkflowFactory.simpleAssignment(activity, guid, userName, null, WorkflowFactory.wfManualName(), 
                    WorkflowFactory.SCOPE_ONEUSER, null, null);
    }
    
    /**
     * Zwraca nastepnego u�ytkownika do dekretacji, jesli nie ma nastepnego zwraca autora
     * Jesli uzytkownika zalogowanego nie ma na liscie dekretujacych zwraca blad
     * @param usersId
     * @param authorId
     * @return
     * @throws EdmException 
     */
    public static Integer getNextUser(List<Integer> usersId, Integer authorId) throws EdmException
    {
        Iterator it = usersId.iterator();
        Integer nextUser = new Integer(authorId);
        boolean ok = true;
        
        while(it.hasNext())
        {
            Integer uId = (Integer) it.next();
            if(DSApi.context().getDSUser().getId().equals(uId.longValue()))
            {
                if(it.hasNext())
                {
                    nextUser = (Integer) it.next();
                    ok = false;
                }
            }
            
        }
        
        if (ok)
        {
            throw new EdmException("Nie nale�ysz do listy akceptujacych");
        }
        return nextUser;
    }
    
        /**
     * Zwraca powi�zane dokumenty nationwide z dokumentem o okre�lonym id.
     * 
     * @param documentId
     * @return
     */
    public static List<RelatedDocumentBean> getReleatedDocuments(Long wfNazwaId) throws EdmException
    {
        List<Object[]> res = DSApi.context().session().createSQLQuery(
                "SELECT d.ID, d.TITLE FROM DS_DOCUMENT d JOIN DSG_WF as d1 ON d.ID = d1.DOCUMENT_ID WHERE d1.WF_NAZWA = ?").setLong(0, wfNazwaId).list();

        List<RelatedDocumentBean> beans = new ArrayList<RelatedDocumentBean>();
        for (Object[] row : res)
        {
            beans.add(createReleatedDocumentBean(row));
        }

        return beans;
    }

    /**
     * Tworzy bean powi�zanego dokumentu
     * 
     * @param row
     * @return
     * @throws EdmException
     */
    private static RelatedDocumentBean createReleatedDocumentBean(Object[] row) throws EdmException
    {
        Long docId = ((BigDecimal) row[0]).longValue();
        String title = (String) row[1];
        String activityKey = null;
        String docType = null;
        Long attachmentId = null;
        String attachmentMime = null;

        // pobranie ID za��cznika
        Object[] res = (Object[]) DSApi.context().session().createSQLQuery("SELECT TOP 1 ar.ID, ar.MIME FROM DS_ATTACHMENT a join DS_ATTACHMENT_REVISION ar on a.ID = ar.ATTACHMENT_ID "
                + "WHERE a.DOCUMENT_ID = :docId AND a.DELETED = 0 AND (a.cn IS NULL OR a.cn <> 'nw_signature') ORDER BY ar.REVISION DESC").setParameter("docId", docId).uniqueResult();

        if (res != null)
        {
            attachmentId = ((BigDecimal) res[0]).longValue();
            attachmentMime = (String) res[1];
        }

        RelatedDocumentBean rdb = new RelatedDocumentBean(docId, title, activityKey, docType, attachmentId, attachmentMime);
        return rdb;
    }
    
    /**
     * updatuje status i typ akceptacji 
     * @param documentId
     * @param value
     * @param status
     * @throws SQLException
     * @throws EdmException 
     */
    public static void updateApproveType(Long documentId, Integer value, Integer status) throws SQLException, EdmException
    {
        PreparedStatement ps = DSApi.context().prepareStatement("UPDATE DSG_WF SET APPROVE_TYPE = ? ,STATUS = ? WHERE DOCUMENT_ID = ?");
        ps.setInt(1, value);
        ps.setInt(2, status);
        ps.setLong(3, documentId);

        ps.executeUpdate();
    }
}
