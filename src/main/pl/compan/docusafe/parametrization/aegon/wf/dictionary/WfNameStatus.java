package pl.compan.docusafe.parametrization.aegon.wf.dictionary;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

/**
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class WfNameStatus
{

    private Long id;
    private String cn;
    private String title;
    private Long centrum;
    private String refValue;
    private Long available;

    /**
     * @return java.util.List
     * @throws EdmException 
     */
    public static List list() throws EdmException
    {
        try
        {
            Criteria criteria = DSApi.context().session().createCriteria(WfNameStatus.class);
            criteria.add(Expression.eq("available",new Long(1)));
            return criteria.list();

        } catch (HibernateException e)
        {
            throw new EdmHibernateException(e);
        }
    }

    public Long getAvailable()
    {
        return available;
    }

    public void setAvailable(Long available)
    {
        this.available = available;
    }

    public Long getCentrum()
    {
        return centrum;
    }

    public void setCentrum(Long centrum)
    {
        this.centrum = centrum;
    }

    public String getCn()
    {
        return cn;
    }

    public void setCn(String cn)
    {
        this.cn = cn;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getRefValue()
    {
        return refValue;
    }

    public void setRefValue(String refValue)
    {
        this.refValue = refValue;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }
}
