package pl.compan.docusafe.parametrization.aegon.wf;

import java.io.IOException;

/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public interface WfReport
{
       static final String PROJECT_CLASS_ID = "1";  
       static final String PRODUCT_CLASS_ID = "2";  
       static final String PROCEDURE_CLASS_ID = "3";  
}
