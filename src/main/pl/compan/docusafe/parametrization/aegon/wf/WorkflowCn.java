package pl.compan.docusafe.parametrization.aegon.wf;

public interface WorkflowCn {
	public static final String KLASA_FIELD_CN = "KLASA";
	public static final String TYP_FIELD_CN = "TYP";
	public static final String DATA_PISMA_FIELD_CN = "DATA_PISMA";
	public static final String DATA_DEADLINE_FIELD_CN = "DATA_DEADLINE";
	public static final String PROCEDURE_NUMBER_FIELD_CN = "PROCEDURE_NUMBER";
        public static final String WF_NAZWA_CN = "WF_NAZWA";
	/*** KLASY */
	public static final String PROJEKTY_CLASS_CN = "Projects";
	public static final String PRODUKTY_CLASS_CN = "Products";
	public static final String PROCEDURY_CLASS_CN = "Procedures";

	/*** TYP */
	public static final String ZAPOTRZEBOWANIE_NA_PROJKETY_TYP_CN = "BC";
	public static final String KARTA_OTWARCIA_PROJKETU_TYP_CN = "KOP";
	public static final String ZALACZNIK_KARTY_OTWARCIA_PROJKETU_TYP_CN = "ZKOP";
	public static final String DOKUMENT_ZAKONCZENIA_TESTU_TYP_CN = "UAT";
	public static final String SCENARIUSZ_TESTOWY_TYP_CN = "ST";
	public static final String SPECYFIKACJA_FUNKCJONALA_TYP_CN = "SF";
	public static final String BROSZURA_TYP_CN = "PB";
	public static final String HARMONOGRAM_TYP_CN = "PH";
	public static final String SPECYFIKACJA_PRODUKTOWA_TYP_CN = "SP";
	
	/** POZWOLENIA */
	public static final String WF_READ = "WF_READ";
	public static final String WF_ADMIN = "WF_ADMIN"; 
	public static final String WF_READ_PROJECT = "WF_READ_PROJECT";
	public static final String WF_ADMIN_PROJECT = "WF_ADMIN_PROJECT"; 
	public static final String WF_READ_PRODUCT = "WF_READ_PRODUCT"; 
	public static final String WF_ADMIN_PRODUCT = "WF_ADMIN_PRODUCT"; 
	public static final String WF_READ_PROCDURE = "WF_READ_PROCEDURE"; 
	public static final String WF_ADMIN_PROCEDURE = "WF_ADMIN_PROCEDURE"; 
        
        public static final String WF_READ_DICTIONARY = "WF_READ_DICTIONARY";
        public static final String WF_ADMIN_DICTIONARY = "WF_ADMIN_DICTIONARY";
        public static final String WF_ACCEPTANCE_GROUP = "WF_ACCEPTANCE";
        
        
        public static final String WF_ROOT_FOLDER = "Archiwum Workflow";
        public static final String WF_PROJOEKTY_FOLDER = "Dokumentacja projektowa";
        public static final String WF_PRODUKTY_FOLDER = "Dokumentacja produktowa";
        public static final String WF_PROCEDURY_FOLDER = "Procedury";
        
        /** Uzytkownicy zaznaczenii do dekretacji*/
        public static final String DS_USER_APPROVER_CN = "DS_USER_APPROVER";
        /** nazwy dekretacji*/
        public static final String WF_GOTO_CHAIN_CN = "GOTO_CHAIN";
        public static final Integer WF_CHAIN_VALUE = 0;
        public static final String WF_GOTO_PARALLEL_CN = "GOTO_PARALLEL";
        public static final Integer WF_PARALLEL_VALUE = 1;
        
        public static final String WF_DECRET_AUTHOR_CN = "DECRET_AUTHOR";
        public static final String WF_DECRET_FURTHER_CN = "DECRET_FURTHER";
        
        public static final String WF_REJECT_REASON_CN = "REJECT_REASON";
        
        
        public static final String STATUS_FIELD_CN = "STATUS";
        public static final Integer STATUS_ZAREJESTROWANY = 5;
        public static final Integer STATUS_REALIZOWANY = 10;
        public static final Integer STATUS_ODRZUCONY = 20;
        public static final Integer STATUS_ZAAKCEPTOWANY = 30;
        public static final Integer STATUS_ANULOWANY = 40;
        
        public static final String APPROVE_TYPE_CN = "APPROVE_TYPE";
        
        public static final String PRACOWNIK = "pracownik"; 
        public static final String PRACOWNIK_ROWNOLEGLA = "pracownik_rownolegla"; 
        public static final String SZEF_DZIALU = "szef_dzialu"; 
        public static final String GUID = "rootdivision";
}
