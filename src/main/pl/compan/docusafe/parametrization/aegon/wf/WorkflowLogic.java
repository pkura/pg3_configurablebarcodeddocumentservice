package pl.compan.docusafe.parametrization.aegon.wf;

import com.opensymphony.xwork.ActionContext;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.sql.UserImpl;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.logic.Acceptable;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import com.opensymphony.webwork.ServletActionContext;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.RelatedDocumentBean;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.parametrization.aegon.wf.WfReport;
import pl.compan.docusafe.parametrization.aegon.wf.dictionary.WfName;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import org.hibernate.HibernateException;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSHttpContext;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesManager;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import static pl.compan.docusafe.parametrization.aegon.wf.WorkflowCn.*;

/**
 * Klasa logiki dokument�w workflow dla aegonu
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class WorkflowLogic extends BaseWorkflowLogic implements Acceptable
{

    private static final StringManager sm = StringManager.getManager(WorkflowLogic.class.getPackage().getName());
    private static WorkflowLogic instance;
    private final static Logger log = LoggerFactory.getLogger(WorkflowLogic.class);
    private WfName wfNameObject;
    
    public static WorkflowLogic getInstance()
    {
        if( instance == null )
            instance = new WorkflowLogic();
        return instance;
    }

    @Override
    public void correctValues(Map<String, Object> values, DocumentKind kind) throws EdmException
    {
      log.trace("WorkflowLogic#validate");
      
      String status = (String) values.get(STATUS_FIELD_CN);
      if(isZAREJESTROWANY(Integer.parseInt(status)))
      {
          String klasaCn = kind.getFieldByCn(KLASA_FIELD_CN).getEnumItem(Integer.parseInt(values.get(KLASA_FIELD_CN).toString())).getCn();
          String dataPisma = (String)values.get(DATA_PISMA_FIELD_CN);
          String dataDeadLine = (String)values.get(DATA_DEADLINE_FIELD_CN);


          if(values.get(KLASA_FIELD_CN) == null )
                throw new EdmException(sm.getString("NieWybranoDokumentu"));

          if(klasaCn.equals(WfReport.PROCEDURE_CLASS_ID))
          {
              if(values.get(PROCEDURE_NUMBER_FIELD_CN) == null)
                 throw new EdmException(sm.getString("PoleNumerProceduryWymagane"));

    //          if(!DSApi.context().hasPermission(new DSPermission("WF_ADMIN_PROCEDURE")))
    //            throw new EdmException(sm.getString("BrakUprawnien"));
          }

    //      if(klasaCn.equals(PROJEKTY_CLASS_CN) && !DSApi.context().hasPermission(new DSPermission("WF_ADMIN_PROJECT")))
    //          throw new EdmException(sm.getString("BrakUprawnien"));
    //      if(klasaCn.equals(PRODUKTY_CLASS_CN) && !DSApi.context().hasPermission(new DSPermission("WF_ADMIN_PRODUCT")))
    //          throw new EdmException(sm.getString("BrakUprawnien"));

          if (!DateUtils.isGreaterDate( dataDeadLine, dataPisma))
               throw new EdmException(sm.getString("DataZwrotuMusiBycWieksza"));

          try{
            if(!values.containsKey(DS_USER_APPROVER_CN))
              throw new EdmException(sm.getString("OsobyAkceptujaceSaWymagane"));
          } catch (NullPointerException e)
            {
              throw new EdmException(sm.getString("OsobyAkceptujaceSaWymagane"));
            }
      }
    }

    
    @Override
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
    {
      
    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
        log.info("type: {}" + type);
        Map<String, Object> values = new HashMap<String, Object>();
        values.put("TYPE", type);
        values.put(STATUS_FIELD_CN, STATUS_ZAREJESTROWANY);
        fm.reloadValues(values);
    }
    /**
     * Archiwizjacja dokumentacji workflow struktura jest zapisana w zmiennych
     * WF_ROOT_FOLDER/classFolder/year/....
     * @param document
     * @param type
     * @param as
     * @throws EdmException 
     */
    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
        if( document.getDocumentKind() == null )
            throw new NullPointerException("document.getDocumentKind()");

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        Calendar cal =  Calendar.getInstance();        
        Folder rootFolder = Folder.getRootFolder();
        Object docDate = fm.getValue(DATA_PISMA_FIELD_CN);
        
        cal.setTime(checkDocDate(docDate));
        
        int year = cal.get(Calendar.YEAR);
        
        //String dateFolder = getStringDate(cal.getTime());
        
        String klasaCn = fm.getEnumItem(KLASA_FIELD_CN).getCn();
        String typCn = fm.getValue(TYP_FIELD_CN).toString();
        WfName wfNazwaCn = (WfName)fm.getValue(WF_NAZWA_CN);
        String summaryTitle;
        
        if(isProject(fm) || isProduct(fm))
        {
            rootFolder = rootFolder.createSubfolderIfNotPresent(WF_ROOT_FOLDER);
            rootFolder = rootFolder.createSubfolderIfNotPresent(getClassFolder(klasaCn));
            rootFolder = rootFolder.createSubfolderIfNotPresent(new Integer(year).toString());
            rootFolder = rootFolder.createSubfolderIfNotPresent(wfNazwaCn.getNazwa());
            summaryTitle = typCn;
        }
        else
        {
            rootFolder = rootFolder.createSubfolderIfNotPresent(WF_ROOT_FOLDER);
            rootFolder = rootFolder.createSubfolderIfNotPresent(getClassFolder(klasaCn));
            rootFolder = rootFolder.createSubfolderIfNotPresent(new Integer(year).toString());
            rootFolder = rootFolder.createSubfolderIfNotPresent(typCn);
            summaryTitle = wfNazwaCn.getNazwa();
        }
        
        document.setFolder(rootFolder);
        document.setTitle(summaryTitle);
        
        if (document instanceof OfficeDocument)
            ((OfficeDocument)document).setSummary(summaryTitle);
        
    }

    @Override
    public void documentPermissions(Document document) throws EdmException
    {

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        
        DSApi.context().clearPermissions(document);
        String subject = WF_READ;
        String subjectModify = WF_ADMIN;

        if (isProject(fm))
        {
            subject = WF_READ_PROJECT;
            subjectModify = WF_ADMIN_PROJECT;
        }

        if (isProduct(fm))
        {
            subject = WF_READ_PRODUCT;
            subjectModify = WF_ADMIN_PRODUCT;
        }

        if (isProcedure(fm))
        {
            subject = WF_READ_PROCDURE;
            subjectModify = WF_ADMIN_PROCEDURE;
        }
        
        if(!subject.equalsIgnoreCase(WF_READ) && !subjectModify.equalsIgnoreCase(WF_ADMIN))
        {
            DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, WF_ADMIN, ObjectPermission.GROUP);
            DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, WF_READ, ObjectPermission.GROUP);
            
        }
        
        DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, subject, ObjectPermission.GROUP);
        DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, subjectModify, ObjectPermission.GROUP);

        try
        {
            DSApi.context().session().flush();
        }
        catch (HibernateException ex )
        {
            throw new EdmHibernateException(ex);
        }
    }
    
    private void findWfName(long id)
    {
        try{
            DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
            wfNameObject = new WfName().find(id);
            
        }catch (EdmException e)
        {
            log.error(e.toString());
        } finally
        {
            try
            {
                DSApi.close();
            } catch (EdmException e)
            {
            }
        }
    }

    @Override
    public List<RelatedDocumentBean> getRelatedDocuments(FieldsManager fm) throws EdmException
    {
        DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
    	
    	WfName wfNazwa = (WfName)fm.getValue(WF_NAZWA_CN);
        
    	// wyszukanie dokument�w
    	List<RelatedDocumentBean> documentBeans = DocWorkflowFactory.getReleatedDocuments(wfNazwa.getId());
    	
    	DSApi.close();
    	
    	return documentBeans;
    }
    
    /**
     * Metoda pobiera list� do akceptacji
     * 
     * @param document
     * @return
     * @throws EdmException 
     */
    public Map<String, String> getAcceptancesMap(OfficeDocument document) throws EdmException
    {
       Map<String, String> acceptancesMap = new LinkedHashMap<String, String>();
       FieldsManager fm = document.getFieldsManager();
       Integer approveType = (Integer) fm.getKey(APPROVE_TYPE_CN);
       //akceptujacy ludzie
       List<Integer> usersApprover = (ArrayList<Integer>) fm.getKey(DS_USER_APPROVER_CN);
       Long loggedUser = DSApi.context().getDSUser().getId();
       
       //akceptacja r�wnoleg�a nie daje si� typu dekretacji
       if(isParallelAcceptance(approveType))
           return acceptancesMap;
       
       try
        {
           Integer status = (Integer)fm.getKey(STATUS_FIELD_CN);
            if(isZAAKCEPTOWANY(status) || isANULOWANY(status))
                return acceptancesMap;
           
           // wniosek u pracownika zwracana jest lista dekretacja do autora i dalej(nastepna osoba na li�cie;
           if((isREALIZOWANY(status) || isODRZUCONY(status)) && usersApprover.contains(loggedUser.intValue()) && !isAuthor(document))
           {
               DSUser nextAcceptanceUser = getNextUserApprover(usersApprover, loggedUser.intValue(), document.getAuthor());
               
               //jest ostatnim u�ytkownikiem zwracany jest zawsze do autora
               if(document.getAuthor().equals(nextAcceptanceUser.getName()))
                   return acceptancesMap;
               
               if(checkUserAcceptedDocument(document))
                   return acceptancesMap;
               
               acceptancesMap.put(nextAcceptanceUser.getName(), "Dekretuj dalej");
               acceptancesMap.put(document.getAuthor(), "Dekretuj do autora");
           }
           
        }catch (UserNotFoundException e) 
        {
        }
        
       return acceptancesMap;
    }
        
    @Override
    public void checkCanFinishProcess(OfficeDocument document) throws AccessDeniedException, EdmException
    {
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        Integer status = (Integer) fm.getKey(STATUS_FIELD_CN);
        String error = "Nie mo�na zako�czy� pracy z dokumentem: Wniosek musi by� Odrzucony lub zaakceptowany finalnie.";
        boolean goodStatus = isZAAKCEPTOWANY(status) || isODRZUCONY(status);
        fm.initializeAcceptances();
        //lancuchowa
        if(isChainAcceptance((Integer) fm.getKey(APPROVE_TYPE_CN)))
        {
            if (goodStatus)
            {
            }
                return;
        }
        //rownolegla
        if(isParallelAcceptance((Integer) fm.getKey(APPROVE_TYPE_CN)))
        {
            if (goodStatus)
            {
            }
                return;
        }
        
        throw new AccessDeniedException(error);
    }

    /**
     * Funkcja zmienia status w zale�no�ci od ustawionego
     * @param document
     * @throws EdmException 
     */
    @Override
    public void onDecretation(Document document) throws EdmException
    {
        
        
    }
        
    @Override
    public void onSendDecretation(Document document, AcceptanceCondition condition) throws EdmException
    {
        
        String[] activities = WorkflowFactory.findAllDocumentTasks(document.getId());
        String activity = null;
        
        if(activities.length < 1)
            return;
        else
            for(String a: activities)
                activity = a;
        
        
        FieldsManager fm = document.getFieldsManager();
        Integer  approveType = (Integer)fm.getKey(APPROVE_TYPE_CN);
        Integer status = (Integer) fm.getKey(STATUS_FIELD_CN);
        List<Integer> usersApprover = (ArrayList<Integer>) fm.getKey(DS_USER_APPROVER_CN);
        
        if(usersApprover.isEmpty())
            throw new EdmException("Lista os�b akceptuj�cych jest pusta!");
        
        try{
            
            if(isZAREJESTROWANY(status) && isAuthor(document))
                DocWorkflowFactory.goToDocument(approveType, (OfficeDocument)document);
            
        }catch(SQLException e){ throw new EdmException("Blad dekretacji!");}
        
        //r�wnoleg�a wysylam do wiadomo�ci
        if(isParallelAcceptance(approveType))
        {
            for(Integer userId : usersApprover)
            {
                DSUser user = UserImpl.find(userId.longValue());
                
                WorkflowFactory.simpleAssignment(activity, GUID, user.getName(), null, WorkflowFactory.wfCcName(), 
                            WorkflowFactory.SCOPE_ONEUSER, null, null);
            }
            
            //ubijam dokument dla w�a�ciciela
            WorkflowFactory.getInstance().manualFinish(activity, false);
        }
        
        if(isChainAcceptance(approveType))
        {
            //przekazany do autora w ostatnim kroku
            if(condition.getUsername().equals(document.getAuthor()))
            {
                //NIE zaakceptowany przez wszystkich ustawia odrzucony
                if(!isAcceptedByAll(document))
                    setStatus(fm, STATUS_ODRZUCONY);
                else
                    setStatus(fm, STATUS_ZAAKCEPTOWANY);
            }
           
        }
    }
    
    /**
     * Funkcja wywolywana podczas akcji 'Dekretuj do akceptacji' podczas gdy u�ytkownik wybierze osob� z listy
     * (lista osob jest defioniowana zawsze w logice dokumentu)
     * @param document Document
     * @param userName String
     * @throws EdmException 
     */
    @Override
    public void onDecretationWhenUsernameSet(Document document,String userName) throws EdmException
    {
        FieldsManager fm = document.getFieldsManager();
        Integer status = (Integer) fm.getKey(STATUS_FIELD_CN);
        List<Integer> usersApprover = (ArrayList<Integer>) fm.getKey(DS_USER_APPROVER_CN);
        boolean isAuthor = isAuthor(document, userName);
       
        
        //wniosek ma status odrzucony i jest przekazany do pracownika
        if(isODRZUCONY(status) && !isAuthor)
        {
            setStatus(fm, STATUS_REALIZOWANY);
        }
        
        //Je�li author to ustawia status odrzucony
        if(isAuthor)
        {
            //nie zaakceptowany przez wszystkich
            if(!isAcceptedByAll(usersApprover, document.getId()))
                setStatus(fm, STATUS_ODRZUCONY);
            else
                setStatus(fm, STATUS_ZAAKCEPTOWANY);   
        }
    }
    
    @Override
    public void onAccept(String acceptanceCn, Long documentId) throws EdmException
    {
    }

    @Override
    public void onWithdraw(String acceptanceCn, Long documentId) throws EdmException
    {
        Document document = Document.find(documentId);
        FieldsManager fm = document.getFieldsManager();
        
        if(acceptanceCn.equals(PRACOWNIK))
        {
                if(isAcceptedByAll(document))
                    setStatus(fm, STATUS_ZAAKCEPTOWANY);
                else
                    setStatus(fm, STATUS_REALIZOWANY);
        }
        
        if(acceptanceCn.equals(SZEF_DZIALU))
        {
            document.setBlocked(Boolean.FALSE);
        }
    }

    @Override
    public boolean canSendDecretation(FieldsManager fm) throws EdmException
    {   
        try{
            Integer status = (Integer)fm.getKey(STATUS_FIELD_CN);
            List<Integer> usersApprover = (List<Integer>) fm.getKey(DS_USER_APPROVER_CN);
            
            DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));

            Document document = Document.find(fm.getDocumentId());
            Integer loggedUserId = DSApi.context().getDSUser().getId().intValue();
            boolean isAuthor = isAuthor(document);

            DSApi.close();
            
            // jesli inny ni� zarejestrowany nie mo�e dekretowa� dalej
            if(isZAREJESTROWANY(status) && isAuthor)
                return true;
            
            if(isChainAcceptance((Integer) fm.getKey(APPROVE_TYPE_CN)))
            {
                if(usersApprover.contains(loggedUserId) || isAuthor )
                    return true;
            }

            if (isParallelAcceptance((Integer) fm.getKey(APPROVE_TYPE_CN)))
            {
                if(isAuthor && isZAREJESTROWANY(status))
                {
                    return true;
                }
            }
            
            return false;
            
        } catch (EdmException ex)
        {
            log.error("",ex);
            throw ex;
        }
    }

    @Override
    public AcceptanceCondition nextAcceptanceCondition(Document document) throws EdmException
    {
        FieldsManager fm = document.getFieldsManager();
        Integer approveType = (Integer)fm.getKey(APPROVE_TYPE_CN);
        //lista osob akceptujacych
        List<Integer> usersApprover = (ArrayList<Integer>) fm.getKey(DS_USER_APPROVER_CN);
        //zalogowany
        Integer loggedUserId = new Integer(DSApi.context().getDSUser().getId().intValue());
        //czy zalogowany jest autorem
        boolean loggedAuthor = isAuthor(document);
        AcceptanceCondition acceptance;
        DSUser decretationUser;
        String type = PRACOWNIK;
        
        if(usersApprover.isEmpty())
            throw new EdmException("Pusta lista os�b dekretuj�cych!");
        
        if(!usersApprover.contains(loggedUserId) && !loggedAuthor)
            throw new EdmException("uzytkownik nie mo�e dekretowa�!");
        
        
        if(isChainAcceptance(approveType))
        {
            //zalogowany jest autor pobieram pierwsz� osob�
            if(loggedAuthor) 
               decretationUser = UserImpl.find(usersApprover.get(0).longValue());
            else   //pobieram nastepna po zalogowanej;
            {
                //sprawdzam czy odrzucil czy zaakceptowa�, nie autor
                 if(!isAcceptedOrRejected(document) )
                    throw new EdmException("Musisz zaakceptowa� b�d� te� odrzuci� dokument!");
                 
                int loggedPosition  = usersApprover.indexOf(loggedUserId);
                //Je�li ostatni to przekazuje do autora
                if((new Integer(usersApprover.size()).equals(loggedPosition+1)))
                {
                    decretationUser = UserImpl.findByUsername(document.getAuthor());
                    type = SZEF_DZIALU;
                }
                else //przekazuje nast�pn� osob�
                {
                   decretationUser = UserImpl.find(usersApprover.get(loggedPosition+1).longValue());
                }
            }
        }
        //r�wnoleg�a
        else if(isParallelAcceptance(approveType))
            {
                 decretationUser = DSApi.context().getDSUser();
            }
            else
                throw new EdmException("Ustaw typ akceptacji!");
        
        acceptance = getAcceptance(type, decretationUser.getName());
        return acceptance;
    }

    @Override
    public void onGiveAcceptance(Document document, String acceptanceCn) throws EdmException
    {
        FieldsManager fm = document.getFieldsManager();
        
        if(!isUserRejected(document))
            throw new EdmException("Dokument zosta� przez Ciebie odrzucony!");
        
        if(acceptanceCn.equals(PRACOWNIK))
        {
                if(isAcceptedByAll(document))
                    setStatus(fm, STATUS_ZAAKCEPTOWANY);
                else
                    setStatus(fm, STATUS_REALIZOWANY);
        }
        
        if(acceptanceCn.equals(SZEF_DZIALU))
        {
            setStatus(fm, STATUS_ZAAKCEPTOWANY);
            document.setBlocked(Boolean.TRUE);
        }
    }

    @Override
    public void onCommitManualFinish(Document document, Boolean isDoWiadomosci) throws EdmException
    {
        //normalne zakonczenie dokumentu
        if(!isDoWiadomosci)
            return;

        FieldsManager fm = document.getFieldsManager();
        boolean isODRZUCONY = isODRZUCONY((Integer)fm.getKey(STATUS_FIELD_CN));
        List<Integer> usersApprover = (List<Integer>) fm.getKey(DS_USER_APPROVER_CN);
        
        //rownolegla
        if (isParallelAcceptance((Integer) fm.getKey(APPROVE_TYPE_CN)) && !isODRZUCONY)
        {
             if(!isAcceptedOrRejected(document) )
                    throw new EdmException("Musisz zaakceptowa� b�d� te� odrzuci� dokument!");
             
            checkAcceptedRejectForParallel(document);
        }
    }

    @Override
    public boolean checkRemarkField(Document document, Map<String, Object> values, String remark) throws EdmException
    {
        DocumentKind documentKind = document.getDocumentKind();
        FieldsManager fm = document.getFieldsManager();
        Integer approveType = (Integer)fm.getKey(APPROVE_TYPE_CN);
        
        String finishFieldCn = documentKind.getProperties().get(DocumentKind.REMARK_FIELD_CN);
        if (finishFieldCn != null)
        {
            Field finishField = documentKind.getFieldByCn(finishFieldCn);
            Integer enumId = (Integer) finishField.coerce(values.get(finishFieldCn));
            if (enumId == null)
                return false;
            if (remark != null)
            {
                if (document instanceof OfficeDocument)
                {
                    if(!isUserAccepted(document))
                        throw new EdmException("Nie mo�esz odrzuci�.Dokument zosta� ju� zaakceptowany!");
                    
                    remark = "Przyczyna odrzucenia - " + remark;
                    ((OfficeDocument) document).addRemark(new Remark(StringUtils.left(remark, 4000), DSApi.context().getPrincipalName()));
                    
                    //r�wnoleg�a
                    if(isParallelAcceptance(approveType))
                    {
                        String[] activities = WorkflowFactory.findDocumentTasks(document.getId(), DSApi.context().getPrincipalName());
                        if(activities.length > 0)
                        {
                            String activity = activities[0];
                            
                            for(String a : activities)
                                activity = a;

                            //po podaniu przyczyny koncze z dokumentem
//                            DSHttpContext.setHttpFlashAttribute("document.archive.flash.message","Zadanie zwiazane z dokumentem zosta�o zako�czone!");
                            
//                            checkAcceptedRejectForParallel(document);
//                            WorkflowFactory.getInstance().manualFinish(activity, true);                         
                        }
                            
                    }
                }
                
                if ("true".equals(documentKind.getProperties().get(DocumentKind.FINISH_WITH_REMARK_ADD)))
                    return true;
            }
        }
        return false;
    }    
    
    /**
     * Sprawdza ile osob ju� zakonczylo zadanie zwiazane z dokumentem
     * @param document
     * @throws EdmException 
     */
    public void checkAcceptedRejectForParallel(Document document) throws EdmException
    {
        FieldsManager fm = document.getFieldsManager();
        List<Integer> usersApprover = (List<Integer>) fm.getKey(DS_USER_APPROVER_CN);
        boolean isAcceptedByAll = isAcceptedByAll(usersApprover, document.getId());
        
        String activites[] = WorkflowFactory.findAllDocumentTasks(document.getId());
        log.error("na li�cie=" + activites.length);
        
        //ilo�� zadan zwiazanych z dokumentem na li�cie u�ytkownik�w mniejsza od 1
        //dekretacja na autora dokumentu
        //-1 autora, -1 aktualnej osoby zalogowanej
        if(activites.length-1 < 1)
        {
                //sprawdzamy czy sa wszystkie akceptacje, 
                //je�li nie ma wszystkich => status = odrzucony
                //jesli sa wszystkie => status = zaakceptowany
                if (!isAcceptedByAll)
                    setStatus(fm, STATUS_ODRZUCONY);
                else
                {
                    setStatus(fm, STATUS_ZAAKCEPTOWANY);
                }
                
                WorkflowFactory.reopenWf(document.getId(), document.getAuthor());
                TaskSnapshot.updateTaskList(document.getId(), "wf", 2, document.getAuthor());
//                WorkflowFactory.simpleAssignment(activitesAuthor[0], GUID, document.getAuthor(), null, WorkflowFactory.wfManualName(), 
//                        WorkflowFactory.SCOPE_ONEUSER, null, null);
        }
    }

    @Override
    public void canReopenDocument(Document document) throws EdmException
    {
        if (!isAuthor(document))
            throw new EdmException("Nie masz uprawnie� do przywr�cenia dokumentu!");
    }
    
    
}
