package pl.compan.docusafe.parametrization.aegon.wf.dictionary;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.dictionary.AbstractDocumentKindDictionary;
import pl.compan.docusafe.parametrization.aegon.wf.WorkflowLogic;
import static pl.compan.docusafe.parametrization.aegon.wf.WorkflowCn.*;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * S�ownik dla dokument�w workflow - nazwa.
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class WfName extends AbstractDocumentKindDictionary
{
    private static WfName instance;
    private static final Logger log = LoggerFactory.getLogger(WfName.class);
    private StringManager sm = StringManager.getManager(WfName.class.getPackage().getName());
    private Long id;
    private String nazwa;
    private String opis;
    private Date dataRozpoczecia;
    private Long status;
    private Long kierownikTechniczny;
    private Long kierownikBiznesowy;
    private String statusName;

    public static synchronized WfName getInstance()
    {
        if( instance == null )
            instance = new WfName();

        return instance;
    }

    public WfName()
    {
    }

    /**
     * Wyszukuje konkretna nazwe ze slownika
     * @param id
     * @return
     * @throws EdmException
     */
    @Override
    public WfName find(Long id) throws EdmException
    {
        WfName inst = DSApi.getObject(WfName.class, id);
        if( inst == null )
            throw new EdmException(sm.getString("NieZnalezionoNazwyDlaDokumentu") + id);
        else
            return inst;
    }

    /**
     * Znajduje obiekty WfName na podstawie formularza wyszukiwania.
     * @param form
     * @return
     * @throws EdmException
     */
    public static SearchResults<? extends WfName> search(QueryForm form) throws EdmException
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(WfName.class);

            if( form.hasProperty("nazwa") )
                c.add(Restrictions.like("nazwa", "%" + form.getProperty("nazwa") + "%"));
            if( form.hasProperty("opis") )
                c.add(Restrictions.like("opis", "%" + form.getProperty("opis") + "%"));
            if( form.hasProperty("dataRozpoczecia") )
                c.add(Expression.eq("dataRozpoczecia", DateUtils.nullSafeParseJsDate(form.getProperty("dataRozpoczecia").toString())));
            if( form.hasProperty("status") )
                c.add(Expression.eq("status", form.getProperty("status")));
            if( form.hasProperty("kierownikTechniczny") )
                c.add(Expression.eq("kierownikTechniczny", form.getProperty("kierownikTechniczny")));
            if( form.hasProperty("kierownikBiznesowy") )
                c.add(Expression.eq("kierownikBiznesowy", form.getProperty("kierownikBiznesowy")));
            
            for( Iterator iter = form.getSortFields().iterator(); iter.hasNext(); )
            {
                QueryForm.SortField field = (QueryForm.SortField) iter.next();
                if( field.isAscending() )
                    c.addOrder(Order.asc(field.getName()));
                else
                    c.addOrder(Order.desc(field.getName()));
            }
            List<WfName> list = (List<WfName>) c.list();

            if( list.isEmpty() )
                return SearchResultsAdapter.emptyResults(WfName.class);
            else
            {
                int fromIndex = form.getOffset() < list.size() ? form.getOffset() : list.size() - 1;
                int toIndex = (form.getOffset() + form.getLimit() <= list.size()) ? form.getOffset() + form.getLimit() : list.size();

                return new SearchResultsAdapter<WfName>(list.subList(fromIndex, toIndex), form.getOffset(), list.size(), WfName.class);
            }
        }
        catch ( HibernateException e )
        {
            throw new EdmHibernateException(e);
        }
    }

    @Override
    public void create() throws EdmException
    {
        try
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch ( HibernateException e )
        {
            log.error(sm.getString(
                    "BladDodaniaNazwyDokumentuWorkflow") + e.getMessage());
            throw new EdmException(
                    sm.getString("BladDodaniaNazwyDokumentuWorkflow") + e.getMessage());
        }
    }

    @Override
    public void delete() throws EdmException
    {
        try
        {
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();
        }
        catch ( HibernateException e )
        {
            log.error(sm.getString(
                    "BladUsunieciaNazwyDokumentuWorkflow"));
            throw new EdmException(sm.getString(
                    "BladUsunieciaNazwyDokumentuWorkflow") + e.getMessage());
        }
    }
    
    public void cleanFolders() throws EdmException
    {
        Set<Folder> folders = new HashSet<Folder>();
        
        if( DocumentKind.isAvailable(DocumentKind.WORKFLOW_WF) )
        {
            DockindQuery query = new DockindQuery(0, 0);
            DocumentKind dc = DocumentKind.findByCn(DocumentKind.WORKFLOW_WF);
            query.setDocumentKind(dc);
            query.setCheckPermissions(false);
            query.field(dc.getFieldByCn(WF_NAZWA_CN), getId());
            Iterator<Document> it = DocumentKindsManager.search(query);
            while( it.hasNext() )
            {
                Document doc = it.next();
                folders.add(doc.getFolder());
                dc.logic().archiveActions(doc, -500);
            }
        }
        
    }
    /**
     * @param results List<? extends WfName>
     * @param statuses List<? extends WfNameStatus>
     * @return List<? extends WfName> 
     */
    public static List<? extends WfName> setWfNameStatusName(List<? extends WfName> results, List<WfNameStatus> statuses) {

        try {
            int i = 0;
            while (i < results.size()) {
                results.get(i).setStatusName(findStatusName(statuses, results.get(i).getStatus()));
                i++;
            }
        } catch (NullPointerException e) {
        } finally {
            return results;
        }

    }
    /**
     * @param statuses List<WfNameStatus>
     * @param id Long
     * @return String
     */
    public static String findStatusName(List<WfNameStatus> statuses, Long id) {
        Iterator userIterator = statuses.iterator();

        while (userIterator.hasNext()) {
            WfNameStatus status = (WfNameStatus) userIterator.next();
            if ((status.getId()).equals(id)) {
                return status.getTitle();
            }
        }

        return id.toString();
    }
    
    @Override
    public String getDictionaryDescription()
    {
        return nazwa;
    }

    @Override
    public String dictionaryAccept()
    {
        return null;
    }

    @Override
    public Map<String, String> dictionaryAttributes()
    {
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put("nazwa", String.valueOf("Nazwa projektu/produktu/ procedury/regulaminu"));

        return map;
    }

    @Override
    public Map<String, String> popUpDictionaryAttributes()
    {
        return null;
    }

    @Override
    public String dictionaryAction()
    {
        return "/admin/dictionaries/wf-name.action";
    }

    public Date getDataRozpoczecia()
    {
        return dataRozpoczecia;
    }

    public void setDataRozpoczecia(Date dataRozpoczecia)
    {
        this.dataRozpoczecia = dataRozpoczecia;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getKierownikBiznesowy()
    {
        return kierownikBiznesowy;
    }

    public void setKierownikBiznesowy(Long kierownikBiznesowy)
    {
        this.kierownikBiznesowy = kierownikBiznesowy;
    }

    public Long getKierownikTechniczny()
    {
        return kierownikTechniczny;
    }

    public void setKierownikTechniczny(Long kierownikTechniczny)
    {
        this.kierownikTechniczny = kierownikTechniczny;
    }

    public String getNazwa()
    {
        return nazwa;
    }

    public void setNazwa(String nazwa)
    {
        this.nazwa = nazwa;
    }

    public String getOpis()
    {
        return opis;
    }

    public void setOpis(String opis)
    {
        this.opis = opis;
    }

    public Long getStatus()
    {
        return status;
    }

    public void setStatus(Long status)
    {
        this.status = status;
    }

    public String getStatusName()
    {
        return statusName;
    }

    public void setStatusName(String statusName)
    {
        this.statusName = statusName;
    }  
}
