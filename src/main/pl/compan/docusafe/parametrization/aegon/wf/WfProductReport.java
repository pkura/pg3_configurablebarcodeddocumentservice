package pl.compan.docusafe.parametrization.aegon.wf;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class WfProductReport extends AbstractWfReport implements WfReport
{
    private static final Logger log = LoggerFactory.getLogger(WfProjectReport.class);
    
    private void initValues() throws Exception
    {
        for (ReportParameter param : getParams())
        {
            if (param.getType().equals("break-line"))
            {
                continue;
            }
        }
    }

    @Override
    public void doReport() throws Exception
    {
        LoggerFactory.getLogger("lukaszl").debug("WfProjectReport");

        try
        {
            this.initValues();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        initializeDumpers();
        //Naglowek
        setHeaders();
        
        PreparedStatement ps = null;
        
        try {
            ps = DSApi.context().prepareStatement(getSQL());
            ps.setLong(1, new Long(PRODUCT_CLASS_ID));
            ResultSet rs = ps.executeQuery();
            
            ps = DSApi.context().prepareStatement(getAcceptedUsers());
            
            int counter = 0;
            while (rs.next()) {
                Long documentId = rs.getLong("id");
                ps.setLong(1, documentId);
                ResultSet rsAccepted = ps.executeQuery();
                
                while(rsAccepted.next())
                {
                    addRecordLine(rs, ++counter, rsAccepted.getString(1), rsAccepted.getString(2));
                }
            }

            rs.close();

        } catch (Exception e) {
            throw new EdmException(e);
            //e.printStackTrace();
        } finally {
            DSApi.context().closeStatement(ps);
        }

        dumper.closeFileQuietly();
    }
}
