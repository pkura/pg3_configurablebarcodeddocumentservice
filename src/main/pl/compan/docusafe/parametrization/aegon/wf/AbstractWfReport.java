package pl.compan.docusafe.parametrization.aegon.wf;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.tools.CsvDumper;
import pl.compan.docusafe.reports.tools.PDFDumper;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;

/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public abstract class AbstractWfReport extends Report
{
    protected UniversalTableDumper dumper = new UniversalTableDumper();
    
     public void initializeDumpers() throws IOException{
        /** DUMPER **/
        XlsPoiDumper poiDumper = new XlsPoiDumper();
        CsvDumper csvDumper = new CsvDumper();
        PDFDumper pdfDumper = new PDFDumper();

        poiDumper.openFile(new File(this.getDestination(), "raport.xls"));
        this.dumper.addDumper(poiDumper);

        csvDumper.openFile(new File(this.getDestination(), "raport.csv"));
        this.dumper.addDumper(csvDumper);

        pdfDumper.openFile(new File(this.getDestination(), "raport.pdf"));
        this.dumper.addDumper(pdfDumper);
     }
     
    public String getSQL()
    {
        StringBuilder sql = new StringBuilder();

        sql.append("SELECT wf.DOCUMENT_ID as id, doc.AUTHOR as autor,wf.NUMER_PROCEDURY as numerprocedury, wfDic.NAZWA as nazwa, wf.DATA_DEADLINE as deadline ");
        sql.append("FROM DSG_WF as wf ");
        sql.append("LEFT JOIN DSG_WF_NAZWA_DICTIONARY as wfDic ON wf.WF_NAZWA = wfDic.ID ");
        sql.append("LEFT JOIN DS_DOCUMENT as doc ON wf.DOCUMENT_ID = doc.ID ");
        sql.append(" LEFT JOIN DSG_WF_KLASA as dsg_klasa ON dsg_klasa.id = wf.KLASA WHERE dsg_klasa.cn = ?");
        
//        DocumentKind.findByCn("wf").getFieldByCn("KLASA").getEnumItems();

        return sql.toString();
    }
    
    /**
     * Zwraca u�ytkownik�w, kt�rzy zaakceptowali dokument
     * @return 
     */
    public String getAcceptedUsers()
    {
        StringBuilder sql = new StringBuilder();
        
        sql.append("DECLARE @acceptedUser VARCHAR(8000);");
        sql.append("DECLARE @acceptedToDo VARCHAR(8000);");
        sql.append("EXEC getAcceptedUsers ?, @acceptedUser OUTPUT, @acceptedToDo OUTPUT;");
        sql.append("SELECT @acceptedUser as unameAccepted, @acceptedToDo as unameToDo;");
        return sql.toString();
    }
    
    public void setHeaders() throws IOException, EdmException 
    {
        this.addTitle();
        this.addCreatedBy();
        this.addCreatedDate();
        
        this.dumper.newLine();
       // this.dumper.addText("Lp.");
        this.dumper.addText(sm.getString("DokumentId"));
        this.dumper.addText(sm.getString("Autor"));
        this.dumper.addText(sm.getString("Nazwa"));
        this.dumper.addText(sm.getString("Deadline"));
        this.dumper.addText(sm.getString("WykonaneAkceptacje"));
        this.dumper.addText(sm.getString("AkceptacjeDoWykonania"));
        this.dumper.dumpLine();
    }
    
    /**
     * Ustawia tytul
     * @param reportTitle 
     */
    public void addTitle() throws IOException
    {
        this.dumper.addParagraph(sm.getString("Tytul") +": " + title);
        /** poprawi� dla procedur*/
//        this.dumper.newLine();
//        this.dumper.addText(sm.getString("Tytul"));
//        
//        for(String tit : titles)
//            this.dumper.addText(tit);
//        
//        for(int i =0; i < 6 - titles.length; i++)
//            this.dumper.addEmptyText();
//        
//        this.dumper.dumpLine();
    }
    /**
     * Tworzy wpis o tworcy raportu
     * @throws IOException
     * @throws EdmException 
     */
    public void addCreatedBy() throws IOException, EdmException
    {
        DSUser creator = UserImpl.findByUsername(username);
        this.dumper.addParagraph(sm.getString("UtworzonyPrzez") + ": " + creator.asFirstnameLastname());
//        this.dumper.newLine();
//        this.dumper.addText(sm.getString("UtworzonyPrzez"));
//        this.dumper.addText(creator.getFirstname());
//        this.dumper.addText(creator.getLastname());
//        this.dumper.addEmptyText();
//        this.dumper.addEmptyText();
//        this.dumper.addEmptyText();
//        this.dumper.addEmptyText();
//        this.dumper.dumpLine();
    }
    
    /**
     * Tworzy wpis o dacie utworzenia
     * @throws IOException 
     */
    public void addCreatedDate() throws IOException
    {
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        String dateFormat = sdf.format(date);
        this.dumper.addParagraph(sm.getString("DataUtworzenia") + ": " +dateFormat);
//        this.dumper.newLine();
//        this.dumper.addText(sm.getString("DataUtworzenia"));
//        this.dumper.addText(dateFormat);
//        this.dumper.addEmptyText();
//        this.dumper.addEmptyText();
//        this.dumper.addEmptyText();
//        this.dumper.addEmptyText();
//        this.dumper.addEmptyText();
//        this.dumper.dumpLine();
    }
            
    
    /**
     * Dodaje rekord do pliku
     */
     public void addRecordLine(ResultSet rs, int counter, String acceptedUsers, String acceptedToDo) throws IOException, SQLException {
        
        dumper.newLine();
        //dumper.addInteger(counter);
        dumper.addLong(rs.getLong("id"));
        dumper.addText(rs.getString("autor"));
        dumper.addText(rs.getString("nazwa"));
        dumper.addDate(rs.getDate("deadline"), "dd-MM-yyyy");
        
        try{
            dumper.addText(acceptedUsers.trim().substring(1));
        } catch (Exception ex) {
             log.info("Blad przy dokumencie acc: "+ rs.getLong("id") + "dla " + acceptedUsers, ex);
             dumper.addEmptyText();
         }
        
        try{
            dumper.addText(acceptedToDo.trim().substring(1));
        } catch (Exception ex){
             log.info("Blad przy dokumencie accTodo: "+ rs.getLong("id")+ "dla " + acceptedToDo, ex);
             dumper.addEmptyText();
         }
        dumper.dumpLine();
    }
}
