package pl.compan.docusafe.parametrization.aegon.wf;

import pl.compan.docusafe.core.office.Remark;
import com.lowagie.text.BadElementException;
import pl.compan.docusafe.parametrization.aegon.wf.dictionary.WfName;
import com.lowagie.text.Cell;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import pl.compan.docusafe.boot.Docusafe;
import com.opensymphony.webwork.ServletActionContext;
import java.io.File;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import java.util.*;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import static pl.compan.docusafe.parametrization.aegon.wf.WorkflowCn.*;

/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class BaseWorkflowLogic extends AbstractDocumentLogic
{
    private static final Logger log = LoggerFactory.getLogger(BaseWorkflowLogic.class);
    public Date checkDocDate(Object docDate)
    {                
        if(docDate == null)
            return new Date();
        else
            return (Date) docDate;       
    }
      
    public void setStatus(FieldsManager fm, Integer status) throws EdmException
    {
        Map<String, Object> values = new HashMap<String, Object>();
        values.put(STATUS_FIELD_CN, status);
        
        fm.reloadValues(values);
        fm.getDocumentKind().setOnly(fm.getDocumentId(), values);
    }
    
    @Override
    public boolean canAcceptDocument(FieldsManager fm, String acceptanceCn) throws EdmException
    {
        List<Integer> usersApprover = (ArrayList<Integer>) fm.getKey(DS_USER_APPROVER_CN);
        Document document = Document.find(fm.getDocumentId());
        Integer loggedUserId = new Integer(DSApi.context().getDSUser().getId().intValue());
        Integer status = (Integer) fm.getKey(STATUS_FIELD_CN);
        
        //akceptacja szefa dzialu zwraca akceptacje tylko autora
        if(acceptanceCn.equals(SZEF_DZIALU))
        {
            if(!isAuthor(document) || isZAREJESTROWANY(status))
                return false;
            else
            {
//                boolean isAcceptedByAll = isAcceptedByAll(usersApprover, fm.getDocumentId());
//                if(!isAcceptedByAll)
//                    return false;
                    return true;
            }
        }
        //zwraca akceptacje zalogowanego u�ytkownika je�li znajduje sie na li�cie
        if(acceptanceCn.equals(PRACOWNIK))
        {            
            if(isZAAKCEPTOWANY(status) || isODRZUCONY(status) ||
                    (document.getBlocked() != null && document.getBlocked().equals(Boolean.TRUE)))
                return false;
            
            if(usersApprover.contains(loggedUserId))
            {
                return true;
            }
        }
        
        return false;
    }
    
    
    public static String getStringDate(Date date)
    {
        return DateUtils.formatSqlDate(date != null ? date : new Date());
    }
          
    public String getClassFolder(String klasaCn)
    {
        if(klasaCn.equals(PROJEKTY_CLASS_CN))
            return WF_PROJOEKTY_FOLDER;
        else if(klasaCn.equals(PRODUKTY_CLASS_CN))
            return WF_PRODUKTY_FOLDER;
        else
            return WF_PROCEDURY_FOLDER;
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
    }

    @Override
    public void documentPermissions(Document document) throws EdmException
    {
    }
    
    public static boolean isZAREJESTROWANY(Integer status)
    {
        if(STATUS_ZAREJESTROWANY.equals(status))
            return true;
        else
            return false;
    }
    
    public static boolean isZAAKCEPTOWANY(Integer status)
    {
        if(STATUS_ZAAKCEPTOWANY.equals(status))
            return true;
        else
            return false;
    }
    
    public static boolean isODRZUCONY(Integer status)
    {
        if(STATUS_ODRZUCONY.equals(status))
            return true;
        else
            return false;
    }
    
    public static boolean isREALIZOWANY(Integer status)
    {
        if(STATUS_REALIZOWANY.equals(status))
            return true;
        else
            return false;
    }
    
    public static boolean isANULOWANY(Integer status)
    {
        if(STATUS_ANULOWANY.equals(status))
            return true;
        else
            return false;
    }
    
    /**
     * sprawdza czy akceptacja jest lancuchowa ustawiona
     * @param type
     * @return 
     */
    public static boolean isChainAcceptance(Integer type)
    {
        if(WF_CHAIN_VALUE.equals(type))
            return true;
        else
            return false;
    }
    
    /**
     * Sprawdza czy akceptacja ustawiona rownolegla
     */
    public static boolean isParallelAcceptance(Integer type)
    {
        if(WF_PARALLEL_VALUE.equals(type))
            return true;
        else
            return false;
    }
    
    
    /**
     * zwraca akceptacj� dla u�ytkownika 
     * @param type typ pracownik, szef_dziulu
     * @param username
     * @return AcceptanceCondition
     */
    public static AcceptanceCondition getAcceptance(String type, String username)
    {
        AcceptanceCondition accCon = new AcceptanceCondition();
        accCon.setCn(type);
        accCon.setUsername(username);
        
        return accCon;
    }
    
    /**
     * Zwraca nast�pnego u�ytkownika z listy
     * @param fm (ArrayList<Integer>) fm.getKey(DS_USER_APPROVER_CN)
     * @param actualUser
     * @param author
     * @return
     * @throws EdmException 
     */
    public DSUser getNextUserApprover(List<Integer> usersApprover, Integer actualUserId, String author) throws EdmException
    {
        DSUser decretationUser;
        int loggedPosition  = usersApprover.indexOf(actualUserId);
        
        if((new Integer(usersApprover.size()).equals(loggedPosition+1)))
        {
            decretationUser = UserImpl.findByUsername(author);
        }
        else //przekazuje nast�pn� osob�
        {
           decretationUser = UserImpl.find(usersApprover.get(loggedPosition+1).longValue());
        }
        
        return decretationUser;
    }    
    
    /** 
     * Zwraca czy wszyscy zaakceptowali dokument w lanuchowej
     * @todo kontrola na typie akceptacji lancuchowa czy rowno
     * @param usersApprover
     * @param documentId
     * @return
     * @throws EdmException 
     */
    public boolean isAcceptedByAll(List<Integer> usersApprover, Long documentId) throws EdmException
    {
        int docAcceptCount = DocumentAcceptance.count(documentId);
        if(usersApprover.size() == docAcceptCount)
            return true;
        else
            return false;
    }
    
    /** 
     * Zwraca czy wszyscy zaakceptowali dokument
     * @todo kontrola na typie akceptacji lancuchowa czy rowno
     * @param document
     * @return
     * @throws EdmException 
     */
    public boolean isAcceptedByAll(Document document) throws EdmException
    {
        FieldsManager fm = document.getFieldsManager();
        List<Integer> usersApprover = (ArrayList<Integer>) fm.getKey(DS_USER_APPROVER_CN);
        
        return isAcceptedByAll(usersApprover, document.getId());
        
    }
    
    public boolean canRemarkDocument(FieldsManager fm,Document document) throws EdmException
    {
        DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
            Integer loggedUserId = DSApi.context().getDSUser().getId().intValue();
        DSApi.close();
        
        List<Integer> usersApprover = (List<Integer>) fm.getKey(DS_USER_APPROVER_CN);
        
        //rownolegla
        if(isParallelAcceptance((Integer)fm.getKey(APPROVE_TYPE_CN)))
        {
            if(usersApprover.contains(loggedUserId) && isREALIZOWANY((Integer)fm.getKey(STATUS_FIELD_CN)))
                return true;
        }
        //lancuchowa
        if(isChainAcceptance((Integer) fm.getKey(APPROVE_TYPE_CN)))
        {
            if(usersApprover.contains(loggedUserId) && isREALIZOWANY((Integer)fm.getKey(STATUS_FIELD_CN)))
                return true;
        }
        
        return false;
    }

    @Override
    public boolean canGenerateDocumentView()
    {
        return true;
    }

    @Override
    public File generateDocumentView(Document document) throws EdmException
    {
        FieldsManager fm = document.getFieldsManager();
        
        File pdfFile = null;
        try
        {
            File fontDir = new File(Docusafe.getHome(), "fonts");
            File arial = new File(fontDir, "arial.ttf");
            BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font fontSuperSmall = new Font(baseFont, (float) 6);
            Font fontSmall = new Font(baseFont, (float) 8);
            Font fontNormal = new Font(baseFont, (float) 12);
            Font fontLarge = new Font(baseFont, (float) 16);

            pdfFile = File.createTempFile("docusafe_", "_tmp");
            pdfFile.deleteOnExit();
            com.lowagie.text.Document pdfDoc = new com.lowagie.text.Document(PageSize.A4);
            PdfWriter.getInstance(pdfDoc, new FileOutputStream(pdfFile));

            pdfDoc.open();

            Table topTable = new Table(2);
            topTable.setCellsFitPage(true);
            topTable.setWidth(100);
            topTable.setPadding(3);
            topTable.setBorder(0);

            Paragraph title = new Paragraph();
            title.add(new Phrase("Raport Akceptacji", fontLarge));

            Cell titleCell = new Cell(title);
            titleCell.setBorder(0);
            topTable.addCell(titleCell);
            // logo
            Image logo = Image.getInstance(Docusafe.getHome().getAbsolutePath() + "/img/" + Docusafe.getAdditionProperty("pdfCardGif"));
            Cell logoCell = new Cell(logo);
            logoCell.setBorder(0);
            topTable.addCell(logoCell);

            pdfDoc.add(topTable);
            Date deadline = (Date)fm.getValue(DATA_DEADLINE_FIELD_CN);
            WfName wfNazwaCn = (WfName)fm.getValue(WF_NAZWA_CN);
            DSUser author = UserImpl.findByUsername(document.getAuthor());
            String klasaCn = fm.getEnumItem(KLASA_FIELD_CN).getCn();
            // parametry dokumentu
            pdfDoc.add(new Paragraph("Id dokumentu: " + document.getId()));
            pdfDoc.add(new Paragraph("Autor: " + author.asFirstnameLastname()));
            pdfDoc.add(new Paragraph("Deadline: " + DateUtils.formatCommonDate(deadline)));
            pdfDoc.add(new Paragraph("Klasa dokumentu: " + fm.getValue(KLASA_FIELD_CN).toString()));
            pdfDoc.add(new Paragraph("Typ dokumentu: " + fm.getValue(TYP_FIELD_CN).toString()));
            pdfDoc.add(new Paragraph("Nazwa: " + wfNazwaCn.getNazwa()));
            
            if (klasaCn.equals(PROCEDURY_CLASS_CN))
            {
                pdfDoc.add(new Paragraph("Numer procedury/regulaminu: " + fm.getValue(PROCEDURE_NUMBER_FIELD_CN)));
            }
            
            Table table = new Table(2);
            table.setCellsFitPage(true);
            table.setWidth(100);
            table.setPadding(3);
            table.setBorder(0);

            pdfDoc.add(table);
            
            pdfDoc.add(new Paragraph("Wykonane akceptacje:"));
            
            int[] widths = new int[]{1, 1};
            table = new Table(2);
            table.setCellsFitPage(true);
            table.setWidth(100);
            table.setWidths(widths);
            table.setPadding(3);
            
            for (String text : new String[]{"Osoba wykonujaca akceptacj�", "Data i godzina akceptacji / przyczyna odrzucenia"})
            {
                Font fontHeader = fontSmall;
                fontHeader.setStyle(Font.BOLDITALIC);
                Cell cell = new Cell(new Phrase(text, fontHeader));
                cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
                cell.setVerticalAlignment(Cell.ALIGN_CENTER);
                cell.setBorderWidthBottom(2);
                cell.setBorderWidthLeft(1);
                cell.setBorderWidthRight(1);

                table.addCell(cell);
            }             
            
            List<DocumentAcceptance> docAcc = DocumentAcceptance.find(document.getId());
            
            for (DocumentAcceptance acceptance : docAcc)
            {
                DSUser authorAcc = UserImpl.findByUsername(acceptance.getUsername());
                String accTime = DateUtils.formatCommonDateTime(acceptance.getAcceptanceTime());
                table.addCell(getDefaultCell(authorAcc.asFirstnameLastname(), fontSmall));
                table.addCell(getDefaultCell(accTime, fontSmall));
            }
            
            List<Remark> remarks = document.getRemarks();
            
            for (Remark remark : remarks)
            {
                DSUser authorRemark = UserImpl.findByUsername(remark.getAuthor());
                table.addCell(getDefaultCell(authorRemark.asFirstnameLastname(), fontSmall));
                table.addCell(getDefaultCell(remark.getContent(), fontSmall));
            }
            
            pdfDoc.add(table);
            pdfDoc.close();
            
            
        } catch (Exception e)
        {
            LoggerFactory.getLogger("lukaszw").debug(e.getMessage(), e);
            throw new EdmException(e);
        }

        return pdfFile;
    }
    
     /**
     * Zwrca g��wne ustawienia dla kom�rki w pdf
     */
    private static Cell getDefaultCell(String text, Font font) throws BadElementException
    {
        Cell cell = new Cell(new Phrase(text, font));
        cell.setVerticalAlignment(Cell.ALIGN_CENTER);
        cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
        return cell;
    }
    
    public static boolean isProject(FieldsManager fm) throws EdmException
    {
        String klasaCn = fm.getEnumItem(KLASA_FIELD_CN).getCn();
        
        if (klasaCn.equals(PROJEKTY_CLASS_CN))
            return true;
        else
            return false;
    }
    
    public static boolean isProduct(FieldsManager fm) throws EdmException
    {
        String klasaCn = fm.getEnumItem(KLASA_FIELD_CN).getCn();
        
        if (klasaCn.equals(PRODUKTY_CLASS_CN))
            return true;
        else
            return false;
    }
    
    public static boolean isProcedure(FieldsManager fm) throws EdmException
    {
        String klasaCn = fm.getEnumItem(KLASA_FIELD_CN).getCn();
        
        if (klasaCn.equals(PROCEDURY_CLASS_CN))
            return true;
        else
            return false;
    }
    
    /** sprawdza czy u�ytkownik zaakceptowa� dokument*/
    public boolean checkUserAcceptedDocument(Document document) throws EdmException
    {
        List<DocumentAcceptance> acceptances = DocumentAcceptance.find(document.getId(), DSApi.context().getPrincipalName(),null,null);
               
        if(acceptances.isEmpty())
           return false;

        return true;
    }
    
    
    
    
}
