package pl.compan.docusafe.parametrization.aegon;

import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.ArrayList;

import org.apache.commons.dbutils.DbUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.imports.dsi.DSIBean;
import pl.compan.docusafe.service.imports.dsi.DSIImportHandler;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.service.tasklist.TaskList;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.LoggerFactory;

public class AbsenceRequestDSIHandler extends DSIImportHandler {

	private Long DocExternalId;
	
	protected void prepareImport() throws Exception 
	{
		if(values.get("EXTERNAL_ID") == null)
		{
			DocExternalId = null;
		}
		else
		{
			DocExternalId = Long.parseLong((String) values.get("EXTERNAL_ID"));
			importDocument = Document.find(DocExternalId);
		}
		
		LoggerFactory.getLogger("tomekl").debug("DocExternalId {}",DocExternalId);
		LoggerFactory.getLogger("tomekl").debug("importDocument {}",importDocument);
		
		String nowyId = (String) values.get("NEW_EXTERNAL_ID");
		values.put("EXTERNAL_ID", nowyId);
		values.remove("NEW_EXTERNAL_ID");		
	}

	@Override
	protected boolean isStartProces()
	{
		return true;
	}
	
	public void actionAfterCreate(Long documentId, DSIBean dsiB)throws Exception
	{
		super.actionAfterCreate(documentId, dsiB);
		OfficeDocument doc = OfficeDocument.find(documentId);
		PreparedStatement ps = null;
		try
		{
			ps = DSApi.context().prepareStatement("delete from DS_DOCUMENT_ACCEPTANCE where document_id = ?");
			ps.setLong(1, documentId);
			ps.execute();
			DbUtils.closeQuietly(ps);
			
			for(String str : values.keySet())
			{
				if(str.startsWith("ACCEPTANCE_CN_"))
				{
					LoggerFactory.getLogger("tomekl").debug("ac {} {}",str,str.split("_")[2]);
					Integer i = Integer.parseInt(str.split("_")[2]);
					ps = DSApi.context().prepareStatement("insert into DS_DOCUMENT_ACCEPTANCE(document_id,acceptanceTime,acceptanceCn,username) values(?,?,?,?)");
					ps.setLong(1, documentId);
					ps.setTimestamp(2, new Timestamp(DateUtils.parseJsDateTime((String) values.get("ACCEPTANCE_TIME_"+i.intValue())).getTime()));
					ps.setString(3, (String) values.get("ACCEPTANCE_CN_"+i.intValue()));
					ps.setString(4, (String) values.get("ACCEPTANCE_USER_"+i.intValue()));
					ps.execute();
					DbUtils.closeQuietly(ps);
				}
			}
		}
		catch (Exception e) 
		{
			//e.printStackTrace();
			log.debug(e.getMessage(),e);
		}
		
		setUpDocumentAfterCreate(doc);
		String targetUser = (String) values.get("TARGET_USER");
		WorkflowFactory.createNewProcess(doc, false, targetUser, Docusafe.getAdditionProperty("external.acceptance.agent"), DSDivision.ROOT_GUID, Docusafe.getAdditionProperty("external.acceptance.agent"));
		
		//if(DocExternalId == null)
		//	doc.getDocumentKind().logic().onStartProcess(doc);
		
		TaskSnapshot.updateByDocumentId(doc.getId(), doc.getStringType());
	}
	
	public boolean isRequiredAttachment() 
	{
		return false;
	}
	
	public boolean isRequiredBox()
	{
		return false;
	}
	
	public String getDockindCn() 
	{
		return DocumentLogicLoader.ABSENCE_REQUEST;
	}

}
