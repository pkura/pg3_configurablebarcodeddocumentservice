package pl.compan.docusafe.parametrization.aegon;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.durables.Durable;
import pl.compan.docusafe.core.base.durables.DurablesFactory;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesManager;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance;
import pl.compan.docusafe.core.dockinds.dictionary.AccountNumber;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dictionary.DpInst;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.reports.tools.CsvDumper;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.util.StringManager;

import com.lowagie.text.Cell;
import com.lowagie.text.Chapter;
import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.opensymphony.webwork.ServletActionContext;

/**
 * Logika faktury kosztowej PTE
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class InvoicePteLogic extends InvoiceLogic 
{
	private static final String DOTYCZY_SRODKOW_TRWALYCH = "FAK_DO_SR_TRW";
	private static final String SRODKI_TRWALE = "SRODKI_TRWALE";
	
	/* AKCEPTACJE */
	private static final String AC_ADMINISTRATOR_ST = "admin_st";
	private static final String AC_MERYTORYCZNA = "merytoryczna";
	private static final String AC_FORMALNO_PRAWNA = "prawna";
	private static final String AC_SZEFA_CENTRUM = "szefa_centrum";
	private static final String AC_KSIEGOWA = "ksiegowa";
	
	private static final String STATE_NADANO_PRAWNA = "NADANO_PRAWNA";
	
	private static final String ADMINISTRATOR_ST_GUID = "ADMINISTRATOR_ST";
	private static final String ACCEPTANCE_STATE = "ACCEPTANCE_STATE";
	private static final String IS_ADMIN_ST_ACC = "IS_ADMIN_ST_ACC";
	private static final String CURRENCY_CN = "CURRENCY";
	
	private static InvoicePteLogic instance;
	
	private CentrumKosztowDlaFaktury centrum;
	
	/**
	 * Akcja wykonywana podczas akceptacji faktury
	 */
	public void onAccept(String acceptanceCn, Long documentId) throws EdmException
	{
		// nie potrzebne w ej chwili
		//this.nextAcceptanceCondition(Document.find(documentId));
	}
	
	/**
	 * Akcja wykonywana podczas cofni�cia akcjeptacji
	 */
	public void onWithdraw(String acceptanceCn, Long documentId) throws EdmException
	{
		// nie potrzebne w tej chwili
		//this.nextAcceptanceCondition(Document.find(documentId));
	}
	
	/**
	 * Ustawienie warto�ci pocz�tkowych
	 */
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
        Map<String,Object> values = new HashMap<String,Object>();
        if (DocumentLogic.TYPE_ARCHIVE != type)
            values.put(STATUS_FIELD_CN, STATUS_W_TRAKCIE);
        
        values.put(CURRENCY_CN, 240);
        
        fm.reloadValues(values);
    }

    @Override
    public TaskListParams getTaskListParams(DocumentKind kind, long documentId) throws EdmException {
        return TaskListParams.fromMagicArray(getTaskListParam(kind, documentId));
    }

	private Object[] getTaskListParam(DocumentKind dockind,Long id) throws EdmException
    {
        Object[] result = new Object[10];
        StringBuilder tmp = new StringBuilder();
        List<CentrumKosztowDlaFaktury> centrumList =  CentrumKosztowDlaFaktury.findByDocumentId(id);
        for (CentrumKosztowDlaFaktury centrum : centrumList)
        {
            tmp.append(centrum.getDescription()+" | ");
        }
        FieldsManager fm = dockind.getFieldsManager(id);
        DpInst dic = (DpInst) fm.getValue(DOSTAWCA_FIELD_CN);

        result[0] = fm.getValue(STATUS_FIELD_CN);
        result[3] = fm.getValue("KATEGORIA");
        result[4] = StringUtils.left(tmp.toString(), 20);
        result[5] =  dic != null ? StringUtils.left(String.valueOf(dic.getNumerKontrahenta()), 20) : "brak";
        result[6] = fm.getValue(DATA_PLATNOSCI_FIELD_CN) != null ? DateUtils.formatCommonDate((Date)fm.getValue(DATA_PLATNOSCI_FIELD_CN)) : "brak";
        result[7] = String.valueOf(fm.getValue(KWOTA_BRUTTO_FIELD_CN));
        result[8] = fm.getValue(DATA_WYSTAWIENIA_FIELD_CN);
        return result;
    }
	
	/**
	 * Sprawdza czy mo�na dekretowa� (na formatce blokuje guzik 'dekretuj do akceptacji')
	 */
	@Override
	public boolean canSendDecretation(FieldsManager fm) throws EdmException 
	{
		DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
		boolean ret = true;
		// je�li dokument jest w stanie akceptacji formalno-prawnej - wy��czenie przycisk�w dekretacji
		if (DocumentAcceptance.find(fm.getDocumentId(), AC_FORMALNO_PRAWNA, null) == null || 
			DocumentAcceptance.find(fm.getDocumentId(), AC_MERYTORYCZNA, null) == null)
		{
			ret = false;
		}
		DSApi.close();
		return ret;
	}

	/**
	 * Sprawdza czy mo�na zaakceptowa� faktur� dan� akceptacj� (sprawdza kolejno�� akceptacji)
	 */
	@Override
	public boolean canAcceptDocument(FieldsManager fm, String acceptanceCn)
			throws EdmException 
	{
		if (acceptanceCn.equals(AC_FORMALNO_PRAWNA) && DocumentAcceptance.find(fm.getDocumentId(), AC_FORMALNO_PRAWNA, null) == null)
			return true;
		else if (acceptanceCn.equals(AC_MERYTORYCZNA) && 
				 DocumentAcceptance.find(fm.getDocumentId(), AC_MERYTORYCZNA, null) == null &&
				 DocumentAcceptance.find(fm.getDocumentId(), AC_FORMALNO_PRAWNA, null) != null)
		{
			if (DocumentAcceptance.find(fm.getDocumentId(), AC_MERYTORYCZNA, null) != null) 
				// je�li nadano ju� akceptacj� merytoryczn� to nie mo�na tego powt�rzy�
				return false;
			return true;
		}
		else if (acceptanceCn.equals(AC_ADMINISTRATOR_ST) && fm.getBoolean(DOTYCZY_SRODKOW_TRWALYCH) &&
				 DocumentAcceptance.find(fm.getDocumentId(), AC_ADMINISTRATOR_ST, null) == null &&
				 DocumentAcceptance.find(fm.getDocumentId(), AC_FORMALNO_PRAWNA, null) != null &&
				 DocumentAcceptance.find(fm.getDocumentId(), AC_MERYTORYCZNA, null) != null)
		{
			return true;
		}
		else if (acceptanceCn.equals(AC_SZEFA_CENTRUM) &&
				 DocumentAcceptance.find(fm.getDocumentId(), AC_FORMALNO_PRAWNA, null) != null &&
				 DocumentAcceptance.find(fm.getDocumentId(), AC_MERYTORYCZNA, null) != null)
		{
			if (centrum != null)
			{
				if (fm.getBoolean(AKCEPTACJA_BEZ_KWOTY_CN))
					return true;
				
				List<AcceptanceCondition> conditions = AcceptanceCondition.findByAmount(centrum.getCentrumId(), centrum.getRealAmount(), acceptanceCn);
				for (AcceptanceCondition condition : conditions)
				{
					// je�li akceptacje posiada zalogowany uzytkownik
					if (condition.getUsername().equals(DSApi.context().getPrincipalName()))
						return true;
				}
			}
		}
		else if (acceptanceCn.equals(AC_KSIEGOWA) && 
				 DocumentAcceptance.find(fm.getDocumentId(), AC_KSIEGOWA, null) == null &&
				 DocumentAcceptance.find(fm.getDocumentId(), AC_FORMALNO_PRAWNA, null) != null &&
				 DocumentAcceptance.find(fm.getDocumentId(), AC_MERYTORYCZNA, null) != null)
		{
			List<CentrumKosztowDlaFaktury> fakturies = CentrumKosztowDlaFaktury.findByDocumentId(fm.getDocumentId());
			boolean status = true;
			// sprawdzenie akceptacji wszystkich centrow
			for (CentrumKosztowDlaFaktury faktura : fakturies)
			{
				if (DocumentAcceptance.find(fm.getDocumentId(), AC_SZEFA_CENTRUM, faktura.getId()) == null)
					status = false;
			}

			if (status)
				return true;
		}
		
		return false;
	}
	
	/**
	 * Akcja uruchamiana na starcie procesu obiegu faktury.
	 */
	@Override
	public void onStartProcess(OfficeDocument document) throws EdmException 
	{
		super.onStartProcess(document);
		updateAcceptanceState(document, AC_FORMALNO_PRAWNA);
	}
	
	/**
	 * Akcja uruchamiana podczas nadania akceptacji
	 */
	@Override
	public void onGiveAcceptance(Document document, String acceptanceCn)
			throws EdmException 
	{
		if (acceptanceCn.equals(AC_FORMALNO_PRAWNA))
		{
			updateAcceptanceState(document, STATE_NADANO_PRAWNA);
		}
		else if (acceptanceCn.equals(AC_MERYTORYCZNA))
		{
			if (document.getFieldsManager().getValue(POTWIERDZENIE_ZGODNOSCI_FIELD_CN) == null)
			{
				AcceptancesManager.withdrawAcceptance(document.getId(), acceptanceCn);
				throw new EdmException("Brak potwierdzenia zgodno�ci!");
			}
			
			List centrum = (List) document.getFieldsManager().getValue(CENTRUM_KOSZTOWE_FIELD_CN);
			if (centrum == null || centrum.size() == 0)
			{
				AcceptancesManager.withdrawAcceptance(document.getId(), acceptanceCn);
				throw new EdmException("Brak wybranych centr�w kosztowych!");
			}
			
			updateAcceptanceState(document, AC_MERYTORYCZNA);
		}
		else if (acceptanceCn.equals(AC_ADMINISTRATOR_ST))
		{
			List values = (List) document.getFieldsManager().getValue(SRODKI_TRWALE);
			if (values == null || values.size() == 0)
			{
				AcceptancesManager.withdrawAcceptance(document.getId(), acceptanceCn);
				throw new EdmException("Nie przypisano �rodk�w trwa�ych dla faktury!");
			}
			
			Map<String, Object> vals = new HashMap<String, Object>();
			vals.put(IS_ADMIN_ST_ACC, Boolean.TRUE);
			document.getDocumentKind().setOnly(document.getId(), vals);
		}
	}
	
	/**
	 * Sprawdza czy mo�na cofn�� akceptacj�
	 * @param document
	 * @param acceptanceCn
	 */
	@Override
	public void canWithdrawAcceptance(Document document, String acceptanceCn) throws EdmException
	{
		if (document.getDocumentKind().getFieldsManager(document.getId()).getBoolean(AKCEPTACJA_FINALNA_FIELD_CN) &&
				!AC_KSIEGOWA.equals(acceptanceCn))
			throw new EdmException("Faktura jest zaakceptowana finalnie! Mo�na cofn�� tylko akceptacj� kontrolingow�. " +
					"Przedekretuj faktur� do u�ytkownika z odpowiednimi uprawnieniami!");
	}
	
	/**
	 * Akcja uruchamiana pod anulowaniu akcpetacji
	 */
	@Override
	public void onWithdrawAcceptance(Document document, String acceptanceCn)
			throws EdmException 
	{
		if (AC_ADMINISTRATOR_ST.equals(acceptanceCn))
		{
			Map<String, Object> vals = new HashMap<String, Object>();
			vals.put(IS_ADMIN_ST_ACC, Boolean.FALSE);
			document.getDocumentKind().setOnly(document.getId(), vals);
		}
		super.onWithdrawAcceptance(document, acceptanceCn);
	}
	
	/**
	 * Sprawdza czy mo�na usuwa� za��czniki.
	 */
	@Override
	public boolean canDeleteAttachments(Document document) throws EdmException
	{
		// je�li akceptacja finalna to zablokowanie mo�liwo�� usuwania za��cznik�w
		return !document.getDocumentKind().getFieldsManager(document.getId()).getBoolean(AKCEPTACJA_FINALNA_FIELD_CN);
	}
	
	/**
	 * Akcja archiwizacji faktury.
	 */
	@Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");

        if(document.getDocumentKind().equals(DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND)))
        {
            archiveActionsVAT(document, type);
            return;
        }

        Calendar cal = Calendar.getInstance();

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        Folder folder = Folder.getRootFolder();

        Date dat = (Date) fm.getValue(DATA_WYSTAWIENIA_FIELD_CN);
        
        Map<String,Object> values = new HashMap<String,Object>();
        if(fm.getValue(DATA_ZAKSIEGOWANIA_FIELD_CN) != null) //bug fix 441
        {
        	dat = (Date) fm.getValue(DATA_ZAKSIEGOWANIA_FIELD_CN);
        	values.put(STATUS_FIELD_CN, STATUS_ZAKSIEGOWANA);
        }
        
        if (fm.getValue(DATA_ZAPLATY_FIELD_CN) != null)
        	values.put(ZAPLACONO_FIELD_CN, Boolean.TRUE);
            
        // ustawienie warto�ci p�l
        document.getDocumentKind().setOnly(document.getId(), values);

        cal.setTime(dat);
        folder = folder.createSubfolderIfNotPresent("Archiwum Faktur Kosztowych");
        folder = folder.createSubfolderIfNotPresent(String.valueOf(cal.get(Calendar.YEAR)));
        folder = folder.createSubfolderIfNotPresent(String.valueOf(MONTHS[cal.get(Calendar.MONTH)]));
        document.setFolder(folder);
        
        DpInst inst = (DpInst) fm.getValue(DOSTAWCA_FIELD_CN);
        //DicInvoice inst = (DicInvoice) fm.getValue(DOSTAWCA_FIELD_CN);
        String title = inst.getName() + " (";
        if (fm.getDescription(NUMER_TRANSAKCJI_FIELD_CN) != null)
            title +=  fm.getDescription(NUMER_TRANSAKCJI_FIELD_CN) + ", ";
        title += fm.getDescription(NUMER_FAKTURY_FIELD_CN);
        title += ", "  +fm.getDescription(DATA_WYSTAWIENIA_FIELD_CN) + ")";
        String summary = inst.getName() + ", " + fm.getDescription(NUMER_FAKTURY_FIELD_CN) + ", " + fm.getDescription(DATA_WYSTAWIENIA_FIELD_CN);

        if (document instanceof OfficeDocument)
            ((OfficeDocument)document).setSummaryOnly(summary);

        document.setTitle(title);
        document.setDescription(title);
        
        /** SENDER **/
        if (document instanceof OfficeDocument)
        {
        	Sender sender = null;
        	OfficeDocument doc = (OfficeDocument) document;
        	if(doc.getSender() != null)
        	{
        		sender = doc.getSender();        		
        	}
        	else
        	{
        		sender = new Sender();        		
        	}
        	
        	sender.setAnonymous(Boolean.FALSE);
        	sender.setCountry(inst.getKraj());
        	sender.setEmail(inst.getEmail());
        	sender.setFax(inst.getFaks());
        	sender.setFirstname(inst.getImie());
        	sender.setLastname(inst.getNazwisko());
        	sender.setLocation(inst.getMiejscowosc());
        	sender.setNip(inst.getNip());
        	sender.setStreet(inst.getUlica());
        	sender.setZip(inst.getKod());
        	sender.setOrganization(inst.getName());
        	doc.setSender(sender);
        }
        //uruchomione aby uaktualinic pole acceptances state
        //this.getInstance().nextAcceptanceCondition(document);
	}
	
	/**
	 * Ustawienie bierz�cego centrum kosztowego.
	 * @param centrum
	 */
	public void setCentrum(CentrumKosztowDlaFaktury centrum) 
	{
		this.centrum = centrum;
	}
	
	/**
	 * Zwr�cenie bierz�cego centrum kosztowego.
	 * @return
	 */
	public CentrumKosztowDlaFaktury getCentrum() 
	{
		return centrum;
	}
	
	/**
	 * Singleton
	 * @return
	 */
	public static synchronized InvoicePteLogic getInstance()
    {
        if (instance == null)
            instance = new InvoicePteLogic();
        return instance;
    }
	
	/**
	 * Zwraca base guid
	 */
	@Override
	protected String getBaseGUID(Document document) throws EdmException
    {
    	return Docusafe.getAdditionProperty("baseGuid");
    }
	
	/**
	 * Ustawienie uprawnie� do faktury.
	 */
	@Override
	public void documentPermissions(Document document) throws EdmException 
	{
		//super.documentPermissions(document);
		
		Set<PermissionBean> perms = new HashSet<PermissionBean>();
		
//		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
//        List<CentrumKosztowDlaFaktury> list = (List<CentrumKosztowDlaFaktury>) fm.getValue(CENTRUM_KOSZTOWE_FIELD_CN);

//        for (CentrumKosztowDlaFaktury centrum : list)
//        {
//            //createPermission(document, "Faktury kosztowe - MPK "+centrum.getCentrumCode()+" - odczyt", "INVOICE_"+centrum.getCentrumId()+"_READ", new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS});
//            String readName = "Faktury kosztowe - MPK "+centrum.getCentrumCode()+" - odczyt";
//            String readGuid = "INVOICE_"+centrum.getCentrumId()+"_READ";
//
//            perms.add(new PermissionBean(ObjectPermission.READ,readGuid,ObjectPermission.GROUP,readName));
//            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,readGuid,ObjectPermission.GROUP,readName));
//
//            //createPermission(document, "Faktury kosztowe - MPK "+centrum.getCentrumCode()+" - modyfikacja", "INVOICE_"+centrum.getCentrumId()+"_MODIFY", new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS});
//
//            String writeName = "Faktury kosztowe - MPK "+centrum.getCentrumCode()+" - modyfikacja";
//            String writeGuid = "INVOICE_"+centrum.getCentrumId()+"_MODIFY";
//
//            perms.add(new PermissionBean(ObjectPermission.MODIFY, writeGuid,ObjectPermission.GROUP,writeName));
//            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, writeGuid,ObjectPermission.GROUP,writeName));
//
//            AccountNumber an = centrum.getAccountNumber() != null ? AccountNumber.findByNumber(centrum.getAccountNumber()) : null;
//            if(document.getDocumentKind().getCn().equals(DocumentKind.INVOICE_PTE) && an != null)
//            {
//                //createPermission(document, "Faktury kosztowe - KONTO "+an.getNumber()+" - odczyt", "INVOICE_ACCOUNT_"+an.getId()+"_READ", new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS});
//
//                readName = "Faktury kosztowe - KONTO "+an.getNumber()+" - odczyt";
//                readGuid = "INVOICE_ACCOUNT_"+an.getId()+"_READ";
//
//                perms.add(new PermissionBean(ObjectPermission.READ,readGuid,ObjectPermission.GROUP,readName));
//                perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,readGuid,ObjectPermission.GROUP,readName));
//
//            }
//        }
        //DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "INVOICE_READ", ObjectPermission.GROUP);
        perms.add(new PermissionBean(ObjectPermission.READ,"INVOICE_READ",ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"INVOICE_READ",ObjectPermission.GROUP));

        //DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "INVOICE_MODIFY", ObjectPermission.GROUP);
        perms.add(new PermissionBean(ObjectPermission.MODIFY,"INVOICE_MODIFY",ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"INVOICE_MODIFY",ObjectPermission.GROUP));
        
		
		perms.add(new PermissionBean(ObjectPermission.MODIFY, ADMINISTRATOR_ST_GUID, ObjectPermission.GROUP, "Administrator ST"));
		setUpPermission(document, perms);
	}
	
	/**
	 * Zwraca nast�pn� akceptacj�
	 */
	@Override
	public AcceptanceCondition nextAcceptanceCondition(Document document)
			throws EdmException 
	{
		FieldsManager fm = document.getFieldsManager();
		
		if (DocumentAcceptance.find(document.getId(), AC_FORMALNO_PRAWNA, null) == null)
		{
			updateAcceptanceState(document, AC_FORMALNO_PRAWNA);
			// nie ma akceptacji formalno prawnej, wyszukanie takiego warunku akceptacji
			return getAcceptanceConditionByCN(AC_FORMALNO_PRAWNA);
		}
		else if (DocumentAcceptance.find(document.getId(), AC_MERYTORYCZNA, null) == null)
		{
			updateAcceptanceState(document, AC_MERYTORYCZNA);
			// brak akceptacji merytorycznej, wyszukanie takiego warunku akceptacji
			return getAcceptanceConditionByCN(AC_MERYTORYCZNA);
		}
		else if (fm.getBoolean(DOTYCZY_SRODKOW_TRWALYCH) && DocumentAcceptance.find(document.getId(), AC_ADMINISTRATOR_ST, null) == null)
		{
			updateAcceptanceState(document, AC_ADMINISTRATOR_ST);
			return getAcceptanceConditionByCN(AC_ADMINISTRATOR_ST);
		}
		
		AcceptanceCondition returnVal = super.nextAcceptanceCondition(document);
		updateAcceptanceState(document, returnVal.getCn());
		return returnVal;
	}
	
	/**
	 * Wyszukanie danej akceptacji po CN
	 * @param cn
	 * @return
	 * @throws EdmException
	 */
	private AcceptanceCondition getAcceptanceConditionByCN(String cn) throws EdmException
	{
		List<AcceptanceCondition> conditions = AcceptanceCondition.find(cn);
		if (conditions != null && conditions.size() > 0)
			return conditions.get(0);
		else
			throw new EdmException("Nie uda�o si� przekaza� pisma. Brak zdefiniowanych os�b dla akceptacji: Akceptacja " + cn + 
					". Prosz� przedekretowa� faktur� r�cznie.");
	}
	
	/**
	 * Uaktualnienie statusu akceptacji
	 * @param document
	 * @param acceptanceCn
	 * @throws EdmException
	 */
	private void updateAcceptanceState(Document document, String acceptanceCn) throws EdmException
	{
		FieldsManager fm = document.getFieldsManager();
		fm.initialize();
		Map<String, Object> values = new HashMap<String, Object>();
		
		LoggerFactory.getLogger("kamilj").debug(acceptanceCn);
		
		if (AC_FORMALNO_PRAWNA.equals(acceptanceCn))
			values.put(ACCEPTANCE_STATE, 10);
		else if (STATE_NADANO_PRAWNA.equals(acceptanceCn))
			values.put(ACCEPTANCE_STATE, 15);
		else if (AC_MERYTORYCZNA.equals(acceptanceCn))
			values.put(ACCEPTANCE_STATE, 20);
		else if (AC_ADMINISTRATOR_ST.equals(acceptanceCn))
			values.put(ACCEPTANCE_STATE, 30);
		else if (AC_SZEFA_CENTRUM.equals(acceptanceCn))
			values.put(ACCEPTANCE_STATE, 40);
		else if (AC_KSIEGOWA.equals(acceptanceCn))
			values.put(ACCEPTANCE_STATE, 50);
		
		LoggerFactory.getLogger("kamilj").debug(values.get(ACCEPTANCE_STATE).toString());
		
		fm.getDocumentKind().setOnly(fm.getDocumentId(), values);
	}
	
	/**
	 * Wygenerowanie pliku CSV
	 */
    public File generateCsvDocumentView(Document document) throws EdmException 
    {
    	File csv = null;
    	UniversalTableDumper dumper = null;
        try
        {
        	dumper = new UniversalTableDumper();
        	csv = File.createTempFile("docusafe_tmp_", ".csv");
    		CsvDumper csvDumper = new CsvDumper();
    		csvDumper.openFile(csv);
    		dumper.addDumper(csvDumper);
        	generateCsvDump(dumper,document);

            return csv;
        }
        catch (Exception e)
        {
        	log.error("",e);
            if (csv != null)
            	csv.delete();
            throw new EdmException("B��d podczas generowania dokumentu CSV", e);
        }
    }
    
    /**
     * Dla podanego dokumentu generowany jest jego obraz w postaci PDF.
     *
     * @return wygenerowany plik PDF
     */
    public File generateDocumentView(Document document) throws EdmException
    {
        File pdf = null;
        try
        {
            pdf = File.createTempFile("docusafe_tmp_", ".pdf");
            OutputStream os = new BufferedOutputStream(new FileOutputStream(pdf));

            PdfUtils.attachmentsAsPdf(new Long[] {document.getId()},
                    /*uwagi*/false, os,
                    /*wstep*/createDocumentSummary(document),
                    /*keywords*/null,
                    /*zako�czenie*/null,
                    /*pomini�te na pocz�tku*/false);
            os.close();

            return pdf;
        }
        catch (Exception e)
        {
        	log.error("",e);
            if (pdf != null)
                pdf.delete();
            throw new EdmException("B��d podczas generowania obrazu dokumentu", e);
        }
    }
    
    /**
     * Wygenerowanie zrzutu do CSV
     * @param dumper
     * @param document
     * @throws IOException
     * @throws EdmException
     */
    private void generateCsvDump(UniversalTableDumper dumper, Document document) throws IOException, EdmException {
    	 StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

         DocumentKind documentKind = document.getDocumentKind();
         FieldsManager fm = documentKind.getFieldsManager(document.getId());
         fm.initialize();
         fm.initializeAcceptances();

         OfficeDocument officeDocument = null;
         if (document instanceof OfficeDocument)
             officeDocument = (OfficeDocument) document;
         
    	dumper.newLine();
    	dumper.addText("FAKTURA KOSZTOWA PTE");
    	dumper.dumpLine();
    	dumper.newLine();
    	
    	dumper.newLine();
    	if (officeDocument != null && officeDocument.getOfficeNumber() != null)
    		dumper.addText(sm.getString("NumerKancelaryjny")+": "+officeDocument.getOfficeNumber());
        else
        	dumper.addText(sm.getString("Identyfikator")+": "+document.getId());
    	dumper.dumpLine();
    	
    	dumpText("", dumper);
    	dumpText(sm.getString("DokumentBiznesowy"), dumper);
        
    	dumper.newLine();
    	if (officeDocument != null && officeDocument.getType() == DocumentType.INCOMING)
        	dumper.addText(sm.getString("DataPrzyjecia")+": "+DateUtils.formatCommonDate(((InOfficeDocument) officeDocument).getIncomingDate()));
        else
        	dumper.addText(sm.getString("DataPrzyjecia")+": "+DateUtils.formatCommonDate(document.getCtime()));
    	dumper.dumpLine();
    	
        Boolean dot_sr_trwalych = (Boolean)fm.getKey(DOTYCZY_SRODKOW_TRWALYCH);
        if (dot_sr_trwalych!=null && dot_sr_trwalych) {
        	dumpParagraph(fm, DOTYCZY_SRODKOW_TRWALYCH, dumper);
        	dumpParagraph(fm, SRODKI_TRWALE, dumper);
        }
    	
    	// ATRYBUTY BIZNESOWE
        dumpParagraph(fm, DATA_WYSTAWIENIA_FIELD_CN, dumper);
        dumpParagraph(fm, DATA_PLATNOSCI_FIELD_CN, dumper);
        dumpParagraph(fm, DATA_ZAPLATY_FIELD_CN, dumper);
        dumpParagraph(fm, DATA_ZAKSIEGOWANIA_FIELD_CN, dumper);
        dumpParagraph(fm, NUMER_FAKTURY_FIELD_CN, dumper);
        dumpParagraph(fm, DOSTAWCA_FIELD_CN, dumper);
        dumpParagraph(fm, KWOTA_BRUTTO_FIELD_CN, dumper);
        // NUMER KONTA BANKOWEGO
        dumpParagraph(fm, NUMER_RACHUNKU_BANKOWEGO_CN, dumper);

        // OPIS TOWARU
        dumpParagraph(fm, OPIS_TOWARU_FIELD_CN, dumper);
        
        // AKCEPTACJE OG�LNE
        dumpText("", dumper);
        dumpText("Akceptacje og�lne", dumper);
        dumpText("", dumper);
        
        for (DocumentAcceptance documentAcceptance : fm.getAcceptancesState().getGeneralAcceptances()) {
        	dumpText(documentAcceptance.getDescription(), dumper);
        }
        dumpText("", dumper);

        // AKCEPTACJE CENTRUM KOSZTOWEGO
        dumpText("Akceptacje centrum kosztowego", dumper);
        dumpText("", dumper);
        // tworzenie nag��wka tabeli
        dumper.newLine();
        dumper.addText("MPK (Kwota)");
        for (Acceptance acceptance : fm.getAcceptancesDefinition().getFieldAcceptances().values())
        {
        	dumper.addText(acceptance.getName());
        }
        dumper.dumpLine();
        // tworzenie tre�ci tabeli
        String numerKontaTmp = "";
        List<CentrumKosztowDlaFaktury> list = (List<CentrumKosztowDlaFaktury>) fm.getValue(fm.getAcceptancesDefinition().getCentrumKosztFieldCn());
        for (CentrumKosztowDlaFaktury centrum : list)
        {
        	dumper.newLine();
            CentrumKosztow ck = CentrumKosztow.find(centrum.getCentrumId());

            if(centrum.getAccountNumber() != null)
            {
                AccountNumber an = AccountNumber.findByNumber(centrum.getAccountNumber());
                numerKontaTmp = an.getNumber()+" "+an.getName();
            }
            else
                numerKontaTmp = "(brak konta)";
            dumper.addText(ck.getSymbol()+" "+ck.getName()+" | "+numerKontaTmp+" | Kwota "+centrum.getAmount());
            for (String acceptanceCn : fm.getAcceptancesDefinition().getFieldAcceptances().keySet())
            {
                DocumentAcceptance documentAcceptance = fm.getAcceptancesState().getFieldAcceptance(acceptanceCn, centrum.getId());
                dumper.addText(documentAcceptance != null ? documentAcceptance.getDescriptionWithoutCn() : "");
            }
            dumper.dumpLine();
        }
        dumpText("", dumper);

        // UWAGI
        if (officeDocument != null)
        {
            List lstRemarks = officeDocument.getRemarks();
            List<String> remarks = new ArrayList<String>();
            boolean done = false;
            for (Iterator iter=lstRemarks.iterator(); iter.hasNext(); )
            {
                if (!done) {
                    dumpText(sm.getString("Uwagi")+" :", dumper);
                    done = true;
                }
                Remark remark = (Remark) iter.next();
                remarks.add(" - "+remark.getContent()+" ("+DSUser.safeToFirstnameLastname(remark.getAuthor())+" "+DateUtils.formatJsDateTime(remark.getCtime())+")");
            }
            for(String r : remarks) {
            	dumpText(r, dumper);            	
            }
        }
    }
    
    /**
     * Generuje elementu dokumentu PDF opisuj�cy g��wne informacje danego dokumentu.
     * @throws Exception 
     */
    @SuppressWarnings("unchecked")
    private Element createDocumentSummary(Document document) throws Exception
    {
        StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);

        DocumentKind documentKind = document.getDocumentKind();
        FieldsManager fm = documentKind.getFieldsManager(document.getId());
        fm.initialize();
        fm.initializeAcceptances();

        OfficeDocument officeDocument = null;
        if (document instanceof OfficeDocument)
            officeDocument = (OfficeDocument) document;

        try
        {
            File fontDir = new File(Docusafe.getHome(), "fonts");
            File arial = new File(fontDir, "arial.ttf");
            File arialBold = new File(fontDir, "arialbd.ttf");
            BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont boldBaseFont = BaseFont.createFont(arialBold.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font font = new Font(baseFont, 10);
            Font boldFont = new Font(boldBaseFont, 10);

            // PODSTAWOWE DANE
            Paragraph paragraph = new Paragraph("AEGON Powszechne Towarzystwo Emerytalne S.A.\n", boldFont);
            paragraph.add(new Chunk("RAPORT DEKRETACJI #KO ", font));
            paragraph.add(new Chunk((officeDocument.getOfficeNumber()!=null?officeDocument.getOfficeNumber():" ")+" / ", boldFont));
            paragraph.add(new Chunk(sm.getString("#ID "), font));
            paragraph.add(new Chunk(document.getId()+"", boldFont));
            paragraph.setAlignment(Element.ALIGN_CENTER);
            paragraph.setSpacingAfter(12);
            
            Chapter beginning = new Chapter(paragraph, 2);
            beginning.setNumberDepth(0);
            
            float[] tWidths = new float[] { 1, 3};

            Table table = new Table(tWidths.length);
            table.setWidths(tWidths);
            table.setBorder(Table.NO_BORDER);
            table.setAlignment(Table.ALIGN_LEFT);
            table.setWidth(80);
            table.setCellsFitPage(true);
            table.setPadding(0);
            
            // data przyj�cia
            addCell(sm.getString("DataPrzyjecia")+":", table, font);
            if (officeDocument != null && officeDocument.getType() == DocumentType.INCOMING)
                addCell(DateUtils.formatCommonDate(((InOfficeDocument) officeDocument).getIncomingDate()), table, font);
            else
                addCell(DateUtils.formatCommonDate(document.getCtime()), table, font);            
            // kategoria
            addCell(sm.getString("Kategoria")+":", table, font);
            addCell((String) fm.getValue("KATEGORIA"), table, font);
            //addCell(sm.getString(DocumentKind.INVOICE_PTE), table, font);
            // numer dokumentu
            addCell(sm.getString("NumerDokumentu")+":", table, boldFont);
            addCell(getFieldDesc(fm, NUMER_FAKTURY_FIELD_CN), table, boldFont);
            // data wystawienia
            addCell(sm.getString("DataWystawienia")+":", table, font);
            addCell(getFieldDesc(fm, DATA_WYSTAWIENIA_FIELD_CN), table, font);
            // wystawca
            addCell(sm.getString("Wystawca")+":", table, font);
            addCell(getFieldDesc(fm, DOSTAWCA_FIELD_CN), table, font);
            // zdogno��
            addCell(sm.getString("Zgodnosc")+":", table, font);
            addCell(getFieldDesc(fm, POTWIERDZENIE_ZGODNOSCI_FIELD_CN), table, font);
            // odst�p
            addCell("\n", table, font); addCell("\n", table, font);
            // data zaksi�gowania
            addCell(sm.getString("DataZaksiegowania")+":", table, font);
            addCell(getFieldDesc(fm, DATA_ZAKSIEGOWANIA_FIELD_CN), table, font);
            // data p�atno�ci
            addCell(sm.getString("DataPlatnosci")+":", table, font);
            addCell(getFieldDesc(fm, DATA_PLATNOSCI_FIELD_CN), table, font);
            // rzeczowy �r tr
            addCell(sm.getString("RzeczowyST")+":", table, font);
            addCell(getFieldDesc(fm, DOTYCZY_SRODKOW_TRWALYCH).toUpperCase(), table, font);
            // kwota brutto
            addCell(sm.getString("KwotaBrutto")+":", table, boldFont);
            String currency = getFieldDesc(fm, CURRENCY_FIELD_CN);
            
            Object description = fm.getDescription(KWOTA_BRUTTO_FIELD_CN);
            String kwotaBr = "";
            if (description != null)
            	kwotaBr = NumberFormat.getInstance(new Locale("pl", "PL")).format(description).replace(',', '.');
            addCell(kwotaBr + (!currency.equals("")?" ("+currency.substring(0, 3)+")":""), table, boldFont);
            // odst�p
            addCell("\n", table, font); addCell("\n", table, font);
            // opis us�ugi towaru
            addCell(sm.getString("OpisUslugiTowaru")+":", table, font);
            addCell(getFieldDesc(fm, OPIS_TOWARU_FIELD_CN), table, font);
            // odst�p
            addCell("\n", table, font); addCell("\n", table, font);
            beginning.add(table);
            
            // NUMERACJA �T
            Boolean dot_sr_trwalych = (Boolean)fm.getKey(DOTYCZY_SRODKOW_TRWALYCH);
            if (dot_sr_trwalych != null && dot_sr_trwalych) {
            	beginning.add(createParagraph("Numeracja �T", Cell.ALIGN_LEFT, font));
            	
				  float[] stWitdths = new float[] { 2, 3, 3, 2};
				  Table stTable = new Table(stWitdths.length);
				  stTable.setWidths(stWitdths);
				  stTable.setDefaultVerticalAlignment(Table.ALIGN_TOP);
				  stTable.setAlignment(Table.ALIGN_LEFT);
				  stTable.setWidth(100);
				  stTable.setCellsFitPage(true);
				  stTable.setPadding(2);
				  
				  addCell(sm.getString("NrMetki"), stTable, font, true);
				  addCell(sm.getString("OpisST"), stTable, font, true);
				  addCell(sm.getString("Lokalizacja"), stTable, font, true);
				  addCell(sm.getString("NumerSeryjny"), stTable, font, true);
				  List<String> ids = (ArrayList<String>)fm.getValue(SRODKI_TRWALE);
				  for (String id : ids) {
					  if (!StringUtils.isEmpty(id)) {
						  Durable d = DurablesFactory.getInstance().find(Long.valueOf(id));
						  addCell(d.getInventoryNumber(), stTable, font, true);
				          addCell(d.getInfo(), stTable, font, true);
				          addCell(CentrumKosztow.find(d.getLocation()).getName(), stTable, font, true);
				          addCell(d.getSerialNumber(), stTable, font, true);
					  }
				  }
                  
                  beginning.add(stTable);
            }


            // AKCEPTACJE OG�LNE
            beginning.add(createParagraph("\nAkceptacje og�lne\n", Element.ALIGN_LEFT, font));
            float[] generalWidths = new float[] { 2, 4};
			Table generalTable = new Table(generalWidths.length);
			generalTable.setWidths(generalWidths);
			generalTable.setWidth(100);
			generalTable.setPadding(2);
            
			addCell(sm.getString("RodzajAkceptacji"), generalTable, font, true);
			addCell(sm.getString("Akceptujacy"), generalTable, font, true);
			
            for (DocumentAcceptance documentAcceptance : fm.getAcceptancesState().getGeneralAcceptances()) {
            	addCell(getAcceptanceName(documentAcceptance.getAcceptanceCn()), generalTable, font, true);
            	addCell(documentAcceptance.getDescription(), generalTable, font, true);
            }
            
            beginning.add(generalTable);
            
            // AKCEPTACJE CENTRUM KOSZTOWEGO
            paragraph = createParagraph("\nAkceptacje centrum kosztowego\n", Element.ALIGN_LEFT, font);
            beginning.add(paragraph);

            float[] ckWidths = new float[] {2, 1, 3};
            Table ckTable = new Table(ckWidths.length);
            ckTable.setWidths(ckWidths);
            ckTable.setWidth(100);
            ckTable.setPadding(2);

            // tworzenie nag��wka tabeli
            ckTable.addCell(new Phrase("MPK (Kwota)", font));
            ckTable.addCell(new Phrase("Kwota", font));
            ckTable.addCell(new Phrase(sm.getString("Akceptujacy"), font));

            // tworzenie tre�ci tabeli
            String numerKontaTmp = "";
            List<CentrumKosztowDlaFaktury> list = (List<CentrumKosztowDlaFaktury>) fm.getValue(fm.getAcceptancesDefinition().getCentrumKosztFieldCn());
            for (CentrumKosztowDlaFaktury centrum : list)
            {
                CentrumKosztow ck = CentrumKosztow.find(centrum.getCentrumId());

                if(centrum.getAccountNumber() != null)
                {
                    AccountNumber an = AccountNumber.findByNumber(centrum.getAccountNumber());
                    numerKontaTmp = an.getNumber()+" "+an.getName();
                }
                else
                    numerKontaTmp = "(brak konta)";
                
                addCell(ck.getSymbol()+" "+ck.getName()+"\n"+numerKontaTmp, ckTable, font, true);
                String amount = "";
                if (centrum.getAmount() != null)
                	amount = NumberFormat.getInstance(new Locale("pl", "PL")).format(centrum.getAmount()).replace(',', '.');
                addCell(amount, ckTable, font, true);
                
                for (String acceptanceCn : fm.getAcceptancesDefinition().getFieldAcceptances().keySet())
                {
                    DocumentAcceptance documentAcceptance = fm.getAcceptancesState().getFieldAcceptance(acceptanceCn, centrum.getId());
                    addCell(documentAcceptance != null ? documentAcceptance.getDescriptionWithoutCn() : "", ckTable, font, true);
                }
            }

            beginning.add(ckTable);
            paragraph = createParagraph("\nDEKRET KSI�GOWY (Data: _ _ - _ _ - _ _ _ _   Podpis:.........................)\n", Element.ALIGN_LEFT, font);
            beginning.add(paragraph);
            
            float[] dkWitdths = new float[] { 4, 2, 2};
			Table dkTable = new Table(dkWitdths.length);
			dkTable.setWidths(dkWitdths);
			dkTable.setDefaultVerticalAlignment(Table.ALIGN_TOP);
			dkTable.setAlignment(Table.ALIGN_LEFT);
			dkTable.setWidth(100);
			dkTable.setCellsFitPage(true);
			dkTable.setPadding(2);
			  
			  addCell(sm.getString("KontoKsiegowe"), dkTable, font, true);
			  addCell(sm.getString("DebetPLN"), dkTable, font, true);
			  addCell(sm.getString("KredytPLN"), dkTable, font, true);
			  for(int i =0; i<4;i++) {
				  addCell("1  -  _ _ _ _ _ _ _  -  _ _  -  _ _ _  -  _  -  _  -  _ _  -  _ _ _\n ", dkTable, font, true);
				  addCell("\n", dkTable, font, true);
				  addCell("\n", dkTable, font, true);
			  }
            
            beginning.add(dkTable);
            
            return beginning;
        }
        catch (DocumentException e)
        {
        }
        catch (IOException e)
        {
        }
        return null;
    }
    
    private String getAcceptanceName(String cn)
    {
    	if(cn.equals("merytoryczna"))
    		return "Merytoryczna";
    	else if(cn.equals("prawna"))
    		return "Formalno-prawna";
    	else if(cn.equals("szefa_centrum"))
    		return "Szefa centrum";
    	else if(cn.equals("ksiegowa"))
    		return "Kontrolingowa";
    	else if(cn.equals("admin_st"))
    		return "Administratora ST";

    	return "brak";
    }
    
	private static void addCell(String name, Table table, Font font, boolean border) throws Exception {
		  Cell cell = new Cell(new Phrase((name!=null?name:""), font));
		  cell.setHorizontalAlignment(Cell.ALIGN_LEFT);
		  cell.setVerticalAlignment(Cell.ALIGN_MIDDLE);
		  if (!border) cell.setBorder(Cell.NO_BORDER);
	      table.addCell(cell);
	}
	
	private static void addCell(String name, Table table, Font font) throws Exception {
		  addCell(name, table, font, false);
	}
    
    private void dumpParagraph(FieldsManager fm, String fieldCn, UniversalTableDumper dumper) throws EdmException, IOException {
    	Field field = fm.getField(fieldCn);
        Object description = fm.getDescription(fieldCn);
        dumper.newLine();
        dumper.addText(field.getName()+": "+(description != null ? description.toString() : ""));
        dumper.dumpLine();
    }
    
    private void dumpText(String text, UniversalTableDumper dumper) throws IOException {
        dumper.newLine();
        dumper.addText(text);
        dumper.dumpLine();
    }
    
    /**
     * Tworzy paragraf dla dokumentu PDF opisuj�cy warto�� danego pola z dockinda.
     */
    private Paragraph createParagraph(FieldsManager fm, String fieldCn, Font font) throws EdmException
    {
        Field field = fm.getField(fieldCn);
        Object description = fm.getDescription(fieldCn);
        return new Paragraph(field.getName()+": "+(description != null ? description.toString() : ""), font);
    }
    
    /**
     * Tworzy paragraf dla dokumentu PDF opisuj�cy warto�� danego pola z dockinda.
     */
    private String getFieldDesc(FieldsManager fm, String fieldCn) throws EdmException
    {
        Object description = fm.getDescription(fieldCn);
        return description != null ? description.toString() : "";
    }

    /**
     * Tworzy paragraf zawieraj�cy dany tekst.
     */
    private Paragraph createParagraph(String text, int alignment, Font font) throws EdmException
    {
        Paragraph paragraph = new Paragraph(text, font);
        paragraph.setAlignment(alignment);
        return paragraph;
    }
    
    public boolean canGenerateCsvView() 
    {
    	return true;
    }
    
    public void onSendDecretation(Document document,AcceptanceCondition condition) throws EdmException 
    {
    	//nic nie robimy
    }
    
    @Override
    public Map<Long, String> getAvailableArchiveBoxes(FieldsManager fm)
    		throws EdmException
    {
    	DSApi.open(AuthUtil.getSubject(ServletActionContext.getRequest()));
    	
    	Map<Long, String> boxes = new LinkedHashMap<Long, String>();
    	boxes.put(0L, "wybierz pud�o archiwalne ...");
    	
    	String boxLine = (String) fm.getDocumentKind().getDockindInfo()
    		.getProperties().get("box-line");
    	
    	if (boxLine == null || boxLine.equals(""))
    		return boxes;
    	
    	@SuppressWarnings("unchecked")
    	List<Object[]> res = DSApi.context().session().createQuery(
    			"select b.id, b.name from " + Box.class.getName() + " b where b.line = :line")
    			.setParameter("line", boxLine)
    		.list();
    	
    	for (Object[] row : res)
    		boxes.put((Long) row[0], row[1].toString());
    	
    	DSApi.close();
    	
    	return boxes;
    }
}
