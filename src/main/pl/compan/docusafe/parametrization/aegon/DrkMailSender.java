package pl.compan.docusafe.parametrization.aegon;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Timer;
import java.util.TimerTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.nationwide.DrkMailThread;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;

public class DrkMailSender extends ServiceDriver  implements Service
{
	private static final Logger log = LoggerFactory.getLogger(DrkMailSender.class);
	
	private Timer timer;

	protected boolean canStop() {
		return false;
	}

	protected void start() throws ServiceException 
	{

		timer = new Timer(true);
        timer.schedule(new RunDrkThreads(), 1000, 1*DateUtils.MINUTE);		
	}

	protected void stop() throws ServiceException 
	{
	}

	class RunDrkThreads extends TimerTask
	{
		
		public void run() 
		{
			PreparedStatement ps = null;
			String pss = "update dso_drk_mail_sender set mailed = ? where id in ( ";
			try
			{
				DSApi.openAdmin();
				
				ps = DSApi.context().prepareStatement("select * from dso_drk_mail_sender where MAILED = ?");
				ps.setInt(1, 0);
				ResultSet rs = ps.executeQuery();
				while(rs.next())
				{
					pss += rs.getLong("ID")+" , ";
					
					Thread thread = new Thread(new DrkMailThread(rs.getLong("ID"), rs.getLong("FLAG_ID"), rs.getLong("DOCUMENT_ID"), rs.getString("NR_POLISY")));
		        	thread.start();
		        	
				}
				DSApi.context().closeStatement(ps);
				ps = null;
				pss += " 0 )";
				
				DSApi.context().begin();
				
				ps = DSApi.context().prepareStatement(pss);
				ps.setLong(1, 1);
				ps.execute();
				DSApi.context().closeStatement(ps);
				
				DSApi.context().commit();
			}
			catch (Exception e) 
			{
				log.error("",e);
			}
			finally
			{
				DSApi.context().closeStatement(ps);
				DSApi._close();
			}
			
		}
	}
	
}
