package pl.compan.docusafe.parametrization.aegon;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.agent.AgentManager;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.ws.client.absences.DocumentExchangeServiceStub;
import pl.compan.docusafe.ws.client.absences.DocumentExchangeServiceStub.DocumentExAcceptances;
import pl.compan.docusafe.ws.client.absences.DocumentExchangeServiceStub.DocumentExAttribute;
import pl.compan.docusafe.ws.client.absences.DocumentExchangeServiceStub.DocumentExRequest;
import pl.compan.docusafe.ws.client.absences.DocumentExchangeServiceStub.DocumentExResponse;
import pl.compan.docusafe.ws.client.absences.DocumentExchangeServiceStub.Upload;
import pl.compan.docusafe.ws.client.absences.DocumentExchangeServiceStub.UploadResponse;

public class AbsenceAgent extends AegonAgent 
{
	
	private static final Log log = AgentManager.log;
	private Timer timer;
	
	protected void setupAgent() 
	{
		//log.trace("SetupAbsenceAgent");
	}

	public boolean agentAllowed()
	{
		return true;
	}
	
	public String getAgentCode() 
	{
		return "absenceagent";
	}

	
	public String getPropertyPrefix() 
	{
		return String.valueOf("absence.");
	}

	
	public void run() 
	{
		//LoggerFactory.getLogger("tomekl").debug("run()");
		//LoggerFactory.getLogger("tomekl").debug("ABSENCE Agent sprawdza liste zadan");
		log.debug("ABSENCE Agent sprawdza liste zadan");
        TaskSnapshot.Query query;
        query = new TaskSnapshot.Query();
        TaskSnapshot task =null;
        try
        {
               DSApi.openAdmin();
               query.setUser(DSUser.findByUsername(agentName));
               SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
               //LoggerFactory.getLogger("tomekl").debug("ABSENCE Na liscie Agenta znajduje sie "+ results.count()+ " zadan");
               log.debug("ABSENCE Na liscie Agenta znajduje sie "+ results.count()+ " zadan");
               setupAgent();
               while (results.hasNext())
               {
                   task = results.next();
                   task.toFullTask(true);                    
                   doOne(task);                                                                               
               }                
        }          
        catch (Exception e)
        {
        	log.error("ABSENCE "+e.getMessage(),e);
        }
        finally
        {
           	DSApi._close();
        }

	}
	
	private void doOne(TaskSnapshot task)
	{
		OfficeDocument doc = null;
		
		try
		{
			DSApi.context().begin();
			doc = OfficeDocument.find(task.getDocumentId());
                        String docKind = doc.getDocumentKind().getCn();
                        
			if(task.getDocumentType().equals(OutOfficeDocument.INTERNAL_TYPE) && 
                                (DocumentLogicLoader.ABSENCE_REQUEST.equals(docKind) || DocumentKind.OVERTIME_APP.equals(docKind) || DocumentKind.OVERTIME_RECEIVE.equals(docKind)))
			{
				log.trace("DocId : "+doc.getId());
				
				if(task.getObjective().startsWith("ext:"))
				{
					log.trace("Pidmo na webservice : "+task.getObjective()+" "+doc.getId());
					log.trace(Docusafe.getAdditionProperty("DocumentExchangeStub"));
					DocumentExchangeServiceStub stub = new DocumentExchangeServiceStub();
					stub._getServiceClient().getOptions().setTo(new EndpointReference(Docusafe.getAdditionProperty("DocumentExchangeStub")));
					
					DocumentExRequest request = new DocumentExRequest();
					
					request.setExternalId(doc.getId());
					request.setTargetUser(task.getObjective().replace("ext:", ""));
					
					//zalacznik
					if(doc.getAttachments().size() > 0)
					{
						Attachment at =  doc.getAttachments().get(0);
						InputStream is = at.getMostRecentRevision().getBinaryStream();
						ByteArrayOutputStream os = new ByteArrayOutputStream();
						IOUtils.copy(is, os);
						IOUtils.closeQuietly(is);
						IOUtils.closeQuietly(os);
						request.setImageArray(Base64.encodeBase64String(os.toByteArray()));
						request.setImageName(at.getMostRecentRevision().getOriginalFilename());
					}
					else
					{
						request.setImageArray(null);
						request.setImageName("noneed");						
					}
					//zalacznik:end
					
					//wartosci
					FieldsManager fm = doc.getDocumentKind().getFieldsManager(doc.getId());
					List<DocumentExAttribute> attributes = new ArrayList<DocumentExAttribute>();
					for(Field field : doc.getDocumentKind().getFields())
					{
						if(field.getCn() == null)
							continue;
						
						DocumentExAttribute attribute = new DocumentExAttribute();
						attribute.setAttributeCn(field.getCn());
						if(field.getType().equals("date"))
						{
							Date date = (Date) fm.getKey(field.getCn());
							attribute.setAttributeValue(DateUtils.formatJsDate(date));							
						}
						else
						{
							attribute.setAttributeValue(fm.getKey(field.getCn()) != null ? fm.getKey(field.getCn()).toString() : null);
						}
						
						attributes.add(attribute);
					}
					
					request.setAtributes(attributes.toArray(new DocumentExAttribute[attributes.size()]));
					//wartosci:end
					
					//akceptacje
					List<DocumentExAcceptances> acceptances = new ArrayList<DocumentExchangeServiceStub.DocumentExAcceptances>();
					
					PreparedStatement ps = null;
			    	ResultSet rs = null;
			    	try
			    	{
			    		ps = DSApi.context().prepareStatement("select acceptanceTime, username, acceptanceCn from DS_DOCUMENT_ACCEPTANCE where document_id = ?");
			    		ps.setLong(1, doc.getId());
			    		rs = ps.executeQuery();
						while(rs.next())
						{
							DocumentExAcceptances ac = new DocumentExAcceptances();
							ac.setAcceptanceCn(rs.getString("acceptanceCn"));
							ac.setUserName(rs.getString("username"));
							Calendar acTime = Calendar.getInstance();
							acTime.setTimeInMillis(rs.getTimestamp("acceptanceTime").getTime());
							ac.setAcceptanceTime(acTime);
							acceptances.add(ac);
						}
			    	}
			    	catch (Exception e) 
			    	{
			    		log.debug(e.getMessage(),e);
						e.printStackTrace();
					}
			    	finally
			    	{
			    		DbUtils.closeQuietly(rs);
			    		DbUtils.closeQuietly(ps);
			    	}
					
					request.setAcceptances(acceptances.toArray(new DocumentExAcceptances[acceptances.size()]));
					//akceptacje:end
					
					
					Upload upload = new Upload();
					upload.setRequest(request);
					UploadResponse uploadResponse =  stub.upload(upload);
					DocumentExResponse response = uploadResponse.get_return();
					Long documentId = response.getDocumentId();
					if(documentId < 0)
					{
						throw new EdmException(response.getError());
					}
					else
					{
						Map<String,Object> values = new HashMap<String,Object>();
						values.put("EXTERNAL_ID", documentId);
						doc.getDocumentKind().setOnly(doc.getId(), values);
						log.trace("Udalo sie zaimportowac pismo "+doc.getId()+" "+documentId);
						
						WorkflowFactory wff = WorkflowFactory.getInstance();
						wff.manualFinish(wff.keyToId(task.getActivityKey()), false);
						TaskSnapshot.updateByDocumentId(doc.getId(), doc.getStringType());
					}
				}
				else
				{
					log.trace("Pismo do uzytkownika : "+task.getObjective()+" "+doc.getId());
					String plannedAssignment = DSDivision.ROOT_GUID+"/"+task.getObjective()+"/"+WorkflowFactory.wfManualName()+"//"+"realizacja";
			        String[] activityIds = {WorkflowFactory.activityIdOfWfNameAndKey(task.getWorkflowName(), task.getActivityKey())};
			        WorkflowFactory.multiAssignment(activityIds,agentName, new String[] {plannedAssignment}, null, null);
				}
			}
			else
			{				
				log.trace("Zadanie nie dotyczy wewnetrznego wniosku urlopowego. Zwracam dekretacje");
                removeAssign(task);
			}
			DSApi.context().commit();
		
		}
		catch (Exception e) 
		{
			DSApi.context()._rollback();
			log.error(e.getMessage(),e);
		}
	}

}
