package pl.compan.docusafe.parametrization.aegon;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.EnumRefField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.dictionary.WorkerDictionary;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.EmployeeDocumentLogic;
import pl.compan.docusafe.service.imports.dsi.DSICsvParser;
import pl.compan.docusafe.service.imports.dsi.DSIImportHandler;
import pl.compan.docusafe.service.imports.dsi.DSIParser;
import pl.compan.docusafe.service.imports.dsi.ImportValidationException;
import pl.compan.docusafe.util.DateUtils;

public class EmployeeDSIHandler extends DSIImportHandler
{
	public static final String BOX_COLUMN_NAME = "BOX";
	public static final String FILE_COLUMN_NAME = "FILE";

	public static final String PRACOWNIK_NAZWISKO = EmployeeDocumentLogic.PRACOWNIK_FIELD_CN+"_NAZWISKO";
	public static final String PRACOWNIK_IMIE = EmployeeDocumentLogic.PRACOWNIK_FIELD_CN+"_IMIE";
	public static final String PRACOWNIK_NR = EmployeeDocumentLogic.PRACOWNIK_FIELD_CN+"_NR";
	public static final String PRACOWNIK_INST = EmployeeDocumentLogic.PRACOWNIK_FIELD_CN+"_INST";
	public String getDockindCn()
	{
		return DocumentLogicLoader.EMPLOYEE_KIND;
	}

	private static LinkedList<String> attributes;

	static
	{
		attributes = new LinkedList<String>();
		attributes.add(EmployeeDocumentLogic.TYP_FIELD_CN);
		attributes.add(PRACOWNIK_NAZWISKO);
		attributes.add(PRACOWNIK_IMIE);
		attributes.add(PRACOWNIK_NR);
		attributes.add(EmployeeDocumentLogic.DATA_FIELD_CN);
		attributes.add(EmployeeDocumentLogic.OPIS_FIELD_CN);
		attributes.add(PRACOWNIK_INST);
		attributes.add(BOX_COLUMN_NAME);
		attributes.add(FILE_COLUMN_NAME);
	}

	@Override
	protected boolean isStartProces()
	{
		return true;
	}
	
	public DSIParser getParser()
	{
		return new DSICsvParser(attributes,ARCHIVE_ONLY_IMPORT,BOX_COLUMN_NAME,FILE_COLUMN_NAME);
	}

	protected void prepareImport() throws Exception
	{
		DockindInfoBean di = DocumentKind.getDockindInfo(DocumentLogicLoader.EMPLOYEE_KIND);

		Integer typ = Integer.parseInt((String) values.get(EmployeeDocumentLogic.TYP_FIELD_CN));
		Field typField = di.getDockindFieldsMap().get(EmployeeDocumentLogic.TYP_FIELD_CN);
		Integer klasa = (Integer) ((EnumRefField) typField).getEnumItemDiscriminator(typ);
		values.put(EmployeeDocumentLogic.KLASA_FIELD_CN, klasa);
		values.put(EmployeeDocumentLogic.PRACOWNIK_FIELD_CN, getWorker(values).getId());
		String data = null;
		if(values.get(EmployeeDocumentLogic.DATA_FIELD_CN) != null)
			data = (String) values.get(EmployeeDocumentLogic.DATA_FIELD_CN);
		else
			data = DateUtils.formatJsDateOrEmptyString(new Date());

		if(data != null && data.length() > 0 )
		{
			values.put(EmployeeDocumentLogic.DATA_FIELD_CN, data.replaceAll("\\.", "-"));
		}
		values.remove(PRACOWNIK_NAZWISKO);
		values.remove(PRACOWNIK_IMIE);
		values.remove(PRACOWNIK_NR);
		values.remove(PRACOWNIK_INST);
		values.remove(FILE_COLUMN_NAME);
		values.remove(BOX_COLUMN_NAME);
	}

	private WorkerDictionary getWorker(Map<String, Object> values) throws EdmException, ImportValidationException
	{
		String nazwisko = (String) values.get(PRACOWNIK_NAZWISKO);
		String imie = (String) values.get(PRACOWNIK_IMIE);
		String idPracownika = (String) values.get(PRACOWNIK_NR);
		String instytucja = (String) values.get(PRACOWNIK_INST);
		if(idPracownika == null || idPracownika.length() < 1)
			throw new ImportValidationException("Brak nrPracownika");
		if(nazwisko == null || nazwisko.length() < 1)
			throw new ImportValidationException("Brak nazwiska pracownika");
		if(imie == null || imie.length() < 1)
			throw new ImportValidationException("Brak imienia pracownika");
		if(instytucja == null || instytucja.length() < 1)
			throw new ImportValidationException("Brak instytucji pracownika");
		WorkerDictionary worker = WorkerDictionary.getWorkerByIDpracownika(idPracownika);
		if(worker != null)
			return worker;

		List<WorkerDictionary> workers = WorkerDictionary.findWorkerByImieNazwisko(imie, nazwisko);
		if(workers != null && workers.size() > 0)
			return workers.get(0);
		else
		{
			worker = new WorkerDictionary();
			worker.setImie(imie);
			worker.setNazwisko(nazwisko);
			worker.setIdPracownika(idPracownika);
			worker.setPracuje(true);
			worker.setInstytucja((instytucja.equals("PTE") ? WorkerDictionary.PTE : WorkerDictionary.LIFE));
			worker.create();
		}
		return worker;
	}
}
