package pl.compan.docusafe.parametrization.aegon;

import java.util.Date;
import java.util.List;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.absences.EmployeeAbsenceEntry;
import pl.compan.docusafe.core.base.absences.FreeDay;

public abstract class AbstractAbsenceRequestLogic
{
	public static AbstractAbsenceRequestLogic getNewInstance()
	{
		if (AvailabilityManager.isAvailable("aegon.docusafe.remote"))
		{
			return new RemoteAbsenceRequestLogic();
		}
		
		return new LocalAbsenceRequestLogic();
	}
	
	public abstract boolean hasAbsenceBetween(EmployeeCard empCard,Date startDate,Date endDate) throws EdmException;
	
	public abstract Long getAbsenceSumDaysFromCurrentYear(EmployeeCard empCard, String type) throws EdmException;
	
	public abstract EmployeeCard getActiveEmployeeCardByUserName(String userName) throws EdmException;
	
	public abstract void unchargeLastAbsences(EmployeeCard empCard, Long documentId) throws EdmException;
	
	public abstract void chargeAbsence(EmployeeCard empCard, Date from, Date to, String type,String info,int numDays, Long documentId) throws EdmException;
	
	public abstract List<EmployeeAbsenceEntry> getEmployeeAbsences(EmployeeCard empCard, int currentYear) throws EdmException;
	
	public abstract int getAvailableDaysForCurrentYear(EmployeeCard empCard) throws EdmException;
	
	public abstract List<FreeDay> getFreeDays() throws EdmException;
}
