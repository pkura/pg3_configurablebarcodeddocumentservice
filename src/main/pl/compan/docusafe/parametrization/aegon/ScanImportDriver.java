package pl.compan.docusafe.parametrization.aegon;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AccessLog;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Doctype;
import pl.compan.docusafe.core.base.DocumentChangelog;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.InOfficeDocumentStatus;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.NameValue;
import pl.compan.docusafe.core.office.workflow.WfProcess;
import pl.compan.docusafe.core.office.workflow.WfProcessMgr;
import pl.compan.docusafe.core.office.workflow.WfService;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowServiceFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowUtils;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.mail.MailerDriver;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
/* User: Administrator, Date: 2005-11-18 10:23:27 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ScanImportDriver.java,v 1.17 2009/02/28 09:30:58 wkuty Exp $
 */
public class ScanImportDriver extends ServiceDriver implements Service
{
    private final static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(MailerDriver.class.getPackage().getName(),null);
    
    private Timer timer;
    /**
     * Tryb testowy - silnik uruchamiany jest zaraz po starcie aplikacji.
     */
    private static boolean TEST = false;
    private final static String CONFIG_FILE = "import.config";
    /**
     * Plik musi mie� czas modyfikacji co najmniej tyle milisekund
     * wstecz, aby by� uwzgl�dniony w imporcie.
     */
    private final static long TIME_THRESHOLD = 60 * 1000;

    private Property[] properties;

    private File directory;
    private String guid;

    public ScanImportDriver()
    {
        properties = new Property[] {
            new DirectoryProperty(),
            new GuidProperty()
        };

        TEST = Boolean.valueOf(Docusafe.getConfigProperty("nw.scan.import.test", "false")).booleanValue();
    }

    public Property[] getProperties()
    {
        return properties;
    }

    protected void start() throws ServiceException
    {
        // us�uga dzia�a wy��cznie dla Nationwide
        if (!Configuration.hasExtra("nationwide"))
            return;

        timer = new Timer(true);
        timer.scheduleAtFixedRate(new Import(),
            TEST ? 10*1000 : 60*1000,
            TEST ? 500 : 5*60*1000);
    }

    protected void stop() throws ServiceException
    {
        if (timer != null)
            timer.cancel();
    }

    protected boolean canStop()
    {
        return false;
    }

    public boolean hasConsole()
    {
        return true;
    }

    private class Import extends TimerTask
    {
        private boolean running = false;

        public void run()
        {
            synchronized (this)
            {
                if (running)
                {
                    console(Console.INFO, "Daa Import w trakcie - pomijanie uruchomienia");
                    return;
                }

                running = true;
            }

            try
            {
                if (directory == null || !directory.isDirectory())
                {
                    console(Console.ERROR, "Nie podano poprawnej nazwy katalogu");
                    return;
                }

                List doctypeDirectories;
                try
                {
                    doctypeDirectories = readDoctypeDirectories(directory);
                }
                catch (EdmException e)
                {
                    console(Console.ERROR, e.getMessage());
                    return;
                }

                for (Iterator iter=doctypeDirectories.iterator(); iter.hasNext(); )
                {
                    DoctypeDirectory dd = (DoctypeDirectory) iter.next();
                    if (!dd.directory.isDirectory())
                    {
                        console(Console.ERROR, dd.directory.getAbsolutePath()+
                            " nie jest katalogiem, pomijanie");
                        continue;
                    }

                    importFiles(dd.directory, dd.cn);
                }
            }
            finally
            {
                synchronized (this)
                {
                    running = false;
                }
            }
        }
    }

    /**
     * Importuje pliki ze wskazanego katalogu jako dokumenty
     * o wskazanym typie.
     */
    private void importFiles(File directory, String cn)
    {
        File[] tiffs = directory.listFiles(new FilenameFilter()
        {
            public boolean accept(File dir, String name)
            {
                return name.toLowerCase().endsWith(".tif") ||
                    name.toLowerCase().endsWith(".tiff");
            }
        });

        for (int i=0; i < tiffs.length; i++)
        {
            if ((System.currentTimeMillis() - tiffs[i].lastModified()) < TIME_THRESHOLD)
            {
                console(Console.INFO, "Pomijanie pliku "+tiffs[i]+" (zbyt nowy)");
                continue;
            }

            console(Console.INFO, "DaaImport "+tiffs[i]);

            try
            {
                importFile(tiffs[i], cn);

                tiffs[i].renameTo(new File(tiffs[i]+".done"));
            }
            catch (Exception e)
            {
                console(Console.ERROR, "B��d importu pliku "+tiffs[i]+" ("+e.getMessage()+")");
            }

            // import plik�w starszych ni� minuta
            // po imporcie pliku nale�y mu zmieni� nazw�
        }
    }

    private void importFile(File file, String cn) throws EdmException
    {
        InOfficeDocument doc = new InOfficeDocument();

        //doc.setSource("fax");

        doc.setSender(new Sender());

        doc.setPackageRemarks("Dokument zaimportowany z pliku "+file.getAbsolutePath());

        doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
        doc.setCurrentAssignmentAccepted(Boolean.FALSE);
        //doc.setDivisionGuid(DSDivision.ROOT_GUID);

        // data, z jak� zostanie utworzony wpis w dzienniku
        Date entryDate;
        Long journalId;

        // sprawdzam, czy w bazie istnieje ju� dokument o danym referenceId
        try
        {
            DSApi.openAdmin();

            DSApi.context().begin();

            Calendar currentDay = Calendar.getInstance();
            currentDay.setTime(GlobalPreferences.getCurrentDay());

            entryDate = new Date(currentDay.getTime().getTime());

            Date breakOfDay = DateUtils.endOfDay(currentDay.getTime());

            // je�eli faktyczna data jest p�niejsza ni� otwarty
            // dzie�, pozostawiam dat� i ustawiam godzin� 23:59:59.999
            if (new Date().after(breakOfDay))
            {
                currentDay.setTime(breakOfDay);
            }
            else
            {
                Calendar now = Calendar.getInstance();
                currentDay.set(Calendar.HOUR_OF_DAY, now.get(Calendar.HOUR_OF_DAY));
                currentDay.set(Calendar.MINUTE, now.get(Calendar.MINUTE));
                currentDay.set(Calendar.SECOND, now.get(Calendar.SECOND));
            }

            Journal journal = Journal.getMainIncoming();

            journalId = journal.getId();

            doc.setCreatingUser(DSApi.context().getPrincipalName());

            doc.setIncomingDate(currentDay.getTime());

            doc.setAssignedDivision(DSDivision.ROOT_GUID);
            doc.setSummary("");

/*
            InOfficeDocumentKind kind = null;
            List kinds = InOfficeDocumentKind.list();
            if (kinds.size() == 0)
                throw new EdmException("Brak zdefiniowanych typ�w dokument�w");
            for (Iterator iter=kinds.iterator(); iter.hasNext(); )
            {
                InOfficeDocumentKind k = (InOfficeDocumentKind) iter.next();
                if (k.getName().indexOf("Faks") >= 0 ||
                    k.getName().indexOf("Fax") >= 0 ||
                    k.getName().indexOf("faks") >= 0 ||
                    k.getName().indexOf("fax") >= 0)
                {
                    kind = k;
                    break;
                }
            }

            if (kind != null)
                doc.setKind(kind);
            else
            {
                kind = (InOfficeDocumentKind) kinds.iterator().next();
                doc.setKind(kind);
            }
*/
            doc.setKind((InOfficeDocumentKind) InOfficeDocumentKind.list().iterator().next());

            if (log.isDebugEnabled())
                log.debug("Wybrany rodzaj pisma: "+doc.getKind().getName());

//                Calendar cal = Calendar.getInstance();
//                cal.add(Calendar.DATE, DateUtils.getBusinessDays(kind.getDays()));

//                doc.setStatus(InOfficeDocumentStatus.find(statusId));

//                doc.setDelivery(InOfficeDocumentDelivery.find(deliveryId));

//                doc.setOutgoingDelivery(OutOfficeDocumentDelivery.find(outgoingDeliveryId));

            doc.create();

            doc.setStatus(InOfficeDocumentStatus.findByCn("PRZYJETY"));

            Doctype doctype = Doctype.findByCn("nationwide");
            Doctype.Field fldSTATUS = doctype.getFieldByCn("STATUS");
            Doctype.Field fldRODZAJ = doctype.getFieldByCn("RODZAJ");

            doctype.set(doc.getId(), fldSTATUS.getId(), fldSTATUS.getEnumItemByCn("PRZYJETY").getId());
            doctype.set(doc.getId(), fldRODZAJ.getId(), fldRODZAJ.getEnumItemByCn(cn).getId());

            doc.setSummary("Typ dokumentu: "+fldRODZAJ.getEnumItemByCn(cn).getTitle());

            // Long newDocumentId = doc.getId();

            // za��czniki mo�na tworzy� dopiero po zapisaniu dokumentu,
            // inaczej w Document.onSave wyst�puje b��d zapisywania dokumentu
            // nie zwi�zanego z folderem (createAttachment zapisuje dokument
            // w bazie).
/*
                if (kind.getRequiredAttachments() != null && kind.getRequiredAttachments().size() > 0)
                {
                    for (Iterator iter=kind.getRequiredAttachments().iterator(); iter.hasNext(); )
                    {
                        InOfficeDocumentKindRequiredAttachment req = (InOfficeDocumentKindRequiredAttachment) iter.next();
                        doc.createAttachment(new Attachment(req.getTitle()));
                    }
                }
*/

            Attachment attachment = new Attachment("Skan");
            doc.createAttachment(attachment);
            attachment.createRevision(file).setOriginalFilename(file.getName());

            DSApi.context().commit();
        }
        catch (EdmException e)
        {
            DSApi.context().setRollbackOnly();
            throw e;
        }
        finally
        {
            try { DSApi.close(); } catch (Exception e) { }
        }

        log.info("Przyj�to faks jako dokumentu "+doc.getId());

        // tworzenie wpisu w dzienniku

        Integer sequenceId;
        try
        {
            sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), entryDate);
            log.info("Przyj�to faks pod numerem KO: "+sequenceId);
        }
        catch (EdmException e)
        {
            throw e;
        }

        try
        {
            DSApi.openAdmin();

            DSApi.context().begin();

            // powi�zanie dokumentu z bie��c� sesj�
            doc = InOfficeDocument.findInOfficeDocument(doc.getId());
            doc.bindToJournal(journalId, sequenceId);

            // tworzenie i uruchamianie wewn�trznego procesu workflow
            WorkflowFactory.createNewProcess(doc, false, "Domy�lnie", "ds_guid_"+guid, guid, "");

            doc.setCurrentAssignmentGuid(guid);
            doc.setDivisionGuid(guid);

            DSApi.context().commit();

            AccessLog.logPersonalDataInput(DSApi.context().getPrincipalName(),
                doc.getId(),
                Arrays.asList(new Sender[] { doc.getSender() }));
        }
        catch (EdmException e)
        {
            DSApi.context().setRollbackOnly();
            throw e;
        }
        finally
        {
            try { DSApi.close(); } catch (Exception e) { }
        }
    }

    /**
     * Odczytuje we wskazanym katalogu plik konfiguracyjny import.config
     * i zwraca list� obiekt�w {@link DoctypeDirectory}.
     */
    private List<DoctypeDirectory> readDoctypeDirectories(File directory) throws EdmException
    {
        File propertiesFile = new File(directory, CONFIG_FILE);
        if (!propertiesFile.isFile())
        {
            throw new EdmException("Nie znaleziono pliku "+propertiesFile.getAbsolutePath());
        }

        Properties config = new Properties();
        InputStream is = null;
        try
        {
            is = new FileInputStream(propertiesFile);
            config.load(is);
        }
        catch (IOException e)
        {
            throw new EdmException("B��d odczytu pliku "+propertiesFile.getAbsolutePath()+
                ": "+e.getMessage());
        }
        finally
        {
        	org.apache.commons.io.IOUtils.closeQuietly(is);
        }

        /*
            import.cn.ZLECENIE_ZMIANY_ALOKACJI = zmiany_alokacji
            import.cn.ZLECENIE_TRANSFERU = zmiany_transferu
        */

        List<DoctypeDirectory> doctypeDirectories = new LinkedList<DoctypeDirectory>();

        Enumeration e = config.propertyNames();
        while (e.hasMoreElements())
        {
            String key = (String) e.nextElement();
            if (key.startsWith("import.cn."))
            {
                String cn = key.substring("import.cn.".length());
                File dir = new File(directory, config.getProperty(key));
                doctypeDirectories.add(new DoctypeDirectory(cn, dir));
            }
        }

        return doctypeDirectories;
    }

    private static class DoctypeDirectory
    {
        private String cn;
        private File directory;

        public DoctypeDirectory(String cn, File directory)
        {
            this.cn = cn;
            this.directory = directory;
        }
    }

    class DirectoryProperty extends Property
    {
        public DirectoryProperty()
        {
            super(SIMPLE, PERSISTENT, ScanImportDriver.this, "directory", "Katalog z plikami", String.class);
        }

        protected Object getValueSpi()
        {
            return directory != null ? directory.getAbsolutePath() : null;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            if (object == null)
                return;
            File tmp = new File((String) object);
            if (!tmp.isDirectory())
                throw new ServiceException(tmp+" nie istnieje lub nie jest katalogiem");
            if (!tmp.canWrite())
                throw new ServiceException("Brak praw zapisu w katalogu "+tmp);
            directory = tmp;
        }
    }

    class GuidProperty extends Property
    {
        public GuidProperty()
        {
            super(SIMPLE, PERSISTENT, ScanImportDriver.this, "guid", "Dzia� do dekretacji", String.class, GUID);
        }

        protected Object getValueSpi()
        {
            return guid;
        }

        protected void setValueSpi(Object object) throws ServiceException
        {
            if (object == null)
                return;
            String divisionGuid = (String) object;
            try
            {
                DSDivision division = DSDivision.find(divisionGuid);
                if (!division.isDivision())
                    throw new ServiceException("Dekretacja musi odbywa� si� na dzia� (nie stanowisko lub grup�)");
                guid = division.getGuid();
            }
            catch (DivisionNotFoundException e)
            {
                throw new ServiceException("Nie znaleziono wskazanego dzia�u");
            }
            catch (EdmException e)
            {
                throw new ServiceException(e.getMessage(), e);
            }
        }
    }
}
