package pl.compan.docusafe.parametrization.aegon;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.tools.CsvDumper;
import pl.compan.docusafe.reports.tools.PDFDumper;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;

public class DurablesReport extends Report {
	private static final Logger log = LoggerFactory.getLogger(DurablesReport.class);
	/**
	 * Dumper dla raportow tabelarycznych
	 */
	protected UniversalTableDumper dumper = null;
	/**
	 * Data przyjecia dokumentu OD
	 */
	protected String dateFrom = "";
	/**
	 * Data przyjecia dokumentu DO
	 */
	protected String dateTo = "";
	

	@Override
	public void doReport() throws Exception {
//		if (this.parametersMap.get("submit_date_from") != null) {
//			dateFrom = parametersMap.get("submit_date_from").getValueAsString();
//		}
//		if (this.parametersMap.get("submit_date_to") != null) {
//			dateTo = parametersMap.get("submit_date_to").getValueAsString();
//		}

		PreparedStatement ps = null;
		try {
			initDumpers();
			dumpHeader();

			ps = DSApi.context().prepareStatement("select st.INVENTORY_NUM, st.INFO, st.SERIAL_NUM, st.LOCATION, doc.nr_faktury, doc.DOCUMENT_ID " +
					"from DS_CONSUMER_DURABLES st, DSG_INVOICE doc,dsg_invoice_multiple_value multiple " +
					"where multiple.DOCUMENT_ID=doc.DOCUMENT_ID and multiple.FIELD_CN='SRODKI_TRWALE' and cast(multiple.FIELD_VAL AS int)=st.ID order by st.CTIME");
			
			// na razie nie ustawiamy daty
//			SimpleSQLHelper sqlHelper = new SimpleSQLHelper();
//			ps.setDate(1, sqlHelper.parseDate(dateFrom,new java.util.Date()));
//			ps.setDate(2, sqlHelper.parseDate(dateTo,1,new java.util.Date()));
			
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				dumper.newLine();
				dumper.addText(rs.getString("INVENTORY_NUM")); //numer inwentaryzacyjny
				dumper.addText(rs.getString("INFO")); //opis �rodka trwa�ego
				dumper.addText(rs.getString("SERIAL_NUM")); // nr seryjny
				dumper.addText(rs.getString("LOCATION")); // lokalizacja
				dumper.addText(rs.getString("nr_faktury")); //nr faktury
				dumper.dumpLine();
			}
			rs.close();

		} catch (Exception e) {
			
		} finally {
			DSApi.context().closeStatement(ps);
			if (dumper != null) {
				dumper.closeFileQuietly();
			}
		}

	}
	
	/**
	 * Inicjowanie dumpertow
	 * @throws Exception
	 */
	protected void initDumpers() throws Exception
	{
		dumper = new UniversalTableDumper();
		
		CsvDumper csvDumper = new CsvDumper();
		File csvFile = new File(this.getDestination(), "raport_srodkow_trwalych.csv");			
		csvDumper.openFile(csvFile);
		dumper.addDumper(csvDumper);
		
		PDFDumper pdfDumper = new PDFDumper();
		File pdfFile = new File(this.getDestination(), "raport_srodkow_trwalych.pdf");	
		pdfDumper.openFile(pdfFile);
		dumper.addDumper(pdfDumper);
		
	}
	
	protected void dumpHeader() throws IOException {
		dumper.newLine();
		dumper.addText("Nr inwen.");
		dumper.addText("Opis �rodka trwa�ego.");
		dumper.addText("Nr seryjny");
		dumper.addText("Lokalizacja / u�ytkownik");
		dumper.addText("Nr faktury");
		dumper.dumpLine();
	}

}
