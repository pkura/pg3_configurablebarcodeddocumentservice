package pl.compan.docusafe.parametrization.aegon;

import static pl.compan.docusafe.util.FolderInserter.root;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.commons.dbutils.DbUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.base.absences.EmployeeAbsenceEntry;
import pl.compan.docusafe.core.base.absences.FreeDay;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.logic.AbsenceLogic;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.Acceptable;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.absences.CardServiceStub;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.GetUserInfo;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.GetUserInfoResponse;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.LoginRequest;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.LoginResponse;

/**
 * Logika wniosku urlopowego AEGON - oparty jest na akceptacjach.
 * 
 * @author <a href="mailto:kamil.jasek@docusafe.pl">Kamil Jasek</a>
 */
public class AbsenceRequestLogic extends AbstractDocumentLogic implements Acceptable, AbsenceLogic
{
	protected static final Logger LOG = LoggerFactory.getLogger(AbsenceRequestLogic.class);
	
	protected static final String KIND_CN = "KIND";
	protected static final String USER_CN = "USER";
	protected static final String POSITION_CN = "POSITION";
	protected static final String RECREATIONAL_CN = "RECREATIONAL";
	protected static final String AVAILABLE_DAYS_CN = "AVAILABLE_DAYS";
	protected static final String FROM_CN = "FROM";
	protected static final String TO_CN = "TO";
	protected static final String WORKING_DAYS_CN = "WORKING_DAYS";
	protected static final String DESCRIPTION_CN = "DESCRIPTION";
	public static final String STATUS_FIELD_CN = "STATUS";
	protected static final String AKCEPTACJA_FINALNA_FIELD_CN = "AKCEPTACJA_FINALNA";
	protected static final String ABSENCE_CHARGED_CN = "ABSENCE_CHARGED";
	protected static final String AUTHOR_CN = "AUTHOR";
	protected static final String REMOTE_SYSTEM_CN = "REMOTE_SYSTEM";
	
	protected static final String SZEF_DZIALU = "szef_dzialu";
	protected static final String DZIAL_PERSONALNY = "dzial_personalny";
	protected static final String PRACOWNIK = "pracownik";
	
	public static final Integer STATUS_ZAREJESTROWANY = 10;
	public static final Integer STATUS_PRZESLANY_DO_AKCEPTACJI = 20;
	public static final Integer STATUS_ZAAKCEPTOWANY = 30;
	public static final Integer STATUS_ANULOWANY = 40;
	public static final Integer STATUS_ODRZUCONY = 50;
	
	protected static final Map<Integer, String> absenceTypes = new HashMap<Integer, String>();
	
	static {
		absenceTypes.put(10, "UW");
		absenceTypes.put(20, "U�");
		absenceTypes.put(30, "188");
		absenceTypes.put(40, "OG");
		absenceTypes.put(50, "UOK");
		absenceTypes.put(60, "UB");
		absenceTypes.put(70, "INW");
		absenceTypes.put(80, "UM");
		absenceTypes.put(90, "UMD");
		absenceTypes.put(100, "UJ");
		absenceTypes.put(110, "NN");
		absenceTypes.put(120, "NUN");
		absenceTypes.put(130, "NUP");
	}
	
	private AbstractAbsenceRequestLogic requestLogic;
	
	// PONI�EJ ZMIENNE PRZEKAZYWANE DLA WIDOKU 
	/**
	 * Lista urlop�w pracownika
	 */
	protected List<EmployeeAbsenceEntry> emplAbsences;
	
	/**
	 * Lista dni wolnych
	 */
	protected List<FreeDay> freeDays;
	
	/**
	 * Karta pracownika wnioskodawcy
	 */
	protected EmployeeCard empCard;
	
	/**
	 * Konstruktor
	 */
	public AbsenceRequestLogic() 
	{
		// ustalenie typu logiki dokumentu, lokalna lub zdalna
		requestLogic = AbstractAbsenceRequestLogic.getNewInstance();
	}
	
	/**
	 * Akcje archiwum
	 */
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		root().subfolder("Dokumenty pracownicze").subfolder("Wnioski o urlop").insert(document);

		FieldsManager fm = document.getFieldsManager();
		if (STATUS_ZAREJESTROWANY.equals(fm.getKey(STATUS_FIELD_CN)))
			validateData(document);
		// ustawienie statusu
		setStatus(fm, document);
		// ustawienie opisu
		setDocumentDescription(document);
	}

	/**
	 * Sprawdza czy mozna zakonczyc proces
	 */
        @Override
	public void checkCanFinishProcess(OfficeDocument document) throws AccessDeniedException, EdmException 
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		String error = "Nie mo�na zako�czy� pracy z dokumentem: Wniosek musi by� odrzucony lub zaakceptowany finalnie.";
		
		fm.initializeAcceptances();
		if (STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN)) || STATUS_ANULOWANY.equals(fm.getKey(STATUS_FIELD_CN)) 
				|| fm.getBoolean(AKCEPTACJA_FINALNA_FIELD_CN))
		{
			return;
		}
		
		throw new AccessDeniedException(error);
	}
	
	@Override
	public void onStartProcess(OfficeDocument document) throws EdmException
	{
		// sprawdzenie poprawnosci danych
		validateData(document);
	}
	
	@Override
	public boolean canAcceptDocument(FieldsManager fm, String acceptanceCn)
			throws EdmException 
	{
		String documentAuthor = (String) fm.getValue(AUTHOR_CN);
		String principalName = DSApi.context().getPrincipalName();
		if (acceptanceCn.equals(SZEF_DZIALU))
		{
			// sprawdza czy zalogowany uzytkownik jest przelozonym dla wnioskodowacy
			List<AcceptanceCondition> conditions = null;
			if ((fm.getBoolean(REMOTE_SYSTEM_CN) && AvailabilityManager.isAvailable("aegon.docusafe.remote")) || 
					(!fm.getBoolean(REMOTE_SYSTEM_CN) && !AvailabilityManager.isAvailable("aegon.docusafe.remote")))
			{
				conditions = AcceptanceCondition.find(SZEF_DZIALU, documentAuthor);
			}
			else
			{
				conditions = AcceptanceCondition.find(SZEF_DZIALU, "ext:"+documentAuthor);
			}
			for (AcceptanceCondition condition : conditions)
			{
				if (condition.getUsername().equals(principalName))
					return true;
			}
			
		}
		else if (acceptanceCn.equals(DZIAL_PERSONALNY) && AcceptanceCondition.canAcceptDocumentByUser(DZIAL_PERSONALNY, principalName))
		{
			return true;
		}
		
		return false;
	}
	
	@Override
	public void onDecretation(Document document) throws EdmException 
	{
		AcceptanceCondition ac = nextAcceptanceCondition(document);
		if (ac.getCn().equals(SZEF_DZIALU))
		{
			FieldsManager fm = document.getFieldsManager();
			fm.initialize();
			if (!STATUS_ANULOWANY.equals(fm.getKey(STATUS_FIELD_CN)) && !STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN)))
			{
				Map<String, Object> values = new HashMap<String, Object>();
				values.put(STATUS_FIELD_CN, STATUS_PRZESLANY_DO_AKCEPTACJI);
				document.getDocumentKind().setOnly(document.getId(), values);
			}
			
			setDocumentDescription(document);
		}
	}
	
	@Override
	public void onGiveAcceptance(Document document, String acceptanceCn) throws EdmException 
	{
		FieldsManager fm = document.getFieldsManager();
		
		if (STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN)) 
				|| STATUS_ANULOWANY.equals(fm.getKey(STATUS_FIELD_CN)))
			return;
		
		//validateData(document);
		
		Map<String, Object> values = new HashMap<String, Object>();
		
		if (acceptanceCn.equals(SZEF_DZIALU))
		{
			if (!fm.getBoolean(ABSENCE_CHARGED_CN))
			{
				values.put(ABSENCE_CHARGED_CN, Boolean.TRUE);
				chargeAbsence(fm);
			}
		}
		
		values.put(STATUS_FIELD_CN, STATUS_ZAAKCEPTOWANY);
		fm.reloadValues(values);
		fm.getDocumentKind().setOnly(fm.getDocumentId(), values);
		setStatus(fm, document);
		setDocumentDescription(document);
	}
	
	@Override
	public void onWithdrawAcceptance(Document document, String acceptanceCn) throws EdmException 
	{
		FieldsManager fm = document.getFieldsManager();
		Map<String, Object> values = new HashMap<String, Object>();
		// je�li cofniecie akceptacji wniosku AEGON przez szefa_dzialu
		if (acceptanceCn.equals(SZEF_DZIALU) && !STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN))
				&& !STATUS_ANULOWANY.equals(fm.getKey(STATUS_FIELD_CN)))
		{
			if (fm.getBoolean(ABSENCE_CHARGED_CN))
			{
				values.put(ABSENCE_CHARGED_CN, Boolean.FALSE);
				unchargeLastAbsence(fm);
			}
			values.put(STATUS_FIELD_CN, STATUS_PRZESLANY_DO_AKCEPTACJI);
		}
		fm.reloadValues(values);
		fm.getDocumentKind().setOnly(fm.getDocumentId(), values);
		setStatus(fm, document);
		setDocumentDescription(document);
	}
	
	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
        {
            Map<String,Object> values = new HashMap<String,Object>();
            values.put(KIND_CN, fm.getField(KIND_CN).getEnumItemByCn(RECREATIONAL_CN).getId());
            values.put(STATUS_FIELD_CN, STATUS_ZAREJESTROWANY); //status domyslnie na zarejestrowany
            values.put(AUTHOR_CN, DSApi.context().getPrincipalName());
            values.put(REMOTE_SYSTEM_CN, AvailabilityManager.isAvailable("aegon.docusafe.remote"));
            values.put(USER_CN, DSApi.context().getDSUser().asFirstnameLastname());
            fm.reloadValues(values);
        }
	
	@Override
	public void onLoadData(FieldsManager fm) throws EdmException 
	{
		String author = null;
		if (fm.getDocumentId() != null)
			author = (String) fm.getValue(AUTHOR_CN);
		// pobranie obiektu u�ytkownika zglaszaj�cego wniosek
		if (author == null || author.equals(""))
			author = DSApi.context().getDSUser().getName();
		
		// pobranie karty pracownika, moze rzucic wyjatek o braku aktualnej karty
		loadEmployeeCard(author);
		Map<String,Object> values = new HashMap<String,Object>();
		
		// ustawienie stanowiska
		values.put(POSITION_CN, empCard.getPosition());
		
		// wyliczenie dostepnych dni
		int availableDays = requestLogic.getAvailableDaysForCurrentYear(empCard);
		// ustawienie liczby dost�pnych dni
		values.put(AVAILABLE_DAYS_CN, availableDays);
		
		fm.reloadValues(values);
		// za�adowanie listy urlop�w dla widoku
		loadEmployeeAbsences(empCard);
		// za�adowanie listy dni wolnych dla JavaScriptu
		freeDays = requestLogic.getFreeDays();
	}
	
	/**
	 * Zwraca parametry dla task listy
	 */
	public TaskListParams getTaskListParams(DocumentKind dockind, long id)throws EdmException
	{
    	TaskListParams params = new TaskListParams();
    	try
    	{
			FieldsManager fm = dockind.getFieldsManager(id);
			params.setCategory((String) fm.getValue("KIND"));
			params.setStatus((String) fm.getValue("STATUS"));
    	}
    	catch (Exception e) 
    	{
			LOG.error(e.getMessage(), e);
		}
		return params;
	}
	
	/**
	 * Metoda zwraca nastepna akceptacje
	 * @param document
	 * @return AcceptanceCondition
	 * @throws EdmException
	 */
	public AcceptanceCondition nextAcceptanceCondition(Document document) throws EdmException
	{	
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		fm.initialize();
		fm.initializeAcceptances();
		
		// pobranie wykonanych akceptacji og�lnych
		List<DocumentAcceptance> docAccs = fm.getAcceptancesState().getGeneralAcceptances();
		
		boolean hasBossAcceptance = false;
		boolean hasPersonalDivAcceptance = false;
		if (docAccs != null && docAccs.size() > 0)
		{
			// sprawdzenie ju� danych akceptacji
			for (DocumentAcceptance acceptance : docAccs)
			{
				if (acceptance.getAcceptanceCn().equals(SZEF_DZIALU))
					hasBossAcceptance = true;
				
				if (acceptance.getAcceptanceCn().equals(DZIAL_PERSONALNY))
					hasPersonalDivAcceptance = true;
			}	
		}
		
		if (!hasBossAcceptance)
		{
			return getBossAcceptanceCondition(fm);
		}
		else if (!hasPersonalDivAcceptance)
		{
			// akceptacja dzialu personalnego
			return AcceptanceCondition.find(DZIAL_PERSONALNY).get(0);
		}	
		else
		{
			// przekazanie do wiadomosci dla wnioskodawcy jako akceptacja
			return getEmployeeAcceptanceCondition(fm);
		}
	}
	
	/**
	 * Zwraca map� akceptacji dla widoku
	 * 
	 * @param document
	 * @return
	 * @throws EdmException
	 */
	public Map<String, String> getAcceptancesMap(Document document) throws EdmException
	{
		Map<String, String> acceptancesMap = new LinkedHashMap<String, String>();
		
		FieldsManager fm = document.getFieldsManager();
		
		//acceptancesMap.put("", "Dekretacja automatyczna");
		AcceptanceCondition condition = nextAcceptanceCondition(document);
		
		if (STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN)))
		{
			// wniosek odrzucony przez szefa dzialu lub dzial personalny, zaladowanie do listy wnioskodawcy
			acceptancesMap.clear();
			if (!DSApi.context().getPrincipalName().equals((String) fm.getValue(AUTHOR_CN)))
				// je�li nie wnioskodawca to dodanie go do mapy akceptacji
				acceptancesMap.put((String) fm.getValue(AUTHOR_CN), (String) fm.getValue(USER_CN));
		}
		else if (STATUS_ANULOWANY.equals(fm.getKey(STATUS_FIELD_CN)) && condition.getCn().equals(PRACOWNIK))
		{
			// wniosek anulowany przez pracownika, zaladowanie do mapy ludzi z dzialu personalnego
			acceptancesMap.clear();
			addPersonalDivisionUsers(acceptancesMap);
		}
		else if (condition.getCn().equals(SZEF_DZIALU))
		{
			addDivisionBosses(acceptancesMap, (String) fm.getValue(AUTHOR_CN));
		}
		else if (condition.getCn().equals(DZIAL_PERSONALNY))
		{
			addPersonalDivisionUsers(acceptancesMap);              			
		}
		else if (condition.getCn().equals(PRACOWNIK))
		{
			addEmployeeToAcceptanceMap(fm, condition, acceptancesMap);
		}
		
		return acceptancesMap;
	}
	
	/**
	 * Zwraca list� urlop�w pracownika w bierzacym roku, dla widoku w dockind-specific-additions.jsp
	 * 
	 * @return
	 */
	public List<EmployeeAbsenceEntry> getEmplAbsences() 
	{
		return emplAbsences;
	}
	
	public List<FreeDay> getFreeDays()
	{
		return freeDays;
	}
	
	/**
	 * Zwraca kart� pracownika
	 * 
	 * @return
	 */
	public EmployeeCard getEmployeeCard()
	{
		return empCard;
	}
	
	public boolean isPersonalRightsAllowed()
	{
		return true;
	}
	
	public void documentPermissions(Document document) throws EdmException 
	{
		LoggerFactory.getLogger("tomekl").debug("documentPermissions");
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		String author = (String) fm.getValue(AUTHOR_CN);
		String name = null;
		
		try
		{
			name = DSUser.findByUsername(author).asFirstnameLastname();
		}
		catch (UserNotFoundException e) 
		{
			try
			{
				CardServiceStub cardServiceStub = new CardServiceStub();
				cardServiceStub._getServiceClient().getOptions().setTo(new EndpointReference(Docusafe.getAdditionProperty("cardService.endPointReference")));
				
				LoginRequest request = new LoginRequest();
				request.setLogin(author);
				
				GetUserInfo info = new GetUserInfo();
				info.setRequest(request);
				
				GetUserInfoResponse response = cardServiceStub.getUserInfo(info);
				LoginResponse loginResponse = response.get_return();
				
				name = loginResponse.getFirstName()+" "+loginResponse.getLastName();
			}
			catch (Exception e1) 
			{
				LoggerFactory.getLogger("kamilj").debug(e1.getMessage(),e1);
			}
		}
		
		if(name == null || name.length() < 1)
		{
			//returnujemy bo nie wiemy na kogo nadac uprawnienia
			return;
		}
		
		String readName = "Wniosek urlopowy "+name;
		String readGuid = "ABSENCE_"+author;
        
		Set<PermissionBean> perms = new HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ,readGuid,ObjectPermission.GROUP,readName));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,readGuid,ObjectPermission.GROUP,readName));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, readGuid,ObjectPermission.GROUP,readName));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, readGuid,ObjectPermission.GROUP,readName));
        
        if(document.getAuthor().equalsIgnoreCase(author))
        {
	        perms.add(new PermissionBean(ObjectPermission.READ,document.getAuthor(),ObjectPermission.USER,document.getAuthor()));
	        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,document.getAuthor(),ObjectPermission.USER,document.getAuthor()));
	        perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getAuthor(),ObjectPermission.USER,document.getAuthor()));
	        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getAuthor(),ObjectPermission.USER,document.getAuthor()));
        }
        
        setUpPermission(document, perms);
	}
	
        protected String getBaseGUID(Document document) throws EdmException
    {
    	return "AR_PERMISSIONS";
    }
	
	private void setDocumentDescription(Document document) throws EdmException
	{
		FieldsManager fm = document.getFieldsManager();
		String opis = "";
		opis += document.getDocumentKind().getName();
		opis += ", "+fm.getValue(USER_CN);
		opis += ", "+fm.getValue(STATUS_FIELD_CN);
		//Wniosek urlopowy, Paluch Micha�, Zarejestrowany
		document.setWparam(Document.WPARAMBIT_NW_NEEDS_NOT_BOX);
		
		document.setTitle(opis);
		((OfficeDocument) document).setSummaryWithoutHistory(opis);
		document.setDescription(opis);
	}
	
	/**
	 * Ustawia status wniosku
	 * 
	 * @param fm
	 * @param values
	 * @throws EdmException
	 */
	private void setStatus(FieldsManager fm, Document document) throws EdmException
	{
		fm.initializeAcceptances();
		AcceptanceCondition ac = nextAcceptanceCondition(document);
		Map<String,Object> values = new HashMap<String, Object>();
		
		if (STATUS_ZAREJESTROWANY.equals(fm.getKey(STATUS_FIELD_CN)) && ((String) fm.getValue(AUTHOR_CN)).equals(DSApi.context().getPrincipalName()))	
		{
			values.put(STATUS_FIELD_CN, STATUS_ZAREJESTROWANY);
		}
		else if (!STATUS_ANULOWANY.equals(fm.getKey(STATUS_FIELD_CN)) && !STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN)))
		{
			if (fm.getAcceptancesState().getGeneralAcceptancesDef().size() > 0 
					&& !STATUS_ZAAKCEPTOWANY.equals(fm.getKey(STATUS_FIELD_CN)))
			{
				values.put(STATUS_FIELD_CN, STATUS_PRZESLANY_DO_AKCEPTACJI);
			}
			else if ((DocumentAcceptance.find(document.getId(), SZEF_DZIALU, null) != null
						|| DocumentAcceptance.find(document.getId(), DZIAL_PERSONALNY, null) != null))
			{
				values.put(STATUS_FIELD_CN, STATUS_ZAAKCEPTOWANY);
			}
		}
		
		if (STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN)) &&
				DocumentAcceptance.hasAcceptance(document.getId()) && fm.getBoolean(ABSENCE_CHARGED_CN))
		{
			// usuniecie ostatniego wpisu urlopowego jesli odrzucil go szef dzialu lub dzial personalny
			values.put(ABSENCE_CHARGED_CN, Boolean.FALSE);
			unchargeLastAbsence(fm);
		}
		else if (STATUS_ANULOWANY.equals(fm.getKey(STATUS_FIELD_CN)) && fm.getBoolean(ABSENCE_CHARGED_CN) && 
				DocumentAcceptance.find(document.getId(), DZIAL_PERSONALNY, null) != null && ac.getCn().equals(PRACOWNIK))
		{
			// usuniecie ostatniego wpisu urlopowego jesli anulowal go uzytkownik
			values.put(ABSENCE_CHARGED_CN, Boolean.FALSE);
			unchargeLastAbsence(fm);
		}
		
		fm.reloadValues(values);
		fm.getDocumentKind().setOnly(document.getId(), values);
	}
	
	/**
	 * Dodaje uzytkownikow z dzialu personalnego do mapy akceptacji
	 * 
	 * @param acceptancesMap
	 * @throws EdmException
	 */
	private void addPersonalDivisionUsers(Map<String, String> acceptancesMap) throws EdmException
	{
		// wybranie osob z dzialu personalnego
		List<AcceptanceCondition> conditions = AcceptanceCondition.find(DZIAL_PERSONALNY);
		for (AcceptanceCondition cond : conditions)
    	{
    		try
    		{
    			acceptancesMap.put(cond.getUsername(), DSUser.findByUsername(cond.getUsername()).asLastnameFirstname());
    		}
    		catch (UserNotFoundException ex)
    		{
    			// je�li nie znajdzie sie uzytkownik - sprawdzenie czy jest on zewnetrznym userem
    			if (cond.getUsername().startsWith("ext:"))
    				acceptancesMap.put(cond.getUsername(), "U�ytk. zewn.: " + cond.getUsername().substring(4));
    			LOG.warn(ex.getMessage(), ex);
    		}
    	}  
	}
	
	/**
	 * Dodaje uzytkownikow jako szefow dzialu do mapy akceptacji
	 * 
	 * @param acceptancesMap
	 * @throws EdmException
	 */
	private void addDivisionBosses(Map<String, String> acceptancesMap, String documentAuthor) throws EdmException
	{
		// wybranie szefow dzialu dla uzytkownika
		List<AcceptanceCondition> conditions = AcceptanceCondition.find(SZEF_DZIALU, documentAuthor);
    	for (AcceptanceCondition cond : conditions)
    	{
    		try
    		{
    			acceptancesMap.put(cond.getUsername(), DSUser.findByUsername(cond.getUsername()).asLastnameFirstname());
    		}
    		catch (UserNotFoundException ex)
    		{
    			if (cond.getUsername().startsWith("ext:"))
    				acceptancesMap.put(cond.getUsername(), "U�ytk. zewn.: " + cond.getUsername().substring(4));
    			LOG.warn(ex.getMessage(), ex);
    		}
    	}	
	}
	
	/**
	 * Dodaje wnioskodawce do mapy akceptacji
	 * 
	 * @param acceptancesMap
	 * @throws EdmException
	 */
	private void addEmployeeToAcceptanceMap(FieldsManager fm, AcceptanceCondition condition, Map<String, String> acceptancesMap) throws EdmException
	{
		if (!DSApi.context().getDSUser().getName().equals((String) fm.getValue(AUTHOR_CN)))
		{
			// po akceptacji dzialu personalnego, zaladowanie do listy wnioskodawcy
			
			// sprawdzenie czy dalsza dekretacja ma isc na uzytkownika zewnetrznego
			if ((fm.getBoolean(REMOTE_SYSTEM_CN) && AvailabilityManager.isAvailable("aegon.docusafe.remote"))
					|| (!fm.getBoolean(REMOTE_SYSTEM_CN) && !AvailabilityManager.isAvailable("aegon.docusafe.remote")))
				acceptancesMap.put(condition.getUsername(), DSUser.findByUsername(condition.getUsername()).asLastnameFirstname());
			else
				acceptancesMap.put("ext:" + condition.getUsername(), "U�ytk. zewn.: " + condition.getUsername());
		}	
		else
			// je�li wniosek zosta� zwr�cony dla wnioskodawcy
			acceptancesMap.clear();
	}
	
	/**
	 * Sprawdzenie poprawno�ci danych
	 * 
	 * @param document
	 * @throws EdmException
	 */
	private void validateData(Document document) throws EdmException
	{
		FieldsManager fm = document.getFieldsManager();
		loadEmployeeCard((String) fm.getValue(AUTHOR_CN));
		
		// sprawdzenie typu wniosku urlopu
		Integer kindValue = (Integer) fm.getKey(KIND_CN);
		checkAbsenceType(kindValue);
		
		Date startDate = (Date) fm.getValue(FROM_CN);
		Date endDate = (Date) fm.getValue(TO_CN);
		
		final int daysLimit = 4;
		if (!AbsenceFactory.isCorrectStartDate(startDate, daysLimit))
			throw new EdmException("Wype�nienie wniosku niemo�liwe. Prosimy o kontakt z HR na adres: HR@aegon.pl");
		
		// SPRAWDZENIE CZY PRZYPADKIEM NIE ISTNIEJE JU� URLOP W PODANYM PRZEDZIALE DAT
		// LUB SPRAWDZENIE CZY NIE ISTNIEJE ODPOWIEDNI JU� WNIOSEK W PODANYM PRZEDZIALE DAT
		boolean hasAbsenceBetween = requestLogic.hasAbsenceBetween(empCard, startDate, endDate);
		LoggerFactory.getLogger("kamilj").debug("Has absence between: " + hasAbsenceBetween);
		if (hasAbsenceBetween || 
			hasAbsenceRequestBetween(document, startDate, endDate))
			throw new EdmException("Ju� istnieje wpis urlopowy lub z�o�ony wniosek w podanym zakresie dat!");
		
		// ZLICZENIE DOST�PNYCH DNI URLOPU
		checkAbsenceRequestDays(fm);
		
		if (kindValue.equals(20) /*URLOP NA ��DANIE*/)
		{
			// SPRAWDZENIE CZY TYP URLOPU JEST 'U�' I NIE PRZEKRACZA 4 DNI URLOPOWYCH W DANYM ROKU KALENDARZOWYM
			Long uzSum = requestLogic.getAbsenceSumDaysFromCurrentYear(empCard, "U�");
			if (uzSum == null) uzSum = 0l;
			Integer numDays = (Integer) fm.getValue(WORKING_DAYS_CN);
			uzSum += numDays;
			if (uzSum > 4)
				throw new EdmException("Nie mo�na wzi�� wi�cej ni� 4 dni urlopowe na ��danie w roku kalendarzowym!");
		}
		else if (kindValue.equals(30) || kindValue.equals(100) || kindValue.equals(70))
		{
			// SPRAWDZENIE ILO�CI DNI DODATKOWYCH INNYCH TYP�W (UJ, 188 kp, INW) 
			Integer maxDays = kindValue.equals(30) ? empCard.getT188kpDaysNum() : (kindValue.equals(100) ? empCard.getTujDaysNum() : 
				(kindValue.equals(70) ? empCard.getInvalidAbsenceDaysNum() : 0));
			Long sum = requestLogic.getAbsenceSumDaysFromCurrentYear(empCard, absenceTypes.get(kindValue));
			if (maxDays == null) maxDays = 0;
			if (sum == null) sum = 0l;
			Integer numDays = (Integer) fm.getValue(WORKING_DAYS_CN);
			sum += numDays;
			if (sum > maxDays)
				throw new EdmException("Nie mo�esz wzi�� wi�cej ni� " + maxDays + " dni urlopowe tego typu w roku kalendarzowym!");
		}
		
		// SPRAWDZENIE CZY DATA POCZATKOWA URLOPU NIE JEST PRZED DATA ZATRUDNIENIA
		if (startDate.before(empCard.getEmploymentStartDate()))
			throw new EdmException("Nie mo�na wzi�� urlopu przed dat� zatrudnienia wpisanej w karcie pracownika!");
		
	}

	/**
	 * Sprawdza czy karta pracownika posiada odpowiednie uprawnienia do danego typu urlopu
	 * 
	 * @param kindValue
	 * @throws EdmException
	 */
	private void checkAbsenceType(Integer kindValue) throws EdmException
	{
		if (kindValue.equals(30) && (empCard.getT188KP() == null || empCard.getT188KP().booleanValue() == false))
			throw new EdmException("Brak uprawnienia do skorzystania z tego typu urlopu!");
		else if (kindValue.equals(100) && (empCard.getTUJ() == null || empCard.getTUJ().booleanValue() == false))
			throw new EdmException("Brak uprawnienia do skorzystania z tego typu urlopu!");
		else if (kindValue.equals(70) && (empCard.getTINW() == null || empCard.getTINW().booleanValue() == false))
			throw new EdmException("Brak uprawnienia do skorzystania z tego typu urlopu!");
	}
	
	/**
	 * Sprawdza czy istnieje ju� nienaliczony wniosek w podanym przedziale dat
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	private boolean hasAbsenceRequestBetween(Document document, Date startDate, Date endDate) throws EdmException
	{
		PreparedStatement ps = null;
    	ResultSet rs = null;
    	try
    	{
    		String author = (String) document.getFieldsManager().getValue(AUTHOR_CN);
    		
    		Calendar start = Calendar.getInstance();
    		start.setTime(startDate);
    		start.set(Calendar.HOUR_OF_DAY, start.getMinimum(Calendar.HOUR_OF_DAY));
    		start.set(Calendar.MINUTE, start.getMinimum(Calendar.MINUTE));
    		start.set(Calendar.SECOND, start.getMinimum(Calendar.SECOND));
    		start.set(Calendar.MILLISECOND, start.getMinimum(Calendar.MILLISECOND));
    		
    		Calendar end = Calendar.getInstance();
    		end.setTime(endDate);
    		end.set(Calendar.HOUR_OF_DAY, end.getMinimum(Calendar.HOUR_OF_DAY));
    		end.set(Calendar.MINUTE, end.getMinimum(Calendar.MINUTE));
    		end.set(Calendar.SECOND, end.getMinimum(Calendar.SECOND));
    		end.set(Calendar.MILLISECOND, end.getMinimum(Calendar.MILLISECOND));
    		
    		ps = DSApi.context().prepareStatement("SELECT count(a.document_id) FROM dsg_absence_request a WHERE " +
    			  "((a.period_from <= ? AND a.period_to >= ?) OR (a.period_from <= ? AND a.period_to >= ?))" +
    			  " AND a.author = ? AND a.document_id <> ? AND (a.abs_charged IS NULL OR a.abs_charged = 0) AND (a.status IS NULL OR a.status IN (10,20))");
    		
    		ps.setTimestamp(1, new java.sql.Timestamp(start.getTime().getTime()));
    		ps.setTimestamp(2, new java.sql.Timestamp(start.getTime().getTime()));
    		ps.setTimestamp(3, new java.sql.Timestamp(end.getTime().getTime()));
    		ps.setTimestamp(4, new java.sql.Timestamp(end.getTime().getTime()));
    		ps.setString(5, author);
    		ps.setLong(6, document.getId());
			rs = ps.executeQuery();
			
			if(rs.next())
			{
				int res = rs.getInt(1);
				return res > 0;
			}
			return false;
    	}
    	catch (SQLException ex)
    	{
    		throw new EdmException(ex.getMessage(), ex);
    	}
    	finally
    	{
    		DbUtils.closeQuietly(rs);
    		DbUtils.closeQuietly(ps);
    	}
	}
	
	/**
	 * Sprawdza liczb� dost�pnych dni urlopowych na podstawie z�o�onych wniosk�w
	 * 
	 * @throws EdmException
	 */
	private void checkAbsenceRequestDays(FieldsManager fm) throws EdmException
	{
		int availableDays = (Integer) fm.getValue(AVAILABLE_DAYS_CN);
		
		PreparedStatement ps = null;
    	ResultSet rs = null;
    	try
    	{
    		String author = (String) fm.getValue(AUTHOR_CN);
    		
    		/* NIE SPRAWDZAM WNIOSKOW TYLKO Z TEGO ROKU, NIECH SPRAWDZA WSZYSTKIE ZLOZONE
    		Calendar firstDate = GregorianCalendar.getInstance();
    		firstDate.set(firstDate.get(Calendar.YEAR), Calendar.JANUARY, 1, 
    				firstDate.getMinimum(Calendar.HOUR_OF_DAY), firstDate.getMinimum(Calendar.MINUTE), firstDate.getMinimum(Calendar.SECOND));
    		Calendar secondDate = GregorianCalendar.getInstance();
    		secondDate.set(secondDate.get(Calendar.YEAR), Calendar.DECEMBER, 31, 
    				secondDate.getMaximum(Calendar.HOUR_OF_DAY), secondDate.getMaximum(Calendar.MINUTE), secondDate.getMaximum(Calendar.SECOND));
    		*/
    		
    		ps = DSApi.context().prepareStatement("SELECT SUM(a.working_days) FROM dsg_absence_request a WHERE " +
    			  /*"(a.period_from >= ? AND a.period_to <= ?) AND*/" a.kind IN (10,50,110,120,130)" +
    			  " AND a.author = ? AND (a.abs_charged IS NULL OR a.abs_charged = 0) AND (a.status IS NULL OR a.status IN (10,20))");
    		
    		//ps.setTimestamp(1, new java.sql.Timestamp(firstDate.getTime().getTime()));
    		//ps.setTimestamp(2, new java.sql.Timestamp(secondDate.getTime().getTime()));
    		ps.setString(1, author);
			rs = ps.executeQuery();
			
			int sum = 0;
			if (rs.next())
			{
				sum += rs.getInt(1);
			}
			if (sum > availableDays)
				throw new EdmException("Ilo�� dni w z�o�onych wnioskach urlopowych przekracza ilo�� dost�pnych dni! Zmniejsz ilo�� dni we wniosku!");
    	}
    	catch (SQLException ex)
    	{
    		throw new EdmException(ex.getMessage(), ex);
    	}
    	finally
    	{
    		DbUtils.closeQuietly(rs);
    		DbUtils.closeQuietly(ps);
    	}
	}
	
	/**
	 * Za�adowanie listy urlop�w pracownika w bierzacym roku, dla widoku w dockind-specific-additions.jsp
	 * 
	 * @param empCard
	 * @throws EdmException
	 */
	private void loadEmployeeAbsences(EmployeeCard empCard) throws EdmException
	{
		LoggerFactory.getLogger("kamilj").debug("loadEmployeeAbsences:empCard=" + empCard.getExternalUser());
		int currentYear = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
		emplAbsences = requestLogic.getEmployeeAbsences(empCard, currentYear);
	}
	
	/**
	 * Dodanie wpisu urlopowego dla wnioskodawcy, do aktywnych kart pracowniczych
	 * 
	 * @param document
	 * @throws EdmException
	 */
	private void chargeAbsence(FieldsManager fm) throws EdmException
	{
		loadEmployeeCard((String) fm.getValue(AUTHOR_CN));
		// ustawienie dat
		Date from = (Date) fm.getValue(FROM_CN);
		Date to = (Date) fm.getValue(TO_CN);
		// ustawienie typu urlopu
		int typeId = fm.getField(KIND_CN).getEnumItemByTitle(
				(String) fm.getValue(KIND_CN)).getId();
		// info
		String info = (String) fm.getValue(DESCRIPTION_CN);
		// ustawienie liczby dni
		int numDays = Integer.parseInt(fm.getValue(WORKING_DAYS_CN) + "");
		
		requestLogic.chargeAbsence(empCard, from, to, absenceTypes.get(typeId), info, numDays, fm.getDocumentId());
	}
	
	/**
	 * Usuwa ostatni wpis urlopu z karty glownej i o tych samych KPX z roznych spolek
	 * 
	 * @param document
	 * @throws EdmException
	 */
	private void unchargeLastAbsence(FieldsManager fm) throws EdmException
	{
		// usuniecie z glowenj karty
		loadEmployeeCard((String) fm.getValue(AUTHOR_CN));
		requestLogic.unchargeLastAbsences(empCard, fm.getDocumentId());
	}
	
	/**
	 * �aduje kart� pracownika
	 * 
	 * @param userImpl
	 * @return
	 * @throws Exception
	 */
	private void loadEmployeeCard(String userName) throws EdmException
	{LoggerFactory.getLogger("kamilj").debug("loadEmployeeCard:userName=" + userName);
		empCard = requestLogic.getActiveEmployeeCardByUserName(userName);
		
		if (empCard == null)
			throw new EdmException("Brak aktualnej karty pracownika!");
	}
	
	/**
	 * Zwraca akceptacje pracownika
	 * 
	 * @param document
	 * @return
	 * @throws EdmException
	 */
	private AcceptanceCondition getEmployeeAcceptanceCondition(FieldsManager fm) throws EdmException
	{
		AcceptanceCondition retCondition = null;

		retCondition = new AcceptanceCondition();
		retCondition.setCn(PRACOWNIK);
		retCondition.setUsername((String) fm.getValue(AUTHOR_CN));
		
		return retCondition;
	}
	
	/**
	 * Wyszukuje akceptacj� szefa dzia�u
	 * 
	 * @param document
	 * @return
	 * @throws EdmException
	 */
	private AcceptanceCondition getBossAcceptanceCondition(FieldsManager fm) throws EdmException
	{
		// akceptacja szefa dzialu
		List<AcceptanceCondition> conditions = AcceptanceCondition.find(SZEF_DZIALU);
		
		if (conditions == null || conditions.size() == 0)
			throw new EdmException("Nie okre�lono szefa dzia�u!");
		
		
		AcceptanceCondition retValue = null;
		for (AcceptanceCondition ac : conditions)
		{
			if ((fm.getBoolean(REMOTE_SYSTEM_CN) && AvailabilityManager.isAvailable("aegon.docusafe.remote")) || (!fm.getBoolean(REMOTE_SYSTEM_CN) && !AvailabilityManager.isAvailable("aegon.docusafe.remote")))
	         {
	             if (ac.getFieldValue().equals((String) fm.getValue(AUTHOR_CN)))
	             {
	                 retValue = ac;
	                 break;
	             }
	         }
	         else
	         {
	             if (ac.getFieldValue().contains((String) fm.getValue(AUTHOR_CN)))
	             {
	                 retValue = ac;
	                 break;
	             }
	         }	
		}
		
		 
		 
		 
		if (retValue == null)
			throw new EdmException("Nie okre�lono szefa dzia�u!");
		
		return retValue;
	}
	
	public void onSendDecretation(Document document,AcceptanceCondition condition) throws EdmException 
	{
		//nic nie robimy		
	}
	
	public void onAccept(String acceptanceCn, Long documentId) throws EdmException
	{
		//nic nie robimy
	}
	
	public void onWithdraw(String acceptanceCn, Long documentId) throws EdmException
	{
		//nic nie robimy
	}
	
	public boolean canSendDecretation(FieldsManager fm) throws EdmException {
		// TODO Auto-generated method stub
		return true;
	}
}
