package pl.compan.docusafe.parametrization.aegon;

import java.util.Date;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.base.absences.EmployeeAbsenceEntry;
import pl.compan.docusafe.core.base.absences.FreeDay;
import pl.compan.docusafe.core.users.sql.UserImpl;

/**
 * Lokalne moetody logiki wniosku urlopowego dla aegon
 * 
 * @author User
 */
public class LocalAbsenceRequestLogic extends AbstractAbsenceRequestLogic 
{
	@Override
	public boolean hasAbsenceBetween(EmployeeCard empCard, Date startDate,
			Date endDate) throws EdmException 
	{
		return AbsenceFactory.hasAbsenceBetween(empCard, startDate, endDate);
	}
	
	@Override
	public Long getAbsenceSumDaysFromCurrentYear(EmployeeCard empCard,
			String type) throws EdmException 
	{
		return AbsenceFactory.getAbsenceSumDaysFromCurrentYear(empCard, type);
	}
	
	@Override
	public EmployeeCard getActiveEmployeeCardByUserName(String userName)
			throws EdmException 
	{
		return AbsenceFactory.getActiveEmployeeCardByUserName(userName);
	}
	
	@Override
	public void unchargeLastAbsences(EmployeeCard empCard, Long documentId) throws EdmException 
	{
		AbsenceFactory.unchargeLastAbsences(empCard, documentId);
	}
	
	@Override
	public void chargeAbsence(EmployeeCard empCard, Date from, Date to,
			String type, String info, int numDays, Long documentId) throws EdmException 
	{
		AbsenceFactory.chargeAbsence(empCard, from, to, type, info, numDays, (UserImpl) DSApi.context().getDSUser(), null, documentId);
	}
	
	@Override
	public List<EmployeeAbsenceEntry>  getEmployeeAbsences(EmployeeCard empCard, int currentYear)
			throws EdmException 
	{
		return AbsenceFactory.getEmployeeAbsences(empCard, currentYear);
	}
	
	@Override
	public int getAvailableDaysForCurrentYear(EmployeeCard empCard)
			throws EdmException 
	{
		return AbsenceFactory.getAvailableDaysForCurrentYear(empCard);
	}
	
	@Override
	public List<FreeDay> getFreeDays() throws EdmException 
	{
		return AbsenceFactory.getFreeDays();
	}
}
