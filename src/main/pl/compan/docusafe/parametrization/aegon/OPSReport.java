package pl.compan.docusafe.parametrization.aegon;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.SqlUtils;
import edu.emory.mathcs.backport.java.util.Arrays;

public class OPSReport extends Report {

	UniversalTableDumper dumper;
	
	private Set<String> guid;
	private Set<String> user;
	private Set<String> status;
	private Set<String> rodzaj;
	
	private Date dateFrom;
	private Date dateTo;
	
	private void initValues() throws Exception
	{
		for(ReportParameter param : getParams())
		{
			if(param.getType().equals("break-line"))
				continue;
			
			if(param.getFieldCn().equals("division"))
			{
				guid = new HashSet<String>();
				guid.addAll(param.valuesAsList());
			}
			
			if(param.getFieldCn().equals("user"))
			{
				user = new HashSet<String>();
				user.addAll(param.valuesAsList());
			}
			
			if(param.getFieldCn().equals("STATUS"))
			{
				status = new HashSet<String>();
				status.addAll(param.valuesAsList());
			}
			
			if(param.getFieldCn().equals("RODZAJ"))
			{
				rodzaj = new HashSet<String>();
				rodzaj.addAll(param.valuesAsList());
			}
			
			if(param.getFieldCn().equals("dateFrom"))
			{
				dateFrom = DateUtils.jsDateFormat.parse((String) param.getValue());
			}
			
			if(param.getFieldCn().equals("dateTo"))
			{
				dateTo = DateUtils.jsDateFormat.parse((String) param.getValue());
			}
			
		}
	}
	
	private String getSQL()
	{
		StringBuffer sql = new StringBuffer();
		
		sql.append("select ht.cdate as data, dt.FIELD_3 as typ,");
		
		for(String gd : guid)
			sql.append(String.format("sum(case when ht.targetguid='%s' then 1 else 0 end) as guid_%s, ", gd, gd));
		
		sql.append("count(dt.document_id) as suma from dsg_doctype_1 dt,dso_document_asgn_history ht with (nolock) ");
		sql.append("where (dt.document_id=ht.document_id) ");
		sql.append("and (ht.type is null) ");
		sql.append("and (ht.finished=0) "); 
		sql.append("and (ht.accepted=0) ");
		sql.append("and (ht.processname<>'rejestracja pisma') "); 
		sql.append("and (ht.cdate>=?) and (ht.cdate<=?) ");
		
		if(!guid.isEmpty())
			sql.append(String.format("and (ht.targetguid in (%s)) ", SqlUtils.questionMarks(guid.size())));
		
		if(!rodzaj.isEmpty())
			sql.append(String.format("and (dt.FIELD_3 in (%s)) ", SqlUtils.questionMarks(rodzaj.size())));
		
		if(!status.isEmpty())
			sql.append(String.format("and (dt.FIELD_2 in (%s)) ", SqlUtils.questionMarks(status.size())));
		
		if(!user.isEmpty())
			sql.append(String.format("and (ht.sourceuser in (%s)) ", SqlUtils.questionMarks(user.size())));
		
		sql.append("group by ht.cdate,dt.FIELD_3 ");
		sql.append("order by data, typ");
		return sql.toString();
	}
	
	
	public void doReport() throws Exception 
	{		
		LoggerFactory.getLogger("tomekl").debug("OPS Report");
		try
		{
			this.initValues();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		/** DUMPER **/
		dumper = new UniversalTableDumper();
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "raport.xls");
		poiDumper.openFile(xlsFile);
		dumper.addDumper(poiDumper);
		
		/** NAGLOWEK **/
		dumper.newLine();
		dumper.addText("Data dekretacji");
		dumper.addText("Typ dokumentu");
		for(String g : guid)
			dumper.addText(DSDivision.find(g).getName());
		dumper.addText("Suma");
		dumper.dumpLine();
		/** NAGLOWEK:END **/
				
		
		PreparedStatement ps = null;
		try
		{
			int count = 0;
			ps = DSApi.context().prepareStatement(getSQL());
			ps.setDate(++count, new java.sql.Date(dateFrom.getTime()));
			ps.setDate(++count, new java.sql.Date(dateTo.getTime()));
			
			if(!guid.isEmpty())
			{
				for(String sguid : guid)
					ps.setString(++count, sguid);
			}
			
			if(!rodzaj.isEmpty())
			{
				for(String srodzaj : rodzaj)
					ps.setInt(++count, Integer.valueOf(srodzaj));
			}
			
			if(!status.isEmpty())
			{
				for(String sstatus : status)
					ps.setInt(++count, Integer.valueOf(sstatus));
			}
			
			if(!user.isEmpty())
			{
				for(String suser : user)
					ps.setString(++count, suser);
			}
			
			ResultSet rs = ps.executeQuery();
			
			 
			Date currentDate = new Date();
			Date rsDate = null;
			Map<String,Integer> currentSum = new HashMap<String, Integer>();
			Map<String,Integer> allSum = new HashMap<String, Integer>();
			
			while(rs.next())
			{
				rsDate = rs.getDate("data");
				if(!rsDate.equals(currentDate))
				{
					if(!currentSum.isEmpty())
					{
						dumper.newLine();
						dumper.addText("");
						dumper.addText("Og�em");
						for(String g : guid)
						{
							Integer currAllCount = currentSum.get(g);
							dumper.addInteger(currAllCount);
							
							if(!allSum.containsKey(g))
								allSum.put(g, new Integer(0));
							
							currAllCount = currAllCount + allSum.get(g);
							allSum.put(g, currAllCount);							
						}
						
						Integer curAllSuma = currentSum.get("suma");
						dumper.addInteger(curAllSuma);
						
						if(!allSum.containsKey("suma"))
							allSum.put("suma", new Integer(0));
						
						curAllSuma = curAllSuma + allSum.get("suma");
						allSum.put("suma", curAllSuma);
						
						dumper.dumpLine();	
						currentSum.clear();
					}
					
					dumper.newLine();
					dumper.addDate(rsDate,"dd-MM-yyyy");
					dumper.dumpLine();
					currentDate = rsDate;
				}
				
				dumper.newLine();
				
				dumper.addText("");
				
				dumper.addText(DocumentKind.findByCn(DocumentLogicLoader.NATIONWIDE_KIND).getFieldByCn("RODZAJ").getEnumItem(rs.getInt("typ")).getTitle());
				
				for(String g : guid)
				{
					Integer curCount  = rs.getInt("guid_"+g);
					dumper.addInteger(curCount);
					
					if(!currentSum.containsKey(g))
						currentSum.put(g, new Integer(0));
					
					curCount = curCount + currentSum.get(g);
					currentSum.put(g, curCount);
				}
				
				Integer curSuma = rs.getInt("suma");
				dumper.addInteger(curSuma);
				if(!currentSum.containsKey("suma"))
					currentSum.put("suma", new Integer(0));
				curSuma = curSuma + currentSum.get("suma");
				currentSum.put("suma", curSuma);
				
				dumper.dumpLine();
					
			}
			
			rs.close();
			
			if(!currentSum.isEmpty())
			{
				dumper.newLine();
				dumper.addText("");
				dumper.addText("Og�em");
				for(String g : guid)
				{
					Integer currAllCount = currentSum.get(g);
					dumper.addInteger(currAllCount);
					
					if(!allSum.containsKey(g))
						allSum.put(g, new Integer(0));
					
					currAllCount = currAllCount + allSum.get(g);
					allSum.put(g, currAllCount);
				}
				Integer curAllSuma = currentSum.get("suma");
				dumper.addInteger(curAllSuma);
				
				if(!allSum.containsKey("suma"))
					allSum.put("suma", new Integer(0));
				
				curAllSuma = curAllSuma + allSum.get("suma");
				allSum.put("suma", curAllSuma);
				dumper.dumpLine();				
				currentSum.clear();
			}
			
			dumper.newLine();
			dumper.addText("");
			dumper.dumpLine();
			
			dumper.newLine();
			dumper.addText("Podsumowanie");
			dumper.addText("");
			for(String g : guid)
			{
				dumper.addInteger(allSum.get(g));
			}
			dumper.addInteger(allSum.get("suma"));
			dumper.dumpLine();
			
		}
		catch (Exception e) 
		{
			throw new EdmException(e);
			//e.printStackTrace();
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		
		dumper.closeFileQuietly();
		/** DUMPER:END **/
	}
}