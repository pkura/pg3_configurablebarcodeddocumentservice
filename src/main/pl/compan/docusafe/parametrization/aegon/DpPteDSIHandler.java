package pl.compan.docusafe.parametrization.aegon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.dockinds.dictionary.DpInst;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.service.imports.dsi.DSIImportHandler;

public class DpPteDSIHandler extends DSIImportHandler
{

	
	public String getDockindCn()
	{
		return DocumentLogicLoader.DP_PTE_KIND;
	}

	private Long getInst(String name)
	{
		name = name.trim();
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("name", name);
		
		try
		{
			List<DpInst> insts = DpInst.find(params);
			if(insts != null && insts.size() > 0 )
			{
				return insts.get(0).getId();
			}
			else
			{
				DpInst inst = new DpInst();
				inst.setName(name);
				inst.setKraj("PL");
				inst.create();
				return inst.getId();
			}
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return -1L;
		}
	}
	
	@Override
	protected boolean isStartProces()
	{
		return true;
	}
	
	protected void prepareImport() throws Exception
	{	
		if(values.containsKey("INSTYTUCJA_KONTRAHENT"))
		{
			String inst = (String) values.get("INSTYTUCJA_KONTRAHENT");
			values.remove("INSTYTUCJA_KONTRAHENT");
			values.put("INSTYTUCJA_KONTRAHENT", getInst(inst));
		}
		
		if(values.containsKey("KANCELARIA"))
		{
			String inst = (String) values.get("KANCELARIA");
			values.remove("KANCELARIA");
			values.put("KANCELARIA", getInst(inst));
		}
		
		/*LoggerFactory.getLogger("tomekl").debug(" "+values.get("UCHWALY").getClass());
		if(values.containsKey("UCHWALY"))
		{
			if(values.get("UCHWALY") instanceof ArrayList)
			{
				for(String uchwala : ((ArrayList<String>)values.get("UCHWALY")))
				{
					uchwala = uchwala.trim();
				}
			}
			else if(values.get("UCHWALY") instanceof String)
			{
				String uchwala = (String) values.get("UCHWALY");
				values.remove("UCHWALY");
				values.put("UCHWALY", uchwala.trim());
			}
		}*/
		
	}

}
