package pl.compan.docusafe.parametrization.aegon;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

import oracle.jdbc.driver.OracleDriver;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.Crm;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.StringManager;
/**
 * Klasa agenta sluzy do integracji z CRM w AEGON
 * Uzytkownicy dekretuja zadania na agenta , agent co okerslony czas sprawdza swoja liste zadan 
 * i przesyla zadania do CRM jednoczesnie konczac zadania w docusafe
 * Jesli zadanie nie dotyczy dokumentu biznesowego dokument zostaje zworony do nadawcy
 * Jesli nie dotyczy dokumentu biznesowego i jest do wiadomosci to jest konczona praca z zadaniem 
 * Do dzia�ania agenta potrzeba pliku agent.config w katalogu HOME
 * przykladowy plik:<ul>
 * #dane do po��czenia z baza danych<br>
 * database.type=oracle_9<br>
 * username=z2<br>
 * password=q<br>
 * url=jdbc:oracle:thin:@127.0.0.1:1521:XE<br>
 * #nazwa tabeli do kt�rej agent zapisuje dane<br>
 * entityName = ds_crm<br>
 * #nazwa u�ytkownika symuluj�cego agenta<br>
 * agentName = agent<br>
 * #czestotliwosc w minutach pracy agenta<br>
 * agentTime = 1<br>
 * @author Mariusz Kilja�czyk
 */
public class CrmDriver extends AegonAgent
{
	public StringManager sm = 
        GlobalPreferences.loadPropertiesFile("",null);
    private Timer timer;
    private static final Log log = LogFactory.getLog("agent_log");
    
    
    private static String entityName = null;  
    
    private String url;
    private String user;
    private String pass;
    
    public String getAgentCode()
    {
    	return "CRM";
    }
    
    public String getPropertyPrefix()
    {
    	return "";
    }
    
  
    
    protected void setupAgent()
    {
    	try
        {
    		//ten kawalek bedzie mozna niebawem wyrzucic - jest to rejestrowane przy starcie
        	OracleDriver Driver = new OracleDriver();
        	DriverManager.registerDriver(Driver);
        	
        }
        catch (SQLException e)
        {
        	log.error("Blad w czasie incjowania drivera",e);
        }
        url = configuration.getProperty("url");
    	user = configuration.getProperty("username");
    	pass = configuration.getProperty("password");
    	entityName = configuration.getProperty("entityName");
    }
    
    private Connection connectToOracle() throws SQLException
    {   	
    	return DriverManager.getConnection(url, user, pass);
    }
    
    private void disconnectOracle(Connection con)
    {
    	if (con!=null)
    	{
    		try
    		{ 
    			con.close();
    		} catch (Exception e) { }
    	}
    }
    
    private void doOne(TaskSnapshot task)
    {
    	Connection oracleCon = null;
    	WorkflowFactory wff = WorkflowFactory.getInstance();
    	OfficeDocument doc = null;
    	
    	try
    	{
    		oracleCon = connectToOracle();
    		DSApi.context().begin();
    		
    		String activityId = wff.keyToId(task.getActivityKey());
            WorkflowActivity activity = WorkflowFactory.getWfActivity(activityId);
            
            doc = (OfficeDocument) OfficeDocument.find(task.getDocumentId());
            
            if(!task.isExternalWorkflow())
            {
                log.trace("Zadanie"+task.getActivityKey()+" Zwiazane z documentem "+doc.getId()+" nie znajduje sie w workflow");
                if(doc.getDocumentKind()!=null && doc.getDocumentKind().getCn().equals(DocumentLogicLoader.NATIONWIDE_KIND))
                {
                    log.trace("Zadanie dotyczy dokumentu nationwide");
                    FieldsManager fm = doc.getDocumentKind().getFieldsManager(doc.getId());
                    Crm crm = new Crm();
                    crm.setCrmStatus("DS");                            
                    crm.setDocId(doc.getId().intValue());
                    crm.setActivityId(task.getActivityKey());
                    crm.setActivityKind(task.getProcess().equals("Obieg r�czny") ? 0 : 1);
                    crm.setDocumentDate(doc.getCtime());
                    crm.setEntryDate(new Date());
                    crm.setFromUser(task.getActivity_lastActivityUser());

                    if (doc.getOfficeNumber()!= null)
                        crm.setOfficeNumber(doc.getOfficeNumber().toString());
                    if (task.getDocumentType().equals("in"))
                        crm.setOfficeType("incoming");
                    else if (task.getDocumentType().equals("out"))
                        crm.setOfficeType("outcoming");
                    else
                        crm.setOfficeType("internal");

                    if("fax".equals(doc.getSource()))
                        crm.setSposobDostarczenia("faks");
                    else
                        crm.setSposobDostarczenia("list");                   
                    crm.create(oracleCon);                            
                    log.info("Zapisal do bazy Agenta dokument o nr.ko = "+doc.getOfficeNumber());
                    
                    log.trace("Zadanie mozna zakonczyc = " + canFinish(doc,activity));
                    if(!canFinish(doc,activity))
                    {
                        Remark remark = new Remark("Brak mo�liwo�ci zako�czenia procesu, zadanie przekazano do CRM","AGENT");
                        doc.addRemark(remark);
                        log.trace("Zadanie jest w procesie task.getProcess = "+ task.getProcess());
                        if(wff.isManual(activity))
                        {
                            removeAssign(task);
                            log.trace("Zadanie znajduje sie w obiegu recznym. Zosta�o zwrocone nadawcy");
                        }
                        else
                        {
                            doc.addAssignmentHistoryEntry(new AssignmentHistoryEntry(
                                "AGENT", sm.getString("doWiadomosci"), AssignmentHistoryEntry.PROCESS_TYPE_CC));
                            wff.manualFinish(activityId, false);
                            log.trace("Zadanie do wiadomosci. Zakonczono zadanie");
                        }
                            
                        
                        
                    }
                    else {
                    	WorkflowFactory.getInstance().manualFinish(activityId, true);
                    }
                    //ustawienie statusu
                    Map statusMap = new HashMap<String,Object>();
                    doc.getDocumentKind().setWithHistory(doc.getId(),statusMap,false);
                }                   
                else if (wff.isManual(activity))
                {
                    log.trace("Zadanie nie dotyczy dokumentu nationwide i jest w obiegu recznym. Zwracam dekretacje");
                    removeAssign(task);
                }
                else
                {

                    doc.addAssignmentHistoryEntry(new AssignmentHistoryEntry(
                        "AGENT", sm.getString("doWiadomosci"), AssignmentHistoryEntry.PROCESS_TYPE_CC));
                    wff.manualFinish(activityId, false);
                    log.trace("Zadanie nie dotyczy dokumentu nationwide i jest do wiadomosci. Koncze prace");
                }
                TaskSnapshot.updateAllTasksByDocumentId(task.getDocumentId(),task.getDocumentType());
                log.trace("Aktualizuje liste zadan");
            }
        
       
    		
    		DSApi.context().commit();
    	}
    	catch(UserNotFoundException e)
    	{
    		try {DSApi.context().rollback();} catch (EdmException e1) {log.error("",e1);}
            log.trace("Nie znaleziono uzytkownika Agenta",e);
            log.error(e.getStackTrace());
    	}
    	catch (Exception e) {
    		try {DSApi.context().rollback();} catch (EdmException e1) {log.error("",e1);}
            log.trace("Blad podczas pracy Agenta",e);
            log.error("",e);
		}
    	finally
    	{
    		disconnectOracle(oracleCon);
    	}
    	
    }
    
    public void run()
    {
    	log.info("CRM Agent sprawdza liste zadan");
        TaskSnapshot.Query query;
        query = new TaskSnapshot.Query();
        TaskSnapshot task =null;
        try
        {
               DSApi.openAdmin();
               query.setUser(DSUser.findByUsername(agentName));
               SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
               log.trace("CRM Na liscie Agenta znajduje sie "+ results.count()+ " zadan");
               setupAgent();
               while (results.hasNext())
               {
                   task = results.next();
                   task.toFullTask(true);                    
                   doOne(task);                                                                               
               }                
        }          
        catch (Exception e)
        {
       	 log.error("CRM "+e.getStackTrace());
        }
        finally
        {
           	DSApi._close();
        }
    }
    
    /** Metoda wykonuje glowna funkcje systemu **/
    
        @SuppressWarnings("unchecked")
        public void run_old()
        {
        	WorkflowFactory wff = WorkflowFactory.getInstance();
            log.info("Agent sprawdza liste zadan");
            TaskSnapshot.Query query;
            query = new TaskSnapshot.Query();
            Connection oracleCon = null;
            try
            {
            	oracleCon = connectToOracle();
                DSApi.openAdmin();
                DSApi.context().begin();
                query.setUser(DSUser.findByUsername(agentName));
                SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
                log.trace("Na liscie Agenta znajduje sie "+ results.count()+ " zadan");
                while (results.hasNext())
                {
                	TaskSnapshot task ;
                	OfficeDocument doc;   
                	
                    task = results.next();
                    String activityId = wff.keyToId(task.getActivityKey());
                    WorkflowActivity activity = WorkflowFactory.getWfActivity(activityId);
                    task.toFullTask(true);
                    doc = (OfficeDocument) OfficeDocument.find(task.getDocumentId());
                    if(!task.isExternalWorkflow())
                    {
                        log.trace("Zadanie"+task.getActivityKey()+" Zwiazane z documentem "+doc.getId()+" nie znajduje sie w workflow");
                        if(doc.getDocumentKind()!=null && doc.getDocumentKind().getCn().equals(DocumentLogicLoader.NATIONWIDE_KIND))
                        {
                            log.trace("Zadanie dotyczy dokumentu nationwide");
                            FieldsManager fm = doc.getDocumentKind().getFieldsManager(doc.getId());
                            Crm crm = new Crm();
                            crm.setCrmStatus("DS");                            
                            crm.setDocId(doc.getId().intValue());
                            crm.setActivityId(task.getActivityKey());
                            crm.setActivityKind(task.getProcess().equals("Obieg r�czny") ? 0 : 1);
                            crm.setDocumentDate(doc.getCtime());
                            crm.setEntryDate(new Date());
                            crm.setExportDate(task.getReceiveDate());
                            crm.setFromUser(task.getActivity_lastActivityUser());
                            if (doc.getOfficeNumber()!= null)
                                crm.setOfficeNumber(doc.getOfficeNumber().toString());
                            if (task.getDocumentType().equals("in"))
                                crm.setOfficeType("incoming");
                            else if (task.getDocumentType().equals("out"))
                                crm.setOfficeType("outcoming");
                            else
                                crm.setOfficeType("internal");
                            if("fax".equals(doc.getSource()))
                                crm.setSposobDostarczenia("faks");
                            else
                                crm.setSposobDostarczenia("list");                   
                            crm.create(oracleCon);                            
                            log.info("Zapisal do bazy Agenta dokument o nr.ko = "+doc.getOfficeNumber());
                            
                            log.trace("Zadanie mozna zakonczyc = " + canFinish(doc,activity));
                            if(!canFinish(doc,activity))
                            {
                                Remark remark = new Remark("Brak mo�liwo�ci zako�czenia procesu, zadanie przekazano do CRM","AGENT");
                                doc.addRemark(remark);
                                log.trace("Zadanie jest w procesie task.getProcess = "+ task.getProcess());
                                if(wff.isManual(activity))
                                {
                                    removeAssign(task);
                                    log.trace("Zadanie znajduje sie w obiegu recznym. Zosta�o zwrocone nadawcy");
                                }
                                else
                                {
                                    doc.addAssignmentHistoryEntry(new AssignmentHistoryEntry(
                                        "AGENT", sm.getString("doWiadomosci"), AssignmentHistoryEntry.PROCESS_TYPE_CC));
                                    wff.manualFinish(activityId, false);
                                    log.trace("Zadanie do wiadomosci. Zakonczono zadanie");
                                }
                                    
                                
                                
                            }
                            else {
                            	WorkflowFactory.getInstance().manualFinish(activityId, true);
                            }
                            //ustawienie statusu
                            Map statusMap = new HashMap<String,Object>();
                            doc.getDocumentKind().setWithHistory(doc.getId(),statusMap,false);
                        }                   
                        else if (wff.isManual(activity))
                        {
                            log.trace("Zadanie nie dotyczy dokumentu nationwide i jest w obiegu recznym. Zwracam dekretacje");
                            removeAssign(task);
                        }
                        else
                        {

                            doc.addAssignmentHistoryEntry(new AssignmentHistoryEntry(
                                "AGENT", sm.getString("doWiadomosci"), AssignmentHistoryEntry.PROCESS_TYPE_CC));
                            wff.manualFinish(activityId, false);
                            log.trace("Zadanie nie dotyczy dokumentu nationwide i jest do wiadomosci. Koncze prace");
                        }
                        TaskSnapshot.updateAllTasksByDocumentId(task.getDocumentId(),task.getDocumentType());
                        log.trace("Aktualizuje liste zadan");
                    }
                }
                DSApi.context().commit();
                
              }
            catch (UserNotFoundException e)
            {
                try {DSApi.context().rollback();} catch (EdmException e1) {log.error("",e1);}
                log.trace("Nie znaleziono uzytkownika Agenta",e);
                log.error(e.getStackTrace());
                
            }
            catch (Exception e)
            {
                try {DSApi.context().rollback();} catch (EdmException e1) {log.error("",e1);}
                log.trace("Blad podczas pracy Agenta",e);
                log.error("",e);
            }
            finally
            {
                try {DSApi.close(); } catch (EdmException e1) {log.error("",e1);}
                disconnectOracle(oracleCon);
            }
        }   



   

	public static String getEntityName() {
		return entityName;
	}

	public static void setEntityName(String entityName) {
		CrmDriver.entityName = entityName;
	}

}
