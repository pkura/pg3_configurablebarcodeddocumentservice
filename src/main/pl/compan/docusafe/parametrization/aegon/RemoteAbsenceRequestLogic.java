package pl.compan.docusafe.parametrization.aegon;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.axis2.addressing.EndpointReference;
import org.jfree.util.Log;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.absences.Absence;
import pl.compan.docusafe.core.base.absences.AbsenceType;
import pl.compan.docusafe.core.base.absences.EmployeeAbsenceEntry;
import pl.compan.docusafe.core.base.absences.FreeDay;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.UserImpl;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.client.absences.CardServiceStub;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.AbsenceEntry;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.CardRequest;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.CardResponse;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.ChargeAbsence;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.ChargeAbsenceRequest;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.ChargeAbsenceResponse;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.ChargeAbsenceResponseE;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.FreeDaysResponse;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.GetCard;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.GetCardResponse;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.GetFreeDaysResponse;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.UnchargeLastAbsence;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.UnchargeLastAbsenceRequest;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.UnchargeLastAbsenceResponse;
import pl.compan.docusafe.ws.client.absences.CardServiceStub.UnchargeLastAbsenceResponseE;

public class RemoteAbsenceRequestLogic extends AbstractAbsenceRequestLogic 
{	
	@Override
	public void chargeAbsence(EmployeeCard empCard, Date from, Date to,
			String type, String info, int numDays, Long documentId) throws EdmException 
	{
		try
		{
			CardServiceStub cardServiceStub = new CardServiceStub();
			cardServiceStub._getServiceClient().getOptions().setTo(
					new EndpointReference(Docusafe.getAdditionProperty("cardService.endPointReference")));
			
			// ustawienie parametrow requesta
			ChargeAbsenceRequest absenceRequest = new ChargeAbsenceRequest();
			absenceRequest.setEmpCardId(empCard.getId());
			absenceRequest.setEditorName(DSApi.context().getDSUser().asLastnameFirstname());
			
			Calendar dateFrom = Calendar.getInstance();
			dateFrom.setTime(from);
			absenceRequest.setDateFrom(dateFrom);
			
			Calendar dateTo = Calendar.getInstance();
			dateTo.setTime(to);
			absenceRequest.setDateTo(dateTo);
			
			absenceRequest.setAbsenceTpe(type);
			absenceRequest.setInfo(info);
			absenceRequest.setDaysNum(numDays);
			
			absenceRequest.setDocumentId(documentId == null ? 0 : documentId);
			
			// request
			ChargeAbsence chargeAbsence = new ChargeAbsence();
			chargeAbsence.setRequest(absenceRequest);
			
			// response
			ChargeAbsenceResponseE absenceResponse = cardServiceStub.chargeAbsence(chargeAbsence);
			ChargeAbsenceResponse response = absenceResponse.get_return();
			
			if (response.getStatus() == -1)
				throw new EdmException(response.getError());
		}
		catch (Exception ex)
		{
			Log.error(ex.getMessage(), ex);
			throw new EdmException(ex.getMessage());
		}
	}
	
	@Override
	public Long getAbsenceSumDaysFromCurrentYear(EmployeeCard empCard,
			String type) throws EdmException 
	{
		int currentYear = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
		Long sumDays = 0l;
		for (Absence absence : empCard.getAbsences())
		{
			if (absence.getYear() == currentYear && absence.getAbsenceType().getCode().equals(type))
			{
				sumDays += absence.getDaysNum();
			}
		}
		
		return sumDays;
	}
	
	@Override
	public EmployeeCard getActiveEmployeeCardByUserName(String userName)
			throws EdmException 
	{
		try
		{
			// pobranie karty pracownika z webservicu
			CardServiceStub cardServiceStub = new CardServiceStub();
			cardServiceStub._getServiceClient().getOptions().setTo(
					new EndpointReference(Docusafe.getAdditionProperty("cardService.endPointReference")));
			
			CardRequest cardRequest = new CardRequest();
			cardRequest.setUserName(userName);
			
			GetCard getCard = new GetCard();
			getCard.setRequest(cardRequest);
			
			GetCardResponse cardResponse = cardServiceStub.getCard(getCard);
			CardResponse response = cardResponse.get_return();
			
			if (response.getStatus() == -1)
				throw new EdmException(response.getError());
			
			EmployeeCard card = new EmployeeCard();
			card.setId(response.getId());
			card.setKPX(response.getKPX());
			card.setCompany(response.getCompany());
			card.setDepartment(response.getDepartment());
			card.setDivision(response.getDivision());
			
			if (response.getEmploymentStartDate() != null)
				card.setEmploymentStartDate(response.getEmploymentStartDate().getTime());
			
			if (response.getEmploymentEndDate() != null)
				card.setEmploymentEndDate(response.getEmploymentEndDate().getTime());
			
			card.setExternalUser(response.getExternalUser());
			card.setInfo(response.getInfo());
			card.setInvalidAbsenceDaysNum(response.getInvalidAbsenceDaysNum());
			card.setPosition(response.getPosition());
			card.setT188KP(response.getT188KP());
			card.setT188kpDaysNum(response.getT188KpDaysNum());
			card.setTINW(response.getTINW());
			card.setTUJ(response.getTUJ());
			card.setTujDaysNum(response.getTujDaysNum());
			//card.setUser(userImpl);
			
			Set<Absence> absences = new LinkedHashSet<Absence>();
			
			for (AbsenceEntry entry : response.getAbsences())
			{
				Absence absence = new Absence();
				
				AbsenceType type = new AbsenceType();
				type.setCode(entry.getAbsenceType());
				
				absence.setAbsenceType(type);
				
				if (entry.getCtime() != null)
					absence.setCtime(entry.getCtime().getTime());
				
				absence.setDaysNum(entry.getDaysNum());
				
				if (entry.getEditor() != null)
					absence.setEditor((UserImpl) DSUser.findByUsername(entry.getEditor()));
				
				absence.setEmployeeCard(card);
				
				if (entry.getEndDate() != null)
					absence.setEndDate(entry.getEndDate().getTime());
				
				if (entry.getMtime() != null)
					absence.setMtime(entry.getMtime().getTime());
				
				if (entry.getStartDate() != null)
					absence.setStartDate(entry.getStartDate().getTime());
				
				absence.setYear(entry.getYear());
				
				absences.add(absence);
			}
			
			
			card.setAbsences(absences);
			
			return card;
		}
		catch (Exception ex)
		{
			Log.error(ex.getMessage(), ex);
			throw new EdmException(ex.getMessage());
		}
	}
	
	@Override
	public int getAvailableDaysForCurrentYear(EmployeeCard empCard)
			throws EdmException 
	{
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		int sumDays = 0;
		for (Absence absence : empCard.getAbsences())
		{
			if (absence.getYear() != currentYear)
				continue;
			
			// za�adowanie typu urlopu ze wszystkimi warto�ciami
			absence.setAbsenceType(Finder.find(AbsenceType.class, absence.getAbsenceType().getCode()));
			
			if (absence.getAbsenceType().isAdditional())
			{
				sumDays += absence.getDaysNum();
			}
			if (absence.getAbsenceType().isNegative())
			{
				sumDays -= absence.getDaysNum();
			}
		}
		
		if (empCard.getTINW() != null && empCard.getTINW())
			sumDays += empCard.getInvalidAbsenceDaysNum();
		
		return sumDays;
	}
	
	@Override
	public List<EmployeeAbsenceEntry> getEmployeeAbsences(EmployeeCard empCard,
			int currentYear) throws EdmException 
	{
		List<EmployeeAbsenceEntry> absenceEntries = new ArrayList<EmployeeAbsenceEntry>();
		int sumDays = 0;
		
		// utworzenie listy urlop�w z liczb� porz�dkow� i obliczeniami pozosta�ych dni urlopu
		int i = 1;
		
		if (empCard.getTINW() != null && empCard.getTINW())
		{
			Absence absence = new Absence();
			absence.setAbsenceType(new AbsenceType("INW", "Urlop nale�ny Inwalidzki", "", 2));
			
			Calendar start = Calendar.getInstance();
			start.set(currentYear, 0, 1);
			Calendar end = Calendar.getInstance();
			end.set(currentYear, 11, 31);
			absence.setStartDate(start.getTime());
			absence.setEndDate(end.getTime());
			
			absence.setDaysNum(empCard.getInvalidAbsenceDaysNum());
			absence.setInfo("Nadany urlop nale�ny inwalidzki");
			
			EmployeeAbsenceEntry absenceEntry = new EmployeeAbsenceEntry();
			absenceEntry.setAbsence(absence);
			absenceEntry.setLp(i++);
			
			sumDays += absence.getDaysNum();
			absenceEntry.setRestOfDays(sumDays);
			
			absenceEntries.add(absenceEntry);
		}
		
		for (Absence absence : empCard.getAbsences())
		{
			if (absence.getYear() != currentYear)
				continue;
			
			// za�adowanie typu urlopu ze wszystkimi warto�ciami
			absence.setAbsenceType(Finder.find(AbsenceType.class, absence.getAbsenceType().getCode()));
			
			EmployeeAbsenceEntry absenceEntry = new EmployeeAbsenceEntry();
			absenceEntry.setLp(i++);
			absenceEntry.setAbsence(absence);
			if (absence.getAbsenceType().isAdditional())
			{
				sumDays += absence.getDaysNum();
			}
			else if (absence.getAbsenceType().isNegative())
			{
				sumDays -= absence.getDaysNum();
			}
			absenceEntry.setRestOfDays(sumDays);
			absenceEntries.add(absenceEntry);
		}
		
		return absenceEntries;
	}
	
	@Override
	public List<FreeDay> getFreeDays() throws EdmException 
	{
		try
		{
			CardServiceStub cardServiceStub = new CardServiceStub();
			cardServiceStub._getServiceClient().getOptions().setTo(
					new EndpointReference(Docusafe.getAdditionProperty("cardService.endPointReference")));
			
			GetFreeDaysResponse daysResponse = cardServiceStub.getFreeDays();
			FreeDaysResponse response = daysResponse.get_return();
			
			List<FreeDay> freeDays = new ArrayList<FreeDay>();
			for (Calendar calendar : response.getFreeDays())
				freeDays.add(new FreeDay(calendar.getTime(), null));
			
			return freeDays;
		}
		catch (Exception ex)
		{
			Log.error(ex.getMessage(), ex);
			throw new EdmException(ex.getMessage());
		}
	}
	
	@Override
	public boolean hasAbsenceBetween(EmployeeCard empCard, Date startDate,
			Date endDate) throws EdmException 
	{
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		for (Absence absence : empCard.getAbsences())
		{
			LoggerFactory.getLogger("kamilj").debug("ABS EMPLOYEE: " + absence.getEmployeeCard().getExternalUser() + ", " + absence.getId() + 
					", " + absence.getYear() + ", " + absence.getDaysNum() + ", " + absence.getAbsenceType().getCode() + ", " + 
					absence.getStartDate() + ", " + absence.getEndDate() + ", " + absence.getDocumentId());
			// pomijamy urlopy z innych lat lub urlopy nale�ne, dodatkowe
			if (currentYear != absence.getYear() || absence.getAbsenceType().getCode().startsWith("WU"))
				continue;
			
			LoggerFactory.getLogger("kamilj").debug("ABS EMPLOYEE: " + absence.getEmployeeCard().getExternalUser() + ", " + absence.getId());
			
			if ((startDate.after(absence.getStartDate()) && startDate.before(absence.getEndDate())))
				return true;
			else if (startDate.equals(absence.getStartDate()) || startDate.equals(absence.getEndDate()))
				return true;
			else if ((endDate.after(absence.getStartDate()) && endDate.before(absence.getEndDate())))
				return true;
			else if (endDate.equals(absence.getStartDate()) || endDate.equals(absence.getEndDate()))
				return true;
		}
		
		return false;
	}
	
	@Override
	public void unchargeLastAbsences(EmployeeCard empCard, Long documentId) throws EdmException 
	{
		try
		{
			CardServiceStub cardServiceStub = new CardServiceStub();
			cardServiceStub._getServiceClient().getOptions().setTo(
					new EndpointReference(Docusafe.getAdditionProperty("cardService.endPointReference")));
			
			// ustawienie parametrow requesta
			UnchargeLastAbsenceRequest request = new UnchargeLastAbsenceRequest();
			request.setEmpCardId(empCard.getId());
			request.setDocumentId(documentId == null ? 0 : documentId);
			
			// request
			UnchargeLastAbsence unchargeLastAbsence = new UnchargeLastAbsence();
			unchargeLastAbsence.setRequest(request);
			
			// response
			UnchargeLastAbsenceResponseE absenceResponseE = cardServiceStub.unchargeLastAbsence(unchargeLastAbsence);
			UnchargeLastAbsenceResponse absenceResponse = absenceResponseE.get_return();
			
			if (absenceResponse.getStatus() == -1)
				throw new EdmException(absenceResponse.getError());
		}
		catch (Exception ex)
		{
			Log.error(ex.getMessage(), ex);
			throw new EdmException(ex.getMessage());
		}
	}
}
