package pl.compan.docusafe.parametrization.swd;

import org.directwebremoting.WebContextFactory;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.LinkValue;
import pl.compan.docusafe.web.jsp.component.JspComponentTable;
import pl.compan.docusafe.web.swd.management.utils.LinkCreator;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class SwdOrganisationUnitDictionaryLogic extends DwrDictionaryBase {

//    @Override
//    public Map<String, Object> getValues(String id, Map<String, Object> fieldValues) throws EdmException {
//        Map<String, Object> values = super.getValues(id, fieldValues);
//        values.put(getName() + "_JSPLINK", new LinkValue("Tytul linku " + id, "htmlgogo"));
//
//        return values;
//    }

    private static LinkValue createLink(Long documentId, Long swdOrganisationUnitId) {
        return new LinkValue("Przejd�", new LinkCreator("/docusafe/swd/management/organisation-unit.action")
                .setLayout(JspComponentTable.Layout.POPUP)
                .add("id", swdOrganisationUnitId)
                .add("documentId", documentId)
                .create());
    }

    @Override
    public void filterAfterGetValues(Map<String, Object> dictionaryFilteredFieldsValues, String dicitonaryEntryId, Map<String, Object> fieldsValues, Map<String, Object> documentValues) {
        super.filterAfterGetValues(dictionaryFilteredFieldsValues, dicitonaryEntryId, fieldsValues, documentValues);

        Long documentId = getDocumentIdFromDwrSession();
        if (documentId != null) {
            Long organisationUnitEntryId = (Long) fieldsValues.get("SWD_ORGANISATION_UNIT");
            if (organisationUnitEntryId != null) {
                SwdOrganisationUnitLinkField.OrganisationUnitEntry organisationUnitEntry = SwdOrganisationUnitLinkField.findOrganisationUnitEntry(organisationUnitEntryId);

                if (dictionaryFilteredFieldsValues == null)
                    dictionaryFilteredFieldsValues = new HashMap<String, Object>();
                dictionaryFilteredFieldsValues.put("SWD_ORGANISATION_UNIT_JSPLINK", createLink(documentId, organisationUnitEntry.swdId));
            }
        }
    }

    private static Long getDocumentIdFromDwrSession() {
        Map<String, Object> mapaVal = ((Map<String, Object>) WebContextFactory.get().getSession().getAttribute("dwrSession"));
        if (mapaVal == null) return null;
        Object documentId = mapaVal.get("documentId");
        if (documentId == null) return null;
        return Long.parseLong(documentId.toString());
    }
}
