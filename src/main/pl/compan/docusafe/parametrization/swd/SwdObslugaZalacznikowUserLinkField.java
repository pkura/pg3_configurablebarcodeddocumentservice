package pl.compan.docusafe.parametrization.swd;

import pl.compan.docusafe.web.swd.management.utils.LinkCreator;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class SwdObslugaZalacznikowUserLinkField extends SwdUserLinkField {

    @Override
    protected LinkCreator getLinkCreator(Long documentId, UserEntry entry) {
        return getLinkCreatorWithDictCn(documentId, entry, SwdObslugaZalacznikowLogic.TABLE_SWD_OBSLUGA_ZALACZNIKOW_MULTIPLE_VALUE);
    }

    protected LinkCreator getLinkCreatorWithDictCn(Long documentId, UserEntry entry, String documentMultipleValueTableName) {
        Long dictionaryEntryId = entry.id;
        LinkCreator linkCreator = super.getLinkCreator(documentId, entry);
        addDictionaryCnToLink(linkCreator, documentMultipleValueTableName, dictionaryEntryId);
        return linkCreator;
    }

    protected void addDictionaryCnToLink(LinkCreator linkCreator, String documentMultipleValueTableName, Long dictionaryEntryId) {
        String dictCn = findDictionaryCn(documentMultipleValueTableName, dictionaryEntryId);
        linkCreator.add("dictCn", dictCn);
    }
}
