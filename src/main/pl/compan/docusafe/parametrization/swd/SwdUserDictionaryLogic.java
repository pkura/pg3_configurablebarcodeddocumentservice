package pl.compan.docusafe.parametrization.swd;

import org.directwebremoting.WebContextFactory;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.LinkValue;
import pl.compan.docusafe.web.jsp.component.JspComponentTable;
import pl.compan.docusafe.web.swd.management.utils.LinkCreator;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Damian on 20.01.14.
 */
public class SwdUserDictionaryLogic extends DwrDictionaryBase {

    protected static LinkValue createLink(Long documentId, Long ouId, Map<String, Object> fieldsValues) {
        String label = null;
        LinkCreator linkCreator = new LinkCreator("/docusafe/swd/management/user-view.action")
                                            .setLayout(JspComponentTable.Layout.POPUP)
                                            .add("id", ouId)
                                            .add("documentId", documentId);


        return new LinkValue(label,linkCreator.create());
    }

    @Override
    public void filterAfterGetValues(Map<String, Object> dictionaryFilteredFieldsValues, String dicitonaryEntryId, Map<String, Object> fieldsValues, Map<String, Object> documentValues) {
        super.filterAfterGetValues(dictionaryFilteredFieldsValues, dicitonaryEntryId, fieldsValues, documentValues);

        Long documentId = getDocumentIdFromDwrSession();
        if (documentId != null) {
            Long userEntryId = (Long) fieldsValues.get(getName());

            if (userEntryId != null) {
                SwdUserLinkField.UserEntry userEntry = SwdUserLinkField.findUserEntry(userEntryId);

                if (dictionaryFilteredFieldsValues == null)
                    dictionaryFilteredFieldsValues = new HashMap<String, Object>();
                dictionaryFilteredFieldsValues.put("SWD_USER_JSPLINK", createLink(documentId, userEntry.swdId, fieldsValues));
            }
        }
    }



    private static Long getDocumentIdFromDwrSession() {
        Map<String, Object> mapaVal = ((Map<String, Object>) WebContextFactory.get().getSession().getAttribute("dwrSession"));
        if (mapaVal == null) return null;
        Object documentId = mapaVal.get("documentId");
        if (documentId == null) return null;
        return Long.parseLong(documentId.toString());
    }

}
