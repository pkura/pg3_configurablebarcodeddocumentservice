package pl.compan.docusafe.parametrization.swd;

import edu.emory.mathcs.backport.java.util.Arrays;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.db.criteria.NativeProjection;
import pl.compan.docusafe.core.dockinds.field.LinkFieldLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public abstract class DictionaryLinkFieldLogic<T> implements LinkFieldLogic {

    private static Logger log = LoggerFactory.getLogger(DictionaryLinkFieldLogic.class);

    protected static Long convertToLong (String sId){
        Pattern pattern = Pattern.compile("\\d+");
        if (!pattern.matcher(sId).find()) return null;

        Long lId = Long.parseLong(sId);
        return lId;
    }

    protected static String findDictionaryCn(String documentMultipleValueTableName, String sDictionaryEntryId) {
        Long dictEntryId = convertToLong(sDictionaryEntryId);
        if (dictEntryId == null) return null;
        return findDictionaryCn(documentMultipleValueTableName, dictEntryId);
    }
    protected static String findDictionaryCn(String documentMultipleValueTableName, Long dictionaryEntryId) {
        final String FIELD_CN = "FIELD_CN";
        final String FIELD_VAL = "FIELD_VAL";
        String res = null;
        try {
            NativeCriteria nc = DSApi.context().createNativeCriteria(documentMultipleValueTableName, "alias");
            nc.setProjection(new NativeProjection().addProjection(Arrays.asList(new String []{FIELD_CN,FIELD_VAL})))
                    .add(NativeExps.eq("alias.FIELD_VAL", dictionaryEntryId));
            CriteriaResult cr = nc.criteriaResult();

            if (cr.next()) res = cr.getString(FIELD_CN, null);
            if (cr.next()) throw new IllegalArgumentException("Liczba wynik�w zapytania > 1");
            if (res == null)  throw new IllegalArgumentException("Nie odnaleziono w tabeli ["+documentMultipleValueTableName+"] CN s�ownika dla id wpisu="+dictionaryEntryId);
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
        return res;
    }

    protected static <T> T findDictionaryEntry(String dictionaryTableName, String sDictionaryEntryId, Creator<T> creator) {
        Long dictEntryId = convertToLong(sDictionaryEntryId);
        if (dictEntryId == null) return null;
        return findDictionaryEntry(dictionaryTableName, dictEntryId, creator);
    }
    protected static <T> T findDictionaryEntry(String dictionaryTableName, Long dictionaryEntryId, Creator<T> creator) {
        T res = null;
        try {
            NativeCriteria nc = DSApi.context().createNativeCriteria(dictionaryTableName, "alias");
            nc.setProjection(new NativeProjection().addProjection(creator.getColumns()))
                    .add(NativeExps.eq("alias.id", dictionaryEntryId));
            CriteriaResult cr = nc.criteriaResult();

            if (cr.next()) res = creator.createObject(cr);
            if (cr.next()) throw new IllegalArgumentException("Liczba wynik�w zapytania > 1");
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
        return res;
    }

    @Override
    public String getLinkLabel() {
        return "Otw�rz";
    }

    @Override
    public String createLink(String sDictionaryEntryId) {
        Long documentId = getDocumentIdFromDwrSession();
        T entry = findEntry(sDictionaryEntryId);
        String link = createLink(documentId, entry);
        return link;
    }

    protected abstract String createLink(Long documentId, T entry);

    protected abstract T findEntry(String id);

    protected static Long getDocumentIdFromDwrSession() {

        WebContext webContext = WebContextFactory.get();
        if(webContext != null){
            Map<String, Object> mapaVal = ((Map<String, Object>) webContext.getSession().getAttribute("dwrSession"));
            if (mapaVal != null){
                Object documentId = mapaVal.get("documentId");
                if (documentId != null){
                    return Long.parseLong(documentId.toString());
                }
            }
        }
        return null;
    }

    protected abstract static interface Creator<T> {

        public abstract T createObject(CriteriaResult cr);

        public abstract List<String> getColumns();
    }
}
