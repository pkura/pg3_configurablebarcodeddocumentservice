package pl.compan.docusafe.parametrization.swd;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.dictionary.DictionaryUtils;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class MultipleDictionaryFactory {

    public static void deleteEntry(Long documentId, String multipleTableName, String dictCn, Long dictId) throws EdmException {
        deleteEntryFromMultipleTable(documentId, multipleTableName, dictCn, dictId);
        deleteEntryFromMultipleTable(dictCn, dictId);
    }

    public static void deleteEntryFromMultipleTable(Long documentId, String multipleTableName, String dictCn, Long dictId) throws EdmException {
        String ins = "delete from " + multipleTableName + " where document_id=? and field_cn=? and field_val=?";

        PreparedStatement ps = null;
        try {
            ps = DSApi.context().prepareStatement(ins);
            ps.setLong(1, documentId);
            ps.setString(2, dictCn);
            ps.setString(3, dictId.toString());
            ps.execute();
            DSApi.context().closeStatement(ps);
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        } finally {
            DSApi.context().closeStatement(ps);
        }
    }

    public static void deleteEntryFromMultipleTable(String dictCn, Long dictId) throws EdmException {
        DwrDictionaryBase dwrDictionaryBase = DwrDictionaryFacade.dictionarieObjects.get(dictCn);
        String tableName = ((DictionaryField) dwrDictionaryBase.getOldField()).getTable();
        String ins = "delete from " + tableName + " where id=?";

        PreparedStatement ps = null;
        try {
            ps = DSApi.context().prepareStatement(ins);
            ps.setLong(1, dictId);
            ps.execute();
            DSApi.context().closeStatement(ps);
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        } finally {
            DSApi.context().closeStatement(ps);
        }
    }

//    public static void deleteEntriesFromMultipleTable(String dictCn, Long[] dictIds) throws EdmException {
//        if (dictIds.length == 0) return;
//
//        DwrDictionaryBase dwrDictionaryBase = DwrDictionaryFacade.dictionarieObjects.get(dictCn);
//        String tableName = ((DictionaryField) dwrDictionaryBase.getOldField()).getTable();
//        StringBuilder del = new StringBuilder();
//        del.append("delete from ").append(tableName).append(" where id in (");
//        del.append("?");
//        for (int i=2; i <= dictIds.length; ++i)
//            del.append(",?");
//        del.append(',');
//        PreparedStatement ps = null;
//        try {
//            ps = DSApi.context().prepareStatement(del);
//            for (int i = 1; i <= dictIds.length; ++i)
//                ps.setLong(i, dictIds[i]);
//            ps.execute();
//            DSApi.context().closeStatement(ps);
//        } catch (SQLException e) {
//            throw new EdmSQLException(e);
//        } catch (HibernateException e) {
//            throw new EdmHibernateException(e);
//        } finally {
//            DSApi.context().closeStatement(ps);
//        }
//    }

    public static long saveEntry(Document document, String dictCn, Map<String, FieldData> valuesdic) throws EdmException {
        DwrDictionaryBase dwrDictionaryBase = DwrDictionaryFacade.dictionarieObjects.get(dictCn);
        //String tableName = ((DictionaryField) dwrDictionaryBase.getOldField()).getTable();
        long id = dwrDictionaryBase.add(valuesdic);
        DictionaryUtils.addToMultipleTable(document, dictCn, Long.toString(id));
        return id;
    }
    public static long updateEntry(Document document, String dictCn, Map<String, FieldData> valuesdic) throws EdmException {
        DwrDictionaryBase dwrDictionaryBase = DwrDictionaryFacade.dictionarieObjects.get(dictCn);
        //String tableName = ((DictionaryField) dwrDictionaryBase.getOldField()).getTable();
        long id = dwrDictionaryBase.update(valuesdic);
        return id;
    }

    public static <T> List<T> getEntries(SqlManager<T> sqlManager) throws EdmException, SQLException {
        List<T> entrires = new ArrayList<T>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Collection<String> columns = sqlManager.getColumns();
            ps = DSApi.context().prepareStatement("SELECT " + StringUtils.join(columns.toArray(new String[columns.size()]), ", ") + " FROM " + sqlManager.multipleDictTable + "  dictionary " +
                    " INNER JOIN " + sqlManager.documentMultipleValueTable + "  multiples " +
                    " ON dictionary.ID = multiples.FIELD_VAL " +
                    " AND multiples.FIELD_CN = ? " +
                    " AND multiples.DOCUMENT_ID = ? ");
            ps.setString(1, sqlManager.dictCn);
            ps.setLong(2, sqlManager.docId);

            rs = ps.executeQuery();

            while (rs.next()) {
                T entry = sqlManager.createObject(rs);
                entrires.add(entry);
            }

            return entrires; // brak wpis�w

        } catch (EdmException e) {
            throw e;
        } catch (SQLException e) {
            throw e;
        } finally {
            SwdObslugaZalacznikowDictionaryUpdater.finallyCloseAll(ps, rs);
        }
    }

    public static abstract class SqlManager<T> {
        Long docId;
        Object documentMultipleValueTable;
        Object multipleDictTable;
        String dictCn;

        protected SqlManager(Document document, Object multipleDictTable, String dictCn) {
            this(document.getId(),document.getDocumentKind().getMultipleTableName(),multipleDictTable, dictCn);
        }

        protected SqlManager(Long docId, Object documentMultipleValueTable, Object multipleDictTable, String dictCn) {
            this.docId = docId;
            this.documentMultipleValueTable = documentMultipleValueTable;
            this.multipleDictTable = multipleDictTable;
            this.dictCn = dictCn;
        }

//        protected SqlManager(Object documentMultipleValueTable, Object multipleDictTable) {
//            this(null, documentMultipleValueTable, multipleDictTable, null);
//        }

        public abstract T createObject(ResultSet rs) throws SQLException;

        public abstract Collection<String> getColumns();

        public void setDocId(Long docId) {
            this.docId = docId;
        }

        public void setDictCn(String dictCn) {
            this.dictCn = dictCn;
        }

        public void reset() {
            init(null, null);
        }

        public SqlManager<T> init(Long docId, String dictCn) {
            setDocId(docId);
            setDictCn(dictCn);
            return this;
        }
    }
}
