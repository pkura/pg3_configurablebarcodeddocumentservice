package pl.compan.docusafe.parametrization.swd;

import org.jbpm.api.listener.EventListenerExecution;
import org.jbpm.api.task.Participation;
import org.jbpm.api.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.*;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.service.mail.MailerDriver;
import pl.compan.docusafe.util.DateUtils;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;


public class SendMultiReceiverMailListener extends AbstractEventListener {
    private static final Logger log = LoggerFactory.getLogger(SendMailListener.class);


    private String mailFromLogicCn;
    private String mail;
    private String template;


    @Override
    public void notify(EventListenerExecution execution) throws Exception {
        boolean wasOpened = true;
        try {
            wasOpened = DSApi.isContextOpen();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return;
        }
        Long docId = Jbpm4Utils.getDocumentId(execution);
        try {
            if (!wasOpened)
                DSApi.openAdmin();

            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();



            if (template == null) {
                log.error("Nie podano szablonu maila!");
                throw new EdmException("Nie podano szablonu maila!");
            }
            if (ServiceManager.getService(Mailer.NAME) == null)
                return;
            MailerDriver mailer = (MailerDriver) ServiceManager.getService(Mailer.NAME);

            getMailFromDocumentLogic(doc);
            if (mail != null) {
                String[] mailArr = mail.split(";");
                for(String toMail : mailArr){
                    mailer.send(toMail, toMail, null, Configuration.getMail(template), prepareContext(doc.getFieldsManager(), null, execution, docId), false);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }finally {
            if (DSApi.isContextOpen() && !wasOpened)
                DSApi.close();
        }

    }


    private Map<String, Object> prepareContext(FieldsManager fm, DSUser user, EventListenerExecution execution, Long docId) throws UserNotFoundException, EdmException
    {
        Map<String, Object> ctext = new HashMap<String, Object>();
        if (user != null)
        {
            ctext.put("first-name", user.getFirstname());
            ctext.put("last-name", user.getLastname());
        }
        ctext.put("document-name", fm.getDocumentKind().getName());
        ctext.put("document-id", fm.getDocumentId());
        ctext.put("document-state", fm.getEnumItem("STATUS") != null ? fm.getEnumItem("STATUS").getTitle() : "");
        Document doc = Document.find(docId);

        if (doc instanceof OfficeDocument)
        {
            ctext.put("document-number", ((OfficeDocument)doc).getOfficeNumber());
            ctext.put("document-ko", ((OfficeDocument)doc).getOfficeNumber());
        }
        fm.getDocumentKind().logic().addSpecialValueForProcessMailNotifier(fm, ctext);

        for (String taskId : Jbpm4ProcessLocator.taskIds(docId))
        {
            Task task = Jbpm4Provider.getInstance().getTaskService().getTask(taskId);
            String executionId = execution.getId().split(".to")[0];
            if (task.getExecutionId().contains(executionId))
            {
                if (task.getAssignee() != null)
                    ctext.put("document-assignee", DSUser.findByUsername(task.getAssignee()).getWholeName());
                else
                {
                    for (Participation part : Jbpm4Provider.getInstance().getTaskService().getTaskParticipations(taskId))
                    {
                        StringBuilder zadekretowani = new StringBuilder();
                        if (part.getUserId() != null) {
                            ctext.put("document-assignee", DSUser.findByUsername(part.getUserId()).getWholeName());
                        } else {
                            for (DSUser juser : DSDivision.find(part.getGroupId()).getUsers()) {
                                zadekretowani.append(juser.getWholeName() + ", ");
                            }
                            ctext.put("document-assignee", zadekretowani);
                        }
                    }
                }
            }
        }
        for (String taskId : Jbpm4ProcessLocator.taskIds(fm.getDocumentId()))
        {
            Task task = Jbpm4Provider.getInstance().getTaskService().getTask(taskId);
            if (execution.getId().equals(task.getExecutionId()))
            {
                GregorianCalendar gregorianCalendar = new GregorianCalendar();
                gregorianCalendar.setTime(task.getCreateTime());

                ctext.put("document-deadline", gregorianCalendar.getTime().toLocaleString());
            }
        }
        Map<String, Object> values = fm.getFieldValues();

        /**
         * Dodanie do szablonu wszystkich warto�ci pol na dokumencie
         */
        for (String cn : fm.getFieldsCns())
        {
            Object value = fm.getValue(cn);
            if (value instanceof Date)
            {
                String date = DateUtils.formatCommonDate((Date) value);
                ctext.put(cn, date);
            }
            else
                ctext.put(cn, value);
        }

        return ctext;
    }

    /**
     * Wyciaga maile na jakie ma zosta� wyslane z logiki dokumentu (wiele adres�w trzeba oddzieli� ";")
     * @param document
     */
    public void getMailFromDocumentLogic(OfficeDocument document)
    {
        if(mailFromLogicCn != null)
        {
            mail = document.getDocumentKind().logic().getMailForJbpmMailListener(document, mailFromLogicCn);
        }
    }
}
