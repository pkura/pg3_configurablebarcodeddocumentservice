package pl.compan.docusafe.parametrization.swd;

import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.swd.management.utils.LinkCreator;

import java.util.List;


public class SwdUserLinkField extends DictionaryLinkFieldLogic<SwdUserLinkField.UserEntry> {

    private static Logger log = LoggerFactory.getLogger(SwdUserLinkField.class);

    public static UserEntry findUserEntry(Long id) {
        return findDictionaryEntry("DSG_SWD_USER", id, new UserCreator());
    }

    public static UserEntry findUserEntry(String tableName, Long id) {
        return findDictionaryEntry(tableName, id, new UserCreator());
    }

    @Override
    protected String createLink(Long documentId, SwdUserLinkField.UserEntry entry){
        LinkCreator linkCreator = getLinkCreator(documentId, entry);
        return linkCreator.create();
    }
    protected  LinkCreator getLinkCreator(Long documentId, SwdUserLinkField.UserEntry entry) {
        LinkCreator linkCreator = new LinkCreator("/docusafe/swd/management/user-view.action")
                .add("pop",true)
                .add("userId", entry.swdId)
                .add("documentId", documentId);

        return linkCreator;
    }

    @Override
    protected SwdUserLinkField.UserEntry findEntry(String id) {
        return SwdUserLinkField.findUserEntry(id);
    }

    public static UserEntry findUserEntry(String id) {
        return findDictionaryEntry("DSG_SWD_USER", id, new UserCreator());
    }

    public static class UserCreator implements Creator<UserEntry> {

        @Override
        public UserEntry createObject(CriteriaResult cr) {
            List<String> columns = getColumns();
            Long id = cr.getLong(columns.get(0), null);
            String firstName = cr.getString(columns.get(1), null);
            String lastName = cr.getString(columns.get(2), null);
            Long swdId = cr.getLong(columns.get(3), null);
            String status = cr.getString(columns.get(4), null);

            return new UserEntry(id,firstName,lastName, swdId, status);
        }

        @Override
        public List<String> getColumns() {
            return java.util.Arrays.asList(new String[]{"id", "FIRST_NAME", "LAST_NAME", "SWDUID", "STATUS_NOTE"});
        }
    }

    public static class UserEntry {
        public final Long id;
        public final String firstName;
        public final String lastName;
        public final Long swdId;
        public final String status;


        public UserEntry(Long id, String firstName, String lastName, Long swdId, String status) {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.swdId = swdId;
            this.status = status;
        }

    }
}
