package pl.compan.docusafe.parametrization.swd;

import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.web.jsp.component.permission.JspStatusFieldsPropertiesSet;
import pl.compan.docusafe.web.swd.management.jsp.properties.SwdJspPermissionManager;
import pl.compan.docusafe.web.swd.management.utils.SwdPermission;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.util.Map;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

/**
 * Created by Damian on 17.01.14.
 */
public class SwdUmowyLogic extends AbstractDocumentLogic implements SwdJspPermissionManager.FieldPropertiesGetter {
    private static final Logger log = LoggerFactory.getLogger(SwdUmowyLogic.class);


    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
        document.setFolder(folder);

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        document.setTitle(fm.getEnumItem("DOCUMENT_KIND").getTitle());
        document.setDescription(fm.getEnumItem("DOCUMENT_KIND").getTitle() + " klienta " + fm.getStringValue("NAME"));
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {}



    @Override
    public String getMailForJbpmMailListener(Document document, String mailForLogicCn) {
        return MailForJbpmHelper.getMailForJbpmMailListener(mailForLogicCn);
    }




    public void onStartProcess(OfficeDocument document,ActionEvent event)
    {
        try
        {
            Map<String, Object> map = Maps.newHashMap();
            map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public JspStatusFieldsPropertiesSet getFieldsProperties(String statusCn, String jspName) {
        return getFieldsProperties(statusCn,jspName,null);
    }

    @Override
    public JspStatusFieldsPropertiesSet getFieldsProperties(String statusCn, String jspName, String dictCn) {
        SwdPermission.JspType jspType = SwdPermission.JspType.getJspType(jspName);

        switch(jspType){
            case ORGANISATION_UNIT:
                return getFieldsPropertiesOnOrganisationUnitJsp(statusCn);
            case ORGANISATION_UNIT_LIST:
                return getFieldsPropertiesOnOrganisationUnitListJsp(statusCn);
            case PASSED_PASSWORD:
                break;
            case PASSED_PASSWORD_LIST:
                break;
            default:
                throw new IllegalArgumentException();
        }

        return null;
    }


    protected JspStatusFieldsPropertiesSet getFieldsPropertiesOnOrganisationUnitJsp(String status) {
        JspStatusFieldsPropertiesSet properties = new JspStatusFieldsPropertiesSet();

        properties.setFieldModificatorsByPermissions(SwdPermission.Permission.getPermissionByTypeForJsp(SwdPermission.PermissionType.READ, SwdPermission.JspType.ORGANISATION_UNIT));
        if("edycja_uczestnika".equalsIgnoreCase(status)){
            properties.setFieldModificatorsByPermissions(SwdPermission.Permission.OU_DANE_ORG_EDIT);
            properties.setFieldModificatorsByPermissions(SwdPermission.Permission.OU_BUTTON_SAVE_EDIT);

        }else if("nadanie_uprawnien".equalsIgnoreCase(status) || "odebranie_uprawnien".equalsIgnoreCase(status)){
            properties.setFieldModificatorsByPermissions(SwdPermission.Permission.OU_SPOSOBY_DOSTEPU_EDIT);
            properties.setFieldModificatorsByPermissions(SwdPermission.Permission.OU_BUTTON_SAVE_EDIT);
        }
        return properties;
    }

    protected JspStatusFieldsPropertiesSet getFieldsPropertiesOnOrganisationUnitListJsp(String status) {
        JspStatusFieldsPropertiesSet properties = new JspStatusFieldsPropertiesSet();

        properties.setFieldModificatorsByPermissions(SwdPermission.Permission.getPermissionByTypeForJsp(SwdPermission.PermissionType.READ, SwdPermission.JspType.ORGANISATION_UNIT_LIST));
        properties.setFieldModificatorsByPermissions(SwdPermission.Permission.getPermissionByTypeForJsp(SwdPermission.PermissionType.SHOW, SwdPermission.JspType.ORGANISATION_UNIT_LIST));

        return properties;
    }
}
