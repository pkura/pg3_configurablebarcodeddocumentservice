package pl.compan.docusafe.parametrization.swd;

import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.jsp.component.JspComponentTable;
import pl.compan.docusafe.web.swd.management.utils.LinkCreator;

import java.util.List;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class SwdOrganisationUnitLinkField extends DictionaryLinkFieldLogic<SwdOrganisationUnitLinkField.OrganisationUnitEntry> {

    private static Logger log = LoggerFactory.getLogger(SwdOrganisationUnitLinkField.class);

    public static OrganisationUnitEntry findOrganisationUnitEntry(Long id) {
        return findDictionaryEntry("DSG_SWD_ORGANISATION_UNIT", id, new OrganisationUnitCreator());
    }

    public static OrganisationUnitEntry findOrganisationUnitEntry(String tableName, Long id) {
        return findDictionaryEntry(tableName, id, new OrganisationUnitCreator());
    }

    @Override
    protected String createLink(Long documentId, SwdOrganisationUnitLinkField.OrganisationUnitEntry entry) {
        LinkCreator linkCreator = new LinkCreator("/docusafe/swd/management/organisation-unit.action")
                .setLayout(JspComponentTable.Layout.POPUP)
                .add("id", entry.swdId)
                .add("documentId", documentId);

        return linkCreator.create();
    }

    @Override
    protected SwdOrganisationUnitLinkField.OrganisationUnitEntry findEntry(String id) {
        return findOrganisationUnitEntry(id);
    }

    public static OrganisationUnitEntry findOrganisationUnitEntry(String id) {
        return findDictionaryEntry("DSG_SWD_ORGANISATION_UNIT", id, new OrganisationUnitCreator());
    }

    public static class OrganisationUnitCreator implements Creator<OrganisationUnitEntry> {

        @Override
        public OrganisationUnitEntry createObject(CriteriaResult cr) {
            Long id = cr.getLong("id", null);
            String name = cr.getString("name", null);
            Long swdId = cr.getLong("swdId", null);

            return new OrganisationUnitEntry(id, name, swdId);
        }

        @Override
        public List<String> getColumns() {
            return java.util.Arrays.asList(new String[]{"id", "name", "swdId"});
        }
    }

    public static class OrganisationUnitEntry {
        public final Long id;
        public final String name;
        public final Long swdId;

        public OrganisationUnitEntry(Long id, String name, Long swdId) {
            this.id = id;
            this.name = name;
            this.swdId = swdId;
        }
    }
}
