package pl.compan.docusafe.parametrization.swd;

import pl.compan.docusafe.core.DSContextOpener;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.jsp.component.JspPermissionManager;

import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Set;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class SwdDocumentLogicNotifier {

    private static Logger log = LoggerFactory.getLogger(SwdDocumentLogicNotifier.class);

    public static void notifyUserModified(final Long documentId, final Long userId, final String dictCode) {
        notify(documentId, new Functionality<ModifiedUser>() {
            @Override
            public void notifyBody(ModifiedUser logic, String statusCn) {
                logic.notifyUserModified(documentId, statusCn, userId, dictCode);
            }
        }, ModifiedUser.class);
    }

    public interface ModifiedUser {

        void notifyUserModified(Long documentId, String status, Long userId, String dictCode);
    }


    public static void notify(Long documentId, Functionality functionality, Class clazz) {
        if (documentId == null) return;

        DSContextOpener opener = null;
        try {
            Document document = getDocumentUnsafe(documentId);
            if (document == null) return;

            DocumentLogic logic = document.getDocumentKind().logic();
            String statusCn = JspPermissionManager.DocumentStatus.getStatusCn(document);

            if (!clazz.isInstance(logic))
                throw new ClassCastException("Logika dokumentu nie implementuje odpowiedniego interfejsu");

            functionality.notifyBody(clazz.cast(logic), statusCn);

        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        } finally {
            DSContextOpener.closeIfNeed(opener);
        }
    }

    public static Document getDocument(Long documentId) {
        DSContextOpener opener = null;
        Document document = null;
        try {
            document = getDocumentUnsafe(documentId);
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        } finally {
            DSContextOpener.closeIfNeed(opener);
            return document;
        }
    }

    /**
     * Wymaga otwartego kontekstu
     */
    public static Document getDocumentUnsafe(Long documentId) throws EdmException {
        return (documentId != null) ? Document.find(documentId) : null;
    }

    public static interface Functionality<T> {

        void notifyBody(T logic, String statusCn);
    }
}
