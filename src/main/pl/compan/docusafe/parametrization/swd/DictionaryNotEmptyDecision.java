package pl.compan.docusafe.parametrization.swd;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;

import java.util.List;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class DictionaryNotEmptyDecision implements DecisionHandler {
    private final static Logger LOG = LoggerFactory.getLogger(DictionaryNotEmptyDecision.class);

    private String dictionariesCns;

    @Override
    public String decide(OpenExecution openExecution) {
        try{
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();

            String [] dictsCns = TextUtils.splitNames(dictionariesCns,",");
            for (String dCn : dictsCns){
                Object rows = fm.getKey(dCn);
                if (rows != null && rows instanceof List){
                    if (((List) rows).size() > 0)
                        return "true";
                }
            }
            return "false";

        } catch (EdmException ex) {
            LOG.error(ex.getMessage(), ex);
            return "error";
        }
    }
}

