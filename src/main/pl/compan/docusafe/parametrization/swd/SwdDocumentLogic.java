package pl.compan.docusafe.parametrization.swd;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class SwdDocumentLogic
        extends
        AbstractDocumentLogic {


    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {

    }

    @Override
    public void documentPermissions(Document document) throws EdmException {

    }
}
