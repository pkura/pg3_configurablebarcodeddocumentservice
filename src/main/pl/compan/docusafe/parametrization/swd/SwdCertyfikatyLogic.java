package pl.compan.docusafe.parametrization.swd;

import com.google.common.collect.Maps;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.jsp.component.permission.JspStatusFieldsPropertiesSet;
import pl.compan.docusafe.web.swd.management.jsp.properties.SwdJspPermissionManager;
import pl.compan.docusafe.web.swd.management.utils.SwdPermission;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

/**
 * Created by Damian on 22.01.14.
 */
public class SwdCertyfikatyLogic extends AbstractDocumentLogic implements SwdJspPermissionManager.FieldPropertiesGetter  {

    private Logger log = LoggerFactory.getLogger(SwdCertyfikatyLogic.class);

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
        document.setFolder(folder);

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        document.setTitle(fm.getEnumItem("DOCUMENT_KIND").getTitle());
        document.setDescription(fm.getEnumItem("DOCUMENT_KIND").getTitle());
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {

    }

    @Override
    public String getMailForJbpmMailListener(Document document, String mailForLogicCn) {
        return MailForJbpmHelper.getMailForJbpmMailListener(mailForLogicCn);
    }

    @Override
    public JspStatusFieldsPropertiesSet getFieldsProperties(String statusCn, String jspName) {
        return getFieldsProperties(statusCn,jspName,null);
    }

    @Override
    public JspStatusFieldsPropertiesSet getFieldsProperties(String statusCn, String jspName, String dictCn) {
        SwdPermission.JspType jspType = SwdPermission.JspType.getJspType(jspName);

        switch(jspType){
            case ORGANISATION_UNIT:
                return getFieldsPropertiesOnOrganisationUnitJsp(statusCn);
            case ORGANISATION_UNIT_LIST:
                return getFieldsPropertiesOnOrganisationUnitListJsp(statusCn);
            case USERS_LIST:
                break;
            case USER_VIEW:
                return getFieldsPropertiesOnUserView(statusCn);
            case PASSED_PASSWORD:
                break;
            case PASSED_PASSWORD_LIST:
                break;
            default:
                throw new IllegalArgumentException();
        }

        return null;
    }

    private JspStatusFieldsPropertiesSet getFieldsPropertiesOnUserView(String statusCn) {
        JspStatusFieldsPropertiesSet properties = new JspStatusFieldsPropertiesSet();
        Set<SwdPermission.Permission> permissions = new HashSet<SwdPermission.Permission>();
        if("rozpatrzenie_reklamacji".equalsIgnoreCase(statusCn)){
            properties.setFieldModificatorsByPermissions(SwdPermission.Permission.getPermissionByTypeForJsp(SwdPermission.PermissionType.READ, SwdPermission.JspType.USER_VIEW));
        } else if("uaktualnienie_uprawnien".equalsIgnoreCase(statusCn)){
            properties.setFieldModificatorsByPermissions(SwdPermission.Permission.getPermissionByTypeForJsp(SwdPermission.PermissionType.READ, SwdPermission.JspType.USER_VIEW));
            permissions.add(SwdPermission.Permission.USER_RIGHTS_EDIT);
            permissions.add(SwdPermission.Permission.USER_SAVE_EDIT);
        }else if("sprawdzenienie_merytoryczne".equalsIgnoreCase(statusCn)){
            permissions.add(SwdPermission.Permission.USER_PROFILE_READ);
            permissions.add(SwdPermission.Permission.USER_RIGHTS_READ);
        }else if("dodanie_certyfikatu".equalsIgnoreCase(statusCn)){
            permissions.add(SwdPermission.Permission.USER_PROFILE_READ);
            permissions.add(SwdPermission.Permission.USER_RIGHTS_READ);
            permissions.add(SwdPermission.Permission.USER_CERTS_READ);
            permissions.add(SwdPermission.Permission.USER_CERTS_EDIT);
            permissions.add(SwdPermission.Permission.USER_SAVE_EDIT);
        }else if("dodatkowa_obsluga".equalsIgnoreCase(statusCn)){
            permissions.add(SwdPermission.Permission.USER_PROFILE_READ);
            permissions.add(SwdPermission.Permission.USER_CERTS_READ);
            permissions.add(SwdPermission.Permission.USER_MAIL_EDIT);
            permissions.add(SwdPermission.Permission.USER_RIGHTS_READ);
            permissions.add(SwdPermission.Permission.USER_RIGHTS_EDIT);
            permissions.add(SwdPermission.Permission.USER_ACCESS_DATA_READ);
            permissions.add(SwdPermission.Permission.USER_ACCESS_DATA_EDIT);
            permissions.add(SwdPermission.Permission.USER_SAVE_EDIT);
        }else if("rozliczenie_certyfikatow".equalsIgnoreCase(statusCn)){
            permissions.add(SwdPermission.Permission.USER_PROFILE_READ);
            permissions.add(SwdPermission.Permission.USER_CERTS_READ);
        }

        properties.setFieldModificatorsByPermissions(permissions);
        return properties;
    }


    protected JspStatusFieldsPropertiesSet getFieldsPropertiesOnOrganisationUnitJsp(String status) {
        JspStatusFieldsPropertiesSet properties = new JspStatusFieldsPropertiesSet();

        properties.setFieldModificatorsByPermissions(SwdPermission.Permission.getPermissionByTypeForJsp(SwdPermission.PermissionType.READ, SwdPermission.JspType.ORGANISATION_UNIT));
        properties.setFieldModificatorsByPermissions(SwdPermission.Permission.getPermissionByTypeForJsp(SwdPermission.PermissionType.EDIT, SwdPermission.JspType.ORGANISATION_UNIT));

        return properties;
    }

    protected JspStatusFieldsPropertiesSet getFieldsPropertiesOnOrganisationUnitListJsp(String status) {
        JspStatusFieldsPropertiesSet properties = new JspStatusFieldsPropertiesSet();

        properties.setFieldModificatorsByPermissions(SwdPermission.Permission.getPermissionByTypeForJsp(SwdPermission.PermissionType.READ, SwdPermission.JspType.ORGANISATION_UNIT_LIST));
        properties.setFieldModificatorsByPermissions(SwdPermission.Permission.getPermissionByTypeForJsp(SwdPermission.PermissionType.EDIT, SwdPermission.JspType.ORGANISATION_UNIT_LIST));

        return properties;
    }

    public void onStartProcess(OfficeDocument document, ActionEvent event) {
        try {
            Map<String, Object> map = Maps.newHashMap();
            map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
            //map.put(ASSIGN_DIVISION_GUID_PARAM, DSDivision.ROOT_GUID);
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

}