package pl.compan.docusafe.parametrization.swd;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.zbp.users.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class SwdObslugaZalacznikowDictionaryUpdater {

    private static Logger log = LoggerFactory.getLogger(SwdObslugaZalacznikowDictionaryUpdater.class);

    public static void notifyUserDataModified(final Long documentId, final User swdUser) {
        saveToDb(documentId, swdUser, SwdObslugaZalacznikowLogic.DICT_CN_USER_DATA_MODIFIED, SwdObslugaZalacznikowLogic.UserMofication.MODIFIED.title);
    }

    public static void notifyUserPermissionsChanged(final Long documentId, final User swdUser, String dictCn) {
        saveToDb(documentId, swdUser, dictCn, SwdObslugaZalacznikowLogic.UserMofication.PERMISSIONS_CHANGED.title);
    }

    public static void notifyUserDeactivated(final Long documentId, final User swdUser, String dictCn) {
        saveToDb(documentId, swdUser, SwdObslugaZalacznikowLogic.DICT_CN_USER_DEACTIVATED, SwdObslugaZalacznikowLogic.UserMofication.DEACTIVATED.title);
    }

    public static void saveToDb(Long documentId, User swdUser, String dictCn, String status) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {

            Long dictId = getDictionaryId(documentId, swdUser, dictCn);

            if (dictId != null) {

                //update

                ps = DSApi.context().prepareStatement("UPDATE " + SwdObslugaZalacznikowLogic.TABLE_SWD_USER + " SET " + SwdObslugaZalacznikowLogic.TABLE_COLUMN_STATUS_NOTE + " = ? WHERE ID = ?");
                ps.setString(1, status);
                ps.setLong(2, dictId);
                ps.executeUpdate();

                closePs(ps);

            } else {

                //zapisanie w tabeli slownikowej

                ps = DSApi.context().prepareStatement("INSERT INTO " + SwdObslugaZalacznikowLogic.TABLE_SWD_USER + " (" + SwdObslugaZalacznikowLogic.TABLE_COLUMN_SWDUID + ", FIRST_NAME, LAST_NAME, " + SwdObslugaZalacznikowLogic.TABLE_COLUMN_STATUS_NOTE + ") values (?,?,?,?)");
                ps.setInt(1, swdUser.getUserID());
                ps.setString(2, swdUser.getFirstName());
                ps.setString(3, swdUser.getLastName());
                ps.setString(4, status);
                ps.executeUpdate();

                finallyCloseAll(ps, rs);

                //powiazanie ze slownikiem

                dictId = getDocumentLastUserDictionaryId(documentId, swdUser, dictCn);

                ps = DSApi.context().prepareStatement("INSERT INTO " + SwdObslugaZalacznikowLogic.TABLE_SWD_OBSLUGA_ZALACZNIKOW_MULTIPLE_VALUE + "  values (?,?,?)");
                ps.setLong(1, documentId);
                ps.setString(2, dictCn);
                ps.setLong(3, dictId);
                ps.executeUpdate();

                finallyCloseAll(ps, rs);
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        } finally {
            finallyCloseAll(ps, rs);
        }
    }

    public static Long getDictionaryId(Long documentId, User swdUser, String dictCn) throws EdmException, SQLException {

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DSApi.context().prepareStatement("SELECT ID FROM " + SwdObslugaZalacznikowLogic.TABLE_SWD_USER + "  dictionary " +
                    " INNER JOIN " + SwdObslugaZalacznikowLogic.TABLE_SWD_OBSLUGA_ZALACZNIKOW_MULTIPLE_VALUE + "  multiples " +
                    " ON dictionary.ID = multiples.FIELD_VAL " +
                    " AND multiples.FIELD_CN = ? " +
                    " AND multiples.DOCUMENT_ID = ? " +
                    " WHERE dictionary." + SwdObslugaZalacznikowLogic.TABLE_COLUMN_SWDUID + " = ? ");
            ps.setString(1, dictCn);
            ps.setLong(2, documentId);
            ps.setInt(3, swdUser.getUserID());

            rs = ps.executeQuery();

            if (!rs.next())
                return null; // brak wpisu

            Long dictId = rs.getLong(1);

            if (rs.next())
                throw new IllegalArgumentException("Niejednoznaczny wpis slownika"); // niejednoznaczny wpis

            return dictId; // wpis


        } catch (EdmException e) {
            throw e;
        } catch (SQLException e) {
            throw e;
        } finally {
            finallyCloseAll(ps, rs);
        }
    }

    public static Long getDocumentLastUserDictionaryId(Long documentId, User swdUser, String dictCn) throws EdmException, SQLException {

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DSApi.context().prepareStatement("SELECT TOP 1 ID FROM " + SwdObslugaZalacznikowLogic.TABLE_SWD_USER + "  "
                    + " WHERE dictionary.SWDID = ? "
                    + " ORDER BY ID DESC ");
            ps.setInt(1, swdUser.getUserID());

            rs = ps.executeQuery();

            if (!rs.next())
                throw new IllegalArgumentException("Brak wpisu mimo ze dodawano");// brak wpisu

            Long dictId = rs.getLong(1);

            if (rs.next())
                throw new IllegalArgumentException("Niejednoznaczny wpis slownika"); // niejednoznaczny wpis

            return dictId; // wpis


        } catch (EdmException e) {
            throw e;
        } catch (SQLException e) {
            throw e;
        } finally {
            finallyCloseAll(ps, rs);
        }
    }

    @Deprecated
    private static void notifyModified(Long documentId, User swdUser, Modification modification) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DSApi.context().prepareStatement("SELECT ID FROM " + SwdObslugaZalacznikowLogic.TABLE_SWD_MODIFIED_USER + "  dictionary " +
                    " INNER JOIN " + SwdObslugaZalacznikowLogic.TABLE_SWD_OBSLUGA_ZALACZNIKOW_MULTIPLE_VALUE + "  multiples " +
                    " ON dictionary.ID = multiples.FIELD_VAL " +
                    " AND multiples.FIELD_CN = ? " +
                    " AND multiples.DOCUMENT_ID = ? " +
                    " WHERE dictionary.SWDID = ? ");
            ps.setString(1, SwdObslugaZalacznikowLogic.FIELD_SWD_MODIFIED_USER);
            ps.setLong(2, documentId);
            ps.setInt(3, swdUser.getUserID());

            rs = ps.executeQuery();
            if (rs.next()) {
                Long dictId = rs.getLong(1);

                if (rs.next())
                    throw new IllegalArgumentException("Niejednoznaczny wpis slownika");

                finallyCloseAll(ps, rs);

                //update

                ps = DSApi.context().prepareStatement("UPDATE " + SwdObslugaZalacznikowLogic.TABLE_SWD_MODIFIED_USER + " SET " + modification.getColumnName() + " = ? WHERE ID = ?");
                modification.setValue(1, ps);
                ps.setLong(2, dictId);
                ps.executeUpdate();

                finallyCloseAll(ps, rs);
            } else {
                finallyCloseAll(ps, rs);

                //zapisanie w tabeli slownikowej

                ps = DSApi.context().prepareStatement("INSERT INTO " + SwdObslugaZalacznikowLogic.TABLE_SWD_MODIFIED_USER + " (SWDID, FIRST_NAME, LAST_NAME, " + modification.getColumnName() + ") values (?,?,?,?)");
                ps.setInt(1, swdUser.getUserID());
                ps.setString(2, swdUser.getFirstName());
                ps.setString(3, swdUser.getLastName());
                modification.setValue(4, ps);
                ps.executeUpdate();
                DSApi.context().closeStatement(ps);

                ps = DSApi.context().prepareStatement("SELECT TOP 1 ID FROM " + SwdObslugaZalacznikowLogic.TABLE_SWD_MODIFIED_USER + "  "
                        + " WHERE dictionary.SWDID = ? "
                        + " ORDER BY ID DESC ");
                ps.setInt(1, swdUser.getUserID());

                rs = ps.executeQuery();
                if (rs.next()) {
                    Long dictId = rs.getLong(1);

                    finallyCloseAll(ps, rs);

                    //powiazanie ze slownikiem

                    ps = DSApi.context().prepareStatement("INSERT INTO " + SwdObslugaZalacznikowLogic.TABLE_SWD_OBSLUGA_ZALACZNIKOW_MULTIPLE_VALUE + "  values (?,?,?)");
                    ps.setLong(1, documentId);
                    ps.setString(2, SwdObslugaZalacznikowLogic.FIELD_SWD_MODIFIED_USER);
                    ps.setLong(3, dictId);
                    ps.executeUpdate();

                    finallyCloseAll(ps, rs);

                } else {
                    throw new IllegalArgumentException("Brak wpisu mimo ze dodawano");
                }
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        } finally {
            finallyCloseAll(ps, rs);
        }
    }

    @Deprecated
    public static void notifyAddedDataUser(Long doucmentId, User swdUser) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DSApi.context().prepareStatement("SELECT ID FROM " + SwdObslugaZalacznikowLogic.TABLE_SWD_MODIFIED_USER + "  dictionary " +
                    " INNER JOIN " + SwdObslugaZalacznikowLogic.TABLE_SWD_OBSLUGA_ZALACZNIKOW_MULTIPLE_VALUE + "  multiples " +
                    " ON dictionary.ID = multiples.FIELD_VAL " +
                    " AND multiples.FIELD_CN = ? " +
                    " AND multiples.DOCUMENT_ID = ? " +
                    " WHERE dictionary.SWDID = ? ");
            ps.setString(1, SwdObslugaZalacznikowLogic.FIELD_SWD_MODIFIED_USER);
            ps.setLong(2, doucmentId);
            ps.setInt(3, swdUser.getUserID());

            rs = ps.executeQuery();
            if (rs.next()) {
                throw new IllegalArgumentException("U�ytkownik jest ju� powi�zany z dokumentem - nie mo�na dodac drugiego takiego");
            } else {
                finallyCloseAll(ps, rs);

                //zapisanie w tabeli slownikowej

                ps = DSApi.context().prepareStatement("INSERT INTO " + SwdObslugaZalacznikowLogic.TABLE_SWD_MODIFIED_USER + " (SWDID, FIRST_NAME, LAST_NAME, DATA_MODIFIED, TO_ADD_PERMISSION) values (?,?,?,1,1)");
                ps.setInt(1, swdUser.getUserID());
                ps.setString(2, swdUser.getFirstName());
                ps.setString(3, swdUser.getLastName());
                ps.executeUpdate();
                DSApi.context().closeStatement(ps);

                ps = DSApi.context().prepareStatement("SELECT TOP 1 ID FROM " + SwdObslugaZalacznikowLogic.TABLE_SWD_MODIFIED_USER + "  "
                        + " WHERE dictionary.SWDID = ? "
                        + " ORDER BY ID DESC ");
                ps.setInt(1, swdUser.getUserID());

                rs = ps.executeQuery();
                if (rs.next()) {
                    Long dictId = rs.getLong(1);

                    finallyCloseAll(ps, rs);

                    //powiazanie ze slownikiem

                    ps = DSApi.context().prepareStatement("INSERT INTO " + SwdObslugaZalacznikowLogic.TABLE_SWD_OBSLUGA_ZALACZNIKOW_MULTIPLE_VALUE + "  values (?,?,?)");
                    ps.setLong(1, doucmentId);
                    ps.setString(2, SwdObslugaZalacznikowLogic.FIELD_SWD_MODIFIED_USER);
                    ps.setLong(3, dictId);
                    ps.executeUpdate();

                    finallyCloseAll(ps, rs);

                } else {
                    throw new IllegalArgumentException("Brak wpisu mimo ze dodawano");
                }
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        } finally {
            finallyCloseAll(ps, rs);
        }
    }

    public static void finallyCloseAll(PreparedStatement ps, ResultSet rs) {
        //closeRs(rs);
        closePs(ps);
    }

    public static void closeRs(ResultSet rs) {
        synchronized (rs) {
            try {
                if (rs != null && !rs.isClosed()) // rs.isClosed() ?? NullPointerException
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void closePs(PreparedStatement ps) {
        if (ps != null)
            DSApi.context().closeStatement(ps);
    }

    public static interface Modification {
        public String getColumnName();

        public void setValue(final int index, PreparedStatement ps) throws SQLException;
    }

    public static class IntModification implements Modification {

        private String columnName;
        private Integer value;

        public IntModification(String columnName, Integer value) {
            this.columnName = columnName;
            this.value = value;
        }

        @Override
        public String getColumnName() {
            return columnName;
        }

        @Override
        public void setValue(int index, PreparedStatement ps) throws SQLException {
            ps.setInt(index, value);
        }
    }
}
