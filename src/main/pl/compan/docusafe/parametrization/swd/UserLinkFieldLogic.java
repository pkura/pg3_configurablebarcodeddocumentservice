package pl.compan.docusafe.parametrization.swd;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.dockinds.field.LinkFieldLogic;
import pl.compan.docusafe.core.security.AccessDeniedException;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class UserLinkFieldLogic implements LinkFieldLogic {
    @Override
    public String createLink(String id) throws DocumentNotFoundException, DocumentLockedException, AccessDeniedException, EdmException {
        return "KAMIL";
    }

    @Override
    public String getLinkLabel() {
        return "Wiktor";
    }
}
