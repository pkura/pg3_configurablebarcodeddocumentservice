package pl.compan.docusafe.parametrization.swd;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.jsp.component.permission.JspStatusFieldsPropertiesSet;
import pl.compan.docusafe.web.swd.management.jsp.properties.SwdJspPermissionManager;
import pl.compan.docusafe.web.swd.management.utils.SwdPermission;
import pl.compan.docusafe.web.swd.management.utils.SwdUsersFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.util.HashMap;
import java.util.Map;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class SwdObslugaZalacznikowLogic
        extends
        AbstractDocumentLogic
        implements
        SwdJspPermissionManager.FieldPropertiesGetter,
        SwdDocumentLogicNotifier.ModifiedUser {

    public static final String TABLE_SWD_OBSLUGA_ZALACZNIKOW_MULTIPLE_VALUE = "DSG_SWD_OBSLUGA_ZALACZNIKOW_MULTIPLE_VALUE";
    public static final String TABLE_SWD_USER = "DSG_SWD_USER";
    public static final String TABLE_COLUMN_STATUS_NOTE = "STATUS_NOTE";

    public static final String DICT_CN_USER_NEW = "SWD_USER_NEW";
    public static final String DICT_CN_USER_DATA_MODIFIED = "SWD_USER_DATA_MODIFIED";
    public static final String DICT_CN_USER_DEACTIVATED = "SWD_USER_DEACTIVATED";

    @Deprecated
    public static final String TABLE_SWD_MODIFIED_USER = "DSG_SWD_MODIFIED_USER";
    @Deprecated
    public static final String FIELD_SWD_MODIFIED_USER = "SWD_MODIFIED_USER";
    public static final String TABLE_COLUMN_SWDUID = "SWDUID";
    private static final String FIELD_DATE_IN = "DATE_IN";


    private Logger log = LoggerFactory.getLogger(SwdObslugaZalacznikowLogic.class);


    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = new HashMap<String, Object>();
        for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
            if (f.getDefaultValue() != null)
                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));

//        java.sql.Date sqlDate = new java.sql.Date (new java.util.Date ().getTime());
//        toReload.put(FIELD_DATE_IN, sqlDate);

        fm.reloadValues(toReload);
    }

    public void documentPermissions(Document document) throws EdmException {
    }

    public void onStartProcess(OfficeDocument document, ActionEvent event) {
        try {
            Map<String, Object> map = Maps.newHashMap();
            map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
            //map.put(ASSIGN_DIVISION_GUID_PARAM, DSDivision.ROOT_GUID);
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public int getPermissionPolicyMode() {
        return PermissionManager.NORMAL_POLICY;
    }

    @Override
    public boolean searchCheckPermissions(Map<String, Object> values) {
        return false;
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
        document.setFolder(folder);

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        document.setTitle("" + (String) fm.getValue(fm.getMainFieldCn()));
        document.setDescription("" + (String) fm.getValue(fm.getMainFieldCn()));

    }

//    @Override
//    public void setAdditionalValues(FieldsManager fm, Integer valueOf, String activity) throws EdmException {
//        try {
//            ActivityInfo activityInfo = new Jbpm4ActivityInfo(activity);
//            String taskName = (String) activityInfo.get("task_name");
//
//            Object organisationUnit = fm.getFieldValues().get("SWD_ORGANISATION_UNIT");
//            Object modifiedUsers = fm.getFieldValues().get("FIELD_SWD_MODIFIED_USER");
////            if (mpk != null)
////            {
////                if (mpk instanceof List<?>)
////                {
////                    mpkIds = (List<Long>) fieldsManager.getFieldValues().get("MPK");
////                }
////                else if (mpk instanceof Long)
////                {
////                    mpkIds.add((Long)mpk);
////                }
////            }
//        } catch (Exception e) {
//            log.error(e.getMessage(), e);
//        }
//    }

    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues, Long documentId) {
        return super.setMultipledictionaryValue(dictionaryName, values, fieldsValues, documentId);
    }

    @Override
    public JspStatusFieldsPropertiesSet getFieldsProperties(String statusCn, String jspName) {
        return getFieldsProperties(statusCn, jspName, null);
    }

    @Override
    public JspStatusFieldsPropertiesSet getFieldsProperties(String statusCn, String jspName, String dictCn) {
        //^.*cn=("[^"]+").*$
        //$1

        SwdPermission.JspType jspType = SwdPermission.JspType.getJspType(jspName);

        switch (jspType) {
            case ORGANISATION_UNIT:
                return getFieldsPropertiesOnOrganisationUnitJsp(statusCn, dictCn);
            case ORGANISATION_UNIT_LIST:
                return getFieldsPropertiesOnOrganisationUnitListJsp(statusCn, dictCn);
            case USERS_LIST:
                break;
            case USER_VIEW:
                return getFieldsPropertiesOnUserViewJsp(statusCn, dictCn);
            case PASSED_PASSWORD:
                break;
            case PASSED_PASSWORD_LIST:
                break;
            default:
                throw new IllegalArgumentException();
        }

        return null;
    }

    protected JspStatusFieldsPropertiesSet getFieldsPropertiesOnOrganisationUnitJsp(String statusCn, String dictCn) {
        JspStatusFieldsPropertiesSet properties = new JspStatusFieldsPropertiesSet();

        properties.setFieldModificatorsByPermissions(SwdPermission.Permission.getPermissionByTypeForJsp(SwdPermission.PermissionType.READ, SwdPermission.JspType.ORGANISATION_UNIT));
        properties.setFieldModificatorsByPermissions(SwdPermission.Permission.OU_BUTTON_DOCUMENTS_EDIT);

        if (statusCn.equalsIgnoreCase("process_creation")) {
        } else if (statusCn.equalsIgnoreCase("wybor_osob")) {
        } else if (statusCn.equalsIgnoreCase("zarzadzanie_uzytkownikami")) {
            properties.setFieldModificatorsByPermissions(
                    SwdPermission.Permission.OU_DANE_ORG_EDIT,
                    SwdPermission.Permission.OU_BUTTON_SAVE_EDIT
            );
        } else if (statusCn.equalsIgnoreCase("uprawnienia_dezaktywacja")) {
            properties.setFieldModificatorsByPermissions(
                    SwdPermission.Permission.OU_SPOSOBY_DOSTEPU_EDIT,
                    SwdPermission.Permission.OU_BUTTON_SAVE_EDIT
            );
        } else if (statusCn.equalsIgnoreCase("dezaktywacja")) {
        } else if (statusCn.equalsIgnoreCase("weryfikacja")) {
        } else if (statusCn.equalsIgnoreCase("obciazenie")) {
        } else if (statusCn.equalsIgnoreCase("process_end")) {
        } else {
            throw new IllegalArgumentException("No status cn=" + statusCn);
        }

        return properties;
    }

    protected JspStatusFieldsPropertiesSet getFieldsPropertiesOnOrganisationUnitListJsp(String statusCn, String dictCn) {
        JspStatusFieldsPropertiesSet properties = new JspStatusFieldsPropertiesSet();

        properties.setFieldModificatorsByPermissions(SwdPermission.Permission.getPermissionByTypeForJsp(SwdPermission.PermissionType.READ, SwdPermission.JspType.ORGANISATION_UNIT_LIST));

        if (statusCn.equalsIgnoreCase("process_creation")) {
            properties.setFieldModificatorsByPermissions(
                    SwdPermission.Permission.OULIST_BUTTON_CHOOSE_EDIT,
                    SwdPermission.Permission.OULIST_BUTTON_CREATE_OUNIT_EDIT
            );
        } else if (statusCn.equalsIgnoreCase("wybor_osob")) {
        } else if (statusCn.equalsIgnoreCase("zarzadzanie_uzytkownikami")) {
        } else if (statusCn.equalsIgnoreCase("uprawnienia_dezaktywacja")) {
        } else if (statusCn.equalsIgnoreCase("dezaktywacja")) {
        } else if (statusCn.equalsIgnoreCase("weryfikacja")) {
        } else if (statusCn.equalsIgnoreCase("obciazenie")) {
        } else if (statusCn.equalsIgnoreCase("process_end")) {
        } else {
            throw new IllegalArgumentException("No status cn=" + statusCn);
        }

        return properties;
    }

    protected JspStatusFieldsPropertiesSet getFieldsPropertiesOnUserViewJsp(String statusCn, String dictCn) {
        JspStatusFieldsPropertiesSet properties = new JspStatusFieldsPropertiesSet();

        properties.setFieldModificatorsByPermissions(SwdPermission.Permission.getPermissionByTypeForJsp(SwdPermission.PermissionType.READ, SwdPermission.JspType.USER_VIEW));

        if (statusCn.equalsIgnoreCase("process_creation")) {
        } else if (statusCn.equalsIgnoreCase("wybor_osob")) {
        } else if (statusCn.equalsIgnoreCase("zarzadzanie_uzytkownikami")) {
            if (dictCn.equalsIgnoreCase(DICT_CN_USER_DATA_MODIFIED)){
                properties.setFieldModificatorsByPermissions(
                        SwdPermission.Permission.USER_SAVE_EDIT,

                        SwdPermission.Permission.USER_PROFILE_EDIT
                );
            } else if (dictCn.equalsIgnoreCase(DICT_CN_USER_DEACTIVATED)){
                properties.setFieldModificatorsByPermissions(
                        SwdPermission.Permission.USER_SAVE_EDIT,

                        SwdPermission.Permission.USER_ACCESS_DATA_EDIT
                );
            }
        } else if (statusCn.equalsIgnoreCase("uprawnienia_dezaktywacja")) {
                properties.setFieldModificatorsByPermissions(
                    SwdPermission.Permission.USER_SAVE_EDIT,

                    SwdPermission.Permission.USER_ACCESS_DATA_EDIT,
                    SwdPermission.Permission.USER_ROLES_EDIT,
                    SwdPermission.Permission.USER_RIGHTS_EDIT
                );
        } else if (statusCn.equalsIgnoreCase("dezaktywacja")) {
            properties.setFieldModificatorsByPermissions(
                    SwdPermission.Permission.USER_SAVE_EDIT,

                    SwdPermission.Permission.USER_ACCESS_DATA_EDIT

                    //TODO DEAKTYWACJA
            );
        } else if (statusCn.equalsIgnoreCase("weryfikacja")) {
        } else if (statusCn.equalsIgnoreCase("obciazenie")) {
        } else if (statusCn.equalsIgnoreCase("process_end")) {
        } else {
            throw new IllegalArgumentException("No status cn=" + statusCn);
        }

        return properties;
    }

    @Override
    public void notifyUserModified(Long documentId, String statusCn, Long userId, String dictCn) {

        if (documentId == null || userId == null || StringUtils.isBlank(dictCn))
            return;

        if (dictCn.equalsIgnoreCase(DICT_CN_USER_DATA_MODIFIED)) {
            if (statusCn.equalsIgnoreCase("zarzadzanie_uzytkownikami")){
                SwdObslugaZalacznikowDictionaryUpdater.notifyUserDataModified(documentId, SwdUsersFactory.getUserById(userId.intValue()));
                return;
            }
            if (statusCn.equalsIgnoreCase("uprawnienia_dezaktywacja")){
                SwdObslugaZalacznikowDictionaryUpdater.notifyUserPermissionsChanged(documentId, SwdUsersFactory.getUserById(userId.intValue()), dictCn);
                return;
            }
        }
        if (dictCn.equalsIgnoreCase(DICT_CN_USER_DEACTIVATED)) {
            if (statusCn.equalsIgnoreCase("dezaktywacja")){
                SwdObslugaZalacznikowDictionaryUpdater.notifyUserDeactivated(documentId, SwdUsersFactory.getUserById(userId.intValue()), dictCn);
                return;
            }
        }

        throw new IllegalArgumentException("Brak obsługilugi zmiany na etapie o statusCn=" + statusCn + " dla dokumentu o id=" + documentId);
    }

    public enum UserMofication {
        PERMISSIONS_CHANGED("zmieniono uprawnienia"),
        MODIFIED("zmodyfikowano dane"),
        DEACTIVATED("deaktywowano");

        public final String title;

        UserMofication(String title) {
            this.title = title;
        }
    }
}
