package pl.compan.docusafe.parametrization.swd;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.LinkValue;

import java.util.Map;

/**
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class UserDictionaryLogic extends DwrDictionaryBase {
    @Override
    public Map<String, Object> getValues(String id, Map<String, Object> fieldValues) throws EdmException {
        Map<String, Object> values = super.getValues(id, fieldValues);
        values.put(getName() + "_GOGO", new LinkValue("TytulGogo", "htmlgogo" + id));
        return values;
    }

    public static void test (FieldsManager fm){

    }
}
