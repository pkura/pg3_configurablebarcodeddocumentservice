package pl.compan.docusafe.parametrization.swd;

import org.jfree.util.Log;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.Profile;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Damian on 22.01.14.
 */
public class MailForJbpmHelper {

    /**
     * Returns mail adresses for all users with given profiles
     *
     *
     * @param profileNames - string with comma (",") separated profile names
     * @return - string with separated by ";" mail addresses for all users with any of given profiles
     */
    static public String getMailForJbpmMailListener(String profileNames) {
        Set<DSUser> users = getUsersByProfilename(profileNames);


        StringBuilder sb = new StringBuilder();
        for(DSUser u : users){
            sb.append(u.getEmail()).append(";");
        }
        if(sb.lastIndexOf(";") > 0){
            sb.deleteCharAt(sb.lastIndexOf(";"));
        }
        return sb.toString();
    }

    /**
     * Returns Users for given profile names (names could be separated by ",")
     *
     * @param profileName - string with comma (",") separated profile names
     * @return user with any profiles of given names
     */
    static public Set<DSUser> getUsersByProfilename(String profileName){
        Set<DSUser> allUsers = new HashSet<DSUser>();

        if(profileName != null){
            String[] profNames = profileName.split(",");
            for(String name : profNames){
                try{
                    Profile profile = Profile.findByProfilename(profileName);
                    if(profile != null){
                        Set<DSUser> users = profile.getUsers();
                        if(users != null){
                            allUsers.addAll(users);
                        }
                    }
                }catch (EdmException e){
                    Log.error(e.getMessage(), e);
                }
            }
        }
        return allUsers;
    }
}
