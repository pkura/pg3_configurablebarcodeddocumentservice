package pl.compan.docusafe.parametrization.swd;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Sprawdza czy jakikowlwiek wpis s�ownika w kolumnie statusu zawiera okre�lony status
 *
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class AnyUserWithStatusDecision implements DecisionHandler {
    private final static Logger LOG = LoggerFactory.getLogger(AnyUserWithStatusDecision.class);

    protected String status;
    protected String dictCn;

    private String multipleDictTable = SwdObslugaZalacznikowLogic.TABLE_SWD_USER;
    private String documentMultipleValueTable = SwdObslugaZalacznikowLogic.TABLE_SWD_OBSLUGA_ZALACZNIKOW_MULTIPLE_VALUE;
    private String statusColumn = SwdObslugaZalacznikowLogic.TABLE_COLUMN_STATUS_NOTE;

    @Override
    public String decide(OpenExecution openExecution) {
        try {
            Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();

            Pattern pattern = Pattern.compile("^(|(,.*))\\s*" + status.toUpperCase() + "\\s*((,.*)|)$"); // ^(|(,.*))\s* <STATUS> \s*((,.*)|)$

            List<String> entries = MultipleDictionaryFactory.getEntries(new UserDictEntry(docId, documentMultipleValueTable, multipleDictTable, dictCn, statusColumn));
            for (String status : entries){
                if (StringUtils.isNotBlank(status) && pattern.matcher(status.toUpperCase()).find())
                    return "true";
            }

            return "false";
        } catch (EdmException e) {
            LOG.error(e.getMessage(), e);
            return "error";
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
            return "error";
        }
    }

    private static class UserDictEntry extends MultipleDictionaryFactory.SqlManager<String> { /*SwdUserLinkField.UserEntry*/

        private String statusColumn;

        protected UserDictEntry(Long docId, Object documentMultipleValueTable, Object multipleDictTable, String dictCn, String statusColumn) {
            super(docId, documentMultipleValueTable, multipleDictTable, dictCn);
            this.statusColumn = statusColumn;
        }

//        @Override
//        public SwdUserLinkField.UserEntry createObject(ResultSet rs) throws SQLException {
//            List<String> columns = getColumns();
//            Long id = rs.getLong(columns.get(0));
//            String firstName = rs.getString(columns.get(1));
//            String lastName = rs.getString(columns.get(2));
//            Long swdId = rs.getLong(columns.get(3));
//            String status = rs.getString(columns.get(4));
//
//            return new SwdUserLinkField.UserEntry(id,firstName,lastName, swdId, status);
//        }
//        @Override
//        public List<String> getColumns() {
//            return java.util.Arrays.asList(new String[]{"id", "FIRST_NAME", "LAST_NAME", "SWDUID", "STATUS_NOTE"});
//        }

        @Override
        public String createObject(ResultSet rs) throws SQLException {
            String status = rs.getString(statusColumn);
            return status;
        }

        @Override
        public Collection<String> getColumns() {
            return java.util.Arrays.asList(new String[]{statusColumn});
        }
    }

}
