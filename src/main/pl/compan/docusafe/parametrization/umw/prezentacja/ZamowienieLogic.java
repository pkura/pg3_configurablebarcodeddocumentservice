package pl.compan.docusafe.parametrization.umw.prezentacja;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionType;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * User: kk
 * Date: 12.01.14
 * Time: 22:08
 */
public class ZamowienieLogic extends AbstractDocumentLogic {

    public static final String CASE_NUMBER_FIELD_CN = "NR_SPRAWY";
    protected static final Logger log = LoggerFactory.getLogger(ZamowienieLogic.class);
    protected static final SortedSet<Integer> CASE_NUMBERS = Sets.newTreeSet();

    private static final String UPDATE_CASE_NO = "declare @n int;\n" +
            "select @n=ISNULL(max(cast(substring(nr_sprawy, 6, 4) as int)),0)+1\n" +
            "from dsg_umw_zamowienie\n" +
            "where SUBSTRING(nr_sprawy, 1, 4)=?;\n" +
            "update dsg_umw_zamowienie set nr_sprawy=?+replace(str(@n,4),' ','0') where document_id=?;";


    private static final Map<String, String> MAIN_PERMISSION = ImmutableMap.<String, String>builder().
            put(ObjectPermission.READ, "odczyt").
            put(ObjectPermission.READ_ATTACHMENTS, "odczyt zalacznika").
            put(ObjectPermission.MODIFY, "edycja").
            put(ObjectPermission.MODIFY_ATTACHMENTS, "edycja zalacznika").
            put(ObjectPermission.DELETE, "usuwanie").
            build();

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {
    }

    private void assigneDecreeNumber(Long id, String year) throws Exception {
        int updated;
        PreparedStatement ps = null;
        try {
            ps = DSApi.context().prepareStatement(UPDATE_CASE_NO);
            ps.setString(1, year);
            ps.setString(2, String.format("%.4s-", year));
            ps.setLong(3, id);

            updated = ps.executeUpdate();

            Preconditions.checkState(updated == 1);
        } catch (Exception e) {
            if (ps != null) {
                DSApi.context().closeStatement(ps);
            }
            throw e;
        }
    }

    @Override
    public boolean canChangeDockind(Document document) throws EdmException {
        return false;
    }

    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
        super.onStartProcess(document, event);

        createPermissions(document);

        createDecreeNumber(document);
    }


    private void createDecreeNumber(OfficeDocument document) throws EdmException {
        try {
            FieldsManager fm = document.getFieldsManager();
            if (fm.getKey(CASE_NUMBER_FIELD_CN) == null) {
                Date currentDay = GlobalPreferences.getCurrentDay();
                Calendar calInstance = Calendar.getInstance();
                calInstance.setTime(currentDay);

                String year = String.valueOf(calInstance.get(Calendar.YEAR));

                assigneDecreeNumber(document.getId(), year);
            } else {
                log.error("Pr�bowano ponownie nada� numer dekretu");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new EdmException("B��d w generowaniu numeru dekretu: " + e.getMessage());
        }
    }

    private void createPermissions(OfficeDocument document) throws EdmException {
        Set<PermissionBean> perms = new HashSet<PermissionBean>();
        String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
        String documentKindName = document.getDocumentKind().getName();

        for (Map.Entry<String, String> entry : MAIN_PERMISSION.entrySet()) {
            perms.add(new PermissionBean(entry.getKey(), String.format("%s_%s", documentKindCn, entry.getKey().toUpperCase()), ObjectPermission.GROUP, String.format("%s - %s", documentKindName, entry.getValue())));
        }

        Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
        perms.addAll(documentPermissions);
        this.setUpPermission(document, perms);
    }

    @Override
    public List<String> getActionMessage(ActionType source, Document doc) {
        List<String> messages = super.getActionMessage(source, doc);

        try {
            switch (source) {
                case OFFICE_CREATE_DOCUMENT:
                    messages.add("Zosta� nadany numer sprawy: " + doc.getFieldsManager().getStringValue(CASE_NUMBER_FIELD_CN));
                    break;
                default:
                    break;
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }

        return messages;
    }

    @Override
    public void setAdditionalTemplateValues(long docId, Map<String, Object> values, Map<Class, Format> defaultFormatters){
        try{
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setDecimalSeparator(',');
            defaultFormatters.put(BigDecimal.class, new DecimalFormat("### ### ##0.00", symbols));
            defaultFormatters.put(Date.class, new SimpleDateFormat("dd-MM-yyyy"));
            defaultFormatters.put(Timestamp.class, new SimpleDateFormat("dd-MM-yyyy"));

            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();

            values.put("CTIME", doc.getCtime());

        }catch(Exception e){
            log.error(e.getMessage(), e);
        }
    }
}
