package pl.compan.docusafe.parametrization.umw.prezentacja;

import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;
import org.directwebremoting.WebContextFactory;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * User: kk
 * Date: 12.01.14
 * Time: 21:12
 */
public class AccountNumberDictionary extends DwrDictionaryBase {

    public static final String CONTRACTOR = "CONTRACTOR";
    public static final String KONTO_BANKOWE = "KONTO_BANKOWE";
    public static final String KONTO_BANKOWE_SUFFIX = KONTO_BANKOWE + "_";
    public static final String KONTRAHENT_ID = "KONTRAHENT_ID";


    @Override
    public long add(Map<String, FieldData> values) throws EdmException {
        List<Number> contractorIds = getContractorIdsFromSession();

        if (contractorIds.isEmpty()) {
            return -1;
            //throw new EdmException("Najpierw wybierz kontrahenta");
        }

        values.put(KONTO_BANKOWE_SUFFIX + KONTRAHENT_ID, new FieldData(Field.Type.LONG, contractorIds.get(0)));

        return super.add(values);
    }

    @Override
    public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset, boolean fromDwrSearch, Map<String, FieldData> dockindFields) throws EdmException {
        List<Map<String, Object>> ret = Lists.newArrayList();

        List<Number> contractorIds = getContractorIdsFromSession();

        if (fromDwrSearch && contractorIds.isEmpty()) {
            return ret;
        }

        if (!contractorIds.isEmpty()) {
            values.put(KONTO_BANKOWE_SUFFIX + KONTRAHENT_ID, new FieldData(Field.Type.LONG, contractorIds.get(0)));
        }

        List<Map<String, Object>> results =  super.search(values, limit, offset, fromDwrSearch, dockindFields);

        return results;
    }

    private List<Number> getContractorIdsFromSession() {
        List<Number> contractorIds = Lists.newArrayList();
        HttpSession session;
        if (WebContextFactory.get() != null && (session = WebContextFactory.get().getSession()) != null && session.getAttribute("dwrSession") != null) {
            Map<String, Object> documentValues = (Map<String, Object>) ((Map<String, Object>) session.getAttribute("dwrSession")).get("FIELDS_VALUES");

            if (documentValues != null && documentValues.get(CONTRACTOR) != null) {
                Object raw = documentValues.get(CONTRACTOR);
                contractorIds = Lists.newArrayList();
                if (raw instanceof Iterable) {
                    for (Object item : (Iterable)raw) {
                        if (item != null) {
                            contractorIds.add(Ints.tryParse(item.toString()));
                        }
                    }
                } else if (raw instanceof Number) {
                    contractorIds.add((Number) raw);
                } else {
                    Integer erpId = Ints.tryParse(raw.toString());
                    if (erpId != null) {
                        contractorIds.add(erpId);
                    }
                }
            }
        }
        return contractorIds;
    }
}
