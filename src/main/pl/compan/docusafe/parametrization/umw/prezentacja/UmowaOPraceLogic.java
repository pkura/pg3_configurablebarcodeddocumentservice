package pl.compan.docusafe.parametrization.umw.prezentacja;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.ul.CostInvoiceTemplateHelper;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * Created by brs on 10.01.14.
 */
public class UmowaOPraceLogic extends AbstractDocumentLogic{

    private final static Logger log = LoggerFactory.getLogger(UmowaOPraceLogic.class);

    @Override
    public void setAdditionalTemplateValues(long docId, Map<String, Object> values, Map<Class, Format> defaultFormatters){
        try{
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setDecimalSeparator(',');
            defaultFormatters.put(BigDecimal.class, new DecimalFormat("### ### ##0.00", symbols));
            defaultFormatters.put(Date.class, new SimpleDateFormat("dd-MM-yyyy"));
            defaultFormatters.put(Timestamp.class, new SimpleDateFormat("dd-MM-yyyy"));

            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();

            values.put("CTIME", doc.getCtime());

        }catch(Exception e){
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        Folder folder = Folder.getRootFolder();
        String doctitle = document.getDocumentKind().getName();
        folder = folder.createSubfolderIfNotPresent(doctitle);

        folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(document.getCtime()));

        document.setFolder(folder);
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {
        Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
        String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
        String documentKindName = document.getDocumentKind().getName();

        perms.add(new PermissionBean(ObjectPermission.READ, documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, documentKindName + " - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, documentKindName + " zalacznik - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, documentKindName + " - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, documentKindName + " zalacznik - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, documentKindName + " - usuwanie"));
        Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
        perms.addAll(documentPermissions);
        this.setUpPermission(document, perms);
    }
}
