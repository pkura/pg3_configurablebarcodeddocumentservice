package pl.compan.docusafe.parametrization.umw.prezentacja;

import com.google.common.collect.Maps;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.ocr.OcrSupport;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.tktelekom.TKTelekomOcrSupport;
import pl.compan.docusafe.parametrization.tktelekom.TKTelekomUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.util.*;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

/**
 *
 */
public class NormalLogic extends AbstractDocumentLogic {

    private static NormalLogic instance;
    protected static Logger log = LoggerFactory.getLogger(NormalLogic.class);
    public static NormalLogic getInstance() {
        if (instance == null)
            instance = new NormalLogic();
        return instance;
    }

    public boolean canChangeDockind(Document document) throws EdmException
    {
        return false;
    }

    public void onStartProcess(OfficeDocument document, pl.compan.docusafe.webwork.event.ActionEvent event) throws EdmException
    {
        String docName = document.getDocumentKind()!=null?document.getDocumentKind().getName():"no name";

        log.info("--- "+docName+" : START PROCESS !!! ---- {}", event);
        try
        {
            Map<String, Object> map = Maps.newHashMap();

            if (document instanceof pl.compan.docusafe.core.office.OutOfficeDocument)
            {
                if (!((pl.compan.docusafe.core.office.OutOfficeDocument) document).isInternal())
                {
                    pl.compan.docusafe.core.office.Journal journal = document.getJournal();
                     if (journal.isMain())
                         throw new EdmException("Nie wybrano dziennika!");
                     String code = journal.getSymbol();
                     String guid = "";
                     if (journal.isIncoming())
                         guid=code.replace("_IN", "");
                    else
                         guid=code.replace("_OUT", "");

                    map.put(ASSIGN_DIVISION_GUID_PARAM, guid);
                }
                else
                {
                    map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
                    map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
                }
            }
            else
            {
                map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
                map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
            }

            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
            Set<PermissionBean> perms = new HashSet<PermissionBean>();

            pl.compan.docusafe.core.users.DSUser author = DSApi.context().getDSUser();
            String user = author.getName();
            String fullName = author.asFirstnameLastname();

            perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

            Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
            perms.addAll(documentPermissions);
            this.setUpPermission(document, perms);

            DSApi.context().watch(pl.compan.docusafe.core.names.URN.create(document));
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
            throw new EdmException(e.getMessage());
        }
    }
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();
        for(Field f : fm.getFields()){
            if(f.getDefaultValue() != null){
                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
            }
        }
        toReload.put("DATA_GODZINA_WPLYWU", Calendar.getInstance().getTime());
        toReload.put("DOC_DATE", Calendar.getInstance().getTime());

        log.info("toReload = {}", toReload);
        fm.reloadValues(toReload);
    }
    public void documentPermissions(Document document) throws EdmException {
        Set<PermissionBean> perms = new HashSet<PermissionBean>();
        perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

        for (PermissionBean perm : perms) {
            if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP)) {
                String groupName = perm.getGroupName();
                if (groupName == null) {
                    groupName = perm.getSubject();
                }
                createOrFind(document, groupName, perm.getSubject());
            }
            DSApi.context().grant(document, perm);
            DSApi.context().session().flush();
        }
    }
	
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        as.useFolderResolver(document);
        as.useAvailableProviders(document);
        log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());

        if (document != null && document.getDocumentKind().getCn().equals("normal_out"))
        {
            OfficeCase officeCase = ((OfficeDocument) document).getContainingCase();
            if (officeCase != null)
            {
                PreparedStatement ps = null;
                try {
                    ps = DSApi.context().prepareStatement("update dsg_normal_dockind set NR_DOKUMENTU = ? where document_id = ?");
                    ps.setString(1, officeCase.getOfficeId());
                    ps.setLong(2, document.getId());
                    ps.executeUpdate();
                    ps.close();

                    DSApi.context().closeStatement(ps);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                } finally {
                    DSApi.context().closeStatement(ps);
                }
            }
        }
    }
    @Override
    public OcrSupport getOcrSupport()
    {
        return TKTelekomOcrSupport.get();

    }
    
    public void printLabel(Document document, int type, Map<String, Object> dockindKeys) throws EdmException
    {
    	TKTelekomUtils.printLabel(document, type, dockindKeys);
    }


    public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
    {
        pl.compan.docusafe.core.dockinds.dwr.Field msg = null;

        if (DwrUtils.isNotNull(values, "DWR_NUMER_DOKUMENTU") && values.get("DWR_NUMER_DOKUMENTU").isSender())
        {
            boolean isOpened = true;
            List<Long> ids = new LinkedList<Long>();
            try
            {
                if (!DSApi.isContextOpen())
                {
                    isOpened = false;
                    DSApi.openAdmin();
                }

                // proste zapytanie, wyci庵ni璚ie wbranych kolumn z warunkiem where
                NativeCriteria nc = DSApi.context().createNativeCriteria("dsg_normal_dockind", "d");
                nc.setProjection(NativeExps.projection()
                        // ustalenie kolumn
                        .addProjection("d.DOCUMENT_ID").addProjection("d.NR_DOKUMENTU"))
                        // warunek where
                        .add(NativeExps.eq("d.NR_DOKUMENTU", values.get("DWR_NUMER_DOKUMENTU").getData()));

                // pobranie i wy鈍ietlenie wynik闚
                CriteriaResult cr = nc.criteriaResult();

                while (cr.next())
                {
                    ids.add(cr.getLong(0, null));
                }
            }catch (EdmException e)
            {
                log.error(e.getMessage(), e);
            }
            finally
            {
                if (DSApi.isContextOpen() && !isOpened)
                {
                    DSApi._close();
                }
            }
            if (ids.size() > 0)
            {
                StringBuilder msgString = new StringBuilder("Pismo o numerze '").append(values.get("DWR_NUMER_DOKUMENTU").getData()).append("' ju� istnieje w systemie. ID pism z takim numerem: ");
                for (Long id : ids)
                {
                    msgString.append(id);
                    if (ids.indexOf(id) < ids.size()-1)
                        msgString.append(", ");
                }
                msg = new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgString.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
                values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
            }
        }
        return msg;
    }
}
