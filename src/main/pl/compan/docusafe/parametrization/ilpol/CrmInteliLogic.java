/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.compan.docusafe.parametrization.ilpol;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;

/**
 *
 * @author Tomasz Lipka
 */
public class CrmInteliLogic  extends AbstractDocumentLogic{

   

   private static CrmInteliLogic instance;

    public static CrmInteliLogic getInstance()
    {
        if (instance == null)
            instance = new CrmInteliLogic();
        return instance;
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
    	
        if(document instanceof OfficeDocument)
        {
            ((OfficeDocument)document).setSummary("intelis");
        }
    }

    public void documentPermissions(Document document) throws EdmException
    {

    }

}
