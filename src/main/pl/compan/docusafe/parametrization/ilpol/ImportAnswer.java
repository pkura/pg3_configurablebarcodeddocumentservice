package pl.compan.docusafe.parametrization.ilpol;

public class ImportAnswer {
	
	private Long applicationId;
	private Integer result;
	private String error;
	
//	public ImportAnswer() {
//	}
	public Long getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}
	public Integer getResult() {
		return result;
	}
	public void setResult(Integer result) {
		this.result = result;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	

}
