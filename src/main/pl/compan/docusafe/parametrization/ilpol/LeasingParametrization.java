package pl.compan.docusafe.parametrization.ilpol;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.security.auth.Subject;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.permission.DlPermissionManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.workflow.Jbpm4WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.TasklistSynchro;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.snapshot.SnapshotUtils;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.ReferenceToHimselfException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.sql.SubstituteHistory;
import pl.compan.docusafe.parametrization.AbstractParametrization;

public class LeasingParametrization  extends AbstractParametrization
{
	/***
	 * Metoda wywo�ywana po utowrzeniu persona
	 * @throws EdmException 
	 */
	@Override
	public void setBeforeUserDelete(String username, String toUser, DSContext ctx,Subject subject)throws EdmException
	{
		ctx = DSApi.open(subject);
    	TaskSnapshot.Query query;
        query = new TaskSnapshot.Query();
        if(toUser == null)
        	throw new EdmException("Nale�y wybra� u�ytkownika na kty�rego zostan� przekazane zadnia");
        DSUser userTo = DSUser.findByUsername(toUser);
        if(userTo.getAllDivisions() == null || userTo.getAllDivisions().length < 1)
        	throw new EdmException("Wybrany u�ytkownik musi znajdowa� si� w dziale");
        try
        {
        	TasklistSynchro.Synchro(username);
        	DSUser toDSUser = DSUser.findByUsername(toUser);
            DSUser user = DSUser.findByUsername(username);
            query.setUser(user);
            SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
            int i = 0 ;
            while (results.hasNext())
            {   
            	try
            	{
	            	ctx.begin();
	            	TaskSnapshot task = results.next();
	            	Document document = Document.find(task.getDocumentId());
	            	if(DocumentLogicLoader.CRM_MARKETING_TASK_KIND.equals(document.getDocumentKind().getCn()))
	            	{
		            	Map<String, Object> fieldValues = new HashMap<String, Object>();
		            	fieldValues.put(CrmMarketingTaskLogic.UZYTKOWNIK_FIELD_CN,toDSUser.getId());
		    			document.getDocumentKind().setOnly(document.getId(), fieldValues);
	            	}
	            	i++;
	            	if(task.getSameProcessSnapshotsCount() < 2)
	            	{
	            		Jbpm4WorkflowFactory.reassign(task.getActivityKey(), null, username, toUser);
	            	}
	            		//WorkflowFactory.simpleAssignment("internal,"+task.getActivityKey(), userTo.getAllDivisions()[0].getGuid(), toUser, DSApi.context().getPrincipalName(), null, WorkflowFactory.SCOPE_ONEUSER, null, null);
	            	ctx.commit();
					if(i%100 == 99)
					{
						log.error("Czyszczenie sesi po "+i+" dokumentach");
						ctx.session().flush();
						ctx.session().clear();
						DSApi.close();
						ctx = DSApi.open(subject);
					}
            	}
            	catch (Exception e) 
            	{
                    DSApi.context().setRollbackOnly();
                    throw new EdmException(e.getMessage(), e);
				}
            }         
        }
        catch (Exception e)
        {
            throw new EdmException(e.getMessage(), e);
        }
        finally
        {
        	try
        	{
        	DSApi.close();
        	}
        	catch (Exception e) {
				log.error(e.getMessage(), e);
			}
        }
	}
	
	public void afterAddSubstution(SubstituteHistory history,
			pl.compan.docusafe.web.common.event.ActionEvent event) {
		//wyslij maila
		
	}
	
}
