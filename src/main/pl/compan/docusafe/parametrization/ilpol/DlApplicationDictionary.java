package pl.compan.docusafe.parametrization.ilpol;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.dockinds.dictionary.AbstractDocumentKindDictionary;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.QueryForm.SortField;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.OrderClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

/**
 * S�ownik wniosk�w dla dokument�w leasignowych.
 * 
 * @author Mariusz Kiljanczyk
 */
public class DlApplicationDictionary extends AbstractDocumentKindDictionary
{
	private static final Logger log = LoggerFactory.getLogger(DlApplicationDictionary.class);
	private StringManager sm = GlobalPreferences.loadPropertiesFile(DlApplicationDictionary.class.getPackage().getName(), null);

	private Long id;
	private String numerWniosku;
	private Long idKlienta;
	private String opis;
	private Float kwota;
	private Float wartosc;
	private Integer liczbaRat;
	private Float wykup;
	private String rodzaj;
	private Long contractId;
	private static ArrayList<Long> contractIds;
	private Long idDostawcy;
	private String status;
	private Long leasingObjectType;
	private static final List<String> rodzajList = new ArrayList<String>();

	private static DlApplicationDictionary instance;
	public static synchronized DlApplicationDictionary getInstance()
	{
		if (instance == null)
			instance = new DlApplicationDictionary();
		return instance;
	}
	
	static
	{
		rodzajList.add("finansowy");
		rodzajList.add("operacyjny");
	}

	public DlApplicationDictionary()
	{}

	public String dictionaryAction()
	{
		return "/office/common/dlApplicationDictionary.action";
	}

	/**
	 * Zwraca stringa zawieraj�cego kod javascript kt�ry wykona si� w funkcji
	 * accept_cn
	 * 
	 */
	public String dictionaryAccept()
	{
		return "accept_application(map);";
	}

	public Map<String, String> dictionaryAttributes()
	{
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("numerWniosku", "");
		return map;
	}

	public String getDictionaryDescription()
	{
		return numerWniosku;
	}

	/**
	 * Zapisuje strone w sesji hibernate
	 * 
	 * @throws EdmException
	 */
	public void create() throws EdmException
	{
		try
		{
			DSApi.context().session().save(this);
			DSApi.context().session().flush();
		}
		catch (HibernateException e)
		{
			log.error("Blad dodania wniosku dokumentu leasingowego. " + e.getMessage(),e);
			throw new EdmException(sm.getString("BladDodaniaWnioskuDokumentuLeasingowego") + e.getMessage());
		}
	}
	
	 public ArrayList<Long> getContracts(Long applicationId)
	    {
	    	PreparedStatement ps = null;
	    	ArrayList<Long> a = new ArrayList<Long>();
	    	try
	    	{
	    		ps = DSApi.context().prepareStatement("select id from dl_contract_dictionary where application = ?");
	    		ps.setLong(1, applicationId);
	    		ResultSet rs = ps.executeQuery();
	    		while(rs.next())
	    		{
	    			a.add(rs.getLong(1));
	    		}
	    		rs.close();
	    		ps.close();
	    		    		
	    	}
	    	catch (Exception e)
	    	{
				log.error(e.getMessage(),e);
			}
	    	finally
	    	{
	    		DbUtils.closeQuietly(ps);
	    	}
	    	return a;
	    }

	public DlApplicationDictionary find(Long id) throws EdmException
	{
		StringManager smL = GlobalPreferences.loadPropertiesFile(DlApplicationDictionary.class.getPackage().getName(), null);
		DlApplicationDictionary inst = DSApi.getObject(DlApplicationDictionary.class, id);
		if (inst == null)
			throw new EdmException(smL.getString("NieZnalezionoWnioskuOnr", id));
		else
			return inst;
	}

	public static List<DlApplicationDictionary> findByNumerWniosku(String numerWniosku) throws EdmException
	{
		Criteria c = DSApi.context().session().createCriteria(DlApplicationDictionary.class);
		c.add(Restrictions.eq("numerWniosku", numerWniosku).ignoreCase());
		return (List<DlApplicationDictionary>) c.list();
	}

	public void delete() throws EdmException
	{
		try
		{
			DSApi.context().session().delete(this);
			DSApi.context().session().flush();
		}
		catch (HibernateException e)
		{
			log.error("Blad usuniecia wniosku dokumentu leasingowego");
			throw new EdmException(sm.getString("BladUsunieciaWnioskuDokumentuLeasingowego") + e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	public static SearchResults<? extends DlApplicationDictionary> search(QueryForm form)
	{
		try
		{
			FromClause from = new FromClause();
            final TableAlias table = from.createTable("DL_APPLICATION_DICTIONARY");
            
            final WhereClause where = new WhereClause();

			if (form.hasProperty("numerWniosku"))
			{
				where.add(Expression.like(table.attribute("numerWniosku"),"%"+(String)form.getProperty("numerWniosku")+"%",true));
			}
			if (form.hasProperty("idKlienta"))
			{
				where.add(Expression.eq(table.attribute("idKlienta"),form.getProperty("idKlienta")));
			}
			if (form.hasProperty("idDostawcy"))
			{
				where.add(Expression.eq(table.attribute("idDostawcy"),form.getProperty("idDostawcy")));
			}
			if(form.hasProperty("idUmowy"))
			{
				where.add(Expression.eq(table.attribute("idUmowy"),form.getProperty("idUmowy")));
			}
			if (form.hasProperty("opis"))
			{
				where.add(Expression.like(table.attribute("opis"),"%"+(String)form.getProperty("opis")+"%",true));
			}
			if (form.hasProperty("wartosc"))
			{
				where.add(Expression.eq(table.attribute("wartosc"),form.getProperty("wartosc")));
			}
			if (form.hasProperty("liczbaRat"))
			{
				where.add(Expression.eq(table.attribute("liczbaRat"),form.getProperty("liczbaRat")));
			}
			if (form.hasProperty("wykup"))
			{
				where.add(Expression.eq(table.attribute("wykup"),form.getProperty("wykup")));
			}
			if (form.hasProperty("rodzaj"))
			{
				where.add(Expression.eq(table.attribute("rodzaj"),form.getProperty("rodzaj")));
			}
			
			  //Uprawnienia 
            if(!DSApi.context().hasPermission(DSPermission.CONTRACTOR_ALL))
            {
            	log.error("NIE MA "+DSPermission.CONTRACTOR_ALL);
            	if(!DSApi.context().hasPermission(DSPermission.CONTRACTOR_REGION))
            	{
            		log.error("NIE MA "+DSPermission.CONTRACTOR_REGION);
            		if(!DSApi.context().hasPermission(DSPermission.CONTRACTOR_USER))
            		{
            			log.error("NIE MA "+DSPermission.CONTRACTOR_USER);
            			return null;
            		}
            		else
            		{
            			log.error("MA "+DSPermission.CONTRACTOR_USER);
            			final TableAlias conTable = from.createTable("dsc_contractor");
            			where.add(Expression.eqAttribute(table.attribute("idKlienta"), conTable.attribute("id")));
            			final TableAlias tabRef = from.createTable("dsc_patron");
                    	where.add(Expression.eqAttribute(tabRef.attribute("CONTRACTORID"), conTable.attribute("ID")));
                    	where.add(Expression.eq(tabRef.attribute("USERNAME"), DSApi.context().getPrincipalName())); 
            		}
            	}
            	else
            	{
            		List<String> users = new ArrayList<String>();
            		for (DSDivision division : DSApi.context().getDSUser().getDivisions()) 
                    {
            			for (DSUser user : division.getUsers())
            			{
            				users.add(user.getName());
						}
            		}
            		
            		final TableAlias conTable = from.createTable("dsc_contractor");
        			where.add(Expression.eqAttribute(table.attribute("idKlienta"), conTable.attribute("id")));
        			final TableAlias tabRef = from.createTable("dsc_patron");
                	where.add(Expression.eqAttribute(tabRef.attribute("CONTRACTORID"), conTable.attribute("ID")));
                	where.add(Expression.in(tabRef.attribute("USERNAME"), users.toArray()));  
//            		log.error("MA "+DSPermission.CONTRACTOR_REGION);
//            		final TableAlias contractorTable = from.createJoinedTable("dsc_contractor", true, table, FromClause.JOIN,"$l.idKlienta = $r.id" );
//                	final TableAlias regionTable = from.createJoinedTable("dsc_region", true, contractorTable, FromClause.LEFT_OUTER,"$l.region = $r.id" );
//                	
//                	Junction OR = pl.compan.docusafe.util.querybuilder.expression.Expression.disjunction();
//                	for (DSDivision division : DSApi.context().getDSUser().getDivisions()) 
//                	{
//                		//grupy uprawnie� s� tworzone na podtswie cn regionu (DV_+CN_REGIONU) i dlatego mozemy to tutaj tak upro�ci� 
//                		//dodanie niepotrzebnych grup nie zaszkodzi
//                		OR.add(Expression.eq(regionTable.attribute("CN"),division.getGuid().substring(3)));
//    				}         
//                    where.add(OR);
            	}
            }
			
            SelectClause selectId = new SelectClause(true);
            SelectColumn idCol = selectId.add(table, "id");
            OrderClause order = new OrderClause();
            List<SortField> l = form.getSortFields();
            for (SortField sortField : l) 
            {
            	order.add(table.attribute(sortField.getName()), sortField.isAscending());
            	selectId.add(table, sortField.getName());
			}
            
            SelectClause selectCount = new SelectClause();
            SelectColumn countCol = selectCount.addSql("count(distinct "+table.getAlias()+".id)");
            SelectQuery selectQuery;
            

            selectQuery = new SelectQuery(selectCount, from, where, null);
            ResultSet rs = selectQuery.resultSet();
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby zada�.");
            int totalCount = rs.getInt(countCol.getPosition());
            rs.close();
            DSApi.context().closeStatement(rs.getStatement());
            
            selectQuery = new SelectQuery(selectId, from, where, order);
            if (form.getLimit() > 0)
            {
                selectQuery.setMaxResults(form.getLimit());
                selectQuery.setFirstResult(form.getOffset());
            }
            rs = selectQuery.resultSet(DSApi.context().session().connection());
            List<DlApplicationDictionary> results = new ArrayList<DlApplicationDictionary>(form.getLimit());
            DlApplicationDictionary temp;
            while (rs.next())
            {
                Long id = new Long(rs.getLong(idCol.getPosition()));
                temp = DSApi.context().load(DlApplicationDictionary.class, id);
                results.add(temp);
            }
            rs.close();
            DSApi.context().closeStatement(rs.getStatement());

            return new SearchResultsAdapter<DlApplicationDictionary>(results, form.getOffset(), totalCount,
            		DlApplicationDictionary.class);
			
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return SearchResultsAdapter.emptyResults(DlApplicationDictionary.class);
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Long getIdKlienta()
	{
		return idKlienta;
	}

	public void setIdKlienta(Long idKlienta)
	{
		this.idKlienta = idKlienta;
	}

	public Float getKwota()
	{
		return kwota;
	}

	public void setKwota(Float kwota)
	{
		this.kwota = kwota;
	}

	public String getNumerWniosku()
	{
		return numerWniosku;
	}

	public void setNumerWniosku(String numerWniosku)
	{
		this.numerWniosku = numerWniosku;
	}

	public Integer getliczbaRat()
	{
		return liczbaRat;
	}

	public void setliczbaRat(Integer liczbaRat)
	{
		this.liczbaRat = liczbaRat;
	}

	public String getOpis()
	{
		return opis;
	}

	public void setOpis(String opis)
	{
		this.opis = opis;
	}

	public String getRodzaj()
	{
		return rodzaj;
	}

	public void setRodzaj(String rodzaj)
	{
		this.rodzaj = rodzaj;
	}

	public Float getWartosc()
	{
		return wartosc;
	}

	public void setWartosc(Float wartosc)
	{
		this.wartosc = wartosc;
	}

	public Float getWykup()
	{
		return wykup;
	}

	public void setWykup(Float wykup)
	{
		this.wykup = wykup;
	}

	public static List<String> getRodzajList()
	{
		return rodzajList;
	}

	public Integer getLiczbaRat()
	{
		return liczbaRat;
	}

	public void setLiczbaRat(Integer liczbaRat)
	{
		this.liczbaRat = liczbaRat;
	}

	public void setIdDostawcy(Long idDostawcy)
	{
		this.idDostawcy = idDostawcy;
	}

	public Long getIdDostawcy()
	{
		return idDostawcy;
	}

	public Long getContractId() {
		return contractId;
	}

	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getLeasingObjectType() {
		return leasingObjectType;
	}

	public void setLeasingObjectType(Long leasingObjectType) {
		this.leasingObjectType = leasingObjectType;
	}

	@Override
	public Map<String, String> popUpDictionaryAttributes() {
		// TODO Auto-generated method stub
		return null;
	}
}
