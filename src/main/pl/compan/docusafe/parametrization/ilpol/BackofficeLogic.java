package pl.compan.docusafe.parametrization.ilpol;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.DateUtils;

/**
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class BackofficeLogic extends AbstractDocumentLogic
{
    private static BackofficeLogic instance;

    public static BackofficeLogic getInstance()
    {
        if (instance == null)
            instance = new BackofficeLogic();
        return instance;
    }

    // nazwy kodowe poszczeg�lnych p�l dla tego rodzaju dokumentu
    public static final String KATEGORIA_FIELD_CN = "KATEGORIA";
    public static final String DATA_FAKTURY_FIELD_CN = "DATA_FAKTURY";
    public static final String STATUS_FIELD_CN = "STATUS";
    public static final String CENTRUM_KOSZTOW_FIELD_CN = "CENTRUM_KOSZTOW";
    public static final String AKCEPTACJA_FINALNA_FIELD_CN = "AKCEPTACJA_FINALNA";
    public static final String NUMER_FAKTURY_FIELD_CN = "NUMER_FAKTURY";

    // nazwy kodowe poszczeg�lnych kategorii dokumentu

    // GUID-y poszczeg�lnych grup

    //  statusy
    private static final Integer STATUS_W_TOKU = 10;
    private static final Integer STATUS_ZAPLACONE = 20;
    private static final Integer STATUS_ZAPLACONE_PRZEDPLATA = 30;
    private static final Integer STATUS_ODRZUCONE = 40;
    
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
    {
          
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

        EnumItem kategoriaItem = fm.getEnumItem(KATEGORIA_FIELD_CN);
        
        List<String> path = new ArrayList<String>();
        
        document.setFolder(Folder.getRootFolder());
        
        path.add(kategoriaItem.getTitle());
        Date date = null;
        if (fm.getValue(DATA_FAKTURY_FIELD_CN) != null)
            date = (Date) fm.getValue(DATA_FAKTURY_FIELD_CN);
        else
            date = document.getCtime();
        if (date != null)
        {
            path.add(DateUtils.formatYearMonth(date));
        }
        
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent("Dokumenty backoffice");
        
        for (Iterator iter = path.iterator(); iter.hasNext();)
        {
            String pathSegment = (String) iter.next();
            // TODO: uprawnienia do folderow obczaic jakie
            DSApi.context().grant(folder, new String[]{ObjectPermission.READ}, "*", ObjectPermission.ANY);
            folder = folder.createSubfolderIfNotPresent(pathSegment);
        }
        
        document.setFolder(folder);
        String nrFaktury = (String) fm.getKey(NUMER_FAKTURY_FIELD_CN);
        String title = kategoriaItem.getTitle() + (nrFaktury != null ? " (" + nrFaktury + ")" : ""); 
        document.setTitle(title);
        document.setDescription(document.getTitle());
        if (document instanceof OfficeDocument)
        {
            ((OfficeDocument) document).setSummary(document.getTitle());
        }
    }

    public void documentPermissions(Document document) throws EdmException
    {
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");
        
        DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "*", ObjectPermission.ANY);
        try 
        {
            DSApi.context().session().flush();
        }
        catch (HibernateException ex) 
        {
            throw new EdmHibernateException(ex);
        }
    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
        Map<String,Object> values = new HashMap<String,Object>();
        values.put(STATUS_FIELD_CN, STATUS_W_TOKU);
        fm.reloadValues(values);
    }

    
    @Override
    public void checkCanFinishProcess(OfficeDocument document) throws AccessDeniedException, EdmException
    {
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");
        
        String error = "Nie mo�na zako�czy� pracy z dokumentem: ";
        
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        Integer statusId = (Integer) fm.getKey(STATUS_FIELD_CN);
        //Integer centrumId = (Integer) fm.getKey(CENTRUM_KOSZTOW_CN);
        Boolean akcFinalna = (Boolean) fm.getKey(AKCEPTACJA_FINALNA_FIELD_CN);
        if (!STATUS_ODRZUCONE.equals(statusId) && (akcFinalna == null || !akcFinalna))
        {
            throw new AccessDeniedException(error+"Status musi by� ustawiony jako Odrzucony lub dokument musi by� zaakceptowany finalnie");
        }
    }       
}
