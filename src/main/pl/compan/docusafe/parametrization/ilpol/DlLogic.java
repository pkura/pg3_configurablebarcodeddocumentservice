package pl.compan.docusafe.parametrization.ilpol;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.dbutils.DbUtils;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.permission.DlPermissionManager;
import pl.compan.docusafe.core.base.permission.DlPermissionPolicy;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.base.permission.PermissionPolicy;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.tiff.ImageKit;
/**
 * Dokument Leasingowy dla impulsa
 * 
 * @author Mariusz Kiljanczyk
 */
public class DlLogic extends AbstractDocumentLogic
{
	private static Logger log  = LoggerFactory.getLogger(DlLogic.class);
    private static DlLogic instance;

    public static DlLogic getInstance()
    {
        if (instance == null)
            instance = new DlLogic();
        return instance;
    }

    // nazwy kodowe poszczeg�lnych p�l dla tego rodzaju dokumentu
    public static final String KLIENT_CN = "KLIENT";
    public static final String NUMER_UMOWY_CN = "NUMER_UMOWY";
    public static final String NUMER_WNIOSKU_CN = "NUMER_WNIOSKU";
    public static final String RODZAJ_DOKUMENTU_CN = "RODZAJ_DOKUMENTU";
    public static final String TYP_DOKUMENTU_CN = "TYP_DOKUMENTU";
    public static final String DATA_DOKUMENTU_CN = "DATA_DOKUMENTU";
    public static final String STATUS_DOKUMENTU_CN = "STATUS_DOKUMENTU";
    public static final String NUMER_DOKUMENTU_CN = "NUMER_DOKUMENTU";
    public static final String KWOTA_CN = "KWOTA";
    
    public static final Integer RODZAJ_KONTRAHENTA_KLIENT = 10;
    public static final Integer RODZAJ_KONTRAHENTA_DOSTAWCA= 20;
    
    
    public static final String RODZAJ_RAPORTU_CN = "RODZAJ_RAPORTU";
    public static final String RODZAJ_DW_CN = "RODZAJ_DW";
    public static final String RODZAJ_DL_CN = "RODZAJ_DL";
    public static final String RODZAJ_DZ_CN = "RODZAJ_DZ";
    public static final String RODZAJ_DRU_CN = "RODZAJ_DRU";
    public static final String RODZAJ_FILPOL_CN = "RODZAJ_FILPOL";
    public static final String RODZAJ_DEKLARACJI_CN = "RODZAJ_DEKLARACJI";
    public static final String RODZAJ_DOKUMENTU_FINANSOWEGO_CN = "RODZAJ_DOKUMENTU_FINANSOWEGO";
    public static final String RODZAJ_ZPL_CN = "RODZAJ_ZPL";
    
    // nazwy kodowe poszczeg�lnych rodzajow dokumentu
    public static final String RD_DOKUMENT_REJESTROWY_CN = "DOKUMENT_REJESTROWY";
    public static final String RD_DOKUMENT_FINANSOWE_CN = "DOKUMENT_FINANSOWE";
    public static final String RD_WNIOSKI_I_FORMULARZE_CN = "WNIOSKI_I_FORMULARZE";
    public static final String RD_DOKUMENTY_DODATKOWE_CN = "DOKUMENTY_DODATKOWE";
    public static final String RD_DOKUMENTY_UMOWY_KLIENTA_CN = "DOKUMENTY_UMOWY_KLIENTA";
    
    // nazwy kodowe poszczeg�lnych typow dokumentu
        //OKUMENT_REJESTROWY
    public static final String TD_KRS_CN = "KRS";
    public static final String TD_REGON_CN = "REGON";
    public static final String TD_NIP_CN = "NIP";
    public static final String TD_EDG_CN = "EDG";
    public static final String TD_DOWOD_OSOBISTY_CN = "DOWOD_OSOBISTY";
    public static final String TD_UMOWY_SPOLKI_CN = "UMOWY_SPOLKI";
    public static final String TD_INNE_CN = "INNE";
        //DOKUMENT_FINANSOWE
    public static final String TD_RAPORTY_CN = "RAPORTY";
    public static final String TD_DEKLARACJE_PODATKOWE_CN = "DEKLARACJE_PODATKOWE";
    public static final String TD_INNE_DOKUMETY_FINANSOWE_CN = "INNE_DOKUMETY_FINANSOWE";
        //WNIOSKI_I_FORMULARZE
    public static final String TD_ARKUSZ_DECYZYJNY_IMPULS_KEASING_CN = "ARKUSZ_DECYZYJNY_IMPULS_KEASING";
    public static final String TD_WNIOSEK_O_PRZYZNANIE_LEASINGU_CN = "WNIOSEK_O_PRZYZNANIE_LEASINGU";
    public static final String TD_FORMULARZ_OSOBISTY_CN = "FORMULARZ_OSOBISTY";
    public static final String TD_ZALACZNIK_O_PRZYZNANIE_LEASINGU_DLA_SPH_CN = "ZALACZNIK_O_PRZYZNANIE_LEASINGU_DLA_SPH";
    public static final String TD_OPINIA_O_KLIENCIE_CN = "OPINIA_O_KLIENCIE";
    public static final String TD_LEASING_APPLICATION_CN = "LEASING_APPLICATION";
    public static final String TD_NOTATKA_Z_WIZYTY_U_KLIENTA_CN = "NOTATKA_Z_WIZYTY_U_KLIENTA";
        //DOKUMENTY_DODATKOWE
    public static final String TD_DOKUMENTY_WSPOLPRACY_CN = "DOKUMENTY_WSPOLPRACY";
    public static final String TD_KONCESJE_I_ZEZWOLENIA_CN = "KONCESJE_I_ZEZWOLENIA";
    public static final String TD_OPINIA_BANKU_CN = "OPINIA_BANKU";
    
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
    {
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");
        
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        
        if(fm.getValue(TYP_DOKUMENTU_CN) != null)
        {
        	
                document.setTitle(fm.getValue(TYP_DOKUMENTU_CN).toString());
        }
        else
        {
        	 document.setTitle("Dokumenty leasingowe");
        }
        
        
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent("Dokumenty leasingowe");
        
        Contractor con = (Contractor)fm.getValue(KLIENT_CN);
        String nazwa = "";
        if(con != null)
        {
        	nazwa = con.getName();
	        nazwa = nazwa.toLowerCase();	        
	        folder = folder.createSubfolderIfNotPresent(FolderInserter.to1Letters(nazwa));
	        folder = folder.createSubfolderIfNotPresent(((Contractor)fm.getValue(KLIENT_CN)).getName());
	        folder.setAnchor(true);
	        if(fm.getValue(RODZAJ_DOKUMENTU_CN) != null)
	        {
		        folder = folder.createSubfolderIfNotPresent(fm.getValue(RODZAJ_DOKUMENTU_CN).toString());
	        
		        if(fm.getEnumItemCn(RODZAJ_DOKUMENTU_CN).toString().equals(RD_DOKUMENTY_UMOWY_KLIENTA_CN))
		        {
		            List<DlContractDictionary> list = (List<DlContractDictionary>) fm.getValue(NUMER_UMOWY_CN);
		            if (list.size() > 0)
		                folder = folder.createSubfolderIfNotPresent(list.get(0).getNumerUmowy());   
		        }    
	        }
        }
        document.setFolder(folder);
    }
    
	public PermissionPolicy getPermissionPolicy() 
	{
		return new DlPermissionPolicy();
	}
	
    public int getPermissionPolicyMode()
    {
        return PermissionManager.DL_POLICY;
    }

    public void documentPermissions(Document document) throws EdmException
    {
    	String region = null;
    	java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
    	if(fm.getValue(KLIENT_CN) != null && ((Contractor)fm.getValue(KLIENT_CN)).getRegion() != null)
		{
			if(((Contractor)fm.getValue(KLIENT_CN)).getRegion() != null)
			{
				region = ((Contractor)fm.getValue(KLIENT_CN)).getRegion().getCn();
			}
        }
    	documentPermissions(document,region,fm,perms);
    }
    
    
    public void documentPermissions(Document document,String region,FieldsManager fm,java.util.Set<PermissionBean> perms) throws EdmException
    {
    	try
    	{
	        Long numerwniosku = null;
	        Long numwerUmowy = null;
	        if(fm.getKey(NUMER_WNIOSKU_CN) != null && ((List)fm.getKey(NUMER_WNIOSKU_CN)).size() > 0)
	        	numerwniosku = (Long) ((List)fm.getKey(NUMER_WNIOSKU_CN)).get(0);
	        if(fm.getKey(NUMER_UMOWY_CN) != null && ((List)fm.getKey(NUMER_UMOWY_CN)).size() > 0)
	        	numwerUmowy = (Long) ((List)fm.getKey(NUMER_UMOWY_CN)).get(0);
	        
	        Map<String,String> map = DlPermissionManager.getUsers(numerwniosku,numwerUmowy);
	        Set<String> set = map.keySet();
	        for (String key : set)
			{
	            perms.add(new PermissionBean(ObjectPermission.READ, map.get(key), ObjectPermission.USER));
	            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, map.get(key), ObjectPermission.USER));
			}
    	}
    	catch (Exception e) {
			log.error(e.getMessage(),e);
		}
    	documentPermissions(document,region,perms);
    }
    
    public void documentPermissions(Document document,String region,java.util.Set<PermissionBean> perms) throws EdmException
    {
    
		
		perms.add(new PermissionBean(ObjectPermission.READ, "DL_READ", ObjectPermission.GROUP));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_READ", ObjectPermission.GROUP));
        //teczki
        //perms.add(new PermissionBean(ObjectPermission.READ, "DL_BINDER_DS", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ, "DL_BINDER_DR", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ, "DL_BINDER_DO", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ, "DL_BINDER_DK", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ, "DL_BINDER_DU", ObjectPermission.GROUP));
        //perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_BINDER_DS", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_BINDER_DR", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_BINDER_DO", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_BINDER_DK", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_BINDER_DU", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ,"DL_ADMIN", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ,"DV_ALL", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ,"DL_DEL_ATTA", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "DL_ADMIN", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "DV_ALL", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "DL_DEL_ATTA", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_ADMIN", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DV_ALL", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_DEL_ATTA", ObjectPermission.GROUP));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DL_ADMIN", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DV_ALL", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DL_DEL_ATTA", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ, "DL_DEL_DOC", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY,"DL_DEL_DOC", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_DEL_DOC", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DL_DEL_DOC", ObjectPermission.GROUP));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DL_DEL_DOC", ObjectPermission.GROUP));
		
        if(region != null && region.length() > 0)
        {      	
        	perms.add(new PermissionBean(ObjectPermission.READ, "DV_" +	region, ObjectPermission.GROUP));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DV_" +	region, ObjectPermission.GROUP));
        }
        perms.add(new PermissionBean(ObjectPermission.READ, document.getAuthor() , ObjectPermission.USER));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getAuthor() , ObjectPermission.USER));
        this.setUpPermission(document, perms);
    }
    
    /***
     * Metoda zwraca list� id dokument�w w kt�rych znajduje si� dany kontrahent
     */
    public static List<Long> getDocumentsByContractorId(Long contractorId)
    {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	List<Long> docs = new ArrayList<Long>();
    	try
    	{
    		ps = DSApi.context().prepareStatement("select DOCUMENT_ID from dsg_leasing where KLIENT = ?");
    		ps.setLong(1, contractorId);
    		rs = ps.executeQuery();
    		while (rs.next()) 
    		{
				docs.add(rs.getLong("DOCUMENT_ID"));
			}
    	}
    	catch (Exception e) 
    	{
			log.error("",e);
		}
    	finally
    	{
    		DbUtils.closeQuietly(rs);
    		DbUtils.closeQuietly(ps);
    	}
    	return docs;
    }
    
    /***
     * Metoda usuwa powi�zania dokumentu z umowami 
     */
    public static void removeContractsFromDocument(Long contractId, Long documentId)
    {
    	PreparedStatement ps = null;
    	try
    	{
    		ps = DSApi.context().prepareStatement("delete from dsg_leasing_multiple_value  where field_cn = 'NUMER_UMOWY' and field_val = ? and document_id = ?");
    		ps.setLong(1, contractId);
    		ps.setLong(2, documentId);
    		ps.executeQuery();
    	}
    	catch (Exception e) 
    	{
			log.error(e.getMessage(),e);
		}
    	finally
    	{
    		DbUtils.closeQuietly(ps);
    	}
    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
        Map<String,Object> values = new HashMap<String,Object>();
        values.put(STATUS_DOKUMENTU_CN, 20);
        fm.reloadValues(values);
    }

    public boolean canReadDocumentDictionaries() throws EdmException    
    {
        return DSApi.context().hasPermission(DSPermission.DL_SLOWNIK_ODCZYTYWANIE);
    }


	public String getPhoneNumber(Document document) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		String number = null;
		if(fm.getValue(KLIENT_CN) != null)
		{
			number =  ((Contractor)fm.getValue(KLIENT_CN)).getPhoneNumber();
			number = number.replaceAll("-", "");
			number = number.replaceAll(" ", "");
			number = number.trim();
			if(number.startsWith("0"))
				number = number.substring(1);			
		}
		return number;
	}
	
	public static List<Field> getFields(DocumentKind kind) throws EdmException
	{
		List<Field> fields = new ArrayList<Field>();
		fields.add(kind.getFieldByCn(DlLogic.TYP_DOKUMENTU_CN));
//		fields.add(kind.getFieldByCn(DlLogic.RODZAJ_DEKLARACJI_CN));                        
//		fields.add(kind.getFieldByCn(DlLogic.RODZAJ_DL_CN));                        
//        fields.add(kind.getFieldByCn(DlLogic.RODZAJ_DOKUMENTU_FINANSOWEGO_CN));                        
//        fields.add(kind.getFieldByCn(DlLogic.RODZAJ_DRU_CN));                        
//        fields.add(kind.getFieldByCn(DlLogic.RODZAJ_DW_CN));                        
//        fields.add(kind.getFieldByCn(DlLogic.RODZAJ_DZ_CN));                        
//        fields.add(kind.getFieldByCn(DlLogic.RODZAJ_FILPOL_CN));                        
//        fields.add(kind.getFieldByCn(DlLogic.RODZAJ_RAPORTU_CN));                        
//        fields.add(kind.getFieldByCn(DlLogic.RODZAJ_ZPL_CN));
		return fields;
	}
	
	public static List<EnumItem> getEnumItms(DocumentKind kind) throws EdmException
	{
		Field f = kind.getFieldByCn(DlLogic.TYP_DOKUMENTU_CN);
		List<EnumItem> types = f.getEnumItems();
//		
//		types.remove(f.getEnumItem(80));
//		types.remove(f.getEnumItem(90));
//		types.remove(f.getEnumItem(100));
//		types.remove(f.getEnumItem(220));
//		types.remove(f.getEnumItem(221));
//		types.remove(f.getEnumItem(222));
//		types.remove(f.getEnumItem(223));
//		types.remove(f.getEnumItem(224));
//		types.remove(f.getEnumItem(227));
		
//        f = kind.getFieldByCn(DlLogic.RODZAJ_DEKLARACJI_CN);                        
//        types.addAll(f.getEnumItems());
//        f = kind.getFieldByCn(DlLogic.RODZAJ_DL_CN);                        
//        types.addAll(f.getEnumItems());
//        f = kind.getFieldByCn(DlLogic.RODZAJ_DOKUMENTU_FINANSOWEGO_CN);                  
//        types.addAll(f.getEnumItems());
//        f = kind.getFieldByCn(DlLogic.RODZAJ_DRU_CN);                        
//        types.addAll(f.getEnumItems());
//        f = kind.getFieldByCn(DlLogic.RODZAJ_DW_CN);                        
//        types.addAll(f.getEnumItems());
//        f = kind.getFieldByCn(DlLogic.RODZAJ_DZ_CN);                        
//        types.addAll(f.getEnumItems());
//        f = kind.getFieldByCn(DlLogic.RODZAJ_FILPOL_CN);                        
//        types.addAll(f.getEnumItems());
//        f = kind.getFieldByCn(DlLogic.RODZAJ_RAPORTU_CN);                        
//        types.addAll(f.getEnumItems());
//        f = kind.getFieldByCn(DlLogic.RODZAJ_ZPL_CN);                        
//        types.addAll(f.getEnumItems());
        //Collections.sort(types,new EnumItem.EnumItemComparator());
        
        return types;
	}
	
	public static Integer getImportKind(Integer type) throws EntityNotFoundException
	{
		DockindInfoBean dif = DocumentKind.getDockindInfo(DocumentLogicLoader.DL_KIND);
		String types = dif.getProperties().get("atta-ass-pdf");
		String cn = dif.getDockindFieldsMap().get(TYP_DOKUMENTU_CN).getEnumItem(type).getCn();
		if(types.contains(cn))
		{
			return ImageKit.IMPORT_KIND_PDF;
		}
		return ImageKit.IMPORT_KIND_GRAY_TIFF;
	}
    public String getInOfficeDocumentKind()
    {
        return "Dokument leasingowy";
    }
}
