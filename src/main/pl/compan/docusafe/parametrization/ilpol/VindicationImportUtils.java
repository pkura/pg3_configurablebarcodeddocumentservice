package pl.compan.docusafe.parametrization.ilpol;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.InOfficeDocumentStatus;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa ImportUtils.java
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 */
public class VindicationImportUtils 
{
	private static Logger log = LoggerFactory.getLogger("VINDICATIONIMPORT");
	public static final String ID_WEZWANIA ="ID_WEZWANIA";
	public static final String NR_WEZWANIA ="NR_WEZWANIA";
	public static final String ID_KLIENTA ="ID_KLIENTA";
	public static final String SYMBOL_KLIENTA ="SYMBOL_KLIENTA";
	public static final String NAZWA_KLIENTA ="NAZWA_KLIENTA";
	public static final String ADRES_KLIENTA ="ADRES_KLIENTA";
	public static final String POCZTA ="POCZTA";
	public static final String NIP ="NIP";
	public static final String DATA_WEZWANIA ="DATA_WEZWANIA";
	public static final String NALEZNOSC ="NALEZNOSC";
	public static final String NALEZNOSC_PIERW ="NALEZNOSC_PIERW";
	public static final String OPIEKUN ="OPIEKUN";
	public static final String CZY_HANDLOWIEC_AKTYWNY ="CZY_HANDLOWIEC_AKTYWNY";
	public static final String LASTNAME ="LASTNAME";
	public static final String FIRSTNAME ="FIRSTNAME";
	public static final String UWAGI ="UWAGI";
	
	public static void setValues(ResultSet rs,Map<String,Object> values) throws EdmException, SQLException
	{                	
		String nip = rs.getString(NIP);
		nip = nip.replaceAll("\"", "");
		nip = nip.replaceAll("-", "");
		List<Contractor> conList = Contractor.findByNIP(nip);
		if(conList != null && conList.size() > 0)
		{
			values.put(CrmVindicationLogic.NUMER_KONTRAHENTA_FIELD_CN, conList.get(0).getId());
		}	                	
		values.put(CrmVindicationLogic.NUMER_WEZWANIA_FIELD_CN, rs.getString(NR_WEZWANIA));
		
		Date date = null;
		try
		{
			date = DateUtils.parseDateAnyFormat(rs.getString(DATA_WEZWANIA).replaceAll("\"", ""));
		}
		catch (Exception e) 
		{
			new EdmException("B��d parsowania daty "+ rs.getString(DATA_WEZWANIA));
		}
		values.put(CrmVindicationLogic.DATA_WEZWANIA_FIELD_CN, date); 
		values.put(CrmVindicationLogic.NALEZNOSC_PIERWOTNA_FIELD_CN, rs.getString(NALEZNOSC_PIERW));
		values.put(CrmVindicationLogic.NALEZNOSC_FIELD_CN, rs.getString(NALEZNOSC));   
	}
	
	
	public static void createTask(String summary,Map<String,Object> values,DSUser user,String remark,InOfficeDocument doc) throws EdmException
    {
		log.debug("TIME6 :"+ new Date().getTime());
        DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.CRM_VINDICATION_KIND);
        doc.setDocumentKind(documentKind);
        doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
        doc.setDivisionGuid(DSDivision.ROOT_GUID);
        doc.setCurrentAssignmentAccepted(Boolean.FALSE);
        doc.setCreatingUser(DSApi.context().getPrincipalName());
        doc.setSummary(summary);
        doc.setForceArchivePermissions(false);
        doc.setAssignedDivision(DSDivision.ROOT_GUID);
        doc.setClerk(DSApi.context().getPrincipalName());
        doc.setSender(new Sender());
        doc.setSource("crm");
        
        List<InOfficeDocumentKind> kinds = InOfficeDocumentKind.list();
        String kindName = documentKind.logic().getInOfficeDocumentKind();
        boolean canChooseKind = (kindName == null);
        if (!canChooseKind)
        {                       
            for (InOfficeDocumentKind inKind : kinds)
            {
                if (kindName.toUpperCase().equals(inKind.getName().toUpperCase()))
                	doc.setKind(InOfficeDocumentKind.find(inKind.getId()));
            }
        }
        if(doc.getKind() == null)
        {
        	doc.setKind(InOfficeDocumentKind.find(1));            	
        }
        Calendar currentDay = Calendar.getInstance();
        currentDay.setTime(GlobalPreferences.getCurrentDay());
        doc.setIncomingDate(currentDay.getTime());
        doc.setDocumentDate(new Date());
        doc.setCurrentAssignmentAccepted(Boolean.FALSE);
        doc.setOriginal(true);
        doc.setStatus(InOfficeDocumentStatus.findByCn("PRZYJETY"));
        log.debug("TIME7 :"+ new Date().getTime());
        doc.create();
        log.debug("TIME8 :"+ new Date().getTime());
        Journal journal = Journal.getMainIncoming();
        Long journalId;
        journalId = journal.getId();
        Integer sequenceId = null;
        sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), new Date(currentDay.getTime().getTime()));
        
        doc.bindToJournal(journalId, sequenceId);
        
        Long newDocumentId = doc.getId();
        
        doc.setDocumentKind(documentKind);
        newDocumentId = doc.getId();
        log.debug("TIME8B:"+ new Date().getTime());
        documentKind.set(newDocumentId, values);
        log.debug("TIME8C:"+ new Date().getTime());
        documentKind.logic().archiveActions(doc, DocumentLogic.TYPE_IN_OFFICE);
        log.debug("TIME8D:"+ new Date().getTime());
        documentKind.logic().documentPermissions(doc);
        log.debug("TIME8E:"+ new Date().getTime());
        DSApi.context().session().flush();
        try
    ///    {
    //    	for(int i=0; i<remark.length(); i=i+4000)
        	{
        		String spLine = StringUtils.left(remark, 4000);
        		Remark rem = new Remark(spLine,DSApi.context().getPrincipalName());
            	doc.addRemark(rem);
        	}
  //      }
        catch (Exception e) 
        {
        	log.error(e.getMessage(),e);
			log.error("REMARK : "+ remark);
			throw new EdmException(e);
		}
        log.debug("TIME8F:"+ new Date().getTime());
        
        String obj = "Windykacja"; 
        log.debug("TIME9 :"+ new Date().getTime());
        if(user == null)
        	WorkflowFactory.createNewProcess(doc, false, obj);     
        else
        	WorkflowFactory.createNewProcess(doc, false, obj, user.getName(),
        		(user.getDivisions() != null && user.getDivisions().length > 0 ? user.getDivisions()[0].getGuid() : DSDivision.ROOT_GUID ), user.getName());
        log.debug("TIME10:"+ new Date().getTime());
    }
}
