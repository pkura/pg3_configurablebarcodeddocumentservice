package pl.compan.docusafe.parametrization.ilpol;

import java.util.HashMap;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;

/**
 *
 * @author Mariusz Kilja�czyk
 */
public class CrmBusinessTaskLogic  extends AbstractDocumentLogic{

   public static final String KATEGORIA_FIELD_CN = "KATEGORIA";
   public static final String NUMER_KONTRAHENTA_FIELD_CN = "NUMER_KONTRAHENTA";
   public static final String NUMER_PRACOWNIKA_FIELD_CN = "NUMER_PRACOWNIKA";
   public static final String WYNIK_KONTAKTU_CN = "WYNIK_KONTAKTU";
   public static final String OPIS_FIELD_CN = "OPIS";
   public static final String ACTION_COUNT_CN = "ACTION_COUNT";
   public static final String STATUS_OFERTY_CN = "STATUS_OFERTY";
   
   //Kategorie
   public static final Integer KONTAKT_Z_KLIENTEM = 10;
   
   //Wyniki kontaktu
   public static final Integer WK_NIE_ODBIERA = 10;
   public static final Integer WK_BLEDNY_NUMER = 20;
   public static final Integer WK_BRAK_ZAINTERESOWANIA = 30;
   public static final Integer WK_KONTAKT_PRZELOZONY = 40;
   public static final Integer WK_ZAJETE_ODDZWONIC = 50;
   public static final Integer WK_SYGNAL_FAX = 60;
   public static final Integer WK_AUTOMATYCZNA_SEKRETARKA = 70;
   public static final Integer WK_WYSLAC_OFERTE = 80;
   public static final Integer WK_WYSLAC_INFO = 85;
   public static final Integer WK_POWTORZONE = 90;
   public static final Integer WK_NIE_DZWONIC_DYREKTOR = 100;  
   
   //Statusy
   public static final Integer STATUS_W_PRZYGOTOWANIU = 10; 
   public static final Integer STATUS_WYSLANA_KLIENTOWI = 20; 
   public static final Integer STATUS_CZEKAM_NA_ODPOWIEDZ = 30; 
   
   private static CrmBusinessTaskLogic instance;

    public static CrmBusinessTaskLogic getInstance()
    {
        if (instance == null)
            instance = new CrmBusinessTaskLogic();
        return instance;
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
    	Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent("CRM");
        folder = folder.createSubfolderIfNotPresent("Zadanie Handlowe");
        
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        folder = folder.createSubfolderIfNotPresent((String)fm.getValue(KATEGORIA_FIELD_CN));

        document.setFolder(folder);
    }

    @Override
    public boolean processActions(OfficeDocument document, String activity) throws EdmException
    {
    	if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");
        
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        Integer wynikKontaktu = (Integer) fm.getKey(WYNIK_KONTAKTU_CN);
        Integer actionCount = (Integer) fm.getKey(ACTION_COUNT_CN);
        if( actionCount == null)
        	actionCount = 0;
        if(wynikKontaktu != null)
        {        	
	        if ( wynikKontaktu.equals(WK_NIE_ODBIERA) || wynikKontaktu.equals(WK_ZAJETE_ODDZWONIC) )
	        {
	        	actionCount++;
	        	if ( actionCount > 2 )
	        	{
	        		manualFinish(document,activity);
	        		return true;
	        	}
	        	Map<String,Object> tmp = new HashMap<String,Object>();
                tmp.put(ACTION_COUNT_CN,actionCount);
                document.getDocumentKind().setWithHistory(document.getId(),tmp,false);
	        }
	        if (wynikKontaktu.equals(WK_SYGNAL_FAX) || wynikKontaktu.equals(WK_AUTOMATYCZNA_SEKRETARKA) || wynikKontaktu.equals(WK_BLEDNY_NUMER)
	        		|| wynikKontaktu.equals(WK_BRAK_ZAINTERESOWANIA) || wynikKontaktu.equals(WK_POWTORZONE) || wynikKontaktu.equals(WK_NIE_DZWONIC_DYREKTOR))
	        {
	        	manualFinish(document,activity);
	        	return true;
	        }
        }
        return false;
    }
    
    public void documentPermissions(Document document) throws EdmException
    {

    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
        Map<String,Object> values = new HashMap<String,Object>();
       
        values.put(STATUS_OFERTY_CN,STATUS_W_PRZYGOTOWANIU);
        fm.reloadValues(values);

    }

    @Override
    public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
    {
    	String channel = document.getDocumentKind().getChannel(document.getId());
    	if(channel != null)
    	{
	    	 if( ProcessCoordinator.find(DocumentLogicLoader.CRMBUSINESS_TASK_KIND,channel) != null)
	    	        return ProcessCoordinator.find(DocumentLogicLoader.CRMBUSINESS_TASK_KIND,channel).get(0);
	    	 else
	    		 return null;
    	}
    	return null;
    }
    
    private void manualFinish(OfficeDocument document, String activity)
    {
    	try
        {
            // ko�czymy zadanie
            WorkflowFactory.getInstance().manualFinish(activity, true);   

        }
        catch (EdmException e)
        {
            DSApi.context().setRollbackOnly();
        }
    }

}
