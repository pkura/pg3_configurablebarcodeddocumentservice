package pl.compan.docusafe.parametrization.ilpol;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.AbstractDictionaryField;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.mail.DSEmailChannel;
import pl.compan.docusafe.core.mail.MailMessage;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.ilpoldwr.*;
import pl.compan.docusafe.service.fax.IncomingFax;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

/**
 * Klasa ImportUtils.java
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 */
public class ServicesUtils 
{
	private static Logger log = LoggerFactory.getLogger("VINDICATIONIMPORT");
	public static final String ID_WEZWANIA ="ID_WEZWANIA";
	public static final String NR_WEZWANIA ="NR_WEZWANIA";
	public static final String ID_KLIENTA ="ID_KLIENTA";
	public static final String SYMBOL_KLIENTA ="SYMBOL_KLIENTA";
	public static final String NAZWA_KLIENTA ="NAZWA_KLIENTA";
	public static final String ADRES_KLIENTA ="ADRES_KLIENTA";
	public static final String POCZTA ="POCZTA";
	public static final String NIP ="NIP";
	public static final String DATA_WEZWANIA ="DATA_WEZWANIA";
	public static final String NALEZNOSC ="NALEZNOSC";
	public static final String NALEZNOSC_PIERW ="NALEZNOSC_PIERW";
	public static final String OPIEKUN ="OPIEKUN";
	public static final String CZY_HANDLOWIEC_AKTYWNY ="CZY_HANDLOWIEC_AKTYWNY";
	public static final String LASTNAME ="LASTNAME";
	public static final String FIRSTNAME ="FIRSTNAME";
	public static final String UWAGI ="UWAGI";
	public static final String CASE_TASK = "ZADANIE";

    
	public static void setVindicationValues(ResultSet rs,Map<String,Object> values) throws EdmException, SQLException
	{                	
		String nip = rs.getString(NIP);
		nip = nip.replaceAll("\"", "");
		nip = nip.replaceAll("-", "");
		List<Contractor> conList = Contractor.findByNIP(nip);
		if(conList != null && conList.size() > 0)
		{
			values.put(CrmVindicationLogic.NUMER_KONTRAHENTA_FIELD_CN, conList.get(0).getId());
		}	                	
		values.put(CrmVindicationLogic.NUMER_WEZWANIA_FIELD_CN, rs.getString(NR_WEZWANIA));
		
		Date date = null;
		try
		{
			date = DateUtils.parseDateAnyFormat(rs.getString(DATA_WEZWANIA).replaceAll("\"", ""));
		}
		catch (Exception e) 
		{
			new EdmException("B��d parsowania daty "+ rs.getString(DATA_WEZWANIA));
		}
		values.put(CrmVindicationLogic.DATA_WEZWANIA_FIELD_CN, date); 
		values.put(CrmVindicationLogic.NALEZNOSC_PIERWOTNA_FIELD_CN, rs.getString(NALEZNOSC_PIERW));
		values.put(CrmVindicationLogic.NALEZNOSC_FIELD_CN, rs.getString(NALEZNOSC));   
	}
	
	public static void setRemarketingValues(ResultSet rs,Map<String,Object> values,DocumentKind documentKind,Long userId) throws EdmException, SQLException
	{                	
		String nip = rs.getString(RemarketingService.NIP);
		nip = nip.replaceAll("\"", "");
		nip = nip.replaceAll("-", "");
		List<Contractor> conList = Contractor.findByNIP(nip);
		if(conList != null && conList.size() > 0)
		{
			values.put(CrmVindicationLogic.NUMER_KONTRAHENTA_FIELD_CN, conList.get(0).getId());
		}	                	
		values.put(CrmMarketingTaskLogic.GRUPA_FIELD_CN,documentKind.getFieldByCn("GRUPA").getEnumItemByTitle("Remarketing").getId()); 
		values.put(CrmMarketingTaskLogic.KATEGORIA_FIELD_CN,2);
		values.put(CrmMarketingTaskLogic.UZYTKOWNIK_FIELD_CN,userId);
	}
	
	public static int setWwwPetitionValues(NodeList childrenOfChild, Map<String,Object> values, DocumentKind documentKind, List<String> uwagi, int propertiesId)
	{	
		Statement st = null;
		ResultSet rs = null;
		int idD = -1;
		try 
		{
			//DSApi.openAdmin();
			Map<String, String> tempMap = new HashMap<String, String>();
			uwagi.clear();
			for(int j=0; j<childrenOfChild.getLength(); ++j)
			{
				Node childOfChildren = childrenOfChild.item(j);
				if(childOfChildren instanceof Element)
				{
					String t = childOfChildren.getNodeName().trim();
					if(t.equals("id"))
					{
						idD = new Integer(childOfChildren.getTextContent());
						if(idD <= propertiesId)
						{
							return -1;
						}
					}
					else if(t.equals("company_name") || t.equals("nip") || t.equals("phone") || t.equals("email") || t.equals("webpage_address") || t.equals("county") || t.equals("contact_person"))
					{
						tempMap.put(childOfChildren.getNodeName().trim(), childOfChildren.getTextContent().trim());
					}
					else
					{            
            		   NamedNodeMap nnm = childOfChildren.getAttributes();
            		   for(int g=0; g<nnm.getLength(); ++g)
            		   {
            			   uwagi.add(nnm.item(g).getNodeValue() + " " + childOfChildren.getTextContent() + ", ");
            		   }
					}
				}
			}

			
            String tempKeyString = tempMap.get("nip");
            tempKeyString = tempKeyString.replaceAll("\"", "");
            tempKeyString = tempKeyString.replaceAll("-", "");
            tempKeyString = tempKeyString.replaceAll(" ", "");
            List<Contractor> contractList = Contractor.findByNIP(tempKeyString);
            if(contractList.isEmpty() || contractList == null)
            {
                Contractor contractor = new Contractor();
                contractor.setNip(tempKeyString); // NIP
                contractor.setFullName(tempMap.get("company_name"));
                String temp = tempMap.get("company_name");
                if(temp.length()>49)
                {
                	temp = temp.substring(0, 49);
                }
//                contractor.setDsUser(tempMap.get("contact_person"));
                contractor.addPatron(tempMap.get("contact_person"), "LEO");
                contractor.setName(temp);
                contractor.setPhoneNumber(tempMap.get("phone"));
                contractor.setEmail(tempMap.get("email"));
                contractor.setWww(tempMap.get("webpage_address"));
                DSApi.context().begin();
                contractor.create();
    			DSApi.context().commit();
    			values.put(CrmVindicationLogic.NUMER_KONTRAHENTA_FIELD_CN, contractor.getId());
            }
            else
            {
            	//System.out.println("Kontrahent juz istnieje");
            	values.put(CrmVindicationLogic.NUMER_KONTRAHENTA_FIELD_CN, contractList.get(0).getId());
            }
            // przerobic na wartosci z parametrami z xml
   
            
            //values
    		values.put(CrmMarketingTaskLogic.GRUPA_FIELD_CN, 1);//documentKind.getFieldByCn("GRUPA").getEnumItemByTitle("WWW").getId()); 
    	//	values.put(CrmMarketingTaskLogic.KATEGORIA_FIELD_CN,2);
			String countyName = tempMap.get("county");
			DSApi.context().begin();
			st = DSApi.context().createStatement();	
			rs = st.executeQuery("SELECT ID from DSG_COUNTY where name like '" + countyName + "'");
			rs.next();
			DSApi.context().commit();
			//System.out.println(countyName + " ID: " + rs.getString(1));
			//System.out.println(documentKind.getFieldByCn("COUNTY").getEnumItemByTitle(countyName).getId() + "CN wojewodztwa");
    		values.put(CrmMarketingTaskLogic.COUNTY_CN, rs.getString(1)); 
            //uwagi = getRemarksFromXML(tempMap);    		
    		return idD;               
		}
		catch(Exception e)
		{
			log.error(e.getMessage(),e);
		}
		finally
		{
			try 
			{
				if(st != null)
				{
					st.close();
				}
				if(rs != null)
				{
					rs.close();
				}
			} 
			catch (Exception e) 
			{
				log.error(e.getMessage(),e);
			}
		}
		return idD;
	}
	
//	private static String getRemarksFromXML(Map<String,String> tempMap)
//	{
//		String remarks = "";
//		tempMap.remove("id");
//		tempMap.remove("nip");
//		tempMap.remove("company_name");
//		tempMap.remove("phone");
//		tempMap.remove("email");
//		tempMap.remove("webpage_address");
//		tempMap.remove("county");
//		tempMap.remove("contact_person");
//		for(String a : tempMap.values())
//		{
//			remarks += a;
//		}
//		return remarks;
//	}

    public static void createTaskFromEmail(String summary, Map<String,Object> values, DSUser user, String remark, InOfficeDocument doc, DocumentKind documentKind, String objective, boolean toCoordinator, String messageId, DSEmailChannel channel) throws EdmException, IOException, SQLException {
        Preconditions.checkNotNull(documentKind);
        if(doc == null)
            doc = new InOfficeDocument();
        if(values == null)
            values = new HashMap<String, Object>();
        if(user == null)
            user = DSApi.context().getDSUser();

        createTaskBase(summary, values, user.getName(), remark, doc, documentKind, objective, toCoordinator);

        MailMessage mailMessage = MailMessage.findByMessageId(String.valueOf(messageId), channel);
        doc.setSource("Mail");
        doc.setMessageId(mailMessage.getId());
        doc.setFolder(Folder.getRootFolder());

        Long newDocumentId = doc.getId();

        mailMessage.createAttachments(doc);
        documentKind.setOnly(newDocumentId, values, false);

        DSApi.context().session().save(doc);
        DocumentLogic logic =  documentKind.logic();
        if(logic != null){
            documentKind.logic().onStartProcess(doc, null);
        }else{
            throw new EdmException("Nie uruchomiono procesu z powodu braku logiki dla dokumentu typu " + documentKind.getCn());
        }
        TaskSnapshot.updateByDocument(doc);
        String senderEmailAddress = mailMessage.getFrom();

        try {
            InternetAddress addr = new InternetAddress(senderEmailAddress);
            senderEmailAddress = addr.getAddress();
        } catch(AddressException e) {
            LoggerFactory.getLogger("kamil").debug("Dziwny adres: " + senderEmailAddress, e);
            throw new EdmException(e.getLocalizedMessage(), e);
        }
        List<Contractor> ctrList = Contractor.findByEmail(senderEmailAddress);
        Long contractorId = null;
        PreparedStatement ps;
        StringBuilder query = new StringBuilder("UPDATE " + IlpolCokLogic.DOCKIND_TABLE_NAME + " SET MESSAGE_ID = ?, TASK_STATUS = ?");
        if(ctrList.size() > 0){
            Contractor contractor = Contractor.findByEmail(senderEmailAddress).get(0);
            contractorId = contractor.getId();
            query.append(", CONTRACTOR_ID = ? WHERE DOCUMENT_ID = ?");
            ps = DSApi.context().prepareStatement(query);
            ps.setLong(3, contractorId);
            ps.setLong(4, newDocumentId);
        }else{
            query.append(" WHERE DOCUMENT_ID = ?");
            ps = DSApi.context().prepareStatement(query);
            ps.setLong(3, newDocumentId);
        }
        ps.setLong(1, mailMessage.getId());
        ps.setLong(2, 1);
        ps.execute();
        DSApi.context().closeStatement(ps);
    }

	public static void createTask(String summary, Map<String,Object> values, DSUser user, String remark, InOfficeDocument doc, DocumentKind documentKind, String objective) throws EdmException
    {
		createTask(summary, values, user, remark, doc, documentKind,objective, false);
    }
	
	private static void createTaskBase(String summary, Map<String,Object> values, String creatingUserName, String remark, InOfficeDocument doc, DocumentKind documentKind, String objective, boolean toCoordinator) throws EdmException
	{
		log.debug("TIME6 :"+ new Date().getTime());
        doc.setDocumentKind(documentKind);
        doc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
        doc.setDivisionGuid(DSDivision.ROOT_GUID);
        doc.setCurrentAssignmentAccepted(Boolean.FALSE);
        doc.setCreatingUser(creatingUserName);
        doc.setSummary(summary);
        doc.setForceArchivePermissions(false);
        doc.setAssignedDivision(DSDivision.ROOT_GUID);
        doc.setClerk(creatingUserName);
        doc.setSender(new Sender());
        doc.setSource("crm");
        
        List<InOfficeDocumentKind> kinds = InOfficeDocumentKind.list();
        String kindName = documentKind.logic().getInOfficeDocumentKind();
        boolean canChooseKind = (kindName == null);
        if (!canChooseKind)
        {                       
            for (InOfficeDocumentKind inKind : kinds)
            {
                if (kindName.toUpperCase().equals(inKind.getName().toUpperCase()))
                	doc.setKind(InOfficeDocumentKind.find(inKind.getId()));
            }
        }
        if(doc.getKind() == null)
        {
        	doc.setKind(InOfficeDocumentKind.find(1));            	
        }
        Calendar currentDay = Calendar.getInstance();
        currentDay.setTime(GlobalPreferences.getCurrentDay());
        doc.setIncomingDate(currentDay.getTime());
        doc.setDocumentDate(new Date());
        doc.setCurrentAssignmentAccepted(Boolean.FALSE);
        doc.setOriginal(true);
        doc.setStatus(InOfficeDocumentStatus.findByCn("PRZYJETY"));
        log.debug("TIME7 :"+ new Date().getTime());
        doc.create();
        log.debug("TIME8 :"+ new Date().getTime());
        Journal journal = Journal.getMainIncoming();
        Long journalId;
        journalId = journal.getId();
        Integer sequenceId = null;
        sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), new Date(currentDay.getTime().getTime()));
        
        doc.bindToJournal(journalId, sequenceId);
        
        Long newDocumentId = doc.getId();
        doc.setPermissionsOn(false);
        doc.setDocumentKind(documentKind);
        newDocumentId = doc.getId();
        log.debug("TIME8B:"+ new Date().getTime());
        documentKind.set(newDocumentId, values);
        log.debug("TIME8C:"+ new Date().getTime());
        documentKind.logic().archiveActions(doc, DocumentLogic.TYPE_IN_OFFICE);
        log.debug("TIME8D:"+ new Date().getTime());
        documentKind.logic().documentPermissions(doc);
        log.debug("TIME8E:"+ new Date().getTime());
        DSApi.context().session().flush();
        if(StringUtils.isNotEmpty(remark))
        {
	        try
	        {
	        	if (remark.length()<=4000) {
	        		Remark rem = new Remark(remark,DSApi.context().getPrincipalName());
	            	doc.addRemark(rem);
	        	} else {
	        		for(int i=0; i<remark.length(); i=i+4000)
	            	{
	            		String spLine = StringUtils.substring(remark,i ,i+4000);
	            		Remark rem = new Remark(spLine,DSApi.context().getPrincipalName());
	                	doc.addRemark(rem);
	            	}
	        	}
	        }
	        catch (Exception e)
	        {
	        	log.error(e.getMessage(),e);
				log.error("REMARK : "+ remark);
				throw new EdmException(e);
			}
        }
        log.debug("TIME8F:"+ new Date().getTime());

        log.debug("TIME9 :"+ new Date().getTime());
	}
	
	public static void createTask(String summary, Map<String,Object> values, DSUser user, String remark, InOfficeDocument doc, DocumentKind documentKind, String objective, boolean toCoordinator) throws EdmException
    {
		createTaskBase(summary, values, DSApi.context().getPrincipalName(), remark, doc, documentKind, objective, toCoordinator);
        String guid =  (user.getDivisions() != null && user.getDivisions().length > 0 ? user.getDivisions()[0].getGuid() : DSDivision.ROOT_GUID );
		Map<String, Object> map = Maps.newHashMap();
		map.put(ASSIGN_USER_PARAM, user.getName());
		map.put(ASSIGN_DIVISION_GUID_PARAM,guid);
		documentKind.getDockindInfo().getProcessesDeclarations().onStart(doc, map);
//        if(user == null)
//        	WorkflowFactory.createNewProcess(doc, toCoordinator, objective);     
//        else
//        	WorkflowFactory.createNewProcess(doc, toCoordinator, objective, user.getName(),
//        		(user.getDivisions() != null && user.getDivisions().length > 0 ? user.getDivisions()[0].getGuid() : DSDivision.ROOT_GUID ), user.getName());
        log.debug("TIME10:"+ new Date().getTime());
    }
	
	public static void createTaskForPersonOrDiv(String summary,Map<String,Object> values,String user,String guid,String remark,InOfficeDocument doc, DocumentKind documentKind,String objective, boolean toCoordinator) throws EdmException
    {
		createTaskBase(summary, values, DSApi.context().getPrincipalName(), remark, doc, documentKind, objective, toCoordinator);
		
		//r�czne tworzenie nowego procesu
		//dla u�ytkownika i guid'u
        Map<String, Object> map = Maps.newHashMap();
		map.put(ASSIGN_USER_PARAM, user);
		map.put(ASSIGN_DIVISION_GUID_PARAM, guid);
		documentKind.getDockindInfo().getProcessesDeclarations().onStart(doc, map);

        log.debug("TIME10:"+ new Date().getTime());
    }
}
