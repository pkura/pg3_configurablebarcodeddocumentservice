package pl.compan.docusafe.parametrization.ilpol;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class ImpulsNormalLogic extends AbstractDocumentLogic
{
    private static ImpulsNormalLogic instance;
    private static Logger log = LoggerFactory.getLogger(ImpulsNormalLogic.class);

	public static ImpulsNormalLogic getInstance()
    {
        if (instance == null)
            instance = new ImpulsNormalLogic();
        return instance;
    }

    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
    {

    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
    	if(document.getFolder() == null || document.getFolder().isRoot())
    	{
	        if (DocumentLogic.TYPE_ARCHIVE == type)
	            document.setFolder(Folder.getRootFolder());
	        else
	            document.setFolder(Folder.findSystemFolder(Folder.SN_OFFICE));
    	}
	    document.setDoctypeOnly(null);
    }
    
    @Override
    public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
    {
    	if(document instanceof OutOfficeDocument)
    	{
    		
    		String kind = "ILPL";
    		String divsILII = document.getDocumentKind().getDockindInfo().getProperties().get("rootIIPL");
    		String divsILPL = document.getDocumentKind().getDockindInfo().getProperties().get("rootILPL");
    		String div = document.getDivisionGuid();
    		boolean znalazl = false;
    		while(!znalazl)
    		{
    		   if(div.equals(divsILII))
    		   {
    			   kind = "IIPL";
    			   break;
    		   }
    		   else if (div.equals(divsILPL))
    		   {
    			   kind = "ILPL";
    			   break;
    			   
    		   }
    		   DSDivision parent =  DSDivision.find(div).getParent();
    		   if(parent == null)
    		   {
    			   throw new EdmException("Nie znaleziono dzia�u g��wnego");
    		   }
    		   div =parent.getGuid();
    		}
    		List<ProcessCoordinator> list =  ProcessCoordinator.find(DocumentKind.NORMAL_KIND, kind);
    		if(list != null && list.size() > 0)
    			return list.get(0);
    	}
    	return null;
    }

	public void documentPermissions(Document document) throws EdmException 
	{	
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ,"DOCUMENT_READ",ObjectPermission.GROUP,"Dokumenty - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"DOCUMENT_READ_ATT_READ",ObjectPermission.GROUP,"Dokumenty zalacznik - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY,"DOCUMENT_READ_MODIFY",ObjectPermission.GROUP,"Dokumenty - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"DOCUMENT_READ_ATT_MODIFY",ObjectPermission.GROUP,"Dokumenty zalacznik - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.DELETE,"DOCUMENT_READ_DELETE",ObjectPermission.GROUP,"Dokumenty - usuwanie"));
   	 	
   	 	for (PermissionBean perm : perms)    		
   	 	{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
   	 	}
   	 	//this.setUpPermission(document, perms);	
	}
    
    /**
     * Zwraca polityk� bezpiecze�stwa dla dokument�w tego rodzaju.
     */
    public int getPermissionPolicyMode()
    {
        return PermissionManager.NORMAL_POLICY;
    }
    
    @Override
    public boolean isPersonalRightsAllowed()
    {
    	return true;
    }
    @Override
    public void onStartProcess(OfficeDocument document,ActionEvent event)
    {
    	try
    	{
    		if(DocumentType.OUTGOING.equals(document.getType()))
    			return ;
    		document.setDivisionGuid(document.getCurrentAssignmentGuid());
	    	log.info("onStartProcess document {} division {}",document.getId(),document.getCurrentAssignmentGuid());
	    	//bindToJournal(document, event);
	    	String[] divs = document.getDocumentKind().getDockindInfo().getProperties().get("divisionOnProcess").split(";");
	    	HashSet<String> divsSet = new HashSet<String>(Arrays.asList(divs));
	    	if(!divsSet.contains(document.getCurrentAssignmentGuid()))
	    	{
	    		DSApi.context().session().flush();
	    		DSApi.context().commit();
	    		DSApi.context().begin();
	    		log.info("Porces stop");
	    		WorkflowFactory.getInstance().manualFinish(document.getId(), false);
	    		event.addActionMessage("Zako�czono proces");
	    	}
	    	else
	    	{
	    		log.info("Porces go");
	    	}
	    	
    	}
    	catch (Exception e) {
			log.error(e.getMessage(),e);
			event.addActionError("Wyst�pi� b��d "+ e.getMessage());
		}
    }
    

}
