package pl.compan.docusafe.parametrization.ilpol;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.parametrization.ilpol.RemarketingService.Import;
import pl.compan.docusafe.parametrization.ilpol.RemarketingService.LastDateProperty;
import pl.compan.docusafe.parametrization.ilpol.RemarketingService.MailTimesProperty;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.imports.ImpulsImportUtils;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

public class WwwPetitionService  extends ServiceDriver implements Service
{
	public static final Logger log = LoggerFactory.getLogger(WwwPetitionService.class);
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(RemarketingService.class.getPackage().getName(),null);

	private Property[] properties;
	private Timer timer;
	private Integer periodWww = 10;
	private String xmlUrl = "http://formularze-crm.60225.v.tld.pl/uploads/dataExport/data_exported_forms_online.xml";
	private String loginUrl = "http://formularze-crm.60225.v.tld.pl/login";
	private String username = "admin";
	private String password = "admin";
	private Integer indexOfImportedDocuments = 0;
	
	
	class ImportTimesProperty extends Property {
        public ImportTimesProperty(){
            super(SIMPLE, PERSISTENT, WwwPetitionService.this, "periodWww", "Co ile minut", Integer.class);
        }
        protected Object getValueSpi(){
            return periodWww;
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (WwwPetitionService.this){
            	if(object != null)
            		periodWww = (Integer) object;
            	else
            		periodWww = 10;
            }
        }
    }
	
	class URLToXmlFileProperty extends Property {
        public URLToXmlFileProperty(){
            super(SIMPLE, PERSISTENT, WwwPetitionService.this, "xmlUrl", "�cie�ka do pliku xml", String.class);
        }
        protected Object getValueSpi(){
            return xmlUrl;
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (WwwPetitionService.this){
            	if(object != null)
            		xmlUrl =  (String) object;
            	else
            		xmlUrl = "http://formularze-crm.60225.v.tld.pl/uploads/dataExport/data_exported_forms_online.xml";
            }
        }
    }
	
	class URLToLoginProperty extends Property {
        public URLToLoginProperty(){
            super(SIMPLE, PERSISTENT, WwwPetitionService.this, "loginUrl", "Adres strony logowania", String.class);
        }
        protected Object getValueSpi(){
            return loginUrl;
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (WwwPetitionService.this){
            	if(object != null)
            		loginUrl = (String) object;
            	else
            		loginUrl = "http://formularze-crm.60225.v.tld.pl/login";
            }
        }
    }
	
	class UsernameProperty extends Property {
        public UsernameProperty(){
            super(SIMPLE, PERSISTENT, WwwPetitionService.this, "username", "Nazwa u�ytkownika", String.class);
        }
        protected Object getValueSpi(){
            return username;
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (WwwPetitionService.this){
            	if(object != null)
            		username = object.toString();
            	else
            		username = "admin";
            }
        }
    }
	
	class PasswordProperty extends Property {
        public PasswordProperty(){
            super(SIMPLE, PERSISTENT, WwwPetitionService.this, "password", "Has�o", String.class);
        }
        protected Object getValueSpi(){
            return password;
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (WwwPetitionService.this){
            	if(object != null)
            		password = (String) object;
            	else
            		password = "admin";
            }
        }
    }
	
	class IndexOfImportedDocumentsProperty extends Property {
        public IndexOfImportedDocumentsProperty(){
            super(SIMPLE, PERSISTENT, WwwPetitionService.this, "period", "ID ostatnio zaimportowanego zadania", Integer.class);
        }
        protected Object getValueSpi(){
            return indexOfImportedDocuments;
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (WwwPetitionService.this){
            	if(object != null)
            		indexOfImportedDocuments = (Integer) object;
            	else
            		indexOfImportedDocuments = 0;
            }
        }
    }
	
	public WwwPetitionService()
	{
		properties = new Property[] { new ImportTimesProperty(),  new IndexOfImportedDocumentsProperty(), new URLToXmlFileProperty(), new URLToLoginProperty(), new UsernameProperty(), new PasswordProperty() };
	}
	
	@Override
	protected void start() throws ServiceException {
		log.info("Start uslugi WwwPetitionService");
		timer = new Timer(true);
		timer.schedule(new Import(), 0 ,periodWww*DateUtils.MINUTE);
		console(Console.INFO, sm.getString("System WwwPetitionService wystartowal"));	
	}

	@Override
	protected void stop() throws ServiceException {
		log.info("Stop uslugi WwwPetitionService");
		console(Console.INFO, sm.getString("System WwwPetitionService zako�czy� dzia�anie"));	
		try
		{
			DSApi.close();
		}
    	catch (Exception e)
    	{
			log.error(e.getMessage(),e);
    	}
	}

	@Override
	protected boolean canStop() 
	{
		return false;
	}
	
	
	
	class Import extends TimerTask
	{
		@Override
		public void run() 
		{
			try
			{
			    if (xmlUrl.startsWith("http://"))
			    {
			    	// Send a GET request to the servlet
			    	try
			    	{
			    		// Construct data
			    		URL urlLogin = new URL(loginUrl);
			    		HttpURLConnection conn = (HttpURLConnection) urlLogin.openConnection();
			    	    conn.setRequestMethod("POST");
			    	    conn.setDoOutput(true);
			    	    conn.setDoInput(true);
			    	    conn.setUseCaches(false);
			    	    conn.setAllowUserInteraction(false);
			    	    conn.setRequestProperty("Content-type", "text/xml; charset=" + "UTF-8");

			    	    //OutputStream out = conn.getOutputStream();
			    
			    		//przekazuje do parsera
			    		//XmlPars.parseAndAdd(conn.getInputStream());
			    
			    		StringBuffer data = new StringBuffer();
					    
			    	    conn.disconnect();
			    	    
				        
			    	    URL urlGet = new URL(xmlUrl);
			    	    //URL urlGet = new URL("http://formularze-crm.60225.v.tld.pl/crm_online/new");
			    		HttpURLConnection connGet = (HttpURLConnection) urlGet.openConnection();
			    		connGet.setRequestMethod("GET");
			    	    connGet.setRequestProperty("Connection","keep-alive");
			    		connGet.setRequestProperty("Host", "http://formularze-crm.60225.v.tld.pl");
			    	    //przekazuje do parsera
			    		//XmlPars.parseAndAdd(conn.getInputStream());
			    	    
			    		
			    		// Get the response
			    	    BufferedReader rd = new BufferedReader(new InputStreamReader(connGet.getInputStream()));
			    	    StringBuffer sb = new StringBuffer();
			    	    String line;
			    	    
			    	    
			    		//StringBuffer data = new StringBuffer();
			    	    			    					    				
			    		//URL url = new URL(xmlUrl);
			    		// Get the response
			    		//BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			    		//StringBuffer sb = new StringBuffer();
			    		//String line;

			    		 FileWriter fstream = new FileWriter("upload.xml");
			    		 BufferedWriter out = new BufferedWriter(fstream);
			    		 while ((line = rd.readLine()) != null)
			    		 {
			    		    	out.write(line + "\n");
			    		    	//sb.append(line);
			    		 }
			    		 
			    		 rd.close();
			    		 out.close();
			    		 //System.out.println(sb.toString());
			    		
			    		}
			    		catch(Exception e)
			    		{
			    			log.error(e.getMessage() + e);
			    		}
			    		
			///////////////////////////    		
//			    		URL url = new URL(xmlUrl);
//			    		// Get the response
//			    		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//			    		//StringBuffer sb = new StringBuffer();
//			    		String line;
//			    
//			    		FileWriter fstream = new FileWriter("upload.xml");
//			    		BufferedWriter out = new BufferedWriter(fstream);
//			    
//			    
//			    
//			    		while ((line = rd.readLine()) != null)
//			    		{
//			    			out.write(line + "\n");
//			    			System.out.println(line);
//			    			//sb.append(line);
//			    		}
//			    		console(Console.INFO, sm.getString("System WwwPetitionService pobra� plik xml"));	
//			    		rd.close();
//			    		out.close();
//			    	}
//			    	catch (Exception e)
//			    	{
//						log.error(e.getMessage(),e);
//			    	}
			    		//////////////////////////
			    }
			    else
			    {
			    	log.error("�cie�ka musi zaczyna� si� od http:// ");
			    	throw new Exception("�cie�ka musi zaczyna� si� od http://");
			    }
				// koniec pobierania pliku xml
			    
    			DSApi.openAdmin();
				DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.CRM_MARKETING_TASK_KIND);
		        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		        DocumentBuilder builder = factory.newDocumentBuilder();
		        File f = File.createTempFile("upload", ".xml");			        
		        Document document = builder.parse(f);
		        Element root = document.getDocumentElement();
		        NodeList children = root.getChildNodes();
        		console(Console.INFO, sm.getString("System WwwPetitionService pobra� plik xml"));

		        for(int i=0; i<children.getLength(); ++i)
		        {
		            Node child = children.item(i);
		            if(child instanceof Element)
		            {			
						Map<String,Object> values = new TreeMap<String, Object>();
						List<String> uwagi = new LinkedList<String>();
						String remarks = "";
		            	int tempID = ServicesUtils.setWwwPetitionValues(child.getChildNodes(), values, documentKind, uwagi, indexOfImportedDocuments);
		                for(String t : uwagi)
		                {
		                	remarks += t;
		                }
		            	if(tempID > 0)
		            	{
		            		try
		            		{
		            			//DSApi.context().begin();
		            			InOfficeDocument doc = new InOfficeDocument();
		            			//System.out.println("uwagi :" + uwagi);
		            			ServicesUtils.createTask("WWW", values, null ,remarks ,doc,documentKind,"WWW", true);
		            			getProperties()[1].setValue(tempID);
		            			DSApi.context().commit();
			            		console(Console.INFO, "Dokument o ID: " + doc.getId() + " zosta� dodany"); 
		            			//System.out.println("Doda� dokument o ID "+doc.getId());
		            		}
		            		catch (Exception e) 
		            		{
		            			log.error("B�D importu " + e.getMessage(),e);
		            			console(Console.ERROR, "B�AD importu " + e.getMessage());
//								DSApi.context().rollback();
//								DSApi.context().session().flush();
//								DSApi.context().session().clear();
//								DSApi.close();
//								DSApi.openAdmin(); 
		            		}
		            		finally
		            		{
								//DSApi.context().rollback();
								/**Nie wiem co sie pieprzy */
								DSApi.context().session().flush();
								DSApi.context().session().clear();
								DSApi.close();
								DSApi.openAdmin(); 
		            		}
		            	}
//		            	else
//		            	{
//		            		System.out.println("Pominieto dodany ju� dokument ID: " + tempID );
//		            	}
		            }
		        }

			}
			catch (Exception e) 
			{
				log.error(e.getMessage(),e);
				console(Console.INFO, sm.getString(e.toString()));	
				//log.error("B��D importu Remarketing nr= "+idUmowy+""+e.getMessage(),e);
				//console(Console.ERROR, "B��D importu Remarketing nr= "+idUmowy+""+e.getMessage());
				//DSApi.context().rollback();
				/**Nie wiem co sie pieprzy */
				//DSApi.context().session().flush();
				//DSApi.context().session().clear();
				//DSApi.close();
				//DSApi.openAdmin(); 
			}
			finally
			{
				try
				{
					DSApi.close();
				}
				catch(Exception e) 
				{
					log.error(e.getMessage(),e);
				}
			}

		}
	}
	
	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties) 
	{
		this.properties = properties;
	}
}
