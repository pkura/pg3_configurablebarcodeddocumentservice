package pl.compan.docusafe.parametrization.ilpol;

import java.io.*;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.mail.DSEmailChannel;
import pl.compan.docusafe.core.mail.MailMessage;
import pl.compan.docusafe.core.mail.MessageAttachment;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.ilpoldwr.IlpolCokLogic;
import pl.compan.docusafe.util.LoggerFactory;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.pop3.POP3Folder;

public class TaskFromEmail {
	
	public void createTask(DSEmailChannel emailChannel, Message message, String messageUid) throws UserNotFoundException, EdmException, MessagingException, IOException, SQLException{
		MailMessage mailMessage = MailMessage.findByMessageId(messageUid, emailChannel);
		Long messageId = mailMessage.getId();
		InOfficeDocument document = new InOfficeDocument();
//		document.setInternal(true);
		document.setCreatingUser(DSApi.context().getDSUser().getName());
		document.setCurrentAssignmentAccepted(Boolean.FALSE);
		DocumentKind documentKind = DocumentKind.findByCn(IlpolCokLogic.DOCKIND_NAME);
		document.setAuthor(DSApi.context().getDSUser().getName());
		document.setSummary(IlpolCokLogic.DOCKIND_NAME);
		document.setAssignedDivision(DSDivision.ROOT_GUID);
		document.setDocumentKind(documentKind);
		document.setForceArchivePermissions(false);
		document.setFolder(Folder.getRootFolder());
		document.setMessageId(messageId);
		document.create();
		
		Long newDocumentId = document.getId();
		//mapa jest wymagana aby zostal dodany wpis do tabeli dockinda o id nowo utworzonego dokumentu
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("DELIVERY_TYPE", 700);
		documentKind.setOnly(newDocumentId, values, false);
		
		createAttachments(document, mailMessage);
//		documentKind.logic().archiveActions(document, DocumentLogic.TYPE_IN_OFFICE);
//		documentKind.logic().documentPermissions(document);
//		documentKind.logic().archiveActions(document, 0);
//		documentKind.logic().documentPermissions(document);
		
		DSApi.context().session().save(document);
		documentKind.logic().onStartProcess(document, null);
		TaskSnapshot.updateByDocument(document);
		
		String senderEmailAddress = getSender(message).trim();
		
		try {
			InternetAddress addr = new InternetAddress(senderEmailAddress);
			senderEmailAddress = addr.getAddress();
		} catch(AddressException e) {
			LoggerFactory.getLogger("kamil").debug("Dziwny adres: " + senderEmailAddress, e);
			throw new EdmException(e.getLocalizedMessage(), e);
		}
		List<Contractor> ctrList = Contractor.findByEmail(senderEmailAddress);
		Long contractorId = null;
		PreparedStatement ps;
		if(ctrList.size() > 0){
			Contractor contractor = Contractor.findByEmail(senderEmailAddress).get(0);
			contractorId = contractor.getId(); 
			ps = DSApi.context().prepareStatement("UPDATE " + IlpolCokLogic.DOCKIND_TABLE_NAME + " SET CONTRACTOR_ID = ?, MESSAGE_ID = ?, DELIVERY_TYPE = ?, TASK_STATUS = ? WHERE DOCUMENT_ID = ? ");
	        ps.setLong(1, contractorId);
	        ps.setLong(2, messageId);
	        ps.setLong(3, 700);
	        ps.setLong(4, 1);
	        ps.setLong(5, newDocumentId);
		}else{
			ps = DSApi.context().prepareStatement("UPDATE " + IlpolCokLogic.DOCKIND_TABLE_NAME + " SET MESSAGE_ID = ?, DELIVERY_TYPE = ?, TASK_STATUS = ? WHERE DOCUMENT_ID = ?");
	        ps.setLong(1, messageId);
	        ps.setLong(2, 700);
	        ps.setLong(3, 1);
	        ps.setLong(4, newDocumentId);
		}
		ps.execute();
        DSApi.context().closeStatement(ps);
	}
	
	private void createAttachments(Document doc, MailMessage mailMessage) throws AccessDeniedException, EdmException, IOException{
		for (MessageAttachment messageAttachment : mailMessage.getAttachments()) 
        {
        	Attachment attachment = new Attachment(messageAttachment.getFilename());
        	attachment.setCn(MailMessage.MAIL_ATTACHMENT_CN);
            attachment.setWparam(mailMessage.getId());
			doc.createAttachment(attachment);
			attachment.createRevision(messageAttachment.getBinaryStream(), messageAttachment.getSize(), messageAttachment.getFilename());
			System.out.println("createAttachments: " + messageAttachment.getFilename());
		}
        
        Attachment attachment = new Attachment("Mail");
    	attachment.setCn(MailMessage.MAIL_ATTACHMENT_CN);
        attachment.setWparam(mailMessage.getId());
		doc.createAttachment(attachment);
        int streamSize = getStreamSize(mailMessage.getBinaryStream());
        attachment.createRevision(mailMessage.getBinaryStream(), streamSize, mailMessage.getSubject() + ".eml");
		mailMessage.setProcessed(Boolean.TRUE);
        //	wiazemy dokument z emailem (message_id)
        if(doc instanceof InOfficeDocument){
        	((InOfficeDocument)doc).setMessageId(mailMessage.getId());
        }
	}

    private int getStreamSize(InputStream inputStream) throws IOException {
        InputStreamReader reader = new InputStreamReader(inputStream);
        char[] content = new char[2048];
        int size = 0;
        while(reader.read(content) != -1){
            size += content.length;
        }
        reader.close();
        return size;
	}
	
	private String getSubject(Message message) throws MessagingException{
		return message.getSubject();
	}
	
	private String getSender(Message message) throws MessagingException{
		String from = "";
		Address address[] = message.getFrom();
        for (int i=0; i < address.length; i++)
        {
            from += (i > 0 ? ", " : "") + address[i].toString();
        }
		return from;
	}
	
	private Date getSentDate(Message message) throws MessagingException{
		return message.getSentDate();
	}
	
	private String getContent(Message message) throws MessagingException, IOException, EdmException{
		// utworzenie pliku tymczasowego dla contentu wiadomo��i
        File file = File.createTempFile("mail",".tmp");
        OutputStream output = new  FileOutputStream(file);
        message.writeTo(output);
        output.close();
        // update tre�ci wiadomo�ci
        InputStream input = new FileInputStream(file);     
		Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);
        MimeMessage mimeMessage = new MimeMessage(session, input);
        input.close();
        file.delete();
		return prepareContent(mimeMessage);
	}
	
	private String prepareContent(Part message) throws MessagingException, IOException{
		List<Map<String, Object>> attachments = new ArrayList<Map<String,Object>>();
		String disposition = message.getDisposition();  
		String emailContent = "";
		String nestedContent = "";
		if (message.getContent() instanceof Multipart)
        {
            Multipart mp = (Multipart) message.getContent();
            for (int partOrdinal = 0, m = mp.getCount(); partOrdinal < m; partOrdinal++) 
            {
                Part emailPart = mp.getBodyPart(partOrdinal);
                if(emailPart.isMimeType("message/rfc822")){
                 	//	zalacznik jest zagniezdzona wiadomoscia eml
                	System.out.println(MimeUtility.decodeText(emailPart.getFileName()));
                	nestedContent = 
                	"\n\n" + 
                	"- - - - - - - - - - - NESTED E-MAIL MESSAGE  - - - - - - - - - - - " +
                	"TITLE: " + MimeUtility.decodeText(emailPart.getFileName()) + "\n" + 
                	"CONTENT: " + "\n" +
                	prepareContent(emailPart);
                }
                else
                {
                	if ((disposition != null) && (disposition.equals(Part.ATTACHMENT)))
    	            {
                		 /*
	                     Map<String,Object> map = new HashMap<String,Object>();
	                     map.put("id", partOrdinal);
	                     map.put("filename", MimeUtility.decodeText(fileName));
	                     map.put("filesize", emailPart.getSize());
	                     attachments.add(map);
	                     */
                     }
                }
                 
                if (disposition == null && partOrdinal == 0 && !(emailPart.getContent() instanceof MimeMultipart) && emailPart.getContent() instanceof String)
                {
                	 emailContent = (String) emailPart.getContent();
                }
                else if(disposition != null && disposition.equals(Part.INLINE) && emailPart.getContent() instanceof String)
                {	//	
                 	//	an inline content-disposition, which means that it should be automatically displayed when the message is displayed, 
                 	//	or an attachment content-disposition, in which case it is not displayed automatically and requires some form of action from the user to open it.
                	 emailContent = (String) emailPart.getContent();
                }
            }
        }
        else
        {
            if (message.getContent() instanceof String)
            	emailContent = (String) message.getContent();
            else if (message.getContent() instanceof MimeMessage)
            {
            	 if(((MimeMessage)message.getContent()).getContent() instanceof String)
            	 {
            		 emailContent += (String)((MimeMessage)message.getContent()).getContent();
            	 }
            }
        }
		return emailContent + nestedContent;
	}
	
}
