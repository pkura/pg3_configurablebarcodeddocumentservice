package pl.compan.docusafe.parametrization.ilpol;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.crm.Contact;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.crm.Role;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


/**
 *
 * @author Mariusz Kilja�czyk
 */
public class CrmMarketingTaskLogic  extends AbstractDocumentLogic
{
	private static final Logger log = LoggerFactory.getLogger(CrmMarketingTaskLogic.class);

	public static final int UPDATE_DOCUMENT = 13;
	public static final String LABEL_NAME_ZA_5_DNI = "Za pi�� dni";
	public static final String LABEL_NAME_ZA_4_DNI = "Za cztery dni";
	public static final String LABEL_NAME_ZA_3_DNI = "Za trzy dni";
	public static final String LABEL_NAME_ZA_2_DNI = "Za dwa dni";
	public static final String LABEL_NAME_ZA_1_DNI = "Za jeden dzie�";
	public static final String LABEL_NAME_DISIAJ = "Dzisiaj";
	public static final String LABEL_NAME_PRZETERMINOWANE = "Przeterminowane";
	public static final String LABEL_NAME_ODLEGLE = "Odleg�y termin";
	public static String[] LABEL_NAMES_TAB;
	public static final String STATUS_FIELD_CN = "STATUS";
	public static final String KATEGORIA_FIELD_CN = "KATEGORIA";
	public static final String GRUPA_FIELD_CN = "GRUPA";
	public static final String NUMER_KONTRAHENTA_FIELD_CN = "NUMER_KONTRAHENTA";
	public static final String UZYTKOWNIK_FIELD_CN = "UZYTKOWNIK";
	public static final String KONTAKT_FIELD_CN ="KONTAKT";
	public static final String COUNTY_CN ="COUNTY";
	private static CrmMarketingTaskLogic instance;
	
	static
	{
		LABEL_NAMES_TAB = new String[8];
		LABEL_NAMES_TAB[0] = LABEL_NAME_ZA_5_DNI;
		LABEL_NAMES_TAB[1] = LABEL_NAME_ZA_4_DNI;
		LABEL_NAMES_TAB[2] = LABEL_NAME_ZA_3_DNI;
		LABEL_NAMES_TAB[3] = LABEL_NAME_ZA_2_DNI;
		LABEL_NAMES_TAB[4] = LABEL_NAME_ZA_1_DNI;
		LABEL_NAMES_TAB[5] = LABEL_NAME_DISIAJ;
		LABEL_NAMES_TAB[6] = LABEL_NAME_PRZETERMINOWANE;
		LABEL_NAMES_TAB[7] = LABEL_NAME_ODLEGLE;
	}

    public static CrmMarketingTaskLogic getInstance()
    {
        if (instance == null)
            instance = new CrmMarketingTaskLogic();
        return instance;
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
    	try
    	{
    		if(document instanceof InOfficeDocument) {
    		
    			if(!((InOfficeDocument)document).getKind().getName().equals(getInOfficeDocumentKind()))
    				((InOfficeDocument)document).setKind(InOfficeDocumentKind.findByPartialName(getInOfficeDocumentKind()).get(0));
    		}
    	}
    	catch (Exception e) {
			log.error("",e);
		}
    	
    	FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
    	String opis = "";
    	opis += fm.getValue(STATUS_FIELD_CN);
    	document.setDescription(opis);
    	document.setSource("crm");
    	
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent("CRM").createSubfolderIfNotPresent("Zadania Marketingowe");
        
        Contractor con = (Contractor)fm.getValue(NUMER_KONTRAHENTA_FIELD_CN);
        String nazwa = "";
        if(con != null && con.getName() != null)
        {
        	nazwa = con.getName().trim();
	        nazwa = nazwa.toLowerCase();	        
	        folder = folder.createSubfolderIfNotPresent(FolderInserter.to3Letters(nazwa));
	        folder = folder.createSubfolderIfNotPresent(((Contractor)fm.getValue(NUMER_KONTRAHENTA_FIELD_CN)).getName());
	        folder.setAnchor(true);
	        
	        if(con.getRole() != null && ( 
	        								( fm.getKey(KATEGORIA_FIELD_CN) != null && !fm.getKey(KATEGORIA_FIELD_CN).equals(1) && !fm.getKey(KATEGORIA_FIELD_CN).equals(2) && !fm.getKey(KATEGORIA_FIELD_CN).equals(61) 
	        								&& !fm.getKey(KATEGORIA_FIELD_CN).equals(82)) || 
	        								fm.getKey(KATEGORIA_FIELD_CN) == null
	        							)
	        )
	        {
	        	for (Role role : con.getRole()) 
	        	{
					if("LEO_2".equals(role.getCn()) || "LEO_3".equals(role.getCn()))
						throw new EdmException("Dla kontrahenta Dealer/Dostawca nale�y wybra� jedn� z czterech kategorii (HVPL, VBL, EBL, MEDICAL-EMPL)");
				}
	        }
        }
        else
        {
        	folder = folder.createSubfolderIfNotPresent("Brak kontrahenta");
        }
        document.setFolder(folder);
        
        if(type == DocumentLogic.TYPE_SAVE_ALL)
        {
        	updateDocument(document);
        }
       
    }
    
    private void updateDocument(Document document) throws EdmException
    {
    	FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
    	List<Contact> l = new ArrayList<Contact>();

         if(fm.getValue(CrmMarketingTaskLogic.KONTAKT_FIELD_CN) != null && !(fm.getValue(CrmMarketingTaskLogic.KONTAKT_FIELD_CN) instanceof Contact))
         {
         	l.addAll((List<Contact>) fm.getValue(CrmMarketingTaskLogic.KONTAKT_FIELD_CN));
         }
        
     	if(l != null && l.size() > 0 )
     	{
     		
     		Contact contact = l.get(l.size()-1);
 			
     		if(contact.getStatus().getId() == 115)
 			{
 				LabelsManager.removeLabelsByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAMES_TAB);
     			log.info("Zakonczono prace z dokumentem o ID: " + document.getId());
 			}
     		else if(contact.getStatus().getId() == 20)
 			{
 				int noAnswerCount = 0;
 				for(Contact c : l)
 				{
 					if(c.getStatus().getId() == 20)
 					{
 						++noAnswerCount;
 					}
 					else
 					{
 						noAnswerCount=0;
 					}
 				}
 				log.info("Ilosc kontaktow bez odbioru: " + noAnswerCount);
 				if(noAnswerCount < 5)
 				{
     				LabelsManager.removeLabelsByName(document.getId(),CrmMarketingTaskLogic.LABEL_NAMES_TAB);    				
 					LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_1_DNI, DSApi.context().getPrincipalName(),true);
 				}
 			}			
     		else
 	    	{
 	    		
 	    		Date d = contact.getDataNastepnegoKontaktu();
 	    		if( d != null)
 	    		{
 	    			Integer i = pl.compan.docusafe.util.DateUtils.substract(d, new Date());
 	    			log.info("XXXXXXXXXXXILOSC dni do kontaktu: " + i);
 	    			if(i > 5)
 	    			{
 	    				LabelsManager.removeLabelsByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAMES_TAB);
 	    				LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ODLEGLE, DSApi.context().getPrincipalName(),true);
 	    				Date doDate = null;//org.apache.commons.lang.time.DateUtils.addDays(new Date(), i-5);
 	    				doDate = null;//org.apache.commons.lang.time.DateUtils.setHours(doDate, 1);
 	    				EventFactory.registerEvent("forOneDay", "crmMarketingTaskSetLabelEventHandler"
 	    						,""+i+"|"+LabelsManager.findLabelByName(CrmMarketingTaskLogic.LABEL_NAME_ZA_5_DNI).getId()+"|"+document.getId(),null, doDate);
 	    			}
 	    			else
 	    			{
 	    				LabelsManager.removeLabelsByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAMES_TAB);    				
 		    			switch (i)
 		    			{
 							case 0:
 								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_DISIAJ, DSApi.context().getPrincipalName(),true);
 								break;
 							case 1:
 								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_1_DNI, DSApi.context().getPrincipalName(),true);
 								break;
 							case 2:
 								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_2_DNI, DSApi.context().getPrincipalName(),true);
 								break;
 							case 3:
 								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_3_DNI, DSApi.context().getPrincipalName(),true);
 								break;
 							case 4:
 								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_4_DNI, DSApi.context().getPrincipalName(),true);
 								break;
 							case 5:
 								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_ZA_5_DNI, DSApi.context().getPrincipalName(),true);
 								break;		
 							default:
 								LabelsManager.addLabelByName(document.getId(), CrmMarketingTaskLogic.LABEL_NAME_PRZETERMINOWANE, DSApi.context().getPrincipalName(),true);
 								break;
 						}
 	    			}
 	    		}
 	    	}
     	}
		TaskSnapshot.updateByDocument(document);
		
    }

    public void documentPermissions(Document document) throws EdmException
    {
    	throw new EdmException("STARA KLASA B�AD");
//    	FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
//		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
//		
//		perms.add(new PermissionBean(ObjectPermission.READ, "DL_READ", ObjectPermission.GROUP));
//		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_READ", ObjectPermission.GROUP));
//		
//		perms.add(new PermissionBean(ObjectPermission.READ,"DL_ADMIN", ObjectPermission.GROUP));	
//		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DL_ADMIN", ObjectPermission.GROUP));
//	    perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_ADMIN", ObjectPermission.GROUP));	    
//	    perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DL_ADMIN", ObjectPermission.GROUP));
//
//		perms.add(new PermissionBean(ObjectPermission.READ,"DV_ALL", ObjectPermission.GROUP));	
//		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DV_ALL", ObjectPermission.GROUP));
//	    perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DV_ALL", ObjectPermission.GROUP));	    
//	    perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DV_ALL", ObjectPermission.GROUP));
//
//		if(fm.getValue(NUMER_KONTRAHENTA_FIELD_CN) != null)
//		{
//			if(((Contractor)fm.getValue(NUMER_KONTRAHENTA_FIELD_CN)).getRegion() != null)
//			{
//				String region = ((Contractor)fm.getValue(NUMER_KONTRAHENTA_FIELD_CN)).getRegion().getCn();
//				perms.add(new PermissionBean(ObjectPermission.READ, "DV_" +	region, ObjectPermission.GROUP));
//	            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DV_" +	region, ObjectPermission.GROUP));
//			}
//			String username = ((Contractor)fm.getValue(NUMER_KONTRAHENTA_FIELD_CN)).getDsUser();
//			if( username != null && username.length() > 0)
//			{
//				perms.add(new PermissionBean(ObjectPermission.READ, username, ObjectPermission.USER));
//	            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, username , ObjectPermission.USER));
//			}
//        }
//		if(fm.getValue(UZYTKOWNIK_FIELD_CN) != null)
//		{
//			String name = fm.getEnumItemCn(UZYTKOWNIK_FIELD_CN);
//			perms.add(new PermissionBean(ObjectPermission.READ, (String) name , ObjectPermission.USER));
//            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, name , ObjectPermission.USER));
//		}
//		perms.add(new PermissionBean(ObjectPermission.READ, document.getAuthor() , ObjectPermission.USER));
//        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getAuthor() , ObjectPermission.USER));
//		this.setUpPermission(document, perms);
    }

    public TaskListParams getTaskListParams(DocumentKind kind, long documentId) throws EdmException
	{
		TaskListParams ret = new TaskListParams();
		FieldsManager fm = kind.getFieldsManager(documentId);
		List<Contact> l = (List<Contact>) fm.getValue(KONTAKT_FIELD_CN);
    	if(l != null && l.size() > 0)
    	{
    		Contact contact = l.get(l.size()-1);
    		Date d = contact.getDataNastepnegoKontaktu();
    		if( d != null )
    		{
    			ret.setDocumentDate(d);
    		}
    		if(contact.getRodzajNastepnegoKontaktu() != null)
    			ret.setAttribute(contact.getRodzajNastepnegoKontaktu().getName(),2);
    		if(contact.getStatus() != null)
    			ret.setStatus(contact.getStatus().getName());
    	}
    	if(fm.getValue(GRUPA_FIELD_CN) != null)
    	{
    		ret.setAttribute(fm.getValue(GRUPA_FIELD_CN).toString(),4);
    	}
    	String nazwa = "brak";
    	String region = "brak";
    	String role = "";
    	String nip = "brak";
		if(fm.getValue(NUMER_KONTRAHENTA_FIELD_CN) != null)
		{
			nazwa = ((Contractor)fm.getValue(NUMER_KONTRAHENTA_FIELD_CN)).getName();
			if(((Contractor)fm.getValue(NUMER_KONTRAHENTA_FIELD_CN)).getNip() != null)
			{
				nip = ((Contractor)fm.getValue(NUMER_KONTRAHENTA_FIELD_CN)).getNip();
			}
			if(((Contractor)fm.getValue(NUMER_KONTRAHENTA_FIELD_CN)).getRegion() != null)
			{
				region = ((Contractor)fm.getValue(NUMER_KONTRAHENTA_FIELD_CN)).getRegion().getName();
			}
			if(((Contractor)fm.getValue(NUMER_KONTRAHENTA_FIELD_CN)).getRole() != null )
			{
				for(Role r :((Contractor)fm.getValue(NUMER_KONTRAHENTA_FIELD_CN)).getRole()) 
				{
					role += r.getName() + "|";
				}
			}
			else
			{
				role = "Brak";
			}
			
			
		}
		if (role == null || role.length() < 2) 
		{
			role = "brak";
		}
		ret.setAttribute(role,3);
		ret.setAttribute(nazwa,1);
		ret.setAttribute(region,0);
		ret.setAttribute(nip, 5);
		return ret;
	}
    
    public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
    {
    	String channel = document.getDocumentKind().getChannel(document.getId());
    	if(channel != null)
    	{
	    	 if( ProcessCoordinator.find(DocumentLogicLoader.CRM_MARKETING_TASK_KIND,channel) != null &&
	    			 ProcessCoordinator.find(DocumentLogicLoader.CRM_MARKETING_TASK_KIND,channel).size() > 0)
	    	        return ProcessCoordinator.find(DocumentLogicLoader.CRM_MARKETING_TASK_KIND,channel).get(0);
	    	 else
	    		 return null;
    	}
    	return null;
    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
        Map<String,Object> values = new HashMap<String,Object>();
        values.put("UZYTKOWNIK", DSApi.context().getDSUser().getId());
        fm.reloadValues(values);
    }
    
    public String getInOfficeDocumentKind()
    {
        return "Zadanie marketingowe";
    }
}