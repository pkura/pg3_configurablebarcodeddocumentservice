package pl.compan.docusafe.parametrization.ilpol;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;

import com.opensymphony.webwork.ServletActionContext;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionListener;
import pl.compan.docusafe.webwork.event.CloseHibernateSession;
import pl.compan.docusafe.webwork.event.EventActionSupport;
import pl.compan.docusafe.webwork.event.OpenHibernateSession;

public class IlpolRedirectAction extends EventActionSupport 
{
	private static Logger log = LoggerFactory.getLogger(IlpolRedirectAction.class);
			
	
	private String pageName;
	private String applicationNumber;
	private String contractorNip; 
	private Long documentId;
	private Long contractorId;
	private Long applicationId;
	private String searchValues;
	
    protected void setup()
    {
        FillForm fillForm = new FillForm();

        registerListener(DEFAULT_ACTION).
            append(OpenHibernateSession.INSTANCE).
            append(fillForm).
            appendFinally(CloseHibernateSession.INSTANCE);
    }

    private class FillForm implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            try
            {
            	if(StringUtils.isBlank(pageName))
            		throw new EdmException("Nie podano nazwy strony");
            	if("binder".equals(pageName))
            	{
            		applicationNumber = encodeHashParam(applicationNumber);
            		documentId = getBinderByNumerWniosku(applicationNumber);
            		if(documentId == null)
            			throw new EdmException("NIe odnaleziono umowy o numerze "+ applicationNumber);
//            		event.setResult("binder");
            		pageName = "/office/incoming/document-archive.action?documentId="+documentId;
            	}
            	else if("new-document".equals(pageName))
            	{
            		applicationNumber = encodeHashParam(applicationNumber);
            		List<DlApplicationDictionary> dla = DlApplicationDictionary.findByNumerWniosku(applicationNumber);
            		if(dla == null || dla.size() < 1)
            			throw new EdmException("Nie znaleziono wniosku o numerze "+applicationNumber);
            		
            		applicationId = dla.get(0).getId();
            		contractorId = dla.get(0).getIdKlienta();        
            		pageName = "/repository/new-portfolios-for-doc.action?doNew=true&id="+contractorId+"&applicationId="+applicationId;
//            		event.setResult("new-document");
            	}
            	else if("new-marketing-task".equals(pageName))
            	{
            		pageName = "/office/incoming/dwr-document-main.action?documentKindCn=crmMarketingTask";
//            		event.setResult("new-marketing-task");	
            		
            	}
            	else if("search-documents".equals(pageName))
            	{
            		contractorNip = encodeHashParam(contractorNip);
            		
            		Map<String,Object> values = new HashedMap();
            		values.put("KLIENT_NIP",contractorNip);
            		ServletActionContext.getRequest().getSession().setAttribute("values", values);
                	Map<String,Object> invalues = new HashMap<String,Object>();
            		ServletActionContext.getRequest().getSession().setAttribute("invalues", invalues);
            		
            		
            		pageName="/repository/search-dockind-documents.action?doSearch=true&offset=0&limit=50&sortField=id&ascending=false&documentKindCn=dl&fieldValuesSource=session";
//            		event.setResult("search-documents");
            	}
            	else
            	{
            		throw new EdmException("Nie wybrano �adnej opcji");
            	}
            }
            catch (Exception e)
            {
            	log.error(e.getMessage(), e);
                event.setResult("find");
            }
        }
    }
    
    private String encodeHashParam(String hash)
    {
    	System.out.println("Decode "+new String(Base64.decodeBase64(hash.getBytes())));
    	return new String( Base64.decodeBase64(hash.getBytes()));
    }
    
    private Long getBinderByNumerWniosku(String numerWniosku) throws EdmException
	{
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			ps = DSApi.context().prepareStatement("select b.document_id from dsg_dlbinder b, dl_application_dictionary l where" +
					" b.numer_wniosku = l.id and l.numerwniosku = ? ");
			ps.setString(1, numerWniosku);
			rs = ps.executeQuery();
			if (rs.next())
			{
				return rs.getLong(1);
			}
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
		return null;
	}

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public String getApplicationNumber() {
		return applicationNumber;
	}

	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}

	public String getContractorNip() {
		return contractorNip;
	}

	public void setContractorNip(String contractorNip) {
		this.contractorNip = contractorNip;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public Long getContractorId() {
		return contractorId;
	}

	public void setContractorId(Long contractorId) {
		this.contractorId = contractorId;
	}

	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	public String getSearchValues() {
		return searchValues;
	}

	public void setSearchValues(String searchValues) {
		this.searchValues = searchValues;
	}
}

