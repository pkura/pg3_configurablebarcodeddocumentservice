package pl.compan.docusafe.parametrization.ilpol;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.imports.ImpulsImportUtils;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 *
 */
public class WwwApplicationService extends ServiceDriver implements Service
{
	public static final Logger log = LoggerFactory.getLogger(WwwApplicationService.class);
	
	private Timer timer;
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(WwwApplicationService.class.getPackage().getName(),null);
	private Property[] properties;
	private Integer period = 5;
	
    class MailTimesProperty extends Property {
        public MailTimesProperty(){
            super(SIMPLE, PERSISTENT, WwwApplicationService.this, "period", "Co ile minut", Integer.class);
        }
        protected Object getValueSpi(){
            return period;
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (WwwApplicationService.this){
            	if(object != null)
            		period = (Integer) object;
            	else
            		period = 10;
            }
        }
    }


	public WwwApplicationService()
	{
		properties = new Property[] { new MailTimesProperty()};
	}

	protected boolean canStop()
	{
		return false;
	}

	protected void start() throws ServiceException
	{
		log.info("start uslugi WwwApplicationService");
		timer = new Timer(true);
		timer.schedule(new Import(), 0 ,period*DateUtils.MINUTE);
		console(Console.INFO, sm.getString("SystemWystartowal"));
	}

	protected void stop() throws ServiceException
	{
		log.info("stop uslugi WwwApplicationService");
		console(Console.INFO, sm.getString("StopUslugi"));
		if(timer != null) timer.cancel();
	}

	public boolean hasConsole()
	{
		return true;
	}	
	
	class Import extends TimerTask
	{
		@Override
		public void run() 
		{
			console(Console.INFO, "Start");
			Long taskId = Long.MAX_VALUE;
			try
			{
				DSApi.openAdmin();
				int i = 0;
				
				try
				{
					
					try
					{

					}
					catch (Exception e) 
					{
						log.error("B��D importu zadania nr= "+2+""+e.getMessage(),e);
						console(Console.ERROR, "B��D importu zadania nr= "+2+""+e.getMessage());
						DSApi.context().rollback();
						/**Nie wiem co sie pieprzy */
						DSApi.context().session().flush();
						DSApi.context().session().clear();
						DSApi.close();
						DSApi.openAdmin(); 
					}
	                i++;
					if(i%100 == 99)
					{
						log.debug("Czyszczenie sesi po "+i+" dokumentach");
						DSApi.context().session().flush();
						DSApi.context().session().clear();
						DSApi.close();
						DSApi.openAdmin();
					}
				}
				catch (Exception e) {
					log.error(e.getMessage(),e);
					console(Console.ERROR, "B��D importu wezwania "+e.getMessage());
				}
				console(Console.INFO,"Zakonczyl dodawanie "+i+" zada�");
			}
			catch (Exception e) 
			{
				log.error(e.getMessage(),e);
				console(Console.ERROR, "B��D: "+e.getMessage());
			}
			finally
			{
				try
				{
					if(taskId > 0)
					{
						DSApi.context().begin();
						DSApi.context().systemPreferences().node("WwwApplicationService").putLong("lastId",taskId);
						DSApi.context().commit();
					}
					DSApi.close();
				}catch (Exception e) {}
			}
		}
	}
	
	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties) 
	{
		this.properties = properties;
	}
}
