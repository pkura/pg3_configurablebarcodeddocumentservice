package pl.compan.docusafe.parametrization.ilpol;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.dbutils.DbUtils;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.permission.DlBinderPermissionPolicy;
import pl.compan.docusafe.core.base.permission.DlPermissionManager;
import pl.compan.docusafe.core.base.permission.PermissionPolicy;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.crm.Role;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.AbstractEnumColumnField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.EnumRefField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskListMarkerManager;
import pl.compan.docusafe.core.office.workflow.WorkflowActivity;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.service.imports.ImpulsImportUtils;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.web.office.common.DocumentArchiveTabAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class DLBinder extends AbstractDocumentLogic
{
	private static final Logger log = LoggerFactory.getLogger(DLBinder.class);

	private static DLBinder instance;
	public static synchronized DLBinder getInstance()
	{
		if (instance == null)
			instance = new DLBinder();
		return instance;
	}

	public static String DS_DIVISION = "DL_BINDER_DS";
	public static String DR_DIVISION = "DL_BINDER_DR";
	public static String DO_DIVISION = "DL_BINDER_DO";
	public static String DK_DIVISION = "DL_BINDER_DK";

	public static String KLIENT_FIELD_CN = "KLIENT";
	public static String NUMER_UMOWY_FIELD_CN = "NUMER_UMOWY";
	public static String NUMER_WNIOSKU_FIELD_CN = "NUMER_WNIOSKU";
	public static String DOSTAWCA_FIELD_CN = "DOSTAWCA";

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
    	try
    	{
    		if(document instanceof InOfficeDocument) 
    		{    		
    			if(!((InOfficeDocument)document).getKind().getName().equals(getInOfficeDocumentKind()))
    				((InOfficeDocument)document).setKind(InOfficeDocumentKind.findByPartialName(getInOfficeDocumentKind()).get(0));
    		}
    	}
    	catch (Exception e) {
			log.error("",e);
		}
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        if(fm.getKey(NUMER_WNIOSKU_FIELD_CN) == null)
        	throw new EdmException("Musisz wybra� wniosek");


        String umowa = "brak";
		String wniosek = "brak";
		if (fm.getValue(NUMER_UMOWY_FIELD_CN) != null)
		{
			umowa = ((DlContractDictionary) fm.getValue(NUMER_UMOWY_FIELD_CN)).getNumerUmowy();
		}
		if (fm.getValue(NUMER_WNIOSKU_FIELD_CN) != null)
		{
			wniosek = ((DlApplicationDictionary) fm.getValue(NUMER_WNIOSKU_FIELD_CN)).getNumerWniosku();
		}
        document.setTitle(umowa + "/" + wniosek);

        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent("Teczki");
        Contractor con = (Contractor)fm.getValue(KLIENT_FIELD_CN);
        if(con != null && con.getName() != null)
        {
        	folder = folder.createSubfolderIfNotPresent(FolderInserter.to2Letters(con.getName()));
        	folder = folder.createSubfolderIfNotPresent(con.getName());
        }
        else
        {
        	folder = folder.createSubfolderIfNotPresent("Poczekalnia");
        }
        document.setFolder(folder);

        if(fm.getKey(NUMER_UMOWY_FIELD_CN) != null)
        {
        	// sprawdzic co jest potrzebne
        	ArrayList<Long> a = getNumeryUmowPowiazaneZWnioskiem(Long.parseLong(""+fm.getKey(NUMER_WNIOSKU_FIELD_CN)));
        	if(!a.contains(Long.valueOf(fm.getKey(NUMER_UMOWY_FIELD_CN).toString())))
        	{
        		a.add(Long.valueOf(fm.getKey(NUMER_UMOWY_FIELD_CN).toString()));
        	}
        	setNumerUmowy(Long.parseLong(""+fm.getKey(NUMER_WNIOSKU_FIELD_CN)),a);
        }
	}
	
    public void onLoadData(FieldsManager fm) throws EdmException
    {
    	if(AvailabilityManager.isAvailable("dlbinder.loadUmowa") && fm.getKey(NUMER_UMOWY_FIELD_CN) == null && fm.getValue(NUMER_WNIOSKU_FIELD_CN) != null)
    	{
    		log.error(fm.getDocumentId()+ " czas in  "+new Date().getTime());
    		ArrayList<Long> contractIds = new ArrayList<Long>();
    		DlApplicationDictionary app = (DlApplicationDictionary) fm.getValue(NUMER_WNIOSKU_FIELD_CN);
    		ArrayList<DlContractDictionary> dlc = ImpulsImportUtils.findContractByAppNumber(app.getNumerWniosku(),app.getId());
    		for(DlContractDictionary d : dlc)
    		{
    			log.error("Id importowanej umowy: " + d.getId());
    			contractIds.add(d.getId());
    		}
    			
    		if(dlc != null && dlc.size()>0)
    		{
    			// w tej metodzie ustawiamy id wszystkich umow powiazanych z wnioskiem dla dokumentow z teczki
	    		setNumerUmowy(app.getId(),contractIds);
	    		setAddNumerUmowy(contractIds.get(0),fm.getDocumentId());
	    		Map<String,Object> values = new HashMap<String, Object>();
	    		values.put(NUMER_UMOWY_FIELD_CN, contractIds.get(0));
	    		fm.reloadValues(values);
    		}
    		if(dlc != null && dlc.size()>1)
    		{
    			for(int i=1;i<dlc.size();++i)
    			{
    				createNewBinder(contractIds.get(i), fm);
    			}    			
    		}

    		
    		log.error(fm.getDocumentId()+ " czas out "+new Date().getTime());
    	}
    }
    
    public void setAddNumerUmowy(Long contractId,Long documentId)
    {
    	PreparedStatement ps = null;
    	try
    	{
    		boolean isopen = true;
    		if(!DSApi.context().isTransactionOpen())
    		{
    			isopen = false;
    			DSApi.context().begin();
    		}
    		ps = DSApi.context().prepareStatement("update dsg_dlbinder set numer_umowy = ? where document_id = ?");
			ps.setLong(1, contractId);
			ps.setLong(2, documentId);
			int i = ps.executeUpdate();
			ps.close();
			if(!isopen)
			{
				DSApi.context().commit();
			}
    	}
    	catch (Exception e)
    	{
			log.error("",e);
		}
    	finally
    	{
    		DbUtils.closeQuietly(ps);
    	}
    }


    public void setNumerUmowy(Long applicationId,ArrayList<Long> contractIds)
    {
    	PreparedStatement ps = null;
    	try
    	{
    		ps = DSApi.context().prepareStatement("delete from dsg_leasing_multiple_value where field_cn = 'NUMER_UMOWY' "+
    				"and document_id in (select document_id from dsg_leasing_multiple_value where field_cn = 'NUMER_WNIOSKU' and field_val = ? ) ");
    		ps.setString(1, applicationId.toString());
    		ps.execute();
    		ps.close();

    		for(int i=0;i<contractIds.size();++i)
    		{
	    		ps = DSApi.context().prepareStatement("insert into dsg_leasing_multiple_value(document_id, field_cn, field_val) " +
	    				"select document_id,'NUMER_UMOWY',? from dsg_leasing_multiple_value  where field_cn = 'NUMER_WNIOSKU' and field_val = ?");
				ps.setLong(1, contractIds.get(i));
				ps.setLong(2, applicationId);
				ps.execute();
				ps.close();
    		}
    	}
    	catch (Exception e)
    	{
			log.error("",e);
		}
    	finally
    	{
    		DbUtils.closeQuietly(ps);
    	}
    }
    
    public static ArrayList<Long> getNumeryUmowPowiazaneZWnioskiem(Long applicationId)
    {
    	PreparedStatement ps = null;
    	ArrayList<Long> a = new ArrayList<Long>();
    	try
    	{
//    		ps = DSApi.context().prepareStatement("select field_value from dsg_leasing_multiple_value where field_cn = 'NUMER_UMOWY' "+
//    				"and document_id in (select document_id from dsg_leasing_multiple_value where field_cn = 'NUMER_WNIOSKU' and field_val = ? ) ");   		
    		ps = DSApi.context().prepareStatement("select numer_umowy from dsg_dlbinder where numer_wniosku = ?");
    		ps.setString(1, applicationId.toString());
    		ResultSet rs = ps.executeQuery();
    		while(rs.next())
    		{
    			a.add(rs.getLong(1));
    		}
    		rs.close();
    		ps.close();
    		    		
    	}
    	catch (Exception e)
    	{
			log.error(e.getMessage(),e);
		}
    	finally
    	{
    		DbUtils.closeQuietly(ps);
    		return a;
    	}
    	
    }

    public TaskListParams getTaskListParams(DocumentKind dockind, long id)throws EdmException
	{
    	TaskListParams params = new TaskListParams();
    	try
    	{
			FieldsManager fm = dockind.getFieldsManager(id);
			if (fm.getValue(KLIENT_FIELD_CN) != null)
			{
				params.setAttribute(((Contractor) fm.getValue(KLIENT_FIELD_CN)).getName(), 1);
				if (((Contractor) fm.getValue(KLIENT_FIELD_CN)).getRegion() != null)
				{
					params.setAttribute(((Contractor) fm.getValue(KLIENT_FIELD_CN)).getRegion().getName(), 0);
				}
				else
				{
					params.setAttribute("brak", 0);
				}
				String role = "";
				if(((Contractor)fm.getValue(KLIENT_FIELD_CN)).getRole() != null )
				{
					for(Role r :((Contractor)fm.getValue(KLIENT_FIELD_CN)).getRole())
					{
						role += r.getName() + "|";
					}
				}
				else
				{
					role = "Brak";
				}
				params.setAttribute(role,2);
			}
			else
			{
				params.setAttribute("brak", 0);
				params.setAttribute("brak", 1);
			}
			String umowa = "brak";
			String wniosek = "brak";
			String typ = "brak";
			if (fm.getKey(NUMER_UMOWY_FIELD_CN) != null)
			{
				umowa = ((DlContractDictionary) fm.getValue(NUMER_UMOWY_FIELD_CN)).getNumerUmowy();
				if(fm.getValue(NUMER_UMOWY_FIELD_CN) != null && ((DlContractDictionary) fm.getValue(NUMER_UMOWY_FIELD_CN)).getLeasingObjectType() != null)
						typ = LeasingObjectType.findById(((DlContractDictionary) fm.getValue(NUMER_UMOWY_FIELD_CN)).getLeasingObjectType()).getName();
			}
			
			if (fm.getKey(NUMER_WNIOSKU_FIELD_CN) != null)
			{
				wniosek = ((DlApplicationDictionary) fm.getValue(NUMER_WNIOSKU_FIELD_CN)).getNumerWniosku();
				if(fm.getValue(NUMER_WNIOSKU_FIELD_CN) != null && ((DlApplicationDictionary) fm.getValue(NUMER_WNIOSKU_FIELD_CN)).getLeasingObjectType() != null)
					typ = LeasingObjectType.findById(((DlApplicationDictionary) fm.getValue(NUMER_WNIOSKU_FIELD_CN)).getLeasingObjectType()).getName();
			}
			params.setDocumentNumber(umowa + "|" + wniosek);
			if(fm.getKey("WORKFLOW_PLACE") != null)
			{
				String wfp = fm.getEnumItem("WORKFLOW_PLACE").getCn();
				if(assHistCounter(id,wfp) > 0)
				{
					params.setMarkerClass(TaskListMarkerManager.PREFIX+"Alert");
				}
			}
			params.setCategory(typ);
    	}
    	catch (Exception e) 
    	{
			log.error(e.getMessage(),e);
		}
		return params;
	}
    
    /***
     * Metoda sprawdza czy dokument by ju� w tym dziale
     */
    public Integer assHistCounter(Long documentId,String wfp)
    {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try
    	{
    		ps = DSApi.context().prepareStatement("select count(1) from dso_document_asgn_history where targetguid = ? and document_id =  ? and targetuser is not null");
    		ps.setString(1, "DL_BINDER_"+wfp);
    		ps.setLong(2, documentId);
    		rs = ps.executeQuery();
    		if(rs.next())
    			return rs.getInt(1);
    	}
    	catch (Exception e)
    	{
			log.error("",e);
		}
    	finally
    	{
    		DbUtils.closeQuietly(ps);
    	}
    	return 0;
    }

	public PermissionPolicy getPermissionPolicy()
	{
		return new DlBinderPermissionPolicy();
	}

	public void documentPermissions(Document document) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
    	try
    	{
	        Long numerwniosku = null;
	        Long numwerUmowy = null;
	        if(fm.getKey(NUMER_WNIOSKU_FIELD_CN) != null )
	        	numerwniosku = (Long) fm.getKey(NUMER_WNIOSKU_FIELD_CN);
	        if(fm.getKey(NUMER_UMOWY_FIELD_CN) != null )
	        	numwerUmowy = (Long) fm.getKey(NUMER_UMOWY_FIELD_CN);
	        
	        Map<String,String> map = DlPermissionManager.getUsers(numerwniosku,numwerUmowy);
	        Set<String> set = map.keySet();
	        for (String key : set)
			{
	            perms.add(new PermissionBean(ObjectPermission.READ, map.get(key), ObjectPermission.USER));
	            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, map.get(key), ObjectPermission.USER));
			}
	    	String region = null;
			
	    	if(fm.getValue(KLIENT_FIELD_CN) != null && ((Contractor)fm.getValue(KLIENT_FIELD_CN)).getRegion() != null)
			{
				if(((Contractor)fm.getValue(KLIENT_FIELD_CN)).getRegion() != null)
				{
					region = ((Contractor)fm.getValue(KLIENT_FIELD_CN)).getRegion().getCn();
				}
	        }
	        if(region != null && region.length() > 0)
	        {      	
	        	perms.add(new PermissionBean(ObjectPermission.READ, "DV_" +	region, ObjectPermission.GROUP));
	            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DV_" +	region, ObjectPermission.GROUP));
	        }
    	}
    	catch (Exception e) 
    	{
			log.error(e.getMessage(),e);
		}
		perms.add(new PermissionBean(ObjectPermission.READ, "DL_READ", ObjectPermission.GROUP));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_READ", ObjectPermission.GROUP));
        //teczki
       // perms.add(new PermissionBean(ObjectPermission.READ, "DL_BINDER_DS", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ, "DL_BINDER_DR", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ, "DL_BINDER_DO", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ, "DL_BINDER_DK", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ, "DL_BINDER_DU", ObjectPermission.GROUP));
     //   perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_BINDER_DS", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_BINDER_DR", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_BINDER_DO", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_BINDER_DK", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_BINDER_DU", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ,"DL_ADMIN", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ,"DV_ALL", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ,"DL_DEL_ATTA", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "DL_ADMIN", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "DV_ALL", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "DL_DEL_ATTA", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_ADMIN", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DV_ALL", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_DEL_ATTA", ObjectPermission.GROUP));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DL_ADMIN", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DV_ALL", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DL_DEL_ATTA", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ, "DL_DEL_DOC", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY,"DL_DEL_DOC", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DL_DEL_DOC", ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DL_DEL_DOC", ObjectPermission.GROUP));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DL_DEL_DOC", ObjectPermission.GROUP));
		
		perms.add(new PermissionBean(ObjectPermission.READ, document.getAuthor() , ObjectPermission.USER));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getAuthor() , ObjectPermission.USER));
        
		this.setUpPermission(document, perms);
	}
	
	public boolean searchCheckPermissions(Map<String, Object> values)
	{
		return false;
	}

	
	public void acceptTask(Document document, ActionEvent event, String activity)
	{
		try
		{
			try
            {
                WorkflowActivity activityWF= activity != null ? WorkflowFactory.getWfActivity(activity): null;
                if (activity != null)
                {
                    DSApi.context().watch(URN.create(activityWF));
                }
                else
                {
                    DSApi.context().watch(URN.create(Document.find(document.getId())));
                }
            }
            catch (EdmException e)
            {
            	event.addActionError(e.getMessage());
				log.error(e.getMessage(), e);
            }
            
			FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
			String wfp = fm.getEnumItem("WORKFLOW_PLACE").getCn();
			Map<String, Object> fieldValues = new HashMap<String, Object>();
			DSDivision division = DSDivision.find("DL_BINDER_" + wfp);
			if(!DSDivision.isInDivision(division))
			{
				event.addActionError("Nie zosta�e� przypisany jako opiekun poniewa� nie jeste� w dziale "+ division.getName());
				return;
			}
			fieldValues.put("DS_USER_" + wfp, DSApi.context().getPrincipalName());
			
			document.getDocumentKind().setOnly(document.getId(), fieldValues);
		}
		catch (Exception e)
		{
			log.error("", e);
		}
	}

	private void goTO(DockindButtonAction eventActionSupport, String dockingEvent, ActionEvent event, Map<String, Object> values, Document document)
			throws EdmException
	{
		DSApi.context().begin();
		String activityId = null;
		if (eventActionSupport instanceof DocumentArchiveTabAction)
			activityId = ((DocumentArchiveTabAction) eventActionSupport).getActivity();
		else
		{
			event.addActionMessage("Nie mo�na wykona� tej akcji.");
			return;
		}
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		String guidCn = dockingEvent.substring(5);
		String userTo = (String) fm.getKey("DS_USER_" + guidCn);
		if (userTo != null && userTo.length() < 3)
			userTo = null;
		String guidTo = "DL_BINDER_" + guidCn;
		WorkflowFactory.simpleAssignment(activityId, guidTo, userTo, DSApi.context().getPrincipalName(), null, WorkflowFactory.SCOPE_DIVISION, null, event);
		Map<String, Object> fieldValues = new HashMap<String, Object>();
		fieldValues.put("WORKFLOW_PLACE", fm.getField("WORKFLOW_PLACE").getEnumItemByCn(guidCn).getId());
		document.getDocumentKind().setOnly(document.getId(), fieldValues);
		event.skip(DocumentArchiveTabAction.EV_FILL);
		event.setResult("confirm");
		DSApi.context().commit();
	}

	private void newDocument(DockindButtonAction eventActionSupport, String dockingEvent, ActionEvent event, Map<String, Object> values)
			throws EdmException
	{
		String[] enumTab;
		Long klient = null;
		try
		{
			enumTab = (String[]) values.get("DOCLISTnewDocument");
		}
		catch (ClassCastException e)
		{
			enumTab = new String[1];
			enumTab[0] = (String) values.get("DOCLISTnewDocument");
		}

		String newDokumentKind = (String) values.get("DOCLISTnewDocumentKind");
		Integer rodzajKontrahenta = 10;
		klient = values.get("KLIENT") != null ? Long.parseLong((String) values.get("KLIENT")) : null;
		if ("DD".equals(newDokumentKind))
		{
			rodzajKontrahenta = 20;
			klient = values.get("DOSTAWCA") != null ? Long.parseLong((String) values.get("DOSTAWCA")) : null;

		}

		if (enumTab == null || enumTab.length < 1)
			throw new EdmException("Nie wybrano dokument�w");

		DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.DL_KIND);
		Map<String, EnumItem> enumMap = new HashMap<String, EnumItem>();
		for (EnumItem ei : DlLogic.getEnumItms(kind))
		{
			enumMap.put(ei.getCn(), ei);
		}
		ArrayList<Long> umowa = new ArrayList<Long>();
		Long wniosek = values.get("NUMER_WNIOSKU") != null ? Long.parseLong((String) values.get("NUMER_WNIOSKU")) : null;
		if(values.get("DOCLISTattachToOtherBinder") != null && values.get("DOCLISTattachToOtherBinder").equals("true"))
		{
			//umowa.add( values.get("NUMER_UMOWY") != null ? Long.parseLong((String) values.get("NUMER_UMOWY")) : null);
			umowa = getNumeryUmowPowiazaneZWnioskiem(wniosek);
		}
		else 
		{
			umowa.add( values.get("NUMER_UMOWY") != null ? Long.parseLong((String) values.get("NUMER_UMOWY")) : null);
		}

		for (int i = 0; i < enumTab.length; i++)
		{
			EnumItem ei = enumMap.get(enumTab[i]);
			Field f = kind.getFieldByCn(ei.getFieldCn());
			if (DlLogic.TYP_DOKUMENTU_CN.equals(ei.getFieldCn()))
			{
				Integer discriminator = Integer.parseInt(ei.getRefValue());
				createDoc(ei.getId(), discriminator, umowa, wniosek, null, null, klient, rodzajKontrahenta, event, ei);
			}
			else
			{
				Integer availableWhen = Integer.parseInt(f.getAvailableWhen().get(0).getFieldValue().toString());
				Field f2 = kind.getFieldByCn(DlLogic.TYP_DOKUMENTU_CN);
				Integer discriminator = (Integer) ((EnumRefField) f2).getEnumItemDiscriminator(availableWhen);
				createDoc(availableWhen, discriminator, umowa, wniosek, ei.getId(), f.getCn(), klient, rodzajKontrahenta, event, ei);
			}
		}
	}

	private Document createDoc(Integer type, Integer parentField, ArrayList<Long> contract, Long application, Integer rodzaj, String cn, Long kontrahentId,
			Integer rodzajKontrahenta, ActionEvent event, EnumItem ei) throws EdmException
	{
		try
		{
			Map<String, Object> values = new HashMap<String, Object>();
			DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.DL_KIND);
			DockindQuery dockindQuery = new DockindQuery(0, 2);
			dockindQuery.setDocumentKind(documentKind);
			dockindQuery.setCheckPermissions(false);
			dockindQuery.enumField(documentKind.getFieldByCn("RODZAJ_KONTRAHENTA"), rodzajKontrahenta);
			dockindQuery.enumField(documentKind.getFieldByCn(DlLogic.RODZAJ_DOKUMENTU_CN), parentField);
			dockindQuery.enumField(documentKind.getFieldByCn(DlLogic.TYP_DOKUMENTU_CN), type);
			if (application != null)
				dockindQuery.field(documentKind.getFieldByCn(DlLogic.NUMER_WNIOSKU_CN), application);
			if (contract != null && !contract.isEmpty())
				dockindQuery.field(documentKind.getFieldByCn(DlLogic.NUMER_UMOWY_CN), contract.get(0)); 
			if (cn != null && rodzaj != null)
			{
				dockindQuery.enumField(documentKind.getFieldByCn(cn), rodzaj);
			}
			dockindQuery.setCheckPermissions(false);
			SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
			if (searchResults.count() != 0)
			{
				event.addActionMessage("Istnieje ju� " + ei.getTitle());
				return null;
			}
			else
			{
				DSApi.context().begin(); 
				String summary = "Dokument LEASINGOWY";
				Document document = null;
				document = new Document(summary, summary);
				document.setDocumentKind(documentKind);
				document.setForceArchivePermissions(true);
				document.setCtime(new Date());
				document.setFolder(Folder.getRootFolder());
				document.create();
				Long newDocumentId = document.getId();
				values.put(DlLogic.RODZAJ_DOKUMENTU_CN, parentField);
				values.put(DlLogic.TYP_DOKUMENTU_CN, type);
				if (cn != null && rodzaj != null)
					values.put(cn, rodzaj);
				values.put(DlLogic.KLIENT_CN, kontrahentId);
				if (application != null)
					values.put(DlLogic.NUMER_WNIOSKU_CN, application);
				if (contract != null)
					values.put(DlLogic.NUMER_UMOWY_CN, contract);
				values.put("RODZAJ_KONTRAHENTA", rodzajKontrahenta);
				documentKind.setOnly(newDocumentId, values);
				documentKind.logic().archiveActions(document, DocumentLogic.TYPE_ARCHIVE);
				documentKind.logic().documentPermissions(document);
				DSApi.context().commit();
				log.trace("Dodaje dokument");
				event.addActionMessage("Dodano dokument " + ei.getTitle());
				return document;
			}
		}
		catch (Exception e)
		{
			DSApi.context().rollback();
			log.error( e.getMessage(),e);
			throw new EdmException("B��d dodania dokumentu " + ei.getTitle());
		}
	}
	
	 public void onStartProcess(OfficeDocument document)
	 {
		 try
		 {
			 DocumentKind documentKindDL = DocumentKind.findByCn(DocumentLogicLoader.DL_KIND);
			 DocumentKind  documentKindDLB =DocumentKind.findByCn(DocumentLogicLoader.DL_BINDER);
			 FieldsManager fm = documentKindDLB.getFieldsManager(document.getId());
			 DockindInfoBean dif = DocumentKind.getDockindInfo(DocumentLogicLoader.DL_BINDER);
			 String docsType = dif.getProperties().get("required-document-type");
			 String docspType = dif.getProperties().get("required-document-ptype");
			 if(docsType != null && docsType.length() > 0)
			 {
				 String[] types = docsType.split(";");
				 for (String type : types) 
				 {
					 EnumRefField enumRef = (EnumRefField) documentKindDL.getDockindInfo().getDockindFieldsMap().get(DlLogic.TYP_DOKUMENTU_CN);
					 Integer typeId = enumRef.getEnumItemByCn(type).getId();
					 Integer rodzaj = (Integer) enumRef.getEnumItemDiscriminator(typeId);
					 createDoc(rodzaj, typeId, null, null, fm, documentKindDL, DlLogic.RODZAJ_KONTRAHENTA_DOSTAWCA);
					 createDoc(rodzaj, typeId, null, null, fm, documentKindDL, DlLogic.RODZAJ_KONTRAHENTA_KLIENT);
				 }
			 }
			 if(docspType != null && docspType.length() > 0)
			 {
				 String[] ptypes = docspType.split(";");
				 for (String ptype : ptypes) 
				 {
					 String[] ptypeTab = ptype.split(",");
					 Integer ptypeId = Integer.parseInt(ptypeTab[1]);
					 String ptypeFieldCn = ptypeTab[0];
					 
					 AbstractEnumColumnField enumF = (AbstractEnumColumnField) documentKindDL.getDockindInfo().getDockindFieldsMap().get(ptypeFieldCn);
					 Integer availableWhen = enumF.getAvailableWhen().get(0).getFieldValue().intValue();
					 EnumRefField enumRef = (EnumRefField) documentKindDL.getDockindInfo().getDockindFieldsMap().get(DlLogic.TYP_DOKUMENTU_CN);
					 Integer typeId = enumRef.getEnumItem(availableWhen).getId();
					 Integer rodzaj = (Integer) enumRef.getEnumItemDiscriminator(typeId);
					 createDoc(rodzaj, typeId, ptypeId, ptypeFieldCn, fm, documentKindDL, DlLogic.RODZAJ_KONTRAHENTA_DOSTAWCA);
					 createDoc(rodzaj, typeId, ptypeId, ptypeFieldCn, fm, documentKindDL, DlLogic.RODZAJ_KONTRAHENTA_KLIENT);
				 }
			 }
		 }
		 catch (Exception e) 
		 {
			log.error("",e);
			e.printStackTrace();
		}
	 }
	 // dopisuje nowa teczke dla danego wniosku i wielu umow
	 private void createNewBinder(Long contractId, FieldsManager fm) throws EdmException //Integer rodzaj,Integer typ,Integer podtypValue,String podtypCN,FieldsManager fm,Integer contractorKind) throws EdmException
	 {
		 	Map<String, Object> values = fm.getFieldValues();
		    DocumentKind  documentKindDLB =DocumentKind.findByCn(DocumentLogicLoader.DL_BINDER);
			String summary = "Dokument Obowi�zkowy";
			Document document = null;
			document = new Document(contractId + "/" + fm.getKey(DLBinder.NUMER_WNIOSKU_FIELD_CN), "Teczka");
			document.setDocumentKind(documentKindDLB);
			document.setForceArchivePermissions(true);
			Document docTemp = Document.find(fm.getDocumentId());
			document.setFolder(docTemp.getFolder());
			docTemp = null;
			document.create();
			Long newDocumentId = document.getId();
			
			values.remove(DLBinder.NUMER_UMOWY_FIELD_CN);
			values.put(DLBinder.NUMER_UMOWY_FIELD_CN, contractId);

//			values.put(DlLogic.RODZAJ_DOKUMENTU_CN, rodzaj);
//			values.put(DlLogic.TYP_DOKUMENTU_CN, typ);
//			if (podtypCN != null && podtypValue != null)
//				values.put(podtypCN, podtypValue);			
//			
//			if(DlLogic.RODZAJ_KONTRAHENTA_KLIENT.equals(contractorKind) && fm.getKey(DLBinder.DOSTAWCA_FIELD_CN) != null)
//			{
//				values.put(DlLogic.KLIENT_CN, fm.getKey(DLBinder.DOSTAWCA_FIELD_CN) );
//			}
//			else if(fm.getKey(DLBinder.KLIENT_FIELD_CN) != null)
//			{
//				values.put(DlLogic.KLIENT_CN, fm.getKey(DLBinder.KLIENT_FIELD_CN));
//			}
//			values.put("RODZAJ_KONTRAHENTA", contractorKind);
//			
//			if (fm.getKey(DLBinder.NUMER_WNIOSKU_FIELD_CN) != null)
//				values.put(DlLogic.NUMER_WNIOSKU_CN, fm.getKey(DLBinder.NUMER_WNIOSKU_FIELD_CN) );
//			if (fm.getKey(DLBinder.NUMER_UMOWY_FIELD_CN) != null)
//				values.put(DlLogic.NUMER_UMOWY_CN, fm.getKey(DLBinder.NUMER_UMOWY_FIELD_CN));
//			
			documentKindDLB.setOnly(newDocumentId, values);
			documentKindDLB.logic().archiveActions(document, DocumentLogic.TYPE_ARCHIVE);
			documentKindDLB.logic().documentPermissions(document);
			log.trace("Dodaje teczke " + document.getId());
	 }
	 
	 private void createDoc(Integer rodzaj,Integer typ,Integer podtypValue,String podtypCN,FieldsManager fm,DocumentKind documentKindDL,Integer contractorKind) throws EdmException
	 {
		 	Map<String, Object> values = new HashMap<String, Object>();
			String summary = "Dokument Obowi�zkowy";
			Document document = null;
			document = new Document(summary, summary);
			document.setDocumentKind(documentKindDL);
			document.setForceArchivePermissions(true);
			document.setFolder(Folder.getRootFolder());
			document.create();
			Long newDocumentId = document.getId();
			values.put(DlLogic.RODZAJ_DOKUMENTU_CN, rodzaj);
			values.put(DlLogic.TYP_DOKUMENTU_CN, typ);
			if (podtypCN != null && podtypValue != null)
				values.put(podtypCN, podtypValue);			
			
			if(DlLogic.RODZAJ_KONTRAHENTA_KLIENT.equals(contractorKind) && fm.getKey(DLBinder.DOSTAWCA_FIELD_CN) != null)
			{
				values.put(DlLogic.KLIENT_CN, fm.getKey(DLBinder.DOSTAWCA_FIELD_CN) );
			}
			else if(fm.getKey(DLBinder.KLIENT_FIELD_CN) != null)
			{
				values.put(DlLogic.KLIENT_CN, fm.getKey(DLBinder.KLIENT_FIELD_CN));
			}
			values.put("RODZAJ_KONTRAHENTA", contractorKind);
			
			if (fm.getKey(DLBinder.NUMER_WNIOSKU_FIELD_CN) != null)
				values.put(DlLogic.NUMER_WNIOSKU_CN, fm.getKey(DLBinder.NUMER_WNIOSKU_FIELD_CN) );
			if (fm.getKey(DLBinder.NUMER_UMOWY_FIELD_CN) != null)
				values.put(DlLogic.NUMER_UMOWY_CN, fm.getKey(DLBinder.NUMER_UMOWY_FIELD_CN));
			
			documentKindDL.setOnly(newDocumentId, values);
			documentKindDL.logic().archiveActions(document, DocumentLogic.TYPE_ARCHIVE);
			documentKindDL.logic().documentPermissions(document);
			log.trace("Dodaje dokument " + document.getId());
	 }

		
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
		
    }
		

	public void doDockindEvent(DockindButtonAction eventActionSupport, ActionEvent event,Document document, String activity,Map<String, Object> values) throws EdmException {
		if ("DOCLIST".equals(eventActionSupport.getDockindEventValue()))
			newDocument(eventActionSupport, eventActionSupport.getDockindEventValue(), event, values);
		else if (eventActionSupport.getDockindEventValue().startsWith("GOTO"))
		{
			goTO(eventActionSupport, eventActionSupport.getDockindEventValue(), event, values, document);
		}
	}
	
    public String getInOfficeDocumentKind()
    {
        return "Teczka";
    }
    
    
	public static LinkedHashMap<String,String> getDSUserAndDivisions() throws EdmException 
	{		        
		LinkedHashMap<String,String> tempMap = new LinkedHashMap<String,String>();
		List<DSDivision> dsd = UserFactory.getInstance().getAllCanAssignDivisionsDivisionType();
    	List<DSDivision> tempForUser = new ArrayList();
		Collections.sort(dsd, new DSDivision.DivisionComparatorAsName());
    	for(DSDivision d : dsd)
    	{
    		if((d.isHidden() || d.isPosition()) || (d.isGroup() && !d.getName().startsWith("Region ")))
    			continue;
    		tempMap.put(d.getName(), d.getName());
    		tempForUser.add(d);
    	}
    	dsd = null;
    	
    	
    	List<DSUser> result; 
    	if (DSApi.context().hasPermission(DSPermission.ZADANIA_WSZYSTKIE_PODGLAD))
         {
             result =  DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
         }
         else if (DSApi.context().hasPermission(DSPermission.ZADANIA_KOMORKA_PODGLAD))
         {       
         	ArrayList<DSUser> users = new ArrayList<DSUser>();

         	for(DSDivision d : tempForUser)
         	{
         		users.addAll(Arrays.asList(d.getUsers()));
         	}
         	
         	//wywalamy powtarzaj�cych si� u�ytkownik�w
    		ArrayList<DSUser> ret = new ArrayList<DSUser>();
         	for(DSUser user : users)
         	{
         		if(!ret.contains(user) && !user.getRoles().contains("archive_only")){
         			ret.add(user);
         		}
         	}
         	result =  ret;            
         }
         else
         {
             List<DSUser> list = new ArrayList<DSUser>(1);
             list.add(DSApi.context().getDSUser());
             result = list;
         }
    	 List<DSUser> result2 = new LinkedList<DSUser>();
    	 for (DSUser user : result) {
    		if (!user.getRoles().contains("archive_only")) {
    			result2.add(user);
    		}
    	 }
    	 Collections.sort(result2, DSUser.LASTNAME_COMPARATOR);
    	
    	
		Collections.sort(result2, new DSUser.UserComparatorAsLastFirstName());    	
        for(DSUser d : result2)
        {
        	tempMap.put(d.getName(),d.asLastnameFirstname());
        }
        return tempMap;
     }    
	
	public final List<DSDivision> getAllDivisions(boolean deleted) throws EdmException
	   {
		   	 if (DSApi.context().hasPermission(DSPermission.ZADANIA_WSZYSTKIE_PODGLAD))
		   	 {
		        	DSDivision[] divs = DSDivision.getAllDivisions();
		        	List<DSDivision> divisTemp = new LinkedList<DSDivision>();
		        	for(DSDivision a : divs)
		        	{
		            		if(!a.isHidden() || deleted)
		            		{
		            			divisTemp.add(a);
		            		}
		        	}
		   		 return divisTemp ;
		     }
		     else if (DSApi.context().hasPermission(DSPermission.ZADANIA_KOMORKA_PODGLAD))
		     {
		        	DSDivision[] divs = DSApi.context().getDSUser().getSubordinateDivisions();
		        	List<DSDivision> divisTemp = new LinkedList<DSDivision>();
		        	for(DSDivision a : divs)
		        	{
		        		if(!a.isHidden() || deleted)
	            		{
	            			divisTemp.add(a);
	            		}
		            	
		        	}
		        	divs = null;
		        	return divisTemp;    
		     }
		     else
		     {
		        	return new LinkedList<DSDivision>();
		     }
	   }
}
