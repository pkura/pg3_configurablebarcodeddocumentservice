package pl.compan.docusafe.parametrization.ilpol;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.permission.DlPermissionManager;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.crm.Role;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.parametrization.ilpoldwr.LeasingLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

/**
 * 
 * @author Mariusz Kilja�czyk
 */
public class CrmVindicationLogic extends AbstractDocumentLogic
{

	private static final Logger log = LoggerFactory.getLogger(CrmVindicationLogic.class);
	private static CrmVindicationLogic instance;

	public static final String NUMER_KONTRAHENTA_FIELD_CN = "KLIENT";
	public static final String NUMER_PRACOWNIKA_FIELD_CN = "NUMER_PRACOWNIKA";
	public static final String NUMER_WEZWANIA_FIELD_CN = "NUMER_WEZWANIA";
	public static final String DATA_WEZWANIA_FIELD_CN = "DATA_WEZWANIA";
	public static final String NUMER_FAKTURY_FIELD_CN = "NUMER_FAKTURY";
	public static final String NALEZNOSC_PIERWOTNA_FIELD_CN = "NALEZNOSC_PIERWOTNA";
	public static final String NALEZNOSC_FIELD_CN = "NALEZNOSC";
	public static final String WYNIK_KONTAKTU_FIELD_CN = "WYNIK_KONTAKTU";
	public static final String DATA_KOLEJNEGO_KONTAKTU_FIELD_CN = "DATA_KOLEJNEGO_KONTAKTU";
	public static final String DATA_KOLEJNEGO_AKCJI_FIELD_CN = "DATA_KOLEJNEGO_AKCJI";
	public static final String OPIS_FIELD_CN = "OPIS";
	public static final String WINDYKACJA_FIELD_CN = "WINDYKACJA";
	public static final String REGION_FIELD_CN = "REGION";

	public static final String STATUS_NOWY_FIELD_CN = "NOWY";
	public static final String STATUS_NIE_ODBIERA_3_FIELD_CN = "NIE_ODBIERA_3";
	public static final String STATUS_BLEDNY_NUMER_FIELD_CN = "BLEDNY_NUMER";
	public static final String STATUS_KONTAKT_PRZELOZONY_FIELD_CN = "KONTAKT_PRZELOZONY";
	public static final String STATUS_ZAPLACIL_FIELD_CN = "ZAPLACIL";
	public static final String STATUS_ZAPLACI_W_CIAGU_3_DNI_FIELD_CN = "ZAPLACI_W_CIAGU_3_DNI";
	public static final String STATUS_NIE_OTRZYMAL_FAKTURY_FIELD_CN = "NIE_OTRZYMAL_FAKTURY";
	public static final String STATUS_ZAKSIEGOWANO_FIELD_CN = "ZAKSIEGOWANO";

	public static CrmVindicationLogic getInstance()
	{
		if (instance == null)
			instance = new CrmVindicationLogic();
		return instance;
	}

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("CRM");
		folder = folder.createSubfolderIfNotPresent("Windykacja");
		document.setFolder(folder);

		Map<String, Object> values = new HashMap<String, Object>();
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		if (fm.getKey(NUMER_KONTRAHENTA_FIELD_CN) != null)
		{
			Contractor con = Contractor.findById((Long) fm.getKey(NUMER_KONTRAHENTA_FIELD_CN));
			if (con.getRegion() != null)
				values.put(REGION_FIELD_CN, con.getRegion().getCn());
		}
		document.getDocumentKind().setOnly(document.getId(), values);

	}

	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- VindicationLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, document.getAuthor());
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}
	
	public void documentPermissions(Document document) throws EdmException
	{
//		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
//		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
//		List<String> users = new ArrayList<String>();
//		try
//		{
//			Long numerwniosku = null;
//			Long numwerUmowy = null;
//			String region = null;
//
//			if (fm.getKey(NUMER_KONTRAHENTA_FIELD_CN) != null)
//			{
//				Contractor con = Contractor.findById((Long) fm.getKey(NUMER_KONTRAHENTA_FIELD_CN));
//				if (con.getRegion() != null)
//					region = con.getRegion().getCn();
//				if (con.getDsUser() != null)
//					users.add(con.getDsUser());
//			}
//			users.add(document.getAuthor());
//
//			DlPermissionManager.setupPermission(region, numerwniosku, numwerUmowy, perms, users,document.getAuthor());
//		}
//		catch (Exception e)
//		{
//			log.error(e.getMessage(), e);
//		}
//		this.setUpPermission(document, perms);
		throw new EdmException("STARA KLASA");
	}

	public boolean searchCheckPermissions(Map<String, Object> values)
	{
		return true;
	}

	public boolean isPersonalRightsAllowed()
	{
		return true;
	}

	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		String channel = document.getDocumentKind().getChannel(document.getId());
		if (channel != null)
		{
			List<ProcessCoordinator> chanels = ProcessCoordinator.find(DocumentLogicLoader.CRM_VINDICATION_KIND, channel);
			if (chanels != null && chanels.size() > 0)
				return chanels.get(0);
			else
				return null;
		}
		return null;
	}

	@Override
	public TaskListParams getTaskListParams(DocumentKind kind, long documentId) throws EdmException
	{
		return TaskListParams.fromMagicArray(getTaskListParam(kind, documentId));
	}

	private Object[] getTaskListParam(DocumentKind dockind, Long id) throws EdmException
	{
		Object[] result = new Object[10];
		FieldsManager fm = dockind.getFieldsManager(id);
		result[0] = fm.getValue(WYNIK_KONTAKTU_FIELD_CN);
		String role = "";
		if (fm.getKey(NUMER_KONTRAHENTA_FIELD_CN) != null)
		{
			Contractor contractor = Contractor.findById((Long)fm.getKey(NUMER_KONTRAHENTA_FIELD_CN));
			
			if (contractor.getRegion() != null)
			{
				result[4] = contractor.getRegion().getName();
			}
			result[5] = contractor.getName();

			if (contractor.getRole() != null)
			{
				for (Role r : contractor.getRole())
				{
					role += r.getName() + "|";
				}
			}
		}
		else
		{
			result[4] = "brak";
			result[5] = "brak";
		}
		result[2] = fm.getKey(NALEZNOSC_FIELD_CN);
		result[6] = role;

		return result;

	}

	public String getInOfficeDocumentKind()
	{
		return "Windykacja";
	}
}
