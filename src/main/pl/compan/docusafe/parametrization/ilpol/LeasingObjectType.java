package pl.compan.docusafe.parametrization.ilpol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * Rodzaj przedmiotu leasingu
 * @author Mariusz Kilja�czyk
 *
 */

public class LeasingObjectType
{
    private static final Logger log = LoggerFactory.getLogger(LeasingObjectType.class);
    
    private Long   id;
    private String name;
    private Long leoId;
    private static Map<Long,LeasingObjectType> typs;
    
    private static Map<Long,LeasingObjectType> getTypes()
    {
    	if(typs == null)
    		init();
    	return typs;
    }
    
    private static void init()
    {
    	typs = new HashMap<Long, LeasingObjectType>();
    	boolean open = true;
    	try
    	{
    		
    		if(!DSApi.isContextOpen())
    		{
    			open = false;
    			DSApi.openAdmin();
    		}
    		for (LeasingObjectType lot : LeasingObjectType.loadList())
    		{
    			typs.put(lot.getId(), lot);
			}
    	}
    	catch (Exception e)
    	{
			log.error("",e);
		}
    	finally
    	{
    		if(!open)
    		{
	    		try {
					DSApi.close();
				} catch (EdmException e) {
					log.error("",e);
				}
    		}
    	}
    }
    
    StringManager sm = 
        GlobalPreferences.loadPropertiesFile(LeasingObjectType.class.getPackage().getName(),null);
    
    public LeasingObjectType()
    {
    }
    
    public static void updateTypes(LeasingObjectType lot)
    {
    	getTypes().put(lot.getId(), lot);
    }
    
    /**
     * Zapisuje strone w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
            updateTypes(this);
        }
        catch (HibernateException e) 
        {
            log.error("Blad dodania typu "+e.getMessage());
            throw new EdmException("Blad dodania typu"+e.getMessage());
        }
    }

    public static LeasingObjectType find(Long id) throws EdmException
    {
    	return findById(id);
    }
    
    public static LeasingObjectType findByName(String name)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(LeasingObjectType.class);
            c.add(Restrictions.like("name",name).ignoreCase());
            List<LeasingObjectType> clist = (List<LeasingObjectType>) c.list();
            if(clist.size() > 0)
            	return clist.get(0);
            else
            	return null; 
        }
        catch (Exception e) 
		{
			log.debug("B��d wyszukania typu o nazwie "+ name +" " + e);
		}
        return null;
    }
   
    private static List<LeasingObjectType> loadList() throws EdmException
    {
        Criteria c = DSApi.context().session().createCriteria(LeasingObjectType.class);
        return (List<LeasingObjectType>) c.list();

    }
    
    public static List<LeasingObjectType> list() throws EdmException
    {
        return new ArrayList<LeasingObjectType>(getTypes().values());

    }
    
    public static LeasingObjectType findById(Long id)
    {
    	return getTypes().get(id);
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String nazwa)
    {
        this.name = nazwa;
    }

	public Long getLeoId() {
		return leoId;
	}

	public void setLeoId(Long leoId) {
		this.leoId = leoId;
	}
}