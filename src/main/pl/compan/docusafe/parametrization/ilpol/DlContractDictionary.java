package pl.compan.docusafe.parametrization.ilpol;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.dockinds.dictionary.AbstractDocumentKindDictionary;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.QueryForm.SortField;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.OrderClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

/**
 * S�ownik um�w dla dokument�w leasignowych.
 * 
 * @author Mariusz Kiljanczyk
 */
public class DlContractDictionary extends AbstractDocumentKindDictionary
{
	private static final Logger log = LoggerFactory.getLogger(DlContractDictionary.class);

	public static final Integer NOWY = 10;
	public static final Integer UZYWANY = 20;
	
	private Long id;
	private String numerUmowy;
	private Long idKlienta;
	private String opisUmowy;
	private Float wartoscUmowy;
	private Date dataUmowy;
	private Date dataKoncaUmowy;
	private String status;
	private Long applicationId;
	private Long idDostawcy;
	private Long leasingObjectType;
	private Integer stan;
	private static final List<String> statusList = new ArrayList<String>();
	public static Map<Integer,String> stanList = new HashMap<Integer,String>();
	private static DlContractDictionary instance;
	public static synchronized DlContractDictionary getInstance()
	{
		if (instance == null)
			instance = new DlContractDictionary();
		return instance;
	}
	StringManager sm = GlobalPreferences.loadPropertiesFile(DlContractDictionary.class.getPackage().getName(), null);
	static
	{
		statusList.add("Nowa");
		statusList.add("Aktywna");
		statusList.add("Zamknieta");
		stanList.put(NOWY, "Nowy");
		stanList.put(UZYWANY, "U�ywany");
	}
	
	public static Integer getStanIdByLeoCN(String cn)
	{
		if("N".equals(cn))
			return NOWY;
		else
			return UZYWANY;
	}

	public DlContractDictionary()
	{}

	public String dictionaryAction()
	{
		return "/office/common/dlContractDictionary.action";
	}

	/**
	 * Zwraca stringa zawieraj�cego kod javascript kt�ry wykona si� w funkcji
	 * accept_cn
	 * 
	 */
	public String dictionaryAccept()
	{
		return "accept_contract(map);";
	}

	public Map<String, String> dictionaryAttributes()
	{
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("numerUmowy", "");
		return map;
	}

	public String getDictionaryDescription()
	{
		return numerUmowy;
	}

	/**
	 * Zapisuje strone w sesji hibernate
	 * 
	 * @throws EdmException
	 */
	public void create() throws EdmException
	{
		try
		{
			if (numerUmowy != null && numerUmowy.length() > 0)
			{
				List list = findByNumerUmowy(numerUmowy);
				if (list != null && list.size() > 0)
					throw new EdmException("Umowa o numerze " + numerUmowy + " ju� istnieje");
			}
			DSApi.context().session().save(this);
			DSApi.context().session().flush();
		}
		catch (HibernateException e)
		{
			log.error("Blad dodania umowy dokumentu leasingowego. " + e.getMessage());
			throw new EdmException(sm.getString("BladDodaniaUmowyDokumentuLeasingowego") + e.getMessage());
		}
	}

	public DlContractDictionary find(Long id) throws EdmException
	{
		StringManager smL = GlobalPreferences.loadPropertiesFile(DlContractDictionary.class.getPackage().getName(), null);
		DlContractDictionary inst = DSApi.getObject(DlContractDictionary.class, id);
		if (inst == null)
			throw new EdmException(smL.getString("NieZnalezionoUmowyOnr", id));
		else
			return inst;
	}

	public static List<DlContractDictionary> findByNumerUmowy(String numerUmowy) throws EdmException
	{
		Criteria c = DSApi.context().session().createCriteria(DlContractDictionary.class);
		c.add(Restrictions.eq("numerUmowy", numerUmowy).ignoreCase());
		return (List<DlContractDictionary>) c.list();
	}

	public static List<DlContractDictionary> findByContractor(Long idKlienta) throws EdmException
	{
		Criteria c = DSApi.context().session().createCriteria(DlContractDictionary.class);
		c.add(Restrictions.eq("idKlienta", idKlienta));
		return (List<DlContractDictionary>) c.list();
	}

	public void delete() throws EdmException
	{
		try
		{
			DSApi.context().session().delete(this);
			DSApi.context().session().flush();
		}
		catch (HibernateException e)
		{
			log.error("Blad usuniecia umowy dokumentu leasingowego");
			throw new EdmException(sm.getString("BladUsunieciaUmowyDokumentuLeasingowego") + e.getMessage());
		}

	}

	public static class DlContractorComparatorByNumerUmowy implements Comparator<DlContractDictionary>
	{
		public int compare(DlContractDictionary e1, DlContractDictionary e2)
		{
			return e1.getNumerUmowy().compareTo(e2.getNumerUmowy());
		}
	}

	@SuppressWarnings("unchecked")
	public static SearchResults<? extends DlContractDictionary> search(QueryForm form)
	{
		try
		{
			FromClause from = new FromClause();
            final TableAlias table = from.createTable("DL_CONTRACT_DICTIONARY");
            
            final WhereClause where = new WhereClause();

			if (form.hasProperty("numerUmowy"))
			{
				where.add(Expression.like(table.attribute("numerUmowy"),"%"+(String)form.getProperty("numerWniosku")+"%",true));
			}
			if (form.hasProperty("idKlienta"))
			{
				where.add(Expression.eq(table.attribute("idKlienta"),form.getProperty("idKlienta")));
			}
			if (form.hasProperty("idWniosku"))
			{
				where.add(Expression.eq(table.attribute("applicationId"),form.getProperty("applicationId")));
			}
			if (form.hasProperty("idDostawcy"))
			{
				where.add(Expression.eq(table.attribute("idDostawcy"),form.getProperty("idDostawcy")));
			}
			if (form.hasProperty("opisUmowy"))
			{
				where.add(Expression.like(table.attribute("opisUmowy"),"%"+(String)form.getProperty("opisUmowy")+"%",true));
			}
			if (form.hasProperty("wartoscUmowy"))
			{
				where.add(Expression.eq(table.attribute("wartoscUmowy"),form.getProperty("wartoscUmowy")));
			}
			if (form.hasProperty("dataUmowy"))
			{
				where.add(Expression.eq(table.attribute("dataUmowy"),form.getProperty("dataUmowy")));
			}
			if (form.hasProperty("dataKoncaUmowy"))
			{
				where.add(Expression.eq(table.attribute("dataKoncaUmowy"),form.getProperty("dataKoncaUmowy")));
			}
			if (form.hasProperty("status"))
			{
				where.add(Expression.eq(table.attribute("status"),form.getProperty("status")));
			}
			if (form.hasProperty("stan"))
			{
				where.add(Expression.eq(table.attribute("stan"),form.getProperty("stan")));
			}
			  //Uprawnienia 
            if(!DSApi.context().hasPermission(DSPermission.CONTRACTOR_ALL))
            {
            	if(!DSApi.context().hasPermission(DSPermission.CONTRACTOR_REGION))
            	{
            		if(!DSApi.context().hasPermission(DSPermission.CONTRACTOR_USER))
            		{
            			return null;
            		}
            		else
            		{
            			final TableAlias conTable = from.createTable("dsc_contractor");
            			where.add(Expression.eqAttribute(table.attribute("idKlienta"), conTable.attribute("id")));
            			final TableAlias tabRef = from.createTable("dsc_patron");
                    	where.add(Expression.eqAttribute(tabRef.attribute("CONTRACTORID"), conTable.attribute("ID")));
                    	where.add(Expression.eq(tabRef.attribute("USERNAME"), DSApi.context().getPrincipalName())); 
            		}
            	}
            	else
            	{
            		List<String> users = new ArrayList<String>();
            		for (DSDivision division : DSApi.context().getDSUser().getDivisions()) 
                    {
            			for (DSUser user : division.getUsers())
            			{
            				users.add(user.getName());
						}
            		}
            		
            		final TableAlias conTable = from.createTable("dsc_contractor");
        			where.add(Expression.eqAttribute(table.attribute("idKlienta"), conTable.attribute("id")));
        			final TableAlias tabRef = from.createTable("dsc_patron");
                	where.add(Expression.eqAttribute(tabRef.attribute("CONTRACTORID"), conTable.attribute("ID")));
                	where.add(Expression.in(tabRef.attribute("USERNAME"), users.toArray()));  
//            		log.error("MA "+DSPermission.CONTRACTOR_REGION);
//            		final TableAlias contractorTable = from.createJoinedTable("dsc_contractor", true, table, FromClause.JOIN,"$l.idKlienta = $r.id" );
//                	final TableAlias regionTable = from.createJoinedTable("dsc_region", true, contractorTable, FromClause.LEFT_OUTER,"$l.region = $r.id" );
//                	
//                	Junction OR = pl.compan.docusafe.util.querybuilder.expression.Expression.disjunction();
//                	for (DSDivision division : DSApi.context().getDSUser().getDivisions()) 
//                	{
//                		//grupy uprawnie� s� tworzone na podtswie cn regionu (DV_+CN_REGIONU) i dlatego mozemy to tutaj tak upro�ci� 
//                		//dodanie niepotrzebnych grup nie zaszkodzi
//                		OR.add(Expression.eq(regionTable.attribute("CN"),division.getGuid().substring(3)));
//    				}         
//        			OR.add(Expression.eq(contractorTable.attribute("dsuser"),DSApi.context().getPrincipalName()));
//                    where.add(OR);
            	}
            }
			
            SelectClause selectId = new SelectClause(true);
            SelectColumn idCol = selectId.add(table, "id");
            OrderClause order = new OrderClause();
            List<SortField> l = form.getSortFields();
            for (SortField sortField : l) 
            {
            	order.add(table.attribute(sortField.getName()), sortField.isAscending());
            	selectId.add(table, sortField.getName());
			}
            
            SelectClause selectCount = new SelectClause();
            SelectColumn countCol = selectCount.addSql("count(distinct "+table.getAlias()+".id)");
            SelectQuery selectQuery;
            

            selectQuery = new SelectQuery(selectCount, from, where, null);
            ResultSet rs = selectQuery.resultSet();
            if (!rs.next())
                throw new EdmException("Nie mo�na pobra� liczby zada�.");
            int totalCount = rs.getInt(countCol.getPosition());
            rs.close();
            DSApi.context().closeStatement(rs.getStatement());
            
            selectQuery = new SelectQuery(selectId, from, where, order);
            if (form.getLimit() > 0)
            {
                selectQuery.setMaxResults(form.getLimit());
                selectQuery.setFirstResult(form.getOffset());
            }
            rs = selectQuery.resultSet(DSApi.context().session().connection());
            List<DlContractDictionary> results = new ArrayList<DlContractDictionary>(form.getLimit());
            DlContractDictionary temp;
            while (rs.next())
            {
                Long id = new Long(rs.getLong(idCol.getPosition()));
                temp = DSApi.context().load(DlContractDictionary.class, id);
                results.add(temp);
            }
            rs.close();
            DSApi.context().closeStatement(rs.getStatement());

            return new SearchResultsAdapter<DlContractDictionary>(results, form.getOffset(), totalCount,
            		DlContractDictionary.class);
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		return SearchResultsAdapter.emptyResults(DlContractDictionary.class);
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Date getDataKoncaUmowy()
	{
		return dataKoncaUmowy;
	}

	public void setDataKoncaUmowy(Date dataKoncaUmowy)
	{
		this.dataKoncaUmowy = dataKoncaUmowy;
	}

	public Date getDataUmowy()
	{
		return dataUmowy;
	}

	public void setDataUmowy(Date dataUmowy)
	{
		this.dataUmowy = dataUmowy;
	}

	public Long getIdKlienta()
	{
		return idKlienta;
	}

	public void setIdKlienta(Long idKlienta)
	{
		this.idKlienta = idKlienta;
	}

	public String getNumerUmowy()
	{
		return numerUmowy;
	}

	public void setNumerUmowy(String numerUmowy)
	{
		this.numerUmowy = numerUmowy;
	}

	public String getOpisUmowy()
	{
		return opisUmowy;
	}

	public void setOpisUmowy(String opisUmowy)
	{
		this.opisUmowy = opisUmowy;
	}

	public Float getWartoscUmowy()
	{
		return wartoscUmowy;
	}

	public void setWartoscUmowy(Float wartoscUmowy)
	{
		this.wartoscUmowy = wartoscUmowy;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public static List<String> getStatusList()
	{
		return statusList;
	}

	public void setIdDostawcy(Long idDostawcy)
	{
		this.idDostawcy = idDostawcy;
	}

	public Long getIdDostawcy()
	{
		return idDostawcy;
	}

	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	public Long getLeasingObjectType() {
		return leasingObjectType;
	}

	public void setLeasingObjectType(Long leasingObjectType) {
		this.leasingObjectType = leasingObjectType;
	}

	@Override
	public Map<String, String> popUpDictionaryAttributes() {
		// TODO Auto-generated method stub
		return null;
	}

	public Integer getStan() {
		return stan;
	}

	public void setStan(Integer stan) {
		this.stan = stan;
	}
}
