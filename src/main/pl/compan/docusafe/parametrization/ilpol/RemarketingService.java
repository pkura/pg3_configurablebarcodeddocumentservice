package pl.compan.docusafe.parametrization.ilpol;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.imports.ImpulsImportUtils;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 *
 */
public class RemarketingService extends ServiceDriver implements Service
{
	public static final Logger log = LoggerFactory.getLogger(RemarketingService.class);
	
	public static final String ID_UMOWY = "ID_UMOWY";
	public static final String NR_UMOWY = "NR_UMOWY";
	public static final String ID_KLIENTA = "ID_KLIENTA";
	public static final String SYMBOL_KLIENTA = "SYMBOL_KLIENTA";
	public static final String NAZWA_KLIENTA = "NAZWA_KLIENTA";	
	public static final String ADRES_KLIENTA = "ADRES_KLIENTA";
	public static final String POCZTA = "POCZTA";
	public static final String NIP = "NIP";
	public static final String DATA_ZAKUMOWY = "DATA_ZAKUMOWY";
	public static final String OPIEKUN = "OPIEKUN";
	public static final String CZY_HANDLOWIEC_AKTYWNY = "CZY_HANDLOWIEC_AKTYWNY";
	public static final String LASTNAME = "LASTNAME";
	public static final String FIRSTNAME = "FIRSTNAME";
	public static final String PODPISUJACY = "PODPISUJACY";
	public static final String PODPISUJACY_NAZWISKO = "PODPISUJACY_NAZWISKO";
	public static final String UWAG = "UWAGI";
	
	private Timer timer;
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(RemarketingService.class.getPackage().getName(),null);
	private Property[] properties;
	private Integer period = 5;
	private Date lastDate = DateUtils.parseJsDate("30-10-2060");
	
    class MailTimesProperty extends Property {
        public MailTimesProperty(){
            super(SIMPLE, PERSISTENT, RemarketingService.this, "period", "Co ile minut", Integer.class);
        }
        protected Object getValueSpi(){
            return period;
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (RemarketingService.this){
            	if(object != null)
            		period = (Integer) object;
            	else
            		period = 10;
            }
        }
    }
    
    class LastDateProperty extends Property {
        public LastDateProperty(){
            super(SIMPLE, PERSISTENT, RemarketingService.this, "lastDate", "Zaczynaj od daty", String.class,DATE);
        }
        protected Object getValueSpi(){
            return DateUtils.formatJsDate(lastDate);
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (RemarketingService.this){
            	if(object != null)
					try {
						lastDate = DateUtils.parseDateAnyFormat(""+object);
					} catch (ParseException e) {
						
					}
				else
            		lastDate = new Date();
            }
        }
    }

	public RemarketingService()
	{
		properties = new Property[] { new MailTimesProperty(),new LastDateProperty()};
	}

	protected boolean canStop()
	{
		return false;
	}

	protected void start() throws ServiceException
	{
		log.info("start uslugi RemarketingService");
		timer = new Timer(true);
		timer.schedule(new Import(), 0 ,period*DateUtils.MINUTE);
		console(Console.INFO, sm.getString("SystemWystartowal"));
	}

	protected void stop() throws ServiceException
	{
		log.info("stop uslugi RemarketingService");
		try
		{
			if(lastDate != null)
			{
				DSApi.context().begin();
				getProperties()[1].setValue(DateUtils.formatJsDate(lastDate));
				console(Console.INFO, "Zapisa� lastDate = "+DateUtils.formatJsDate(lastDate));
				DSApi.context().commit();
			}
			DSApi.close();
		}catch (Exception e) {log.error(e.getMessage(),e);}
		console(Console.INFO, sm.getString("StopUslugi"));
		if(timer != null) timer.cancel();
	}

	public boolean hasConsole()
	{
		return true;
	}	
	
	class Import extends TimerTask
	{
		@Override
		public void run() 
		{
			console(Console.INFO, "Start od daty " + DateUtils.formatJsDate(lastDate));
			Connection con = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			Integer i = 0;
			Long idUmowy;
			try
			{
				DSApi.openAdmin();
				DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.CRM_MARKETING_TASK_KIND);
				console(Console.INFO, "��czy z LEO");
				con = ImpulsImportUtils.connectToLEO();
				ps = con.prepareStatement("select * from "+ImpulsImportUtils.VWYGASAJACEUL_CRM+" where DATA_ZAKUMOWY > ? order by DATA_ZAKUMOWY");
				ps.setTimestamp(1, new Timestamp(lastDate.getTime()));
				console(Console.INFO, "Szuka dla ID > "+lastDate);
				log.debug("SZUKA DLA "+ lastDate);
				rs = ps.executeQuery();
				while(rs.next())
				{
					Map<String,Object> values = new TreeMap<String, Object>();
					try
					{
						DSUser user = DSUser.findByUsername(rs.getString(OPIEKUN));
						ServicesUtils.setRemarketingValues(rs, values,documentKind,user.getId());
						String uwagi = rs.getString(ServicesUtils.UWAGI);
						idUmowy = rs.getLong(ID_UMOWY);
						lastDate = rs.getDate(DATA_ZAKUMOWY);
						console(Console.INFO, "DATA ZAKU" + DateUtils.formatJsDate(lastDate));
						try
						{
							DSApi.context().begin();
							InOfficeDocument doc = new InOfficeDocument();
							ServicesUtils.createTask("Remarketing", values, user,uwagi,doc,documentKind,"Remarketing");
							DSApi.context().commit();
							console(Console.INFO, "Doda� dokument o ID "+doc.getId()+" przekaz�a do "+user.asFirstnameLastname());
						}
						catch (Exception e) 
						{
							log.error("B��D importu Remarketing nr= "+idUmowy+""+e.getMessage(),e);
							console(Console.ERROR, "B��D importu Remarketing nr= "+idUmowy+""+e.getMessage());
							DSApi.context().rollback();
							/**Nie wiem co sie pieprzy */
//							DSApi.context().session().flush();
//							DSApi.context().session().clear();
							DSApi.close();
							DSApi.openAdmin(); 
						}
		                i++;
						if(i%100 == 99)
						{
							log.debug("Czyszczenie sesi po "+i+" dokumentach");
							console(Console.INFO, "Czyszczenie sesi po "+i+" dokumentach");
							DSApi.context().session().flush();
							DSApi.context().session().clear();
							DSApi.close();
							DSApi.openAdmin();
						}
					}
					catch (Exception e) {
						log.error(e.getMessage(),e);
						for (String key : values.keySet()) 
						{
							log.error("=========================="+values.get(key));
						}
						console(Console.ERROR, "B��D importu wniosku "+e.getMessage());
					}
				}
				console(Console.INFO,"Zakonczyl dodawanie "+i+" zada�");
			}
			catch (Exception e) 
			{
				log.error(e.getMessage(),e);
				console(Console.ERROR, "B��D: "+e.getMessage());
			}
			finally
			{
				try
				{
					if(rs != null)
						rs.close();
					if(ps != null)
						ps.close();
					if(lastDate != null)
					{
						DSApi.context().begin();
						getProperties()[1].setValue(DateUtils.formatJsDate(lastDate));
						console(Console.INFO, "Zapisa� lastDate = "+DateUtils.formatJsDate(lastDate));
						DSApi.context().commit();
					}
					DSApi.close();
				}catch (Exception e) {log.error(e.getMessage(),e);}
				ImpulsImportUtils.disconnectOracle(con);
			}
		}
	}
	
	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties) 
	{
		this.properties = properties;
	}
}
