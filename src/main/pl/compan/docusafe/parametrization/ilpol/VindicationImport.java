package pl.compan.docusafe.parametrization.ilpol;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.imports.ImpulsImportUtils;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

/**
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 *
 */
public class VindicationImport extends ServiceDriver implements Service
{
	public static final Logger log = LoggerFactory.getLogger("VINDICATIONIMPORT");
	
	private Timer timer;
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(VindicationImport.class.getPackage().getName(),null);
	private Property[] properties;
	private Integer period = 5;
	private Long startId = Long.MAX_VALUE;
	
    class MailTimesProperty extends Property {
        public MailTimesProperty(){
            super(SIMPLE, PERSISTENT, VindicationImport.this, "period", "Co ile minut", Integer.class);
        }
        protected Object getValueSpi(){
            return period;
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (VindicationImport.this){
            	if(object != null)
            		period = (Integer) object;
            	else
            		period = 10;
            }
        }
    }
    
    class StartIdProperty extends Property {
        public StartIdProperty(){
            super(SIMPLE, PERSISTENT, VindicationImport.this, "startId", "Zaczynaj od ID", Long.class);
        }
        protected Object getValueSpi(){
            return startId;
        } 
        protected void setValueSpi(Object object) throws ServiceException{
            synchronized (VindicationImport.this){
            	if(object != null)
            		startId = (Long) object;
            	else
            		startId = new Long(Long.MAX_VALUE);
            }
        }
    }

	public VindicationImport()
	{
		properties = new Property[] { new MailTimesProperty(),new StartIdProperty()};
	}

	protected boolean canStop()
	{
		return false;
	}

	protected void start() throws ServiceException
	{
		log.info("start uslugi VindicationImport");
		timer = new Timer(true);
		timer.schedule(new Import(), 0 ,period*DateUtils.MINUTE);
		console(Console.INFO, sm.getString("SystemWystartowal"));
	}

	protected void stop() throws ServiceException
	{
		log.info("stop uslugi VindicationImport");
		console(Console.INFO, sm.getString("StopUslugi"));
		if(timer != null) timer.cancel();
	}

	public boolean hasConsole()
	{
		return true;
	}	
	
	class Import extends TimerTask
	{
		@Override
		public void run() 
		{
			console(Console.INFO, "Start");
			Connection con = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			Integer i = 0;
			Long lastId = null;
			try
			{
				DSApi.openAdmin();
				DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.CRM_VINDICATION_KIND);
				lastId = startId;//DSApi.context().systemPreferences().node("VindicationImport").getLong("lastId",startId);
				console(Console.INFO, "��czy z LEO");
				con = ImpulsImportUtils.connectToLEO();
				ps = con.prepareStatement("select ID_WEZWANIA,NR_WEZWANIA,ID_KLIENTA,SYMBOL_KLIENTA,NAZWA_KLIENTA,ADRES_KLIENTA,POCZTA," +
						"NIP,DATA_WEZWANIA,NALEZNOSC,NALEZNOSC_PIERW,OPIEKUN,CZY_HANDLOWIEC_AKTYWNY,LASTNAME,FIRSTNAME,UWAGI from "+ImpulsImportUtils.vwezwania_crm +" where ID_WEZWANIA > ? order by ID_WEZWANIA");
				ps.setLong(1, lastId);
				console(Console.INFO, "Szuka dla ID > "+lastId);
				log.debug("SZUKA DLA "+ lastId);
				rs = ps.executeQuery();
				while(rs.next())
				{
					Map<String,Object> values = new TreeMap<String, Object>();
					try
					{
						log.debug("TIME1 :"+ new Date().getTime());
						lastId = rs.getLong(ServicesUtils.ID_WEZWANIA);						
						log.debug("TIME2 :"+ new Date().getTime());
						DSUser user = DSUser.findByUsername(rs.getString(ServicesUtils.OPIEKUN));
						log.debug("TIME3 :"+ new Date().getTime());
						ServicesUtils.setVindicationValues(rs, values);
						log.debug("TIME4 :"+ new Date().getTime());
						String uwagi = rs.getString(ServicesUtils.UWAGI);
						String numerWezwania = rs.getString(ServicesUtils.NR_WEZWANIA);
						try
						{
							log.debug("TIME5 :"+ new Date().getTime());
							DSApi.context().begin();
							InOfficeDocument doc = new InOfficeDocument();
							ServicesUtils.createTask(numerWezwania, values, user,uwagi,doc,documentKind,"Windykacja");
							DSApi.context().commit();
							log.debug("TIME11:"+ new Date().getTime());
							console(Console.INFO,"Doda� wezwanie "+numerWezwania+" dla :"+user.asFirstnameLastname());
						}
						catch (Exception e) 
						{
							log.error("B��D importu wezwania nr= "+numerWezwania+""+e.getMessage(),e);
							console(Console.ERROR, "B��D importu wezwania nr= "+numerWezwania+""+e.getMessage());
							DSApi.context().rollback();
							/**Nie wiem co sie pieprzy */
							DSApi.context().session().flush();
							DSApi.context().session().clear();
							DSApi.close();
							DSApi.openAdmin(); 
						}
		                i++;
						if(i%100 == 99)
						{
							LoggerFactory.getLogger("mariusz").debug("Czyszczenie sesi po "+i+" dokumentach");
							DSApi.context().session().flush();
							DSApi.context().session().clear();
							DSApi.close();
							DSApi.openAdmin();
						}
					}
					catch (Exception e) {
						log.error(e.getMessage(),e);
						for (String key : values.keySet()) 
						{
							log.error("=========================="+values.get(key));
						}
						console(Console.ERROR, "B��D importu wezwania "+e.getMessage());
					}
				}
				console(Console.INFO,"Zakonczyl dodawanie "+i+" zada�");
			}
			catch (Exception e) 
			{
				log.error(e.getMessage(),e);
				console(Console.ERROR, "B��D: "+e.getMessage());
			}
			finally
			{
				try
				{
					if(rs != null)
						rs.close();
					if(ps != null)
						ps.close();
					if(lastId > 0)
					{
						DSApi.context().begin();
						getProperties()[1].setValue(lastId);
						console(Console.INFO, "Zapisa� lastId = "+lastId);
						//DSApi.context().systemPreferences().node("VindicationImport").putLong("lastId",lastId);
						DSApi.context().commit();
					}
					DSApi.close();
				}catch (Exception e) {}
				ImpulsImportUtils.disconnectOracle(con);
			}
		}
	}
	
	public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties) 
	{
		this.properties = properties;
	}
}
