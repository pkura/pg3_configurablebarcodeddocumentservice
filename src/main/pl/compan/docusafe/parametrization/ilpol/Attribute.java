package pl.compan.docusafe.parametrization.ilpol;

public class Attribute {
	
	private String name;
	private String value;
	private Integer type;
	private String cn;
	
//	public Attribute(String name, String value, Integer type, String cn) {
//		this.name = name;
//		this.value = value;
//		this.type = type;
//		this.cn = cn;
//	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

}
