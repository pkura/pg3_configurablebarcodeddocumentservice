package pl.compan.docusafe.parametrization.ilpol.reports;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.crm.ContactStatus;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.CsvDumper;
import pl.compan.docusafe.reports.tools.SimpleSQLHelper;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.DateUtils;

public class CRMDoneContactsReport extends Report
{

	String sql = "select count(1) from DSG_LEASING_MULTIPLE_VALUE ref, DSC_CONTACT kontakt, DSC_MARKETING_task zadanie"+
	        " where ref.FIELD_CN = 'KONTAKT' and ref.FIELD_VAL = kontakt.id and ref.DOCUMENT_ID = zadanie.DOCUMENT_ID"+
	        " and kontakt.STATUS = ? and kontakt.CREATEUSER = ? and kontakt.DATAKONTAKTU >= ?"+
	                    " and kontakt.DATAKONTAKTU < ? ";


	public void doReport() throws Exception
	{
		Date dateto = null;
		Date datefrom = null;
		Set<String> users = new LinkedHashSet<String>();
		Boolean activeUser = false;
		List<ContactStatus>  opts = ContactStatus.statusKindList();
		
		String grupaKategoriaSql = null;
		List<Integer> grupy = new ArrayList<Integer>();
		List<Integer> kategorie = new ArrayList<Integer>();
		for(ReportParameter p: params)
		{
			String[] vals = new String[0];
			if(p.getValue() instanceof String)
			{
				vals = new String[1];
				vals[0] = p.getValueAsString();
			}
			else if(p.getValue() instanceof String[])
			{
				vals = (String[])p.getValue();
			}

			if("user".equals(p.getFieldCn()))
			{
				Collections.addAll(users, vals);
			}

			if("region".equals(p.getFieldCn()))
			{
				for(String s:vals)
				{
					for(DSUser u : DSDivision.find(s).getUsers(true))
					{ 
						users.add(u.getName());
					}
				}
			}
			if("GRUPA".equals(p.getFieldCn()))
			{
				
				for(String s:vals)
				{
					grupy.add(Integer.parseInt(s));
				}
			}
			else if("KATEGORIA".equals(p.getFieldCn()))
			{
				for(String s:vals)
				{
					kategorie.add(Integer.parseInt(s));
				}
			}
			if("dateTo".equals(p.getFieldCn()))
			{
				dateto = DateUtils.parseJsDate(vals[0]);
			}
			if("dateFrom".equals(p.getFieldCn()))
			{
				datefrom = DateUtils.parseJsDate(vals[0]);
			}
			if("activeUser".equals(p.getFieldCn()) && vals != null && vals.length > 0)
			{
				activeUser = true;
			}
		}

		if(grupy.size() > 0 || kategorie.size() > 0)
		{
			StringBuilder sb = new StringBuilder();
			if(grupy.size() > 0)
			{	
				sb.append(" and ");
				sb.append("zadanie.grupa in ( ");
				for (Integer integer : grupy)
				{
					sb.append(" "+integer+",");
				}
				sb.delete(sb.length()-1, sb.length());
				sb.append(" ) ");
			}
			if(kategorie.size() > 0)
			{
				sb.append(" and ");
				sb.append("zadanie.kategoria in ( ");
				for (Integer integer : kategorie)
				{
					sb.append(" "+integer+",");
				}
				sb.delete(sb.length()-1, sb.length());
				sb.append(" ) ");
			}
			grupaKategoriaSql = sb.toString();
		}

		UniversalTableDumper dumper = new UniversalTableDumper();

		CsvDumper csvDumper = new CsvDumper();
		File csvFile = new File(this.getDestination(), "report.csv");
		csvDumper.openFile(csvFile);
		dumper.addDumper(csvDumper);

		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "report.xls");
		poiDumper.openFile(xlsFile);
		dumper.addDumper(poiDumper);

		dumper.newLine();
		dumper.addText("Numer");
		if(!activeUser)
			dumper.addText("Pracuje");
		dumper.addText("Nazwisko");
		dumper.addText("Imi�");
		dumper.addText("Region");
		dumper.addText("Data od");
		dumper.addText("Data do");
		dumper.addText("Wszystkie");
		dumper.addText("Wizyty");
		dumper.addText("Liczba przeterminowanych zada� na dzie� "+(DateUtils.formatJsDate(new Date())));
		dumper.addText("Ilo�� dodanych rekord�w");

		for(ContactStatus ei : opts)
		{
			dumper.addText(ei.getName());
		}
		
		dumper.dumpLine();
		SimpleSQLHelper sqlHelper = new SimpleSQLHelper();

		int i = 1;
		PreparedStatement ps = null;
		ResultSet rs = null;
		for(String user:users)
		{
			try
			{
				int contactCount = 0;
				DSUser u = DSUser.findByUsername(user);
				if((u.isDeleted() || u.isLoginDisabled()) && activeUser)
					continue;
				dumper.newLine();
				dumper.addText(i+"");
				if(!activeUser)
					dumper.addBoolean(!(u.isDeleted() || u.isLoginDisabled()));
				dumper.addText(u.getLastname());
				dumper.addText(u.getFirstname());
				boolean added = false;
				for(DSDivision div:u.getDivisions())
				{
					if(div.getGuid().startsWith("DV_"))
					{
						dumper.addText(div.getName());
						added=true;
						break;
					}
				}
				if(!added)
					dumper.addText("Brak");
				dumper.addDate(datefrom);
				dumper.addDate(dateto);
				Date datefromafter1501 = new Date(datefrom.getTime());

				if(!datefrom.after(DateUtils.parseJsDate("15-01-2009")))
				{
					ps = DSApi.context().prepareStatement("select count(1)"+
							" from DSG_LEASING_MULTIPLE_VALUE ref, DSC_CONTACT kontakt, DSC_MARKETING_task zadanie, ds_user usr "+
							" where ref.FIELD_CN = 'KONTAKT' and ref.FIELD_VAL = kontakt.id and ref.DOCUMENT_ID = zadanie.DOCUMENT_ID and kontakt.STATUS > 0 "+
							" and usr.id = zadanie.uzytkownik and kontakt.datakontaktu < ? "+
							" and usr.name = ? and kontakt.datakontaktu >= ? "+(grupaKategoriaSql != null ? grupaKategoriaSql: ""));
					if(dateto.before(DateUtils.parseJsDate("15-01-2009")))
					{
						ps.setDate(1, new java.sql.Date(dateto.getTime()));
					}
					else
					{
						ps.setDate(1, new java.sql.Date(DateUtils.parseJsDate("15-01-2009").getTime()));
					}
					ps.setString(2, user);
					ps.setDate(3, new java.sql.Date(datefrom.getTime()));

					rs = ps.executeQuery();
					rs.next();
					contactCount += rs.getInt(1);
					ps.close();
					datefromafter1501 = DateUtils.parseJsDate("15-01-2009");
				}

				ps = DSApi.context().prepareStatement("select count(1) from DSG_LEASING_MULTIPLE_VALUE ref, DSC_CONTACT kontakt, DSC_MARKETING_task zadanie"+
				        " where ref.FIELD_CN = 'KONTAKT' and ref.FIELD_VAL = kontakt.id and ref.DOCUMENT_ID = zadanie.DOCUMENT_ID and kontakt.STATUS > 0 "+
				        " and kontakt.CREATEUSER = ? and kontakt.DATAKONTAKTU >= ? "+
				                    " and kontakt.DATAKONTAKTU < ? "+(grupaKategoriaSql != null ? grupaKategoriaSql: ""));

				ps.setString(1, user);
				ps.setDate(2, new java.sql.Date(datefromafter1501.getTime()));
				ps.setDate(3, sqlHelper.addDayDate(dateto, 1));

				rs = ps.executeQuery();
				rs.next();
				contactCount += rs.getInt(1);
				dumper.addText(contactCount+"");
				contactCount = 0;
				DSApi.context().closeStatement(ps);
				
				/**--Wizyty START--*/
				datefromafter1501 = new Date(datefrom.getTime());
				contactCount = 0;

				if(!datefrom.after(DateUtils.parseJsDate("15-01-2009")))
				{
					ps = DSApi.context().prepareStatement("select count(1)"+
							" from DSG_LEASING_MULTIPLE_VALUE ref, DSC_CONTACT kontakt, DSC_MARKETING_task zadanie, ds_user usr "+
							" where ref.FIELD_CN = 'KONTAKT' and ref.FIELD_VAL = kontakt.id and kontakt.rodzajkontaktu = 1  and ref.DOCUMENT_ID = zadanie.DOCUMENT_ID and kontakt.STATUS > 0 "+
							" and usr.id = zadanie.uzytkownik and kontakt.datakontaktu < ? "+
							" and usr.name = ? and kontakt.datakontaktu >= ? "+(grupaKategoriaSql != null ? grupaKategoriaSql: ""));
					if(dateto.before(DateUtils.parseJsDate("15-01-2009")))
					{
						ps.setDate(1, new java.sql.Date(dateto.getTime()));
					}
					else
					{
						ps.setDate(1, new java.sql.Date(DateUtils.parseJsDate("15-01-2009").getTime()));
					}
					ps.setString(2, user);
					ps.setDate(3, new java.sql.Date(datefrom.getTime()));

					rs = ps.executeQuery();
					rs.next();
					contactCount += rs.getInt(1);
					ps.close();
					datefromafter1501 = DateUtils.parseJsDate("15-01-2009");
				}

				ps = DSApi.context().prepareStatement("select count(1) from DSG_LEASING_MULTIPLE_VALUE ref, DSC_CONTACT kontakt, DSC_MARKETING_task zadanie"+
				        " where ref.FIELD_CN = 'KONTAKT' and ref.FIELD_VAL = kontakt.id and ref.DOCUMENT_ID = zadanie.DOCUMENT_ID and kontakt.rodzajkontaktu = 1 and kontakt.STATUS > 0 "+
				        " and kontakt.CREATEUSER = ? and kontakt.DATAKONTAKTU >= ? "+
				                    " and kontakt.DATAKONTAKTU < ? "+(grupaKategoriaSql != null ? grupaKategoriaSql: ""));

				ps.setString(1, user);
				ps.setDate(2, new java.sql.Date(datefromafter1501.getTime()));
				ps.setDate(3, sqlHelper.addDayDate(dateto, 1));

				rs = ps.executeQuery();
				rs.next();
				contactCount += rs.getInt(1);
				dumper.addText(contactCount+"");
				contactCount = 0;
				DSApi.context().closeStatement(ps);
				
				
				/**--Wizyty KONIEC--*/
				/**--Przetrminowane--*/
				ps = DSApi.context().prepareStatement("select count(1) from dsw_tasklist task, dso_document_to_label label where " +
						"task.dsw_resource_res_key = ? and task.dso_document_id = label.document_id and label.label_id = 1 and dockindstatus <> 'Pr�ba Nawi�zania Kontaktu' and dockindstatus <> 'Nikt Nie Odbiera' and dockindstatus <> 'Firma nie istnieje'");

				ps.setString(1, user);
				rs = ps.executeQuery();
				rs.next();
				contactCount = rs.getInt(1);
				dumper.addText(contactCount+"");
				contactCount = 0;
				DSApi.context().closeStatement(ps);
				
				/**--Przetrminowane koniec*/
				
				/**--DODANE W OKRESIE--*/
				ps.close();
				ps = DSApi.context().prepareStatement("select count(1) from dsc_marketing_task zadanie, ds_document doc  where " +
						"doc.id = zadanie.document_id and zadanie.uzytkownik = ? and doc.ctime > ? and doc.ctime < ? "+(grupaKategoriaSql != null ? grupaKategoriaSql: ""));

				ps.setInt(1, DSUser.findByUsername(user).getId().intValue());
				ps.setTimestamp(2, new java.sql.Timestamp(datefrom.getTime()));
				ps.setTimestamp(3, new java.sql.Timestamp(sqlHelper.addDayDate(dateto, 1).getTime()));
				log.info(DSUser.findByUsername(user).getId().intValue()+"");
				log.info(DateUtils.formatCommonDateTime(datefrom));
				log.info(DateUtils.formatCommonDateTime(sqlHelper.addDayDate(dateto, 1)));
				log.info("select count(1) from dsc_marketing_task zadanie, ds_document doc  where " +
						"doc.id = zadanie.document_id and zadanie.uzytkownik = ? and doc.ctime > ? and doc.ctime < ? "+(grupaKategoriaSql != null ? grupaKategoriaSql: ""));

				rs = ps.executeQuery();
				rs.next();
				int dwo =  rs.getInt(1);
				log.info(dwo+" ilosc ");
				dumper.addText(dwo+"");
				contactCount = 0;
				DSApi.context().closeStatement(ps);
				
				/**--DODANE W OKRESIE koniec*/
				
				for(ContactStatus ei : opts)
				{
					if(!datefrom.after(DateUtils.parseJsDate("15-01-2009")))
					{
						ps = DSApi.context().prepareStatement("select count(1)"+
								"from DSG_LEASING_MULTIPLE_VALUE ref, DSC_CONTACT kontakt, "+
								"DSC_MARKETING_task zadanie, ds_user usr "+
								"where ref.FIELD_CN = 'KONTAKT' and ref.FIELD_VAL = kontakt.id and ref.DOCUMENT_ID = zadanie.DOCUMENT_ID "+
								"and usr.id = zadanie.uzytkownik and kontakt.datakontaktu < ? "+
								"and usr.name = ? and kontakt.datakontaktu >= ? and kontakt.STATUS = ? "+(grupaKategoriaSql != null ? grupaKategoriaSql: ""));

						if(dateto.before(DateUtils.parseJsDate("15-01-2009")))
						{
							ps.setDate(1, new java.sql.Date(dateto.getTime()));
						}
						else
						{
							ps.setDate(1, new java.sql.Date(DateUtils.parseJsDate("15-01-2009").getTime()));
						}
						ps.setString(2, user);
						ps.setDate(3, new java.sql.Date(datefrom.getTime()));
						ps.setLong(4, ei.getId());

						rs = ps.executeQuery();
						rs.next();
						contactCount += rs.getInt(1);
						ps.close();
						datefromafter1501 = DateUtils.parseJsDate("15-01-2009");
					}

					ps = DSApi.context().prepareStatement(sql+(grupaKategoriaSql != null ? grupaKategoriaSql: ""));
					ps.setLong(1, ei.getId());
					ps.setString(2, user);
					ps.setDate(3, new java.sql.Date(datefromafter1501.getTime()));
					ps.setDate(4, sqlHelper.addDayDate(dateto, 1));
					rs = ps.executeQuery();
					rs.next();
					contactCount += rs.getInt(1);
					dumper.addText(contactCount+"");
					contactCount = 0;
					DSApi.context().closeStatement(ps);
				}
				dumper.dumpLine();
			}
			catch(Exception e)
			{
				log.error("",e);
				throw new Exception(e);
			}
			i++;
		}
		dumper.closeFileQuietly();
	}
}
