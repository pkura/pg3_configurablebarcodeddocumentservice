/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.compan.docusafe.parametrization.ilpol;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.security.AccessDeniedException;

/**
 *
 * @author Piter Boss
 */
public class CrmTaskLogic  extends AbstractDocumentLogic{

   public static final String RODZAJ_FIELD_CN = "RODZAJ";
   public static final String KATEGORIA_FIELD_CN = "KATEGORIA";
   
   public static final String NUMER_KONTRAHENTA_FIELD_CN = "NUMER_KONTRAHENTA";
   public static final String NUMER_PRACOWNIKA_FIELD_CN = "NUMER_PRACOWNIKA";
   public static final String DATA_FIELD_CN = "DATA";
   public static final String OPIS_FIELD_CN = "OPIS";
   public static final String ARCHIWIZACJA_Z_KONTRAHENTEM_FIELD_CN = "ARCHIWIZACJA_Z_KONTRAHENTEM";



   public static final String RODZAJ_TELEFON_CN = "Telefon";
   public static final String RODZAJ_E_MAIL_CN = "E-mail";
   public static final String RODZAJ_SPOTKANIE_CN = "Spotkanie";
   public static final String RODZAJ_INNE_CN = "Inne";

   private static CrmTaskLogic instance;

    public static CrmTaskLogic getInstance()
    {
        if (instance == null)
            instance = new CrmTaskLogic();
        return instance;
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
        if(document instanceof OfficeDocument)
        {
            ((OfficeDocument)document).setSummary("Zadanie CRM");
        }
    }

    public void documentPermissions(Document document) throws EdmException
    {

    }

    @Override
    public void checkCanFinishProcess(OfficeDocument document) throws AccessDeniedException, EdmException
    {
        if (document.getRemarks() == null || document.getRemarks().size() == 0)
                throw new AccessDeniedException(" Co najmniej jedna uwaga musi zosta� wprowadzona dla dokumentu");
    }

    @Override
    public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
    {
    	String channel = document.getDocumentKind().getChannel(document.getId());
    	if(channel != null)
    	{
	    	 if( ProcessCoordinator.find(DocumentLogicLoader.CRMTASK_KIND,channel) != null)
	    	        return ProcessCoordinator.find(DocumentLogicLoader.CRMTASK_KIND,channel).get(0);
	    	 else
	    		 return null;
    	}
    	return null;
    }


}
