/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.compan.docusafe.parametrization.ilpol;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.security.AccessDeniedException;

/**
 *
 * @author Piter Boss
 */
public class CrmAppLogic  extends AbstractDocumentLogic{

   public static final String KATEGORIA_FIELD_CN = "KATEGORIA";
   public static final String RODZAJ_FIELD_CN = "RODZAJ";
   public static final String NUMER_KONTRAHENTA_FIELD_CN = "NUMER_KONTRAHENTA";
   public static final String NUMER_PRACOWNIKA_FIELD_CN = "NUMER_PRACOWNIKA";
   public static final String DATA_FIELD_CN = "DATA";
   public static final String OPIS_FIELD_CN = "OPIS";
   public static final String ARCHIWIZACJA_Z_KONTRAHENTEM_FIELD_CN = "ARCHIWIZACJA_Z_KONTRAHENTEM";



   public static final String RODZAJ_TELEFON_CN = "TELEFON";
   public static final String RODZAJ_E_MAIL_CN = "E_MAIL";
   public static final String RODZAJ_SPOTKANIE_CN = "SPOTKANIE";
   public static final String RODZAJ_INNE_CN = "INNE";





   private static CrmAppLogic instance;

    public static CrmAppLogic getInstance()
    {
        if (instance == null)
            instance = new CrmAppLogic();
        return instance;
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {

    }

    public void documentPermissions(Document document) throws EdmException
    {

    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
        Map<String,Object> values = new HashMap<String,Object>();
       
        values.put(DATA_FIELD_CN, new Date());
        fm.reloadValues(values);       
    }
}
