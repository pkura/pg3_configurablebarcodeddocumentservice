package pl.compan.docusafe.parametrization.ilpol;

import java.util.HashMap;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.service.imports.ImpulsImportUtils;
import pl.compan.docusafe.service.imports.dsi.DSIImportHandler;

public class LeasingDSIHandler extends DSIImportHandler 
{
	protected static HashMap<String, String> rodajMap = new HashMap<String, String>();
	static
	{
		rodajMap.put("przekladka", "12");
		rodajMap.put("i11", "11");
		rodajMap.put("i10", "10");
		rodajMap.put("i09", "09");
		rodajMap.put("i08", "08");
		rodajMap.put("i07", "07");
		rodajMap.put("i06", "06");
		rodajMap.put("i05", "05");
		rodajMap.put("i04", "04");
		rodajMap.put("i03", "03");
		rodajMap.put("i02", "02");
		rodajMap.put("i01", "01");
	}
	public String getDockindCn()
	{
		return "dl";
	}
	
	@Override
	protected boolean isStartProces()
	{
		return true;
	}
	
	protected void prepareImport() throws Exception 
	{
		String numerUmowy = ((List<String>)values.get("NUMER_UMOWY")).get(0);
		String rodzaj =  ""+values.get("RODZAJ_DOKUMENTU");
		log.error("Import DSI numerUmowy {}, rodzaj {}",numerUmowy,rodzaj);
		DlContractDictionary contract =  null;
		List<DlContractDictionary>  contracts = DlContractDictionary.findByNumerUmowy(numerUmowy);
		if(contracts == null || contracts.size() < 1)
		{
			contract = ImpulsImportUtils.findContractByNumber(numerUmowy);
		}
		else
		{
			contract = contracts.get(0);
		}
		if(contract == null)
			throw new EdmException("Nie znalazl umowy nr. "+numerUmowy);
		values.remove("RODZAJ");
		values.remove("NUMER_UMOWY");
		values.put("NUMER_UMOWY", contract.getId());
		values.put("KLIENT", contract.getIdKlienta());
		String rodzajCn = "ZAKLADKA_"+rodajMap.get(rodzaj);
		log.error("Import DSI umowa {}, klient {} rodzajCn {}",contract.getId(),contract.getIdKlienta(),rodzajCn);
		EnumItem rodzajEnumItem = null;
		rodzajEnumItem = DocumentKind.getDockindInfo(getDockindCn()).getDockindFieldsMap().get("RODZAJ_DOKUMENTU").getEnumItemByCn(rodzajCn);
		values.put("RODZAJ_DOKUMENTU", rodzajEnumItem.getId());
		values.put("STATUS_DOKUMENTU", 45);
	}
}
