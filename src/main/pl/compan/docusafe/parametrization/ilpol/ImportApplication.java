package pl.compan.docusafe.parametrization.ilpol;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.crm.Role;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.parametrization.ilpol.AppDocument;
import pl.compan.docusafe.parametrization.ilpoldwr.MarketingTaskLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ImportApplication {
	protected static Logger log = LoggerFactory.getLogger(ImportApplication.class);
	public static final String GRUPA_FIELD_CN = "GRUPA";
	public static final String COUNTY_FIELD_CN = "COUNTY";
	public static final String NUMER_KONTRAHENTA_FIELD_CN = MarketingTaskLogic.NUMER_KONTRAHENTA_FIELD_CN;
	private static final String NEW_CONTACT_ROLE_NAME = "Nowy Kontakt";
	private String email, companyName, phone, nip, region;
	private StringBuilder remark;
	private String regionFullName;

	public ImportAnswer export(AppDocument document) {
		remark = new StringBuilder();
		if (document == null) {
			log.debug("Odebrano null'a");
			return getAnswer(0L, 1, "Brak dokumentu");
		}

		if (!document.getDoctype().equals("WNIOSEK_LEASINGOWY"))
			return getAnswer(0L, 1, "B��dny doctype. Doctype mo�e przyj�� jedn� z warto�ci :{WNIOSEK_LEASINGOWY}");

		if (document.getAttributes().length < 1) {
			log.debug("Brak atrybut�w");
			return getAnswer(0L, 1, "Brak atrybut�w");
		}
		if (document.getDoctype() == null) {
			log.debug("Brak parametru doctype");
			return getAnswer(0L, 1, "Brak parametru doctype");
		}

		log.debug("OTRZYMANO: " + "getDoctype()=" + document.getDoctype() + ", getId()=" + document.getId());

		if (document.getAttributes() != null) {
			StringBuilder attrDebug = new StringBuilder();
			int n = 1;
			for (Attribute attr : document.getAttributes()) {
				attrDebug.append("Atrybut nr" + n++ + ": getCn()=" + attr.getCn() + ", getName()=" + attr.getName() + ", getValue()=" + attr.getValue()
						+ ", getType()=" + attr.getType()+"; ");
				if (attr.getCn().equals("person") 
						|| attr.getCn().equals("period") 
						|| attr.getCn().equals("income") 
						|| attr.getCn().equals("leasing-item")
						|| attr.getCn().equals("production-year") 
						|| attr.getCn().equals("value") 
						|| attr.getCn().equals("payment")
						|| attr.getCn().equals("duration") 
						|| attr.getCn().equals("additional")) {
					if (attr.getName() != null && !attr.getName().equals("") && attr.getValue() != null && !attr.getValue().equals(""))
						remark.append(attr.getName() + ": " + attr.getValue() + "\n");
				} else if (attr.getCn().equals("agree-processing") && attr.getValue().equals("1")) {
					remark.append("Wyra�ono zgod� na przetwarzanie danych osobowych\n");
				} else if (attr.getCn().equals("agree-receiving") && attr.getValue().equals("1")) {
					remark.append("Wyra�ono zgod� na otrzymywanie informacji handlowych\n");
				} else if (attr.getCn().equals("email") && !attr.getValue().equals(""))
					email = attr.getValue();
				else if (attr.getCn().equals("company-name") && !attr.getValue().equals(""))
					companyName = attr.getValue();
				else if (attr.getCn().equals("phone") && !attr.getValue().equals(""))
					phone = attr.getValue();
				else if (attr.getCn().equals("nip") && !attr.getValue().equals(""))
					nip = attr.getValue();
				else if (attr.getCn().equals("region") && !attr.getValue().equals("")) {
					region = cleanFromDiacriticalChararacters(attr.getValue().replace("-",""));
					regionFullName = attr.getValue();
				}
					
				else
					log.debug("Nieobs�u�ono atrybutu: " + attr.getName());
			}
			log.debug(attrDebug.toString());
		}
		Long docId = 0L;
		try {
			DSApi.openAdmin();
			DSApi.context().begin();
			createImportedDocument();
			DSApi.close();
		} catch (Exception e) {
			log.error("B��D podczas przetwarzania danych wniosku leasingowego z webservice'u: " + e.getMessage(), e);
			
			try {
				DSApi.close();
			} catch (Exception eClose) {
				log.error(eClose.getMessage());
			}
			return getAnswer(docId, 1, "B��D podczas przetwarzania danych wniosku leasingowego z webservice'u: " + e.getMessage());
		} 
		return getAnswer(docId, 2, "Dokument zaimportowany");
	}
	public static String cleanFromDiacriticalChararacters(String input) {
		Map<String,String> chars = new HashMap<String,String>();
		chars.put("�", "a");
		chars.put("�", "c");
		chars.put("�", "e");
		chars.put("�", "l");
		chars.put("�", "n");
		chars.put("�", "o");
		chars.put("�", "s");
		chars.put("�", "z");
		chars.put("�", "z");
		chars.put("�", "A");
		chars.put("�", "C");
		chars.put("�", "E");
		chars.put("�", "L");
		chars.put("�", "N");
		chars.put("�", "O");
		chars.put("�", "S");
		chars.put("�", "Z");
		chars.put("�", "Z");

		for (String sign:chars.keySet()) {
			input=input.replace(sign, chars.get(sign));
		}
		
		return input;
	}

	private ImportAnswer getAnswer(Long i, Integer result, String error) {
		ImportAnswer a = new ImportAnswer();
		a.setApplicationId(i);
		a.setError(error);
		a.setResult(result);
		return a;
	}

	private Long createImportedDocument() throws Exception {
		ProcessCoordinator proCoordinator = null;
		String userName;
		String guid;
		log.debug("region "+region);
		if (region != null) {
			if (ProcessCoordinator.find(DocumentLogicLoader.CRM_MARKETING_TASK_KIND, region) != null
					&& ProcessCoordinator.find(DocumentLogicLoader.CRM_MARKETING_TASK_KIND, region).size() > 0) {
				proCoordinator = ProcessCoordinator.find(DocumentLogicLoader.CRM_MARKETING_TASK_KIND, region).get(0);
			}
		}
		
		if (proCoordinator == null) {
			throw new EdmException("Nie znaleziono kana�u/koordynatora");
		}
		userName=proCoordinator.getUsername();
		guid=proCoordinator.getGuid();
		
		//force user to admin
		//userName="admin";
		
		log.debug("DEKRETUJE DO user: "+userName+", guid: "+guid);

		DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.CRM_MARKETING_TASK_KIND);
		Map<String, Object> values = setValues(documentKind);

		try {
			InOfficeDocument doc = new InOfficeDocument();
			ServicesUtils.createTaskForPersonOrDiv("WWW", values, userName, guid, remark.toString(), doc, documentKind, "WWW",false);
			DSApi.context().commit();
			return doc.getId();
		} catch (Exception e) {
			log.error("B��D podczas tworzenia wniosku leasing'owego z webservice'u: " + e.getMessage(), e);
			DSApi.context().rollback();
			throw new EdmException("B��d tworzenia zadania "+e.getMessage());
		} 
	}

	private Map<String, Object> setValues(DocumentKind documentKind) throws Exception {
		Map<String, Object> values = new HashMap<String, Object>();
		nip = nip.replaceAll("\"", "");
		nip = nip.replaceAll("-", "");
		if (nip.length()>25) nip=nip.substring(0,25);
		
		List<Contractor> conList = Contractor.findByNIP(nip);

		if (conList != null && conList.size() > 0) {
			values.put(NUMER_KONTRAHENTA_FIELD_CN, conList.get(0).getId());
		} else {
			Contractor cont = new Contractor();
			Role r = Role.findByName(NEW_CONTACT_ROLE_NAME);
			cont.addRole(r);
			
			if (nip != null) cont.setNip(nip);
			if (companyName != null) {
				cont.setName(companyName);
				cont.setFullName(companyName);
			}
			if (phone != null) cont.setPhoneNumber(phone);
			if (email != null) cont.setEmail(email);
			cont.create();
			values.put(NUMER_KONTRAHENTA_FIELD_CN, cont.getId());
		}
		values.put(GRUPA_FIELD_CN, documentKind.getFieldByCn("GRUPA").getEnumItemByTitle("WWW").getId());
		values.put("KATEGORIA", documentKind.getFieldByCn("KATEGORIA").getEnumItemByCn("BazaH").getId());
		values.put(COUNTY_FIELD_CN, documentKind.getFieldByCn("COUNTY").getEnumItemByTitle(regionFullName).getId());

		return values;
	}
}
