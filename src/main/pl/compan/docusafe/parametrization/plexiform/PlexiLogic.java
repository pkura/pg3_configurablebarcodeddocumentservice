package pl.compan.docusafe.parametrization.plexiform;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;

public class PlexiLogic extends AbstractDocumentLogic {

	
	private static final PlexiLogic instance = new PlexiLogic();
	
	public static PlexiLogic getInstance() {
		return instance;
	}
	
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
		document.setFolder(folder);
		
		document.setTitle(String.valueOf(fm.getValue(fm.getMainFieldCn())));
		document.setDescription(String.valueOf(fm.getValue(fm.getMainFieldCn())));
		
	}

	
	public void documentPermissions(Document document) throws EdmException 
	{
	

	}

}
