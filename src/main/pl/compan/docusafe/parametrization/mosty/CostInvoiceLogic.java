package pl.compan.docusafe.parametrization.mosty;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import com.google.common.collect.Maps;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field.Type;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;

import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;

import pl.compan.docusafe.parametrization.polcz.AbstractPczDocumentLogic;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class CostInvoiceLogic extends AbstractPczDocumentLogic
{
	
	private static CostInvoiceLogic instance;
	protected static Logger log = LoggerFactory.getLogger(CostInvoiceLogic.class);
	private static String SECRETARYS_OFFICE_GUID = "sekretariat";
	private static String CASHIERS_OFFICE_GUID = "BFA800025870411113C052A43A79CEF0EC0";

	
	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
		boolean kasa = false;
		boolean sekretariat = false;
//		for(DSDivision division: DSApi.context().getDSUser().getDivisionsAsList()){
//			if(division.getGuid().equalsIgnoreCase(SECRETARYS_OFFICE_GUID)) sekretariat = true;
//			else if(division.getGuid().equalsIgnoreCase(CASHIERS_OFFICE_GUID)) kasa = true; 
//		}
		
		Map<String, Object> toReload = Maps.newHashMap();
//		for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields()) {
//			if (f.getDefaultValue() != null)
//				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
//			if(f.getCn().equals("RODZAJ_FAKTURY"))
//			{
//				if(kasa && !sekretariat){
//					toReload.put("RODZAJ_FAKTURY","202");
//				}else if(!kasa && sekretariat){
//					toReload.put("RODZAJ_FAKTURY","201");
//				}else if(kasa && sekretariat){
//					f.setReadOnly(false);
//				}
//			}
//			DataBaseEnumField.getEnumItemForTable("", 2).
//		}
				

		toReload.put("DATA_WPLYWU", new Date());
		toReload.put("STATUS", 10);
		
		
		fm.reloadValues(toReload);
	}
	
	
	
	@Override
	public Map<String, Object> setMultipledictionaryValue(
			String dictionaryName, Map<String, FieldData> values) { 
		// TODO Auto-generated method stub
		String aaa = "a";
		HashMap<String, Object> toReload = new HashMap<String, Object>();
		
		
		
		if(values.get("MPK_LOKALIZACJA")!=null )
		{	
			EnumValues valu = values.get("MPK_LOKALIZACJA").getEnumValuesData();
			String option = valu.getSelectedId();
			String idBudowy = Docusafe.getAdditionProperty("mosty_id_dla_budow");
			String idDzialy = Docusafe.getAdditionProperty("mosty_id_dla_dzialow"); 
//			String idDzialy = "33,34,35,36,37,38,39,40,41,42";
			
			if(option.equals(idBudowy))
			{
				EnumValues tempVal = values.get("MPK_GROUPID").getEnumValuesData();
				String selval=null;
				
				HashMap<String, String> emptyMap = new HashMap<String, String>();
				emptyMap.put("-5", "Nie dotyczy");
				ArrayList<String> emptySelected = new ArrayList<String>();
				emptySelected.add("-5");
				
				
				if(values.get("MPK_GROUPID")!=null&&values.get("MPK_GROUPID").getEnumValuesData()!=null){
					try{
						selval=values.get("MPK_GROUPID").getEnumValuesData().getSelectedId();
					}catch(Exception e)
					{
						log.error("JEST ERROR!!!!!!!!!!!");
						log.error(e.getMessage(), e);
					}
				}
				if(tempVal.getSelectedId()==null || selval==null || selval.trim().length()==0 || selval.equals("-5"))
				{
					DataBaseEnumField base = DataBaseEnumField.tableToField.get("dsr_projekt_budowa_view").iterator().next();
					EnumValues obje = base.getEnumValuesForForcedPush(values, "MPK");
					obje.getAllOptions().remove(emptyMap);
					toReload.put("MPK_GROUPID", obje);
				}
				String a =values.get("MPK_PRODUCTID").getEnumValuesData().getSelectedId();
				String selval2 = null;
				if(values.get("MPK_PRODUCTID")!=null&&values.get("MPK_PRODUCTID").getEnumValuesData()!=null){
					try{
					selval2=values.get("MPK_PRODUCTID").getEnumValuesData().getSelectedId();
					}catch(Exception e)
					{
						log.error("JEST ERROR!!!!!!!!!!!");
						log.error(e.getMessage(), e);
					}
				}
				if(a==null || a.trim().length()==0|| selval2==null || selval2.trim().length()==0 || selval2.equals("-5"))
				{
					DataBaseEnumField base2 = DataBaseEnumField.tableToField.get("dsg_mosty_kierownicy_budow").iterator().next();
					EnumValues obje2 = base2.getEnumValuesForForcedPush(values, "MPK");
					
					obje2.getAllOptions().remove(emptyMap);
					
					
					toReload.put("MPK_PRODUCTID", obje2);
				}
				
				
				
				
				toReload.put("MPK_INVENTORYID",  new EnumValues(emptyMap, emptySelected));
				toReload.put("MPK_ORDERID",  new EnumValues(emptyMap, emptySelected));
			}
			else if(idDzialy.contains(option))
			{
				
				HashMap<String, String> emptyMap = new HashMap<String, String>();
				emptyMap.put("-5", "Nie dotyczy");
				ArrayList<String> emptySelected = new ArrayList<String>();
				emptySelected.add("-5");
				
				toReload.put("MPK_GROUPID", new EnumValues(emptyMap, emptySelected)); 
				toReload.put("MPK_PRODUCTID", new EnumValues(emptyMap, emptySelected)); 
				
				String a =values.get("MPK_ORDERID").getEnumValuesData().getSelectedId();
				String selval=null;
				if(values.get("MPK_ORDERID")!=null && values.get("MPK_ORDERID").getEnumValuesData()!=null)
				{
					try{
				selval=values.get("MPK_ORDERID").getEnumValuesData().getSelectedId();
					}catch(Exception e){
						log.error("JEST ERROR!!!!!!!!!!!");
						log.error(e.getMessage(), e);
					}
				}
				if(a==null || a.trim().length()==0 || selval==null || selval.trim().length()==0 || selval.equals("-5")) 
				{
					DataBaseEnumField base3 = DataBaseEnumField.tableToField.get("dsg_mosty_dzialy").iterator().next();
					EnumValues obje3 = base3.getEnumValuesForForcedPush(values, "MPK");
					
					obje3.getAllOptions().remove(emptyMap);
					toReload.put("MPK_ORDERID", obje3);
				}
				String selval2 = null;
				a =values.get("MPK_INVENTORYID").getEnumValuesData().getSelectedId();
				if(values.get("MPK_INVENTORYID")!=null &&values.get("MPK_INVENTORYID").getEnumValuesData()!=null){
					try{
				selval2=values.get("MPK_INVENTORYID").getEnumValuesData().getSelectedId();
					}catch(Exception e)
					{
						log.error("JEST ERROR!!!!!!!!!!!");
						log.error(e.getMessage(), e);
					}
				}
				if(a==null || a.trim().length()==0 || selval2==null || selval2.trim().length()==0 || selval2.equals("-5"))
				{
					DataBaseEnumField base4 = DataBaseEnumField.tableToField.get("dsg_mosty_pracownicy").iterator().next();
					EnumValues obje4 = base4.getEnumValuesForForcedPush(values, "MPK");
					obje4.getAllOptions().remove(emptyMap);
					
					toReload.put("MPK_INVENTORYID", obje4);
				}
			}
			else
			{
				HashMap<String, String> emptyMap = new HashMap<String, String>();
				ArrayList<String> emptySelected = new ArrayList<String>();
				emptySelected.add("Nie dotyczy2");
				EnumValues empty = new EnumValues(emptyMap, emptySelected);
				
				toReload.put("MPK_ORDERID", empty);
				toReload.put("MPK_INVENTORYID", empty);
				toReload.put("MPK_GROUPID", empty);
				toReload.put("MPK_PRODUCTID", empty);
			}
		}
		
		
		 

		
		return toReload;
	}



	public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
	{
		StringBuilder msgBuilder = new StringBuilder();
		
		try{
		if(values.containsKey("DWR_VAT") && values.containsKey("DWR_KWOTA_NETTO") && values.get("DWR_KWOTA_NETTO").getMoneyData()!=null && values.get("DWR_VAT").getMoneyData() != null){
			values.put("DWR_KWOTA_BRUTTO", new FieldData(Type.MONEY, values.get("DWR_KWOTA_NETTO").getMoneyData().add(values.get("DWR_VAT").getMoneyData())));
		}
		if(values.containsKey("DWR_DATA_PLATNOSCI") && values.get("DWR_DATA_PLATNOSCI").getDateData()!=null && values.get("DWR_TERMIN_REALIZACJI").getDateData()==null){
			Date dataPl = values.get("DWR_DATA_PLATNOSCI").getDateData();
			Calendar cal = Calendar.getInstance();
			cal.setTime(dataPl);
			cal.add(cal.DAY_OF_MONTH, -4);
			values.put("DWR_TERMIN_REALIZACJI", new FieldData(Type.DATE, cal.getTime()));
		}
		if (msgBuilder.length() > 0)
		{
			values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
			return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
		}
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
		return null;
	}
	
	public BigDecimal setBrutto(FieldsManager fm){
		try{
			BigDecimal netto = (BigDecimal) fm.getValue("KWOTA_NETTO");
			BigDecimal vat = (BigDecimal) fm.getValue("VAT");
			BigDecimal brutto = netto.add(vat); 
			
			return brutto;
			
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
		return null;
	}
	
	public static CostInvoiceLogic getInstance()
	{
		if (instance == null)
			instance = new CostInvoiceLogic();
		return instance;
	}
	
	

	@Override
	public boolean assignee(OfficeDocument doc, Assignable assignable,
			OpenExecution openExecution, String acceptationCn,
			String fieldCnValue)
    {
		try{
		if(acceptationCn.equals("mpk_user") || acceptationCn.equals("additional_user")){
			Long userId = (Long) openExecution.getVariable("costCenterId"); 
			
			Map<String, Object> mpk = DwrDictionaryFacade.dictionarieObjects.get("MPK").getValues(userId.toString());
			EnumValues ev = (EnumValues) mpk.get("MPK_LOKALIZACJA");
			if(ev.getSelectedId() != null && ev.getSelectedId().length()>0){
				Long mpkid = Long.parseLong(ev.getSelectedId());
				FieldsManager fm = doc.getFieldsManager();
				
				
				
				//jezeli mpkId - MPK501 lub MPK550 pismo idzie na kierownika budowy lub pracownika odpowiedniego dzialu
				String directorMpk = Docusafe.getAdditionProperty("mosty_id_dla_budow");
				String divisionMpk = Docusafe.getAdditionProperty("mosty_id_dla_dzialow");
				
				if(ev.getSelectedId().equals(directorMpk))
				{
					fm.initialize();
					Map<String, Object> mapa = fm.getFieldValues(); 
					CentrumKosztowDlaFaktury centrum = CentrumKosztowDlaFaktury.getInstance();
					 
					Criteria criteria = DSApi.context().session().createCriteria(CentrumKosztowDlaFaktury.class);
					criteria.add(Restrictions.eq("id", userId));
					List empek = criteria.list(); 
					CentrumKosztowDlaFaktury idUser = (CentrumKosztowDlaFaktury) empek.get(0);
					DSUser ass = DSUser.findById(Long.valueOf(idUser.getProductId()));
					
					assignable.addCandidateUser(ass.getName());
					log.error("mosty faktura" + fm.getKey("MPK"));
					log.error("mosty faktura" + fm.getValue("MPK"));
				}
				else if(divisionMpk.contains(ev.getSelectedId()))
				{
					fm.initialize();
					Map<String, Object> mapa = fm.getFieldValues(); 
					CentrumKosztowDlaFaktury centrum = CentrumKosztowDlaFaktury.getInstance();
					 
					Criteria criteria = DSApi.context().session().createCriteria(CentrumKosztowDlaFaktury.class);
					criteria.add(Restrictions.eq("id", userId));
					List empek = criteria.list(); 
					CentrumKosztowDlaFaktury idUser = (CentrumKosztowDlaFaktury) empek.get(0);
					log.error("XXXXXXXXXXX ERROR:" + idUser);
					//dekrwetacja na usera
					if(idUser.getInventoryId()!=null)
					{
						DSUser ass = DSUser.findById(Long.valueOf(idUser.getInventoryId()));
						
						assignable.addCandidateUser(ass.getName());
					}
					//dekretacja na dzia�
					else
					{
						DSDivision div = DSDivision.findById(idUser.getOrderId());
						assignable.addCandidateGroup(div.getGuid());
					}
					
					
				}
				else
				{
					EnumItem lokalizacja = DataBaseEnumField.getEnumItemForTable("dsg_mosty_kod_mpk", mpkid.intValue());
					Long uId = Long.parseLong(lokalizacja.getRefValue());
//					userzy.add(userId);
					DSDivision division = DSDivision.findById(uId);
					assignable.addCandidateGroup(division.getGuid());
				}
				return true; 
			}
//			userzy.add(id);
//			DSUser user = DSUser.findById(userId);
//			DSDivision division = DSDivision.find(userId.intValue());
//			assignable.addCandidateUser(user.getName());
//			assignable.addCandidateGroup(division.getGuid());
//			return true;
		}
		}catch(Exception e){
			log.error(e.getMessage(), e);
			
		}
		return false;
    	
    }
	
	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk�y- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk�y - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk�y - usuwanie"));
		this.setUpPermission(document, perms);
	}

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Projekt/Budowa");

		if (fm.getEnumItem("BUDOWA") != null)
			folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getEnumItem("BUDOWA").getTitle()));
		else
			folder = folder.createSubfolderIfNotPresent("Numer projektu nieokre�lony");

		if (fm.getEnumItem("ETAP") != null)
			folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getEnumItem("ETAP").getTitle()));
		else
			folder = folder.createSubfolderIfNotPresent("Nazwa etapu nieokre�lona");
		
		//folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(document.getCtime()));
		
		if (fm.getEnumItem("KATALOG") != null)
			folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getEnumItem("KATALOG").getTitle()));
		else
			folder = folder.createSubfolderIfNotPresent("Nazwa katalogu nieokre�lona");
		
		//folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(document.getCtime()));
		
		document.setTitle("Faktura kosztowa nr: " + String.valueOf(fm.getStringValue("NUMER_FAKTURY")));
//		document.setDescription("Faktura kosztowa nr: " + String.valueOf(fm.getStringValue("NUMER_FAKTURY")));
		document.setFolder(folder);
	}
	
	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		super.onStartProcess(document, event);
	}
	
	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
	{
		super.onSaveActions(kind, documentId, fieldValues);
		FieldsManager fm = Document.find(documentId).getFieldsManager();
		fm.initialize();
		isKwotaBruttoEqSumBudgetPositions(fm);
		int yyy = 0;
		if(fm.getKey("STATUS") != null){
			yyy =(Integer) fm.getKey("STATUS");
		}
		if(fieldValues.keySet().size()> 1 && yyy==370)
		{
			if(!fm.containsField("NUMER_PZ") || fm.getValue("NUMER_PZ")==null){
				throw new EdmException("Przed zako�czeniem pracy z dokumentem nale�y nada� numer PZ");
			}
			
		}
	}
	
	private void isKwotaBruttoEqSumBudgetPositions(FieldsManager fm) throws EdmException
	{
		BigDecimal netto = (BigDecimal) fm.getFieldValues().get("KWOTA_NETTO");
//		netto.setScale(2);
		List<Long> ids = (List<Long>) fm.getKey("MPK");
		BigDecimal suma = BigDecimal.ZERO;

		for (Long id : ids) 
		{
			Map<String, Object> mpk = DwrDictionaryFacade.dictionarieObjects.get("MPK").getValues(id.toString());
			BigDecimal kwota = BigDecimal.valueOf((Double) mpk.get("MPK_AMOUNT"));
			suma = suma.add(kwota);
		}
//		suma.setScale(2);
		int porownanie = netto.compareTo(suma); 
		if (porownanie != 0)
		{
			throw new EdmException("Kwota netto musi by� r�wna sumie kwot wszystkich pozycji bud�etowych.");
		}
	}

	protected BigDecimal sumBudgetItems(Map<String, ?> fieldValues, String budgetName) throws EdmException
	{
		BigDecimal totalCentrum = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
		Map<String, Object> mpkValues = (Map<String, Object>) fieldValues.get(budgetName);
		for (String mpkCn : mpkValues.keySet())
		{
			if (mpkCn.contains("MPK_"))
			{
				Map<String, Object> mpkVal = (Map<String, Object>) mpkValues.get(mpkCn);
				BigDecimal oneAmount = ((FieldData) mpkVal.get("AMOUNT")).getMoneyData();
				if (oneAmount != null)
					totalCentrum = totalCentrum.add(oneAmount);
			}
		}
		return totalCentrum;
	}

}
