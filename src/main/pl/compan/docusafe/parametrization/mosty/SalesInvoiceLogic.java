package pl.compan.docusafe.parametrization.mosty;

import java.util.Set;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.util.FolderInserter;

public class SalesInvoiceLogic extends AbstractDocumentLogic{

	private static final long serialVersionUID = 1L;

	@Override
	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
		String documentKindName = document.getDocumentKind().getName();

		perms.add(new PermissionBean(ObjectPermission.READ, "MOSTY_" + documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, "MOSTY - " + documentKindName + " - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "MOSTY_" + documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "MOSTY - " + documentKindName + " zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "MOSTY_" + documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "MOSTY - " + documentKindName + " - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "MOSTY_" + documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "MOSTY - " + documentKindName + " zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "MOSTY_" + documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "MOSTY - " + documentKindName + " - usuwanie"));
		Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
		perms.addAll(documentPermissions);
		this.setUpPermission(document, perms);
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
		
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Projekt/Budowa");

		if (fm.getEnumItem("BUDOWA") != null)
			folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getEnumItem("BUDOWA").getTitle()));
		else
			folder = folder.createSubfolderIfNotPresent("Numer projektu nieokre�lony");

		if (fm.getEnumItem("ETAP") != null)
			folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getEnumItem("ETAP").getTitle()));
		else
			folder = folder.createSubfolderIfNotPresent("Nazwa etapu nieokre�lona");
		
		//folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(document.getCtime()));
		
		if (fm.getEnumItem("KATALOG") != null)
			folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getEnumItem("KATALOG").getTitle()));
		else
			folder = folder.createSubfolderIfNotPresent("Nazwa katalogu nieokre�lona");
		
		//folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(document.getCtime()));
		
		document.setTitle("Faktura sprzedaży nr " + String.valueOf(fm.getStringValue("NUMER_FAKTURY")));
		document.setDescription("Faktura sprzedaży nr " + String.valueOf(fm.getStringValue("NUMER_FAKTURY")));
		document.setFolder(folder);
		
	}

}
