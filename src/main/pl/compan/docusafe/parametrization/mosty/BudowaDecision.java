package pl.compan.docusafe.parametrization.mosty;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class BudowaDecision implements DecisionHandler {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(BudowaDecision.class);

	private String field;

	@Override
	public String decide(OpenExecution openExecution) {
		try {
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			log.info("documentid = " + docId + " field = " + field);
			EnumItem budowa = fm.getEnumItem(field);
			if (budowa == null) {
				throw new IllegalStateException("Nie wype�niono pola " + fm.getField(field).getName());
			}
			if (budowa.getCentrum() == 0)
				return "false";
			else if (budowa.getCentrum() == 1)
				return "true";
			else 
				throw new EdmException("B��dne dane. DataBaseEnumField \"BUDOWA\"; getCentrum() = " + budowa.getCentrum());
		} catch (EdmException ex) {
			log.error(ex.getMessage(), ex);
			return "error";
		}
	}

}
