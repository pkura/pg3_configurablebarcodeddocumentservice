package pl.compan.docusafe.parametrization.mosty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.sql.AcceptanceDivisionImpl;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ZamowienieKierownikowLogic extends AbstractDocumentLogic {

    private static final long serialVersionUID = 1L;
    protected static Logger log = LoggerFactory.getLogger(ZamowienieKierownikowLogic.class);
    public final static String DZ_OR_DA = "dz_or_da";

    @Override
    public void documentPermissions(Document document) throws EdmException {
	java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
	String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
	String documentKindName = document.getDocumentKind().getName();

	perms.add(new PermissionBean(ObjectPermission.READ, "MOSTY_" + documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, "MOSTY - "
		+ documentKindName + " - odczyt"));
	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "MOSTY_" + documentKindCn + "_DOCUMENT_READ_ATT_READ",
		ObjectPermission.GROUP, "MOSTY - " + documentKindName + " zalacznik - odczyt"));
	perms.add(new PermissionBean(ObjectPermission.MODIFY, "MOSTY_" + documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "MOSTY - "
		+ documentKindName + " - modyfikacja"));
	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "MOSTY_" + documentKindCn + "_DOCUMENT_READ_ATT_MODIFY",
		ObjectPermission.GROUP, "MOSTY - " + documentKindName + " zalacznik - modyfikacja"));
	perms.add(new PermissionBean(ObjectPermission.DELETE, "MOSTY_" + documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "MOSTY - "
		+ documentKindName + " - usuwanie"));
	Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
	perms.addAll(documentPermissions);
	this.setUpPermission(document, perms);
    }

    public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
	    	Map<String, Object> values = new HashMap<String, Object>();
		values.put("DOC_DATE", new Date());
		values.put("STATUS", 10);
		fm.reloadValues(values);
	}
    
    @Override
    public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {

	boolean assigned = false;

	try {
	    if (DZ_OR_DA.equals(acceptationCn)) {
		for (AcceptanceCondition accept : AcceptanceCondition.find("dz_department", null)) {
		    assignable.addCandidateGroup(accept.getDivisionGuid());
		    AssigneeHandler.addToHistory(openExecution, doc, null, accept.getDivisionGuid());
		    assigned = true;
		}
		for (AcceptanceCondition accept : AcceptanceCondition.find("da_department", null)) {
		    assignable.addCandidateGroup(accept.getDivisionGuid());
		    AssigneeHandler.addToHistory(openExecution, doc, null, accept.getDivisionGuid());
		    assigned = true;
		}
	    }
	} catch (UserNotFoundException e) {
	    log.error(e.getMessage(), e);
	} catch (EdmException e) {
	    log.error(e.getMessage(), e);
	}

	return assigned;
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {

	FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

	Folder folder = Folder.getRootFolder();
	folder = folder.createSubfolderIfNotPresent("Projekt/Budowa");

	if (fm.getEnumItem("BUDOWA") != null)
	    folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getEnumItem("BUDOWA").getTitle()));
	else
	    folder = folder.createSubfolderIfNotPresent("Numer projektu nieokreślony");

	if (fm.getEnumItem("ETAP") != null)
	    folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getEnumItem("ETAP").getTitle()));
	else
	    folder = folder.createSubfolderIfNotPresent("Nazwa etapu nieokreślona");

	//folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(document.getCtime()));

	if (fm.getEnumItem("KATALOG") != null)
	    folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getEnumItem("KATALOG").getTitle()));
	else
	    folder = folder.createSubfolderIfNotPresent("Nazwa katalogu nieokreślona");

	//folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(document.getCtime()));

	document.setTitle("Zamówienie materiałów, do projektu: " + String.valueOf(fm.getEnumItem("BUDOWA").getTitle()));
	document.setDescription("Zamówienie materiałów, do projektu: " + String.valueOf(fm.getEnumItem("BUDOWA").getTitle()));
	document.setFolder(folder);

    }

}
