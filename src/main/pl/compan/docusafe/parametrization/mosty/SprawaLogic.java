package pl.compan.docusafe.parametrization.mosty;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import com.google.common.collect.Maps;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SprawaLogic extends AbstractDocumentLogic {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(SprawaLogic.class);

	@Override
	public void documentPermissions(Document document) throws EdmException {
		// TODO Auto-generated method stub

	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
		// TODO Auto-generated method stub

	}

	
	
	public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {

		SortedSet<Long> documentIDs = new TreeSet<Long>();
		String budowa = null;
		String etap = null;
		String katalog = null;
		if (values.containsKey("DWR_BUDOWA") && values.get("DWR_BUDOWA").getData() != null) {
			budowa = (String) values.get("DWR_BUDOWA").getData();

		if (values.containsKey("DWR_ETAP") && values.get("DWR_ETAP").getData() != null) {
			etap = (String) values.get("DWR_ETAP").getData();
		}
		if (values.containsKey("DWR_KATALOG") && values.get("DWR_KATALOG").getData() != null) {
			katalog = (String) values.get("DWR_KATALOG").getData();
		}
		
		getDocumentIds(budowa, etap, katalog, documentIDs);
		
		values.put("DWR_DOKUMENTY", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.DICTIONARY, documentIDs));
		}
		return null;
	}

	private void getDocumentIds(String budowa, String etap, String katalog, SortedSet<Long> documentIDs) throws EdmException {
		Statement stat = null;
		ResultSet result = null;
		boolean contextOpened = true;
		try {
			if (!DSApi.isContextOpen()) {
				DSApi.openAdmin();
				contextOpened = false;
			}
			stat = DSApi.context().createStatement();

			StringBuilder select = new StringBuilder("SELECT distinct doc.id ");
			StringBuilder from = new StringBuilder("FROM dsg_mosty_documents dsg join ds_document doc on doc.id=dsg.document_id ");
			StringBuilder where = new StringBuilder("WHERE ");

			where.append("doc.DELETED = 0");
			if (budowa != null) {
				where.append(" AND dsg.BUDOWA = " + budowa);
			}
			if (etap != null) {
				where.append(" AND dsg.ETAP = " + etap);
			}
			if (katalog != null) {
				where.append(" AND dsg.KATALOG = " + katalog);
			}
			String sql = select + " " + from + " " + where;
			log.error("select: " + sql);
			result = stat.executeQuery(sql);
			boolean empty = true;
			while (result.next()) {
				Long id = result.getLong(1);
				documentIDs.add(id);
				empty = false;
			}
			if (empty)
				documentIDs.removeAll(documentIDs);


		} catch (Exception e) {
			log.error(e.getMessage(), e);
		} finally {
			if (!contextOpened && DSApi.isContextOpen())
				DSApi.close();
		}
	}
	
	@Override
	public void setAdditionalValues(FieldsManager fm, Integer valueOf, String activity) {
		try {
			SortedSet<Long> documentIDs = new TreeSet<Long>();
			Map<String, Object> values = new HashMap<String, Object>();
			
			String budowa = null;
			String etap = null;
			String katalog = null;
			if (fm.getKey("BUDOWA") != null)
				budowa = ((Integer) fm.getKey("BUDOWA")).toString();
			if (fm.getKey("ETAP") != null)
				etap = ((Integer) fm.getKey("ETAP")).toString();
			if (fm.getKey("KATALOG") != null)
				katalog = ((Integer) fm.getKey("KATALOG")).toString();
			getDocumentIds(budowa, etap, katalog, documentIDs);

			values.put("DOKUMENTY", documentIDs);
			values.put("STATUS", 10);
			fm.reloadValues(values);
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
		Map<String, Object> toReload = Maps.newHashMap();
        for(Field f : fm.getFields()){
            if(f.getDefaultValue() != null){
                toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
            }
        }
        
        DSUser user = DSApi.context().getDSUser();
		toReload.put("KOORDYNATOR", user.getId());
        log.info("toReload = {}", toReload);
        fm.reloadValues(toReload);
	}

}
