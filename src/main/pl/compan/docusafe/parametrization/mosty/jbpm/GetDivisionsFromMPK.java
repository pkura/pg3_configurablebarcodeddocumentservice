package pl.compan.docusafe.parametrization.mosty.jbpm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;
import org.jfree.util.Log;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.jbpm4.CloseOtherProcessInstanceTasks;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class GetDivisionsFromMPK implements ExternalActivityBehaviour {
	private static final Logger log = LoggerFactory.getLogger(GetDivisionsFromMPK.class);
	

	@Override
	public void execute(ActivityExecution execution) throws Exception {
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		
		log.error("mpkdiv test: " + fm.getValue("MPK"));
		log.error("mpkdiv test: " + fm.getValue("MPK").getClass().getCanonicalName());
		List lista = (List) fm.getValue("MPK");
		for(Object mpk: lista)
		{
			HashMap<String, Object> mapa = (HashMap<String, Object>) mpk;
			log.error("mapatest: "+mapa.keySet());
		}
		
//		execution.setVariable("count", dictionaryIds.size());
//		execution.setVariable("ids", dictionaryIds);
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
		// TODO Auto-generated method stub

	}

}
