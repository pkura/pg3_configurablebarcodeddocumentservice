package pl.compan.docusafe.parametrization.mosty.jbpm;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class MagazynDecisionHandler implements DecisionHandler
{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(MagazynDecisionHandler.class);

	@Override
	public String decide(OpenExecution openExecution)
	{

		try
		{
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();

			@SuppressWarnings("unchecked")
			List<Long> ids = (List<Long>) fm.getKey("MATERIALY");
			if (ids == null)
				return "error";
			for (Long id : ids)
			{
				Map<String, Object> material = DwrDictionaryFacade.dictionarieObjects.get("MATERIALY").getValues(id.toString());
				BigDecimal ilosMG = (BigDecimal) material.get("ILOSC_W_MG_1");
				if (ilosMG == null || ilosMG.equals(BigDecimal.ZERO.setScale(2)))
					return "brak";
			}
			return "jest";
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			return "error";
		}
	}
}
