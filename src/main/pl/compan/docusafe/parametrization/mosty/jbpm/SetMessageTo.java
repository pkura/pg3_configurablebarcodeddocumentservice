package pl.compan.docusafe.parametrization.mosty.jbpm;

import java.util.HashMap;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SetMessageTo  implements ExternalActivityBehaviour{
	
	private static Logger LOG = LoggerFactory.getLogger(ProcessParamsAssignmentHandler.class);

	@Override
	public void execute(ActivityExecution execution) throws Exception {
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();

		HashMap<String, Object> mapa = new HashMap<String, Object>();
		mapa = (HashMap<String, Object>) fm.getValue("DO_WIADOMOSCI_XXX2");
//		LOG.error("DO_WIADOMOSCI_XXX2:"+fm.getValue("DO_WIADOMOSCI_XXX2"));
//		LOG.error("DO_WIADOMOSCI_XXX2 WARUNEK:"+(mapa!=null));
//		LOG.error("DO_WIADOMOSCI_XXX2 WARUNEK:"+(mapa.containsKey("DO_WIADOMOSCI_XXX2_NAME")));
//		LOG.error("DO_WIADOMOSCI_XXX2 WARUNEK:"+(mapa.get("DO_WIADOMOSCI_XXX2_NAME")!=null));
		if(mapa!=null && mapa.containsKey("DO_WIADOMOSCI_XXX2_NAME") && mapa.get("DO_WIADOMOSCI_XXX2_NAME")!=null)
			execution.setVariable("user", mapa.get("DO_WIADOMOSCI_XXX2_NAME"));

		
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

}
