package pl.compan.docusafe.parametrization.mosty.jbpm;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;

public class PrepareProcessListener implements ExternalActivityBehaviour {
	
//	private static final Logger log = new PrepareProcessListener()

	@Override
	public void execute(ActivityExecution execution) throws Exception {
		LinkedList<Long> userzy = new LinkedList<Long>();
		try{
			Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			
			List<Long> ids = (List<Long>) fm.getKey("MPK");
			userzy.addAll(ids);
//			for (Long id : ids) 
//			{
//				Map<String, Object> mpk = DwrDictionaryFacade.dictionarieObjects.get("MPK").getValues(id.toString());
//				EnumValues ev = (EnumValues) mpk.get("MPK_LOKALIZACJA");
//				if(ev.getSelectedId() != null && ev.getSelectedId().length()>0){
//					Long mpkid = Long.parseLong(ev.getSelectedId());
//					EnumItem lokalizacja = DataBaseEnumField.getEnumItemForTable("dsg_mosty_kod_mpk", mpkid.intValue());
//					Long userId = Long.parseLong(lokalizacja.getRefValue());
//					userzy.add(userId);
//				}
//				userzy.add(id);
//			}
		}catch(Exception e){
//			log.error(e.getMessage(), e);
		}
		
//		execution.setVariable("ids", userzy);
		execution.setVariable("ids", userzy);
		execution.setVariable("count", userzy.size());
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

}
