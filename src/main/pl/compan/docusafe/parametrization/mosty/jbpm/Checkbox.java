package pl.compan.docusafe.parametrization.mosty.jbpm;

import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class Checkbox implements ExternalActivityBehaviour {
	private static final long serialVersionUID = 1L;
	protected static Logger log = LoggerFactory.getLogger(Checkbox.class);

	public void execute(ActivityExecution activityExecution) throws Exception {
		long documentId = (Long) activityExecution
				.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY);
		OfficeDocument doc = OfficeDocument.find(documentId);
		FieldsManager fm = doc.getFieldsManager();
		Boolean cn = fm.getBoolean("LAB");
		if (cn != null && cn.toString().equals("true"))
			return;
		cn = fm.getBoolean("DPP");
		if (cn != null && cn.toString().equals("true"))
			return;
		cn = fm.getBoolean("DP");
		if (cn != null && cn.toString().equals("true"))
			return;
		throw new EdmException(
				"Nie wybrano �adnego dzia�u do sprawdzenia specyfikacji.");
	}

	@Override
	public void signal(ActivityExecution execution, String signalName,
			Map<String, ?> parameters) throws Exception {
	}
}
