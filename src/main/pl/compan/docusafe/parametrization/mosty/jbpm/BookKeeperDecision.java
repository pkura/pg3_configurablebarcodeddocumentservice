package pl.compan.docusafe.parametrization.mosty.jbpm;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.polcz.AssigneeToDepartmentManagerDecision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class BookKeeperDecision implements DecisionHandler {
	
	private static final Logger log = LoggerFactory.getLogger(BookKeeperDecision.class);

	@Override
	public String decide(OpenExecution openExecution) {
		try{
			Long docId = Long.valueOf(openExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			
			List<Long> ids = (List<Long>) fm.getKey("MPK");
			
			for (Long id : ids) 
			{
				Map<String, Object> mpk = DwrDictionaryFacade.dictionarieObjects.get("MPK").getValues(id.toString());
				log.error("mpk: " + mpk);
				EnumValues ev = (EnumValues) mpk.get("MPK_LOKALIZACJA");
				log.error("mpkidtest: " + ev);
				log.error("mpkidtest: " + ev.getSelectedId());
				if(ev.getSelectedId() == null || ev.getSelectedId().length()==0) return "book_keepers_mpk";
			}
			return "setCountBeforeFork";
		}catch(Exception e){
			log.error(e.getMessage(), e);
			return "error";
		}
	}
}
