package pl.compan.docusafe.parametrization.mosty.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class IsCorrectionAfterForkDecision implements DecisionHandler {
	
	private static final Logger log = LoggerFactory.getLogger(IsCorrectionAfterForkDecision.class); 

	@Override
	public String decide(OpenExecution openExecution) {
		Integer cor = (Integer) openExecution.getVariable("correction");

		if (cor != null && cor == 11) {
			openExecution.removeVariable("correction");
			openExecution.setVariable("correction", 2);
			openExecution.setVariable("count", 2);
			return "correction";
		}
		return "normal";
	}

}
