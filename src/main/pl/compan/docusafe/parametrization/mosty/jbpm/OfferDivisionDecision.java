package pl.compan.docusafe.parametrization.mosty.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class OfferDivisionDecision  implements DecisionHandler {

	
	private static final Logger log = LoggerFactory.getLogger(OfferDivisionDecision.class);
	
	@Override
	public String decide(OpenExecution arg0) {
		String poprzedni = (String) arg0.getVariable("previousAssignee");
		String aktualny = (String) arg0.getVariable("currentAssignee");
		log.error("^^^^^^^^^^^^^^^^^^^^^^^pop="+poprzedni+" " + aktualny);
		return "error";
	}

}
