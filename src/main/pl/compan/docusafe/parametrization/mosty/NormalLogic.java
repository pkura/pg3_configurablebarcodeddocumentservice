package pl.compan.docusafe.parametrization.mosty;

import com.google.common.collect.Maps;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.PersonDictionary;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.PersonDirectoryAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.*;

public class NormalLogic extends AbstractDocumentLogic
{

	public static String NORMAL_DOCKIND_CN = "normal";
	public static String NORMAL_OUT_DOCKIND_CN = "normal_out";
	public static String NORMAL_INT_DOCKIND_CN = "normal_int";
	
	public static String CHAIRMAN_DIVISION_CN = "chairman";
	public static String CHAIRMAN_AND_SECRETARYS_OFFICE_CN = "chairman,sekretariat";
	public static String SECRETARYS_OFFICE_DIVISION_CN = "sekretariat";

	private static NormalLogic instance;
	protected static Logger log = LoggerFactory.getLogger(NormalLogic.class);
	
	
@Override
public void onEndManualAssignment(OfficeDocument doc, String fromUsername,
		String toUsername) {
	try{
			super.onEndManualAssignment(doc, fromUsername, toUsername);
			HashMap<String, Object> toRel = new HashMap<String, Object>();
			toRel.put("STATUS",25);
			doc.getDocumentKind().setOnly(doc.getId(), toRel);
	}catch(Exception e){
		log.error(e.getMessage(), e);
	}
}
	public static NormalLogic getInstance()
	{
		if (instance == null)
			instance = new NormalLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));
		this.setUpPermission(document, perms);
	}

	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		log.info("type: {}", type);
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("TYPE", type);
		values.put("DOC_DATE", new Date());
		if (type == 3)
		{
			values.put("TERMIN_REALIZACJI", new Date());
		}
		for (Field f : fm.getFields())
		{
			if (f.getDefaultValue() != null)
			{
				values.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
			}
		}
//		if(fm.containsField("STATUS"))
//		{
		log.error("TEST !!!!!!!!!!");
		values.put("STATUS", 10);
		values.put("NADAWCA", DSApi.context().getDSUser().getId());
//		}
		fm.reloadValues(values);
	}
	
	public Map<String, String> setInitialValuesForReplies(Long inDocumentId) throws EdmException
	{
		Map<String, String> dependsDocs = new HashMap<String, String>();
		Long recipientChildId = OfficeDocument.find(inDocumentId).getSender().getId();
		Long recipientBaseId = OfficeDocument.find(inDocumentId).getSender().getBasePersonId();
		Long recipientId;
		
		recipientId = (recipientBaseId != null && Person.findIfExist(recipientBaseId) != null)?recipientBaseId:recipientChildId;

		dependsDocs.put("RECIPIENT", String.valueOf(recipientId));
		//czy user ma swoj dzial
		if (DSApi.context().getDSUser().getDivisionsWithoutGroup().length>0) {
			String sender = "u:"+DSApi.context().getDSUser().getName();
			sender+=(";d:"+DSApi.context().getDSUser().getDivisionsWithoutGroup()[0].getGuid());
			dependsDocs.put("SENDER_HERE", sender);	
		}
		return dependsDocs;
	}

	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Projekt/Budowa");

		if (fm.getEnumItem("BUDOWA") != null)
			folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getEnumItem("BUDOWA").getTitle()));
		else
			folder = folder.createSubfolderIfNotPresent("Numer projektu nieokre�lony");

		if (fm.getEnumItem("ETAP") != null)
			folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getEnumItem("ETAP").getTitle()));
		else
			folder = folder.createSubfolderIfNotPresent("Nazwa etapu nieokre�lona");
		
		//folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(document.getCtime()));
		
		if (fm.getEnumItem("KATALOG") != null)
			folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getEnumItem("KATALOG").getTitle()));
		else
			folder = folder.createSubfolderIfNotPresent("Nazwa katalogu nieokre�lona");
		
		//folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(document.getCtime()));
		
		document.setFolder(folder);
	}

	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
		try
		
		{
			Map<String, Object> map = Maps.newHashMap();

				String dockindCn = document.getDocumentKind().getCn();
				
				if(dockindCn.equalsIgnoreCase(NORMAL_INT_DOCKIND_CN))
				{
					map.put(ASSIGN_USER_PARAM, document.getAuthor());
					
					HashMap<String, Object> toRel = new HashMap<String, Object>();
					toRel.put("STATUS", 10);
					document.getDocumentKind().setOnly(document.getId(), toRel);
				}
				else if(dockindCn.equalsIgnoreCase(NORMAL_DOCKIND_CN))
				{
					map.put(ASSIGN_DIVISION_GUID_PARAM, CHAIRMAN_DIVISION_CN);
					
					HashMap<String, Object> toRel = new HashMap<String, Object>();
					toRel.put("STATUS", 200);
					document.getDocumentKind().setOnly(document.getId(), toRel);
				}
				else if(dockindCn.equalsIgnoreCase(NORMAL_OUT_DOCKIND_CN)){
					FieldsManager fm = document.getFieldsManager();
					String doPrezesa = (String) fm.getValue("DO_PREZESA");
					if(doPrezesa!=null && doPrezesa.equals("tak"))
					{
						map.put(ASSIGN_DIVISION_GUID_PARAM, CHAIRMAN_DIVISION_CN);
					}
					else
					{
						map.put(ASSIGN_DIVISION_GUID_PARAM, SECRETARYS_OFFICE_DIVISION_CN);
					}
					fm.getKey("DO_PREZESA");
					
					
					HashMap<String, Object> toRel = new HashMap<String, Object>();
					toRel.put("STATUS", 10);
					document.getDocumentKind().setOnly(document.getId(), toRel);
				}
				
			//}
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
				if (AvailabilityManager.isAvailable("addToWatch"))
					DSApi.context().watch(URN.create(document));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}
	
	@Override
 	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException {
		super.onSaveActions(kind, documentId, fieldValues);
		OfficeDocument document = OfficeDocument.find(documentId);
		FieldsManager fm = document.getFieldsManager();
		if(document.getDocumentKind().getCn().equals("normal_out")) 
		{
			if(fieldValues.containsKey("NUMER_KO") && fieldValues.get("NUMER_KO")!=null)
			{
				try
				{
					Integer nko = Integer.parseInt(((String) fieldValues.get("NUMER_KO")));
					document.setOfficeNumber(nko);
					
				}
				catch(Exception e)
				{
					log.error(e.getMessage(), e);
				}
			}
		}
//		else if(document.getDocumentKind().getCn().equals("normal"))
//		{
//			if(fieldValues.containsKey("STATUS_REALIZACJI"))
//			{
//				try
//				{
////					Integer nko = Integer.parseInt(((String) fieldValues.get("NUMER_KO")));
////					document.setOfficeNumber(nko);
//					fieldValues.put("STATUS_REALIZACJI", 890);
//					
//				}
//				catch(Exception e)
//				{
//					log.error(e.getMessage(), e);
//				}
//			}
//		}
		else if(document.getDocumentKind().getCn().equals("normal"))
		{
			if(fieldValues.containsKey("STATUS_REALIZACJI"))
			{
				try
				{
//					Integer nko = Integer.parseInt(((String) fieldValues.get("NUMER_KO")));
//					document.setOfficeNumber(nko);
//					fieldValues.put("STATUS_REALIZACJI", 890);
					
				}
				catch(Exception e)
				{
					log.error(e.getMessage(), e);
				}
			}
		}
	}

	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}
	/*
	 * private final static OcrSupport OCR_SUPPORT =
	 * OcrUtils.getFileOcrSupport( new File(Docusafe.getHome(), "ocr-dest"),
	 * new File(Docusafe.getHome(), "ocr-get"), Collections.singleton("tiff")
	 * );
	 * 
	 * @Override public OcrSupport getOcrSupport() { return OCR_SUPPORT; }
	 */
}
