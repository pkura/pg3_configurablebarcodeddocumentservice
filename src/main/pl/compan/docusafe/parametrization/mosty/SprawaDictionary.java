package pl.compan.docusafe.parametrization.mosty;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.LinkValue;
import pl.compan.docusafe.core.dockinds.field.LinkField;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SprawaDictionary extends DwrDictionaryBase{

	private static final Logger log = LoggerFactory.getLogger(SprawaDictionary.class);
	
/**	public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset) throws EdmException
	{
		List<Map<String, Object>> ret = Lists.newArrayList();
		String documentId = "";
		if (!(values.containsKey("id") && !(values.get("id").getData().toString()).equals("")))
			return ret;
			
		documentId = values.get("id").getData().toString();
		
		Statement stat = null;
		ResultSet result = null;
		String dictionaryName = getName();
		boolean contextOpened = true;
		try
		{
			if (!DSApi.isContextOpen())
			{
				DSApi.openAdmin();
				contextOpened = false;
			}
			stat = DSApi.context().createStatement();

			String sql = createSQL(values);
			
			log.info("select: " + sql);
			result = stat.executeQuery(sql);
			while (result.next())
			{
				Map<String, Object> row = Maps.newLinkedHashMap();
				Long statusDokumentu = result.getLong(1);
				Long budowa = result.getLong(2);
				Long etap = result.getLong(3);
				Long katalog = result.getLong(4);
				
				row.put("id", documentId);

				row.put(dictionaryName + "_STATUS_REALIZACJI", statusDokumentu);
				row.put(dictionaryName + "_BUDOWA", budowa);
				row.put(dictionaryName + "_ETAP", etap);
				row.put(dictionaryName + "_KATALOG", katalog);
				ret.add(row);
			}
			
		}
		catch (Exception ie)
		{
			log.error(ie.getMessage(), ie);
		}
		finally
		{
			DSApi.context().closeStatement(stat);
			if (!contextOpened && DSApi.isContextOpen())
				DSApi.close();
		}
		return ret;
	}  */
	
/**	public Map<String, Object> getValues(String id) throws EdmException
	{
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

		Map<String, Object> fieldsValues = new HashMap<String, Object>();
		try
		{
			Map<String, FieldData> values = new HashMap<String, FieldData>();
			if (id != null)
			{
				FieldData idData = new FieldData();
				idData.setData(id);
				idData.setType(Field.Type.STRING);
				values.put("id", idData);
				result = search(values, 1, 2);
			}

			for (pl.compan.docusafe.core.dockinds.field.Field field : getOldField().getFields())
			{
				if (field.getCn().equals("BUDOWA_1") || field.getCn().equals("STATUS_REALIZACJI_1") || field.getCn().equals("ETAP_1"))
				{
					Map<String, Object> enumVal = new HashMap<String, Object>();
					if (result.size() > 0 && result.get(0).containsKey(getName() + "_"+field.getCn().replace("_1", "")))
					{
						fieldsValues.put(field.getCn(), result.get(0).get(getName() + "_"+field.getCn().replace("_1", "")));
					}
					if (result.size() == 0)
					{
						enumVal.put(getName() + "_"+field.getCn().replace("_1", ""), field.getEnumItemsForDwr(fieldsValues));
						result.add(enumVal);
					}
					else
					{
						result.get(0).put(getName() + "_"+field.getCn().replace("_1", ""), field.getEnumItemsForDwr(fieldsValues));
					}
				}
				else if (field.getCn().equals("DSG_LEASINGG_1") && id != null)
				{
					Map<String, Object> enumVal = new HashMap<String, Object>();

					pl.compan.docusafe.core.dockinds.field.Field linkField  =  (LinkField)field;
					String label = "Przejd�";
					if (result.size() == 0)
					{
						enumVal.put(getName() + "_DSG_LEASINGG", new LinkValue(label, ((LinkField) linkField).getLogicField().createLink(id)));
						result.add(enumVal);
					}
					else
						result.get(0).put(getName() + "_DSG_LEASINGG", new LinkValue(label, ((LinkField) linkField).getLogicField().createLink(id)));
				}
			}
			if (result.size() == 0)
				return new HashMap<String, Object>();
			return result.get(0);
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
			return null;
		}
	} */
	
	public static String createSQL(Map<String, FieldData> values) {
		StringBuilder select = new StringBuilder("SELECT STATUS_REALIZACJI,BUDOWA,ETAP,KATALOG");
		StringBuilder from = new StringBuilder("FROM dsg_mosty_zamowienia");
		StringBuilder where = new StringBuilder("WHERE");
		StringBuilder order = new StringBuilder("ORDER BY ");

		if (values.containsKey("BUDOWA") && values.get("BUDOWA").getData() != null) {
			select.append("BUDOWA = " + values.get("BUDOWA").getData());
		}
		if (values.containsKey("ETAP") && values.get("ETAP").getData() != null) {
			select.append(" AND ETAP = " + values.get("ETAP").getData());
		} else {
			
		}
		if (values.containsKey("KATALOG") && values.get("KATALOG").getData() != null) {
			select.append(" AND KATALOG = " + values.get("KATALOG").getData());
		}

		return select + " " + from + " " + where + " " + order;
	}
	
}
