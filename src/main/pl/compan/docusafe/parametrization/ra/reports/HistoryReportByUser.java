package pl.compan.docusafe.parametrization.ra.reports;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.tools.*;
import java.io.File;
import java.io.IOException;
import java.sql.*;
/**
 * Raport historyczny zawierajacy aktywnosc uzytkownikow
 * @author wkutyla
 * Pobierane s� wszystkie pisma, ktore byly przez uzytkownika zakonczone pomi�dzy DR-60 a DR (na podstawie historii dekretacji)
 * Dla dokumentow tych okreslam:
		- Liczbe zakonczonych dokumentow w tym czasie
		- sredni czas kt�ry uplynal od importu pisma dokumentu do zakonczenia procesu
	W historii dekretacji poszukujemy zdarzenie w kt�rym u�ytkownik po raz pierwszy przyjal pismo na swojej liscie (zaakceptowal). 
	Raport zawiera sredni czas pomiedzy tym momentem a skonczeniem pracy
 */
public class HistoryReportByUser extends Report 
{
	static final Logger log = LoggerFactory.getLogger(HistoryReportByUser.class);
	
	private Map<String, UserStatisticBean> users = new TreeMap<String,UserStatisticBean>();
	
	@Override
	public void doReport() throws Exception 
	{
		log.trace("ustalamy liste uzytkownikow");
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		//for (Iterator)
		try
		{
			SimpleSQLHelper sqlHelper = new SimpleSQLHelper();
			String reportDate = "";
			if (this.parametersMap.get("report_date")!=null)
			{
				reportDate = parametersMap.get("report_date").getValueAsString();
			}
			else
			{
				java.util.Date now = new java.util.Date();
				reportDate = pl.compan.docusafe.util.DateUtils.formatJsDate(now);
			}
			
			//StringBuffer query = new StringBuffer();
			///query.append("select * from dsw_task_history_entry where 1 =1 ");
			//query.append("select * from dso_document_asgn_history where finished = 1 and sourceUser is not null");
			//ps = DSApi.context().prepareStatement(query.toString());
			StringBuilder sql = new StringBuilder();
			DockindHelper helper = new DockindHelper();
			helper.setIdOnly(false); // zamiast dokumentId dostane *
			PsParameterContainer container = helper.getDockindSubquery("rockwell", params,3);
			sql.append("select * from ap_report where process_finish_date >= ? and process_finish_date < ? ");
			if (container.isOn())
			{
				sql.append(" and document_id in (");
				sql.append(container.getSql());
				sql.append(")");
			}
			ps = DSApi.context().prepareStatement(sql.toString());
			ps.setDate(1, sqlHelper.parseDate(reportDate,-60));
			ps.setDate(2, sqlHelper.parseDate(reportDate,1));
			if (container.isOn())
			{
				helper.setUpPreparedStatement(ps, container);
			}							
							
			ResultSet rs = ps.executeQuery();
			while (rs.next())
			{
				
				String login = null;
				Long documentId = rs.getLong("document_id");
				log.trace("documentId={}",documentId);
				if (rs.getTimestamp("document_date")==null)
				{
					// nie policzymy takiego dokumentu
					continue;
				}
				Timestamp documentTimestamp = rs.getTimestamp("document_date");
				long documentCtime = documentTimestamp.getTime();
				
				long finishTime = 0;
				
				
				
				ps1 = DSApi.context().prepareStatement("select * from dso_document_asgn_history where finished = 1 and document_id = ? order by ctime desc");
				ps1.setLong(1, documentId);
				ResultSet rs1 = ps1.executeQuery();
				if (rs1.next())
				{
					login = rs1.getString("sourceuser");
					
					Timestamp finishTimestamp = rs1.getTimestamp("ctime");
					finishTime = finishTimestamp.getTime();
					if (log.isTraceEnabled())
					{
						log.trace("finishDate={}",finishTimestamp);
						log.trace("finishUser={}",login);
						log.trace("documentCtime={}",documentTimestamp);
					}
				}
				rs1.close();
				DSApi.context().closeStatement(ps1);
				
				if (login==null)
				{
					//nie znaleziono usera, ktory skonczyl zadanie
					continue;
				}				
												
				ps1 = DSApi.context().prepareStatement("select * from dso_document_asgn_history where accepted = 1 and targetUser = ? and document_id = ? order by ctime");
				ps1.setString(1, login);
				ps1.setLong(2, documentId);
				rs1 = ps1.executeQuery();
				
				if (rs1.next())
				{
					Timestamp acceptTimestamp = rs1.getTimestamp("ctime");
					long taskAcceptTime = acceptTimestamp.getTime();
					log.trace("acceptTimestamp={}",acceptTimestamp);
					UserStatisticBean stat = getOrCreate(login);
					stat.countDocument(documentId,documentCtime, taskAcceptTime, finishTime);
				}
				else
				{
					log.warn("brak mozliwosci zbudowania statystyki dla documentId={}",documentId);
				}
				rs1.close();
				DSApi.context().closeStatement(ps1);
			}
			rs.close();
			dumpResults();
		}
		catch (Exception e)
		{
			//log.error("",e);
			//wyjatek powinien byc logowany wyzej
			throw new ReportException(e);
		}
		finally 
		{
			DSApi.context().closeStatement(ps);		
			//DSApi.context().closeStatement(ps1);	
		}
		
	}

	protected void dumpResults() throws Exception
	{
		// --- inicuje dumpery
		UniversalTableDumper dumper = new UniversalTableDumper(); 
		CsvDumper csvDumper = new CsvDumper();
		File csvFile = new File(this.getDestination(), "user_history_report.csv");			
		csvDumper.openFile(csvFile);
		dumper.addDumper(csvDumper);
		
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "user_history_report.xls");	
		poiDumper.openFile(xlsFile);
		dumper.addDumper(poiDumper);
		// ---- koniec inicjowania dumperow
		dumpHeader(dumper);
		
		
		Iterator<String> it = users.keySet().iterator();
		while (it.hasNext())
		{
			String user = it.next();
			UserStatisticBean stat = users.get(user);
			stat.countStatistic();
			dumpLine(dumper,stat);
		}
		dumper.closeFileQuietly();
	}
	/**
	 * Metoda zrzuca naglowek raportu
	 * @param dumper
	 * @throws IOException
	 */
	protected void dumpHeader(AbstractTableDumper dumper) throws IOException
	{
		dumper.newLine();
		dumper.addText("User Login");
		dumper.addText("Tasks sample size (last 60 days)");
		dumper.addText("Document proceesing time [days]");
		dumper.addText("User tasklist duration [hours]");
		dumper.dumpLine();
	}
	/**
	 * Zrzuca jedna linie raportu
	 * @param dumper
	 * @param stat
	 * @throws IOException
	 */
	protected void dumpLine(AbstractTableDumper dumper,UserStatisticBean stat) throws IOException
	{
		dumper.newLine();
		dumper.addText(stat.getLogin());
		dumper.addLong(stat.getProcessedDocuments());
		dumper.addMoney(stat.getAverangeTotalDuration());
		dumper.addMoney(stat.getAverangeTaskListDuration());
		dumper.dumpLine();
	}


	/**
	 * Metoda pobiera statystyke dla danego uzytkownika
	 * Jesli nie ma statystyki inicjuje pusta
	 * @param login
	 * @return
	 */
	protected UserStatisticBean getOrCreate(String login)
	{
		UserStatisticBean resp = users.get(login);
		if (resp==null)
		{
			resp = new UserStatisticBean();
			resp.setLogin(login);
			users.put(login, resp);
		}
		return resp;
	}
}
