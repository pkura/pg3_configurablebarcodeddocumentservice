package pl.compan.docusafe.parametrization.ra.reports;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.reports.*;
import pl.compan.docusafe.reports.tools.*;


public class APReport extends Report
{
	protected static final Logger log = LoggerFactory.getLogger(APReport.class);
	
	/**
	 * Czy uzytkownik ma dostep do wszystkich dokumentow AP
	 */
	protected boolean fullApAccess = false;
	
	protected Set<String> allowedBu = new HashSet<String>();
	/**
	 * Czy zwracamy tylko dupikaty
	 * @return
	 */	
	protected boolean duplicatedOnly()
	{
		return false;
	}
	
	@Override
	public void doReport() throws Exception
	{
		configureApPermissions();
		Iterator<ReportParameter> it = getParams().iterator();		
		while (it.hasNext())
		{
			ReportParameter param = it.next();
			log.error("Param {}={}",param.getDockindCn(),param.getValue());
		}
		try
		{
			PreparedStatement ps = null;
			UniversalTableDumper dumper = new UniversalTableDumper(); 
			
			try
			{
				// --- inicuje dumpery
				CsvDumper csvDumper = new CsvDumper();
				File csvFile = new File(this.getDestination(), "report.csv");			
				csvDumper.openFile(csvFile);
				dumper.addDumper(csvDumper);
				
				XlsPoiDumper poiDumper = new XlsPoiDumper();
				File xlsFile = new File(this.getDestination(), "report.xls");	
				poiDumper.openFile(xlsFile);
				dumper.addDumper(poiDumper);
				// ---- koniec inicjowania dumperow
				dumpHeader(dumper);
				DockindHelper helper = new DockindHelper();
				
				
				PsParameterContainer container = helper.getDockindSubquery("rockwell", params,1);
				StringBuilder query = new StringBuilder("select * from ap_report where 1 = 1 ");
				if (duplicatedOnly())
				{
					query.append(" and duplicated_document is not null ");
				}
				if (container.isOn())
				{
					query.append(" and document_id in (");
					query.append(container.getSql());
					query.append(")");
				}
				Collection<Object> psParams = new Vector<Object>();
				for (ReportParameter reportParam : params)
				{
					if (reportParam.getValue()!=null)
					{
						if (reportParam.getFieldCn().equals("VENDOR_ID"))
						{
							query.append(" and VENDOR_ID like ? ");
							psParams.add("%" + reportParam.getValue() + "%");
						}
						else if (reportParam.getFieldCn().equals("VENDOR_NAME"))
						{
							query.append(" and VENDOR_NAME like ? ");
							psParams.add("%" +  reportParam.getValueAsString().toUpperCase() + "%");
						}
					}
				}
				query.append(" order by document_id");
				if (log.isDebugEnabled())
				{
					log.debug(query.toString());
				}
				ps = DSApi.context().prepareStatement(query.toString());
				helper.setUpPreparedStatement(ps, container);
				int i = container.getLastBinding();
				for (Object par : psParams)
				{					
					i++;
					log.trace("parametr {}={}",i,par);
					ps.setObject(i, par);
				}
				ResultSet rs = ps.executeQuery();
				while (rs.next())
				{
					if (isPermittedBU(rs.getString("business_unit")))
					{
						dumper.newLine();
						setDumperByAPResulset(dumper,rs);
						dumper.dumpLine();
					}
				}
				rs.close();
				
				
			}
			finally 
			{
				DSApi.context().closeStatement(ps);
				dumper.closeFileQuietly();
			}
		}
		catch (Exception e)
		{
			log.error("",e);
			throw new ReportException(e);
		}
	}
	
	/**
	 * Metoda generuje naglowek - pierwsza linije raportu
	 * @param dumper
	 * @throws IOException
	 */
	protected void dumpHeader(AbstractTableDumper dumper) throws IOException
	{
		dumper.newLine();
		dumper.addText("document_id");
		dumper.addText("document_date");
		dumper.addText("document_type");
		dumper.addText("document_name");
		dumper.addText("invoice_number");
		dumper.addText("vendor_name");
		dumper.addText("vendor_id");
		dumper.addText("business_unit");
		dumper.addText("invoice_receive_date");
		dumper.addText("invoice_date");
		dumper.addText("gross_amount");
		dumper.addText("net_amount");
		dumper.addText("gross_amount");
		dumper.addText("vat_amount");
		dumper.addText("currency");
		dumper.addText("status");
		dumper.addText("bar_code");
		dumper.addText("registr_no");
		dumper.addText("created_by");
		dumper.addText("voucher_number");
		dumper.addText("eva_number");
		dumper.addText("eva_receive_date");
		dumper.addText("eva_finish_date");
		dumper.addText("process_finish_user");
		dumper.addText("process_finish_date");
		dumper.addText("original_document_id");		
		dumper.dumpLine();
	}
	
	protected void configureApPermissions() throws EdmException 	
	{
		if (reportOwner.isAdmin())
		{
			fullApAccess = true;			
		}
		DSDivision[] groups = reportOwner.getDSGroups();
		for (DSDivision group : groups)
		{
			log.trace("posiada uprawnienie:"+group.getGuid());
			if (group.getGuid().equalsIgnoreCase("ROCKWELL_READ"))
			{
				fullApAccess = true;
			}
			if (group.getGuid().indexOf("BUSINESS_UNIT_")>=0 && group.getGuid().indexOf("_READ")>0)
			{
				String pom = group.getGuid();
				pom = pom.replace("BUSINESS_UNIT_", "");
				pom = pom.replace("_READ", "");
				log.trace(pom);
				allowedBu.add(pom);
			}
		}
	}
	
	/**
	 * Czy uzytkownik jest dopuszczony do dokumentow z danego BU
	 * @param buCode
	 * @return
	 */
	protected boolean isPermittedBU(String buCode)	
	{
		
		return fullApAccess || allowedBu.contains(buCode);
	}
	
	
	protected void setDumperByAPResulset(AbstractTableDumper dumper, ResultSet rs) throws SQLException
	{
		dumper.addText(rs.getString("document_id"));
		dumper.addTimestamp(rs.getTimestamp("document_date"));
		dumper.addText(rs.getString("document_type"));
		dumper.addText(rs.getString("category_name"));
		dumper.addText(rs.getString("invoice_number"));
		dumper.addText(rs.getString("vendor_name"));
		dumper.addText(rs.getString("vendor_id"));
		dumper.addText(rs.getString("business_unit"));
		dumper.addDate(rs.getDate("invoice_receive_date"));
		dumper.addDate(rs.getDate("invoice_receive_date"));
		dumper.addDate(rs.getDate("invoice_date"));
		dumper.addMoney(rs.getDouble("gross_amount"));
		dumper.addMoney(rs.getDouble("net_amount"));
		dumper.addMoney(rs.getDouble("vat_amount"));
		dumper.addText(rs.getString("currency"));
		dumper.addText(rs.getString("status"));
		dumper.addText(rs.getString("bar_code"));
		dumper.addText(rs.getString("registr_no"));
		dumper.addText(rs.getString("created_by"));
		dumper.addText(rs.getString("voucher_number"));
		dumper.addText(rs.getString("eva_number"));
		dumper.addTimestamp(rs.getTimestamp("eva_receive_date"));
		dumper.addTimestamp(rs.getTimestamp("eva_finish_date"));
		dumper.addText(rs.getString("process_finish_user"));
		dumper.addTimestamp(rs.getTimestamp("process_finish_date"));
		dumper.addText(rs.getString("duplicated_document"));
	}
}
