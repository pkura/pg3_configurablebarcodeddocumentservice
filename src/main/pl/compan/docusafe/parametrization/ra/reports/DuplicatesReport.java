package pl.compan.docusafe.parametrization.ra.reports;


/**
 * Bardzo podobny raport do APReport, ale same duplikaty
 * @author wkutyla
 *
 */
public class DuplicatesReport extends APReport
{
	
	@Override
	protected boolean duplicatedOnly()
	{
		return true;
	}
	
}
