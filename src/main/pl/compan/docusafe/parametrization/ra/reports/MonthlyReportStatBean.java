package pl.compan.docusafe.parametrization.ra.reports;

import java.util.*;
/**
 * Bean grupujacy statystyke dla raportu MonthlyReportByUser
 * @author wkutyla
 *
 */
public class MonthlyReportStatBean 
{
	/**
	 * Login uzytkownika
	 */
	private String login;
	/**
	 * Dokument ktory wszedl na liste zadan
	 */
	private Set<Long> enteredDocuments = new HashSet<Long>();
	
	/**
	 * Rejestruje wejscie dokumentu na liste zadan
	 * @param documentId
	 */
	public void registerDocumentEntry(Long documentId)
	{
		enteredDocuments.add(documentId);
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getLogin() {
		return login;
	}
}
