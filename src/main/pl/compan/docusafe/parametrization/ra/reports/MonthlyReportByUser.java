package pl.compan.docusafe.parametrization.ra.reports;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.tools.AbstractTableDumper;
import pl.compan.docusafe.reports.tools.CsvDumper;
import pl.compan.docusafe.reports.tools.SimpleSQLHelper;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Raport miesiecznej aktywnosci uzytkownikow:
 * raport by user z informacja ile splynelo dokumentow na tasklisty i ile z tego zostalo zaprocesowanych 
 * (czy jesli jest kilka osob w ramach tej samej grupy i dostaja te same dokumenty jest mozliwosc zeby te zaprocesowane 
 * rozdzielic juz tak zeby pojawialy sie pod nazwiskiem osob ktore rzeczywiscie przerobily dany dokument,
 *  a nie ze splynely na listy wszystkich uzytkownikow a tylko u jednej sie pojawia jako zaprocesowane - 
 *  to nam nieco zniekszalci obraz - idea jest zeby moc sprawdzic ile dokumentow splynelo i ile z tego 
 *  zostalo zaksiegowanych a ile dalej wisi i jak dlugo wisi na tasklistach).
 * @author wkutyla
 *
 */
public class MonthlyReportByUser extends Report 
{
	static final Logger log = LoggerFactory.getLogger(MonthlyReportByUser.class);
	private Map<String,MonthlyReportStatBean> userStats = new HashMap<String,MonthlyReportStatBean>();
	
	
	@Override
	public void doReport() throws Exception 
	{
		UniversalTableDumper dumper = null;
		PreparedStatement ps = null;
		try
		{
			SimpleSQLHelper sqlHelper = new SimpleSQLHelper();
			String dateFrom = "";
			if (this.parametersMap.get("report_date_from")!=null)
			{
				dateFrom = parametersMap.get("report_date_from").getValueAsString();
			}
			String dateTo = "";
			if (this.parametersMap.get("report_date_to")!=null)
			{
				dateTo = parametersMap.get("report_date_to").getValueAsString();
			}
			String[] users = null;
			if (this.parametersMap.get("user")!=null)
			{
				if(this.parametersMap.get("user").getValue() instanceof String)
				{
					users = new String[1];
					users[0] = (String)this.parametersMap.get("user").getValue();
				}
				else
				{
					users = (String[]) parametersMap.get("user").getValue();
				}
			}
		
			dumper = new UniversalTableDumper(); 
			// --- inicuje dumpery
			CsvDumper csvDumper = new CsvDumper();
			File csvFile = new File(this.getDestination(), "report.csv");			
			csvDumper.openFile(csvFile);
			dumper.addDumper(csvDumper);
			
			XlsPoiDumper poiDumper = new XlsPoiDumper();
			File xlsFile = new File(this.getDestination(), "report.xls");	
			poiDumper.openFile(xlsFile);
			dumper.addDumper(poiDumper);
			// ---- koniec inicjowania dumperow
			dumpHeader(dumper);
			try
			{
				for (String user : users)
				{
					Integer start = countTaskOnUserList(user, dateFrom);
					Integer in = countInTaskOnUserList(user, dateFrom,dateTo);
					Integer out = countOutTaskOnUserList(user,dateFrom,dateTo);
					Integer close = countCloseTaskByUser(user, dateFrom, dateTo);
					Integer stop = countTaskOnUserList(user, dateTo);		
					dumper.newLine();
					dumper.addText(DSUser.findByUsername(user).asFirstnameLastname());
					dumper.addInteger(start);
					dumper.addInteger(in);
					dumper.addInteger(out);
					dumper.addInteger(close);
					dumper.addInteger(stop);
					dumper.dumpLine();
				}
			}
			catch (Exception e) {
				LoggerFactory.getLogger("mariusz").debug("",e);
			}
			
			
			/**
			 * Szukam wszystkich dokumentow ktore
			 * Zaczely sie przed data dateTo + 1
			 * Skonczyly sie po dacie dateFrom lub sie nie skonczyly
			 */
			/*
			ps = DSApi.context().prepareStatement("select * from ap_report where document_date < ? and (process_finish_date is null or process_finish_date >= ? ");
			ps.setDate(1, sqlHelper.parseDate(dateTo, 1));
			ps.setDate(2, sqlHelper.parseDate(dateFrom));
			ResultSet rs = ps.executeQuery();
			while (rs.next())
			{
				long documentId = rs.getLong("document_id");
				
				log.trace("zadanie documentId={}",documentId);
				countDocumentTaskListEntry(documentId);
				boolean finished = false;
				if (rs.getTimestamp("process_finish_date ")!=null)
				{
					finished = true;
				}
				
				//okreslamy kto zakonczyl dokument
			}*/
		}
		finally
		{
			DSApi.context().closeStatement(ps);
			dumper.closeFileQuietly();
		}
		
		
	}

	/**
	 * Metoda generuje naglowek - pierwsza linije raportu
	 * @param dumper
	 * @throws IOException
	 */
	protected void dumpHeader(AbstractTableDumper dumper) throws IOException
	{
		dumper.newLine();
		dumper.addText("User");
		dumper.addText("Initial number of document");
		dumper.addText("In documnet");
		dumper.addText("Out document");
		dumper.addText("Closed document");
		dumper.addText("Final number of document");		
		dumper.dumpLine();
	}
	
	/**@deprecated
	 * Metoda zlicza osoby, do ktorych dokument zostal przypisany
	 * @param documentId
	 * @throws SQLException
	 */
	protected void countDocumentTaskListEntry(Long documentId) throws Exception
	{
		PreparedStatement ps = null;
		try
		{
			ps = DSApi.context().prepareStatement("select * from dso_document_asgn_history where accepted = 1 and document_Id = ? order by posn");
			ps.setLong(1, documentId);
			ResultSet rs = ps.executeQuery();
			while (rs.next())
			{
				String login = rs.getString("targetuser");
				if (login !=null)
				{
					MonthlyReportStatBean stat = getStatBean(login);
					stat.registerDocumentEntry(documentId);
				}
			}
			rs.close();
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
	}
	
	/***
	 * Zwraca liczbe zadan jaka znajduje sie na listach uzytkownika w danym dniu 
	 * Zlicza tylko zadnia ktore sa zadekretowane na uzytkownika, 
	 * a jesli zadanie bylo zadekretowane na dzial to tylko te ktore zostaly przez niego zaakceptowane
	 * @param user
	 * @return 
	 * @throws SQLException
	 * @throws EdmException
	 */
	protected Integer countTaskOnUserList(String user,String date) throws SQLException, EdmException
	{
		LoggerFactory.getLogger("mariusz").debug("String user,String date "+user+" "+date);
		SimpleSQLHelper sqlHelper = new SimpleSQLHelper();
		PreparedStatement ps = null;
		Integer resp = 0;
		try
		{
			ps = DSApi.context().prepareStatement("select count(distinct a1.document_id)  from dso_document_asgn_history a1 where" +
					" (select top 1 targetuser from dso_document_asgn_history a2 where" +
							" a2.ctime < ? and a2.document_id = a1.document_id order by posn desc) = ? " +
					"and " +
					" (select top 1 finished from dso_document_asgn_history a3 where " +
							"a3.ctime < ? and a3.document_id = a1.document_id order by posn desc) = 0 ");
			LoggerFactory.getLogger("mariusz").debug("select count(distinct a1.document_id)  from dso_document_asgn_history a1 where" +
					" (select top 1 targetuser from dso_document_asgn_history a2 where" +
					" a2.ctime < ? and a2.document_id = a1.document_id order by posn desc) = ? " +
			"and " +
			"(select top 1 finished from dso_document_asgn_history a3 where " +
					"a3.ctime < ? and a3.document_id = a1.document_id order by posn desc) = 0 ");
			ps.setDate(1, sqlHelper.parseDate(date,1));
			LoggerFactory.getLogger("mariusz").debug(sqlHelper.parseDate(date,1));
			ps.setString(2, user);
			LoggerFactory.getLogger("mariusz").debug(user);
			ps.setDate(3, sqlHelper.parseDate(date,1));
			LoggerFactory.getLogger("mariusz").debug(sqlHelper.parseDate(date,1));
			ResultSet rs = ps.executeQuery();
			if(rs.next())
			{
				resp = rs.getInt(1);
			}
			rs.close();
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		return resp;
	}
	
	/***
	 * Zwraca liczbe zadan jakie wp造ne造 na liste uzytkownika w zadanym okresie
	 * Zlicza tylko zadnia ktore sa zadekretowane na uzytkownika, 
	 * a jesli zadanie bylo zadekretowane na dzial to tylko te ktore zostaly przez niego zaakceptowane
	 * @param user
	 * @return 
	 * @throws SQLException
	 * @throws EdmException
	 */
	protected Integer countInTaskOnUserList(String user,String dateFrom,String dateTo) throws SQLException, EdmException
	{
		LoggerFactory.getLogger("mariusz").debug("user,String dateFrom,String dateT "+ user + dateFrom +dateTo);
		SimpleSQLHelper sqlHelper = new SimpleSQLHelper();
		PreparedStatement ps = null;
		Integer resp = 0;
		try
		{
			ps = DSApi.context().prepareStatement("select count(distinct document_id)  from dso_document_asgn_history where ctime >= ? and ctime < ? " +
					" and ( (targetuser = ? and accepted = 1 ) or (targetuser = ? and sourceuser <> ?  and  accepted = 0 and type is null))");
			LoggerFactory.getLogger("mariusz").debug("select count(distinct document_id)  from dso_document_asgn_history where ctime >= ? and ctime < ? " +
					" and ( (targetuser = ? and accepted = 1 ) or (targetuser = 'admin' and accepted = 0 and Type is null))");
			ps.setDate(1, sqlHelper.parseDate(dateFrom));
			LoggerFactory.getLogger("mariusz").debug(sqlHelper.parseDate(dateFrom));
			ps.setDate(2, sqlHelper.parseDate(dateTo,1));
			LoggerFactory.getLogger("mariusz").debug(sqlHelper.parseDate(dateTo,1));
			ps.setString(3, user);
			LoggerFactory.getLogger("mariusz").debug(user);
			ps.setString(4, user);
			LoggerFactory.getLogger("mariusz").debug(user);
			ps.setString(5, user);
			LoggerFactory.getLogger("mariusz").debug(user);
			ResultSet rs = ps.executeQuery();			
			if(rs.next())
			{
				resp = rs.getInt(1);
			}
			rs.close();
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		return resp;
	}
	
	/***
	 * Zwraca liczbe zadan jakie wp造ne造 na liste uzytkownika w zadanym okresie
	 * Zlicza tylko zadnia ktore sa zadekretowane na uzytkownika, 
	 * a jesli zadanie bylo zadekretowane na dzial to tylko te ktore zostaly przez niego zaakceptowane
	 * @param user
	 * @return 
	 * @throws SQLException
	 * @throws EdmException
	 */
	protected Integer countOutTaskOnUserList(String user,String dateFrom,String dateTo) throws SQLException, EdmException
	{
		LoggerFactory.getLogger("mariusz").debug("user,String dateFrom,String dateT "+ user + dateFrom +dateTo);
		SimpleSQLHelper sqlHelper = new SimpleSQLHelper();
		PreparedStatement ps = null;
		Integer resp = 0;
		try
		{
			ps = DSApi.context().prepareStatement("select count(distinct document_id)  from dso_document_asgn_history where ctime >= ? and ctime < ? " +
					"and targetuser <> ? and sourceuser = ? and accepted = 0 and type is null");
			LoggerFactory.getLogger("mariusz").debug("select count(distinct document_id)  from dso_document_asgn_history where ctime >= ? and ctime < ? " +
					"and targetuser <> ? and sourceuser = ? and accepted = 0 and type is null");
			ps.setDate(1, sqlHelper.parseDate(dateFrom));
			LoggerFactory.getLogger("mariusz").debug(dateFrom);
			ps.setDate(2, sqlHelper.parseDate(dateTo,1));
			LoggerFactory.getLogger("mariusz").debug(sqlHelper.parseDate(dateTo,1));
			ps.setString(3, user);
			LoggerFactory.getLogger("mariusz").debug(user);
			ps.setString(4, user);
			LoggerFactory.getLogger("mariusz").debug(user);
			ResultSet rs = ps.executeQuery();			
			if(rs.next())
			{
				resp = rs.getInt(1);
			}
			rs.close();
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		return resp;
	}
	
	
	/***
	 * Zwraca liczbe zadan jakie wp造ne造 na liste uzytkownika w zadanym okresie
	 * Zlicza tylko zadnia ktore sa zadekretowane na uzytkownika, 
	 * a jesli zadanie bylo zadekretowane na dzial to tylko te ktore zostaly przez niego zaakceptowane
	 * @param user
	 * @return 
	 * @throws SQLException
	 * @throws EdmException
	 */
	protected Integer countCloseTaskByUser(String user,String dateFrom,String dateTo) throws SQLException, EdmException
	{
		LoggerFactory.getLogger("mariusz").debug("user,String dateFrom,String dateT "+ user + dateFrom +dateTo);
		SimpleSQLHelper sqlHelper = new SimpleSQLHelper();
		PreparedStatement ps = null;
		Integer resp = 0;
		try
		{
			ps = DSApi.context().prepareStatement("select count(distinct document_id)  from dso_document_asgn_history where ctime >= ? and ctime < ? " +
					"and sourceuser = ? and finished = 1 and type is null");
			LoggerFactory.getLogger("mariusz").debug("select count(distinct document_id)  from dso_document_asgn_history where ctime >= ? and ctime < ? " +
			"and sourceuser = ? and finished = 1 and type is null");
			ps.setDate(1, sqlHelper.parseDate(dateFrom));
			LoggerFactory.getLogger("mariusz").debug(sqlHelper.parseDate(dateFrom));
			ps.setDate(2, sqlHelper.parseDate(dateTo,1));
			LoggerFactory.getLogger("mariusz").debug(sqlHelper.parseDate(dateTo,1));
			ps.setString(3, user);
			LoggerFactory.getLogger("mariusz").debug(user);
			ResultSet rs = ps.executeQuery();			
			if(rs.next())
			{
				resp = rs.getInt(1);
			}
			rs.close();
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		return resp;
	}
	
	/**
	 * Pobranie lub utworzenie beanu ze statystyka dokumentu
	 * @param login
	 * @return
	 */
	protected MonthlyReportStatBean getStatBean(String login)
	{
		MonthlyReportStatBean stat = userStats.get(login);
		if (stat == null)
		{
			stat = new MonthlyReportStatBean();
			stat.setLogin(login);
			userStats.put(login, stat);
		}
		return stat;
	}
}
