package pl.compan.docusafe.parametrization.ra.reports;

import java.util.*;
/**
 * Bean zbierajacy dane do statystkyki zbiorczej
 * @author wkutyla
 *
 */
public class FullUserStatisticBean 
{
	private Set<Long> ids = new HashSet<Long>();
	
	/**
	 * Login uzytkownika
	 */
	private String login = "";
	
	/**
	 * Liczba dokumentow na poczatku okresu rozliczeniowego 
	 */
	private long documentsOnStart = 0;
	private long documentsOnFinish = 0;

	
	
    
	public String getLogin() 
	{
		return login;
	}

	public void setLogin(String login) 
	{
		this.login = login;
	}


	static final Long DAY_IN_MILLIS = 24l * 60l * 60l * 1000l;

	public void countStatistic()
	{
	}
}
