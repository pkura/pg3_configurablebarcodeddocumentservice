package pl.compan.docusafe.parametrization.ra.reports;

import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.reports.tools.AverangeTimeContainer;
/**
 * Bean zawierajacy statystyke na potrzeby raportu HistoryReportByUser
 * @author wkutyla
 *
 */
public class UserStatisticBean 
{
	static final Logger log = LoggerFactory.getLogger(UserStatisticBean .class);
	private Set<Long> ids = new HashSet<Long>();
	
	private String login = "";

	private long processedDocuments = 0;
	
	/**
	 * Do policzenia totalnego czasu
	 */
	private AverangeTimeContainer totalDurationCounter = new AverangeTimeContainer();
	
	/**
	 * Do policzenia czasu na liscie zadan
	 */
    private AverangeTimeContainer taskListDuration = new AverangeTimeContainer(); 
	
    /**
     * Sredni czas uplywajacy do zakonczenia dokumentu
     */
    private Double averangeTotalDuration = null;
    /**
     * Sredni czas przebywania dokumentu na liscie zadan
     */
    private Double averangeTaskListDuration = null;
    
	public String getLogin() 
	{
		return login;
	}

	public void setLogin(String login) 
	{
		this.login = login;
	}
	
	/**
	 * Zbiera statystyke dla dokumentu, jednoczesnie kontrolujac czy nie ma duplikatu
	 * @param documentId
	 * @param ctime
	 * @param acceptTime
	 * @param finishTime
	 */
	public void countDocument(Long documentId,long ctime, long acceptTime, long finishTime)
	{
		log.trace("zliczamy dokument {}",documentId);
		if (ids.contains(documentId))
		{
			log.trace("juz policzony anulowanie");
			return;
		}
		if (log.isTraceEnabled())
		{
			long duration = finishTime-ctime;
			log.trace("total duration={}",duration);
			duration = finishTime-acceptTime;
			log.trace("task list duration={}",duration);
		}		
		ids.add(documentId);
		totalDurationCounter.count(ctime,finishTime);				
		taskListDuration.count(acceptTime,finishTime);  			//
		processedDocuments++;
	}

	public void countStatistic()
	{
		if (processedDocuments>0)
		{
			averangeTotalDuration = totalDurationCounter.averangeDuration(AverangeTimeContainer.DAY);
			averangeTaskListDuration =taskListDuration.averangeDuration(AverangeTimeContainer.HOUR);
		}
			
	}
	public long getProcessedDocuments() 
	{
		return processedDocuments;
	}

	public void setProcessedDocuments(long processedDocuments) 
	{
		this.processedDocuments = processedDocuments;
	}


	public Double getAverangeTotalDuration() {
		return averangeTotalDuration;
	}

	public void setAverangeTotalDuration(Double averangeTotalDuration) {
		this.averangeTotalDuration = averangeTotalDuration;
	}

	public Double getAverangeTaskListDuration() {
		return averangeTaskListDuration;
	}

	public void setAverangeTaskListDuration(Double averangeTaskListDuration) {
		this.averangeTaskListDuration = averangeTaskListDuration;
	}
}
