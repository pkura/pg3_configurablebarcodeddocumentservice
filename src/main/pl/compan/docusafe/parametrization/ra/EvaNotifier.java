package pl.compan.docusafe.parametrization.ra;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.boot.Docusafe;
import java.util.Date;
/**  
 * Klasa odpowiedzialna za powiadamianie EVA przez linka
 * Na razie obedzie sie bez servisu
 * @author wkutyla
 *
 */
public class EvaNotifier 
{
	public static final Log log = LogFactory.getLog("messaging_log");
	
	private static String evaLink;
	private static Date lastNotifyDate = new java.util.Date();
	private static long lastProcessTries = 0;
	private static long nextProcessNumber = 1;
	/** 
	 * Metoda odpowiada za powiadomienie EVA
	 */
	public static synchronized void notifyEva()
	{
		EvaSender sender = new EvaSender(nextProcessNumber);
		nextProcessNumber++;
		sender.start();
	}
	
	static int sendLinkToEva() throws HttpException, IOException
    {
		evaLink = Docusafe.getAdditionProperty("evaLink");
    	HttpClient httpClient = new HttpClient();
        httpClient.setConnectionTimeout(20*1000);
        httpClient.setTimeout(20*1000);
    	GetMethod get = new GetMethod(evaLink);
        //post.addRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
        int status = httpClient.executeMethod(get);        
		return status;
    }
	
	/**
	 * 
	 * @param processId
	 * @return zwraca true jesli proces jest tym o najwyzszym Id;
	 */
	public static synchronized boolean markTry (long processId)
	{
		boolean resp = false;
		if (processId>=lastProcessTries)
		{
			lastProcessTries = processId;
			resp = true;
			
		}
		return resp;
	}
	
	/**
	 * Metoda rejestruje udana probe wysylki
	 */
	public static synchronized void markSuccess()
	{
		lastNotifyDate = new java.util.Date();
	}
	/**
	 * Metoda okresla czy byl sukces po dacie wygenerowania procesu (np. inny proces powiadomil EVE)
	 * @param createTime data utworzenia procesu
	 * @return true -  jesli by sukces po dacie utworzenia procesu
	 */
	public static synchronized boolean isProcessOverdue (Date createTime)	
	{
		return (lastNotifyDate.after(createTime));
	}
}
