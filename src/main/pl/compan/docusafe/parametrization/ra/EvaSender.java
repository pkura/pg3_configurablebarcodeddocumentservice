package pl.compan.docusafe.parametrization.ra;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
/**
 * 
 * @author wkutyla
 * Watek z ktorego wysylane sa requesty do EVA 
 */
public class EvaSender extends Thread 
{
	private static final Log log = LogFactory.getLog("messaging_log");
	private static final long SLEEP_TIME = 30000;
	private long threadId;
	private Date createTime;
	private boolean newestProcess = true;
	
	public EvaSender(long id)
	{
		super();
		threadId = id;
		createTime = new Date();
		setDaemon(true);
	}
	
	public void run()
	{
		log.debug("Proces "+threadId +" wystartowany");
		boolean doit = true;
		
		while (doit)
		{
			boolean success = false;
			int httpResponse;
			try
			{
				httpResponse =  EvaNotifier.sendLinkToEva();
				log.info(threadId+". Wysylka do EVA odpowiedz="+httpResponse);
				if (httpResponse == 200)
				{
					success = true;
					EvaNotifier.markSuccess();
				}
			}
			catch (Exception e)
			{
				log.error(threadId +". Nieudane wyslanie do EVA",e);
				success = false;
			}
			newestProcess = EvaNotifier.markTry(threadId);
			doit = nextTry(success);
			if (doit)
			{
				try { sleep(SLEEP_TIME); } catch (Exception x) { };
			}			
		}
		log.debug("Proces "+threadId +" zakonczony");	
	}
	/**
	 * 
	 * @param success
	 * @return
	 * Mechanizm wyznaczania konca procesu
	 * 1. Jesli sie udalo konczymy
	 * 2. Jesli proces o wyzszym ID probuje to konczymy
	 * 3. Jesli data sukcesu > daty powstania watku konczymy 
	 */
	private boolean nextTry(boolean success)
	{
		boolean resp = true;
		if (success)
		{
			resp = false;
			log.debug("Proces "+threadId +" sukces");
		}
		else if (!newestProcess)
		{
				resp = false;
				log.debug("Proces "+threadId +" jest nowszy proces");
		}
		else if (EvaNotifier.isProcessOverdue(createTime))
		{
			resp = false;
			log.debug("Proces "+threadId +" jest url wyslany pozniej");
		}
		return resp;
	}
}
