package pl.compan.docusafe.parametrization.ra;

import java.sql.CallableStatement;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.service.imports.dsi.DSIBean;
import pl.compan.docusafe.service.imports.dsi.DSIImportHandler;

public class RockwellDSIHandler extends DSIImportHandler 
{
	public String getDockindCn()
	{
		return "rockwell";
	}
	protected void prepareImport() throws Exception 
	{
		//tutaj uklon w strone walca
		values.remove("ARCHIVE_BOX");		
	}
	@Override
	protected boolean isStartProces()
	{
		return true;
	}
	@Override
	public void actionAfterCreate(Long documentId, DSIBean dsiB) throws Exception
	{
		CallableStatement cs = null;
		try
		{
            cs = DSApi.context().session().connection().prepareCall("{call dsi_after_import(?)}");
            cs.setLong(1, documentId);
            cs.execute(); 
		}
		finally
		{
			if(cs != null) cs.close();
		}
	}
}
