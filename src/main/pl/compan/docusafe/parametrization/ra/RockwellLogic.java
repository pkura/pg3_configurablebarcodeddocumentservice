package pl.compan.docusafe.parametrization.ra;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Flags;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.RockwellVendor;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class RockwellLogic extends AbstractDocumentLogic
{
	private StringManager sm = GlobalPreferences.loadPropertiesFile(RockwellLogic.class.getPackage().getName(),null);
    private static RockwellLogic instance;
    public static final String GUID_TO_REJECT = "ROCKWELL_REJECT_GUID";

    public static RockwellLogic getInstance()
    {
        if (instance == null)
            instance = new RockwellLogic();
        return instance;
    }

    // nazwy kodowe poszczeg�lnych p�l dla tego rodzaju dokumentu
    public static final String DOCUMENT_TYPE_FIELD_CN = "DOCUMENT_TYPE";
    public static final String DOCUMENT_CATEGORY_FIELD_CN = "DOCUMENT_CATEGORY";
    public static final String BUSINESS_UNIT_FIELD_CN = "BU";
    public static final String BARCODE_FIELD_CN = "BARCODE";
    public static final String VENDOR_ID_FIELD_CN = "VENDOR_ID";
    public static final String INVOICE_RECEIVE_DATE_FIELD_CN = "INVOICE_RECEIVE_DATE";
    public static final String INVOICE_DATE_FIELD_CN = "INVOICE_DATE";
    public static final String INVOICE_NUMBER_FIELD_CN = "INVOICE_NUMBER";
    public static final String CURRENCY_FIELD_CN = "CURRENCY";
    public static final String GROSS_AMOUNT_FIELD_CN = "GROSS_AMOUNT";
    public static final String NET_AMOUNT_FIELD_CN = "NET_AMOUNT";
    public static final String VAT_AMOUNT_FIELD_CN = "VAT_AMOUNT";
    public static final String PO_NUMBER_FIELD_CN = "PO_NUMBER";
    public static final String VOUCHER_NUMBER_FIELD_CN = "VOUCHER_NUMBER";
    public static final String PAYSLIP_1_FIELD_CN = "PAYSLIP_1";
    public static final String PAYSLIP_2_FIELD_CN = "PAYSLIP_2";
    public static final String EVA_STATUS_FIELD_CN = "EVA_STATUS";
    public static final String SUBMITTED_BY_CN = "SUBMITTED_BY";
    public static final String SUBMIT_DATE_FIELD_CN = "SUBMIT_DATE";
    public static final String RECEIVE_DATE_FIELD_CN = "RECEIVE_DATE";
    public static final String EVA_FINISH_DATE_FIELD_CN = "EVA_FINISH_DATE";
    public static final String DOCUMENT_STATUS_FIELD_CN = "DOCUMENT_STATUS";
    public static final String URL_FIELD_CN = "URL";
    public static final String NO_VENDOR_FIELD_CN = "NO_VENDOR";
    public static final String REGISTRATION_NO_CN = "registr_no";

    /**Statusy */
     public static final Integer STAT_REJECT_AP = 10;
     public static final Integer STAT_PENDING = 20;
     public static final Integer STAT_PROCESSED = 30;
     public static final Integer STAT_CANCELLED = 40;
     public static final Integer STAT_PROCESSED_EVA = 50;
     public static final Integer STAT_ARCHIVED = 60;
     public static final Integer STAT_REJECT_EVA = 70;

     /**Document category */
     public static final Integer CATEGORY_RI_OTHER = 10;
     public static final Integer CATEGORY_PO =20;
     public static final Integer CATEGORY_UNDER_100 = 30;
     public static final Integer CATEGORY_ABOVE_100 = 40;
     public static final Integer CATEGORY_INTERCOMPANY = 50;
     /**Document category */
     public static final String CATEGORY_RI_OTHER_CN = "RI_OTHER";
     public static final String CATEGORY_PO_CN ="PO";
     public static final String CATEGORY_UNDER_100_CN = "UNDER_100";
     public static final String CATEGORY_ABOVE_100_CN = "ABOVE_100";
     public static final String CATEGORY_INTERCOMPANY_CN = "INTERCOMPANY";

    //String zawieraj�cy znaki, kt�re m�na u�yc w funkcji generateHash
    public static final String RANDOM_SET= "abcdefghijklmnopqrstuvwxzABCDEFGHIJKLMNOPQRSTUVWXZ1234567890";

    /**
     * funkcja, kt�ra generuje jednorazowy hash, na podstawie kt�rego mo�na
     * zobaczy� za��cznik do pisma bez zalogowania si� do systemu
     *
     */
    public static String generateHash()
    {
        StringBuffer hash = new StringBuffer();
        Random rand = new Random();

        for(int i=0;i<32;i++)
            hash.append(RANDOM_SET.charAt(rand.nextInt(RANDOM_SET.length()-1)));

        return hash.toString();
    }

    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
    {
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

        if (fm.getValue(BUSINESS_UNIT_FIELD_CN)== null)
        	throw new EdmException("BUSINESS UNIT  is mandatory");

        document.getDocumentKind().handleLabels(document, fm);

        Folder folder = Folder.getRootFolder();


        if(fm.getValue(BUSINESS_UNIT_FIELD_CN)!= null)
        {
        	folder = folder.createSubfolderIfNotPresent(fm.getValue(BUSINESS_UNIT_FIELD_CN).toString());
        }
        if(fm.getValue(INVOICE_DATE_FIELD_CN)!= null)
        {
        	folder = folder.createSubfolderIfNotPresent(DateUtils.formatYear_Month((Date)fm.getValue(INVOICE_DATE_FIELD_CN)));
        }
        else
        {
        	folder = folder.createSubfolderIfNotPresent(DateUtils.formatYear_Month(document.getCtime()));
        }

        document.setFolder(folder);
        if (document instanceof OfficeDocument)
        {
            ((OfficeDocument)document).setSummaryOnly(
            		fm.getValue(BUSINESS_UNIT_FIELD_CN).toString()
            		+ (fm.getValue(INVOICE_NUMBER_FIELD_CN) != null ? " - " + fm.getValue(INVOICE_NUMBER_FIELD_CN) : ""));
        }
        Map<String, Object> hiddenValues = new HashMap<String, Object>();

        if(fm.getValue(URL_FIELD_CN) == null)
        {
        	hiddenValues.put(URL_FIELD_CN, generateHash());
        }
        document.getDocumentKind().setOnly(document.getId(), hiddenValues);
    }

    /**
     * Ustawiane sa
     */
    public void documentPermissions(Document document) throws EdmException
    {
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");

        //DSApi.context().clearPermissions(document);
        Set<PermissionBean> perms = new HashSet<PermissionBean>();

      //DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "ROCKWELL_READ", ObjectPermission.GROUP);
        perms.add(new PermissionBean(ObjectPermission.READ,"ROCKWELL_READ",ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"ROCKWELL_READ",ObjectPermission.GROUP));

        //DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "ROCKWELL_MODIFY", ObjectPermission.GROUP);

        perms.add(new PermissionBean(ObjectPermission.MODIFY, "ROCKWELL_MODIFY",ObjectPermission.GROUP));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "ROCKWELL_MODIFY",ObjectPermission.GROUP));

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        fm.initialize();

        if(fm.getValue("BU") != null)
        {
        	String readGuid = "BUSINESS_UNIT_"+fm.getValue("BU")+"_READ";
        	String writeGuid = "BUSINESS_UNIT_"+fm.getValue("BU")+"_MODIFY";
        	String readName = "Business unit "+fm.getValue("BU")+" - Read";
        	String writeName = "Business unit "+fm.getValue("BU")+" - Modify";

        	perms.add(new PermissionBean(ObjectPermission.READ,readGuid,ObjectPermission.GROUP,readName));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,readGuid,ObjectPermission.GROUP,readName));

        	perms.add(new PermissionBean(ObjectPermission.MODIFY, writeGuid,ObjectPermission.GROUP,writeName));
            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, writeGuid,ObjectPermission.GROUP,writeName));

        	//createPermission(document, "Business unit "+fm.getValue("BU")+" - Read", "BUSINESS_UNIT_"+fm.getValue("BU")+"_READ", new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS});
            //createPermission(document, "Business unit "+fm.getValue("BU")+" - Modify", "BUSINESS_UNIT_"+fm.getValue("BU")+"_MODIFY", new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS});
        }
        setUpPermission(document, perms);
    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {

    	Map<String,Object> values = new HashMap<String,Object>();
    	values.put(CURRENCY_FIELD_CN, 10);
    	values.put(DOCUMENT_STATUS_FIELD_CN, STAT_PENDING);
    	fm.reloadValues(values);
    }

    public boolean searchCheckPermissions(Map<String,Object> values)
    {
        return true;
    }

    @Override
    public void checkCanFinishProcess(OfficeDocument document) throws AccessDeniedException, EdmException
    {
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");

        String error = sm.getString("NieMoznaZakonczycPracyZdokumentem")+": ";

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        EnumItem kategoriaItem = fm.getEnumItem(DOCUMENT_TYPE_FIELD_CN);
        if (kategoriaItem == null)
            throw new NullPointerException(DOCUMENT_TYPE_FIELD_CN);
        if (kategoriaItem.getCn()==null)
            throw new NullPointerException("cn");

        if(fm.getKey(DOCUMENT_STATUS_FIELD_CN) == null )
        {
        	throw new AccessDeniedException(error);
        }

        // gdy status odrzucony mo�na zako�czy� proces zawsze - wystarczy tylko doda� uwag�
        if (STAT_PENDING.equals(fm.getKey(DOCUMENT_STATUS_FIELD_CN)))
        {
        	throw new AccessDeniedException(error);

        }
        if (STAT_REJECT_AP.equals(fm.getKey(DOCUMENT_STATUS_FIELD_CN)))
        {
        	if (document.getRemarks() == null || document.getRemarks().size() == 0)
        		throw new AccessDeniedException(error+sm.getString("ConajmniejJednaUwagaMusiZostacWprowadzonaDlaDokumentu"));
        	return;
        }

        // sprawdzanie flag
        boolean ksiegowa = false;
        boolean platnosci = false;
        for(Flags.Flag flag : document.getFlags().getDocumentFlags(false)){
	        if (flag != null)
	        {
	            if ("M".equals(flag.getC()) && flag.isValue())
	                platnosci = true;
	            else if ("K".equals(flag.getC()) && flag.isValue())
	                ksiegowa = true;
	        }
        }
    }

    @Override
    public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
    {
    	String channel = document.getDocumentKind().getChannel(document.getId());
    	if(channel != null)
    	{
    		List<ProcessCoordinator> pcl = ProcessCoordinator.find(DocumentLogicLoader.ROCKWELL_KIND,channel);
	    	 if( pcl != null && pcl.size() > 0)
	    	        return ProcessCoordinator.find(DocumentLogicLoader.ROCKWELL_KIND,channel).get(0);
	    	 else
	    		 return null;
    	}
    	return null;
    }


    @Override
    public TaskListParams getTaskListParams(DocumentKind kind, long documentId) throws EdmException {
        return TaskListParams.fromMagicArray(getTaskListParam(kind, documentId));
    }

	/**
	 * @TODO - powinny byc przechwytywane wyjatki dotyczace pol enumerowanych
	 */
    private Object[] getTaskListParam(DocumentKind dockind,Long id) throws EdmException
    {
		Object[] result = new Object[10];
		FieldsManager fm = dockind.getFieldsManager(id);
		result[0] = fm.getValue(DOCUMENT_STATUS_FIELD_CN);
		result[1] = fm.getKey(INVOICE_NUMBER_FIELD_CN);
		result[2] = fm.getKey(GROSS_AMOUNT_FIELD_CN);
		if(fm.getValue(DOCUMENT_TYPE_FIELD_CN) != null && fm.getValue(DOCUMENT_CATEGORY_FIELD_CN) != null)
			result[3] = fm.getValue(DOCUMENT_TYPE_FIELD_CN).toString()+" - "+fm.getValue(DOCUMENT_CATEGORY_FIELD_CN).toString();
		result[4] = fm.getValue(BUSINESS_UNIT_FIELD_CN);
		if(fm.getValue(VENDOR_ID_FIELD_CN) != null)
		{
			result[5] = ((RockwellVendor)fm.getValue(VENDOR_ID_FIELD_CN)).getName();
			result[6] = ((RockwellVendor)fm.getValue(VENDOR_ID_FIELD_CN)).getVendorId();
		}
		result[7] = fm.getValue(REGISTRATION_NO_CN) != null ? fm.getValue(REGISTRATION_NO_CN) : "";
		result[8] = fm.getValue(INVOICE_DATE_FIELD_CN);;
		return result;

    }

    public void correctImportValues(Document document, Map<String, Object> values, StringBuilder builder) throws EdmException
    {
    	DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.ROCKWELL_KIND);
    	Map<String, Object> valuesTmp = new HashMap<String, Object>(values);
		Set<String> valuesSet = valuesTmp.keySet();

		if( (!values.containsKey("VENDOR_ID") || !values.containsKey("SET_ID"))) //&& !values.equals("XXX"))
		{

			//builder.append("Vendor Id and Set Id is obligatory, ");
			throw new EdmException("Vendor Id and Set Id is obligatory, ");
		}

		for (String cn : valuesSet)
		{
			Field f = documentKind.getFieldByCn(cn);
			if(f != null)
			{
				if(f.getType().equals(Field.ENUM) || f.getType().equals(Field.ENUM_REF))
				{
					values.put(cn, f.getEnumItemByCn((String)values.get(cn)).getId());
				}
			}
			else
			{
				String vendorId = null;
				String setId = null;
				if(cn.equalsIgnoreCase("VENDOR_ID") || cn.equalsIgnoreCase("SET_ID"))
				{
					vendorId = values.get("VENDOR_ID").toString();
					setId = values.get("SET_ID").toString();

				}
				else if(!cn.equalsIgnoreCase("Box_code"))
				{
					throw new EdmException("Incorrect field " + cn+", ");
				}
				if(vendorId != null && setId != null && !setId.equalsIgnoreCase("XXX"))
				{
					Criteria c = DSApi.context().session().createCriteria(RockwellVendor.class);
		            c.add(Expression.eq("vendorId",vendorId));
		            c.add(Expression.eq("setId",setId));
		            List<RockwellVendor> list = (List<RockwellVendor>) c.list();
		            if(list.size() < 1)
		            {
		            	RockwellVendor vendor = new RockwellVendor();
		            	vendor.setSetId(setId);
		            	vendor.setVendorId(vendorId);
		            	vendor.create();
		            	values.put(RockwellLogic.VENDOR_ID_FIELD_CN, vendor.getId());

		            	if(document instanceof InOfficeDocument)
		            	{
		            		Sender sender = new Sender();
		            		((InOfficeDocument)document).setSender(sender);
		            	}
		            }
		            else
		            {
		            	RockwellVendor vendor= list.get(0);
		            	values.put(RockwellLogic.VENDOR_ID_FIELD_CN, vendor.getId());
		            	if(document instanceof InOfficeDocument)
		            	{
		            		Sender sender = new Sender();
		            		((InOfficeDocument)document).setSender(sender);

			            	if(vendor.getName() != null)
			            		sender.setFirstname(TextUtils.trimmedStringOrNull(vendor.getName(), 50));
			            	if(vendor.getCity() != null)
			            		sender.setLocation(TextUtils.trimmedStringOrNull(vendor.getCity(), 50));
			            	if(vendor.getAddress() != null)
			            		sender.setRemarks(TextUtils.trimmedStringOrNull(vendor.getAddress(), 200));
		            	}
		            }
				}
				else if(!cn.equalsIgnoreCase("Box_code") && !setId.equalsIgnoreCase("XXX"))
				{
					//builder.append("Vendor Id and Set Id is obligatory, ");
					throw new EdmException("Vendor Id and Set Id is obligatory, ");
				}
				else
				{
					values.put(RockwellLogic.NO_VENDOR_FIELD_CN, new Boolean(true));
					values.remove(RockwellLogic.VENDOR_ID_FIELD_CN);
				}
			}
			if(document instanceof InOfficeDocument)
        	{
				((InOfficeDocument)document).setSummary(documentKind.getName());
        	}
		}
		values.put(RockwellLogic.DOCUMENT_STATUS_FIELD_CN, RockwellLogic.STAT_PENDING);
    }

    public void manualFinish(Document document) throws EdmException
    {
    	/*
    	if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");

    	FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
    	if(fm.getKey(DOCUMENT_STATUS_FIELD_CN) == null || !fm.getKey(DOCUMENT_STATUS_FIELD_CN).equals(STAT_REJECT_AP))
    	{
    		Map<String, Object> values = new HashMap<String, Object>();
            values.put(DOCUMENT_STATUS_FIELD_CN, STAT_PROCESSED);
            document.getDocumentKind().setOnly(document.getId(), values);
    	}*/
    }

	public boolean isReadPermissionCode(String permCode)
	{
		return permCode.endsWith("_READ");
	}
}
