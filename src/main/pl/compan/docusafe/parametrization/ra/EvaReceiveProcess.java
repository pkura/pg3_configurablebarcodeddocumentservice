package pl.compan.docusafe.parametrization.ra;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.WorkflowUtils;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;

/**
 *Proces przywracania zadan zwroconych przez EVA
 *
 * @author Mariusz Kiljanczyk
 * @version $Id$
 */
public class EvaReceiveProcess extends ServiceDriver implements Service
{
    private Timer timer;

    protected void start() throws ServiceException    
    {
        if (Docusafe.getAdditionProperty("evaReceiveProcess").equals("false"))
            return;
        //Na wszelki wypadek posylamy jedna strzalke do EVA
        EvaNotifier.notifyEva();
        //-- koniec strzalki
        Integer time = Integer.parseInt(Docusafe.getAdditionProperty("evaReceiveProcess.time"));

        timer = new Timer(true);
        timer.schedule(new CheckEvaProcess(), 1000, time*DateUtils.SECOND/*30*DateUtils.SECOND*/);
        
    }

    protected void stop() throws ServiceException
    {
        if (timer != null) timer.cancel();
    }

    protected boolean canStop()
    {
        return false;
    }

    class CheckEvaProcess extends TimerTask
    {
        public void run()
        {
        	ResultSet rs = null;
        	PreparedStatement ps = null;
        	
            try
            {
            	DSApi.openAdmin();
            	String sql = "select doc.id, roc.submitted_by, roc.voucher_number from  ds_document doc , dsg_rockwell roc where roc.document_id = doc.id and  roc.eva_status = 3 and receive_process_date is null";
            	ps = DSApi.context().prepareStatement(sql);
            	rs = ps.executeQuery();
            	while(rs.next())
                {            
            		String stat = rs.getString(3);
            		if (stat==null) 
            			stat ="cancelled";
            		if(stat.equalsIgnoreCase("cancelled") || stat.equalsIgnoreCase("canceled"))
            		{
	            		DSApi.context().begin();
	                    WorkflowFactory.reopenWf(rs.getLong(1),rs.getString(2));
	                    PreparedStatement ps2 = null;
	                    ps2 = DSApi.context().prepareStatement("update dsg_rockwell set receive_process_date = ?, document_status = ? where document_id = ?");
	                    Date date = new Date();
	                    ps2.setTimestamp(1, new Timestamp(date.getTime()));
	                    ps2.setInt(2, RockwellLogic.STAT_REJECT_EVA);
	                    ps2.setLong(3, rs.getLong(1));
	                    ps2.execute();
	                    DSApi.context().closeStatement(ps2);                   
	                    TaskSnapshot.updateByDocumentId(rs.getLong(1),"anyType");
	                    DSApi.context().commit();    
            		}
            		else
            		{
            			DSApi.context().begin();
	                    PreparedStatement ps2 = null;
	                    ps2 = DSApi.context().prepareStatement("update dsg_rockwell set receive_process_date = ?, document_status = ? where document_id = ?");
	                    Date date = new Date();
	                    ps2.setTimestamp(1, new Timestamp(date.getTime()));
	                    ps2.setInt(2, RockwellLogic.STAT_PROCESSED);
	                    ps2.setLong(3, rs.getLong(1));
	                    ps2.execute();
	                    DSApi.context().closeStatement(ps2);	                    
	                    DSApi.context().commit();  
            		}
                }
            	rs.close();            	            	
            }
            catch (Exception e)
            {
            	WorkflowFactory.getLog().error("Blad w czasie obslugi procesu EVA",e);
                DSApi.context().setRollbackOnly();
            }
            finally
            {
                DSApi.context().closeStatement(ps);				 				
				DSApi._close();
            }
        }       
    }
}
