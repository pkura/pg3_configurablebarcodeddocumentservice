package pl.compan.docusafe.parametrization.europol;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class InvoiceEuropolLogic extends AbstractDocumentLogic
{
	private static final Logger log = LoggerFactory.getLogger(InvoiceEuropolLogic.class);

	private static final InvoiceEuropolLogic instance = new InvoiceEuropolLogic();
	private static final String RODZAJ_FIELD_CN = "RODZAJ";
	private static final String DZIAL_FIELD_CN = "DZIAL";
	private static final String KWOTA_FIELD_CN = "KWOTA";
	private static final String NR_FAKTURY_FIELD_CN = "NR_FAKTURY";
	private static final String DOSTAWCA_FIELD_CN = "DOSTAWCA";
	private static final String DATA_WYSTAWIENIA_FIELD_CN = "DATA_WYSTAWIENIA";
	private static final String DATA_WPLYWU_FIELD_CN = "DATA_WPLYWU";
	private static final String DATA_PLATNOSCI_FIELD_CN = "DATA_PLATNOSCI";
	private static final String WALUTA_FIELD_CN = "WALUTA";	
	
	public static InvoiceEuropolLogic getInstance() 
	{
		return instance;
	}


    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
    	FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
    	if(fm.getValue("DOSTAWCA") == null || fm.getValue("DATA_WYSTAWIENIA") == null || fm.getValue("DZIAL") == null ||
    			fm.getValue("NR_FAKTURY") == null)
    		throw new EdmException("Brak podstawowych parametr�w");
    	
    	String data = (fm.getValue("DATA_WYSTAWIENIA") != null ? DateUtils.formatJsDate((Date)fm.getValue("DATA_WYSTAWIENIA")) : "" );
    	DicInvoice dic = null;
    	
    	
    	if(document instanceof InOfficeDocument)
    	{
    		dic = (DicInvoice) fm.getValue("DOSTAWCA");
    		InOfficeDocument doc = (InOfficeDocument) document;
    		doc.setOriginal(true);
    		Sender sender  = null;
    		if(doc.getSender() == null )
    			sender = new Sender();
    		else
    			sender = doc.getSender();
    		sender.setAnonymous(false);
    		sender.setEmail(StringUtils.left(dic.getEmail(),50));
    		sender.setFax(StringUtils.left(dic.getFaks(),30));
    		sender.setFirstname(StringUtils.left(dic.getImie(),50));
    		sender.setLocation(StringUtils.left(dic.getMiejscowosc(),50));
    		sender.setOrganization(StringUtils.left(dic.getName(),50));
    		sender.setLastname(StringUtils.left(dic.getNazwisko(),50));
    		sender.setNip(StringUtils.left(dic.getPrettyNip(),15));
    		sender.setStreet(StringUtils.left(dic.getUlica(),50));
    		sender.setZip(StringUtils.left(dic.getKod(),12));
    		sender.setDocument(doc);
    		DSApi.context().session().saveOrUpdate(sender);
    		DSApi.context().session().flush();
    		doc.setSender(sender);

	        Person person = new Person();
			person.setTitle(sender.getTitle());
			person.setFirstname(sender.getFirstname());
			person.setLastname(sender.getLastname());
			person.setOrganization(sender.getOrganization());
			person.setStreet(sender.getStreet());
			person.setZip(sender.getZip());
			person.setLocation(sender.getLocation());
			person.setCountry((sender.getCountry() == null ? "PL":sender.getCountry()));
			person.setEmail(sender.getEmail());
			person.setFax(sender.getFax());
	        person.setDictionaryGuid("rootdivision");
	        person.setDictionaryType(Person.DICTIONARY_SENDER);
	        person.createIfNew();
    	}
    	
    	if(document instanceof OfficeDocument)
    	{
    		DSDivision div = null;
    		try
	        { 
	            div =  DSDivision.find("DZIAL_"+fm.getKey("DZIAL"));
	        }
	        catch (Exception e)
	        {
	            DSDivision parent = DSDivision.find(getBaseGUID(document));
	            div = parent.createDivision((String)"Dzia� "+ fm.getValue("DZIAL"),null,"DZIAL_"+fm.getKey("DZIAL")); 
	        }
    		((OfficeDocument)document).setDivisionGuid(div.getGuid());
    	}
    	
    	Folder folder = Folder.getRootFolder();
    	folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime((Date) fm.getValue("DATA_WYSTAWIENIA"));    	
    	folder = folder.createSubfolderIfNotPresent((String) fm.getValue("DZIAL"));
    	folder = folder.createSubfolderIfNotPresent(""+calendar.get(Calendar.YEAR));
		folder = folder.createSubfolderIfNotPresent(""+(calendar.get(Calendar.MONTH)+1));
		String title = ""+fm.getValue("NR_FAKTURY") + " "+data + " "+(dic != null ? dic.getName():"");
		document.setTitle(StringUtils.left(title,254));
        document.setDescription(StringUtils.left(title,510));
		document.setFolder(folder); 
	}
    @Override
    public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException {
		String channel = document.getDocumentKind().getChannel(document.getId());
		if (channel != null) {
			List<ProcessCoordinator> cords = ProcessCoordinator.find(DocumentLogicLoader.EUROPOL_INV, channel);
			if (cords!= null && cords.size()>0) {
				return cords.get(0);
			} else {
				return null;
			}
		}
		return null;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		
		perms.add(new PermissionBean(ObjectPermission.READ,DocumentLogicLoader.EUROPOL_INV+"_READ",ObjectPermission.GROUP,"Dokumenty - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,DocumentLogicLoader.EUROPOL_INV+"_ATT_READ",ObjectPermission.GROUP,"Dokumenty zalacznik - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY,DocumentLogicLoader.EUROPOL_INV+"_MODIFY",ObjectPermission.GROUP,"Dokumenty- modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,DocumentLogicLoader.EUROPOL_INV+"_ATT_MODIFY",ObjectPermission.GROUP,"Dokumenty zalacznik - modyfikacja"));
   	 	FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
   	    if(fm.getKey("DZIAL")!= null)
   	    {
   	    	String preffix = "DZIAL_"+fm.getKey("DZIAL");
   	    	String dzial = (String) fm.getValue("DZIAL");
   	    	perms.add(new PermissionBean(ObjectPermission.READ,preffix+"_READ",ObjectPermission.GROUP,"Dzia� "+dzial+" - odczyt"));
   	   	 	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,preffix+"_ATT_READ",ObjectPermission.GROUP,"Dzia� "+dzial+" zalacznik - odczyt"));
   	   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY,preffix+"_MODIFY",ObjectPermission.GROUP,"Dzia� "+dzial+" - modyfikacja"));
   	   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,preffix+"_ATT_MODIFY",ObjectPermission.GROUP,"Dzia� "+dzial+" zalacznik - modyfikacja"));
   	    }
   	 	this.setUpPermission(document, perms);
	}

	public TaskListParams getTaskListParams(DocumentKind kind, long documentId) throws EdmException
	{
		FieldsManager fm = kind.getFieldsManager(documentId);
		TaskListParams params = new TaskListParams();
		if(fm.getKey(DATA_WYSTAWIENIA_FIELD_CN) != null)
			params.setDocumentDate((Date) fm.getKey(DATA_WYSTAWIENIA_FIELD_CN));
		
		if(fm.getKey(DATA_WYSTAWIENIA_FIELD_CN) != null)
			params.setAttribute(DateUtils.formatJsDate((Date) fm.getValue(DATA_PLATNOSCI_FIELD_CN)), 0);
		if(fm.getKey(DATA_WPLYWU_FIELD_CN) != null)
			params.setAttribute(DateUtils.formatJsDate((Date) fm.getValue(DATA_WPLYWU_FIELD_CN)), 1);
		if(fm.getKey(RODZAJ_FIELD_CN) != null)
			params.setCategory(""+fm.getValue(RODZAJ_FIELD_CN));
		if(fm.getKey(NR_FAKTURY_FIELD_CN) != null)
			params.setDocumentNumber(""+fm.getValue(NR_FAKTURY_FIELD_CN));
		if(fm.getKey(KWOTA_FIELD_CN) != null)
			params.setAmount((BigDecimal)fm.getKey(KWOTA_FIELD_CN));
		
		
		//	if(fm.getKey(DATA_WYSTAWIENIA_FIELD_CN) != null)
		//		params.setAttribute((String) fm.getValue(DATA_WPLYWU_FIELD_CN), 1); 
		//	if(fm.getKey(DATA_WYSTAWIENIA_FIELD_CN) != null)
		//	{
		//		DicInvoice dic = (DicInvoice) fm.getValue(DOSTAWCA_FIELD_CN);
		//		params.setAttribute(dic.getDictionaryDescription(), 2);
		//	}
			
		//if(fm.getKey("") != null) 
		//	params.setAttribute(fm.getValue(""), 3);
		
		//params.setStatus(status);
		return params;
	}
	
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
        Map<String,Object> values = new HashMap<String,Object>();
        values.put(RODZAJ_FIELD_CN, 40);
        values.put(WALUTA_FIELD_CN, 1);
        fm.reloadValues(values);
    }
	
	
    /**
     * Bazowy GUID dla nowotworzonych grup
     * @param document
     * @throws EdmException
     */
    protected String getBaseGUID(Document document) throws EdmException
    {
    	return "DIV_ROOT"; 
    }
    
	public void doDockindEvent(DockindButtonAction eventActionSupport, ActionEvent event,Document document, String activity,Map<String, Object> values) throws EdmException
	{
		addFlagAndGo(eventActionSupport, event, document, activity, values);
	}	
	
	public static void addFlagAndGo(DockindButtonAction eventActionSupport, ActionEvent event,Document document, String activity,Map<String, Object> values) throws EdmException
	{
		if (!"ADD_AF_FLAGS".equals(eventActionSupport.getDockindEventValue()))
		{
			event.addActionError("Brak obs�ugi zdarzenia");
			return;
		}		
		Map<String,String> prop = document.getDocumentKind().getDockindInfo().getProperties();
		String guid = null;
		Long flagId = null;
		if( prop != null && prop.get("ksiegowoscGuid") != null && prop.get("AFflagsId") != null)
		{
			guid = prop.get("ksiegowoscGuid");
			flagId = Long.parseLong(prop.get("AFflagsId"));
		}
		else
		{
			event.addActionError("Brak podstawowych parametr�w");
			return;
		}
		try
		{
			DSApi.context().begin();
			if (!LabelsManager.existsDocumentToLabelBean(document.getId(), flagId))
			{
				LabelsManager.canAddLabel(flagId, DSApi.context().getPrincipalName());
				LabelsManager.addLabel(document.getId(),flagId,DSApi.context().getPrincipalName());
				event.addActionMessage("Nadano akceptacje finansow� dokument ID "+ document.getId());
			}
			else
			{
				event.addActionError("Dokument "+document.getId()+" posiada� ju� akceptacje finansow�");
				return;
			}
			WorkflowFactory.simpleAssignment(activity, guid, null, DSApi.context().getPrincipalName(), null, WorkflowFactory.SCOPE_DIVISION, null, event);
			DSApi.context().commit();
		}
		catch (Exception e)
		{
			event.addActionError("B��d podczas nadawania akceptacji dokument ID "+ document.getId()+":"+e.getMessage());
			log.error("",e);
			DSApi.context().rollback();
		}
	}	
}
