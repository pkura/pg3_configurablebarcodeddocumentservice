package pl.compan.docusafe.parametrization.europol;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class NormalEuropolLogic extends AbstractDocumentLogic
{
	private static final NormalEuropolLogic instance = new NormalEuropolLogic();
	private static final Logger log = LoggerFactory.getLogger(NormalEuropolLogic.class);
	
	public static NormalEuropolLogic getInstance() 
	{
		return instance;
	}

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		document.setTitle((String)fm.getValue("RODZAJ"));
		document.setDescription((String)fm.getValue("RODZAJ"));
		if(document  instanceof OfficeDocument) 
		{
			((OfficeDocument)document).setSummaryOnly((String)fm.getValue("RODZAJ"));			
		}		
		Folder folder = Folder.getRootFolder();
    	folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(document.getCtime());
    	folder = folder.createSubfolderIfNotPresent(""+calendar.get(Calendar.YEAR));
		folder = folder.createSubfolderIfNotPresent(""+(calendar.get(Calendar.MONTH)+1));		
		document.setFolder(folder);
	}
    @Override
    public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException {
		String channel = document.getDocumentKind().getChannel(document.getId());
		if (channel != null) {
			List<ProcessCoordinator> cords = ProcessCoordinator.find(DocumentKind.NORMAL_KIND, channel);
			if (cords!= null && cords.size()>0) {
				return cords.get(0);
			} else {
				return null;
			}
		}
		return null;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		
		perms.add(new PermissionBean(ObjectPermission.READ,DocumentLogicLoader.EUROPOL_INV+"_READ",ObjectPermission.GROUP,"Dokumenty - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,DocumentLogicLoader.EUROPOL_INV+"_ATT_READ",ObjectPermission.GROUP,"Dokumenty zalacznik - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY,DocumentLogicLoader.EUROPOL_INV+"_MODIFY",ObjectPermission.GROUP,"Dokumenty- modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,DocumentLogicLoader.EUROPOL_INV+"_ATT_MODIFY",ObjectPermission.GROUP,"Dokumenty zalacznik - modyfikacja"));

   	 	this.setUpPermission(document, perms);
	}
	public void doDockindEvent(DockindButtonAction eventActionSupport, ActionEvent event,Document document, String activity,Map<String, Object> values) throws EdmException
	{
		InvoiceEuropolLogic.addFlagAndGo(eventActionSupport, event, document, activity, values);
	}

	  public void onStartProcess(OfficeDocument document) 
	  {
		/*DocumentKind dk = document.getDocumentKind();
		FieldsManager fm = dk.getFieldsManager(document.getId());
		try 
		{
			EnumItem kategoriaItem = fm.getEnumItem("RODZAJ");
			if (kategoriaItem == null || kategoriaItem.getCn() == null) 
				return;
			if (!kategoriaItem.getCn().equals("KOPIA_FAKTURY"))
				return;
			DSApi.context().session().flush();
			DSApi.context().commit();
			DSApi.context().begin();
			WorkflowFactory.getInstance().manualFinish(document.getId(), false);
		}
		catch (EdmException edme)
		{
			log.error("", edme);
		}*/
	}
}
