package pl.compan.docusafe.parametrization.ic;

import com.google.common.collect.Maps;
import pl.compan.docusafe.api.Fields;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ContractIcLogic extends AbstractDocumentLogic{
    public final static Logger log = LoggerFactory.getLogger(ContractIcLogic.class);
    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        Fields fields = Documents.document(document.getId()).getFields();

        String dateString;
        if(fields.get("DOCUMENT_DATE") != null){
            Date date = fields.get("DOCUMENT_DATE", Date.class);
            dateString =  new SimpleDateFormat(" (yyyy-MM-dd)").format(date);
        } else {
            dateString = "";
        }
        document.setTitle(fields.get("SUBTYPE", EnumItem.class).getTitle() + dateString);
        document.setDescription(document.getTitle());

        log.info("title '{}'", document.getTitle());

        Map<String, String> map = Maps.newHashMap();
        map.put("shortNumber", fields.get("DOCUMENT_NUMBER", String.class).substring(0, 4));
        as.useFolderResolver(document, map);
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {
        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ,"DOCUMENT_READ",ObjectPermission.GROUP,"Dokumenty - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"DOCUMENT_READ_ATT_READ",ObjectPermission.GROUP,"Dokumenty zalacznik - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY,"DOCUMENT_READ_MODIFY",ObjectPermission.GROUP,"Dokumenty - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"DOCUMENT_READ_ATT_MODIFY",ObjectPermission.GROUP,"Dokumenty zalacznik - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.DELETE,"DOCUMENT_READ_DELETE",ObjectPermission.GROUP,"Dokumenty - usuwanie"));

   	 	for (PermissionBean perm : perms){
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP)) {
				String groupName = perm.getGroupName();
				if (groupName == null) {
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
   	 	}
    }
}
