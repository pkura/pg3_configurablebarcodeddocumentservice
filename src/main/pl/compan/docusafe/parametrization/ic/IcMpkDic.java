package pl.compan.docusafe.parametrization.ic;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.dockinds.dictionary.AccountNumber;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;

import javax.persistence.*;

/**
 * User: Tomasz
 * Date: 16.01.14
 * Time: 11:18
 */

@Entity
@Table(name = "DSG_INVOICE_P_MPK")
public class IcMpkDic {

    @Id
    Long id;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="CENTRUMID", referencedColumnName="id", nullable=true)
    CentrumKosztow centrum;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="ACCOUNTNUMBER", referencedColumnName="id", nullable=true)
    AccountNumber account;
    @Column(name="KWOTA")
    Float kwota;
    @Column(name="STATUS")
    Integer status;

    @Column(name="SZEF_DZIALU")
    Boolean accSzefDzialu;
    @Column(name="SZEF_PIONU")
    Boolean accSzefPionu;
    @Column(name="DYREKTOR_FINANSOWY")
    Boolean accDyrektorFinansowy;

    public IcMpkDic() {}

    public static IcMpkDic find(Long id) throws EdmException {
        try {
            return (IcMpkDic) DSApi.context().session().createCriteria(IcMpkDic.class).add(Restrictions.eq("id", id)).uniqueResult();
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public final void create() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch(HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public final void update() throws EdmException {
        try {
            DSApi.context().session().saveOrUpdate(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getKwota() {
        return kwota;
    }

    public void setKwota(Float kwota) {
        this.kwota = kwota;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public CentrumKosztow getCentrum() {
        return centrum;
    }

    public void setCentrum(CentrumKosztow centrum) {
        this.centrum = centrum;
    }

    public AccountNumber getAccount() {
        return account;
    }

    public void setAccount(AccountNumber account) {
        this.account = account;
    }

    public Boolean getAccSzefDzialu() {
        return accSzefDzialu;
    }

    public void setAccSzefDzialu(Boolean accSzefDzialu) {
        this.accSzefDzialu = accSzefDzialu;
    }

    public Boolean getAccSzefPionu() {
        return accSzefPionu;
    }

    public void setAccSzefPionu(Boolean accSzefPionu) {
        this.accSzefPionu = accSzefPionu;
    }

    public Boolean getAccDyrektorFinansowy() {
        return accDyrektorFinansowy;
    }

    public void setAccDyrektorFinansowy(Boolean accDyrektorFinansowy) {
        this.accDyrektorFinansowy = accDyrektorFinansowy;
    }
}
