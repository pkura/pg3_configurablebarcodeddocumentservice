package pl.compan.docusafe.parametrization.ic;

import com.google.common.collect.ImmutableSet;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import static pl.compan.docusafe.parametrization.ic.InvoiceICLogic.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

/**
 * Klasa narz�dziowa dla IC
 *
 * Informacje do budowy kont pochodz� z pliku "dane do uzupenienia po spotkaniu z Docusafe 25.05.2011 (budowanie konta kosztowego).xls"
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class IcUtils {
    private final static Logger LOG = LoggerFactory.getLogger(IcUtils.class);
    /**
     * Konta z prost� budow�
     */
    private final static Set<String> PLAIN_ACCOUNTS_PREFIXES = ImmutableSet.of(
            "010", "020", "081", "220", "260");

    /**
     * Konta, kt�re wewn�trz siebie wstrzukuj� parametry centrum
     */
    private final static Set<String> INSERT_INSIDE_PREFIXES = ImmutableSet.of(
            "737");

    private final static Set<String> INSERT_INSIDE_SN_PREFIXES = ImmutableSet.of(
            "738","751","760");

    private final static Set<String> APPEND_PREFIXES = ImmutableSet.of(
            "400","401","402","403","404","405","406","407","408","409");

    private final static Set<String> APPEND_LOCALIZATION_ACCOUNTS = ImmutableSet.of(
            "300-TH", "314", "316", "341", "342");

    /**
     * Buduje magiczny kod konta kosztowego na podstawie MPK - metoda bazuje na excelach od IC
     * @param ckdf
     * @return
     */
    public static String buildAccountNumberCode(CentrumKosztowDlaFaktury ckdf) throws EdmException {
        StringBuilder ret = new StringBuilder("");
        if (APPEND_LOCALIZATION_ACCOUNTS.contains(ckdf.getAccountNumber())){
            appendLocalization(ckdf, ret);
        } else {
            String[] accountParts = ckdf.getAccountNumber().split("\\-",2);//podzia� np. "777-888-999" na "777" i "888-999"
            if(PLAIN_ACCOUNTS_PREFIXES.contains(accountParts[0])){
                return ckdf.getAccountNumber();
            } else if(INSERT_INSIDE_PREFIXES.contains(accountParts[0])){
                insertInside(ckdf, ret, accountParts);
            } else if(APPEND_PREFIXES.contains(accountParts[0])) {
                append(ckdf, ret);
            } else if (INSERT_INSIDE_SN_PREFIXES.contains(accountParts[0])){
                insertInsideSN(ckdf, ret, accountParts);
            }  else {
                LOG.warn("nieobs�ugiwana budowa konta '{}'", ckdf.getAccountNumber());
                return ckdf.getAccountNumber();
            }
        }
        return ret.toString().replace(" ","");
    }

    private static void appendLocalization(CentrumKosztowDlaFaktury ckdf, StringBuilder ret) throws EdmException {
        ret.append(ckdf.getAccountNumber());
        ret.append(ckdf.getLocationString("-"));
    }

    private static void append(CentrumKosztowDlaFaktury ckdf, StringBuilder ret) throws EdmException {
        ret.append(ckdf.getAccountNumber());
        if(ckdf.getClassTypeId() != null){
            ret.append("-")
               .append(CentrumKosztowDlaFaktury.getClassTypesCns().get(ckdf.getClassTypeId()));
        }
        ret.append(ckdf.getLocationString("-"));
        if(ckdf.getLocationString("").equals("CEN")){ //MPKI tylko dla CEN
            ret.append("-")
                .append(CentrumKosztow.find(ckdf.getCentrumId()).getSymbol2());
        } else if (ckdf.getAnalyticsId() != null) {
            ret.append("-").append(CentrumKosztowDlaFaktury.getAnalyticCns().get(ckdf.getAnalyticsId()));
        }  //dla reszty przypadk�w nie dodajemy nic (np 401-0110-S-BIA)
    }

    private static void insertInsideSN(CentrumKosztowDlaFaktury ckdf, StringBuilder ret, String[] accountParts) {
        ret.append(accountParts[0])
                .append('-');
        if(ckdf.getClassTypeId() != null){
            ret.append(CentrumKosztowDlaFaktury.getClassTypesCns().get(ckdf.getClassTypeId()))
                .append('-');
        }
        if(accountParts.length > 1){
            ret.append(accountParts[1]);
        }
    }

    private static void insertInside(CentrumKosztowDlaFaktury ckdf, StringBuilder ret, String[] accountParts) throws EdmException {
        ret.append(accountParts[0]).append('-');

        Date date = Documents.document(ckdf.getDocumentId()).getFields().get(DATA_WPLYWU_CN, Date.class);
        DateFormat df = new SimpleDateFormat("MM");
        ret.append(df.format(date));

        ret.append(ckdf.getLocationString("-"));
        if(ckdf.getConnectedTypeId() != null){
            ret.append('-')
               .append(CentrumKosztowDlaFaktury.getConnectedTypesCns().get(ckdf.getConnectedTypeId()));
        }

        if(accountParts.length > 1){
            if(!accountParts[1].equals("1-100")){  //konto tak si� ko�cz�ce ma ju� t� sta�� warto��
                ret.append("-2"); //warto�� sta�a z excela
            }
            if(accountParts[1].length() == 4 ){ //tylko dla kont typu 737-XXXX, d�u�sze mog� by� w postaci 737-XXX-XXX, wtedy doklejamy -XXX-XXX na koniec
                accountParts[1] = accountParts[1].substring(0,3);//czyli konto 737-2001, generuje identyczne konta co 737-001
            }
            ret.append('-').append(accountParts[1]);
        } else {
            ret.append("-2"); //warto�� sta�a z excela
        }
    }

    public static boolean isSpecialInvoice(FieldsManager fm) throws EdmException {
        return fm.getBoolean(KOREKTY_FIELD_CN)
                    || fm.getBoolean(FAKTURA_SPECJALNA_CN)
                    || fm.getBoolean(CREDIT_NOTY_FIELD_CN);
    }

    public static boolean hasVatDate(FieldsManager fm) throws EdmException {
        return (RODZAJE_Z_DATA_VAT.contains(fm.getEnumItemCn(InvoiceICLogic.RODZAJ_FAKTURY_CN))
                 || isSpecialInvoice(fm));
    }

    public static boolean hasVatEntries(FieldsManager fm) throws EdmException {
        return isSpecialInvoice(fm)
                || !RODZAJE_BEZ_VAT.contains(fm.getEnumItemCn(RODZAJ_FAKTURY_CN));
    }
}
