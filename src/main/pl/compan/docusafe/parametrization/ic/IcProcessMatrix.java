package pl.compan.docusafe.parametrization.ic;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;

import javax.persistence.*;
import java.util.Collections;
import java.util.List;

/**
 * User: Tomasz
 * Date: 13.01.14
 * Time: 11:55
 */

@Entity
@Table(name = "ds_ic_process_matrix")
public class IcProcessMatrix {

    //@SequenceGenerator(name = "ds_ic_process_matrix_seq", sequenceName = "ds_ic_process_matrix_seq")
    //@GeneratedValue(strategy = GenerationType.AUTO, generator = "ds_ic_process_matrix_seq")
    @Id
    Long id;
    @Column(name="mpkId")
    Long mpkId;
    @Column(name="kkId")
    Long kkId;
    @Column(name="ammount")
    Float ammount;
    @Column(name="processName")
    String processName;
    //DSUser user;
    //DSDivision division;

    public IcProcessMatrix() {}

    public IcProcessMatrix(Long mpkId, Long kkId, Float ammount, String processName) {
        this.mpkId = mpkId;
        this.kkId = kkId;
        this.ammount = ammount;
        this.processName = processName;
    }

    public static IcProcessMatrix find(Long id) throws EdmException {
        try {
            return (IcProcessMatrix) DSApi.context().session().createCriteria(IcProcessMatrix.class).add(Restrictions.eq("id", id)).uniqueResult();
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public static List<IcProcessMatrix> list() throws EdmException {
        try {
            return (List<IcProcessMatrix>) DSApi.context().session().createCriteria(IcProcessMatrix.class).list();
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public final void create() throws EdmException {
        try {
            DSApi.context().session().save(this);
        } catch(HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    public final void update() throws EdmException {
        try {
            DSApi.context().session().saveOrUpdate(this);
        } catch (HibernateException e) {
            throw new EdmHibernateException(e);
        }
    }

    //TODO: metoda powinna zwracac procesy internal dla faktury
    public static List listProcesses() {
        return Collections.EMPTY_LIST;
    }

    public Long getId() {
        return id;
    }

    public Long getMpkId() {
        return mpkId;
    }

    public void setMpkId(Long mpkId) {
        this.mpkId = mpkId;
    }

    public Long getKkId() {
        return kkId;
    }

    public void setKkId(Long kkId) {
        this.kkId = kkId;
    }

    public Float getAmmount() {
        return ammount;
    }

    public void setAmmount(Float ammount) {
        this.ammount = ammount;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }
}
