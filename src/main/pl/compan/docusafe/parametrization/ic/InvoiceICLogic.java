package pl.compan.docusafe.parametrization.ic;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVStrategy;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.api.Fields;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.permission.ClassicPermissionManager;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.*;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance;
import pl.compan.docusafe.core.dockinds.dictionary.AccountNumber;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.*;
import pl.compan.docusafe.core.dockinds.measure.BasicCheckPoint;
import pl.compan.docusafe.core.dockinds.measure.ProcessParameterBean;
import pl.compan.docusafe.core.dockinds.other.InvoiceInfo;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.GroupPermissions;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.permissions.PermissionCache;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.web.viewserver.ViewServer;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.ws.ic.IcDataProcessorStub;
import pl.compan.docusafe.ws.ic.IcDataProcessorStub.DajKhInfoResponse;
import pl.compan.docusafe.ws.ic.IcDataProcessorStub.ImportDokZkResponse;

import com.lowagie.text.Chapter;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.ListItem;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPTable;

import static pl.compan.docusafe.parametrization.ic.IcUtils.*;

/**
 * Logika dla rodzaju dokumentu 'invoice_ic'.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @author <a href="mailto:michal.sankowski@docusafe.pl">Micha� Sankowski</a>
 */
public class InvoiceICLogic extends AbstractDocumentLogic implements Discardable
{
	public final static Logger log = LoggerFactory.getLogger(InvoiceICLogic.class);
    //  nazwy kodowe poszczeg�lnych p�l dla tego rodzaju dokumentu
    public static final String DATA_WYSTAWIENIA_FIELD_CN = "DATA_WYSTAWIENIA";
    public static final String DATA_PLATNOSCI_FIELD_CN = "DATA_PLATNOSCI";
    public static final String DATA_ZAKSIEGOWANIA_FIELD_CN = "DATA_ZAKSIEGOWANIA";
    public static final String NUMER_TRANSAKCJI_FIELD_CN = "NUMER_TRANSAKCJI";
    public static final String DOSTAWCA_FIELD_CN = "DOSTAWCA";
    public static final String NUMER_FAKTURY_FIELD_CN = "NUMER_FAKTURY";
    /**Pole status z kt�rego korzysta system*/
    public static final String STATUS_FIELD_CN = "STATUS";
    /**Pole status wy�wietlane u�ytkownikowi (r�ni si� od STATUS tym, �e wy�wietla ostatnio wykonan� akceptacj�,
     * a STATUS wy�wietla "zaliczony" poziom akceptacji) */
    public static final String STATUS_FOR_USER_FIELD_CN = "STATUS_NEW";
    public static final String DATA_ZAPLATY_FIELD_CN = "DATA_ZAPLATY";
    public static final String ZAPLACONO_FIELD_CN = "ZAPLACONO";
    public static final String AKCEPTACJA_FINALNA_FIELD_CN = "AKCEPTACJA_FINALNA";
    public static final String KWOTA_BRUTTO_FIELD_CN = "KWOTA_BRUTTO";
    public static final String KWOTA_NETTO_FIELD_CN = "KWOTA_NETTO";
    public static final String CENTRUM_KOSZTOWE_FIELD_CN = "CENTRUM_KOSZTOWE";
    public static final String OPIS_TOWARU_FIELD_CN = "OPIS_TOWARU";
    public static final String NUMER_ZESTAWIENIA_FIELD_CN = "NUMER_ZESTAWIENIA";
    public static final String NUMER_RACHUNKU_BANKOWEGO_CN = "NUMER_RACHUNKU_BANKOWEGO";
    public static final String PION_CN = "PION";
    public static final String VAT_CN = "VAT";
    public static final String NAZWA_PROJEKTU_FIELD_CN = "NAZWA_PROJEKTU";
    @Deprecated //u�ywa� AcceptanceMode
    public static final String _LEGACY_SIMPLE_ACCEPTANCE_FIELD_CN = "SIMPLE_ACCEPTANCE";
    public static final String WALUTA_FIELD_CN = "WALUTA";
    public static final String REJESTR_FIELD_CN = "REJESTR";

    public static final String DATA_WPLYWU_CN = "DATA_WPLYWU";
    public static final String DATA_KSIEGOWANIA_CN = "DATA_KSIEGOWANIA";
    public static final String DATA_VAT = "DATA_VAT";
    public static final String LOKALIZACJA_CN = "LOKALIZACJA";
    public static final String DZIAL_FIELD_CN = "DZIAL";
    public static final String ADVANCE_TAKER_FIELD_CN = "ADVANCE_TAKER";
    public static final String ADVANCE_NUMBER_FIELD_CN = "ADVANCE_NUMBER";
    public static final String ADVANCE_INVOICE_NUMBER_FIELD_CN = "ADVANCE_INVOICE_NUMBER";
    public static final String PAYMENT_METHOD_FIELD_CN = "PAYMENT_METHOD";
	public static final String NUMER_KSIEGOWANIA_FIELD_CN = "NUMER_KSIEGOWANIA";
	public static final String NUMER_NABYCIA_FIELD_CN = "NUMER_NABYCIA";
	public static final String NUMER_PZ_FIELD_CN = "NUMER_PZ";
    public static final String SAFO_LAST_MESSAGE_FIELD_CN = "LAST_SAFO_MESSAGE";
    
    public static final String AKCEPTACJA_PROSTA_CN = "AKCEPTACJA_PROSTA";

    public static final String AKCEPTACJA_STALA_CN = "AKCEPTACJA_STALA";
    /**
     * nazwa kodowa pola: Identyfikator procesu jbpm, je�li te pole == null to dokument idzie starym trybem - nie mo�na korzysta� z JBPMAcceptanceManager
     */
    public static final String JBPM_PROCESS_ID = "JBPM_PROCESS_ID";
    public static final String RODZAJ_FAKTURY_CN = "RODZAJ_FAKTURY";
    public static final String FAKTURA_SPECJALNA_CN = "FAKTURA_SPECJALNA";
    public static final String KOREKTY_FIELD_CN = "KOREKTY";
    public static final String CREDIT_NOTY_FIELD_CN = "CREDIT_NOTY";

    public static final String RODZAJ_KOSZTY_CN = "KY";
	public static final String RODZAJ_SRODKI_TRWALE_CN = "ST";

	public static final String RODZAJ_ZAKUPY_KRAJOWE_CN = "ZK";
	public static final String RODZAJ_ZAKUPY_KRAJOWE_FLOTA_CN = "ZK_F";
    public static final String RODZAJ_ZAKUPY_KRAJOWE_FLC_CN = "ZK_FLC";
    @Deprecated
	public static final String RODZAJ_ZAKUPY_KRAJOWE_FBL_CN = "ZK_FBL";
    public static final String RODZAJ_ZAKUPY_KRAJOWE_NAPRAWY_POWYPADKOWE_CN = "ZK_NP";
	public static final String RODZAJ_ZAKUPY_KRAJOWE_FHE_CN = "ZK_FHE";
	public static final String RODZAJ_ZAKUPY_KRAJOWE_SAMOCHODY_IC_CN = "ZK_SIC";
	public static final String RODZAJ_ZAKUPY_KRAJOWE_PROJEKT_KOLO_CN = "ZK_PK";

    public static final String RODZAJ_ZAKUPY_UE_CN = "UE";
    public static final String RODZAJ_ZAKUPY_UE_PROJEKT_KOLO = "UE_PK";
    /**B#3640 - rodzaj podobny do UE*/
    public static final String RODZAJ_ZAKUPY_DT_CN = "ZK_DT";
    public static final String RODZAJ_ZAKUPY_IMP_CN = "IMP";

	public static final Set<String> RODZAJE_ZAKUPY_KRAJOWE_CN = new HashSet<String>(Arrays.asList(
							RODZAJ_ZAKUPY_KRAJOWE_CN,
							RODZAJ_ZAKUPY_KRAJOWE_FLOTA_CN,
							RODZAJ_ZAKUPY_KRAJOWE_FBL_CN,
							RODZAJ_ZAKUPY_KRAJOWE_FHE_CN,
                            RODZAJ_ZAKUPY_KRAJOWE_NAPRAWY_POWYPADKOWE_CN,
							RODZAJ_ZAKUPY_KRAJOWE_SAMOCHODY_IC_CN,
							RODZAJ_ZAKUPY_KRAJOWE_PROJEKT_KOLO_CN,
                            RODZAJ_ZAKUPY_KRAJOWE_FLC_CN
						));


    public static final Set<String> RODZAJE_BEZ_VAT = Sets.newHashSet(
            RODZAJ_ZAKUPY_IMP_CN,
            RODZAJ_ZAKUPY_UE_CN,
            RODZAJ_ZAKUPY_UE_PROJEKT_KOLO);

    /**
     * Rodzaje faktur, kt�re zawsze maj� date vat i pole stawki vat (inne faktury maj� tylko gdy s� specjalne)
     */
    static final Set<String> RODZAJE_Z_DATA_VAT = Sets.newHashSet(
                            RODZAJ_KOSZTY_CN,
                            RODZAJ_SRODKI_TRWALE_CN);

    // statusy
    public static final Integer STATUS_W_TRAKCIE = 10;

    public static final Integer STATUS_ODRZUCONA = 30;
    
    public static final String KOORDYNATOR_KSIEGOWOSC = "GUID_KOR_KSI";
    
    private static final String[] MONTHS = new String[] {"Stycze�", "Luty", "Marzec",
                                           "Kwiecie�","Maj","Czerwiec","Lipiec","Sierpie�",
                                           "Wrzesie�","Pa�dziernik","Listopad","Grudzie�"};
	private static final String DIVISION_TO_DISCARD = "DIVISION_TO_DISCARD";

	/**
	 * Faktury okre�lane mianem zakupowych, nie maj� podobnych proces�w akceptacji, ich cech� wsp�ln� jest
     * brak wybierania centr�w przy przypisywaniu centrum (tak, dok�adnie tak jak napisa�em), najcz�ciej
     * centra przypisywane s� automatycznie (nie zawsze)
     *
     * W momencie gdy klient m�wi "faktury zakupowe" lepiej dok�adnie spyta� o kt�re faktury chodzi,
     * gdy odpowie, spyta� jeszcze raz dla pewno�ci
     *
	 * @see InvoiceICLogic.FAKTURY_ZAKUPOWE_WITH_STRANGE_ACCEPTANCE_PROCESS
	 */
	public static final Set<String> FAKTURY_ZAKUPOWE_CNS = new HashSet<String>(Arrays.asList(
			"ZK",
			RODZAJ_ZAKUPY_KRAJOWE_FLOTA_CN,
            RODZAJ_ZAKUPY_KRAJOWE_FLC_CN,
			"ZK_FBL",
            RODZAJ_ZAKUPY_KRAJOWE_NAPRAWY_POWYPADKOWE_CN,
			"ZK_FHE",
			"ZK_SIC",
			"ZK_PK",
			RODZAJ_ZAKUPY_UE_CN,
            RODZAJ_ZAKUPY_UE_PROJEKT_KOLO,
            RODZAJ_ZAKUPY_DT_CN,
			RODZAJ_ZAKUPY_IMP_CN));

    // B#662
    public static final Set<String> FAKTURY_ZAKUPOWE_X_WITHOUT_300_TH = new HashSet<String>(Arrays.asList(
       RODZAJ_ZAKUPY_KRAJOWE_FLOTA_CN,
       RODZAJ_ZAKUPY_KRAJOWE_FLC_CN,
       RODZAJ_ZAKUPY_KRAJOWE_SAMOCHODY_IC_CN,
       RODZAJ_ZAKUPY_KRAJOWE_FHE_CN
    ));

	/**
	 * Faktury zakupowe z dziwnymi zasadami akceptacji, nowe przyciski, inne ukryte etc, lepiej z nimi uwa�a�
     *
     * Cz�sto pod poj�cie Faktury Zakupowe klient rozumie w�a�nie te faktury
	 */
	public static final Set<String> FAKTURY_ZAKUPOWE_WITH_STRANGE_ACCEPTANCE_PROCESS = new HashSet<String>(Arrays.asList(
			"ZK",
            RODZAJ_ZAKUPY_UE_CN,
            RODZAJ_ZAKUPY_DT_CN,
			"IMP"));

    private static final InvoiceICLogic instance = new InvoiceICLogic();
	public static final IntercarsAcceptanceManager IC_ACCEPTANCES_MANAGER = new IntercarsAcceptanceManager();
	public static final String STATUS_ODRZUCONY = "ODRZUCONY";
	public static final String STATUS_WYCOFANY = "WYCOFANY";

	//(thread safe)
    public static final Pattern GROUP_READ_PERMISSION_PATTERN = Pattern.compile("\\AINVOICE.*READ\\z");
    /** Id statusu wys�ano do asseco */
    public static final int STATUS_SENT_TO_ASSECO_ID = 1000;
    public static final int STATUS_ZAKSIEGOWANA_ID = 2000;
    private static final int STATUS_KONTROLA_JAKOSCI = 77;
    public static final String ASSECO_LABEL = "ASSECO";
    public static final String EXPORT_TYPE_FIELD_CN = "EXPORT_TYPE";
    public static final String VATS_FIELD_CN = "VATS";
    /** Tolerancja sum kwot brutto i netto */
    public static final BigDecimal AMOUNT_SUM_TOLERANCE = new BigDecimal("0.01");



    public static InvoiceICLogic getInstance() {
		return instance;
	}

	/**
	 * Por�wnuje sendera z dicinvoicem
	 */
	static boolean equal(Sender sender, DicInvoice dic){
		return sender.getFirstname()!=null && sender.getFirstname().equals(dic.getName());
	}
    public static void archiveActionsVAT(Document document, int type) throws EdmException {

        FieldsManager fm = Documents.document(document.getId()).getFieldsManager();
		if(Boolean.TRUE.equals(fm.getKey(AKCEPTACJA_FINALNA_FIELD_CN))){
            PermissionCache permCache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);
            if(!permCache.hasPermission(DSApi.context().getPrincipalName(), DSPermission.INVOICE_MODIFY_ACCEPTED.getName())){
                throw new EdmException("Modyfikacja atrybut�w nie jest mo�liwa po akceptacji finalnej");
            }
		}

        Calendar cal = Calendar.getInstance();
    	
        Fields fieldValues = Documents.document(document.getId()).getFields();

        handleNettoVatFields(document, fm, fieldValues);

		if(fm.getKey(STATUS_FIELD_CN) == null){
			fieldValues.put(STATUS_FIELD_CN, 10);
		} 

        Folder folder = Folder.getRootFolder();
    	folder = folder.createSubfolderIfNotPresent("Archiwum Dokument�w Finansowych");
		
		String pion = (String) fm.getValue(PION_CN);
		String rodzajFaktury = fm.getEnumItemCn(RODZAJ_FAKTURY_CN);
        boolean specialInvoice = fm.getBoolean(KOREKTY_FIELD_CN)
                || fm.getBoolean(FAKTURA_SPECJALNA_CN) || fm.getBoolean(CREDIT_NOTY_FIELD_CN);

        if(FAKTURY_ZAKUPOWE_CNS.contains(rodzajFaktury)){
            //przenosimy lokalizacj� z faktury do centrum kosztowego
            if(!specialInvoice && FAKTURY_ZAKUPOWE_WITH_STRANGE_ACCEPTANCE_PROCESS.contains(rodzajFaktury)){
                Integer locationId = (Integer) fm.getKey(LOKALIZACJA_CN);
                if(locationId != null){
                    List<CentrumKosztowDlaFaktury> ckdfs = CentrumKosztowDlaFaktury.findByDocumentId(document.getId());
                    if(!ckdfs.isEmpty() && ckdfs.get(0).getLocationId() == null){
                        ckdfs.get(0).setLocationId(locationId);
                    }
                }
            }
		    if(!StringUtils.isEmpty(pion)){
			    fieldValues.put(PION_CN, null);
			    pion = null;
            }
		}

		if(STATUS_WYCOFANY.equals(fm.getEnumItemCn(STATUS_FIELD_CN))) {
//			log.debug("WYCOFANE");
			folder = folder.createSubfolderIfNotPresent("Dokumenty wycofane");
		} else {
//			log.debug("NIE WYCOFANE - " + fm.getEnumItemCn(STATUS_FIELD_CN));
		}
		
		if(!StringUtils.isEmpty(pion)){
			folder = folder.createSubfolderIfNotPresent(pion);
//			log.debug("NIE WYCOFANE - pion = '" + pion + "' rodzaj = " + rodzajFaktury);
		} else {
			if (document.getDocumentKind().getCn().equals(DocumentLogicLoader.SAD_KIND)){
				folder = folder.createSubfolderIfNotPresent("Dokumenty SAD");
			} else if(rodzajFaktury.equals(RODZAJ_ZAKUPY_IMP_CN)){
				folder = folder.createSubfolderIfNotPresent("Faktury Zakup IMP");
			} else if(RODZAJE_ZAKUPY_KRAJOWE_CN.contains(rodzajFaktury)){
				folder = folder.createSubfolderIfNotPresent("Faktury Zakupy krajowe");
			} else if(rodzajFaktury.equals(RODZAJ_ZAKUPY_UE_CN)){
				folder = folder.createSubfolderIfNotPresent("Faktury Zakupy UE");
			} else if(rodzajFaktury.equals(RODZAJ_ZAKUPY_DT_CN)){
                folder = folder.createSubfolderIfNotPresent("Faktury Zakupy DT");
            }
        }
    	
    	Date dat = (Date) fm.getValue(DATA_WYSTAWIENIA_FIELD_CN);
    	cal.setTime(dat);
    	
    	folder = folder.createSubfolderIfNotPresent(String.valueOf(cal.get(Calendar.YEAR))+"-"+String.valueOf(cal.get(Calendar.MONTH)));
    	document.setFolder(folder);
    	
    	DicInvoice inst = (DicInvoice) fm.getValue(DOSTAWCA_FIELD_CN);

        if(document instanceof InOfficeDocument){
            if(((InOfficeDocument)document).getSender() == null){
                ((InOfficeDocument)document).setSender(new Sender());
                dicInvoiceToSender(inst, ((InOfficeDocument)document).getSender());
                ((InOfficeDocument)document).getSender().create();
            } else if(!equal(((InOfficeDocument)document).getSender(), inst)){
                dicInvoiceToSender(inst, ((InOfficeDocument)document).getSender());
            }
        }

    	String summary = "";
        StringBuilder nrFaktury = new StringBuilder();

        if ( fm.getValue(NUMER_FAKTURY_FIELD_CN) instanceof List) {
            for(Object obj : (List)fm.getValue(NUMER_FAKTURY_FIELD_CN) ){
               nrFaktury.append(obj + ", ");
            }
            nrFaktury.deleteCharAt(nrFaktury.length() - 2);
        } else {
            nrFaktury.append(fm.getValue(NUMER_FAKTURY_FIELD_CN) + "");
        }

		summary = nrFaktury.toString();

        if (document instanceof OfficeDocument){
            ((OfficeDocument)document).setSummaryOnly(summary);
		}

        document.setTitle(summary);
        document.setDescription(summary);
    }

    /**
     * Pola NETTO i VAT s� readonly po najnowszych zmianach i musz� by� aktualizowane bazuj�c na innych wprowadzonych
     * warto�ciach
     *
     * @param document
     * @param fm
     * @param fieldValues
     * @throws EdmException
     */
    private static void handleNettoVatFields(Document document, FieldsManager fm, Fields fieldValues) throws EdmException {
        if(hasVatEntries(fm))  {
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                ps = DSApi.context().prepareStatement(
                        "SELECT COUNT(ve.id) as count, SUM(ve.vat_amount) as vat, SUM(ve.netto_amount) as netto\n" +
                            "  FROM [dsg_vat_entry] ve,\n" +
                            "  dsg_invoice_ic_multiple_value mv\n" +
                            "WHERE mv.field_cn = 'VATS'\n" +
                            " AND mv.field_val = ve.id\n" +
                            " AND mv.document_id = ?\n" +
                            " GROUP BY mv.document_id");
                ps.setLong(1, document.getId());
                rs = ps.executeQuery();
                if(rs.next() && rs.getInt("count") > 0){//przy tworzeniu nie b�dzie jeszce wpis�w vat, wtedy nie sprawdzamy sumy
                    fieldValues.put(VAT_CN, rs.getBigDecimal("vat"));
                    fieldValues.put(KWOTA_NETTO_FIELD_CN, rs.getBigDecimal("netto"));
                    verifyAmounts(fieldValues);
                } else {
//                    fieldValues.put(VAT_CN, BigDecimal.ZERO);
//                    fieldValues.put(KWOTA_NETTO_FIELD_CN, fm.getKey(KWOTA_BRUTTO_FIELD_CN));
                }
            } catch (SQLException ex) {
                throw new EdmException(ex);
            } finally {
                DbUtils.closeQuietly(rs);
                DSApi.context().closeStatement(ps);
            }
        }
        if(fieldValues.get(VAT_CN) == null || fieldValues.get(VAT_CN).toString().length() == 0){
            fieldValues.put(VAT_CN, BigDecimal.ZERO);
        }
        if(fieldValues.get(KWOTA_NETTO_FIELD_CN) == null || fieldValues.get(KWOTA_NETTO_FIELD_CN).toString().length() == 0){
            fieldValues.put(KWOTA_NETTO_FIELD_CN, fm.getKey(KWOTA_BRUTTO_FIELD_CN));
        }
    }

	private static void dicInvoiceToSender(DicInvoice dic, Sender sender) throws EdmException {
		if (dic.getName() != null) {
			sender.setFirstname(TextUtils.trimmedStringOrNull(dic.getName(), 50));
		}
		if (dic.getMiejscowosc() != null) {
			sender.setLocation(TextUtils.trimmedStringOrNull(dic.getMiejscowosc(), 50));
		}
		if (dic.getKod() != null) {
			sender.setZip(TextUtils.trimmedStringOrNull(dic.getKod(), 6));
		}
		if (dic.getNip() != null) {
			sender.setNip(TextUtils.trimmedStringOrNull(dic.getNip(), 20));
		}
		if (dic.getUlica() != null) {
			sender.setStreet(TextUtils.trimmedStringOrNull(dic.getUlica(), 50));
		}
		if (dic.getNumerKontaBankowego() != null) {
			sender.setRemarks(TextUtils.trimmedStringOrNull(dic.getNumerKontaBankowego(), 200));
		}
		sender.create();
	}

    /**
	 * Inicjalizuje proces akceptacji - metoda powinna by� wywo�ywana tylko raz
	 * @param document
	 * @throws pl.compan.docusafe.core.EdmException
	 */
	public void initAcceptanceProcess(OfficeDocument document) throws EdmException {
		getAcceptanceManager().start(document);
        DocFacade df = Documents.document(document.getId());
		FieldsManager fm = df.getFieldsManager();
        df.getFields().put(STATUS_FIELD_CN, 10);
        df.getFields().put(STATUS_FOR_USER_FIELD_CN, 10);
        df.getFields().put(EXPORT_TYPE_FIELD_CN, 20);

		String cn = fm.getEnumItemCn(RODZAJ_FAKTURY_CN);
        log.info("initAcceptanceProcess:" + fm.getKey(DZIAL_FIELD_CN) + " - " + fm.getKey(CENTRUM_KOSZTOWE_FIELD_CN));
        if(fm.getKey(DZIAL_FIELD_CN) != null && ((Collection)fm.getKey(CENTRUM_KOSZTOWE_FIELD_CN)).isEmpty()){
            Integer centrumId = fm.getEnumItem(DZIAL_FIELD_CN).getCentrum();
            try {
                CentrumKosztow centrum = CentrumKosztow.find(centrumId);
                if (centrum.isAutoAdded()) {
                    CentrumKosztowDlaFaktury ckdf = new CentrumKosztowDlaFaktury();
                    ckdf.setDocumentId(document.getId());
                    ckdf.setCentrumId(centrum.getId());
                    ckdf.setAccountNumber(AccountNumber.find(centrum.getDefaultAccountNumber()).getNumber());
                    ckdf.setLocationId(centrum.getDefaultLocationId());
                    ckdf.setAmount((BigDecimal) fm.getKey(KWOTA_NETTO_FIELD_CN));
                    ckdf.setCentrumCode(centrum.getSymbol());
                    DSApi.context().session().save(ckdf);
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
		if(cn.equals(RODZAJ_ZAKUPY_UE_CN)
				&& !fm.getBoolean(CREDIT_NOTY_FIELD_CN)){
			initUeInvoice(document, fm);
        } else if(cn.equals(RODZAJ_ZAKUPY_DT_CN)
                && !fm.getBoolean(CREDIT_NOTY_FIELD_CN)){
            initDtInvoice(document, fm);
		} else if(cn.equals(RODZAJ_ZAKUPY_IMP_CN)
                && !fm.getBoolean(CREDIT_NOTY_FIELD_CN)){
			initImpInvoice(document, fm);
		} else if(cn.equals(RODZAJ_ZAKUPY_KRAJOWE_CN)
				&& !fm.getBoolean(KOREKTY_FIELD_CN)) {
			initZakupKrajowyInvoice(document, fm, cn);
		} else if(RODZAJE_ZAKUPY_KRAJOWE_CN.contains(cn)
                && !FAKTURY_ZAKUPOWE_X_WITHOUT_300_TH.contains(cn)
                && !fm.getBoolean(KOREKTY_FIELD_CN)) { //"Zakup krajowy - X" - bez "Zakup krajowy"
            initZakupKrajowyXInvoice(document, fm, cn);
        }
	}


    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
		document.getDocumentKind().handleLabels(document, Documents.document(document.getId()).getFieldsManager());
        archiveActionsVAT(document, type);
    }

    public boolean isAcceptanceButtonsForCurrentUserVisible(Document doc, FieldsManager fm) throws EdmException {
        try {
//			log.error("isAcceptanceButtonsForCurrentUserVisible");
            if (!FAKTURY_ZAKUPOWE_WITH_STRANGE_ACCEPTANCE_PROCESS.contains(fm.getEnumItemCn(RODZAJ_FAKTURY_CN))
                    || isSpecialInvoice(fm)) {
                //faktura nie nale�y do faktur zakupowych lub jest specjalna
                return true;
            } else {
                return AcceptanceCondition.checkAcceptancePermission(
                        DSUser.findByUsername(DSApi.context().getPrincipalName()),
                        null,
                        "ksiegowa");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return true; // w razie czego daj mo�liwo�� akceptacji
        }
    }

    @Override
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException {
        log.info("validate contents: \n " + values);
        if(values.get(EXPORT_TYPE_FIELD_CN) == null)                {
            values.put(EXPORT_TYPE_FIELD_CN, 20);
        }
        if(values.get(KWOTA_NETTO_FIELD_CN) == null){
            values.remove(KWOTA_NETTO_FIELD_CN);
        }
        if(values.get(VAT_CN) == null){
            values.remove(VAT_CN);
        }
        if(documentId != null){
            values.remove(STATUS_FIELD_CN);
            values.remove(STATUS_FOR_USER_FIELD_CN);
            values.remove(AKCEPTACJA_FINALNA_FIELD_CN);

            if(values.get(NUMER_FAKTURY_FIELD_CN) == null){
                throw new EdmException("Numer faktury nie mo�e by� pusty");
            }

            FieldsManager fm = Documents.document(documentId).getFieldsManager();
            if(!fm.getDocumentKind().getCn().equals(kind.getCn())){//zmiana dockinda jest bardzo dziwnie przesy�ana dalej
                fm = kind.getFieldsManager(documentId);
            }

            if((Boolean.parseBoolean((String)values.get(AKCEPTACJA_STALA_CN)) != fm.getBoolean(AKCEPTACJA_STALA_CN)) &&
            		!((Collection)fm.getKey(CENTRUM_KOSZTOWE_FIELD_CN)).isEmpty()){
            	throw new EdmException("Modyfikacja pola \"Akceptacja sta�a\" nie jest mo�liwa gdy zosta�y przypisane centra kosztowe.");
            }
            checkContractConstraint(values, fm);
            if(FAKTURY_ZAKUPOWE_CNS.contains(
                    fm.getField(RODZAJ_FAKTURY_CN).getEnumItem(
                            Integer.valueOf(values.get(RODZAJ_FAKTURY_CN).toString()))
                        .getCn())){
                values.put(DZIAL_FIELD_CN, null);
                values.put(PION_CN, null);
            }
//            log.info("{}+{}={} (class={})",
//                    values.get(KWOTA_NETTO_FIELD_CN),
//                    values.get(VAT_CN),
//                    values.get(KWOTA_BRUTTO_FIELD_CN),
//                    values.get(VAT_CN).getClass());

            if(Boolean.TRUE.equals(fm.getKey(AKCEPTACJA_FINALNA_FIELD_CN))){
                PermissionCache permCache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);
                if(!permCache.hasPermission(DSApi.context().getPrincipalName(), DSPermission.INVOICE_MODIFY_ACCEPTED.getName())){
                    throw new EdmException("Modyfikacja atrybut�w nie jest mo�liwa po akceptacji finalnej");
                }
            }
        }
    }

    /**
     * Ekstremalnie wra�liwa funkcja - zmiany mog� spowodowa� brak mo�liwo�ci zapisania dokumentu.
     * @param values
     */
    private static void verifyAmounts(Fields values) {
        if(values.containsKey(KWOTA_NETTO_FIELD_CN)
                && values.containsKey(VAT_CN)
                && values.containsKey(KWOTA_BRUTTO_FIELD_CN)){
            BigDecimal netto = new BigDecimal(values.get(KWOTA_NETTO_FIELD_CN).toString()).setScale(2);
            BigDecimal vat = new BigDecimal(values.get(VAT_CN).toString()).setScale(2);
            BigDecimal brutto = new BigDecimal(values.get(KWOTA_BRUTTO_FIELD_CN).toString()).setScale(2);
            //log.info("verifyAmounts : {} {} {}", netto, vat, brutto);
            if(netto.add(vat).subtract(brutto).abs().compareTo(AMOUNT_SUM_TOLERANCE) > 0){ //r�nica wi�ksza ni� okre�lona tolerancja
                throw new IllegalArgumentException("Suma kwot netto i VAT (" + netto.add(vat) + ") nie zgadza si� z kwot� brutto");
            }
        }
    }

    /**
     * Sprawdza czy dokument ma wype�nion� nazw� umowy w przypadku dzia��w, kt�re tego wymagaj�
     * @param values
     * @param fm
     * @throws EdmException
     */
    private void checkContractConstraint(Map<String, Object> values, FieldsManager fm) throws EdmException {
        Field dzial =  fm.getField(DZIAL_FIELD_CN);
        Object dzialId =values.get(DZIAL_FIELD_CN);

        if(dzialId != null){
            EnumItem ei = dzial.getEnumItem((Integer) dzial.simpleCoerce(dzialId));
//                log.info("dzia�:" + ei.getCentrum());

            CentrumKosztow ck = CentrumKosztow.find(ei.getCentrum());
            if(ei.getCentrum() != null
                    && ck.isContractNeeded()
                    && values.get(NAZWA_PROJEKTU_FIELD_CN) == null) {
                throw new EdmException("Nazwa umowy musi zosta� wype�niona dla dzia�u " + ei.getCn());
            }
            log.info("umowa:" + values.get(NAZWA_PROJEKTU_FIELD_CN));
        }
    }

    public static void discardToCn(DocumentLogic logic, OfficeDocument document, String activity, String cn, Object objectId) throws EdmException{
		boolean finalAcceptance;
        FieldsManager fm = document.getFieldsManager();
		if((finalAcceptance = logic.getAcceptanceManager().isFinalAccepted(document, fm))
                && !DSApi.context().isAdmin()
				&& !GroupPermissions.canRejectFinalAcceptance()){
			throw new EdmException("Nie masz uprawnie� do odrzucenia akceptacji finalnej");
		}
		List<DocumentAcceptance> das;
		Set<String> cns = new HashSet<String>();
		das = DocumentAcceptance.find(document.getId());
		for(DocumentAcceptance da: das){
			cns.add(da.getAcceptanceCn());
		}
		cns.retainAll(IntercarsAcceptanceMode.allCnsAfter(cn));//zostaj� cny do usuni�cia
		//ta akceptacja jest niezale�na od innych
		//TODO - utworzy� lepszy mechanizm na tego typu obej�cia
		if(!cn.equals("product_manager")){
			cns.remove("product_manager");
		}
		log.info("cns = " + cns );
		for(DocumentAcceptance da: das){
			if(cns.contains(da.getAcceptanceCn())){
                if(objectId == null || da.getObjectId().equals(objectId)){
				    DSApi.context().session().delete(da);
                }
			}
		}
		if(finalAcceptance){
			log.info("Restartowanie procesu JBPM");
			logic.getAcceptanceManager().start(document);
		}
        LabelsManager.removeLabelsByName(document.getId(), InvoiceICLogic.ASSECO_LABEL);
		logic.getAcceptanceManager().refresh(document,fm, activity);
	}

	public void discardToCn(OfficeDocument document, String activity, ActionEvent event, String cn, Object objectId) throws EdmException{
		discardToCn(this, document, activity, cn, objectId);
	}

	public static void checkCanFinishICProcess(OfficeDocument document, FieldsManager fm) throws ClassCastException, EdmException {
		if (document instanceof OutOfficeDocument) {
			return;
		}
		String error = "Nie mo�na zako�czy� pracy z dokumentem: ";

        //dokument wycofany zawsze mo�na zako�czy�
        if(STATUS_WYCOFANY.equals(fm.getEnumItemCn(STATUS_FIELD_CN))){
            return;
        }

		try {
			//TODO: ustali� metod�, kt�ra jest ZAWSZE wywo�ywana przy utworzeniu nowego dokumentu lub nowego procesu kancelaryjnego
			if (fm.getValue(JBPM_PROCESS_ID) == null) {
				instance.initAcceptanceProcess(document);
			}
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}
		
		if (fm.getValue(NUMER_KSIEGOWANIA_FIELD_CN) == null || ((String) fm.getValue(NUMER_KSIEGOWANIA_FIELD_CN)).length() < 1) {
			throw new AccessDeniedException(error + " numer ksi�gowania musi by� uzupe�niony");
		}
		if (fm.getValue("REJESTR") == null) {
			throw new AccessDeniedException(error + " rejest musi by� uzupe�niony");
		}
		fm.initializeAcceptances();
		if (!fm.getBoolean(AKCEPTACJA_FINALNA_FIELD_CN)) {
			throw new EdmException("Proces akceptacji nie zosta� zako�czony");
		}
	}

	@Override
    public void checkCanFinishProcess(OfficeDocument document) throws AccessDeniedException, EdmException {
		checkCanFinishICProcess(document, Documents.document(document.getId()).getFieldsManager());
	}

    /**
     * W IC jest inny GUID dla tworzonych grup
     */
    @Override
    protected String getBaseGUID(Document document) throws EdmException
    {
    	String parentGuid = document.getDocumentKind().getProperties().get(DocumentKind.CENTRA_DIVISION);
		DSDivision parent = DSDivision.find(parentGuid != null ? parentGuid : DSDivision.ROOT_GUID);
		return parent.getGuid();
    }

	@Override
	public boolean isReadPermissionCode(String permCode) {
		return GROUP_READ_PERMISSION_PATTERN.matcher(permCode).matches();
	}

    @Override
    public boolean searchCheckPermissions(Map<String, Object> values) {
        try {
            if(DSApi.context().isAdmin() || DSApi.context().getDSUser().inDivisionByGuid("INVOICE_READ")){
                return false;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return true;
    }

    /**
     * Nadawnie uprawnien wedlug nowej metody
     */
    public void documentPermissions(Document document) throws EdmException {
    	Set<PermissionBean> perms = new HashSet<PermissionBean>();
    	
    	FieldsManager fm = Documents.document(document.getId()).getFieldsManager();
		List<CentrumKosztowDlaFaktury> list = (List<CentrumKosztowDlaFaktury>) fm.getValue(CENTRUM_KOSZTOWE_FIELD_CN);

		if(Documents.document(document.getId()).getFields().get(STATUS_FIELD_CN, EnumItem.class).getCn().equals(STATUS_WYCOFANY)){
			rejectedDocumentPermissions(list, perms);
		} else {
			for (CentrumKosztowDlaFaktury centrum : list) {
				//createPermission(document, "Faktury kosztowe - MPK " + centrum.getCentrumCode() + " - odczyt", "INVOICE_" + centrum.getCentrumId() + "_READ", new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS});

				String readName = "Faktury kosztowe - MPK " + centrum.getCentrumCode() + " - odczyt";
				String readGuid = "INVOICE_" + centrum.getAcceptingCentrumId() + "_READ";

				perms.add(new PermissionBean(ObjectPermission.READ,readGuid,ObjectPermission.GROUP,readName));
				perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,readGuid,ObjectPermission.GROUP,readName));

				//createPermission(document, "Faktury kosztowe - MPK " + centrum.getCentrumCode() + " - modyfikacja", "INVOICE_" + centrum.getCentrumId() + "_MODIFY", new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS});

				String writeName = "Faktury kosztowe - MPK " + centrum.getCentrumCode() + " - modyfikacja";
				String writeGuid = "INVOICE_" + centrum.getAcceptingCentrumId() + "_MODIFY";

				perms.add(new PermissionBean(ObjectPermission.MODIFY, writeGuid,ObjectPermission.GROUP,writeName));
				perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, writeGuid,ObjectPermission.GROUP,writeName));

				AccountNumber an = centrum.getAccountNumber() != null ? AccountNumber.findByNumber(centrum.getAccountNumber()) : null;
				if (an != null) {
					//createPermission(document, "Faktury kosztowe - KONTO " + an.getNumber() + " - odczyt", "INVOICE_ACCOUNT_" + an.getId() + "_READ", new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS});
					readName = "Faktury kosztowe - KONTO " + an.getNumber() + " - odczyt";
					readGuid = "INVOICE_ACCOUNT_" + an.getId() + "_READ";

					perms.add(new PermissionBean(ObjectPermission.READ,readGuid,ObjectPermission.GROUP,readName));
					perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,readGuid,ObjectPermission.GROUP,readName));
				}
			}
		
			//DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "INVOICE_READ", ObjectPermission.GROUP);
			perms.add(new PermissionBean(ObjectPermission.READ,"INVOICE_READ",ObjectPermission.GROUP));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"INVOICE_READ",ObjectPermission.GROUP));

			//DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.READ_ATTACHMENTS}, "INVOICE_MODIFY", ObjectPermission.GROUP);

			perms.add(new PermissionBean(ObjectPermission.MODIFY,"INVOICE_MODIFY",ObjectPermission.GROUP));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"INVOICE_MODIFY",ObjectPermission.GROUP));
			//	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"INVOICE_MODIFY",ObjectPermission.GROUP));
			
			perms.add(new PermissionBean(ObjectPermission.MODIFY,"INVOICE_ADMIN",ObjectPermission.GROUP));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"INVOICE_ADMIN",ObjectPermission.GROUP));
		}
		
		//DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "INVOICE_ADMIN", ObjectPermission.GROUP);
		
		setUpPermission(document, perms);
    }

    @Override
    public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException {
		String channel = document.getDocumentKind().getChannel(document.getId());
		if (channel != null) {
			List<ProcessCoordinator> cords = ProcessCoordinator.find(DocumentLogicLoader.INVOICE_IC_KIND, channel);
			if (cords!= null && cords.size()>0) {
				return cords.get(0);
			} else {
				return null;
			}
		}
		return null;
	}

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
		Map<String, Object> values = new HashMap<String, Object>();
//		if (fm.getValue(KWOTA_NETTO_FIELD_CN) != null && fm.getValue(LOKALIZACJA_CN) != null) {
//			values.put(LOKALIZACJA_CN, 267);
//		}
		if (DocumentLogic.TYPE_ARCHIVE != type) {
			values.put(STATUS_FIELD_CN, STATUS_W_TRAKCIE);
            values.put(STATUS_FOR_USER_FIELD_CN, STATUS_W_TRAKCIE);
		}
		fm.reloadValues(values);
	}
    
    /**
     * Przekazuje true <=> dla dokument�w tego rodzaju mo�na generowa� obraz dokumentu w postaci PDF.
     */
	@Override
    public boolean canGenerateDocumentView() {
		return true;
	}
    
    /**
     * Dla podanego dokumentu generowany jest jego obraz w postaci PDF.
     * 
     * @return wygenerowany plik PDF
     */
	@Override
    public File generateDocumentView(Document document) throws EdmException {
		File pdf = null;
		try {
			pdf = File.createTempFile("docusafe_tmp_", ".pdf");
			OutputStream os = new BufferedOutputStream(new FileOutputStream(pdf));

			PdfUtils.attachmentsAsPdf(new Long[]{document.getId()},
					/*uwagi*/ false, os,
					/*wstep*/ createDocumentSummary(document),
					/*keywords*/ null,
					/*zako�czenie*/ null,
					/*pomini�te na pocz�tku*/ false);
			os.close();

			return pdf;
		} catch (Exception e) {
			if (pdf != null) {
				pdf.delete();
			}
			throw new EdmException("B��d podczas generowania obrazu dokumentu", e);
		}
	}
    
    /**
     * Generuje elementu dokumentu PDF opisuj�cy g��wne informacje danego dokumentu.
     */
    @SuppressWarnings("unchecked")
    private Element createDocumentSummary(Document document) throws EdmException {
		StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);

		DocumentKind documentKind = document.getDocumentKind();
		FieldsManager fm = documentKind.getFieldsManager(document.getId());
		fm.initialize();
		fm.initializeAcceptances();

		OfficeDocument officeDocument = null;
		if (document instanceof OfficeDocument) {
			officeDocument = (OfficeDocument) document;
		}
		try {
			File fontDir = new File(Docusafe.getHome(), "fonts");
			File arial = new File(fontDir, "arial.ttf");
			BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
					BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			Font font = new Font(baseFont, 18);
			Font smallFont = new Font(baseFont, 14);

			// PODSTAWOWE DANE
			Paragraph paragraph = new Paragraph("FAKTURA KOSZTOWA", font);
			paragraph.setAlignment(Element.ALIGN_CENTER);
			paragraph.setSpacingBefore(15);
			Chapter beginning = new Chapter(paragraph, 2);
			beginning.setNumberDepth(0);

			if (officeDocument != null && officeDocument.getOfficeNumber() != null) {
				beginning.add(createParagraph(sm.getString("NumerKancelaryjny") + ": " + officeDocument.getOfficeNumber() + "\n\n", Element.ALIGN_CENTER, font));
				
			} else {
				beginning.add(createParagraph(sm.getString("Identyfikator") + ": " + document.getId() + "\n\n", Element.ALIGN_CENTER, font));
			}
			beginning.add(new Paragraph(sm.getString("DokumentBiznesowy"), font));

			if (officeDocument != null && officeDocument.getType() == DocumentType.INCOMING) {
				beginning.add(new Paragraph(sm.getString("DataPrzyjecia") + ": " + DateUtils.formatCommonDate(((InOfficeDocument) officeDocument).getIncomingDate()), font));
				
			} else {
				beginning.add(new Paragraph(sm.getString("DataPrzyjecia") + ": " + DateUtils.formatCommonDate(document.getCtime()), font));
				// ATRYBUTY BIZNESOWE
			}
			beginning.add(createParagraph(fm, DATA_WYSTAWIENIA_FIELD_CN, font));
			beginning.add(createParagraph(fm, DATA_PLATNOSCI_FIELD_CN, font));
			beginning.add(createParagraph(fm, DATA_ZAPLATY_FIELD_CN, font));
			if (fm.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_KIND)) {
				beginning.add(createParagraph(fm, DATA_ZAKSIEGOWANIA_FIELD_CN, font));
				
			}
			beginning.add(createParagraph(fm, NUMER_FAKTURY_FIELD_CN, font));
			beginning.add(createParagraph(fm, DOSTAWCA_FIELD_CN, font));
			if (fm.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_KIND)) {
				beginning.add(createParagraph(fm, KWOTA_BRUTTO_FIELD_CN, font));
				
			} else {
				beginning.add(createParagraph(fm, KWOTA_NETTO_FIELD_CN, font));
				// NUMER KONTA BANKOWEGO
				
			}
			beginning.add(createParagraph(fm, NUMER_RACHUNKU_BANKOWEGO_CN, font));
			beginning.add(new Paragraph(" "));

			// OPIS TOWARU
			if (fm.getDocumentKind().getCn().equals(DocumentLogicLoader.INVOICE_KIND)) {
				beginning.add(createParagraph(fm, OPIS_TOWARU_FIELD_CN, font));
				
			}
			beginning.add(new Paragraph(" "));

			// AKCEPTACJE OG�LNE
			beginning.add(createParagraph("Akceptacje og�lne", Element.ALIGN_CENTER, font));

			for (DocumentAcceptance documentAcceptance : fm.getAcceptancesState().getGeneralAcceptances()) {
				beginning.add(new Paragraph(documentAcceptance.getDescription(), font));
			}
			beginning.add(new Paragraph(" "));

			// AKCEPTACJE CENTRUM KOSZTOWEGO
			paragraph = createParagraph("Akceptacje centrum kosztowego", Element.ALIGN_CENTER, font);
			beginning.add(paragraph);

			int[] widths = new int[1 + fm.getAcceptancesDefinition().getFieldAcceptances().size()];
			for (int i = 0; i < widths.length; i++) {
				widths[i] = 1;
				
			}
			PdfPTable table = new PdfPTable(widths.length);
			table.setWidths(widths);
			table.setWidthPercentage(100);

			// tworzenie nag��wka tabeli
			table.addCell(new Phrase("MPK (Kwota)", font));
			for (Acceptance acceptance : fm.getAcceptancesDefinition().getFieldAcceptances().values()) {
				table.addCell(new Phrase(acceptance.getName(), font));
			}

			// tworzenie tre�ci tabeli
			String numerKontaTmp = "";
			List<CentrumKosztowDlaFaktury> list = (List<CentrumKosztowDlaFaktury>) fm.getValue(fm.getAcceptancesDefinition().getCentrumKosztFieldCn());
			for (CentrumKosztowDlaFaktury centrum : list) {
				CentrumKosztow ck = CentrumKosztow.find(centrum.getCentrumId());

				if (centrum.getAccountNumber() != null) {
					AccountNumber an = AccountNumber.findByNumber(centrum.getAccountNumber());
					numerKontaTmp = an.getNumber() + " " + an.getName();
				} else {
					numerKontaTmp = "(brak konta)";

				}
				table.addCell(new Phrase(ck.getSymbol() + " " + ck.getName() + "\n" + numerKontaTmp + "\nKwota " + centrum.getAmount(), font));
				for (String acceptanceCn : fm.getAcceptancesDefinition().getFieldAcceptances().keySet()) {
					DocumentAcceptance documentAcceptance = fm.getAcceptancesState().getFieldAcceptance(acceptanceCn, centrum.getId());
					table.addCell(new Phrase(documentAcceptance != null ? documentAcceptance.getDescriptionWithoutCn() : "", font));
				}
			}

			table.setSpacingBefore(12);
			beginning.add(table);

			beginning.add(new Paragraph(" "));

			// UWAGI
			if (officeDocument != null) {
				List lstRemarks = officeDocument.getRemarks();
				com.lowagie.text.List remarks = new com.lowagie.text.List(false, 10);
				boolean done = false;
				for (Iterator iter = lstRemarks.iterator(); iter.hasNext();) {
					if (!done) {
						paragraph = new Paragraph(sm.getString("Uwagi") + " :", smallFont);
						paragraph.setSpacingBefore(20);
						paragraph.setAlignment(Element.ALIGN_CENTER);
						beginning.add(paragraph);
						done = true;
					}
					Remark remark = (Remark) iter.next();
					remarks.add(new ListItem(remark.getContent() + " (" + DSUser.safeToFirstnameLastname(remark.getAuthor()) + " " + DateUtils.formatJsDateTime(remark.getCtime()) + ")", smallFont));
				}
				beginning.add(remarks);
			}
			return beginning;
		} catch (DocumentException e) {
			log.error(e.getMessage(), e);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		return null;
	}
    
    /**
     * Tworzy paragraf dla dokumentu PDF opisuj�cy warto�� danego pola z dockinda.
     */
    private Paragraph createParagraph(FieldsManager fm, String fieldCn, Font font) throws EdmException {
		Field field = fm.getField(fieldCn);
		Object description = fm.getDescription(fieldCn);
		return new Paragraph(field.getName() + ": " + (description != null ? description.toString() : ""), font);
	}
    
    /**
     * Tworzy paragraf zawieraj�cy dany tekst.
     */
    private Paragraph createParagraph(String text, int alignment, Font font) throws EdmException {
		Paragraph paragraph = new Paragraph(text, font);
		paragraph.setAlignment(alignment);
		return paragraph;
	}
    
    /**
     * Zwraca polityk� bezpiecze�stwa dla dokument�w tego rodzaju.
     */
	@Override
    public int getPermissionPolicyMode() {
		return PermissionManager.BUSINESS_LIGHT_POLICY;
	}

	@Override
	public TaskListParams getTaskListParams(DocumentKind kind, long documentId) throws EdmException {
		TaskListParams ret = new TaskListParams();

		FieldsManager fm = Documents.document(documentId).getFieldsManager();
		DicInvoice dic = (DicInvoice) fm.getValue(DOSTAWCA_FIELD_CN);

		StringBuilder tmp = new StringBuilder();
		List<CentrumKosztowDlaFaktury> centrumList = CentrumKosztowDlaFaktury.findByDocumentId(documentId);
		for (CentrumKosztowDlaFaktury centrum : centrumList) {
			tmp.append(centrum.getDescription() + " | ");
		}

		ret.setStatus(ObjectUtils.toString(fm.getValue(STATUS_FOR_USER_FIELD_CN),null));
		ret.setDocumentNumber(ObjectUtils.toString(fm.getValue(NUMER_NABYCIA_FIELD_CN),null));
		ret.setAmount((BigDecimal)fm.getKey(KWOTA_NETTO_FIELD_CN));

		ret.setCategory(fm.getValue(RODZAJ_FAKTURY_CN)
					+ (fm.getBoolean(InvoiceICLogic.CREDIT_NOTY_FIELD_CN) ? " (CREDIT NOTY)" :"")
					+ (fm.getBoolean(InvoiceICLogic.KOREKTY_FIELD_CN) ? " (Korekty)" : ""));

		ret.setAttribute(StringUtils.left(tmp.toString(), 20), 0);
		ret.setAttribute(
				dic != null
				? StringUtils.left(dic.getNumerKontrahenta().toString(), 20)
				: "brak",
			1);

		ret.setAttribute(
				fm.getValue(DATA_PLATNOSCI_FIELD_CN) != null
				? DateUtils.formatCommonDate((Date) fm.getValue(DATA_PLATNOSCI_FIELD_CN))
				: "brak",
			2);
		ret.setAttribute(ObjectUtils.toString(fm.getValue(NAZWA_PROJEKTU_FIELD_CN), null), 3);

		ret.setDocumentDate((Date)fm.getValue(DATA_WYSTAWIENIA_FIELD_CN));

		return ret;
	}


	
	@Override
	public boolean canReadDocumentDictionaries() throws EdmException {
		return true;
	}
	
	public void discard(Document doc, String act) throws EdmException {

		WorkflowFactory.getInstance().manualFinish(act, false);

		FieldsManager fm = doc.getDocumentKind().getFieldsManager(doc.getId());
		Calendar cal = Calendar.getInstance();

		fm.initialize();
		Map<String, Object> values = fm.getFieldValues();
		values.put(STATUS_FIELD_CN, 6);
        values.put(STATUS_FOR_USER_FIELD_CN, 6);

		doc.getDocumentKind().setWithHistory(doc.getId(), values, false);

		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Archiwum Dokument�w Finansowych");
		folder = folder.createSubfolderIfNotPresent("Dokumenty odrzucone");
        LabelsManager.removeLabelsByName(doc.getId(), InvoiceICLogic.ASSECO_LABEL);
		Date dat = (Date) fm.getValue(DATA_WYSTAWIENIA_FIELD_CN);
		cal.setTime(dat);

		folder = folder.createSubfolderIfNotPresent(String.valueOf(cal.get(Calendar.YEAR)) + "-" + String.valueOf(cal.get(Calendar.MONTH)));

		doc.setFolder(folder);

	}

	public void returnToIM(OfficeDocument doc, String activity, ActionEvent event) throws EdmException {
		String[] documentActivity = new String[1];
		documentActivity[0] = activity;
		String plannedAssignment = "";

		plannedAssignment = InvoiceICLogic.DIVISION_TO_DISCARD + "//" + WorkflowFactory.wfManualName() + "/*/realizacja";

		String curUser = DSApi.context().getDSUser().getName();

		WorkflowFactory.multiAssignment(documentActivity, curUser, new String[]{plannedAssignment}, plannedAssignment, event);
	}


	public void discardToUser(OfficeDocument doc, String activity, ActionEvent event, DSUser user) throws EdmException {
		discardToUser(this, doc, activity, event, user);
	}

	public static void discardToUser(DocumentLogic logic, OfficeDocument doc, String activity, ActionEvent event, DSUser user) throws EdmException {
		boolean finalAcceptance;
        FieldsManager fm = doc.getFieldsManager();
		if((finalAcceptance = logic.getAcceptanceManager().isFinalAccepted(doc, fm))
                && !GroupPermissions.canRejectFinalAcceptance()){
			throw new EdmException("Nie masz uprawnie� do odrzucenia akceptacji finalnej");
		}

		StringManager sm = GlobalPreferences.loadPropertiesFile(AcceptancesManager.class.getPackage().getName(), null);
        LabelsManager.removeLabelsByName(doc.getId(), InvoiceICLogic.ASSECO_LABEL);
		fm.initialize();
		fm.initializeAcceptances();
		List<DocumentAcceptance> daList = fm.getAcceptancesState().getFieldAcceptances();
		daList.addAll(fm.getAcceptancesState().getGeneralAcceptances());
		DocumentAcceptance acceptance = null;
		for (DocumentAcceptance documentAcceptance : daList) {
			if (documentAcceptance.getUsername().equals(user.getName())) {
				acceptance = documentAcceptance;
				break;
			}
		}
		if (acceptance == null) {
			event.addActionError("Nie znaleziono akceptacji");
			return;
		}
		Map<String, Object> values = fm.getFieldValues();

		Long objectId = acceptance.getObjectId();

		Persister.delete(acceptance);

		String cn = acceptance.getAcceptanceCn();
		while ((cn = doc.getDocumentKind().getAcceptancesDefinition().getNextCn(cn)) != null) {
			log.debug("InvoiceLogic.java:cn.prev = {}", cn);

			DocumentAcceptance ac = DocumentAcceptance.find(doc.getId(), cn, objectId);
			if (ac == null) {
				ac = DocumentAcceptance.find(doc.getId(), cn, null);
			}
			if (ac != null) {
				Persister.delete(ac);
				if (objectId != null) {
					if (doc instanceof OfficeDocument) {
						CentrumKosztowDlaFaktury centrum = CentrumKosztowDlaFaktury.getInstance().find(objectId);
						String tmp = sm.getString("CofnietoAkceptacjeDokumenuDlaCentrumKosztowego", ac.getAcceptanceCn(), centrum.getCentrumCode());
						((OfficeDocument) doc).addWorkHistoryEntry(
								Audit.create("withdrawAccept", DSApi.context().getPrincipalName(),
								tmp, ac.getAcceptanceCn(), null));
					}
				} else {
					if (doc instanceof OfficeDocument) {
						String tmp = sm.getString("CofnietoAkceptacjeDokumenu", ac.getAcceptanceCn());
						((OfficeDocument) doc).addWorkHistoryEntry(
								Audit.create("withdrawAccept", DSApi.context().getPrincipalName(),
								tmp, ac.getAcceptanceCn(), null));
					}
				}
				event.addActionMessage(sm.getString("Usunieto") + " " + ac.getAsFirstnameLastname() + ' ' + ac.getDescription());

			}
		}

		if (objectId != null) {
			if (doc instanceof OfficeDocument) {
				CentrumKosztowDlaFaktury centrum = CentrumKosztowDlaFaktury.getInstance().find(objectId);
				String tmp = sm.getString("CofnietoAkceptacjeDokumenuDlaCentrumKosztowego", acceptance.getAcceptanceCn(), centrum.getCentrumCode());
				((OfficeDocument) doc).addWorkHistoryEntry(
						Audit.create("withdrawAccept", DSApi.context().getPrincipalName(),
						tmp, acceptance.getAcceptanceCn(), null));
			}
		} else {
			if (doc instanceof OfficeDocument) {
				String tmp = sm.getString("CofnietoAkceptacjeDokumenu", acceptance.getAcceptanceCn());
				((OfficeDocument) doc).addWorkHistoryEntry(
						Audit.create("withdrawAccept", DSApi.context().getPrincipalName(),
						tmp, acceptance.getAcceptanceCn(), null));
			}
		}

		values.put(STATUS_FIELD_CN, 6);
        values.put(STATUS_FOR_USER_FIELD_CN, 6);

		doc.getDocumentKind().setWithHistory(doc.getId(), values, false);

//		log.info("FinalAcceptance = " + finalAcceptance);
		if(finalAcceptance){
			log.info("Restartowanie procesu JBPM");
			logic.getAcceptanceManager().start(doc);
		}
        fm = doc.getFieldsManager();
		logic.getAcceptanceManager().refresh(doc,fm, null);
	}
	
	@Override
	public void correctImportValues(Document document, Map<String, Object> values, StringBuilder builder) throws EdmException
	{
		DocumentKind documentKind = DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND);
    	Map<String, Object> valuesTmp = new HashMap<String, Object>(values);
		Set<String> valuesSet = valuesTmp.keySet();
		if(document instanceof InOfficeDocument)
    	{
			((InOfficeDocument)document).setSummary(documentKind.getName());
    	}
		
		if(document instanceof InOfficeDocument && values.get("DOSTAWCA") != null)
    	{
			DicInvoice dic = DicInvoice.getInstance().find(new Long(values.get("DOSTAWCA").toString()));
			if(dic != null)
			{
	    		Sender sender = new Sender();
				dicInvoiceToSender(dic, sender)	;
	        	((InOfficeDocument)document).setSender(sender);
			}	
    	}
	}
	
	public static List<InvoiceInfo> parseAndSaveFVFile(File file) throws EdmException
    {
		BufferedReader reader = null;
        List<InvoiceInfo> list = new ArrayList<InvoiceInfo>();
        try
        {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "cp1250"));
            int lineCount = 0;
            CSVStrategy csvStrategy = new CSVStrategy(
                            /*  delimiter   */ ';',
                            /* encapsulator */ '"',
                            /* commentStart */ CSVStrategy.COMMENTS_DISABLED
                    );
            CSVParser parser = new CSVParser(reader,csvStrategy);
            String[] parts;
            while ((parts = parser.getLine()) != null)
            {
                lineCount++;

                // pierwsza linia to nag��wek, kt�ry omijam
                if (lineCount == 1){
                    continue;
                }

                InvoiceInfo info = new InvoiceInfo();
                info.setCtime(new Date());
                info.setCreatingUser(DSApi.context().getPrincipalName());
                info.setLineNumber(lineCount);
                info.setProcessStatus(InvoiceInfo.PROCESS_STATUS_NEW);
                info.setInvoiceType(InvoiceInfo.INVOICE_TYPE_KOSZT);


                if (parts.length > 2)
                {
                	if(parts[0].length()==0) continue;
                    info.setInvoiceNumber(parts[2].trim());

                }
                if (parts.length > 3)
                {
                	try
                	{
                		info.setInvDate(DateUtils.parseDateAnyFormat(parts[3].replace(".", "-")));
                	}
                	catch(Exception e)
                	{
                		throw new EdmException("Blad w linii " + lineCount + " w 4 kolumnie : "+parts[3], e);
                	}
                }
                if (parts.length > 7)
                {
                    String amount = parts[7].trim();
                    amount = amount.replace("\"", "");
                    amount = amount.replace(" ", "");
                    amount = amount.replace(",", ".");
                    try
                    {
                        info.setAmountBrutto(Double.parseDouble(amount));
                    }
                    catch (NumberFormatException e)
                    {
                    	log.debug("", e);
                        info.setStatus(InvoiceInfo.STATUS_PARSE_ERROR);
                        info.setErrorInfo("niepoprawna kwota");
                        info.setAmountBrutto(null);
                    }
                }
                if (parts.length > 19)
                    info.setNip(parts[19]);
                if (parts.length < 19)
                {
                    info.setStatus(InvoiceInfo.STATUS_PARSE_ERROR);
                    info.setErrorInfo("nie podano wszystkich informacji");
                }

                list.add(info);

                if (info.getStatus() == null)
                {
                    // poprawnie odczytano z pliku - szukamy odpowiedni dokument
                    DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND);
                    DockindQuery query = new DockindQuery(0,0);
                    query.setDocumentKind(kind);

                    query.field(kind.getFieldByCn(InvoiceLogic.NUMER_FAKTURY_FIELD_CN), info.getInvoiceNumber());
                    query.otherClassField(kind.getFieldByCn(InvoiceLogic.DOSTAWCA_FIELD_CN),"nip", DicInvoice.cleanNip(info.getNip()));
                    //query.field(kind.getFieldByCn(InvoiceLogic.REJESTR_FIELD_CN),info.getNip());
                    SearchResults<Document> searchResults = DocumentKindsManager.search(query);
                    if (searchResults.totalCount() == 0) {
                        info.setStatus(InvoiceInfo.STATUS_NOT_FOUND);
//                        log.error("nie znaleziono dokumentu : nip = " + DicInvoice.cleanNip(info.getNip()) + " numer faktury = " + info.getInvoiceNumber() );
                    } else if(searchResults.totalCount() > 1000) {
                        info.setStatus(InvoiceInfo.STATUS_PARSE_ERROR);
                    } else {
                        Document[] documents = searchResults.results();
                        checkDocument(documents[0], info);

                        if (documents.length > 1)
                        {
                            info.setStatus(InvoiceInfo.STATUS_TOO_MANY_FOUND);
                            for (int i=1; i < documents.length; i++)
                            {
                                InvoiceInfo copy = new InvoiceInfo();
                                copy.setAmountBrutto(info.getAmountBrutto());
                                copy.setCreatingUser(info.getCreatingUser());
                                copy.setCtime(info.getCtime());
                                copy.setInvoiceNumber(info.getInvoiceNumber());
                                copy.setLineNumber(info.getLineNumber());
                                copy.setInvoiceType(InvoiceInfo.INVOICE_TYPE_KOSZT);
                                checkDocument(documents[i], copy);
                                copy.setStatus(InvoiceInfo.STATUS_TOO_MANY_FOUND);
                                Persister.create(copy);
                                list.add(copy);
                            }
                        }
                    }
                }
                Persister.create(info);

            }
            return list;
        }
        catch (IOException e)
        {
            throw new EdmException("B��d podczas czytania pliku z rejestrem VAT");
        }
        finally
        {
        	if (reader!=null)
        	{
        		try { reader.close(); } catch (Exception e1) { }
        	}
        }
    }
	
	public static List<Map<String,Object>> prepareInvoice2Beans(List<InvoiceInfo> invoiceInfos) throws EdmException
    {
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        for (InvoiceInfo info : invoiceInfos)
        {
            Map<String,Object> bean = new LinkedHashMap<String, Object>(); 
            bean.put("id", info.getId());
            bean.put("lineNumber", info.getLineNumber());
            bean.put("status", info.getStatus());
            bean.put("statusDescription", info.getStatusDescription());
            bean.put("invoiceNumber", info.getInvoiceNumber());
            bean.put("amount", info.getAmountBrutto());
            bean.put("documentId", info.getDocumentId());
            bean.put("processStatus", info.getProcessStatus());
            bean.put("origAmount", info.getOrigAmountBrutto() == null ? 0 : info.getOrigAmountBrutto());
            if (info.getDocumentId() != null)
            {
                Document document = Document.find(info.getDocumentId());
                if (document.getAttachments() != null && document.getAttachments().size() > 0)
                {
                    AttachmentRevision revision = document.getAttachments().get(0).getMostRecentRevision();
                    if (revision != null && ViewServer.mimeAcceptable(revision.getMime()))
                    {           
                        bean.put("viewerLink", "/viewserver/viewer.action?id="+revision.getId()+"&fax=false&width=1000&height=750");
                    }
                }
            }
            list.add(bean);
        }
        return list;
    }
	
	public static void deleteInvoiceInfos(String username) throws EdmException
    {
        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().prepareStatement("delete from DSG_INVOICE_INFO where creatingUser = ?");
            ps.setString(1, username);
            ps.executeUpdate();
            
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);        	
        }
    }
	
	public static void deleteInvoiceInfos(String username, Long[] ids) throws EdmException
    {
        PreparedStatement ps = null;
       
        for(Long id:ids)
        {
	        try
	        {
	            ps = DSApi.context().prepareStatement("delete from DSG_INVOICE_INFO where creatingUser = ? and id = ?");
	            ps.setString(1, username);
	            ps.setLong(2, id);
	            ps.executeUpdate();
	            
	        }
	        catch (SQLException e)
	        {
	            throw new EdmSQLException(e);
	        }
	        finally
	        {
	        	DSApi.context().closeStatement(ps);        	
	        }
        }
    }

    public static void chanegeProcessStatusInvoiceInfos(String username, Long[] ids, Integer processStatus) throws EdmException {
        PreparedStatement ps = null;
        try {
            ps = DSApi.context().prepareStatement("update DSG_INVOICE_INFO set processStatus = ? where creatingUser = ? and id = ?");
            for (Long id : ids) {
                ps.setInt(1, processStatus);
                ps.setString(2, username);
                ps.setLong(3, id);
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            DSApi.context().closeStatement(ps);
        }
    }

	private static void initImpInvoice(OfficeDocument document, FieldsManager fm) throws EdmException {
		log.info("dodawanie akceptacji domy�lnych");
		CentrumKosztowDlaFaktury ckdf = new CentrumKosztowDlaFaktury();
		ckdf.setDocumentId(document.getId());
		ckdf.setCentrumId(CentrumKosztow.findBySymbol(RODZAJ_ZAKUPY_IMP_CN).getId());
		ckdf.setAccountNumber("300-IMP");
		ckdf.setCentrumCode(RODZAJ_ZAKUPY_IMP_CN);
		try {
			ckdf.setRealAmount(new BigDecimal(fm.getKey(KWOTA_NETTO_FIELD_CN).toString()));
		} catch (NumberFormatException nfe) {
			throw new NumberFormatException("Error while parsing " + fm.getValue(KWOTA_NETTO_FIELD_CN).toString());
		}
        if(fm.getKey(LOKALIZACJA_CN) != null){
            ckdf.setLocationId(Integer.parseInt(fm.getKey(LOKALIZACJA_CN) + ""));
        }
		ckdf.setAcceptanceMode(IntercarsAcceptanceMode.LAST_ONLY.ordinal());
		DSApi.context().session().save(ckdf);

		IC_ACCEPTANCES_MANAGER.refresh(document,fm, null);
	}

	private static void initZakupKrajowyInvoice(OfficeDocument document, FieldsManager fm, String rodzaj) throws EdmException {
		log.info("dodawanie akceptacji domy�lnych");
		CentrumKosztowDlaFaktury ckdf = new CentrumKosztowDlaFaktury();
		ckdf.setDocumentId(document.getId());
		ckdf.setCentrumId(CentrumKosztow.findBySymbol(rodzaj).getId());
		ckdf.setAccountNumber("300-TH");
		ckdf.setCentrumCode(rodzaj);
        ckdf.setClassTypeId(10); //KUP (S)
        ckdf.setVatTypeId(20);  //100% - domy�lna warto��
        ckdf.setVatTaxTypeId(10);  //bez podatku VAT
        ckdf.setConnectedTypeId(10);  //Niepowi�zane
		try {
			ckdf.setRealAmount(new BigDecimal(fm.getKey(KWOTA_NETTO_FIELD_CN).toString()));
		} catch (NumberFormatException nfe) {
			throw new NumberFormatException("Error while parsing " + fm.getValue(KWOTA_NETTO_FIELD_CN).toString());
		}
        if(fm.getKey(LOKALIZACJA_CN) != null){
            ckdf.setLocationId(Integer.parseInt(fm.getKey(LOKALIZACJA_CN) + ""));
        }
		ckdf.setAcceptanceMode(IntercarsAcceptanceMode.LAST_ONLY.ordinal());
		DSApi.context().session().save(ckdf);

		IC_ACCEPTANCES_MANAGER.refresh(document,fm, null);
	}

    private void initZakupKrajowyXInvoice(OfficeDocument document, FieldsManager fm, String rodzaj) throws EdmException {
        log.info("dodawanie centrum kosztowego dla " + rodzaj);
		CentrumKosztowDlaFaktury ckdf = new CentrumKosztowDlaFaktury();
		ckdf.setDocumentId(document.getId());
        CentrumKosztow ck = CentrumKosztow.findBySymbol(rodzaj);
		ckdf.setCentrumId(ck.getId());
		ckdf.setAccountNumber("300-TH");
		ckdf.setCentrumCode(rodzaj);
		try {
			ckdf.setRealAmount(new BigDecimal(fm.getKey(KWOTA_NETTO_FIELD_CN).toString()));
		} catch (NumberFormatException nfe) {
			throw new NumberFormatException("Error while parsing " + fm.getValue(KWOTA_NETTO_FIELD_CN).toString());
		}
        if(fm.getKey(LOKALIZACJA_CN) != null){
            ckdf.setLocationId(Integer.parseInt(fm.getKey(LOKALIZACJA_CN) + ""));
        }
		ckdf.setAcceptanceMode(ck.getAvailableAcceptanceModes().iterator().next().getId());
		DSApi.context().session().save(ckdf);

		IC_ACCEPTANCES_MANAGER.refresh(document,fm, null);
    }

	/**
	 * Inicjalizuje faktur� Zakup UE przypisuj�c jej odpowiednie centrum i akceptacje
	 * @param document
	 * @throws EdmException
	 */
	private static void initUeInvoice(Document document, FieldsManager fm) throws EdmException{
		log.info("dodawanie akceptacji domy�lnych");
		CentrumKosztowDlaFaktury ckdf = new CentrumKosztowDlaFaktury();
		ckdf.setDocumentId(document.getId());
		ckdf.setCentrumId(CentrumKosztow.findBySymbol(RODZAJ_ZAKUPY_UE_CN).getId());
		ckdf.setAccountNumber("300-UE");
		ckdf.setCentrumCode(RODZAJ_ZAKUPY_UE_CN);
		try {
			ckdf.setRealAmount(new BigDecimal(fm.getKey(KWOTA_NETTO_FIELD_CN).toString()));
		} catch (NumberFormatException nfe) {
			throw new NumberFormatException("Error while parsing "
                    + fm.getValue(KWOTA_BRUTTO_FIELD_CN).toString());
		}
		ckdf.setAcceptanceMode(IntercarsAcceptanceMode.ACCOUNANT_ONLY.ordinal());
        if(fm.getKey(LOKALIZACJA_CN) != null){
            ckdf.setLocationId(Integer.parseInt(fm.getKey(LOKALIZACJA_CN) + ""));
        }
		DSApi.context().session().save(ckdf);
		
		IC_ACCEPTANCES_MANAGER.refresh(document,fm, null);
	}

    private static void initDtInvoice(Document document, FieldsManager fm) throws EdmException{
        log.info("dodawanie akceptacji domy�lnych");
        CentrumKosztowDlaFaktury ckdf = new CentrumKosztowDlaFaktury();
        ckdf.setDocumentId(document.getId());
        ckdf.setCentrumId(CentrumKosztow.findBySymbol(RODZAJ_ZAKUPY_DT_CN).getId());
        ckdf.setAccountNumber("300-DT");
        ckdf.setCentrumCode(RODZAJ_ZAKUPY_DT_CN);
        try {
            ckdf.setRealAmount(new BigDecimal(fm.getKey(KWOTA_NETTO_FIELD_CN).toString()));
        } catch (NumberFormatException nfe) {
            throw new NumberFormatException("Error while parsing "
                    + fm.getValue(KWOTA_BRUTTO_FIELD_CN).toString());
        }
        ckdf.setAcceptanceMode(IntercarsAcceptanceMode.ACCOUNANT_ONLY.ordinal());
        if(fm.getKey(LOKALIZACJA_CN) != null){
            ckdf.setLocationId(Integer.parseInt(fm.getKey(LOKALIZACJA_CN) + ""));
        }
        DSApi.context().session().save(ckdf);

        IC_ACCEPTANCES_MANAGER.refresh(document,fm, null);
    }

	private static void checkDocument(Document document, InvoiceInfo info) throws EdmException
    {
        info.setDocumentId(document.getId());
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        BigDecimal amount = (BigDecimal) fm.getKey(InvoiceLogic.KWOTA_BRUTTO_FIELD_CN);
        info.setOrigAmountBrutto(amount.doubleValue());
        try
        {
        	info.setStatus(InvoiceInfo.STATUS_OK); 
        	if(!info.getAmountBrutto().equals(info.getOrigAmountBrutto()))
        	{
        		info.setStatus(InvoiceInfo.STATUS_WRONG_AMOUNT);
        	}
	        if(((DicInvoice)fm.getValue(InvoiceLogic.DOSTAWCA_FIELD_CN)).getNip()!=null && !((DicInvoice)fm.getValue(InvoiceLogic.DOSTAWCA_FIELD_CN)).getNip().equals(""))
	        {
	        	if(!((DicInvoice)fm.getValue(InvoiceLogic.DOSTAWCA_FIELD_CN)).getNip().replaceAll("\\D", "").equals(info.getNip()))
	        		info.setStatus(InvoiceInfo.STATUS_WRONG_NIP);
	        }
	        else if(((Date)fm.getValue(DATA_WYSTAWIENIA_FIELD_CN)).compareTo(info.getInvDate())!=0)
	        	info.setStatus(InvoiceInfo.STATUS_WRONG_DATE);
	        else if (!info.getAmountBrutto().equals(amount))
	            info.setStatus(InvoiceInfo.STATUS_NOT_EVERYTHING_MATCH);
	              
        }
        catch(Exception e){
        	info.setStatus(InvoiceInfo.STATUS_NOT_EVERYTHING_MATCH);
        }
    }

	
	@Override
	public JBPMAcceptanceManager getAcceptanceManager() {
		return IC_ACCEPTANCES_MANAGER;
	}
	
	@Override
	public void onStartProcess(OfficeDocument document) {
		log.debug("onStartProcess");
		try {
			if (document.getFieldsManager().getValue(JBPM_PROCESS_ID) == null) {
				 initAcceptanceProcess(document);
			}
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
			return;
		}
	}

    @Override
    public void onEndProcess(OfficeDocument document, boolean isManual) throws EdmException {
        DocumentKind dk = document.getDocumentKind();
        //STATUS zmienia si� wcze�niej ni� STATUS_FOR_USER
        log.info("onEndProcess {}", document.getId());
        try {
            DSApi.context().session().flush();
            FieldsManager fm = document.getFieldsManager();
            if(fm.getBoolean(AKCEPTACJA_FINALNA_FIELD_CN)
                    && fm.getEnumItemCn(STATUS_FIELD_CN).equals("KONTROLA_JAKOSCI")
                    && fm.getEnumItemCn(EXPORT_TYPE_FIELD_CN).equals("NONE")){
                dk.setOnly(document.getId(), ImmutableMap.of(STATUS_FOR_USER_FIELD_CN, STATUS_KONTROLA_JAKOSCI));
            }
        } catch (NullPointerException ex) { //null pointera nie powinno tu by� ale na wszelki wypadek obs�u�my ten przypadek
            log.error("B��d obs�u�ony: " + document.getId(), ex);
        }
    }

    public List<ProcessParameterBean> getProcessParameter(Document document) throws EdmException
	{
		Map<String,Acceptance> acceptancesDef = document.getDocumentKind().getAcceptancesDefinition().getAcceptances();
		List<ProcessParameterBean> result = new ArrayList<ProcessParameterBean>();
		List<CentrumKosztowDlaFaktury> ckfs = CentrumKosztowDlaFaktury.findByDocumentId(document.getId());		
		for(CentrumKosztowDlaFaktury ckf:ckfs)
		{
			ProcessParameterBean ppb = new ProcessParameterBean();
			ppb.setProcessName(ckf.getText());
			boolean lastFullfilled = true;
			for (Iterator<String> iter = ckf.getAcceptanceCnsList().iterator(); iter.hasNext();)
			{
				String cn = iter.next();
				String name = acceptancesDef.get(cn).getName();
				boolean checkpointFinal = !iter.hasNext();
				Long objectId = null;
				if(acceptancesDef.get(cn).getFieldCn() != null)
					objectId = ckf.getId();				
				boolean fullfilled = DocumentAcceptance.find(document.getId(), cn, objectId) != null;
				ppb.addCheckpoint(new BasicCheckPoint(name,null,checkpointFinal,fullfilled,(lastFullfilled && !fullfilled)));
				lastFullfilled = fullfilled;
			}
			result.add(ppb);
		}
		
		return result;
	}

	//Tylko uprawnienia do odczytu
	private void rejectedDocumentPermissions(List<CentrumKosztowDlaFaktury> list, Set<PermissionBean> perms) throws EdmException {
		for (CentrumKosztowDlaFaktury centrum : list) {
			String readName = "Faktury kosztowe - MPK " + centrum.getCentrumCode() + " - odczyt";
			String readGuid = "INVOICE_" + centrum.getAcceptingCentrumId() + "_READ";
			perms.add(new PermissionBean(ObjectPermission.READ, readGuid, ObjectPermission.GROUP, readName));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, readGuid, ObjectPermission.GROUP, readName));
			AccountNumber an = centrum.getAccountNumber() != null ? AccountNumber.findByNumber(centrum.getAccountNumber()) : null;
			if (an != null) {
				readName = "Faktury kosztowe - KONTO " + an.getNumber() + " - odczyt";
				readGuid = "INVOICE_ACCOUNT_" + an.getId() + "_READ";
				perms.add(new PermissionBean(ObjectPermission.READ, readGuid, ObjectPermission.GROUP, readName));
				perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, readGuid, ObjectPermission.GROUP, readName));
			}
		}
		perms.add(new PermissionBean(ObjectPermission.READ, "INVOICE_READ", ObjectPermission.GROUP));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "INVOICE_READ", ObjectPermission.GROUP));
		
		perms.add(new PermissionBean(ObjectPermission.MODIFY,"INVOICE_ADMIN",ObjectPermission.GROUP));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"INVOICE_ADMIN",ObjectPermission.GROUP));
	}
	
	public void doDockindEvent(DockindButtonAction eventActionSupport, ActionEvent event,Document document, String activity,Map<String, Object> values) throws EdmException {
        log.error("doDockindEvent!!!");
//        try {
//            IntercarsAcceptanceManager.exportAsseco(document);
//        } catch (Exception ex) {
//            throw new EdmException(ex);
//        }
        //tryExport(event, document);
    }

//    private void tryExport(ActionEvent event, Document document) {
//        try {
//            if (!Documents.document(document.getId()).getFieldsManager().getBoolean(AKCEPTACJA_FINALNA_FIELD_CN)) {
//                event.addActionError("Nie mo�na wykona� eksportu przed akceptacj� ksi�gow�");
//                return;
//            }
//
//            DSApi.context().begin();
//            AssecoImportManager manager = new AssecoImportManager();
//
//            ImportDokZkResponse idr = manager.importDokZk(document);
//            IcDataProcessorStub.ImportAnswer answer = idr.get_return();
//
//            if (answer.getSuccess() && StringUtils.isBlank(answer.getError())) {
//                Documents.document(document.getId()).getFields().put(STATUS_FIELD_CN, STATUS_SENT_TO_ASSECO_ID);
//                Documents.document(document.getId()).getFields().put(STATUS_FOR_USER_FIELD_CN, STATUS_SENT_TO_ASSECO_ID);
//                LabelsManager.addLabelByName(document.getId(), ASSECO_LABEL, DSApi.context().getPrincipalName(), false);
//                event.addActionMessage("Eksport wykonany poprawnie");
//                DSApi.context().commit();
//            } else {
//                event.addActionError("B��d podczas eksportu: " + answer.getLog() + " " + answer.getError());
//                DSApi.context().setRollbackOnly();
//            }
//        } catch (Exception e) {
//            DSApi.context().setRollbackOnly();
//            event.addActionError("B��d podczas eksportu: " + e.getMessage());
//            log.error(e.getMessage(), e);
//        }
//    }
}