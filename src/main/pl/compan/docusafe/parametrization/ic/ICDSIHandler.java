package pl.compan.docusafe.parametrization.ic;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.dbutils.DbUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.core.dockinds.dictionary.VatRate;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.service.imports.dsi.DSIBean;
import pl.compan.docusafe.service.imports.dsi.DSIImportHandler;
import pl.compan.docusafe.service.imports.dsi.ImportHelper;
import pl.compan.docusafe.service.imports.dsi.ImportValidationException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import static pl.compan.docusafe.parametrization.ic.InvoiceICLogic.*;

public class ICDSIHandler extends DSIImportHandler
{
	private final static Logger log = LoggerFactory.getLogger(ICDSIHandler.class);
	final static String DOSTAWCA_NIP = "DOSTAWCA_NIP";
	final static String DOSTAWCA_NAZWA = "DOSTAWCA_NAZWA";
	final static String DOSTAWCA_NR_KONTRAHENTA = "DOSTAWCA_NR_KONTRAHENTA";
    public static final BigDecimal PERCENT = new BigDecimal("0.01");

    public String getDockindCn()
	{
		return "invoice_ic";
	}
    
    @Override
	protected boolean isStartProces()
	{
		return true;
	}
    
	protected void prepareImport() throws Exception
	{
		if (values.get(InvoiceICLogic.RODZAJ_FAKTURY_CN).equals(InvoiceICLogic.RODZAJ_ZAKUPY_UE_CN)) {
			values.put(InvoiceLogic.WALUTA_FIELD_CN, "EUR");
		} else {
			values.put(InvoiceLogic.WALUTA_FIELD_CN, "PLN");
		}

		if (!values.containsKey(InvoiceLogic.LOKALIZACJA_CN)) {
			//je�li rodzaj faktury nale�y do faktur z dziwnym procesem
			if (InvoiceICLogic.FAKTURY_ZAKUPOWE_WITH_STRANGE_ACCEPTANCE_PROCESS
					.contains(values.get(InvoiceICLogic.RODZAJ_FAKTURY_CN).toString())) {
				values.put(InvoiceLogic.LOKALIZACJA_CN, "INTER");
			} else {
				values.put(InvoiceLogic.LOKALIZACJA_CN, "CEN");
			}
		}
        //w przypadku gdy jest jeszcze dodawany
//        values.remove(InvoiceLogic.LOKALIZACJA_CN);
        List<Long> vats = Lists.newArrayList();
        BigDecimal bruttoSum = BigDecimal.ZERO;
        for(String key : Sets.newHashSet(values.keySet())){
            if(key.contains(";")){
                VatRate rate = VatRate.fromCsvEntry(key);
                if(rate == null) {
                    log.info("unknown vat rate " + key);
                    continue;
                } else {
                    log.info("vat {} for key {}", rate.getValue(), key);
                }
                String value = values.get(key).toString();
                String[] valParts = value.split(";");
                if(valParts.length == 3){
                    if(valParts[2].trim().length() > 0) {
                        VatEntry entry = new VatEntry();
                        BigDecimal nettoAmount = new BigDecimal(valParts[2].replace(",", "."));
                        if(nettoAmount.compareTo(BigDecimal.ZERO) == 0) continue;         //to nie to samo co equals
                        BigDecimal vatAmount = nettoAmount.multiply(new BigDecimal(rate.getValue())).multiply(PERCENT).setScale(2, RoundingMode.HALF_EVEN);
                        bruttoSum = bruttoSum.add(nettoAmount).add(vatAmount);
                        entry.setNettoAmount(nettoAmount);
                        entry.setVatAmount(vatAmount);
                        entry.setVatSymbol(rate.getSymbol());
                        entry.setVat(rate.getValue());
                        DSApi.context().session().save(entry);
                        vats.add(entry.getId());
                    } else {
                        log.info("empty entry for key " + key);
                    }
                } else {
                    log.info("empty entry for key " + key);
                }
                values.remove(key);
            }
            if(bruttoSum.compareTo(BigDecimal.ZERO) != 0){
                values.put("KWOTA_BRUTTO", bruttoSum.setScale(2, RoundingMode.HALF_EVEN).toPlainString());
            }
        }
        values.put("VATS", vats);

		ImportHelper.setDefaultValue(values, "DATA_WYSTAWIENIA", "01-01-1900");
		ImportHelper.correctDateFormat(values, "DATA_WPLYWU");
		ImportHelper.correctDateFormat(values, "DATA_PLATNOSCI");
		ImportHelper.correctDateFormat(values, "DATA_WYSTAWIENIA");
		ImportHelper.renameField(values, "NR_FAKTURY", InvoiceLogic.NUMER_FAKTURY_FIELD_CN);
        ImportHelper.renameField(values, "METODAPLATNOSCI_KOD", PAYMENT_METHOD_FIELD_CN);
        ImportHelper.removeSpaces(values, InvoiceLogic.NUMER_FAKTURY_FIELD_CN);

		ImportHelper.removeEntryIfEquals(values, "DZIAL_IC", "XXX"); //XXX to oznaczenie nulla
		ImportHelper.removeEntryIfEquals(values, "PION", "XXX"); //XXX to oznaczenie nulla

		ImportHelper.renameField(values, "DZIAL_IC", InvoiceLogic.DZIAL_FIELD_CN);

        if(values.get(PAYMENT_METHOD_FIELD_CN) != null && values.get(PAYMENT_METHOD_FIELD_CN).toString().length() == 1){
            String code = "0" + values.get(PAYMENT_METHOD_FIELD_CN).toString();
            values.put(PAYMENT_METHOD_FIELD_CN, code);
        }

		if (values.containsKey(InvoiceICLogic.FAKTURA_SPECJALNA_CN)) {
			String fakturaSpecjalna = values.get(InvoiceICLogic.FAKTURA_SPECJALNA_CN) + "";

			if (!fakturaSpecjalna.equals("T") && !fakturaSpecjalna.equals("N")) {
				throw new ImportValidationException("Niew�a�ciwa warto�� w polu: " + InvoiceICLogic.FAKTURA_SPECJALNA_CN + " (\"" + fakturaSpecjalna + "\" powinno by� \"T\" lub \"N\")");
			}

			if (values.get(InvoiceICLogic.RODZAJ_FAKTURY_CN).equals(InvoiceICLogic.RODZAJ_ZAKUPY_IMP_CN)
                    || values.get(InvoiceICLogic.RODZAJ_FAKTURY_CN).equals(InvoiceICLogic.RODZAJ_ZAKUPY_UE_CN)
                    || values.get(InvoiceICLogic.RODZAJ_FAKTURY_CN).equals(InvoiceICLogic.RODZAJ_ZAKUPY_DT_CN)) {
				values.put(InvoiceICLogic.CREDIT_NOTY_FIELD_CN, fakturaSpecjalna.equals("T"));
                values.put(InvoiceLogic.LOKALIZACJA_CN, "INTER");//B#4000
				values.remove(InvoiceICLogic.FAKTURA_SPECJALNA_CN);
			} else if (values.get(InvoiceICLogic.RODZAJ_FAKTURY_CN).equals(InvoiceICLogic.RODZAJ_ZAKUPY_KRAJOWE_CN)) {
				values.put(InvoiceICLogic.KOREKTY_FIELD_CN, fakturaSpecjalna.equals("T"));
                values.put(InvoiceLogic.LOKALIZACJA_CN, "INTER");//B#4000
				values.remove(InvoiceICLogic.FAKTURA_SPECJALNA_CN);
			} else {
                values.put(InvoiceLogic.LOKALIZACJA_CN, "INTER");//B#4000
				values.put(InvoiceICLogic.FAKTURA_SPECJALNA_CN, fakturaSpecjalna.equals("T"));
			}
		}

		ImportHelper.changeCnToID(values, getDockindCn());

		double brutto = ImportHelper.getDoubleValue(values, "KWOTA_BRUTTO");
//		double netto = ImportHelper.getDoubleValue(values, "KWOTA_NETTO");

		values.remove("NR_PUDLA");
		values.remove("DOCUMENT_KIND");
		String nip = (String) values.get(DOSTAWCA_NIP);
		String nazwa = (String) values.get(DOSTAWCA_NAZWA);
		String nrKontrahenta = (String) values.get(DOSTAWCA_NR_KONTRAHENTA);

        String dzialId = null;
        if(values.containsKey(InvoiceICLogic.DZIAL_FIELD_CN)){
            dzialId = values.get(InvoiceICLogic.DZIAL_FIELD_CN) + "";
        }

		boolean ok = nip != null && nazwa !=null && nrKontrahenta != null;
		if (!ok) {
			throw new ImportValidationException("Brak atrybutow zwiazanych z kontrahentem");
		}

		values.remove(DOSTAWCA_NIP);
		values.remove(DOSTAWCA_NAZWA);
		values.remove(DOSTAWCA_NR_KONTRAHENTA);
		Long dicId = setUpSupplierId(nip,nazwa,nrKontrahenta);
		values.put("DOSTAWCA", (dicId!=null?dicId.toString():null));

		if (!getDockindCn().equals(DocumentLogicLoader.SAD_KIND)) {
			if (isDocumentExists(
                    (String) values.get(InvoiceLogic.NUMER_FAKTURY_FIELD_CN),
                    dicId,
                    DateUtils.parseDateAnyFormat((String)values.get("DATA_WYSTAWIENIA")),
                    brutto )) {
				throw new ImportValidationException("Istnieje juz dokument o podanych parametrach");
			}
		}
	}

    private Long setUpSupplierId(String nip, String nazwa, String nrKontrahenta) throws Exception {
        Long resp = null;
        PreparedStatement ps = null;
        nip = DicInvoice.cleanNip(nip);
        if (nrKontrahenta.startsWith("XXX")) {
            return null;
        } else {
            ResultSet rs = null;
            try {
                ps = DSApi.context().prepareStatement("select id as ile from DF_DICINVOICE where numerKontrahenta = ?");
                ps.setString(1, nrKontrahenta);
                rs = ps.executeQuery();
                if (rs.next()) {
                    resp = rs.getLong("ile");
                    return resp;
                }
            } finally {
                DbUtils.closeQuietly(rs);
                DSApi.context().closeStatement(ps);
            }

            if (!nip.startsWith("0000000000")) {
                try {
                    ps = DSApi.context().prepareStatement("select id as ile from DF_DICINVOICE where nip = ?");
                    ps.setString(1, nip);
                    rs = ps.executeQuery();
                    if (rs.next()) {
                        resp = rs.getLong("ile");
                        return resp;
                    }
                } finally {
                    DbUtils.closeQuietly(rs);
                    DSApi.context().closeStatement(ps);
                }
            }
            try {
                DicInvoice di = new DicInvoice();
                di.setName(nazwa);
                di.setNumerKontrahenta(nrKontrahenta);
                di.setNip(nip);
                DSApi.context().session().save(di);
                DSApi.context().session().flush();
                DSApi.context().session().refresh(di);
                resp = di.getId();
                return resp;
            } catch (Exception e) {
                DSApi.context().setRollbackOnly();
                throw e;
            }
        }
    }

    private Boolean isDocumentExists(
                String invoiceNumber,
                Long dicId,
                java.util.Date invdate,
                Double brutto) throws Exception {
	   PreparedStatement ps = null;
	   int ile = 0;
	   try {
           int dicIndex = 4;
		   String sql = "select count(*) as ile from "+documentKind.getTablename()+" where "
			   +documentKind.getFieldByCn(InvoiceLogic.NUMER_FAKTURY_FIELD_CN).getColumn()+" = ? and "
			   +documentKind.getFieldByCn(InvoiceLogic.DATA_WYSTAWIENIA_FIELD_CN).getColumn()+" = ? and "
			   +documentKind.getFieldByCn(InvoiceLogic.KWOTA_BRUTTO_FIELD_CN).getColumn()+" = ? ";
		   if(dicId!=null){
			   sql+= " and " + documentKind.getFieldByCn(InvoiceLogic.DOSTAWCA_FIELD_CN).getColumn()+" = ? ";
           }
		   ps = DSApi.context().prepareStatement(sql);

		   ps.setString(1, invoiceNumber);
		   ps.setDate(2, new Date(invdate.getTime()));
		   ps.setDouble(3, brutto);
		   if(dicId != null){
			   ps.setLong(dicIndex, dicId);
           }
		   ResultSet rs = ps.executeQuery();
		   rs.next();
		   ile = rs.getInt("ile");
		   rs.close();
	   } finally {
		   DSApi.context().closeStatement(ps);
	   }
	   return ile > 0;
   }

	@Override
	public void actionAfterCreate(Long documentId, DSIBean dsiB) throws Exception {
		super.actionAfterCreate(documentId, dsiB);
		Document doc = Document.find(documentId);
		doc.getDocumentKind().logic().documentPermissions(doc);
	}
}
