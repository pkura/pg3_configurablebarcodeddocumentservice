package pl.compan.docusafe.parametrization.ic;

import pl.compan.docusafe.service.imports.dsi.DSIImportHandler;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class ContractDsiHandler extends DSIImportHandler {
    @Override
    protected void prepareImport() throws Exception {

    }

    @Override
    public String getDockindCn() {
        return null;
    }
    
    @Override
	protected boolean isStartProces()
	{
		return true;
	}
}
