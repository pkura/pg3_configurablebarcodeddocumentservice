package pl.compan.docusafe.parametrization.ic;

import org.springframework.stereotype.Component;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * User: Tomasz
 * Date: 29.01.14
 * Time: 14:38
 */
@Component("obieg")
public class Obieg {
    private static final Logger LOG = LoggerFactory.getLogger(IcPorcessBean.class);

    public boolean kwotaWZakresie(Long docId, String fieldCn, String from, String to) {
        LOG.info("isMoneyInRange {} {} ",docId, fieldCn);
        try {
            Document doc = Document.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            BigDecimal v = (BigDecimal) fm.getKey(fieldCn);
            LOG.info("isMoneyInRange == {}",v.floatValue());
            BigDecimal f = BigDecimal.valueOf(Double.valueOf(from));
            BigDecimal t = BigDecimal.valueOf(Double.valueOf(to));
            LOG.info("f {} t {}",f,t);
            LOG.info("cf {} ct {}",v.compareTo(f),v.compareTo(t));
            if(v.compareTo(f) > v.compareTo(t)){
                LOG.info("< true");
                return true;
            }
            LOG.info("> false");
            return false;
        } catch (Exception e) {
            LOG.debug(e.getMessage(),e);
        }
        LOG.info("false");
        return false;
    }

    public void nadajNumer(Long docId, String fieldCn) {
        try {
            LOG.info("setInvoiceNumber {} {}",docId,fieldCn);
            Document doc = Document.find(docId);
            Map<String,Object> m = new HashMap<String, Object>();
            String[] n = UUID.randomUUID().toString().split("-");
            Calendar c = Calendar.getInstance();
            c.setTime(doc.getCtime());
            String num = "ZL/"+ c.get(Calendar.YEAR) +"/"+ n[0] +"/"+ n[1];
            m.put(fieldCn,num);
            doc.getDocumentKind().setOnly(docId,m,false);
            LOG.info("setInvoiceNumber nadany {}");
        } catch(Exception e) {
            LOG.debug(e.getMessage(),e);
        }
    }

    public String emailUzytkownika(String username) {
        LOG.info("getUserEmail {}",username);
        try {
            DSUser u = DSUser.findByUsername(username);
            return u.getEmail();

        } catch (Exception e) {
            LOG.debug(e.getMessage(),e);
        }
        return "intercars@docusafe.pl";
    }

    public void zmienStatusDokumentu(Long docId, String fieldCn, Integer fieldValue) {
        LOG.info("setDocumentStatus {} {} {}",docId, fieldCn, fieldValue);
        try {
            Document doc = Document.find(docId);
            Map<String,Object> m = new HashMap<String, Object>();
            m.put(fieldCn,fieldValue);
            doc.getDocumentKind().setOnly(docId,m,false);
        } catch(Exception e) {
            LOG.debug(e.getMessage(),e);
        }
    }

}
