package pl.compan.docusafe.parametrization.ic;

import pl.compan.docusafe.ws.ic.IcDataProcessorCallbackHandler;
import pl.compan.docusafe.ws.ic.IcDataProcessorStub.DajKhInfoResponse;
import pl.compan.docusafe.ws.ic.IcDataProcessorStub.ImportDokZkResponse;

public class AssHan extends IcDataProcessorCallbackHandler
{
    public void receiveResultimportDokZk(ImportDokZkResponse result)
    {
    	System.out.println("receiveResultimportDokZk");
    }

   /**
    * auto generated Axis2 Error handler
    * override this method for handling error response from importDokZk operation
    */
     public void receiveErrorimportDokZk(java.lang.Exception e)
     {
    	 e.printStackTrace();
    	 System.out.println("receiveErrorimportDokZk "+e.getMessage());
     }
         
    /**
     * auto generated Axis2 call back method for dajKhInfo method
     * override this method for handling normal response from dajKhInfo operation
     */
    public void receiveResultdajKhInfo(DajKhInfoResponse result) {
    
	 System.out.println("receiveResultdajKhInfo ");
    }

   /**
    * auto generated Axis2 Error handler
    * override this method for handling error response from dajKhInfo operation
    */
     public void receiveErrordajKhInfo(java.lang.Exception e) {
    	 e.printStackTrace();
    	 System.out.println("receiveErrorimportDokZk "+e.getMessage());
     }
}
