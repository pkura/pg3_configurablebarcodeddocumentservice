package pl.compan.docusafe.parametrization.ic;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;

/**
 * User: Tomasz
 * Date: 08.01.14
 * Time: 14:09
 */
public class ZaliczkaLogic extends AbstractDocumentLogic {

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
        document.setFolder(folder);

        document.setTitle("Title id: "+document.getId());
        //document.setDescription("Opis id: "+document.getId());
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {}
}
