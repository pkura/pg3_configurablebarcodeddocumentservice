package pl.compan.docusafe.parametrization.ic;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.JBPMAcceptanceManager;
import pl.compan.docusafe.core.dockinds.dictionary.AccountNumber;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.Discardable;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.parametrization.ic.InvoiceICLogic;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.permissions.PermissionCache;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import static pl.compan.docusafe.parametrization.ic.InvoiceICLogic.*;

/**
 *
 * @author Micha� Sankowski
 */
public class SadLogic extends AbstractDocumentLogic implements Discardable{
	private final static Logger log = LoggerFactory.getLogger(SadLogic.class);
    public static final String JBPM_PROCESS_ID = "JBPM_PROCESS_ID";
    public static final String NUMER_NABYCIA_FIELD_CN = "NUMER_NABYCIA";

	public static final Pattern GROUP_READ_PERMISSION_PATTERN = Pattern.compile("\\A(INVOICE|SAD).*READ\\z");

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
		document.getDocumentKind().handleLabels(document, document.getFieldsManager());
		document.getDocumentKind().setOnly(document.getId(),
					Collections.singletonMap(InvoiceICLogic.RODZAJ_FAKTURY_CN, (Object)Integer.valueOf(775)));
		InvoiceICLogic.archiveActionsVAT(document, type);
	}

	@Override
    public int getPermissionPolicyMode() {
		return PermissionManager.BUSINESS_LIGHT_POLICY;
	}

	@Override
	public boolean isReadPermissionCode(String permCode) {
		return GROUP_READ_PERMISSION_PATTERN.matcher(permCode).matches();
	}

    @Override
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException {
        log.info("validate contents: \n " + values);
        if (documentId != null) {
            values.remove(STATUS_FIELD_CN);
            values.remove(AKCEPTACJA_FINALNA_FIELD_CN);
            FieldsManager fm = kind.getFieldsManager(documentId);
            if (Boolean.TRUE.equals(fm.getKey(AKCEPTACJA_FINALNA_FIELD_CN))) {
                PermissionCache permCache = (PermissionCache) ServiceManager.getService(PermissionCache.NAME);
                if (!permCache.hasPermission(DSApi.context().getPrincipalName(), DSPermission.INVOICE_MODIFY_ACCEPTED.getName())) {
                    throw new EdmException("Modyfikacja atrybut�w nie jest mo�liwa po akceptacji finalnej");
                }
            }
        }
    }

    public void documentPermissions(Document document) throws EdmException {
		Set<PermissionBean> perms = new HashSet<PermissionBean>();

		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		List<CentrumKosztowDlaFaktury> list = (List<CentrumKosztowDlaFaktury>) fm.getValue(CENTRUM_KOSZTOWE_FIELD_CN);

		for (CentrumKosztowDlaFaktury centrum : list) {
			//createPermission(document, "Faktury kosztowe - MPK " + centrum.getCentrumCode() + " - odczyt", "INVOICE_" + centrum.getCentrumId() + "_READ", new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS});


			String readName = "Faktury kosztowe - MPK " + centrum.getCentrumCode() + " - odczyt";
			String readGuid = "INVOICE_" + centrum.getCentrumId() + "_READ";

			perms.add(new PermissionBean(ObjectPermission.READ, readGuid, ObjectPermission.GROUP, readName));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, readGuid, ObjectPermission.GROUP, readName));

			//createPermission(document, "Faktury kosztowe - MPK " + centrum.getCentrumCode() + " - modyfikacja", "INVOICE_" + centrum.getCentrumId() + "_MODIFY", new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS});

			String writeName = "Faktury kosztowe - MPK " + centrum.getCentrumCode() + " - modyfikacja";
			String writeGuid = "INVOICE_" + centrum.getCentrumId() + "_MODIFY";

			perms.add(new PermissionBean(ObjectPermission.MODIFY, writeGuid, ObjectPermission.GROUP, writeName));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, writeGuid, ObjectPermission.GROUP, writeName));

			AccountNumber an = centrum.getAccountNumber() != null ? AccountNumber.findByNumber(centrum.getAccountNumber()) : null;
			if (an != null) {
				//createPermission(document, "Faktury kosztowe - KONTO " + an.getNumber() + " - odczyt", "INVOICE_ACCOUNT_" + an.getId() + "_READ", new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS});
				readName = "Faktury kosztowe - KONTO " + an.getNumber() + " - odczyt";
				readGuid = "INVOICE_ACCOUNT_" + an.getId() + "_READ";

				perms.add(new PermissionBean(ObjectPermission.READ, readGuid, ObjectPermission.GROUP, readName));
				perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, readGuid, ObjectPermission.GROUP, readName));
			}
		}


		//DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "INVOICE_READ", ObjectPermission.GROUP);
		perms.add(new PermissionBean(ObjectPermission.READ, "SAD_READ", ObjectPermission.GROUP));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "SAD_READ", ObjectPermission.GROUP));

		//DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.READ_ATTACHMENTS}, "INVOICE_MODIFY", ObjectPermission.GROUP);

		perms.add(new PermissionBean(ObjectPermission.MODIFY, "SAD_MODIFY", ObjectPermission.GROUP));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "SAD_MODIFY", ObjectPermission.GROUP));
		//	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"INVOICE_MODIFY",ObjectPermission.GROUP));

		//DSApi.context().grant(document, new String[]{ObjectPermission.MODIFY, ObjectPermission.MODIFY_ATTACHMENTS}, "INVOICE_ADMIN", ObjectPermission.GROUP);
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "SAD_ADMIN", ObjectPermission.GROUP));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "SAD_ADMIN", ObjectPermission.GROUP));

		setUpPermission(document, perms);
	}

	private Object[] getTaskListParam(DocumentKind dockind, Long id) throws EdmException {
		Object[] result = new Object[10];
		StringBuilder tmp = new StringBuilder();
		List<CentrumKosztowDlaFaktury> centrumList = CentrumKosztowDlaFaktury.findByDocumentId(id);
		for (CentrumKosztowDlaFaktury centrum : centrumList) {
			tmp.append(centrum.getDescription() + " | ");
		}
		FieldsManager fm = dockind.getFieldsManager(id);
		DicInvoice dic = (DicInvoice) fm.getValue(InvoiceICLogic.DOSTAWCA_FIELD_CN);

		result[0] = fm.getValue(InvoiceICLogic.STATUS_FIELD_CN);
		result[1] = fm.getKey(SadLogic.NUMER_NABYCIA_FIELD_CN);
		result[2] = fm.getKey(InvoiceICLogic.KWOTA_NETTO_FIELD_CN);
		result[3] = "Dokument SAD"
				+ (fm.getBoolean(InvoiceICLogic.CREDIT_NOTY_FIELD_CN) ? " (CREDIT NOTY)" :"") ;
		result[4] = StringUtils.left(tmp.toString(), 20);
		result[5] = dic != null ? StringUtils.left(dic.getNumerKontrahenta().toString(), 20) : "brak";
		result[6] = fm.getValue(InvoiceICLogic.DATA_PLATNOSCI_FIELD_CN) != null ? DateUtils.formatCommonDate((Date) fm.getValue(InvoiceICLogic.DATA_PLATNOSCI_FIELD_CN)) : "brak";
		result[7] = fm.getValue(InvoiceICLogic.NAZWA_PROJEKTU_FIELD_CN);
		result[8] = fm.getValue(InvoiceICLogic.DATA_WYSTAWIENIA_FIELD_CN);

		return result;
	}

    @Override
    public TaskListParams getTaskListParams(DocumentKind kind, long documentId) throws EdmException {
        return TaskListParams.fromMagicArray(getTaskListParam(kind, documentId));
    }

    @Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
		Map<String, Object> values = new HashMap<String, Object>();
		values.put(InvoiceICLogic.RODZAJ_FAKTURY_CN, 775);
		fm.reloadValues(values);
	}

	public void discardToUser(OfficeDocument doc, String activity, ActionEvent event, DSUser user) throws EdmException {
		InvoiceICLogic.discardToUser(this, doc, activity, event, user);
	}
	
	@Override
	public JBPMAcceptanceManager getAcceptanceManager() {
		return InvoiceICLogic.IC_ACCEPTANCES_MANAGER;
	}

	@Override
	public void onStartProcess(OfficeDocument document) {
		log.debug("onStartProcess");
		try {
			if (document.getFieldsManager().getValue(JBPM_PROCESS_ID) == null) {
				document.getDocumentKind().setOnly(document.getId(),
						Collections.singletonMap(JBPM_PROCESS_ID, (Object) getAcceptanceManager().start(document)));
			}
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
			return;
		}
	}

	@Override
	public void checkCanFinishProcess(OfficeDocument document) throws AccessDeniedException, EdmException {
		InvoiceICLogic.checkCanFinishICProcess(document, Documents.document(document.getId()).getFieldsManager());
	}

	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException {
		String channel = document.getDocumentKind().getChannel(document.getId());
		if (channel != null) {
			List<ProcessCoordinator> cords = ProcessCoordinator.find(DocumentLogicLoader.SAD_KIND, channel);
			if (cords.size()>0) {
				return cords.get(0);
			} else {
				return null;
			}
		}
		return null;
	}

	public void discardToCn(OfficeDocument document, String activity, ActionEvent event, String cn, Object objectId) throws EdmException {
		InvoiceICLogic.discardToCn(this, document, activity, cn, objectId);
	}
}
