package pl.compan.docusafe.parametrization.ic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *
 * @author Micha� Sankowski
 */
public class SADImportHandler  extends ICDSIHandler{

	private final static Logger log = LoggerFactory.getLogger(SADImportHandler.class);

	@Override
	public String getDockindCn() {
		return DocumentLogicLoader.SAD_KIND;
	}

}
