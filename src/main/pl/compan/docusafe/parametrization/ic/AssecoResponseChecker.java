package pl.compan.docusafe.parametrization.ic;

import com.google.common.collect.Lists;
import org.apache.commons.dbutils.DbUtils;
import org.bouncycastle.crypto.DSA;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.workflow.DSjBPM;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.*;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Klasa oczekuj�ca na odpowied� od asseco
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class AssecoResponseChecker extends ServiceDriver {
    private final static Logger LOG = LoggerFactory.getLogger(AssecoResponseChecker.class);

    private Timer timer;

    @Override
    protected void start() throws ServiceException {
        try {
            if(DocumentKind.isAvailable(DocumentLogicLoader.INVOICE_IC_KIND)){
                LOG.info("uruchomiono AssecoResponseChecker");
                timer = new Timer(true);
                //sprawdza po��czenie przy starcie i nie uruchamia timera je�li si� nie powiedzie
                AssecoImportManager aim = new AssecoImportManager();
                timer.schedule(new Checker(), 1000, 120 * DateUtils.SECOND);
            }
        } catch(Throwable ex) {
            LOG.error(ex.getMessage(), ex);
            return;
        }
    }

    private void checkSentDocuments() throws Exception{
        LOG.info("checking sent documents");
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Long> documentIds = Lists.newArrayList();
        AssecoImportManager aim = new AssecoImportManager();
        try {
            ps = DSApi.context().prepareStatement("SELECT document_id FROM dsg_invoice_ic WHERE status = 1000");
            rs = ps.executeQuery();
            while(rs.next()){
                documentIds.add(rs.getLong(1));
            }
            LOG.info("checking documents :{}", documentIds);
        } finally {
            DbUtils.close(rs);
            DSApi.context().closeStatement(ps);
        }
        for(long documentId: documentIds){
            checkDocument(aim,documentId);
        }
    }

    private void checkDocument(AssecoImportManager aim, long documentId) throws Exception{
        try {
            DSApi.context().begin();
            aim.getStatus(documentId);
            DSApi.context().commit();
        } catch (Exception ex) {
            DSApi.context().rollback();
            if(LOG.isDebugEnabled()){
                LOG.error("exception {} while checking document {}", ex.getMessage(), documentId);
                LOG.debug("stack trace", ex);
            } else {
                LOG.error("exception '{}' while checking document {} (set level to debug to see stack trace)", ex.getMessage(), documentId);
            }
        }
        DSApi.context().session().clear();
    }

    private class Checker extends TimerTask {
        @Override
        public void run() {
            try {
                DSApi.openAdmin();
                DSjBPM.open();
                checkSentDocuments();
            } catch (Throwable t) {
                LOG.error(t.getMessage(), t);
            } finally {
                DSjBPM.closeContextQuietly();
                DSApi._close();
            }
        }
    }

    @Override
    protected void stop() throws ServiceException {
    }

    @Override
    protected boolean canStop() {
        return true;
    }
}
