package pl.compan.docusafe.parametrization.ic;

import com.google.common.collect.Maps;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.*;

/**
 * User: Tomasz
 * Date: 28.01.14
 * Time: 14:08
 */
public class FakturaLogic extends AbstractDocumentLogic {

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
        document.setFolder(folder);

        document.setTitle("Title id: "+document.getId());

        try {
            FieldsManager fm = document.getFieldsManager();
            List<Long> ids = (List<Long>) fm.getKey("DSG_INVOICE_P_MPK");
            Iterator<Long> i = ids.iterator();
            while(i.hasNext()) {
                Long id = i.next();
                IcMpkDic el = IcMpkDic.find(id);
                if(el.getStatus() == null) {
                    el.setStatus(1);
                    el.update();
                }

            }
        } catch(Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void documentPermissions(Document document) throws EdmException {}

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException {
        Map<String, Object> toReload = Maps.newHashMap();
        toReload.put("STATUS", 1);
        fm.reloadValues(toReload);
    }
}
