package pl.compan.docusafe.parametrization.ic.reports;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;

public class WebServiceCentraUpdater extends Report
{

	UniversalTableDumper dumper;
	private static final String sql = "INSERT INTO dsg_ic_centra_report " +
			"(centrum_id, centrum_name, kwota_zaakceptowane, kwota_niezaakceptowane, waluta) " +
			"values (?,?,?,?,?)";
	
	public void doReport() throws Exception 
	{
		this.getDestination().delete();
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		StringBuilder sbQuery = new StringBuilder();		
		List<Integer> projectsList = new ArrayList<Integer> ();
		for(CentrumKosztow ck : CentrumKosztow.listAll()){
			projectsList.add(ck.getId());
		}
				
		sbQuery.append(" SELECT cent.id, cent.name ,ic.waluta,");
		sbQuery.append(" SUM(CASE WHEN ISNULL(ic.akceptacja_finalna, 0) != 1 THEN ck.amount ELSE 0 END) AS sum1,");
		sbQuery.append(" SUM(CASE WHEN ISNULL(ic.akceptacja_finalna, 0) = 1 THEN ck.amount ELSE 0 END) AS sum2");
		sbQuery.append(" FROM dsg_centrum_kosztow_faktury ck");
		sbQuery.append(" JOIN dsg_invoice_ic ic ON ic.document_id = ck.documentId");
		sbQuery.append(" JOIN dsg_centrum_kosztow cent ON cent.id = ck.centrumId");
		sbQuery.append(" JOIN ds_document doc ON doc.id = ic.document_id " );
		
		
		sbQuery.append(" WHERE ck.centrumId IN (");

		for (int i = 0; i < projectsList.size(); i++)
			if (i == projectsList.size() -1 ) sbQuery.append("?)");
			else sbQuery.append("?,");

		sbQuery.append(" AND doc.dockind_id = ?");
		sbQuery.append(" AND ic.status !=6 ");
		sbQuery.append(" GROUP BY cent.name, cent.id, ic.waluta");
		sbQuery.append(" ORDER BY cent.name, ic.waluta");	
		
			
		try 
		{
			ps = DSApi.context().prepareStatement(sbQuery.toString());
			for (int i = 0; i < projectsList.size(); i++)
			{
				ps.setInt(i+1, projectsList.get(i));
			}
			ps.setInt(projectsList.size()+1, DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND).getId().intValue());
			ResultSet rs = ps.executeQuery();

			while (rs.next()) 
			{
				ps1 = DSApi.context().prepareStatement(sql);
				ps1.setInt(1, rs.getInt("id"));
				ps1.setString(2, rs.getString("name"));
				ps1.setDouble(3, rs.getDouble("sum2"));
				ps1.setDouble(4, rs.getDouble("sum1"));
				ps1.setString(5, ReportUtilsIC.convertIntegerToCurrencyString(rs.getInt("waluta")));
				ps1.executeUpdate();
				DSApi.context().closeStatement(ps1);
			}
			rs.close();
		}
		catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		}
		finally	{
			DSApi.context().closeStatement(ps);
			DSApi.context().closeStatement(ps1);
			if (dumper != null)
				dumper.closeFileQuietly();
		}		
	}
}
