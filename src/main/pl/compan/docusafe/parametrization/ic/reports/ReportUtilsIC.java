package pl.compan.docusafe.parametrization.ic.reports;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceLogic;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.StringManager;

/**
 * Klasa pomocnicza dla raportow IC
 * @author r.powroznik
 */
public class ReportUtilsIC 
{
	private static StringManager sm = StringManager.getManager(ReportUtilsIC.class.getPackage().getName());
	protected static final Logger log = LoggerFactory.getLogger(ReportUtilsIC.class);
	
	
	public final static String ALL_REPORTS_GUID = "WSZYSTKIE_CENTRA_RAPORTY";
	
	public final static int OKRES_PODSUMOWANIA_MIESIECZNY = 0;
	public final static int OKRES_PODSUMOWANIA_KWARTALNY = 1;
	public final static int OKRES_PODSUMOWANIA_ROCZNY = 2;
	public final static int OKRES_PODSUMOWANIA_CALOSCIOWY = 3;
	
	public final static int RODZAJ_FAKTURY_ZAREJESTROWANA = 0;
	public final static int RODZAJ_FAKTURY_ZAAKCEPTOWANA = 1;
	public final static int RODZAJ_FAKTURY_WSZYSTKIE = 2;
	
	/**
	 * Metoda pobiera liste projektow, ktore definiowane sa w slowniku dokumentow.
	 * @return lista projektow
	 */
	public static Map <String, String> getProjectsMap(DSUser user) throws EdmException
	{
        //je�eli osoba nie ma uprawnie� do wszystkich centr�w to trzeba je posortowa� po stronie serwera produkcyjnego

		List<AcceptanceCondition> accs = new ArrayList<AcceptanceCondition>();
		CentrumKosztow ck;
        if(user.inDivisionByGuid(ALL_REPORTS_GUID)){
            return fullMap();
        } else {
            List<CentrumKosztow> list = new ArrayList<CentrumKosztow>();
            for (AcceptanceCondition cond : AcceptanceCondition.fullUserAcceptances(user)) {
                if (cond.getCn().equals("dyrektora_fin")
                        || cond.getCn().startsWith("ksiegowa")
                        || cond.getCn().startsWith("kontrola_jakosci")
                        || cond.getCn().startsWith("admin")) {
                    return fullMap();
                } else if (!cond.getCn().equals("zwykla") && !StringUtils.isBlank(cond.getFieldValue())) { //jest w tej chwili wiele innych typ�w akceptacji
                    ck = CentrumKosztow.find(Integer.parseInt(cond.getFieldValue()));
                    list.add(ck);
                }
            }

            Collections.sort(list, new Comparator<CentrumKosztow>(){
                public int compare(CentrumKosztow o1, CentrumKosztow o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });

            Map<String, String> ret = new LinkedHashMap<String, String>();
            for(CentrumKosztow cent: list){
                ret.put(cent.getId().toString(), cent.getTitle());
            }
            return ret;
        }
	}

    private static Map<String, String> fullMap() throws EdmException {
        Map <String, String> map = new LinkedHashMap<String, String>();
        for(CentrumKosztow cent:CentrumKosztow.listAllBy("name")){
		    map.put(cent.getId().toString(), cent.getTitle());
		}
        return map;
    }
	
	/**
	 * Metoda pobiera liste oznaczen okresow rozliczeniowych
	 * @return lista okresow rozliczeniowych
	 */
	public static Map <String, String> getPeriodsMap() {
		Map<String, String> map = new HashMap<String, String>();
		map.put(Integer.toString(OKRES_PODSUMOWANIA_MIESIECZNY), sm.getString("miesieczny"));
		map.put(Integer.toString(OKRES_PODSUMOWANIA_KWARTALNY), sm.getString("kwartalny"));
		map.put(Integer.toString(OKRES_PODSUMOWANIA_ROCZNY), sm.getString("roczny"));
		map.put(Integer.toString(OKRES_PODSUMOWANIA_CALOSCIOWY), sm.getString("calosciowy"));
		return map;
	}
	
	/**
	 * Metoda pobiera liste typow faktur.
	 * @return lista typow faktur
	 */
	public static Map <String, String> getInvoicesTypeMap() {
		Map<String, String> map = new HashMap<String, String>();
		map.put(Integer.toString(RODZAJ_FAKTURY_ZAREJESTROWANA), sm.getString("zarejestrowana"));
		map.put(Integer.toString(RODZAJ_FAKTURY_ZAAKCEPTOWANA), sm.getString("zaakceptowana"));
		map.put(Integer.toString(RODZAJ_FAKTURY_WSZYSTKIE), sm.getString("wszystkie"));
		return map;
	}
	
	/**
	 * Zamienia kod waluty na odpowiadajacy jej napis. Jezeli wystapi blad to zwraca kod w postaci stringa.
	 * @param currencyCode kod waluty
	 * @return typ waluty
	 */
	public static String convertIntegerToCurrencyString(int currencyCode) 
	{
		String result = null;
		try 
		{
			result = DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND).getFieldByCn("WALUTA").getEnumItem(currencyCode).getTitle();
		}
		catch(Exception e) 
		{
			log.error("Nie mozna skonwertowac", e);
		}
		return result == null ? "Nieznana" : result;		
	} 
}
