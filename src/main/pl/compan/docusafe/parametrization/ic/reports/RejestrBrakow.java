package pl.compan.docusafe.parametrization.ic.reports;

import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.dockinds.logic.InvoiceLogic;
import pl.compan.docusafe.core.dockinds.other.InvoiceInfo;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.DateUtils;

public class RejestrBrakow extends Report
{	
	UniversalTableDumper dumper;

	public void doReport() throws Exception
	{
		initDumpers("rejestr_brakow");
		initHeader();
		File file = new File(this.getDestination(), "kryteria.dsr");
		parseFile(file);
		dumper.closeFileQuietly();
	}

	protected void initDumpers(String fileName) throws Exception
	{
		dumper = new UniversalTableDumper();
		/*
		CsvDumper csvDumper = new CsvDumper();
		File csvFile = new File(this.getDestination(), "fax_kpi_time.csv");			
		csvDumper.openFile(csvFile);
		dumper.addDumper(csvDumper);
		*/
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), fileName+".xls");	
		poiDumper.openFile(xlsFile);
		dumper.addDumper(poiDumper);
	}
	
	protected void initHeader() throws Exception
	{
		dumper.newLine();
		dumper.addText("Numer faktury");
		dumper.addText("Data wystawienia faktury");
		dumper.addText("Kod kontrahenta");
		dumper.addText("Nazwa kontrahenta");
		dumper.addText("Kwota");
		dumper.addText("Osoba ksi�guj�ca");
		dumper.addText("ID dokumentu");
		dumper.addText("Status");
		dumper.addText("Lokalizacja");
		dumper.dumpLine();
	}
	
	public void parseFile(File file) throws EdmException
    {
		BufferedReader reader = null;
        PreparedStatement ps = null;
        PreparedStatement ps1 = null;
        try
        {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "cp1250"));
            String line;
            int lineCount = 0;
            
            ps = DSApi.context().prepareStatement("select targetuser, targetguid from dso_document_asgn_history where document_id = ? order by ctime desc");
            ps1 = DSApi.context().prepareStatement("select count(id) from dsw_tasklist where dso_document_id = ?");
            while ((line = reader.readLine()) != null){
                lineCount++;
                processLine(ps, ps1, line, lineCount);
            }
        }
        catch (Exception e)
        {
            throw new EdmException("B��d podczas czytania pliku z rejestrem");
        }
        finally
        {
        	DSApi.context().closeStatement(ps);
        	DSApi.context().closeStatement(ps1);
        	if (reader!=null)
        	{
        		try { reader.close(); } catch (Exception e1) { }
        	}
        }
    }

    private void processLine(PreparedStatement documentStatement, PreparedStatement ps1, String line, int lineCount) throws IOException {
        dumper.newLine();
        try {
            String status = "error";
            String dzial = "";
            String iddok = "";

            line = line.replace("\"", "");

            line = line.trim();
            // pierwsza linia to nag��wek, kt�ry omijam
            if (lineCount == 1 || line.startsWith("#"))
                return;

            InvoiceInfo info = new InvoiceInfo();
            info.setLineNumber(lineCount);

            String[] parts = line.split(";");

            if (parts.length >= 5) {
                if (parts[0].length() == 0) return;
                info.setInvoiceNumber(parts[0].trim());
                try {
                    info.setInvDate(DateUtils.parseDateAnyFormat(parts[1].replace(".", "-")));
                }
                catch (Exception e) {
                    throw new EdmException("Blad w linii " + lineCount + ": " + parts[1], e);
                }
                String amount = parts[4].trim();
                amount = amount.replace("\"", "");
                amount = amount.replace(" ", "");
                amount = amount.replace(",", ".");
                try {
                    info.setAmountBrutto(Double.parseDouble(amount));
                }
                catch (NumberFormatException e) {
                    log.debug("", e);
                    info.setStatus(InvoiceInfo.STATUS_PARSE_ERROR);
                    info.setErrorInfo("niepoprawna kwota");
                    info.setAmountBrutto(null);
                }
                info.setNumerKontrahenta(parts[2]);

                dumper.addText(parts[0]);
                dumper.addText(parts[1]);
                dumper.addText(parts[2]);
                dumper.addText(parts[3]);
                dumper.addText(parts[4]);
                if (parts.length > 5)
                    dumper.addText(parts[5]);
                else
                    dumper.addText("brak");
            } else {
                dumper.addText(status);
                dumper.addText(status);
                dumper.addText(status);
                dumper.addText(status);
                dumper.addText(status);
                dumper.addText(status);
                info.setStatus(-1);
            }

            if (info.getStatus() == null) {

                // poprawnie odczytano z pliku - szukamy odpowiedni dokument
                DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND);
                DockindQuery query = new DockindQuery(0, 0);
                query.setDocumentKind(kind);

                query.field(kind.getFieldByCn(InvoiceLogic.NUMER_FAKTURY_FIELD_CN), info.getInvoiceNumber());
                query.otherClassField(kind.getFieldByCn(InvoiceLogic.DOSTAWCA_FIELD_CN), "numerKontrahenta", info.getNumerKontrahenta());

                SearchResults<Document> searchResults = DocumentKindsManager.search(query);
                Document[] documents = searchResults.results();
                if (searchResults.totalCount() == 0) {
                    status = "brak";
                } else if ((Integer) documents[0]
                        .getFieldsManager()
                        .getKey(InvoiceLogic.STATUS_FIELD_CN) == 666) {
                    status = "brak";
                } else {
                    DSUser user;
                    DSDivision div;
                    Document doc = documents[0];
                    iddok = doc.getId().toString();
                    ps1.setLong(1, doc.getId());
                    ResultSet rs = ps1.executeQuery();
                    rs.next();
                    if (rs.getInt(1) < 1) {
                        status = "archiwum";
                    }
                    rs.close();
                    if (!status.equals("archiwum")) {
                        status = "obieg";
                        documentStatement.setLong(1, documents[0].getId());
                        rs = documentStatement.executeQuery();
                        if (rs.next()) {

                            String username = rs.getString("targetuser");
                            String guid = rs.getString("targetguid");
                            if (guid != null && !"x".equals(guid)) {
                                try {
                                    div = DSDivision.find(guid);
                                    dzial = div.getName();
                                }
                                catch (Exception e) {
                                }
                            } else if (username != null && !"x".equals(username)) {
                                try {
                                    user = DSUser.findByUsername(username);
                                    dzial = user.asFirstnameLastnameName();
                                }
                                catch (Exception e) {
                                }
                            }
                        }
                        rs.close();
                    }
                }
            }
            dumper.addText(iddok);
            dumper.addText(status);
            dumper.addText(dzial);
        }
        catch (Exception e) {
            dumper.addText("Line Error");
        }
        dumper.dumpLine();
    }
}
