package pl.compan.docusafe.parametrization.ic.reports;

import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;

public class DocumentStatusReport extends Report
{

	String sql = "select distinct(ic.document_id) from dsw_tasklist tl " +
			"join dsg_invoice_ic ic on ic.document_id = tl.dso_document_id ";

	String sqlSAD = "select distinct(ic.document_id) from dsw_tasklist tl " +
		"join dsg_sad ic on ic.document_id = tl.dso_document_id ";
		


	String addSqlUser = "";
	String addSqlRodzaj = "";
	String addSqlAkceptacja = "";

	UniversalTableDumper dumper;

	public void doReport() throws Exception
	{
		Integer[] akceptacje = new Integer[0]; 
		String[] users = new String[0];
		Integer[] rodzaje = new Integer[0];
		
		for(ReportParameter p: params)
		{
			String[] vals = new String[0];
			if(p.getValue() instanceof String)
			{
				vals = new String[1];
				vals[0] = p.getValueAsString();
			}
			else if(p.getValue() instanceof String[])
			{
				vals = (String[])p.getValue();
			}

			if("user".equals(p.getFieldCn()))
			{
				if(vals.length>0)
				{
					addSqlUser = "tl.dsw_resource_res_key in (";
					
					users = new String[vals.length];
					for(int i=0;i<vals.length;i++)
					{
						users[i] = vals[i];
						if(i==0)
							addSqlUser+="?";
						else
							addSqlUser+=",?";
					}
					addSqlUser+=")";
					
				}
			}
			if("akceptacja".equals(p.getFieldCn()))
			{
				if(vals.length>0)
				{
					addSqlAkceptacja = "ic.status in (";
					
					akceptacje = new Integer[vals.length];
					for(int i=0;i<vals.length;i++)
					{
						akceptacje[i] = Integer.parseInt(vals[i]);
						if(i==0)
							addSqlAkceptacja+="?";
						else
							addSqlAkceptacja+=",?";
					}
					addSqlAkceptacja+=")";
					
				}
			}
			if("rodzaj".equals(p.getFieldCn()))
			{
				if(vals.length>0)
				{
					addSqlRodzaj = "ic.rodzaj_faktury in (";
					
					rodzaje = new Integer[vals.length];
					for(int i=0;i<vals.length;i++)
					{
						rodzaje[i] = Integer.parseInt(vals[i]);
						if(i==0)
							addSqlRodzaj+="?";
						else
							addSqlRodzaj+=",?";
					}
					addSqlRodzaj+=")";
					
				}
			}
		}


		
		initDumpers("dokumenty");
		dumpHeader();
		
		if(rodzaje.length>0 || users.length>0 || akceptacje.length>0)
		{
			sql+= " where ";
			sqlSAD += " where ";
		}

		boolean added = false;
		
		if(rodzaje.length>0)
		{
			if(added)
			{
				sql+= " and ";
				sqlSAD+= " and ";
			}
			sql += addSqlRodzaj;
			sqlSAD += addSqlRodzaj;
			added = true;
		}
		if(akceptacje.length>0)
		{
			if(added)
			{
				sql+= " and ";
				sqlSAD+= " and ";
			}
			sql += addSqlAkceptacja;
			sqlSAD += addSqlAkceptacja;
			added = true;
		}
		if(users.length>0)
		{
			if(added)
			{
				sql+= " and ";
				sqlSAD+= " and ";
			}
			sql += addSqlUser;
			sqlSAD += addSqlUser;
			added = true;
		}
		PreparedStatement ps = null;
		PreparedStatement psDekretacja = null;
		PreparedStatement psSAD = null;
		ResultSet rs = null;
		ResultSet rsSAD = null;
		ResultSet rsDekretacja = null;
		
		try
		{
			ps = DSApi.context().prepareStatement(sql);
			psSAD = DSApi.context().prepareStatement(sqlSAD);
			psDekretacja = DSApi.context().prepareStatement("select targetuser, targetguid from dso_document_asgn_history where document_id = ? order by ctime desc");
			
			int counter = 0;
			if(rodzaje.length>0)
			{
				for(int j=0;j<rodzaje.length;j++)
				{
					counter++;
					ps.setInt(counter, rodzaje[j]);
					psSAD.setInt(counter, rodzaje[j]);
				}
			}
			if(akceptacje.length>0)
			{
				for(int j=0;j<akceptacje.length;j++)
				{
					counter++;
					ps.setInt(counter, akceptacje[j]);
					psSAD.setInt(counter, akceptacje[j]);
				}
			}
			if(users.length>0)
			{
				for(int j=0;j<users.length;j++)
				{
					counter++;
					ps.setString(counter, users[j]);
					psSAD.setString(counter, users[j]);
				}
			}
			
			Long id;
			counter = 0;
			
			rs = ps.executeQuery();
			while(rs.next())
			{
				dumper.newLine();
				try
				{
					
					id = rs.getLong(1);
					Document doc = Document.find(id);
					FieldsManager fm = doc.getFieldsManager();
					
					counter++;
					
					dumper.addLong(id);
					dumper.addText("invoice");
					dumper.addText((String)fm.getValue("RODZAJ_FAKTURY"));
					dumper.addText((String)fm.getValue("NUMER_FAKTURY"));
					dumper.addDate((Date)fm.getValue("DATA_WYSTAWIENIA"));
					dumper.addText((String)fm.getValue("KWOTA_NETTO"));
					dumper.addText(((DicInvoice)fm.getValue("DOSTAWCA")).getNumerKontrahenta());
					dumper.addText(((DicInvoice)fm.getValue("DOSTAWCA")).getName());
					dumper.addText(((DicInvoice)fm.getValue("DOSTAWCA")).getNip());
					
					
					String dzial = "brak";
					psDekretacja.setLong(1, id);
                	rsDekretacja = psDekretacja.executeQuery();
                	if(rsDekretacja.next())
                	{
                		String username = rsDekretacja.getString("targetuser");
                    	String guid  = rsDekretacja.getString("targetguid");	
                		if(username!=null && !"x".equals(username))
                		{
                			try
                			{
                				dzial = DSUser.findByUsername(username).asFirstnameLastnameName();
                			}
                			catch (Exception e) {}
                		}
                		else if(guid!=null && !"x".equals(guid))
                		{
                			try
                			{
                				dzial = DSDivision.find(guid).getName();
                			}
                			catch (Exception e) {}
                			
                		}
                	}
                	rsDekretacja.close();
                	dumper.addText(dzial);
                	dumper.addText((String)fm.getValue("STATUS"));
					
				}
				catch (Exception e) 
				{
					dumper.addText("error");
					
					log.trace(e.getMessage(), e);
					//e.printStackTrace();
				}
				dumper.dumpLine();
			}
			rs.close();
			rsSAD = psSAD.executeQuery();
			while(rsSAD.next())
			{
				dumper.newLine();
				try
				{
					
					id = rs.getLong(1);
					Document doc = Document.find(id);
					FieldsManager fm = doc.getFieldsManager();
					
					counter++;
					
					dumper.addLong(id);
					dumper.addText("sad");
					dumper.addText((String)fm.getValue("RODZAJ_FAKTURY"));
					dumper.addText((String)fm.getValue("NUMER_FAKTURY"));
					dumper.addDate((Date)fm.getValue("DATA_WYSTAWIENIA"));
					dumper.addText((String)fm.getValue("KWOTA_NETTO"));
					dumper.addText(((DicInvoice)fm.getValue("DOSTAWCA")).getNumerKontrahenta());
					dumper.addText(((DicInvoice)fm.getValue("DOSTAWCA")).getName());
					dumper.addText(((DicInvoice)fm.getValue("DOSTAWCA")).getNip());
					
					
					String dzial = "brak";
					psDekretacja.setLong(1, id);
                	rsDekretacja = psDekretacja.executeQuery();
                	if(rsDekretacja.next())
                	{
                		String username = rs.getString("targetuser");
                    	String guid  = rs.getString("targetguid");
                    	if(username!=null && !"x".equals(username))
                		{
                			try
                			{
                				dzial = DSUser.findByUsername(username).asFirstnameLastnameName();
                			}
                			catch (Exception e) {}
                		}
                		else if(guid!=null && !"x".equals(guid))
                		{
                			try
                			{
                				dzial = DSDivision.find(guid).getName();
                			}
                			catch (Exception e) {}
                			
                		}
                	}
                	rsDekretacja.close();
                	dumper.addText(dzial);
                	dumper.addText((String)fm.getValue("STATUS"));
					
				}
				catch (Exception e) 
				{
					dumper.addText("error");
					
					log.trace(e.getMessage(), e);
				}
				dumper.dumpLine();
			}
			rsSAD.close();
			
			dumper.newLine();
			dumper.addText("SUMA");
			dumper.addInteger(counter);
			dumper.dumpLine();
			
		}
		catch (Exception e) 
		{
			throw new ReportException(e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
			DSApi.context().closeStatement(psSAD);
			DSApi.context().closeStatement(psDekretacja);
		}
		dumper.closeFileQuietly();
	}
	
	
	private void dumpHeader() throws Exception
	{
		dumper.newLine();
		dumper.addText("ID dokumentu");
		dumper.addText("Rodzaj dokumentu");
		dumper.addText("Rodzaj faktury");
		dumper.addText("Numer faktury");
		dumper.addText("Data wystawienia");
		dumper.addText("Kwota netto");
		dumper.addText("Numer kontrahenta");
		dumper.addText("Nazwa kontrahenta");
		dumper.addText("NIP kontrahenta");
		dumper.addText("Lokalizacja");
		dumper.addText("Poziom akceptacji");
		dumper.dumpLine();
	}
	
	protected void initDumpers(String fileName) throws Exception
	{
		dumper = new UniversalTableDumper();
		/*
		CsvDumper csvDumper = new CsvDumper();
		File csvFile = new File(this.getDestination(), "fax_kpi_time.csv");			
		csvDumper.openFile(csvFile);
		dumper.addDumper(csvDumper);
		*/
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), fileName+".xls");	
		poiDumper.openFile(xlsFile);
		dumper.addDumper(poiDumper);
	}

}



