package pl.compan.docusafe.parametrization.ic.reports;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dictionary.AccountNumberComment;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.parametrization.ic.CommentsForAccountsProvider;
import pl.compan.docusafe.parametrization.ic.InvoiceICLogic;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.SimpleSQLHelper;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.DateUtils;

/**
 * Klasa raportu podsumowania projektu [IC]
 * @author r.powroznik
 */
public class ProjectsSummaryReport extends Report
{	
	static final Logger log = LoggerFactory.getLogger(ProjectsSummaryReport.class);
	UniversalTableDumper dumper;
	
	@Override
	public void initParametersAvailableValues() throws ReportException
	{
		try
		{
			super.initParametersAvailableValues();
			for(ReportParameter rp:params)
			{
				if(rp.getFieldCn().equals("project"))
					rp.setAvailableValues(ReportUtilsIC.getProjectsMap(DSApi.context().getDSUser()));
				if(rp.getFieldCn().equals("invoiceType"))
					rp.setAvailableValues(ReportUtilsIC.getInvoicesTypeMap());			
			}
		}catch (Exception e) {
			throw new ReportException(e);
		}
	}
	
	public void doReport() throws Exception 
	{
		PreparedStatement ps = null;
		int invoiceType = -1;
		String dateFrom = null;
		String dateTo = null;
		List<String> projectsList = new ArrayList<String> ();
		
		for(ReportParameter p: params) {
			if (p.getValue() == null)
				continue;
			if (p.getFieldCn().equals("project")) {
				if(p.getValue() instanceof String)
					projectsList.add(p.getValueAsString());
				else if (p.getValue() instanceof String[])
					for (String str : (String[]) p.getValue())
						projectsList.add(str);
			}
			if (p.getFieldCn().equals("invoiceType")) {
				invoiceType = Integer.parseInt(p.getValueAsString());
			}
			if (p.getFieldCn().equals("registrationDateFrom")) {
				dateFrom = p.getValueAsString();
			}
			if (p.getFieldCn().equals("registrationDateTo")) {
				dateTo = p.getValueAsString();
			}
		}		
		
		StringBuilder sbQuery = new StringBuilder();
		/*sbQuery.append("SELECT  e5.title, ic.opis_faktury, ic.data_wystawienia, dos.name + ' (' + dos.nip + ')' AS dostawca, ic.nr_faktury, ic.uwaga_opis, ic.kwota_netto");
		sbQuery.append(" FROM dsg_invoice_ic ic ");
		sbQuery.append(" JOIN df_dicinvoice dos on ic.dostawca = dos.id");
		sbQuery.append(" JOIN dsg_enum5 e5 ON e5.id = ic.stanowisko");
		sbQuery.append(" WHERE ic.stanowisko IN (");
		
		for (int i = 0; i < projectsList.size(); i++) {
			if (i == projectsList.size() -1 ) sbQuery.append("?)");
			else sbQuery.append("?,");
		}
		
		if (invoiceType == ReportUtilsIC.RODZAJ_FAKTURY_ZAAKCEPTOWANA)
			sbQuery.append(" AND akceptacja_zwykla = 1");
//		else if (invoiceType == ReportUtils.RODZAJ_FAKTURY_ZAREJESTROWANA)
//			 sb.append(" AND ");
		// TODO: po co wybor wszystkie skoro zarejestrowane maja byc tak traktowane? 
		if (dateFrom != null)
			sbQuery.append(" AND data_wystawienia >= ?");
		if (dateTo != null)
			sbQuery.append(" AND data_wystawienia <= ?");
		sbQuery.append(";");*/
		
		sbQuery.append(" SELECT cent.name, ic.document_id, ic.data_wystawienia, ic.data_ksiegowania, dos.name + ' (' + dos.nip + ')' AS dostawca, ck.descriptionAmount, ")
		    .append(" ic.nr_faktury, ck.descriptionAmount, ck.accountNumber, ck.amount, CASE WHEN ic.akceptacja_finalna = 1 THEN 'TAK' ELSE 'NIE' END as accepted, ")
            .append(" ic.waluta AS waluta")
		    .append(" FROM dsg_centrum_kosztow_faktury ck")
		    .append(" JOIN dsg_invoice_ic ic ON ic.document_id = ck.documentId")
		    .append(" JOIN dsg_centrum_kosztow cent ON cent.id = ck.centrumId")
		    .append(" JOIN ds_document doc ON doc.id = ic.document_id " )
		    .append(" JOIN df_dicinvoice dos on ic.dostawca = dos.id");
		
		sbQuery.append(" WHERE ck.centrumId IN (");

		for (int i = 0; i < projectsList.size(); i++)
			if (i == projectsList.size() -1 ) sbQuery.append("?)");
			else sbQuery.append("?,");

		
		sbQuery.append(" AND doc.dockind_id = ?");
		sbQuery.append(" AND ic.status != 6 AND ic.status != 666");
		if (dateFrom != null)
			sbQuery.append(" AND ic.data_ksiegowania >= ?");
		if (dateTo != null)
			sbQuery.append(" AND ic.data_ksiegowania <= ?");
		if (invoiceType == ReportUtilsIC.RODZAJ_FAKTURY_ZAAKCEPTOWANA)
			sbQuery.append(" AND ISNULL(akceptacja_finalna,0) = 1");
		else if(invoiceType == ReportUtilsIC.RODZAJ_FAKTURY_ZAREJESTROWANA)
			sbQuery.append(" AND ISNULL(akceptacja_finalna,0) != 1");

		dumper = new UniversalTableDumper(); 
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "raport.xls");
		poiDumper.openFile(xlsFile);
		dumper.addDumper(poiDumper);
		
		dumper.addRow(new String[] { "Centrum kosztowe",
				"Konto",
				"ID dokumentu",
				"Data wystawienia",
				"Data ksi�gowania",
				"Dostawca",
				"Numer faktury",
				"Ostatnia uwaga",
				"Kwota netto",
                "Waluta",
				"Zaakceptowana",
                "Komentarze do pozycji"}
		);
			
		try {
			ps = DSApi.context().prepareStatement(sbQuery.toString());
			int pos = 1;
			for (String projectNb : projectsList)
				ps.setInt(pos++, Integer.parseInt(projectNb));
			SimpleSQLHelper sh = new SimpleSQLHelper();
			ps.setInt(pos++, DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND).getId().intValue());
            Field walutaEnum = DocumentKind.findByCn("invoice_ic").getFieldByCn(InvoiceICLogic.WALUTA_FIELD_CN);
			if (dateFrom != null) ps.setDate(pos++, sh.parseDate(dateFrom));
			if (dateTo != null) ps.setDate(pos++, sh.parseDate(dateTo));

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				dumper.newLine();
				dumper.addText(rs.getString("name"));
				dumper.addText(rs.getString("accountNumber"));
                long documentId = rs.getLong("document_id");
                dumper.addLong(documentId);
				dumper.addDate(rs.getDate("data_wystawienia"));
				dumper.addDate(rs.getDate("data_ksiegowania"));
				dumper.addText(rs.getString("dostawca"));
				dumper.addText(rs.getString("nr_faktury"));
				dumper.addText(rs.getString("descriptionAmount"));
				dumper.addMoney(rs.getDouble("amount"), true, 2);
                int wal = rs.getInt("waluta");
                if(rs.wasNull()){
                    dumper.addText("(brak)");
                } else {
                    dumper.addText(walutaEnum.getEnumItem(wal).getCn());
                }
				dumper.addText(rs.getString("accepted"));
                dumper.addText(getCommentsForDocument(documentId));
				dumper.dumpLine();
			}
			rs.close();
		}catch(SQLException ex) {
            log.error("B��d SQL dla zapytania:" + sbQuery.toString());
            log.error(ex.getMessage(), ex);
        } catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		} finally {
			DSApi.context().closeStatement(ps);
			if (dumper != null)
				dumper.closeFileQuietly();
		}		
	}

    private static String getCommentsForDocument(long documentId) throws EdmException {
        StringBuilder sb = new StringBuilder();
        for(CentrumKosztowDlaFaktury ckdf : CentrumKosztowDlaFaktury.findByDocumentId(documentId)){
            if(ckdf.getRemarkId() != null){
                sb.append(AccountNumberComment.find(ckdf.getRemarkId()).getName()).append(", ");
            }
        }

        if(sb.length() > 0){
            return sb.substring(0, sb.length()-2);
        } else {
            return "";
        }
    }
}

