package pl.compan.docusafe.parametrization.ic.reports;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;

/**
 * Klasa krotkiego podsumowania projektu [IC]
 * @author r.powroznik
 */
public class ProjectsShortSummaryReport extends Report
{	
	UniversalTableDumper dumper;

	@Override
	public void initParametersAvailableValues() throws ReportException
	{
		super.initParametersAvailableValues();
		try
		{
			for(ReportParameter rp:params)
			{
				if(rp.getFieldCn().equals("project"))
					rp.setAvailableValues(ReportUtilsIC.getProjectsMap(DSApi.context().getDSUser()));
			}
		}catch (Exception e) {
			throw new ReportException(e);
		}
	}
	
	public void doReport() throws Exception 
	{
		PreparedStatement ps = null;
		StringBuilder sbQuery = new StringBuilder();		
		List<String> projectsList = new ArrayList<String> ();
		
		for(ReportParameter p: params) {
			if(p.getValue() instanceof String)
				projectsList.add(p.getValueAsString());
			else if (p.getValue() instanceof String[]) {
				for (String str : (String[]) p.getValue())
					projectsList.add(str);
			}
		}		
		sbQuery.append(" SELECT cent.name ,ic.waluta,");
		sbQuery.append(" SUM(CASE WHEN ISNULL(ic.akceptacja_finalna, 0) != 1 THEN ck.amount ELSE 0 END) AS sum1,");
		sbQuery.append(" SUM(CASE WHEN ISNULL(ic.akceptacja_finalna, 0) = 1 THEN ck.amount ELSE 0 END) AS sum2");
		sbQuery.append(" FROM dsg_centrum_kosztow_faktury ck");
		sbQuery.append(" JOIN dsg_invoice_ic ic ON ic.document_id = ck.documentId");
		sbQuery.append(" JOIN dsg_centrum_kosztow cent ON cent.id = ck.centrumId");
		sbQuery.append(" JOIN ds_document doc ON doc.id = ic.document_id " );
		
		
		sbQuery.append(" WHERE ck.centrumId IN (");

		for (int i = 0; i < projectsList.size(); i++)
			if (i == projectsList.size() -1 ) sbQuery.append("?)");
			else sbQuery.append("?,");

		sbQuery.append(" AND doc.dockind_id = ?");
		sbQuery.append(" AND ic.status !=6 AND ic.status != 666");
		sbQuery.append(" GROUP BY cent.name, ic.waluta");
		sbQuery.append(" ORDER BY cent.name, ic.waluta");	

			
		dumper = new UniversalTableDumper(); 
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "raport.xls");
		poiDumper.openFile(xlsFile);
		dumper.addDumper(poiDumper);
		
		dumper.addRow(new String[] {"Centrum koszt�w", "Waluta", 
					"Suma kwot faktur zarejestrowanych",
					"Suma kwot faktur zaakceptowanych"}
		);
			
		try 
		{
			ps = DSApi.context().prepareStatement(sbQuery.toString());
			for (int i = 0; i < projectsList.size(); i++)
			{
				ps.setInt(i+1, Integer.parseInt(projectsList.get(i)));
			}
			ps.setInt(projectsList.size()+1, DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND).getId().intValue());
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				dumper.newLine();
				dumper.addText(rs.getString("name"));
				dumper.addText(ReportUtilsIC.convertIntegerToCurrencyString(rs.getInt("waluta")));
				dumper.addMoney(rs.getDouble("sum1"), true, 2);
				dumper.addMoney(rs.getDouble("sum2"), true, 2);
				dumper.dumpLine();
			}
			rs.close();
		}
		catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		}
		finally	{
			DSApi.context().closeStatement(ps);
			if (dumper != null)
				dumper.closeFileQuietly();
		}		
	}
}
