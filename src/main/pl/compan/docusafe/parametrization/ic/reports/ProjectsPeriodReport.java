package pl.compan.docusafe.parametrization.ic.reports;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.DateUtils;

/**
 * Klasa raportu podgladowego projektu [IC]
 * @author r.powroznik
 */
public class ProjectsPeriodReport extends Report
{
	static final Logger log = LoggerFactory.getLogger(ProjectsDemonstrationReport.class);
	UniversalTableDumper dumper;
	
	@Override
	public void initParametersAvailableValues() throws ReportException
	{
		super.initParametersAvailableValues();
		try
		{
			for(ReportParameter rp:params)
			{
				if(rp.getFieldCn().equals("project"))
					rp.setAvailableValues(ReportUtilsIC.getProjectsMap(DSApi.context().getDSUser()));
				if(rp.getFieldCn().equals("period"))
					rp.setAvailableValues(ReportUtilsIC.getPeriodsMap());			
			}
		}catch (Exception e) 
		{
			throw new ReportException(e);
		}
	}
	
	public void doReport() throws Exception 
	{
		try
		{
			PreparedStatement ps = null;
			int periodType = -1;
			String dateFrom = null;
			String dateTo = null;
			List<String> projectsList = new ArrayList<String> ();
			Date minYearInInvoiceTable = new Date();
			Date maxYearInInvoiceTable = new Date();
			
			Integer interval = 1;
			
			for(ReportParameter p: params) {
				if (p.getValue() == null)
					continue;
				if (p.getFieldCn().equals("project")) {
					if(p.getValue() instanceof String)
						projectsList.add(p.getValueAsString());
					else if (p.getValue() instanceof String[]) {
						for (String str : (String[]) p.getValue())
							projectsList.add(str);
					}
				}
				if (p.getFieldCn().equals("period")) {
					periodType = Integer.parseInt(p.getValueAsString());
				}
				if (p.getFieldCn().equals("dateFrom")) {
					dateFrom = p.getValueAsString();
				}
				if (p.getFieldCn().equals("dateTo")) {
					dateTo = p.getValueAsString();
				}
			}
			
			try
			{
			
				ps = DSApi.context().prepareStatement(
							"SELECT MIN(data_ksiegowania) AS year_min, MAX(data_ksiegowania) AS year_max FROM dsg_invoice_ic;");	
					
				ResultSet rs = ps.executeQuery();
				while (rs.next()) 
				{
					minYearInInvoiceTable = rs.getDate("year_min");
					maxYearInInvoiceTable = rs.getDate("year_max");
				}
				rs.close();
			}
			catch (Exception e) 
			{
				log.error(e.getMessage(),e);
				
			}
			finally
			{
				DSApi.context().closeStatement(ps);
			}
			
			Date tempDateFrom;
			Date tempDateTo;
			
			dumper = new UniversalTableDumper(); 
			XlsPoiDumper poiDumper = new XlsPoiDumper();
			File xlsFile = new File(this.getDestination(), "raport.xls");
			poiDumper.openFile(xlsFile);
			dumper.addDumper(poiDumper);
			
			dumper.newLine();
			dumper.addText("Projekt");
			dumper.addText("Typ waluty");
			
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cent.name AS projekt, ic.waluta, sum(ck.amount) as suma");		
			switch (periodType) 
			{
				case ReportUtilsIC.OKRES_PODSUMOWANIA_MIESIECZNY:
					interval = 1;
					break;
				case ReportUtilsIC.OKRES_PODSUMOWANIA_KWARTALNY:
					interval = 3;
					break;
				case ReportUtilsIC.OKRES_PODSUMOWANIA_ROCZNY:
					interval = 12;
			}
			if(dateFrom!=null)
			{
				
			}
			else
				tempDateFrom = minYearInInvoiceTable;
			tempDateFrom = dateFrom!=null ? DateUtils.parseDateFromYear(dateFrom):minYearInInvoiceTable;
			tempDateTo = dateTo!=null ? DateUtils.parseDateFromYear(dateTo):maxYearInInvoiceTable;
			Calendar cal = Calendar.getInstance(new Locale("pl","PL"));
			cal.setTime(tempDateFrom);
			cal.set(Calendar.DAY_OF_MONTH, 1);
			if(interval==1)
			{
			}
			else if(interval==3)
			{
				cal.set(Calendar.MONTH, (int)(Math.floor(cal.get(Calendar.MONTH)/4))*4);
			}
			else if(interval == 12)
			{
				cal.set(Calendar.MONTH, 0);
			}
			
			Calendar auxilary = Calendar.getInstance(); 
			
			SimpleDateFormat monthformat = new SimpleDateFormat("MMMM yyyy");
			while(cal.getTime().before(tempDateTo))
			{
				if(interval==1)
				{
					dumper.addText(monthformat.format(cal.getTime()));
				}
				else if(interval==3)
				{
					dumper.addText("Kwartal "+Math.ceil((cal.get(Calendar.MONTH)+1)/4) + " " + cal.get(Calendar.YEAR));
				}
				else if(interval == 12)
				{
					dumper.addText(new Integer(cal.get(Calendar.YEAR)).toString());
				}
				cal.add(Calendar.MONTH, interval);
			}
					
			
			
			sbQuery.append(" FROM dsg_centrum_kosztow_faktury ck");
			sbQuery.append(" JOIN dsg_invoice_ic ic ON ic.document_id = ck.documentId");
			sbQuery.append(" JOIN dsg_centrum_kosztow cent ON cent.id = ck.centrumId");
			sbQuery.append(" JOIN ds_document doc ON doc.id = ic.document_id " );
			
			
			sbQuery.append(" WHERE ck.centrumId = ?");
			sbQuery.append(" AND ic.waluta = ?");
	
			sbQuery.append(" AND doc.dockind_id = ?");
			sbQuery.append(" AND ic.status != 6");
			
			sbQuery.append(" AND ISNULL(ic.data_ksiegowania, " + DateUtils.formatSqlDate(maxYearInInvoiceTable) +") >= ? ");
			sbQuery.append(" AND ISNULL(ic.data_ksiegowania, " + DateUtils.formatSqlDate(maxYearInInvoiceTable) +") <= ?");
			
			sbQuery.append(" GROUP BY cent.name, ic.waluta");
			sbQuery.append(" ORDER BY cent.name, ic.waluta");
				
			StringBuilder sb = new StringBuilder();
			sb.append("select distinct cent.name, ic.waluta ");
			sb.append(" FROM dsg_centrum_kosztow_faktury ck");
			sb.append(" JOIN dsg_invoice_ic ic ON ic.document_id = ck.documentId");
			sb.append(" JOIN dsg_centrum_kosztow cent ON cent.id = ck.centrumId");
			sb.append(" JOIN ds_document doc ON doc.id = ic.document_id " );
			sb.append(" where ck.centrumId = ?");
			
			
			PreparedStatement ps1 = null;
			Map<Integer, Entry> projectsMap = new HashMap<Integer, Entry>();
			try
			{
				ps1 = DSApi.context().prepareStatement(sb.toString());
				PreparedStatement ps2 = DSApi.context().prepareStatement("select name from dsg_centrum_kosztow where id=?");
				for(String p:projectsList)
				{
					Integer id = Integer.parseInt(p);
					Entry e = new Entry();
					e.setId(id);
					
					
					ps2.setInt(1, id);
					ResultSet rs2 = ps2.executeQuery();
					rs2.next();
					e.setName(rs2.getString(1));
					rs2.close();
					
					ps1.setInt(1, id);
					ResultSet rs1 = ps1.executeQuery();
					while(rs1.next())
					{
						e.getWaluty().add(rs1.getInt("waluta"));
					}
					projectsMap.put(id, e);
				}
				DSApi.context().closeStatement(ps2);
				
			}
			catch (Exception e) 
			{
				log.error(e.getMessage(),e);
				DSApi.context().closeStatement(ps1);
			}
			
			
			try 
			{
				ps = DSApi.context().prepareStatement(sbQuery.toString());
				ps.setInt(3, DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND).getId().intValue());
				
				for(String p:projectsList)
				{
					Integer id = Integer.parseInt(p);
					
					for(Integer wal:projectsMap.get(id).getWaluty())
					{
						dumper.newLine();
						dumper.addText(projectsMap.get(id).name);
						dumper.addText(ReportUtilsIC.convertIntegerToCurrencyString(wal));
						ps.setInt(1, id);
						ps.setInt(2, wal);
						
						auxilary = Calendar.getInstance(new Locale("pl","PL"));
						cal = Calendar.getInstance(new Locale("pl","PL"));
						cal.setTime(tempDateFrom);
						if(interval==1)
						{
						}
						else if(interval==3)
						{
							cal.set(Calendar.MONTH, (int)(Math.floor(cal.get(Calendar.MONTH)/4))*4);
						}
						else if(interval == 12)
						{
							cal.set(Calendar.MONTH, 0);
						}
						auxilary.setTime(cal.getTime());
						auxilary.add(Calendar.MONTH, interval);
						auxilary.add(Calendar.DAY_OF_MONTH, -1);
						
						while(cal.getTime().before(tempDateTo))
						{
							ps.setDate(4, new java.sql.Date(cal.getTime().getTime()));
							ps.setDate(5, new java.sql.Date(auxilary.getTime().getTime()));
							
							ResultSet rs = ps.executeQuery();
							if(rs.next())
							{
								dumper.addMoney(rs.getDouble("suma"), true, 2);
							}
							rs.close();
							DSApi.context().closeStatement(ps);
						}
					}
				}
			}
			catch (Exception ex) 
			{
				log.error(ex.getMessage(), ex);
			}
			finally 
			{
				DSApi.context().closeStatement(ps);
				if (dumper != null)
					dumper.closeFileQuietly();
			}		
		}
		catch(Exception e)
		{
			log.error(e.getMessage(),e);
		}
	}
	
	private class Entry 
	{
		private String name;
		private Integer id;
		private List<Integer> waluty;
		
		public Entry()
		{
			waluty = new ArrayList<Integer>();
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public List<Integer> getWaluty() {
			return waluty;
		}
		public void setWaluty(List<Integer> waluty) {
			this.waluty = waluty;
		}
		
		
	}
}

