package pl.compan.docusafe.parametrization.ic.reports;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.DateUtils;

/**
 * Klasa raportu podgladowego projektu [IC]
 * @author r.powroznik
 */
public class ProjectsDemonstrationReport extends Report
{
	static final Logger log = LoggerFactory.getLogger(ProjectsDemonstrationReport.class);
	UniversalTableDumper dumper;

    //wydzielone wyra�enia roku i miesi�ca dla danej faktury - IC raz zmieni�o kolumn� raportu, teraz wystarczy zmieni�
    //tylko tutaj
    private final static String MONTH_EXPRESSION = "MONTH(ISNULL(ic.data_ksiegowania, GETDATE()))";
    private final static String YEAR_EXPRESSION = "YEAR(ISNULL(ic.data_ksiegowania, GETDATE()))";

	@Override
	public void initParametersAvailableValues() throws ReportException
	{
		super.initParametersAvailableValues();
		try
		{
			for(ReportParameter rp:params)
			{
				if(rp.getFieldCn().equals("project"))
					rp.setAvailableValues(ReportUtilsIC.getProjectsMap(DSApi.context().getDSUser()));
				if(rp.getFieldCn().equals("period"))
					rp.setAvailableValues(ReportUtilsIC.getPeriodsMap());			
			}
		}catch (Exception e) 
		{
			throw new ReportException(e);
		}
	}
	
	private StringBuilder createPartOfSqlQuarterly(int dateFrom, int dateTo) {
		StringBuilder sb = new StringBuilder();
		String year = null;
		for (Integer y = dateFrom; y <= dateTo; y++) {
			year = y.toString();
			sb.append(" ,sum(case when ").append(MONTH_EXPRESSION).append("<4 AND ").append(YEAR_EXPRESSION).append("=".concat(year).concat(" then ck.amount else 0 end) as Kwartal_I_"+year));
			sb.append(" ,sum(case when ").append(MONTH_EXPRESSION).append(">3 AND  ").append(MONTH_EXPRESSION).append("<7 AND  ").append(YEAR_EXPRESSION).append("=".concat(year).concat(" then ck.amount else 0 end) as Kwartal_II_"+year));
			sb.append(" ,sum(case when ").append(MONTH_EXPRESSION).append(">6 AND  ").append(MONTH_EXPRESSION).append("<10 AND ").append(YEAR_EXPRESSION).append("=".concat(year).concat(" then ck.amount else 0 end) as Kwartal_III_"+year));
			sb.append(" ,sum(case when ").append(MONTH_EXPRESSION).append(">9 AND ").append(YEAR_EXPRESSION).append("=".concat(year).concat(" then ck.amount else 0 end) as Kwartal_IV_"+year));
		}
		return sb;
	}
	
	private String [] createHeadersQuarterly(int dateFrom, int dateTo) {
		List <String> tempHeaders = new ArrayList <String>();
		String headersQuarterly [] = { "Kwartal_I", "Kwartal_II", "Kwartal_III", "Kwartal_IV" };
		String year = null;
		for (Integer y = dateFrom; y <= dateTo; y++) {
			year = y.toString();
			for (int i = 0; i < headersQuarterly.length; i++)
				tempHeaders.add(headersQuarterly[i].concat("_").concat(year));
		}
		return (String[]) tempHeaders.toArray(new String[tempHeaders.size()]);
	}
	
	private StringBuilder createPartOfSqlMonthly(int dateFrom, int dateTo) {
		StringBuilder sb = new StringBuilder();
		String year = null;
		for (Integer y = dateFrom; y <= dateTo; y++) {
			year = y.toString();			
			sb.append(" ,sum(case when ").append(MONTH_EXPRESSION).append("=1 AND ").append(YEAR_EXPRESSION).append("=").append(year).append(" then ck.amount else 0 end) as Styczen_").append(year);
			sb.append(" ,sum(case when ").append(MONTH_EXPRESSION).append("=2 AND ").append(YEAR_EXPRESSION).append("=").append(year).append(" then ck.amount else 0 end) as Luty_").append(year);
			sb.append(" ,sum(case when ").append(MONTH_EXPRESSION).append("=3 AND ").append(YEAR_EXPRESSION).append("=").append(year).append(" then ck.amount else 0 end) as Marzec_").append(year);
			sb.append(" ,sum(case when ").append(MONTH_EXPRESSION).append("=4 AND ").append(YEAR_EXPRESSION).append("=").append(year).append(" then ck.amount else 0 end) as Kwiecien_").append(year);
			sb.append(" ,sum(case when ").append(MONTH_EXPRESSION).append("=5 AND ").append(YEAR_EXPRESSION).append("=").append(year).append(" then ck.amount else 0 end) as Maj_").append(year);
			sb.append(" ,sum(case when ").append(MONTH_EXPRESSION).append("=6 AND ").append(YEAR_EXPRESSION).append("=").append(year).append(" then ck.amount else 0 end) as Czerwiec_").append(year);
			sb.append(" ,sum(case when ").append(MONTH_EXPRESSION).append("=7 AND ").append(YEAR_EXPRESSION).append("=").append(year).append(" then ck.amount else 0 end) as Lipiec_").append(year);
			sb.append(" ,sum(case when ").append(MONTH_EXPRESSION).append("=8 AND ").append(YEAR_EXPRESSION).append("=").append(year).append(" then ck.amount else 0 end) as Sierpien_").append(year);
			sb.append(" ,sum(case when ").append(MONTH_EXPRESSION).append("=9 AND ").append(YEAR_EXPRESSION).append("=").append(year).append(" then ck.amount else 0 end) as Wrzesien_").append(year);
			sb.append(" ,sum(case when ").append(MONTH_EXPRESSION).append("=10 AND ").append(YEAR_EXPRESSION).append("=").append(year).append(" then ck.amount else 0 end) as Pazdziernik_").append(year);
			sb.append(" ,sum(case when ").append(MONTH_EXPRESSION).append("=11 AND ").append(YEAR_EXPRESSION).append("=").append(year).append(" then ck.amount else 0 end) as Listopad_").append(year);
			sb.append(" ,sum(case when ").append(MONTH_EXPRESSION).append("=12 AND ").append(YEAR_EXPRESSION).append("=").append(year).append(" then ck.amount else 0 end) as Grudzien_").append(year);
		}
		return sb;
	}
	
	private String[] createHeadersMonthly(int dateFrom, int dateTo) {
		List <String> tempHeaders = new ArrayList <String>();
		String headersMonthly[] = { "Styczen", "Luty", "Marzec", "Kwiecien",
				"Maj", "Czerwiec", "Lipiec", "Sierpien", "Wrzesien", "Pazdziernik", "Listopad", "Grudzien" };
		String year = null;
		for (Integer y = dateFrom; y <= dateTo; y++) {
			year = y.toString();
			for (int i = 0; i < headersMonthly.length; i++)
				tempHeaders.add(headersMonthly[i].concat("_").concat(year));
		}
		return (String[]) tempHeaders.toArray(new String[tempHeaders.size()]);
	}

	public void doReport() throws Exception 
	{
		PreparedStatement ps = null;
		int periodType = -1;
		String dateFrom = null;
		String dateTo = null;
		List<String> projectsList = new ArrayList<String> ();
		List<Integer> yearsList = new ArrayList<Integer> ();
		List<String> headersYearly = new ArrayList<String> ();
		int minYearInInvoiceTable = 0;
		int maxYearInInvoiceTable = 0;
		
		for(ReportParameter p: params) {
			if (p.getValue() == null)
				continue;
			if (p.getFieldCn().equals("project")) {
				if(p.getValue() instanceof String)
					projectsList.add(p.getValueAsString());
				else if (p.getValue() instanceof String[]) {
					for (String str : (String[]) p.getValue())
						projectsList.add(str);
				}
			}
			if (p.getFieldCn().equals("period")) {
				periodType = Integer.parseInt(p.getValueAsString());
			}
			if (p.getFieldCn().equals("dateFrom")) {
				dateFrom = p.getValueAsString();
			}
			if (p.getFieldCn().equals("dateTo")) {
				dateTo = p.getValueAsString();
			}
		}
		
		try {
			StringBuilder sbMinMaxYear = new StringBuilder()
    			.append("SELECT MIN(").append(YEAR_EXPRESSION).append(") AS year_min, MAX(").append(YEAR_EXPRESSION).append(") AS year_max")
	    		.append(" FROM dsg_centrum_kosztow_faktury ck")
			    .append(" JOIN dsg_invoice_ic ic ON ic.document_id = ck.documentId")
		    	.append(" JOIN dsg_centrum_kosztow cent ON cent.id = ck.centrumId")
    			.append(" JOIN ds_document doc ON doc.id = ic.document_id")
	    		.append(" WHERE doc.dockind_id = ?")
		    	.append(" AND ic.status != 6 AND ic.status != 666")
			    .append(" AND ck.centrumId IN (");
			
			for (int i = 0; i < projectsList.size(); i++)
				if (i == projectsList.size() -1 ) sbMinMaxYear.append("?);");
				else sbMinMaxYear.append("?,");

            log.info("SQL Statement: " + sbMinMaxYear);
			ps = DSApi.context().prepareStatement(sbMinMaxYear.toString());	
			ps.setInt(1, DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND).getId().intValue());
			for (int i = 0; i < projectsList.size(); i++) {
				ps.setInt(i+2, Integer.parseInt(projectsList.get(i)));
			}
			
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				minYearInInvoiceTable = rs.getInt("year_min");
				maxYearInInvoiceTable = rs.getInt("year_max");
			}
			rs.close();
		}
		catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		}
		finally {
			DSApi.context().closeStatement(ps);
		}
		
		Integer tempDateFrom = DateUtils.parseJsYear(dateFrom);
		Integer tempDateTo = DateUtils.parseJsYear(dateTo);
		if (tempDateFrom == null || tempDateTo == null) {
			int currYear = java.util.Calendar.getInstance().get(java.util.Calendar.YEAR);
			tempDateFrom = (currYear - minYearInInvoiceTable) > 9 ? currYear - 10 : minYearInInvoiceTable; // currYear;
			tempDateTo = maxYearInInvoiceTable; //currYear;
		}
		
		String headersMonthly [] = createHeadersMonthly(tempDateFrom, tempDateTo);
		StringBuilder sqlPartMonthly = createPartOfSqlMonthly(tempDateFrom, tempDateTo);
		
		String headersQuarterly [] = createHeadersQuarterly(tempDateFrom, tempDateTo);		
		StringBuilder sqlPartQuarterly = createPartOfSqlQuarterly(tempDateFrom, tempDateTo);
		
		String headersOverall [] = { "Suma" };
		String sqlPartOverall = ", SUM(ck.amount) as suma";		
		
		dumper = new UniversalTableDumper(); 
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "raport.xls");
		poiDumper.openFile(xlsFile);
		dumper.addDumper(poiDumper);
		
		dumper.newLine();
		dumper.addText("Projekt");
		dumper.addText("Typ waluty");
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT cent.name AS projekt, ic.waluta");		
		switch (periodType) {
			case ReportUtilsIC.OKRES_PODSUMOWANIA_MIESIECZNY:
				dumper.addRowWithoutNewLine(headersMonthly);
				sbQuery.append(sqlPartMonthly);
				break;
			case ReportUtilsIC.OKRES_PODSUMOWANIA_KWARTALNY:
				dumper.addRowWithoutNewLine(headersQuarterly);
				sbQuery.append(sqlPartQuarterly);
				break;
			case ReportUtilsIC.OKRES_PODSUMOWANIA_ROCZNY:
				
				if (tempDateFrom != null && tempDateTo != null) {					
					for (Integer y = tempDateFrom; y <= tempDateTo; y++) 
						headersYearly.add(y.toString());				
				}
				else if (dateFrom != null)
					for (Integer y = tempDateFrom; y <= maxYearInInvoiceTable; y++) 
						headersYearly.add(y.toString());
				else if (tempDateTo != null)
					for (Integer y = minYearInInvoiceTable; y <= tempDateTo; y++) 
						headersYearly.add(y.toString());
				else
					for (Integer y = minYearInInvoiceTable; y <= maxYearInInvoiceTable; y++) 
						headersYearly.add(y.toString());

				for (String str : headersYearly) {
					dumper.addText("ROK_".concat(str));
					sbQuery.append(" ,SUM(CASE WHEN ").append(YEAR_EXPRESSION).append("=".concat(str).concat(" THEN ck.amount ELSE 0 END) AS ROK_").concat(str));
				}
					
				break;
			case ReportUtilsIC.OKRES_PODSUMOWANIA_CALOSCIOWY:
				dumper.addText(headersOverall[0]);
				sbQuery.append(sqlPartOverall);
				break;
				
		}
		sbQuery.append(" FROM dsg_centrum_kosztow_faktury ck");
		sbQuery.append(" JOIN dsg_invoice_ic ic ON ic.document_id = ck.documentId");
		sbQuery.append(" JOIN dsg_centrum_kosztow cent ON cent.id = ck.centrumId");
		sbQuery.append(" JOIN ds_document doc ON doc.id = ic.document_id " );
		
		
		sbQuery.append(" WHERE ck.centrumId IN (");

		for (int i = 0; i < projectsList.size(); i++)
			if (i == projectsList.size() -1 ) sbQuery.append("?)");
			else sbQuery.append("?,");

		sbQuery.append(" AND doc.dockind_id = ?");
		sbQuery.append(" AND ic.status != 6 AND ic.status != 666 ");
		sbQuery.append(" GROUP BY cent.name, ic.waluta");
		sbQuery.append(" ORDER BY cent.name, ic.waluta");
			
		try 
		{
            log.info("SQL Statement: " + sbQuery);
			ps = DSApi.context().prepareStatement(sbQuery.toString());
			for (int i = 0; i < projectsList.size(); i++)
			{
				ps.setInt(i + 1, Integer.parseInt(projectsList.get(i)));
			}
			ps.setInt(projectsList.size()+1, DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND).getId().intValue());

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				dumper.newLine();
				dumper.addText(rs.getString("Projekt"));
				dumper.addText(ReportUtilsIC.convertIntegerToCurrencyString(rs.getInt("waluta")));
				switch (periodType) {
				case ReportUtilsIC.OKRES_PODSUMOWANIA_MIESIECZNY:
					for (String str : headersMonthly) dumper.addMoney(rs.getDouble(str), true, 2);
					break;
				case ReportUtilsIC.OKRES_PODSUMOWANIA_KWARTALNY:
					for (String str : headersQuarterly) dumper.addMoney(rs.getDouble(str), true, 2);
					break;
				case ReportUtilsIC.OKRES_PODSUMOWANIA_ROCZNY:
					for (String str: headersYearly) dumper.addMoney(rs.getDouble("ROK_".concat(str)), true, 2);
					break;
				case ReportUtilsIC.OKRES_PODSUMOWANIA_CALOSCIOWY:
					dumper.addMoney(rs.getDouble("suma"), true, 2);
					break;
				}
				dumper.dumpLine();
			}
			rs.close();
		}
		catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		}
		finally {
			DSApi.context().closeStatement(ps);
			if (dumper != null)
				dumper.closeFileQuietly();
		}		
	}
}

