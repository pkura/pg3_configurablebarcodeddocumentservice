package pl.compan.docusafe.parametrization.ic.reports;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;

public class UserTasklistReport extends Report
{

	String sql = "select count(distinct(tl.dso_document_id)) from dsw_tasklist tl " +
			"join dsg_invoice_ic ic on ic.document_id = tl.dso_document_id " +
			"where tl.dsw_resource_res_key = ? " +
			"and DATEDIFF(day, ISNULL(tl.dsw_activity_laststatetime,current_timestamp), current_timestamp)>=? " +
			"and DATEDIFF(day, ISNULL(tl.dsw_activity_laststatetime,current_timestamp), current_timestamp)<? ";

	String sqlSAD = "select count(distinct(tl.dso_document_id)) from dsw_tasklist tl " +
		"join dsg_sad ic on ic.document_id = tl.dso_document_id " +
		"where tl.dsw_resource_res_key = ? " +
		"and DATEDIFF(day, tl.dsw_activity_laststatetime, current_timestamp)>=? " +
		"and DATEDIFF(day, tl.dsw_activity_laststatetime, current_timestamp)<? ";

	String addSql = "";

	UniversalTableDumper dumper;

	public void doReport() throws Exception
	{
		List<Integer> intervals = new ArrayList<Integer>();
		intervals.add(0);
		intervals.add(1);
		intervals.add(3);
		intervals.add(10);
		intervals.add(Integer.MAX_VALUE);

		Integer[] akceptacje = new Integer[0]; 

		List<DSUser> users = new ArrayList<DSUser>();
		for(ReportParameter p: params)
		{
			String[] vals = new String[0];
			if(p.getValue() instanceof String)
			{
				vals = new String[1];
				vals[0] = p.getValueAsString();
			}
			else if(p.getValue() instanceof String[])
			{
				vals = (String[])p.getValue();
			}

			if("user".equals(p.getFieldCn()))
			{
				for(String s : vals)
				{
					try
					{
						users.add(DSUser.findByUsername(s));
					}
					catch (Exception e)
					{
						log.debug("Nie znaleziono u�ytkownika", e);
					}
				}
			}
			if("akceptacja".equals(p.getFieldCn()))
			{
				if(vals.length>0)
				{
					addSql = "and ic.status in (";
					
					akceptacje = new Integer[vals.length];
					for(int i=0;i<vals.length;i++)
					{
						akceptacje[i] = Integer.parseInt(vals[i]);
						if(i==0)
							addSql+="?";
						else
							addSql+=",?";
					}
					addSql+=")";
					
				}
			}
		}

		dumper = new UniversalTableDumper();

		/*CsvDumper csvDumper = new CsvDumper();
		File csvFile = new File(this.getDestination(), "report.csv");
		csvDumper.openFile(csvFile);
		dumper.addDumper(csvDumper);*/

		Collections.sort(users, DSUser.LASTNAME_COMPARATOR);

		Integer[] summary = new Integer[5];
		Arrays.fill(summary, 0);

		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "raport.xls");
		poiDumper.openFile(xlsFile);
		dumper.addDumper(poiDumper);


		dumpHeader();

		PreparedStatement ps = null;
		ResultSet rs = null;
		PreparedStatement psSAD = null;
		ResultSet rsSAD = null;
		for(DSUser u: users)
		{
			dumper.newLine();
			dumper.addText(u.asFirstnameLastnameName());
			DSDivision[] divs =  u.getAllDivisions();
			if(divs==null || divs.length<1)
				dumper.addText("");
			else
			{
				boolean added = false;
				for(DSDivision d : divs)
				{
					if(!d.isPosition() && !d.isGroup() && !d.isHidden())
					{
						dumper.addText(d.getName());
						added = true;
						break;
					}
				}
				if(!added)
				{
					dumper.addText("");
				}
			}


			Integer userSum = 0;
			for(int i=1;i<intervals.size();i++)
			{
				try
				{
					if(akceptacje.length>0)
					{
						ps = DSApi.context().prepareStatement(sql + addSql);
						psSAD = DSApi.context().prepareStatement(sqlSAD + addSql);
					}
					else
					{
						ps = DSApi.context().prepareStatement(sql);
						psSAD = DSApi.context().prepareStatement(sqlSAD);
					}
					ps.setString(1, u.getName());
					ps.setInt(2, intervals.get(i-1));
					ps.setInt(3, intervals.get(i));

					psSAD.setString(1, u.getName());
					psSAD.setInt(2, intervals.get(i-1));
					psSAD.setInt(3, intervals.get(i));
					if(akceptacje.length>0)
					{
						for(int j=0;j<akceptacje.length;j++)
						{
							ps.setInt(4 + j, akceptacje[j]);
							psSAD.setInt(4 + j, akceptacje[j]);
						}
					}
					rs = ps.executeQuery();
					rsSAD = psSAD.executeQuery();
					rs.next();
					rsSAD.next();
					Integer value = rs.getInt(1);
					value += rsSAD.getInt(1);
					summary[i] += value;
					userSum += value;
					dumper.addInteger(value);
				}
				catch(Exception e)
				{
					dumper.addInteger(0);
					log.error(e.getMessage(),e);
				}

				finally
				{
					DSApi.context().closeStatement(ps);
				}
			}
			dumper.addInteger(userSum);
			dumper.dumpLine();

		}
		dumper.newLine();
		dumper.addText("");
		dumper.addText("SUMA");
		Integer sum = 0;
		for(int i=1;i<summary.length;i++)
		{
			sum += summary[i];
			dumper.addInteger(summary[i]);
		}
		dumper.addInteger(sum);
		dumper.dumpLine();
		dumper.closeFileQuietly();
	}
	@Override
	public void initParametersAvailableValues() throws ReportException
	{
		super.initParametersAvailableValues();
		Map<String, String> mapka = new HashMap<String, String>();
		mapka.put("aaaa", "Nazwa dla usera aaa");
		mapka.put("bbb", "Nazwa dla usera bbb");
		mapka.put("ccc", "Nazwa dla usera ccc");
		for(ReportParameter rp:params)
		{
			if(rp.getFieldCn().equals("dekretacja"))
				rp.setAvailableValues(mapka);
		}
	}
	private void dumpHeader() throws Exception
	{
		dumper.newLine();
		dumper.addText("U�ytkownik");
		dumper.addText("Dzia�");
		dumper.addText("Mniej ni� 1 dzie�");
		dumper.addText("Pomi�dzy 1 i 3 dni");
		dumper.addText("Pomi�dzy 3 i 10 dni");
		dumper.addText("Powy�ej 10 dni");
		dumper.addText("Suma");
		dumper.dumpLine();
	}

	private String getSeries(Integer i)
	{
		switch (i)
		{
		case 1:
			return "Mniej ni� 1 dzie�";
		case 2:
			return "Pomi�dzy 1 i 3 dni";
		case 3:
			return "Pomi�dzy 3 i 10 dni";
		case 4:
			return "Powy�ej 10 dni";
		default:
			return "Blad";
		}
	}
}
