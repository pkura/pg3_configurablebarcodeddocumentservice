package pl.compan.docusafe.parametrization.ic.reports;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.dictionary.AccountNumberComment;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.ic.InvoiceICLogic;
import pl.compan.docusafe.parametrization.ic.VatEntry;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.CsvDumper;
import pl.compan.docusafe.reports.tools.SimpleSQLHelper;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.base.Objects;

public class InvoiceSentToAssecoReport extends Report{
	private static final Logger LOG = LoggerFactory.getLogger(InvoiceSentToAssecoReport.class);

	String sql = "select doc.ID, doc.CTIME, docAcp.acceptanceCn, docAcp.acceptanceTime " +
            " from DS_DOCUMENT doc " +
            " inner join dsg_invoice_ic ic on (doc.id = ic.document_id) " +
            " inner join DS_DOCUMENT_ACCEPTANCE docAcp on (doc.ID = docAcp.document_id and docAcp.acceptanceCn='ksiegowa') " +
            " where ic.export_type = 20 AND ( ic.status in (1000,2000) or ic.last_safo_message is not null) ";//order by doc.ID ";

	
	protected UniversalTableDumper dumper = null;
	
	@Override
	public void doReport() throws Exception {
		LOG.info("InvoiceSentToAssecoReport.doReport");
		PreparedStatement ps = null;
		String dateFrom = null;
		String dateTo = null;

		try {
			super.initParametersAvailableValues();
			
			for(ReportParameter p: params) {
				if (p.getFieldCn().equals("cDateFrom")) {
					dateFrom = p.getValueAsString();
				}
				if (p.getFieldCn().equals("cDateTo")) {
					dateTo = p.getValueAsString();
				}
			}		
			
			initDumpers();
			dumpHeader();

			if (dateFrom != null) {
				sql+=" AND docAcp.acceptanceTime >= ?";
            }
			if (dateTo != null) {
				sql+=" AND docAcp.acceptanceTime <= ?";
            }
            sql += " ORDER BY doc.id DESC ";
			LOG.info("QUERY: "+sql);
			ps = DSApi.context().prepareStatement(sql);
			
			SimpleSQLHelper sh = new SimpleSQLHelper();
			if (dateFrom != null) ps.setDate(1, sh.parseDate(dateFrom));
			if (dateTo != null) ps.setDate(2, sh.parseDate(dateTo));
			
			ResultSet rs = ps.executeQuery();
			
	        Format formatter = new SimpleDateFormat("dd-MM-yyyy");
	        
			while (rs.next())
			{
                dumpEntry(rs,formatter);
			}
			rs.close();

		} catch(SQLException ex) {
		    LOG.error(ex.getMessage(), ex);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
			if (dumper!=null) { dumper.closeFileQuietly(); }
		}
		
	}

    private void dumpEntry(ResultSet rs,Format formatter) throws SQLException, IOException, EdmException  {
    	Long id = rs.getLong("ID");
    	DocumentKind dk = DocumentKind.findByCn(DocumentLogicLoader.INVOICE_IC_KIND);
    	FieldsManager fm = dk.getFieldsManager(id);    	
        
        DocumentAcceptance docAcc = DocumentAcceptance.find(id, "ksiegowa", null);
        List<CentrumKosztowDlaFaktury> centraKosztow = CentrumKosztowDlaFaktury.findByDocumentId(id);
        
        StringBuilder centraKosztowKodText = new StringBuilder("");
        StringBuilder centraKosztowOpisText = new StringBuilder("");
        StringBuilder centraKosztowCommentText = new StringBuilder("");
        
        for(CentrumKosztowDlaFaktury c:centraKosztow){
            if(centraKosztowKodText.length()!=0)
            	centraKosztowKodText.append("\n");
            if(centraKosztowOpisText.length()!=0)
            	centraKosztowOpisText.append("\n");
            if(centraKosztowCommentText.length()!=0)
            	centraKosztowCommentText.append("\n");

            
            centraKosztowKodText.append(c.getCentrumCode()!=null?c.getCentrumCode():"");
            centraKosztowOpisText.append(c.getDescriptionAmount()!=null?c.getDescriptionAmount():"");
            if(c.getRemarkId()!=null)
            {
            	AccountNumberComment accNrCom = AccountNumberComment.find(c.getRemarkId());
            	centraKosztowCommentText.append(accNrCom.getName()!=null?accNrCom.getName():"");
            }else{
            	centraKosztowCommentText.append("");
            }
        }
        
        DicInvoice dic = null;
        if(fm.getValue(InvoiceICLogic.DOSTAWCA_FIELD_CN)!=null)
        	dic = (DicInvoice) fm.getValue(InvoiceICLogic.DOSTAWCA_FIELD_CN);
    	String accDatetext=null;
        if(docAcc!=null){
        	Date accDate = docAcc.getAcceptanceTime();
        	if(accDate!=null)
        		formatter.format(accDate);
        
        	if(accDate!=null)
        		accDatetext = formatter.format(accDate);
        }

        Iterable<VatEntry> vats = null;
        
        if(fm.getValue(InvoiceICLogic.VATS_FIELD_CN)!=null)
        	vats = (Iterable<VatEntry>) fm.getValue(InvoiceICLogic.VATS_FIELD_CN);
        
        StringBuilder vatCodeSb = new StringBuilder();
        StringBuilder vatNettoSb = new StringBuilder();
        StringBuilder vatNettoCostsSb = new StringBuilder();
        StringBuilder vatVat = new StringBuilder();
        StringBuilder vatVatCosts = new StringBuilder();
        
        if(vats!=null)
        	for(VatEntry v: vats){
        		if(vatCodeSb.length()!=0)
        			vatCodeSb.append("\n");
        		vatCodeSb.append(v.getVatSymbol()).append(" ").append(v.getVat()).append("%");
        		
        		if(vatNettoSb.length()!=0)
        			vatNettoSb.append("\n");
        		vatNettoSb.append(v.getNettoAmount());
        		
        		if(vatNettoCostsSb.length()!=0)
        			vatNettoCostsSb.append("\n");
        		vatNettoCostsSb.append(v.getCostsNettoAmount());
        		
        		if(vatVat.length()!=0)
        			vatVat.append("\n");
        		vatVat.append(v.getVatAmount());
        		
        		if(vatVatCosts.length()!=0)
        			vatVatCosts.append("\n");
        		vatVatCosts.append(v.getCostsVatAmount());
        	}
        boolean korekta = fm.getBoolean(InvoiceICLogic.FAKTURA_SPECJALNA_CN)
                ||  fm.getBoolean(InvoiceICLogic.CREDIT_NOTY_FIELD_CN)
                ||  fm.getBoolean(InvoiceICLogic.KOREKTY_FIELD_CN);

        dumper.newLine();
        dumper.addText(accDatetext);
        dumper.addLong(id);
        dumper.addText(fm.getStringValue(InvoiceICLogic.NUMER_FAKTURY_FIELD_CN));
        dumper.addText( dic==null?"" :dic.getNumerKontrahenta());
        dumper.addText(korekta?"Tak":"Nie");
        dumper.addText(Objects.firstNonNull(fm.getDescription(InvoiceICLogic.DATA_WYSTAWIENIA_FIELD_CN),"").toString());
        dumper.addText(Objects.firstNonNull(fm.getDescription(InvoiceICLogic.DATA_WPLYWU_CN),"").toString());
        dumper.addText(fm.getStringValue(InvoiceICLogic.KWOTA_NETTO_FIELD_CN));
        dumper.addText(fm.getStringValue(InvoiceICLogic.VAT_CN));	
        dumper.addText(fm.getStringValue(InvoiceICLogic.KWOTA_BRUTTO_FIELD_CN));
        dumper.addText(fm.getStringValue(InvoiceICLogic.WALUTA_FIELD_CN));
        dumper.addText(fm.getStringValue(InvoiceICLogic.LOKALIZACJA_CN));
        dumper.addText(fm.getStringValue(InvoiceICLogic.REJESTR_FIELD_CN));
        dumper.addText(fm.getStringValue(InvoiceICLogic.PAYMENT_METHOD_FIELD_CN));	
        dumper.addText(Objects.firstNonNull(fm.getDescription(InvoiceICLogic.DATA_PLATNOSCI_FIELD_CN),"").toString());
        dumper.addText(fm.getStringValue(InvoiceICLogic.ADVANCE_TAKER_FIELD_CN));	
        dumper.addText(fm.getStringValue(InvoiceICLogic.ADVANCE_NUMBER_FIELD_CN));	
        dumper.addText(Objects.firstNonNull(fm.getDescription(InvoiceICLogic.DATA_VAT),"").toString());	
        dumper.addText(vatCodeSb.toString());
        dumper.addText(vatNettoSb.toString());
        dumper.addText(vatNettoCostsSb.toString());
        dumper.addText(vatVat.toString());
        dumper.addText(vatVatCosts.toString());
        dumper.addText(fm.getStringValue(InvoiceICLogic.SAFO_LAST_MESSAGE_FIELD_CN));
        dumper.addText(fm.getStringValue(InvoiceICLogic.STATUS_FOR_USER_FIELD_CN));
        dumper.addText(docAcc==null?"": DSUser.findByUsername(docAcc.getUsername()).asLastnameFirstnameName());
        dumper.addText(fm.getStringValue(InvoiceICLogic.PION_CN));
        dumper.addText(fm.getStringValue(InvoiceICLogic.DZIAL_FIELD_CN));
        dumper.addText(dic==null?"" :dic.getName());
        dumper.addText(fm.getStringValue(InvoiceICLogic.NAZWA_PROJEKTU_FIELD_CN));
        dumper.addText(centraKosztowKodText.toString());
        dumper.addText(centraKosztowCommentText.toString());
        dumper.addText(centraKosztowOpisText.toString());
    
        dumper.dumpLine();
    }

	/**
	 * Inicjowanie dumpertow
	 * @throws Exception
	 */
	protected void initDumpers() throws Exception
	{
		dumper = new UniversalTableDumper();
		CsvDumper csvDumper = new CsvDumper();
		File csvFile = new File(this.getDestination(), "raport_"+DateUtils.formatJsDate(new java.util.Date())+".csv");			
		csvDumper.openFile(csvFile);
		dumper.addDumper(csvDumper);
		
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "raport_"+DateUtils.formatJsDate(new java.util.Date())+".xls");	
		poiDumper.openFile(xlsFile);
		dumper.addDumper(poiDumper);
	}
	
	/**
	 * Zrzut naglowka
	 * @throws IOException
	 */
	protected void dumpHeader() throws IOException
	{
		dumper.newLine();
		dumper.addText("DATA WYS�ANIA DO ASSECO");
		dumper.addText("ID DS");
		dumper.addText("NR FAKTURY");
		dumper.addText("NR KH");
		dumper.addText("KOR");
		dumper.addText("DATA WYSTAWIENIA");
		dumper.addText("DATA WP�YWU");
		dumper.addText("WARTO�� NETTO");
		dumper.addText("WARTO�� VAT");
		dumper.addText("WARTO�� BRUTTO");
		dumper.addText("WALUTA");
		dumper.addText("LOKALIZACJA");
		dumper.addText("REJESTR");
		dumper.addText("SP.P�AT.");
		dumper.addText("DATA P�ATNO�CI");
		dumper.addText("ZALICZKOBIORCA");
		dumper.addText("NR ZALICZKI");
		dumper.addText("DATA VAT");
		dumper.addText("KOD VAT");
		dumper.addText("VAT WARTO�� NETTO");
		dumper.addText("VAT WARTO�� NETTO W KOSZTY");
		dumper.addText("VAT WARTO�� VAT");
		dumper.addText("VAT WARTO�� VAT W KOSZTY");
		dumper.addText("KOMUNIKATY Z IMPORTU");
		dumper.addText("STATUS FAKTURY");
		dumper.addText("OSOBA DOKONUJ�CA AKCEPTACJI KSI�GOWEJ");
		dumper.addText("Pion");
		dumper.addText("Dzia�");
		dumper.addText("Nazwa kontrahenta");
		dumper.addText("Nazwa umowy");
        dumper.addText("Centrum kosztowe");
		dumper.addText("Komentarz do poyzcji");
		dumper.addText("Opis");
		dumper.dumpLine();
	}
}
