package pl.compan.docusafe.parametrization.ic.reports;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.commons.dbutils.DbUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.ic.InvoiceICLogic;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.CsvDumper;
import pl.compan.docusafe.reports.tools.SimpleSQLHelper;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.LoggerFactory;

public class DocumentAcceptancesReport extends Report{

	String sql = "select doc.ID, doc.CTIME, docAcp.acceptanceCn, docAcp.acceptanceTime from DS_DOCUMENT doc left join DS_DOCUMENT_ACCEPTANCE docAcp " +
				"on (doc.ID = docAcp.document_id) where 1=1 ";//order by doc.ID ";
	
	protected UniversalTableDumper dumper = null;
	
	@Override
	public void doReport() throws Exception {
		LoggerFactory.getLogger("jakub").debug("Start report wykaz akcpetacji");
		PreparedStatement ps = null;
		String dateFrom = null;
		String dateTo = null;

		try {
			super.initParametersAvailableValues();
			
			for(ReportParameter p: params) {
				if (p.getFieldCn().equals("cDateFrom")) {
					dateFrom = p.getValueAsString();
				}
				if (p.getFieldCn().equals("cDateTo")) {
					dateTo = p.getValueAsString();
				}
			}		
			
			initDumpers();
			dumpHeader();

			if (dateFrom != null)
				sql+=" AND doc.CTIME >= ?";
			if (dateTo != null)
				sql+=" AND doc.CTIME <= ?";
			
			ps = DSApi.context().prepareStatement(sql);
			
			SimpleSQLHelper sh = new SimpleSQLHelper();
			if (dateFrom != null) ps.setDate(1, sh.parseDate(dateFrom));
			if (dateTo != null) ps.setDate(2, sh.parseDate(dateTo));
			
			ResultSet rs = ps.executeQuery();
			
			SortedMap<String, Accept> accepts = new TreeMap<String, Accept>();
	        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
	        
			while (rs.next())
			{
				String id = rs.getString("ID");
				Date ctime = rs.getTimestamp("CTIME");
				String acpCn = rs.getString("acceptanceCn");
                //String acpDate = null;
                Date date = rs.getTimestamp("acceptanceTime");
//                if(date != null){
//				    acpDate = formatter.format(date);
//                }

				Accept accept = new Accept();
				accept.ctime = ctime;

                if(acpCn != null){
				    accept.acceptences.put(acpCn, date);
                }
				
				if (accepts.containsKey(id)) {
					accepts.get(id).acceptences.put(acpCn, date);
				}
				else {
					accepts.put(id, accept);
				}									
			}
			rs.close();
			
			Set s = accepts.entrySet();
	        Iterator it=s.iterator();
	        
	        while(it.hasNext()) {
                dumpEntry(it);
	        }
	       	
		}catch(SQLException ex) {
		    log.error(ex.getMessage(), ex);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
			if (dumper!=null) { dumper.closeFileQuietly(); }
		}
		
	}

    private void dumpEntry(Iterator it) throws IOException, EdmException {
        Map.Entry m =(Map.Entry)it.next();

        dumper.newLine();
        long documentId = Long.valueOf(m.getKey().toString());
//        dumper.addText((String)m.getKey());
        dumper.addLong(documentId);
        Accept accept =(Accept)m.getValue();
        dumper.addDate(accept.ctime, "dd-MM-yyyy HH:mm");

        String statusString = "Nowy";

        /**
         * Akcpetacja I - zwykla
         */
        if (accept.acceptences.containsKey("zwykla")) {
            statusString = "Akceptacja zwyk�a";
            dumper.addDate(accept.acceptences.get("zwykla"), "dd-MM-yyyy HH:mm");
            LoggerFactory.getLogger("jakub").debug("zwykla:" + accept.acceptences.get("zwykla"));
        } else {
            dumper.addText("-");
        }
        //Akceptacja II - szefa_dzialu
        if (accept.acceptences.containsKey("szefa_dzialu")) {
            statusString = "Akceptacja szefa dzia�u";
            dumper.addDate(accept.acceptences.get("szefa_dzialu"), "dd-MM-yyyy HH:mm");
        } else {
            dumper.addText("-");
        }
        //Akcpetacja III - szefa_pionu
        if (accept.acceptences.containsKey("szefa_pionu")) {
            statusString = "Akceptacja szefa pionu";
            dumper.addDate(accept.acceptences.get("szefa_pionu"), "dd-MM-yyyy HH:mm");
        } else {
            dumper.addText("-");
        }
        //Akcpetacja IV - product_manager
        if (accept.acceptences.containsKey("product_manager")) {
            statusString = "Akceptacja product manager";
            dumper.addDate(accept.acceptences.get("product_manager"), "dd-MM-yyyy HH:mm");
        } else {
            dumper.addText("-");
        }
        //Akcpetacja V - dyrektora_fin
        if (accept.acceptences.containsKey("dyrektora_fin")) {
            statusString = "Akceptacja dyrektora finansowego";
            dumper.addDate(accept.acceptences.get("dyrektora_fin"), "dd-MM-yyyy HH:mm");
        } else {
            dumper.addText("-");
        }
        //Akcpetacja VI - ksiegowa
        if (accept.acceptences.containsKey("ksiegowa")) {
            statusString = "Akceptacja ksi�gowa";
            dumper.addDate(accept.acceptences.get("ksiegowa"), "dd-MM-yyyy HH:mm");
        } else {
            dumper.addText("-");
        }
        //Akcpetacja VII - kontrola_jakosci
        if (accept.acceptences.containsKey("kontrola_jakosci")) {
            statusString = "Akceptacja kontrola jako�ci";
            dumper.addDate(accept.acceptences.get("kontrola_jakosci"), "dd-MM-yyyy HH:mm");
        } else {
            dumper.addText("-");
        }

        FieldsManager fm = Documents.document(documentId).getFieldsManager();

        dumper.addObject(fm.getDescription(InvoiceICLogic.RODZAJ_FAKTURY_CN));
        dumper.addDate((Date) fm.getKey(InvoiceICLogic.DATA_WYSTAWIENIA_FIELD_CN), "dd-MM-yyyy");
        String userString = findUsersForDocument(documentId);
        dumper.addText(userString);
        dumper.addText(userString.length() > 0 ? statusString : "Zako�czony");

        dumper.dumpLine();
    }

    class Accept {
		Date ctime;
		Map<String, Date> acceptences;
		
		Accept() {
			acceptences = new HashMap<String, Date>();
		}
	}
	/**
	 * Inicjowanie dumpertow
	 * @throws Exception
	 */
	protected void initDumpers() throws Exception
	{
		dumper = new UniversalTableDumper();
		CsvDumper csvDumper = new CsvDumper();
		File csvFile = new File(this.getDestination(), "raport_"+DateUtils.formatJsDate(new java.util.Date())+".csv");			
		csvDumper.openFile(csvFile);
		dumper.addDumper(csvDumper);
		
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "raport_"+DateUtils.formatJsDate(new java.util.Date())+".xls");	
		poiDumper.openFile(xlsFile);
		dumper.addDumper(poiDumper);
	}

    /**
     * Zwraca u�ytkownik�w, kt�rzy maj� dokument na swojej li�cie zada� B#3168
     * @return String opisany w rozszerzeniu
     **/
    private final static String findUsersForDocument(long documentId) throws EdmException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DSApi.context().prepareStatement("SELECT dsw_resource_res_key FROM DSW_TASKLIST WHERE dso_document_id = ?");
            ps.setLong(1, documentId);
            rs = ps.executeQuery();
            StringBuilder sb = new StringBuilder();
            while(rs.next()){
                sb.append(DSUser.findByUsername(rs.getString(1)).asLastnameFirstname())
                    .append(",\r\n");
            }
            if(sb.length() > 0){//je�li s� jacy� u�ytkownicy usu� ostatnie 3 znaki (",\r\n")
                sb.delete(sb.length()-3, sb.length());
            }
            return sb.toString();
        } catch (SQLException ex) {
            throw new EdmSQLException(ex);
        } finally {
            DbUtils.closeQuietly(rs);
            DSApi.context().closeStatement(ps);
        }
    }
	
	/**
	 * Zrzut naglowka
	 * @throws IOException
	 */
	protected void dumpHeader() throws IOException
	{
		dumper.newLine();
		dumper.addText("ID Dokumentu");
		dumper.addText("Data wprowadzenia do systemu");
		dumper.addText("Data akceptacji I (Zwyk�a)");
		dumper.addText("Data akceptacji II (Szef dzia�u)");
		dumper.addText("Data akceptacji III (Szef pionu)");
		dumper.addText("Data akceptacji IV (Product manager)");
		dumper.addText("Data akceptacji V (Dyrektor finansowy)");
		dumper.addText("Data akceptacji VI (Ksi�gowa)");
		dumper.addText("Data akceptacji VII (Kontrola jakosci)");
        dumper.addText("Rodzaj faktury");
        dumper.addText("Data wystawienia faktury");
        dumper.addText("U�ytkownicy");
        dumper.addText("Status (ostatnia wykonana akceptacja)");
		dumper.dumpLine();
	}
}
