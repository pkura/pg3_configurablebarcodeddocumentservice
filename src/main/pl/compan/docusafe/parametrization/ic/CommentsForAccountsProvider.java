package pl.compan.docusafe.parametrization.ic;

import com.google.common.collect.Maps;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dictionary.AccountNumberComment;
import pl.compan.docusafe.web.rest.SelectDataProvider;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Map;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class CommentsForAccountsProvider extends SelectDataProvider{
    @Override
    public Map<String, String> getSelectData(HttpServletRequest req) throws EdmException {
        String accountNumber = req.getParameter("account-number");
        String remarkIdOld = req.getParameter("remarkIdOld");
        if(accountNumber == null){
            return Collections.emptyMap();
        } else {
            //AccountNumber account = AccountNumber.find(accountId);
            Map<String, String> ret = Maps.newLinkedHashMap();
            
            int remarkId = 0;
            if(remarkIdOld!=null && !remarkIdOld.isEmpty()){
            	remarkId = Integer.parseInt(remarkIdOld);
            }
            for(AccountNumberComment anc : AccountNumberComment.findForAccount(accountNumber)){
            	if(anc.isVisible() || remarkId==anc.getId())
            		ret.put(anc.getId().toString(), anc.getName());
            }
            return ret;
        }
    }
}
