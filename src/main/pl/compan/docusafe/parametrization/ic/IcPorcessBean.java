package pl.compan.docusafe.parametrization.ic;

import org.springframework.stereotype.Component;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.DictionaryField;
import pl.compan.docusafe.core.dockinds.field.XmlEnumColumnField;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ParamsManager;

import java.math.BigDecimal;
import java.util.*;

/**
 * User: Tomasz
 * Date: 08.01.14
 * Time: 10:08
 */
@Component("icPorcessBean")
public class IcPorcessBean {

    private static final Logger LOG = LoggerFactory.getLogger(IcPorcessBean.class);
    public static final String ACC_SZEF_DZIALU = "SZEF_DZIALU";
    public static final String ACC_SZEF_PIONU = "SZEF_PIONU";
    public static final String ACC_DYREKTOR_FINANSOWY = "DYREKTOR_FINANSOWY";
    public static final String ACC_KSIEGOWA = "KSIEGOWA";
    public static final String NO_NEED_ACC_L = "noneed";
    private static final String DIC_NAME = "DSG_INVOICE_P_MPK";

    public String getUserEmail(String username) {
        LOG.info("getUserEmail {}",username);
        try {
            DSUser u = DSUser.findByUsername(username);
            return u.getEmail();

        } catch (Exception e) {
            LOG.debug(e.getMessage(),e);
        }
        return "intercars@docusafe.pl";
    }

    public void setAcceptanceStateForMpk(Long docId, List<Long> mpkList, String statusCN) {
        LOG.info("setAcceptanceStateForMpk string {} {} {}",docId, statusCN, mpkList != null ? mpkList.size() : "null");
        try {
            OfficeDocument doc = OfficeDocument.find(docId);
            DocumentKind dk = doc.getDocumentKind();
            DictionaryField f = (DictionaryField) dk.getFieldByCn(DIC_NAME);
            XmlEnumColumnField xf = (XmlEnumColumnField) f.getDictionaryFieldByCn("STATUS");
            int accId = xf.getEnumItemByCn(statusCN).getId();
            for(Long id : mpkList) {
                IcMpkDic entry = IcMpkDic.find(id);
                entry.setStatus(accId);
                entry.update();
            }
        } catch (Exception e) {
            LOG.debug(e.getMessage(),e);
        }
    }

    public String getUserToAccept(Long docId, String CN, List<Long> mpkIds) {
        return getUserToAcceptInt(docId, CN, mpkIds.get(0));
    }

    private String getUserToAcceptInt(Long docId, String CN, Long mpkId) {
        LOG.info("getUserToAccept {} {} {}",docId,CN,mpkId);
        try {
            OfficeDocument doc = OfficeDocument.find(docId);
            DocumentKind dk = doc.getDocumentKind();
            DictionaryField f = (DictionaryField) dk.getFieldByCn(DIC_NAME);
            XmlEnumColumnField xf = (XmlEnumColumnField) f.getDictionaryFieldByCn("STATUS");
            int accId = xf.getEnumItemByCn("ACC_"+CN).getId();
            IcMpkDic el = IcMpkDic.find(mpkId);
            if(el.getStatus() != null && el.getStatus() >= accId) {
                return NO_NEED_ACC_L;
            }
            String r = ParamsManager.getEngine().get("akceptacja.mpk",el.getCentrum().getSymbol(),el.getAccount().getNumber(),el.getKwota().intValue(),CN).getString();
            LOG.info("getUserToAccept(3) returning {}",r);
            return r;

        } catch (Exception e) { LOG.debug(e.getMessage(),e); }
        return "admin";
    }

    public String getUserToAccept(Long docId, String CN) {
        LOG.info("getUserToAccept {} {}",docId,CN);
        try {
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            BigDecimal v = (BigDecimal) fm.getKey("KWOTA");
            String r = ParamsManager.getEngine().get("akceptacja.dokument",v.intValue(),CN).getString();
            LOG.info("getUserToAccept(2) returning {}",r);
            return r;

        } catch (Exception e) {
            LOG.debug(e.getMessage(),e);
        }
        return "admin";
    }

    public List<List<Long>> getListOfMpkForAcceptation(Long docId,String CN) {
        LOG.info("getListOfMpkForAcceptation {} {}",docId, CN);
        List<List<Long>> rList = new ArrayList<List<Long>>();
        Map<String, List<Long>> uList = new HashMap<String, List<Long>>();

        try {
            OfficeDocument document = OfficeDocument.find(docId);
            FieldsManager fm = document.getFieldsManager();
            List<Long> ids = (List<Long>) fm.getKey(DIC_NAME);
            Iterator<Long> i = ids.iterator();
            while(i.hasNext()) {
                Long id = i.next();
                String login = getUserToAcceptInt(docId, CN, id);
                if(uList.containsKey(login)) {
                    uList.get(login).add(id);
                } else {
                    List<Long> l = new ArrayList<Long>();
                    l.add(id);
                    uList.put(login,l);
                }
            }
            uList.remove(NO_NEED_ACC_L);
            for(String s : uList.keySet()) {
                rList.add(uList.get(s));
            }

        } catch(Exception e) {
            LOG.debug(e.getMessage(),e);
        }
        return rList;
    }

    public void logThisShit(String text) {
        LOG.info("logThisShit {}: {}",Thread.currentThread().getName(),text);
    }

    public void setInvoiceNumber(Long docId, String fieldCn) {
        try {
            LOG.info("setInvoiceNumber {} {}",docId,fieldCn);
            Document doc = Document.find(docId);
            Map<String,Object> m = new HashMap<String, Object>();
            String[] n = UUID.randomUUID().toString().split("-");
            Calendar c = Calendar.getInstance();
            c.setTime(doc.getCtime());
            String num = "ZL/"+ c.get(Calendar.YEAR) +"/"+ n[0] +"/"+ n[1];
            m.put(fieldCn,num);
            doc.getDocumentKind().setOnly(docId,m,false);
            LOG.info("setInvoiceNumber nadany {}");
        } catch(Exception e) {
            LOG.debug(e.getMessage(),e);
        }
    }

    public boolean isMoneyInRange(Long docId, String fieldCn, String from, String to) {
        LOG.info("isMoneyInRange {} {} ",docId, fieldCn);
        try {
            Document doc = Document.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            BigDecimal v = (BigDecimal) fm.getKey(fieldCn);
            LOG.info("isMoneyInRange == {}",v.floatValue());
            BigDecimal f = BigDecimal.valueOf(Double.valueOf(from));
            BigDecimal t = BigDecimal.valueOf(Double.valueOf(to));
            LOG.info("f {} t {}",f,t);
            LOG.info("cf {} ct {}",v.compareTo(f),v.compareTo(t));
            if(v.compareTo(f) > v.compareTo(t)){
                LOG.info("< true");
                return true;
            }
            LOG.info("> false");
            return false;
        } catch (Exception e) {
            LOG.debug(e.getMessage(),e);
        }
        LOG.info("false");
        return false;
    }

    public float moneyValue(Long docId, String fieldCn) {
        LOG.info("moneyValue {} {} ",docId, fieldCn);
        try {
            Document doc = Document.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            BigDecimal v = (BigDecimal) fm.getKey(fieldCn);
            LOG.info("moneyValue == {}",v.floatValue());
            return v.floatValue();
        } catch (Exception e) {
            LOG.debug(e.getMessage(),e);
        }
        return 0;
    }

    public void setDocumentStatusByCn(Long docId, String fieldCn, String statusCn) {
        LOG.info("setDocumentStatusByCn {} {} {}",docId, fieldCn, statusCn);
        try {
            Document doc = Document.find(docId);
            int i = doc.getDocumentKind().getFieldByCn(fieldCn).getEnumItemByCn(statusCn).getId();
            setDocumentStatus(docId,fieldCn,i);
        } catch (Exception e) {
            LOG.debug(e.getMessage(),e);
        }
    }

    public void setDocumentStatus(Long docId, String fieldCn, Integer fieldValue) {
        LOG.info("setDocumentStatus {} {} {}",docId, fieldCn, fieldValue);
        try {
            Document doc = Document.find(docId);
            Map<String,Object> m = new HashMap<String, Object>();
            m.put(fieldCn,fieldValue);
            doc.getDocumentKind().setOnly(docId,m,false);
        } catch(Exception e) {
            LOG.debug(e.getMessage(),e);
        }
    }
}