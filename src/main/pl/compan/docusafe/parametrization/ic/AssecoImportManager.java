package pl.compan.docusafe.parametrization.ic;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.*;

import com.google.common.collect.Lists;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.api.DocFacade;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Documents;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.AccountNumberComment;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztow;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.ic.IcDataProcessorStub;
import pl.compan.docusafe.ws.ic.IcDataProcessorStub.DajKhInfo;
import pl.compan.docusafe.ws.ic.IcDataProcessorStub.DajKhInfoResponse;
import pl.compan.docusafe.ws.ic.IcDataProcessorStub.DokumentZakupu;
import pl.compan.docusafe.ws.ic.IcDataProcessorStub.DokumentZakupuMpk;
import pl.compan.docusafe.ws.ic.IcDataProcessorStub.ImportDokZk;
import pl.compan.docusafe.ws.ic.IcDataProcessorStub.ImportDokZkResponse;

/**
 * Klasa AssecoImportManager.java
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 */
public class AssecoImportManager 
{
    private final static Logger LOG = LoggerFactory.getLogger(AssecoImportManager.class);
    private final IcDataProcessorStub service;

    public void test()
	{

	} 
	
	public AssecoImportManager() throws Exception 
	{
        super();

        ConfigurationContext configurationContext =
                ConfigurationContextFactory.createDefaultConfigurationContext();

        MultiThreadedHttpConnectionManager multiThreadedHttpConnectionManager = new MultiThreadedHttpConnectionManager();

        HttpConnectionManagerParams params = new HttpConnectionManagerParams();
        params.setDefaultMaxConnectionsPerHost(20);
        multiThreadedHttpConnectionManager.setParams(params);
        HttpClient httpClient = new HttpClient(multiThreadedHttpConnectionManager);
        configurationContext.setProperty(HTTPConstants.CACHED_HTTP_CLIENT, httpClient);

//		AxisClientConfigurator conf = new AxisClientConfigurator();
        String targetEndpoint = Docusafe.getAdditionProperty("IcDataProcessorStub.targetEndpoint");
        if(StringUtils.isBlank(targetEndpoint)){
            LOG.info("targetEndpoint : {}", "http://10.234.105.12:8080/absws/services/IcDataProcessor.IcDataProcessorHttpSoap12Endpoint/");
            this.service = new IcDataProcessorStub(configurationContext);
        } else {
            LOG.info("targetEndpoint : {}", targetEndpoint);
		    this.service = new IcDataProcessorStub(configurationContext, targetEndpoint);
        }
//		conf.setUpHttpParameters(service, "/IcDataProcessor");  
	}
    
    public void updateDicInvoice(long id) throws EdmException {
        IcDataProcessorStub.DajKhInfo kh = new DajKhInfo();
        DicInvoice dic = DicInvoice.get(id);
        kh.setNip(dic.getNip());

        try {
            DajKhInfoResponse resp  = service.dajKhInfo(kh);
            if(resp.get_return().getSuccess()){
                IcDataProcessorStub.Kontrahent kont = resp.get_return().getKh();
                DSApi.context().begin();
                dic.setMiejscowosc(kont.getAdresMiasto());
               //(kont.getAdresKodPocztowy();
                dic.setUlica(kont.getAdresUlica());
                dic.setName(kont.getNazwaKontrahenta());
                dic.setKod(kont.getNumerKontrahenta());
                dic.setTelefon(kont.getTelefon());
                DSApi.context().session().update(dic);
                DSApi.context().commit();
            } else {
                LOG.error("B��d podczas aktualizacji kontrahenta " + kh.getNip() + " " + resp.get_return().getError());
            }
        } catch (Exception ex) {
            throw new EdmException(ex);
        }
    }

    public void getStatus(long documentId) throws Exception {
        IcDataProcessorStub.DokZkAnswer ret = dajDokZkInfo(documentId);
        if (ret.getSuccess()
                && ret.getSafoId() != Integer.MIN_VALUE //AXIS
                && !StringUtils.isBlank(ret.getAccountDate())) { //dodatkowy warunek ustalony w mailu z 10-01-2011
            WorkflowFactory wf = WorkflowFactory.getInstance();
            String activityId = wf.findManualTask(documentId);
            if (activityId != null) {
                wf.manualFinish(activityId, false);                
            }
            DocFacade docFacade = Documents.document(documentId);
            docFacade.getFields().put(InvoiceICLogic.NUMER_KSIEGOWANIA_FIELD_CN, ret.getSafoId() + "");
            docFacade.getFields().put(InvoiceICLogic.STATUS_FIELD_CN, InvoiceICLogic.STATUS_ZAKSIEGOWANA_ID);
            docFacade.getFields().put(InvoiceICLogic.STATUS_FOR_USER_FIELD_CN, InvoiceICLogic.STATUS_ZAKSIEGOWANA_ID);
            LOG.error("dokument eksportowany : status for document {} = '{}' safoId = {} data = '{}'", documentId, ret.getStatus(), ret.getSafoId(), ret.getAccountDate());
        } else if(ret.getSuccess() && ret.getSafoId() != Integer.MIN_VALUE) {
            LOG.info("dokument {} jeszcze nie przeprocesowany po stronie asseco", documentId);
        } else if(ret.getSuccess()){
            //przypadek wyst�pi tylko w przypadku gdy by� b��d programistyczny
            //i pismo zosta�o zaakceptowane pomimo tego, �e eksport si� nie uda�
            LabelsManager.removeLabelsByName(documentId, InvoiceICLogic.ASSECO_LABEL);
            OfficeDocument doc = OfficeDocument.find(documentId);
            Remark remark = new Remark("Nieudany eksport dokumentu. Wiadomo�� od Asseco : \"" + StringUtils.defaultString(ret.getStatus()) + ' ' + StringUtils.defaultString(ret.getError()) + '"');
            doc.addRemark(remark);
            InvoiceICLogic.getInstance().discardToCn(doc, null, null, "ksiegowa", null);
            LOG.error("nieudany eksport dokumentu {} : status '{}', error : '{}', data:'{}'", documentId, ret.getStatus(), ret.getError(), ret.getAccountDate());
        } else {
            LOG.error("b��d podczas sprawdzania statusu dokumentu {} : status '{}', error : '{}'", documentId, ret.getStatus(), ret.getError());
        }
    }

    public IcDataProcessorStub.DokZkAnswer dajDokZkInfo(long documentId) throws RemoteException {
        IcDataProcessorStub.DajDokZkInfo arg = new IcDataProcessorStub.DajDokZkInfo();
        arg.setDsId(documentId + "");
        return service.dajDokZkInfo(arg).get_return();
    }

    public DajKhInfoResponse DajKhInfo(String nip) throws RemoteException
	{
		DajKhInfo dajK = new DajKhInfo();
		dajK.setNip(nip);
		return service.dajKhInfo(dajK);
	}
    


	public ImportDokZkResponse importDokZk(Document document) throws RemoteException, EdmException
	{
		ImportDokZk importDokZk = new ImportDokZk();

        DokumentZakupu dokZak = new DokumentZakupu();
        setValuesDokZakFromDSDocument(dokZak, document);
		importDokZk.setDokumentZakupu(new DokumentZakupu[]{dokZak});
		return service.importDokZk(importDokZk);
	}

    /**
     * Wype�nienie beana ws dla pojedynczego dokumentu
     * @param dokZak
     * @param doc
     * @throws EdmException
     */
	public void setValuesDokZakFromDSDocument(DokumentZakupu dokZak,Document doc) throws EdmException
	{
        DocFacade df = Documents.document(doc.getId());
		FieldsManager fm = df.getFieldsManager();

		dokZak.setBrutto((BigDecimal)fm.getKey(InvoiceICLogic.KWOTA_BRUTTO_FIELD_CN));

		dokZak.setDataPlatnosci(DateUtils.formatAxisNullableDateTime((Date)fm.getValue(InvoiceICLogic.DATA_PLATNOSCI_FIELD_CN)));
		dokZak.setDataWplywu(DateUtils.formatAxisNullableDateTime((Date)fm.getValue(InvoiceICLogic.DATA_WPLYWU_CN)));
		dokZak.setDataWystawienia(DateUtils.formatAxisNullableDateTime((Date)fm.getValue(InvoiceICLogic.DATA_WYSTAWIENIA_FIELD_CN)));

		List<DokumentZakupuMpk> mpk = new ArrayList<IcDataProcessorStub.DokumentZakupuMpk>();
		List<CentrumKosztowDlaFaktury> ckdf = (List<CentrumKosztowDlaFaktury>) fm.getValue(InvoiceICLogic.CENTRUM_KOSZTOWE_FIELD_CN);
		setValuesMPpkFromCentrumKosztow(ckdf, mpk);
		dokZak.setDokumentZakupuMpk(mpk.toArray(new DokumentZakupuMpk[mpk.size()]));

        List<IcDataProcessorStub.DokumentZakupuVat> vats = vatsFromDocument(df);
        dokZak.setDokumentZakupuVat(vats.toArray(new IcDataProcessorStub.DokumentZakupuVat[vats.size()]));
		DicInvoice dic = (DicInvoice) fm.getValue(InvoiceICLogic.DOSTAWCA_FIELD_CN);

		dokZak.setDostawcaKod(dic.getKod());
		dokZak.setDostawcaMiasto(dic.getMiejscowosc());
		dokZak.setDostawcaNazwa(dic.getName());
		dokZak.setDostawcaNip(dic.getNip());
		dokZak.setDostawcaNumer(dic.getNumerKontrahenta());
		dokZak.setDostawcaUlica(dic.getUlica());
//		dokZak.setDzial("");
//		dokZak.setFakturaSpecjalna((Boolean) (fm.getKey(InvoiceICLogic.FAKTURA_SPECJALNA_CN) == null ? false :fm.getKey(InvoiceICLogic.FAKTURA_SPECJALNA_CN)));
		dokZak.setIdDokumentu(doc.getId().intValue());
//        dokZak.setKomentarz(remarksFromDocument(df, ckdf));
		dokZak.setKorekta(fm.getBoolean(InvoiceICLogic.KOREKTY_FIELD_CN)
                || fm.getBoolean(InvoiceICLogic.CREDIT_NOTY_FIELD_CN)
                || fm.getBoolean(InvoiceICLogic.FAKTURA_SPECJALNA_CN));
		dokZak.setLokalizacja(getLocationCn(fm, ckdf));

        Date dataWplywu = (Date)fm.getKey(InvoiceICLogic.DATA_WPLYWU_CN);
        if(dataWplywu != null){
            Calendar calDataPlatnosci = Calendar.getInstance();
            calDataPlatnosci.setTime(dataWplywu);
            dokZak.setMiesiacKosztowy(calDataPlatnosci.get(Calendar.MONTH) + 1);
            dokZak.setRokKosztowy(calDataPlatnosci.get(Calendar.YEAR));
        }

//		dokZak.setNazwaProjektu(""+fm.getValue(InvoiceICLogic.NAZWA_PROJEKTU_FIELD_CN));
		dokZak.setNetto((BigDecimal)fm.getKey(InvoiceICLogic.KWOTA_NETTO_FIELD_CN));
		dokZak.setNumerFaktury(""+fm.getValue(InvoiceICLogic.NUMER_FAKTURY_FIELD_CN));
//		dokZak.setNumerNabycia(""+fm.getValue(InvoiceICLogic.NUMER_NABYCIA_FIELD_CN));
//		dokZak.setNumerPz(""+fm.getValue(InvoiceICLogic.NUMER_PZ_FIELD_CN));
//		dokZak.setNumerRachunkuBankowego(""+fm.getValue(InvoiceICLogic.NUMER_RACHUNKU_BANKOWEGO_CN));
		dokZak.setNumerSad(ObjectUtils.toString(fm.getValue("NUMER_SAD"),null));
        dokZak.setNumerZaliczki(advanceNumberFromDocument(df));
        dokZak.setNumerRozliczenia(advanceInvoiceNumberFromDocument(df));//?Numer faktury zaliczkowej? import do pola w Safo zakup ?numer w�asny rozliczenia?.
//		dokZak.setPion(fm.getEnumItemCn(InvoiceICLogic.PION_CN));
		dokZak.setRejestr(fm.getEnumItemCn(InvoiceICLogic.REJESTR_FIELD_CN));
//		dokZak.setRodzajFaktury(fm.getEnumItemCn(InvoiceICLogic.RODZAJ_FAKTURY_CN));

        Date vatDate = (Date)fm.getKey(InvoiceICLogic.DATA_VAT);
        if(vatDate != null){
            Calendar calVatDate = Calendar.getInstance();
            calVatDate.setTime(vatDate);
            dokZak.setMiesiacVat(calVatDate.get(Calendar.MONTH) + 1);
            dokZak.setRokVat(calVatDate.get(Calendar.YEAR));
        }

        dokZak.setSposobPlatnosci(paymentMethodFromDocument(df));
//		dokZak.setTypFaktury("BRAK DANYCH");
		dokZak.setVat((BigDecimal)fm.getKey(InvoiceICLogic.VAT_CN));
		dokZak.setWaluta(fm.getEnumItemCn(InvoiceICLogic.WALUTA_FIELD_CN));
        dokZak.setZaliczkobiorca(advanceTakerFromDocument(df));
	}

    private String advanceInvoiceNumberFromDocument(DocFacade doc) throws EdmException {
        return doc.getFields().get(InvoiceICLogic.ADVANCE_INVOICE_NUMBER_FIELD_CN, String.class);
    }

    private String getLocationCn(FieldsManager fm, List<CentrumKosztowDlaFaktury> ckdfs) throws EdmException {
        String ret = fm.getEnumItemCn(InvoiceICLogic.LOKALIZACJA_CN);
        if(ret == null){
            //TODO okre�li� z kt�rego centrum bra� cn lokalizacji
            ret = ckdfs.get(0).getLocationString("");
        }
        return  ret;
    }

    private String advanceTakerFromDocument(DocFacade doc) throws EdmException {
        EnumItem advanceTaker = doc.getFields().get(InvoiceICLogic.ADVANCE_TAKER_FIELD_CN, EnumItem.class);

        return advanceTaker == null ? null : advanceTaker.getCn();
    }

    private String paymentMethodFromDocument(DocFacade doc) throws EdmException {
        return doc.getFields().get(InvoiceICLogic.PAYMENT_METHOD_FIELD_CN, EnumItem.class).getCn();
    }

    private String advanceNumberFromDocument(DocFacade doc) throws EdmException {
        return doc.getFields().get(InvoiceICLogic.ADVANCE_NUMBER_FIELD_CN, String.class);
    }

//    private String remarksFromDocument(DocFacade doc, List<CentrumKosztowDlaFaktury> ckdfs) throws EdmException {
//        StringBuilder sb = new StringBuilder();
//        for(CentrumKosztowDlaFaktury ckdf: ckdfs){
//            if(ckdf.getRemarkId() != null){
//                sb.append(AccountNumberComment.find(ckdf.getRemarkId()).getName()).append(" ");
//            }
//        }
//        return sb.toString();
//    }

    /**
     * Tworzenie beanow dla stawek vat na podstawie pojedynczego dokumentu
     * @param doc
     * @return
     * @throws EdmException
     */
    private List<IcDataProcessorStub.DokumentZakupuVat> vatsFromDocument(DocFacade doc) throws EdmException {
        List<IcDataProcessorStub.DokumentZakupuVat> ret = Lists.newArrayList();
        Iterable<VatEntry> vats =  (Iterable<VatEntry>) doc.getFields().get(InvoiceICLogic.VATS_FIELD_CN, Iterable.class);
        if(vats != null){
            for(VatEntry ve: vats){
                IcDataProcessorStub.DokumentZakupuVat vat = new IcDataProcessorStub.DokumentZakupuVat();
                vat.setVat(BigDecimal.valueOf(ve.getVat()));
                vat.setWartoscNetto(ve.getNettoAmount());
                vat.setWartoscVat(ve.getVatAmount());
                vat.setWartoscNettoKoszty(ve.getCostsNettoAmount());
                vat.setWartoscVatKoszty(ve.getCostsVatAmount());
                vat.setInwestycja(ve.isInvestment());
                vat.setKodVat(ve.getVatSymbol());
                ret.add(vat);
            }
        }
        return ret;
    }

    /**
     * Tworzenie beanow dla centrum kosztowego na podstawie listy mpk�w
     * @param ckdf
     * @param mpk
     */
    public void setValuesMPpkFromCentrumKosztow(List<CentrumKosztowDlaFaktury> ckdf,List<DokumentZakupuMpk> mpk) throws EdmException {
		for (CentrumKosztowDlaFaktury centrumKosztowDlaFaktury : ckdf) {
			DokumentZakupuMpk dzmpk = new DokumentZakupuMpk();
			dzmpk.setKodMpk(CentrumKosztow.find(centrumKosztowDlaFaktury.getCentrumId()).getSymbol2());
			dzmpk.setKwotaNetto(centrumKosztowDlaFaktury.getRealAmount());
			dzmpk.setNumerKonta(IcUtils.buildAccountNumberCode(centrumKosztowDlaFaktury));
            if(centrumKosztowDlaFaktury.getRemarkId() != null){
                dzmpk.setKomentarz(AccountNumberComment.find(centrumKosztowDlaFaktury.getRemarkId()).getName());
            } else {
                dzmpk.setKomentarz(centrumKosztowDlaFaktury.getDescriptionAmount());
            }
			mpk.add(dzmpk);
		}
	}
}