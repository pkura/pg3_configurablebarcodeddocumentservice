package pl.compan.docusafe.parametrization.ic;

import com.google.common.collect.Maps;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.crm.Contact;
import pl.compan.docusafe.core.dockinds.dictionary.AbstractDocumentKindDictionary;
import pl.compan.docusafe.core.dockinds.dictionary.DocumentKindDictionary;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;

/**
 * Klasa zawieraj�ca informacje o stawce VAT i cz�ci kwoty, kt�ra jest obj�ta t� stawk� VAT
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
@Entity
@Table(name="dsg_vat_entry")
public class VatEntry extends AbstractDocumentKindDictionary {//extends AbstractDocumentKindDictionary{
    private final static Logger LOG = LoggerFactory.getLogger(VatEntry.class);
    private final static Map<String, String> attributes;
    static {
        Map<String, String> map = Maps.newLinkedHashMap();
//        map.put("investment","Inwestycja");
        map.put("vatSymbol","Symbol VAT");
        map.put("vat","Stawka VAT [%]");
        map.put("vatAmount","Kwota VAT");
        map.put("nettoAmount","Kwota netto");
        map.put("costsVatAmount", "Kwota VAT koszty");
        map.put("costsNettoAmount", "Kwota netto koszty");
        attributes = Collections.unmodifiableMap(map);
    }

    @Id
    @SequenceGenerator(name = "dsg_vat_entry_seq", sequenceName = "dsg_vat_entry_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "dsg_vat_entry_seq")
    Long id;

    @Column(name = "vat_symbol", length = 5, nullable = false)
    private String vatSymbol = "A";

    /** Stawka VAT np 0,7,22 */
    @Column(name = "vat", nullable = false)
    int vat = 7;

    @Column(name = "investment", nullable = false)
    boolean investment = false;

    @Column(name = "vat_amount", nullable = false)
    BigDecimal vatAmount = BigDecimal.ZERO;
    @Column(name = "netto_amount", nullable = false)
    BigDecimal nettoAmount = BigDecimal.ZERO;
    @Column(name = "costs_vat_amount", nullable = false)
    BigDecimal costsVatAmount = BigDecimal.ZERO;
    @Column(name = "costs_netto_amount", nullable = false)
    BigDecimal costsNettoAmount = BigDecimal.ZERO;


    public static VatEntry get(long id) throws EdmException {
        return (VatEntry) DSApi.context().session().get(VatEntry.class, id);
    }

    /***********************
     *   DICTIONARY IMPL
     ***********************/

    @Override
    public void create() throws EdmException {
        DSApi.context().session().save(this);
    }

    @Override
    public void delete() throws EdmException {
        DSApi.context().session().delete(this);
    }

    @Override
    public String getDictionaryDescription() {
        return "Stawka VAT";
    }

    @Override
    public VatEntry find(Long key) throws EdmException {
        return (VatEntry) DSApi.context().session().get(VatEntry.class, key);
    }

    @Override
    public String dictionaryAccept() {
        return "";
    }

    @Override
    public Map<String, String> dictionaryAttributes() {
        return attributes;
    }

    @Override
    public String dictionaryAction() {
        return "/office/common/vat-entry.action";
    }

    @Override
    public Map<String, String> popUpDictionaryAttributes() {
        return attributes;
    }

    /************************
     *      GET & SET
     ************************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getCostsNettoAmount() {
        return costsNettoAmount;
    }

    public void setCostsNettoAmount(BigDecimal costsNettoAmount) {
        this.costsNettoAmount = costsNettoAmount;
    }

    public BigDecimal getCostsVatAmount() {
        return costsVatAmount;
    }

    public void setCostsVatAmount(BigDecimal costsVatAmount) {
        this.costsVatAmount = costsVatAmount;
    }

    public boolean isInvestment() {
        return investment;
    }

    public void setInvestment(boolean investment) {
        this.investment = investment;
    }

    public BigDecimal getNettoAmount() {
        return nettoAmount;
    }

    public void setNettoAmount(BigDecimal nettoAmount) {
        this.nettoAmount = nettoAmount;
    }

    public int getVat() {
        return vat;
    }

    public void setVat(int vat) {
        this.vat = vat;
    }

    public BigDecimal getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(BigDecimal vatAmount) {
        this.vatAmount = vatAmount;
    }

    public String getVatSymbol() {
        return vatSymbol;
    }

    public void setVatSymbol(String vatSymbol) {
        this.vatSymbol = vatSymbol;
    }

    public String getVatRateString(){
        return vatSymbol + "|" + vat;
    }

    public void setVatRateString(String string){
//        LOG.info("setVatRate = {} {}", vatSymbol, vat);
        String[] params = string.split("\\|");
        vatSymbol = params[0];
        vat = Integer.valueOf(params[1]);
        LOG.info("setVatRate = {} {}", vatSymbol, vat);
    }

}
