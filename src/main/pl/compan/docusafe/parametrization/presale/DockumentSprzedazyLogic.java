package pl.compan.docusafe.parametrization.presale;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.crm.Contractor;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.workflow.TaskListMarkerManager;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.parametrization.ilpol.DlApplicationDictionary;
import pl.compan.docusafe.parametrization.ilpol.DlContractDictionary;
import pl.compan.docusafe.parametrization.ilpol.LeasingObjectType;
import pl.compan.docusafe.util.DateUtils;


public class DockumentSprzedazyLogic extends AbstractDocumentLogic {

	
	private static DockumentSprzedazyLogic instance;

    public static synchronized DockumentSprzedazyLogic getInstance()
    {
        if (instance == null)
            instance = new DockumentSprzedazyLogic();
        return instance;
    }
	
	
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {

		if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");
        
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		Folder folder = Folder.getRootFolder();
    	folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
    	
    	folder = folder.createSubfolderIfNotPresent(DateUtils.formatYear_Month(document.getCtime()));
    	
		document.setFolder(folder);
	}
	
    public void onLoadData(FieldsManager fm) throws EdmException
    {
    		Map<String,Object> values = new HashMap<String, Object>();
    		values.put("NR_SEG", getNumerSeg());
    		values.put("NR_W_SEG", getNumerWseg()+1);
    		fm.reloadValues(values);
    }
    public Integer getNumerSeg()
    {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
		List<Integer> l = new ArrayList<Integer>();
		l.add(1);
    	try
    	{
    		ps = DSApi.context().prepareStatement("select MAX(FK_SPR_numer_segregatora),MAX(WZ_numer_segregatora)" +
    				",MAX(FK_Z_OSnumer_segregatora), MAX(FK_KO_numer_segregatora) from dsg_docspr;");
			rs = ps.executeQuery();
			rs.next();
			l.add(rs.getInt(1));
			l.add(rs.getInt(2));
			l.add(rs.getInt(3));
			l.add(rs.getInt(4));
			ps.close();
    	}
    	catch (Exception e)
    	{
			e.printStackTrace();
		}
    	finally
    	{
    		DbUtils.closeQuietly(ps);
    	}
    	return Collections.max(l);
    }
    
    public Integer getNumerWseg()
    {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
		List<Integer> l = new ArrayList<Integer>();
		l.add(1);
    	try
    	{
    		ps = DSApi.context().prepareStatement("select MAX(FK_SPR_numer_w_segregatorze),MAX(WZ_numer_w_segregatorze)" +
    				",MAX(FK_Z_OSnumer_w_segregatorze), MAX(FK_KO_numer_w_segregatorze) from dsg_docspr;");
			rs = ps.executeQuery();
			rs.next();
			l.add(rs.getInt(1));
			l.add(rs.getInt(2));
			l.add(rs.getInt(3));
			l.add(rs.getInt(4));
			ps.close();
    	}
    	catch (Exception e)
    	{
			e.printStackTrace();
		}
    	finally
    	{
    		DbUtils.closeQuietly(ps);
    	}
    	return Collections.max(l);
    }

	public void documentPermissions(Document document) throws EdmException {
		 if (document.getDocumentKind() == null)
	            throw new NullPointerException("document.getDocumentKind()");
		 
		 DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "*", ObjectPermission.ANY);
	}

	@Override
	public boolean searchCheckPermissions(Map<String, Object> values) {
		return false;
	}

	
	public boolean canReadDocumentDictionaries() throws EdmException
	{
		return true;
	}
	
	 public TaskListParams getTaskListParams(DocumentKind dockind, long id)throws EdmException
	{
    	TaskListParams params = new TaskListParams();
    	try
    	{
			FieldsManager fm = dockind.getFieldsManager(id);
			if(fm.getKey("WZ_DATA_WYSLANIA") != null )
			{
				params.setDocumentDate((Date) fm.getValue("WZ_DATA_WYSLANIA"));
			}
			else if(fm.getKey("FK_Z_OSDATA_WYSLANIA") != null )
			{
				params.setDocumentDate((Date) fm.getValue("FK_Z_OSDATA_WYSLANIA"));
			}
			else if(fm.getKey("FK_KO_DATA_WYSLANIA") != null )
			{
				params.setDocumentDate((Date) fm.getValue("FK_KO_DATA_WYSLANIA"));
			}
			else if(fm.getKey("FK_SPR_DATA_WYSLANIA") != null )
			{
				params.setDocumentDate((Date) fm.getValue("FK_SPR_DATA_WYSLANIA"));
			}	
    	}
    	catch (Exception e) 
    	{
			e.printStackTrace();
		}
		return params;
	}
}
