package pl.compan.docusafe.parametrization.presale;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.dictionary.AbstractDocumentKindDictionary;
import pl.compan.docusafe.parametrization.ilpol.DLBinder;
import pl.compan.docusafe.parametrization.ilpol.DlApplicationDictionary;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

public class ZaliczkaDictionary extends AbstractDocumentKindDictionary
{
	private static final Logger log = LoggerFactory.getLogger(ZaliczkaDictionary.class);
	StringManager sm = GlobalPreferences.loadPropertiesFile(ZaliczkaDictionary.class.getPackage().getName(), null);

	private Long id;
	private Double amount;
	private String numerRachunku;
	private String bank;

	private static ZaliczkaDictionary instance;
	public static synchronized ZaliczkaDictionary getInstance()
	{
		if (instance == null)
			instance = new ZaliczkaDictionary();
		return instance;
	}

	public ZaliczkaDictionary()
	{
	}
	
    public String getDictionaryDescription()
    {
        return "brak";
    }
    
    public String dictionaryAccept()
    {
        return "";              
    }
	
	public String dictionaryAction()
	{
		return "/office/common/zaliczka.action";
	}

	public static List<ZaliczkaDictionary> list(Long contractorId) throws EdmException
	{
		return null;
	}
	
	public Map<String, String> dictionaryAttributes()
	{
		StringManager smL = GlobalPreferences.loadPropertiesFile(DlApplicationDictionary.class.getPackage().getName(), null);
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("amount", "Kwota");
		map.put("bank", "Bank");
		map.put("numerRachunku", "Numer Rachunku");
		
		return map;
	}

	public ZaliczkaDictionary find(Long id) throws EdmException
	{
		ZaliczkaDictionary inst = DSApi.getObject(ZaliczkaDictionary.class, id);
		if (inst == null)
			throw new EdmException("Nie znaleziono zaliczki " + id);
		else
			return inst;
	}
	
	/**
	 * Zapisuje strone w sesji hibernate
	 * 
	 * @throws EdmException
	 */
	public void create() throws EdmException
	{
		try
		{
			DSApi.context().session().save(this);
			DSApi.context().session().flush();
		}
		catch (HibernateException e)
		{
			log.error("Blad dodania zaliczki. " + e.getMessage());
			throw new EdmException(sm.getString("Blad dodania pozycji zamówienia") + e.getMessage());
		}
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Double getAmount()
	{
		return amount;
	}

	public void setAmount(Double amount)
	{
		this.amount = amount;
	}

	public void setNumerRachunku(String numerRachunku)
	{
		this.numerRachunku = numerRachunku;
	}

	public String getNumerRachunku()
	{
		return numerRachunku;
	}

	public void setBank(String bank)
	{
		this.bank = bank;
	}

	public String getBank()
	{
		return bank;
	}

	@Override
	public void delete() throws EdmException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Map<String, String> popUpDictionaryAttributes() {
		// TODO Auto-generated method stub
		return null;
	}


}
