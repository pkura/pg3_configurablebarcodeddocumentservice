package pl.compan.docusafe.parametrization.presale;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dictionary.AccountNumber;
import pl.compan.docusafe.core.dockinds.dictionary.KontoKsiegowe;

public class ProductDictionary
{
	private Long id;
	private String name;
	private String cn;
	//wykozystamy konto ksiegowe poniewaz tutaj pasuje do logoiki
	private AccountNumber productGroup;
	private Long contractorId;
	private Double price;

	public static List<ProductDictionary> findByContractor(Long contractorId) throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(ProductDictionary.class);
    	criteria.add(Restrictions.eq("contractorId", contractorId));
    	return criteria.list();
	}
	public static ProductDictionary find(Long id) throws EdmException
	{
		return DSApi.getObject(ProductDictionary.class, id);
	}
	
	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getCn()
	{
		return cn;
	}

	public void setCn(String cn)
	{
		this.cn = cn;
	}

	public AccountNumber getProductGroup()
	{
		return productGroup;
	}

	public void setProductGroup(AccountNumber productGroup)
	{
		this.productGroup = productGroup;
	}

	public void setContractorId(Long contractorId)
	{
		this.contractorId = contractorId;
	}

	public Long getContractorId()
	{
		return contractorId;
	}

	public void setPrice(Double price)
	{
		this.price = price;
	}

	public Double getPrice()
	{
		return price;
	}
}
