package pl.compan.docusafe.parametrization.presale;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.dictionary.AbstractDocumentKindDictionary;
import pl.compan.docusafe.core.dockinds.dictionary.DpStrona;
import pl.compan.docusafe.parametrization.ilpol.DlApplicationDictionary;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

public class ItemOrderDictionary extends AbstractDocumentKindDictionary
{
	private static final Logger log = LoggerFactory.getLogger(ItemOrderDictionary.class);
	StringManager sm = GlobalPreferences.loadPropertiesFile(ItemOrderDictionary.class.getPackage().getName(), null);

	private Long id;
	private Integer accountId;
	private Double volume;
	private Double amount;
	private String description;
	private static ItemOrderDictionary instance;
	public static synchronized ItemOrderDictionary getInstance()
	{
		if (instance == null)
			instance = new ItemOrderDictionary();
		return instance;
	}
	public ItemOrderDictionary()
	{
	}
	
    public String getDictionaryDescription()
    {
        return description +" /ilo��. "+ (volume != null ? volume : "0")+" /cena "+(amount != null? amount: " 0 ") +" /warto��: "+(amount != null &&  volume != null? amount*volume : " 0 ");
    }
    
    public String dictionaryAccept()
    {
        return "";              
    }
    
    public Double getWartosc()
    {
    	return amount != null &&  volume != null ? amount*volume : 0.0 ;
    }
	
	public String dictionaryAction()
	{
		return "/office/common/item-order.action?dostawca='+document.getElementById('dockind_DOSTAWCA').value+'";
	}

	public static List<ItemOrderDictionary> list(Long contractorId) throws EdmException
	{
		return null;
	}
	
	public Map<String, String> dictionaryAttributes()
	{
		StringManager smL = GlobalPreferences.loadPropertiesFile(DlApplicationDictionary.class.getPackage().getName(), null);
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("description", smL.getString("Opis"));
		map.put("volume", smL.getString("Ilosc"));
		map.put("amount", smL.getString("Cena"));
		return map;
	}

	public ItemOrderDictionary find(Long id) throws EdmException
	{
		ItemOrderDictionary inst = DSApi.getObject(ItemOrderDictionary.class, id);
		if (inst == null)
			throw new EdmException("Nie znaleziono pozycji zam�wienia " + id);
		else
			return inst;
	}
	
	/**
	 * Zapisuje strone w sesji hibernate
	 * 
	 * @throws EdmException
	 */
	public void create() throws EdmException
	{
		try
		{
			DSApi.context().session().save(this);
			DSApi.context().session().flush();
		}
		catch (HibernateException e)
		{
			log.error("Blad dodania pozycji zam�wienia. " + e.getMessage());
			throw new EdmException(sm.getString("Blad dodania pozycji zam�wienia") + e.getMessage());
		}
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getDescription()
	{
		return description;
	}

	public Double getVolume()
	{
		return volume;
	}

	public void setVolume(Double volume)
	{
		this.volume = volume;
	}

	public Double getAmount()
	{
		return amount;
	}

	public void setAmount(Double amount)
	{
		this.amount = amount;
	}

	public void setAccountId(Integer productId)
	{
		this.accountId = productId;
	}

	public Integer getAccountId()
	{
		return accountId;
	}

	@Override
	public void delete() throws EdmException {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Map<String, String> popUpDictionaryAttributes() {
		// TODO Auto-generated method stub
		return null;
	}
}
