package pl.compan.docusafe.parametrization.presale;

import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.util.DateUtils;

@Deprecated
public class BankLogic extends AbstractDocumentLogic {

	public static final String GRUPA_CN = "GRUPA";
	
	private static BankLogic instance;

    public static synchronized BankLogic getInstance()
    {
        if (instance == null)
            instance = new BankLogic();
        return instance;
    }
	
	
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {

		if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");
        
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		Folder folder = Folder.getRootFolder();
    	folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
    	
    	folder = folder.createSubfolderIfNotPresent(DateUtils.formatYear_Month(document.getCtime()));
    	
    	folder = folder.createSubfolderIfNotPresent((String) fm.getValue(BankLogic.GRUPA_CN));
    	
		document.setFolder(folder);
	}


	public void documentPermissions(Document document) throws EdmException {
		 if (document.getDocumentKind() == null)
	            throw new NullPointerException("document.getDocumentKind()");
		 
		 DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "*", ObjectPermission.ANY);
	}


	@Override
	public boolean searchCheckPermissions(Map<String, Object> values) {
		return false;
	}

	
	public boolean canReadDocumentDictionaries() throws EdmException
	{
		return true;
	}

}
