package pl.compan.docusafe.parametrization.presale;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.dictionary.AbstractDocumentKindDictionary;
import pl.compan.docusafe.core.dockinds.dictionary.DocumentKindDictionary;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

public class InvoiceZamowienieDictionary extends AbstractDocumentKindDictionary 
{
	private static final Logger log = LoggerFactory.getLogger(InvoiceZamowienieDictionary.class);
	
	private Long documentId;
	private String numerUmowy;

    @Override
    public Long getId() {
        return documentId;
    }

    @Override
	public void create() throws EdmException 
	{
	}

	@Override
	public void delete() throws EdmException {
	}

	@Override
	public String getDictionaryDescription() 
	{
		return ""+numerUmowy;
	}
	
	public String dictionaryAction()
	{
		return "/repository/edit-dockind-document-popup.action?popup=true";
	}
	
	public  String dictionaryAccept()
	{
		return "";
	}
	
	public List<String> additionalSearchParam()
	{
		List<String> list = new ArrayList<String>();
		list.add("document.getElementById('dockind_DOSTAWCAprettyNip').value");
		return list;
	}
	

	public Map<String, String> dictionaryAttributes()
	{
		StringManager smL = GlobalPreferences.loadPropertiesFile(InvoiceZamowienieDictionary.class.getPackage().getName(), null);
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("numerUmowy", "");
		return map;
	}

	@Override
	public DocumentKindDictionary find(Long key) throws EdmException 
	{
		if(key == null)
			return null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		documentId = key;
		try
		{
			ps =DSApi.context().prepareStatement("select TITLE from DS_document where id = ? ");
			ps.setLong(1, key);
			rs = ps.executeQuery();
			if(rs.next())
				numerUmowy = rs.getString(1);
		}
		catch (Exception e) 
		{
			log.error(e.getMessage(),e);
			throw new EdmException(e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		return this;
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public String getNumerUmowy() {
		return numerUmowy;
	}

	public void setNumerUmowy(String numerUmowy) {
		this.numerUmowy = numerUmowy;
	}

	public List<Map<String, String>> search(Object[] args) throws EdmException
	{
		if(args == null || args.length < 1)
			return null;
		
		List<Map<String, String>> result = new ArrayList<Map<String,String>>();
		Map<String, String> mapT = new LinkedHashMap<String, String>();
		mapT.put("NR_ZAMOWIENIA", "ID");
		mapT.put("NR_ZAMOWIENIAnumerZamowienia", "Numer zamówienia");
		mapT.put("UMOWAKwota", "Kwota");
		
		result.add(mapT);
		String nazwaUmowy = args[0].toString();
		String kontrahentNIP = null;
		if(args.length > 1)
		{
			kontrahentNIP = args[1].toString();
			kontrahentNIP = kontrahentNIP.replaceAll("-", "");
		}
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			ps =DSApi.context().prepareStatement("select top 10 zam.document_id,zam.nr_zamowienia, zam.kwota from DSG_SANTANDER zam, DF_DICINVOICE dic where zam.nr_zamowienia like ? and zam.dostawca = dic.id and dic.nip = ? ");
			
			ps.setString(1, nazwaUmowy+"%");
			if(kontrahentNIP != null)
				ps.setString(2, kontrahentNIP);
			rs = ps.executeQuery();
			while(rs.next())
			{
				Map<String, String> map = new LinkedHashMap<String, String>();
				map.put("NR_ZAMOWIENIA", getStringNotNull(rs.getString(1)));
				map.put("NR_ZAMOWIENIAnumerZamowienia", getStringNotNull(rs.getString(2)));
				map.put("UMOWAKwota", getStringNotNull(rs.getString(2)));
				result.add(map);
			}
			/**Jesli nie znalazl i byla nazwa kontrahenta to szuka tylko po tytule*/
			if(result.size() < 2 && kontrahentNIP != null)
			{
				return search(new Object[]{args[0]});
			}
		}
		catch (Exception e) 
		{
			log.error(e.getMessage(),e);
			throw new EdmException(e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		return result;
	}
	
	private String getStringNotNull(String value)
	{
		if(value == null || value.equalsIgnoreCase("NULL"))
		{
			return "";
		}
		return value;
	}

	public boolean isAjaxAvailable() 
	{
		return true;
	}

	@Override
	public Map<String, String> popUpDictionaryAttributes() {
		// TODO Auto-generated method stub
		return null;
	}
}
