package pl.compan.docusafe.parametrization.presale;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.dictionary.AbstractDocumentKindDictionary;
import pl.compan.docusafe.core.dockinds.dictionary.DicAegonCommission;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;

/**
 * @author Mariusz Kilja�czyk
 *
 */

public class DJPersonDictionary extends AbstractDocumentKindDictionary
{
    private static final Logger log = LoggerFactory.getLogger(DJPersonDictionary.class);
    
    private Long   id;
    private String idPracownika;
    private String nazwisko;
    private String imie;
    private String pesel;
    private DJSklepDictionary nrsklepu;
    private Boolean pracuje;
    
    private String nrSap;
    private String nrPeoplesoft;
    
    StringManager sm = 
        GlobalPreferences.loadPropertiesFile(DJPersonDictionary.class.getPackage().getName(),null);
	private static DJPersonDictionary instance;
	public static synchronized DJPersonDictionary getInstance()
	{
		if (instance == null)
			instance = new DJPersonDictionary();
		return instance;
	}
    public DJPersonDictionary()
    {
    	
    }
    
    public String dictionaryAction()
    {
        return "/office/common/djPersonDictionary.action";
    }
    public Map<String,String> dictionaryAttributes()
    {
        Map<String,String> map = new LinkedHashMap<String,String>();
        map.put("imie","Imi�");
        map.put("nazwisko", "Nazwisko");
        map.put("idPracownika", "ID Pracownika");
        map.put("pesel", "Pesel");
        map.put("sklep", "Jednostka");
        map.put("nrSap", "Numer SAP");
        map.put("nrPeoplesoft", "Numer Peoplesoft");

        return map;
    }
    
    /**
     * Zwraca stringa zawieraj�cego nazwe funkcji (najlepiej umieszczonej w pliku dockind.js)
     *  kt�ra wykona si� w funkcji accept_cn
     * 
     */
    public String dictionaryAccept()
    {
        return "";              
    }
    
    
    public String getDictionaryDescription()
    {
        return imie +" "+nazwisko;
    } 
    
    /**
     * Zapisuje strone w sesji hibernate
     * @throws EdmException
     */
    public void create() throws EdmException
    {
        try 
        {
            DSApi.context().session().save(this);
            DSApi.context().session().flush();
        }
        catch (HibernateException e) 
        {
            log.error("Blad dodania pracownika. "+e.getMessage());
            throw new EdmException("B��d dodania pracownika");
        }
    }

    public DJPersonDictionary find(Long id) throws EdmException
    {
        StringManager smL = GlobalPreferences.loadPropertiesFile(DJPersonDictionary.class.getPackage().getName(),null);
        DJPersonDictionary inst = DSApi.getObject(DJPersonDictionary.class, id);
        if (inst == null) 
            throw new EdmException("Nie znaleziono pracownika "+id);
        else 
            return inst;
    }

    public void delete() throws EdmException
    {
        try 
        {   
        	DockindQuery dockindQuery = new DockindQuery(0, 10);
        	dockindQuery.setDocumentKind(DocumentKind.findByCn(DocumentLogicLoader.JYSK_KIND));
        	dockindQuery.field(DocumentKind.findByCn(DocumentLogicLoader.JYSK_KIND).getFieldByCn(DJLogic.PRACOWNIK_FIELD_CN), id);
        	SearchResults<Document> searchResults = DocumentKindsManager.search(dockindQuery);
        	if(searchResults.count() > 0 )
        		throw new EdmException("Nie mo�na usun�� pracownika zwi�zanego z dokumentem.");
        	
            DSApi.context().session().delete(this);
            DSApi.context().session().flush();  
        } 
        catch (HibernateException e) 
        {
            log.error("Blad usuniecia pracownika");
            throw new EdmException("B��d usuni�cia pracownika");
        }        
    }
    
    public void save() throws EdmException
    {
        try 
        {            
            DSApi.context().session().save(this);
            DSApi.context().session().flush();  
        } 
        catch (HibernateException e) 
        {
            log.error("Blad zapisu pracownika");
            throw new EdmException("B��d zapisu pracownika");
        }
        
    }

    @SuppressWarnings("unchecked")
    public static SearchResults<? extends DJPersonDictionary> search(QueryForm form)
    {
        try
        {
            Criteria c = DSApi.context().session().createCriteria(DJPersonDictionary.class);
            
            if (form.hasProperty("imie"))
            {
                c.add(Expression.like("imie","%" + form.getProperty("imie") + "%").ignoreCase());
            }
            if (form.hasProperty("nazwisko"))
            {
                c.add(Expression.like("nazwisko","%" + form.getProperty("nazwisko") + "%").ignoreCase());
            }
            if (form.hasProperty("pesel"))
            {
                c.add(Expression.like("pesel", "%" + form.getProperty("pesel") + "%").ignoreCase());
            }
            if (form.hasProperty("nrsklepu"))
            {
                c.add(Expression.eq("nrsklepu",form.getProperty("nrsklepu")));
            }
            if (form.hasProperty("idPracownika"))
            {
                c.add(Expression.eq("idPracownika",form.getProperty("idPracownika")).ignoreCase());
            }
            if (form.hasProperty("pracuje"))
            {
                c.add(Expression.eq("pracuje",form.getProperty("pracuje")));
            }
            if (form.hasProperty("nrSap"))
            {
                c.add(Expression.eq("nrSap",form.getProperty("nrSap")));
            }
            if (form.hasProperty("nrPeoplesoft"))
            {
                c.add(Expression.eq("nrPeoplesoft",form.getProperty("nrPeoplesoft")));
            }
            for (Iterator iter=form.getSortFields().iterator(); iter.hasNext(); )
            {
                QueryForm.SortField field = (QueryForm.SortField) iter.next();
                if (field.isAscending())
                    c.addOrder(Order.asc(field.getName()));
                else
                    c.addOrder(Order.desc(field.getName()));
            }

            List<DJPersonDictionary> list = (List<DJPersonDictionary>) c.list();

            if (list.size() == 0)
                return SearchResultsAdapter.emptyResults(DJPersonDictionary.class);
            else
            {
                int toIndex;
                int fromIndex = form.getOffset() < list.size() ? form.getOffset() : list.size()-1;
                if(form.getLimit()== 0)
                    toIndex = list.size();
                else
                    toIndex = (form.getOffset()+form.getLimit() <= list.size()) ? form.getOffset()+form.getLimit() : list.size();

                return new SearchResultsAdapter<DJPersonDictionary>(list.subList(fromIndex, toIndex), form.getOffset(), list.size(), DJPersonDictionary.class);
            }
        }
        catch (HibernateException e)
        {
            try
            {
                throw new EdmHibernateException(e);
            }
            catch (EdmHibernateException e1)
            {
            	LogFactory.getLog("eprint").debug("", e1);
            }
        }
        catch (EdmException e)
        {
        	LogFactory.getLog("eprint").debug("", e);
        }
        return null;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public String getIdPracownika() {
		return idPracownika;
	}

	public void setIdPracownika(String idPracownika) {
		
		this.idPracownika = idPracownika;
	}

	public void setNrsklepu(DJSklepDictionary nrsklepu) {
		this.nrsklepu = nrsklepu;
	}

	public DJSklepDictionary getNrsklepu() {
		return nrsklepu;
	}
	
	public String getSklep()
	{
		if(nrsklepu != null)
		{
			return nrsklepu.getNumer() + " " +nrsklepu.getMiasto();
		}
		else
		{
			return "";
		}
	}

	public Boolean getPracuje() {
		return pracuje;
	}

	public void setPracuje(Boolean pracuje) {
		this.pracuje = pracuje;
	}

	public String getNrSap() {
		return nrSap;
	}

	public void setNrSap(String nrSap) {
		this.nrSap = nrSap;
	}

	public String getNrPeoplesoft() {
		return nrPeoplesoft;
	}

	public void setNrPeoplesoft(String nrPeoplesoft) {
		this.nrPeoplesoft = nrPeoplesoft;
	}
	@Override
	public Map<String, String> popUpDictionaryAttributes() {
		// TODO Auto-generated method stub
		return null;
	}


}
