package pl.compan.docusafe.parametrization.presale;

import java.util.List;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.util.StringManager;

/**
 * @author Mariusz Kilja�czyk
 *
 */

public class DJSklepDictionary
{
    private static final Logger log = LoggerFactory.getLogger(DJSklepDictionary.class);
    
    private Long   id;
    private Integer numer;
    private String miasto;
    
    
    StringManager sm = 
        GlobalPreferences.loadPropertiesFile(DJSklepDictionary.class.getPackage().getName(),null);
    
    public DJSklepDictionary()
    {
    }
    
    public static List<DJSklepDictionary> list() throws EdmException
    {
    	return Finder.list(DJSklepDictionary.class, "numer");
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNumer() {
		return numer;
	}

	public void setNumer(Integer numer) {
		this.numer = numer;
	}

	public String getMiasto() {
		return miasto;
	}

	public void setMiasto(String miasto) {
		this.miasto = miasto;
	}

	public static DJSklepDictionary find(Long id) throws EdmException
    {
        DJSklepDictionary inst = DSApi.getObject(DJSklepDictionary.class, id);
        if (inst == null) 
            throw new EdmException("Nie znaleziono sklepu "+id);
        else 
            return inst;
    }

}
