package pl.compan.docusafe.parametrization.presale;

import java.util.Collections;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.acceptances.JBPMAcceptanceManager;
import pl.compan.docusafe.core.dockinds.acceptances.LucasAcceptanceManager;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.parametrization.ic.InvoiceICLogic;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;

/**
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class InvoiceLucasLogic extends InvoiceICLogic {

	private final static JBPMAcceptanceManager manager = new LucasAcceptanceManager();

	@Override
	public JBPMAcceptanceManager getAcceptanceManager() {
		return manager;
	}

	@Override
	public void initAcceptanceProcess(OfficeDocument document) throws EdmException {
		getAcceptanceManager().start(document);
	}

	@Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
		if (document.getFieldsManager().getValue(STATUS_FIELD_CN) == null) {
			document.getDocumentKind().setOnly(document.getId(),
					Collections.<String, Object>singletonMap(STATUS_FIELD_CN, 10));
		}

		if (document.getFieldsManager().getValue(JBPM_PROCESS_ID) == null) {
			initAcceptanceProcess((OfficeDocument)document);
		}
	}

	@Override
	public void checkCanFinishProcess(OfficeDocument document) throws AccessDeniedException, EdmException {
		if (!document.getFieldsManager().getBoolean(AKCEPTACJA_FINALNA_FIELD_CN)) {
			throw new EdmException("Proces akceptacji nie zosta� zako�czony");
		}
	}

	@Override
	protected String getBaseGUID(Document document) throws EdmException {
		return DSDivision.ROOT_GUID;
	}
}
