package pl.compan.docusafe.parametrization.presale;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.SearchResultsAdapter;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.dockinds.dictionary.AbstractDocumentKindDictionary;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.StringManager;

public class UmowaDictionary  extends AbstractDocumentKindDictionary
{
	private static final Logger log = LoggerFactory.getLogger(UmowaDictionary.class);
	StringManager sm = GlobalPreferences.loadPropertiesFile(UmowaDictionary.class.getPackage().getName(), null);

	private Long id;
	private Date termin;
	private DicInvoice kontrahent;
	private Attachment zalacznik;
	private Double kwota;


	public UmowaDictionary()
	{
	}
	
    public String getDictionaryDescription()
    {
        return "brak";
    }
    
    public String dictionaryAccept()
    {
        return "";              
    }
	
	public String dictionaryAction()
	{
		return "/office/common/umowa.action";
	}
	
	public Map<String, String> dictionaryAttributes()
	{
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("kwota","Maksymalna kwota zamówienia");
		map.put("termin", "Termin");
		return map;
	}

	public UmowaDictionary find(Long id) throws EdmException
	{
		UmowaDictionary inst = DSApi.getObject(UmowaDictionary.class, id);
		if (inst == null)
			throw new EdmException("Nie znaleziono umowy " + id);
		else
			return inst;
	}
	
	public static SearchResults<? extends UmowaDictionary> search(QueryForm form)
	{
		try
		{
			Criteria c = DSApi.context().session().createCriteria(UmowaDictionary.class);

			if (form.hasProperty("termin"))
			{
				Date date = DateUtils.parseJsDate((String) form.getProperty("termin"));
				c.add(Restrictions.le("termin", date));
			}
			if (form.hasProperty("kontrahent"))
			{
				DicInvoice dic = DicInvoice.getInstance().find((Long) form.getProperty("kontrahent"));
				c.add(Restrictions.eq("kontrahent", dic));
			}
			if (form.hasProperty("kwota"))
			{
				c.add(Restrictions.eq("kwota", form.getProperty("kwota")));
			}

			List<UmowaDictionary> list = (List<UmowaDictionary>) c.list();
			
			if (list.size() == 0)
				return SearchResultsAdapter.emptyResults(UmowaDictionary.class);
			else
			{
				int toIndex;
				int fromIndex = form.getOffset() < list.size() ? form.getOffset() : list.size() - 1;
				if (form.getLimit() == 0)
					toIndex = list.size();
				else
					toIndex = (form.getOffset() + form.getLimit() <= list.size()) ? form.getOffset() + form.getLimit() : list.size();

				return new SearchResultsAdapter<UmowaDictionary>(list.subList(fromIndex, toIndex), form.getOffset(), list.size(),
						UmowaDictionary.class);
			}
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		return null;
	}
	
	/**
	 * Zapisuje strone w sesji hibernate
	 * 
	 * @throws EdmException
	 */
	public void create() throws EdmException
	{
		try
		{
			DSApi.context().session().save(this);
			DSApi.context().session().flush();
		}
		catch (HibernateException e)
		{
			log.error("Blad dodania umowy. " + e.getMessage());
			throw new EdmException(sm.getString("Blad dodania pozycji umowy") + e.getMessage());
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DicInvoice getKontrahent() {
		return kontrahent;
	}

	public void setKontrahent(DicInvoice kontrahent) {
		this.kontrahent = kontrahent;
	}

	public Double getKwota() {
		return kwota;
	}

	public void setKwota(Double kwota) {
		this.kwota = kwota;
	}

	public Date getTermin() {
		return termin;
	}

	public void setTermin(Date termin) {
		this.termin = termin;
	}

	public Attachment getZalacznik() {
		return zalacznik;
	}

	public void setZalacznik(Attachment zalacznik) {
		this.zalacznik = zalacznik;
	}

	@Override
	public void delete() throws EdmException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Map<String, String>> search(Object[] args) throws EdmException {
		// TODO Auto-generated method stub
		return null;
	}

	public List<String> additionalSearchParam() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isAjaxAvailable() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Map<String, String> popUpDictionaryAttributes() {
		// TODO Auto-generated method stub
		return null;
	}

}
