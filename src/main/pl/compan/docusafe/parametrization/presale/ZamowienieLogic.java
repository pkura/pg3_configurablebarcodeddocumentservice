package pl.compan.docusafe.parametrization.presale;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.hibernate.HibernateException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartEvent;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.web.office.common.DocumentArchiveTabAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.lowagie.text.Cell;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Row;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.codec.GifImage;

/**
 * Szykowane pod jakas prezentacje
 * 
 * @author wkutyla
 * 
 */
public class ZamowienieLogic extends AbstractDocumentLogic
{
	private static final StringManager sm = GlobalPreferences.loadPropertiesFile(ZamowienieLogic.class.getPackage().getName(), null);
	private static Logger log = LoggerFactory.getLogger(ZamowienieLogic.class);
	private static ZamowienieLogic instance;

	public static synchronized ZamowienieLogic getInstance()
	{
		if (instance == null)
			instance = new ZamowienieLogic();
		return instance;
	}

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{

		if (document.getDocumentKind() == null)
			throw new NullPointerException("document.getDocumentKind()");

		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());

		folder = folder.createSubfolderIfNotPresent(DateUtils.formatYear_Month(document.getCtime()));

		if (fm.getValue("NR_ZAMOWIENIA") != null)
			folder = folder.createSubfolderIfNotPresent((String) fm.getValue("NR_ZAMOWIENIA"));

		Double kwotaPln = 0.0;
		
	//	values.put("O_UMOWA", false);
		if (fm.getValue("POZYCJA_ZAMOWIENIA") != null)
		{
			Double tmpDouble = 0.0;
			Map<String, Object> values = new HashMap<String, Object>();
			List<ItemOrderDictionary> l = (List<ItemOrderDictionary>) fm.getValue("POZYCJA_ZAMOWIENIA");
			for (ItemOrderDictionary itemOrderDictionary : l)
			{
				tmpDouble += itemOrderDictionary.getWartosc();
			}
			LoggerFactory.getLogger("mariusz").debug("tmpDouble " + tmpDouble);
			
			values.put("KWOTA", tmpDouble);
			Double kurs = null;
			if (fm.getKey("WALUTA") != null)
			{
				LoggerFactory.getLogger("mariusz").debug("Waluta " + fm.getKey("WALUTA"));
				// if(fm.getKey("KURS") != null)
				{
					// kurs = (Double) fm.getKey("KURS");
				}
				// else
				{
					kurs = getKurs((Integer) fm.getKey("WALUTA"));
				}
				LoggerFactory.getLogger("mariusz").debug("kurs " + kurs);
				values.put("KURS", kurs);
			}
			if (tmpDouble != null && tmpDouble > 0 && kurs != null)
			{
				kwotaPln = kurs * tmpDouble;
				values.put("KWOTA_PLN", kwotaPln);
			}
			if (fm.getKey("DOSTAWCA") != null)
			{
				if (kwotaPln > getMaxKwotaDic((Long) fm.getKey("DOSTAWCA")).doubleValue())
				{
					values.put("O_UMOWA", true);
				}
			}
			document.getDocumentKind().setOnly(document.getId(), values);			
		}
		document.setFolder(folder);
	}


	/**
	 * Przerobic na klase i zaciagac przez hibernate
	 * 
	 * @throws EdmException
	 * @throws SQLException
	 */
	private Double getKurs(Integer waluta) throws EdmException
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		Double kurs = 0.0;
		try
		{
			ps = DSApi.context().prepareStatement("select kurs from dsg_kurs where id_waluty = ? ");
			ps.setInt(1, waluta);
			rs = ps.executeQuery();
			if (rs.next())
				kurs = rs.getDouble(1);
		}
		catch (Exception e)
		{
			log.error("", e);
			throw new EdmException(e);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
		return kurs;
	}

	/**
	 * Przerobic na klase i zaciagac przez hibernate
	 * 
	 * @throws EdmException
	 * @throws SQLException
	 */
	public static String getNrRachunku(Integer centrum) throws EdmException
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		Double kurs = 0.0;
		try
		{
			ps = DSApi.context().prepareStatement("select numer_rach from dsg_centrum_to_rachunek where id_centrum = ? ");
			ps.setInt(1, centrum);
			rs = ps.executeQuery();
			if (rs.next())
				return rs.getString(1);
		}
		catch (Exception e)
		{
			log.error("", e);
			throw new EdmException(e);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
		return "";
	}

	/**
	 * Przerobic na klase i zaciagac przez hibernate
	 * 
	 * @throws EdmException
	 * @throws SQLException
	 */
	public static BigDecimal getMaxKwotaDic(Long dicId) throws EdmException
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		Double kurs = 0.0;
		try
		{
			ps = DSApi.context().prepareStatement("select kwota from dsg_dic_to_max where dic_id = ? ");
			ps.setLong(1, dicId);
			rs = ps.executeQuery();
			if (rs.next())
				return new BigDecimal(rs.getDouble(1));
		}
		catch (Exception e)
		{
			log.error("", e);
			throw new EdmException(e);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
		}
		return new BigDecimal(0);
	}

	/**
	 * Przerobic na klase i zaciagac przez hibernate
	 * 
	 * @throws EdmException
	 * @throws SQLException
	 */
	public static void setMaxKwotaDic(Double max, Long dicId) throws EdmException
	{
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		ResultSet rs = null;
		try
		{
			ps = DSApi.context().prepareStatement("select kwota from dsg_dic_to_max where dic_id = ? ");
			ps.setLong(1, dicId);
			rs = ps.executeQuery();
			if (rs.next())
			{
				ps2 = DSApi.context().prepareStatement("update dsg_dic_to_max set kwota = ? where dic_id = ?");
				ps2.setDouble(1, max);
				ps2.setLong(2, dicId);
				ps2.execute();
			}
			else
			{
				ps2 = DSApi.context().prepareStatement("INSERT INTO dsg_dic_to_max(dic_id,kwota)VALUES(?,?)");
				ps2.setLong(1, dicId);
				ps2.setDouble(2, max);
				ps2.execute();
			}
		}
		catch (Exception e)
		{
			log.error("", e);
			throw new EdmException(e);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(ps);
			DbUtils.closeQuietly(ps2);
		}
	}

	public void documentPermissions(Document document) throws EdmException
	{
		if (document.getDocumentKind() == null)
			throw new NullPointerException("document.getDocumentKind()");

		DSApi.context().grant(document, new String[]
		{ ObjectPermission.READ,ObjectPermission.MODIFY, ObjectPermission.READ_ATTACHMENTS,ObjectPermission.MODIFY_ATTACHMENTS,ObjectPermission.DELETE }, "*", ObjectPermission.ANY);

        try 
        {
            DSApi.context().session().flush();
        }
        catch (HibernateException ex) 
        {
            throw new EdmHibernateException(ex);
        }
	}

	@Override
	public boolean searchCheckPermissions(Map<String, Object> values)
	{
		return false;
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("DATA_WYSTAWIENIA", new Date());
		values.put("WALUTA", 40);
		fm.reloadValues(values);
	}

	public void doDockindEvent(DockindButtonAction eventActionSupport, ActionEvent event,Document document, String activity,Map<String, Object> values) throws EdmException
	{
		if ("accept_go".equals(eventActionSupport.getDockindEventValue()))
		{
			acceptGO(eventActionSupport, eventActionSupport.getDockindEventValue(), event, values, document);
		}
		else if ("getPdf".equals(eventActionSupport.getDockindEventValue()))
		{
			try
			{
				createZamowienie(eventActionSupport, eventActionSupport.getDockindEventValue(), event, values, document);
			}
			catch (Exception e) {
				log.error("",e); 
			}
		}
	}

	public void createZamowienie(DockindButtonAction eventActionSupport, String dockingEvent, ActionEvent event, Map<String, Object> values, Document document) throws EdmException, Exception
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		String nrZamowienia = (String) fm.getValue("NR_ZAMOWIENIA");
		String odbiorca = "OBI Centrala Systemowa sp. z o.o.\n" +
				"al. Krakowska 102 \n" +
				"02-180 Warszawa";
		String dostawca = "";
		if(fm.getValue("DOSTAWCA") != null)
		{
			DicInvoice dic = (DicInvoice) fm.getValue("DOSTAWCA");
			dostawca = dic.getDicDescription();
		}
		List<ItemOrderDictionary> items = (List<ItemOrderDictionary>) fm.getValue("POZYCJA_ZAMOWIENIA");
		File f = robi(nrZamowienia, odbiorca, dostawca, items);
		DSApi.context().begin();
		Attachment attachment = new Attachment("Zam�wienie");
		document.createAttachment(attachment);
		attachment.createRevision(f).setOriginalFilename("Zam�wienie");
		DSApi.context().commit();
		event.addActionMessage("Wygenerowano PDF i do��czono do zam�wienia jako za��cznik");
		 
	}

	private static File robi(String nrZamowienia,String odbiorca,String dostawca,List<ItemOrderDictionary> items) throws Exception
	{
		File fontDir = new File(Docusafe.getHome(),"/fonts");
		File arial = new File(fontDir, "arial.ttf");
		BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

		Font font = new Font(baseFont, 8);
		Font fontB = new Font(baseFont, 8,Font.BOLD);

		File temp = File.createTempFile("zamowienie", ".pdf");
		com.lowagie.text.Document pdfDoc = new com.lowagie.text.Document(PageSize.A4, 30, 60, 30, 30);
		PdfWriter writer = PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));
		

		HeaderFooter footer = new HeaderFooter(new Phrase("Zam�wienie ", font), new Phrase("."));
		footer.setAlignment(Element.ALIGN_CENTER);
		pdfDoc.setFooter(footer);
		
		pdfDoc.open();
		PdfContentByte cb = writer.getDirectContent();	
	
		File imageF =  new File(Docusafe.getHome(),"/img/logo_c.gif");
		Image image = new GifImage(imageF.getAbsolutePath()).getImage(1);
		image.scaleToFit(200, 200);
		image.setAbsolutePosition(20, 800);
		cb.addImage(image);
		


		Cell cell;
		Row row;
		Rectangle rect;
		Table table = new Table(10);
		table.setCellsFitPage(true);
		table.setAutoFillEmptyCells(true);
		table.setSpaceInsideCell(1);
		table.setOffset(50);
		table.setTableFitsPage(true);
		rect = new Rectangle(0, 0);

		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

		cell = new Cell();
		cell.setColspan(10);
		cell.setRowspan(2);
		cell.add(new Phrase("Zam�wienie ",new Font(baseFont, 16,Font.BOLD)));
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(10);
		cell.setRowspan(1);
		cell.add(new Phrase(" ",font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(10);
		cell.setRowspan(1);
		cell.add(new Phrase(" ",font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(2);
		cell.setRowspan(1);
		cell.add(new Phrase("  Data :",font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(8);
		cell.setRowspan(1);
		cell.add(new Phrase(formatter.format(new Date()),font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(2);
		cell.setRowspan(1);
		cell.add(new Phrase("  Zam�wienie numer : ",font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(8);
		cell.setRowspan(1);
		cell.add(new Phrase(nrZamowienia,font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(10);
		cell.setRowspan(1);
		cell.add(new Phrase(" ",font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(5);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.add(new Phrase("DOSTAWCA",new Font(baseFont, 10)));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(5);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.add(new Phrase("ODBIORCA",new Font(baseFont, 10)));		
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(5);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.add(new Phrase(odbiorca,font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(5);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.add(new Phrase(dostawca,font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(10);
		cell.setRowspan(1);
		cell.add(new Phrase(" ",font));
		cell.setBorder(0);
		table.addCell(cell);
		cell = new Cell();
		cell.setColspan(10);
		cell.setRowspan(1);
		cell.add(new Phrase(" ",font));
		cell.setBorder(0);
		table.addCell(cell);
		cell = new Cell();
		cell.setColspan(10);
		cell.setRowspan(1);
		cell.add(new Phrase(" ",font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(4);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.add(new Phrase("Produkt",fontB));
		//cell.setBorder(1);
		cell.setBorderColor(Color.BLACK);
		cell.setBorderColorLeft(Color.BLACK);
		//cell.setBorderWidth(2);
		cell.setBorderColorRight(Color.BLACK);
		cell.setBackgroundColor(Color.LIGHT_GRAY);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(2);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.add(new Phrase("Cena",fontB));
		///cell.setBorder(1);
		cell.setBorderColor(Color.BLACK);
		cell.setBorderColorLeft(Color.BLACK);
		//cell.setBorderWidth(2);
		cell.setBorderColorRight(Color.BLACK);
		cell.setBackgroundColor(Color.LIGHT_GRAY);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(2);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.add(new Phrase("Ilo��",fontB));
		//cell.setBorder(1);
		cell.setBorderColor(Color.BLACK);
		cell.setBorderColorLeft(Color.BLACK);
		//cell.setBorderWidth(2);
		cell.setBorderColorRight(Color.BLACK);
		cell.setBackgroundColor(Color.LIGHT_GRAY);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(2);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.add(new Phrase("Warto��",fontB));
		//cell.setBorder(1);
		cell.setBorderColor(Color.BLACK);
		cell.setBorderColorLeft(Color.BLACK);
		//cell.setBorderWidth(2);
		cell.setBorderColorRight(Color.BLACK);
		cell.setBackgroundColor(Color.LIGHT_GRAY);
		table.addCell(cell);
		
		
		Double suma = 0.0;
		for (ItemOrderDictionary it : items)
		{
			cell = new Cell();
			cell.setColspan(4);
			cell.setRowspan(1);
			cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
			cell.add(new Phrase(it.getDescription(),font));
			//cell.setBorder(1);
			table.addCell(cell);
			
			cell = new Cell();
			cell.setColspan(2);
			cell.setRowspan(1);
			cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
			cell.add(new Phrase(it.getAmount().toString(),font));
			//cell.setBorder(1);
			table.addCell(cell);
			
			cell = new Cell();
			cell.setColspan(2);
			cell.setRowspan(1);
			cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
			cell.add(new Phrase(it.getVolume().toString(),font));
			//cell.setBorder(1);
			table.addCell(cell);
			
			cell = new Cell();
			cell.setColspan(2);
			cell.setRowspan(1);
			cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
			cell.add(new Phrase(it.getWartosc().toString(),font));
			//cell.setBorder(1);
			table.addCell(cell);
			
			suma += it.getWartosc();
		}
		
		cell = new Cell();
		cell.setColspan(6);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.add(new Phrase(" ",font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(2);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
		cell.add(new Phrase(" Suma : ",fontB));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(2);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.add(new Phrase(suma.toString(),fontB));
		cell.setBorder(0);
		table.addCell(cell);

		pdfDoc.add(table);
		pdfDoc.close();
		
		return temp; 
	}

    public static void addGifImage(File file, com.lowagie.text.Document pdfDoc, PdfContentByte cb, float width, float height) throws IOException, DocumentException
	{
		Image image = new GifImage(file.getAbsolutePath()).getImage(1);
		image.scaleToFit(width, height);
		image.setAbsolutePosition(20, 20);
		cb.addImage(image);
		pdfDoc.newPage(); 
	} 
    
	public void acceptGO(DockindButtonAction eventActionSupport, String dockingEvent, ActionEvent event, Map<String, Object> values, Document document) throws EdmException
	{
		try
		{
			DSApi.context().begin();
			String activityId = null;
			if (eventActionSupport instanceof DocumentArchiveTabAction)
				activityId = ((DocumentArchiveTabAction) eventActionSupport).getActivity();
			else
			{
				event.addActionMessage("Nie mo�na wykona� tej akcji.");
				return;
			}

			FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
			BigDecimal kwotaPln = (BigDecimal) fm.getKey("KWOTA_PLN");
			UmowaDictionary umowa = (UmowaDictionary) fm.getValue("UMOWA");
			if (fm.getKey("DOSTAWCA") != null)
			{
				if (kwotaPln.doubleValue() > getMaxKwotaDic((Long) fm.getKey("DOSTAWCA")).doubleValue() && umowa == null)
				{
					throw new EdmException("Zam�wienie na kwot� " + fm.getValue("KWOTA_PLN") + " wymaga umowy");
				}
			}

			if (umowa != null &&  umowa.getKwota() != null && kwotaPln.doubleValue() > umowa.getKwota())
				throw new EdmException("Maksymalna kwota zam�wienia dla wybranej umowy wynosi " + umowa.getKwota());

			if (umowa != null && umowa.getTermin() != null && umowa.getTermin().before(new Date()))
				throw new EdmException("Umowa ju� nie obowi�zuj�");

			LoggerFactory.getLogger("mariusz").debug("Akcja " + dockingEvent);
			List<Acceptance> wszystkie = document.getDocumentKind().getAcceptancesDefinition().getLevelAcceptancesAsList();
			//Map<String, AcceptanceCondition> akceptacjeuzytkownika = AcceptanceCondition.findAcceptanceConditionByUserGuids(DSApi.context().getPrincipalName());
			List<DocumentAcceptance> wykonane = DocumentAcceptance.find(document.getId());
			String cn = null;
			String fieldValue = null;
			for (DocumentAcceptance documentAcceptance : wykonane)
			{
				LoggerFactory.getLogger("mariusz").debug("wykonane " + documentAcceptance.getAcceptanceCn());
			}
			
			for (Acceptance q : wszystkie)
			{
				LoggerFactory.getLogger("mariusz").debug("wszystkie " + q.getCn());
				boolean isAcc = false;
				for (DocumentAcceptance da : wykonane)
				{
					if(q.getCn().equals(da.getAcceptanceCn()))
					{
						isAcc = true;
						break;
					}						
				}
				if(!isAcc)
				{
					cn = q.getCn();
					break;
				}
			}
			fieldValue = (String) values.get("mpk");
			if(fieldValue == null)
				throw new EdmException("Nie wybrano MPK");
			LoggerFactory.getLogger("mariusz").debug("CN = "+ cn + " fieldValue "+ fieldValue);
			
			if (!AcceptanceCondition.canCentrumAccept(Integer.parseInt(fieldValue), cn))
				throw new EdmException("Nie mo�esz zaakceptowa� dokumentu");

			DocumentAcceptance acceptance = new DocumentAcceptance();
			acceptance.setAcceptanceCn(cn);
			acceptance.setDocumentId(document.getId());
			acceptance.setUsername(DSApi.context().getPrincipalName());
			acceptance.setAcceptanceTime(new Date());
			Persister.create(acceptance);
			event.addActionMessage("Dokonano akceptacji");

			Map<String, Object> val = new HashMap<String, Object>();
			val.put(cn, DSApi.context().getDSUser().asFirstnameLastname() + " " + DateUtils.formatJsDateTime(new Date()));
			if ("AKCEPTACJA_3".equals(cn))
			{
				val.put("NR_ZAMOWIENIA", Integer.parseInt(DateUtils.formatCommonTime(new Date()).replace(":", "")) + 133);
			}
			document.getDocumentKind().setOnly(document.getId(), val);

			DataMartEvent dme = new DataMartEvent(true, document.getId(), null, DataMartDefs.ACCEPTANCE_GIVE, cn, null, null, null);
			if (document instanceof OfficeDocument)
			{
				String tmp = sm.getString("DokonoanoAkceptacjiDokumentu", acceptance.getAcceptanceCn());
				((OfficeDocument) document).addWorkHistoryEntry(Audit.create("giveAccept", DSApi.context().getPrincipalName(), tmp, acceptance.getAcceptanceCn(), null));
			}
			DSApi.context().commit();
			DSApi.context().begin();
			String nextCn = document.getDocumentKind().getAcceptancesDefinition().getNextCn(cn);
			String division = null;
			LoggerFactory.getLogger("mariusz").debug("NEXT "+ nextCn);
			LoggerFactory.getLogger("mariusz").debug("fieldValue "+fieldValue);
			if (nextCn == null)
			{
				// jak juz wszystkie to do wysylki
				division = "ZAMOWIENIE_WYSYLKA";
			}
			else
			{
				List<AcceptanceCondition> accList = AcceptanceCondition.find(nextCn, fieldValue);
				if (accList == null || accList.size() < 1)
					throw new EdmException("Nie znaleziono definicji kolejnej akceptacji");
				division = accList.get(0).getDivisionGuid();
			}

			WorkflowFactory.simpleAssignment(activityId, division, null, DSApi.context().getPrincipalName(), null, WorkflowFactory.SCOPE_DIVISION, null, event);
			DSApi.context().commit();
			event.skip(DocumentArchiveTabAction.EV_FILL);
			event.setResult("confirm");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("", e);
			DSApi.context()._rollback();
			throw new EdmException(e.getMessage());
		}
	}

}
