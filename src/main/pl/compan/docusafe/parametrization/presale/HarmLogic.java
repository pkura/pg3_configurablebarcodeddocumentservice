package pl.compan.docusafe.parametrization.presale;

import java.io.File;
import java.util.Collections;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.InsuranceParticipant;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.EventActionSupport;

import static pl.compan.docusafe.util.FolderInserter.*;
/**
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class HarmLogic extends AbstractDocumentLogic{
	private static final Logger log = LoggerFactory.getLogger(HarmLogic.class);

	public static enum Fields{
		NUMER_ZGLOSZENIA, ZGLASZAJACY, STATUS
	}

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException{
		FieldsManager fm = document.getFieldsManager();
		String number = (String)fm.getValue(Fields.NUMER_ZGLOSZENIA);
		if (number != null) {
			FolderInserter fi = root().subfolder("Likwidacja szkody AC+OC")
				.subfolders(number.split("/"));

			fi.subfolder("Korespondencja z klientem").insert(document);
			fi.subfolder("Dokumentacja medyczna");
			fi.subfolder("Dokumenty operacyjne");
		}
	}

	public void documentPermissions(Document document) throws EdmException {
		
	}

	public void onStartProcess(OfficeDocument document) {
		FieldsManager fm = document.getFieldsManager();

		try {
			document.getDocumentKind().setOnly(document.getId(),
					Collections.singletonMap(Fields.STATUS.toString(), 10));
			if (fm.getValue(Fields.NUMER_ZGLOSZENIA) == null) {
				document.getDocumentKind().setOnly(document.getId(),
					Collections.singletonMap(Fields.NUMER_ZGLOSZENIA.toString(), generateNumber(fm)));
			}

			archiveActions(document, 9898989);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	private static String generateNumber(FieldsManager fm) throws EdmException{
		return "WAW/" + ((InsuranceParticipant)fm.getValue(Fields.ZGLASZAJACY)).getPesel() + "/" + System.currentTimeMillis();
	}

	@Override
	public void doDockindEvent(DockindButtonAction eventActionSupport, ActionEvent event,Document document, String activity,Map<String, Object> values) throws EdmException {
		try {
			DSApi.context().begin();
			if (eventActionSupport.getDockindEventValue().equals("DO_RZECZOZNAWCY")) {
				log.error("It is working ! event = DO_RZECZOZNAWCY");
				document.getDocumentKind().setOnly(document.getId(),
						Collections.singletonMap(Fields.STATUS.toString(), 20));
				WorkflowFactory.simpleAssignment(WorkflowFactory.findManualTask(document.getId()),
						"RZECZOZNAWCA",
						null,
						DSApi.context().getPrincipalName(),
						null,
						WorkflowFactory.SCOPE_DIVISION,
						"do rzeczoznwcy",
						null);
				event.setResult("task-list");
				Attachment attachment = new Attachment("formularz rzeczoznawcy");
				attachment.setType(TYPE_IN_OFFICE);
				document.addAttachment(attachment);
				DSApi.context().session().save(attachment);
				DSApi.context().session().saveOrUpdate(attachment.createRevision(new File(Docusafe.getHome(),"form.tiff")));
//				document.addAttachment();
			} else if (eventActionSupport.getDockindEventValue().equals("DO_LIKWIDATORA")) {
				log.error("It is working ! event = DO_LIKWIDATORA");
				document.getDocumentKind().setOnly(document.getId(),
						Collections.singletonMap(Fields.STATUS.toString(), 30));
				WorkflowFactory.simpleAssignment(WorkflowFactory.findManualTask(document.getId()),
						"LIKWIDATOR",
						null,
						DSApi.context().getPrincipalName(),
						null,
						WorkflowFactory.SCOPE_DIVISION,
						"do likwidatora",
						null);
				event.setResult("task-list");
			} else if (eventActionSupport.getDockindEventValue().equals("DO_KSIEGOWOSCI")) {
				log.error("It is working ! event = DO_KSIEGOWOSCI");
				document.getDocumentKind().setOnly(document.getId(),
						Collections.singletonMap(Fields.STATUS.toString(), 40));
				WorkflowFactory.simpleAssignment(WorkflowFactory.findManualTask(document.getId()),
						"KSIEGOWA",
						null,
						DSApi.context().getPrincipalName(),
						null,
						WorkflowFactory.SCOPE_DIVISION,
						"do ksi�gowej",
						null);
				event.setResult("task-list");
			}
			TaskSnapshot.updateByDocument(document);
			DSApi.context().commit();
		} catch (Exception e) {
			DSApi.context().setRollbackOnly();
			log.error(e.getMessage(), e);
		}
	}
}
