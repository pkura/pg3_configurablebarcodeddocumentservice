package pl.compan.docusafe.parametrization.presale;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.record.projects.ProjectEntry;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

public class DzkLogic extends AbstractDocumentLogic
{
	private static Logger log = LoggerFactory.getLogger(DzkLogic.class);

	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		Map<String, Object> toReload = Maps.newHashMap();
		for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
		{
			if (f.getDefaultValue() != null)
			{
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
			}
		}
		log.info("toReload = {}", toReload);
		fm.reloadValues(toReload);
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn() + "_DOCUMENT_READ", ObjectPermission.GROUP, "Faktura - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Faktura zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn() + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Faktura - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn() + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Faktura zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn() + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Faktura - usuwanie"));
		this.setUpPermission(document, perms);
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{
		log.info("--- AbstractInvoiceLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn)
	{
		boolean assigned = false;
		
		return assigned;
	}

	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException, EdmException
	{
		
	}

	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		

		return null;
	}

	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		try
		{
			Document doc = Document.find(docId);
			FieldsManager fm = doc.getFieldsManager();
		}
		catch (EdmException e)
		{
			log.error(e.getMessage());
		}
	}

	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();
		try
		{
			FieldsManager fm = dockind.getFieldsManager(id);

			// Ustawienie statusy dokumentu
			params.setStatus(fm.getValue("STATUS") != null ? (String) fm.getValue("STATUS") : null);

			// Nr faktury jako nr dokumentu
			params.setDocumentNumber((fm.getValue("NR_FAKTURY") != null ? (String) fm.getValue("NR_FAKTURY") : null));

			// data wystawienia faktury jako data dokumentu
			params.setDocumentDate((fm.getValue("DATA_WYSTAWIENIA") != null ? (Date) fm.getValue("DATA_WYSTAWIENIA") : null));

		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return params;
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException 
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent("Faktury");
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(fm.getValue("DATA_WYSTAWIENIA")));
		
		if (fm.getEnumItem("RODZAJ_FAKTURY") != null)
			folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getEnumItem("RODZAJ_FAKTURY").getTitle()));
		else
			folder = folder.createSubfolderIfNotPresent("Rejestr nieokre�lony");
		
		folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(fm.getValue("DATA_WYSTAWIENIA")));
		document.setFolder(folder);

		document.setTitle("Faktura nr " + String.valueOf(fm.getValue("NR_FAKTURY")));
		document.setDescription("Faktura nr " + String.valueOf(fm.getValue("NR_FAKTURY")));
	}

	public void specialSetDictionaryValues(Map<String, ?> dockindFields)
	{
		log.info("Special dictionary values: {}", dockindFields);

	}

	public void addSpecialInsertDictionaryValues(String dictionaryName, Map<String, FieldData> values)
	{
		log.info("Before Insert - Special dictionary: '{}' values: {}", dictionaryName, values);

	}

	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		// UMOWA UMOWA_TYTULUMOWY=tytul, UMOWA_NRUMOWY=123
		log.info("Before Search - Special dictionary: '{}' values: {}", dictionaryName, values);
		if (dictionaryName.equals("UMOWA"))
		{
			//values.put("UMOWA_RODZAJ", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.LONG, 100));
		}
	}

	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values)
	{
		Map<String, Object> results = new HashMap<String, Object>();

		try
		{
			if (dictionaryName.equals("POZYCJE"))
			{
				// wykozystanie budzetu
				BigDecimal cena = ((FieldData) values.get("POZYCJE_CENA")).getMoneyData();
				String entryId = values.get("POZYCJE_VATSTAW").getEnumValuesData().getSelectedOptions().get(0);
				
				String procent = DataBaseEnumField.getEnumItemForTable("ERP_vatstaw_view", Integer.parseInt(entryId)).getRefValue();
				BigDecimal vat = new BigDecimal(procent);
				vat = vat.add(new BigDecimal(100));
				BigDecimal brutto = cena.multiply(vat).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
				
				//ILOSC_1
				BigDecimal ilosc = ((FieldData) values.get("POZYCJE_ILOSC")).getMoneyData();
				brutto = brutto.multiply(ilosc);
				results.put("POZYCJE_KWOTABRUTTO", brutto);

			}
		}
		catch (Exception e)
		{
			log.warn(e.getMessage(), e);
		}
		return results;
	}

	private String checkNettoBrutto(Map<String, ?> fieldValues)
	{
		String msg = "";
		BigDecimal netto = (BigDecimal) fieldValues.get("NETTO");
		BigDecimal brutto = (BigDecimal) fieldValues.get("BRUTTO");

		if (netto.setScale(2, RoundingMode.HALF_UP).compareTo(brutto.setScale(2, RoundingMode.HALF_UP)) == 1)
			msg = "B��d: Kwota brutto nie mo�e by� mniejsza ni� kwota netto";

		return msg;
	}

	private boolean assigneToAcceptMpk(Assignable assignable, OpenExecution openExecution, String accpetionCn) throws EdmException
	{
		boolean assigned = false;
		String assigneeTo = (String) openExecution.getVariable("assigneeTo");
		log.info("assigneeTo: {}", assigneeTo);

		if (assigneeTo.contains("u:"))
		{
			assignable.addCandidateUser(assigneeTo.replace("u:", ""));
			assigned = true;
		}
		else if (assigneeTo.contains("d:"))
		{
			assignable.addCandidateGroup(assigneeTo.replace("d:", ""));
			assigned = true;
		}
		return assigned;
	}

	private String validateNettoBrutto(Map<String, FieldData> values)
	{
		BigDecimal netto = values.get("DWR_NETTO").getMoneyData();
		BigDecimal brutto = values.get("DWR_BRUTTO").getMoneyData();
		BigDecimal vat = BigDecimal.ZERO;
		String msg = "";
		if (netto.setScale(2, RoundingMode.HALF_UP).compareTo(brutto.setScale(2, RoundingMode.HALF_UP)) == 1)
			msg = "B��d: Kwota brutto nie mo�e by� mniejsza ni� kwota netto";
		else
		{
			vat = brutto.subtract(netto);
			values.put("DWR_VAT", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.MONEY, vat));
		}
		return msg;
	}

	private String checkNettoSumMpk(Map<String, ?> fieldValues) throws EdmException
	{
		String msg = "";
		BigDecimal netto = (BigDecimal) fieldValues.get("NETTO");
		BigDecimal sum = BigDecimal.ZERO;

		List<Long> mpkIds = (List<Long>) fieldValues.get("MPK");
		for (Long id : mpkIds)
		{
			BigDecimal oneAmount = new BigDecimal((Double) DwrDictionaryFacade.dictionarieObjects.get("MPK").getValues(id.toString()).get("MPK" + "_AMOUNT"));
			sum = sum.add(oneAmount);
		}

		if (netto.setScale(2, RoundingMode.HALF_UP).compareTo(sum.setScale(2, RoundingMode.HALF_UP)) != 0)
			msg = "B��d: Suma kwot poszczegolnych MPK musi by� r�wna kwocie netto!";

		return msg;
	}

}
