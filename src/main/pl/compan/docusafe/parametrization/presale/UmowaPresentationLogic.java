package pl.compan.docusafe.parametrization.presale;

import java.util.Calendar;
import java.util.Date;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class UmowaPresentationLogic extends AbstractDocumentLogic {
    private final static Logger LOG = LoggerFactory.getLogger(UmowaPresentationLogic.class);

	private static final UmowaPresentationLogic instance = new UmowaPresentationLogic();
	
	public static UmowaPresentationLogic getInstance() 
	{
		return instance;
	}

     @Override
    public void onStartProcess(OfficeDocument document) {
        LOG.info("START PROCESS !!!");
        try {
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }
	
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		Folder folder = Folder.getRootFolder();
    	folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
    	
    	FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
    	
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime((Date) fm.getValue("DATA_WPLUWU"));
    	
    	folder = folder.createSubfolderIfNotPresent(""+calendar.get(Calendar.YEAR));
		folder = folder.createSubfolderIfNotPresent(""+(calendar.get(Calendar.MONTH)+1));
    	
    	document.setFolder(folder);
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		
		perms.add(new PermissionBean(ObjectPermission.READ,"UMOWA_PRESENTATION_READ",ObjectPermission.GROUP,"Dokument "+document.getDocumentKind().getName()+" - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"UMOWA_PRESENTATION_ATT_READ",ObjectPermission.GROUP,"Dokument "+document.getDocumentKind().getName()+" zalacznik - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY,"UMOWA_PRESENTATION_MODIFY",ObjectPermission.GROUP,"Dokument "+document.getDocumentKind().getName()+" - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"UMOWA_PRESENTATION_ATT_MODIFY",ObjectPermission.GROUP,"Dokument "+document.getDocumentKind().getName()+" zalacznik - modyfikacja"));
   	 	
   	 	this.setUpPermission(document, perms);

	}

}
