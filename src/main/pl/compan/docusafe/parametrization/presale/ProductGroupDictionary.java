package pl.compan.docusafe.parametrization.presale;

import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.Box;

public class ProductGroupDictionary
{
	private Long id;
	private String name;
	private String cn;

	public static ProductGroupDictionary find(Long productGroupId) throws EdmException
	{
		return DSApi.getObject(ProductGroupDictionary.class, productGroupId);
	}

	public static List<ProductGroupDictionary> list() throws EdmException
	{
		return Finder.list(ProductGroupDictionary.class, "name");
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getCn()
	{
		return cn;
	}

	public void setCn(String cn)
	{
		this.cn = cn;
	}
}
