package pl.compan.docusafe.parametrization.presale;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.util.StringManager;

public class DJLogic extends AbstractDocumentLogic
{
	private StringManager sm = GlobalPreferences.loadPropertiesFile(DJLogic.class.getPackage().getName(),null);
    private static DJLogic instance;

    public static DJLogic getInstance()
    {
        if (instance == null)
            instance = new DJLogic();
        return instance;
    }

    
    public static final String PRACOWNIK_FIELD_CN = "PRACOWNIK";
    public static final String NAZWA_FIELD_CN = "PRACOWNIK";
    public static final String OPIS_FIELD_CN = "PRACOWNIK";
    public static final String KATEGORIA_FIELD_CN = "KATEGORIA";

    
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
    {
      
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
    	 if (document.getDocumentKind() == null)
             throw new NullPointerException("document.getDocumentKind()");
         
         FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
         DJPersonDictionary person = (DJPersonDictionary)fm.getValue(PRACOWNIK_FIELD_CN);
         
         Folder folder = Folder.getRootFolder();
         /*if(person.getPracuje() != null && person.getPracuje())
         {
        	 folder = folder.createSubfolderIfNotPresent("Pracuj�cy pracownicy");
         }
         else
         {
        	 folder = folder.createSubfolderIfNotPresent("Zwolnieni pracownicy");
         }*/
         folder = folder.createSubfolderIfNotPresent(person.getNrsklepu().getNumer().toString()+" "+person.getNrsklepu().getMiasto());
         
         folder = folder.createSubfolderIfNotPresent(person.getNazwisko()+" "+person.getImie()+"/"+person.getIdPracownika());

         folder = folder.createSubfolderIfNotPresent(""+fm.getField("KATEGORIA").getEnumItem((Integer) fm.getKey("KATEGORIA")).getArg(1));
         
         if(fm.getValue(KATEGORIA_FIELD_CN)!=null)
             document.setTitle(fm.getValue(KATEGORIA_FIELD_CN).toString()+"-"+person.getImie()+" "+person.getNazwisko());
         else
             document.setTitle(document.getDocumentKind().getName());
         
         document.setFolder(folder);
    }
    
    public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
    	Map<String,Object> values = new HashMap<String,Object>();
    	
    	values.put("DATA_WPLYWU_DOKUMENTU", new Date());
    	
    	fm.reloadValues(values);
    }

    public void documentPermissions(Document document) throws EdmException
    {
    	java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
    	perms.add(new PermissionBean(ObjectPermission.READ,"DPERSON_READ",ObjectPermission.GROUP,"Dokument personalny - odczyt"));
	    perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"DPERSON_READ_ATTACHMENT",ObjectPermission.GROUP,"Dokument personalny zalacznik - odczyt"));
	    perms.add(new PermissionBean(ObjectPermission.MODIFY,"DPERSON_MODIFY",ObjectPermission.GROUP,"Dokument personalny - modyfikacja"));
	    perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"DPERSON_MODIFY_ATTACHMENT",ObjectPermission.GROUP,"Dokument personalny zlacznik - modyfikacja"));
	    this.setUpPermission(document, perms);
    }

    public int getPermissionPolicyMode()
    {
        //return PermissionManager.NORMAL_POLICY;
		return PermissionManager.BUSINESS_LIGHT_POLICY;
    }
    
    /*public String getInOfficeDocumentKind()
    {
    	return "Dokument Biznesowy";
    }*/

}
