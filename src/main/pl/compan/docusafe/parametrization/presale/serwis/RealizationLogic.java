package pl.compan.docusafe.parametrization.presale.serwis;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.FolderResolver;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import com.google.common.collect.Maps;

public class RealizationLogic extends AbstractDocumentLogic
{

	private static RealizationLogic instance;
	public final static String ASSIGN_USER_PARAM = "assignee";
	public final static String ASSIGN_DIVISION_GUID_PARAM = "guid";

	protected static Logger log = LoggerFactory.getLogger(RealizationLogic.class);

	public static RealizationLogic getInstance()
	{
		if (instance == null)
			instance = new RealizationLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
	{
		StringBuilder msgBuilder = null;

		DocumentKind kind = fm.getDocumentKind();

		if (values.get("DWR_POZYCJE_NAPRAWY") != null && values.get("DWR_POZYCJE_NAPRAWY").getDictionaryData() != null)
		{
			BigDecimal totalNetto = BigDecimal.ZERO;
			BigDecimal totalBrutto = BigDecimal.ZERO;
			Map<String, FieldData> dvalues = values.get("DWR_POZYCJE_NAPRAWY").getDictionaryData();
			BigDecimal ilosc = null;
			for (String o : dvalues.keySet())
			{
				if (o.contains("NETTO") && dvalues.get(o).getMoneyData() != null )
				{
					String id = o.split("_")[3];
					ilosc = dvalues.get("POZYCJE_NAPRAWY_ILOSC_"+id).getMoneyData();
					totalNetto = totalNetto.add(dvalues.get(o).getMoneyData().multiply(ilosc));
				}
				else if (o.contains("WARTOSC_BRUTTO") && dvalues.get(o).getMoneyData() != null)
				{
					totalBrutto = totalBrutto.add(dvalues.get(o).getMoneyData());
				}
			}
			
			
			if (!totalNetto.equals(new BigDecimal(0)))
			{
				values.get("DWR_TOTAL_NETTO").setMoneyData(totalNetto);
			}
			if (!totalBrutto.equals(new BigDecimal(0)))
			{
				values.get("DWR_TOTAL_BRUTTO").setMoneyData(totalBrutto);
			}
		}
		
		return null;
	}
	
	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			Map<String, Object> toReload = Maps.newHashMap();
			toReload.put("NR_ZGLOSZENIA", document.getId() + "/12/2011");
			String deviceId = (String) document.getFieldsManager().getKey("DEVICE");
			if (deviceId != null && Long.valueOf(deviceId) != -1)
			{
				Map<String, Object> toReload2 = Maps.newHashMap();
				List<Long> a  = (List<Long>) Document.find(Long.parseLong(deviceId)).getFieldsManager().getValue("ZGLOSZENIA");
				if (a == null)
					a = new ArrayList<Long>();
				a.add(document.getId());
				toReload2.put("ZGLOSZENIA", a);
				Document.find(Long.parseLong(deviceId)).getDocumentKind().setOnly(Long.parseLong(deviceId), toReload2);
			}
			document.getDocumentKind().setOnly(document.getId(), toReload);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		Map<String, Object> toReload = Maps.newHashMap();
		for (pl.compan.docusafe.core.dockinds.field.Field f : fm.getFields())
		{
			if (f.getDefaultValue() != null)
			{
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));
			}
		}
		DSUser user = DSApi.context().getDSUser();
		toReload.put("PRZYJMUJACY", user.getId());
		toReload.put("DOC_DATE", new Date());
		log.info("toReload = {}", toReload);
		fm.reloadValues(toReload);
	}

	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue)
	{
		if (acceptationCn.equals("realization"))
		{
			String user = openExecution.getVariable(ASSIGN_USER_PARAM) != null ? (String) openExecution.getVariable(ASSIGN_USER_PARAM) : null;
			String guid = openExecution.getVariable(ASSIGN_DIVISION_GUID_PARAM) != null ? (String) openExecution.getVariable(ASSIGN_DIVISION_GUID_PARAM) : null;

			if (user != null)
			{
				assignable.addCandidateUser(user);
				return true;
			}
			else if (guid != null)
			{
				assignable.addCandidateGroup(guid);
				return true;
			}
		}
		return false;
	}

	public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
	{
		TaskListParams params = new TaskListParams();

		FieldsManager fm = dockind.getFieldsManager(id);
		params.setStatus((String) fm.getValue("STATUS"));
		params.setCategory((String) fm.getValue("TYP"));
		params.setDocumentNumber((String) fm.getValue("NR_ZGLOSZENIA"));
		params.setAttribute(OfficeDocument.find(id).getSender().getSummary(), 0);
		return params;
	}

	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		try
		{
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();

			values.put("NAPRAWA_GWARANCYJNA", fm.getKey("GWARANCJA") != null ? "TAK" : "NIE");
			values.put("UMOWA_SERWISOWA", fm.getKey("UMOWA") != null ? "TAK" : "NIE");
			String deviceId = (String) fm.getKey("DEVICE");
			if (deviceId != null && Long.valueOf(deviceId) != -1)
			{
				Document device = Document.find(Long.valueOf(deviceId));
				values.put("URZADZENIE_NAZWA", device.getFieldsManager().getKey("NAZWA"));
				values.put("URZADZENIE_NRSERYJNY", device.getFieldsManager().getKey("NR_SERYJNY"));
			}

			if (fm.getEnumItem("STATUS").getId().equals(1400))
			{
				values.put("PRZE_TERMIN_ZAKONCZENIA", DateUtils.formatCommonDate((Date) fm.getValue("PRZE_TERMIN_ZAKONCZENIA")));
			}
			else if (fm.getEnumItem("STATUS").getId().equals(1402))
			{
				values.put("DATA_WYDANIA", DateUtils.formatCommonDate(new Date()));
				values.put("KLIENT", doc.getSender().getSummary());
				
				Object pozycje = fm.getKey("POZYCJE_NAPRAWY");
				List<Pozycja> pozycjeList = new ArrayList<Pozycja>();
				Map<String, Object> pozycjaValues = new HashMap<String, Object>();
				Pozycja pozycja = null;
				int count = 1;
				for (Long pozycjaId : (List<Long>) pozycje)
				{
					pozycja = new Pozycja();
					pozycjaValues = DwrDictionaryFacade.dictionarieObjects.get("POZYCJE_NAPRAWY").getValues(pozycjaId.toString());
					pozycja.setTowar(((EnumValues) pozycjaValues.get("POZYCJE_NAPRAWY_TOWAR_USLUGA")).getSelectedValue());
					pozycja.setIlosc((BigDecimal)pozycjaValues.get("POZYCJE_NAPRAWY_ILOSC"));
					pozycja.setJm(((EnumValues) pozycjaValues.get("POZYCJE_NAPRAWY_JM")).getSelectedValue());
					pozycja.setNetto((BigDecimal)pozycjaValues.get("POZYCJE_NAPRAWY_NETTO"));
					pozycja.setVat(((EnumValues) pozycjaValues.get("POZYCJE_NAPRAWY_VAT")).getSelectedValue());
					pozycja.setBrutto((BigDecimal)pozycjaValues.get("POZYCJE_NAPRAWY_BRUTTO"));
					pozycja.setWartoscBrutto((BigDecimal)pozycjaValues.get("POZYCJE_NAPRAWY_WARTOSC_BRUTTO"));
					pozycja.setWartoscNetto(pozycja.getIlosc().multiply(pozycja.getNetto()).setScale(2, RoundingMode.HALF_UP));
					pozycja.setCount(count++);
					pozycjeList.add(pozycja);
				}
				values.put("POZYCJA", pozycjeList);
			}
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
	}

	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values)
	{
		Map<String, Object> results = new HashMap<String, Object>();

		try
		{
			if (dictionaryName.equals("POZYCJE_NAPRAWY"))
			{
				BigDecimal ilosc = ((FieldData) values.get("POZYCJE_NAPRAWY_ILOSC")).getMoneyData();
				BigDecimal netto = ((FieldData) values.get("POZYCJE_NAPRAWY_NETTO")).getMoneyData();
				BigDecimal vat = new BigDecimal(values.get("POZYCJE_NAPRAWY_VAT").getEnumValuesData().getSelectedValue());
				
				BigDecimal brutto = netto.add(netto.multiply(vat.divide(new BigDecimal(100))));
				
				results.put("POZYCJE_NAPRAWY_BRUTTO", brutto);
				results.put("POZYCJE_NAPRAWY_WARTOSC_BRUTTO", brutto.multiply(ilosc));
			}

		}
		catch (NullPointerException e)
		{
			log.error("Lapiemy to: " + e.getMessage(), e);
		}
		finally
		{
			return results;
		}
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FolderResolver fr = document.getDocumentKind().getDockindInfo().getFolderResolver();
		if (fr != null)
		{
			as.useFolderResolver(document);
		}
	}
}