package pl.compan.docusafe.parametrization.presale.serwis;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import com.google.common.collect.Maps;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.record.projects.ProjectEntry;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SetParts implements ExternalActivityBehaviour  {
	
	private final static Logger log = LoggerFactory.getLogger(SetParts.class);
	
	public void execute(ActivityExecution execution) throws Exception {
		Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");

		OfficeDocument doc = OfficeDocument.find(docId);
		ArrayList<Long> dictionaryIds = (ArrayList<Long>) doc.getFieldsManager().getKey("POZYCJE_NAPRAWY");
		String deviceId = (String) doc.getFieldsManager().getKey("DEVICE");
		List<Long> wymienioneCzesci = null;
		Map<String, Object> toReload = Maps.newHashMap();
		if (deviceId != null && Long.valueOf(deviceId) != -1)
		{
			wymienioneCzesci = (List<Long>) Document.find(Long.parseLong(deviceId)).getFieldsManager().getValue("CZESCI_WYM");
			if (wymienioneCzesci == null)
				wymienioneCzesci = new ArrayList<Long>();
		}
		for (Long dicId : dictionaryIds)
		{
			String partId = ((EnumValues) DwrDictionaryFacade.dictionarieObjects.get("POZYCJE_NAPRAWY").getValues(dicId.toString()).get("POZYCJE_NAPRAWY" + "_TOWAR_USLUGA")).getSelectedOptions().get(0);
			Map<String, Object> toReload2 = Maps.newHashMap();
			List<String> a = (List<String>) Document.find(Long.parseLong(partId)).getFieldsManager().getValue("URZADZENIA");
			if (a == null || a.get(0).equals("-1"))
				a = new ArrayList<String>();
			a.add(deviceId.toString());
			toReload2.put("URZADZENIA", a);
			toReload2.put("UZYTY", true);
			toReload2.put("TYP", 1001);
			Document.find(Long.parseLong(partId)).getDocumentKind().setOnly(Long.parseLong(partId), toReload2);
			
			wymienioneCzesci.add(Long.parseLong(partId));
			
		}
		toReload.put("CZESCI_WYM", wymienioneCzesci);
		Document.find(Long.parseLong(deviceId)).getDocumentKind().setOnly(Long.parseLong(deviceId), toReload);
		execution.takeDefaultTransition();
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		// TODO Auto-generated method stub
		
	}

}
