package pl.compan.docusafe.parametrization.presale.serwis;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.FolderResolver;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;

public class DeviceLogic extends AbstractDocumentLogic
{

	private static DeviceLogic instance;
	protected static Logger log = LoggerFactory.getLogger(DeviceLogic.class);

	public static DeviceLogic getInstance()
	{
		if (instance == null)
			instance = new DeviceLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
	{
		StringBuilder msgBuilder = null;
		return null;
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

			List<String> ids = (List<String>) document.getFieldsManager().getValue("CZESCI_WYM");
			if (ids != null && ids.size() > 0)
			{
				for (String id : ids)
				{
					Map<String, Object> toReload2 = Maps.newHashMap();
					List<String> a = (List<String>) Document.find(Long.parseLong(id)).getFieldsManager().getValue("URZADZENIA");
					if (a == null || a.get(0).equals("-1"))
						a = new ArrayList<String>();
					a.add(document.getId().toString());

					toReload2.put("URZADZENIA", a);
					Document.find(Long.parseLong(id)).getDocumentKind().setOnly(Long.parseLong(id), toReload2);
				}
			}

		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret = new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		if (dictionaryName.contains("CZESCI_WYM"))
		{
			values.put(dictionaryName + "_TYP", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.ENUM, "1001"));
		}
	}
	
	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FolderResolver fr = document.getDocumentKind().getDockindInfo().getFolderResolver();
		if (fr != null)
		{
			as.useFolderResolver(document);
		}
	}
}