package pl.compan.docusafe.parametrization.presale.serwis;

import java.math.BigDecimal;

public class Pozycja
{
	private int count;
	private String towar;
	private BigDecimal ilosc;
	private String jm;
	private BigDecimal netto;
	private String vat;
	private BigDecimal brutto;
	private BigDecimal wartoscBrutto;
	private BigDecimal wartoscNetto;
	
	public String getTowar()
	{
		return towar;
	}
	public void setTowar(String towar)
	{
		this.towar = towar;
	}
	public BigDecimal getIlosc()
	{
		return ilosc;
	}
	public void setIlosc(BigDecimal ilosc)
	{
		this.ilosc = ilosc;
	}
	public String getJm()
	{
		return jm;
	}
	public void setJm(String jm)
	{
		this.jm = jm;
	}
	public BigDecimal getNetto()
	{
		return netto;
	}
	public void setNetto(BigDecimal netto)
	{
		this.netto = netto;
	}
	public String getVat()
	{
		return vat;
	}
	public void setVat(String vat)
	{
		this.vat = vat;
	}
	public BigDecimal getBrutto()
	{
		return brutto;
	}
	public void setBrutto(BigDecimal brutto)
	{
		this.brutto = brutto;
	}
	public int getCount()
	{
		return count;
	}
	public void setCount(int count)
	{
		this.count = count;
	}
	public BigDecimal getWartoscBrutto()
	{
		return wartoscBrutto;
	}
	public void setWartoscBrutto(BigDecimal wartoscBrutto)
	{
		this.wartoscBrutto = wartoscBrutto;
	}
	public BigDecimal getWartoscNetto()
	{
		return wartoscNetto;
	}
	public void setWartoscNetto(BigDecimal wartoscNetto)
	{
		this.wartoscNetto = wartoscNetto;
	}

}
