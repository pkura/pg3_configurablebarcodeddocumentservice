package pl.compan.docusafe.parametrization.presale;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Timer;
import java.util.TimerTask;

import javax.naming.NamingException;
import javax.naming.directory.SearchResult;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.mail.MailerDriver;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.axis.simple.SimpleManager;
import pl.compan.docusafe.ws.simple.SimpleDictionaryServiceStub.DictionaryItem;
import pl.compan.docusafe.ws.simple.SimpleDictionaryServiceStub.GetDictionaryByNameResponse;

/**
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 *
 */
public class SimpleDicImport extends ServiceDriver implements Service
{
	public static final Logger log = LoggerFactory.getLogger(SimpleDicImport.class);

	private Timer timer;
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(MailerDriver.class.getPackage().getName(),null);
	private Property[] properties;
	private Long period = 240L;
	private String dictionaryNames = "";
	
    class LdapTimesProperty extends Property
    {
        public LdapTimesProperty()
        {
            super(SIMPLE, PERSISTENT, SimpleDicImport.this, "period", "Czestotliwo�c synchronizacji w minutach", String.class);
        }

        protected Object getValueSpi()
        {
            return period;
        } 

        protected void setValueSpi(Object object) throws ServiceException
        {
            synchronized (SimpleDicImport.this)
            {
            	if(object != null)
            	{
            		period = Long.parseLong((String) object);
            	}
            	else
            	{
            		period = 240L;
            	}
            
            }
        }
    }
    
    class DictionaryNamesProperty extends Property
    {
        public DictionaryNamesProperty()
        {
            super(SIMPLE, PERSISTENT, SimpleDicImport.this, "dictionaryNames", "S�owniki", String.class);
        }

        protected Object getValueSpi()
        {
            return dictionaryNames;
        } 

        protected void setValueSpi(Object object) throws ServiceException
        {
            synchronized (SimpleDicImport.this)
            {
            	dictionaryNames = (String) object;
            }
        }
    }


	public SimpleDicImport()
	{
		properties = new Property[] { new LdapTimesProperty() ,new DictionaryNamesProperty()};
	}

	protected boolean canStop()
	{
		return false;
	}

	protected void start() throws ServiceException
	{
		log.info("start uslugi LdapUserImport");
		timer = new Timer(true);
		timer.schedule(new Import(), (long) 2*1000 ,(long) period*DateUtils.MINUTE);
		console(Console.INFO, sm.getString("StartUslugiDicImport"));
	}

	protected void stop() throws ServiceException
	{
		log.info("stop uslugi");
		console(Console.INFO, sm.getString("StopUslugi"));
		if(timer != null) timer.cancel();
	}

	public boolean hasConsole()
	{
		return true;
	}

	class Import extends TimerTask
	{
		public void run()
		{
			try
			{
				System.out.println(dictionaryNames);
				DSApi.openAdmin();
				SimpleManager mgr = new SimpleManager();
				String[] dics = dictionaryNames.split(",");
	        	for (String dicName : dics)
	        	{
	        		console(Console.INFO, "Aktualizuje s�ownik "+ dicName);
	    			GetDictionaryByNameResponse dicResp = mgr.getDictionaryByNameResponse(dicName);
	    			DictionaryItem [] dicsR = dicResp.get_return();
	    			System.out.println(dicsR.length);
	    			String dsDicName = "ERP_"+dicName;
	        		String sql = mgr.getCreateSQLDic(dsDicName,dicsR);
	        		createIfNotExists(dsDicName, sql);
	        		String insertSql = mgr.getInsertSQLDic(dsDicName,dicsR);
	    			for (DictionaryItem itm : dicsR)
	    			{
	    				insertIfNotExists(dsDicName, itm, mgr, insertSql);
	    			}
	        	}
	        	console(Console.INFO, "Aktuializyje dostawc�w");
	        	DSApi.context().begin();
	        	mgr.getSuppliers();
	        	DSApi.context().commit();
	        	console(Console.INFO, "Zakonczy� aktualizacje");
        	}
        	catch (Exception e) {
				log.error(e.getMessage(), e);
			}
			finally
			{
				try {
					DSApi.close();
				} catch (EdmException e) {
					log.error(e.getMessage(), e);
				}
			}
		}
		
	}
	
	 private void insertIfNotExists(String name, DictionaryItem itm,SimpleManager mgr,String insertSql)
		{
			PreparedStatement ps = null;
			ResultSet rs = null;
			try
			{
				ps = DSApi.context().prepareStatement("SELECT ID FROM "+name+" where id = ?");
				ps.setLong(1,itm.getId());
				rs = ps.executeQuery();
				if(rs.next())
					console(Console.INFO, "Istnieje juz wpis o ID "+ name + " "+ itm.getId());
				else
				{
					DSApi.context().closeStatement(ps);
					ps = DSApi.context().prepareStatement(insertSql);
					ps.setLong(1, itm.getId());
					for (int i = 1; i < itm.getAttributes().length; i++) {
						//System.out.println(itm.getAttributes()[i].getName()+"="+itm.getAttributes()[i].getValue()+"|");
						ps.setString(i+1, itm.getAttributes()[i].getValue());
					}
					ps.executeUpdate();
					console(Console.INFO, "Doda� wpis "+ itm.getId());
				}
			}
			catch (Exception e) {
				log.error(insertSql);
				log.error(e.getMessage(), e);
			}
			finally
			{
				DSApi.context().closeStatement(ps);
			}
		}
		
		private void createIfNotExists(String name, String sql) throws EdmException
		{
			PreparedStatement ps = null;
			ResultSet rs = null;
			try
			{
				ps = DSApi.context().prepareStatement("SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(?) AND type in (N'U')");
				ps.setString(1, name);
				rs = ps.executeQuery();
				if(rs.next())
					console(Console.INFO, "Istnieje juz s�ownik "+ name);
				else
				{
					DSApi.context().closeStatement(ps);
					ps = DSApi.context().prepareStatement(sql);
					ps.execute();
					console(Console.INFO, "Doda� s�ownik "+ name);
				}
			}
			catch (Exception e) {
				log.error(sql);
				log.error(e.getMessage(), e);
				throw new EdmException(e);
			}
			finally
			{
				DSApi.context().closeStatement(ps);
			}
		}
	    
	
	private String getStringAttr(SearchResult element,String name) throws NamingException
	{
		if(element.getAttributes().get(name) != null)
		{
			return (String) element.getAttributes().get(name) .get();
		}
		return null;
	}

	public Property[] getProperties() {
		return properties;
	}

	public void setProperties(Property[] properties) {
		this.properties = properties;
	}
}
