package pl.compan.docusafe.parametrization.presale;

import java.util.Map;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;

/**
 * System zgłaszania reklamacji IM
 * 
 * @author Mariusz Kiljanczyk
 */
public class ComplainIronLogic extends AbstractDocumentLogic
{
    private static ComplainIronLogic instance;

    public static ComplainIronLogic getInstance()
    {
        if (instance == null)
            instance = new ComplainIronLogic();
        return instance;
    }
    
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
    {
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {

    }

    public void documentPermissions(Document document) throws EdmException
    {

    }
        
    public boolean canReadDocumentDictionaries() throws EdmException    
    {
		return true;

    }

}
