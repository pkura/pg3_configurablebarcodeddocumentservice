package pl.compan.docusafe.parametrization.p4;

import com.google.common.collect.Maps;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.FolderInserter;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

public class ParcelRegistrationLogic extends AbstractDocumentLogic
{

    private final static Logger log = LoggerFactory.getLogger(ParcelRegistrationLogic.class);
    public static final String RECIPIENT_HERE = "RECIPIENT_HERE";
    public static final String DOC_DATE = "DOC_DATE";

    @Override
	public void documentPermissions(Document document) throws EdmException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		super.setInitialValues(fm, type);
		Map<String, Object> toReload = new HashMap<String, Object>();
		String sender = ";d:" + DSDivision.ROOT_GUID;
		toReload.put(RECIPIENT_HERE, sender);

        String journalGuidOwner = P4Helper.journaGuidOwner();
        Date currentDay = GlobalPreferences.getCurrentDay(journalGuidOwner);
        toReload.put(DOC_DATE, currentDay);

		fm.reloadValues(toReload);
	}
	
	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent(fm.getDocumentKind().getName());
        if (fm.getValue("STATUS") != null && !fm.getBoolean("OCR"))
        {
            folder = folder.createSubfolderIfNotPresent(FolderInserter.toYear(fm.getValue(DOC_DATE)));
            folder = folder.createSubfolderIfNotPresent(FolderInserter.toMonth(fm.getValue(DOC_DATE)));

            document.setTitle(fm.getDocumentKind().getName() + " " +String.valueOf(fm.getValue("POSTAL_REG_NR")));
            document.setDescription(fm.getDocumentKind().getName() + " " + String.valueOf(fm.getValue("POSTAL_REG_NR")));
        }
        document.setFolder(folder);
		
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
	{		
		try
		{
			Map<String, Object> map = Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			
			Map<String, Object> fieldValues = new HashMap<String, Object>();
            FieldsManager fm = document.getFieldsManager();
            // ustawienie odbiorcy na P4
            if (fm.getKey(RECIPIENT_HERE) != null
                    && document.getRecipients().size() > 0
                    && !document.getRecipients().get(0).getDocumentId().equals(document.getId()))
            {
                String sender = ";d:" + DSDivision.ROOT_GUID;
                fieldValues.put(RECIPIENT_HERE, sender);
            }

			document.getDocumentKind().setOnly(document.getId(), fieldValues);
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

			java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

			DSUser author = DSApi.context().getDSUser();
			String user = author.getName();
			String fullName = author.getLastname() + " " + author.getFirstname();

			perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
			perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn().toUpperCase() + "_DOCUMENT_READ", ObjectPermission.GROUP, document.getDocumentKind().getName() + " - odczyt"));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn().toUpperCase() + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, document.getDocumentKind().getName() + " za��cznik - odczyt"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn().toUpperCase() + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, document.getDocumentKind().getName() + " - modyfikacja"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn().toUpperCase() + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, document.getDocumentKind().getName() + " za��cznik - modyfikacja"));
            perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn().toUpperCase() + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, document.getDocumentKind().getName() + " - usuwanie"));

            Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
			perms.addAll(documentPermissions);
			this.setUpPermission(document, perms);

			DSApi.context().watch(URN.create(document));
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new EdmException(e.getMessage());
		}
	}

    public TaskListParams getTaskListParams(DocumentKind dockind, long id, TaskSnapshot task) throws EdmException
	{
		TaskListParams params = super.getTaskListParams(dockind, id, task);
		FieldsManager fm = dockind.getFieldsManager(id);

		// grupa
		if (fm.getKey("GROUP") != null) {
			params.setAttribute(fm.getEnumItem("GROUP").getTitle(), 2);
		}

        // rodzja przesy�ki
        if (fm.getKey("DELIVERY_TYPE") != null) {
            log.error("SET DELIVERTY TYPE: {}", fm.getEnumItem("DELIVERY_TYPE").getTitle());
            params.setAttribute(fm.getEnumItem("DELIVERY_TYPE").getTitle(), 3);
        }

        // nr dokumentu
        if (fm.getKey("POSTAL_REG_NR") != null) {
            params.setDocumentNumber(fm.getStringKey("POSTAL_REG_NR"));
        }

        // nr dokumentu
        if (fm.getKey(DOC_DATE) != null) {
            params.setDocumentDate((Date) fm.getKey(DOC_DATE));
        }

		return params;
	}
	
}
