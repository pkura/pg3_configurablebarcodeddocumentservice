package pl.compan.docusafe.parametrization.p4;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Jan �wi�cki <jan.swiecki@docusafe.pl>
 */
public class HandWrittenSignatureHelper
{
	private static HandWrittenSignatureHelper instance
		= new HandWrittenSignatureHelper();
	
	private Map<String, Long> map = new HashMap<String, Long>();

	public static HandWrittenSignatureHelper getInstance()
	{
		return instance;
	}
	
	/**
	 * @author Jan �wi�cki <jan.swiecki@docusafe.pl>
	 */
	public void clear()
	{
		synchronized(map)
		{
			map.clear();
		}
	}
	
	/**
	 * @author Jan �wi�cki <jan.swiecki@docusafe.pl>
	 * @param documentId
	 */
	public void add(String barcode, Long documentId)
	{
		synchronized(map)
		{
//			Pair p = new Pair(documentId, barcode);
//			barcodes.add(p);
			map.put(barcode, documentId);
		}
	}
	
	public void removeByBarcode(String barcode)
	{
		synchronized(map)
		{
			map.remove(barcode);
		}
	}
	
	/**
	 * @author Jan �wi�cki <jan.swiecki@docusafe.pl>
	 * @return barcodes
	 */
	public List<Long> getDocumentIds()
	{
		synchronized(map)
		{
			return new ArrayList<Long>(map.values());
		}
	}
	
	/**
	 * @author Jan �wi�cki <jan.swiecki@docusafe.pl>
	 */
//	public static class Pair
//	{
//		Pair(Long documentId, String barcode)
//		{
//			this.documentId = documentId;
//			this.barcode = barcode;
//		}
//		
//		public Long documentId;
//		public String barcode;
//	}

	
}
