package pl.compan.docusafe.parametrization.p4;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;

import java.util.Arrays;

import static pl.compan.docusafe.core.office.Journal.findByDivisionGuid;

/**
 * Created with IntelliJ IDEA.
 * User: jtarasiuk
 * Date: 22.05.13
 * Time: 16:52
 * To change this template use File | Settings | File Templates.
 */
public class P4Helper
{

    public static final String TASMOWA_GUID = "TASMOWA";
    public static final String TASMOWA_JOURNAL_IN = "TASMOWA_IN";
    public static final String TASMOWA_JOURNAL_OUT = "TASMOWA_OUT";
    public static final String MINERALNA_GUID = "MINERALNA";
    public static final String MINERALNA_JOURNAL_IN = "MINERALNA_IN";
    public static final String MINERALNA_JOURNAL_OUT = "MINERALNA_OUT";

    public final static String journaGuidOwner() throws EdmException
    {
        DSUser user = DSApi.context().getDSUser();
        if(Arrays.asList(user.getDivisionsGuid()).contains(P4Helper.TASMOWA_GUID))
        {
            return P4Helper.TASMOWA_GUID;
        }
        else if(Arrays.asList(user.getDivisionsGuid()).contains(P4Helper.MINERALNA_GUID))
        {
            return P4Helper.MINERALNA_GUID;
        }
        return null;
    }

    public final static String journaGuidOwner(DSUser user) throws EdmException
    {

        if(Arrays.asList(user.getDivisionsGuid()).contains(P4Helper.TASMOWA_GUID))
        {
            return P4Helper.TASMOWA_GUID;
        }
        else if(Arrays.asList(user.getDivisionsGuid()).contains(P4Helper.MINERALNA_GUID))
        {
            return P4Helper.MINERALNA_GUID;
        }
        return null;
    }

    public final static Long getJournalId(Document document) throws EdmException
    {
        DSUser user = DSUser.findByUsername(document.getAuthor());

        for (DSDivision division : user.getDivisionsWithoutGroupPosition())
        {
            if(division.getGuid().equals(TASMOWA_GUID))
            {
                for (Object journal : findByDivisionGuid(TASMOWA_GUID))
                {
                    if(document instanceof InOfficeDocument && ((Journal) journal).getJournalType().equals(Journal.INCOMING))
                        return ((Journal) journal).getId();
                    else if(document instanceof OutOfficeDocument && ((Journal) journal).getJournalType().equals(Journal.OUTGOING))
                        return ((Journal) journal).getId();
                }
            }
            else if(division.getGuid().equals(MINERALNA_GUID))
            {
                for (Object journal : findByDivisionGuid(MINERALNA_GUID))
                {
                    if(document instanceof InOfficeDocument && ((Journal) journal).getJournalType().equals(Journal.INCOMING))
                        return ((Journal) journal).getId();
                    else if(document instanceof OutOfficeDocument && ((Journal) journal).getJournalType().equals(Journal.OUTGOING))
                        return ((Journal) journal).getId();
                }
            }
        }
        return null;
    }

    public final static void bindToJournal(Document document) throws EdmException
    {
        OfficeDocument doc = (OfficeDocument) document;
        Long journalId = P4Helper.getJournalId(document);
        DSApi.context().session().flush();
        Integer sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), GlobalPreferences.getCurrentDay());
        if(doc instanceof InOfficeDocument)
            ((InOfficeDocument) doc).bindToJournal(journalId, sequenceId, null);
        else
            ((OutOfficeDocument) doc).bindToJournal(journalId, sequenceId, GlobalPreferences.getCurrentDay(), null);

    }

    public final static String[] getColumn()
    {
        String[] inTasmowaColumns = {"officenumber", "incomingdate", "referenceId", "senderSummary", "summary", "recipientSummary", "recipientUser", "divisionName", "delivery"};
        return inTasmowaColumns;
    }
}
