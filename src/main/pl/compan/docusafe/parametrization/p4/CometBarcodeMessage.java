package pl.compan.docusafe.parametrization.p4;

import java.util.Date;

import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.web.comet.CometMessage;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class CometBarcodeMessage extends CometMessage {
	private static Gson gson = new GsonBuilder()
		.registerTypeAdapter(Date.class, DateUtils.gsonTimestampSerializer)
		.registerTypeAdapter(Date.class, DateUtils.gsonTimestampDeserializer)
		.create();
	
	public static enum BarcodeOperation {
		ADD,
		REMOVE
	}
	
	private MessageKind messageKind;
	private String barcode;
	private BarcodeOperation barcodeOperation;
	private ShortDocumentMetric documentMetric;
	
	public CometBarcodeMessage() {}
	
	public CometBarcodeMessage(MessageKind messageKind, String barcode, BarcodeOperation barcodeOperation, 
			ShortDocumentMetric documentMetric) {
		this.messageKind = messageKind;
		this.barcode = barcode;
		this.barcodeOperation = barcodeOperation;
		this.documentMetric = documentMetric;
	}

	@Override
	public MessageKind getMessageKind() {
		return this.messageKind;
	}

	@Override
	public String toJsonString() {
		return gson.toJson(this);
	}

	public String getBarcode() {
		return barcode;
	}

	public BarcodeOperation getBarcodeOperation() {
		return barcodeOperation;
	}

}
