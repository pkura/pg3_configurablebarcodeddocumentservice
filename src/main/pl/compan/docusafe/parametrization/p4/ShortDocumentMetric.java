package pl.compan.docusafe.parametrization.p4;

import java.util.Date;
import java.util.Map;

public class ShortDocumentMetric {
	public String documentId;
	public String barcode;
	public String delivery;
	public String sender;
	public String recipient;
	public Date date;
	public Map<String, String> recipientUser;
	
	public ShortDocumentMetric() {}
	
	public ShortDocumentMetric(String documentId, String barcode, String delivery, String sender, String recipient, Date date) {
		this.documentId = documentId;
		this.barcode = barcode;
		this.delivery = delivery;
		this.sender = sender;
		this.recipient = recipient;
		this.date = date;
	}
}
