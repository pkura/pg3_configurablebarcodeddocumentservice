package pl.compan.docusafe.parametrization.p4;

import java.util.Calendar;

import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.service.imports.dsi.DSIBean;
import pl.compan.docusafe.service.imports.dsi.DSIImportHandler;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class P4DSIHandler extends DSIImportHandler {
	
	private static final Logger log = LoggerFactory.getLogger(P4DSIHandler.class);
	private static int diff = 0;
	
	@Override
	public String getDockindCn() {
		return DocumentLogicLoader.P4_KIND;
	}
	
	@Override
	protected boolean isStartProces()
	{
		return true;
	}
	
	@Override
	public void actionAfterCreate(Long documentId, DSIBean dsiB) throws Exception
	{	
		// ustawienie losowych informacji dla dokumentu pobranych z bazy danych
		InOfficeDocument doc = (InOfficeDocument)OfficeDocument.find(documentId);
		
		doc.setSummary((String)values.get("SUMMARY"));
		doc.setDescription((String)values.get("DESCRIPTION"));
		doc.setDocumentDate(Calendar.getInstance().getTime());
		doc.setIncomingDate(Calendar.getInstance().getTime());
		doc.setPostalRegNumber((String)values.get("POSTALREGNUMBER"));
		
		// ustawienie wartosci dla odbiorcy pisma
		Recipient r = new Recipient();
		r.setFirstname((String)values.get("RICIPIENT_FIRSTNAME"));
		r.setLastname((String)values.get("RICIPIENT_LASTNAME"));
		r.create();
		doc.addRecipient(r);
		// ustawienie wartosci dla nadawcy pisma
		Sender s = new Sender();
		s.setFirstname((String)values.get("SENDER_FIRSTNAME"));
		s.setLastname((String)values.get("SENDER_LASTNAME"));
		s.setOrganization((String)(String)values.get("SENDER_COMPANY"));
		s.setStreet((String)values.get("SENDER_STREET"));
		s.setZip("25-900");
		s.setLocation((String)values.get("SENDER_CITY"));
		s.create();
		doc.setSender(s);
		
		diff++;
		if (diff>15) {
			log.debug("Zmiana daty");
			changeDate();
			diff=0;
		}
	}
	
	@Override
	public boolean isRequiredAttachment() {
		return false; 
	}
	
	@Override
	public boolean isRequiredBox() {
		return false;
	}
	
	private void changeDate() throws Exception {
		Calendar c = Calendar.getInstance();
		c.setTime(GlobalPreferences.getCurrentDay());
		c.add(Calendar.DATE, 1);
		GlobalPreferences.setCurrentDay(c.getTime());
      if (c.get(Calendar.YEAR) != GlobalPreferences.getCurrentYear())
      {
    	  // zmiana roku
          GlobalPreferences.setCurrentYear(c.get(Calendar.YEAR));
      }
	}

	@Override
	protected void prepareImport() throws Exception {
		
	}

}
