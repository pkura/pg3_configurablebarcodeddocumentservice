package pl.compan.docusafe.parametrization.p4;

import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.web.comet.CometMessage;

import com.google.gson.Gson;


public class CometSignatureMessage extends CometMessage {
	private static Gson gson = new Gson();
	public static enum MessageStatus {
		BEGIN,
		CONFIRM,
		CANCEL,
		ERROR,
		ACCEPT,
		FINISH,
		CORRECT
	}
	
	private MessageKind messageKind = MessageKind.SIGNATURE_REQUEST;
	private String login;
	private String firstname;
	private String lastname;
	private String base64img;
	private String token;
	private MessageStatus status = MessageStatus.BEGIN;
	
	public CometSignatureMessage(DSUser user, MessageStatus messageStatus) {
		this.login = user.getName();
		this.firstname = user.getFirstname();
		this.lastname = user.getLastname();
		this.status = messageStatus;
	}
	
	public CometSignatureMessage(DSUser user, MessageStatus messageStatus, MessageKind messageKind) {
		this(user, messageStatus);
		this.messageKind = messageKind;
	}
	
	public CometSignatureMessage(DSUser user, String base64img, MessageStatus messageStatus) {
		this(user, messageStatus);
		this.base64img = base64img;
	}
	
	@Override
	public String toJsonString() {
		return gson.toJson(this);
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public MessageKind getMessageKind() {
		return this.messageKind;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	
}
