package pl.compan.docusafe.parametrization.p4;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.service.*;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

public class ReportTaskMailerService extends ServiceDriver implements Service
{
	public static final Logger log = LoggerFactory.getLogger(ReportTaskMailerService.class);
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(ReportTaskMailerService.class.getPackage().getName(), null);
	private Property[] properties;
	private Timer timer;
	private String mailTime;
	private Integer dayNumber;

	public ReportTaskMailerService()
	{
		properties = new Property[] { new MailTimesProperty(), new MailDayProperty() };
	}

	class MailTimesProperty extends Property
	{
		public MailTimesProperty()
		{
			super(SIMPLE, PERSISTENT, ReportTaskMailerService.this, "mailTime", "Godzina powiadomie� (np:13:00)", String.class);
		}

		protected Object getValueSpi()
		{
			return mailTime;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (ReportTaskMailerService.this)
			{
				if (object != null)
					mailTime = (String) object;
				else
					mailTime = "10:00";
			}
		}
	}

	class MailDayProperty extends Property
	{
		public MailDayProperty()
		{
			super(SIMPLE, PERSISTENT, ReportTaskMailerService.this, "dayNumber", "Dzie� tygodnia powadomie� (Nale�y poda� liczb�: 1 to Niedziela, 2 to Poniedzia�ek, [...], 6 to Pi�tek)", Integer.class);
		}

		protected Object getValueSpi()
		{
			return dayNumber;
		}

		protected void setValueSpi(Object object) throws ServiceException
		{
			synchronized (ReportTaskMailerService.this)
			{
				if (object != null)
					dayNumber = (Integer) object;
				else
					dayNumber = 6;
			}
		}
	}

	@Override
	protected void start() throws ServiceException
	{
		log.info("Start uslugi UsersIntegrationService");
		timer = new Timer(true);
		timer.schedule(new Report(), (long) 2*1000 ,(long) 5*DateUtils.MINUTE);
		console(Console.INFO, sm.getString("System ReportTaskMailerService wystartowal"));
	}

	@Override
	protected void stop() throws ServiceException
	{
		log.info("Stop uslugi ReportTaskMailerService");
		console(Console.INFO, sm.getString("System ReportTaskMailerService zako�czy� dzia�anie"));
		try
		{
            if (DSApi.context().inTransaction())
                DSApi.context().commit();
		//	DSApi.close();

	        if (timer != null) timer.cancel();
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}

	@Override
	protected boolean canStop()
	{
		return true;
	}

	class Report extends TimerTask
	{
		@Override
		public void run()
		{
			try
			{
				DSApi.openAdmin();
				if (shoudSend())
				{
                    DSApi.context().begin();
					DSApi.context().systemPreferences().node("reportTaskMailer").putLong("lastActivityTime", new Date().getTime());
                    refreshReportTaskTable();

                    DSApi.context().commit();                    
				//	ArrayList<ReportTask> list = (ArrayList<ReportTask>) DSApi.context().session().createQuery("from " + ReportTask.class.getName()).list();
                    ArrayList<Long> documentIds =  new ArrayList<Long>();
					PreparedStatement ps = null;		
						ps = DSApi.context().prepareStatement("select " +
						"inDoc.id as document_id  from DSO_IN_DOCUMENT inDoc "+ 
						"join ds_document d on d.id = inDoc.id "+
						"join dso_journal j on j.id = inDoc.journal_id "+
						"join DSG_NORMAL_DOCKIND nor on nor.DOCUMENT_ID = inDoc.id "+
						"where inDoc.recipientUser is null and " +
						"j.id = (select id from dso_journal where ownerguid = 'TASMOWA' and journaltype = 'incoming')");
						ResultSet rs = ps.executeQuery();
						while (rs.next()){			
							documentIds.add(rs.getLong(1));
						}
						rs.close();						
						
					HashMap<String, ArrayList<Long>> mapa = new HashMap<String, ArrayList<Long>>();

					if (documentIds.size() == 0)
						console(Console.INFO, "Brak dokument�w do odbioru");
					for(Long id : documentIds)
					{
						Long personId =  (Long) Document.find(id).getFieldsManager().getKey("RECIPIENT_HERE");
						if(personId != null){
							Person p = Person.find(personId.longValue());
							String lParam = p.getLparam();
							String split1[] = lParam.split(";");
							String login[] = split1[0].split(":");
							if (mapa.containsKey(login[1]) && mapa.get(login[1]) != null){
								mapa.get(login[1]).add(id);
							}	
							else{
								ArrayList<Long> tempList = new ArrayList<Long>();
								tempList.add(id);
								mapa.put(login[1], tempList);
							}
						}
						else{
							console(Console.WARN, "Dokument od " + OfficeDocument.find(id).getAuthor()+ " ,Tytu�: " + OfficeDocument.find(id).getTitle() +" nie ma okre�lonego odbiorcy");
						}
					}
					
					for (String tempUser : mapa.keySet())
					{
						console(Console.INFO, "Wysy�anie powiadomienia do u�ytkownika " + tempUser);
						DSUser user = DSUser.findByUsername(tempUser);
						String userImie = user.getFirstname();
						Integer n = mapa.get(tempUser).size();
						StringBuilder msg = new StringBuilder();
						Map<String, Object> ctext = new HashMap<String, Object>();
						ctext.put("imie", userImie);
						ctext.put("n", n);

						for (Long documentId : mapa.get(tempUser))
						{
							OfficeDocument ofd = OfficeDocument.find(documentId);
							msg.append(ofd.getDescription()).append("\n");
						}
						
						ctext.put("msg", msg);
						console(Console.INFO, "Msg: " + msg);
						((Mailer) ServiceManager.getService(Mailer.NAME)).send(user.getEmail(), user.getEmail(), null, Configuration.getMail(Mail.REPORT_TASK_MAILER), ctext, false);
					}
				}
			}
			catch (Exception e)
			{
				String msgg = e.getMessage();
				console(Console.ERROR, msgg);
			}
			finally
			{
					DSApi._close();
			}
		}
	}

    private void refreshReportTaskTable()
    {
        PreparedStatement ps = null;
        try {
            String sql = "delete from ds_p4_report_task where document_id not in (select " +
                    "inDoc.id as document_id  from DSO_IN_DOCUMENT inDoc " +
                    "join ds_document d on d.id = inDoc.id " +
                    "where inDoc.recipientUser is null)";
            ps = pl.compan.docusafe.core.DSApi.context().prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            pl.compan.docusafe.core.DSApi.context().closeStatement(ps);
        }
    }

    public Property[] getProperties()
	{
		return properties;
	}

	public void setProperties(Property[] properties)
	{
		this.properties = properties;
	}

	public boolean shoudSend()
	{
		console(Console.INFO, "Sprawdzenie czy nale�y wys�a�");
		Long lastActivityTime = DSApi.context().systemPreferences().node("reportTaskMailer").getLong("lastActivityTime", 0);
		Calendar lastActivityDate = new GregorianCalendar();
		lastActivityDate.setTime(new Date(lastActivityTime));
        Date now = new Date();

		Calendar ca1 = new GregorianCalendar();
		int DAY_OF_WEEK = ca1.get(Calendar.DAY_OF_WEEK);
		if (DAY_OF_WEEK == dayNumber)
		{
			String[] timeTab = mailTime.split(":");
			int hour = Integer.parseInt(timeTab[0]);
			int minut = Integer.parseInt(timeTab[1]);
			Calendar cal = Calendar.getInstance();
			cal.set(GregorianCalendar.HOUR_OF_DAY, hour);
			cal.set(GregorianCalendar.MINUTE, minut);


			if (cal.getTime().before(now) && lastActivityDate.getTime().before(cal.getTime()))
			{
				console(Console.INFO, "Wysy�am");
				return true;
			}
			console(Console.INFO, "Nie wysy�am");
			return false;
		}
		else
		{
			console(Console.INFO, "Nie wysy�am");
			return false;
		}
	}
}
