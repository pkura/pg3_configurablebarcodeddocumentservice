package pl.compan.docusafe.parametrization.p4;

import java.util.Date;

public class ReportTask {
	private Long taskId;
	private Long documentId;
	private Date docTime;
	private String userName;
	
	public ReportTask(){
		
	}
	
	public ReportTask(Long taskId, Long documentId, Date docTime,
			String userName) {
		super();
		this.taskId = taskId;
		this.documentId = documentId;
		this.docTime = docTime;
		this.userName = userName;
	}


	public Long getTaskId() {
		return taskId;
	}
	
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	
	public Long getDocumentId() {
		return documentId;
	}
	
	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}
	
	public Date getTime() {
		return docTime;
	}
	
	public void setTime(Date time){
		this.docTime = time;
	}


	public Date getDocTime() {
		return docTime;
	}


	public void setDocTime(Date docTime) {
		this.docTime = docTime;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
}