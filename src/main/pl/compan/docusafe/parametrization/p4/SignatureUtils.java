package pl.compan.docusafe.parametrization.p4;

import com.lowagie.text.*;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SignatureUtils {
	public static final String SIGNATURE_ATTACHMENT = "SIGNATURE_ATTACHMENT";
	private static final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	private static final Logger log = LoggerFactory.getLogger(SignatureUtils.class);
	public static final String WYDAWCA_USER = Docusafe.getAdditionProperty("p4.comet.message.username") != null ? Docusafe.getAdditionProperty("p4.comet.message.username") : "wydawca";
	public static final String MONITOR_DOTYKOWY_USER = Docusafe.getAdditionProperty("p4.comet.message.monitor.username") != null ? Docusafe.getAdditionProperty("p4.comet.message.monitor.username") : "monitor";
	
	public static Image getSignatureImage(long masterDocumentId) throws EdmException {
		InOfficeDocument masterDoc = (InOfficeDocument) InOfficeDocument.find(masterDocumentId, false);
		
		if(masterDoc != null) {
			Attachment att = masterDoc.getAttachmentByCn(SIGNATURE_ATTACHMENT);
			AttachmentRevision rev = att.getMostRecentRevision();
			if(rev != null) {
				return rev.getItextImage(0);
			}
		} 
		
		return null;
	}
	
	private static void createSignaturePdfTableHeader(PdfPTable table, StringManager sm) {
		table.addCell(sm.getString("officenumber"));
		table.addCell(sm.getString("NrPrzesylkiRejestrowanej"));
		table.addCell(sm.getString("incomingdate"));
		table.addCell(sm.getString("delivery"));
		table.addCell(sm.getString("senderSummary"));
		table.addCell(sm.getString("recipientSummary"));
	}
	
	public static File createSignaturePdf(DSUser user, List<InOfficeDocument> documents, Image img, StringManager sm) 
			throws EdmException {
		int[] colsWidth = new int[] {4, 10, 8, 10, 14, 10};
		try {
			File temp = null;
			File fontDir = new File(Docusafe.getHome(), "fonts");
			File arial = new File(fontDir, "arial.ttf");
			BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(), BaseFont.IDENTITY_H, 
					BaseFont.EMBEDDED);
			
			Font font = new Font(baseFont, 12);
            temp = File.createTempFile("docusafe_", ".pdf");
            Document pdfDoc = new Document(PageSize.A4.rotate(),30,60,30,30);
            PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));
            pdfDoc.open();
            
            StringBuilder docDate = new StringBuilder("Data odbioru: ").append(dateFormat.format(new Date()));
            Paragraph datePar = new Paragraph(docDate.toString());
            datePar.setAlignment(Paragraph.ALIGN_LEFT);
            pdfDoc.add(datePar);
            StringBuilder header = new StringBuilder("Dokumenty odebrane przez ")
            	.append(user.getFirstname()).append(" ").append(user.getLastname());
            Font headerFont = new Font(baseFont, 16);            
            Paragraph headerPar = new Paragraph(header.toString(), headerFont);
            headerPar.setAlignment(Paragraph.ALIGN_CENTER);
            headerPar.setSpacingAfter(5);
            pdfDoc.add(headerPar);
            
            PdfPTable table = new PdfPTable(6);
            table.setWidthPercentage(100);
            table.setWidths(colsWidth);
            table.setSpacingBefore(5);
            table.setSpacingAfter(5);
            table.setHeaderRows(1);
            createSignaturePdfTableHeader(table, sm);
            
            for(int i = 0; i < documents.size(); i ++) {
            	InOfficeDocument doc = documents.get(i);
            	
            	String officeNumber = "";
            	String postalRegNumber = "";
            	String ctime = "";
            	String delivery = "";
            	String sender = "";
            	String recipient = "";
            	if(doc.getOfficeNumber() != null)
            		officeNumber = Integer.toString(doc.getOfficeNumber());
            	if(doc.getPostalRegNumber() != null)
            		postalRegNumber = doc.getPostalRegNumber();
            	if(doc.getCtime() != null)
            		ctime = dateFormat.format(doc.getCtime());
            	if(doc.getDelivery() != null && doc.getDelivery().getName() != null)
            		delivery = doc.getDelivery().getName();
            	if(doc.getSender() != null && doc.getSender().getSummary() != null)
            		sender = doc.getSender().getSummary();
            	List<Recipient> recipients = doc.getRecipients();
            	if(recipients != null && recipients.size() > 0) {
            		if(recipients.get(0).getSummary() != null)
            			recipient = recipients.get(0).getSummary();
            	}
            	
            	
            	table.addCell(officeNumber);
            	table.addCell(postalRegNumber);
            	table.addCell(ctime);
            	table.addCell(delivery);
            	table.addCell(sender);
            	table.addCell(recipient);
            }
            pdfDoc.add(table);
            
            Paragraph signaturePar = new Paragraph("Podpis odbiorcy:");
            signaturePar.setSpacingBefore(20);
            signaturePar.setAlignment(Paragraph.ALIGN_LEFT);
            pdfDoc.add(signaturePar);
            
            img.setAlignment(Image.ALIGN_LEFT);
            img.setBorder(Image.BOX);
            img.setBorderWidth(1);
            img.scalePercent(50);
            pdfDoc.add(img);
            pdfDoc.close();
            
            return temp;
            
		} catch (DocumentException e) {
			log.error(e.getMessage(), e);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		
		return null;
	}
}
