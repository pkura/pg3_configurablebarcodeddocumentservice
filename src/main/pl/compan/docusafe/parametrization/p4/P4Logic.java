package pl.compan.docusafe.parametrization.p4;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.NormalLogic;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.util.*;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

public class P4Logic extends AbstractDocumentLogic {
    public static final String RECIPIENT_HERE = "RECIPIENT_HERE";
    public static final String DWR_SENDER = "DWR_SENDER";
    private static NormalLogic instance;
	private final static Logger log = LoggerFactory.getLogger(P4Logic.class);

	public static NormalLogic getInstance() {
		if (instance == null)
			instance = new NormalLogic();
		return instance;
	}

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
        super.setInitialValues(fm, type);
        Map<String, Object> toReload = new HashMap<String, Object>();
        String journalGuidOwner = P4Helper.journaGuidOwner();
        Date currentDay = GlobalPreferences.getCurrentDay(journalGuidOwner);
        toReload.put("DOC_DATE", currentDay);
        fm.reloadValues(toReload);
    }

    public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException
    {
        try
        {
            Map<String, Object> map = Maps.newHashMap();
            map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
            map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));

            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);

            java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

            DSUser author = DSApi.context().getDSUser();
            String user = author.getName();
            String fullName = author.asFirstnameLastname();

            perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));
            perms.add(new PermissionBean(ObjectPermission.READ, document.getDocumentKind().getCn().toUpperCase() + "_DOCUMENT_READ", ObjectPermission.GROUP, document.getDocumentKind().getName() + " - odczyt"));
            perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, document.getDocumentKind().getCn().toUpperCase() + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, document.getDocumentKind().getName() + " za��cznik - odczyt"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY, document.getDocumentKind().getCn().toUpperCase() + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, document.getDocumentKind().getName() + " - modyfikacja"));
            perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, document.getDocumentKind().getCn().toUpperCase() + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, document.getDocumentKind().getName() + " za��cznik - modyfikacja"));
            perms.add(new PermissionBean(ObjectPermission.DELETE, document.getDocumentKind().getCn().toUpperCase() + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, document.getDocumentKind().getName() + " - usuwanie"));

            Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
            perms.addAll(documentPermissions);
            this.setUpPermission(document, perms);

            DSApi.context().watch(URN.create(document));
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
            throw new EdmException(e.getMessage());
        }
    }
	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
    {    	
		log.info("DokumentKind: {}, DocumentId: {}, Values from JavaScript: {}", fm.getDocumentKind().getCn(), fm.getDocumentId(), values);
		/*try {
			Object obj = fm.getKey(RECIPIENT_HERE);
			if(values.get(DWR_SENDER) != null && values.get(DWR_SENDER).getDictionaryData().get("SENDER_ANONYMOUS").getBooleanData() == Boolean.TRUE){
				DSApi.openAdmin();
				Criteria crit = DSApi.context().session().createCriteria(Person.class);
				crit.add(Restrictions.eq("anonymous", Boolean.TRUE));
				crit.add(Restrictions.sqlRestriction("DISCRIMINATOR = 'PERSON'"));
				crit.add(Restrictions.eq("dictionaryType", Person.DICTIONARY_SENDER));
				crit.add(Restrictions.isNull("firstname"));
				crit.add(Restrictions.isNull("lastname"));
				
				Object obje = fm.getKey(RECIPIENT_HERE);
				
				
				Person anonimousPerson = (Person) crit.list().get(crit.list().size()-1);
				if(anonimousPerson == null)
					throw new EdmException("Nie znaleziono anonimowej osoby w bazie");
				else
					values.get(DWR_SENDER).getDictionaryData().get("id").setLongData(anonimousPerson.getId());
			}
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		} finally {
			DSApi._close();
		}*/
		return null;
    }
	
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		log.trace("!!! docId: {}", document.getId());
		if(AvailabilityManager.isAvailable("p4LogicW"))
		{
			try 
			{
				Integer time = Integer.parseInt(Docusafe.getAdditionProperty("p4LogicWtime"));
				Thread.sleep(time*1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		if (document instanceof OfficeDocument)
		{
			OfficeDocument doc = (OfficeDocument) document;
			Sender s = doc.getSender();
			String opis = null;
			log.trace("!!! docId: {}, opis: {}, getSummary: {}", doc.getId(), opis, doc.getSummary());
			if(s != null)
			{
				opis = ""+(s.getFirstname() != null ? s.getFirstname() : "")
				+" "+(s.getLastname() != null ? s.getLastname() : "")
				+" "+(s.getOrganization() != null ? s.getOrganization() : "")
				+" "+(s.getLocation() != null ? s.getLocation() : "")
				+" "+(s.getZip() != null ? s.getZip()  : "")
				+" "+(s.getStreet() != null ? "ul. "+s.getStreet(): "");
			}
			else
			{
				opis = "brak";
			}
			log.trace("!!! docId: {}, opis: {}, getSummary: {}", doc.getId(), opis, doc.getSummary());
			document.setDescription(opis);
			document.setTitle(opis);
			doc.setSummary(opis);
			log.trace("!!! docId: {}, opis: {}, getSummary: {}", doc.getId(), opis, doc.getSummary());
		}

	}

	public void documentPermissions(Document document) throws EdmException {
		// TODO Auto-generated method stub

	}
	
	@Override
	public String countAttachmentRevisionVerficationCode(AttachmentRevision ar) {
		String code = super.countAttachmentRevisionVerficationCode(ar);
		
		try {
			InOfficeDocument doc = (InOfficeDocument) ar.getAttachment().getDocument();
			if(doc.getLparam() == null) {
				doc.setLparam(code);
			} else {
				code = doc.getLparam();
			}
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}
		
		return code;
	}

	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException 
	{
		if(document instanceof InOfficeDocument)
		{
			List<Recipient> recs = ((InOfficeDocument)document).getRecipients();
			if(recs != null && recs.size()> 0)
			{
				Recipient rec = recs.get(0);
				
				if(StringUtils.isNotBlank(rec.getFirstname()) && StringUtils.isNotBlank(rec.getLastname()))
				{
					DSUser user = DSUser.findByFirstnameLastname(rec.getFirstname(), rec.getLastname(),false);
					if(user != null)
						return new ProcessCoordinator(user);
				}
			}
		}
		return null;
	}
	

}
