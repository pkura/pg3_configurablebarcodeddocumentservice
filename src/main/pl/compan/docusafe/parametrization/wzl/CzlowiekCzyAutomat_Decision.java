package pl.compan.docusafe.parametrization.wzl;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

@SuppressWarnings("serial")
public class CzlowiekCzyAutomat_Decision implements DecisionHandler {

	private final static Logger log = LoggerFactory.getLogger(CzlowiekCzyAutomat_Decision.class);
	
	public String decide(OpenExecution openExecution) {
		String decision = "realizacja";
		if ("admin".equals(DSApi.context().getPrincipalName()))
			decision = "automat";
		else 
			decision = "czlowiek";
		
		return decision;
	}
}