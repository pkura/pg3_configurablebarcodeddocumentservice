package pl.compan.docusafe.parametrization.wzl;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.service.imports.dsi.DSIAttribute;
import pl.compan.docusafe.service.imports.dsi.DSIBean;
import pl.compan.docusafe.service.imports.dsi.DSIImportHandler;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class WZLDSIHandler extends DSIImportHandler {

	private final static Logger log = LoggerFactory.getLogger(WZLDSIHandler.class);
	private final static String _SQL = "SELECT DOCUMENT_ID FROM DSG_NORMAL_DOCKIND WHERE sygnatura = ?";

	
	public WZLDSIHandler() {
		super();
	}
	protected boolean isStartProces()
	{
		return true;
	}

	@Override
	protected void prepareImport() throws Exception 
	{
		Long documentId = getDocumentIdBySignature();
		if(documentId != null)
			importDocument = Document.find(documentId);
		String data = null;
		data = DateUtils.formatJsDateOrEmptyString(new Date());
		if(data != null && data.length() > 0 )
		{
			values.put("DOC_DATE", data.replaceAll("\\.", "-"));
		}
	    values.put("RECIPIENT_HERE", "u:j.pietowski;d:BFA800652550036C13C28BD4061B95FAD19");

	}

	@Override
	public String getDockindCn() {
		return DocumentLogicLoader.NORMAL_KIND;
	}

	@Override
	public void actionAfterCreate(Long documentId, DSIBean dsiB) throws Exception {
		try
		{
			String destFilePath = Docusafe.getAdditionProperty("importDocument.moveFile");
			destFilePath.replace("/", File.separator);
			
			File file = new File(dsiB.getFilePath());
			
			if(!file.exists())
				return;
			
			File destFolder = new File(destFilePath + File.separator + DateUtils.folderDateFormat.format(new Date()));

			if(!destFolder.exists())
				if(!destFolder.mkdir())
				{
					log.error("can't create folder");
					return;
				}
			
			File destFile = new File(destFolder+File.separator+file.getName());
			
			if(file.renameTo(destFile)){
				log.error("MOVED DSIbean: " + dsiB.getImportId() + "," + dsiB.getFilePath());
			}
			else
			{
				log.error("ERROR DSIbean: " + dsiB.getImportId() + "," + dsiB.getFilePath());
			}
		}catch(Exception e){
			log.error("Nie uda�o si� przeniesc", e);
		}
	}

	
	@Override
	protected void setUpDocumentBeforeCreate(Document document) throws Exception
	{
		if(importDocument == null)
			super.setUpDocumentBeforeCreate(document);
	}

	@Override
	public void supplyImport(File file, DSIBean dsiBean) throws Exception {
		String fileName = file.getName();
		List<DSIAttribute> dsiAttributes = new ArrayList<DSIAttribute>();
		
		String[] fileNames = fileName.split("\\.");
		if(fileNames.length > 0 )
		{
			if(file.getAbsolutePath().contains("Skan")){
				dsiAttributes.add((new DSIAttribute("DOC_DELIVERY", "3")));
			}else if(file.getAbsolutePath().contains("Fax")){
				dsiAttributes.add((new DSIAttribute("DOC_DELIVERY", "1")));
			}else if(file.getAbsolutePath().contains("Email")){
				dsiAttributes.add((new DSIAttribute("DOC_DELIVERY", "2")));
				try
				{
					PreparedStatement ps = DSApi.context().prepareStatement("SELECT count(*)  FROM DSO_IN_DOCUMENT as a" +
					" WHERE a.delivery=2 AND DATEDIFF(month, a.INCOMINGDATE, GETDATE())=0");					
					ResultSet rs = ps.executeQuery();
					if(rs.next())
					{
						String sygnatura =  rs.getLong(1)+1+"/"+DateUtils.formatSlashMinDate(new Date())+"/E";
						dsiAttributes.add(new DSIAttribute("sygnatura", sygnatura));
					}
				}catch(Exception e){
					log.error("", e);
				}
				
				 
				
			}
		}
		
		dsiBean.setAttributes(dsiAttributes);
		dsiBean.setBoxCode("noneed");
		dsiBean.setDocumentKind(getDockindCn());
		dsiBean.setImageArray(null);
		dsiBean.setFilePath(file.getAbsolutePath());
		dsiBean.setImportKind(1);
		dsiBean.setSubmitDate(new Date());
		dsiBean.setUserImportId(null);
		dsiBean.setImageName(null);
		dsiBean.setImportStatus(1);
		dsiBean.setImportId(null);
	}

	/**
	 * Pobiera document po kodzie sygnaturze pisma
	 * @return
	 */
	private Long getDocumentIdBySignature()
	{
		Long docId = null;
		try
		{
			PreparedStatement ps = DSApi.context().prepareStatement(_SQL);
			if(values.get("sygnatura") == null)
				return docId;
			
			ps.setString(1, values.get("sygnatura").toString());
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next())
			{
				docId = Long.valueOf(rs.getLong(1));
			}
		}catch(Exception e){
			log.error("", e);
		}
		return docId;
	}

    @Override
    protected void setUpDocumentAfterCreate(Document document) throws Exception {
        super.setUpDocumentAfterCreate(document);

        /**
         * PRzeniesione z DSIImportHandler przyzdefiniowanej metodzie onStart na logice. Proces tworzy� si� 2x
         */
        Map<String, Object> map = Maps.newHashMap();
        map.put(ASSIGN_USER_PARAM, "admin"); // TODO : do przemy�lenia
        map.put(ASSIGN_DIVISION_GUID_PARAM, "rootdivision");
        try
        {
            //uruchomienie procesu okre�lonego w dockindzie
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart((OfficeDocument)document, map);
            TaskSnapshot.updateByDocument(document);
        }
        catch (Exception e)
        {
            log.error("B��d przy uruchamianiu procesu: "+e.getMessage(), e);
        }
    }
}
