package pl.compan.docusafe.parametrization.wzl;

import java.sql.Date;
import java.util.Calendar;

import org.jbpm.api.listener.EventListenerExecution;

import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.jbpm4.AbstractEventListener;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

@SuppressWarnings("serial")
public class FillJournalEntry extends AbstractEventListener {
	private static final long serialVersionUID = 1L;
	private final static Logger log = LoggerFactory.getLogger(FillJournalEntry.class);
	
	public void notify(EventListenerExecution eventListenerExecution) throws Exception {
		Long docId = Long.valueOf(eventListenerExecution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
		InOfficeDocument document =  (InOfficeDocument) InOfficeDocument.find(docId);
		
		Calendar currentDay = Calendar.getInstance();
        currentDay.setTime(GlobalPreferences.getCurrentDay());
        
	    Journal journal = Journal.getMainIncoming();
	    Long journalId;
	    if(journal.findDocumentEntry(docId)==null){
		    journalId = journal.getId();
		    Integer sequenceId = null;
		    sequenceId = Journal.TX_newEntry2(journalId, document.getId(), new Date(currentDay.getTime().getTime()));
		    
		    document.bindToJournal(journalId, sequenceId);
	    }
    }
}
