package pl.compan.docusafe.parametrization.bisk;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;

import com.lowagie.text.Cell;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Row;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.codec.GifImage;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.parametrization.presale.ItemOrderDictionary;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.web.office.common.DocumentArchiveTabAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class BiskLogic extends AbstractDocumentLogic {

	
	private static final BiskLogic instance = new BiskLogic();
	
	public static BiskLogic getInstance() {
		return instance;
	}

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
		document.setFolder(folder);
		
		document.setTitle(String.valueOf(fm.getValue(fm.getMainFieldCn())));
		document.setDescription(String.valueOf(fm.getValue(fm.getMainFieldCn())));
		
		Double suma = 0.0;
		if(fm.getKey("KOSZT_ROBOCIZNY") != null)
		{
			suma = suma + ((BigDecimal)fm.getKey("KOSZT_ROBOCIZNY")).doubleValue();
		}
		if(fm.getKey("KOSZT_WYSYLKI") != null)
		{
			suma = suma + ((BigDecimal)fm.getKey("KOSZT_WYSYLKI")).doubleValue();
		}
		if(fm.getKey("KOSZT_DOJAZDU") != null)
		{
			suma = suma + ((BigDecimal)fm.getKey("KOSZT_DOJAZDU")).doubleValue();
		}
        Map<String, Object> values = new HashMap<String, Object>();
		values.put("SUMA", suma); 
			
		if(fm.getKey("PODRODZAJ") != null && fm.getKey("STATUS") == null &&  ((Integer)fm.getKey("PODRODZAJ")).equals(20) )
		{
			values.put("STATUS", 10);
		}
			
		if(values != null && values.size() > 0){
			document.getDocumentKind().setOnly(document.getId(), values);
		}
	}

	
	public void documentPermissions(Document document) throws EdmException 
	{
	

	}
	
    public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
        Map<String,Object> values = new HashMap<String,Object>();
        values.put("NR_REKLAMACJI", getNumerReklamacji());
        values.put("PODRODZAJ", 10);
        fm.reloadValues(values);
    }
    
    private Integer getNumerReklamacji()
    {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	Integer max = 1;
    	try
    	{
    		ps = DSApi.context().prepareStatement("select MAX(nr_reklamacji) from dsg_bisk_reklamacja;");
			rs = ps.executeQuery();
			if(rs.next())
				max = rs.getInt(1);			
			ps.close();
    	}
    	catch (Exception e) 
    	{
			e.printStackTrace();
		}
    	finally
    	{
    		DbUtils.closeQuietly(rs);
    		DbUtils.closeQuietly(ps);
    	}
    	return max+1;
    }    

	public void doDockindEvent(DockindButtonAction eventActionSupport, ActionEvent event,Document document, String activity,Map<String, Object> values) throws EdmException {
		if ("PUSH".equals(eventActionSupport.getDockindEventValue()))
			goTO(eventActionSupport, eventActionSupport.getDockindEventValue(), event, values, document);
		else
			goReturn(eventActionSupport, eventActionSupport.getDockindEventValue(), event, values, document);
	}
	
	private void goReturn(DockindButtonAction eventActionSupport, String dockingEvent, ActionEvent event, Map<String, Object> values, Document document)
	throws EdmException
	{
		DSApi.context().begin();
		String activityId = null;
		if (eventActionSupport instanceof DocumentArchiveTabAction)
			activityId = ((DocumentArchiveTabAction) eventActionSupport).getActivity();
		else
		{
			event.addActionMessage("Nie mo�na wykona� tej akcji.");
			return;
		}

		String userTo = document.getAuthor();
		String guidTo = DSDivision.ROOT_GUID;
		WorkflowFactory.simpleAssignment(activityId, guidTo, userTo, DSApi.context().getPrincipalName(), null, WorkflowFactory.SCOPE_DIVISION, null, event);
		Map<String, Object> fieldValues = new HashMap<String, Object>();
		fieldValues.put("STATUS",20);
		document.getDocumentKind().setOnly(document.getId(), fieldValues);
		event.addActionMessage("Zadanie zwr�cono do zg�aszaj�cego");
		event.skip(DocumentArchiveTabAction.EV_FILL);
		event.setResult("confirm");
		DSApi.context().commit();
	}
	

	private void goTO(DockindButtonAction eventActionSupport, String dockingEvent, ActionEvent event, Map<String, Object> values, Document document)
			throws EdmException
	{
		DSApi.context().begin();
		String activityId = null;
		if (eventActionSupport instanceof DocumentArchiveTabAction)
			activityId = ((DocumentArchiveTabAction) eventActionSupport).getActivity();
		else
		{
			event.addActionMessage("Nie mo�na wykona� tej akcji.");
			return;
		}
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		Integer serId = (Integer) fm.getKey("SERWISANT");
    	String userTo = document.getDocumentKind().getFieldByCn("SERWISANT").getEnumItem(serId).getCn();
    	String guidTo = DSDivision.ROOT_GUID;
		WorkflowFactory.simpleAssignment(activityId, guidTo, userTo, DSApi.context().getPrincipalName(), null, WorkflowFactory.SCOPE_DIVISION, null, event);
		Map<String, Object> fieldValues = new HashMap<String, Object>();
		fieldValues.put("STATUS",10);
		document.getDocumentKind().setOnly(document.getId(), fieldValues);
		event.addActionMessage("Przekazano zadanie do serwisanta");
		event.skip(DocumentArchiveTabAction.EV_FILL);
		event.setResult("confirm");
		DSApi.context().commit();
	}
	
	private static File robi(String nrZamowienia,String odbiorca,String dostawca,List<ItemOrderDictionary> items) throws Exception
	{
		File fontDir = new File(Docusafe.getHome(),"/fonts");
		File arial = new File(fontDir, "arial.ttf");
		BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

		Font font = new Font(baseFont, 8);
		Font fontB = new Font(baseFont, 8,Font.BOLD);

		File temp = File.createTempFile("zgloszeni zeklamacji", ".pdf");
		com.lowagie.text.Document pdfDoc = new com.lowagie.text.Document(PageSize.A4, 30, 60, 30, 30);
		PdfWriter writer = PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));
		

		HeaderFooter footer = new HeaderFooter(new Phrase("Zg�oszenie reklamacji ", font), new Phrase("."));
		footer.setAlignment(Element.ALIGN_CENTER);
		pdfDoc.setFooter(footer);
		
		pdfDoc.open();
		PdfContentByte cb = writer.getDirectContent();	
	
		File imageF =  new File(Docusafe.getHome(),"/img/logo_c.gif");
		Image image = new GifImage(imageF.getAbsolutePath()).getImage(1);
		image.scaleToFit(200, 200);
		image.setAbsolutePosition(20, 800);
		cb.addImage(image);
		


		Cell cell;
		Row row;
		Rectangle rect;
		Table table = new Table(10);
		table.setCellsFitPage(true);
		table.setAutoFillEmptyCells(true);
		table.setSpaceInsideCell(1);
		table.setOffset(50);
		table.setTableFitsPage(true);
		rect = new Rectangle(0, 0);

		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

		cell = new Cell();
		cell.setColspan(10);
		cell.setRowspan(2);
		cell.add(new Phrase("Zam�wienie ",new Font(baseFont, 16,Font.BOLD)));
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(10);
		cell.setRowspan(1);
		cell.add(new Phrase(" ",font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(10);
		cell.setRowspan(1);
		cell.add(new Phrase(" ",font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(2);
		cell.setRowspan(1);
		cell.add(new Phrase("  Data :",font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(8);
		cell.setRowspan(1);
		cell.add(new Phrase(formatter.format(new Date()),font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(2);
		cell.setRowspan(1);
		cell.add(new Phrase("  Zam�wienie numer : ",font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(8);
		cell.setRowspan(1);
		cell.add(new Phrase(nrZamowienia,font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(10);
		cell.setRowspan(1);
		cell.add(new Phrase(" ",font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(5);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.add(new Phrase("DOSTAWCA",new Font(baseFont, 10)));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(5);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.add(new Phrase("ODBIORCA",new Font(baseFont, 10)));		
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(5);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.add(new Phrase(odbiorca,font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(5);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.add(new Phrase(dostawca,font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(10);
		cell.setRowspan(1);
		cell.add(new Phrase(" ",font));
		cell.setBorder(0);
		table.addCell(cell);
		cell = new Cell();
		cell.setColspan(10);
		cell.setRowspan(1);
		cell.add(new Phrase(" ",font));
		cell.setBorder(0);
		table.addCell(cell);
		cell = new Cell();
		cell.setColspan(10);
		cell.setRowspan(1);
		cell.add(new Phrase(" ",font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(4);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.add(new Phrase("Produkt",fontB));
		//cell.setBorder(1);
		cell.setBorderColor(Color.BLACK);
		cell.setBorderColorLeft(Color.BLACK);
		//cell.setBorderWidth(2);
		cell.setBorderColorRight(Color.BLACK);
		cell.setBackgroundColor(Color.LIGHT_GRAY);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(2);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.add(new Phrase("Cena",fontB));
		///cell.setBorder(1);
		cell.setBorderColor(Color.BLACK);
		cell.setBorderColorLeft(Color.BLACK);
		//cell.setBorderWidth(2);
		cell.setBorderColorRight(Color.BLACK);
		cell.setBackgroundColor(Color.LIGHT_GRAY);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(2);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.add(new Phrase("Ilo��",fontB));
		//cell.setBorder(1);
		cell.setBorderColor(Color.BLACK);
		cell.setBorderColorLeft(Color.BLACK);
		//cell.setBorderWidth(2);
		cell.setBorderColorRight(Color.BLACK);
		cell.setBackgroundColor(Color.LIGHT_GRAY);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(2);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.add(new Phrase("Warto��",fontB));
		//cell.setBorder(1);
		cell.setBorderColor(Color.BLACK);
		cell.setBorderColorLeft(Color.BLACK);
		//cell.setBorderWidth(2);
		cell.setBorderColorRight(Color.BLACK);
		cell.setBackgroundColor(Color.LIGHT_GRAY);
		table.addCell(cell);
		
		
		Double suma = 0.0;
		for (ItemOrderDictionary it : items)
		{
			cell = new Cell();
			cell.setColspan(4);
			cell.setRowspan(1);
			cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
			cell.add(new Phrase(it.getDescription(),font));
			//cell.setBorder(1);
			table.addCell(cell);
			
			cell = new Cell();
			cell.setColspan(2);
			cell.setRowspan(1);
			cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
			cell.add(new Phrase(it.getAmount().toString(),font));
			//cell.setBorder(1);
			table.addCell(cell);
			
			cell = new Cell();
			cell.setColspan(2);
			cell.setRowspan(1);
			cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
			cell.add(new Phrase(it.getVolume().toString(),font));
			//cell.setBorder(1);
			table.addCell(cell);
			
			cell = new Cell();
			cell.setColspan(2);
			cell.setRowspan(1);
			cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
			cell.add(new Phrase(it.getWartosc().toString(),font));
			//cell.setBorder(1);
			table.addCell(cell);
			
			suma += it.getWartosc();
		}
		
		cell = new Cell();
		cell.setColspan(6);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.add(new Phrase(" ",font));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(2);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
		cell.add(new Phrase(" Suma : ",fontB));
		cell.setBorder(0);
		table.addCell(cell);
		
		cell = new Cell();
		cell.setColspan(2);
		cell.setRowspan(1);
		cell.setHorizontalAlignment(Cell.ALIGN_CENTER);
		cell.add(new Phrase(suma.toString(),fontB));
		cell.setBorder(0);
		table.addCell(cell);

		pdfDoc.add(table);
		pdfDoc.close();
		
		return temp; 
	}

}
