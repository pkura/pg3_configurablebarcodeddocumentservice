package pl.compan.docusafe.parametrization.ald.reports;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.ObjectInputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.field.*;
import pl.compan.docusafe.core.dockinds.dictionary.DicInvoice;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.TextUtils;

public class AldSearchResultReport extends Report {

	private static final int MAX_IDS = 900;
	static final Logger log = LoggerFactory.getLogger(AldSearchResultReport.class);
	UniversalTableDumper dumper;
	
	private List<Long> documentsIds;
	
	public List<Long> search(Map<String,Object> values, Map<String,Object> invalues, Map<String,Object> elseMap) throws Exception
	{
		String sortField = "id";
		
		DockindQuery dockindQuery = new DockindQuery(0 , 0);
        
        
        	
			if (elseMap.get("accessedBy") != null || elseMap.get("accessedAs") != null)
	        {
	            if (elseMap.get("accessedBy") != null)
	            {
	                dockindQuery.accessedBy((String) elseMap.get("accessedBy"));
	            }
	
	            if (elseMap.get("accessedAs") != null)
	            {
	                dockindQuery.enumAccessedAs((String[]) elseMap.get("accessedAs"));
	            }
	
	            Date accFrom = DateUtils.nullSafeParseJsDate((String) elseMap.get("accessedFrom"));
	            Date accTo = DateUtils.nullSafeParseJsDate((String) elseMap.get("accessedTo"));
	
	            dockindQuery.accessedFromTo(
	                DateUtils.nullSafeMidnight(accFrom, 0),
	                DateUtils.nullSafeMidnight((accFrom != null && accTo == null) ? accFrom : accTo, 1));
	        }
        	
        	DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.ALD_KIND);
            kind.initialize();
            
            if (elseMap.get("boxName") != null)
            {
                Box box = Box.findByName(kind.getProperties().get(DocumentKind.BOX_LINE_KEY), TextUtils.trimmedStringOrNull((String) elseMap.get("boxName")));
                dockindQuery.boxId(box.getId());
            }
            
            if("true".equals(Configuration.getProperty("flags")))
            {
                dockindQuery.setAllFlags((Long[]) elseMap.get("flagIds"),
                		DateUtils.nullSafeParseJsDate((String) elseMap.get("flagsDateFrom")),
                		DateUtils.nullSafeParseJsDate((String) elseMap.get("flagsDateTo")),
                		(Long[]) elseMap.get("userFlagIds"),
                		DateUtils.nullSafeParseJsDate((String) elseMap.get("userFlagsDateFrom")),
                		DateUtils.nullSafeParseJsDate((String) elseMap.get("userFlagsDateTo"))
                );
            }
            
            Map<String,Object> fieldValues = new HashMap<String,Object>();

            dockindQuery.setDocumentKind(kind);
            dockindQuery.setCheckPermissions(true);
            
            for (Field field : kind.getFields())
            {       
                if (Field.ENUM.equals(field.getType()) ||  Field.ENUM_REF.equals(field.getType()))
                {   
                    if (values.get(field.getCn()) == null)
                        continue;                          
                    Integer[] enumIds = TextUtils.parseIntoIntegerArray(values.get(field.getCn()));
                    dockindQuery.enumField(field, enumIds);
                    fieldValues.put(field.getCn(), enumIds);
                }
                else if (field instanceof DateField || field.isSearchByRange())
                {
                	
                	if(invalues.get(field.getCn()) == null)
                	{	
                    String from = (String) values.get(field.getCn()+"_from");
                    String to = (String) values.get(field.getCn()+"_to");
                    /*Date dateFrom = (Date)*/ Object valueFrom = field.simpleCoerce(from);
                    /*Date dateTo = (Date)*/ Object valueTo = field.simpleCoerce(to);
                    dockindQuery.rangeField(field, valueFrom, valueTo);
                    fieldValues.put(field.getCn()+"_from", from);
                    fieldValues.put(field.getCn()+"_to", to);
                	}
                	else 
                	{
                		String value = (String)invalues.get(field.getCn());
                		String[] tmp = value.split("\n");
                		for (int i = 0; i < tmp.length; i++)
                		{
                			tmp[i] = tmp[i].trim();
						}
                        dockindQuery.InStringField(field, tmp);
                        fieldValues.put(field.getCn(), tmp);
                	}
                }
                else if (Field.STRING.equals(field.getType()))
                {
                	if(invalues.get(field.getCn()) != null)
                	{
                		String value = (String)invalues.get(field.getCn());
                		String[] tmp = value.split("\n");
                		for (int i = 0; i < tmp.length; i++)
                		{
                			tmp[i] = tmp[i].trim();
						}
                        dockindQuery.InStringField(field, tmp);
                        fieldValues.put(field.getCn(), tmp);
                	}
                	else
                	{
                		String value = (String) field.simpleCoerce(values.get(field.getCn()));
                        dockindQuery.stringField(field, value);
                        fieldValues.put(field.getCn(), value);
                	}
                        
                }
                else if (Field.BOOL.equals(field.getType()))
                {
                    String value = (String) values.get(field.getCn());
                    Boolean bool = (Boolean) field.simpleCoerce(value);
                    dockindQuery.boolField(field, bool);
                    fieldValues.put(field.getCn(), bool);
                }
//                else if (Field.FLOAT.equals(field.getType()))
                else if(field instanceof FloatField)
                {
                    String value = (String) values.get(field.getCn());
                    Float fl = (Float) field.simpleCoerce(value);
                    dockindQuery.field(field, fl);
                    fieldValues.put(field.getCn(), fl);
                }
//                else if (Field.DOUBLE.equals(field.getType()))
                else if (field instanceof DoubleField)
                {
                    String value = (String) values.get(field.getCn());
                    Double fl = (Double) field.simpleCoerce(value);
                    dockindQuery.field(field, fl);
                    fieldValues.put(field.getCn(), fl);
                }
                    else if (Field.CLASS.equals(field.getType()))
                    {
                        String value = null;
                        String[] multiClass = null;
                    	if(invalues.get(field.getCn()) != null)
                    	{
                    		value = (String)invalues.get(field.getCn());
                    		String[] tmp = value.split("\n");
                    		for (int i = 0; i < tmp.length; i++)
                    		{
                    			tmp[i] = tmp[i].trim();
							}
                            dockindQuery.classField(field, tmp,values.get(field.getCn()+"_attrName").toString());
                    	}
                    	else
                    	{
	                        try
	                        {
	                        	multiClass = (String[]) values.get(field.getCn());
	                        }
	                        catch (ClassCastException e) 
							{
	                        	value = (String) values.get(field.getCn());
							}
	                        if(multiClass != null)
	                        {
	                        	dockindQuery.InField(field, multiClass);
	                        }
	                        else if(value != null)
	                        {
	                        	dockindQuery.field(field, value);
	                        }
	                        else
	                        {
		                        for (String property : field.getDictionaryAttributes().keySet())
		                        {
		                            value = (String) values.get(field.getCn()+"_"+property);
		                            if((property.equals("nip") || property.equals("prettyNip")) && (kind.getCn().equals(DocumentLogicLoader.INVOICE_KIND) || kind.getCn().equals(DocumentLogicLoader.INVOICE_IC_KIND) || kind.getCn().equals(DocumentLogicLoader.SAD_KIND))){
		                            	value = DicInvoice.cleanNip(value);
										property = "nip";
									}
		                            
		                            dockindQuery.otherClassField(field, property, value);
		                            fieldValues.put(field.getCn()+"_"+property, value);
		                        }
	                        }
                    	}
                    }
                    else if (Field.CENTRUM_KOSZT.equals(field.getType()))
                    {                    	
                    	String value = (String) values.get(field.getCn());                    	
                    	if(value != null)
                    	{
                    		Integer integ = Integer.parseInt(value);
                    		dockindQuery.centrumKosztowField(field, integ);
                    		fieldValues.put(field.getCn(), integ);
                    	}
                    }
                    else
                    {
                        Object value = field.simpleCoerce(values.get(field.getCn()));
                        if (value instanceof Object[])
                        {
                            Object[] array = (Object[]) value;
                            if (array.length == 0)
                                value = null;
                            else
                                value = array[0];
                        }
                        if (value != null)
                        {
                            dockindQuery.field(field,value);
                            fieldValues.put(field.getCn(),value);
                        }
                    }
            }
            if(values.get("KONTA_KOSZTOWE") != null)
            {
            	String value = (String) values.get("KONTA_KOSZTOWE");                    	
            	if(value != null)
            	{
            		dockindQuery.kontoKosztoweField(value);
            		fieldValues.put("KONTA_KOSZTOWE", value);
            	}
            }
        
            dockindQuery.orderAsc(sortField);
            

            return DocumentKindsManager.searchIdForUser(dockindQuery, this.getUsername());
	}
	
	
	@SuppressWarnings("unchecked")
	public void fillDocumentsIds() throws Exception
	{
		Map<String,Object> values = new HashMap<String, Object>();
		Map<String,Object> invalues = new HashMap<String, Object>();
		Map<String,Object> elseMap = new HashMap<String, Object>();
		
		for(ReportParameter rp: params) 
		{
			ByteArrayInputStream b = null;
			ObjectInputStream o = null;
			
			if(rp.getFieldCn().equalsIgnoreCase("VALUES"))
			{
				b = new ByteArrayInputStream(Base64.decodeBase64((String) rp.getValue()));
				o = new ObjectInputStream(b);
				values = (Map<String,Object>) o.readObject();
			}
			if(rp.getFieldCn().equalsIgnoreCase("INVALUES"))
			{
				b = new ByteArrayInputStream(Base64.decodeBase64((String) rp.getValue()));
				o = new ObjectInputStream(b);
				invalues = (Map<String,Object>) o.readObject();
			}
			if(rp.getFieldCn().equalsIgnoreCase("ELSEMAP"))
			{
				b = new ByteArrayInputStream(Base64.decodeBase64((String) rp.getValue()));
				o = new ObjectInputStream(b);
				elseMap = (Map<String,Object>) o.readObject();				
			}
			o.close();
			b.close();
		}
		
		documentsIds = search(values, invalues, elseMap);
	}
	
	
	@Override
	public void doReport() throws Exception 
	{
		LoggerFactory.getLogger("tomekl").debug("doReport ");
		fillDocumentsIds();
		
		LoggerFactory.getLogger("tomekl").debug("count "+documentsIds.size());
		
		Map<String, String> columns = new LinkedHashMap<String, String>();
		
		columns.put("document.id", "ID");
		columns.put("document.author", sm.getString("Autor"));
		columns.put("document.title", sm.getString("Tytul"));
		columns.put("document.description", sm.getString("Opis"));
		columns.put("document.ctime", sm.getString("DataUtworzenia"));
        columns.put("document.mtime", sm.getString("DataModyfikacji"));
		
        Map<String, Field> enums = new LinkedHashMap<String, Field>();
        
		
		String cols = DSApi.context().userPreferences(this.getUsername()).node("search-documents").get("columns_"+DocumentLogicLoader.ALD_KIND,"null");
		for(Field fd : DocumentKind.findByCn(DocumentLogicLoader.ALD_KIND).getFields())
        {
	 		  if(cols.indexOf(fd.getCn())>0)
	 		  {
	 			  if(fd.getType().equals("enum"))
	 			  {
	 				enums.put(fd.getColumn(), fd);  
	 			  }
	 			  
	 			  columns.put("dockind."+fd.getColumn(), fd.getName());
	 		  }
        }
		
		
		List<List<Long>> idsParts = new ArrayList<List<Long>>();
		for(int i = 0; i < documentsIds.size() ; i=i+MAX_IDS)
		{
			try
			{
				idsParts.add(documentsIds.subList(i, i+MAX_IDS));
			}
			catch (IndexOutOfBoundsException e) 
			{
				idsParts.add(documentsIds.subList(i, documentsIds.size()));
			}
			
		}
		
		
		dumper = new UniversalTableDumper(); 
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "raport.xls");
		poiDumper.openFile(xlsFile);
		dumper.addDumper(poiDumper);
		
		dumper.newLine();
		for(String str : columns.keySet())
			dumper.addText(columns.get(str));
		dumper.dumpLine();
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select ");
		sbQuery.append(" "+columns.keySet().toString().split("\\[")[1].split("\\]")[0]+" ");
		sbQuery.append(" from DS_DOCUMENT document with (nolock) ");
		sbQuery.append(" JOIN DSG_ALD_DOCKIND dockind on document.ID = dockind.DOCUMENT_ID ");
		sbQuery.append(" where document.ID in ( ");
		
		String halfSQL = sbQuery.toString();
		
		PreparedStatement ps = null;
		for(List<Long> list : idsParts)
		{
			String atmSQL = halfSQL + list.toString().split("\\[")[1].split("\\]")[0]+" )";
			try
			{
				ps = DSApi.context().prepareStatement(atmSQL);
				ResultSet rs = ps.executeQuery();
				while(rs.next())
				{
					dumper.newLine();

					for(int i=1; i <= rs.getMetaData().getColumnCount(); i++)
					{
						if(rs.getObject(i) == null)
						{
							dumper.addText("");
						}
						else if(enums.containsKey(rs.getMetaData().getColumnName(i)))
						{
							dumper.addText(""+enums.get(rs.getMetaData().getColumnName(i)).getEnumItem(rs.getInt(i)).getTitle());
						}
						else
						{
							dumper.addText(""+rs.getObject(i));
						}
					}
					
					dumper.dumpLine();
					
				}
				rs.close();
			}
			catch (Exception e) 
			{
				log.debug(e.getMessage(),e);
				e.printStackTrace();
			}
			finally
			{
				DSApi.context().closeStatement(ps);
			}
		}
		dumper.closeFileQuietly();
		LoggerFactory.getLogger("tomekl").debug("doneReport ");
	}

}
