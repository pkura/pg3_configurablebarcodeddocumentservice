package pl.compan.docusafe.parametrization.ald.reports;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.CsvDumper;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.DateUtils;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.Date;

/**
 * Raport wg zg�oszenia 3292:
 *  Wykaz akceptacji merytorycznych.
 *
 * Raport b�dzie generowany dla zakresu czasowego ograniczaj�cego dat� wp�yni�cia dokumentu (data od-do, w��cznie) i dla grupy u�ytkownik�w (wyb�r z listy wszystkich u�ytkownik�w systemu).
 *
 * Rezultatem raportu b�d� pliki xls i csv, zawieraj�ce nast�puj�ce dane w kolejnych kolumnach:
 * - ID systemowe dokumentu
 * - Numer faktury
 * - U�ytkownik wykonuj�cy akceptacj� merytoryczn�
 * - Data akceptacji
 * - Status dokumentu w momencie wykonywania raportu
 * - Data wp�yni�cia do systemu
 * - Opis pisma
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class AldAcceptancesReport extends Report {

    private final static String LABEL_QUERY = "SELECT am.id AS mid, ak.id as kid, af.id as fid\n" +
                                            "  FROM dso_label am, dso_label ak, dso_label af\n" +
                                            "  WHERE am.description = 'Akceptacja merytoryczna'\n" +
                                            "  AND ak.description = 'Akceptacja ksi�gowa'\n" +
                                            "  AND af.description = 'Akceptacja finansowa'";

    private final static String QUERY =
            "SELECT ald.document_id AS document_id, ald.status AS status, ald.nr_faktury AS number, ald.data AS date,\n" +
            " doc.DESCRIPTION as description, mdto.username AS username, mdto.ctime AS mtime, adto.ctime AS atime, fdto.ctime AS ftime\n" +
            "FROM dsg_ald_dockind ald \n" +
            "INNER JOIN dso_document_to_label mdto\n" +
            "ON mdto.document_id = ald.DOCUMENT_ID\n" +
            "INNER JOIN ds_document doc\n" +
            "ON doc.ID = ald.DOCUMENT_ID\n" +
            "LEFT JOIN dso_document_to_label fdto\n" +
            "ON fdto.document_id = ald.DOCUMENT_ID\n" +
            "LEFT JOIN dso_document_to_label adto\n" +
            "ON adto.document_id = ald.DOCUMENT_ID\n" +
            "WHERE ald.data >= ? and ald.data <= ? \n" + //1 from, 2 to
            "AND mdto.label_id = ?\n" + //3 mlabelId
            "AND (adto.label_id = ? OR adto.label_id IS NULL)\n" + //4 aLabelId
            "AND (fdto.label_id = ? OR fdto.label_id IS NULL)\n"; //5 fLabelId

    UniversalTableDumper dumper = null;

    /** id labela od akceptacji ksi�gowej */
    int aLabelId;
    /** id labela od akceptacji merytorycznej */
    int mLabelId;
    /** id labela od akceptacji finansowej */
    int fLabelId;

    Date from;
    Date to;
    String[] usernames = new String[]{};
    Field statusField;

    @Override
    public void doReport() throws Exception {
        initDumpers();
        try {
            initParams();
            dumpHeader();
            initLabelIds();
            initFields();
            createReport();
        } finally {
            dumper.closeFileQuietly();
        }
    }

    private void initFields() throws Exception{
        statusField = DocumentKind.findByCn("ald").getFieldByCn("STATUS");
    }

    private void createReport() throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String usernameClause = createUsernameClause();
            ps = DSApi.context().prepareStatement(QUERY + usernameClause);
            ps.setDate(1, new java.sql.Date(from.getTime()));
            ps.setDate(2, new java.sql.Date(to.getTime()));
            ps.setInt(3, mLabelId);
            ps.setInt(4, aLabelId);
            ps.setInt(5, fLabelId);
            for(int i=0; i < usernames.length; ++i){
                ps.setString(6 + i, usernames[i]);
            }
            rs = ps.executeQuery();
            while(rs.next()){
                dumpRow(rs);
            }
        } finally {
            DbUtils.closeQuietly(rs);
            DSApi.context().closeStatement(ps);
        }
    }

    private String createUsernameClause() {
        if(usernames.length == 0){
            return "";
        } else {
            StringBuilder sb = new StringBuilder("\nAND mdto.username IN (");
            for(int i = 0; i < usernames.length; ++ i){
                sb.append("?,");
            }
            sb.deleteCharAt(sb.length()-1)
              .append(')');
            return sb.toString();
        }
    }

    private void dumpRow(ResultSet rs) throws Exception{
        dumper.newLine();
        dumper.addText(rs.getString("document_id"));
        dumper.addText(rs.getString("number"));
        dumper.addText(DateUtils.formatJsDateTime(rs.getTimestamp("mtime")));
        dumper.addText(DSUser.findByUsername(rs.getString("username")).asLastnameFirstname());
        dumper.addText(DateUtils.formatJsDateTime(rs.getTimestamp("atime")));
        dumper.addText(DateUtils.formatJsDateTime(rs.getTimestamp("ftime")));
        dumper.addText(statusField.getEnumItem(rs.getInt("status")).getTitle());
        dumper.addText(DateUtils.formatJsDate(rs.getDate("date")));
        dumper.addText(rs.getString("description"));
        dumper.dumpLine();
    }

    void dumpHeader() throws IOException {
		dumper.newLine();
		dumper.addText("ID Dokumentu");
		dumper.addText("Numer faktury");
        dumper.addText("Akceptacja merytoryczna");
		dumper.addText("U�ytkownik");
        dumper.addText("Akceptacja ksi�gowa");
        dumper.addText("Akceptacja finansowa");
		dumper.addText("Status");
		dumper.addText("Data wplyni�cia");
		dumper.addText("Opis");
		dumper.dumpLine();
	}

    void initDumpers() throws Exception {
		dumper = new UniversalTableDumper();
		CsvDumper csvDumper = new CsvDumper();
		File csvFile = new File(this.getDestination(), "raport_"+DateUtils.formatJsDate(new java.util.Date())+".csv");
		csvDumper.openFile(csvFile);
		dumper.addDumper(csvDumper);

		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "raport_"+DateUtils.formatJsDate(new java.util.Date())+".xls");
		poiDumper.openFile(xlsFile);
		dumper.addDumper(poiDumper);
	}

    private void initLabelIds() throws Exception{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DSApi.context().prepareStatement(LABEL_QUERY);
            rs = ps.executeQuery();
            if(rs.next()){
                aLabelId = rs.getInt("kid");
                mLabelId = rs.getInt("mid");
                fLabelId = rs.getInt("fid");
            } else {
                throw new IllegalStateException("brak wymaganych etykiet!");
            }
        } finally {
            DbUtils.closeQuietly(rs);
            DSApi.context().closeStatement(ps);
        }
    }

    void initParams() {
        for(ReportParameter rp : getParams()){
            if(rp.getFieldCn().equals("FROM")){
                from = DateUtils.parseJsDate(rp.getValue().toString());
            } else if (rp.getFieldCn().equals("TO")) {
                to = DateUtils.parseJsDate(rp.getValue().toString());
            } else if (rp.getFieldCn().equals("USER")) {
                if(rp.getValue() instanceof String) {
					usernames = new String[]{rp.getValue().toString()};
                } else if (rp.getValue() instanceof String[]) {
					usernames = (String[]) rp.getValue();
                }
            }
        }
    }
}
