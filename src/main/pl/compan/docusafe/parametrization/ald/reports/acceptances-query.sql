SELECT ald.document_id AS document_id, ald.status AS status, ald.nr_faktury AS number, ald.data AS date, doc.DESCRIPTION as description, mdto.username AS username, mdto.ctime AS mtime, adto.ctime AS atime, fdto.ctime AS ftime
FROM dsg_ald_dockind ald
INNER JOIN dso_document_to_label mdto
ON mdto.document_id = ald.DOCUMENT_ID
INNER JOIN ds_document doc
ON doc.ID = ald.DOCUMENT_ID
LEFT JOIN dso_document_to_label fdto
ON fdto.document_id = ald.DOCUMENT_ID
LEFT JOIN dso_document_to_label adto
ON adto.document_id = ald.DOCUMENT_ID
WHERE ald.data > '2001-07-02 00:00:00.000' and ald.data < '2017-07-02 00:00:00.000'
AND mdto.label_id = 3
AND (adto.label_id = 1 OR adto.label_id IS NULL)
AND (fdto.label_id = 2 OR fdto.label_id IS NULL)