package pl.compan.docusafe.parametrization.ald;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;

/**
 * Informacja o por�wnaniu faktury wczytanej z pliku CSV i odpowiadaj�cego jej dokumentowi w systemie
 * (na potrzeby weryfikacji rejestru VAT).
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class ALDInvoiceInfo2
{
    public static Integer STATUS_OK = 1;
    public static Integer STATUS_NOT_FOUND = 0;    
    public static Integer STATUS_NOT_EVERYTHING_MATCH = 3;
    public static Integer STATUS_TOO_MANY_FOUND = 4;
    public static Integer STATUS_PARSE_ERROR = 5; 
    
    private Long id;
    private Integer lineNumber;
    private String bookCode;
    private String nrKsiegowania;
    private String invoiceNumber;
    private Float amount;
    /**
     * kwota jaka jest zapisana w bazie dla danego dokumentu
     */
    private Float origAmount;
    private Date ctime;
    private String creatingUser;
    private Integer status;
    private String errorInfo;
    private Long documentId;
    
    public ALDInvoiceInfo2()
    {
        
    }
    
    @SuppressWarnings("unchecked")
    public static List<ALDInvoiceInfo2> find(String username, String sortField, boolean asc) throws EdmException
    {        
        Criteria c = DSApi.context().session().createCriteria(ALDInvoiceInfo2.class);
        c.add(Restrictions.eq("creatingUser", username));
        if (sortField == null)
        {
            sortField = "lineNumber";
            asc = true;
        }        
        c.addOrder(asc ? Order.asc(sortField) : Order.desc(sortField));
        if ("bookCode".equals(sortField))
        {
            c.addOrder(asc ? Order.asc("nrKsiegowania") : Order.desc("nrKsiegowania"));
        }
        return (List<ALDInvoiceInfo2>) c.list();
    }
    
    public static ALDInvoiceInfo2 find(Long id) throws EdmException
    {
        return Finder.find(ALDInvoiceInfo2.class, id);
    }
    
    public Float getAmount()
    {
        return amount;
    }

    public void setAmount(Float amount)
    {
        this.amount = amount;
    }

    public String getBookCode()
    {
        return bookCode;
    }

    public void setBookCode(String bookCode)
    {
        this.bookCode = bookCode;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getInvoiceNumber()
    {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber)
    {
        this.invoiceNumber = invoiceNumber;
    }

    public String getNrKsiegowania()
    {
        return nrKsiegowania;
    }

    public void setNrKsiegowania(String nrKsiegowania)
    {
        this.nrKsiegowania = nrKsiegowania;
    }

    public Date getCtime()
    {
        return ctime;
    }

    public void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }

    public String getCreatingUser()
    {
        return creatingUser;
    }

    public void setCreatingUser(String creatingUser)
    {
        this.creatingUser = creatingUser;
    }

    public Integer getStatus()
    {
        return status;
    }
    
    public String getStatusDescription()
    {
        if (STATUS_OK.equals(status))
            return "OK";
        else if (STATUS_NOT_FOUND.equals(status))
            return "nie znaleziono dokumentu";
        else if (STATUS_TOO_MANY_FOUND.equals(status))
            return "znaleziono wi�cej ni� 1 dokument";
        else if (STATUS_NOT_EVERYTHING_MATCH.equals(status))
            return "niezgodno�� kwoty";
        else if (STATUS_PARSE_ERROR.equals(status))
            return "b��d w pliku - "+errorInfo;
        else
            return null;
    }

    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public Integer getLineNumber()
    {
        return lineNumber;
    }

    public void setLineNumber(Integer lineNumber)
    {
        this.lineNumber = lineNumber;
    }

    public String getErrorInfo()
    {
        return errorInfo;
    }

    public void setErrorInfo(String errorInfo)
    {
        this.errorInfo = errorInfo;
    }

    public Float getOrigAmount()
    {
        return origAmount;
    }

    public void setOrigAmount(Float origAmount)
    {
        this.origAmount = origAmount;
    }
}
