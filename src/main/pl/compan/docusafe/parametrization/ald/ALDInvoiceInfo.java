package pl.compan.docusafe.parametrization.ald;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;

/**
 * Informacja o por�wnaniu faktury wczytanej z pliku CSV i odpowiadaj�cego jej dokumentowi w systemie
 * (na potrzeby weryfikacji p�atno�ci faktur).
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class ALDInvoiceInfo
{
    public static Integer STATUS_OK = 1;
    public static Integer STATUS_NOT_FOUND = 0;    
    public static Integer STATUS_ALREADY_PAID = 2;
    public static Integer STATUS_NOT_EVERYTHING_MATCH = 3;
    public static Integer STATUS_TOO_MANY_FOUND = 4;
    public static Integer STATUS_PARSE_ERROR = 5; 
    public static Integer STATUS_PAID = 10;
    
    private Long id;
    private Integer lineNumber;
    private String bookCode;
    private String nrKsiegowania;
    private String supplierNumber;
    private String invoiceNumber;
    private String nip;             // obecnie nieu�ywane
    private Float amount;
    /**
     * kwota jaka jest zapisana w bazie dla danego dokumentu
     */
    private Float origAmount;
    private Date ctime;
    private String creatingUser;
    private Integer status;
    private String errorInfo;
    private Boolean initiallyPaid;
    private Long documentId;
    private Date paymentDate;
    
    public ALDInvoiceInfo()
    {
        
    }
    
    @SuppressWarnings("unchecked")
    public static List<ALDInvoiceInfo> find(String username, String sortField, boolean asc) throws EdmException
    {        
        Criteria c = DSApi.context().session().createCriteria(ALDInvoiceInfo.class);
        c.add(Restrictions.eq("creatingUser", username));
        if (sortField == null)
        {
            sortField = "supplierNumber";
            asc = true;
        }        
        c.addOrder(asc ? Order.asc(sortField) : Order.desc(sortField));
        if ("bookCode".equals(sortField))
        {
            c.addOrder(asc ? Order.asc("nrKsiegowania") : Order.desc("nrKsiegowania"));
        }
        return (List<ALDInvoiceInfo>) c.list();
    }
    
    public static ALDInvoiceInfo find(Long id) throws EdmException
    {
        return Finder.find(ALDInvoiceInfo.class, id);
    }
    
    public Float getAmount()
    {
        return amount;
    }

    public void setAmount(Float amount)
    {
        this.amount = amount;
    }

    public String getBookCode()
    {
        return bookCode;
    }

    public void setBookCode(String bookCode)
    {
        this.bookCode = bookCode;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getInvoiceNumber()
    {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber)
    {
        this.invoiceNumber = invoiceNumber;
    }

    public String getNip()
    {
        return nip;
    }

    public void setNip(String nip)
    {
        this.nip = nip;
    }

    public String getNrKsiegowania()
    {
        return nrKsiegowania;
    }

    public void setNrKsiegowania(String nrKsiegowania)
    {
        this.nrKsiegowania = nrKsiegowania;
    }

    public String getSupplierNumber()
    {
        return supplierNumber;
    }

    public void setSupplierNumber(String supplierNumber)
    {
        this.supplierNumber = supplierNumber;
    }

    public Date getCtime()
    {
        return ctime;
    }

    public void setCtime(Date ctime)
    {
        this.ctime = ctime;
    }

    public String getCreatingUser()
    {
        return creatingUser;
    }

    public void setCreatingUser(String creatingUser)
    {
        this.creatingUser = creatingUser;
    }

    public Boolean getInitiallyPaid()
    {
        return initiallyPaid;
    }

    public void setInitiallyPaid(Boolean initiallyPaid)
    {
        this.initiallyPaid = initiallyPaid;
    }

    public Integer getStatus()
    {
        return status;
    }
    
    public String getStatusDescription()
    {
        if (STATUS_OK.equals(status))
            return "niezap�acone";
        else if (STATUS_ALREADY_PAID.equals(status))
            return "zap�acone";
        else if (STATUS_NOT_FOUND.equals(status))
            return "nie znaleziono dokumentu";
        else if (STATUS_TOO_MANY_FOUND.equals(status))
            return "znaleziono wi�cej ni� 1 dokument";
        else if (STATUS_NOT_EVERYTHING_MATCH.equals(status))
            return "niezgodno�� kwoty";
        else if (STATUS_PARSE_ERROR.equals(status))
            return "b��d w pliku - "+errorInfo;
        else if (STATUS_PAID.equals(status))
            return "zap�acone";
        else
            return null;
    }

    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public Integer getLineNumber()
    {
        return lineNumber;
    }

    public void setLineNumber(Integer lineNumber)
    {
        this.lineNumber = lineNumber;
    }

    public String getErrorInfo()
    {
        return errorInfo;
    }

    public void setErrorInfo(String errorInfo)
    {
        this.errorInfo = errorInfo;
    }

    public Date getPaymentDate()
    {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate)
    {
        this.paymentDate = paymentDate;
    }        
    
    public boolean isPaid()
    {
        return (initiallyPaid != null && initiallyPaid) || STATUS_PAID.equals(status);
    }

    public Float getOrigAmount()
    {
        return origAmount;
    }

    public void setOrigAmount(Float origAmount)
    {
        this.origAmount = origAmount;
    }
}
