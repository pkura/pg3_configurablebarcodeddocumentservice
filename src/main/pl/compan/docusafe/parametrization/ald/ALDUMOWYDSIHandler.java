package pl.compan.docusafe.parametrization.ald;

import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.service.imports.dsi.DSIImportHandler;

public class ALDUMOWYDSIHandler extends DSIImportHandler 
{
		
	public String getDockindCn()
	{
		return DocumentLogicLoader.ALD_UMOWY_KIND;
	}
	protected void prepareImport() throws Exception 
	{
		
	}
	
	@Override
	protected boolean isStartProces()
	{
		return true;
	}

}
