package pl.compan.docusafe.parametrization.ald;

import java.util.Map;

import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.util.StringManager;

public class ALDUmowyLogic extends AbstractDocumentLogic {

	public static final String KATEGORIA_FIELD_CN = "KATEGORIA";
	public static final String OPIS_CN = "OPIS_GL";
	public static final String NR_KLIENTA_CN = "NR_KLIENTA";
	public static final String NR_KONTRAKTU_CN = "NR_KONTRAKTU";
	public static final String NR_REJESTRACYJNY_CN = "NR_REJESTRACYJNY";
	public static final String NAZWA_KLIENTA_CN = "NAZWA_KLIENTA";
	
	public static final Integer UMOWY_INDYWIDUALNE_ID = 20;
	
	private static ALDUmowyLogic instance;
	private StringManager sm = GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(), null);

    public static synchronized ALDUmowyLogic getInstance()
    {
        if (instance == null)
            instance = new ALDUmowyLogic();
        return instance;
    }


    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {

		if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");
        
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		
		Folder folder = Folder.getRootFolder();
    	folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
    	
		String nazwaKlienta = (String) fm.getValue(NAZWA_KLIENTA_CN);
		String nazwaKlientaFirst = nazwaKlienta.substring(0, 1);
		
		//foldery po pierwszej literze nazwy klienta
		if(nazwaKlientaFirst.matches("[a-dA-D]"))
		{
			//LoggerFactory.getLogger("tomekl").debug("[a-dA-D]");
			folder = folder.createSubfolderIfNotPresent("A-D");
		}
		else if(nazwaKlientaFirst.matches("[e-iE-I]"))
		{
			//LoggerFactory.getLogger("tomekl").debug("[e-iE-I]");
			folder = folder.createSubfolderIfNotPresent("E-I");
		}
		else if(nazwaKlientaFirst.matches("[j-oJ-O]"))
		{
			//LoggerFactory.getLogger("tomekl").debug("[j-oJ-O]");
			folder = folder.createSubfolderIfNotPresent("J-O");
		}
		else if(nazwaKlientaFirst.matches("[p-tP-T]"))
		{
			//LoggerFactory.getLogger("tomekl").debug("[p-tP-T]");
			folder = folder.createSubfolderIfNotPresent("P-T");
		}
		else if(nazwaKlientaFirst.matches("[u-zU-Z]"))
		{
			//LoggerFactory.getLogger("tomekl").debug("[u-zU-Z]");
			folder = folder.createSubfolderIfNotPresent("U-Z");
		}
		
		//folder z nazwa klienta i numer klienta
		StringBuffer sb = new StringBuffer();
		sb.append(nazwaKlienta);
		sb.append(" (");
		sb.append(fm.getStringValue(NR_KLIENTA_CN));
		sb.append(")");
		folder = folder.createSubfolderIfNotPresent(sb.toString());
		
		StringBuffer title = new StringBuffer();
		
		if(UMOWY_INDYWIDUALNE_ID.equals((Integer) fm.getKey(KATEGORIA_FIELD_CN)))
		{
			//folder z umerem kontraktu i umerem rejestracyjnym
			sb = new StringBuffer();
			sb.append(fm.getStringValue(NR_KONTRAKTU_CN));
			sb.append(" (");
			sb.append(fm.getStringValue(NR_REJESTRACYJNY_CN));
			sb.append(")");
			folder = folder.createSubfolderIfNotPresent(sb.toString());
			
			//tytul
			title.append(fm.getStringValue(OPIS_CN));
		}
		else
		{
			title.append("Umowa generalna (");
			title.append(fm.getStringValue(OPIS_CN));
			title.append(")");
		}
		
		document.setTitle(title.toString());
    	
		document.setDescription((String) fm.getField(OPIS_CN).getEnumItem((Integer) fm.getKey(OPIS_CN)).getArg(1));
    	
    	document.setFolder(folder);
	}

	public void documentPermissions(Document document) throws EdmException {
		 if (document.getDocumentKind() == null)
	            throw new NullPointerException("document.getDocumentKind()");
		 
		 DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "*", ObjectPermission.ANY);
	}

	@Override
	public boolean searchCheckPermissions(Map<String, Object> values) {
		return false;
	}

}
