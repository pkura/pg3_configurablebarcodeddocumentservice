package pl.compan.docusafe.parametrization.ald;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Persister;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmSQLException;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.web.viewserver.ViewServer;


/**
 * Menad�er udost�pniaj�cy funkcje specyficzne dla dokument�w rodzaju {@link DocumentLogicLoader#ALD_KIND}.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class ALDManager
{
	static final Logger log = LoggerFactory.getLogger(ALDManager.class);
    /**
     * Kontrola p�atno�ci faktur.
     * Z pliku CSV wczytywane wierszami informacje o kolejnych fakturach i zapisywane do bazy.
     * Od razu nast�puje przetwarzanie danych - ustawienie odpowiedniego status b��du.
     * Zwracana jest lista utworzonych obiekt�w {@link ALDInvoiceInfo}.
     * 
     * UWAGA: Obecnie pole "nip" nieu�ywane (ale nie usun��em go, bo mo�e w przysz�o�ci si� przyda�).
     */
    public static List<ALDInvoiceInfo> parseAndSavePaymentFile(File file) throws EdmException
    {
        List<ALDInvoiceInfo> list = new ArrayList<ALDInvoiceInfo>();
        BufferedReader reader = null;
        try
        {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "cp1250"));
            String line;
            int lineCount = 0;
            while ((line = reader.readLine()) != null)
            {
                lineCount++;
                line = line.trim();
                // pierwsza linia to nag��wek, kt�ry omijam
                if (lineCount == 1 || line.startsWith("#"))
                    continue;
                
                ALDInvoiceInfo info = new ALDInvoiceInfo();
                info.setCtime(new Date());
                info.setCreatingUser(DSApi.context().getPrincipalName());
                info.setLineNumber(lineCount);
                
                String[] parts = line.split("\\s*;\\s*");
                if (parts.length > 0)
                    info.setNrKsiegowania(parts[0].replace("\"", "").trim());
                
                
                if (parts.length > 1)
                    info.setBookCode(parts[1].trim());
                
                
                if (parts.length > 2)
                    info.setSupplierNumber(parts[2].trim());
                
                
                if (parts.length > 3)
                    info.setInvoiceNumber(parts[3].trim());
                
                
                if (parts.length > 4)
                {
                    String amount = parts[4].trim();
                    amount = amount.replace(".", "");
                    amount = amount.replace(",", ".");
                    amount = amount.replace("\"", "");
                    try
                    {
                        info.setAmount(Float.parseFloat(amount));
                    }
                    catch (NumberFormatException e)
                    {
                        info.setStatus(ALDInvoiceInfo.STATUS_PARSE_ERROR);
                        info.setErrorInfo("niepoprawna kwota");
                    }
                }
                if (parts.length > 5)
                    info.setNip(parts[5].trim());
                if (parts.length < 5)
                {
                    info.setStatus(ALDInvoiceInfo.STATUS_PARSE_ERROR);
                    info.setErrorInfo("nie podano wszystkich informacji");
                }
                
                list.add(info);
                
                if (info.getStatus() == null)                
                {
                    // poprawnie odczytano z pliku - szukamy odpowiedni dokument
                    DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.ALD_KIND);
                    DockindQuery query = new DockindQuery(0,0);
                    query.setDocumentKind(kind);
                    query.field(kind.getFieldByCn(ALDLogic.BOOK_CODE_FIELD_CN), info.getBookCode());
                    query.field(kind.getFieldByCn(ALDLogic.NR_KSIEGOWANIA_FIELD_CN), info.getNrKsiegowania());
                    SearchResults<Document> searchResults = DocumentKindsManager.search(query);
                    if (searchResults.totalCount() == 0)
                        info.setStatus(ALDInvoiceInfo.STATUS_NOT_FOUND);                                                                       
                    else
                    {
                        Document[] documents = searchResults.results();
                        checkDocument(documents[0], info);
                        
                        if (documents.length > 1)
                        {
                            info.setStatus(ALDInvoiceInfo.STATUS_TOO_MANY_FOUND);
                            for (int i=1; i < documents.length; i++)
                            {
                                ALDInvoiceInfo copy = new ALDInvoiceInfo();
                                copy.setAmount(info.getAmount());
                                copy.setBookCode(info.getBookCode());
                                copy.setCreatingUser(info.getCreatingUser());
                                copy.setCtime(info.getCtime());
                                copy.setInvoiceNumber(info.getInvoiceNumber());
                                copy.setNrKsiegowania(info.getNrKsiegowania());
                                copy.setNip(info.getNip());
                                copy.setLineNumber(info.getLineNumber());
                                copy.setSupplierNumber(info.getSupplierNumber());                                
                                checkDocument(documents[i], copy);
                                copy.setStatus(ALDInvoiceInfo.STATUS_TOO_MANY_FOUND);
                                Persister.create(copy);
                                list.add(copy);
                            }                            
                        }
                    }
                }
                
                Persister.create(info);
            }
            return list;
        }
        catch (IOException e)
        {
            throw new EdmException("B��d podczas czytania pliku z p�atno�ciami faktur");
        }
        finally
        {
        	org.apache.commons.io.IOUtils.closeQuietly(reader);
        }
    }
    
    /**
     * Sprawdza zgodno�� danego dokumentu z utworzonym obiektem {@link ALDInvoiceInfo} na potrzeby 
     * kontroli p�atno�ci faktur.
     * Na koniec ustala odpowiedni status w tym obiekcie (tak�e pewne inne rzeczy np. 
     * dat� p�atno�ci).
     */
    private static void checkDocument(Document document, ALDInvoiceInfo info) throws EdmException
    {
        info.setDocumentId(document.getId());
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        //String nip = (String) fm.getKey(ALDLogic.NIP_DOSTAWCY_FIELD_CN);
        Float amount = (Float) fm.getKey(ALDLogic.KWOTA_BRUTTO_FIELD_CN);
        Boolean paid = (Boolean) fm.getKey(ALDLogic.ZAPLACONY_FIELD_CN);
        
        info.setPaymentDate((Date) fm.getValue(ALDLogic.DATA_PLATNOSCI_FIELD_CN));
        info.setOrigAmount(amount);
        if (paid == null)
            paid = Boolean.FALSE;
        info.setInitiallyPaid(paid);
        if (/*info.getNip() == null || !info.getNip().equals(nip) ||*/ !info.getAmount().equals(amount))
            info.setStatus(ALDInvoiceInfo.STATUS_NOT_EVERYTHING_MATCH);
        else if (paid)
            info.setStatus(ALDInvoiceInfo.STATUS_ALREADY_PAID);
        else
            info.setStatus(ALDInvoiceInfo.STATUS_OK);   
        
        // uaktualniam numer dostawcy w dokumencie na podstawie wczytanego z pliku
        Map<String,Object> fieldValues = new LinkedHashMap<String, Object>();
        fieldValues.put(ALDLogic.NUMER_DOSTAWCY_FIELD_CN, info.getSupplierNumber());
        document.getDocumentKind().setWithHistory(document.getId(), fieldValues,false);
    }        
    
    /**
     * Kasowanie z bazy wszystkich obiekt�w {@link ALDInvoiceInfo} utworzonych przez
     * podanego u�ytkownika. 
     * Powinno by� wo�ane w momencie �adowanie przez tego u�ytkownika nowego pliku!
     */
    public static void deleteInvoiceInfos(String username) throws EdmException
    {
        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().prepareStatement("delete from DS_ALD_INVOICE_INFO where creatingUser = ?");
            ps.setString(1, username);
            ps.executeUpdate();            
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);        	
        }
    }
    
    /**
     * Oznaczanie podanych dokument�w jako zap�acone. 
     * 
     * @param payments mapa, gdzie klucze okre�laj� dokumenty, kt�re maj� by� zap�acone. 
     * Klucz jest postaci: docXXXX;YYYY gdzie XXXX to identyfikator dokumentu, a YYYY to
     * identyfikator obiektu {@link ALDInvoiceInfo}.
     */
    public static void executePayments(Map<String,Boolean> payments, Date data) throws EdmException
    {
        DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.ALD_KIND);
        for (String key : payments.keySet())
        {
            // parsujemy klucz
            String[] parts = key.split("_");
            Long documentId = Long.parseLong(parts[0].substring(3));
            Long infoId = Long.parseLong(parts[1]);
            Map<String,Object> fieldValues = new HashMap<String, Object>();
            // zapisujemy w bazie, �e zap�acone
            fieldValues.put(ALDLogic.ZAPLACONY_FIELD_CN, Boolean.TRUE);
            fieldValues.put(ALDLogic.DATA_PLATNOSCI_FIELD_CN, data);
            kind.setOnly(documentId, fieldValues);
            // uaktualniamy 'info'
            ALDInvoiceInfo info = ALDInvoiceInfo.find(infoId);
            info.setStatus(ALDInvoiceInfo.STATUS_PAID);
            info.setPaymentDate(data);            
        }
    }
    
    public static List<Map<String,Object>> prepareBeans(List<ALDInvoiceInfo> invoiceInfos) throws EdmException
    {
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        for (ALDInvoiceInfo info : invoiceInfos)
        {
            Map<String,Object> bean = new LinkedHashMap<String, Object>(); 
            bean.put("id", info.getId());
            bean.put("lineNumber", info.getLineNumber());
            bean.put("status", info.getStatus());
            bean.put("statusDescription", info.getStatusDescription());
            bean.put("bookCode", info.getBookCode());
            bean.put("nrKsiegowania", info.getNrKsiegowania());
            bean.put("supplierNumber", info.getSupplierNumber());
            bean.put("invoiceNumber", info.getInvoiceNumber());
            bean.put("paymentDate", info.getPaymentDate());
            bean.put("amount", info.getAmount());
            bean.put("nip", info.getNip());
            bean.put("documentId", info.getDocumentId());
            bean.put("paid", info.isPaid());
            bean.put("origAmount", info.getOrigAmount() == null ? 0 : info.getOrigAmount());
            if (info.getDocumentId() != null)
            {
                Document document = Document.find(info.getDocumentId());
                if (document.getAttachments() != null && document.getAttachments().size() > 0)
                {
                    AttachmentRevision revision = document.getAttachments().get(0).getMostRecentRevision();
                    if (revision != null && ViewServer.mimeAcceptable(revision.getMime()))
                    {                        
                        bean.put("viewerLink", "/viewserver/viewer.action?id="+revision.getId()+"&fax=false&width=1000&height=750");
                    }
                }
            }
            list.add(bean);
        }
        return list;
    }
    
    /**
     * Weryfkacja rejestru VAT.
     * Z pliku CSV wczytywane wierszami informacje o kolejnych fakturach i zapisywane do bazy.
     * Od razu nast�puje przetwarzanie danych - ustawienie odpowiedniego status b��du.
     * Zwracana jest lista utworzonych obiekt�w {@link ALDInvoiceInfo2}.
     */
    public static List<ALDInvoiceInfo2> parseAndSaveVATRegistryFile(File file) throws EdmException
    {
        List<ALDInvoiceInfo2> list = new ArrayList<ALDInvoiceInfo2>();
        BufferedReader reader = null;
        try
        {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "cp1250"));
            String line;
            int lineCount = 0;
            while ((line = reader.readLine()) != null)
            {
                lineCount++;
                
                line = line.replace("\"","");
                
                line = line.trim();
                // pierwsza linia to nag��wek, kt�ry omijam
                if (lineCount == 1 || line.startsWith("#"))
                    continue;
                
                ALDInvoiceInfo2 info = new ALDInvoiceInfo2();
                info.setCtime(new Date());
                info.setCreatingUser(DSApi.context().getPrincipalName());
                info.setLineNumber(lineCount);
                
                String[] parts = line.split("\\s*;\\s*");
                if (parts.length > 0)
                    info.setNrKsiegowania(parts[0].trim());
                if (parts.length > 1)
                {
                    String bookCode = parts[1].trim();
                    if (bookCode.length() > 5)
                        bookCode = bookCode.substring(0, 5);
                    info.setBookCode(bookCode);                    
                }
                if (parts.length > 2)
                    info.setInvoiceNumber(parts[2].trim());
                // nip obecnie ignorujemy
                //if (parts.length > 3)
                //    info.setNip(parts[3].trim());
                if (parts.length > 4)
                {
                    String amount = parts[4].trim();
                    amount = amount.replace("\"", "");
                    amount = amount.replace(".", "");
                    amount = amount.replace(",", ".");
                    try
                    {
                        info.setAmount(Float.parseFloat(amount));
                    }
                    catch (NumberFormatException e)
                    {
                    	LogFactory.getLog("eprint").debug("", e);
                        info.setStatus(ALDInvoiceInfo2.STATUS_PARSE_ERROR);
                        info.setErrorInfo("niepoprawna kwota");
                        info.setAmount(null);
                    }
                }
                if (parts.length < 5)
                {
                    info.setStatus(ALDInvoiceInfo2.STATUS_PARSE_ERROR);
                    info.setErrorInfo("nie podano wszystkich informacji");
                }
                
                list.add(info);
                
                if (info.getStatus() == null)                
                {
                    // poprawnie odczytano z pliku - szukamy odpowiedni dokument
                    DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.ALD_KIND);
                    DockindQuery query = new DockindQuery(0,0);
                    query.setDocumentKind(kind);
                    query.field(kind.getFieldByCn(ALDLogic.BOOK_CODE_FIELD_CN), info.getBookCode());
                    query.field(kind.getFieldByCn(ALDLogic.NR_KSIEGOWANIA_FIELD_CN), info.getNrKsiegowania());
                    SearchResults<Document> searchResults = DocumentKindsManager.search(query);
                    if (searchResults.totalCount() == 0)
                        info.setStatus(ALDInvoiceInfo2.STATUS_NOT_FOUND);                                                                       
                    else
                    {
                        Document[] documents = searchResults.results();
                        checkDocument(documents[0], info);
                        
                        if (documents.length > 1)
                        {
                            info.setStatus(ALDInvoiceInfo2.STATUS_TOO_MANY_FOUND);
                            for (int i=1; i < documents.length; i++)
                            {
                                ALDInvoiceInfo2 copy = new ALDInvoiceInfo2();
                                copy.setAmount(info.getAmount());
                                copy.setBookCode(info.getBookCode());
                                copy.setCreatingUser(info.getCreatingUser());
                                copy.setCtime(info.getCtime());
                                copy.setInvoiceNumber(info.getInvoiceNumber());
                                copy.setNrKsiegowania(info.getNrKsiegowania());
                                copy.setLineNumber(info.getLineNumber());                          
                                checkDocument(documents[i], copy);
                                copy.setStatus(ALDInvoiceInfo2.STATUS_TOO_MANY_FOUND);
                                Persister.create(copy);
                                list.add(copy);
                            }                            
                        }
                    }
                }
                Persister.create(info);
            }
            return list;
        }
        catch (IOException e)
        {
            throw new EdmException("B��d podczas czytania pliku z rejestrem VAT");
        }
        finally
        {
        	org.apache.commons.io.IOUtils.closeQuietly(reader);
        	/*if (reader!=null)
        	{
        		try { reader.close(); } catch (Exception e1) { }
        	}*/
        	
        }
    }
    
    /**
     * Sprawdza zgodno�� danego dokumentu z utworzonym obiektem {@link ALDInvoiceInfo2} na potrzeby
     * weryfikacji rejestr VAT.
     * Na koniec ustala odpowiedni status w tym obiekcie (tak�e pewne inne rzeczy np. 
     * dat� p�atno�ci).
     */
    private static void checkDocument(Document document, ALDInvoiceInfo2 info) throws EdmException
    {
        info.setDocumentId(document.getId());
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        Float amount = (Float) fm.getKey(ALDLogic.KWOTA_BRUTTO_FIELD_CN);        

        info.setOrigAmount(amount);
        if (!info.getAmount().equals(amount))
            info.setStatus(ALDInvoiceInfo2.STATUS_NOT_EVERYTHING_MATCH);
        else
            info.setStatus(ALDInvoiceInfo2.STATUS_OK);   
    }  
    
    /**
     * Kasowanie z bazy wszystkich obiekt�w {@link ALDInvoiceInfo2} utworzonych przez
     * podanego u�ytkownika. 
     * Powinno by� wo�ane w momencie �adowanie przez tego u�ytkownika nowego pliku!
     */
    public static void deleteInvoiceInfos2(String username) throws EdmException
    {
        PreparedStatement ps = null;
        try
        {
            ps = DSApi.context().prepareStatement("delete from DS_ALD_INVOICE_INFO2 where creatingUser = ?");
            ps.setString(1, username);
            ps.executeUpdate();            
        }
        catch (SQLException e)
        {
            throw new EdmSQLException(e);
        }
        finally
        {
        	DSApi.context().closeStatement(ps);        	
        }
    }
    
    public static List<Map<String,Object>> prepareInvoice2Beans(List<ALDInvoiceInfo2> invoiceInfos2) throws EdmException
    {
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        for (ALDInvoiceInfo2 info : invoiceInfos2)
        {
            Map<String,Object> bean = new LinkedHashMap<String, Object>(); 
            bean.put("id", info.getId());
            bean.put("lineNumber", info.getLineNumber());
            bean.put("status", info.getStatus());
            bean.put("statusDescription", info.getStatusDescription());
            bean.put("bookCode", info.getBookCode());
            bean.put("nrKsiegowania", info.getNrKsiegowania());
            bean.put("invoiceNumber", info.getInvoiceNumber());
            bean.put("amount", info.getAmount());
            bean.put("documentId", info.getDocumentId());
            bean.put("origAmount", info.getOrigAmount() == null ? 0 : info.getOrigAmount());
            if (info.getDocumentId() != null)
            {
                Document document = Document.find(info.getDocumentId());
                if (document.getAttachments() != null && document.getAttachments().size() > 0)
                {
                    AttachmentRevision revision = document.getAttachments().get(0).getMostRecentRevision();
                    if (revision != null && ViewServer.mimeAcceptable(revision.getMime()))
                    {                        
                        bean.put("viewerLink", "/viewserver/viewer.action?id="+revision.getId()+"&fax=false&width=1000&height=750");
                    }
                }
            }
            list.add(bean);
        }
        return list;
    }
}
