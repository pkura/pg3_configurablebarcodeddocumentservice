package pl.compan.docusafe.parametrization.ald;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.ValidationException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.core.base.Flags;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.service.tasklist.Task;
import pl.compan.docusafe.util.DateUtils;

/**
 * Logika dla dokument�w ALD.
 *
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 * @version $Id$
 */
public class ALDLogic extends AbstractDocumentLogic
{
    private static ALDLogic instance;

    public static ALDLogic getInstance()
    {
        if (instance == null)
            instance = new ALDLogic();
        return instance;
    }

    // nazwy kodowe poszczeg�lnych p�l dla tego rodzaju dokumentu
    public static final String KATEGORIA_FIELD_CN = "KATEGORIA";
    public static final String DATA_FIELD_CN = "DATA";
    public static final String DATA_PLATNOSCI_FIELD_CN = "DATA_PLATNOSCI";
    public static final String NR_KONTRAKTU_FIELD_CN = "NR_KONTRAKTU";
    public static final String NR_REJESTRACYJNY_FIELD_CN = "NR_REJESTRACYJNY";
    public static final String NR_VIN_FIELD_CN = "NR_VIN";
    public static final String NR_FAKTURY_FIELD_CN = "NR_FAKTURY";
    public static final String NIP_DOSTAWCY_FIELD_CN = "NIP_DOSTAWCY";
    public static final String NUMER_DOSTAWCY_FIELD_CN = "NUMER_DOSTAWCY";
    public static final String BOOK_CODE_FIELD_CN = "BOOK_CODE";
    public static final String ZAPLACONY_FIELD_CN = "ZAPLACONY";
    public static final String KWOTA_BRUTTO_FIELD_CN = "KWOTA_BRUTTO";
    public static final String NR_KSIEGOWANIA_FIELD_CN = "NR_KSIEGOWANIA";
    public static final String NR_AUTORYZACJI_FIELD_CN = "NR_AUTORYZACJI";
    public static final String NAZWA_KLIENTA_FIELD_CN = "NAZWA_KLIENTA";
    public static final String RODZAJ_DOKUMENTU_FIELD_CN = "RODZAJ_DOKUMENTU";
    public static final String STATUS_FIELD_CN = "STATUS";
    public static final String NAZWA_DOSTAWCY_FIELD_CN = "NAZWA_DOSTAWCY";
    public static final String TERMIN_PLATNOSCI_CN = "TERMIN_PLATNOSCI";

    // nazwy kodowe poszczeg�lnych kategorii dokumentu
    public static final String SERWIS_CN = "SERWIS";
    public static final String OPONY_CN = "OPONY";
    public static final String SERWIS_INNE_CN = "SERWIS_INNE";
    public static final String HANDLOWY_CN = "HANDLOWY";
    public static final String HANDLOWY_ZAKUPY_CN = "HANDLOWY_ZAKUPY";
    public static final String IT_CN = "IT";
    public static final String KSIEGOWOSC_CN = "KSIEGOWOSC";
    public static final String MARKETING_CN = "MARKETING";
    public static final String UBEZPIECZENIA_CN = "UBEZPIECZENIA";
    public static final String BACKOFFICE_CN = "BACKOFFICE";
    public static final String ZARZAD_CN = "ZARZAD";
    public static final String KREDYT_FINANSE_CN = "KREDYT_FINANSE";
    public static final String ADMINISTRACJA_CN = "ADMINISTRACJA";
    public static final String SERWIS_SZKODY_CN = "SERWIS_SZKODY";
    public static final String PISMA_WARSZTATY_CN = "PISMA_WARSZTATY";
    public static final String RENT_A_CAR_CN = "RENT_A_CAR";
    public static final String CUSTOMER_CARE_CN = "CUSTOMER_CARE";
    public static final String KSIEGOWOSC_INNE_CN = "KSIEGOWOSC_INNE";
    public static final String OPONY_POTWIERDZENIA_CN = "OPONY_POTWIERDZENIA";
    public static final String WEZWANIA_DO_ZAPLATY_CN = "WEZWANIA_DO_ZAPLATY";
    public static final String POTWIERDZENIA_SALD_CN = "POTWIERDZENIA_SALD";
    public static final String INNE_CN = "INNE";
    public static final String KOREKTY_SPRZEDAZ_CN = "KOREKTY_SPRZEDAZ";
    public static final String KOREKTY_SERWIS_SZKODY_CN = "KOREKTY_SERWIS_SZKODY";

    // GUID-y poszczeg�lnych grup
    private static final String GROUP_ACCOUNTING = "ALD_ACCOUNTING";
    private static final String GROUP_SERWIS = "ALD_SERWIS";
    private static final String GROUP_HANDLOWY = "ALD_HANDLOWY";
    private static final String GROUP_IT = "ALD_IT";
    private static final String GROUP_MARKETING = "ALD_MARKETING";
    private static final String GROUP_UBEZPIECZENIA = "ALD_UBEZPIECZENIA";
    private static final String GROUP_ZARZAD = "ALD_ZARZAD";
    private static final String GROUP_BACKOFFICE = "ALD_BACKOFFICE";
    private static final String GROUP_CREDIT = "ALD_CREDIT";

    // statusy
    public static final Integer STATUS_NOWY = 10;
    public static final Integer STATUS_ODRZUCONY = 20;
    public static final Integer STATUS_ZAAKCEPTOWANY = 30;
    public static final Integer STATUS_ZAAKCEPTOWANY_INNY = 40;

    @Override
    public TaskListParams getTaskListParams(DocumentKind kind, long documentId) throws EdmException {
        return TaskListParams.fromMagicArray(getTaskListParam(kind, documentId));
    }

    private Object[] getTaskListParam(DocumentKind dockind,Long id) throws EdmException
    {
        Object[] result = new Object[10];
        FieldsManager fm = dockind.getFieldsManager(id);
        result[2] = fm.getValue(KWOTA_BRUTTO_FIELD_CN);
        //result[4] = fm.getKey(TERMIN_PLATNOSCI_CN) != null ? DateUtils.formatJsDate((Date) fm.getValue(TERMIN_PLATNOSCI_CN)) : String.valueOf("");
        result[4] = fm.getKey(TERMIN_PLATNOSCI_CN) != null ? DateUtils.formatSqlDate((Date) fm.getValue(TERMIN_PLATNOSCI_CN)) : String.valueOf("");
        return result;
    }

    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
    {
        if (values.get(KATEGORIA_FIELD_CN) == null)
            throw new EdmException("Nie wybrano kategorii dokumentu");
        Field field = kind.getFieldByCn(KATEGORIA_FIELD_CN);
        Integer kategoriaId = (Integer) field.coerce(values.get(KATEGORIA_FIELD_CN));
        String kategoriaCn = field.getEnumItem(kategoriaId).getCn();
        if (SERWIS_CN.equals(kategoriaCn) || OPONY_CN.equals(kategoriaCn))
        {
            if (getValue(kind, values, DATA_FIELD_CN) == null)
                throw new ValidationException("Nie podano daty dokumentu");
            if (getValue(kind, values, NR_KONTRAKTU_FIELD_CN) == null)
                throw new ValidationException("Nie podano numeru kontraktu");
            if (getValue(kind, values, NR_REJESTRACYJNY_FIELD_CN) == null)
                throw new ValidationException("Nie podano numeru rejestracyjnego");
            if (getValue(kind, values, NR_VIN_FIELD_CN) == null)
                throw new ValidationException("Nie podano numeru VIN");
            if (getValue(kind, values, NR_FAKTURY_FIELD_CN) == null)
                throw new ValidationException("Nie podano numeru faktury");
            if (getValue(kind, values, NIP_DOSTAWCY_FIELD_CN) == null)
                throw new ValidationException("Nie podano NIP-u dostawcy");
            if (getValue(kind, values, NAZWA_KLIENTA_FIELD_CN) == null)
                throw new ValidationException("Nie podano nazwy klienta");
        }
        if (getValue(kind, values, STATUS_FIELD_CN) == null)
            throw new ValidationException("Nie wybrano statusu dokumentu");
        /* TODO: wylaczone poki ALD nie dostarczy spisu rodzajow dokumentow
        if (getValue(kind, values, RODZAJ_DOKUMENTU_FIELD_CN) == null)
            throw new ValidationException("Nie wybrano rodzaju dokumentu");*/
    }

    /**
     * Bezpieczny substring - odporny na nullpointery i dlugosci VINow
     * @param param
     * @param chars
     * @return
     */
    protected String safeSubstring(String param, int chars)
    {
    	if (param == null)
    		return "";
    	if (param.length()<=chars)
    		return param;
    	return param.substring(0,chars);
    }

    /**
     * Budowanie sciezki folderow opartych o numer VIN
     * @param path
     * @param fm
     * @throws EdmException
     */
    protected void vinBasedPath(List<String> path, FieldsManager fm) throws EdmException
    {
    	path.add("Serwis");
        String vin = fm.getValue(NR_VIN_FIELD_CN).toString();
        vin = vin.replace("\"", ""); // czyscimy VIN z cudzyslowow

        path.add(safeSubstring(vin,3)); // oznaczenie producenta
        path.add(safeSubstring(vin,9));// producent + model
        path.add(safeSubstring(vin,14)); // bez 3 ostatnich cyfr
        path.add(vin);
        path.add(fm.getValue(NR_REJESTRACYJNY_FIELD_CN).toString());
        path.add(fm.getValue(NR_KONTRAKTU_FIELD_CN).toString());
    }

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
    {
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");
        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());

        List<String> path = new ArrayList<String>();

        EnumItem kategoriaItem = fm.getEnumItem(KATEGORIA_FIELD_CN);
        if (kategoriaItem.getCn().equals(SERWIS_CN) || kategoriaItem.getCn().equals(OPONY_CN))
        {
        	vinBasedPath(path,fm);
        }
        else if (kategoriaItem.getCn().equals(SERWIS_INNE_CN))
        {

            if (fm.getValue(NR_VIN_FIELD_CN) != null && fm.getValue(NR_REJESTRACYJNY_FIELD_CN) != null
                && fm.getValue(NR_KONTRAKTU_FIELD_CN) != null)
            {
            	vinBasedPath(path,fm);
            }
            else
            {
            	// jak nie ma nr VIN, rejestracyjnego lub kontraktu to umieszczam w katalogu Serwis/Inne/YYYYMM
            	path.add("Serwis");
                path.add("Inne");
                Date date = null;
                if (fm.getValue(DATA_FIELD_CN) != null)
                    date = (Date) fm.getValue(DATA_FIELD_CN);
                else
                    date = document.getCtime();
                if (date != null)
                {
                    path.add(DateUtils.formatYearMonth(date));
                }
            }
        }
        else
        {
            path.add(kategoriaItem.getTitle());
            Date date = null;
            if (fm.getValue(DATA_FIELD_CN) != null)
                date = (Date) fm.getValue(DATA_FIELD_CN);
            else
                date = document.getCtime();
            if (date != null)
            {
                path.add(DateUtils.formatYearMonth(date));
            }
        }

        Folder folder = Folder.getRootFolder();
        for (Iterator iter = path.iterator(); iter.hasNext();)
        {
            String pathSegment = (String) iter.next();
            // TODO: uprawnienia do folderow obczaic jakie
            DSApi.context().grant(folder, new String[]{ObjectPermission.READ}, "*", ObjectPermission.ANY);
            //DSApi.context().grant(folder, new String[] { ObjectPermission.CREATE}, "DAA", ObjectPermission.GROUP);
            folder = folder.createSubfolderIfNotPresent(pathSegment);
        }

        document.setFolder(folder);
        if(type == DocumentLogic.TYPE_IN_OFFICE)
        {
        	InOfficeDocument iod = (InOfficeDocument)document;

        	if(iod.getSender().getOrganization()==null){
        		iod.getSender().setAnonymous(false);
        		iod.getSender().setOrganization((String)fm.getValue(ALDLogic.NAZWA_DOSTAWCY_FIELD_CN));
        		DSApi.context().session().save(iod);
        	}
        }
        // ustawienie 'summary' i 'description'
        if (kategoriaItem.getCn().equals(SERWIS_CN))
        {
            document.setTitle("Faktura serwisowa");
            document.setDescription(document.getTitle());
            if (document instanceof OfficeDocument)
            {
             //  ((OfficeDocument) document).setSummary(document.getTitle());
            }
        }
        else if (kategoriaItem.getCn().equals(OPONY_CN))
        {
            document.setTitle("Faktura dotycz�ca opon");
            document.setDescription(document.getTitle());
            if (document instanceof OfficeDocument)
            {
             //   ((OfficeDocument) document).setSummary(document.getTitle());
            }
        }
        else if (kategoriaItem.getCn().equals(SERWIS_SZKODY_CN))
        {
            document.setTitle("Faktura szkodowa");
            document.setDescription(document.getTitle());
            if (document instanceof OfficeDocument)
            {
              //  ((OfficeDocument) document).setSummary(document.getTitle());
            }
        }
        else if(document instanceof OutOfficeDocument)
            return;

            EnumItem tmp = fm.getEnumItem("KATEGORIA");
            document.setTitle(tmp.getTitle());
            document.setDescription(document.getTitle());
            if (document instanceof OfficeDocument)
            ((OfficeDocument)document).setSummaryOnly(document.getTitle());

    }

    public void documentPermissions(Document document) throws EdmException
    {
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");

        DSApi.context().grant(document, new String[]{ObjectPermission.READ, ObjectPermission.READ_ATTACHMENTS}, "*", ObjectPermission.ANY);
        try
        {
            DSApi.context().session().flush();
        }
        catch (HibernateException ex)
        {
            throw new EdmHibernateException(ex);
        }
    }

    @Override
    public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
        Map<String,Object> values = new HashMap<String,Object>();
        if (DocumentLogic.TYPE_OUT_OFFICE == type)
            values.put(KATEGORIA_FIELD_CN, fm.getField(KATEGORIA_FIELD_CN).getEnumItemByCn(ADMINISTRACJA_CN).getId());
        else
            values.put(KATEGORIA_FIELD_CN, fm.getField(KATEGORIA_FIELD_CN).getEnumItemByCn(SERWIS_CN).getId());
        values.put(DATA_FIELD_CN, new Date());
        values.put(STATUS_FIELD_CN, STATUS_NOWY);
        fm.reloadValues(values);
    }

    @Override
    public void checkCanFinishProcess(OfficeDocument document) throws AccessDeniedException, EdmException
    {
        if (document.getDocumentKind() == null)
            throw new NullPointerException("document.getDocumentKind()");

		if (document instanceof OutOfficeDocument)
            return;

        String error = "Nie mo�na zako�czy� pracy z dokumentem: ";

        FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
        EnumItem kategoriaItem = fm.getEnumItem(KATEGORIA_FIELD_CN);
        if (kategoriaItem== null)
            throw new NullPointerException("kategoria");
        if (kategoriaItem.getCn()==null)
            throw new NullPointerException("cn");

        if(kategoriaItem.getCn().equals(OPONY_POTWIERDZENIA_CN))
            return;

        if (STATUS_NOWY.equals(fm.getKey(STATUS_FIELD_CN)))
            throw new AccessDeniedException(error + "Nie mo�na ko�czy� pracy z dokumentem o statusie Nowy");

        // gdy status odrzucony mo�na zako�czy� proces zawsze - wystarczy tylko doda� uwag�
        if (STATUS_ODRZUCONY.equals(fm.getKey(STATUS_FIELD_CN)) || STATUS_ZAAKCEPTOWANY_INNY.equals(fm.getKey(STATUS_FIELD_CN)))
        {
            if (kategoriaItem.getCn().equals(SERWIS_CN) && fm.getKey(NR_AUTORYZACJI_FIELD_CN)!= null)
                    throw new AccessDeniedException(error+"Status musi by� ustawiony jako Zaakceptowany, gdy� wprowadzono nr autoryzacji");
            if (document.getRemarks() == null || document.getRemarks().size() == 0)
                throw new AccessDeniedException(error+" Co najmniej jedna uwaga musi zosta� wprowadzona dla dokumentu");
            return;
        }

        //tutaj na pewno jest status ZAAKCEPTOWANY

        if (kategoriaItem.getCn().equals(SERWIS_CN) || kategoriaItem.getCn().equals(OPONY_CN) )
        {
            if ("XXX".equals(fm.getKey(NR_KONTRAKTU_FIELD_CN)))
            {
                throw new AccessDeniedException(error+"Numer kontraktu musi by� r�ny od XXX");
            }
            if ("XXX".equals(fm.getKey(NR_REJESTRACYJNY_FIELD_CN)))
            {
                throw new AccessDeniedException(error+"Numer rejestracyjny musi by� r�ny od XXX");
            }
            if ("XXX".equals(fm.getKey(NR_VIN_FIELD_CN)))
            {
                throw new AccessDeniedException(error+"Numer VIN musi by� r�ny od XXX");
            }
            if (fm.getKey(NAZWA_KLIENTA_FIELD_CN) == null || "XXX".equals(fm.getKey(NAZWA_KLIENTA_FIELD_CN)))
            {
                throw new AccessDeniedException(error+"Nazwa klienta musi zosta� wprowadzona i by� r�na od XXX");
            }
            if (fm.getKey(NR_KSIEGOWANIA_FIELD_CN) == null)
            {
                throw new AccessDeniedException(error+"Numer ksi�gowania musi zosta� wprowadzony");
            }
            if (fm.getKey(BOOK_CODE_FIELD_CN) == null)
            {
                throw new AccessDeniedException(error+"Book code musi zosta� wprowadzony");
            }
            if (fm.getKey(KWOTA_BRUTTO_FIELD_CN) == null)
            {
                throw new AccessDeniedException(error+"Kwota brutto musi zosta� wprowadzona");
            }
            if (kategoriaItem.getCn().equals(SERWIS_CN) && (fm.getKey(NR_AUTORYZACJI_FIELD_CN) == null))
            {
                throw new AccessDeniedException(error+"Numer autoryzacji musi zosta� wprowadzony");
            }
            if (document.getRemarks() == null || document.getRemarks().size() == 0)
            {
                throw new AccessDeniedException(error+" Co najmniej jedna uwaga musi zosta� wprowadzona dla dokumentu");
            }
        }


        if (kategoriaItem.getCn().equals(HANDLOWY_ZAKUPY_CN) && fm.getKey(BOOK_CODE_FIELD_CN) == null)
        {
            throw new AccessDeniedException(error+"Book code musi zosta� wprowadzony");
        }

        // sprawdzanie flag
        boolean ksiegowa = false;
        boolean merytoryczna = false;
        boolean finansowa = false;

        for(Flags.Flag flag : document.getFlags().getDocumentFlags(false)){
	        if (flag != null)
	        {
	            if ("M".equals(flag.getC()) && flag.isValue())
	                merytoryczna = true;
	            else if ("K".equals(flag.getC()) && flag.isValue())
	                ksiegowa = true;
	            else if ("F".equals(flag.getC()) && flag.isValue())
	                finansowa = true;
	        }
        }


        if (kategoriaItem.getCn().equals(KSIEGOWOSC_INNE_CN) || kategoriaItem.getCn().equals(WEZWANIA_DO_ZAPLATY_CN)
        		|| kategoriaItem.getCn().equals(POTWIERDZENIA_SALD_CN))
        {
            if (!merytoryczna)
                throw new AccessDeniedException(error+"Flaga Akceptacja merytoryczna musi by� ustawiona");
        }
        else if (kategoriaItem.getCn().equals(SERWIS_CN) || kategoriaItem.getCn().equals(OPONY_CN)
        || kategoriaItem.getCn().equals(SERWIS_INNE_CN) || kategoriaItem.getCn().equals(SERWIS_SZKODY_CN)
        || kategoriaItem.getCn().equals(PISMA_WARSZTATY_CN) || kategoriaItem.getCn().equals(UBEZPIECZENIA_CN)
        || kategoriaItem.getCn().equals(BACKOFFICE_CN) || kategoriaItem.getCn().equals(INNE_CN)
        || kategoriaItem.getCn().equals(RENT_A_CAR_CN) || kategoriaItem.getCn().equals(CUSTOMER_CARE_CN)
        || kategoriaItem.getCn().equals(KOREKTY_SERWIS_SZKODY_CN))
        {
            if (!ksiegowa)
                throw new AccessDeniedException(error+"Flaga Akceptacja ksi�gowa musi by� ustawiona");
        }
        else if (kategoriaItem.getCn().equals(HANDLOWY_CN) || kategoriaItem.getCn().equals(IT_CN)
        || kategoriaItem.getCn().equals(KSIEGOWOSC_CN) || kategoriaItem.getCn().equals(MARKETING_CN)
        || kategoriaItem.getCn().equals(ZARZAD_CN) || kategoriaItem.getCn().equals(KREDYT_FINANSE_CN)
        || kategoriaItem.getCn().equals(ADMINISTRACJA_CN)
        || kategoriaItem.getCn().equals(KOREKTY_SPRZEDAZ_CN))
        {
            if (!finansowa)
                throw new AccessDeniedException(error+"Flaga Akceptacja finansowa musi by� ustawiona");
        }
    }

    @Override
    public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
    {
    	String channel = document.getDocumentKind().getChannel(document.getId());
    	if(channel != null)
    	{
	    	 if( ProcessCoordinator.find(DocumentLogicLoader.ALD_KIND,channel) != null &&
	    			 ProcessCoordinator.find(DocumentLogicLoader.ALD_KIND,channel).size() > 0)
	    	        return ProcessCoordinator.find(DocumentLogicLoader.ALD_KIND,channel).get(0);
	    	 else
	    		 return null;
    	}
    	return null;
    }

    public void onStartProcess(OfficeDocument document)
    {
        DocumentKind dk = document.getDocumentKind();
        FieldsManager fm = dk.getFieldsManager(document.getId());
        try
        {
            EnumItem kategoriaItem = fm.getEnumItem(KATEGORIA_FIELD_CN);
            if (kategoriaItem== null || kategoriaItem.getCn()==null)
                return;
            if(!kategoriaItem.getCn().equals(OPONY_POTWIERDZENIA_CN))
                return;

            TaskSnapshot.Query query;
            query = new TaskSnapshot.Query();
            query.setDocumentId(document.getId());
            SearchResults<TaskSnapshot> results = TaskSnapshot.getInstance().search(query);
            List<Task> res = new ArrayList<Task>();
            boolean flagsOn = "true".equals(Configuration.getProperty("flags"));

            for (TaskSnapshot task : results.results())
            {
                res.add(task.toFullTask(flagsOn));
            }
            WorkflowFactory.getInstance().manualFinish(document.getId(), false);

        }
        catch(EdmException edme)
        {
        	LogFactory.getLog("eprint").error("", edme);
        }
    }

	public boolean isReadPermissionCode(String permCode)
	{
		return false;
	}
}
