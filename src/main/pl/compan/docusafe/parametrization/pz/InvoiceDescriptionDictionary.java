package pl.compan.docusafe.parametrization.pz;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExp;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.db.criteria.NativeProjection;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.AbstractDictionaryField;
import pl.compan.docusafe.core.dockinds.field.NonColumnField;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: kk
 * Date: 09.09.13
 * Time: 16:52
 */
public class InvoiceDescriptionDictionary extends DwrDictionaryBase {

    private static final Logger log = LoggerFactory.getLogger(InvoiceDescriptionDictionary.class);


    @Override
    public List<Map<String, Object>> search(Map<String, FieldData> values, int limit, int offset, boolean fromDwrSearch,
    		Map<String, FieldData> dockindFields) throws EdmException {
        List<Map<String, Object>> result;
        if ((result = search(values, limit, offset)) != null) {
            return result;
        }

        String filteringDictionary = ((AbstractDictionaryField) getOldField()).getFilteringDictionaryCn();
        Map<String, String> filteredColumns = new HashMap<String, String>();
        Map<String, FieldData> filteringValues = null;
        //	jesli slownik jest slownikiem filtrowanym
        Map<String, String> whereExpsToValue = new HashMap<String, String>();
        List<Map<String, Object>> ret = Lists.newArrayList();
        String tableName;
        String id = "id";
        FieldData fieldData = null;
        tableName = ((AbstractDictionaryField) getOldField()).getTable();

        NativeCriteria nc = DSApi.context().createNativeCriteria(tableName, "d");
        NativeProjection np = NativeExps.projection();
        for (pl.compan.docusafe.core.dockinds.field.Field f : getOldField().getFields()) // dodaje do projekcji pola posiadaj±ce swe kolumny
        {
            if ((!(f instanceof NonColumnField) && !f.getCn().equals("ID") && !f.getCn().equals("ID_1"))) {
                String column = f.getColumn();
                if (isMultiple() && !(f instanceof NonColumnField))
                    column = column.substring(0, column.lastIndexOf("_"));
                np.addProjection("d." + column, column);
            }
        }
        np.addProjection("d." + id, "id");
        nc.setProjection(np);
        for (pl.compan.docusafe.core.dockinds.field.Field f : getOldField().getFields()) {
            String column = f.getColumn();
            String fCn = f.getCn().toUpperCase();

            //nazwa pola
            String nameCn = new StringBuilder(getName()).append("_").append(fCn).toString();

            if (values.containsKey(nameCn) && !(f instanceof NonColumnField) && values.get(nameCn).getData() != null) {
                if (!values.get(nameCn).getStringData().equals("")) {
                    try {
                        String valueData = (String) values.get(nameCn).getData();
                        NativeExp exp = null;
                        if (f.isFullTextSearch())
                            exp = NativeExps.contains(column, valueData);
                        else
                            exp = NativeExps.likeCaseInsensitive(column, "%" + valueData + "%");
                        nc.add(exp);
                        whereExpsToValue.put(exp.toSQL(), "'%" + valueData + "%'");
                        if (filteredColumns.containsKey(column) && (fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column))) != null) {
                            fieldData = filteringValues.get(filteringDictionary + "_" + filteredColumns.get(column));
                            valueData = (String) fieldData.getData();
                            if (column.equalsIgnoreCase("nip"))
                                valueData = valueData.replace("-", "").replace(" ", "");
                            if (f.isFullTextSearch())
                                exp = NativeExps.contains(column, valueData);
                            else
                                exp = NativeExps.like(column, "%" + valueData + "%");
                            nc.add(exp);
                            whereExpsToValue.put(exp.toSQL(), "'%" + valueData + "%'");
                        }
                    } catch (Exception e) {
                        log.debug(e);
                    }
                }
            }
        }

        if (fromDwrSearch) {
            NativeExp exp = NativeExps.isNotNull("is_template");
            nc.add(exp);
            whereExpsToValue.put(exp.toSQL(), "is_template");
        }
        // ograniczenie za pomocą atrybutu not-nullable-fields
        if (this.getOldField() instanceof AbstractDictionaryField && ((AbstractDictionaryField) this.getOldField()).getNotNullableFields() != null) {
            for (String notNullField : ((AbstractDictionaryField) this.getOldField()).getNotNullableFields()) {
                if (StringUtils.isNotEmpty(notNullField)) {
                    NativeExp exp = NativeExps.isNotNull(notNullField);
                    nc.add(exp);
                    whereExpsToValue.put(exp.toSQL(), notNullField.toString());
                }
            }
        }

        if (values.containsKey("id") && !(values.get("id").getData().toString()).equals("")) {
            NativeExp exp = NativeExps.eq("d." + id, values.get("id").getData());
            nc.add(exp);
            whereExpsToValue.put(exp.toSQL(), values.get("id").getData().toString());
        } else
            nc.setLimit(limit).setOffset(offset);
        try {
            CriteriaResult cr = nc.criteriaResult();
            while (cr.next()) {
                Map<String, Object> row = Maps.newLinkedHashMap();
                row.put("id", cr.getString("id", null));
                for (pl.compan.docusafe.core.dockinds.field.Field f : getOldField().getFields()) {
                    String fCn = f.getCn();
                    String column = f.getColumn();

                    if (!(f instanceof NonColumnField) && cr.getValue(column, null) != null) {
                        row.put(getName().toUpperCase() + "_" + fCn.toUpperCase(), cr.getValue(column, null));
                    }
                }
                ret.add(row);
            }
        } catch (Exception a) {
            log.error(a.getMessage(), a);
            throw new EdmException(a);
        }
        return ret;
    }
}
