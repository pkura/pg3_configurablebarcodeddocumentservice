package pl.compan.docusafe.parametrization.pz;

import com.google.common.base.Optional;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * User: kk
 * Date: 27.09.13
 * Time: 21:12
 */
public class PzHelper {


    static class AccountFields extends HashMap<String, String> {

        public AccountFields() {
             for (String key : FakturaZakupowaLogic.POSITION_ENUM_FIELDS) {
                 super.put(key, null);
             }
        }

        void clearNonControlFields() {

        }

    }

    static class PositionAmount {
        private BigDecimal currentAmount;
        private BigDecimal previousAmount;

        public PositionAmount(String dictionaryNameWithPrefix, String fieldName, Map<String, Object> results, Map<String, FieldData> values) {
            currentAmount = (BigDecimal) results.get(dictionaryNameWithPrefix + fieldName);
            previousAmount = values.get(dictionaryNameWithPrefix + fieldName) != null ? values.get(dictionaryNameWithPrefix + fieldName).getMoneyData() : null;
        }

        public BigDecimal getCurrentAmount() {
            return Optional.fromNullable(currentAmount).or(BigDecimal.ZERO);
        }

        public BigDecimal getPreviousAmount() {
            return previousAmount;
        }

        boolean wasEvaluated() {
            if (currentAmount == null) {
                return false;
            }
            return true;
        }

        void setCurrentAmount(String dictionaryNameWithPrefix, String fieldName, Map<String, Object> results) {
            currentAmount = (BigDecimal) results.get(dictionaryNameWithPrefix + fieldName);
        }
    }

    static class Position {
        private Integer id;
        private Map<String,FieldData> data;

        public Position(Integer id, Map<String, FieldData> data) {
            this.id = id;
            this.data = data;
        }

        public Position(Long id, Map<String, FieldData> values) {
            this(id != null ? id.intValue() : null, values);
        }

        public Position(Number id, Map<String, FieldData> values) {
            if (id != null) {
                if (id instanceof Integer) {
                    this.id = (Integer) id;
                } else {
                    this.id = id.intValue();
                }
            }
            this.data = values;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Map<String, FieldData> getData() {
            return data;
        }

        public void setData(Map<String, FieldData> position) {
            this.data = position;
        }
    }
}
