package pl.compan.docusafe.parametrization.pz;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.primitives.Ints;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;

import static pl.compan.docusafe.parametrization.pz.FakturaZakupowaLogic.*;
import static pl.compan.docusafe.parametrization.pz.PzHelper.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * User: kk
 * Date: 30.09.13
 * Time: 16:51
 */
public class PositionDictionary extends DwrDictionaryBase {

    private static final Logger log = LoggerFactory.getLogger(PositionDictionary.class);
    public static final Logger debug = LoggerFactory.getLogger("kk");
    private static final List<String> BASIC_FIELDS = ImmutableList.<String>builder()
            .add(PODZIAL_KOSZTOW_PREFIX + OPIS_FIELD)
            .add(PODZIAL_KOSZTOW_PREFIX + KWOTA_FIELD)
            .add(PODZIAL_KOSZTOW_PREFIX + PROCENT_FIELD)
            .build();

    @Override
    public Map<String, Object> getValues(String dicitonaryEntryId, Map<String, Object> fieldValues) throws EdmException {
        Integer entryId = dicitonaryEntryId != null ? Ints.tryParse(dicitonaryEntryId) : null;
        if (entryId != null && entryId < 0) {
            return new HashMap<String, Object>();
        } else {
            return super.getValues(dicitonaryEntryId, fieldValues);
        }
    }

    @Override
    public void filterAfterGetValues(Map<String, Object> dictionaryFilteredFieldsValues, String dicitonaryEntryId, Map<String, Object> fieldsValues, Map<String, Object> documentValues) {
        Integer entryId = dicitonaryEntryId != null ? Ints.tryParse(dicitonaryEntryId) : null;
        Map<String, Object> results = Maps.newHashMap();
        Map<String, String> accountField = new PzHelper.AccountFields();

        Position lastEdited = (Position) PzUtils.getFromSession(FakturaZakupowaLogic.SESSION_LAST_EDITED_POSITION);
        Integer addedItemId = (Integer) PzUtils.getFromSession(FakturaZakupowaLogic.SESSION_ADDED_ITEM_ID);

        if (isFirstFilledPosition(entryId)) {
            setDefaultValues(fieldsValues, results, accountField, null, true);
            setEnumFieldValues(Collections.EMPTY_MAP, documentValues, results, accountField);
            dictionaryFilteredFieldsValues.putAll(results);
            dictionaryFilteredFieldsValues.put(PODZIAL_KOSZTOW_PREFIX + "ID", -new Random().nextInt(100));
        } else if (isNewAddedPosition(entryId, addedItemId)) {
            PzUtils.clearSession(FakturaZakupowaLogic.SESSION_ADDED_ITEM_ID);
            Position lastPosition = (Position) PzUtils.getFromSession(SESSION_LAST_POSITION);

            Preconditions.checkState(lastPosition != null, "Nie pobrano ostatniej pozcji");
            setDefaultValues(fieldsValues, results, accountField, lastPosition.getData(), false);
            setEnumFieldValues(Collections.EMPTY_MAP, documentValues, results, accountField);

            dictionaryFilteredFieldsValues.put(PODZIAL_KOSZTOW_PREFIX + "ID", entryId);
            dictionaryFilteredFieldsValues.putAll(results);
        } else if (isLastEditedPosition(lastEdited, entryId)) {
            Map<String, FieldData> position = lastEdited.getData();

            setEnumFieldValues(position, documentValues, results, accountField);

            for (String cn : BASIC_FIELDS) {
                results.put(cn, position.get(cn) != null ? position.get(cn).getData() : null);
            }
            dictionaryFilteredFieldsValues.put(PODZIAL_KOSZTOW_PREFIX + "ID", lastEdited.getId());
            dictionaryFilteredFieldsValues.putAll(results);
            PzUtils.clearSession(SESSION_ADDED_ITEM_ID, SESSION_LAST_EDITED_POSITION);
        }


    }

    private boolean isLastEditedPosition(Position lastEdited, Integer entryId) {
        return lastEdited != null
                && entryId != null
                && entryId.equals(lastEdited.getId());
    }

    private boolean isNewAddedPosition(Integer dicitonaryEntryId, Integer addedItemId) {
        return addedItemId != null && dicitonaryEntryId.equals(addedItemId);
    }

    private boolean isFirstFilledPosition(Integer dicitonaryEntryId) {
        return dicitonaryEntryId == null;
    }


    private void setDefaultValues(Map<String, Object> fieldsValues, Map<String, Object> results, Map<String, String> accountField, Map<String, FieldData> lastPosition, boolean firstDefault) {
        BigDecimal netAmount = Optional.fromNullable((BigDecimal) fieldsValues.get(KWOTA_NETTO_FIELD)).or(BigDecimal.ZERO);

        setDefaultVatRateId(results, fieldsValues, lastPosition, netAmount);
        setDefaultPositionDescription(results, lastPosition);
        setDefaultAmountAndPercent(fieldsValues, results, netAmount, firstDefault);

        if (lastPosition != null && lastPosition.get(PODZIAL_KOSZTOW_PREFIX + RODZAJ_KOSZTU_FIELD) != null) {
            for (String fieldCn : POSITION_ENUM_FIELDS) {
                accountField.put(fieldCn, TextUtils.nullSafeObject(lastPosition.get(PODZIAL_KOSZTOW_PREFIX + fieldCn).getData()));
            }
        }
    }


    private void setDefaultPositionDescription(Map<String, Object> results, Map<String, FieldData> lastPosition) {
        if (lastPosition != null && DwrUtils.isNotNull(lastPosition, PODZIAL_KOSZTOW_PREFIX + OPIS_FIELD)) {
            String description = TextUtils.nullSafeObject(lastPosition.get(PODZIAL_KOSZTOW_PREFIX + OPIS_FIELD).getData());
            results.put(PODZIAL_KOSZTOW_PREFIX + OPIS_FIELD, description);
        }
    }

    private void setDefaultVatRateId(Map<String, Object> results, Map<String, Object> fieldsValues, Map<String, FieldData> lastPosition, BigDecimal netAmount) {
        Integer defaultVatRateId;

        //pozycja wypełniana jako pierwsza, lub wybrano stawki vat w ostaniej pozycji
        if (lastPosition == null || !DwrUtils.isNotNull(lastPosition, PODZIAL_KOSZTOW_PREFIX + STAWKA_VAT_FIELD)) {
            BigDecimal grossAmount = Optional.fromNullable((BigDecimal) fieldsValues.get(KWOTA_BRUTTO_FIELD)).or(BigDecimal.ONE);
            defaultVatRateId = netAmount.compareTo(grossAmount) == 0 ? NPD_VAT_RATE_ID : FULL_VAT_RATE_ID;
        } else {
            defaultVatRateId = Ints.tryParse(lastPosition.get(PODZIAL_KOSZTOW_PREFIX + STAWKA_VAT_FIELD).getData().toString());
        }

        if (defaultVatRateId != null) {
            fillVatRateField(results, defaultVatRateId);
        }
    }


    private void setDefaultAmountAndPercent(Map<String, Object> fieldsValues, Map<String, Object> results, BigDecimal netAmount, boolean firstDefault) {
        BigDecimal remainFromSession = (BigDecimal) PzUtils.getFromSession(SESSION_REMAIN_AMOUNT);
        PzUtils.clearSession(SESSION_REMAIN_AMOUNT);
        BigDecimal restAmount = remainFromSession != null ? remainFromSession : (BigDecimal) fieldsValues.get(KOSZTY_DO_ROZPISANIA_FIELD);
        if (netAmount != BigDecimal.ZERO) {
            BigDecimal amount = netAmount;
            if (!firstDefault && restAmount != null) {
                amount = restAmount;
            }
            results.put(PODZIAL_KOSZTOW_PREFIX + PROCENT_FIELD, amount == netAmount ? 100 : amount.divide(netAmount, 2, RoundingMode.HALF_UP).movePointRight(2).intValue());
            results.put(PODZIAL_KOSZTOW_PREFIX + KWOTA_FIELD, amount);
        }
    }


    private void fillVatRateField(Map<String, Object> results, Integer defaultVatRateId) {
        try {
            DataBaseEnumField ratesField = DataBaseEnumField.getEnumFiledForTable(VAT_RATES_TABLENAME);
            results.put(PODZIAL_KOSZTOW_PREFIX + STAWKA_VAT_FIELD, ratesField.getEnumValuesForForcedPush(defaultVatRateId.toString()));
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }


    private void setEnumFieldValues(Map<String, FieldData> values, Map<String, Object> fieldsValues, Map<String, Object> results, Map<String, String> accountField) {
        String typeOfCost = getTypeOfCostCn(values, results, accountField);

        if (typeOfCost != null) {
            if (!typeOfCost.startsWith("4")) {
                accountField.remove(DZIALALNOSC_FIELD);
            }

            if (!(typeOfCost.startsWith("650") || (typeOfCost.startsWith("4") && typeOfCost.toUpperCase().charAt(INDEX_IN_STRING) == 'P'))) {
                accountField.remove(MPK_FIELD);
            }

            if (!typeOfCost.equalsIgnoreCase("746")) {
                accountField.remove(ODBIORCA_REFAKTURY_FIELD);
            }

        } else {
            accountField.remove(DZIALALNOSC_FIELD);
            accountField.remove(MPK_FIELD);
            accountField.remove(ODBIORCA_REFAKTURY_FIELD);
        }
        String erpId = TextUtils.nullSafeObject(fieldsValues.get(FakturaZakupowaLogic.ERP_FIELD));
        putEnumItemsIntoResults(values, fieldsValues, results, accountField, erpId);
    }


    private String getTypeOfCostCn(Map<String, FieldData> values, Map<String, Object> results, Map<String, String> accountField) {
        try {
            if (accountField.get(RODZAJ_KOSZTU_FIELD) != null) {
                Integer itemId = Ints.tryParse(accountField.get(RODZAJ_KOSZTU_FIELD));
                if (itemId != null) {
                    EnumItem cost = DataBaseEnumField.getEnumItemForTable(FakturaZakupowaLogic.PRODUKT_TABLE, itemId);
                    return cost.getCn();
                }
            } else {
                return DwrUtils.getSelectedEnumCn(values, results, PODZIAL_KOSZTOW_PREFIX + RODZAJ_KOSZTU_FIELD, FakturaZakupowaLogic.PRODUKT_TABLE);
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }

    private void putEnumItemsIntoResults(Map<String, FieldData> values, Map<String, Object> fieldsValues, Map<String, Object> results, Map<String, String> accountField, String refFieldSelectedId) {
        String idFromTemplate = "";
        Object templateValue = fieldsValues.get("ODBIORCY_REFAKTURY");
        if (templateValue != null) {
            if (templateValue instanceof Iterable) {
                idFromTemplate = Iterables.getFirst((Iterable<? extends Object>) templateValue, "").toString();
            } else {
                idFromTemplate = templateValue.toString();
            }
        }

        putCorrectEnumFieldToResults(values, results, accountField, RODZAJ_KOSZTU_FIELD, PRODUKT_TABLE, refFieldSelectedId, "", false);
        putCorrectEnumFieldToResults(values, results, accountField, DZIALALNOSC_FIELD, "dsg_pz_komorka", refFieldSelectedId, "", true);
        putCorrectEnumFieldToResults(values, results, accountField, MPK_FIELD, "dsg_pz_mpk", refFieldSelectedId, "", true);
        putCorrectEnumFieldToResults(values, results, accountField, ODBIORCA_REFAKTURY_FIELD, "dsg_pz_odbiorca_refaktury", refFieldSelectedId, idFromTemplate, true);
    }

    private void putCorrectEnumFieldToResults(Map<String, FieldData> values, Map<String, Object> results, Map<String, String> accountField, String fieldCn, String tableName, String refFieldSelectedId, String fieldSelectedId, boolean putHidden) {
        if (accountField.containsKey(fieldCn)) {
            fieldSelectedId = Optional.fromNullable(accountField.get(fieldCn)).or(fieldSelectedId);
            DwrUtils.setRefValueEnumOptions(values, results, FakturaZakupowaLogic.ERP_FIELD, PODZIAL_KOSZTOW_PREFIX, fieldCn, tableName, refFieldSelectedId, EMPTY_ENUM_VALUE, "_1", "", fieldSelectedId);
        } else {
            if (putHidden) {
                results.put(PODZIAL_KOSZTOW_PREFIX + fieldCn, EMPTY_ENUM_VALUE);
            }
        }
    }
}
