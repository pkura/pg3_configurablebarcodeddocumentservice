package pl.compan.docusafe.parametrization.pz;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateMidnight;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.dwr.PersonDictionary;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.events.handlers.DocumentMailHandler;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * User: kk
 * Date: 09.09.13
 * Time: 11:52
 */
public class ArchiwumLogic extends AbstractDocumentLogic {

    protected static final Logger log = LoggerFactory.getLogger(ArchiwumLogic.class);
    public static final String UPDATE_SIGNATURE_NUMBER = "declare @n int;\n" +
            "select @n=ISNULL(max(cast(substring(sygnatura, 10, len(sygnatura)-9) as int)),0)+1\n" +
            "from dsg_pz_archiwum\n" +
            "where SUBSTRING(sygnatura, 1, 2)=? and SUBSTRING(sygnatura, 4, 2)=? and SUBSTRING(sygnatura, 7, 2)=?;\n" +
            "update dsg_pz_archiwum set sygnatura=?+cast(@n as varchar(19)) where document_id=?;";
    private static final String DOCUMENT_TYPE_SHORTCUT_PARAM = "document-type-shortcut";
    private static final Integer ARCHIWUM_REMINDER_ID = 1;
    private static final String ARCHIWUM_REMINDER_TEMPLATE_FILENAME = "archiwum_powiadomienie.txt";
    private static final String SYGNATURA_FIELD = "SYGNATURA";


    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        FieldsManager fm = document.getFieldsManager();

        if (Strings.isNullOrEmpty(fm.getStringValue(SYGNATURA_FIELD))) {
            try {
                Date currentDay = GlobalPreferences.getCurrentDay();
                Calendar calInstance = Calendar.getInstance();
                calInstance.setTime(currentDay);

                String docNameShortcut = document.getDocumentKind().getProperties().get(DOCUMENT_TYPE_SHORTCUT_PARAM);
                Preconditions.checkArgument(!Strings.isNullOrEmpty(docNameShortcut), "Nie zdefiniowano skr�tu " + DOCUMENT_TYPE_SHORTCUT_PARAM);

                String month = StringUtils.leftPad(String.valueOf(calInstance.get(Calendar.MONTH) + 1), 2, '0');
                String year = StringUtils.right(String.valueOf(calInstance.get(Calendar.YEAR)), 2);

                setCurrentSignatureNumber(document, month, year, docNameShortcut);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                throw new EdmException("B��d w generowaniu numeru sygnatury");
            }
        }

        Date remindDate = (Date) fm.getKey("DATA_POWIADOMIENIA");
        Boolean wasStarted = Optional.fromNullable((Boolean) fm.getKey("POWIADOMIENIE_URUCHOMIONE")).or(Boolean.FALSE);

        if (remindDate != null && !wasStarted) {
            Number remindMainUserId = (Number) fm.getKey("OSOBA_ODPOWIEDZIALNA");
            Number remindSubstituteUserId = (Number) fm.getKey("OSOBA_ZSTEPUJACA");
            Preconditions.checkState(!new DateMidnight(new Date().getTime()).isAfter(remindDate.getTime()), "Data powiadomienie nie mo�e z przes�o�ci");
            Preconditions.checkState(remindMainUserId != null || remindSubstituteUserId != null, "Nie wskazano �adnej osoby do powiadomienie");

            Long documentId = document.getId();

            if (remindMainUserId != null) {
                EventFactory.registerEvent("immediateTrigger", "documentMail", documentId + "|" + ARCHIWUM_REMINDER_ID + "|" + remindMainUserId, null, remindDate, documentId);
            }

            if (remindSubstituteUserId != null) {
                EventFactory.registerEvent("immediateTrigger", "documentMail", documentId + "|" + ARCHIWUM_REMINDER_ID + "|" + remindSubstituteUserId, null, remindDate, documentId);
            }

            fm.getDocumentKind().setOnly(documentId, ImmutableMap.of("POWIADOMIENIE_URUCHOMIONE", Boolean.TRUE));
        }
    }

    @Override
    public void prepareMailToSend(Document document, String[] eventParams, DocumentMailHandler.DocumentMailBean documentMailBean) {
        DSUser user = getUserFromEventParams(eventParams);
        Map<String, String> templateParameters = makeTemplateParams(document, user);

        documentMailBean.setTemplateParameters(templateParameters);
        documentMailBean.setEmail(user.getEmail());
        documentMailBean.setTemplate(ARCHIWUM_REMINDER_TEMPLATE_FILENAME);
    }

    private Map<String, String> makeTemplateParams(Document document, DSUser user) {
        Map<String, String> templateParameters = Maps.newHashMap();
        try {
            templateParameters.put("tytul", document.getTitle());
            templateParameters.put("imienazwisko", user.asFirstnameLastname());
            templateParameters.put("link", Docusafe.getBaseUrl()
                    + "/repository/edit-dockind-document.action?id="
                    + document.getId());
            templateParameters.put("sygnatura", document.getFieldsManager().getStringValue(SYGNATURA_FIELD));
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
        return templateParameters;
    }

    private DSUser getUserFromEventParams(String[] eventParam) {
        String userId = eventParam[2];
        DSUser user = null;
        try {
            user = DSUser.findById(Long.valueOf(userId));
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
        Preconditions.checkNotNull(user);
        return user;
    }

    private void setCurrentSignatureNumber(Document document, String month, String year, String docNameShortcut) throws Exception {
        int updated;
        PreparedStatement ps = null;
        try {
            ps = DSApi.context().prepareStatement(UPDATE_SIGNATURE_NUMBER);
            ps.setString(1, year);
            ps.setString(2, month);
            ps.setString(3, month);
            ps.setString(4, String.format("%.2s/%.2s/%.2s/", docNameShortcut, year, month));
            ps.setLong(5, document.getId());

            updated = ps.executeUpdate();

            Preconditions.checkState(updated == 1);
        } catch (Exception e) {
            if (ps != null) {
                DSApi.context().closeStatement(ps);
            }
            throw e;
        }
    }

    @Override
    public void documentPermissions(Document document) throws EdmException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void specialSetDictionaryValues(Map<String, ?> dockindFields) {
        if (dockindFields.keySet().contains("CONTRACTOR_DICTIONARY")) {
            Map<String, FieldData> m = (Map<String, FieldData>) dockindFields.get("CONTRACTOR_DICTIONARY");
            m.put("CONTRACTOR_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
            m.put("CONTRACTOR_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
        }
    }

    public void addSpecialInsertDictionaryValues(String dictionaryName, Map<String, FieldData> values) {
        log.info("Before Insert - Special dictionary: '{}' values: {}",
                dictionaryName, values);
        if (dictionaryName.contains("CONTRACTOR")) {
            Person p = new Person();
            p.createBasePerson(dictionaryName, values);
            try {
                p.create();
            } catch (EdmException e) {
                log.error("", e);
            }
            PersonDictionary.prepareFieldNamesForSenderPerson(p.getId(), dictionaryName, values);
        }
    }

    @Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields) {
        log.info("Before Search - Special dictionary: '{}' values: {}", dictionaryName, values);
        if (dictionaryName.contains("CONTRACTOR")) {
            values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
            values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
        }
    }

}
