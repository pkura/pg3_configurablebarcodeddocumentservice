package pl.compan.docusafe.parametrization.pz;

import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.primitives.Ints;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.webwork.event.ActionEvent;
import pl.compan.docusafe.webwork.event.ActionType;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.util.*;

/**
 * @author Michal Domasat <michal.domasat@docusafe.pl>
 */
public class FakturaZakupowaLogic extends AbstractDocumentLogic {

    private static final Logger log = LoggerFactory.getLogger(FakturaZakupowaLogic.class);

    public static final String DOCKIND_CN = "faktura_zakupowa";

    public static final EnumValues EMPTY_ENUM_VALUE = new EnumValues(new HashMap<String, String>(), new ArrayList<String>());
    private static final String DWR_PREFIX = "DWR_";

    public static final Integer ERP_PORTHOL = 1;
    public static final Integer ERP_PROJECT = 2;
    public static final Integer ERP_PROJECT_GD = 3;
    public static final Integer ERP_WUZ = 4;

    public static final Integer INDEX_IN_STRING = 3;

    public static final String PODZIAL_KOSZTOW_FIELD = "PODZIAL_KOSZTOW";
    public static final String PODZIAL_KOSZTOW_DWR_FIELD = DWR_PREFIX + PODZIAL_KOSZTOW_FIELD;
    public static final String ERP_FIELD = "ERP";

    public static final String PODZIAL_KOSZTOW_PREFIX = PODZIAL_KOSZTOW_FIELD + "_";


    public static final String VAT_RATES_TABLENAME = "dsg_pz_vat_rates";
    public static final String PRODUKT_TABLE = "dsg_pz_produkt";

    public static final String KWOTA_NETTO_FIELD = "KWOTA_NETTO";
    public static final String KWOTA_BRUTTO_FIELD = "KWOTA_BRUTTO";
    public static final String KWOTA_VAT_FIELD = "KWOTA_VAT";
    private static final String KWOTA_NETTO_DWR_FIELD = DWR_PREFIX + KWOTA_NETTO_FIELD;
    static final String KWOTA_BRUTTO_DWR_FIELD = DWR_PREFIX + KWOTA_BRUTTO_FIELD;
    private static final String KWOTA_VAT_DWR_FIELD = DWR_PREFIX + KWOTA_VAT_FIELD;
    public static final String KWOTA_DO_ZAPLATY_FIELD = "DO_ZAPLATY_POZOSTAJE";
    public static final String KWOTA_DO_ZAPLATY_DWR_FIELD = DWR_PREFIX + KWOTA_DO_ZAPLATY_FIELD;

    public static final int NPD_VAT_RATE_ID = 6;
    public static final int FULL_VAT_RATE_ID = 7;
    public static final String AKCEPTACJA_ZARZADU_ACCEPTNAME = "akceptacja_zarzadu";
    public static final String CZLONEK_ZARZADU_FIELDNAME = "CZLONEK_ZARZADU";
    public static final String DZIAL_OPERATORA_FIELDNAME = "DZIAL_OPERATORA";
    public static final String KOSZTY_DO_ROZPISANIA_FIELD = "KOSZTY_DO_ROZPISANIA";
    public static final String STAWKA_VAT_FIELD = "STAWKA_VAT";
    public static final String MPK_FIELD = "MPK";
    public static final String DZIALALNOSC_FIELD = "DZIALALNOSC";
    public static final String RODZAJ_KOSZTU_FIELD = "RODZAJ_KOSZTU";
    public static final String ODBIORCA_REFAKTURY_FIELD = "ODBIORCA_REFAKTURY";
    public static final List<String> POSITION_ENUM_FIELDS = ImmutableList.of(RODZAJ_KOSZTU_FIELD, DZIALALNOSC_FIELD, MPK_FIELD, ODBIORCA_REFAKTURY_FIELD);
    public static final String OPIS_FIELD = "OPIS";
    public static final String KWOTA_FIELD = "KWOTA";
    public static final String PROCENT_FIELD = "PROCENT";
    private static final String LAST_EDITED_AMOUNT = "LAST_EDITED_AMOUNT";
    private static final String DODAJ_POZYCJE_DWR_FIELD = "DWR_DODAJ_POZYCJE";

    public static final String SESSION_LAST_POSITION = "LAST_POSITION_ITEM";
    public static final String SESSION_LAST_EDITED_POSITION = "SESSION_LAST_EDITED_POSITION";
    public static final String SESSION_ADDED_ITEM_ID = "SESSION_ADDED_ITEM_ID";
    public static final String SESSION_REMAIN_AMOUNT = "SESSION_REMAIN_AMOUNT";
    private static final String TYP_FAKTURY_FIELD = "TYP_FAKTURY";
    private static final String GRUPA_FIELD = "GRUPA";
    private static final String UPDATE_DECREE_NUMBER =  "declare @n int;\n" +
                                                        "select @n=ISNULL(max(cast(substring(numer_dekretu, 12, 4) as int)),0)+1\n" +
                                                        "from dsg_pz_faktura_zakupowa\n" +
                                                        "where SUBSTRING(numer_dekretu, 1, 2)=? and SUBSTRING(numer_dekretu, 6, 2)=? and SUBSTRING(numer_dekretu, 9, 2)=?;\n" +
                                                        "update dsg_pz_faktura_zakupowa set numer_dekretu=?+replace(str(@n,4),' ','0') where document_id=?;";
    private static final String NUMER_DEKRETU_FIELD = "NUMER_DEKRETU";


    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException {
        super.onStartProcess(document, event);

        createDecreeNumber(document);
    }

    private void createDecreeNumber(OfficeDocument document) throws EdmException {
        try {
            FieldsManager fm = document.getFieldsManager();
            if (fm.getKey(NUMER_DEKRETU_FIELD) == null) {
                DocumentKind documentKind = document.getDocumentKind();

                Integer id = (Integer) fm.getKey(TYP_FAKTURY_FIELD);
                Preconditions.checkNotNull(id, "Nie wybrano typu faktury");

                EnumItem invoiceType = documentKind.getFieldByCn(TYP_FAKTURY_FIELD).getEnumItem(id);
                String invoiceCode = StringUtils.rightPad(StringUtils.left(invoiceType.getCn(), 2), 2, ' ');

                String groupCode = getGroupCode(fm, documentKind);
                String correctionCode = getCorrectionInvoiceCode(fm);

                Date currentDay = GlobalPreferences.getCurrentDay();
                Calendar calInstance = Calendar.getInstance();
                calInstance.setTime(currentDay);

                String month = StringUtils.leftPad(String.valueOf(calInstance.get(Calendar.MONTH) + 1), 2, '0');
                String year = StringUtils.right(String.valueOf(calInstance.get(Calendar.YEAR)), 2);

                assigneDecreeNumber(document.getId(), invoiceCode, correctionCode, groupCode, month, year);
            } else {
                log.error("Pr�bowano ponownie nada� numer dekretu");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new EdmException("B��d w generowaniu numeru dekretu: "+e.getMessage());
        }
    }

    private void assigneDecreeNumber(Long id, String invoiceCode, String correctionCode, String groupCode, String month, String year) throws Exception {
        int updated;
        PreparedStatement ps = null;
        try {
            /*
            do test�w:
                declare @n int;
                select @n=ISNULL(max(cast(substring(numer_dekretu, 12, 4) as int)),0)+1
                from dsg_pz_faktura_zakupowa
                where SUBSTRING(numer_dekretu, 1, 2)='V' and SUBSTRING(numer_dekretu, 6, 2)='14' and SUBSTRING(numer_dekretu, 9, 2)='06';
                --KZ  -13-07-0001
                --KZK -11-09-0001
                --V   -13-06-0002
                --V  G-13-06-0003
                select replace(str(@n,4),' ','0');
                update dsg_pz_faktura_zakupowa set numer_dekretu=?+replace(str(@n,4),' ','0') where document_id=?;
             */
            ps = DSApi.context().prepareStatement(UPDATE_DECREE_NUMBER);
            ps.setString(1, invoiceCode);
            ps.setString(2, year);
            ps.setString(3, month);
            ps.setString(4, String.format("%.2s%.1s%.1s-%.2s-%.2s-", invoiceCode, correctionCode, groupCode, year, month));
            ps.setLong(5, id);

            updated = ps.executeUpdate();

            Preconditions.checkState(updated == 1);
        } catch (Exception e) {
            if (ps != null) {
                DSApi.context().closeStatement(ps);
            }
            throw e;
        }
    }

    private String getGroupCode(FieldsManager fm, DocumentKind documentKind) throws EdmException {
        Integer contractorGroupId = (Integer) fm.getKey(GRUPA_FIELD);
        EnumItem group = null;
        if (contractorGroupId != null) {
            group = documentKind.getFieldByCn(GRUPA_FIELD).getEnumItem(contractorGroupId);
        }

        if (group == null || "NIEPOWIAZANE".equals(group.getCn())) {
            return " ";
        } else {
            return group.getCn().substring(0,1);
        }
    }

    private String getCorrectionInvoiceCode(FieldsManager fm) {
        return " ";
    }

    @Override
    public List<String> getActionMessage(ActionType source, Document doc) {
        List<String> messages = super.getActionMessage(source, doc);

        try {
            switch (source) {
                case OFFICE_CREATE_DOCUMENT:
                    messages.add("Zosta� nadany numer dekretu: "+doc.getFieldsManager().getStringValue(NUMER_DEKRETU_FIELD));
                    break;
                default:
                    break;
            }
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }

        return messages;
    }

    @Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
    }

    @Override
    public void setAdditionalValues(FieldsManager fm, Integer valueOf, String activity) throws EdmException {
        PzUtils.clearSession(LAST_EDITED_AMOUNT, SESSION_LAST_POSITION, SESSION_ADDED_ITEM_ID);

        BigDecimal net = (BigDecimal) fm.getKey(KWOTA_NETTO_FIELD);

        if (net != null) {
            Object dic = fm.getValue(PODZIAL_KOSZTOW_FIELD);
            BigDecimal rest = net;

            if (dic != null && dic instanceof List) {
                if (!((List)dic).isEmpty() && ((List) dic).get(0) instanceof Map) {
                    //raz mapa, raz longi, fuck!
                    for (Map<String, Object> item : (Iterable<Map<String, Object>>) dic) {
                        if (item != null) {
                            Object itemAmount = item.get(PODZIAL_KOSZTOW_PREFIX + KWOTA_FIELD);
                            if (itemAmount != null) {
                                rest = rest.subtract((BigDecimal) itemAmount);
                            }
                        }
                    }
                }
            }

            fm.reloadValues(ImmutableMap.of(KOSZTY_DO_ROZPISANIA_FIELD, rest == net ? BigDecimal.ZERO : (Object) rest));
        }

        super.setAdditionalValues(fm, valueOf, activity);
    }

    @Override
    public void beforeSetWithHistory(Document document, Map<String, Object> values, String activity) throws EdmException {
        if (values.containsKey(DocumentKind.MULTIPLE_DICTIONARY_VALUES)) {
            Map<String, Map<String, FieldData>> dValues = (Map<String, Map<String, FieldData>>) values.get(DocumentKind.MULTIPLE_DICTIONARY_VALUES);

            for (Map.Entry<String, Map<String, FieldData>> entry : dValues.entrySet()) {
                if (entry.getKey().startsWith(PODZIAL_KOSZTOW_PREFIX) && entry.getValue().get("ID") != null) {
                    Integer id = Ints.tryParse(TextUtils.nullSafeObject(entry.getValue().get("ID").getData()));
                    if (id != null && id < 0) {
                        entry.getValue().get("ID").setData("");
                    }
                }
            }
        }

        super.beforeSetWithHistory(document, values, activity);
    }

    @Override
    public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException {
        StringBuilder msgBuilder = new StringBuilder();

        //wyliczanie kwoty netto
        if (DwrUtils.isSenderField(values, KWOTA_NETTO_DWR_FIELD, KWOTA_BRUTTO_DWR_FIELD)) {
            calulateTaxAmount(values);
        }

        //wyliczenie kwoty pozostalej do zap�aty
        if (DwrUtils.isSenderField(values, "DWR_ZAPLACONO", "DWR_DOKONANE_PLATNOSCI", KWOTA_BRUTTO_DWR_FIELD, KWOTA_NETTO_DWR_FIELD)) {
            calculatePaymentRestAmount(values);
        }

        //walidacja kwot dokonancyh p�atno�ci
        validateSumOfPaymentRestAmount(values, msgBuilder);

        calculatePositionItemsAmount(values);


/*        if (DwrUtils.isSenderField(values, PODZIAL_KOSZTOW_DWR_FIELD)) {
            Map<String, FieldData> lastItemMap = findLastPositionItem(values);
            PzUtils.putToSession(SESSION_LAST_POSITION, lastItemMap);
        }*/

        if (DwrUtils.isSenderField(values, DODAJ_POZYCJE_DWR_FIELD)) {
            createNewPosition(values);
        }

        if (msgBuilder.length() > 0) {
            values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
            return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
        }

        return null;
    }

    private void createNewPosition(Map<String, FieldData> values) {
        List<Object> ids = DwrUtils.getValueListFromDictionary(values, PODZIAL_KOSZTOW_FIELD, true, "ID");
            /*
            int noOfItems = DwrUtils.countNoOfItemsInDictionary(values, PODZIAL_KOSZTOW_FIELD) - ids.size();
            for (int i = 0; i < noOfItems; i++) {
                ids.add(new Random().nextInt(100));
            }
            */
        int randomId = -new Random().nextInt(100);
        ids.add(randomId);
        PzUtils.putToSession(SESSION_ADDED_ITEM_ID, randomId);

        Map<String, FieldData> lastItemMap = findLastPositionItem(values);
        if (lastItemMap != null) {
            Number id = (Number) lastItemMap.get(PODZIAL_KOSZTOW_PREFIX + "ID").getData();
            PzUtils.putToSession(SESSION_LAST_POSITION, new PzHelper.Position(id, lastItemMap));
        }

        values.put(PODZIAL_KOSZTOW_DWR_FIELD, new FieldData(Field.Type.STRING, Joiner.on(",").join(ids)));
    }

    private Map<String, FieldData> findLastPositionItem(Map<String, FieldData> values) {
        FieldData dic = values.get(PODZIAL_KOSZTOW_DWR_FIELD);
        if (dic != null) {
            Map<String, FieldData> dicData = dic.getDictionaryData();

            if (dicData != null) {

                int lastItemNo = 0;
                for (int i = 1; dicData.get(PODZIAL_KOSZTOW_PREFIX + RODZAJ_KOSZTU_FIELD + "_" + i) != null; i++) {
                    lastItemNo = i;
                }

                if (lastItemNo > 0) {
                    Map<String, FieldData> lastItemMap = Maps.newHashMap();
                    List<String> fields = Lists.newArrayList(
                            "ID", RODZAJ_KOSZTU_FIELD, DZIALALNOSC_FIELD, MPK_FIELD, ODBIORCA_REFAKTURY_FIELD, STAWKA_VAT_FIELD, OPIS_FIELD, KWOTA_FIELD, PROCENT_FIELD);

                    for (String field : fields) {
                        lastItemMap.put(PODZIAL_KOSZTOW_PREFIX + field, dicData.get(PODZIAL_KOSZTOW_PREFIX + field + "_" + lastItemNo));
                    }
                    return lastItemMap;
                }
            }
        }

        return null;
    }

    private void calculatePositionItemsAmount(Map<String, FieldData> values) {
/*        if (DwrUtils.isSenderField(values, "DWR_KOSZTY_DO_ROZPISANIA_BUTTON")) {
            BigDecimal difference = BigDecimal.ZERO;
            if (values.get(DWR_PREFIX + KOSZTY_DO_ROZPISANIA_FIELD) != null) {
                if (DwrUtils.isNotNull(values, KWOTA_NETTO_DWR_FIELD)) {
                    BigDecimal dicSum = DwrUtils.sumDictionaryMoneyFields(values, PODZIAL_KOSZTOW_FIELD, KWOTA_FIELD);
                    BigDecimal net = values.get(KWOTA_NETTO_DWR_FIELD).getMoneyData();
                    difference = net.subtract(dicSum);
                }
            }

            values.get(DWR_PREFIX + KOSZTY_DO_ROZPISANIA_FIELD).setMoneyData(difference);
        }*/


        if (values.get(DWR_PREFIX + KOSZTY_DO_ROZPISANIA_FIELD) != null) {
            BigDecimal difference = BigDecimal.ZERO;
            PzHelper.PositionAmount lastAmounts = (PzHelper.PositionAmount) PzUtils.getFromSession(LAST_EDITED_AMOUNT);
            PzUtils.clearSession(LAST_EDITED_AMOUNT);

            if (DwrUtils.isNotNull(values, KWOTA_NETTO_DWR_FIELD)) {
                BigDecimal amountsSum = BigDecimal.ZERO;
                if (lastAmounts != null && lastAmounts.wasEvaluated()) {
                    List<BigDecimal> amounts = DwrUtils.getValueListFromDictionary(values, PODZIAL_KOSZTOW_FIELD, true, KWOTA_FIELD);

                    for (BigDecimal amount : amounts) {
                        if (lastAmounts != null && amount.equals(lastAmounts.getPreviousAmount())) {
                            amount = lastAmounts.getCurrentAmount();
                        }
                        amountsSum = amountsSum.add(amount);
                    }

                    if (lastAmounts != null && lastAmounts.getPreviousAmount() == null) {
                        amountsSum = amountsSum.add(lastAmounts.getCurrentAmount());
                    }
                } else {
                    amountsSum = DwrUtils.sumDictionaryMoneyFields(values, PODZIAL_KOSZTOW_FIELD, KWOTA_FIELD);
                }


                BigDecimal net = values.get(KWOTA_NETTO_DWR_FIELD).getMoneyData();
                difference = net.subtract(amountsSum);
                PzUtils.putToSession(SESSION_REMAIN_AMOUNT, difference);


            }
            values.get(DWR_PREFIX + KOSZTY_DO_ROZPISANIA_FIELD).setMoneyData(DwrUtils.isSenderField(values, DODAJ_POZYCJE_DWR_FIELD) ? BigDecimal.ZERO : difference);
        }

    }

    private void validateSumOfPaymentRestAmount(Map<String, FieldData> values, StringBuilder msgBuilder) {
        if (DwrUtils.isNotNull(values, KWOTA_DO_ZAPLATY_DWR_FIELD)) {
            if (values.get(KWOTA_DO_ZAPLATY_DWR_FIELD).getMoneyData().compareTo(BigDecimal.ZERO) == -1) {
                msgBuilder.append("Suma rozpisanych kwot dokonaych p�atno�ci nie mo�e by� wi�ksza od kwoty brutto faktury. ");
            }
        }
    }

    private void calculatePaymentRestAmount(Map<String, FieldData> values) {
        if (values.get(KWOTA_DO_ZAPLATY_DWR_FIELD) != null && DwrUtils.isNotNull(values, "DWR_ZAPLACONO", KWOTA_BRUTTO_DWR_FIELD)) {
            String paymentStatusId = values.get("DWR_ZAPLACONO").getEnumValuesData().getSelectedId();
            BigDecimal toPay = BigDecimal.ZERO;
            if ("100".equals(paymentStatusId) || "150".equals(paymentStatusId)) {
                toPay = values.get(KWOTA_BRUTTO_DWR_FIELD).getMoneyData().subtract(DwrUtils.sumDictionaryMoneyFields(values, "DOKONANE_PLATNOSCI", KWOTA_FIELD));
            } else if ("200".equals(paymentStatusId)) {
                toPay = values.get(KWOTA_BRUTTO_DWR_FIELD).getMoneyData();
            }

            values.get(KWOTA_DO_ZAPLATY_DWR_FIELD).setMoneyData(toPay);
        }
    }

    private void calulateTaxAmount(Map<String, FieldData> values) {
        if (values.get(KWOTA_VAT_DWR_FIELD) != null) {
            values.get(KWOTA_VAT_DWR_FIELD).setMoneyData(DwrUtils.calcMoneyFieldValues(DwrUtils.CalcOperation.SUBTRACT, 2, false, values, KWOTA_BRUTTO_DWR_FIELD, KWOTA_NETTO_DWR_FIELD));
        }
    }

    public void documentPermissions(Document document) throws EdmException {
        // TODO Auto-generated method stub

    }

    @Override
    public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values, Map<String, Object> fieldsValues, Long documentId) {
        Map<String, Object> results = new HashMap<String, Object>();
        try {

            if (PODZIAL_KOSZTOW_FIELD.equals(dictionaryName)) {
                setPositionsDictionary(values, fieldsValues, results);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    private void setPositionsDictionary(Map<String, FieldData> values, Map<String, Object> fieldsValues, Map<String, Object> results) throws EdmException {
        PzHelper.PositionAmount lastEditedAmount = new PzHelper.PositionAmount(PODZIAL_KOSZTOW_PREFIX, KWOTA_FIELD, results, values);
        setPercentOrAmountValues(values, fieldsValues, results);
        lastEditedAmount.setCurrentAmount(PODZIAL_KOSZTOW_PREFIX, KWOTA_FIELD, results);

        Number id = (Number) values.get(PODZIAL_KOSZTOW_PREFIX + "ID").getData();
        PzUtils.putToSession(SESSION_LAST_EDITED_POSITION, new PzHelper.Position(id, values));

        PzUtils.putToSession(LAST_EDITED_AMOUNT, lastEditedAmount);
    }/*
      jakby si� odwidzia�o !!!!!!!!!!!!!!!!!!

    private void setPositionsDictionary(Map<String, FieldData> values, Map<String, Object> fieldsValues, Map<String, Object> results) throws EdmException {
        Map<String, String> accountField = new DocumentHelper.AccountFields();

        if (DwrUtils.isSenderField(values, PODZIAL_KOSZTOW_PREFIX + "LOAD")) {
            setDefaultValues(values, fieldsValues, results, accountField);
        } else {
            setPercentOrAmountValues(values, fieldsValues, results);
        }

        setEnumFieldValues(values, fieldsValues, results, accountField);

        DocumentHelper.PositionAmount lastEditedAmount = new DocumentHelper.PositionAmount(PODZIAL_KOSZTOW_PREFIX, KWOTA_FIELD, results, values);
        putToSession(LAST_EDITED_AMOUNT, lastEditedAmount);
    }

    private void setEnumFieldValues(Map<String, FieldData> values, Map<String, Object> fieldsValues, Map<String, Object> results, Map<String, String> accountField) throws EdmException {
        String typeOfCost = getTypeOfCostCn(values, results, accountField);

        if (typeOfCost != null) {
            if (!typeOfCost.startsWith("4")) {
                accountField.remove(DZIALALNOSC_FIELD);
            }

            if (!(typeOfCost.startsWith("650") || (typeOfCost.startsWith("4") && typeOfCost.toUpperCase().charAt(INDEX_IN_STRING) == 'P'))) {
                accountField.remove(MPK_FIELD);
            }

            if (!typeOfCost.equalsIgnoreCase("746")) {
                accountField.remove(ODBIORCA_REFAKTURY_FIELD);
            }

        } else {
            accountField.remove(DZIALALNOSC_FIELD);
            accountField.remove(MPK_FIELD);
            accountField.remove(ODBIORCA_REFAKTURY_FIELD);
        }
        String erpId = TextUtils.nullSafeObject(fieldsValues.get(FakturaZakupowaLogic.ERP_FIELD));
        putEnumItemsIntoResults(values, fieldsValues, results, accountField, erpId);
    }

    private String getTypeOfCostCn(Map<String, FieldData> values, Map<String, Object> results, Map<String, String> accountField) throws EdmException {
        if (accountField.get(RODZAJ_KOSZTU_FIELD) != null) {
            Integer itemId = Ints.tryParse(accountField.get(RODZAJ_KOSZTU_FIELD));
            if (itemId != null) {
                EnumItem cost = DataBaseEnumField.getEnumItemForTable(PRODUKT_TABLE, itemId);
                return cost.getCn();
            }
        } else {
            return DwrUtils.getSelectedEnumCn(values, results, PODZIAL_KOSZTOW_PREFIX + RODZAJ_KOSZTU_FIELD, PRODUKT_TABLE);
        }
        return null;
    }

    private void putEnumItemsIntoResults(Map<String, FieldData> values, Map<String, Object> fieldsValues, Map<String, Object> results, Map<String, String> accountField, String refFieldSelectedId) {
        String idFromTemplate = "";
        Object templateValue = fieldsValues.get("ODBIORCY_REFAKTURY");
        if (templateValue != null) {
            if (templateValue instanceof Iterable) {
                idFromTemplate = Iterables.getFirst((Iterable<? extends Object>) templateValue, "").toString();
            } else {
                idFromTemplate = templateValue.toString();
            }
        }

        putCorrectEnumFieldToResults(values, results, accountField, RODZAJ_KOSZTU_FIELD, PRODUKT_TABLE, refFieldSelectedId, "", false);
        putCorrectEnumFieldToResults(values, results, accountField, DZIALALNOSC_FIELD, "dsg_pz_komorka", refFieldSelectedId, "", true);
        putCorrectEnumFieldToResults(values, results, accountField, MPK_FIELD, "dsg_pz_mpk", refFieldSelectedId, "", true);
        putCorrectEnumFieldToResults(values, results, accountField, ODBIORCA_REFAKTURY_FIELD, "dsg_pz_odbiorca_refaktury", refFieldSelectedId, idFromTemplate, true);
    }

    private void putCorrectEnumFieldToResults(Map<String, FieldData> values, Map<String, Object> results, Map<String, String> accountField, String fieldCn, String tableName, String refFieldSelectedId, String fieldSelectedId, boolean putHidden) {
        if (accountField.containsKey(fieldCn)) {
            fieldSelectedId = Optional.fromNullable(accountField.get(fieldCn)).or(fieldSelectedId);
            DwrUtils.setRefValueEnumOptions(values, results, FakturaZakupowaLogic.ERP_FIELD, PODZIAL_KOSZTOW_PREFIX, fieldCn, tableName, refFieldSelectedId, EMPTY_ENUM_VALUE, "_1", "", fieldSelectedId);
        } else {
            if (putHidden) {
                results.put(PODZIAL_KOSZTOW_PREFIX + fieldCn, EMPTY_ENUM_VALUE);
            }
        }
    }

    */

    private void setDefaultValues(Map<String, FieldData> values, Map<String, Object> fieldsValues, Map<String, Object> results, Map<String, String> accountField) {
        if (isPositionItemNotFilled(values)) {
            Map<String, FieldData> lastPosition = (Map<String, FieldData>) PzUtils.getFromSession(SESSION_LAST_POSITION);

            BigDecimal netAmount = Optional.fromNullable((BigDecimal) fieldsValues.get(KWOTA_NETTO_FIELD)).or(BigDecimal.ZERO);

            setDefaultVatRateId(results, fieldsValues, lastPosition, netAmount);
            setDefaultPositionDescription(results, lastPosition);
            setDefaultAmountAndPercent(fieldsValues, results, netAmount);

            if (lastPosition != null && lastPosition.get(PODZIAL_KOSZTOW_PREFIX + RODZAJ_KOSZTU_FIELD) != null) {
                for (String fieldCn : POSITION_ENUM_FIELDS) {
                    accountField.put(fieldCn, TextUtils.nullSafeObject(lastPosition.get(PODZIAL_KOSZTOW_PREFIX + fieldCn).getData()));
                }
            }
        }


    }

    private void setDefaultAmountAndPercent(Map<String, Object> fieldsValues, Map<String, Object> results, BigDecimal netAmount) {
        BigDecimal restAmount = (BigDecimal) fieldsValues.get(KOSZTY_DO_ROZPISANIA_FIELD);
        if (netAmount != BigDecimal.ZERO) {
            BigDecimal amount = netAmount;
            if (restAmount != null) {
                amount = restAmount;
            }
            results.put(PODZIAL_KOSZTOW_PREFIX + PROCENT_FIELD, amount == netAmount ? 100 : amount.divide(netAmount, 2, RoundingMode.HALF_UP).movePointRight(2).intValue());
            results.put(PODZIAL_KOSZTOW_PREFIX + KWOTA_FIELD, amount);
        }
    }

    private void setPercentOrAmountValues(Map<String, FieldData> values, Map<String, Object> fieldsValues, Map<String, Object> results) {
        if (DwrUtils.isSenderField(values, PODZIAL_KOSZTOW_PREFIX + KWOTA_FIELD) && values.get(PODZIAL_KOSZTOW_PREFIX + PROCENT_FIELD) != null) {
            BigDecimal sum = (BigDecimal) fieldsValues.get(KWOTA_NETTO_FIELD);
            if (sum != null) {
                BigDecimal part = BigDecimal.ZERO;
                if (DwrUtils.isNotNull(values, PODZIAL_KOSZTOW_PREFIX + KWOTA_FIELD)) {
                    part = values.get(PODZIAL_KOSZTOW_PREFIX + KWOTA_FIELD).getMoneyData();
                }
                BigDecimal percent = part.movePointRight(2).divide(sum, 0, RoundingMode.HALF_UP);
                results.put(PODZIAL_KOSZTOW_PREFIX + PROCENT_FIELD, percent.intValue());
                values.get(PODZIAL_KOSZTOW_PREFIX + PROCENT_FIELD).setIntegerData(percent.intValue());
            }
        }
        if (DwrUtils.isSenderField(values, PODZIAL_KOSZTOW_PREFIX + PROCENT_FIELD) && values.get(PODZIAL_KOSZTOW_PREFIX + KWOTA_FIELD) != null) {
            BigDecimal sum = (BigDecimal) fieldsValues.get(KWOTA_NETTO_FIELD);
            if (sum != null) {
                BigDecimal percent = BigDecimal.ZERO;
                if (DwrUtils.isNotNull(values, PODZIAL_KOSZTOW_PREFIX + PROCENT_FIELD)) {
                    percent = new BigDecimal(values.get(PODZIAL_KOSZTOW_PREFIX + PROCENT_FIELD).getIntegerData());
                }

                BigDecimal netAmount = sum.multiply(percent.movePointLeft(2)).setScale(2, RoundingMode.HALF_UP);
                results.put(PODZIAL_KOSZTOW_PREFIX + KWOTA_FIELD, netAmount);
                values.get(PODZIAL_KOSZTOW_PREFIX + KWOTA_FIELD).setMoneyData(netAmount);
            }
        }
    }

    private void setDefaultPositionDescription(Map<String, Object> results, Map<String, FieldData> lastPosition) {
        if (lastPosition != null && DwrUtils.isNotNull(lastPosition, PODZIAL_KOSZTOW_PREFIX + OPIS_FIELD)) {
            String description = TextUtils.nullSafeObject(lastPosition.get(PODZIAL_KOSZTOW_PREFIX + OPIS_FIELD).getData());
            results.put(PODZIAL_KOSZTOW_PREFIX + OPIS_FIELD, description);
        }
    }

    private void setDefaultVatRateId(Map<String, Object> results, Map<String, Object> fieldsValues, Map<String, FieldData> lastPosition, BigDecimal netAmount) {
        Integer defaultVatRateId;

        //pozycja wype�niana jako pierwsza, lub wybrano stawki vat w ostaniej pozycji
        if (lastPosition == null || !DwrUtils.isNotNull(lastPosition, PODZIAL_KOSZTOW_PREFIX + STAWKA_VAT_FIELD)) {
            BigDecimal grossAmount = Optional.fromNullable((BigDecimal) fieldsValues.get(KWOTA_BRUTTO_FIELD)).or(BigDecimal.ONE);
            defaultVatRateId = netAmount.compareTo(grossAmount) == 0 ? NPD_VAT_RATE_ID : FULL_VAT_RATE_ID;
        } else {
            defaultVatRateId = Ints.tryParse(lastPosition.get(PODZIAL_KOSZTOW_PREFIX + STAWKA_VAT_FIELD).getData().toString());
        }

        if (defaultVatRateId != null) {
            fillVatRateField(results, defaultVatRateId);
        }
    }

    private void fillVatRateField(Map<String, Object> results, Integer defaultVatRateId) {
        try {
            DataBaseEnumField ratesField = DataBaseEnumField.getEnumFiledForTable(VAT_RATES_TABLENAME);
            results.put(PODZIAL_KOSZTOW_PREFIX + STAWKA_VAT_FIELD, ratesField.getEnumValuesForForcedPush(defaultVatRateId.toString()));
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
        }
    }

    private boolean isPositionItemNotFilled(Map<String, FieldData> values) {
        return !(DwrUtils.isNotNull(values, PODZIAL_KOSZTOW_PREFIX + KWOTA_FIELD)
                || DwrUtils.isNotNull(values, PODZIAL_KOSZTOW_PREFIX + "ID")
                || DwrUtils.isNotNull(values, PODZIAL_KOSZTOW_PREFIX + STAWKA_VAT_FIELD)
        );
    }

    @Override
    public TaskListParams getTaskListParams(DocumentKind kind, long documentId, TaskSnapshot task) throws EdmException {
        TaskListParams params = super.getTaskListParams(kind, documentId, task);

        FieldsManager fm = kind.getFieldsManager(documentId);

        //OPIS FAKTURY
        params.setAttribute(getInvoiceDescription(fm), 3);

        //LICZBA ZA��CZNIK�W
        params.setAttribute(getNumberOfAttachment(documentId), 4);

        return params;
    }

    private String getNumberOfAttachment(long documentId) {
        try {
            Document doc = Document.find(documentId, false);
            return String.valueOf(doc.getAttachments().size());
        } catch (EdmException e) {
            log.error(e.getMessage(), e);
            return "";
        }
    }

    private String getInvoiceDescription(FieldsManager fm) throws EdmException {
        try {
            Map<String, Object> invoiceDescriptionDic = (Map<String, Object>) fm.getValue("TRESC_FAKTURY");
            return TextUtils.nullSafeObject(invoiceDescriptionDic.get("TRESC_FAKTURY_OPIS_FAKTURY"));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return "";
        }
    }

}
