package pl.compan.docusafe.parametrization.pz.jbpm;

import com.google.common.base.Preconditions;
import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.pz.FakturaZakupowaLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * User: kk
 * Date: 20.09.13
 * Time: 12:44
 */
public class OmitOperatorDepartmentDecision implements DecisionHandler {
    private final static Logger LOG = LoggerFactory.getLogger(OmitOperatorDepartmentDecision.class);


    private boolean openContextAsNeeded = false;

    @Override
    public String decide(OpenExecution execution) {
        boolean wasOpened = false;

        try{
            if (openContextAsNeeded && !DSApi.isContextOpen()) {
                wasOpened = true;
                DSApi.openAdmin();
            }
            Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();
            if ("7F00000170A1AA6A140DEED252D17F769AD".equals(fm.getEnumItemCn(FakturaZakupowaLogic.DZIAL_OPERATORA_FIELDNAME))) {
                Preconditions.checkState(fm.getKey(FakturaZakupowaLogic.CZLONEK_ZARZADU_FIELDNAME) != null, "Brak wybranego cz�onka zarz�du, nie omini�to edycji metryki");
                return "omit";
            } else {
                return "not_omit";
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            return "not_omit";
        } finally {
            if (openContextAsNeeded && DSApi.isContextOpen() && wasOpened)
                try {
                    DSApi.close();
                } catch (EdmException e) {
                    LOG.error(e.getMessage(), e);
                }
        }
    }
}
