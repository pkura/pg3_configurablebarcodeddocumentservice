package pl.compan.docusafe.parametrization.pz.jbpm;

import com.google.common.base.Preconditions;
import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.parametrization.pz.FakturaZakupowaLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * User: kk
 * Date: 20.09.13
 * Time: 13:25
 */
public class OmitMainManagmentDecision implements DecisionHandler {
    private final static Logger LOG = LoggerFactory.getLogger(OmitMainManagmentDecision.class);

    private boolean openContextAsNeeded = false;

    @Override
    public String decide(OpenExecution execution) {
        boolean wasOpened = false;

        try{
            if (openContextAsNeeded && !DSApi.isContextOpen()) {
                wasOpened = true;
                DSApi.openAdmin();
            }
            Long docId = Long.valueOf(execution.getVariable(Jbpm4Constants.DOCUMENT_ID_PROPERTY) + "");
            OfficeDocument doc = OfficeDocument.find(docId);
            FieldsManager fm = doc.getFieldsManager();

            Preconditions.checkState(fm.getEnumItemCn(FakturaZakupowaLogic.CZLONEK_ZARZADU_FIELDNAME) != null, "Brak wybranego cz�onka zarz�du, nie omini�to akceptacji zarzadu");
            if (omitMainManagment(fm.getEnumItemCn(FakturaZakupowaLogic.CZLONEK_ZARZADU_FIELDNAME))) {
                return "omit";
            } else {
                return "not_omit";
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            return "not_omit";
        } finally {
            if (openContextAsNeeded && DSApi.isContextOpen() && wasOpened)
                try {
                    DSApi.close();
                } catch (EdmException e) {
                    LOG.error(e.getMessage(), e);
                }
        }
    }

    private boolean omitMainManagment(String username) throws EdmException {
        for (AcceptanceCondition accept : AcceptanceCondition.find(FakturaZakupowaLogic.AKCEPTACJA_ZARZADU_ACCEPTNAME)) {
            if (username.equals(accept.getUsername())) {
                return true;
            } else {
                if (accept.getDivisionGuid() != null && DSDivision.isInDivision(accept.getDivisionGuid(), username)) {
                    return true;
                }
            }
        }

        return false;
    }
}
