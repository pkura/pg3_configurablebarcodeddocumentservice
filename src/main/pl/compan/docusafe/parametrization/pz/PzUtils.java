package pl.compan.docusafe.parametrization.pz;

import org.directwebremoting.WebContextFactory;

import javax.servlet.http.HttpSession;

/**
 * User: kk
 * Date: 03.10.13
 * Time: 13:27
 */
public class PzUtils {
    static Object getFromSession(String key) {
        return getFromSession(key, true);
    }

    static Object getFromSession(String key, boolean nullable) {
        Object value = null;
        HttpSession session = WebContextFactory.get().getSession();
        if (session != null) {
            value = session.getAttribute(key);
        }

        if (!nullable && value == null) {
            return "";
        }
        return value;
    }

    static void putToSession(String key, Object value) {
        HttpSession session = WebContextFactory.get().getSession();
        if (session != null) {
            session.setAttribute(key, value);
        }
    }

    static void clearSession(String... keys) {
        HttpSession session = WebContextFactory.get().getSession();
        if (session != null) {
            for (String key : keys) {
                session.removeAttribute(key);
            }
        }
    }
}
