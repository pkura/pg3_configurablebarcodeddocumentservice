package pl.compan.docusafe.parametrization.nordea;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.base.permission.PermissionManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dictionary.DaaAgent;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;

public class Agentlogic extends AbstractDocumentLogic 
{
	public static final String AGENT_CN = "AGENT";
	public static final String TYP_CN = "TYP";

	private static final Agentlogic instance = new Agentlogic();
	
	public static Agentlogic getInstance() 
	{
		return instance;
	}

    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		DaaAgent agent = (DaaAgent) fm.getValue(AGENT_CN);
		
		Folder folder = Folder.getRootFolder();
		folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
		
		folder = folder.createSubfolderIfNotPresent(java.lang.String.format("%s - %s %s", agent.getNumer(),agent.getNazwisko(),agent.getImie()));
		
		document.setFolder(folder);
		
		String name = java.lang.String.format("%s", fm.getValue(TYP_CN));
		
		document.setTitle(name);
		document.setDescription(name);
	}

	public void documentPermissions(Document document) throws EdmException 
	{
		Set<PermissionBean> perms = new HashSet<PermissionBean>();
		
		perms.add(new PermissionBean(ObjectPermission.READ, "AGENT_READ", ObjectPermission.GROUP, document.getDocumentKind().getName()+" - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "AGENT_READ_ATT", ObjectPermission.GROUP, document.getDocumentKind().getName()+" - odczyt (zalacznik)"));
        
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "AGENT_MODIFY", ObjectPermission.GROUP, document.getDocumentKind().getName()+" - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "AGENT_MODIFY_ATT", ObjectPermission.GROUP, document.getDocumentKind().getName()+" - modyfikacja (zalacznik)"));
        
        setUpPermission(document, perms);
	}
	
	

}
