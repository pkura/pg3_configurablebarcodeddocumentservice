package pl.compan.docusafe.parametrization.nordea;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;

public class Polisowylogic extends AbstractDocumentLogic 
{

	public static final String NUMER_POLISY_CN = "NUMER_POLISY";
	public static final String KLASA_CN = "KLASA";
	public static final String TYP_CN = "TYP";
	
	private static final Polisowylogic instance = new Polisowylogic();
	
	public static Polisowylogic getInstance() 
	{
		return instance;
	}
	
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		FieldsManager fm = document.getDocumentKind().getFieldsManager(document.getId());
		
		Folder folder = Folder.getRootFolder();
		
		folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
		folder = folder.createSubfolderIfNotPresent(String.valueOf(fm.getValue(NUMER_POLISY_CN)));
		
		document.setFolder(folder);
		
		String name = java.lang.String.format("%s / %s", fm.getValue(KLASA_CN), fm.getValue(TYP_CN)); 
		
		document.setTitle(name);
		document.setDescription(name);
		
		
		//wzor podpisu
		try		
		{
			File wzor = null;

				for(Attachment at : document.listAttachments())
				{
					if(at.getMostRecentRevision().getMime().contains("image/tiff"))
					{
						File tif = at.getMostRecentRevision().saveToTempFile();

					}
				}
			
			if(wzor != null)
			{
				Attachment sigAttachment = new Attachment("Wz�r podpisu", "Wz�r podpisu wyci�ty z wniosku");
				document.createAttachment(sigAttachment);
				sigAttachment.createRevision(wzor);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
			
		
		
	}

	
	public void documentPermissions(Document document) throws EdmException 
	{
		Set<PermissionBean> perms = new HashSet<PermissionBean>();
		
		perms.add(new PermissionBean(ObjectPermission.READ, "POLISOWY_READ", ObjectPermission.GROUP, document.getDocumentKind().getName()+" - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "POLISOWY_READ_ATT", ObjectPermission.GROUP, document.getDocumentKind().getName()+" - odczyt (zalacznik)"));
        
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "POLISOWY_MODIFY", ObjectPermission.GROUP, document.getDocumentKind().getName()+" - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "POLISOWY_MODIFY_ATT", ObjectPermission.GROUP, document.getDocumentKind().getName()+" - modyfikacja (zalacznik)"));
        
		setUpPermission(document, perms);
	}

}
