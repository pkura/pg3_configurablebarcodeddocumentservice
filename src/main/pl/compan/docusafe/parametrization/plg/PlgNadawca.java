package pl.compan.docusafe.parametrization.plg;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Kontrahenci---
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
@Entity
@Table(name="DSG_PLG_NADAWCA")
public class PlgNadawca implements Serializable
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="external_id")
	private Long externalId;
	
//	private String skrot;
	@Column(name="NAZWA")
	private String nazwa;
	
	@Column(name="MIASTO")
	private String miejscowosc;
	
	@Column(name="ULICA")
	private String ulica;
//	private String numerDomu;
//	private String numerMieszk;
	
	@Column(name="POCZTA")
	private String kod;
//	private String nip;
//	private String pesel;
//	private String regon;
//	private String uwagi;
//	private int statusUE;
//	private int aktywnyKredyt;
//	private long limitKredytu;
//	private String walutaKredytu;
//	private int osfiz;
//	private String app;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Long getExternalId()
	{
		return externalId;
	}

	public void setExternalId(Long externalId)
	{
		this.externalId = externalId;
	}

	public String getNazwa()
	{
		return nazwa;
	}

	public void setNazwa(String nazwa)
	{
		this.nazwa = nazwa;
	}

	public String getMiejscowosc()
	{
		return miejscowosc;
	}

	public void setMiejscowosc(String miejscowosc)
	{
		this.miejscowosc = miejscowosc;
	}

	public String getUlica()
	{
		return ulica;
	}

	public void setUlica(String ulica)
	{
		this.ulica = ulica;
	}

	public String getKod()
	{
		return kod;
	}

	public void setKod(String kod)
	{
		this.kod = kod;
	}
}
