package pl.compan.docusafe.parametrization.plg.reports;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.parametrization.plg.PlgLogic;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.SqlUtils;

public class PlgReport extends Report
{
	private final static Logger log = LoggerFactory.getLogger(PlgReport.class);
	UniversalTableDumper dumper;
	
	private Date dateFrom;
	private Date dateTo;
	
	@Override
	public void doReport() throws Exception
	{
		log.error("PLG Report");
		try
		{
			this.initValues();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		dumper = new UniversalTableDumper();
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "raport.xls");
		poiDumper.openFile(xlsFile);
		dumper.addDumper(poiDumper);
		
		/** NAGLOWEK KO, data wprowadzenia, skr�cony opis, pud�o, nadawca, odbiorca**/
		dumper.newLine();
		dumper.addText("KO");
		dumper.addText("Data wprowadzenia");
		dumper.addText("Skr�cony opis");
		dumper.addText("Pud�o");
		dumper.addText("Nadawca");
		dumper.addText("Odbiorca");
		dumper.dumpLine();
		/** NAGLOWEK:END **/
		
		PreparedStatement ps = null;
		try
		{
			int count = 0;
			ps = DSApi.context().prepareStatement(getSQL());
			ps.setDate(++count, new java.sql.Date(dateFrom.getTime()));
			Calendar cal2 = Calendar.getInstance();
			cal2.setTime(dateTo);
			cal2.add(Calendar.DAY_OF_MONTH,1);
			
			ps.setDate(++count, new java.sql.Date(cal2.getTime().getTime()));
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next())
			{
				dumper.newLine();
				dumper.addText(rs.getString("KO"));
				
				java.sql.Timestamp date = rs.getTimestamp("ctime");
				Calendar cal = Calendar.getInstance();
				cal.setTimeInMillis(date.getTime());

				dumper.addText(DateUtils.formatCommonDateTime(cal.getTime()));
				dumper.addText(rs.getString("opis"));
				dumper.addText(rs.getString("pudlo"));
				dumper.addText(rs.getString("nadawca"));
				if("1".equals(rs.getString("typ_id")))
					dumper.addText(rs.getString("odbiorca_uzytkownik"));
				else
					dumper.addText(rs.getString("odbiorca"));
				dumper.dumpLine();
			}
		}
		catch (Exception e) 
		{
			throw new EdmException(e);
			//e.printStackTrace();
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		dumper.closeFileQuietly();
	}
	
	private String getSQL()
	{
		StringBuffer sql = new StringBuffer();
		
		sql.append("select * from plg_report ");
		sql.append("where ctime>=? and ctime<=? order by KO");
		return sql.toString();
	}
	
	private void initValues() throws Exception
	{
		for(ReportParameter param : getParams())
		{
			if(param.getType().equals("break-line"))
				continue;
			
			if(param.getFieldCn().equals("dateFrom"))
			{
				dateFrom = DateUtils.jsDateFormat.parse((String) param.getValue());
			}
			
			if(param.getFieldCn().equals("dateTo"))
			{
				dateTo = DateUtils.jsDateFormat.parse((String) param.getValue());
			}
			
		}
	}
}
