package pl.compan.docusafe.parametrization.plg.jbpm;

import java.util.Date;

import org.jbpm.JbpmException;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;
import org.jbpm.api.task.AssignmentHandler;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSException;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.plg.AbstractPlgLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class PlgAssignmentHandler implements AssignmentHandler
{
	private static final String USER_DICTIONARY = "user-dictionary";
	private static final String PROC_USER_ID = "procUserId";
	private String type;
	private final static Logger log = LoggerFactory.getLogger(PlgAssignmentHandler.class);
	
	@Override
	public void assign(Assignable assignable, OpenExecution execution) throws Exception
	{
		Document document = Jbpm4Utils.getDocument(execution);
		FieldsManager fm = document.getFieldsManager();
		
		if(USER_DICTIONARY.equals(type))
		{
			Long userId = fm.getLongKey(AbstractPlgLogic._USER_DICT);
			
			if(userId == null)
				throw new EdmException("Wybierz użytkownika!");
			
			DSUser user = DSUser.findById(userId);
			
			assignable.addCandidateUser(user.getName());
			
			if (AvailabilityManager.isAvailable("addToWatch")) {
				DSApi.context().watch(URN.create(document),user.getName());
			}
			
			addToHistory(execution, (OfficeDocument)document, user.getName(), null);
		}
		else if(PROC_USER_ID.equals(type))
		{
			Long userId = (Long) execution.getVariable("procUserId");
			
			if(userId == null)
				throw new EdmException("Wybierz lidera procedującego!");
			
			DSUser user = DSUser.findById(userId);
			
			assignable.addCandidateUser(user.getName());
			
			if (AvailabilityManager.isAvailable("addToWatch")) {
				DSApi.context().watch(URN.create(document),user.getName());
			}
			
			addToHistory(execution, (OfficeDocument)document, user.getName(), null);
		}

	}
	public String getType()
	{
		return type;
	}
	public void setType(String type)
	{
		this.type = type;
	}
	
	/**
	 * @param openExecution
	 * @param doc
	 * @throws EdmException
	 * @throws UserNotFoundException
	 */
	public static void addToHistory(OpenExecution openExecution, OfficeDocument doc, String user, String guid) throws EdmException, UserNotFoundException {
		AssignmentHistoryEntry ahe = new AssignmentHistoryEntry();
		ahe.setSourceUser(DSApi.context().getPrincipalName());
		ahe.setProcessName(openExecution.getProcessDefinitionId());
		if (user == null) {
			ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_DIVISION);
			ahe.setTargetGuid(guid);
		} else {
			ahe.setType(AssignmentHistoryEntry.EntryType.JBPM4_REASSIGN_SINGLE_USER);
			ahe.setTargetUser(user);
		}
		DSDivision[] divs = DSApi.context().getDSUser().getOriginalDivisionsWithoutGroup();
		if (divs != null && divs.length > 0)
			ahe.setSourceGuid(divs[0].getName());
		ahe.setCtime(new Date());
		ahe.setProcessType(AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION);
		try 
		{
			String status = doc.getFieldsManager().getEnumItem("STATUS").getTitle();
			ahe.setStatus(status);
		}
		catch (Exception e)
		{
			log.warn(e.getMessage());
		}
		doc.addAssignmentHistoryEntry(ahe);
	}
	
	

}
