package pl.compan.docusafe.parametrization.plg.jbpm;

import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.parametrization.opzz.jbpm.SetEachForkOpinionVaraibles;
import pl.compan.docusafe.parametrization.plg.PlgFactory;
import pl.compan.docusafe.parametrization.plg.PlgOutLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

public class BindToJournalBehaviour implements ExternalActivityBehaviour
{
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(BindToJournalBehaviour.class);

	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		Document document = Jbpm4Utils.getDocument(execution);
		
		log.error("wi��e z dziennikiem");
		
		if(document.getDocumentKind().logic() instanceof PlgOutLogic)
			PlgFactory.bindToJournal((OfficeDocument)document, (ActionEvent) null, Journal.OUTGOING);
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
	}

}
