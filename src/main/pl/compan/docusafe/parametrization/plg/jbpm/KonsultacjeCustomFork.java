package pl.compan.docusafe.parametrization.plg.jbpm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.parametrization.plg.AbstractPlgLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Fork dla konsultuj�cych
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class KonsultacjeCustomFork  implements ExternalActivityBehaviour
{
	private final static Logger log = LoggerFactory.getLogger(KonsultacjeCustomFork.class);
	
	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		Document document = Jbpm4Utils.getDocument(execution);
		
		log.error("ustawiam dynamicznego forka");
		
		FieldsManager fm = document.getFieldsManager();
		fm.initializeValues();
		List<Long> users = (List<Long>)fm.getKey(AbstractPlgLogic._KONSULTANT_DICT);
		
		if(users == null || users.isEmpty())
			throw new EdmException("Wybierz u�ytkownik�w do konsultacji!");
		
		execution.setVariable("ids", users);
		execution.setVariable("counts", users.size());
		execution.setVariable("count2", 2);
		
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		// TODO Auto-generated method stub
		
	}

}
