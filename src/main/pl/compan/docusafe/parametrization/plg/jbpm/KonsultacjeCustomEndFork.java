package pl.compan.docusafe.parametrization.plg.jbpm;

import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

public class KonsultacjeCustomEndFork  implements ExternalActivityBehaviour
{

	@Override
	public void execute(ActivityExecution arg0) throws Exception
	{	
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{	
	}

}
