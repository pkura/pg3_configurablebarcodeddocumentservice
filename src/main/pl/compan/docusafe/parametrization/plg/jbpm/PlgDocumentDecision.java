package pl.compan.docusafe.parametrization.plg.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.parametrization.opzz.jbpm.cases.CheckCasesLists;
import pl.compan.docusafe.parametrization.plg.AbstractPlgLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Decyduje jak dokument ma isc na pocz�tku
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class PlgDocumentDecision implements DecisionHandler
{
	private static final String RAK_WLASNYCH = "to rak-wlasnych";
	private static final String INNE = "to inne";
	private final static Logger log = LoggerFactory.getLogger(PlgDocumentDecision.class);
	
	@Override
	public String decide(OpenExecution execution)
	{
		try
		{
			Document document = Jbpm4Utils.getDocument(execution);
			FieldsManager fm = document.getFieldsManager();
			
			Long rodzajId = fm.getLongKey(AbstractPlgLogic._RODZAJ);
						
			if(rodzajId.equals(0L)) // typ do r�k w�asnych
				return RAK_WLASNYCH;
			
		}catch(Exception e ){
			log.error("", e);
			
		}
		
		return INNE;
	}

}
