package pl.compan.docusafe.parametrization.plg.jbpm;

import org.jbpm.api.jpdl.DecisionHandler;
import org.jbpm.api.model.OpenExecution;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.parametrization.opzz.jbpm.cases.CheckCasesLists;
import pl.compan.docusafe.parametrization.plg.AbstractPlgLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Decyduje jak dokument ma isc na pocz�tku
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class PlgProcedujacyDecision implements DecisionHandler
{
	private static final String _LIDER = "to uzytkownika";
	private static final String _POZOSTALI = "to pozostali";
	private final static Logger log = LoggerFactory.getLogger(PlgProcedujacyDecision.class);
	
	@Override
	public String decide(OpenExecution execution)
	{
		try
		{
			String userId = execution.getVariable("procUserId").toString();
			Document document = Jbpm4Utils.getDocument(execution);
			FieldsManager fm = document.getFieldsManager();
			
			Long liderId = fm.getLongKey(AbstractPlgLogic._USER_DICT);
						
			if(liderId.equals(Long.valueOf(userId)))
				return _LIDER;
			
		}catch(Exception e ){
			log.error("", e);
			
		}
		
		return _POZOSTALI;
	}

}
