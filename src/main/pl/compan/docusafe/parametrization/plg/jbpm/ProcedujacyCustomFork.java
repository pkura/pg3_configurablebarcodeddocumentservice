package pl.compan.docusafe.parametrization.plg.jbpm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesManager;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.parametrization.plg.AbstractPlgLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


/**
 * Zbiera dane do pierwszego forma dla procedujacych
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class ProcedujacyCustomFork implements ExternalActivityBehaviour
{
	private final static Logger log = LoggerFactory.getLogger(ProcedujacyCustomFork.class);
	
	@Override
	public void execute(ActivityExecution execution) throws Exception
	{
		Document document = Jbpm4Utils.getDocument(execution);
		
		log.error("ustawiam dynamicznego forka procedujacych");
		
		FieldsManager fm = document.getFieldsManager();
		fm.initializeValues();
		List<Long> users = (List<Long>)fm.getKey(AbstractPlgLogic._SUBPROCEDUJACY_DICT);
		Long liderId = fm.getLongKey(AbstractPlgLogic._USER_DICT);
		int procSize = 1; //doliczony lider
	
		if(users != null && !users.isEmpty())
		{
			procSize += users.size();
			users.add(liderId);
		}
		else
		{
			users = new ArrayList<Long>();
			users.add(liderId);
		}
		
		execution.setVariable("procIds", users);
		execution.setVariable("procCounts", procSize);
		execution.setVariable("procCount1", 1);
		
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		
	}

}
