package pl.compan.docusafe.parametrization.plg;

import net.sourceforge.jtds.jdbc.Driver;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.mail.MailerDriver;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static pl.compan.docusafe.service.Console.INFO;

public class PlgSupply extends ServiceDriver implements Service
{
	private final static Logger log = LoggerFactory.getLogger(PlgSupply.class);
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(MailerDriver.class.getPackage().getName(),null);
	
	private Timer timer;
	private Property[] properties;
	private Long period = 25L;
	private String filter = "";
	private String divisionAttrName = "";
	private String emailAttrName = "mail";
	
	@Override
	protected void start() throws ServiceException
	{
		log.info("Start us�ugi PlgSupply");
		timer = new Timer(true);
		timer.schedule(new Supply(), (long) 2*1000 ,(long) period*DateUtils.MINUTE);
		console(INFO, "Start us�ugi LdapDSImport");

	}

	@Override
	protected void stop() throws ServiceException
	{
		log.info("stop uslugi");
		console(INFO, sm.getString("StopUslugi"));
		if(timer != null) timer.cancel();

	}

	@Override
	protected boolean canStop()
	{
		return false;
	}
	
	public class Supply extends TimerTask
	{
		
		@Override
		public void run()
		{
			try
			{
				List results = getExternalData();
			} catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		private List getExternalData() throws SQLException
		{
			String dbUrl = Docusafe.getAdditionProperty("plgsupply.dburl");
			String user = Docusafe.getAdditionProperty("plgsupply.user");
			String pass = Docusafe.getAdditionProperty("plgsupply.pass");
			Driver driver = new Driver();
	        DriverManager.registerDriver(driver);
	        Connection con = DriverManager.getConnection(dbUrl,user,pass);
	        
	        PreparedStatement ps1 = con.prepareStatement(Docusafe.getAdditionProperty("plgsupply.select"));
	        ResultSet rs = ps1.executeQuery();
	        
	        try
			{
				DSApi.openAdmin();
				DSApi.context().begin();
				int i = 0;
	            while(rs.next())
	            {
	            	i++;
	                try
					{
	                	
	                	Long id = rs.getLong("id");
	                	if(id == null || id < 1)
	                		continue;
						PlgNadawca nadawca = PlgFactory.findNadawcaByExternalId(id);
						
						
						if(nadawca == null)
							nadawca = new PlgNadawca();
						
						nadawca.setExternalId(id);
		                nadawca.setNazwa(rs.getString("nazwa"));
		                nadawca.setMiejscowosc(rs.getString("miasto"));
		                nadawca.setUlica(rs.getString("ulica"));
		                nadawca.setKod(rs.getString("kod"));
		                
		                DSApi.context().session().save(nadawca);
		                
		                log.info("zapisuje {}", nadawca.getExternalId());
		                
//		                if(i > 50)
//		                {
//		                	DSApi.context().session().flush();
//		                	i =0;
//		                }
					} catch (Exception e)
					{
						log.error("", e);
					}
	            }
	            
	            DSApi.context().commit();
			}catch(Exception e)
			{
				log.error("", e);
			}finally {
                DSApi._close();
            }
	        
            rs.close();
            ps1.close();
	        return new ArrayList();
		}
		
	}
}
