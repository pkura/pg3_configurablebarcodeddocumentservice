package pl.compan.docusafe.parametrization.plg;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa logiki dla dokumentu PLG
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class PlgLogic extends AbstractPlgLogic
{
	private final static Logger log = LoggerFactory.getLogger(PlgLogic.class);
	
	private static PlgLogic instance;
	

    public static PlgLogic getInstance() {
        if (instance == null)
            instance = new PlgLogic();
        return instance;
    }
    
    
	@Override
	public Long getBoxLine(Document document) throws EdmException
	{
		return PlgFactory.findOrCreateBox().getId();
	}


	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		Folder rootFolder = Folder.getRootFolder();
		rootFolder = rootFolder.createSubfolderIfNotPresent("Korespondencja przychodz�ca");
		
		document.setFolder(rootFolder);
		
		if(document.getBox() == null)
		{
			Box box = PlgFactory.findOrCreateBox();	
			document.setBox(box);
		}
	}

	@Override
	protected Set<PermissionBean> getPermissionBeans()
	{
		Set<PermissionBean> perms = new HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ,"PLG_READ",ObjectPermission.GROUP,"Korespondencja przychodz�ca - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"PLG_READ_ATT_READ",ObjectPermission.GROUP,"Korespondencja przychodz�ca zalacznik - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY,"PLG_READ_MODIFY",ObjectPermission.GROUP,"Korespondencja przychodz�ca - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"PLG_READ_ATT_MODIFY",ObjectPermission.GROUP,"Korespondencja przychodz�ca zalacznik - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.DELETE,"PLG_READ_DELETE",ObjectPermission.GROUP,"Korespondencja przychodz�ca - usuwanie"));

		return perms;
	}
	
	
	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		super.setInitialValues(fm, type);
		
		Box box = PlgFactory.findOrCreateBox();	
		Map<String, Object> values = new HashMap<String, Object>();
		values.put(_ODBIORCA_DICT, 1);
		fm.reloadValues(values);
	}

	/**
	 * Metoda u�ywana podczas dekretacji
	 */
	@Override
	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution,	String acceptationCn, String fieldCnValue)
	{
		boolean assignee = false;
		
		try
		{
			//konsultant dekretacja
			if(_KONSULTANT_ACC_CN.equals(acceptationCn))
			{
				Long userId = Long.parseLong(openExecution.getVariable("userId").toString());
				String username = DSUser.findById(userId).getName();
				assignable.addCandidateUser(username);
				if (AvailabilityManager.isAvailable("addToWatch")) {
					DSApi.context().watch(URN.create(doc),username);
				}
				assignee = true;
			}
			
		}catch(Exception e){
			log.error("", e);
		}
		
		return assignee;
	}

	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		try
		{
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			fm.initialize();
			
			// czas wygenerowania metryki
			values.put("GENERATE_DATE", DateUtils.formatCommonDate(new java.util.Date()).toString());
			values.put("DATA_DOKUMENTU", DateUtils.formatCommonDate((Date)fm.getValue(_DATA)).toString());
			
			Long userId = fm.getLongKey(_USER_DICT);
			if(userId != null)
				values.put("PROCEDUJACY", DSUser.findById(userId).asFirstnameLastname());
			else
				values.put("PROCEDUJACY", "");
			
			Long nadawcaId = fm.getLongKey("NADAWCA_DICT");
			if(nadawcaId != null)
			{
				Map<String,Object> nadawca = DwrDictionaryFacade.dictionarieObjects.get("NADAWCA_DICT").getValues(nadawcaId.toString());
				values.put("NADAWCA_NAZWA", nadawca.get("NADAWCA_DICT_NAZWA"));
				values.put("NADAWCA_ULICA", nadawca.get("NADAWCA_DICT_ULICA"));
				values.put("NADAWCA_POCZTA", nadawca.get("NADAWCA_DICT_POCZTA"));
				values.put("NADAWCA_MIASTO", nadawca.get("NADAWCA_DICT_MIASTO"));
			}
			else
			{
				values.put("NADAWCA_NAZWA", "");
				values.put("NADAWCA_ULICA", "");
				values.put("NADAWCA_POCZTA", "");
				values.put("NADAWCA_MIASTO", "");
			}
			
			Boolean zostaje = fm.getBoolean("ZOSTAJE_W_PORTCIE");
			if(zostaje)
				values.put("ZOSTAJE", "TAK");
			else
				values.put("ZOSTAJE", "NIE");
			
			Long odbiorcaId = fm.getLongKey("ODBIORCA_DICT");
			if(odbiorcaId != null)
			{
				Map<String,Object> odbiorca = DwrDictionaryFacade.dictionarieObjects.get("ODBIORCA_DICT").getValues(odbiorcaId.toString());
				values.put("ODBIORCA_NAZWA", odbiorca.get("ODBIORCA_DICT_NAZWA"));
				values.put("ODBIORCA_ULICA", odbiorca.get("ODBIORCA_DICT_ULICA"));
				values.put("ODBIORCA_POCZTA", odbiorca.get("ODBIORCA_DICT_POCZTA"));
				values.put("ODBIORCA_MIASTO", odbiorca.get("ODBIORCA_DICT_MIASTO"));
			}
			else
			{
				values.put("ODBIORCA_NAZWA", "");
				values.put("ODBIORCA_ULICA", "");
				values.put("ODBIORCA_POCZTA", "");
				values.put("ODBIORCA_MIASTO", "");
			}
			Long pudloId = fm.getLongKey("PUDLO_DICT");
			if(pudloId != null)
			{
				Map<String,Object> pudlo = DwrDictionaryFacade.dictionarieObjects.get("PUDLO_DICT").getValues(pudloId.toString());
				values.put("PUDLO", pudlo.get("PUDLO_DICT_NUMER"));
			}
			
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
	}
	
//	@Override
//	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException 
//	{
//		log.info("DokumentKind: {}, DocumentId: {}, Values from JavaScript: {}", fm.getDocumentKind().getCn(), fm.getDocumentId(), values);
//		
//		//jesli wybral miasto w DOZIE to zwraca przydzielony mu id adresata.
//		FieldData lider = values.get("DWR_" + _USER_DICT);
//		FieldData procedujacy = values.get("DWR_"+_SUBPROCEDUJACY_DICT);
//		
//		System.out.println("lider: " + lider.getData());
//		System.out.println("procedujacy: " + procedujacy.getData());
//		if(lider != null && lider.getData() != null && lider.getData() instanceof Map &&
//		   procedujacy != null && procedujacy.getData() != null)
//		{
//			Long liderId = Long.valueOf((String)((FieldData)((Map)lider.getData()).get("id")).getData());
//			
//			Map<String,FieldData> procedujacyValues = (Map<String,FieldData>) procedujacy.getData();
//			int i = 0;
//			boolean ok = true;
//			while(ok){
//				i++;
//				FieldData fd = procedujacyValues.get(_SUBPROCEDUJACY_DICT + "_ID_" + i);
//				// nie ma nastepnego koncze p�tle
//				if(fd == null) 
//					break;
//				Long prodId = ((Integer)fd.getData()).longValue();
//				if(prodId != null && liderId != null && prodId.equals(liderId))
//				{
//					values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
//					return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField","U�ytkownik jest ju� wyznaczony jako lider!", pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
//				}
//			}
//		}
//		
//		return null;
//	}


	@Override
	public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
	{
		log.info("DokumentKind: {}, DocumentId: {}, now: {}", kind.getCn(), documentId, values);
		
		Long liderId = (Long) values.get(_USER_DICT);
		List<Long> osobyProcedujace = new ArrayList<Long>();
		if(liderId != null)
		{
			Map<String, Object> multiValue = (Map<String, Object>) values.get("M_DICT_VALUES");
			if(multiValue!=null)
			{
				int i = 1;
				while(multiValue.containsKey(_SUBPROCEDUJACY_DICT + "_" +i))
				{
					Map<String, FieldData> procedujacy = (Map<String, FieldData>) multiValue.get(_SUBPROCEDUJACY_DICT + "_" +i);
					if(procedujacy != null && procedujacy.containsKey("ID") && !StringUtils.isBlank(((String)procedujacy.get("ID").getData())))
					{
						Long procId = Long.valueOf((String)procedujacy.get("ID").getData());
						if(osobyProcedujace.contains(procId))
							throw new EdmException("Na li�cie proceduj�cych osoby powtarzaj� si� wielokrotnie!");
						
						osobyProcedujace.add(procId);
						if(procId != null && procId.equals(liderId))
							throw new EdmException("U�ytkownik procedujacy jest ju� wyznaczony jako lider!");
					}
					i++;
				}
			}
		}
		
		
	}
	
}
