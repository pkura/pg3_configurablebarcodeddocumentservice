package pl.compan.docusafe.parametrization.plg;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Maps;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

public abstract class AbstractPlgLogic extends AbstractDocumentLogic
{
	private final static Logger log = LoggerFactory.getLogger(AbstractPlgLogic.class);
	public final static String _TYP = "TYP";
	public final static String _RODZAJ = "RODZAJ";
	public final static String _STATUS ="STATUS";
	public final static String _DATA = "PLG_DATA";
	public final static String _USER_DICT = "USER_DICT";
	public final static String _SUBPROCEDUJACY_DICT = "SUBPROCEDUJACY_DICT";
	public final static String _KONSULTANT_DICT = "KONSULTANT_DICT";
	public final static String _ODBIORCA_DICT = "ODBIORCA_DICT";
	public final static String _KONSULTANT_ACC_CN = "konsultant";
	
	protected abstract Set<PermissionBean> getPermissionBeans();
	
	@Override
	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = getPermissionBeans();   	 	
   	 	for (PermissionBean perm : perms)    		
   	 	{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
   	 	}
	}
	
	
	@Override
	public boolean canChangeDockind(Document document) throws EdmException
	{
		return false;
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException 
	{	
		super.setInitialValues(fm, type);
		
		Map<String, Object> values = new HashMap<String, Object>();
		values.put(_DATA, Calendar.getInstance().getTime());
		//status nowy
//		values.put(_STATUS, 5);
		fm.reloadValues(values);
	}
	
	@Override
	public void onStartProcess(OfficeDocument document, ActionEvent event) throws EdmException 
	{
		log.info("--- PlgLogic : START PROCESS !!! ---- {}", event);
		try
		{
			FieldsManager fm = document.getFieldsManager();
			Map<String, Object> map = Maps.newHashMap();
			
			map.put(ASSIGN_USER_PARAM, DSApi.context().getPrincipalName());
			map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
			
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			
			if (AvailabilityManager.isAvailable("addToWatch")) {
				DSApi.context().watch(URN.create(document));
			}
			
		}catch(Exception e)
		{
			log.error("", e);
			throw new EdmException(e.getMessage());
		}
	}
}
