package pl.compan.docusafe.parametrization.plg;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Box;
import pl.compan.docusafe.core.base.EntityNotFoundException;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.parametrization.opzz.dictionary.OrganizationUserDictionary;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

/**
 * Fabryka do dokument�w PLG
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 */
public class PlgFactory implements java.io.Serializable
{
	private final static Logger log = LoggerFactory.getLogger(PlgFactory.class);
	/** Nadanie numeru KO i przypinanie dokumentu do dziennika dokument�w wewn�trznych, wychodz�cych lub przychodz�cych.
	 * @param document - dokument, kt�rego operacja dotyczy
	 * @param event - mo�e by� null
	 * @param typDziennika - sta�a tekstowa z klasy Journal (INTERNAL,INCOMING,OUTGOING) */
	public static void bindToJournal(OfficeDocument document, ActionEvent event, String typDziennika) throws EdmException {
		
		Journal journal = null;
		if (typDziennika==null)
			journal = Journal.getMainInternal();
		
		else if (typDziennika.equals(Journal.INTERNAL) )
			journal = Journal.getMainInternal();
		
		else if (typDziennika.equals(Journal.INCOMING) )
			journal = Journal.getMainIncoming();
		
		else if (typDziennika.equals(Journal.OUTGOING) )
			journal = Journal.getMainOutgoing();
		
		Long journalId = journal.getId();
		DSApi.context().session().flush();
		Date entryDate = new Date();
		
		Integer sequenceId = Journal.TX_newEntry2(journalId, document.getId(), entryDate);
				
		if (typDziennika.equals(Journal.INCOMING) )
		{
			InOfficeDocument doc = (InOfficeDocument) document;
			doc.bindToJournal(journalId, sequenceId);
		}
		else
		{
			OutOfficeDocument doc = (OutOfficeDocument) document;
			doc.bindToJournal(journalId, sequenceId, null, event);
			doc.setOfficeDate(entryDate);
		}
	}
	
	/**
	 * Wyszukuje nadawc� po externanId
	 * @param id
	 * @throws EdmException
	 */
	public static final PlgNadawca findNadawcaByExternalId(Long id) throws EdmException
	{
		Criteria criteria = DSApi.context().session().createCriteria(PlgNadawca.class);

		if (id != null)
		{
			criteria.add(Restrictions.eq("externalId", id));
			
			List<PlgNadawca> results = criteria.list();
			
			if(results != null && results.size() > 0)
				return results.get(0);
			else
				return null;
		}
		else
			return null;
	}

	/**
	 * Zwraca pud�o je�li istnieje inaczej tworzy nowe
	 * @return
	 * @throws EdmException 
	 */
	public static Box findOrCreateBox() throws EdmException
	{
		String name = new SimpleDateFormat("MM/yyyy").format(Calendar.getInstance().getTime());
		Box box = null;
		try
		{
			box = Box.findByName(null, name);
			return box;
		} catch (EntityNotFoundException e){
			log.error("Brak pud�a {}, tworze", name);
			box = new Box(name);
			box.create();
		}
		return box;
	}
}
