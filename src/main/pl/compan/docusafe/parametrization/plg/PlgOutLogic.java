package pl.compan.docusafe.parametrization.plg;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Maps;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.pika.PikaLogic;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;

/**
 * Klasa logiki dla dokumentu PLG
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class PlgOutLogic extends AbstractPlgLogic
{
	private final static Logger log = LoggerFactory.getLogger(PlgOutLogic.class);
	
	private static PlgOutLogic instance;
	
    public static PlgOutLogic getInstance() {
        if (instance == null)
            instance = new PlgOutLogic();
        return instance;
    }
    
    @Override
	protected Set<PermissionBean> getPermissionBeans()
	{
		Set<PermissionBean> perms = new HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ,"PLG_OUT_READ",ObjectPermission.GROUP,"Korespondencja wychodz�ca - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"PLG_OUT_READ_ATT_READ",ObjectPermission.GROUP,"Korespondencja wychodz�ca zalacznik - odczyt"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY,"PLG_OUT_READ_MODIFY",ObjectPermission.GROUP,"Korespondencja wychodz�ca - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"PLG_OUT_READ_ATT_MODIFY",ObjectPermission.GROUP,"Korespondencja wychodz�ca zalacznik - modyfikacja"));
   	 	perms.add(new PermissionBean(ObjectPermission.DELETE,"PLG_OUT_READ_DELETE",ObjectPermission.GROUP,"Korespondencja wychodz�ca - usuwanie"));

		return perms;
	}
    
	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		Folder rootFolder = Folder.getRootFolder();
		rootFolder = rootFolder.createSubfolderIfNotPresent("Korespondencja wychodz�ca");
		
		document.setFolder(rootFolder);
	}

	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		try
		{
			OfficeDocument doc = OfficeDocument.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			fm.initialize();
			
			// czas wygenerowania metryki
			values.put("GENERATE_DATE", DateUtils.formatCommonDate(new java.util.Date()).toString());
			values.put("DATA_DOKUMENTU", DateUtils.formatCommonDate((Date)fm.getValue(_DATA)).toString());
			
			Long nadawcaId = fm.getLongKey("NADAWCA_DICT");
			if(nadawcaId != null)
			{
				Map<String,Object> nadawca = DwrDictionaryFacade.dictionarieObjects.get("NADAWCA_DICT").getValues(nadawcaId.toString());
				values.put("NADAWCA_NAZWA", nadawca.get("NADAWCA_DICT_NAZWA"));
				values.put("NADAWCA_ULICA", nadawca.get("NADAWCA_DICT_ULICA"));
				values.put("NADAWCA_POCZTA", nadawca.get("NADAWCA_DICT_POCZTA"));
				values.put("NADAWCA_MIASTO", nadawca.get("NADAWCA_DICT_MIASTO"));
			}
			else
			{
				values.put("NADAWCA_NAZWA", "");
				values.put("NADAWCA_ULICA", "");
				values.put("NADAWCA_POCZTA", "");
				values.put("NADAWCA_MIASTO", "");
			}
			
			Boolean zostaje = fm.getBoolean("ZOSTAJE_W_PORTCIE");
			if(zostaje)
				values.put("ZOSTAJE", "TAK");
			else
				values.put("ZOSTAJE", "NIE");
			
			Long odbiorcaId = fm.getLongKey("ODBIORCA_DICT");
			if(odbiorcaId != null)
			{
				Map<String,Object> odbiorca = DwrDictionaryFacade.dictionarieObjects.get("ODBIORCA_DICT").getValues(odbiorcaId.toString());
				values.put("ODBIORCA_NAZWA", odbiorca.get("ODBIORCA_DICT_NAZWA"));
				values.put("ODBIORCA_ULICA", odbiorca.get("ODBIORCA_DICT_ULICA"));
				values.put("ODBIORCA_POCZTA", odbiorca.get("ODBIORCA_DICT_POCZTA"));
				values.put("ODBIORCA_MIASTO", odbiorca.get("ODBIORCA_DICT_MIASTO"));
			}
			else
			{
				values.put("ODBIORCA_NAZWA", "");
				values.put("ODBIORCA_ULICA", "");
				values.put("ODBIORCA_POCZTA", "");
				values.put("ODBIORCA_MIASTO", "");
			}
			
			Long pudloId = fm.getLongKey("PUDLO_DICT");
			if(pudloId != null)
			{
				Map<String,Object> pudlo = DwrDictionaryFacade.dictionarieObjects.get("PUDLO_DICT").getValues(pudloId.toString());
				values.put("PUDLO", pudlo.get("PUDLO_DICT_NUMER"));
			}
			
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
	}
}
