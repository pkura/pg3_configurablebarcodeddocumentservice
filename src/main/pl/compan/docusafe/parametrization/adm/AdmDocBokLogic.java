package pl.compan.docusafe.parametrization.adm;

import com.google.common.collect.Maps;


import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.DwrDocumentHelper;
import pl.compan.docusafe.webwork.event.ActionEvent;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.*;

@SuppressWarnings("serial")
public class AdmDocBokLogic extends AbstractDocumentLogic
{
	public final static String _USER = "DOC_AUTOR";
	public final static String _DIVISION = "DOC_AUTORDIVISION";
	public static final String _odbiorca = "RECIPIENT";
	
	

	protected static DwrDocumentHelper dwrDocumentHelper = new DwrDocumentHelper();
	

	private static AdmDocBokLogic instance;
	protected static Logger log = LoggerFactory.getLogger(AdmDocInLogic.class);

	 
	public static AdmDocBokLogic getInstance()
	{
		if (instance == null)
			instance = new AdmDocBokLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	//nowa kolumna na liscie zadan 
	public TaskListParams getTaskListParams(DocumentKind dockind, long id)
			throws EdmException {
		TaskListParams params = new TaskListParams();

		try {
			
			} catch (Exception e) {
			log.error("", e);
		}
		return params;
	}
	
	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
	{
		Field msg = null;
		
		if (values.get("DWR_DOC_DATE") != null && values.get("DWR_DOC_DATE").getData() != null)
		{
			Date startDate = new Date();
			Date finishDate = values.get("DWR_DOC_DATE").getDateData();
			
			if (finishDate.after(startDate))
			{
				msg = new Field("messageField", "Data pisma nie mo瞠 by� dat� z przysz這�ci.", Field.Type.BOOLEAN);
				values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
			}
		
		}

		return msg;
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		
		log.info("type: {}", type);
		Map<String, Object> values= new HashMap<String, Object>();
		values.put("TYPE", type);
		values.put(_odbiorca, Long.parseLong(Docusafe.getAdditionProperty("DomyslnyOdbiorcaInNadawcaOut")));
		fillCreatingUser(values);
		
		fm.reloadValues(values);
			
	}
	public boolean searchCheckPermissions(Map<String,Object> values)
    {
        return true;
    }
	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
	}
	
	
	
	public Map<String, String> setInitialValuesForReplies(Long inDocumentId) throws EdmException
	{
		Map<String, String> dependsDocs = new HashMap<String, String>();
	
	
		return dependsDocs;
	}
	
	
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		as.useFolderResolver(document);
		as.useAvailableProviders(document);
		log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
	}

	@Override
	public String onStartManualAssignment(OfficeDocument doc, String fromUsername, String toUsername)
	{
		log.info("--- AdmDocBokLogic : onStartManualAssignment  dekretacja reczna dokument闚 :: dokument ID = "+doc.getId()+" : pomiedzy nadawc� -->"+fromUsername +" a odbiorc� ->"+toUsername );
		return toUsername;
	
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- AdmDocBokLogic : START PROCESS !!! ---- {}", event);
		try {

			Map<String, Object> map = Maps.newHashMap();
	
			DSDivision[] divs = DSApi.context().getDSUser().getOriginalDivisionsWithoutGroup();
			if (divs.length>0){
			for (DSDivision  d: divs){
				map.put(ASSIGN_DIVISION_GUID_PARAM,d.getGuid()!=null ? d.getGuid() : DSDivision.ROOT_GUID);
			}
			}else 
				map.put(ASSIGN_DIVISION_GUID_PARAM,DSDivision.ROOT_GUID);
			
			map.put(ASSIGN_USER_PARAM, DSApi.context().getDSUser().getName());
			document.getDocumentKind()
					.getDockindInfo()
					.getProcessesDeclarations()
					.onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

	}
	 
	@Override
	public void validate(Map<String, Object> values, DocumentKind kind,Long documentId) throws EdmException {
		
		System.out.println(kind.getCn());
		System.out.println(values);
	
	}



	@Override
	public void onStartProcess(OfficeDocument document)
	{
	
		try {
			Map<String, Object> map= Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, document.getAuthor());
			map.put(ASSIGN_DIVISION_GUID_PARAM, document.getDivisionGuid());
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	
	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret= new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	@Override
	public void onRejectedListener(Document doc) throws EdmException
	{
	}

	@Override
	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException
	{
	}
	
	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException,
			EdmException
	{ 
		
		
	}
	private void fillCreatingUser(Map<String, Object> values) throws UserNotFoundException, EdmException
	{
		Long userDivision = (long) 1;
		DSUser user = DSApi.context().getDSUser();


		DSDivision[] divisions = user.getOriginalDivisions();

		for (int i = 0; i < divisions.length; i++)
		{
			if (divisions[i].isPosition())
			{
				
				DSDivision parent = divisions[i].getParent();
				if (parent != null)
				{
					userDivision = parent.getId();
					break;
				} else
				{
					userDivision = divisions[i].getId();
				}
			} else if (divisions[i].isDivision())
			{
				userDivision = divisions[i].getId();
				break;
			}
		}
		values.put(_USER, DSApi.context().getDSUser().getId());
		values.put(_DIVISION, userDivision);
		
	}

}
	
	