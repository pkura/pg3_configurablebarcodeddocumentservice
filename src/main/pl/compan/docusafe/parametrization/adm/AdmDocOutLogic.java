package pl.compan.docusafe.parametrization.adm;

import com.google.common.collect.Maps;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.DwrDocumentHelper;
import pl.compan.docusafe.webwork.event.ActionEvent;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;



import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.*;

@SuppressWarnings("serial")
public class AdmDocOutLogic extends AbstractDocumentLogic
{
	
	

	protected static DwrDocumentHelper dwrDocumentHelper = new DwrDocumentHelper();
	
	public static final String multiOdbiorca = "M_DICT_VALUES";
	public static final String recipientID = "ID";
	private static final String czystopisDwr = "DWR_DOC_OUT_OFFICEPREPARESTATE";
	private static final String czystopis = "DOC_OUT_OFFICEPREPARESTATE";
	public static final String _dataDokumentu = "DOC_DATE";
	public static final String _dataDokumentuDwr = "DWR_DOC_DATE";
	public final static String _USER = "DOC_AUTOR";
	public final static String _DIVISION = "DOC_AUTORDIVISION";
	public static final String _nadawca = "SENDER";
	public static final String _sposobOdbioru = "DOC_DELIVERY";
	private static AdmDocOutLogic instance;
	protected static Logger log = LoggerFactory.getLogger(AdmDocOutLogic.class);

	 
	public static AdmDocOutLogic getInstance()
	{
		if (instance == null)
			instance = new AdmDocOutLogic();
		return instance;
	}

	public void documentPermissions(Document document) throws EdmException
	{
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms)
		{
			if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP))
			{
				String groupName = perm.getGroupName();
				if (groupName == null)
				{
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
	}

	//nowa kolumna na liscie zadan 
	public TaskListParams getTaskListParams(DocumentKind dockind, long id)
			throws EdmException {
		TaskListParams params = new TaskListParams();

		try {
			
			} catch (Exception e) {
			log.error("", e);
		}
		return params;
	}
	
	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		Field msg = null;
		
		if (values.get(_dataDokumentuDwr) != null && values.get(_dataDokumentuDwr).getData() != null)
		{
			Date startDate = new Date();
			Date finishDate = values.get(_dataDokumentuDwr).getDateData();
			
			if (finishDate.after(startDate))
			{
				msg = new Field("messageField", "Data pisma nie mo瞠 by� dat� z przysz這�ci.", Field.Type.BOOLEAN);
				values.put("messageField", new FieldData(Field.Type.BOOLEAN, true));
			}
		
		}
		/*if (values.get(czystopisDwr)!=null
				&& (values.get(czystopisDwr).getEnumValuesData().getSelectedId().contains("1")))
				msg = new Field("messageField", "W pismie oznaczonym jako czystpis nie mo積a dodawa� odbiorc闚", Field.Type.BOOLEAN);
				*/
		return msg;
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		
		log.info("type: {}", type);
		Map<String, Object> values= new HashMap<String, Object>();
		values.put("TYPE", type);
		if(fm.getField(czystopis)!=null)
		values.put(czystopis,2);
		if(fm.getField(_nadawca)!=null)
		values.put(_nadawca, Long.parseLong(Docusafe.getAdditionPropertyOrDefault("DomyslnyOdbiorcaInNadawcaOut", "1")));
		if(fm.getField("JOURNAL")!=null)
		fm.getField("JOURNAL").setHidden(true);
		if(fm.getField("DOC_REFERENT")!=null)
		fm.getField("DOC_REFERENT").setHidden(true);
		fillCreatingUser(values);
		fm.reloadValues(values);
		
	
			
	}
	public boolean searchCheckPermissions(Map<String,Object> values)
    {
        return true;
    }
	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values)
	{
		
	}
	
	
	
	public Map<String, String> setInitialValuesForReplies(Long inDocumentId) throws EdmException
	{
		Map<String, String> dependsDocs = new HashMap<String, String>();
	
	
		return dependsDocs;
	}
	
	
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		as.useFolderResolver(document);
		as.useAvailableProviders(document);
		log.info("document title : '{}', description : '{}'", document.getTitle(), document.getDescription());
	}

	@Override
	public String onStartManualAssignment(OfficeDocument doc, String fromUsername, String toUsername)
	{
		log.info("--- AdmDocOutLogic : onStartManualAssignment  dekretacja reczna dokument闚 :: dokument ID = "+doc.getId()+" : pomiedzy nadawc� -->"+fromUsername +" a odbiorc� ->"+toUsername );
		return toUsername;
	
	}

	public void onStartProcess(OfficeDocument document, ActionEvent event)
	{
		log.info("--- AdmDocOutLogic : START PROCESS !!! ---- {}", event);
		try {

			Map<String, Object> map = Maps.newHashMap();
			
			DSDivision[] divs = DSApi.context().getDSUser().getOriginalDivisionsWithoutGroup();
			if (divs.length>0){
			for (DSDivision  d: divs){
				map.put(ASSIGN_DIVISION_GUID_PARAM,d.getGuid()!=null ? d.getGuid() : DSDivision.ROOT_GUID);
			}
			}else 
				map.put(ASSIGN_DIVISION_GUID_PARAM,DSDivision.ROOT_GUID);
			
			map.put(ASSIGN_USER_PARAM, DSApi.context().getDSUser().getName());
			document.getDocumentKind()
					.getDockindInfo()
					.getProcessesDeclarations()
					.onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

	}
	 
	@Override
	public void validate(Map<String, Object> values, DocumentKind kind,Long documentId) throws EdmException {
	
		System.out.println(kind.getCn());
		System.out.println(values);
		if (documentId!=null)
			validateRecipientInDocument(values, documentId);
	
	}
	private void fillCreatingUser(Map<String, Object> values) throws UserNotFoundException, EdmException
	{
		Long userDivision = (long) 1;
		DSUser user = DSApi.context().getDSUser();

		DSDivision[] divisions = user.getOriginalDivisions();

		for (int i = 0; i < divisions.length; i++)
		{
			if (divisions[i].isPosition())
			{
				
				DSDivision parent = divisions[i].getParent();
				if (parent != null)
				{
					userDivision = parent.getId();
					break;
				} else
				{
					userDivision = divisions[i].getId();
				}
			} else if (divisions[i].isDivision())
			{
				userDivision = divisions[i].getId();
				break;
			}
		}
		values.put(_USER, DSApi.context().getDSUser().getId());
		values.put(_DIVISION, userDivision);
		
	}
	private Recipient utworzRecipient(Person person)
	{
	Recipient recipient = new Recipient();
	
	try
	{
		recipient.fromMap(person.toMap());
		recipient.setBasePersonId(person.getId());
		recipient.create();
		
	} catch (EdmException e)
	{
		log.error("", e);
	}
	
	return recipient;
		
	}

	private static void removeDuplicates(List<Long> recipientsToRemove) {
        HashSet set = new HashSet(recipientsToRemove);
        recipientsToRemove.clear();
        recipientsToRemove.addAll(set);
}
	private void validateRecipientInDocument(Map<String, Object> values, Long documentId) throws DocumentNotFoundException, EdmException
	{

		OutOfficeDocument doc = null;
		doc = (OutOfficeDocument) OutOfficeDocument.find(documentId);
		List<Long> selectetPersonIds = new ArrayList();
		Map<String, FieldData> odbiorcy = (Map<String, FieldData>) values.get(multiOdbiorca);
		if (odbiorcy != null)
		{
				for (String key : odbiorcy.keySet())
				{
					Map<String, FieldData> wpisMulti = (Map<String, FieldData>) odbiorcy.get(key);
					selectetPersonIds.add(wpisMulti.get(recipientID).getLongData());
				}
				if (isAnyInvalidRecipients(selectetPersonIds, doc))
					createValidRecipient(doc, selectetPersonIds);
			}
	}

	private boolean isAnyInvalidRecipients( List<Long> selectetPersonIds, OutOfficeDocument doc) throws EdmException
	{
		List<Long> wybrane = new ArrayList();
		wybrane.addAll(selectetPersonIds);
		List<Long> basePersonIdsRecipients = getListBasePersonIdRecipiets(doc);
		basePersonIdsRecipients.removeAll(wybrane);
				if(!basePersonIdsRecipients.isEmpty())
					return true;
				else {
					wybrane.removeAll(getListBasePersonIdRecipiets(doc));
					if (!wybrane.isEmpty())
						return true;
				}
			
		return false;
		
		
	}
		

	/** Nadanie numeru KO i przypinanie dokumentu do dziennika  wychodz鉍ych 
	* @param document - dokument, kt鏎ego operacja dotyczy
	
	*/
	
	public void bindToJournal(OfficeDocument oficeDocument ,ActionEvent event) throws EdmException
	{/*
		OutOfficeDocument doc = (OutOfficeDocument) oficeDocument;
		Date entryDate = new Date();
		Journal journal = Journal.getMainOutgoing();
		Integer sequenceId = Journal.TX_newEntry2(journal.getId(), doc.getId(), entryDate);
		doc.setOfficeDate(entryDate);*/
	}

	
	private void createValidRecipient(OutOfficeDocument doc, List<Long> selectetPersonIds) throws EdmException
	{

		//selectetPersonIds - id perso闚 z dokinda w s這wniku multi
		
		List<Recipient> newListRecipient = new ArrayList();
		//lista person闚 z kt鏎ych utworzeni zostali odbiorcy danego dokumentu
		List<Long> basePersonIdsRecipients = getListBasePersonIdRecipiets(doc);
		//lista id person z kt鏎eg otrzeba utworzy� odbiorce i doda� do dokumentu 
		List<Long> personToCreate = new  ArrayList();
		//lista odbiorc闚 dokumentu do usuni璚ia 
		List<Long> recipientsToRemove = getListBasePersonIdRecipiets(doc);
		removeDuplicates(recipientsToRemove);
		//wyciagniecie recipienta kt鏎ego trezeaba usun寞 bo nie jest zgodny z dokumentem
		recipientsToRemove.removeAll(selectetPersonIds);
		
		//usuniecie wszytkich id wybranych person闚 z kt鏎ych utworzeni s� juz odbiorcy kt鏎zy s� zgodni z tymi co na dokumencie 
		selectetPersonIds.removeAll(basePersonIdsRecipients);
		
		for (Long l :selectetPersonIds)
		personToCreate.add(l);
		
		Iterator<Recipient> iter = doc.getRecipients().listIterator();
		while (iter.hasNext())
		{
			Recipient rec = iter.next();
			for (Long l : recipientsToRemove)
			{
				if (rec.getBasePersonId().compareTo(l) == 0){
					iter.remove();
					rec.delete();
				}
			}
		}
		
		for (Long l : personToCreate){	
			doc.addRecipient(utworzRecipient(Person.findIfExist(l)));
		}
	}

	private List<Long> getListBasePersonIdRecipiets(OutOfficeDocument doc) throws EdmException
	{
		List<Long> list = new ArrayList();
		for (Recipient rec : doc.getRecipients()){
			list.add(rec.getBasePersonId());
			}
		return list;
	}

	public void specialSetDictionaryValues(Map<String, ?> dockindFields)
	{
		log.info("specialSetDictionaryValues: \n {} ", dockindFields);

		if (dockindFields.keySet().contains("RECIPIENT_DICTIONARY"))
		{
			Map<String, FieldData> m = new HashMap<String, FieldData>();
			m.put("DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			m.put("ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}

	}

	public void addSpecialInsertDictionaryValues(String dictionaryName, Map<String, FieldData> values)
	{
		if (dictionaryName.contains("RECIPIENT"))
		{
			values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}

	}

	@Override
	public void addSpecialSearchDictionaryValues(String dictionaryName, Map<String, FieldData> values, Map<String, FieldData> dockindFields)
	{
		if (dictionaryName.contains("RECIPIENT"))
		{
			values.put(dictionaryName + "_DISCRIMINATOR", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "PERSON"));
			values.put(dictionaryName + "_ANONYMOUS", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING, "0"));
		}
	}


	@Override
	public void onStartProcess(OfficeDocument document)
	{
	
		try {
			Map<String, Object> map= Maps.newHashMap();
			map.put(ASSIGN_USER_PARAM, document.getAuthor());
			map.put(ASSIGN_DIVISION_GUID_PARAM, document.getDivisionGuid());
			document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
			if (AvailabilityManager.isAvailable("addToWatch"))
				DSApi.context().watch(URN.create(document));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	
	@Override
	public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException
	{
		ProcessCoordinator ret= new ProcessCoordinator();
		ret.setUsername(document.getAuthor());
		return ret;
	}

	@Override
	public void onRejectedListener(Document doc) throws EdmException
	{
	}

	@Override
	public void onAcceptancesListener(Document doc, String acceptationCn) throws EdmException
	{
	}
	
	@Override
	public void onSaveActions(DocumentKind kind, Long documentId, Map<String, ?> fieldValues) throws SQLException,
			EdmException
	{ 
		
		
	}
	
}
	
	