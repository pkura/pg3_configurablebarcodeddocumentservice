package pl.compan.docusafe.parametrization.adm;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.UserToBriefcase;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


public class MigrationDataFactoryForAdm extends AbstractDocumentLogic 
{
	protected static Logger log = LoggerFactory.getLogger(MigrationDataFactoryForAdm.class);
	public static long numRows = 0;
	@Override
	public void documentPermissions(Document document) throws EdmException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException
	{
		// TODO Auto-generated method stub
		
	}
	private Long getIdUserDivision(DSUser user) throws EdmException
	{
		Long userDivision = (long) 1;
		
	
		DSDivision[] divisions = user.getOriginalDivisions();

		for (int i = 0; i < divisions.length; i++)
		{
			if (divisions[i].isPosition())
			{
				
				DSDivision parent = divisions[i].getParent();
				if (parent != null)
				{
					userDivision = parent.getId();
					break;
				} else
				{
					userDivision = divisions[i].getId();
				}
			} else if (divisions[i].isDivision())
			{
				userDivision = divisions[i].getId();
				break;
			}
		}
		return userDivision;
	}

	public void updateDocumentData()
	{
		long docid = 0;
		String autor="";
		boolean zly = false;
		try
		{
			
			DocumentKind inkind = DocumentKind.find(Long.parseLong(Docusafe.getAdditionProperty("IdDokindaPrzychodzacego")));
			DocumentKind intkind = DocumentKind.find(Long.parseLong(Docusafe.getAdditionProperty("IdDokindaWewnetrznego")));
			DocumentKind outkind = DocumentKind.find(Long.parseLong(Docusafe.getAdditionProperty("IdDokindaWychodzacego")));
			DocumentKind bokkind = DocumentKind.find(Long.parseLong(Docusafe.getAdditionProperty("IdDokindaBok")));
			
			
			List<OfficeDocument> list = new ArrayList<OfficeDocument>();

			PreparedStatement pst = null;

			pst = DSApi.context().prepareStatement("select max (id) from DS_ADM_OUT_DOC_MULTIPLE");
			ResultSet rss = pst.executeQuery();
			while (rss.next())
			{numRows=0;
				numRows = rss.getLong(1) + 1;
			}
			rss.close();

			String selecDocuments = " Select ID from  DS_DOCUMENT  where deleted =0 and id>? and id <=? order by id asc";
			
			pst = null;
			pst = DSApi.context().prepareStatement(selecDocuments);
			pst.setLong(1, Long.parseLong(Docusafe.getAdditionProperty("idOd")));
			pst.setLong(2, Long.parseLong(Docusafe.getAdditionProperty("idDo")));
			ResultSet rs = pst.executeQuery();
			int i = 0;
			while (rs.next())
			{
				list.add(OfficeDocument.find(rs.getLong(1)));
			}
			rs.close();

			for (OfficeDocument doc : list)
			{ zly = false;
				DSUser user;
				docid = doc.getId();
				String  baddocs= (String )Docusafe.getAdditionProperty("zleDokumenty");
				for (String s : baddocs.split(",")){
					if ( Long.parseLong(s) == docid)
						zly = true;
				}
				autor = doc.getAuthor();
				i++;
				try {
				if (doc.getAuthor()!=null)
				 user = DSUser.findByUsername(doc.getAuthor());
				else 
				 user = DSUser.findByUsername("admin");
				
				}catch (Exception e) {
					 user = DSUser.findByUsername("admin");
				}
				
				if (!zly && doc instanceof InOfficeDocument)
				{
					if (((InOfficeDocument) doc).isBok())
						doc.setDocumentKind(bokkind);
					else
						doc.setDocumentKind(inkind);

					updateInDoc(doc, user);

				} else if (!zly && doc instanceof OutOfficeDocument)
				{
					if (((OutOfficeDocument) doc).isInternal())
						doc.setDocumentKind(intkind);
					else{
						doc.setDocumentKind(outkind);
						updateRecipientsOutDoc(doc);
						}
					updateOutDoc(doc, user);
					
					
				}
				if(!zly &&  doc.getFolder()!=null&&doc.getFolderId()!=null)
					DSApi.context().session().saveOrUpdate(doc);
				
				if (i % 100 == 0)
				{
					log.error("Prerobilem dokument�w : " + i + "Czyscze sesje ", i);

					DSApi.context().commit();
					//				migrationLog.error("Wyczyszczenie sesji commit na idx: " + i);
					DSApi.context().session().flush();
					//				migrationLog.error("Wyczyszczenie sesji flush na idx: " + i);
					DSApi.context().session().clear();
					//				migrationLog.error("Wyczyszczenie sesji  clear na idx: " + i);
					DSApi.close();
					//				migrationLog.error("Wyczyszczenie sesji close na idx: " + i);
					DSApi.openAdmin();
					//				migrationLog.error("Wyczyszczenie sesji open na idx: " + i);
					DSApi.context().begin();
					//				migrationLog.error("Wyczyszczenie sesji begin na idx: " + i);
				}
			}
		} catch (Exception e)
		{
			
			log.error("Jaki� b�ad z sqlami ,Id dokumentu"+docid+" autor "+autor , e);
		}

	}

	private void updateRecipientsOutDoc(OfficeDocument doc ) throws SQLException, EdmException
	{
		String updMultiple = "insert into  DS_ADM_OUT_DOC_MULTIPLE  VALUES(?,?,?,?)";

		PreparedStatement pst = null;
		String selectbasePerson = "Select basePersonId  from DSO_PERSON where DISCRIMINATOR='RECIPIENT' and DOCUMENT_ID =";
		Long basepersonId = null;
		pst = null;
		pst = DSApi.context().prepareStatement(selectbasePerson + doc.getId());
		ResultSet rs = pst.executeQuery();
		if (rs.next())
		{
			basepersonId = rs.getLong(1);
			pst = null;
			pst = DSApi.context().prepareStatement(updMultiple);
			pst.setLong(1, this.numRows);
			pst.setLong(2, doc.getId());
			pst.setString(3, "RECIPIENT");
			if (basepersonId != null)
				pst.setLong(4, basepersonId);
			else
				pst.setObject(4, null);

			pst.execute();
			pst.close();
			numRows++;
		}

	}
		
	

	private void updateOutDoc(OfficeDocument doc, DSUser user) throws SQLException, EdmException
	{
		PreparedStatement pst = null;
		int anonim = 0;
		String updOut = "insert into   DS_ADM_OUT_DOC VALUES( ?,?,?,?)";
		String updInt = "insert into   DS_ADM_INT_DOC VALUES( ?,?,?,?)";
		if (((OutOfficeDocument) doc).isInternal())
			pst = DSApi.context().prepareStatement(updInt);
		else
			pst = DSApi.context().prepareStatement(updOut);
		pst.setLong(1, doc.getId());
		pst.setInt(2, anonim);
		pst.setLong(3, user.getId());
		pst.setLong(4, getIdUserDivision(user));
		pst.execute();
		pst.close();
		
	}

	private void updateInDoc(OfficeDocument doc ,DSUser user) throws SQLException, EdmException
	{
		String updIn = "insert into  DS_ADM_IN_DOC VALUES( ?,?,?,?)";
		PreparedStatement pst = null;
		int anonim = 0;
		if (((InOfficeDocument) doc).isSenderAnonymous())
			anonim = 1;
		else
			anonim = 0;
		
		pst = DSApi.context().prepareStatement(updIn);
		pst.setLong(1, doc.getId());
		pst.setInt(2, anonim);
		pst.setLong(3, user.getId());
		pst.setLong(4, getIdUserDivision(user));
		pst.execute();
		pst.close();
		
	}

}
