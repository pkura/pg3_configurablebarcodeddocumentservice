package pl.compan.docusafe.parametrization;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.security.auth.Subject;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSContext;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.sql.SubstituteHistory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.tasklist.TaskListAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

public abstract class AbstractParametrization
{
	private static AbstractParametrization instance;
	protected static Logger log = LoggerFactory.getLogger(AbstractParametrization.class);

	public static AbstractParametrization getInstance()
	{
		if (instance == null)
		{
			String className = Docusafe.getAdditionProperty("parametrization.class");
			try
			{
				instance = (AbstractParametrization) Class.forName(className).newInstance();
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
				throw new IllegalStateException("B��d inicjalizacji parametryzacji - klasa: " + className);
			}
		}
		return instance;
	}

	/***
	 * Metoda wywo�ywana po utowrzeniu persona
	 * 
	 * @throws EdmException
	 */
	public void setAfterCreatePerson(Person person) throws EdmException
	{

	}

	public void doTaskListAction(TaskListAction taskListAction, String eventName, ActionEvent event, Long[] docIds) throws EdmException
	{

	}

	public Map<String, String> getTaskListEvent()
	{
		return new HashMap<String, String>();
	}

	public void setBeforeUserDelete(String username, String toUser, DSContext ctx, Subject subject)throws EdmException
	{
		// TODO Auto-generated method stub
	}
	
	public void createDocFromOcr(Document doc, Map<String, Object> toReload, Long docId)throws IOException, DocumentException, ParseException, EdmException {}

	public void createDocFromAutostacja(Document doc, Map<String, Object> toReload)throws IOException, DocumentException, ParseException, EdmException {}

	/***
	 * Metoda wywo�ywane j�sli nie znalaz� uzytkoniwka o podanum loginie z bazy. 
	 * Moze s�u�y� do dodania uzytkoniwka z innej bazy .
	 * Zwraca nowo utworzonego uzytkownika 
	 * @param username
	 * @return
	 * @throws EdmException 
	 * @throws SQLException 
	 */
	public DSUser addUserIsNotExist(String username) throws EdmException, SQLException {
		return null;
	}

	/**
	 * Metoda sprawdza has�o u�ytkownika, metoda przeniesiona poniewa� zaimpotrowani uzytkownicy z innych baz 
	 * maja hasla zahashowane innym algorytmem. Domy�lnie jest wywo�ywana metoda user.verifyPassword(password)
	 * @param user
	 * @param password
	 * @return
	 */
	public boolean verifyPassword(DSUser user, String password) {
		return user.verifyPassword(password);
	}
/**
 * Metoda wywolywana po utworzeniu zastepstwa, wywolana z otwartym konteksem i transakcja 
 * @param history
 * @param event
 */
	public void afterAddSubstution(SubstituteHistory history,
			pl.compan.docusafe.web.common.event.ActionEvent event) {
		
		
	}

}
