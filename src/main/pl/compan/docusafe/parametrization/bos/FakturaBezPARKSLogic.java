package pl.compan.docusafe.parametrization.bos;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Map;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.workflow.ProcessCoordinator;
import pl.compan.docusafe.core.office.workflow.snapshot.TaskListParams;
import pl.compan.docusafe.core.record.projects.Project;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.ServletUtils;
import pl.compan.docusafe.web.commons.DockindButtonAction;
import pl.compan.docusafe.webwork.event.ActionEvent;

import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;

public class FakturaBezPARKSLogic extends AbstractDocumentLogic
{
	private static FakturaBezPARKSLogic instance;
    protected static Logger log = LoggerFactory.getLogger(FakturaBezPARKSLogic.class);


	
    public static FakturaBezPARKSLogic getInstance() {
        if (instance == null)
            instance = new FakturaBezPARKSLogic();
        return instance;
    }
	
	
	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String accpetionCn, String enumCn)
	{
		boolean assigned = false;
		try
		{
			log.info("DokumentKind: {}, DokumentId: {}', acceptationCn: {}, enumCn: {}", doc.getDocumentKind().getCn(), doc.getId(), accpetionCn, enumCn);

			if (accpetionCn.equals("kierownicyPAKS"))
			{
				
				String username = openExecution.getVariable("username").toString();
				assignable.addCandidateUser(username);
				System.out.println("username: " + username);
				assigned = true;
			}

		}
		catch (Exception ee)
		{
			log.error(ee.getMessage(), ee);
			return false;
		}
		return assigned;
	}
    
	    public TaskListParams getTaskListParams(DocumentKind dockind, long id) throws EdmException
		{
			TaskListParams params = new TaskListParams();
			try
			{
				FieldsManager fm = dockind.getFieldsManager(id);
				
				// Ustawienie statusy dokumentu
				params.setStatus(fm.getValue("STATUS") != null ? (String) fm.getValue("STATUS") : null);

				// Ustawienie numeru faktury jako numery dokumentu
				params.setDocumentNumber(fm.getValue("NUMER_FAKTURY") != null ? (String) fm.getValue("NUMER_FAKTURY") : null);
			}
			catch (Exception e)
			{
				log.error(e.getMessage(), e);
			}
			return params;
		}
	    
    public void documentPermissions(Document document) throws EdmException {
        java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
        perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
        perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
        perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));
 
        for (PermissionBean perm : perms) {
            if (perm.isCreateGroup() && perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP)) {
                String groupName = perm.getGroupName();
                if (groupName == null) {
                    groupName = perm.getSubject();
                }
                createOrFind(document, groupName, perm.getSubject());
            }
            DSApi.context().grant(document, perm);
            DSApi.context().session().flush();
        }
    }
	
	public void setInitialValues(FieldsManager fm, int type) throws EdmException
    {
		// Map<String,Object> values = new HashMap<String,Object>();
	}
	
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
    }

    @Override
    public void onStartProcess(OfficeDocument document, ActionEvent event) {
        log.info("--- NormalLogic : START PROCESS !!! ---- {}", event);
        try {
            Map<String, Object> map = Maps.newHashMap();
            map.put(ASSIGN_USER_PARAM, event.getAttribute(ASSIGN_USER_PARAM));
            map.put(ASSIGN_DIVISION_GUID_PARAM, event.getAttribute(ASSIGN_DIVISION_GUID_PARAM));
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document, map);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    public void validate(Map<String, Object> values, DocumentKind kind, Long documentId) throws EdmException
    {

    }


    public void onStartProcess(OfficeDocument document) throws EdmException
	{
    	log.info("START PROCESS ONE PARAMETER!!!");
        try {
            document.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(document);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
	}
    
	public void doDockindEvent(DockindButtonAction eventActionSupport, ActionEvent event,Document document, String activity,Map<String, Object> values) throws EdmException
	{
		log.info("Start GEN RAPORT BECK INVOICE");
		BufferedWriter out = null;
		try 
		{	
		 File file = new File("raportFakturaBeck.cvs");
         file.deleteOnExit(); 
		 FileWriter fstream = new FileWriter("raportFakturaBeck.cvs");
   		 out = new BufferedWriter(fstream);
   		 out.write("Numer faktury;Kwota;Kontrahent\n");

   		 
//			//DSApi.context().begin();
//			File file = File.createTempFile("raportFakturaBeck", "importBeck");
//            file.deleteOnExit();                
//            OutputStream os = new FileOutputStream(file);
//            StringBuilder sb = new StringBuilder();
//            sb.append("Numer faktury;Kwota;Kontrahent\n");
			Statement stat = DSApi.context().createStatement();	
		    ResultSet result = stat.executeQuery("SELECT f.invoice_number, f.gross_amount, p.organization FROM dsg_beck_coinvoice f INNER JOIN dso_person p ON f.document_id=p.document_id");
		    while(result.next())
		    {
		    	String temp = result.getString(1)+";";
		    	temp += result.getFloat(2)+";";
		    	temp += result.getString(3);		    	
		    	temp += "\n";
		     	out.write(temp);
		     	log.info("Linia eksportu faktury beck" + temp);
		    }
	   		 out.close();

			//DSApi.context().commit();
            ServletUtils.streamFile(ServletActionContext.getResponse(), file, "application/vnd.ms-excel", "Accept-Charset: iso-8859-2","Content-Disposition: attachment; filename=\"LOG.csv\"");
		} 
		catch(IOException e)
		{
			log.error(e.getMessage(), e);
			event.addActionError(e.getMessage() + e);
			DSApi.context()._rollback();
		}
		catch (Exception e) 
		{
			log.error(e.getMessage(), e);
			DSApi.context()._rollback();
		}
		finally
		{
		}
	}	
    
    @Override
    public ProcessCoordinator getProcessCoordinator(OfficeDocument document) throws EdmException {
        ProcessCoordinator ret = new ProcessCoordinator();
        ret.setUsername(document.getAuthor());
        return ret;
    }

}
