package pl.compan.docusafe.parametrization.bos;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.db.criteria.CriteriaResult;
import pl.compan.docusafe.core.db.criteria.NativeCriteria;
import pl.compan.docusafe.core.db.criteria.NativeExps;
import pl.compan.docusafe.core.db.criteria.NativeOrderExp.OrderType;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.jbpm4.Jbpm4Constants;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class SetPAKSDirectorsForFork implements ExternalActivityBehaviour
{

	private static final Logger log = LoggerFactory.getLogger(SetPAKSDirectorsForFork.class);

	@SuppressWarnings("unchecked")
	public void execute(ActivityExecution execution) throws Exception
	{
		try
		{
			List<String> argSets = new LinkedList<String>();
	
			// proste zapytanie, wyciągnięcie wbranych kolumn z warunkiem where
			NativeCriteria nc = DSApi.context().createNativeCriteria("ds_acceptance_condition", "d");
			nc.setProjection(NativeExps.projection()
					// ustalenie kolumn
					.addProjection("d.username"))
				// warunek where
				.add(NativeExps.like("d.cn", "kier_paks%"));
	
			
			// pobranie i wyświetlenie wyników
			CriteriaResult cr = nc.criteriaResult();
			while (cr.next())
			{
				argSets.add(cr.getString(0, null));
				System.out.println("Dyr paks: " + cr.getString(0, null));
			}
				
			// ids - id projektow
			execution.setVariable("usernames", argSets);
			// liczba subtaskow na ptorzeby forka
			execution.setVariable("count", argSets.size());
			// pomocnicza zmienna okreslajac licze akceptacji dyrektorow
			execution.setVariable("count2", 2 );
			execution.setVariable("twoAcc", 2 );
		}
		catch(Exception e)
		{
			log.error(e.getMessage(),e);
		}
	}

	@Override
	public void signal(ActivityExecution arg0, String arg1, Map<String, ?> arg2) throws Exception
	{
		// TODO Auto-generated method stub

	}
}
