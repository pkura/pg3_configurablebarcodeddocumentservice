package pl.compan.docusafe.parametrization.ukw;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.util.HSSFColor;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

public class UTPPunctualityXlsReport extends AbstractXlsReports {
	private String szukany;
	private Map<String, String> uzytkownicy;

	public UTPPunctualityXlsReport(String reportName) {
		super(reportName);
		title = new String[] { "Raport terminowo�ci" };

		columns = new String[] { "Lp.", "Imi�", "Nazwisko","Ilo�� zada� przed terminem"," Ilo�c zada� bez wyznaczonego terminu","Ilo�� zada� po terminie" };
	}

	@Override
	protected void createEntries() {
		
		

		try {
			int rowCount = 3;
			int lp = 1;

			StringBuilder sqlStatement = new StringBuilder(
					"select u.firstname,u.lastname, x.iloscPrzed,y.iloscBez,z.iloscPo from ds_user u "
					+ "left join( select u.firstname,u.lastname,u.name,count(u.name)  as iloscPrzed from DSW_JBPM_TASKLIST task  "
					+ "left join ds_user u on u.name=task.assigned_resource  "
					+ "left join DS_DOCUMENT d on d.id=task.document_id  "
					+ "where DATEdiff(d,CURRENT_TIMESTAMP,task.deadline_time) is not null and DATEdiff(d,CURRENT_TIMESTAMP,task.deadline_time)>0 and d.CZY_AKTUALNY=1  "
					+ "group by u.firstname,u.lastname,u.name) as x on x.name=u.name "
					+ "left join( select u.firstname,u.lastname,u.name,count(u.name)  as iloscBez from DSW_JBPM_TASKLIST task  "
					+ "left join ds_user u on u.name=task.assigned_resource  "
					+ "left join DS_DOCUMENT d on d.id=task.document_id  "
					+ "where DATEdiff(d,CURRENT_TIMESTAMP,task.deadline_time) is null and d.CZY_AKTUALNY=1  "
					+ "group by u.firstname,u.lastname,u.name) as y on y.name=u.name "
					+ "left join( select u.firstname,u.lastname,u.name,count(u.name)  as iloscPo from DSW_JBPM_TASKLIST task "
					+ "left join ds_user u on u.name=task.assigned_resource "
					+ "left join DS_DOCUMENT d on d.id=task.document_id "
					+ "where DATEdiff(d,CURRENT_TIMESTAMP,task.deadline_time) is not null and DATEdiff(d,CURRENT_TIMESTAMP,task.deadline_time)<0 and d.CZY_AKTUALNY=1 "
					+ "group by u.firstname,u.lastname,u.name) as z on z.name=u.name ");
			sqlStatement.append("WHERE ");
			// dodanie klauzuli where

			if (szukany != null && !szukany.equals("")) {

				sqlStatement.append(" u.id='" + szukany + "' AND");
			} else {
				sqlStatement.append(" ( ");
				for (String idSzukanego: uzytkownicy.keySet()) {

					sqlStatement.append(" u.id='" + idSzukanego + "' OR");
				}

				sqlStatement.delete(sqlStatement.length() - 2,
						sqlStatement.length());
				sqlStatement.append(") AND");
			}
			

			sqlStatement.delete(sqlStatement.length() - 3,
					sqlStatement.length());

			// wykonanie zapytania
			String sqlStringStatement = sqlStatement.toString();
			PreparedStatement ps = DSApi.context().prepareStatement(
					sqlStringStatement);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				String firstName = rs.getString("firstname");
				String lastName = rs.getString("lastname");
				String iloscPrzed = rs.getString("iloscPrzed");
				String iloscPo = rs.getString("iloscPo");
				String iloscBez = rs.getString("iloscBez");


				int i = -1;
				HSSFRow row = sheet.createRow(rowCount++);
				row.createCell(++i).setCellValue(
						new HSSFRichTextString(lp++ + ""));

				if (firstName != null)
					row.createCell(++i).setCellValue(
							new HSSFRichTextString(firstName));
				else
					row.createCell(++i).setCellValue(
							new HSSFRichTextString("-"));

				if (lastName != null)
					row.createCell(++i).setCellValue(
							new HSSFRichTextString(lastName));
				else
					row.createCell(++i).setCellValue(
							new HSSFRichTextString("-"));

				if (iloscPrzed != null){
					HSSFCellStyle style=this.workbook.createCellStyle();
					style.setFillForegroundColor(HSSFColor.GREEN.index);

					HSSFCell cell = row.createCell(++i);
					cell.setCellStyle(style);
					style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

					HSSFFont font = this.workbook.createFont();
					font.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
					font.setColor(HSSFColor.BLACK.index);

					style.setFont(font);
					cell.setCellValue(new HSSFRichTextString(iloscPrzed));}
				else
					row.createCell(++i).setCellValue(
							new HSSFRichTextString("0"));

				if (iloscBez != null)
					row.createCell(++i).setCellValue(
							new HSSFRichTextString(iloscBez));
				else
					row.createCell(++i).setCellValue(
							new HSSFRichTextString("0"));
				if (iloscPo != null){
					HSSFCellStyle style=this.workbook.createCellStyle();
					style.setFillForegroundColor(HSSFColor.RED.index);

					HSSFCell cell = row.createCell(++i);
					cell.setCellStyle(style);
					style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

					HSSFFont font = this.workbook.createFont();
					font.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
					font.setColor(HSSFColor.BLACK.index);

					style.setFont(font);
					cell.setCellValue(new HSSFRichTextString(iloscPo));
				}
				else
					row.createCell(++i).setCellValue(
							new HSSFRichTextString("0"));

			}

		} catch (HibernateException e) {
			Logger.getLogger(UTPDocumentUserAssignXlsReport.class.getName())
					.log(Level.SEVERE, null, e);
		} catch (SQLException e) {
			Logger.getLogger(UTPDocumentUserAssignXlsReport.class.getName())
					.log(Level.SEVERE, null, e);

		} catch (EdmException ex) {
			Logger.getLogger(UTPDocumentUserAssignXlsReport.class.getName())
					.log(Level.SEVERE, null, ex);
		}

	}

	public void setUzytkownicy(Map<String, String> uzytkownicy) {
		this.uzytkownicy = uzytkownicy;

	}

	public void setSzukany(String szukany) {
		this.szukany = szukany;

	}

}
