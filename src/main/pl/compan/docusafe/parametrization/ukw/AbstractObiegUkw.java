package pl.compan.docusafe.parametrization.ukw;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.activiti.helpers.UserAssignmentHelper;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import com.google.common.collect.Maps;

public abstract class AbstractObiegUkw {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractObiegUkw.class);
	protected static final String ADMIN_NAME="admin";
	private static final String ILOSC_AKCEPTACJI="ILOSC_AKCEPTACJI";
	private static final String ILOSC_ODRZUCEN="ILOSC_ODRZUCEN";

	public final String OLD_DATE_FORMAT = "EEE MMM dd kk:mm:ss z yyyy";
	public final String NEW_DATE_FORMAT = "dd/MM/yyyy";
	
	
	public String getOsobaOdpowiedzialna()
	{
		try {
			return Docusafe.getAdditionProperty("ukw.wniosekPozyczkaZapomoga.osobaOdpowiedzialna");
		} catch (Exception e) {
			LOG.debug(e.getMessage()+"Sprawd� w adds: ukw.wniosekPozyczkaZapomoga.osobaOdpowiedzialna",e);
		}
		return ADMIN_NAME;
	}	
	
	
	public String getPrzewodniczacyKomisji()
	{
		
		try {
			return Docusafe.getAdditionProperty("ukw.wniosekPozyczkaZapomoga.przewodniczacyKomisji");
		} catch (Exception e) {
			LOG.debug(e.getMessage()+"Sprawd� w adds: ukw.wniosekPozyczkaZapomoga.przewodniczacyKomisji",e);
		}
		return ADMIN_NAME;
	}	
	
	
	public String getCurrentUser()
	{
		try {
			return DSUser.getLoggedInUser().getName();
		} catch (Exception e) {
			LOG.debug(e.getMessage()+"B��d pr�by pobrania loginu aktualnie zalogowanego user'a",e);
		}
		return ADMIN_NAME;
	}
	
	
	
	public List<String> getCzlonkowieKomisjiList(String divisionName) {
		DSDivision division;
		List<String> lista = new ArrayList<String>();
		try 
		{	
			division = DSDivision.findByName(divisionName);
			DSUser []user = division.getOriginalUsers();
			for (DSUser dsUser : user) {
				lista.add(dsUser.getName());
			}
			 return lista;
		} catch (DivisionNotFoundException e) {
			e.printStackTrace();
		} catch (EdmException e) {
			e.printStackTrace();
		}
		return lista;
    }


	public void odnotowanieWyboru(long docId,boolean wybor)
	{
		try {
			Document document = Document.find(docId);
			FieldsManager fieldsManager = document.getFieldsManager();
			Map<String,Object> fieldValues = Maps.newHashMap();
			if(wybor)
			{
				if(fieldsManager.getIntegerValue(ILOSC_AKCEPTACJI)==null || fieldsManager.getIntegerValue(ILOSC_AKCEPTACJI)==0)
					fieldValues.put(ILOSC_AKCEPTACJI, 1);
				else
					fieldValues.put(ILOSC_AKCEPTACJI, fieldsManager.getIntegerValue(ILOSC_AKCEPTACJI)+1);
				fieldsManager.getDocumentKind().setOnly(docId, fieldValues);
			}
			else
			{
				if(fieldsManager.getIntegerValue(ILOSC_ODRZUCEN)==null || fieldsManager.getIntegerValue(ILOSC_ODRZUCEN)==0)
					fieldValues.put(ILOSC_ODRZUCEN,1);
				else
					fieldValues.put(ILOSC_ODRZUCEN, fieldsManager.getIntegerValue(ILOSC_ODRZUCEN)+1);
				fieldsManager.getDocumentKind().setOnly(docId, fieldValues);
			}
			
		} catch (DocumentNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentLockedException e) {
			e.printStackTrace();
		} catch (AccessDeniedException e) {
			e.printStackTrace();
		} catch (EdmException e) {
			e.printStackTrace();
		}
	}
	
	
	public void pokazPolaIloscOdrzucenAkceptacji(long docId)
	{
		Document document;
		try {
			document = Document.find(docId);
			FieldsManager fieldsManager = document.getFieldsManager();
			fieldsManager.getField(ILOSC_AKCEPTACJI).setHidden(false);
			fieldsManager.getField(ILOSC_ODRZUCEN).setHidden(false);
		} catch (DocumentNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentLockedException e) {
			e.printStackTrace();
		} catch (AccessDeniedException e) {
			e.printStackTrace();
		} catch (EdmException e) {
			e.printStackTrace();
		}
	}

	public void zmienStatusDokumentu(long docId,String cn,String val)
	{
		
		try{
			FieldsManager fm = Document.find(docId).getFieldsManager();
			Map<String,Object> fieldValues = Maps.newHashMap();
			fieldValues.put(cn, val);
			fm.getDocumentKind().setOnly(docId, fieldValues);	
		} catch (DocumentNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentLockedException e) {
			e.printStackTrace();
		} catch (AccessDeniedException e) {
			e.printStackTrace();
		} catch (EdmException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public abstract String getTemplateSavePath();
	
	public abstract String getTemplateName();
}
