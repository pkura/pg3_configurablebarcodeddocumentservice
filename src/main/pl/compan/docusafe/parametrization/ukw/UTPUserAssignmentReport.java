package pl.compan.docusafe.parametrization.ukw;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

public class UTPUserAssignmentReport extends Report {

	private String szukany;
	private Map<String,String> uzytkownicy;
	private UniversalTableDumper dumper;
	private Date dekOd;
	private Date dekDo;
	private String zalogowany;
	private Map<String, String> uzytkownicyPodlegli = new HashMap<String, String>();
	
	public void initParametersAvailableValues() throws ReportException {
		super.initParametersAvailableValues();

		try {

			for (ReportParameter rp : params) {

				if (rp.getFieldCn().equals("user")) {
					zalogowany = DSApi.context().getDSUser().getId().toString();

					// zabezpieczony przed sytuacja gdy p1 jest podlegly p2 i p2
					// jest podlegy p1
					if(AvailabilityManager.isAvailable("reports.tylkoPodwladni"))
						rp.setAvailableValues(zwrocPracownikowPodleglych(
								uzytkownicyPodlegli, zalogowany));
						else{
							rp.setAvailableValues(uzytkownicyPodlegli=zwrocUzytkownikow());
						}
					
				}
			}
		} catch (Exception e) {
			throw new ReportException(e);
		}
	}

	private Map<String, String> zwrocUzytkownikow() {
		HashMap<String, String> wszyscy = new HashMap<String, String>();
		try {
			StringBuilder sqlStatement = new StringBuilder(
					"select id ,name,firstname,lastname from DS_USER");

			// wywolanie zapytania
			String sqlStringStatement = sqlStatement.toString();
			PreparedStatement ps = DSApi.context().prepareStatement(
					sqlStringStatement);

			ResultSet rs = ps.executeQuery();
			// dodanie wynikow
			while (rs.next()) {
				wszyscy.put(rs.getString("name"), rs.getString("firstname")
						+ " " + rs.getString("lastname"));

			}

		} catch (HibernateException e) {
			Logger.getLogger(UTPUserAssignmentReport.class.getName()).log(Level.SEVERE, null, e);
		} catch (SQLException e) {
			Logger.getLogger(UTPUserAssignmentReport.class.getName()).log(Level.SEVERE, null, e);
		} catch (EdmException e) {
			Logger.getLogger(UTPUserAssignmentReport.class.getName()).log(Level.SEVERE, null, e);
		}

		return wszyscy;

	}

	



	@Override
	public void doReport() throws Exception {
		for (ReportParameter rp : params) {

			if (rp.getFieldCn().equals("user")) {
				szukany = rp.getValueAsString();
			}
			if (rp.getFieldCn().equals("dataOd")) {
				String[] tmp = rp.getValueAsString().split("-");
				if (tmp.length != 1) {
					dekOd = new Date(Integer.parseInt(tmp[2]) - 1900,
							Integer.parseInt(tmp[1]) - 1,
							Integer.parseInt(tmp[0]));
				}
			}
			if (rp.getFieldCn().equals("dataDo")) {
				String[] tmp = rp.getValueAsString().split("-");
				if (tmp.length != 1) {
					dekDo = new Date(Integer.parseInt(tmp[2]) - 1900,
							Integer.parseInt(tmp[1]) - 1,
							Integer.parseInt(tmp[0]));
				}
			}
		}



	
		

		dumper = new UniversalTableDumper();
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "raport.xls");
		poiDumper.openFile(xlsFile);
		FileOutputStream fis = new FileOutputStream(xlsFile);

		doReportForAll().getWorkbook().write(fis);
		fis.close();
		dumper.addDumper(poiDumper);

	}

	private UTPUserAssignmentXlsReport doReportForAll() throws Exception{

		UTPUserAssignmentXlsReport xlsReport=new UTPUserAssignmentXlsReport("Raport dekretacji");

		if (szukany != null && !szukany.equals(""))
			xlsReport.setSzukany(szukany);
		else {
			zalogowany = this.username;
			// name uzytkownika zamawiajacego -> jego id

			FromClause from = new FromClause();
			TableAlias userTable = from.createTable("ds_user");

			SelectClause select = new SelectClause(true);

			SelectColumn colId = select.add(userTable, "id");

			WhereClause where = new WhereClause();
			where.add(Expression.eq(userTable.attribute("name"), zalogowany));

			SelectQuery selectQuery = new SelectQuery(select, from, where, null);
			ResultSet rs;

			try {
				rs = selectQuery.resultSet(DSApi.context().session()
						.connection());
				rs.next();
				this.zalogowany = rs.getString(colId.getPosition());
			} catch (HibernateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (EdmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			uzytkownicyPodlegli = new HashMap<String, String>();
			
			
			if(AvailabilityManager.isAvailable("reports.tylkoPodwladni"))
				uzytkownicyPodlegli=zwrocPracownikowPodleglych(uzytkownicyPodlegli, zalogowany);
			else{
				uzytkownicyPodlegli=zwrocUzytkownikow();
			}
			xlsReport.setUzytkownicy(uzytkownicyPodlegli);
		}

		xlsReport.setDekOd(dekOd);
		xlsReport.setDekDo(dekDo);
		xlsReport.generate();
		return xlsReport;

	}
	// przyjmuje pusta mape, zwraca uzytkownikow podleglych
		private Map<String, String> zwrocPracownikowPodleglych(
				Map<String, String> uzytkownicyPodlegli, String zalogowany) {

			try {
				// dodanie aktualnego pracownika
				FromClause from = new FromClause();
				TableAlias userTable = from.createTable("ds_user");

				SelectClause select = new SelectClause(true);

				SelectColumn colName = select.add(userTable, "name");
				SelectColumn colFirstName = select.add(userTable, "firstname");
				SelectColumn colLastName = select.add(userTable, "lastname");

				WhereClause where = new WhereClause();
				where.add(Expression.eq(userTable.attribute("id"), zalogowany));

				SelectQuery selectQuery = new SelectQuery(select, from, where, null);
				ResultSet rs;

				rs = selectQuery.resultSet(DSApi.context().session().connection());

				rs.next();
				// nie wstawi jesli juz jest w mapie, ale bedzie sdzukal takze jego
				// podleglych osob
				if (!uzytkownicyPodlegli.containsKey(zalogowany))
					uzytkownicyPodlegli.put(
							zalogowany,
							rs.getString(colName.getPosition()) + ": "
									+ rs.getString(colLastName.getPosition()) + " "
									+ rs.getString(colFirstName.getPosition()));

				// wybranie podleglych id

				from = new FromClause();
				select = new SelectClause(true);
				where = new WhereClause();
				TableAlias US = from.createTable("DS_USER_SUPERVISOR");
				SelectColumn colId = select.add(US, "user_id");
				where.add(Expression.eq(US.attribute("SUPERVISOR_ID"), zalogowany));
				selectQuery = new SelectQuery(select, from, where, null);
				rs = selectQuery.resultSet(DSApi.context().session().connection());
				// i wykonanie dla nich rowniez tej metody rowniez z zastrzezeniem,
				// ze ich nie ma jeszcze na mapie
				while (rs.next()) {
					if (!uzytkownicyPodlegli.containsKey(rs.getString(colId
							.getPosition())))
						uzytkownicyPodlegli.putAll(zwrocPracownikowPodleglych(
								uzytkownicyPodlegli,
								rs.getString(colId.getPosition())));

				}

			} catch (HibernateException e) {
				log.debug(e.getMessage(), e);
				e.printStackTrace();
			} catch (SQLException e) {
				log.debug(e.getMessage(), e);
				e.printStackTrace();
			} catch (EdmException e) {
				log.debug(e.getMessage(), e);
				e.printStackTrace();
			}

			return uzytkownicyPodlegli;
		}
		
		


}
