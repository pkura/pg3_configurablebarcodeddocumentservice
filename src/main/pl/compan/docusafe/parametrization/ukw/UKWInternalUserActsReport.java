package pl.compan.docusafe.parametrization.ukw;

import java.io.File;
import java.io.FileOutputStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;

public class UKWInternalUserActsReport extends Report{


	private String[] name;
	private String zapoznalSie;

	private UniversalTableDumper dumper;



	public void initParametersAvailableValues() throws ReportException {
		super.initParametersAvailableValues();
		try {

			for (ReportParameter rp : params) {
				if (rp.getFieldCn().equals("zapoznalSie")) {
					Map<String, String> tmp = new TreeMap<String, String>();
					tmp.put("tak", sm.getString("tak"));
					tmp.put("nie", sm.getString("nie"));
					rp.setAvailableValues(tmp);
				}
				else if (rp.getFieldCn().equals("name")) {
					rp.setAvailableValues(pobierzUzytkownikow());
				}

			}
		} catch (Exception e) {
			throw new ReportException(e);
		}
	}



	@Override
	public void doReport() throws Exception {
		for (ReportParameter rp : params) {
			if (rp.getFieldCn().equals("name")) {
				if(rp.getValue() instanceof String) {
					name=new String[1];
					name[0]=rp.getValueAsString();
				}
				else name=(String[]) rp.getValue();
			}
			else if(rp.getFieldCn().equals("zapoznalSie")){
				zapoznalSie=rp.getValueAsString();
			}
		}


		dumper = new UniversalTableDumper();
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "raport.xls");
		poiDumper.openFile(xlsFile);
		FileOutputStream fis = new FileOutputStream(xlsFile);

		doReportForAll().getWorkbook().write(fis);
		fis.close();
		dumper.addDumper(poiDumper);
	}

	private UKWInternalUserActsReportXLS doReportForAll() {
		UKWInternalUserActsReportXLS xlsReport=new UKWInternalUserActsReportXLS("Raport użytkowników do rozporządzeń");

		if (name!=null&&!name.equals("")){
			xlsReport.setName(name);
		}
		if (zapoznalSie!=null&&!zapoznalSie.equals("")){
			xlsReport.setZapoznalSie(null);
			if(zapoznalSie.equals(sm.getString("tak")))xlsReport.setZapoznalSie(true);
			if(zapoznalSie.equals(sm.getString("nie")))xlsReport.setZapoznalSie(false);
		}



		xlsReport.generate();
		return xlsReport;
	}



	private Map<String, String> pobierzUzytkownikow() {
		Map<String, String> tmp = new LinkedHashMap<String, String>();
		try {
			List<DSUser> users=DSUser.list(DSUser.SORT_LASTNAME_FIRSTNAME);
			for(DSUser usr:users){
				tmp.put(usr.getName(), usr.asLastnameFirstname());
			}
			return tmp;
		} catch (EdmException e) {
			log.debug("",e);
			e.printStackTrace();
		}
		return tmp;
	}

	public String[] getName() {
		return name;
	}

	public void setName(String[] name) {
		this.name = name;
	}

	public String getZapoznalSie() {
		return zapoznalSie;
	}

	public void setZapoznalSie(String zapoznalSie) {
		this.zapoznalSie = zapoznalSie;
	}

}
