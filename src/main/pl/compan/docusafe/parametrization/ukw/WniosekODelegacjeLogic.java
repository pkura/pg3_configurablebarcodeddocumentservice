package pl.compan.docusafe.parametrization.ukw;

import com.google.common.collect.Maps;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.users.DSUser;

/**
 * Logika dla dokumentu typu wniosek_o_delegacje.
 * Przy korzystaniu z proces�w w ACTIVITI nie mo�na prze�adowywa� 
 * metody onStartProcess w klasie logiki procesu (powoduje to podw�jne uruchomienie procesu i/lub b��dy).
 * Metoda onStartProcess jest uruchomiona z klasy AbstractDocumentLogic (klasy po kt�rej dziedziczy logika).
 *
 */
public class WniosekODelegacjeLogic extends AbstractDocumentLogic{

	private static WniosekODelegacjeLogic instance;

	
	public static WniosekODelegacjeLogic getInstance() {
		if (instance == null)
			instance = new WniosekODelegacjeLogic();
		return instance;
	}
	
	@Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
        document.setFolder(folder);
        document.setTitle("Title id: "+document.getId());        
    }
	
	
//	@Override
//	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
//		Map<String, Object> values = Maps.newHashMap();
//		values.put("BYDGOSZCZ_DNIA", Calendar.getInstance().getTime());
//		fm.reloadValues(values);
//	}
	
	
	@Override
	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk�y- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk�y - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk�y - usuwanie"));
		
		for (PermissionBean perm : perms) {
			if (perm.isCreateGroup()
					&& perm.getSubjectType().equalsIgnoreCase(
							ObjectPermission.GROUP)) {
				String groupName = perm.getGroupName();
				if (groupName == null) {
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
		
	}
	
	/* (non-Javadoc)
	 * @see pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic#onLoadData(pl.compan.docusafe.core.dockinds.FieldsManager)
	 * Metoda zastosowana tutaj w celu uzupe�nienia pola "NUMER_SPRAWY" na dockindzie. Pocz�tkowo dokument nie ma warto�ci w tym polu,
	 * ale zostanie ona dodana po przypisaniu dokumentu do jakiej� sprawy. Pole "NUMER_SPRAWY" jest readonly.
	 */
//	@Override
//	public void onLoadData(FieldsManager fm) throws EdmException {
//		
//		
//	}
	
}
