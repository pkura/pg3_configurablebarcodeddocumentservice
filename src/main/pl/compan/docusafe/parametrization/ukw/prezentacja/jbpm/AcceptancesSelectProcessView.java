package pl.compan.docusafe.parametrization.ukw.prezentacja.jbpm;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.acceptances.DocumentAcceptance;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.process.InstantiationParameters;
import pl.compan.docusafe.core.dockinds.process.ProcessActionContext;
import pl.compan.docusafe.core.dockinds.process.ProcessInstance;
import pl.compan.docusafe.core.jbpm4.ActivitiesExtensionProcessView;
import pl.compan.docusafe.process.form.ActivityController;
import pl.compan.docusafe.process.form.SelectItem;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessInstance;
import pl.compan.docusafe.core.jbpm4.Jbpm4ProcessLocator;
import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.parametrization.ukw.prezentacja.FakturaZakupuLogic;
import pl.compan.docusafe.web.common.ExtendedRenderBean;

public class AcceptancesSelectProcessView extends ActivitiesExtensionProcessView {
	
	public AcceptancesSelectProcessView(InstantiationParameters ip) throws Exception {
	super(ip);
    }

    @Override
	public ExtendedRenderBean render(ProcessInstance pi, ProcessActionContext context) throws EdmException {
		return render((Jbpm4ProcessInstance) pi, context);
	}

	private ExtendedRenderBean render(Jbpm4ProcessInstance pi, ProcessActionContext context) throws EdmException {
		ExtendedRenderBean ret = super.render(pi, context);

		List<String> mpkAcceptationNames = new ArrayList<String>();
		mpkAcceptationNames.add(FakturaZakupuLogic.BUDGET_ACCEPTANCE);

		Long docId = Jbpm4ProcessLocator.documentIdForExtenedProcesssId(((Jbpm4ProcessInstance) pi).getProcessInstanceId());

		List<DocumentAcceptance> acceptances = DocumentAcceptance.find(docId);

		Field docProcesses = Document.find(docId).getDocumentKind().getFieldByCn("STATUS");

		String docStatus = Document.find(docId).getFieldsManager().getEnumItemCn("STATUS");

		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		for (DocumentAcceptance itemAcceptance : acceptances) {
			itemAcceptance.initDescription();
			
			String executionInfo = processAndExecutionIdFromProcessInstance(pi.getId());
			if (executionInfo != null && mpkAcceptationNames.contains(itemAcceptance.getAcceptanceCn())) {
				String[] exec = executionInfo.split("#");

				String processName = "";
				Long executionId = null;

				if (exec.length == 2) {
					processName = exec[0];
					executionId = Long.valueOf(exec[1]);
				}

				String mpkIdRaw = Jbpm4Utils.getVariable(executionId, FakturaZakupuLogic.MPK_VARIABLE_NAME);
				if (mpkIdRaw != null) {
					Long mpkId = null;
					try {
						mpkId = Long.valueOf(mpkIdRaw);
					} catch (NumberFormatException e) {
						log.error("B��d w parsowaniu zmiennej procesowej dla: " + executionInfo);
					}
					if (mpkId != null && !itemAcceptance.getObjectId().equals(mpkId)) {
						continue;
					}
				}
				
			} else if (itemAcceptance.getAcceptanceCn().equals(docStatus)) { // nie pokazywanie na li�cie akceptacji dla takiego samego etapu, co aktualnie wykonywany
				continue;
			}
			selectItems.add(ActivityController.createSelectItem(itemAcceptance.getId(), itemAcceptance.getAsFirstnameLastname(),
					itemAcceptance.getUsername(), docProcesses.getEnumItemByCn(itemAcceptance.getAcceptanceCn()).getTitle(),
					itemAcceptance.getAcceptanceCn()));
		}

		ret.putValue("selectItems", selectItems);

		return ret;
    }

	private String processAndExecutionIdFromProcessInstance(String processInstanceId) {
		Pattern p = Pattern.compile("(\\w+)(.)(\\d+)(.)(\\d+)(.)(\\d+)");
		Matcher m = p.matcher(processInstanceId);

		if (m.find()) {
			processInstanceId = m.group(1) + "#" + m.group(7);
		} else {
			p = Pattern.compile("(\\w+)(.)(\\d+)");
			m = p.matcher(processInstanceId);
			if (m.find()) {
				processInstanceId = m.group(1) + "#" + m.group(3);
			} else {
				return null;
			}
		}
		
		return processInstanceId;
	}
}
