package pl.compan.docusafe.parametrization.ukw.prezentacja;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Maps;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.FolderResolver;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class DelegacjaLogic extends AbstractDocumentLogic
{
	private static DelegacjaLogic instance;
	protected static Logger log = LoggerFactory.getLogger(DelegacjaLogic.class);
	
	public static DelegacjaLogic getInstance()
	{
		if (instance == null)
			instance = new DelegacjaLogic();
		return instance;
	}

	@Override
	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
		String documentKindName = document.getDocumentKind().getName();

		perms.add(new PermissionBean(ObjectPermission.READ, "UKW_" + documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, "UKW - " + documentKindName + " - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "UKW_" + documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "UKW - " + documentKindName + " zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "UKW_" + documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "UKW - " + documentKindName + " - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "UKW_" + documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "UKW - " + documentKindName + " zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "UKW_" + documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "UKW - " + documentKindName + " - usuwanie"));
		Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
		perms.addAll(documentPermissions);
		this.setUpPermission(document, perms);
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
		FolderResolver fr = document.getDocumentKind().getDockindInfo().getFolderResolver();
		if (fr != null)
		{
			as.useFolderResolver(document);
		}
	}
	
	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) 
	{
		StringBuilder msgBuilder = new StringBuilder();
		
		try 
		{
			BigDecimal suma = new BigDecimal(0);
			
			if (values.get("DWR_KOSZTY_PODROZY") != null) {
				
			Map<String, FieldData> kosztyPodrozy = values.get("DWR_KOSZTY_PODROZY").getDictionaryData();
			if (kosztyPodrozy != null) {
				for (String cn : kosztyPodrozy.keySet()){
					if(cn.contains("KOSZT_") && kosztyPodrozy.get(cn).getData() != null)
						suma = suma.add(kosztyPodrozy.get(cn).getMoneyData());
				}
				values.get("DWR_SUMA_KOSZTOW_PODROZY_COST_TO_SUM").setMoneyData(suma);
			}
				
			suma = new BigDecimal(0);
			for (String cn : values.keySet()) {
				if (cn.contains("COST_TO_SUM") && values.get(cn).getData() != null)
					suma = suma.add(values.get(cn).getMoneyData());
			}
			values.get("DWR_SUMA").setMoneyData(suma);
			}
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return null;
	}

	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		Map<String, Object> toReload = Maps.newHashMap();
		for (Field f : fm.getFields())
			if (f.getDefaultValue() != null)
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));

		DSUser user = DSApi.context().getDSUser();
		toReload.put("WORKER", user.getId());
		toReload.put("DATA_WYPELNIENIA", new Date());
		fm.reloadValues(toReload);
	}
	
}