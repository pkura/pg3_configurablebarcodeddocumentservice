package pl.compan.docusafe.parametrization.ukw.prezentacja;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import org.jbpm.api.model.OpenExecution;
import org.jbpm.api.task.Assignable;

import com.google.common.collect.Maps;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.FolderResolver;
import pl.compan.docusafe.core.dockinds.dictionary.CentrumKosztowDlaFaktury;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.jbpm4.AssigneeHandler;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class OgolneLogic extends AbstractDocumentLogic
{
	private static OgolneLogic instance;
	protected static Logger log = LoggerFactory.getLogger(OgolneLogic.class);
	
	public static final String USER_ACCEPTANCE = "user_acceptance";
	public static final String MPK_VARIABLE_NAME = "costCenterId";
	
	public static OgolneLogic getInstance()
	{
		if (instance == null)
			instance = new OgolneLogic();
		return instance;
	}

	@Override
	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		String documentKindCn = document.getDocumentKind().getCn().toUpperCase();
		String documentKindName = document.getDocumentKind().getName();

		perms.add(new PermissionBean(ObjectPermission.READ, "UKW_" + documentKindCn + "_DOCUMENT_READ", ObjectPermission.GROUP, "UKW - " + documentKindName + " - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "UKW_" + documentKindCn + "_DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "UKW - " + documentKindName + " zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "UKW_" + documentKindCn + "_DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "UKW - " + documentKindName + " - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "UKW_" + documentKindCn + "_DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "UKW - " + documentKindName + " zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "UKW_" + documentKindCn + "_DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "UKW - " + documentKindName + " - usuwanie"));
		Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
		perms.addAll(documentPermissions);
		this.setUpPermission(document, perms);
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
		FolderResolver fr = document.getDocumentKind().getDockindInfo().getFolderResolver();
		if (fr != null)
		{
			as.useFolderResolver(document);
		}
	}
	
	@Override
	public pl.compan.docusafe.core.dockinds.dwr.Field validateDwr(Map<String, FieldData> values, FieldsManager fm) 
	{
		StringBuilder msgBuilder = new StringBuilder();
		
		try 
		{
			
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		return null;
	}

	public void setInitialValues(FieldsManager fm, int type) throws EdmException
	{
		Map<String, Object> toReload = Maps.newHashMap();
		for (Field f : fm.getFields())
			if (f.getDefaultValue() != null)
				toReload.put(f.getCn(), f.simpleCoerce(f.getDefaultValue()));

		DSUser user = DSApi.context().getDSUser();
		toReload.put("WORKER", user.getId());
		toReload.put("DATA_WYPELNIENIA", new Date());
		fm.reloadValues(toReload);
	}
	
	@Override
	public boolean assignee(OfficeDocument doc, Assignable assignable, OpenExecution openExecution, String acceptationCn, String fieldCnValue) {
		
		boolean assigned = false;

		// akceptacja pozycji budzetowych
		if (USER_ACCEPTANCE.equals(acceptationCn))
			assigned = assignToUser(doc, openExecution, assignable, acceptationCn);
		
		return assigned;
	}
	
	private boolean assignToUser(OfficeDocument doc, OpenExecution openExecution, Assignable assignable, String acceptationCn) {
		boolean assigned = false;
		try {
			Long mpkId = null;
			mpkId = (Long) openExecution.getVariable(MPK_VARIABLE_NAME);
			
			CentrumKosztowDlaFaktury mpkInstance = CentrumKosztowDlaFaktury.getInstance();
			CentrumKosztowDlaFaktury mpk = mpkInstance.find(mpkId);

			Long userID = mpk.getCentrumId().longValue();
			if (userID != null && userID != -1) {
				try {
					DSUser user = DSUser.findById(userID);
					assignable.addCandidateUser(user.getName());
					AssigneeHandler.addToHistory(openExecution, doc, user.getName(), null);
					return true;
				} catch (UserNotFoundException e) {
					log.error("Nie znaleziono użytkownika o id = " + userID);
				}
			}
			
		} catch (Exception e) {
			log.error("Nie znaleziono osoby do przypisania, przypisano do admin'a");
			assignable.addCandidateUser("admin");
			return true;
		}
		return assigned;
	}
	
}