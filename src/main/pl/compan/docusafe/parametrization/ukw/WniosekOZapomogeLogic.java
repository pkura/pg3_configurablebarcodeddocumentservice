package pl.compan.docusafe.parametrization.ukw;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.names.URN;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.webwork.event.ActionEvent;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

public class WniosekOZapomogeLogic extends AbstractDocumentLogic {
	private static final long serialVersionUID = 1L;
	private static WniosekOZapomogeLogic instance;

	public final String OLD_DATE_FORMAT = "EEE MMM dd kk:mm:ss z yyyy";
	public final String NEW_DATE_FORMAT = "dd/MM/yyyy";
	protected static Logger LOG = LoggerFactory.getLogger(WniosekOZapomogeLogic.class);

	public static WniosekOZapomogeLogic getInstance() {
		if (instance == null)
			instance = new WniosekOZapomogeLogic();
		return instance;
	}
	
	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values) {
		try{
			Document document =  Document.find(docId);
	        FieldsManager fm = document.getFieldsManager();
	        Map<String, Object> dockindFieldMap=fm.getFieldValues();

	        SimpleDateFormat oldSdf = new SimpleDateFormat(OLD_DATE_FORMAT,Locale.ENGLISH);
	        SimpleDateFormat newSdf = new SimpleDateFormat(NEW_DATE_FORMAT);
	        
	        values.put("DOCUMENT_ID", Long.toString(docId));
	        
	        for (Map.Entry<String, Object> entry : dockindFieldMap.entrySet()) {
	            String key = entry.getKey();
	            Object value = entry.getValue();
	            if(key.equals("SENDER_HERE"))
	            {
	            	long val = Long.valueOf(value.toString()).longValue();
	            	values.put("FIRSTNAME_WARTOSC",Person.find(val).getFirstname());
	    	        values.put("LASTNAME_WARTOSC",Person.find(val).getLastname());
	    	        values.put("DIVISION_WARTOSC",Person.find(val).getOrganizationDivision());
	    	        continue;
	            }
	            else if(key.equals("BYDGOSZCZ_DNIA") || key.equals("DATA_ZATRUDNIENIA"))
	            {
	            	
	            	if(value == null || value.toString() == "")
	            		values.put(key+"_WARTOSC","");
	            	else
	            	{
	            		Date result =  oldSdf.parse(value.toString());
	            		values.put(key+"_WARTOSC", newSdf.format(result));
	            	}
	            	continue;
	            }
	            else if(key.equals("STATUS_PRACY") || key.equals("DEFINICJA_ZAPOMOGI"))
	            {
	            	values.put(key+"_WARTOSC", value==null ? " " : fm.getEnumItem(key).getTitle());
	            	continue;
	            }
	            
	            values.put(key+"_WARTOSC", value==null ? " " : value.toString());
	        }
        
		} catch (DocumentNotFoundException e) {
			LOG.error(e.getMessage(),e);
		} catch (DocumentLockedException e) {
			LOG.error(e.getMessage(),e);
		} catch (AccessDeniedException e) {
			LOG.error(e.getMessage(),e);
		} catch (EdmException e) {
			LOG.error(e.getMessage(),e);
		} catch (Exception e) {
			LOG.error(e.getMessage(),e);
		}
		
		
	}
	
	
	@Override
	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ",ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS,"DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY,"DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS,"DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE,"DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));

		for (PermissionBean perm : perms) {
			if (perm.isCreateGroup()&& perm.getSubjectType().equalsIgnoreCase(ObjectPermission.GROUP)) {
				String groupName = perm.getGroupName();
				if (groupName == null) {
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}

	}


	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {
		as.useFolderResolver(document);
		as.useAvailableProviders(document);
		LOG.info("document title : '{}', description : '{}'",
				document.getTitle(), document.getDescription());
	}

	@Override
	public void setInitialValues(FieldsManager fm, int type)
			throws EdmException {
		LOG.info("--- WniosekOZapomogeLogic :  setInitialValues  !!! ---- {}");
		super.setInitialValues(fm, type);
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("BYDGOSZCZ_DNIA", Calendar.getInstance().getTime());
		fm.reloadValues(values);
	}

}
