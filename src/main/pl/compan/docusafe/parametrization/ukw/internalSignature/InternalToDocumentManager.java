package pl.compan.docusafe.parametrization.ukw.internalSignature;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class InternalToDocumentManager {
	
	private static final Logger log = LoggerFactory.getLogger(InternalToDocumentManager.class);
	private static List list;
	
	
	@SuppressWarnings("unchecked")
	public static List<InternalToDocumentEntity> list() {
		Criteria c;
		try {
			c = DSApi.context().session().createCriteria(InternalToDocumentEntity.class);
			return (List<InternalToDocumentEntity>) c.list();
		} catch (EdmException e) {
			log.error("", e);
			return null;
		}
	}
	
	public static InternalToDocumentEntity find(Long id) {
		Criteria c;
		try {
			c = DSApi.context().session().createCriteria(InternalToDocumentEntity.class);
			c.add(Restrictions.eq("id", id));
			return (InternalToDocumentEntity) c.uniqueResult();
		} catch (EdmException e) {
			if(e instanceof EdmHibernateException) throw new HibernateException("Zapytanie zwraca wi�cej ni� jeden wynik.");
			log.error("", e);
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public static List<InternalToDocumentEntity> findBySignId(Long signId){
		Criteria c;
		list = null;
		try {
			c = DSApi.context().session().createCriteria(InternalToDocumentEntity.class);
			c.add(Restrictions.eq("signId", signId));
			list = c.list();
			if(list.isEmpty()) return null;
			return list;
		} catch (EdmException e) {
			log.error("", e);
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public static List<InternalToDocumentEntity> findByDocumentId(Long documentId){
		Criteria c;
		list = null;
		try {
			c = DSApi.context().session().createCriteria(InternalToDocumentEntity.class);
			c.add(Restrictions.eq("documentId", documentId));
			list = c.list();
			if(list.isEmpty()) return null;
			return list;
		} catch (EdmException e) {
			log.error("", e);
			return null;
		}
	}
	
	
}
