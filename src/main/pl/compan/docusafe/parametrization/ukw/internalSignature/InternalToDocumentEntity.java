package pl.compan.docusafe.parametrization.ukw.internalSignature;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

@Entity
@Table(name = "DS_INT_TO_DOC")
public class InternalToDocumentEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private Long id;
	
	@Column(name = "SIGN_ID", nullable = false)
	private Long signId;
	
	@Column(name = "DOCUMENT_ID", nullable = false)
	private Long documentId;
	
	@Column(name = "CTIME", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	public Long getSignId() {
		return signId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public Long getId() {
		return id;
	}
	
	public Boolean saveOrUpdate() throws EdmException{
		try {
			if (id == null){
				createDate = Calendar.getInstance().getTime();
			}
			DSApi.context().session().saveOrUpdate(this);
			return true;
		} catch (HibernateException he) {
			throw new EdmHibernateException(he);
		}
	}

	public void setSignId(Long signId) {
		this.signId = signId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public Long getDocumentId() {
		return documentId;
	}
	
}
