package pl.compan.docusafe.parametrization.ukw.internalSignature;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.HibernateException;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;

/**
 * Klasa reprezentuje encje tabeli "DS_INTERNAL_SIGNATURE"
 * Przechowuje wewnętrzene sygnatury
 * 
 */
@Entity
@Table(name = "DS_INTERNAL_SIGN")
public class InternalSignatureEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private Long id;
	
	@Column(name = "USER_ID", nullable = false)
	private Long userId;
	
	@Column(name = "CTIME", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTime;
	
	@Column(name = "SIGNATURE", nullable = false)
	private String signature;
	
	public Boolean saveOrUpdate() throws EdmException {
		try {
			if (id == null){
				createTime = Calendar.getInstance().getTime();
				Calendar cal = Calendar.getInstance();
				cal.setTime(createTime);
				signature = InternalSignatureManager.generateSignature(cal.getTimeInMillis());
			}
			DSApi.context().session().saveOrUpdate(this);
			return true;
		} catch (HibernateException he) {
			throw new EdmHibernateException(he);
		}
	}
	
	public Long getId() {
		return id;
	}
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public String getSignature() {
		return signature;
	}
}
