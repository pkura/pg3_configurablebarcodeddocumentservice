package pl.compan.docusafe.parametrization.ukw.internalSignature;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmHibernateException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class InternalSignatureManager {

private static final Logger log = LoggerFactory.getLogger(InternalSignatureManager.class);
	
	@SuppressWarnings("unchecked")
	public static List<InternalSignatureEntity> list() {
		Criteria c;
		try {
			c = DSApi.context().session().createCriteria(InternalSignatureEntity.class);
			return (List<InternalSignatureEntity>) c.list();
		} catch (EdmException e) {
			log.error("", e);
			return null;
		}
	}
	
	public static InternalSignatureEntity find(Long id) {
		Criteria c;
		try {
			c = DSApi.context().session().createCriteria(InternalSignatureEntity.class);
			c.add(Restrictions.eq("id", id));
			return (InternalSignatureEntity) c.uniqueResult();
		} catch (EdmException e) {
			if(e instanceof EdmHibernateException) throw new HibernateException("Zapytanie zwraca wi�cej ni� jeden wynik.");
			log.error("", e);
			return null;
		}
	}
	/**
	 * 
	 * @param userId 
	 * @return liste obiekt�w klasy InternalSignatureEntity; null w przypadku wyjatku lub pustej listy
	 */
	@SuppressWarnings("unchecked")
	public static List<InternalSignatureEntity> findByUserId(Long userId){
		List<InternalSignatureEntity> list = null;
		Criteria c;
		try {
			c = DSApi.context().session().createCriteria(InternalSignatureEntity.class);
			c.add(Restrictions.eq("userId", userId));
			list = (List<InternalSignatureEntity>) c.list();
			if(list.isEmpty()) return null;
			return list;
		} catch (EdmException e) {
			log.error("", e);
			return null;
		}
	}
	
	/**
	 * Szyfruje przeslany parametr funkcja MD5.
	 * Zmienia Long na String i wywo�uje {@link #generateSignature(String)}
	 * @param toEncode
	 * @return null - je�li wyst�pi NoSuchAlgorithmException
	 */
	public static String generateSignature(Long toEncode) {
		return generateSignature(Long.toString(toEncode));
	}
	
	/**
	 * Szyfruje przeslany parametr funkcja MD5.
	 * @param toEncode
	 * @return null - je�li wyst�pi NoSuchAlgorithmException
	 */
	public static String generateSignature(String toEncode) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA");
			String signature = "";
			byte[] bytes = md.digest(toEncode.getBytes());
			for (byte b : bytes) {
				String hex = Integer.toHexString(b);
				if(hex.length()<2) hex = "0"+hex;
				else if(hex.length()>2) hex = hex.substring(hex.length()-2, hex.length());
				signature += hex;
			}
			return signature;
		} catch (NoSuchAlgorithmException e) {
			log.error("", e);
			return null;
		}
	}
	
	/**
	 * Zwraca sygnatur� u�ytkownika.
	 * Bierze ostatni� dodan� do u�ytkownika.
	 * @param userId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static InternalSignatureEntity findUserSignature(Long userId){
		Criteria c;
		List<InternalSignatureEntity> list = null;
		try {
			c = DSApi.context().session().createCriteria(InternalSignatureEntity.class);
			c.add(Restrictions.eq("userId", userId));
			c.addOrder(Order.desc("createTime"));
			list = (List<InternalSignatureEntity>) c.list();
			if(list.isEmpty())
				return null;
			return list.get(0);
		} catch (EdmException e) {
			log.error("", e);
			return null;
		}
	}
}
