package pl.compan.docusafe.parametrization.ukw.ws;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import com.google.common.collect.Maps;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;

import org.apache.axis2.context.MessageContext;
import org.apache.axis2.context.OperationContext;
import org.apache.axis2.wsdl.WSDLConstants;
import org.apache.commons.io.FileUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.CaseNotFoundException;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.InOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.InOfficeDocumentKind;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OfficeFolder;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocumentDelivery;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Rwa;
import pl.compan.docusafe.core.office.Sender;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.archiwum.ArchivePackageManager;
import pl.compan.docusafe.parametrization.utp.OfficeUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class UTPService {

    private final static Logger log = LoggerFactory.getLogger(UTPService.class);

    /**
     * Metoda s�u�aca do pobierania za��cznik�w dla dokumentu oraz pliku metadanych opisuj�cym jego zawarto��.
     * @param documentId - identyfikator dokumentu kancelaryjnego
     * @return spakowany plik metadanych+za��czniki dokumentu wraz z obiektem {@link DocumentResponse}
     */
    public DocumentResponse getDocument(Long documentId){
        DocumentResponse response = new DocumentResponse();
        response.setMessage("Rozpocz�to pobieranie dokumentu wraz z metadanymi z EOD");
        response.setError(false);

        boolean contextOpened = false;
        try {
            contextOpened = DSApi.openContextIfNeeded();
            if(!DSApi.context().inTransaction())
                DSApi.context().begin();

            OfficeDocument doc = OfficeDocument.find(documentId);
            File file = ArchivePackageManager.preparePackage(doc);

            MessageContext inMessageContext  = MessageContext.getCurrentMessageContext();
            OperationContext operationContext = inMessageContext.getOperationContext();

            MessageContext outMessageContext = operationContext.getMessageContext(WSDLConstants.MESSAGE_LABEL_OUT_VALUE);

            FileDataSource fileDataSource = new FileDataSource(file);
            DataHandler dataHandler = new DataHandler(fileDataSource);
            outMessageContext.addAttachment(file.getName(), dataHandler);

            response.setMessage("Pobieranie dokumentu zako�czone pomy�lnie.");
            response.setFilename(file.getName());
        }catch (Exception e){
            log.error("B��d: "+e);
            response.setMessage("B��d: "+e);
            response.setError(true);
        } finally {
            try {
                DSApi.context().commit();
            } catch (Exception e) {
                log.error("Blad podczas zatwierdzania transakcji bazy danych: " + e);
                response.setError(true);
                response.setMessage("Blad podczas otwierania kontekstu bazy danych: " + e);
                try {
                    DSApi.context().rollback();
                } catch (Exception ex) {
                    log.error("Blad podczas wycofywania transakcji bazy danych: " + ex);
                    response.setError(true);
                    response.setMessage("Blad podczas wycofywania transakcji bazy danych: " + ex);
                }
            }
            DSApi.closeContextIfNeeded(contextOpened);
        }
        return response;
    }

    /**
     * Metoda s�u�aca do pobierania pliku za��cznika
     * @param attachmentId - identyfikator za��cznika
     * @return plik za��cznika wraz z opisuj�cym go obiektem {@link AttachmentResponse}
     *
     * @throws {@link Exception}
     */
    private AttachmentResponse getAttachment(Long attachmentId) throws Exception
    {
        AttachmentResponse response = new AttachmentResponse();
        response.setError(false);
        response.setMessage("Rozpocz�to pobieranie za��cznika.");

        boolean contextOpened = false;
        try {
            contextOpened = DSApi.openContextIfNeeded();
            if(!DSApi.context().inTransaction())
                DSApi.context().begin();

            File attachment = new File(Attachment.getPath(attachmentId));
            String originalFilename = Attachment.find(attachmentId).getMostRecentRevision().getOriginalFilename();

            File newAtt = new File(originalFilename);
            FileUtils.copyFile(attachment, newAtt);

            MessageContext inMessageContext  = MessageContext.getCurrentMessageContext();
            OperationContext operationContext = inMessageContext.getOperationContext();

            MessageContext outMessageContext = operationContext.getMessageContext(WSDLConstants.MESSAGE_LABEL_OUT_VALUE);

            FileDataSource fileDataSource = new FileDataSource(attachment);
            DataHandler dataHandler = new DataHandler(fileDataSource);
            outMessageContext.addAttachment(originalFilename, dataHandler);

            response.setFileName(originalFilename);
            response.setMessage("Pobieranie za��cznika zako�czone sukcesem.");
        }catch (Exception e){
            log.error("B��d: "+e);
            response.setMessage("B��d: "+e);
            response.setError(true);
        } finally {
            try {
                DSApi.context().commit();
            } catch (Exception e) {
                log.error("Blad podczas zatwierdzania transakcji bazy danych: " + e);
                response.setError(true);
                response.setMessage("Blad podczas otwierania kontekstu bazy danych: " + e);
                try {
                    DSApi.context().rollback();
                } catch (Exception ex) {
                    log.error("Blad podczas wycofywania transakcji bazy danych: " + ex);
                    response.setError(true);
                    response.setMessage("Blad podczas wycofywania transakcji bazy danych: " + ex);
                }
            }
            DSApi.closeContextIfNeeded(contextOpened);
        }
        return response;
    }

    /**
     * Metoda s�u�aca do tworzenia odpowiedniego dokumentu w systemie wraz ze ustawianiem odpowiednich warto�ci
     * @param request - {@link AddDocumentRequest} obiekt ��dania (dane dot. wstawianego dokumentu kancelaryjnego)
     *
     * @return {@link AddDocumentResponse} response - obiekt odpowiedzi (posiada informacje, czy dokument zosta� dodany poprawnie do systemu
     * */
    public AddDocumentResponse addDocument(AddDocumentRequest request) {
        AddDocumentResponse response = new AddDocumentResponse();
        response.setError(false);
        response.setMessage("Rozpocz�to proces dodawania dokumentu do EOD");
        boolean contextOpened = false;
        try {
            contextOpened = DSApi.openContextIfNeeded();
            if(!DSApi.context().inTransaction())
                DSApi.context().begin();

            if(request.getZnakSprawy() == null ||
                    request.getZnakSprawy().equals("") ||
                    request.getTytul()== null ||
                    request.getTytul().equals("") ||
                    request.getRodzajDokumentu() == null ||
                    request.getZalacznik() == null ||
                    request.getZalacznik().equals("") ||
                    request.getNadawca() == null ||
                    request.getOdbiorca() == null
                    ){
                log.error("Brak wymaganych danych dla dokumentu.");
                response.setError(true);
                response.setMessage("Brak wymaganych danych dla dokumentu.");
                return response;
            }

            try {
                OfficeCase sprawa = OfficeCase.findByOfficeId(request.getZnakSprawy());
                if(sprawa == null)
                    throw new EdmException("Nie znaleziono sprawy dla znaku: "+request.getZnakSprawy());

                //		Sprawdzanie czy jest za��cznik do faktury.
                //		Je�eli istnieje, zostanie dodany
                //		Je�eli nie lub wyst�pi b��d, brak nazwy: plik nie zostanie dodany

                File file = catchAttachmentFromRequest(request.getZalacznik());

                if (request.getRodzajDokumentu().intValue()==0){
                    //pismo przychodzace
                    DSUser uzytkownik = null;
                    DSDivision dzial = null;
                    if(request.getLogin() != null && !"".equals(request.getLogin())){
                        try{
                            uzytkownik = DSUser.findByUsername(request.getLogin());
                        }catch (UserNotFoundException e){
                            log.info("Nie znaleziono uzytkownika dla podanego loginu: "+e);
                            throw new EdmException("Nie znaleziono uzytkownika dla podanego loginu: " + e);
                        }
                    }
                    if(request.getGuidDzialu() != null && !"".equals(request.getGuidDzialu())){
                        try{
                            dzial = DSDivision.find(request.getGuidDzialu());
                        }catch (DivisionNotFoundException e){
                            log.info("Nie znaleziono dzialu dla podanego GUID: "+e);
                            throw new EdmException("Nie znaleziono dzialu dla podanego GUID: " + e);
                        }
                    }

                    String summary = "Importowany dokument - "+ request.getTytul();

                    InOfficeDocument doc = OfficeUtils.newInOfficeDocument(
                            dzial != null ? dzial.getGuid() : "rootdivision",
                            uzytkownik != null ? uzytkownik.getName() : "admin",
                            dzial != null ? dzial.getGuid() : "rootdivision",
                            uzytkownik != null ? uzytkownik.getName() : "admin",
                            request.getTytul(),
                            request.getTytul(),
                            file);
                    doc.create();

                    OfficeUtils.createAttachmentForDocument(doc,file);

                    OfficeUtils.bindToJournal(doc, null, Journal.INCOMING);
                    DocumentKind documentKind = DocumentKind
                            .findByCn(DocumentLogicLoader.NORMAL_KIND);
                    doc.setDocumentKind(documentKind);
                    doc.setIncomingDate(new Date());
                    doc.setDocumentDate(new Date());

                    Sender nadawca = createSenderFromRequest(request.getNadawca());
                    OfficeUtils.addSenderToInOfficeDocument(doc, nadawca);
                    validateUserData(request.getOdbiorca());

                    doc.setDelivery(InOfficeDocumentDelivery.findByName("Zwyk�y"));
                    doc.setKind(InOfficeDocumentKind.getInOfficeDocumentKind(documentKind));

                    Map<String, Object> dockindKeys = new HashMap<String, Object>();
                    dockindKeys.put("SENDER", nadawca.getId());
                    dockindKeys.put("RECIPIENT_HERE", "u:" + request.getOdbiorca().getLogin()+
                            ";d:" + request.getOdbiorca().getGuidDzialu());
                    dockindKeys.put("oryginal_kopia",0);
                    dockindKeys.put("RODZAJ_PISMA_IN",1);
                    dockindKeys.put("typ_slownika", 4);
                    dockindKeys.put("data", new Date());

                    Long newDocumentId = doc.getId();

                    documentKind.set(newDocumentId, dockindKeys);

                    doc.getDocumentKind().logic().archiveActions(doc, 0);
                    doc.getDocumentKind().logic().documentPermissions(doc);

                    DSApi.context().session().save(doc);

                    if(uzytkownik != null){
                        Map<String, Object> map = Maps.newHashMap();
                        map.put(ASSIGN_USER_PARAM, uzytkownik != null ? uzytkownik.getName(): "admin");
                        map.put(ASSIGN_DIVISION_GUID_PARAM, dzial != null ? dzial.getGuid() : "rootdivision");

                        doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(doc, map);

                        TaskSnapshot.updateByDocument(doc);
                    }

                    OfficeUtils.addDocToCase(sprawa, doc);

                    response.setMessage("Utworzono dokument w EOD.");
                    response.setDocumentId(doc.getId());
                    return response;
                } else if(request.getRodzajDokumentu().intValue()==1){
                    //pismo wychodzace
                    DSUser uzytkownik = null;
                    DSDivision dzial = null;
                    if(request.getLogin() != null && !"".equals(request.getLogin())){
                        try{
                            uzytkownik = DSUser.findByUsername(request.getLogin());
                        }catch (UserNotFoundException e){
                            log.info("Nie znaleziono uzytkownika dla podanego loginu: "+e);
                            throw new EdmException("Nie znaleziono uzytkownika dla podanego loginu: " + e);
                        }
                    }
                    if(request.getGuidDzialu() != null && !"".equals(request.getGuidDzialu())){
                        try{
                            dzial = DSDivision.find(request.getGuidDzialu());
                        }catch (DivisionNotFoundException e){
                            log.info("Nie znaleziono dzialu dla podanego GUID: "+e);
                            throw new EdmException("Nie znaleziono dzialu dla podanego GUID: " + e);
                        }
                    }
                    OutOfficeDocument doc = OfficeUtils.newOutOfficeDocument(
                            dzial != null ? dzial.getGuid() : "rootdivision",
                            uzytkownik != null ? uzytkownik.getName() : "admin",
                            dzial != null ? dzial.getGuid() : "rootdivision",
                            uzytkownik != null ? uzytkownik.getName() : "admin",
                            request.getTytul(),
                            request.getTytul(),
                            false,
                            file
                    );
                    doc.create();
                    OfficeUtils.createAttachmentForDocument(doc,file);

                    OfficeUtils.bindToJournal(doc, null, Journal.OUTGOING);
                    DocumentKind documentKind = DocumentKind
                            .findByCn("normal_out");
                    doc.setDocumentKind(documentKind);
                    doc.setDocumentDate(new Date());

                    Recipient odbiorca = createRecipientFromRequest(request.getOdbiorca());
                    OfficeUtils.addRecipientToOutOfficeDocument(doc, odbiorca);
                    validateUserData(request.getNadawca());

                    doc.setDelivery(OutOfficeDocumentDelivery.findByName("Zwyk�y"));
                    Map<String, Object> dockindKeys = new HashMap<String, Object>();

                    dockindKeys.put("SENDER_HERE", "u:"+ request.getNadawca().getLogin() + ";d:"+request.getNadawca().getGuidDzialu());
                    dockindKeys.put("RECIPIENT", odbiorca.getId());

                    Long newDocumentId = doc.getId();

                    documentKind.set(newDocumentId, dockindKeys);

                    doc.getDocumentKind().logic().archiveActions(doc, 0);
                    doc.getDocumentKind().logic().documentPermissions(doc);

                    DSApi.context().session().save(doc);

                    if(uzytkownik != null){
                        Map<String, Object> map = Maps.newHashMap();
                        map.put(ASSIGN_USER_PARAM, uzytkownik != null ? uzytkownik.getName(): "admin");
                        map.put(ASSIGN_DIVISION_GUID_PARAM, dzial != null ? dzial.getGuid() : "rootdivision");

                        doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().onStart(doc, map);

                        TaskSnapshot.updateByDocument(doc);
                    }

                    OfficeUtils.addDocToCase(sprawa, doc);

                    response.setMessage("Utworzono dokument w EOD.");
                    response.setDocumentId(doc.getId());
                    return response;
                }
            } catch (CaseNotFoundException e) {
                log.error("Nie znaleziono sprawy dla znaku: "+ request.getZnakSprawy());
                response.setError(true);
                response.setMessage("Nie znaleziono sprawy dla znaku: "+ request.getZnakSprawy());
                return response;
            } catch (Exception e){
                log.error("Blad podczas tworzenia dokumentu: " + e);
                response.setError(true);
                response.setMessage("Blad podczas tworzenia dokumentu: " + e);
                return response;
            }
        } catch (EdmException e) {
            log.error("Blad podczas otwierania kontekstu bazy danych: "+e);
            response.setError(true);
            response.setMessage("Blad podczas otwierania kontekstu bazy danych: "+e);
        } finally {
            try {
                DSApi.context().commit();
            } catch (Exception e) {
                log.error("Blad podczas zatwierdzania transakcji bazy danych: " + e);
                response.setError(true);
                response.setMessage("Blad podczas otwierania kontekstu bazy danych: " + e);
                try {
                    DSApi.context().rollback();
                } catch (Exception ex) {
                    log.error("Blad podczas wycofywania transakcji bazy danych: " + ex);
                    response.setError(true);
                    response.setMessage("Blad podczas wycofywania transakcji bazy danych: " + ex);
                }
            }
            DSApi.closeContextIfNeeded(contextOpened);
        }
        return response;
    }

    private void validateUserData(DaneTeleadresoweBean person) throws EdmException {
        try{
            DSUser uzytkownik= DSUser.findByUsername(person.getLogin());
            DSDivision dzial = DSDivision.find(person.getGuidDzialu());
            DSDivision[] dzialyUzytkownika = uzytkownik.getDivisions();
            for (DSDivision div : dzialyUzytkownika){
                if(div.getGuid().equals(dzial.getGuid()))
                    return;
            }
        }catch (UserNotFoundException e){
            throw new EdmException("Nie znaleziono u�ytkownika dla podanego loginu: "+e);
        }catch (DivisionNotFoundException e){
            throw new EdmException("Nie znaleziono dzia�u dla podanego GUID: "+e);
        }
        throw new EdmException("Wskazany uzytkownik nie nale�y do dzia�u, kt�ry zosta� wskazany.");
    }

    private Recipient createRecipientFromRequest(DaneTeleadresoweBean odbiorca) throws EdmException {
        Recipient recipient = new Recipient();
        recipient.setNip(odbiorca.getNip());
        recipient.setCountry(odbiorca.getKraj());
        recipient.setStreet(odbiorca.getUlica());
        recipient.setEmail(odbiorca.getEmail());
        recipient.setFax(odbiorca.getFax());
        recipient.setFirstname(odbiorca.getImie());
        recipient.setLastname(odbiorca.getNazwisko());
        recipient.setLocation(odbiorca.getMiejscowosc());
        recipient.setOrganization(odbiorca.getOrganizacja());
        recipient.setOrganizationDivision(odbiorca.getOddzial());
        recipient.setTitle(odbiorca.getTytul());
        recipient.setPesel(odbiorca.getPesel());
        recipient.setRegon(odbiorca.getRegon());
        recipient.setZip(odbiorca.getKodPocztowy());

        recipient.create();
        return recipient;
    }

    private Sender createSenderFromRequest(DaneTeleadresoweBean nadawca) throws EdmException {
        Sender sender = new Sender();
        sender.setNip(nadawca.getNip());
        sender.setCountry(nadawca.getKraj());
        sender.setStreet(nadawca.getUlica());
        sender.setEmail(nadawca.getEmail());
        sender.setFax(nadawca.getFax());
        sender.setFirstname(nadawca.getImie());
        sender.setLastname(nadawca.getNazwisko());
        sender.setLocation(nadawca.getMiejscowosc());
        sender.setOrganization(nadawca.getOrganizacja());
        sender.setOrganizationDivision(nadawca.getOddzial());
        sender.setTitle(nadawca.getTytul());
        sender.setPesel(nadawca.getPesel());
        sender.setRegon(nadawca.getRegon());
        sender.setZip(nadawca.getKodPocztowy());

        sender.create();
        return sender;
    }

    private File catchAttachmentFromRequest(String zalacznik) {
        File file = null;
        try{
            if (!zalacznik.isEmpty() && zalacznik != null){

                MessageContext msgCtx = MessageContext.getCurrentMessageContext();

                org.apache.axiom.attachments.Attachments attachment = msgCtx.getAttachmentMap();

                DataHandler dataHandler = attachment.getDataHandler(zalacznik);

                file = new File(zalacznik);

                FileOutputStream fileOutputStream = new FileOutputStream(file);
                dataHandler.writeTo(fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();
            }
        } catch (Exception e){
            if (file.exists()){
                file.delete();
            }
            file = null;
        }
        return file;
    }

    /**
     * Metoda s�u�aca do tworzenia sprawy/teczki/podteczki w systemie wraz ze ustawianiem odpowiednich warto�ci
     * @param request - {@link AddOfficeCaseRequest} obiekt ��dania
     *
     * @return {@link AddOfficeCaseResponse} response - obiekt odpowiedzi (posiada informacje, czy sprawa/teczka zosta�a dodana poprawnie do systemu
     * */
    public AddOfficeCaseResponse addOfficeCase(AddOfficeCaseRequest request){
        AddOfficeCaseResponse response = new AddOfficeCaseResponse();
        response.setError(false);
        response.setMessage("Uruchomiono proces tworzenia sprawy");

        if(request.getSprawa() == null ||
                request.getSprawa().getUzytkTworzacy() == null ||
                request.getSprawa().getUzytkTworzacy().equals("") ||
                request.getSprawa().getReferent() == null ||
                request.getSprawa().getReferent().equals("") ||
                request.getSprawa().getPrzewTerminZak() == null ||
                request.getSprawa().getPriorytet() == null ||
                request.getSprawa().getTytul() == null ||
                request.getSprawa().getTytul().equals("") ||
                request.getSprawa().getOpis() == null ||
                request.getSprawa().getOpis().equals("")
                )
        {
            response.setError(true);
            response.setMessage("Brak wymaganych danych sprawy.");return response;
        }

        if(request.getSprawa().getZnakSprawy() != null){
            boolean contextOpened = false;
            try {
                contextOpened = DSApi.openContextIfNeeded();
                if(!DSApi.context().inTransaction())
                    DSApi.context().begin();
                OfficeCase sprawa = OfficeCase.findByOfficeId(request.getSprawa().getZnakSprawy());

                //sprawa istnieje
                response.setError(true);
                response.setMessage("Istnieje ju� sprawa o znaku " + request.getSprawa().getZnakSprawy() + ".");
                response.setSprawaId(sprawa.getId());
                return response;
            } catch (CaseNotFoundException e) {
                if(request.getTeczka() == null){
                    response.setError(true);
                    response.setMessage("Brak pola <teczka>!");
                    return response;
                }

                if(request.getTeczka().getZnakTeczki() != null){
                    try {
                        OfficeFolder teczka = OfficeFolder.findByOfficeId(request.getTeczka().getZnakTeczki());
                        //teczka istnieje
                        //tworzenie sprawy dla tej teczki
                        DSUser referent = null, uzytkTworzacy = null;
                        try{
                            referent = DSUser.findByUsername(request.getSprawa().getReferent());
                            uzytkTworzacy = DSUser.findByUsername(request.getSprawa().getUzytkTworzacy());
                        }catch (UserNotFoundException ex){
                            if(referent != null){
                                log.error("Nie znaleziono uzytkownika dla pola uzytkTworzacy. "+ex);
                                response.setError(true);
                                response.setMessage("Nie znaleziono uzytkownika dla pola uzytkTworzacy. "+ex);
                                return response;
                            }else{
                                log.error("Nie znaleziono uzytkownika dla pola referent. "+ex);
                                response.setError(true);
                                response.setMessage("Nie znaleziono uzytkownika dla pola referent. "+ex);
                                return response;
                            }
                        }
                        OfficeCase sprawa = OfficeUtils.newOfficeCase(
                                teczka.getRwa().getRwa(),
                                teczka,
                                null,
                                request.getSprawa().getTytul(),
                                request.getSprawa().getOpis(),
                                referent.getName(),
                                request.getSprawa().getZnakSprawy()
                        );
                        response.setMessage("Tworzenie sprawy pomyslnie zakonczone w EOD.");
                        response.setSprawaId(sprawa.getId());
                        return response;
                    } catch (CaseNotFoundException ex) {
                        //teczka nie istnieje
                        //tworzenie teczki
                        if(request.getTeczka().getKatRWA() == null){
                            log.error("Nie podano kategorii archiwalnej RWA dla nowotworzonej teczki.");
                            response.setError(true);
                            response.setMessage("Nie podano kategorii archiwalnej RWA dla nowotworzonej teczki.");
                            return response;
                        } else if(request.getTeczka().getGuidDzialu()== null){
                            log.error("Nie podano dzialu dla nowotworzonej teczki.");
                            response.setError(true);
                            response.setMessage("Nie podano dzialu dla nowotworzonej teczki.");
                            return response;
                        }

                        try {
                            Rwa rwa = null;
                            rwa = Rwa.findByCode(request.getTeczka().getKatRWA());
                            //rwa znalezione
                            DSDivision dzial = DSDivision.find(request.getTeczka().getGuidDzialu());
                            OfficeFolder teczkaNadrzedna = null;
                            if(request.getTeczka().getZnakTeczkiNadrzednej()!=null &&
                                    !"".equals(request.getTeczka().getZnakTeczkiNadrzednej())){
                                try{
                                    teczkaNadrzedna  = OfficeFolder.findByOfficeId(
                                            request.getTeczka().getZnakTeczkiNadrzednej());
                                }catch(CaseNotFoundException e1){
                                    throw new EdmException("Nie znaleziono teczki dla danego znaku: "+e1);
                                }
                            }

                            OfficeFolder teczka = OfficeUtils.newOfficeFolder(
                                    rwa.getId(),
                                    dzial.getGuid(),
                                    null,
                                    request.getTeczka().getTytul(),
                                    request.getTeczka().getDniNaZalatwienie().toString(),
                                    request.getTeczka().getZnakTeczki(),
                                    teczkaNadrzedna
                            );
//                                    createOfficeFolder(rwa.getDescription(), dzial, rwa, null, request.getTeczka().getDniNaZalatwienie());
                            DSUser referent = null, uzytkTworzacy = null;
                            referent = DSUser.findByUsername(request.getSprawa().getReferent());
                            uzytkTworzacy = DSUser.findByUsername(request.getSprawa().getUzytkTworzacy());
                            //tworzenie sprawy dla tej teczki
                            OfficeCase sprawa = OfficeUtils.newOfficeCase(
                                    rwa.getRwa(),
                                    teczka,
                                    null,
                                    request.getSprawa().getTytul(),
                                    request.getSprawa().getOpis(),
                                    referent.getName(),
                                    request.getSprawa().getZnakSprawy()
                            );
                            response.setMessage("Tworzenie teczki i sprawy pomyslnie zakonczone w EOD.");
                            response.setSprawaId(sprawa.getId());
                            response.setTeczkaId(teczka.getId());
                            return response;
                        } catch (Exception e1) {
                            log.error("B��d podczas tworzenia nowej teczki: " + e1);
                            response.setError(true);
                            response.setMessage("B��d podczas tworzenia nowej teczki: "+e1);
                            return response;
                        }
                    } catch (Exception ex){
                        log.error("B��d podczas tworzenia nowej teczki: "+e);
                        response.setError(true);
                        response.setMessage("B��d podczas tworzenia nowej teczki: "+e);
                        return response;
                    }
                } else {
                    log.error("Nie podano znaku teczki dla nowotworzonej sprawy.");
                    response.setError(true);
                    response.setMessage("Nie podano znaku teczki dla nowotworzonej sprawy.");
                    return response;
                }
            } catch (Exception e){
                log.error("B��d podczas tworzenia nowej sprawy: "+e);
                response.setError(true);
                response.setMessage("B��d podczas tworzenia nowej sprawy: "+e);
                return response;
            }finally {
                try {
                    DSApi.context().commit();
                } catch (Exception e) {
                    log.error("Blad podczas zatwierdzania transakcji bazy danych: " + e);
                    response.setError(true);
                    response.setMessage("Blad podczas otwierania kontekstu bazy danych: " + e);
                    try {
                        DSApi.context().rollback();
                    } catch (Exception ex) {
                        log.error("Blad podczas wycofywania transakcji bazy danych: " + ex);
                        response.setError(true);
                        response.setMessage("Blad podczas wycofywania transakcji bazy danych: " + ex);
                        return response;
                    }
                }
                DSApi.closeContextIfNeeded(contextOpened);
            }
        }else{
            log.error("Nie podano znaku sprawy.");
            response.setError(true);
            response.setMessage("Nie podano znaku sprawy.");
            return response;
        }
    }
}