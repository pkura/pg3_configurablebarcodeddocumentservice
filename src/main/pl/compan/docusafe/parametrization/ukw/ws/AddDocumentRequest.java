package pl.compan.docusafe.parametrization.ukw.ws;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 26.09.13
 * Time: 09:32
 * To change this template use File | Settings | File Templates.
 */
public class AddDocumentRequest {
    private String login;
    private String guidDzialu;
    /**
     * 0 - przychodzacy
     * 1 - wychodzacy
     */
    private Integer rodzajDokumentu;
    private String zalacznik;
    private String znakSprawy;
    private String tytul;
    private DaneTeleadresoweBean nadawca;
    private DaneTeleadresoweBean odbiorca;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
    /**
     * 0 - przychodzacy
     * 1 - wychodzacy
     */
    public Integer getRodzajDokumentu() {
        return rodzajDokumentu;
    }

    public void setRodzajDokumentu(Integer rodzajDokumentu) {
        this.rodzajDokumentu = rodzajDokumentu;
    }

    public String getZalacznik() {
        return zalacznik;
    }

    public void setZalacznik(String zalacznik) {
        this.zalacznik = zalacznik;
    }

    public String getZnakSprawy() {
        return znakSprawy;
    }

    public void setZnakSprawy(String znakSprawy) {
        this.znakSprawy = znakSprawy;
    }

    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public String getGuidDzialu() {
        return guidDzialu;
    }

    public void setGuidDzialu(String guidDzialu) {
        this.guidDzialu = guidDzialu;
    }

    public DaneTeleadresoweBean getNadawca() {
        return nadawca;
    }

    public void setNadawca(DaneTeleadresoweBean nadawca) {
        this.nadawca = nadawca;
    }

    public DaneTeleadresoweBean getOdbiorca() {
        return odbiorca;
    }

    public void setOdbiorca(DaneTeleadresoweBean odbiorca) {
        this.odbiorca = odbiorca;
    }
}
