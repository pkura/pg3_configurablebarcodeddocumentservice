package pl.compan.docusafe.parametrization.ukw.ws;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 26.09.13
 * Time: 10:35
 * To change this template use File | Settings | File Templates.
 */
public class AddOfficeCaseResponse {
    public Long sprawaId;
    public Long teczkaId;
    public boolean isError;
    public String message;

    public Long getSprawaId() {
        return sprawaId;
    }

    public void setSprawaId(Long sprawaId) {
        this.sprawaId = sprawaId;
    }

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getTeczkaId() {
        return teczkaId;
    }

    public void setTeczkaId(Long teczkaId) {
        this.teczkaId = teczkaId;
    }
}
