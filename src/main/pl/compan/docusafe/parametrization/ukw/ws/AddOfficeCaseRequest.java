package pl.compan.docusafe.parametrization.ukw.ws;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 26.09.13
 * Time: 10:36
 * To change this template use File | Settings | File Templates.
 */
public class AddOfficeCaseRequest {
    private SprawaBean sprawa;
    private TeczkaBean teczka;

    public TeczkaBean getTeczka() {
        return teczka;
    }

    public void setTeczka(TeczkaBean teczka) {
        this.teczka = teczka;
    }

    public SprawaBean getSprawa() {
        return sprawa;
    }

    public void setSprawa(SprawaBean sprawa) {
        this.sprawa = sprawa;
    }
}
