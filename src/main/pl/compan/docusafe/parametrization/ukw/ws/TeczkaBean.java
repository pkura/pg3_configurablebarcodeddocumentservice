package pl.compan.docusafe.parametrization.ukw.ws;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 26.09.13
 * Time: 09:41
 * To change this template use File | Settings | File Templates.
 */
public class TeczkaBean {
    private String znakTeczki;
    public String guidDzialu;
    public String katRWA;
    public String tytul;
    public Integer dniNaZalatwienie;
    public String znakTeczkiNadrzednej;

    public String getZnakTeczkiNadrzednej() {
        return znakTeczkiNadrzednej;
    }

    public void setZnakTeczkiNadrzednej(String znakTeczkiNadrzednej) {
        this.znakTeczkiNadrzednej = znakTeczkiNadrzednej;
    }

    public String getGuidDzialu() {
        return guidDzialu;
    }

    public void setGuidDzialu(String guidDzialu) {
        this.guidDzialu = guidDzialu;
    }

    public String getKatRWA() {
        return katRWA;
    }

    public void setKatRWA(String katRWA) {
        this.katRWA = katRWA;
    }

    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public Integer getDniNaZalatwienie() {
        return dniNaZalatwienie;
    }

    public void setDniNaZalatwienie(Integer dniNaZalatwienie) {
        this.dniNaZalatwienie = dniNaZalatwienie;
    }

    public String getZnakTeczki() {
        return znakTeczki;
    }

    public void setZnakTeczki(String znakTeczki) {
        this.znakTeczki = znakTeczki;
    }
}
