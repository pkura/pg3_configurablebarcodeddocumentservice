package pl.compan.docusafe.parametrization.ukw.ws;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 26.09.13
 * Time: 09:32
 * To change this template use File | Settings | File Templates.
 */
public class AddDocumentResponse {
    public Long documentId;
    public boolean isError;
    public String message;

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
