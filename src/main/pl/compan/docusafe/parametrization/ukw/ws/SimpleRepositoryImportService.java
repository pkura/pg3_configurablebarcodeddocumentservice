package pl.compan.docusafe.parametrization.ukw.ws;

import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.axis2.AxisFault;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.imports.simple.ServicesUtils;
import pl.compan.docusafe.core.imports.simple.repository.CacheHandler;
import pl.compan.docusafe.parametrization.ukw.entities.UKWDictionary;
import pl.compan.docusafe.core.imports.simple.repository.DockindUpdater;
import pl.compan.docusafe.core.imports.simple.repository.ReposiotryAttributeDict;
import pl.compan.docusafe.core.imports.simple.repository.RepositoryAttributesSerializer;
import pl.compan.docusafe.parametrization.ukw.entities.UKWSimpleRepositoryAttribute;
import pl.compan.docusafe.service.Console;
import pl.compan.docusafe.service.Property;
import pl.compan.docusafe.service.Service;
import pl.compan.docusafe.service.ServiceDriver;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.TextUtils;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.parametrization.ukw.ws.DictionaryServicesRepositoryStub;
import pl.compan.docusafe.parametrization.ukw.ws.DictionaryServicesRepositoryStub.GetRepositoryAttributesContext;
import pl.compan.docusafe.parametrization.ukw.ws.DictionaryServicesRepositoryStub.GetRepositoryAttributesContextResponse;
import pl.compan.docusafe.parametrization.ukw.ws.DictionaryServicesRepositoryStub.GetRepositoryClassInstancesExt;
import pl.compan.docusafe.parametrization.ukw.ws.DictionaryServicesRepositoryStub.GetRepositoryClassInstancesExtResponse;
import pl.compan.docusafe.parametrization.ukw.ws.DictionaryServicesRepositoryStub.RepositoryAttributesContext;
import pl.compan.docusafe.parametrization.ukw.ws.DictionaryServicesRepositoryStub.RepositoryClassInstance;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public abstract class SimpleRepositoryImportService extends ServiceDriver implements Service {
	private Timer timer;
	private Long period = 10l;
	private Boolean run = false;
	private Boolean reloadDockind = false;
	private boolean syncing = false;
	
	private Property[] properties;

	class PeriodSyncTimeProperty extends Property {
		public PeriodSyncTimeProperty() {
			super(SIMPLE, PERSISTENT, SimpleRepositoryImportService.this, "period", "Czestotliwo�c synchronizacji w sekundach", String.class);
		}

		protected Object getValueSpi() {
			return period;
		}

		protected void setValueSpi(Object object) throws ServiceException {
			synchronized (SimpleRepositoryImportService.this) {
				if (object != null) {
					period = Long.parseLong((String) object);
				} else {
					period = 1440L;
				}
			}
		}
	}
	
	class RunProperty extends Property {
		public RunProperty() {
			super(SIMPLE, PERSISTENT, SimpleRepositoryImportService.this, "run", "Synchronizacja [w�/wy�]", Boolean.class);
		}

		protected Object getValueSpi() {
			return run;
		}

		protected void setValueSpi(Object object) throws ServiceException {
			synchronized (SimpleRepositoryImportService.this) {
				if (object != null) {
					run = (Boolean) object;
				} else {
					run = false;
				}
			}
		}
	}

	class ReloadDockindProperty extends Property {
		public ReloadDockindProperty() {
			super(SIMPLE, PERSISTENT, SimpleRepositoryImportService.this, "reloadDockind", "Prze�adowuj dockinda po ka�dej synchronizacji (Uwaga, powoduje blokowanie pracy z dokumentem w systemie)", Boolean.class);
		}

		protected Object getValueSpi() {
			return reloadDockind;
		}

		protected void setValueSpi(Object object) throws ServiceException {
			synchronized (SimpleRepositoryImportService.this) {
				if (object != null) {
                    reloadDockind = (Boolean) object;
				} else {
                    reloadDockind = false;
				}
			}
		}
	}
	
	@Override
	public Property[] getProperties() {
		return properties;
	}
	
	public SimpleRepositoryImportService() {
		properties = new Property[] { new PeriodSyncTimeProperty() , new RunProperty() , new ReloadDockindProperty()};
	}
	
	@Override
	protected void start() throws pl.compan.docusafe.service.ServiceException {
		timer = new Timer(true);
		timer.schedule(new Import(), 60*DateUtils.SECOND, period*DateUtils.SECOND);
		
	}

	@Override
	protected void stop() throws pl.compan.docusafe.service.ServiceException {
		if (timer != null) {
			timer.cancel();
		}
	}

	@Override
	protected boolean canStop() {
		return !syncing;
	}

	public abstract String getDockindCn() throws EdmException;
	public abstract String getDynamicDictionaryCn() throws EdmException;
	public abstract Long getContextId() throws EdmException;
	public abstract Long getDictIdToOmitDictionariesUpdate() throws EdmException;
	public abstract List<Long> getObjectIds() throws EdmException;
	
	class Import extends TimerTask {
		private static final String REPOSITORY_TABLE_NAME_PREFIX = "dbo.dsd_repo";
		private static final String SERVICES_REPOSITORY_ATTRIBUTES_TABLE = "dsg_simple_repository_attribute";

		private static final String SERVICE_PATH = "/services/DictionaryServicesRepository";
		
		long periodCounter = 0;
		
		DictionaryServicesRepositoryStub stub;
		
		public DictionaryServicesRepositoryStub getStub() throws AxisFault, Exception {
			if (stub == null) {
				AxisClientConfigurator conf = new AxisClientConfigurator();
				stub = new DictionaryServicesRepositoryStub(conf.getAxisConfigurationContext());
				conf.setUpHttpParameters(stub, SERVICE_PATH);
			}
			stub._getServiceClient().cleanupTransport();
			return stub;
		}
		
		@Override
		public void run() {
			if (run) {
				synchronized (this) {
					try {
						console(Console.INFO, "-----Rozpocz�to synchronizacj�-----");
						DSApi.openAdmin();
						syncing = true;
						DictionaryServicesRepositoryStub stub = getStub();
						stub._getServiceClient().cleanupTransport();
						DSApi.context().begin();
						try {
							Long dictToOmitDictionariesUpdate = getDictIdToOmitDictionariesUpdate();
							List<Long> objectIds = getObjectIds();
							Long contextId = getContextId();
							String docKindCn = getDockindCn();
							String dynamicDictionaryCn = getDynamicDictionaryCn();
							
							List<String> itemIds = Lists.newLinkedList();
							
						
							console(Console.INFO, "Rozpocz�to pobieranie definicji s�ownik�w");
							
							UKWSimpleRepositoryAttribute repositoryInstance;
							UKWDictionary dictionariesInstance;
							for (Long objectId : objectIds) {
								GetRepositoryAttributesContext params = new GetRepositoryAttributesContext();
								params.setContextId(contextId);
								params.setObjectId(objectId);
								GetRepositoryAttributesContextResponse response = stub.getRepositoryAttributesContext(params);

								if (response != null && response.get_return() != null) {
									RepositoryAttributesContext[] results = response.get_return();

									for (RepositoryAttributesContext item : results) {
										if (item != null) {
											
											Preconditions.checkNotNull(item.getRepKlasaId());
											Preconditions.checkNotNull(item.getRepAtrybutId());
											Preconditions.checkNotNull(item.getRepAtrybutIdo());
											repositoryInstance = UKWSimpleRepositoryAttribute.getInstance();
											List<UKWSimpleRepositoryAttribute> founds = repositoryInstance.find(objectId.intValue(), contextId.intValue(),item.getRepAtrybutId());
											
											if (!founds.isEmpty()) {
												for (UKWSimpleRepositoryAttribute found : founds) {
													found.setAllFieldsFromServiceObject(item);
													found.setAvailable(true);
													found.setUsed(true);
													found.setContextId(contextId.intValue());
													found.setObjectId(objectId.intValue());
													found.save();

													itemIds.add(found.getLongId().toString());
												}
											} else {
												UKWSimpleRepositoryAttribute attr = new UKWSimpleRepositoryAttribute();
												attr.setAllFieldsFromServiceObject(item);
												attr.setTableName(REPOSITORY_TABLE_NAME_PREFIX+item.getRepKlasaId()+"_"+TextUtils.normalizeForSqlTableName(item.getRepAtrybutIdo()));
												attr.setAvailable(true);
												attr.setUsed(true);
												attr.setContextId(contextId.intValue());
												attr.setObjectId(objectId.intValue());
												attr.save();

												itemIds.add(attr.getLongId().toString());
											}
										}
									}
								}
							}
							console(Console.INFO, "Zako�czono pobieranie definicji s�ownik�w");
							console(Console.INFO, "Aktualizuje dost�pne s�owniki repozytorium");
							ServicesUtils.disableDeprecatedItem(true, itemIds, SERVICES_REPOSITORY_ATTRIBUTES_TABLE, "id", "context_id", contextId);
							
							repositoryInstance = UKWSimpleRepositoryAttribute.getInstance();
							List<UKWSimpleRepositoryAttribute> founds = repositoryInstance.findAvailable(true, contextId.intValue());
							
							List<ReposiotryAttributeDict> attrEnums = Lists.newArrayList();
							ReposiotryAttributeDict attribute;
							
							dictionariesInstance = UKWDictionary.getInstance();
							if (!founds.isEmpty()) {
								
								Set<Integer> availableDictIds = Sets.newHashSet();
								
								console(Console.INFO, "Rozpocz�to pobieranie warto�ci s�ownik�w");
								for (UKWSimpleRepositoryAttribute found : founds) {
									
									updateDictionaries(dictionariesInstance, availableDictIds, found, dictToOmitDictionariesUpdate, docKindCn);
									
									attribute = new ReposiotryAttributeDict();
									attribute.setTableName(found.getTableName());
									attribute.setCn(found.getTableName().toUpperCase(Locale.ENGLISH).substring(4));
									attribute.setColumnName(attribute.getCn());
									attribute.setTitle(found.getRepAtrybutIdo());
									attribute.setCode(found.getRepKlasaId());

									GetRepositoryClassInstancesExt params = new GetRepositoryClassInstancesExt();
									params.setRepAtrybutId(found.getRepAtrybutId());

									GetRepositoryClassInstancesExtResponse response = stub.getRepositoryClassInstancesExt(params);
									if (response != null && response.get_return() != null) {
										RepositoryClassInstance[] results = response.get_return();

										for (RepositoryClassInstance attrResult : results) {
											if (attrResult != null) {
												addItemToDict(attribute, attrResult, found.getRepAtrybutId());
											}
										}
									}
									attrEnums.add(attribute);
								}
								console(Console.INFO, "Zako�czono pobieranie warto�ci s�ownik�w");
								
								console(Console.INFO, "Aktualizuje dost�pne s�owniki");
								for (UKWDictionary dict : dictionariesInstance.findRepositoryDictToDelete(docKindCn, availableDictIds)) {
									dict.setDeleted(true);
									dict.save();
								}

								RepositoryAttributesSerializer serializer = new RepositoryAttributesSerializer(attrEnums);
								console(Console.INFO, "Synchronizuje tabele s�ownikowe");
								serializer.synchronizeTables();
								console(Console.INFO, "Synchronizuje warto�ci tabeli s�ownikowych");
								serializer.synchronizeValues();
								console(Console.INFO, "Synchronizuje definicje dokumentu");
								DockindUpdater updater = new DockindUpdater(attrEnums, dictToOmitDictionariesUpdate);
								updater.updateDockindFields(docKindCn, dynamicDictionaryCn, reloadDockind);
							} else {
								console(Console.INFO, "Brak s�ownik�w w repozytorium");
							}
							
							
							DSApi.context().commit();
							CacheHandler.cleanUpCache();
							console(Console.INFO, "-----Zako�czono synchronizacj�-----");
						} catch (Exception e) {
							DSApi.context().rollback();
							throw new EdmException(e);
						}
					} catch (Exception e) {
						console(Console.ERROR, ": " + e.getMessage());
						log.error(e.getMessage(), e);
					} finally {
						syncing = false;
						try {
							DSApi.close();
						} catch (EdmException e) {
							console(Console.ERROR, ": BLAD " + e.getMessage());
							log.error(e.getMessage(), e);
						}
					}
				}
			}
		}

		private void updateDictionaries(UKWDictionary dictionariesInstance, Set<Integer> availableDictIds, UKWSimpleRepositoryAttribute found, Long dictToOmit, String dockindCn)
				throws EdmException {
			String newTableName;
			if (dictToOmit == null || !found.getRepKlasaId().equals(dictToOmit)) {
				UKWDictionary dictionary = dictionariesInstance.find(found.getRepKlasaId().toString(), dockindCn);
				if (dictionary == null) {
					dictionary = new UKWDictionary();
					dictionary.setCode(found.getRepKlasaId());
					dictionary.setCn(found.getTableName().toUpperCase(Locale.ENGLISH).substring(4));
					dictionary.setTablename(found.getTableName());
					dictionary.setDictSource(UKWDictionary.SourceType.REPOSITORY);
                    dictionary.setDockindCn(dockindCn);
					dictionary.save();
				}
				availableDictIds.add(dictionary.getId());
			}
		}

		private void addItemToDict(ReposiotryAttributeDict attribute, RepositoryClassInstance attrResult, Long repAtrybutId) {
			List<String> values = Lists.newArrayList(Splitter.on('|').trimResults().split(attrResult.getValue()));
			attribute.addItem(attrResult.getId(), values.size()>0?values.get(0):null, values.size()>1?values.get(1):null, repAtrybutId);
		}
	}
}
