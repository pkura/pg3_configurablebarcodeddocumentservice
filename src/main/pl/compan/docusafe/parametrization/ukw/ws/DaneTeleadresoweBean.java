package pl.compan.docusafe.parametrization.ukw.ws;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 02.10.13
 * Time: 14:26
 * To change this template use File | Settings | File Templates.
 */
public class DaneTeleadresoweBean {
    private String tytul;
    private String imie;
    private String nazwisko;
    private String organizacja;
    private String oddzial;
    private String ulica;
    private String kodPocztowy;
    private String miejscowosc;
    private String pesel;
    private String nip;
    private String regon;
    private String kraj;
    private String email;
    private String fax;
    private String login;
    private String guidDzialu;

    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getOrganizacja() {
        return organizacja;
    }

    public void setOrganizacja(String organizacja) {
        this.organizacja = organizacja;
    }

    public String getOddzial() {
        return oddzial;
    }

    public void setOddzial(String oddzial) {
        this.oddzial = oddzial;
    }

    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public String getKodPocztowy() {
        return kodPocztowy;
    }

    public void setKodPocztowy(String kodPocztowy) {
        this.kodPocztowy = kodPocztowy;
    }

    public String getMiejscowosc() {
        return miejscowosc;
    }

    public void setMiejscowosc(String miejscowosc) {
        this.miejscowosc = miejscowosc;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getRegon() {
        return regon;
    }

    public void setRegon(String regon) {
        this.regon = regon;
    }

    public String getKraj() {
        return kraj;
    }

    public void setKraj(String kraj) {
        this.kraj = kraj;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getGuidDzialu() {
        return guidDzialu;
    }

    public void setGuidDzialu(String guidDzialu) {
        this.guidDzialu = guidDzialu;
    }
}
