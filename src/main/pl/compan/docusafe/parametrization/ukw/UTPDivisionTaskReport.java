package pl.compan.docusafe.parametrization.ukw;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.querybuilder.TableAlias;
import pl.compan.docusafe.util.querybuilder.select.FromClause;
import pl.compan.docusafe.util.querybuilder.select.OrderClause;
import pl.compan.docusafe.util.querybuilder.select.SelectClause;
import pl.compan.docusafe.util.querybuilder.select.SelectColumn;
import pl.compan.docusafe.util.querybuilder.select.SelectQuery;
import pl.compan.docusafe.util.querybuilder.select.WhereClause;

public class UTPDivisionTaskReport extends Report {

	private String komorka;
	private String zalatwioneSprawy;
	private UniversalTableDumper dumper;

	public void initParametersAvailableValues() throws ReportException {
		super.initParametersAvailableValues();

		try {

			for (ReportParameter rp : params) {

				if (rp.getFieldCn().equals("komorka")) {

					rp.setAvailableValues(pobierzWszytkieKomorkiOrganizacyjne());
				}
				if (rp.getFieldCn().equals("zalatwioneSprawy")) {
					Map<String, String> zal=new HashMap<String,String>(2);
					zal.put("TAK","TAK");
					zal.put("NIE","NIE");
					rp.setAvailableValues(zal);
				}
				
				
			}
		} catch (Exception e) {
			Logger.getLogger(UTPDivisionTaskReport.class.getName()).log(Level.SEVERE, null, e);
		}
	}

	@Override
	public void doReport() throws Exception {
		for (ReportParameter rp : params) {
			if (rp.getFieldCn().equals("komorka")) {
				komorka = rp.getValueAsString();
			}
			if (rp.getFieldCn().equals("zalatwioneSprawy")) {
				zalatwioneSprawy = rp.getValueAsString();
			}
		}

		dumper = new UniversalTableDumper();
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "raport.xls");
		poiDumper.openFile(xlsFile);
		FileOutputStream fis = new FileOutputStream(xlsFile);

		doReportForAll().getWorkbook().write(fis);
		fis.close();
		dumper.addDumper(poiDumper);
	}

	private UTPDivisionTaskXlsReport doReportForAll() throws Exception {
		boolean boolZalatwioneSprawy=false;
		if (zalatwioneSprawy!=null&&!zalatwioneSprawy.equals("")){
			if(zalatwioneSprawy.equals("TAK"))
				boolZalatwioneSprawy=true;
			}
		
		UTPDivisionTaskXlsReport xlsReport = new UTPDivisionTaskXlsReport(
				"Raport zada� przydzielonych kom�rce organizacyjnej",boolZalatwioneSprawy);

		if (komorka != null && !komorka.equals(""))
			xlsReport.setKomorka(komorka);
		if (zalatwioneSprawy!=null&&!zalatwioneSprawy.equals("")){
			if(zalatwioneSprawy.equals("TAK"))
				xlsReport.setZalatwioneSprawy(true);
			else xlsReport.setZalatwioneSprawy(false);
			}
		else xlsReport.setZalatwioneSprawy(boolZalatwioneSprawy);
		
		xlsReport.setName(this.username);
		xlsReport.generate();
		return xlsReport;
	}

	// zwraca komorki organizacyjne
	private Map<String, String> pobierzWszytkieKomorkiOrganizacyjne() {
		Map<String, String> komorki = new TreeMap<String, String>();
		try {
			// ////
			FromClause from = new FromClause();
			TableAlias divisionTable = from.createTable("DS_DIVISION");
			WhereClause where = new WhereClause();
			OrderClause order = new OrderClause();

			SelectClause selectId = new SelectClause(true);
			SelectColumn colId = selectId.add(divisionTable, "id");
			SelectColumn colName = selectId.add(divisionTable, "NAME");
			SelectColumn colCode = selectId.add(divisionTable, "CODE");
			// SelectColumn col = selectId.add(divisionTable, "DIVISIONTYPE");

			// where.add(Expression.ne(divisionTable.attribute("CODE"),
			// "NULL"));

			SelectQuery selectQuery = new SelectQuery(selectId, from, where,
					null);

			ResultSet rs = selectQuery.resultSet(DSApi.context().session()
					.connection());

			while (rs.next()) {
				String id = rs.getString(colId.getPosition());
				String code = rs.getString(colCode.getPosition());
				String name = rs.getString(colName.getPosition());
				if (!(code != null) || code.equals(""))
					code = " - ";
				if (!(name != null) || name.equals(""))
					name = " - ";

				komorki.put(id, code + " - " + name);

			}

		} catch (Exception e) {
			Logger.getLogger(UTPDivisionTaskReport.class.getName()).log(Level.SEVERE, null, e);
		}
		return komorki;
	}
}
