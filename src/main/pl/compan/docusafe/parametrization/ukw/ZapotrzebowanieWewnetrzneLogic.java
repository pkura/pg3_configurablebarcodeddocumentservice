package pl.compan.docusafe.parametrization.ukw;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Map;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.utp.ZamowieniePubliczneLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Maps;
import com.sun.star.lang.NullPointerException;

public class ZapotrzebowanieWewnetrzneLogic extends ZamowieniePubliczneLogic{
	
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ZapotrzebowanieWewnetrzneLogic.class);
    
	@Override
	public void setInitialValues(FieldsManager fm, int type)
			throws EdmException {
		Map<String, Object> values = Maps.newHashMap();
		if (AvailabilityManager.isAvailable("dev")) {
			try {
				String sql = "select * from ds_document"
						+ " where dockind_id = (select id from DS_DOCUMENT_KIND where cn = 'zamowienie')"
						+ " order by ID desc";
				PreparedStatement ps = DSApi.context().prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				Long docID = null;
				while (rs.next()) {
					docID = rs.getLong(1);
					break;
				}
				
				if (docID == null)
					throw new NullPointerException("Brak dokumentów typu: zamowienie");
				
				values.putAll(Document.find(docID).getFieldsManager().getFieldValues());
				
			} catch (Exception e) {
				log.error("", e);
			}
		}
		values.put(STATUS_CN, 0);
		values.put(DATA_UTWORZENIA_CN, Calendar.getInstance().getTime());
        DSUser author = DSApi.context().getDSUser();
        values.put(OS_REJESTRUJACA_CN, author.getId());
//        new BigDecimal(Docusafe.getAdditionProperty("kurs_euro"))
		values.put(KURS_CN, Docusafe.getAdditionProperty("kurs_euro"));
		fm.reloadValues(values);
		super.setInitialValues(fm, type);
	}
}
