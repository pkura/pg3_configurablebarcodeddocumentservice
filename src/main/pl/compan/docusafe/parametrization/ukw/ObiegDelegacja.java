package pl.compan.docusafe.parametrization.ukw;

import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import org.springframework.stereotype.Component;

@Component("obiegdelegacja")
public class ObiegDelegacja extends ObiegUkw{
	private final String  RODZAJ_WYJAZDU = "RODZAJ_WYJAZDU";
	private final String  KRAJOWY = "Krajowy";
	private final String  ZAGRANICZNY = "Zagraniczny";
	private final String  ATT_ZALICZKA = "ATT_ZALICZKA";
	private final String  PRACOWNIK_TYP = "PRACOWNIK_TYP";
	private final String  ADMINISTRACYJNY = "administracyjny";
	
	public boolean czyDelegacjaZagraniczna(long docId) {
		try{
			Document document =  Document.find(docId);
	        FieldsManager fm = document.getFieldsManager();
	        if(fm.getEnumItem(RODZAJ_WYJAZDU).getTitle().equals(ZAGRANICZNY))
	        	return true;
	       
		}catch (Exception e) {
			LOG.error(e.getMessage(),e);
		}

		return false;
	}
	
	public boolean czyZalacznikZaliczka(long docId)
	{
		try{
			Document document =  Document.find(docId);
			FieldsManager fm = document.getFieldsManager();
	        if( fm.getIntegerKey(ATT_ZALICZKA) != null )
	        	return true;
	        
		}catch (Exception e) {
			LOG.error(e.getMessage(),e);
		}

		return false;		
	}
	

	public String getDivisionGUIDFromAdds(String addsName)
	{
		try {
			return Docusafe.getAdditionProperty(addsName);
		} catch (Exception e) {
			LOG.error(e.getMessage(),e);
		}
		return ADMIN_NAME;	
	}

	public boolean czyPracownikAdministracyjny(long docId)
	{
		try{
			Document document =  Document.find(docId);
	        FieldsManager fm = document.getFieldsManager();
	        if(fm.getValue(PRACOWNIK_TYP).equals(ADMINISTRACYJNY))
	        	return true;
	       
		}catch (Exception e) {
			LOG.error(e.getMessage(),e);
		}
		
		return false;
	}
	
}
