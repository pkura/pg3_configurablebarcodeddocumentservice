package pl.compan.docusafe.parametrization.ukw;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;

import com.beust.jcommander.internal.Lists;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.FieldsManagerUtils;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.parametrization.utp.ZamowieniePubliczneConst;
import pl.compan.docusafe.parametrization.utp.ZamowieniePubliczneLogic;
import pl.compan.docusafe.parametrization.utp.entities.Acceptation;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ObiegZapotrzebowanieWewnetrzneBean extends AbstractObiegUkw {

    private static final Logger log = LoggerFactory.getLogger(ObiegZapotrzebowanieWewnetrzneBean.class);
    //Wyjscia dla pierwszego sprawdzenia
    private static final String DRUGIE_SPRAWDZENIE_CN = "drugieSprawdzenie";
    private static final String OPINIA_DZIALU_REMONTOW_I_INWESTYCJI_CN = "opiniaDzialuRemontowIInwestycji";
    private static final String OPINIA_DZIALU_TELEINFORMATYKI_CN = "opiniaDzialuTeleinformatyki";
    private static final String ZATWIERDZENIE_KANCLERZA_CN = "zatwierdzenieKanclerza";
    //Wyj�cia dla drugiego sprawdzenia
    private static final String AKCEPTACJA_SPECJALISTY_FINANSOWEGO_CN = "akceptacjaSpecjalistyFinansowego";
    private static final String AKCEPTACJA_DZIALU_REMONTOW_I_INWESTYCJI_CN = "akceptacjaDzialuRemontowIInwestycji";
    private static final String AKCEPTACJA_DZIALU_TELEINFORMATYKI_CN = "akceptacjaDzialuTeleinformatyki";
    private static final String WYDANIE_ZGODY_NA_ZAKUP_CN = "wydanieZgodyNaZakup";
    private static final String AKCEPTACJA_KIEROWNIKA_DZIALU_ZAMOWIEN_PUBLICZNYCH_CN = "akceptacjaKierownikaDZP";
    //Wyjscia dla trzeciego sprawdzenia
    private static final String WSZCZECIE_POSTEPOWANIA_PRZETARGOWEGO_CN = "wszczeciePostepowaniaPrzetargowego";
    private static final String AKCEPTACJA_KIEROWNIKA_DZP_CN = "akceptacjaKierownikaDzialuZamowienPublicznych";
    private static final String REALIZACJA_ZAMOWIENIA_SAMODZIELNA_CN = "realizacjaZamowieniaSamodzielna";
    
    //Export do erp
    /*private static final String FAST_REPEAT = "fast_repeat";
    private static final String REPEAT = "repeat";
    private static final String SUCCESS_TRANSITION_WSTEPNA_REZERWACJA = "success_transition_wstepna_rezerwacja";
    private static final String SUCCESS_TRANSITION_ZAAKCEPTOWANIE = "success_transition_zaakceptowanie";
    private static final String SUCCESS_TRANSITION_ANULOWANIE = "success_transition_anulowanie";
    private static final String FAIL_TRANSITION = "fail_transition";
    private static final String DEFAULT_TRANSITION = FAIL_TRANSITION;
    public static final String TO_CONFIRM = "to-confirm";

    //ZGL_BLEDU = ZGLOSZENIE_BLEDU, Formularz do przesyłania info o błędzie do admina (usługą lub przez człowieka)  ----------
	*//** zgłoszenie błędu: ID statusu błędu nowego *//*				String ZGL_BLEDU_nowy = "11";
	*//** zgłoszenie błędu: ID błędu będącego w obsłudze *//* 			String ZGL_BLEDU_obslugiwany = "12";
	*//** zgłoszenie błędu: ID błędu rozwiązanego *//* 					String ZGL_BLEDU_rozwiazany = "13";
	*//** zgłoszenie błędu: ID błędu odrzuconego *//* 					String ZGL_BLEDU_odrzucony = "14";
	*//** zgłoszenie błędu: ID błędu odsuniętego w czasie *//* 			String ZGL_BLEDU_odsuniety= "15";

	*//** Komunikat gotowy do pobrania przez szynę. *//*				String ERP_PRZESLANO_NA_SZYNE = "NEW"; 
	*//** Komunikat został pobrany przez task *//*						String ESB_POBRANY_PRZEZ_SZYNE = "UPLOADED"; 
	*//** Komunikat jest przetwarzany przez szynę danych *//*			String ESB_PRZETWARZANY_PRZEZ_SZYNE = "PROCESSING"; 
	*//** Komunikat został poprawnie dodany do Simple.ERP *//*			String ESB_DODANO_DO_ERP = "FINISHED"; 
	*//** Komunikat nie przeszedł wstępnej walidacji XSD *//*			String ESB_BLAD_WALIDACJI_PLIKU_XML = "INVALID"; 
	*//** Komunikat nie został dodany do Simple.ERP system 
	 * zgłosił kod błędu *//* 										String ESB_BLAD_WEWNETRZNY_ERP = "FAULT"; */
    /*
	 //FIXME - 
	 private Integer fastRepeats;
	 private String transitionToTake;*/
	 
	@Override
	public String getTemplateSavePath() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTemplateName() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Pobiera uzytkownika z pola typu dsuser.
	 * W przypadku wyst�pienia wyj�tku zwraca login admina (admin).
	 * @param docId
	 * @param dsUserFieldCN - cn pola z dockind-u
	 * @return 
	 */
	public String userFromDsUserField(Long docId, String dsUserFieldCN){
		try {
			Document doc = Document.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			Long userId = fm.getLongKey(dsUserFieldCN);
			String userName = DSUser.findById(userId).getName();
			log.debug("CandidateUser: {}", userName);
			return userName;
		} catch (Exception e) {
			log.error("", e);
		}
		return "admin";
	}

    public String pierwszeSprawdzenie(Long docId) throws EdmException {
    	log.debug("pierwszeSprawdzenie");
    	if(docId == null)
        	throw new EdmException("B��d parametr�w wej�ciowych.");
    	
        try {
            boolean czyUslugaRemontowa = ZamowieniePubliczneLogic.checkIfRepairServices(docId);
            boolean czyRobotaBudownictwaTelekom = ZamowieniePubliczneLogic.checkIfDepartmentOfICT(docId);
            boolean czyKwotaWPrzedziale = ZamowieniePubliczneLogic.checkIfAmountIsInRange(
                    docId, new BigDecimal(ZamowieniePubliczneLogic.pobierzProgZaproszenia()),
                    new BigDecimal(ZamowieniePubliczneLogic.pobierzProgPrzetargu()));
            if(!czyKwotaWPrzedziale)
            	return DRUGIE_SPRAWDZENIE_CN;
            if (czyUslugaRemontowa) {
                if (czyRobotaBudownictwaTelekom)
                    return OPINIA_DZIALU_TELEINFORMATYKI_CN;
                else
                    return OPINIA_DZIALU_REMONTOW_I_INWESTYCJI_CN;
            } else
                return ZATWIERDZENIE_KANCLERZA_CN;
        } catch (Exception e) {
            log.error("B��d podczas pierwszego sprawdzenia przy wniosku zam�wienia publicznego: "+e);
            
        }
        return DRUGIE_SPRAWDZENIE_CN;
    }
    
    public String drugieSprawdzenie(Long docId) throws EdmException {
    	log.debug("drugieSprawdzenie");
        if(docId == null)
        	throw new EdmException("B��d parametr�w wej�ciowych.");
        
        /*boolean isOpened = true;
        
        if (!DSApi.isContextOpen()) {
            isOpened = false;
            DSApi.openAdmin();
        }*/
        
        try {
            boolean czyUslugaRemontowa = ZamowieniePubliczneLogic.checkIfRepairServices(docId);
            boolean czyDzialTeleinformatyki = ZamowieniePubliczneLogic.checkIfDepartmentOfICT(docId);
            boolean czyKwotaPonizej = ZamowieniePubliczneLogic.checkIfAmountIsLessOrEqual(
                    docId, new BigDecimal(ZamowieniePubliczneLogic.pobierzProgPrzetargu()));
            
            if (!czyKwotaPonizej){
                return AKCEPTACJA_SPECJALISTY_FINANSOWEGO_CN;
            }else if (czyUslugaRemontowa)
            	if(czyDzialTeleinformatyki)
            		return AKCEPTACJA_DZIALU_TELEINFORMATYKI_CN;
            	else 
            		return AKCEPTACJA_DZIALU_REMONTOW_I_INWESTYCJI_CN;
            else if (ZamowieniePubliczneLogic.checkIfCentralPurchasing(docId))
                return AKCEPTACJA_SPECJALISTY_FINANSOWEGO_CN;
            else if (ZamowieniePubliczneLogic.checkIfRiskOfExceeding(docId))
                return WYDANIE_ZGODY_NA_ZAKUP_CN;
            else if (ZamowieniePubliczneLogic.checkIfPurchaseFrom6A(docId))
                return AKCEPTACJA_KIEROWNIKA_DZIALU_ZAMOWIEN_PUBLICZNYCH_CN;
            else
                return AKCEPTACJA_SPECJALISTY_FINANSOWEGO_CN;

        } catch (Exception e) {
            log.error("B��d podczas drugiego sprawdzenia przy wniosku zam�wienia publicznego: "+e);
        }/*finally {
            if (DSApi.isContextOpen() && !isOpened)
                DSApi._close();
        }*/
        return AKCEPTACJA_SPECJALISTY_FINANSOWEGO_CN;
    }
    
    public String assigneToGuidFromAdditionPropertyWithPermissionToWholeGroup(Long docId, String additionProperty) throws EdmException {
    	log.debug("assigneToGuidFromAdditionPropertyWithPermissionToWholeGroup");
		String guids;
		if ((guids = Docusafe.getAdditionProperty(additionProperty)) != null) {
			for (String g : guids.split(",")) {
//				assignable.addCandidateGroup(g);
//				if (Boolean.parseBoolean(addToHistory))
//					addToHistory(openExecution, doc, null, g);
//				assigned = true;
				
				java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
				//nadanie uprawnien na ta grupe
				perms.add(new PermissionBean(ObjectPermission.READ, g, ObjectPermission.GROUP, "Dokumenty zwyk�y- odczyt"));
				perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, g, ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - odczyt"));
				perms.add(new PermissionBean(ObjectPermission.MODIFY, g, ObjectPermission.GROUP, "Dokumenty zwyk�y - modyfikacja"));
				perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, g, ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - modyfikacja"));
				perms.add(new PermissionBean(ObjectPermission.DELETE, g, ObjectPermission.GROUP, "Dokumenty zwyk�y - usuwanie"));
				for (PermissionBean perm : perms) {
					if (perm.isCreateGroup()
							&& perm.getSubjectType().equalsIgnoreCase(
									ObjectPermission.GROUP)) {
						String groupName = perm.getGroupName();
						if (groupName == null) {
							groupName = perm.getSubject();
						}
					}
					DSApi.context().grant(Document.find(docId), perm);
					DSApi.context().session().flush();
				}
			}
			for (String g : guids.split(",")) {
				String users = "";
				DSUser[] dsusers = DSDivision.find(g).getUsers();
				for (DSUser dsUser : dsusers) {
					users += dsUser.getName() + ' ';
				}
				log.debug("CandidateGroup:{} [{}]",g,users);
			}
		}
		return guids;
	}
    
    public Boolean checkIfPurchaseFrom6A(Long docId) throws EdmException{
    	return ZamowieniePubliczneLogic.checkIfPurchaseFrom6A(docId);
    }
        
    public String trzecieSprawdzenie(Long docId) throws EdmException {
    	log.debug("trzecieSprawdzenie");
    	if(docId == null)
        	throw new EdmException("B��d parametr�w wej�ciowych.");

        boolean isOpened = true;
        try {
            if (!DSApi.isContextOpen()) {
                isOpened = false;
                DSApi.openAdmin();
            }

            boolean czyKwotaPonizej = ZamowieniePubliczneLogic.checkIfAmountIsLessOrEqual(
                    docId, new BigDecimal(ZamowieniePubliczneLogic.pobierzProgPrzetargu()));
            if (!czyKwotaPonizej){
                if (ZamowieniePubliczneLogic.sprawdzCzyInneWylaczeniaUstawowe(docId))
                    return AKCEPTACJA_KIEROWNIKA_DZP_CN;
                else
                    return WSZCZECIE_POSTEPOWANIA_PRZETARGOWEGO_CN;
            }else if (ZamowieniePubliczneLogic.checkIfCentralPurchasing(docId)){
                if (ZamowieniePubliczneLogic.sprawdzCzyInneWylaczeniaUstawowe(docId))
                    return AKCEPTACJA_KIEROWNIKA_DZP_CN;
                else
                    return WSZCZECIE_POSTEPOWANIA_PRZETARGOWEGO_CN;
            }else if (ZamowieniePubliczneLogic.checkIfPurchaseFrom6A(docId))
                return WSZCZECIE_POSTEPOWANIA_PRZETARGOWEGO_CN;
            else
                return REALIZACJA_ZAMOWIENIA_SAMODZIELNA_CN;
        }catch (Exception e) {
            log.error("B��d podczas trzeciego sprawdzenia przy wniosku zam�wienia publicznego: "+e);
        }finally {
            if (DSApi.isContextOpen() && !isOpened)
                DSApi._close();
        }
        return WSZCZECIE_POSTEPOWANIA_PRZETARGOWEGO_CN;
    }
    
    public String pobierzDysponenta(Long docId) {
    	log.debug("pobierzDeysponenta");
    	try {
    		Document doc = Document.find(docId);
	    	Acceptation acceptation = null;
	    	if(doc.getFieldsManager().getField(ZamowieniePubliczneConst.AKCEPTACJE_DYSPONENTOW_CN).isMultiple())
	    		acceptation = Acceptation.findFirstAcceptation(ZamowieniePubliczneConst.AKCEPTACJE_DYSPONENTOW_CN, doc.getDocumentKind().getMultipleTableName(), doc.getId());
	    	else 
	    		acceptation = Acceptation.findAcceptation(doc.getFieldsManager().getLongKey(ZamowieniePubliczneConst.AKCEPTACJE_DYSPONENTOW_CN));
	    	
	        if (acceptation == null)
	            throw new Exception("Dodaj dysponent�w �rodk�w do akceptacji.");
	        DSUser dysponent = DSUser.findById(acceptation.getOsoba());
	        log.debug("CandidateUser: {}", dysponent.getName());
	        return dysponent.getName();
	    } catch (Exception e) {
	    	String admin = GlobalPreferences.getAdminUsername();
	    	log.error("cant find any user to assignee, go to " + admin);
			return admin;
		}
//        assignable.addCandidateUser(dysponent.getName());
//        log.info("For accept " + acceptationCn +
//                ") - candidateUser: " + dysponent.getName());
//        AssigneeHandler.addToHistory(openExecution, doc, dysponent.getName(), null);
//        return true;
//		return null;
    	
    }

    
    public String userFromFieldSourceType(Long docId, String fieldCN){
    	log.debug("userFromFieldSourceType");
	    try {
	    	FieldsManager fm = Document.find(docId).getFieldsManager();
			String username = fm.getEnumItemCn(fieldCN);
			log.debug("CandidateUser: {}", username);
			return username;
			/*if (Boolean.parseBoolean(addToHistory))
				addToHistory(openExecution, doc, username, null);*/
		} catch (EdmException e) {
			log.error("cant find any user to assignee for user field: " + fieldCN + ", go to admin");
			String admin = GlobalPreferences.getAdminUsername();
			log.debug("CandidateUser: {}", admin);
			return admin;
			/*if (Boolean.parseBoolean(addToHistory))
				addToHistory(openExecution, doc, admin, null);*/
		}
    }
    
    public Boolean czyUsluga(Long docId){
    	try {
    		String rodzajZamowienie = Document.find(docId).getFieldsManager().getEnumItemCn(ZamowieniePubliczneConst.RODZAJ_ZAMOWIENIA_CN);
    		if(rodzajZamowienie.equalsIgnoreCase("USLUGA"))
    			return true;
    		else
    			return false;
    		} catch (Exception e) {
    			log.error(e.getMessage(), e);
    		}
    	return false;
    }
    
    public List<String> pobierzDysponentaMulti(Long docId){
    	List<String> dysponenciRet = Lists.newArrayList();
    	try {
    		Document doc = Document.find(docId);
	    	List<Acceptation> listAcceptations = Lists.newArrayList();
	    	if(doc.getFieldsManager().getField(ZamowieniePubliczneConst.AKCEPTACJE_DYSPONENTOW_CN).isMultiple()){
//	    		acceptation = Acceptation.findFirstAcceptation(ZamowieniePubliczneConst.AKCEPTACJE_DYSPONENTOW_CN, doc.getDocumentKind().getMultipleTableName(), doc.getId());
	    		listAcceptations = Acceptation.findAcceptationForDocument(ZamowieniePubliczneConst.AKCEPTACJE_DYSPONENTOW_CN, doc.getDocumentKind().getMultipleTableName(), doc.getId());
	    	}
	    	else 
	    		listAcceptations.add(Acceptation.findAcceptation(doc.getFieldsManager().getLongKey(ZamowieniePubliczneConst.AKCEPTACJE_DYSPONENTOW_CN)));
	    	
	        if (listAcceptations.isEmpty())
	            throw new Exception("Dodaj dysponent�w �rodk�w do akceptacji.");
	        
	        for (Acceptation acc : listAcceptations) {
	        	DSUser dysponent = DSUser.findById(acc.getOsoba());
	        	dysponenciRet.add(dysponent.getName());
			}
	        log.debug("CandidateUser: {}", dysponenciRet.toString());
	    } catch (Exception e) {
	    	String admin = GlobalPreferences.getAdminUsername();
	    	log.error("cant find any user to assignee, go to " + admin);
			dysponenciRet.add(admin);
		}
    	return dysponenciRet;
    }
    
    public List<String> pobierzDysponentow(Long docId, String dictionaryCn, String fieldCn) throws EdmException {
    	List<String> dysponenciRet = Lists.newArrayList();
    	OfficeDocument doc = OfficeDocument.find(docId);
    	FieldsManager fm = doc.getFieldsManager();
    	List<EnumValues> dysopnenci = FieldsManagerUtils.getDictionaryItems(fm, dictionaryCn, dictionaryCn + "_" + fieldCn, EnumValues.class);
    	
    	for (EnumValues div : dysopnenci){
    		Long selectedId = Long.parseLong(div.getSelectedId());
    		String username = DSUser.findById(selectedId).getName();
    		if (!dysponenciRet.contains(username))
    			dysponenciRet.add(username);
         }
    	
    	log.debug("CandidateUsers[Dysponenci]: {}", dysponenciRet.toString());
    	
    	return dysponenciRet;
    }
        
    public void acceptationDysponent(Long docId, DelegateExecution execution, String dictionaryCn, String multiDictCn, String statusCn, String dateCn) throws EdmException, SQLException{

    	int acceptationStatus = AcceptationListener.getStatus(execution); 
    	
//        String currentAssignee = (String) execution.getVariable("assignee");
        String currentAssignee = DSApi.context().getDSUser().getName();
        OfficeDocument doc = OfficeDocument.find(docId);
        
        DSUser user = DSUser.findByUsername(currentAssignee);
        ZamowieniePubliczneLogic.fillAcceptationStatusForBudgetPositions(doc,
                user, new Date(), dictionaryCn, multiDictCn, statusCn, dateCn, acceptationStatus);
    }
    
    public String author(Long docId) throws UserNotFoundException {

    	Document doc = null;
    	boolean contextOpened = false;
    	try{
    		contextOpened= DSApi.openContextIfNeeded(); 
    		doc = Document.find(docId);
        	return doc.getAuthor();
    	} catch (Exception e) {
    		throw new UserNotFoundException("Nie znaleziono u�ytkownika dla dokumentu: " + docId);
    	} finally {
    		DSApi.closeContextIfNeeded(contextOpened);
    	}
    }
    
}
