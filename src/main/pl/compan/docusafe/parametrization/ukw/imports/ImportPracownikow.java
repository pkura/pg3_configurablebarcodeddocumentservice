package pl.compan.docusafe.parametrization.ukw.imports;

import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;

/**
 * Klasa odpowiadaj�ca za import  produkt�w dla faktury kosztowej z uslugi zewn�trznej.
 * @author Jerzy Pir�g
 *
 */
public class ImportPracownikow extends AbstractDictionaryImport
{
//	DictionaryServicesEmployeeStub stub;
	private static final String SERVICE_PATH = "/services/DictionaryServicesEmployee";

    private static final Logger log = LoggerFactory.getLogger(ImportPracownikow.class.getPackage().getName());
    
	@Override
	public void initConfiguration(AxisClientConfigurator conf) throws Exception {
//		stub = new DictionaryServicesEmployeeStub(conf.getAxisConfigurationContext());
//		conf.setUpHttpParameters(stub, SERVICE_PATH);
	}

	@Override
	public void doImport() throws RemoteException, EdmException {
//		if (stub != null) {
//			GetEmployees params = new GetEmployees();
//			GetEmployeesResponse response;
//			if (params != null && (response = stub.getEmployees(params)) != null) {
//				Employee[] items = response.get_return();
//				if (items != null) {
//					Set<Long> employeeIds = Sets.newHashSet();
//					for (Employee employee : items) {
//						if (employee != null) {
//							employeeIds.add(employee.getPracownikId());
//							List<PracownikERP> foundEmployees = PracownikERP.findByPracownikId(employee.getPracownikId());
//							if (foundEmployees.isEmpty()) {
//								PracownikERP newEmployee = new PracownikERP();
//								newEmployee.setImieNazwisko(employee.getImieNazwisko());
//								newEmployee.setOpkIdn(employee.getOpkIdn());
//								newEmployee.setPracownikId(employee.getPracownikId());
//								newEmployee.setPracownikNrEwid(Integer.valueOf(employee.getPracownikNrEwid()).longValue());
//								newEmployee.setPrzelozonyNrEwid(Integer.valueOf(employee.getPrzelozonyNrEwid()).longValue());
//								newEmployee.setDisabled(false);
//								newEmployee.create();
//							} else {
//								for (PracownikERP found : foundEmployees) {
//									found.setImieNazwisko(employee.getImieNazwisko());
//									found.setOpkIdn(employee.getOpkIdn());
//									found.setPracownikNrEwid(Integer.valueOf(employee.getPracownikNrEwid()).longValue());
//									found.setPrzelozonyNrEwid(Integer.valueOf(employee.getPrzelozonyNrEwid()).longValue());
//									found.setDisabled(false);
//									found.create();
//								}
//							}
//						}
//					}

					// available na 0 dla pozycji nie wyst�puj�cych ju� w erpie
//					List<PracownikERP> itemsNotFromErp = PracownikERP.findByPracownikId(employeeIds, true);
//					//TODO: dorobic wyszukiwanie produktow
//					for (PracownikERP item : itemsNotFromErp) {
//						item.setDisabled(true);
//						item.create();
//					}
				}

    @Override
    void cleanupTransport() throws AxisFault {
//        stub._getServiceClient().cleanupTransport();
    }
}
