package pl.compan.docusafe.parametrization.ukw;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.google.common.collect.Maps;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

@Component("wniosekUrlopowy")
public class ObiegWniosekUrlopowy {
	private static final Logger log = LoggerFactory.getLogger(ObiegWniosekUrlopowy.class);
	private static final String ADMIN_NAME="admin";
	
	
	public String getCurrentUser()
	{
		try {
			return DSUser.getLoggedInUser().getName();
		} catch (Exception e) {
			log.error(e.getMessage()+"B��d pr�by pobrania loginu aktualnie zalogowanego user'a",e);
		}
		return ADMIN_NAME;
	}
	
	public void getLiczbaDniUrlopowych(Long docId)
	{
		Document document;
		try {
			document = Document.find(docId);
		FieldsManager fieldsManager = document.getFieldsManager();
		Map<String,Object> fieldValues = Maps.newHashMap();
		
		fieldValues.put("WYMIARDNI", 26);
		fieldsManager.getDocumentKind().setOnly(docId, fieldValues);
		} catch (DocumentNotFoundException e) {
			log.error("",e);
		} catch (DocumentLockedException e) {
			log.error("",e);
		} catch (AccessDeniedException e) {
			log.error("",e);
		} catch (EdmException e) {
			log.error("",e);
		}
	}
	
	public void ustawStatus(Long docId, int status)
	{
		Document document;
		try {
			document = Document.find(docId);
		FieldsManager fieldsManager = document.getFieldsManager();
		Map<String,Object> fieldValues = Maps.newHashMap();
		
		fieldValues.put("STATUS", status);
		fieldsManager.getDocumentKind().setOnly(docId, fieldValues);
		} catch (Exception e) {
			log.error("",e);
		}
	}
	
	public String getAutor(Long docId)
	{
		try {
			Document doc = Document.find(docId);
			return doc.getAuthor();
		} catch (Exception e) {
			log.error(e.getMessage()+"B��d pr�by pobrania loginu aktualnie zalogowanego user'a",e);
		}
		return ADMIN_NAME;
	}
	
	public String getPrzelozony(Long docId)
	{
		try {
			Document doc = Document.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			long przelozonyId = fm.getEnumItem("PRZELOZONY").getId();
			String przelozony = DSUser.findById(przelozonyId).getName();
			return przelozony;
		} catch(Exception e) {
			log.error("", e);
		}
		return ADMIN_NAME;
	}
	
	public List<String> getKierownicyList(String divisionGuid) {
		DSDivision division;
		List<String> lista = new ArrayList<String>();
		String div = Docusafe.getAdditionProperty(divisionGuid);
		try 
		{	
			division = DSDivision.find(div);
			DSUser []user = division.getOriginalUsers();
			for (DSUser dsUser : user) {
				lista.add(dsUser.getName());
			}
			 return lista;
		} catch (DivisionNotFoundException e) {
			e.printStackTrace();
		} catch (EdmException e) {
			e.printStackTrace();
		}
		return lista;
    }
	
	public void odblokujNrEwid(Long docId) 
	{
		try {
			Document doc = Document.find(docId);
			FieldsManager fm = doc.getFieldsManager();
			fm.getField("NREWID").setReadOnly(false);
		} catch(Exception e) {
			log.error("", e);
		}
	}	
}
