package pl.compan.docusafe.parametrization.ukw;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.DateUtils;


public class UTPUserXlsReport extends AbstractXlsReports {

	private String znak;// znak sprawy
	private String identyfikator;// unikalny identyfikator
	private String symbol;// klasa z JRWA
	private String kategoria;// kategoria archiwalne
	private String tytul;// tytul sprawy
	private String komorka;// komorka organizacyjna
	private Date rozp;
	private Date zak;
	private Date utwOd;// data stworzenia dokumentu od
	private Date utwDo;// data stworzenia dokumentu do
	private String liczbaSpraw;
	private String liczbaWszystkichSpraw;
	private String stanowisko;
	boolean liczbaSprawNieBranaPodUwage = true;
	boolean liczbaWszystkichSprawNieBranaPodUwage = true;

	public UTPUserXlsReport(String reportName) {
		super(reportName);
		title = new String[] { "Raport Uzytkownik�w" };

		columns = new String[] { "Lp.", "Imi�", "Nazwisko",
				"Kom�rka organizacyjna", "Telefon", "Stanowisko", };
	}

	@Override
	protected void createEntries() {
		// przygotowanie zapytania
		StringBuilder sqlStatement = new StringBuilder(
				"select distinct  u.FIRSTNAME, u.LASTNAME,h.name,u.name as nameuser, u.MOBILE_PHONE_NUM,h.id  from dbo.DS_USER u"
						+ " left join dbo.DSW_JBPM_TASKLIST p on u.name=p.assigned_resource "
						+ " left join dbo.DS_DOCUMENT a on a.ID=p.document_id"
						+ " left join dso_in_document b on b.id=a.ID"
						+ " left join DSO_OUT_DOCUMENT c on c.ID=a.ID"
						+ " left join dso_container e on b.CASE_ID=e.ID or c.CASE_ID=e.ID"
						+ " left join v_dso_rwaa f on f.ID=e.RWA_ID "
						+ " left join dbo.DS_USER_TO_DIVISION g on g.USER_ID=u.ID "
						+ " left join dbo.DS_DIVISION h on h.ID=g.DIVISION_ID "
						+ " where u.name is not null ");

		// dodanie warunk�w
		sqlStatement.append(" AND");
		if (znak != null && !znak.equals("")) {
			sqlStatement.append(" e.officeid='" + znak + "' AND");
		}
		if (identyfikator != null && !identyfikator.equals("")) {
			sqlStatement.append(" a.id='" + identyfikator + "' AND");
		}
		if (symbol != null && !symbol.equals("")) {
			sqlStatement.append(" f.code='" + symbol + "' AND");
		}
		if (tytul != null && !tytul.equals("")) {
			sqlStatement.append(" e.title='" + title + "' AND");
		}
		if (kategoria != null && !kategoria.equals("")) {
			sqlStatement.append(" f.achome='" + kategoria + "' AND");
		}
		if (rozp != null) {
			sqlStatement.append(" e.opendate>'" + DateUtils.formatSqlDate(rozp)
					+ "' AND");
		}
		if (zak != null) {
			sqlStatement.append(" e.finishdate<'"
					+ DateUtils.formatSqlDate(zak) + "' AND");
		}
		if (komorka != null && !komorka.equals("")) {
			String[] tmp = komorka.split("-");
			sqlStatement.append(" h.code='" + tmp[0] + "' AND");
		}
		if (utwOd != null) {
			sqlStatement.append(" a.ctime>'" + DateUtils.formatSqlDate(utwOd)
					+ "' AND");
		}
		if (utwDo != null) {
			sqlStatement.append(" a.ctime<'" + DateUtils.formatSqlDate(utwDo)
					+ "' AND");
		}
		if (stanowisko != null && !stanowisko.equals("")) {
			//sqlStatement = znajdzIdKomorekPodleglych(sqlStatement, stanowisko);
			// usuniecie ostatniego 'OR'
			//sqlStatement.delete(sqlStatement.length() - 2,
			//		sqlStatement.length());
			sqlStatement.append(" h.id = "+stanowisko);
			sqlStatement.append(" AND");
		}
		if(AvailabilityManager.isAvailable("kancelaria.wlacz.wersjeDokumentu")){
			//jesli jest wlaczone wersjonowanie, to tylko po dokumentach aktualnych
			if(znak!=null || symbol!=null || kategoria!=null || tytul!=null || rozp!=null ||zak!=null || utwOd!=null || utwDo !=null){
				sqlStatement.append(" a.czy_aktualny = 1 ");
				sqlStatement.append(" AND");
			}
		}

		// usuniecie ostatniego 'AND'
		sqlStatement.delete(sqlStatement.length() - 3, sqlStatement.length());
		sqlStatement.append(" order by u.lastname asc");
		String sqlStringStatement = sqlStatement.toString();

		try {
			PreparedStatement ps = DSApi.context().prepareStatement(
					sqlStringStatement);

			ResultSet rs = ps.executeQuery();

			int rowCount = 3;
			int lp = 1;

			while (rs.next()) {
				String firstName = rs.getString("FIRSTNAME");
				String lastName = rs.getString("LASTNAME");
				String komorkaZajmowana = rs.getString("NAME");
				String stanowiskoZajmowane = (rs
						.getString("id") == null ? null: znajdzZajmowaneStanowisko(rs.getString("id")));
				String phone = rs.getString("MOBILE_PHONE_NUM");
				String name = rs.getString("nameuser");
				String liczbaSprawDlaAktualnegoUzytkownika = null;
				String liczbaWszystkichSprawDlaAktualnegoUzytkownika = null;

				// sprawdzenie ilosci spraw
				if (liczbaSpraw != null && !liczbaSpraw.equals("")) {

					StringBuilder sqlStatement2 = new StringBuilder(
							"select clerk, Count(clerk) as iloscSpraw "
									+ " from dso_container "
									+ " where discriminator='case' AND closed='0' AND clerk='"
									+ name + "' group by CLERK");
					String sqlStringStatement2 = sqlStatement2.toString();
					PreparedStatement ps2 = DSApi.context().prepareStatement(
							sqlStringStatement2);
					ResultSet rs2 = ps2.executeQuery();
					if (rs2.next())
						liczbaSprawDlaAktualnegoUzytkownika = rs2
								.getString("iloscSpraw");
					else
						liczbaSprawDlaAktualnegoUzytkownika = "0";

				}

				if (liczbaWszystkichSpraw != null
						&& !liczbaWszystkichSpraw.equals("")) {
					;
					StringBuilder sqlStatement2 = new StringBuilder(
							"select clerk, Count(clerk) as iloscSpraw "
									+ " from dso_container "
									+ " where discriminator='case'  AND clerk='"
									+ name + "' group by CLERK");
					String sqlStringStatement2 = sqlStatement2.toString();
					PreparedStatement ps2 = DSApi.context().prepareStatement(
							sqlStringStatement2);
					ResultSet rs2 = ps2.executeQuery();
					if (rs2.next())
						liczbaWszystkichSprawDlaAktualnegoUzytkownika = rs2
								.getString("iloscSpraw");
					else
						liczbaWszystkichSprawDlaAktualnegoUzytkownika = "0";

				}

				if (liczbaSprawNieBranaPodUwage
						&& liczbaWszystkichSprawNieBranaPodUwage) {
					// dodaje do xls gdy nie jest brany pod uwage liczba spraw
					wpisz(rowCount, lp, firstName, lastName,
							stanowiskoZajmowane, phone, komorkaZajmowana);
					rowCount++;
					lp++;
				} else {
					if (!liczbaSprawNieBranaPodUwage
							&& !liczbaWszystkichSprawNieBranaPodUwage) {
						if (liczbaSpraw
								.equals(liczbaSprawDlaAktualnegoUzytkownika)) {
							if (liczbaWszystkichSpraw
									.equals(liczbaWszystkichSprawDlaAktualnegoUzytkownika)) {
								// dodaje gdy jest brana pod uwage liczba spraw
								// i liczba wszyustkich spraw
								wpisz(rowCount, lp, firstName, lastName,
										stanowiskoZajmowane, phone,
										komorkaZajmowana);
								rowCount++;
								lp++;
							}
						}
					} else if (!liczbaSprawNieBranaPodUwage) {
						if (liczbaSpraw
								.equals(liczbaSprawDlaAktualnegoUzytkownika)) {
							// dodaje gdy jest brana pod uwage liczba spraw
							wpisz(rowCount, lp, firstName, lastName,
									stanowiskoZajmowane, phone,
									komorkaZajmowana);
							rowCount++;
							lp++;
						}
					} else {
						if (liczbaWszystkichSpraw
								.equals(liczbaWszystkichSprawDlaAktualnegoUzytkownika)) {
							// dodaje gdy jest brana pod uwage
							// liczba wszyustkich spraw
							wpisz(rowCount, lp, firstName, lastName,
									stanowiskoZajmowane, phone,
									komorkaZajmowana);
							rowCount++;
							lp++;
						}

					}
				}

			}

		} catch (SQLException e) {
			Logger.getLogger(UTPUsersReport.class.getName()).log(Level.SEVERE,
					null, e);
		} catch (EdmException e) {
			Logger.getLogger(UTPUsersReport.class.getName()).log(Level.SEVERE,
					null, e);
		}

	}

	private String znajdzZajmowaneStanowisko(String StanowiskoZajmowaneId) {
		
		if (StanowiskoZajmowaneId.equals("1")) {
			return null;
		}// doszlo do roota - brak przypisanego stanowiska
		else {

			try {

				StringBuilder sqlStatement2 = new StringBuilder(
						"select name,parent_id,DIVISIONTYPE "
								+ " from ds_division where id='"
								+ StanowiskoZajmowaneId + "' ");
				String sqlStringStatement2 = sqlStatement2.toString();
				PreparedStatement ps2 = DSApi.context().prepareStatement(
						sqlStringStatement2);

				ResultSet rs = ps2.executeQuery();
				if (rs.next()) {
					if (rs.getString("DIVISIONTYPE").equals("position"))
						return rs.getString("name");
					else
						return znajdzZajmowaneStanowisko(rs
								.getString("parent_id"));
				}
				return null;

			} catch (SQLException e) {
				Logger.getLogger(UTPUsersReport.class.getName()).log(
						Level.SEVERE, null, e);
			} catch (EdmException e) {
				Logger.getLogger(UTPUsersReport.class.getName()).log(
						Level.SEVERE, null, e);
			}

		}
		return null;

	}


	public void setZnak(String znak) {
		this.znak = znak;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public void setIdentyfikator(String identyfikator) {
		this.identyfikator = identyfikator;
	}

	public void setTytul(String tytul) {
		this.tytul = tytul;
	}

	public void setKategoria(String kategoria) {
		this.kategoria = kategoria;
	}

	public void setDataRozp(Date rozp) {
		this.rozp = rozp;
	}

	public void setDataZakn(Date zak) {
		this.zak = zak;
	}

	public void setKomorka(String komorka) {
		this.komorka = komorka;
	}

	public void setDataUtwOd(Date utwOd) {
		this.utwOd = utwOd;
	}

	public void setDataUtwDo(Date utwDo) {
		this.utwDo = utwDo;
	}

	public void setLS(String liczbaSpraw) {
		this.liczbaSpraw = liczbaSpraw;
		liczbaSprawNieBranaPodUwage = false;
	}

	public void setLWS(String liczbaWszystkichSpraw) {
		this.liczbaWszystkichSpraw = liczbaWszystkichSpraw;
		liczbaWszystkichSprawNieBranaPodUwage = false;
	}

	public void setStanowisko(String stanowisko) {
		this.stanowisko = stanowisko;
	}

	public void wpisz(int rowCount, int lp, String firstName, String lastName,
			String stanowiskoZajmowane, String phone, String komorkaZajmowana) {
		int i = -1;
		HSSFRow row = sheet.createRow(rowCount);
		row.createCell(++i).setCellValue(new HSSFRichTextString(lp + ""));

		if (firstName != null)
			row.createCell(++i).setCellValue(new HSSFRichTextString(firstName));
		else
			row.createCell(++i).setCellValue(new HSSFRichTextString("-"));

		if (lastName != null)
			row.createCell(++i).setCellValue(new HSSFRichTextString(lastName));
		else
			row.createCell(++i).setCellValue(new HSSFRichTextString("-"));

		if (komorkaZajmowana != null)
			row.createCell(++i).setCellValue(
					new HSSFRichTextString(komorkaZajmowana));
		else
			row.createCell(++i).setCellValue(new HSSFRichTextString("-"));

		if (phone != null)
			row.createCell(++i).setCellValue(new HSSFRichTextString(phone));
		else
			row.createCell(++i).setCellValue(new HSSFRichTextString("-"));

		if (stanowiskoZajmowane != null)
			row.createCell(++i).setCellValue(
					new HSSFRichTextString(stanowiskoZajmowane));
		else
			row.createCell(++i).setCellValue(new HSSFRichTextString("-"));

	}
}
