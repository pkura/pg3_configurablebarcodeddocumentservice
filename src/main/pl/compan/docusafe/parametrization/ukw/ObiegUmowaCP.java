package pl.compan.docusafe.parametrization.ukw;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.google.common.collect.Maps;

import org.springframework.stereotype.Component;


@Component("obiegUmowaCP")
public class ObiegUmowaCP { //extends AbstractObiegUkw
	private static final Logger log = LoggerFactory.getLogger(ObiegWniosekUrlopowy.class);
	private static final String ADMIN_NAME="admin";
	
	public void generujRTF(long docId) {
		
	}
	
	public void generujPDF(long docId) {
		
	}
	
	/**
	 * Ustawienie pola cn na wartosc value dla dokumentu docId
	 * @param docId
	 * @param cn
	 * @param value
	 */
	public void aktualizujPole(long docId, String cn, Object value) {
		Document document;
		try {
			document = Document.find(docId);
			FieldsManager fieldsManager = document.getFieldsManager();
			Map<String,Object> fieldValues = Maps.newHashMap();
			fieldValues.put(cn, value);
			fieldsManager.getDocumentKind().setOnly(docId, fieldValues);
		} catch (Exception e) {
			log.error("",e);
		}
	}
	
	public void przekazDaneDoERP(long docId) {
		
	}
	
	public String getDivisionGUIDFromAdds(String addsName)
	{
		try {
			return Docusafe.getAdditionProperty(addsName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ADMIN_NAME;
	}
	
	public String getCurrentUser()
	{
		try {
			return DSUser.getLoggedInUser().getName();
		} catch (Exception e) {
			log.error(e.getMessage()+"Blad proby pobrania loginu aktualnie zalogowanego usera",e);
		}
		return ADMIN_NAME;
	}
	
	public String getAutor(Long docId)
	{
		try {
			Document doc = Document.find(docId);
			return doc.getAuthor();
		} catch (Exception e) {
			log.error(e.getMessage()+"Blad proby pobrania loginu autora dokumentu",e);
		}
		return ADMIN_NAME;
	}
	
	/*
	 * Ustawienie nr KO dla dokumentu docId
	 */
	public void ustawNrKO(Long docId) {
		Document document;
		try {
			document = Document.find(docId);
			FieldsManager fieldsManager = document.getFieldsManager();
			
			if (document instanceof OutOfficeDocument) {
				OutOfficeDocument outOfficeDocument = (OutOfficeDocument) document;
				Map<String, Object> values = new HashMap<String, Object>();
				values.put("NR_KO", outOfficeDocument.getOfficeNumber());
				try {
					fieldsManager.getDocumentKind().setOnly(docId, values);
				} catch (Exception e) {
					log.error("",e);
					return;
				}
			}
		} catch (Exception e) {
			log.error("",e);
		}
	}
	
}
