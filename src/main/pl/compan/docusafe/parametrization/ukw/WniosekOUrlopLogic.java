package pl.compan.docusafe.parametrization.ukw;

import java.util.Date;
import java.util.Map;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.core.dockinds.dwr.Field;

public class WniosekOUrlopLogic extends AbstractDocumentLogic{
	
	private static WniosekOUrlopLogic instance;
	protected static Logger log = LoggerFactory.getLogger(WniosekOUrlopLogic.class);
	private static StringManager sm = GlobalPreferences.loadPropertiesFile(WniosekOUrlopLogic.class.getPackage().getName(), null);
	
	
	
	public static WniosekOUrlopLogic getInstance(){
		if(instance == null)
			instance = new WniosekOUrlopLogic();
		return instance;
	}
	
	@Override
	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk造 zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk造 - usuwanie"));
		
		for (PermissionBean perm : perms) {
			if (perm.isCreateGroup()
					&& perm.getSubjectType().equalsIgnoreCase(
							ObjectPermission.GROUP)) {
				String groupName = perm.getGroupName();
				if (groupName == null) {
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
		
	}

	@Override
	public void archiveActions(Document document, int type, ArchiveSupport as)
			throws EdmException {
	}
	
	@Override
	public void validate(Map<String, Object> values, DocumentKind kind, Long documentId)
			throws EdmException {
		if(values.get("TYPWNIOSKU").equals(2)){
			int liczbaDni = (Integer) values.get("WYMIARDNI");
			if(liczbaDni > 4){
				throw new EdmException(sm.getString("blednaLiczbaUrlopu"));
			}
		}
		Date dataOd = (Date) values.get("DATAURLOPUOD");
		Date dataDo = (Date) values.get("DATAURLOPUDO");
		Date dataPrzeOd = (Date) values.get("DATAURLOPUPRZELOZONYOD");
		Date dataPrzeDo = (Date) values.get("DATAURLOPUPRZELOZONYDO");
		if(dataDo.before(dataOd)){
			throw new EdmException(sm.getString("blednaData"));
		} else if(dataPrzeDo !=null && dataPrzeOd != null){
			if(dataPrzeDo.before(dataPrzeOd))
				throw new EdmException(sm.getString("blednaData"));
		}
	}
	
	@Override
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm)
			throws EdmException {
		if(values.get("DWR_STATUS") != null){
			if(values.get("DWR_STATUS").getEnumValuesData().getSelectedId().equals("1")){
				if(values.get("DWR_TYPWNIOSKU") != null){
					if(values.get("DWR_TYPWNIOSKU").getEnumValuesData().getSelectedId().equals("2")){
						values.put("DWR_DOSTEPNOSCDNI", new FieldData(Field.Type.INTEGER, 4));
					} else {
						values.put("DWR_DOSTEPNOSCDNI", new FieldData(Field.Type.INTEGER, 26));
					}
				}
			}
		}
		return super.validateDwr(values, fm);
	}
	
}
