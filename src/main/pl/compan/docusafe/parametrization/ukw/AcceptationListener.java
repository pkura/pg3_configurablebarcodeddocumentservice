package pl.compan.docusafe.parametrization.ukw;

import java.util.Date;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.activiti.listeners.AcceptancesListener;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.utp.ZamowieniePubliczneLogic;


public class AcceptationListener extends AcceptancesListener {
	
	private static final long serialVersionUID = 1L;

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		
		Long docId = (Long)execution.getVariable("docId");
		addAcceptation(docId, execution);
		
		OfficeDocument doc = OfficeDocument.find(docId);
		
//		String currentAssignee = (String)execution.getVariable("assignee");
		String currentAssignee = DSApi.context().getDSUser().getName();
		DSUser user = DSUser.findByUsername(currentAssignee);
		int status = getStatus(execution);
		
		if (user!=null)
//			ZamowieniePubliczneLogic.fillAcceptationTable(doc, dictionaryCn, user,new Date(),acceptationStatus,uwagi, updateExistingAcceptation==1 ? true:false);
			ZamowieniePubliczneLogic.fillAcceptationTable(doc, "AKCEPTACJE", user, new Date(), status, "", false);
	}
	
	static int getStatus(DelegateExecution execution){
		String actionVar = (String)execution.getVariable("action");
		return (actionVar.equalsIgnoreCase("accepted"))?1:0;
	}
	
}
