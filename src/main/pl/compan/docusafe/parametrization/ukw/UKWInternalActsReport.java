package pl.compan.docusafe.parametrization.ukw;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.TreeMap;

import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportException;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;

public class UKWInternalActsReport extends Report{

	private String rok;
	private String numerKO;
	private String documentId;
	private String zapoznalSie;
	private String numerAktPraw;
	
	
	private UniversalTableDumper dumper;
	
	public void initParametersAvailableValues() throws ReportException {
		super.initParametersAvailableValues();
		try {

			for (ReportParameter rp : params) {
				if (rp.getFieldCn().equals("zapoznalSie")) {
					Map<String, String> tmp = new TreeMap<String, String>();
					tmp.put("tak", sm.getString("tak"));
					tmp.put("nie", sm.getString("nie"));
					rp.setAvailableValues(tmp);
				}
			}
		} catch (Exception e) {
			throw new ReportException(e);
		}
	}
	
	
	@Override
	public void doReport() throws Exception {
		for (ReportParameter rp : params) {
			if (rp.getFieldCn().equals("rok")) {
				rok=rp.getValueAsString();
			}
			else if(rp.getFieldCn().equals("numerKO")){
				numerKO=rp.getValueAsString();
			}
			else if(rp.getFieldCn().equals("documentId")){
				documentId=rp.getValueAsString();
			}
			else if(rp.getFieldCn().equals("zapoznalSie")){
				zapoznalSie=rp.getValueAsString();
			}
			else if(rp.getFieldCn().equals("numerAktPraw")){
				numerAktPraw=rp.getValueAsString();
			}
		}
		
		
		dumper = new UniversalTableDumper();
		XlsPoiDumper poiDumper = new XlsPoiDumper();
		File xlsFile = new File(this.getDestination(), "raport.xls");
		poiDumper.openFile(xlsFile);
		FileOutputStream fis = new FileOutputStream(xlsFile);

		doReportForAll().getWorkbook().write(fis);
		fis.close();
		dumper.addDumper(poiDumper);
		
	}


	private UKWInternalActsReportXLS doReportForAll() {
		UKWInternalActsReportXLS xlsReport = new UKWInternalActsReportXLS("Raport rozporządzeń do użytkowników");
		
		if(rok!=null&&!rok.equals("")){
			xlsReport.setRok(rok);
		}
		if(numerKO!=null&&!numerKO.equals("")){
			xlsReport.setNumerKO(numerKO);
		}
		if(documentId!=null&&!documentId.equals("")){
			xlsReport.setDocumentId(documentId);
		}
		if (zapoznalSie!=null&&!zapoznalSie.equals("")){
			xlsReport.setZapoznalSie(null);
			if(zapoznalSie.equals(sm.getString("tak")))xlsReport.setZapoznalSie(true);
			if(zapoznalSie.equals(sm.getString("nie")))xlsReport.setZapoznalSie(false);
		}
		if(numerAktPraw!=null&&!numerAktPraw.equals("")){
			xlsReport.setNumerAktPraw(numerAktPraw);
		}
		
		
		xlsReport.generate();
		return xlsReport;
	}


	public String getRok() {
		return rok;
	}


	public void setRok(String rok) {
		this.rok = rok;
	}


	public String getNumerKO() {
		return numerKO;
	}


	public void setNumerKO(String numerKO) {
		this.numerKO = numerKO;
	}

	public String getZapoznalSie() {
		return zapoznalSie;
	}


	public void setZapoznalSie(String zapoznalSie) {
		this.zapoznalSie = zapoznalSie;
	}


	public String getNumerAktPraw() {
		return numerAktPraw;
	}


	public void setNumerAktPraw(String numerAktPraw) {
		this.numerAktPraw = numerAktPraw;
	}


	public String getDocumentId() {
		return documentId;
	}


	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

}
