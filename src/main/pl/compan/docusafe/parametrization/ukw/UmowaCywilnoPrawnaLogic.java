package pl.compan.docusafe.parametrization.ukw;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.google.common.collect.Maps;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;

import java.util.Date;

import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.DwrUtils;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Logika dla dokumentu typu wniosek_o_udzielenie_pozyczki.
 * Przy korzystaniu z proces�w w ACTIVITI nie mo�na prze�adowywa� 
 * metody onStartProcess w klasie logiki procesu (powoduje to podw�jne uruchomienie procesu i/lub b��dy).
 * Metoda onStartProcess jest uruchomiona z klasy AbstractDocumentLogic (klasy po kt�rej dziedziczy logika).
 *
 */
public class UmowaCywilnoPrawnaLogic extends AbstractDocumentLogic {

	
	private static final long serialVersionUID = 1L;
	private static UmowaCywilnoPrawnaLogic instance;
	
	public final String OLD_DATE_FORMAT = "EEE MMM dd kk:mm:ss z yyyy";
	public final String NEW_DATE_FORMAT = "dd-MM-yyyy";
	
	private static final String ADMIN_NAME="admin";
	
	protected static Logger log = LoggerFactory.getLogger(UmowaCywilnoPrawnaLogic.class);

	public static final EnumValues EMPTY_ENUM_VALUE = new EnumValues(new HashMap<String,String>(), new ArrayList<String>());
	
	public static UmowaCywilnoPrawnaLogic getInstance() {
		if (instance == null)
			instance = new UmowaCywilnoPrawnaLogic();
		return instance;
	}
	
	
	@Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
        document.setFolder(folder);

        document.setTitle("Title id: "+document.getId());
    }
	
	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
		/* 
		 * Wnioskodawca	- pole listy bez mo�liwo�ci wyboru.
		 * System automatycznie ustawia warto�� tego pola na zalogowanego u�ytkownika.
		 */
		Map<String, Object> values = Maps.newHashMap();
		values.put("SPORZADZIL", DSUser.getLoggedInUser().getId());
		values.put("STATUS", 0);
		values.put("DATA_SPORZADZENIA", Calendar.getInstance().getTime());
		
		DSDivision[] jednostka = DSUser.getLoggedInUser().getDivisions();
		if (jednostka.length > 0)
			values.put("JEDN_SPORZADZAJACA", jednostka[0].getId());
		
		fm.reloadValues(values);
		//fm.getField("WNIOSKODAWCA").setReadOnly(true);
	}
	
	
	@Override
	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk�y- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk�y - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk�y - usuwanie"));
		
		for (PermissionBean perm : perms) {
			if (perm.isCreateGroup()
					&& perm.getSubjectType().equalsIgnoreCase(
							ObjectPermission.GROUP)) {
				String groupName = perm.getGroupName();
				if (groupName == null) {
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
		
	}
	
	/* (non-Javadoc)
	 * @see pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic#onLoadData(pl.compan.docusafe.core.dockinds.FieldsManager)
	 * Metoda zastosowana tutaj w celu uzupe�nienia pola "NUMER_SPRAWY" na dockindzie. Pocz�tkowo dokument nie ma warto�ci w tym polu,
	 * ale zostanie ona dodana po przypisaniu dokumentu do jakiej� sprawy. Pole "NUMER_SPRAWY" jest readonly.
	 */
	@Override
	public void onLoadData(FieldsManager fm) throws EdmException {

		/*
		 * Po przekazaniu dokumentu do Dzia�u P�ac, pola z danymi nie powinny by� edytowalne (edytowalne jedynie przez administratora)
		 * 
		 */
		String userName = "";
		try {
			userName =  DSUser.getLoggedInUser().getName();
		} catch (Exception e) {
			log.error(e.getMessage()+"Blad proby pobrania loginu aktualnie zalogowanego usera",e);
		}
		if (userName.equals(ADMIN_NAME)) {
			fm.getField("RODZAJ_UMOWY").setReadOnly(false);
			fm.getField("JEDNOSTKA_ZAWIERAJACA_UMOWE").setReadOnly(false);
			fm.getField("NR_UMOWY").setReadOnly(false);
			fm.getField("DATA_ZAWARCIA").setReadOnly(false);
			fm.getField("DATA_POCZATKOWA").setReadOnly(false);
			fm.getField("DATA_KONCOWA").setReadOnly(false);
			fm.getField("PESEL").setReadOnly(false);
			fm.getField("NR_EWID_ERP").setReadOnly(false);
			fm.getField("ZLECENIOBIORCA").setReadOnly(false);
			fm.getField("NAZWISKO").setReadOnly(false);
			fm.getField("IMIE").setReadOnly(false);
			fm.getField("IM_NAZ_NARZEDNIK").setReadOnly(false);
			fm.getField("DATA_UR").setReadOnly(false);
			fm.getField("MIEJSCE_UR").setReadOnly(false);
			fm.getField("DRUGIE_IMIE").setReadOnly(false);
			fm.getField("NAZWISKO_RODOWE").setReadOnly(false);
			fm.getField("IMIE_OJCA").setReadOnly(false);
			fm.getField("IMIE_MATKI").setReadOnly(false);
			fm.getField("OBYWATEL_PL").setReadOnly(false);
			fm.getField("OBYWATELSTWO_INNE").setReadOnly(false);
			fm.getField("SERIA_NR_DOWODU").setReadOnly(false);
			fm.getField("SERIA_NR_DOK_OBCO").setReadOnly(false);
			fm.getField("RODZAJ_ADRESU_PIT").setReadOnly(false);
			fm.getField("ADRES_ZAMIESZKANIA").setReadOnly(false);
			fm.getField("MIE_KOD_POCZTOWY").setReadOnly(false);
			fm.getField("MIE_ULICA").setReadOnly(false);
			fm.getField("MIE_NR_DOMU").setReadOnly(false);
			fm.getField("MIE_NR_MIESZKANIA").setReadOnly(false);
			fm.getField("MIE_MIEJSCOWOSC").setReadOnly(false);
			fm.getField("MIE_WOJEWODZTWO").setReadOnly(false);
			fm.getField("ADRES_ZAMELDOWANIA").setReadOnly(false);
			fm.getField("MEL_KOD_POCZTOWY").setReadOnly(false);
			fm.getField("MEL_ULICA").setReadOnly(false);
			fm.getField("MEL_NR_DOMU").setReadOnly(false);
			fm.getField("MEL_NR_MIESZKANIA").setReadOnly(false);
			fm.getField("MEL_MIEJSCOWOSC").setReadOnly(false);
			fm.getField("MEL_WOJEWODZTWO").setReadOnly(false);
			fm.getField("URZAD_SKARBOWY").setReadOnly(false);
			fm.getField("TEL_KONTAKTOWY").setReadOnly(false);
			fm.getField("CZY_PROWADZI_DG").setReadOnly(false);
			fm.getField("NIP").setReadOnly(false);
			fm.getField("CZY_DG_W_UMOWIE").setReadOnly(false);
			fm.getField("STATUS_REZYDENTA").setReadOnly(false);
			fm.getField("US_REZYDENT").setReadOnly(false);
			fm.getField("NR_ZASW_REZYDENT").setReadOnly(false);
			fm.getField("UM_O_PRAC").setReadOnly(false);
			fm.getField("UBEZP_EMERYT_RENT").setReadOnly(false);
			fm.getField("UM_ZLEC").setReadOnly(false);
			fm.getField("UM_ZLEC_POCZATEK").setReadOnly(false);
			fm.getField("UM_ZLEC_KONIEC").setReadOnly(false);
			fm.getField("MIN_PENSJA").setReadOnly(false);
			fm.getField("WNIOSEK_O_UBEZP_CHOROB").setReadOnly(false);
			fm.getField("CZY_NA_URLOPIE").setReadOnly(false);
			fm.getField("PRAWO_DO_EMERYT_RENT").setReadOnly(false);
			fm.getField("NR_DECYZJI_EMERYT_RENT").setReadOnly(false);
			fm.getField("OS_NIEPELNOSPRAWNA").setReadOnly(false);
			fm.getField("STUDENT").setReadOnly(false);
			fm.getField("ZLECENIODAWCA_1").setReadOnly(false);
			fm.getField("ZLECENIODAWCA_2").setReadOnly(false);
			fm.getField("ROK_AKADEMICKI").setReadOnly(false);
			fm.getField("GODZ_DYDAKT").setReadOnly(false);
			fm.getField("STAWKA_BRUTTO").setReadOnly(false);
			fm.getField("WARTOSC_BRUTTO").setReadOnly(false);
			fm.getField("WARTOSC_SLOWNIE").setReadOnly(false);
			fm.getField("OPIS_UMOWY").setReadOnly(false);
			fm.getField("SPORZADZIL").setReadOnly(false);
			fm.getField("JEDN_SPORZADZAJACA").setReadOnly(false);
			fm.getField("DATA_SPORZADZENIA").setReadOnly(false);
			fm.getField("NR_KO").setReadOnly(false);
		}
	}

	/**
	 * Ustawienie numeru KO dla dokumentu o danym docId
	 * @param docId
	 */
	
	public void ustawNrKO(long docId) {
		Document document;
		try {
			document = Document.find(docId);
			if (document == null)
				return;
		} catch (Exception e) {
			log.error("",e);
			return;
		}
		Long nrKO = null;
		try {
			PreparedStatement ps = null;
//			ps = DSApi.context().prepare
			//ps = DSApi.context().prepareStatement("SELECT OFFICENUMBER FROM DSO_OUT_DOCUMENT with(nolock) where ID = ?", docId);
			
			StringBuilder sbQuery = new StringBuilder("select OFFICENUMBER from DSO_OUT_DOCUMENT with(nolock) where ID = ?");
        	ps = DSApi.context().prepareStatement(sbQuery.toString());
			ps.setLong(1, docId);			
			ResultSet rs = ps.executeQuery();
			if (rs != null) {
				rs.next();
				if (rs.getLong("OFFICENUMBER") != 0 )
					nrKO = rs.getLong("OFFICENUMBER");
			}
		} catch (SQLException e) {
			log.error("",e);
		} catch (EdmException e) {
			log.error("",e);
		}  catch (Exception e) {
			log.error("",e);
		}
		FieldsManager fieldsManager = document.getFieldsManager();
		Map<String,Object> fieldValues = Maps.newHashMap();
		fieldValues.put("NR_KO", nrKO);
		try {
			fieldsManager.getDocumentKind().setOnly(docId, fieldValues);
		} catch (EdmException e) {
			log.error("",e);
			return;
		}
	}
	
	@Override
	public Map<String, Object> setMultipledictionaryValue(String dictionaryName, Map<String, FieldData> values) {
		Map<String, Object> results = new HashMap<String, Object>();
		
		DwrUtils.setRefValueEnumOptions(values, results,"CENTRUMID", dictionaryName+"_", "ACCOUNTNUMBER", "dsg_invoice_general_budzet_poz", null, EMPTY_ENUM_VALUE);
		DwrUtils.setRefValueEnumOptions(values, results,"ACCOUNTNUMBER", dictionaryName+"_", "ACCEPTINGCENTRUMID", "simple_erp_per_docusafe_zrodla", null, EMPTY_ENUM_VALUE);
		
		return results;
	}
	
	public Field validateDwr(Map<String, FieldData> values, FieldsManager fm) throws EdmException
	{
		pl.compan.docusafe.core.dockinds.dwr.Field msgField = super.validateDwr(values, fm);
		
		StringBuilder msgBuilder = new StringBuilder();
		
		if (msgField != null)
			msgBuilder.append(msgField.getLabel());

		// sprawdzi� czy data zawarcia nie jest p�niejsza ni� data realizacji
		validateDate(values, msgBuilder);
		
		if (msgBuilder.length() > 0)
		{
			values.put("messageField", new FieldData(pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN, true));
			return new pl.compan.docusafe.core.dockinds.dwr.Field("messageField", msgBuilder.toString(), pl.compan.docusafe.core.dockinds.dwr.Field.Type.BOOLEAN);
		}
		
		/**
		 * TODO obliczanie wartosci brutto
		 */
		/*
		if (values.get("DWR_GODZ_DYDAKT") != null)
			if (values.get("DWR_STAWKA_BRUTTO") != null) {
				double godzin = Double.parseDouble(values.get("DWR_GODZ_DYDAKT").getStringData());
				double stawka = Double.parseDouble(values.get("DWR_STAWKA_BRUTTO").getStringData());
				values.put("DWR_WARTOSC_BRUTTO", new FieldData(Type.STRING, BigDecimal.valueOf(godzin*stawka).setScale(2, BigDecimal.ROUND_HALF_UP)));
		}
		*/
		
		return null;
	}
	
	private void validateDate(Map<String, FieldData> values, StringBuilder msgBuilder)
	{
		
		Date start = null, finish = null, fill = null;
		
		if (values.get("DWR_DATA_ZAWARCIA") != null)
		{
			fill = values.get("DWR_DATA_ZAWARCIA").getDateData();
		}
			
		if (values.get("DWR_DATA_POCZATKOWA") != null && values.get("DWR_DATA_KONCOWA")!= null)
		{
			start = values.get("DWR_DATA_POCZATKOWA").getDateData();
			finish = values.get("DWR_DATA_KONCOWA").getDateData();
		}
		
		
		if (start != null && finish != null && start.after(finish))
		{
			if (msgBuilder.length() == 0)
				msgBuilder.append("B��d: Data zako�czenia nie mo�e by� wcze�niejsza ni� data rozpocz�cia.");
			else
				msgBuilder.append(";  Data zako�czenia nie mo�e by� wcze�niejsza ni� data rozpocz�cia.");
		}
		if (fill != null && start != null && fill.after(start))
		{
			if (msgBuilder.length() == 0)
				msgBuilder.append("B��d: Data rozpocz�cia nie mo�e by� wcze�niejsza ni� data wype�nienia.");
			else
				msgBuilder.append(";  Data rozpocz�cia nie mo�e by� wcze�niejsza ni� data wype�nienia.");
		}
	}


	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values, Map<Class, Format> defaultFormatters) {
		try{
			Document document =  Document.find(docId);
	        FieldsManager fm = document.getFieldsManager();
	        Map<String, Object> dockindFieldMap=fm.getFieldValues();

	        SimpleDateFormat oldSdf = new SimpleDateFormat(OLD_DATE_FORMAT,Locale.ENGLISH);
	        SimpleDateFormat newSdf = new SimpleDateFormat(NEW_DATE_FORMAT);
	        
	        values.put("DOCUMENT_ID", Long.toString(docId));
	        String key;
	        Object value;
	        for (Map.Entry<String, Object> entry : dockindFieldMap.entrySet()) {
	            key = entry.getKey();
	            value = entry.getValue();
	            
	            if(key.equals("SPORZADZIL"))
	            {
	            	long val = Long.valueOf(value.toString()).longValue();
	            	values.put("SPORZADZIL_WARTOSC", DSUser.findById(val).getFirstname() + " " + DSUser.findById(val).getLastname());
	    	        continue;
	            }
	            else if(key.equals("JEDN_SPORZADZAJACA"))
	            {
	            	long val = Long.valueOf(value.toString()).longValue();
	            	values.put("JEDN_SPORZADZAJACA_WARTOSC", DSDivision.find((int) val).getName());
	            	continue;
	            }
	            else if(key.equals("DATA_ZAWARCIA") || key.equals("DATA_POCZATKOWA") || key.equals("DATA_KONCOWA") || key.equals("DATA_UR") || key.equals("DATA_SPORZADZENIA") || key.equals("UM_ZLEC_POCZATEK") || key.equals("UM_ZLEC_KONIEC"))
	            {
	            	if(value == null || value.toString() == "")
	            		values.put(key+"_WARTOSC","");
	            	else
	            	{
	            		Date result =  oldSdf.parse(value.toString());
	            		values.put(key+"_WARTOSC", newSdf.format(result));
	            	}
	            	continue;
	            }
	            else if(key.equals("OBYWATEL_PL"))
	            {
	            	if(value != null && value.toString().equals("1")) 
	            	{
	            		values.put("OBYWATEL_PL_WARTOSC","polskie");
	            	}
	            	else
	            	{
	            		values.put("OBYWATEL_PL_WARTOSC","-");
	            	}
	            	continue;
	            }
	            else if(key.equals("STATUS_REZYDENTA") || key.equals("CZY_PROWADZI_DG") || key.equals("CZY_DG_W_UMOWIE") || key.equals("UM_O_PRAC") || key.equals("UBEZP_EMERYT_RENT") || key.equals("UM_ZLEC") || key.equals("MIN_PENSJA") || key.equals("WNIOSEK_O_UBEZP_CHOROB") || key.equals("CZY_NA_URLOPIE") || key.equals("CZY_NA_URLOPIE") || key.equals("PRAWO_DO_EMERYT_RENT") || key.equals("OS_NIEPELNOSPRAWNA") || key.equals("STUDENT") || key.equals("RODZAJ_ADRESU_PIT"))
	            {
	            	values.put(key+"_WARTOSC", fm.getValue(key));
	            	continue;
	            }
	            
	            values.put(key+"_WARTOSC", value==null ? " " : value.toString());
	        }
        
		} catch (DocumentNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentLockedException e) {
			e.printStackTrace();
		} catch (AccessDeniedException e) {
			e.printStackTrace();
		} catch (EdmException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}