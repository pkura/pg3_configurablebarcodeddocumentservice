package pl.compan.docusafe.parametrization.ukw;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.stereotype.Component;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.activiti.NotFoundSolution;
import pl.compan.docusafe.core.activiti.helpers.UserAssignmentHelper;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.exports.ExportHandler;
import pl.compan.docusafe.core.exports.RequestResult;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.general.canonical.dao.FakturaDAO;
import pl.compan.docusafe.general.canonical.model.Faktura;
import pl.compan.docusafe.general.hibernate.dao.FakturaDBDAO;
import pl.compan.docusafe.general.hibernate.dao.FakturaExportStatusDBDAO;
import pl.compan.docusafe.general.mapers.CanonicalToWSMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.parametrization.invoice.InvoiceGeneralConstans;
import pl.compan.docusafe.parametrization.utp.export.SimpleErpExportFactory;
import pl.compan.docusafe.parametrization.utp.export.SimpleWSChecker;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.office.common.TemplatesTabAction;
import pl.compan.docusafe.ws.client.simple.general.ExportCostInvoiceFactory;
import pl.compan.docusafe.ws.client.simple.general.RequestServiceFactory;
import pl.compan.docusafe.ws.client.simple.general.RequestServiceStub;
import pl.compan.docusafe.ws.simple.general.dokzak.DokzakTYPE;

import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.*;


@Component("obiegFaktura")
public class ObiegFaktura
{
	private static final Logger LOG = LoggerFactory.getLogger(ObiegFaktura.class);

    private static final String EXPORT_STATUS_PROCESSING = "processing";
    private static final String EXPORT_STATUS_ACCEPTED = "accepted";
    private static final String EXPORT_STATUS_REJECTED = "invalid";

    //ZGL_BLEDU = ZGLOSZENIE_BLEDU, Formularz do przesyłania info o błędzie do admina (usługą lub przez człowieka)  ----------
    /** zgłoszenie błędu: ID statusu błędu nowego */				String ZGL_BLEDU_nowy = "11";
    /** zgłoszenie błędu: ID błędu będącego w obsłudze */ 			String ZGL_BLEDU_obslugiwany = "12";
    /** zgłoszenie błędu: ID błędu rozwiązanego */ 					String ZGL_BLEDU_rozwiazany = "13";
    /** zgłoszenie błędu: ID błędu odrzuconego */ 					String ZGL_BLEDU_odrzucony = "14";
    /** zgłoszenie błędu: ID błędu odsuniętego w czasie */ 			String ZGL_BLEDU_odsuniety= "15";

    /** Komunikat gotowy do pobrania przez szynę. */				String ERP_PRZESLANO_NA_SZYNE = "NEW";
    /** Komunikat został pobrany przez task */						String ESB_POBRANY_PRZEZ_SZYNE = "UPLOADED";
    /** Komunikat jest przetwarzany przez szynę danych */			String ESB_PRZETWARZANY_PRZEZ_SZYNE = "PROCESSING";
    /** Komunikat został poprawnie dodany do Simple.ERP */			String ESB_DODANO_DO_ERP = "FINISHED";
    /** Komunikat nie przeszedł wstępnej walidacji XSD */			String ESB_BLAD_WALIDACJI_PLIKU_XML = "INVALID";
    /** Komunikat nie został dodany do Simple.ERP system
     * zgłosił kod błędu */ 										String ESB_BLAD_WEWNETRZNY_ERP = "FAULT";

    public String getCandidateUsersFromAcceptationCn(Long docId, String acceptationCn) throws EdmException {
        return getCandidateUsersFromAcceptationCn(docId, acceptationCn, NotFoundSolution.EXCEPTION.name());
    }

    public String getCandidateUsersFromAcceptationCn(Long docId, String acceptationCn, String solution) throws EdmException {
        String candidates = null;
        OfficeDocument doc = OfficeDocument.find(docId);
        FieldsManager fm = doc.getFieldsManager();
        if (acceptationCn.equalsIgnoreCase("wskazanieDzialuRealizujacego")){
            if (czyDzialZaopatrzenia(doc))
                candidates = doc.getAuthor();
            else{
                String guid = Docusafe.getAdditionProperty("ukw.dzialFinansowy.guid");
                DSDivision div = DSDivision.find(guid);
                Collection<DSUser> users = Arrays.asList(div.getUsers());
                candidates = UserAssignmentHelper.usersFromDSUserCollection(users,solution);
            }
        }else if (acceptationCn.equalsIgnoreCase("kierownikDzialuRealizujacegoFakture")){
            DSDivision div = DSDivision.find(fm.getEnumItemCn("DZIAL_REALIZUJACY"));
            for (DSUser user : div.getUsers())
            {
                DSDivision[] usersDivs = user.getDivisions();
                for (DSDivision division : usersDivs){
                    if (division.getName().equalsIgnoreCase("Kierownicy")){
                        return user.getName();
                    }
                }

            }
//            candidates = UserAssignmentHelper.usersFromDSUserCollection(users,solution);
        }else if (acceptationCn.equalsIgnoreCase("osobaRealizujacaFakture")){
            candidates = fm.getEnumItemCn("OSOBA_REALIZUJACA");
        }else if (acceptationCn.equalsIgnoreCase("akceptacjaMerytoryczna")){
            candidates = fm.getEnumItemCn("PELNOMOCNIK");
        }

        return NotFoundSolution.checkCandidates(candidates, solution);
    }

    private boolean czyDzialZaopatrzenia(OfficeDocument doc) throws EdmException {
        String guid = Docusafe.getAdditionProperty("ukw.dzialZaopatrzenia.guid");
        if (guid == null)
            throw new EdmException("brak wskazanego dzia�u zaopatrzenia w pliku adds.properties w katalogu HOME.");
        DSUser author = DSUser.findByUsername(doc.getAuthor());

        for (DSDivision div : author.getDivisions()){
            if (div.getGuid().equalsIgnoreCase(guid))
                return true;
        }

        return false;
    }

    public String czyDzialZaopatrzenia(Long docId) throws EdmException {
        Document doc = Document.find(docId);
        String guid = Docusafe.getAdditionProperty("ukw.dzialZaopatrzenia.guid");
        if (guid == null)
            throw new EdmException("brak wskazanego dzia�u zaopatrzenia w pliku adds.properties w katalogu HOME.");
        DSUser author = DSUser.findByUsername(doc.getAuthor());

        for (DSDivision div : author.getDivisions()){
            if (div.getGuid().equalsIgnoreCase(guid))
                return "true";
        }

        return "false";
    }

    public void setDocumentStatus(Long docId, String fieldCn, Integer fieldValue) {
        LOG.info("setDocumentStatus {} {} {}",docId, fieldCn, fieldValue);
        try {
            Document doc = Document.find(docId);
            Map<String,Object> m = new HashMap<String, Object>();
            m.put(fieldCn,fieldValue);
            doc.getDocumentKind().setOnly(docId,m,false);
        } catch(Exception e) {
            LOG.debug(e.getMessage(),e);
        }
    }

    public void setDocumentStatusByCn(Long docId, String fieldCn, String statusCn) {
        LOG.info("setDocumentStatusByCn {} {} {}",docId, fieldCn, statusCn);
        try {
            Document doc = Document.find(docId);
            int i = doc.getDocumentKind().getFieldByCn(fieldCn).getEnumItemByCn(statusCn).getId();
            setDocumentStatus(docId,fieldCn,i);
        } catch (Exception e) {
            LOG.debug(e.getMessage(),e);
        }
    }

    public String export(Long docId) throws Exception {
        Faktura faktura = HibernateToCanonicalMapper.MapFakturaDBToFaktura(FakturaDBDAO.findModelById(docId), docId);
        DokzakTYPE dokzak = CanonicalToWSMapper.MapFakturaToDokzakTYPE(faktura);

        String guid = null;
        guid = new ExportCostInvoiceFactory().sendInvoice(dokzak);

        if(guid == null)
            throw new Exception("Blad wyslania faktury: guid==null");

        System.out.println(new Date()+"Export faktury GUID: " + guid);
        return guid;
    }

    public String checkStatus(String guid,Long docId) throws Exception {
        Thread.sleep(DateUtils.SECOND*5);
        String result = EXPORT_STATUS_PROCESSING;
        ExportHandler handler = new ExportHandler(new SimpleErpExportFactory());
        RequestResult requestResult = handler.checkExport(guid);

        int state = requestResult.getStateId();

        String stateName = requestResult.getStateName();

        String requestResultXml = requestResult.getResult();

        if (ESB_BLAD_WEWNETRZNY_ERP.equals(stateName) || ESB_BLAD_WALIDACJI_PLIKU_XML.equals(stateName)) {
            LOG.error("B��d eksportu, guid: " + guid);
            LOG.error("B��d eksportu, docId: " + docId);
            LOG.error("B��d eksportu, state: " + state);
            LOG.error("B��d eksportu, stateName: " + stateName);
            LOG.error("B��d eksportu, request result: " + requestResultXml);
//            result = EXPORT_STATUS_REJECTED;
            result = EXPORT_STATUS_ACCEPTED;

            setResults("B��d eksportu!\n"+ buildExportRemarks(guid, stateName, requestResultXml), docId);
        } else if (ESB_DODANO_DO_ERP.equals(stateName)) {
            result = EXPORT_STATUS_ACCEPTED;

            setResults("Eksport zako�czony pomy�lnie\n"+ buildExportRemarks(guid, stateName, requestResultXml),
                    docId);
        }
        System.out.println(new Date()+": Sprawdzenie statusu faktury("+guid+"): " + result);
        return result ;
    }

    protected void setResults(String result, Long docId) throws EdmException {
        OfficeDocument doc = OfficeDocument.findOfficeDocument(docId);
        doc.addRemark(new Remark(result, "admin"));
    }

    private String buildExportRemarks(String guid, String stateName, String requestResult) {
        StringBuilder sb = new StringBuilder();

        try {
            org.dom4j.Document requestResultXml = DocumentHelper.parseText(requestResult);
            Element root = requestResultXml.getRootElement();
            if (root != null) {
                if (root.element("ID") != null && !Strings.isNullOrEmpty(root.element("ID").getText())) {
                    sb.append("ID dokumentu ERP: ").append(root.element("ID").getText()).append("\n");
                }
                if (root.element("IDM") != null && !Strings.isNullOrEmpty(root.element("IDM").getText())) {
                    sb.append("IDM dokumentu ERP: ").append(root.element("IDM").getText()).append("\n");
                }
                if (root.element("StatusMessage") != null && !Strings.isNullOrEmpty(root.element("StatusMessage").getText())) {
                    sb.append("Status eksportu: ").append(root.element("StatusMessage").getText()).append("\n");
                }
            }
        } catch (Exception e) {
            LOG.error("CAUGHT: "+e.getMessage(),e);
        }

        sb.append("\n").append("Guid dokumentu: ").append(guid).append("\n");
        sb.append("Nazwa stanu: ").append(stateName).append("\n");
        return sb.toString();
    }
}
