package pl.compan.docusafe.parametrization.ukw;


import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Maps;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.PermissionBean;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.base.Folder;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.ArchiveSupport;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Logika dla dokumentu typu wniosek_o_udzielenie_pozyczki.
 * Przy korzystaniu z proces�w w ACTIVITI nie mo�na prze�adowywa� 
 * metody onStartProcess w klasie logiki procesu (powoduje to podw�jne uruchomienie procesu i/lub b��dy).
 * Metoda onStartProcess jest uruchomiona z klasy AbstractDocumentLogic (klasy po kt�rej dziedziczy logika).
 *
 */
public class WniosekOPozyczkeLogic extends AbstractDocumentLogic{

	
	private static final long serialVersionUID = 1L;
	private static WniosekOPozyczkeLogic instance;
	public final String OLD_DATE_FORMAT = "EEE MMM dd kk:mm:ss z yyyy";
	public final String NEW_DATE_FORMAT = "dd/MM/yyyy";
	private final String KWOTA_PRZYZNANA = "KWOTA_PRZYZNANA";
	private final String KWOTA_WNIOSKOWANA = "KWOTA_WNIOSKOWANA";
	private final static Logger LOG = LoggerFactory.getLogger(WniosekOPozyczkeLogic.class);
	
	public static WniosekOPozyczkeLogic getInstance() {
		if (instance == null)
			instance = new WniosekOPozyczkeLogic();
		return instance;
	}
	
	
	@Override
	public void setAdditionalTemplateValues(long docId, Map<String, Object> values) {
		try{
			Document document =  Document.find(docId);
	        FieldsManager fm = document.getFieldsManager();
	        Map<String, Object> dockindFieldMap=fm.getFieldValues();
	        SimpleDateFormat oldSdf = new SimpleDateFormat(OLD_DATE_FORMAT, Locale.ENGLISH);
	        SimpleDateFormat newSdf = new SimpleDateFormat(NEW_DATE_FORMAT);
	        
	        values.put("DOCUMENT_ID", Long.toString(docId));
	        
	        for (Map.Entry<String, Object> entry : dockindFieldMap.entrySet()) {
	            String key = entry.getKey();
	            Object value = entry.getValue();
	            if(key.equals("SENDER_HERE"))
	            {
	            	if(value!= null & value.toString().trim()!="")
		            {
		            	long val = Long.valueOf(value.toString()).longValue();
		            	values.put("FIRSTNAME_WARTOSC",Person.find(val).getFirstname());
		    	        values.put("LASTNAME_WARTOSC",Person.find(val).getLastname());
		    	        values.put("DIVISION_WARTOSC",Person.find(val).getOrganizationDivision());
		    	        continue;
	            	}
	            }
	            else if(key.equals("DATA_POTWIERDZENIA") || key.equals("BYDGOSZCZ_DNIA") || key.equals("DATA_KWALIFIKACJI"))
	            {
	            	
	            	if(value == null || value.toString() == "")
	            		values.put(key+"_WARTOSC","");
	            	else
	            	{
	            		Date result =  oldSdf.parse(value.toString());
	            		values.put(key+"_WARTOSC", newSdf.format(result));
	            	}
	            	
	            	continue;
	            }
	            else if(key.equals("CEL_POZYCZKI") || key.equals("PRZEKAZANIE_NA") || key.equals("WYZ_WYMIENIONY_JEST") || key.equals("POTW_DZIALU_KADR"))
	            {
	            	values.put(key+"_WARTOSC", value==null ? " " : fm.getEnumItem(key).getTitle());
	            	continue;
	            }
	            
	            values.put(key+"_WARTOSC", value==null ? " " : value.toString());
	            
	        }
        
		} catch (DocumentNotFoundException e) {
			LOG.error(e.getMessage(),e);
		} catch (DocumentLockedException e) {
			LOG.error(e.getMessage(),e);
		} catch (AccessDeniedException e) {
			LOG.error(e.getMessage(),e);
		} catch (EdmException e) {
			LOG.error(e.getMessage(),e);
		} catch (Exception e) {
			LOG.error(e.getMessage(),e);
		}
		
		
	}


	@Override
    public void archiveActions(Document document, int type, ArchiveSupport as) throws EdmException {
        Folder folder = Folder.getRootFolder();
        folder = folder.createSubfolderIfNotPresent(document.getDocumentKind().getName());
        document.setFolder(folder);
        document.setTitle("Title id: "+document.getId());
    }
	
	
	@Override
	public void setInitialValues(FieldsManager fm, int type) throws EdmException {
		Map<String, Object> values = Maps.newHashMap();
		values.put("BYDGOSZCZ_DNIA", Calendar.getInstance().getTime());
		values.put("LINK_DO_ZALACZNIKOW", "");
		fm.reloadValues(values);
	}
	
	
	@Override
	public void documentPermissions(Document document) throws EdmException {
		java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();
		perms.add(new PermissionBean(ObjectPermission.READ, "DOCUMENT_READ", ObjectPermission.GROUP, "Dokumenty zwyk�y- odczyt"));
		perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, "DOCUMENT_READ_ATT_READ", ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - odczyt"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY, "DOCUMENT_READ_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk�y - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, "DOCUMENT_READ_ATT_MODIFY", ObjectPermission.GROUP, "Dokumenty zwyk�y zalacznik - modyfikacja"));
		perms.add(new PermissionBean(ObjectPermission.DELETE, "DOCUMENT_READ_DELETE", ObjectPermission.GROUP, "Dokumenty zwyk�y - usuwanie"));
		
		for (PermissionBean perm : perms) {
			if (perm.isCreateGroup()
					&& perm.getSubjectType().equalsIgnoreCase(
							ObjectPermission.GROUP)) {
				String groupName = perm.getGroupName();
				if (groupName == null) {
					groupName = perm.getSubject();
				}
				createOrFind(document, groupName, perm.getSubject());
			}
			DSApi.context().grant(document, perm);
			DSApi.context().session().flush();
		}
		
	}
	
	/* (non-Javadoc)
	 * @see pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic#onLoadData(pl.compan.docusafe.core.dockinds.FieldsManager)
	 * 
	 * Metoda zastosowana tutaj w celu uzupe�nienia pola "NUMER_SPRAWY" na dockindzie. Pocz�tkowo dokument nie ma warto�ci w tym polu,
	 * ale zostanie ona dodana po przypisaniu dokumentu do jakiej� sprawy. Pole "NUMER_SPRAWY" jest readonly.
	 * Metoda r�wnie� uzupe�nia warto�� pola kwota przyznana warto�ci� wprowadzon� przez wnioskuj�cego w polu kwota wnioskowana.
	 */
	@Override
	public void onLoadData(FieldsManager fm) throws EdmException {
		
		if(fm.getDocumentId()!=null)
		{	
			OfficeDocument document = OfficeDocument.find(fm.getDocumentId());
			Map<String, Object> values = new HashMap<String, Object>();
			// uzupe�nienie warto�ci numer sprawy na dokumencie
			if (document instanceof OutOfficeDocument) {
				OutOfficeDocument outOfficeDocument = (OutOfficeDocument) document;
				
				if(outOfficeDocument.getContainingCase()!= null)
					values.put("NUMER_SPRAWY", outOfficeDocument.getContainingCase().getOfficeId());
				
			}
			//uzupe�nienie kwoty przyznanej kwot� wnioskowan�
			if (!StringUtils.isEmpty( fm.getStringValue(KWOTA_WNIOSKOWANA)) && (StringUtils.isEmpty(fm.getStringValue(KWOTA_PRZYZNANA))) ){
				

				values.put( KWOTA_PRZYZNANA, new BigDecimal(fm.getStringValue(KWOTA_WNIOSKOWANA).replace(" ","")) );
				
			}
			
			//prze�adowanie fields�w w pami�ci i w DB
			fm.reloadValues(values);
			fm.getDocumentKind().setOnly(fm.getDocumentId(),values);
		}
	}

}
