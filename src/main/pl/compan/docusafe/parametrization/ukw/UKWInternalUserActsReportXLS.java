package pl.compan.docusafe.parametrization.ukw;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.util.HSSFColor;
import org.hibernate.HibernateException;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;

public class UKWInternalUserActsReportXLS extends AbstractXlsReports{

	protected static Logger log = LoggerFactory.getLogger(UKWInternalUserActsReportXLS.class);


	private String[] name;
	private Boolean zapoznalSie;
    private StringManager sm =
            GlobalPreferences.loadPropertiesFile(this.getClass().getPackage().getName(),null);
    
	public static final String UKW_WEW_AKTY_PRAW_NUMER_AKTU_CN="NUMER_AKTU_PRAWNEGO";
	public static final String UKW_WEW_AKTY_PRAW_RODZAJ_WEWNETRZNE_AKTY_PRAWNE_CN="RODZAJ_WEWNETRZNE_AKTY_PRAWNE";
	public static final String UKW_WEW_AKTY_PRAW_RODZAJ_WEWNETRZNE_AKTY_PRAWNE_TABLENAME="dsg_ukw_rodzaj_aktu";
	public static final String UKW_WEW_AKTY_DATA_WEJSCIA_CN="DATA_WEJSCIA";
	public static final String UKW_WEW_AKTY_PRAW_UCHYLONY_CN="UCHYLONY";



	public UKWInternalUserActsReportXLS(String reportName) {
		super(reportName);
		title = new String[] { reportName };

		columns = new String[] { "Lp.","ID dokumentu","Numer aktu prawnego", "Rodzaj",
				"Data rozporz�dzenia", "Wchodzi w �ycie","Opis","Uchylony/Zmieniony","Imie","Nazwisko","Czy zapozna� si�" };
	}

	@Override
	protected void createEntries() {
		int rowCount = 3;
		int lp = 1;
		HashSet<String> usernames=new HashSet<String>(0);

		if(name!=null){
			for(int i=0;i<name.length;i++){
				usernames.add(name[i]);
			}

		}
		try {
			ArrayList<Long> idiki=(ArrayList<Long>) Document.findByDocumentKind(DocumentKind.findByCn(DocumentLogicLoader.UKW_WEW_AKTY_PRAW).getId());
			ArrayList<HashMap<String, Document>> usersToDocuments=new ArrayList<HashMap<String, Document>>();
			HashMap<String, DSUser> users=new HashMap<String, DSUser>();
			HashMap<String, Boolean> usersCzyZapoznalSie=new HashMap<String, Boolean>();
			for(Long id:idiki){
				OfficeDocument officeDocument = OfficeDocument.find(id);
				List<AssignmentHistoryEntry> ahes=officeDocument.getAssignmentHistory();

				znajdzPojedyczneDekretacje(ahes,users,usersCzyZapoznalSie,usersToDocuments);
				znajdzZDekretacjiNaGrupe(ahes,users,usersCzyZapoznalSie,usersToDocuments);
			}
			for(int i=0;i<usersToDocuments.size();i++){
				for(String key:usersToDocuments.get(i).keySet()){
					if(usernames.size()==0||(usernames.size()!=0&&usernames.contains(key))){
						if(zapoznalSie == null ||
								(zapoznalSie!=null&&zapoznalSie.equals(usersCzyZapoznalSie.get(key+usersToDocuments.get(i).get(key).getId())))){
							DSUser user=DSUser.findByUsername(key);
							wpisz(rowCount,lp,usersToDocuments.get(i).get(key).getId(),user,usersCzyZapoznalSie.get(key+usersToDocuments.get(i).get(key).getId()));
							rowCount++;
							lp++;
						}
					}
				}
			}


		} catch (HibernateException e) {
			log.debug("",e);
			e.printStackTrace();
		} catch (SQLException e) {
			log.debug("",e);
			e.printStackTrace();
		} catch (EdmException e) {
			log.debug("",e);
			e.printStackTrace();
		}



	}
	public void wpisz(int rowCount, int lp,Long documentId, DSUser user, Boolean czySieZapoznal) {
		try {
			int i = -1;
			HSSFRow row = sheet.createRow(rowCount);
			//1
			row.createCell(++i).setCellValue(new HSSFRichTextString(lp + ""));
			Document doc=Document.find(documentId);
			FieldsManager fm=doc.getFieldsManager();
			Map<String, Object> values=fm.getFieldValues();
			//2
			if (documentId != null)
				row.createCell(++i).setCellValue(new HSSFRichTextString(documentId.toString()));
			else
				row.createCell(++i).setCellValue(new HSSFRichTextString("-"));

			//3
			String numerAktu="-";
			if(values.containsKey(UKW_WEW_AKTY_PRAW_NUMER_AKTU_CN)&&values.get(UKW_WEW_AKTY_PRAW_NUMER_AKTU_CN)!=null&&!values.get(UKW_WEW_AKTY_PRAW_NUMER_AKTU_CN).equals(""))
				numerAktu=(String) values.get(UKW_WEW_AKTY_PRAW_NUMER_AKTU_CN);

			row.createCell(++i).setCellValue(new HSSFRichTextString(numerAktu));
			//4
			String rodzaj="-";
			if(values.containsKey(UKW_WEW_AKTY_PRAW_RODZAJ_WEWNETRZNE_AKTY_PRAWNE_CN)&&values.get(UKW_WEW_AKTY_PRAW_RODZAJ_WEWNETRZNE_AKTY_PRAWNE_CN)!=null&&!values.get(UKW_WEW_AKTY_PRAW_RODZAJ_WEWNETRZNE_AKTY_PRAWNE_CN).equals("")){
				DataBaseEnumField dbef=DataBaseEnumField.getEnumFiledForTable(UKW_WEW_AKTY_PRAW_RODZAJ_WEWNETRZNE_AKTY_PRAWNE_TABLENAME);
				EnumItem ei=dbef.getEnumItem((Integer) values.get(UKW_WEW_AKTY_PRAW_RODZAJ_WEWNETRZNE_AKTY_PRAWNE_CN));
				rodzaj=ei.getTitle();
			}
			row.createCell(++i).setCellValue(new HSSFRichTextString(rodzaj));

			//5
			SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
			row.createCell(++i).setCellValue(new HSSFRichTextString(sdf.format(doc.getCtime())));

			//6
			Date wchodziWZycie=null;
			if(values.containsKey(UKW_WEW_AKTY_DATA_WEJSCIA_CN)&&values.get(UKW_WEW_AKTY_DATA_WEJSCIA_CN)!=null&&!values.get(UKW_WEW_AKTY_DATA_WEJSCIA_CN).equals(""))
				wchodziWZycie=(Date) values.get(UKW_WEW_AKTY_DATA_WEJSCIA_CN);
			if(wchodziWZycie!=null)
				row.createCell(++i).setCellValue(new HSSFRichTextString(sdf.format(wchodziWZycie)));
			else
				row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
			//7
			row.createCell(++i).setCellValue(new HSSFRichTextString(doc.getDescription()));
			//8
			String uchylony=sm.getString("nie");
			if(values.containsKey(UKW_WEW_AKTY_PRAW_UCHYLONY_CN)&&values.get(UKW_WEW_AKTY_PRAW_UCHYLONY_CN)!=null&&!values.get(UKW_WEW_AKTY_PRAW_UCHYLONY_CN).equals(""))
				if((Boolean)values.get(UKW_WEW_AKTY_PRAW_UCHYLONY_CN))uchylony=sm.getString("tak"); else uchylony=sm.getString("nie");

			row.createCell(++i).setCellValue(new HSSFRichTextString(uchylony));
			//9
			if (user.getFirstname() != null)
				row.createCell(++i).setCellValue(new HSSFRichTextString(user.getFirstname()));
			else
				row.createCell(++i).setCellValue(new HSSFRichTextString("-"));
			//10
			if (user.getLastname() != null)
				row.createCell(++i).setCellValue(new HSSFRichTextString(user.getLastname()));
			else
				row.createCell(++i).setCellValue(new HSSFRichTextString("-"));

/*			//11
			if (user.getDivisions() != null){
				String div="";
				for(int j=0;j<user.getDivisions().length;j++){
					div+=user.getDivisions()[j].getName()+"; ";
				}
				if(user.getDivisions().length==0)div="-";
				row.createCell(++i).setCellValue(
						new HSSFRichTextString(div));
			}
			else
				row.createCell(++i).setCellValue(new HSSFRichTextString("-"));*/

			//12
			String czy=sm.getString("nie");
			if (czySieZapoznal != null)
				if(czySieZapoznal)czy=sm.getString("tak");
				else czy=sm.getString("nie");
			
			if(czy.equals(sm.getString("nie"))){
				HSSFCellStyle style=this.workbook.createCellStyle();
				style.setFillForegroundColor(HSSFColor.RED.index);

				HSSFCell cell = row.createCell(++i);
				cell.setCellStyle(style);
				style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

				HSSFFont font = this.workbook.createFont();
				font.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
				font.setColor(HSSFColor.BLACK.index);

				style.setFont(font);

				cell.setCellValue(new HSSFRichTextString(czy));
			} 
			else	row.createCell(++i).setCellValue(new HSSFRichTextString(czy));
		} catch (EdmException e) {
			log.debug("",e);
			e.printStackTrace();
		}

	}

	private void znajdzZDekretacjiNaGrupe(List<AssignmentHistoryEntry> ahes,
			HashMap<String, DSUser> users, HashMap<String, Boolean> usersCzyZapoznalSie, ArrayList<HashMap<String, Document>> documents) {
		for(AssignmentHistoryEntry ahe:ahes){
			if(ahe.getType().equals(AssignmentHistoryEntry.JBPM4_REASSIGN_DIVISION)){
				try {
					DSUser[] usersTMP=DSDivision.find(ahe.getTargetGuid()).getUsers();
					for(int i=0;i<usersTMP.length;i++){
						users.put(usersTMP[i].getName()+ahe.getDocumentId(), usersTMP[i]);
						usersCzyZapoznalSie.put(usersTMP[i].getName()+ahe.getDocumentId(), czySieZapoznal(ahes,usersTMP[i],ahe.getCdate()));
						HashMap<String, Document> tmp=new HashMap<String, Document>();
						tmp.put(usersTMP[i].getName(),Document.find(ahe.getDocumentId()));
						documents.add(tmp);
					}

				} catch (DivisionNotFoundException e) {
					log.debug("",e);
					e.printStackTrace();
				} catch (EdmException e) {
					log.debug("",e);
					e.printStackTrace();
				}
			}
		}

	}

	private Boolean czySieZapoznal(List<AssignmentHistoryEntry> ahes,
			DSUser dsUser, Date date) {
		for(AssignmentHistoryEntry ahe:ahes){
			if(ahe.getCdate().before(date))continue;
			if(ahe.getSourceUser().equals(dsUser.getName())&&ahe.getType().equals(AssignmentHistoryEntry.JBPM4_CONFIRMED))return true;
		}
		return false;
	}

	private void znajdzPojedyczneDekretacje(List<AssignmentHistoryEntry> ahes,
			HashMap<String, DSUser> users, HashMap<String, Boolean> usersCzyZapoznalSie, ArrayList<HashMap<String, Document>> documents) {
		for(AssignmentHistoryEntry ahe:ahes){
			if(ahe.getType().equals(AssignmentHistoryEntry.JBPM4_REASSIGN_SINGLE_USER)){
				try {
					users.put(ahe.getTargetUser(), DSUser.findByUsername(ahe.getTargetUser()));
					usersCzyZapoznalSie.put(ahe.getTargetUser()+ahe.getDocumentId(), czySieZapoznal(ahes,DSUser.findByUsername(ahe.getTargetUser()),ahe.getCdate()));
					HashMap<String, Document> tmp=new HashMap<String, Document>();
					tmp.put(ahe.getTargetUser(),Document.find(ahe.getDocumentId()));
					documents.add(tmp);
				} catch (DivisionNotFoundException e) {
					log.debug("",e);
					e.printStackTrace();
				} catch (EdmException e) {
					log.debug("",e);
					e.printStackTrace();
				}
			}
		}

	}


	public void setName(String[] name) {
		this.name = name;
	}

	public void setZapoznalSie(Boolean zapoznalSie) {
		this.zapoznalSie = zapoznalSie;
	}

}
