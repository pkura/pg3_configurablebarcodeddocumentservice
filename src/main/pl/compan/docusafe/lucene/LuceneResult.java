package pl.compan.docusafe.lucene;

public class LuceneResult {
	private Long id;
    private String title;
	private String originalFilename;

	public LuceneResult(Long id, String originalFilename, String title) {
		this.id = id;
		this.originalFilename = originalFilename;
        this.title = title;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOriginalFilename() {
		return originalFilename;
	}

	public void setOriginalFilename(String originalFilename) {
		this.originalFilename = originalFilename;
	}

	@Override
	public String toString() {
		return "LuceneResult [id=" + id + ", originalFilename=" + originalFilename + "]";
	}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
