package pl.compan.docusafe.lucene.searcher;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.util.Version;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.lucene.DocumentSearcher;
import pl.compan.docusafe.lucene.IndexHelper;
import pl.compan.docusafe.lucene.LuceneResult;

public class DefaultSearcher implements DocumentSearcher {

	public List<LuceneResult> search(String query, Integer hits) throws Exception  {
		if (query == null)
			throw new NullPointerException("Zapytanie nie mo�e by� puste!");
		
		List<LuceneResult> results = new ArrayList<LuceneResult>();
		// tworzymy standard analyzer
		StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_46);
		
		// parsujemy zapytanie z tekstu podanego jako parametr, szukamy po tre�ci zaindeksowanych dokument�w
		Query q = new QueryParser(Version.LUCENE_46, IndexHelper.CONTENT_FIELD, analyzer).parse(query);
		
		// wyszukiwanie
        IndexReader reader = DirectoryReader.open(Docusafe.getIndexes());
		IndexSearcher searcher = new IndexSearcher(reader);
		// wybieramy pewn� ilo�� maksymalnych wynik�w
		TopScoreDocCollector collector = TopScoreDocCollector.create(hits, true);
		searcher.search(q, collector);
		ScoreDoc[] hitsDoc = collector.topDocs().scoreDocs;

        for (ScoreDoc aHitsDoc : hitsDoc) {
            int docId = aHitsDoc.doc;
            Document d = searcher.doc(docId);
            results.add(new LuceneResult(
                    new Long(d.get(IndexHelper.DOCUMENT_ID_FIELD)),
                    d.get(IndexHelper.ORIGINAL_FILENAME_FIELD), d.get(IndexHelper.TITLE_FIELD)));
        }

		return results;
	}

}
