package pl.compan.docusafe.lucene.exception;

import pl.compan.docusafe.core.EdmException;

public class FileHandlerException extends EdmException {
	private static final long serialVersionUID = 1L;

	public FileHandlerException(String message) {
		super(message);
	}
	
    public FileHandlerException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public FileHandlerException(Throwable cause)
    {
        super(cause);
    }

}
