package pl.compan.docusafe.lucene.exception;

import pl.compan.docusafe.core.EdmException;

public class DocumentHandlerException extends EdmException {
	private static final long serialVersionUID = 1L;

	public DocumentHandlerException(String message) {
		super(message);
	}
	
    public DocumentHandlerException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public DocumentHandlerException(Throwable cause)
    {
        super(cause);
    }
}
