package pl.compan.docusafe.lucene;

import java.io.IOException;
import java.io.Reader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.dbutils.DbUtils;
import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.lucene.exception.DocumentHandlerException;

/**
 * Klasa wspomagaj�ca indeksowanie plik�w przez Lucene.
 * Znajduj� si� tutaj nazwy kodowe p�l oraz funkcje wspomagaj�ce dodawanie p�l do indeksu.
 *
 * @author przemek
 */
public class IndexHelper {
    private static final Logger log = LoggerFactory.getLogger(IndexHelper.class);
    public static final String CONTENT_FIELD = "content";
    public static final String ORIGINAL_FILENAME_FIELD = "originalFilename";
    public static final String DOCUMENT_ID_FIELD = "documentId";
    public static final String TITLE_FIELD = "documentTitle";
    public static final String IDS_TO_INDEX_QUERY = "SELECT id FROM ds_document WHERE dockind_id=? AND (index_status_id = ? OR index_status_id = ? OR index_status_id = ?)";

    // given caveat of increased search times when using
    //MICROSECOND, only use SECOND by default
    private DateTools.Resolution dateTimeResolution = DateTools.Resolution.SECOND;

    /**
     * Dodaje pole do dokumentu o podanej nazwie i warto�ci. Pole jest przechowywane w indeksie, nie jest analizowane.
     *
     * @param document - document lucene
     * @param name     - nazwa pola (kod)
     * @param value-   warto�� pola (String)
     */
    public void addKeywordField( Document document, String name, String value )
    {
        if ( value != null )
        {
            document.add( new Field( name, value, Field.Store.YES, Field.Index.NOT_ANALYZED ) );
        }
    }

    /**
     * Dodaje pole do dokumentu o podanej nazwie i warto�ci. Pole jest przechowywane w indeksie, nie jest analizowane.
     *
     * @param document - document lucene
     * @param name     - nazwa pola (kod)
     * @param value-   warto�� pola (Long)
     */
    public void addKeywordField( Document document, String name, Long value )
    {
        if ( value != null )
        {
            document.add( new Field( name, value.toString(), Field.Store.YES, Field.Index.NOT_ANALYZED ) );
        }
    }

    /**
     * Dodaje pole o podanej nazwie do indeksu. Jako warto�� przekazuje si� Readera.
     * Pole nie jest przechowywane w indeksie, u�ywane do wyszukiwania (pole jest analizowane).
     *
     * @param document - lucene document
     * @param name     - nazwa pola (kod)
     * @param value    - warto�� (java.io.Reader)
     */
    public void addTextField( Document document, String name, Reader value )
    {
        if ( value != null )
        {
            document.add( new Field( name, value ) );
        }
    }

    /**
     * Dodanie pola o podanej nazwie i warto�ci. Pole nie jest trzymane w indeksie.
     * Pole jest analizowane.
     *
     * @param document - dokument lucene
     * @param name     - nazwa pola
     * @param value    - warto�� pola
     */
    public void addTextField( Document document, String name, String value ) {
        if ( value != null ) {
            document.add( new Field( name, value, Field.Store.NO, Field.Index.ANALYZED ) );
        }
    }

    /**
     * Dodanie pola o podanej nazwie i warto�ci. Pole jest trzymane w indeksie orazy analizowane.
     *
     * @param document - dokument lucene
     * @param name     - nazwa pola
     * @param value    - warto�� pola
     */
    public void addStoredTextField( Document document, String name, String value )
    {
        if ( value != null )
        {
            document.add( new Field( name, value, Field.Store.YES, Field.Index.ANALYZED ) );
        }
    }

    /**
     * Dodanie pola typu Date do indeksu, pole trzymane w indeksie.
     *
     * @param document - dokument lucene
     * @param name     - nazwa pola
     * @param value    - warto�� pola
     */
    public void addTextField( Document document, String name, Date value )
    {
        if ( value != null )
        {
            addTextField( document, name, DateTools.dateToString( value, dateTimeResolution ) );
        }
    }

    /**
     * Dodanie pola typu Calendar do indeksu, pole trzymane w indeksie.
     *
     * @param document - dokument lucene
     * @param name     - nazwa pola
     * @param value    - warto�� pola
     */
    public void addTextField( Document document, String name, Calendar value )
    {
        if ( value != null )
        {
            addTextField( document, name, value.getTime() );
        }
    }

    /**
     * Dodanie pola do indeksu, aczkolwiek bez indeksaci. Pole b�dzie przechowywane w indeksie ale nie b�dzie
     * mo�na po nim wyszukiwa�.
     */
    public void addUnindexedField( Document document, String name, String value )
    {
        if ( value != null )
        {
            document.add( new Field( name, value, Field.Store.YES, Field.Index.NO ) );
        }
    }

    /**
     * Dodanie pola, kt�re nie b�dzie przetrzymywane w indeksie, nie b�dzie te� analizowane (pole kluczowe),
     * lecz b�dzie mo�na go u�y� przy wyszukiwaniu.
     *
     * @param document
     * @param name
     * @param value
     */
    public void addUnstoredKeywordField( Document document, String name, String value )
    {
        if ( value != null )
        {
            document.add( new Field( name, value, Field.Store.NO, Field.Index.NOT_ANALYZED ) );
        }
    }

    /**
     * Pobiera "rozdzielczo��" czasu.
     *
     * @return current date/time resolution
     */
    public DateTools.Resolution getDateTimeResolution()
    {
        return dateTimeResolution;
    }

    /**
     * Ustawia "rozdzielczo��" czasu.
     *
     * @param resolution set new date/time resolution
     */
    public void setDateTimeResolution( DateTools.Resolution resolution )
    {
        dateTimeResolution = resolution;
    }

    //
    // compatibility methods for lucene-1.9+
    //
    public String timeToString( long time )
    {
        return DateTools.timeToString( time, dateTimeResolution );
    }

    /**
     * Metoda zwraca list� dokument�w do zaindeksowania.
     *
     * @return
     * @throws Exception
     */
	public List<Long> getDocumentsToIndex(Long dockindId) throws Exception
	{
		log.trace("dockingId: {}",dockindId);
        List<Long> documents = new ArrayList<Long>();
        PreparedStatement ps = null;
        ResultSet rs = null;
		try {
            ps = DSApi.context().prepareStatement(IDS_TO_INDEX_QUERY);
            ps.setLong(1, dockindId);
            ps.setInt(2, IndexStatus.NOT_INDEXED.id);
            ps.setInt(3, IndexStatus.DIRTY_INDEX.id);
            ps.setInt(4, IndexStatus.RETURNED_FROM_OCR.id);
            rs = ps.executeQuery();
			while (rs.next()){
                Long docId = rs.getLong("id");
                documents.add(docId);
            }
		}
		finally
		{
            DbUtils.closeQuietly(rs);
            DSApi.context().closeStatement(ps);
        }

        return documents;
    }

    public void IndexDocument(Long docId, IndexWriter indexWriter) {
        /**
         * Zmienna okre�la czy indeksowa� ostatnio dodany za�acznik dokumentu, czy wszystkie za��czniki.
         */
        log.info("IndexDocument " + docId);

        boolean indexDockinds = AvailabilityManager.isAvailable("fulltextSearch.indexDockindAttributes");
        boolean mostRecentRevision = AvailabilityManager.isAvailable("fulltextSearch.mostRecentRevision");
        org.apache.lucene.document.Document luceneDocument = null;
        try {
            pl.compan.docusafe.core.base.Document doc = pl.compan.docusafe.core.base.Document.find(docId);
            if(indexDockinds){
                indexDockindDocument(doc, indexWriter);
            }
            List<Attachment> attList = doc.getAttachments();
            for (Attachment att : attList) {
                // zaindeksowanie ostatniego za��cznika
                if (mostRecentRevision) {
                    AttachmentRevision rev = att.getMostRecentRevision();
                    log.info("index status : {} ", rev == null ? null : rev.getIndexStatus());
                    indexAttachmentRevision(rev, indexWriter);
                } else {
                    Set<AttachmentRevision> revisions = att.getRevisions();
                    for (AttachmentRevision rev : revisions) {
                        indexAttachmentRevision(rev, indexWriter);
                    }
                }
            }
        } catch (Exception e) {
            log.error("", e);
        }
    }

    private void indexDockindDocument(pl.compan.docusafe.core.base.Document doc, IndexWriter indexWriter) throws Exception {
        DockindIndexer.index(doc, indexWriter);
    }

    public void indexAttachmentRevision(AttachmentRevision rev, IndexWriter indexWriter) throws DocumentHandlerException, IOException {
        ExtensionDocumentHandler extDocHandler = new ExtensionDocumentHandler();
        if (rev != null && rev.getIndexStatus() != IndexStatus.INDEXED) {
            try {
                Document luceneDocument = extDocHandler.getDocument(rev);
                if (luceneDocument == null) {
                    rev.setIndexStatus(IndexStatus.INDEXING_NOT_SUPPORTED);
                    pl.compan.docusafe.core.base.Document doc = rev.getAttachment().getDocument();
                    doc.getDocumentKind().logic().getOcrSupport().checkForOcr(doc, rev);
                } else {
                    addKeywordField(luceneDocument, "attachmentRevisionId", rev.getId());
                    addKeywordField(luceneDocument, "attachmentnId", rev.getAttachment().getId());
                    addKeywordField(luceneDocument, TITLE_FIELD, rev.getAttachment().getDocument().getTitle());
                    addKeywordField(luceneDocument, "documentId", rev.getAttachment().getDocument().getId());
                    indexWriter.addDocument(luceneDocument);
                    rev.setIndexStatus(IndexStatus.INDEXED);
                }
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
                rev.setIndexStatus(IndexStatus.INDEXING_ERROR);
            }
        }
    }
}
