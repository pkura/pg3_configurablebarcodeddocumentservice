package pl.compan.docusafe.lucene;

import java.util.List;

public interface DocumentSearcher {
	public List<LuceneResult> search(String query, Integer hits) throws Exception;
}