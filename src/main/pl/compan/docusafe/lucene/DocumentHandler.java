package pl.compan.docusafe.lucene;

import java.io.IOException;
import java.io.InputStream;

import org.apache.lucene.document.Document;

import pl.compan.docusafe.lucene.exception.DocumentHandlerException;

public interface DocumentHandler {
	public Document getDocument(InputStream is) throws DocumentHandlerException, IOException;
}
