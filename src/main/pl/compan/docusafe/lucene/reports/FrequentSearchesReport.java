package pl.compan.docusafe.lucene.reports;

import com.google.common.base.Objects;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportParameter;
import pl.compan.docusafe.reports.tools.UniversalTableDumper;
import pl.compan.docusafe.reports.tools.XlsPoiDumper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;

/**
 * Klasa podsumowuj�ce najcz�ciej wykonywane zapytania do lucynki
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class FrequentSearchesReport extends Report {
    private final static Logger LOG = LoggerFactory.getLogger(FrequentSearchesReport.class);
    private static final String QUERY =
            "SELECT object_id as query, count(1) as query_count " +
            "FROM ds_datamart WHERE event_code = ? " +
            "GROUP BY object_id ";
    private static final String ORDER = "ORDER BY query_count DESC";

    @Override
    protected void validatePrams(Map<String, ReportParameter> newMap) {
        String minCount = newMap.get("minCount").getValueAsString().trim();
        String topCount = newMap.get("resultsCount").getValueAsString().trim();
        LOG.info("validateParams : map {} {} {}", newMap,  minCount, topCount);
        if(!StringUtils.isBlank(minCount) && !StringUtils.isNumeric(minCount)){
            throw new IllegalArgumentException(newMap.get("minCount").getLabel() + " musi by� liczb� ca�kowit�");
        }
        if(!StringUtils.isBlank(topCount) && !StringUtils.isNumeric(topCount)){
            throw new IllegalArgumentException(newMap.get("resultsCount").getLabel() + " musi by� liczb� ca�kowit�");
        }
    }

    private Integer getMinCount(){
        String minCount = parametersMap.get("minCount").getValueAsString().trim();
        return minCount.length() == 0 ? null : Integer.valueOf(minCount);
    }

    private Integer getTopCount(){
        String topCount = parametersMap.get("resultsCount").getValueAsString().trim();
        return topCount.length() == 0 ? null : Integer.valueOf(topCount);
    }

    @Override
    public void doReport() throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;

        LOG.info("doReport params : {}", parametersMap);

        Integer topCount = getTopCount();
        Integer minCount = getMinCount();

        try {
            String having = minCount == null ? "" : " HAVING count(1) >= ? ";
            ps = DSApi.context().prepareStatement(QUERY + having + ORDER);
            LOG.info(QUERY + having + ORDER);

            ps.setString(1, DataMartDefs.FULL_TEXT_SEARCH);
            if(minCount != null){
                ps.setInt(2, minCount);
            }

            if(topCount != null){
                ps.setMaxRows(topCount);
            }

            rs = ps.executeQuery();

            dumpResults(rs);
        } finally {
            DbUtils.closeQuietly(rs);
            DSApi.context().closeStatement(ps);
        }
    }

    private void dumpResults(ResultSet rs) throws Exception {
        UniversalTableDumper dumper = new UniversalTableDumper();
        try {
            XlsPoiDumper poiDumper = new XlsPoiDumper();
            File xlsFile = new File(this.getDestination(), "raport.xls");
            poiDumper.openFile(xlsFile);
            dumper.addDumper(poiDumper);

            dumper.addRow(
                    "Zapytanie",
                    "Liczba"
            );

            while(rs.next()){
                dumper.newLine();
                dumper.addText(rs.getString(1));
                dumper.addInteger(rs.getInt(2));
                dumper.dumpLine();
            }
        } finally {
            dumper.closeFileQuietly();
        }
    }
}
