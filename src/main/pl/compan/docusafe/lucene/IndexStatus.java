package pl.compan.docusafe.lucene;

import pl.compan.docusafe.util.IdEntity;
import pl.compan.docusafe.util.IdEnumSupport;

/** Status zaindeksowania danego obiektu */
public enum IndexStatus implements IdEntity<Integer> {
    /** Indeks nie zosta� jeszcze utworzony */
    NOT_INDEXED(0),
    /** Tworzenie indeksu dla danego rodzaju dokumentu / za��cznika / dockindu nie jest jeszcze wspierane*/
    INDEXING_NOT_SUPPORTED(10),
    /** Wyst�pi� b��d podczas indeksowania */
    INDEXING_ERROR(20),
    /** Indeks jest ju� nieaktualny - obiekt zmodyfikowano*/
    DIRTY_INDEX(30),
    /**Dokument wys�any do OCR-a*/
    SEND_TO_OCR(40),
    /**Wr�ci� z OCR*/
    RETURNED_FROM_OCR(50),
    /** Indeks zosta� utworzony */
    INDEXED(100);

    private final static IdEnumSupport<Integer, IndexStatus> indexStatusSupport
            = IdEnumSupport.create(IndexStatus.class);

    public static IndexStatus get(int id){
        return indexStatusSupport.get(id);
    }

    public final int id;

    IndexStatus(int statusId) {
        this.id = statusId;
    }

    public Integer getId(){
        return id;
    }
}
