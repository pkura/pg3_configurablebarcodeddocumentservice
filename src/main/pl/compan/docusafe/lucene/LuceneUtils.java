package pl.compan.docusafe.lucene;

import java.util.*;

/**
 * Klasa narz�dziowa do Lucynki
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public final class LuceneUtils {
    private final static Set<String> SUPPORTED_EXTENSIONS;

    static {
        Set<String> set = new HashSet<String>(Arrays.asList("pdf", "doc", "txt", "xml", "html", "rtf", "odt"));
        SUPPORTED_EXTENSIONS = Collections.unmodifiableSet(set);
    }

    public static Set<String> getSupportedExtensions(){
        return SUPPORTED_EXTENSIONS;
    }

    private LuceneUtils(){}
}
