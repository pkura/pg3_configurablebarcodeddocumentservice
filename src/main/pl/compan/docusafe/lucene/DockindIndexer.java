package pl.compan.docusafe.lucene;

import com.google.common.base.Objects;
import org.apache.commons.lang.NotImplementedException;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.Query;
import org.apache.lucene.util.Version;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Arrays;

/**
 * Klasa narz�dziowa indeksuj�ca dokumenty Dockind
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class DockindIndexer {
    private final static Logger LOG = LoggerFactory.getLogger(DockindIndexer.class);
    public final static String INDEXED_FIELDS_CNS_PROPERTY = "indexed-fields";

    public static void index(pl.compan.docusafe.core.base.Document doc, IndexWriter indexWriter) throws Exception {
        if(doc.getIndexStatus() == IndexStatus.INDEXED){
            LOG.warn("document {} already indexed", doc.getId());
        } else try {
            LOG.info("indexing dockind document : {}", doc.getId());
            Document luceneDoc = new Document();
            IndexHelper helper = new IndexHelper();
            String[] cns = Objects.firstNonNull(
                                doc.getDocumentKind().getDockindInfo().getProperties().get(INDEXED_FIELDS_CNS_PROPERTY),
                                "")
                            .split(",");
            LOG.info("indexed fields : '{}'", Arrays.asList(cns));

            if(cns.length > 0){//istniej� pola do indeksowania
                FieldsManager fm = doc.getFieldsManager();
                for(String cn : cns) {
                    helper.addTextField(luceneDoc, IndexHelper.CONTENT_FIELD, fm.getStringValue(cn));
                }
                helper.addKeywordField(luceneDoc, "documentId", doc.getId());
                helper.addKeywordField(luceneDoc, "documentTitle", doc.getTitle());
                helper.addKeywordField(luceneDoc, "dockindIndex", 1L);
                if(doc.getIndexStatus() == IndexStatus.DIRTY_INDEX){
                    removeDockindIndices(indexWriter, doc);
                }
                indexWriter.addDocument(luceneDoc);
                doc.setIndexStatus(IndexStatus.INDEXED);
            } else { //nie okre�lono p�l do indeksowania
                doc.setIndexStatus(IndexStatus.INDEXING_NOT_SUPPORTED);
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            doc.setIndexStatus(IndexStatus.INDEXING_ERROR);
        }
    }

    private static void removeDockindIndices(IndexWriter indexWriter, pl.compan.docusafe.core.base.Document doc) throws Exception {
        StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_46);
        Query q = new QueryParser(Version.LUCENE_46, IndexHelper.CONTENT_FIELD, analyzer)
                .parse("documentId:" + doc.getId() + " AND dockindIndex:1");
        indexWriter.deleteDocuments(q);
    }
}
