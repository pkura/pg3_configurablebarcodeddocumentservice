package pl.compan.docusafe.lucene;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.document.Document;

import pl.compan.docusafe.lucene.exception.FileHandlerException;

public interface FileHandler {
	public Document getDocument( File file ) throws FileHandlerException ,IOException;
}
