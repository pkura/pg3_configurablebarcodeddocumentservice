package pl.compan.docusafe.lucene.handlers;

import org.apache.lucene.document.Document;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.odf.OpenDocumentParser;
import org.apache.tika.parser.rtf.RTFParser;
import pl.compan.docusafe.lucene.exception.DocumentHandlerException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Klasa parsuj�ca dokumenty zgodne ze standardem Opendocument, wykorzystuje apache tika
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class LuceneOpendocumentHandler extends BaseDocumentHandler{
    private final static Logger LOG = LoggerFactory.getLogger(LuceneOpendocumentHandler.class);

    public Document getDocument(InputStream is) throws DocumentHandlerException, IOException {
        LOG.info("opendocument handler");
        CharacterContentHandler handler = new CharacterContentHandler();
        OpenDocumentParser parser = new OpenDocumentParser();
        try {
            parser.parse(is, handler, new Metadata(), new ParseContext());
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return handler.document;
    }
}