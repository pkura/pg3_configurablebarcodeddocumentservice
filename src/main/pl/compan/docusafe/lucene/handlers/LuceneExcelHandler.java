package pl.compan.docusafe.lucene.handlers;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import pl.compan.docusafe.lucene.DocumentHandler;
import pl.compan.docusafe.lucene.IndexHelper;
import pl.compan.docusafe.lucene.exception.DocumentHandlerException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Klasa indeksuj�ca pliki excela z u�yciem POI
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class LuceneExcelHandler extends BaseDocumentHandler {
    private final static Logger LOG = LoggerFactory.getLogger(LuceneExcelHandler.class);

    public Document getDocument(InputStream is) throws DocumentHandlerException, IOException {
        LOG.info("excel handler");
        Document document = new Document();
        try {
            Workbook wb = WorkbookFactory.create(is);
            fillDocument(document, wb);
        } catch (InvalidFormatException e) {
            throw new RuntimeException(e);
        }
        return document;
    }

    private void fillDocument(Document document, Workbook wb) {
        int sheetCount = wb.getNumberOfSheets();
        for(int i = 0; i < sheetCount; ++i){
            fillDocument(document, wb.getSheetAt(i));
        }
    }

    private void fillDocument(Document document, Sheet sheet) {
        addText(document, sheet.getSheetName());
        int lastRow = sheet.getLastRowNum();
        for(int i = sheet.getFirstRowNum(); i < lastRow; ++i){
            fillDocument(document, sheet.getRow(i));
        }
    }

    private void fillDocument(Document document, Row row) {
        for(Cell cell: row){
            switch(cell.getCellType()){
                case Cell.CELL_TYPE_STRING:
                    addText(document, cell.getStringCellValue());
                    break;
                default:break;
            }
        }
    }

}