package pl.compan.docusafe.lucene.handlers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;

import org.apache.lucene.document.Document;
import org.apache.pdfbox.exceptions.CryptographyException;
import org.apache.pdfbox.exceptions.InvalidPasswordException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.lucene.DocumentHandler;
import pl.compan.docusafe.lucene.FileHandler;
import pl.compan.docusafe.lucene.IndexHelper;

/**
 * This class is used to create a document for the lucene search engine.
 * This should easily plug into the IndexHTML or IndexFiles that comes with
 * the lucene project.  This class will populate the following fields.
 * <table>
 *      <tr>
 *          <th>Lucene - nazwa pola</th>
 *          <th>Opis</th>
 *      </tr>
 *      <tr>
 *          <td>contents</td>
 *          <td>Tre�� dokumentu PDF, indexowana lecz nie przechowywana w indexie</td>
 *      </tr>
 * </table>
 *
 * @version $Revision: 1.00 $
 */
public final class LucenePDFHandler implements DocumentHandler, FileHandler
{
	private static final Logger log = LoggerFactory.getLogger(LucenePDFHandler.class);
    public static final char FILE_SEPARATOR = System.getProperty("file.separator").charAt(0);

    private PDFTextStripper stripper = null;
    private IndexHelper indexHelper;

    public LucenePDFHandler() {
    	indexHelper = new IndexHelper();
    }

    /**
     * Ustawienie odpowiedniego strippera
     *
     * @param aStripper - stripper PDF.
     */
    public void setTextStripper( PDFTextStripper aStripper )
    {
        stripper = aStripper;
    }

    /**
     * Konwersja dokumentu PDF na dokument Lucene.
     *
     * @param is strumie� danych.
     * @return dokument lucene.
     * @throws IOException B��dy podczas konwersji PDF na dokument lucene.
     */
    public Document convertDocument( InputStream is ) throws IOException
    {
        Document document = new Document();
        addContent( document, is);
        return document;
    }

    /**
     * Zaindeksowanie PDF na dokument Lucene.
     *
     * @param file referencja do pliku PDF.
     * @return dokument lucene.
     *
     * @throws IOException .
     */
    public Document convertDocument( File file ) throws IOException
    {
        Document document = new Document();

        FileInputStream input = null;
        try
        {
            input = new FileInputStream(file);
            addContent(document, input);
        }
        finally
        {
            if( input != null )
            {
                input.close();
            }
        }

        return document;
    }

    /**
     * Metoda zwraca dokument PDF z pliku.
     *
     * @param file strumie� pliku PDF.
     * @return lucene document.
     * @throws IOException
     */
    public Document getDocument( InputStream is ) throws IOException
    {
        LucenePDFHandler converter = new LucenePDFHandler();
        return converter.convertDocument( is );
    }

    /**
     * Metoda zwraca dokument PDF z pliku.
     *
     * @param file plik PDF.
     * @return lucene document.
     * @throws IOException
     */
    public Document getDocument( File file ) throws IOException
    {
    	LucenePDFHandler converter = new LucenePDFHandler();
        return converter.convertDocument( file );
    }

    /**
     * Metoda indeksuje odpowiedni plik PDF (podany jako InputStream) do odpowiedniego documentu Lucene.
     *
     * @param document dokument do kt�rego zostanie zaindeksowana tre��.
     * @param is strumie� z tre�ci�.
     *
     * @throws IOException exception podczas parsowania dokumentu.
     */
    private void addContent( Document document, InputStream is) throws IOException
    {
        PDDocument pdfDocument = null;
        try
        {
            pdfDocument = PDDocument.load(is);
            
            // je�eli dokument jest zahas�owany
            if( pdfDocument.isEncrypted() )
            {
                pdfDocument.decrypt( "" );
            }

            StringWriter writer = new StringWriter();
            if( stripper == null )
            {
                stripper = new PDFTextStripper();
            }
            else
            {
                stripper.resetEngine();
            }
            // wyci�gni�cie tekstu z PDFa i zapisanie do Writera
            stripper.writeText( pdfDocument, writer );
            // wyci�gni�cie tekstu z writera
            String contents = writer.getBuffer().toString();
            StringReader reader = new StringReader( contents );
            // zaindeksowanie pola
            indexHelper.addTextField( document, "content", reader );
        }
        catch( CryptographyException e )
        {
        	log.error(e.getMessage(),e);
            throw new IOException( "Error decrypting document: " + e );
        }
        catch( InvalidPasswordException e )
        {
        	log.error("",e);
            //they didn't suppply a password and the default of "" was wrong.
            throw new IOException(
                "Error: The document is encrypted and will not be indexed." );
        }
        finally
        {
            if( pdfDocument != null )
            {
                pdfDocument.close();
            }
        }
    }
}
