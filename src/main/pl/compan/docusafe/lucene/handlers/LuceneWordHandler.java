package pl.compan.docusafe.lucene.handlers;

import java.io.IOException;
import java.io.InputStream;

import org.apache.lucene.document.Document;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.lucene.DocumentHandler;
import pl.compan.docusafe.lucene.IndexHelper;
import pl.compan.docusafe.lucene.exception.DocumentHandlerException;

/**
 * Klasa indeksuj�ca pliki Word z u�yciem POI
 */
public class LuceneWordHandler implements DocumentHandler {
	private static final Logger log = LoggerFactory.getLogger(LuceneWordHandler.class);
	public Document getDocument(InputStream is) throws DocumentHandlerException, IOException {
		POIFSFileSystem fs = null;
		WordExtractor we = null;
		try {
			fs = new POIFSFileSystem(is);
			HWPFDocument doc = new HWPFDocument(fs);
			we = new WordExtractor(doc);
		} catch (Exception e) {
			log.error("", e);
			throw new DocumentHandlerException("Nie mo�na zaindeksowa� pliku word!");
		}
		
		if (we != null && we.getText() != null  && we.getText().trim().length()>0) {
			IndexHelper ih = new IndexHelper();
			Document doc = new Document();
			ih.addTextField(doc, IndexHelper.CONTENT_FIELD, we.getText());
			return doc;
		}
		return null;
	}
}