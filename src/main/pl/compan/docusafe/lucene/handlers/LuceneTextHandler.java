package pl.compan.docusafe.lucene.handlers;

import org.apache.commons.io.IOUtils;
import org.apache.lucene.document.Document;
import pl.compan.docusafe.lucene.exception.DocumentHandlerException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Klasa obs�uguj�ca zwyk�e pliki tekstowe - zak�ada kodowanie UTF-8
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class LuceneTextHandler extends BaseDocumentHandler {
    private final static Logger LOG = LoggerFactory.getLogger(LuceneTextHandler.class);

    public Document getDocument(InputStream is) throws DocumentHandlerException, IOException {
        String text = IOUtils.toString(is, "utf-8");
        Document document = new Document();
        addText(document, text);
        return document;
    }
}