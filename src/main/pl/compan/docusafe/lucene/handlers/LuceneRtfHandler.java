package pl.compan.docusafe.lucene.handlers;

import org.apache.lucene.document.Document;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.rtf.RTFParser;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import pl.compan.docusafe.lucene.exception.DocumentHandlerException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.*;

/**
 * Klasa indeksuj�ca pliki RTF
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class LuceneRtfHandler extends BaseDocumentHandler{
    private final static Logger LOG = LoggerFactory.getLogger(LuceneRtfHandler.class);

    public Document getDocument(InputStream is) throws DocumentHandlerException, IOException {
        CharacterContentHandler handler = new CharacterContentHandler();
        RTFParser parser = new RTFParser();
        try {
            parser.parse(is, handler, new Metadata(), new ParseContext());
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return handler.document;
    }
}