package pl.compan.docusafe.lucene.handlers;

import org.apache.lucene.document.Document;
import org.apache.poi.hslf.extractor.PowerPointExtractor;
import pl.compan.docusafe.lucene.exception.DocumentHandlerException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class LucenePowerPointHandler extends BaseDocumentHandler {
    private final static Logger LOG = LoggerFactory.getLogger(LucenePowerPointHandler.class);

    public Document getDocument(InputStream is) throws DocumentHandlerException, IOException {
        LOG.info("power point handler");
        Document doc = new Document();

        addText(doc, new PowerPointExtractor(is).getText());
        return doc;
    }
}