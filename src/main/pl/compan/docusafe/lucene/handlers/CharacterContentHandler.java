package pl.compan.docusafe.lucene.handlers;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import pl.compan.docusafe.lucene.IndexHelper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Bardzo prymitywna implementacja s�u��ca do wyci�gania tekstu przez apache tika
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
class CharacterContentHandler implements ContentHandler {
    private static final Logger LOG = LoggerFactory.getLogger(CharacterContentHandler.class);

    Document document = new Document();

    public void setDocumentLocator(Locator locator) {}
    public void startDocument() throws SAXException {}
    public void endDocument() throws SAXException {}
    public void startPrefixMapping(String prefix, String uri) throws SAXException {}
    public void endPrefixMapping(String prefix) throws SAXException {}
    public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {}
    public void endElement(String uri, String localName, String qName) throws SAXException {}
    public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {}
    public void processingInstruction(String target, String data) throws SAXException {}
    public void skippedEntity(String name) throws SAXException {}

    public void characters(char[] ch, int start, int length) throws SAXException {
        addText(document, String.copyValueOf(ch, start, length));
    }

    protected final void addText(Document document, String text){
        LOG.info("adding text : '{}'", text);
        document.add( new Field( IndexHelper.CONTENT_FIELD, text, Field.Store.NO, Field.Index.ANALYZED ) );
    }
}
