package pl.compan.docusafe.lucene.handlers;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import pl.compan.docusafe.lucene.DocumentHandler;
import pl.compan.docusafe.lucene.IndexHelper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Klasa bazowa obs�uguj�ce indeksowanie tre�ci
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public abstract class BaseDocumentHandler implements DocumentHandler{
    private final static Logger LOG = LoggerFactory.getLogger(BaseDocumentHandler.class);

    /**
     * Metoda dodaje tekst do zindeksowania
     * @param document
     * @param text
     */
    protected final void addText(Document document, String text){
        LOG.info("adding text to content field : '{}'", text);
        document.add( new Field( IndexHelper.CONTENT_FIELD, text, Field.Store.NO, Field.Index.ANALYZED ) );
    }
}
