package pl.compan.docusafe.lucene;

import java.io.IOException;
import org.apache.lucene.document.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.lucene.exception.DocumentHandlerException;

public class ExtensionDocumentHandler  {
	private static final Logger log = LoggerFactory.getLogger(ExtensionDocumentHandler.class);
    public static final String FILENAME_EXTENSION_KEY = "originalFilenameExtension";
    private IndexHelper indexHelper;
	
	public ExtensionDocumentHandler() {
		indexHelper = new IndexHelper();
	}

	public Document getDocument(AttachmentRevision ar) throws DocumentHandlerException, IOException {
		String ext = getAddsHandlerKey(ar.getMime());
        if(ext == null){
            return null;
        }
		String handlerClassName = Docusafe.getAdditionProperty(ext);

		Document document = new Document();

		if (handlerClassName != null) {
			// zaindeksowanie tre�ci
			try {
				Class handlerClass = Class.forName(handlerClassName);
				DocumentHandler docHandler = (DocumentHandler)handlerClass.newInstance();
				document = docHandler.getDocument(ar.getBinaryStream());
			} catch (Exception e) {
				log.error(e.getMessage(),e);
			}		
		}
        String filename = ar.getOriginalFilename();
		indexHelper.addKeywordField(document, "originalFilename", filename);
        if(filename.contains(".")){
            indexHelper.addKeywordField(document, FILENAME_EXTENSION_KEY, filename.substring(filename.lastIndexOf(".") + 1));
        }
		return document;
	}

    /**
     * Zwraca klucz addsa handlera, kt�ry obs�uguje dany typ plik�w
     * @param mime mime pliku, nie mo�e by� null
     * @return klucz addsa lub null gdy nie istnieje
     */
	private String getAddsHandlerKey(String mime) {
        log.info("mime {}", mime);
		if (mime.contains("pdf") || mime.contains("PDF")){
			return "fulltextSearch.lucenePDF";
        }
        if(mime.contains("opendocument")){
            return "fulltextSearch.opendocument";
        }
		if (mime.contains("msword") || mime.contains("MSWORD")){
			return "fulltextSearch.luceneWord";
        }
        if(mime.contains("ms-excel")){
            return "fulltextSearch.luceneExcel";
        }
        if(mime.contains("ms-powerpoint")){
            return "fulltextSearch.lucenePowerPoint";
        }
        if(mime.contains("application/rtf")){
            return "fulltextSearch.luceneRtf";
        }
        if(mime.contains("text")){
            return "fulltextSearch.luceneText";
        }
		return null;
	}
}
