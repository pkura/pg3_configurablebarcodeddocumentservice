package pl.compan.docusafe.boot.datasource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;
import javax.sql.DataSource;
/**
 * Klasa odpowiedzialna za zrobienie testow puli polaczen
 * @author wkutyla
 *
 */
public class DataSourceTester 
{
	static final Logger log = LoggerFactory.getLogger(DataSourceTester.class);
	
	static private Map<Integer,TesterThread> threads = new HashMap<Integer,TesterThread>();
	
	
	private Integer threadCount = 10;
	public void test(Properties dsProperties,DataSource dataSource)
	{
		try
		{
			threadCount = new Integer(dsProperties.getProperty("poolProvider.threadCount"));
			
		}
		catch (Exception e)
		{
		}
		log.info("ilosc watkow = {}",threadCount);
		try
		{
			long timeA = System.currentTimeMillis();
			log.info("start testu");
			for (int i = 1;i<=threadCount;i++)
			{
				TesterThread t = new TesterThread();
				t.setThreadNumber(i);
				t.setDataSource(dataSource);
				addThread(t);
				t.start();
				log.trace("wystartowano watek {}",i);
			}
			while (countThreads()>0)
			{
				Thread.sleep(100);
			}
			long timeB = System.currentTimeMillis();
			long duration = timeB - timeA;
			log.info("stop testu - czas trwania = {}",duration);
			
		}
		catch (Exception e)
		{
			log.error("",e);
		}
	}
	
	public synchronized static void addThread(TesterThread thread)
	{
		threads.put(thread.getThreadNumber(), thread);
	}
	
	public synchronized static void removeThread(TesterThread thread)
	{
		threads.remove(thread.getThreadNumber());
	}
	
	public synchronized static int countThreads()
	{
		return threads.size();
	}
}
