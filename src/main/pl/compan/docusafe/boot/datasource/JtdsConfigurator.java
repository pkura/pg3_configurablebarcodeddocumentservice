package pl.compan.docusafe.boot.datasource;

import java.util.Properties;

import javax.sql.DataSource;

import net.sourceforge.jtds.jdbcx.JtdsDataSource;

/**
 * Klasa odpowiedzialna za przygotowanie puli dla Drivera Microsoft
 * SQLJDBC
 * @author wkutyla
 *
 */
public class JtdsConfigurator implements DataSourceConfigurator 
{
	static String driverClass = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	public DataSource initDataSource(Properties dsProperties)
	{
		net.sourceforge.jtds.jdbcx.JtdsDataSource dataSource = new net.sourceforge.jtds.jdbcx.JtdsDataSource();		
//		dataSource.setUser(dsProperties.getProperty("username"));
//		dataSource.setPassword(dsProperties.getProperty("password"));
//	//	dataSource.setPrepareSql(2);
//		dataSource.setServerName("localhost");
//		dataSource.setDatabaseName("bfl");
//		dataSource.setPortNumber(1433);
//	//	dataSource.setURL(dsProperties.getProperty("url"));
//		tuneDataSource(dataSource,dsProperties);
		return dataSource;
	}
	
	protected void tuneDataSource(JtdsDataSource dataSource,Properties dsProperties)
	{
		//dataSource.set
	}
	
	public void setDriverClass(String driverClass)
	{
		//nic nie robimy, driver jest ustalony
	}
}
