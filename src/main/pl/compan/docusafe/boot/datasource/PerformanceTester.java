package pl.compan.docusafe.boot.datasource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.DSApi;
import java.sql.*;
import pl.compan.docusafe.core.office.InOfficeDocument;
/**
 * Pomocnicza klasa wykonujaca testy wybranych obszarow systemu
 * @author wkutyla
 *
 */
public class PerformanceTester 
{
	static final Logger log = LoggerFactory.getLogger(PerformanceTester.class);
	public void test()
	{
		log.info("test wydajnosci start");
		PreparedStatement ps = null;		
		try
		{
			DSApi.openAdmin();
			ps = DSApi.context().prepareStatement("select id from dso_in_document");
			ResultSet rs = ps.executeQuery();
			int licznik = 0;
			long timeA = System.currentTimeMillis(); 
			while (rs.next() && licznik < 10000)
			{				
				long id = rs.getLong("id");
				InOfficeDocument inDocument = (InOfficeDocument) DSApi.context().session().load(InOfficeDocument.class, id);
				inDocument.getBarcode();
				long duration = System.currentTimeMillis() - timeA;
				log.trace(""+licznik+" - {}: wyciagam dokument {}", duration,id);
				licznik++;
			}
			rs.close();
		}
		catch (Exception e)
		{
			log.error("",e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
			DSApi._close();
		}
		log.info("test wydajnosci stop");
	}
}
