package pl.compan.docusafe.boot.datasource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.*;
import org.apache.commons.dbutils.DbUtils;
import javax.sql.DataSource;
public class TesterThread extends Thread
{
	static final Logger log = LoggerFactory.getLogger(DataSourceTester.class);
	
	private Integer threadNumber;	
	private DataSource dataSource;
	
	

	@Override
	public void run()
	{
		Connection con = null;
		try
		{
			log.trace("watek {} wystartowal",threadNumber);
			long a = System.currentTimeMillis();
			con = getDataSource().getConnection();
			long b = System.currentTimeMillis();
			long duration = b-a;
			log.trace("nawiazano polaczenie {} [ms]",duration);
			doTest(con);
			long c = System.currentTimeMillis();
			duration = c-b;
			log.trace("po operacjach - trwalo to {} [ms]",duration);
			log.trace("watek {} skonczyl prace",threadNumber);
			DataSourceTester.removeThread(this);
		}
		catch (Exception e)
		{
			log.error("",e);			
		}
		finally
		{
			DbUtils.closeQuietly(con);
		}
	}

	protected void doTest(Connection con) throws SQLException
	{
		PreparedStatement ps = null;
		try
		{
			ps = con.prepareStatement("select * from ds_document");
			ps.execute();
			ps.close();
			Thread.sleep(1000);			
		}
		catch (InterruptedException e) 
		{
			log.error("",e);
		}		
		finally
		{
			DbUtils.closeQuietly(ps);
		}
	}
	public Integer getThreadNumber() {
		return threadNumber;
	}

	public void setThreadNumber(Integer threadNumber) {
		this.threadNumber = threadNumber;
	}
	
	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
