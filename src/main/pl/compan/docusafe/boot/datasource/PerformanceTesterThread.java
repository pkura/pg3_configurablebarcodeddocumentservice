package pl.compan.docusafe.boot.datasource;

public class PerformanceTesterThread extends Thread 
{
	@Override
	public void run()
	{
		PerformanceTester tester = new PerformanceTester();
		tester.test();
	}
}
