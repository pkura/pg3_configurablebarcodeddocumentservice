package pl.compan.docusafe.boot.datasource;

import java.sql.DriverManager;
import java.util.Properties;
import java.util.Map;
import java.util.HashMap;
import javax.sql.DataSource;

import oracle.jdbc.driver.OracleDriver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Klasa odpowiedzialan za konfiguracje obiektow Datasource
 * @author wkutyla
 *
 */
public class DataSourceFactory
{
	static final Logger log = LoggerFactory.getLogger(DataSourceFactory.class);
	static Map<String,DataSourceConfigurator> configurators = null;
	
	public void setUp()
	{
		try
		{
			configurators = new HashMap<String,DataSourceConfigurator>();
			configurators.put("default", new DbcpConfigurator());
			configurators.put("jtds", new JtdsConfigurator());
			configurators.put("dbcp", new DbcpConfigurator());
			configurators.put("microsoft", new MsSqlConfigurator());
			com.microsoft.sqlserver.jdbc.SQLServerDriver driver = new com.microsoft.sqlserver.jdbc.SQLServerDriver();
			DriverManager.registerDriver(driver);
			OracleDriver Driver = new OracleDriver();
        	DriverManager.registerDriver(Driver);
		}
		catch (Exception e)
		{
			log.error("",e);
		}
	}
	public DataSource initDataSource(String driverClass, Properties dsProperties)
	{
		DataSourceConfigurator configurator = null;
		String provider = dsProperties.getProperty("poolProvider");
		if (provider==null)
		{
			provider = "default";
		}
		provider = provider.toLowerCase();
		configurator = configurators.get(provider);
		if (configurator == null)
		{
			configurator = configurators.get("default");
		}
		configurator.setDriverClass(driverClass);
		DataSource resp = configurator.initDataSource(dsProperties);
		if(resp instanceof net.sourceforge.jtds.jdbcx.JtdsDataSource)
		{
			//((net.sourceforge.jtds.jdbcx.JtdsDataSource)resp).setPrepareSql(2);
		}
		
		if ("true".equalsIgnoreCase(dsProperties.getProperty("poolProvider.doTest")))
		{
			DataSourceTester tester = new DataSourceTester();
			tester.test(dsProperties,resp);
			//test wykonamy dwa razy - aby zobaczyc czasy drugiego nawiazania polaczen
			tester.test(dsProperties,resp);
		}
		return resp;
	}
}
