package pl.compan.docusafe.boot.datasource;

import java.util.Properties;
import javax.sql.DataSource;

/**
 * Obiekty odpowiedzialne za inicjowanie puli polaczen
 * @author wkutyla
 *
 */
public interface DataSourceConfigurator 
{
	/**
	 * Inicjowanie puli polaczen
	 * @param dsProperties
	 * @return
	 */
	public DataSource initDataSource(Properties dsProperties); 
	
	/**
	 * Driver JDBC
	 * @param driverClass
	 */
	public void setDriverClass(String driverClass);
}
