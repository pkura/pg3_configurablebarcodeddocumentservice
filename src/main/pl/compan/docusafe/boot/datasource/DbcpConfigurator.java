package pl.compan.docusafe.boot.datasource;

import org.apache.commons.dbcp.BasicDataSource;
import java.util.Properties;
import javax.sql.DataSource;
/**
 * Klasa odpowiedzialna za zainicjowanie puli polaczen
 * Przeniesiona z klasy Docusafe
 * #TODO Umozliwia ustawienie wielu innych parametrow dotyczacych puli
 * 
 * @author wkutyla
 *
 */
public class DbcpConfigurator implements DataSourceConfigurator
{
	/**
	 * Nazwa drivera
	 */
	private String driverClass;
	
	/**
	 * Startowa wielkosc puli. Domyslna wartosc to 0
	 */
	public static final String DBCP_INITIAL_SIZE = "dbcp.initialSize";
	/**
	 * Maksymalna liczba aktywnych polaczen - domyslne jest to 8
	 */
	public static final String DBCP_MAX_ACTIVE = "dbcp.maxActive";
	
	/**
	 * Maksymalna liczba nieuzywanych polaczen - domyslnie jest to 8
	 */
	public static final String DBCP_MAX_IDLE = "dbcp.maxIdle";
	/**
	 * Minimalna liczba nieuzywanych polaczen
	 */
	public static final String DBCP_MIN_IDLE = "dbcp.minIdle";
	
	/**
	 * The maximum number of milliseconds that the pool will wait (when there are no available connections) for a 
	 * connection to be returned before throwing an exception, or -1 to wait indefinitely.
	 * Default: -1 
	 */
	public static final String DBCP_MAX_WAIT = "dbcp.maxWait";
	
	/**
	 * The SQL query that will be used to validate connections from this pool before returning them to the caller. 
	 * If specified, this query MUST be an SQL SELECT statement that returns at least one row. 
	 */
	public static final String DBCP_VALIDATION_QUERY = "dbcp.validationQuery";
	
	/**
	 * The indication of whether objects will be validated before being borrowed from the pool. If the object fails to validate, 
	 * it will be dropped from the pool, and we will attempt to borrow another.
	 * NOTE - for a true value to have any effect, the validationQuery parameter must be set to a non-null string. 
	 */
	public static final String DBCP_TEST_ON_BORROW = "dbcp.testOnBorrow";
	
	/**
	 * The indication of whether objects will be validated before being returned to the pool.
	 * NOTE - for a true value to have any effect, the validationQuery parameter must be set to a non-null string. 
	 */
	public static final String DBCP_TEST_ON_RETURN = "dbcp.testOnReturn";
	
	/**
	 * The indication of whether objects will be validated by the idle object evictor (if any). If an object fails to validate, it will be dropped from the pool.
	 * NOTE - for a true value to have any effect, the validationQuery parameter must be set to a non-null string. 
	 */
	public static final String DBCP_TEST_WHILE_IDLE = "dbcp.testWhileIdle";
	
	/**
	 * The number of milliseconds to sleep between runs of the idle object evictor thread. 
	 * When non-positive, no idle object evictor thread will be run. 
	 * Default -1
	 */
	public static final String DBCP_TIME_BETWEEN_EVICTION_RUNS_MILLIS = "dbcp.timeBetweenEvictionRunsMillis";
	
	/**
	 * The number of objects to examine during each run of the idle object evictor thread (if any).
	 * DEFAULT = 3
	 */
	public static final String DBCP_NUM_TESTS_PER_EVICTION_RUN = "dbcp.numTestsPerEvictionRun";
	
	/**
	 *  The minimum amount of time an object may sit idle in the pool before it is eligable for eviction by the idle object evictor (if any). 
	 *  DEFAULT 30 minut
	 */
	public static final String DBCP_MIN_EVICTABLE_IDLE_TIME_MILLIS = "dbcp.minEvictableIdleTimeMillis";
	
	/**
	 *  false  	Enable prepared statement pooling for this pool.
	 */
	public static final String DBCP_POOL_PREPARED_STATEMENTS = "dbcp.poolPreparedStatements";
	
	/**
	 * unlimited  	 The maximum number of open statements that can be allocated from the statement pool at the same time, or zero for no limit.
	 */
	public static final String DBCP_MAX_OPEN_PREPARED_STATEMENTS = "dbcp.maxOpenPreparedStatements";
	
	/**
	 *  	false  	Controls if the PoolGuard allows access to the underlying connection.
	 */
	public static final String DBCP_ACCESS_TO_UNDERLYING_CONNECTION_ALLOWED = "dbcp.accessToUnderlyingConnectionAllowed";
	
	/**
	 * false  	 Flag to remove abandoned connections if they exceed the removeAbandonedTimout.
	 * If set to true a connection is considered abandoned and eligible for removal if it has been idle longer than the removeAbandonedTimeout. 
	 * Setting this to true can recover db connections from poorly written applications which fail to close a connection. 
	 */
	public static final String DBCP_REMOVE_ABANDONED = "dbcp.removeAbandoned";
	
	/**
	 * 300  	Timeout in seconds before an abandoned connection can be removed.
	 */
	public static final String DBCP_REMOVE_ABANDONED_TIMEOUT = "dbcp.removeAbandonedTimeout";
	
	/**
	 * false  	 Flag to log stack traces for application code which abandoned a Statement or Connection.
	 * Logging of abandoned Statements and Connections adds overhead for every Connection open or new Statement because a stack trace has to be generated. 
	 */
	public static final String DBCP_LOG_ABANDONED = "dbcp.logAbandoned";
	/**
	 * Metoda inicjuje pule polaczen
	 * @param dsProperties
	 * @return
	 */
	public DataSource initDataSource(Properties dsProperties) 
	{
		BasicDataSource dataSource;
		

		dataSource = new BasicDataSource();
		dataSource.setDriverClassName(driverClass);
		dataSource.setUsername(dsProperties.getProperty("username"));
		dataSource.setPassword(dsProperties.getProperty("password"));
		dataSource.setUrl(dsProperties.getProperty("url"));
		
		tuneDataSource(dataSource,dsProperties);
		
		return dataSource;
	}

	
	/**
	 * Wykonywany jest tuning - ustawienia parametrow w zaleznosci od
	 * @param ds
	 * @param dsProperties
	 */
	protected void tuneDataSource(BasicDataSource ds, Properties dsProperties)
	{
		String val = dsProperties.getProperty(DBCP_INITIAL_SIZE);
		if (val!=null)
		{
			ds.setInitialSize(Integer.valueOf(val));
		}
		val = dsProperties.getProperty(DBCP_MAX_ACTIVE);
		if (val!=null)
		{
			ds.setMaxActive(Integer.valueOf(val));
		}
		val = dsProperties.getProperty(DBCP_MAX_IDLE);
		if (val!=null)
		{
			ds.setMaxIdle(Integer.valueOf(val));
		}
		val = dsProperties.getProperty(DBCP_MIN_IDLE);
		if (val!=null)
		{
			ds.setMinIdle(Integer.valueOf(val));
		}
		val = dsProperties.getProperty(DBCP_MAX_WAIT);
		if (val!=null)
		{
			ds.setMaxWait(Long.valueOf(val));
		}
		ds.setValidationQuery(dsProperties.getProperty(DBCP_VALIDATION_QUERY));
		
		val = dsProperties.getProperty(DBCP_TEST_ON_BORROW);
		if (val!=null)
		{
			ds.setTestOnBorrow(Boolean.valueOf(val));
		}
		val = dsProperties.getProperty(DBCP_TEST_ON_RETURN);
		if (val!=null)
		{
			ds.setTestOnReturn(Boolean.valueOf(val));
		}
		val = dsProperties.getProperty(DBCP_TEST_WHILE_IDLE);
		if (val!=null)
		{
			ds.setTestWhileIdle(Boolean.valueOf(val));
		}
		val = dsProperties.getProperty(DBCP_TIME_BETWEEN_EVICTION_RUNS_MILLIS);
		if (val!=null)
		{
			ds.setTimeBetweenEvictionRunsMillis(Long.valueOf(val));
		}
		val = dsProperties.getProperty(DBCP_NUM_TESTS_PER_EVICTION_RUN);
		if (val!=null)
		{
			ds.setNumTestsPerEvictionRun(Integer.valueOf(val));
		}
		val = dsProperties.getProperty(DBCP_MIN_EVICTABLE_IDLE_TIME_MILLIS);
		if (val!=null)
		{
			ds.setMinEvictableIdleTimeMillis(Long.valueOf(val));
		}
		val = dsProperties.getProperty(DBCP_POOL_PREPARED_STATEMENTS);
		if (val!=null)
		{
			ds.setPoolPreparedStatements(Boolean.valueOf(val));
		}
		val = dsProperties.getProperty(DBCP_MAX_OPEN_PREPARED_STATEMENTS);
		if (val!=null)
		{
			ds.setMaxOpenPreparedStatements(Integer.valueOf(val));
		}
		val = dsProperties.getProperty(DBCP_ACCESS_TO_UNDERLYING_CONNECTION_ALLOWED);
		if (val!=null)
		{
			ds.setAccessToUnderlyingConnectionAllowed(Boolean.valueOf(val));
		}
		val = dsProperties.getProperty(DBCP_REMOVE_ABANDONED);
		if (val!=null)
		{
			ds.setRemoveAbandoned(Boolean.valueOf(val));
		}
		val = dsProperties.getProperty(DBCP_REMOVE_ABANDONED_TIMEOUT);
		if (val!=null)
		{
			ds.setRemoveAbandonedTimeout(Integer.valueOf(val));
		}
		val = dsProperties.getProperty(DBCP_LOG_ABANDONED);
		if (val!=null)
		{
			ds.setLogAbandoned(Boolean.valueOf(val));
		}
	}
	
	
	
	
	
	
	
	
	public String getDriverClass() {
		return driverClass;
	}

	public void setDriverClass(String driverClass) {
		this.driverClass = driverClass;
	}
	
	
}
