package pl.compan.docusafe.boot.datasource;

import java.util.Properties;

import javax.sql.DataSource;
import com.microsoft.sqlserver.jdbc.SQLServerConnectionPoolDataSource;

/**
 * Klasa odpowiedzialna za przygotowanie puli dla Drivera Microsoft
 * SQLJDBC
 * @author wkutyla
 *
 */
public class MsSqlConfigurator implements DataSourceConfigurator 
{
	static String driverClass = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	public DataSource initDataSource(Properties dsProperties)
	{
		SQLServerConnectionPoolDataSource dataSource = new SQLServerConnectionPoolDataSource();		
		dataSource.setUser(dsProperties.getProperty("username"));
		dataSource.setPassword(dsProperties.getProperty("password"));
		dataSource.setURL(dsProperties.getProperty("url"));
		tuneDataSource(dataSource,dsProperties);
		return dataSource;
	}
	
	protected void tuneDataSource(SQLServerConnectionPoolDataSource dataSource,Properties dsProperties)
	{
		//dataSource.set
	}
	
	public void setDriverClass(String driverClass)
	{
		//nic nie robimy, driver jest ustalony
	}
}
