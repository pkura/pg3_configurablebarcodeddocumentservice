package pl.compan.docusafe.boot;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl> 16.07.14
 */
public class DSFileSystemXmlApplicationContext extends FileSystemXmlApplicationContext {
    /**
     * Create a new FileSystemXmlApplicationContext for bean-style configuration.
     *
     * @see #setConfigLocation
     * @see #setConfigLocations
     * @see #afterPropertiesSet()
     */
    public DSFileSystemXmlApplicationContext() {
    }

    /**
     * Create a new FileSystemXmlApplicationContext for bean-style configuration.
     *
     * @param parent the parent context
     * @see #setConfigLocation
     * @see #setConfigLocations
     * @see #afterPropertiesSet()
     */
    public DSFileSystemXmlApplicationContext(ApplicationContext parent) {
        super(parent);
    }

    /**
     * Create a new FileSystemXmlApplicationContext, loading the definitions
     * from the given XML file and automatically refreshing the context.
     *
     * @param configLocation file path
     * @throws org.springframework.beans.BeansException if context creation failed
     */
    public DSFileSystemXmlApplicationContext(String configLocation) throws BeansException {
        super(configLocation);
    }

    /**
     * Create a new FileSystemXmlApplicationContext, loading the definitions
     * from the given XML files and automatically refreshing the context.
     *
     * @param configLocations array of file paths
     * @throws org.springframework.beans.BeansException if context creation failed
     */
    public DSFileSystemXmlApplicationContext(String... configLocations) throws BeansException {
        super(configLocations);
    }

    /**
     * Create a new FileSystemXmlApplicationContext with the given parent,
     * loading the definitions from the given XML files and automatically
     * refreshing the context.
     *
     * @param configLocations array of file paths
     * @param parent          the parent context
     * @throws org.springframework.beans.BeansException if context creation failed
     */
    public DSFileSystemXmlApplicationContext(String[] configLocations, ApplicationContext parent) throws BeansException {
        super(configLocations, parent);
    }

    /**
     * Create a new FileSystemXmlApplicationContext, loading the definitions
     * from the given XML files.
     *
     * @param configLocations array of file paths
     * @param refresh         whether to automatically refresh the context,
     *                        loading all bean definitions and creating all singletons.
     *                        Alternatively, call refresh manually after further configuring the context.
     * @throws org.springframework.beans.BeansException if context creation failed
     * @see #refresh()
     */
    public DSFileSystemXmlApplicationContext(String[] configLocations, boolean refresh) throws BeansException {
        super(configLocations, refresh);
    }

    /**
     * Create a new FileSystemXmlApplicationContext with the given parent,
     * loading the definitions from the given XML files.
     *
     * @param configLocations array of file paths
     * @param refresh         whether to automatically refresh the context,
     *                        loading all bean definitions and creating all singletons.
     *                        Alternatively, call refresh manually after further configuring the context.
     * @param parent          the parent context
     * @throws org.springframework.beans.BeansException if context creation failed
     * @see #refresh()
     */
    public DSFileSystemXmlApplicationContext(String[] configLocations, boolean refresh, ApplicationContext parent) throws BeansException {
        super(configLocations, refresh, parent);
    }

    @Override
    protected Resource getResourceByPath(String path) {
        return new FileSystemResource(path);
    }
}
