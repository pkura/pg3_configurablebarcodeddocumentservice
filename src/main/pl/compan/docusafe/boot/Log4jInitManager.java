package pl.compan.docusafe.boot;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.io.IOException;
/** 
 * Klasa odpowiedzialna za inicjowanie log4ja.
 * @author wkutyla
 *
 */
public class Log4jInitManager 
{
	public static void initLog4j(File home,File log4jproperties) throws IOException
	{
        Properties props = null; 
        props = new Properties();

        new File(home, "logs").mkdir();
        props.load(Docusafe.class.getClassLoader().getResourceAsStream("log4j.properties"));
        props.put("log4j.appender.upgrade_log.File","${pl.com-pan.docusafe.home}/logs/upgrade."+Docusafe.getDistNumber()+".log");
        if (log4jproperties.exists())
        {
            InputStream is = new FileInputStream(log4jproperties);
            props.load(is);
        }
        org.apache.log4j.PropertyConfigurator.configure(props);
	}
}
