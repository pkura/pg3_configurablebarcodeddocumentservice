package pl.compan.docusafe.boot;

import org.apache.commons.beanutils.DynaBean;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.el.ELContext;
import javax.el.ELResolver;
import java.beans.FeatureDescriptor;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * ELResolver dla obiekt�w DynaBean, z jakich� wzgl�d�w przesta�y dzia�a� po podbiciu wersji
 * Servlet�w i jsp
 */
public class DynaBeanELResolver extends ELResolver{
    private final static Logger LOG = LoggerFactory.getLogger(DynaBeanELResolver.class);
    @Override
    public Class<?> getCommonPropertyType(ELContext elContext, Object o) {
        return Object.class;
    }

    @Override
    public Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext elContext, Object o) {
        return new ArrayList<FeatureDescriptor>().iterator();
    }

    @Override
    public Class<?> getType(ELContext elContext, Object object, Object property) {
//        LOG.warn("trying getType");
        if(object instanceof DynaBean){
            elContext.setPropertyResolved(true);
            DynaBean b = (DynaBean) object;
            Object value = b.get(object.toString());
            if(value != null){
                return value.getClass();
            } else {
                return Object.class;
            }
        } else {
            return null;
        }
    }

    @Override
    public Object getValue(ELContext elContext, Object object, Object property) {
//        LOG.warn("trying getValue");
        if(object instanceof DynaBean){
            elContext.setPropertyResolved(true);
            DynaBean b = (DynaBean) object;
            return b.get(property.toString());
        } else {
            return null;
        }
    }

    @Override
    public boolean isReadOnly(ELContext elContext, Object object, Object property) {
//        LOG.warn("trying isReadOnly");
        if(object instanceof DynaBean){
            elContext.setPropertyResolved(true);
        }
        return false;
    }

    @Override
    public void setValue(ELContext elContext, Object object, Object property, Object valueToSet) {
//        LOG.warn("trying isReadOnly");
        if(object instanceof DynaBean){
            elContext.setPropertyResolved(true);
            DynaBean b = (DynaBean) object;
            b.set(property.toString(), valueToSet);
        }
    }
}
