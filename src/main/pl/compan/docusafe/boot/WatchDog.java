package pl.compan.docusafe.boot;

import java.util.Iterator;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.opensymphony.webwork.ServletActionContext;

/**
 * Klasa ktorej zadaniem jest kontrola czasu wykonywania 
 * @author wkutyla
 *
 * Przyk�ad uzycia 
 * watchDog = new WatchDog("kod")
 * watchDog.tick();
 * Jesli przekraczamy czas to jest to wypisywane w logu
 */


public class WatchDog
{
	private static final Logger log = LoggerFactory.getLogger("pl.docusafe.performance");
	public final static long DEFAULT_ALERT_TIME = 1500;
	public final static long MEGA_ALERT_MULTIPLIER = 4;
	/**
	 * Czas utworzenia czujki
	 */
	private long startTime;
	/**
	 * Wyliczony czas alertu
	 */
	private long alertTime;
	
	private long megaAlertTime;
	private String messageCode;
	
	/**
	 * Jesli tru to moge wyciagnac dane metoda specyficzna dla webworks
	 */
	private boolean webworkAction = true;
	
	
	/**
	 * Zrzuca wartosc parametrow akcji w sytuacji RED ALERT
	 */
	protected void dumpParameters()
	{
		if (log.isTraceEnabled() && webworkAction)
		{
			try
			{
				log.trace("parametry:");			
				Map params = ServletActionContext.getRequest().getParameterMap();
				Iterator itp = params.keySet().iterator();
				while (itp.hasNext())
				{
	    			String key = (String) itp.next();
	    			Object val = params.get(key);
	    			log.trace("{}={}",key,val.toString());
				}
			}
			catch (Exception e)
			{
				log.debug("",e);
			}
		}
	}
	/**
	 * Metoda sprawdza czas wykonania operacji od poczatku
	 * i zapisuje czas debugu. Jesli czas jest dluzszy niz alert to zapisujemy w trybie error
	 */
	public void tick()
	{
		long now = System.currentTimeMillis();
		long duration = now - startTime;		
		String kom = messageCode + ";"+duration;
		if (now>megaAlertTime)
		{
			log.error("RED ALERT!!!");
			dumpParameters();
		}
		if (now>alertTime)
		{
			log.error(kom);
		}
		else
		{
			log.debug(kom);
		}
	}
	
	/**
	 * Konstruktor
	 * @param alertTimeInMillis
	 */
	public WatchDog(long alertTimeInMillis)
	{
		super();		
		startTime = System.currentTimeMillis();
		alertTime = startTime + alertTimeInMillis;
		megaAlertTime = startTime + alertTimeInMillis * MEGA_ALERT_MULTIPLIER;
	}
	
	
	public WatchDog()
	{		
		this(DEFAULT_ALERT_TIME);
		
	}
			
	public WatchDog(String code)
	{
		this();
		messageCode = code;		
	}
	
	public WatchDog(String code, long alertTimeInMillis)
	{
		this(alertTimeInMillis);
		messageCode = code;		
	}

	
	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public boolean isWebworkAction() {
		return webworkAction;
	}
	public void setWebworkAction(boolean webworkAction) {
		this.webworkAction = webworkAction;
	}
}
