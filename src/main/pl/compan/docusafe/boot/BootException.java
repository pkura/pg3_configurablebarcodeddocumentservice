package pl.compan.docusafe.boot;
/* User: Administrator, Date: 2005-09-06 10:59:32 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: BootException.java,v 1.1 2005/09/06 16:32:49 lk Exp $
 */
public class BootException extends RuntimeException
{
    public BootException(String message)
    {
        super(message);
        Docusafe.printk("B��d inicjalizacji "+message);
    }

    public BootException(String message, Throwable cause)
    {
        super(message, cause);
        Docusafe.printk("B��d inicjalizacji "+message+" ("+cause+")");
        cause.printStackTrace();
    }
}
