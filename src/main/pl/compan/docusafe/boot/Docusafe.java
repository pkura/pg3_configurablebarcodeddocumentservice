package pl.compan.docusafe.boot;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.dom4j.Element;
import org.jbpm.JbpmConfiguration;
import org.jbpm.JbpmContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import pl.compan.docusafe.boot.datasource.DataSourceFactory;
import pl.compan.docusafe.boot.upgrade.Upgrade;
import pl.compan.docusafe.common.Modules;
import pl.compan.docusafe.common.license.License;
import pl.compan.docusafe.common.license.LicenseVerificationException;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.attachments.DwrAttachmentStorage;
import pl.compan.docusafe.core.fop.Fop;
import pl.compan.docusafe.core.jbpm4.MigrationWorkflow;
import pl.compan.docusafe.core.office.DSPermission;
import pl.compan.docusafe.core.office.workflow.TasklistSynchro;
import pl.compan.docusafe.core.office.workflow.internal.InternalWorkflowService;
import pl.compan.docusafe.core.repository.RepositoryProvider;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.auth.UserPrincipal;
import pl.compan.docusafe.core.users.sql.SqlUserFactory;
import pl.compan.docusafe.process.DocuSafeProcessDefinition;
import pl.compan.docusafe.service.ServiceException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.agent.AgentManager;
import pl.compan.docusafe.util.BundleStringManager;
import pl.compan.docusafe.util.StreamUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.web.common.Sitemap;
import pl.compan.docusafe.web.filter.AuthFilter;

import javax.naming.InitialContext;
import javax.security.auth.Subject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.sql.DataSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.security.Security;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.google.common.base.Preconditions.checkState;

/**
 * Kernel.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Docusafe.java,v 1.127 2010/08/13 11:02:12 mariuszk Exp $
 */
public class Docusafe
{
    private static final Logger log = LoggerFactory.getLogger(Docusafe.class);
    public static final String DOCUSAFE_LOGIN_CONTEXT_NAME = "DocuSafe";
    // runlevele oznaczaj� stan, w jaki aplikacja w�a�nie wchodzi
    // lub w jakim jest

    public static final int RL_NONE = 0;
    /**
     * Odczyt parametr�w z serwera aplikacji, pobranie kontekstu
     * aplikacji.
     */
    public static final int RL_AS_INIT = 1;
    /**
     * Odczyt parametr�w z pliku WAR, niezale�nych od konkretnej
     * instalacji.  Inicjalizacja tych danych, kt�re mog� by�
     * zainicjalizowane bez licencji i innych plik�w z katalogu
     * domowego.
     */
    public static final int RL_WAR_INIT = 2;
    /**
     * Inicjalizacja nowej instancji aplikacji.
     */
    public static final int RL_NEWINSTANCE_INIT = 3;
    /**
     * Odczyt konfiguracji z katalogu domowego.
     */
    public static final int RL_HOME_INIT = 4;
    public static final int RL_LICENSE_INIT = 5;
    /**
     * Modu� aktualizuj�cy aplikacj� (baz� danych, katalog domowy,
     * LDAP i inne wymagane elementy).  Przed Hibernate.  Mo�e
     * wymusza� restart (RL_RESTART).
     */
    /**
     * Inicjalizacja �r�de� danych.
     */
    public static final int RL_DATASOURCE_INIT = 6;

    public static final int RL_UPGRADE = 7;

    /**
     * Oznacza, �e serwer aplikacji wymaga restartu.
     */
    public static final int RL_RESTART = 8;
    public static final int RL_HIBERNATE_INIT = 9;
    public static final int RL_USERFACTORY_INIT = 10;
    /**
     * Inicjalizacja danych wymaganych przez aplikacj�, np. fonty
     * dla FOP, Hibernate, uprawnienia, fabryka u�ytkownik�w,
     * JAAS, mailing (stary), workflow (internal), us�ugi,
     * velocity.
     */
    public static final int RL_CORE_INIT = 11;
    public static final int RL_WORKFLOW_INIT = 12;
    public static final int RL_SERVICES_INIT = 13;
    /**
     * Ostatni poziom uruchomienia, aplikacja dzia�a.
     */
    public static final int RL_RUNNING = 14;

    public static final String DB_FIREBIRD_1_5 = "firebird_1.5";
    public static final String DB_SQLSERVER2000 = "sqlserver_2000";
    public static final String DB_SQLSERVER2008 = "sqlserver_2008";
    public static final String ORACLE_9 = "oracle_9";
    public static final String POSTGRESQL = "postgreSQL";


    /**
     * Domy�lne sterowniki dla u�ywanych baz danych.
     */
    private static final Map<String, String> DB_DRIVERS = new HashMap<String, String>();
    static
    {
        DB_DRIVERS.put(DB_FIREBIRD_1_5, "org.firebirdsql.jdbc.FBDriver");
        DB_DRIVERS.put(DB_SQLSERVER2008, "com.microsoft.sqlserver.jdbc.SQLServerDriver");
        DB_DRIVERS.put(DB_SQLSERVER2000, "net.sourceforge.jtds.jdbc.Driver");
        DB_DRIVERS.put(ORACLE_9,"oracle.jdbc.driver.OracleDriver");
        DB_DRIVERS.put(POSTGRESQL,"org.postgresql.Driver");
    
    }

    public static final String TEMP_FOLDER_NAME_FOR_PNG_FILES = "tempPng";


    private static Exception exception;
    private static int runlevel;

    private static ServletContext servletContext;
    /**Zwraca kontekst aplikacji (czyli np: docusafe)*/
    public static String pageContext;
    private static LinkedHashSet<HttpSession> sessions = new LinkedHashSet<HttpSession>();
    private static File home;
    private static String pngTempFile;
    private static File temp;
    
    /**
     * Indeksy Lucene
     */
    private static Directory indexes;
    /**
     * Pliki z indekasami Lucene.
     */
    private static File indexesFiles;
    
    private static String version;
    private static int distNumber;
    private static String distTime;
    private static long distTimestamp;
    private static String buildId;
    private static License license;
    private static Properties properties;
    private static Properties additions;
    private static Map<String, Properties> additionsForDockind;
    private static Set<String> moduleSet;
    private static Set<String> extraSet;
    /**
     * Bazowy adres aplikacji odczytany z pliku HOME/docusafe.config.
     * B�dzie zast�piony ustawieniem w Preferences.
     */
    private static String baseUrl;

    // obiekty u�ywane tylko na etapie uruchamiania
    private static Properties docusafeProperties;

    private static String hibernateDialectClass;
    private static DataSource dataSource;
    private static String uploadPath;


    private static String mailFromEmail;
    private static String mailFromPersonal;

    private static String ldapRoot;
    // TODO: we wdro�eniach aplikacji dystrybuowanej nale�y u�y� rozproszonego cache
    private static Map<String, Object> attributes = new HashMap<String, Object>();

    private static boolean verificationLibraryLoaded = false;


    private static JbpmConfiguration jbpmConfiguration = null;

    /**
     * Czy ma byc wykonywany test wydajnosci bazy
     */
    private static boolean doDatabaseTest = false;

    /**
     * Springowy application context, inicjalizowany poprzez DocuSafeWebApplicationInitializer
     **/
    static ApplicationContext applicationContext;

    //private static Preferences indivdualPrefs = Preferences.systemNodeForPackage(Docusafe.class);

    /**
     * Bootstrap uruchamia kolejne fazy inicjalizacji kernela.
     * Wyj�tek w kt�rejkolwiek z nich powoduje fatalny b��d
     * i aplikacja nie zostanie uruchomiona.
     * <p>
     * Je�eli faza initHome() stwierdzi, �e nie istnieje katalog
     * domowy, proces jest zawieszany (metoda wait()) i oczekuje
     * na powstanie katalogu domowego.
     * Filtr RunlevelWatchingFilter sprawdza, czy powinien przekierowa�
     * do inicjalizacji nowej instancji aplikacji.  Robi to na podstawie
     * statusu ustawianego w kernelu (nie runlevelu).
     *
     */
    private static class Bootstrap extends Thread
    {
        public Bootstrap()
        {
            super("Docusafe.Bootstrap");
            setDaemon(false);
        }

        public void run()
        {
            try
            {
//                initAS();
//                initWar();
//                initHome();
//                initCheckDate();
//                initLicense();
//                initAvailable();
//                initDatasource();
                
                //zamienilem kolejnosc - upgrade bedzie po datasourcie, ale przed inicjowaniem hbm
                upgrade();
                initHibernate();
                initRepositories();
                initJBPM();
                initUserFactory();
                initCore();
                pl.compan.docusafe.core.datamart.DataMartManager.init();
                initDockinds();
                initJbpm4();
                initWorkflow();
                initServices();
                //tu takze wystartujemy watek testujacy
                if (doDatabaseTest)
                {
                	log.debug("startujemy test");
                	pl.compan.docusafe.boot.datasource.PerformanceTesterThread t = new pl.compan.docusafe.boot.datasource.PerformanceTesterThread();
                	t.start();
                }
                //initDockinds();
                
                setRunlevel(RL_RUNNING);
            }
            catch (Throwable e)
            {
                Docusafe.getServletContext().log(e.getMessage(), e);
                log.error(e.getMessage(), e);
                printk(e.toString());
                exception = new BootException(e.getMessage(), e);
            }
        }
    }
    private static void initRepositories() throws EdmException {
        RepositoryProvider.init();
    }

    static void initAvailable() throws Exception
    {
    	AvailabilityManager.initAdds();
        StringManager.initHomeLocalStringsProperties();
    }
    
    static void initDockinds() throws Exception
    {
    	try
        {
            DSApi.openAdmin();
        	for(DocumentKind dk:DocumentKind.list())
        	{
        		//System.out.println(dk.getName());
        		//DocumentKind dk = DocumentKind.findByCn(s);
        		dk.initialize();
        	}
        }
        catch (Exception e) 
        {
        	e.printStackTrace();
        	throw new BootException(e.getMessage(), e);
		} finally {
            DSApi._close();
        }
    }

    /**
     * Uruchamia proces inicjalizacji kernela.
     * @throws IllegalStateException Je�eli metoda start zosta�a ju�
     * wcze�niej wywo�ana.
     */
    static synchronized void start(ServletContext servletContext)
    {
        if (servletContext == null)
            throw new NullPointerException("servletContext");
        if (Docusafe.servletContext != null)
            throw new IllegalStateException("Kernel zosta� ju� uruchomiony");

        Random r = new Random();
        Docusafe.servletContext = servletContext;
        try{
        	//servletContext.
	        String path = servletContext.getResource("/").getPath();
			path = path.substring(0, path.lastIndexOf("/"));
			path = path.substring(path.lastIndexOf("/"));

	        uploadPath = path.replace("/", "_");
	        pageContext = path.substring(1);

            initAS();
            initWar();
            initHome();
            initCheckDate();
            initLicense();
            initAvailable();
            initDatasource();
        }
        catch(Exception e )
        {
        	uploadPath = "docusafe";
        	pageContext = uploadPath;
        	printk("Docusafe nie moze znalezc contextPath. Nie powinna dzialac wiecej niz jedna istancja na serwer");
        }
        //System.out.println(uploadPath);
        printk("Uruchamianie aplikacji");
        new Bootstrap().start();
    }

    static synchronized void stop() throws ServiceException
    {
        ServiceManager.destroy();
        AgentManager.destroy();
        /** zapisywanie nieznalezionych kluczy jezykowych do pliku*/
        BundleStringManager.dropMissingProperties();
    }

    /**
     * Wznawia uruchamianie aplikacji.  Uruchamianie mo�e zosta� przerwane
     * przed etapem RL_HOME_INIT, gdy nie istnieje katalog domowy.
     * Proces uruchamiania wchodzi w�wczas na poziom RL_NEWINSTANCE_INIT,
     * rozpoczynaj�c oczekiwanie na monitorze obiektu Docusafe.class.
     * Po otrzymaniu sygna�u (notify) uruchamianie jest wznawiane, o ile
     * istnieje katalog domowy.
     * <p>
     * Metod� mo�na wywo�a� tylko wtedy, gdy {@link #getRunlevel()} zwraca
     * warto�� {@link #RL_NEWINSTANCE_INIT}.
     */
    public static void resumeBoot()
    {
        synchronized (Docusafe.class)
        {
            if (runlevel != RL_NEWINSTANCE_INIT)
                throw new IllegalStateException("Proces uruchamiania aplikacji " +
                    "mo�na wznowi� tylko na poziomie RL_NEWINSTANCE_INIT");

            printk("Wznawianie startu aplikacji");
            Docusafe.class.notify();
        }
    }

    /**
     * Zmienia poziom uruchamiania.  Nie mo�na przej�� z poziomu wy�szego
     * na ni�szy.
     */
    private static void setRunlevel(int rl)
    {
        if (rl < runlevel)
            throw new IllegalArgumentException("Nie mo�na przej�� na poziom "+
                "ni�szy od bie��cego");

        runlevel = rl;

        printk("Poziom "+getRunlevelName(rl)+" ("+rl+")");
    }

    public static String getRunlevelName(int rl)
    {
        Field[] fields = Docusafe.class.getFields();
        for (int i=0; i < fields.length; i++)
        {
            if (fields[i].getName().startsWith("RL_") &&
                fields[i].getType() == int.class)
            {
                try
                {
                    Integer iRL = (Integer) fields[i].get(null);
                    if (iRL.intValue() == rl)
                    {
                        return fields[i].getName();
                    }
                }
                catch (IllegalAccessException e)
                {
                    throw new Error(e.getMessage(), e);
                }
            }
        }
        throw new IllegalArgumentException("Nieznana warto�� poziomu: "+rl);
    }

    static void initCheckDate() throws Exception
    {
    	java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd");
    	java.util.Date criticalDate = format.parse("2088-01-01");
    	Date actualDate = new Date();
    	if(actualDate.after(criticalDate))
    		throw new Exception("B��dna data systemu "+format.format(actualDate));
    }

    /**
     * Pobranie ustawie� z konfiguracji serwera aplikacyjnego.
     * Obecnie pobierany jest tylko parametr homeDirectory, w
     * przysz�o�ci na przyk�ad ustawienia load-balancingu.
     */
    static void initAS()
    {
        setRunlevel(RL_AS_INIT);

        printk(getJVMString());

        String homeDirectory = servletContext.getInitParameter("homeDirectory");
        if (homeDirectory == null || homeDirectory.trim().length() == 0)
        {
            throw new BootException("W konfiguracji aplikacji nie znaleziono "+
                "parametru \"homeDirectory\"");
        }
        home = new File(homeDirectory);

        printk("Katalog domowy: "+home.getAbsolutePath());


        // wyb�r katalogu na pliki tymczasowe; najpierw sprawdzany jest
        // katalog okre�lony atrybutem java.io.tmpdir, nast�pnie podejmowana
        // jest pr�ba utworzenia pliku tymczasowego i odczytu katalogu,
        // w kt�rym zosta� utworzony
        File tmp = null;
        try
        {
            if (! (System.getProperty("java.io.tmpdir") != null &&
                    (temp = new File(System.getProperty("java.io.tmpdir"))) != null &&
                    ((temp.isDirectory() && temp.canWrite()) || temp.mkdirs())) &&
                ! ( (temp = (tmp = File.createTempFile("docusafe_temp", null)).getParentFile()) != null &&
                    ((temp.isDirectory() && temp.canWrite()) || temp.mkdirs())))
                throw new RuntimeException("Nie mo�na znale�� ani utworzy� katalogu " +
                    "na pliki tymczasowe");
        }
        catch (IOException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
        finally
        {
            if (tmp != null) tmp.delete();
        }



        printk("Security.addProvider(new BouncyCastleProvider())");
        Security.addProvider(new BouncyCastleProvider());

        printk("Katalog na pliki tymczasowe: "+temp.getAbsolutePath());

        // Log4j
        org.apache.log4j.BasicConfigurator.resetConfiguration();

    }

    static void initWar()
    {
        setRunlevel(RL_WAR_INIT);

        try
        {
            // marketingowy numer wersji (np. "2005XP-super-hiper" :-) )
            version = StreamUtils.toString(Docusafe.class.
                getClassLoader().getResourceAsStream("version.txt")).trim();

            // kolejny numer dystrybucji
            Properties distNumberProperties = new Properties();
            distNumberProperties.load(Docusafe.class.
                getClassLoader().getResourceAsStream("dist.number"));
            distNumber = Integer.parseInt(distNumberProperties.getProperty("build.number"));

            // czas kompilacji
            Properties distTimeProperties = new Properties();
            distTimeProperties.load(Docusafe.class.getClassLoader().getResourceAsStream("buildtime.txt"));
            distTime = distTimeProperties.getProperty("BuildTime");
            
            try {
            	distTimestamp = Long.parseLong(distTimeProperties.getProperty("BuildTimestamp", Long.toString(System.currentTimeMillis())));
            } catch(NumberFormatException e) {
            	distTimestamp = System.currentTimeMillis();
            }

            // parametry �rodowiska, w kt�rym wykonano kompilacj�
            Properties buildIdProperties = new Properties();
            buildIdProperties.load(Docusafe.class.getClassLoader().getResourceAsStream("buildid.txt"));
            buildId = buildIdProperties.getProperty("buildid");
        }
        catch (IOException e)
        {
            throw new BootException("B��d podczas odczytu numeru wersji", e);
        }

        printk("Wersja "+version+"."+distNumber);

        try
        {
            properties = new Properties();
            properties.load(Docusafe.class.getClassLoader().
                getResourceAsStream("docusafe.properties"));
        }
        catch (IOException e)
        {
            throw new BootException("B��d podczas odczytu docusafe.properties", e);
        }

        try
        {
            additions = new Properties();
            additions.load(Docusafe.class.getClassLoader().
                getResourceAsStream("adds.properties"));
        }
        catch (IOException e)
        {
            throw new BootException("B��d podczas odczytu adds.properties", e);
        }
    }

    /**
     * Czeka na powstanie katalogu domowego.
     */
    static void initHome()
    {
        boolean log4j = false;

        // je�eli katalog domowy nie istnieje, aplikacja wchodzi
        // na poziom RL_NEWINSTANCE_INIT i czeka, a� katalog
        // domowy powstanie;  wznowienie pracy aplikacji nast�pi
        // dopiero po wywo�aniu metody resumeBoot()
        while (!home.exists())
        {
            setRunlevel(RL_NEWINSTANCE_INIT);

            if (!log4j)
            {
                DateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
                File logFile = new File("docusafe-init.log."+df.format(new Date()));
                Properties props = new Properties();
                props.setProperty("log4j.rootLogger", "INFO, R");
                props.setProperty("log4j.appender.R", "org.apache.log4j.FileAppender");
                props.setProperty("log4j.appender.R.File", logFile.getAbsolutePath());
                props.setProperty("log4j.appender.R.layout", "org.apache.log4j.PatternLayout");
                props.setProperty("log4j.appender.R.layout.ConversionPattern", "%d{dd MMM yyyy HH:mm:ss} %-5p (%t) %c - %m%n");
                props.setProperty("log4j.logger.org.apache.struts", "INFO");
                props.setProperty("log4j.logger.org.apache.commons", "INFO");
                props.setProperty("log4j.logger.org.hibernate", "INFO");
                printk("Docusafe: Konfiguracja logowania do "+logFile.getAbsolutePath());
                org.apache.log4j.PropertyConfigurator.configure(props);
                log4j = true;
            }

            try
            {
                synchronized (Docusafe.class)
                {
                    Docusafe.class.wait();
                }
            }
            catch (InterruptedException e)
            {
            }
        }

        setRunlevel(RL_HOME_INIT);

        if (!home.isDirectory())
        {
            throw new BootException(home.getAbsolutePath()+" nie jest katalogiem");
        }

        if (!home.canRead())
        {
            throw new BootException("Brak uprawnie� do odczytu katalogu " +
                "domowego "+home.getAbsolutePath());
        }
        
        // inicjacja indeks�w lucynki
        try
        {
        	System.out.println("Tworze katalogu dla indexow Lucynki:)");
	        indexesFiles = new File(home, "indexes");
	        if(!indexesFiles.exists()||!indexesFiles.isDirectory())
	        	indexesFiles.mkdir();
	        // otwarcie indeks�w lucene
	        indexes = FSDirectory.open(indexesFiles);
        }
        catch(Exception e)
        {
        	throw new BootException(e.getMessage(), e);
        }
        
        // z tej w�asno�ci korzysta m.in. Log4j
        System.setProperty("pl.com-pan.docusafe.home", home.getAbsolutePath());

        // Log4j

        File log4jproperties = new File(home, "log4j.properties");

        try
        {
        	Log4jInitManager.initLog4j(home, log4jproperties);
        }
        catch (IOException e)
        {
            throw new BootException(e.getMessage(), e);
        }

        // zapisywanie w katalogu domowym pliku z numerem wersji
        try
        {
            System.out.println("zapisywanie w katalogu domowym pliku z numerem wersji");
            FileWriter writer = new FileWriter(new File(home, "version.txt"));
            writer.write(getVersion()+"."+getDistNumber());
            writer.close();
        }
        catch (IOException e)
        {
            throw new BootException("Nie mo�na zapisa� pliku version.txt w katalogu "+home, e);
        }

        // ustawienia aplikacji
        File docusafeConfigFile = new File(home, "docusafe.config");
        try
        {
            docusafeProperties = loadPropertiesFile(docusafeConfigFile);
            // TODO: przenie�� do Preferences
            baseUrl = docusafeProperties.getProperty("base.url");
        }
        catch (IOException e)
        {
            throw new BootException("Nie mo�na odczyta� pliku "+
                docusafeConfigFile.getAbsolutePath());
        }

        initAdds();

        try
        {
        	File cache = null;
	        do
	        {
	            cache = new File(Docusafe.getHome(), TEMP_FOLDER_NAME_FOR_PNG_FILES);
	            // znaleziono ju� istniej�ce cache
	            if (cache.isDirectory() && new File(cache, "viewserver").isFile())
	                break;

	            if (!cache.exists() && !cache.mkdirs())
	            {
	                throw new EdmException("Nie mo�na utworzy� katalogu "+cache);
	            }
	            else
	            {
	                new File(cache, "png").createNewFile();
	                break;
	            }
	        }
	        while (true);
            pngTempFile = cache.getPath();
        }
        catch (Exception e)
        {
        	log.error("", e);
        }
    }
    
    public static void initAdds()
    {
        // adds.properties z HOME'a
        File addsPropertiesFile = new File(home, "adds.properties");
        try
        {
            if (addsPropertiesFile.exists())
            {
                FileInputStream fis = new FileInputStream(addsPropertiesFile);
                additions.load(fis);
                org.apache.commons.io.IOUtils.closeQuietly(fis);
            }
        }
        catch (IOException e)
        {
            System.out.println("B��d podczas czytania pliku "+
                addsPropertiesFile.getAbsolutePath());
        }
    }

    /**
     * Wczytuje adds dla dockinda
     * @param rootElement
     * @param cn
     */
    public static void initAddsForDockind(Element rootElement, String cn) {
        if(additionsForDockind == null) {
            additionsForDockind = new HashMap<String, Properties>();
        }
        if(rootElement != null) {
            List<Element> addsElements = rootElement.elements("additional");
            if(addsElements != null) {
                Properties dockindProp = new Properties();
                for(Element el : addsElements) {
                    dockindProp.setProperty(el.attributeValue("name"), el.attributeValue("value"));
                }
                additionsForDockind.put(cn, dockindProp);
            }
        }
    }

    static void initLicense()
    {
        setRunlevel(RL_LICENSE_INIT);

        // licencja
        File licenseFile = new File(home, "license.class");
        if (!licenseFile.exists())
            throw new BootException("Nie znaleziono pliku licencji ("+licenseFile.getAbsolutePath()+")");

        try
        {
            license = License.load(licenseFile);
        }
        catch (LicenseVerificationException e)
        {
            throw new BootException("Nieprawid�owy plik licencji ("+licenseFile.getAbsolutePath()+")", e);
        }

        moduleSet = new HashSet<String>();

        String[] moduleNames = license.getModules();
        for (int i=0; i < moduleNames.length; i++)
        {
            moduleSet.add(moduleNames[i]);
        }

        // zale�no�ci pomi�dzy modu�ami
        if (moduleSet.contains(Modules.MODULE_INVOICES) ||
            moduleSet.contains(Modules.MODULE_CONTRACTS) ||
            moduleSet.contains(Modules.MODULE_GRANTS) ||
            moduleSet.contains(Modules.MODULE_CONSTRUCTION))
        {
            moduleSet.add(Modules.MODULE_REGISTRIES);
        }

        if (moduleSet.contains(Modules.MODULE_SIMPLEOFFICE) || moduleSet.contains(Modules.MODULE_OFFICE) ||
            moduleSet.contains(Modules.MODULE_REGISTRIES) || moduleSet.contains(Modules.MODULE_FAX))
        {
            moduleSet.add(Modules.MODULE_COREOFFICE);
        }

        extraSet = new HashSet<String>();

        String[] extra = license.getExtras();
        if (extra != null)
        {
            for (int i=0; i < extra.length; i++)
            {
                extraSet.add(extra[i]);
            }
        }

        printk("Licencja dla: "+license.getCustomer()+//", nr: "+license.getLicenseNo()+
            ", modu�y: "+license.getModulesAsString(", ")+", "+moduleSet+
            ", extras: "+extraSet);
    }

    static void initDatasource()
    {
        setRunlevel(RL_DATASOURCE_INIT);

        try
        {
            File dsConfig = new File(home, "datasource.config");
            if (!dsConfig.exists())
                throw new BootException("Nie istnieje plik "+
                    dsConfig.getAbsolutePath());

            Properties dsProperties = loadPropertiesFile(dsConfig);

            String databaseType = dsProperties.getProperty("database.type");
            
            if(dsProperties.getProperty("database.jndiresource") != null)
            {
            	printk("laczymy JNDI z resurce o nazwie: " + dsProperties.getProperty("database.jndiresource"));
            	javax.naming.Context initContext = new InitialContext();
                javax.naming.Context envContext  = (javax.naming.Context)initContext.lookup("java:/comp/env");
                dataSource = (javax.sql.DataSource)envContext.lookup(dsProperties.getProperty("database.jndiresource"));
                if (dataSource==null)
                    throw new BootException("Nieznane zrodlo danych");
            }
            else
            {	            
	            // Warunek dla wersji Docusafe starszych ni� 75
	            if (databaseType == null)
	            {
	                printk("w pliku "+dsConfig.getAbsolutePath()+" brakuje " +
	                    "parametru database.type, przyjmowanie warto�ci domy�lnej " +
	                    "("+DB_FIREBIRD_1_5+")");
	                databaseType = DB_FIREBIRD_1_5;
	            }
	            else if (DB_DRIVERS.get(databaseType) == null)
	            {
	                throw new BootException("Nieznany rodzaj bazy danych: "+databaseType);
	            }
	
	            String driverClass = dsProperties.getProperty("driver.class");
	            if (driverClass!=null)
	            {
	            	driverClass = driverClass.trim();
	            }
	
	            if (driverClass == null)
	            {
	                driverClass = (String) DB_DRIVERS.get(databaseType);
	                printk("W pliku "+dsConfig.getAbsolutePath()+" brakuje " +
	                    "parametru driver.class, przyjmowanie warto�ci domy�lnej ("+
	                    driverClass+")");
	
	            }
	            DataSourceFactory dsFactory = new DataSourceFactory();
	            dsFactory.setUp();
	            dataSource  = dsFactory.initDataSource(driverClass, dsProperties);
            }
            
            hibernateDialectClass = getHibernateDialectClass(databaseType);

            /*
            -- ten kawalek jest juz stary
            DataSourceInitManager dsInit = new DataSourceInitManager();
            dsInit.setDriverClass(driverClass);


            if (Boolean.valueOf(getConfigProperty("debug.sql", "false")).booleanValue())
                dsInit.setDebugSQL(true);
            dataSource = dsInit.initDataSource(dsProperties);
            */
            String testProp = dsProperties.getProperty("database.performance.test");
            log.debug("testProperty = {}",testProp);
            if ("true".equalsIgnoreCase(testProp))
            {
            	log.debug("doDataBaseTest=true");
            	doDatabaseTest = true;
            }
          }
        catch (IOException e)
        {
            throw new BootException(e.getMessage(), e);
        }
        catch (Exception e)
        {
            throw new BootException(e.getMessage(), e);
        }
    }

    static void initHibernate()
    {
        setRunlevel(RL_HIBERNATE_INIT);

        try
        {
            DSApi.reloadHibernate();
            //DSApi.testHibernate();
        }
        catch (EdmException e)
        {
            throw new BootException(e.getMessage(), e);
        }
    }

    static void initJbpm4()
    {
        try
        {
            DSApi.initJbpm4();
            if(AvailabilityManager.isAvailable("migrationWorkflow"))
            {
            	Thread thread = new Thread(new MigrationWorkflow());
	            thread.start(); 	
            }
            if(Configuration.isAdditionOn(Configuration.ADDITION_RELOAD_TASKLIST_ON_BOOT))
            {
                try
                {
                    final DSContext ctx = DSApi.openAdmin();
                    ctx.begin();
                    TasklistSynchro.Synchro(ctx);
                    ctx.commit();
                    log.info("reloadTasklistOnBoot zakonczyl dzialanie");
                }
                catch (Exception e)
                {
                    log.error("Wystapil wyjatek podczas procesu reloadTasklistOnBoot",e);
                    DSApi.context().setRollbackOnly();
                    servletContext.log("B��d inicjalizacji workflow reloadTasklistOnBoot", e);
                    throw new BootException(e.getMessage(), e);
                }
                finally
                {
                	DSApi._close();
                }
         	   // synchronizacja snapshota
            }
        }
        catch (EdmException e)
        {
            throw new BootException(e.getMessage(), e);
        }
    }

    /**
     * Nie korzystamy z tego modulu workflow
     * @throws EdmException
     */
    @Deprecated
    static void initJBPM() throws EdmException
    {
        if (Configuration.workflowAvailable())
        {
            printk("Inicjalizacja JBPM");
            jbpmConfiguration = JbpmConfiguration./*parseXmlString(DSApi.getHibernateJbpmCfg());*/getInstance();

            // Testowanie po��czenia
            JbpmContext jbpmContext = jbpmConfiguration.createJbpmContext();


            jbpmContext.close();

            printk("Pomyslnie zainicjalizowano JBPM");
        }
    }

    static void initUserFactory()
    {
        setRunlevel(RL_USERFACTORY_INIT);

        String factoryName = docusafeProperties.getProperty("user.factory");

        if (!SqlUserFactory.NAME.equals(factoryName))
            throw new BootException("W docusafe.config nie znaleziono parametru " +
                "user.factory");

        UserFactory.setFactory(SqlUserFactory.NAME);
        try
        {
			DSApi.openAdmin();
//			DSApi.context().begin();
//	    	if(AvailabilityManager.isAvailable("reloadSubstitute.onStart"))
//	    		SqlUserFactory.reloadSubstitute();
//			DSApi.context().commit();
			DSApi.close();
        }
        catch (EdmException e)
        {
            throw new BootException(e.getMessage(), e);
        }
    }

    static void initCore()
    {
        setRunlevel(RL_CORE_INIT);

        try
        {
            Sitemap.read(servletContext.getResourceAsStream("/WEB-INF/freemap.xml"));
        }
        catch (EdmException e)
        {
            throw new BootException(e.getMessage(), e);
        }

        try
        {
            Velocity.init();
        }
        catch (Exception e)
        {
            throw new BootException(e.getMessage(), e);
        }

        // FOP
        try
        {
            Fop.initFonts(new File(home, "fonts"));
        }
        catch (Exception e)
        {
            throw new BootException(e.getMessage(), e);
        }

        // JAAS (login config)

        File loginConfig = new File(home, "login.config");
        // TODO: tworzy� plik domy�lny
        if (!loginConfig.exists())
            throw new BootException("Nie znaleziono pliku "+
                loginConfig.getAbsolutePath());

        System.setProperty("java.security.auth.login.config", "file:"+loginConfig.getAbsolutePath());

        // mailing

        File mailProperties = new File(home, "mail.config");
        if (!mailProperties.exists())
            throw new BootException("Nie znaleziono pliku "+
                mailProperties.getAbsolutePath());

        FileInputStream mailPropertiesInputStream = null;
        try
        {
            Properties props = new Properties();
            mailPropertiesInputStream = new FileInputStream(mailProperties);
            props.load(mailPropertiesInputStream);

            System.setProperty("mail.smtp.host", props.getProperty("mail.smtp.host"));
            if (props.getProperty("mail.smtp.auth") != null)
                System.setProperty("mail.smtp.auth", props.getProperty("mail.smtp.auth"));
            if (props.getProperty("mail.smtp.username") != null)
                System.setProperty("mail.smtp.username", props.getProperty("mail.smtp.username"));
            if (props.getProperty("mail.smtp.password") != null)
                System.setProperty("mail.smtp.password", props.getProperty("mail.smtp.password"));

            mailFromEmail = props.getProperty("mail.docusafe.from.email");
            mailFromPersonal = props.getProperty("mail.docusafe.from.personal");
        }
        catch (IOException e)
        {
            throw new BootException(e.getMessage(), e);
        }
        finally
        {
            if (mailPropertiesInputStream != null)
                try { mailPropertiesInputStream.close(); } catch (IOException e) { }
        }

        // uprawnienia

        try
        {
            DSPermission.init();
        }
        catch (EdmException e)
        {
            throw new BootException(e.getMessage(), e);
        }

        try { //fragment kodu zak�ada, �e istnieje ju� po��czenie z baz� danych
            applicationContext = new ClassPathXmlApplicationContext("docusafeApplicationContext.xml");
            File customApplicationContext = new File(getHome(), "customApplicationContext.xml");
            //za�aduj dodatkowy plik z HOME'a je�li istnieje
            applicationContext = customApplicationContext.isFile()
                    ? new FileSystemXmlApplicationContext(new String[]{"file:"+customApplicationContext.getAbsolutePath()}, true, applicationContext)
                    : applicationContext;

        } catch (Exception ex) {
            throw new BootException("B��d podczas inicjalizacji Spring application context:" + ex.getMessage(), ex);
        }

        // podpis (biblioteka dll)
        try
        {
            System.loadLibrary("verification");
            verificationLibraryLoaded = true;
        }
        catch (UnsatisfiedLinkError e)
        {
            printk("Blad podczas wczytywania biblioteki verification.dll\n" + e);
        }
    }

    public static void refreshAppliactionContext() {
        if(applicationContext instanceof ClassPathXmlApplicationContext) {
            ((ClassPathXmlApplicationContext) applicationContext).refresh();
        } else if(applicationContext instanceof FileSystemXmlApplicationContext) {
            ((FileSystemXmlApplicationContext) applicationContext).refresh();
        }
    }

    /**
     * Zwraca instancj� danego typu, pobiera j� ze springa,
     * @param clazz typ
     * @return instancja skonfigurowana w springu
     */
    public static <T> T getBean(Class<T> clazz){
        checkState(applicationContext != null, "applicationContext nie jest jeszcze zainicjalizowany");
        return applicationContext.getBean(clazz);
    }

    /**
     * Zwraca instancj� danego typu o danej nazwie, pobiera j� ze springa,
     * @param clazz typ
     * @return instancja skonfigurowana w springu
     */
    public static <T> T getBean(String name, Class<T> clazz){
        checkState(applicationContext != null, "applicationContext nie jest jeszcze zainicjalizowany");
        return applicationContext.getBean(name, clazz);
    }

    public static String[] getBeanNamesForType(Class clazz) {
        return applicationContext.getBeanNamesForType(clazz);
    }

    static void initWorkflow()
    {
        setRunlevel(RL_WORKFLOW_INIT);

        long time = System.currentTimeMillis();

        try
        {
            final DSContext ctx = DSApi.openAdmin();
            ctx.begin();

            if (moduleAvailable(Modules.MODULE_COREOFFICE))
            {
                InternalWorkflowService.initialize(ctx);
            }

            ctx.commit();
            log.info("initWorkflow zakonczyl dzialanie");
        }
        catch (Exception e)
        {
            log.error("Wystapil wyjatek podczas procesu initWorkflow",e);
            DSApi.context().setRollbackOnly();
            servletContext.log("B��d inicjalizacji wewn�trznego workflow", e);
            throw new BootException(e.getMessage(), e);
        }
        finally
        {
            //if(DSApi.isContextOpen())
           // {
                DSApi._close();
           // }
        }
        printk("Workflow: "+(System.currentTimeMillis()-time)+"ms");
    }

    static void initServices()
    {
        setRunlevel(RL_SERVICES_INIT);

        try
        {
            ServiceManager.init();
            AgentManager.initAgentFactory(home);
        }
        catch (ServiceException e)
        {
            throw new BootException(e.getMessage(), e);
        }
    }

    static void upgrade()
    {
        setRunlevel(RL_UPGRADE);

        /*
        Oddzielny modu�, kt�ry wykonuje aktualizacj�.

        Musi odczyta� katalog domowy, parametry bazy danych.  Klasa
        Upgrade udost�pniaj�ca metody pomocnicze klasom uruchamianym.
        Klasa Upgrade sama ��czy si� z baz� danych, aby odczyta� aktualny
        numer wersji.


        */

        Upgrade upgrade = new Upgrade();
        try
        {
            upgrade.start();
        }
        catch (Exception e)
        {
            e.printStackTrace(System.out);
            throw new BootException(e.getMessage(), e);
        }

        // tutaj upgrade aplikacji
    }

    public static void printk(String msg)
    {
        System.out.println("Docusafe: "+msg);
        servletContext.log(msg);
    }

    static void sessionCreated(HttpSessionEvent event)
    {
    	LoggerFactory.getLogger("tomekl").debug("sessionCreated");
        synchronized (sessions)
        {
            sessions.add(event.getSession());
        }
    }

    static void sessionDestroyed(HttpSessionEvent event)
    {
    	LoggerFactory.getLogger("tomekl").debug("sessionDestroyed");
        synchronized (sessions)
        {
            sessions.remove(event.getSession());
        }
        
        //niszczenie autentyfikacji do zalacznikow
        Subject subject = (Subject) event.getSession().getAttribute(AuthFilter.SUBJECT_KEY);
        UserPrincipal principal = AuthUtil.getUserPrincipal(subject);
        String username = principal != null ? principal.getName() : "";
        DSApi.clearCredentials(username);
        
        // usuni�cie sesji z tabeli phpbb
        String sessionTable = Docusafe.getAdditionProperty("phpbb.sessionTable");
        if(sessionTable != null)
        	AuthUtil.destroyPhpbbSession(event.getSession(), sessionTable);
        
        DwrAttachmentStorage.getInstance().removeAll(event.getSession(), null);
    }

    public static Set<HttpSession> getSessions()
    {
        synchronized (sessions)
        {
            return new LinkedHashSet<HttpSession>(sessions);
        }
    }

    /**
     * Zwraca kontekst aplikacji.  Warto�� poprawna w ka�dym momencie.
     */
    public static ServletContext getServletContext()
    {
        return servletContext;
    }

    public synchronized static int getRunlevel()
    {
        return runlevel;
    }

    public static boolean failed()
    {
        return exception != null;
    }

    public static Throwable getThrowable()
    {
        return exception;
    }

    /**
     * Katalog domowy aplikacji.  Warto�� nadawana jest w runlevelu
     * {@link #RL_AS_INIT}, ale do czasu uko�czenia runlevelu
     * {@link #RL_HOME_INIT} nie ma gwarancji, �e ten katalog istnieje.
     */
    public static File getHome()
    {
        return home;
    }

    public static File getTemp()
    {
        return temp;
    }

    /**
     * Zwraca DataSource, metoda wykorzystywana przy tworzeniu kontekstu springa
     * @return
     */
    public static DataSource getDataSource()
    {
        return dataSource;
    }

    public static String getHibernateDialectClass()
    {
        if (runlevel < RL_DATASOURCE_INIT)
            throw new IllegalStateException("Nie mo�na wywo�a� tej metody " +
                "przed uko�czeniem poziomu RL_DATASOURCE_INIT");
        return hibernateDialectClass;
    }

    public static String getHibernateDialectClass(String databaseType)
    {
        if (DB_FIREBIRD_1_5.equals(databaseType))
        {
            return "org.hibernate.dialect.FirebirdDialect";
        }
        else if (DB_SQLSERVER2000.equals(databaseType))
        {
            return "org.hibernate.dialect.SQLServerDialect";
        }
        else if (DB_SQLSERVER2008.equals(databaseType))
        {
            return "org.hibernate.dialect.SQLServerDialect";
        }
        else if(ORACLE_9.equals(databaseType))
        {
            return "org.hibernate.dialect.Oracle10gDialect";
        }
        else if(POSTGRESQL.equals(databaseType))
        {
            return "org.hibernate.dialect.PostgreSQLDialect";
        }
        else
        {
            throw new IllegalArgumentException("Nieznany typ bazy danych: "+
                databaseType);
        }
    }

    public static String getDefaultDriver(String databaseType)
    {
        return (String) DB_DRIVERS.get(databaseType);
    }

    /** Powinno zwracac jedynie property - wycofujemy sie z individual properties
     *
     * @param name
     * @return
     */
    public static String getProperty(String name)
    {
        if (runlevel < RL_WAR_INIT)
            throw new IllegalStateException("Nie mo�na wywo�a� tej metody " +
                "przed uko�czeniem poziomu RL_WAR_INIT");
        /*
        Preferences indivdualPrefs = Preferences.systemNodeForPackage(Docusafe.class);
        String ret = indivdualPrefs.get(name, null);

        ret = ret!=null?ret:properties.getProperty(name);
        return ret==null?"":ret;*/
        return properties.getProperty(name);
    }

    public static void setProperty(String name, String value)
    {
    	if (runlevel < RL_WAR_INIT)
            throw new IllegalStateException("Nie mo�na wywo�a� tej metody " +
                "przed uko�czeniem poziomu RL_WAR_INIT");
    	properties.setProperty(name, value);
    }

    /**
     * Dostajemy propertiesy
     * @return
     */
    public static Properties getProperties()
    {
        if (runlevel < RL_WAR_INIT)
            throw new IllegalStateException("Nie mo�na wywo�a� tej metody " +
                "przed uko�czeniem poziomu RL_WAR_INIT");

        return properties;
    }
    /*
    public static String getIndividualProperty(String propertyName, String defaultValue)
    {
    	Preferences indivdualPrefs = Preferences.systemNodeForPackage(Docusafe.class);
        return indivdualPrefs.get(propertyName, defaultValue);
    }
    */
    
    /**
     * Funkcja s�u�y do pobierania danych z pliku adds.properties
     * 
     * @param name
     * @return 
     */
    public static String getAdditionProperty(String name)
    {
         if (runlevel < RL_WAR_INIT)
         {
        	 log.warn("Proba pobrania wlasnosci {} ponizej poziomu RL_WAR_INIT", name);
        	 return "";
         }
         // nie rzucam wyjatku, bo pojawial sie blad przy tworzeniu WARa
         /*   throw new IllegalStateException("Nie mo�na wywo�a� tej metody " +
                "przed uko�czeniem poziomu RL_WAR_INIT");*/

        return additions.getProperty(name);
    }

    /**
     * Pobiera warto�� adds dla dockinda
     * @param name
     * @param dockindCn
     * @return
     */
    public static String getAdditionProperty(String name, String dockindCn) {
        if(additionsForDockind.containsKey(dockindCn)) {
            return additionsForDockind.get(dockindCn).getProperty(name);
        }

        return getAdditionProperty(name);
    }

    /**
     * Zwraca adds'a o podanym kluczu lub warto�� domy�ln� gdy adds nie zosta� ustawiony
     * @param name
     * @param def
     * @return
     */
    public static String getAdditionPropertyOrDefault(String name, String def) {
        String ret = getAdditionProperty(name);
        return ret != null ? ret : def;
    }

    public static String getAdditionPropertyOrDefault(String name, String dockindCn, String def) {
        String ret = getAdditionProperty(name, dockindCn);
        return ret != null ? ret : def;
    }

    public static int getIntAdditionPropertyOrDefault(String name, int def) {
        int ret;
        try {
            String add = getAdditionProperty(name);
            if(add == null) return def;
            ret = Integer.parseInt(add);
            return ret;
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            return def;
        }
    }

    /**
     * Marketingowy numer wersji.
     */
    public static String getVersion()
    {
        return version;
    }

    /**
     * Kolejny numer dystrybucji.
     */
    public static int getDistNumber()
    {
        return distNumber;
    }

    /**
     * Parametry �rodowiska, w kt�rym wykonano kompilacj�.
     */
    public static String getBuildId()
    {
        return buildId;
    }

    public static String getMailFromPersonal()
    {
        return mailFromPersonal;
    }

    public static String getMailFromEmail()
    {
        return mailFromEmail;
    }

    public static String[] getAvailableModules()
    {
        if (runlevel < RL_LICENSE_INIT)
            throw new IllegalStateException("Nie mo�na wywo�a� tej metody " +
                "przed wej�ciem na poziom RL_LICENSE_INIT");

        return (String[]) moduleSet.toArray(new String[moduleSet.size()]);
    }

    public static String[] getAvailableExtras()
    {
        if (runlevel < RL_LICENSE_INIT)
            throw new IllegalStateException("Nie mo�na wywo�a� tej metody " +
                "przed wej�ciem na poziom RL_LICENSE_INIT");

        return (String[]) extraSet.toArray(new String[extraSet.size()]);
    }

    public static boolean isAvailableExtras(String extras)
    {
    	if (runlevel < RL_LICENSE_INIT)
            throw new IllegalStateException("Nie mo�na wywo�a� tej metody " +
                "przed wej�ciem na poziom RL_LICENSE_INIT");

    	return  (extraSet != null ? extraSet.contains(extras) : false);
    }

    public static boolean hasExtra(String extra)
    {
        if (runlevel < RL_LICENSE_INIT)
            throw new IllegalStateException("Nie mo�na wywo�a� tej metody " +
                "przed wej�ciem na poziom RL_LICENSE_INIT");

        return extraSet.contains(extra);
    }

    /**
     * Testuje dost�pno�� modu�u w danym momencie.  Metoda mo�e by�
     * wywo�ywana dopiero po zako�czeniu uruchamiania na poziomie
     * RL_HOME_INIT (runlevel > RL_HOME_INIT).
     * @param moduleName Nazwa modu�u (jedna ze sta�ych zdefiniowanych
     * w klasie {@link Modules}).
     * @return True, je�eli modu� jest aktywny.
     * @throws IllegalArgumentException Je�eli nazwa modu�u jest niepoprawna
     * lub poziom uruchamiania ni�szy albo r�wny RL_HOME_INIT.
     */
    public static boolean moduleAvailable(String moduleName)
    {
        if (!Modules.modulePossible(moduleName))
            throw new IllegalArgumentException("Niepoprawna nazwa modu�u: "+moduleName);

        if (runlevel <= RL_HOME_INIT)
            throw new IllegalStateException("Metoda mo�e by� wywo�ana dopiero po "+
                "zako�czeniu uruchamiania na poziomie RL_HOME_INIT");

        return moduleSet.contains(moduleName);
    }

    public static License getLicense()
    {
        return license;
    }

    public static String getLicenseString()
    {
        if (runlevel < RL_LICENSE_INIT)
            throw new IllegalStateException("Nie mo�na wywo�a� tej metody " +
                "przed wej�ciem na poziom RL_LICENSE_INIT");

        if (license != null)
        {
            return license.getCustomer()+" ["+license.getModulesAsString(" ")+"] "+
                "["+license.getExtrasAsString(" ")+"]";
        }
        return "[brak licencji]";
    }

    public static String getJVMString()
    {
        return "java.version="+System.getProperty("java.version")+
            " java.vm.version="+System.getProperty("java.vm.version")+
            " java.vm.name=\""+System.getProperty("java.vm.name")+"\""+
            " java.home="+System.getProperty("java.home")+
            " java.class.version="+System.getProperty("java.class.version");
    }

    public static String getIdString()
    {
        return "v"+version+"."+distNumber+//" "+moduleSet+
            " home="+home;
    }

    public static VelocityEngine getVelocityEngine() throws EdmException
    {
        Properties props = new Properties();
        props.setProperty("resource.loader", "cp");
        props.setProperty("cp.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        //props.setProperty("file.resource.loader.class", org.apache.velocity.runtime.resource.loader.FileResourceLoader.class.getName());

        VelocityEngine ve = new VelocityEngine();
        try
        {
            ve.init(props);
        }
        catch (Exception e)
        {
            throw new EdmException("B��d podczas tworzenia VelocityEngine", e);
        }

        return ve;
    }

    public static String getLdapRoot()
    {
        return ldapRoot;
    }

    public static String getBaseUrl()
    {
        return baseUrl;
    }
    
    /**
     * zwraca wartosc bazow� url localhost:8080
     * @return
     */
    public static String getBaseContextUrl()
    {
    	String url = null;

    	try
    	{
	    	if(baseUrl.startsWith("http://"))
	    		url = baseUrl.substring("http://".length());
	    	else 	if(baseUrl.startsWith("https://"))
	    		url = baseUrl.substring("https://".length());
	    	
	    	if(url.indexOf("/") > 0)
	    		url = url.substring(0, url.indexOf("/"));
    	}catch(NullPointerException e){
//    		log.error("",e);
    		return null;
    	}
    	return url;
    }

    public static String getConfigProperty(String name, String defaultValue)
    {
        if (runlevel <= RL_HOME_INIT)
            throw new IllegalStateException("Nie mo�na wywo�a� tej metody " +
                "na poziomie uruchomienia mniejszym lub r�wnym RL_HOME_INIT");

        return docusafeProperties.getProperty(name, defaultValue);
    }

    /* funkcje pomocnicze */

    public static Properties loadPropertiesFile(File file) throws IOException
    {
        FileInputStream fis = new FileInputStream(file);
        Properties props = new Properties();
        props.load(fis);
        org.apache.commons.io.IOUtils.closeQuietly(fis);
        return props;
    }

    public synchronized static Object getAttribute(String name)
    {
        return attributes.get(name);
    }

    public synchronized static void setAttribute(String name, Object value)
    {
        attributes.put(name, value);
    }

    public static boolean getVerificationLibraryLoaded()
    {
        return verificationLibraryLoaded;
    }

    public static JbpmConfiguration getJbpmConfiguration()
    {
        return jbpmConfiguration;
    }

    public static String getCurrentLanguageLocaleString()
    {
        String str = getCurrentLanguage();
        if(str.equals("en"))
            return "en_GB";
        else
            return "pl_PL";
    }
    public static Locale getCurrentLanguageLocale()
    {
    	return GlobalPreferences.localeMap.get(getCurrentLanguage());

        /*String str = getCurrentLanguage();
        if(str.equals("en"))
            return new Locale("en", "GB");
        else
            return new Locale("pl", "PL");*/
    }

    public static Locale getDefaultLocale()
    {
    	return new Locale("pl", "PL");
    }
    /**
    /**
     * @TODO - wedug mnie dziala to jakos krzywo
     * Zwraca napis z kodem jezyka odczytanym z sesji.
     * Jakies inne propozycje ?
     */
    public static String getCurrentLanguage()
    {
    	javax.servlet.http.HttpServletRequest request = null;
    	javax.servlet.http.HttpSession session = null;
    	
    	
    	try
    	{
    		request = ServletActionContext.getRequest();
    		if (request != null)
    			session = request.getSession(false);
    		
    		if (session==null)
    		{
    			WebContext ctx = WebContextFactory.get();
    			if (ctx!=null)
    			{
    				request = ctx.getHttpServletRequest();
    				if (request!=null)
    				{
    					session = request.getSession(false);
    				}
    			}
    		}
    	}
    	catch (Exception e)
    	{
    		log.warn("Blad w czasie pobierania requestu",e);
    	}
    	String tempLang = null;
  		if (session != null)
        {
            try
            {
                String lang = (String) session.getAttribute(AuthFilter.LANGUAGE_KEY);
                tempLang = lang;
                if (lang != null)
                {
                    return lang;
                }
            }
            catch (Exception exc)
            {
                tempLang = null;
                log.debug("",exc);
            }
        }
        if(tempLang==null)
        {
        	//OtherSettingsAction.class.
        	if (Docusafe.getAdditionProperty("locale") != null)
        	{
        		return Docusafe.getAdditionProperty("locale");
        	}
        }
        return "pl";
    }

    /**
     * Z mapy warto�ci indeksowanej po j�zykach zwraca warto�� odpowiadaj�cej aktualnie wybranemu
     * j�zykowi. Gdy jest ona r�wna null - pr�ba zwr�cenia innej warto�ci.
     *
     * @param values   mapa (j�zyk,warto��)
     */
    public static String getLanguageValue(Map<String,String> values)
    {
        if (values.get(Docusafe.getCurrentLanguage()) != null)
            return values.get(Docusafe.getCurrentLanguage());

        if (values.get("en") != null)
            return values.get("en");
        else
            return values.get("pl");
    }

	public static String getDistTime() {
		return distTime;
	}

	public static long getDistTimestamp() {
		return distTimestamp;
	}

	public static void setDistTime(String distTime) {
		Docusafe.distTime = distTime;
	}

	public static String getUploadPath() {
		return uploadPath;
	}

	public static Directory getIndexes() {
		return indexes;
	}

	public static void setIndexes(Directory indexes) {
		Docusafe.indexes = indexes;
	}

	public static File getIndexesFiles() {
		return indexesFiles;
	}

	public static void setIndexesFiles(File indexesFiles) {
		Docusafe.indexesFiles = indexesFiles;
	}

	/***
	 * �cie�ka do katalogu w kt�rym przechowywane s� strony z pocietych tiffow oraz pdfow
	 * do zlaczenia
	 * @return
	 */
	public static String getPngTempFile()
	{
		return pngTempFile;
	}

	/**
	 * Sciezka do folderu w ktorym przechowywane sa strony z pocietych tiffow
	 * do zlaczenia
	 * @param pngTempFile
	 */
	public static void setPngTempFile(String pngTempFile)
	{
		Docusafe.pngTempFile = pngTempFile;
	}

	public static void setPageContext(String pageContext)
	{
		Docusafe.pageContext = pageContext;
	}

	public static String getPageContext()
	{
		return pageContext;
	}



}
