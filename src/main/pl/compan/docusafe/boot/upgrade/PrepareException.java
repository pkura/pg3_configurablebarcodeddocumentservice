package pl.compan.docusafe.boot.upgrade;
/* User: Administrator, Date: 2005-09-21 13:53:01 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: PrepareException.java,v 1.1 2005/09/22 15:59:59 lk Exp $
 */
public class PrepareException extends Exception
{
    public PrepareException(String message)
    {
        super(message);
    }

    public PrepareException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
