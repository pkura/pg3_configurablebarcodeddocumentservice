package pl.compan.docusafe.boot.upgrade;

import java.util.Map;
/* User: Administrator, Date: 2005-09-14 11:13:06 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ExecBase.java,v 1.6 2007/07/25 12:10:48 lk Exp $
 */
public abstract class ExecBase
{
    /** numer wersji dla kt�rej ma by� wykonany ten "upgrade" */
    private int version;
    
    /**
     * Metoda wykonywana w pierwszym przebiegu, przed prepare().
     */
    public abstract void init(Upgrade upgrade, Map<String, String> params) throws Exception;

    /**
     * Metoda wykonywana w pierwszym przebiegu; nie powinna niczego modyfikowa�,
     * jedynie sprawdza�, czy �rodowisko zosta�o odpowiednio przygotowane
     * w zakresie, w kt�rym klasa aktualizuj�ca sama nie mo�e sobie poradzi�.
     * Zazwyczaj dotyczy to konfiguracji serwera, kt�r� musi wykona� administrator.
     * @throws PrepareException Je�eli jaki� warunek nie jest spe�niony.
     */
    public abstract void prepare() throws PrepareException;
    public abstract void run() throws Exception;

    public int getVersion()
    {
        return version;
    }

    public void setVersion(int version)
    {
        this.version = version;
    }        
}
