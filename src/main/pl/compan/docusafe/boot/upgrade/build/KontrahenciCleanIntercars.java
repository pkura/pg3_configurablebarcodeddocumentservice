package pl.compan.docusafe.boot.upgrade.build;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.upgrade.ExecBase;
import pl.compan.docusafe.boot.upgrade.PrepareException;
import pl.compan.docusafe.boot.upgrade.Upgrade;

public class KontrahenciCleanIntercars extends ExecBase
{
	private Logger log = LoggerFactory.getLogger(KontrahenciCleanIntercars.class);
    public void init(Upgrade upgrade, Map params) throws Exception
    {
    }

    public void prepare() throws PrepareException
    {
    }

    public void run() throws Exception
    {
    	Connection conn = null;
        PreparedStatement ps = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;
        try
        {
            conn = Upgrade.getDatabaseConfiguration().getDataSource().getConnection();

            ps = conn.prepareStatement("select distinct numerKontrahenta from df_dicinvoice");
            ResultSet rs = ps.executeQuery();
           
            String numerKontrahenta = null;
            List<Long> toremove = new ArrayList<Long>();
            
            while(rs.next()) 
            {
            	numerKontrahenta = rs.getString(1);
            	if(numerKontrahenta.length()<6 || numerKontrahenta.contains("X"))
            		continue;
            	
            	ps1 = conn.prepareStatement("select id from df_dicinvoice where numerKontrahenta = ?");
            	ps1.setString(1, numerKontrahenta);
            	ResultSet rs1 = ps1.executeQuery();
            	Long id = null;
            	Long tempid = null;
            	while(rs1.next())
            	{
            		tempid = rs1.getLong(1);
            		if(id==null)
            			id = tempid;
            		else
            			toremove.add(tempid);
            		
            		
            		ps2 = conn.prepareStatement("update dsg_invoice_ic set dostawca = ? where dostawca = ?");
            		ps2.setLong(1, id);
            		ps2.setLong(2, tempid);
            		ps2.executeUpdate();
            		DbUtils.closeQuietly(ps2);
            		
            		ps2 = conn.prepareStatement("update dsg_sad set dostawca = ? where dostawca = ?");
            		ps2.setLong(1, id);
            		ps2.setLong(2, tempid);
            		ps2.executeUpdate();
            		DbUtils.closeQuietly(ps2);
            		
            	}
            	DbUtils.closeQuietly(rs1);
            	DbUtils.close(ps1);
            }
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(ps);
            
            for(Long id:toremove)
            {
            	ps2 = conn.prepareStatement("delete from df_dicinvoice where id=?");
            	ps2.setLong(1, id);
            	ps2.executeUpdate();
            	DbUtils.closeQuietly(ps2);
            }
            
            
        } catch (Exception e) {
        	log.debug("", e);
        	throw e;
        	
        	// TODO: handle exception
        }
        finally
        {
        	DbUtils.closeQuietly(ps);
        	DbUtils.closeQuietly(ps1);
        	DbUtils.closeQuietly(ps2);
        	DbUtils.closeQuietly(conn);
        }
    }

}
