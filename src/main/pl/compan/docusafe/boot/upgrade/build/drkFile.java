package pl.compan.docusafe.boot.upgrade.build;

import java.io.File;
import java.io.InputStream;
import java.util.Map;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.boot.upgrade.ExecBase;
import pl.compan.docusafe.boot.upgrade.PrepareException;
import pl.compan.docusafe.boot.upgrade.Upgrade;
import pl.compan.docusafe.util.FileUtils;

/* User: Administrator, Date: 2006-11-21 17:34:49 */

/**
 * @author Tomasz Lipka</a>
 */
public class drkFile extends ExecBase {
	public void init(Upgrade upgrade, Map params) throws Exception 
	{
	}

	public void prepare() throws PrepareException 
	{
	}

	public void run() throws Exception 
	{
		InputStream is = Docusafe.getServletContext().getResourceAsStream("/WEB-INF/mail/new-drk.txt");
		
        File mailer = new File(Docusafe.getServletContext().getInitParameter("homeDirectory") +"/"+ "mail" + "/" + "new-drk.txt");
        if(!mailer.exists())
        {
        	mailer.createNewFile();
        	FileUtils.writeIsToFile(is, mailer);
        }
	}	
}
