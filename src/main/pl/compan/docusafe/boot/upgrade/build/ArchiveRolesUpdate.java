package pl.compan.docusafe.boot.upgrade.build;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.boot.upgrade.ExecBase;
import pl.compan.docusafe.boot.upgrade.PrepareException;
import pl.compan.docusafe.boot.upgrade.Upgrade;

public class ArchiveRolesUpdate extends ExecBase
{
    public void init(Upgrade upgrade, Map params) throws Exception
    {
    }

    public void prepare() throws PrepareException
    {
    }

    public void run() throws Exception
    {
    	Connection conn = null;
        PreparedStatement ps = null;
        try
        {
            conn = Upgrade.getDatabaseConfiguration().getDataSource().getConnection();

            ps = conn.prepareStatement("select id, userroles from ds_user");
            ResultSet rs = ps.executeQuery();
            Map<Long,String> roles = new HashMap<Long, String>();
            Long id = null;
            String role = null;
            while(rs.next()) {
            	id = rs.getLong(1);
            	role = rs.getString(2);
            	if (role != null) {
            		roles.put(id, role);
            	}
            }
            Statement stmt = conn.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);

            rs = stmt.executeQuery("select user_id, role, source from ds_user_to_archive_role");
            Set<Entry<Long,String>> entries = roles.entrySet();
            Iterator<Entry<Long,String>> iter = entries.iterator();
            String roleNamesEncoded;
            String[] roleNamesDecoded;
            while(iter.hasNext()) {
            	Entry<Long,String> entry = iter.next();
            	roleNamesEncoded = entry.getValue();
            	roleNamesDecoded = StringUtils.split(roleNamesEncoded, " "); 
            	for (String rolename : roleNamesDecoded) {
            		if (rolename.compareToIgnoreCase("data_admin") == 0 ||
            				rolename.compareToIgnoreCase("admin") == 0	) {
            			ps = conn.prepareStatement("INSERT INTO ds_user_to_archive_role (user_id,role,source) VALUES(?, ?, ?)");
            			ps.setLong(1, entry.getKey());
            			ps.setString(2, rolename);
            			ps.setString(3, "own");
            			ps.execute();
            		}
            	}
            }
            
            
        } catch (Exception e) {
        	LogFactory.getLog("eprint").debug("", e);
        	throw e;
        	
        	// TODO: handle exception
        }
    }
}
