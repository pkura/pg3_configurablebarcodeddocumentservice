package pl.compan.docusafe.boot.upgrade.build;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.boot.upgrade.ExecBase;
import pl.compan.docusafe.boot.upgrade.PrepareException;
import pl.compan.docusafe.boot.upgrade.Upgrade;

/**
 * Zmiana w bazie danych zapisu flag
 * 
 * @author Mariusz Kilja�czyk
 * 
 */
public class UpgradeFlags extends ExecBase
{
    public void init(Upgrade upgrade, Map params) throws Exception
    {
    }

    public void prepare() throws PrepareException
    {
    }

    public void run() throws Exception
    {
        Connection conn = null;
        PreparedStatement ps = null;
        try
        {
            conn = Upgrade.getDatabaseConfiguration().getDataSource().getConnection();

            ps = conn.prepareStatement("select ID from ds_prefs_node where NAME = 'documentflags'");
            ResultSet rs = ps.executeQuery();
            List<Long> documentflagsList = new ArrayList<Long>();
            while(rs.next())
            {
                documentflagsList.add(rs.getLong(1));
            }
            if(documentflagsList.size()>0)
            {
                StringBuilder sb = new StringBuilder();
                sb.append("select NODE_ID,PREF_KEY,PREF_VALUE from DS_PREFS_VALUE where ");
    
                for (Iterator iter = documentflagsList.iterator(); iter.hasNext();)
                {
                    Long element = (Long) iter.next();
                    sb.append("NODE_ID = ");
                    sb.append(element);
                    if(iter.hasNext())
                        sb.append(" OR ");
                    
                }
                ps = conn.prepareStatement(sb.toString());
                rs = ps.executeQuery();
                List<prefsBean> prefsList = new ArrayList<prefsBean>();
                while(rs.next())
                {
                    prefsList.add(new prefsBean(rs.getLong(1),rs.getString(2),rs.getString(3)));
                }
                
                String tmp;
                for (Iterator iter = prefsList.iterator(); iter.hasNext();)
                {
                    prefsBean element = (prefsBean) iter.next();
                    tmp = element.getValue();
                    String[] fl = tmp.split("\\s*;\\s*", 4);
                    if (fl.length  < 4)
                    {
                        if(fl.length  < 3)
                        {
                            tmp +=";all;all";
                            ps = conn.prepareStatement("UPDATE ds_prefs_value SET PREF_VALUE = ? WHERE PREF_KEY = ? and NODE_ID = ?");
                            ps.setString(1,tmp);
                            ps.setString(2,element.getId());
                            ps.setLong(3,element.getNode_id());
                            ps.executeUpdate();
                        }
                        else
                        {
                            tmp +=";all";
                            ps = conn.prepareStatement("UPDATE ds_prefs_value SET PREF_VALUE = ? WHERE PREF_KEY = ? and NODE_ID = ?");
                            ps.setString(1,tmp);
                            ps.setString(2,element.getId());
                            ps.setLong(3,element.getNode_id());
                            ps.executeUpdate();
                        }
    
                    }
    
                }
            }
            
        }
        finally
        {
            if (ps != null) try { ps.close(); } catch (Exception e) { }
            if (conn != null) try { conn.close(); } catch (Exception e) { }
        }
    }
    private class prefsBean
    {
        private Long node_id;
        private String id;
        private String value;
        prefsBean(Long node_id,String id,String value)
        {
            this.node_id = node_id;
            this.value = value;
            this.id = id;
        }
        public Long getNode_id()
        {
            return node_id;
        }
        public String getValue()
        {
            return value;
        }
        public String getId()
        {
            return id;
        }
    }
}
