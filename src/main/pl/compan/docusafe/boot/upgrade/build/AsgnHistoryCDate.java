package pl.compan.docusafe.boot.upgrade.build;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Map;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.boot.upgrade.ExecBase;
import pl.compan.docusafe.boot.upgrade.PrepareException;
import pl.compan.docusafe.boot.upgrade.Upgrade;

public class AsgnHistoryCDate extends ExecBase
{
    public void init(Upgrade upgrade, Map params) throws Exception
    {
    }

    public void prepare() throws PrepareException
    {
    }

    public void run() throws Exception
    {
        Connection conn = null;
        PreparedStatement ps = null;
        try
        {
            conn = Upgrade.getDatabaseConfiguration().getDataSource().getConnection();
            if (Upgrade.getDatabaseConfiguration().getDatabaseType().equals(Docusafe.DB_SQLSERVER2000))
            {
                ps = conn.prepareStatement("update dso_document_asgn_history set cdate=convert(datetime,convert(varchar,ctime,111))");
            }
            else if(Upgrade.getDatabaseConfiguration().getDatabaseType().equals(Docusafe.ORACLE_9))
            {
                ps = conn.prepareStatement("update dso_document_asgn_history set cdate=cast(ctime as date)");
            }
            else if(Upgrade.getDatabaseConfiguration().getDatabaseType().equals(Docusafe.DB_FIREBIRD_1_5))
            {
                ps = conn.prepareStatement("update dso_document_asgn_history set cdate=cast(ctime as date)");
            }
            ps.executeUpdate();
        }
        finally
        {
            if (ps != null) try { ps.close(); } catch (Exception e) { }
            if (conn != null) try { conn.close(); } catch (Exception e) { }
        }    
    }
}
