package pl.compan.docusafe.boot.upgrade.build;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Map;

import pl.compan.docusafe.boot.upgrade.ExecBase;
import pl.compan.docusafe.boot.upgrade.PrepareException;
import pl.compan.docusafe.boot.upgrade.Upgrade;

/**
 * @author Tomasz Lipka</a>
 */
public class DaaDoctypev506 extends ExecBase {
	public void init(Upgrade upgrade, Map params) throws Exception {
	}

	public void prepare() throws PrepareException {
	}

	public void run() throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = Upgrade.getDatabaseConfiguration().getDataSource()
					.getConnection();

			ps = conn.prepareStatement("update ds_doctype set content = ? "
					+ "where cn = ?");
			byte[] buf = xml.getBytes("utf-8");
			ps.setBinaryStream(1, new ByteArrayInputStream(buf), buf.length);
			ps.setString(2, "daa");
			ps.executeUpdate();
		} finally {
			if (ps != null)
				try {
					ps.close();
				} catch (Exception e) {
				}
			if (conn != null)
				try {
					conn.close();
				} catch (Exception e) {
				}
		}
	}

	private static final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
	+ "<doctype>"
	+ "<fields>"
	+ "<field id=\"10\" cn=\"STATUS\" column=\"FIELD_0\" name=\"Status\" type=\"enum\">"
	+"<enum-item id=\"10\" cn=\"PRZYJETY\" title=\"Przyjęty\"/>"
	+"<enum-item id=\"20\" cn=\"W_REALIZACJI\" title=\"W realizacji\"/>"
	+"<enum-item id=\"30\" cn=\"ZAWIESZONY\" title=\"Zawieszony\"/>"
	+"<enum-item id=\"40\" cn=\"ZREALIZOWANY\" title=\"Zrealizowany\"/>"
	+"<enum-item id=\"50\" cn=\"ODRZUCONY\" title=\"Odrzucony\"/>"
	+"<enum-item id=\"60\" cn=\"ARCHIWALNY\" title=\"Archiwalny\"/>"
	+"</field>"
	+"<field id=\"20\" cn=\"DATA_WPLYWU\" column=\"FIELD_2\" name=\"Data\" type=\"date\"/>"
	+"<field id=\"30\" cn=\"AGENT\" column=\"FIELD_4\" name=\"Specjalista/OWCA\" type=\"long\"/>"
	+"<field id=\"40\" cn=\"AGENCJA\" column=\"FIELD_5\" name=\"Biuro regionalne/agencja/bank\" type=\"long\"/>"
	+"<field id=\"50\" cn=\"TYP_DOKUMENTU\" column=\"FIELD_3\" name=\"Typ dokumentu\" type=\"enum\" >"
	+"<enum-item id=\"10\" title=\"Ankieta danych\" cn=\"ANKIETA_DANYCH\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"20\" title=\"Ankieta danych\" cn=\"ANKIETA_DANYCH\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"30\" title=\"Aneks\" cn=\"ANEKS\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"40\" title=\"Aneks\" cn=\"ANEKS\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"50\" title=\"Certyfikat Citiinsurance\" cn=\"CITIINSURANCE\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"60\" title=\"Dane osoby fizycznej\" cn=\"DANE_OSOBY_FIZYCZNEJ\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"70\" title=\"Dyplom / świadectwo\" cn=\"DYPLOM_SWIADECTWO\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"80\" title=\"Dyplom / świadectwo\" cn=\"DYPLOM_SWIADECTWO\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"90\" title=\"Inne\" cn=\"INNE\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"100\" title=\"Inne\" cn=\"INNE\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"110\" title=\"Karta egzaminacyjna\" cn=\"KARTA_EGZAMINACYJNA\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"120\" title=\"Karta egzaminacyjna\" cn=\"KARTA_EGZAMINACYJNA\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"130\" title=\"Karta szkoleniowa\" cn=\"KARTA_SZKOLENIOWA\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"140\" title=\"Korespondencja przychodząca do Nationwide / AEGON\" cn=\"KORESPONDENCJA_PRZYCHODZACA\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"150\" title=\"Korespondencja przychodząca do Nationwide / AEGON\" cn=\"KORESPONDENCJA_PRZYCHODZACA\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"160\" title=\"Korespondencja wewnętrzna\" cn=\"KORESPONDENCJA_WEWNETRZNA\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"170\" title=\"Korespondencja wewnętrzna\" cn=\"KORESPONDENCJA_WEWNETRZNA\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"180\" title=\"Korespondencja wychodząca z Nationwide / AEGON\" cn=\"KORESPONDENCJA_WYCHODZACA\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"190\" title=\"Korespondencja wychodząca z Nationwide / AEGON\" cn=\"KORESPONDENCJA_WYCHODZACA\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"200\" title=\"Lista osób do rejestracji KNUIFE\" cn=\"KNUIFE\" arg1=\"RODZAJ_RAPORT\"/>"
	+"<enum-item id=\"202\" title=\"Lista OWCA - rejestracja\" cn=\"OWCA_REJESTRACJA\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"204\" title=\"Lista OWCA - wykreślenie\" cn=\"OWCA_WYKRESLENIE\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"210\" title=\"NIP\" cn=\"NIP\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"220\" title=\"NIP\" cn=\"NIP\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"230\" title=\"Oświadczenie (PZCP)\" cn=\"PZCP\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"240\" title=\"Oświadczenie (PZCP)\" cn=\"PZCP\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"250\" title=\"Pełnomocnictwo\" cn=\"PELNOMOCNICTWO\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"260\" title=\"Pełnomocnictwo\" cn=\"PELNOMOCNICTWO\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"270\" title=\"Plany szkoleń\" cn=\"PLANY_SZKOLEN\" arg1=\"RODZAJ_RAPORT\"/>"
	+"<enum-item id=\"271\" title=\"Przeniesienie obsługi\" cn=\"PRZENIESIENIE_OBSLUGI\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"280\" title=\"Protokoły egzaminacyjne\" cn=\"PROTOKOLY_EGZAMINACYJNE\" arg1=\"RODZAJ_RAPORT\"/>"
	+"<enum-item id=\"290\" title=\"Rachunek bankowy\" cn=\"RACHUNEK_BANKOWY\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"300\" title=\"Rachunek bankowy\" cn=\"RACHUNEK_BANKOWY\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"310\" title=\"Raporty aktywności szkoleniowej\" cn=\"RAPORT_AKTYWNOSCI_SZKOLENIOWEJ\" arg1=\"RODZAJ_RAPORT\"/>"
	+"<enum-item id=\"320\" title=\"REGON\" cn=\"REGON\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"330\" title=\"REGON\" cn=\"REGON\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"340\" title=\"Szkolenie\" cn=\"SZKOLENIE\" arg1=\"RODZAJ_RAPORT\"/>"
	+"<enum-item id=\"380\" title=\"Umowa 1\" cn=\"UMOWA_1\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"390\" title=\"Umowa 1\" cn=\"UMOWA_1\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"400\" title=\"Umowa 2\" cn=\"UMOWA_2\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"410\" title=\"Umowa 2\" cn=\"UMOWA_2\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"420\" title=\"Umowa 3\" cn=\"UMOWA_3\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"430\" title=\"Umowa 3\" cn=\"UMOWA_3\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"440\" title=\"Umowa\" cn=\"UMOWA\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"450\" title=\"Umowa\" cn=\"UMOWA\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"452\" title=\"Umowa fakturowa\" cn=\"UMOWA_FAKTUROWA\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"454\" title=\"Umowa na powierzenie przetwarzania danych\" cn=\"UMOWA_NA_POWIERZENIE_PRZETWARZANIA_DANYCH\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"455\" title=\"Umowa OFE\" cn=\"UMOWA_OFE\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"456\" title=\"Umowa o zarządzanie\" cn=\"UMOWA_O_ZARZADZANIE\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"457\" title=\"Umowa OFE kierownicza\" cn=\"UMOWA_OFE_KIEROWNICZA\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"458\" title=\"Umowa specjalisty\" cn=\"UMOWA_SPECJALISTY\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"460\" title=\"Wpis do ewidencji lub KRS\" cn=\"KRS\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"470\" title=\"Wpis do ewidencji lub KRS\" cn=\"KRS\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"480\" title=\"Wypowiedzenie umowy\" cn=\"WYPOWIEDZENIE_UMOWY\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"490\" title=\"Wypowiedzenie umowy\" cn=\"WYPOWIEDZENIE_UMOWY\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"500\" title=\"Zaświadczenie KRK\" cn=\"KRK\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"510\" title=\"Zaświadczenie o uczestnictwie w szkoleniu (inne TU)\" cn=\"UCZESTNICTWO_SZKOLENIE_INNE_TU\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"511\" title=\"Zaświadczenie KRK\" cn=\"KRK\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"520\" title=\"Zaświadczenie o odbyciu szkolenia (AEGON)\" cn=\"ODBYCIE_SZKOLENIA\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"530\" title=\"Zaświadczenie o odbyciu szkolenia (AEGON)\" cn=\"ODBYCIE_SZKOLENIA\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"540\" title=\"Zaświadczenie o ukończeniu szkolenia\" cn=\"UKONCZENIE_SZKOLENIA\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"550\" title=\"Zaświadczenie o ukończeniu szkolenia\" cn=\"UKONCZENIE_SZKOLENIA\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"560\" title=\"Zaświadczenie o ukończeniu szkolenia / zdaniu egzaminu / licencja (inne TU)\" cn=\"ZASWIADCZENIE_INNE\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"570\" title=\"Zaświadczenie o ukończeniu szkolenia / zdaniu egzaminu / licencja (inne TU)\" cn=\"ZASWIADCZENIE_INNE\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"580\" title=\"Zaświadczenie o zdaniu egzaminu (AEGON)\" cn=\"ZDANIE_EGZAMINU\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"590\" title=\"Zaświadczenie o zdaniu egzaminu (AEGON)\" cn=\"ZDANIE_EGZAMINU\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"595\" title=\"Zgłoszenie do US\" cn=\"ZGLOSZENIE_DO_US\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"600\" title=\"Zgłoszenie szkolenia\" cn=\"ZGLOSZENIE_SZKOLENIA\" arg1=\"RODZAJ_RAPORT\"/>"
	+"<enum-item id=\"610\" title=\"Umowa agencyjna\" cn=\"UMOWA_AGENCYJNA\" arg1=\"RODZAJ_AGENCJA\"/>"
    +"<enum-item id=\"620\" title=\"Aneks premiowy\" cn=\"ANEKS_PREMIOWY\" arg1=\"RODZAJ_AGENCJA\"/>"
    +"<enum-item id=\"630\" title=\"Aneks produktowy\" cn=\"ANEKS_PRODUKTOWY\" arg1=\"RODZAJ_AGENCJA\"/>"
    +"<enum-item id=\"640\" title=\"Aneks niestandardowy\" cn=\"ANEKS_NIESTANDARDOWY\" arg1=\"RODZAJ_AGENCJA\"/>"
    +"<enum-item id=\"650\" title=\"Cesja/Porozumienie\" cn=\"CESJA_POROZUMIENIE\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"660\" title=\"Formularz prowizyjny\" cn=\"FORMULARZ_PROWIZYJNY\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"670\" title=\"Formularz cesji/porozumienia\" cn=\"FORMULARZ_CESJI_POROZUMIENIA\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"680\" title=\"Umowa fakturowa\" cn=\"UMOWA_FAKTUROWA\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"</field>"
	+"<field id=\"55\" cn=\"KLASA_RAPORTU\" column=\"FIELD_6\" name=\"Klasa raportu\" type=\"enum\" length=\"50\" >"
	+"<enum-item id=\"1\" title=\"Szkolenie 36h\" cn=\"SZKOLENIE_36\" />"
	+"<enum-item id=\"2\" title=\"Szkolenie 48h\" cn=\"SZKOLENIE_48\" />"
	+"<enum-item id=\"3\" title=\"Szkolenie 152h\" cn=\"SZKOLENIE_128\" />"
	+"<enum-item id=\"4\" title=\"Szkolenie dla Zarządu\" cn=\"SZKOLENIE_DLA_ZARZADU\" />"
	+"<enum-item id=\"5\" title=\"Inne\" cn=\"INNE\" />"
	+"</field>"
	+"<field id=\"60\" cn=\"RODZAJ_SIECI\" column=\"FIELD_1\" name=\"Rodzaj sieci\" type=\"enum\" length=\"50\" >"
	+"<enum-item id=\"1\" title=\"Sieć własna\" cn=\"SIEC_WLASNA\" arg1=\"RODZAJ_SIECI\"/>"
	+"<enum-item id=\"2\" title=\"Sieć zewnętrzna\" cn=\"SIEC_ZEWNETRZNA\" arg1=\"RODZAJ_SIECI\"/>"
	+"<enum-item id=\"3\" title=\"Banki\" cn=\"BANKI\" arg1=\"RODZAJ_SIECI\"/>"
	+"</field>"
	+"</fields>"
	+"</doctype>"
	;	
}
