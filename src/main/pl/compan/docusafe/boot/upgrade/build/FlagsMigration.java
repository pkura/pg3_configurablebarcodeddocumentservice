package pl.compan.docusafe.boot.upgrade.build;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;

import pl.compan.docusafe.boot.upgrade.ExecBase;
import pl.compan.docusafe.boot.upgrade.PrepareException;
import pl.compan.docusafe.boot.upgrade.Upgrade;
import pl.compan.docusafe.core.labels.Label;
import org.apache.commons.logging.Log;


public class FlagsMigration extends ExecBase 
{
	private static final Log log = Upgrade.log;
	public void init(Upgrade upgrade, Map params) throws Exception {
	}

	public void prepare() throws PrepareException {
	}
	
	public void run() throws Exception {
		//log.debug("#############################################################333");
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		boolean done;
		Long labelId;
		try {
			conn = Upgrade.getDatabaseConfiguration().getDataSource()
					.getConnection();

			ps = conn.prepareStatement("select * from ds_prefs_node where name = ?");
			ps.setString(1, "documentflags");
			rs = ps.executeQuery();
			String flag;
			String[] values;
			while(rs.next())
			{
				//flagi wspolne
				log.debug("Flagi wspolne");
				if(rs.getString("username")==null||rs.getString("username").equals(""))
				{
					ps = conn.prepareStatement("select * from ds_prefs_value where node_id=?");
					ps.setLong(1, rs.getLong("id"));
					rs1 = ps.executeQuery();
					while(rs1.next())
					{
						flag = rs1.getString("pref_value");
						log.debug("Przetwarzam flage " + flag);
						values = flag.split(";");
						String query = "insert into dso_label " +
						"(name, description, parent_id, hidden, modifiable, deletes_after, type," +
						" read_rights, write_rights, modify_rights) values (?,?,0,0,1,0,?,?,?,?)";
						ps = conn.prepareStatement("insert into dso_label " +
								"(name, description, parent_id, hidden, modifiable, deletes_after, type," +
								" read_rights, write_rights, modify_rights) values (?,?,0,0,1,0,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
						ps.setString(1, values[0]);
						ps.setString(2, values[1]);
						ps.setString(3, Label.FLAG_TYPE);
						if(values.length>3)
						{
							ps.setString(4, "all".equals(values[3])?Label.SYSTEM_LABEL_OWNER:values[3]);
							ps.setString(5, "all".equals(values[2])?Label.SYSTEM_LABEL_OWNER:values[2]);
						}
						else
						{
							ps.setString(4, Label.SYSTEM_LABEL_OWNER);
							ps.setString(5, Label.SYSTEM_LABEL_OWNER);
						}
						ps.setString(6, Label.SYSTEM_LABEL_OWNER);
						ps.execute();
						rs2 = ps.getGeneratedKeys();
						
						if(rs2.next())
						{
							
							labelId = rs2.getLong(1);
							rs2.close();
							ps = conn.prepareStatement("select * from ds_document_flags where fl"+rs1.getInt("pref_key") + "=1");
							rs2 = ps.executeQuery();

							while(rs2.next())
							{
								Date d = rs2.getDate("fl"+rs1.getInt("pref_key")+"mtime");
								log.debug("Dodaje wpisy dla flagi:"+ labelId);
								ps = conn.prepareStatement("insert into dso_document_to_label " +
										"(document_id, label_id, ctime, hidding, username) values(?,?,?,?,?)");
								ps.setLong(1, rs2.getLong("document_id"));
								ps.setLong(2, labelId);
								ps.setDate(3, d==null?new Date(new java.util.Date().getTime()):d);
								ps.setBoolean(4, false);
								ps.setString(5, rs2.getString("fl"+rs1.getInt("pref_key")+"author"));
								if(ps.execute()) log.debug("Udalo sie");
								else log.debug("nic z tego");
								
							}
						}
					}
				}
				//flagi uzytkownika
				else
				{
					log.debug("Flagi użytkownika");
					ps = conn.prepareStatement("select * from ds_prefs_value where node_id=?");
					ps.setLong(1, rs.getLong("id"));
					rs1 = ps.executeQuery();
					while(rs1.next())
					{
						flag = rs1.getString("pref_value");
						log.debug("Przetwarzam flage " + flag);
						values = flag.split(";");
						ps = conn.prepareStatement("insert into dso_label " +
								"(name, description, parent_id, hidden, modifiable, deletes_after, type," +
								" read_rights, write_rights, modify_rights) values (?,?,0,0,1,0,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
						ps.setString(1, values[0]);
						ps.setString(2, values[1]);
						ps.setString(3, Label.FLAG_TYPE);

						ps.setString(4, rs.getString("username"));
						ps.setString(5, rs.getString("username"));
						
						ps.setString(6, rs.getString("username"));
						done = ps.execute();
						rs2 = ps.getGeneratedKeys();
						
						if(rs2.next())
						{
							labelId = rs2.getLong(1);
							rs2.close();
							log.debug("Dodano etykiete id:" + labelId);
							ps = conn.prepareStatement("select * from ds_document_user_flags where fl"+rs1.getInt("pref_key") + "=1" + 
									" and username=?");
							
							ps.setString(1, rs.getString("username"));
							rs2 = ps.executeQuery();
							
							while(rs2.next())
							{
								Date d = rs2.getDate("fl"+rs1.getInt("pref_key")+"mtime");
								log.debug("Dodaje wpisy dla flagi:"+ labelId);
								ps = conn.prepareStatement("insert into dso_document_to_label " +
										"(document_id, label_id, ctime, hidding, username) values(?,?,?,?,?)");
								ps.setLong(1, rs2.getLong("document_id"));
								ps.setLong(2, labelId);
								ps.setDate(3, d==null?new Date(new java.util.Date().getTime()):d);
								ps.setBoolean(4, false);
								ps.setString(5, rs2.getString("fl"+rs1.getInt("pref_key")+"author"));
								if(ps.execute()) log.debug("Udalo sie");
								else log.debug("nic z tego");
							}
						}
					}
				}
			}
			
		}
		catch(Exception e){
			log.debug(e.getMessage());
			for(StackTraceElement s:e.getStackTrace()){
				log.debug(s.toString());
			}
		}
		finally {
			if (ps != null)
				try {
					ps.close();
				} catch (Exception e) {
				}
			if (conn != null)
				try {
					conn.close();
				} catch (Exception e) {
				}
			if (rs != null)
				try {
					rs.close();
				} catch (Exception e) {
				}
			if (rs1 != null)
				try {
					rs1.close();
				} catch (Exception e) {
				}
			if (rs1 != null)
				try {
					rs1.close();
				} catch (Exception e) {
				}
		}
	}
	

}
