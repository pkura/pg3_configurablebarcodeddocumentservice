package pl.compan.docusafe.boot.upgrade.build;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Map;

import org.apache.commons.logging.Log;

import pl.compan.docusafe.boot.upgrade.ExecBase;
import pl.compan.docusafe.boot.upgrade.PrepareException;
import pl.compan.docusafe.boot.upgrade.Upgrade;
import pl.compan.docusafe.core.cfg.Configuration;

public class UpdateRwaCloseOld extends ExecBase 
{
	private static final Log log = Upgrade.log;

	@Override
	public void init(Upgrade upgrade, Map<String, String> params)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void prepare() throws PrepareException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void run() throws Exception 
	{
		if(Configuration.hasExtra("business"))
		{
			log.info("Business version - skip");
			return;
		}
		log.error("UpgradeRwa");
		Connection conn = null;
		Statement stat = null;
		PreparedStatement deactive = null;
		ResultSet rs = null;
		try
		{
			conn = Upgrade.getDatabaseConfiguration().getDataSource().getConnection();
			conn.setAutoCommit(false);
			stat = conn.createStatement();
			deactive = conn.prepareStatement("UPDATE DSO_RWA SET ISACTIVE = ? where id < ?");	
			rs = stat.executeQuery("select ID from dso_rwa where rwalevel = 1 and digit1 = 0 order by id desc");
			rs.next();
			Integer firstNewRwaId = rs.getInt(1);
			log.error("Nowe RWA znajduje si� od ID = "+ firstNewRwaId);
			deactive.setBoolean(1, false);
			deactive.setInt(2, firstNewRwaId);
			deactive.executeUpdate();	
			conn.commit();
			log.error("Aktualizacja zako�czona");
		}
		catch(Exception e){
			log.error(e.getMessage(),e);
		}
		finally 
		{			
			if (stat!= null)
				try {
					stat.close();
				} catch (Exception e) {
				}
			if (deactive!= null)
				try {
					deactive.close();
				} catch (Exception e) {
				}		
			if (rs!= null)
				try {
					rs.close();
				} catch (Exception e) {
				}
			if (conn != null)
				try {
					conn.close();
				} catch (Exception e) {
				}			
		}
	}
}