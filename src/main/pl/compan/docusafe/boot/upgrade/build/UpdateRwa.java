package pl.compan.docusafe.boot.upgrade.build;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import org.apache.commons.logging.Log;

import pl.compan.docusafe.boot.upgrade.ExecBase;
import pl.compan.docusafe.boot.upgrade.PrepareException;
import pl.compan.docusafe.boot.upgrade.Upgrade;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.web.archive.init.InitialConfigurationAction.SqlFileReader;

public class UpdateRwa extends ExecBase 
{
	private static final Log log = Upgrade.log;

	@Override
	public void init(Upgrade upgrade, Map<String, String> params)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void prepare() throws PrepareException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void run() throws Exception 
	{
		if(Configuration.hasExtra("business"))
		{
			log.info("Business version - skip");
			return;
		}
		log.info("UpgradeRwa");
		Connection conn = null;
		Statement stat = null;
		PreparedStatement setAllIsActive = null;
		PreparedStatement prestat2 = null;
		PreparedStatement prestatUpdateLev = null;
		ResultSet rs = null;
		try
		{
			conn = Upgrade.getDatabaseConfiguration().getDataSource().getConnection();
			conn.setAutoCommit(false);
			stat = conn.createStatement();		
			String dbBrand;
			
			DatabaseMetaData dbmd = stat.getConnection().getMetaData();
			String dbName = dbmd.getDatabaseProductName();
			log.info("Nazwa bazy danych pobrana z meta: " + dbName);
			if(dbName.equals("Oracle"))
			{
				dbBrand = "oracle";		
				log.info("Skrypt dla: " + dbBrand);
				stat.executeUpdate("alter table DSO_RWA add isactive NUMBER(1)");
				conn.commit();
			}
			else{
				dbBrand = "firebird";
				log.info("Skrypt dla: " + dbBrand);
				stat.executeUpdate("alter table DSO_RWA add isactive CHAR(1)");
				conn.commit();
			}	
			
			setAllIsActive = conn.prepareStatement("UPDATE DSO_RWA SET ISACTIVE = ?");	
			
			rs = stat.executeQuery("SELECT COUNT(*) FROM DSO_RWA WHERE RWALEVEL < 0");
			rs.next();
			
			
			if(rs.getInt(1)==0)
			{
				log.info("Niezaktualizowane RWA");
				
				setAllIsActive.setBoolean(1, false);
				setAllIsActive.executeUpdate();

				InputStream rwa = null;
				BufferedReader rrwa = null;
				rs = stat.executeQuery("SELECT COUNT(*) FROM DSO_RWA WHERE DIGIT1=9 AND DESCRIPTION LIKE 'ZATRUDNIENIE I SPRAWY SOCJALNE'");
				rs.next();
				if(rs.getInt(1)==0)
				{
					log.info("SP");
                    rwa = getClass().getClassLoader().getResourceAsStream("rwaPowiat2011." +dbBrand +".sql");
					rrwa = new BufferedReader(new InputStreamReader(rwa, Charset.forName("ISO-8859-2")));
	                executeSqlFile(rrwa,conn);
	                rwa.close();
	                rrwa.close();
				}
				else
				{
					log.info("UM");
                    rwa = getClass().getClassLoader().getResourceAsStream("rwa2011Gmina." +dbBrand +".sql");
                    log.info("Plik sql do aktualizacji " + rwa.toString());
					rrwa = new BufferedReader(new InputStreamReader(rwa, Charset.forName("ISO-8859-2")));
	                executeSqlFile(rrwa,conn);
	                rwa.close();
	                rrwa.close();
				}
                prestat2 = conn.prepareStatement("UPDATE DSO_RWA SET ISACTIVE = ? WHERE ISACTIVE IS NULL");		
                prestat2.setBoolean(1, true);
                prestat2.executeUpdate();
                log.info("Wykonano aktualizacje RWA");
			}
			else
			{
				log.info("zaktualizowane RWA");
				setAllIsActive.setBoolean(1, true);
				setAllIsActive.executeUpdate();
				prestatUpdateLev = conn.prepareStatement("UPDATE DSO_RWA SET ISACTIVE = ?,RWALEVEL = RWALEVEL*(-1) WHERE RWALEVEL < 0");
				prestatUpdateLev.setBoolean(1, false);				
				prestatUpdateLev.executeUpdate();
			}			
			conn.commit();
			log.info("Aktualizacja zako�czona");
		}
		catch(Exception e){
			log.error(e.getMessage(),e);
		}
		finally 
		{			
			if (stat!= null)
				try {
					stat.close();
				} catch (Exception e) {
				}
			if (setAllIsActive!= null)
				try {
					setAllIsActive.close();
				} catch (Exception e) {
				}		
			if (prestat2!= null)
				try {
					prestat2.close();
				} catch (Exception e) {
				}	
			if (prestatUpdateLev!= null)
				try {
					prestatUpdateLev.close();
				} catch (Exception e) {
				}
			if (rs!= null)
				try {
					rs.close();
				} catch (Exception e) {
				}
			if (conn != null)
				try {
					conn.close();
				} catch (Exception e) {
				}			
		}
	}
	
	 private void executeSqlFile(BufferedReader reader, Connection conn)
     throws IOException, SQLException
     {
     //Zapisanie skryptow sql do plikow
     
     Statement st = null;
     try
     {
         st = conn.createStatement();
         
         SqlFileReader initSql = new SqlFileReader(reader);
         
         while (initSql.hasNextStatement())
         {
         	
             String stmt = initSql.nextStatement();
             try
             {
             	st.executeUpdate(stmt);
             }
             catch (SQLException e)
             {
             	e.printStackTrace();
             	throw e;
             }
         }
         
         st.close();
     }
	 catch(Exception e){
		log.error(e.getMessage(),e);
	 }
     finally
     {
         if (st != null) try { st.close(); } catch (Exception e) { }
     }
 }
}
		
		
//ROLNICTWO, LE�NICTWO, PRZEMYS�, US�UGI, HANDEL