package pl.compan.docusafe.boot.upgrade.build;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Map;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.boot.upgrade.ExecBase;
import pl.compan.docusafe.boot.upgrade.PrepareException;
import pl.compan.docusafe.boot.upgrade.Upgrade;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.office.workflow.TaskHistoryEntry;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.StringManager;

/**
 * Aktualizacja historii listy zada� na podstawie historii dekretacji.
 * 
 * UWAGA: aktualizowane s? tylko zadania wewn�trznego Workflow (bez zada� JBPM) - nale�y pami�ta�
 *        �e ta aktualizacja mo�e wygenerowa� dane nie do ko�ca zgodne z rzeczywisto?ci?, gdy�
 *        w historii dekretacji mog�o by� cze?� rzeczy niedok�adnie zapisanych. 
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class TasklistHistory extends ExecBase
{
	private static StringManager smG = GlobalPreferences.loadPropertiesFile("",null);
    public void init(Upgrade upgrade, Map params) throws Exception
    {
    }

    public void prepare() throws PrepareException
    {        
    }

    private Object getValue(ResultSet rs, String type, int pos) throws SQLException
    {
        if ("string".equals(type))
        {
            String s = rs.getString(pos);
            if (rs.wasNull())
                return null;
            else
                return s;
        }
        else if ("integer".equals(type))
        {
            Integer i = rs.getInt(pos);
            if (rs.wasNull())
                return null;
            else
                return i;
        }
        else if ("boolean".equals(type))
        {
            Boolean b = rs.getBoolean(pos);
            if (rs.wasNull())
                return Boolean.FALSE;
            else
                return b;
        }
        else if ("date".equals(type))
        {
            java.sql.Date date = rs.getDate(pos);
            java.util.Date value = date != null ? new java.util.Date(date.getTime()) : null;
            return value;
        }
        else if ("timestamp".equals(type))
        {
            java.sql.Timestamp date = rs.getTimestamp(pos);
            java.util.Date value = date != null ? new java.util.Date(date.getTime()) : null;
            return value;
        }
        return null;
    }
    
    private void setValue(PreparedStatement ps, String type, int pos, Object value) throws SQLException
    {
    	
    	if (value == null)
    	{
        	ps.setObject(pos, null);
        	return;
    	}
    	
        if ("string".equals(type))
        {   
             ps.setString(pos, (String) value);
        }
        else if ("boolean".equals(type))
        {
            ps.setBoolean(pos, ((Boolean) value).booleanValue());
        }
        else if ("date".equals(type))
        {
            ps.setDate(pos, new java.sql.Date(((Date) value).getTime()));
        }
        else if ("timestamp".equals(type))
        {

            ps.setTimestamp(pos, new java.sql.Timestamp(((Date) value).getTime()));
        }
        else if ("long".equals(type))
        {
            ps.setLong(pos, (Long) value);
        }
    }
    
    private void finishEntry(Connection conn,TaskHistoryEntry the, Date ctime, Date cdate, String username) throws SQLException, IOException
    {
        if (the != null)
        {
            if (the.getAcceptDate() == null)
            {
                the.setAcceptDate(cdate);
                the.setAcceptTime(ctime);
            }
            if (the.getUsername() == null)
                the.setUsername(username);
            the.setFinishDate(cdate);
            the.setFinishTime(ctime);            
            insertThe(conn,the);
        }
    }
    
    private TaskHistoryEntry processHistoryEntry(Connection conn,TaskHistoryEntry the, Long documentId, Date ctime, Date cdate, String sourceUser, String sourceGuid, 
            String targetUser, String targetGuid, Boolean accepted, Boolean finished, String processName, Integer type) throws SQLException, IOException
    {
        if (type != null)
        {
            // wpisy z Workflow JBPM  olewam (bo i tak w du�ej cz�?ci by�yby przek�amane)
        }
        else
        {
            // tylko przetwarzam wpisy dla zada� do realizacji 
            if (processName != null && (smG.getString("doRealizacji").equals(processName.toLowerCase()) || ("obieg reczny".equals(processName.toLowerCase()))))
            {
                if (finished)
                {
                    // koniec zadania
                    finishEntry(conn,the, ctime, cdate, sourceUser);
                    return null;
                }
                else if (targetUser != null)
                {
                    if (accepted)
                    {
                        // akceptacja zadania
                        if (the != null)
                        {
                            the.setUsername(targetUser);
                            the.setAcceptDate(cdate);
                            the.setAcceptTime(ctime);
                        }
                        return the;
                    }
                    else
                    {
                        // dekretacja zadania do u�ytkownika
                        finishEntry(conn,the, ctime, cdate, sourceUser);
                        return new TaskHistoryEntry(documentId, cdate, ctime, 
                                targetUser, targetGuid, null, Boolean.FALSE);
                    }
                }
                else if ("rejestracja pisma".equals(processName))
                {
                    // rejestracja pisma
                    return new TaskHistoryEntry(documentId, cdate, ctime, 
                            sourceUser, DSDivision.ROOT_GUID, null, Boolean.FALSE);
                }
                else
                {                
                    // dekretacja na dzia�
                    finishEntry(conn,the, ctime, cdate, sourceUser);
                    return new TaskHistoryEntry(documentId, cdate, ctime, 
                            null, targetGuid, null, Boolean.FALSE);
                }
            }                    
        }
        return the;
    }

    
    
    private void insertThe(Connection conn, TaskHistoryEntry the) throws SQLException, IOException
    {        
        //Connection conn = Upgrade.getDatabaseConfiguration().getDataSource().getConnection();
        PreparedStatement ps3 = null;
        try
        {
        	if (Upgrade.getDatabaseConfiguration().getDatabaseType().equals(Docusafe.ORACLE_9))
        	{
        		ps3=conn.prepareStatement("insert into dsw_task_history_entry (id,documentId,username,divisionGuid,receiveDate,acceptDate,finishDate,receiveTime,acceptTime,finishTime,externalWorkflow) values (" +
        				("DSW_TASK_HISTORY_ENTRY_ID.NEXTVAL,") + "?,?,?,?,?,?,?,?,?,?)");
        	}
        	else if(Upgrade.getDatabaseConfiguration().getDatabaseType().equals(Docusafe.DB_FIREBIRD_1_5))
        	{
        		ps3=conn.prepareStatement("insert into dsw_task_history_entry (id,documentId,username,divisionGuid,receiveDate,acceptDate,finishDate,receiveTime,acceptTime,finishTime,externalWorkflow) values (" +
        				("gen_id(dsw_task_history_entry_id, 1),") + "?,?,?,?,?,?,?,?,?,?)");
        	}
        	else
        	{
        		ps3 = conn.prepareStatement("insert into dsw_task_history_entry (documentId,username,divisionGuid,receiveDate,acceptDate,finishDate,receiveTime,acceptTime,finishTime,externalWorkflow) values (?,?,?,?,?,?,?,?,?,?)");
        	}

            setValue(ps3, "long", 1, the.getDocumentId());
            setValue(ps3, "string", 2, the.getUsername());
            setValue(ps3, "string", 3, the.getDivisionGuid());
            setValue(ps3, "date", 4, the.getReceiveDate());
            setValue(ps3, "date", 5, the.getAcceptDate());
            setValue(ps3, "date", 6, the.getFinishDate());
            setValue(ps3, "timestamp", 7, the.getReceiveTime());
            setValue(ps3, "timestamp", 8, the.getAcceptTime());
            setValue(ps3, "timestamp", 9, the.getFinishTime());
            setValue(ps3, "boolean", 10, the.getExternalWorkflow());
            ps3.executeUpdate();
        }
        finally
        {
            if (ps3 != null) try { ps3.close(); } catch (Exception e) { }
        }
    }

    public void run() throws Exception
    {
        Connection conn = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        try
        {
            conn = Upgrade.getDatabaseConfiguration().getDataSource().getConnection();
            ps = conn.prepareStatement("delete from dsw_task_history_entry where externalWorkflow=0");
            ps.executeUpdate();
            ps.close();

            ps = conn.prepareStatement("select document_id from dso_document_asgn_history group by document_id");
            ResultSet rs = ps.executeQuery();

            while (rs.next())
            {
                try
                {
                    // kolejno dla ka�dego dokumentu przechodz� po jego historii i tworz� odpowiedni? histori� listy zada�
                    Long documentId = rs.getLong(1);
                    ps2 = conn.prepareStatement("select ctime, cdate, sourceUser, sourceGuid, targetUser, targetGuid, accepted, finished, processName, type from dso_document_asgn_history where document_id=? order by posn asc");
                    ps2.setLong(1, documentId);
                    ResultSet rs2 = ps2.executeQuery();
                    TaskHistoryEntry the = null;
                    while (rs2.next())
                    {
                        Date ctime = (Date) getValue(rs2, "timestamp", 1);
                        Date cdate = (Date) getValue(rs2, "date", 2);
                        String sourceUser = (String) getValue(rs2, "string", 3);
                        String sourceGuid = (String) getValue(rs2, "string", 4);
                        String targetUser = (String) getValue(rs2, "string", 5);
                        String targetGuid = (String) getValue(rs2, "string", 6);
                        Boolean accepted = (Boolean) getValue(rs2, "boolean", 7);
                        Boolean finished = (Boolean) getValue(rs2, "boolean", 8);
                        String processName = (String) getValue(rs2, "string", 9);
                        Integer type = (Integer) getValue(rs2, "integer", 10);
                        the = processHistoryEntry(conn,the, documentId, ctime, cdate, sourceUser, sourceGuid, targetUser, targetGuid, accepted, finished, processName, type);                                        
                    }
                    if (the != null)
                        insertThe(conn,the);
                }
                finally
                {
                    if (ps2 != null) try { ps2.close(); } catch (Exception e) { }
                }
            }
        }
        finally
        {
            if (ps != null) try { ps.close(); } catch (Exception e) { }
            if (conn != null) try { conn.close(); } catch (Exception e) { }
        } 
    }
}
