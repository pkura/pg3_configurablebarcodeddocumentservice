package pl.compan.docusafe.boot.upgrade.build;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.upgrade.ExecBase;
import pl.compan.docusafe.boot.upgrade.PrepareException;
import pl.compan.docusafe.boot.upgrade.Upgrade;

public class DrkTriggerUpgrade  extends ExecBase 
{
	
	private static final Logger log = LoggerFactory.getLogger(DrkTriggerUpgrade.class);
	
	
	public void init(Upgrade upgrade, Map params) throws Exception 
	{
	}

	public void prepare() throws PrepareException 
	{
	}

	public void run() throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		try 
		{
			conn = Upgrade.getDatabaseConfiguration().getDataSource().getConnection();

			ps = conn.prepareStatement("CREATE TRIGGER new_for_drk\n"
					+ "ON DSG_DOCTYPE_1\n"
					+ "FOR INSERT\n"
					+ "AS\n"
					+ "DECLARE @FLAG_ID numeric(19,0);\n"
					+ "DECLARE @DOCUMENT_ID numeric(19,0);\n"
					+ "DECLARE @NR_POLISY varchar(10);\n"
					+ "SET @NR_POLISY = (SELECT FIELD_0 FROM Inserted);\n"
					+ "SET @DOCUMENT_ID = (SELECT DOCUMENT_ID FROM Inserted);\n"
					+ "DECLARE trigger_cursor CURSOR FOR\n"
					+ "	SELECT DISTINCT lab.ID from DSO_LABEL lab\n"
					+ "	JOIN DSO_DOCUMENT_TO_LABEL doc_to_lab on doc_to_lab.LABEL_ID = lab.ID\n"
					+ "	JOIN DSG_DOCTYPE_1 nw on nw.DOCUMENT_ID = doc_to_lab.DOCUMENT_ID\n"
					+ "	WHERE nw.FIELD_0 = @NR_POLISY and lab.name like 'DRK%' and nw.DOCUMENT_ID != @DOCUMENT_ID\n"
					+ "OPEN trigger_cursor\n"
					+ "FETCH NEXT FROM trigger_cursor INTO @FLAG_ID\n"
					+ "WHILE @@FETCH_STATUS = 0\n"
					+ "	BEGIN\n"
					+ "		INSERT INTO dso_drk_mail_sender(FLAG_ID,DOCUMENT_ID,NR_POLISY,MAILED) values(@FLAG_ID,@DOCUMENT_ID,@NR_POLISY,0);\n" 
					+ "		FETCH NEXT FROM trigger_cursor INTO @FLAG_ID\n"
					+ "	END\n"
					+ "CLOSE trigger_cursor\n"
					+ "DEALLOCATE trigger_cursor\n");
			
			ps.executeUpdate();
		}
		catch(Exception e)
		{
			log.error("",e);
		}
		finally 
		{
			if (ps != null)
				try 
				{
					ps.close();
				} 
				catch (Exception e) {}				
			if (conn != null)
				try 
				{
					conn.close();
				} 
				catch (Exception e) {} 				
		}
	}	
}
