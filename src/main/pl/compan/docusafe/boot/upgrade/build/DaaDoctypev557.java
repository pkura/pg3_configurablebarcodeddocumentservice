package pl.compan.docusafe.boot.upgrade.build;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Map;

import pl.compan.docusafe.boot.upgrade.ExecBase;
import pl.compan.docusafe.boot.upgrade.PrepareException;
import pl.compan.docusafe.boot.upgrade.Upgrade;

/**
 * @author Tomasz Lipka</a>
 */
public class DaaDoctypev557 extends ExecBase {
	public void init(Upgrade upgrade, Map params) throws Exception {
	} 

	public void prepare() throws PrepareException {
	}

	public void run() throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = Upgrade.getDatabaseConfiguration().getDataSource()
					.getConnection();

			ps = conn.prepareStatement("update ds_doctype set content = ? "
					+ "where cn = ?");
			byte[] buf = xml.getBytes("utf-8");
			ps.setBinaryStream(1, new ByteArrayInputStream(buf), buf.length);
			ps.setString(2, "daa");
			ps.executeUpdate();
		} finally {
			if (ps != null)
				try {
					ps.close();
				} catch (Exception e) {
				}
			if (conn != null)
				try {
					conn.close();
				} catch (Exception e) {
				}
		}
	}

	private static final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
	+ "<doctype>"
	+ "<fields>"
	+ "<field id=\"10\" cn=\"STATUS\" column=\"FIELD_0\" name=\"Status\" type=\"enum\">"
	+"<enum-item id=\"10\" cn=\"PRZYJETY\" title=\"Przyj�ty\"/>"
	+"<enum-item id=\"20\" cn=\"W_REALIZACJI\" title=\"W realizacji\"/>"
	+"<enum-item id=\"30\" cn=\"ZAWIESZONY\" title=\"Zawieszony\"/>"
	+"<enum-item id=\"40\" cn=\"ZREALIZOWANY\" title=\"Zrealizowany\"/>"
	+"<enum-item id=\"50\" cn=\"ODRZUCONY\" title=\"Odrzucony\"/>"
	+"<enum-item id=\"60\" cn=\"ARCHIWALNY\" title=\"Archiwalny\"/>"
	+"</field>"
	+"<field id=\"20\" cn=\"DATA_WPLYWU\" column=\"FIELD_2\" name=\"Data\" type=\"date\"/>"
	+"<field id=\"30\" cn=\"AGENT\" column=\"FIELD_4\" name=\"Specjalista/OWCA\" type=\"long\"/>"
	+"<field id=\"40\" cn=\"AGENCJA\" column=\"FIELD_5\" name=\"Biuro regionalne/agencja/bank\" type=\"long\"/>"
	+"<field id=\"50\" cn=\"TYP_DOKUMENTU\" column=\"FIELD_3\" name=\"Typ dokumentu\" type=\"enum\" >"
	+"<enum-item id=\"10\" title=\"Ankieta danych\" cn=\"ANKIETA_DANYCH\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"30\" title=\"Aneks\" cn=\"ANEKS\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"50\" title=\"Certyfikat Citiinsurance\" cn=\"CITIINSURANCE\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"60\" title=\"Dane osoby fizycznej\" cn=\"DANE_OSOBY_FIZYCZNEJ\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"70\" title=\"Dyplom / �wiadectwo\" cn=\"DYPLOM_SWIADECTWO\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"110\" title=\"Karta egzaminacyjna\" cn=\"KARTA_EGZAMINACYJNA\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"130\" title=\"Karta szkoleniowa\" cn=\"KARTA_SZKOLENIOWA\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"131\" title=\"Klauzula o przetwarzaniu danych osobowych\" cn=\"KLAUZULA_OPDO\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"210\" title=\"NIP\" cn=\"NIP\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"230\" title=\"O�wiadczenie (PZCP)\" cn=\"PZCP\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"250\" title=\"Pe�nomocnictwo\" cn=\"PELNOMOCNICTWO\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"290\" title=\"Rachunek bankowy\" cn=\"RACHUNEK_BANKOWY\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"320\" title=\"REGON\" cn=\"REGON\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"596\" title=\"Reklamacja Przedstawiciela\" cn=\"REKLAMACJA_PRZEDSTAWICIELA\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"452\" title=\"Umowa fakturowa\" cn=\"UMOWA_FAKTUROWA\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"454\" title=\"Umowa na powierzenie przetwarzania danych\" cn=\"UMOWA_NA_POWIERZENIE_PRZETWARZANIA_DANYCH\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"456\" title=\"Umowa o zarz�dzanie\" cn=\"UMOWA_O_ZARZADZANIE\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"458\" title=\"Umowa specjalisty\" cn=\"UMOWA_SPECJALISTY\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"460\" title=\"Wpis do ewidencji lub KRS\" cn=\"KRS\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"480\" title=\"Wypowiedzenie umowy\" cn=\"WYPOWIEDZENIE_UMOWY\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"500\" title=\"Za�wiadczenie KRK\" cn=\"KRK\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"510\" title=\"Za�wiadczenie o uczestnictwie w szkoleniu (inne TU)\" cn=\"UCZESTNICTWO_SZKOLENIE_INNE_TU\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"520\" title=\"Za�wiadczenie o odbyciu szkolenia (AEGON)\" cn=\"ODBYCIE_SZKOLENIA\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"540\" title=\"Za�wiadczenie o uko�czeniu szkolenia\" cn=\"UKONCZENIE_SZKOLENIA\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"560\" title=\"Za�wiadczenie o uko�czeniu szkolenia / zdaniu egzaminu / licencja (inne TU)\" cn=\"ZASWIADCZENIE_INNE\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"580\" title=\"Za�wiadczenie o zdaniu egzaminu (AEGON)\" cn=\"ZDANIE_EGZAMINU\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"595\" title=\"Zg�oszenie do US\" cn=\"ZGLOSZENIE_DO_US\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"700\" title=\"Zmiana danych agenta\" cn=\"ZMIANA_DANYCH_AGENTA\" arg1=\"RODZAJ_AGENT\"/>"
	+"<enum-item id=\"640\" title=\"Aneks niestandardowy\" cn=\"ANEKS_NIESTANDARDOWY\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"620\" title=\"Aneks premiowy\" cn=\"ANEKS_PREMIOWY\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"630\" title=\"Aneks produktowy\" cn=\"ANEKS_PRODUKTOWY\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"20\" title=\"Ankieta danych agencji\" cn=\"ANKIETA_DANYCH\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"650\" title=\"Cesja/Porozumienie\" cn=\"CESJA_POROZUMIENIE\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"670\" title=\"Formularz cesji/porozumienia\" cn=\"FORMULARZ_CESJI_POROZUMIENIA\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"660\" title=\"Formularz prowizyjny\" cn=\"FORMULARZ_PROWIZYJNY\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"694\" title=\"Formularz wnioskowania zmian do um�w agencyjnych\" cn=\"FORM_WNIOSKOW_ZMIAN_DO_UMOW_AG\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"220\" title=\"NIP\" cn=\"NIP\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"260\" title=\"Pe�nomocnictwo agencji\" cn=\"PELNOMOCNICTWO\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"690\" title=\"Polisa OC\" cn=\"POLISA_OC\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"300\" title=\"Rachunek bankowy\" cn=\"RACHUNEK_BANKOWY\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"330\" title=\"REGON\" cn=\"REGON\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"610\" title=\"Umowa agencyjna\" cn=\"UMOWA_AGENCYJNA\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"680\" title=\"Umowa fakturowa\" cn=\"UMOWA_FAKTUROWA\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"691\" title=\"Umowa zlecenie\" cn=\"UMOWA_ZLECENIE\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"693\" title=\"Wniosek o zawarcie umowy agencyjnej\" cn=\"WNIOSEK_O_ZAWAR_UMOWY_AGENCYJNEJ\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"470\" title=\"Wpis do ewidencji lub KRS\" cn=\"KRS\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"490\" title=\"Wypowiedzenie umowy agencyjnej\" cn=\"WYPOWIEDZENIE_UMOWY\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"692\" title=\"Zg�oszenie US\" cn=\"ZGLOSZENIE US\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"695\" title=\"Zmiana danych agencji\" cn=\"ZMIANA_DANYCH_AGENCJI\" arg1=\"RODZAJ_AGENCJA\"/>"
	+"<enum-item id=\"601\" title=\"Kopia przelewu\" cn=\"KOPIA_PRZELEWU\" arg1=\"RODZAJ_RAPORT\"/>"
	+"<enum-item id=\"202\" title=\"Lista OFWCA do rejestracji\" cn=\"OWCA_REJESTRACJA\" arg1=\"RODZAJ_RAPORT\"/>"
	+"<enum-item id=\"204\" title=\"Lista OFWCA do wyrejestrowania\" cn=\"OWCA_WYKRESLENIE\" arg1=\"RODZAJ_RAPORT\"/>"
	+"<enum-item id=\"270\" title=\"Plany szkole�\" cn=\"PLANY_SZKOLEN\" arg1=\"RODZAJ_RAPORT\"/>"
	+"<enum-item id=\"280\" title=\"Protoko�y egzaminacyjne\" cn=\"PROTOKOLY_EGZAMINACYJNE\" arg1=\"RODZAJ_RAPORT\"/>"
	+"<enum-item id=\"310\" title=\"Raporty aktywno�ci szkoleniowej\" cn=\"RAPORT_AKTYWNOSCI_SZKOLENIOWEJ\" arg1=\"RODZAJ_RAPORT\"/>"
	+"<enum-item id=\"340\" title=\"Skierowanie na szkolenie\" cn=\"SZKOLENIE\" arg1=\"RODZAJ_RAPORT\"/>"
	+"<enum-item id=\"600\" title=\"Zg�oszenie szkolenia\" cn=\"ZGLOSZENIE_SZKOLENIA\" arg1=\"RODZAJ_RAPORT\"/>"
	+"<enum-item id=\"150\" title=\"Korespondencja przychodz�ca\" cn=\"KORESPONDENCJA_PRZYCHODZACA\" arg1=\"RODZAJ_INNE\"/>"
	+"<enum-item id=\"170\" title=\"Korespondencja wewn�trzna\" cn=\"KORESPONDENCJA_WEWNETRZNA\" arg1=\"RODZAJ_INNE\"/>"
	+"<enum-item id=\"190\" title=\"Korespondencja wychodz�ca\" cn=\"KORESPONDENCJA_WYCHODZACA\" arg1=\"RODZAJ_INNE\"/>"
	+"<enum-item id=\"271\" title=\"Przeniesienie obs�ugi\" cn=\"PRZENIESIENIE_OBSLUGI\" arg1=\"RODZAJ_INNE\"/>"
	+"<enum-item id=\"272\" title=\"Specyfikacja do przesy�ki\" cn=\"SPECYFIKACJA_DO_PRZESYLKI\" arg1=\"RODZAJ_INNE\"/>"
	+"<enum-item id=\"274\" title=\"Upowa�nienie do konta administracyjnego MP\" cn=\"UPOW_DO_KONTA_ADMINA_MP\" arg1=\"RODZAJ_INNE\"/>"
	+"<enum-item id=\"275\" title=\"Wniosek o niestandard\" cn=\"UPOW_DO_KONTA_ADMINA_MP\" arg1=\"RODZAJ_INNE\"/>"
	+"<enum-item id=\"273\" title=\"Zwrot korespondencji\" cn=\"ZWROT_KORESPONDENCJI\" arg1=\"RODZAJ_INNE\"/>"
	+"</field>"
	+"<field id=\"55\" cn=\"KLASA_RAPORTU\" column=\"FIELD_6\" name=\"Klasa raportu\" type=\"enum\" length=\"50\" >"
	+"<enum-item id=\"1\" title=\"Szkolenie 36h\" cn=\"SZKOLENIE_36\" />"
	+"<enum-item id=\"2\" title=\"Szkolenie 48h\" cn=\"SZKOLENIE_48\" />"
	+"<enum-item id=\"3\" title=\"Szkolenie 152h\" cn=\"SZKOLENIE_128\" />"
	+"<enum-item id=\"4\" title=\"Szkolenie dla Zarz�du\" cn=\"SZKOLENIE_DLA_ZARZADU\" />"
	+"<enum-item id=\"5\" title=\"Inne\" cn=\"INNE\" />"
	+"</field>"
	+"<field id=\"60\" cn=\"RODZAJ_SIECI\" column=\"FIELD_1\" name=\"Rodzaj sieci\" type=\"enum\" length=\"50\" >"
	+"<enum-item id=\"1\" title=\"Sie� w�asna\" cn=\"SIEC_WLASNA\" arg1=\"RODZAJ_SIECI\"/>"
	+"<enum-item id=\"2\" title=\"Sie� zewn�trzna\" cn=\"SIEC_ZEWNETRZNA\" arg1=\"RODZAJ_SIECI\"/>"
	+"<enum-item id=\"3\" title=\"Banki\" cn=\"BANKI\" arg1=\"RODZAJ_SIECI\"/>"
	+"</field>"
	+"</fields>"
	+"</doctype>"
	;	
}
