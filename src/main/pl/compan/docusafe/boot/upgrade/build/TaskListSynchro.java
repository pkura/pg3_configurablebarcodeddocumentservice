package pl.compan.docusafe.boot.upgrade.build;

import java.util.Map;

import pl.compan.docusafe.boot.upgrade.ExecBase;
import pl.compan.docusafe.boot.upgrade.PrepareException;
import pl.compan.docusafe.boot.upgrade.Upgrade;
import pl.compan.docusafe.core.AvailabilityManager;

public class TaskListSynchro extends ExecBase
{

	public void init(Upgrade upgrade, Map<String, String> params)throws Exception
	{

	}

	public void prepare() throws PrepareException 
	{

	}

	public void run() throws Exception
	{
		AvailabilityManager.getAvailableMap().put("reloadTasklistOnBoot", true);
	}

}
