package pl.compan.docusafe.boot.upgrade.build;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.boot.upgrade.ExecBase;
import pl.compan.docusafe.boot.upgrade.PrepareException;
import pl.compan.docusafe.boot.upgrade.Upgrade;
import pl.compan.docusafe.core.bookmarks.BookmarkManager;

import com.google.gson.Gson; 
/**
 * Upgrade ustawia domy�lne zak�adki
 */
public class SetDefaultBookmarks extends ExecBase
{
	private Long prefNodeId;
	private Long parentId;
	private Gson gson = new Gson();

	public void init(Upgrade upgrade, Map<String, String> params)throws Exception {}

	public void prepare() throws PrepareException {}
	
	 private Long[] getDefaultsBookmarks() throws SQLException, IOException
    {        
    	Long[] defIds = new Long[BookmarkManager.AVAILABLE_BOOKMARKS_IDS.size()];
		for(int i=0;i<BookmarkManager.AVAILABLE_BOOKMARKS_IDS.size();i++)
		{
			defIds[i] = BookmarkManager.AVAILABLE_BOOKMARKS_IDS.get(i);
		}
    	return defIds;
    }
	 
	public boolean preparePreferences(Connection conn) throws Exception
	{
		PreparedStatement ps = null;
		conn.setAutoCommit(true);
        try
        {
    	   ps = conn.prepareStatement("SELECT id FROM ds_prefs_node WHERE username is null AND parent_id is null");
    	   ResultSet  rs = ps.executeQuery();
           if (rs.next())
           {
        	   parentId = rs.getLong("id");
           }
           else
           {
        	   parentId = 1L;
           }
        	
        	ps = conn.prepareStatement("SELECT id FROM ds_prefs_node WHERE NAME='bookmarks' AND parent_id=?");
        	ps.setLong(1, parentId);
        	rs = ps.executeQuery();
        	if (!rs.next())
        	{
        		if (Upgrade.getDatabaseConfiguration().getDatabaseType().equals(Docusafe.ORACLE_9))
        		{
        			ps=conn.prepareStatement("INSERT INTO ds_prefs_node (ID,PARENT_ID,NAME,USERNAME) VALUES (DS_PREFS_NODE_ID.NEXTVAL,?,'bookmarks',null)");
        		} 
        		else if (Upgrade.getDatabaseConfiguration().getDatabaseType().equals(Docusafe.DB_FIREBIRD_1_5)) 
        		{
        			ps=conn.prepareStatement("INSERT INTO ds_prefs_node (ID,PARENT_ID,NAME,USERNAME) VALUES (gen_id(DS_PREFS_NODE_ID, 1),?,'bookmarks',null)");
        		}
        		else
        		{
        			ps=conn.prepareStatement("INSERT INTO ds_prefs_node(parent_id,name,username) VALUES (?,'bookmarks',null)");
        		}
        		ps.setLong(1, parentId);
        		ps.executeUpdate();

        		ps = conn.prepareStatement("SELECT id FROM ds_prefs_node ORDER BY id desc");
        		rs = ps.executeQuery();
                if (rs.next())
                {
                	prefNodeId = rs.getLong("id");
                }
                LoggerFactory.getLogger("przemek").debug("PREF NODE ID: {}",prefNodeId);
                
        	}
        	else
        	{
        		prefNodeId = rs.getLong("id");
        	}

            ps = conn.prepareStatement("SELECT * FROM ds_prefs_value WHERE pref_key='list' AND node_id=?");
            ps.setLong(1, prefNodeId);
            rs = ps.executeQuery();
            if (!rs.next())
            {
    			ps=conn.prepareStatement("INSERT INTO ds_prefs_value(node_id,pref_key,username,pref_value) VALUES (?,'list',null,?)");
    			ps.setLong(1, prefNodeId);
    			ps.setString(2, gson.toJson(getDefaultsBookmarks()));
            }
            else
            {
            	ps=conn.prepareStatement("UPDATE ds_prefs_value SET pref_value=? WHERE node_id=? and pref_key='list'");
            	ps.setString(1, gson.toJson(getDefaultsBookmarks()));
            	ps.setLong(2, prefNodeId);
            }
            ps.executeUpdate();
            
        }
        finally
        {
            if (ps != null) try { ps.close(); } catch (Exception e) { }
        }
		
		return true;
	}

	public void run() throws Exception
	{
		 Connection conn = null;
        try
        {
        	conn = Upgrade.getDatabaseConfiguration().getDataSource().getConnection();
        	preparePreferences(conn);
        }
        finally
        {
            if (conn != null) try { conn.close(); } catch (Exception e) { }
        } 
	}

}
