package pl.compan.docusafe.boot.upgrade.build;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.boot.upgrade.ExecBase;
import pl.compan.docusafe.boot.upgrade.Upgrade;
import pl.compan.docusafe.boot.upgrade.PrepareException;
import java.util.Map;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.io.ByteArrayInputStream;

/**
 * Dodanie do bazy typu dokumentu normal
 * 
 * @author Mariusz Kilja�czyk
 * 
 */
public class NormalDoctype extends ExecBase
{
    public void init(Upgrade upgrade, Map params) throws Exception
    {
    }

    public void prepare() throws PrepareException
    {
    }

    public void run() throws Exception
    {
        Connection conn = null;
        PreparedStatement ps = null;
        try
        {
            long count;
            conn = Upgrade.getDatabaseConfiguration().getDataSource().getConnection();
            if(!Upgrade.getDatabaseConfiguration().getDatabaseType().equals(Docusafe.ORACLE_9))
            {
                ps = conn.prepareStatement("select count(ID) from DS_DOCUMENT_KIND where cn = 'normal'");// +

                ResultSet rs = ps.executeQuery();
                rs.next();
                count = rs.getLong(1);
            }
            else
            {
                count = 1;
            }
            if(count==0)
            {
                if(Upgrade.getDatabaseConfiguration().getDatabaseType().equals(Docusafe.DB_FIREBIRD_1_5))
                {                    
                    ps = conn.prepareStatement("insert into ds_document_kind (ID,HVERSION" +
                            ",NAME,TABLENAME,ENABLED,CONTENT,CN) values (" +
                            ("gen_id(ds_document_kind_id, 1),") +
                            "0,'Zwyk�y dokument','dsg_normal_dockind',1,?," +
                            "'normal');");
                    byte[] buf = pl.compan.docusafe.boot.upgrade.build.NormalDoctype.xml.getBytes("utf-8");
                   ps.setBinaryStream(1, new ByteArrayInputStream(buf), buf.length);
                   ps.executeUpdate();
                }
                else if(Upgrade.getDatabaseConfiguration().getDatabaseType().equals(Docusafe.ORACLE_9))
                {
                    
                    ps = conn.prepareStatement("insert into ds_document_kind (ID,HVERSION" +
                            ",NAME,TABLENAME,ENABLED,CONTENT,CN) values (" +
                            ("DS_DOCUMENT_KIND_ID.NEXTVAL,") +
                            "0,'Zwyk�y dokument','dsg_normal_dockind',1,?," +
                            "'normal');");
                    byte[] buf = pl.compan.docusafe.boot.upgrade.build.NormalDoctype.xml.getBytes("utf-8");
                   ps.setBinaryStream(1, new ByteArrayInputStream(buf), buf.length);
                   ps.executeUpdate();
                }
                else if(Upgrade.getDatabaseConfiguration().getDatabaseType().equals(Docusafe.DB_SQLSERVER2000))
                {                    
                    ps = conn.prepareStatement("insert into ds_document_kind (HVERSION" +
                            ",NAME,TABLENAME,ENABLED,CONTENT,CN) values (" +
                            "0,'Zwyk�y dokument','dsg_normal_dockind',1,?," +
                            "'normal');");
                    byte[] buf = pl.compan.docusafe.boot.upgrade.build.NormalDoctype.xml.getBytes("utf-8");
                   ps.setBinaryStream(1, new ByteArrayInputStream(buf), buf.length);
                   ps.executeUpdate();
                }
            }
        }
        finally
        {
            if (ps != null) try { ps.close(); } catch (Exception e) { }
            if (conn != null) try { conn.close(); } catch (Exception e) { }
        }
    }
    
    private static final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
        "<doctype>" +
        "<fields>" +
        "</fields>" +
        "</doctype>";
}
