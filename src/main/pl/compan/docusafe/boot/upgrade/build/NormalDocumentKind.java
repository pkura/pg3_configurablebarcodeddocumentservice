package pl.compan.docusafe.boot.upgrade.build;

import pl.compan.docusafe.boot.upgrade.ExecBase;
import pl.compan.docusafe.boot.upgrade.Upgrade;
import pl.compan.docusafe.boot.upgrade.PrepareException;

import java.util.Map;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.io.ByteArrayInputStream;
/* User: Administrator, Date: 2006-11-21 17:34:49 */

/**
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class NormalDocumentKind extends ExecBase
{
    public void init(Upgrade upgrade, Map params) throws Exception
    {
    }

    public void prepare() throws PrepareException
    {
    }

    public void run() throws Exception
    {
        Connection conn = null;
        PreparedStatement ps = null;
        try
        {
            conn = Upgrade.getDatabaseConfiguration().getDataSource().getConnection();
         
            ps = conn.prepareStatement("insert into ds_document_kind " +
                    "(id, hversion, name, tablename, enabled, content, cn, logic) values " +
                    "(1, 0, ?, ?, 1, ?, ?, ?)");

            ps.setString(1, "Zwyk�y dokument");
            ps.setString(2, "DSG_NORMAL_DOCKIND");
            byte[] buf = pl.compan.docusafe.boot.upgrade.build.NormalDocumentKind.xml.getBytes("utf-8");
            ps.setBinaryStream(3, new ByteArrayInputStream(buf), buf.length);
            ps.setString(4, "normal");
            ps.setString(5, "pl.compan.docusafe.core.dockinds.NormalLogic");
            ps.executeUpdate();
            
        }
        finally
        {
            if (ps != null) try { ps.close(); } catch (Exception e) { }
            if (conn != null) try { conn.close(); } catch (Exception e) { }
        }
    }

    private static final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
        "<doctype><fields>" +        
        "</fields></doctype>";
}


