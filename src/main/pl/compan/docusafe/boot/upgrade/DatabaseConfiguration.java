package pl.compan.docusafe.boot.upgrade;

import pl.compan.docusafe.boot.BootException;
import pl.compan.docusafe.boot.Docusafe;

import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
/* User: Administrator, Date: 2005-09-21 14:37:14 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: DatabaseConfiguration.java,v 1.6 2009/07/28 10:54:24 wkuty Exp $
 */
public class DatabaseConfiguration
{
    private String databaseType;
    private String driverClass;
    private String username;
    private String password;
    private String url;

    public DatabaseConfiguration(File dsConfig) throws IOException
    {
        if (!dsConfig.exists())
            throw new IOException("Nie znaleziono pliku "+dsConfig.getAbsolutePath());

        Properties dsProperties = Upgrade.loadPropertiesFile(dsConfig);

        // Warunek dla wersji Docusafe starszych ni� 75
        databaseType = dsProperties.getProperty("database.type");
        if (databaseType == null)
        {
            databaseType = Docusafe.DB_FIREBIRD_1_5;
        }
        else if (Docusafe.getDefaultDriver(databaseType) == null)
        {
            throw new BootException("Nieznany rodzaj bazy danych: "+databaseType);
        }

        driverClass = dsProperties.getProperty("driver.class");
        if (driverClass == null)
        {
            driverClass = Docusafe.getDefaultDriver(databaseType);
        }

        username = dsProperties.getProperty("username");
        password = dsProperties.getProperty("password");
        url = dsProperties.getProperty("url");
    }

    public Connection getConnection() throws SQLException
    {
        return getDataSource().getConnection();
    }

    public DataSource getDataSource()
    {
        return Docusafe.getDataSource();        
    }

    public String getDatabaseType()
    {
        return databaseType;
    }

    public String getDriverClass()
    {
        return driverClass;
    }

    public String getUsername()
    {
        return username;
    }

    public String getPassword()
    {
        return password;
    }

    public String getUrl()
    {
        return url;
    }
}
