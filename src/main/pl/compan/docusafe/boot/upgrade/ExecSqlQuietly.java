package pl.compan.docusafe.boot.upgrade;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Wykonuje sql'a ignoruj�c wyj�tki
 * @author Micha� Sankowski
 */
public class ExecSqlQuietly extends ExecSql{

	private final static Logger log = LoggerFactory.getLogger(ExecSqlQuietly.class);

	@Override
	public void run() throws Exception {
		try{
			super.run();
		} catch(Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
	}
}
