package pl.compan.docusafe.boot.upgrade;

import org.apache.commons.logging.Log;
import pl.compan.docusafe.boot.Docusafe;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Map;
/* User: Administrator, Date: 2005-09-14 11:13:29 */

/**
 * Wykonuje skrypt SQL przekazany jako warto�� parametru o nazwie
 * <b>script.TYP_BAZY_DANYCH</b>, gdzie TYP_BAZY_DANYCH to jedna
 * z warto�ci sta�ych Docusafe.DB_*.
 * <p>
 * Skrypt powinien sk�ada� si� z szeregu polece� SQL lub dyrektyw
 * charakterystycznych dla tej klasy rozdzielonych �rednikami.
 * <p>
 * Polecenia SQL musz� by� zapisane w dialekcie zrozumia�ym dla 
 * bazy danych, natomiast dyrektywy maj� posta� <b>@dyrektywa ;</b>.
 * Mo�liwe dyrektywy to:
 * <ul>
 * <li>@ignore-exceptions - powoduje zignorowanie wyj�tku, jaki mo�e
 * wygenerowa� nast�pne w kolejno�ci polecenie SQL.
 * </ul>
 * Po ostatnim poleceniu w skrypcie SQL powinien znale�� si� znak
 * nowej linii.
 * <p>
 * Skrypty generowane przez Enterprise Manager (SQL Server) wymagaj�
 * poprawek przed uruchomieniem ich przez t� klas�.  Nale�y z nich
 * usun�� wyst�pienia polecenia "GO" oraz doda� �redniki.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ExecSql.java,v 1.9 2006/02/06 16:05:47 lk Exp $
 */
public class ExecSql extends ExecBase
{
    private static final Log log = Upgrade.log;

    private DatabaseConfiguration cfg;
    private String script;
    private boolean disabled;

    public void init(Upgrade upgrade, Map<String, String> params) throws Exception
    {
        cfg = Upgrade.getDatabaseConfiguration();
        script = params.get("script."+cfg.getDatabaseType());

        // sprawdzenie, czy aplikacja posiada modu�y wymagane do wykonania
        // tego skryptu
        String modules = params.get("modules");
        if (modules != null)
        {
            String[] moduleNames = modules.split("[, ]+");
            for (int i=0; i < moduleNames.length; i++)
            {
                if (!Docusafe.moduleAvailable(moduleNames[i]))
                {
                    log.info("Pomijanie skryptu "+script+", poniewa� wymaga on " +
                        "modu�u "+moduleNames[i]);
                    disabled = true;
                    break;
                }
            }
        }
    }

    public void prepare() throws PrepareException
    {
        if (disabled)
            return;

        if (script == null)
            throw new PrepareException("Nie okre�lono skryptu dla bazy "+cfg.getDatabaseType());

        InputStream is = Docusafe.getServletContext().getResourceAsStream(script);
        if (is == null)
            throw new PrepareException("Nie znaleziono skryptu "+script);
        try 
        { 
            is.close();
        }
        catch (IOException e) 
        {
            throw new PrepareException(e.getMessage(), e);
        }
    }

    public void run() throws Exception
    {
        if (disabled)
            return;

        BufferedReader reader = new BufferedReader(new InputStreamReader(
            Docusafe.getServletContext().getResourceAsStream(script), "iso-8859-2"));
        StringBuilder sql = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null)
        {
            sql.append(line);
            sql.append("\n");
        }

        String[] queries = sql.toString().split(";(?!--@ignore-semicolon)\\s*[\\n\\r]");
            //"(;\\s*(--.*)?[\\n\\r])|([\\n\\r]\\s*--.*[\\n\\r])");

        if (Upgrade.log.isDebugEnabled())
            Upgrade.log.debug("Zapytania: "+ Arrays.asList(queries));

        if (queries != null && queries.length > 0)
        {
            Connection conn = null;
            try
            {
                conn = Upgrade.getDatabaseConfiguration().getConnection();
                Statement st = conn.createStatement();

                //for (int i=0; i < queries.length; i++)
                for (String query : queries)
                {
                    if (query == null || query.trim().length() == 0)
                        continue;

                    if (parseFlag(query))
                        continue;

                    if (query.startsWith("--"))
                        continue;

                    Upgrade.log.info(query);
                    Docusafe.printk("SQL: "+query);

                    try
                    {
                        st.executeUpdate(query);
                    }
                    catch (SQLException e)
                    {
                        if ((flags & IGNORE_EXCEPTIONS) == 0)
                        {
                            Upgrade.log.error(e.getMessage(), e);
                            throw e;
                        }
                        else
                        {
                            Docusafe.printk("ZIGNOROWANY WYJ�TEK (IGNORE_EXCEPTIONS): "+e.toString());
                            Upgrade.log.warn("ZIGNOROWANY WYJ�TEK (IGNORE_EXCEPTIONS): "+e.toString());
                        }
                    }

                    flags = 0;
                }
            }
            finally
            {
                if (conn != null) try { conn.close(); } catch (Exception e) { }
            }
        }
    }

    private int flags = 0;
    private static final int IGNORE_EXCEPTIONS = 1;

    private boolean parseFlag(String flag) throws Exception
    {
        if (!flag.startsWith("--@"))
            return false;

        if (flag.equals("--@ignore-exceptions"))
        {
            flags |= IGNORE_EXCEPTIONS;
        }
        // else if
        else
            throw new Exception("Nieznana flaga: "+flag);

        Docusafe.printk("Znaleziono flag� "+flag);
        Upgrade.log.error("Znaleziono flag� "+flag);

        return true;
    }
}
