package pl.compan.docusafe.boot.upgrade;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import pl.compan.docusafe.boot.Docusafe;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
/* User: Administrator, Date: 2005-09-14 11:11:58 */

/**
 * Wykonuje aktualizacj� aplikacji.  Je�eli katalog domowy nie
 * istnieje lub istnieje, ale znaleziono w nim plik noupgrade.txt,
 * aktualizacja nie jest wykonywana.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Upgrade.java,v 1.16 2010/05/06 14:24:02 mariuszk Exp $
 */
public class Upgrade
{
    public static final Log log = LogFactory.getLog("upgrade_log");

    public void start() throws Exception
    {
        // pierwszy przebieg - za�adowanie klas
        // drugi przebieg - prerequisities
        // trzeci przebieg - upgrade

        if (!Docusafe.getHome().exists())
        {
            Docusafe.printk("Nie istnieje katalog domowy ("+Docusafe.getHome()+"), " +
                "aktualizacja nie zostanie uruchomiona");
            return;
        }

        if (new File(Docusafe.getHome(), "noupgrade.txt").exists())
        {
            Docusafe.printk("W katalogu domowym ("+Docusafe.getHome()+") " +
                "znaleziono plik noupgrade.txt, aktualizacja nie zostanie uruchomiona");
            return;
        }

        // wersja dotychczas zainstalowanej aplikacji
        /*final */int appVersion = getAppVersion();
        Docusafe.printk("Obecna wersja aplikacji: "+appVersion);

        if (appVersion > Docusafe.getDistNumber())
        {
            throw new Exception("Obecna wersja aplikacji jest ni�sza ni� poprzednia, " +
                "nie mo�na kontynuowa� uruchamiania");
        }

        if (appVersion == Docusafe.getDistNumber())
        {
            Docusafe.printk("Wersja aplikacji pozosta�a niezmieniona, nie ma potrzeby "+
                "wykonywania aktualizacji");
            return;
        }

        SAXReader saxReader = new SAXReader();
        Document document = saxReader.read(
            Docusafe.getServletContext().getResourceAsStream("/WEB-INF/upgrade/upgrade.xml"));

        List<String> prepareErrors = new LinkedList<String>();
        List<ExecBase> execs = new LinkedList<ExecBase>(); // List<ExecBase>
        List<List> changelogs = new LinkedList<List>(); // List<List>

        // pierwszy przebieg - metody prepare
        for (int i=appVersion+1; i <= Docusafe.getDistNumber(); i++)
        {
            Element elVersion = firstElement(document, "/upgrade/version[@id='"+i+"']");

            if (elVersion != null)
            {
                Docusafe.printk("Znaleziono aktualizacj� do wersji "+i);

                List lstelExec = elVersion.elements("exec");
                for (Iterator iter=lstelExec.iterator(); iter.hasNext(); )
                {
                    Element elExec = ((Element) iter.next());
                    String clazz = elExec.attributeValue("class");
                    if (clazz == null)
                        throw new Exception("Nie znaleziono atrybutu class w elemencie exec");


                    String extras = elExec.attributeValue("extras");
                    if (extras != null && !Docusafe.hasExtra(extras))
                    {
                        log.info("Pomijanie "+clazz+", extras="+extras);
                        continue;
                    }

                    String modules = elExec.attributeValue("modules");
                    if (modules != null && !Docusafe.moduleAvailable(modules))
                    {
                        log.info("Pomijanie "+clazz+", modules="+modules);
                        continue;
                    }

                    String licensee = elExec.attributeValue("licensee");
                    if (licensee != null &&
                        !Docusafe.getLicense().getCustomer().equals(licensee))
                    {
                        log.info("Pomijanie "+clazz+" licensee="+licensee);
                        continue;
                    }

                    Class.forName(clazz);
                    ExecBase exec = (ExecBase) Class.forName(clazz).newInstance();
                    Map params = getParams(elExec);
                    log.info("init "+clazz+" "+params);
                    exec.init(this, params);
                    try
                    {
                        log.info("prepare "+clazz);
                        exec.prepare();
                    }
                    catch (PrepareException e)
                    {
                    	e.printStackTrace();
                        prepareErrors.add(e.getMessage() + " w wersji:" + i);
                    }
                    exec.setVersion(i);
                    execs.add(exec);
                }

                // do listy changelogs dodane zostan� listy obiekt�w String
                // nast�pnie lista changelogs zostanie odwr�cona (aby najnowsze
                // zmiany znalaz�y si� na pocz�tku)
                Element elChlog = elVersion.element("changelog");
                if (elChlog != null)
                {
                    Object entries = elChlog.createXPath("entry").evaluate(elChlog);
                    List<String> lstEntries = new LinkedList<String>();

                    if (entries instanceof Element)
                    {
                        entries = Arrays.asList(new Object[] { entries } );
                    }

                    for (Iterator iter=((List) entries).iterator(); iter.hasNext(); )
                    {
                        Element el = (Element) iter.next();
                        String extras = el.attributeValue("extras");
                        if (extras == null ||
                            (extras != null && Docusafe.hasExtra(extras)))
                        {
                            String text = el.getTextTrim();
                            if (text != null)
                                lstEntries.add(text);
                        }
                    }

                    changelogs.add(lstEntries);
                }
            }
        }

        if (prepareErrors.size() > 0)
            throw new Exception("Aktualizacja aplikacji nie jest mo�liwa, skoryguj " +
                "nast�puj�ce problemy: "+formatMessages(prepareErrors));

        for (Iterator iter=execs.iterator(); iter.hasNext(); )
        {            
            ExecBase exec = (ExecBase) iter.next();
            if (appVersion+1 < exec.getVersion())
            {
                appVersion = exec.getVersion()-1;
                updateAppVersion(appVersion);
            }
            log.info("run "+exec.getClass().getName());
            exec.run();
        }

        if (changelogs.size() > 0)
        {
            PrintWriter out = new PrintWriter(
                new OutputStreamWriter(
                    new FileOutputStream(
                        new File(Docusafe.getHome(), "changelog."+Docusafe.getDistNumber()+".xml")),
                    "utf-8"));

            out.println("<?xml version=\"1.0\" ?>\n<changelog>\n");

            for (Iterator iter=changelogs.iterator(); iter.hasNext(); )
            {
                for (Iterator i=((Iterator) ((List) iter.next()).iterator()); i.hasNext(); )
                {
                    out.println("<entry>");
                    out.println((String) i.next());
                    out.println("</entry>");
                }
            }

            out.println("\n</changelog>\n");
            out.close();
        }

        updateAppVersion(Docusafe.getDistNumber());
    }

    private Element firstElement(Node ctx, String xpath)
    {
        Object o = ctx.createXPath(xpath).evaluate(ctx);
        if (o instanceof Element)
        {
            return (Element) o;
        }
        else if (o instanceof List && ((List) o).size() > 0)
        {
            return (Element) ((List) o).get(0);
        }
        return null;
    }

    private Map<String, String> getParams(Element elParamsContainer)
    {
        Map<String, String> params = new HashMap<String, String>();

        List lstParams = elParamsContainer.elements("param");
        for (Object o : lstParams)
        {
            Element el = (Element) o;
            params.put(el.attributeValue("name"), el.attributeValue("value"));
        }

        return params;
    }

    public static final String APP_VERSION = "APP_VERSION";

    private int getAppVersion() throws IOException, SQLException, Exception
    {
        Connection conn = null;
        try
        {
        	System.out.println("��czy");
        	boolean connectSuccess = false;
        	while(!connectSuccess)
        	{
	        	try
	        	{
	        		conn = getDatabaseConfiguration().getDataSource().getConnection();
	        		connectSuccess = true;
	        	}
	        	catch (Exception e) 
	        	{
					log.error(e.getMessage(),e); 
					Thread.sleep(60*1000);
				}
        	}
            PreparedStatement ps = conn.prepareStatement(
                "select var_value from ds_docusafe where var_name = ?");
            ps.setString(1, APP_VERSION);
            ResultSet rs = ps.executeQuery();

            if (!rs.next())
                throw new Exception("W tabeli DS_DOCUSAFE nie znaleziono zmiennej "+
                    APP_VERSION);

            int version;
            try
            {
                String string = rs.getString(1);
                version = Integer.parseInt(string);
                
            }
            catch (Exception e)
            {
                throw new Exception("Numer wersji w tabeli DS_DOCUSAFE jest " +
                    "niepoprawny");
            }

            if (rs.wasNull())
                throw new Exception("W tabeli DS_DOCUSAFE nie znaleziono numeru wersji");

            ps.close();

            return version;
        }
        catch (SQLException e)
        {
            throw e;
        }
        finally
        {
            if (conn != null) try { conn.close(); } catch (Exception e) { }
        }
    }

    private void updateAppVersion(int version) throws IOException, SQLException
    {
        Connection conn = null;
        try
        {
            conn = getDatabaseConfiguration().getDataSource().getConnection();
            PreparedStatement ps = conn.prepareStatement(
                "update ds_docusafe set var_value = ? where var_name = ?");
            ps.setString(1, String.valueOf(version));
            ps.setString(2, APP_VERSION);
            ps.executeUpdate();
        }
        finally
        {
            if (conn != null) try { conn.close(); } catch (Exception e) { }
        }
    }

    private String formatMessages(List messages)
    {
        StringBuilder result = new StringBuilder();
        for (Iterator iter=messages.iterator(); iter.hasNext(); )
        {
            result.append("\n- ");
            result.append(iter.next());
        }

        return result.toString();
    }

    public static DatabaseConfiguration getDatabaseConfiguration() throws IOException
    {
        return new DatabaseConfiguration(new File(Docusafe.getHome(), "datasource.config"));
    }

    static protected Properties loadPropertiesFile(File file) throws IOException
    {
        FileInputStream is = new FileInputStream(file);
        Properties properties = new Properties();
        properties.load(is);
        is.close();
        return properties;
    }
}
