package pl.compan.docusafe.boot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.web.filter.AuthFilter;

import com.opensymphony.webwork.ServletActionContext;

public class LanguageUtil {

	 private static final Logger log = LoggerFactory.getLogger(Docusafe.class);
	
	 public static String getCurrentLanguage()
	    {
	    	javax.servlet.http.HttpServletRequest request = null;
	    	javax.servlet.http.HttpSession session = null;
	    	try
	    	{
	    		request = ServletActionContext.getRequest();
	    		if (request != null)
	    			session = request.getSession(true);
	    	}
	    	catch (Exception e)
	    	{
	    		log.warn("Blad w czasie pobierania requestu",e);
	    	}
	    	String tempLang = null;
	  		if (session != null)
	        {
	            try
	            {
	                String lang = (String) session.getAttribute(AuthFilter.LANGUAGE_KEY);
	                tempLang = lang;
	                if (lang != null)
	                {
	                    return lang;
	                }
	            }
	            catch (Exception exc)
	            {
	                tempLang = null;
	                log.debug("",exc);
	            }
	        }
	        if(tempLang==null)
	        {
	        	//OtherSettingsAction.class.
	        	if (Docusafe.getAdditionProperty("locale") != null)
	        	{
	        		return Docusafe.getAdditionProperty("locale");
	        	}
	        }
	        return "pl";
	    }
}
