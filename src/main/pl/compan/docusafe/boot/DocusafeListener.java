package pl.compan.docusafe.boot;

import org.springframework.web.context.ContextLoaderListener;
import pl.compan.docusafe.service.ServiceException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
/* User: Administrator, Date: 2005-09-06 10:39:47 */
import java.io.PrintStream;
import java.text.*;
/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: DocusafeListener.java,v 1.11 2008/10/30 14:16:42 pecet1 Exp $
 */
public class DocusafeListener implements ServletContextListener, HttpSessionListener
{
	private final static PrintStream out = System.out;
	private final static String FREE_MEMORY_FORMAT ="Wolna pamiec : %5.2f%%";
	private final static String MAX_MEMORY_FORMAT ="Maksymalna zajetosc : %5.2f%%";
	
	static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");



    public void contextInitialized(ServletContextEvent event)
    {
    	//Tu moze byc system out
//    	System.out.println("[START] Docusafe rozpoczyna prace o "+dateFormat.format(new java.util.Date()));
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent)
    {
        try
        {   
        	out.println("[STOP] Docusafe konczy prace o " + dateFormat.format(new java.util.Date()));
        	out.println(String.format(FREE_MEMORY_FORMAT, (double)Runtime.getRuntime().freeMemory()/(double)Runtime.getRuntime().totalMemory()*100));
        	out.println(String.format(MAX_MEMORY_FORMAT, (double)Runtime.getRuntime().totalMemory()/(double)Runtime.getRuntime().maxMemory()*100));
        	
            Docusafe.stop();
        }
        catch (ServiceException e)
        {
            servletContextEvent.getServletContext().log(e.getMessage(), e);
        }
    }

    public void sessionCreated(HttpSessionEvent event)
    {
        Docusafe.sessionCreated(event);
    }

    public void sessionDestroyed(HttpSessionEvent event)
    {
        Docusafe.sessionDestroyed(event);
    }
}
