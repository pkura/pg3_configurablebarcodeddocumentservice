package pl.compan.docusafe.boot;

import org.apache.commons.beanutils.DynaBean;
import org.directwebremoting.guice.spring.WebApplicationContextLoader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.context.support.GenericWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import pl.com.simple.simpleerp.dokzak.ObjectFactory;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.rest.RestConfiguration;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import javax.el.ELContext;
import javax.el.ELResolver;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.jsp.JspApplicationContext;
import javax.servlet.jsp.JspFactory;
import java.beans.FeatureDescriptor;
import java.io.File;
import java.util.Collections;
import java.util.Iterator;

/**
 * Klasa umo�liwiaj�ca konfiguracj� servlet�w dynamicznie - z pomini�ciem web.xml
 * Dzia�a tylko z kontenerami wspieraj�cymi Servlet 3.0 (Tomcat 7+)
 *  http://www.codejava.net/frameworks/spring/bootstrapping-a-spring-web-mvc-application-programmatically
 */
public class DocuSafeWebApplicationInitializer implements WebApplicationInitializer {
    private final static Logger LOG = LoggerFactory.getLogger(DocuSafeWebApplicationInitializer.class);

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        try { //fragment kodu zak�ada, �e istnieje ju� po��czenie z baz� danych
            System.out.println("inicjalizacja DocuSafe poprzez DocuSafeWebApplicationInitializer!");
            Docusafe.start(servletContext); //metoda wychodzi w momencie gdy DataSource jest ju� gotowy
            ApplicationContext applicationContext = new ClassPathXmlApplicationContext("docusafeApplicationContext.xml");
            File customApplicationContext = new File(Docusafe.getHome(), "customApplicationContext.xml");
            //za�aduj dodatkowy plik z HOME'a je�li istnieje
            applicationContext = customApplicationContext.isFile()
                    ? getCustomApplicationContext(applicationContext, customApplicationContext)
                    : applicationContext;
            Docusafe.applicationContext = applicationContext;
            LOG.info("Spring application context initialized");
            AnnotationConfigWebApplicationContext wac = new AnnotationConfigWebApplicationContext();
            wac.register(RestConfiguration.class);
            wac.setParent(Docusafe.applicationContext);
            ServletRegistration.Dynamic dispatcher = servletContext.addServlet("RestDispatcherServlet",
                    new DispatcherServlet(wac));
            dispatcher.setLoadOnStartup(1);
            dispatcher.addMapping("/api/*");

            JspFactory.getDefaultFactory().getJspApplicationContext(servletContext)
                    .addELResolver(new DynaBeanELResolver());
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            throw new BootException("B��d podczas inicjalizacji:" + ex.getMessage(), ex);
        }
    }

    private FileSystemXmlApplicationContext getCustomApplicationContext(ApplicationContext applicationContext, File customApplicationContext) {
        if(AvailabilityManager.isAvailable("docusafe.customApplicationContext.resolver"))
            return new DSFileSystemXmlApplicationContext(new String[]{customApplicationContext.getAbsolutePath()}, true, applicationContext);
        else
            return new FileSystemXmlApplicationContext(new String[]{customApplicationContext.getAbsolutePath()}, true, applicationContext);
    }
}
