package pl.compan.docusafe.boot;

import org.apache.commons.logging.Log;
/* User: Administrator, Date: 2006-01-31 14:33:44 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: DebugTimer.java,v 1.2 2006/02/06 16:05:47 lk Exp $
 */
public class DebugTimer
{
    private Log log;
    private long time = -1;
    private String message;
    private String prefix;

    DebugTimer(Log log, String prefix)
    {
        this.log = log;
        this.prefix = prefix;
    }

    public void restart(String message)
    {
        stop();
        start(message);
    }

    public void start(String message)
    {
        this.message = message;
        time = System.currentTimeMillis();
    }

    public void stop()
    {
        if (time != -1)
        {
            log.info(prefix + ": " + message + ": " + (System.currentTimeMillis()-time)+"ms");
            message = null;
            time = -1;
        }
    }
}
