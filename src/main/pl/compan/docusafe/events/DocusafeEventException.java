package pl.compan.docusafe.events;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.EdmException;

/**
 * Wyjatek rzucany podczas obslugi operacji
 * @author wkutyla
 *
 */
public class DocusafeEventException extends EdmException 
{
	static final long serialVersionUID = 3323;
	private static final Log log = LogFactory.getLog(DocusafeEventException.class);
	
	public DocusafeEventException(Throwable cause)
    {
        super(cause);
        log.debug("", this);
    }
	
	public DocusafeEventException(String message)
    {
        super(message);
        log.debug(message, this);
    }
	
}
