package pl.compan.docusafe.events;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Mechanizm obslugi zdarzen sluzy do uporzadkowania kwestii zwiazanych z
 * wykonywaniem czynnosci: - nie zwiazanych z bezposrednia funkcjonalnoscia
 * obiegu - wyzwalanych wedlug zaawanswoanych regul
 * 
 * @author wkutyla
 * 
 */
public class EventFactory {
	private static final Log log = LogFactory.getLog(EventFactory.class);
	public final static int ACTIVE_EVENT_STATUS = 0;
	public final static int DONE_EVENT_STATUS = 1;
	public final static int IN_PROGRESS_EVENT_STATUS = 2;
	public final static int ERROR_EVENT_STATUS = -1;
	public final static int CANCELLED_EVENT_STATUS = -10;
	public final static int OVERDUE_EVENT_STATUS = 99;

	/**
	 * Metoda rejestruje zdarzenie w systemie, zwraca id eventu
	 * 
	 * @param triggerCode
	 * @param handlerCode
	 * @param params
	 * @param dueDate
	 * @param nextCheckDate
	 * @throws DocusafeEventException
	 */
	public static Long registerEvent(String triggerCode, String handlerCode,
			String params, Date dueDate, Date nextCheckDate)
			throws DocusafeEventException {
		try {
			DocusafeEvent event = new DocusafeEvent(
					EventFactory.ACTIVE_EVENT_STATUS, nextCheckDate, params);
			event.setTrigger(triggerCode);
			event.setHandler(handlerCode);
			DSApi.context().session().save(event);
			DSApi.context().session().flush();
			return event.getEventId();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return null;
	}

	public static Long registerEvent(String triggerCode, String handlerCode,
			String params, Date dueDate, Date nextCheckDate, Long documentId)
			throws DocusafeEventException {
		try {
			DocusafeEvent event = new DocusafeEvent(
					EventFactory.ACTIVE_EVENT_STATUS, nextCheckDate, params,
					documentId);
			event.setTrigger(triggerCode);
			event.setHandler(handlerCode);
			DSApi.context().session().save(event);
			DSApi.context().session().flush();
			return event.getEventId();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return null;
	}

	/**
	 * Metoda ustawia status dla wszystkich zdarzen powiazanych z dokumentem
	 * 
	 * @param eventStatus
	 *            - status zdarzenia do ustawienia
	 * @param documentId
	 *            - identyfikator dokumentu
	 */
	public static void setEventStatus(int eventStatus, Long documentId)
			throws DocusafeEventException {
		PreparedStatement ps = null;
		try {
			ps = DSApi
					.context()
					.prepareStatement(
							"update dse_event set event_status_id = ? where document_id = ?");
			ps.setInt(1, eventStatus);
			ps.setLong(2, documentId);
			ps.execute();
			DSApi.context().closeStatement(ps);
			ps = null;
		} catch (Exception e) {
			log.error("", e);
			throw new DocusafeEventException(e);
		} finally {
			DSApi.context().closeStatement(ps);
		}
	}

	public static void setEventStatus(Long eventId, int eventStatus)
			throws DocusafeEventException {
		PreparedStatement ps = null;
		try {
			ps = DSApi
					.context()
					.prepareStatement(
							"update dse_event set event_status_id = ? where event_id = ?");
			ps.setInt(1, eventStatus);
			ps.setLong(2, eventId);
			ps.execute();
			DSApi.context().closeStatement(ps);
			ps = null;
		} catch (Exception e) {
			log.error("", e);
			throw new DocusafeEventException(e);
		} finally {
			DSApi.context().closeStatement(ps);
		}
	}

	public static void setNextCheckDate(Long eventId, Date nextCheckDate)
			throws DocusafeEventException {
		PreparedStatement ps = null;
		try {
			ps = DSApi
					.context()
					.prepareStatement(
							"update dse_event set next_check_date = ? where event_id = ?");
			ps.setTimestamp(1, new java.sql.Timestamp(nextCheckDate.getTime()));
			ps.setLong(2, eventId);
			ps.execute();
			DSApi.context().closeStatement(ps);
			ps = null;
		} catch (Exception e) {
			log.error("", e);
			throw new DocusafeEventException(e);
		} finally {
			DSApi.context().closeStatement(ps);
		}
	}

	/**
	 * Lista identyfikatorow do przetworzenia
	 * 
	 * @return
	 * @throws Exception
	 */

	public static synchronized Collection<DocusafeEvent> getToCheckList()
			throws DocusafeEventException {
		return getToCheckList(null);
	}

	private static synchronized Collection<DocusafeEvent> getToCheckList(
			String handlerCode) throws DocusafeEventException {
		PreparedStatement ps = null;
		Collection<DocusafeEvent> resp = new Vector<DocusafeEvent>();
		try {
            StringBuilder sql = new StringBuilder("select * from dse_event where next_check_date < ? and event_status_id = 0 ");
			if (handlerCode != null) {
                sql.append(" and HANDLER_CODE = ? ");
                if(DSApi.isOracleServer()) {
                    sql.append(" for update of event_status_id");
                }
				ps = DSApi.context().prepareStatement(sql);
				ps.setString(2, handlerCode);
			} else {
                if(DSApi.isOracleServer()) {
                    sql.append(" for update of event_status_id");
                }
				ps = DSApi.context().prepareStatement(sql);
			}
			ps.setTimestamp(1, new java.sql.Timestamp(new java.util.Date()
					.getTime()));
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				resp.add(new DocusafeEvent(rs.getLong("event_id"), rs
						.getInt("event_status_id"), rs
						.getString("trigger_code"), rs
						.getString("handler_code"), rs
						.getTimestamp("next_check_date"), rs
						.getString("params")));
				EventFactory.setEventStatus(rs.getLong("event_id"),
						EventFactory.IN_PROGRESS_EVENT_STATUS);
			}
			rs.close();
			DSApi.context().closeStatement(ps);
			ps = null;
		} catch (Exception e) {
			log.error("", e);
			throw new DocusafeEventException(e);
		} finally {
			DSApi.context().closeStatement(ps);
		}
		return resp;
	}

	/**
	 * Lista identyfikatorow eventow danego typu do przetworzenia
	 * 
	 * @return
	 * @throws Exception
	 */

	public static Collection<DocusafeEvent> getToCheckListByHandler(
			String handlerCode) throws DocusafeEventException {
		return getToCheckList(handlerCode);
	}

	public static void startEventByHandler(String handlerCode) {
		ExecutorService exec = null;
		try {
			exec = Executors.newFixedThreadPool(5); // .newSingleThreadExecutor();
													// //
													// Executors.newCachedThreadPool();
			for (DocusafeEvent ev : EventFactory
					.getToCheckListByHandler(handlerCode)) {
				;
				if (ev.getTriggerObj().trig()) {
                    if(log.isDebugEnabled()) {
                        log.debug("Event handler code " + ev.getHandler());
                        log.debug(ExceptionUtils.getFullStackTrace(new Throwable()));
                    }
					exec.execute(ev.getHandlerObj());
				}
				ev.setNextCheckDate();
			}
		} catch (Exception e) {
			log.error("startEventByHandler : " + e.getMessage(), e);
		} finally {
			exec.shutdown();
		}
	}

	/**
	 * Metoda zamyka wszystkie zdarzenia zwiazane z danym dokumentem
	 * 
	 * @param document_id
	 */
	public static int closeEventsByDocumentId(Long document_id) {
		PreparedStatement ps = null;
		try {
			ps = DSApi
					.context()
					.prepareStatement(
							"delete from DSE_EVENT where document_id = ?");
			ps.setLong(1, document_id);
			
			return ps.executeUpdate();

		} catch (Exception exc) {
			log.debug(exc.getMessage());
			return 0;
		} finally {
			DSApi.context().closeStatement(ps);			
		}
	}
	
	/**
	 * Metoda zwraca liczbe zdarzen powiazanych z dokumentem
	 * @param document_id
	 * @return
	 */
	public static int getDocumentEventsCount(Long document_id)
	{
		PreparedStatement ps = null;
		try {
			ps = DSApi
					.context()
					.prepareStatement(
							"select count(*) from  DSE_EVENT where document_id = ?");
			ps.setLong(1, document_id);
			ResultSet rs = ps.executeQuery();
			if (rs.next())
			{
				return rs.getInt(1);
			}
			
			return 0;

		} catch (Exception exc) {
			log.debug(exc.getMessage());
			return 0;
		} finally {
			DSApi.context().closeStatement(ps);			
		}
		
	}
}
