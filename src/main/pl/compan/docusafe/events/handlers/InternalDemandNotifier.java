package pl.compan.docusafe.events.handlers;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventHandler;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.DateUtils;

public class InternalDemandNotifier extends EventHandler
{
    static final Logger log = LoggerFactory.getLogger(InternalDemandNotifier.class);
    Integer st = 0;
    String username;
    String nrErp;
    String komorka;
    Date dataOczekiwana;

    @Override
    public void doHandle()
    {
        try
        {
            log.info("InternalDemandNotifier:START");
            DSUser user;
            try {
                log.debug("SZUKA UZYTKOWNIKA "+ username);
                user = DSUser.findByUsername(username);
            } catch (UserNotFoundException e) {
                log.debug("InternalDemandNotifier: brak uzytkownika: "+username);
                log.error(e.getMessage(),e);
                st=1;
                return;
            }

            Map<String, Object> params = prepareParams(user);

            if(StringUtils.isBlank(user.getEmail())){
                log.warn("InternalDemandNotifier: uzytkownik '{}' nie posiada adresu e-mial",  user.getName());
                st = 1;
            } else {
                log.debug("InternalDemandNotifier: Znalazl wysyla maila");
                Mailer mailer =  ((Mailer) ServiceManager.getService(Mailer.NAME));

                Mail template = Mail.INTERNAL_DEMAND_NOTIFIER;

                mailer.send(user.getEmail(), user.getEmail(),
                        null,
                        Configuration.getMail(template), params, true, null);
                log.debug("InternalDemandNotifier: WYSLAL DO "+username);
                st=1;
            }
        } catch (Exception e) {
            st=0;
            log.error("InternalDemandNotifier: Blad wysylania",e);
        }
    }



    private Map<String, Object> prepareParams(DSUser user) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("komorka", komorka);
        params.put("dataOczekiwana", DateUtils.formatJsDate(dataOczekiwana));
        params.put("nrERP", nrErp);
        return params;
    }

    @Override
    public int getEndStatus() {
        return st;
    }

    @Override
    public String getHandlerCode() {
        return "InternalDemandNotifier";
    }

    @Override
    public void parseStringParameters(String params) throws DocusafeEventException
    {
        String[] paramsTab = params.split("\\|");
        if(paramsTab.length < 4)
            throw new DocusafeEventException("InternalDemandNotifier: Brak parametr�w "+params);

        try
        {
            log.info("InternalDemandNotifier: params : {}", params);
            username = paramsTab[0];
            nrErp = paramsTab[1];
            komorka = paramsTab[2];
            dataOczekiwana = DateUtils.parseJsDate(paramsTab[3]);
        }
        catch (Exception e)
        {
            log.debug("B��d parsowania parametr�w",e);
            throw new DocusafeEventException(e);
        }
    }


}