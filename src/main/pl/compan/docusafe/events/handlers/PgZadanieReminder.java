package pl.compan.docusafe.events.handlers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.events.EventHandler;
import pl.compan.docusafe.parametrization.opzz.OpzzFactory.MAIL_TYPE;
import pl.compan.docusafe.parametrization.pg.ZadanieOdbiorca;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.service.mail.Mailer.Attachment;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


public class PgZadanieReminder  extends EventHandler
{
	private final static Logger log = LoggerFactory.getLogger(OpzzMailHandler.class);
	Integer st = 0;
	private Long documentId;
	private MAIL_TYPE mailType;
	private Date remindTime ;
	private  String[] recipientEmails;
	private String emailTitle;
	private String emailContent;
	private Boolean isSent;
	List<String> userEmails;
	final static String  regexp = ",";
	public PgZadanieReminder()
	{
		
	}
	@Override
	public String getHandlerCode()
	{
		return "PgZadanieReminder";
	}
	/**
	 * Parametry
	 * documentId|username|enum
	 */
	@Override
	public void parseStringParameters(String params) throws DocusafeEventException
	{
		String[] parameters = params.split(PARAM_SIGN_SPLIT);
		
		if(parameters.length < 1)
			throw new DocusafeEventException("PgZadanieReminder: Brak parametrów "+parameters);
		
		try
		{
			documentId = Long.parseLong(parameters[0]);
			//mailType = MAIL_TYPE.valueOf(parameters[2]);
			
			OfficeDocument doc = 	OfficeDocument.findOfficeDocument(documentId, false);
			
			FieldsManager fm = doc.getFieldsManager();

			String ids = fm.getStringKey("RECIPIENT");
			List<ZadanieOdbiorca> odbiorcy = ZadanieOdbiorca.getOdbiorcyFromIds(ids);
			userEmails = new ArrayList<String>();
			
			for(ZadanieOdbiorca odb : odbiorcy){
				
				DSUser odbiorca =  DSUser.findById(odb.getUserId());
				if(odbiorca.getEmail()!=null)
				userEmails.add(odbiorca.getEmail());
			}
			recipientEmails = userEmails.toArray(new String[userEmails.size()]);
			
			
		}catch (Exception e) {
			log.error("OpzzMailerHandler: ", e);
		}
		
	}
	@Override
	public void doHandle()
	{
		try 
		{
			log.info("PgZadanieReminder:START");
			DSUser user;
            
            Map<String, Object> params = prepareParams();
           
           
                log.info("PgZadanieReminder:  wysyla maila");
                Mailer mailer =  ((Mailer) ServiceManager.getService(Mailer.NAME));
                String[] emailCC = new String[1];
                String[] emailBCC = null; 
                String autor =    OfficeDocument.findOfficeDocument(documentId, false).getAuthor();
             	emailCC[0] = DSUser.findByUsername(autor).getEmail();
                mailer.send(recipientEmails,null, emailCC, emailBCC, Configuration.getMail(getTemplate()), params);
                      
                log.info("PgZadanieReminder: WYSLAL DO "+recipientEmails.toString());
                st=EventFactory.DONE_EVENT_STATUS;
            
        } catch (Exception e) {
			st=EventFactory.ERROR_EVENT_STATUS;
			log.error("PgZadanieReminder: Blad wysylania",e);
		}

	}

	private Map<String, Object> prepareParams() throws DocumentNotFoundException, EdmException
	{
		OfficeDocument doc = OfficeDocument.findOfficeDocument(documentId, false);
		Date date = (Date) doc.getFieldsManager().getValue("DOC_DATE_REALIZATION");
		DSUser.findByUsername(doc.getAuthor());
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("documentId", documentId);
			params.put("title", doc.getDescription());
			params.put("description", (doc.getAbstractDescription()!=null ? doc.getAbstractDescription() : ""));
			params.put("autor", DSUser.findByUsername(doc.getAuthor()).asLastnameFirstname());
			params.put("realizationDate",DateUtils.formatJsDate(date));
	        params.put("link", Docusafe.getBaseUrl()+"/office/internal/summary.action?documentId="+documentId);
	        return params;
		
	}
	/**
	 * Zwraca template Maila
	 * @return
	 */
	private Mail getTemplate()
	{
		return Mail.getInstance("REMIND_ZADANIE" + ".txt");
	}
	/**
	 * Kolejno: documentId, usernames,mailType
	 * @param params
	 * @throws DocusafeEventException 
	 */
	public static void createRemindEvent(Long documentId , Date  remindDate , String... params) throws DocusafeEventException
	{
		if(params.length < 2)
			throw new DocusafeEventException("PgZadanieReminder: Brak parametrów "+params);
		//recipientEmails = params[1];

		EventFactory.registerEvent("immediateTrigger", "PgZadanieReminder",params[0] + "|" + params[1], null, remindDate,documentId);
	}
	@Override
	public int getEndStatus()
	{
		return st;
	}

}
