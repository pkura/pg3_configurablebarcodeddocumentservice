package pl.compan.docusafe.events.handlers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.events.EventHandler;
import java.util.Calendar;
import java.util.Date;


public class RefreshTaskListHandler extends EventHandler
{
    private static final Log log = LogFactory.getLog(RefreshTaskListHandler.class);
	private int endStatus = 0;
	private Long documentId;
    public final static String HANDLER_CODE ="RefreshTaskListHandler";

	@Override
	public String getHandlerCode() 
	{
		return "RefreshTaskListHandler";
	}

	@Override
	public void parseStringParameters(String params) throws DocusafeEventException 
	{
		this.documentId = Long.valueOf(params);
	}

	@Override
	public void doHandle() 
	{
        boolean contextOpened = false;
        try {
            contextOpened = DSApi.openContextIfNeeded();
            DSApi.beginTransacionSafely();
            TaskSnapshot.updateByDocument(Document.find(documentId));
            DSApi.context().commit();
            DSApi.beginTransacionSafely();
            this.endStatus = EventFactory.DONE_EVENT_STATUS;
        } catch(Exception e){
            log.error("RefreshTaskListListener: B��d podczas od�wie�ania listy zada� (docId= "+documentId+" ): " + e);
            DSApi.context()._rollback();
            this.endStatus = EventFactory.ERROR_EVENT_STATUS;
        } finally {
            DSApi.closeContextIfNeeded(contextOpened);
        }
	}

	@Override
	public int getEndStatus() 
	{
		return this.endStatus;
	}

    public static Long createEvent(Long documentId, Date nextCheckDate) throws DocusafeEventException{
        return EventFactory.registerEvent("immediateTrigger", HANDLER_CODE, documentId.toString() , null, nextCheckDate, documentId);
    }

    public static Long createEvent(Long documentId) throws DocusafeEventException{
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.SECOND, 30);
        dt = c.getTime();
        return createEvent(documentId, dt);
    }
}
