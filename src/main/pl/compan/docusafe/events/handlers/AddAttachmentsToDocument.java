package pl.compan.docusafe.events.handlers;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Constants;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventHandler;
import pl.compan.docusafe.parametrization.ilpol.DlLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.tiff.ImageKit;

/** Dodaje za�acznik tworzony z danych stron */
public class AddAttachmentsToDocument extends EventHandler
{
	static final StringManager sm = StringManager.getManager(Constants.Package);
	private static final Logger log = LoggerFactory.getLogger(AddAttachmentsToDocument.class);
	private List<Long> documentId = new ArrayList<Long>();
	private Long importId;
	private boolean endStat = false;
	private Integer iterator;
	private Integer importKind;
	private String username;

    public void doHandle()
	{
		log.trace("START HANDLER " + getEventId() + " from " + Thread.currentThread().getName());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			List<String> pages = new ArrayList<String>();
			ps = DSApi.context().prepareStatement("select * from dl_import_attachment where import_id = ?");
			ps.setLong(1, importId);
			rs = ps.executeQuery();
			log.trace("Wczytal strony do zlaczenia");
			while (rs.next())
			{
				pages.add(rs.getString("attachment_name"));
				username = rs.getString("username");
				iterator = rs.getInt("iterator");
				importKind = rs.getInt("importKind");
			}
			rs.close();
			ps.close();

			if (pages.size() > 0)
			{
				endStat = addAttachments(pages, documentId);
				if (!endStat)
				{
					setIterator(ps, rs);
				}
				log.trace("Status koncowy " + endStat);
			}
			else
			{
				throw new EdmException("Brak stron dla event_id " + getEventId() + " import_id = " + importId);
			}
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		log.trace("STOP HANDLER " + getEventId());
	}

	private void setIterator(PreparedStatement ps, ResultSet rs) throws SQLException, EdmException
	{
		log.trace("Podbijam iterator" + iterator + " dla importu " + importId);
		iterator++;
		ps = DSApi.context().prepareStatement("update dl_import_attachment set iterator = ? where import_id = ?");
		ps.setInt(1, iterator);
		ps.setLong(2, importId);
		ps.executeUpdate();
	}

	private boolean addAttachments(List<String> pages, List<Long> inDoc)
	{
		try
		{
			log.trace("START addAttachments " + getEventId());

			File output = null;
			if(ImageKit.IMPORT_KIND_PDF.equals(importKind))
			{
				output = PdfUtils.getAttachmentAsFileFromPages(pages);
				
			}
			else
			{
				output = ImageKit.createTiffFromPages(pages);
			}
			
			if(output == null)
				return false;
			
			if(!DSApi.context().isTransactionOpen())
			{
				log.error("ADDA :0001 Robi begine" );
				DSApi.context().begin();
			}
			else
			{
				log.error("ADDA :0001 nie musi robic begine" );
			}
            for(Long docId : inDoc) {
                Attachment attachment = new Attachment("Za��cznik", "Z��czone pliki");
                Document doc = Document.find(docId);
                doc.createAttachment(attachment);
                log.trace("Dodaje multiTiffa do dokumentu");
                attachment.createRevision(output);

                String property = DataMartDefs.DOCUMENT_ATTACHMENT_ADD;
                // String descriptionKey =
                // DataMartManager.getAuditDescription(DataMartDefs.DOCUMENT_ATTACHMENT_ADD);
                String description = "Dodano za��cznik " + attachment.getTitle();// sm.getString(descriptionKey,attachment.getTitle());
                Audit audit = Audit.create(property, username, description);
                DataMartManager.addHistoryEntry(doc.getId(), audit);
                Map<String, Object> values = new HashMap<String, Object>();
                values.put(DlLogic.STATUS_DOKUMENTU_CN, 20);
                doc.getDocumentKind().setWithHistory(doc.getId(), values, false,Label.SYSTEM_LABEL_OWNER);
                TaskSnapshot.updateByDocument(doc);
            }
			DSApi.context().commit();
			log.trace("STOP addAttachments " + getEventId());
			// usuwanie nie powinno wplywac na wynik laczenia
			
			if(AvailabilityManager.isAvailable("delete.png.temp.file"))
			{
				log.trace("START delete.png.temp.file " + getEventId());
				try
				{
					for (String page : pages)
					{
						String repl = page.replace("\\", "/");
						if (repl != null && repl.length() > 0)
						{
							File ft = new File(repl + ".png");
							if (ft.exists())
							{
								ft.delete();
							}
						}
					}
					log.trace("STOP delete.png.temp.file " + getEventId());
				}
				catch (Exception e)
				{
					log.error("", e);
				}
			}
			return true;
		}
		catch (Exception e)
		{
			log.error("", e);
		}
		return false;
	}

	@Override
	public int getEndStatus()
	{
		if (iterator > 3 && !endStat)
			return 6;
		if (endStat)
			return 1;
		else
			return 0;
	}

	@Override
	public String getHandlerCode()
	{
		return "AddAttachmentsToDocument"; 	
	}

	@Override
	public void parseStringParameters(String params) throws DocusafeEventException
	{
        // pr�bujemy sparsowa� z JSONa
        ImageKit.HandlerParameters eventParams = null;
        if(params.startsWith("{")) {
            Gson gson = new Gson();
            try {
                eventParams = gson.fromJson(params, ImageKit.HandlerParameters.class);
                documentId.addAll(eventParams.getDocumentId());
                importId = eventParams.getImportId();
            } catch(JsonParseException e) {
                log.error("B��d parsowania parametr�w type JSON" + params);
            }
        }

        // to jednak nie by� JSON
        if(eventParams == null) {
            String[] paramsTab = params.split("\\|");
            if (paramsTab.length < 2)
                throw new DocusafeEventException("Brak parametr�w " + params);

            try
            {
                documentId.add(Long.parseLong(paramsTab[0]));
                importId = Long.parseLong(paramsTab[1]);
            }
            catch (Exception e)
            {
                log.error("B��d parsowania parametr�w", e);
                throw new DocusafeEventException(e);
            }
        }
	}

}
