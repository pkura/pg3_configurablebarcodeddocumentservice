package pl.compan.docusafe.events.handlers;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.events.EventHandler;
import pl.compan.docusafe.service.ServiceDriverNotSelectedException;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.ServiceNotFoundException;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.Reader;
import java.util.Map;

/**
 * Handler wysy�aj�cy przypomnienie o terminie sprawy
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class CaseReminder extends EventHandler {
    private final static Logger LOG = LoggerFactory.getLogger(CaseReminder.class);
    public String getHandlerCode() { return "case-reminder"; }

    long caseId;
    int endStatus;

    @Override
    public void parseStringParameters(String params) throws DocusafeEventException {
        LOG.info("parseStringParameters : '{}'", params);
        this.caseId = Long.valueOf(params);
    }

    @Override
    public void doHandle() {
        LOG.info("pr�ba wysy�ania powiadomienia dla sprawy o id = '{}'", caseId);
        try {
            doHandleExc();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            endStatus = EventFactory.ERROR_EVENT_STATUS;
        }
    }

    private void doHandleExc() throws Exception{
        OfficeCase officeCase = OfficeCase.find(caseId);
        String username = officeCase.getClerk();
        LOG.info("referent sprawy : {}", username);
        DSUser user = DSUser.findByUsername(username);
        DSUser supervisor = user.getSupervisor();
        if(supervisor == null) {
            LOG.error("no supervisor for user {}", user.getName());
            endStatus = EventFactory.ERROR_EVENT_STATUS;
        } else {
            String email = supervisor.getEmail();
            LOG.info("supervisor email : {}", email);
            if(Strings.isNullOrEmpty(email)){
                LOG.error("no email for supervisor : '{}'", supervisor.getName());
                endStatus = EventFactory.ERROR_EVENT_STATUS;
            } else {
                sendReminder(email, officeCase);
                endStatus = EventFactory.DONE_EVENT_STATUS;
            }
        }
    }

    /**
     * Wysy�a powiadomienie jak wszystko jest ju� przygotowane
     */
    private void sendReminder(String email, OfficeCase officeCase) throws Exception {
        LOG.info("przygotowywania maila dla office case : '{}'", officeCase.getOfficeId());
        Mailer mailer = ((Mailer) ServiceManager.getService(Mailer.NAME));
//        LOG.info("mailer = {}", mailer);
        Map<String, String> params = Maps.newHashMap();

        params.put("clerk", officeCase.getClerk());
        params.put("finishDate", DateUtils.formatCommonDate(officeCase.getFinishDate()));
        params.put("openDate", DateUtils.formatCommonDate(officeCase.getOpenDate()));
        params.put("officeId", officeCase.getOfficeId());
        Reader mailTemplate = Configuration.getMail("caseReminder.txt");
        mailer.send(email, email,
                "Przypomnienie o terminie sprawy : " + officeCase.getOfficeId(),
                mailTemplate, params, false);
    }

    @Override
    public int getEndStatus() {
        return endStatus;
    }
}
