package pl.compan.docusafe.events.handlers;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.events.EventHandler;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;
import java.util.Map;

/**
 * Og�lna klasa obs�ugi eventu powiadomie� mailowych.
 * Parametry s� przekazywane do logiki dokumentu i tam mozna zbudowa�
 * logik� wysy�ania powiadomie� mailowych
 *
 * @see {@link DocumentLogic.prepareMailToSend(Document document, String[] params, DocumentMailBean)}
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class DocumentMailHandler extends EventHandler {

    private final static Logger log = LoggerFactory.getLogger(DocumentMailHandler.class);
    //parametr 0 to id documentu
    private String[] eventParams;
    private Long documentId;
    private Integer st = 0;

    @Override
    public String getHandlerCode() {
       return "documentMail";
    }

    @Override
    public void parseStringParameters(String params) throws DocusafeEventException {
         eventParams = params.split("\\|");
        try
        {
            documentId = Long.parseLong(eventParams[0]);

        }catch (Exception e) {
            log.error("DocumentMailHandler: ", e);
        }
    }

    @Override
    public void doHandle()
    {
        try
        {
            DocumentMailBean  mailBean = new DocumentMailBean();

            Document document = Document.find(documentId);

            document.getDocumentKind().logic().prepareMailToSend(document, eventParams, mailBean);
            if(mailBean.getEmail() == null){
                log.info("brak zdefiniowane maila: " + documentId);
                st = EventFactory.ERROR_EVENT_STATUS;
                return;
            }
            sendEmail(mailBean);
            st = EventFactory.DONE_EVENT_STATUS;
        }catch(Exception e){
           log.error("", e);
        }
    }

    /**
     * Wysy�a maila
     * @param mailBean
     * @throws EdmException
     * @throws IOException
     */
    private void sendEmail(DocumentMailBean mailBean) throws EdmException, IOException {
        Mailer mailer =  ((Mailer) ServiceManager.getService(Mailer.NAME));
        mailer.send(mailBean.getEmail(), mailBean.getEmail(),null,
                Configuration.getMail(mailBean.getTemplate()), mailBean.getTemplateParameters(), true,
                mailBean.getAttachments());
    }

    @Override
    public int getEndStatus() {
        return st;
    }


    /**
     * Kolejno: documentId, i pozosta�e
     * @param params
     * @throws DocusafeEventException
     */
    public static void createEvent(Long documentId, String... params) throws DocusafeEventException
    {
        if(params.length < 1)
            throw new DocusafeEventException("DocumentMailHandler: Brak parametr�w "+params);

        Calendar calendar = Calendar.getInstance();

        EventFactory.registerEvent("immediateTrigger","documentMail", documentId + "|" + StringUtils.join(params, "|"), null, calendar.getTime(), documentId);

    }

    /**
     * Bean sluz�cy do definiowania parametr�w wiadomo�ci email w logice dokumentu
     */
    public class DocumentMailBean implements Serializable {
        private String email;
        private Map<String, String> templateParameters;
        private String template;
        private Collection<Mailer.Attachment> attachments;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Map<String, String> getTemplateParameters() {
            return templateParameters;
        }

        public void setTemplateParameters(Map<String, String> templateParameters) {
            this.templateParameters = templateParameters;
        }

        public String getTemplate() {
            return template;
        }

        public void setTemplate(String template) {
            this.template = template;
        }

        public Collection<Mailer.Attachment> getAttachments() {
            return attachments;
        }

        public void setAttachments(Collection<Mailer.Attachment> attachments) {
            this.attachments = attachments;
        }
    }
}
