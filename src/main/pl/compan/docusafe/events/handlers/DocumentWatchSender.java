package pl.compan.docusafe.events.handlers;

import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentWatch.Type;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.events.EventHandler;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.web.archive.repository.EditDocumentAction;
import pl.compan.docusafe.web.office.common.DocumentArchiveTabAction;
import pl.compan.docusafe.web.office.in.DocumentArchiveAction;

public class DocumentWatchSender extends EventHandler 
{
	private static final Logger log = LoggerFactory.getLogger(DocumentWatchSender.class);
	Integer st = EventFactory.IN_PROGRESS_EVENT_STATUS;
	Long documentId;
	String username;
	Integer type;

	@Override
	public void doHandle() 
	{
		try 
		{
			  if (log.isDebugEnabled())
		            log.debug("Wysyłanie powiadomienia "+toString()+" do użytkownika "+username);

		        DSUser user = null;
		        try
		        {
		            user = DSUser.findByUsername(username);
		            if (StringUtils.isEmpty(user.getEmail()))
		            {
		            	throw new EdmException("DocumentWatchSender: Użytkownik "+user.getName()+" nie ma adresu email");
		            }
		        }
		        catch (UserNotFoundException e)
		        {
		        	throw new EdmException("DocumentWatchSender: Nie znaleziono użytkownika "+username);
		        }

		        Map<String, Object> context = new HashMap<String, Object>();
		        context.put("documentUrl", Configuration.getBaseUrl()+EditDocumentAction.getLink(documentId));
		        context.put("documentId", documentId);
		        if(documentId != null )
		        {
		        	Document doc = Document.find(documentId);
		        	if(doc.getAttachments() != null && doc.getAttachments().size() > 0)
		        		context.put("attachmentTitle", doc.getAttachments().get(0).getTitle());
		        	context.put("documentTitle", doc.getTitle());
		        }
		        Reader mail = null;
		        if (type.equals(Type.DOCUMENT_MODIFIED.getValue()))
		        {
		            mail = Configuration.getMail(Mail.WATCH_DOCUMENT_MODIFIED);
		        }
		        else if (type.equals(Type.ATTACHMENT_DELETED.getValue()))
		        {
		            mail = Configuration.getMail(Mail.WATCH_ATTACHMENT_DELETED);
		        }
		        else if (type.equals(Type.ATTACHMENT_ADDED.getValue()))
		        {
		            mail = Configuration.getMail(Mail.WATCH_ATTACHMENT_ADDED);
		        }
		        else if (type.equals(Type.ATTACHMENT_REVISION_ADDED.getValue()))
		        {
		            mail = Configuration.getMail(Mail.WATCH_ATTACHMENT_REVISION_ADDED);
		        }
		        else if(type.equals(Type.INVOICE_ASSIGNED.getValue()))
		        {
		        	mail = Configuration.getMail(Mail.ACCEPTANCE_NOTIFIER);
		        	context.put("documentUrl", Configuration.getBaseUrl()+DocumentArchiveAction.getLink(documentId));
		        }
		        else
		        {
		        	throw new EdmException("DocumentWatchSender:Nieobsługiwany rodzaj obserwatora: "+type);
		        }
		        ((Mailer) ServiceManager.getService(Mailer.NAME)).send(user.getEmail(), user.asFirstnameLastname(), null, mail, context);
		        st =  EventFactory.DONE_EVENT_STATUS;
		}
		catch (Exception e) 
		{
			st = EventFactory.ERROR_EVENT_STATUS;
			log.error("DocumentWatchSender:Błąd wysyłania",e);
		}
	}

	@Override
	public int getEndStatus() {
		return this.st;
	}

	@Override 
	public String getHandlerCode() {
		return "DocumentWatchSender";
	}

	@Override
	public void parseStringParameters(String params) throws DocusafeEventException 
	{	
		try 
		{
			String[] paramsTab = params.split(";");
			if(paramsTab.length < 3)
				throw new DocusafeEventException("Brak parametrów "+params);	
			documentId = Long.parseLong(paramsTab[0]);			
			username = paramsTab[1];
			type = Integer.parseInt(paramsTab[2]);
		}
		catch (Exception e)
		{
			log.error("Błąd parsowania parametrów",e);
			st = EventFactory.ERROR_EVENT_STATUS;
			throw new DocusafeEventException(e);
		}
	}
}
