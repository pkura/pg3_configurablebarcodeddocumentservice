package pl.compan.docusafe.events.handlers;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.events.EventHandler;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.parametrization.opzz.OpzzFactory.MAIL_TYPE;

public class OpzzMailHandler extends EventHandler
{
	private final static Logger log = LoggerFactory.getLogger(OpzzMailHandler.class);
	Integer st = 0;
	private Long documentId;
	private String username;
	private MAIL_TYPE mailType;

	@Override
	public void doHandle()
	{
		try 
		{
			log.info("OpzzMailHandler:START");
			DSUser user;
			try 
			{
				log.info("SZUKA UZYTKOWNIKA "+ username);
				user = DSUser.findByUsername(username);	
            } catch (UserNotFoundException e) {
				log.debug("OpzzMailHandler: brak uzytkownika: "+username);
				log.error(e.getMessage(),e);
				st=EventFactory.ERROR_EVENT_STATUS;
                return;
			}
            
            Map<String, Object> params = prepareParams(user);
           
            if(StringUtils.isBlank(user.getEmail()))
            {
                log.warn("OpzzMailHandler: uzytkownik '{}' nie posiada adresu e-mial",  user.getName());
                st = EventFactory.ACTIVE_EVENT_STATUS;
            } else 
            {
                log.info("OpzzMailHandler: Znalazl wysyla maila");
                Mailer mailer =  ((Mailer) ServiceManager.getService(Mailer.NAME));
                
                String userMail = user.getEmail();
//                userMail = "lukasz.wozniak@docusafe.pl";
                
                mailer.send(userMail, userMail,
                        null,
                        Configuration.getMail(getTemplate()), params, true, null);
                log.info("OpzzMailHandler: WYSLAL DO "+username);
                st=EventFactory.DONE_EVENT_STATUS;
            }
        } catch (Exception e) {
			st=EventFactory.ERROR_EVENT_STATUS;
			log.error("OpzzMailHandler: Blad wysylania",e);
		}

	}

	private Map<String, Object> prepareParams(DSUser user)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("documentId", documentId);
        params.put("link", Docusafe.getBaseUrl()+"/office/incoming/summary.action?documentId="+documentId);
        return params;
	}

	@Override
	public int getEndStatus()
	{
		return st;
	}

	/**
	 * Zwraca template Maila
	 * @return
	 */
	private Mail getTemplate()
	{
		return Mail.getInstance(mailType.toString() + ".txt");
	}
	
	/**
	 * Kolejno: documentId, username,mailType
	 * @param params
	 * @throws DocusafeEventException 
	 */
	public static void createEvent(String... params) throws DocusafeEventException
	{
		if(params.length < 3)
			throw new DocusafeEventException("OpzzMailHandler: Brak parametrów "+params);
		
		Calendar calendar = Calendar.getInstance();
//		calendar.add(Calendar.MINUTE, 1);

		EventFactory.registerEvent("immediateTrigger", "OpzzMailHandler", params[0] + "|" + params[1] + "|" + params[2], null, calendar.getTime());
	}
	/**
	 * Parametry
	 * documentId|username|enum
	 */
	@Override
	public void parseStringParameters(String params) throws DocusafeEventException
	{
		String[] parameters = params.split(PARAM_SIGN_SPLIT);
		
		if(parameters.length < 3)
			throw new DocusafeEventException("OpzzMailHandler: Brak parametrów "+parameters);
		
		try
		{
			documentId = Long.parseLong(parameters[0]);
			username = parameters[1];
			mailType = MAIL_TYPE.valueOf(parameters[2]);
			
			
		}catch (Exception e) {
			log.error("OpzzMailerHandler: ", e);
		}
		
	}
	
	@Override
	public String getHandlerCode()
	{
		return "OpzzMailHandler";
	}
}
