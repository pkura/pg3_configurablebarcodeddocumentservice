package pl.compan.docusafe.events.handlers;

import java.util.HashMap;
import java.util.Map;

import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptancesDefinition.Acceptance;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.events.EventHandler;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;

public class AcceptanceReminder extends EventHandler
{

	private int endStatus = 0;
	private String username;
	private String acceptanceCn;
	private Long documentId;
	private String documentKindCn;
	
	@Override
	public String getHandlerCode() 
	{
		return "AcceptanceReminder";
	}

	@Override
	public void parseStringParameters(String params) throws DocusafeEventException 
	{
		String[] parts = params.split(";");
		this.documentId = Long.valueOf(parts[0]);
		this.username = parts[1];
		this.acceptanceCn = parts[2];
		this.documentKindCn = parts[3];
	}

	@Override
	public void doHandle() 
	{
		try
		{
			DSUser user = DSUser.findByUsername(username);
			Map<String, Object> ctext = new HashMap<String, Object>();
			ctext.put("documentId", this.documentId);
			ctext.put("imie", user.getFirstname());
			ctext.put("nazwisko", user.getLastname());
			ctext.put("acceptance", DocumentKind.findByCn(this.documentKindCn).getDockindInfo().getAcceptancesDefinition().getAcceptances().get(this.acceptanceCn).getName());
			
			((Mailer) ServiceManager.getService(Mailer.NAME)).send(user.getEmail(), user.getEmail(), null, Configuration.getMail(Mail.ACCEPTANCE_NOTIFIER), ctext);
			this.endStatus = EventFactory.DONE_EVENT_STATUS;
		}
		catch (Exception e) 
		{
			this.endStatus = EventFactory.ERROR_EVENT_STATUS;
		}
		
	}

	@Override
	public int getEndStatus() 
	{
		return this.endStatus;
	}

}
