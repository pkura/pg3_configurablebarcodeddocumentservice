package pl.compan.docusafe.events.handlers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentLockedException;
import pl.compan.docusafe.core.base.DocumentNotFoundException;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.events.EventHandler;
import pl.compan.docusafe.parametrization.opzz.OpzzFactory.MAIL_TYPE;
import pl.compan.docusafe.parametrization.pika.PikaFactory;
import pl.compan.docusafe.parametrization.pika.PikaLogic;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.AttachmentUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.web.common.HtmlUtils;

public class PikaMailHandler extends EventHandler
{
	private final static Logger log = LoggerFactory.getLogger(PikaMailHandler.class);
	private Long documentId;
	private int mailType = 1;
	private String email = null;
	private boolean withUrl = false;
	
	private boolean attachmentSizeOk = true;
	private Integer size = 0;
	public final static int skarga = 6;
	Integer st = 0;
	
	private static final int[] mailTypeToCheck = {1,2,3,5,6};
	public static final Integer DORADCA = 1;
	public static final Integer KIEROWNIK = 2;
	public static final Integer ODPOWIEDZIALNY = 3;
	public static final Integer USERZEWNETRZNY = 4;
	public static final Integer WSPARCIE = 5;
	public static final Integer ADRESAT = 6;
	
	@Override
	public String getHandlerCode()
	{
		return "pikaMailHandler";
	}

	@Override
	public void parseStringParameters(String params) throws DocusafeEventException
	{
		String[] parameters = params.split(PARAM_SIGN_SPLIT);
		
		if(parameters.length < 4)
			throw new DocusafeEventException("PikaMailHandler: Brak parametr�w "+parameters);
		
		try
		{
			documentId = Long.parseLong(parameters[0]);
			mailType = Integer.valueOf(parameters[1].toString());
			email = parameters[2].toString();
			withUrl = Boolean.valueOf(parameters[3].toString());
			
		}catch (Exception e) {
			log.error("PikaMailHandler: ", e);
		}
	}

	@Override
	public void doHandle()
	{
		//tymczasowa tablica na za�aczniki
		List<File> tempAttachments = new ArrayList<File>();
		boolean attachmentLink = false;
		try
		{
			log.info("PikaMailHandler: start");
			
			Document doc = Document.find(documentId);
			Map<String, Object> params = prepareParams(doc);
			Mail mailTemplate = Mail.getInstance("pikaDocument.txt");
			Collection<Mailer.Attachment> attachments = null;
			Collection<Mailer.Attachment> attachmentsTmp = null;
			if (mailType == 10) {
				Map<Integer,String> maileDoWyslania = new HashMap<Integer, String>();
				boolean ifMailExist = false; 
				Long odpId = (Long) params.get("ODPOWIEDZIALNY");
				if(odpId != null && odpId > 0){
					getEmailToSend(doc,3);
					//sprawdzannie maila
					if(!isKancelariaEmail() && email != null && !email.isEmpty())
					{
						maileDoWyslania.put(3, email);
						log.info("Do wyslania " + email);
						ifMailExist = true;
					}
				}
				else{
					for(int mailToCheck : mailTypeToCheck){
						getEmailToSend(doc,mailToCheck);
						//sprawdzannie maila
						if(!isKancelariaEmail() && email != null && !email.isEmpty() && checkMail(doc,email,maileDoWyslania,params,mailToCheck))
						{
							maileDoWyslania.put(mailToCheck, email);
							log.info("Do wyslania " + email);
							ifMailExist = true;
						}
					}
				}
				attachments = getMailerAttachment(doc, tempAttachments);
				if(ifMailExist){
					if(withUrl)
					{
						mailTemplate = Mail.getInstance("pikaDocumentUrl.txt");
					}
					
					if(attachments == null)
					{
						log.info("no attachments: {}, email: {} ", doc.getId(), maileDoWyslania.toString());
						return;
					} else {
						if(!attachmentSizeOk)
						{
							log.info("za�acznik za duzy id={} -> ustawiam urle", doc.getId());
							addAttachmentsUrl(doc, params);
							attachments = null;
							attachmentLink = true;
						}
						else
						{
							params.put("ZALACZNIKI_LINKI", " ");
						}
					}
				} else{
					if(attachments != null)
					{
						st=EventFactory.CANCELLED_EVENT_STATUS;
					}
					return;
				}
				Mailer mailer =  ((Mailer) ServiceManager.getService(Mailer.NAME));
				for (String emailToSend : maileDoWyslania.values()) {
					if(sendAttachmentAsLink(emailToSend) && attachmentLink == false) {
						addAttachmentsUrl(doc, params);
						attachmentsTmp = null;
					} 
					else if(attachmentLink == false){
						params.put("ZALACZNIKI_LINKI", " ");
						attachmentsTmp = attachments;
					}
					mailer.send(emailToSend, emailToSend,null, Configuration.getMail(mailTemplate), params, true, attachmentsTmp);
					addHistoryEntry(doc.getId(), emailToSend, withUrl);
					log.info("PikaMailHandler: send {},{} attchs: {} ",doc.getId(),emailToSend, attachmentsTmp != null ? attachments.size() : "null");
		        }
				
				st=EventFactory.DONE_EVENT_STATUS;
			}
			else {
				getEmailToSend(doc);
				
				if(isKancelariaEmail())
				{
					log.debug("email do kancelarii eob, anuluje");
					st = EventFactory.CANCELLED_EVENT_STATUS;
					return;
				}
				
				log.trace("Sprawdzam email dla doc {} {} {}", doc.getId(), email, mailType);
				
				if(email != null && !email.isEmpty())
				{
					if(withUrl)
					{
						mailTemplate = Mail.getInstance("pikaDocumentUrl.txt");
					}
					
					attachments = getMailerAttachment(doc, tempAttachments);
					if(attachments == null)
					{
						log.info("no attachments: {}, email: {} ", doc.getId(), email);
						return;
					}
				}
				else
				{
					log.error("brak adresu email");
					st = EventFactory.CANCELLED_EVENT_STATUS;
					return;
				}
				
				if(!attachmentSizeOk)
				{
					log.info("za�acznik za duzy id={} -> ustawiam urle", doc.getId());
					addAttachmentsUrl(doc, params);
					attachments = null;
				}
				else
				{
					params.put("ZALACZNIKI_LINKI", " ");
				}
				
				Mailer mailer =  ((Mailer) ServiceManager.getService(Mailer.NAME));
				mailer.send(email, email,null, Configuration.getMail(mailTemplate), params, true, attachments);
				
	            log.info("PikaMailHandler: send {},{} attchs: {} ",doc.getId(),email, attachments != null ? attachments.size() : "null");
	            st=EventFactory.DONE_EVENT_STATUS;
	            
	            /**@TODO zaktualizowa� parametry aby byla jednolito�� w debugowaniu*/
	            
	            addHistoryEntry(doc.getId(), email, withUrl);
	
				log.info("PikaMailHandler: stop");
			}
		}catch(Exception e)
		{
			log.error("", e);
			//nieustawiam nie udanego wys�ania maila.
		}finally{
			clearTemporaryFile(tempAttachments);
		}
	}
	
	/**
	 * Dodaje linki do za��cznik�w w przypadku gdy b�d� za du�e
	 * @param doc
	 * @param params
	 * @throws EdmException 
	 */
	private void addAttachmentsUrl(Document doc, Map<String, Object> params) throws EdmException
	{
		StringBuilder sb = new StringBuilder(" ");
		List<Attachment> docAtta = doc.getAttachments();
		
		if(docAtta != null && !docAtta.isEmpty())
		{
			for(Attachment att : docAtta)
			{
				String title = att.getTitle();
				if(att.getMostRecentRevision() !=null)
					title = att.getMostRecentRevision().getOriginalFilename();
				
				sb.append(AttachmentUtils.getAnonymousUrl(att.getId())).append("\n");
			}
		}
		
		params.put("ZALACZNIKI_LINKI", sb.toString());
	}

	/**
	 * Sprawdza czy email jest r�wny emailowi kancelarii
	 * @return
	 */
	private boolean isKancelariaEmail()
	{
		String kancelariaEmail = Docusafe.getAdditionProperty("pika.kancelariaEmail");
		return kancelariaEmail.equalsIgnoreCase(email);
	}
	
	/**
	 * Ustawia adres email do wys�ania z formatki
	 * @throws EdmException 
	 */
	private void getEmailToSend(Document doc) throws EdmException
	{
		switch(mailType)
		{
			case 2 : //kierownik 
				email = PikaFactory.getKierownikEmail(doc);
				break;
			case 3: //odpowiedzialny
				email = PikaFactory.getOdpowiedzialnyEmail(doc);
				break;
			case 4: //user zewn�trzny
				email = PikaFactory.getUserZewnEmail(doc);
				break;
			case 5: //wsparcie
				email = PikaFactory.getWsparcieEmail(doc);
				break; 
			case 6: //adresat
				email = PikaFactory.getAdresatEmail(doc);
				break; 
			case 1: //doradca
				email = PikaFactory.getDoradcaEmail(doc);
				break;
			default: 
				break;	
		}
	}
	private void getEmailToSend(Document doc,int mailTypeLocal) throws EdmException
	{
		switch(mailTypeLocal)
		{
			case 2 : //kierownik 
				email = PikaFactory.getKierownikEmail(doc);
				break;
			case 3: //odpowiedzialny
				email = PikaFactory.getOdpowiedzialnyEmail(doc);
				break;
			case 4: //user zewn�trzny
				email = PikaFactory.getUserZewnEmail(doc);
				break;
			case 5: //wsparcie
				email = PikaFactory.getWsparcieEmail(doc);
				break; 
			case 6: //adresat
				email = PikaFactory.getAdresatEmail(doc);
				break; 
			case 1: //doradca
				email = PikaFactory.getDoradcaEmail(doc);
				break;
			default: 
				break;	
		}
	}
	/**
	 * Usuwa tymczasowe pliki, kt�re by�y do wys�ania
	 * @param tempAttachments
	 */
	private void clearTemporaryFile(List<File> tempAttachments)
	{
		ListIterator<File> it = tempAttachments.listIterator();
		log.info("Usuwam tymczasowy pliki po wys�aniu.");
		while(it.hasNext())
		{
			File f = it.next();
			try
			{
				f.delete();
			}catch(Exception e){
				log.error("", e);
			}
		}
			
	}

	/**
	 * Kolejno: documentId,mailType
	 * @param params
	 * @throws DocusafeEventException 
	 */
	public static void createEvent(Long documentId, String... params) throws DocusafeEventException
	{
		if(params.length < 4)
			throw new DocusafeEventException("PikaMailHandler: Brak parametr�w "+params);
		
		Calendar calendar = Calendar.getInstance();

		EventFactory.registerEvent("immediateTrigger", "pikaMailHandler", params[0] + "|" + params[1] + "|" + params[2] + "|" + params[3], null, calendar.getTime(), documentId);
		
	}
	
	private Map<String, Object> prepareParams(Document doc) throws Exception
	{	
		Map<String, Object> params = getEmailParams(doc);
		
        return params;
	}
	
	/**
	 * Zwraca za��czniki do email
	 * @param doc
	 * @param tempAttachments lista na tymczasowo utworzone zalaczniki do usuniecia potem
	 * @param params lista parametr�w do szablonu
	 * @return
	 * @throws EdmException
	 * @throws IOException
	 */
	private Collection<Mailer.Attachment> getMailerAttachment(Document doc, List<File> tempAttachments) throws EdmException, IOException
	{
		/** Dodaje za��czniki */
		List<Attachment> docAtta = doc.getAttachments();
		Collection<Mailer.Attachment> attachments = null;
		
		if(docAtta != null && !docAtta.isEmpty())
		{
			attachments = new ArrayList<Mailer.Attachment>();
			
			for(Attachment att : docAtta)
			{
				if(att.isDeleted()) continue;
				AttachmentRevision ar = att.getMostRecentRevision();
				File tempAr = ar.saveToTempFile();
				if(ar.getSize() > 0)
				{
					checkAttachmentSize(ar);
					attachments.add(new Mailer.Attachment(ar.getOriginalFilename(), tempAr.toURI().toURL()));
				}
				tempAttachments.add(tempAr);
			}
		}
		
		return attachments;
	}
	
	/**
	 * Sprawdza wielko�� za��cznika je�li jest za du�a ustawia flag� attachmetnSizeOk = false
	 * @param att
	 */
	private void checkAttachmentSize(AttachmentRevision ar)
	{
		try
		{
			//zaokraglam wielkosc do 1000
			size += (Integer.valueOf(Docusafe.getAdditionProperty("mail.attachment.maxsize")) * 1000 * 1000);
			if(ar.getSize() > size){
				attachmentSizeOk = false;
			}
			
		}catch(Exception e){
			log.error("", e);
		}
	}
	
	private Map<String, Object> getEmailParams(Document doc) throws EdmException
	{
		FieldsManager fm = doc.getFieldsManager();
		Map<String, Object> params = new HashMap<String, Object>();
		
		DSUser autor = getAutor(doc);
//		params.put("AUTOR", autor.getLastname() + " " + autor.getFirstname() + " " + autor.getEmail() == null ? "" : autor.getEmail());
		params.put("TYP_INT", fm.getLongKey(PikaLogic._TYP));
		params.put("ODPOWIEDZIALNY", fm.getLongKey(PikaLogic._ODPOWIEDZIALNY));
		params.put("KOD_PIKA", fm.getStringValue(PikaLogic._KOD_PIKA));
		params.put("KATEGORIA", fm.getLongKey(PikaLogic._KATEGORIA));
		params.put("link", Docusafe.getBaseUrl()+"/office/incoming/document-archive.action?documentId="+doc.getId());
		params.put("DORADCA_EMAIL", PikaFactory.getDoradcaEmail(doc) == null ? "" : PikaFactory.getDoradcaEmail(doc));
		params.put("WSPARCIE_EMAIL", PikaFactory.getWsparcieEmail(doc) == null ? "" : PikaFactory.getWsparcieEmail(doc));
		params.put("KIEROWNIK_EMAIL", PikaFactory.getKierownikEmail(doc) == null ? "" : PikaFactory.getKierownikEmail(doc));
		params.put("ADRESAT_EMAIL", PikaFactory.getAdresatEmail(doc) == null ? "" : PikaFactory.getAdresatEmail(doc));
		
		Long nadawcaId = fm.getLongKey(PikaLogic._NADAWCA);
		
		if(nadawcaId != null && nadawcaId > 0)
		{
			Map<String,Object> nadawca = DwrDictionaryFacade.dictionarieObjects.get(PikaLogic._NADAWCA).getValues(nadawcaId.toString());
		
			String nazwa = (String) nadawca.get(PikaLogic._NADAWCA + "_FIRMA");
			params.put("NAZWA_KLIENTA", nazwa);
		}
		else
		{
			params.put("NAZWA_KLIENTA", "");
		}
		
		return params;
	}

	private DSUser getAutor(Document doc)
	{
		DSUser autor = null;
		try
		{
			autor = DSUser.findByUsername(doc.getAuthor());
		}catch(Exception e){
			log.error("blad pobierania autora", e);
		}
		return autor;
	}
	
	/**
	 * Dodaje wpis do Historii pisma dokumentu, �e wiadomo�� zosta�a wys�ana.
	 * @param docId
	 * @param email
	 * @param withUrl
	 */
	private void addHistoryEntry(Long docId, String email, boolean withUrl)
	{
		try
		{
			StringBuilder sb = new StringBuilder("Wiadomo�� email z linkiem zosta�a wys�ana na adres: ");
			if(!withUrl)
				sb = new StringBuilder("Wiadomo�� email wraz z za��cznikami zosta�a wys�ana na adres: ");
			
			sb.append(email);

			DataMartManager.addHistoryEntry(docId, Audit.create("::create", DSApi.context().getPrincipalName(),  sb.toString()));
		} catch (EdmException e)
		{
			log.error("", e);
		}
	}
	
	@Override
	public int getEndStatus()
	{
		return st;
	}
	
	/**
	 * Sprawdza czy uzytkownik istnieje w systemie i czy nalezy do grupy KancelariiKEOB lub KancelariaPIKA
	 * @param user
	 * @param events
	 */
	public static boolean checkMail(Document document, String email,
			Map<Integer, String> maileDoWyslania, Map<String, Object> params,
			int emailType) throws Exception {
		Long kategoriaId = (Long) params.get("KATEGORIA");
		// sprawdzenie dla kierownika
		if ((kategoriaId != null && kategoriaId.intValue() == skarga && emailType == 2)
				|| (emailType == 1 && kategoriaId.intValue() != skarga)) {

			// sprawdzenie dla doradcy
			DSUser user = PikaFactory.getByEmail(email);
			boolean inKancelariaKEOB = user == null ? false : user.inDivisionByGuid(Docusafe.getAdditionProperty("pika.kancelariaGUID"));
			boolean inKancelariaPIKA = user == null ? false : user.inDivisionByGuid(Docusafe.getAdditionProperty("pika.kancelariaPIKA"));
			if (!inKancelariaKEOB && !inKancelariaPIKA) {
				if (!maileDoWyslania.containsValue(email.trim()))
					return true;
			} else {
				return false;
			}
		}
		return true;
	}
	public static boolean sendAttachmentAsLink(String email) throws Exception
	{
		DSUser user = PikaFactory.getByEmail(email);
		if(user == null) {
			return false;
		}
		return true;
	}
}
