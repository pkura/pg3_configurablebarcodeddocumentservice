package pl.compan.docusafe.events.handlers;


import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.dbutils.DbUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.labels.LabelsManager;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.events.EventHandler;
import pl.compan.docusafe.parametrization.ilpol.CrmMarketingTaskLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class crmMarketingTaskSetLabelEventHandler extends EventHandler 
{
	private static final Logger log = LoggerFactory.getLogger(crmMarketingTaskSetLabelEventHandler.class);
	private Long labelId;
	private Long documentId;

	@Override
	public void doHandle()
	{
		try
		{
			LoggerFactory.getLogger("mariusz").debug("getEventId() "+getEventId());
			if(getEventId().equals(getLastEventId()))
			{
				LoggerFactory.getLogger("mariusz").debug("ROWNE" + getEventId());
				DSApi.context().begin();
				String[] lt = new String[1];
				lt[0] =  CrmMarketingTaskLogic.LABEL_NAME_ODLEGLE;
				LabelsManager.removeLabelsByName(documentId,lt);
				LabelsManager.addLabel(documentId, labelId, "admin");
				DSApi.context().commit();
			}
			else
			{
				LoggerFactory.getLogger("mariusz").debug("NIE ROWNE ");
			}
		}
		catch (Exception e) 
		{
			log.error("B��d zmiany etykiety",e);
		}
	}
	
	private Long getLastEventId()
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try 
		{
			ps = DSApi.context().prepareStatement("select event_id from dse_event where HANDLER_CODE = 'crmMarketingTaskSetLabelEventHandler' " +
					"and PARAMS like ? order by event_id desc");
			ps.setString(1, "%|"+documentId);
			rs= ps.executeQuery();
			if(rs.next()) 
			{
				return rs.getLong(1);
			}
		} 
		catch (Exception e) 
		{
			log.error("",e);
		}
		finally
		{
			DbUtils.closeQuietly(ps);
		}
		return null;
	}

	@Override
	public int getEndStatus() 
	{
		return EventFactory.DONE_EVENT_STATUS;
	}

	@Override
	public String getHandlerCode() 
	{
		return "crmMarketingTaskSetLabelEventHandler";
	}

	@Override
	public void parseStringParameters(String params)throws DocusafeEventException 
	{
		if(params == null)
			throw new DocusafeEventException("Brak parametr�w");
		
		String[] paramsTab = params.split("\\|");
		if(paramsTab.length < 3)
			throw new DocusafeEventException("Brak parametr�w "+params);
		
		labelId = Long.parseLong(paramsTab[1]);
		documentId = Long.parseLong(paramsTab[2]);
	}
}
