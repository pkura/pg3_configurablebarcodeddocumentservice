package pl.compan.docusafe.events.handlers;

import java.util.Calendar;


import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.events.EventHandler;
import pl.compan.docusafe.general.canonical.model.Faktura;
import pl.compan.docusafe.general.hibernate.dao.FakturaDBDAO;
import pl.compan.docusafe.general.hibernate.dao.FakturaExportStatusDBDAO;
import pl.compan.docusafe.general.hibernate.model.FakturaExportStatusDB;
import pl.compan.docusafe.general.mapers.CanonicalToWSMapper;
import pl.compan.docusafe.general.mapers.HibernateToCanonicalMapper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.ws.client.simple.general.ExportCostInvoiceFactory;
import pl.compan.docusafe.ws.simple.general.dokzak.DokzakTYPE;

public class ExportToErpHandler extends EventHandler {

	private final static Logger log = LoggerFactory.getLogger(ExportToErpHandler.class);
	private final static StringManager sm = GlobalPreferences.loadPropertiesFile(ExportToErpHandler.class.getPackage().getName(),null) ;
	
	public final static String HANDLER_CODE = "exportToErpHandler";
	private Long documentId;
	private RodzajExportu rodzaj;
	private Integer endStatus = EventFactory.ACTIVE_EVENT_STATUS;
	
	public enum RodzajExportu{
		FAKTURA;
	}
	
	@Override
	public String getHandlerCode() {
		return HANDLER_CODE;
	}

	@Override
	public void parseStringParameters(String params) throws DocusafeEventException {
		String[] parameters = params.split(PARAM_SIGN_SPLIT);
		
		if(parameters.length < 2)
			throw new DocusafeEventException("ExportToErpHandler: Brak parametrów "+parameters);
		
		rodzaj = RodzajExportu.valueOf(parameters[0]);
		documentId = Long.valueOf(parameters[1]);		
	}

	@Override
	public void doHandle() {
		switch(rodzaj){
			case FAKTURA:
				wyslijFakture();
				break;
		}
	}

	@Override
	public int getEndStatus() {
		return endStatus;
	}
	
	private void wyslijFakture(){
		Faktura faktura = HibernateToCanonicalMapper.MapFakturaDBToFaktura(FakturaDBDAO.findModelById(documentId),documentId);
		
		DokzakTYPE dokzak = CanonicalToWSMapper.MapFakturaToDokzakTYPE(faktura);
		
		String guid = null;
		
		try {
			guid = new ExportCostInvoiceFactory().sendInvoice(dokzak);
			
			if(guid == null)
				throw new Exception("Blad wyslania faktury: guid==null");
		} 
		catch (Exception e){			 
			log.error(e.getMessage(), e);
			return;
		}
		
		FakturaExportStatusDBDAO.create(faktura.getId(), guid);
		
		endStatus = EventFactory.DONE_EVENT_STATUS;
	}
	
	public static Long createEvent(RodzajExportu rodzaj, Long documentId) throws DocusafeEventException{
		return EventFactory.registerEvent("immediateTrigger", HANDLER_CODE, rodzaj.toString()+"|"+documentId, null, Calendar.getInstance().getTime(), documentId);
	}

}
