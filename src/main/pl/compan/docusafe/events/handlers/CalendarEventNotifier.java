package pl.compan.docusafe.events.handlers;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.calendar.ICalNotificationHelper;
import pl.compan.docusafe.core.calendar.sql.CalendarEvent;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.events.EventHandler;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;

public class CalendarEventNotifier extends EventHandler 
{
	static final Logger log = LoggerFactory.getLogger(CalendarEventNotifier.class);
	Integer st = 0;
	String status;
	Long id;
	String username;
    Long timeId;
    Long eventId;

    String type;

	@Override
	public void doHandle() 
	{
		try 
		{
			log.info("CalendarEventNotifier:START");
			DSUser user;
			try {
				log.debug("SZUKA UZYTKOWNIKA "+ username);
				user = DSUser.findByUsername(username);	
            } catch (UserNotFoundException e) {
				log.debug("NIe znalazl "+username);
				log.error(e.getMessage(),e);
				st=1;
                return;
			}
            File ical = null;
            if(timeId != null){
                CalendarEvent event;
                event = CalendarEvent.find(timeId, eventId);
                if(event == null)
                {
                	log.error("Nie ma ju� zadania timeId, eventId"+timeId+" "+eventId);
                	st = EventFactory.ERROR_EVENT_STATUS;
                	return;
                }
                ical = prepareICal(user, event);
            }
            Map<String, Object> params = prepareParams(user);

            if(StringUtils.isBlank(user.getEmail())){
                log.warn("No email for user : '{}'", user.getName());
                st = 1;
            } else {
                log.debug("Znalazl wysyla maila");
                Mailer mailer =  ((Mailer) ServiceManager.getService(Mailer.NAME));
                Collection<Mailer.Attachment> attachments;
                if(ical == null){
                    attachments = Collections.emptyList();
                } else {
                    attachments = Collections.singleton(new Mailer.Attachment("event.ics", ical.toURI().toURL()));
                }
                Mail template;
                if(type == null || type.equals("create")){
                    template = Mail.CALENDAR_NEW_EVENT_NOTIFIER;
                } else if(type.equals("update")) {
                    template = Mail.CALENDAR_EVENT_UPDATE_NOTIFIER;
                } else {
                    throw new IllegalStateException("unknown event type : '" + type + "'");
                }
                mailer.send(user.getEmail(), user.getEmail(),
                        null,
                        Configuration.getMail(template), params, true, attachments);
                log.debug("WYSLAL DO "+username);
                st=1;
            }
        } catch (Exception e) {
			st=0;
			log.error("B��d wysy�ania",e);
		}
	}

    private File prepareICal(DSUser user, CalendarEvent event) throws IOException, EdmException {
        File temp = File.createTempFile("event", ".ics");
        FileUtils.writeStringToFile(temp, ICalNotificationHelper.iCalStringFromEvent(event), "UTF-8");
        return temp;
    }

    private Map<String, Object> prepareParams(DSUser user) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("imie", user.getFirstname());
        params.put("nazwisko", user.getLastname());
        params.put("link", Docusafe.getBaseUrl()+"/office/tasklist/calendar-own.action?daily=true&tab=calendar");
        return params;
    }

    @Override
	public int getEndStatus() {
		return st;
	}

	@Override
	public String getHandlerCode() {
		return "CalendarEventNotifier";
	}

	@Override
	public void parseStringParameters(String params) throws DocusafeEventException 
	{
		String[] paramsTab = params.split("\\|");
		if(paramsTab.length < 1)
			throw new DocusafeEventException("Brak parametr�w "+params);
		
		try 
		{
            log.info("params : {}", params);
			username = paramsTab[0];
            if(paramsTab.length > 1){
                eventId = Long.valueOf(paramsTab[1]);
                timeId = Long.valueOf(paramsTab[2]);
                type =  paramsTab[3];
            }
		} 
		catch (Exception e)
		{
			log.debug("B��d parsowania parametr�w",e);
			throw new DocusafeEventException(e);
		}
	}


}
