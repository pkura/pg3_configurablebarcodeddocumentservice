package pl.compan.docusafe.events.handlers;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.logic.DcLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.events.EventHandler;
import pl.compan.docusafe.export.DefaultExport;
import pl.compan.docusafe.export.ExportKind;

public class ExportDocuments extends EventHandler {
	private static final pl.compan.docusafe.util.Logger log = pl.compan.docusafe.util.LoggerFactory.getLogger(ExportDocuments.class);
	
	public static final String CHARACTER_ENCODING = "Cp1250";
	public static final String SEPARATOR = "|";
	public static final String FOLDER_PREFIX = "AMS_";
	public static final String SUFFIX = ".txt";
	/**
	 * Status zako�czenia eksportu
	 */
	private int endStatus;
	/**
	 *  Dokumenty do eksportu o podanym rodzaju
	 */
	private String documentKindCn;
	
	/**
	 * CN typu (kategorii) dokumentu.
	 */
	private Integer docTypeId;
	
	/**
	 * �cie�ka do pliku z za��cznikami (podana przez u�ytkownika).
	 */
	private String path;
	/**
	 * Aktualna �cie�ka dla za��cznik�w (w zale�no�ci od kolejnego pliku AMS).
	 */
	private String actualPath;
	/**
	 *  Ilo�� rekord�w w jednym pliku CSV (ustawiana w adds.properties)
	 */
	private Integer amountOnFile;
	/**
	 * Maksymalna ilo�c zestaw�w eksportowanych plik�w.
	 */
	private Integer maxFileSet;
	/**
	 *  Oznaczenie, kt�ry z kolei plik AMS eksportujemy.
	 */
	private int amsCount;
	/**
	 * Aktualnie przetwarzany plik CSV (do kt�rego zapisywane s� kolejne rekordy).
	 */
	private BufferedWriter writer;
	/**
	 * Numer rekordu w aktualnie przetwarzanym pliku.
	 */
	private long recordNumber = 1;
	/**
	 * Numer rekordu w zrzucanych plikach w przypadku numerowania ci�g�ego.
	 * W aktualnej chwili niewykorzystywany poniewa� ka�dy plik jest numerowany osobno.
	 */
	private long filesRecordNumber = 1;
	/**
	 * Rodzaj eksportu.
	 */
	private ExportKind export;

	@Override
	public void doHandle() {
		try {
			log.debug("Exportuje dokumenty");
			
			System.out.println("DOC KIND: " + documentKindCn + ", DOC TYPE:" + docTypeId);
			
			DocumentKind dk = DocumentKind.findByCn(documentKindCn);
			amountOnFile = Integer.valueOf(Docusafe.getAdditionProperty("exportDocuments.amountOnFile"));
			maxFileSet = Integer.valueOf(Docusafe.getAdditionProperty("exportDocuments.maxFileSet"));
			log.debug("W jednym pliku mo�e znale�� si� maksymalnie {} plik�w",amountOnFile);
			
			// dla odpowiedniego exportu linii w pliku (ustalenie exportera)
			if (dk.getCn().equalsIgnoreCase("dc")) {
			} else {
				export = new DefaultExport();
			}
			// tutaj wczytanie odpowiedniego numeru pliku AMS z adds�w
			amsCount = new Integer(Docusafe.getAdditionProperty("exportDocuments.AMS_N"));
			
			
			
			// pobranie dokument�w o podanym docKindzie
			List<Long> documents = null;
			if (docTypeId != null && docTypeId > 0)
			{
				if (documentKindCn.equals(DocumentLogicLoader.DC_KIND))
					documents = DcLogic.getInstance().findIdsByCategory(docTypeId);
			}
			else
				documents = Document.findByDocumentKind(dk.getId());
			
			log.debug("DO WYEKSPORTOWANIA JEST: {} DOKUMENT�W",documents.size());
			
			// ustalenie pierwszych nazw plik�w
			setActualDumpArea();
			// zczytanie dokument�w prawid�owych
			List<Long> incorrect = dumpCorrectDocs(dk, documents);
			
			if (export.addFooter()) {
				writer.write("E"+SEPARATOR+(recordNumber-1));
				writer.newLine();
			}
			
			log.debug("ROZPOCZYNAM ZRZUT NIEPOPRAWNYCH DOKUMENT�W OD: " + amsCount);
			// zaczytanie nieprawid�owych dokument�w
			setActualDumpArea();
			dumpIncorrectDocs(dk, incorrect);
			
			if (export.addFooter()) {
				writer.write("E"+SEPARATOR+(recordNumber-1));
				writer.newLine();
			}

			this.endStatus = EventFactory.DONE_EVENT_STATUS;
		} catch (Exception e) {
			this.endStatus = EventFactory.ERROR_EVENT_STATUS;
			log.error("", e);
		} finally {
			try {
				if (writer!=null)
					writer.close();
			} catch (IOException e) {}
		}
	}
	
	private List<Long> dumpCorrectDocs(DocumentKind dk, List<Long> documents)
		throws Exception
	{
		List<Long> incorrect = new ArrayList<Long>();
		for (Long id : documents) {
			log.debug("Przetwarzam dokument {} / {}",filesRecordNumber,documents.size());
			
			if (!export.canExport(dk, id))
			{
				incorrect.add(id);
				log.debug("Pomijam nieprawid�owy dokument {} / {}",filesRecordNumber,documents.size());
				continue;
			}

			if (!dumpDoc(dk, id)) break;
		}
		
		return incorrect;
	}
	
	private void dumpIncorrectDocs(DocumentKind dk, List<Long> documents)
			throws Exception
	{
		for (Long id : documents) {
			log.debug("Przetwarzam dokument {} / {}",filesRecordNumber,documents.size());

			if (!dumpDoc(dk, id)) break;
		}
	}
	
	private boolean dumpDoc(DocumentKind dk, Long id) throws Exception
	{
		copyAttachment(id);
		// dodawanie lub nie numeru rekordu
		if (export.isNumberTheRecords())
			writer.write(recordNumber+SEPARATOR);
		// pobranie odpowiedniego rekordu do pliku
		writer.write(export.getRecord(dk,id));
		writer.newLine();
		writer.flush();
		recordNumber++;
		filesRecordNumber++;
		if (recordNumber>amountOnFile) 
		{
			if (amsCount>=maxFileSet) return false;
			if (export.addFooter()) 
			{
				writer.write("E"+SEPARATOR+(recordNumber-1));
				writer.newLine();
			}
			setActualDumpArea();
		}
		
		return true;
	}
	
	private void setActualDumpArea() throws IOException {
		amsCount++;
		recordNumber = 1;
		// ustalenie pliku csv (writera)
		String fileCsvPath = path + FOLDER_PREFIX + amsCount + SUFFIX;
		File csvFile = new File(fileCsvPath);
		if (!csvFile.exists())
			csvFile.getParentFile().mkdirs();
		if (writer!=null) writer.close();
		writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFile),CHARACTER_ENCODING));
		//ustalenie nazwy i �cie�ki aktualnego folderu
		actualPath = path + FOLDER_PREFIX + amsCount + File.separator;
		File f = new File(actualPath);
		f.mkdirs();
	}
	
	/**
	 * Metoda kopiuje za��cznik do odpowiedniego pliku.
	 * @param documentId
	 * @throws Exception
	 */
	private void copyAttachment(Long documentId) throws Exception {
		Attachment att = Attachment.findFirstAttachment(documentId);
		if (att != null) {
			AttachmentRevision ar = att.getMostRecentRevision();
			if (ar != null) {
				String extension = ar.getOriginalFilename().substring(ar.getOriginalFilename().lastIndexOf(".")); 
				File attfile = new File(actualPath + documentId + extension);
				writeToFile(attfile.getAbsolutePath(), ar.getAttachmentStream(), true);
			}
		}
	}
	
	@Override
	public int getEndStatus() {
		return this.endStatus;
	}

	@Override
	public String getHandlerCode() {
		return "ExportDocuments";
	}
	
	
	@Override
	public void parseStringParameters(String params) throws DocusafeEventException {
		String[] p = params.split(";");
		documentKindCn = p[0];
		if (!p[1].equals(""))
			docTypeId = Integer.parseInt(p[1]);
		path = p[2];
		log.debug("docelowa �cie�ka: {}",path);
		path = checkLastSlash(path);
	}
	
	/**
	 * Metoda u�atwia sprawdzenie podanych �cie�ek do plik�w
	 * @param s
	 * @return
	 */
	private String checkLastSlash(String s) {
		if (!s.endsWith("/") || !s.endsWith("\\"))
			return s + File.separator;
		return s;
	}

	/**
	 * Zapis strumienia z za��cznikiem do pliku.
	 * @param fileName - nazwa pliku
	 * @param iStream - strumie�
	 * @param createDir - czy utworzyc folder
	 * @throws IOException
	 */
	private static void writeToFile(String fileName, InputStream iStream, boolean createDir) throws IOException {
		String me = "ExportDocuments: ";
		if (fileName == null) {
			throw new IOException(me + ": filename is null");
		}
		if (iStream == null) {
			throw new IOException(me + ": InputStream is null");
		}

		File theFile = new File(fileName);

		if (theFile.exists()) {
			String msg = theFile.isDirectory() ? "directory" : (!theFile.canWrite() ? "not writable" : null);
			if (msg != null) {
				throw new IOException(me + ": file '" + fileName + "' is " + msg);
			}
		}

		if (createDir && theFile.getParentFile() != null) {
			theFile.getParentFile().mkdirs();
		}
		
		BufferedOutputStream fOut = new BufferedOutputStream(new FileOutputStream(theFile));
		try {
			IOUtils.copy(iStream, fOut);
		} catch (IOException e) {
			log.error("",e);
		} finally {
			IOUtils.closeQuietly(iStream);
			IOUtils.closeQuietly(fOut);
		}
		
	}

}
