package pl.compan.docusafe.events.handlers;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.dbutils.DbUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.events.EventHandler;
import pl.compan.docusafe.parametrization.ifpan.IfpanUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Handler wysy�aj�cy przypomnienie o terminie sprawy
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class IfpanExportRepeater extends EventHandler {
    private final static Logger LOG = LoggerFactory.getLogger(IfpanExportRepeater.class);
    public String getHandlerCode() { return "IfpanExportRepeater"; }

    long id;
    String kind;
    int endStatus;

    @Override
    public void parseStringParameters(String params) throws DocusafeEventException {
        LOG.info("parseStringParameters : '{}'", params);
        this.id = Long.valueOf(params.split(",")[0]);
        this.kind = params.split(",")[1];
    }

    @Override
    public void doHandle() {
        LOG.info("pr�ba eksportu {} id = {}", kind,id);
        try {
            doHandleExc();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            endStatus = EventFactory.ERROR_EVENT_STATUS;
        }
    }

    private void doHandleExc() throws Exception
    {
    	try
    	{
			if(kind.equals("person"))
			{
				
				//IfpanUtils.exportPerson(Person.find(id),false);
			}
			else if(kind.equals("invoice"))
			{
				//IfpanUtils.exportInvoice(Document.find(id),false);
			}
			else
			{
				 LOG.error("b�edny rodzaj eksportu {}", kind);
		         endStatus = EventFactory.ERROR_EVENT_STATUS;
			}
			endStatus = EventFactory.DONE_EVENT_STATUS;
    	}
    	catch (Exception e) {
    		LOG.error("b�edny eksportu "+e.getMessage(), e);
	        endStatus = EventFactory.ERROR_EVENT_STATUS;
		}
    }
    
//    private boolean checkEventExist()
//    {
//    	PreparedStatement ps = null;
//    	ResultSet rs = null;
//    	try
//    	{
//    		ps = DSApi.context().prepareStatement("select count(1) from dso_document_asgn_history where targetguid = ? and document_id =  ? and targetuser is not null");
//    		ps.setString(1, "DL_BINDER_"+wfp);
//    		ps.setLong(2, documentId);
//    		rs = ps.executeQuery();
//    		if(rs.next())
//    			return rs.getInt(1);
//    	}
//    	catch (Exception e)
//    	{
//			log.error("",e);
//		}
//    	finally
//    	{
//    		DbUtils.closeQuietly(ps);
//    	}
//    	return 0;
//    }

    @Override
    public int getEndStatus() {
        return endStatus;
    }
}
