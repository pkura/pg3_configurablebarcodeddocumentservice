package pl.compan.docusafe.events.handlers;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Constants;
import pl.compan.docusafe.core.datamart.DataMartDefs;
import pl.compan.docusafe.core.datamart.DataMartManager;
import pl.compan.docusafe.core.labels.Label;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventHandler;
import pl.compan.docusafe.parametrization.ilpol.DlLogic;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.PdfUtils;
import pl.compan.docusafe.util.StringManager;
import pl.compan.docusafe.util.tiff.ImageKit;
import pl.compan.docusafe.webwork.FormFile;

/** Dodaje nowa wersje zalacznika tworzony z danych stron */
public class AddRevisionToAttachment extends EventHandler
{
	private static final Logger log = LoggerFactory.getLogger(AddRevisionToAttachment.class);
	static final StringManager sm = StringManager.getManager(Constants.Package);
	private Long attachmentId;
	private Long importId;
	private boolean endStat = false;
	private boolean attachFiles;
	private String username;
	private Integer iterator;
	private Integer importKind;

	public void doHandle()
	{
		log.trace("START HANDLER " + getEventId());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			List<String> pages = new ArrayList<String>();
			if(attachFiles)
    		{
    				log.info("Do��czamy strony do ostatniego za��cznika");
    	        	Map<String,String> previousRevision = null;
    	        	Attachment atta = Attachment.find(attachmentId,false);
    	        	AttachmentRevision ar = atta.getMostRecentRevision();
    	
    	            List<FormFile> multiFiles = new ArrayList<FormFile>();
    	            multiFiles.add(new FormFile(ar.saveToTempFile(), "zalacznik", ar.getMime()));
    	            try
    	            {
    	            	previousRevision = ImageKit.splitAttachments(multiFiles, Docusafe.getPngTempFile(),true);            	
    	            }
    	            catch(Exception e)
    	            {
    	            	log.error(e.getMessage() , e);
    	            }
    	            
    	            pages.addAll(previousRevision.keySet());
    		}
			ps = DSApi.context().prepareStatement("select * from dl_import_attachment where import_id = ?");
			ps.setLong(1, importId);
			rs = ps.executeQuery();
			log.trace("Wczytal strony do zlaczenia");
			while (rs.next())
			{
				pages.add(rs.getString("attachment_name"));
				username = rs.getString("username");
				iterator = rs.getInt("iterator");
				importKind = rs.getInt("importKind");
			}
			rs.close();
			ps.close();
			
			if (pages.size() > 0)
			{
				endStat = addRevision(pages, attachmentId);
				if (!endStat)
				{
					setIterator(ps, rs);
				}
				log.trace("Status koncowy " + endStat);
			}
		}
		catch (Exception e)
		{
			log.error("", e);
			endStat = false;
		}
		finally
		{
			DSApi.context().closeStatement(ps);
		}
		log.trace("START HANDLER " + getEventId());
	}

	private void setIterator(PreparedStatement ps, ResultSet rs) throws SQLException, EdmException
	{
		log.trace("Podbijam iterator" + iterator + " dla importu " + importId);
		iterator++;
		ps = DSApi.context().prepareStatement("update dl_import_attachment set iterator = ? where import_id = ?");
		ps.setInt(1, iterator);
		ps.setLong(2, importId);
		ps.executeUpdate();
	}

	private boolean addRevision(List<String> pages, Long attachmentId)
	{
		try
		{
			log.trace("START addAttachments " + getEventId());
			
			File output = null;
			if(ImageKit.IMPORT_KIND_PDF.equals(importKind))
			{
				log.info("Generujemy PDF");
				output = PdfUtils.getAttachmentAsFileFromPages(pages);
			}
			else
			{
				log.info("Tworzymy z wybranych stron TIFF");
				output = ImageKit.createTiffFromPages(pages);

			}
			
			if(!DSApi.context().isTransactionOpen())
			{
				log.error("ADDR :0001 Robi begine" );
				DSApi.context().begin();
			}
			else
			{
				log.error("ADDR :0001 nie musi robic begine" );
			}
			Attachment atta = Attachment.find(attachmentId);
			atta.createRevision(output);

			String property = DataMartDefs.DOCUMENT_ATTACHMENT_REVISION_ADD;// DataMartManager.getAuditProperty(DataMartDefs.DOCUMENT_ATTACHMENT_REVISION_ADD);
			// String descriptionKey =
			// DataMartManager.getAuditDescription(DataMartDefs.DOCUMENT_ATTACHMENT_REVISION_ADD);
			String description = "Utworzono now� wersje za��cznika " + atta.getTitle();// sm.getString(descriptionKey,atta.getTitle());
			Audit audit = Audit.create(property, username, description);
			DataMartManager.addHistoryEntry(atta.getDocument().getId(), audit);
			Map<String, Object> values = new HashMap<String, Object>();
			values.put(DlLogic.STATUS_DOKUMENTU_CN, 20);
			atta.getDocument().getDocumentKind().setWithHistory(atta.getDocument().getId(), values, false,Label.SYSTEM_LABEL_OWNER);

			DSApi.context().commit();
			log.trace("STOP addAttachments " + getEventId());
			// usuwanie nie powinno wplywac na wynik laczenia
			if(AvailabilityManager.isAvailable("delete.png.temp.file"))
			{
				try
				{
					log.trace("START delete.png.temp.file " + getEventId());
					for (String page : pages)
					{
						String repl = page.replace("\\", "/");
						if (repl != null && repl.length() > 0)
						{
							File ft = new File(repl + ".png");
							if (ft.exists())
							{
								ft.delete();
							}
						}
					}
					log.trace("STOP delete.png.temp.file " + getEventId());
				}
				catch (Exception e)
				{
					log.error("", e);
				}
			}
			return true;
		}
		catch (Exception e)
		{
			log.error("", e);
			try
			{
				DSApi.context().rollback();
			}catch (Exception e2) {}
			return false;
		}
	}

	@Override
	public int getEndStatus()
	{
		if (iterator > 3 && !endStat)
			return 6;
		if (endStat)
			return 1;
		else
			return 0;
	}

	@Override
	public String getHandlerCode()
	{
		return "AddRevisionToAttachment";
	}

	@Override
	public void parseStringParameters(String params) throws DocusafeEventException
	{
		String[] paramsTab = params.split("\\|");
		if (paramsTab.length < 3)
			throw new DocusafeEventException("Brak parametr�w " + params);

		try
		{
			attachmentId = Long.parseLong(paramsTab[0]);
			importId = Long.parseLong(paramsTab[1]);
			attachFiles = Boolean.parseBoolean(paramsTab[2]);
		}
		catch (Exception e)
		{
			log.error("B��d parsowania parametr�w", e);
			throw new DocusafeEventException(e);
		}
	}

}
