package pl.compan.docusafe.events.handlers;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventHandler;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;

public class BarcodesNotifier extends EventHandler 
{
	static final Logger log = LoggerFactory.getLogger(BarcodesNotifier.class);
	Integer st = 0;
	String status;
	Long id;
	String username;

	@Override
	public void doHandle() 
	{
		try 
		{
			DSUser user = DSUser.findByUsername(username);
			if(user == null)
				return;
			Map<String, Object> ctext = new HashMap<String, Object>();
			ctext.put("imie", user.getFirstname());
			ctext.put("nazwisko", user.getLastname());
			ctext.put("orderId", id);
			ctext.put("status", status);
			
			((Mailer) ServiceManager.getService(Mailer.NAME)).send(user.getEmail(), user.getEmail(), null, Configuration.getMail(Mail.BARCODES_NOTIFIER), ctext);
			st=1;
		}
		catch (Exception e) 
		{
			st=0;
			log.error("B��d wysy�ania",e);
		}
	}

	@Override
	public int getEndStatus() {
		return st;
	}

	@Override
	public String getHandlerCode() {
		return "BarcodesNotifier";
	}

	@Override
	public void parseStringParameters(String params) throws DocusafeEventException 
	{
		String[] paramsTab = params.split("\\|");
		if(paramsTab.length < 3)
			throw new DocusafeEventException("Brak parametr�w "+params);
		
		try 
		{
			status = paramsTab[0];
			id = Long.parseLong(paramsTab[1]);
			username = paramsTab[2];
		} 
		catch (Exception e)
		{
			log.debug("B��d parsowania parametr�w",e);
			throw new DocusafeEventException(e);
		}
	}


}
