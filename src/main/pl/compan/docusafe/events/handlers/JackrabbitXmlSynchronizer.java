package pl.compan.docusafe.events.handlers;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.DSTransaction;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.jackrabbit.JackrabbitMetadata;
import pl.compan.docusafe.core.jackrabbit.JackrabbitPrivileges;
import pl.compan.docusafe.events.DocusafeEvent;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.events.EventHandler;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

/**
 * <p>
 *     Mechanizm propagacji plik�w xml do Jackrabbit.
 *     Plik xml zawiera metadane oraz tre�ci za��cznik�w
 *     w Base64.
 * </p>
 *
 * <p>
 *     Mechanizm si� nie uruchomi je�li ju� jest zarejestrowany
 *     event o statusie 0 dla danego dokumentu (na podstawie {@code documentId}).
 * </p>
 *
 * <p>
 *     Mechanizm si� nie uruchomi r�wnie� je�li od ostatniego zrzutu xml
 *     dane xml si� nie zmieni�y (sprawdzany jest hash SHA1 z xml z ostatniego
 *     poprawnie zako�czonego eventu wzgl�dem aktualnego xml).
 * </p>
 *
 * <p>
 *     XML jest przesy�any przez HTTP do aplikacji Jackrabbitowej.
 *     Szczeg�y w metodzie {@link pl.compan.docusafe.core.jackrabbit.JackrabbitRepository#storeAdditionalData}.
 * </p>
 *
 * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
public class JackrabbitXmlSynchronizer extends EventHandler {
    private final static Logger log = LoggerFactory.getLogger(JackrabbitXmlSynchronizer.class);

    private Long documentId;
    private Integer st = 0;

    @Override
    public String getHandlerCode() {
        return "JackrabbitXmlSynchronizer";
    }

    @Override
    public void parseStringParameters(String params) throws DocusafeEventException {
    }

    @Override
    public void doHandle() {
        try {

            DocusafeEvent event = getEvent();

            JackrabbitMetadata metadata = new JackrabbitMetadata(event.getDocument_id());
            metadata.init();
            String sha1 = metadata.getSha1();

            updateSha1(event, sha1);

            if(! isDuplicateOperation(event.getEventId(), event.getDocument_id(), sha1)) {
                log.debug("[doHandle] not duplicate operation, sending to jackrabbit, event = {}, sha1 = {}", event, sha1);

                // TODO
                // send i propagate powinny i�� w tym samym reque�cie
                // po to, �eby po stronie Jackrabbita ca�a operacja by�a
                // wykonana w jednej sesji.
                metadata.send();
            } else {
                log.debug("[doHandle] is duplicate operation, NOT SENDING to jackrabbit, event = {}, sha1 = {}", event, sha1);
            }

            Thread.sleep(10000);
            // odpalamy propagacje uprawnie� niezale�nie
            // od tego czy przesy�amy xml, bo event jest
            // jest te� rejestrowany przy dekretacji,
            // dodaniu/odebraniu obserwowanych
            // i przy modyfikacji uprawnie� na dokumencie
            // - w tych sytuacjach chcemy zaktualizowa� uprawnienia.
            JackrabbitPrivileges.instance().propagate(event.getDocument_id());

            st = EventFactory.DONE_EVENT_STATUS;
        } catch(Exception e) {
            log.error("[doHandle] error", e);
            st = EventFactory.ERROR_EVENT_STATUS;
        }
    }

    private void updateSha1(final DocusafeEvent event, final String sha1) throws EdmException {
        log.debug("[updateSha1] event.id = {}, sha1 = {}", event.getEventId(), sha1);
        new DSTransaction().execute(new DSTransaction.Function<Object>() {
            @Override
            public Object apply() throws Exception {
                event.setParams(sha1);
                save(event);
                return null;
            }
        });
    }


    @Override
    public int getEndStatus() {
        return st;
    }

    /**
     * Kolejno: documentId
     * @param documentId
     * @throws DocusafeEventException
     */
    public static Long createEvent(Long documentId) throws EdmException {

        log.debug("[createEvent] documentId = {}", documentId);

        if(! AvailabilityManager.isAvailable("jackrabbit.metadata")) {
            log.error("[send] `jackrabbit.metadata` is not available, documentId = {}", documentId);
            return 0L;
        }

        if(documentId == null)
            throw new DocusafeEventException("JackrabbitXmlSynchronizer: Brak identyfikatora dla dokumentu");

        Calendar calendar = Calendar.getInstance();

        if(checkEventExist(documentId)) {
            return null;
        }
        else {
            return EventFactory.registerEvent("immediateTrigger","JackrabbitXmlSynchronizer", null, null, calendar.getTime(), documentId);
        }
    }

    /**
     * <p>
     *     Metoda sprawdza czy poprzedni udany event
     *     dla dokumentu o id {@code documentId} mia�
     *     wygenerowany ten sam SHA1. W ten spos�b zapobiegamy
     *     duplikowaniu si� wersji XML w jackrabbit.
     * </p>
     * @param documentId
     * @param sha1
     * @return
     * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
     */
    private boolean isDuplicateOperation(Long eventId, Long documentId, String sha1) throws EdmException {

        Criteria criteria = DSApi.context().session().createCriteria(DocusafeEvent.class);
        criteria.add(Restrictions.eq("document_id", documentId));
        criteria.add(Restrictions.eq("eventStatus", EventFactory.DONE_EVENT_STATUS));
        criteria.add(Restrictions.not(Restrictions.eq("eventId", eventId)));
        criteria.addOrder(Order.desc("eventId"));
        criteria.setMaxResults(1);

        List<DocusafeEvent> list = criteria.list();

        if(list.size() > 0) {

            DocusafeEvent ev = list.get(0);

            if(sha1.equals(ev.getParams())) {
                log.debug("[isDuplicateOperation] true, first event has same hash, ev.id = {}, eventId = {}", ev.getEventId(), eventId);
                return true;
            } else {
                log.debug("[isDuplicateOperation] false, first event different hash, ev.id = {}, eventId = {}", ev.getEventId(), eventId);
                return false;
            }

        } else {
            return false;
        }
    }

    private static boolean checkEventExist(Long documentId) throws EdmException {
        try {
            PreparedStatement ps = DSApi.context().prepareStatement("select event_id from dse_event "+
                    " where event_status_id = 0 and handler_code = 'JackrabbitXmlSynchronizer' "+
                    " and document_id = ? ");

        ps.setLong(1,documentId);

        ResultSet rs =  ps.executeQuery();
        while (rs.next()){
            return true;
        }
        }catch (SQLException e){
            log.error("SQLException: ",e);
        }
        return false;

    }
}
