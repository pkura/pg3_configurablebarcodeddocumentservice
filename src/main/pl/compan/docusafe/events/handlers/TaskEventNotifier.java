package pl.compan.docusafe.events.handlers;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.cfg.Mail;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventHandler;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.util.DateUtils;

/**
 * Klasa rejestruje zdarzenie przypominajace o uplywajacym terminie wykonania zadania.
 * Parametr params przyjmuje wartosci:
 * username
 * username - nazwa uzytkownika, do ktorego zostanie wyslany e-mail
 * @author lenovo
 *
 */
public class TaskEventNotifier extends EventHandler 
{
	static final Logger log = LoggerFactory.getLogger(TaskEventNotifier.class);
	Integer st = 0;
	String username;
	Date deadline;	
    String type;
    Long document_id;

	@Override
	public void doHandle() 
	{
		try 
		{
			log.info("TaskEventNotifier:START");
			DSUser user;
			try {
				log.debug("SZUKA UZYTKOWNIKA "+ username);
				user = DSUser.findByUsername(username);	
            } catch (UserNotFoundException e) {
				log.debug("TaskEventNotifier: brak uzytkownika: "+username);
				log.error(e.getMessage(),e);
				st=1;
                return;
			}
            
            Map<String, Object> params = prepareParams(user);
           
            if(StringUtils.isBlank(user.getEmail())){
                log.warn("TaskEventNotifier: uzytkownik '{}' nie posiada adresu e-mial",  user.getName());
                st = 1;
            } else {
                log.debug("TaskEventNotifier: Znalazl wysyla maila");
                Mailer mailer =  ((Mailer) ServiceManager.getService(Mailer.NAME));
                
                Mail template = Mail.TASK_REMINDER;
                
                mailer.send(user.getEmail(), user.getEmail(),
                        null,
                        Configuration.getMail(template), params, true, null);
                log.debug("TaskEventNotifier: WYSLAL DO "+username);
                st=1;
            }
        } catch (Exception e) {
			st=0;
			log.error("TaskEventNotifier: Blad wysylania",e);
		}
	}



    private Map<String, Object> prepareParams(DSUser user) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("imie", user.getFirstname());
        params.put("nazwisko", user.getLastname());
        params.put("deadline", deadline);
        params.put("link", Docusafe.getBaseUrl()+"/office/incoming/summary.action?documentId="+document_id);
        return params;
    }

    @Override
	public int getEndStatus() {
		return st;
	}

	@Override
	public String getHandlerCode() {
		return "TaskEventNotifier";
	}

	@Override
	public void parseStringParameters(String params) throws DocusafeEventException 
	{
		String[] paramsTab = params.split("\\|");
		if(paramsTab.length < 3)
			throw new DocusafeEventException("TaskEventNotifier: Brak parametr�w "+params);
		
		try 
		{
            log.info("TaskEventNotifier: params : {}", params);
			username = paramsTab[0];
			deadline = DateUtils.parseJsDate(paramsTab[1]);
			document_id = Long.parseLong(paramsTab[2]);
            
		} 
		catch (Exception e)
		{
			log.debug("B��d parsowania parametr�w",e);
			throw new DocusafeEventException(e);
		}
	}


}
