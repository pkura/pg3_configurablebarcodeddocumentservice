package pl.compan.docusafe.events.handlers;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.fop.Fop;
import pl.compan.docusafe.core.reports.Report;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.events.EventHandler;
import pl.compan.docusafe.service.ServiceManager;
import pl.compan.docusafe.service.mail.Mailer;
import pl.compan.docusafe.service.reports.ReportGenerator;
import pl.compan.docusafe.web.reports.ViewReportPdfAction;
import pl.compan.docusafe.web.reports.office.TemplatesReportAction;

public class SendReportViaEmail extends EventHandler {

	private int endStatus;
	private String username;
	private String templatename;
	
	public void doHandle() 
	{
		if(!(getEventId().intValue() == DSApi.context().userPreferences(username).node("autoreport").node(templatename).getInt("eventId", 0)))
		{
			this.endStatus = EventFactory.DONE_EVENT_STATUS;
			return;
		}
		
		try
		{
			DSUser user = DSUser.findByUsername(username);
			TemplatesReportAction tmp = new TemplatesReportAction();
    		tmp.setTemplateName(templatename);
    		tmp.readValues();
    		tmp.generate(null);
    		File xml = Report.find(tmp.getId()).getXmlAsFile();
    		InputStream xmlIs = new BufferedInputStream(new FileInputStream(xml));
    		InputStream sheet = ViewReportPdfAction.class.getClassLoader().getResourceAsStream(ReportGenerator.getReportProperty(Report.find(tmp.getId()).getReportId(), "foXslResource")+"_"+Docusafe.getCurrentLanguage()+".xsl");
    		File pdf = File.createTempFile("docusafe_report_", ".pdf");
    		OutputStream os = new FileOutputStream(pdf);
    		Fop.renderPdf(xmlIs, sheet, os);
    		
    		((Mailer) ServiceManager.getService(Mailer.NAME)).send(user.getEmail(), ((Mailer) ServiceManager.getService(Mailer.NAME)).getProperty("fromEmail").getValue().toString(), "docusafe", "raport", "raport", pdf.getAbsolutePath());
    		
    		pdf.delete();
		}
		catch (Exception e)
		{
			LoggerFactory.getLogger("tomekl").debug(e.getMessage(),e);
		}
		
		this.endStatus = EventFactory.ACTIVE_EVENT_STATUS;
		
	}

	public int getEndStatus() 
	{
		return this.endStatus;
	}

	public String getHandlerCode() 
	{
		return "SendReportViaEmail";
	}

	public void parseStringParameters(String params) throws DocusafeEventException 
	{
		String paramsDirty = params.split("\\|")[1].trim();
		templatename = paramsDirty.split(";")[0].trim();
		username = paramsDirty.split(";")[1].trim();
	}

}
