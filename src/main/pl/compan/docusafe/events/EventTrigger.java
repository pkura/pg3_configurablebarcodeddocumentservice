package pl.compan.docusafe.events;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.FileUtils;

/**
 * Klasa reprezentujaca trigger - czyli mechanizm 
 * uruchomienia zdarzenia
 * @author wkutyla
 *
 */
public abstract class EventTrigger 
{
	private static final Logger log = LoggerFactory.getLogger(EventTrigger.class);
	public final static String TRIGGERS_PACKAGE_NAME = EventTrigger.class.getPackage().getName()+".triggers";
	
	private static Map<String, Class<EventTrigger>> triggerCodeToClassMap;
	
	/**
	 * Metoda wykonywana przy starcie systemu, nie ma potrzeby ponownego wykonywania
	 * @throws Exception
	 */
	private static void initialize() throws DocusafeEventException
	{
		try
		{
			triggerCodeToClassMap = new HashMap<String, Class<EventTrigger>>();
			for(Class<EventTrigger> clazz : FileUtils.getClassesForPackage(TRIGGERS_PACKAGE_NAME))
			{
				EventTrigger tmpTrigger = clazz.newInstance();
				triggerCodeToClassMap.put(tmpTrigger.getTriggerCode(), clazz);
			}
		}
		catch (Exception e)
		{
			throw new DocusafeEventException(e);
		}
	}
	
	public static EventTrigger getInstance(Long eventId, String triggerCode, String triggerParams) throws DocusafeEventException
	{
		if(triggerCodeToClassMap == null){
			initialize();
		}

        if(!triggerCodeToClassMap.containsKey(triggerCode)){
            throw new IllegalArgumentException("Unknown trigger code : " + triggerCode);
        }
		
		EventTrigger resp = null;
		try {
			resp = triggerCodeToClassMap.get(triggerCode).newInstance();
			resp.parseStringParameters(triggerParams);
			resp.setEventId(eventId);
		} catch (Exception e) {
			log.error("triggerCode = "+triggerCode+" "+e.getMessage(),e);
			throw new DocusafeEventException(e);
		}
		return resp;
	}
	
	private Long eventId;
	private void setEventId(Long eventId)
	{
		this.eventId = eventId;
	}
	
	/**
	 * Kod triggera
	 * @return
	 */
	public abstract String getTriggerCode();
	
	/**
	 * Metoda sprawdza czy nalezy wywolac zdarzenie
	 * @return
	 * @throws DocusafeEventException
	 */
	public abstract boolean trig() throws DocusafeEventException;
	
	/**
	 * Metoda ustawia kontekst triggera wedlug okreslonego tekstu
	 * @param params
	 * @throws DocusafeEventException
	 */
	public abstract void parseStringParameters(String params) throws DocusafeEventException;
		
	/**
	 * Metoda zwraca date nastepnego sprawdzenia eventu
	 * @param checkDate
	 * @return
	 */
	public abstract Date getNextCheckDateFromOldCheckDate(Date checkDate);
}
