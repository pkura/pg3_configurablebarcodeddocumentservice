package pl.compan.docusafe.events;

import com.google.common.base.Optional;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;

import java.util.Date;
import java.util.List;

/**
 * Klasa reprezentujaca 
 * @author wkutyla
 *
 */
public class DocusafeEvent 
{	
	private Long eventId;
	private int eventStatus;
	private EventTrigger trigger;
	private EventHandler handler;
	private Date nextCheckDate;
    //private Date dueDate;
    private String params;
	private Long document_id;

    public DocusafeEvent() {
    }

	public DocusafeEvent(Long eventId, int eventStatus, String triggerCode, String handlerCode, Date nextCheckDate, String params) throws DocusafeEventException
	{
		this.eventId = eventId;
		this.eventStatus = eventStatus;
		this.params = params;
		this.trigger = EventTrigger.getInstance(eventId, triggerCode, params);
		this.handler = EventHandler.getInstance(eventId, handlerCode, params);
		this.nextCheckDate = nextCheckDate;
	}
	
	/**
	 * Rejestrowanie zdarzenia powiazanego z dokumentem
	 * @param eventStatus
	 * @param nextCheckDate
	 * @param params
	 * @param document_id
	 * @throws DocusafeEventException
	 */
	public DocusafeEvent(int eventStatus, Date nextCheckDate, String params,Long document_id) throws DocusafeEventException
	{
		this.eventStatus = eventStatus;
		this.params = params;
		this.nextCheckDate = nextCheckDate;
		this.document_id = document_id;
	}
	
	
	public DocusafeEvent(int eventStatus, Date nextCheckDate, String params) throws DocusafeEventException
	{
		this.eventStatus = eventStatus;
		this.params = params;
		this.nextCheckDate = nextCheckDate;
	}

    public static Optional<DocusafeEvent> find(Long eventId) throws EdmException {
        Optional<DocusafeEvent> event = Optional.absent();

        Criteria criteria = DSApi.context().session().createCriteria(DocusafeEvent.class);
        criteria.add(Restrictions.eq("eventId", eventId));

        List<DocusafeEvent> list = criteria.list();

        if(list.size() > 0) {
            return Optional.of(list.get(0));
        }

        return event;
    }

	public void setNextCheckDate() throws DocusafeEventException
	{
		EventFactory.setNextCheckDate(this.eventId, this.trigger.getNextCheckDateFromOldCheckDate(nextCheckDate));
	}

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public int getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(int eventStatus) {
		this.eventStatus = eventStatus;
	}

	public EventTrigger getTriggerObj() {
		return trigger;
	}

		public EventHandler getHandlerObj() {
		return handler;
	}

	public String getTrigger()
	{
		return this.trigger.getTriggerCode();
	}
	
	public void setTrigger(String trigger) throws Exception
	{
		this.trigger = EventTrigger.getInstance(eventId, trigger, this.params);
	}
	
	public String getHandler()
	{
		return this.handler.getHandlerCode();
	}
	
	public void setHandler(String handler) throws Exception
	{
		this.handler = EventHandler.getInstance(eventId, handler, this.params);
	}
	
	public Date getNextCheckDate() {
		return nextCheckDate;
	}

	public void setNextCheckDate(Date nextCheckDate) {
		this.nextCheckDate = nextCheckDate;
	}

	public Long getDocument_id() {
		return document_id;
	}

	public void setDocument_id(Long documentId) {
		document_id = documentId;
	}

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    @Override
    public String toString() {
        return "DocusafeEvent{" +
                "eventId=" + eventId +
                ", eventStatus=" + eventStatus +
                ", trigger=" + trigger +
                ", handler=" + handler +
                ", nextCheckDate=" + nextCheckDate +
                ", params='" + params + '\'' +
                ", document_id=" + document_id +
                '}';
    }
}
