package pl.compan.docusafe.events;

import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;

public class Start implements Runnable
{
	private String handlerCode;
	public Start(String handlerCode)
	{
		this.handlerCode = handlerCode;
	}
	
	public void run()
	{
		try
		{
			DSApi.openAdmin();
			for(DocusafeEvent ev : EventFactory.getToCheckListByHandler(handlerCode))
	    	{
	    		if(ev.getTriggerObj().trig())
	    		{
	    			ev.getHandlerObj().doHandle();
	    		}
	    		ev.setNextCheckDate();
	    		EventFactory.setEventStatus(ev.getEventId(), ev.getHandlerObj().getEndStatus());
	    	}
		} 
		catch (Exception e) {
			LoggerFactory.getLogger("mariusz").error("",e);
		}
		finally
		{
			DSApi._close();
		}
		
	}
}