package pl.compan.docusafe.events;

import java.util.HashMap;
import java.util.Map;

import com.google.common.base.Optional;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.events.handlers.DocumentMailHandler.DocumentMailBean;
import pl.compan.docusafe.util.FileUtils;

/**
 * Klasa zarzadzajaca obsluga jednego zdarzenia 
 * Kazda obsluga jest realizowana w innym watku, aby uniknac konfliktow
 * @author wkutyla
 *
 */
public abstract class EventHandler implements Runnable//extends Thread 
{
	private static final Log log = LogFactory.getLog(EventHandler.class);

	public final static String HANDLER_PACKAGE_NAME = EventHandler.class.getPackage().getName()+".handlers";
	
	public static final String PARAM_SIGN_SPLIT = "\\|";
	
	private static Map<String, Class<EventHandler>> handlerCodeToClassMap;

    private DocusafeEvent event;
	
	private static void initialize() throws DocusafeEventException
	{
		//FIXME - powinien szukac tez po pakiecie parametrization - tam chce przeniesc handlery
		try
		{
			handlerCodeToClassMap = new HashMap<String, Class<EventHandler>>();
			for(Class<EventHandler> clazz : FileUtils.getClassesForPackage(HANDLER_PACKAGE_NAME))
			{
				
				String objName = clazz.getName();
				try {
					clazz.newInstance();
				} catch (Exception e) {
					continue;
				}
				
				EventHandler tmpHandler = clazz.newInstance();
				handlerCodeToClassMap.put(tmpHandler.getHandlerCode(), clazz);
			}
		}
		catch (Exception e)
		{
			log.error("",e);
			throw new DocusafeEventException(e);
		}
	}
	
	public static EventHandler getInstance(Long eventId, String handlerCode, String handlerParams) throws DocusafeEventException
	{
		if(handlerCodeToClassMap == null || handlerCodeToClassMap.size() < 1)
		{
			initialize();
		}
		
		for (String keyy : handlerCodeToClassMap.keySet()) {
			log.trace(keyy);
			log.trace(handlerCodeToClassMap.get(keyy));
			
		}
		
		EventHandler resp = null;
		try
		{
			resp = handlerCodeToClassMap.get(handlerCode).newInstance();
			resp.parseStringParameters(handlerParams);
			resp.setEventId(eventId);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
			throw new DocusafeEventException(e);
		}
		return resp;
	}
	
	/**
	 * Nazwa kodowa handlera
	 * @return
	 */
	public abstract String getHandlerCode();
	
	/**
	 * metoda ustawia kontekst handlera wedlog podanego tekstu
	 * @param params
	 * @throws DocusafeEventException
	 */
	public abstract void parseStringParameters(String params) throws DocusafeEventException;
	
	/**
	 * metoda ktora wykonuje zadania zwiazane z obsloga eventu
	 * ma otwarty kontekst jako admin
	 */
	public abstract void doHandle();
	
	/**
	 * metoda ktora zwraca koncowy status dla eventa
	 * @return
	 */
	public abstract int getEndStatus();
	
	private Long eventId;
	
	public Long getEventId()
	{
		return eventId;
	}

	private void setEventId(Long eventId)
	{
		this.eventId = eventId;
	}

    protected DocusafeEvent getEvent() throws EdmException {
        if(event == null) {
            Optional<DocusafeEvent> eventOpt = DocusafeEvent.find(eventId);

            if(! eventOpt.isPresent()) {
                throw new EdmException("Cannot find event, eventId = "+eventId);
            } else {
                event = eventOpt.get();
            }
        }
        return event;
    }

	public final void run()
	{
		Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
		try
		{
			DSApi.openAdmin();
			//EventFactory.setEventStatus(eventId, EventFactory.IN_PROGRESS_EVENT_STATUS);
			
			if(!"true".equals(Docusafe.getAdditionProperty("handler.fake."+this.getHandlerCode())))
			{
				doHandle();
			}
			EventFactory.setEventStatus(eventId, getEndStatus());			
		}
		catch (Throwable e)
		{
			log.error("RUN EVENT HANDLER :"+e.getMessage(),e);
			//e.printStackTrace();
		}
		finally
		{
			DSApi._close();
		}
		Thread.yield();
	}
}
