package pl.compan.docusafe.events.rxjava;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import rx.Observable;
import rx.functions.Action1;

public class RxJavaCore {

    private static final Logger log = LoggerFactory.getLogger(RxJavaCore.class);

    public static void init() {
        log.info("[init] Initializing RxJava");
    }

    public void test() {

        Observable<String> obs = Observable.from("a", "b", "c");

        Observable.from(obs).subscribe(new Action1<Observable<String>>() {
            @Override
            public void call(Observable<String> str) {
                log.debug("str -> "+str);
            }
        });

        Observable.from(obs).subscribe(new Action1<Observable<String>>() {
            @Override
            public void call(Observable<String> str) {
                log.debug("str2 -> "+str);
            }
        });

    }
}
