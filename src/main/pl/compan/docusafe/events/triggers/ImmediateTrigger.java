package pl.compan.docusafe.events.triggers;

import java.util.Date;

import org.joda.time.Duration;
import org.joda.time.Instant;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventTrigger;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


public class ImmediateTrigger extends EventTrigger {
    private final static Logger LOG = LoggerFactory.getLogger(ImmediateTrigger.class);

	@Override
	public Date getNextCheckDateFromOldCheckDate(Date checkDate) {
		return new Instant().plus(Duration.standardMinutes(5)).toDate();
	}

	@Override
	public String getTriggerCode() {
		return "immediateTrigger";
	}

	@Override
	public boolean trig() throws DocusafeEventException {
        LOG.info("trig");
		return true;
	}

    @Override
    public void parseStringParameters(String params) throws DocusafeEventException  {}
}
