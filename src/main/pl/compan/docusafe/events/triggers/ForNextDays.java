package pl.compan.docusafe.events.triggers;

import java.util.Calendar;
import java.util.Date;

import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventTrigger;

public class ForNextDays extends EventTrigger {

private Integer days;
	
	public Date getNextCheckDateFromOldCheckDate(Date checkDate) 
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(checkDate);
		cal.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR) + days);
		Date nextCheckDate = new Date(cal.getTimeInMillis());
		return nextCheckDate;
	}

	public String getTriggerCode() 
	{
		return "ForNextDays";
	}

	public void parseStringParameters(String params) throws DocusafeEventException 
	{
		this.days = Integer.parseInt(params.split("\\|")[0].trim());
	}

	public boolean trig() throws DocusafeEventException 
	{
		return true;
	}
}
