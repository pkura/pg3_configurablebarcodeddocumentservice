package pl.compan.docusafe.events.triggers;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.OfficeCase;
import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.events.EventTrigger;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Date;

/**
 * Klasa kt�ra sprawdza czy sprawa zosta�a zamkni�ta i w przypadku braku zako�czenia
 * sprawy pozwala na wykonanie zdarzenia
 *
 * Jako parametr zdarzenia przyjmuje id sprawy
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class CaseFinishedTrigger extends EventTrigger {
    private final static Logger LOG = LoggerFactory.getLogger(CaseFinishedTrigger.class);

    long caseId;

    @Override
    public Date getNextCheckDateFromOldCheckDate(Date checkDate) {
        return checkDate;
    }

    @Override
    public String getTriggerCode() {
        return "case-finished-trigger";
    }

    @Override
    public boolean trig() throws DocusafeEventException {
        try {
            boolean ret = !OfficeCase.find(caseId).isClosed();
            LOG.info("return trig {}", ret);
            return ret;
        } catch (EdmException e) {
            throw new DocusafeEventException(e);
        }
    }

    @Override
    public void parseStringParameters(String params) throws DocusafeEventException {
        caseId = Long.valueOf(params);
    }
}
