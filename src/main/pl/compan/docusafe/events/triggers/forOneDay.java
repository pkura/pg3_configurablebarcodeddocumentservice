package pl.compan.docusafe.events.triggers;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

import pl.compan.docusafe.events.DocusafeEventException;
import pl.compan.docusafe.events.EventTrigger;


public class forOneDay extends EventTrigger 
{	
	@Override
	public Date getNextCheckDateFromOldCheckDate(Date checkDate) 
	{		
		Date nextCheckDate = DateUtils.addDays(checkDate, 1);
		nextCheckDate = DateUtils.setHours(nextCheckDate, 1);
		return nextCheckDate;
	}

	@Override
	public String getTriggerCode() {
		return "forOneDay";
	}

	@Override
	public void parseStringParameters(String params)throws DocusafeEventException
	{

	}

	@Override
	public boolean trig() throws DocusafeEventException 
	{
		return true;
	}

}
