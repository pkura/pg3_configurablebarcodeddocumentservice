package pl.compan.docusafe.ajax;

import java.util.Date;

import pl.compan.docusafe.util.DateUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Rezultat akcji ajaksowej.
 * Metoda {@link AjaxJsonResult#toJson()} zwraca obiekt jako JSON
 * Uwaga - data jest zwracano jako timestamp
 * @author Kamil
 *
 */
public class AjaxJsonResult {
	protected static Gson gson = new GsonBuilder()
		.registerTypeAdapter(Date.class, DateUtils.gsonTimestampSerializer)
		.registerTypeAdapter(Date.class, DateUtils.gsonTimestampDeserializer)
		.create();
	
	/**
	 * <code>false</code> oznacza b��d
	 */
	public boolean success = true;
	/**
	 * Kr�tki identyfikator b��du
	 */
	public String error;
	/**
	 * Opis (komunikat) b��du
	 */
	public String errorMessage;
	
	public String toJson() {
		return gson.toJson(this);
	}
}
