package pl.compan.docusafe.util;

import java.util.*;

/**
 * Klasa umo�liwiaj�ca przekazywanie kryteri�w wyszukiwania do metod
 * wyszukuj�cych np. dokumenty.  Nie umo�liwia tworzenia z�o�onych wyra�e�,
 * wszelkie kryteria trzymane s� w p�askiej strukturze.
 * <p>
 * Tworzenie z�o�onych zapyta� umo�liwia klasa {@link pl.compan.docusafe.core.FormQuery}.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: QueryForm.java,v 1.5 2008/02/22 17:28:58 mmanski Exp $
 */
public class QueryForm
{
    private int offset;
    private int limit;
    private List<SortField> sortFields = new ArrayList<SortField>(3);
    private Map<String, Object> properties = new HashMap<String, Object>();
    private String encoding;

    public QueryForm(int offset, int limit)
    {
        this.offset = offset;
        this.limit = limit;
    }

    public int getOffset()
    {
        return offset;
    }

    public int getLimit()
    {
        return limit;
    }

    public void addOrderAsc(String field)
    {
        for (Iterator<SortField> iter=sortFields.iterator(); iter.hasNext(); )
        {
            if (iter.next().getName().equals(field))
                return;
        }
        sortFields.add(SortField.asc(field));
    }

    public void addOrderDesc(String field)
    {
        for (Iterator<SortField> iter=sortFields.iterator(); iter.hasNext(); )
        {
            if (iter.next().getName().equals(field))
                return;
        }
        sortFields.add(SortField.desc(field));
    }

    public void addProperty(String name, Object value)
    {
        if (name == null)
            throw new NullPointerException("name");
        if (value == null)
            throw new NullPointerException("value");
        if (properties.get(name) != null)
            throw new IllegalArgumentException("Istnieje ju� pole "+name);
        properties.put(name, value);
    }

    public Object getProperty(String name)
    {
        return properties.get(name);
    }

    public Map<String, Object> getProperties() {
        return properties;
    }

    public boolean hasProperty(String name)
    {
        return getProperty(name) != null;
    }

    public boolean hasSortFields()
    {
        return !sortFields.isEmpty();
    }

    public List getSortFields()
    {
        return sortFields;
    }

    public String toString()
    {
        return "QueryForm[properties="+properties+", offset="+offset+" limit="+limit+" sortFields="+sortFields+"]";
    }

    public static class SortField
    {
        private String name;
        private boolean ascending;

        public String getName()
        {
            return name;
        }

        public boolean isAscending()
        {
            return ascending;
        }

        public SortField(String name, boolean ascending)
        {
            this.name = name;
            this.ascending = ascending;
        }

        private static SortField asc(String name)
        {
            return new SortField(name, true);
        }

        private static SortField desc(String name)
        {
            return new SortField(name, false);
        }
    }


    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }
}
