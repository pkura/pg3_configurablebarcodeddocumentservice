package pl.compan.docusafe.util.filter;

import pl.compan.docusafe.core.users.DSUser;

class NoneFilter implements UserFilter{

	public boolean acceptCurrentUser() {
		return false;
	}

	public boolean accept(DSUser o) {
		return false;
	}

}