package pl.compan.docusafe.util.filter;

import java.util.Collection;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.DivisionNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.filter.FilterUtils.AndUserFilter;
import pl.compan.docusafe.util.filter.FilterUtils.OrUserFilter;



/**
 * Filtry u�ytkownik�w
 * @author Micha� Sankowski
 */
public class UserFilters {

	private final static Logger LOG = LoggerFactory.getLogger(UserFilters.class);
    /**
	 * Filtr, kt�ry akceptuje/przepuszcza wszystko
	 */
	public static final UserFilter ALL_PASS = new pl.compan.docusafe.util.filter.NullFilter();

	/**
	 * Filtr, kt�ru nikogo nie akceptuje
	 */
	public static final UserFilter NONE_PASS = new pl.compan.docusafe.util.filter.NoneFilter();

	public final static UserFilter divisionFilter(String divisionGuid) throws EdmException{
		try{
			return new DivisionFilter(DSDivision.find(divisionGuid));
		} catch (DivisionNotFoundException e) {
			LOG.warn("Nieistniej�cy dzia�:" + divisionGuid);
			return NONE_PASS;
		}
	}

	public final static UserFilter divisionFilter(DSDivision division){
		return new DivisionFilter(division);
	}

	public final static UserFilter fromXml(Element xml) throws EdmException{
		return XmlFilterParser.userFilter(xml);
	}

	public static UserFilter or(Collection<UserFilter> filters){
		if(filters.size() == 1){
			return filters.iterator().next();
		} else {
			return new OrUserFilter(filters);
		}
	}

	public static UserFilter and(Collection<UserFilter> filters){
		if(filters.size() == 1){
			return filters.iterator().next();
		} else {
			return new AndUserFilter(filters);
		}
	}

	private static class DivisionFilter extends AbstractUserFilter{
		private final DSDivision division;

		public DivisionFilter(DSDivision division) {
			this.division = division;
		}

		public boolean accept(DSUser o) {
			try {
				return o.inOriginalDivision(division);
			} catch (EdmException ex) {
				LOG.error(ex.getMessage(), ex);
				return false;
			}
		}

		@Override
		public boolean equals(Object obj) {
			return (obj instanceof DivisionFilter)
					&& ((DivisionFilter)obj).division.equals(this.division);
		}

		@Override
		public int hashCode() {
			return division.hashCode();
		}

		@Override
		public String toString() {
			return "nale�y do " + division.getName();
		}
	}


}
