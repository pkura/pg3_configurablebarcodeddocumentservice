package pl.compan.docusafe.util.filter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *
 * @author Micha� Sankowski
 */
public class FilterUtils {

	private final static Logger LOG = LoggerFactory.getLogger(FilterUtils.class);

	public static <T> Filter<T> or(Collection<Filter<T>> filters){
		if(filters.size() == 1){
			return filters.iterator().next();
		} else {
			return new OrFilter<T>(filters);
		}
	}

	public static <T> Filter<T> and(Collection<Filter<T>> filters){
		if(filters.size() == 1){
			return filters.iterator().next();
		} else {
			return new AndFilter<T>(filters);
		}
	}

	

	private static class AndFilter<T> implements Filter<T>{
		private final List<? extends Filter<T>> filters;

		AndFilter(Collection<? extends Filter<T>> filters){
			this.filters = new ArrayList<Filter<T>>(filters);
		}

		public boolean accept(T o) {
			for(Filter<T> filter:filters){
				if(!filter.accept(o)){
					return false;
				}
			}
			return true;
		}

		@Override
		public String toString() {
			if(filters.size() > 0){
				StringBuilder ret = new StringBuilder();
				ret.append(filters.get(0).toString());
				for(int i=1; i<filters.size(); ++i){
					ret.append("\ni " + filters.get(i).toString());
				}
				return ret.toString();
			} else {
				return "";
			}
		}
	}

	private static class OrFilter<T> implements Filter<T>{
		private final List<? extends Filter<T>> filters;

		OrFilter(Collection<? extends Filter<T>> filters){
			this.filters = new ArrayList<Filter<T>>(filters);
		}

		public boolean accept(T o) {
			for(Filter<T> filter:filters){
				if(filter.accept(o)){
					return true;
				}
			}
			return false;
		}

		@Override
		public String toString() {
			if(filters.size() > 0){
				StringBuilder ret = new StringBuilder();
				ret.append(filters.get(0).toString());
				for(int i=1; i<filters.size(); ++i){
					ret.append("\nlub " + filters.get(i).toString());
				}
				return ret.toString();
			} else {
				return "";
			}
		}
	}

	static class AndUserFilter extends AndFilter<DSUser> implements UserFilter{

		public AndUserFilter(Collection<? extends Filter<DSUser>> filters) {
			super(filters);
		}
		public boolean acceptCurrentUser() {
			try {
				return accept(DSApi.context().getDSUser());
			} catch (EdmException ex) {
				LOG.error(ex.getMessage() ,ex);
				return false;
			}
		}
	}

	static class OrUserFilter extends OrFilter<DSUser> implements UserFilter{

		public OrUserFilter(Collection<? extends Filter<DSUser>> filters) {
			super(filters);
		}
		public boolean acceptCurrentUser() {
			try {
				return accept(DSApi.context().getDSUser());
			} catch (EdmException ex) {
				LOG.error(ex.getMessage() ,ex);
				return false;
			}
		}
	}
}
