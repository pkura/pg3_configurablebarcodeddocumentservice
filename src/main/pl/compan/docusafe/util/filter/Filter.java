package pl.compan.docusafe.util.filter;

/**
 *
 * @author Micha� Sankowski
 */
public interface Filter<T> {

	/**
	 * Akceptuje dany obiekt
	 * @param o
	 * @return
	 */
	boolean accept(T o);

}
