package pl.compan.docusafe.util.filter;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *
 * @author Micha� Sankowski
 */
public abstract class AbstractUserFilter implements UserFilter{

	private final static Logger LOG = LoggerFactory.getLogger(AbstractUserFilter.class);

	public boolean acceptCurrentUser() {
		try {
			return accept(DSApi.context().getDSUser());
		} catch (UserNotFoundException ex) {
			return false;
		} catch (EdmException ex) {
			LOG.error(ex.getMessage(), ex);
			return false;
		}
	}
}
