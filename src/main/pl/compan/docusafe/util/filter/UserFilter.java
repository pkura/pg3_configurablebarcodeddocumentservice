package pl.compan.docusafe.util.filter;

import pl.compan.docusafe.core.users.DSUser;

/**
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface UserFilter extends Filter<DSUser>{

	/**
	 * Akceptuje tego u�ytkownika
	 * @return
	 */
	boolean acceptCurrentUser();


}





