package pl.compan.docusafe.util.filter;

import pl.compan.docusafe.core.users.DSUser;

class NullFilter implements UserFilter
{

	public boolean acceptCurrentUser() {
		return true;
	}

	public boolean accept(DSUser o) {
		return true;
	}

	@Override
	public String toString() {
		return "zawsze";
	}
}