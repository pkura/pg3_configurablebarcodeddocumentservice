package pl.compan.docusafe.util.filter;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Element;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *
 * @author Micha� Sankowski
 */
class XmlFilterParser {

	private final static Logger log = LoggerFactory.getLogger(XmlFilterParser.class);

	private final static String GUID_NODE_NAME = "guid";
	private final static String OR_NODE_NAME = "or";
	private final static String AND_NODE_NAME = "and";

	static UserFilter userFilter(Element xml) throws EdmException{
		List<Element> elements = xml.elements();

		if(elements.size() < 1){
			throw new IllegalArgumentException("Brak warunk�w dla u�ytkownika");
		}

		List<UserFilter> list = userFilterSubelements(elements);

		return UserFilters.or(list);
	}

	private static UserFilter userFilterFromElement(Element element) throws EdmException{
		if(element.getName().equals(GUID_NODE_NAME)){
			return UserFilters.divisionFilter(element.getText().trim());
		} else if(element.getName().equals(OR_NODE_NAME)) {
			return UserFilters.or(userFilterSubelements(element.elements()));
		} else if(element.getName().equals(AND_NODE_NAME)) {
			return UserFilters.and(userFilterSubelements(element.elements()));
		} else {
			throw new IllegalArgumentException("nieznany element:" + element.getName());
		}
	}

	private static List<UserFilter> userFilterSubelements(List<Element> elements) throws EdmException {
		List<UserFilter> list = new ArrayList<UserFilter>(elements.size());
		for (Element e : elements) {
			list.add(userFilterFromElement(e));
		}
		return list;
	}
}
