package pl.compan.docusafe.util;

import ognl.OgnlContext;
import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.*;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: FormatUtils.java,v 1.10 2008/10/06 10:10:45 pecet4 Exp $
 */
public class FormatUtils
{
    private static final Object INSTRING = new Object();
    private static final Object INCODE = new Object();

    private static class Token { }

    private static class Field extends Token
    {
        private String name;

        public Field(String name)
        {
            this.name = name;
        }

        public String getName()
        {
            return name;
        }

        public String toString() { return "Field["+name+"]"; }
    }

    private static class Literal extends Token
    {
        private String chars;

        public Literal(String chars)
        {
            this.chars = chars;
        }

        public String getChars()
        {
            return chars;
        }

        public String toString() { return "Literal["+chars+"]"; }
    }

    /**
     * Analizuje przekazany format i zwraca tablic� obiekt�w
     * {@link Field} i {@link Literal}. Obiekt Field odpowiada
     * kodowi formatuj�cemu uj�temu w nawiasy klamrowe, np.
     * <code>{d0}</code>, za� obiekt Literal pozosta�ym ci�gom
     * znak�w, znajduj�cym si� mi�dzy kodami.
     * @param format
     * @return
     * @throws EdmException
     */
    private static Token[] parse(String format) throws EdmException
    {
        // na tej li�cie znajd� si� obiekty Field i Literal
        List tokens = new LinkedList();

        CharacterIterator iter = new StringCharacterIterator(format);

        // w tej zmiennej akumulowane s� znaki nie b�d�ce kodami
        StringBuilder tmpString = new StringBuilder();

        // flaga okre�laj�ca, czy p�tla jest w trakcie akumulowania
        // znak�w napisu (INSTRING) czy wewn�trz kodu (INCODE)
        Object where = null;

        for (char c=iter.first(); c != CharacterIterator.DONE; c=iter.next())
        {
            if (c == '{')
            {
                // je�eli do tej pory przetwarzano napis, jest on dodawany
                // do listy jako token Literal
                if (where == INSTRING)
                {
                    if (tmpString.length() > 0) tokens.add(new Literal(tmpString.toString()));
                    tmpString.setLength(0);
                }

                where = INCODE;

                StringBuilder code = new StringBuilder(16);

                for (c = iter.next(); c != CharacterIterator.DONE; c=iter.next())
                {
                    if (c == '}')
                        break;
                    code.append(c);
                }

                if (c == CharacterIterator.DONE)
                    throw new EdmException("Niedomkni�ty nawias klamrowy, znak "+iter.getIndex());

                if (code.length() == 0)
                    throw new EdmException("Napotkano puste nawiasy klamrowe, znak "+iter.getIndex());

                tokens.add(new Field(code.toString()));
            }
            else
            {
                where = INSTRING;
                tmpString.append(c);
            }
        }

        // je�eli format ko�czy� si� napisem, dopiero tutaj ten
        // napis jest dodawany to tablicy expr
        if (where == INSTRING)
        {
            if (tmpString.length() > 0) tokens.add(new Literal(tmpString.toString()));
            tmpString.setLength(0);
        }

        return (Token[]) tokens.toArray(new Token[tokens.size()]);
    }

    public static void validateSyntax(String format) throws EdmException
    {
        parse(format);
    }

    public static String formatExpression(String expression, Map context) throws EdmException
    {
        Token[] tokens = parse(expression);

        StringBuilder ognl2 = new StringBuilder();

        for (int i=0; i < tokens.length; i++)
        {
            if (ognl2.length() > 0) ognl2.append(" + ");

            if (tokens[i] instanceof Field)
            {
                String var = ((Field) tokens[i]).getName();
                if (tokens.length > i+1 && tokens[i+1] instanceof Literal)
                {
                    ognl2.append("(#"+var+" != null ? #"+var+
                        // kolejnym elementem po Field musi by� Literal (o ile taki element istnieje)
                        (++i < tokens.length ? " + '"+((Literal) tokens[i]).getChars()+"'" : "") +
                        " : '')");
                }
                else
                {
                    ognl2.append("(#"+var+" != null ? #"+var+" : '')");
                }
            }
            else
            {
                ognl2.append("'"+((Literal) tokens[i]).getChars()+"'");
            }
        }

        OgnlContext ctx = new OgnlContext(context);

        // jako root object przyjmuj� Object.class (nie jest u�ywany)
        return new OgnlExpression(ognl2.toString()).getValue(ctx, Object.class).toString();
    }

    /**
     * @param object Obiekt, na podstawie kt�rego w�asno�ci zostan� u�yte
     * przez wyra�enie formatuj�ce.
     * @param expression Wyra�enie formatuj�ce (kody w�asno�ci uj�te
     *  w nawiasy klamrowe).
     * @ param propertyMap Tablica mapuj�ca kody w�asno�ci (key) na nazwy
     *  w�asno�ci przekazanego obiektu (value).
     * @return
     * @deprecated Nale�y u�ywa� formatExpression
     */
    public static String format(Object object, Map context,
                                String expression)
        throws EdmException
    {
        // przeszukuje tablic� w poszukiwaniu kod�w postaci "{znaki}"
        // zamienia je na wyra�enia OGNL zgodnie z tablic� propertyMap

//        class Property
//        {
//            String property;
//            Property(String property) { this.property = property; }
//        }

        class Variable
        {
            String variable;
            Variable(String variable) { this.variable = variable; }
        }

        CharacterIterator iter = new StringCharacterIterator(expression);

        List expr = new LinkedList();
        // w tej zmiennej akumulowane s� znaki nie b�d�ce kodami
        StringBuilder tmpString = new StringBuilder();

        // flaga okre�laj�ca, czy p�tla jest w trakcie akumulowania
        // znak�w napisu (INSTRING) czy wewn�trz kodu (INCODE)
        Object where = null;

        // w tej p�tli do tablicy expr dodawane s� na przemian
        // obiekty String i Variable

        for (char c=iter.first(); c != CharacterIterator.DONE; c=iter.next())
        {
            if (c == '{')
            {
                if (where == INSTRING)
                {
                    if (tmpString.length() > 0) expr.add(tmpString.toString());
                    tmpString.setLength(0);
                }

                where = INCODE;

                StringBuilder code = new StringBuilder(5);

                for (c = iter.next(); c != CharacterIterator.DONE; c=iter.next())
                {
                    if (c == '}')
                        break;
                    code.append(c);
                }

                if (c == CharacterIterator.DONE)
                    throw new EdmException("Niedomkni�ty nawias klamrowy");

                if (code.length() == 0)
                    throw new EdmException("Napotkano puste nawiasy klamrowe");

//                if (context.get(code.toString()) == null)
//                    throw new EdmException("Napotkano nieznany kod: "+code);

//                if (propertyMap.get(code.toString()) == null)
//                    throw new EdmException("Napotkano nieznany kod: "+code);

                expr.add(new Variable(code.toString()));
                //expr.add(new Property((String) propertyMap.get(code.toString())));
            }
            else
            {
                where = INSTRING;
                tmpString.append(c);
            }
        }

        // je�eli format ko�czy� si� napisem, dopiero tutaj ten
        // napis jest dodawany to tablicy expr
        if (where == INSTRING)
        {
            if (tmpString.length() > 0) expr.add(tmpString.toString());
            tmpString.setLength(0);
        }


        // na podstawie tablicy expr tworzone jest wyra�enie
        // OGNL; dla ka�dego elementu tablicy typu Property
        // poprzedzonego obiektem String tworzone jest wyra�enie
        // (prop != null ? string + prop : ""), pozosta�e
        // s� przepisywane wprost

        StringBuilder ognl2 = new StringBuilder();

        for (int i=0; i < expr.size(); i++)
        {
            Object obj = expr.get(i);

            if (ognl2.length() > 0) ognl2.append(" + ");

            if (obj instanceof String)
            {
                // istnieje kolejny element listy
                // (w�wczas jest on typu Property)
                if (i+1 < expr.size())
                {
                    String str = (String) expr.get(i);
                    //Property prop = (Property) expr.get(i+1);
                    Variable var = (Variable) expr.get(i+1);
                    i++;
                    ognl2.append("(#"+var.variable+" != null ? "+
                        "'"+str+"' + #"+var.variable+" : '')");
                }
                else
                {
                    ognl2.append("'"+expr.get(i)+"'");
                }
            }
            else
            {
                //ognl2.append(((Property) obj).property);
                ognl2.append("#"+((Variable) obj).variable);
            }
        }

        OgnlContext ctx = new OgnlContext(context);

        return new OgnlExpression(ognl2.toString()).getValue(ctx, object).toString();
    }

    public static boolean hasField(String expression, String field) throws EdmException
    {
        Token[] tokens = parse(expression);

        for (int i=0; i < tokens.length; i++)
        {
            if (tokens[i] instanceof Field && ((Field) tokens[i]).getName().equals(field))
                return true;
        }

        return false;
    }

    /**
     * Zwraca zbi�r z nazwami wszystkich p�l u�ytych w formacie.
     */
    public static Set getFields(String expression) throws EdmException
    {
        Token[] tokens = parse(expression);
        Set fields = new HashSet();

        for (int i=0; i < tokens.length; i++)
        {
            if (tokens[i] instanceof Field)
                fields.add(((Field) tokens[i]).getName());
        }

        return fields;
    }

    public static void validate(String expression, Set requiredCodes, Set allowedCodes)
        throws EdmException
    {
        Token[] tokens = parse(expression);
        Set requiredCopy = new HashSet(requiredCodes);

        // ka�dy napotkany kod usuwam z required i sprawdzam, czy istnieje w allowed
        // je�eli nie istnieje w allowed - wyj�tek
        // je�eli pozosta�o co� w required - wyj�tek

        for (int i=0; i < tokens.length; i++)
        {
            if (tokens[i] instanceof Field)
            {
                requiredCopy.remove(((Field) tokens[i]).getName());
                if (!allowedCodes.contains(((Field) tokens[i]).getName()))
                    throw new EdmException("Niedozwolony kod: "+((Field) tokens[i]).getName());
            }
        }

        if (requiredCopy.size() > 0)
            throw new EdmException("Brakuje wymaganych kod�w: "+
                StringUtils.join(requiredCopy.iterator(), ", "));
    }

    public static void main(String[] args) throws EdmException
    {
        Map map = new HashMap();
        map.put("d1", "division1");
        map.put("c", "caseSequenceNumber");
        map.put("y2", "year2");

//        Set req = new HashSet();
//        req.add("c");
//        req.add("y2");
//
//        validate(args[0], req, map.keySet());
    }
}
