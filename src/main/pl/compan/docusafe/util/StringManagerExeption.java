package pl.compan.docusafe.util;

import pl.compan.docusafe.core.EdmException;

public class StringManagerExeption extends EdmException
{

	public StringManagerExeption(String message)
	{
		super(message);
	}

}
