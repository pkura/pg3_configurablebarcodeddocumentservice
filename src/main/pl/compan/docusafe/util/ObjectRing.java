package pl.compan.docusafe.util;

import java.util.Arrays;

/* User: Administrator, Date: 2005-06-28 10:32:02 */

/**
 * Kolekcja trzymaj�ca obiekty w tablicy o sta�ej d�ugo�ci.  Je�eli
 * dodanie nowego obiektu spowodowa�oby przekroczenie rozmiaru tablicy,
 * ostatni obiekt jest z niej usuwany.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ObjectRing.java,v 1.5 2008/10/06 10:51:33 pecet4 Exp $
 */
public class ObjectRing
{
    private Object[] ring;
    private int head = -1;
    private int tail;

    public ObjectRing(int size)
    {
        if (size <= 0)
            throw new IllegalArgumentException("size="+size+"<=0");
        ring = new Object[size];
    }

    public void add(Object object)
    {
        head++;

        if (head >= ring.length) // head wysz�o poza tablic�
        {
            head = 0; //head % ring.length;
            if (head == tail) tail++;
        }
        else if (head == tail && head != 0)
        {
            tail++;
        }

        if (tail >= ring.length)
        {
            tail = tail % ring.length;
        }

        ring[head] = object;
    }

    /**
     * @return wszystkie obiekty znajduj�ce si� w kolekcji.
     */
    public Object[] list(Object[] template)
    {
        if (head < 0) return instantiateArray(template, 0);

        Object[] result;
        if (tail <= head)
        {
            result = instantiateArray(template, head - tail + 1);
            System.arraycopy(ring, tail, result, 0, result.length);
        }
        else // head < tail
        {
            result = instantiateArray(template, head + 1 + (ring.length - tail));
            // TODO: bez jaj, ten warunek jest zawsze prawdziwy
            if (tail < ring.length)
                System.arraycopy(ring, tail, result, 0, ring.length - tail);
            System.arraycopy(ring, 0, result, ring.length - tail, head + 1);
        }
        return result;
    }

    private Object[] instantiateArray(Object[] template, int length)
    {
        if (template == null)
            return new Object[length];
        if (template.length == length)
            return template;
        else
            return (Object[]) java.lang.reflect.Array.newInstance(
                template.getClass().getComponentType(),
                length);
    }

    public static void main(String[] args)
    {
        ObjectRing ring = new ObjectRing(2);
        for (int i=0; i < 3; i++)
        {
            ring.add("Komunikat "+(i+1));
        }
    }
}
