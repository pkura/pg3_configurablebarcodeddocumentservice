package pl.compan.docusafe.util;

import java.util.List;

import pl.compan.docusafe.core.GlobalPreferences;

/**
 * Klasa generuj�ca HTML przedstawiaj�cy drzewo dowolnych obiekt�w
 * na podstawie modelu reprezentowanego przez {@link HtmlTreeModel}.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: HtmlTree.java,v 1.14 2008/10/23 14:35:38 pecet1 Exp $
 */
public class HtmlTree
{
	StringManager sm = GlobalPreferences.loadPropertiesFile(HtmlTree.class.getPackage().getName(), null);
    private HtmlTreeModel model;

    //-----------------
    
    private int[] columnInfo = new int[1];
    
    private static final int LAST_SIBLING = 1;		//?
    private static final int MIDDLE_SIBLING = 2;	//?

    private HtmlTreeTheme theme;
    private String contextPath;

    //add
    private Long selectedFolderId = new Long(-1);
    
    private HtmlTreeStyleVisitor styleVisitor;
    private HtmlTreeImageVisitor imageVisitor;
    private HtmlTreeAdditionalButtonVisitor additionalButtonVisitor;
    
    
    //dodatkowy wybor akcji mozna tam dac jakis link
    private String additionLink;
    private String additionName;

    public HtmlTree(HtmlTreeModel model, String contextPath)
    {
        this.model = model;
        this.contextPath = contextPath != null ? contextPath : "";
        this.theme = model.getTheme();
    }

    public void setStyleVisitor(HtmlTreeStyleVisitor styleVisitor)
    {
        this.styleVisitor = styleVisitor;
    }

    public void setImageVisitor(HtmlTreeImageVisitor imageVisitor)
    {
        this.imageVisitor = imageVisitor;
    }

    public void setAdditionalButtonVisitor(HtmlTreeAdditionalButtonVisitor additionalButtonVisitor) {
        this.additionalButtonVisitor = additionalButtonVisitor;
    }

    public String generateTree()
    {
    	index_1 = 0;
    	index_2 = 0;
    	find = false;

    	selectedFolderId = new Long (432); // id_req;
        StringBuilder out = new StringBuilder();

        Object rootElement = model.getRootElement();

        // numer kolejnej kolumny drzewa; root dostaje 0, je�eli
        // istnieje
        int nextColumn = 0;

        out.append("<nobr>");

        // ga��� g��wna
        if (rootElement != null)
        {
            HtmlImage image = getIconImage(rootElement, true);
            //out.append("<nobr>");
            out.append("<img src='"+contextPath + image.getSrc()+"' " +
                "width='"+image.getWidth()+"' " +
                "height='"+image.getHeight()+"' " +
                "border='0' " +
                "class='tree'>");
            out.append("&nbsp;");
           
            String styleClass =
                styleVisitor != null ? styleVisitor.getStyleClass(rootElement) : null;

            out.append("<a class='"+(styleClass != null ? styleClass : theme.getLinkClass())+"' " +
                "name='"+model.getAnchorName(rootElement)+"' href='"+model.getUrl(rootElement)+"'>");
            //if (model.isHighlighted(rootElement)) out.append("<strong>");
            //if (model.isEmphasised(rootElement)) out.append("<em>");
            out.append(model.getTitle(rootElement));
            //if (model.isEmphasised(rootElement)) out.append("</em>");
            //if (model.isHighlighted(rootElement)) out.append("</strong>");
            out.append("</a>");
            //out.append("</nobr>");
            out.append("<br>");

            setColumnInfo(0, LAST_SIBLING);

            nextColumn++;
        }

        walkTree(out, rootElement, model.getChildElements(rootElement), nextColumn);

        out.append("</nobr>");

        return out.toString();
    }

    /**
     * Rekurencyjnie generuje reszt� drzewa rozwijaj�c tylko te
     * ga��zie, dla kt�rych metoda {@link HtmlTreeModel#isExpanded}
     * zwraca true.
     * @param out
     * @param parent Element nadrz�dny dla listy element�w childElements.
     *  U�ywany do generowania odno�nika do elementu nadrz�dnego (ikonka
     *  z minusem s�u��ca do "zwijania" fragmentu drzewa prowadzi do folderu
     *  nadrz�dnego).
     * @param childElements Lista element�w, kt�re maj� by� pokazane na
     *  aktualnym poziomie. Przekazuj� list�, zamiast pozwoli� metodzie
     *  na samodzielne pobranie tej listy z elementu parent, aby unikn��
     *  dwukrotnego pobierania element�w podrz�dnych z tego samego elementu
     *  nadrz�dnego.
     */
    
    private int index_1;
	private int currId;
	private int index_2;
	private boolean find;
    private void walkTree(StringBuilder out, Object parent, List childElements,
                          int column)
    {
        if (childElements == null || childElements.size() == 0)
            return;      
    	
        for (int j=0; j < childElements.size(); j++)
        {
            Object child1 = childElements.get(j);
            List children1 = model.getChildElements(child1);
            
            if (model.isSelected(child1))
            {
            	find = true;
            	currId = index_1;
            }
            index_1++;
        }
        
        //for gl
        for (int i=0, n=childElements.size(); i < n; i++)
        {
            Object child = childElements.get(i);
            List children = model.getChildElements(child);
            
            setColumnInfo(column, i < n-1 ? MIDDLE_SIBLING : LAST_SIBLING);

            boolean isLastSibling = i == n-1;
            boolean isExpanded = model.isExpanded(child);
            boolean hasChildren = model.hasChildElements(child);
            String link = model.getUrl(child);

            // wype�niacz (pionowe linie, gdzie s� potrzebne, w pozosta�ych
            // miejscach pusty obrazek)
                        
            for (int col=1; col < column; col++)
            {
                if (getColumnInfo(col) == MIDDLE_SIBLING)
                {
                    out.append("<img " +
                        "src='"+contextPath+theme.getVerticalLineImage().getSrc()+"' " +
                        "width='"+theme.getVerticalLineImage().getWidth()+"' " +
                        "height='"+theme.getVerticalLineImage().getHeight()+"' " +
                        "border='0' class='tree'>");
                }
                else //if (getColumnInfo(col) == LAST_SIBLING)
                {
                    out.append("<img " +
                        "src='"+contextPath+theme.getBlankImage().getSrc()+"' " +
                        "width='"+theme.getBlankImage().getWidth()+"' " +
                        "height='"+theme.getBlankImage().getHeight()+"' " +
                        "border='0' class='tree'>");
                }
            }
            
            // odpowiedni znaczek (plus/minus) przy ikonce folderu
            if (isExpanded)
            {
            	
            	
                String parentLink = parent != null ? model.getUrl(parent) : null;
                String parentAnchorName = parent != null ? model.getAnchorName(parent) : null;
                if (isLastSibling)
                {
                	// mimo oznaczenia elementu jako rozwini�tego (expanded),
                    // nie mo�na wykluczy�, �e model zwr�ci takie oznaczenie
                    // dla folderu nie maj�cego folder�w podrz�dnych
                    if (hasChildren)
                    {
                    	// #1
                        out.append((parentLink != null ? "<a name='"+model.getAnchorName(child)+"' class='"+theme.getControlLinkClass()+"' href='"+parentLink+"'" : "") +
                            "<img " +
                                "src='"+contextPath+theme.getLastExpandedControlImage().getSrc()+"' " +
                                "width='"+theme.getLastExpandedControlImage().getWidth()+"' " +
                                "height='"+theme.getLastExpandedControlImage().getHeight()+"' " +
                                "border='0' class='tree'>" +
                            (parentLink != null ? "</a>" : ""));
                    }
                    else
                    {
                        out.append("<img " +
                            "src='"+contextPath+theme.getLastChildlessControlImage().getSrc()+"' " +
                            "width='"+theme.getLastChildlessControlImage().getWidth()+"' " +
                            "height='"+theme.getLastChildlessControlImage().getHeight()+"' " +
                            "border='0' class='tree'>");
                    }
                }
                else
                {
                    if (hasChildren)
                    { // #2 
                        out.append((parentLink != null ? "<a name='"+model.getAnchorName(child)+"' class='"+theme.getControlLinkClass()+"' href='"+parentLink+"'>" : "") +
                            "<img " +
                            "src='"+contextPath+theme.getMiddleExpandedControlImage().getSrc()+"' " +
                            "width='"+theme.getMiddleExpandedControlImage().getWidth()+"' " +
                            "height='"+theme.getMiddleExpandedControlImage().getHeight()+"' " +
                            "border='0' class='tree'>" +
                            (parentLink != null ? "</a>" : ""));
                    }
                    else
                    {
                        out.append("<img " +
                            "src='"+contextPath+theme.getMiddleChildlessControlImage().getSrc()+"' " +
                            "width='"+theme.getMiddleChildlessControlImage().getWidth()+"' " +
                            "height='"+theme.getMiddleChildlessControlImage().getHeight()+"' " +
                            "border='0' class='tree'>");
                    }
                }
            }
            else
            {
                if (isLastSibling)
                {
                    if (hasChildren)
                    {
                    	// #3
                        out.append("<a name='"+model.getAnchorName(child)+"' class='"+theme.getControlLinkClass()+"' href='"+link+"'>" +
                            "<img " +
                            "src='"+contextPath+theme.getLastCollapsedControlImage().getSrc()+"' " +
                            "width='"+theme.getLastCollapsedControlImage().getWidth()+"' " +
                            "height='"+theme.getLastCollapsedControlImage().getHeight()+"' " +
                            "border='0' class='tree'></a>");
                    }
                    else
                    {
                        out.append("<img " +
                            "src='"+contextPath+theme.getLastChildlessControlImage().getSrc()+"' " +
                            "width='"+theme.getLastChildlessControlImage().getWidth()+"' " +
                            "height='"+theme.getLastChildlessControlImage().getHeight()+"' " +
                            "border='0' class='tree'>");
                    }
                }
                else
                {
                	// #4
                    if (hasChildren)
                    {
                        out.append("<a name='"+model.getAnchorName(child)+"' class='"+theme.getControlLinkClass()+"' href='"+link+"'>" +
                            "<img " +
                            "src='"+contextPath+theme.getMiddleCollapsedControlImage().getSrc()+"' " +
                            "width='"+theme.getMiddleCollapsedControlImage().getWidth()+"' " +
                            "height='"+theme.getMiddleCollapsedControlImage().getHeight()+"' " +
                            "border='0' class='tree'></a>");
                    }
                    else
                    {
                        out.append("<img " +
                            "src='"+contextPath+theme.getMiddleChildlessControlImage().getSrc()+"' " +
                            "width='"+theme.getMiddleChildlessControlImage().getWidth()+"' " +
                            "height='"+theme.getMiddleChildlessControlImage().getHeight()+"' " +
                            "border='0' class='tree'>");
                    }
                }
            }

            HtmlImage image = getIconImage(child, isExpanded);
            out.append("<img src='"+contextPath+image.getSrc()+"' " +
                "width='"+image.getWidth()+"' " +
                "height='"+image.getHeight()+"' " +
                "border='0' " +
                "class='tree'>");

            // obrazek folderu (otwarty lub zamkni�ty)
/*
            if (isExpanded)
            {
                out.append("<img src='"+contextPath+theme.getExpandedFolderImage().getSrc()+"' " +
                    "width='"+theme.getExpandedFolderImage().getWidth()+"' " +
                    "height='"+theme.getExpandedFolderImage().getHeight()+"' " +
                    "border='0' " +
                    "class='tree'>");
            }
            else
            {
                out.append("<img src='"+contextPath+theme.getCollapsedFolderImage().getSrc()+"' " +
                    "width='"+theme.getCollapsedFolderImage().getWidth()+"' " +
                    "height='"+theme.getCollapsedFolderImage().getHeight()+"' " +
                    "border='0' " +
                    "class='tree'>");
            }
*/

            String styleClass =
                styleVisitor != null ? styleVisitor.getStyleClass(child) : null;

            out.append("&nbsp;");
            out.append("<a name='"+model.getAnchorName(child)+"' class='"+(styleClass != null ? styleClass : theme.getLinkClass())+"' href='"+link+"'>");
            //if (model.isHighlighted(child)) out.append("<strong>");
            //if (model.isEmphasised(child)) out.append("<em>");
            out.append(model.getTitle(child));
            //if (model.isEmphasised(child)) out.append("</em>");
            //if (model.isHighlighted(child)) out.append("</strong>");
            out.append("</a>");
            
            if(additionLink != null && additionName != null)
            {
            	out.append("<a href=\"javascript:void(window.opener.accept('"+model.getDivision(child)+"','"+additionLink+"','"+model.getTitle(child)+"',null));javascript:void(window.close());\">");
            	out.append(" ("+sm.getString("wybierz")+")");
            	out.append("</a>");
            }

            putAdditionalButtonAction(child, out);
            out.append("<br>");
            
            if ( ( currId-2) == index_2 )
            {
            	if ( find )
                {
                	out.append("<a name='k1'></a>");
                }
            }
            
            	
            
          /*  if ( idDirAnc.equals(((Folder)child).getId()) )
            {
            	out.append("<a name='k1'></a>");
            } */

            if (isExpanded && hasChildren)
            {
                walkTree(out, child, model.getChildElements(child), column+1);
            }
        }
    }

    /**
     * Tworzy dodatkow� akcj� na elemencie
     * @param child
     * @param out
     */
    private void putAdditionalButtonAction(Object child, StringBuilder out) {
        if(additionalButtonVisitor != null){
            if(additionalButtonVisitor.hasAdditionalAction(child)){
                additionalButtonVisitor.getAdditionalAction(child, out);
            }
        }

    }

    private HtmlImage getIconImage(Object element, boolean expanded)
    {
        HtmlImage image = null;
        if (imageVisitor != null)
        {
            image = imageVisitor.getIconImage(element, expanded);
        }
        if (image == null)
        {
            if (expanded)
                image = theme.getExpandedFolderImage();
            else
                image = theme.getCollapsedFolderImage();
        }
        return image;
    }

    private void setColumnInfo(int column, int info)
    {
        if (columnInfo.length < column+1)
        {
            int[] tmp = new int[column+1];
            System.arraycopy(columnInfo, 0, tmp, 0, columnInfo.length);
            columnInfo = tmp;
        }

        columnInfo[column] = info;
    }

    private int getColumnInfo(int column)
    {
        if (columnInfo.length < column+1)
            throw new IllegalArgumentException("columnInfo.length="+columnInfo.length+
                ", column="+column);
        return columnInfo[column];
    }

	public Long getSelectedFolderId() {
		return selectedFolderId;
	}

	public void setSelectedFolderId(Long selectedFolderId) {
		this.selectedFolderId = selectedFolderId;
	}

	public String getAdditionLink() {
		return additionLink;
	}

	public void setAdditionLink(String additionLink) {
		this.additionLink = additionLink;
	}

	public String getAdditionName() {
		return additionName;
	}

	public void setAdditionName(String additionName) {
		this.additionName = additionName;
	}
}
