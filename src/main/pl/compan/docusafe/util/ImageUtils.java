package pl.compan.docusafe.util;

import com.asprise.util.tiff.TIFFReader;
import com.sun.media.jai.codec.FileSeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;

import javax.imageio.ImageIO;
import javax.media.jai.JAI;
import javax.media.jai.RenderedOp;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.RenderedImage;
import java.awt.image.WritableRaster;
import java.awt.image.renderable.ParameterBlock;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ImageUtils.java,v 1.14 2010/04/06 16:35:23 wkuty Exp $
 */
public class ImageUtils
{
	static StringManager sm = 
        GlobalPreferences.loadPropertiesFile("",null);
    private static final Log log = LogFactory.getLog(ImageUtils.class);
    
    public static BufferedImage getBufferedImage(RenderedImage ri)
    {
        if (ri instanceof BufferedImage)
            return (BufferedImage) ri;

        ColorModel cm = ri.getColorModel();
        WritableRaster raster = cm.createCompatibleWritableRaster(ri.getWidth(), ri.getHeight());
/*
        Hashtable<String, Object> properties = new Hashtable<String, Object>();
        String[] keys = ri.getPropertyNames();
        if (keys!=null)
        {
            for (int i = 0; i < keys.length; i++)
            {
                properties.put(keys[i], ri.getProperty(keys[i]));
            }
        }
*/
        BufferedImage bi = new BufferedImage(cm, raster,
            cm.isAlphaPremultiplied(), null);
        ri.copyData(raster);
        return bi;
    }

    public static BufferedImage getSubimage(BufferedImage im,
                                            int x, int y, int w, int h)
    {
        // BufferedImage.getSubimage zwraca obraz o niew�a�ciwych wymiarach
        BufferedImage sub = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        sub.createGraphics().drawImage(im, 0, 0, w, h, x, y, x+w, y+h, null);
        return sub;
    }

    public static void save(BufferedImage im, String format, File file)
        throws IOException
    {		
        FileOutputStream os = new FileOutputStream(file);
        ParameterBlock pb = new ParameterBlock();
        pb.addSource(im);
        pb.add(os);
        pb.add(format);
        RenderedOp op = JAI.create("encode", pb);
        op.dispose();
        os.close();
    }

    public static Point getImageSize(InputStream file, String mime)
        throws IOException
    {
        BufferedImage image = ImageIO.read(file);
        if (image != null)
        {
            return new Point(image.getWidth(), image.getHeight());
        }
/*
        Image image = Toolkit.getDefaultToolkit().getImage(file.getAbsolutePath());
        if (image != null)
        {
            return new Point(image.getWidth(null), image.getHeight(null));
        }
*/
        return null;
    }

    public static void main(String[] args) throws Exception
    {
    	
        TIFFReader reader = new TIFFReader(new File(args[0]));
        BufferedImage im = ImageUtils.getBufferedImage(reader.getPage(0));

        save(getSubimage(im, 1200, 1700, 1000, 1000), "jpeg", new File(args[1]));

/*
        ParameterBlock pb = new ParameterBlock();
        pb.addSource(im);
        pb.add(new Float(1200));
        pb.add(new Float(1700));
        pb.add(new Float(1000));
        pb.add(new Float(1000));

        RenderedOp op = JAI.create("crop", pb);
        forceLoad(op);
        save(getBufferedImage(op), "jpeg", new File(args[1]));
*/

/*
        BufferedImage newIm = im.getSubimage(1200, 1700, 1000, 1000);
        Raster data = newIm.getData(new Rectangle(newIm.getMinX(), newIm.getMinY(),
            newIm.getWidth(), newIm.getHeight()));
*/

//        BufferedImage cim = new BufferedImage(1000, 1000, im.getType());
//        cim.setData(im.getData(new Rectangle(1200, 1700, 1000, 1000)));

/*
        newIm.setData(data);
*/

//        save(cim, "jpeg", new File(args[1]));
    }
    
    
    /**
     * Dzieli na strony przekazany plik TIFF. Je�eli w pliku jest nie wi�cej ni� jedna strona rzucany
     * jest odpowiedni wyj�tek, w przeciwnym razie jest dzielony na pliki PNG.
     */
    public static File[] splitTIFFToPNG(File tif) throws EdmException
    {
        if (tif == null || !tif.exists() || !tif.isFile())        
            throw new EdmException(sm.getString("WybranyPlikDoPodzialuNieIstniejeLubJestNiepoprawny"));
        
        File[] result = null;

        final String format = "png";

        // dzielenie tifa na strony
        SeekableStream s = null;
        try
        {
            s = new FileSeekableStream(tif);
            ImageDecoder dec = ImageCodec.createImageDecoder("tiff", s, null);

            if (dec.getNumPages() == 0)
                throw new EdmException(sm.getString("WwybranymPlikuDoPodzialuBrakujeStron"));

            if (dec.getNumPages() == 1)
                throw new EdmException(sm.getString("WybranyPlikZawieraTylkoJednaStroneInieZostalPodzielony"));
            
            if (dec.getNumPages() > 1)
            {
                List<File> imageFiles = new ArrayList<File>(dec.getNumPages());

                for (int i=0; i < dec.getNumPages(); i++)
                {
                    File tmp = File.createTempFile("docusafe_fax", "."+format);

                    RenderedImage im = dec.decodeAsRenderedImage(i);

                    if (log.isDebugEnabled())
                        log.debug("width="+im.getWidth()+", height="+im.getHeight());

                    // faksy mog� przychodzi� w rozdzielczo�ci 204x196 dpi lub 204x98 dpi
                    // w tym drugim wypadku nale�y faks przeskalowa� o czynnik 2.0 w pionie
                    if (im.getWidth() > im.getHeight())
                    {
                        if (log.isDebugEnabled())
                            log.debug("skalowanie 1.0x2.0");
                        ParameterBlock pb = new ParameterBlock();
                        pb.addSource(im);
                        pb.add(new Float(1.0));
                        pb.add(new Float(2.0));
                        pb.add(new Float(0.0));
                        pb.add(new Float(0.0));
                        im = JAI.create("scale", pb);
                    }

                    if (log.isDebugEnabled())
                        log.debug("Tworzenie pliku strumieniem");
                    // stream
                    FileOutputStream os = new FileOutputStream(tmp);
                    ParameterBlock pb = new ParameterBlock();
                    pb.addSource(im);
                    pb.add(os);
                    pb.add(format);
                    RenderedOp op = JAI.create("encode", pb);
                    forceLoad(op);
                    op.dispose();
                    os.close();

                    log.info("Utworzono plik "+tmp+" o rozmiarze "+tmp.length());

                    imageFiles.add(tmp);
                    result = (File[]) imageFiles.toArray(new File[imageFiles.size()]);
                }
            }
        }
        catch (IOException e)
        {
           throw new EdmException(sm.getString("BladPodczasDzieleniaPliku"), e);
        }
        finally
        {
            try {if (s != null) s.close(); } catch (IOException e) {};
        }

        return result;
    }
    
    private static void forceLoad(RenderedImage op)
    {
        // wymuszanie zako�czenia operacji
        // http://java.sun.com/products/java-media/jai/forDevelopers/jaifaq.html#deferred

        if (op instanceof RenderedOp)
        {
            ((RenderedOp) op).getRendering();
        }

        int minX = op.getMinTileX();
        int minY = op.getMinTileY();
        int maxX = minX + op.getNumXTiles();
        int maxY = minY + op.getNumYTiles();

        for (int y = minY; y < maxY; y++)
        {
            for (int x = minX; x < maxX; x++)
            {
                op.getTile(x, y);
            }
        }
    }
    
    /**
     * Zwraca true <=> mo�na przekszta�ci� plik o danym typie mime do TIFF-a.
     */
    public static boolean canConvertToTiff(String mime)
    {
        return (mime.indexOf("image/png") >= 0 ||
                mime.indexOf("image/bmp") >= 0 ||
                mime.indexOf("image/gif") >= 0 ||
                mime.indexOf("image/jpeg") >= 0 ||
                mime.indexOf("image/tiff") >= 0);
    }

    public static BufferedImage rotateImage(BufferedImage image, int angle) {
        double sin = Math.abs(Math.sin(Math.toRadians(angle)));
        double cos = Math.abs(Math.cos(Math.toRadians(angle)));
        int w = image.getWidth();
        int h = image.getHeight();
        int newW = (int) Math.floor(w * cos + h * sin);
        int newH = (int) Math.floor(h * cos + w * sin);

        BufferedImage bimg = new BufferedImage(newW,newH,image.getType());
        Graphics2D g = bimg.createGraphics();
        g.translate((newW - w) / 2, (newH - h) / 2);
        g.rotate(Math.toRadians(angle), w / 2, h / 2);
        g.drawRenderedImage(image, null);
        g.dispose();
        return bimg;
    }
}
