package pl.compan.docusafe.util;

import java.util.Locale;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: LocaleUtil.java,v 1.5 2007/08/29 15:12:58 lk Exp $
 */
public class LocaleUtil
{
    /**
     * Lista j�zyk�w dost�pnych w Docusafe. Je�eli bie��ce Locale
     * ({@link java.util.Locale#getDefault()}) nie znajduje si� na li�cie,
     * domy�lnym Locale dla aplikacji b�dzie pierwszy element tej tablicy.
     */
    public static final Locale[] AVAILABLE_LOCALES = new Locale[] {
        new Locale("pl", "PL"),
        new Locale("de", "DE"),
        new Locale("it", "IT"),
        new Locale("en", "GB")};

    private static Locale locale;


    /**
     * Zwraca domy�lne Locale dla aplikacji.
     */
    public synchronized static Locale getLocale()
    {
        if (locale == null)
        {
            for (int i=0; i < AVAILABLE_LOCALES.length; i++)
            {
                if (AVAILABLE_LOCALES[i].equals(Locale.getDefault()))
                {
                    locale = Locale.getDefault();
                    break;
                }
            }
            if (locale == null)
                locale = AVAILABLE_LOCALES[0];
        }
        return locale;
    }
}