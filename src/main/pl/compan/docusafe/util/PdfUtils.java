package pl.compan.docusafe.util;

import com.artofsolving.jodconverter.DefaultDocumentFormatRegistry;
import com.artofsolving.jodconverter.DocumentConverter;
import com.artofsolving.jodconverter.DocumentFormat;
import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.converter.OpenOfficeDocumentConverter;
import com.lowagie.text.*;
import com.lowagie.text.pdf.*;
import com.lowagie.text.pdf.codec.BmpImage;
import com.lowagie.text.pdf.codec.TiffImage;
import org.antlr.stringtemplate.StringTemplate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.AdditionManager;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.AttachmentRevision;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.cfg.Configuration;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry.EntryType;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Remark;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.nationwide.LayeredPdf;
import pl.compan.docusafe.web.viewserver.ViewerAction;

import javax.sql.DataSource;
import java.io.*;
import java.net.ConnectException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: PdfUtils.java,v 1.37 2009/09/29 10:19:03 tomekl Exp $
 */
public abstract class PdfUtils
{
	static StringManager sm = 
        GlobalPreferences.loadPropertiesFile(PdfUtils.class.getPackage().getName(),null);
    private static final Log log = LogFactory.getLog(PdfUtils.class);
  
    /**
     * Generowanie dokumentu PDF, skleja zalaczniki w jeden pdf oraz jezeli zaznaczone checkbox'y to dokleja do pdf'a tabele z informacjami o
     * uwagach, historii dekretacji, historii pisma dla dokumentu
     * @param documents
     * @param remarksLayer
     * @param os
     * @param remarks wartosc z checkbox'a
     * @param history wartosc z checkbox'a
     * @param attribution wartosc z checkbox'a
     * @param beginning
     * @param keywords
     * @param ending
     * @param skippedFirst
     * @throws DocumentException
     * @throws EdmException
     * @throws IOException
     */
    public static void attachmentsAsPdfWith(Long[] documents,
    					boolean remarksLayer, OutputStream os,boolean remarks, boolean history, boolean attribution,
    					Element beginning, String keywords, Element ending, boolean skippedFirst)
            	throws DocumentException, EdmException, IOException
	{
    	_attachmentsAsPdfWithAdditional(documents, remarksLayer,
                os,  remarks,  history,  attribution,  beginning,  keywords,  ending, skippedFirst);
	}
    /**
     * Generowanie dokumentu PDF z��czaj�c wszystkie za��czniki z podanych dokument�w.
     * 
     * @param documents   tablica dokument�w, kt�rych za��czniki do��czy�
     * @param remarksLayer   true <=> do��czane b�d� te� uwagi z dokument�w
     * @param os   strumie�, na kt�ry wynik wypisa�
     * @param skippedFirst   true <=> informacja o pomi�tych za��cznikach (czyli tych, kt�re by�y w 
     * nieobs�ugiwanych formatach) zostanie dodana przed plikami; wpp. zostanie dodana za plikami
     */
    public static void attachmentsAsPdf(Document[] documents,
                                        boolean remarksLayer, OutputStream os, boolean skippedFirst)
        throws DocumentException, EdmException, IOException
    {
        _attachmentsAsPdf(documents, remarksLayer, os, null, null, null, skippedFirst);
    }

    /**
     * Generowanie dokumentu PDF z��czaj�c wszystkie za��czniki z podanych dokument�w.
     * 
     * @param documents   tablica dokument�w, kt�rych za��czniki do��czy�
     * @param remarksLayer   true <=> do��czane b�d� te� uwagi z dokument�w
     * @param os   strumie�, na kt�ry wynik wypisa�
     */    
    public static void attachmentsAsPdf(Document[] documents,
            boolean remarksLayer, OutputStream os)
        throws DocumentException, EdmException, IOException
    {
        _attachmentsAsPdf(documents, remarksLayer, os, null, null, null, false);
    }
    
    /**
     * Generowanie dokumentu PDF z��czaj�c wszystkie za��czniki z podanych dokument�w.
     * 
     * @param documents   tablica identyfikator�w dokument�w, kt�rych za��czniki do��czy�
     * @param remarksLayer   true <=> do��czane b�d� te� uwagi z dokument�w
     * @param os   strumie�, na kt�ry wynik wypisa�
     * @param beginning   wst�p - gotowy Element dla PDF, kt�ry w�o�y� na pierwsz� stron�
     * @param keywords   s�owa kluczowe, kt�re zapisa� w PDF w polu "keywords"
     * @param ending   zako�czenie - gotowy Element dla PDF, kt�ry w�o�y� na ostatni� stron�
     * @param skippedFirst   true <=> informacja o pomi�tych za��cznikach (czyli tych, kt�re by�y w 
     * nieobs�ugiwanych formatach) zostanie dodana przed plikami; wpp. zostanie dodana za plikami
     */
    public static void attachmentsAsPdf(Long[] documents,
                                        boolean remarksLayer, OutputStream os, 
                                        Element beginning, String keywords, Element ending, 
                                        boolean skippedFirst)
        throws DocumentException, EdmException, IOException
    {
        _attachmentsAsPdf((Object[]) documents, remarksLayer, os, beginning, keywords, ending, skippedFirst);
    }

    public static boolean canAddToPdf(String mime)
    {
        return mime.indexOf("image/tiff") >= 0 ||
            mime.indexOf("image/gif") >= 0 ||
            mime.indexOf("image/jpeg") >= 0 ||
            mime.indexOf("image/png") >= 0 ||
            mime.indexOf("image/bmp") >= 0 ||
            mime.indexOf("application/pdf") >= 0;
    }

    public static Table getRemarksToTable(Document document) throws IOException, DocumentException, EdmException{
    	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm");
    	File fontDir= new File(Docusafe.getHome(), "fonts");
     	File arial= new File(fontDir, "arial.ttf");
        BaseFont baseFont= BaseFont.createFont(arial.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        Font fontSuperSmall= new Font(baseFont, (float) 6);
 		Font fontSmall= new Font(baseFont, (float) 8);
 		Font fontNormal= new Font(baseFont, (float) 12);
 		Font fontNormal_Large= new Font(baseFont, (float) 14);
 		Font fontLarge= new Font(baseFont, (float) 16);
 		
 		DSUser dsUser = null; 
    	
    	Cell dataCell = new Cell(new Paragraph("Data:", fontNormal));
        Cell osobaCell = new Cell(new Paragraph("Osoba:", fontNormal));
        Cell descCell = new Cell(new Paragraph("Uwagi:", fontNormal));
         
    	Table remarksToTable = new Table(3);
    	remarksToTable.setCellsFitPage(true);
    	remarksToTable.setWidth(100);
    	remarksToTable.setPadding(3);
    	remarksToTable.setBorder(1);
    	remarksToTable.setWidths( new int[]{20,20,60});
        
    	remarksToTable.addCell(dataCell);
    	remarksToTable.addCell(osobaCell);
    	remarksToTable.addCell(descCell);
    	
    	List lstRemarks = ((OfficeDocument) document).getRemarks();
        for (Iterator iter=lstRemarks.iterator(); iter.hasNext(); )
        {
            Remark remark = (Remark) iter.next();
            dsUser = DSUser.findByUsername(remark.getAuthor());
            remarksToTable.addCell(new Cell(new Paragraph(sdf.format(remark.getCtime()), fontNormal)));
            remarksToTable.addCell(new Cell(new Paragraph(dsUser.getFirstname() + " " + dsUser.getLastname(), fontNormal)));
            remarksToTable.addCell(new Cell(new Paragraph(remark.getContent(), fontNormal)));
            
        }
    	return remarksToTable;
    }
    
    public static Table getHistoryToTable(Document document) throws IOException, DocumentException, EdmException{
    	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm");
    	File fontDir= new File(Docusafe.getHome(), "fonts");
     	File arial= new File(fontDir, "arial.ttf");
        BaseFont baseFont= BaseFont.createFont(arial.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        Font fontSuperSmall= new Font(baseFont, (float) 6);
 		Font fontSmall= new Font(baseFont, (float) 8);
 		Font fontNormal= new Font(baseFont, (float) 12);
 		Font fontNormal_Large= new Font(baseFont, (float) 14);
 		Font fontLarge= new Font(baseFont, (float) 16);
 		
 		DSUser dsUser = null; 
    	
    	Cell dataCell = new Cell(new Paragraph("Data:", fontNormal));
        Cell osobaCell = new Cell(new Paragraph("Osoba:", fontNormal));
        Cell descCell = new Cell(new Paragraph("Historia pisma:", fontNormal));
         
    	Table historyToTable = new Table(3);
    	historyToTable.setCellsFitPage(true);
    	historyToTable.setWidth(100);
    	historyToTable.setPadding(3);
    	historyToTable.setBorder(1);
    	historyToTable.setWidths( new int[]{20,20,60});
        
    	historyToTable.addCell(dataCell);
    	historyToTable.addCell(osobaCell);
    	historyToTable.addCell(descCell);
    	
    	List workHistory = document.getWorkHistory();
    	for (Iterator iter=workHistory.iterator(); iter.hasNext(); )
        {
            Audit workEntry = (Audit) iter.next();
            dsUser = DSUser.findByUsername(workEntry.getUsername());
            historyToTable.addCell(new Cell(new Paragraph(sdf.format(workEntry.getCtime()), fontNormal)));
            historyToTable.addCell(new Cell(new Paragraph(dsUser.getFirstname() + " " + dsUser.getLastname(), fontNormal)));
            historyToTable.addCell(new Cell(new Paragraph(workEntry.getDescription(), fontNormal)));
        }
    	return historyToTable;
    }
    
    public static Table getAttributionToTable(Document document) throws IOException, DocumentException, EdmException{
    	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm");
    	StringTemplate template = null;
    	boolean initDone = false;
    	
    	File fontDir= new File(Docusafe.getHome(), "fonts");
     	File arial= new File(fontDir, "arial.ttf");
        BaseFont baseFont= BaseFont.createFont(arial.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        Font fontSuperSmall= new Font(baseFont, (float) 6);
 		Font fontSmall= new Font(baseFont, (float) 8);
 		Font fontNormal= new Font(baseFont, (float) 12);
 		Font fontNormal_Large= new Font(baseFont, (float) 14);
 		Font fontLarge= new Font(baseFont, (float) 16);
 		
 		DSUser dsUser = null; 
    	
    	Cell dataCell = new Cell(new Paragraph("Data:", fontNormal));
        Cell osobaCell = new Cell(new Paragraph("Osoba:", fontNormal));
        Cell descCell = new Cell(new Paragraph("Historia dekretacji:", fontNormal));
         
    	Table attributionToTable = new Table(3);
    	attributionToTable.setCellsFitPage(true);
    	attributionToTable.setWidth(100);
    	attributionToTable.setPadding(3);
    	attributionToTable.setBorder(1);
    	attributionToTable.setWidths( new int[]{20,20,60});
        
    	attributionToTable.addCell(dataCell);
    	attributionToTable.addCell(osobaCell);
    	attributionToTable.addCell(descCell);
    	
    	List assHistory = ((OfficeDocument)document).getAssignmentHistory();
    	List workHistory = document.getWorkHistory();
    	for(Iterator iter = assHistory.iterator(); iter.hasNext();){
    		AssignmentHistoryEntry assignEntry = (AssignmentHistoryEntry) iter.next();
    		dsUser = DSUser.findByUsername(assignEntry.getSourceUser());
    		
    		if(EntryType.JBPM_INIT.equals(assignEntry.getType()) && !initDone) {
                template = new StringTemplate(sm.getString("mma.JBPM_INIT"));
                initDone = true;
            }
            else if(EntryType.JBPM_INIT.equals(assignEntry.getType())) {
                continue;
            } else if(EntryType.JBPM4_REASSIGN_SINGLE_USER.equals(assignEntry.getType())) {
                template = new StringTemplate(sm.getString("mma.JBPM4_REASSIGN_SINGLE_USER"));
            } else if(EntryType.JBPM4_REASSIGN_DIVISION.equals(assignEntry.getType())) {
                template = new StringTemplate(sm.getString("mma.JBPM4_REASSIGN_DIVISION"));
            } else if(EntryType.JBPM4_ACCEPT_TASK.equals(assignEntry.getType())) {
                template = new StringTemplate(sm.getString("mma.JBPM4_ACCEPT_TASK"));
            } else {
                template = new StringTemplate(sm.getString("jbpm4."+assignEntry.getType()));
            }
			
    		
    		attributionToTable.addCell(new Cell(new Paragraph(sdf.format(assignEntry.getCtime()), fontNormal)));
    		attributionToTable.addCell(new Cell(new Paragraph(dsUser.getFirstname() + " " + dsUser.getLastname(), fontNormal)));
    		
            if(template != null && template.toString().length() > 0) { 
            	String processName = assignEntry.getProcessName();
            	// \\d Any digit, short for [0-9]
            	processName = processName.split("-\\d")[0];
                template.setAttribute("type",("mma."+ processName).equals(sm.getString("mma."+ processName)) ? null : sm.getString("mma."+ processName));
                template.setAttribute("targetUser",assignEntry.getTargetUser() != null && !assignEntry.getTargetUser().equals("x") ? DSUser.findByUsername(assignEntry.getTargetUser()).asFirstnameLastname() : null);
                template.setAttribute("targetDivision",assignEntry.getTargetGuid() != null && !assignEntry.getTargetGuid().equals("x") ? DSDivision.find(assignEntry.getTargetGuid()).getName() : null);
                template.setAttribute("clerk",assignEntry.getClerk() != null ? DSUser.findByUsername(assignEntry.getClerk()).asFirstnameLastname() : null);
                template.setAttribute("deadline",String.valueOf("BRAK").equals(assignEntry.getDeadline()) ? null : assignEntry.getDeadline());
                template.setAttribute("objective","x".equals(assignEntry.getObjective()) ? null : assignEntry.getObjective());
                attributionToTable.addCell(new Cell(new Paragraph(template.toString(), fontNormal)));
            } else {
            	attributionToTable.addCell(new Cell(new Paragraph(template.toString(), fontNormal)));
            }
    	}
    	return attributionToTable;
    }
    
    private static void _attachmentsAsPdfWithAdditional(Object[] entities, boolean remarksLayer,
            OutputStream os, boolean remarks, boolean history, boolean attribution, Element beginning, String keywords, Element ending,
            boolean skippedFirst) throws DocumentException, EdmException, IOException{
    	
    	 java.util.List<InputStream> files = new ArrayList<InputStream>();

         StringBuilder strSkipped = new StringBuilder();

         StringBuilder sbForPdf = null;
         
         HashMap<String, Table> mapTable = new HashMap();
         
         SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm");
         
         DSUser dsUser = null;
         
         int count = 0;
         
         Document document = null;
         
        
         
         for (int i=0; i < entities.length; i++)
         {
             
             if (entities[i] instanceof Document){
             	document = (Document) entities[i];
             }
                 
             else{
             	document = Document.find((Long) entities[i]);
             	boolean init = false;
             }

             if(remarksLayer && sbForPdf == null && document instanceof OfficeDocument){
                if(remarks == true){
                	mapTable.put("uwagi", getRemarksToTable(document));
                }
             	if(attribution == true){
             		mapTable.put("dekretacja", getAttributionToTable(document));
             	}
             	if(history ==  true){
             		mapTable.put("historia", getHistoryToTable(document));
             	}
             }

             List<Attachment> attachments = document.listAttachments();
             count += attachments.size();
             for (Attachment attachment : attachments)
             {
                 AttachmentRevision revision = attachment.getMostRecentRevision();
                 boolean toSkip = false;
                 String skipMsg = null;
                 if (revision != null && attachment.getDocument().canReadAttachments()
                     && canAddToPdf(revision.getMime()))
                 {
                 	try
                 	{
                 		files.add(revision.getImageAsPDF());
                 	}
                 	catch (Exception e) 
                 	{
                 		toSkip = true;
                 		skipMsg = sm.getString("BladPobieraniaZalacznika");
 					}
                 }
                 else
                 {
                 	toSkip = true;
                 	skipMsg = sm.getString("BrakUprawnien");
                 }
                 
                 if(toSkip)
                 {
                     Integer officeNumber = null;
                     if (entities[i] instanceof OfficeDocument)
                     {
                         officeNumber = ((OfficeDocument) entities[i]).getOfficeNumber();
                     }

                     StringBuilder message = new StringBuilder(sm.getString("PominietoPlik")+" ");
                     message.append(revision.getOriginalFilename());
                     message.append("\n");
                     if (officeNumber != null)
                     {
                         message.append(sm.getString("NumerKO")+": ");
                         message.append(officeNumber);
                         message.append("\n");
                     }
                     else
                     {
                         message.append(sm.getString("DokumentNr")+" ");
                         message.append(document.getId());
                         message.append("\n");
                     }

                     if (entities[i] instanceof OfficeDocument)
                     {
                         message.append(sm.getString("Tytul")+": ");
                         message.append(((OfficeDocument) entities[i]).getSummary());
                         message.append("\n");
                     }
                     else
                     {
                         message.append(sm.getString("Tytul")+": ");
                         message.append(document.getTitle());
                         message.append("\n");
                     }

                     message.append(sm.getString("TytulZalacznika")+": ");
                     message.append(attachment.getTitle());
                     message.append("\n");

                     if (Docusafe.hasExtra("business"))
                     {
                         /*if (Nationwide.nwDocument(document))
                         {
                             Map vm = document.getDoctype().getValueMapByCn(document.getId());
                             if (vm.get("NR_POLISY") != null)
                             {
                                 message.append(sm.getString("NumerPolisy")+": ");
                                 message.append(vm.get("NR_POLISY"));
                             }
                         }*/
                     }

                     message.append(sm.getString("SkipCause"));
                     message.append(skipMsg);
                     message.append("\n");
                     
                     strSkipped.append(message);
                     strSkipped.append("\n\n");
                 }
             }
         }
 		String layer = null;
 		if(AvailabilityManager.isAvailable("viewAttaAsPdf.addInformationAboutUser"))
 		{
 			layer = sm.getString("ZalacznikPobranyPrzeUzytkownikaDnia",
 					DSApi.context().getDSUser().asFirstnameLastname(),DateUtils.formatCommonDateTime(new Date()));
 		}
 		if(!mapTable.isEmpty() && count != 0){
 			LayeredPdf.streamsAsPdf(files, null, mapTable,
 					strSkipped.toString(), skippedFirst,  os,
 					 beginning,  keywords,  ending, layer, document );
 		}
 		else if(count==0){
         	 LayeredPdf.streamsAsPdf(files,
         			 sbForPdf != null ? sbForPdf.toString() : null,
         	            sm.getString("WybraneDokumentyNiePosiadajaZalacznikow"), skippedFirst,
         	            os, beginning, keywords, ending,layer);
         } 
         else
         {
 	        LayeredPdf.streamsAsPdf(files,
 	        		sbForPdf != null ? sbForPdf.toString() : null,
 	            strSkipped.toString(), skippedFirst,
 	            os, beginning, keywords, ending,layer,document);
         }

    	
    }
    
    
    
    
    /**
     * Generowanie dokumentu PDF z��czaj�c wszystkie za��czniki z podanych dokument�w.
     * 
     * @param entities   tablica dokument�w (albo w postaci identyfikator�w albo ca�ych dokument�w)
     * @param remarksLayer   true <=> do��czane b�d� te� uwagi z dokument�w
     * @param os   strumie�, na kt�ry wynik wypisa�
     * @param beginning   wst�p - gotowy Element dla PDF, kt�ry w�o�y� na pierwsz� stron�
     * @param keywords   s�owa kluczowe, kt�re zapisa� w PDF w polu "keywords"
     * @param ending   zako�czenie - gotowy Element dla PDF, kt�ry w�o�y� na ostatni� stron�
     * @param skippedFirst   true <=> informacja o pomi�tych za��cznikach (czyli tych, kt�re by�y w 
     * nieobs�ugiwanych formatach) zostanie dodana przed plikami; wpp. zostanie dodana za plikami
     */
    private static void _attachmentsAsPdf(Object[] entities, boolean remarksLayer,
                                         OutputStream os, Element beginning, String keywords, Element ending,
                                         boolean skippedFirst)
        throws DocumentException, EdmException, IOException
    {
        java.util.List<InputStream> files = new ArrayList<InputStream>();

        StringBuilder strSkipped = new StringBuilder();

        StringBuilder remarks = null;
        int count = 0;
        Document document = null;
        for (int i=0; i < entities.length; i++)
        {

            if (entities[i] instanceof Document)
                document = (Document) entities[i];
            else
                document = Document.find((Long) entities[i]);

            if (remarksLayer && remarks == null && document instanceof OfficeDocument)
            {
                remarks = new StringBuilder();
                List lstRemarks = ((OfficeDocument) document).getRemarks();
                for (Iterator iter=lstRemarks.iterator(); iter.hasNext(); )
                {
                    Remark remark = (Remark) iter.next();
                    remarks.append(remark.getContent());
                    remarks.append("\n\n");
                }
            }

            List<Attachment> attachments = document.listAttachments();
            count += attachments.size();
            for (Attachment attachment : attachments)
            {
                AttachmentRevision revision = attachment.getMostRecentRevision();
                boolean toSkip = false;
                String skipMsg = null;
                if (revision != null && attachment.getDocument().canReadAttachments()
                        && canAddToPdf(revision.getMime()))
                {
                    try
                    {
                        files.add(revision.getImageAsPDF());
                    }
                    catch (Exception e)
                    {
                        toSkip = true;
                        skipMsg = sm.getString("BladPobieraniaZalacznika");
                    }
                }
                else
                {
                    toSkip = true;
                    skipMsg = sm.getString("BrakUprawnien");
                }

                if(toSkip)
                {
                    Integer officeNumber = null;
                    if (entities[i] instanceof OfficeDocument)
                    {
                        officeNumber = ((OfficeDocument) entities[i]).getOfficeNumber();
                    }

                    StringBuilder message = new StringBuilder(sm.getString("PominietoPlik")+" ");
                    message.append(revision.getOriginalFilename());
                    message.append("\n");
                    if (officeNumber != null)
                    {
                        message.append(sm.getString("NumerKO")+": ");
                        message.append(officeNumber);
                        message.append("\n");
                    }
                    else
                    {
                        message.append(sm.getString("DokumentNr")+" ");
                        message.append(document.getId());
                        message.append("\n");
                    }

                    if (entities[i] instanceof OfficeDocument)
                    {
                        message.append(sm.getString("Tytul")+": ");
                        message.append(((OfficeDocument) entities[i]).getSummary());
                        message.append("\n");
                    }
                    else
                    {
                        message.append(sm.getString("Tytul")+": ");
                        message.append(document.getTitle());
                        message.append("\n");
                    }

                    message.append(sm.getString("TytulZalacznika")+": ");
                    message.append(attachment.getTitle());
                    message.append("\n");

                    message.append(sm.getString("SkipCause"));
                    message.append(skipMsg);
                    message.append("\n");

                    strSkipped.append(message);
                    strSkipped.append("\n\n");
                }
            }
        }
        String layer = null;
        if(AvailabilityManager.isAvailable("viewAttaAsPdf.addInformationAboutUser"))
        {
            layer = sm.getString("ZalacznikPobranyPrzeUzytkownikaDnia",
                    DSApi.context().getDSUser().asFirstnameLastname(),DateUtils.formatCommonDateTime(new Date()));
        }
        if(count==0){
            LayeredPdf.streamsAsPdf(files,
                    remarks != null ? remarks.toString() : null,
                    sm.getString("WybraneDokumentyNiePosiadajaZalacznikow"), skippedFirst,
                    os, beginning, keywords, ending,layer);
        }
        else
        {
            LayeredPdf.streamsAsPdf(files,
                    remarks != null ? remarks.toString() : null,
                    strSkipped.toString(), skippedFirst,
                    os, beginning, keywords, ending,layer,document);
        }
    }

    /**
     * Generuje do strumienia plik PDF zawieraj�cy w sobie obrazy wszystkich
     * za��cznik�w z przekazanych dokument�w.  Strumie� NIE JEST zamykany.
     * @param entities Tablica obiekt�w Long lub Document.
     */
    private static void __attachmentsAsPdf(Object[] entities,
                                         boolean remarksLayer,
                                         OutputStream os)
        throws DocumentException, EdmException, IOException
    {
        final float iwidth = 550, iheight = 700;
        final float absX = 20, absY = 20;

        com.lowagie.text.Document pdfDoc =
            new com.lowagie.text.Document();
        PdfWriter writer = PdfWriter.getInstance(pdfDoc, os);
        pdfDoc.open();
        PdfContentByte cb = writer.getDirectContent();

        File fontDir = new File(Docusafe.getHome(), "fonts");
        File arial = new File(fontDir, "arial.ttf");
        BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
            BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

        Font font = new Font(baseFont, 12);

        boolean attachmentSkipped = false;
        int count = 0;
        boolean addedRemarksLayer = false;

        for (int i=0; i < entities.length; i++)
        {
            Document document;
            if (entities[i] instanceof Document)
                document = (Document) entities[i];
            else
                document = Document.find((Long) entities[i]);

            if (remarksLayer && !addedRemarksLayer &&
                (document instanceof OfficeDocument) &&
                ((OfficeDocument) document).getRemarks().size() > 0)
            {
                // na razie adnotacje tylko na pierwszej stronie sie mieszcza
                PdfLayer l1 = new PdfLayer("Uwagi", writer);

                StringBuilder content = new StringBuilder();
                List remarks = ((OfficeDocument) document).getRemarks();

                for (Iterator ri=remarks.iterator(); ri.hasNext(); )
                {
                    content.append(((Remark) ri.next()).getContent());
                    content.append("\n\n");
                }

                String[] columns = getColumns(cb, content.toString());

                for (int c=0; c < columns.length; c++)
                {
                    cb.beginLayer(l1);
                    Phrase p1 = new Phrase(columns[c], font);
                    ColumnText ct = new ColumnText(cb);
                    ct.setSimpleColumn(p1, 20, 20, 500, 50 * 15, 15, Element.ALIGN_LEFT);
                    ct.go();
                    cb.endLayer();
                }

                addedRemarksLayer = true;

/*
                Phrase p1 = new Phrase(content.toString(), font);
                ColumnText ct = new ColumnText(cb);
                ct.setSimpleColumn(p1, 20, 20, 500, 50 * 15, 15, Element.ALIGN_LEFT);
                ct.go();
                cb.endLayer();
                addedRemarksLayer = true;
*/
            }

            List attachments = document.listAttachments();
            for (Iterator iter=attachments.iterator(); iter.hasNext(); )
            {
                Attachment attachment = (Attachment) iter.next();
                AttachmentRevision revision = attachment.getMostRecentRevision();
                if (attachment.getCn() == null && revision != null)
                {
                    count++;

                    try
                    {
                        if (revision.getMime().indexOf("image/tif") >= 0)
                        {
                            File tmp = File.createTempFile("docusafe_tmp_", ".tmp");
                            revision.saveToFile(tmp);
                            if (log.isDebugEnabled())
                                log.debug("Za��cznik "+revision+" zapisano "+
                                    "do pliku "+tmp+" ("+tmp.length()+")");

                            if (attachmentSkipped)
                            {
                                pdfDoc.newPage();
                                attachmentSkipped = false;
                            }

                            addTiffImage(tmp, pdfDoc, cb, iwidth, iheight, absX, absY);
                            pdfDoc.newPage();
                        }
                        else if (revision.getMime().indexOf("image/png") >= 0 ||
                                 revision.getMime().indexOf("image/gif") >= 0 ||
                                 revision.getMime().indexOf("image/jpeg") >= 0)
                        {
                            File tmp = File.createTempFile("docusafe_tmp_", ".tmp");
                            revision.saveToFile(tmp);
                            if (log.isDebugEnabled())
                                log.debug("Za��cznik "+revision+" zapisano "+
                                    "do pliku "+tmp+" ("+tmp.length()+")");

                            if (attachmentSkipped)
                            {
                                pdfDoc.newPage();
                                attachmentSkipped = false;
                            }

                            addGifJpegPngImage(tmp, pdfDoc, cb, iwidth, iheight, absX, absY);
                            pdfDoc.newPage();
                        }
                        else if (revision.getMime().indexOf("image/bmp") >= 0)
                        {
                            File tmp = File.createTempFile("docusafe_tmp_", ".tmp");
                            revision.saveToFile(tmp);
                            if (log.isDebugEnabled())
                                log.debug("Za��cznik "+revision+" zapisano "+
                                    "do pliku "+tmp+" ("+tmp.length()+")");

                            if (attachmentSkipped)
                            {
                                pdfDoc.newPage();
                                attachmentSkipped = false;
                            }

                            addBmpImage(tmp, pdfDoc, cb, iwidth, iheight, absX, absY);
                            pdfDoc.newPage();
                        }
                        else if (revision.getMime().indexOf("application/pdf") >= 0)
                        {
                            File tmp = File.createTempFile("docusafe_tmp_", ".tmp");
                            revision.saveToFile(tmp);
                            if (log.isDebugEnabled())
                                log.debug("Za��cznik "+revision+" zapisano "+
                                    "do pliku "+tmp+" ("+tmp.length()+")");

                            if (attachmentSkipped)
                            {
                                pdfDoc.newPage();
                                attachmentSkipped = false;
                            }

                            addPdf(tmp, pdfDoc, writer, cb, iwidth, iheight, absX, absY);
                            pdfDoc.newPage();
                        }
                        else
                        {
                            Integer officeNumber = null;
                            if (entities[i] instanceof OfficeDocument)
                            {
                                officeNumber = ((OfficeDocument) entities[i]).getOfficeNumber();
                            }

                            StringBuilder message = new StringBuilder(sm.getString("PominietoPlik")+" ");
                            message.append(revision.getOriginalFilename());
                            message.append("\n");
                            if (officeNumber != null)
                            {
                                message.append(sm.getString("NumerKO")+": ");
                                message.append(officeNumber);
                                message.append("\n");
                            }
                            else
                            {
                                message.append(sm.getString("DokumentNr")+" ");
                                message.append(document.getId());
                                message.append("\n");
                            }

                            if (entities[i] instanceof OfficeDocument)
                            {
                                message.append(sm.getString("Tytul")+": ");
                                message.append(((OfficeDocument) entities[i]).getSummary());
                                message.append("\n");
                            }
                            else
                            {
                                message.append(sm.getString("Tytul")+": ");
                                message.append(document.getTitle());
                                message.append("\n");
                            }

                            message.append(sm.getString("TytulZalacznika")+": ");
                            message.append(attachment.getTitle());
                            message.append("\n");

                            pdfDoc.add(new Paragraph(message.toString(), font));
                            pdfDoc.add(new Paragraph("", font));
                            attachmentSkipped = true;
                        }
                    }
                    catch (EdmException e)
                    {
                        StringBuilder message = new StringBuilder();
                        message.append(sm.getString("NapotkanoBlednyZalacznik")+": ");
                        message.append(revision.getOriginalFilename());
                        message.append("\n");
                        message.append(sm.getString("DokumentNr")+" ");
                        message.append(document.getId());
                        message.append(", "+sm.getString("ZalacznikNr")+" ");
                        message.append(attachment.getId().toString());
                        message.append(".");
                        message.append(revision.getId().toString());
                        message.append("\n");
                        message.append(sm.getString("KomunikatBledu")+": \"");
                        message.append(e.getMessage());
                        message.append("\"");
                        message.append("\n");
                        pdfDoc.add(new Paragraph(message.toString(), font));
                        pdfDoc.add(new Paragraph("", font));
                    }
                }
            }
        }

        if (count == 0)
        {
            pdfDoc.add(new Phrase(sm.getString("WwybranymZakresieWyszukiwaniaNieZnalezionoZalacznikow"), font));
        }

        pdfDoc.close();
        writer.close();
    }

    /**
     * Metoda dzieli podany tekst na kolumny w ten sposob, ze doklada kolejny wyraz
     * i sprawdza czy caly tekst miesci sie w kolumnie, jesli nie to tworzona jest nastepna kolumna
     * @param cb Obiekt klasy PdfContentByte
     * @param text wyswietlany tekst
     * @return Tablica tekstow w kolejnych kolumnach
     * @throws DocumentException
     */
    private static String[] getColumns(PdfContentByte cb, String text) throws DocumentException {

        ArrayList columns = new ArrayList();
        StringTokenizer tokenizer = new StringTokenizer(text, " \n\r\t");
        StringBuilder buffer = new StringBuilder();
        while (tokenizer.hasMoreTokens()) {
            int status = ColumnText.NO_MORE_TEXT;
            String piece = tokenizer.nextToken();
            Phrase p1 = new Phrase(buffer.toString() + piece);
            ColumnText ct = new ColumnText(cb);
            ct.setSimpleColumn(p1, 20, 20, 500, 50 * 15, 15, Element.ALIGN_LEFT);
            status = ct.go(true);
            if (status == ColumnText.NO_MORE_COLUMN) {
                columns.add(buffer.toString());
                buffer = new StringBuilder();
            }
            buffer.append(piece);
            buffer.append(" ");
        }
        columns.add(buffer.toString());
        return (String[]) columns.toArray(new String[columns.size()]);
    }

    private static void addTiffImage(File file, com.lowagie.text.Document pdfDoc,
                                     PdfContentByte cb, float width, float height,
                                     float absX, float absY)
        throws IOException, DocumentException, EdmException
    {
        RandomAccessFileOrArray raf = new RandomAccessFileOrArray(file.getAbsolutePath());
        int pages = TiffImage.getNumberOfPages(raf);
        for (int i=0; i < pages; ++i)
        {
            Image image = null;
            try
            {
                image = TiffImage.getTiffImage(raf, i+1);
            }
            catch (Exception e)
            {
                throw new EdmException(sm.getString("BladOdczytuStrony")+": "+e.toString());
            }
            float imageWidth = image.plainWidth();
            float imageHeight = image.plainHeight();

            // niekt�re faksy maj� rozdzielczo�� 204x98 dpi, trzeba
            // je w�wczas rozci�gn�� w d� dwukrotnie
            // @see RelayIncomingFax
            if (imageHeight < imageWidth)
            {
                imageHeight *= 2;
            }

            float sx = imageWidth > width ? width / imageWidth : 1.0f;
            float sy = imageHeight > height ? height / imageHeight : 1.0f;

            float scale = Math.min(sx, sy);

            if (imageWidth > width || imageHeight > height)
            {
                image.scaleAbsolute(scale * imageWidth, scale * imageHeight);
            }

            image.setAbsolutePosition(absX, absY);
            cb.addImage(image);
            if (pages > 1 && i < pages-1)
                pdfDoc.newPage();
        }
    }

    private static void addBmpImage(File file, com.lowagie.text.Document pdfDoc,
                                    PdfContentByte cb, float width, float height,
                                    float absX, float absY)
        throws IOException, DocumentException
    {
        Image image = BmpImage.getImage(file.getAbsolutePath());
        image.scaleToFit(width, height);
        image.setAbsolutePosition(absX, absY);
        cb.addImage(image);
        //pdfDoc.newPage();
    }

    private static void addGifJpegPngImage(File file, com.lowagie.text.Document pdfDoc,
                                    PdfContentByte cb, float width, float height,
                                    float absX, float absY)
        throws IOException, DocumentException
    {
        Image image = Image.getInstance(file.getAbsolutePath());
        image.scaleToFit(width, height);
        image.setAbsolutePosition(absX, absY);
        cb.addImage(image);
        //pdfDoc.newPage();
    }

    private static void addPdf(File file, com.lowagie.text.Document pdfDoc,
                               PdfWriter writer, PdfContentByte cb,
                               float width, float height, float absX, float absY)
        throws IOException, DocumentException
    {
        PdfReader reader = new PdfReader(file.getAbsolutePath());

        for (int i=0; i < reader.getNumberOfPages(); i++)
        {
            int pageNumber = i+1;
            pdfDoc.setPageSize(reader.getPageSizeWithRotation(pageNumber));
            PdfImportedPage page = writer.getImportedPage(reader, pageNumber);
            int rotation = reader.getPageRotation(pageNumber);
            if (rotation == 90 || rotation == 270) {
                cb.addTemplate(page, 0, -1f, 1f, 0, 0, reader.getPageSizeWithRotation(pageNumber).height());
            }
            else {
                cb.addTemplate(page, 1f, 0, 0, 1f, 0, 0);
            }
            pdfDoc.newPage();
        }
    }

    public static void addHeaderImageToPdf(com.lowagie.text.Document pdfDoc, String title, BaseFont baseFont) throws com.lowagie.text.DocumentException
    {
        String filename = Configuration.getProperty("office.header_image");
        if (filename != null)
        {
            Rectangle imageSize = new Rectangle(50,50);
            Image image = null;
            boolean correctImage = true;
            try
            {
                File file = new File(Docusafe.getHome(), filename);
                image = LayeredPdf.getImage(file, 0);
                image.scaleToFit(imageSize.width(), imageSize.height());
                image.setAbsolutePosition(0, 0);
            }
            catch (IOException e)
            {
                correctImage = false;
            }

            Font font = new Font(baseFont, 18);
            font.setStyle("bold");

            Paragraph paragraph;
            if (correctImage)
            {
                Chunk chunk = new Chunk(image, 0, -20);
                paragraph = new Paragraph(chunk);
                Phrase phrase = new Phrase("    "+title, font);
                paragraph.add(phrase);
                paragraph.setSpacingAfter(20);
            }
            else
            {
                Chunk chunk = new Chunk(title, font);
                paragraph = new Paragraph(chunk);
                paragraph.setAlignment(Element.ALIGN_CENTER);
            }
           
            	

            pdfDoc.add(paragraph);
        }
    }
    
    /**
     *  Funkcja, kt�ra pobiera id za��cznika zwraca go jako plik PDF.
     *  Je�li jest wi�cej ni� jedna wersja za��cznika to zwraca najnowsz�.
     *  W StringBuffer-ze podanym jako parametr zapisuje oryginaln� nazwe pliku.
     * @throws IOException 
     * @throws DocumentException 
     */
    
    public static File getAttachmentAsFile(long attachment_id, StringBuffer sb) throws EdmException, SQLException, IOException, DocumentException
    {
    	final float iwidth = 550, iheight = 700;
        final float absX = 20, absY = 20;    	

    	checkDirectory();
    	String outFileName = getTempPDFName(attachment_id);
    	File pom = new File(outFileName);   	
    	if (!pom.exists())
    	{
	    	DataSource dataSource = null;
	        Connection connection = null;
	        PreparedStatement ps = null;
	        String mime;
	        File f = null;
	        try
	        {
	        	
	        	dataSource = Docusafe.getDataSource();
	        	connection = dataSource.getConnection(); 
	        	ps = connection.prepareStatement("select id, contentdata, onDisc, originalfilename,mime from ds_attachment_revision " +
	                                                           "where attachment_id=? order by ctime desc");
	        	ps.setLong(1, attachment_id);
	        	ResultSet rs = ps.executeQuery();
	        	if (!rs.next())
	        		throw new EdmException("Attachment revision not found");
	        
	        	mime = rs.getString("mime");
	        	int onDisc = rs.getInt("onDisc");	        
	        
	        	
	        
	        	if(AttachmentRevision.FILE_ON_DISC.equals(onDisc))
	        	{	        	
	        		String path = Attachment.getPath(rs.getLong("ID"));	            	
	        		try
	        		{ 
	        			f = new File(path);
	        		}
	        		catch(Exception e)
	        		{
	        			LogFactory.getLog("eprint").debug("", e);
	        		}
	        	}
	        	else
	        	{
	        		f  = File.createTempFile("docusafe", "tmp");
	        		FileOutputStream fos = new FileOutputStream(f);
	        		InputStream is = rs.getBinaryStream("contentData");			        		
	        		org.apache.commons.io.IOUtils.copy(is,fos);
	        		org.apache.commons.io.IOUtils.closeQuietly(is);
	        		org.apache.commons.io.IOUtils.closeQuietly(fos);
	        	}
	        	rs.close();
	        	
	        }
	        finally
	        {
	        	if (ps!=null) { try { ps.close(); } catch (Exception e) { } }
	        	if (connection!= null) { try { connection.close(); } catch (Exception e) { } }
	        }
	        FileOutputStream os = new FileOutputStream(outFileName); 
	        
	        com.lowagie.text.Document pdfDoc = new com.lowagie.text.Document();
	        PdfWriter writer = PdfWriter.getInstance(pdfDoc, os);
	        pdfDoc.open();
	        PdfContentByte cb = writer.getDirectContent();
	        
	        if (mime.indexOf("image/tif") >= 0)
	        {
	            addTiffImage(f, pdfDoc, cb, iwidth, iheight, absX, absY);
	            pdfDoc.newPage();
	        }
	        else if (mime.indexOf("image/png") >= 0 ||
	        		mime.indexOf("image/gif") >= 0 ||
	        		mime.indexOf("image/jpeg") >= 0)
	        {
	            addGifJpegPngImage(f, pdfDoc, cb, iwidth, iheight, absX, absY);
	            pdfDoc.newPage();
	        }
	        else if (mime.indexOf("image/bmp") >= 0)
	        {
	            addBmpImage(f, pdfDoc, cb, iwidth, iheight, absX, absY);
	            pdfDoc.newPage();
	        }
	        else if (mime.indexOf("application/pdf") >= 0)
	        {
	            addPdf(f, pdfDoc, writer, cb, iwidth, iheight, absX, absY);
	            pdfDoc.newPage();
	        }
	        else
	        {
	        	pdfDoc.newPage();
	        	return null;
	        }       

	        pdfDoc.close();
   			writer.close();
   			
   			org.apache.commons.io.IOUtils.closeQuietly(os);
   			
  
	       return new File(outFileName);
    	}
    	
    	return new File(outFileName);
       
    }
    
    /**
     *  Funkcja, kt�ra pobiera liste plikow png i zwraca jako plik PDF.
     * @throws IOException 
     * @throws DocumentException 
     */
    public static File getAttachmentAsFileFromPages(List<String> pages) throws EdmException, SQLException, IOException, DocumentException
    {
    	List<File> files = new ArrayList<File>();
    	
		for (String page : pages)
		{
			System.out.println("Sciezka do pliku z ktorego tworzymy nowy PDF zalacznik: " + page);

			String repl = page.replace("\\", "/");
			log.info("Wczytuje strone pdf " + repl);
			if (repl != null && repl.length() > 0)
			{
				File ft = new File(repl + ".png");
				System.out.println(ft.getAbsolutePath());
				if (ft.exists())
				{
					log.trace("Strona istnieje " + repl);
					files.add(ft);
				}
				else
				{
					throw new IOException("Brak strony do generowania za��cznika o nazwie: " + repl+".png");
				}
			}
		}
        
        File file = File.createTempFile("attachment_from_pages", ".pdf");
		OutputStream os =  new FileOutputStream(file);
	    LayeredPdf.filesAsPdf(files.iterator(), null, null, false, os, null, null, null);
	    os.close();
    	return file;  
    }
    
    private static String getTempPDFName(Long attachmentId)
    {	
    	return ViewerAction.getDecompressedPath()+"/"+attachmentId+".00P";
    }
    	
    private static void checkDirectory() throws IOException
    {
    	String path = ViewerAction.getDecompressedPath();
    	new File(path.toString()).mkdirs();    	
    	
    }    
    
    /**Splituje storne na A4 dodajac margin do szerokosci */
    public static File splitA3toA4(File source,Integer margin) throws Exception, DocumentException
    {
    	File dest = File.createTempFile("a3toa4", ".pdf");
    	
        // we create a reader for a certain document 
        PdfReader reader = new PdfReader(new FileInputStream(source));
        // we retrieve the total number of pages and the page size 
        int total = reader.getNumberOfPages(); 
        
        Rectangle pageSize = reader.getPageSize(1); 
        Rectangle newSize = new Rectangle(PageSize.A4.width() , PageSize.A4.height()); 
        // step 1: creation of a document-object 
        com.lowagie.text.Document document = new com.lowagie.text.Document(newSize); 
        // step 2: we create a writer that listens to the document 
        PdfWriter writer = PdfWriter.getInstance(document,  new FileOutputStream(dest)); 
        // step 3: we open the document
        document.open(); 
        // step 4: adding the content 
        PdfContentByte cb = writer.getDirectContent(); 
        PdfImportedPage page;
        int p; 
        for (int i = 0; i < total; i++) 
        { 
          p = i + 1; 
          pageSize = reader.getPageSize(p); 
    	  if(pageSize.width() > 600)
    	  {
        	  Rectangle ns = new Rectangle((pageSize.width()/2)+margin , pageSize.height()); 
	          document.setPageSize(ns);
    	  }
    	  if(p >1)
        	  document.newPage(); 
          int rotation = reader.getPageRotation(1);
          page = writer.getImportedPage(reader, p);
          if( rotation == 90)
          {
	          cb.addTemplate(page, 0, -1, 1, 0,0, PageSize.A4.height()); 
	          if(pageSize.width() > 750)
	          {
	        	  
	        	  document.newPage();
		          page = writer.getImportedPage(reader, p); 
		          cb.addTemplate(page, 0, -1, 1, 0,-(pageSize.width()/2)+(margin), PageSize.A4.height()); 
	          }
	          document.setPageSize(PageSize.A4);
          }
          else
          {
	          cb.addTemplate(page, 1, 0, 0, 1,0, 0); 
	          
	          if(pageSize.width() > 750)
	          {
	        	  document.newPage();
		          page = writer.getImportedPage(reader, p); 
		          cb.addTemplate(page, 1, 0, 0, 1,-(pageSize.width()/2)+(margin), 0); 
	          }
	          document.setPageSize(PageSize.A4);
          }
        } 	
        document.close();     
        return dest;
    }

	public static void addHeaderImageToPdfUTP(com.lowagie.text.Document pdfDoc, String title, BaseFont baseFont, boolean extended)
			throws com.lowagie.text.DocumentException
	{

		String filename = Configuration.getProperty("office.header_image");
		if (filename != null)
		{
			Rectangle imageSize = new Rectangle(50, 50);
			Image image = null;
			boolean correctImage = true;
			try
			{
				File file = new File(Docusafe.getHome(), filename);
				image = LayeredPdf.getImage(file, 0);
				image.scaleToFit(imageSize.width(), imageSize.height());
				image.setAbsolutePosition(0, 0);
			} catch (IOException e)
			{
				correctImage = false;
			}

			Font font = new Font(baseFont, 18);
			font.setStyle("bold");

			Paragraph paragraph;
			if (correctImage)
			{
				Chunk chunk = new Chunk(image, 0, -20);
				paragraph = new Paragraph(chunk);
				Phrase phrase = new Phrase("    " + title, font);
				paragraph.add(phrase);
				paragraph.setSpacingAfter(20);
			} else
			{
				Chunk chunk = new Chunk(title, font);
				paragraph = new Paragraph(chunk);
				paragraph.setAlignment(Element.ALIGN_CENTER);
			}
			pdfDoc.add(paragraph);
		}
		if (extended)
		{
			Chunk chunk = null;
			Font font = new Font(baseFont, 10);
			font.setStyle("bold");
			Paragraph paragraph;
			String zal = "Za�acznik nr.................";
			chunk = new Chunk(zal, font);
			paragraph = new Paragraph(chunk);
			paragraph.setAlignment(Element.ALIGN_RIGHT);
			pdfDoc.add(paragraph);

			String s = "Imi� i Nazwisko (nazwa) oraz adres nadawcy............................";
			chunk = new Chunk(s, font);
			paragraph = new Paragraph(chunk);
			paragraph.setAlignment(Element.ALIGN_LEFT);
			pdfDoc.add(paragraph);
		}


	}

    /**
     * <p>Metoda konwertuje rtf do pdf za pomoc� programu OpenOffice uruchomionego jako us�uga.</p>
     *
     * <p>OpenOffice uruchamiamy poleceniem: <code>soffice -headless -accept="socket,host=127.0.0.1,port=8100;urp;" -nofirststartwizard</code></p>
     *
     * <p>Wi�cej info: <a href="http://www.artofsolving.com/opensource/jodconverter">http://www.artofsolving.com/opensource/jodconverter</a></p>
     *
     * @deprecated use {@link pl.compan.docusafe.integration.DocumentConverter} instead.
     * @param inputStream
     * @param outputStream
     * @param setPageNumber
     * @throws EdmException
     */
    public static void convertRtfToPdf(InputStream inputStream, OutputStream outputStream, boolean setPageNumber) throws EdmException
    {
        Integer port = AdditionManager.getIntProperty("openOffice.port");
        if(port == null) {
            port = AdditionManager.getIntPropertyOrDefault("openoffice.port", 8100);
        }

        String host = AdditionManager.getPropertyOrDefault("openoffice.host", "127.0.0.1");

        try
        {
            Class.forName("com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection");
            OpenOfficeConnection connection = new SocketOpenOfficeConnection(host, port);

            connection.connect();
            // convert
            DocumentConverter converter = new OpenOfficeDocumentConverter(connection);
            DefaultDocumentFormatRegistry bb = new DefaultDocumentFormatRegistry();
            DocumentFormat rtf = bb.getFormatByFileExtension("rtf");
            DocumentFormat pdf = bb.getFormatByFileExtension("pdf");
            if (!setPageNumber)
                converter.convert(inputStream, rtf, outputStream, pdf);
            else
            {
                OutputStream pdfOutput = new ByteArrayOutputStream();
                converter.convert(inputStream, rtf, pdfOutput, pdf);
                try
                {
                    InputStream decodedInputFromPdfOutput = new ByteArrayInputStream(((ByteArrayOutputStream) pdfOutput).toByteArray());
                    PdfReader reader = new PdfReader(decodedInputFromPdfOutput);
                    int n = reader.getNumberOfPages();

                    PdfStamper stamp = new PdfStamper(reader, outputStream);

                    int i = 0;

                    PdfContentByte over;
                    BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.EMBEDDED);
                    while (i < n)
                    {
                        i++;
                        over = stamp.getOverContent(i);
                        over.beginText();
                        over.setFontAndSize(bf, 8);
                        over.setTextMatrix(30, 30);
                        over.showText("Strona " + i + " z " + n);
                        over.setFontAndSize(bf, 32);
                        over.endText();
                    }
                    stamp.close();
                }
                catch (Exception de)
                {
                    de.printStackTrace();
                }
            }
            // close the connection
            connection.disconnect();
        }catch (ConnectException ex){
        	 throw new EdmException("Open Office nie jest uruchomiony");
        }
        catch (Exception ex)
        {
            throw new EdmException(ex.getMessage(), ex);
        }
    }

    /**
     * <p>
     *     Konwersja <code>input</code> na <code>output</code> poprzez OpenOffice.
     * </p>
     *
     * <p>
     *     <b>Uwaga</b>: algorytm nie jest wydajny dla du�ej liczby wywo�a�.
     *     Tworzone jest za ka�dym razem po��czenie do OpenOffice. Przyk�adowo
     *     zgodnie z <a href="http://www.artofsolving.com/node/16">dokumentacj�</a>
     *     na stronie JOD Convertara nale�a�oby utworzy� jedno po��czenie przy inicjalizacji
     *     aplikacji.
     * </p>
     *
     * @deprecated use {@link pl.compan.docusafe.integration.DocumentConverter} instead.
     * @param input plik inputowy
     * @param output pusty plik outputowy, do kt�rego b�dzie zapisywany wynik konwersji
     * @throws ConnectException
     */
    public static void documentToPdf(File input, File output) throws ConnectException {
        // connect to an OpenOffice.org instance

        int port = AdditionManager.getIntPropertyOrDefault("openoffice.port", 8100);
        String host = AdditionManager.getPropertyOrDefault("openoffice.host", "127.0.0.1");

        OpenOfficeConnection connection = new SocketOpenOfficeConnection(host, port);
        connection.connect();

        // convert
        DocumentConverter converter = new OpenOfficeDocumentConverter(connection);
        converter.convert(input, output);

        // close the connection
        connection.disconnect();
    }

}
