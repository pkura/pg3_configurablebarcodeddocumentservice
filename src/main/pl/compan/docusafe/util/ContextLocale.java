package pl.compan.docusafe.util;

import java.util.Locale;

/**
 * Klasa zarz�dzaj�ca obiektami Locale zwi�zanymi z w�tkami.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ContextLocale.java,v 1.3 2006/02/14 16:00:05 lk Exp $
 */
public class ContextLocale
{
    private static ThreadLocal<Locale> contextLocales = new ThreadLocal<Locale>();
    private static StringManager sm = StringManager.getManager(Constants.Package);

    /**
     * Zwraca obiekt Locale skojarzony z bie��cym w�tkiem.
     * Je�eli z w�tkiem nie skojarzono obiektu Locale, zwracana
     * jest warto�� parametru defaultLocale.
     * @return Obiekt Locale skojarzony z bie��cym w�tkiem.
     */
    public static Locale getLocale(Locale defaultLocale)
    {
        Locale locale = contextLocales.get();
        return locale != null ? locale : defaultLocale;
    }

    /**
     * Zwraca obiekt Locale skojarzony z bie��cym w�tkiem.
     * Je�eli z w�tkiem nie skojarzono obiektu Locale, zwracana
     * jest warto�� Locale.getDefault().
     * @return Obiekt Locale skojarzony z bie��cym w�tkiem.
     */
    public static Locale getLocale()
    {
        return getLocale(LocaleUtil.getLocale());
    }

    /**
     * Wi��e obiekt Locale z bie��cym w�tkiem. Obiekt ten mo�na
     * nast�pnie odczyta� przy pomocy metody getLocale().
     * @param locale Obiekt Locale kojarzony z bie��cym w�tkiem,
     *  nie mo�e by� r�wny null.
     */
    public static void setContextLocale(Locale locale)
    {
        if (locale == null)
            throw new NullPointerException(
                sm.getString("contextLocale.nullParameter", "locale"));

        // XXX: mo�na sprawdza�, czy najpopularniejsze klasy
        // wspieraj� dane Locale (np. Number.getAvailableLocales())

        contextLocales.set(locale);
    }
}
