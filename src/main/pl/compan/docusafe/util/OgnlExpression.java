package pl.compan.docusafe.util;

import ognl.Ognl;
import ognl.OgnlContext;
import ognl.OgnlException;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmOgnlException;

/**
 * @version $Id: OgnlExpression.java,v 1.6 2006/02/20 15:42:31 lk Exp $
 */
public class OgnlExpression
{
    private Object expression;

    public OgnlExpression(String expressionString) throws EdmException
    {
        try
        {
            expression = Ognl.parseExpression(expressionString);
        }
        catch (OgnlException e)
        {
            throw new EdmOgnlException(e, String.valueOf(expression));
        }
    }

    public Object getExpression()
    {
        return expression;
    }

    public Object getValue(OgnlContext context, Object rootObject) throws EdmException
    {
        try
        {
            return Ognl.getValue(getExpression(), context, rootObject);
        }
        catch (OgnlException e)
        {
            throw new EdmOgnlException(e, String.valueOf(expression));
        }
    }

    public void setValue(OgnlContext context, Object rootObject, Object value) throws EdmException
    {
        try
        {
            Ognl.setValue(getExpression(), context, rootObject, value);
        }
        catch (OgnlException e)
        {
            throw new EdmOgnlException(e, String.valueOf(expression));
        }
    }

    /**
     * getTotal(new OgnlContext(), new Object())
     */ 
    public Object getValue() throws EdmException
    {
        try
        {
            return Ognl.getValue(getExpression(), new OgnlContext(), new Object());
        }
        catch (OgnlException e)
        {
            throw new EdmOgnlException(e, String.valueOf(expression));
        }
    }
}
